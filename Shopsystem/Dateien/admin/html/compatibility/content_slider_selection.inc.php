<?php
/* --------------------------------------------------------------
   content_slider_selection.inc.php 2016-11-01
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * This file is included in admin/content_manager.php
 */

function generateContentSliderSelect($group_id)
{
	global $sliderArray;
	
	$t_content_slider_check = xtc_db_query('SELECT slider_set_id FROM ' . TABLE_CONTENT_SLIDER_SET
	                                       . ' WHERE content_slider_set_id = ' . xtc_db_input($group_id));
	$t_result               = xtc_db_fetch_array($t_content_slider_check);
	$t_content_slider_id    = $t_result['slider_set_id'];
	
	$pro_slider_set_id  = $pInfo->slider_set_id;
	$html               = '';
	$t_text_select_none = TEXT_SELECT_NONE;
	if(strpos($p_param_name, 'index') > 0)
	{
		$t_text_select_none = TEXT_SELECT_NONE_INDEX;
	}
	$html .= '<select name="content_slider" size="1">' . "";
	$html .= '<option value="0">' . $t_text_select_none . '</option>' . "<br>\n";
	foreach($sliderArray as $f_key => $coo_slider)
	{
		$t_slider_set_id   = $coo_slider->v_slider_set_id;
		$t_slider_set_name = $coo_slider->v_slider_set_name;
		$t_mark            = ($t_slider_set_id == $t_content_slider_id)
			? ' selected="selected"'
			: '';
		$html .= '<option value="' . $t_slider_set_id . '"' . $t_mark . '>' . $t_slider_set_name . '</option>'
		         . "<br>\n";
	}
	$html .= '</select>' . "";
	
	return $html;
}