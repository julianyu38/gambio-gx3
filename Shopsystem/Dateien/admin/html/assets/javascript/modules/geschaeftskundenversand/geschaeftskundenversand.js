'use strict';

/* --------------------------------------------------------------
	geschaeftskundenversand.js 2016-07-06
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$(function () {
	'use strict';

	var $table = $('.orders .table-main');

	$table.on('init.dt', function () {

		var _initSingleAction = function _initSingleAction($theTable) {
			$theTable.find('.btn-group.dropdown').each(function () {
				var orderId = $(this).parents('tr').data('id'),
				    defaultRowAction = $theTable.data('init-default-row-action') || 'edit';

				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('gkv_label_get', 'module_center_module'),
					href: jse.core.config.get('appUrl') + '/admin/admin.php?do=Geschaeftskundenversand/PrepareLabel&oID=' + orderId,
					class: 'gkv-single',
					data: { configurationValue: 'gkv-single' },
					isDefault: defaultRowAction === 'gkv-single'
				});
			});
		};

		/*
  const _initBulkAction = function() {
  	var isDefault = $table.data('defaultBulkAction') === 'gkv-bulk';
  	jse.libs.button_dropdown.addAction($('.bulk-action'), {
  		text: jse.core.lang.translate('gkv_label_get', 'module_center_module'),
  		href: jse.core.config.get('appUrl') + '/admin/admin.php?do=Geschaeftskundenversand/PrepareLabelBulk',
  		data: {configurationValue: 'gkv-bulk'},
  		isDefault: isDefault,
  	});
  }
  */

		$table.on('draw.dt', function () {
			return _initSingleAction($table);
		});

		_initSingleAction($table);
		// _initBulkAction();
	});
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdlc2NoYWVmdHNrdW5kZW52ZXJzYW5kLmpzIl0sIm5hbWVzIjpbIiQiLCIkdGFibGUiLCJvbiIsIl9pbml0U2luZ2xlQWN0aW9uIiwiJHRoZVRhYmxlIiwiZmluZCIsImVhY2giLCJvcmRlcklkIiwicGFyZW50cyIsImRhdGEiLCJkZWZhdWx0Um93QWN0aW9uIiwianNlIiwibGlicyIsImJ1dHRvbl9kcm9wZG93biIsImFkZEFjdGlvbiIsInRleHQiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsImhyZWYiLCJjb25maWciLCJnZXQiLCJjbGFzcyIsImNvbmZpZ3VyYXRpb25WYWx1ZSIsImlzRGVmYXVsdCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVdBQSxFQUFFLFlBQVc7QUFDWjs7QUFFQSxLQUFNQyxTQUFTRCxFQUFFLHFCQUFGLENBQWY7O0FBRUFDLFFBQU9DLEVBQVAsQ0FBVSxTQUFWLEVBQXFCLFlBQVc7O0FBRS9CLE1BQU1DLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQVNDLFNBQVQsRUFBb0I7QUFDN0NBLGFBQVVDLElBQVYsQ0FBZSxxQkFBZixFQUFzQ0MsSUFBdEMsQ0FBMkMsWUFBVztBQUNyRCxRQUFNQyxVQUFVUCxFQUFFLElBQUYsRUFBUVEsT0FBUixDQUFnQixJQUFoQixFQUFzQkMsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBaEI7QUFBQSxRQUNDQyxtQkFBbUJOLFVBQVVLLElBQVYsQ0FBZSx5QkFBZixLQUE2QyxNQURqRTs7QUFHQUUsUUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ2QsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDZSxXQUFNSixJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixlQUF4QixFQUF5QyxzQkFBekMsQ0FEcUM7QUFFM0NDLFdBQU1SLElBQUlLLElBQUosQ0FBU0ksTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsK0RBQWhDLEdBQWtHZCxPQUY3RDtBQUczQ2UsWUFBTyxZQUhvQztBQUkzQ2IsV0FBTSxFQUFDYyxvQkFBb0IsWUFBckIsRUFKcUM7QUFLM0NDLGdCQUFXZCxxQkFBcUI7QUFMVyxLQUE1QztBQU9BLElBWEQ7QUFZQSxHQWJEOztBQWVBOzs7Ozs7Ozs7Ozs7QUFZQVQsU0FBT0MsRUFBUCxDQUFVLFNBQVYsRUFBcUI7QUFBQSxVQUFNQyxrQkFBa0JGLE1BQWxCLENBQU47QUFBQSxHQUFyQjs7QUFFQUUsb0JBQWtCRixNQUFsQjtBQUNBO0FBQ0EsRUFqQ0Q7QUFrQ0EsQ0F2Q0QiLCJmaWxlIjoiZ2VzY2hhZWZ0c2t1bmRlbnZlcnNhbmQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRnZXNjaGFlZnRza3VuZGVudmVyc2FuZC5qcyAyMDE2LTA3LTA2XG5cdEdhbWJpbyBHbWJIXG5cdGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG5cdENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuXHRSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcblx0W2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG5cdC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4qL1xuXG5cbiQoZnVuY3Rpb24oKSB7XG5cdCd1c2Ugc3RyaWN0JztcblxuXHRjb25zdCAkdGFibGUgPSAkKCcub3JkZXJzIC50YWJsZS1tYWluJyk7XG5cblx0JHRhYmxlLm9uKCdpbml0LmR0JywgZnVuY3Rpb24oKSB7XG5cblx0XHRjb25zdCBfaW5pdFNpbmdsZUFjdGlvbiA9IGZ1bmN0aW9uKCR0aGVUYWJsZSkge1xuXHRcdFx0JHRoZVRhYmxlLmZpbmQoJy5idG4tZ3JvdXAuZHJvcGRvd24nKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRjb25zdCBvcmRlcklkID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoJ2lkJyksXG5cdFx0XHRcdFx0ZGVmYXVsdFJvd0FjdGlvbiA9ICR0aGVUYWJsZS5kYXRhKCdpbml0LWRlZmF1bHQtcm93LWFjdGlvbicpIHx8ICdlZGl0JztcblxuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcblx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZ2t2X2xhYmVsX2dldCcsICdtb2R1bGVfY2VudGVyX21vZHVsZScpLFxuXHRcdFx0XHRcdGhyZWY6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89R2VzY2hhZWZ0c2t1bmRlbnZlcnNhbmQvUHJlcGFyZUxhYmVsJm9JRD0nICsgb3JkZXJJZCxcblx0XHRcdFx0XHRjbGFzczogJ2drdi1zaW5nbGUnLFxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdna3Ytc2luZ2xlJ30sXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAnZ2t2LXNpbmdsZScsXG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0fVxuXG5cdFx0Lypcblx0XHRjb25zdCBfaW5pdEJ1bGtBY3Rpb24gPSBmdW5jdGlvbigpIHtcblx0XHRcdHZhciBpc0RlZmF1bHQgPSAkdGFibGUuZGF0YSgnZGVmYXVsdEJ1bGtBY3Rpb24nKSA9PT0gJ2drdi1idWxrJztcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCgnLmJ1bGstYWN0aW9uJyksIHtcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2drdl9sYWJlbF9nZXQnLCAnbW9kdWxlX2NlbnRlcl9tb2R1bGUnKSxcblx0XHRcdFx0aHJlZjoganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1HZXNjaGFlZnRza3VuZGVudmVyc2FuZC9QcmVwYXJlTGFiZWxCdWxrJyxcblx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2drdi1idWxrJ30sXG5cdFx0XHRcdGlzRGVmYXVsdDogaXNEZWZhdWx0LFxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdCovXG5cblx0XHQkdGFibGUub24oJ2RyYXcuZHQnLCAoKSA9PiBfaW5pdFNpbmdsZUFjdGlvbigkdGFibGUpKTtcblxuXHRcdF9pbml0U2luZ2xlQWN0aW9uKCR0YWJsZSk7XG5cdFx0Ly8gX2luaXRCdWxrQWN0aW9uKCk7XG5cdH0pO1xufSk7XG4iXX0=
