'use strict';

/* --------------------------------------------------------------
	geschaeftskundenversand-orderdetails.js 2016-08-31
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$(function () {
	$('div.gkv_orderdetails').closest('div.frame-wrapper').hide();

	var gkvButtonDropdownHandler = function gkvButtonDropdownHandler(event) {
		var orderId = $(event.target).parents('tr').data('row-id') || $('body').find('#gm_order_id').val();
		window.location = jse.core.config.get('appUrl') + '/admin/admin.php?do=Geschaeftskundenversand/PrepareLabel&oID=' + orderId;
	};

	var interval_counter = 10,
	    interval = setInterval(function () {
		if (jse.libs.button_dropdown && $('.js-button-dropdown').length) {
			clearInterval(interval);
			jse.libs.button_dropdown.mapAction($('.bottom-save-bar'), 'gkv_label_get', 'admin_labels', gkvButtonDropdownHandler);
		}
		if (interval_counter-- === 0) {
			clearInterval(interval);
		}
	}, 400);
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdlc2NoYWVmdHNrdW5kZW52ZXJzYW5kLW9yZGVyZGV0YWlscy5qcyJdLCJuYW1lcyI6WyIkIiwiY2xvc2VzdCIsImhpZGUiLCJna3ZCdXR0b25Ecm9wZG93bkhhbmRsZXIiLCJldmVudCIsIm9yZGVySWQiLCJ0YXJnZXQiLCJwYXJlbnRzIiwiZGF0YSIsImZpbmQiLCJ2YWwiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJpbnRlcnZhbF9jb3VudGVyIiwiaW50ZXJ2YWwiLCJzZXRJbnRlcnZhbCIsImxpYnMiLCJidXR0b25fZHJvcGRvd24iLCJsZW5ndGgiLCJjbGVhckludGVydmFsIiwibWFwQWN0aW9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLEVBQUUsWUFBVztBQUNaQSxHQUFFLHNCQUFGLEVBQTBCQyxPQUExQixDQUFrQyxtQkFBbEMsRUFBdURDLElBQXZEOztBQUVBLEtBQUlDLDJCQUEyQixTQUEzQkEsd0JBQTJCLENBQVNDLEtBQVQsRUFDL0I7QUFDQyxNQUFJQyxVQUFVTCxFQUFFSSxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4QkMsSUFBOUIsQ0FBbUMsUUFBbkMsS0FBZ0RSLEVBQUUsTUFBRixFQUFVUyxJQUFWLENBQWUsY0FBZixFQUErQkMsR0FBL0IsRUFBOUQ7QUFDQUMsU0FBT0MsUUFBUCxHQUFrQkMsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQywrREFBaEMsR0FBa0dYLE9BQXBIO0FBQ0EsRUFKRDs7QUFNQSxLQUFJWSxtQkFBbUIsRUFBdkI7QUFBQSxLQUNJQyxXQUFXQyxZQUFZLFlBQVk7QUFDdEMsTUFBSU4sSUFBSU8sSUFBSixDQUFTQyxlQUFULElBQTRCckIsRUFBRSxxQkFBRixFQUF5QnNCLE1BQXpELEVBQWlFO0FBQ2hFQyxpQkFBY0wsUUFBZDtBQUNBTCxPQUFJTyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJHLFNBQXpCLENBQW1DeEIsRUFBRSxrQkFBRixDQUFuQyxFQUEwRCxlQUExRCxFQUEyRSxjQUEzRSxFQUEyRkcsd0JBQTNGO0FBQ0E7QUFDRCxNQUFJYyx1QkFBdUIsQ0FBM0IsRUFBOEI7QUFDN0JNLGlCQUFjTCxRQUFkO0FBQ0E7QUFDRCxFQVJjLEVBUVosR0FSWSxDQURmO0FBVUEsQ0FuQkQiLCJmaWxlIjoiZ2VzY2hhZWZ0c2t1bmRlbnZlcnNhbmQtb3JkZXJkZXRhaWxzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Z2VzY2hhZWZ0c2t1bmRlbnZlcnNhbmQtb3JkZXJkZXRhaWxzLmpzIDIwMTYtMDgtMzFcblx0R2FtYmlvIEdtYkhcblx0aHR0cDovL3d3dy5nYW1iaW8uZGVcblx0Q29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG5cdFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuXHRbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cblx0LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiovXG5cbiQoZnVuY3Rpb24oKSB7XG5cdCQoJ2Rpdi5na3Zfb3JkZXJkZXRhaWxzJykuY2xvc2VzdCgnZGl2LmZyYW1lLXdyYXBwZXInKS5oaWRlKCk7XG5cblx0dmFyIGdrdkJ1dHRvbkRyb3Bkb3duSGFuZGxlciA9IGZ1bmN0aW9uKGV2ZW50KVxuXHR7XG5cdFx0dmFyIG9yZGVySWQgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cygndHInKS5kYXRhKCdyb3ctaWQnKSB8fCAkKCdib2R5JykuZmluZCgnI2dtX29yZGVyX2lkJykudmFsKCk7XG5cdFx0d2luZG93LmxvY2F0aW9uID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1HZXNjaGFlZnRza3VuZGVudmVyc2FuZC9QcmVwYXJlTGFiZWwmb0lEPScgKyBvcmRlcklkO1xuXHR9XG5cblx0dmFyIGludGVydmFsX2NvdW50ZXIgPSAxMCxcblx0ICAgIGludGVydmFsID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuXHRcdGlmIChqc2UubGlicy5idXR0b25fZHJvcGRvd24gJiYgJCgnLmpzLWJ1dHRvbi1kcm9wZG93bicpLmxlbmd0aCkge1xuXHRcdFx0Y2xlYXJJbnRlcnZhbChpbnRlcnZhbCk7XG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24ubWFwQWN0aW9uKCQoJy5ib3R0b20tc2F2ZS1iYXInKSwgJ2drdl9sYWJlbF9nZXQnLCAnYWRtaW5fbGFiZWxzJywgZ2t2QnV0dG9uRHJvcGRvd25IYW5kbGVyKTtcblx0XHR9XG5cdFx0aWYgKGludGVydmFsX2NvdW50ZXItLSA9PT0gMCkge1xuXHRcdFx0Y2xlYXJJbnRlcnZhbChpbnRlcnZhbCk7XG5cdFx0fVxuXHR9LCA0MDApO1xufSk7XG4iXX0=
