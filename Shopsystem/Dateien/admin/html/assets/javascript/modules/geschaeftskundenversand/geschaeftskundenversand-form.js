'use strict';

/* --------------------------------------------------------------
	geschaeftskundenversand-form.js 2016-08-22
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$(function () {
	$('#toggle-extd-view').on('click', function (e) {
		e.preventDefault();
		$('.extd-view').toggle();
	});
	$('.extd-view').hide();

	$('select#gkv_product_index').on('change', function (e) {
		var $option = $('option:selected', this),
		    product = $option.data('dhl_product'),
		    isExport = $option.data('dhl_targetarea') !== 'domestic';

		$('input, select', 'div.dhl_service').attr('disabled', 'disabled');
		$('input[type="checkbox"]', 'div.dhl_service').closest('span.single-checkbox').addClass('disabled');
		$('input, select', 'div.dhl_service.' + product).removeAttr('disabled');
		$('input[type="checkbox"]', 'div.dhl_service.' + product).closest('span.single-checkbox').removeClass('disabled');
		$('div.exportdoc').toggle(isExport);
	});
	$('select#gkv_product_index').change();

	$('select.country').on('change', function (e) {
		var shipper_country = $('select.shipper_country').val(),
		    receiver_country = $('select.receiver_country:visible').val(),
		    isExport = shipper_country !== receiver_country;
		if (shipper_country !== undefined && receiver_country !== undefined) {
			if (isExport === true) {
				// alert('to export ' + shipper_country + ' ' + receiver_country);
				$('select#gkv_product_index').val($('select#gkv_product_index option').not('option[data-dhl_targetarea="domestic"]').first().val());
				$('select#gkv_product_index').trigger('change');
				$('div.exportdoc').show();
			} else {
				// alert('to domestic ' + shipper_country + ' ' + receiver_country);
				$('select#gkv_product_index').val($('select#gkv_product_index option[data-dhl_targetarea="domestic"]').first().val());
				$('select#gkv_product_index').trigger('change');
				$('div.exportdoc').hide();
			}
		}
	});

	$('select#receiver_type').on('change', function (e) {
		var $option = $('option:selected', this),
		    receiver_type = $option.attr('value');
		$('div.receiver_data').each(function () {
			$(this).toggle($(this).hasClass('receiver_' + receiver_type));
		});
		$('select.country').first().trigger('change');
	});
	$('select#receiver_type').trigger('change');

	$('button#customize_shipper').on('click', function (e) {
		e.preventDefault();
		$('div.customize-shipper-button').remove();
		$('div.customize-shipper').css('display', 'inline-block');
	});

	$('button#customize_returnreceiver').on('click', function (e) {
		e.preventDefault();
		$('div.customize-returnreceiver-button').remove();
		$('div.customize-returnreceiver').css('display', 'inline-block');
	});

	$('input[name="identcheck"]').on('change', function (e) {
		$('.dhl_service_identcheck').toggle($(this).get(0).checked);
	});
	$('input[name="identcheck"]').trigger('change');

	/*
 $('select[name="exportdoc/exporttype"]').on('change', function(e) {
 	const exportActivated = $(this).val() !== 'NONE';
 	$('div.exportdoc').not($(this).closest('div.exportdoc')).toggle(exportActivated);
 });
 $('select[name="exportdoc/exporttype"]').trigger('change');
 */

	$('button#add_export_position').on('click', function (e) {
		e.preventDefault();
		var $newrow = $('table.exportpositions tr.template_row').clone(true);
		$newrow.removeClass('template_row');
		$('table.exportpositions tbody').append($newrow);
	});

	$('button.remove_row').on('click', function (e) {
		e.preventDefault();
		$(this).closest('tr').remove();
	});

	$('button#cod_helper').on('click', function (e) {
		e.preventDefault();
		var cod_amount = $(this).data('cod_amount');
		$('input[name="cashondelivery"]').val(cod_amount);
	});
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdlc2NoYWVmdHNrdW5kZW52ZXJzYW5kLWZvcm0uanMiXSwibmFtZXMiOlsiJCIsIm9uIiwiZSIsInByZXZlbnREZWZhdWx0IiwidG9nZ2xlIiwiaGlkZSIsIiRvcHRpb24iLCJwcm9kdWN0IiwiZGF0YSIsImlzRXhwb3J0IiwiYXR0ciIsImNsb3Nlc3QiLCJhZGRDbGFzcyIsInJlbW92ZUF0dHIiLCJyZW1vdmVDbGFzcyIsImNoYW5nZSIsInNoaXBwZXJfY291bnRyeSIsInZhbCIsInJlY2VpdmVyX2NvdW50cnkiLCJ1bmRlZmluZWQiLCJub3QiLCJmaXJzdCIsInRyaWdnZXIiLCJzaG93IiwicmVjZWl2ZXJfdHlwZSIsImVhY2giLCJoYXNDbGFzcyIsInJlbW92ZSIsImNzcyIsImdldCIsImNoZWNrZWQiLCIkbmV3cm93IiwiY2xvbmUiLCJhcHBlbmQiLCJjb2RfYW1vdW50Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLEVBQUUsWUFBVztBQUNaQSxHQUFFLG1CQUFGLEVBQXVCQyxFQUF2QixDQUEwQixPQUExQixFQUFtQyxVQUFTQyxDQUFULEVBQVk7QUFDOUNBLElBQUVDLGNBQUY7QUFDQUgsSUFBRSxZQUFGLEVBQWdCSSxNQUFoQjtBQUNBLEVBSEQ7QUFJQUosR0FBRSxZQUFGLEVBQWdCSyxJQUFoQjs7QUFFQUwsR0FBRSwwQkFBRixFQUE4QkMsRUFBOUIsQ0FBaUMsUUFBakMsRUFBMkMsVUFBU0MsQ0FBVCxFQUFZO0FBQ3RELE1BQU1JLFVBQVVOLEVBQUUsaUJBQUYsRUFBcUIsSUFBckIsQ0FBaEI7QUFBQSxNQUNNTyxVQUFVRCxRQUFRRSxJQUFSLENBQWEsYUFBYixDQURoQjtBQUFBLE1BRU1DLFdBQVdILFFBQVFFLElBQVIsQ0FBYSxnQkFBYixNQUFtQyxVQUZwRDs7QUFJQVIsSUFBRSxlQUFGLEVBQW1CLGlCQUFuQixFQUFzQ1UsSUFBdEMsQ0FBMkMsVUFBM0MsRUFBdUQsVUFBdkQ7QUFDQVYsSUFBRSx3QkFBRixFQUE0QixpQkFBNUIsRUFBK0NXLE9BQS9DLENBQXVELHNCQUF2RCxFQUErRUMsUUFBL0UsQ0FBd0YsVUFBeEY7QUFDQVosSUFBRSxlQUFGLEVBQW1CLHFCQUFxQk8sT0FBeEMsRUFBaURNLFVBQWpELENBQTRELFVBQTVEO0FBQ0FiLElBQUUsd0JBQUYsRUFBNEIscUJBQXFCTyxPQUFqRCxFQUEwREksT0FBMUQsQ0FBa0Usc0JBQWxFLEVBQTBGRyxXQUExRixDQUFzRyxVQUF0RztBQUNBZCxJQUFFLGVBQUYsRUFBbUJJLE1BQW5CLENBQTBCSyxRQUExQjtBQUNBLEVBVkQ7QUFXQVQsR0FBRSwwQkFBRixFQUE4QmUsTUFBOUI7O0FBRUFmLEdBQUUsZ0JBQUYsRUFBb0JDLEVBQXBCLENBQXVCLFFBQXZCLEVBQWlDLFVBQVNDLENBQVQsRUFBWTtBQUM1QyxNQUFNYyxrQkFBbUJoQixFQUFFLHdCQUFGLEVBQTRCaUIsR0FBNUIsRUFBekI7QUFBQSxNQUNNQyxtQkFBbUJsQixFQUFFLGlDQUFGLEVBQXFDaUIsR0FBckMsRUFEekI7QUFBQSxNQUVNUixXQUFtQk8sb0JBQW9CRSxnQkFGN0M7QUFHQSxNQUFHRixvQkFBb0JHLFNBQXBCLElBQWlDRCxxQkFBcUJDLFNBQXpELEVBQ0E7QUFDQyxPQUFHVixhQUFhLElBQWhCLEVBQ0E7QUFDQztBQUNBVCxNQUFFLDBCQUFGLEVBQThCaUIsR0FBOUIsQ0FBa0NqQixFQUFFLGlDQUFGLEVBQXFDb0IsR0FBckMsQ0FBeUMsd0NBQXpDLEVBQW1GQyxLQUFuRixHQUEyRkosR0FBM0YsRUFBbEM7QUFDQWpCLE1BQUUsMEJBQUYsRUFBOEJzQixPQUE5QixDQUFzQyxRQUF0QztBQUNBdEIsTUFBRSxlQUFGLEVBQW1CdUIsSUFBbkI7QUFDQSxJQU5ELE1BUUE7QUFDQztBQUNBdkIsTUFBRSwwQkFBRixFQUE4QmlCLEdBQTlCLENBQWtDakIsRUFBRSxpRUFBRixFQUFxRXFCLEtBQXJFLEdBQTZFSixHQUE3RSxFQUFsQztBQUNBakIsTUFBRSwwQkFBRixFQUE4QnNCLE9BQTlCLENBQXNDLFFBQXRDO0FBQ0F0QixNQUFFLGVBQUYsRUFBbUJLLElBQW5CO0FBQ0E7QUFDRDtBQUNELEVBckJEOztBQXVCQUwsR0FBRSxzQkFBRixFQUEwQkMsRUFBMUIsQ0FBNkIsUUFBN0IsRUFBdUMsVUFBU0MsQ0FBVCxFQUFZO0FBQ2xELE1BQU1JLFVBQVVOLEVBQUUsaUJBQUYsRUFBcUIsSUFBckIsQ0FBaEI7QUFBQSxNQUNNd0IsZ0JBQWdCbEIsUUFBUUksSUFBUixDQUFhLE9BQWIsQ0FEdEI7QUFFQVYsSUFBRSxtQkFBRixFQUF1QnlCLElBQXZCLENBQTRCLFlBQVc7QUFDdEN6QixLQUFFLElBQUYsRUFBUUksTUFBUixDQUFlSixFQUFFLElBQUYsRUFBUTBCLFFBQVIsQ0FBaUIsY0FBY0YsYUFBL0IsQ0FBZjtBQUNBLEdBRkQ7QUFHQXhCLElBQUUsZ0JBQUYsRUFBb0JxQixLQUFwQixHQUE0QkMsT0FBNUIsQ0FBb0MsUUFBcEM7QUFDQSxFQVBEO0FBUUF0QixHQUFFLHNCQUFGLEVBQTBCc0IsT0FBMUIsQ0FBa0MsUUFBbEM7O0FBRUF0QixHQUFFLDBCQUFGLEVBQThCQyxFQUE5QixDQUFpQyxPQUFqQyxFQUEwQyxVQUFTQyxDQUFULEVBQVk7QUFDckRBLElBQUVDLGNBQUY7QUFDQUgsSUFBRSw4QkFBRixFQUFrQzJCLE1BQWxDO0FBQ0EzQixJQUFFLHVCQUFGLEVBQTJCNEIsR0FBM0IsQ0FBK0IsU0FBL0IsRUFBMEMsY0FBMUM7QUFDQSxFQUpEOztBQU1BNUIsR0FBRSxpQ0FBRixFQUFxQ0MsRUFBckMsQ0FBd0MsT0FBeEMsRUFBaUQsVUFBU0MsQ0FBVCxFQUFZO0FBQzVEQSxJQUFFQyxjQUFGO0FBQ0FILElBQUUscUNBQUYsRUFBeUMyQixNQUF6QztBQUNBM0IsSUFBRSw4QkFBRixFQUFrQzRCLEdBQWxDLENBQXNDLFNBQXRDLEVBQWlELGNBQWpEO0FBQ0EsRUFKRDs7QUFNQTVCLEdBQUUsMEJBQUYsRUFBOEJDLEVBQTlCLENBQWlDLFFBQWpDLEVBQTJDLFVBQVNDLENBQVQsRUFBWTtBQUN0REYsSUFBRSx5QkFBRixFQUE2QkksTUFBN0IsQ0FBb0NKLEVBQUUsSUFBRixFQUFRNkIsR0FBUixDQUFZLENBQVosRUFBZUMsT0FBbkQ7QUFDQSxFQUZEO0FBR0E5QixHQUFFLDBCQUFGLEVBQThCc0IsT0FBOUIsQ0FBc0MsUUFBdEM7O0FBRUE7Ozs7Ozs7O0FBUUF0QixHQUFFLDRCQUFGLEVBQWdDQyxFQUFoQyxDQUFtQyxPQUFuQyxFQUE0QyxVQUFTQyxDQUFULEVBQVk7QUFDdkRBLElBQUVDLGNBQUY7QUFDQSxNQUFJNEIsVUFBVS9CLEVBQUUsdUNBQUYsRUFBMkNnQyxLQUEzQyxDQUFpRCxJQUFqRCxDQUFkO0FBQ0FELFVBQVFqQixXQUFSLENBQW9CLGNBQXBCO0FBQ0FkLElBQUUsNkJBQUYsRUFBaUNpQyxNQUFqQyxDQUF3Q0YsT0FBeEM7QUFDQSxFQUxEOztBQU9BL0IsR0FBRSxtQkFBRixFQUF1QkMsRUFBdkIsQ0FBMEIsT0FBMUIsRUFBbUMsVUFBU0MsQ0FBVCxFQUFZO0FBQzlDQSxJQUFFQyxjQUFGO0FBQ0FILElBQUUsSUFBRixFQUFRVyxPQUFSLENBQWdCLElBQWhCLEVBQXNCZ0IsTUFBdEI7QUFDQSxFQUhEOztBQUtBM0IsR0FBRSxtQkFBRixFQUF1QkMsRUFBdkIsQ0FBMEIsT0FBMUIsRUFBbUMsVUFBU0MsQ0FBVCxFQUFZO0FBQzlDQSxJQUFFQyxjQUFGO0FBQ0EsTUFBSStCLGFBQWFsQyxFQUFFLElBQUYsRUFBUVEsSUFBUixDQUFhLFlBQWIsQ0FBakI7QUFDQVIsSUFBRSw4QkFBRixFQUFrQ2lCLEdBQWxDLENBQXNDaUIsVUFBdEM7QUFDQSxFQUpEO0FBTUEsQ0FoR0QiLCJmaWxlIjoiZ2VzY2hhZWZ0c2t1bmRlbnZlcnNhbmQtZm9ybS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdGdlc2NoYWVmdHNrdW5kZW52ZXJzYW5kLWZvcm0uanMgMjAxNi0wOC0yMlxuXHRHYW1iaW8gR21iSFxuXHRodHRwOi8vd3d3LmdhbWJpby5kZVxuXHRDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcblx0UmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG5cdFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuXHQtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuKi9cblxuJChmdW5jdGlvbigpIHtcblx0JCgnI3RvZ2dsZS1leHRkLXZpZXcnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdCQoJy5leHRkLXZpZXcnKS50b2dnbGUoKTtcblx0fSk7XG5cdCQoJy5leHRkLXZpZXcnKS5oaWRlKCk7XG5cblx0JCgnc2VsZWN0I2drdl9wcm9kdWN0X2luZGV4Jykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKGUpIHtcblx0XHRjb25zdCAkb3B0aW9uID0gJCgnb3B0aW9uOnNlbGVjdGVkJywgdGhpcyksXG5cdFx0ICAgICAgcHJvZHVjdCA9ICRvcHRpb24uZGF0YSgnZGhsX3Byb2R1Y3QnKSxcblx0XHQgICAgICBpc0V4cG9ydCA9ICRvcHRpb24uZGF0YSgnZGhsX3RhcmdldGFyZWEnKSAhPT0gJ2RvbWVzdGljJztcblxuXHRcdCQoJ2lucHV0LCBzZWxlY3QnLCAnZGl2LmRobF9zZXJ2aWNlJykuYXR0cignZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcblx0XHQkKCdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0nLCAnZGl2LmRobF9zZXJ2aWNlJykuY2xvc2VzdCgnc3Bhbi5zaW5nbGUtY2hlY2tib3gnKS5hZGRDbGFzcygnZGlzYWJsZWQnKTtcblx0XHQkKCdpbnB1dCwgc2VsZWN0JywgJ2Rpdi5kaGxfc2VydmljZS4nICsgcHJvZHVjdCkucmVtb3ZlQXR0cignZGlzYWJsZWQnKTtcblx0XHQkKCdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0nLCAnZGl2LmRobF9zZXJ2aWNlLicgKyBwcm9kdWN0KS5jbG9zZXN0KCdzcGFuLnNpbmdsZS1jaGVja2JveCcpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuXHRcdCQoJ2Rpdi5leHBvcnRkb2MnKS50b2dnbGUoaXNFeHBvcnQpO1xuXHR9KTtcblx0JCgnc2VsZWN0I2drdl9wcm9kdWN0X2luZGV4JykuY2hhbmdlKCk7XG5cblx0JCgnc2VsZWN0LmNvdW50cnknKS5vbignY2hhbmdlJywgZnVuY3Rpb24oZSkge1xuXHRcdGNvbnN0IHNoaXBwZXJfY291bnRyeSAgPSAkKCdzZWxlY3Quc2hpcHBlcl9jb3VudHJ5JykudmFsKCksXG5cdFx0ICAgICAgcmVjZWl2ZXJfY291bnRyeSA9ICQoJ3NlbGVjdC5yZWNlaXZlcl9jb3VudHJ5OnZpc2libGUnKS52YWwoKSxcblx0XHQgICAgICBpc0V4cG9ydCAgICAgICAgID0gc2hpcHBlcl9jb3VudHJ5ICE9PSByZWNlaXZlcl9jb3VudHJ5O1xuXHRcdGlmKHNoaXBwZXJfY291bnRyeSAhPT0gdW5kZWZpbmVkICYmIHJlY2VpdmVyX2NvdW50cnkgIT09IHVuZGVmaW5lZClcblx0XHR7XG5cdFx0XHRpZihpc0V4cG9ydCA9PT0gdHJ1ZSlcblx0XHRcdHtcblx0XHRcdFx0Ly8gYWxlcnQoJ3RvIGV4cG9ydCAnICsgc2hpcHBlcl9jb3VudHJ5ICsgJyAnICsgcmVjZWl2ZXJfY291bnRyeSk7XG5cdFx0XHRcdCQoJ3NlbGVjdCNna3ZfcHJvZHVjdF9pbmRleCcpLnZhbCgkKCdzZWxlY3QjZ2t2X3Byb2R1Y3RfaW5kZXggb3B0aW9uJykubm90KCdvcHRpb25bZGF0YS1kaGxfdGFyZ2V0YXJlYT1cImRvbWVzdGljXCJdJykuZmlyc3QoKS52YWwoKSk7XG5cdFx0XHRcdCQoJ3NlbGVjdCNna3ZfcHJvZHVjdF9pbmRleCcpLnRyaWdnZXIoJ2NoYW5nZScpO1xuXHRcdFx0XHQkKCdkaXYuZXhwb3J0ZG9jJykuc2hvdygpO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZVxuXHRcdFx0e1xuXHRcdFx0XHQvLyBhbGVydCgndG8gZG9tZXN0aWMgJyArIHNoaXBwZXJfY291bnRyeSArICcgJyArIHJlY2VpdmVyX2NvdW50cnkpO1xuXHRcdFx0XHQkKCdzZWxlY3QjZ2t2X3Byb2R1Y3RfaW5kZXgnKS52YWwoJCgnc2VsZWN0I2drdl9wcm9kdWN0X2luZGV4IG9wdGlvbltkYXRhLWRobF90YXJnZXRhcmVhPVwiZG9tZXN0aWNcIl0nKS5maXJzdCgpLnZhbCgpKTtcblx0XHRcdFx0JCgnc2VsZWN0I2drdl9wcm9kdWN0X2luZGV4JykudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0XHRcdCQoJ2Rpdi5leHBvcnRkb2MnKS5oaWRlKCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9KTtcblxuXHQkKCdzZWxlY3QjcmVjZWl2ZXJfdHlwZScpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbihlKSB7XG5cdFx0Y29uc3QgJG9wdGlvbiA9ICQoJ29wdGlvbjpzZWxlY3RlZCcsIHRoaXMpLFxuXHRcdCAgICAgIHJlY2VpdmVyX3R5cGUgPSAkb3B0aW9uLmF0dHIoJ3ZhbHVlJyk7XG5cdFx0JCgnZGl2LnJlY2VpdmVyX2RhdGEnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0JCh0aGlzKS50b2dnbGUoJCh0aGlzKS5oYXNDbGFzcygncmVjZWl2ZXJfJyArIHJlY2VpdmVyX3R5cGUpKTtcblx0XHR9KTtcblx0XHQkKCdzZWxlY3QuY291bnRyeScpLmZpcnN0KCkudHJpZ2dlcignY2hhbmdlJyk7XG5cdH0pO1xuXHQkKCdzZWxlY3QjcmVjZWl2ZXJfdHlwZScpLnRyaWdnZXIoJ2NoYW5nZScpO1xuXG5cdCQoJ2J1dHRvbiNjdXN0b21pemVfc2hpcHBlcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0JCgnZGl2LmN1c3RvbWl6ZS1zaGlwcGVyLWJ1dHRvbicpLnJlbW92ZSgpO1xuXHRcdCQoJ2Rpdi5jdXN0b21pemUtc2hpcHBlcicpLmNzcygnZGlzcGxheScsICdpbmxpbmUtYmxvY2snKTtcblx0fSk7XG5cblx0JCgnYnV0dG9uI2N1c3RvbWl6ZV9yZXR1cm5yZWNlaXZlcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0JCgnZGl2LmN1c3RvbWl6ZS1yZXR1cm5yZWNlaXZlci1idXR0b24nKS5yZW1vdmUoKTtcblx0XHQkKCdkaXYuY3VzdG9taXplLXJldHVybnJlY2VpdmVyJykuY3NzKCdkaXNwbGF5JywgJ2lubGluZS1ibG9jaycpO1xuXHR9KTtcblxuXHQkKCdpbnB1dFtuYW1lPVwiaWRlbnRjaGVja1wiXScpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbihlKSB7XG5cdFx0JCgnLmRobF9zZXJ2aWNlX2lkZW50Y2hlY2snKS50b2dnbGUoJCh0aGlzKS5nZXQoMCkuY2hlY2tlZCk7XG5cdH0pO1xuXHQkKCdpbnB1dFtuYW1lPVwiaWRlbnRjaGVja1wiXScpLnRyaWdnZXIoJ2NoYW5nZScpO1xuXG5cdC8qXG5cdCQoJ3NlbGVjdFtuYW1lPVwiZXhwb3J0ZG9jL2V4cG9ydHR5cGVcIl0nKS5vbignY2hhbmdlJywgZnVuY3Rpb24oZSkge1xuXHRcdGNvbnN0IGV4cG9ydEFjdGl2YXRlZCA9ICQodGhpcykudmFsKCkgIT09ICdOT05FJztcblx0XHQkKCdkaXYuZXhwb3J0ZG9jJykubm90KCQodGhpcykuY2xvc2VzdCgnZGl2LmV4cG9ydGRvYycpKS50b2dnbGUoZXhwb3J0QWN0aXZhdGVkKTtcblx0fSk7XG5cdCQoJ3NlbGVjdFtuYW1lPVwiZXhwb3J0ZG9jL2V4cG9ydHR5cGVcIl0nKS50cmlnZ2VyKCdjaGFuZ2UnKTtcblx0Ki9cblxuXHQkKCdidXR0b24jYWRkX2V4cG9ydF9wb3NpdGlvbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0bGV0ICRuZXdyb3cgPSAkKCd0YWJsZS5leHBvcnRwb3NpdGlvbnMgdHIudGVtcGxhdGVfcm93JykuY2xvbmUodHJ1ZSk7XG5cdFx0JG5ld3Jvdy5yZW1vdmVDbGFzcygndGVtcGxhdGVfcm93Jyk7XG5cdFx0JCgndGFibGUuZXhwb3J0cG9zaXRpb25zIHRib2R5JykuYXBwZW5kKCRuZXdyb3cpO1xuXHR9KTtcblxuXHQkKCdidXR0b24ucmVtb3ZlX3JvdycpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0JCh0aGlzKS5jbG9zZXN0KCd0cicpLnJlbW92ZSgpO1xuXHR9KTtcblxuXHQkKCdidXR0b24jY29kX2hlbHBlcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0bGV0IGNvZF9hbW91bnQgPSAkKHRoaXMpLmRhdGEoJ2NvZF9hbW91bnQnKTtcblx0XHQkKCdpbnB1dFtuYW1lPVwiY2FzaG9uZGVsaXZlcnlcIl0nKS52YWwoY29kX2Ftb3VudCk7XG5cdH0pXG5cdFxufSk7XG4iXX0=
