'use strict';

/* --------------------------------------------------------------
 initialize.js 2018-04-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Admin Layout Initialization Controller
 *
 * This controller will handle the initialization of the admin pages. Bind this controller
 * in the body element of the page.
 */
gx.controllers.module('initialize', ['hooks'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$('body').on('JSENGINE_INIT_FINISHED', function () {
			$this.fadeIn(200, function () {
				$this.removeClass('page-loading');
			});
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxheW91dHMvbWFpbi9pbml0aWFsaXplLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiaW5pdCIsImRvbmUiLCJvbiIsImZhZGVJbiIsInJlbW92ZUNsYXNzIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7OztBQU1BQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0IsWUFBdEIsRUFBb0MsQ0FBQyxPQUFELENBQXBDLEVBQStDLFVBQVNDLElBQVQsRUFBZTs7QUFFN0Q7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBQSxRQUFPSSxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCRixJQUFFLE1BQUYsRUFBVUcsRUFBVixDQUFhLHdCQUFiLEVBQXVDLFlBQU07QUFDNUNKLFNBQU1LLE1BQU4sQ0FBYSxHQUFiLEVBQWtCLFlBQU07QUFDdkJMLFVBQU1NLFdBQU4sQ0FBa0IsY0FBbEI7QUFDQSxJQUZEO0FBR0EsR0FKRDs7QUFNQUg7QUFDQSxFQVJEOztBQVVBLFFBQU9MLE1BQVA7QUFFQSxDQXRDRCIsImZpbGUiOiJsYXlvdXRzL21haW4vaW5pdGlhbGl6ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBpbml0aWFsaXplLmpzIDIwMTgtMDQtMTJcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogQWRtaW4gTGF5b3V0IEluaXRpYWxpemF0aW9uIENvbnRyb2xsZXJcclxuICpcclxuICogVGhpcyBjb250cm9sbGVyIHdpbGwgaGFuZGxlIHRoZSBpbml0aWFsaXphdGlvbiBvZiB0aGUgYWRtaW4gcGFnZXMuIEJpbmQgdGhpcyBjb250cm9sbGVyXHJcbiAqIGluIHRoZSBib2R5IGVsZW1lbnQgb2YgdGhlIHBhZ2UuXHJcbiAqL1xyXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ2luaXRpYWxpemUnLCBbJ2hvb2tzJ10sIGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gVkFSSUFCTEVTXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdCAqL1xyXG5cdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0ICovXHJcblx0Y29uc3QgbW9kdWxlID0ge307XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gSU5JVElBTElaQVRJT05cclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdCQoJ2JvZHknKS5vbignSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsICgpID0+IHtcclxuXHRcdFx0JHRoaXMuZmFkZUluKDIwMCwgKCkgPT4ge1xyXG5cdFx0XHRcdCR0aGlzLnJlbW92ZUNsYXNzKCdwYWdlLWxvYWRpbmcnKTtcclxuXHRcdFx0fSk7XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0ZG9uZSgpO1xyXG5cdH07XHJcblx0XHJcblx0cmV0dXJuIG1vZHVsZTtcclxuXHRcclxufSk7Il19
