'use strict';

/* --------------------------------------------------------------
 delete.js 2016-09-30
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Delete Invoice Modal Controller
 */
gx.controllers.module('delete', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: {
			selectedInvoices: $this.find('.selected-invoice-ids')
		}
	};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Send the modal data to the form through an AJAX call.
  */
	function _onDeleteClick() {
		var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=InvoicesModalsAjax/DeleteInvoice';
		var data = {
			selectedInvoices: module.bindings.selectedInvoices.get().split(','),
			pageToken: jse.core.config.get('pageToken')
		};
		var $deleteButton = $(this);

		$deleteButton.addClass('disabled').prop('disabled', true);

		$.ajax({
			url: url,
			data: data,
			method: 'POST',
			dataType: 'json'
		}).done(function (response) {
			jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('DELETE_INVOICES_SUCCESS', 'admin_invoices'));
			$('.invoices .table-main').DataTable().ajax.reload(null, false);
			$('.invoices .table-main').invoices_overview_filter('reload');
		}).fail(function (jqxhr, textStatus, errorThrown) {
			jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'), jse.core.lang.translate('DELETE_INVOICES_ERROR', 'admin_invoices'));
		}).always(function () {
			$this.modal('hide');
			$deleteButton.removeClass('disabled').prop('disabled', false);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.delete', _onDeleteClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL21vZGFscy9kZWxldGUuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJiaW5kaW5ncyIsInNlbGVjdGVkSW52b2ljZXMiLCJmaW5kIiwiX29uRGVsZXRlQ2xpY2siLCJ1cmwiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0Iiwic3BsaXQiLCJwYWdlVG9rZW4iLCIkZGVsZXRlQnV0dG9uIiwiYWRkQ2xhc3MiLCJwcm9wIiwiYWpheCIsIm1ldGhvZCIsImRhdGFUeXBlIiwiZG9uZSIsInJlc3BvbnNlIiwibGlicyIsImluZm9fYm94IiwiYWRkU3VjY2Vzc01lc3NhZ2UiLCJsYW5nIiwidHJhbnNsYXRlIiwiRGF0YVRhYmxlIiwicmVsb2FkIiwiaW52b2ljZXNfb3ZlcnZpZXdfZmlsdGVyIiwiZmFpbCIsImpxeGhyIiwidGV4dFN0YXR1cyIsImVycm9yVGhyb3duIiwibW9kYWwiLCJzaG93TWVzc2FnZSIsImFsd2F5cyIsInJlbW92ZUNsYXNzIiwiaW5pdCIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7OztBQUdBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0IsUUFBdEIsRUFBZ0MsQ0FBQyxPQUFELENBQWhDLEVBQTJDLFVBQVNDLElBQVQsRUFBZTs7QUFFekQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTO0FBQ2RJLFlBQVU7QUFDVEMscUJBQWtCSCxNQUFNSSxJQUFOLENBQVcsdUJBQVg7QUFEVDtBQURJLEVBQWY7O0FBTUE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxVQUFTQyxjQUFULEdBQTBCO0FBQ3pCLE1BQU1DLE1BQU1DLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0Msc0RBQTVDO0FBQ0EsTUFBTVgsT0FBTztBQUNaSSxxQkFBa0JMLE9BQU9JLFFBQVAsQ0FBZ0JDLGdCQUFoQixDQUFpQ08sR0FBakMsR0FBdUNDLEtBQXZDLENBQTZDLEdBQTdDLENBRE47QUFFWkMsY0FBV0wsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUZDLEdBQWI7QUFJQSxNQUFNRyxnQkFBZ0JaLEVBQUUsSUFBRixDQUF0Qjs7QUFFQVksZ0JBQWNDLFFBQWQsQ0FBdUIsVUFBdkIsRUFBbUNDLElBQW5DLENBQXdDLFVBQXhDLEVBQW9ELElBQXBEOztBQUVBZCxJQUFFZSxJQUFGLENBQU87QUFDTlYsV0FETTtBQUVOUCxhQUZNO0FBR05rQixXQUFRLE1BSEY7QUFJTkMsYUFBVTtBQUpKLEdBQVAsRUFNRUMsSUFORixDQU1PLFVBQVNDLFFBQVQsRUFBbUI7QUFDeEJiLE9BQUljLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsaUJBQWxCLENBQ0NoQixJQUFJQyxJQUFKLENBQVNnQixJQUFULENBQWNDLFNBQWQsQ0FBd0IseUJBQXhCLEVBQW1ELGdCQUFuRCxDQUREO0FBRUF4QixLQUFFLHVCQUFGLEVBQTJCeUIsU0FBM0IsR0FBdUNWLElBQXZDLENBQTRDVyxNQUE1QyxDQUFtRCxJQUFuRCxFQUF5RCxLQUF6RDtBQUNBMUIsS0FBRSx1QkFBRixFQUEyQjJCLHdCQUEzQixDQUFvRCxRQUFwRDtBQUNBLEdBWEYsRUFZRUMsSUFaRixDQVlPLFVBQVNDLEtBQVQsRUFBZ0JDLFVBQWhCLEVBQTRCQyxXQUE1QixFQUF5QztBQUM5Q3pCLE9BQUljLElBQUosQ0FBU1ksS0FBVCxDQUFlQyxXQUFmLENBQTJCM0IsSUFBSUMsSUFBSixDQUFTZ0IsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBQTNCLEVBQ0NsQixJQUFJQyxJQUFKLENBQVNnQixJQUFULENBQWNDLFNBQWQsQ0FBd0IsdUJBQXhCLEVBQWlELGdCQUFqRCxDQUREO0FBRUEsR0FmRixFQWdCRVUsTUFoQkYsQ0FnQlMsWUFBVztBQUNsQm5DLFNBQU1pQyxLQUFOLENBQVksTUFBWjtBQUNBcEIsaUJBQWN1QixXQUFkLENBQTBCLFVBQTFCLEVBQXNDckIsSUFBdEMsQ0FBMkMsVUFBM0MsRUFBdUQsS0FBdkQ7QUFDQSxHQW5CRjtBQW9CQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUFqQixRQUFPdUMsSUFBUCxHQUFjLFVBQVNsQixJQUFULEVBQWU7QUFDNUJuQixRQUFNc0MsRUFBTixDQUFTLE9BQVQsRUFBa0IsYUFBbEIsRUFBaUNqQyxjQUFqQztBQUNBYztBQUNBLEVBSEQ7O0FBS0EsUUFBT3JCLE1BQVA7QUFDQSxDQTNFRCIsImZpbGUiOiJpbnZvaWNlcy9tb2RhbHMvZGVsZXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGRlbGV0ZS5qcyAyMDE2LTA5LTMwXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIERlbGV0ZSBJbnZvaWNlIE1vZGFsIENvbnRyb2xsZXJcclxuICovXHJcbmd4LmNvbnRyb2xsZXJzLm1vZHVsZSgnZGVsZXRlJywgWydtb2RhbCddLCBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIFZBUklBQkxFU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdCAqXHJcblx0ICogQHR5cGUge2pRdWVyeX1cclxuXHQgKi9cclxuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIEluc3RhbmNlXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG1vZHVsZSA9IHtcclxuXHRcdGJpbmRpbmdzOiB7XHJcblx0XHRcdHNlbGVjdGVkSW52b2ljZXM6ICR0aGlzLmZpbmQoJy5zZWxlY3RlZC1pbnZvaWNlLWlkcycpXHJcblx0XHR9XHJcblx0fTtcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBGVU5DVElPTlNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBTZW5kIHRoZSBtb2RhbCBkYXRhIHRvIHRoZSBmb3JtIHRocm91Z2ggYW4gQUpBWCBjYWxsLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vbkRlbGV0ZUNsaWNrKCkge1xyXG5cdFx0Y29uc3QgdXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1JbnZvaWNlc01vZGFsc0FqYXgvRGVsZXRlSW52b2ljZSc7XHJcblx0XHRjb25zdCBkYXRhID0ge1xyXG5cdFx0XHRzZWxlY3RlZEludm9pY2VzOiBtb2R1bGUuYmluZGluZ3Muc2VsZWN0ZWRJbnZvaWNlcy5nZXQoKS5zcGxpdCgnLCcpLFxyXG5cdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpXHJcblx0XHR9O1xyXG5cdFx0Y29uc3QgJGRlbGV0ZUJ1dHRvbiA9ICQodGhpcyk7XHJcblx0XHRcclxuXHRcdCRkZWxldGVCdXR0b24uYWRkQ2xhc3MoJ2Rpc2FibGVkJykucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcclxuXHRcdFxyXG5cdFx0JC5hamF4KHtcclxuXHRcdFx0dXJsLFxyXG5cdFx0XHRkYXRhLFxyXG5cdFx0XHRtZXRob2Q6ICdQT1NUJyxcclxuXHRcdFx0ZGF0YVR5cGU6ICdqc29uJ1xyXG5cdFx0fSlcclxuXHRcdFx0LmRvbmUoZnVuY3Rpb24ocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRqc2UubGlicy5pbmZvX2JveC5hZGRTdWNjZXNzTWVzc2FnZShcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdERUxFVEVfSU5WT0lDRVNfU1VDQ0VTUycsICdhZG1pbl9pbnZvaWNlcycpKTtcclxuXHRcdFx0XHQkKCcuaW52b2ljZXMgLnRhYmxlLW1haW4nKS5EYXRhVGFibGUoKS5hamF4LnJlbG9hZChudWxsLCBmYWxzZSk7XHJcblx0XHRcdFx0JCgnLmludm9pY2VzIC50YWJsZS1tYWluJykuaW52b2ljZXNfb3ZlcnZpZXdfZmlsdGVyKCdyZWxvYWQnKTtcclxuXHRcdFx0fSlcclxuXHRcdFx0LmZhaWwoZnVuY3Rpb24oanF4aHIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XHJcblx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyksXHJcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnREVMRVRFX0lOVk9JQ0VTX0VSUk9SJywgJ2FkbWluX2ludm9pY2VzJykpO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQuYWx3YXlzKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdCR0aGlzLm1vZGFsKCdoaWRlJyk7XHJcblx0XHRcdFx0JGRlbGV0ZUJ1dHRvbi5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcclxuXHRcdFx0fSk7XHJcblx0fVxyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHQkdGhpcy5vbignY2xpY2snLCAnLmJ0bi5kZWxldGUnLCBfb25EZWxldGVDbGljayk7XHJcblx0XHRkb25lKCk7XHJcblx0fTtcclxuXHRcclxuXHRyZXR1cm4gbW9kdWxlO1xyXG59KTsiXX0=
