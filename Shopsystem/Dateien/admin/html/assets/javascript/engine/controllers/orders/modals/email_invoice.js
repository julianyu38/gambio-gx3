'use strict';

/* --------------------------------------------------------------
 email_invoice.js 2016-10-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Email Invoice Modal Controller
 *
 * Handles the functionality of the Email Invoice modal.
 */
gx.controllers.module('email_invoice', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Send the modal data to the form through an AJAX call.
  *
  * @param {jQuery.Event} event
  */
	function _onSendClick(event) {
		var getParams = {
			oID: $this.data('orderId'),
			type: 'invoice',
			mail: '1',
			gm_quick_mail: '1'
		};
		var url = jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php?' + $.param(getParams);
		var data = $this.find('form').serialize();
		var $sendButton = $(event.target);

		$sendButton.addClass('disabled').prop('disabled', true);

		$.ajax({
			url: url,
			data: data,
			method: 'POST'
		}).done(function (response) {
			var message = jse.core.lang.translate('MAIL_SUCCESS', 'gm_send_order');
			$('.orders .table-main').DataTable().ajax.reload(null, false);
			$('.orders .table-main').orders_overview_filter('reload');

			// Show success message in the admin info box.
			jse.libs.info_box.addSuccessMessage(message);
		}).fail(function (jqxhr, textStatus, errorThrown) {
			var title = jse.core.lang.translate('error', 'messages');
			var message = jse.core.lang.translate('MAIL_UNSUCCESS', 'gm_send_order');
			jse.libs.modal.showMessage(title, message);
		}).always(function () {
			$this.modal('hide');
			$sendButton.removeClass('disabled').prop('disabled', false);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvZW1haWxfaW52b2ljZS5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIl9vblNlbmRDbGljayIsImV2ZW50IiwiZ2V0UGFyYW1zIiwib0lEIiwidHlwZSIsIm1haWwiLCJnbV9xdWlja19tYWlsIiwidXJsIiwianNlIiwiY29yZSIsImNvbmZpZyIsImdldCIsInBhcmFtIiwiZmluZCIsInNlcmlhbGl6ZSIsIiRzZW5kQnV0dG9uIiwidGFyZ2V0IiwiYWRkQ2xhc3MiLCJwcm9wIiwiYWpheCIsIm1ldGhvZCIsImRvbmUiLCJyZXNwb25zZSIsIm1lc3NhZ2UiLCJsYW5nIiwidHJhbnNsYXRlIiwiRGF0YVRhYmxlIiwicmVsb2FkIiwib3JkZXJzX292ZXJ2aWV3X2ZpbHRlciIsImxpYnMiLCJpbmZvX2JveCIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwiZmFpbCIsImpxeGhyIiwidGV4dFN0YXR1cyIsImVycm9yVGhyb3duIiwidGl0bGUiLCJtb2RhbCIsInNob3dNZXNzYWdlIiwiYWx3YXlzIiwicmVtb3ZlQ2xhc3MiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQXNCLGVBQXRCLEVBQXVDLENBQUMsT0FBRCxDQUF2QyxFQUFrRCxVQUFTQyxJQUFULEVBQWU7O0FBRWhFOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUgsU0FBUyxFQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxVQUFTSSxZQUFULENBQXNCQyxLQUF0QixFQUE2QjtBQUM1QixNQUFNQyxZQUFZO0FBQ2pCQyxRQUFLTCxNQUFNRCxJQUFOLENBQVcsU0FBWCxDQURZO0FBRWpCTyxTQUFNLFNBRlc7QUFHakJDLFNBQU0sR0FIVztBQUlqQkMsa0JBQWU7QUFKRSxHQUFsQjtBQU1BLE1BQU1DLE1BQU1DLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsMEJBQWhDLEdBQTZEWixFQUFFYSxLQUFGLENBQVFWLFNBQVIsQ0FBekU7QUFDQSxNQUFNTCxPQUFPQyxNQUFNZSxJQUFOLENBQVcsTUFBWCxFQUFtQkMsU0FBbkIsRUFBYjtBQUNBLE1BQU1DLGNBQWNoQixFQUFFRSxNQUFNZSxNQUFSLENBQXBCOztBQUVBRCxjQUFZRSxRQUFaLENBQXFCLFVBQXJCLEVBQWlDQyxJQUFqQyxDQUFzQyxVQUF0QyxFQUFrRCxJQUFsRDs7QUFFQW5CLElBQUVvQixJQUFGLENBQU87QUFDTlosV0FETTtBQUVOVixhQUZNO0FBR051QixXQUFRO0FBSEYsR0FBUCxFQUtFQyxJQUxGLENBS08sVUFBU0MsUUFBVCxFQUFtQjtBQUN4QixPQUFNQyxVQUFVZixJQUFJQyxJQUFKLENBQVNlLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxlQUF4QyxDQUFoQjtBQUNBMUIsS0FBRSxxQkFBRixFQUF5QjJCLFNBQXpCLEdBQXFDUCxJQUFyQyxDQUEwQ1EsTUFBMUMsQ0FBaUQsSUFBakQsRUFBdUQsS0FBdkQ7QUFDQTVCLEtBQUUscUJBQUYsRUFBeUI2QixzQkFBekIsQ0FBZ0QsUUFBaEQ7O0FBRUE7QUFDQXBCLE9BQUlxQixJQUFKLENBQVNDLFFBQVQsQ0FBa0JDLGlCQUFsQixDQUFvQ1IsT0FBcEM7QUFDQSxHQVpGLEVBYUVTLElBYkYsQ0FhTyxVQUFTQyxLQUFULEVBQWdCQyxVQUFoQixFQUE0QkMsV0FBNUIsRUFBeUM7QUFDOUMsT0FBTUMsUUFBUTVCLElBQUlDLElBQUosQ0FBU2UsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBQWQ7QUFDQSxPQUFNRixVQUFVZixJQUFJQyxJQUFKLENBQVNlLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixnQkFBeEIsRUFBMEMsZUFBMUMsQ0FBaEI7QUFDQWpCLE9BQUlxQixJQUFKLENBQVNRLEtBQVQsQ0FBZUMsV0FBZixDQUEyQkYsS0FBM0IsRUFBa0NiLE9BQWxDO0FBQ0EsR0FqQkYsRUFrQkVnQixNQWxCRixDQWtCUyxZQUFXO0FBQ2xCekMsU0FBTXVDLEtBQU4sQ0FBWSxNQUFaO0FBQ0F0QixlQUFZeUIsV0FBWixDQUF3QixVQUF4QixFQUFvQ3RCLElBQXBDLENBQXlDLFVBQXpDLEVBQXFELEtBQXJEO0FBQ0EsR0FyQkY7QUFzQkE7O0FBRUQ7QUFDQTtBQUNBOztBQUVBdEIsUUFBTzZDLElBQVAsR0FBYyxVQUFTcEIsSUFBVCxFQUFlO0FBQzVCdkIsUUFBTTRDLEVBQU4sQ0FBUyxPQUFULEVBQWtCLFdBQWxCLEVBQStCMUMsWUFBL0I7QUFDQXFCO0FBQ0EsRUFIRDs7QUFLQSxRQUFPekIsTUFBUDtBQUNBLENBOUVEIiwiZmlsZSI6Im9yZGVycy9tb2RhbHMvZW1haWxfaW52b2ljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZW1haWxfaW52b2ljZS5qcyAyMDE2LTEwLTE3XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBFbWFpbCBJbnZvaWNlIE1vZGFsIENvbnRyb2xsZXJcbiAqXG4gKiBIYW5kbGVzIHRoZSBmdW5jdGlvbmFsaXR5IG9mIHRoZSBFbWFpbCBJbnZvaWNlIG1vZGFsLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ2VtYWlsX2ludm9pY2UnLCBbJ21vZGFsJ10sIGZ1bmN0aW9uKGRhdGEpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBWQVJJQUJMRVNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdCAqXG5cdCAqIEB0eXBlIHtqUXVlcnl9XG5cdCAqL1xuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFxuXHQvKipcblx0ICogTW9kdWxlIEluc3RhbmNlXG5cdCAqXG5cdCAqIEB0eXBlIHtPYmplY3R9XG5cdCAqL1xuXHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBGVU5DVElPTlNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogU2VuZCB0aGUgbW9kYWwgZGF0YSB0byB0aGUgZm9ybSB0aHJvdWdoIGFuIEFKQVggY2FsbC5cblx0ICpcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XG5cdCAqL1xuXHRmdW5jdGlvbiBfb25TZW5kQ2xpY2soZXZlbnQpIHtcblx0XHRjb25zdCBnZXRQYXJhbXMgPSB7XG5cdFx0XHRvSUQ6ICR0aGlzLmRhdGEoJ29yZGVySWQnKSxcblx0XHRcdHR5cGU6ICdpbnZvaWNlJyxcblx0XHRcdG1haWw6ICcxJyxcblx0XHRcdGdtX3F1aWNrX21haWw6ICcxJ1xuXHRcdH07XG5cdFx0Y29uc3QgdXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2dtX3BkZl9vcmRlci5waHA/JyArICQucGFyYW0oZ2V0UGFyYW1zKTtcblx0XHRjb25zdCBkYXRhID0gJHRoaXMuZmluZCgnZm9ybScpLnNlcmlhbGl6ZSgpO1xuXHRcdGNvbnN0ICRzZW5kQnV0dG9uID0gJChldmVudC50YXJnZXQpO1xuXHRcdFxuXHRcdCRzZW5kQnV0dG9uLmFkZENsYXNzKCdkaXNhYmxlZCcpLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG5cdFx0XG5cdFx0JC5hamF4KHtcblx0XHRcdHVybCxcblx0XHRcdGRhdGEsXG5cdFx0XHRtZXRob2Q6ICdQT1NUJ1xuXHRcdH0pXG5cdFx0XHQuZG9uZShmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRjb25zdCBtZXNzYWdlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ01BSUxfU1VDQ0VTUycsICdnbV9zZW5kX29yZGVyJyk7XG5cdFx0XHRcdCQoJy5vcmRlcnMgLnRhYmxlLW1haW4nKS5EYXRhVGFibGUoKS5hamF4LnJlbG9hZChudWxsLCBmYWxzZSk7XG5cdFx0XHRcdCQoJy5vcmRlcnMgLnRhYmxlLW1haW4nKS5vcmRlcnNfb3ZlcnZpZXdfZmlsdGVyKCdyZWxvYWQnKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFNob3cgc3VjY2VzcyBtZXNzYWdlIGluIHRoZSBhZG1pbiBpbmZvIGJveC5cblx0XHRcdFx0anNlLmxpYnMuaW5mb19ib3guYWRkU3VjY2Vzc01lc3NhZ2UobWVzc2FnZSk7XG5cdFx0XHR9KVxuXHRcdFx0LmZhaWwoZnVuY3Rpb24oanF4aHIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XG5cdFx0XHRcdGNvbnN0IHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyk7XG5cdFx0XHRcdGNvbnN0IG1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnTUFJTF9VTlNVQ0NFU1MnLCAnZ21fc2VuZF9vcmRlcicpO1xuXHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZSh0aXRsZSwgbWVzc2FnZSk7XG5cdFx0XHR9KVxuXHRcdFx0LmFsd2F5cyhmdW5jdGlvbigpIHtcblx0XHRcdFx0JHRoaXMubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0JHNlbmRCdXR0b24ucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG5cdFx0XHR9KTtcblx0fVxuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIElOSVRJQUxJWkFUSU9OXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0JHRoaXMub24oJ2NsaWNrJywgJy5idG4uc2VuZCcsIF9vblNlbmRDbGljayk7XG5cdFx0ZG9uZSgpO1xuXHR9O1xuXHRcblx0cmV0dXJuIG1vZHVsZTtcbn0pOyJdfQ==
