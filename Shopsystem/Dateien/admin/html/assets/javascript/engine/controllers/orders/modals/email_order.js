'use strict';

/* --------------------------------------------------------------
 email_order.js 2016-05-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Email Order Modal Controller
 */
gx.controllers.module('email_order', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: {
			subject: $this.find('.subject'),
			emailAddress: $this.find('.email-address')
		}
	};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Send the modal data to the form through an AJAX call.
  *
  * @param {jQuery.Event} event
  */
	function _onSendClick(event) {
		var getParams = {
			oID: $this.data('orderId'),
			type: 'send_order'
		};
		var url = jse.core.config.get('appUrl') + '/admin/gm_send_order.php?' + $.param(getParams);
		var data = {
			gm_mail: module.bindings.emailAddress.get(),
			gm_subject: module.bindings.subject.get()
		};
		var $sendButton = $(event.target);

		$sendButton.addClass('disabled').prop('disabled', true);

		$.ajax({
			url: url,
			data: data,
			method: 'POST'
		}).done(function (response) {
			var message = jse.core.lang.translate('MAIL_SUCCESS', 'gm_send_order');
			var $tableRow = $('tbody tr#' + getParams.oID);

			// Remove the e-mail symbol
			$tableRow.find('td.actions i.tooltip-confirmation-not-sent').remove();

			// Show success message in the admin info box.
			jse.libs.info_box.addSuccessMessage(message);
		}).fail(function (jqxhr, textStatus, errorThrown) {
			var title = jse.core.lang.translate('error', 'messages');
			var content = jse.core.lang.translate('MAIL_UNSUCCESS', 'gm_send_order');

			// Show error message in a modal.
			jse.libs.modal.message({ title: title, content: content });
		}).always(function () {
			$this.modal('hide');
			$sendButton.removeClass('disabled').prop('disabled', false);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvZW1haWxfb3JkZXIuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJiaW5kaW5ncyIsInN1YmplY3QiLCJmaW5kIiwiZW1haWxBZGRyZXNzIiwiX29uU2VuZENsaWNrIiwiZXZlbnQiLCJnZXRQYXJhbXMiLCJvSUQiLCJ0eXBlIiwidXJsIiwianNlIiwiY29yZSIsImNvbmZpZyIsImdldCIsInBhcmFtIiwiZ21fbWFpbCIsImdtX3N1YmplY3QiLCIkc2VuZEJ1dHRvbiIsInRhcmdldCIsImFkZENsYXNzIiwicHJvcCIsImFqYXgiLCJtZXRob2QiLCJkb25lIiwicmVzcG9uc2UiLCJtZXNzYWdlIiwibGFuZyIsInRyYW5zbGF0ZSIsIiR0YWJsZVJvdyIsInJlbW92ZSIsImxpYnMiLCJpbmZvX2JveCIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwiZmFpbCIsImpxeGhyIiwidGV4dFN0YXR1cyIsImVycm9yVGhyb3duIiwidGl0bGUiLCJjb250ZW50IiwibW9kYWwiLCJhbHdheXMiLCJyZW1vdmVDbGFzcyIsImluaXQiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7QUFHQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQXNCLGFBQXRCLEVBQXFDLENBQUMsT0FBRCxDQUFyQyxFQUFnRCxVQUFTQyxJQUFULEVBQWU7O0FBRTlEOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUgsU0FBUztBQUNkSSxZQUFVO0FBQ1RDLFlBQVNILE1BQU1JLElBQU4sQ0FBVyxVQUFYLENBREE7QUFFVEMsaUJBQWNMLE1BQU1JLElBQU4sQ0FBVyxnQkFBWDtBQUZMO0FBREksRUFBZjs7QUFPQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0UsWUFBVCxDQUFzQkMsS0FBdEIsRUFBNkI7QUFDNUIsTUFBTUMsWUFBWTtBQUNqQkMsUUFBS1QsTUFBTUQsSUFBTixDQUFXLFNBQVgsQ0FEWTtBQUVqQlcsU0FBTTtBQUZXLEdBQWxCO0FBSUEsTUFBTUMsTUFBTUMsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQywyQkFBaEMsR0FBOERkLEVBQUVlLEtBQUYsQ0FBUVIsU0FBUixDQUExRTtBQUNBLE1BQU1ULE9BQU87QUFDWmtCLFlBQVNuQixPQUFPSSxRQUFQLENBQWdCRyxZQUFoQixDQUE2QlUsR0FBN0IsRUFERztBQUVaRyxlQUFZcEIsT0FBT0ksUUFBUCxDQUFnQkMsT0FBaEIsQ0FBd0JZLEdBQXhCO0FBRkEsR0FBYjtBQUlBLE1BQU1JLGNBQWNsQixFQUFFTSxNQUFNYSxNQUFSLENBQXBCOztBQUVBRCxjQUFZRSxRQUFaLENBQXFCLFVBQXJCLEVBQWlDQyxJQUFqQyxDQUFzQyxVQUF0QyxFQUFrRCxJQUFsRDs7QUFFQXJCLElBQUVzQixJQUFGLENBQU87QUFDTlosV0FETTtBQUVOWixhQUZNO0FBR055QixXQUFRO0FBSEYsR0FBUCxFQUtFQyxJQUxGLENBS08sVUFBU0MsUUFBVCxFQUFtQjtBQUN4QixPQUFNQyxVQUFVZixJQUFJQyxJQUFKLENBQVNlLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxlQUF4QyxDQUFoQjtBQUNBLE9BQU1DLFlBQVk3QixnQkFBY08sVUFBVUMsR0FBeEIsQ0FBbEI7O0FBRUE7QUFDQXFCLGFBQVUxQixJQUFWLENBQWUsNENBQWYsRUFBNkQyQixNQUE3RDs7QUFFQTtBQUNBbkIsT0FBSW9CLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsaUJBQWxCLENBQW9DUCxPQUFwQztBQUNBLEdBZEYsRUFlRVEsSUFmRixDQWVPLFVBQVNDLEtBQVQsRUFBZ0JDLFVBQWhCLEVBQTRCQyxXQUE1QixFQUF5QztBQUM5QyxPQUFNQyxRQUFRM0IsSUFBSUMsSUFBSixDQUFTZSxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FBZDtBQUNBLE9BQU1XLFVBQVU1QixJQUFJQyxJQUFKLENBQVNlLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixnQkFBeEIsRUFBMEMsZUFBMUMsQ0FBaEI7O0FBRUE7QUFDQWpCLE9BQUlvQixJQUFKLENBQVNTLEtBQVQsQ0FBZWQsT0FBZixDQUF1QixFQUFDWSxZQUFELEVBQVFDLGdCQUFSLEVBQXZCO0FBQ0EsR0FyQkYsRUFzQkVFLE1BdEJGLENBc0JTLFlBQVc7QUFDbEIxQyxTQUFNeUMsS0FBTixDQUFZLE1BQVo7QUFDQXRCLGVBQVl3QixXQUFaLENBQXdCLFVBQXhCLEVBQW9DckIsSUFBcEMsQ0FBeUMsVUFBekMsRUFBcUQsS0FBckQ7QUFDQSxHQXpCRjtBQTBCQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUF4QixRQUFPOEMsSUFBUCxHQUFjLFVBQVNuQixJQUFULEVBQWU7QUFDNUJ6QixRQUFNNkMsRUFBTixDQUFTLE9BQVQsRUFBa0IsV0FBbEIsRUFBK0J2QyxZQUEvQjtBQUNBbUI7QUFDQSxFQUhEOztBQUtBLFFBQU8zQixNQUFQO0FBQ0EsQ0F4RkQiLCJmaWxlIjoib3JkZXJzL21vZGFscy9lbWFpbF9vcmRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZW1haWxfb3JkZXIuanMgMjAxNi0wNS0wNVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogRW1haWwgT3JkZXIgTW9kYWwgQ29udHJvbGxlclxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ2VtYWlsX29yZGVyJywgWydtb2RhbCddLCBmdW5jdGlvbihkYXRhKSB7XG5cdFxuXHQndXNlIHN0cmljdCc7XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gVkFSSUFCTEVTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIE1vZHVsZSBTZWxlY3RvclxuXHQgKlxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHQgKi9cblx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcblx0LyoqXG5cdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHQgKlxuXHQgKiBAdHlwZSB7T2JqZWN0fVxuXHQgKi9cblx0Y29uc3QgbW9kdWxlID0ge1xuXHRcdGJpbmRpbmdzOiB7XG5cdFx0XHRzdWJqZWN0OiAkdGhpcy5maW5kKCcuc3ViamVjdCcpLFxuXHRcdFx0ZW1haWxBZGRyZXNzOiAkdGhpcy5maW5kKCcuZW1haWwtYWRkcmVzcycpXG5cdFx0fVxuXHR9O1xuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIEZVTkNUSU9OU1xuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XG5cdC8qKlxuXHQgKiBTZW5kIHRoZSBtb2RhbCBkYXRhIHRvIHRoZSBmb3JtIHRocm91Z2ggYW4gQUpBWCBjYWxsLlxuXHQgKlxuXHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0ICovXG5cdGZ1bmN0aW9uIF9vblNlbmRDbGljayhldmVudCkge1xuXHRcdGNvbnN0IGdldFBhcmFtcyA9IHtcblx0XHRcdG9JRDogJHRoaXMuZGF0YSgnb3JkZXJJZCcpLFxuXHRcdFx0dHlwZTogJ3NlbmRfb3JkZXInXG5cdFx0fTtcblx0XHRjb25zdCB1cmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vZ21fc2VuZF9vcmRlci5waHA/JyArICQucGFyYW0oZ2V0UGFyYW1zKTtcblx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0Z21fbWFpbDogbW9kdWxlLmJpbmRpbmdzLmVtYWlsQWRkcmVzcy5nZXQoKSxcblx0XHRcdGdtX3N1YmplY3Q6IG1vZHVsZS5iaW5kaW5ncy5zdWJqZWN0LmdldCgpXG5cdFx0fTtcblx0XHRjb25zdCAkc2VuZEJ1dHRvbiA9ICQoZXZlbnQudGFyZ2V0KTtcblx0XHRcblx0XHQkc2VuZEJ1dHRvbi5hZGRDbGFzcygnZGlzYWJsZWQnKS5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xuXHRcdFxuXHRcdCQuYWpheCh7XG5cdFx0XHR1cmwsXG5cdFx0XHRkYXRhLFxuXHRcdFx0bWV0aG9kOiAnUE9TVCdcblx0XHR9KVxuXHRcdFx0LmRvbmUoZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdNQUlMX1NVQ0NFU1MnLCAnZ21fc2VuZF9vcmRlcicpO1xuXHRcdFx0XHRjb25zdCAkdGFibGVSb3cgPSAkKGB0Ym9keSB0ciMke2dldFBhcmFtcy5vSUR9YCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBSZW1vdmUgdGhlIGUtbWFpbCBzeW1ib2xcblx0XHRcdFx0JHRhYmxlUm93LmZpbmQoJ3RkLmFjdGlvbnMgaS50b29sdGlwLWNvbmZpcm1hdGlvbi1ub3Qtc2VudCcpLnJlbW92ZSgpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2hvdyBzdWNjZXNzIG1lc3NhZ2UgaW4gdGhlIGFkbWluIGluZm8gYm94LlxuXHRcdFx0XHRqc2UubGlicy5pbmZvX2JveC5hZGRTdWNjZXNzTWVzc2FnZShtZXNzYWdlKTtcblx0XHRcdH0pXG5cdFx0XHQuZmFpbChmdW5jdGlvbihqcXhociwgdGV4dFN0YXR1cywgZXJyb3JUaHJvd24pIHtcblx0XHRcdFx0Y29uc3QgdGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKTtcblx0XHRcdFx0Y29uc3QgY29udGVudCA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdNQUlMX1VOU1VDQ0VTUycsICdnbV9zZW5kX29yZGVyJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTaG93IGVycm9yIG1lc3NhZ2UgaW4gYSBtb2RhbC5cblx0XHRcdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZSh7dGl0bGUsIGNvbnRlbnR9KTtcblx0XHRcdH0pXG5cdFx0XHQuYWx3YXlzKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkdGhpcy5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XHQkc2VuZEJ1dHRvbi5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcblx0XHRcdH0pO1xuXHR9XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gSU5JVElBTElaQVRJT05cblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHQkdGhpcy5vbignY2xpY2snLCAnLmJ0bi5zZW5kJywgX29uU2VuZENsaWNrKTtcblx0XHRkb25lKCk7XG5cdH07XG5cdFxuXHRyZXR1cm4gbW9kdWxlO1xufSk7Il19
