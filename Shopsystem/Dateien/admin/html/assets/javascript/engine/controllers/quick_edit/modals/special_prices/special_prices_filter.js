'use strict';

/* --------------------------------------------------------------
 special_price_filter.js 2016-12-20
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 *  Special Prices Table Filter controller
 *
 * Handles the QuickEdit special prices table filtering.
 *
 * ### Methods
 *
 * **Reload Filtering Options**
 *
 * ```
 * // Reload the filter options with an AJAX request (optionally provide a second parameter for the AJAX URL).
 * $('.table-main').quick_edit_special_price_filter('reload');
 * ```
 */
gx.controllers.module('special_prices_filter', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js', 'loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Enter Key Code
  *
  * @type {Number}
  */

	var ENTER_KEY_CODE = 13; // ENTER

	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	var $this = $(this);

	/**
  * Filter Row Selector
  *
  * @type {jQuery}
  */
	var $filter = $this.find('tr.special-price-filter');

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = { bindings: {} };

	// Dynamically define the filter row data-bindings. 
	$filter.find('th').each(function () {
		var columnName = $(this).data('columnName');

		if (columnName === 'checkbox' || columnName === 'actions') {
			return true;
		}

		module.bindings[columnName] = $(this).find('input, select').first();
	});

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Reload filter options with an AJAX request.
  *
  * This function implements the $('.datatable').quick_edit_special_price_filter('reload') which will reload the
  * filtering "multi_select" instances will new options. It must be used after some table data are changed and the
  * filtering options need to be updated.
  *
  * @param {String} url Optional, the URL to be used for fetching the options. Do not add the "pageToken"
  * parameter to URL, it will be appended in this method.
  */
	function _reload(url) {
		url = url || jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/FilterOptions';
		var data = { pageToken: jse.core.config.get('pageToken') };

		$.getJSON(url, data).done(function (response) {
			for (var column in response) {
				var $select = $filter.find('.SumoSelect > select.' + column);
				var currentValueBackup = $select.val();

				if (!$select.length) {
					return;
				}

				$select.empty();

				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = response[column][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var option = _step.value;

						$select.append(new Option(option.text, option.value));
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				if (currentValueBackup !== null) {
					$select.val(currentValueBackup);
				}

				$select.multi_select('refresh');
			}
		});
	}

	/**
  * Add public "quick_edit_special_price_filter" method to jQuery in order.
  */
	function _addPublicMethod() {
		if ($.fn.quick_edit_special_price_filter) {
			return;
		}

		$.fn.extend({
			quick_edit_special_price_filter: function quick_edit_special_price_filter(action) {
				for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
					args[_key - 1] = arguments[_key];
				}

				$.each(this, function () {
					switch (action) {
						case 'reload':
							_reload.apply(this, args);
							break;
					}
				});
			}
		});
	}

	/**
  * On Filter Button Click
  *
  * Apply the provided filters and update the table rows.
  */
	function _onApplyFiltersClick() {
		var filter = {};

		$filter.find('th').each(function () {
			var columnName = $(this).data('columnName');

			if (columnName === 'checkbox' || columnName === 'actions') {
				return true;
			}

			var value = module.bindings[columnName].get();

			if (value) {
				filter[columnName] = value;
				$this.DataTable().column(columnName + ':name').search(value);
			} else {
				$this.DataTable().column(columnName + ':name').search('');
			}
		});

		$this.trigger('quick_edit_special_price_filter:change', [filter]);
		$this.DataTable().draw();
	}

	/**
  * On Reset Button Click
  *
  * Reset the filter form and reload the table data without filtering.
  */
	function _onResetFiltersClick() {
		$filter.find('input, select').not('.length, .select-page-mode').val('');
		$filter.find('select').not('.length, .select-page-mode').multi_select('refresh');
		$this.DataTable().columns().search('').draw();
		$this.trigger('quick_edit_special_price_filter:change', [{}]);
	}

	/**
  * Apply the filters when the user presses the Enter key.
  *
  * @param {jQuery.Event} event
  */
	function _onInputTextKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$filter.find('.apply-special-price-filters').trigger('click');
		}
	}

	/**
  * Parse the initial filtering parameters and apply them to the table.
  */
	function _parseFilteringParameters() {
		var _$$deparam = $.deparam(window.location.search.slice(1)),
		    filter = _$$deparam.filter;

		for (var name in filter) {
			var value = filter[name];

			if (module.bindings[name]) {
				module.bindings[name].set(value);
			}
		}
	}

	/**
  * Normalize array filtering values.
  *
  * By default datatables will concatenate array search values into a string separated with "," commas. This
  * is not acceptable though because some filtering elements may contain values with comma and thus the array
  * cannot be parsed from backend. This method will reset those cases back to arrays for a clearer transaction
  * with the backend.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {DataTables.Settings} settings DataTables settings object.
  * @param {Object} data Data that will be sent to the server in an object form.
  */
	function _normalizeArrayValues(event, settings, data) {
		var filter = {};

		for (var name in module.bindings) {
			var value = module.bindings[name].get();

			if (value && value.constructor === Array) {
				filter[name] = value;
			}
		}

		for (var entry in filter) {
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = data.columns[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var column = _step2.value;

					if (entry === column.name && filter[entry].constructor === Array) {
						column.search.value = filter[entry];
						break;
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		_addPublicMethod();
		_parseFilteringParameters();

		$filter.on('keyup', 'input:text', _onInputTextKeyUp).on('click', '.apply-special-price-filters', _onApplyFiltersClick);

		$filter.parents('.special-prices.modal').on('click', 'button.reset-special-price-filters', _onResetFiltersClick);

		$this.on('preXhr.dt', _normalizeArrayValues);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3NwZWNpYWxfcHJpY2VzL3NwZWNpYWxfcHJpY2VzX2ZpbHRlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIkVOVEVSX0tFWV9DT0RFIiwiJHRoaXMiLCIkIiwiJGZpbHRlciIsImZpbmQiLCJiaW5kaW5ncyIsImVhY2giLCJjb2x1bW5OYW1lIiwiZmlyc3QiLCJfcmVsb2FkIiwidXJsIiwiY29yZSIsImNvbmZpZyIsImdldCIsInBhZ2VUb2tlbiIsImdldEpTT04iLCJkb25lIiwicmVzcG9uc2UiLCJjb2x1bW4iLCIkc2VsZWN0IiwiY3VycmVudFZhbHVlQmFja3VwIiwidmFsIiwibGVuZ3RoIiwiZW1wdHkiLCJvcHRpb24iLCJhcHBlbmQiLCJPcHRpb24iLCJ0ZXh0IiwidmFsdWUiLCJtdWx0aV9zZWxlY3QiLCJfYWRkUHVibGljTWV0aG9kIiwiZm4iLCJxdWlja19lZGl0X3NwZWNpYWxfcHJpY2VfZmlsdGVyIiwiZXh0ZW5kIiwiYWN0aW9uIiwiYXJncyIsImFwcGx5IiwiX29uQXBwbHlGaWx0ZXJzQ2xpY2siLCJmaWx0ZXIiLCJEYXRhVGFibGUiLCJzZWFyY2giLCJ0cmlnZ2VyIiwiZHJhdyIsIl9vblJlc2V0RmlsdGVyc0NsaWNrIiwibm90IiwiY29sdW1ucyIsIl9vbklucHV0VGV4dEtleVVwIiwiZXZlbnQiLCJ3aGljaCIsIl9wYXJzZUZpbHRlcmluZ1BhcmFtZXRlcnMiLCJkZXBhcmFtIiwid2luZG93IiwibG9jYXRpb24iLCJzbGljZSIsIm5hbWUiLCJzZXQiLCJfbm9ybWFsaXplQXJyYXlWYWx1ZXMiLCJzZXR0aW5ncyIsImNvbnN0cnVjdG9yIiwiQXJyYXkiLCJlbnRyeSIsImluaXQiLCJvbiIsInBhcmVudHMiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7QUFjQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsdUJBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLG1EQUVDLGlCQUZELENBSEQsRUFRQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxpQkFBaUIsRUFBdkIsQ0FiYyxDQWFhOztBQUUzQjs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxVQUFVRixNQUFNRyxJQUFOLENBQVcseUJBQVgsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTVIsU0FBUyxFQUFDUyxVQUFVLEVBQVgsRUFBZjs7QUFFQTtBQUNBRixTQUFRQyxJQUFSLENBQWEsSUFBYixFQUFtQkUsSUFBbkIsQ0FBd0IsWUFBVztBQUNsQyxNQUFNQyxhQUFhTCxFQUFFLElBQUYsRUFBUUgsSUFBUixDQUFhLFlBQWIsQ0FBbkI7O0FBRUEsTUFBSVEsZUFBZSxVQUFmLElBQTZCQSxlQUFlLFNBQWhELEVBQTJEO0FBQzFELFVBQU8sSUFBUDtBQUNBOztBQUVEWCxTQUFPUyxRQUFQLENBQWdCRSxVQUFoQixJQUE4QkwsRUFBRSxJQUFGLEVBQVFFLElBQVIsQ0FBYSxlQUFiLEVBQThCSSxLQUE5QixFQUE5QjtBQUNBLEVBUkQ7O0FBVUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBVUEsVUFBU0MsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDckJBLFFBQU1BLE9BQU9iLElBQUljLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MseURBQTdDO0FBQ0EsTUFBTWQsT0FBTyxFQUFDZSxXQUFXakIsSUFBSWMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQixDQUFaLEVBQWI7O0FBRUFYLElBQUVhLE9BQUYsQ0FBVUwsR0FBVixFQUFlWCxJQUFmLEVBQXFCaUIsSUFBckIsQ0FBMEIsVUFBQ0MsUUFBRCxFQUFjO0FBQ3ZDLFFBQUssSUFBSUMsTUFBVCxJQUFtQkQsUUFBbkIsRUFBNkI7QUFDNUIsUUFBTUUsVUFBVWhCLFFBQVFDLElBQVIsQ0FBYSwwQkFBMEJjLE1BQXZDLENBQWhCO0FBQ0EsUUFBTUUscUJBQXFCRCxRQUFRRSxHQUFSLEVBQTNCOztBQUVBLFFBQUksQ0FBQ0YsUUFBUUcsTUFBYixFQUFxQjtBQUNwQjtBQUNBOztBQUVESCxZQUFRSSxLQUFSOztBQVI0QjtBQUFBO0FBQUE7O0FBQUE7QUFVNUIsMEJBQW1CTixTQUFTQyxNQUFULENBQW5CLDhIQUFxQztBQUFBLFVBQTVCTSxNQUE0Qjs7QUFDcENMLGNBQVFNLE1BQVIsQ0FBZSxJQUFJQyxNQUFKLENBQVdGLE9BQU9HLElBQWxCLEVBQXdCSCxPQUFPSSxLQUEvQixDQUFmO0FBQ0E7QUFaMkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFjNUIsUUFBSVIsdUJBQXVCLElBQTNCLEVBQWlDO0FBQ2hDRCxhQUFRRSxHQUFSLENBQVlELGtCQUFaO0FBQ0E7O0FBRURELFlBQVFVLFlBQVIsQ0FBcUIsU0FBckI7QUFDQTtBQUNELEdBckJEO0FBc0JBOztBQUVEOzs7QUFHQSxVQUFTQyxnQkFBVCxHQUE0QjtBQUMzQixNQUFJNUIsRUFBRTZCLEVBQUYsQ0FBS0MsK0JBQVQsRUFBMEM7QUFDekM7QUFDQTs7QUFFRDlCLElBQUU2QixFQUFGLENBQUtFLE1BQUwsQ0FBWTtBQUNYRCxvQ0FBaUMseUNBQVNFLE1BQVQsRUFBMEI7QUFBQSxzQ0FBTkMsSUFBTTtBQUFOQSxTQUFNO0FBQUE7O0FBQzFEakMsTUFBRUksSUFBRixDQUFPLElBQVAsRUFBYSxZQUFXO0FBQ3ZCLGFBQVE0QixNQUFSO0FBQ0MsV0FBSyxRQUFMO0FBQ0N6QixlQUFRMkIsS0FBUixDQUFjLElBQWQsRUFBb0JELElBQXBCO0FBQ0E7QUFIRjtBQUtBLEtBTkQ7QUFPQTtBQVRVLEdBQVo7QUFXQTs7QUFFRDs7Ozs7QUFLQSxVQUFTRSxvQkFBVCxHQUFnQztBQUMvQixNQUFNQyxTQUFTLEVBQWY7O0FBRUFuQyxVQUFRQyxJQUFSLENBQWEsSUFBYixFQUFtQkUsSUFBbkIsQ0FBd0IsWUFBVztBQUNsQyxPQUFNQyxhQUFhTCxFQUFFLElBQUYsRUFBUUgsSUFBUixDQUFhLFlBQWIsQ0FBbkI7O0FBRUEsT0FBSVEsZUFBZSxVQUFmLElBQTZCQSxlQUFlLFNBQWhELEVBQTJEO0FBQzFELFdBQU8sSUFBUDtBQUNBOztBQUVELE9BQUlxQixRQUFRaEMsT0FBT1MsUUFBUCxDQUFnQkUsVUFBaEIsRUFBNEJNLEdBQTVCLEVBQVo7O0FBRUEsT0FBSWUsS0FBSixFQUFXO0FBQ1ZVLFdBQU8vQixVQUFQLElBQXFCcUIsS0FBckI7QUFDQTNCLFVBQU1zQyxTQUFOLEdBQWtCckIsTUFBbEIsQ0FBNEJYLFVBQTVCLFlBQStDaUMsTUFBL0MsQ0FBc0RaLEtBQXREO0FBQ0EsSUFIRCxNQUdPO0FBQ04zQixVQUFNc0MsU0FBTixHQUFrQnJCLE1BQWxCLENBQTRCWCxVQUE1QixZQUErQ2lDLE1BQS9DLENBQXNELEVBQXREO0FBQ0E7QUFDRCxHQWZEOztBQWlCQXZDLFFBQU13QyxPQUFOLENBQWMsd0NBQWQsRUFBd0QsQ0FBQ0gsTUFBRCxDQUF4RDtBQUNBckMsUUFBTXNDLFNBQU4sR0FBa0JHLElBQWxCO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0Msb0JBQVQsR0FBZ0M7QUFDL0J4QyxVQUFRQyxJQUFSLENBQWEsZUFBYixFQUE4QndDLEdBQTlCLENBQWtDLDRCQUFsQyxFQUFnRXZCLEdBQWhFLENBQW9FLEVBQXBFO0FBQ0FsQixVQUFRQyxJQUFSLENBQWEsUUFBYixFQUF1QndDLEdBQXZCLENBQTJCLDRCQUEzQixFQUF5RGYsWUFBekQsQ0FBc0UsU0FBdEU7QUFDQTVCLFFBQU1zQyxTQUFOLEdBQWtCTSxPQUFsQixHQUE0QkwsTUFBNUIsQ0FBbUMsRUFBbkMsRUFBdUNFLElBQXZDO0FBQ0F6QyxRQUFNd0MsT0FBTixDQUFjLHdDQUFkLEVBQXdELENBQUMsRUFBRCxDQUF4RDtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNLLGlCQUFULENBQTJCQyxLQUEzQixFQUFrQztBQUNqQyxNQUFJQSxNQUFNQyxLQUFOLEtBQWdCaEQsY0FBcEIsRUFBb0M7QUFDbkNHLFdBQVFDLElBQVIsQ0FBYSw4QkFBYixFQUE2Q3FDLE9BQTdDLENBQXFELE9BQXJEO0FBQ0E7QUFDRDs7QUFFRDs7O0FBR0EsVUFBU1EseUJBQVQsR0FBcUM7QUFBQSxtQkFDbkIvQyxFQUFFZ0QsT0FBRixDQUFVQyxPQUFPQyxRQUFQLENBQWdCWixNQUFoQixDQUF1QmEsS0FBdkIsQ0FBNkIsQ0FBN0IsQ0FBVixDQURtQjtBQUFBLE1BQzdCZixNQUQ2QixjQUM3QkEsTUFENkI7O0FBR3BDLE9BQUssSUFBSWdCLElBQVQsSUFBaUJoQixNQUFqQixFQUF5QjtBQUN4QixPQUFNVixRQUFRVSxPQUFPZ0IsSUFBUCxDQUFkOztBQUVBLE9BQUkxRCxPQUFPUyxRQUFQLENBQWdCaUQsSUFBaEIsQ0FBSixFQUEyQjtBQUMxQjFELFdBQU9TLFFBQVAsQ0FBZ0JpRCxJQUFoQixFQUFzQkMsR0FBdEIsQ0FBMEIzQixLQUExQjtBQUNBO0FBQ0Q7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7O0FBWUEsVUFBUzRCLHFCQUFULENBQStCVCxLQUEvQixFQUFzQ1UsUUFBdEMsRUFBZ0QxRCxJQUFoRCxFQUFzRDtBQUNyRCxNQUFNdUMsU0FBUyxFQUFmOztBQUVBLE9BQUssSUFBSWdCLElBQVQsSUFBaUIxRCxPQUFPUyxRQUF4QixFQUFrQztBQUNqQyxPQUFNdUIsUUFBUWhDLE9BQU9TLFFBQVAsQ0FBZ0JpRCxJQUFoQixFQUFzQnpDLEdBQXRCLEVBQWQ7O0FBRUEsT0FBSWUsU0FBU0EsTUFBTThCLFdBQU4sS0FBc0JDLEtBQW5DLEVBQTBDO0FBQ3pDckIsV0FBT2dCLElBQVAsSUFBZTFCLEtBQWY7QUFDQTtBQUNEOztBQUVELE9BQUssSUFBSWdDLEtBQVQsSUFBa0J0QixNQUFsQixFQUEwQjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUN6QiwwQkFBbUJ2QyxLQUFLOEMsT0FBeEIsbUlBQWlDO0FBQUEsU0FBeEIzQixNQUF3Qjs7QUFDaEMsU0FBSTBDLFVBQVUxQyxPQUFPb0MsSUFBakIsSUFBeUJoQixPQUFPc0IsS0FBUCxFQUFjRixXQUFkLEtBQThCQyxLQUEzRCxFQUFrRTtBQUNqRXpDLGFBQU9zQixNQUFQLENBQWNaLEtBQWQsR0FBc0JVLE9BQU9zQixLQUFQLENBQXRCO0FBQ0E7QUFDQTtBQUNEO0FBTndCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPekI7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O0FBRUFoRSxRQUFPaUUsSUFBUCxHQUFjLFVBQVM3QyxJQUFULEVBQWU7QUFDNUJjO0FBQ0FtQjs7QUFFQTlDLFVBQ0UyRCxFQURGLENBQ0ssT0FETCxFQUNjLFlBRGQsRUFDNEJoQixpQkFENUIsRUFFRWdCLEVBRkYsQ0FFSyxPQUZMLEVBRWMsOEJBRmQsRUFFOEN6QixvQkFGOUM7O0FBSUFsQyxVQUFRNEQsT0FBUixDQUFnQix1QkFBaEIsRUFDRUQsRUFERixDQUNLLE9BREwsRUFDYyxvQ0FEZCxFQUNvRG5CLG9CQURwRDs7QUFHQTFDLFFBQU02RCxFQUFOLENBQVMsV0FBVCxFQUFzQk4scUJBQXRCOztBQUVBeEM7QUFDQSxFQWREOztBQWdCQSxRQUFPcEIsTUFBUDtBQUNBLENBL09GIiwiZmlsZSI6InF1aWNrX2VkaXQvbW9kYWxzL3NwZWNpYWxfcHJpY2VzL3NwZWNpYWxfcHJpY2VzX2ZpbHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gc3BlY2lhbF9wcmljZV9maWx0ZXIuanMgMjAxNi0xMi0yMFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIFNwZWNpYWwgUHJpY2VzIFRhYmxlIEZpbHRlciBjb250cm9sbGVyXG4gKlxuICogSGFuZGxlcyB0aGUgUXVpY2tFZGl0IHNwZWNpYWwgcHJpY2VzIHRhYmxlIGZpbHRlcmluZy5cbiAqXG4gKiAjIyMgTWV0aG9kc1xuICpcbiAqICoqUmVsb2FkIEZpbHRlcmluZyBPcHRpb25zKipcbiAqXG4gKiBgYGBcbiAqIC8vIFJlbG9hZCB0aGUgZmlsdGVyIG9wdGlvbnMgd2l0aCBhbiBBSkFYIHJlcXVlc3QgKG9wdGlvbmFsbHkgcHJvdmlkZSBhIHNlY29uZCBwYXJhbWV0ZXIgZm9yIHRoZSBBSkFYIFVSTCkuXG4gKiAkKCcudGFibGUtbWFpbicpLnF1aWNrX2VkaXRfc3BlY2lhbF9wcmljZV9maWx0ZXIoJ3JlbG9hZCcpO1xuICogYGBgXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3NwZWNpYWxfcHJpY2VzX2ZpbHRlcicsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS1kZXBhcmFtL2pxdWVyeS1kZXBhcmFtLm1pbi5qc2AsXG5cdFx0J2xvYWRpbmdfc3Bpbm5lcidcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRW50ZXIgS2V5IENvZGVcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtOdW1iZXJ9XG5cdFx0ICovXG5cdFx0Y29uc3QgRU5URVJfS0VZX0NPREUgPSAxMzsgLy8gRU5URVJcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbHRlciBSb3cgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJGZpbHRlciA9ICR0aGlzLmZpbmQoJ3RyLnNwZWNpYWwtcHJpY2UtZmlsdGVyJyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHtiaW5kaW5nczoge319O1xuXHRcdFxuXHRcdC8vIER5bmFtaWNhbGx5IGRlZmluZSB0aGUgZmlsdGVyIHJvdyBkYXRhLWJpbmRpbmdzLiBcblx0XHQkZmlsdGVyLmZpbmQoJ3RoJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdGNvbnN0IGNvbHVtbk5hbWUgPSAkKHRoaXMpLmRhdGEoJ2NvbHVtbk5hbWUnKTtcblx0XHRcdFxuXHRcdFx0aWYgKGNvbHVtbk5hbWUgPT09ICdjaGVja2JveCcgfHwgY29sdW1uTmFtZSA9PT0gJ2FjdGlvbnMnKSB7XG5cdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRtb2R1bGUuYmluZGluZ3NbY29sdW1uTmFtZV0gPSAkKHRoaXMpLmZpbmQoJ2lucHV0LCBzZWxlY3QnKS5maXJzdCgpO1xuXHRcdH0pO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlbG9hZCBmaWx0ZXIgb3B0aW9ucyB3aXRoIGFuIEFKQVggcmVxdWVzdC5cblx0XHQgKlxuXHRcdCAqIFRoaXMgZnVuY3Rpb24gaW1wbGVtZW50cyB0aGUgJCgnLmRhdGF0YWJsZScpLnF1aWNrX2VkaXRfc3BlY2lhbF9wcmljZV9maWx0ZXIoJ3JlbG9hZCcpIHdoaWNoIHdpbGwgcmVsb2FkIHRoZVxuXHRcdCAqIGZpbHRlcmluZyBcIm11bHRpX3NlbGVjdFwiIGluc3RhbmNlcyB3aWxsIG5ldyBvcHRpb25zLiBJdCBtdXN0IGJlIHVzZWQgYWZ0ZXIgc29tZSB0YWJsZSBkYXRhIGFyZSBjaGFuZ2VkIGFuZCB0aGVcblx0XHQgKiBmaWx0ZXJpbmcgb3B0aW9ucyBuZWVkIHRvIGJlIHVwZGF0ZWQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gdXJsIE9wdGlvbmFsLCB0aGUgVVJMIHRvIGJlIHVzZWQgZm9yIGZldGNoaW5nIHRoZSBvcHRpb25zLiBEbyBub3QgYWRkIHRoZSBcInBhZ2VUb2tlblwiXG5cdFx0ICogcGFyYW1ldGVyIHRvIFVSTCwgaXQgd2lsbCBiZSBhcHBlbmRlZCBpbiB0aGlzIG1ldGhvZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfcmVsb2FkKHVybCkge1xuXHRcdFx0dXJsID0gdXJsIHx8IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89UXVpY2tFZGl0T3ZlcnZpZXdBamF4L0ZpbHRlck9wdGlvbnMnO1xuXHRcdFx0Y29uc3QgZGF0YSA9IHtwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpfTtcblx0XHRcdFxuXHRcdFx0JC5nZXRKU09OKHVybCwgZGF0YSkuZG9uZSgocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0Zm9yIChsZXQgY29sdW1uIGluIHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0Y29uc3QgJHNlbGVjdCA9ICRmaWx0ZXIuZmluZCgnLlN1bW9TZWxlY3QgPiBzZWxlY3QuJyArIGNvbHVtbik7XG5cdFx0XHRcdFx0Y29uc3QgY3VycmVudFZhbHVlQmFja3VwID0gJHNlbGVjdC52YWwoKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAoISRzZWxlY3QubGVuZ3RoKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCRzZWxlY3QuZW1wdHkoKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRmb3IgKGxldCBvcHRpb24gb2YgcmVzcG9uc2VbY29sdW1uXSkge1xuXHRcdFx0XHRcdFx0JHNlbGVjdC5hcHBlbmQobmV3IE9wdGlvbihvcHRpb24udGV4dCwgb3B0aW9uLnZhbHVlKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmIChjdXJyZW50VmFsdWVCYWNrdXAgIT09IG51bGwpIHtcblx0XHRcdFx0XHRcdCRzZWxlY3QudmFsKGN1cnJlbnRWYWx1ZUJhY2t1cCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCRzZWxlY3QubXVsdGlfc2VsZWN0KCdyZWZyZXNoJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGQgcHVibGljIFwicXVpY2tfZWRpdF9zcGVjaWFsX3ByaWNlX2ZpbHRlclwiIG1ldGhvZCB0byBqUXVlcnkgaW4gb3JkZXIuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2FkZFB1YmxpY01ldGhvZCgpIHtcblx0XHRcdGlmICgkLmZuLnF1aWNrX2VkaXRfc3BlY2lhbF9wcmljZV9maWx0ZXIpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkLmZuLmV4dGVuZCh7XG5cdFx0XHRcdHF1aWNrX2VkaXRfc3BlY2lhbF9wcmljZV9maWx0ZXI6IGZ1bmN0aW9uKGFjdGlvbiwgLi4uYXJncykge1xuXHRcdFx0XHRcdCQuZWFjaCh0aGlzLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHN3aXRjaCAoYWN0aW9uKSB7XG5cdFx0XHRcdFx0XHRcdGNhc2UgJ3JlbG9hZCc6XG5cdFx0XHRcdFx0XHRcdFx0X3JlbG9hZC5hcHBseSh0aGlzLCBhcmdzKTtcblx0XHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9uIEZpbHRlciBCdXR0b24gQ2xpY2tcblx0XHQgKlxuXHRcdCAqIEFwcGx5IHRoZSBwcm92aWRlZCBmaWx0ZXJzIGFuZCB1cGRhdGUgdGhlIHRhYmxlIHJvd3MuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uQXBwbHlGaWx0ZXJzQ2xpY2soKSB7XG5cdFx0XHRjb25zdCBmaWx0ZXIgPSB7fTtcblx0XHRcdFxuXHRcdFx0JGZpbHRlci5maW5kKCd0aCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGNvbnN0IGNvbHVtbk5hbWUgPSAkKHRoaXMpLmRhdGEoJ2NvbHVtbk5hbWUnKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmIChjb2x1bW5OYW1lID09PSAnY2hlY2tib3gnIHx8IGNvbHVtbk5hbWUgPT09ICdhY3Rpb25zJykge1xuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRsZXQgdmFsdWUgPSBtb2R1bGUuYmluZGluZ3NbY29sdW1uTmFtZV0uZ2V0KCk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAodmFsdWUpIHtcblx0XHRcdFx0XHRmaWx0ZXJbY29sdW1uTmFtZV0gPSB2YWx1ZTtcblx0XHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oYCR7Y29sdW1uTmFtZX06bmFtZWApLnNlYXJjaCh2YWx1ZSk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKGAke2NvbHVtbk5hbWV9Om5hbWVgKS5zZWFyY2goJycpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JHRoaXMudHJpZ2dlcigncXVpY2tfZWRpdF9zcGVjaWFsX3ByaWNlX2ZpbHRlcjpjaGFuZ2UnLCBbZmlsdGVyXSk7XG5cdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5kcmF3KCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9uIFJlc2V0IEJ1dHRvbiBDbGlja1xuXHRcdCAqXG5cdFx0ICogUmVzZXQgdGhlIGZpbHRlciBmb3JtIGFuZCByZWxvYWQgdGhlIHRhYmxlIGRhdGEgd2l0aG91dCBmaWx0ZXJpbmcuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uUmVzZXRGaWx0ZXJzQ2xpY2soKSB7XG5cdFx0XHQkZmlsdGVyLmZpbmQoJ2lucHV0LCBzZWxlY3QnKS5ub3QoJy5sZW5ndGgsIC5zZWxlY3QtcGFnZS1tb2RlJykudmFsKCcnKTtcblx0XHRcdCRmaWx0ZXIuZmluZCgnc2VsZWN0Jykubm90KCcubGVuZ3RoLCAuc2VsZWN0LXBhZ2UtbW9kZScpLm11bHRpX3NlbGVjdCgncmVmcmVzaCcpO1xuXHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuY29sdW1ucygpLnNlYXJjaCgnJykuZHJhdygpO1xuXHRcdFx0JHRoaXMudHJpZ2dlcigncXVpY2tfZWRpdF9zcGVjaWFsX3ByaWNlX2ZpbHRlcjpjaGFuZ2UnLCBbe31dKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQXBwbHkgdGhlIGZpbHRlcnMgd2hlbiB0aGUgdXNlciBwcmVzc2VzIHRoZSBFbnRlciBrZXkuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25JbnB1dFRleHRLZXlVcChldmVudCkge1xuXHRcdFx0aWYgKGV2ZW50LndoaWNoID09PSBFTlRFUl9LRVlfQ09ERSkge1xuXHRcdFx0XHQkZmlsdGVyLmZpbmQoJy5hcHBseS1zcGVjaWFsLXByaWNlLWZpbHRlcnMnKS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBQYXJzZSB0aGUgaW5pdGlhbCBmaWx0ZXJpbmcgcGFyYW1ldGVycyBhbmQgYXBwbHkgdGhlbSB0byB0aGUgdGFibGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3BhcnNlRmlsdGVyaW5nUGFyYW1ldGVycygpIHtcblx0XHRcdGNvbnN0IHtmaWx0ZXJ9ID0gJC5kZXBhcmFtKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSkpO1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBuYW1lIGluIGZpbHRlcikge1xuXHRcdFx0XHRjb25zdCB2YWx1ZSA9IGZpbHRlcltuYW1lXTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmIChtb2R1bGUuYmluZGluZ3NbbmFtZV0pIHtcblx0XHRcdFx0XHRtb2R1bGUuYmluZGluZ3NbbmFtZV0uc2V0KHZhbHVlKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBOb3JtYWxpemUgYXJyYXkgZmlsdGVyaW5nIHZhbHVlcy5cblx0XHQgKlxuXHRcdCAqIEJ5IGRlZmF1bHQgZGF0YXRhYmxlcyB3aWxsIGNvbmNhdGVuYXRlIGFycmF5IHNlYXJjaCB2YWx1ZXMgaW50byBhIHN0cmluZyBzZXBhcmF0ZWQgd2l0aCBcIixcIiBjb21tYXMuIFRoaXNcblx0XHQgKiBpcyBub3QgYWNjZXB0YWJsZSB0aG91Z2ggYmVjYXVzZSBzb21lIGZpbHRlcmluZyBlbGVtZW50cyBtYXkgY29udGFpbiB2YWx1ZXMgd2l0aCBjb21tYSBhbmQgdGh1cyB0aGUgYXJyYXlcblx0XHQgKiBjYW5ub3QgYmUgcGFyc2VkIGZyb20gYmFja2VuZC4gVGhpcyBtZXRob2Qgd2lsbCByZXNldCB0aG9zZSBjYXNlcyBiYWNrIHRvIGFycmF5cyBmb3IgYSBjbGVhcmVyIHRyYW5zYWN0aW9uXG5cdFx0ICogd2l0aCB0aGUgYmFja2VuZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0LlxuXHRcdCAqIEBwYXJhbSB7RGF0YVRhYmxlcy5TZXR0aW5nc30gc2V0dGluZ3MgRGF0YVRhYmxlcyBzZXR0aW5ncyBvYmplY3QuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGRhdGEgRGF0YSB0aGF0IHdpbGwgYmUgc2VudCB0byB0aGUgc2VydmVyIGluIGFuIG9iamVjdCBmb3JtLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9ub3JtYWxpemVBcnJheVZhbHVlcyhldmVudCwgc2V0dGluZ3MsIGRhdGEpIHtcblx0XHRcdGNvbnN0IGZpbHRlciA9IHt9O1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBuYW1lIGluIG1vZHVsZS5iaW5kaW5ncykge1xuXHRcdFx0XHRjb25zdCB2YWx1ZSA9IG1vZHVsZS5iaW5kaW5nc1tuYW1lXS5nZXQoKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICh2YWx1ZSAmJiB2YWx1ZS5jb25zdHJ1Y3RvciA9PT0gQXJyYXkpIHtcblx0XHRcdFx0XHRmaWx0ZXJbbmFtZV0gPSB2YWx1ZTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBlbnRyeSBpbiBmaWx0ZXIpIHtcblx0XHRcdFx0Zm9yIChsZXQgY29sdW1uIG9mIGRhdGEuY29sdW1ucykge1xuXHRcdFx0XHRcdGlmIChlbnRyeSA9PT0gY29sdW1uLm5hbWUgJiYgZmlsdGVyW2VudHJ5XS5jb25zdHJ1Y3RvciA9PT0gQXJyYXkpIHtcblx0XHRcdFx0XHRcdGNvbHVtbi5zZWFyY2gudmFsdWUgPSBmaWx0ZXJbZW50cnldO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRfYWRkUHVibGljTWV0aG9kKCk7XG5cdFx0XHRfcGFyc2VGaWx0ZXJpbmdQYXJhbWV0ZXJzKCk7XG5cdFx0XHRcblx0XHRcdCRmaWx0ZXJcblx0XHRcdFx0Lm9uKCdrZXl1cCcsICdpbnB1dDp0ZXh0JywgX29uSW5wdXRUZXh0S2V5VXApXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmFwcGx5LXNwZWNpYWwtcHJpY2UtZmlsdGVycycsIF9vbkFwcGx5RmlsdGVyc0NsaWNrKTtcblx0XHRcdFxuXHRcdFx0JGZpbHRlci5wYXJlbnRzKCcuc3BlY2lhbC1wcmljZXMubW9kYWwnKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJ2J1dHRvbi5yZXNldC1zcGVjaWFsLXByaWNlLWZpbHRlcnMnLCBfb25SZXNldEZpbHRlcnNDbGljayk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdwcmVYaHIuZHQnLCBfbm9ybWFsaXplQXJyYXlWYWx1ZXMpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTsiXX0=
