'use strict';

/* --------------------------------------------------------------
 add_tracking_number.js 2016-09-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Add Tracking Number Modal Controller
 *
 * Handles the functionality of the "Add Tracking Number" modal.
 */
gx.controllers.module('add_tracking_number', ['modal', gx.source + '/libs/info_box'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Stores the tracking number for a specific order.
  *
  * @param {jQuery.Event} event
  */
	function _onStoreTrackingNumberClick(event) {
		event.preventDefault();

		var orderId = $this.data('orderId');
		var parcelServiceId = $('#delivery-service').find('option:selected').val();
		var trackingNumber = $('input:text[name="tracking-number"]').val();

		// Make an AJAX call to store the tracking number if one was provided.
		if (trackingNumber.length) {
			$.ajax({
				url: './admin.php?do=OrdersModalsAjax/StoreTrackingNumber',
				data: {
					orderId: orderId,
					trackingNumber: trackingNumber,
					parcelServiceId: parcelServiceId,
					pageToken: jse.core.config.get('pageToken')
				},
				method: 'POST',
				dataType: 'JSON'
			}).done(function (response) {
				$this.modal('hide');
				jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('ADD_TRACKING_NUMBER_SUCCESS', 'admin_orders'));
				$('.table-main').DataTable().ajax.reload(null, false);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				jse.libs.modal.message({
					title: jse.core.lang.translate('error', 'messages'),
					content: jse.core.lang.translate('ADD_TRACKING_NUMBER_ERROR', 'admin_orders')
				});
				jse.core.debug.error('Store Tracking Number Error', jqXHR, textStatus, errorThrown);
			});
		} else {
			// Show an error message
			var $modalFooter = $this.find('.modal-footer');
			var errorMessage = jse.core.lang.translate('TXT_SAVE_ERROR', 'admin_general');

			// Remove error message
			$modalFooter.find('span').remove();
			$modalFooter.prepend('<span class="text-danger">' + errorMessage + '</span>');
		}
	}

	/**
  * On Add Tracking Number Modal Hidden
  *
  * Reset the tracking number modal.
  */
	function _onAddTrackingNumberModalHidden() {
		$(this).find('#tracking-number').val('');
		$(this).find('.modal-footer span').remove();
	}

	/**
  * On Add Tracking Number Modal Show
  *
  * Handles the event for storing a a tracking number from the tracking number modal.
  *
  * @param {jQuery.Event} event
  */
	function _onAddTrackingNumberModalShow(event) {
		event.stopPropagation();
		// Element which invoked the tracking number modal.
		$(this).data('orderId', $(event.relatedTarget).data('orderId'));
	}

	/**
  * Checks if the enter key was pressed and delegates to
  * the tracking number store method.
  *
  * @param {jQuery.Event} event
  */
	function _saveOnPressedEnterKey(event) {
		var keyCode = event.keyCode ? event.keyCode : event.which;

		if (keyCode === 13) {
			_onStoreTrackingNumberClick(event);
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('show.bs.modal', _onAddTrackingNumberModalShow).on('hidden.bs.modal', _onAddTrackingNumberModalHidden).on('click', '#store-tracking-number', _onStoreTrackingNumberClick).on('keypress', _saveOnPressedEnterKey);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvYWRkX3RyYWNraW5nX251bWJlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIl9vblN0b3JlVHJhY2tpbmdOdW1iZXJDbGljayIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJvcmRlcklkIiwicGFyY2VsU2VydmljZUlkIiwiZmluZCIsInZhbCIsInRyYWNraW5nTnVtYmVyIiwibGVuZ3RoIiwiYWpheCIsInVybCIsInBhZ2VUb2tlbiIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJtZXRob2QiLCJkYXRhVHlwZSIsImRvbmUiLCJyZXNwb25zZSIsIm1vZGFsIiwibGlicyIsImluZm9fYm94IiwiYWRkU3VjY2Vzc01lc3NhZ2UiLCJsYW5nIiwidHJhbnNsYXRlIiwiRGF0YVRhYmxlIiwicmVsb2FkIiwiZmFpbCIsImpxWEhSIiwidGV4dFN0YXR1cyIsImVycm9yVGhyb3duIiwibWVzc2FnZSIsInRpdGxlIiwiY29udGVudCIsImRlYnVnIiwiZXJyb3IiLCIkbW9kYWxGb290ZXIiLCJlcnJvck1lc3NhZ2UiLCJyZW1vdmUiLCJwcmVwZW5kIiwiX29uQWRkVHJhY2tpbmdOdW1iZXJNb2RhbEhpZGRlbiIsIl9vbkFkZFRyYWNraW5nTnVtYmVyTW9kYWxTaG93Iiwic3RvcFByb3BhZ2F0aW9uIiwicmVsYXRlZFRhcmdldCIsIl9zYXZlT25QcmVzc2VkRW50ZXJLZXkiLCJrZXlDb2RlIiwid2hpY2giLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQXNCLHFCQUF0QixFQUE2QyxDQUFDLE9BQUQsRUFBYUYsR0FBR0csTUFBaEIsb0JBQTdDLEVBQXNGLFVBQVNDLElBQVQsRUFBZTs7QUFFcEc7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSixTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNLLDJCQUFULENBQXFDQyxLQUFyQyxFQUE0QztBQUMzQ0EsUUFBTUMsY0FBTjs7QUFFQSxNQUFNQyxVQUFVTCxNQUFNRCxJQUFOLENBQVcsU0FBWCxDQUFoQjtBQUNBLE1BQU1PLGtCQUFrQkwsRUFBRSxtQkFBRixFQUF1Qk0sSUFBdkIsQ0FBNEIsaUJBQTVCLEVBQStDQyxHQUEvQyxFQUF4QjtBQUNBLE1BQU1DLGlCQUFpQlIsRUFBRSxvQ0FBRixFQUF3Q08sR0FBeEMsRUFBdkI7O0FBRUE7QUFDQSxNQUFJQyxlQUFlQyxNQUFuQixFQUEyQjtBQUMxQlQsS0FBRVUsSUFBRixDQUFPO0FBQ05DLFNBQUsscURBREM7QUFFTmIsVUFBTTtBQUNMTSxxQkFESztBQUVMSSxtQ0FGSztBQUdMSCxxQ0FISztBQUlMTyxnQkFBV0MsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUpOLEtBRkE7QUFRTkMsWUFBUSxNQVJGO0FBU05DLGNBQVU7QUFUSixJQUFQLEVBV0VDLElBWEYsQ0FXTyxVQUFTQyxRQUFULEVBQW1CO0FBQ3hCckIsVUFBTXNCLEtBQU4sQ0FBWSxNQUFaO0FBQ0FSLFFBQUlTLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsaUJBQWxCLENBQ0NYLElBQUlDLElBQUosQ0FBU1csSUFBVCxDQUFjQyxTQUFkLENBQXdCLDZCQUF4QixFQUF1RCxjQUF2RCxDQUREO0FBRUExQixNQUFFLGFBQUYsRUFBaUIyQixTQUFqQixHQUE2QmpCLElBQTdCLENBQWtDa0IsTUFBbEMsQ0FBeUMsSUFBekMsRUFBK0MsS0FBL0M7QUFDQSxJQWhCRixFQWlCRUMsSUFqQkYsQ0FpQk8sVUFBU0MsS0FBVCxFQUFnQkMsVUFBaEIsRUFBNEJDLFdBQTVCLEVBQXlDO0FBQzlDbkIsUUFBSVMsSUFBSixDQUFTRCxLQUFULENBQWVZLE9BQWYsQ0FBdUI7QUFDdEJDLFlBQU9yQixJQUFJQyxJQUFKLENBQVNXLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQURlO0FBRXRCUyxjQUFTdEIsSUFBSUMsSUFBSixDQUFTVyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsMkJBQXhCLEVBQXFELGNBQXJEO0FBRmEsS0FBdkI7QUFJQWIsUUFBSUMsSUFBSixDQUFTc0IsS0FBVCxDQUFlQyxLQUFmLENBQXFCLDZCQUFyQixFQUFvRFAsS0FBcEQsRUFBMkRDLFVBQTNELEVBQXVFQyxXQUF2RTtBQUNBLElBdkJGO0FBd0JBLEdBekJELE1BeUJPO0FBQ047QUFDQSxPQUFNTSxlQUFldkMsTUFBTU8sSUFBTixDQUFXLGVBQVgsQ0FBckI7QUFDQSxPQUFNaUMsZUFBZTFCLElBQUlDLElBQUosQ0FBU1csSUFBVCxDQUFjQyxTQUFkLENBQXdCLGdCQUF4QixFQUEwQyxlQUExQyxDQUFyQjs7QUFFQTtBQUNBWSxnQkFBYWhDLElBQWIsQ0FBa0IsTUFBbEIsRUFBMEJrQyxNQUExQjtBQUNBRixnQkFBYUcsT0FBYixnQ0FBa0RGLFlBQWxEO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7QUFLQSxVQUFTRywrQkFBVCxHQUEyQztBQUMxQzFDLElBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsa0JBQWIsRUFBaUNDLEdBQWpDLENBQXFDLEVBQXJDO0FBQ0FQLElBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsb0JBQWIsRUFBbUNrQyxNQUFuQztBQUNBOztBQUdEOzs7Ozs7O0FBT0EsVUFBU0csNkJBQVQsQ0FBdUN6QyxLQUF2QyxFQUE4QztBQUM3Q0EsUUFBTTBDLGVBQU47QUFDQTtBQUNBNUMsSUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxTQUFiLEVBQXdCRSxFQUFFRSxNQUFNMkMsYUFBUixFQUF1Qi9DLElBQXZCLENBQTRCLFNBQTVCLENBQXhCO0FBQ0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNnRCxzQkFBVCxDQUFnQzVDLEtBQWhDLEVBQXVDO0FBQ3RDLE1BQU02QyxVQUFVN0MsTUFBTTZDLE9BQU4sR0FBZ0I3QyxNQUFNNkMsT0FBdEIsR0FBZ0M3QyxNQUFNOEMsS0FBdEQ7O0FBRUEsTUFBSUQsWUFBWSxFQUFoQixFQUFvQjtBQUNuQjlDLCtCQUE0QkMsS0FBNUI7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7QUFFQU4sUUFBT3FELElBQVAsR0FBYyxVQUFTOUIsSUFBVCxFQUFlO0FBQzVCcEIsUUFDRW1ELEVBREYsQ0FDSyxlQURMLEVBQ3NCUCw2QkFEdEIsRUFFRU8sRUFGRixDQUVLLGlCQUZMLEVBRXdCUiwrQkFGeEIsRUFHRVEsRUFIRixDQUdLLE9BSEwsRUFHYyx3QkFIZCxFQUd3Q2pELDJCQUh4QyxFQUlFaUQsRUFKRixDQUlLLFVBSkwsRUFJaUJKLHNCQUpqQjs7QUFNQTNCO0FBQ0EsRUFSRDs7QUFVQSxRQUFPdkIsTUFBUDtBQUNBLENBaElEIiwiZmlsZSI6Im9yZGVycy9tb2RhbHMvYWRkX3RyYWNraW5nX251bWJlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBhZGRfdHJhY2tpbmdfbnVtYmVyLmpzIDIwMTYtMDktMTJcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogQWRkIFRyYWNraW5nIE51bWJlciBNb2RhbCBDb250cm9sbGVyXHJcbiAqXHJcbiAqIEhhbmRsZXMgdGhlIGZ1bmN0aW9uYWxpdHkgb2YgdGhlIFwiQWRkIFRyYWNraW5nIE51bWJlclwiIG1vZGFsLlxyXG4gKi9cclxuZ3guY29udHJvbGxlcnMubW9kdWxlKCdhZGRfdHJhY2tpbmdfbnVtYmVyJywgWydtb2RhbCcsIGAke2d4LnNvdXJjZX0vbGlicy9pbmZvX2JveGBdLCBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIFZBUklBQkxFU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdCAqXHJcblx0ICogQHR5cGUge2pRdWVyeX1cclxuXHQgKi9cclxuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIEluc3RhbmNlXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIEZVTkNUSU9OU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFN0b3JlcyB0aGUgdHJhY2tpbmcgbnVtYmVyIGZvciBhIHNwZWNpZmljIG9yZGVyLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uU3RvcmVUcmFja2luZ051bWJlckNsaWNrKGV2ZW50KSB7XHJcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHJcblx0XHRjb25zdCBvcmRlcklkID0gJHRoaXMuZGF0YSgnb3JkZXJJZCcpO1xyXG5cdFx0Y29uc3QgcGFyY2VsU2VydmljZUlkID0gJCgnI2RlbGl2ZXJ5LXNlcnZpY2UnKS5maW5kKCdvcHRpb246c2VsZWN0ZWQnKS52YWwoKTtcclxuXHRcdGNvbnN0IHRyYWNraW5nTnVtYmVyID0gJCgnaW5wdXQ6dGV4dFtuYW1lPVwidHJhY2tpbmctbnVtYmVyXCJdJykudmFsKCk7XHJcblx0XHRcclxuXHRcdC8vIE1ha2UgYW4gQUpBWCBjYWxsIHRvIHN0b3JlIHRoZSB0cmFja2luZyBudW1iZXIgaWYgb25lIHdhcyBwcm92aWRlZC5cclxuXHRcdGlmICh0cmFja2luZ051bWJlci5sZW5ndGgpIHtcclxuXHRcdFx0JC5hamF4KHtcclxuXHRcdFx0XHR1cmw6ICcuL2FkbWluLnBocD9kbz1PcmRlcnNNb2RhbHNBamF4L1N0b3JlVHJhY2tpbmdOdW1iZXInLFxyXG5cdFx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRcdG9yZGVySWQsXHJcblx0XHRcdFx0XHR0cmFja2luZ051bWJlcixcclxuXHRcdFx0XHRcdHBhcmNlbFNlcnZpY2VJZCxcclxuXHRcdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJylcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdG1ldGhvZDogJ1BPU1QnLFxyXG5cdFx0XHRcdGRhdGFUeXBlOiAnSlNPTidcclxuXHRcdFx0fSlcclxuXHRcdFx0XHQuZG9uZShmdW5jdGlvbihyZXNwb25zZSkge1xyXG5cdFx0XHRcdFx0JHRoaXMubW9kYWwoJ2hpZGUnKTtcclxuXHRcdFx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKFxyXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQUREX1RSQUNLSU5HX05VTUJFUl9TVUNDRVNTJywgJ2FkbWluX29yZGVycycpKTtcclxuXHRcdFx0XHRcdCQoJy50YWJsZS1tYWluJykuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQobnVsbCwgZmFsc2UpO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LmZhaWwoZnVuY3Rpb24oanFYSFIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XHJcblx0XHRcdFx0XHRqc2UubGlicy5tb2RhbC5tZXNzYWdlKHtcclxuXHRcdFx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpLFxyXG5cdFx0XHRcdFx0XHRjb250ZW50OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQUREX1RSQUNLSU5HX05VTUJFUl9FUlJPUicsICdhZG1pbl9vcmRlcnMnKVxyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5lcnJvcignU3RvcmUgVHJhY2tpbmcgTnVtYmVyIEVycm9yJywganFYSFIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdC8vIFNob3cgYW4gZXJyb3IgbWVzc2FnZVxyXG5cdFx0XHRjb25zdCAkbW9kYWxGb290ZXIgPSAkdGhpcy5maW5kKCcubW9kYWwtZm9vdGVyJyk7XHJcblx0XHRcdGNvbnN0IGVycm9yTWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUWFRfU0FWRV9FUlJPUicsICdhZG1pbl9nZW5lcmFsJyk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBSZW1vdmUgZXJyb3IgbWVzc2FnZVxyXG5cdFx0XHQkbW9kYWxGb290ZXIuZmluZCgnc3BhbicpLnJlbW92ZSgpO1xyXG5cdFx0XHQkbW9kYWxGb290ZXIucHJlcGVuZChgPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPiR7ZXJyb3JNZXNzYWdlfTwvc3Bhbj5gKTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogT24gQWRkIFRyYWNraW5nIE51bWJlciBNb2RhbCBIaWRkZW5cclxuXHQgKlxyXG5cdCAqIFJlc2V0IHRoZSB0cmFja2luZyBudW1iZXIgbW9kYWwuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uQWRkVHJhY2tpbmdOdW1iZXJNb2RhbEhpZGRlbigpIHtcclxuXHRcdCQodGhpcykuZmluZCgnI3RyYWNraW5nLW51bWJlcicpLnZhbCgnJyk7XHJcblx0XHQkKHRoaXMpLmZpbmQoJy5tb2RhbC1mb290ZXIgc3BhbicpLnJlbW92ZSgpO1xyXG5cdH1cclxuXHRcclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBBZGQgVHJhY2tpbmcgTnVtYmVyIE1vZGFsIFNob3dcclxuXHQgKlxyXG5cdCAqIEhhbmRsZXMgdGhlIGV2ZW50IGZvciBzdG9yaW5nIGEgYSB0cmFja2luZyBudW1iZXIgZnJvbSB0aGUgdHJhY2tpbmcgbnVtYmVyIG1vZGFsLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uQWRkVHJhY2tpbmdOdW1iZXJNb2RhbFNob3coZXZlbnQpIHtcclxuXHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cdFx0Ly8gRWxlbWVudCB3aGljaCBpbnZva2VkIHRoZSB0cmFja2luZyBudW1iZXIgbW9kYWwuXHJcblx0XHQkKHRoaXMpLmRhdGEoJ29yZGVySWQnLCAkKGV2ZW50LnJlbGF0ZWRUYXJnZXQpLmRhdGEoJ29yZGVySWQnKSk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENoZWNrcyBpZiB0aGUgZW50ZXIga2V5IHdhcyBwcmVzc2VkIGFuZCBkZWxlZ2F0ZXMgdG9cclxuXHQgKiB0aGUgdHJhY2tpbmcgbnVtYmVyIHN0b3JlIG1ldGhvZC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9zYXZlT25QcmVzc2VkRW50ZXJLZXkoZXZlbnQpIHtcclxuXHRcdGNvbnN0IGtleUNvZGUgPSBldmVudC5rZXlDb2RlID8gZXZlbnQua2V5Q29kZSA6IGV2ZW50LndoaWNoO1xyXG5cdFx0XHJcblx0XHRpZiAoa2V5Q29kZSA9PT0gMTMpIHtcclxuXHRcdFx0X29uU3RvcmVUcmFja2luZ051bWJlckNsaWNrKGV2ZW50KTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gSU5JVElBTElaQVRJT05cclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdCR0aGlzXHJcblx0XHRcdC5vbignc2hvdy5icy5tb2RhbCcsIF9vbkFkZFRyYWNraW5nTnVtYmVyTW9kYWxTaG93KVxyXG5cdFx0XHQub24oJ2hpZGRlbi5icy5tb2RhbCcsIF9vbkFkZFRyYWNraW5nTnVtYmVyTW9kYWxIaWRkZW4pXHJcblx0XHRcdC5vbignY2xpY2snLCAnI3N0b3JlLXRyYWNraW5nLW51bWJlcicsIF9vblN0b3JlVHJhY2tpbmdOdW1iZXJDbGljaylcclxuXHRcdFx0Lm9uKCdrZXlwcmVzcycsIF9zYXZlT25QcmVzc2VkRW50ZXJLZXkpO1xyXG5cdFx0XHJcblx0XHRkb25lKCk7XHJcblx0fTtcclxuXHRcclxuXHRyZXR1cm4gbW9kdWxlO1xyXG59KTsgXHJcbiJdfQ==
