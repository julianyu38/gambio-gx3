'use strict';

/* --------------------------------------------------------------
 bulk_email_invoice.js 2016-10-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Bulk Email Invoice Modal Controller
 */
gx.controllers.module('bulk_email_invoice', ['modal', 'loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: { subject: $this.find('.subject') }
	};

	/**
  * Selector for the email list item.
  *
  * @type {String}
  */
	var emailListItemSelector = '.email-list-item';

	/**
  * Selector for the email list item ID.
  *
  * @type {String}
  */
	var emailListItemEmailSelector = '.email-input';

	/**
  * Selector for the modal content body layer.
  *
  * @type {String}
  */
	var modalContentSelector = '.modal-content';

	/**
  * Request URL
  *
  * @type {String}
  */
	var requestUrl = jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php';

	/**
  * Loading spinner instance.
  *
  * @type {jQuery|null}
  */
	var $spinner = null;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Show/hide loading spinner.
  *
  * @param {Boolean} doShow Show the loading spinner?
  */
	function _toggleSpinner(doShow) {
		if (doShow) {
			$spinner = jse.libs.loading_spinner.show($this.find(modalContentSelector), $this.css('z-index'));
		} else {
			jse.libs.loading_spinner.hide($spinner);
		}
	}

	/**
  * Parse subject and replace the placeholders with the variables.
  *
  * @param {Object} invoiceData Invoice data
  *
  * @return {String}
  */
	function _getParsedSubject(invoiceData) {
		var dateFormat = jse.core.config.get('languageCode') === 'de' ? 'DD.MM.YY' : 'MM.DD.YY';

		return module.bindings.subject.get().replace('{INVOICE_NUM}', invoiceData.invoiceNumber).replace('{INVOICE_DATE}', moment(invoiceData).format(dateFormat)).replace('{ORDER_ID}', invoiceData.orderId);
	}

	/**
  * Handles the successful delivery of all messages.
  */
	function _handleDeliverySuccess() {
		var message = jse.core.lang.translate('BULK_MAIL_SUCCESS', 'gm_send_order');

		// Show success message in the admin info box.
		jse.libs.info_box.addSuccessMessage(message);

		$('.invoices .table-main').DataTable().ajax.reload(null, false);
		$('.invoices .table-main').invoices_overview_filter('reload');

		// Hide modal and loading spinner.
		_toggleSpinner(false);
		$this.modal('hide');
	}

	/**
  * Handles the failure of the message delivery.
  */
	function _handleDeliveryFail() {
		var title = jse.core.lang.translate('error', 'messages');
		var message = jse.core.lang.translate('BULK_MAIL_UNSUCCESS', 'gm_send_order');

		// Show error message in a modal.
		jse.libs.modal.showMessage(title, message);

		// Hide modal and the loading spinner and re-enable the send button.
		_toggleSpinner(false);
		$this.modal('hide');
	}

	/**
  * Validate the form for empty fields. 
  * 
  * @return {Boolean}
  */
	function _validateForm() {
		$this.find('.has-error').removeClass('has-error');

		$this.find('input:text').each(function (index, input) {
			var $input = $(input);

			if ($input.val() === '') {
				$input.parents('.form-group').addClass('has-error');
			}
		});

		return !$this.find('.has-error').length;
	}

	/**
  * Send the modal data to the form through an AJAX call.
  */
	function _onSendClick() {
		if (!_validateForm()) {
			return;
		}

		// Collection of requests in promise format.
		var promises = [];

		// Email list item elements.
		var $emailListItems = $this.find(emailListItemSelector);

		// Abort and hide modal on empty email list entries.
		if (!$emailListItems.length) {
			$this.modal('hide');
			return;
		}

		// Show loading spinner.
		_toggleSpinner(true);

		// Fill orders array with data.
		$emailListItems.each(function (index, element) {
			// Order data.
			var invoiceData = $(element).data('invoice');

			// Email address entered in input field.
			var enteredEmail = $(element).find(emailListItemEmailSelector).val();

			// Promise wrapper for AJAX requests.
			var promise = new Promise(function (resolve, reject) {
				// Request GET parameters to send.
				var getParameters = {
					oID: invoiceData.orderId,
					iID: invoiceData.invoiceId,
					type: 'invoice',
					mail: '1',
					gm_quick_mail: '1',
					preview: '1'
				};

				// Composed request URL.
				var url = requestUrl + '?' + $.param(getParameters);

				// Data to send.
				var data = {
					gm_mail: enteredEmail,
					gm_subject: _getParsedSubject(invoiceData)
				};

				$.ajax({ method: 'POST', url: url, data: data }).done(resolve).fail(reject);
			});

			// Add promise to array.
			promises.push(promise);
		});

		// Wait for all promise to respond and handle success/error.
		Promise.all(promises).then(_handleDeliverySuccess).catch(_handleDeliveryFail);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL21vZGFscy9idWxrX2VtYWlsX2ludm9pY2UuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJiaW5kaW5ncyIsInN1YmplY3QiLCJmaW5kIiwiZW1haWxMaXN0SXRlbVNlbGVjdG9yIiwiZW1haWxMaXN0SXRlbUVtYWlsU2VsZWN0b3IiLCJtb2RhbENvbnRlbnRTZWxlY3RvciIsInJlcXVlc3RVcmwiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwiJHNwaW5uZXIiLCJfdG9nZ2xlU3Bpbm5lciIsImRvU2hvdyIsImxpYnMiLCJsb2FkaW5nX3NwaW5uZXIiLCJzaG93IiwiY3NzIiwiaGlkZSIsIl9nZXRQYXJzZWRTdWJqZWN0IiwiaW52b2ljZURhdGEiLCJkYXRlRm9ybWF0IiwicmVwbGFjZSIsImludm9pY2VOdW1iZXIiLCJtb21lbnQiLCJmb3JtYXQiLCJvcmRlcklkIiwiX2hhbmRsZURlbGl2ZXJ5U3VjY2VzcyIsIm1lc3NhZ2UiLCJsYW5nIiwidHJhbnNsYXRlIiwiaW5mb19ib3giLCJhZGRTdWNjZXNzTWVzc2FnZSIsIkRhdGFUYWJsZSIsImFqYXgiLCJyZWxvYWQiLCJpbnZvaWNlc19vdmVydmlld19maWx0ZXIiLCJtb2RhbCIsIl9oYW5kbGVEZWxpdmVyeUZhaWwiLCJ0aXRsZSIsInNob3dNZXNzYWdlIiwiX3ZhbGlkYXRlRm9ybSIsInJlbW92ZUNsYXNzIiwiZWFjaCIsImluZGV4IiwiaW5wdXQiLCIkaW5wdXQiLCJ2YWwiLCJwYXJlbnRzIiwiYWRkQ2xhc3MiLCJsZW5ndGgiLCJfb25TZW5kQ2xpY2siLCJwcm9taXNlcyIsIiRlbWFpbExpc3RJdGVtcyIsImVsZW1lbnQiLCJlbnRlcmVkRW1haWwiLCJwcm9taXNlIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJnZXRQYXJhbWV0ZXJzIiwib0lEIiwiaUlEIiwiaW52b2ljZUlkIiwidHlwZSIsIm1haWwiLCJnbV9xdWlja19tYWlsIiwicHJldmlldyIsInVybCIsInBhcmFtIiwiZ21fbWFpbCIsImdtX3N1YmplY3QiLCJtZXRob2QiLCJkb25lIiwiZmFpbCIsInB1c2giLCJhbGwiLCJ0aGVuIiwiY2F0Y2giLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7O0FBR0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUFzQixvQkFBdEIsRUFBNEMsQ0FBQyxPQUFELEVBQVUsaUJBQVYsQ0FBNUMsRUFBMEUsVUFBU0MsSUFBVCxFQUFlOztBQUV4Rjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVM7QUFDZEksWUFBVSxFQUFDQyxTQUFTSCxNQUFNSSxJQUFOLENBQVcsVUFBWCxDQUFWO0FBREksRUFBZjs7QUFJQTs7Ozs7QUFLQSxLQUFNQyx3QkFBd0Isa0JBQTlCOztBQUVBOzs7OztBQUtBLEtBQU1DLDZCQUE2QixjQUFuQzs7QUFFQTs7Ozs7QUFLQSxLQUFNQyx1QkFBdUIsZ0JBQTdCOztBQUVBOzs7OztBQUtBLEtBQU1DLGFBQWFDLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MseUJBQW5EOztBQUVBOzs7OztBQUtBLEtBQUlDLFdBQVcsSUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0MsY0FBVCxDQUF3QkMsTUFBeEIsRUFBZ0M7QUFDL0IsTUFBSUEsTUFBSixFQUFZO0FBQ1hGLGNBQVdKLElBQUlPLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsSUFBekIsQ0FBOEJsQixNQUFNSSxJQUFOLENBQVdHLG9CQUFYLENBQTlCLEVBQWdFUCxNQUFNbUIsR0FBTixDQUFVLFNBQVYsQ0FBaEUsQ0FBWDtBQUNBLEdBRkQsTUFFTztBQUNOVixPQUFJTyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJHLElBQXpCLENBQThCUCxRQUE5QjtBQUNBO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTUSxpQkFBVCxDQUEyQkMsV0FBM0IsRUFBd0M7QUFDdkMsTUFBTUMsYUFBYWQsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixjQUFwQixNQUF3QyxJQUF4QyxHQUErQyxVQUEvQyxHQUE0RCxVQUEvRTs7QUFFQSxTQUFPZCxPQUFPSSxRQUFQLENBQWdCQyxPQUFoQixDQUF3QlMsR0FBeEIsR0FDTFksT0FESyxDQUNHLGVBREgsRUFDb0JGLFlBQVlHLGFBRGhDLEVBRUxELE9BRkssQ0FFRyxnQkFGSCxFQUVxQkUsT0FBT0osV0FBUCxFQUFvQkssTUFBcEIsQ0FBMkJKLFVBQTNCLENBRnJCLEVBR0xDLE9BSEssQ0FHRyxZQUhILEVBR2lCRixZQUFZTSxPQUg3QixDQUFQO0FBSUE7O0FBRUQ7OztBQUdBLFVBQVNDLHNCQUFULEdBQWtDO0FBQ2pDLE1BQU1DLFVBQVVyQixJQUFJQyxJQUFKLENBQVNxQixJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUJBQXhCLEVBQTZDLGVBQTdDLENBQWhCOztBQUVBO0FBQ0F2QixNQUFJTyxJQUFKLENBQVNpQixRQUFULENBQWtCQyxpQkFBbEIsQ0FBb0NKLE9BQXBDOztBQUVBN0IsSUFBRSx1QkFBRixFQUEyQmtDLFNBQTNCLEdBQXVDQyxJQUF2QyxDQUE0Q0MsTUFBNUMsQ0FBbUQsSUFBbkQsRUFBeUQsS0FBekQ7QUFDQXBDLElBQUUsdUJBQUYsRUFBMkJxQyx3QkFBM0IsQ0FBb0QsUUFBcEQ7O0FBRUE7QUFDQXhCLGlCQUFlLEtBQWY7QUFDQWQsUUFBTXVDLEtBQU4sQ0FBWSxNQUFaO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNDLG1CQUFULEdBQStCO0FBQzlCLE1BQU1DLFFBQVFoQyxJQUFJQyxJQUFKLENBQVNxQixJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FBZDtBQUNBLE1BQU1GLFVBQVVyQixJQUFJQyxJQUFKLENBQVNxQixJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLGVBQS9DLENBQWhCOztBQUVBO0FBQ0F2QixNQUFJTyxJQUFKLENBQVN1QixLQUFULENBQWVHLFdBQWYsQ0FBMkJELEtBQTNCLEVBQWtDWCxPQUFsQzs7QUFFQTtBQUNBaEIsaUJBQWUsS0FBZjtBQUNBZCxRQUFNdUMsS0FBTixDQUFZLE1BQVo7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTSSxhQUFULEdBQXlCO0FBQ3hCM0MsUUFBTUksSUFBTixDQUFXLFlBQVgsRUFBeUJ3QyxXQUF6QixDQUFxQyxXQUFyQzs7QUFFQTVDLFFBQU1JLElBQU4sQ0FBVyxZQUFYLEVBQXlCeUMsSUFBekIsQ0FBOEIsVUFBQ0MsS0FBRCxFQUFRQyxLQUFSLEVBQWtCO0FBQy9DLE9BQU1DLFNBQVMvQyxFQUFFOEMsS0FBRixDQUFmOztBQUVBLE9BQUlDLE9BQU9DLEdBQVAsT0FBaUIsRUFBckIsRUFBeUI7QUFDeEJELFdBQU9FLE9BQVAsQ0FBZSxhQUFmLEVBQThCQyxRQUE5QixDQUF1QyxXQUF2QztBQUNBO0FBQ0QsR0FORDs7QUFRQSxTQUFPLENBQUNuRCxNQUFNSSxJQUFOLENBQVcsWUFBWCxFQUF5QmdELE1BQWpDO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNDLFlBQVQsR0FBd0I7QUFDdkIsTUFBSSxDQUFDVixlQUFMLEVBQXNCO0FBQ3JCO0FBQ0E7O0FBRUQ7QUFDQSxNQUFNVyxXQUFXLEVBQWpCOztBQUVBO0FBQ0EsTUFBTUMsa0JBQWtCdkQsTUFBTUksSUFBTixDQUFXQyxxQkFBWCxDQUF4Qjs7QUFFQTtBQUNBLE1BQUksQ0FBQ2tELGdCQUFnQkgsTUFBckIsRUFBNkI7QUFDNUJwRCxTQUFNdUMsS0FBTixDQUFZLE1BQVo7QUFDQTtBQUNBOztBQUVEO0FBQ0F6QixpQkFBZSxJQUFmOztBQUVBO0FBQ0F5QyxrQkFBZ0JWLElBQWhCLENBQXFCLFVBQUNDLEtBQUQsRUFBUVUsT0FBUixFQUFvQjtBQUN4QztBQUNBLE9BQU1sQyxjQUFjckIsRUFBRXVELE9BQUYsRUFBV3pELElBQVgsQ0FBZ0IsU0FBaEIsQ0FBcEI7O0FBRUE7QUFDQSxPQUFNMEQsZUFBZXhELEVBQUV1RCxPQUFGLEVBQVdwRCxJQUFYLENBQWdCRSwwQkFBaEIsRUFBNEMyQyxHQUE1QyxFQUFyQjs7QUFFQTtBQUNBLE9BQU1TLFVBQVUsSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNoRDtBQUNBLFFBQU1DLGdCQUFnQjtBQUNyQkMsVUFBS3pDLFlBQVlNLE9BREk7QUFFckJvQyxVQUFLMUMsWUFBWTJDLFNBRkk7QUFHckJDLFdBQU0sU0FIZTtBQUlyQkMsV0FBTSxHQUplO0FBS3JCQyxvQkFBZSxHQUxNO0FBTXJCQyxjQUFTO0FBTlksS0FBdEI7O0FBU0E7QUFDQSxRQUFNQyxNQUFNOUQsYUFBYSxHQUFiLEdBQW1CUCxFQUFFc0UsS0FBRixDQUFRVCxhQUFSLENBQS9COztBQUVBO0FBQ0EsUUFBTS9ELE9BQU87QUFDWnlFLGNBQVNmLFlBREc7QUFFWmdCLGlCQUFZcEQsa0JBQWtCQyxXQUFsQjtBQUZBLEtBQWI7O0FBS0FyQixNQUFFbUMsSUFBRixDQUFPLEVBQUNzQyxRQUFRLE1BQVQsRUFBaUJKLFFBQWpCLEVBQXNCdkUsVUFBdEIsRUFBUCxFQUNFNEUsSUFERixDQUNPZixPQURQLEVBRUVnQixJQUZGLENBRU9mLE1BRlA7QUFHQSxJQXZCZSxDQUFoQjs7QUF5QkE7QUFDQVAsWUFBU3VCLElBQVQsQ0FBY25CLE9BQWQ7QUFDQSxHQW5DRDs7QUFxQ0E7QUFDQUMsVUFBUW1CLEdBQVIsQ0FBWXhCLFFBQVosRUFDRXlCLElBREYsQ0FDT2xELHNCQURQLEVBRUVtRCxLQUZGLENBRVF4QyxtQkFGUjtBQUdBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTFDLFFBQU9tRixJQUFQLEdBQWMsVUFBU04sSUFBVCxFQUFlO0FBQzVCM0UsUUFBTWtGLEVBQU4sQ0FBUyxPQUFULEVBQWtCLFdBQWxCLEVBQStCN0IsWUFBL0I7QUFDQXNCO0FBQ0EsRUFIRDs7QUFLQSxRQUFPN0UsTUFBUDtBQUNBLENBNU5EIiwiZmlsZSI6Imludm9pY2VzL21vZGFscy9idWxrX2VtYWlsX2ludm9pY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGJ1bGtfZW1haWxfaW52b2ljZS5qcyAyMDE2LTEwLTA1XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBCdWxrIEVtYWlsIEludm9pY2UgTW9kYWwgQ29udHJvbGxlclxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ2J1bGtfZW1haWxfaW52b2ljZScsIFsnbW9kYWwnLCAnbG9hZGluZ19zcGlubmVyJ10sIGZ1bmN0aW9uKGRhdGEpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBWQVJJQUJMRVNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdCAqXG5cdCAqIEB0eXBlIHtqUXVlcnl9XG5cdCAqL1xuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFxuXHQvKipcblx0ICogTW9kdWxlIEluc3RhbmNlXG5cdCAqXG5cdCAqIEB0eXBlIHtPYmplY3R9XG5cdCAqL1xuXHRjb25zdCBtb2R1bGUgPSB7XG5cdFx0YmluZGluZ3M6IHtzdWJqZWN0OiAkdGhpcy5maW5kKCcuc3ViamVjdCcpfVxuXHR9O1xuXHRcblx0LyoqXG5cdCAqIFNlbGVjdG9yIGZvciB0aGUgZW1haWwgbGlzdCBpdGVtLlxuXHQgKlxuXHQgKiBAdHlwZSB7U3RyaW5nfVxuXHQgKi9cblx0Y29uc3QgZW1haWxMaXN0SXRlbVNlbGVjdG9yID0gJy5lbWFpbC1saXN0LWl0ZW0nO1xuXHRcblx0LyoqXG5cdCAqIFNlbGVjdG9yIGZvciB0aGUgZW1haWwgbGlzdCBpdGVtIElELlxuXHQgKlxuXHQgKiBAdHlwZSB7U3RyaW5nfVxuXHQgKi9cblx0Y29uc3QgZW1haWxMaXN0SXRlbUVtYWlsU2VsZWN0b3IgPSAnLmVtYWlsLWlucHV0Jztcblx0XG5cdC8qKlxuXHQgKiBTZWxlY3RvciBmb3IgdGhlIG1vZGFsIGNvbnRlbnQgYm9keSBsYXllci5cblx0ICpcblx0ICogQHR5cGUge1N0cmluZ31cblx0ICovXG5cdGNvbnN0IG1vZGFsQ29udGVudFNlbGVjdG9yID0gJy5tb2RhbC1jb250ZW50Jztcblx0XHRcblx0LyoqXG5cdCAqIFJlcXVlc3QgVVJMXG5cdCAqXG5cdCAqIEB0eXBlIHtTdHJpbmd9XG5cdCAqL1xuXHRjb25zdCByZXF1ZXN0VXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2dtX3BkZl9vcmRlci5waHAnO1xuXHRcblx0LyoqXG5cdCAqIExvYWRpbmcgc3Bpbm5lciBpbnN0YW5jZS5cblx0ICpcblx0ICogQHR5cGUge2pRdWVyeXxudWxsfVxuXHQgKi9cblx0bGV0ICRzcGlubmVyID0gbnVsbDtcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBGVU5DVElPTlNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogU2hvdy9oaWRlIGxvYWRpbmcgc3Bpbm5lci5cblx0ICpcblx0ICogQHBhcmFtIHtCb29sZWFufSBkb1Nob3cgU2hvdyB0aGUgbG9hZGluZyBzcGlubmVyP1xuXHQgKi9cblx0ZnVuY3Rpb24gX3RvZ2dsZVNwaW5uZXIoZG9TaG93KSB7XG5cdFx0aWYgKGRvU2hvdykge1xuXHRcdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcy5maW5kKG1vZGFsQ29udGVudFNlbGVjdG9yKSwgJHRoaXMuY3NzKCd6LWluZGV4JykpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuaGlkZSgkc3Bpbm5lcik7XG5cdFx0fVxuXHR9XG5cdFxuXHQvKipcblx0ICogUGFyc2Ugc3ViamVjdCBhbmQgcmVwbGFjZSB0aGUgcGxhY2Vob2xkZXJzIHdpdGggdGhlIHZhcmlhYmxlcy5cblx0ICpcblx0ICogQHBhcmFtIHtPYmplY3R9IGludm9pY2VEYXRhIEludm9pY2UgZGF0YVxuXHQgKlxuXHQgKiBAcmV0dXJuIHtTdHJpbmd9XG5cdCAqL1xuXHRmdW5jdGlvbiBfZ2V0UGFyc2VkU3ViamVjdChpbnZvaWNlRGF0YSkge1xuXHRcdGNvbnN0IGRhdGVGb3JtYXQgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdsYW5ndWFnZUNvZGUnKSA9PT0gJ2RlJyA/ICdERC5NTS5ZWScgOiAnTU0uREQuWVknOyBcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlLmJpbmRpbmdzLnN1YmplY3QuZ2V0KClcblx0XHRcdC5yZXBsYWNlKCd7SU5WT0lDRV9OVU19JywgaW52b2ljZURhdGEuaW52b2ljZU51bWJlcilcblx0XHRcdC5yZXBsYWNlKCd7SU5WT0lDRV9EQVRFfScsIG1vbWVudChpbnZvaWNlRGF0YSkuZm9ybWF0KGRhdGVGb3JtYXQpKVxuXHRcdFx0LnJlcGxhY2UoJ3tPUkRFUl9JRH0nLCBpbnZvaWNlRGF0YS5vcmRlcklkKTtcblx0fVxuXHRcblx0LyoqXG5cdCAqIEhhbmRsZXMgdGhlIHN1Y2Nlc3NmdWwgZGVsaXZlcnkgb2YgYWxsIG1lc3NhZ2VzLlxuXHQgKi9cblx0ZnVuY3Rpb24gX2hhbmRsZURlbGl2ZXJ5U3VjY2VzcygpIHtcblx0XHRjb25zdCBtZXNzYWdlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVTEtfTUFJTF9TVUNDRVNTJywgJ2dtX3NlbmRfb3JkZXInKTtcblx0XHRcblx0XHQvLyBTaG93IHN1Y2Nlc3MgbWVzc2FnZSBpbiB0aGUgYWRtaW4gaW5mbyBib3guXG5cdFx0anNlLmxpYnMuaW5mb19ib3guYWRkU3VjY2Vzc01lc3NhZ2UobWVzc2FnZSk7XG5cdFx0XG5cdFx0JCgnLmludm9pY2VzIC50YWJsZS1tYWluJykuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQobnVsbCwgZmFsc2UpO1xuXHRcdCQoJy5pbnZvaWNlcyAudGFibGUtbWFpbicpLmludm9pY2VzX292ZXJ2aWV3X2ZpbHRlcigncmVsb2FkJyk7XG5cdFx0XG5cdFx0Ly8gSGlkZSBtb2RhbCBhbmQgbG9hZGluZyBzcGlubmVyLlxuXHRcdF90b2dnbGVTcGlubmVyKGZhbHNlKTtcblx0XHQkdGhpcy5tb2RhbCgnaGlkZScpO1xuXHR9XG5cdFxuXHQvKipcblx0ICogSGFuZGxlcyB0aGUgZmFpbHVyZSBvZiB0aGUgbWVzc2FnZSBkZWxpdmVyeS5cblx0ICovXG5cdGZ1bmN0aW9uIF9oYW5kbGVEZWxpdmVyeUZhaWwoKSB7XG5cdFx0Y29uc3QgdGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKTtcblx0XHRjb25zdCBtZXNzYWdlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVTEtfTUFJTF9VTlNVQ0NFU1MnLCAnZ21fc2VuZF9vcmRlcicpO1xuXHRcdFxuXHRcdC8vIFNob3cgZXJyb3IgbWVzc2FnZSBpbiBhIG1vZGFsLlxuXHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKHRpdGxlLCBtZXNzYWdlKTtcblx0XHRcblx0XHQvLyBIaWRlIG1vZGFsIGFuZCB0aGUgbG9hZGluZyBzcGlubmVyIGFuZCByZS1lbmFibGUgdGhlIHNlbmQgYnV0dG9uLlxuXHRcdF90b2dnbGVTcGlubmVyKGZhbHNlKTtcblx0XHQkdGhpcy5tb2RhbCgnaGlkZScpO1xuXHR9XG5cdFxuXHQvKipcblx0ICogVmFsaWRhdGUgdGhlIGZvcm0gZm9yIGVtcHR5IGZpZWxkcy4gXG5cdCAqIFxuXHQgKiBAcmV0dXJuIHtCb29sZWFufVxuXHQgKi9cblx0ZnVuY3Rpb24gX3ZhbGlkYXRlRm9ybSgpIHtcblx0XHQkdGhpcy5maW5kKCcuaGFzLWVycm9yJykucmVtb3ZlQ2xhc3MoJ2hhcy1lcnJvcicpOyBcblx0XHRcblx0XHQkdGhpcy5maW5kKCdpbnB1dDp0ZXh0JykuZWFjaCgoaW5kZXgsIGlucHV0KSA9PiB7XG5cdFx0XHRjb25zdCAkaW5wdXQgPSAkKGlucHV0KTsgXG5cdFx0XHRcblx0XHRcdGlmICgkaW5wdXQudmFsKCkgPT09ICcnKSB7XG5cdFx0XHRcdCRpbnB1dC5wYXJlbnRzKCcuZm9ybS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgXG5cdFx0XHR9XG5cdFx0fSk7IFxuXHRcdFx0XG5cdFx0cmV0dXJuICEkdGhpcy5maW5kKCcuaGFzLWVycm9yJykubGVuZ3RoOyBcblx0fVxuXHRcblx0LyoqXG5cdCAqIFNlbmQgdGhlIG1vZGFsIGRhdGEgdG8gdGhlIGZvcm0gdGhyb3VnaCBhbiBBSkFYIGNhbGwuXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25TZW5kQ2xpY2soKSB7XG5cdFx0aWYgKCFfdmFsaWRhdGVGb3JtKCkpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XHRcdFxuXHRcdFxuXHRcdC8vIENvbGxlY3Rpb24gb2YgcmVxdWVzdHMgaW4gcHJvbWlzZSBmb3JtYXQuXG5cdFx0Y29uc3QgcHJvbWlzZXMgPSBbXTtcblx0XHRcblx0XHQvLyBFbWFpbCBsaXN0IGl0ZW0gZWxlbWVudHMuXG5cdFx0Y29uc3QgJGVtYWlsTGlzdEl0ZW1zID0gJHRoaXMuZmluZChlbWFpbExpc3RJdGVtU2VsZWN0b3IpO1xuXHRcdFxuXHRcdC8vIEFib3J0IGFuZCBoaWRlIG1vZGFsIG9uIGVtcHR5IGVtYWlsIGxpc3QgZW50cmllcy5cblx0XHRpZiAoISRlbWFpbExpc3RJdGVtcy5sZW5ndGgpIHtcblx0XHRcdCR0aGlzLm1vZGFsKCdoaWRlJyk7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXHRcdFxuXHRcdC8vIFNob3cgbG9hZGluZyBzcGlubmVyLlxuXHRcdF90b2dnbGVTcGlubmVyKHRydWUpO1xuXHRcdFxuXHRcdC8vIEZpbGwgb3JkZXJzIGFycmF5IHdpdGggZGF0YS5cblx0XHQkZW1haWxMaXN0SXRlbXMuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcblx0XHRcdC8vIE9yZGVyIGRhdGEuXG5cdFx0XHRjb25zdCBpbnZvaWNlRGF0YSA9ICQoZWxlbWVudCkuZGF0YSgnaW52b2ljZScpO1xuXHRcdFx0XG5cdFx0XHQvLyBFbWFpbCBhZGRyZXNzIGVudGVyZWQgaW4gaW5wdXQgZmllbGQuXG5cdFx0XHRjb25zdCBlbnRlcmVkRW1haWwgPSAkKGVsZW1lbnQpLmZpbmQoZW1haWxMaXN0SXRlbUVtYWlsU2VsZWN0b3IpLnZhbCgpO1xuXHRcdFx0XG5cdFx0XHQvLyBQcm9taXNlIHdyYXBwZXIgZm9yIEFKQVggcmVxdWVzdHMuXG5cdFx0XHRjb25zdCBwcm9taXNlID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0XHQvLyBSZXF1ZXN0IEdFVCBwYXJhbWV0ZXJzIHRvIHNlbmQuXG5cdFx0XHRcdGNvbnN0IGdldFBhcmFtZXRlcnMgPSB7XG5cdFx0XHRcdFx0b0lEOiBpbnZvaWNlRGF0YS5vcmRlcklkLFxuXHRcdFx0XHRcdGlJRDogaW52b2ljZURhdGEuaW52b2ljZUlkLFxuXHRcdFx0XHRcdHR5cGU6ICdpbnZvaWNlJyxcblx0XHRcdFx0XHRtYWlsOiAnMScsXG5cdFx0XHRcdFx0Z21fcXVpY2tfbWFpbDogJzEnLFxuXHRcdFx0XHRcdHByZXZpZXc6ICcxJ1xuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQ29tcG9zZWQgcmVxdWVzdCBVUkwuXG5cdFx0XHRcdGNvbnN0IHVybCA9IHJlcXVlc3RVcmwgKyAnPycgKyAkLnBhcmFtKGdldFBhcmFtZXRlcnMpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRGF0YSB0byBzZW5kLlxuXHRcdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHRcdGdtX21haWw6IGVudGVyZWRFbWFpbCxcblx0XHRcdFx0XHRnbV9zdWJqZWN0OiBfZ2V0UGFyc2VkU3ViamVjdChpbnZvaWNlRGF0YSlcblx0XHRcdFx0fTtcblx0XHRcdFx0XG5cdFx0XHRcdCQuYWpheCh7bWV0aG9kOiAnUE9TVCcsIHVybCwgZGF0YX0pXG5cdFx0XHRcdFx0LmRvbmUocmVzb2x2ZSlcblx0XHRcdFx0XHQuZmFpbChyZWplY3QpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIEFkZCBwcm9taXNlIHRvIGFycmF5LlxuXHRcdFx0cHJvbWlzZXMucHVzaChwcm9taXNlKTtcblx0XHR9KTtcblx0XHRcblx0XHQvLyBXYWl0IGZvciBhbGwgcHJvbWlzZSB0byByZXNwb25kIGFuZCBoYW5kbGUgc3VjY2Vzcy9lcnJvci5cblx0XHRQcm9taXNlLmFsbChwcm9taXNlcylcblx0XHRcdC50aGVuKF9oYW5kbGVEZWxpdmVyeVN1Y2Nlc3MpXG5cdFx0XHQuY2F0Y2goX2hhbmRsZURlbGl2ZXJ5RmFpbCk7XG5cdH1cblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBJTklUSUFMSVpBVElPTlxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdCR0aGlzLm9uKCdjbGljaycsICcuYnRuLnNlbmQnLCBfb25TZW5kQ2xpY2spO1xuXHRcdGRvbmUoKTtcblx0fTtcblx0XG5cdHJldHVybiBtb2R1bGU7XG59KTsiXX0=
