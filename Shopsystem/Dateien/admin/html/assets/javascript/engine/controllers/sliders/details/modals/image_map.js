'use strict';

/* --------------------------------------------------------------
 image_map.js 2016-11-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Slide Image Map Modal.
 *
 * Public methods:
 *  - 'show' shows the dropdown.
 */
gx.controllers.module('image_map', [jse.source + '/vendor/jquery-canvas-area-draw/jquery.canvasAreaDraw.min.js'], function () {
	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module element (modal element).
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * CSS Class Map
  *
  * @type {Object}
  */
	var classes = {
		// Hidden class.
		hidden: 'hidden',

		// New inserted option.
		newOption: 'new'
	};

	/**
  * CSS ID Map
  *
  * @type {Object}
  */
	var ids = {
		canvas: 'canvas'
	};

	/**
  * Element Map
  *
  * @type {Object}
  */
	var elements = {
		// Canvas extension element.
		extension: $this.find('#' + ids.canvas),

		// Container elements.
		containers: {
			// Image container.
			image: $this.find('.row.image'),

			// Canvas container.
			canvas: $this.find('.row.canvas'),

			// Action buttons container.
			actionButtons: $this.find('.row.actions')
		},

		// Form inputs.
		inputs: {
			// Link area dropdown.
			area: $this.find('#image-map-area'),

			// Link title.
			linkTitle: $this.find('#image-map-link-title'),

			// Link URL.
			linkUrl: $this.find('#image-map-link-url'),

			// Link target.
			linkTarget: $this.find('#image-map-link-target')
		},

		// Buttons.
		buttons: {
			// Close modal.
			close: $this.find('.btn.action-close'),

			// Create image area.
			create: $this.find('.btn.action-create'),

			// Abort edit.
			abort: $this.find('.btn.action-abort'),

			// Delete image area.
			delete: $this.find('.btn.action-delete'),

			// Apply image area changes.
			apply: $this.find('.btn.action-apply'),

			// Reset path.
			reset: $this.find('.btn-default.action-reset')
		},

		// Alerts 
		alerts: {
			info: $this.find('.alert')
		}
	};

	/**
  * Value Map
  *
  * @type {Object}
  */
	var values = {
		// Empty string.
		empty: '',

		// 'Please select' value.
		nil: '',

		// Open in same window.
		sameWindowTarget: '_self'
	};

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Data container list.
  *
  * @type {jQuery|null}
  */
	var $list = null;

	/**
  * Slide image URL.
  *
  * @type {String|null}
  */
	var imageUrl = null;

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Fills the area dropdown with the data container list items.
  */
	function _fillAreaDropdown() {
		/**
   * Transfers the data from the data container list item to the area dropdown.
   *
   * @param {Number} index Iteration index.
   * @param {Element} element Iteration element.
   */
		var _transferToAreaDropdown = function _transferToAreaDropdown(index, element) {
			// Create new option element.
			var $option = $('<option>');

			// Set text content and value and append option to area dropdown.
			$option.text(element.dataset.linkTitle).val(index).appendTo(elements.inputs.area);
		};

		// Delete all children, except the first one.
		elements.inputs.area.children().not(':first').remove();

		// Transfer data container list items to area dropdown.
		$list.children().each(_transferToAreaDropdown);
	}

	/**
  * Switches to the default view.
  */
	function _switchToDefaultView() {
		// Image tag.
		var $image = $('<img src="' + imageUrl + '">');

		// Enable area dropdown.
		elements.inputs.area.prop('disabled', false);

		// Disable and empty the link title input.
		elements.inputs.linkTitle.prop('disabled', true).val(values.empty);

		// Disable and empty the link URL title input.
		elements.inputs.linkUrl.prop('disabled', true).val(values.empty);

		// Disable and empty the link target title input.
		elements.inputs.linkTarget.prop('disabled', true).val(values.sameWindowTarget);

		// Hide button container.
		elements.containers.actionButtons.addClass(classes.hidden);

		// Show and empty image container and append image.
		elements.containers.image.empty().removeClass(classes.hidden).append($image);

		// Hide canvas container.
		elements.containers.canvas.addClass(classes.hidden);

		// Empty extension element.
		elements.extension.empty();

		// Show create button.
		elements.buttons.create.removeClass(classes.hidden);

		// Hide reset button.
		elements.buttons.reset.addClass(classes.hidden);

		// Hide information alert-box. 
		elements.alerts.info.addClass('hidden');
	}

	/**
  * Switches to the new image area view.
  */
	function _switchToNewView() {
		// Create new draw extension element.
		var $extension = $('<div\n\t\t\t\tid="' + ids.canvas + '"\n\t\t\t\tdata-gx-extension="canvas_area_draw"\n\t\t\t\tdata-canvas_area_draw-image-url="' + imageUrl + '">');

		// Enable link title input.
		elements.inputs.linkTitle.prop('disabled', false);

		// Enable link URL input.
		elements.inputs.linkUrl.prop('disabled', false);

		// Enable the link target input and set the value to 'self'.
		elements.inputs.linkTarget.prop('disabled', false).val(values.sameWindowTarget);

		// Disable the area dropdown.
		elements.inputs.area.prop('disabled', true);

		// Hide create button.
		elements.buttons.create.addClass(classes.hidden);

		// Show apply button.
		elements.buttons.apply.removeClass(classes.hidden);

		// Show abort button.
		elements.buttons.abort.removeClass(classes.hidden);

		// Hide delete button.
		elements.buttons.delete.addClass(classes.hidden);

		// Show action button container.
		elements.containers.actionButtons.removeClass(classes.hidden);

		// Hide image container.
		elements.containers.image.addClass(classes.hidden);

		// Show canvas container.
		elements.containers.canvas.removeClass(classes.hidden);

		// Remove existing canvas element.
		elements.extension.remove();

		// Add newly created canvas extension element.
		elements.containers.canvas.append($extension);

		// Assign new element reference.
		elements.extension = $extension;

		// Initialize extension.
		gx.extensions.init($extension);

		// Insert text into link title input and focus to that element.
		elements.inputs.linkTitle.val(jse.core.lang.translate('NEW_LINKED_AREA', 'sliders')).trigger('focus').trigger('select');

		// Show reset button.
		elements.buttons.reset.removeClass(classes.hidden);

		// Display information alert-box. 
		elements.alerts.info.removeClass('hidden');
	}

	/**
  * Switches to the image area edit view.
  */
	function _switchToEditView() {
		// Index of the selected option (subtracted 1 to be compatible with data container list element).
		var optionIndex = elements.inputs.area.find('option:selected').index() - 1;

		// Corresponding list item in the data container list element.
		var $listItem = $list.children().eq(optionIndex);

		// Create new draw extension element.
		var $extension = $('<div\n\t\t\t\tid="' + ids.canvas + '"\n\t\t\t\tdata-gx-extension="canvas_area_draw"\n\t\t\t\tdata-canvas_area_draw-image-url="' + imageUrl + '"\n\t\t\t\tdata-canvas_area_draw-coordinates="' + $listItem.data('coordinates') + '"\n\t\t\t>');

		// Enable the link title input element and assign value.
		elements.inputs.linkTitle.prop('disabled', false).val($listItem.data('linkTitle'));

		// Enable the link URL input element and assign value.
		elements.inputs.linkUrl.prop('disabled', false).val($listItem.data('linkUrl'));

		// Enable the link target input element and assign value.
		elements.inputs.linkTarget.prop('disabled', false).val($listItem.data('linkTarget'));

		// Disable area dropdown.
		elements.inputs.area.prop('disabled', true);

		// Show apply button.
		elements.buttons.apply.removeClass(classes.hidden);

		// Show abort button.
		elements.buttons.abort.removeClass(classes.hidden);

		// Show delete button.
		elements.buttons.delete.removeClass(classes.hidden);

		// Hide create button.
		elements.buttons.create.addClass(classes.hidden);

		// Show action button container.
		elements.containers.actionButtons.removeClass(classes.hidden);

		// Hide image container.
		elements.containers.image.addClass(classes.hidden);

		// Show canvas container.
		elements.containers.canvas.removeClass(classes.hidden);

		// Remove existing canvas element.
		elements.extension.remove();

		// Add newly created canvas extension element.
		elements.containers.canvas.append($extension);

		// Assign new element reference.
		elements.extension = $extension;

		// Initialize extension.
		gx.extensions.init($extension);

		// Show reset button.
		elements.buttons.reset.removeClass(classes.hidden);

		// Display information alert-box. 
		elements.alerts.info.removeClass('hidden');
	}

	/**
  * Handles the 'input' event on the link title input field.
  */
	function _onLinkTitleInput() {
		// Link title input value.
		var linkTitle = elements.inputs.linkTitle.val();

		// Transfer link title value to option text.
		elements.inputs.area.find('option:selected').text(linkTitle);
	}

	/**
  * Switches to a specific image map view, depending on the area dropdown selection.
  */
	function _onSwitchArea() {
		// Selected option element.
		var $selectedOption = elements.inputs.area.find('option:selected');

		// Is the 'please select' selected?
		var isDefaultValueSelected = !$selectedOption.index();

		// Is a newly added option selected?
		var isNewOptionSelected = $selectedOption.hasClass(classes.newOption);

		// If option is selected, then switch to default view.
		// Or if the a new option is selected, switch to new area view.
		// Otherwise switch to edit view.
		if (isDefaultValueSelected) {
			_switchToDefaultView();
		} else if (isNewOptionSelected) {
			_switchToNewView();
		} else {
			_switchToEditView();
		}
	}

	/**
  * Creates a new image area.
  */
	function _onCreate() {
		// Create new option with random value.
		var $option = $('<option>', {
			class: classes.newOption,
			value: Math.random() * Math.pow(10, 5) ^ 1,
			text: jse.core.lang.translate('NEW_LINKED_AREA', 'sliders')
		});

		// Add new option to input.
		elements.inputs.area.append($option);

		// Select new option in dropdown.
		elements.inputs.area.val($option.val());

		// Trigger change event in area dropdown to switch to image area.
		elements.inputs.area.trigger('change');
	}

	/**
  * Handles the 'click' event on the apply button.
  */
	function _onApply() {
		// Selected option.
		var $selected = elements.inputs.area.find('option:selected');

		// Index of the selected option (subtracted 1 to be compatible with data container list element).
		var optionIndex = $selected.index() - 1;

		// Is the image area new?
		var isNewImageArea = $selected.hasClass(classes.newOption);

		// Image map coordinates.
		var coordinates = elements.extension.find(':hidden').val();

		// Create new list item element.
		var $listItem = $('<li\n\t\t\t\tdata-id="0"\n\t\t\t\tdata-link-title="' + elements.inputs.linkTitle.val() + '"\n\t\t\t\tdata-link-url="' + elements.inputs.linkUrl.val() + '"\n\t\t\t\tdata-link-target="' + elements.inputs.linkTarget.val() + '"\n\t\t\t\tdata-coordinates="' + coordinates + '"\n\t\t\t>');

		// Trimmed link title value.
		var linkTitle = elements.inputs.linkTitle.val().trim();

		// Abort and show modal, if link title or coordinates are empty.
		if (!coordinates || !linkTitle) {
			jse.libs.modal.showMessage(jse.core.lang.translate('MISSING_PATH_OR_LINK_TITLE_MODAL_TITLE', 'sliders'), jse.core.lang.translate('MISSING_PATH_OR_LINK_TITLE_MODAL_TEXT', 'sliders'));

			return;
		}

		// Add list item, if the selected image area is new.
		// Otherwise replace the already listed item.
		if (isNewImageArea) {
			// Remove new option class.
			$selected.removeClass(classes.newOption);

			// Add list item to data container list element.
			$list.append($listItem);
		} else {
			// Replace data container list item with created one.
			$list.children().eq(optionIndex).replaceWith($listItem);
		}

		// Select 'please select' dropdown item.
		elements.inputs.area.val(values.nil);

		// Trigger 'change' event to get to the default view.
		elements.inputs.area.trigger('change');
	}

	/**
  * Handles the 'click' event on the abort button.
  */
	function _onAbort() {
		// Selected option.
		var $selected = elements.inputs.area.find('option:selected');

		// Is the image area new?
		var isNewImageArea = $selected.hasClass(classes.newOption);

		// Remove option from area dropdown, if the selected image area is new.
		// Otherwise the area dropdown will be refilled.
		if (isNewImageArea) {
			$selected.remove();
		} else {
			_fillAreaDropdown();
		}

		// Select 'please select' dropdown item.
		elements.inputs.area.val(values.nil);

		// Trigger 'change' event to get to the default view.
		elements.inputs.area.trigger('change');
	}

	/**
  * Handles the 'click' event on the delete button.
  */
	function _onDelete() {
		// Selected option.
		var $selected = elements.inputs.area.find('option:selected');

		// Index of the selected option (subtracted 1 to be compatible with data container list element).
		var optionIndex = $selected.index() - 1;

		// Delete data container list item.
		$list.children().eq(optionIndex).remove();

		// Syncronize area dropdown.
		_fillAreaDropdown();

		// Select 'please select' dropdown item.
		elements.inputs.area.val(values.nil);

		// Trigger 'change' event to get to the default view.
		elements.inputs.area.trigger('change');
	}

	/**
  * Handles the 'click' event on the reset button.
  */
	function _onReset() {
		// Trigger the 'reset' event to clear the path.
		elements.extension.trigger('reset');
	}

	/**
  * Handles the 'show' event.
  *
  * @param {jQuery.Event} event Triggered event.
  * @param {jQuery} $listParameter Data container list element.
  * @param {String} imageUrlPath URL to slide image.
  */
	function _onShow(event, $listParameter, imageUrlPath) {
		// Show modal.
		$this.modal('show');

		// Assign data container list element value.
		$list = $listParameter;

		// Assign image URL value.
		imageUrl = imageUrlPath;
	}

	/**
  * Handles the 'click' event.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onClick(event) {
		// Check, whether the create button is clicked.
		if (elements.buttons.create.is(event.target)) {
			_onCreate();
			return;
		}

		// Check, whether the apply button is clicked.
		if (elements.buttons.apply.is(event.target)) {
			_onApply();
			return;
		}

		// Check, whether the abort button is clicked.
		if (elements.buttons.abort.is(event.target)) {
			_onAbort();
			return;
		}

		// Check, whether the delete button is clicked.
		if (elements.buttons.delete.is(event.target)) {
			_onDelete();
			return;
		}

		// Check, whether the reset button is clicked.
		if (elements.buttons.reset.is(event.target)) {
			_onReset();
			return;
		}
	}

	/**
  * Handles the 'change' event.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onChange(event) {
		// Check, whether the area dropdown is changed.
		if (elements.inputs.area.is(event.target)) {
			_onSwitchArea();
		}
	}

	/**
  * Handles the modal shown event, which is triggered by the bootstrap modal plugin.
  */
	function _onModalShown() {
		// Fill the area dropdown with the values from the data container list.
		_fillAreaDropdown();

		// Select 'please select' dropdown item and trigger 'change' event to get to the default view.
		elements.inputs.area.val(values.nil).trigger('change');
	}

	/**
  * Handles the modal hidden event, which is triggered by the bootstrap modal plugin.
  */
	function _onModalHidden() {
		// Select 'please select' dropdown item and trigger 'change' event to get to the default view.
		elements.inputs.area.val(values.nil).trigger('change');
	}

	/**
  * Handles the 'input' event.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onInput(event) {
		// Check, whether the link title is the changed element.
		if (elements.inputs.linkTitle.is(event.target)) {
			_onLinkTitleInput();
		}
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Bind event handlers.
		$this.on('click', _onClick).on('change', _onChange).on('shown.bs.modal', _onModalShown).on('hidden.bs.modal', _onModalHidden).on('show', _onShow).on('input', _onInput);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNsaWRlcnMvZGV0YWlscy9tb2RhbHMvaW1hZ2VfbWFwLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCIkdGhpcyIsIiQiLCJjbGFzc2VzIiwiaGlkZGVuIiwibmV3T3B0aW9uIiwiaWRzIiwiY2FudmFzIiwiZWxlbWVudHMiLCJleHRlbnNpb24iLCJmaW5kIiwiY29udGFpbmVycyIsImltYWdlIiwiYWN0aW9uQnV0dG9ucyIsImlucHV0cyIsImFyZWEiLCJsaW5rVGl0bGUiLCJsaW5rVXJsIiwibGlua1RhcmdldCIsImJ1dHRvbnMiLCJjbG9zZSIsImNyZWF0ZSIsImFib3J0IiwiZGVsZXRlIiwiYXBwbHkiLCJyZXNldCIsImFsZXJ0cyIsImluZm8iLCJ2YWx1ZXMiLCJlbXB0eSIsIm5pbCIsInNhbWVXaW5kb3dUYXJnZXQiLCIkbGlzdCIsImltYWdlVXJsIiwiX2ZpbGxBcmVhRHJvcGRvd24iLCJfdHJhbnNmZXJUb0FyZWFEcm9wZG93biIsImluZGV4IiwiZWxlbWVudCIsIiRvcHRpb24iLCJ0ZXh0IiwiZGF0YXNldCIsInZhbCIsImFwcGVuZFRvIiwiY2hpbGRyZW4iLCJub3QiLCJyZW1vdmUiLCJlYWNoIiwiX3N3aXRjaFRvRGVmYXVsdFZpZXciLCIkaW1hZ2UiLCJwcm9wIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImFwcGVuZCIsIl9zd2l0Y2hUb05ld1ZpZXciLCIkZXh0ZW5zaW9uIiwiZXh0ZW5zaW9ucyIsImluaXQiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsInRyaWdnZXIiLCJfc3dpdGNoVG9FZGl0VmlldyIsIm9wdGlvbkluZGV4IiwiJGxpc3RJdGVtIiwiZXEiLCJkYXRhIiwiX29uTGlua1RpdGxlSW5wdXQiLCJfb25Td2l0Y2hBcmVhIiwiJHNlbGVjdGVkT3B0aW9uIiwiaXNEZWZhdWx0VmFsdWVTZWxlY3RlZCIsImlzTmV3T3B0aW9uU2VsZWN0ZWQiLCJoYXNDbGFzcyIsIl9vbkNyZWF0ZSIsImNsYXNzIiwidmFsdWUiLCJNYXRoIiwicmFuZG9tIiwicG93IiwiX29uQXBwbHkiLCIkc2VsZWN0ZWQiLCJpc05ld0ltYWdlQXJlYSIsImNvb3JkaW5hdGVzIiwidHJpbSIsImxpYnMiLCJtb2RhbCIsInNob3dNZXNzYWdlIiwicmVwbGFjZVdpdGgiLCJfb25BYm9ydCIsIl9vbkRlbGV0ZSIsIl9vblJlc2V0IiwiX29uU2hvdyIsImV2ZW50IiwiJGxpc3RQYXJhbWV0ZXIiLCJpbWFnZVVybFBhdGgiLCJfb25DbGljayIsImlzIiwidGFyZ2V0IiwiX29uQ2hhbmdlIiwiX29uTW9kYWxTaG93biIsIl9vbk1vZGFsSGlkZGVuIiwiX29uSW5wdXQiLCJvbiIsImRvbmUiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7O0FBTUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFdBREQsRUFHQyxDQUFJQyxJQUFJQyxNQUFSLGtFQUhELEVBS0MsWUFBVztBQUNWOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVTtBQUNmO0FBQ0FDLFVBQVEsUUFGTzs7QUFJZjtBQUNBQyxhQUFXO0FBTEksRUFBaEI7O0FBUUE7Ozs7O0FBS0EsS0FBTUMsTUFBTTtBQUNYQyxVQUFRO0FBREcsRUFBWjs7QUFJQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCO0FBQ0FDLGFBQVdSLE1BQU1TLElBQU4sT0FBZUosSUFBSUMsTUFBbkIsQ0FGSzs7QUFJaEI7QUFDQUksY0FBWTtBQUNYO0FBQ0FDLFVBQU9YLE1BQU1TLElBQU4sQ0FBVyxZQUFYLENBRkk7O0FBSVg7QUFDQUgsV0FBUU4sTUFBTVMsSUFBTixDQUFXLGFBQVgsQ0FMRzs7QUFPWDtBQUNBRyxrQkFBZVosTUFBTVMsSUFBTixDQUFXLGNBQVg7QUFSSixHQUxJOztBQWdCaEI7QUFDQUksVUFBUTtBQUNQO0FBQ0FDLFNBQU1kLE1BQU1TLElBQU4sQ0FBVyxpQkFBWCxDQUZDOztBQUlQO0FBQ0FNLGNBQVdmLE1BQU1TLElBQU4sQ0FBVyx1QkFBWCxDQUxKOztBQU9QO0FBQ0FPLFlBQVNoQixNQUFNUyxJQUFOLENBQVcscUJBQVgsQ0FSRjs7QUFVUDtBQUNBUSxlQUFZakIsTUFBTVMsSUFBTixDQUFXLHdCQUFYO0FBWEwsR0FqQlE7O0FBK0JoQjtBQUNBUyxXQUFTO0FBQ1I7QUFDQUMsVUFBT25CLE1BQU1TLElBQU4sQ0FBVyxtQkFBWCxDQUZDOztBQUlSO0FBQ0FXLFdBQVFwQixNQUFNUyxJQUFOLENBQVcsb0JBQVgsQ0FMQTs7QUFPUjtBQUNBWSxVQUFPckIsTUFBTVMsSUFBTixDQUFXLG1CQUFYLENBUkM7O0FBVVI7QUFDQWEsV0FBUXRCLE1BQU1TLElBQU4sQ0FBVyxvQkFBWCxDQVhBOztBQWFSO0FBQ0FjLFVBQU92QixNQUFNUyxJQUFOLENBQVcsbUJBQVgsQ0FkQzs7QUFnQlI7QUFDQWUsVUFBT3hCLE1BQU1TLElBQU4sQ0FBVywyQkFBWDtBQWpCQyxHQWhDTzs7QUFvRGhCO0FBQ0FnQixVQUFRO0FBQ1BDLFNBQU0xQixNQUFNUyxJQUFOLENBQVcsUUFBWDtBQURDO0FBckRRLEVBQWpCOztBQTBEQTs7Ozs7QUFLQSxLQUFNa0IsU0FBUztBQUNkO0FBQ0FDLFNBQU8sRUFGTzs7QUFJZDtBQUNBQyxPQUFLLEVBTFM7O0FBT2Q7QUFDQUMsb0JBQWtCO0FBUkosRUFBZjs7QUFXQTs7Ozs7QUFLQSxLQUFNakMsU0FBUyxFQUFmOztBQUVBOzs7OztBQUtBLEtBQUlrQyxRQUFRLElBQVo7O0FBRUE7Ozs7O0FBS0EsS0FBSUMsV0FBVyxJQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0EsVUFBU0MsaUJBQVQsR0FBNkI7QUFDNUI7Ozs7OztBQU1BLE1BQU1DLDBCQUEwQixTQUExQkEsdUJBQTBCLENBQUNDLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUNuRDtBQUNBLE9BQU1DLFVBQVVwQyxFQUFFLFVBQUYsQ0FBaEI7O0FBRUE7QUFDQW9DLFdBQ0VDLElBREYsQ0FDT0YsUUFBUUcsT0FBUixDQUFnQnhCLFNBRHZCLEVBRUV5QixHQUZGLENBRU1MLEtBRk4sRUFHRU0sUUFIRixDQUdXbEMsU0FBU00sTUFBVCxDQUFnQkMsSUFIM0I7QUFJQSxHQVREOztBQVdBO0FBQ0FQLFdBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQ0U0QixRQURGLEdBRUVDLEdBRkYsQ0FFTSxRQUZOLEVBR0VDLE1BSEY7O0FBS0E7QUFDQWIsUUFBTVcsUUFBTixHQUNFRyxJQURGLENBQ09YLHVCQURQO0FBRUE7O0FBRUQ7OztBQUdBLFVBQVNZLG9CQUFULEdBQWdDO0FBQy9CO0FBQ0EsTUFBTUMsU0FBUzlDLGlCQUFlK0IsUUFBZixRQUFmOztBQUVBO0FBQ0F6QixXQUFTTSxNQUFULENBQWdCQyxJQUFoQixDQUNFa0MsSUFERixDQUNPLFVBRFAsRUFDbUIsS0FEbkI7O0FBR0E7QUFDQXpDLFdBQVNNLE1BQVQsQ0FBZ0JFLFNBQWhCLENBQ0VpQyxJQURGLENBQ08sVUFEUCxFQUNtQixJQURuQixFQUVFUixHQUZGLENBRU1iLE9BQU9DLEtBRmI7O0FBSUE7QUFDQXJCLFdBQVNNLE1BQVQsQ0FBZ0JHLE9BQWhCLENBQ0VnQyxJQURGLENBQ08sVUFEUCxFQUNtQixJQURuQixFQUVFUixHQUZGLENBRU1iLE9BQU9DLEtBRmI7O0FBSUE7QUFDQXJCLFdBQVNNLE1BQVQsQ0FBZ0JJLFVBQWhCLENBQ0UrQixJQURGLENBQ08sVUFEUCxFQUNtQixJQURuQixFQUVFUixHQUZGLENBRU1iLE9BQU9HLGdCQUZiOztBQUlBO0FBQ0F2QixXQUFTRyxVQUFULENBQW9CRSxhQUFwQixDQUNFcUMsUUFERixDQUNXL0MsUUFBUUMsTUFEbkI7O0FBR0E7QUFDQUksV0FBU0csVUFBVCxDQUFvQkMsS0FBcEIsQ0FDRWlCLEtBREYsR0FFRXNCLFdBRkYsQ0FFY2hELFFBQVFDLE1BRnRCLEVBR0VnRCxNQUhGLENBR1NKLE1BSFQ7O0FBS0E7QUFDQXhDLFdBQVNHLFVBQVQsQ0FBb0JKLE1BQXBCLENBQ0UyQyxRQURGLENBQ1cvQyxRQUFRQyxNQURuQjs7QUFHQTtBQUNBSSxXQUFTQyxTQUFULENBQW1Cb0IsS0FBbkI7O0FBRUE7QUFDQXJCLFdBQVNXLE9BQVQsQ0FBaUJFLE1BQWpCLENBQ0U4QixXQURGLENBQ2NoRCxRQUFRQyxNQUR0Qjs7QUFHQTtBQUNBSSxXQUFTVyxPQUFULENBQWlCTSxLQUFqQixDQUNFeUIsUUFERixDQUNXL0MsUUFBUUMsTUFEbkI7O0FBR0E7QUFDQUksV0FBU2tCLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCdUIsUUFBckIsQ0FBOEIsUUFBOUI7QUFDQTs7QUFFRDs7O0FBR0EsVUFBU0csZ0JBQVQsR0FBNEI7QUFDM0I7QUFDQSxNQUFNQyxhQUFhcEQseUJBQ1pJLElBQUlDLE1BRFEsa0dBR2lCMEIsUUFIakIsUUFBbkI7O0FBS0E7QUFDQXpCLFdBQVNNLE1BQVQsQ0FBZ0JFLFNBQWhCLENBQ0VpQyxJQURGLENBQ08sVUFEUCxFQUNtQixLQURuQjs7QUFHQTtBQUNBekMsV0FBU00sTUFBVCxDQUFnQkcsT0FBaEIsQ0FDRWdDLElBREYsQ0FDTyxVQURQLEVBQ21CLEtBRG5COztBQUdBO0FBQ0F6QyxXQUFTTSxNQUFULENBQWdCSSxVQUFoQixDQUNFK0IsSUFERixDQUNPLFVBRFAsRUFDbUIsS0FEbkIsRUFFRVIsR0FGRixDQUVNYixPQUFPRyxnQkFGYjs7QUFJQTtBQUNBdkIsV0FBU00sTUFBVCxDQUFnQkMsSUFBaEIsQ0FDRWtDLElBREYsQ0FDTyxVQURQLEVBQ21CLElBRG5COztBQUdBO0FBQ0F6QyxXQUFTVyxPQUFULENBQWlCRSxNQUFqQixDQUNFNkIsUUFERixDQUNXL0MsUUFBUUMsTUFEbkI7O0FBR0E7QUFDQUksV0FBU1csT0FBVCxDQUFpQkssS0FBakIsQ0FDRTJCLFdBREYsQ0FDY2hELFFBQVFDLE1BRHRCOztBQUdBO0FBQ0FJLFdBQVNXLE9BQVQsQ0FBaUJHLEtBQWpCLENBQ0U2QixXQURGLENBQ2NoRCxRQUFRQyxNQUR0Qjs7QUFHQTtBQUNBSSxXQUFTVyxPQUFULENBQWlCSSxNQUFqQixDQUNFMkIsUUFERixDQUNXL0MsUUFBUUMsTUFEbkI7O0FBR0E7QUFDQUksV0FBU0csVUFBVCxDQUFvQkUsYUFBcEIsQ0FDRXNDLFdBREYsQ0FDY2hELFFBQVFDLE1BRHRCOztBQUdBO0FBQ0FJLFdBQVNHLFVBQVQsQ0FBb0JDLEtBQXBCLENBQ0VzQyxRQURGLENBQ1cvQyxRQUFRQyxNQURuQjs7QUFHQTtBQUNBSSxXQUFTRyxVQUFULENBQW9CSixNQUFwQixDQUNFNEMsV0FERixDQUNjaEQsUUFBUUMsTUFEdEI7O0FBR0E7QUFDQUksV0FBU0MsU0FBVCxDQUNFb0MsTUFERjs7QUFHQTtBQUNBckMsV0FBU0csVUFBVCxDQUFvQkosTUFBcEIsQ0FDRTZDLE1BREYsQ0FDU0UsVUFEVDs7QUFHQTtBQUNBOUMsV0FBU0MsU0FBVCxHQUFxQjZDLFVBQXJCOztBQUVBO0FBQ0ExRCxLQUFHMkQsVUFBSCxDQUFjQyxJQUFkLENBQW1CRixVQUFuQjs7QUFFQTtBQUNBOUMsV0FBU00sTUFBVCxDQUFnQkUsU0FBaEIsQ0FDRXlCLEdBREYsQ0FDTTFDLElBQUkwRCxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixpQkFBeEIsRUFBMkMsU0FBM0MsQ0FETixFQUVFQyxPQUZGLENBRVUsT0FGVixFQUdFQSxPQUhGLENBR1UsUUFIVjs7QUFLQTtBQUNBcEQsV0FBU1csT0FBVCxDQUFpQk0sS0FBakIsQ0FDRTBCLFdBREYsQ0FDY2hELFFBQVFDLE1BRHRCOztBQUdBO0FBQ0FJLFdBQVNrQixNQUFULENBQWdCQyxJQUFoQixDQUFxQndCLFdBQXJCLENBQWlDLFFBQWpDO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNVLGlCQUFULEdBQTZCO0FBQzVCO0FBQ0EsTUFBTUMsY0FBY3RELFNBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCTCxJQUFyQixDQUEwQixpQkFBMUIsRUFBNkMwQixLQUE3QyxLQUF1RCxDQUEzRTs7QUFFQTtBQUNBLE1BQU0yQixZQUFZL0IsTUFDaEJXLFFBRGdCLEdBRWhCcUIsRUFGZ0IsQ0FFYkYsV0FGYSxDQUFsQjs7QUFJQTtBQUNBLE1BQU1SLGFBQWFwRCx5QkFDWkksSUFBSUMsTUFEUSxrR0FHaUIwQixRQUhqQixzREFJbUI4QixVQUFVRSxJQUFWLENBQWUsYUFBZixDQUpuQixnQkFBbkI7O0FBT0E7QUFDQXpELFdBQVNNLE1BQVQsQ0FBZ0JFLFNBQWhCLENBQ0VpQyxJQURGLENBQ08sVUFEUCxFQUNtQixLQURuQixFQUVFUixHQUZGLENBRU1zQixVQUFVRSxJQUFWLENBQWUsV0FBZixDQUZOOztBQUlBO0FBQ0F6RCxXQUFTTSxNQUFULENBQWdCRyxPQUFoQixDQUNFZ0MsSUFERixDQUNPLFVBRFAsRUFDbUIsS0FEbkIsRUFFRVIsR0FGRixDQUVNc0IsVUFBVUUsSUFBVixDQUFlLFNBQWYsQ0FGTjs7QUFJQTtBQUNBekQsV0FBU00sTUFBVCxDQUFnQkksVUFBaEIsQ0FDRStCLElBREYsQ0FDTyxVQURQLEVBQ21CLEtBRG5CLEVBRUVSLEdBRkYsQ0FFTXNCLFVBQVVFLElBQVYsQ0FBZSxZQUFmLENBRk47O0FBSUE7QUFDQXpELFdBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQ0VrQyxJQURGLENBQ08sVUFEUCxFQUNtQixJQURuQjs7QUFHQTtBQUNBekMsV0FBU1csT0FBVCxDQUFpQkssS0FBakIsQ0FDRTJCLFdBREYsQ0FDY2hELFFBQVFDLE1BRHRCOztBQUdBO0FBQ0FJLFdBQVNXLE9BQVQsQ0FBaUJHLEtBQWpCLENBQ0U2QixXQURGLENBQ2NoRCxRQUFRQyxNQUR0Qjs7QUFHQTtBQUNBSSxXQUFTVyxPQUFULENBQWlCSSxNQUFqQixDQUNFNEIsV0FERixDQUNjaEQsUUFBUUMsTUFEdEI7O0FBR0E7QUFDQUksV0FBU1csT0FBVCxDQUFpQkUsTUFBakIsQ0FDRTZCLFFBREYsQ0FDVy9DLFFBQVFDLE1BRG5COztBQUdBO0FBQ0FJLFdBQVNHLFVBQVQsQ0FBb0JFLGFBQXBCLENBQ0VzQyxXQURGLENBQ2NoRCxRQUFRQyxNQUR0Qjs7QUFHQTtBQUNBSSxXQUFTRyxVQUFULENBQW9CQyxLQUFwQixDQUNFc0MsUUFERixDQUNXL0MsUUFBUUMsTUFEbkI7O0FBR0E7QUFDQUksV0FBU0csVUFBVCxDQUFvQkosTUFBcEIsQ0FDRTRDLFdBREYsQ0FDY2hELFFBQVFDLE1BRHRCOztBQUdBO0FBQ0FJLFdBQVNDLFNBQVQsQ0FDRW9DLE1BREY7O0FBR0E7QUFDQXJDLFdBQVNHLFVBQVQsQ0FBb0JKLE1BQXBCLENBQ0U2QyxNQURGLENBQ1NFLFVBRFQ7O0FBR0E7QUFDQTlDLFdBQVNDLFNBQVQsR0FBcUI2QyxVQUFyQjs7QUFFQTtBQUNBMUQsS0FBRzJELFVBQUgsQ0FBY0MsSUFBZCxDQUFtQkYsVUFBbkI7O0FBRUE7QUFDQTlDLFdBQVNXLE9BQVQsQ0FBaUJNLEtBQWpCLENBQ0UwQixXQURGLENBQ2NoRCxRQUFRQyxNQUR0Qjs7QUFHQTtBQUNBSSxXQUFTa0IsTUFBVCxDQUFnQkMsSUFBaEIsQ0FBcUJ3QixXQUFyQixDQUFpQyxRQUFqQztBQUNBOztBQUVEOzs7QUFHQSxVQUFTZSxpQkFBVCxHQUE2QjtBQUM1QjtBQUNBLE1BQU1sRCxZQUFZUixTQUFTTSxNQUFULENBQWdCRSxTQUFoQixDQUEwQnlCLEdBQTFCLEVBQWxCOztBQUVBO0FBQ0FqQyxXQUFTTSxNQUFULENBQWdCQyxJQUFoQixDQUFxQkwsSUFBckIsQ0FBMEIsaUJBQTFCLEVBQTZDNkIsSUFBN0MsQ0FBa0R2QixTQUFsRDtBQUNBOztBQUVEOzs7QUFHQSxVQUFTbUQsYUFBVCxHQUF5QjtBQUN4QjtBQUNBLE1BQU1DLGtCQUFrQjVELFNBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCTCxJQUFyQixDQUEwQixpQkFBMUIsQ0FBeEI7O0FBRUE7QUFDQSxNQUFNMkQseUJBQXlCLENBQUNELGdCQUFnQmhDLEtBQWhCLEVBQWhDOztBQUVBO0FBQ0EsTUFBTWtDLHNCQUFzQkYsZ0JBQWdCRyxRQUFoQixDQUF5QnBFLFFBQVFFLFNBQWpDLENBQTVCOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BQUlnRSxzQkFBSixFQUE0QjtBQUMzQnRCO0FBQ0EsR0FGRCxNQUVPLElBQUl1QixtQkFBSixFQUF5QjtBQUMvQmpCO0FBQ0EsR0FGTSxNQUVBO0FBQ05RO0FBQ0E7QUFDRDs7QUFFRDs7O0FBR0EsVUFBU1csU0FBVCxHQUFxQjtBQUNwQjtBQUNBLE1BQU1sQyxVQUFVcEMsRUFBRSxVQUFGLEVBQWM7QUFDN0J1RSxVQUFPdEUsUUFBUUUsU0FEYztBQUU3QnFFLFVBQU9DLEtBQUtDLE1BQUwsS0FBZ0JELEtBQUtFLEdBQUwsQ0FBUyxFQUFULEVBQWEsQ0FBYixDQUFoQixHQUFrQyxDQUZaO0FBRzdCdEMsU0FBTXhDLElBQUkwRCxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixpQkFBeEIsRUFBMkMsU0FBM0M7QUFIdUIsR0FBZCxDQUFoQjs7QUFNQTtBQUNBbkQsV0FBU00sTUFBVCxDQUFnQkMsSUFBaEIsQ0FBcUJxQyxNQUFyQixDQUE0QmQsT0FBNUI7O0FBRUE7QUFDQTlCLFdBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCMEIsR0FBckIsQ0FBeUJILFFBQVFHLEdBQVIsRUFBekI7O0FBRUE7QUFDQWpDLFdBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCNkMsT0FBckIsQ0FBNkIsUUFBN0I7QUFDQTs7QUFFRDs7O0FBR0EsVUFBU2tCLFFBQVQsR0FBb0I7QUFDbkI7QUFDQSxNQUFNQyxZQUFZdkUsU0FBU00sTUFBVCxDQUFnQkMsSUFBaEIsQ0FBcUJMLElBQXJCLENBQTBCLGlCQUExQixDQUFsQjs7QUFFQTtBQUNBLE1BQU1vRCxjQUFjaUIsVUFBVTNDLEtBQVYsS0FBb0IsQ0FBeEM7O0FBRUE7QUFDQSxNQUFNNEMsaUJBQWlCRCxVQUFVUixRQUFWLENBQW1CcEUsUUFBUUUsU0FBM0IsQ0FBdkI7O0FBRUE7QUFDQSxNQUFNNEUsY0FBY3pFLFNBQVNDLFNBQVQsQ0FBbUJDLElBQW5CLENBQXdCLFNBQXhCLEVBQW1DK0IsR0FBbkMsRUFBcEI7O0FBRUE7QUFDQSxNQUFNc0IsWUFBWTdELDBEQUVFTSxTQUFTTSxNQUFULENBQWdCRSxTQUFoQixDQUEwQnlCLEdBQTFCLEVBRkYsa0NBR0FqQyxTQUFTTSxNQUFULENBQWdCRyxPQUFoQixDQUF3QndCLEdBQXhCLEVBSEEscUNBSUdqQyxTQUFTTSxNQUFULENBQWdCSSxVQUFoQixDQUEyQnVCLEdBQTNCLEVBSkgscUNBS0d3QyxXQUxILGdCQUFsQjs7QUFRQTtBQUNBLE1BQU1qRSxZQUFZUixTQUFTTSxNQUFULENBQWdCRSxTQUFoQixDQUEwQnlCLEdBQTFCLEdBQWdDeUMsSUFBaEMsRUFBbEI7O0FBRUE7QUFDQSxNQUFJLENBQUNELFdBQUQsSUFBZ0IsQ0FBQ2pFLFNBQXJCLEVBQWdDO0FBQy9CakIsT0FBSW9GLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxXQUFmLENBQ0N0RixJQUFJMEQsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isd0NBQXhCLEVBQWtFLFNBQWxFLENBREQsRUFFQzVELElBQUkwRCxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix1Q0FBeEIsRUFBaUUsU0FBakUsQ0FGRDs7QUFLQTtBQUNBOztBQUVEO0FBQ0E7QUFDQSxNQUFJcUIsY0FBSixFQUFvQjtBQUNuQjtBQUNBRCxhQUFVNUIsV0FBVixDQUFzQmhELFFBQVFFLFNBQTlCOztBQUVBO0FBQ0EyQixTQUFNb0IsTUFBTixDQUFhVyxTQUFiO0FBQ0EsR0FORCxNQU1PO0FBQ047QUFDQS9CLFNBQU1XLFFBQU4sR0FBaUJxQixFQUFqQixDQUFvQkYsV0FBcEIsRUFBaUN3QixXQUFqQyxDQUE2Q3ZCLFNBQTdDO0FBQ0E7O0FBRUQ7QUFDQXZELFdBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCMEIsR0FBckIsQ0FBeUJiLE9BQU9FLEdBQWhDOztBQUVBO0FBQ0F0QixXQUFTTSxNQUFULENBQWdCQyxJQUFoQixDQUFxQjZDLE9BQXJCLENBQTZCLFFBQTdCO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVMyQixRQUFULEdBQW9CO0FBQ25CO0FBQ0EsTUFBTVIsWUFBWXZFLFNBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCTCxJQUFyQixDQUEwQixpQkFBMUIsQ0FBbEI7O0FBRUE7QUFDQSxNQUFNc0UsaUJBQWlCRCxVQUFVUixRQUFWLENBQW1CcEUsUUFBUUUsU0FBM0IsQ0FBdkI7O0FBRUE7QUFDQTtBQUNBLE1BQUkyRSxjQUFKLEVBQW9CO0FBQ25CRCxhQUFVbEMsTUFBVjtBQUNBLEdBRkQsTUFFTztBQUNOWDtBQUNBOztBQUVEO0FBQ0ExQixXQUFTTSxNQUFULENBQWdCQyxJQUFoQixDQUFxQjBCLEdBQXJCLENBQXlCYixPQUFPRSxHQUFoQzs7QUFFQTtBQUNBdEIsV0FBU00sTUFBVCxDQUFnQkMsSUFBaEIsQ0FBcUI2QyxPQUFyQixDQUE2QixRQUE3QjtBQUNBOztBQUVEOzs7QUFHQSxVQUFTNEIsU0FBVCxHQUFxQjtBQUNwQjtBQUNBLE1BQU1ULFlBQVl2RSxTQUFTTSxNQUFULENBQWdCQyxJQUFoQixDQUFxQkwsSUFBckIsQ0FBMEIsaUJBQTFCLENBQWxCOztBQUVBO0FBQ0EsTUFBTW9ELGNBQWNpQixVQUFVM0MsS0FBVixLQUFvQixDQUF4Qzs7QUFFQTtBQUNBSixRQUFNVyxRQUFOLEdBQWlCcUIsRUFBakIsQ0FBb0JGLFdBQXBCLEVBQWlDakIsTUFBakM7O0FBRUE7QUFDQVg7O0FBRUE7QUFDQTFCLFdBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCMEIsR0FBckIsQ0FBeUJiLE9BQU9FLEdBQWhDOztBQUVBO0FBQ0F0QixXQUFTTSxNQUFULENBQWdCQyxJQUFoQixDQUFxQjZDLE9BQXJCLENBQTZCLFFBQTdCO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVM2QixRQUFULEdBQW9CO0FBQ25CO0FBQ0FqRixXQUFTQyxTQUFULENBQW1CbUQsT0FBbkIsQ0FBMkIsT0FBM0I7QUFDQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVM4QixPQUFULENBQWlCQyxLQUFqQixFQUF3QkMsY0FBeEIsRUFBd0NDLFlBQXhDLEVBQXNEO0FBQ3JEO0FBQ0E1RixRQUFNbUYsS0FBTixDQUFZLE1BQVo7O0FBRUE7QUFDQXBELFVBQVE0RCxjQUFSOztBQUVBO0FBQ0EzRCxhQUFXNEQsWUFBWDtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNDLFFBQVQsQ0FBa0JILEtBQWxCLEVBQXlCO0FBQ3hCO0FBQ0EsTUFBSW5GLFNBQVNXLE9BQVQsQ0FBaUJFLE1BQWpCLENBQXdCMEUsRUFBeEIsQ0FBMkJKLE1BQU1LLE1BQWpDLENBQUosRUFBOEM7QUFDN0N4QjtBQUNBO0FBQ0E7O0FBRUQ7QUFDQSxNQUFJaEUsU0FBU1csT0FBVCxDQUFpQkssS0FBakIsQ0FBdUJ1RSxFQUF2QixDQUEwQkosTUFBTUssTUFBaEMsQ0FBSixFQUE2QztBQUM1Q2xCO0FBQ0E7QUFDQTs7QUFFRDtBQUNBLE1BQUl0RSxTQUFTVyxPQUFULENBQWlCRyxLQUFqQixDQUF1QnlFLEVBQXZCLENBQTBCSixNQUFNSyxNQUFoQyxDQUFKLEVBQTZDO0FBQzVDVDtBQUNBO0FBQ0E7O0FBRUQ7QUFDQSxNQUFJL0UsU0FBU1csT0FBVCxDQUFpQkksTUFBakIsQ0FBd0J3RSxFQUF4QixDQUEyQkosTUFBTUssTUFBakMsQ0FBSixFQUE4QztBQUM3Q1I7QUFDQTtBQUNBOztBQUVEO0FBQ0EsTUFBSWhGLFNBQVNXLE9BQVQsQ0FBaUJNLEtBQWpCLENBQXVCc0UsRUFBdkIsQ0FBMEJKLE1BQU1LLE1BQWhDLENBQUosRUFBNkM7QUFDNUNQO0FBQ0E7QUFDQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVNRLFNBQVQsQ0FBbUJOLEtBQW5CLEVBQTBCO0FBQ3pCO0FBQ0EsTUFBSW5GLFNBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQXFCZ0YsRUFBckIsQ0FBd0JKLE1BQU1LLE1BQTlCLENBQUosRUFBMkM7QUFDMUM3QjtBQUNBO0FBQ0Q7O0FBRUQ7OztBQUdBLFVBQVMrQixhQUFULEdBQXlCO0FBQ3hCO0FBQ0FoRTs7QUFFQTtBQUNBMUIsV0FBU00sTUFBVCxDQUFnQkMsSUFBaEIsQ0FDRTBCLEdBREYsQ0FDTWIsT0FBT0UsR0FEYixFQUVFOEIsT0FGRixDQUVVLFFBRlY7QUFHQTs7QUFFRDs7O0FBR0EsVUFBU3VDLGNBQVQsR0FBMEI7QUFDekI7QUFDQTNGLFdBQVNNLE1BQVQsQ0FBZ0JDLElBQWhCLENBQ0UwQixHQURGLENBQ01iLE9BQU9FLEdBRGIsRUFFRThCLE9BRkYsQ0FFVSxRQUZWO0FBR0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU3dDLFFBQVQsQ0FBa0JULEtBQWxCLEVBQXlCO0FBQ3hCO0FBQ0EsTUFBSW5GLFNBQVNNLE1BQVQsQ0FBZ0JFLFNBQWhCLENBQTBCK0UsRUFBMUIsQ0FBNkJKLE1BQU1LLE1BQW5DLENBQUosRUFBZ0Q7QUFDL0M5QjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBcEUsUUFBTzBELElBQVAsR0FBYyxnQkFBUTtBQUNyQjtBQUNBdkQsUUFDRW9HLEVBREYsQ0FDSyxPQURMLEVBQ2NQLFFBRGQsRUFFRU8sRUFGRixDQUVLLFFBRkwsRUFFZUosU0FGZixFQUdFSSxFQUhGLENBR0ssZ0JBSEwsRUFHdUJILGFBSHZCLEVBSUVHLEVBSkYsQ0FJSyxpQkFKTCxFQUl3QkYsY0FKeEIsRUFLRUUsRUFMRixDQUtLLE1BTEwsRUFLYVgsT0FMYixFQU1FVyxFQU5GLENBTUssT0FOTCxFQU1jRCxRQU5kOztBQVFBO0FBQ0FFO0FBQ0EsRUFaRDs7QUFjQSxRQUFPeEcsTUFBUDtBQUNBLENBeHJCRiIsImZpbGUiOiJzbGlkZXJzL2RldGFpbHMvbW9kYWxzL2ltYWdlX21hcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gaW1hZ2VfbWFwLmpzIDIwMTYtMTEtMjJcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIENvbnRyb2xsZXIgTW9kdWxlIEZvciBTbGlkZSBJbWFnZSBNYXAgTW9kYWwuXG4gKlxuICogUHVibGljIG1ldGhvZHM6XG4gKiAgLSAnc2hvdycgc2hvd3MgdGhlIGRyb3Bkb3duLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdpbWFnZV9tYXAnLFxuXHRcblx0W2Ake2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktY2FudmFzLWFyZWEtZHJhdy9qcXVlcnkuY2FudmFzQXJlYURyYXcubWluLmpzYF0sXG5cdFxuXHRmdW5jdGlvbigpIHtcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBlbGVtZW50IChtb2RhbCBlbGVtZW50KS5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENTUyBDbGFzcyBNYXBcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgY2xhc3NlcyA9IHtcblx0XHRcdC8vIEhpZGRlbiBjbGFzcy5cblx0XHRcdGhpZGRlbjogJ2hpZGRlbicsXG5cdFx0XHRcblx0XHRcdC8vIE5ldyBpbnNlcnRlZCBvcHRpb24uXG5cdFx0XHRuZXdPcHRpb246ICduZXcnXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDU1MgSUQgTWFwXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGlkcyA9IHtcblx0XHRcdGNhbnZhczogJ2NhbnZhcydcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEVsZW1lbnQgTWFwXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGVsZW1lbnRzID0ge1xuXHRcdFx0Ly8gQ2FudmFzIGV4dGVuc2lvbiBlbGVtZW50LlxuXHRcdFx0ZXh0ZW5zaW9uOiAkdGhpcy5maW5kKGAjJHtpZHMuY2FudmFzfWApLFxuXHRcdFx0XG5cdFx0XHQvLyBDb250YWluZXIgZWxlbWVudHMuXG5cdFx0XHRjb250YWluZXJzOiB7XG5cdFx0XHRcdC8vIEltYWdlIGNvbnRhaW5lci5cblx0XHRcdFx0aW1hZ2U6ICR0aGlzLmZpbmQoJy5yb3cuaW1hZ2UnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIENhbnZhcyBjb250YWluZXIuXG5cdFx0XHRcdGNhbnZhczogJHRoaXMuZmluZCgnLnJvdy5jYW52YXMnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFjdGlvbiBidXR0b25zIGNvbnRhaW5lci5cblx0XHRcdFx0YWN0aW9uQnV0dG9uczogJHRoaXMuZmluZCgnLnJvdy5hY3Rpb25zJylcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8vIEZvcm0gaW5wdXRzLlxuXHRcdFx0aW5wdXRzOiB7XG5cdFx0XHRcdC8vIExpbmsgYXJlYSBkcm9wZG93bi5cblx0XHRcdFx0YXJlYTogJHRoaXMuZmluZCgnI2ltYWdlLW1hcC1hcmVhJyksXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBMaW5rIHRpdGxlLlxuXHRcdFx0XHRsaW5rVGl0bGU6ICR0aGlzLmZpbmQoJyNpbWFnZS1tYXAtbGluay10aXRsZScpLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gTGluayBVUkwuXG5cdFx0XHRcdGxpbmtVcmw6ICR0aGlzLmZpbmQoJyNpbWFnZS1tYXAtbGluay11cmwnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIExpbmsgdGFyZ2V0LlxuXHRcdFx0XHRsaW5rVGFyZ2V0OiAkdGhpcy5maW5kKCcjaW1hZ2UtbWFwLWxpbmstdGFyZ2V0Jylcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8vIEJ1dHRvbnMuXG5cdFx0XHRidXR0b25zOiB7XG5cdFx0XHRcdC8vIENsb3NlIG1vZGFsLlxuXHRcdFx0XHRjbG9zZTogJHRoaXMuZmluZCgnLmJ0bi5hY3Rpb24tY2xvc2UnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIENyZWF0ZSBpbWFnZSBhcmVhLlxuXHRcdFx0XHRjcmVhdGU6ICR0aGlzLmZpbmQoJy5idG4uYWN0aW9uLWNyZWF0ZScpLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQWJvcnQgZWRpdC5cblx0XHRcdFx0YWJvcnQ6ICR0aGlzLmZpbmQoJy5idG4uYWN0aW9uLWFib3J0JyksXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBEZWxldGUgaW1hZ2UgYXJlYS5cblx0XHRcdFx0ZGVsZXRlOiAkdGhpcy5maW5kKCcuYnRuLmFjdGlvbi1kZWxldGUnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFwcGx5IGltYWdlIGFyZWEgY2hhbmdlcy5cblx0XHRcdFx0YXBwbHk6ICR0aGlzLmZpbmQoJy5idG4uYWN0aW9uLWFwcGx5JyksXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBSZXNldCBwYXRoLlxuXHRcdFx0XHRyZXNldDogJHRoaXMuZmluZCgnLmJ0bi1kZWZhdWx0LmFjdGlvbi1yZXNldCcpXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvLyBBbGVydHMgXG5cdFx0XHRhbGVydHM6IHtcblx0XHRcdFx0aW5mbzogJHRoaXMuZmluZCgnLmFsZXJ0Jylcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFZhbHVlIE1hcFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCB2YWx1ZXMgPSB7XG5cdFx0XHQvLyBFbXB0eSBzdHJpbmcuXG5cdFx0XHRlbXB0eTogJycsXG5cdFx0XHRcblx0XHRcdC8vICdQbGVhc2Ugc2VsZWN0JyB2YWx1ZS5cblx0XHRcdG5pbDogJycsXG5cdFx0XHRcblx0XHRcdC8vIE9wZW4gaW4gc2FtZSB3aW5kb3cuXG5cdFx0XHRzYW1lV2luZG93VGFyZ2V0OiAnX3NlbGYnXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGF0YSBjb250YWluZXIgbGlzdC5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl8bnVsbH1cblx0XHQgKi9cblx0XHRsZXQgJGxpc3QgPSBudWxsO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNsaWRlIGltYWdlIFVSTC5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtTdHJpbmd8bnVsbH1cblx0XHQgKi9cblx0XHRsZXQgaW1hZ2VVcmwgPSBudWxsO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBGaWxscyB0aGUgYXJlYSBkcm9wZG93biB3aXRoIHRoZSBkYXRhIGNvbnRhaW5lciBsaXN0IGl0ZW1zLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9maWxsQXJlYURyb3Bkb3duKCkge1xuXHRcdFx0LyoqXG5cdFx0XHQgKiBUcmFuc2ZlcnMgdGhlIGRhdGEgZnJvbSB0aGUgZGF0YSBjb250YWluZXIgbGlzdCBpdGVtIHRvIHRoZSBhcmVhIGRyb3Bkb3duLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwYXJhbSB7TnVtYmVyfSBpbmRleCBJdGVyYXRpb24gaW5kZXguXG5cdFx0XHQgKiBAcGFyYW0ge0VsZW1lbnR9IGVsZW1lbnQgSXRlcmF0aW9uIGVsZW1lbnQuXG5cdFx0XHQgKi9cblx0XHRcdGNvbnN0IF90cmFuc2ZlclRvQXJlYURyb3Bkb3duID0gKGluZGV4LCBlbGVtZW50KSA9PiB7XG5cdFx0XHRcdC8vIENyZWF0ZSBuZXcgb3B0aW9uIGVsZW1lbnQuXG5cdFx0XHRcdGNvbnN0ICRvcHRpb24gPSAkKCc8b3B0aW9uPicpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2V0IHRleHQgY29udGVudCBhbmQgdmFsdWUgYW5kIGFwcGVuZCBvcHRpb24gdG8gYXJlYSBkcm9wZG93bi5cblx0XHRcdFx0JG9wdGlvblxuXHRcdFx0XHRcdC50ZXh0KGVsZW1lbnQuZGF0YXNldC5saW5rVGl0bGUpXG5cdFx0XHRcdFx0LnZhbChpbmRleClcblx0XHRcdFx0XHQuYXBwZW5kVG8oZWxlbWVudHMuaW5wdXRzLmFyZWEpO1xuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gRGVsZXRlIGFsbCBjaGlsZHJlbiwgZXhjZXB0IHRoZSBmaXJzdCBvbmUuXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMuYXJlYVxuXHRcdFx0XHQuY2hpbGRyZW4oKVxuXHRcdFx0XHQubm90KCc6Zmlyc3QnKVxuXHRcdFx0XHQucmVtb3ZlKCk7XG5cdFx0XHRcblx0XHRcdC8vIFRyYW5zZmVyIGRhdGEgY29udGFpbmVyIGxpc3QgaXRlbXMgdG8gYXJlYSBkcm9wZG93bi5cblx0XHRcdCRsaXN0LmNoaWxkcmVuKClcblx0XHRcdFx0LmVhY2goX3RyYW5zZmVyVG9BcmVhRHJvcGRvd24pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTd2l0Y2hlcyB0byB0aGUgZGVmYXVsdCB2aWV3LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9zd2l0Y2hUb0RlZmF1bHRWaWV3KCkge1xuXHRcdFx0Ly8gSW1hZ2UgdGFnLlxuXHRcdFx0Y29uc3QgJGltYWdlID0gJChgPGltZyBzcmM9XCIke2ltYWdlVXJsfVwiPmApO1xuXHRcdFx0XG5cdFx0XHQvLyBFbmFibGUgYXJlYSBkcm9wZG93bi5cblx0XHRcdGVsZW1lbnRzLmlucHV0cy5hcmVhXG5cdFx0XHRcdC5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcblx0XHRcdFxuXHRcdFx0Ly8gRGlzYWJsZSBhbmQgZW1wdHkgdGhlIGxpbmsgdGl0bGUgaW5wdXQuXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMubGlua1RpdGxlXG5cdFx0XHRcdC5wcm9wKCdkaXNhYmxlZCcsIHRydWUpXG5cdFx0XHRcdC52YWwodmFsdWVzLmVtcHR5KTtcblx0XHRcdFxuXHRcdFx0Ly8gRGlzYWJsZSBhbmQgZW1wdHkgdGhlIGxpbmsgVVJMIHRpdGxlIGlucHV0LlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmxpbmtVcmxcblx0XHRcdFx0LnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSlcblx0XHRcdFx0LnZhbCh2YWx1ZXMuZW1wdHkpO1xuXHRcdFx0XG5cdFx0XHQvLyBEaXNhYmxlIGFuZCBlbXB0eSB0aGUgbGluayB0YXJnZXQgdGl0bGUgaW5wdXQuXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMubGlua1RhcmdldFxuXHRcdFx0XHQucHJvcCgnZGlzYWJsZWQnLCB0cnVlKVxuXHRcdFx0XHQudmFsKHZhbHVlcy5zYW1lV2luZG93VGFyZ2V0KTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSBidXR0b24gY29udGFpbmVyLlxuXHRcdFx0ZWxlbWVudHMuY29udGFpbmVycy5hY3Rpb25CdXR0b25zXG5cdFx0XHRcdC5hZGRDbGFzcyhjbGFzc2VzLmhpZGRlbik7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgYW5kIGVtcHR5IGltYWdlIGNvbnRhaW5lciBhbmQgYXBwZW5kIGltYWdlLlxuXHRcdFx0ZWxlbWVudHMuY29udGFpbmVycy5pbWFnZVxuXHRcdFx0XHQuZW1wdHkoKVxuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pXG5cdFx0XHRcdC5hcHBlbmQoJGltYWdlKTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSBjYW52YXMgY29udGFpbmVyLlxuXHRcdFx0ZWxlbWVudHMuY29udGFpbmVycy5jYW52YXNcblx0XHRcdFx0LmFkZENsYXNzKGNsYXNzZXMuaGlkZGVuKTtcblx0XHRcdFxuXHRcdFx0Ly8gRW1wdHkgZXh0ZW5zaW9uIGVsZW1lbnQuXG5cdFx0XHRlbGVtZW50cy5leHRlbnNpb24uZW1wdHkoKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBjcmVhdGUgYnV0dG9uLlxuXHRcdFx0ZWxlbWVudHMuYnV0dG9ucy5jcmVhdGVcblx0XHRcdFx0LnJlbW92ZUNsYXNzKGNsYXNzZXMuaGlkZGVuKTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSByZXNldCBidXR0b24uXG5cdFx0XHRlbGVtZW50cy5idXR0b25zLnJlc2V0XG5cdFx0XHRcdC5hZGRDbGFzcyhjbGFzc2VzLmhpZGRlbik7XG5cdFx0XHRcblx0XHRcdC8vIEhpZGUgaW5mb3JtYXRpb24gYWxlcnQtYm94LiBcblx0XHRcdGVsZW1lbnRzLmFsZXJ0cy5pbmZvLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU3dpdGNoZXMgdG8gdGhlIG5ldyBpbWFnZSBhcmVhIHZpZXcuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3N3aXRjaFRvTmV3VmlldygpIHtcblx0XHRcdC8vIENyZWF0ZSBuZXcgZHJhdyBleHRlbnNpb24gZWxlbWVudC5cblx0XHRcdGNvbnN0ICRleHRlbnNpb24gPSAkKGA8ZGl2XG5cdFx0XHRcdGlkPVwiJHtpZHMuY2FudmFzfVwiXG5cdFx0XHRcdGRhdGEtZ3gtZXh0ZW5zaW9uPVwiY2FudmFzX2FyZWFfZHJhd1wiXG5cdFx0XHRcdGRhdGEtY2FudmFzX2FyZWFfZHJhdy1pbWFnZS11cmw9XCIke2ltYWdlVXJsfVwiPmApO1xuXHRcdFx0XG5cdFx0XHQvLyBFbmFibGUgbGluayB0aXRsZSBpbnB1dC5cblx0XHRcdGVsZW1lbnRzLmlucHV0cy5saW5rVGl0bGVcblx0XHRcdFx0LnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuXHRcdFx0XG5cdFx0XHQvLyBFbmFibGUgbGluayBVUkwgaW5wdXQuXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMubGlua1VybFxuXHRcdFx0XHQucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG5cdFx0XHRcblx0XHRcdC8vIEVuYWJsZSB0aGUgbGluayB0YXJnZXQgaW5wdXQgYW5kIHNldCB0aGUgdmFsdWUgdG8gJ3NlbGYnLlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmxpbmtUYXJnZXRcblx0XHRcdFx0LnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpXG5cdFx0XHRcdC52YWwodmFsdWVzLnNhbWVXaW5kb3dUYXJnZXQpO1xuXHRcdFx0XG5cdFx0XHQvLyBEaXNhYmxlIHRoZSBhcmVhIGRyb3Bkb3duLlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmFyZWFcblx0XHRcdFx0LnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG5cdFx0XHRcblx0XHRcdC8vIEhpZGUgY3JlYXRlIGJ1dHRvbi5cblx0XHRcdGVsZW1lbnRzLmJ1dHRvbnMuY3JlYXRlXG5cdFx0XHRcdC5hZGRDbGFzcyhjbGFzc2VzLmhpZGRlbik7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgYXBwbHkgYnV0dG9uLlxuXHRcdFx0ZWxlbWVudHMuYnV0dG9ucy5hcHBseVxuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IGFib3J0IGJ1dHRvbi5cblx0XHRcdGVsZW1lbnRzLmJ1dHRvbnMuYWJvcnRcblx0XHRcdFx0LnJlbW92ZUNsYXNzKGNsYXNzZXMuaGlkZGVuKTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSBkZWxldGUgYnV0dG9uLlxuXHRcdFx0ZWxlbWVudHMuYnV0dG9ucy5kZWxldGVcblx0XHRcdFx0LmFkZENsYXNzKGNsYXNzZXMuaGlkZGVuKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBhY3Rpb24gYnV0dG9uIGNvbnRhaW5lci5cblx0XHRcdGVsZW1lbnRzLmNvbnRhaW5lcnMuYWN0aW9uQnV0dG9uc1xuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRlIGltYWdlIGNvbnRhaW5lci5cblx0XHRcdGVsZW1lbnRzLmNvbnRhaW5lcnMuaW1hZ2Vcblx0XHRcdFx0LmFkZENsYXNzKGNsYXNzZXMuaGlkZGVuKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBjYW52YXMgY29udGFpbmVyLlxuXHRcdFx0ZWxlbWVudHMuY29udGFpbmVycy5jYW52YXNcblx0XHRcdFx0LnJlbW92ZUNsYXNzKGNsYXNzZXMuaGlkZGVuKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVtb3ZlIGV4aXN0aW5nIGNhbnZhcyBlbGVtZW50LlxuXHRcdFx0ZWxlbWVudHMuZXh0ZW5zaW9uXG5cdFx0XHRcdC5yZW1vdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWRkIG5ld2x5IGNyZWF0ZWQgY2FudmFzIGV4dGVuc2lvbiBlbGVtZW50LlxuXHRcdFx0ZWxlbWVudHMuY29udGFpbmVycy5jYW52YXNcblx0XHRcdFx0LmFwcGVuZCgkZXh0ZW5zaW9uKTtcblx0XHRcdFxuXHRcdFx0Ly8gQXNzaWduIG5ldyBlbGVtZW50IHJlZmVyZW5jZS5cblx0XHRcdGVsZW1lbnRzLmV4dGVuc2lvbiA9ICRleHRlbnNpb247XG5cdFx0XHRcblx0XHRcdC8vIEluaXRpYWxpemUgZXh0ZW5zaW9uLlxuXHRcdFx0Z3guZXh0ZW5zaW9ucy5pbml0KCRleHRlbnNpb24pO1xuXHRcdFx0XG5cdFx0XHQvLyBJbnNlcnQgdGV4dCBpbnRvIGxpbmsgdGl0bGUgaW5wdXQgYW5kIGZvY3VzIHRvIHRoYXQgZWxlbWVudC5cblx0XHRcdGVsZW1lbnRzLmlucHV0cy5saW5rVGl0bGVcblx0XHRcdFx0LnZhbChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnTkVXX0xJTktFRF9BUkVBJywgJ3NsaWRlcnMnKSlcblx0XHRcdFx0LnRyaWdnZXIoJ2ZvY3VzJylcblx0XHRcdFx0LnRyaWdnZXIoJ3NlbGVjdCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IHJlc2V0IGJ1dHRvbi5cblx0XHRcdGVsZW1lbnRzLmJ1dHRvbnMucmVzZXRcblx0XHRcdFx0LnJlbW92ZUNsYXNzKGNsYXNzZXMuaGlkZGVuKTtcblx0XHRcdFxuXHRcdFx0Ly8gRGlzcGxheSBpbmZvcm1hdGlvbiBhbGVydC1ib3guIFxuXHRcdFx0ZWxlbWVudHMuYWxlcnRzLmluZm8ucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTd2l0Y2hlcyB0byB0aGUgaW1hZ2UgYXJlYSBlZGl0IHZpZXcuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3N3aXRjaFRvRWRpdFZpZXcoKSB7XG5cdFx0XHQvLyBJbmRleCBvZiB0aGUgc2VsZWN0ZWQgb3B0aW9uIChzdWJ0cmFjdGVkIDEgdG8gYmUgY29tcGF0aWJsZSB3aXRoIGRhdGEgY29udGFpbmVyIGxpc3QgZWxlbWVudCkuXG5cdFx0XHRjb25zdCBvcHRpb25JbmRleCA9IGVsZW1lbnRzLmlucHV0cy5hcmVhLmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpLmluZGV4KCkgLSAxO1xuXHRcdFx0XG5cdFx0XHQvLyBDb3JyZXNwb25kaW5nIGxpc3QgaXRlbSBpbiB0aGUgZGF0YSBjb250YWluZXIgbGlzdCBlbGVtZW50LlxuXHRcdFx0Y29uc3QgJGxpc3RJdGVtID0gJGxpc3Rcblx0XHRcdFx0LmNoaWxkcmVuKClcblx0XHRcdFx0LmVxKG9wdGlvbkluZGV4KTtcblx0XHRcdFxuXHRcdFx0Ly8gQ3JlYXRlIG5ldyBkcmF3IGV4dGVuc2lvbiBlbGVtZW50LlxuXHRcdFx0Y29uc3QgJGV4dGVuc2lvbiA9ICQoYDxkaXZcblx0XHRcdFx0aWQ9XCIke2lkcy5jYW52YXN9XCJcblx0XHRcdFx0ZGF0YS1neC1leHRlbnNpb249XCJjYW52YXNfYXJlYV9kcmF3XCJcblx0XHRcdFx0ZGF0YS1jYW52YXNfYXJlYV9kcmF3LWltYWdlLXVybD1cIiR7aW1hZ2VVcmx9XCJcblx0XHRcdFx0ZGF0YS1jYW52YXNfYXJlYV9kcmF3LWNvb3JkaW5hdGVzPVwiJHskbGlzdEl0ZW0uZGF0YSgnY29vcmRpbmF0ZXMnKX1cIlxuXHRcdFx0PmApO1xuXHRcdFx0XG5cdFx0XHQvLyBFbmFibGUgdGhlIGxpbmsgdGl0bGUgaW5wdXQgZWxlbWVudCBhbmQgYXNzaWduIHZhbHVlLlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmxpbmtUaXRsZVxuXHRcdFx0XHQucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSlcblx0XHRcdFx0LnZhbCgkbGlzdEl0ZW0uZGF0YSgnbGlua1RpdGxlJykpO1xuXHRcdFx0XG5cdFx0XHQvLyBFbmFibGUgdGhlIGxpbmsgVVJMIGlucHV0IGVsZW1lbnQgYW5kIGFzc2lnbiB2YWx1ZS5cblx0XHRcdGVsZW1lbnRzLmlucHV0cy5saW5rVXJsXG5cdFx0XHRcdC5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKVxuXHRcdFx0XHQudmFsKCRsaXN0SXRlbS5kYXRhKCdsaW5rVXJsJykpO1xuXHRcdFx0XG5cdFx0XHQvLyBFbmFibGUgdGhlIGxpbmsgdGFyZ2V0IGlucHV0IGVsZW1lbnQgYW5kIGFzc2lnbiB2YWx1ZS5cblx0XHRcdGVsZW1lbnRzLmlucHV0cy5saW5rVGFyZ2V0XG5cdFx0XHRcdC5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKVxuXHRcdFx0XHQudmFsKCRsaXN0SXRlbS5kYXRhKCdsaW5rVGFyZ2V0JykpO1xuXHRcdFx0XG5cdFx0XHQvLyBEaXNhYmxlIGFyZWEgZHJvcGRvd24uXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMuYXJlYVxuXHRcdFx0XHQucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBhcHBseSBidXR0b24uXG5cdFx0XHRlbGVtZW50cy5idXR0b25zLmFwcGx5XG5cdFx0XHRcdC5yZW1vdmVDbGFzcyhjbGFzc2VzLmhpZGRlbik7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgYWJvcnQgYnV0dG9uLlxuXHRcdFx0ZWxlbWVudHMuYnV0dG9ucy5hYm9ydFxuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IGRlbGV0ZSBidXR0b24uXG5cdFx0XHRlbGVtZW50cy5idXR0b25zLmRlbGV0ZVxuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRlIGNyZWF0ZSBidXR0b24uXG5cdFx0XHRlbGVtZW50cy5idXR0b25zLmNyZWF0ZVxuXHRcdFx0XHQuYWRkQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IGFjdGlvbiBidXR0b24gY29udGFpbmVyLlxuXHRcdFx0ZWxlbWVudHMuY29udGFpbmVycy5hY3Rpb25CdXR0b25zXG5cdFx0XHRcdC5yZW1vdmVDbGFzcyhjbGFzc2VzLmhpZGRlbik7XG5cdFx0XHRcblx0XHRcdC8vIEhpZGUgaW1hZ2UgY29udGFpbmVyLlxuXHRcdFx0ZWxlbWVudHMuY29udGFpbmVycy5pbWFnZVxuXHRcdFx0XHQuYWRkQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IGNhbnZhcyBjb250YWluZXIuXG5cdFx0XHRlbGVtZW50cy5jb250YWluZXJzLmNhbnZhc1xuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0XG5cdFx0XHQvLyBSZW1vdmUgZXhpc3RpbmcgY2FudmFzIGVsZW1lbnQuXG5cdFx0XHRlbGVtZW50cy5leHRlbnNpb25cblx0XHRcdFx0LnJlbW92ZSgpO1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgbmV3bHkgY3JlYXRlZCBjYW52YXMgZXh0ZW5zaW9uIGVsZW1lbnQuXG5cdFx0XHRlbGVtZW50cy5jb250YWluZXJzLmNhbnZhc1xuXHRcdFx0XHQuYXBwZW5kKCRleHRlbnNpb24pO1xuXHRcdFx0XG5cdFx0XHQvLyBBc3NpZ24gbmV3IGVsZW1lbnQgcmVmZXJlbmNlLlxuXHRcdFx0ZWxlbWVudHMuZXh0ZW5zaW9uID0gJGV4dGVuc2lvbjtcblx0XHRcdFxuXHRcdFx0Ly8gSW5pdGlhbGl6ZSBleHRlbnNpb24uXG5cdFx0XHRneC5leHRlbnNpb25zLmluaXQoJGV4dGVuc2lvbik7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgcmVzZXQgYnV0dG9uLlxuXHRcdFx0ZWxlbWVudHMuYnV0dG9ucy5yZXNldFxuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0XG5cdFx0XHQvLyBEaXNwbGF5IGluZm9ybWF0aW9uIGFsZXJ0LWJveC4gXG5cdFx0XHRlbGVtZW50cy5hbGVydHMuaW5mby5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlICdpbnB1dCcgZXZlbnQgb24gdGhlIGxpbmsgdGl0bGUgaW5wdXQgZmllbGQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uTGlua1RpdGxlSW5wdXQoKSB7XG5cdFx0XHQvLyBMaW5rIHRpdGxlIGlucHV0IHZhbHVlLlxuXHRcdFx0Y29uc3QgbGlua1RpdGxlID0gZWxlbWVudHMuaW5wdXRzLmxpbmtUaXRsZS52YWwoKTtcblx0XHRcdFxuXHRcdFx0Ly8gVHJhbnNmZXIgbGluayB0aXRsZSB2YWx1ZSB0byBvcHRpb24gdGV4dC5cblx0XHRcdGVsZW1lbnRzLmlucHV0cy5hcmVhLmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpLnRleHQobGlua1RpdGxlKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU3dpdGNoZXMgdG8gYSBzcGVjaWZpYyBpbWFnZSBtYXAgdmlldywgZGVwZW5kaW5nIG9uIHRoZSBhcmVhIGRyb3Bkb3duIHNlbGVjdGlvbi5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25Td2l0Y2hBcmVhKCkge1xuXHRcdFx0Ly8gU2VsZWN0ZWQgb3B0aW9uIGVsZW1lbnQuXG5cdFx0XHRjb25zdCAkc2VsZWN0ZWRPcHRpb24gPSBlbGVtZW50cy5pbnB1dHMuYXJlYS5maW5kKCdvcHRpb246c2VsZWN0ZWQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gSXMgdGhlICdwbGVhc2Ugc2VsZWN0JyBzZWxlY3RlZD9cblx0XHRcdGNvbnN0IGlzRGVmYXVsdFZhbHVlU2VsZWN0ZWQgPSAhJHNlbGVjdGVkT3B0aW9uLmluZGV4KCk7XG5cdFx0XHRcblx0XHRcdC8vIElzIGEgbmV3bHkgYWRkZWQgb3B0aW9uIHNlbGVjdGVkP1xuXHRcdFx0Y29uc3QgaXNOZXdPcHRpb25TZWxlY3RlZCA9ICRzZWxlY3RlZE9wdGlvbi5oYXNDbGFzcyhjbGFzc2VzLm5ld09wdGlvbik7XG5cdFx0XHRcblx0XHRcdC8vIElmIG9wdGlvbiBpcyBzZWxlY3RlZCwgdGhlbiBzd2l0Y2ggdG8gZGVmYXVsdCB2aWV3LlxuXHRcdFx0Ly8gT3IgaWYgdGhlIGEgbmV3IG9wdGlvbiBpcyBzZWxlY3RlZCwgc3dpdGNoIHRvIG5ldyBhcmVhIHZpZXcuXG5cdFx0XHQvLyBPdGhlcndpc2Ugc3dpdGNoIHRvIGVkaXQgdmlldy5cblx0XHRcdGlmIChpc0RlZmF1bHRWYWx1ZVNlbGVjdGVkKSB7XG5cdFx0XHRcdF9zd2l0Y2hUb0RlZmF1bHRWaWV3KCk7XG5cdFx0XHR9IGVsc2UgaWYgKGlzTmV3T3B0aW9uU2VsZWN0ZWQpIHtcblx0XHRcdFx0X3N3aXRjaFRvTmV3VmlldygpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0X3N3aXRjaFRvRWRpdFZpZXcoKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlcyBhIG5ldyBpbWFnZSBhcmVhLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkNyZWF0ZSgpIHtcblx0XHRcdC8vIENyZWF0ZSBuZXcgb3B0aW9uIHdpdGggcmFuZG9tIHZhbHVlLlxuXHRcdFx0Y29uc3QgJG9wdGlvbiA9ICQoJzxvcHRpb24+Jywge1xuXHRcdFx0XHRjbGFzczogY2xhc3Nlcy5uZXdPcHRpb24sXG5cdFx0XHRcdHZhbHVlOiBNYXRoLnJhbmRvbSgpICogTWF0aC5wb3coMTAsIDUpIF4gMSxcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ05FV19MSU5LRURfQVJFQScsICdzbGlkZXJzJylcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgbmV3IG9wdGlvbiB0byBpbnB1dC5cblx0XHRcdGVsZW1lbnRzLmlucHV0cy5hcmVhLmFwcGVuZCgkb3B0aW9uKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2VsZWN0IG5ldyBvcHRpb24gaW4gZHJvcGRvd24uXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMuYXJlYS52YWwoJG9wdGlvbi52YWwoKSk7XG5cdFx0XHRcblx0XHRcdC8vIFRyaWdnZXIgY2hhbmdlIGV2ZW50IGluIGFyZWEgZHJvcGRvd24gdG8gc3dpdGNoIHRvIGltYWdlIGFyZWEuXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMuYXJlYS50cmlnZ2VyKCdjaGFuZ2UnKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgJ2NsaWNrJyBldmVudCBvbiB0aGUgYXBwbHkgYnV0dG9uLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkFwcGx5KCkge1xuXHRcdFx0Ly8gU2VsZWN0ZWQgb3B0aW9uLlxuXHRcdFx0Y29uc3QgJHNlbGVjdGVkID0gZWxlbWVudHMuaW5wdXRzLmFyZWEuZmluZCgnb3B0aW9uOnNlbGVjdGVkJyk7XG5cdFx0XHRcblx0XHRcdC8vIEluZGV4IG9mIHRoZSBzZWxlY3RlZCBvcHRpb24gKHN1YnRyYWN0ZWQgMSB0byBiZSBjb21wYXRpYmxlIHdpdGggZGF0YSBjb250YWluZXIgbGlzdCBlbGVtZW50KS5cblx0XHRcdGNvbnN0IG9wdGlvbkluZGV4ID0gJHNlbGVjdGVkLmluZGV4KCkgLSAxO1xuXHRcdFx0XG5cdFx0XHQvLyBJcyB0aGUgaW1hZ2UgYXJlYSBuZXc/XG5cdFx0XHRjb25zdCBpc05ld0ltYWdlQXJlYSA9ICRzZWxlY3RlZC5oYXNDbGFzcyhjbGFzc2VzLm5ld09wdGlvbik7XG5cdFx0XHRcblx0XHRcdC8vIEltYWdlIG1hcCBjb29yZGluYXRlcy5cblx0XHRcdGNvbnN0IGNvb3JkaW5hdGVzID0gZWxlbWVudHMuZXh0ZW5zaW9uLmZpbmQoJzpoaWRkZW4nKS52YWwoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQ3JlYXRlIG5ldyBsaXN0IGl0ZW0gZWxlbWVudC5cblx0XHRcdGNvbnN0ICRsaXN0SXRlbSA9ICQoYDxsaVxuXHRcdFx0XHRkYXRhLWlkPVwiMFwiXG5cdFx0XHRcdGRhdGEtbGluay10aXRsZT1cIiR7ZWxlbWVudHMuaW5wdXRzLmxpbmtUaXRsZS52YWwoKX1cIlxuXHRcdFx0XHRkYXRhLWxpbmstdXJsPVwiJHtlbGVtZW50cy5pbnB1dHMubGlua1VybC52YWwoKX1cIlxuXHRcdFx0XHRkYXRhLWxpbmstdGFyZ2V0PVwiJHtlbGVtZW50cy5pbnB1dHMubGlua1RhcmdldC52YWwoKX1cIlxuXHRcdFx0XHRkYXRhLWNvb3JkaW5hdGVzPVwiJHtjb29yZGluYXRlc31cIlxuXHRcdFx0PmApO1xuXHRcdFx0XG5cdFx0XHQvLyBUcmltbWVkIGxpbmsgdGl0bGUgdmFsdWUuXG5cdFx0XHRjb25zdCBsaW5rVGl0bGUgPSBlbGVtZW50cy5pbnB1dHMubGlua1RpdGxlLnZhbCgpLnRyaW0oKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWJvcnQgYW5kIHNob3cgbW9kYWwsIGlmIGxpbmsgdGl0bGUgb3IgY29vcmRpbmF0ZXMgYXJlIGVtcHR5LlxuXHRcdFx0aWYgKCFjb29yZGluYXRlcyB8fCAhbGlua1RpdGxlKSB7XG5cdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKFxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdNSVNTSU5HX1BBVEhfT1JfTElOS19USVRMRV9NT0RBTF9USVRMRScsICdzbGlkZXJzJyksXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ01JU1NJTkdfUEFUSF9PUl9MSU5LX1RJVExFX01PREFMX1RFWFQnLCAnc2xpZGVycycpXG5cdFx0XHRcdCk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIEFkZCBsaXN0IGl0ZW0sIGlmIHRoZSBzZWxlY3RlZCBpbWFnZSBhcmVhIGlzIG5ldy5cblx0XHRcdC8vIE90aGVyd2lzZSByZXBsYWNlIHRoZSBhbHJlYWR5IGxpc3RlZCBpdGVtLlxuXHRcdFx0aWYgKGlzTmV3SW1hZ2VBcmVhKSB7XG5cdFx0XHRcdC8vIFJlbW92ZSBuZXcgb3B0aW9uIGNsYXNzLlxuXHRcdFx0XHQkc2VsZWN0ZWQucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5uZXdPcHRpb24pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQWRkIGxpc3QgaXRlbSB0byBkYXRhIGNvbnRhaW5lciBsaXN0IGVsZW1lbnQuXG5cdFx0XHRcdCRsaXN0LmFwcGVuZCgkbGlzdEl0ZW0pO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Ly8gUmVwbGFjZSBkYXRhIGNvbnRhaW5lciBsaXN0IGl0ZW0gd2l0aCBjcmVhdGVkIG9uZS5cblx0XHRcdFx0JGxpc3QuY2hpbGRyZW4oKS5lcShvcHRpb25JbmRleCkucmVwbGFjZVdpdGgoJGxpc3RJdGVtKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gU2VsZWN0ICdwbGVhc2Ugc2VsZWN0JyBkcm9wZG93biBpdGVtLlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmFyZWEudmFsKHZhbHVlcy5uaWwpO1xuXHRcdFx0XG5cdFx0XHQvLyBUcmlnZ2VyICdjaGFuZ2UnIGV2ZW50IHRvIGdldCB0byB0aGUgZGVmYXVsdCB2aWV3LlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmFyZWEudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlICdjbGljaycgZXZlbnQgb24gdGhlIGFib3J0IGJ1dHRvbi5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25BYm9ydCgpIHtcblx0XHRcdC8vIFNlbGVjdGVkIG9wdGlvbi5cblx0XHRcdGNvbnN0ICRzZWxlY3RlZCA9IGVsZW1lbnRzLmlucHV0cy5hcmVhLmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBJcyB0aGUgaW1hZ2UgYXJlYSBuZXc/XG5cdFx0XHRjb25zdCBpc05ld0ltYWdlQXJlYSA9ICRzZWxlY3RlZC5oYXNDbGFzcyhjbGFzc2VzLm5ld09wdGlvbik7XG5cdFx0XHRcblx0XHRcdC8vIFJlbW92ZSBvcHRpb24gZnJvbSBhcmVhIGRyb3Bkb3duLCBpZiB0aGUgc2VsZWN0ZWQgaW1hZ2UgYXJlYSBpcyBuZXcuXG5cdFx0XHQvLyBPdGhlcndpc2UgdGhlIGFyZWEgZHJvcGRvd24gd2lsbCBiZSByZWZpbGxlZC5cblx0XHRcdGlmIChpc05ld0ltYWdlQXJlYSkge1xuXHRcdFx0XHQkc2VsZWN0ZWQucmVtb3ZlKCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRfZmlsbEFyZWFEcm9wZG93bigpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTZWxlY3QgJ3BsZWFzZSBzZWxlY3QnIGRyb3Bkb3duIGl0ZW0uXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMuYXJlYS52YWwodmFsdWVzLm5pbCk7XG5cdFx0XHRcblx0XHRcdC8vIFRyaWdnZXIgJ2NoYW5nZScgZXZlbnQgdG8gZ2V0IHRvIHRoZSBkZWZhdWx0IHZpZXcuXG5cdFx0XHRlbGVtZW50cy5pbnB1dHMuYXJlYS50cmlnZ2VyKCdjaGFuZ2UnKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgJ2NsaWNrJyBldmVudCBvbiB0aGUgZGVsZXRlIGJ1dHRvbi5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25EZWxldGUoKSB7XG5cdFx0XHQvLyBTZWxlY3RlZCBvcHRpb24uXG5cdFx0XHRjb25zdCAkc2VsZWN0ZWQgPSBlbGVtZW50cy5pbnB1dHMuYXJlYS5maW5kKCdvcHRpb246c2VsZWN0ZWQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gSW5kZXggb2YgdGhlIHNlbGVjdGVkIG9wdGlvbiAoc3VidHJhY3RlZCAxIHRvIGJlIGNvbXBhdGlibGUgd2l0aCBkYXRhIGNvbnRhaW5lciBsaXN0IGVsZW1lbnQpLlxuXHRcdFx0Y29uc3Qgb3B0aW9uSW5kZXggPSAkc2VsZWN0ZWQuaW5kZXgoKSAtIDE7XG5cdFx0XHRcblx0XHRcdC8vIERlbGV0ZSBkYXRhIGNvbnRhaW5lciBsaXN0IGl0ZW0uXG5cdFx0XHQkbGlzdC5jaGlsZHJlbigpLmVxKG9wdGlvbkluZGV4KS5yZW1vdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gU3luY3Jvbml6ZSBhcmVhIGRyb3Bkb3duLlxuXHRcdFx0X2ZpbGxBcmVhRHJvcGRvd24oKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2VsZWN0ICdwbGVhc2Ugc2VsZWN0JyBkcm9wZG93biBpdGVtLlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmFyZWEudmFsKHZhbHVlcy5uaWwpO1xuXHRcdFx0XG5cdFx0XHQvLyBUcmlnZ2VyICdjaGFuZ2UnIGV2ZW50IHRvIGdldCB0byB0aGUgZGVmYXVsdCB2aWV3LlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmFyZWEudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlICdjbGljaycgZXZlbnQgb24gdGhlIHJlc2V0IGJ1dHRvbi5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25SZXNldCgpIHtcblx0XHRcdC8vIFRyaWdnZXIgdGhlICdyZXNldCcgZXZlbnQgdG8gY2xlYXIgdGhlIHBhdGguXG5cdFx0XHRlbGVtZW50cy5leHRlbnNpb24udHJpZ2dlcigncmVzZXQnKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgJ3Nob3cnIGV2ZW50LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IFRyaWdnZXJlZCBldmVudC5cblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gJGxpc3RQYXJhbWV0ZXIgRGF0YSBjb250YWluZXIgbGlzdCBlbGVtZW50LlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSBpbWFnZVVybFBhdGggVVJMIHRvIHNsaWRlIGltYWdlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblNob3coZXZlbnQsICRsaXN0UGFyYW1ldGVyLCBpbWFnZVVybFBhdGgpIHtcblx0XHRcdC8vIFNob3cgbW9kYWwuXG5cdFx0XHQkdGhpcy5tb2RhbCgnc2hvdycpO1xuXHRcdFx0XG5cdFx0XHQvLyBBc3NpZ24gZGF0YSBjb250YWluZXIgbGlzdCBlbGVtZW50IHZhbHVlLlxuXHRcdFx0JGxpc3QgPSAkbGlzdFBhcmFtZXRlcjtcblx0XHRcdFxuXHRcdFx0Ly8gQXNzaWduIGltYWdlIFVSTCB2YWx1ZS5cblx0XHRcdGltYWdlVXJsID0gaW1hZ2VVcmxQYXRoO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSAnY2xpY2snIGV2ZW50LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IFRyaWdnZXJlZCBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25DbGljayhldmVudCkge1xuXHRcdFx0Ly8gQ2hlY2ssIHdoZXRoZXIgdGhlIGNyZWF0ZSBidXR0b24gaXMgY2xpY2tlZC5cblx0XHRcdGlmIChlbGVtZW50cy5idXR0b25zLmNyZWF0ZS5pcyhldmVudC50YXJnZXQpKSB7XG5cdFx0XHRcdF9vbkNyZWF0ZSgpO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrLCB3aGV0aGVyIHRoZSBhcHBseSBidXR0b24gaXMgY2xpY2tlZC5cblx0XHRcdGlmIChlbGVtZW50cy5idXR0b25zLmFwcGx5LmlzKGV2ZW50LnRhcmdldCkpIHtcblx0XHRcdFx0X29uQXBwbHkoKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBDaGVjaywgd2hldGhlciB0aGUgYWJvcnQgYnV0dG9uIGlzIGNsaWNrZWQuXG5cdFx0XHRpZiAoZWxlbWVudHMuYnV0dG9ucy5hYm9ydC5pcyhldmVudC50YXJnZXQpKSB7XG5cdFx0XHRcdF9vbkFib3J0KCk7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gQ2hlY2ssIHdoZXRoZXIgdGhlIGRlbGV0ZSBidXR0b24gaXMgY2xpY2tlZC5cblx0XHRcdGlmIChlbGVtZW50cy5idXR0b25zLmRlbGV0ZS5pcyhldmVudC50YXJnZXQpKSB7XG5cdFx0XHRcdF9vbkRlbGV0ZSgpO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrLCB3aGV0aGVyIHRoZSByZXNldCBidXR0b24gaXMgY2xpY2tlZC5cblx0XHRcdGlmIChlbGVtZW50cy5idXR0b25zLnJlc2V0LmlzKGV2ZW50LnRhcmdldCkpIHtcblx0XHRcdFx0X29uUmVzZXQoKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSAnY2hhbmdlJyBldmVudC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uQ2hhbmdlKGV2ZW50KSB7XG5cdFx0XHQvLyBDaGVjaywgd2hldGhlciB0aGUgYXJlYSBkcm9wZG93biBpcyBjaGFuZ2VkLlxuXHRcdFx0aWYgKGVsZW1lbnRzLmlucHV0cy5hcmVhLmlzKGV2ZW50LnRhcmdldCkpIHtcblx0XHRcdFx0X29uU3dpdGNoQXJlYSgpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBtb2RhbCBzaG93biBldmVudCwgd2hpY2ggaXMgdHJpZ2dlcmVkIGJ5IHRoZSBib290c3RyYXAgbW9kYWwgcGx1Z2luLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbk1vZGFsU2hvd24oKSB7XG5cdFx0XHQvLyBGaWxsIHRoZSBhcmVhIGRyb3Bkb3duIHdpdGggdGhlIHZhbHVlcyBmcm9tIHRoZSBkYXRhIGNvbnRhaW5lciBsaXN0LlxuXHRcdFx0X2ZpbGxBcmVhRHJvcGRvd24oKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2VsZWN0ICdwbGVhc2Ugc2VsZWN0JyBkcm9wZG93biBpdGVtIGFuZCB0cmlnZ2VyICdjaGFuZ2UnIGV2ZW50IHRvIGdldCB0byB0aGUgZGVmYXVsdCB2aWV3LlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmFyZWFcblx0XHRcdFx0LnZhbCh2YWx1ZXMubmlsKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIG1vZGFsIGhpZGRlbiBldmVudCwgd2hpY2ggaXMgdHJpZ2dlcmVkIGJ5IHRoZSBib290c3RyYXAgbW9kYWwgcGx1Z2luLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbk1vZGFsSGlkZGVuKCkge1xuXHRcdFx0Ly8gU2VsZWN0ICdwbGVhc2Ugc2VsZWN0JyBkcm9wZG93biBpdGVtIGFuZCB0cmlnZ2VyICdjaGFuZ2UnIGV2ZW50IHRvIGdldCB0byB0aGUgZGVmYXVsdCB2aWV3LlxuXHRcdFx0ZWxlbWVudHMuaW5wdXRzLmFyZWFcblx0XHRcdFx0LnZhbCh2YWx1ZXMubmlsKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlICdpbnB1dCcgZXZlbnQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbklucHV0KGV2ZW50KSB7XG5cdFx0XHQvLyBDaGVjaywgd2hldGhlciB0aGUgbGluayB0aXRsZSBpcyB0aGUgY2hhbmdlZCBlbGVtZW50LlxuXHRcdFx0aWYgKGVsZW1lbnRzLmlucHV0cy5saW5rVGl0bGUuaXMoZXZlbnQudGFyZ2V0KSkge1xuXHRcdFx0XHRfb25MaW5rVGl0bGVJbnB1dCgpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGRvbmUgPT4ge1xuXHRcdFx0Ly8gQmluZCBldmVudCBoYW5kbGVycy5cblx0XHRcdCR0aGlzXG5cdFx0XHRcdC5vbignY2xpY2snLCBfb25DbGljaylcblx0XHRcdFx0Lm9uKCdjaGFuZ2UnLCBfb25DaGFuZ2UpXG5cdFx0XHRcdC5vbignc2hvd24uYnMubW9kYWwnLCBfb25Nb2RhbFNob3duKVxuXHRcdFx0XHQub24oJ2hpZGRlbi5icy5tb2RhbCcsIF9vbk1vZGFsSGlkZGVuKVxuXHRcdFx0XHQub24oJ3Nob3cnLCBfb25TaG93KVxuXHRcdFx0XHQub24oJ2lucHV0JywgX29uSW5wdXQpO1xuXHRcdFx0XG5cdFx0XHQvLyBGaW5pc2ggaW5pdGlhbGl6YXRpb24uXG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTsiXX0=
