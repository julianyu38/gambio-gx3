'use strict';

/* --------------------------------------------------------------
 graduated_prices.js 2017-03-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Graduate Prices Modal Controller
 *
 * Handles the graduate prices modal functionality.
 */
gx.controllers.module('graduated_prices', ['modal', gx.source + '/libs/info_box'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Creates a graduated prices form row.
  *
  * @param {Object} graduatedPrice Contains the graduated price information.
  *
  * @return {jQuery} Returns the created row selector.
  */
	function _createRow(graduatedPrice) {
		var $row = $('<div />', {
			'class': 'row',
			'data-graduated-price': graduatedPrice
		});

		var $quantityFormGroup = $('<div />', {
			'class': 'form-group col-xs-4'
		});

		var $quantityLabel = $('<label />', {
			'class': 'control-label',
			'text': jse.core.lang.translate('NUMBER_OF_PIECES', 'admin_quick_edit')
		});

		var $quantityInput = $('<input />', {
			'type': 'text',
			'class': 'form-control quantity',
			'value': graduatedPrice.quantity
		});

		$quantityFormGroup.append($quantityLabel, $quantityInput);

		var $priceFormGroup = $('<div />', {
			'class': 'form-group col-xs-4'
		});

		var $priceLabel = $('<label />', {
			'class': 'control-label',
			'text': jse.core.lang.translate('PRICE', 'admin_quick_edit')
		});

		var $priceInputGroup = $('<div />', {
			'class': 'input-group'
		});

		var $priceInput = $('<input />', {
			'type': 'text',
			'class': 'form-control price',
			'value': graduatedPrice.personal_offer
		});

		var $priceInputGroupButton = $('<span />', {
			'class': 'input-group-btn'
		});

		var $deleteButton = $('<button />', {
			'class': 'btn delete',
			'html': '<i class="fa fa-trash"></i>'
		});

		if (graduatedPrice.price_id === '') {
			$deleteButton.prop('disabled', true);
		}

		$priceInputGroupButton.append($deleteButton);

		$priceInputGroup.append($priceInput, $priceInputGroupButton);

		$priceFormGroup.append($priceLabel, $priceInputGroup);

		$row.append($quantityFormGroup, $priceFormGroup);

		return $row;
	}

	/**
  * Handles AJAX request errors.
  *
  * @param {jQuery.jqXHR} jqXHR jQuery request object.
  * @param {String} textStatus Request status string.
  * @param {Error} errorThrown Thrown error object.
  */
	function _handleRequestError(jqXHR, textStatus, errorThrown) {
		jse.libs.modal.message({
			title: jse.core.lang.translate('error', 'messages'),
			content: jse.core.lang.translate('UNEXPECTED_REQUEST_ERROR', 'admin_quick_edit')
		});
	}

	/**
  * Get graduated prices for selected product.
  *
  * @param {Number} productId Selected product ID.
  *
  * @return {jQuery.jqXHR} Returns request's deferred object.
  */
	function _getGraduatedPrices(productId) {
		return $.ajax({
			method: 'GET',
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/ProductGraduatedPrices',
			data: {
				productId: productId,
				pageToken: jse.core.config.get('pageToken')
			},
			dataType: 'json'
		});
	}

	/**
  * Renders graduated prices content.
  *
  * @param {Object} graduatedPrices Contains graduated prices info for selected product.
  */
	function _displayGraduatedPrices(graduatedPrices) {
		var $modalBody = $this.find('.modal-body');

		$modalBody.empty();

		var $tabList = $('<ul />', {
			'class': 'nav nav-tabs',
			'role': 'tablist'
		});

		var $tabContent = $('<div />', {
			'class': 'tab-content'
		});

		for (var customerStatusId in graduatedPrices.data[0].customers) {
			var customerStatus = graduatedPrices.data[0].customers[customerStatusId];

			var $tab = $('<li />');

			var graduatedPricesCount = customerStatus.graduations.length ? ' (' + customerStatus.graduations.length + ')' : '';

			var $link = $('<a />', {
				'href': '#customer-status-' + customerStatusId,
				'role': 'tab',
				'data-toggle': 'tab',
				'html': customerStatus.status_name + graduatedPricesCount
			});

			$tab.append($link);

			$tabList.append($tab);

			// Add new tab container in tab content. 
			var $tabPane = $('<div />', {
				'role': 'tabpanel',
				'class': 'tab-pane fade',
				'id': 'customer-status-' + customerStatusId,
				'data-customer-status-id': customerStatusId
			});

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = customerStatus.graduations[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var graduation = _step.value;

					graduation.customer_status_id = customerStatusId;
					$tabPane.append(_createRow(graduation));
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			$tabPane.append(_createRow({
				price_id: '',
				quantity: '',
				personal_offer: '',
				customer_status_id: customerStatusId
			}));

			$tabContent.append($tabPane);
		}

		$modalBody.append($tabList, $tabContent);

		// Show the first tab contents. 
		$tabList.find('a:first').tab('show');
	}

	/**
  * Updates row count in tab link.
  */
	function _updateTabCounters() {
		$this.find('.tab-pane').each(function () {
			var $tabPane = $(this);
			var graduatedPricesCount = $tabPane.find('.row').length - 1;
			var $tabLink = $this.find('[href="#' + $tabPane.attr('id') + '"]');
			var countText = graduatedPricesCount > 0 ? '(' + graduatedPricesCount + ')' : '';

			if ($tabLink.text().search(/\(.*\)/) !== -1) {
				$tabLink.text($tabLink.text().replace(/\(.*\)/, countText));
			} else {
				$tabLink.text($tabLink.text() + countText);
			}
		});
	}

	/**
  * Row input key up event handler.
  */
	function _onRowInputKeyUp() {
		var $row = $(this).parents('.row:first');
		var $lastRow = $row.parents('.tab-pane').find('.row:last');

		if ($lastRow[0] === $row[0] && $row.find('input.quantity').val() !== '') {
			var $tabPane = $row.parents('.tab-pane:first');

			$row.find('.btn.delete').prop('disabled', false);

			$tabPane.append(_createRow({
				price_id: '',
				quantity: '',
				personal_offer: '',
				customer_status_id: $tabPane.data('customerStatusId')
			}));

			_updateTabCounters();
		}
	}

	/**
  * Row delete button click event handler.
  */
	function _onRowDeleteClick() {
		$(this).parents('.row:first').remove();
		_updateTabCounters();
	}

	/**
  * Graduated prices modal show event handler.
  *
  * Loads and displays graduated price info for the selected product.
  */
	function _onModalShow() {
		_getGraduatedPrices($this.data('productId')).done(_displayGraduatedPrices).fail(_handleRequestError);
	}

	/**
  * Saves graduated prices and closes the modal.
  */
	function _onSaveClick() {
		var customerStatuses = {};

		$this.find('.tab-pane').each(function (index, tabPane) {
			var $tabPane = $(tabPane);
			var customerStatusId = $tabPane.data('customerStatusId');

			customerStatuses[customerStatusId] = [];

			$tabPane.find('.row').each(function (index, row) {
				var $row = $(row);

				if ($row.is(':last-child')) {
					return false;
				}

				customerStatuses[customerStatusId].push({
					price_id: $row.data('price_id'),
					quantity: $row.find('input.quantity').val(),
					personal_offer: $row.find('input.price').val()
				});
			});

			// Add value for empty groups.
			if (!customerStatuses[customerStatusId].length) {
				customerStatuses[customerStatusId].push('empty');
			}
		});

		$.ajax({
			method: 'POST',
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/SaveGraduatedPrices',
			data: {
				pageToken: jse.core.config.get('pageToken'),
				productId: $this.data('productId'),
				customerStatuses: customerStatuses
			},
			dataType: 'json'
		}).done(function (response) {
			$this.modal('hide');
			jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('SUCCESS_PRODUCT_UPDATED', 'admin_quick_edit'));
		}).fail(_handleRequestError);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.delete', _onRowDeleteClick).on('click', '.btn.save', _onSaveClick).on('keyup', 'input.form-control', _onRowInputKeyUp).on('show.bs.modal', _onModalShow);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL2dyYWR1YXRlZF9wcmljZXMuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJfY3JlYXRlUm93IiwiZ3JhZHVhdGVkUHJpY2UiLCIkcm93IiwiJHF1YW50aXR5Rm9ybUdyb3VwIiwiJHF1YW50aXR5TGFiZWwiLCJqc2UiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsIiRxdWFudGl0eUlucHV0IiwicXVhbnRpdHkiLCJhcHBlbmQiLCIkcHJpY2VGb3JtR3JvdXAiLCIkcHJpY2VMYWJlbCIsIiRwcmljZUlucHV0R3JvdXAiLCIkcHJpY2VJbnB1dCIsInBlcnNvbmFsX29mZmVyIiwiJHByaWNlSW5wdXRHcm91cEJ1dHRvbiIsIiRkZWxldGVCdXR0b24iLCJwcmljZV9pZCIsInByb3AiLCJfaGFuZGxlUmVxdWVzdEVycm9yIiwianFYSFIiLCJ0ZXh0U3RhdHVzIiwiZXJyb3JUaHJvd24iLCJsaWJzIiwibW9kYWwiLCJtZXNzYWdlIiwidGl0bGUiLCJjb250ZW50IiwiX2dldEdyYWR1YXRlZFByaWNlcyIsInByb2R1Y3RJZCIsImFqYXgiLCJtZXRob2QiLCJ1cmwiLCJjb25maWciLCJnZXQiLCJwYWdlVG9rZW4iLCJkYXRhVHlwZSIsIl9kaXNwbGF5R3JhZHVhdGVkUHJpY2VzIiwiZ3JhZHVhdGVkUHJpY2VzIiwiJG1vZGFsQm9keSIsImZpbmQiLCJlbXB0eSIsIiR0YWJMaXN0IiwiJHRhYkNvbnRlbnQiLCJjdXN0b21lclN0YXR1c0lkIiwiY3VzdG9tZXJzIiwiY3VzdG9tZXJTdGF0dXMiLCIkdGFiIiwiZ3JhZHVhdGVkUHJpY2VzQ291bnQiLCJncmFkdWF0aW9ucyIsImxlbmd0aCIsIiRsaW5rIiwic3RhdHVzX25hbWUiLCIkdGFiUGFuZSIsImdyYWR1YXRpb24iLCJjdXN0b21lcl9zdGF0dXNfaWQiLCJ0YWIiLCJfdXBkYXRlVGFiQ291bnRlcnMiLCJlYWNoIiwiJHRhYkxpbmsiLCJhdHRyIiwiY291bnRUZXh0IiwidGV4dCIsInNlYXJjaCIsInJlcGxhY2UiLCJfb25Sb3dJbnB1dEtleVVwIiwicGFyZW50cyIsIiRsYXN0Um93IiwidmFsIiwiX29uUm93RGVsZXRlQ2xpY2siLCJyZW1vdmUiLCJfb25Nb2RhbFNob3ciLCJkb25lIiwiZmFpbCIsIl9vblNhdmVDbGljayIsImN1c3RvbWVyU3RhdHVzZXMiLCJpbmRleCIsInRhYlBhbmUiLCJyb3ciLCJpcyIsInB1c2giLCJpbmZvX2JveCIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwiaW5pdCIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUFzQixrQkFBdEIsRUFBMEMsQ0FBQyxPQUFELEVBQWFGLEdBQUdHLE1BQWhCLG9CQUExQyxFQUFtRixVQUFTQyxJQUFULEVBQWU7O0FBRWpHOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUosU0FBUyxFQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVNLLFVBQVQsQ0FBb0JDLGNBQXBCLEVBQW9DO0FBQ25DLE1BQU1DLE9BQU9ILEVBQUUsU0FBRixFQUFhO0FBQ3pCLFlBQVMsS0FEZ0I7QUFFekIsMkJBQXdCRTtBQUZDLEdBQWIsQ0FBYjs7QUFLQSxNQUFNRSxxQkFBcUJKLEVBQUUsU0FBRixFQUFhO0FBQ3ZDLFlBQVM7QUFEOEIsR0FBYixDQUEzQjs7QUFJQSxNQUFNSyxpQkFBaUJMLEVBQUUsV0FBRixFQUFlO0FBQ3JDLFlBQVMsZUFENEI7QUFFckMsV0FBUU0sSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isa0JBQXhCLEVBQTRDLGtCQUE1QztBQUY2QixHQUFmLENBQXZCOztBQUtBLE1BQU1DLGlCQUFpQlYsRUFBRSxXQUFGLEVBQWU7QUFDckMsV0FBUSxNQUQ2QjtBQUVyQyxZQUFTLHVCQUY0QjtBQUdyQyxZQUFTRSxlQUFlUztBQUhhLEdBQWYsQ0FBdkI7O0FBTUFQLHFCQUFtQlEsTUFBbkIsQ0FBMEJQLGNBQTFCLEVBQTBDSyxjQUExQzs7QUFFQSxNQUFNRyxrQkFBa0JiLEVBQUUsU0FBRixFQUFhO0FBQ3BDLFlBQVM7QUFEMkIsR0FBYixDQUF4Qjs7QUFJQSxNQUFNYyxjQUFjZCxFQUFFLFdBQUYsRUFBZTtBQUNsQyxZQUFTLGVBRHlCO0FBRWxDLFdBQVFNLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLGtCQUFqQztBQUYwQixHQUFmLENBQXBCOztBQUtBLE1BQU1NLG1CQUFtQmYsRUFBRSxTQUFGLEVBQWE7QUFDckMsWUFBUztBQUQ0QixHQUFiLENBQXpCOztBQUlBLE1BQU1nQixjQUFjaEIsRUFBRSxXQUFGLEVBQWU7QUFDbEMsV0FBUSxNQUQwQjtBQUVsQyxZQUFTLG9CQUZ5QjtBQUdsQyxZQUFTRSxlQUFlZTtBQUhVLEdBQWYsQ0FBcEI7O0FBTUEsTUFBTUMseUJBQXlCbEIsRUFBRSxVQUFGLEVBQWM7QUFDNUMsWUFBUztBQURtQyxHQUFkLENBQS9COztBQUlBLE1BQU1tQixnQkFBZ0JuQixFQUFFLFlBQUYsRUFBZ0I7QUFDckMsWUFBUyxZQUQ0QjtBQUVyQyxXQUFRO0FBRjZCLEdBQWhCLENBQXRCOztBQUtBLE1BQUlFLGVBQWVrQixRQUFmLEtBQTRCLEVBQWhDLEVBQW9DO0FBQ25DRCxpQkFBY0UsSUFBZCxDQUFtQixVQUFuQixFQUErQixJQUEvQjtBQUNBOztBQUVESCx5QkFBdUJOLE1BQXZCLENBQThCTyxhQUE5Qjs7QUFFQUosbUJBQWlCSCxNQUFqQixDQUF3QkksV0FBeEIsRUFBcUNFLHNCQUFyQzs7QUFFQUwsa0JBQWdCRCxNQUFoQixDQUF1QkUsV0FBdkIsRUFBb0NDLGdCQUFwQzs7QUFFQVosT0FBS1MsTUFBTCxDQUFZUixrQkFBWixFQUFnQ1MsZUFBaEM7O0FBRUEsU0FBT1YsSUFBUDtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU21CLG1CQUFULENBQTZCQyxLQUE3QixFQUFvQ0MsVUFBcEMsRUFBZ0RDLFdBQWhELEVBQTZEO0FBQzVEbkIsTUFBSW9CLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxPQUFmLENBQXVCO0FBQ3RCQyxVQUFPdkIsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FEZTtBQUV0QnFCLFlBQVN4QixJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwwQkFBeEIsRUFBb0Qsa0JBQXBEO0FBRmEsR0FBdkI7QUFJQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNzQixtQkFBVCxDQUE2QkMsU0FBN0IsRUFBd0M7QUFDdkMsU0FBT2hDLEVBQUVpQyxJQUFGLENBQU87QUFDYkMsV0FBUSxLQURLO0FBRWJDLFFBQUs3QixJQUFJQyxJQUFKLENBQVM2QixNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxrRUFGeEI7QUFHYnZDLFNBQU07QUFDTGtDLHdCQURLO0FBRUxNLGVBQVdoQyxJQUFJQyxJQUFKLENBQVM2QixNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUZOLElBSE87QUFPYkUsYUFBVTtBQVBHLEdBQVAsQ0FBUDtBQVNBOztBQUVEOzs7OztBQUtBLFVBQVNDLHVCQUFULENBQWlDQyxlQUFqQyxFQUFrRDtBQUNqRCxNQUFNQyxhQUFhM0MsTUFBTTRDLElBQU4sQ0FBVyxhQUFYLENBQW5COztBQUVBRCxhQUFXRSxLQUFYOztBQUVBLE1BQU1DLFdBQVc3QyxFQUFFLFFBQUYsRUFBWTtBQUM1QixZQUFTLGNBRG1CO0FBRTVCLFdBQVE7QUFGb0IsR0FBWixDQUFqQjs7QUFLQSxNQUFNOEMsY0FBYzlDLEVBQUUsU0FBRixFQUFhO0FBQ2hDLFlBQVM7QUFEdUIsR0FBYixDQUFwQjs7QUFJQSxPQUFLLElBQUkrQyxnQkFBVCxJQUE2Qk4sZ0JBQWdCM0MsSUFBaEIsQ0FBcUIsQ0FBckIsRUFBd0JrRCxTQUFyRCxFQUFnRTtBQUMvRCxPQUFNQyxpQkFBaUJSLGdCQUFnQjNDLElBQWhCLENBQXFCLENBQXJCLEVBQXdCa0QsU0FBeEIsQ0FBa0NELGdCQUFsQyxDQUF2Qjs7QUFFQSxPQUFNRyxPQUFPbEQsRUFBRSxRQUFGLENBQWI7O0FBRUEsT0FBTW1ELHVCQUF1QkYsZUFBZUcsV0FBZixDQUEyQkMsTUFBM0IsVUFDckJKLGVBQWVHLFdBQWYsQ0FBMkJDLE1BRE4sU0FDa0IsRUFEL0M7O0FBR0EsT0FBTUMsUUFBUXRELEVBQUUsT0FBRixFQUFXO0FBQ3hCLGtDQUE0QitDLGdCQURKO0FBRXhCLFlBQVEsS0FGZ0I7QUFHeEIsbUJBQWUsS0FIUztBQUl4QixZQUFRRSxlQUFlTSxXQUFmLEdBQTZCSjtBQUpiLElBQVgsQ0FBZDs7QUFPQUQsUUFBS3RDLE1BQUwsQ0FBWTBDLEtBQVo7O0FBRUFULFlBQVNqQyxNQUFULENBQWdCc0MsSUFBaEI7O0FBRUE7QUFDQSxPQUFNTSxXQUFXeEQsRUFBRSxTQUFGLEVBQWE7QUFDN0IsWUFBUSxVQURxQjtBQUU3QixhQUFTLGVBRm9CO0FBRzdCLCtCQUF5QitDLGdCQUhJO0FBSTdCLCtCQUEyQkE7QUFKRSxJQUFiLENBQWpCOztBQXBCK0Q7QUFBQTtBQUFBOztBQUFBO0FBMkIvRCx5QkFBdUJFLGVBQWVHLFdBQXRDLDhIQUFtRDtBQUFBLFNBQTFDSyxVQUEwQzs7QUFDbERBLGdCQUFXQyxrQkFBWCxHQUFnQ1gsZ0JBQWhDO0FBQ0FTLGNBQVM1QyxNQUFULENBQWdCWCxXQUFXd0QsVUFBWCxDQUFoQjtBQUNBO0FBOUI4RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWdDL0RELFlBQVM1QyxNQUFULENBQWdCWCxXQUFXO0FBQzFCbUIsY0FBVSxFQURnQjtBQUUxQlQsY0FBVSxFQUZnQjtBQUcxQk0sb0JBQWdCLEVBSFU7QUFJMUJ5Qyx3QkFBb0JYO0FBSk0sSUFBWCxDQUFoQjs7QUFPQUQsZUFBWWxDLE1BQVosQ0FBbUI0QyxRQUFuQjtBQUNBOztBQUVEZCxhQUFXOUIsTUFBWCxDQUFrQmlDLFFBQWxCLEVBQTRCQyxXQUE1Qjs7QUFFQTtBQUNBRCxXQUFTRixJQUFULENBQWMsU0FBZCxFQUF5QmdCLEdBQXpCLENBQTZCLE1BQTdCO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNDLGtCQUFULEdBQThCO0FBQzdCN0QsUUFBTTRDLElBQU4sQ0FBVyxXQUFYLEVBQXdCa0IsSUFBeEIsQ0FBNkIsWUFBVztBQUN2QyxPQUFNTCxXQUFXeEQsRUFBRSxJQUFGLENBQWpCO0FBQ0EsT0FBTW1ELHVCQUF1QkssU0FBU2IsSUFBVCxDQUFjLE1BQWQsRUFBc0JVLE1BQXRCLEdBQStCLENBQTVEO0FBQ0EsT0FBTVMsV0FBVy9ELE1BQU00QyxJQUFOLGNBQXNCYSxTQUFTTyxJQUFULENBQWMsSUFBZCxDQUF0QixRQUFqQjtBQUNBLE9BQU1DLFlBQVliLHVCQUF1QixDQUF2QixTQUErQkEsb0JBQS9CLFNBQXlELEVBQTNFOztBQUVBLE9BQUlXLFNBQVNHLElBQVQsR0FBZ0JDLE1BQWhCLENBQXVCLFFBQXZCLE1BQXFDLENBQUMsQ0FBMUMsRUFBNkM7QUFDNUNKLGFBQVNHLElBQVQsQ0FBY0gsU0FBU0csSUFBVCxHQUFnQkUsT0FBaEIsQ0FBd0IsUUFBeEIsRUFBa0NILFNBQWxDLENBQWQ7QUFDQSxJQUZELE1BRU87QUFDTkYsYUFBU0csSUFBVCxDQUFjSCxTQUFTRyxJQUFULEtBQWtCRCxTQUFoQztBQUNBO0FBQ0QsR0FYRDtBQVlBOztBQUVEOzs7QUFHQSxVQUFTSSxnQkFBVCxHQUE0QjtBQUMzQixNQUFNakUsT0FBT0gsRUFBRSxJQUFGLEVBQVFxRSxPQUFSLENBQWdCLFlBQWhCLENBQWI7QUFDQSxNQUFNQyxXQUFXbkUsS0FBS2tFLE9BQUwsQ0FBYSxXQUFiLEVBQTBCMUIsSUFBMUIsQ0FBK0IsV0FBL0IsQ0FBakI7O0FBRUEsTUFBSTJCLFNBQVMsQ0FBVCxNQUFnQm5FLEtBQUssQ0FBTCxDQUFoQixJQUEyQkEsS0FBS3dDLElBQUwsQ0FBVSxnQkFBVixFQUE0QjRCLEdBQTVCLE9BQXNDLEVBQXJFLEVBQXlFO0FBQ3hFLE9BQU1mLFdBQVdyRCxLQUFLa0UsT0FBTCxDQUFhLGlCQUFiLENBQWpCOztBQUVBbEUsUUFBS3dDLElBQUwsQ0FBVSxhQUFWLEVBQXlCdEIsSUFBekIsQ0FBOEIsVUFBOUIsRUFBMEMsS0FBMUM7O0FBRUFtQyxZQUFTNUMsTUFBVCxDQUFnQlgsV0FBVztBQUMxQm1CLGNBQVUsRUFEZ0I7QUFFMUJULGNBQVUsRUFGZ0I7QUFHMUJNLG9CQUFnQixFQUhVO0FBSTFCeUMsd0JBQW9CRixTQUFTMUQsSUFBVCxDQUFjLGtCQUFkO0FBSk0sSUFBWCxDQUFoQjs7QUFPQThEO0FBQ0E7QUFDRDs7QUFFRDs7O0FBR0EsVUFBU1ksaUJBQVQsR0FBNkI7QUFDNUJ4RSxJQUFFLElBQUYsRUFBUXFFLE9BQVIsQ0FBZ0IsWUFBaEIsRUFBOEJJLE1BQTlCO0FBQ0FiO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU2MsWUFBVCxHQUF3QjtBQUN2QjNDLHNCQUFvQmhDLE1BQU1ELElBQU4sQ0FBVyxXQUFYLENBQXBCLEVBQ0U2RSxJQURGLENBQ09uQyx1QkFEUCxFQUVFb0MsSUFGRixDQUVPdEQsbUJBRlA7QUFHQTs7QUFFRDs7O0FBR0EsVUFBU3VELFlBQVQsR0FBd0I7QUFDdkIsTUFBTUMsbUJBQW1CLEVBQXpCOztBQUVBL0UsUUFBTTRDLElBQU4sQ0FBVyxXQUFYLEVBQXdCa0IsSUFBeEIsQ0FBNkIsVUFBQ2tCLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUNoRCxPQUFNeEIsV0FBV3hELEVBQUVnRixPQUFGLENBQWpCO0FBQ0EsT0FBTWpDLG1CQUFtQlMsU0FBUzFELElBQVQsQ0FBYyxrQkFBZCxDQUF6Qjs7QUFFQWdGLG9CQUFpQi9CLGdCQUFqQixJQUFxQyxFQUFyQzs7QUFFQVMsWUFBU2IsSUFBVCxDQUFjLE1BQWQsRUFBc0JrQixJQUF0QixDQUEyQixVQUFDa0IsS0FBRCxFQUFRRSxHQUFSLEVBQWdCO0FBQzFDLFFBQU05RSxPQUFPSCxFQUFFaUYsR0FBRixDQUFiOztBQUVBLFFBQUk5RSxLQUFLK0UsRUFBTCxDQUFRLGFBQVIsQ0FBSixFQUE0QjtBQUMzQixZQUFPLEtBQVA7QUFDQTs7QUFFREoscUJBQWlCL0IsZ0JBQWpCLEVBQW1Db0MsSUFBbkMsQ0FBd0M7QUFDdkMvRCxlQUFVakIsS0FBS0wsSUFBTCxDQUFVLFVBQVYsQ0FENkI7QUFFdkNhLGVBQVVSLEtBQUt3QyxJQUFMLENBQVUsZ0JBQVYsRUFBNEI0QixHQUE1QixFQUY2QjtBQUd2Q3RELHFCQUFnQmQsS0FBS3dDLElBQUwsQ0FBVSxhQUFWLEVBQXlCNEIsR0FBekI7QUFIdUIsS0FBeEM7QUFLQSxJQVpEOztBQWNBO0FBQ0EsT0FBSSxDQUFDTyxpQkFBaUIvQixnQkFBakIsRUFBbUNNLE1BQXhDLEVBQWdEO0FBQy9DeUIscUJBQWlCL0IsZ0JBQWpCLEVBQW1Db0MsSUFBbkMsQ0FBd0MsT0FBeEM7QUFDQTtBQUVELEdBekJEOztBQTJCQW5GLElBQUVpQyxJQUFGLENBQU87QUFDTkMsV0FBUSxNQURGO0FBRU5DLFFBQUs3QixJQUFJQyxJQUFKLENBQVM2QixNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQywrREFGL0I7QUFHTnZDLFNBQU07QUFDTHdDLGVBQVdoQyxJQUFJQyxJQUFKLENBQVM2QixNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQixDQUROO0FBRUxMLGVBQVdqQyxNQUFNRCxJQUFOLENBQVcsV0FBWCxDQUZOO0FBR0xnRjtBQUhLLElBSEE7QUFRTnZDLGFBQVU7QUFSSixHQUFQLEVBVUVvQyxJQVZGLENBVU8sb0JBQVk7QUFDakI1RSxTQUFNNEIsS0FBTixDQUFZLE1BQVo7QUFDQXJCLE9BQUlvQixJQUFKLENBQVMwRCxRQUFULENBQWtCQyxpQkFBbEIsQ0FBb0MvRSxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix5QkFBeEIsRUFBbUQsa0JBQW5ELENBQXBDO0FBQ0EsR0FiRixFQWNFbUUsSUFkRixDQWNPdEQsbUJBZFA7QUFlQTs7QUFHRDtBQUNBO0FBQ0E7O0FBRUExQixRQUFPMEYsSUFBUCxHQUFjLFVBQVNYLElBQVQsRUFBZTtBQUM1QjVFLFFBQ0V3RixFQURGLENBQ0ssT0FETCxFQUNjLGFBRGQsRUFDNkJmLGlCQUQ3QixFQUVFZSxFQUZGLENBRUssT0FGTCxFQUVjLFdBRmQsRUFFMkJWLFlBRjNCLEVBR0VVLEVBSEYsQ0FHSyxPQUhMLEVBR2Msb0JBSGQsRUFHb0NuQixnQkFIcEMsRUFJRW1CLEVBSkYsQ0FJSyxlQUpMLEVBSXNCYixZQUp0Qjs7QUFNQUM7QUFDQSxFQVJEOztBQVVBLFFBQU8vRSxNQUFQO0FBRUEsQ0F0VUQiLCJmaWxlIjoicXVpY2tfZWRpdC9tb2RhbHMvZ3JhZHVhdGVkX3ByaWNlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBncmFkdWF0ZWRfcHJpY2VzLmpzIDIwMTctMDMtMDlcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogR3JhZHVhdGUgUHJpY2VzIE1vZGFsIENvbnRyb2xsZXJcclxuICpcclxuICogSGFuZGxlcyB0aGUgZ3JhZHVhdGUgcHJpY2VzIG1vZGFsIGZ1bmN0aW9uYWxpdHkuXHJcbiAqL1xyXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ2dyYWR1YXRlZF9wcmljZXMnLCBbJ21vZGFsJywgYCR7Z3guc291cmNlfS9saWJzL2luZm9fYm94YF0sIGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gVkFSSUFCTEVTXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdCAqL1xyXG5cdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0ICovXHJcblx0Y29uc3QgbW9kdWxlID0ge307XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gRlVOQ1RJT05TXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0LyoqXHJcblx0ICogQ3JlYXRlcyBhIGdyYWR1YXRlZCBwcmljZXMgZm9ybSByb3cuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gZ3JhZHVhdGVkUHJpY2UgQ29udGFpbnMgdGhlIGdyYWR1YXRlZCBwcmljZSBpbmZvcm1hdGlvbi5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm4ge2pRdWVyeX0gUmV0dXJucyB0aGUgY3JlYXRlZCByb3cgc2VsZWN0b3IuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX2NyZWF0ZVJvdyhncmFkdWF0ZWRQcmljZSkge1xyXG5cdFx0Y29uc3QgJHJvdyA9ICQoJzxkaXYgLz4nLCB7XHJcblx0XHRcdCdjbGFzcyc6ICdyb3cnLFxyXG5cdFx0XHQnZGF0YS1ncmFkdWF0ZWQtcHJpY2UnOiBncmFkdWF0ZWRQcmljZVxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRxdWFudGl0eUZvcm1Hcm91cCA9ICQoJzxkaXYgLz4nLCB7XHJcblx0XHRcdCdjbGFzcyc6ICdmb3JtLWdyb3VwIGNvbC14cy00J1xyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRxdWFudGl0eUxhYmVsID0gJCgnPGxhYmVsIC8+Jywge1xyXG5cdFx0XHQnY2xhc3MnOiAnY29udHJvbC1sYWJlbCcsXHJcblx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ05VTUJFUl9PRl9QSUVDRVMnLCAnYWRtaW5fcXVpY2tfZWRpdCcpXHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgJHF1YW50aXR5SW5wdXQgPSAkKCc8aW5wdXQgLz4nLCB7XHJcblx0XHRcdCd0eXBlJzogJ3RleHQnLFxyXG5cdFx0XHQnY2xhc3MnOiAnZm9ybS1jb250cm9sIHF1YW50aXR5JyxcclxuXHRcdFx0J3ZhbHVlJzogZ3JhZHVhdGVkUHJpY2UucXVhbnRpdHlcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHQkcXVhbnRpdHlGb3JtR3JvdXAuYXBwZW5kKCRxdWFudGl0eUxhYmVsLCAkcXVhbnRpdHlJbnB1dCk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRwcmljZUZvcm1Hcm91cCA9ICQoJzxkaXYgLz4nLCB7XHJcblx0XHRcdCdjbGFzcyc6ICdmb3JtLWdyb3VwIGNvbC14cy00J1xyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRwcmljZUxhYmVsID0gJCgnPGxhYmVsIC8+Jywge1xyXG5cdFx0XHQnY2xhc3MnOiAnY29udHJvbC1sYWJlbCcsXHJcblx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1BSSUNFJywgJ2FkbWluX3F1aWNrX2VkaXQnKVxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRwcmljZUlucHV0R3JvdXAgPSAkKCc8ZGl2IC8+Jywge1xyXG5cdFx0XHQnY2xhc3MnOiAnaW5wdXQtZ3JvdXAnXHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgJHByaWNlSW5wdXQgPSAkKCc8aW5wdXQgLz4nLCB7XHJcblx0XHRcdCd0eXBlJzogJ3RleHQnLFxyXG5cdFx0XHQnY2xhc3MnOiAnZm9ybS1jb250cm9sIHByaWNlJyxcclxuXHRcdFx0J3ZhbHVlJzogZ3JhZHVhdGVkUHJpY2UucGVyc29uYWxfb2ZmZXJcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHRjb25zdCAkcHJpY2VJbnB1dEdyb3VwQnV0dG9uID0gJCgnPHNwYW4gLz4nLCB7XHJcblx0XHRcdCdjbGFzcyc6ICdpbnB1dC1ncm91cC1idG4nXHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgJGRlbGV0ZUJ1dHRvbiA9ICQoJzxidXR0b24gLz4nLCB7XHJcblx0XHRcdCdjbGFzcyc6ICdidG4gZGVsZXRlJyxcclxuXHRcdFx0J2h0bWwnOiAnPGkgY2xhc3M9XCJmYSBmYS10cmFzaFwiPjwvaT4nXHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0aWYgKGdyYWR1YXRlZFByaWNlLnByaWNlX2lkID09PSAnJykge1xyXG5cdFx0XHQkZGVsZXRlQnV0dG9uLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdCRwcmljZUlucHV0R3JvdXBCdXR0b24uYXBwZW5kKCRkZWxldGVCdXR0b24pO1xyXG5cdFx0XHJcblx0XHQkcHJpY2VJbnB1dEdyb3VwLmFwcGVuZCgkcHJpY2VJbnB1dCwgJHByaWNlSW5wdXRHcm91cEJ1dHRvbik7XHJcblx0XHRcclxuXHRcdCRwcmljZUZvcm1Hcm91cC5hcHBlbmQoJHByaWNlTGFiZWwsICRwcmljZUlucHV0R3JvdXApO1xyXG5cdFx0XHJcblx0XHQkcm93LmFwcGVuZCgkcXVhbnRpdHlGb3JtR3JvdXAsICRwcmljZUZvcm1Hcm91cCk7XHJcblx0XHRcclxuXHRcdHJldHVybiAkcm93O1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBIYW5kbGVzIEFKQVggcmVxdWVzdCBlcnJvcnMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge2pRdWVyeS5qcVhIUn0ganFYSFIgalF1ZXJ5IHJlcXVlc3Qgb2JqZWN0LlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSB0ZXh0U3RhdHVzIFJlcXVlc3Qgc3RhdHVzIHN0cmluZy5cclxuXHQgKiBAcGFyYW0ge0Vycm9yfSBlcnJvclRocm93biBUaHJvd24gZXJyb3Igb2JqZWN0LlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9oYW5kbGVSZXF1ZXN0RXJyb3IoanFYSFIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XHJcblx0XHRqc2UubGlicy5tb2RhbC5tZXNzYWdlKHtcclxuXHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpLFxyXG5cdFx0XHRjb250ZW50OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVU5FWFBFQ1RFRF9SRVFVRVNUX0VSUk9SJywgJ2FkbWluX3F1aWNrX2VkaXQnKVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEdldCBncmFkdWF0ZWQgcHJpY2VzIGZvciBzZWxlY3RlZCBwcm9kdWN0LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtOdW1iZXJ9IHByb2R1Y3RJZCBTZWxlY3RlZCBwcm9kdWN0IElELlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7alF1ZXJ5LmpxWEhSfSBSZXR1cm5zIHJlcXVlc3QncyBkZWZlcnJlZCBvYmplY3QuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX2dldEdyYWR1YXRlZFByaWNlcyhwcm9kdWN0SWQpIHtcclxuXHRcdHJldHVybiAkLmFqYXgoe1xyXG5cdFx0XHRtZXRob2Q6ICdHRVQnLFxyXG5cdFx0XHR1cmw6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89UXVpY2tFZGl0T3ZlcnZpZXdBamF4L1Byb2R1Y3RHcmFkdWF0ZWRQcmljZXMnLFxyXG5cdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0cHJvZHVjdElkLFxyXG5cdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJylcclxuXHRcdFx0fSxcclxuXHRcdFx0ZGF0YVR5cGU6ICdqc29uJ1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFJlbmRlcnMgZ3JhZHVhdGVkIHByaWNlcyBjb250ZW50LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IGdyYWR1YXRlZFByaWNlcyBDb250YWlucyBncmFkdWF0ZWQgcHJpY2VzIGluZm8gZm9yIHNlbGVjdGVkIHByb2R1Y3QuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX2Rpc3BsYXlHcmFkdWF0ZWRQcmljZXMoZ3JhZHVhdGVkUHJpY2VzKSB7XHJcblx0XHRjb25zdCAkbW9kYWxCb2R5ID0gJHRoaXMuZmluZCgnLm1vZGFsLWJvZHknKTtcclxuXHRcdFxyXG5cdFx0JG1vZGFsQm9keS5lbXB0eSgpO1xyXG5cdFx0XHJcblx0XHRjb25zdCAkdGFiTGlzdCA9ICQoJzx1bCAvPicsIHtcclxuXHRcdFx0J2NsYXNzJzogJ25hdiBuYXYtdGFicycsXHJcblx0XHRcdCdyb2xlJzogJ3RhYmxpc3QnXHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgJHRhYkNvbnRlbnQgPSAkKCc8ZGl2IC8+Jywge1xyXG5cdFx0XHQnY2xhc3MnOiAndGFiLWNvbnRlbnQnXHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Zm9yIChsZXQgY3VzdG9tZXJTdGF0dXNJZCBpbiBncmFkdWF0ZWRQcmljZXMuZGF0YVswXS5jdXN0b21lcnMpIHtcclxuXHRcdFx0Y29uc3QgY3VzdG9tZXJTdGF0dXMgPSBncmFkdWF0ZWRQcmljZXMuZGF0YVswXS5jdXN0b21lcnNbY3VzdG9tZXJTdGF0dXNJZF07XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCAkdGFiID0gJCgnPGxpIC8+Jyk7XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCBncmFkdWF0ZWRQcmljZXNDb3VudCA9IGN1c3RvbWVyU3RhdHVzLmdyYWR1YXRpb25zLmxlbmd0aFxyXG5cdFx0XHRcdD8gYCAoJHtjdXN0b21lclN0YXR1cy5ncmFkdWF0aW9ucy5sZW5ndGh9KWAgOiAnJztcclxuXHRcdFx0XHJcblx0XHRcdGNvbnN0ICRsaW5rID0gJCgnPGEgLz4nLCB7XHJcblx0XHRcdFx0J2hyZWYnOiBgI2N1c3RvbWVyLXN0YXR1cy0ke2N1c3RvbWVyU3RhdHVzSWR9YCxcclxuXHRcdFx0XHQncm9sZSc6ICd0YWInLFxyXG5cdFx0XHRcdCdkYXRhLXRvZ2dsZSc6ICd0YWInLFxyXG5cdFx0XHRcdCdodG1sJzogY3VzdG9tZXJTdGF0dXMuc3RhdHVzX25hbWUgKyBncmFkdWF0ZWRQcmljZXNDb3VudFxyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdCR0YWIuYXBwZW5kKCRsaW5rKTtcclxuXHRcdFx0XHJcblx0XHRcdCR0YWJMaXN0LmFwcGVuZCgkdGFiKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEFkZCBuZXcgdGFiIGNvbnRhaW5lciBpbiB0YWIgY29udGVudC4gXHJcblx0XHRcdGNvbnN0ICR0YWJQYW5lID0gJCgnPGRpdiAvPicsIHtcclxuXHRcdFx0XHQncm9sZSc6ICd0YWJwYW5lbCcsXHJcblx0XHRcdFx0J2NsYXNzJzogJ3RhYi1wYW5lIGZhZGUnLFxyXG5cdFx0XHRcdCdpZCc6IGBjdXN0b21lci1zdGF0dXMtJHtjdXN0b21lclN0YXR1c0lkfWAsXHJcblx0XHRcdFx0J2RhdGEtY3VzdG9tZXItc3RhdHVzLWlkJzogY3VzdG9tZXJTdGF0dXNJZFxyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdGZvciAobGV0IGdyYWR1YXRpb24gb2YgY3VzdG9tZXJTdGF0dXMuZ3JhZHVhdGlvbnMpIHtcclxuXHRcdFx0XHRncmFkdWF0aW9uLmN1c3RvbWVyX3N0YXR1c19pZCA9IGN1c3RvbWVyU3RhdHVzSWQ7XHJcblx0XHRcdFx0JHRhYlBhbmUuYXBwZW5kKF9jcmVhdGVSb3coZ3JhZHVhdGlvbikpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHQkdGFiUGFuZS5hcHBlbmQoX2NyZWF0ZVJvdyh7XHJcblx0XHRcdFx0cHJpY2VfaWQ6ICcnLFxyXG5cdFx0XHRcdHF1YW50aXR5OiAnJyxcclxuXHRcdFx0XHRwZXJzb25hbF9vZmZlcjogJycsXHJcblx0XHRcdFx0Y3VzdG9tZXJfc3RhdHVzX2lkOiBjdXN0b21lclN0YXR1c0lkXHJcblx0XHRcdH0pKTtcclxuXHRcdFx0XHJcblx0XHRcdCR0YWJDb250ZW50LmFwcGVuZCgkdGFiUGFuZSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdCRtb2RhbEJvZHkuYXBwZW5kKCR0YWJMaXN0LCAkdGFiQ29udGVudCk7XHJcblx0XHRcclxuXHRcdC8vIFNob3cgdGhlIGZpcnN0IHRhYiBjb250ZW50cy4gXHJcblx0XHQkdGFiTGlzdC5maW5kKCdhOmZpcnN0JykudGFiKCdzaG93Jyk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFVwZGF0ZXMgcm93IGNvdW50IGluIHRhYiBsaW5rLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF91cGRhdGVUYWJDb3VudGVycygpIHtcclxuXHRcdCR0aGlzLmZpbmQoJy50YWItcGFuZScpLmVhY2goZnVuY3Rpb24oKSB7XHJcblx0XHRcdGNvbnN0ICR0YWJQYW5lID0gJCh0aGlzKTtcclxuXHRcdFx0Y29uc3QgZ3JhZHVhdGVkUHJpY2VzQ291bnQgPSAkdGFiUGFuZS5maW5kKCcucm93JykubGVuZ3RoIC0gMTtcclxuXHRcdFx0Y29uc3QgJHRhYkxpbmsgPSAkdGhpcy5maW5kKGBbaHJlZj1cIiMkeyR0YWJQYW5lLmF0dHIoJ2lkJyl9XCJdYCk7XHJcblx0XHRcdGNvbnN0IGNvdW50VGV4dCA9IGdyYWR1YXRlZFByaWNlc0NvdW50ID4gMCA/IGAoJHtncmFkdWF0ZWRQcmljZXNDb3VudH0pYCA6ICcnO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKCR0YWJMaW5rLnRleHQoKS5zZWFyY2goL1xcKC4qXFwpLykgIT09IC0xKSB7XHJcblx0XHRcdFx0JHRhYkxpbmsudGV4dCgkdGFiTGluay50ZXh0KCkucmVwbGFjZSgvXFwoLipcXCkvLCBjb3VudFRleHQpKTtcdFxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCR0YWJMaW5rLnRleHQoJHRhYkxpbmsudGV4dCgpICsgY291bnRUZXh0KTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFJvdyBpbnB1dCBrZXkgdXAgZXZlbnQgaGFuZGxlci5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25Sb3dJbnB1dEtleVVwKCkge1xyXG5cdFx0Y29uc3QgJHJvdyA9ICQodGhpcykucGFyZW50cygnLnJvdzpmaXJzdCcpO1xyXG5cdFx0Y29uc3QgJGxhc3RSb3cgPSAkcm93LnBhcmVudHMoJy50YWItcGFuZScpLmZpbmQoJy5yb3c6bGFzdCcpO1xyXG5cdFx0XHJcblx0XHRpZiAoJGxhc3RSb3dbMF0gPT09ICRyb3dbMF0gJiYgJHJvdy5maW5kKCdpbnB1dC5xdWFudGl0eScpLnZhbCgpICE9PSAnJykge1xyXG5cdFx0XHRjb25zdCAkdGFiUGFuZSA9ICRyb3cucGFyZW50cygnLnRhYi1wYW5lOmZpcnN0Jyk7XHJcblx0XHRcdFxyXG5cdFx0XHQkcm93LmZpbmQoJy5idG4uZGVsZXRlJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcblx0XHRcdFxyXG5cdFx0XHQkdGFiUGFuZS5hcHBlbmQoX2NyZWF0ZVJvdyh7XHJcblx0XHRcdFx0cHJpY2VfaWQ6ICcnLFxyXG5cdFx0XHRcdHF1YW50aXR5OiAnJyxcclxuXHRcdFx0XHRwZXJzb25hbF9vZmZlcjogJycsXHJcblx0XHRcdFx0Y3VzdG9tZXJfc3RhdHVzX2lkOiAkdGFiUGFuZS5kYXRhKCdjdXN0b21lclN0YXR1c0lkJylcclxuXHRcdFx0fSkpO1xyXG5cdFx0XHRcclxuXHRcdFx0X3VwZGF0ZVRhYkNvdW50ZXJzKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFJvdyBkZWxldGUgYnV0dG9uIGNsaWNrIGV2ZW50IGhhbmRsZXIuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uUm93RGVsZXRlQ2xpY2soKSB7XHJcblx0XHQkKHRoaXMpLnBhcmVudHMoJy5yb3c6Zmlyc3QnKS5yZW1vdmUoKTtcclxuXHRcdF91cGRhdGVUYWJDb3VudGVycygpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBHcmFkdWF0ZWQgcHJpY2VzIG1vZGFsIHNob3cgZXZlbnQgaGFuZGxlci5cclxuXHQgKlxyXG5cdCAqIExvYWRzIGFuZCBkaXNwbGF5cyBncmFkdWF0ZWQgcHJpY2UgaW5mbyBmb3IgdGhlIHNlbGVjdGVkIHByb2R1Y3QuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uTW9kYWxTaG93KCkge1xyXG5cdFx0X2dldEdyYWR1YXRlZFByaWNlcygkdGhpcy5kYXRhKCdwcm9kdWN0SWQnKSlcclxuXHRcdFx0LmRvbmUoX2Rpc3BsYXlHcmFkdWF0ZWRQcmljZXMpXHJcblx0XHRcdC5mYWlsKF9oYW5kbGVSZXF1ZXN0RXJyb3IpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBTYXZlcyBncmFkdWF0ZWQgcHJpY2VzIGFuZCBjbG9zZXMgdGhlIG1vZGFsLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vblNhdmVDbGljaygpIHtcclxuXHRcdGNvbnN0IGN1c3RvbWVyU3RhdHVzZXMgPSB7fTtcclxuXHRcdFxyXG5cdFx0JHRoaXMuZmluZCgnLnRhYi1wYW5lJykuZWFjaCgoaW5kZXgsIHRhYlBhbmUpID0+IHtcclxuXHRcdFx0Y29uc3QgJHRhYlBhbmUgPSAkKHRhYlBhbmUpO1xyXG5cdFx0XHRjb25zdCBjdXN0b21lclN0YXR1c0lkID0gJHRhYlBhbmUuZGF0YSgnY3VzdG9tZXJTdGF0dXNJZCcpO1xyXG5cdFx0XHRcclxuXHRcdFx0Y3VzdG9tZXJTdGF0dXNlc1tjdXN0b21lclN0YXR1c0lkXSA9IFtdO1xyXG5cdFx0XHRcclxuXHRcdFx0JHRhYlBhbmUuZmluZCgnLnJvdycpLmVhY2goKGluZGV4LCByb3cpID0+IHtcclxuXHRcdFx0XHRjb25zdCAkcm93ID0gJChyb3cpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmICgkcm93LmlzKCc6bGFzdC1jaGlsZCcpKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGN1c3RvbWVyU3RhdHVzZXNbY3VzdG9tZXJTdGF0dXNJZF0ucHVzaCh7XHJcblx0XHRcdFx0XHRwcmljZV9pZDogJHJvdy5kYXRhKCdwcmljZV9pZCcpLFxyXG5cdFx0XHRcdFx0cXVhbnRpdHk6ICRyb3cuZmluZCgnaW5wdXQucXVhbnRpdHknKS52YWwoKSxcclxuXHRcdFx0XHRcdHBlcnNvbmFsX29mZmVyOiAkcm93LmZpbmQoJ2lucHV0LnByaWNlJykudmFsKClcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBBZGQgdmFsdWUgZm9yIGVtcHR5IGdyb3Vwcy5cclxuXHRcdFx0aWYgKCFjdXN0b21lclN0YXR1c2VzW2N1c3RvbWVyU3RhdHVzSWRdLmxlbmd0aCkge1xyXG5cdFx0XHRcdGN1c3RvbWVyU3RhdHVzZXNbY3VzdG9tZXJTdGF0dXNJZF0ucHVzaCgnZW1wdHknKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHRtZXRob2Q6ICdQT1NUJyxcclxuXHRcdFx0dXJsOiBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVF1aWNrRWRpdE92ZXJ2aWV3QWpheC9TYXZlR3JhZHVhdGVkUHJpY2VzJyxcclxuXHRcdFx0ZGF0YToge1xyXG5cdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJyksXHJcblx0XHRcdFx0cHJvZHVjdElkOiAkdGhpcy5kYXRhKCdwcm9kdWN0SWQnKSxcclxuXHRcdFx0XHRjdXN0b21lclN0YXR1c2VzXHJcblx0XHRcdH0sXHJcblx0XHRcdGRhdGFUeXBlOiAnanNvbidcclxuXHRcdH0pXHJcblx0XHRcdC5kb25lKHJlc3BvbnNlID0+IHtcclxuXHRcdFx0XHQkdGhpcy5tb2RhbCgnaGlkZScpO1xyXG5cdFx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTVUNDRVNTX1BST0RVQ1RfVVBEQVRFRCcsICdhZG1pbl9xdWlja19lZGl0JykpO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQuZmFpbChfaGFuZGxlUmVxdWVzdEVycm9yKVxyXG5cdH1cclxuXHRcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0JHRoaXNcclxuXHRcdFx0Lm9uKCdjbGljaycsICcuYnRuLmRlbGV0ZScsIF9vblJvd0RlbGV0ZUNsaWNrKVxyXG5cdFx0XHQub24oJ2NsaWNrJywgJy5idG4uc2F2ZScsIF9vblNhdmVDbGljaylcclxuXHRcdFx0Lm9uKCdrZXl1cCcsICdpbnB1dC5mb3JtLWNvbnRyb2wnLCBfb25Sb3dJbnB1dEtleVVwKVxyXG5cdFx0XHQub24oJ3Nob3cuYnMubW9kYWwnLCBfb25Nb2RhbFNob3cpO1xyXG5cdFx0XHJcblx0XHRkb25lKCk7XHJcblx0fTtcclxuXHRcclxuXHRyZXR1cm4gbW9kdWxlO1xyXG5cdFxyXG59KTsgIl19
