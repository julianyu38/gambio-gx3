'use strict';

/* --------------------------------------------------------------
 menu.js 2017-05-30
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Menu Controller
 *
 * Whenever the user clicks on a menu item the browser must be redirected to the respective location.
 *
 * Middle button clicks are also supported.
 */
gx.controllers.module('menu', [jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.min.js', jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js', 'user_configuration_service', gx.source + '/libs/shortcuts'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Menu List Selector
  *
  * @type {Object}
  */
	var $list = $this.children('ul');

	/**
  * Sub-list Selector
  *
  * This object is used for correctly displaying the sub-list <ul> element whenever it goes out
  * of viewport.
  *
  * @type {jQuery}
  */
	var $sublist = null;

	/**
  * Favorites Box
  *
  * @type {Object}
  */
	var $favoritesMenu = $this.find('ul li').first().find('ul');

	/**
  * Draggable Menu Items
  *
  * @type {Object}
  */
	var $draggableMenuItems = $this.find('.fav-drag-item');

	/**
  * Dropzone Box
  *
  * The draggable elements will be placed here.
  *
  * @type {Object}
  */
	var favDropzoneBox = '#fav-dropzone-box';

	/**
  * Drag and drop flag to prevent the default action of a menu item when it is dragged.
  *
  * @type {Boolean} True while am item is dragged.
  */
	var onDragAndDrop = false;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Repositions the sub-list when it goes off the viewport.
  */
	function _onMenuItemMouseEnter() {
		if ($list.hasClass('expand-all')) {
			return; // Do not check for viewport in "expand-all" state.
		}

		$sublist = $(this).children('ul');

		if ($sublist.length === 0) {
			return;
		}

		$sublist.offset = $sublist.offset().top + $sublist.height();

		if ($sublist.offset > window.innerHeight) {
			$sublist.css('margin-top', -1 * ($sublist.offset - window.innerHeight + $('#main-header').outerHeight() + $('#main-footer .info').outerHeight()) + 'px').addClass('stay-within-viewport');
		}
	}

	/**
  * Remove the helper class "stay-within-viewport" and reset the "margin-top" value.
  */
	function _onMenuItemMouseLeave() {
		if ($list.hasClass('expand-all')) {
			return; // Do not check for viewport in "expand-all" state.
		}

		$sublist.css('margin-top', '').removeClass('stay-within-viewport');
	}

	/**
  * Makes all menu items draggable.
  *
  * This function should be executed in the module.init() method.
  *
  * @param {Object} $draggableMenuItems Menu item jQuery selector.
  */
	function _makeMenuItemsDraggable($draggableMenuItems) {
		$draggableMenuItems.draggable({
			helper: 'clone', // Clone the element, don't move the element itself.
			start: function start(event, ui) {
				onDragAndDrop = true;
				ui.helper.addClass('currentlyDragged');
				_createFavoritesDropzone(this);
			},
			stop: function stop(event) {
				onDragAndDrop = false;
				$(favDropzoneBox).remove();
			}
		});
	}

	/**
  * Creates the favorites box, where the draggable items can be dropped on.
  *
  * @param {HTMLElement} draggedElement Dragged menu item.
  */
	function _createFavoritesDropzone(draggedElement) {
		var dropzoneBox = '';
		var action = '';

		if ($(draggedElement).parents('li').find('.fa-heart').length === 0) {
			dropzoneBox = '\n\t\t\t\t<div id="fav-dropzone-box" class="fav-add">\n\t\t\t\t\t<i class="fa fa-heart"></i>\n\t\t\t\t</div>\n\t\t\t';
			action = 'save';
		} else {
			dropzoneBox = '\n\t\t\t\t<div id="fav-dropzone-box" class="fav-delete">\n\t\t\t\t\t<i class="fa fa-trash"></i>\n\t\t\t\t</div>\n\t\t\t';
			action = 'delete';
		}

		_positionDropzoneBox(dropzoneBox, draggedElement);

		$(favDropzoneBox).droppable(_getObjectFromAction(action, draggedElement));
	}

	/**
  * Stores the menu item as a favorite in the database.
  *
  * @param {String} linkKey Unique link key from the menu item.
  * @param {Object} draggedElement Dragged menu item.
  */
	function _saveToFavorites(linkKey, draggedElement) {
		$.ajax({
			url: 'admin.php?do=AdminFavoritesAjax/AddMenuItem&link_key=' + linkKey,
			error: function error(_error) {
				console.error('Could not save the menu item with the link key: ' + linkKey);
			},
			success: function success() {
				if (!_isLinkKeyInFavorites(linkKey)) {
					var $newLink = $(draggedElement).clone().addClass('fav-drag-item');
					var $newListItem = $('<li/>').append($newLink);
					$favoritesMenu.append($newListItem);
					_makeMenuItemsDraggable($newListItem.find('.fav-drag-item'));
				}
			}
		});
	}

	/**
  * Deletes the menu item as a favorite from the database.
  *
  * @param {String} linkKey Unique link key from the menu item.
  * @param {Object} draggedElement Dragged menu item.
  */
	function _deleteFromFavorites(linkKey, draggedElement) {
		$.ajax({
			url: 'admin.php?do=AdminFavoritesAjax/RemoveMenuItem&link_key=' + linkKey,
			error: function error(_error2) {
				console.error('Could not remove the menu item with the link key: ' + linkKey);
			},
			success: function success() {
				$(draggedElement).parent('li').remove();
			}
		});
	}

	/**
  * Checks if a menu item is already stored in the favorites menu.
  *
  * @param {String} linkKey Unique link key of a menu item.
  *
  * @return {Boolean} True if menu item is already stored, else false will be returned.
  */
	function _isLinkKeyInFavorites(linkKey) {
		return $favoritesMenu.find('#' + linkKey).length !== 0;
	}

	/**
  * Get jQueryUI droppable options object
  *
  * @param {String} action Action to execute value=save|delete.
  * @param {Object} draggedElement Dragged meu item.
  *
  * @return {Object} jQueryUI droppable options.
  */
	function _getObjectFromAction(action, draggedElement) {
		var droppableOptions = {
			accept: '.fav-drag-item',
			tolerance: 'pointer',
			// Function when hovering over the favorites box.
			over: function over() {
				$(favDropzoneBox).css('opacity', '1.0');
			},
			// Function when hovering out from the favorites box.
			out: function out() {
				$(favDropzoneBox).css('opacity', '0.9');
			},
			// Function when dropping an element on the favorites box.
			drop: function drop(event, ui) {
				var linkKey = $(ui.draggable).attr('id');

				if (action === 'save') {
					_saveToFavorites(linkKey, draggedElement);
				} else if (action === 'delete') {
					_deleteFromFavorites(linkKey, draggedElement);
				}
			}
		};

		return droppableOptions;
	}

	/**
  * Positions the DropzoneBox at the correct place.
  *
  * @param {String} dropzoneBox DropzoneBox HTML.
  * @param {Object} draggedElement Dragged menu item.
  */
	function _positionDropzoneBox(dropzoneBox, draggedElement) {
		var $dropzoneBox = $(dropzoneBox);

		$(draggedElement).parent('li').prepend($dropzoneBox);

		var dropzoneBoxHeight = $dropzoneBox.outerHeight();

		$dropzoneBox.css({
			top: $(draggedElement).position().top - dropzoneBoxHeight / 2
		});
	}

	/**
  * Open the active menu group.
  *
  * This method will find the menu item that contains the same "do" GET parameter and set the "active"
  * class to its parent.
  */
	function _toggleActiveMenuGroup() {
		var currentUrlParameters = $.deparam(window.location.search.slice(1));

		$list.find('li:gt(0) ul li a').each(function (index, link) {
			var linkUrlParameters = $.deparam($(link).attr('href').replace(/.*(\?)/, '$1').slice(1));

			if (linkUrlParameters.do === currentUrlParameters.do) {
				$(link).parents('li:lt(2)').addClass('active');
				return false;
			}
		});

		if ($list.find('.active').length) {
			return;
		}

		// If no match was found, check for sub-page links in content navigation.
		$('#main-content .content-navigation .nav-item').each(function (index, navItem) {
			var $navItem = $(navItem);
			var navItemUrl = $navItem.find('a').attr('href');

			$list.find('li:gt(1) a').each(function (index, link) {
				var $link = $(link);
				var linkUrl = $link.attr('href').split('/').pop();

				if (linkUrl === navItemUrl) {
					$link.parents('li:lt(2)').addClass('active');
					return false;
				}
			});
		});

		if ($list.find('.active').length) {
			return;
		}

		// If no match was found, only check for the controller name.
		var currentControllerName = _getControllerName(currentUrlParameters.do);

		$list.find('li:gt(0) ul li a').each(function (index, link) {
			var linkUrlParameters = $.deparam($(link).attr('href').replace(/.*(\?)/, '$1').slice(1));
			var linkControllerName = _getControllerName(linkUrlParameters.do);

			if (linkControllerName === currentControllerName) {
				$(link).parents('li:lt(2)').addClass('active');
				return false;
			}
		});
	}

	/**
  * Registers a shortcut to open the favorite entries.
  *
  * @private
  */
	function _registerShortcut() {
		// Shortcut library abbreviation.
		var lib = jse.libs.shortcuts;

		// Combination name.
		var name = 'openFavoriteMenuEntry';

		// Callback function.
		var callback = function callback(index) {
			// Get link from favorite entry.
			var link = $favoritesMenu.children().eq(index).find('a').attr('href');

			// Open link in same window.
			window.open(link, '_self');
		};

		// Iterating over each above number key starting with '1' until '9'.
		var firstKeyCode = jse.libs.shortcuts.KEY_CODES.NUM_1;

		var _loop = function _loop(iteration) {
			// Key combination (CTRL + SHIFT + keyCode).
			var combination = [lib.KEY_CODES.CTRL_L, lib.KEY_CODES.SHIFT_L, firstKeyCode + iteration];

			// Register shortcut.
			lib.registerShortcut(name + iteration, combination, function () {
				return callback(iteration);
			});
		};

		for (var iteration = 0; iteration < 9; iteration++) {
			_loop(iteration);
		}
	}

	/**
  * Returns only the PHP controller value from the provided link.
  *
  * Example:
  *   Input: GoogleShopping/editScheme
  *   Output: GoogleShopping
  *
  * @param link do parameter from the fetched window.location
  */
	function _getControllerName(link) {
		return link && link.indexOf('/') !== -1 ? link.split('/')[0] : link;
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$list.children('li').on('mouseenter', _onMenuItemMouseEnter).on('mouseleave', _onMenuItemMouseLeave);

		_makeMenuItemsDraggable($draggableMenuItems);
		_toggleActiveMenuGroup();
		_registerShortcut();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxheW91dHMvbWFpbi9tZW51L21lbnUuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkbGlzdCIsImNoaWxkcmVuIiwiJHN1Ymxpc3QiLCIkZmF2b3JpdGVzTWVudSIsImZpbmQiLCJmaXJzdCIsIiRkcmFnZ2FibGVNZW51SXRlbXMiLCJmYXZEcm9wem9uZUJveCIsIm9uRHJhZ0FuZERyb3AiLCJfb25NZW51SXRlbU1vdXNlRW50ZXIiLCJoYXNDbGFzcyIsImxlbmd0aCIsIm9mZnNldCIsInRvcCIsImhlaWdodCIsIndpbmRvdyIsImlubmVySGVpZ2h0IiwiY3NzIiwib3V0ZXJIZWlnaHQiLCJhZGRDbGFzcyIsIl9vbk1lbnVJdGVtTW91c2VMZWF2ZSIsInJlbW92ZUNsYXNzIiwiX21ha2VNZW51SXRlbXNEcmFnZ2FibGUiLCJkcmFnZ2FibGUiLCJoZWxwZXIiLCJzdGFydCIsImV2ZW50IiwidWkiLCJfY3JlYXRlRmF2b3JpdGVzRHJvcHpvbmUiLCJzdG9wIiwicmVtb3ZlIiwiZHJhZ2dlZEVsZW1lbnQiLCJkcm9wem9uZUJveCIsImFjdGlvbiIsInBhcmVudHMiLCJfcG9zaXRpb25Ecm9wem9uZUJveCIsImRyb3BwYWJsZSIsIl9nZXRPYmplY3RGcm9tQWN0aW9uIiwiX3NhdmVUb0Zhdm9yaXRlcyIsImxpbmtLZXkiLCJhamF4IiwidXJsIiwiZXJyb3IiLCJjb25zb2xlIiwic3VjY2VzcyIsIl9pc0xpbmtLZXlJbkZhdm9yaXRlcyIsIiRuZXdMaW5rIiwiY2xvbmUiLCIkbmV3TGlzdEl0ZW0iLCJhcHBlbmQiLCJfZGVsZXRlRnJvbUZhdm9yaXRlcyIsInBhcmVudCIsImRyb3BwYWJsZU9wdGlvbnMiLCJhY2NlcHQiLCJ0b2xlcmFuY2UiLCJvdmVyIiwib3V0IiwiZHJvcCIsImF0dHIiLCIkZHJvcHpvbmVCb3giLCJwcmVwZW5kIiwiZHJvcHpvbmVCb3hIZWlnaHQiLCJwb3NpdGlvbiIsIl90b2dnbGVBY3RpdmVNZW51R3JvdXAiLCJjdXJyZW50VXJsUGFyYW1ldGVycyIsImRlcGFyYW0iLCJsb2NhdGlvbiIsInNlYXJjaCIsInNsaWNlIiwiZWFjaCIsImluZGV4IiwibGluayIsImxpbmtVcmxQYXJhbWV0ZXJzIiwicmVwbGFjZSIsImRvIiwibmF2SXRlbSIsIiRuYXZJdGVtIiwibmF2SXRlbVVybCIsIiRsaW5rIiwibGlua1VybCIsInNwbGl0IiwicG9wIiwiY3VycmVudENvbnRyb2xsZXJOYW1lIiwiX2dldENvbnRyb2xsZXJOYW1lIiwibGlua0NvbnRyb2xsZXJOYW1lIiwiX3JlZ2lzdGVyU2hvcnRjdXQiLCJsaWIiLCJsaWJzIiwic2hvcnRjdXRzIiwibmFtZSIsImNhbGxiYWNrIiwiZXEiLCJvcGVuIiwiZmlyc3RLZXlDb2RlIiwiS0VZX0NPREVTIiwiTlVNXzEiLCJpdGVyYXRpb24iLCJjb21iaW5hdGlvbiIsIkNUUkxfTCIsIlNISUZUX0wiLCJyZWdpc3RlclNob3J0Y3V0IiwiaW5kZXhPZiIsImluaXQiLCJkb25lIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7OztBQU9BQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxNQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUiwwQ0FFSUQsSUFBSUMsTUFGUix5Q0FHSUQsSUFBSUMsTUFIUixtREFJQyw0QkFKRCxFQUtJSixHQUFHSSxNQUxQLHFCQUhELEVBV0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUwsU0FBUyxFQUFmOztBQUVBOzs7OztBQUtBLEtBQU1NLFFBQVFGLE1BQU1HLFFBQU4sQ0FBZSxJQUFmLENBQWQ7O0FBRUE7Ozs7Ozs7O0FBUUEsS0FBSUMsV0FBVyxJQUFmOztBQUVBOzs7OztBQUtBLEtBQU1DLGlCQUFpQkwsTUFBTU0sSUFBTixDQUFXLE9BQVgsRUFBb0JDLEtBQXBCLEdBQTRCRCxJQUE1QixDQUFpQyxJQUFqQyxDQUF2Qjs7QUFFQTs7Ozs7QUFLQSxLQUFNRSxzQkFBc0JSLE1BQU1NLElBQU4sQ0FBVyxnQkFBWCxDQUE1Qjs7QUFFQTs7Ozs7OztBQU9BLEtBQU1HLGlCQUFpQixtQkFBdkI7O0FBRUE7Ozs7O0FBS0EsS0FBSUMsZ0JBQWdCLEtBQXBCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0EsVUFBU0MscUJBQVQsR0FBaUM7QUFDaEMsTUFBSVQsTUFBTVUsUUFBTixDQUFlLFlBQWYsQ0FBSixFQUFrQztBQUNqQyxVQURpQyxDQUN6QjtBQUNSOztBQUVEUixhQUFXSCxFQUFFLElBQUYsRUFBUUUsUUFBUixDQUFpQixJQUFqQixDQUFYOztBQUVBLE1BQUlDLFNBQVNTLE1BQVQsS0FBb0IsQ0FBeEIsRUFBMkI7QUFDMUI7QUFDQTs7QUFFRFQsV0FBU1UsTUFBVCxHQUFrQlYsU0FBU1UsTUFBVCxHQUFrQkMsR0FBbEIsR0FBd0JYLFNBQVNZLE1BQVQsRUFBMUM7O0FBRUEsTUFBSVosU0FBU1UsTUFBVCxHQUFrQkcsT0FBT0MsV0FBN0IsRUFBMEM7QUFDekNkLFlBQ0VlLEdBREYsQ0FDTSxZQUROLEVBQ29CLENBQUMsQ0FBRCxJQUFNZixTQUFTVSxNQUFULEdBQWtCRyxPQUFPQyxXQUF6QixHQUF1Q2pCLEVBQUUsY0FBRixFQUFrQm1CLFdBQWxCLEVBQXZDLEdBQ3RCbkIsRUFBRSxvQkFBRixFQUF3Qm1CLFdBQXhCLEVBRGdCLElBQ3lCLElBRjdDLEVBR0VDLFFBSEYsQ0FHVyxzQkFIWDtBQUlBO0FBQ0Q7O0FBRUQ7OztBQUdBLFVBQVNDLHFCQUFULEdBQWlDO0FBQ2hDLE1BQUlwQixNQUFNVSxRQUFOLENBQWUsWUFBZixDQUFKLEVBQWtDO0FBQ2pDLFVBRGlDLENBQ3pCO0FBQ1I7O0FBRURSLFdBQ0VlLEdBREYsQ0FDTSxZQUROLEVBQ29CLEVBRHBCLEVBRUVJLFdBRkYsQ0FFYyxzQkFGZDtBQUdBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0MsdUJBQVQsQ0FBaUNoQixtQkFBakMsRUFBc0Q7QUFDckRBLHNCQUFvQmlCLFNBQXBCLENBQThCO0FBQzdCQyxXQUFRLE9BRHFCLEVBQ1o7QUFDakJDLFVBQU8sZUFBU0MsS0FBVCxFQUFnQkMsRUFBaEIsRUFBb0I7QUFDMUJuQixvQkFBZ0IsSUFBaEI7QUFDQW1CLE9BQUdILE1BQUgsQ0FBVUwsUUFBVixDQUFtQixrQkFBbkI7QUFDQVMsNkJBQXlCLElBQXpCO0FBQ0EsSUFONEI7QUFPN0JDLFNBQU0sY0FBU0gsS0FBVCxFQUFnQjtBQUNyQmxCLG9CQUFnQixLQUFoQjtBQUNBVCxNQUFFUSxjQUFGLEVBQWtCdUIsTUFBbEI7QUFDQTtBQVY0QixHQUE5QjtBQVlBOztBQUVEOzs7OztBQUtBLFVBQVNGLHdCQUFULENBQWtDRyxjQUFsQyxFQUFrRDtBQUNqRCxNQUFJQyxjQUFjLEVBQWxCO0FBQ0EsTUFBSUMsU0FBUyxFQUFiOztBQUVBLE1BQUlsQyxFQUFFZ0MsY0FBRixFQUFrQkcsT0FBbEIsQ0FBMEIsSUFBMUIsRUFBZ0M5QixJQUFoQyxDQUFxQyxXQUFyQyxFQUFrRE8sTUFBbEQsS0FBNkQsQ0FBakUsRUFBb0U7QUFDbkVxQjtBQUtBQyxZQUFTLE1BQVQ7QUFDQSxHQVBELE1BT087QUFDTkQ7QUFLQUMsWUFBUyxRQUFUO0FBQ0E7O0FBRURFLHVCQUFxQkgsV0FBckIsRUFBa0NELGNBQWxDOztBQUVBaEMsSUFBRVEsY0FBRixFQUFrQjZCLFNBQWxCLENBQTRCQyxxQkFBcUJKLE1BQXJCLEVBQTZCRixjQUE3QixDQUE1QjtBQUNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTTyxnQkFBVCxDQUEwQkMsT0FBMUIsRUFBbUNSLGNBQW5DLEVBQW1EO0FBQ2xEaEMsSUFBRXlDLElBQUYsQ0FBTztBQUNOQyxRQUFLLDBEQUEwREYsT0FEekQ7QUFFTkcsVUFBTyxlQUFTQSxNQUFULEVBQWdCO0FBQ3RCQyxZQUFRRCxLQUFSLENBQWMscURBQXFESCxPQUFuRTtBQUNBLElBSks7QUFLTkssWUFBUyxtQkFBVztBQUNuQixRQUFJLENBQUNDLHNCQUFzQk4sT0FBdEIsQ0FBTCxFQUFxQztBQUNwQyxTQUFNTyxXQUFXL0MsRUFBRWdDLGNBQUYsRUFBa0JnQixLQUFsQixHQUEwQjVCLFFBQTFCLENBQW1DLGVBQW5DLENBQWpCO0FBQ0EsU0FBTTZCLGVBQWVqRCxFQUFFLE9BQUYsRUFBV2tELE1BQVgsQ0FBa0JILFFBQWxCLENBQXJCO0FBQ0EzQyxvQkFBZThDLE1BQWYsQ0FBc0JELFlBQXRCO0FBQ0ExQiw2QkFBd0IwQixhQUFhNUMsSUFBYixDQUFrQixnQkFBbEIsQ0FBeEI7QUFDQTtBQUNEO0FBWkssR0FBUDtBQWNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTOEMsb0JBQVQsQ0FBOEJYLE9BQTlCLEVBQXVDUixjQUF2QyxFQUF1RDtBQUN0RGhDLElBQUV5QyxJQUFGLENBQU87QUFDTkMsUUFBSyw2REFBNkRGLE9BRDVEO0FBRU5HLFVBQU8sZUFBU0EsT0FBVCxFQUFnQjtBQUN0QkMsWUFBUUQsS0FBUixDQUFjLHVEQUF1REgsT0FBckU7QUFDQSxJQUpLO0FBS05LLFlBQVMsbUJBQVc7QUFDbkI3QyxNQUFFZ0MsY0FBRixFQUFrQm9CLE1BQWxCLENBQXlCLElBQXpCLEVBQStCckIsTUFBL0I7QUFDQTtBQVBLLEdBQVA7QUFTQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNlLHFCQUFULENBQStCTixPQUEvQixFQUF3QztBQUN2QyxTQUFRcEMsZUFBZUMsSUFBZixDQUFvQixNQUFNbUMsT0FBMUIsRUFBbUM1QixNQUFuQyxLQUE4QyxDQUF0RDtBQUNBOztBQUVEOzs7Ozs7OztBQVFBLFVBQVMwQixvQkFBVCxDQUE4QkosTUFBOUIsRUFBc0NGLGNBQXRDLEVBQXNEO0FBQ3JELE1BQU1xQixtQkFBbUI7QUFDeEJDLFdBQVEsZ0JBRGdCO0FBRXhCQyxjQUFXLFNBRmE7QUFHeEI7QUFDQUMsU0FBTSxnQkFBVztBQUNoQnhELE1BQUVRLGNBQUYsRUFBa0JVLEdBQWxCLENBQXNCLFNBQXRCLEVBQWlDLEtBQWpDO0FBQ0EsSUFOdUI7QUFPeEI7QUFDQXVDLFFBQUssZUFBVztBQUNmekQsTUFBRVEsY0FBRixFQUFrQlUsR0FBbEIsQ0FBc0IsU0FBdEIsRUFBaUMsS0FBakM7QUFDQSxJQVZ1QjtBQVd4QjtBQUNBd0MsU0FBTSxjQUFTL0IsS0FBVCxFQUFnQkMsRUFBaEIsRUFBb0I7QUFDekIsUUFBSVksVUFBVXhDLEVBQUU0QixHQUFHSixTQUFMLEVBQWdCbUMsSUFBaEIsQ0FBcUIsSUFBckIsQ0FBZDs7QUFFQSxRQUFJekIsV0FBVyxNQUFmLEVBQXVCO0FBQ3RCSyxzQkFBaUJDLE9BQWpCLEVBQTBCUixjQUExQjtBQUNBLEtBRkQsTUFFTyxJQUFJRSxXQUFXLFFBQWYsRUFBeUI7QUFDL0JpQiwwQkFBcUJYLE9BQXJCLEVBQThCUixjQUE5QjtBQUNBO0FBQ0Q7QUFwQnVCLEdBQXpCOztBQXVCQSxTQUFPcUIsZ0JBQVA7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU2pCLG9CQUFULENBQThCSCxXQUE5QixFQUEyQ0QsY0FBM0MsRUFBMkQ7QUFDMUQsTUFBTTRCLGVBQWU1RCxFQUFFaUMsV0FBRixDQUFyQjs7QUFFQWpDLElBQUVnQyxjQUFGLEVBQWtCb0IsTUFBbEIsQ0FBeUIsSUFBekIsRUFBK0JTLE9BQS9CLENBQXVDRCxZQUF2Qzs7QUFFQSxNQUFNRSxvQkFBb0JGLGFBQWF6QyxXQUFiLEVBQTFCOztBQUVBeUMsZUFBYTFDLEdBQWIsQ0FBaUI7QUFDaEJKLFFBQUtkLEVBQUVnQyxjQUFGLEVBQWtCK0IsUUFBbEIsR0FBNkJqRCxHQUE3QixHQUFvQ2dELG9CQUFvQjtBQUQ3QyxHQUFqQjtBQUdBOztBQUVEOzs7Ozs7QUFNQSxVQUFTRSxzQkFBVCxHQUFrQztBQUNqQyxNQUFNQyx1QkFBdUJqRSxFQUFFa0UsT0FBRixDQUFVbEQsT0FBT21ELFFBQVAsQ0FBZ0JDLE1BQWhCLENBQXVCQyxLQUF2QixDQUE2QixDQUE3QixDQUFWLENBQTdCOztBQUVBcEUsUUFBTUksSUFBTixDQUFXLGtCQUFYLEVBQStCaUUsSUFBL0IsQ0FBb0MsVUFBQ0MsS0FBRCxFQUFRQyxJQUFSLEVBQWlCO0FBQ3BELE9BQU1DLG9CQUFvQnpFLEVBQUVrRSxPQUFGLENBQVVsRSxFQUFFd0UsSUFBRixFQUFRYixJQUFSLENBQWEsTUFBYixFQUFxQmUsT0FBckIsQ0FBNkIsUUFBN0IsRUFBdUMsSUFBdkMsRUFBNkNMLEtBQTdDLENBQW1ELENBQW5ELENBQVYsQ0FBMUI7O0FBRUEsT0FBSUksa0JBQWtCRSxFQUFsQixLQUF5QlYscUJBQXFCVSxFQUFsRCxFQUFzRDtBQUNyRDNFLE1BQUV3RSxJQUFGLEVBQVFyQyxPQUFSLENBQWdCLFVBQWhCLEVBQTRCZixRQUE1QixDQUFxQyxRQUFyQztBQUNBLFdBQU8sS0FBUDtBQUNBO0FBQ0QsR0FQRDs7QUFTQSxNQUFJbkIsTUFBTUksSUFBTixDQUFXLFNBQVgsRUFBc0JPLE1BQTFCLEVBQWtDO0FBQ2pDO0FBQ0E7O0FBRUQ7QUFDQVosSUFBRSw2Q0FBRixFQUFpRHNFLElBQWpELENBQXNELFVBQUNDLEtBQUQsRUFBUUssT0FBUixFQUFvQjtBQUN6RSxPQUFNQyxXQUFXN0UsRUFBRTRFLE9BQUYsQ0FBakI7QUFDQSxPQUFNRSxhQUFhRCxTQUFTeEUsSUFBVCxDQUFjLEdBQWQsRUFBbUJzRCxJQUFuQixDQUF3QixNQUF4QixDQUFuQjs7QUFFQTFELFNBQU1JLElBQU4sQ0FBVyxZQUFYLEVBQXlCaUUsSUFBekIsQ0FBOEIsVUFBQ0MsS0FBRCxFQUFRQyxJQUFSLEVBQWlCO0FBQzlDLFFBQU1PLFFBQVEvRSxFQUFFd0UsSUFBRixDQUFkO0FBQ0EsUUFBTVEsVUFBVUQsTUFBTXBCLElBQU4sQ0FBVyxNQUFYLEVBQW1Cc0IsS0FBbkIsQ0FBeUIsR0FBekIsRUFBOEJDLEdBQTlCLEVBQWhCOztBQUVBLFFBQUlGLFlBQVlGLFVBQWhCLEVBQTRCO0FBQzNCQyxXQUFNNUMsT0FBTixDQUFjLFVBQWQsRUFBMEJmLFFBQTFCLENBQW1DLFFBQW5DO0FBQ0EsWUFBTyxLQUFQO0FBQ0E7QUFDRCxJQVJEO0FBU0EsR0FiRDs7QUFlQSxNQUFJbkIsTUFBTUksSUFBTixDQUFXLFNBQVgsRUFBc0JPLE1BQTFCLEVBQWtDO0FBQ2pDO0FBQ0E7O0FBRUQ7QUFDQSxNQUFNdUUsd0JBQXdCQyxtQkFBbUJuQixxQkFBcUJVLEVBQXhDLENBQTlCOztBQUVBMUUsUUFBTUksSUFBTixDQUFXLGtCQUFYLEVBQStCaUUsSUFBL0IsQ0FBb0MsVUFBQ0MsS0FBRCxFQUFRQyxJQUFSLEVBQWlCO0FBQ3BELE9BQU1DLG9CQUFvQnpFLEVBQUVrRSxPQUFGLENBQVVsRSxFQUFFd0UsSUFBRixFQUFRYixJQUFSLENBQWEsTUFBYixFQUFxQmUsT0FBckIsQ0FBNkIsUUFBN0IsRUFBdUMsSUFBdkMsRUFBNkNMLEtBQTdDLENBQW1ELENBQW5ELENBQVYsQ0FBMUI7QUFDQSxPQUFNZ0IscUJBQXFCRCxtQkFBbUJYLGtCQUFrQkUsRUFBckMsQ0FBM0I7O0FBRUEsT0FBSVUsdUJBQXVCRixxQkFBM0IsRUFBa0Q7QUFDakRuRixNQUFFd0UsSUFBRixFQUFRckMsT0FBUixDQUFnQixVQUFoQixFQUE0QmYsUUFBNUIsQ0FBcUMsUUFBckM7QUFDQSxXQUFPLEtBQVA7QUFDQTtBQUNELEdBUkQ7QUFTQTs7QUFFRDs7Ozs7QUFLQSxVQUFTa0UsaUJBQVQsR0FBNkI7QUFDNUI7QUFDQSxNQUFNQyxNQUFNM0YsSUFBSTRGLElBQUosQ0FBU0MsU0FBckI7O0FBRUE7QUFDQSxNQUFNQyxPQUFPLHVCQUFiOztBQUVBO0FBQ0EsTUFBTUMsV0FBVyxTQUFYQSxRQUFXLFFBQVM7QUFDekI7QUFDQSxPQUFNbkIsT0FBT3BFLGVBQ1hGLFFBRFcsR0FFWDBGLEVBRlcsQ0FFUnJCLEtBRlEsRUFHWGxFLElBSFcsQ0FHTixHQUhNLEVBSVhzRCxJQUpXLENBSU4sTUFKTSxDQUFiOztBQU1BO0FBQ0EzQyxVQUFPNkUsSUFBUCxDQUFZckIsSUFBWixFQUFrQixPQUFsQjtBQUNBLEdBVkQ7O0FBWUE7QUFDQSxNQUFNc0IsZUFBZWxHLElBQUk0RixJQUFKLENBQVNDLFNBQVQsQ0FBbUJNLFNBQW5CLENBQTZCQyxLQUFsRDs7QUFyQjRCLDZCQXVCbkJDLFNBdkJtQjtBQXdCM0I7QUFDQSxPQUFNQyxjQUFjLENBQUNYLElBQUlRLFNBQUosQ0FBY0ksTUFBZixFQUF1QlosSUFBSVEsU0FBSixDQUFjSyxPQUFyQyxFQUE4Q04sZUFBZUcsU0FBN0QsQ0FBcEI7O0FBRUE7QUFDQVYsT0FBSWMsZ0JBQUosQ0FBcUJYLE9BQU9PLFNBQTVCLEVBQXVDQyxXQUF2QyxFQUFvRDtBQUFBLFdBQU1QLFNBQVNNLFNBQVQsQ0FBTjtBQUFBLElBQXBEO0FBNUIyQjs7QUF1QjVCLE9BQUssSUFBSUEsWUFBWSxDQUFyQixFQUF3QkEsWUFBWSxDQUFwQyxFQUF1Q0EsV0FBdkMsRUFBb0Q7QUFBQSxTQUEzQ0EsU0FBMkM7QUFNbkQ7QUFDRDs7QUFFRDs7Ozs7Ozs7O0FBU0EsVUFBU2Isa0JBQVQsQ0FBNEJaLElBQTVCLEVBQWtDO0FBQ2pDLFNBQU9BLFFBQVFBLEtBQUs4QixPQUFMLENBQWEsR0FBYixNQUFzQixDQUFDLENBQS9CLEdBQW1DOUIsS0FBS1MsS0FBTCxDQUFXLEdBQVgsRUFBZ0IsQ0FBaEIsQ0FBbkMsR0FBd0RULElBQS9EO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBN0UsUUFBTzRHLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJ2RyxRQUFNQyxRQUFOLENBQWUsSUFBZixFQUNFdUcsRUFERixDQUNLLFlBREwsRUFDbUIvRixxQkFEbkIsRUFFRStGLEVBRkYsQ0FFSyxZQUZMLEVBRW1CcEYscUJBRm5COztBQUlBRSwwQkFBd0JoQixtQkFBeEI7QUFDQXlEO0FBQ0FzQjs7QUFFQWtCO0FBQ0EsRUFWRDs7QUFZQSxRQUFPN0csTUFBUDtBQUNBLENBalpGIiwiZmlsZSI6ImxheW91dHMvbWFpbi9tZW51L21lbnUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIG1lbnUuanMgMjAxNy0wNS0zMFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogTWFpbiBNZW51IENvbnRyb2xsZXJcbiAqXG4gKiBXaGVuZXZlciB0aGUgdXNlciBjbGlja3Mgb24gYSBtZW51IGl0ZW0gdGhlIGJyb3dzZXIgbXVzdCBiZSByZWRpcmVjdGVkIHRvIHRoZSByZXNwZWN0aXZlIGxvY2F0aW9uLlxuICpcbiAqIE1pZGRsZSBidXR0b24gY2xpY2tzIGFyZSBhbHNvIHN1cHBvcnRlZC5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnbWVudScsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS11aS9qcXVlcnktdWkubWluLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS11aS9qcXVlcnktdWkubWluLmpzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LWRlcGFyYW0vanF1ZXJ5LWRlcGFyYW0ubWluLmpzYCxcblx0XHQndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnLCBcblx0XHRgJHtneC5zb3VyY2V9L2xpYnMvc2hvcnRjdXRzYFxuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNZW51IExpc3QgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgJGxpc3QgPSAkdGhpcy5jaGlsZHJlbigndWwnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTdWItbGlzdCBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogVGhpcyBvYmplY3QgaXMgdXNlZCBmb3IgY29ycmVjdGx5IGRpc3BsYXlpbmcgdGhlIHN1Yi1saXN0IDx1bD4gZWxlbWVudCB3aGVuZXZlciBpdCBnb2VzIG91dFxuXHRcdCAqIG9mIHZpZXdwb3J0LlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRsZXQgJHN1Ymxpc3QgPSBudWxsO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZhdm9yaXRlcyBCb3hcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgJGZhdm9yaXRlc01lbnUgPSAkdGhpcy5maW5kKCd1bCBsaScpLmZpcnN0KCkuZmluZCgndWwnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEcmFnZ2FibGUgTWVudSBJdGVtc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCAkZHJhZ2dhYmxlTWVudUl0ZW1zID0gJHRoaXMuZmluZCgnLmZhdi1kcmFnLWl0ZW0nKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEcm9wem9uZSBCb3hcblx0XHQgKlxuXHRcdCAqIFRoZSBkcmFnZ2FibGUgZWxlbWVudHMgd2lsbCBiZSBwbGFjZWQgaGVyZS5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZmF2RHJvcHpvbmVCb3ggPSAnI2Zhdi1kcm9wem9uZS1ib3gnO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERyYWcgYW5kIGRyb3AgZmxhZyB0byBwcmV2ZW50IHRoZSBkZWZhdWx0IGFjdGlvbiBvZiBhIG1lbnUgaXRlbSB3aGVuIGl0IGlzIGRyYWdnZWQuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7Qm9vbGVhbn0gVHJ1ZSB3aGlsZSBhbSBpdGVtIGlzIGRyYWdnZWQuXG5cdFx0ICovXG5cdFx0bGV0IG9uRHJhZ0FuZERyb3AgPSBmYWxzZTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXBvc2l0aW9ucyB0aGUgc3ViLWxpc3Qgd2hlbiBpdCBnb2VzIG9mZiB0aGUgdmlld3BvcnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uTWVudUl0ZW1Nb3VzZUVudGVyKCkge1xuXHRcdFx0aWYgKCRsaXN0Lmhhc0NsYXNzKCdleHBhbmQtYWxsJykpIHtcblx0XHRcdFx0cmV0dXJuOyAvLyBEbyBub3QgY2hlY2sgZm9yIHZpZXdwb3J0IGluIFwiZXhwYW5kLWFsbFwiIHN0YXRlLlxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkc3VibGlzdCA9ICQodGhpcykuY2hpbGRyZW4oJ3VsJyk7XG5cdFx0XHRcblx0XHRcdGlmICgkc3VibGlzdC5sZW5ndGggPT09IDApIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkc3VibGlzdC5vZmZzZXQgPSAkc3VibGlzdC5vZmZzZXQoKS50b3AgKyAkc3VibGlzdC5oZWlnaHQoKTtcblx0XHRcdFxuXHRcdFx0aWYgKCRzdWJsaXN0Lm9mZnNldCA+IHdpbmRvdy5pbm5lckhlaWdodCkge1xuXHRcdFx0XHQkc3VibGlzdFxuXHRcdFx0XHRcdC5jc3MoJ21hcmdpbi10b3AnLCAtMSAqICgkc3VibGlzdC5vZmZzZXQgLSB3aW5kb3cuaW5uZXJIZWlnaHQgKyAkKCcjbWFpbi1oZWFkZXInKS5vdXRlckhlaWdodCgpXG5cdFx0XHRcdFx0XHQrICQoJyNtYWluLWZvb3RlciAuaW5mbycpLm91dGVySGVpZ2h0KCkpICsgJ3B4Jylcblx0XHRcdFx0XHQuYWRkQ2xhc3MoJ3N0YXktd2l0aGluLXZpZXdwb3J0Jyk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlbW92ZSB0aGUgaGVscGVyIGNsYXNzIFwic3RheS13aXRoaW4tdmlld3BvcnRcIiBhbmQgcmVzZXQgdGhlIFwibWFyZ2luLXRvcFwiIHZhbHVlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbk1lbnVJdGVtTW91c2VMZWF2ZSgpIHtcblx0XHRcdGlmICgkbGlzdC5oYXNDbGFzcygnZXhwYW5kLWFsbCcpKSB7XG5cdFx0XHRcdHJldHVybjsgLy8gRG8gbm90IGNoZWNrIGZvciB2aWV3cG9ydCBpbiBcImV4cGFuZC1hbGxcIiBzdGF0ZS5cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JHN1Ymxpc3Rcblx0XHRcdFx0LmNzcygnbWFyZ2luLXRvcCcsICcnKVxuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoJ3N0YXktd2l0aGluLXZpZXdwb3J0Jyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1ha2VzIGFsbCBtZW51IGl0ZW1zIGRyYWdnYWJsZS5cblx0XHQgKlxuXHRcdCAqIFRoaXMgZnVuY3Rpb24gc2hvdWxkIGJlIGV4ZWN1dGVkIGluIHRoZSBtb2R1bGUuaW5pdCgpIG1ldGhvZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSAkZHJhZ2dhYmxlTWVudUl0ZW1zIE1lbnUgaXRlbSBqUXVlcnkgc2VsZWN0b3IuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX21ha2VNZW51SXRlbXNEcmFnZ2FibGUoJGRyYWdnYWJsZU1lbnVJdGVtcykge1xuXHRcdFx0JGRyYWdnYWJsZU1lbnVJdGVtcy5kcmFnZ2FibGUoe1xuXHRcdFx0XHRoZWxwZXI6ICdjbG9uZScsIC8vIENsb25lIHRoZSBlbGVtZW50LCBkb24ndCBtb3ZlIHRoZSBlbGVtZW50IGl0c2VsZi5cblx0XHRcdFx0c3RhcnQ6IGZ1bmN0aW9uKGV2ZW50LCB1aSkge1xuXHRcdFx0XHRcdG9uRHJhZ0FuZERyb3AgPSB0cnVlO1xuXHRcdFx0XHRcdHVpLmhlbHBlci5hZGRDbGFzcygnY3VycmVudGx5RHJhZ2dlZCcpO1xuXHRcdFx0XHRcdF9jcmVhdGVGYXZvcml0ZXNEcm9wem9uZSh0aGlzKTtcblx0XHRcdFx0fSxcblx0XHRcdFx0c3RvcDogZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0XHRvbkRyYWdBbmREcm9wID0gZmFsc2U7XG5cdFx0XHRcdFx0JChmYXZEcm9wem9uZUJveCkucmVtb3ZlKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDcmVhdGVzIHRoZSBmYXZvcml0ZXMgYm94LCB3aGVyZSB0aGUgZHJhZ2dhYmxlIGl0ZW1zIGNhbiBiZSBkcm9wcGVkIG9uLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZHJhZ2dlZEVsZW1lbnQgRHJhZ2dlZCBtZW51IGl0ZW0uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2NyZWF0ZUZhdm9yaXRlc0Ryb3B6b25lKGRyYWdnZWRFbGVtZW50KSB7XG5cdFx0XHRsZXQgZHJvcHpvbmVCb3ggPSAnJztcblx0XHRcdGxldCBhY3Rpb24gPSAnJztcblx0XHRcdFxuXHRcdFx0aWYgKCQoZHJhZ2dlZEVsZW1lbnQpLnBhcmVudHMoJ2xpJykuZmluZCgnLmZhLWhlYXJ0JykubGVuZ3RoID09PSAwKSB7XG5cdFx0XHRcdGRyb3B6b25lQm94ID0gYFxuXHRcdFx0XHQ8ZGl2IGlkPVwiZmF2LWRyb3B6b25lLWJveFwiIGNsYXNzPVwiZmF2LWFkZFwiPlxuXHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtaGVhcnRcIj48L2k+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0YDtcblx0XHRcdFx0YWN0aW9uID0gJ3NhdmUnO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0ZHJvcHpvbmVCb3ggPSBgXG5cdFx0XHRcdDxkaXYgaWQ9XCJmYXYtZHJvcHpvbmUtYm94XCIgY2xhc3M9XCJmYXYtZGVsZXRlXCI+XG5cdFx0XHRcdFx0PGkgY2xhc3M9XCJmYSBmYS10cmFzaFwiPjwvaT5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHRgO1xuXHRcdFx0XHRhY3Rpb24gPSAnZGVsZXRlJztcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0X3Bvc2l0aW9uRHJvcHpvbmVCb3goZHJvcHpvbmVCb3gsIGRyYWdnZWRFbGVtZW50KTtcblx0XHRcdFxuXHRcdFx0JChmYXZEcm9wem9uZUJveCkuZHJvcHBhYmxlKF9nZXRPYmplY3RGcm9tQWN0aW9uKGFjdGlvbiwgZHJhZ2dlZEVsZW1lbnQpKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU3RvcmVzIHRoZSBtZW51IGl0ZW0gYXMgYSBmYXZvcml0ZSBpbiB0aGUgZGF0YWJhc2UuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gbGlua0tleSBVbmlxdWUgbGluayBrZXkgZnJvbSB0aGUgbWVudSBpdGVtLlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBkcmFnZ2VkRWxlbWVudCBEcmFnZ2VkIG1lbnUgaXRlbS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2F2ZVRvRmF2b3JpdGVzKGxpbmtLZXksIGRyYWdnZWRFbGVtZW50KSB7XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR1cmw6ICdhZG1pbi5waHA/ZG89QWRtaW5GYXZvcml0ZXNBamF4L0FkZE1lbnVJdGVtJmxpbmtfa2V5PScgKyBsaW5rS2V5LFxuXHRcdFx0XHRlcnJvcjogZnVuY3Rpb24oZXJyb3IpIHtcblx0XHRcdFx0XHRjb25zb2xlLmVycm9yKCdDb3VsZCBub3Qgc2F2ZSB0aGUgbWVudSBpdGVtIHdpdGggdGhlIGxpbmsga2V5OiAnICsgbGlua0tleSk7XG5cdFx0XHRcdH0sXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdGlmICghX2lzTGlua0tleUluRmF2b3JpdGVzKGxpbmtLZXkpKSB7XG5cdFx0XHRcdFx0XHRjb25zdCAkbmV3TGluayA9ICQoZHJhZ2dlZEVsZW1lbnQpLmNsb25lKCkuYWRkQ2xhc3MoJ2Zhdi1kcmFnLWl0ZW0nKTtcblx0XHRcdFx0XHRcdGNvbnN0ICRuZXdMaXN0SXRlbSA9ICQoJzxsaS8+JykuYXBwZW5kKCRuZXdMaW5rKTtcblx0XHRcdFx0XHRcdCRmYXZvcml0ZXNNZW51LmFwcGVuZCgkbmV3TGlzdEl0ZW0pO1xuXHRcdFx0XHRcdFx0X21ha2VNZW51SXRlbXNEcmFnZ2FibGUoJG5ld0xpc3RJdGVtLmZpbmQoJy5mYXYtZHJhZy1pdGVtJykpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlbGV0ZXMgdGhlIG1lbnUgaXRlbSBhcyBhIGZhdm9yaXRlIGZyb20gdGhlIGRhdGFiYXNlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGxpbmtLZXkgVW5pcXVlIGxpbmsga2V5IGZyb20gdGhlIG1lbnUgaXRlbS5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gZHJhZ2dlZEVsZW1lbnQgRHJhZ2dlZCBtZW51IGl0ZW0uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2RlbGV0ZUZyb21GYXZvcml0ZXMobGlua0tleSwgZHJhZ2dlZEVsZW1lbnQpIHtcblx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdHVybDogJ2FkbWluLnBocD9kbz1BZG1pbkZhdm9yaXRlc0FqYXgvUmVtb3ZlTWVudUl0ZW0mbGlua19rZXk9JyArIGxpbmtLZXksXG5cdFx0XHRcdGVycm9yOiBmdW5jdGlvbihlcnJvcikge1xuXHRcdFx0XHRcdGNvbnNvbGUuZXJyb3IoJ0NvdWxkIG5vdCByZW1vdmUgdGhlIG1lbnUgaXRlbSB3aXRoIHRoZSBsaW5rIGtleTogJyArIGxpbmtLZXkpO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRzdWNjZXNzOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHQkKGRyYWdnZWRFbGVtZW50KS5wYXJlbnQoJ2xpJykucmVtb3ZlKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDaGVja3MgaWYgYSBtZW51IGl0ZW0gaXMgYWxyZWFkeSBzdG9yZWQgaW4gdGhlIGZhdm9yaXRlcyBtZW51LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGxpbmtLZXkgVW5pcXVlIGxpbmsga2V5IG9mIGEgbWVudSBpdGVtLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7Qm9vbGVhbn0gVHJ1ZSBpZiBtZW51IGl0ZW0gaXMgYWxyZWFkeSBzdG9yZWQsIGVsc2UgZmFsc2Ugd2lsbCBiZSByZXR1cm5lZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfaXNMaW5rS2V5SW5GYXZvcml0ZXMobGlua0tleSkge1xuXHRcdFx0cmV0dXJuICgkZmF2b3JpdGVzTWVudS5maW5kKCcjJyArIGxpbmtLZXkpLmxlbmd0aCAhPT0gMCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdldCBqUXVlcnlVSSBkcm9wcGFibGUgb3B0aW9ucyBvYmplY3Rcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSBhY3Rpb24gQWN0aW9uIHRvIGV4ZWN1dGUgdmFsdWU9c2F2ZXxkZWxldGUuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGRyYWdnZWRFbGVtZW50IERyYWdnZWQgbWV1IGl0ZW0uXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPYmplY3R9IGpRdWVyeVVJIGRyb3BwYWJsZSBvcHRpb25zLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRPYmplY3RGcm9tQWN0aW9uKGFjdGlvbiwgZHJhZ2dlZEVsZW1lbnQpIHtcblx0XHRcdGNvbnN0IGRyb3BwYWJsZU9wdGlvbnMgPSB7XG5cdFx0XHRcdGFjY2VwdDogJy5mYXYtZHJhZy1pdGVtJyxcblx0XHRcdFx0dG9sZXJhbmNlOiAncG9pbnRlcicsXG5cdFx0XHRcdC8vIEZ1bmN0aW9uIHdoZW4gaG92ZXJpbmcgb3ZlciB0aGUgZmF2b3JpdGVzIGJveC5cblx0XHRcdFx0b3ZlcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0JChmYXZEcm9wem9uZUJveCkuY3NzKCdvcGFjaXR5JywgJzEuMCcpO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHQvLyBGdW5jdGlvbiB3aGVuIGhvdmVyaW5nIG91dCBmcm9tIHRoZSBmYXZvcml0ZXMgYm94LlxuXHRcdFx0XHRvdXQ6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQoZmF2RHJvcHpvbmVCb3gpLmNzcygnb3BhY2l0eScsICcwLjknKTtcblx0XHRcdFx0fSxcblx0XHRcdFx0Ly8gRnVuY3Rpb24gd2hlbiBkcm9wcGluZyBhbiBlbGVtZW50IG9uIHRoZSBmYXZvcml0ZXMgYm94LlxuXHRcdFx0XHRkcm9wOiBmdW5jdGlvbihldmVudCwgdWkpIHtcblx0XHRcdFx0XHRsZXQgbGlua0tleSA9ICQodWkuZHJhZ2dhYmxlKS5hdHRyKCdpZCcpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmIChhY3Rpb24gPT09ICdzYXZlJykge1xuXHRcdFx0XHRcdFx0X3NhdmVUb0Zhdm9yaXRlcyhsaW5rS2V5LCBkcmFnZ2VkRWxlbWVudCk7XG5cdFx0XHRcdFx0fSBlbHNlIGlmIChhY3Rpb24gPT09ICdkZWxldGUnKSB7XG5cdFx0XHRcdFx0XHRfZGVsZXRlRnJvbUZhdm9yaXRlcyhsaW5rS2V5LCBkcmFnZ2VkRWxlbWVudCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gZHJvcHBhYmxlT3B0aW9ucztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUG9zaXRpb25zIHRoZSBEcm9wem9uZUJveCBhdCB0aGUgY29ycmVjdCBwbGFjZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSBkcm9wem9uZUJveCBEcm9wem9uZUJveCBIVE1MLlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBkcmFnZ2VkRWxlbWVudCBEcmFnZ2VkIG1lbnUgaXRlbS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfcG9zaXRpb25Ecm9wem9uZUJveChkcm9wem9uZUJveCwgZHJhZ2dlZEVsZW1lbnQpIHtcblx0XHRcdGNvbnN0ICRkcm9wem9uZUJveCA9ICQoZHJvcHpvbmVCb3gpO1xuXHRcdFx0XG5cdFx0XHQkKGRyYWdnZWRFbGVtZW50KS5wYXJlbnQoJ2xpJykucHJlcGVuZCgkZHJvcHpvbmVCb3gpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCBkcm9wem9uZUJveEhlaWdodCA9ICRkcm9wem9uZUJveC5vdXRlckhlaWdodCgpO1xuXHRcdFx0XG5cdFx0XHQkZHJvcHpvbmVCb3guY3NzKHtcblx0XHRcdFx0dG9wOiAkKGRyYWdnZWRFbGVtZW50KS5wb3NpdGlvbigpLnRvcCAtIChkcm9wem9uZUJveEhlaWdodCAvIDIpXG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogT3BlbiB0aGUgYWN0aXZlIG1lbnUgZ3JvdXAuXG5cdFx0ICpcblx0XHQgKiBUaGlzIG1ldGhvZCB3aWxsIGZpbmQgdGhlIG1lbnUgaXRlbSB0aGF0IGNvbnRhaW5zIHRoZSBzYW1lIFwiZG9cIiBHRVQgcGFyYW1ldGVyIGFuZCBzZXQgdGhlIFwiYWN0aXZlXCJcblx0XHQgKiBjbGFzcyB0byBpdHMgcGFyZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF90b2dnbGVBY3RpdmVNZW51R3JvdXAoKSB7XG5cdFx0XHRjb25zdCBjdXJyZW50VXJsUGFyYW1ldGVycyA9ICQuZGVwYXJhbSh3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNsaWNlKDEpKTtcblx0XHRcdFxuXHRcdFx0JGxpc3QuZmluZCgnbGk6Z3QoMCkgdWwgbGkgYScpLmVhY2goKGluZGV4LCBsaW5rKSA9PiB7XG5cdFx0XHRcdGNvbnN0IGxpbmtVcmxQYXJhbWV0ZXJzID0gJC5kZXBhcmFtKCQobGluaykuYXR0cignaHJlZicpLnJlcGxhY2UoLy4qKFxcPykvLCAnJDEnKS5zbGljZSgxKSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAobGlua1VybFBhcmFtZXRlcnMuZG8gPT09IGN1cnJlbnRVcmxQYXJhbWV0ZXJzLmRvKSB7XG5cdFx0XHRcdFx0JChsaW5rKS5wYXJlbnRzKCdsaTpsdCgyKScpLmFkZENsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRpZiAoJGxpc3QuZmluZCgnLmFjdGl2ZScpLmxlbmd0aCkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIElmIG5vIG1hdGNoIHdhcyBmb3VuZCwgY2hlY2sgZm9yIHN1Yi1wYWdlIGxpbmtzIGluIGNvbnRlbnQgbmF2aWdhdGlvbi5cblx0XHRcdCQoJyNtYWluLWNvbnRlbnQgLmNvbnRlbnQtbmF2aWdhdGlvbiAubmF2LWl0ZW0nKS5lYWNoKChpbmRleCwgbmF2SXRlbSkgPT4ge1xuXHRcdFx0XHRjb25zdCAkbmF2SXRlbSA9ICQobmF2SXRlbSk7IFxuXHRcdFx0XHRjb25zdCBuYXZJdGVtVXJsID0gJG5hdkl0ZW0uZmluZCgnYScpLmF0dHIoJ2hyZWYnKTtcblx0XHRcdFx0XG5cdFx0XHRcdCRsaXN0LmZpbmQoJ2xpOmd0KDEpIGEnKS5lYWNoKChpbmRleCwgbGluaykgPT4ge1xuXHRcdFx0XHRcdGNvbnN0ICRsaW5rID0gJChsaW5rKTtcblx0XHRcdFx0XHRjb25zdCBsaW5rVXJsID0gJGxpbmsuYXR0cignaHJlZicpLnNwbGl0KCcvJykucG9wKCk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKGxpbmtVcmwgPT09IG5hdkl0ZW1VcmwpIHtcblx0XHRcdFx0XHRcdCRsaW5rLnBhcmVudHMoJ2xpOmx0KDIpJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0aWYgKCRsaXN0LmZpbmQoJy5hY3RpdmUnKS5sZW5ndGgpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBJZiBubyBtYXRjaCB3YXMgZm91bmQsIG9ubHkgY2hlY2sgZm9yIHRoZSBjb250cm9sbGVyIG5hbWUuXG5cdFx0XHRjb25zdCBjdXJyZW50Q29udHJvbGxlck5hbWUgPSBfZ2V0Q29udHJvbGxlck5hbWUoY3VycmVudFVybFBhcmFtZXRlcnMuZG8pOyBcblx0XHRcdFxuXHRcdFx0JGxpc3QuZmluZCgnbGk6Z3QoMCkgdWwgbGkgYScpLmVhY2goKGluZGV4LCBsaW5rKSA9PiB7XG5cdFx0XHRcdGNvbnN0IGxpbmtVcmxQYXJhbWV0ZXJzID0gJC5kZXBhcmFtKCQobGluaykuYXR0cignaHJlZicpLnJlcGxhY2UoLy4qKFxcPykvLCAnJDEnKS5zbGljZSgxKSk7XG5cdFx0XHRcdGNvbnN0IGxpbmtDb250cm9sbGVyTmFtZSA9IF9nZXRDb250cm9sbGVyTmFtZShsaW5rVXJsUGFyYW1ldGVycy5kbyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAobGlua0NvbnRyb2xsZXJOYW1lID09PSBjdXJyZW50Q29udHJvbGxlck5hbWUpIHtcblx0XHRcdFx0XHQkKGxpbmspLnBhcmVudHMoJ2xpOmx0KDIpJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlZ2lzdGVycyBhIHNob3J0Y3V0IHRvIG9wZW4gdGhlIGZhdm9yaXRlIGVudHJpZXMuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9yZWdpc3RlclNob3J0Y3V0KCkge1xuXHRcdFx0Ly8gU2hvcnRjdXQgbGlicmFyeSBhYmJyZXZpYXRpb24uXG5cdFx0XHRjb25zdCBsaWIgPSBqc2UubGlicy5zaG9ydGN1dHM7XG5cdFx0XHRcblx0XHRcdC8vIENvbWJpbmF0aW9uIG5hbWUuXG5cdFx0XHRjb25zdCBuYW1lID0gJ29wZW5GYXZvcml0ZU1lbnVFbnRyeSc7XG5cdFx0XHRcblx0XHRcdC8vIENhbGxiYWNrIGZ1bmN0aW9uLlxuXHRcdFx0Y29uc3QgY2FsbGJhY2sgPSBpbmRleCA9PiB7XG5cdFx0XHRcdC8vIEdldCBsaW5rIGZyb20gZmF2b3JpdGUgZW50cnkuXG5cdFx0XHRcdGNvbnN0IGxpbmsgPSAkZmF2b3JpdGVzTWVudVxuXHRcdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdFx0LmVxKGluZGV4KVxuXHRcdFx0XHRcdC5maW5kKCdhJylcblx0XHRcdFx0XHQuYXR0cignaHJlZicpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gT3BlbiBsaW5rIGluIHNhbWUgd2luZG93LlxuXHRcdFx0XHR3aW5kb3cub3BlbihsaW5rLCAnX3NlbGYnKTtcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIEl0ZXJhdGluZyBvdmVyIGVhY2ggYWJvdmUgbnVtYmVyIGtleSBzdGFydGluZyB3aXRoICcxJyB1bnRpbCAnOScuXG5cdFx0XHRjb25zdCBmaXJzdEtleUNvZGUgPSBqc2UubGlicy5zaG9ydGN1dHMuS0VZX0NPREVTLk5VTV8xO1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBpdGVyYXRpb24gPSAwOyBpdGVyYXRpb24gPCA5OyBpdGVyYXRpb24rKykge1xuXHRcdFx0XHQvLyBLZXkgY29tYmluYXRpb24gKENUUkwgKyBTSElGVCArIGtleUNvZGUpLlxuXHRcdFx0XHRjb25zdCBjb21iaW5hdGlvbiA9IFtsaWIuS0VZX0NPREVTLkNUUkxfTCwgbGliLktFWV9DT0RFUy5TSElGVF9MLCBmaXJzdEtleUNvZGUgKyBpdGVyYXRpb25dO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUmVnaXN0ZXIgc2hvcnRjdXQuXG5cdFx0XHRcdGxpYi5yZWdpc3RlclNob3J0Y3V0KG5hbWUgKyBpdGVyYXRpb24sIGNvbWJpbmF0aW9uLCAoKSA9PiBjYWxsYmFjayhpdGVyYXRpb24pKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyBvbmx5IHRoZSBQSFAgY29udHJvbGxlciB2YWx1ZSBmcm9tIHRoZSBwcm92aWRlZCBsaW5rLlxuXHRcdCAqXG5cdFx0ICogRXhhbXBsZTpcblx0XHQgKiAgIElucHV0OiBHb29nbGVTaG9wcGluZy9lZGl0U2NoZW1lXG5cdFx0ICogICBPdXRwdXQ6IEdvb2dsZVNob3BwaW5nXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0gbGluayBkbyBwYXJhbWV0ZXIgZnJvbSB0aGUgZmV0Y2hlZCB3aW5kb3cubG9jYXRpb25cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0Q29udHJvbGxlck5hbWUobGluaykge1xuXHRcdFx0cmV0dXJuIGxpbmsgJiYgbGluay5pbmRleE9mKCcvJykgIT09IC0xID8gbGluay5zcGxpdCgnLycpWzBdIDogbGluaztcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCRsaXN0LmNoaWxkcmVuKCdsaScpXG5cdFx0XHRcdC5vbignbW91c2VlbnRlcicsIF9vbk1lbnVJdGVtTW91c2VFbnRlcilcblx0XHRcdFx0Lm9uKCdtb3VzZWxlYXZlJywgX29uTWVudUl0ZW1Nb3VzZUxlYXZlKTtcblx0XHRcdFxuXHRcdFx0X21ha2VNZW51SXRlbXNEcmFnZ2FibGUoJGRyYWdnYWJsZU1lbnVJdGVtcyk7XG5cdFx0XHRfdG9nZ2xlQWN0aXZlTWVudUdyb3VwKCk7XG5cdFx0XHRfcmVnaXN0ZXJTaG9ydGN1dCgpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTsiXX0=
