'use strict';

/* --------------------------------------------------------------
 slides_container.js 2016-12-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Slides Container (Tabs)
 *
 * Handles the sliders container functionality in the sliders details page.
 */
gx.controllers.module('slides_container', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js', jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.min.js', 'xhr', 'modal'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);
	var $footer = $('#main-footer');

	/**
  * Elements
  *
  * @type {Object}
  */
	var elements = {
		// Buttons.
		buttons: {
			// Sort mode button.
			sort: $this.find('.sort-button'),

			// Create button.
			create: $this.find('.btn-create'),

			// Submit button group.
			submit: $footer.find('.submit-button-group'),

			// Submit button for save slider
			submitSave: $footer.find('.save'),

			// Submit button for save and refresh slider
			submitRefresh: $footer.find('.refresh')
		},

		// Template.
		templates: {
			// Slide panel set template.
			slidePanel: $this.find('#slide-panel-template')
		},

		// Modals.
		modals: {
			// Delete image modal.
			deleteImage: $('.delete-image.modal'),

			// Delete slide modal.
			deleteSlide: $('.delete-slide.modal'),

			// Edit image map modal.
			imageMap: $('.image-map.modal')
		},

		// Tabs.
		tabHeader: $this.find('.nav-tabs'),

		// Select box which holds all images that will be deleted.
		deleteImageSelect: $('#delete_images')
	};

	/**
  * CSS class names.
  *
  * @type {Object}
  */
	var classes = {
		// New image.
		newImage: 'new'
	};

	/**
  * Selector Strings
  *
  * @type {Object}
  */
	var selectors = {
		// Icon selector strings.
		icons: {
			// Delete button on the panel header.
			delete: '.icon.delete',

			// Drag button on the panel header.
			drag: '.drag-handle',

			// Collapser button on the panel header.
			collapser: '.collapser',

			// Image delete button.
			imageDelete: '.action-icon.delete',

			// Image map edit button.
			imageMap: '.action-icon.image-map',

			// Upload image button.
			upload: '.action-icon.upload'
		},

		// Inputs selector strings.
		inputs: {
			// General image select dropdowns.
			dropdown: '.dropdown-input',

			// Thumbnail dropdown.
			thumbnailImageDropdown: '[name="thumbnail"]',

			// Title.
			title: 'input[name="title"]',

			// File.
			file: '.file-input'
		},

		// Slide panel.
		slidePanel: '.panel',

		// Tab body.
		tabBody: '.tab-pane',

		// Slide panel title.
		slidePanelTitle: '.slide-title',

		// Setting row (form group).
		configurationRow: '.row.form-group',

		// Data list container for image map.
		imageMapDataList: '.image-map-data-list'
	};

	/**
  * Cache list of open slide panels.
  *
  * @type {jQuery[]}
  */
	var openSlidePanels = [];

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Registers a change, so that the user gets a confirmation dialog while leaving the page.
  */
	function _registerChange() {
		// Object of GET parameters.
		var getParameters = $.deparam(window.location.search.slice(1));

		// Only register in slider edit mode.
		if ('id' in getParameters) {
			window.onbeforeunload = function () {
				return jse.core.lang.translate('EXIT_CONFIRMATION_TEXT', 'sliders');
			};
		}
	}

	/**
  * Handles the image dropdown change event.
  *
  * @param {jQuery.Event} event Triggered event.
  * @param {Boolean} [removeAllDataListItems = false] Remove all data list container list items?
  */
	function _onImageChange(event) {
		var removeAllDataListItems = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		// Image dropdown element.
		var $dropdown = $(event.target);

		// Remove icon element.
		var $removeIcon = $dropdown.parents(selectors.configurationRow).find(selectors.icons.imageDelete);

		// Image map icon element.
		var $imageMapIcon = $dropdown.parents(selectors.configurationRow).find(selectors.icons.imageMap);

		// Image map data container list element.
		var $list = $dropdown.parents(selectors.configurationRow).find(selectors.imageMapDataList);

		// Remove the remove icon if 'do not use' is selected.
		$removeIcon[$dropdown.val() ? 'show' : 'hide']();

		// Remove the image map icon if 'do not use' is selected.
		$imageMapIcon[$dropdown.val() ? 'show' : 'hide']();

		// Empty image map data container list.
		$list.children().filter(removeAllDataListItems ? '*' : '.new').remove();
	}

	/**
  * Triggers the file select (click) event of the invisible file input field.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onUploadIconClick(event) {
		$(event.target).parents(selectors.configurationRow).find(selectors.inputs.file).trigger('click');
	}

	/**
  * Handles the file select (change) event.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onImageAdd(event) {
		// Exit method, if selection has been aborted.
		if (!event.target.files.length) {
			return;
		}

		// File input element.
		var $fileInput = $(event.target);

		// Image dropdown.
		var $dropdown = $fileInput.parents(selectors.configurationRow).find(selectors.inputs.dropdown);

		// Regular expression to validate the file name.
		var regex = /(.)(jpg|jpeg|png|gif|bmp)$/i;

		// File name.
		var fileName = event.target.files[0].name;

		// Is the dropdown for thumbnail images?
		var isThumbnailImage = !!$fileInput.parents(selectors.configurationRow).find(selectors.inputs.thumbnailImageDropdown).length;

		// Exit method and show modal, if file type does not match.
		if (!regex.test(fileName)) {
			// Show modal.
			jse.libs.modal.showMessage(jse.core.lang.translate('INVALID_FILE_MODAL_TITLE', 'sliders'), jse.core.lang.translate('INVALID_FILE_MODAL_TEXT', 'sliders'));

			// Reset value.
			$fileInput.val('');

			return;
		}

		// Exit method and show modal, if filename is already present in dropdown.
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = $dropdown[0].children[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var $option = _step.value;

				// Check if option's text content matches with the name of the selected file.
				if ($option.textContent === fileName) {
					// Show modal.
					jse.libs.modal.showMessage(jse.core.lang.translate('FILENAME_ALREADY_USED_MODAL_TITLE', 'sliders'), jse.core.lang.translate('FILENAME_ALREADY_USED_MODAL_TEXT', 'sliders'));

					// Reset value.
					$fileInput.val('');

					return;
				}
			}

			// Add files to dropdowns.
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		_addImageToDropdowns(fileName, isThumbnailImage);

		// Select value.
		$dropdown.val(fileName).trigger('change');
	}

	/**
  * Handles the image delete button click event.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onImageDelete(event) {
		// Loading CSS class name.
		var loadingClass = 'loading';

		// Image dropdown container.
		var $configurationRow = $(event.target).parents(selectors.configurationRow);

		// Image dropdown.
		var $dropdown = $configurationRow.find(selectors.inputs.dropdown);

		// Slide ID.
		var slideId = $configurationRow.parents(selectors.slidePanel).data('id');

		// Slide image ID.
		var slideImageId = $configurationRow.data('id');

		// Is the dropdown for thumbnail images?
		var isThumbnailImage = !!$dropdown.is(selectors.inputs.thumbnailImageDropdown);

		// Selected file name.
		var fileName = $dropdown.val();

		// Add loading state.
		$dropdown.addClass(loadingClass);

		// Image usage check request options.
		var requestOptions = {
			url: 'admin.php?do=SlidersDetailsAjax/CheckImageUsage',
			data: {
				filename: fileName,
				is_thumbnail: isThumbnailImage,
				slide_id: slideId,
				slide_image_id: slideImageId
			}
		};

		// Perform deletion.
		var performDeletion = function performDeletion() {
			// Put image name into deleter select box.
			elements.deleteImageSelect.append($('<option>', {
				val: fileName,
				class: isThumbnailImage ? 'thumbnail' : ''
			}));

			// Delete image from dropdowns.
			_deleteImageFromDropdowns(fileName, isThumbnailImage);
		};

		// Check image usage.
		jse.libs.xhr.get(requestOptions).then(function (response) {
			// Remove loading state.
			$dropdown.removeClass(loadingClass);

			if (response.isUsed) {
				// Modal confirmation button element.
				var $confirmButton = elements.modals.deleteImage.find('button.confirm');

				// Show modal.
				elements.modals.deleteImage.modal('show');

				// Listen to confirmation button click event.
				$confirmButton.off('click').on('click', performDeletion);
			} else {
				performDeletion();
			}
		});
	}

	/**
  * Handles the image map edit button click event.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onImageMap(event) {
		// Slide image ID.
		var slideImageId = $(event.target).parents(selectors.configurationRow).data('id');

		// List element which acts like a data container.
		var $list = $(event.target).parents(selectors.configurationRow).find(selectors.imageMapDataList);

		// Image dropdown.
		var $dropdown = $(event.target).parents(selectors.configurationRow).find(selectors.inputs.dropdown);

		// Slide image file name.
		var imageFilename = $dropdown.val();

		// Is a new image selected?
		var isNewImageSelected = $dropdown.find('option:selected').hasClass(classes.newImage);

		// Path to image URL.
		var imageUrl = jse.core.config.get('appUrl') + '/images/slider_images/' + imageFilename;

		// Show save first notice modal and return immediately, if the slide image has no ID.
		if (!slideImageId || isNewImageSelected) {
			jse.libs.modal.showMessage(jse.core.lang.translate('IMAGE_MAP_MODAL_TITLE', 'sliders'), jse.core.lang.translate('SAVE_SLIDER_FIRST_NOTICE_TEXT', 'sliders'));

			return;
		}

		// Show image map modal.
		elements.modals.imageMap.trigger('show', [$list, imageUrl]);
	}

	/**
  * Handles the sort button click event.
  */
	function _onSortButtonClick() {
		// Indicator CSS classes.
		var indicatorClass = 'mode-on btn-primary';

		// Selector string for the slide panel body.
		var slidePanelBodySelector = '.panel-body';

		// Slides container tabs, except the active one.
		var $otherTabs = elements.tabHeader.children().not('.active');

		// Is the sort mode on?
		var isModeOn = elements.buttons.sort.hasClass(indicatorClass);

		// Language-specific button texts.
		var enterText = elements.buttons.sort.data('textEnter');
		var exitText = elements.buttons.sort.data('textExit');

		// All slide panels.
		var $slides = $this.find(selectors.slidePanel);

		// Apply fade effect onto slide panels.
		$slides.hide().fadeIn();

		// Switch text and toggle indicator class.
		elements.buttons.sort[isModeOn ? 'removeClass' : 'addClass'](indicatorClass).text(isModeOn ? enterText : exitText);

		// Toggle create button.
		elements.buttons.create.prop('disabled', !isModeOn);

		// Toggle drag handle buttons.
		$slides.find(selectors.icons.drag)[isModeOn ? 'hide' : 'show']();

		// Toggle other tabs.
		$otherTabs[isModeOn ? 'fadeIn' : 'fadeOut']();

		// Toggle collapser and hide buttons.
		$slides.find(selectors.icons.collapser).add(selectors.icons.delete)[isModeOn ? 'show' : 'hide']();

		// Save open slide panels.
		if (!isModeOn) {
			openSlidePanels = $slides.filter(function (index, element) {
				return $(element).find(slidePanelBodySelector).is(':visible');
			});
		}

		// Toggle saved open slide panels.
		openSlidePanels.each(function (index, element) {
			return $(element).find(selectors.icons.collapser).trigger('click');
		});
	}

	/**
  * Handles the sort start event.
  */
	function _onSortStart() {
		// Tab content element for selected language. 
		var $tabBody = $this.find(selectors.tabBody).filter(':visible');

		// Refresh tab sizes and positions.
		$tabBody.sortable('refreshPositions');
	}

	/**
  * Handles the sort stop event.
  */
	function _onSortStop() {
		// Register change, to make prompt on page unload.
		_registerChange();
	}

	/**
  * Handles the delete icon click event.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onDeleteIconClick(event) {
		// Slide panel element.
		var $slidePanel = $(event.target).parents(selectors.slidePanel);

		// Modal confirmation button element.
		var $confirmButton = elements.modals.deleteSlide.find('button.confirm');

		// Show modal.
		elements.modals.deleteSlide.modal('show');

		// Listen to confirmation button click event.
		$confirmButton.off('click').on('click', function () {
			return _onDeleteConfirmationButtonClick(elements.modals.deleteSlide, $slidePanel);
		});
	}

	/**
  * Handles the create button click event.
  */
	function _onCreateButtonClick() {
		// Make a clone of the slide panel template and create a new element.
		var $slidePanel = $(elements.templates.slidePanel.clone().html());

		// Tab content element for selected language. 
		var $tabBody = $this.find(selectors.tabBody).filter(':visible');

		// Slide panels.
		var $slidePanels = $tabBody.find(selectors.slidePanel);

		// Next panel index.
		var panelIndex = $slidePanels.length + 1;

		// Title for new slide panel.
		var newTitle = jse.core.lang.translate('NEW_SLIDE', 'sliders') + ' ' + panelIndex;

		// Add title to slide panel header.
		$slidePanel.find(selectors.slidePanelTitle).text(newTitle);

		// Add title to input field.
		$slidePanel.find(selectors.inputs.title).val(newTitle);

		// Add values to dropdowns.
		if ($slidePanels.length) {
			// Get all image dropdowns of the first panel.
			var $dropdowns = $slidePanels.first().find(selectors.inputs.dropdown);

			// Get the thumbnail dropdown options.
			var $thumbnailOptions = $dropdowns.filter(selectors.inputs.thumbnailImageDropdown).children().clone();

			// Get the image dropdown options.
			var $imageOptions = $dropdowns.not(selectors.inputs.thumbnailImageDropdown).first().children().clone();

			// Replace thumbnail options in new slide panel.
			$slidePanel.find(selectors.inputs.thumbnailImageDropdown).empty().append($thumbnailOptions).val('');

			// Replace image options in new slide panel.
			$slidePanel.find(selectors.inputs.dropdown).not(selectors.inputs.thumbnailImageDropdown).empty().append($imageOptions).val('');
		}

		// Add new slide panel element to tab body with fade effect.
		$slidePanel.hide().prependTo($tabBody).fadeIn();

		// Initialize widgets and extensions on the new slide panel element.
		gx.widgets.init($slidePanel);
		gx.extensions.init($slidePanel);

		// Trigger change to show the right action icons.
		$slidePanel.find('select').trigger('change');

		// Register change, to make prompt on page unload.
		_registerChange();

		// Toggle submit buttons.
		toggleSubmitButtons();
	}

	/**
  * Handles the confirmation button click event in the delete confirmation modal.
  *
  * @param {jQuery} $modal Delete confirmation modal element.
  * @param {jQuery} $slidePanel Slide panel element.
  */
	function _onDeleteConfirmationButtonClick($modal, $slidePanel) {
		// Hide modal.
		$modal.modal('hide');

		// Fade out slide panel element and then remove it.
		$slidePanel.fadeOut(400, function () {
			// Remove slide panel.
			$slidePanel.remove();

			// Toggle submit buttons.
			toggleSubmitButtons();
		});

		// Register change, to make prompt on page unload.
		_registerChange();
	}

	/**
  * Handles the key-up event on the slide title input field.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onTitleKeyup(event) {
		// Title input field.
		var $input = $(event.target);

		// Slide panel title element.
		var $title = $input.parents(selectors.slidePanel).find(selectors.slidePanelTitle);

		// Transfer input value to slide panel title.
		$title.text($input.val());
	}

	/**
  * Handles the mouse-enter event on a configuration row.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onConfigRowMouseEnter(event) {
		// Configuration row element.
		var $row = $(event.target);

		// Image map edit icon.
		var $imageMapIcon = $row.find(selectors.icons.imageMap);

		// Image map data container list element.
		var $list = $row.find(selectors.imageMapDataList);

		// Return immediately, if the image map edit icon does not exist.
		if (!$imageMapIcon.length || !$list.length) {
			return;
		}

		if ($list.children().length) {
			$imageMapIcon.removeClass('fa-external-link').addClass('fa-external-link-square');
		} else {
			$imageMapIcon.removeClass('fa-external-link-square').addClass('fa-external-link');
		}
	}

	/**
  * Handles the click event on the save button.
  */
	function _onSubmitSave() {
		$this.parents('form').trigger('submit');
	}

	/**
  * Handles the click event on the refresh list item in the submit button group.
  */
	function _onSubmitRefresh() {
		$this.parents('form').trigger('submit', { refresh: true });
	}

	/**
  * Adds an image to the image dropdowns.
  *
  * @param {String} fileName Name of the selected file.
  * @param {Boolean} [thumbnailImagesOnly = false] Apply on thumbnail image dropdowns only?
  */
	function _addImageToDropdowns(fileName) {
		var thumbnailImagesOnly = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

		// Select specific dropdowns.
		var $dropdowns = $this.find(selectors.inputs.dropdown)[thumbnailImagesOnly ? 'filter' : 'not'](selectors.inputs.thumbnailImageDropdown);

		// Create new image option element.
		var $option = $('<option>', { value: fileName, text: fileName, class: classes.newImage });

		// Append new options to dropdowns.
		$dropdowns.append($option);
	}

	/**
  * Deletes an image from the image dropdowns.
  *
  * @param {String} fileName Name of the selected file.
  * @param {Boolean} [thumbnailImagesOnly = false] Apply on thumbnail image dropdowns only?
  */
	function _deleteImageFromDropdowns(fileName) {
		var thumbnailImagesOnly = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

		// Select all dropdowns.
		var $dropdowns = $this.find(selectors.inputs.dropdown)[thumbnailImagesOnly ? 'filter' : 'not'](selectors.inputs.thumbnailImageDropdown);

		// Remove image option from each dropdown.
		$dropdowns.each(function (index, element) {
			// Dropdown element.
			var $dropdown = $(element);

			// Remove option.
			$dropdown.find('[value="' + fileName + '"]').remove();

			// Set to default value if there are no image file options.
			if ($dropdown.children().length <= 1) {
				$dropdown.val('');
			}

			// Trigger change.
			$dropdown.trigger('change');
		});
	}

	/**
  * Disables or enables the submit buttons.
  */
	function toggleSubmitButtons() {
		// Enable the submit buttons?
		var doEnableSubmitButtons = true;

		// Slides.
		var $slides = $this.find(selectors.slidePanel);

		// Disable submit buttons, if there are no slides.
		if (!$slides.length) {
			doEnableSubmitButtons = false;
		}

		// Disable/Enable submit buttons.
		elements.buttons.submit.children().not('ul').prop('disabled', !doEnableSubmitButtons);
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Attach click event handler to sort button.
		elements.buttons.sort.on('click', _onSortButtonClick);

		// Attach event handlers to sort actions, slide panel delete button and inputs fields.
		$this.on('sortstart', _onSortStart).on('sortstop', _onSortStop).on('click', selectors.icons.delete, _onDeleteIconClick).on('keyup', selectors.inputs.title, _onTitleKeyup).on('change', selectors.inputs.file, _onImageAdd).on('click', selectors.icons.upload, _onUploadIconClick).on('click', selectors.icons.imageDelete, _onImageDelete).on('click', selectors.icons.imageMap, _onImageMap).on('change', selectors.inputs.dropdown, _onImageChange).on('mouseenter', selectors.configurationRow, _onConfigRowMouseEnter);

		// Attach event listeners to submit buttons.
		elements.buttons.submitSave.on('click', _onSubmitSave);
		elements.buttons.submitRefresh.on('click', _onSubmitRefresh);

		// Attach click event handler to create button.
		elements.buttons.create.on('click', _onCreateButtonClick);

		// Activate first tab.
		elements.tabHeader.children().first().addClass('active');

		// Activate first tab content.
		$this.find(selectors.tabBody).first().addClass('active in');

		// Trigger dropdown change event to hide the remove icon, if 'do not use' is selected.
		$this.find(selectors.inputs.dropdown).trigger('change', [false]);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNsaWRlcnMvZGV0YWlscy9zbGlkZXNfY29udGFpbmVyLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJGZvb3RlciIsImVsZW1lbnRzIiwiYnV0dG9ucyIsInNvcnQiLCJmaW5kIiwiY3JlYXRlIiwic3VibWl0Iiwic3VibWl0U2F2ZSIsInN1Ym1pdFJlZnJlc2giLCJ0ZW1wbGF0ZXMiLCJzbGlkZVBhbmVsIiwibW9kYWxzIiwiZGVsZXRlSW1hZ2UiLCJkZWxldGVTbGlkZSIsImltYWdlTWFwIiwidGFiSGVhZGVyIiwiZGVsZXRlSW1hZ2VTZWxlY3QiLCJjbGFzc2VzIiwibmV3SW1hZ2UiLCJzZWxlY3RvcnMiLCJpY29ucyIsImRlbGV0ZSIsImRyYWciLCJjb2xsYXBzZXIiLCJpbWFnZURlbGV0ZSIsInVwbG9hZCIsImlucHV0cyIsImRyb3Bkb3duIiwidGh1bWJuYWlsSW1hZ2VEcm9wZG93biIsInRpdGxlIiwiZmlsZSIsInRhYkJvZHkiLCJzbGlkZVBhbmVsVGl0bGUiLCJjb25maWd1cmF0aW9uUm93IiwiaW1hZ2VNYXBEYXRhTGlzdCIsIm9wZW5TbGlkZVBhbmVscyIsIl9yZWdpc3RlckNoYW5nZSIsImdldFBhcmFtZXRlcnMiLCJkZXBhcmFtIiwid2luZG93IiwibG9jYXRpb24iLCJzZWFyY2giLCJzbGljZSIsIm9uYmVmb3JldW5sb2FkIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJfb25JbWFnZUNoYW5nZSIsImV2ZW50IiwicmVtb3ZlQWxsRGF0YUxpc3RJdGVtcyIsIiRkcm9wZG93biIsInRhcmdldCIsIiRyZW1vdmVJY29uIiwicGFyZW50cyIsIiRpbWFnZU1hcEljb24iLCIkbGlzdCIsInZhbCIsImNoaWxkcmVuIiwiZmlsdGVyIiwicmVtb3ZlIiwiX29uVXBsb2FkSWNvbkNsaWNrIiwidHJpZ2dlciIsIl9vbkltYWdlQWRkIiwiZmlsZXMiLCJsZW5ndGgiLCIkZmlsZUlucHV0IiwicmVnZXgiLCJmaWxlTmFtZSIsIm5hbWUiLCJpc1RodW1ibmFpbEltYWdlIiwidGVzdCIsImxpYnMiLCJtb2RhbCIsInNob3dNZXNzYWdlIiwiJG9wdGlvbiIsInRleHRDb250ZW50IiwiX2FkZEltYWdlVG9Ecm9wZG93bnMiLCJfb25JbWFnZURlbGV0ZSIsImxvYWRpbmdDbGFzcyIsIiRjb25maWd1cmF0aW9uUm93Iiwic2xpZGVJZCIsInNsaWRlSW1hZ2VJZCIsImlzIiwiYWRkQ2xhc3MiLCJyZXF1ZXN0T3B0aW9ucyIsInVybCIsImZpbGVuYW1lIiwiaXNfdGh1bWJuYWlsIiwic2xpZGVfaWQiLCJzbGlkZV9pbWFnZV9pZCIsInBlcmZvcm1EZWxldGlvbiIsImFwcGVuZCIsImNsYXNzIiwiX2RlbGV0ZUltYWdlRnJvbURyb3Bkb3ducyIsInhociIsImdldCIsInRoZW4iLCJyZW1vdmVDbGFzcyIsInJlc3BvbnNlIiwiaXNVc2VkIiwiJGNvbmZpcm1CdXR0b24iLCJvZmYiLCJvbiIsIl9vbkltYWdlTWFwIiwiaW1hZ2VGaWxlbmFtZSIsImlzTmV3SW1hZ2VTZWxlY3RlZCIsImhhc0NsYXNzIiwiaW1hZ2VVcmwiLCJjb25maWciLCJfb25Tb3J0QnV0dG9uQ2xpY2siLCJpbmRpY2F0b3JDbGFzcyIsInNsaWRlUGFuZWxCb2R5U2VsZWN0b3IiLCIkb3RoZXJUYWJzIiwibm90IiwiaXNNb2RlT24iLCJlbnRlclRleHQiLCJleGl0VGV4dCIsIiRzbGlkZXMiLCJoaWRlIiwiZmFkZUluIiwidGV4dCIsInByb3AiLCJhZGQiLCJpbmRleCIsImVsZW1lbnQiLCJlYWNoIiwiX29uU29ydFN0YXJ0IiwiJHRhYkJvZHkiLCJzb3J0YWJsZSIsIl9vblNvcnRTdG9wIiwiX29uRGVsZXRlSWNvbkNsaWNrIiwiJHNsaWRlUGFuZWwiLCJfb25EZWxldGVDb25maXJtYXRpb25CdXR0b25DbGljayIsIl9vbkNyZWF0ZUJ1dHRvbkNsaWNrIiwiY2xvbmUiLCJodG1sIiwiJHNsaWRlUGFuZWxzIiwicGFuZWxJbmRleCIsIm5ld1RpdGxlIiwiJGRyb3Bkb3ducyIsImZpcnN0IiwiJHRodW1ibmFpbE9wdGlvbnMiLCIkaW1hZ2VPcHRpb25zIiwiZW1wdHkiLCJwcmVwZW5kVG8iLCJ3aWRnZXRzIiwiaW5pdCIsImV4dGVuc2lvbnMiLCJ0b2dnbGVTdWJtaXRCdXR0b25zIiwiJG1vZGFsIiwiZmFkZU91dCIsIl9vblRpdGxlS2V5dXAiLCIkaW5wdXQiLCIkdGl0bGUiLCJfb25Db25maWdSb3dNb3VzZUVudGVyIiwiJHJvdyIsIl9vblN1Ym1pdFNhdmUiLCJfb25TdWJtaXRSZWZyZXNoIiwicmVmcmVzaCIsInRodW1ibmFpbEltYWdlc09ubHkiLCJ2YWx1ZSIsImRvRW5hYmxlU3VibWl0QnV0dG9ucyIsImRvbmUiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msa0JBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLG1EQUVJRCxJQUFJQyxNQUZSLDBDQUdJRCxJQUFJQyxNQUhSLHlDQUlDLEtBSkQsRUFLQyxPQUxELENBSEQsRUFXQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDtBQUNBLEtBQU1DLFVBQVVELEVBQUUsY0FBRixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRSxXQUFXO0FBQ2hCO0FBQ0FDLFdBQVM7QUFDUjtBQUNBQyxTQUFNTCxNQUFNTSxJQUFOLENBQVcsY0FBWCxDQUZFOztBQUlSO0FBQ0FDLFdBQVFQLE1BQU1NLElBQU4sQ0FBVyxhQUFYLENBTEE7O0FBT1I7QUFDQUUsV0FBUU4sUUFBUUksSUFBUixDQUFhLHNCQUFiLENBUkE7O0FBVVI7QUFDQUcsZUFBWVAsUUFBUUksSUFBUixDQUFhLE9BQWIsQ0FYSjs7QUFhUjtBQUNBSSxrQkFBZVIsUUFBUUksSUFBUixDQUFhLFVBQWI7QUFkUCxHQUZPOztBQW1CaEI7QUFDQUssYUFBVztBQUNWO0FBQ0FDLGVBQVlaLE1BQU1NLElBQU4sQ0FBVyx1QkFBWDtBQUZGLEdBcEJLOztBQXlCaEI7QUFDQU8sVUFBUTtBQUNQO0FBQ0FDLGdCQUFhYixFQUFFLHFCQUFGLENBRk47O0FBSVA7QUFDQWMsZ0JBQWFkLEVBQUUscUJBQUYsQ0FMTjs7QUFPUDtBQUNBZSxhQUFVZixFQUFFLGtCQUFGO0FBUkgsR0ExQlE7O0FBcUNoQjtBQUNBZ0IsYUFBV2pCLE1BQU1NLElBQU4sQ0FBVyxXQUFYLENBdENLOztBQXdDaEI7QUFDQVkscUJBQW1CakIsRUFBRSxnQkFBRjtBQXpDSCxFQUFqQjs7QUE0Q0E7Ozs7O0FBS0EsS0FBTWtCLFVBQVU7QUFDZjtBQUNBQyxZQUFVO0FBRkssRUFBaEI7O0FBS0E7Ozs7O0FBS0EsS0FBTUMsWUFBWTtBQUNqQjtBQUNBQyxTQUFPO0FBQ047QUFDQUMsV0FBUSxjQUZGOztBQUlOO0FBQ0FDLFNBQU0sY0FMQTs7QUFPTjtBQUNBQyxjQUFXLFlBUkw7O0FBVU47QUFDQUMsZ0JBQWEscUJBWFA7O0FBYU47QUFDQVYsYUFBVSx3QkFkSjs7QUFnQk47QUFDQVcsV0FBUTtBQWpCRixHQUZVOztBQXNCakI7QUFDQUMsVUFBUTtBQUNQO0FBQ0FDLGFBQVUsaUJBRkg7O0FBSVA7QUFDQUMsMkJBQXdCLG9CQUxqQjs7QUFPUDtBQUNBQyxVQUFPLHFCQVJBOztBQVVQO0FBQ0FDLFNBQU07QUFYQyxHQXZCUzs7QUFxQ2pCO0FBQ0FwQixjQUFZLFFBdENLOztBQXdDakI7QUFDQXFCLFdBQVMsV0F6Q1E7O0FBMkNqQjtBQUNBQyxtQkFBaUIsY0E1Q0E7O0FBOENqQjtBQUNBQyxvQkFBa0IsaUJBL0NEOztBQWlEakI7QUFDQUMsb0JBQWtCO0FBbERELEVBQWxCOztBQXFEQTs7Ozs7QUFLQSxLQUFJQyxrQkFBa0IsRUFBdEI7O0FBRUE7Ozs7O0FBS0EsS0FBTXpDLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBLFVBQVMwQyxlQUFULEdBQTJCO0FBQzFCO0FBQ0EsTUFBTUMsZ0JBQWdCdEMsRUFBRXVDLE9BQUYsQ0FBVUMsT0FBT0MsUUFBUCxDQUFnQkMsTUFBaEIsQ0FBdUJDLEtBQXZCLENBQTZCLENBQTdCLENBQVYsQ0FBdEI7O0FBRUE7QUFDQSxNQUFJLFFBQVFMLGFBQVosRUFBMkI7QUFDMUJFLFVBQU9JLGNBQVAsR0FBd0I7QUFBQSxXQUFNaEQsSUFBSWlELElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHdCQUF4QixFQUFrRCxTQUFsRCxDQUFOO0FBQUEsSUFBeEI7QUFDQTtBQUNEOztBQUVEOzs7Ozs7QUFNQSxVQUFTQyxjQUFULENBQXdCQyxLQUF4QixFQUE4RDtBQUFBLE1BQS9CQyxzQkFBK0IsdUVBQU4sSUFBTTs7QUFDN0Q7QUFDQSxNQUFNQyxZQUFZbkQsRUFBRWlELE1BQU1HLE1BQVIsQ0FBbEI7O0FBRUE7QUFDQSxNQUFNQyxjQUFjRixVQUNsQkcsT0FEa0IsQ0FDVmxDLFVBQVVjLGdCQURBLEVBRWxCN0IsSUFGa0IsQ0FFYmUsVUFBVUMsS0FBVixDQUFnQkksV0FGSCxDQUFwQjs7QUFJQTtBQUNBLE1BQU04QixnQkFBZ0JKLFVBQ3BCRyxPQURvQixDQUNabEMsVUFBVWMsZ0JBREUsRUFFcEI3QixJQUZvQixDQUVmZSxVQUFVQyxLQUFWLENBQWdCTixRQUZELENBQXRCOztBQUlBO0FBQ0EsTUFBTXlDLFFBQVFMLFVBQ1pHLE9BRFksQ0FDSmxDLFVBQVVjLGdCQUROLEVBRVo3QixJQUZZLENBRVBlLFVBQVVlLGdCQUZILENBQWQ7O0FBSUE7QUFDQWtCLGNBQVlGLFVBQVVNLEdBQVYsS0FBa0IsTUFBbEIsR0FBMkIsTUFBdkM7O0FBRUE7QUFDQUYsZ0JBQWNKLFVBQVVNLEdBQVYsS0FBa0IsTUFBbEIsR0FBMkIsTUFBekM7O0FBRUE7QUFDQUQsUUFDRUUsUUFERixHQUVFQyxNQUZGLENBRVNULHlCQUF5QixHQUF6QixHQUErQixNQUZ4QyxFQUdFVSxNQUhGO0FBSUE7O0FBRUQ7Ozs7O0FBS0EsVUFBU0Msa0JBQVQsQ0FBNEJaLEtBQTVCLEVBQW1DO0FBQ2xDakQsSUFBRWlELE1BQU1HLE1BQVIsRUFDRUUsT0FERixDQUNVbEMsVUFBVWMsZ0JBRHBCLEVBRUU3QixJQUZGLENBRU9lLFVBQVVPLE1BQVYsQ0FBaUJJLElBRnhCLEVBR0UrQixPQUhGLENBR1UsT0FIVjtBQUlBOztBQUVEOzs7OztBQUtBLFVBQVNDLFdBQVQsQ0FBcUJkLEtBQXJCLEVBQTRCO0FBQzNCO0FBQ0EsTUFBSSxDQUFDQSxNQUFNRyxNQUFOLENBQWFZLEtBQWIsQ0FBbUJDLE1BQXhCLEVBQWdDO0FBQy9CO0FBQ0E7O0FBRUQ7QUFDQSxNQUFNQyxhQUFhbEUsRUFBRWlELE1BQU1HLE1BQVIsQ0FBbkI7O0FBRUE7QUFDQSxNQUFNRCxZQUFZZSxXQUNoQlosT0FEZ0IsQ0FDUmxDLFVBQVVjLGdCQURGLEVBRWhCN0IsSUFGZ0IsQ0FFWGUsVUFBVU8sTUFBVixDQUFpQkMsUUFGTixDQUFsQjs7QUFJQTtBQUNBLE1BQU11QyxRQUFRLDZCQUFkOztBQUVBO0FBQ0EsTUFBTUMsV0FBV25CLE1BQU1HLE1BQU4sQ0FBYVksS0FBYixDQUFtQixDQUFuQixFQUFzQkssSUFBdkM7O0FBRUE7QUFDQSxNQUFNQyxtQkFBbUIsQ0FBQyxDQUFDSixXQUN6QlosT0FEeUIsQ0FDakJsQyxVQUFVYyxnQkFETyxFQUV6QjdCLElBRnlCLENBRXBCZSxVQUFVTyxNQUFWLENBQWlCRSxzQkFGRyxFQUd6Qm9DLE1BSEY7O0FBS0E7QUFDQSxNQUFJLENBQUNFLE1BQU1JLElBQU4sQ0FBV0gsUUFBWCxDQUFMLEVBQTJCO0FBQzFCO0FBQ0F4RSxPQUFJNEUsSUFBSixDQUFTQyxLQUFULENBQWVDLFdBQWYsQ0FDQzlFLElBQUlpRCxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwwQkFBeEIsRUFBb0QsU0FBcEQsQ0FERCxFQUVDbkQsSUFBSWlELElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHlCQUF4QixFQUFtRCxTQUFuRCxDQUZEOztBQUtBO0FBQ0FtQixjQUFXVCxHQUFYLENBQWUsRUFBZjs7QUFFQTtBQUNBOztBQUVEO0FBeEMyQjtBQUFBO0FBQUE7O0FBQUE7QUF5QzNCLHdCQUFzQk4sVUFBVSxDQUFWLEVBQWFPLFFBQW5DLDhIQUE2QztBQUFBLFFBQWxDaUIsT0FBa0M7O0FBQzVDO0FBQ0EsUUFBSUEsUUFBUUMsV0FBUixLQUF3QlIsUUFBNUIsRUFBc0M7QUFDckM7QUFDQXhFLFNBQUk0RSxJQUFKLENBQVNDLEtBQVQsQ0FBZUMsV0FBZixDQUNDOUUsSUFBSWlELElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1DQUF4QixFQUE2RCxTQUE3RCxDQURELEVBRUNuRCxJQUFJaUQsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isa0NBQXhCLEVBQTRELFNBQTVELENBRkQ7O0FBS0E7QUFDQW1CLGdCQUFXVCxHQUFYLENBQWUsRUFBZjs7QUFFQTtBQUNBO0FBQ0Q7O0FBRUQ7QUF6RDJCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBMEQzQm9CLHVCQUFxQlQsUUFBckIsRUFBK0JFLGdCQUEvQjs7QUFFQTtBQUNBbkIsWUFDRU0sR0FERixDQUNNVyxRQUROLEVBRUVOLE9BRkYsQ0FFVSxRQUZWO0FBR0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU2dCLGNBQVQsQ0FBd0I3QixLQUF4QixFQUErQjtBQUM5QjtBQUNBLE1BQU04QixlQUFlLFNBQXJCOztBQUVBO0FBQ0EsTUFBTUMsb0JBQW9CaEYsRUFBRWlELE1BQU1HLE1BQVIsRUFBZ0JFLE9BQWhCLENBQXdCbEMsVUFBVWMsZ0JBQWxDLENBQTFCOztBQUVBO0FBQ0EsTUFBTWlCLFlBQVk2QixrQkFBa0IzRSxJQUFsQixDQUF1QmUsVUFBVU8sTUFBVixDQUFpQkMsUUFBeEMsQ0FBbEI7O0FBRUE7QUFDQSxNQUFNcUQsVUFBVUQsa0JBQWtCMUIsT0FBbEIsQ0FBMEJsQyxVQUFVVCxVQUFwQyxFQUFnRGIsSUFBaEQsQ0FBcUQsSUFBckQsQ0FBaEI7O0FBRUE7QUFDQSxNQUFNb0YsZUFBZUYsa0JBQWtCbEYsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBckI7O0FBRUE7QUFDQSxNQUFNd0UsbUJBQW1CLENBQUMsQ0FBQ25CLFVBQVVnQyxFQUFWLENBQWEvRCxVQUFVTyxNQUFWLENBQWlCRSxzQkFBOUIsQ0FBM0I7O0FBRUE7QUFDQSxNQUFNdUMsV0FBV2pCLFVBQVVNLEdBQVYsRUFBakI7O0FBRUE7QUFDQU4sWUFBVWlDLFFBQVYsQ0FBbUJMLFlBQW5COztBQUVBO0FBQ0EsTUFBTU0saUJBQWlCO0FBQ3RCQyxRQUFLLGlEQURpQjtBQUV0QnhGLFNBQU07QUFDTHlGLGNBQVVuQixRQURMO0FBRUxvQixrQkFBY2xCLGdCQUZUO0FBR0xtQixjQUFVUixPQUhMO0FBSUxTLG9CQUFnQlI7QUFKWDtBQUZnQixHQUF2Qjs7QUFVQTtBQUNBLE1BQU1TLGtCQUFrQixTQUFsQkEsZUFBa0IsR0FBTTtBQUM3QjtBQUNBekYsWUFBU2UsaUJBQVQsQ0FBMkIyRSxNQUEzQixDQUFrQzVGLEVBQUUsVUFBRixFQUFjO0FBQy9DeUQsU0FBS1csUUFEMEM7QUFFL0N5QixXQUFPdkIsbUJBQW1CLFdBQW5CLEdBQWlDO0FBRk8sSUFBZCxDQUFsQzs7QUFLQTtBQUNBd0IsNkJBQTBCMUIsUUFBMUIsRUFBb0NFLGdCQUFwQztBQUNBLEdBVEQ7O0FBV0E7QUFDQTFFLE1BQUk0RSxJQUFKLENBQVN1QixHQUFULENBQWFDLEdBQWIsQ0FBaUJYLGNBQWpCLEVBQWlDWSxJQUFqQyxDQUFzQyxvQkFBWTtBQUNqRDtBQUNBOUMsYUFBVStDLFdBQVYsQ0FBc0JuQixZQUF0Qjs7QUFFQSxPQUFJb0IsU0FBU0MsTUFBYixFQUFxQjtBQUNwQjtBQUNBLFFBQU1DLGlCQUFpQm5HLFNBQVNVLE1BQVQsQ0FBZ0JDLFdBQWhCLENBQTRCUixJQUE1QixDQUFpQyxnQkFBakMsQ0FBdkI7O0FBRUE7QUFDQUgsYUFBU1UsTUFBVCxDQUFnQkMsV0FBaEIsQ0FBNEI0RCxLQUE1QixDQUFrQyxNQUFsQzs7QUFFQTtBQUNBNEIsbUJBQ0VDLEdBREYsQ0FDTSxPQUROLEVBRUVDLEVBRkYsQ0FFSyxPQUZMLEVBRWNaLGVBRmQ7QUFHQSxJQVhELE1BV087QUFDTkE7QUFDQTtBQUNELEdBbEJEO0FBbUJBOztBQUVEOzs7OztBQUtBLFVBQVNhLFdBQVQsQ0FBcUJ2RCxLQUFyQixFQUE0QjtBQUMzQjtBQUNBLE1BQU1pQyxlQUFlbEYsRUFBRWlELE1BQU1HLE1BQVIsRUFDbkJFLE9BRG1CLENBQ1hsQyxVQUFVYyxnQkFEQyxFQUVuQnBDLElBRm1CLENBRWQsSUFGYyxDQUFyQjs7QUFJQTtBQUNBLE1BQU0wRCxRQUFReEQsRUFBRWlELE1BQU1HLE1BQVIsRUFDWkUsT0FEWSxDQUNKbEMsVUFBVWMsZ0JBRE4sRUFFWjdCLElBRlksQ0FFUGUsVUFBVWUsZ0JBRkgsQ0FBZDs7QUFJQTtBQUNBLE1BQU1nQixZQUFZbkQsRUFBRWlELE1BQU1HLE1BQVIsRUFDaEJFLE9BRGdCLENBQ1JsQyxVQUFVYyxnQkFERixFQUVoQjdCLElBRmdCLENBRVhlLFVBQVVPLE1BQVYsQ0FBaUJDLFFBRk4sQ0FBbEI7O0FBSUE7QUFDQSxNQUFNNkUsZ0JBQWdCdEQsVUFBVU0sR0FBVixFQUF0Qjs7QUFFQTtBQUNBLE1BQU1pRCxxQkFBcUJ2RCxVQUFVOUMsSUFBVixDQUFlLGlCQUFmLEVBQWtDc0csUUFBbEMsQ0FBMkN6RixRQUFRQyxRQUFuRCxDQUEzQjs7QUFFQTtBQUNBLE1BQU15RixXQUFXaEgsSUFBSWlELElBQUosQ0FBU2dFLE1BQVQsQ0FBZ0JiLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHdCQUFoQyxHQUEyRFMsYUFBNUU7O0FBRUE7QUFDQSxNQUFJLENBQUN2QixZQUFELElBQWlCd0Isa0JBQXJCLEVBQXlDO0FBQ3hDOUcsT0FBSTRFLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxXQUFmLENBQ0M5RSxJQUFJaUQsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsdUJBQXhCLEVBQWlELFNBQWpELENBREQsRUFFQ25ELElBQUlpRCxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwrQkFBeEIsRUFBeUQsU0FBekQsQ0FGRDs7QUFLQTtBQUNBOztBQUVEO0FBQ0E3QyxXQUFTVSxNQUFULENBQWdCRyxRQUFoQixDQUF5QitDLE9BQXpCLENBQWlDLE1BQWpDLEVBQXlDLENBQUNOLEtBQUQsRUFBUW9ELFFBQVIsQ0FBekM7QUFDQTs7QUFFRDs7O0FBR0EsVUFBU0Usa0JBQVQsR0FBOEI7QUFDN0I7QUFDQSxNQUFNQyxpQkFBaUIscUJBQXZCOztBQUVBO0FBQ0EsTUFBTUMseUJBQXlCLGFBQS9COztBQUVBO0FBQ0EsTUFBTUMsYUFBYS9HLFNBQVNjLFNBQVQsQ0FDakIwQyxRQURpQixHQUVqQndELEdBRmlCLENBRWIsU0FGYSxDQUFuQjs7QUFJQTtBQUNBLE1BQU1DLFdBQVdqSCxTQUFTQyxPQUFULENBQWlCQyxJQUFqQixDQUFzQnVHLFFBQXRCLENBQStCSSxjQUEvQixDQUFqQjs7QUFFQTtBQUNBLE1BQU1LLFlBQVlsSCxTQUFTQyxPQUFULENBQWlCQyxJQUFqQixDQUFzQk4sSUFBdEIsQ0FBMkIsV0FBM0IsQ0FBbEI7QUFDQSxNQUFNdUgsV0FBV25ILFNBQVNDLE9BQVQsQ0FBaUJDLElBQWpCLENBQXNCTixJQUF0QixDQUEyQixVQUEzQixDQUFqQjs7QUFFQTtBQUNBLE1BQU13SCxVQUFVdkgsTUFBTU0sSUFBTixDQUFXZSxVQUFVVCxVQUFyQixDQUFoQjs7QUFFQTtBQUNBMkcsVUFDRUMsSUFERixHQUVFQyxNQUZGOztBQUlBO0FBQ0F0SCxXQUFTQyxPQUFULENBQWlCQyxJQUFqQixDQUFzQitHLFdBQVcsYUFBWCxHQUEyQixVQUFqRCxFQUE2REosY0FBN0QsRUFDRVUsSUFERixDQUNPTixXQUFXQyxTQUFYLEdBQXVCQyxRQUQ5Qjs7QUFHQTtBQUNBbkgsV0FBU0MsT0FBVCxDQUFpQkcsTUFBakIsQ0FBd0JvSCxJQUF4QixDQUE2QixVQUE3QixFQUF5QyxDQUFDUCxRQUExQzs7QUFFQTtBQUNBRyxVQUFRakgsSUFBUixDQUFhZSxVQUFVQyxLQUFWLENBQWdCRSxJQUE3QixFQUFtQzRGLFdBQVcsTUFBWCxHQUFvQixNQUF2RDs7QUFFQTtBQUNBRixhQUFXRSxXQUFXLFFBQVgsR0FBc0IsU0FBakM7O0FBRUE7QUFDQUcsVUFDRWpILElBREYsQ0FDT2UsVUFBVUMsS0FBVixDQUFnQkcsU0FEdkIsRUFFRW1HLEdBRkYsQ0FFTXZHLFVBQVVDLEtBQVYsQ0FBZ0JDLE1BRnRCLEVBRThCNkYsV0FBVyxNQUFYLEdBQW9CLE1BRmxEOztBQUlBO0FBQ0EsTUFBSSxDQUFDQSxRQUFMLEVBQWU7QUFDZC9FLHFCQUFrQmtGLFFBQVEzRCxNQUFSLENBQWUsVUFBQ2lFLEtBQUQsRUFBUUMsT0FBUjtBQUFBLFdBQW9CN0gsRUFBRTZILE9BQUYsRUFDbkR4SCxJQURtRCxDQUM5QzJHLHNCQUQ4QyxFQUVuRDdCLEVBRm1ELENBRWhELFVBRmdELENBQXBCO0FBQUEsSUFBZixDQUFsQjtBQUdBOztBQUVEO0FBQ0EvQyxrQkFBZ0IwRixJQUFoQixDQUFxQixVQUFDRixLQUFELEVBQVFDLE9BQVI7QUFBQSxVQUFvQjdILEVBQUU2SCxPQUFGLEVBQ3ZDeEgsSUFEdUMsQ0FDbENlLFVBQVVDLEtBQVYsQ0FBZ0JHLFNBRGtCLEVBRXZDc0MsT0FGdUMsQ0FFL0IsT0FGK0IsQ0FBcEI7QUFBQSxHQUFyQjtBQUdBOztBQUVEOzs7QUFHQSxVQUFTaUUsWUFBVCxHQUF3QjtBQUN2QjtBQUNBLE1BQU1DLFdBQVdqSSxNQUNmTSxJQURlLENBQ1ZlLFVBQVVZLE9BREEsRUFFZjJCLE1BRmUsQ0FFUixVQUZRLENBQWpCOztBQUlBO0FBQ0FxRSxXQUFTQyxRQUFULENBQWtCLGtCQUFsQjtBQUNBOztBQUVEOzs7QUFHQSxVQUFTQyxXQUFULEdBQXVCO0FBQ3RCO0FBQ0E3RjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVM4RixrQkFBVCxDQUE0QmxGLEtBQTVCLEVBQW1DO0FBQ2xDO0FBQ0EsTUFBTW1GLGNBQWNwSSxFQUFFaUQsTUFBTUcsTUFBUixFQUFnQkUsT0FBaEIsQ0FBd0JsQyxVQUFVVCxVQUFsQyxDQUFwQjs7QUFFQTtBQUNBLE1BQU0wRixpQkFBaUJuRyxTQUFTVSxNQUFULENBQWdCRSxXQUFoQixDQUE0QlQsSUFBNUIsQ0FBaUMsZ0JBQWpDLENBQXZCOztBQUVBO0FBQ0FILFdBQVNVLE1BQVQsQ0FBZ0JFLFdBQWhCLENBQTRCMkQsS0FBNUIsQ0FBa0MsTUFBbEM7O0FBRUE7QUFDQTRCLGlCQUNFQyxHQURGLENBQ00sT0FETixFQUVFQyxFQUZGLENBRUssT0FGTCxFQUVjO0FBQUEsVUFBTThCLGlDQUFpQ25JLFNBQVNVLE1BQVQsQ0FBZ0JFLFdBQWpELEVBQThEc0gsV0FBOUQsQ0FBTjtBQUFBLEdBRmQ7QUFHQTs7QUFFRDs7O0FBR0EsVUFBU0Usb0JBQVQsR0FBZ0M7QUFDL0I7QUFDQSxNQUFNRixjQUFjcEksRUFBRUUsU0FBU1EsU0FBVCxDQUFtQkMsVUFBbkIsQ0FBOEI0SCxLQUE5QixHQUFzQ0MsSUFBdEMsRUFBRixDQUFwQjs7QUFFQTtBQUNBLE1BQU1SLFdBQVdqSSxNQUNmTSxJQURlLENBQ1ZlLFVBQVVZLE9BREEsRUFFZjJCLE1BRmUsQ0FFUixVQUZRLENBQWpCOztBQUlBO0FBQ0EsTUFBTThFLGVBQWVULFNBQVMzSCxJQUFULENBQWNlLFVBQVVULFVBQXhCLENBQXJCOztBQUVBO0FBQ0EsTUFBTStILGFBQWFELGFBQWF4RSxNQUFiLEdBQXNCLENBQXpDOztBQUVBO0FBQ0EsTUFBTTBFLFdBQWMvSSxJQUFJaUQsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsV0FBeEIsRUFBcUMsU0FBckMsQ0FBZCxTQUFpRTJGLFVBQXZFOztBQUVBO0FBQ0FOLGNBQ0UvSCxJQURGLENBQ09lLFVBQVVhLGVBRGpCLEVBRUV3RixJQUZGLENBRU9rQixRQUZQOztBQUlBO0FBQ0FQLGNBQ0UvSCxJQURGLENBQ09lLFVBQVVPLE1BQVYsQ0FBaUJHLEtBRHhCLEVBRUUyQixHQUZGLENBRU1rRixRQUZOOztBQUlBO0FBQ0EsTUFBSUYsYUFBYXhFLE1BQWpCLEVBQXlCO0FBQ3hCO0FBQ0EsT0FBTTJFLGFBQWFILGFBQ2pCSSxLQURpQixHQUVqQnhJLElBRmlCLENBRVplLFVBQVVPLE1BQVYsQ0FBaUJDLFFBRkwsQ0FBbkI7O0FBSUE7QUFDQSxPQUFNa0gsb0JBQW9CRixXQUN4QmpGLE1BRHdCLENBQ2pCdkMsVUFBVU8sTUFBVixDQUFpQkUsc0JBREEsRUFFeEI2QixRQUZ3QixHQUd4QjZFLEtBSHdCLEVBQTFCOztBQUtBO0FBQ0EsT0FBTVEsZ0JBQWdCSCxXQUNwQjFCLEdBRG9CLENBQ2hCOUYsVUFBVU8sTUFBVixDQUFpQkUsc0JBREQsRUFFcEJnSCxLQUZvQixHQUdwQm5GLFFBSG9CLEdBSXBCNkUsS0FKb0IsRUFBdEI7O0FBTUE7QUFDQUgsZUFDRS9ILElBREYsQ0FDT2UsVUFBVU8sTUFBVixDQUFpQkUsc0JBRHhCLEVBRUVtSCxLQUZGLEdBR0VwRCxNQUhGLENBR1NrRCxpQkFIVCxFQUlFckYsR0FKRixDQUlNLEVBSk47O0FBTUE7QUFDQTJFLGVBQ0UvSCxJQURGLENBQ09lLFVBQVVPLE1BQVYsQ0FBaUJDLFFBRHhCLEVBRUVzRixHQUZGLENBRU05RixVQUFVTyxNQUFWLENBQWlCRSxzQkFGdkIsRUFHRW1ILEtBSEYsR0FJRXBELE1BSkYsQ0FJU21ELGFBSlQsRUFLRXRGLEdBTEYsQ0FLTSxFQUxOO0FBTUE7O0FBRUQ7QUFDQTJFLGNBQ0ViLElBREYsR0FFRTBCLFNBRkYsQ0FFWWpCLFFBRlosRUFHRVIsTUFIRjs7QUFLQTtBQUNBL0gsS0FBR3lKLE9BQUgsQ0FBV0MsSUFBWCxDQUFnQmYsV0FBaEI7QUFDQTNJLEtBQUcySixVQUFILENBQWNELElBQWQsQ0FBbUJmLFdBQW5COztBQUVBO0FBQ0FBLGNBQ0UvSCxJQURGLENBQ08sUUFEUCxFQUVFeUQsT0FGRixDQUVVLFFBRlY7O0FBSUE7QUFDQXpCOztBQUVBO0FBQ0FnSDtBQUNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTaEIsZ0NBQVQsQ0FBMENpQixNQUExQyxFQUFrRGxCLFdBQWxELEVBQStEO0FBQzlEO0FBQ0FrQixTQUFPN0UsS0FBUCxDQUFhLE1BQWI7O0FBRUE7QUFDQTJELGNBQVltQixPQUFaLENBQW9CLEdBQXBCLEVBQXlCLFlBQU07QUFDOUI7QUFDQW5CLGVBQVl4RSxNQUFaOztBQUVBO0FBQ0F5RjtBQUNBLEdBTkQ7O0FBUUE7QUFDQWhIO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU21ILGFBQVQsQ0FBdUJ2RyxLQUF2QixFQUE4QjtBQUM3QjtBQUNBLE1BQU13RyxTQUFTekosRUFBRWlELE1BQU1HLE1BQVIsQ0FBZjs7QUFFQTtBQUNBLE1BQU1zRyxTQUFTRCxPQUNibkcsT0FEYSxDQUNMbEMsVUFBVVQsVUFETCxFQUViTixJQUZhLENBRVJlLFVBQVVhLGVBRkYsQ0FBZjs7QUFJQTtBQUNBeUgsU0FBT2pDLElBQVAsQ0FBWWdDLE9BQU9oRyxHQUFQLEVBQVo7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTa0csc0JBQVQsQ0FBZ0MxRyxLQUFoQyxFQUF1QztBQUN0QztBQUNBLE1BQU0yRyxPQUFPNUosRUFBRWlELE1BQU1HLE1BQVIsQ0FBYjs7QUFFQTtBQUNBLE1BQU1HLGdCQUFnQnFHLEtBQUt2SixJQUFMLENBQVVlLFVBQVVDLEtBQVYsQ0FBZ0JOLFFBQTFCLENBQXRCOztBQUVBO0FBQ0EsTUFBTXlDLFFBQVFvRyxLQUFLdkosSUFBTCxDQUFVZSxVQUFVZSxnQkFBcEIsQ0FBZDs7QUFFQTtBQUNBLE1BQUksQ0FBQ29CLGNBQWNVLE1BQWYsSUFBeUIsQ0FBQ1QsTUFBTVMsTUFBcEMsRUFBNEM7QUFDM0M7QUFDQTs7QUFFRCxNQUFJVCxNQUFNRSxRQUFOLEdBQWlCTyxNQUFyQixFQUE2QjtBQUM1QlYsaUJBQWMyQyxXQUFkLENBQTBCLGtCQUExQixFQUE4Q2QsUUFBOUMsQ0FBdUQseUJBQXZEO0FBQ0EsR0FGRCxNQUVPO0FBQ043QixpQkFBYzJDLFdBQWQsQ0FBMEIseUJBQTFCLEVBQXFEZCxRQUFyRCxDQUE4RCxrQkFBOUQ7QUFDQTtBQUNEOztBQUVEOzs7QUFHQSxVQUFTeUUsYUFBVCxHQUF5QjtBQUN4QjlKLFFBQ0V1RCxPQURGLENBQ1UsTUFEVixFQUVFUSxPQUZGLENBRVUsUUFGVjtBQUdBOztBQUVEOzs7QUFHQSxVQUFTZ0csZ0JBQVQsR0FBNEI7QUFDM0IvSixRQUNFdUQsT0FERixDQUNVLE1BRFYsRUFFRVEsT0FGRixDQUVVLFFBRlYsRUFFb0IsRUFBQ2lHLFNBQVMsSUFBVixFQUZwQjtBQUdBOztBQUVEOzs7Ozs7QUFNQSxVQUFTbEYsb0JBQVQsQ0FBOEJULFFBQTlCLEVBQXFFO0FBQUEsTUFBN0I0RixtQkFBNkIsdUVBQVAsS0FBTzs7QUFDcEU7QUFDQSxNQUFNcEIsYUFBYTdJLE1BQ2pCTSxJQURpQixDQUNaZSxVQUFVTyxNQUFWLENBQWlCQyxRQURMLEVBRWpCb0ksc0JBQXNCLFFBQXRCLEdBQWlDLEtBRmhCLEVBRXVCNUksVUFBVU8sTUFBVixDQUFpQkUsc0JBRnhDLENBQW5COztBQUlBO0FBQ0EsTUFBTThDLFVBQVUzRSxFQUFFLFVBQUYsRUFBYyxFQUFDaUssT0FBTzdGLFFBQVIsRUFBa0JxRCxNQUFNckQsUUFBeEIsRUFBa0N5QixPQUFPM0UsUUFBUUMsUUFBakQsRUFBZCxDQUFoQjs7QUFFQTtBQUNBeUgsYUFBV2hELE1BQVgsQ0FBa0JqQixPQUFsQjtBQUNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTbUIseUJBQVQsQ0FBbUMxQixRQUFuQyxFQUEwRTtBQUFBLE1BQTdCNEYsbUJBQTZCLHVFQUFQLEtBQU87O0FBQ3pFO0FBQ0EsTUFBTXBCLGFBQWE3SSxNQUNqQk0sSUFEaUIsQ0FDWmUsVUFBVU8sTUFBVixDQUFpQkMsUUFETCxFQUVqQm9JLHNCQUFzQixRQUF0QixHQUFpQyxLQUZoQixFQUV1QjVJLFVBQVVPLE1BQVYsQ0FBaUJFLHNCQUZ4QyxDQUFuQjs7QUFJQTtBQUNBK0csYUFBV2QsSUFBWCxDQUFnQixVQUFDRixLQUFELEVBQVFDLE9BQVIsRUFBb0I7QUFDbkM7QUFDQSxPQUFNMUUsWUFBWW5ELEVBQUU2SCxPQUFGLENBQWxCOztBQUVBO0FBQ0ExRSxhQUFVOUMsSUFBVixjQUEwQitELFFBQTFCLFNBQXdDUixNQUF4Qzs7QUFFQTtBQUNBLE9BQUlULFVBQVVPLFFBQVYsR0FBcUJPLE1BQXJCLElBQStCLENBQW5DLEVBQXNDO0FBQ3JDZCxjQUFVTSxHQUFWLENBQWMsRUFBZDtBQUNBOztBQUVEO0FBQ0FOLGFBQVVXLE9BQVYsQ0FBa0IsUUFBbEI7QUFDQSxHQWREO0FBZUE7O0FBRUQ7OztBQUdBLFVBQVN1RixtQkFBVCxHQUErQjtBQUM5QjtBQUNBLE1BQUlhLHdCQUF3QixJQUE1Qjs7QUFFQTtBQUNBLE1BQU01QyxVQUFVdkgsTUFBTU0sSUFBTixDQUFXZSxVQUFVVCxVQUFyQixDQUFoQjs7QUFFQTtBQUNBLE1BQUksQ0FBQzJHLFFBQVFyRCxNQUFiLEVBQXFCO0FBQ3BCaUcsMkJBQXdCLEtBQXhCO0FBQ0E7O0FBRUQ7QUFDQWhLLFdBQVNDLE9BQVQsQ0FBaUJJLE1BQWpCLENBQ0VtRCxRQURGLEdBRUV3RCxHQUZGLENBRU0sSUFGTixFQUdFUSxJQUhGLENBR08sVUFIUCxFQUdtQixDQUFDd0MscUJBSHBCO0FBSUE7O0FBRUQ7QUFDQTtBQUNBOztBQUVBdkssUUFBT3dKLElBQVAsR0FBYyxnQkFBUTtBQUNyQjtBQUNBakosV0FBU0MsT0FBVCxDQUFpQkMsSUFBakIsQ0FBc0JtRyxFQUF0QixDQUF5QixPQUF6QixFQUFrQ08sa0JBQWxDOztBQUVBO0FBQ0EvRyxRQUNFd0csRUFERixDQUNLLFdBREwsRUFDa0J3QixZQURsQixFQUVFeEIsRUFGRixDQUVLLFVBRkwsRUFFaUIyQixXQUZqQixFQUdFM0IsRUFIRixDQUdLLE9BSEwsRUFHY25GLFVBQVVDLEtBQVYsQ0FBZ0JDLE1BSDlCLEVBR3NDNkcsa0JBSHRDLEVBSUU1QixFQUpGLENBSUssT0FKTCxFQUljbkYsVUFBVU8sTUFBVixDQUFpQkcsS0FKL0IsRUFJc0MwSCxhQUp0QyxFQUtFakQsRUFMRixDQUtLLFFBTEwsRUFLZW5GLFVBQVVPLE1BQVYsQ0FBaUJJLElBTGhDLEVBS3NDZ0MsV0FMdEMsRUFNRXdDLEVBTkYsQ0FNSyxPQU5MLEVBTWNuRixVQUFVQyxLQUFWLENBQWdCSyxNQU45QixFQU1zQ21DLGtCQU50QyxFQU9FMEMsRUFQRixDQU9LLE9BUEwsRUFPY25GLFVBQVVDLEtBQVYsQ0FBZ0JJLFdBUDlCLEVBTzJDcUQsY0FQM0MsRUFRRXlCLEVBUkYsQ0FRSyxPQVJMLEVBUWNuRixVQUFVQyxLQUFWLENBQWdCTixRQVI5QixFQVF3Q3lGLFdBUnhDLEVBU0VELEVBVEYsQ0FTSyxRQVRMLEVBU2VuRixVQUFVTyxNQUFWLENBQWlCQyxRQVRoQyxFQVMwQ29CLGNBVDFDLEVBVUV1RCxFQVZGLENBVUssWUFWTCxFQVVtQm5GLFVBQVVjLGdCQVY3QixFQVUrQ3lILHNCQVYvQzs7QUFZQTtBQUNBekosV0FBU0MsT0FBVCxDQUFpQkssVUFBakIsQ0FBNEIrRixFQUE1QixDQUErQixPQUEvQixFQUF3Q3NELGFBQXhDO0FBQ0EzSixXQUFTQyxPQUFULENBQWlCTSxhQUFqQixDQUErQjhGLEVBQS9CLENBQWtDLE9BQWxDLEVBQTJDdUQsZ0JBQTNDOztBQUVBO0FBQ0E1SixXQUFTQyxPQUFULENBQWlCRyxNQUFqQixDQUF3QmlHLEVBQXhCLENBQTJCLE9BQTNCLEVBQW9DK0Isb0JBQXBDOztBQUVBO0FBQ0FwSSxXQUFTYyxTQUFULENBQ0UwQyxRQURGLEdBRUVtRixLQUZGLEdBR0V6RCxRQUhGLENBR1csUUFIWDs7QUFLQTtBQUNBckYsUUFDRU0sSUFERixDQUNPZSxVQUFVWSxPQURqQixFQUVFNkcsS0FGRixHQUdFekQsUUFIRixDQUdXLFdBSFg7O0FBS0E7QUFDQXJGLFFBQ0VNLElBREYsQ0FDT2UsVUFBVU8sTUFBVixDQUFpQkMsUUFEeEIsRUFFRWtDLE9BRkYsQ0FFVSxRQUZWLEVBRW9CLENBQUMsS0FBRCxDQUZwQjs7QUFJQTtBQUNBcUc7QUFDQSxFQTNDRDs7QUE2Q0EsUUFBT3hLLE1BQVA7QUFDQSxDQTl5QkYiLCJmaWxlIjoic2xpZGVycy9kZXRhaWxzL3NsaWRlc19jb250YWluZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNsaWRlc19jb250YWluZXIuanMgMjAxNi0xMi0yOVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogQ29udHJvbGxlciBNb2R1bGUgRm9yIFNsaWRlcyBDb250YWluZXIgKFRhYnMpXG4gKlxuICogSGFuZGxlcyB0aGUgc2xpZGVycyBjb250YWluZXIgZnVuY3Rpb25hbGl0eSBpbiB0aGUgc2xpZGVycyBkZXRhaWxzIHBhZ2UuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3NsaWRlc19jb250YWluZXInLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktZGVwYXJhbS9qcXVlcnktZGVwYXJhbS5taW4uanNgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5jc3NgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5qc2AsXG5cdFx0J3hocicsXG5cdFx0J21vZGFsJ1xuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRjb25zdCAkZm9vdGVyID0gJCgnI21haW4tZm9vdGVyJyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRWxlbWVudHNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZWxlbWVudHMgPSB7XG5cdFx0XHQvLyBCdXR0b25zLlxuXHRcdFx0YnV0dG9uczoge1xuXHRcdFx0XHQvLyBTb3J0IG1vZGUgYnV0dG9uLlxuXHRcdFx0XHRzb3J0OiAkdGhpcy5maW5kKCcuc29ydC1idXR0b24nKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIENyZWF0ZSBidXR0b24uXG5cdFx0XHRcdGNyZWF0ZTogJHRoaXMuZmluZCgnLmJ0bi1jcmVhdGUnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFN1Ym1pdCBidXR0b24gZ3JvdXAuXG5cdFx0XHRcdHN1Ym1pdDogJGZvb3Rlci5maW5kKCcuc3VibWl0LWJ1dHRvbi1ncm91cCcpLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU3VibWl0IGJ1dHRvbiBmb3Igc2F2ZSBzbGlkZXJcblx0XHRcdFx0c3VibWl0U2F2ZTogJGZvb3Rlci5maW5kKCcuc2F2ZScpLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU3VibWl0IGJ1dHRvbiBmb3Igc2F2ZSBhbmQgcmVmcmVzaCBzbGlkZXJcblx0XHRcdFx0c3VibWl0UmVmcmVzaDogJGZvb3Rlci5maW5kKCcucmVmcmVzaCcpLFxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0Ly8gVGVtcGxhdGUuXG5cdFx0XHR0ZW1wbGF0ZXM6IHtcblx0XHRcdFx0Ly8gU2xpZGUgcGFuZWwgc2V0IHRlbXBsYXRlLlxuXHRcdFx0XHRzbGlkZVBhbmVsOiAkdGhpcy5maW5kKCcjc2xpZGUtcGFuZWwtdGVtcGxhdGUnKVxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0Ly8gTW9kYWxzLlxuXHRcdFx0bW9kYWxzOiB7XG5cdFx0XHRcdC8vIERlbGV0ZSBpbWFnZSBtb2RhbC5cblx0XHRcdFx0ZGVsZXRlSW1hZ2U6ICQoJy5kZWxldGUtaW1hZ2UubW9kYWwnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIERlbGV0ZSBzbGlkZSBtb2RhbC5cblx0XHRcdFx0ZGVsZXRlU2xpZGU6ICQoJy5kZWxldGUtc2xpZGUubW9kYWwnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEVkaXQgaW1hZ2UgbWFwIG1vZGFsLlxuXHRcdFx0XHRpbWFnZU1hcDogJCgnLmltYWdlLW1hcC5tb2RhbCcpLFxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0Ly8gVGFicy5cblx0XHRcdHRhYkhlYWRlcjogJHRoaXMuZmluZCgnLm5hdi10YWJzJyksXG5cdFx0XHRcblx0XHRcdC8vIFNlbGVjdCBib3ggd2hpY2ggaG9sZHMgYWxsIGltYWdlcyB0aGF0IHdpbGwgYmUgZGVsZXRlZC5cblx0XHRcdGRlbGV0ZUltYWdlU2VsZWN0OiAkKCcjZGVsZXRlX2ltYWdlcycpXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDU1MgY2xhc3MgbmFtZXMuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGNsYXNzZXMgPSB7XG5cdFx0XHQvLyBOZXcgaW1hZ2UuXG5cdFx0XHRuZXdJbWFnZTogJ25ldydcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNlbGVjdG9yIFN0cmluZ3Ncblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgc2VsZWN0b3JzID0ge1xuXHRcdFx0Ly8gSWNvbiBzZWxlY3RvciBzdHJpbmdzLlxuXHRcdFx0aWNvbnM6IHtcblx0XHRcdFx0Ly8gRGVsZXRlIGJ1dHRvbiBvbiB0aGUgcGFuZWwgaGVhZGVyLlxuXHRcdFx0XHRkZWxldGU6ICcuaWNvbi5kZWxldGUnLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRHJhZyBidXR0b24gb24gdGhlIHBhbmVsIGhlYWRlci5cblx0XHRcdFx0ZHJhZzogJy5kcmFnLWhhbmRsZScsXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBDb2xsYXBzZXIgYnV0dG9uIG9uIHRoZSBwYW5lbCBoZWFkZXIuXG5cdFx0XHRcdGNvbGxhcHNlcjogJy5jb2xsYXBzZXInLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gSW1hZ2UgZGVsZXRlIGJ1dHRvbi5cblx0XHRcdFx0aW1hZ2VEZWxldGU6ICcuYWN0aW9uLWljb24uZGVsZXRlJyxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEltYWdlIG1hcCBlZGl0IGJ1dHRvbi5cblx0XHRcdFx0aW1hZ2VNYXA6ICcuYWN0aW9uLWljb24uaW1hZ2UtbWFwJyxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFVwbG9hZCBpbWFnZSBidXR0b24uXG5cdFx0XHRcdHVwbG9hZDogJy5hY3Rpb24taWNvbi51cGxvYWQnXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvLyBJbnB1dHMgc2VsZWN0b3Igc3RyaW5ncy5cblx0XHRcdGlucHV0czoge1xuXHRcdFx0XHQvLyBHZW5lcmFsIGltYWdlIHNlbGVjdCBkcm9wZG93bnMuXG5cdFx0XHRcdGRyb3Bkb3duOiAnLmRyb3Bkb3duLWlucHV0Jyxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFRodW1ibmFpbCBkcm9wZG93bi5cblx0XHRcdFx0dGh1bWJuYWlsSW1hZ2VEcm9wZG93bjogJ1tuYW1lPVwidGh1bWJuYWlsXCJdJyxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFRpdGxlLlxuXHRcdFx0XHR0aXRsZTogJ2lucHV0W25hbWU9XCJ0aXRsZVwiXScsXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBGaWxlLlxuXHRcdFx0XHRmaWxlOiAnLmZpbGUtaW5wdXQnXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvLyBTbGlkZSBwYW5lbC5cblx0XHRcdHNsaWRlUGFuZWw6ICcucGFuZWwnLFxuXHRcdFx0XG5cdFx0XHQvLyBUYWIgYm9keS5cblx0XHRcdHRhYkJvZHk6ICcudGFiLXBhbmUnLFxuXHRcdFx0XG5cdFx0XHQvLyBTbGlkZSBwYW5lbCB0aXRsZS5cblx0XHRcdHNsaWRlUGFuZWxUaXRsZTogJy5zbGlkZS10aXRsZScsXG5cdFx0XHRcblx0XHRcdC8vIFNldHRpbmcgcm93IChmb3JtIGdyb3VwKS5cblx0XHRcdGNvbmZpZ3VyYXRpb25Sb3c6ICcucm93LmZvcm0tZ3JvdXAnLFxuXHRcdFx0XG5cdFx0XHQvLyBEYXRhIGxpc3QgY29udGFpbmVyIGZvciBpbWFnZSBtYXAuXG5cdFx0XHRpbWFnZU1hcERhdGFMaXN0OiAnLmltYWdlLW1hcC1kYXRhLWxpc3QnLFxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2FjaGUgbGlzdCBvZiBvcGVuIHNsaWRlIHBhbmVscy5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnlbXX1cblx0XHQgKi9cblx0XHRsZXQgb3BlblNsaWRlUGFuZWxzID0gW107XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZWdpc3RlcnMgYSBjaGFuZ2UsIHNvIHRoYXQgdGhlIHVzZXIgZ2V0cyBhIGNvbmZpcm1hdGlvbiBkaWFsb2cgd2hpbGUgbGVhdmluZyB0aGUgcGFnZS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfcmVnaXN0ZXJDaGFuZ2UoKSB7XG5cdFx0XHQvLyBPYmplY3Qgb2YgR0VUIHBhcmFtZXRlcnMuXG5cdFx0XHRjb25zdCBnZXRQYXJhbWV0ZXJzID0gJC5kZXBhcmFtKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSkpO1xuXHRcdFx0XG5cdFx0XHQvLyBPbmx5IHJlZ2lzdGVyIGluIHNsaWRlciBlZGl0IG1vZGUuXG5cdFx0XHRpZiAoJ2lkJyBpbiBnZXRQYXJhbWV0ZXJzKSB7XG5cdFx0XHRcdHdpbmRvdy5vbmJlZm9yZXVubG9hZCA9ICgpID0+IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdFWElUX0NPTkZJUk1BVElPTl9URVhUJywgJ3NsaWRlcnMnKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgaW1hZ2UgZHJvcGRvd24gY2hhbmdlIGV2ZW50LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IFRyaWdnZXJlZCBldmVudC5cblx0XHQgKiBAcGFyYW0ge0Jvb2xlYW59IFtyZW1vdmVBbGxEYXRhTGlzdEl0ZW1zID0gZmFsc2VdIFJlbW92ZSBhbGwgZGF0YSBsaXN0IGNvbnRhaW5lciBsaXN0IGl0ZW1zP1xuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkltYWdlQ2hhbmdlKGV2ZW50LCByZW1vdmVBbGxEYXRhTGlzdEl0ZW1zID0gdHJ1ZSkge1xuXHRcdFx0Ly8gSW1hZ2UgZHJvcGRvd24gZWxlbWVudC5cblx0XHRcdGNvbnN0ICRkcm9wZG93biA9ICQoZXZlbnQudGFyZ2V0KTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVtb3ZlIGljb24gZWxlbWVudC5cblx0XHRcdGNvbnN0ICRyZW1vdmVJY29uID0gJGRyb3Bkb3duXG5cdFx0XHRcdC5wYXJlbnRzKHNlbGVjdG9ycy5jb25maWd1cmF0aW9uUm93KVxuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaWNvbnMuaW1hZ2VEZWxldGUpO1xuXHRcdFx0XG5cdFx0XHQvLyBJbWFnZSBtYXAgaWNvbiBlbGVtZW50LlxuXHRcdFx0Y29uc3QgJGltYWdlTWFwSWNvbiA9ICRkcm9wZG93blxuXHRcdFx0XHQucGFyZW50cyhzZWxlY3RvcnMuY29uZmlndXJhdGlvblJvdylcblx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLmljb25zLmltYWdlTWFwKTtcblx0XHRcdFxuXHRcdFx0Ly8gSW1hZ2UgbWFwIGRhdGEgY29udGFpbmVyIGxpc3QgZWxlbWVudC5cblx0XHRcdGNvbnN0ICRsaXN0ID0gJGRyb3Bkb3duXG5cdFx0XHRcdC5wYXJlbnRzKHNlbGVjdG9ycy5jb25maWd1cmF0aW9uUm93KVxuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW1hZ2VNYXBEYXRhTGlzdCk7XG5cdFx0XHRcblx0XHRcdC8vIFJlbW92ZSB0aGUgcmVtb3ZlIGljb24gaWYgJ2RvIG5vdCB1c2UnIGlzIHNlbGVjdGVkLlxuXHRcdFx0JHJlbW92ZUljb25bJGRyb3Bkb3duLnZhbCgpID8gJ3Nob3cnIDogJ2hpZGUnXSgpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZW1vdmUgdGhlIGltYWdlIG1hcCBpY29uIGlmICdkbyBub3QgdXNlJyBpcyBzZWxlY3RlZC5cblx0XHRcdCRpbWFnZU1hcEljb25bJGRyb3Bkb3duLnZhbCgpID8gJ3Nob3cnIDogJ2hpZGUnXSgpO1xuXHRcdFx0XG5cdFx0XHQvLyBFbXB0eSBpbWFnZSBtYXAgZGF0YSBjb250YWluZXIgbGlzdC5cblx0XHRcdCRsaXN0XG5cdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdC5maWx0ZXIocmVtb3ZlQWxsRGF0YUxpc3RJdGVtcyA/ICcqJyA6ICcubmV3Jylcblx0XHRcdFx0LnJlbW92ZSgpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBUcmlnZ2VycyB0aGUgZmlsZSBzZWxlY3QgKGNsaWNrKSBldmVudCBvZiB0aGUgaW52aXNpYmxlIGZpbGUgaW5wdXQgZmllbGQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblVwbG9hZEljb25DbGljayhldmVudCkge1xuXHRcdFx0JChldmVudC50YXJnZXQpXG5cdFx0XHRcdC5wYXJlbnRzKHNlbGVjdG9ycy5jb25maWd1cmF0aW9uUm93KVxuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW5wdXRzLmZpbGUpXG5cdFx0XHRcdC50cmlnZ2VyKCdjbGljaycpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBmaWxlIHNlbGVjdCAoY2hhbmdlKSBldmVudC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uSW1hZ2VBZGQoZXZlbnQpIHtcblx0XHRcdC8vIEV4aXQgbWV0aG9kLCBpZiBzZWxlY3Rpb24gaGFzIGJlZW4gYWJvcnRlZC5cblx0XHRcdGlmICghZXZlbnQudGFyZ2V0LmZpbGVzLmxlbmd0aCkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIEZpbGUgaW5wdXQgZWxlbWVudC5cblx0XHRcdGNvbnN0ICRmaWxlSW5wdXQgPSAkKGV2ZW50LnRhcmdldCk7XG5cdFx0XHRcblx0XHRcdC8vIEltYWdlIGRyb3Bkb3duLlxuXHRcdFx0Y29uc3QgJGRyb3Bkb3duID0gJGZpbGVJbnB1dFxuXHRcdFx0XHQucGFyZW50cyhzZWxlY3RvcnMuY29uZmlndXJhdGlvblJvdylcblx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLmlucHV0cy5kcm9wZG93bik7XG5cdFx0XHRcblx0XHRcdC8vIFJlZ3VsYXIgZXhwcmVzc2lvbiB0byB2YWxpZGF0ZSB0aGUgZmlsZSBuYW1lLlxuXHRcdFx0Y29uc3QgcmVnZXggPSAvKC4pKGpwZ3xqcGVnfHBuZ3xnaWZ8Ym1wKSQvaTtcblx0XHRcdFxuXHRcdFx0Ly8gRmlsZSBuYW1lLlxuXHRcdFx0Y29uc3QgZmlsZU5hbWUgPSBldmVudC50YXJnZXQuZmlsZXNbMF0ubmFtZTtcblx0XHRcdFxuXHRcdFx0Ly8gSXMgdGhlIGRyb3Bkb3duIGZvciB0aHVtYm5haWwgaW1hZ2VzP1xuXHRcdFx0Y29uc3QgaXNUaHVtYm5haWxJbWFnZSA9ICEhJGZpbGVJbnB1dFxuXHRcdFx0XHQucGFyZW50cyhzZWxlY3RvcnMuY29uZmlndXJhdGlvblJvdylcblx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLmlucHV0cy50aHVtYm5haWxJbWFnZURyb3Bkb3duKVxuXHRcdFx0XHQubGVuZ3RoO1xuXHRcdFx0XG5cdFx0XHQvLyBFeGl0IG1ldGhvZCBhbmQgc2hvdyBtb2RhbCwgaWYgZmlsZSB0eXBlIGRvZXMgbm90IG1hdGNoLlxuXHRcdFx0aWYgKCFyZWdleC50ZXN0KGZpbGVOYW1lKSkge1xuXHRcdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnSU5WQUxJRF9GSUxFX01PREFMX1RJVExFJywgJ3NsaWRlcnMnKSxcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnSU5WQUxJRF9GSUxFX01PREFMX1RFWFQnLCAnc2xpZGVycycpXG5cdFx0XHRcdCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBSZXNldCB2YWx1ZS5cblx0XHRcdFx0JGZpbGVJbnB1dC52YWwoJycpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBFeGl0IG1ldGhvZCBhbmQgc2hvdyBtb2RhbCwgaWYgZmlsZW5hbWUgaXMgYWxyZWFkeSBwcmVzZW50IGluIGRyb3Bkb3duLlxuXHRcdFx0Zm9yIChjb25zdCAkb3B0aW9uIG9mICRkcm9wZG93blswXS5jaGlsZHJlbikge1xuXHRcdFx0XHQvLyBDaGVjayBpZiBvcHRpb24ncyB0ZXh0IGNvbnRlbnQgbWF0Y2hlcyB3aXRoIHRoZSBuYW1lIG9mIHRoZSBzZWxlY3RlZCBmaWxlLlxuXHRcdFx0XHRpZiAoJG9wdGlvbi50ZXh0Q29udGVudCA9PT0gZmlsZU5hbWUpIHtcblx0XHRcdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKFxuXHRcdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0ZJTEVOQU1FX0FMUkVBRFlfVVNFRF9NT0RBTF9USVRMRScsICdzbGlkZXJzJyksXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRklMRU5BTUVfQUxSRUFEWV9VU0VEX01PREFMX1RFWFQnLCAnc2xpZGVycycpXG5cdFx0XHRcdFx0KTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBSZXNldCB2YWx1ZS5cblx0XHRcdFx0XHQkZmlsZUlucHV0LnZhbCgnJyk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIEFkZCBmaWxlcyB0byBkcm9wZG93bnMuXG5cdFx0XHRfYWRkSW1hZ2VUb0Ryb3Bkb3ducyhmaWxlTmFtZSwgaXNUaHVtYm5haWxJbWFnZSk7XG5cdFx0XHRcblx0XHRcdC8vIFNlbGVjdCB2YWx1ZS5cblx0XHRcdCRkcm9wZG93blxuXHRcdFx0XHQudmFsKGZpbGVOYW1lKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIGltYWdlIGRlbGV0ZSBidXR0b24gY2xpY2sgZXZlbnQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkltYWdlRGVsZXRlKGV2ZW50KSB7XG5cdFx0XHQvLyBMb2FkaW5nIENTUyBjbGFzcyBuYW1lLlxuXHRcdFx0Y29uc3QgbG9hZGluZ0NsYXNzID0gJ2xvYWRpbmcnO1xuXHRcdFx0XG5cdFx0XHQvLyBJbWFnZSBkcm9wZG93biBjb250YWluZXIuXG5cdFx0XHRjb25zdCAkY29uZmlndXJhdGlvblJvdyA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKHNlbGVjdG9ycy5jb25maWd1cmF0aW9uUm93KTtcblx0XHRcdFxuXHRcdFx0Ly8gSW1hZ2UgZHJvcGRvd24uXG5cdFx0XHRjb25zdCAkZHJvcGRvd24gPSAkY29uZmlndXJhdGlvblJvdy5maW5kKHNlbGVjdG9ycy5pbnB1dHMuZHJvcGRvd24pO1xuXHRcdFx0XG5cdFx0XHQvLyBTbGlkZSBJRC5cblx0XHRcdGNvbnN0IHNsaWRlSWQgPSAkY29uZmlndXJhdGlvblJvdy5wYXJlbnRzKHNlbGVjdG9ycy5zbGlkZVBhbmVsKS5kYXRhKCdpZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBTbGlkZSBpbWFnZSBJRC5cblx0XHRcdGNvbnN0IHNsaWRlSW1hZ2VJZCA9ICRjb25maWd1cmF0aW9uUm93LmRhdGEoJ2lkJyk7XG5cdFx0XHRcblx0XHRcdC8vIElzIHRoZSBkcm9wZG93biBmb3IgdGh1bWJuYWlsIGltYWdlcz9cblx0XHRcdGNvbnN0IGlzVGh1bWJuYWlsSW1hZ2UgPSAhISRkcm9wZG93bi5pcyhzZWxlY3RvcnMuaW5wdXRzLnRodW1ibmFpbEltYWdlRHJvcGRvd24pO1xuXHRcdFx0XG5cdFx0XHQvLyBTZWxlY3RlZCBmaWxlIG5hbWUuXG5cdFx0XHRjb25zdCBmaWxlTmFtZSA9ICRkcm9wZG93bi52YWwoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWRkIGxvYWRpbmcgc3RhdGUuXG5cdFx0XHQkZHJvcGRvd24uYWRkQ2xhc3MobG9hZGluZ0NsYXNzKTtcblx0XHRcdFxuXHRcdFx0Ly8gSW1hZ2UgdXNhZ2UgY2hlY2sgcmVxdWVzdCBvcHRpb25zLlxuXHRcdFx0Y29uc3QgcmVxdWVzdE9wdGlvbnMgPSB7XG5cdFx0XHRcdHVybDogJ2FkbWluLnBocD9kbz1TbGlkZXJzRGV0YWlsc0FqYXgvQ2hlY2tJbWFnZVVzYWdlJyxcblx0XHRcdFx0ZGF0YToge1xuXHRcdFx0XHRcdGZpbGVuYW1lOiBmaWxlTmFtZSxcblx0XHRcdFx0XHRpc190aHVtYm5haWw6IGlzVGh1bWJuYWlsSW1hZ2UsXG5cdFx0XHRcdFx0c2xpZGVfaWQ6IHNsaWRlSWQsXG5cdFx0XHRcdFx0c2xpZGVfaW1hZ2VfaWQ6IHNsaWRlSW1hZ2VJZFxuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBQZXJmb3JtIGRlbGV0aW9uLlxuXHRcdFx0Y29uc3QgcGVyZm9ybURlbGV0aW9uID0gKCkgPT4ge1xuXHRcdFx0XHQvLyBQdXQgaW1hZ2UgbmFtZSBpbnRvIGRlbGV0ZXIgc2VsZWN0IGJveC5cblx0XHRcdFx0ZWxlbWVudHMuZGVsZXRlSW1hZ2VTZWxlY3QuYXBwZW5kKCQoJzxvcHRpb24+Jywge1xuXHRcdFx0XHRcdHZhbDogZmlsZU5hbWUsXG5cdFx0XHRcdFx0Y2xhc3M6IGlzVGh1bWJuYWlsSW1hZ2UgPyAndGh1bWJuYWlsJyA6ICcnXG5cdFx0XHRcdH0pKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIERlbGV0ZSBpbWFnZSBmcm9tIGRyb3Bkb3ducy5cblx0XHRcdFx0X2RlbGV0ZUltYWdlRnJvbURyb3Bkb3ducyhmaWxlTmFtZSwgaXNUaHVtYm5haWxJbWFnZSk7XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBDaGVjayBpbWFnZSB1c2FnZS5cblx0XHRcdGpzZS5saWJzLnhoci5nZXQocmVxdWVzdE9wdGlvbnMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHQvLyBSZW1vdmUgbG9hZGluZyBzdGF0ZS5cblx0XHRcdFx0JGRyb3Bkb3duLnJlbW92ZUNsYXNzKGxvYWRpbmdDbGFzcyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAocmVzcG9uc2UuaXNVc2VkKSB7XG5cdFx0XHRcdFx0Ly8gTW9kYWwgY29uZmlybWF0aW9uIGJ1dHRvbiBlbGVtZW50LlxuXHRcdFx0XHRcdGNvbnN0ICRjb25maXJtQnV0dG9uID0gZWxlbWVudHMubW9kYWxzLmRlbGV0ZUltYWdlLmZpbmQoJ2J1dHRvbi5jb25maXJtJyk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gU2hvdyBtb2RhbC5cblx0XHRcdFx0XHRlbGVtZW50cy5tb2RhbHMuZGVsZXRlSW1hZ2UubW9kYWwoJ3Nob3cnKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBMaXN0ZW4gdG8gY29uZmlybWF0aW9uIGJ1dHRvbiBjbGljayBldmVudC5cblx0XHRcdFx0XHQkY29uZmlybUJ1dHRvblxuXHRcdFx0XHRcdFx0Lm9mZignY2xpY2snKVxuXHRcdFx0XHRcdFx0Lm9uKCdjbGljaycsIHBlcmZvcm1EZWxldGlvbik7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0cGVyZm9ybURlbGV0aW9uKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBpbWFnZSBtYXAgZWRpdCBidXR0b24gY2xpY2sgZXZlbnQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkltYWdlTWFwKGV2ZW50KSB7XG5cdFx0XHQvLyBTbGlkZSBpbWFnZSBJRC5cblx0XHRcdGNvbnN0IHNsaWRlSW1hZ2VJZCA9ICQoZXZlbnQudGFyZ2V0KVxuXHRcdFx0XHQucGFyZW50cyhzZWxlY3RvcnMuY29uZmlndXJhdGlvblJvdylcblx0XHRcdFx0LmRhdGEoJ2lkJyk7XG5cdFx0XHRcblx0XHRcdC8vIExpc3QgZWxlbWVudCB3aGljaCBhY3RzIGxpa2UgYSBkYXRhIGNvbnRhaW5lci5cblx0XHRcdGNvbnN0ICRsaXN0ID0gJChldmVudC50YXJnZXQpXG5cdFx0XHRcdC5wYXJlbnRzKHNlbGVjdG9ycy5jb25maWd1cmF0aW9uUm93KVxuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW1hZ2VNYXBEYXRhTGlzdCk7XG5cdFx0XHRcblx0XHRcdC8vIEltYWdlIGRyb3Bkb3duLlxuXHRcdFx0Y29uc3QgJGRyb3Bkb3duID0gJChldmVudC50YXJnZXQpXG5cdFx0XHRcdC5wYXJlbnRzKHNlbGVjdG9ycy5jb25maWd1cmF0aW9uUm93KVxuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW5wdXRzLmRyb3Bkb3duKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2xpZGUgaW1hZ2UgZmlsZSBuYW1lLlxuXHRcdFx0Y29uc3QgaW1hZ2VGaWxlbmFtZSA9ICRkcm9wZG93bi52YWwoKTtcblx0XHRcdFxuXHRcdFx0Ly8gSXMgYSBuZXcgaW1hZ2Ugc2VsZWN0ZWQ/XG5cdFx0XHRjb25zdCBpc05ld0ltYWdlU2VsZWN0ZWQgPSAkZHJvcGRvd24uZmluZCgnb3B0aW9uOnNlbGVjdGVkJykuaGFzQ2xhc3MoY2xhc3Nlcy5uZXdJbWFnZSk7XG5cdFx0XHRcblx0XHRcdC8vIFBhdGggdG8gaW1hZ2UgVVJMLlxuXHRcdFx0Y29uc3QgaW1hZ2VVcmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvaW1hZ2VzL3NsaWRlcl9pbWFnZXMvJyArIGltYWdlRmlsZW5hbWU7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgc2F2ZSBmaXJzdCBub3RpY2UgbW9kYWwgYW5kIHJldHVybiBpbW1lZGlhdGVseSwgaWYgdGhlIHNsaWRlIGltYWdlIGhhcyBubyBJRC5cblx0XHRcdGlmICghc2xpZGVJbWFnZUlkIHx8IGlzTmV3SW1hZ2VTZWxlY3RlZCkge1xuXHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnSU1BR0VfTUFQX01PREFMX1RJVExFJywgJ3NsaWRlcnMnKSxcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnU0FWRV9TTElERVJfRklSU1RfTk9USUNFX1RFWFQnLCAnc2xpZGVycycpXG5cdFx0XHRcdCk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgaW1hZ2UgbWFwIG1vZGFsLlxuXHRcdFx0ZWxlbWVudHMubW9kYWxzLmltYWdlTWFwLnRyaWdnZXIoJ3Nob3cnLCBbJGxpc3QsIGltYWdlVXJsXSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIHNvcnQgYnV0dG9uIGNsaWNrIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblNvcnRCdXR0b25DbGljaygpIHtcblx0XHRcdC8vIEluZGljYXRvciBDU1MgY2xhc3Nlcy5cblx0XHRcdGNvbnN0IGluZGljYXRvckNsYXNzID0gJ21vZGUtb24gYnRuLXByaW1hcnknO1xuXHRcdFx0XG5cdFx0XHQvLyBTZWxlY3RvciBzdHJpbmcgZm9yIHRoZSBzbGlkZSBwYW5lbCBib2R5LlxuXHRcdFx0Y29uc3Qgc2xpZGVQYW5lbEJvZHlTZWxlY3RvciA9ICcucGFuZWwtYm9keSc7XG5cdFx0XHRcblx0XHRcdC8vIFNsaWRlcyBjb250YWluZXIgdGFicywgZXhjZXB0IHRoZSBhY3RpdmUgb25lLlxuXHRcdFx0Y29uc3QgJG90aGVyVGFicyA9IGVsZW1lbnRzLnRhYkhlYWRlclxuXHRcdFx0XHQuY2hpbGRyZW4oKVxuXHRcdFx0XHQubm90KCcuYWN0aXZlJyk7XG5cdFx0XHRcblx0XHRcdC8vIElzIHRoZSBzb3J0IG1vZGUgb24/XG5cdFx0XHRjb25zdCBpc01vZGVPbiA9IGVsZW1lbnRzLmJ1dHRvbnMuc29ydC5oYXNDbGFzcyhpbmRpY2F0b3JDbGFzcyk7XG5cdFx0XHRcblx0XHRcdC8vIExhbmd1YWdlLXNwZWNpZmljIGJ1dHRvbiB0ZXh0cy5cblx0XHRcdGNvbnN0IGVudGVyVGV4dCA9IGVsZW1lbnRzLmJ1dHRvbnMuc29ydC5kYXRhKCd0ZXh0RW50ZXInKTtcblx0XHRcdGNvbnN0IGV4aXRUZXh0ID0gZWxlbWVudHMuYnV0dG9ucy5zb3J0LmRhdGEoJ3RleHRFeGl0Jyk7XG5cdFx0XHRcblx0XHRcdC8vIEFsbCBzbGlkZSBwYW5lbHMuXG5cdFx0XHRjb25zdCAkc2xpZGVzID0gJHRoaXMuZmluZChzZWxlY3RvcnMuc2xpZGVQYW5lbCk7XG5cdFx0XHRcblx0XHRcdC8vIEFwcGx5IGZhZGUgZWZmZWN0IG9udG8gc2xpZGUgcGFuZWxzLlxuXHRcdFx0JHNsaWRlc1xuXHRcdFx0XHQuaGlkZSgpXG5cdFx0XHRcdC5mYWRlSW4oKTtcblx0XHRcdFxuXHRcdFx0Ly8gU3dpdGNoIHRleHQgYW5kIHRvZ2dsZSBpbmRpY2F0b3IgY2xhc3MuXG5cdFx0XHRlbGVtZW50cy5idXR0b25zLnNvcnRbaXNNb2RlT24gPyAncmVtb3ZlQ2xhc3MnIDogJ2FkZENsYXNzJ10oaW5kaWNhdG9yQ2xhc3MpXG5cdFx0XHRcdC50ZXh0KGlzTW9kZU9uID8gZW50ZXJUZXh0IDogZXhpdFRleHQpO1xuXHRcdFx0XG5cdFx0XHQvLyBUb2dnbGUgY3JlYXRlIGJ1dHRvbi5cblx0XHRcdGVsZW1lbnRzLmJ1dHRvbnMuY3JlYXRlLnByb3AoJ2Rpc2FibGVkJywgIWlzTW9kZU9uKTtcblx0XHRcdFxuXHRcdFx0Ly8gVG9nZ2xlIGRyYWcgaGFuZGxlIGJ1dHRvbnMuXG5cdFx0XHQkc2xpZGVzLmZpbmQoc2VsZWN0b3JzLmljb25zLmRyYWcpW2lzTW9kZU9uID8gJ2hpZGUnIDogJ3Nob3cnXSgpO1xuXHRcdFx0XG5cdFx0XHQvLyBUb2dnbGUgb3RoZXIgdGFicy5cblx0XHRcdCRvdGhlclRhYnNbaXNNb2RlT24gPyAnZmFkZUluJyA6ICdmYWRlT3V0J10oKTtcblx0XHRcdFxuXHRcdFx0Ly8gVG9nZ2xlIGNvbGxhcHNlciBhbmQgaGlkZSBidXR0b25zLlxuXHRcdFx0JHNsaWRlc1xuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaWNvbnMuY29sbGFwc2VyKVxuXHRcdFx0XHQuYWRkKHNlbGVjdG9ycy5pY29ucy5kZWxldGUpW2lzTW9kZU9uID8gJ3Nob3cnIDogJ2hpZGUnXSgpO1xuXHRcdFx0XG5cdFx0XHQvLyBTYXZlIG9wZW4gc2xpZGUgcGFuZWxzLlxuXHRcdFx0aWYgKCFpc01vZGVPbikge1xuXHRcdFx0XHRvcGVuU2xpZGVQYW5lbHMgPSAkc2xpZGVzLmZpbHRlcigoaW5kZXgsIGVsZW1lbnQpID0+ICQoZWxlbWVudClcblx0XHRcdFx0XHQuZmluZChzbGlkZVBhbmVsQm9keVNlbGVjdG9yKVxuXHRcdFx0XHRcdC5pcygnOnZpc2libGUnKSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIFRvZ2dsZSBzYXZlZCBvcGVuIHNsaWRlIHBhbmVscy5cblx0XHRcdG9wZW5TbGlkZVBhbmVscy5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4gJChlbGVtZW50KVxuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaWNvbnMuY29sbGFwc2VyKVxuXHRcdFx0XHQudHJpZ2dlcignY2xpY2snKSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIHNvcnQgc3RhcnQgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uU29ydFN0YXJ0KCkge1xuXHRcdFx0Ly8gVGFiIGNvbnRlbnQgZWxlbWVudCBmb3Igc2VsZWN0ZWQgbGFuZ3VhZ2UuIFxuXHRcdFx0Y29uc3QgJHRhYkJvZHkgPSAkdGhpc1xuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMudGFiQm9keSlcblx0XHRcdFx0LmZpbHRlcignOnZpc2libGUnKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVmcmVzaCB0YWIgc2l6ZXMgYW5kIHBvc2l0aW9ucy5cblx0XHRcdCR0YWJCb2R5LnNvcnRhYmxlKCdyZWZyZXNoUG9zaXRpb25zJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIHNvcnQgc3RvcCBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25Tb3J0U3RvcCgpIHtcblx0XHRcdC8vIFJlZ2lzdGVyIGNoYW5nZSwgdG8gbWFrZSBwcm9tcHQgb24gcGFnZSB1bmxvYWQuXG5cdFx0XHRfcmVnaXN0ZXJDaGFuZ2UoKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgZGVsZXRlIGljb24gY2xpY2sgZXZlbnQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkRlbGV0ZUljb25DbGljayhldmVudCkge1xuXHRcdFx0Ly8gU2xpZGUgcGFuZWwgZWxlbWVudC5cblx0XHRcdGNvbnN0ICRzbGlkZVBhbmVsID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoc2VsZWN0b3JzLnNsaWRlUGFuZWwpO1xuXHRcdFx0XG5cdFx0XHQvLyBNb2RhbCBjb25maXJtYXRpb24gYnV0dG9uIGVsZW1lbnQuXG5cdFx0XHRjb25zdCAkY29uZmlybUJ1dHRvbiA9IGVsZW1lbnRzLm1vZGFscy5kZWxldGVTbGlkZS5maW5kKCdidXR0b24uY29uZmlybScpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0ZWxlbWVudHMubW9kYWxzLmRlbGV0ZVNsaWRlLm1vZGFsKCdzaG93Jyk7XG5cdFx0XHRcblx0XHRcdC8vIExpc3RlbiB0byBjb25maXJtYXRpb24gYnV0dG9uIGNsaWNrIGV2ZW50LlxuXHRcdFx0JGNvbmZpcm1CdXR0b25cblx0XHRcdFx0Lm9mZignY2xpY2snKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgKCkgPT4gX29uRGVsZXRlQ29uZmlybWF0aW9uQnV0dG9uQ2xpY2soZWxlbWVudHMubW9kYWxzLmRlbGV0ZVNsaWRlLCAkc2xpZGVQYW5lbCkpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBjcmVhdGUgYnV0dG9uIGNsaWNrIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkNyZWF0ZUJ1dHRvbkNsaWNrKCkge1xuXHRcdFx0Ly8gTWFrZSBhIGNsb25lIG9mIHRoZSBzbGlkZSBwYW5lbCB0ZW1wbGF0ZSBhbmQgY3JlYXRlIGEgbmV3IGVsZW1lbnQuXG5cdFx0XHRjb25zdCAkc2xpZGVQYW5lbCA9ICQoZWxlbWVudHMudGVtcGxhdGVzLnNsaWRlUGFuZWwuY2xvbmUoKS5odG1sKCkpO1xuXHRcdFx0XG5cdFx0XHQvLyBUYWIgY29udGVudCBlbGVtZW50IGZvciBzZWxlY3RlZCBsYW5ndWFnZS4gXG5cdFx0XHRjb25zdCAkdGFiQm9keSA9ICR0aGlzXG5cdFx0XHRcdC5maW5kKHNlbGVjdG9ycy50YWJCb2R5KVxuXHRcdFx0XHQuZmlsdGVyKCc6dmlzaWJsZScpO1xuXHRcdFx0XG5cdFx0XHQvLyBTbGlkZSBwYW5lbHMuXG5cdFx0XHRjb25zdCAkc2xpZGVQYW5lbHMgPSAkdGFiQm9keS5maW5kKHNlbGVjdG9ycy5zbGlkZVBhbmVsKTtcblx0XHRcdFxuXHRcdFx0Ly8gTmV4dCBwYW5lbCBpbmRleC5cblx0XHRcdGNvbnN0IHBhbmVsSW5kZXggPSAkc2xpZGVQYW5lbHMubGVuZ3RoICsgMTtcblx0XHRcdFxuXHRcdFx0Ly8gVGl0bGUgZm9yIG5ldyBzbGlkZSBwYW5lbC5cblx0XHRcdGNvbnN0IG5ld1RpdGxlID0gYCR7anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ05FV19TTElERScsICdzbGlkZXJzJyl9ICR7cGFuZWxJbmRleH1gO1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgdGl0bGUgdG8gc2xpZGUgcGFuZWwgaGVhZGVyLlxuXHRcdFx0JHNsaWRlUGFuZWxcblx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLnNsaWRlUGFuZWxUaXRsZSlcblx0XHRcdFx0LnRleHQobmV3VGl0bGUpO1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgdGl0bGUgdG8gaW5wdXQgZmllbGQuXG5cdFx0XHQkc2xpZGVQYW5lbFxuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW5wdXRzLnRpdGxlKVxuXHRcdFx0XHQudmFsKG5ld1RpdGxlKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWRkIHZhbHVlcyB0byBkcm9wZG93bnMuXG5cdFx0XHRpZiAoJHNsaWRlUGFuZWxzLmxlbmd0aCkge1xuXHRcdFx0XHQvLyBHZXQgYWxsIGltYWdlIGRyb3Bkb3ducyBvZiB0aGUgZmlyc3QgcGFuZWwuXG5cdFx0XHRcdGNvbnN0ICRkcm9wZG93bnMgPSAkc2xpZGVQYW5lbHNcblx0XHRcdFx0XHQuZmlyc3QoKVxuXHRcdFx0XHRcdC5maW5kKHNlbGVjdG9ycy5pbnB1dHMuZHJvcGRvd24pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gR2V0IHRoZSB0aHVtYm5haWwgZHJvcGRvd24gb3B0aW9ucy5cblx0XHRcdFx0Y29uc3QgJHRodW1ibmFpbE9wdGlvbnMgPSAkZHJvcGRvd25zXG5cdFx0XHRcdFx0LmZpbHRlcihzZWxlY3RvcnMuaW5wdXRzLnRodW1ibmFpbEltYWdlRHJvcGRvd24pXG5cdFx0XHRcdFx0LmNoaWxkcmVuKClcblx0XHRcdFx0XHQuY2xvbmUoKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEdldCB0aGUgaW1hZ2UgZHJvcGRvd24gb3B0aW9ucy5cblx0XHRcdFx0Y29uc3QgJGltYWdlT3B0aW9ucyA9ICRkcm9wZG93bnNcblx0XHRcdFx0XHQubm90KHNlbGVjdG9ycy5pbnB1dHMudGh1bWJuYWlsSW1hZ2VEcm9wZG93bilcblx0XHRcdFx0XHQuZmlyc3QoKVxuXHRcdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdFx0LmNsb25lKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBSZXBsYWNlIHRodW1ibmFpbCBvcHRpb25zIGluIG5ldyBzbGlkZSBwYW5lbC5cblx0XHRcdFx0JHNsaWRlUGFuZWxcblx0XHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW5wdXRzLnRodW1ibmFpbEltYWdlRHJvcGRvd24pXG5cdFx0XHRcdFx0LmVtcHR5KClcblx0XHRcdFx0XHQuYXBwZW5kKCR0aHVtYm5haWxPcHRpb25zKVxuXHRcdFx0XHRcdC52YWwoJycpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUmVwbGFjZSBpbWFnZSBvcHRpb25zIGluIG5ldyBzbGlkZSBwYW5lbC5cblx0XHRcdFx0JHNsaWRlUGFuZWxcblx0XHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW5wdXRzLmRyb3Bkb3duKVxuXHRcdFx0XHRcdC5ub3Qoc2VsZWN0b3JzLmlucHV0cy50aHVtYm5haWxJbWFnZURyb3Bkb3duKVxuXHRcdFx0XHRcdC5lbXB0eSgpXG5cdFx0XHRcdFx0LmFwcGVuZCgkaW1hZ2VPcHRpb25zKVxuXHRcdFx0XHRcdC52YWwoJycpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBBZGQgbmV3IHNsaWRlIHBhbmVsIGVsZW1lbnQgdG8gdGFiIGJvZHkgd2l0aCBmYWRlIGVmZmVjdC5cblx0XHRcdCRzbGlkZVBhbmVsXG5cdFx0XHRcdC5oaWRlKClcblx0XHRcdFx0LnByZXBlbmRUbygkdGFiQm9keSlcblx0XHRcdFx0LmZhZGVJbigpO1xuXHRcdFx0XG5cdFx0XHQvLyBJbml0aWFsaXplIHdpZGdldHMgYW5kIGV4dGVuc2lvbnMgb24gdGhlIG5ldyBzbGlkZSBwYW5lbCBlbGVtZW50LlxuXHRcdFx0Z3gud2lkZ2V0cy5pbml0KCRzbGlkZVBhbmVsKTtcblx0XHRcdGd4LmV4dGVuc2lvbnMuaW5pdCgkc2xpZGVQYW5lbCk7XG5cdFx0XHRcblx0XHRcdC8vIFRyaWdnZXIgY2hhbmdlIHRvIHNob3cgdGhlIHJpZ2h0IGFjdGlvbiBpY29ucy5cblx0XHRcdCRzbGlkZVBhbmVsXG5cdFx0XHRcdC5maW5kKCdzZWxlY3QnKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0XHRcblx0XHRcdC8vIFJlZ2lzdGVyIGNoYW5nZSwgdG8gbWFrZSBwcm9tcHQgb24gcGFnZSB1bmxvYWQuXG5cdFx0XHRfcmVnaXN0ZXJDaGFuZ2UoKTtcblx0XHRcdFxuXHRcdFx0Ly8gVG9nZ2xlIHN1Ym1pdCBidXR0b25zLlxuXHRcdFx0dG9nZ2xlU3VibWl0QnV0dG9ucygpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBjb25maXJtYXRpb24gYnV0dG9uIGNsaWNrIGV2ZW50IGluIHRoZSBkZWxldGUgY29uZmlybWF0aW9uIG1vZGFsLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICRtb2RhbCBEZWxldGUgY29uZmlybWF0aW9uIG1vZGFsIGVsZW1lbnQuXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICRzbGlkZVBhbmVsIFNsaWRlIHBhbmVsIGVsZW1lbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uRGVsZXRlQ29uZmlybWF0aW9uQnV0dG9uQ2xpY2soJG1vZGFsLCAkc2xpZGVQYW5lbCkge1xuXHRcdFx0Ly8gSGlkZSBtb2RhbC5cblx0XHRcdCRtb2RhbC5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XG5cdFx0XHQvLyBGYWRlIG91dCBzbGlkZSBwYW5lbCBlbGVtZW50IGFuZCB0aGVuIHJlbW92ZSBpdC5cblx0XHRcdCRzbGlkZVBhbmVsLmZhZGVPdXQoNDAwLCAoKSA9PiB7XG5cdFx0XHRcdC8vIFJlbW92ZSBzbGlkZSBwYW5lbC5cblx0XHRcdFx0JHNsaWRlUGFuZWwucmVtb3ZlKClcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFRvZ2dsZSBzdWJtaXQgYnV0dG9ucy5cblx0XHRcdFx0dG9nZ2xlU3VibWl0QnV0dG9ucygpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIFJlZ2lzdGVyIGNoYW5nZSwgdG8gbWFrZSBwcm9tcHQgb24gcGFnZSB1bmxvYWQuXG5cdFx0XHRfcmVnaXN0ZXJDaGFuZ2UoKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUga2V5LXVwIGV2ZW50IG9uIHRoZSBzbGlkZSB0aXRsZSBpbnB1dCBmaWVsZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uVGl0bGVLZXl1cChldmVudCkge1xuXHRcdFx0Ly8gVGl0bGUgaW5wdXQgZmllbGQuXG5cdFx0XHRjb25zdCAkaW5wdXQgPSAkKGV2ZW50LnRhcmdldCk7XG5cdFx0XHRcblx0XHRcdC8vIFNsaWRlIHBhbmVsIHRpdGxlIGVsZW1lbnQuXG5cdFx0XHRjb25zdCAkdGl0bGUgPSAkaW5wdXRcblx0XHRcdFx0LnBhcmVudHMoc2VsZWN0b3JzLnNsaWRlUGFuZWwpXG5cdFx0XHRcdC5maW5kKHNlbGVjdG9ycy5zbGlkZVBhbmVsVGl0bGUpO1xuXHRcdFx0XG5cdFx0XHQvLyBUcmFuc2ZlciBpbnB1dCB2YWx1ZSB0byBzbGlkZSBwYW5lbCB0aXRsZS5cblx0XHRcdCR0aXRsZS50ZXh0KCRpbnB1dC52YWwoKSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIG1vdXNlLWVudGVyIGV2ZW50IG9uIGEgY29uZmlndXJhdGlvbiByb3cuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkNvbmZpZ1Jvd01vdXNlRW50ZXIoZXZlbnQpIHtcblx0XHRcdC8vIENvbmZpZ3VyYXRpb24gcm93IGVsZW1lbnQuXG5cdFx0XHRjb25zdCAkcm93ID0gJChldmVudC50YXJnZXQpO1xuXHRcdFx0XG5cdFx0XHQvLyBJbWFnZSBtYXAgZWRpdCBpY29uLlxuXHRcdFx0Y29uc3QgJGltYWdlTWFwSWNvbiA9ICRyb3cuZmluZChzZWxlY3RvcnMuaWNvbnMuaW1hZ2VNYXApO1xuXHRcdFx0XG5cdFx0XHQvLyBJbWFnZSBtYXAgZGF0YSBjb250YWluZXIgbGlzdCBlbGVtZW50LlxuXHRcdFx0Y29uc3QgJGxpc3QgPSAkcm93LmZpbmQoc2VsZWN0b3JzLmltYWdlTWFwRGF0YUxpc3QpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZXR1cm4gaW1tZWRpYXRlbHksIGlmIHRoZSBpbWFnZSBtYXAgZWRpdCBpY29uIGRvZXMgbm90IGV4aXN0LlxuXHRcdFx0aWYgKCEkaW1hZ2VNYXBJY29uLmxlbmd0aCB8fCAhJGxpc3QubGVuZ3RoKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKCRsaXN0LmNoaWxkcmVuKCkubGVuZ3RoKSB7XG5cdFx0XHRcdCRpbWFnZU1hcEljb24ucmVtb3ZlQ2xhc3MoJ2ZhLWV4dGVybmFsLWxpbmsnKS5hZGRDbGFzcygnZmEtZXh0ZXJuYWwtbGluay1zcXVhcmUnKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCRpbWFnZU1hcEljb24ucmVtb3ZlQ2xhc3MoJ2ZhLWV4dGVybmFsLWxpbmstc3F1YXJlJykuYWRkQ2xhc3MoJ2ZhLWV4dGVybmFsLWxpbmsnKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgY2xpY2sgZXZlbnQgb24gdGhlIHNhdmUgYnV0dG9uLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblN1Ym1pdFNhdmUoKSB7XG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQucGFyZW50cygnZm9ybScpXG5cdFx0XHRcdC50cmlnZ2VyKCdzdWJtaXQnKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgY2xpY2sgZXZlbnQgb24gdGhlIHJlZnJlc2ggbGlzdCBpdGVtIGluIHRoZSBzdWJtaXQgYnV0dG9uIGdyb3VwLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblN1Ym1pdFJlZnJlc2goKSB7XG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQucGFyZW50cygnZm9ybScpXG5cdFx0XHRcdC50cmlnZ2VyKCdzdWJtaXQnLCB7cmVmcmVzaDogdHJ1ZX0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGRzIGFuIGltYWdlIHRvIHRoZSBpbWFnZSBkcm9wZG93bnMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gZmlsZU5hbWUgTmFtZSBvZiB0aGUgc2VsZWN0ZWQgZmlsZS5cblx0XHQgKiBAcGFyYW0ge0Jvb2xlYW59IFt0aHVtYm5haWxJbWFnZXNPbmx5ID0gZmFsc2VdIEFwcGx5IG9uIHRodW1ibmFpbCBpbWFnZSBkcm9wZG93bnMgb25seT9cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfYWRkSW1hZ2VUb0Ryb3Bkb3ducyhmaWxlTmFtZSwgdGh1bWJuYWlsSW1hZ2VzT25seSA9IGZhbHNlKSB7XG5cdFx0XHQvLyBTZWxlY3Qgc3BlY2lmaWMgZHJvcGRvd25zLlxuXHRcdFx0Y29uc3QgJGRyb3Bkb3ducyA9ICR0aGlzXG5cdFx0XHRcdC5maW5kKHNlbGVjdG9ycy5pbnB1dHMuZHJvcGRvd24pXG5cdFx0XHRcdFt0aHVtYm5haWxJbWFnZXNPbmx5ID8gJ2ZpbHRlcicgOiAnbm90J10oc2VsZWN0b3JzLmlucHV0cy50aHVtYm5haWxJbWFnZURyb3Bkb3duKTtcblx0XHRcdFxuXHRcdFx0Ly8gQ3JlYXRlIG5ldyBpbWFnZSBvcHRpb24gZWxlbWVudC5cblx0XHRcdGNvbnN0ICRvcHRpb24gPSAkKCc8b3B0aW9uPicsIHt2YWx1ZTogZmlsZU5hbWUsIHRleHQ6IGZpbGVOYW1lLCBjbGFzczogY2xhc3Nlcy5uZXdJbWFnZX0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBcHBlbmQgbmV3IG9wdGlvbnMgdG8gZHJvcGRvd25zLlxuXHRcdFx0JGRyb3Bkb3ducy5hcHBlbmQoJG9wdGlvbik7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlbGV0ZXMgYW4gaW1hZ2UgZnJvbSB0aGUgaW1hZ2UgZHJvcGRvd25zLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGZpbGVOYW1lIE5hbWUgb2YgdGhlIHNlbGVjdGVkIGZpbGUuXG5cdFx0ICogQHBhcmFtIHtCb29sZWFufSBbdGh1bWJuYWlsSW1hZ2VzT25seSA9IGZhbHNlXSBBcHBseSBvbiB0aHVtYm5haWwgaW1hZ2UgZHJvcGRvd25zIG9ubHk/XG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2RlbGV0ZUltYWdlRnJvbURyb3Bkb3ducyhmaWxlTmFtZSwgdGh1bWJuYWlsSW1hZ2VzT25seSA9IGZhbHNlKSB7XG5cdFx0XHQvLyBTZWxlY3QgYWxsIGRyb3Bkb3ducy5cblx0XHRcdGNvbnN0ICRkcm9wZG93bnMgPSAkdGhpc1xuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW5wdXRzLmRyb3Bkb3duKVxuXHRcdFx0XHRbdGh1bWJuYWlsSW1hZ2VzT25seSA/ICdmaWx0ZXInIDogJ25vdCddKHNlbGVjdG9ycy5pbnB1dHMudGh1bWJuYWlsSW1hZ2VEcm9wZG93bik7XG5cdFx0XHRcblx0XHRcdC8vIFJlbW92ZSBpbWFnZSBvcHRpb24gZnJvbSBlYWNoIGRyb3Bkb3duLlxuXHRcdFx0JGRyb3Bkb3ducy5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4ge1xuXHRcdFx0XHQvLyBEcm9wZG93biBlbGVtZW50LlxuXHRcdFx0XHRjb25zdCAkZHJvcGRvd24gPSAkKGVsZW1lbnQpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUmVtb3ZlIG9wdGlvbi5cblx0XHRcdFx0JGRyb3Bkb3duLmZpbmQoYFt2YWx1ZT1cIiR7ZmlsZU5hbWV9XCJdYCkucmVtb3ZlKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTZXQgdG8gZGVmYXVsdCB2YWx1ZSBpZiB0aGVyZSBhcmUgbm8gaW1hZ2UgZmlsZSBvcHRpb25zLlxuXHRcdFx0XHRpZiAoJGRyb3Bkb3duLmNoaWxkcmVuKCkubGVuZ3RoIDw9IDEpIHtcblx0XHRcdFx0XHQkZHJvcGRvd24udmFsKCcnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gVHJpZ2dlciBjaGFuZ2UuXG5cdFx0XHRcdCRkcm9wZG93bi50cmlnZ2VyKCdjaGFuZ2UnKTtcblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBEaXNhYmxlcyBvciBlbmFibGVzIHRoZSBzdWJtaXQgYnV0dG9ucy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiB0b2dnbGVTdWJtaXRCdXR0b25zKCkge1xuXHRcdFx0Ly8gRW5hYmxlIHRoZSBzdWJtaXQgYnV0dG9ucz9cblx0XHRcdGxldCBkb0VuYWJsZVN1Ym1pdEJ1dHRvbnMgPSB0cnVlO1xuXHRcdFx0XG5cdFx0XHQvLyBTbGlkZXMuXG5cdFx0XHRjb25zdCAkc2xpZGVzID0gJHRoaXMuZmluZChzZWxlY3RvcnMuc2xpZGVQYW5lbCk7XG5cdFx0XHRcblx0XHRcdC8vIERpc2FibGUgc3VibWl0IGJ1dHRvbnMsIGlmIHRoZXJlIGFyZSBubyBzbGlkZXMuXG5cdFx0XHRpZiAoISRzbGlkZXMubGVuZ3RoKSB7XG5cdFx0XHRcdGRvRW5hYmxlU3VibWl0QnV0dG9ucyA9IGZhbHNlO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBEaXNhYmxlL0VuYWJsZSBzdWJtaXQgYnV0dG9ucy5cblx0XHRcdGVsZW1lbnRzLmJ1dHRvbnMuc3VibWl0XG5cdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdC5ub3QoJ3VsJylcblx0XHRcdFx0LnByb3AoJ2Rpc2FibGVkJywgIWRvRW5hYmxlU3VibWl0QnV0dG9ucyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZG9uZSA9PiB7XG5cdFx0XHQvLyBBdHRhY2ggY2xpY2sgZXZlbnQgaGFuZGxlciB0byBzb3J0IGJ1dHRvbi5cblx0XHRcdGVsZW1lbnRzLmJ1dHRvbnMuc29ydC5vbignY2xpY2snLCBfb25Tb3J0QnV0dG9uQ2xpY2spO1xuXHRcdFx0XG5cdFx0XHQvLyBBdHRhY2ggZXZlbnQgaGFuZGxlcnMgdG8gc29ydCBhY3Rpb25zLCBzbGlkZSBwYW5lbCBkZWxldGUgYnV0dG9uIGFuZCBpbnB1dHMgZmllbGRzLlxuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdzb3J0c3RhcnQnLCBfb25Tb3J0U3RhcnQpXG5cdFx0XHRcdC5vbignc29ydHN0b3AnLCBfb25Tb3J0U3RvcClcblx0XHRcdFx0Lm9uKCdjbGljaycsIHNlbGVjdG9ycy5pY29ucy5kZWxldGUsIF9vbkRlbGV0ZUljb25DbGljaylcblx0XHRcdFx0Lm9uKCdrZXl1cCcsIHNlbGVjdG9ycy5pbnB1dHMudGl0bGUsIF9vblRpdGxlS2V5dXApXG5cdFx0XHRcdC5vbignY2hhbmdlJywgc2VsZWN0b3JzLmlucHV0cy5maWxlLCBfb25JbWFnZUFkZClcblx0XHRcdFx0Lm9uKCdjbGljaycsIHNlbGVjdG9ycy5pY29ucy51cGxvYWQsIF9vblVwbG9hZEljb25DbGljaylcblx0XHRcdFx0Lm9uKCdjbGljaycsIHNlbGVjdG9ycy5pY29ucy5pbWFnZURlbGV0ZSwgX29uSW1hZ2VEZWxldGUpXG5cdFx0XHRcdC5vbignY2xpY2snLCBzZWxlY3RvcnMuaWNvbnMuaW1hZ2VNYXAsIF9vbkltYWdlTWFwKVxuXHRcdFx0XHQub24oJ2NoYW5nZScsIHNlbGVjdG9ycy5pbnB1dHMuZHJvcGRvd24sIF9vbkltYWdlQ2hhbmdlKVxuXHRcdFx0XHQub24oJ21vdXNlZW50ZXInLCBzZWxlY3RvcnMuY29uZmlndXJhdGlvblJvdywgX29uQ29uZmlnUm93TW91c2VFbnRlcik7XG5cdFx0XHRcblx0XHRcdC8vIEF0dGFjaCBldmVudCBsaXN0ZW5lcnMgdG8gc3VibWl0IGJ1dHRvbnMuXG5cdFx0XHRlbGVtZW50cy5idXR0b25zLnN1Ym1pdFNhdmUub24oJ2NsaWNrJywgX29uU3VibWl0U2F2ZSk7XG5cdFx0XHRlbGVtZW50cy5idXR0b25zLnN1Ym1pdFJlZnJlc2gub24oJ2NsaWNrJywgX29uU3VibWl0UmVmcmVzaCk7XG5cdFx0XHRcblx0XHRcdC8vIEF0dGFjaCBjbGljayBldmVudCBoYW5kbGVyIHRvIGNyZWF0ZSBidXR0b24uXG5cdFx0XHRlbGVtZW50cy5idXR0b25zLmNyZWF0ZS5vbignY2xpY2snLCBfb25DcmVhdGVCdXR0b25DbGljayk7XG5cdFx0XHRcblx0XHRcdC8vIEFjdGl2YXRlIGZpcnN0IHRhYi5cblx0XHRcdGVsZW1lbnRzLnRhYkhlYWRlclxuXHRcdFx0XHQuY2hpbGRyZW4oKVxuXHRcdFx0XHQuZmlyc3QoKVxuXHRcdFx0XHQuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XG5cdFx0XHQvLyBBY3RpdmF0ZSBmaXJzdCB0YWIgY29udGVudC5cblx0XHRcdCR0aGlzXG5cdFx0XHRcdC5maW5kKHNlbGVjdG9ycy50YWJCb2R5KVxuXHRcdFx0XHQuZmlyc3QoKVxuXHRcdFx0XHQuYWRkQ2xhc3MoJ2FjdGl2ZSBpbicpO1xuXHRcdFx0XG5cdFx0XHQvLyBUcmlnZ2VyIGRyb3Bkb3duIGNoYW5nZSBldmVudCB0byBoaWRlIHRoZSByZW1vdmUgaWNvbiwgaWYgJ2RvIG5vdCB1c2UnIGlzIHNlbGVjdGVkLlxuXHRcdFx0JHRoaXNcblx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLmlucHV0cy5kcm9wZG93bilcblx0XHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScsIFtmYWxzZV0pO1xuXHRcdFx0XG5cdFx0XHQvLyBGaW5pc2ggaW5pdGlhbGl6YXRpb24uXG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9XG4pO1xuIl19
