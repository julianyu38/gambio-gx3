'use strict';

/* --------------------------------------------------------------
 product_content.js 2017-09-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('overview', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', 'datatable'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		filterColumnIndex: 0,
		filterRegexPrefix: '^',
		expert: ''
	},


	/**
  * Final Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * URLs for deleting different types of content
  * 
  * @type {{page: string, element: string, file: string, link: string}}
  */
	urls = {
		page: jse.core.config.get('appUrl') + '/admin/admin.php?do=ContentManagerAjax/Delete',
		element: jse.core.config.get('appUrl') + '/admin/admin.php?do=ContentManagerAjax/Delete',
		file: jse.core.config.get('appUrl') + '/admin/admin.php?do=ContentManagerProductContentsAjax/deleteFile',
		link: jse.core.config.get('appUrl') + '/admin/admin.php?do=ContentManagerProductContentsAjax/deleteLink'
	},


	/**
  * Content Manager delete confirmation modal.
  *
  * @type {jQuery}
  */
	$modal = $('.delete-content.modal'),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	function _createTopBar($table) {

		$table.parent().prepend('\n\t\t\t\t<div class="row headline">\n\t\t\t\t\t<div class="col-md-6 quick-search">\n\t\t\t\t\t\t<form class="form-inline form-group remove-padding">\n\t\t\t\t\t\t\t<label for="search-keyword">' + jse.core.lang.translate('search', 'admin_labels') + '</label>\n\t\t\t\t\t\t\t<input type="text" class="search-keyword" />\n\t\t\t\t\t\t</form>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-md-6">\n\n\t\t\t\t\t\t<div data-gx-widget="button_dropdown"\n\t\t\t\t\t\t     data-button_dropdown-user_id="{$smarty.session.customer_id}"\n\t\t\t\t\t\t>\n\t\t\t\t\t\t\t<div data-use-button_dropdown="true"\n\t\t\t\t\t\t\t     data-custom_caret_btn_class="btn-success"\n\t\t\t\t\t\t\t     class="pull-right"\n\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t<button data-gx-extension="link" data-link-url="admin.php?do=ContentManagerProductContents/new&type=file' + options.expert + '" class="btn btn-success">\n\t\t\t\t\t\t\t\t\t<i class="fa fa-plus btn-icon"></i>\n\t\t\t\t\t\t\t\t\t' + jse.core.lang.translate('BUTTON_NEW_PREFIX', 'admin_buttons') + '\n\t\t\t\t\t\t\t\t\t' + jse.core.lang.translate('LABEL_PRODUCT_CONTENT_FILE', 'content_manager') + '\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t\t<li><span data-gx-extension="link" data-link-url="admin.php?do=ContentManagerProductContents/new&type=link' + options.expert + '">\n\t\t\t\t\t\t\t\t\t' + jse.core.lang.translate('BUTTON_NEW_PREFIX_2', 'admin_buttons') + '\n\t\t\t\t\t\t\t\t\t' + jse.core.lang.translate('LABEL_PRODUCT_CONTENT_LINK', 'content_manager') + '</span></li>\n\t\t\t\t\t\t\t\t\t<li><span data-gx-extension="link" data-link-url="admin.php?do=ContentManagerProductContents/new&type=text' + options.expert + '">\n\t\t\t\t\t\t\t\t\t' + jse.core.lang.translate('BUTTON_NEW_PREFIX_2', 'admin_buttons') + '\n\t\t\t\t\t\t\t\t\t' + jse.core.lang.translate('LABEL_PRODUCT_CONTENT_TEXT', 'content_manager') + '</span></li>\n\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t');

		gx.widgets.init($table.parent());
		gx.extensions.init($table.parent());
	}

	function _createBottomBar($table) {
		var $paginator = $('<div class="paginator" />');
		var $datatableComponents = $('<div class="row datatable-components" />');

		var $pageLength = $('<select class="page-length" />');
		$pageLength.append(new Option('20 ' + jse.core.lang.translate('PER_PAGE', 'admin_general'), 20, true, true)).append(new Option('30 ' + jse.core.lang.translate('PER_PAGE', 'admin_general')), 30).append(new Option('50 ' + jse.core.lang.translate('PER_PAGE', 'admin_general')), 50).append(new Option('100 ' + jse.core.lang.translate('PER_PAGE', 'admin_general')), 100).css('float', 'left').appendTo($datatableComponents);

		$table.siblings('.dataTables_info').appendTo($datatableComponents).css('clear', 'none');

		$table.siblings('.dataTables_paginate').appendTo($datatableComponents).css('clear', 'none');

		$paginator.append($datatableComponents);

		$table.parent().append($paginator);
	}

	function _onQuickSearchSubmit(event) {
		event.preventDefault();

		var $table = $(this).parent().siblings('.data-table');
		var keyword = $(this).find('.search-keyword').val();

		$table.DataTable().search(keyword, true, false).draw();
	}

	function _onPageLengthChange() {
		var $table = $(this).parents('.paginator').siblings('.data-table');
		$table.DataTable().page.len($(this).val()).draw();
	}

	/**
  * Handles the delete click event by opening the delete confirmation modal.
  *
  * @param {jQuery.Event} event Triggered event.
  * @param {String} url URL for AjaxRequest.
  * @param {Object} $table DataTable object
  */
	function _onDeleteClicked(event, url, $table) {
		// Prevent default action.
		event.preventDefault();

		// Clicked content entry
		var $content_entry = $(event.target).closest('.content-action-icons').siblings('.content-name');

		// Id of the content that should be deleted
		var content_id = $(event.target).parents('.delete-content').data('contentId');

		// Group Id of the content that should be deleted
		var group_id = $(event.target).parents('.delete-content').data('groupId');

		// Delete confirmation modal button.
		var $confirmButton = $modal.find('button.confirm');

		// Empty modal body.
		$modal.find('.modal-body .form-group').remove();

		// Slider data.
		var contentData = {
			id: content_id,
			groupId: group_id,
			name: $content_entry.text()
		};

		// Put new slider information into modal body.
		$modal.find('.modal-body fieldset').html(_generateContentInfoMarkup(contentData));

		// Show modal.
		$modal.modal('show');

		// Handle delete confirmation modal button click event.
		$confirmButton.off('click').on('click', function () {
			return _onConfirmButtonClick(contentData.groupId, url, $table);
		});
	}

	/**
  * Handles the delete confirmation button click event by removing the content through an AJAX request.
  *
  * @param {Number} groupId Group ID.
  * @param {String} url URL for AjaxRequest.
  * @param {Object} $table DataTable object
  */
	function _onConfirmButtonClick(groupId, url, $table) {
		// AJAX request options.
		var requestOptions = {
			type: 'POST',
			data: { id: groupId },
			url: url
		};

		// Perform request.
		$.ajax(requestOptions).done(function (response) {
			return _handleDeleteRequestResponse(response, $table, groupId);
		}).always(function () {
			return $modal.modal('hide');
		});
	}

	/**
  * Handles content deletion AJAX action server response.
  * 
  * @param {Object} response Server response.
  * @param {Object} $table DataTable object
  * @param {Number} groupId ID of deleted content.
  */
	function _handleDeleteRequestResponse(response, $table, groupId) {
		// Error message phrases.
		var errorTitle = jse.core.lang.translate('DELETE_CONTENT_ERROR_TITLE', 'content_manager');
		var errorMessage = jse.core.lang.translate('DELETE_CONTENT_ERROR_TEXT', 'content_manager');

		// Element that triggers the delete action
		var $delete_trigger = $('.delete-content' + ('[data-group-id="' + groupId + '"]'));

		// List element that should be removed
		var $element_to_delete = $delete_trigger.closest('.content-manager-element');

		// Check for action success.
		if (response.includes('success')) {
			// Delete respective table rows.

			$table.row($element_to_delete).remove().draw();

			// Add success message to admin info box.
			jse.libs.info_box.addSuccessMessage();
		} else {
			// Show error message modal.
			jse.libs.modal.showMessage(errorTitle, errorMessage);
		}
	}

	/**
  * Generates HTML containing the content entry information for the delete confirmation modal.
  *
  * @param {Object} data Content Manager data.
  * @param {String} data.name Name of the Content Manager entry.
  * @param {Number} data.groupId Id of the Content Manager entry.
  *
  * @return {String} Created HTML string.
  */
	function _generateContentInfoMarkup(data) {
		// Label phrases.
		var contentNameLabel = jse.core.lang.translate('TEXT_TITLE', 'content_manager');
		var groupIdLabel = jse.core.lang.translate('TEXT_GROUP', 'content_manager');

		// Return markup.
		return '\n\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t<label class="col-md-5">' + contentNameLabel + '</label>\n\t\t\t\t\t\t<div class="col-md-7">' + data.name + '</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t<label class="col-md-5">' + groupIdLabel + '</label>\n\t\t\t\t\t\t<div class="col-md-7">' + data.groupId + '</div>\n\t\t\t\t\t</div>\n\t\t\t';
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		var $tables = $this.find('.data-table');

		$tables.on('init.dt', function (event, settings, json) {
			var $table = $(this);
			_createTopBar($table);
			_createBottomBar($table);
		});

		var $table = jse.libs.datatable.create($tables, {
			autoWidth: false,
			dom: 'rtip',
			pageLength: 20,
			language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
			createdRow: function createdRow(row, data, dataIndex) {
				$(row).find('td').each(function () {
					$(this).html($(this).html().trim().replace(/(\r\n|\n|\r)/gm, ''));
				});
			}
		});

		$this.on('submit', '.quick-search', _onQuickSearchSubmit).on('change', '.page-length', _onPageLengthChange).on('click', '.delete-content.file', function (event) {
			_onDeleteClicked(event, urls.file, $table);
		}).on('click', '.delete-content.link', function (event) {
			_onDeleteClicked(event, urls.link, $table);
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRfbWFuYWdlci9wcm9kdWN0X2NvbnRlbnQvb3ZlcnZpZXcuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsImZpbHRlckNvbHVtbkluZGV4IiwiZmlsdGVyUmVnZXhQcmVmaXgiLCJleHBlcnQiLCJvcHRpb25zIiwiZXh0ZW5kIiwidXJscyIsInBhZ2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwiZWxlbWVudCIsImZpbGUiLCJsaW5rIiwiJG1vZGFsIiwiX2NyZWF0ZVRvcEJhciIsIiR0YWJsZSIsInBhcmVudCIsInByZXBlbmQiLCJsYW5nIiwidHJhbnNsYXRlIiwid2lkZ2V0cyIsImluaXQiLCJleHRlbnNpb25zIiwiX2NyZWF0ZUJvdHRvbUJhciIsIiRwYWdpbmF0b3IiLCIkZGF0YXRhYmxlQ29tcG9uZW50cyIsIiRwYWdlTGVuZ3RoIiwiYXBwZW5kIiwiT3B0aW9uIiwiY3NzIiwiYXBwZW5kVG8iLCJzaWJsaW5ncyIsIl9vblF1aWNrU2VhcmNoU3VibWl0IiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsImtleXdvcmQiLCJmaW5kIiwidmFsIiwiRGF0YVRhYmxlIiwic2VhcmNoIiwiZHJhdyIsIl9vblBhZ2VMZW5ndGhDaGFuZ2UiLCJwYXJlbnRzIiwibGVuIiwiX29uRGVsZXRlQ2xpY2tlZCIsInVybCIsIiRjb250ZW50X2VudHJ5IiwidGFyZ2V0IiwiY2xvc2VzdCIsImNvbnRlbnRfaWQiLCJncm91cF9pZCIsIiRjb25maXJtQnV0dG9uIiwicmVtb3ZlIiwiY29udGVudERhdGEiLCJpZCIsImdyb3VwSWQiLCJuYW1lIiwidGV4dCIsImh0bWwiLCJfZ2VuZXJhdGVDb250ZW50SW5mb01hcmt1cCIsIm1vZGFsIiwib2ZmIiwib24iLCJfb25Db25maXJtQnV0dG9uQ2xpY2siLCJyZXF1ZXN0T3B0aW9ucyIsInR5cGUiLCJhamF4IiwiZG9uZSIsIl9oYW5kbGVEZWxldGVSZXF1ZXN0UmVzcG9uc2UiLCJyZXNwb25zZSIsImFsd2F5cyIsImVycm9yVGl0bGUiLCJlcnJvck1lc3NhZ2UiLCIkZGVsZXRlX3RyaWdnZXIiLCIkZWxlbWVudF90b19kZWxldGUiLCJpbmNsdWRlcyIsInJvdyIsImxpYnMiLCJpbmZvX2JveCIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwic2hvd01lc3NhZ2UiLCJjb250ZW50TmFtZUxhYmVsIiwiZ3JvdXBJZExhYmVsIiwiJHRhYmxlcyIsInNldHRpbmdzIiwianNvbiIsImRhdGF0YWJsZSIsImNyZWF0ZSIsImF1dG9XaWR0aCIsImRvbSIsInBhZ2VMZW5ndGgiLCJsYW5ndWFnZSIsImdldFRyYW5zbGF0aW9ucyIsImNyZWF0ZWRSb3ciLCJkYXRhSW5kZXgiLCJlYWNoIiwidHJpbSIsInJlcGxhY2UiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsVUFERCxFQUdDLENBQ0lDLElBQUlDLE1BRFIsbURBRUlELElBQUlDLE1BRlIsa0RBR0MsV0FIRCxDQUhELEVBU0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFlBQVc7QUFDVkMscUJBQW1CLENBRFQ7QUFFVkMscUJBQW1CLEdBRlQ7QUFHRUMsVUFBUTtBQUhWLEVBYlo7OztBQW1CQzs7Ozs7QUFLQUMsV0FBVUwsRUFBRU0sTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CTCxRQUFuQixFQUE2QkgsSUFBN0IsQ0F4Qlg7OztBQTJCQzs7Ozs7QUFLQVMsUUFBTztBQUNOQyxRQUFTWixJQUFJYSxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQVQsa0RBRE07QUFFTkMsV0FBWWhCLElBQUlhLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsQ0FBWixrREFGTTtBQUdORSxRQUFTakIsSUFBSWEsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixDQUFULHFFQUhNO0FBSU5HLFFBQVNsQixJQUFJYSxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQVQ7QUFKTSxFQWhDUjs7O0FBdUNDOzs7OztBQUtBSSxVQUFTZixFQUFFLHVCQUFGLENBNUNWOzs7QUE4Q0M7Ozs7O0FBS0FMLFVBQVMsRUFuRFY7O0FBcURBO0FBQ0E7QUFDQTs7QUFFQSxVQUFTcUIsYUFBVCxDQUF1QkMsTUFBdkIsRUFBK0I7O0FBRzlCQSxTQUFPQyxNQUFQLEdBQWdCQyxPQUFoQixDQUF3QixzTUFJWXZCLElBQUlhLElBQUosQ0FBU1csSUFBVCxDQUFjQyxTQUFkLENBQXdCLFFBQXhCLEVBQWtDLGNBQWxDLENBSlosNGtCQWlCeUZoQixRQUFRRCxNQWpCakcsNkdBbUJkUixJQUFJYSxJQUFKLENBQVNXLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixtQkFBeEIsRUFBNkMsZUFBN0MsQ0FuQmMsNEJBb0JkekIsSUFBSWEsSUFBSixDQUFTVyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsNEJBQXhCLEVBQXNELGlCQUF0RCxDQXBCYyx1TEF1QjRGaEIsUUFBUUQsTUF2QnBHLDhCQXdCZFIsSUFBSWEsSUFBSixDQUFTVyxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLGVBQS9DLENBeEJjLDRCQXlCZHpCLElBQUlhLElBQUosQ0FBU1csSUFBVCxDQUFjQyxTQUFkLENBQXdCLDRCQUF4QixFQUFzRCxpQkFBdEQsQ0F6QmMsa0pBMEI0RmhCLFFBQVFELE1BMUJwRyw4QkEyQmRSLElBQUlhLElBQUosQ0FBU1csSUFBVCxDQUFjQyxTQUFkLENBQXdCLHFCQUF4QixFQUErQyxlQUEvQyxDQTNCYyw0QkE0QmR6QixJQUFJYSxJQUFKLENBQVNXLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qiw0QkFBeEIsRUFBc0QsaUJBQXRELENBNUJjLDRIQUF4Qjs7QUFvQ0E1QixLQUFHNkIsT0FBSCxDQUFXQyxJQUFYLENBQWdCTixPQUFPQyxNQUFQLEVBQWhCO0FBQ0F6QixLQUFHK0IsVUFBSCxDQUFjRCxJQUFkLENBQW1CTixPQUFPQyxNQUFQLEVBQW5CO0FBQ0E7O0FBRUQsVUFBU08sZ0JBQVQsQ0FBMEJSLE1BQTFCLEVBQWtDO0FBQ2pDLE1BQU1TLGFBQWExQixFQUFFLDJCQUFGLENBQW5CO0FBQ0EsTUFBTTJCLHVCQUF1QjNCLEVBQUUsMENBQUYsQ0FBN0I7O0FBRUEsTUFBTTRCLGNBQWM1QixFQUFFLGdDQUFGLENBQXBCO0FBQ0E0QixjQUNFQyxNQURGLENBQ1MsSUFBSUMsTUFBSixDQUFXLFFBQVFsQyxJQUFJYSxJQUFKLENBQVNXLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixVQUF4QixFQUFvQyxlQUFwQyxDQUFuQixFQUF5RSxFQUF6RSxFQUE2RSxJQUE3RSxFQUFtRixJQUFuRixDQURULEVBRUVRLE1BRkYsQ0FFUyxJQUFJQyxNQUFKLENBQVcsUUFBUWxDLElBQUlhLElBQUosQ0FBU1csSUFBVCxDQUFjQyxTQUFkLENBQXdCLFVBQXhCLEVBQW9DLGVBQXBDLENBQW5CLENBRlQsRUFFbUYsRUFGbkYsRUFHRVEsTUFIRixDQUdTLElBQUlDLE1BQUosQ0FBVyxRQUFRbEMsSUFBSWEsSUFBSixDQUFTVyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsVUFBeEIsRUFBb0MsZUFBcEMsQ0FBbkIsQ0FIVCxFQUdtRixFQUhuRixFQUlFUSxNQUpGLENBSVMsSUFBSUMsTUFBSixDQUFXLFNBQVNsQyxJQUFJYSxJQUFKLENBQVNXLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixVQUF4QixFQUFvQyxlQUFwQyxDQUFwQixDQUpULEVBSW9GLEdBSnBGLEVBS0VVLEdBTEYsQ0FLTSxPQUxOLEVBS2UsTUFMZixFQU1FQyxRQU5GLENBTVdMLG9CQU5YOztBQVFBVixTQUFPZ0IsUUFBUCxDQUFnQixrQkFBaEIsRUFDRUQsUUFERixDQUNXTCxvQkFEWCxFQUVFSSxHQUZGLENBRU0sT0FGTixFQUVlLE1BRmY7O0FBSUFkLFNBQU9nQixRQUFQLENBQWdCLHNCQUFoQixFQUNFRCxRQURGLENBQ1dMLG9CQURYLEVBRUVJLEdBRkYsQ0FFTSxPQUZOLEVBRWUsTUFGZjs7QUFJQUwsYUFDRUcsTUFERixDQUNTRixvQkFEVDs7QUFHQVYsU0FBT0MsTUFBUCxHQUFnQlcsTUFBaEIsQ0FBdUJILFVBQXZCO0FBQ0E7O0FBRUQsVUFBU1Esb0JBQVQsQ0FBOEJDLEtBQTlCLEVBQXFDO0FBQ3BDQSxRQUFNQyxjQUFOOztBQUVBLE1BQU1uQixTQUFTakIsRUFBRSxJQUFGLEVBQVFrQixNQUFSLEdBQWlCZSxRQUFqQixDQUEwQixhQUExQixDQUFmO0FBQ0EsTUFBTUksVUFBVXJDLEVBQUUsSUFBRixFQUFRc0MsSUFBUixDQUFhLGlCQUFiLEVBQWdDQyxHQUFoQyxFQUFoQjs7QUFFQXRCLFNBQU91QixTQUFQLEdBQW1CQyxNQUFuQixDQUEwQkosT0FBMUIsRUFBbUMsSUFBbkMsRUFBeUMsS0FBekMsRUFBZ0RLLElBQWhEO0FBQ0E7O0FBRUQsVUFBU0MsbUJBQVQsR0FBK0I7QUFDOUIsTUFBTTFCLFNBQVNqQixFQUFFLElBQUYsRUFBUTRDLE9BQVIsQ0FBZ0IsWUFBaEIsRUFBOEJYLFFBQTlCLENBQXVDLGFBQXZDLENBQWY7QUFDQWhCLFNBQU91QixTQUFQLEdBQW1CaEMsSUFBbkIsQ0FBd0JxQyxHQUF4QixDQUE0QjdDLEVBQUUsSUFBRixFQUFRdUMsR0FBUixFQUE1QixFQUEyQ0csSUFBM0M7QUFDQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNJLGdCQUFULENBQTBCWCxLQUExQixFQUFpQ1ksR0FBakMsRUFBc0M5QixNQUF0QyxFQUE4QztBQUM3QztBQUNBa0IsUUFBTUMsY0FBTjs7QUFFQTtBQUNBLE1BQU1ZLGlCQUFpQmhELEVBQUVtQyxNQUFNYyxNQUFSLEVBQWdCQyxPQUFoQixDQUF3Qix1QkFBeEIsRUFBaURqQixRQUFqRCxDQUEwRCxlQUExRCxDQUF2Qjs7QUFFQTtBQUNBLE1BQU1rQixhQUFhbkQsRUFBRW1DLE1BQU1jLE1BQVIsRUFBZ0JMLE9BQWhCLENBQXdCLGlCQUF4QixFQUEyQzlDLElBQTNDLENBQWdELFdBQWhELENBQW5COztBQUVBO0FBQ0EsTUFBTXNELFdBQVdwRCxFQUFFbUMsTUFBTWMsTUFBUixFQUFnQkwsT0FBaEIsQ0FBd0IsaUJBQXhCLEVBQTJDOUMsSUFBM0MsQ0FBZ0QsU0FBaEQsQ0FBakI7O0FBRUE7QUFDQSxNQUFNdUQsaUJBQWlCdEMsT0FBT3VCLElBQVAsQ0FBWSxnQkFBWixDQUF2Qjs7QUFFQTtBQUNBdkIsU0FBT3VCLElBQVAsQ0FBWSx5QkFBWixFQUF1Q2dCLE1BQXZDOztBQUVBO0FBQ0EsTUFBTUMsY0FBYztBQUNuQkMsT0FBSUwsVUFEZTtBQUVuQk0sWUFBU0wsUUFGVTtBQUduQk0sU0FBTVYsZUFBZVcsSUFBZjtBQUhhLEdBQXBCOztBQU1BO0FBQ0E1QyxTQUNFdUIsSUFERixDQUNPLHNCQURQLEVBRUVzQixJQUZGLENBRU9DLDJCQUEyQk4sV0FBM0IsQ0FGUDs7QUFJQTtBQUNBeEMsU0FBTytDLEtBQVAsQ0FBYSxNQUFiOztBQUVBO0FBQ0FULGlCQUNFVSxHQURGLENBQ00sT0FETixFQUVFQyxFQUZGLENBRUssT0FGTCxFQUVjO0FBQUEsVUFBTUMsc0JBQXNCVixZQUFZRSxPQUFsQyxFQUEyQ1YsR0FBM0MsRUFBZ0Q5QixNQUFoRCxDQUFOO0FBQUEsR0FGZDtBQUdBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU2dELHFCQUFULENBQStCUixPQUEvQixFQUF3Q1YsR0FBeEMsRUFBNkM5QixNQUE3QyxFQUFxRDtBQUNwRDtBQUNBLE1BQU1pRCxpQkFBaUI7QUFDdEJDLFNBQU0sTUFEZ0I7QUFFdEJyRSxTQUFNLEVBQUMwRCxJQUFJQyxPQUFMLEVBRmdCO0FBR3RCVjtBQUhzQixHQUF2Qjs7QUFNQTtBQUNBL0MsSUFBRW9FLElBQUYsQ0FBT0YsY0FBUCxFQUNFRyxJQURGLENBQ087QUFBQSxVQUFZQyw2QkFBNkJDLFFBQTdCLEVBQXVDdEQsTUFBdkMsRUFBK0N3QyxPQUEvQyxDQUFaO0FBQUEsR0FEUCxFQUVFZSxNQUZGLENBRVM7QUFBQSxVQUFNekQsT0FBTytDLEtBQVAsQ0FBYSxNQUFiLENBQU47QUFBQSxHQUZUO0FBR0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTUSw0QkFBVCxDQUFzQ0MsUUFBdEMsRUFBZ0R0RCxNQUFoRCxFQUF3RHdDLE9BQXhELEVBQWlFO0FBQ2hFO0FBQ0EsTUFBTWdCLGFBQWE3RSxJQUFJYSxJQUFKLENBQVNXLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qiw0QkFBeEIsRUFBc0QsaUJBQXRELENBQW5CO0FBQ0EsTUFBTXFELGVBQWU5RSxJQUFJYSxJQUFKLENBQVNXLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwyQkFBeEIsRUFBcUQsaUJBQXJELENBQXJCOztBQUVBO0FBQ0EsTUFBTXNELGtCQUFrQjNFLEVBQUUsMENBQXFDeUQsT0FBckMsUUFBRixDQUF4Qjs7QUFFQTtBQUNBLE1BQU1tQixxQkFBcUJELGdCQUFnQnpCLE9BQWhCLENBQXdCLDBCQUF4QixDQUEzQjs7QUFFQTtBQUNBLE1BQUlxQixTQUFTTSxRQUFULENBQWtCLFNBQWxCLENBQUosRUFBa0M7QUFDakM7O0FBRUE1RCxVQUFPNkQsR0FBUCxDQUFXRixrQkFBWCxFQUErQnRCLE1BQS9CLEdBQXdDWixJQUF4Qzs7QUFFQTtBQUNBOUMsT0FBSW1GLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsaUJBQWxCO0FBQ0EsR0FQRCxNQU9PO0FBQ047QUFDQXJGLE9BQUltRixJQUFKLENBQVNqQixLQUFULENBQWVvQixXQUFmLENBQTJCVCxVQUEzQixFQUF1Q0MsWUFBdkM7QUFDQTtBQUNEOztBQUVEOzs7Ozs7Ozs7QUFTQSxVQUFTYiwwQkFBVCxDQUFvQy9ELElBQXBDLEVBQTBDO0FBQ3pDO0FBQ0EsTUFBTXFGLG1CQUFtQnZGLElBQUlhLElBQUosQ0FBU1csSUFBVCxDQUFjQyxTQUFkLENBQXdCLFlBQXhCLEVBQXNDLGlCQUF0QyxDQUF6QjtBQUNBLE1BQU0rRCxlQUFleEYsSUFBSWEsSUFBSixDQUFTVyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsWUFBeEIsRUFBc0MsaUJBQXRDLENBQXJCOztBQUVBO0FBQ0Esd0ZBRTZCOEQsZ0JBRjdCLG9EQUcyQnJGLEtBQUs0RCxJQUhoQyw0R0FPNkIwQixZQVA3QixvREFRMkJ0RixLQUFLMkQsT0FSaEM7QUFXQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE5RCxRQUFPNEIsSUFBUCxHQUFjLFVBQVM4QyxJQUFULEVBQWU7QUFDNUIsTUFBTWdCLFVBQVV0RixNQUFNdUMsSUFBTixDQUFXLGFBQVgsQ0FBaEI7O0FBRUErQyxVQUFRckIsRUFBUixDQUFXLFNBQVgsRUFBc0IsVUFBUzdCLEtBQVQsRUFBZ0JtRCxRQUFoQixFQUEwQkMsSUFBMUIsRUFBZ0M7QUFDckQsT0FBTXRFLFNBQVNqQixFQUFFLElBQUYsQ0FBZjtBQUNBZ0IsaUJBQWNDLE1BQWQ7QUFDQVEsb0JBQWlCUixNQUFqQjtBQUNBLEdBSkQ7O0FBTUEsTUFBTUEsU0FBUXJCLElBQUltRixJQUFKLENBQVNTLFNBQVQsQ0FBbUJDLE1BQW5CLENBQTBCSixPQUExQixFQUFtQztBQUNoREssY0FBVyxLQURxQztBQUVoREMsUUFBSyxNQUYyQztBQUdoREMsZUFBWSxFQUhvQztBQUloREMsYUFBVWpHLElBQUltRixJQUFKLENBQVNTLFNBQVQsQ0FBbUJNLGVBQW5CLENBQW1DbEcsSUFBSWEsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixjQUFwQixDQUFuQyxDQUpzQztBQUtoRG9GLGVBQVksb0JBQVNqQixHQUFULEVBQWNoRixJQUFkLEVBQW9Ca0csU0FBcEIsRUFBK0I7QUFDMUNoRyxNQUFFOEUsR0FBRixFQUFPeEMsSUFBUCxDQUFZLElBQVosRUFBa0IyRCxJQUFsQixDQUF1QixZQUFXO0FBQ2pDakcsT0FBRSxJQUFGLEVBQVE0RCxJQUFSLENBQWE1RCxFQUFFLElBQUYsRUFBUTRELElBQVIsR0FBZXNDLElBQWYsR0FBc0JDLE9BQXRCLENBQThCLGdCQUE5QixFQUFnRCxFQUFoRCxDQUFiO0FBQ0EsS0FGRDtBQUdBO0FBVCtDLEdBQW5DLENBQWQ7O0FBWUFwRyxRQUNFaUUsRUFERixDQUNLLFFBREwsRUFDZSxlQURmLEVBQ2dDOUIsb0JBRGhDLEVBRUU4QixFQUZGLENBRUssUUFGTCxFQUVlLGNBRmYsRUFFK0JyQixtQkFGL0IsRUFHRXFCLEVBSEYsQ0FHSyxPQUhMLEVBR2Msc0JBSGQsRUFHc0MsVUFBUzdCLEtBQVQsRUFBZ0I7QUFDcERXLG9CQUFpQlgsS0FBakIsRUFBd0I1QixLQUFLTSxJQUE3QixFQUFtQ0ksTUFBbkM7QUFDQSxHQUxGLEVBTUUrQyxFQU5GLENBTUssT0FOTCxFQU1jLHNCQU5kLEVBTXNDLFVBQVM3QixLQUFULEVBQWdCO0FBQ3BEVyxvQkFBaUJYLEtBQWpCLEVBQXdCNUIsS0FBS08sSUFBN0IsRUFBbUNHLE1BQW5DO0FBQ0EsR0FSRjs7QUFVQW9EO0FBQ0EsRUFoQ0Q7O0FBa0NBLFFBQU8xRSxNQUFQO0FBQ0EsQ0FyVUYiLCJmaWxlIjoiY29udGVudF9tYW5hZ2VyL3Byb2R1Y3RfY29udGVudC9vdmVydmlldy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gcHJvZHVjdF9jb250ZW50LmpzIDIwMTctMDktMjJcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdvdmVydmlldycsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmpzYCxcblx0XHQnZGF0YXRhYmxlJ1xuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdGZpbHRlckNvbHVtbkluZGV4OiAwLFxuXHRcdFx0XHRmaWx0ZXJSZWdleFByZWZpeDogJ14nLFxuICAgICAgICAgICAgICAgIGV4cGVydDogJydcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogVVJMcyBmb3IgZGVsZXRpbmcgZGlmZmVyZW50IHR5cGVzIG9mIGNvbnRlbnRcblx0XHRcdCAqIFxuXHRcdFx0ICogQHR5cGUge3twYWdlOiBzdHJpbmcsIGVsZW1lbnQ6IHN0cmluZywgZmlsZTogc3RyaW5nLCBsaW5rOiBzdHJpbmd9fVxuXHRcdFx0ICovXG5cdFx0XHR1cmxzID0ge1xuXHRcdFx0XHRwYWdlOiBgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYWRtaW4vYWRtaW4ucGhwP2RvPUNvbnRlbnRNYW5hZ2VyQWpheC9EZWxldGVgLFxuXHRcdFx0XHRlbGVtZW50OiBgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYWRtaW4vYWRtaW4ucGhwP2RvPUNvbnRlbnRNYW5hZ2VyQWpheC9EZWxldGVgLFxuXHRcdFx0XHRmaWxlOiBgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYWRtaW4vYWRtaW4ucGhwP2RvPUNvbnRlbnRNYW5hZ2VyUHJvZHVjdENvbnRlbnRzQWpheC9kZWxldGVGaWxlYCxcblx0XHRcdFx0bGluazogYCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2FkbWluL2FkbWluLnBocD9kbz1Db250ZW50TWFuYWdlclByb2R1Y3RDb250ZW50c0FqYXgvZGVsZXRlTGlua2Bcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogQ29udGVudCBNYW5hZ2VyIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0JG1vZGFsID0gJCgnLmRlbGV0ZS1jb250ZW50Lm1vZGFsJyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdGZ1bmN0aW9uIF9jcmVhdGVUb3BCYXIoJHRhYmxlKSB7XG5cdFx0XHRcblx0XHRcdFxuXHRcdFx0JHRhYmxlLnBhcmVudCgpLnByZXBlbmQoYFxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwicm93IGhlYWRsaW5lXCI+XG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC02IHF1aWNrLXNlYXJjaFwiPlxuXHRcdFx0XHRcdFx0PGZvcm0gY2xhc3M9XCJmb3JtLWlubGluZSBmb3JtLWdyb3VwIHJlbW92ZS1wYWRkaW5nXCI+XG5cdFx0XHRcdFx0XHRcdDxsYWJlbCBmb3I9XCJzZWFyY2gta2V5d29yZFwiPmAgKyBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc2VhcmNoJywgJ2FkbWluX2xhYmVscycpICsgYDwvbGFiZWw+XG5cdFx0XHRcdFx0XHRcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwic2VhcmNoLWtleXdvcmRcIiAvPlxuXHRcdFx0XHRcdFx0PC9mb3JtPlxuXHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxuXG5cdFx0XHRcdFx0XHQ8ZGl2IGRhdGEtZ3gtd2lkZ2V0PVwiYnV0dG9uX2Ryb3Bkb3duXCJcblx0XHRcdFx0XHRcdCAgICAgZGF0YS1idXR0b25fZHJvcGRvd24tdXNlcl9pZD1cInskc21hcnR5LnNlc3Npb24uY3VzdG9tZXJfaWR9XCJcblx0XHRcdFx0XHRcdD5cblx0XHRcdFx0XHRcdFx0PGRpdiBkYXRhLXVzZS1idXR0b25fZHJvcGRvd249XCJ0cnVlXCJcblx0XHRcdFx0XHRcdFx0ICAgICBkYXRhLWN1c3RvbV9jYXJldF9idG5fY2xhc3M9XCJidG4tc3VjY2Vzc1wiXG5cdFx0XHRcdFx0XHRcdCAgICAgY2xhc3M9XCJwdWxsLXJpZ2h0XCJcblx0XHRcdFx0XHRcdFx0PlxuXHRcdFx0XHRcdFx0XHRcdDxidXR0b24gZGF0YS1neC1leHRlbnNpb249XCJsaW5rXCIgZGF0YS1saW5rLXVybD1cImFkbWluLnBocD9kbz1Db250ZW50TWFuYWdlclByb2R1Y3RDb250ZW50cy9uZXcmdHlwZT1maWxlYCArIG9wdGlvbnMuZXhwZXJ0ICsgYFwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8aSBjbGFzcz1cImZhIGZhLXBsdXMgYnRuLWljb25cIj48L2k+XG5cdFx0XHRcdFx0XHRcdFx0XHRgICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9ORVdfUFJFRklYJywgJ2FkbWluX2J1dHRvbnMnKSArIGBcblx0XHRcdFx0XHRcdFx0XHRcdGAgKyBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnTEFCRUxfUFJPRFVDVF9DT05URU5UX0ZJTEUnLCAnY29udGVudF9tYW5hZ2VyJykgKyBgXG5cdFx0XHRcdFx0XHRcdFx0PC9idXR0b24+XG5cdFx0XHRcdFx0XHRcdFx0PHVsPlxuXHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxzcGFuIGRhdGEtZ3gtZXh0ZW5zaW9uPVwibGlua1wiIGRhdGEtbGluay11cmw9XCJhZG1pbi5waHA/ZG89Q29udGVudE1hbmFnZXJQcm9kdWN0Q29udGVudHMvbmV3JnR5cGU9bGlua2AgKyBvcHRpb25zLmV4cGVydCArIGBcIj5cblx0XHRcdFx0XHRcdFx0XHRcdGAgKyBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX05FV19QUkVGSVhfMicsICdhZG1pbl9idXR0b25zJykgKyBgXG5cdFx0XHRcdFx0XHRcdFx0XHRgICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0xBQkVMX1BST0RVQ1RfQ09OVEVOVF9MSU5LJywgJ2NvbnRlbnRfbWFuYWdlcicpICsgYDwvc3Bhbj48L2xpPlxuXHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxzcGFuIGRhdGEtZ3gtZXh0ZW5zaW9uPVwibGlua1wiIGRhdGEtbGluay11cmw9XCJhZG1pbi5waHA/ZG89Q29udGVudE1hbmFnZXJQcm9kdWN0Q29udGVudHMvbmV3JnR5cGU9dGV4dGAgKyBvcHRpb25zLmV4cGVydCArIGBcIj5cblx0XHRcdFx0XHRcdFx0XHRcdGAgKyBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX05FV19QUkVGSVhfMicsICdhZG1pbl9idXR0b25zJykgKyBgXG5cdFx0XHRcdFx0XHRcdFx0XHRgICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0xBQkVMX1BST0RVQ1RfQ09OVEVOVF9URVhUJywgJ2NvbnRlbnRfbWFuYWdlcicpICsgYDwvc3Bhbj48L2xpPlxuXHRcdFx0XHRcdFx0XHRcdDwvdWw+XG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0YCk7XG5cdFx0XHRcblx0XHRcdGd4LndpZGdldHMuaW5pdCgkdGFibGUucGFyZW50KCkpO1xuXHRcdFx0Z3guZXh0ZW5zaW9ucy5pbml0KCR0YWJsZS5wYXJlbnQoKSk7XG5cdFx0fVxuXHRcdFxuXHRcdGZ1bmN0aW9uIF9jcmVhdGVCb3R0b21CYXIoJHRhYmxlKSB7XG5cdFx0XHRjb25zdCAkcGFnaW5hdG9yID0gJCgnPGRpdiBjbGFzcz1cInBhZ2luYXRvclwiIC8+Jyk7XG5cdFx0XHRjb25zdCAkZGF0YXRhYmxlQ29tcG9uZW50cyA9ICQoJzxkaXYgY2xhc3M9XCJyb3cgZGF0YXRhYmxlLWNvbXBvbmVudHNcIiAvPicpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCAkcGFnZUxlbmd0aCA9ICQoJzxzZWxlY3QgY2xhc3M9XCJwYWdlLWxlbmd0aFwiIC8+Jyk7XG5cdFx0XHQkcGFnZUxlbmd0aFxuXHRcdFx0XHQuYXBwZW5kKG5ldyBPcHRpb24oJzIwICcgKyBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnUEVSX1BBR0UnLCAnYWRtaW5fZ2VuZXJhbCcpLCAyMCwgdHJ1ZSwgdHJ1ZSkpXG5cdFx0XHRcdC5hcHBlbmQobmV3IE9wdGlvbignMzAgJyArIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdQRVJfUEFHRScsICdhZG1pbl9nZW5lcmFsJykpLCAzMClcblx0XHRcdFx0LmFwcGVuZChuZXcgT3B0aW9uKCc1MCAnICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1BFUl9QQUdFJywgJ2FkbWluX2dlbmVyYWwnKSksIDUwKVxuXHRcdFx0XHQuYXBwZW5kKG5ldyBPcHRpb24oJzEwMCAnICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1BFUl9QQUdFJywgJ2FkbWluX2dlbmVyYWwnKSksIDEwMClcblx0XHRcdFx0LmNzcygnZmxvYXQnLCAnbGVmdCcpXG5cdFx0XHRcdC5hcHBlbmRUbygkZGF0YXRhYmxlQ29tcG9uZW50cyk7XG5cdFx0XHRcblx0XHRcdCR0YWJsZS5zaWJsaW5ncygnLmRhdGFUYWJsZXNfaW5mbycpXG5cdFx0XHRcdC5hcHBlbmRUbygkZGF0YXRhYmxlQ29tcG9uZW50cylcblx0XHRcdFx0LmNzcygnY2xlYXInLCAnbm9uZScpO1xuXHRcdFx0XG5cdFx0XHQkdGFibGUuc2libGluZ3MoJy5kYXRhVGFibGVzX3BhZ2luYXRlJylcblx0XHRcdFx0LmFwcGVuZFRvKCRkYXRhdGFibGVDb21wb25lbnRzKVxuXHRcdFx0XHQuY3NzKCdjbGVhcicsICdub25lJyk7XG5cdFx0XHRcblx0XHRcdCRwYWdpbmF0b3Jcblx0XHRcdFx0LmFwcGVuZCgkZGF0YXRhYmxlQ29tcG9uZW50cyk7XG5cdFx0XHRcblx0XHRcdCR0YWJsZS5wYXJlbnQoKS5hcHBlbmQoJHBhZ2luYXRvcik7XG5cdFx0fVxuXHRcdFxuXHRcdGZ1bmN0aW9uIF9vblF1aWNrU2VhcmNoU3VibWl0KGV2ZW50KSB7XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCAkdGFibGUgPSAkKHRoaXMpLnBhcmVudCgpLnNpYmxpbmdzKCcuZGF0YS10YWJsZScpO1xuXHRcdFx0Y29uc3Qga2V5d29yZCA9ICQodGhpcykuZmluZCgnLnNlYXJjaC1rZXl3b3JkJykudmFsKCk7XG5cdFx0XHRcblx0XHRcdCR0YWJsZS5EYXRhVGFibGUoKS5zZWFyY2goa2V5d29yZCwgdHJ1ZSwgZmFsc2UpLmRyYXcoKTtcblx0XHR9XG5cdFx0XG5cdFx0ZnVuY3Rpb24gX29uUGFnZUxlbmd0aENoYW5nZSgpIHtcblx0XHRcdGNvbnN0ICR0YWJsZSA9ICQodGhpcykucGFyZW50cygnLnBhZ2luYXRvcicpLnNpYmxpbmdzKCcuZGF0YS10YWJsZScpO1xuXHRcdFx0JHRhYmxlLkRhdGFUYWJsZSgpLnBhZ2UubGVuKCQodGhpcykudmFsKCkpLmRyYXcoKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgZGVsZXRlIGNsaWNrIGV2ZW50IGJ5IG9wZW5pbmcgdGhlIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGV2ZW50LlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSB1cmwgVVJMIGZvciBBamF4UmVxdWVzdC5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gJHRhYmxlIERhdGFUYWJsZSBvYmplY3Rcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25EZWxldGVDbGlja2VkKGV2ZW50LCB1cmwsICR0YWJsZSkge1xuXHRcdFx0Ly8gUHJldmVudCBkZWZhdWx0IGFjdGlvbi5cblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdC8vIENsaWNrZWQgY29udGVudCBlbnRyeVxuXHRcdFx0Y29uc3QgJGNvbnRlbnRfZW50cnkgPSAkKGV2ZW50LnRhcmdldCkuY2xvc2VzdCgnLmNvbnRlbnQtYWN0aW9uLWljb25zJykuc2libGluZ3MoJy5jb250ZW50LW5hbWUnKTtcblx0XHRcdFxuXHRcdFx0Ly8gSWQgb2YgdGhlIGNvbnRlbnQgdGhhdCBzaG91bGQgYmUgZGVsZXRlZFxuXHRcdFx0Y29uc3QgY29udGVudF9pZCA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCcuZGVsZXRlLWNvbnRlbnQnKS5kYXRhKCdjb250ZW50SWQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gR3JvdXAgSWQgb2YgdGhlIGNvbnRlbnQgdGhhdCBzaG91bGQgYmUgZGVsZXRlZFxuXHRcdFx0Y29uc3QgZ3JvdXBfaWQgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cygnLmRlbGV0ZS1jb250ZW50JykuZGF0YSgnZ3JvdXBJZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBEZWxldGUgY29uZmlybWF0aW9uIG1vZGFsIGJ1dHRvbi5cblx0XHRcdGNvbnN0ICRjb25maXJtQnV0dG9uID0gJG1vZGFsLmZpbmQoJ2J1dHRvbi5jb25maXJtJyk7XG5cdFx0XHRcblx0XHRcdC8vIEVtcHR5IG1vZGFsIGJvZHkuXG5cdFx0XHQkbW9kYWwuZmluZCgnLm1vZGFsLWJvZHkgLmZvcm0tZ3JvdXAnKS5yZW1vdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2xpZGVyIGRhdGEuXG5cdFx0XHRjb25zdCBjb250ZW50RGF0YSA9IHtcblx0XHRcdFx0aWQ6IGNvbnRlbnRfaWQsXG5cdFx0XHRcdGdyb3VwSWQ6IGdyb3VwX2lkLFxuXHRcdFx0XHRuYW1lOiAkY29udGVudF9lbnRyeS50ZXh0KClcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIFB1dCBuZXcgc2xpZGVyIGluZm9ybWF0aW9uIGludG8gbW9kYWwgYm9keS5cblx0XHRcdCRtb2RhbFxuXHRcdFx0XHQuZmluZCgnLm1vZGFsLWJvZHkgZmllbGRzZXQnKVxuXHRcdFx0XHQuaHRtbChfZ2VuZXJhdGVDb250ZW50SW5mb01hcmt1cChjb250ZW50RGF0YSkpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0JG1vZGFsLm1vZGFsKCdzaG93Jyk7XG5cdFx0XHRcblx0XHRcdC8vIEhhbmRsZSBkZWxldGUgY29uZmlybWF0aW9uIG1vZGFsIGJ1dHRvbiBjbGljayBldmVudC5cblx0XHRcdCRjb25maXJtQnV0dG9uXG5cdFx0XHRcdC5vZmYoJ2NsaWNrJylcblx0XHRcdFx0Lm9uKCdjbGljaycsICgpID0+IF9vbkNvbmZpcm1CdXR0b25DbGljayhjb250ZW50RGF0YS5ncm91cElkLCB1cmwsICR0YWJsZSkpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBkZWxldGUgY29uZmlybWF0aW9uIGJ1dHRvbiBjbGljayBldmVudCBieSByZW1vdmluZyB0aGUgY29udGVudCB0aHJvdWdoIGFuIEFKQVggcmVxdWVzdC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7TnVtYmVyfSBncm91cElkIEdyb3VwIElELlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSB1cmwgVVJMIGZvciBBamF4UmVxdWVzdC5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gJHRhYmxlIERhdGFUYWJsZSBvYmplY3Rcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25Db25maXJtQnV0dG9uQ2xpY2soZ3JvdXBJZCwgdXJsLCAkdGFibGUpIHtcblx0XHRcdC8vIEFKQVggcmVxdWVzdCBvcHRpb25zLlxuXHRcdFx0Y29uc3QgcmVxdWVzdE9wdGlvbnMgPSB7XG5cdFx0XHRcdHR5cGU6ICdQT1NUJyxcblx0XHRcdFx0ZGF0YToge2lkOiBncm91cElkfSxcblx0XHRcdFx0dXJsXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBQZXJmb3JtIHJlcXVlc3QuXG5cdFx0XHQkLmFqYXgocmVxdWVzdE9wdGlvbnMpXG5cdFx0XHRcdC5kb25lKHJlc3BvbnNlID0+IF9oYW5kbGVEZWxldGVSZXF1ZXN0UmVzcG9uc2UocmVzcG9uc2UsICR0YWJsZSwgZ3JvdXBJZCkpXG5cdFx0XHRcdC5hbHdheXMoKCkgPT4gJG1vZGFsLm1vZGFsKCdoaWRlJykpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIGNvbnRlbnQgZGVsZXRpb24gQUpBWCBhY3Rpb24gc2VydmVyIHJlc3BvbnNlLlxuXHRcdCAqIFxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSByZXNwb25zZSBTZXJ2ZXIgcmVzcG9uc2UuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9ICR0YWJsZSBEYXRhVGFibGUgb2JqZWN0XG5cdFx0ICogQHBhcmFtIHtOdW1iZXJ9IGdyb3VwSWQgSUQgb2YgZGVsZXRlZCBjb250ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9oYW5kbGVEZWxldGVSZXF1ZXN0UmVzcG9uc2UocmVzcG9uc2UsICR0YWJsZSwgZ3JvdXBJZCkge1xuXHRcdFx0Ly8gRXJyb3IgbWVzc2FnZSBwaHJhc2VzLlxuXHRcdFx0Y29uc3QgZXJyb3JUaXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdERUxFVEVfQ09OVEVOVF9FUlJPUl9USVRMRScsICdjb250ZW50X21hbmFnZXInKTtcblx0XHRcdGNvbnN0IGVycm9yTWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdERUxFVEVfQ09OVEVOVF9FUlJPUl9URVhUJywgJ2NvbnRlbnRfbWFuYWdlcicpO1xuXHRcdFx0XG5cdFx0XHQvLyBFbGVtZW50IHRoYXQgdHJpZ2dlcnMgdGhlIGRlbGV0ZSBhY3Rpb25cblx0XHRcdGNvbnN0ICRkZWxldGVfdHJpZ2dlciA9ICQoJy5kZWxldGUtY29udGVudCcrYFtkYXRhLWdyb3VwLWlkPVwiJHtncm91cElkfVwiXWApO1xuXHRcdFx0XG5cdFx0XHQvLyBMaXN0IGVsZW1lbnQgdGhhdCBzaG91bGQgYmUgcmVtb3ZlZFxuXHRcdFx0Y29uc3QgJGVsZW1lbnRfdG9fZGVsZXRlID0gJGRlbGV0ZV90cmlnZ2VyLmNsb3Nlc3QoJy5jb250ZW50LW1hbmFnZXItZWxlbWVudCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBDaGVjayBmb3IgYWN0aW9uIHN1Y2Nlc3MuXG5cdFx0XHRpZiAocmVzcG9uc2UuaW5jbHVkZXMoJ3N1Y2Nlc3MnKSkge1xuXHRcdFx0XHQvLyBEZWxldGUgcmVzcGVjdGl2ZSB0YWJsZSByb3dzLlxuXHRcdFx0XHRcblx0XHRcdFx0JHRhYmxlLnJvdygkZWxlbWVudF90b19kZWxldGUpLnJlbW92ZSgpLmRyYXcoKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFkZCBzdWNjZXNzIG1lc3NhZ2UgdG8gYWRtaW4gaW5mbyBib3guXG5cdFx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQvLyBTaG93IGVycm9yIG1lc3NhZ2UgbW9kYWwuXG5cdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKGVycm9yVGl0bGUsIGVycm9yTWVzc2FnZSk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdlbmVyYXRlcyBIVE1MIGNvbnRhaW5pbmcgdGhlIGNvbnRlbnQgZW50cnkgaW5mb3JtYXRpb24gZm9yIHRoZSBkZWxldGUgY29uZmlybWF0aW9uIG1vZGFsLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGRhdGEgQ29udGVudCBNYW5hZ2VyIGRhdGEuXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGRhdGEubmFtZSBOYW1lIG9mIHRoZSBDb250ZW50IE1hbmFnZXIgZW50cnkuXG5cdFx0ICogQHBhcmFtIHtOdW1iZXJ9IGRhdGEuZ3JvdXBJZCBJZCBvZiB0aGUgQ29udGVudCBNYW5hZ2VyIGVudHJ5LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7U3RyaW5nfSBDcmVhdGVkIEhUTUwgc3RyaW5nLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZW5lcmF0ZUNvbnRlbnRJbmZvTWFya3VwKGRhdGEpIHtcblx0XHRcdC8vIExhYmVsIHBocmFzZXMuXG5cdFx0XHRjb25zdCBjb250ZW50TmFtZUxhYmVsID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfVElUTEUnLCAnY29udGVudF9tYW5hZ2VyJyk7XG5cdFx0XHRjb25zdCBncm91cElkTGFiZWwgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9HUk9VUCcsICdjb250ZW50X21hbmFnZXInKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmV0dXJuIG1hcmt1cC5cblx0XHRcdHJldHVybiBgXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC01XCI+JHtjb250ZW50TmFtZUxhYmVsfTwvbGFiZWw+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTdcIj4ke2RhdGEubmFtZX08L2Rpdj5cblx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtNVwiPiR7Z3JvdXBJZExhYmVsfTwvbGFiZWw+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTdcIj4ke2RhdGEuZ3JvdXBJZH08L2Rpdj5cblx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdGA7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRjb25zdCAkdGFibGVzID0gJHRoaXMuZmluZCgnLmRhdGEtdGFibGUnKTtcblx0XHRcdFxuXHRcdFx0JHRhYmxlcy5vbignaW5pdC5kdCcsIGZ1bmN0aW9uKGV2ZW50LCBzZXR0aW5ncywganNvbikge1xuXHRcdFx0XHRjb25zdCAkdGFibGUgPSAkKHRoaXMpO1xuXHRcdFx0XHRfY3JlYXRlVG9wQmFyKCR0YWJsZSk7XG5cdFx0XHRcdF9jcmVhdGVCb3R0b21CYXIoJHRhYmxlKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRjb25zdCAkdGFibGUgPWpzZS5saWJzLmRhdGF0YWJsZS5jcmVhdGUoJHRhYmxlcywge1xuXHRcdFx0XHRhdXRvV2lkdGg6IGZhbHNlLFxuXHRcdFx0XHRkb206ICdydGlwJyxcblx0XHRcdFx0cGFnZUxlbmd0aDogMjAsXG5cdFx0XHRcdGxhbmd1YWdlOiBqc2UubGlicy5kYXRhdGFibGUuZ2V0VHJhbnNsYXRpb25zKGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpKSxcblx0XHRcdFx0Y3JlYXRlZFJvdzogZnVuY3Rpb24ocm93LCBkYXRhLCBkYXRhSW5kZXgpIHtcblx0XHRcdFx0XHQkKHJvdykuZmluZCgndGQnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0JCh0aGlzKS5odG1sKCQodGhpcykuaHRtbCgpLnRyaW0oKS5yZXBsYWNlKC8oXFxyXFxufFxcbnxcXHIpL2dtLCAnJykpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdzdWJtaXQnLCAnLnF1aWNrLXNlYXJjaCcsIF9vblF1aWNrU2VhcmNoU3VibWl0KVxuXHRcdFx0XHQub24oJ2NoYW5nZScsICcucGFnZS1sZW5ndGgnLCBfb25QYWdlTGVuZ3RoQ2hhbmdlKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5kZWxldGUtY29udGVudC5maWxlJywgZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0XHRfb25EZWxldGVDbGlja2VkKGV2ZW50LCB1cmxzLmZpbGUsICR0YWJsZSk7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmRlbGV0ZS1jb250ZW50LmxpbmsnLCBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0XHRcdF9vbkRlbGV0ZUNsaWNrZWQoZXZlbnQsIHVybHMubGluaywgJHRhYmxlKTtcblx0XHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pOyJdfQ==
