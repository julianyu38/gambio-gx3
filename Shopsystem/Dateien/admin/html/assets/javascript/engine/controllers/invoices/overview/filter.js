'use strict';

/* --------------------------------------------------------------
 filter.js 2016-09-30
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles the invoices table filtering.
 *
 * ### Methods
 *
 * **Reload Filtering Options**
 *
 * ```
 * // Reload the filter options with an AJAX request (optionally provide a second parameter for the AJAX URL).
 * $('.table-main').invoices_overview_filter('reload');
 * ```
 */
gx.controllers.module('filter', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Enter Key Code
  *
  * @type {Number}
  */

	var ENTER_KEY_CODE = 13;

	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	var $this = $(this);

	/**
  * Filter Row Selector
  *
  * @type {jQuery}
  */
	var $filter = $this.find('tr.filter');

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = { bindings: {} };

	// Dynamically define the filter row data-bindings. 
	$filter.find('th').each(function () {
		var columnName = $(this).data('columnName');

		if (columnName === 'checkbox' || columnName === 'actions') {
			return true;
		}

		module.bindings[columnName] = $(this).find('input, select').first();
	});

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Reload filter options with an Ajax request.
  *
  * This function implements the $('.datatable').invoices_overview_filter('reload') which will reload the filtering
  * "multi_select" instances will new options. It must be used after some table data are changed and the filtering
  * options need to be updated.
  *
  * @param {String} url Optional, the URL to be used for fetching the options. Do not add the "pageToken"
  * parameter to URL, it will be appended in this method.
  */
	function _reload(url) {
		url = url || jse.core.config.get('appUrl') + '/admin/admin.php?do=InvoicesOverviewAjax/FilterOptions';
		var data = { pageToken: jse.core.config.get('pageToken') };

		$.getJSON(url, data).done(function (response) {
			for (var column in response) {
				var $select = $filter.find('.SumoSelect > select.' + column);
				var currentValueBackup = $select.val(); // Will try to set it back if it still exists. 

				if (!$select.length) {
					return; // The select element was not found.
				}

				$select.empty();

				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = response[column][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var option = _step.value;

						$select.append(new Option(option.text, option.value));
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				if (currentValueBackup !== null) {
					$select.val(currentValueBackup);
				}

				$select.multi_select('refresh');
			}
		});
	}

	/**
  * Add public "invoices_overview_filter" method to jQuery in order.
  */
	function _addPublicMethod() {
		if ($.fn.invoices_overview_filter) {
			return;
		}

		$.fn.extend({
			invoices_overview_filter: function invoices_overview_filter(action) {
				for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
					args[_key - 1] = arguments[_key];
				}

				$.each(this, function (instance) {
					switch (action) {
						case 'reload':
							_reload.apply(instance, args);
							break;
					}
				});
			}
		});
	}

	/**
  * On Filter Button Click
  *
  * Apply the provided filters and update the table rows.
  */
	function _onApplyFiltersClick() {
		// Prepare the object with the final filtering data.
		var filter = {};

		$filter.find('th').each(function () {
			var columnName = $(this).data('columnName');

			if (columnName === 'checkbox' || columnName === 'actions') {
				return true;
			}

			var value = module.bindings[columnName].get();

			if (value) {
				filter[columnName] = value;
				$this.DataTable().column(columnName + ':name').search(value);
			} else {
				$this.DataTable().column(columnName + ':name').search('');
			}
		});

		$this.trigger('invoices_overview_filter:change', [filter]);
		$this.DataTable().draw();
	}

	/**
  * On Reset Button Click
  *
  * Reset the filter form and reload the table data without filtering.
  */
	function _onResetFiltersClick() {
		// Remove values from the input boxes.
		$filter.find('input, select').not('.length').val('');
		$filter.find('select').not('.length').multi_select('refresh');

		// Reset the filtering values.
		$this.DataTable().columns().search('').draw();

		// Trigger Event
		$this.trigger('invoices_overview_filter:change', [{}]);
	}

	/**
  * Apply the filters when the user presses the Enter key.
  *
  * @param {jQuery.Event} event
  */
	function _onInputTextKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$filter.find('.apply-filters').trigger('click');
		}
	}

	/**
  * Parse the initial filtering parameters and apply them to the table.
  */
	function _parseFilteringParameters() {
		var _$$deparam = $.deparam(window.location.search.slice(1)),
		    filter = _$$deparam.filter;

		for (var name in filter) {
			var value = decodeURIComponent(filter[name]);

			if (module.bindings[name]) {
				module.bindings[name].set(value);
			}
		}
	}

	/**
  * Normalize array filtering values.
  *
  * By default datatables will concatenate array search values into a string separated with "," commas. This
  * is not acceptable though because some filtering elements may contain values with comma and thus the array
  * cannot be parsed from backend. This method will reset those cases back to arrays for a clearer transaction
  * with the backend.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {DataTables.Settings} settings DataTables settings object.
  * @param {Object} data Data that will be sent to the server in an object form.
  */
	function _normalizeArrayValues(event, settings, data) {
		var filter = {};

		for (var name in module.bindings) {
			var value = module.bindings[name].get();

			if (value && value.constructor === Array) {
				filter[name] = value;
			}
		}

		for (var entry in filter) {
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = data.columns[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var column = _step2.value;

					if (entry === column.name && filter[entry].constructor === Array) {
						column.search.value = filter[entry];
						break;
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Add public module method. 
		_addPublicMethod();

		// Parse filtering GET parameters. 
		_parseFilteringParameters();

		// Bind event handlers.
		$filter.on('keyup', 'input:text', _onInputTextKeyUp).on('click', '.apply-filters', _onApplyFiltersClick).on('click', '.reset-filters', _onResetFiltersClick);

		$this.on('preXhr.dt', _normalizeArrayValues);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL292ZXJ2aWV3L2ZpbHRlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIkVOVEVSX0tFWV9DT0RFIiwiJHRoaXMiLCIkIiwiJGZpbHRlciIsImZpbmQiLCJiaW5kaW5ncyIsImVhY2giLCJjb2x1bW5OYW1lIiwiZmlyc3QiLCJfcmVsb2FkIiwidXJsIiwiY29yZSIsImNvbmZpZyIsImdldCIsInBhZ2VUb2tlbiIsImdldEpTT04iLCJkb25lIiwicmVzcG9uc2UiLCJjb2x1bW4iLCIkc2VsZWN0IiwiY3VycmVudFZhbHVlQmFja3VwIiwidmFsIiwibGVuZ3RoIiwiZW1wdHkiLCJvcHRpb24iLCJhcHBlbmQiLCJPcHRpb24iLCJ0ZXh0IiwidmFsdWUiLCJtdWx0aV9zZWxlY3QiLCJfYWRkUHVibGljTWV0aG9kIiwiZm4iLCJpbnZvaWNlc19vdmVydmlld19maWx0ZXIiLCJleHRlbmQiLCJhY3Rpb24iLCJhcmdzIiwiaW5zdGFuY2UiLCJhcHBseSIsIl9vbkFwcGx5RmlsdGVyc0NsaWNrIiwiZmlsdGVyIiwiRGF0YVRhYmxlIiwic2VhcmNoIiwidHJpZ2dlciIsImRyYXciLCJfb25SZXNldEZpbHRlcnNDbGljayIsIm5vdCIsImNvbHVtbnMiLCJfb25JbnB1dFRleHRLZXlVcCIsImV2ZW50Iiwid2hpY2giLCJfcGFyc2VGaWx0ZXJpbmdQYXJhbWV0ZXJzIiwiZGVwYXJhbSIsIndpbmRvdyIsImxvY2F0aW9uIiwic2xpY2UiLCJuYW1lIiwiZGVjb2RlVVJJQ29tcG9uZW50Iiwic2V0IiwiX25vcm1hbGl6ZUFycmF5VmFsdWVzIiwic2V0dGluZ3MiLCJjb25zdHJ1Y3RvciIsIkFycmF5IiwiZW50cnkiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7O0FBWUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFFBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLGtEQUhELEVBT0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsaUJBQWlCLEVBQXZCOztBQUVBOzs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVVGLE1BQU1HLElBQU4sQ0FBVyxXQUFYLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1SLFNBQVMsRUFBQ1MsVUFBVSxFQUFYLEVBQWY7O0FBRUE7QUFDQUYsU0FBUUMsSUFBUixDQUFhLElBQWIsRUFBbUJFLElBQW5CLENBQXdCLFlBQVc7QUFDbEMsTUFBTUMsYUFBYUwsRUFBRSxJQUFGLEVBQVFILElBQVIsQ0FBYSxZQUFiLENBQW5COztBQUVBLE1BQUlRLGVBQWUsVUFBZixJQUE2QkEsZUFBZSxTQUFoRCxFQUEyRDtBQUMxRCxVQUFPLElBQVA7QUFDQTs7QUFFRFgsU0FBT1MsUUFBUCxDQUFnQkUsVUFBaEIsSUFBOEJMLEVBQUUsSUFBRixFQUFRRSxJQUFSLENBQWEsZUFBYixFQUE4QkksS0FBOUIsRUFBOUI7QUFDQSxFQVJEOztBQVVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQVVBLFVBQVNDLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQ3JCQSxRQUFNQSxPQUFPYixJQUFJYyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHdEQUE3QztBQUNBLE1BQU1kLE9BQU8sRUFBQ2UsV0FBV2pCLElBQUljLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FBWixFQUFiOztBQUVBWCxJQUFFYSxPQUFGLENBQVVMLEdBQVYsRUFBZVgsSUFBZixFQUFxQmlCLElBQXJCLENBQTBCLFVBQUNDLFFBQUQsRUFBYztBQUN2QyxRQUFLLElBQUlDLE1BQVQsSUFBbUJELFFBQW5CLEVBQTZCO0FBQzVCLFFBQU1FLFVBQVVoQixRQUFRQyxJQUFSLENBQWEsMEJBQTBCYyxNQUF2QyxDQUFoQjtBQUNBLFFBQU1FLHFCQUFxQkQsUUFBUUUsR0FBUixFQUEzQixDQUY0QixDQUVjOztBQUUxQyxRQUFJLENBQUNGLFFBQVFHLE1BQWIsRUFBcUI7QUFDcEIsWUFEb0IsQ0FDWjtBQUNSOztBQUVESCxZQUFRSSxLQUFSOztBQVI0QjtBQUFBO0FBQUE7O0FBQUE7QUFVNUIsMEJBQW1CTixTQUFTQyxNQUFULENBQW5CLDhIQUFxQztBQUFBLFVBQTVCTSxNQUE0Qjs7QUFDcENMLGNBQVFNLE1BQVIsQ0FBZSxJQUFJQyxNQUFKLENBQVdGLE9BQU9HLElBQWxCLEVBQXdCSCxPQUFPSSxLQUEvQixDQUFmO0FBQ0E7QUFaMkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFjNUIsUUFBSVIsdUJBQXVCLElBQTNCLEVBQWlDO0FBQ2hDRCxhQUFRRSxHQUFSLENBQVlELGtCQUFaO0FBQ0E7O0FBRURELFlBQVFVLFlBQVIsQ0FBcUIsU0FBckI7QUFDQTtBQUNELEdBckJEO0FBc0JBOztBQUVEOzs7QUFHQSxVQUFTQyxnQkFBVCxHQUE0QjtBQUMzQixNQUFJNUIsRUFBRTZCLEVBQUYsQ0FBS0Msd0JBQVQsRUFBbUM7QUFDbEM7QUFDQTs7QUFFRDlCLElBQUU2QixFQUFGLENBQUtFLE1BQUwsQ0FBWTtBQUNYRCwyQkFEVyxvQ0FDY0UsTUFEZCxFQUMrQjtBQUFBLHNDQUFOQyxJQUFNO0FBQU5BLFNBQU07QUFBQTs7QUFDekNqQyxNQUFFSSxJQUFGLENBQU8sSUFBUCxFQUFhLFVBQUM4QixRQUFELEVBQWM7QUFDMUIsYUFBUUYsTUFBUjtBQUNDLFdBQUssUUFBTDtBQUNDekIsZUFBUTRCLEtBQVIsQ0FBY0QsUUFBZCxFQUF3QkQsSUFBeEI7QUFDQTtBQUhGO0FBS0EsS0FORDtBQU9BO0FBVFUsR0FBWjtBQVdBOztBQUVEOzs7OztBQUtBLFVBQVNHLG9CQUFULEdBQWdDO0FBQy9CO0FBQ0EsTUFBTUMsU0FBUyxFQUFmOztBQUVBcEMsVUFBUUMsSUFBUixDQUFhLElBQWIsRUFBbUJFLElBQW5CLENBQXdCLFlBQVc7QUFDbEMsT0FBTUMsYUFBYUwsRUFBRSxJQUFGLEVBQVFILElBQVIsQ0FBYSxZQUFiLENBQW5COztBQUVBLE9BQUlRLGVBQWUsVUFBZixJQUE2QkEsZUFBZSxTQUFoRCxFQUEyRDtBQUMxRCxXQUFPLElBQVA7QUFDQTs7QUFFRCxPQUFJcUIsUUFBUWhDLE9BQU9TLFFBQVAsQ0FBZ0JFLFVBQWhCLEVBQTRCTSxHQUE1QixFQUFaOztBQUVBLE9BQUllLEtBQUosRUFBVztBQUNWVyxXQUFPaEMsVUFBUCxJQUFxQnFCLEtBQXJCO0FBQ0EzQixVQUFNdUMsU0FBTixHQUFrQnRCLE1BQWxCLENBQTRCWCxVQUE1QixZQUErQ2tDLE1BQS9DLENBQXNEYixLQUF0RDtBQUNBLElBSEQsTUFHTztBQUNOM0IsVUFBTXVDLFNBQU4sR0FBa0J0QixNQUFsQixDQUE0QlgsVUFBNUIsWUFBK0NrQyxNQUEvQyxDQUFzRCxFQUF0RDtBQUNBO0FBQ0QsR0FmRDs7QUFpQkF4QyxRQUFNeUMsT0FBTixDQUFjLGlDQUFkLEVBQWlELENBQUNILE1BQUQsQ0FBakQ7QUFDQXRDLFFBQU11QyxTQUFOLEdBQWtCRyxJQUFsQjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNDLG9CQUFULEdBQWdDO0FBQy9CO0FBQ0F6QyxVQUFRQyxJQUFSLENBQWEsZUFBYixFQUE4QnlDLEdBQTlCLENBQWtDLFNBQWxDLEVBQTZDeEIsR0FBN0MsQ0FBaUQsRUFBakQ7QUFDQWxCLFVBQVFDLElBQVIsQ0FBYSxRQUFiLEVBQXVCeUMsR0FBdkIsQ0FBMkIsU0FBM0IsRUFBc0NoQixZQUF0QyxDQUFtRCxTQUFuRDs7QUFFQTtBQUNBNUIsUUFBTXVDLFNBQU4sR0FBa0JNLE9BQWxCLEdBQTRCTCxNQUE1QixDQUFtQyxFQUFuQyxFQUF1Q0UsSUFBdkM7O0FBRUE7QUFDQTFDLFFBQU15QyxPQUFOLENBQWMsaUNBQWQsRUFBaUQsQ0FBQyxFQUFELENBQWpEO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0ssaUJBQVQsQ0FBMkJDLEtBQTNCLEVBQWtDO0FBQ2pDLE1BQUlBLE1BQU1DLEtBQU4sS0FBZ0JqRCxjQUFwQixFQUFvQztBQUNuQ0csV0FBUUMsSUFBUixDQUFhLGdCQUFiLEVBQStCc0MsT0FBL0IsQ0FBdUMsT0FBdkM7QUFDQTtBQUNEOztBQUVEOzs7QUFHQSxVQUFTUSx5QkFBVCxHQUFxQztBQUFBLG1CQUNuQmhELEVBQUVpRCxPQUFGLENBQVVDLE9BQU9DLFFBQVAsQ0FBZ0JaLE1BQWhCLENBQXVCYSxLQUF2QixDQUE2QixDQUE3QixDQUFWLENBRG1CO0FBQUEsTUFDN0JmLE1BRDZCLGNBQzdCQSxNQUQ2Qjs7QUFHcEMsT0FBSyxJQUFJZ0IsSUFBVCxJQUFpQmhCLE1BQWpCLEVBQXlCO0FBQ3hCLE9BQU1YLFFBQVE0QixtQkFBbUJqQixPQUFPZ0IsSUFBUCxDQUFuQixDQUFkOztBQUVBLE9BQUkzRCxPQUFPUyxRQUFQLENBQWdCa0QsSUFBaEIsQ0FBSixFQUEyQjtBQUMxQjNELFdBQU9TLFFBQVAsQ0FBZ0JrRCxJQUFoQixFQUFzQkUsR0FBdEIsQ0FBMEI3QixLQUExQjtBQUNBO0FBQ0Q7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7O0FBWUEsVUFBUzhCLHFCQUFULENBQStCVixLQUEvQixFQUFzQ1csUUFBdEMsRUFBZ0Q1RCxJQUFoRCxFQUFzRDtBQUNyRCxNQUFNd0MsU0FBUyxFQUFmOztBQUVBLE9BQUssSUFBSWdCLElBQVQsSUFBaUIzRCxPQUFPUyxRQUF4QixFQUFrQztBQUNqQyxPQUFNdUIsUUFBUWhDLE9BQU9TLFFBQVAsQ0FBZ0JrRCxJQUFoQixFQUFzQjFDLEdBQXRCLEVBQWQ7O0FBRUEsT0FBSWUsU0FBU0EsTUFBTWdDLFdBQU4sS0FBc0JDLEtBQW5DLEVBQTBDO0FBQ3pDdEIsV0FBT2dCLElBQVAsSUFBZTNCLEtBQWY7QUFDQTtBQUNEOztBQUVELE9BQUssSUFBSWtDLEtBQVQsSUFBa0J2QixNQUFsQixFQUEwQjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUN6QiwwQkFBbUJ4QyxLQUFLK0MsT0FBeEIsbUlBQWlDO0FBQUEsU0FBeEI1QixNQUF3Qjs7QUFDaEMsU0FBSTRDLFVBQVU1QyxPQUFPcUMsSUFBakIsSUFBeUJoQixPQUFPdUIsS0FBUCxFQUFjRixXQUFkLEtBQThCQyxLQUEzRCxFQUFrRTtBQUNqRTNDLGFBQU91QixNQUFQLENBQWNiLEtBQWQsR0FBc0JXLE9BQU91QixLQUFQLENBQXRCO0FBQ0E7QUFDQTtBQUNEO0FBTndCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPekI7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O0FBRUFsRSxRQUFPbUUsSUFBUCxHQUFjLFVBQVMvQyxJQUFULEVBQWU7QUFDNUI7QUFDQWM7O0FBRUE7QUFDQW9COztBQUVBO0FBQ0EvQyxVQUNFNkQsRUFERixDQUNLLE9BREwsRUFDYyxZQURkLEVBQzRCakIsaUJBRDVCLEVBRUVpQixFQUZGLENBRUssT0FGTCxFQUVjLGdCQUZkLEVBRWdDMUIsb0JBRmhDLEVBR0UwQixFQUhGLENBR0ssT0FITCxFQUdjLGdCQUhkLEVBR2dDcEIsb0JBSGhDOztBQUtBM0MsUUFBTStELEVBQU4sQ0FBUyxXQUFULEVBQXNCTixxQkFBdEI7O0FBRUExQztBQUNBLEVBaEJEOztBQWtCQSxRQUFPcEIsTUFBUDtBQUNBLENBdFBGIiwiZmlsZSI6Imludm9pY2VzL292ZXJ2aWV3L2ZpbHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZmlsdGVyLmpzIDIwMTYtMDktMzBcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIEhhbmRsZXMgdGhlIGludm9pY2VzIHRhYmxlIGZpbHRlcmluZy5cbiAqXG4gKiAjIyMgTWV0aG9kc1xuICpcbiAqICoqUmVsb2FkIEZpbHRlcmluZyBPcHRpb25zKipcbiAqXG4gKiBgYGBcbiAqIC8vIFJlbG9hZCB0aGUgZmlsdGVyIG9wdGlvbnMgd2l0aCBhbiBBSkFYIHJlcXVlc3QgKG9wdGlvbmFsbHkgcHJvdmlkZSBhIHNlY29uZCBwYXJhbWV0ZXIgZm9yIHRoZSBBSkFYIFVSTCkuXG4gKiAkKCcudGFibGUtbWFpbicpLmludm9pY2VzX292ZXJ2aWV3X2ZpbHRlcigncmVsb2FkJyk7XG4gKiBgYGBcbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnZmlsdGVyJyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LWRlcGFyYW0vanF1ZXJ5LWRlcGFyYW0ubWluLmpzYFxuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBFbnRlciBLZXkgQ29kZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge051bWJlcn1cblx0XHQgKi9cblx0XHRjb25zdCBFTlRFUl9LRVlfQ09ERSA9IDEzO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmlsdGVyIFJvdyBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkZmlsdGVyID0gJHRoaXMuZmluZCgndHIuZmlsdGVyJyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHtiaW5kaW5nczoge319O1xuXHRcdFxuXHRcdC8vIER5bmFtaWNhbGx5IGRlZmluZSB0aGUgZmlsdGVyIHJvdyBkYXRhLWJpbmRpbmdzLiBcblx0XHQkZmlsdGVyLmZpbmQoJ3RoJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdGNvbnN0IGNvbHVtbk5hbWUgPSAkKHRoaXMpLmRhdGEoJ2NvbHVtbk5hbWUnKTtcblx0XHRcdFxuXHRcdFx0aWYgKGNvbHVtbk5hbWUgPT09ICdjaGVja2JveCcgfHwgY29sdW1uTmFtZSA9PT0gJ2FjdGlvbnMnKSB7XG5cdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRtb2R1bGUuYmluZGluZ3NbY29sdW1uTmFtZV0gPSAkKHRoaXMpLmZpbmQoJ2lucHV0LCBzZWxlY3QnKS5maXJzdCgpO1xuXHRcdH0pO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlbG9hZCBmaWx0ZXIgb3B0aW9ucyB3aXRoIGFuIEFqYXggcmVxdWVzdC5cblx0XHQgKlxuXHRcdCAqIFRoaXMgZnVuY3Rpb24gaW1wbGVtZW50cyB0aGUgJCgnLmRhdGF0YWJsZScpLmludm9pY2VzX292ZXJ2aWV3X2ZpbHRlcigncmVsb2FkJykgd2hpY2ggd2lsbCByZWxvYWQgdGhlIGZpbHRlcmluZ1xuXHRcdCAqIFwibXVsdGlfc2VsZWN0XCIgaW5zdGFuY2VzIHdpbGwgbmV3IG9wdGlvbnMuIEl0IG11c3QgYmUgdXNlZCBhZnRlciBzb21lIHRhYmxlIGRhdGEgYXJlIGNoYW5nZWQgYW5kIHRoZSBmaWx0ZXJpbmdcblx0XHQgKiBvcHRpb25zIG5lZWQgdG8gYmUgdXBkYXRlZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSB1cmwgT3B0aW9uYWwsIHRoZSBVUkwgdG8gYmUgdXNlZCBmb3IgZmV0Y2hpbmcgdGhlIG9wdGlvbnMuIERvIG5vdCBhZGQgdGhlIFwicGFnZVRva2VuXCJcblx0XHQgKiBwYXJhbWV0ZXIgdG8gVVJMLCBpdCB3aWxsIGJlIGFwcGVuZGVkIGluIHRoaXMgbWV0aG9kLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9yZWxvYWQodXJsKSB7XG5cdFx0XHR1cmwgPSB1cmwgfHwganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1JbnZvaWNlc092ZXJ2aWV3QWpheC9GaWx0ZXJPcHRpb25zJztcblx0XHRcdGNvbnN0IGRhdGEgPSB7cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKX07XG5cdFx0XHRcblx0XHRcdCQuZ2V0SlNPTih1cmwsIGRhdGEpLmRvbmUoKHJlc3BvbnNlKSA9PiB7XG5cdFx0XHRcdGZvciAobGV0IGNvbHVtbiBpbiByZXNwb25zZSkge1xuXHRcdFx0XHRcdGNvbnN0ICRzZWxlY3QgPSAkZmlsdGVyLmZpbmQoJy5TdW1vU2VsZWN0ID4gc2VsZWN0LicgKyBjb2x1bW4pO1xuXHRcdFx0XHRcdGNvbnN0IGN1cnJlbnRWYWx1ZUJhY2t1cCA9ICRzZWxlY3QudmFsKCk7IC8vIFdpbGwgdHJ5IHRvIHNldCBpdCBiYWNrIGlmIGl0IHN0aWxsIGV4aXN0cy4gXG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKCEkc2VsZWN0Lmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0cmV0dXJuOyAvLyBUaGUgc2VsZWN0IGVsZW1lbnQgd2FzIG5vdCBmb3VuZC5cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHNlbGVjdC5lbXB0eSgpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGZvciAobGV0IG9wdGlvbiBvZiByZXNwb25zZVtjb2x1bW5dKSB7XG5cdFx0XHRcdFx0XHQkc2VsZWN0LmFwcGVuZChuZXcgT3B0aW9uKG9wdGlvbi50ZXh0LCBvcHRpb24udmFsdWUpKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKGN1cnJlbnRWYWx1ZUJhY2t1cCAhPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0JHNlbGVjdC52YWwoY3VycmVudFZhbHVlQmFja3VwKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHNlbGVjdC5tdWx0aV9zZWxlY3QoJ3JlZnJlc2gnKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFkZCBwdWJsaWMgXCJpbnZvaWNlc19vdmVydmlld19maWx0ZXJcIiBtZXRob2QgdG8galF1ZXJ5IGluIG9yZGVyLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9hZGRQdWJsaWNNZXRob2QoKSB7XG5cdFx0XHRpZiAoJC5mbi5pbnZvaWNlc19vdmVydmlld19maWx0ZXIpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkLmZuLmV4dGVuZCh7XG5cdFx0XHRcdGludm9pY2VzX292ZXJ2aWV3X2ZpbHRlcihhY3Rpb24sIC4uLmFyZ3MpIHtcblx0XHRcdFx0XHQkLmVhY2godGhpcywgKGluc3RhbmNlKSA9PiB7XG5cdFx0XHRcdFx0XHRzd2l0Y2ggKGFjdGlvbikge1xuXHRcdFx0XHRcdFx0XHRjYXNlICdyZWxvYWQnOlxuXHRcdFx0XHRcdFx0XHRcdF9yZWxvYWQuYXBwbHkoaW5zdGFuY2UsIGFyZ3MpO1xuXHRcdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogT24gRmlsdGVyIEJ1dHRvbiBDbGlja1xuXHRcdCAqXG5cdFx0ICogQXBwbHkgdGhlIHByb3ZpZGVkIGZpbHRlcnMgYW5kIHVwZGF0ZSB0aGUgdGFibGUgcm93cy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25BcHBseUZpbHRlcnNDbGljaygpIHtcblx0XHRcdC8vIFByZXBhcmUgdGhlIG9iamVjdCB3aXRoIHRoZSBmaW5hbCBmaWx0ZXJpbmcgZGF0YS5cblx0XHRcdGNvbnN0IGZpbHRlciA9IHt9O1xuXHRcdFx0XG5cdFx0XHQkZmlsdGVyLmZpbmQoJ3RoJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0Y29uc3QgY29sdW1uTmFtZSA9ICQodGhpcykuZGF0YSgnY29sdW1uTmFtZScpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKGNvbHVtbk5hbWUgPT09ICdjaGVja2JveCcgfHwgY29sdW1uTmFtZSA9PT0gJ2FjdGlvbnMnKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGxldCB2YWx1ZSA9IG1vZHVsZS5iaW5kaW5nc1tjb2x1bW5OYW1lXS5nZXQoKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICh2YWx1ZSkge1xuXHRcdFx0XHRcdGZpbHRlcltjb2x1bW5OYW1lXSA9IHZhbHVlO1xuXHRcdFx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmNvbHVtbihgJHtjb2x1bW5OYW1lfTpuYW1lYCkuc2VhcmNoKHZhbHVlKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oYCR7Y29sdW1uTmFtZX06bmFtZWApLnNlYXJjaCgnJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy50cmlnZ2VyKCdpbnZvaWNlc19vdmVydmlld19maWx0ZXI6Y2hhbmdlJywgW2ZpbHRlcl0pO1xuXHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuZHJhdygpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBPbiBSZXNldCBCdXR0b24gQ2xpY2tcblx0XHQgKlxuXHRcdCAqIFJlc2V0IHRoZSBmaWx0ZXIgZm9ybSBhbmQgcmVsb2FkIHRoZSB0YWJsZSBkYXRhIHdpdGhvdXQgZmlsdGVyaW5nLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblJlc2V0RmlsdGVyc0NsaWNrKCkge1xuXHRcdFx0Ly8gUmVtb3ZlIHZhbHVlcyBmcm9tIHRoZSBpbnB1dCBib3hlcy5cblx0XHRcdCRmaWx0ZXIuZmluZCgnaW5wdXQsIHNlbGVjdCcpLm5vdCgnLmxlbmd0aCcpLnZhbCgnJyk7XG5cdFx0XHQkZmlsdGVyLmZpbmQoJ3NlbGVjdCcpLm5vdCgnLmxlbmd0aCcpLm11bHRpX3NlbGVjdCgncmVmcmVzaCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZXNldCB0aGUgZmlsdGVyaW5nIHZhbHVlcy5cblx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmNvbHVtbnMoKS5zZWFyY2goJycpLmRyYXcoKTtcblx0XHRcdFxuXHRcdFx0Ly8gVHJpZ2dlciBFdmVudFxuXHRcdFx0JHRoaXMudHJpZ2dlcignaW52b2ljZXNfb3ZlcnZpZXdfZmlsdGVyOmNoYW5nZScsIFt7fV0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBBcHBseSB0aGUgZmlsdGVycyB3aGVuIHRoZSB1c2VyIHByZXNzZXMgdGhlIEVudGVyIGtleS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbklucHV0VGV4dEtleVVwKGV2ZW50KSB7XG5cdFx0XHRpZiAoZXZlbnQud2hpY2ggPT09IEVOVEVSX0tFWV9DT0RFKSB7XG5cdFx0XHRcdCRmaWx0ZXIuZmluZCgnLmFwcGx5LWZpbHRlcnMnKS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBQYXJzZSB0aGUgaW5pdGlhbCBmaWx0ZXJpbmcgcGFyYW1ldGVycyBhbmQgYXBwbHkgdGhlbSB0byB0aGUgdGFibGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3BhcnNlRmlsdGVyaW5nUGFyYW1ldGVycygpIHtcblx0XHRcdGNvbnN0IHtmaWx0ZXJ9ID0gJC5kZXBhcmFtKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSkpO1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBuYW1lIGluIGZpbHRlcikge1xuXHRcdFx0XHRjb25zdCB2YWx1ZSA9IGRlY29kZVVSSUNvbXBvbmVudChmaWx0ZXJbbmFtZV0pO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKG1vZHVsZS5iaW5kaW5nc1tuYW1lXSkge1xuXHRcdFx0XHRcdG1vZHVsZS5iaW5kaW5nc1tuYW1lXS5zZXQodmFsdWUpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE5vcm1hbGl6ZSBhcnJheSBmaWx0ZXJpbmcgdmFsdWVzLlxuXHRcdCAqXG5cdFx0ICogQnkgZGVmYXVsdCBkYXRhdGFibGVzIHdpbGwgY29uY2F0ZW5hdGUgYXJyYXkgc2VhcmNoIHZhbHVlcyBpbnRvIGEgc3RyaW5nIHNlcGFyYXRlZCB3aXRoIFwiLFwiIGNvbW1hcy4gVGhpc1xuXHRcdCAqIGlzIG5vdCBhY2NlcHRhYmxlIHRob3VnaCBiZWNhdXNlIHNvbWUgZmlsdGVyaW5nIGVsZW1lbnRzIG1heSBjb250YWluIHZhbHVlcyB3aXRoIGNvbW1hIGFuZCB0aHVzIHRoZSBhcnJheVxuXHRcdCAqIGNhbm5vdCBiZSBwYXJzZWQgZnJvbSBiYWNrZW5kLiBUaGlzIG1ldGhvZCB3aWxsIHJlc2V0IHRob3NlIGNhc2VzIGJhY2sgdG8gYXJyYXlzIGZvciBhIGNsZWFyZXIgdHJhbnNhY3Rpb25cblx0XHQgKiB3aXRoIHRoZSBiYWNrZW5kLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXG5cdFx0ICogQHBhcmFtIHtEYXRhVGFibGVzLlNldHRpbmdzfSBzZXR0aW5ncyBEYXRhVGFibGVzIHNldHRpbmdzIG9iamVjdC5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YSBEYXRhIHRoYXQgd2lsbCBiZSBzZW50IHRvIHRoZSBzZXJ2ZXIgaW4gYW4gb2JqZWN0IGZvcm0uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX25vcm1hbGl6ZUFycmF5VmFsdWVzKGV2ZW50LCBzZXR0aW5ncywgZGF0YSkge1xuXHRcdFx0Y29uc3QgZmlsdGVyID0ge307XG5cdFx0XHRcblx0XHRcdGZvciAobGV0IG5hbWUgaW4gbW9kdWxlLmJpbmRpbmdzKSB7XG5cdFx0XHRcdGNvbnN0IHZhbHVlID0gbW9kdWxlLmJpbmRpbmdzW25hbWVdLmdldCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKHZhbHVlICYmIHZhbHVlLmNvbnN0cnVjdG9yID09PSBBcnJheSkge1xuXHRcdFx0XHRcdGZpbHRlcltuYW1lXSA9IHZhbHVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGZvciAobGV0IGVudHJ5IGluIGZpbHRlcikge1xuXHRcdFx0XHRmb3IgKGxldCBjb2x1bW4gb2YgZGF0YS5jb2x1bW5zKSB7XG5cdFx0XHRcdFx0aWYgKGVudHJ5ID09PSBjb2x1bW4ubmFtZSAmJiBmaWx0ZXJbZW50cnldLmNvbnN0cnVjdG9yID09PSBBcnJheSkge1xuXHRcdFx0XHRcdFx0Y29sdW1uLnNlYXJjaC52YWx1ZSA9IGZpbHRlcltlbnRyeV07XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdC8vIEFkZCBwdWJsaWMgbW9kdWxlIG1ldGhvZC4gXG5cdFx0XHRfYWRkUHVibGljTWV0aG9kKCk7XG5cdFx0XHRcblx0XHRcdC8vIFBhcnNlIGZpbHRlcmluZyBHRVQgcGFyYW1ldGVycy4gXG5cdFx0XHRfcGFyc2VGaWx0ZXJpbmdQYXJhbWV0ZXJzKCk7XG5cdFx0XHRcblx0XHRcdC8vIEJpbmQgZXZlbnQgaGFuZGxlcnMuXG5cdFx0XHQkZmlsdGVyXG5cdFx0XHRcdC5vbigna2V5dXAnLCAnaW5wdXQ6dGV4dCcsIF9vbklucHV0VGV4dEtleVVwKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5hcHBseS1maWx0ZXJzJywgX29uQXBwbHlGaWx0ZXJzQ2xpY2spXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLnJlc2V0LWZpbHRlcnMnLCBfb25SZXNldEZpbHRlcnNDbGljayk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdwcmVYaHIuZHQnLCBfbm9ybWFsaXplQXJyYXlWYWx1ZXMpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTsiXX0=
