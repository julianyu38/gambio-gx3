'use strict';

/* --------------------------------------------------------------
 events.js 2018-03-28
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Events
 *
 * Handles the events of the main orders table.
 */
gx.controllers.module('events', [jse.source + '/vendor/momentjs/moment.min.js', 'loading_spinner', 'modal', 'xhr'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * On Bulk Selection Change
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Boolean} propagate Whether to affect the body elements. We do not need this on "draw.dt" event.
  */
	function _onBulkSelectionChange(event) {
		var propagate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		if (propagate === false) {
			return; // Do not propagate on draw event because the body checkboxes are unchecked by default.
		}

		$this.find('tbody input:checkbox').single_checkbox('checked', $(this).prop('checked')).trigger('change');
	}

	/**
  * On Table Row Click
  *
  * When a row is clicked then the row-checkbox must be toggled.
  *
  * @param {jQuery.Event} event
  */
	function _onTableRowClick(event) {
		if (!$(event.target).is('td')) {
			return;
		}

		$(this).find('input:checkbox').prop('checked', !$(this).find('input:checkbox').prop('checked')).trigger('change');
	}

	/**
  * On Table Row Checkbox Change
  *
  * Adjust the bulk actions state whenever there are changes in the table checkboxes.
  */
	function _onTableRowCheckboxChange() {
		if ($this.find('input:checkbox:checked').length > 0) {
			$this.parents('.orders').find('.bulk-action > button').removeClass('disabled');
		} else {
			$this.parents('.orders').find('.bulk-action > button').addClass('disabled');
		}
	}

	/**
  * On Cancel Order Click
  *
  * @param {jQuery.Event} event
  */
	function _onCancelOrderClick(event) {
		event.preventDefault();

		var selectedOrders = _getSelectedOrders($(this));

		// Show the order delete modal.
		$('.cancel.modal .selected-orders').text(selectedOrders.join(', '));
		$('.cancel.modal').modal('show');
	}

	/**
  * On Delete Order Click
  *
  * Display the delete-modal.
  *
  * @param {jQuery.Event} event
  */
	function _onDeleteOrderClick(event) {
		event.preventDefault();

		var selectedOrders = _getSelectedOrders($(this));

		// Show the order delete modal.
		$('.delete.modal .selected-orders').text(selectedOrders.join(', '));
		$('.delete.modal').modal('show');
	}

	/**
  * On Send Order Click.
  *
  * Sends the email order confirmations.
  *
  * @param {jQuery.Event} event jQuery event object.
  */
	function _onBulkEmailOrderClick(event) {
		var $modal = $('.bulk-email-order.modal');
		var $mailList = $modal.find('.email-list');

		var generateMailRowMarkup = function generateMailRowMarkup(data) {
			var $row = $('<div/>', { class: 'form-group email-list-item' });
			var $idColumn = $('<div/>', { class: 'col-sm-3' });
			var $emailColumn = $('<div/>', { class: 'col-sm-9' });

			var $idLabel = $('<label/>', {
				class: 'control-label id-label force-text-color-black force-text-normal-weight',
				text: data.id
			});

			var $emailInput = $('<input/>', {
				class: 'form-control email-input',
				type: 'text',
				value: data.customerEmail
			});

			$idLabel.appendTo($idColumn);
			$emailInput.appendTo($emailColumn);

			$row.append([$idColumn, $emailColumn]);
			$row.data('order', data);

			return $row;
		};

		var selectedOrders = [];

		event.preventDefault();

		$this.find('tbody input:checkbox:checked').each(function () {
			var rowData = $(this).parents('tr').data();
			selectedOrders.push(rowData);
		});

		if (selectedOrders.length) {
			$mailList.empty();
			selectedOrders.forEach(function (order) {
				return $mailList.append(generateMailRowMarkup(order));
			});
			$modal.modal('show');
		}
	}

	/**
  * On Send Invoice Click.
  *
  * Sends the email invoice.
  *
  * @param {jQuery.Event} event Fired event.
  */
	function _onBulkEmailInvoiceClick(event) {
		var $modal = $('.bulk-email-invoice.modal');
		var $mailList = $modal.find('.email-list');

		var generateMailRowMarkup = function generateMailRowMarkup(data) {
			var $row = $('<div/>', { class: 'form-group email-list-item' });
			var $idColumn = $('<div/>', { class: 'col-sm-3' });
			var $invoiceColumn = $('<div/>', { class: 'col-sm-3' });
			var $emailColumn = $('<div/>', { class: 'col-sm-6' });

			var $latestInvoiceIdInput = $('<input/>', {
				class: 'form-control latest-invoice-id',
				type: 'hidden',
				value: data.latestInvoiceId
			});

			var $idLabel = $('<label/>', {
				class: 'control-label id-label force-text-color-black force-text-normal-weight',
				text: data.id
			});

			var $invoiceLink = $('<label/>', {
				class: 'control-label id-label force-text-color-black force-text-normal-weight',
				html: data.latestInvoiceNumber ? '<a href="request_port.php?module=OrderAdmin&action=showPdf&type=invoice' + ('&invoice_number=' + data.latestInvoiceNumber + '&order_id=' + data.id + '" target="_blank">') + (data.latestInvoiceNumber + '</a>') : '-'
			});

			var $emailInput = $('<input/>', {
				class: 'form-control email-input',
				type: 'text',
				value: data.customerEmail
			});

			$idLabel.appendTo($idColumn);
			$invoiceLink.appendTo($invoiceColumn);
			$emailInput.appendTo($emailColumn);

			$row.append([$idColumn, $invoiceColumn, $emailColumn, $latestInvoiceIdInput]);
			$row.data('order', data);

			return $row;
		};

		var selectedInvoice = [];

		event.preventDefault();

		$this.find('tbody input:checkbox:checked').each(function () {
			var rowData = $(this).parents('tr').data();
			selectedInvoice.push(rowData);
		});

		if (selectedInvoice.length) {
			$mailList.empty();
			selectedInvoice.forEach(function (order) {
				return $mailList.append(generateMailRowMarkup(order));
			});
			$modal.modal('show');
		}
	}

	/**
  * Collects the IDs of the selected orders and returns them as an array.
  *
  * @param {jQuery} $target The triggering target
  *
  * @return {Number[]} array of order IDs
  */
	function _getSelectedOrders($target) {
		var selectedOrders = [];

		if ($target.parents('.bulk-action').length > 0) {
			// Fetch the selected order IDs.
			$this.find('tbody input:checkbox:checked').each(function () {
				selectedOrders.push($(this).parents('tr').data('id'));
			});
		} else {
			var rowId = $target.parents('tr').data('id');

			if (!rowId) {
				return; // No order ID was found.
			}

			selectedOrders.push(rowId);
		}

		return selectedOrders;
	}

	/**
  * On Email Invoice Click
  *
  * Display the email-invoice modal.
  */
	function _onEmailInvoiceClick() {
		var $modal = $('.email-invoice.modal');
		var rowData = $(this).parents('tr').data();
		var url = jse.core.config.get('appUrl') + '/admin/admin.php';
		var data = {
			id: rowData.id,
			do: 'OrdersModalsAjax/GetEmailInvoiceSubject',
			pageToken: jse.core.config.get('pageToken')
		};
		var invoiceNumbersHtml = '';

		$modal.find('.customer-info').text('"' + rowData.customerName + '"');
		$modal.find('.email-address').val(rowData.customerEmail);

		$modal.data('orderId', rowData.id).modal('show');

		$.ajax({ url: url, data: data, dataType: 'json' }).done(function (response) {
			$modal.attr('data-gx-widget', 'single_checkbox');

			$modal.find('.subject').val(response.subject);
			if (response.invoiceIdExists) {
				$modal.find('.invoice-num-info').addClass('hidden');
				$modal.find('.no-invoice').removeClass('hidden');
			} else {
				$modal.find('.invoice-num-info').removeClass('hidden');
				$modal.find('.no-invoice').addClass('hidden');
			}

			if (Object.keys(response.invoiceNumbers).length <= 1) {
				$modal.find('.invoice-numbers').addClass('hidden');
			} else {
				$modal.find('.invoice-numbers').removeClass('hidden');
			}

			for (var invoiceId in response.invoiceNumbers) {
				invoiceNumbersHtml += '<p><input type="checkbox" name="invoice_ids[]" value="' + invoiceId + '" checked="checked" class="invoice-numbers-checkbox" /> ' + response.invoiceNumbers[invoiceId] + '</p>';
			}

			$modal.find('.invoice-numbers-checkboxes').html(invoiceNumbersHtml);

			$modal.find('.invoice-numbers-checkbox').on('change', _onChangeEmailInvoiceCheckbox);

			gx.widgets.init($modal);
		});
	}

	/**
  * On Email Invoice Checkbox Change
  *
  * Disable send button if all invoice number checkboxes are unchecked. Otherwise enable the send button again.
  */
	function _onChangeEmailInvoiceCheckbox() {
		var $modal = $('.email-invoice.modal');

		if ($modal.find('.invoice-numbers-checkbox').length > 0) {
			if ($modal.find('.invoice-numbers-checkbox:checked').length > 0) {
				$modal.find('.send').prop('disabled', false);
			} else {
				$modal.find('.send').prop('disabled', true);
			}
		} else {
			$modal.find('.send').prop('disabled', false);
		}
	}

	/**
  * On Email Order Click
  *
  * Display the email-order modal.
  *
  * @param {jQuery.Event} event
  */
	function _onEmailOrderClick(event) {
		var $modal = $('.email-order.modal');
		var rowData = $(this).parents('tr').data();
		var dateFormat = jse.core.config.get('languageCode') === 'de' ? 'DD.MM.YY' : 'MM/DD/YY';

		$modal.find('.customer-info').text('"' + rowData.customerName + '"');
		$modal.find('.subject').val(jse.core.lang.translate('ORDER_SUBJECT', 'gm_order_menu') + rowData.id + jse.core.lang.translate('ORDER_SUBJECT_FROM', 'gm_order_menu') + moment(rowData.purchaseDate.date).format(dateFormat));
		$modal.find('.email-address').val(rowData.customerEmail);

		$modal.data('orderId', rowData.id).modal('show');
	}

	/**
  * On Change Order Status Click
  *
  * Display the change order status modal.
  *
  * @param {jQuery.Event} event
  */
	function _onChangeOrderStatusClick(event) {
		if ($(event.target).hasClass('order-status')) {
			event.stopPropagation();
		}

		var $modal = $('.status.modal');
		var rowData = $(this).parents('tr').data();
		var selectedOrders = _getSelectedOrders($(this));

		$modal.find('#status-dropdown').val(rowData ? rowData.statusId : '');

		$modal.find('#comment').val('');
		$modal.find('#notify-customer, #send-parcel-tracking-code, #send-comment').attr('checked', false).parents('.single-checkbox').removeClass('checked');

		// Show the order change status modal.
		$modal.find('.selected-orders').text(selectedOrders.join(', '));
		$modal.modal('show');
	}

	/**
  * On Add Tracking Number Click
  *
  * @param {jQuery.Event} event
  */
	function _onAddTrackingNumberClick(event) {
		var $modal = $('.add-tracking-number.modal');
		var rowData = $(event.target).parents('tr').data();

		$modal.data('orderId', rowData.id);
		$modal.modal('show');
	}

	/**
  * Opens the gm_pdf_order.php in a new tab with invoices as type $_GET argument.
  *
  * The order ids are passed as a serialized array to the oID $_GET argument.
  */
	function _onBulkDownloadInvoiceClick() {
		var orderIds = [];
		var maxAmountInvoicesBulkPdf = data.maxAmountInvoicesBulkPdf;

		$this.find('tbody input:checkbox:checked').each(function () {
			orderIds.push($(this).parents('tr').data('id'));
		});

		if (orderIds.length > maxAmountInvoicesBulkPdf) {
			var $modal = $('.bulk-error.modal');
			$modal.modal('show');

			var $invoiceMessageContainer = $modal.find('.invoices-message');
			$invoiceMessageContainer.removeClass('hidden');
			$modal.on('hide.bs.modal', function () {
				return $invoiceMessageContainer.addClass('hidden');
			});

			return;
		}

		_createBulkPdf(orderIds, 'invoice');
	}

	/**
  * Opens the gm_pdf_order.php in a new tab with packing slip as type $_GET argument.
  *
  * The order ids are passed as a serialized array to the oID $_GET argument.
  */
	function _onBulkDownloadPackingSlipClick() {
		var orderIds = [];
		var maxAmountPackingSlipsBulkPdf = data.maxAmountPackingSlipsBulkPdf;
		var $modal = void 0;
		var $packingSlipsMessageContainer = void 0;

		$this.find('tbody input:checkbox:checked').each(function () {
			orderIds.push($(this).parents('tr').data('id'));
		});

		if (orderIds.length > maxAmountPackingSlipsBulkPdf) {
			$modal = $('.bulk-error.modal');
			$modal.modal('show');
			$packingSlipsMessageContainer = $modal.find('.packing-slips-message');

			$packingSlipsMessageContainer.removeClass('hidden');

			$modal.on('hide.bs.modal', function () {
				$packingSlipsMessageContainer.addClass('hidden');
			});

			return;
		}

		_createBulkPdf(orderIds, 'packingslip');
	}

	/**
  * Creates a bulk pdf with invoices or packing slips information.
  *
  * This method will check if all the selected orders have a document and open a concatenated PDF file. If there
  * are orders that do not have any document then a modal will be shown, prompting the user to create the missing
  * documents or continue without them.
  *
  * @param {Number[]} orderIds Provide the selected order IDs.
  * @param {String} type Provide the bulk PDF type "invoice" or "packingslip".
  */
	function _createBulkPdf(orderIds, type) {
		if (type !== 'invoice' && type !== 'packingslip') {
			throw new Error('Invalid type provided: ' + type);
		}

		var url = jse.core.config.get('appUrl') + '/admin/admin.php';
		var data = {
			do: 'OrdersOverviewAjax/GetOrdersWithoutDocuments',
			pageToken: jse.core.config.get('pageToken'),
			type: type,
			orderIds: orderIds
		};

		$.getJSON(url, data).done(function (orderIdsWithoutDocument) {
			if (orderIdsWithoutDocument.exception) {
				var title = jse.core.lang.translate('error', 'messages');
				var message = jse.core.lang.translate('GET_ORDERS_WITHOUT_DOCUMENT_ERROR', 'admin_orders');
				jse.libs.modal.showMessage(title, message);
				return;
			}

			if (!orderIdsWithoutDocument.length) {
				_openBulkPdfUrl(orderIds, type); // All the selected order have documents. 
				return;
			}

			// Some orders do not have documents, display the confirmation modal.
			var $modal = $('.modal.create-missing-documents');
			$modal.find('.order-ids-list').text(orderIdsWithoutDocument.join(', '));
			$modal.data({
				orderIds: orderIds,
				orderIdsWithoutDocument: orderIdsWithoutDocument,
				type: type
			}).modal('show');
		}).fail(function (jqxhr, textStatus, errorThrown) {
			var title = jse.core.lang.translate('error', 'messages');
			var message = jse.core.lang.translate('GET_ORDERS_WITHOUT_DOCUMENT_ERROR', 'admin_orders');
			jse.libs.modal.showMessage(title, message);
		});
	}

	/**
  * Create Missing Documents Proceed Handler
  *
  * This handler will be executed whenever the user proceeds through the "create-missing-documents" modal. It will
  * be resolved even if the user does not select the checkbox "create-missing-documents".
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Number[]} orderIds The selected orders to be included in the PDF.
  * @param {String} type Whether 'invoice' or 'packingslip'.
  * @param {Object} downloadPdfWindow Provide a window handle for bypassing browser's popup blocking.
  */
	function _onCreateMissingDocumentsProceed(event, orderIds, type, downloadPdfWindow) {
		_openBulkPdfUrl(orderIds, type, downloadPdfWindow);
	}

	/**
  * On Single Checkbox Ready
  *
  * This callback will use the event.data.orderIds to set the checked checkboxes after a table re-render.
  *
  * @param {jQuery.Event} event
  */
	function _onSingleCheckboxReady(event) {
		event.data.orderIds.forEach(function (id) {
			$this.find('tr#' + id + ' input:checkbox').single_checkbox('checked', true).trigger('change');
		});

		// Bulk action button should't be disabled after a datatable reload.
		if ($('tr input:checkbox:checked').length) {
			$('.bulk-action').find('button').removeClass('disabled');
		}
	}

	/**
  * Opens the URL which provides the bulk PDF for download.
  *
  * @param {Number[]} orderIds The orders to be used for the concatenated document.
  * @param {String} type Whether 'invoice' or 'packingslip'.
  */
	function _openBulkPdfUrl(orderIds, type) {
		var parameters = {
			do: 'OrdersModalsAjax/BulkPdf' + (type === 'invoice' ? 'Invoices' : 'PackingSlips'),
			pageToken: jse.core.config.get('pageToken'),
			o: orderIds
		};

		var url = jse.core.config.get('appUrl') + '/admin/admin.php?' + $.param(parameters);

		window.open(url, '_parent');

		// Keep checkboxes checked after a datatable reload.
		$this.DataTable().ajax.reload(function () {
			$this.off('single_checkbox:ready', _onSingleCheckboxReady).on('single_checkbox:ready', { orderIds: orderIds }, _onSingleCheckboxReady);
		});
		$this.orders_overview_filter('reload');
	}

	/**
  * On Packing Slip Click
  */
	function _onShowPackingSlipClick() {
		// Message modal data.
		var title = jse.core.lang.translate('TITLE_SHOW_PACKINGSLIP', 'orders');
		var message = jse.core.lang.translate('NO_PACKINGSLIP_AVAILABLE', 'orders');

		// Request data.
		var rowData = $(this).parents('tr').data();
		var url = jse.core.config.get('appUrl') + '/admin/admin.php';

		// Request parameters.
		var data = {
			id: rowData.id,
			do: 'OrdersOverviewAjax/GetLatestPackingSlip',
			pageToken: jse.core.config.get('pageToken')
		};

		// Directly open a new tab (popup blocker workaround)
		var newTab = window.open('about:blank');

		$.ajax({ dataType: 'json', url: url, data: data }).done(function (response) {
			if (response.length) {
				// Get the file name from the response.
				var filename = response[0].file;

				// Packing slip link parameters.
				var parameters = {
					module: 'OrderAdmin',
					action: 'showPdf',
					type: 'packingslip',
					file: filename
				};

				// Open package slip.
				newTab.location = jse.core.config.get('appUrl') + '/admin/request_port.php?' + $.param(parameters);
			} else {
				// No packing slip found
				newTab.close();
				jse.libs.modal.showMessage(title, message);
			}
		});
	}

	/**
  * On Invoice Create Click
  */
	function _onCreateInvoiceClick() {
		var link = $(this).attr('href');
		var $loadingSpinner = jse.libs.loading_spinner.show($this);
		var pageToken = jse.core.config.get('pageToken');
		var orderId = $(this).parents('tr').data().id;
		var url = jse.core.config.get('appUrl') + ('/admin/admin.php?do=OrdersModalsAjax/GetInvoiceCount&pageToken=' + pageToken + '&orderId=' + orderId);

		// Directly open a new tab (popup blocker workaround)
		var newTab = window.open('about:blank');

		function createInvoice() {
			newTab.location = link;
			$this.DataTable().ajax.reload(null, false);
		}

		function addInvoice() {
			window.open(link, '_blank');
			$this.DataTable().ajax.reload(null, false);
		}

		function onRequestSuccess(response) {
			var modalTitle = jse.core.lang.translate('TITLE_CREATE_INVOICE', 'orders');
			var modalMessage = jse.core.lang.translate('TEXT_CREATE_INVOICE_CONFIRMATION', 'orders');
			var modalButtons = [{
				title: jse.core.lang.translate('yes', 'buttons'),
				callback: function callback(event) {
					closeModal(event);
					addInvoice();
				}
			}, {
				title: jse.core.lang.translate('no', 'buttons'),
				callback: closeModal
			}];

			function closeModal(event) {
				$(event.target).parents('.modal').modal('hide');
			}

			jse.libs.loading_spinner.hide($loadingSpinner);

			if (!response.count) {
				createInvoice();
			} else {
				newTab.close();
				jse.libs.modal.showMessage(modalTitle, modalMessage, modalButtons);
			}
		}

		function onRequestFailure() {
			jse.libs.loading_spinner.hide($loadingSpinner);
			createInvoice();
		}

		jse.libs.xhr.get({ url: url }).done(onRequestSuccess).fail(onRequestFailure);
	}

	/**
  * On Invoice Link Click
  *
  * The script that generates the PDFs is changing the status of an order to "invoice-created". Thus the
  * table data need to be redrawn and the filter options to be updated.
  */
	function _onShowInvoiceClick() {
		// Message modal data.
		var title = jse.core.lang.translate('TITLE_SHOW_INVOICE', 'orders');
		var message = jse.core.lang.translate('NO_INVOICE_AVAILABLE', 'orders');

		// Request data.
		var rowData = $(this).parents('tr').data();
		var url = jse.core.config.get('appUrl') + '/admin/admin.php';

		// Request parameters.
		var data = {
			id: rowData.id,
			do: 'OrdersOverviewAjax/GetInvoices',
			pageToken: jse.core.config.get('pageToken')
		};

		// Directly open a new tab (popup blocker workaround)
		var newTab = window.open('about:blank');

		$.ajax({ dataType: 'json', url: url, data: data }).done(function (response) {
			if (response.length) {
				// Get the file name from object with the highest ID within response array.
				var _response$ = response[0],
				    invoiceNumber = _response$.invoiceNumber,
				    orderId = _response$.orderId;

				// Invoice link parameters.

				var parameters = {
					module: 'OrderAdmin',
					action: 'showPdf',
					type: 'invoice',
					invoice_number: invoiceNumber,
					order_id: orderId
				};

				// Open invoice
				newTab.location = jse.core.config.get('appUrl') + '/admin/request_port.php?' + $.param(parameters);
			} else {
				// No invoice found
				newTab.close();
				jse.libs.modal.showMessage(title, message);
			}
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Bind table row actions.
		$this.on('click', 'tbody tr', _onTableRowClick).on('change', '.bulk-selection', _onBulkSelectionChange).on('change', 'input:checkbox', _onTableRowCheckboxChange).on('click', '.show-invoice', _onShowInvoiceClick).on('click', '.show-packing-slip', _onShowPackingSlipClick).on('click', '.create-invoice', _onCreateInvoiceClick).on('click', '.email-invoice', _onEmailInvoiceClick).on('click', '.email-order', _onEmailOrderClick).on('click', '.order-status.label', _onChangeOrderStatusClick).on('click', '.add-tracking-number', _onAddTrackingNumberClick);

		// Bind table row and bulk actions.
		$this.parents('.orders').on('click', '.btn-group .change-status', _onChangeOrderStatusClick).on('click', '.btn-group .cancel', _onCancelOrderClick).on('click', '.btn-group .delete, .actions .delete', _onDeleteOrderClick).on('click', '.btn-group .bulk-email-order', _onBulkEmailOrderClick).on('click', '.btn-group .bulk-email-invoice', _onBulkEmailInvoiceClick).on('click', '.btn-group .bulk-download-invoice', _onBulkDownloadInvoiceClick).on('click', '.btn-group .bulk-download-packing-slip', _onBulkDownloadPackingSlipClick);

		// Bind custom events.
		$(document).on('create_missing_documents:proceed', '.modal.create-missing-documents', _onCreateMissingDocumentsProceed);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vdmVydmlldy9ldmVudHMuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJfb25CdWxrU2VsZWN0aW9uQ2hhbmdlIiwiZXZlbnQiLCJwcm9wYWdhdGUiLCJmaW5kIiwic2luZ2xlX2NoZWNrYm94IiwicHJvcCIsInRyaWdnZXIiLCJfb25UYWJsZVJvd0NsaWNrIiwidGFyZ2V0IiwiaXMiLCJfb25UYWJsZVJvd0NoZWNrYm94Q2hhbmdlIiwibGVuZ3RoIiwicGFyZW50cyIsInJlbW92ZUNsYXNzIiwiYWRkQ2xhc3MiLCJfb25DYW5jZWxPcmRlckNsaWNrIiwicHJldmVudERlZmF1bHQiLCJzZWxlY3RlZE9yZGVycyIsIl9nZXRTZWxlY3RlZE9yZGVycyIsInRleHQiLCJqb2luIiwibW9kYWwiLCJfb25EZWxldGVPcmRlckNsaWNrIiwiX29uQnVsa0VtYWlsT3JkZXJDbGljayIsIiRtb2RhbCIsIiRtYWlsTGlzdCIsImdlbmVyYXRlTWFpbFJvd01hcmt1cCIsIiRyb3ciLCJjbGFzcyIsIiRpZENvbHVtbiIsIiRlbWFpbENvbHVtbiIsIiRpZExhYmVsIiwiaWQiLCIkZW1haWxJbnB1dCIsInR5cGUiLCJ2YWx1ZSIsImN1c3RvbWVyRW1haWwiLCJhcHBlbmRUbyIsImFwcGVuZCIsImVhY2giLCJyb3dEYXRhIiwicHVzaCIsImVtcHR5IiwiZm9yRWFjaCIsIm9yZGVyIiwiX29uQnVsa0VtYWlsSW52b2ljZUNsaWNrIiwiJGludm9pY2VDb2x1bW4iLCIkbGF0ZXN0SW52b2ljZUlkSW5wdXQiLCJsYXRlc3RJbnZvaWNlSWQiLCIkaW52b2ljZUxpbmsiLCJodG1sIiwibGF0ZXN0SW52b2ljZU51bWJlciIsInNlbGVjdGVkSW52b2ljZSIsIiR0YXJnZXQiLCJyb3dJZCIsIl9vbkVtYWlsSW52b2ljZUNsaWNrIiwidXJsIiwiY29yZSIsImNvbmZpZyIsImdldCIsImRvIiwicGFnZVRva2VuIiwiaW52b2ljZU51bWJlcnNIdG1sIiwiY3VzdG9tZXJOYW1lIiwidmFsIiwiYWpheCIsImRhdGFUeXBlIiwiZG9uZSIsInJlc3BvbnNlIiwiYXR0ciIsInN1YmplY3QiLCJpbnZvaWNlSWRFeGlzdHMiLCJPYmplY3QiLCJrZXlzIiwiaW52b2ljZU51bWJlcnMiLCJpbnZvaWNlSWQiLCJvbiIsIl9vbkNoYW5nZUVtYWlsSW52b2ljZUNoZWNrYm94Iiwid2lkZ2V0cyIsImluaXQiLCJfb25FbWFpbE9yZGVyQ2xpY2siLCJkYXRlRm9ybWF0IiwibGFuZyIsInRyYW5zbGF0ZSIsIm1vbWVudCIsInB1cmNoYXNlRGF0ZSIsImRhdGUiLCJmb3JtYXQiLCJfb25DaGFuZ2VPcmRlclN0YXR1c0NsaWNrIiwiaGFzQ2xhc3MiLCJzdG9wUHJvcGFnYXRpb24iLCJzdGF0dXNJZCIsIl9vbkFkZFRyYWNraW5nTnVtYmVyQ2xpY2siLCJfb25CdWxrRG93bmxvYWRJbnZvaWNlQ2xpY2siLCJvcmRlcklkcyIsIm1heEFtb3VudEludm9pY2VzQnVsa1BkZiIsIiRpbnZvaWNlTWVzc2FnZUNvbnRhaW5lciIsIl9jcmVhdGVCdWxrUGRmIiwiX29uQnVsa0Rvd25sb2FkUGFja2luZ1NsaXBDbGljayIsIm1heEFtb3VudFBhY2tpbmdTbGlwc0J1bGtQZGYiLCIkcGFja2luZ1NsaXBzTWVzc2FnZUNvbnRhaW5lciIsIkVycm9yIiwiZ2V0SlNPTiIsIm9yZGVySWRzV2l0aG91dERvY3VtZW50IiwiZXhjZXB0aW9uIiwidGl0bGUiLCJtZXNzYWdlIiwibGlicyIsInNob3dNZXNzYWdlIiwiX29wZW5CdWxrUGRmVXJsIiwiZmFpbCIsImpxeGhyIiwidGV4dFN0YXR1cyIsImVycm9yVGhyb3duIiwiX29uQ3JlYXRlTWlzc2luZ0RvY3VtZW50c1Byb2NlZWQiLCJkb3dubG9hZFBkZldpbmRvdyIsIl9vblNpbmdsZUNoZWNrYm94UmVhZHkiLCJwYXJhbWV0ZXJzIiwibyIsInBhcmFtIiwid2luZG93Iiwib3BlbiIsIkRhdGFUYWJsZSIsInJlbG9hZCIsIm9mZiIsIm9yZGVyc19vdmVydmlld19maWx0ZXIiLCJfb25TaG93UGFja2luZ1NsaXBDbGljayIsIm5ld1RhYiIsImZpbGVuYW1lIiwiZmlsZSIsImFjdGlvbiIsImxvY2F0aW9uIiwiY2xvc2UiLCJfb25DcmVhdGVJbnZvaWNlQ2xpY2siLCJsaW5rIiwiJGxvYWRpbmdTcGlubmVyIiwibG9hZGluZ19zcGlubmVyIiwic2hvdyIsIm9yZGVySWQiLCJjcmVhdGVJbnZvaWNlIiwiYWRkSW52b2ljZSIsIm9uUmVxdWVzdFN1Y2Nlc3MiLCJtb2RhbFRpdGxlIiwibW9kYWxNZXNzYWdlIiwibW9kYWxCdXR0b25zIiwiY2FsbGJhY2siLCJjbG9zZU1vZGFsIiwiaGlkZSIsImNvdW50Iiwib25SZXF1ZXN0RmFpbHVyZSIsInhociIsIl9vblNob3dJbnZvaWNlQ2xpY2siLCJpbnZvaWNlTnVtYmVyIiwiaW52b2ljZV9udW1iZXIiLCJvcmRlcl9pZCIsImRvY3VtZW50Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFFBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLHFDQUVDLGlCQUZELEVBR0MsT0FIRCxFQUlJLEtBSkosQ0FIRCxFQVVDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1MLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BLFVBQVNNLHNCQUFULENBQWdDQyxLQUFoQyxFQUF5RDtBQUFBLE1BQWxCQyxTQUFrQix1RUFBTixJQUFNOztBQUN4RCxNQUFJQSxjQUFjLEtBQWxCLEVBQXlCO0FBQ3hCLFVBRHdCLENBQ2hCO0FBQ1I7O0FBRURKLFFBQU1LLElBQU4sQ0FBVyxzQkFBWCxFQUFtQ0MsZUFBbkMsQ0FBbUQsU0FBbkQsRUFBOERMLEVBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsU0FBYixDQUE5RCxFQUF1RkMsT0FBdkYsQ0FBK0YsUUFBL0Y7QUFDQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNDLGdCQUFULENBQTBCTixLQUExQixFQUFpQztBQUNoQyxNQUFJLENBQUNGLEVBQUVFLE1BQU1PLE1BQVIsRUFBZ0JDLEVBQWhCLENBQW1CLElBQW5CLENBQUwsRUFBK0I7QUFDOUI7QUFDQTs7QUFFRFYsSUFBRSxJQUFGLEVBQVFJLElBQVIsQ0FBYSxnQkFBYixFQUNFRSxJQURGLENBQ08sU0FEUCxFQUNrQixDQUFDTixFQUFFLElBQUYsRUFBUUksSUFBUixDQUFhLGdCQUFiLEVBQStCRSxJQUEvQixDQUFvQyxTQUFwQyxDQURuQixFQUVFQyxPQUZGLENBRVUsUUFGVjtBQUdBOztBQUVEOzs7OztBQUtBLFVBQVNJLHlCQUFULEdBQXFDO0FBQ3BDLE1BQUlaLE1BQU1LLElBQU4sQ0FBVyx3QkFBWCxFQUFxQ1EsTUFBckMsR0FBOEMsQ0FBbEQsRUFBcUQ7QUFDcERiLFNBQU1jLE9BQU4sQ0FBYyxTQUFkLEVBQXlCVCxJQUF6QixDQUE4Qix1QkFBOUIsRUFBdURVLFdBQXZELENBQW1FLFVBQW5FO0FBQ0EsR0FGRCxNQUVPO0FBQ05mLFNBQU1jLE9BQU4sQ0FBYyxTQUFkLEVBQXlCVCxJQUF6QixDQUE4Qix1QkFBOUIsRUFBdURXLFFBQXZELENBQWdFLFVBQWhFO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxtQkFBVCxDQUE2QmQsS0FBN0IsRUFBb0M7QUFDbkNBLFFBQU1lLGNBQU47O0FBRUEsTUFBTUMsaUJBQWlCQyxtQkFBbUJuQixFQUFFLElBQUYsQ0FBbkIsQ0FBdkI7O0FBRUE7QUFDQUEsSUFBRSxnQ0FBRixFQUFvQ29CLElBQXBDLENBQXlDRixlQUFlRyxJQUFmLENBQW9CLElBQXBCLENBQXpDO0FBQ0FyQixJQUFFLGVBQUYsRUFBbUJzQixLQUFuQixDQUF5QixNQUF6QjtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0MsbUJBQVQsQ0FBNkJyQixLQUE3QixFQUFvQztBQUNuQ0EsUUFBTWUsY0FBTjs7QUFFQSxNQUFNQyxpQkFBaUJDLG1CQUFtQm5CLEVBQUUsSUFBRixDQUFuQixDQUF2Qjs7QUFFQTtBQUNBQSxJQUFFLGdDQUFGLEVBQW9Db0IsSUFBcEMsQ0FBeUNGLGVBQWVHLElBQWYsQ0FBb0IsSUFBcEIsQ0FBekM7QUFDQXJCLElBQUUsZUFBRixFQUFtQnNCLEtBQW5CLENBQXlCLE1BQXpCO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTRSxzQkFBVCxDQUFnQ3RCLEtBQWhDLEVBQXVDO0FBQ3RDLE1BQU11QixTQUFTekIsRUFBRSx5QkFBRixDQUFmO0FBQ0EsTUFBTTBCLFlBQVlELE9BQU9yQixJQUFQLENBQVksYUFBWixDQUFsQjs7QUFFQSxNQUFNdUIsd0JBQXdCLFNBQXhCQSxxQkFBd0IsT0FBUTtBQUNyQyxPQUFNQyxPQUFPNUIsRUFBRSxRQUFGLEVBQVksRUFBQzZCLE9BQU8sNEJBQVIsRUFBWixDQUFiO0FBQ0EsT0FBTUMsWUFBWTlCLEVBQUUsUUFBRixFQUFZLEVBQUM2QixPQUFPLFVBQVIsRUFBWixDQUFsQjtBQUNBLE9BQU1FLGVBQWUvQixFQUFFLFFBQUYsRUFBWSxFQUFDNkIsT0FBTyxVQUFSLEVBQVosQ0FBckI7O0FBRUEsT0FBTUcsV0FBV2hDLEVBQUUsVUFBRixFQUFjO0FBQzlCNkIsV0FBTyx3RUFEdUI7QUFFOUJULFVBQU10QixLQUFLbUM7QUFGbUIsSUFBZCxDQUFqQjs7QUFLQSxPQUFNQyxjQUFjbEMsRUFBRSxVQUFGLEVBQWM7QUFDakM2QixXQUFPLDBCQUQwQjtBQUVqQ00sVUFBTSxNQUYyQjtBQUdqQ0MsV0FBT3RDLEtBQUt1QztBQUhxQixJQUFkLENBQXBCOztBQU1BTCxZQUFTTSxRQUFULENBQWtCUixTQUFsQjtBQUNBSSxlQUFZSSxRQUFaLENBQXFCUCxZQUFyQjs7QUFFQUgsUUFBS1csTUFBTCxDQUFZLENBQUNULFNBQUQsRUFBWUMsWUFBWixDQUFaO0FBQ0FILFFBQUs5QixJQUFMLENBQVUsT0FBVixFQUFtQkEsSUFBbkI7O0FBRUEsVUFBTzhCLElBQVA7QUFDQSxHQXZCRDs7QUF5QkEsTUFBTVYsaUJBQWlCLEVBQXZCOztBQUVBaEIsUUFBTWUsY0FBTjs7QUFFQWxCLFFBQU1LLElBQU4sQ0FBVyw4QkFBWCxFQUEyQ29DLElBQTNDLENBQWdELFlBQVc7QUFDMUQsT0FBTUMsVUFBVXpDLEVBQUUsSUFBRixFQUFRYSxPQUFSLENBQWdCLElBQWhCLEVBQXNCZixJQUF0QixFQUFoQjtBQUNBb0Isa0JBQWV3QixJQUFmLENBQW9CRCxPQUFwQjtBQUNBLEdBSEQ7O0FBS0EsTUFBSXZCLGVBQWVOLE1BQW5CLEVBQTJCO0FBQzFCYyxhQUFVaUIsS0FBVjtBQUNBekIsa0JBQWUwQixPQUFmLENBQXVCO0FBQUEsV0FBU2xCLFVBQVVhLE1BQVYsQ0FBaUJaLHNCQUFzQmtCLEtBQXRCLENBQWpCLENBQVQ7QUFBQSxJQUF2QjtBQUNBcEIsVUFBT0gsS0FBUCxDQUFhLE1BQWI7QUFDQTtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsVUFBU3dCLHdCQUFULENBQWtDNUMsS0FBbEMsRUFBeUM7QUFDeEMsTUFBTXVCLFNBQVN6QixFQUFFLDJCQUFGLENBQWY7QUFDQSxNQUFNMEIsWUFBWUQsT0FBT3JCLElBQVAsQ0FBWSxhQUFaLENBQWxCOztBQUVBLE1BQU11Qix3QkFBd0IsU0FBeEJBLHFCQUF3QixPQUFRO0FBQ3JDLE9BQU1DLE9BQU81QixFQUFFLFFBQUYsRUFBWSxFQUFDNkIsT0FBTyw0QkFBUixFQUFaLENBQWI7QUFDQSxPQUFNQyxZQUFZOUIsRUFBRSxRQUFGLEVBQVksRUFBQzZCLE9BQU8sVUFBUixFQUFaLENBQWxCO0FBQ0EsT0FBTWtCLGlCQUFpQi9DLEVBQUUsUUFBRixFQUFZLEVBQUM2QixPQUFPLFVBQVIsRUFBWixDQUF2QjtBQUNBLE9BQU1FLGVBQWUvQixFQUFFLFFBQUYsRUFBWSxFQUFDNkIsT0FBTyxVQUFSLEVBQVosQ0FBckI7O0FBRUEsT0FBTW1CLHdCQUF3QmhELEVBQUUsVUFBRixFQUFjO0FBQzNDNkIsV0FBTyxnQ0FEb0M7QUFFM0NNLFVBQU0sUUFGcUM7QUFHM0NDLFdBQU90QyxLQUFLbUQ7QUFIK0IsSUFBZCxDQUE5Qjs7QUFNQSxPQUFNakIsV0FBV2hDLEVBQUUsVUFBRixFQUFjO0FBQzlCNkIsV0FBTyx3RUFEdUI7QUFFOUJULFVBQU10QixLQUFLbUM7QUFGbUIsSUFBZCxDQUFqQjs7QUFLQSxPQUFNaUIsZUFBZWxELEVBQUUsVUFBRixFQUFjO0FBQ2xDNkIsV0FBTyx3RUFEMkI7QUFFbENzQixVQUFNckQsS0FBS3NELG1CQUFMLEdBQTJCLGtHQUNYdEQsS0FBS3NELG1CQURNLGtCQUMwQnRELEtBQUttQyxFQUQvQiw0QkFFM0JuQyxLQUFLc0QsbUJBRnNCLFVBQTNCO0FBRjRCLElBQWQsQ0FBckI7O0FBT0EsT0FBTWxCLGNBQWNsQyxFQUFFLFVBQUYsRUFBYztBQUNqQzZCLFdBQU8sMEJBRDBCO0FBRWpDTSxVQUFNLE1BRjJCO0FBR2pDQyxXQUFPdEMsS0FBS3VDO0FBSHFCLElBQWQsQ0FBcEI7O0FBTUFMLFlBQVNNLFFBQVQsQ0FBa0JSLFNBQWxCO0FBQ0FvQixnQkFBYVosUUFBYixDQUFzQlMsY0FBdEI7QUFDQWIsZUFBWUksUUFBWixDQUFxQlAsWUFBckI7O0FBRUFILFFBQUtXLE1BQUwsQ0FBWSxDQUFDVCxTQUFELEVBQVlpQixjQUFaLEVBQTRCaEIsWUFBNUIsRUFBMENpQixxQkFBMUMsQ0FBWjtBQUNBcEIsUUFBSzlCLElBQUwsQ0FBVSxPQUFWLEVBQW1CQSxJQUFuQjs7QUFFQSxVQUFPOEIsSUFBUDtBQUNBLEdBdENEOztBQXdDQSxNQUFNeUIsa0JBQWtCLEVBQXhCOztBQUVBbkQsUUFBTWUsY0FBTjs7QUFFQWxCLFFBQU1LLElBQU4sQ0FBVyw4QkFBWCxFQUEyQ29DLElBQTNDLENBQWdELFlBQVc7QUFDMUQsT0FBTUMsVUFBVXpDLEVBQUUsSUFBRixFQUFRYSxPQUFSLENBQWdCLElBQWhCLEVBQXNCZixJQUF0QixFQUFoQjtBQUNBdUQsbUJBQWdCWCxJQUFoQixDQUFxQkQsT0FBckI7QUFDQSxHQUhEOztBQUtBLE1BQUlZLGdCQUFnQnpDLE1BQXBCLEVBQTRCO0FBQzNCYyxhQUFVaUIsS0FBVjtBQUNBVSxtQkFBZ0JULE9BQWhCLENBQXdCO0FBQUEsV0FBU2xCLFVBQVVhLE1BQVYsQ0FBaUJaLHNCQUFzQmtCLEtBQXRCLENBQWpCLENBQVQ7QUFBQSxJQUF4QjtBQUNBcEIsVUFBT0gsS0FBUCxDQUFhLE1BQWI7QUFDQTtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0gsa0JBQVQsQ0FBNEJtQyxPQUE1QixFQUFxQztBQUNwQyxNQUFNcEMsaUJBQWlCLEVBQXZCOztBQUVBLE1BQUlvQyxRQUFRekMsT0FBUixDQUFnQixjQUFoQixFQUFnQ0QsTUFBaEMsR0FBeUMsQ0FBN0MsRUFBZ0Q7QUFDL0M7QUFDQWIsU0FBTUssSUFBTixDQUFXLDhCQUFYLEVBQTJDb0MsSUFBM0MsQ0FBZ0QsWUFBVztBQUMxRHRCLG1CQUFld0IsSUFBZixDQUFvQjFDLEVBQUUsSUFBRixFQUFRYSxPQUFSLENBQWdCLElBQWhCLEVBQXNCZixJQUF0QixDQUEyQixJQUEzQixDQUFwQjtBQUNBLElBRkQ7QUFHQSxHQUxELE1BS087QUFDTixPQUFNeUQsUUFBUUQsUUFBUXpDLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JmLElBQXRCLENBQTJCLElBQTNCLENBQWQ7O0FBRUEsT0FBSSxDQUFDeUQsS0FBTCxFQUFZO0FBQ1gsV0FEVyxDQUNIO0FBQ1I7O0FBRURyQyxrQkFBZXdCLElBQWYsQ0FBb0JhLEtBQXBCO0FBQ0E7O0FBRUQsU0FBT3JDLGNBQVA7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTc0Msb0JBQVQsR0FBZ0M7QUFDL0IsTUFBTS9CLFNBQVN6QixFQUFFLHNCQUFGLENBQWY7QUFDQSxNQUFNeUMsVUFBVXpDLEVBQUUsSUFBRixFQUFRYSxPQUFSLENBQWdCLElBQWhCLEVBQXNCZixJQUF0QixFQUFoQjtBQUNBLE1BQU0yRCxNQUFNN0QsSUFBSThELElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0Msa0JBQTVDO0FBQ0EsTUFBTTlELE9BQU87QUFDWm1DLE9BQUlRLFFBQVFSLEVBREE7QUFFWjRCLE9BQUkseUNBRlE7QUFHWkMsY0FBV2xFLElBQUk4RCxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFdBQXBCO0FBSEMsR0FBYjtBQUtBLE1BQUlHLHFCQUFxQixFQUF6Qjs7QUFFQXRDLFNBQU9yQixJQUFQLENBQVksZ0JBQVosRUFBOEJnQixJQUE5QixPQUF1Q3FCLFFBQVF1QixZQUEvQztBQUNBdkMsU0FBT3JCLElBQVAsQ0FBWSxnQkFBWixFQUE4QjZELEdBQTlCLENBQWtDeEIsUUFBUUosYUFBMUM7O0FBRUFaLFNBQ0UzQixJQURGLENBQ08sU0FEUCxFQUNrQjJDLFFBQVFSLEVBRDFCLEVBRUVYLEtBRkYsQ0FFUSxNQUZSOztBQUlBdEIsSUFBRWtFLElBQUYsQ0FBTyxFQUFDVCxRQUFELEVBQU0zRCxVQUFOLEVBQVlxRSxVQUFVLE1BQXRCLEVBQVAsRUFBc0NDLElBQXRDLENBQTJDLFVBQUNDLFFBQUQsRUFBYztBQUN4RDVDLFVBQU82QyxJQUFQLENBQVksZ0JBQVosRUFBOEIsaUJBQTlCOztBQUVBN0MsVUFBT3JCLElBQVAsQ0FBWSxVQUFaLEVBQXdCNkQsR0FBeEIsQ0FBNEJJLFNBQVNFLE9BQXJDO0FBQ0EsT0FBSUYsU0FBU0csZUFBYixFQUE4QjtBQUM3Qi9DLFdBQU9yQixJQUFQLENBQVksbUJBQVosRUFBaUNXLFFBQWpDLENBQTBDLFFBQTFDO0FBQ0FVLFdBQU9yQixJQUFQLENBQVksYUFBWixFQUEyQlUsV0FBM0IsQ0FBdUMsUUFBdkM7QUFDQSxJQUhELE1BR087QUFDTlcsV0FBT3JCLElBQVAsQ0FBWSxtQkFBWixFQUFpQ1UsV0FBakMsQ0FBNkMsUUFBN0M7QUFDQVcsV0FBT3JCLElBQVAsQ0FBWSxhQUFaLEVBQTJCVyxRQUEzQixDQUFvQyxRQUFwQztBQUNBOztBQUVELE9BQUkwRCxPQUFPQyxJQUFQLENBQVlMLFNBQVNNLGNBQXJCLEVBQXFDL0QsTUFBckMsSUFBK0MsQ0FBbkQsRUFBc0Q7QUFDckRhLFdBQU9yQixJQUFQLENBQVksa0JBQVosRUFBZ0NXLFFBQWhDLENBQXlDLFFBQXpDO0FBQ0EsSUFGRCxNQUVPO0FBQ05VLFdBQU9yQixJQUFQLENBQVksa0JBQVosRUFBZ0NVLFdBQWhDLENBQTRDLFFBQTVDO0FBQ0E7O0FBRUQsUUFBSyxJQUFJOEQsU0FBVCxJQUFzQlAsU0FBU00sY0FBL0IsRUFBK0M7QUFDOUNaLDBCQUNDLDJEQUEyRGEsU0FBM0QsR0FDRSwwREFERixHQUVFUCxTQUFTTSxjQUFULENBQXdCQyxTQUF4QixDQUZGLEdBRXVDLE1BSHhDO0FBSUE7O0FBRURuRCxVQUFPckIsSUFBUCxDQUFZLDZCQUFaLEVBQTJDK0MsSUFBM0MsQ0FBZ0RZLGtCQUFoRDs7QUFFQXRDLFVBQU9yQixJQUFQLENBQVksMkJBQVosRUFBeUN5RSxFQUF6QyxDQUE0QyxRQUE1QyxFQUFzREMsNkJBQXREOztBQUVBckYsTUFBR3NGLE9BQUgsQ0FBV0MsSUFBWCxDQUFnQnZELE1BQWhCO0FBQ0EsR0E5QkQ7QUErQkE7O0FBRUQ7Ozs7O0FBS0EsVUFBU3FELDZCQUFULEdBQXlDO0FBQ3hDLE1BQU1yRCxTQUFTekIsRUFBRSxzQkFBRixDQUFmOztBQUVBLE1BQUl5QixPQUFPckIsSUFBUCxDQUFZLDJCQUFaLEVBQXlDUSxNQUF6QyxHQUFrRCxDQUF0RCxFQUF5RDtBQUN4RCxPQUFJYSxPQUFPckIsSUFBUCxDQUFZLG1DQUFaLEVBQWlEUSxNQUFqRCxHQUEwRCxDQUE5RCxFQUFpRTtBQUNoRWEsV0FBT3JCLElBQVAsQ0FBWSxPQUFaLEVBQXFCRSxJQUFyQixDQUEwQixVQUExQixFQUFzQyxLQUF0QztBQUNBLElBRkQsTUFFTztBQUNObUIsV0FBT3JCLElBQVAsQ0FBWSxPQUFaLEVBQXFCRSxJQUFyQixDQUEwQixVQUExQixFQUFzQyxJQUF0QztBQUNBO0FBQ0QsR0FORCxNQU1PO0FBQ05tQixVQUFPckIsSUFBUCxDQUFZLE9BQVosRUFBcUJFLElBQXJCLENBQTBCLFVBQTFCLEVBQXNDLEtBQXRDO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFVBQVMyRSxrQkFBVCxDQUE0Qi9FLEtBQTVCLEVBQW1DO0FBQ2xDLE1BQU11QixTQUFTekIsRUFBRSxvQkFBRixDQUFmO0FBQ0EsTUFBTXlDLFVBQVV6QyxFQUFFLElBQUYsRUFBUWEsT0FBUixDQUFnQixJQUFoQixFQUFzQmYsSUFBdEIsRUFBaEI7QUFDQSxNQUFNb0YsYUFBYXRGLElBQUk4RCxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLE1BQXdDLElBQXhDLEdBQStDLFVBQS9DLEdBQTRELFVBQS9FOztBQUVBbkMsU0FBT3JCLElBQVAsQ0FBWSxnQkFBWixFQUE4QmdCLElBQTlCLE9BQXVDcUIsUUFBUXVCLFlBQS9DO0FBQ0F2QyxTQUFPckIsSUFBUCxDQUFZLFVBQVosRUFBd0I2RCxHQUF4QixDQUE0QnJFLElBQUk4RCxJQUFKLENBQVN5QixJQUFULENBQWNDLFNBQWQsQ0FBd0IsZUFBeEIsRUFBeUMsZUFBekMsSUFBNEQzQyxRQUFRUixFQUFwRSxHQUN6QnJDLElBQUk4RCxJQUFKLENBQVN5QixJQUFULENBQWNDLFNBQWQsQ0FBd0Isb0JBQXhCLEVBQThDLGVBQTlDLENBRHlCLEdBRXpCQyxPQUFPNUMsUUFBUTZDLFlBQVIsQ0FBcUJDLElBQTVCLEVBQWtDQyxNQUFsQyxDQUF5Q04sVUFBekMsQ0FGSDtBQUdBekQsU0FBT3JCLElBQVAsQ0FBWSxnQkFBWixFQUE4QjZELEdBQTlCLENBQWtDeEIsUUFBUUosYUFBMUM7O0FBRUFaLFNBQ0UzQixJQURGLENBQ08sU0FEUCxFQUNrQjJDLFFBQVFSLEVBRDFCLEVBRUVYLEtBRkYsQ0FFUSxNQUZSO0FBR0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTbUUseUJBQVQsQ0FBbUN2RixLQUFuQyxFQUEwQztBQUN6QyxNQUFJRixFQUFFRSxNQUFNTyxNQUFSLEVBQWdCaUYsUUFBaEIsQ0FBeUIsY0FBekIsQ0FBSixFQUE4QztBQUM3Q3hGLFNBQU15RixlQUFOO0FBQ0E7O0FBRUQsTUFBTWxFLFNBQVN6QixFQUFFLGVBQUYsQ0FBZjtBQUNBLE1BQU15QyxVQUFVekMsRUFBRSxJQUFGLEVBQVFhLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JmLElBQXRCLEVBQWhCO0FBQ0EsTUFBTW9CLGlCQUFpQkMsbUJBQW1CbkIsRUFBRSxJQUFGLENBQW5CLENBQXZCOztBQUVBeUIsU0FBT3JCLElBQVAsQ0FBWSxrQkFBWixFQUFnQzZELEdBQWhDLENBQXFDeEIsT0FBRCxHQUFZQSxRQUFRbUQsUUFBcEIsR0FBK0IsRUFBbkU7O0FBRUFuRSxTQUFPckIsSUFBUCxDQUFZLFVBQVosRUFBd0I2RCxHQUF4QixDQUE0QixFQUE1QjtBQUNBeEMsU0FBT3JCLElBQVAsQ0FBWSw2REFBWixFQUNFa0UsSUFERixDQUNPLFNBRFAsRUFDa0IsS0FEbEIsRUFFRXpELE9BRkYsQ0FFVSxrQkFGVixFQUdFQyxXQUhGLENBR2MsU0FIZDs7QUFLQTtBQUNBVyxTQUFPckIsSUFBUCxDQUFZLGtCQUFaLEVBQWdDZ0IsSUFBaEMsQ0FBcUNGLGVBQWVHLElBQWYsQ0FBb0IsSUFBcEIsQ0FBckM7QUFDQUksU0FBT0gsS0FBUCxDQUFhLE1BQWI7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTdUUseUJBQVQsQ0FBbUMzRixLQUFuQyxFQUEwQztBQUN6QyxNQUFNdUIsU0FBU3pCLEVBQUUsNEJBQUYsQ0FBZjtBQUNBLE1BQU15QyxVQUFVekMsRUFBRUUsTUFBTU8sTUFBUixFQUFnQkksT0FBaEIsQ0FBd0IsSUFBeEIsRUFBOEJmLElBQTlCLEVBQWhCOztBQUVBMkIsU0FBTzNCLElBQVAsQ0FBWSxTQUFaLEVBQXVCMkMsUUFBUVIsRUFBL0I7QUFDQVIsU0FBT0gsS0FBUCxDQUFhLE1BQWI7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTd0UsMkJBQVQsR0FBdUM7QUFDdEMsTUFBTUMsV0FBVyxFQUFqQjtBQUNBLE1BQU1DLDJCQUEyQmxHLEtBQUtrRyx3QkFBdEM7O0FBRUFqRyxRQUFNSyxJQUFOLENBQVcsOEJBQVgsRUFBMkNvQyxJQUEzQyxDQUFnRCxZQUFXO0FBQzFEdUQsWUFBU3JELElBQVQsQ0FBYzFDLEVBQUUsSUFBRixFQUFRYSxPQUFSLENBQWdCLElBQWhCLEVBQXNCZixJQUF0QixDQUEyQixJQUEzQixDQUFkO0FBQ0EsR0FGRDs7QUFJQSxNQUFJaUcsU0FBU25GLE1BQVQsR0FBa0JvRix3QkFBdEIsRUFBZ0Q7QUFDL0MsT0FBTXZFLFNBQVN6QixFQUFFLG1CQUFGLENBQWY7QUFDQXlCLFVBQU9ILEtBQVAsQ0FBYSxNQUFiOztBQUVBLE9BQU0yRSwyQkFBMkJ4RSxPQUFPckIsSUFBUCxDQUFZLG1CQUFaLENBQWpDO0FBQ0E2Riw0QkFBeUJuRixXQUF6QixDQUFxQyxRQUFyQztBQUNBVyxVQUFPb0QsRUFBUCxDQUFVLGVBQVYsRUFBMkI7QUFBQSxXQUFNb0IseUJBQXlCbEYsUUFBekIsQ0FBa0MsUUFBbEMsQ0FBTjtBQUFBLElBQTNCOztBQUVBO0FBQ0E7O0FBRURtRixpQkFBZUgsUUFBZixFQUF5QixTQUF6QjtBQUNBOztBQUdEOzs7OztBQUtBLFVBQVNJLCtCQUFULEdBQTJDO0FBQzFDLE1BQU1KLFdBQVcsRUFBakI7QUFDQSxNQUFNSywrQkFBK0J0RyxLQUFLc0csNEJBQTFDO0FBQ0EsTUFBSTNFLGVBQUo7QUFDQSxNQUFJNEUsc0NBQUo7O0FBRUF0RyxRQUFNSyxJQUFOLENBQVcsOEJBQVgsRUFBMkNvQyxJQUEzQyxDQUFnRCxZQUFXO0FBQzFEdUQsWUFBU3JELElBQVQsQ0FBYzFDLEVBQUUsSUFBRixFQUFRYSxPQUFSLENBQWdCLElBQWhCLEVBQXNCZixJQUF0QixDQUEyQixJQUEzQixDQUFkO0FBQ0EsR0FGRDs7QUFJQSxNQUFJaUcsU0FBU25GLE1BQVQsR0FBa0J3Riw0QkFBdEIsRUFBb0Q7QUFDbkQzRSxZQUFTekIsRUFBRSxtQkFBRixDQUFUO0FBQ0F5QixVQUFPSCxLQUFQLENBQWEsTUFBYjtBQUNBK0UsbUNBQWdDNUUsT0FBT3JCLElBQVAsQ0FBWSx3QkFBWixDQUFoQzs7QUFFQWlHLGlDQUE4QnZGLFdBQTlCLENBQTBDLFFBQTFDOztBQUVBVyxVQUFPb0QsRUFBUCxDQUFVLGVBQVYsRUFBMkIsWUFBVztBQUNyQ3dCLGtDQUE4QnRGLFFBQTlCLENBQXVDLFFBQXZDO0FBQ0EsSUFGRDs7QUFJQTtBQUNBOztBQUVEbUYsaUJBQWVILFFBQWYsRUFBeUIsYUFBekI7QUFDQTs7QUFFRDs7Ozs7Ozs7OztBQVVBLFVBQVNHLGNBQVQsQ0FBd0JILFFBQXhCLEVBQWtDNUQsSUFBbEMsRUFBd0M7QUFDdkMsTUFBSUEsU0FBUyxTQUFULElBQXNCQSxTQUFTLGFBQW5DLEVBQWtEO0FBQ2pELFNBQU0sSUFBSW1FLEtBQUosQ0FBVSw0QkFBNEJuRSxJQUF0QyxDQUFOO0FBQ0E7O0FBRUQsTUFBTXNCLE1BQU03RCxJQUFJOEQsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxrQkFBNUM7QUFDQSxNQUFNOUQsT0FBTztBQUNaK0QsT0FBSSw4Q0FEUTtBQUVaQyxjQUFXbEUsSUFBSThELElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FGQztBQUdaekIsYUFIWTtBQUlaNEQ7QUFKWSxHQUFiOztBQU9BL0YsSUFBRXVHLE9BQUYsQ0FBVTlDLEdBQVYsRUFBZTNELElBQWYsRUFDRXNFLElBREYsQ0FDTyxtQ0FBMkI7QUFDaEMsT0FBSW9DLHdCQUF3QkMsU0FBNUIsRUFBdUM7QUFDdEMsUUFBTUMsUUFBUTlHLElBQUk4RCxJQUFKLENBQVN5QixJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FBZDtBQUNBLFFBQU11QixVQUFVL0csSUFBSThELElBQUosQ0FBU3lCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixtQ0FBeEIsRUFBNkQsY0FBN0QsQ0FBaEI7QUFDQXhGLFFBQUlnSCxJQUFKLENBQVN0RixLQUFULENBQWV1RixXQUFmLENBQTJCSCxLQUEzQixFQUFrQ0MsT0FBbEM7QUFDQTtBQUNBOztBQUVELE9BQUksQ0FBQ0gsd0JBQXdCNUYsTUFBN0IsRUFBcUM7QUFDcENrRyxvQkFBZ0JmLFFBQWhCLEVBQTBCNUQsSUFBMUIsRUFEb0MsQ0FDSDtBQUNqQztBQUNBOztBQUVEO0FBQ0EsT0FBTVYsU0FBU3pCLEVBQUUsaUNBQUYsQ0FBZjtBQUNBeUIsVUFBT3JCLElBQVAsQ0FBWSxpQkFBWixFQUErQmdCLElBQS9CLENBQW9Db0Ysd0JBQXdCbkYsSUFBeEIsQ0FBNkIsSUFBN0IsQ0FBcEM7QUFDQUksVUFDRTNCLElBREYsQ0FDTztBQUNMaUcsc0JBREs7QUFFTFMsb0RBRks7QUFHTHJFO0FBSEssSUFEUCxFQU1FYixLQU5GLENBTVEsTUFOUjtBQU9BLEdBeEJGLEVBeUJFeUYsSUF6QkYsQ0F5Qk8sVUFBQ0MsS0FBRCxFQUFRQyxVQUFSLEVBQW9CQyxXQUFwQixFQUFvQztBQUN6QyxPQUFNUixRQUFROUcsSUFBSThELElBQUosQ0FBU3lCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQUFkO0FBQ0EsT0FBTXVCLFVBQVUvRyxJQUFJOEQsSUFBSixDQUFTeUIsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1DQUF4QixFQUE2RCxjQUE3RCxDQUFoQjtBQUNBeEYsT0FBSWdILElBQUosQ0FBU3RGLEtBQVQsQ0FBZXVGLFdBQWYsQ0FBMkJILEtBQTNCLEVBQWtDQyxPQUFsQztBQUNBLEdBN0JGO0FBOEJBOztBQUVEOzs7Ozs7Ozs7OztBQVdBLFVBQVNRLGdDQUFULENBQTBDakgsS0FBMUMsRUFBaUQ2RixRQUFqRCxFQUEyRDVELElBQTNELEVBQWlFaUYsaUJBQWpFLEVBQW9GO0FBQ25GTixrQkFBZ0JmLFFBQWhCLEVBQTBCNUQsSUFBMUIsRUFBZ0NpRixpQkFBaEM7QUFDQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNDLHNCQUFULENBQWdDbkgsS0FBaEMsRUFBdUM7QUFDdENBLFFBQU1KLElBQU4sQ0FBV2lHLFFBQVgsQ0FBb0JuRCxPQUFwQixDQUE0QixjQUFNO0FBQ2pDN0MsU0FBTUssSUFBTixTQUFpQjZCLEVBQWpCLHNCQUFzQzVCLGVBQXRDLENBQXNELFNBQXRELEVBQWlFLElBQWpFLEVBQXVFRSxPQUF2RSxDQUErRSxRQUEvRTtBQUNBLEdBRkQ7O0FBSUE7QUFDQSxNQUFJUCxFQUFFLDJCQUFGLEVBQStCWSxNQUFuQyxFQUEyQztBQUMxQ1osS0FBRSxjQUFGLEVBQWtCSSxJQUFsQixDQUF1QixRQUF2QixFQUFpQ1UsV0FBakMsQ0FBNkMsVUFBN0M7QUFDQTtBQUNEOztBQUVEOzs7Ozs7QUFNQSxVQUFTZ0csZUFBVCxDQUF5QmYsUUFBekIsRUFBbUM1RCxJQUFuQyxFQUF5QztBQUN4QyxNQUFNbUYsYUFBYTtBQUNsQnpELE9BQUksOEJBQThCMUIsU0FBUyxTQUFULEdBQXFCLFVBQXJCLEdBQWtDLGNBQWhFLENBRGM7QUFFbEIyQixjQUFXbEUsSUFBSThELElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FGTztBQUdsQjJELE1BQUd4QjtBQUhlLEdBQW5COztBQU1BLE1BQU10QyxNQUFNN0QsSUFBSThELElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsbUJBQWhDLEdBQXNENUQsRUFBRXdILEtBQUYsQ0FBUUYsVUFBUixDQUFsRTs7QUFFQUcsU0FBT0MsSUFBUCxDQUFZakUsR0FBWixFQUFpQixTQUFqQjs7QUFFQTtBQUNBMUQsUUFBTTRILFNBQU4sR0FBa0J6RCxJQUFsQixDQUF1QjBELE1BQXZCLENBQThCLFlBQU07QUFDbkM3SCxTQUNFOEgsR0FERixDQUNNLHVCQUROLEVBQytCUixzQkFEL0IsRUFFRXhDLEVBRkYsQ0FFSyx1QkFGTCxFQUU4QixFQUFDa0Isa0JBQUQsRUFGOUIsRUFFMENzQixzQkFGMUM7QUFHQSxHQUpEO0FBS0F0SCxRQUFNK0gsc0JBQU4sQ0FBNkIsUUFBN0I7QUFDQTs7QUFFRDs7O0FBR0EsVUFBU0MsdUJBQVQsR0FBbUM7QUFDbEM7QUFDQSxNQUFNckIsUUFBUTlHLElBQUk4RCxJQUFKLENBQVN5QixJQUFULENBQWNDLFNBQWQsQ0FBd0Isd0JBQXhCLEVBQWtELFFBQWxELENBQWQ7QUFDQSxNQUFNdUIsVUFBVS9HLElBQUk4RCxJQUFKLENBQVN5QixJQUFULENBQWNDLFNBQWQsQ0FBd0IsMEJBQXhCLEVBQW9ELFFBQXBELENBQWhCOztBQUVBO0FBQ0EsTUFBTTNDLFVBQVV6QyxFQUFFLElBQUYsRUFBUWEsT0FBUixDQUFnQixJQUFoQixFQUFzQmYsSUFBdEIsRUFBaEI7QUFDQSxNQUFNMkQsTUFBTTdELElBQUk4RCxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLGtCQUE1Qzs7QUFFQTtBQUNBLE1BQU05RCxPQUFPO0FBQ1ptQyxPQUFJUSxRQUFRUixFQURBO0FBRVo0QixPQUFJLHlDQUZRO0FBR1pDLGNBQVdsRSxJQUFJOEQsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUhDLEdBQWI7O0FBTUE7QUFDQSxNQUFNb0UsU0FBU1AsT0FBT0MsSUFBUCxDQUFZLGFBQVosQ0FBZjs7QUFFQTFILElBQUVrRSxJQUFGLENBQU8sRUFBQ0MsVUFBVSxNQUFYLEVBQW1CVixRQUFuQixFQUF3QjNELFVBQXhCLEVBQVAsRUFDRXNFLElBREYsQ0FDTyxvQkFBWTtBQUNqQixPQUFJQyxTQUFTekQsTUFBYixFQUFxQjtBQUNwQjtBQUNBLFFBQU1xSCxXQUFXNUQsU0FBUyxDQUFULEVBQVk2RCxJQUE3Qjs7QUFFQTtBQUNBLFFBQU1aLGFBQWE7QUFDbEIzSCxhQUFRLFlBRFU7QUFFbEJ3SSxhQUFRLFNBRlU7QUFHbEJoRyxXQUFNLGFBSFk7QUFJbEIrRixXQUFNRDtBQUpZLEtBQW5COztBQU9BO0FBQ0FELFdBQU9JLFFBQVAsR0FDSXhJLElBQUk4RCxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBREosZ0NBQzRENUQsRUFBRXdILEtBQUYsQ0FBUUYsVUFBUixDQUQ1RDtBQUVBLElBZkQsTUFlTztBQUNOO0FBQ0FVLFdBQU9LLEtBQVA7QUFDQXpJLFFBQUlnSCxJQUFKLENBQVN0RixLQUFULENBQWV1RixXQUFmLENBQTJCSCxLQUEzQixFQUFrQ0MsT0FBbEM7QUFDQTtBQUNELEdBdEJGO0FBdUJBOztBQUVEOzs7QUFHQSxVQUFTMkIscUJBQVQsR0FBaUM7QUFDaEMsTUFBTUMsT0FBa0J2SSxFQUFFLElBQUYsRUFBUXNFLElBQVIsQ0FBYSxNQUFiLENBQXhCO0FBQ0EsTUFBTWtFLGtCQUFrQjVJLElBQUlnSCxJQUFKLENBQVM2QixlQUFULENBQXlCQyxJQUF6QixDQUE4QjNJLEtBQTlCLENBQXhCO0FBQ0EsTUFBTStELFlBQWtCbEUsSUFBSThELElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FBeEI7QUFDQSxNQUFNK0UsVUFBa0IzSSxFQUFFLElBQUYsRUFBUWEsT0FBUixDQUFnQixJQUFoQixFQUFzQmYsSUFBdEIsR0FBNkJtQyxFQUFyRDtBQUNBLE1BQU13QixNQUFrQjdELElBQUk4RCxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLHlFQUM2Q0UsU0FEN0MsaUJBQ2tFNkUsT0FEbEUsQ0FBeEI7O0FBR0E7QUFDQSxNQUFNWCxTQUFTUCxPQUFPQyxJQUFQLENBQVksYUFBWixDQUFmOztBQUVBLFdBQVNrQixhQUFULEdBQXlCO0FBQ3hCWixVQUFPSSxRQUFQLEdBQWtCRyxJQUFsQjtBQUNBeEksU0FBTTRILFNBQU4sR0FBa0J6RCxJQUFsQixDQUF1QjBELE1BQXZCLENBQThCLElBQTlCLEVBQW9DLEtBQXBDO0FBQ0E7O0FBRUQsV0FBU2lCLFVBQVQsR0FBc0I7QUFDckJwQixVQUFPQyxJQUFQLENBQVlhLElBQVosRUFBa0IsUUFBbEI7QUFDQXhJLFNBQU00SCxTQUFOLEdBQWtCekQsSUFBbEIsQ0FBdUIwRCxNQUF2QixDQUE4QixJQUE5QixFQUFvQyxLQUFwQztBQUNBOztBQUVELFdBQVNrQixnQkFBVCxDQUEwQnpFLFFBQTFCLEVBQW9DO0FBQ25DLE9BQU0wRSxhQUFhbkosSUFBSThELElBQUosQ0FBU3lCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixzQkFBeEIsRUFBZ0QsUUFBaEQsQ0FBbkI7QUFDQSxPQUFNNEQsZUFBZXBKLElBQUk4RCxJQUFKLENBQVN5QixJQUFULENBQWNDLFNBQWQsQ0FBd0Isa0NBQXhCLEVBQTRELFFBQTVELENBQXJCO0FBQ0EsT0FBTTZELGVBQWUsQ0FDcEI7QUFDQ3ZDLFdBQU85RyxJQUFJOEQsSUFBSixDQUFTeUIsSUFBVCxDQUFjQyxTQUFkLENBQXdCLEtBQXhCLEVBQStCLFNBQS9CLENBRFI7QUFFQzhELGNBQVUseUJBQVM7QUFDbEJDLGdCQUFXakosS0FBWDtBQUNBMkk7QUFDQTtBQUxGLElBRG9CLEVBUXBCO0FBQ0NuQyxXQUFPOUcsSUFBSThELElBQUosQ0FBU3lCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixJQUF4QixFQUE4QixTQUE5QixDQURSO0FBRUM4RCxjQUFVQztBQUZYLElBUm9CLENBQXJCOztBQWNBLFlBQVNBLFVBQVQsQ0FBb0JqSixLQUFwQixFQUEyQjtBQUMxQkYsTUFBRUUsTUFBTU8sTUFBUixFQUFnQkksT0FBaEIsQ0FBd0IsUUFBeEIsRUFBa0NTLEtBQWxDLENBQXdDLE1BQXhDO0FBQ0E7O0FBRUQxQixPQUFJZ0gsSUFBSixDQUFTNkIsZUFBVCxDQUF5QlcsSUFBekIsQ0FBOEJaLGVBQTlCOztBQUVBLE9BQUksQ0FBQ25FLFNBQVNnRixLQUFkLEVBQXFCO0FBQ3BCVDtBQUNBLElBRkQsTUFFTztBQUNOWixXQUFPSyxLQUFQO0FBQ0F6SSxRQUFJZ0gsSUFBSixDQUFTdEYsS0FBVCxDQUFldUYsV0FBZixDQUEyQmtDLFVBQTNCLEVBQXVDQyxZQUF2QyxFQUFxREMsWUFBckQ7QUFDQTtBQUNEOztBQUVELFdBQVNLLGdCQUFULEdBQTRCO0FBQzNCMUosT0FBSWdILElBQUosQ0FBUzZCLGVBQVQsQ0FBeUJXLElBQXpCLENBQThCWixlQUE5QjtBQUNBSTtBQUNBOztBQUVEaEosTUFBSWdILElBQUosQ0FBUzJDLEdBQVQsQ0FBYTNGLEdBQWIsQ0FBaUIsRUFBQ0gsUUFBRCxFQUFqQixFQUNFVyxJQURGLENBQ08wRSxnQkFEUCxFQUVFL0IsSUFGRixDQUVPdUMsZ0JBRlA7QUFHQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU0UsbUJBQVQsR0FBK0I7QUFDOUI7QUFDQSxNQUFNOUMsUUFBUTlHLElBQUk4RCxJQUFKLENBQVN5QixJQUFULENBQWNDLFNBQWQsQ0FBd0Isb0JBQXhCLEVBQThDLFFBQTlDLENBQWQ7QUFDQSxNQUFNdUIsVUFBVS9HLElBQUk4RCxJQUFKLENBQVN5QixJQUFULENBQWNDLFNBQWQsQ0FBd0Isc0JBQXhCLEVBQWdELFFBQWhELENBQWhCOztBQUVBO0FBQ0EsTUFBTTNDLFVBQVV6QyxFQUFFLElBQUYsRUFBUWEsT0FBUixDQUFnQixJQUFoQixFQUFzQmYsSUFBdEIsRUFBaEI7QUFDQSxNQUFNMkQsTUFBTTdELElBQUk4RCxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLGtCQUE1Qzs7QUFFQTtBQUNBLE1BQU05RCxPQUFPO0FBQ1ptQyxPQUFJUSxRQUFRUixFQURBO0FBRVo0QixPQUFJLGdDQUZRO0FBR1pDLGNBQVdsRSxJQUFJOEQsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUhDLEdBQWI7O0FBTUE7QUFDQSxNQUFNb0UsU0FBU1AsT0FBT0MsSUFBUCxDQUFZLGFBQVosQ0FBZjs7QUFFQTFILElBQUVrRSxJQUFGLENBQU8sRUFBQ0MsVUFBVSxNQUFYLEVBQW1CVixRQUFuQixFQUF3QjNELFVBQXhCLEVBQVAsRUFDRXNFLElBREYsQ0FDTyxvQkFBWTtBQUNqQixPQUFJQyxTQUFTekQsTUFBYixFQUFxQjtBQUNwQjtBQURvQixxQkFFYXlELFNBQVMsQ0FBVCxDQUZiO0FBQUEsUUFFYm9GLGFBRmEsY0FFYkEsYUFGYTtBQUFBLFFBRUVkLE9BRkYsY0FFRUEsT0FGRjs7QUFJcEI7O0FBQ0EsUUFBTXJCLGFBQWE7QUFDbEIzSCxhQUFRLFlBRFU7QUFFbEJ3SSxhQUFRLFNBRlU7QUFHbEJoRyxXQUFNLFNBSFk7QUFJbEJ1SCxxQkFBZ0JELGFBSkU7QUFLbEJFLGVBQVVoQjtBQUxRLEtBQW5COztBQVFBO0FBQ0FYLFdBQU9JLFFBQVAsR0FDSXhJLElBQUk4RCxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBREosZ0NBQzRENUQsRUFBRXdILEtBQUYsQ0FBUUYsVUFBUixDQUQ1RDtBQUVBLElBaEJELE1BZ0JPO0FBQ047QUFDQVUsV0FBT0ssS0FBUDtBQUNBekksUUFBSWdILElBQUosQ0FBU3RGLEtBQVQsQ0FBZXVGLFdBQWYsQ0FBMkJILEtBQTNCLEVBQWtDQyxPQUFsQztBQUNBO0FBQ0QsR0F2QkY7QUF3QkE7O0FBRUQ7QUFDQTtBQUNBOztBQUVBaEgsUUFBT3FGLElBQVAsR0FBYyxVQUFTWixJQUFULEVBQWU7QUFDNUI7QUFDQXJFLFFBQ0U4RSxFQURGLENBQ0ssT0FETCxFQUNjLFVBRGQsRUFDMEJyRSxnQkFEMUIsRUFFRXFFLEVBRkYsQ0FFSyxRQUZMLEVBRWUsaUJBRmYsRUFFa0M1RSxzQkFGbEMsRUFHRTRFLEVBSEYsQ0FHSyxRQUhMLEVBR2UsZ0JBSGYsRUFHaUNsRSx5QkFIakMsRUFJRWtFLEVBSkYsQ0FJSyxPQUpMLEVBSWMsZUFKZCxFQUkrQjJFLG1CQUovQixFQUtFM0UsRUFMRixDQUtLLE9BTEwsRUFLYyxvQkFMZCxFQUtvQ2tELHVCQUxwQyxFQU1FbEQsRUFORixDQU1LLE9BTkwsRUFNYyxpQkFOZCxFQU1pQ3lELHFCQU5qQyxFQU9FekQsRUFQRixDQU9LLE9BUEwsRUFPYyxnQkFQZCxFQU9nQ3JCLG9CQVBoQyxFQVFFcUIsRUFSRixDQVFLLE9BUkwsRUFRYyxjQVJkLEVBUThCSSxrQkFSOUIsRUFTRUosRUFURixDQVNLLE9BVEwsRUFTYyxxQkFUZCxFQVNxQ1kseUJBVHJDLEVBVUVaLEVBVkYsQ0FVSyxPQVZMLEVBVWMsc0JBVmQsRUFVc0NnQix5QkFWdEM7O0FBWUE7QUFDQTlGLFFBQU1jLE9BQU4sQ0FBYyxTQUFkLEVBQ0VnRSxFQURGLENBQ0ssT0FETCxFQUNjLDJCQURkLEVBQzJDWSx5QkFEM0MsRUFFRVosRUFGRixDQUVLLE9BRkwsRUFFYyxvQkFGZCxFQUVvQzdELG1CQUZwQyxFQUdFNkQsRUFIRixDQUdLLE9BSEwsRUFHYyxzQ0FIZCxFQUdzRHRELG1CQUh0RCxFQUlFc0QsRUFKRixDQUlLLE9BSkwsRUFJYyw4QkFKZCxFQUk4Q3JELHNCQUo5QyxFQUtFcUQsRUFMRixDQUtLLE9BTEwsRUFLYyxnQ0FMZCxFQUtnRC9CLHdCQUxoRCxFQU1FK0IsRUFORixDQU1LLE9BTkwsRUFNYyxtQ0FOZCxFQU1tRGlCLDJCQU5uRCxFQU9FakIsRUFQRixDQU9LLE9BUEwsRUFPYyx3Q0FQZCxFQU93RHNCLCtCQVB4RDs7QUFTQTtBQUNBbkcsSUFBRTRKLFFBQUYsRUFBWS9FLEVBQVosQ0FBZSxrQ0FBZixFQUFtRCxpQ0FBbkQsRUFDQ3NDLGdDQUREOztBQUdBL0M7QUFDQSxFQTdCRDs7QUErQkEsUUFBT3pFLE1BQVA7QUFDQSxDQXB3QkYiLCJmaWxlIjoib3JkZXJzL292ZXJ2aWV3L2V2ZW50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBldmVudHMuanMgMjAxOC0wMy0yOFxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTggR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBNYWluIFRhYmxlIEV2ZW50c1xyXG4gKlxyXG4gKiBIYW5kbGVzIHRoZSBldmVudHMgb2YgdGhlIG1haW4gb3JkZXJzIHRhYmxlLlxyXG4gKi9cclxuZ3guY29udHJvbGxlcnMubW9kdWxlKFxyXG5cdCdldmVudHMnLFxyXG5cdFxyXG5cdFtcclxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9tb21lbnRqcy9tb21lbnQubWluLmpzYCxcclxuXHRcdCdsb2FkaW5nX3NwaW5uZXInLFxyXG5cdFx0J21vZGFsJyxcclxuXHQgICAgJ3hocidcclxuXHRdLFxyXG5cdFxyXG5cdGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFxyXG5cdFx0J3VzZSBzdHJpY3QnO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIFZBUklBQkxFU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIEZVTkNUSU9OU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gQnVsayBTZWxlY3Rpb24gQ2hhbmdlXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXHJcblx0XHQgKiBAcGFyYW0ge0Jvb2xlYW59IHByb3BhZ2F0ZSBXaGV0aGVyIHRvIGFmZmVjdCB0aGUgYm9keSBlbGVtZW50cy4gV2UgZG8gbm90IG5lZWQgdGhpcyBvbiBcImRyYXcuZHRcIiBldmVudC5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uQnVsa1NlbGVjdGlvbkNoYW5nZShldmVudCwgcHJvcGFnYXRlID0gdHJ1ZSkge1xyXG5cdFx0XHRpZiAocHJvcGFnYXRlID09PSBmYWxzZSkge1xyXG5cdFx0XHRcdHJldHVybjsgLy8gRG8gbm90IHByb3BhZ2F0ZSBvbiBkcmF3IGV2ZW50IGJlY2F1c2UgdGhlIGJvZHkgY2hlY2tib3hlcyBhcmUgdW5jaGVja2VkIGJ5IGRlZmF1bHQuXHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdCR0aGlzLmZpbmQoJ3Rib2R5IGlucHV0OmNoZWNrYm94Jykuc2luZ2xlX2NoZWNrYm94KCdjaGVja2VkJywgJCh0aGlzKS5wcm9wKCdjaGVja2VkJykpLnRyaWdnZXIoJ2NoYW5nZScpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIFRhYmxlIFJvdyBDbGlja1xyXG5cdFx0ICpcclxuXHRcdCAqIFdoZW4gYSByb3cgaXMgY2xpY2tlZCB0aGVuIHRoZSByb3ctY2hlY2tib3ggbXVzdCBiZSB0b2dnbGVkLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25UYWJsZVJvd0NsaWNrKGV2ZW50KSB7XHJcblx0XHRcdGlmICghJChldmVudC50YXJnZXQpLmlzKCd0ZCcpKSB7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHQkKHRoaXMpLmZpbmQoJ2lucHV0OmNoZWNrYm94JylcclxuXHRcdFx0XHQucHJvcCgnY2hlY2tlZCcsICEkKHRoaXMpLmZpbmQoJ2lucHV0OmNoZWNrYm94JykucHJvcCgnY2hlY2tlZCcpKVxyXG5cdFx0XHRcdC50cmlnZ2VyKCdjaGFuZ2UnKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBUYWJsZSBSb3cgQ2hlY2tib3ggQ2hhbmdlXHJcblx0XHQgKlxyXG5cdFx0ICogQWRqdXN0IHRoZSBidWxrIGFjdGlvbnMgc3RhdGUgd2hlbmV2ZXIgdGhlcmUgYXJlIGNoYW5nZXMgaW4gdGhlIHRhYmxlIGNoZWNrYm94ZXMuXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vblRhYmxlUm93Q2hlY2tib3hDaGFuZ2UoKSB7XHJcblx0XHRcdGlmICgkdGhpcy5maW5kKCdpbnB1dDpjaGVja2JveDpjaGVja2VkJykubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdCR0aGlzLnBhcmVudHMoJy5vcmRlcnMnKS5maW5kKCcuYnVsay1hY3Rpb24gPiBidXR0b24nKS5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHQkdGhpcy5wYXJlbnRzKCcub3JkZXJzJykuZmluZCgnLmJ1bGstYWN0aW9uID4gYnV0dG9uJykuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBDYW5jZWwgT3JkZXIgQ2xpY2tcclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uQ2FuY2VsT3JkZXJDbGljayhldmVudCkge1xyXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3Qgc2VsZWN0ZWRPcmRlcnMgPSBfZ2V0U2VsZWN0ZWRPcmRlcnMoJCh0aGlzKSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBTaG93IHRoZSBvcmRlciBkZWxldGUgbW9kYWwuXHJcblx0XHRcdCQoJy5jYW5jZWwubW9kYWwgLnNlbGVjdGVkLW9yZGVycycpLnRleHQoc2VsZWN0ZWRPcmRlcnMuam9pbignLCAnKSk7XHJcblx0XHRcdCQoJy5jYW5jZWwubW9kYWwnKS5tb2RhbCgnc2hvdycpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIERlbGV0ZSBPcmRlciBDbGlja1xyXG5cdFx0ICpcclxuXHRcdCAqIERpc3BsYXkgdGhlIGRlbGV0ZS1tb2RhbC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uRGVsZXRlT3JkZXJDbGljayhldmVudCkge1xyXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3Qgc2VsZWN0ZWRPcmRlcnMgPSBfZ2V0U2VsZWN0ZWRPcmRlcnMoJCh0aGlzKSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBTaG93IHRoZSBvcmRlciBkZWxldGUgbW9kYWwuXHJcblx0XHRcdCQoJy5kZWxldGUubW9kYWwgLnNlbGVjdGVkLW9yZGVycycpLnRleHQoc2VsZWN0ZWRPcmRlcnMuam9pbignLCAnKSk7XHJcblx0XHRcdCQoJy5kZWxldGUubW9kYWwnKS5tb2RhbCgnc2hvdycpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIFNlbmQgT3JkZXIgQ2xpY2suXHJcblx0XHQgKlxyXG5cdFx0ICogU2VuZHMgdGhlIGVtYWlsIG9yZGVyIGNvbmZpcm1hdGlvbnMuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vbkJ1bGtFbWFpbE9yZGVyQ2xpY2soZXZlbnQpIHtcclxuXHRcdFx0Y29uc3QgJG1vZGFsID0gJCgnLmJ1bGstZW1haWwtb3JkZXIubW9kYWwnKTtcclxuXHRcdFx0Y29uc3QgJG1haWxMaXN0ID0gJG1vZGFsLmZpbmQoJy5lbWFpbC1saXN0Jyk7XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCBnZW5lcmF0ZU1haWxSb3dNYXJrdXAgPSBkYXRhID0+IHtcclxuXHRcdFx0XHRjb25zdCAkcm93ID0gJCgnPGRpdi8+Jywge2NsYXNzOiAnZm9ybS1ncm91cCBlbWFpbC1saXN0LWl0ZW0nfSk7XHJcblx0XHRcdFx0Y29uc3QgJGlkQ29sdW1uID0gJCgnPGRpdi8+Jywge2NsYXNzOiAnY29sLXNtLTMnfSk7XHJcblx0XHRcdFx0Y29uc3QgJGVtYWlsQ29sdW1uID0gJCgnPGRpdi8+Jywge2NsYXNzOiAnY29sLXNtLTknfSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y29uc3QgJGlkTGFiZWwgPSAkKCc8bGFiZWwvPicsIHtcclxuXHRcdFx0XHRcdGNsYXNzOiAnY29udHJvbC1sYWJlbCBpZC1sYWJlbCBmb3JjZS10ZXh0LWNvbG9yLWJsYWNrIGZvcmNlLXRleHQtbm9ybWFsLXdlaWdodCcsXHJcblx0XHRcdFx0XHR0ZXh0OiBkYXRhLmlkXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y29uc3QgJGVtYWlsSW5wdXQgPSAkKCc8aW5wdXQvPicsIHtcclxuXHRcdFx0XHRcdGNsYXNzOiAnZm9ybS1jb250cm9sIGVtYWlsLWlucHV0JyxcclxuXHRcdFx0XHRcdHR5cGU6ICd0ZXh0JyxcclxuXHRcdFx0XHRcdHZhbHVlOiBkYXRhLmN1c3RvbWVyRW1haWxcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQkaWRMYWJlbC5hcHBlbmRUbygkaWRDb2x1bW4pO1xyXG5cdFx0XHRcdCRlbWFpbElucHV0LmFwcGVuZFRvKCRlbWFpbENvbHVtbik7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JHJvdy5hcHBlbmQoWyRpZENvbHVtbiwgJGVtYWlsQ29sdW1uXSk7XHJcblx0XHRcdFx0JHJvdy5kYXRhKCdvcmRlcicsIGRhdGEpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdHJldHVybiAkcm93O1xyXG5cdFx0XHR9O1xyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3Qgc2VsZWN0ZWRPcmRlcnMgPSBbXTtcclxuXHRcdFx0XHJcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFxyXG5cdFx0XHQkdGhpcy5maW5kKCd0Ym9keSBpbnB1dDpjaGVja2JveDpjaGVja2VkJykuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRjb25zdCByb3dEYXRhID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoKTtcclxuXHRcdFx0XHRzZWxlY3RlZE9yZGVycy5wdXNoKHJvd0RhdGEpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdGlmIChzZWxlY3RlZE9yZGVycy5sZW5ndGgpIHtcclxuXHRcdFx0XHQkbWFpbExpc3QuZW1wdHkoKTtcclxuXHRcdFx0XHRzZWxlY3RlZE9yZGVycy5mb3JFYWNoKG9yZGVyID0+ICRtYWlsTGlzdC5hcHBlbmQoZ2VuZXJhdGVNYWlsUm93TWFya3VwKG9yZGVyKSkpO1xyXG5cdFx0XHRcdCRtb2RhbC5tb2RhbCgnc2hvdycpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gU2VuZCBJbnZvaWNlIENsaWNrLlxyXG5cdFx0ICpcclxuXHRcdCAqIFNlbmRzIHRoZSBlbWFpbCBpbnZvaWNlLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBGaXJlZCBldmVudC5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uQnVsa0VtYWlsSW52b2ljZUNsaWNrKGV2ZW50KSB7XHJcblx0XHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5idWxrLWVtYWlsLWludm9pY2UubW9kYWwnKTtcclxuXHRcdFx0Y29uc3QgJG1haWxMaXN0ID0gJG1vZGFsLmZpbmQoJy5lbWFpbC1saXN0Jyk7XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCBnZW5lcmF0ZU1haWxSb3dNYXJrdXAgPSBkYXRhID0+IHtcclxuXHRcdFx0XHRjb25zdCAkcm93ID0gJCgnPGRpdi8+Jywge2NsYXNzOiAnZm9ybS1ncm91cCBlbWFpbC1saXN0LWl0ZW0nfSk7XHJcblx0XHRcdFx0Y29uc3QgJGlkQ29sdW1uID0gJCgnPGRpdi8+Jywge2NsYXNzOiAnY29sLXNtLTMnfSk7XHJcblx0XHRcdFx0Y29uc3QgJGludm9pY2VDb2x1bW4gPSAkKCc8ZGl2Lz4nLCB7Y2xhc3M6ICdjb2wtc20tMyd9KTtcclxuXHRcdFx0XHRjb25zdCAkZW1haWxDb2x1bW4gPSAkKCc8ZGl2Lz4nLCB7Y2xhc3M6ICdjb2wtc20tNid9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRjb25zdCAkbGF0ZXN0SW52b2ljZUlkSW5wdXQgPSAkKCc8aW5wdXQvPicsIHtcclxuXHRcdFx0XHRcdGNsYXNzOiAnZm9ybS1jb250cm9sIGxhdGVzdC1pbnZvaWNlLWlkJyxcclxuXHRcdFx0XHRcdHR5cGU6ICdoaWRkZW4nLFxyXG5cdFx0XHRcdFx0dmFsdWU6IGRhdGEubGF0ZXN0SW52b2ljZUlkXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y29uc3QgJGlkTGFiZWwgPSAkKCc8bGFiZWwvPicsIHtcclxuXHRcdFx0XHRcdGNsYXNzOiAnY29udHJvbC1sYWJlbCBpZC1sYWJlbCBmb3JjZS10ZXh0LWNvbG9yLWJsYWNrIGZvcmNlLXRleHQtbm9ybWFsLXdlaWdodCcsXHJcblx0XHRcdFx0XHR0ZXh0OiBkYXRhLmlkXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y29uc3QgJGludm9pY2VMaW5rID0gJCgnPGxhYmVsLz4nLCB7XHJcblx0XHRcdFx0XHRjbGFzczogJ2NvbnRyb2wtbGFiZWwgaWQtbGFiZWwgZm9yY2UtdGV4dC1jb2xvci1ibGFjayBmb3JjZS10ZXh0LW5vcm1hbC13ZWlnaHQnLFxyXG5cdFx0XHRcdFx0aHRtbDogZGF0YS5sYXRlc3RJbnZvaWNlTnVtYmVyID8gYDxhIGhyZWY9XCJyZXF1ZXN0X3BvcnQucGhwP21vZHVsZT1PcmRlckFkbWluJmFjdGlvbj1zaG93UGRmJnR5cGU9aW52b2ljZWBcclxuXHRcdFx0XHRcdFx0KyBgJmludm9pY2VfbnVtYmVyPSR7ZGF0YS5sYXRlc3RJbnZvaWNlTnVtYmVyfSZvcmRlcl9pZD0ke2RhdGEuaWR9XCIgdGFyZ2V0PVwiX2JsYW5rXCI+YFxyXG5cdFx0XHRcdFx0XHQrIGAke2RhdGEubGF0ZXN0SW52b2ljZU51bWJlcn08L2E+YCA6IGAtYFxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGNvbnN0ICRlbWFpbElucHV0ID0gJCgnPGlucHV0Lz4nLCB7XHJcblx0XHRcdFx0XHRjbGFzczogJ2Zvcm0tY29udHJvbCBlbWFpbC1pbnB1dCcsXHJcblx0XHRcdFx0XHR0eXBlOiAndGV4dCcsXHJcblx0XHRcdFx0XHR2YWx1ZTogZGF0YS5jdXN0b21lckVtYWlsXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JGlkTGFiZWwuYXBwZW5kVG8oJGlkQ29sdW1uKTtcclxuXHRcdFx0XHQkaW52b2ljZUxpbmsuYXBwZW5kVG8oJGludm9pY2VDb2x1bW4pO1xyXG5cdFx0XHRcdCRlbWFpbElucHV0LmFwcGVuZFRvKCRlbWFpbENvbHVtbik7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JHJvdy5hcHBlbmQoWyRpZENvbHVtbiwgJGludm9pY2VDb2x1bW4sICRlbWFpbENvbHVtbiwgJGxhdGVzdEludm9pY2VJZElucHV0XSk7XHJcblx0XHRcdFx0JHJvdy5kYXRhKCdvcmRlcicsIGRhdGEpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdHJldHVybiAkcm93O1xyXG5cdFx0XHR9O1xyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3Qgc2VsZWN0ZWRJbnZvaWNlID0gW107XHJcblx0XHRcdFxyXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcclxuXHRcdFx0JHRoaXMuZmluZCgndGJvZHkgaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZCcpLmVhY2goZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0Y29uc3Qgcm93RGF0YSA9ICQodGhpcykucGFyZW50cygndHInKS5kYXRhKCk7XHJcblx0XHRcdFx0c2VsZWN0ZWRJbnZvaWNlLnB1c2gocm93RGF0YSk7XHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKHNlbGVjdGVkSW52b2ljZS5sZW5ndGgpIHtcclxuXHRcdFx0XHQkbWFpbExpc3QuZW1wdHkoKTtcclxuXHRcdFx0XHRzZWxlY3RlZEludm9pY2UuZm9yRWFjaChvcmRlciA9PiAkbWFpbExpc3QuYXBwZW5kKGdlbmVyYXRlTWFpbFJvd01hcmt1cChvcmRlcikpKTtcclxuXHRcdFx0XHQkbW9kYWwubW9kYWwoJ3Nob3cnKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIENvbGxlY3RzIHRoZSBJRHMgb2YgdGhlIHNlbGVjdGVkIG9yZGVycyBhbmQgcmV0dXJucyB0aGVtIGFzIGFuIGFycmF5LlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGFyZ2V0IFRoZSB0cmlnZ2VyaW5nIHRhcmdldFxyXG5cdFx0ICpcclxuXHRcdCAqIEByZXR1cm4ge051bWJlcltdfSBhcnJheSBvZiBvcmRlciBJRHNcclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX2dldFNlbGVjdGVkT3JkZXJzKCR0YXJnZXQpIHtcclxuXHRcdFx0Y29uc3Qgc2VsZWN0ZWRPcmRlcnMgPSBbXTtcclxuXHRcdFx0XHJcblx0XHRcdGlmICgkdGFyZ2V0LnBhcmVudHMoJy5idWxrLWFjdGlvbicpLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0XHQvLyBGZXRjaCB0aGUgc2VsZWN0ZWQgb3JkZXIgSURzLlxyXG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3Rib2R5IGlucHV0OmNoZWNrYm94OmNoZWNrZWQnKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0c2VsZWN0ZWRPcmRlcnMucHVzaCgkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKSk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0Y29uc3Qgcm93SWQgPSAkdGFyZ2V0LnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoIXJvd0lkKSB7XHJcblx0XHRcdFx0XHRyZXR1cm47IC8vIE5vIG9yZGVyIElEIHdhcyBmb3VuZC5cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0c2VsZWN0ZWRPcmRlcnMucHVzaChyb3dJZCk7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdHJldHVybiBzZWxlY3RlZE9yZGVycztcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBFbWFpbCBJbnZvaWNlIENsaWNrXHJcblx0XHQgKlxyXG5cdFx0ICogRGlzcGxheSB0aGUgZW1haWwtaW52b2ljZSBtb2RhbC5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uRW1haWxJbnZvaWNlQ2xpY2soKSB7XHJcblx0XHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5lbWFpbC1pbnZvaWNlLm1vZGFsJyk7XHJcblx0XHRcdGNvbnN0IHJvd0RhdGEgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgpO1xyXG5cdFx0XHRjb25zdCB1cmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwJztcclxuXHRcdFx0Y29uc3QgZGF0YSA9IHtcclxuXHRcdFx0XHRpZDogcm93RGF0YS5pZCxcclxuXHRcdFx0XHRkbzogJ09yZGVyc01vZGFsc0FqYXgvR2V0RW1haWxJbnZvaWNlU3ViamVjdCcsXHJcblx0XHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxyXG5cdFx0XHR9O1xyXG5cdFx0XHRsZXQgaW52b2ljZU51bWJlcnNIdG1sID0gJyc7XHJcblx0XHRcdFxyXG5cdFx0XHQkbW9kYWwuZmluZCgnLmN1c3RvbWVyLWluZm8nKS50ZXh0KGBcIiR7cm93RGF0YS5jdXN0b21lck5hbWV9XCJgKTtcclxuXHRcdFx0JG1vZGFsLmZpbmQoJy5lbWFpbC1hZGRyZXNzJykudmFsKHJvd0RhdGEuY3VzdG9tZXJFbWFpbCk7XHJcblx0XHRcdFxyXG5cdFx0XHQkbW9kYWxcclxuXHRcdFx0XHQuZGF0YSgnb3JkZXJJZCcsIHJvd0RhdGEuaWQpXHJcblx0XHRcdFx0Lm1vZGFsKCdzaG93Jyk7XHJcblx0XHRcdFxyXG5cdFx0XHQkLmFqYXgoe3VybCwgZGF0YSwgZGF0YVR5cGU6ICdqc29uJ30pLmRvbmUoKHJlc3BvbnNlKSA9PiB7XHJcblx0XHRcdFx0JG1vZGFsLmF0dHIoJ2RhdGEtZ3gtd2lkZ2V0JywgJ3NpbmdsZV9jaGVja2JveCcpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdCRtb2RhbC5maW5kKCcuc3ViamVjdCcpLnZhbChyZXNwb25zZS5zdWJqZWN0KTtcclxuXHRcdFx0XHRpZiAocmVzcG9uc2UuaW52b2ljZUlkRXhpc3RzKSB7XHJcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtLWluZm8nKS5hZGRDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLm5vLWludm9pY2UnKS5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcuaW52b2ljZS1udW0taW5mbycpLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcclxuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcubm8taW52b2ljZScpLmFkZENsYXNzKCdoaWRkZW4nKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKE9iamVjdC5rZXlzKHJlc3BvbnNlLmludm9pY2VOdW1iZXJzKS5sZW5ndGggPD0gMSkge1xyXG5cdFx0XHRcdFx0JG1vZGFsLmZpbmQoJy5pbnZvaWNlLW51bWJlcnMnKS5hZGRDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcuaW52b2ljZS1udW1iZXJzJykucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRmb3IgKGxldCBpbnZvaWNlSWQgaW4gcmVzcG9uc2UuaW52b2ljZU51bWJlcnMpIHtcclxuXHRcdFx0XHRcdGludm9pY2VOdW1iZXJzSHRtbCArPVxyXG5cdFx0XHRcdFx0XHQnPHA+PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIG5hbWU9XCJpbnZvaWNlX2lkc1tdXCIgdmFsdWU9XCInICsgaW52b2ljZUlkXHJcblx0XHRcdFx0XHRcdCsgJ1wiIGNoZWNrZWQ9XCJjaGVja2VkXCIgY2xhc3M9XCJpbnZvaWNlLW51bWJlcnMtY2hlY2tib3hcIiAvPiAnXHJcblx0XHRcdFx0XHRcdCsgcmVzcG9uc2UuaW52b2ljZU51bWJlcnNbaW52b2ljZUlkXSArICc8L3A+JztcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JG1vZGFsLmZpbmQoJy5pbnZvaWNlLW51bWJlcnMtY2hlY2tib3hlcycpLmh0bWwoaW52b2ljZU51bWJlcnNIdG1sKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtYmVycy1jaGVja2JveCcpLm9uKCdjaGFuZ2UnLCBfb25DaGFuZ2VFbWFpbEludm9pY2VDaGVja2JveCk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Z3gud2lkZ2V0cy5pbml0KCRtb2RhbCk7XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIEVtYWlsIEludm9pY2UgQ2hlY2tib3ggQ2hhbmdlXHJcblx0XHQgKlxyXG5cdFx0ICogRGlzYWJsZSBzZW5kIGJ1dHRvbiBpZiBhbGwgaW52b2ljZSBudW1iZXIgY2hlY2tib3hlcyBhcmUgdW5jaGVja2VkLiBPdGhlcndpc2UgZW5hYmxlIHRoZSBzZW5kIGJ1dHRvbiBhZ2Fpbi5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uQ2hhbmdlRW1haWxJbnZvaWNlQ2hlY2tib3goKSB7XHJcblx0XHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5lbWFpbC1pbnZvaWNlLm1vZGFsJyk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoJG1vZGFsLmZpbmQoJy5pbnZvaWNlLW51bWJlcnMtY2hlY2tib3gnKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0aWYgKCRtb2RhbC5maW5kKCcuaW52b2ljZS1udW1iZXJzLWNoZWNrYm94OmNoZWNrZWQnKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLnNlbmQnKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0JG1vZGFsLmZpbmQoJy5zZW5kJykucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0JG1vZGFsLmZpbmQoJy5zZW5kJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBFbWFpbCBPcmRlciBDbGlja1xyXG5cdFx0ICpcclxuXHRcdCAqIERpc3BsYXkgdGhlIGVtYWlsLW9yZGVyIG1vZGFsLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25FbWFpbE9yZGVyQ2xpY2soZXZlbnQpIHtcclxuXHRcdFx0Y29uc3QgJG1vZGFsID0gJCgnLmVtYWlsLW9yZGVyLm1vZGFsJyk7XHJcblx0XHRcdGNvbnN0IHJvd0RhdGEgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgpO1xyXG5cdFx0XHRjb25zdCBkYXRlRm9ybWF0ID0ganNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJykgPT09ICdkZScgPyAnREQuTU0uWVknIDogJ01NL0REL1lZJztcclxuXHRcdFx0XHJcblx0XHRcdCRtb2RhbC5maW5kKCcuY3VzdG9tZXItaW5mbycpLnRleHQoYFwiJHtyb3dEYXRhLmN1c3RvbWVyTmFtZX1cImApO1xyXG5cdFx0XHQkbW9kYWwuZmluZCgnLnN1YmplY3QnKS52YWwoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ09SREVSX1NVQkpFQ1QnLCAnZ21fb3JkZXJfbWVudScpICsgcm93RGF0YS5pZFxyXG5cdFx0XHRcdCsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ09SREVSX1NVQkpFQ1RfRlJPTScsICdnbV9vcmRlcl9tZW51JylcclxuXHRcdFx0XHQrIG1vbWVudChyb3dEYXRhLnB1cmNoYXNlRGF0ZS5kYXRlKS5mb3JtYXQoZGF0ZUZvcm1hdCkpO1xyXG5cdFx0XHQkbW9kYWwuZmluZCgnLmVtYWlsLWFkZHJlc3MnKS52YWwocm93RGF0YS5jdXN0b21lckVtYWlsKTtcclxuXHRcdFx0XHJcblx0XHRcdCRtb2RhbFxyXG5cdFx0XHRcdC5kYXRhKCdvcmRlcklkJywgcm93RGF0YS5pZClcclxuXHRcdFx0XHQubW9kYWwoJ3Nob3cnKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBDaGFuZ2UgT3JkZXIgU3RhdHVzIENsaWNrXHJcblx0XHQgKlxyXG5cdFx0ICogRGlzcGxheSB0aGUgY2hhbmdlIG9yZGVyIHN0YXR1cyBtb2RhbC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uQ2hhbmdlT3JkZXJTdGF0dXNDbGljayhldmVudCkge1xyXG5cdFx0XHRpZiAoJChldmVudC50YXJnZXQpLmhhc0NsYXNzKCdvcmRlci1zdGF0dXMnKSkge1xyXG5cdFx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCAkbW9kYWwgPSAkKCcuc3RhdHVzLm1vZGFsJyk7XHJcblx0XHRcdGNvbnN0IHJvd0RhdGEgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgpO1xyXG5cdFx0XHRjb25zdCBzZWxlY3RlZE9yZGVycyA9IF9nZXRTZWxlY3RlZE9yZGVycygkKHRoaXMpKTtcclxuXHRcdFx0XHJcblx0XHRcdCRtb2RhbC5maW5kKCcjc3RhdHVzLWRyb3Bkb3duJykudmFsKChyb3dEYXRhKSA/IHJvd0RhdGEuc3RhdHVzSWQgOiAnJyk7XHJcblx0XHRcdFxyXG5cdFx0XHQkbW9kYWwuZmluZCgnI2NvbW1lbnQnKS52YWwoJycpO1xyXG5cdFx0XHQkbW9kYWwuZmluZCgnI25vdGlmeS1jdXN0b21lciwgI3NlbmQtcGFyY2VsLXRyYWNraW5nLWNvZGUsICNzZW5kLWNvbW1lbnQnKVxyXG5cdFx0XHRcdC5hdHRyKCdjaGVja2VkJywgZmFsc2UpXHJcblx0XHRcdFx0LnBhcmVudHMoJy5zaW5nbGUtY2hlY2tib3gnKVxyXG5cdFx0XHRcdC5yZW1vdmVDbGFzcygnY2hlY2tlZCcpO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gU2hvdyB0aGUgb3JkZXIgY2hhbmdlIHN0YXR1cyBtb2RhbC5cclxuXHRcdFx0JG1vZGFsLmZpbmQoJy5zZWxlY3RlZC1vcmRlcnMnKS50ZXh0KHNlbGVjdGVkT3JkZXJzLmpvaW4oJywgJykpO1xyXG5cdFx0XHQkbW9kYWwubW9kYWwoJ3Nob3cnKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBBZGQgVHJhY2tpbmcgTnVtYmVyIENsaWNrXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vbkFkZFRyYWNraW5nTnVtYmVyQ2xpY2soZXZlbnQpIHtcclxuXHRcdFx0Y29uc3QgJG1vZGFsID0gJCgnLmFkZC10cmFja2luZy1udW1iZXIubW9kYWwnKTtcclxuXHRcdFx0Y29uc3Qgcm93RGF0YSA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmRhdGEoKTtcclxuXHRcdFx0XHJcblx0XHRcdCRtb2RhbC5kYXRhKCdvcmRlcklkJywgcm93RGF0YS5pZCk7XHJcblx0XHRcdCRtb2RhbC5tb2RhbCgnc2hvdycpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9wZW5zIHRoZSBnbV9wZGZfb3JkZXIucGhwIGluIGEgbmV3IHRhYiB3aXRoIGludm9pY2VzIGFzIHR5cGUgJF9HRVQgYXJndW1lbnQuXHJcblx0XHQgKlxyXG5cdFx0ICogVGhlIG9yZGVyIGlkcyBhcmUgcGFzc2VkIGFzIGEgc2VyaWFsaXplZCBhcnJheSB0byB0aGUgb0lEICRfR0VUIGFyZ3VtZW50LlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25CdWxrRG93bmxvYWRJbnZvaWNlQ2xpY2soKSB7XHJcblx0XHRcdGNvbnN0IG9yZGVySWRzID0gW107XHJcblx0XHRcdGNvbnN0IG1heEFtb3VudEludm9pY2VzQnVsa1BkZiA9IGRhdGEubWF4QW1vdW50SW52b2ljZXNCdWxrUGRmO1xyXG5cdFx0XHRcclxuXHRcdFx0JHRoaXMuZmluZCgndGJvZHkgaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZCcpLmVhY2goZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0b3JkZXJJZHMucHVzaCgkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKSk7XHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKG9yZGVySWRzLmxlbmd0aCA+IG1heEFtb3VudEludm9pY2VzQnVsa1BkZikge1xyXG5cdFx0XHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5idWxrLWVycm9yLm1vZGFsJyk7XHJcblx0XHRcdFx0JG1vZGFsLm1vZGFsKCdzaG93Jyk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y29uc3QgJGludm9pY2VNZXNzYWdlQ29udGFpbmVyID0gJG1vZGFsLmZpbmQoJy5pbnZvaWNlcy1tZXNzYWdlJyk7XHJcblx0XHRcdFx0JGludm9pY2VNZXNzYWdlQ29udGFpbmVyLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcclxuXHRcdFx0XHQkbW9kYWwub24oJ2hpZGUuYnMubW9kYWwnLCAoKSA9PiAkaW52b2ljZU1lc3NhZ2VDb250YWluZXIuYWRkQ2xhc3MoJ2hpZGRlbicpKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdF9jcmVhdGVCdWxrUGRmKG9yZGVySWRzLCAnaW52b2ljZScpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT3BlbnMgdGhlIGdtX3BkZl9vcmRlci5waHAgaW4gYSBuZXcgdGFiIHdpdGggcGFja2luZyBzbGlwIGFzIHR5cGUgJF9HRVQgYXJndW1lbnQuXHJcblx0XHQgKlxyXG5cdFx0ICogVGhlIG9yZGVyIGlkcyBhcmUgcGFzc2VkIGFzIGEgc2VyaWFsaXplZCBhcnJheSB0byB0aGUgb0lEICRfR0VUIGFyZ3VtZW50LlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25CdWxrRG93bmxvYWRQYWNraW5nU2xpcENsaWNrKCkge1xyXG5cdFx0XHRjb25zdCBvcmRlcklkcyA9IFtdO1xyXG5cdFx0XHRjb25zdCBtYXhBbW91bnRQYWNraW5nU2xpcHNCdWxrUGRmID0gZGF0YS5tYXhBbW91bnRQYWNraW5nU2xpcHNCdWxrUGRmO1xyXG5cdFx0XHRsZXQgJG1vZGFsO1xyXG5cdFx0XHRsZXQgJHBhY2tpbmdTbGlwc01lc3NhZ2VDb250YWluZXI7XHJcblx0XHRcdFxyXG5cdFx0XHQkdGhpcy5maW5kKCd0Ym9keSBpbnB1dDpjaGVja2JveDpjaGVja2VkJykuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRvcmRlcklkcy5wdXNoKCQodGhpcykucGFyZW50cygndHInKS5kYXRhKCdpZCcpKTtcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAob3JkZXJJZHMubGVuZ3RoID4gbWF4QW1vdW50UGFja2luZ1NsaXBzQnVsa1BkZikge1xyXG5cdFx0XHRcdCRtb2RhbCA9ICQoJy5idWxrLWVycm9yLm1vZGFsJyk7XHJcblx0XHRcdFx0JG1vZGFsLm1vZGFsKCdzaG93Jyk7XHJcblx0XHRcdFx0JHBhY2tpbmdTbGlwc01lc3NhZ2VDb250YWluZXIgPSAkbW9kYWwuZmluZCgnLnBhY2tpbmctc2xpcHMtbWVzc2FnZScpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdCRwYWNraW5nU2xpcHNNZXNzYWdlQ29udGFpbmVyLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQkbW9kYWwub24oJ2hpZGUuYnMubW9kYWwnLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdCRwYWNraW5nU2xpcHNNZXNzYWdlQ29udGFpbmVyLmFkZENsYXNzKCdoaWRkZW4nKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdF9jcmVhdGVCdWxrUGRmKG9yZGVySWRzLCAncGFja2luZ3NsaXAnKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBDcmVhdGVzIGEgYnVsayBwZGYgd2l0aCBpbnZvaWNlcyBvciBwYWNraW5nIHNsaXBzIGluZm9ybWF0aW9uLlxyXG5cdFx0ICpcclxuXHRcdCAqIFRoaXMgbWV0aG9kIHdpbGwgY2hlY2sgaWYgYWxsIHRoZSBzZWxlY3RlZCBvcmRlcnMgaGF2ZSBhIGRvY3VtZW50IGFuZCBvcGVuIGEgY29uY2F0ZW5hdGVkIFBERiBmaWxlLiBJZiB0aGVyZVxyXG5cdFx0ICogYXJlIG9yZGVycyB0aGF0IGRvIG5vdCBoYXZlIGFueSBkb2N1bWVudCB0aGVuIGEgbW9kYWwgd2lsbCBiZSBzaG93biwgcHJvbXB0aW5nIHRoZSB1c2VyIHRvIGNyZWF0ZSB0aGUgbWlzc2luZ1xyXG5cdFx0ICogZG9jdW1lbnRzIG9yIGNvbnRpbnVlIHdpdGhvdXQgdGhlbS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge051bWJlcltdfSBvcmRlcklkcyBQcm92aWRlIHRoZSBzZWxlY3RlZCBvcmRlciBJRHMuXHJcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gdHlwZSBQcm92aWRlIHRoZSBidWxrIFBERiB0eXBlIFwiaW52b2ljZVwiIG9yIFwicGFja2luZ3NsaXBcIi5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX2NyZWF0ZUJ1bGtQZGYob3JkZXJJZHMsIHR5cGUpIHtcclxuXHRcdFx0aWYgKHR5cGUgIT09ICdpbnZvaWNlJyAmJiB0eXBlICE9PSAncGFja2luZ3NsaXAnKSB7XHJcblx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIHR5cGUgcHJvdmlkZWQ6ICcgKyB0eXBlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3QgdXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocCc7XHJcblx0XHRcdGNvbnN0IGRhdGEgPSB7XHJcblx0XHRcdFx0ZG86ICdPcmRlcnNPdmVydmlld0FqYXgvR2V0T3JkZXJzV2l0aG91dERvY3VtZW50cycsXHJcblx0XHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKSxcclxuXHRcdFx0XHR0eXBlLFxyXG5cdFx0XHRcdG9yZGVySWRzXHJcblx0XHRcdH07XHJcblx0XHRcdFxyXG5cdFx0XHQkLmdldEpTT04odXJsLCBkYXRhKVxyXG5cdFx0XHRcdC5kb25lKG9yZGVySWRzV2l0aG91dERvY3VtZW50ID0+IHtcclxuXHRcdFx0XHRcdGlmIChvcmRlcklkc1dpdGhvdXREb2N1bWVudC5leGNlcHRpb24pIHtcclxuXHRcdFx0XHRcdFx0Y29uc3QgdGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKTtcclxuXHRcdFx0XHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdHRVRfT1JERVJTX1dJVEhPVVRfRE9DVU1FTlRfRVJST1InLCAnYWRtaW5fb3JkZXJzJyk7XHJcblx0XHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKHRpdGxlLCBtZXNzYWdlKTtcclxuXHRcdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRpZiAoIW9yZGVySWRzV2l0aG91dERvY3VtZW50Lmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHRfb3BlbkJ1bGtQZGZVcmwob3JkZXJJZHMsIHR5cGUpOyAvLyBBbGwgdGhlIHNlbGVjdGVkIG9yZGVyIGhhdmUgZG9jdW1lbnRzLiBcclxuXHRcdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBTb21lIG9yZGVycyBkbyBub3QgaGF2ZSBkb2N1bWVudHMsIGRpc3BsYXkgdGhlIGNvbmZpcm1hdGlvbiBtb2RhbC5cclxuXHRcdFx0XHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5tb2RhbC5jcmVhdGUtbWlzc2luZy1kb2N1bWVudHMnKTtcclxuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcub3JkZXItaWRzLWxpc3QnKS50ZXh0KG9yZGVySWRzV2l0aG91dERvY3VtZW50LmpvaW4oJywgJykpO1xyXG5cdFx0XHRcdFx0JG1vZGFsXHJcblx0XHRcdFx0XHRcdC5kYXRhKHtcclxuXHRcdFx0XHRcdFx0XHRvcmRlcklkcyxcclxuXHRcdFx0XHRcdFx0XHRvcmRlcklkc1dpdGhvdXREb2N1bWVudCxcclxuXHRcdFx0XHRcdFx0XHR0eXBlXHJcblx0XHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0XHRcdC5tb2RhbCgnc2hvdycpO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LmZhaWwoKGpxeGhyLCB0ZXh0U3RhdHVzLCBlcnJvclRocm93bikgPT4ge1xyXG5cdFx0XHRcdFx0Y29uc3QgdGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKTtcclxuXHRcdFx0XHRcdGNvbnN0IG1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnR0VUX09SREVSU19XSVRIT1VUX0RPQ1VNRU5UX0VSUk9SJywgJ2FkbWluX29yZGVycycpO1xyXG5cdFx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UodGl0bGUsIG1lc3NhZ2UpO1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIENyZWF0ZSBNaXNzaW5nIERvY3VtZW50cyBQcm9jZWVkIEhhbmRsZXJcclxuXHRcdCAqXHJcblx0XHQgKiBUaGlzIGhhbmRsZXIgd2lsbCBiZSBleGVjdXRlZCB3aGVuZXZlciB0aGUgdXNlciBwcm9jZWVkcyB0aHJvdWdoIHRoZSBcImNyZWF0ZS1taXNzaW5nLWRvY3VtZW50c1wiIG1vZGFsLiBJdCB3aWxsXHJcblx0XHQgKiBiZSByZXNvbHZlZCBldmVuIGlmIHRoZSB1c2VyIGRvZXMgbm90IHNlbGVjdCB0aGUgY2hlY2tib3ggXCJjcmVhdGUtbWlzc2luZy1kb2N1bWVudHNcIi5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdC5cclxuXHRcdCAqIEBwYXJhbSB7TnVtYmVyW119IG9yZGVySWRzIFRoZSBzZWxlY3RlZCBvcmRlcnMgdG8gYmUgaW5jbHVkZWQgaW4gdGhlIFBERi5cclxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlIFdoZXRoZXIgJ2ludm9pY2UnIG9yICdwYWNraW5nc2xpcCcuXHJcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gZG93bmxvYWRQZGZXaW5kb3cgUHJvdmlkZSBhIHdpbmRvdyBoYW5kbGUgZm9yIGJ5cGFzc2luZyBicm93c2VyJ3MgcG9wdXAgYmxvY2tpbmcuXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vbkNyZWF0ZU1pc3NpbmdEb2N1bWVudHNQcm9jZWVkKGV2ZW50LCBvcmRlcklkcywgdHlwZSwgZG93bmxvYWRQZGZXaW5kb3cpIHtcclxuXHRcdFx0X29wZW5CdWxrUGRmVXJsKG9yZGVySWRzLCB0eXBlLCBkb3dubG9hZFBkZldpbmRvdyk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gU2luZ2xlIENoZWNrYm94IFJlYWR5XHJcblx0XHQgKlxyXG5cdFx0ICogVGhpcyBjYWxsYmFjayB3aWxsIHVzZSB0aGUgZXZlbnQuZGF0YS5vcmRlcklkcyB0byBzZXQgdGhlIGNoZWNrZWQgY2hlY2tib3hlcyBhZnRlciBhIHRhYmxlIHJlLXJlbmRlci5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uU2luZ2xlQ2hlY2tib3hSZWFkeShldmVudCkge1xyXG5cdFx0XHRldmVudC5kYXRhLm9yZGVySWRzLmZvckVhY2goaWQgPT4ge1xyXG5cdFx0XHRcdCR0aGlzLmZpbmQoYHRyIyR7aWR9IGlucHV0OmNoZWNrYm94YCkuc2luZ2xlX2NoZWNrYm94KCdjaGVja2VkJywgdHJ1ZSkudHJpZ2dlcignY2hhbmdlJyk7XHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gQnVsayBhY3Rpb24gYnV0dG9uIHNob3VsZCd0IGJlIGRpc2FibGVkIGFmdGVyIGEgZGF0YXRhYmxlIHJlbG9hZC5cclxuXHRcdFx0aWYgKCQoJ3RyIGlucHV0OmNoZWNrYm94OmNoZWNrZWQnKS5sZW5ndGgpIHtcclxuXHRcdFx0XHQkKCcuYnVsay1hY3Rpb24nKS5maW5kKCdidXR0b24nKS5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9wZW5zIHRoZSBVUkwgd2hpY2ggcHJvdmlkZXMgdGhlIGJ1bGsgUERGIGZvciBkb3dubG9hZC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge051bWJlcltdfSBvcmRlcklkcyBUaGUgb3JkZXJzIHRvIGJlIHVzZWQgZm9yIHRoZSBjb25jYXRlbmF0ZWQgZG9jdW1lbnQuXHJcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gdHlwZSBXaGV0aGVyICdpbnZvaWNlJyBvciAncGFja2luZ3NsaXAnLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb3BlbkJ1bGtQZGZVcmwob3JkZXJJZHMsIHR5cGUpIHtcclxuXHRcdFx0Y29uc3QgcGFyYW1ldGVycyA9IHtcclxuXHRcdFx0XHRkbzogJ09yZGVyc01vZGFsc0FqYXgvQnVsa1BkZicgKyAodHlwZSA9PT0gJ2ludm9pY2UnID8gJ0ludm9pY2VzJyA6ICdQYWNraW5nU2xpcHMnKSxcclxuXHRcdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpLFxyXG5cdFx0XHRcdG86IG9yZGVySWRzXHJcblx0XHRcdH07XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCB1cmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwPycgKyAkLnBhcmFtKHBhcmFtZXRlcnMpO1xyXG5cdFx0XHRcclxuXHRcdFx0d2luZG93Lm9wZW4odXJsLCAnX3BhcmVudCcpO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gS2VlcCBjaGVja2JveGVzIGNoZWNrZWQgYWZ0ZXIgYSBkYXRhdGFibGUgcmVsb2FkLlxyXG5cdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5hamF4LnJlbG9hZCgoKSA9PiB7XHJcblx0XHRcdFx0JHRoaXNcclxuXHRcdFx0XHRcdC5vZmYoJ3NpbmdsZV9jaGVja2JveDpyZWFkeScsIF9vblNpbmdsZUNoZWNrYm94UmVhZHkpXHJcblx0XHRcdFx0XHQub24oJ3NpbmdsZV9jaGVja2JveDpyZWFkeScsIHtvcmRlcklkc30sIF9vblNpbmdsZUNoZWNrYm94UmVhZHkpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0JHRoaXMub3JkZXJzX292ZXJ2aWV3X2ZpbHRlcigncmVsb2FkJyk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gUGFja2luZyBTbGlwIENsaWNrXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vblNob3dQYWNraW5nU2xpcENsaWNrKCkge1xyXG5cdFx0XHQvLyBNZXNzYWdlIG1vZGFsIGRhdGEuXHJcblx0XHRcdGNvbnN0IHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RJVExFX1NIT1dfUEFDS0lOR1NMSVAnLCAnb3JkZXJzJyk7XHJcblx0XHRcdGNvbnN0IG1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnTk9fUEFDS0lOR1NMSVBfQVZBSUxBQkxFJywgJ29yZGVycycpO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gUmVxdWVzdCBkYXRhLlxyXG5cdFx0XHRjb25zdCByb3dEYXRhID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoKTtcclxuXHRcdFx0Y29uc3QgdXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocCc7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBSZXF1ZXN0IHBhcmFtZXRlcnMuXHJcblx0XHRcdGNvbnN0IGRhdGEgPSB7XHJcblx0XHRcdFx0aWQ6IHJvd0RhdGEuaWQsXHJcblx0XHRcdFx0ZG86ICdPcmRlcnNPdmVydmlld0FqYXgvR2V0TGF0ZXN0UGFja2luZ1NsaXAnLFxyXG5cdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJylcclxuXHRcdFx0fTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIERpcmVjdGx5IG9wZW4gYSBuZXcgdGFiIChwb3B1cCBibG9ja2VyIHdvcmthcm91bmQpXHJcblx0XHRcdGNvbnN0IG5ld1RhYiA9IHdpbmRvdy5vcGVuKCdhYm91dDpibGFuaycpO1xyXG5cdFx0XHRcclxuXHRcdFx0JC5hamF4KHtkYXRhVHlwZTogJ2pzb24nLCB1cmwsIGRhdGF9KVxyXG5cdFx0XHRcdC5kb25lKHJlc3BvbnNlID0+IHtcclxuXHRcdFx0XHRcdGlmIChyZXNwb25zZS5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdFx0Ly8gR2V0IHRoZSBmaWxlIG5hbWUgZnJvbSB0aGUgcmVzcG9uc2UuXHJcblx0XHRcdFx0XHRcdGNvbnN0IGZpbGVuYW1lID0gcmVzcG9uc2VbMF0uZmlsZTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdC8vIFBhY2tpbmcgc2xpcCBsaW5rIHBhcmFtZXRlcnMuXHJcblx0XHRcdFx0XHRcdGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcblx0XHRcdFx0XHRcdFx0bW9kdWxlOiAnT3JkZXJBZG1pbicsXHJcblx0XHRcdFx0XHRcdFx0YWN0aW9uOiAnc2hvd1BkZicsXHJcblx0XHRcdFx0XHRcdFx0dHlwZTogJ3BhY2tpbmdzbGlwJyxcclxuXHRcdFx0XHRcdFx0XHRmaWxlOiBmaWxlbmFtZVxyXG5cdFx0XHRcdFx0XHR9O1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gT3BlbiBwYWNrYWdlIHNsaXAuXHJcblx0XHRcdFx0XHRcdG5ld1RhYi5sb2NhdGlvbiA9XHJcblx0XHRcdFx0XHRcdFx0YCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2FkbWluL3JlcXVlc3RfcG9ydC5waHA/JHskLnBhcmFtKHBhcmFtZXRlcnMpfWA7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHQvLyBObyBwYWNraW5nIHNsaXAgZm91bmRcclxuXHRcdFx0XHRcdFx0bmV3VGFiLmNsb3NlKCk7XHJcblx0XHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKHRpdGxlLCBtZXNzYWdlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBJbnZvaWNlIENyZWF0ZSBDbGlja1xyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25DcmVhdGVJbnZvaWNlQ2xpY2soKSB7XHJcblx0XHRcdGNvbnN0IGxpbmsgICAgICAgICAgICA9ICQodGhpcykuYXR0cignaHJlZicpO1xyXG5cdFx0XHRjb25zdCAkbG9hZGluZ1NwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcyk7XHJcblx0XHRcdGNvbnN0IHBhZ2VUb2tlbiAgICAgICA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpO1xyXG5cdFx0XHRjb25zdCBvcmRlcklkICAgICAgICAgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgpLmlkO1xyXG5cdFx0XHRjb25zdCB1cmwgICAgICAgICAgICAgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKVxyXG5cdFx0XHRcdCsgYC9hZG1pbi9hZG1pbi5waHA/ZG89T3JkZXJzTW9kYWxzQWpheC9HZXRJbnZvaWNlQ291bnQmcGFnZVRva2VuPSR7cGFnZVRva2VufSZvcmRlcklkPSR7b3JkZXJJZH1gO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gRGlyZWN0bHkgb3BlbiBhIG5ldyB0YWIgKHBvcHVwIGJsb2NrZXIgd29ya2Fyb3VuZClcclxuXHRcdFx0Y29uc3QgbmV3VGFiID0gd2luZG93Lm9wZW4oJ2Fib3V0OmJsYW5rJyk7XHJcblx0XHRcdFxyXG5cdFx0XHRmdW5jdGlvbiBjcmVhdGVJbnZvaWNlKCkge1xyXG5cdFx0XHRcdG5ld1RhYi5sb2NhdGlvbiA9IGxpbms7XHJcblx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQobnVsbCwgZmFsc2UpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRmdW5jdGlvbiBhZGRJbnZvaWNlKCkge1xyXG5cdFx0XHRcdHdpbmRvdy5vcGVuKGxpbmssICdfYmxhbmsnKTtcclxuXHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5hamF4LnJlbG9hZChudWxsLCBmYWxzZSk7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdGZ1bmN0aW9uIG9uUmVxdWVzdFN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRjb25zdCBtb2RhbFRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RJVExFX0NSRUFURV9JTlZPSUNFJywgJ29yZGVycycpO1xyXG5cdFx0XHRcdGNvbnN0IG1vZGFsTWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdURVhUX0NSRUFURV9JTlZPSUNFX0NPTkZJUk1BVElPTicsICdvcmRlcnMnKTtcclxuXHRcdFx0XHRjb25zdCBtb2RhbEJ1dHRvbnMgPSBbXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgneWVzJywgJ2J1dHRvbnMnKSxcclxuXHRcdFx0XHRcdFx0Y2FsbGJhY2s6IGV2ZW50ID0+IHtcclxuXHRcdFx0XHRcdFx0XHRjbG9zZU1vZGFsKGV2ZW50KTtcclxuXHRcdFx0XHRcdFx0XHRhZGRJbnZvaWNlKCk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnbm8nLCAnYnV0dG9ucycpLFxyXG5cdFx0XHRcdFx0XHRjYWxsYmFjazogY2xvc2VNb2RhbFxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdF07XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0ZnVuY3Rpb24gY2xvc2VNb2RhbChldmVudCkge1xyXG5cdFx0XHRcdFx0JChldmVudC50YXJnZXQpLnBhcmVudHMoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5oaWRlKCRsb2FkaW5nU3Bpbm5lcik7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKCFyZXNwb25zZS5jb3VudCkge1xyXG5cdFx0XHRcdFx0Y3JlYXRlSW52b2ljZSgpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRuZXdUYWIuY2xvc2UoKTtcclxuXHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKG1vZGFsVGl0bGUsIG1vZGFsTWVzc2FnZSwgbW9kYWxCdXR0b25zKVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0ZnVuY3Rpb24gb25SZXF1ZXN0RmFpbHVyZSgpIHtcclxuXHRcdFx0XHRqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuaGlkZSgkbG9hZGluZ1NwaW5uZXIpO1xyXG5cdFx0XHRcdGNyZWF0ZUludm9pY2UoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0anNlLmxpYnMueGhyLmdldCh7dXJsfSlcclxuXHRcdFx0XHQuZG9uZShvblJlcXVlc3RTdWNjZXNzKVxyXG5cdFx0XHRcdC5mYWlsKG9uUmVxdWVzdEZhaWx1cmUpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIEludm9pY2UgTGluayBDbGlja1xyXG5cdFx0ICpcclxuXHRcdCAqIFRoZSBzY3JpcHQgdGhhdCBnZW5lcmF0ZXMgdGhlIFBERnMgaXMgY2hhbmdpbmcgdGhlIHN0YXR1cyBvZiBhbiBvcmRlciB0byBcImludm9pY2UtY3JlYXRlZFwiLiBUaHVzIHRoZVxyXG5cdFx0ICogdGFibGUgZGF0YSBuZWVkIHRvIGJlIHJlZHJhd24gYW5kIHRoZSBmaWx0ZXIgb3B0aW9ucyB0byBiZSB1cGRhdGVkLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25TaG93SW52b2ljZUNsaWNrKCkge1xyXG5cdFx0XHQvLyBNZXNzYWdlIG1vZGFsIGRhdGEuXHJcblx0XHRcdGNvbnN0IHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RJVExFX1NIT1dfSU5WT0lDRScsICdvcmRlcnMnKTtcclxuXHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdOT19JTlZPSUNFX0FWQUlMQUJMRScsICdvcmRlcnMnKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIFJlcXVlc3QgZGF0YS5cclxuXHRcdFx0Y29uc3Qgcm93RGF0YSA9ICQodGhpcykucGFyZW50cygndHInKS5kYXRhKCk7XHJcblx0XHRcdGNvbnN0IHVybCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHAnO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gUmVxdWVzdCBwYXJhbWV0ZXJzLlxyXG5cdFx0XHRjb25zdCBkYXRhID0ge1xyXG5cdFx0XHRcdGlkOiByb3dEYXRhLmlkLFxyXG5cdFx0XHRcdGRvOiAnT3JkZXJzT3ZlcnZpZXdBamF4L0dldEludm9pY2VzJyxcclxuXHRcdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpXHJcblx0XHRcdH07XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBEaXJlY3RseSBvcGVuIGEgbmV3IHRhYiAocG9wdXAgYmxvY2tlciB3b3JrYXJvdW5kKVxyXG5cdFx0XHRjb25zdCBuZXdUYWIgPSB3aW5kb3cub3BlbignYWJvdXQ6YmxhbmsnKTtcclxuXHRcdFx0XHJcblx0XHRcdCQuYWpheCh7ZGF0YVR5cGU6ICdqc29uJywgdXJsLCBkYXRhfSlcclxuXHRcdFx0XHQuZG9uZShyZXNwb25zZSA9PiB7XHJcblx0XHRcdFx0XHRpZiAocmVzcG9uc2UubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRcdC8vIEdldCB0aGUgZmlsZSBuYW1lIGZyb20gb2JqZWN0IHdpdGggdGhlIGhpZ2hlc3QgSUQgd2l0aGluIHJlc3BvbnNlIGFycmF5LlxyXG5cdFx0XHRcdFx0XHRjb25zdCB7aW52b2ljZU51bWJlciwgb3JkZXJJZH0gPSByZXNwb25zZVswXTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdC8vIEludm9pY2UgbGluayBwYXJhbWV0ZXJzLlxyXG5cdFx0XHRcdFx0XHRjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG5cdFx0XHRcdFx0XHRcdG1vZHVsZTogJ09yZGVyQWRtaW4nLFxyXG5cdFx0XHRcdFx0XHRcdGFjdGlvbjogJ3Nob3dQZGYnLFxyXG5cdFx0XHRcdFx0XHRcdHR5cGU6ICdpbnZvaWNlJyxcclxuXHRcdFx0XHRcdFx0XHRpbnZvaWNlX251bWJlcjogaW52b2ljZU51bWJlcixcclxuXHRcdFx0XHRcdFx0XHRvcmRlcl9pZDogb3JkZXJJZFxyXG5cdFx0XHRcdFx0XHR9O1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gT3BlbiBpbnZvaWNlXHJcblx0XHRcdFx0XHRcdG5ld1RhYi5sb2NhdGlvbiA9XHJcblx0XHRcdFx0XHRcdFx0YCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2FkbWluL3JlcXVlc3RfcG9ydC5waHA/JHskLnBhcmFtKHBhcmFtZXRlcnMpfWA7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHQvLyBObyBpbnZvaWNlIGZvdW5kXHJcblx0XHRcdFx0XHRcdG5ld1RhYi5jbG9zZSgpO1xyXG5cdFx0XHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZSh0aXRsZSwgbWVzc2FnZSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdFx0Ly8gQmluZCB0YWJsZSByb3cgYWN0aW9ucy5cclxuXHRcdFx0JHRoaXNcclxuXHRcdFx0XHQub24oJ2NsaWNrJywgJ3Rib2R5IHRyJywgX29uVGFibGVSb3dDbGljaylcclxuXHRcdFx0XHQub24oJ2NoYW5nZScsICcuYnVsay1zZWxlY3Rpb24nLCBfb25CdWxrU2VsZWN0aW9uQ2hhbmdlKVxyXG5cdFx0XHRcdC5vbignY2hhbmdlJywgJ2lucHV0OmNoZWNrYm94JywgX29uVGFibGVSb3dDaGVja2JveENoYW5nZSlcclxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5zaG93LWludm9pY2UnLCBfb25TaG93SW52b2ljZUNsaWNrKVxyXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLnNob3ctcGFja2luZy1zbGlwJywgX29uU2hvd1BhY2tpbmdTbGlwQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuY3JlYXRlLWludm9pY2UnLCBfb25DcmVhdGVJbnZvaWNlQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuZW1haWwtaW52b2ljZScsIF9vbkVtYWlsSW52b2ljZUNsaWNrKVxyXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmVtYWlsLW9yZGVyJywgX29uRW1haWxPcmRlckNsaWNrKVxyXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLm9yZGVyLXN0YXR1cy5sYWJlbCcsIF9vbkNoYW5nZU9yZGVyU3RhdHVzQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuYWRkLXRyYWNraW5nLW51bWJlcicsIF9vbkFkZFRyYWNraW5nTnVtYmVyQ2xpY2spO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gQmluZCB0YWJsZSByb3cgYW5kIGJ1bGsgYWN0aW9ucy5cclxuXHRcdFx0JHRoaXMucGFyZW50cygnLm9yZGVycycpXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuYnRuLWdyb3VwIC5jaGFuZ2Utc3RhdHVzJywgX29uQ2hhbmdlT3JkZXJTdGF0dXNDbGljaylcclxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5idG4tZ3JvdXAgLmNhbmNlbCcsIF9vbkNhbmNlbE9yZGVyQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuYnRuLWdyb3VwIC5kZWxldGUsIC5hY3Rpb25zIC5kZWxldGUnLCBfb25EZWxldGVPcmRlckNsaWNrKVxyXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmJ0bi1ncm91cCAuYnVsay1lbWFpbC1vcmRlcicsIF9vbkJ1bGtFbWFpbE9yZGVyQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuYnRuLWdyb3VwIC5idWxrLWVtYWlsLWludm9pY2UnLCBfb25CdWxrRW1haWxJbnZvaWNlQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuYnRuLWdyb3VwIC5idWxrLWRvd25sb2FkLWludm9pY2UnLCBfb25CdWxrRG93bmxvYWRJbnZvaWNlQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuYnRuLWdyb3VwIC5idWxrLWRvd25sb2FkLXBhY2tpbmctc2xpcCcsIF9vbkJ1bGtEb3dubG9hZFBhY2tpbmdTbGlwQ2xpY2spO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gQmluZCBjdXN0b20gZXZlbnRzLlxyXG5cdFx0XHQkKGRvY3VtZW50KS5vbignY3JlYXRlX21pc3NpbmdfZG9jdW1lbnRzOnByb2NlZWQnLCAnLm1vZGFsLmNyZWF0ZS1taXNzaW5nLWRvY3VtZW50cycsXHJcblx0XHRcdFx0X29uQ3JlYXRlTWlzc2luZ0RvY3VtZW50c1Byb2NlZWQpO1xyXG5cdFx0XHRcclxuXHRcdFx0ZG9uZSgpO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0cmV0dXJuIG1vZHVsZTtcclxuXHR9KTtcclxuIl19
