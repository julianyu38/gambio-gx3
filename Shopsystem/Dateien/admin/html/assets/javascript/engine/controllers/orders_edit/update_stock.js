'use strict';

/* --------------------------------------------------------------
 update_stock.js 2018-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
// ------------------------------------------------------------------------
// CONTROLLER NAME
// ------------------------------------------------------------------------
'update_stock',

// ------------------------------------------------------------------------
// CONTROLLER LIBRARIES
// ------------------------------------------------------------------------
[],

// ------------------------------------------------------------------------
// CONTROLLER BUSINESS LOGIC
// ------------------------------------------------------------------------
function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Controller reference.
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default options for controller,
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final controller options.
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module object.
  *
  * @type {{}}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// EVENT HANDLER
	// ------------------------------------------------------------------------

	/**
  * Click handler for the create role button.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _updateStockValue() {
		$('input[name="update_stock"]').not($this).val($this.is(':checked') ? '1' : '0');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	module.init = function (done) {
		$this.on('change', _updateStockValue);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVyc19lZGl0L3VwZGF0ZV9zdG9jay5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl91cGRhdGVTdG9ja1ZhbHVlIiwibm90IiwidmFsIiwiaXMiLCJpbml0Iiwib24iLCJkb25lIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZjtBQUNDO0FBQ0E7QUFDQTtBQUNBLGNBSkQ7O0FBTUM7QUFDQTtBQUNBO0FBQ0EsRUFURDs7QUFXQztBQUNBO0FBQ0E7QUFDQSxVQUFTQyxJQUFULEVBQWU7QUFDZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVcsRUFBakI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUQsU0FBUyxFQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU08saUJBQVQsR0FBNkI7QUFDNUJKLElBQUUsNEJBQUYsRUFBZ0NLLEdBQWhDLENBQW9DTixLQUFwQyxFQUEyQ08sR0FBM0MsQ0FBK0NQLE1BQU1RLEVBQU4sQ0FBUyxVQUFULElBQXNCLEdBQXRCLEdBQTRCLEdBQTNFO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBO0FBQ0FWLFFBQU9XLElBQVAsR0FBYyxnQkFBUTtBQUNyQlQsUUFBTVUsRUFBTixDQUFTLFFBQVQsRUFBbUJMLGlCQUFuQjs7QUFFQU07QUFDQSxFQUpEOztBQU1BLFFBQU9iLE1BQVA7QUFDQSxDQTVFRiIsImZpbGUiOiJvcmRlcnNfZWRpdC91cGRhdGVfc3RvY2suanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHVwZGF0ZV9zdG9jay5qcyAyMDE4LTA1LTI5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gQ09OVFJPTExFUiBOQU1FXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQndXBkYXRlX3N0b2NrJyxcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBDT05UUk9MTEVSIExJQlJBUklFU1xuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0W10sXG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gQ09OVFJPTExFUiBCVVNJTkVTUyBMT0dJQ1xuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDb250cm9sbGVyIHJlZmVyZW5jZS5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmF1bHQgb3B0aW9ucyBmb3IgY29udHJvbGxlcixcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaW5hbCBjb250cm9sbGVyIG9wdGlvbnMuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBvYmplY3QuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7e319XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBNRVRIT0RTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsaWNrIGhhbmRsZXIgZm9yIHRoZSBjcmVhdGUgcm9sZSBidXR0b24uXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdCBjb250YWlucyBpbmZvcm1hdGlvbiBvZiB0aGUgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3VwZGF0ZVN0b2NrVmFsdWUoKSB7XG5cdFx0XHQkKCdpbnB1dFtuYW1lPVwidXBkYXRlX3N0b2NrXCJdJykubm90KCR0aGlzKS52YWwoJHRoaXMuaXMoJzpjaGVja2VkJyk/ICcxJyA6ICcwJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdCR0aGlzLm9uKCdjaGFuZ2UnLCBfdXBkYXRlU3RvY2tWYWx1ZSk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKTsiXX0=
