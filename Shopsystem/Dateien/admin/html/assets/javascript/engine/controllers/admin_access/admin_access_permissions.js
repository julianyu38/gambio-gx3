'use strict';

/* --------------------------------------------------------------
 admin_access_permissions.js 2018-02-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
// ------------------------------------------------------------------------
// CONTROLLER NAME
// ------------------------------------------------------------------------
'admin_access_permissions',

// ------------------------------------------------------------------------
// CONTROLLER LIBRARIES
// ------------------------------------------------------------------------
['modal'],

// ------------------------------------------------------------------------
// CONTROLLER BUSINESS LOGIC
// ------------------------------------------------------------------------
function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Controller reference.
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default options for controller,
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final controller options.
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module object.
  *
  * @type {{}}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Opens a modal with an error message for an unexpected error.
  */
	function _setAllPermissionGrantingSwitcherValues() {
		var $readingAll = $('input.all-permission-checkbox[data-permission-type=reading]');
		var $writingAll = $('input.all-permission-checkbox[data-permission-type=writing]');
		var $deletingAll = $('input.all-permission-checkbox[data-permission-type=deleting]');
		if ($readingAll !== undefined) {
			$readingAll.switcher('checked', $('input.permission-checkbox[data-permission-type=reading]').length === $('input.permission-checkbox[data-permission-type=reading]:checked').length);
		}
		if ($writingAll !== undefined) {
			$writingAll.switcher('checked', $('input.permission-checkbox[data-permission-type=writing]').length === $('input.permission-checkbox[data-permission-type=writing]:checked').length);
		}
		if ($deletingAll !== undefined) {
			$deletingAll.switcher('checked', $('input.permission-checkbox[data-permission-type=deleting]').length === $('input.permission-checkbox[data-permission-type=deleting]:checked').length);
		}
	}

	// ------------------------------------------------------------------------
	// EVENT HANDLER
	// ------------------------------------------------------------------------

	/**
  * Click handler for the permission granting switchers.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _updatePermissionGranting(event) {
		// Set checkbox for parent group or child groups for special conditions
		if ($(this).data('parent-id') > 0 && $(this).is(':checked')) {
			$('input.permission-checkbox[data-group-id=' + $(this).data('parent-id') + '][data-permission-type=' + $(this).data('permission-type') + ']').switcher('checked', true);
		}
		if ($(this).data('group-id') > 0 && $(this).is(':checked') === false) {
			$('input.permission-checkbox[data-parent-id=' + $(this).data('group-id') + '][data-permission-type=' + $(this).data('permission-type') + ']').switcher('checked', false);
		}

		_setAllPermissionGrantingSwitcherValues();
	}

	/**
  * Click handler for the all permission granting switchers.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _updateAllPermissionGranting(event) {
		if ($(this).is(':checked')) {
			$('input.permission-checkbox:not(:checked)').switcher('checked', true);
		} else {
			$('input.permission-checkbox[data-permission-type=' + $(this).data('permission-type') + ']:checked').switcher('checked', false);
		}
	}

	/**
  * Click handler for the group collapse handler.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _collapsePermissionSubGroups(event) {
		var groupId = $(this).data('groupId');
		var $icon = $(this).find('span');

		if ($('li[data-parent-group-id=' + groupId + ']').length) {
			if ($icon.hasClass('fa-minus-square-o')) {
				$('li[data-parent-group-id=' + groupId + ']').hide();
				$icon.removeClass('fa-minus-square-o');
				$icon.addClass('fa-plus-square-o');
			} else {
				$('li[data-parent-group-id=' + groupId + ']').show();
				$icon.addClass('fa-minus-square-o');
				$icon.removeClass('fa-plus-square-o');
			}
			$('li.list-element').css('background-color', '#FFFFFF');
			$('li.list-element:visible:even').css('background-color', '#F9F9F9');
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	module.init = function (done) {
		// initialization logic
		$('input.permission-checkbox').on('change', _updatePermissionGranting);
		$('input.all-permission-checkbox').on('change', _updateAllPermissionGranting);
		$('span.list-element-collapse-handler').on('click', _collapsePermissionSubGroups);
		_setAllPermissionGrantingSwitcherValues();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkbWluX2FjY2Vzcy9hZG1pbl9hY2Nlc3NfcGVybWlzc2lvbnMuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfc2V0QWxsUGVybWlzc2lvbkdyYW50aW5nU3dpdGNoZXJWYWx1ZXMiLCIkcmVhZGluZ0FsbCIsIiR3cml0aW5nQWxsIiwiJGRlbGV0aW5nQWxsIiwidW5kZWZpbmVkIiwic3dpdGNoZXIiLCJsZW5ndGgiLCJfdXBkYXRlUGVybWlzc2lvbkdyYW50aW5nIiwiZXZlbnQiLCJpcyIsIl91cGRhdGVBbGxQZXJtaXNzaW9uR3JhbnRpbmciLCJfY29sbGFwc2VQZXJtaXNzaW9uU3ViR3JvdXBzIiwiZ3JvdXBJZCIsIiRpY29uIiwiZmluZCIsImhhc0NsYXNzIiwiaGlkZSIsInJlbW92ZUNsYXNzIiwiYWRkQ2xhc3MiLCJzaG93IiwiY3NzIiwiaW5pdCIsIm9uIiwiZG9uZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxHQUFHQyxXQUFILENBQWVDLE1BQWY7QUFDQztBQUNBO0FBQ0E7QUFDQSwwQkFKRDs7QUFNQztBQUNBO0FBQ0E7QUFDQSxDQUNDLE9BREQsQ0FURDs7QUFhQztBQUNBO0FBQ0E7QUFDQSxVQUFTQyxJQUFULEVBQWU7QUFDZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVcsRUFBakI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUQsU0FBUyxFQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0EsVUFBU08sdUNBQVQsR0FBbUQ7QUFDbEQsTUFBTUMsY0FBY0wsRUFBRSw2REFBRixDQUFwQjtBQUNBLE1BQU1NLGNBQWNOLEVBQUUsNkRBQUYsQ0FBcEI7QUFDQSxNQUFNTyxlQUFlUCxFQUFFLDhEQUFGLENBQXJCO0FBQ0EsTUFBSUssZ0JBQWdCRyxTQUFwQixFQUErQjtBQUM5QkgsZUFDRUksUUFERixDQUNXLFNBRFgsRUFDc0JULEVBQUUseURBQUYsRUFBNkRVLE1BQTdELEtBQ2hCVixFQUFFLGlFQUFGLEVBQXFFVSxNQUYzRTtBQUdBO0FBQ0QsTUFBSUosZ0JBQWdCRSxTQUFwQixFQUErQjtBQUM5QkYsZUFDRUcsUUFERixDQUNXLFNBRFgsRUFDc0JULEVBQUUseURBQUYsRUFBNkRVLE1BQTdELEtBQ2hCVixFQUFFLGlFQUFGLEVBQXFFVSxNQUYzRTtBQUdBO0FBQ0QsTUFBSUgsaUJBQWlCQyxTQUFyQixFQUFnQztBQUMvQkQsZ0JBQ0VFLFFBREYsQ0FDVyxTQURYLEVBQ3NCVCxFQUFFLDBEQUFGLEVBQThEVSxNQUE5RCxLQUNoQlYsRUFBRSxrRUFBRixFQUFzRVUsTUFGNUU7QUFHQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxVQUFTQyx5QkFBVCxDQUFtQ0MsS0FBbkMsRUFBMEM7QUFDekM7QUFDQSxNQUFJWixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLFdBQWIsSUFBNEIsQ0FBNUIsSUFBaUNFLEVBQUUsSUFBRixFQUFRYSxFQUFSLENBQVcsVUFBWCxDQUFyQyxFQUE2RDtBQUM1RGIsS0FBRSw2Q0FBNkNBLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsV0FBYixDQUE3QyxHQUF5RSx5QkFBekUsR0FDQ0UsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxpQkFBYixDQURELEdBQ21DLEdBRHJDLEVBQzBDVyxRQUQxQyxDQUNtRCxTQURuRCxFQUM4RCxJQUQ5RDtBQUVBO0FBQ0QsTUFBSVQsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxVQUFiLElBQTJCLENBQTNCLElBQWdDRSxFQUFFLElBQUYsRUFBUWEsRUFBUixDQUFXLFVBQVgsTUFBMkIsS0FBL0QsRUFBc0U7QUFDckViLEtBQUUsOENBQThDQSxFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLFVBQWIsQ0FBOUMsR0FBeUUseUJBQXpFLEdBQ0NFLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsaUJBQWIsQ0FERCxHQUNtQyxHQURyQyxFQUMwQ1csUUFEMUMsQ0FDbUQsU0FEbkQsRUFDOEQsS0FEOUQ7QUFFQTs7QUFFREw7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTVSw0QkFBVCxDQUFzQ0YsS0FBdEMsRUFBNkM7QUFDNUMsTUFBSVosRUFBRSxJQUFGLEVBQVFhLEVBQVIsQ0FBVyxVQUFYLENBQUosRUFBNEI7QUFDM0JiLEtBQUUseUNBQUYsRUFDRVMsUUFERixDQUNXLFNBRFgsRUFDc0IsSUFEdEI7QUFFQSxHQUhELE1BR087QUFDTlQsS0FBRSxvREFBb0RBLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsaUJBQWIsQ0FBcEQsR0FBc0YsV0FBeEYsRUFDRVcsUUFERixDQUNXLFNBRFgsRUFDc0IsS0FEdEI7QUFFQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVNNLDRCQUFULENBQXNDSCxLQUF0QyxFQUE2QztBQUM1QyxNQUFNSSxVQUFVaEIsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxTQUFiLENBQWhCO0FBQ0EsTUFBTW1CLFFBQVFqQixFQUFFLElBQUYsRUFBUWtCLElBQVIsQ0FBYSxNQUFiLENBQWQ7O0FBRUEsTUFBSWxCLEVBQUUsNkJBQTZCZ0IsT0FBN0IsR0FBdUMsR0FBekMsRUFBOENOLE1BQWxELEVBQTBEO0FBQ3pELE9BQUlPLE1BQU1FLFFBQU4sQ0FBZSxtQkFBZixDQUFKLEVBQXlDO0FBQ3hDbkIsTUFBRSw2QkFBNkJnQixPQUE3QixHQUF1QyxHQUF6QyxFQUE4Q0ksSUFBOUM7QUFDQUgsVUFBTUksV0FBTixDQUFrQixtQkFBbEI7QUFDQUosVUFBTUssUUFBTixDQUFlLGtCQUFmO0FBQ0EsSUFKRCxNQUlPO0FBQ050QixNQUFFLDZCQUE2QmdCLE9BQTdCLEdBQXVDLEdBQXpDLEVBQThDTyxJQUE5QztBQUNBTixVQUFNSyxRQUFOLENBQWUsbUJBQWY7QUFDQUwsVUFBTUksV0FBTixDQUFrQixrQkFBbEI7QUFDQTtBQUNEckIsS0FBRSxpQkFBRixFQUFxQndCLEdBQXJCLENBQXlCLGtCQUF6QixFQUE2QyxTQUE3QztBQUNBeEIsS0FBRSw4QkFBRixFQUFrQ3dCLEdBQWxDLENBQXNDLGtCQUF0QyxFQUEwRCxTQUExRDtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EzQixRQUFPNEIsSUFBUCxHQUFjLGdCQUFRO0FBQ3JCO0FBQ0F6QixJQUFFLDJCQUFGLEVBQStCMEIsRUFBL0IsQ0FBa0MsUUFBbEMsRUFBNENmLHlCQUE1QztBQUNBWCxJQUFFLCtCQUFGLEVBQW1DMEIsRUFBbkMsQ0FBc0MsUUFBdEMsRUFBZ0RaLDRCQUFoRDtBQUNBZCxJQUFFLG9DQUFGLEVBQXdDMEIsRUFBeEMsQ0FBMkMsT0FBM0MsRUFBb0RYLDRCQUFwRDtBQUNBWDs7QUFFQXVCO0FBQ0EsRUFSRDs7QUFVQSxRQUFPOUIsTUFBUDtBQUNBLENBM0pGIiwiZmlsZSI6ImFkbWluX2FjY2Vzcy9hZG1pbl9hY2Nlc3NfcGVybWlzc2lvbnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGFkbWluX2FjY2Vzc19wZXJtaXNzaW9ucy5qcyAyMDE4LTAyLTA1XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gQ09OVFJPTExFUiBOQU1FXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQnYWRtaW5fYWNjZXNzX3Blcm1pc3Npb25zJyxcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBDT05UUk9MTEVSIExJQlJBUklFU1xuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0W1xuXHRcdCdtb2RhbCcsXG5cdF0sXG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gQ09OVFJPTExFUiBCVVNJTkVTUyBMT0dJQ1xuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDb250cm9sbGVyIHJlZmVyZW5jZS5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmF1bHQgb3B0aW9ucyBmb3IgY29udHJvbGxlcixcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaW5hbCBjb250cm9sbGVyIG9wdGlvbnMuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBvYmplY3QuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7e319XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBNRVRIT0RTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogT3BlbnMgYSBtb2RhbCB3aXRoIGFuIGVycm9yIG1lc3NhZ2UgZm9yIGFuIHVuZXhwZWN0ZWQgZXJyb3IuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3NldEFsbFBlcm1pc3Npb25HcmFudGluZ1N3aXRjaGVyVmFsdWVzKCkge1xuXHRcdFx0Y29uc3QgJHJlYWRpbmdBbGwgPSAkKCdpbnB1dC5hbGwtcGVybWlzc2lvbi1jaGVja2JveFtkYXRhLXBlcm1pc3Npb24tdHlwZT1yZWFkaW5nXScpO1xuXHRcdFx0Y29uc3QgJHdyaXRpbmdBbGwgPSAkKCdpbnB1dC5hbGwtcGVybWlzc2lvbi1jaGVja2JveFtkYXRhLXBlcm1pc3Npb24tdHlwZT13cml0aW5nXScpO1xuXHRcdFx0Y29uc3QgJGRlbGV0aW5nQWxsID0gJCgnaW5wdXQuYWxsLXBlcm1pc3Npb24tY2hlY2tib3hbZGF0YS1wZXJtaXNzaW9uLXR5cGU9ZGVsZXRpbmddJyk7XG5cdFx0XHRpZiAoJHJlYWRpbmdBbGwgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHQkcmVhZGluZ0FsbFxuXHRcdFx0XHRcdC5zd2l0Y2hlcignY2hlY2tlZCcsICQoJ2lucHV0LnBlcm1pc3Npb24tY2hlY2tib3hbZGF0YS1wZXJtaXNzaW9uLXR5cGU9cmVhZGluZ10nKS5sZW5ndGhcblx0XHRcdFx0XHRcdD09PSAkKCdpbnB1dC5wZXJtaXNzaW9uLWNoZWNrYm94W2RhdGEtcGVybWlzc2lvbi10eXBlPXJlYWRpbmddOmNoZWNrZWQnKS5sZW5ndGgpO1xuXHRcdFx0fVxuXHRcdFx0aWYgKCR3cml0aW5nQWxsICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0JHdyaXRpbmdBbGxcblx0XHRcdFx0XHQuc3dpdGNoZXIoJ2NoZWNrZWQnLCAkKCdpbnB1dC5wZXJtaXNzaW9uLWNoZWNrYm94W2RhdGEtcGVybWlzc2lvbi10eXBlPXdyaXRpbmddJykubGVuZ3RoXG5cdFx0XHRcdFx0XHQ9PT0gJCgnaW5wdXQucGVybWlzc2lvbi1jaGVja2JveFtkYXRhLXBlcm1pc3Npb24tdHlwZT13cml0aW5nXTpjaGVja2VkJykubGVuZ3RoKTtcblx0XHRcdH1cblx0XHRcdGlmICgkZGVsZXRpbmdBbGwgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHQkZGVsZXRpbmdBbGxcblx0XHRcdFx0XHQuc3dpdGNoZXIoJ2NoZWNrZWQnLCAkKCdpbnB1dC5wZXJtaXNzaW9uLWNoZWNrYm94W2RhdGEtcGVybWlzc2lvbi10eXBlPWRlbGV0aW5nXScpLmxlbmd0aFxuXHRcdFx0XHRcdFx0PT09ICQoJ2lucHV0LnBlcm1pc3Npb24tY2hlY2tib3hbZGF0YS1wZXJtaXNzaW9uLXR5cGU9ZGVsZXRpbmddOmNoZWNrZWQnKS5sZW5ndGgpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciBmb3IgdGhlIHBlcm1pc3Npb24gZ3JhbnRpbmcgc3dpdGNoZXJzLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF91cGRhdGVQZXJtaXNzaW9uR3JhbnRpbmcoZXZlbnQpIHtcblx0XHRcdC8vIFNldCBjaGVja2JveCBmb3IgcGFyZW50IGdyb3VwIG9yIGNoaWxkIGdyb3VwcyBmb3Igc3BlY2lhbCBjb25kaXRpb25zXG5cdFx0XHRpZiAoJCh0aGlzKS5kYXRhKCdwYXJlbnQtaWQnKSA+IDAgJiYgJCh0aGlzKS5pcygnOmNoZWNrZWQnKSkge1xuXHRcdFx0XHQkKCdpbnB1dC5wZXJtaXNzaW9uLWNoZWNrYm94W2RhdGEtZ3JvdXAtaWQ9JyArICQodGhpcykuZGF0YSgncGFyZW50LWlkJykgKyAnXVtkYXRhLXBlcm1pc3Npb24tdHlwZT0nXG5cdFx0XHRcdFx0KyAkKHRoaXMpLmRhdGEoJ3Blcm1pc3Npb24tdHlwZScpICsgJ10nKS5zd2l0Y2hlcignY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0fVxuXHRcdFx0aWYgKCQodGhpcykuZGF0YSgnZ3JvdXAtaWQnKSA+IDAgJiYgJCh0aGlzKS5pcygnOmNoZWNrZWQnKSA9PT0gZmFsc2UpIHtcblx0XHRcdFx0JCgnaW5wdXQucGVybWlzc2lvbi1jaGVja2JveFtkYXRhLXBhcmVudC1pZD0nICsgJCh0aGlzKS5kYXRhKCdncm91cC1pZCcpICsgJ11bZGF0YS1wZXJtaXNzaW9uLXR5cGU9J1xuXHRcdFx0XHRcdCsgJCh0aGlzKS5kYXRhKCdwZXJtaXNzaW9uLXR5cGUnKSArICddJykuc3dpdGNoZXIoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdF9zZXRBbGxQZXJtaXNzaW9uR3JhbnRpbmdTd2l0Y2hlclZhbHVlcygpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDbGljayBoYW5kbGVyIGZvciB0aGUgYWxsIHBlcm1pc3Npb24gZ3JhbnRpbmcgc3dpdGNoZXJzLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF91cGRhdGVBbGxQZXJtaXNzaW9uR3JhbnRpbmcoZXZlbnQpIHtcblx0XHRcdGlmICgkKHRoaXMpLmlzKCc6Y2hlY2tlZCcpKSB7XG5cdFx0XHRcdCQoJ2lucHV0LnBlcm1pc3Npb24tY2hlY2tib3g6bm90KDpjaGVja2VkKScpXG5cdFx0XHRcdFx0LnN3aXRjaGVyKCdjaGVja2VkJywgdHJ1ZSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQkKCdpbnB1dC5wZXJtaXNzaW9uLWNoZWNrYm94W2RhdGEtcGVybWlzc2lvbi10eXBlPScgKyAkKHRoaXMpLmRhdGEoJ3Blcm1pc3Npb24tdHlwZScpICsgJ106Y2hlY2tlZCcpXG5cdFx0XHRcdFx0LnN3aXRjaGVyKCdjaGVja2VkJywgZmFsc2UpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDbGljayBoYW5kbGVyIGZvciB0aGUgZ3JvdXAgY29sbGFwc2UgaGFuZGxlci5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfY29sbGFwc2VQZXJtaXNzaW9uU3ViR3JvdXBzKGV2ZW50KSB7XG5cdFx0XHRjb25zdCBncm91cElkID0gJCh0aGlzKS5kYXRhKCdncm91cElkJyk7XG5cdFx0XHRjb25zdCAkaWNvbiA9ICQodGhpcykuZmluZCgnc3BhbicpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJCgnbGlbZGF0YS1wYXJlbnQtZ3JvdXAtaWQ9JyArIGdyb3VwSWQgKyAnXScpLmxlbmd0aCkge1xuXHRcdFx0XHRpZiAoJGljb24uaGFzQ2xhc3MoJ2ZhLW1pbnVzLXNxdWFyZS1vJykpIHtcblx0XHRcdFx0XHQkKCdsaVtkYXRhLXBhcmVudC1ncm91cC1pZD0nICsgZ3JvdXBJZCArICddJykuaGlkZSgpO1xuXHRcdFx0XHRcdCRpY29uLnJlbW92ZUNsYXNzKCdmYS1taW51cy1zcXVhcmUtbycpO1xuXHRcdFx0XHRcdCRpY29uLmFkZENsYXNzKCdmYS1wbHVzLXNxdWFyZS1vJyk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JCgnbGlbZGF0YS1wYXJlbnQtZ3JvdXAtaWQ9JyArIGdyb3VwSWQgKyAnXScpLnNob3coKTtcblx0XHRcdFx0XHQkaWNvbi5hZGRDbGFzcygnZmEtbWludXMtc3F1YXJlLW8nKTtcblx0XHRcdFx0XHQkaWNvbi5yZW1vdmVDbGFzcygnZmEtcGx1cy1zcXVhcmUtbycpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdCQoJ2xpLmxpc3QtZWxlbWVudCcpLmNzcygnYmFja2dyb3VuZC1jb2xvcicsICcjRkZGRkZGJyk7XG5cdFx0XHRcdCQoJ2xpLmxpc3QtZWxlbWVudDp2aXNpYmxlOmV2ZW4nKS5jc3MoJ2JhY2tncm91bmQtY29sb3InLCAnI0Y5RjlGOScpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdG1vZHVsZS5pbml0ID0gZG9uZSA9PiB7XG5cdFx0XHQvLyBpbml0aWFsaXphdGlvbiBsb2dpY1xuXHRcdFx0JCgnaW5wdXQucGVybWlzc2lvbi1jaGVja2JveCcpLm9uKCdjaGFuZ2UnLCBfdXBkYXRlUGVybWlzc2lvbkdyYW50aW5nKTtcblx0XHRcdCQoJ2lucHV0LmFsbC1wZXJtaXNzaW9uLWNoZWNrYm94Jykub24oJ2NoYW5nZScsIF91cGRhdGVBbGxQZXJtaXNzaW9uR3JhbnRpbmcpO1xuXHRcdFx0JCgnc3Bhbi5saXN0LWVsZW1lbnQtY29sbGFwc2UtaGFuZGxlcicpLm9uKCdjbGljaycsIF9jb2xsYXBzZVBlcm1pc3Npb25TdWJHcm91cHMpO1xuXHRcdFx0X3NldEFsbFBlcm1pc3Npb25HcmFudGluZ1N3aXRjaGVyVmFsdWVzKCk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKTsiXX0=
