'use strict';

/* --------------------------------------------------------------
 viewport.js 2016-06-14
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('viewport', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Info Row
  *
  * @type {jQuery}
  */
	var $infoRow = $('#main-footer .info.row');

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Checks if the provided dropdown is out of the vertical viewport.
  *
  * @param {jQuery} $dropDownMenu
  *
  * @returns {boolean}
  */
	function _isDropDownOutOfVerticalView($dropDownMenu) {
		var infoRowTopPosition = $infoRow.offset().top;
		var dropDownMenuTopPosition = $dropDownMenu.height() + $dropDownMenu.siblings('.dropdown-toggle').offset().top;

		return dropDownMenuTopPosition > infoRowTopPosition;
	}

	/**
  * Checks if the provided dropdown is out of the horizontal viewport.
  *
  * @param {jQuery} $dropDownMenu
  *
  * @returns {boolean}
  */
	function _isDropDownOutOfHorizontalView($dropDownMenu) {
		var viewportWidth = $('body').width();
		var dropDownMenuLeftPosition = $dropDownMenu.width() + $dropDownMenu.siblings('.dropdown-toggle').prev().offset().left;

		return dropDownMenuLeftPosition > viewportWidth;
	}

	/**
  * Adjust the dropdown position, depending on the current viewport.
  */
	function _adjustDropDownPosition() {

		var $target = $(this);

		var $dropDownMenu = $target.find('.dropdown-menu');

		// Put the dropdown menu above the clicked target,
		// if the menu would touch or even be larger than the info row in the main footer.
		if (_isDropDownOutOfVerticalView($dropDownMenu)) {
			$target.addClass('dropup');
			$target.removeClass('dropdown');
			$target.find('.caret').addClass('caret-reversed');
		} else if ($target.hasClass('dropup')) {
			$target.removeClass('dropup');
			$target.addClass('dropdown');
			$target.find('.caret').removeClass('caret-reversed');
		}

		if (_isDropDownOutOfHorizontalView($dropDownMenu)) {
			$dropDownMenu.addClass('dropdown-menu-right');
		} else if ($target.hasClass('dropdown-menu-right')) {
			$target.removeClass('dropdown-menu-right');
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$('body').on('show.bs.dropdown', '.btn-group.dropdown, .btn-group.dropup', _adjustDropDownPosition);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxheW91dHMvbWFpbi92aWV3cG9ydC5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRpbmZvUm93IiwiX2lzRHJvcERvd25PdXRPZlZlcnRpY2FsVmlldyIsIiRkcm9wRG93bk1lbnUiLCJpbmZvUm93VG9wUG9zaXRpb24iLCJvZmZzZXQiLCJ0b3AiLCJkcm9wRG93bk1lbnVUb3BQb3NpdGlvbiIsImhlaWdodCIsInNpYmxpbmdzIiwiX2lzRHJvcERvd25PdXRPZkhvcml6b250YWxWaWV3Iiwidmlld3BvcnRXaWR0aCIsIndpZHRoIiwiZHJvcERvd25NZW51TGVmdFBvc2l0aW9uIiwicHJldiIsImxlZnQiLCJfYWRqdXN0RHJvcERvd25Qb3NpdGlvbiIsIiR0YXJnZXQiLCJmaW5kIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImhhc0NsYXNzIiwiaW5pdCIsImRvbmUiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0IsVUFBdEIsRUFBa0MsRUFBbEMsRUFBc0MsVUFBU0MsSUFBVCxFQUFlOztBQUVwRDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVMsRUFBZjs7QUFFQTs7Ozs7QUFLQSxLQUFNSSxXQUFXRCxFQUFFLHdCQUFGLENBQWpCOztBQUdBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVNFLDRCQUFULENBQXNDQyxhQUF0QyxFQUFxRDtBQUNwRCxNQUFNQyxxQkFBcUJILFNBQVNJLE1BQVQsR0FBa0JDLEdBQTdDO0FBQ0EsTUFBTUMsMEJBQTBCSixjQUFjSyxNQUFkLEtBQXlCTCxjQUFjTSxRQUFkLENBQXVCLGtCQUF2QixFQUEyQ0osTUFBM0MsR0FBb0RDLEdBQTdHOztBQUVBLFNBQU9DLDBCQUEwQkgsa0JBQWpDO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTTSw4QkFBVCxDQUF3Q1AsYUFBeEMsRUFBdUQ7QUFDdEQsTUFBTVEsZ0JBQWdCWCxFQUFFLE1BQUYsRUFBVVksS0FBVixFQUF0QjtBQUNBLE1BQU1DLDJCQUEyQlYsY0FBY1MsS0FBZCxLQUF3QlQsY0FBY00sUUFBZCxDQUF1QixrQkFBdkIsRUFDdERLLElBRHNELEdBRXREVCxNQUZzRCxHQUU3Q1UsSUFGWjs7QUFJQSxTQUFPRiwyQkFBMkJGLGFBQWxDO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNLLHVCQUFULEdBQW1DOztBQUVsQyxNQUFNQyxVQUFVakIsRUFBRSxJQUFGLENBQWhCOztBQUVBLE1BQUlHLGdCQUFnQmMsUUFBUUMsSUFBUixDQUFhLGdCQUFiLENBQXBCOztBQUVBO0FBQ0E7QUFDQSxNQUFHaEIsNkJBQTZCQyxhQUE3QixDQUFILEVBQWdEO0FBQy9DYyxXQUFRRSxRQUFSLENBQWlCLFFBQWpCO0FBQ0FGLFdBQVFHLFdBQVIsQ0FBb0IsVUFBcEI7QUFDQUgsV0FBUUMsSUFBUixDQUFhLFFBQWIsRUFBdUJDLFFBQXZCLENBQWdDLGdCQUFoQztBQUNBLEdBSkQsTUFJTyxJQUFJRixRQUFRSSxRQUFSLENBQWlCLFFBQWpCLENBQUosRUFBZ0M7QUFDdENKLFdBQVFHLFdBQVIsQ0FBb0IsUUFBcEI7QUFDQUgsV0FBUUUsUUFBUixDQUFpQixVQUFqQjtBQUNBRixXQUFRQyxJQUFSLENBQWEsUUFBYixFQUF1QkUsV0FBdkIsQ0FBbUMsZ0JBQW5DO0FBQ0E7O0FBRUQsTUFBR1YsK0JBQStCUCxhQUEvQixDQUFILEVBQWtEO0FBQ2pEQSxpQkFBY2dCLFFBQWQsQ0FBdUIscUJBQXZCO0FBQ0EsR0FGRCxNQUVPLElBQUlGLFFBQVFJLFFBQVIsQ0FBaUIscUJBQWpCLENBQUosRUFBNkM7QUFDbkRKLFdBQVFHLFdBQVIsQ0FBb0IscUJBQXBCO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O0FBRUF2QixRQUFPeUIsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QnZCLElBQUUsTUFBRixFQUFVd0IsRUFBVixDQUFhLGtCQUFiLEVBQWlDLHdDQUFqQyxFQUEyRVIsdUJBQTNFOztBQUVBTztBQUNBLEVBSkQ7O0FBTUEsUUFBTzFCLE1BQVA7QUFDQSxDQXZHRCIsImZpbGUiOiJsYXlvdXRzL21haW4vdmlld3BvcnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gdmlld3BvcnQuanMgMjAxNi0wNi0xNFxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbmd4LmNvbnRyb2xsZXJzLm1vZHVsZSgndmlld3BvcnQnLCBbXSwgZnVuY3Rpb24oZGF0YSkge1xyXG5cclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIFZBUklBQkxFU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuXHQvKipcclxuXHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cclxuXHQvKipcclxuXHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0ICovXHJcblx0Y29uc3QgbW9kdWxlID0ge307XHJcblxyXG5cdC8qKlxyXG5cdCAqIEluZm8gUm93XHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdCAqL1xyXG5cdGNvbnN0ICRpbmZvUm93ID0gJCgnI21haW4tZm9vdGVyIC5pbmZvLnJvdycpO1xyXG5cclxuXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gRlVOQ1RJT05TXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5cdC8qKlxyXG5cdCAqIENoZWNrcyBpZiB0aGUgcHJvdmlkZWQgZHJvcGRvd24gaXMgb3V0IG9mIHRoZSB2ZXJ0aWNhbCB2aWV3cG9ydC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7alF1ZXJ5fSAkZHJvcERvd25NZW51XHJcblx0ICpcclxuXHQgKiBAcmV0dXJucyB7Ym9vbGVhbn1cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfaXNEcm9wRG93bk91dE9mVmVydGljYWxWaWV3KCRkcm9wRG93bk1lbnUpIHtcclxuXHRcdGNvbnN0IGluZm9Sb3dUb3BQb3NpdGlvbiA9ICRpbmZvUm93Lm9mZnNldCgpLnRvcDtcclxuXHRcdGNvbnN0IGRyb3BEb3duTWVudVRvcFBvc2l0aW9uID0gJGRyb3BEb3duTWVudS5oZWlnaHQoKSArICRkcm9wRG93bk1lbnUuc2libGluZ3MoJy5kcm9wZG93bi10b2dnbGUnKS5vZmZzZXQoKS50b3BcclxuXHRcdFxyXG5cdFx0cmV0dXJuIGRyb3BEb3duTWVudVRvcFBvc2l0aW9uID4gaW5mb1Jvd1RvcFBvc2l0aW9uO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBDaGVja3MgaWYgdGhlIHByb3ZpZGVkIGRyb3Bkb3duIGlzIG91dCBvZiB0aGUgaG9yaXpvbnRhbCB2aWV3cG9ydC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7alF1ZXJ5fSAkZHJvcERvd25NZW51XHJcblx0ICpcclxuXHQgKiBAcmV0dXJucyB7Ym9vbGVhbn1cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfaXNEcm9wRG93bk91dE9mSG9yaXpvbnRhbFZpZXcoJGRyb3BEb3duTWVudSkge1xyXG5cdFx0Y29uc3Qgdmlld3BvcnRXaWR0aCA9ICQoJ2JvZHknKS53aWR0aCgpO1xyXG5cdFx0Y29uc3QgZHJvcERvd25NZW51TGVmdFBvc2l0aW9uID0gJGRyb3BEb3duTWVudS53aWR0aCgpICsgJGRyb3BEb3duTWVudS5zaWJsaW5ncygnLmRyb3Bkb3duLXRvZ2dsZScpXHJcblx0XHRcdFx0LnByZXYoKVxyXG5cdFx0XHRcdC5vZmZzZXQoKS5sZWZ0O1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gZHJvcERvd25NZW51TGVmdFBvc2l0aW9uID4gdmlld3BvcnRXaWR0aDtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIEFkanVzdCB0aGUgZHJvcGRvd24gcG9zaXRpb24sIGRlcGVuZGluZyBvbiB0aGUgY3VycmVudCB2aWV3cG9ydC5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfYWRqdXN0RHJvcERvd25Qb3NpdGlvbigpIHtcclxuXHJcblx0XHRjb25zdCAkdGFyZ2V0ID0gJCh0aGlzKTtcclxuXHJcblx0XHRsZXQgJGRyb3BEb3duTWVudSA9ICR0YXJnZXQuZmluZCgnLmRyb3Bkb3duLW1lbnUnKTtcclxuXHJcblx0XHQvLyBQdXQgdGhlIGRyb3Bkb3duIG1lbnUgYWJvdmUgdGhlIGNsaWNrZWQgdGFyZ2V0LFxyXG5cdFx0Ly8gaWYgdGhlIG1lbnUgd291bGQgdG91Y2ggb3IgZXZlbiBiZSBsYXJnZXIgdGhhbiB0aGUgaW5mbyByb3cgaW4gdGhlIG1haW4gZm9vdGVyLlxyXG5cdFx0aWYoX2lzRHJvcERvd25PdXRPZlZlcnRpY2FsVmlldygkZHJvcERvd25NZW51KSkge1xyXG5cdFx0XHQkdGFyZ2V0LmFkZENsYXNzKCdkcm9wdXAnKTtcclxuXHRcdFx0JHRhcmdldC5yZW1vdmVDbGFzcygnZHJvcGRvd24nKTtcclxuXHRcdFx0JHRhcmdldC5maW5kKCcuY2FyZXQnKS5hZGRDbGFzcygnY2FyZXQtcmV2ZXJzZWQnKTtcclxuXHRcdH0gZWxzZSBpZiAoJHRhcmdldC5oYXNDbGFzcygnZHJvcHVwJykpIHtcclxuXHRcdFx0JHRhcmdldC5yZW1vdmVDbGFzcygnZHJvcHVwJyk7XHJcblx0XHRcdCR0YXJnZXQuYWRkQ2xhc3MoJ2Ryb3Bkb3duJyk7XHJcblx0XHRcdCR0YXJnZXQuZmluZCgnLmNhcmV0JykucmVtb3ZlQ2xhc3MoJ2NhcmV0LXJldmVyc2VkJyk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdGlmKF9pc0Ryb3BEb3duT3V0T2ZIb3Jpem9udGFsVmlldygkZHJvcERvd25NZW51KSkge1xyXG5cdFx0XHQkZHJvcERvd25NZW51LmFkZENsYXNzKCdkcm9wZG93bi1tZW51LXJpZ2h0Jyk7XHJcblx0XHR9IGVsc2UgaWYgKCR0YXJnZXQuaGFzQ2xhc3MoJ2Ryb3Bkb3duLW1lbnUtcmlnaHQnKSkge1xyXG5cdFx0XHQkdGFyZ2V0LnJlbW92ZUNsYXNzKCdkcm9wZG93bi1tZW51LXJpZ2h0Jyk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdCQoJ2JvZHknKS5vbignc2hvdy5icy5kcm9wZG93bicsICcuYnRuLWdyb3VwLmRyb3Bkb3duLCAuYnRuLWdyb3VwLmRyb3B1cCcsIF9hZGp1c3REcm9wRG93blBvc2l0aW9uKTtcclxuXHJcblx0XHRkb25lKCk7XHJcblx0fTtcclxuXHJcblx0cmV0dXJuIG1vZHVsZTtcclxufSk7XHJcbiJdfQ==
