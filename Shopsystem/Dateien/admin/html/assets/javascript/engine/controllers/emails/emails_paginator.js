'use strict';

/* --------------------------------------------------------------
 emails_paginator.js 2017-09-07
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Emails Paginator Controller
 *
 * This controller will handle the main table paginator operations of the admin/emails page.
 *
 * @module Controllers/emails_paginator
 */
gx.controllers.module('emails_paginator', [gx.source + '/libs/emails', gx.source + '/libs/button_dropdown', 'loading_spinner', 'modal'],

/** @lends module:Controllers/emails_paginator */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Table Selector
  *
  * @type {object}
  */
	$table = $('#emails-table'),


	/**
  * Attachments Size Selector
  *
  * @type {object}
  */
	$attachmentsSize = $('#attachments-size'),


	/**
  * Default Module Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Module Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Refresh page data.
  *
  * @param {object} event Contains event information.
  */
	var _onRefreshData = function _onRefreshData(event) {
		$table.DataTable().ajax.reload();
		jse.libs.emails.getAttachmentsSize($attachmentsSize);
	};

	/**
  * Change current page length.
  *
  * @param {object} event Contains the event data.
  */
	var _onTableLengthChange = function _onTableLengthChange(event) {
		var length = $this.find('#display-records').val();
		$table.DataTable().page.len(length).draw();
	};

	/**
  * Open handle attachments modal window.
  *
  * @param {object} event Contains event information.
  */
	var _onHandleAttachments = function _onHandleAttachments(event) {
		var $attachmentsModal = $('#attachments-modal');

		// Reset modal state.
		$attachmentsModal.find('#removal-date').val('').datepicker({
			maxDate: new Date()
		});
		$(document).find('.ui-datepicker').not('.gx-container').addClass('gx-container');

		// Display modal to the user.
		$attachmentsModal.dialog({
			title: jse.core.lang.translate('handle_attachments', 'emails'),
			width: 400,
			modal: true,
			dialogClass: 'gx-container',
			closeOnEscape: true
		});

		$('#close-modal').on('click', function () {
			$($attachmentsModal).dialog('close');
		});
	};

	/**
  * Execute the delete operation for the selected email records.
  *
  * @param {object} event Contains the event information.
  */
	var _onBulkDelete = function _onBulkDelete(event) {
		// Check if there are table rows selected.
		if ($table.find('tr td input:checked').length === 0 || $('#bulk-action').val() === '') {
			return; // No selected records, exit method.
		}

		// Get selected rows data - create a new email collection.
		var collection = jse.libs.emails.getSelectedEmails($table);

		// Display confirmation modal to the user.
		jse.libs.modal.message({
			title: jse.core.lang.translate('bulk_action', 'admin_labels'),
			content: jse.core.lang.translate('prompt_delete_collection', 'emails'),
			buttons: [{
				text: jse.core.lang.translate('no', 'lightbox_buttons'),
				click: function click() {
					$(this).dialog('close');
				}
			}, {
				text: jse.core.lang.translate('yes', 'lightbox_buttons'),
				click: function click() {
					jse.libs.emails.deleteCollection(collection).done(function (response) {
						$table.DataTable().ajax.reload();
						jse.libs.emails.getAttachmentsSize($attachmentsSize);
					}).fail(function (response) {
						var title = jse.core.lang.translate('error', 'messages');

						jse.libs.modal.message({
							title: title,
							content: response.message
						});
					});

					$(this).dialog('close');
					$table.find('input[type=checkbox]').prop('checked', false);
				}
			}]
		});
	};

	/**
  * Execute the send operation for the selected email records.
  *
  * @param {object} event Contains the event information.
  */
	var _onBulkSend = function _onBulkSend(event) {
		// Check if there are table rows selected.
		if ($table.find('tr td input:checked').length === 0 || $('#bulk-action').val() === '') {
			return; // No selected records, exit method.
		}

		// Get selected rows data - create a new email collection.
		var collection = jse.libs.emails.getSelectedEmails($table);

		// Display confirmation modal to the user.
		jse.libs.modal.message({
			title: jse.core.lang.translate('bulk_action', 'admin_labels'),
			content: jse.core.lang.translate('prompt_send_collection', 'emails'),
			buttons: [{
				text: jse.core.lang.translate('no', 'lightbox_buttons'),
				click: function click() {
					$(this).dialog('close');
				}
			}, {
				text: jse.core.lang.translate('yes', 'lightbox_buttons'),
				click: function click() {
					jse.libs.emails.sendCollection(collection).done(function (response) {
						$table.DataTable().ajax.reload();
						jse.libs.emails.getAttachmentsSize($attachmentsSize);
					}).fail(function (response) {
						var title = jse.core.lang.translate('error', 'messages');

						jse.libs.modal.message({
							title: title,
							content: response.message
						});
					});

					$(this).dialog('close');
					$table.find('input[type=checkbox]').prop('checked', false);
				}
			}]
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the module, called by the engine.
  */
	module.init = function (done) {
		// Bind paginator event handlers.
		$this.on('click', '#refresh-table', _onRefreshData).on('change', '#display-records', _onTableLengthChange);
		$('body').on('click', '#handle-attachments', _onHandleAttachments);

		var $dropdown = $this.find('.bulk-action');
		jse.libs.button_dropdown.mapAction($dropdown, 'bulk_send_selected', 'emails', _onBulkSend);
		jse.libs.button_dropdown.mapAction($dropdown, 'bulk_delete_selected', 'emails', _onBulkDelete);

		// Get current attachments size.
		jse.libs.emails.getAttachmentsSize($attachmentsSize);

		done();
	};

	// Return module object to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVtYWlscy9lbWFpbHNfcGFnaW5hdG9yLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJHRhYmxlIiwiJGF0dGFjaG1lbnRzU2l6ZSIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9vblJlZnJlc2hEYXRhIiwiZXZlbnQiLCJEYXRhVGFibGUiLCJhamF4IiwicmVsb2FkIiwianNlIiwibGlicyIsImVtYWlscyIsImdldEF0dGFjaG1lbnRzU2l6ZSIsIl9vblRhYmxlTGVuZ3RoQ2hhbmdlIiwibGVuZ3RoIiwiZmluZCIsInZhbCIsInBhZ2UiLCJsZW4iLCJkcmF3IiwiX29uSGFuZGxlQXR0YWNobWVudHMiLCIkYXR0YWNobWVudHNNb2RhbCIsImRhdGVwaWNrZXIiLCJtYXhEYXRlIiwiRGF0ZSIsImRvY3VtZW50Iiwibm90IiwiYWRkQ2xhc3MiLCJkaWFsb2ciLCJ0aXRsZSIsImNvcmUiLCJsYW5nIiwidHJhbnNsYXRlIiwid2lkdGgiLCJtb2RhbCIsImRpYWxvZ0NsYXNzIiwiY2xvc2VPbkVzY2FwZSIsIm9uIiwiX29uQnVsa0RlbGV0ZSIsImNvbGxlY3Rpb24iLCJnZXRTZWxlY3RlZEVtYWlscyIsIm1lc3NhZ2UiLCJjb250ZW50IiwiYnV0dG9ucyIsInRleHQiLCJjbGljayIsImRlbGV0ZUNvbGxlY3Rpb24iLCJkb25lIiwicmVzcG9uc2UiLCJmYWlsIiwicHJvcCIsIl9vbkJ1bGtTZW5kIiwic2VuZENvbGxlY3Rpb24iLCJpbml0IiwiJGRyb3Bkb3duIiwiYnV0dG9uX2Ryb3Bkb3duIiwibWFwQWN0aW9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msa0JBREQsRUFHQyxDQUNDRixHQUFHRyxNQUFILEdBQVksY0FEYixFQUVDSCxHQUFHRyxNQUFILEdBQVksdUJBRmIsRUFHQyxpQkFIRCxFQUlDLE9BSkQsQ0FIRDs7QUFVQzs7QUFFQSxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsVUFBU0QsRUFBRSxlQUFGLENBYlY7OztBQWVDOzs7OztBQUtBRSxvQkFBbUJGLEVBQUUsbUJBQUYsQ0FwQnBCOzs7QUFzQkM7Ozs7O0FBS0FHLFlBQVcsRUEzQlo7OztBQTZCQzs7Ozs7QUFLQUMsV0FBVUosRUFBRUssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkwsSUFBN0IsQ0FsQ1g7OztBQW9DQzs7Ozs7QUFLQUYsVUFBUyxFQXpDVjs7QUEyQ0E7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLEtBQUlVLGlCQUFpQixTQUFqQkEsY0FBaUIsQ0FBU0MsS0FBVCxFQUFnQjtBQUNwQ04sU0FBT08sU0FBUCxHQUFtQkMsSUFBbkIsQ0FBd0JDLE1BQXhCO0FBQ0FDLE1BQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsa0JBQWhCLENBQW1DWixnQkFBbkM7QUFDQSxFQUhEOztBQUtBOzs7OztBQUtBLEtBQUlhLHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQVNSLEtBQVQsRUFBZ0I7QUFDMUMsTUFBSVMsU0FBU2pCLE1BQU1rQixJQUFOLENBQVcsa0JBQVgsRUFBK0JDLEdBQS9CLEVBQWI7QUFDQWpCLFNBQU9PLFNBQVAsR0FBbUJXLElBQW5CLENBQXdCQyxHQUF4QixDQUE0QkosTUFBNUIsRUFBb0NLLElBQXBDO0FBQ0EsRUFIRDs7QUFLQTs7Ozs7QUFLQSxLQUFJQyx1QkFBdUIsU0FBdkJBLG9CQUF1QixDQUFTZixLQUFULEVBQWdCO0FBQzFDLE1BQUlnQixvQkFBb0J2QixFQUFFLG9CQUFGLENBQXhCOztBQUVBO0FBQ0F1QixvQkFBa0JOLElBQWxCLENBQXVCLGVBQXZCLEVBQXdDQyxHQUF4QyxDQUE0QyxFQUE1QyxFQUFnRE0sVUFBaEQsQ0FBMkQ7QUFDMURDLFlBQVMsSUFBSUMsSUFBSjtBQURpRCxHQUEzRDtBQUdBMUIsSUFBRTJCLFFBQUYsRUFBWVYsSUFBWixDQUFpQixnQkFBakIsRUFBbUNXLEdBQW5DLENBQXVDLGVBQXZDLEVBQXdEQyxRQUF4RCxDQUFpRSxjQUFqRTs7QUFFQTtBQUNBTixvQkFBa0JPLE1BQWxCLENBQXlCO0FBQ3hCQyxVQUFPcEIsSUFBSXFCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG9CQUF4QixFQUE4QyxRQUE5QyxDQURpQjtBQUV4QkMsVUFBTyxHQUZpQjtBQUd4QkMsVUFBTyxJQUhpQjtBQUl4QkMsZ0JBQWEsY0FKVztBQUt4QkMsa0JBQWU7QUFMUyxHQUF6Qjs7QUFRQXRDLElBQUUsY0FBRixFQUFrQnVDLEVBQWxCLENBQXFCLE9BQXJCLEVBQThCLFlBQVk7QUFDekN2QyxLQUFFdUIsaUJBQUYsRUFBcUJPLE1BQXJCLENBQTRCLE9BQTVCO0FBQ0EsR0FGRDtBQUdBLEVBckJEOztBQXVCQTs7Ozs7QUFLQSxLQUFJVSxnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVNqQyxLQUFULEVBQWdCO0FBQ25DO0FBQ0EsTUFBSU4sT0FBT2dCLElBQVAsQ0FBWSxxQkFBWixFQUFtQ0QsTUFBbkMsS0FBOEMsQ0FBOUMsSUFBbURoQixFQUFFLGNBQUYsRUFBa0JrQixHQUFsQixPQUE0QixFQUFuRixFQUF1RjtBQUN0RixVQURzRixDQUM5RTtBQUNSOztBQUVEO0FBQ0EsTUFBSXVCLGFBQWE5QixJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0I2QixpQkFBaEIsQ0FBa0N6QyxNQUFsQyxDQUFqQjs7QUFFQTtBQUNBVSxNQUFJQyxJQUFKLENBQVN3QixLQUFULENBQWVPLE9BQWYsQ0FBdUI7QUFDdEJaLFVBQU9wQixJQUFJcUIsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsYUFBeEIsRUFBdUMsY0FBdkMsQ0FEZTtBQUV0QlUsWUFBU2pDLElBQUlxQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwwQkFBeEIsRUFBb0QsUUFBcEQsQ0FGYTtBQUd0QlcsWUFBUyxDQUNSO0FBQ0NDLFVBQU1uQyxJQUFJcUIsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsSUFBeEIsRUFBOEIsa0JBQTlCLENBRFA7QUFFQ2EsV0FBTyxpQkFBVztBQUNqQi9DLE9BQUUsSUFBRixFQUFROEIsTUFBUixDQUFlLE9BQWY7QUFDQTtBQUpGLElBRFEsRUFPUjtBQUNDZ0IsVUFBTW5DLElBQUlxQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixLQUF4QixFQUErQixrQkFBL0IsQ0FEUDtBQUVDYSxXQUFPLGlCQUFXO0FBQ2pCcEMsU0FBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCbUMsZ0JBQWhCLENBQWlDUCxVQUFqQyxFQUNFUSxJQURGLENBQ08sVUFBU0MsUUFBVCxFQUFtQjtBQUN4QmpELGFBQU9PLFNBQVAsR0FBbUJDLElBQW5CLENBQXdCQyxNQUF4QjtBQUNBQyxVQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLGtCQUFoQixDQUFtQ1osZ0JBQW5DO0FBQ0EsTUFKRixFQUtFaUQsSUFMRixDQUtPLFVBQVNELFFBQVQsRUFBbUI7QUFDeEIsVUFBSW5CLFFBQVFwQixJQUFJcUIsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FBWjs7QUFFQXZCLFVBQUlDLElBQUosQ0FBU3dCLEtBQVQsQ0FBZU8sT0FBZixDQUF1QjtBQUN0QlosY0FBT0EsS0FEZTtBQUV0QmEsZ0JBQVNNLFNBQVNQO0FBRkksT0FBdkI7QUFJQSxNQVpGOztBQWNBM0MsT0FBRSxJQUFGLEVBQVE4QixNQUFSLENBQWUsT0FBZjtBQUNBN0IsWUFBT2dCLElBQVAsQ0FBWSxzQkFBWixFQUFvQ21DLElBQXBDLENBQXlDLFNBQXpDLEVBQW9ELEtBQXBEO0FBQ0E7QUFuQkYsSUFQUTtBQUhhLEdBQXZCO0FBaUNBLEVBM0NEOztBQTZDQTs7Ozs7QUFLQSxLQUFJQyxjQUFjLFNBQWRBLFdBQWMsQ0FBUzlDLEtBQVQsRUFBZ0I7QUFDakM7QUFDQSxNQUFJTixPQUFPZ0IsSUFBUCxDQUFZLHFCQUFaLEVBQW1DRCxNQUFuQyxLQUE4QyxDQUE5QyxJQUFtRGhCLEVBQUUsY0FBRixFQUFrQmtCLEdBQWxCLE9BQTRCLEVBQW5GLEVBQXVGO0FBQ3RGLFVBRHNGLENBQzlFO0FBQ1I7O0FBRUQ7QUFDQSxNQUFJdUIsYUFBYTlCLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQjZCLGlCQUFoQixDQUFrQ3pDLE1BQWxDLENBQWpCOztBQUVBO0FBQ0FVLE1BQUlDLElBQUosQ0FBU3dCLEtBQVQsQ0FBZU8sT0FBZixDQUF1QjtBQUN0QlosVUFBT3BCLElBQUlxQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxjQUF2QyxDQURlO0FBRXRCVSxZQUFTakMsSUFBSXFCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHdCQUF4QixFQUFrRCxRQUFsRCxDQUZhO0FBR3RCVyxZQUFTLENBQ1I7QUFDQ0MsVUFBTW5DLElBQUlxQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixJQUF4QixFQUE4QixrQkFBOUIsQ0FEUDtBQUVDYSxXQUFPLGlCQUFXO0FBQ2pCL0MsT0FBRSxJQUFGLEVBQVE4QixNQUFSLENBQWUsT0FBZjtBQUNBO0FBSkYsSUFEUSxFQU9SO0FBQ0NnQixVQUFNbkMsSUFBSXFCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLEtBQXhCLEVBQStCLGtCQUEvQixDQURQO0FBRUNhLFdBQU8saUJBQVc7QUFDakJwQyxTQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0J5QyxjQUFoQixDQUErQmIsVUFBL0IsRUFDRVEsSUFERixDQUNPLFVBQVNDLFFBQVQsRUFBbUI7QUFDeEJqRCxhQUFPTyxTQUFQLEdBQW1CQyxJQUFuQixDQUF3QkMsTUFBeEI7QUFDQUMsVUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxrQkFBaEIsQ0FBbUNaLGdCQUFuQztBQUNBLE1BSkYsRUFLRWlELElBTEYsQ0FLTyxVQUFTRCxRQUFULEVBQW1CO0FBQ3hCLFVBQUluQixRQUFRcEIsSUFBSXFCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBQVo7O0FBRUF2QixVQUFJQyxJQUFKLENBQVN3QixLQUFULENBQWVPLE9BQWYsQ0FBdUI7QUFDdEJaLGNBQU9BLEtBRGU7QUFFdEJhLGdCQUFTTSxTQUFTUDtBQUZJLE9BQXZCO0FBSUEsTUFaRjs7QUFjQTNDLE9BQUUsSUFBRixFQUFROEIsTUFBUixDQUFlLE9BQWY7QUFDQTdCLFlBQU9nQixJQUFQLENBQVksc0JBQVosRUFBb0NtQyxJQUFwQyxDQUF5QyxTQUF6QyxFQUFvRCxLQUFwRDtBQUNBO0FBbkJGLElBUFE7QUFIYSxHQUF2QjtBQWlDQSxFQTNDRDs7QUE2Q0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQXhELFFBQU8yRCxJQUFQLEdBQWMsVUFBU04sSUFBVCxFQUFlO0FBQzVCO0FBQ0FsRCxRQUNFd0MsRUFERixDQUNLLE9BREwsRUFDYyxnQkFEZCxFQUNnQ2pDLGNBRGhDLEVBRUVpQyxFQUZGLENBRUssUUFGTCxFQUVlLGtCQUZmLEVBRW1DeEIsb0JBRm5DO0FBR0FmLElBQUUsTUFBRixFQUNFdUMsRUFERixDQUNLLE9BREwsRUFDYyxxQkFEZCxFQUNxQ2pCLG9CQURyQzs7QUFHQSxNQUFJa0MsWUFBWXpELE1BQU1rQixJQUFOLENBQVcsY0FBWCxDQUFoQjtBQUNBTixNQUFJQyxJQUFKLENBQVM2QyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ0YsU0FBbkMsRUFBOEMsb0JBQTlDLEVBQW9FLFFBQXBFLEVBQThFSCxXQUE5RTtBQUNBMUMsTUFBSUMsSUFBSixDQUFTNkMsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNGLFNBQW5DLEVBQThDLHNCQUE5QyxFQUFzRSxRQUF0RSxFQUFnRmhCLGFBQWhGOztBQUVBO0FBQ0E3QixNQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLGtCQUFoQixDQUFtQ1osZ0JBQW5DOztBQUVBK0M7QUFDQSxFQWhCRDs7QUFrQkE7QUFDQSxRQUFPckQsTUFBUDtBQUNBLENBbFBGIiwiZmlsZSI6ImVtYWlscy9lbWFpbHNfcGFnaW5hdG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBlbWFpbHNfcGFnaW5hdG9yLmpzIDIwMTctMDktMDdcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIEVtYWlscyBQYWdpbmF0b3IgQ29udHJvbGxlclxuICpcbiAqIFRoaXMgY29udHJvbGxlciB3aWxsIGhhbmRsZSB0aGUgbWFpbiB0YWJsZSBwYWdpbmF0b3Igb3BlcmF0aW9ucyBvZiB0aGUgYWRtaW4vZW1haWxzIHBhZ2UuXG4gKlxuICogQG1vZHVsZSBDb250cm9sbGVycy9lbWFpbHNfcGFnaW5hdG9yXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J2VtYWlsc19wYWdpbmF0b3InLFxuXHRcblx0W1xuXHRcdGd4LnNvdXJjZSArICcvbGlicy9lbWFpbHMnLFxuXHRcdGd4LnNvdXJjZSArICcvbGlicy9idXR0b25fZHJvcGRvd24nLFxuXHRcdCdsb2FkaW5nX3NwaW5uZXInLFxuXHRcdCdtb2RhbCdcblx0XSxcblx0XG5cdC8qKiBAbGVuZHMgbW9kdWxlOkNvbnRyb2xsZXJzL2VtYWlsc19wYWdpbmF0b3IgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFRhYmxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRhYmxlID0gJCgnI2VtYWlscy10YWJsZScpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEF0dGFjaG1lbnRzIFNpemUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkYXR0YWNobWVudHNTaXplID0gJCgnI2F0dGFjaG1lbnRzLXNpemUnKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE1vZHVsZSBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBNb2R1bGUgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlZnJlc2ggcGFnZSBkYXRhLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqL1xuXHRcdHZhciBfb25SZWZyZXNoRGF0YSA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHQkdGFibGUuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQoKTtcblx0XHRcdGpzZS5saWJzLmVtYWlscy5nZXRBdHRhY2htZW50c1NpemUoJGF0dGFjaG1lbnRzU2l6ZSk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDaGFuZ2UgY3VycmVudCBwYWdlIGxlbmd0aC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBDb250YWlucyB0aGUgZXZlbnQgZGF0YS5cblx0XHQgKi9cblx0XHR2YXIgX29uVGFibGVMZW5ndGhDaGFuZ2UgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0dmFyIGxlbmd0aCA9ICR0aGlzLmZpbmQoJyNkaXNwbGF5LXJlY29yZHMnKS52YWwoKTtcblx0XHRcdCR0YWJsZS5EYXRhVGFibGUoKS5wYWdlLmxlbihsZW5ndGgpLmRyYXcoKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9wZW4gaGFuZGxlIGF0dGFjaG1lbnRzIG1vZGFsIHdpbmRvdy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBDb250YWlucyBldmVudCBpbmZvcm1hdGlvbi5cblx0XHQgKi9cblx0XHR2YXIgX29uSGFuZGxlQXR0YWNobWVudHMgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0dmFyICRhdHRhY2htZW50c01vZGFsID0gJCgnI2F0dGFjaG1lbnRzLW1vZGFsJyk7XG5cdFx0XHRcblx0XHRcdC8vIFJlc2V0IG1vZGFsIHN0YXRlLlxuXHRcdFx0JGF0dGFjaG1lbnRzTW9kYWwuZmluZCgnI3JlbW92YWwtZGF0ZScpLnZhbCgnJykuZGF0ZXBpY2tlcih7XG5cdFx0XHRcdG1heERhdGU6IG5ldyBEYXRlKClcblx0XHRcdH0pO1xuXHRcdFx0JChkb2N1bWVudCkuZmluZCgnLnVpLWRhdGVwaWNrZXInKS5ub3QoJy5neC1jb250YWluZXInKS5hZGRDbGFzcygnZ3gtY29udGFpbmVyJyk7XG5cdFx0XHRcblx0XHRcdC8vIERpc3BsYXkgbW9kYWwgdG8gdGhlIHVzZXIuXG5cdFx0XHQkYXR0YWNobWVudHNNb2RhbC5kaWFsb2coe1xuXHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2hhbmRsZV9hdHRhY2htZW50cycsICdlbWFpbHMnKSxcblx0XHRcdFx0d2lkdGg6IDQwMCxcblx0XHRcdFx0bW9kYWw6IHRydWUsXG5cdFx0XHRcdGRpYWxvZ0NsYXNzOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0Y2xvc2VPbkVzY2FwZTogdHJ1ZVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCQoJyNjbG9zZS1tb2RhbCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0JCgkYXR0YWNobWVudHNNb2RhbCkuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBFeGVjdXRlIHRoZSBkZWxldGUgb3BlcmF0aW9uIGZvciB0aGUgc2VsZWN0ZWQgZW1haWwgcmVjb3Jkcy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBDb250YWlucyB0aGUgZXZlbnQgaW5mb3JtYXRpb24uXG5cdFx0ICovXG5cdFx0dmFyIF9vbkJ1bGtEZWxldGUgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0Ly8gQ2hlY2sgaWYgdGhlcmUgYXJlIHRhYmxlIHJvd3Mgc2VsZWN0ZWQuXG5cdFx0XHRpZiAoJHRhYmxlLmZpbmQoJ3RyIHRkIGlucHV0OmNoZWNrZWQnKS5sZW5ndGggPT09IDAgfHwgJCgnI2J1bGstYWN0aW9uJykudmFsKCkgPT09ICcnKSB7XG5cdFx0XHRcdHJldHVybjsgLy8gTm8gc2VsZWN0ZWQgcmVjb3JkcywgZXhpdCBtZXRob2QuXG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIEdldCBzZWxlY3RlZCByb3dzIGRhdGEgLSBjcmVhdGUgYSBuZXcgZW1haWwgY29sbGVjdGlvbi5cblx0XHRcdHZhciBjb2xsZWN0aW9uID0ganNlLmxpYnMuZW1haWxzLmdldFNlbGVjdGVkRW1haWxzKCR0YWJsZSk7XG5cdFx0XHRcblx0XHRcdC8vIERpc3BsYXkgY29uZmlybWF0aW9uIG1vZGFsIHRvIHRoZSB1c2VyLlxuXHRcdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZSh7XG5cdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnYnVsa19hY3Rpb24nLCAnYWRtaW5fbGFiZWxzJyksXG5cdFx0XHRcdGNvbnRlbnQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdwcm9tcHRfZGVsZXRlX2NvbGxlY3Rpb24nLCAnZW1haWxzJyksXG5cdFx0XHRcdGJ1dHRvbnM6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnbm8nLCAnbGlnaHRib3hfYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0Y2xpY2s6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHQkKHRoaXMpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCd5ZXMnLCAnbGlnaHRib3hfYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0Y2xpY2s6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRqc2UubGlicy5lbWFpbHMuZGVsZXRlQ29sbGVjdGlvbihjb2xsZWN0aW9uKVxuXHRcdFx0XHRcdFx0XHRcdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHQkdGFibGUuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQoKTtcblx0XHRcdFx0XHRcdFx0XHRcdGpzZS5saWJzLmVtYWlscy5nZXRBdHRhY2htZW50c1NpemUoJGF0dGFjaG1lbnRzU2l6ZSk7XG5cdFx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdFx0XHQuZmFpbChmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0dmFyIHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLm1lc3NhZ2Uoe1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR0aXRsZTogdGl0bGUsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGNvbnRlbnQ6IHJlc3BvbnNlLm1lc3NhZ2Vcblx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0XHRcdCR0YWJsZS5maW5kKCdpbnB1dFt0eXBlPWNoZWNrYm94XScpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdXG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEV4ZWN1dGUgdGhlIHNlbmQgb3BlcmF0aW9uIGZvciB0aGUgc2VsZWN0ZWQgZW1haWwgcmVjb3Jkcy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBDb250YWlucyB0aGUgZXZlbnQgaW5mb3JtYXRpb24uXG5cdFx0ICovXG5cdFx0dmFyIF9vbkJ1bGtTZW5kID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIENoZWNrIGlmIHRoZXJlIGFyZSB0YWJsZSByb3dzIHNlbGVjdGVkLlxuXHRcdFx0aWYgKCR0YWJsZS5maW5kKCd0ciB0ZCBpbnB1dDpjaGVja2VkJykubGVuZ3RoID09PSAwIHx8ICQoJyNidWxrLWFjdGlvbicpLnZhbCgpID09PSAnJykge1xuXHRcdFx0XHRyZXR1cm47IC8vIE5vIHNlbGVjdGVkIHJlY29yZHMsIGV4aXQgbWV0aG9kLlxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBHZXQgc2VsZWN0ZWQgcm93cyBkYXRhIC0gY3JlYXRlIGEgbmV3IGVtYWlsIGNvbGxlY3Rpb24uXG5cdFx0XHR2YXIgY29sbGVjdGlvbiA9IGpzZS5saWJzLmVtYWlscy5nZXRTZWxlY3RlZEVtYWlscygkdGFibGUpO1xuXHRcdFx0XG5cdFx0XHQvLyBEaXNwbGF5IGNvbmZpcm1hdGlvbiBtb2RhbCB0byB0aGUgdXNlci5cblx0XHRcdGpzZS5saWJzLm1vZGFsLm1lc3NhZ2Uoe1xuXHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2J1bGtfYWN0aW9uJywgJ2FkbWluX2xhYmVscycpLFxuXHRcdFx0XHRjb250ZW50OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgncHJvbXB0X3NlbmRfY29sbGVjdGlvbicsICdlbWFpbHMnKSxcblx0XHRcdFx0YnV0dG9uczogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdubycsICdsaWdodGJveF9idXR0b25zJyksXG5cdFx0XHRcdFx0XHRjbGljazogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3llcycsICdsaWdodGJveF9idXR0b25zJyksXG5cdFx0XHRcdFx0XHRjbGljazogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdGpzZS5saWJzLmVtYWlscy5zZW5kQ29sbGVjdGlvbihjb2xsZWN0aW9uKVxuXHRcdFx0XHRcdFx0XHRcdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHQkdGFibGUuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQoKTtcblx0XHRcdFx0XHRcdFx0XHRcdGpzZS5saWJzLmVtYWlscy5nZXRBdHRhY2htZW50c1NpemUoJGF0dGFjaG1lbnRzU2l6ZSk7XG5cdFx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdFx0XHQuZmFpbChmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0dmFyIHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLm1lc3NhZ2Uoe1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR0aXRsZTogdGl0bGUsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGNvbnRlbnQ6IHJlc3BvbnNlLm1lc3NhZ2Vcblx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0XHRcdCR0YWJsZS5maW5kKCdpbnB1dFt0eXBlPWNoZWNrYm94XScpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdXG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIG1vZHVsZSwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBCaW5kIHBhZ2luYXRvciBldmVudCBoYW5kbGVycy5cblx0XHRcdCR0aGlzXG5cdFx0XHRcdC5vbignY2xpY2snLCAnI3JlZnJlc2gtdGFibGUnLCBfb25SZWZyZXNoRGF0YSlcblx0XHRcdFx0Lm9uKCdjaGFuZ2UnLCAnI2Rpc3BsYXktcmVjb3JkcycsIF9vblRhYmxlTGVuZ3RoQ2hhbmdlKTtcblx0XHRcdCQoJ2JvZHknKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJyNoYW5kbGUtYXR0YWNobWVudHMnLCBfb25IYW5kbGVBdHRhY2htZW50cyk7XG5cdFx0XHRcblx0XHRcdHZhciAkZHJvcGRvd24gPSAkdGhpcy5maW5kKCcuYnVsay1hY3Rpb24nKTtcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5tYXBBY3Rpb24oJGRyb3Bkb3duLCAnYnVsa19zZW5kX3NlbGVjdGVkJywgJ2VtYWlscycsIF9vbkJ1bGtTZW5kKTtcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5tYXBBY3Rpb24oJGRyb3Bkb3duLCAnYnVsa19kZWxldGVfc2VsZWN0ZWQnLCAnZW1haWxzJywgX29uQnVsa0RlbGV0ZSk7XG5cdFx0XHRcblx0XHRcdC8vIEdldCBjdXJyZW50IGF0dGFjaG1lbnRzIHNpemUuXG5cdFx0XHRqc2UubGlicy5lbWFpbHMuZ2V0QXR0YWNobWVudHNTaXplKCRhdHRhY2htZW50c1NpemUpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gbW9kdWxlIG9iamVjdCB0byBtb2R1bGUgZW5naW5lLlxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
