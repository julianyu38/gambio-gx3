'use strict';

/* --------------------------------------------------------------
 emails_table.js 2016-10-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Emails Table Controller
 *
 * This controller will handle the main table operations of the admin/emails page.
 *
 * @module Controllers/emails_table
 */
gx.controllers.module('emails_table', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', gx.source + '/libs/emails', 'modal', 'datatable', 'normalize'],

/** @lends module:Controllers/emails_table */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Modal Selector
  *
  * @type {object}
  */
	$modal = $('#emails-modal'),


	/**
  * Default Module Options
  *
  * @type {object}
  */
	defaults = {
		emailsTableActions: function emailsTableActions() {
			return '<div class="row-actions">' + '<span class="send-email action-item" title="' + jse.core.lang.translate('send', 'buttons') + '">' + '<i class="fa fa-envelope-o"></i>' + '</span>' + '<span class="forward-email action-item" title="' + jse.core.lang.translate('forward', 'buttons') + '">' + '<i class="fa fa-share"></i>' + '</span>' + '<span class="delete-email action-item" title="' + jse.core.lang.translate('delete', 'buttons') + '">' + '<i class="fa fa-trash-o"></i>' + '</span>' + '<span class="preview-email action-item" title="' + jse.core.lang.translate('preview', 'buttons') + '">' + '<i class="fa fa-eye"></i>' + '</span>' + '</div>';
		},

		convertPendingToString: function convertPendingToString(data, type, row, meta) {
			return data === true ? jse.core.lang.translate('email_pending', 'emails') : jse.core.lang.translate('email_sent', 'emails');
		}
	},


	/**
  * Final Module Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Toggle row selection for main page table.
  *
  * @param {object} event Contains event information.
  */
	var _onSelectAllRows = function _onSelectAllRows(event) {
		if ($(this).prop('checked')) {
			$this.find('tbody .single-checkbox').addClass('checked');
			$this.find('tbody input:checkbox').prop('checked', true);
		} else {
			$this.find('tbody .single-checkbox').removeClass('checked');
			$this.find('tbody input:checkbox').prop('checked', false);
		}
	};

	/**
  * Will send the email to its contacts (even if its status is "sent").
  *
  * @param {object} event Contains event information.
  */
	var _onSendEmail = function _onSendEmail(event) {
		var $row = $(this).parents('tr');

		jse.libs.modal.message({
			title: jse.core.lang.translate('send', 'buttons'),
			content: jse.core.lang.translate('prompt_send_email', 'emails'),
			buttons: [{
				text: jse.core.lang.translate('no', 'lightbox_buttons'),
				click: function click() {
					$(this).dialog('close');
				}
			}, {
				text: jse.core.lang.translate('yes', 'lightbox_buttons'),
				click: function click() {
					var email = $row.data();
					jse.libs.emails.sendCollection([email]).done(function (response) {
						$this.DataTable().ajax.reload();
						jse.libs.emails.getAttachmentsSize($('#attachments-size'));
					}).fail(function (response) {
						var title = jse.core.lang.translate('error', 'messages');

						jse.libs.modal.message({
							title: title,
							content: response.message
						});
					});
					$(this).dialog('close');
				}
			}]
		});
	};

	/**
  * Display modal with email information but without contacts.
  *
  * The user will be able to set new contacts and send the email (kind of "duplicate" method).
  *
  * @param {object} event Contains event information.
  */
	var _onForwardEmail = function _onForwardEmail(event) {
		var email = $(this).parents('tr').data();

		jse.libs.emails.resetModal($modal);
		jse.libs.emails.loadEmailOnModal(email, $modal);

		// Clear contact fields but let the rest of the data untouched.
		$modal.find('#email-id').val('');
		$modal.find('#sender-email, #sender-name').val('');
		$modal.find('#reply-to-email, #reply-to-name').val('');
		$modal.find('#recipient-email, #recipient-name').val('');
		$modal.find('#contacts-table').DataTable().clear().draw();

		$modal.dialog({
			title: jse.core.lang.translate('forward', 'buttons'),
			width: 1000,
			height: 800,
			modal: true,
			dialogClass: 'gx-container',
			closeOnEscape: false,
			buttons: jse.libs.emails.getDefaultModalButtons($modal, $this),
			open: jse.libs.emails.colorizeButtonsForEditMode
		});
	};

	/**
  * Delete selected row email.
  *
  * @param {object} event Contains event information.
  */
	var _onDeleteEmail = function _onDeleteEmail(event) {
		var $row = $(this).parents('tr'),
		    email = $row.data();

		jse.libs.modal.message({
			title: jse.core.lang.translate('delete', 'buttons'),
			content: jse.core.lang.translate('prompt_delete_email', 'emails'),
			buttons: [{
				text: jse.core.lang.translate('no', 'lightbox_buttons'),
				click: function click() {
					$(this).dialog('close');
				}
			}, {
				text: jse.core.lang.translate('yes', 'lightbox_buttons'),
				click: function click() {
					jse.libs.emails.deleteCollection([email]).done(function (response) {
						$this.DataTable().ajax.reload();
						jse.libs.emails.getAttachmentsSize($('#attachments-size'));
					}).fail(function (response) {
						var title = jse.core.lang.translate('error', 'messages');
						jse.libs.modal.message({
							title: title,
							content: response.message
						});
					});
					$(this).dialog('close');
				}
			}]
		});
	};

	/**
  * Display modal with email information
  *
  * The user can select an action to perform upon the previewed email (Send, Forward,
  * Delete, Close).
  *
  * @param  {object} event Contains event information.
  */
	var _onPreviewEmail = function _onPreviewEmail(event) {
		var email = $(this).parents('tr').data();

		jse.libs.emails.resetModal($modal);
		jse.libs.emails.loadEmailOnModal(email, $modal);

		$modal.dialog({
			title: jse.core.lang.translate('preview', 'buttons'),
			width: 1000,
			height: 800,
			modal: false,
			dialogClass: 'gx-container',
			closeOnEscape: false,
			buttons: jse.libs.emails.getPreviewModalButtons($modal, $this),
			open: jse.libs.emails.colorizeButtonsForPreviewMode
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the module, called by the engine.
  *
  * The emails table operates with server processing because it is much faster and efficient than preparing
  * and sending all the records in every AJAX request. Check the Emails/DataTable controller method for
  * requested data and the following link for more info about server processing in DataTables.
  *
  * {@link http://www.datatables.net/manual/server-side}
  */
	module.init = function (done) {
		// Create a DataTable instance for the email records.
		jse.libs.datatable.create($this, {
			processing: false,
			serverSide: true,
			dom: 'rtip',
			autoWidth: false,
			language: jse.core.config.get('languageCode') === 'de' ? jse.libs.datatable.getGermanTranslation() : null,
			ajax: {
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=Emails/DataTable',
				type: 'POST'
			},
			order: [[2, 'desc']],
			pageLength: 20,
			columns: [{
				data: null,
				orderable: false,
				defaultContent: '<input type="checkbox" data-single_checkbox />',
				width: '2%',
				className: 'dt-head-center dt-body-center'
			}, {
				data: 'row_count',
				orderable: false,
				width: '3%',
				className: 'dt-head-center dt-body-center'
			}, {
				data: 'creation_date',
				width: '12%'
			}, {
				data: 'sent_date',
				width: '12%'
			}, {
				data: 'sender',
				width: '12%'
			}, {
				data: 'recipient',
				width: '12%'
			}, {
				data: 'subject',
				width: '27%'
			}, {
				data: 'is_pending',
				width: '8%',
				render: options.convertPendingToString
			}, {
				data: null,
				orderable: false,
				defaultContent: '',
				render: options.emailsTableActions,
				width: '12%'
			}]
		});

		// Add table error handler.
		jse.libs.datatable.error($this, function (event, settings, techNote, message) {
			jse.libs.modal.message({
				title: 'DataTables ' + jse.core.lang.translate('error', 'messages'),
				content: message
			});
		});

		// Add ajax error handler.
		jse.libs.datatable.ajaxComplete($this, function (event, settings, json) {
			if (json.exception === true) {
				jse.core.debug.error('DataTables Processing Error', $this.get(0), json);
				jse.libs.modal.message({
					title: 'AJAX ' + jse.core.lang.translate('error', 'messages'),
					content: json.message
				});
			}
		});

		// Combine ".paginator" with the DataTable HTML output in order to create a unique pagination
		// frame at the bottom of the table (executed after table initialization).
		$this.on('init.dt', function (e, settings, json) {
			$('.paginator').appendTo($('#emails-table_wrapper'));
			$('#emails-table_info').appendTo($('.paginator .datatable-components')).css('clear', 'none');
			$('#emails-table_paginate').appendTo($('.paginator .datatable-components')).css('clear', 'none');
		});

		// Recreate the checkbox widgets.
		$this.on('draw.dt', function () {
			$this.find('tbody').attr('data-gx-widget', 'checkbox');
			gx.widgets.init($this); // Initialize the checkbox widget.
		});

		// Add spinner to table loading actions.
		var $spinner;
		$this.on('preXhr.dt', function (e, settings, json) {
			$spinner = jse.libs.loading_spinner.show($this);
		});
		$this.on('xhr.dt', function (e, settings, json) {
			if ($spinner) {
				jse.libs.loading_spinner.hide($spinner);
			}
		});

		// Bind event handlers of the emails table.
		$this.on('click', '#select-all-rows', _onSelectAllRows).on('click', '.send-email', _onSendEmail).on('click', '.forward-email', _onForwardEmail).on('click', '.delete-email', _onDeleteEmail).on('click', '.preview-email', _onPreviewEmail);

		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVtYWlscy9lbWFpbHNfdGFibGUuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkbW9kYWwiLCJkZWZhdWx0cyIsImVtYWlsc1RhYmxlQWN0aW9ucyIsImNvcmUiLCJsYW5nIiwidHJhbnNsYXRlIiwiY29udmVydFBlbmRpbmdUb1N0cmluZyIsInR5cGUiLCJyb3ciLCJtZXRhIiwib3B0aW9ucyIsImV4dGVuZCIsIl9vblNlbGVjdEFsbFJvd3MiLCJldmVudCIsInByb3AiLCJmaW5kIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsIl9vblNlbmRFbWFpbCIsIiRyb3ciLCJwYXJlbnRzIiwibGlicyIsIm1vZGFsIiwibWVzc2FnZSIsInRpdGxlIiwiY29udGVudCIsImJ1dHRvbnMiLCJ0ZXh0IiwiY2xpY2siLCJkaWFsb2ciLCJlbWFpbCIsImVtYWlscyIsInNlbmRDb2xsZWN0aW9uIiwiZG9uZSIsInJlc3BvbnNlIiwiRGF0YVRhYmxlIiwiYWpheCIsInJlbG9hZCIsImdldEF0dGFjaG1lbnRzU2l6ZSIsImZhaWwiLCJfb25Gb3J3YXJkRW1haWwiLCJyZXNldE1vZGFsIiwibG9hZEVtYWlsT25Nb2RhbCIsInZhbCIsImNsZWFyIiwiZHJhdyIsIndpZHRoIiwiaGVpZ2h0IiwiZGlhbG9nQ2xhc3MiLCJjbG9zZU9uRXNjYXBlIiwiZ2V0RGVmYXVsdE1vZGFsQnV0dG9ucyIsIm9wZW4iLCJjb2xvcml6ZUJ1dHRvbnNGb3JFZGl0TW9kZSIsIl9vbkRlbGV0ZUVtYWlsIiwiZGVsZXRlQ29sbGVjdGlvbiIsIl9vblByZXZpZXdFbWFpbCIsImdldFByZXZpZXdNb2RhbEJ1dHRvbnMiLCJjb2xvcml6ZUJ1dHRvbnNGb3JQcmV2aWV3TW9kZSIsImluaXQiLCJkYXRhdGFibGUiLCJjcmVhdGUiLCJwcm9jZXNzaW5nIiwic2VydmVyU2lkZSIsImRvbSIsImF1dG9XaWR0aCIsImxhbmd1YWdlIiwiY29uZmlnIiwiZ2V0IiwiZ2V0R2VybWFuVHJhbnNsYXRpb24iLCJ1cmwiLCJvcmRlciIsInBhZ2VMZW5ndGgiLCJjb2x1bW5zIiwib3JkZXJhYmxlIiwiZGVmYXVsdENvbnRlbnQiLCJjbGFzc05hbWUiLCJyZW5kZXIiLCJlcnJvciIsInNldHRpbmdzIiwidGVjaE5vdGUiLCJhamF4Q29tcGxldGUiLCJqc29uIiwiZXhjZXB0aW9uIiwiZGVidWciLCJvbiIsImUiLCJhcHBlbmRUbyIsImNzcyIsImF0dHIiLCJ3aWRnZXRzIiwiJHNwaW5uZXIiLCJsb2FkaW5nX3NwaW5uZXIiLCJzaG93IiwiaGlkZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLGNBREQsRUFHQyxDQUNDQyxJQUFJQyxNQUFKLEdBQWEsOENBRGQsRUFFQ0QsSUFBSUMsTUFBSixHQUFhLDZDQUZkLEVBR0NKLEdBQUdJLE1BQUgsR0FBWSxjQUhiLEVBSUMsT0FKRCxFQUtDLFdBTEQsRUFNQyxXQU5ELENBSEQ7O0FBWUM7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFVBQVNELEVBQUUsZUFBRixDQWJWOzs7QUFlQzs7Ozs7QUFLQUUsWUFBVztBQUNWQyxzQkFBb0IsOEJBQVc7QUFDOUIsVUFBTyw4QkFBOEIsOENBQTlCLEdBQ0pQLElBQUlRLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQ0QsTUFEQyxFQUVELFNBRkMsQ0FESSxHQUdRLElBSFIsR0FHZSxrQ0FIZixHQUdvRCxTQUhwRCxHQUlOLGlEQUpNLEdBS0pWLElBQUlRLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFNBQXhCLEVBQW1DLFNBQW5DLENBTEksR0FNTixJQU5NLEdBT04sNkJBUE0sR0FPMEIsU0FQMUIsR0FRTixnREFSTSxHQVE2Q1YsSUFBSVEsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FDbEQsUUFEa0QsRUFDeEMsU0FEd0MsQ0FSN0MsR0FTa0IsSUFUbEIsR0FTeUIsK0JBVHpCLEdBUzJELFNBVDNELEdBVU4saURBVk0sR0FXSlYsSUFBSVEsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsU0FBeEIsRUFBbUMsU0FBbkMsQ0FYSSxHQVlOLElBWk0sR0FhTiwyQkFiTSxHQWF3QixTQWJ4QixHQWFvQyxRQWIzQztBQWNBLEdBaEJTOztBQWtCVkMsMEJBQXdCLGdDQUFTVCxJQUFULEVBQWVVLElBQWYsRUFBcUJDLEdBQXJCLEVBQTBCQyxJQUExQixFQUFnQztBQUN2RCxVQUFRWixTQUNKLElBREcsR0FDS0YsSUFBSVEsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsZUFBeEIsRUFBeUMsUUFBekMsQ0FETCxHQUMwRFYsSUFBSVEsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FDaEUsWUFEZ0UsRUFDbEQsUUFEa0QsQ0FEakU7QUFHQTtBQXRCUyxFQXBCWjs7O0FBNkNDOzs7OztBQUtBSyxXQUFVWCxFQUFFWSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJWLFFBQW5CLEVBQTZCSixJQUE3QixDQWxEWDs7O0FBb0RDOzs7OztBQUtBSCxVQUFTLEVBekRWOztBQTJEQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsS0FBSWtCLG1CQUFtQixTQUFuQkEsZ0JBQW1CLENBQVNDLEtBQVQsRUFBZ0I7QUFDdEMsTUFBSWQsRUFBRSxJQUFGLEVBQVFlLElBQVIsQ0FBYSxTQUFiLENBQUosRUFBNkI7QUFDNUJoQixTQUFNaUIsSUFBTixDQUFXLHdCQUFYLEVBQXFDQyxRQUFyQyxDQUE4QyxTQUE5QztBQUNBbEIsU0FBTWlCLElBQU4sQ0FBVyxzQkFBWCxFQUFtQ0QsSUFBbkMsQ0FBd0MsU0FBeEMsRUFBbUQsSUFBbkQ7QUFDQSxHQUhELE1BR087QUFDTmhCLFNBQU1pQixJQUFOLENBQVcsd0JBQVgsRUFBcUNFLFdBQXJDLENBQWlELFNBQWpEO0FBQ0FuQixTQUFNaUIsSUFBTixDQUFXLHNCQUFYLEVBQW1DRCxJQUFuQyxDQUF3QyxTQUF4QyxFQUFtRCxLQUFuRDtBQUNBO0FBQ0QsRUFSRDs7QUFVQTs7Ozs7QUFLQSxLQUFJSSxlQUFlLFNBQWZBLFlBQWUsQ0FBU0wsS0FBVCxFQUFnQjtBQUNsQyxNQUFJTSxPQUFPcEIsRUFBRSxJQUFGLEVBQVFxQixPQUFSLENBQWdCLElBQWhCLENBQVg7O0FBRUF6QixNQUFJMEIsSUFBSixDQUFTQyxLQUFULENBQWVDLE9BQWYsQ0FBdUI7QUFDdEJDLFVBQU83QixJQUFJUSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixNQUF4QixFQUFnQyxTQUFoQyxDQURlO0FBRXRCb0IsWUFBUzlCLElBQUlRLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1CQUF4QixFQUE2QyxRQUE3QyxDQUZhO0FBR3RCcUIsWUFBUyxDQUNSO0FBQ0NDLFVBQU1oQyxJQUFJUSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixJQUF4QixFQUE4QixrQkFBOUIsQ0FEUDtBQUVDdUIsV0FBTyxpQkFBVztBQUNqQjdCLE9BQUUsSUFBRixFQUFROEIsTUFBUixDQUFlLE9BQWY7QUFDQTtBQUpGLElBRFEsRUFPUjtBQUNDRixVQUFNaEMsSUFBSVEsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsS0FBeEIsRUFBK0Isa0JBQS9CLENBRFA7QUFFQ3VCLFdBQU8saUJBQVc7QUFDakIsU0FBSUUsUUFBUVgsS0FBS3RCLElBQUwsRUFBWjtBQUNBRixTQUFJMEIsSUFBSixDQUFTVSxNQUFULENBQWdCQyxjQUFoQixDQUErQixDQUFDRixLQUFELENBQS9CLEVBQ0VHLElBREYsQ0FDTyxVQUFTQyxRQUFULEVBQW1CO0FBQ3hCcEMsWUFBTXFDLFNBQU4sR0FBa0JDLElBQWxCLENBQXVCQyxNQUF2QjtBQUNBMUMsVUFBSTBCLElBQUosQ0FBU1UsTUFBVCxDQUFnQk8sa0JBQWhCLENBQW1DdkMsRUFBRSxtQkFBRixDQUFuQztBQUNBLE1BSkYsRUFLRXdDLElBTEYsQ0FLTyxVQUFTTCxRQUFULEVBQW1CO0FBQ3hCLFVBQUlWLFFBQVE3QixJQUFJUSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQUFaOztBQUVBVixVQUFJMEIsSUFBSixDQUFTQyxLQUFULENBQWVDLE9BQWYsQ0FBdUI7QUFDdEJDLGNBQU9BLEtBRGU7QUFFdEJDLGdCQUFTUyxTQUFTWDtBQUZJLE9BQXZCO0FBSUEsTUFaRjtBQWFBeEIsT0FBRSxJQUFGLEVBQVE4QixNQUFSLENBQWUsT0FBZjtBQUNBO0FBbEJGLElBUFE7QUFIYSxHQUF2QjtBQWdDQSxFQW5DRDs7QUFxQ0E7Ozs7Ozs7QUFPQSxLQUFJVyxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQVMzQixLQUFULEVBQWdCO0FBQ3JDLE1BQUlpQixRQUFRL0IsRUFBRSxJQUFGLEVBQVFxQixPQUFSLENBQWdCLElBQWhCLEVBQXNCdkIsSUFBdEIsRUFBWjs7QUFFQUYsTUFBSTBCLElBQUosQ0FBU1UsTUFBVCxDQUFnQlUsVUFBaEIsQ0FBMkJ6QyxNQUEzQjtBQUNBTCxNQUFJMEIsSUFBSixDQUFTVSxNQUFULENBQWdCVyxnQkFBaEIsQ0FBaUNaLEtBQWpDLEVBQXdDOUIsTUFBeEM7O0FBRUE7QUFDQUEsU0FBT2UsSUFBUCxDQUFZLFdBQVosRUFBeUI0QixHQUF6QixDQUE2QixFQUE3QjtBQUNBM0MsU0FBT2UsSUFBUCxDQUFZLDZCQUFaLEVBQTJDNEIsR0FBM0MsQ0FBK0MsRUFBL0M7QUFDQTNDLFNBQU9lLElBQVAsQ0FBWSxpQ0FBWixFQUErQzRCLEdBQS9DLENBQW1ELEVBQW5EO0FBQ0EzQyxTQUFPZSxJQUFQLENBQVksbUNBQVosRUFBaUQ0QixHQUFqRCxDQUFxRCxFQUFyRDtBQUNBM0MsU0FBT2UsSUFBUCxDQUFZLGlCQUFaLEVBQStCb0IsU0FBL0IsR0FBMkNTLEtBQTNDLEdBQW1EQyxJQUFuRDs7QUFFQTdDLFNBQU82QixNQUFQLENBQWM7QUFDYkwsVUFBTzdCLElBQUlRLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFNBQXhCLEVBQW1DLFNBQW5DLENBRE07QUFFYnlDLFVBQU8sSUFGTTtBQUdiQyxXQUFRLEdBSEs7QUFJYnpCLFVBQU8sSUFKTTtBQUtiMEIsZ0JBQWEsY0FMQTtBQU1iQyxrQkFBZSxLQU5GO0FBT2J2QixZQUFTL0IsSUFBSTBCLElBQUosQ0FBU1UsTUFBVCxDQUFnQm1CLHNCQUFoQixDQUF1Q2xELE1BQXZDLEVBQStDRixLQUEvQyxDQVBJO0FBUWJxRCxTQUFNeEQsSUFBSTBCLElBQUosQ0FBU1UsTUFBVCxDQUFnQnFCO0FBUlQsR0FBZDtBQVVBLEVBdkJEOztBQXlCQTs7Ozs7QUFLQSxLQUFJQyxpQkFBaUIsU0FBakJBLGNBQWlCLENBQVN4QyxLQUFULEVBQWdCO0FBQ3BDLE1BQUlNLE9BQU9wQixFQUFFLElBQUYsRUFBUXFCLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBWDtBQUFBLE1BQ0NVLFFBQVFYLEtBQUt0QixJQUFMLEVBRFQ7O0FBR0FGLE1BQUkwQixJQUFKLENBQVNDLEtBQVQsQ0FBZUMsT0FBZixDQUF1QjtBQUN0QkMsVUFBTzdCLElBQUlRLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFFBQXhCLEVBQWtDLFNBQWxDLENBRGU7QUFFdEJvQixZQUFTOUIsSUFBSVEsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLFFBQS9DLENBRmE7QUFHdEJxQixZQUFTLENBQ1I7QUFDQ0MsVUFBTWhDLElBQUlRLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLElBQXhCLEVBQThCLGtCQUE5QixDQURQO0FBRUN1QixXQUFPLGlCQUFXO0FBQ2pCN0IsT0FBRSxJQUFGLEVBQVE4QixNQUFSLENBQWUsT0FBZjtBQUNBO0FBSkYsSUFEUSxFQU9SO0FBQ0NGLFVBQU1oQyxJQUFJUSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixLQUF4QixFQUErQixrQkFBL0IsQ0FEUDtBQUVDdUIsV0FBTyxpQkFBVztBQUNqQmpDLFNBQUkwQixJQUFKLENBQVNVLE1BQVQsQ0FBZ0J1QixnQkFBaEIsQ0FBaUMsQ0FBQ3hCLEtBQUQsQ0FBakMsRUFDRUcsSUFERixDQUNPLFVBQVNDLFFBQVQsRUFBbUI7QUFDeEJwQyxZQUFNcUMsU0FBTixHQUFrQkMsSUFBbEIsQ0FBdUJDLE1BQXZCO0FBQ0ExQyxVQUFJMEIsSUFBSixDQUFTVSxNQUFULENBQWdCTyxrQkFBaEIsQ0FBbUN2QyxFQUFFLG1CQUFGLENBQW5DO0FBQ0EsTUFKRixFQUtFd0MsSUFMRixDQUtPLFVBQVNMLFFBQVQsRUFBbUI7QUFDeEIsVUFBSVYsUUFBUTdCLElBQUlRLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBQVo7QUFDQVYsVUFBSTBCLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxPQUFmLENBQXVCO0FBQ3RCQyxjQUFPQSxLQURlO0FBRXRCQyxnQkFBU1MsU0FBU1g7QUFGSSxPQUF2QjtBQUlBLE1BWEY7QUFZQXhCLE9BQUUsSUFBRixFQUFROEIsTUFBUixDQUFlLE9BQWY7QUFDQTtBQWhCRixJQVBRO0FBSGEsR0FBdkI7QUE4QkEsRUFsQ0Q7O0FBb0NBOzs7Ozs7OztBQVFBLEtBQUkwQixrQkFBa0IsU0FBbEJBLGVBQWtCLENBQVMxQyxLQUFULEVBQWdCO0FBQ3JDLE1BQUlpQixRQUFRL0IsRUFBRSxJQUFGLEVBQVFxQixPQUFSLENBQWdCLElBQWhCLEVBQXNCdkIsSUFBdEIsRUFBWjs7QUFFQUYsTUFBSTBCLElBQUosQ0FBU1UsTUFBVCxDQUFnQlUsVUFBaEIsQ0FBMkJ6QyxNQUEzQjtBQUNBTCxNQUFJMEIsSUFBSixDQUFTVSxNQUFULENBQWdCVyxnQkFBaEIsQ0FBaUNaLEtBQWpDLEVBQXdDOUIsTUFBeEM7O0FBRUFBLFNBQU82QixNQUFQLENBQWM7QUFDYkwsVUFBTzdCLElBQUlRLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFNBQXhCLEVBQW1DLFNBQW5DLENBRE07QUFFYnlDLFVBQU8sSUFGTTtBQUdiQyxXQUFRLEdBSEs7QUFJYnpCLFVBQU8sS0FKTTtBQUtiMEIsZ0JBQWEsY0FMQTtBQU1iQyxrQkFBZSxLQU5GO0FBT2J2QixZQUFTL0IsSUFBSTBCLElBQUosQ0FBU1UsTUFBVCxDQUFnQnlCLHNCQUFoQixDQUF1Q3hELE1BQXZDLEVBQStDRixLQUEvQyxDQVBJO0FBUWJxRCxTQUFNeEQsSUFBSTBCLElBQUosQ0FBU1UsTUFBVCxDQUFnQjBCO0FBUlQsR0FBZDtBQVVBLEVBaEJEOztBQWtCQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7OztBQVNBL0QsUUFBT2dFLElBQVAsR0FBYyxVQUFTekIsSUFBVCxFQUFlO0FBQzVCO0FBQ0F0QyxNQUFJMEIsSUFBSixDQUFTc0MsU0FBVCxDQUFtQkMsTUFBbkIsQ0FBMEI5RCxLQUExQixFQUFpQztBQUNoQytELGVBQVksS0FEb0I7QUFFaENDLGVBQVksSUFGb0I7QUFHaENDLFFBQUssTUFIMkI7QUFJaENDLGNBQVcsS0FKcUI7QUFLaENDLGFBQVd0RSxJQUFJUSxJQUFKLENBQVMrRCxNQUFULENBQWdCQyxHQUFoQixDQUFvQixjQUFwQixNQUNQLElBRE0sR0FDRXhFLElBQUkwQixJQUFKLENBQVNzQyxTQUFULENBQW1CUyxvQkFBbkIsRUFERixHQUM4QyxJQU54QjtBQU9oQ2hDLFNBQU07QUFDTGlDLFNBQUsxRSxJQUFJUSxJQUFKLENBQVMrRCxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxzQ0FEaEM7QUFFTDVELFVBQU07QUFGRCxJQVAwQjtBQVdoQytELFVBQU8sQ0FBQyxDQUFDLENBQUQsRUFBSSxNQUFKLENBQUQsQ0FYeUI7QUFZaENDLGVBQVksRUFab0I7QUFhaENDLFlBQVMsQ0FDUjtBQUNDM0UsVUFBTSxJQURQO0FBRUM0RSxlQUFXLEtBRlo7QUFHQ0Msb0JBQWdCLGdEQUhqQjtBQUlDNUIsV0FBTyxJQUpSO0FBS0M2QixlQUFXO0FBTFosSUFEUSxFQVFSO0FBQ0M5RSxVQUFNLFdBRFA7QUFFQzRFLGVBQVcsS0FGWjtBQUdDM0IsV0FBTyxJQUhSO0FBSUM2QixlQUFXO0FBSlosSUFSUSxFQWNSO0FBQ0M5RSxVQUFNLGVBRFA7QUFFQ2lELFdBQU87QUFGUixJQWRRLEVBa0JSO0FBQ0NqRCxVQUFNLFdBRFA7QUFFQ2lELFdBQU87QUFGUixJQWxCUSxFQXNCUjtBQUNDakQsVUFBTSxRQURQO0FBRUNpRCxXQUFPO0FBRlIsSUF0QlEsRUEwQlI7QUFDQ2pELFVBQU0sV0FEUDtBQUVDaUQsV0FBTztBQUZSLElBMUJRLEVBOEJSO0FBQ0NqRCxVQUFNLFNBRFA7QUFFQ2lELFdBQU87QUFGUixJQTlCUSxFQWtDUjtBQUNDakQsVUFBTSxZQURQO0FBRUNpRCxXQUFPLElBRlI7QUFHQzhCLFlBQVFsRSxRQUFRSjtBQUhqQixJQWxDUSxFQXVDUjtBQUNDVCxVQUFNLElBRFA7QUFFQzRFLGVBQVcsS0FGWjtBQUdDQyxvQkFBZ0IsRUFIakI7QUFJQ0UsWUFBUWxFLFFBQVFSLGtCQUpqQjtBQUtDNEMsV0FBTztBQUxSLElBdkNRO0FBYnVCLEdBQWpDOztBQThEQTtBQUNBbkQsTUFBSTBCLElBQUosQ0FBU3NDLFNBQVQsQ0FBbUJrQixLQUFuQixDQUF5Qi9FLEtBQXpCLEVBQWdDLFVBQVNlLEtBQVQsRUFBZ0JpRSxRQUFoQixFQUEwQkMsUUFBMUIsRUFBb0N4RCxPQUFwQyxFQUE2QztBQUM1RTVCLE9BQUkwQixJQUFKLENBQVNDLEtBQVQsQ0FBZUMsT0FBZixDQUF1QjtBQUN0QkMsV0FBTyxnQkFBZ0I3QixJQUFJUSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQUREO0FBRXRCb0IsYUFBU0Y7QUFGYSxJQUF2QjtBQUlBLEdBTEQ7O0FBT0E7QUFDQTVCLE1BQUkwQixJQUFKLENBQVNzQyxTQUFULENBQW1CcUIsWUFBbkIsQ0FBZ0NsRixLQUFoQyxFQUF1QyxVQUFTZSxLQUFULEVBQWdCaUUsUUFBaEIsRUFBMEJHLElBQTFCLEVBQWdDO0FBQ3RFLE9BQUlBLEtBQUtDLFNBQUwsS0FBbUIsSUFBdkIsRUFBNkI7QUFDNUJ2RixRQUFJUSxJQUFKLENBQVNnRixLQUFULENBQWVOLEtBQWYsQ0FBcUIsNkJBQXJCLEVBQW9EL0UsTUFBTXFFLEdBQU4sQ0FBVSxDQUFWLENBQXBELEVBQWtFYyxJQUFsRTtBQUNBdEYsUUFBSTBCLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxPQUFmLENBQXVCO0FBQ3RCQyxZQUFPLFVBQVU3QixJQUFJUSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQURLO0FBRXRCb0IsY0FBU3dELEtBQUsxRDtBQUZRLEtBQXZCO0FBSUE7QUFDRCxHQVJEOztBQVVBO0FBQ0E7QUFDQXpCLFFBQU1zRixFQUFOLENBQVMsU0FBVCxFQUFvQixVQUFTQyxDQUFULEVBQVlQLFFBQVosRUFBc0JHLElBQXRCLEVBQTRCO0FBQy9DbEYsS0FBRSxZQUFGLEVBQWdCdUYsUUFBaEIsQ0FBeUJ2RixFQUFFLHVCQUFGLENBQXpCO0FBQ0FBLEtBQUUsb0JBQUYsRUFDRXVGLFFBREYsQ0FDV3ZGLEVBQUUsa0NBQUYsQ0FEWCxFQUVFd0YsR0FGRixDQUVNLE9BRk4sRUFFZSxNQUZmO0FBR0F4RixLQUFFLHdCQUFGLEVBQ0V1RixRQURGLENBQ1d2RixFQUFFLGtDQUFGLENBRFgsRUFFRXdGLEdBRkYsQ0FFTSxPQUZOLEVBRWUsTUFGZjtBQUdBLEdBUkQ7O0FBVUE7QUFDQXpGLFFBQU1zRixFQUFOLENBQVMsU0FBVCxFQUFvQixZQUFXO0FBQzlCdEYsU0FBTWlCLElBQU4sQ0FBVyxPQUFYLEVBQW9CeUUsSUFBcEIsQ0FBeUIsZ0JBQXpCLEVBQTJDLFVBQTNDO0FBQ0FoRyxNQUFHaUcsT0FBSCxDQUFXL0IsSUFBWCxDQUFnQjVELEtBQWhCLEVBRjhCLENBRU47QUFDeEIsR0FIRDs7QUFLQTtBQUNBLE1BQUk0RixRQUFKO0FBQ0E1RixRQUFNc0YsRUFBTixDQUFTLFdBQVQsRUFBc0IsVUFBU0MsQ0FBVCxFQUFZUCxRQUFaLEVBQXNCRyxJQUF0QixFQUE0QjtBQUNqRFMsY0FBVy9GLElBQUkwQixJQUFKLENBQVNzRSxlQUFULENBQXlCQyxJQUF6QixDQUE4QjlGLEtBQTlCLENBQVg7QUFDQSxHQUZEO0FBR0FBLFFBQU1zRixFQUFOLENBQVMsUUFBVCxFQUFtQixVQUFTQyxDQUFULEVBQVlQLFFBQVosRUFBc0JHLElBQXRCLEVBQTRCO0FBQzlDLE9BQUlTLFFBQUosRUFBYztBQUNiL0YsUUFBSTBCLElBQUosQ0FBU3NFLGVBQVQsQ0FBeUJFLElBQXpCLENBQThCSCxRQUE5QjtBQUNBO0FBQ0QsR0FKRDs7QUFNQTtBQUNBNUYsUUFDRXNGLEVBREYsQ0FDSyxPQURMLEVBQ2Msa0JBRGQsRUFDa0N4RSxnQkFEbEMsRUFFRXdFLEVBRkYsQ0FFSyxPQUZMLEVBRWMsYUFGZCxFQUU2QmxFLFlBRjdCLEVBR0VrRSxFQUhGLENBR0ssT0FITCxFQUdjLGdCQUhkLEVBR2dDNUMsZUFIaEMsRUFJRTRDLEVBSkYsQ0FJSyxPQUpMLEVBSWMsZUFKZCxFQUkrQi9CLGNBSi9CLEVBS0UrQixFQUxGLENBS0ssT0FMTCxFQUtjLGdCQUxkLEVBS2dDN0IsZUFMaEM7O0FBT0F0QjtBQUNBLEVBekhEOztBQTJIQTtBQUNBLFFBQU92QyxNQUFQO0FBQ0EsQ0EzWEYiLCJmaWxlIjoiZW1haWxzL2VtYWlsc190YWJsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZW1haWxzX3RhYmxlLmpzIDIwMTYtMTAtMTFcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIEVtYWlscyBUYWJsZSBDb250cm9sbGVyXG4gKlxuICogVGhpcyBjb250cm9sbGVyIHdpbGwgaGFuZGxlIHRoZSBtYWluIHRhYmxlIG9wZXJhdGlvbnMgb2YgdGhlIGFkbWluL2VtYWlscyBwYWdlLlxuICpcbiAqIEBtb2R1bGUgQ29udHJvbGxlcnMvZW1haWxzX3RhYmxlXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J2VtYWlsc190YWJsZScsXG5cdFxuXHRbXG5cdFx0anNlLnNvdXJjZSArICcvdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmNzcycsXG5cdFx0anNlLnNvdXJjZSArICcvdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmpzJyxcblx0XHRneC5zb3VyY2UgKyAnL2xpYnMvZW1haWxzJyxcblx0XHQnbW9kYWwnLFxuXHRcdCdkYXRhdGFibGUnLFxuXHRcdCdub3JtYWxpemUnXG5cdF0sXG5cdFxuXHQvKiogQGxlbmRzIG1vZHVsZTpDb250cm9sbGVycy9lbWFpbHNfdGFibGUgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZGFsIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JG1vZGFsID0gJCgnI2VtYWlscy1tb2RhbCcpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgTW9kdWxlIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0ZW1haWxzVGFibGVBY3Rpb25zOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRyZXR1cm4gJzxkaXYgY2xhc3M9XCJyb3ctYWN0aW9uc1wiPicgKyAnPHNwYW4gY2xhc3M9XCJzZW5kLWVtYWlsIGFjdGlvbi1pdGVtXCIgdGl0bGU9XCInXG5cdFx0XHRcdFx0XHQrIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKFxuXHRcdFx0XHRcdFx0XHQnc2VuZCcsXG5cdFx0XHRcdFx0XHRcdCdidXR0b25zJykgKyAnXCI+JyArICc8aSBjbGFzcz1cImZhIGZhLWVudmVsb3BlLW9cIj48L2k+JyArICc8L3NwYW4+JyArXG5cdFx0XHRcdFx0XHQnPHNwYW4gY2xhc3M9XCJmb3J3YXJkLWVtYWlsIGFjdGlvbi1pdGVtXCIgdGl0bGU9XCInXG5cdFx0XHRcdFx0XHQrIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdmb3J3YXJkJywgJ2J1dHRvbnMnKSArXG5cdFx0XHRcdFx0XHQnXCI+JyArXG5cdFx0XHRcdFx0XHQnPGkgY2xhc3M9XCJmYSBmYS1zaGFyZVwiPjwvaT4nICsgJzwvc3Bhbj4nICtcblx0XHRcdFx0XHRcdCc8c3BhbiBjbGFzcz1cImRlbGV0ZS1lbWFpbCBhY3Rpb24taXRlbVwiIHRpdGxlPVwiJyArIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKFxuXHRcdFx0XHRcdFx0XHQnZGVsZXRlJywgJ2J1dHRvbnMnKSArICdcIj4nICsgJzxpIGNsYXNzPVwiZmEgZmEtdHJhc2gtb1wiPjwvaT4nICsgJzwvc3Bhbj4nICtcblx0XHRcdFx0XHRcdCc8c3BhbiBjbGFzcz1cInByZXZpZXctZW1haWwgYWN0aW9uLWl0ZW1cIiB0aXRsZT1cIidcblx0XHRcdFx0XHRcdCsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3ByZXZpZXcnLCAnYnV0dG9ucycpICtcblx0XHRcdFx0XHRcdCdcIj4nICtcblx0XHRcdFx0XHRcdCc8aSBjbGFzcz1cImZhIGZhLWV5ZVwiPjwvaT4nICsgJzwvc3Bhbj4nICsgJzwvZGl2Pic7XG5cdFx0XHRcdH0sXG5cdFx0XHRcdFxuXHRcdFx0XHRjb252ZXJ0UGVuZGluZ1RvU3RyaW5nOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHRyZXR1cm4gKGRhdGFcblx0XHRcdFx0XHQ9PT0gdHJ1ZSkgPyBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZW1haWxfcGVuZGluZycsICdlbWFpbHMnKSA6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKFxuXHRcdFx0XHRcdFx0J2VtYWlsX3NlbnQnLCAnZW1haWxzJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgTW9kdWxlIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBUb2dnbGUgcm93IHNlbGVjdGlvbiBmb3IgbWFpbiBwYWdlIHRhYmxlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqL1xuXHRcdHZhciBfb25TZWxlY3RBbGxSb3dzID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdGlmICgkKHRoaXMpLnByb3AoJ2NoZWNrZWQnKSkge1xuXHRcdFx0XHQkdGhpcy5maW5kKCd0Ym9keSAuc2luZ2xlLWNoZWNrYm94JykuYWRkQ2xhc3MoJ2NoZWNrZWQnKTtcblx0XHRcdFx0JHRoaXMuZmluZCgndGJvZHkgaW5wdXQ6Y2hlY2tib3gnKS5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQkdGhpcy5maW5kKCd0Ym9keSAuc2luZ2xlLWNoZWNrYm94JykucmVtb3ZlQ2xhc3MoJ2NoZWNrZWQnKTtcblx0XHRcdFx0JHRoaXMuZmluZCgndGJvZHkgaW5wdXQ6Y2hlY2tib3gnKS5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogV2lsbCBzZW5kIHRoZSBlbWFpbCB0byBpdHMgY29udGFjdHMgKGV2ZW4gaWYgaXRzIHN0YXR1cyBpcyBcInNlbnRcIikuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgQ29udGFpbnMgZXZlbnQgaW5mb3JtYXRpb24uXG5cdFx0ICovXG5cdFx0dmFyIF9vblNlbmRFbWFpbCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgJHJvdyA9ICQodGhpcykucGFyZW50cygndHInKTtcblx0XHRcdFxuXHRcdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZSh7XG5cdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc2VuZCcsICdidXR0b25zJyksXG5cdFx0XHRcdGNvbnRlbnQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdwcm9tcHRfc2VuZF9lbWFpbCcsICdlbWFpbHMnKSxcblx0XHRcdFx0YnV0dG9uczogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdubycsICdsaWdodGJveF9idXR0b25zJyksXG5cdFx0XHRcdFx0XHRjbGljazogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3llcycsICdsaWdodGJveF9idXR0b25zJyksXG5cdFx0XHRcdFx0XHRjbGljazogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdHZhciBlbWFpbCA9ICRyb3cuZGF0YSgpO1xuXHRcdFx0XHRcdFx0XHRqc2UubGlicy5lbWFpbHMuc2VuZENvbGxlY3Rpb24oW2VtYWlsXSlcblx0XHRcdFx0XHRcdFx0XHQuZG9uZShmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQoKTtcblx0XHRcdFx0XHRcdFx0XHRcdGpzZS5saWJzLmVtYWlscy5nZXRBdHRhY2htZW50c1NpemUoJCgnI2F0dGFjaG1lbnRzLXNpemUnKSk7XG5cdFx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdFx0XHQuZmFpbChmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0dmFyIHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLm1lc3NhZ2Uoe1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR0aXRsZTogdGl0bGUsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGNvbnRlbnQ6IHJlc3BvbnNlLm1lc3NhZ2Vcblx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHQkKHRoaXMpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdF1cblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGlzcGxheSBtb2RhbCB3aXRoIGVtYWlsIGluZm9ybWF0aW9uIGJ1dCB3aXRob3V0IGNvbnRhY3RzLlxuXHRcdCAqXG5cdFx0ICogVGhlIHVzZXIgd2lsbCBiZSBhYmxlIHRvIHNldCBuZXcgY29udGFjdHMgYW5kIHNlbmQgdGhlIGVtYWlsIChraW5kIG9mIFwiZHVwbGljYXRlXCIgbWV0aG9kKS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBDb250YWlucyBldmVudCBpbmZvcm1hdGlvbi5cblx0XHQgKi9cblx0XHR2YXIgX29uRm9yd2FyZEVtYWlsID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBlbWFpbCA9ICQodGhpcykucGFyZW50cygndHInKS5kYXRhKCk7XG5cdFx0XHRcblx0XHRcdGpzZS5saWJzLmVtYWlscy5yZXNldE1vZGFsKCRtb2RhbCk7XG5cdFx0XHRqc2UubGlicy5lbWFpbHMubG9hZEVtYWlsT25Nb2RhbChlbWFpbCwgJG1vZGFsKTtcblx0XHRcdFxuXHRcdFx0Ly8gQ2xlYXIgY29udGFjdCBmaWVsZHMgYnV0IGxldCB0aGUgcmVzdCBvZiB0aGUgZGF0YSB1bnRvdWNoZWQuXG5cdFx0XHQkbW9kYWwuZmluZCgnI2VtYWlsLWlkJykudmFsKCcnKTtcblx0XHRcdCRtb2RhbC5maW5kKCcjc2VuZGVyLWVtYWlsLCAjc2VuZGVyLW5hbWUnKS52YWwoJycpO1xuXHRcdFx0JG1vZGFsLmZpbmQoJyNyZXBseS10by1lbWFpbCwgI3JlcGx5LXRvLW5hbWUnKS52YWwoJycpO1xuXHRcdFx0JG1vZGFsLmZpbmQoJyNyZWNpcGllbnQtZW1haWwsICNyZWNpcGllbnQtbmFtZScpLnZhbCgnJyk7XG5cdFx0XHQkbW9kYWwuZmluZCgnI2NvbnRhY3RzLXRhYmxlJykuRGF0YVRhYmxlKCkuY2xlYXIoKS5kcmF3KCk7XG5cdFx0XHRcblx0XHRcdCRtb2RhbC5kaWFsb2coe1xuXHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2ZvcndhcmQnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHR3aWR0aDogMTAwMCxcblx0XHRcdFx0aGVpZ2h0OiA4MDAsXG5cdFx0XHRcdG1vZGFsOiB0cnVlLFxuXHRcdFx0XHRkaWFsb2dDbGFzczogJ2d4LWNvbnRhaW5lcicsXG5cdFx0XHRcdGNsb3NlT25Fc2NhcGU6IGZhbHNlLFxuXHRcdFx0XHRidXR0b25zOiBqc2UubGlicy5lbWFpbHMuZ2V0RGVmYXVsdE1vZGFsQnV0dG9ucygkbW9kYWwsICR0aGlzKSxcblx0XHRcdFx0b3BlbjoganNlLmxpYnMuZW1haWxzLmNvbG9yaXplQnV0dG9uc0ZvckVkaXRNb2RlXG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlbGV0ZSBzZWxlY3RlZCByb3cgZW1haWwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgQ29udGFpbnMgZXZlbnQgaW5mb3JtYXRpb24uXG5cdFx0ICovXG5cdFx0dmFyIF9vbkRlbGV0ZUVtYWlsID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciAkcm93ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLFxuXHRcdFx0XHRlbWFpbCA9ICRyb3cuZGF0YSgpO1xuXHRcdFx0XG5cdFx0XHRqc2UubGlicy5tb2RhbC5tZXNzYWdlKHtcblx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdkZWxldGUnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRjb250ZW50OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgncHJvbXB0X2RlbGV0ZV9lbWFpbCcsICdlbWFpbHMnKSxcblx0XHRcdFx0YnV0dG9uczogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdubycsICdsaWdodGJveF9idXR0b25zJyksXG5cdFx0XHRcdFx0XHRjbGljazogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3llcycsICdsaWdodGJveF9idXR0b25zJyksXG5cdFx0XHRcdFx0XHRjbGljazogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdGpzZS5saWJzLmVtYWlscy5kZWxldGVDb2xsZWN0aW9uKFtlbWFpbF0pXG5cdFx0XHRcdFx0XHRcdFx0LmRvbmUoZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRcdFx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmFqYXgucmVsb2FkKCk7XG5cdFx0XHRcdFx0XHRcdFx0XHRqc2UubGlicy5lbWFpbHMuZ2V0QXR0YWNobWVudHNTaXplKCQoJyNhdHRhY2htZW50cy1zaXplJykpO1xuXHRcdFx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHRcdFx0LmZhaWwoZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRcdFx0XHRcdHZhciB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpO1xuXHRcdFx0XHRcdFx0XHRcdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZSh7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHRpdGxlOiB0aXRsZSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0Y29udGVudDogcmVzcG9uc2UubWVzc2FnZVxuXHRcdFx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XVxuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEaXNwbGF5IG1vZGFsIHdpdGggZW1haWwgaW5mb3JtYXRpb25cblx0XHQgKlxuXHRcdCAqIFRoZSB1c2VyIGNhbiBzZWxlY3QgYW4gYWN0aW9uIHRvIHBlcmZvcm0gdXBvbiB0aGUgcHJldmlld2VkIGVtYWlsIChTZW5kLCBGb3J3YXJkLFxuXHRcdCAqIERlbGV0ZSwgQ2xvc2UpLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtICB7b2JqZWN0fSBldmVudCBDb250YWlucyBldmVudCBpbmZvcm1hdGlvbi5cblx0XHQgKi9cblx0XHR2YXIgX29uUHJldmlld0VtYWlsID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBlbWFpbCA9ICQodGhpcykucGFyZW50cygndHInKS5kYXRhKCk7XG5cdFx0XHRcblx0XHRcdGpzZS5saWJzLmVtYWlscy5yZXNldE1vZGFsKCRtb2RhbCk7XG5cdFx0XHRqc2UubGlicy5lbWFpbHMubG9hZEVtYWlsT25Nb2RhbChlbWFpbCwgJG1vZGFsKTtcblx0XHRcdFxuXHRcdFx0JG1vZGFsLmRpYWxvZyh7XG5cdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgncHJldmlldycsICdidXR0b25zJyksXG5cdFx0XHRcdHdpZHRoOiAxMDAwLFxuXHRcdFx0XHRoZWlnaHQ6IDgwMCxcblx0XHRcdFx0bW9kYWw6IGZhbHNlLFxuXHRcdFx0XHRkaWFsb2dDbGFzczogJ2d4LWNvbnRhaW5lcicsXG5cdFx0XHRcdGNsb3NlT25Fc2NhcGU6IGZhbHNlLFxuXHRcdFx0XHRidXR0b25zOiBqc2UubGlicy5lbWFpbHMuZ2V0UHJldmlld01vZGFsQnV0dG9ucygkbW9kYWwsICR0aGlzKSxcblx0XHRcdFx0b3BlbjoganNlLmxpYnMuZW1haWxzLmNvbG9yaXplQnV0dG9uc0ZvclByZXZpZXdNb2RlXG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIG1vZHVsZSwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICpcblx0XHQgKiBUaGUgZW1haWxzIHRhYmxlIG9wZXJhdGVzIHdpdGggc2VydmVyIHByb2Nlc3NpbmcgYmVjYXVzZSBpdCBpcyBtdWNoIGZhc3RlciBhbmQgZWZmaWNpZW50IHRoYW4gcHJlcGFyaW5nXG5cdFx0ICogYW5kIHNlbmRpbmcgYWxsIHRoZSByZWNvcmRzIGluIGV2ZXJ5IEFKQVggcmVxdWVzdC4gQ2hlY2sgdGhlIEVtYWlscy9EYXRhVGFibGUgY29udHJvbGxlciBtZXRob2QgZm9yXG5cdFx0ICogcmVxdWVzdGVkIGRhdGEgYW5kIHRoZSBmb2xsb3dpbmcgbGluayBmb3IgbW9yZSBpbmZvIGFib3V0IHNlcnZlciBwcm9jZXNzaW5nIGluIERhdGFUYWJsZXMuXG5cdFx0ICpcblx0XHQgKiB7QGxpbmsgaHR0cDovL3d3dy5kYXRhdGFibGVzLm5ldC9tYW51YWwvc2VydmVyLXNpZGV9XG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBDcmVhdGUgYSBEYXRhVGFibGUgaW5zdGFuY2UgZm9yIHRoZSBlbWFpbCByZWNvcmRzLlxuXHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmNyZWF0ZSgkdGhpcywge1xuXHRcdFx0XHRwcm9jZXNzaW5nOiBmYWxzZSxcblx0XHRcdFx0c2VydmVyU2lkZTogdHJ1ZSxcblx0XHRcdFx0ZG9tOiAncnRpcCcsXG5cdFx0XHRcdGF1dG9XaWR0aDogZmFsc2UsXG5cdFx0XHRcdGxhbmd1YWdlOiAoanNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJylcblx0XHRcdFx0PT09ICdkZScpID8ganNlLmxpYnMuZGF0YXRhYmxlLmdldEdlcm1hblRyYW5zbGF0aW9uKCkgOiBudWxsLFxuXHRcdFx0XHRhamF4OiB7XG5cdFx0XHRcdFx0dXJsOiBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPUVtYWlscy9EYXRhVGFibGUnLFxuXHRcdFx0XHRcdHR5cGU6ICdQT1NUJ1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRvcmRlcjogW1syLCAnZGVzYyddXSxcblx0XHRcdFx0cGFnZUxlbmd0aDogMjAsXG5cdFx0XHRcdGNvbHVtbnM6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiBudWxsLFxuXHRcdFx0XHRcdFx0b3JkZXJhYmxlOiBmYWxzZSxcblx0XHRcdFx0XHRcdGRlZmF1bHRDb250ZW50OiAnPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGRhdGEtc2luZ2xlX2NoZWNrYm94IC8+Jyxcblx0XHRcdFx0XHRcdHdpZHRoOiAnMiUnLFxuXHRcdFx0XHRcdFx0Y2xhc3NOYW1lOiAnZHQtaGVhZC1jZW50ZXIgZHQtYm9keS1jZW50ZXInXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiAncm93X2NvdW50Jyxcblx0XHRcdFx0XHRcdG9yZGVyYWJsZTogZmFsc2UsXG5cdFx0XHRcdFx0XHR3aWR0aDogJzMlJyxcblx0XHRcdFx0XHRcdGNsYXNzTmFtZTogJ2R0LWhlYWQtY2VudGVyIGR0LWJvZHktY2VudGVyJ1xuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0ZGF0YTogJ2NyZWF0aW9uX2RhdGUnLFxuXHRcdFx0XHRcdFx0d2lkdGg6ICcxMiUnXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiAnc2VudF9kYXRlJyxcblx0XHRcdFx0XHRcdHdpZHRoOiAnMTIlJ1xuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0ZGF0YTogJ3NlbmRlcicsXG5cdFx0XHRcdFx0XHR3aWR0aDogJzEyJSdcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGRhdGE6ICdyZWNpcGllbnQnLFxuXHRcdFx0XHRcdFx0d2lkdGg6ICcxMiUnXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiAnc3ViamVjdCcsXG5cdFx0XHRcdFx0XHR3aWR0aDogJzI3JSdcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGRhdGE6ICdpc19wZW5kaW5nJyxcblx0XHRcdFx0XHRcdHdpZHRoOiAnOCUnLFxuXHRcdFx0XHRcdFx0cmVuZGVyOiBvcHRpb25zLmNvbnZlcnRQZW5kaW5nVG9TdHJpbmdcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGRhdGE6IG51bGwsXG5cdFx0XHRcdFx0XHRvcmRlcmFibGU6IGZhbHNlLFxuXHRcdFx0XHRcdFx0ZGVmYXVsdENvbnRlbnQ6ICcnLFxuXHRcdFx0XHRcdFx0cmVuZGVyOiBvcHRpb25zLmVtYWlsc1RhYmxlQWN0aW9ucyxcblx0XHRcdFx0XHRcdHdpZHRoOiAnMTIlJ1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIEFkZCB0YWJsZSBlcnJvciBoYW5kbGVyLlxuXHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmVycm9yKCR0aGlzLCBmdW5jdGlvbihldmVudCwgc2V0dGluZ3MsIHRlY2hOb3RlLCBtZXNzYWdlKSB7XG5cdFx0XHRcdGpzZS5saWJzLm1vZGFsLm1lc3NhZ2Uoe1xuXHRcdFx0XHRcdHRpdGxlOiAnRGF0YVRhYmxlcyAnICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyksXG5cdFx0XHRcdFx0Y29udGVudDogbWVzc2FnZVxuXHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgYWpheCBlcnJvciBoYW5kbGVyLlxuXHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmFqYXhDb21wbGV0ZSgkdGhpcywgZnVuY3Rpb24oZXZlbnQsIHNldHRpbmdzLCBqc29uKSB7XG5cdFx0XHRcdGlmIChqc29uLmV4Y2VwdGlvbiA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdGpzZS5jb3JlLmRlYnVnLmVycm9yKCdEYXRhVGFibGVzIFByb2Nlc3NpbmcgRXJyb3InLCAkdGhpcy5nZXQoMCksIGpzb24pO1xuXHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLm1lc3NhZ2Uoe1xuXHRcdFx0XHRcdFx0dGl0bGU6ICdBSkFYICcgKyBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKSxcblx0XHRcdFx0XHRcdGNvbnRlbnQ6IGpzb24ubWVzc2FnZVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gQ29tYmluZSBcIi5wYWdpbmF0b3JcIiB3aXRoIHRoZSBEYXRhVGFibGUgSFRNTCBvdXRwdXQgaW4gb3JkZXIgdG8gY3JlYXRlIGEgdW5pcXVlIHBhZ2luYXRpb25cblx0XHRcdC8vIGZyYW1lIGF0IHRoZSBib3R0b20gb2YgdGhlIHRhYmxlIChleGVjdXRlZCBhZnRlciB0YWJsZSBpbml0aWFsaXphdGlvbikuXG5cdFx0XHQkdGhpcy5vbignaW5pdC5kdCcsIGZ1bmN0aW9uKGUsIHNldHRpbmdzLCBqc29uKSB7XG5cdFx0XHRcdCQoJy5wYWdpbmF0b3InKS5hcHBlbmRUbygkKCcjZW1haWxzLXRhYmxlX3dyYXBwZXInKSk7XG5cdFx0XHRcdCQoJyNlbWFpbHMtdGFibGVfaW5mbycpXG5cdFx0XHRcdFx0LmFwcGVuZFRvKCQoJy5wYWdpbmF0b3IgLmRhdGF0YWJsZS1jb21wb25lbnRzJykpXG5cdFx0XHRcdFx0LmNzcygnY2xlYXInLCAnbm9uZScpO1xuXHRcdFx0XHQkKCcjZW1haWxzLXRhYmxlX3BhZ2luYXRlJylcblx0XHRcdFx0XHQuYXBwZW5kVG8oJCgnLnBhZ2luYXRvciAuZGF0YXRhYmxlLWNvbXBvbmVudHMnKSlcblx0XHRcdFx0XHQuY3NzKCdjbGVhcicsICdub25lJyk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVjcmVhdGUgdGhlIGNoZWNrYm94IHdpZGdldHMuXG5cdFx0XHQkdGhpcy5vbignZHJhdy5kdCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkdGhpcy5maW5kKCd0Ym9keScpLmF0dHIoJ2RhdGEtZ3gtd2lkZ2V0JywgJ2NoZWNrYm94Jyk7XG5cdFx0XHRcdGd4LndpZGdldHMuaW5pdCgkdGhpcyk7IC8vIEluaXRpYWxpemUgdGhlIGNoZWNrYm94IHdpZGdldC5cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgc3Bpbm5lciB0byB0YWJsZSBsb2FkaW5nIGFjdGlvbnMuXG5cdFx0XHR2YXIgJHNwaW5uZXI7XG5cdFx0XHQkdGhpcy5vbigncHJlWGhyLmR0JywgZnVuY3Rpb24oZSwgc2V0dGluZ3MsIGpzb24pIHtcblx0XHRcdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcyk7XG5cdFx0XHR9KTtcblx0XHRcdCR0aGlzLm9uKCd4aHIuZHQnLCBmdW5jdGlvbihlLCBzZXR0aW5ncywganNvbikge1xuXHRcdFx0XHRpZiAoJHNwaW5uZXIpIHtcblx0XHRcdFx0XHRqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuaGlkZSgkc3Bpbm5lcik7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBCaW5kIGV2ZW50IGhhbmRsZXJzIG9mIHRoZSBlbWFpbHMgdGFibGUuXG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQub24oJ2NsaWNrJywgJyNzZWxlY3QtYWxsLXJvd3MnLCBfb25TZWxlY3RBbGxSb3dzKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5zZW5kLWVtYWlsJywgX29uU2VuZEVtYWlsKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5mb3J3YXJkLWVtYWlsJywgX29uRm9yd2FyZEVtYWlsKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5kZWxldGUtZW1haWwnLCBfb25EZWxldGVFbWFpbClcblx0XHRcdFx0Lm9uKCdjbGljaycsICcucHJldmlldy1lbWFpbCcsIF9vblByZXZpZXdFbWFpbCk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIFJldHVybiBkYXRhIHRvIG1vZHVsZSBlbmdpbmUuXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
