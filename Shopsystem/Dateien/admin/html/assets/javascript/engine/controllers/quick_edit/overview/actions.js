'use strict';

/* --------------------------------------------------------------
 actions.js 2017-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Actions
 *
 * This module creates the bulk and row actions for the table.
 */
gx.controllers.module('actions', ['user_configuration_service', gx.source + '/libs/button_dropdown'], function () {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Create Bulk Actions
  *
  * This callback can be called once during the initialization of this module.
  */
	function _createBulkActions() {
		// Add actions to the bulk-action dropdown.
		var $bulkActions = $('.overview-bulk-action');
		var defaultBulkAction = $this.data('defaultBulkAction') || 'bulk-row-edit';

		// Edit
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
			class: 'bulk-row-edit',
			data: { configurationValue: 'bulk-row-edit' },
			isDefault: defaultBulkAction === 'bulk-row-edit' || defaultBulkAction === 'save-bulk-row-edits',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Save
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
			class: 'save-bulk-row-edits hidden',
			data: { configurationValue: 'save-bulk-row-edits' },
			isDefault: false, // "Save" must not be shown as a default value. 
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Properties
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_PROPERTIES', 'admin_quick_edit'),
			class: 'bulk-properties-edit',
			data: { configurationValue: 'bulk-properties-edit' },
			isDefault: defaultBulkAction === 'bulk-properties-edit',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Special Price
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_SPECIAL_PRICE', 'admin_quick_edit'),
			class: 'bulk-special-price-edit',
			data: { configurationValue: 'bulk-special-price-edit' },
			isDefault: defaultBulkAction === 'bulk-special-price-edit',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		$this.parents('.quick-edit.overview').find('a.bulk-properties-edit').attr('data-toggle', 'modal').attr('data-target', '.properties.modal');

		$this.parents('.quick-edit.overview').find('a.bulk-special-price-edit').attr('data-toggle', 'modal').attr('data-target', '.special-prices.modal');

		$this.datatable_default_actions('ensure', 'bulk');
	}

	/**
  * Create Table Row Actions
  *
  * This function must be call with every table draw.dt event.
  */
	function _createRowActions() {
		// Re-create the checkbox widgets and the row actions. 
		var defaultRowAction = $this.data('defaultRowAction') || 'show-product';

		$this.find('.btn-group.dropdown').each(function () {
			var productId = $(this).parents('tr').data('id');

			// Show Product
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_SHOW', 'admin_quick_edit'),
				href: jse.core.config.get('appUrl') + '/admin/categories.php?pID=' + productId + '&action=new_product',
				class: 'show-product',
				data: { configurationValue: 'show-product' },
				isDefault: defaultRowAction === 'show-product',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Edit
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
				class: 'row-edit',
				data: { configurationValue: 'row-edit' },
				isDefault: defaultRowAction === 'row-edit' || defaultRowAction === 'save-row-edits',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Save
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
				class: 'save-row-edits hidden',
				data: { configurationValue: 'save-row-edits' },
				isDefault: false, // "Save" must not be saved as a pre-selected value.
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Properties
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_PROPERTIES', 'admin_quick_edit'),
				class: 'products-properties',
				data: { configurationValue: 'properties' },
				isDefault: defaultRowAction === 'properties',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Special Price
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_SPECIAL_PRICE', 'admin_quick_edit'),
				class: 'special-price',
				data: { configurationValue: 'special-price' },
				isDefault: defaultRowAction === 'special-price',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Graduated Prices
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_GRADUATED_PRICES', 'admin_quick_edit'),
				class: 'graduated-prices',
				data: { configurationValue: 'graduated-prices' },
				isDefault: defaultRowAction === 'graduated-prices',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			$this.parents('.quick-edit.overview').find('a.products-properties').attr('data-toggle', 'modal').attr('data-target', '.properties.modal');

			$this.parents('.quick-edit.overview').find('a.special-price').attr('data-toggle', 'modal').attr('data-target', '.special-prices.modal');

			$this.datatable_default_actions('ensure', 'row');
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(window).on('JSENGINE_INIT_FINISHED', function () {
			$this.on('draw.dt', _createRowActions);
			_createRowActions();
			_createBulkActions();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvb3ZlcnZpZXcvYWN0aW9ucy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwiJHRoaXMiLCIkIiwiX2NyZWF0ZUJ1bGtBY3Rpb25zIiwiJGJ1bGtBY3Rpb25zIiwiZGVmYXVsdEJ1bGtBY3Rpb24iLCJkYXRhIiwianNlIiwibGlicyIsImJ1dHRvbl9kcm9wZG93biIsImFkZEFjdGlvbiIsInRleHQiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsImNsYXNzIiwiY29uZmlndXJhdGlvblZhbHVlIiwiaXNEZWZhdWx0IiwiY2FsbGJhY2siLCJlIiwicHJldmVudERlZmF1bHQiLCJwYXJlbnRzIiwiZmluZCIsImF0dHIiLCJkYXRhdGFibGVfZGVmYXVsdF9hY3Rpb25zIiwiX2NyZWF0ZVJvd0FjdGlvbnMiLCJkZWZhdWx0Um93QWN0aW9uIiwiZWFjaCIsInByb2R1Y3RJZCIsImhyZWYiLCJjb25maWciLCJnZXQiLCJpbml0IiwiZG9uZSIsIndpbmRvdyIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFNBREQsRUFHQyxDQUFDLDRCQUFELEVBQWtDRixHQUFHRyxNQUFyQywyQkFIRCxFQUtDLFlBQVc7O0FBRVY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNJLGtCQUFULEdBQThCO0FBQzdCO0FBQ0EsTUFBTUMsZUFBZUYsRUFBRSx1QkFBRixDQUFyQjtBQUNBLE1BQU1HLG9CQUFvQkosTUFBTUssSUFBTixDQUFXLG1CQUFYLEtBQW1DLGVBQTdEOztBQUVBO0FBQ0FDLE1BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNOLFlBQW5DLEVBQWlEO0FBQ2hETyxTQUFNSixJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxrQkFBdkMsQ0FEMEM7QUFFaERDLFVBQU8sZUFGeUM7QUFHaERULFNBQU0sRUFBQ1Usb0JBQW9CLGVBQXJCLEVBSDBDO0FBSWhEQyxjQUFXWixzQkFBc0IsZUFBdEIsSUFBeUNBLHNCQUFzQixxQkFKMUI7QUFLaERhLGFBQVU7QUFBQSxXQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxzQyxHQUFqRDs7QUFRQTtBQUNBYixNQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLFNBQXpCLENBQW1DTixZQUFuQyxFQUFpRDtBQUNoRE8sU0FBTUosSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsYUFBeEIsRUFBdUMsa0JBQXZDLENBRDBDO0FBRWhEQyxVQUFPLDRCQUZ5QztBQUdoRFQsU0FBTSxFQUFDVSxvQkFBb0IscUJBQXJCLEVBSDBDO0FBSWhEQyxjQUFXLEtBSnFDLEVBSTdCO0FBQ25CQyxhQUFVO0FBQUEsV0FBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMc0MsR0FBakQ7O0FBUUE7QUFDQWIsTUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ04sWUFBbkMsRUFBaUQ7QUFDaERPLFNBQU1KLElBQUlLLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1CQUF4QixFQUE2QyxrQkFBN0MsQ0FEMEM7QUFFaERDLFVBQU8sc0JBRnlDO0FBR2hEVCxTQUFNLEVBQUNVLG9CQUFvQixzQkFBckIsRUFIMEM7QUFJaERDLGNBQVdaLHNCQUFzQixzQkFKZTtBQUtoRGEsYUFBVTtBQUFBLFdBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTHNDLEdBQWpEOztBQVFBO0FBQ0FiLE1BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNOLFlBQW5DLEVBQWlEO0FBQ2hETyxTQUFNSixJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixzQkFBeEIsRUFBZ0Qsa0JBQWhELENBRDBDO0FBRWhEQyxVQUFPLHlCQUZ5QztBQUdoRFQsU0FBTSxFQUFDVSxvQkFBb0IseUJBQXJCLEVBSDBDO0FBSWhEQyxjQUFXWixzQkFBc0IseUJBSmU7QUFLaERhLGFBQVU7QUFBQSxXQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxzQyxHQUFqRDs7QUFRQW5CLFFBQU1vQixPQUFOLENBQWMsc0JBQWQsRUFDRUMsSUFERixDQUNPLHdCQURQLEVBRUVDLElBRkYsQ0FFTyxhQUZQLEVBRXNCLE9BRnRCLEVBR0VBLElBSEYsQ0FHTyxhQUhQLEVBR3NCLG1CQUh0Qjs7QUFLQXRCLFFBQU1vQixPQUFOLENBQWMsc0JBQWQsRUFDRUMsSUFERixDQUNPLDJCQURQLEVBRUVDLElBRkYsQ0FFTyxhQUZQLEVBRXNCLE9BRnRCLEVBR0VBLElBSEYsQ0FHTyxhQUhQLEVBR3NCLHVCQUh0Qjs7QUFLQXRCLFFBQU11Qix5QkFBTixDQUFnQyxRQUFoQyxFQUEwQyxNQUExQztBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNDLGlCQUFULEdBQTZCO0FBQzVCO0FBQ0EsTUFBTUMsbUJBQW1CekIsTUFBTUssSUFBTixDQUFXLGtCQUFYLEtBQWtDLGNBQTNEOztBQUVBTCxRQUFNcUIsSUFBTixDQUFXLHFCQUFYLEVBQWtDSyxJQUFsQyxDQUF1QyxZQUFXO0FBQ2pELE9BQU1DLFlBQVkxQixFQUFFLElBQUYsRUFBUW1CLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JmLElBQXRCLENBQTJCLElBQTNCLENBQWxCOztBQUVBO0FBQ0FDLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNSLEVBQUUsSUFBRixDQUFuQyxFQUE0QztBQUMzQ1MsVUFBTUosSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsYUFBeEIsRUFBdUMsa0JBQXZDLENBRHFDO0FBRTNDZSxVQUFTdEIsSUFBSUssSUFBSixDQUFTa0IsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsQ0FBVCxrQ0FBbUVILFNBQW5FLHdCQUYyQztBQUczQ2IsV0FBTyxjQUhvQztBQUkzQ1QsVUFBTSxFQUFDVSxvQkFBb0IsY0FBckIsRUFKcUM7QUFLM0NDLGVBQVdTLHFCQUFxQixjQUxXO0FBTTNDUixjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFOaUMsSUFBNUM7O0FBU0E7QUFDQWIsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ1IsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDUyxVQUFNSixJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxrQkFBdkMsQ0FEcUM7QUFFM0NDLFdBQU8sVUFGb0M7QUFHM0NULFVBQU0sRUFBQ1Usb0JBQW9CLFVBQXJCLEVBSHFDO0FBSTNDQyxlQUFXUyxxQkFBcUIsVUFBckIsSUFBbUNBLHFCQUFxQixnQkFKeEI7QUFLM0NSLGNBQVU7QUFBQSxZQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxpQyxJQUE1Qzs7QUFRQTtBQUNBYixPQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLFNBQXpCLENBQW1DUixFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NTLFVBQU1KLElBQUlLLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGFBQXhCLEVBQXVDLGtCQUF2QyxDQURxQztBQUUzQ0MsV0FBTyx1QkFGb0M7QUFHM0NULFVBQU0sRUFBQ1Usb0JBQW9CLGdCQUFyQixFQUhxQztBQUkzQ0MsZUFBVyxLQUpnQyxFQUl6QjtBQUNsQkMsY0FBVTtBQUFBLFlBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTGlDLElBQTVDOztBQVFBO0FBQ0FiLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNSLEVBQUUsSUFBRixDQUFuQyxFQUE0QztBQUMzQ1MsVUFBTUosSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUJBQXhCLEVBQTZDLGtCQUE3QyxDQURxQztBQUUzQ0MsV0FBTyxxQkFGb0M7QUFHM0NULFVBQU0sRUFBQ1Usb0JBQW9CLFlBQXJCLEVBSHFDO0FBSTNDQyxlQUFXUyxxQkFBcUIsWUFKVztBQUszQ1IsY0FBVTtBQUFBLFlBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTGlDLElBQTVDOztBQVFBO0FBQ0FiLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNSLEVBQUUsSUFBRixDQUFuQyxFQUE0QztBQUMzQ1MsVUFBTUosSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isc0JBQXhCLEVBQWdELGtCQUFoRCxDQURxQztBQUUzQ0MsV0FBTyxlQUZvQztBQUczQ1QsVUFBTSxFQUFDVSxvQkFBb0IsZUFBckIsRUFIcUM7QUFJM0NDLGVBQVdTLHFCQUFxQixlQUpXO0FBSzNDUixjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsSUFBNUM7O0FBUUE7QUFDQWIsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ1IsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDUyxVQUFNSixJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix5QkFBeEIsRUFBbUQsa0JBQW5ELENBRHFDO0FBRTNDQyxXQUFPLGtCQUZvQztBQUczQ1QsVUFBTSxFQUFDVSxvQkFBb0Isa0JBQXJCLEVBSHFDO0FBSTNDQyxlQUFXUyxxQkFBcUIsa0JBSlc7QUFLM0NSLGNBQVU7QUFBQSxZQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxpQyxJQUE1Qzs7QUFRQW5CLFNBQU1vQixPQUFOLENBQWMsc0JBQWQsRUFDRUMsSUFERixDQUNPLHVCQURQLEVBRUVDLElBRkYsQ0FFTyxhQUZQLEVBRXNCLE9BRnRCLEVBR0VBLElBSEYsQ0FHTyxhQUhQLEVBR3NCLG1CQUh0Qjs7QUFLQXRCLFNBQU1vQixPQUFOLENBQWMsc0JBQWQsRUFDRUMsSUFERixDQUNPLGlCQURQLEVBRUVDLElBRkYsQ0FFTyxhQUZQLEVBRXNCLE9BRnRCLEVBR0VBLElBSEYsQ0FHTyxhQUhQLEVBR3NCLHVCQUh0Qjs7QUFLQXRCLFNBQU11Qix5QkFBTixDQUFnQyxRQUFoQyxFQUEwQyxLQUExQztBQUNBLEdBckVEO0FBc0VBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQXpCLFFBQU9pQyxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCL0IsSUFBRWdDLE1BQUYsRUFBVUMsRUFBVixDQUFhLHdCQUFiLEVBQXVDLFlBQU07QUFDNUNsQyxTQUFNa0MsRUFBTixDQUFTLFNBQVQsRUFBb0JWLGlCQUFwQjtBQUNBQTtBQUNBdEI7QUFDQSxHQUpEOztBQU1BOEI7QUFDQSxFQVJEOztBQVVBLFFBQU9sQyxNQUFQO0FBRUEsQ0EzTEYiLCJmaWxlIjoicXVpY2tfZWRpdC9vdmVydmlldy9hY3Rpb25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBhY3Rpb25zLmpzIDIwMTctMDUtMjlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIE1haW4gVGFibGUgQWN0aW9uc1xuICpcbiAqIFRoaXMgbW9kdWxlIGNyZWF0ZXMgdGhlIGJ1bGsgYW5kIHJvdyBhY3Rpb25zIGZvciB0aGUgdGFibGUuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J2FjdGlvbnMnLFxuXHRcblx0Wyd1c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZScsIGAke2d4LnNvdXJjZX0vbGlicy9idXR0b25fZHJvcGRvd25gXSxcblx0XG5cdGZ1bmN0aW9uKCkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDcmVhdGUgQnVsayBBY3Rpb25zXG5cdFx0ICpcblx0XHQgKiBUaGlzIGNhbGxiYWNrIGNhbiBiZSBjYWxsZWQgb25jZSBkdXJpbmcgdGhlIGluaXRpYWxpemF0aW9uIG9mIHRoaXMgbW9kdWxlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9jcmVhdGVCdWxrQWN0aW9ucygpIHtcblx0XHRcdC8vIEFkZCBhY3Rpb25zIHRvIHRoZSBidWxrLWFjdGlvbiBkcm9wZG93bi5cblx0XHRcdGNvbnN0ICRidWxrQWN0aW9ucyA9ICQoJy5vdmVydmlldy1idWxrLWFjdGlvbicpO1xuXHRcdFx0Y29uc3QgZGVmYXVsdEJ1bGtBY3Rpb24gPSAkdGhpcy5kYXRhKCdkZWZhdWx0QnVsa0FjdGlvbicpIHx8ICdidWxrLXJvdy1lZGl0Jztcblx0XHRcdFxuXHRcdFx0Ly8gRWRpdFxuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkYnVsa0FjdGlvbnMsIHtcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9FRElUJywgJ2FkbWluX3F1aWNrX2VkaXQnKSxcblx0XHRcdFx0Y2xhc3M6ICdidWxrLXJvdy1lZGl0Jyxcblx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2J1bGstcm93LWVkaXQnfSxcblx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0QnVsa0FjdGlvbiA9PT0gJ2J1bGstcm93LWVkaXQnIHx8IGRlZmF1bHRCdWxrQWN0aW9uID09PSAnc2F2ZS1idWxrLXJvdy1lZGl0cycsXG5cdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIFNhdmVcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XG5cdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fU0FWRScsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdGNsYXNzOiAnc2F2ZS1idWxrLXJvdy1lZGl0cyBoaWRkZW4nLFxuXHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnc2F2ZS1idWxrLXJvdy1lZGl0cyd9LFxuXHRcdFx0XHRpc0RlZmF1bHQ6IGZhbHNlLCAgLy8gXCJTYXZlXCIgbXVzdCBub3QgYmUgc2hvd24gYXMgYSBkZWZhdWx0IHZhbHVlLiBcblx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gUHJvcGVydGllc1xuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkYnVsa0FjdGlvbnMsIHtcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9QUk9QRVJUSUVTJywgJ2FkbWluX3F1aWNrX2VkaXQnKSxcblx0XHRcdFx0Y2xhc3M6ICdidWxrLXByb3BlcnRpZXMtZWRpdCcsXG5cdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdidWxrLXByb3BlcnRpZXMtZWRpdCd9LFxuXHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRCdWxrQWN0aW9uID09PSAnYnVsay1wcm9wZXJ0aWVzLWVkaXQnLFxuXHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBTcGVjaWFsIFByaWNlXG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCRidWxrQWN0aW9ucywge1xuXHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX1NQRUNJQUxfUFJJQ0UnLCAnYWRtaW5fcXVpY2tfZWRpdCcpLFxuXHRcdFx0XHRjbGFzczogJ2J1bGstc3BlY2lhbC1wcmljZS1lZGl0Jyxcblx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2J1bGstc3BlY2lhbC1wcmljZS1lZGl0J30sXG5cdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdEJ1bGtBY3Rpb24gPT09ICdidWxrLXNwZWNpYWwtcHJpY2UtZWRpdCcsXG5cdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLnBhcmVudHMoJy5xdWljay1lZGl0Lm92ZXJ2aWV3Jylcblx0XHRcdFx0LmZpbmQoJ2EuYnVsay1wcm9wZXJ0aWVzLWVkaXQnKVxuXHRcdFx0XHQuYXR0cignZGF0YS10b2dnbGUnLCAnbW9kYWwnKVxuXHRcdFx0XHQuYXR0cignZGF0YS10YXJnZXQnLCAnLnByb3BlcnRpZXMubW9kYWwnKTtcblx0XHRcdFxuXHRcdFx0JHRoaXMucGFyZW50cygnLnF1aWNrLWVkaXQub3ZlcnZpZXcnKVxuXHRcdFx0XHQuZmluZCgnYS5idWxrLXNwZWNpYWwtcHJpY2UtZWRpdCcpXG5cdFx0XHRcdC5hdHRyKCdkYXRhLXRvZ2dsZScsICdtb2RhbCcpXG5cdFx0XHRcdC5hdHRyKCdkYXRhLXRhcmdldCcsICcuc3BlY2lhbC1wcmljZXMubW9kYWwnKTtcblx0XHRcdFxuXHRcdFx0JHRoaXMuZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucygnZW5zdXJlJywgJ2J1bGsnKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlIFRhYmxlIFJvdyBBY3Rpb25zXG5cdFx0ICpcblx0XHQgKiBUaGlzIGZ1bmN0aW9uIG11c3QgYmUgY2FsbCB3aXRoIGV2ZXJ5IHRhYmxlIGRyYXcuZHQgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2NyZWF0ZVJvd0FjdGlvbnMoKSB7XG5cdFx0XHQvLyBSZS1jcmVhdGUgdGhlIGNoZWNrYm94IHdpZGdldHMgYW5kIHRoZSByb3cgYWN0aW9ucy4gXG5cdFx0XHRjb25zdCBkZWZhdWx0Um93QWN0aW9uID0gJHRoaXMuZGF0YSgnZGVmYXVsdFJvd0FjdGlvbicpIHx8ICdzaG93LXByb2R1Y3QnO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5maW5kKCcuYnRuLWdyb3VwLmRyb3Bkb3duJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0Y29uc3QgcHJvZHVjdElkID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoJ2lkJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTaG93IFByb2R1Y3Rcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XG5cdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9TSE9XJywgJ2FkbWluX3F1aWNrX2VkaXQnKSxcblx0XHRcdFx0XHRocmVmOiBgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYWRtaW4vY2F0ZWdvcmllcy5waHA/cElEPSR7cHJvZHVjdElkfSZhY3Rpb249bmV3X3Byb2R1Y3RgLFxuXHRcdFx0XHRcdGNsYXNzOiAnc2hvdy1wcm9kdWN0Jyxcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnc2hvdy1wcm9kdWN0J30sXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAnc2hvdy1wcm9kdWN0Jyxcblx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBFZGl0XG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fRURJVCcsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdFx0Y2xhc3M6ICdyb3ctZWRpdCcsXG5cdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ3Jvdy1lZGl0J30sXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAncm93LWVkaXQnIHx8IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdzYXZlLXJvdy1lZGl0cycsXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2F2ZVxuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcblx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX1NBVkUnLCAnYWRtaW5fcXVpY2tfZWRpdCcpLFxuXHRcdFx0XHRcdGNsYXNzOiAnc2F2ZS1yb3ctZWRpdHMgaGlkZGVuJyxcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnc2F2ZS1yb3ctZWRpdHMnfSxcblx0XHRcdFx0XHRpc0RlZmF1bHQ6IGZhbHNlLCAvLyBcIlNhdmVcIiBtdXN0IG5vdCBiZSBzYXZlZCBhcyBhIHByZS1zZWxlY3RlZCB2YWx1ZS5cblx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBQcm9wZXJ0aWVzXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fUFJPUEVSVElFUycsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdFx0Y2xhc3M6ICdwcm9kdWN0cy1wcm9wZXJ0aWVzJyxcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAncHJvcGVydGllcyd9LFxuXHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ3Byb3BlcnRpZXMnLFxuXHRcdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFNwZWNpYWwgUHJpY2Vcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XG5cdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9TUEVDSUFMX1BSSUNFJywgJ2FkbWluX3F1aWNrX2VkaXQnKSxcblx0XHRcdFx0XHRjbGFzczogJ3NwZWNpYWwtcHJpY2UnLFxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdzcGVjaWFsLXByaWNlJ30sXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAnc3BlY2lhbC1wcmljZScsXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gR3JhZHVhdGVkIFByaWNlc1xuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcblx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX0dSQURVQVRFRF9QUklDRVMnLCAnYWRtaW5fcXVpY2tfZWRpdCcpLFxuXHRcdFx0XHRcdGNsYXNzOiAnZ3JhZHVhdGVkLXByaWNlcycsXG5cdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2dyYWR1YXRlZC1wcmljZXMnfSxcblx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdncmFkdWF0ZWQtcHJpY2VzJyxcblx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkdGhpcy5wYXJlbnRzKCcucXVpY2stZWRpdC5vdmVydmlldycpXG5cdFx0XHRcdFx0LmZpbmQoJ2EucHJvZHVjdHMtcHJvcGVydGllcycpXG5cdFx0XHRcdFx0LmF0dHIoJ2RhdGEtdG9nZ2xlJywgJ21vZGFsJylcblx0XHRcdFx0XHQuYXR0cignZGF0YS10YXJnZXQnLCAnLnByb3BlcnRpZXMubW9kYWwnKTtcblx0XHRcdFx0XG5cdFx0XHRcdCR0aGlzLnBhcmVudHMoJy5xdWljay1lZGl0Lm92ZXJ2aWV3Jylcblx0XHRcdFx0XHQuZmluZCgnYS5zcGVjaWFsLXByaWNlJylcblx0XHRcdFx0XHQuYXR0cignZGF0YS10b2dnbGUnLCAnbW9kYWwnKVxuXHRcdFx0XHRcdC5hdHRyKCdkYXRhLXRhcmdldCcsICcuc3BlY2lhbC1wcmljZXMubW9kYWwnKTtcblx0XHRcdFx0XG5cdFx0XHRcdCR0aGlzLmRhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnMoJ2Vuc3VyZScsICdyb3cnKTtcblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JCh3aW5kb3cpLm9uKCdKU0VOR0lORV9JTklUX0ZJTklTSEVEJywgKCkgPT4ge1xuXHRcdFx0XHQkdGhpcy5vbignZHJhdy5kdCcsIF9jcmVhdGVSb3dBY3Rpb25zKTtcblx0XHRcdFx0X2NyZWF0ZVJvd0FjdGlvbnMoKTtcblx0XHRcdFx0X2NyZWF0ZUJ1bGtBY3Rpb25zKCk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0XHRcblx0fSk7ICJdfQ==
