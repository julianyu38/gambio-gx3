'use strict';

/* --------------------------------------------------------------
 admin_access_admin_edit.js 2018-04-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
// ------------------------------------------------------------------------
// CONTROLLER NAME
// ------------------------------------------------------------------------
'admin_access_roles',

// ------------------------------------------------------------------------
// CONTROLLER LIBRARIES
// ------------------------------------------------------------------------
['modal', jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.js'],

// ------------------------------------------------------------------------
// CONTROLLER BUSINESS LOGIC
// ------------------------------------------------------------------------
function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Controller reference.
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default options for controller,
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final controller options.
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module object.
  *
  * @type {{}}
  */
	var module = {};

	/**
  * Modal objects.
  *
  * @type {object}
  */
	var $modal = {
		'create_edit': $('.create-edit-role-modal'),
		'delete': $('.delete-role-modal')
	};

	/**
  * Sortable list.
  *
  * @type {jQuery}
  */
	var $sortableList = $('ul.overview-list');

	/**
  * URLs object. Contains all URLs that are necessary for this controller.
  *
  * @type {object}
  */
	var urls = {
		'getRoleData': 'admin.php?do=AdminAccessAjax/getRoleData',
		'deleteRole': 'admin.php?do=AdminAccessAjax/deleteRole',
		'saveRoleData': 'admin.php?do=AdminAccessAjax/saveRoleData',
		'saveSorting': 'admin.php?do=AdminAccessAjax/saveRoleSorting'
	};

	/**
  * Object for the selected role row.
  *
  * @type {object}
  */
	var $selectedRoleRow = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Opens a modal with an error message for an unexpected error.
  */
	function _unexpectedError() {
		jse.libs.modal.showMessage(jse.core.lang.translate('error_modal_title', 'admin_access'), jse.core.lang.translate('error_ajax_request_failed', 'admin_access'));
	}

	/**
  * Clears the form inputs of the role modal.
  */
	function _clearModalForm() {
		$modal['create_edit'].find('input[name=roleId]').val('');
		$modal['create_edit'].find('input[name=roleName]').val('').removeClass('error');
		$modal['create_edit'].find('input[name=roleDescription]').val('');
		$modal['create_edit'].find('.nav-tabs li:first a').click();
	}

	/**
  * Updates the form inputs of the role modal.
  */
	function _updateModalForm(formdata) {
		_clearModalForm();

		$modal['create_edit'].find('input[name=roleId]').val(formdata['id']);
		$modal['create_edit'].find('input[name=roleSortOrder]').val(formdata['sortOrder']);
		for (var lang in formdata['names']) {
			$modal['create_edit'].find('input[name=roleName][data-lang=' + lang + ']').val(formdata['names'][lang]);
		}
		for (var _lang in formdata['descriptions']) {
			$modal['create_edit'].find('input[name=roleDescription][data-lang=' + _lang + ']').val(formdata['descriptions'][_lang]);
		}
	}

	/**
  * Return the form data of the role modal.
  */
	function _getModalFormData() {
		var roleId = parseInt($modal['create_edit'].find('input[name=roleId]').val());
		var names = {};
		var descriptions = {};
		var sortOrder = $('.overview-list li').length;

		$modal['create_edit'].find('input[name=roleName]').each(function (index, element) {
			names[$(element).data('lang')] = $(element).val();
		});
		$modal['create_edit'].find('input[name=roleDescription]').each(function (index, element) {
			descriptions[$(element).data('lang')] = $(element).val();
		});

		return {
			'id': roleId === NaN ? '' : roleId,
			'sortOrder': sortOrder,
			'names': names,
			'descriptions': descriptions
		};
	}

	/**
  * Refreshes the role overview
  */
	function _addRowToOverview(data) {
		if ($selectedRoleRow !== undefined) {
			$selectedRoleRow.find('.list-element-title').text(data['names'][jse.core.config.get('languageCode').toUpperCase()]);
		} else {
			$('.overview-list').append('\n\t\t\t\t\t<li class="col-md-12 list-element" data-list-element-id="' + data['id'] + '">\n\t\t\t\t\t\t<span class="list-element-text list-element-title col-md-8"\n\t\t\t\t\t\t      title="' + data['descriptions'][jse.core.config.get('languageCode').toUpperCase()] + '"\n\t\t\t\t\t\t>\n\t\t\t\t\t\t\t' + data['names'][jse.core.config.get('languageCode').toUpperCase()].replace(/</g, '&lt;') + '\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class="col-md-4 list-element-actions">\n\t\t\t\t\t\t\t<a href="#" class="edit-role" data-id="' + data['id'] + '"><i class="fa fa-pencil"></i></a>\n\t\t\t\t\t\t\t<a href="admin.php?do=AdminAccess/managePermissions&id=' + data['id'] + '"><i class="fa fa-cog"></i></a>\n\t\t\t\t\t\t\t<a href="#" class="delete-role" data-id="' + data['id'] + '"><i class="fa fa-trash-o"></i></a>\n\t\t\t\t\t\t\t<a href="#" class="sort-handle ui-sortable-handle"><i class="fa fa-sort"></i></a>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</li>\n\t\t\t\t');

			$('.overview-list .list-element:last a.create-role').on('click', _openCreateRoleModal);
			$('.overview-list .list-element:last a.edit-role').on('click', _openEditRoleModal);
			$('.overview-list .list-element:last a.delete-role').on('click', _openDeleteRoleModal);
			$('.overview-list .list-element:last a.sort-handle').on('click', function (event) {
				event.preventDefault();
			});
		}
	}

	// ------------------------------------------------------------------------
	// EVENT HANDLER
	// ------------------------------------------------------------------------

	/**
  * Click handler for the create role button.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _openCreateRoleModal(event) {
		// Prevent default action.
		event.preventDefault();

		$selectedRoleRow = undefined;

		// Reset modal data and open modal
		_clearModalForm();
		$modal['create_edit'].find('.modal-title').text(jse.core.lang.translate('role_modal_create_title', 'admin_access'));
		$modal['create_edit'].find('button.confirm').text(jse.core.lang.translate('BUTTON_CREATE', 'admin_buttons'));
		$modal['create_edit'].modal('show');
	}

	/**
  * Click handler for the edit role button.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _openEditRoleModal(event) {
		// Prevent default action.
		event.preventDefault();

		var roleId = $(this).data('id');
		$selectedRoleRow = $(this).closest('.list-element');

		// Load role data and open modal on success
		$.ajax({
			type: "GET",
			url: urls.getRoleData + '&roleId=' + roleId,
			success: function success(response) {
				response = JSON.parse(response);
				if (response['success'] === true) {
					_updateModalForm(response['data']);
					$modal['create_edit'].find('.modal-title').text(jse.core.lang.translate('role_modal_edit_title', 'admin_access'));
					$modal['create_edit'].find('button.confirm').text(jse.core.lang.translate('BUTTON_SAVE', 'admin_buttons'));
					$modal['create_edit'].modal('show');

					return;
				}
				_unexpectedError();
			},
			error: function error() {
				_unexpectedError();
			}
		});
	}

	/**
  * Click handler for the delete role button.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _openDeleteRoleModal(event) {
		// Prevent default action.
		event.preventDefault();

		var roleId = $(this).data('id');
		$selectedRoleRow = $(this).closest('.list-element');

		// Load role data and open modal on success
		$.ajax({
			type: "GET",
			url: urls.getRoleData + '&roleId=' + roleId,
			success: function success(response) {
				response = JSON.parse(response);
				if (response['success'] === true) {
					$modal['delete'].find('input[name=roleId]').val(response['data']['id']);
					$modal['delete'].find('fieldset .role-name').text(response['data']['names'][jse.core.config.get('languageCode').toUpperCase()]);
					$modal['delete'].find('fieldset .role-description').text(response['data']['descriptions'][jse.core.config.get('languageCode').toUpperCase()]);
					$modal['delete'].modal('show');

					return;
				}
				_unexpectedError();
			},
			error: function error() {
				_unexpectedError();
			}
		});
	}

	/**
  * Click handler for the delete modal submit button.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _submitCreateEditModalForm(event) {
		// Prevent default action.
		event.preventDefault();

		var data = _getModalFormData();
		var $emptyNameInputs = $modal['create_edit'].find('input[name=roleName]').filter(function () {
			return $(this).val() === '';
		});
		var $nonEmptyNameInputs = $modal['create_edit'].find('input[name=roleName]').filter(function () {
			return $(this).val() !== '';
		});

		if ($emptyNameInputs.size() > 0) {
			if ($nonEmptyNameInputs.size() > 0) {
				for (var lang in data.names) {
					if (data.names[lang] === '') {
						data.names[lang] = $nonEmptyNameInputs.first().val();
					}
				}
			} else {
				$emptyNameInputs.addClass('error');
				return;
			}
		}

		// Update role data
		{
			$.ajax({
				type: "POST",
				url: urls.saveRoleData,
				data: data,
				success: function success(response) {
					response = JSON.parse(response);
					if (response['success'] === true) {
						$modal['create_edit'].modal('hide');
						data['id'] = response['roleId'];
						_addRowToOverview(data);

						if ($('.overview-list-container').hasClass('empty')) {
							$('.overview-list li:not(".list-headline"):first').remove();
							$('.overview-list-container').removeClass('empty');
						}

						return;
					}
					_unexpectedError();
				},
				error: function error() {
					_unexpectedError();
				}
			});
		}
	}

	/**
  * Click handler for the create edit modal submit button.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _submitDeleteModalForm(event) {
		// Prevent default action.
		event.preventDefault();

		var roleId = $modal['delete'].find('input[name=roleId]').val();

		// Update role data
		$.ajax({
			type: "POST",
			url: urls.deleteRole,
			data: {
				'roleId': roleId
			},
			success: function success(response) {
				response = JSON.parse(response);
				if (response['success'] === true) {
					$modal['delete'].modal('hide');
					$selectedRoleRow.remove();

					if ($('.overview-list li:not(".list-headline")').length === 0) {
						$('.overview-list').append('\n\t\t\t\t\t\t\t\t<li class="col-md-12 list-element">\n\t\t\t\t\t\t\t\t\t<span class="title">' + jse.core.lang.translate('text_empty_roles_overview_list', 'admin_access') + '</span>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t');
						$('.overview-list-container').addClass('empty');
					}

					return;
				}
				_unexpectedError();
			},
			error: function error() {
				_unexpectedError();
			}
		});
	}

	/**
  * Sorting event handler for sortable plugin
  *
  * Makes a call to the ajax controller after a sorting event
  *
  * @param {object} event jQuery event object contains information of the event.
  * @param {object} ui    Sortable list (ul) object with new sort order
  */
	function _saveSorting(event, ui) {
		if (!ui.item.parent().is('ul')) {
			$sortableList.sortable('cancel');
		}

		$.ajax({
			url: urls.saveSorting,
			method: 'POST',
			data: {
				'sorting': $sortableList.sortable('toArray', { attribute: 'data-list-element-id' })
			},
			success: function success(response) {
				response = JSON.parse(response);
				if (response.success === false) {
					_unexpectedError();
				} else {
					jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('text_saved_sorting', 'admin_access'));
				}
			},
			error: function error() {
				_unexpectedError();
			}
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	module.init = function (done) {
		// initialization logic
		$('a.create-role').on('click', _openCreateRoleModal);
		$('a.edit-role').on('click', _openEditRoleModal);
		$('a.delete-role').on('click', _openDeleteRoleModal);
		$('a.sort-handle').on('click', function (event) {
			event.preventDefault();
		});

		$modal['create_edit'].find('button.confirm').on('click', _submitCreateEditModalForm);
		$modal['delete'].find('button.confirm').on('click', _submitDeleteModalForm);

		$sortableList.sortable({
			items: 'li.list-element',
			axis: 'y',
			cursor: 'move',
			handle: '.sort-handle',
			containment: 'document',
			opacity: 0.75,
			placeholder: 'col-md-12 list-element sort-placeholder'
		}).on('sortupdate', _saveSorting).disableSelection();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkbWluX2FjY2Vzcy9hZG1pbl9hY2Nlc3Nfcm9sZXMuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCIkbW9kYWwiLCIkc29ydGFibGVMaXN0IiwidXJscyIsIiRzZWxlY3RlZFJvbGVSb3ciLCJfdW5leHBlY3RlZEVycm9yIiwibGlicyIsIm1vZGFsIiwic2hvd01lc3NhZ2UiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsIl9jbGVhck1vZGFsRm9ybSIsImZpbmQiLCJ2YWwiLCJyZW1vdmVDbGFzcyIsImNsaWNrIiwiX3VwZGF0ZU1vZGFsRm9ybSIsImZvcm1kYXRhIiwiX2dldE1vZGFsRm9ybURhdGEiLCJyb2xlSWQiLCJwYXJzZUludCIsIm5hbWVzIiwiZGVzY3JpcHRpb25zIiwic29ydE9yZGVyIiwibGVuZ3RoIiwiZWFjaCIsImluZGV4IiwiZWxlbWVudCIsIk5hTiIsIl9hZGRSb3dUb092ZXJ2aWV3IiwidW5kZWZpbmVkIiwidGV4dCIsImNvbmZpZyIsImdldCIsInRvVXBwZXJDYXNlIiwiYXBwZW5kIiwicmVwbGFjZSIsIm9uIiwiX29wZW5DcmVhdGVSb2xlTW9kYWwiLCJfb3BlbkVkaXRSb2xlTW9kYWwiLCJfb3BlbkRlbGV0ZVJvbGVNb2RhbCIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJjbG9zZXN0IiwiYWpheCIsInR5cGUiLCJ1cmwiLCJnZXRSb2xlRGF0YSIsInN1Y2Nlc3MiLCJyZXNwb25zZSIsIkpTT04iLCJwYXJzZSIsImVycm9yIiwiX3N1Ym1pdENyZWF0ZUVkaXRNb2RhbEZvcm0iLCIkZW1wdHlOYW1lSW5wdXRzIiwiZmlsdGVyIiwiJG5vbkVtcHR5TmFtZUlucHV0cyIsInNpemUiLCJmaXJzdCIsImFkZENsYXNzIiwic2F2ZVJvbGVEYXRhIiwiaGFzQ2xhc3MiLCJyZW1vdmUiLCJfc3VibWl0RGVsZXRlTW9kYWxGb3JtIiwiZGVsZXRlUm9sZSIsIl9zYXZlU29ydGluZyIsInVpIiwiaXRlbSIsInBhcmVudCIsImlzIiwic29ydGFibGUiLCJzYXZlU29ydGluZyIsIm1ldGhvZCIsImF0dHJpYnV0ZSIsImluZm9fYm94IiwiYWRkU3VjY2Vzc01lc3NhZ2UiLCJpbml0IiwiaXRlbXMiLCJheGlzIiwiY3Vyc29yIiwiaGFuZGxlIiwiY29udGFpbm1lbnQiLCJvcGFjaXR5IiwicGxhY2Vob2xkZXIiLCJkaXNhYmxlU2VsZWN0aW9uIiwiZG9uZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxHQUFHQyxXQUFILENBQWVDLE1BQWY7QUFDQztBQUNBO0FBQ0E7QUFDQSxvQkFKRDs7QUFNQztBQUNBO0FBQ0E7QUFDQSxDQUNDLE9BREQsRUFFSUMsSUFBSUMsTUFGUiwwQ0FHSUQsSUFBSUMsTUFIUixvQ0FURDs7QUFlQztBQUNBO0FBQ0E7QUFDQSxVQUFTQyxJQUFULEVBQWU7QUFDZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVcsRUFBakI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUgsU0FBUyxFQUFmOztBQUVBOzs7OztBQUtBLEtBQU1TLFNBQVM7QUFDZCxpQkFBZUosRUFBRSx5QkFBRixDQUREO0FBRWQsWUFBVUEsRUFBRSxvQkFBRjtBQUZJLEVBQWY7O0FBS0E7Ozs7O0FBS0EsS0FBTUssZ0JBQWdCTCxFQUFFLGtCQUFGLENBQXRCOztBQUVBOzs7OztBQUtBLEtBQU1NLE9BQU87QUFDWixpQkFBZSwwQ0FESDtBQUVaLGdCQUFjLHlDQUZGO0FBR1osa0JBQWdCLDJDQUhKO0FBSVosaUJBQWU7QUFKSCxFQUFiOztBQU9BOzs7OztBQUtBLEtBQUlDLG1CQUFtQixFQUF2Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBLFVBQVNDLGdCQUFULEdBQTRCO0FBQzNCWixNQUFJYSxJQUFKLENBQVNDLEtBQVQsQ0FBZUMsV0FBZixDQUNDZixJQUFJZ0IsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUJBQXhCLEVBQTZDLGNBQTdDLENBREQsRUFFQ2xCLElBQUlnQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwyQkFBeEIsRUFBcUQsY0FBckQsQ0FGRDtBQUlBOztBQUVEOzs7QUFHQSxVQUFTQyxlQUFULEdBQTJCO0FBQzFCWCxTQUFPLGFBQVAsRUFDRVksSUFERixDQUNPLG9CQURQLEVBRUVDLEdBRkYsQ0FFTSxFQUZOO0FBR0FiLFNBQU8sYUFBUCxFQUNFWSxJQURGLENBQ08sc0JBRFAsRUFFRUMsR0FGRixDQUVNLEVBRk4sRUFHRUMsV0FIRixDQUdjLE9BSGQ7QUFJQWQsU0FBTyxhQUFQLEVBQ0VZLElBREYsQ0FDTyw2QkFEUCxFQUVFQyxHQUZGLENBRU0sRUFGTjtBQUdBYixTQUFPLGFBQVAsRUFDRVksSUFERixDQUNPLHNCQURQLEVBRUVHLEtBRkY7QUFHQTs7QUFFRDs7O0FBR0EsVUFBU0MsZ0JBQVQsQ0FBMEJDLFFBQTFCLEVBQW9DO0FBQ25DTjs7QUFFQVgsU0FBTyxhQUFQLEVBQ0VZLElBREYsQ0FDTyxvQkFEUCxFQUVFQyxHQUZGLENBRU1JLFNBQVMsSUFBVCxDQUZOO0FBR0FqQixTQUFPLGFBQVAsRUFDRVksSUFERixDQUNPLDJCQURQLEVBRUVDLEdBRkYsQ0FFTUksU0FBUyxXQUFULENBRk47QUFHQSxPQUFLLElBQUlSLElBQVQsSUFBaUJRLFNBQVMsT0FBVCxDQUFqQixFQUFvQztBQUNuQ2pCLFVBQU8sYUFBUCxFQUNFWSxJQURGLENBQ08sb0NBQW9DSCxJQUFwQyxHQUEyQyxHQURsRCxFQUVFSSxHQUZGLENBRU1JLFNBQVMsT0FBVCxFQUFrQlIsSUFBbEIsQ0FGTjtBQUdBO0FBQ0QsT0FBSyxJQUFJQSxLQUFULElBQWlCUSxTQUFTLGNBQVQsQ0FBakIsRUFBMkM7QUFDMUNqQixVQUFPLGFBQVAsRUFDRVksSUFERixDQUNPLDJDQUEyQ0gsS0FBM0MsR0FBa0QsR0FEekQsRUFFRUksR0FGRixDQUVNSSxTQUFTLGNBQVQsRUFBeUJSLEtBQXpCLENBRk47QUFHQTtBQUNEOztBQUVEOzs7QUFHQSxVQUFTUyxpQkFBVCxHQUE2QjtBQUM1QixNQUFNQyxTQUFTQyxTQUFTcEIsT0FBTyxhQUFQLEVBQXNCWSxJQUF0QixDQUEyQixvQkFBM0IsRUFBaURDLEdBQWpELEVBQVQsQ0FBZjtBQUNBLE1BQUlRLFFBQVEsRUFBWjtBQUNBLE1BQUlDLGVBQWUsRUFBbkI7QUFDQSxNQUFNQyxZQUFZM0IsRUFBRSxtQkFBRixFQUF1QjRCLE1BQXpDOztBQUVBeEIsU0FBTyxhQUFQLEVBQ0VZLElBREYsQ0FDTyxzQkFEUCxFQUVFYSxJQUZGLENBRU8sVUFBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQW9CO0FBQ3pCTixTQUFNekIsRUFBRStCLE9BQUYsRUFBV2pDLElBQVgsQ0FBZ0IsTUFBaEIsQ0FBTixJQUFpQ0UsRUFBRStCLE9BQUYsRUFBV2QsR0FBWCxFQUFqQztBQUNBLEdBSkY7QUFLQWIsU0FBTyxhQUFQLEVBQ0VZLElBREYsQ0FDTyw2QkFEUCxFQUVFYSxJQUZGLENBRU8sVUFBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQW9CO0FBQ3pCTCxnQkFBYTFCLEVBQUUrQixPQUFGLEVBQVdqQyxJQUFYLENBQWdCLE1BQWhCLENBQWIsSUFBd0NFLEVBQUUrQixPQUFGLEVBQVdkLEdBQVgsRUFBeEM7QUFDQSxHQUpGOztBQU1BLFNBQU87QUFDTixTQUFNTSxXQUFXUyxHQUFYLEdBQWlCLEVBQWpCLEdBQXNCVCxNQUR0QjtBQUVOLGdCQUFhSSxTQUZQO0FBR04sWUFBU0YsS0FISDtBQUlOLG1CQUFnQkM7QUFKVixHQUFQO0FBTUE7O0FBRUQ7OztBQUdBLFVBQVNPLGlCQUFULENBQTJCbkMsSUFBM0IsRUFBaUM7QUFDaEMsTUFBSVMscUJBQXFCMkIsU0FBekIsRUFBb0M7QUFDbkMzQixvQkFDRVMsSUFERixDQUNPLHFCQURQLEVBRUVtQixJQUZGLENBRU9yQyxLQUFLLE9BQUwsRUFBY0YsSUFBSWdCLElBQUosQ0FBU3dCLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLEVBQW9DQyxXQUFwQyxFQUFkLENBRlA7QUFHQSxHQUpELE1BSU87QUFDTnRDLEtBQUUsZ0JBQUYsRUFBb0J1QyxNQUFwQixDQUEyQiwwRUFDbUN6QyxLQUFLLElBQUwsQ0FEbkMsOEdBR1JBLEtBQUssY0FBTCxFQUFxQkYsSUFBSWdCLElBQUosQ0FBU3dCLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLEVBQW9DQyxXQUFwQyxFQUFyQixDQUhRLHdDQUtwQnhDLEtBQUssT0FBTCxFQUFjRixJQUFJZ0IsSUFBSixDQUFTd0IsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsY0FBcEIsRUFBb0NDLFdBQXBDLEVBQWQsRUFBaUVFLE9BQWpFLENBQXlFLElBQXpFLEVBQStFLE1BQS9FLENBTG9CLDhJQVNtQjFDLEtBQUssSUFBTCxDQVRuQixpSEFVbUNBLEtBQUssSUFBTCxDQVZuQyxnR0FXcUJBLEtBQUssSUFBTCxDQVhyQix5TEFBM0I7O0FBaUJBRSxLQUFFLGlEQUFGLEVBQXFEeUMsRUFBckQsQ0FBd0QsT0FBeEQsRUFBaUVDLG9CQUFqRTtBQUNBMUMsS0FBRSwrQ0FBRixFQUFtRHlDLEVBQW5ELENBQXNELE9BQXRELEVBQStERSxrQkFBL0Q7QUFDQTNDLEtBQUUsaURBQUYsRUFBcUR5QyxFQUFyRCxDQUF3RCxPQUF4RCxFQUFpRUcsb0JBQWpFO0FBQ0E1QyxLQUFFLGlEQUFGLEVBQXFEeUMsRUFBckQsQ0FBd0QsT0FBeEQsRUFBaUUsVUFBQ0ksS0FBRCxFQUFXO0FBQzNFQSxVQUFNQyxjQUFOO0FBQ0EsSUFGRDtBQUdBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNKLG9CQUFULENBQThCRyxLQUE5QixFQUFxQztBQUNwQztBQUNBQSxRQUFNQyxjQUFOOztBQUVBdkMscUJBQW1CMkIsU0FBbkI7O0FBRUE7QUFDQW5CO0FBQ0FYLFNBQU8sYUFBUCxFQUNFWSxJQURGLENBQ08sY0FEUCxFQUVFbUIsSUFGRixDQUVPdkMsSUFBSWdCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHlCQUF4QixFQUFtRCxjQUFuRCxDQUZQO0FBR0FWLFNBQU8sYUFBUCxFQUNFWSxJQURGLENBQ08sZ0JBRFAsRUFFRW1CLElBRkYsQ0FFT3ZDLElBQUlnQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixlQUF4QixFQUF5QyxlQUF6QyxDQUZQO0FBR0FWLFNBQU8sYUFBUCxFQUNFTSxLQURGLENBQ1EsTUFEUjtBQUVBOztBQUVEOzs7OztBQUtBLFVBQVNpQyxrQkFBVCxDQUE0QkUsS0FBNUIsRUFBbUM7QUFDbEM7QUFDQUEsUUFBTUMsY0FBTjs7QUFFQSxNQUFNdkIsU0FBU3ZCLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsSUFBYixDQUFmO0FBQ0FTLHFCQUFtQlAsRUFBRSxJQUFGLEVBQVErQyxPQUFSLENBQWdCLGVBQWhCLENBQW5COztBQUVBO0FBQ0EvQyxJQUFFZ0QsSUFBRixDQUFPO0FBQ05DLFNBQU0sS0FEQTtBQUVOQyxRQUFLNUMsS0FBSzZDLFdBQUwsR0FBbUIsVUFBbkIsR0FBZ0M1QixNQUYvQjtBQUdONkIsWUFBUyxpQkFBQ0MsUUFBRCxFQUFjO0FBQ3RCQSxlQUFXQyxLQUFLQyxLQUFMLENBQVdGLFFBQVgsQ0FBWDtBQUNBLFFBQUlBLFNBQVMsU0FBVCxNQUF3QixJQUE1QixFQUFrQztBQUNqQ2pDLHNCQUFpQmlDLFNBQVMsTUFBVCxDQUFqQjtBQUNBakQsWUFBTyxhQUFQLEVBQ0VZLElBREYsQ0FDTyxjQURQLEVBRUVtQixJQUZGLENBRU92QyxJQUFJZ0IsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsdUJBQXhCLEVBQWlELGNBQWpELENBRlA7QUFHQVYsWUFBTyxhQUFQLEVBQ0VZLElBREYsQ0FDTyxnQkFEUCxFQUVFbUIsSUFGRixDQUVPdkMsSUFBSWdCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGFBQXhCLEVBQXVDLGVBQXZDLENBRlA7QUFHQVYsWUFBTyxhQUFQLEVBQ0VNLEtBREYsQ0FDUSxNQURSOztBQUdBO0FBQ0E7QUFDREY7QUFDQSxJQW5CSztBQW9CTmdELFVBQU8saUJBQU07QUFDWmhEO0FBQ0E7QUF0QkssR0FBUDtBQXdCQTs7QUFFRDs7Ozs7QUFLQSxVQUFTb0Msb0JBQVQsQ0FBOEJDLEtBQTlCLEVBQXFDO0FBQ3BDO0FBQ0FBLFFBQU1DLGNBQU47O0FBRUEsTUFBTXZCLFNBQVN2QixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLElBQWIsQ0FBZjtBQUNBUyxxQkFBbUJQLEVBQUUsSUFBRixFQUFRK0MsT0FBUixDQUFnQixlQUFoQixDQUFuQjs7QUFFQTtBQUNBL0MsSUFBRWdELElBQUYsQ0FBTztBQUNOQyxTQUFNLEtBREE7QUFFTkMsUUFBSzVDLEtBQUs2QyxXQUFMLEdBQW1CLFVBQW5CLEdBQWdDNUIsTUFGL0I7QUFHTjZCLFlBQVMsaUJBQUNDLFFBQUQsRUFBYztBQUN0QkEsZUFBV0MsS0FBS0MsS0FBTCxDQUFXRixRQUFYLENBQVg7QUFDQSxRQUFJQSxTQUFTLFNBQVQsTUFBd0IsSUFBNUIsRUFBa0M7QUFDakNqRCxZQUFPLFFBQVAsRUFDRVksSUFERixDQUNPLG9CQURQLEVBRUVDLEdBRkYsQ0FFTW9DLFNBQVMsTUFBVCxFQUFpQixJQUFqQixDQUZOO0FBR0FqRCxZQUFPLFFBQVAsRUFDRVksSUFERixDQUNPLHFCQURQLEVBRUVtQixJQUZGLENBRU9rQixTQUFTLE1BQVQsRUFBaUIsT0FBakIsRUFBMEJ6RCxJQUFJZ0IsSUFBSixDQUFTd0IsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsY0FBcEIsRUFBb0NDLFdBQXBDLEVBQTFCLENBRlA7QUFHQWxDLFlBQU8sUUFBUCxFQUNFWSxJQURGLENBQ08sNEJBRFAsRUFFRW1CLElBRkYsQ0FFT2tCLFNBQVMsTUFBVCxFQUFpQixjQUFqQixFQUFpQ3pELElBQUlnQixJQUFKLENBQVN3QixNQUFULENBQWdCQyxHQUFoQixDQUFvQixjQUFwQixFQUFvQ0MsV0FBcEMsRUFBakMsQ0FGUDtBQUdBbEMsWUFBTyxRQUFQLEVBQ0VNLEtBREYsQ0FDUSxNQURSOztBQUdBO0FBQ0E7QUFDREY7QUFDQSxJQXJCSztBQXNCTmdELFVBQU8saUJBQU07QUFDWmhEO0FBQ0E7QUF4QkssR0FBUDtBQTBCQTs7QUFFRDs7Ozs7QUFLQSxVQUFTaUQsMEJBQVQsQ0FBb0NaLEtBQXBDLEVBQTJDO0FBQzFDO0FBQ0FBLFFBQU1DLGNBQU47O0FBRUEsTUFBTWhELE9BQU93QixtQkFBYjtBQUNBLE1BQU1vQyxtQkFBbUJ0RCxPQUFPLGFBQVAsRUFBc0JZLElBQXRCLENBQTJCLHNCQUEzQixFQUFtRDJDLE1BQW5ELENBQTBELFlBQVc7QUFDN0YsVUFBTzNELEVBQUUsSUFBRixFQUFRaUIsR0FBUixPQUFrQixFQUF6QjtBQUNBLEdBRndCLENBQXpCO0FBR0EsTUFBTTJDLHNCQUFzQnhELE9BQU8sYUFBUCxFQUFzQlksSUFBdEIsQ0FBMkIsc0JBQTNCLEVBQW1EMkMsTUFBbkQsQ0FBMEQsWUFBVztBQUNoRyxVQUFPM0QsRUFBRSxJQUFGLEVBQVFpQixHQUFSLE9BQWtCLEVBQXpCO0FBQ0EsR0FGMkIsQ0FBNUI7O0FBSUEsTUFBSXlDLGlCQUFpQkcsSUFBakIsS0FBMEIsQ0FBOUIsRUFBaUM7QUFDaEMsT0FBSUQsb0JBQW9CQyxJQUFwQixLQUE2QixDQUFqQyxFQUFvQztBQUNuQyxTQUFLLElBQUloRCxJQUFULElBQWlCZixLQUFLMkIsS0FBdEIsRUFBNkI7QUFDNUIsU0FBSTNCLEtBQUsyQixLQUFMLENBQVdaLElBQVgsTUFBcUIsRUFBekIsRUFBNkI7QUFDNUJmLFdBQUsyQixLQUFMLENBQVdaLElBQVgsSUFBbUIrQyxvQkFBb0JFLEtBQXBCLEdBQTRCN0MsR0FBNUIsRUFBbkI7QUFDQTtBQUNEO0FBQ0QsSUFORCxNQU1PO0FBQ055QyxxQkFBaUJLLFFBQWpCLENBQTBCLE9BQTFCO0FBQ0E7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQy9ELEtBQUVnRCxJQUFGLENBQU87QUFDTkMsVUFBTSxNQURBO0FBRU5DLFNBQUs1QyxLQUFLMEQsWUFGSjtBQUdObEUsVUFBTUEsSUFIQTtBQUlOc0QsYUFBUyxpQkFBQ0MsUUFBRCxFQUFjO0FBQ3RCQSxnQkFBV0MsS0FBS0MsS0FBTCxDQUFXRixRQUFYLENBQVg7QUFDQSxTQUFJQSxTQUFTLFNBQVQsTUFBd0IsSUFBNUIsRUFBa0M7QUFDakNqRCxhQUFPLGFBQVAsRUFDRU0sS0FERixDQUNRLE1BRFI7QUFFQVosV0FBSyxJQUFMLElBQWF1RCxTQUFTLFFBQVQsQ0FBYjtBQUNBcEIsd0JBQWtCbkMsSUFBbEI7O0FBRUEsVUFBSUUsRUFBRSwwQkFBRixFQUE4QmlFLFFBQTlCLENBQXVDLE9BQXZDLENBQUosRUFBcUQ7QUFDcERqRSxTQUFFLCtDQUFGLEVBQW1Ea0UsTUFBbkQ7QUFDQWxFLFNBQUUsMEJBQUYsRUFBOEJrQixXQUE5QixDQUEwQyxPQUExQztBQUNBOztBQUVEO0FBQ0E7QUFDRFY7QUFDQSxLQXBCSztBQXFCTmdELFdBQU8saUJBQU07QUFDWmhEO0FBQ0E7QUF2QkssSUFBUDtBQXlCQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVMyRCxzQkFBVCxDQUFnQ3RCLEtBQWhDLEVBQXVDO0FBQ3RDO0FBQ0FBLFFBQU1DLGNBQU47O0FBRUEsTUFBTXZCLFNBQVNuQixPQUFPLFFBQVAsRUFBaUJZLElBQWpCLENBQXNCLG9CQUF0QixFQUE0Q0MsR0FBNUMsRUFBZjs7QUFFQTtBQUNBakIsSUFBRWdELElBQUYsQ0FBTztBQUNOQyxTQUFNLE1BREE7QUFFTkMsUUFBSzVDLEtBQUs4RCxVQUZKO0FBR050RSxTQUFNO0FBQ0wsY0FBVXlCO0FBREwsSUFIQTtBQU1ONkIsWUFBUyxpQkFBQ0MsUUFBRCxFQUFjO0FBQ3RCQSxlQUFXQyxLQUFLQyxLQUFMLENBQVdGLFFBQVgsQ0FBWDtBQUNBLFFBQUlBLFNBQVMsU0FBVCxNQUF3QixJQUE1QixFQUFrQztBQUNqQ2pELFlBQU8sUUFBUCxFQUNFTSxLQURGLENBQ1EsTUFEUjtBQUVBSCxzQkFBaUIyRCxNQUFqQjs7QUFFQSxTQUFJbEUsRUFBRSx5Q0FBRixFQUE2QzRCLE1BQTdDLEtBQXdELENBQTVELEVBQStEO0FBQzlENUIsUUFBRSxnQkFBRixFQUFvQnVDLE1BQXBCLENBQTJCLGtHQUd4QjNDLElBQUlnQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixnQ0FBeEIsRUFBMEQsY0FBMUQsQ0FId0IsbURBQTNCO0FBTUFkLFFBQUUsMEJBQUYsRUFBOEIrRCxRQUE5QixDQUF1QyxPQUF2QztBQUNBOztBQUVEO0FBQ0E7QUFDRHZEO0FBQ0EsSUExQks7QUEyQk5nRCxVQUFPLGlCQUFNO0FBQ1poRDtBQUNBO0FBN0JLLEdBQVA7QUErQkE7O0FBRUQ7Ozs7Ozs7O0FBUUEsVUFBUzZELFlBQVQsQ0FBc0J4QixLQUF0QixFQUE2QnlCLEVBQTdCLEVBQWlDO0FBQ2hDLE1BQUksQ0FBQ0EsR0FBR0MsSUFBSCxDQUFRQyxNQUFSLEdBQWlCQyxFQUFqQixDQUFvQixJQUFwQixDQUFMLEVBQWdDO0FBQy9CcEUsaUJBQWNxRSxRQUFkLENBQXVCLFFBQXZCO0FBQ0E7O0FBRUQxRSxJQUFFZ0QsSUFBRixDQUFPO0FBQ05FLFFBQUs1QyxLQUFLcUUsV0FESjtBQUVOQyxXQUFRLE1BRkY7QUFHTjlFLFNBQU07QUFDTCxlQUFXTyxjQUFjcUUsUUFBZCxDQUF1QixTQUF2QixFQUFrQyxFQUFDRyxXQUFXLHNCQUFaLEVBQWxDO0FBRE4sSUFIQTtBQU1OekIsWUFBUyxpQkFBU0MsUUFBVCxFQUFtQjtBQUMzQkEsZUFBV0MsS0FBS0MsS0FBTCxDQUFXRixRQUFYLENBQVg7QUFDQSxRQUFJQSxTQUFTRCxPQUFULEtBQXFCLEtBQXpCLEVBQWdDO0FBQy9CNUM7QUFDQSxLQUZELE1BRU87QUFDTlosU0FBSWEsSUFBSixDQUFTcUUsUUFBVCxDQUFrQkMsaUJBQWxCLENBQW9DbkYsSUFBSWdCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG9CQUF4QixFQUE4QyxjQUE5QyxDQUFwQztBQUNBO0FBQ0QsSUFiSztBQWNOMEMsVUFBTyxpQkFBVztBQUNqQmhEO0FBQ0E7QUFoQkssR0FBUDtBQWtCQTs7QUFFRDtBQUNBO0FBQ0E7QUFDQWIsUUFBT3FGLElBQVAsR0FBYyxnQkFBUTtBQUNyQjtBQUNBaEYsSUFBRSxlQUFGLEVBQW1CeUMsRUFBbkIsQ0FBc0IsT0FBdEIsRUFBK0JDLG9CQUEvQjtBQUNBMUMsSUFBRSxhQUFGLEVBQWlCeUMsRUFBakIsQ0FBb0IsT0FBcEIsRUFBNkJFLGtCQUE3QjtBQUNBM0MsSUFBRSxlQUFGLEVBQW1CeUMsRUFBbkIsQ0FBc0IsT0FBdEIsRUFBK0JHLG9CQUEvQjtBQUNBNUMsSUFBRSxlQUFGLEVBQW1CeUMsRUFBbkIsQ0FBc0IsT0FBdEIsRUFBK0IsVUFBQ0ksS0FBRCxFQUFXO0FBQ3pDQSxTQUFNQyxjQUFOO0FBQ0EsR0FGRDs7QUFJQTFDLFNBQU8sYUFBUCxFQUNFWSxJQURGLENBQ08sZ0JBRFAsRUFFRXlCLEVBRkYsQ0FFSyxPQUZMLEVBRWNnQiwwQkFGZDtBQUdBckQsU0FBTyxRQUFQLEVBQ0VZLElBREYsQ0FDTyxnQkFEUCxFQUVFeUIsRUFGRixDQUVLLE9BRkwsRUFFYzBCLHNCQUZkOztBQUlBOUQsZ0JBQ0VxRSxRQURGLENBQ1c7QUFDVE8sVUFBTyxpQkFERTtBQUVUQyxTQUFNLEdBRkc7QUFHVEMsV0FBUSxNQUhDO0FBSVRDLFdBQVEsY0FKQztBQUtUQyxnQkFBYSxVQUxKO0FBTVRDLFlBQVMsSUFOQTtBQU9UQyxnQkFBYTtBQVBKLEdBRFgsRUFVRTlDLEVBVkYsQ0FVSyxZQVZMLEVBVW1CNEIsWUFWbkIsRUFXRW1CLGdCQVhGOztBQWFBQztBQUNBLEVBOUJEOztBQWdDQSxRQUFPOUYsTUFBUDtBQUNBLENBMWVGIiwiZmlsZSI6ImFkbWluX2FjY2Vzcy9hZG1pbl9hY2Nlc3Nfcm9sZXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGFkbWluX2FjY2Vzc19hZG1pbl9lZGl0LmpzIDIwMTgtMDQtMDVcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBDT05UUk9MTEVSIE5BTUVcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdCdhZG1pbl9hY2Nlc3Nfcm9sZXMnLFxuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIENPTlRST0xMRVIgTElCUkFSSUVTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRbXG5cdFx0J21vZGFsJyxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LXVpL2pxdWVyeS11aS5taW4uY3NzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LXVpL2pxdWVyeS11aS5qc2Bcblx0XSxcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBDT05UUk9MTEVSIEJVU0lORVNTIExPR0lDXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENvbnRyb2xsZXIgcmVmZXJlbmNlLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBvcHRpb25zIGZvciBjb250cm9sbGVyLFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBkZWZhdWx0cyA9IHt9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmFsIGNvbnRyb2xsZXIgb3B0aW9ucy5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIG9iamVjdC5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHt7fX1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2RhbCBvYmplY3RzLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCAkbW9kYWwgPSB7XG5cdFx0XHQnY3JlYXRlX2VkaXQnOiAkKCcuY3JlYXRlLWVkaXQtcm9sZS1tb2RhbCcpLFxuXHRcdFx0J2RlbGV0ZSc6ICQoJy5kZWxldGUtcm9sZS1tb2RhbCcpXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTb3J0YWJsZSBsaXN0LlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkc29ydGFibGVMaXN0ID0gJCgndWwub3ZlcnZpZXctbGlzdCcpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVSTHMgb2JqZWN0LiBDb250YWlucyBhbGwgVVJMcyB0aGF0IGFyZSBuZWNlc3NhcnkgZm9yIHRoaXMgY29udHJvbGxlci5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgdXJscyA9IHtcblx0XHRcdCdnZXRSb2xlRGF0YSc6ICdhZG1pbi5waHA/ZG89QWRtaW5BY2Nlc3NBamF4L2dldFJvbGVEYXRhJyxcblx0XHRcdCdkZWxldGVSb2xlJzogJ2FkbWluLnBocD9kbz1BZG1pbkFjY2Vzc0FqYXgvZGVsZXRlUm9sZScsXG5cdFx0XHQnc2F2ZVJvbGVEYXRhJzogJ2FkbWluLnBocD9kbz1BZG1pbkFjY2Vzc0FqYXgvc2F2ZVJvbGVEYXRhJyxcblx0XHRcdCdzYXZlU29ydGluZyc6ICdhZG1pbi5waHA/ZG89QWRtaW5BY2Nlc3NBamF4L3NhdmVSb2xlU29ydGluZydcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9iamVjdCBmb3IgdGhlIHNlbGVjdGVkIHJvbGUgcm93LlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRsZXQgJHNlbGVjdGVkUm9sZVJvdyA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFBSSVZBVEUgTUVUSE9EU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9wZW5zIGEgbW9kYWwgd2l0aCBhbiBlcnJvciBtZXNzYWdlIGZvciBhbiB1bmV4cGVjdGVkIGVycm9yLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF91bmV4cGVjdGVkRXJyb3IoKSB7XG5cdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShcblx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yX21vZGFsX3RpdGxlJywgJ2FkbWluX2FjY2VzcycpLFxuXHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3JfYWpheF9yZXF1ZXN0X2ZhaWxlZCcsICdhZG1pbl9hY2Nlc3MnKVxuXHRcdFx0KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xlYXJzIHRoZSBmb3JtIGlucHV0cyBvZiB0aGUgcm9sZSBtb2RhbC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfY2xlYXJNb2RhbEZvcm0oKSB7XG5cdFx0XHQkbW9kYWxbJ2NyZWF0ZV9lZGl0J11cblx0XHRcdFx0LmZpbmQoJ2lucHV0W25hbWU9cm9sZUlkXScpXG5cdFx0XHRcdC52YWwoJycpO1xuXHRcdFx0JG1vZGFsWydjcmVhdGVfZWRpdCddXG5cdFx0XHRcdC5maW5kKCdpbnB1dFtuYW1lPXJvbGVOYW1lXScpXG5cdFx0XHRcdC52YWwoJycpXG5cdFx0XHRcdC5yZW1vdmVDbGFzcygnZXJyb3InKTtcblx0XHRcdCRtb2RhbFsnY3JlYXRlX2VkaXQnXVxuXHRcdFx0XHQuZmluZCgnaW5wdXRbbmFtZT1yb2xlRGVzY3JpcHRpb25dJylcblx0XHRcdFx0LnZhbCgnJyk7XG5cdFx0XHQkbW9kYWxbJ2NyZWF0ZV9lZGl0J11cblx0XHRcdFx0LmZpbmQoJy5uYXYtdGFicyBsaTpmaXJzdCBhJylcblx0XHRcdFx0LmNsaWNrKCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZXMgdGhlIGZvcm0gaW5wdXRzIG9mIHRoZSByb2xlIG1vZGFsLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF91cGRhdGVNb2RhbEZvcm0oZm9ybWRhdGEpIHtcblx0XHRcdF9jbGVhck1vZGFsRm9ybSgpO1xuXHRcdFx0XG5cdFx0XHQkbW9kYWxbJ2NyZWF0ZV9lZGl0J11cblx0XHRcdFx0LmZpbmQoJ2lucHV0W25hbWU9cm9sZUlkXScpXG5cdFx0XHRcdC52YWwoZm9ybWRhdGFbJ2lkJ10pO1xuXHRcdFx0JG1vZGFsWydjcmVhdGVfZWRpdCddXG5cdFx0XHRcdC5maW5kKCdpbnB1dFtuYW1lPXJvbGVTb3J0T3JkZXJdJylcblx0XHRcdFx0LnZhbChmb3JtZGF0YVsnc29ydE9yZGVyJ10pO1xuXHRcdFx0Zm9yIChsZXQgbGFuZyBpbiBmb3JtZGF0YVsnbmFtZXMnXSkge1xuXHRcdFx0XHQkbW9kYWxbJ2NyZWF0ZV9lZGl0J11cblx0XHRcdFx0XHQuZmluZCgnaW5wdXRbbmFtZT1yb2xlTmFtZV1bZGF0YS1sYW5nPScgKyBsYW5nICsgJ10nKVxuXHRcdFx0XHRcdC52YWwoZm9ybWRhdGFbJ25hbWVzJ11bbGFuZ10pO1xuXHRcdFx0fVxuXHRcdFx0Zm9yIChsZXQgbGFuZyBpbiBmb3JtZGF0YVsnZGVzY3JpcHRpb25zJ10pIHtcblx0XHRcdFx0JG1vZGFsWydjcmVhdGVfZWRpdCddXG5cdFx0XHRcdFx0LmZpbmQoJ2lucHV0W25hbWU9cm9sZURlc2NyaXB0aW9uXVtkYXRhLWxhbmc9JyArIGxhbmcgKyAnXScpXG5cdFx0XHRcdFx0LnZhbChmb3JtZGF0YVsnZGVzY3JpcHRpb25zJ11bbGFuZ10pO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXR1cm4gdGhlIGZvcm0gZGF0YSBvZiB0aGUgcm9sZSBtb2RhbC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0TW9kYWxGb3JtRGF0YSgpIHtcblx0XHRcdGNvbnN0IHJvbGVJZCA9IHBhcnNlSW50KCRtb2RhbFsnY3JlYXRlX2VkaXQnXS5maW5kKCdpbnB1dFtuYW1lPXJvbGVJZF0nKS52YWwoKSk7XG5cdFx0XHRsZXQgbmFtZXMgPSB7fTtcblx0XHRcdGxldCBkZXNjcmlwdGlvbnMgPSB7fTtcblx0XHRcdGNvbnN0IHNvcnRPcmRlciA9ICQoJy5vdmVydmlldy1saXN0IGxpJykubGVuZ3RoO1xuXHRcdFx0XG5cdFx0XHQkbW9kYWxbJ2NyZWF0ZV9lZGl0J11cblx0XHRcdFx0LmZpbmQoJ2lucHV0W25hbWU9cm9sZU5hbWVdJylcblx0XHRcdFx0LmVhY2goKGluZGV4LCBlbGVtZW50KSA9PiB7XG5cdFx0XHRcdFx0bmFtZXNbJChlbGVtZW50KS5kYXRhKCdsYW5nJyldID0gJChlbGVtZW50KS52YWwoKTtcblx0XHRcdFx0fSk7XG5cdFx0XHQkbW9kYWxbJ2NyZWF0ZV9lZGl0J11cblx0XHRcdFx0LmZpbmQoJ2lucHV0W25hbWU9cm9sZURlc2NyaXB0aW9uXScpXG5cdFx0XHRcdC5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4ge1xuXHRcdFx0XHRcdGRlc2NyaXB0aW9uc1skKGVsZW1lbnQpLmRhdGEoJ2xhbmcnKV0gPSAkKGVsZW1lbnQpLnZhbCgpO1xuXHRcdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0J2lkJzogcm9sZUlkID09PSBOYU4gPyAnJyA6IHJvbGVJZCxcblx0XHRcdFx0J3NvcnRPcmRlcic6IHNvcnRPcmRlcixcblx0XHRcdFx0J25hbWVzJzogbmFtZXMsXG5cdFx0XHRcdCdkZXNjcmlwdGlvbnMnOiBkZXNjcmlwdGlvbnNcblx0XHRcdH07XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlZnJlc2hlcyB0aGUgcm9sZSBvdmVydmlld1xuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9hZGRSb3dUb092ZXJ2aWV3KGRhdGEpIHtcblx0XHRcdGlmICgkc2VsZWN0ZWRSb2xlUm93ICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0JHNlbGVjdGVkUm9sZVJvd1xuXHRcdFx0XHRcdC5maW5kKCcubGlzdC1lbGVtZW50LXRpdGxlJylcblx0XHRcdFx0XHQudGV4dChkYXRhWyduYW1lcyddW2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpLnRvVXBwZXJDYXNlKCldKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCQoJy5vdmVydmlldy1saXN0JykuYXBwZW5kKGBcblx0XHRcdFx0XHQ8bGkgY2xhc3M9XCJjb2wtbWQtMTIgbGlzdC1lbGVtZW50XCIgZGF0YS1saXN0LWVsZW1lbnQtaWQ9XCJgICsgZGF0YVsnaWQnXSArIGBcIj5cblx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwibGlzdC1lbGVtZW50LXRleHQgbGlzdC1lbGVtZW50LXRpdGxlIGNvbC1tZC04XCJcblx0XHRcdFx0XHRcdCAgICAgIHRpdGxlPVwiYCArIGRhdGFbJ2Rlc2NyaXB0aW9ucyddW2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpLnRvVXBwZXJDYXNlKCldICsgYFwiXG5cdFx0XHRcdFx0XHQ+XG5cdFx0XHRcdFx0XHRcdGAgKyBkYXRhWyduYW1lcyddW2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpLnRvVXBwZXJDYXNlKCldLnJlcGxhY2UoLzwvZywgJyZsdDsnKVxuXHRcdFx0XHRcdCsgYFxuXHRcdFx0XHRcdFx0PC9zcGFuPlxuXHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJjb2wtbWQtNCBsaXN0LWVsZW1lbnQtYWN0aW9uc1wiPlxuXHRcdFx0XHRcdFx0XHQ8YSBocmVmPVwiI1wiIGNsYXNzPVwiZWRpdC1yb2xlXCIgZGF0YS1pZD1cImAgKyBkYXRhWydpZCddICsgYFwiPjxpIGNsYXNzPVwiZmEgZmEtcGVuY2lsXCI+PC9pPjwvYT5cblx0XHRcdFx0XHRcdFx0PGEgaHJlZj1cImFkbWluLnBocD9kbz1BZG1pbkFjY2Vzcy9tYW5hZ2VQZXJtaXNzaW9ucyZpZD1gICsgZGF0YVsnaWQnXSArIGBcIj48aSBjbGFzcz1cImZhIGZhLWNvZ1wiPjwvaT48L2E+XG5cdFx0XHRcdFx0XHRcdDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJkZWxldGUtcm9sZVwiIGRhdGEtaWQ9XCJgICsgZGF0YVsnaWQnXSArIGBcIj48aSBjbGFzcz1cImZhIGZhLXRyYXNoLW9cIj48L2k+PC9hPlxuXHRcdFx0XHRcdFx0XHQ8YSBocmVmPVwiI1wiIGNsYXNzPVwic29ydC1oYW5kbGUgdWktc29ydGFibGUtaGFuZGxlXCI+PGkgY2xhc3M9XCJmYSBmYS1zb3J0XCI+PC9pPjwvYT5cblx0XHRcdFx0XHRcdDwvc3Bhbj5cblx0XHRcdFx0XHQ8L2xpPlxuXHRcdFx0XHRgKTtcblx0XHRcdFx0XG5cdFx0XHRcdCQoJy5vdmVydmlldy1saXN0IC5saXN0LWVsZW1lbnQ6bGFzdCBhLmNyZWF0ZS1yb2xlJykub24oJ2NsaWNrJywgX29wZW5DcmVhdGVSb2xlTW9kYWwpO1xuXHRcdFx0XHQkKCcub3ZlcnZpZXctbGlzdCAubGlzdC1lbGVtZW50Omxhc3QgYS5lZGl0LXJvbGUnKS5vbignY2xpY2snLCBfb3BlbkVkaXRSb2xlTW9kYWwpO1xuXHRcdFx0XHQkKCcub3ZlcnZpZXctbGlzdCAubGlzdC1lbGVtZW50Omxhc3QgYS5kZWxldGUtcm9sZScpLm9uKCdjbGljaycsIF9vcGVuRGVsZXRlUm9sZU1vZGFsKTtcblx0XHRcdFx0JCgnLm92ZXJ2aWV3LWxpc3QgLmxpc3QtZWxlbWVudDpsYXN0IGEuc29ydC1oYW5kbGUnKS5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcblx0XHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsaWNrIGhhbmRsZXIgZm9yIHRoZSBjcmVhdGUgcm9sZSBidXR0b24uXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdCBjb250YWlucyBpbmZvcm1hdGlvbiBvZiB0aGUgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29wZW5DcmVhdGVSb2xlTW9kYWwoZXZlbnQpIHtcblx0XHRcdC8vIFByZXZlbnQgZGVmYXVsdCBhY3Rpb24uXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHQkc2VsZWN0ZWRSb2xlUm93ID0gdW5kZWZpbmVkO1xuXHRcdFx0XG5cdFx0XHQvLyBSZXNldCBtb2RhbCBkYXRhIGFuZCBvcGVuIG1vZGFsXG5cdFx0XHRfY2xlYXJNb2RhbEZvcm0oKTtcblx0XHRcdCRtb2RhbFsnY3JlYXRlX2VkaXQnXVxuXHRcdFx0XHQuZmluZCgnLm1vZGFsLXRpdGxlJylcblx0XHRcdFx0LnRleHQoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3JvbGVfbW9kYWxfY3JlYXRlX3RpdGxlJywgJ2FkbWluX2FjY2VzcycpKTtcblx0XHRcdCRtb2RhbFsnY3JlYXRlX2VkaXQnXVxuXHRcdFx0XHQuZmluZCgnYnV0dG9uLmNvbmZpcm0nKVxuXHRcdFx0XHQudGV4dChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX0NSRUFURScsICdhZG1pbl9idXR0b25zJykpO1xuXHRcdFx0JG1vZGFsWydjcmVhdGVfZWRpdCddXG5cdFx0XHRcdC5tb2RhbCgnc2hvdycpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDbGljayBoYW5kbGVyIGZvciB0aGUgZWRpdCByb2xlIGJ1dHRvbi5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb3BlbkVkaXRSb2xlTW9kYWwoZXZlbnQpIHtcblx0XHRcdC8vIFByZXZlbnQgZGVmYXVsdCBhY3Rpb24uXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCByb2xlSWQgPSAkKHRoaXMpLmRhdGEoJ2lkJyk7XG5cdFx0XHQkc2VsZWN0ZWRSb2xlUm93ID0gJCh0aGlzKS5jbG9zZXN0KCcubGlzdC1lbGVtZW50Jyk7XG5cdFx0XHRcblx0XHRcdC8vIExvYWQgcm9sZSBkYXRhIGFuZCBvcGVuIG1vZGFsIG9uIHN1Y2Nlc3Ncblx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdHR5cGU6IFwiR0VUXCIsXG5cdFx0XHRcdHVybDogdXJscy5nZXRSb2xlRGF0YSArICcmcm9sZUlkPScgKyByb2xlSWQsXG5cdFx0XHRcdHN1Y2Nlc3M6IChyZXNwb25zZSkgPT4ge1xuXHRcdFx0XHRcdHJlc3BvbnNlID0gSlNPTi5wYXJzZShyZXNwb25zZSk7XG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlWydzdWNjZXNzJ10gPT09IHRydWUpIHtcblx0XHRcdFx0XHRcdF91cGRhdGVNb2RhbEZvcm0ocmVzcG9uc2VbJ2RhdGEnXSk7XG5cdFx0XHRcdFx0XHQkbW9kYWxbJ2NyZWF0ZV9lZGl0J11cblx0XHRcdFx0XHRcdFx0LmZpbmQoJy5tb2RhbC10aXRsZScpXG5cdFx0XHRcdFx0XHRcdC50ZXh0KGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdyb2xlX21vZGFsX2VkaXRfdGl0bGUnLCAnYWRtaW5fYWNjZXNzJykpO1xuXHRcdFx0XHRcdFx0JG1vZGFsWydjcmVhdGVfZWRpdCddXG5cdFx0XHRcdFx0XHRcdC5maW5kKCdidXR0b24uY29uZmlybScpXG5cdFx0XHRcdFx0XHRcdC50ZXh0KGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fU0FWRScsICdhZG1pbl9idXR0b25zJykpO1xuXHRcdFx0XHRcdFx0JG1vZGFsWydjcmVhdGVfZWRpdCddXG5cdFx0XHRcdFx0XHRcdC5tb2RhbCgnc2hvdycpO1xuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdF91bmV4cGVjdGVkRXJyb3IoKTtcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXJyb3I6ICgpID0+IHtcblx0XHRcdFx0XHRfdW5leHBlY3RlZEVycm9yKCk7XG5cdFx0XHRcdH0sXG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciBmb3IgdGhlIGRlbGV0ZSByb2xlIGJ1dHRvbi5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb3BlbkRlbGV0ZVJvbGVNb2RhbChldmVudCkge1xuXHRcdFx0Ly8gUHJldmVudCBkZWZhdWx0IGFjdGlvbi5cblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IHJvbGVJZCA9ICQodGhpcykuZGF0YSgnaWQnKTtcblx0XHRcdCRzZWxlY3RlZFJvbGVSb3cgPSAkKHRoaXMpLmNsb3Nlc3QoJy5saXN0LWVsZW1lbnQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gTG9hZCByb2xlIGRhdGEgYW5kIG9wZW4gbW9kYWwgb24gc3VjY2Vzc1xuXHRcdFx0JC5hamF4KHtcblx0XHRcdFx0dHlwZTogXCJHRVRcIixcblx0XHRcdFx0dXJsOiB1cmxzLmdldFJvbGVEYXRhICsgJyZyb2xlSWQ9JyArIHJvbGVJZCxcblx0XHRcdFx0c3VjY2VzczogKHJlc3BvbnNlKSA9PiB7XG5cdFx0XHRcdFx0cmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRpZiAocmVzcG9uc2VbJ3N1Y2Nlc3MnXSA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdFx0JG1vZGFsWydkZWxldGUnXVxuXHRcdFx0XHRcdFx0XHQuZmluZCgnaW5wdXRbbmFtZT1yb2xlSWRdJylcblx0XHRcdFx0XHRcdFx0LnZhbChyZXNwb25zZVsnZGF0YSddWydpZCddKTtcblx0XHRcdFx0XHRcdCRtb2RhbFsnZGVsZXRlJ11cblx0XHRcdFx0XHRcdFx0LmZpbmQoJ2ZpZWxkc2V0IC5yb2xlLW5hbWUnKVxuXHRcdFx0XHRcdFx0XHQudGV4dChyZXNwb25zZVsnZGF0YSddWyduYW1lcyddW2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpLnRvVXBwZXJDYXNlKCldKTtcblx0XHRcdFx0XHRcdCRtb2RhbFsnZGVsZXRlJ11cblx0XHRcdFx0XHRcdFx0LmZpbmQoJ2ZpZWxkc2V0IC5yb2xlLWRlc2NyaXB0aW9uJylcblx0XHRcdFx0XHRcdFx0LnRleHQocmVzcG9uc2VbJ2RhdGEnXVsnZGVzY3JpcHRpb25zJ11banNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJykudG9VcHBlckNhc2UoKV0pO1xuXHRcdFx0XHRcdFx0JG1vZGFsWydkZWxldGUnXVxuXHRcdFx0XHRcdFx0XHQubW9kYWwoJ3Nob3cnKTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRfdW5leHBlY3RlZEVycm9yKCk7XG5cdFx0XHRcdH0sXG5cdFx0XHRcdGVycm9yOiAoKSA9PiB7XG5cdFx0XHRcdFx0X3VuZXhwZWN0ZWRFcnJvcigpO1xuXHRcdFx0XHR9LFxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsaWNrIGhhbmRsZXIgZm9yIHRoZSBkZWxldGUgbW9kYWwgc3VibWl0IGJ1dHRvbi5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc3VibWl0Q3JlYXRlRWRpdE1vZGFsRm9ybShldmVudCkge1xuXHRcdFx0Ly8gUHJldmVudCBkZWZhdWx0IGFjdGlvbi5cblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IGRhdGEgPSBfZ2V0TW9kYWxGb3JtRGF0YSgpO1xuXHRcdFx0Y29uc3QgJGVtcHR5TmFtZUlucHV0cyA9ICRtb2RhbFsnY3JlYXRlX2VkaXQnXS5maW5kKCdpbnB1dFtuYW1lPXJvbGVOYW1lXScpLmZpbHRlcihmdW5jdGlvbigpIHtcblx0XHRcdFx0cmV0dXJuICQodGhpcykudmFsKCkgPT09ICcnO1xuXHRcdFx0fSk7XG5cdFx0XHRjb25zdCAkbm9uRW1wdHlOYW1lSW5wdXRzID0gJG1vZGFsWydjcmVhdGVfZWRpdCddLmZpbmQoJ2lucHV0W25hbWU9cm9sZU5hbWVdJykuZmlsdGVyKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gJCh0aGlzKS52YWwoKSAhPT0gJyc7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0aWYgKCRlbXB0eU5hbWVJbnB1dHMuc2l6ZSgpID4gMCkge1xuXHRcdFx0XHRpZiAoJG5vbkVtcHR5TmFtZUlucHV0cy5zaXplKCkgPiAwKSB7XG5cdFx0XHRcdFx0Zm9yIChsZXQgbGFuZyBpbiBkYXRhLm5hbWVzKSB7XG5cdFx0XHRcdFx0XHRpZiAoZGF0YS5uYW1lc1tsYW5nXSA9PT0gJycpIHtcblx0XHRcdFx0XHRcdFx0ZGF0YS5uYW1lc1tsYW5nXSA9ICRub25FbXB0eU5hbWVJbnB1dHMuZmlyc3QoKS52YWwoKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JGVtcHR5TmFtZUlucHV0cy5hZGRDbGFzcygnZXJyb3InKTtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gVXBkYXRlIHJvbGUgZGF0YVxuXHRcdFx0e1xuXHRcdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHRcdHR5cGU6IFwiUE9TVFwiLFxuXHRcdFx0XHRcdHVybDogdXJscy5zYXZlUm9sZURhdGEsXG5cdFx0XHRcdFx0ZGF0YTogZGF0YSxcblx0XHRcdFx0XHRzdWNjZXNzOiAocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0XHRcdHJlc3BvbnNlID0gSlNPTi5wYXJzZShyZXNwb25zZSk7XG5cdFx0XHRcdFx0XHRpZiAocmVzcG9uc2VbJ3N1Y2Nlc3MnXSA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdFx0XHQkbW9kYWxbJ2NyZWF0ZV9lZGl0J11cblx0XHRcdFx0XHRcdFx0XHQubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0XHRcdFx0ZGF0YVsnaWQnXSA9IHJlc3BvbnNlWydyb2xlSWQnXTtcblx0XHRcdFx0XHRcdFx0X2FkZFJvd1RvT3ZlcnZpZXcoZGF0YSk7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRpZiAoJCgnLm92ZXJ2aWV3LWxpc3QtY29udGFpbmVyJykuaGFzQ2xhc3MoJ2VtcHR5JykpIHtcblx0XHRcdFx0XHRcdFx0XHQkKCcub3ZlcnZpZXctbGlzdCBsaTpub3QoXCIubGlzdC1oZWFkbGluZVwiKTpmaXJzdCcpLnJlbW92ZSgpO1xuXHRcdFx0XHRcdFx0XHRcdCQoJy5vdmVydmlldy1saXN0LWNvbnRhaW5lcicpLnJlbW92ZUNsYXNzKCdlbXB0eScpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRfdW5leHBlY3RlZEVycm9yKCk7XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRlcnJvcjogKCkgPT4ge1xuXHRcdFx0XHRcdFx0X3VuZXhwZWN0ZWRFcnJvcigpO1xuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDbGljayBoYW5kbGVyIGZvciB0aGUgY3JlYXRlIGVkaXQgbW9kYWwgc3VibWl0IGJ1dHRvbi5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc3VibWl0RGVsZXRlTW9kYWxGb3JtKGV2ZW50KSB7XG5cdFx0XHQvLyBQcmV2ZW50IGRlZmF1bHQgYWN0aW9uLlxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFxuXHRcdFx0Y29uc3Qgcm9sZUlkID0gJG1vZGFsWydkZWxldGUnXS5maW5kKCdpbnB1dFtuYW1lPXJvbGVJZF0nKS52YWwoKTtcblx0XHRcdFxuXHRcdFx0Ly8gVXBkYXRlIHJvbGUgZGF0YVxuXHRcdFx0JC5hamF4KHtcblx0XHRcdFx0dHlwZTogXCJQT1NUXCIsXG5cdFx0XHRcdHVybDogdXJscy5kZWxldGVSb2xlLFxuXHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0J3JvbGVJZCc6IHJvbGVJZFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRzdWNjZXNzOiAocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0XHRyZXNwb25zZSA9IEpTT04ucGFyc2UocmVzcG9uc2UpO1xuXHRcdFx0XHRcdGlmIChyZXNwb25zZVsnc3VjY2VzcyddID09PSB0cnVlKSB7XG5cdFx0XHRcdFx0XHQkbW9kYWxbJ2RlbGV0ZSddXG5cdFx0XHRcdFx0XHRcdC5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XHRcdFx0JHNlbGVjdGVkUm9sZVJvdy5yZW1vdmUoKTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0aWYgKCQoJy5vdmVydmlldy1saXN0IGxpOm5vdChcIi5saXN0LWhlYWRsaW5lXCIpJykubGVuZ3RoID09PSAwKSB7XG5cdFx0XHRcdFx0XHRcdCQoJy5vdmVydmlldy1saXN0JykuYXBwZW5kKGBcblx0XHRcdFx0XHRcdFx0XHQ8bGkgY2xhc3M9XCJjb2wtbWQtMTIgbGlzdC1lbGVtZW50XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cInRpdGxlXCI+YFxuXHRcdFx0XHRcdFx0XHRcdCsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3RleHRfZW1wdHlfcm9sZXNfb3ZlcnZpZXdfbGlzdCcsICdhZG1pbl9hY2Nlc3MnKSArIGA8L3NwYW4+XG5cdFx0XHRcdFx0XHRcdFx0PC9saT5cblx0XHRcdFx0XHRcdFx0YCk7XG5cdFx0XHRcdFx0XHRcdCQoJy5vdmVydmlldy1saXN0LWNvbnRhaW5lcicpLmFkZENsYXNzKCdlbXB0eScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdF91bmV4cGVjdGVkRXJyb3IoKTtcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXJyb3I6ICgpID0+IHtcblx0XHRcdFx0XHRfdW5leHBlY3RlZEVycm9yKCk7XG5cdFx0XHRcdH0sXG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU29ydGluZyBldmVudCBoYW5kbGVyIGZvciBzb3J0YWJsZSBwbHVnaW5cblx0XHQgKlxuXHRcdCAqIE1ha2VzIGEgY2FsbCB0byB0aGUgYWpheCBjb250cm9sbGVyIGFmdGVyIGEgc29ydGluZyBldmVudFxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSB1aSAgICBTb3J0YWJsZSBsaXN0ICh1bCkgb2JqZWN0IHdpdGggbmV3IHNvcnQgb3JkZXJcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2F2ZVNvcnRpbmcoZXZlbnQsIHVpKSB7XG5cdFx0XHRpZiAoIXVpLml0ZW0ucGFyZW50KCkuaXMoJ3VsJykpIHtcblx0XHRcdFx0JHNvcnRhYmxlTGlzdC5zb3J0YWJsZSgnY2FuY2VsJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdHVybDogdXJscy5zYXZlU29ydGluZyxcblx0XHRcdFx0bWV0aG9kOiAnUE9TVCcsXG5cdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHQnc29ydGluZyc6ICRzb3J0YWJsZUxpc3Quc29ydGFibGUoJ3RvQXJyYXknLCB7YXR0cmlidXRlOiAnZGF0YS1saXN0LWVsZW1lbnQtaWQnfSlcblx0XHRcdFx0fSxcblx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRyZXNwb25zZSA9IEpTT04ucGFyc2UocmVzcG9uc2UpO1xuXHRcdFx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzID09PSBmYWxzZSkge1xuXHRcdFx0XHRcdFx0X3VuZXhwZWN0ZWRFcnJvcigpXG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCd0ZXh0X3NhdmVkX3NvcnRpbmcnLCAnYWRtaW5fYWNjZXNzJykpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSxcblx0XHRcdFx0ZXJyb3I6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdF91bmV4cGVjdGVkRXJyb3IoKVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRtb2R1bGUuaW5pdCA9IGRvbmUgPT4ge1xuXHRcdFx0Ly8gaW5pdGlhbGl6YXRpb24gbG9naWNcblx0XHRcdCQoJ2EuY3JlYXRlLXJvbGUnKS5vbignY2xpY2snLCBfb3BlbkNyZWF0ZVJvbGVNb2RhbCk7XG5cdFx0XHQkKCdhLmVkaXQtcm9sZScpLm9uKCdjbGljaycsIF9vcGVuRWRpdFJvbGVNb2RhbCk7XG5cdFx0XHQkKCdhLmRlbGV0ZS1yb2xlJykub24oJ2NsaWNrJywgX29wZW5EZWxldGVSb2xlTW9kYWwpO1xuXHRcdFx0JCgnYS5zb3J0LWhhbmRsZScpLm9uKCdjbGljaycsIChldmVudCkgPT4ge1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCRtb2RhbFsnY3JlYXRlX2VkaXQnXVxuXHRcdFx0XHQuZmluZCgnYnV0dG9uLmNvbmZpcm0nKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgX3N1Ym1pdENyZWF0ZUVkaXRNb2RhbEZvcm0pO1xuXHRcdFx0JG1vZGFsWydkZWxldGUnXVxuXHRcdFx0XHQuZmluZCgnYnV0dG9uLmNvbmZpcm0nKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgX3N1Ym1pdERlbGV0ZU1vZGFsRm9ybSk7XG5cdFx0XHRcblx0XHRcdCRzb3J0YWJsZUxpc3Rcblx0XHRcdFx0LnNvcnRhYmxlKHtcblx0XHRcdFx0XHRpdGVtczogJ2xpLmxpc3QtZWxlbWVudCcsXG5cdFx0XHRcdFx0YXhpczogJ3knLFxuXHRcdFx0XHRcdGN1cnNvcjogJ21vdmUnLFxuXHRcdFx0XHRcdGhhbmRsZTogJy5zb3J0LWhhbmRsZScsXG5cdFx0XHRcdFx0Y29udGFpbm1lbnQ6ICdkb2N1bWVudCcsXG5cdFx0XHRcdFx0b3BhY2l0eTogMC43NSxcblx0XHRcdFx0XHRwbGFjZWhvbGRlcjogJ2NvbC1tZC0xMiBsaXN0LWVsZW1lbnQgc29ydC1wbGFjZWhvbGRlcidcblx0XHRcdFx0fSlcblx0XHRcdFx0Lm9uKCdzb3J0dXBkYXRlJywgX3NhdmVTb3J0aW5nKVxuXHRcdFx0XHQuZGlzYWJsZVNlbGVjdGlvbigpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fVxuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7Il19
