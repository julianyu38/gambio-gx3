'use strict';

/* --------------------------------------------------------------
 tooltip.js 2018-01-16
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Orders Table Tooltip
 *
 * This controller displays tooltips for the orders overview table. The tooltips are loaded after the
 * table data request is ready for optimization purposes.
 */
gx.controllers.module('tooltips', [jse.source + '/vendor/qtip2/jquery.qtip.css', jse.source + '/vendor/qtip2/jquery.qtip.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @var {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		sourceUrl: jse.core.config.get('appUrl') + '/admin/admin.php?do=OrdersOverviewAjax/Tooltips',
		selectors: {
			mouseenter: {
				orderItems: '.tooltip-order-items',
				invoices: '.tooltip-invoices',
				customerMemos: '.tooltip-customer-memos',
				customerAddresses: '.tooltip-customer-addresses',
				orderSumBlock: '.tooltip-order-sum-block',
				orderStatusHistory: '.tooltip-order-status-history',
				orderComment: '.tooltip-order-comment',
				trackingLinks: '.tooltip-tracking-links'
			}
		}
	};

	/**
  * Final Options
  *
  * @var {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Tooltip Contents
  *
  * Contains the rendered HTML of the tooltips. The HTML is rendered with each table draw.
  *
  * e.g. tooltips.400210.orderItems >> HTML for order items tooltip of order #400210.
  *
  * @type {Object}
  */
	var tooltips = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Target Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTargetPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'left' : 'right';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'top' : 'bottom';

		return horizontal + ' ' + vertical;
	}

	/**
  * Get Tooltip Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTooltipPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'right' : 'left';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'bottom' : 'top';

		return horizontal + ' ' + vertical;
	}

	/**
  * If there is only one link then open it in a new tab. 
  */
	function _onTrackingLinksClick() {
		var trackingLinks = $(this).parents('tr').data('trackingLinks');

		if (trackingLinks.length === 1) {
			window.open(trackingLinks[0], '_blank');
		}
	}

	/**
  * Initialize tooltip for static table data.
  *
  * Replaces the browsers default tooltip with a qTip instance for every element on the table which has
  * a title attribute.
  */
	function _initTooltipsForStaticContent() {
		$this.find('tbody [title]').qtip({
			style: { classes: 'gx-qtip info' }
		});
	}

	/**
  * Show Tooltip
  *
  * Display the Qtip instance of the target. The tooltip contents are fetched after the table request
  * is finished for performance reasons. This method will not show anything until the tooltip contents
  * are fetched.
  *
  * @param {jQuery.Event} event
  */
	function _showTooltip(event) {
		event.stopPropagation();

		var orderId = $(this).parents('tr').data('id');

		if (!tooltips || !tooltips[orderId]) {
			return; // The requested tooltip is not loaded, do not continue.
		}

		var tooltipPosition = _getTooltipPosition($(this));
		var targetPosition = _getTargetPosition($(this));

		$(this).qtip({
			content: tooltips[orderId][event.data.name],
			style: {
				classes: 'gx-qtip info'
			},
			position: {
				my: tooltipPosition,
				at: targetPosition,
				effect: false,
				viewport: $(window),
				adjust: {
					method: 'none shift'
				}
			},
			hide: {
				fixed: true,
				delay: 300
			},
			show: {
				ready: true,
				delay: 100
			},
			events: {
				hidden: function hidden(event, api) {
					api.destroy(true);
				}
			}
		});
	}

	/**
  * Get Tooltips 
  * 
  * Fetches the tooltips with an AJAX request, based on the current state of the table.  
  */
	function _getTooltips() {
		tooltips = [];
		var datatablesXhrParameters = $this.DataTable().ajax.params();
		$.post(options.sourceUrl, datatablesXhrParameters, function (response) {
			return tooltips = response;
		}, 'json');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('draw.dt', _initTooltipsForStaticContent).on('xhr.dt', _getTooltips).on('click', '.tooltip-tracking-links', _onTrackingLinksClick);

		$(window).on('JSENGINE_INIT_FINISHED', function () {
			if ($this.DataTable().ajax.json() !== undefined && tooltips === undefined) {
				_getTooltips();
			}
		});

		for (var event in options.selectors) {
			for (var name in options.selectors[event]) {
				$this.on(event, options.selectors[event][name], { name: name }, _showTooltip);
			}
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vdmVydmlldy90b29sdGlwcy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwic291cmNlVXJsIiwiY29yZSIsImNvbmZpZyIsImdldCIsInNlbGVjdG9ycyIsIm1vdXNlZW50ZXIiLCJvcmRlckl0ZW1zIiwiaW52b2ljZXMiLCJjdXN0b21lck1lbW9zIiwiY3VzdG9tZXJBZGRyZXNzZXMiLCJvcmRlclN1bUJsb2NrIiwib3JkZXJTdGF0dXNIaXN0b3J5Iiwib3JkZXJDb21tZW50IiwidHJhY2tpbmdMaW5rcyIsIm9wdGlvbnMiLCJleHRlbmQiLCJ0b29sdGlwcyIsIl9nZXRUYXJnZXRQb3NpdGlvbiIsIiR0YXJnZXQiLCJob3Jpem9udGFsIiwib2Zmc2V0IiwibGVmdCIsIndpbmRvdyIsInNjcm9sbExlZnQiLCJ3aWR0aCIsInZlcnRpY2FsIiwidG9wIiwic2Nyb2xsVG9wIiwiaGVpZ2h0IiwiX2dldFRvb2x0aXBQb3NpdGlvbiIsIl9vblRyYWNraW5nTGlua3NDbGljayIsInBhcmVudHMiLCJsZW5ndGgiLCJvcGVuIiwiX2luaXRUb29sdGlwc0ZvclN0YXRpY0NvbnRlbnQiLCJmaW5kIiwicXRpcCIsInN0eWxlIiwiY2xhc3NlcyIsIl9zaG93VG9vbHRpcCIsImV2ZW50Iiwic3RvcFByb3BhZ2F0aW9uIiwib3JkZXJJZCIsInRvb2x0aXBQb3NpdGlvbiIsInRhcmdldFBvc2l0aW9uIiwiY29udGVudCIsIm5hbWUiLCJwb3NpdGlvbiIsIm15IiwiYXQiLCJlZmZlY3QiLCJ2aWV3cG9ydCIsImFkanVzdCIsIm1ldGhvZCIsImhpZGUiLCJmaXhlZCIsImRlbGF5Iiwic2hvdyIsInJlYWR5IiwiZXZlbnRzIiwiaGlkZGVuIiwiYXBpIiwiZGVzdHJveSIsIl9nZXRUb29sdGlwcyIsImRhdGF0YWJsZXNYaHJQYXJhbWV0ZXJzIiwiRGF0YVRhYmxlIiwiYWpheCIsInBhcmFtcyIsInBvc3QiLCJyZXNwb25zZSIsImluaXQiLCJkb25lIiwib24iLCJqc29uIiwidW5kZWZpbmVkIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7OztBQU1BQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxVQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixvQ0FFSUQsSUFBSUMsTUFGUixrQ0FIRCxFQVFDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVc7QUFDaEJDLGFBQVdOLElBQUlPLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsaURBRDNCO0FBRWhCQyxhQUFXO0FBQ1ZDLGVBQVk7QUFDWEMsZ0JBQVksc0JBREQ7QUFFWEMsY0FBVSxtQkFGQztBQUdYQyxtQkFBZSx5QkFISjtBQUlYQyx1QkFBbUIsNkJBSlI7QUFLWEMsbUJBQWUsMEJBTEo7QUFNWEMsd0JBQW9CLCtCQU5UO0FBT1hDLGtCQUFjLHdCQVBIO0FBUVhDLG1CQUFlO0FBUko7QUFERjtBQUZLLEVBQWpCOztBQWdCQTs7Ozs7QUFLQSxLQUFNQyxVQUFVaEIsRUFBRWlCLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQmhCLFFBQW5CLEVBQTZCSCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTLEVBQWY7O0FBRUE7Ozs7Ozs7OztBQVNBLEtBQUl1QixpQkFBSjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUFPQSxVQUFTQyxrQkFBVCxDQUE0QkMsT0FBNUIsRUFBcUM7QUFDcEMsTUFBTUMsYUFBYUQsUUFBUUUsTUFBUixHQUFpQkMsSUFBakIsR0FBd0J2QixFQUFFd0IsTUFBRixFQUFVQyxVQUFWLEVBQXhCLEdBQWlEekIsRUFBRXdCLE1BQUYsRUFBVUUsS0FBVixLQUFvQixDQUFyRSxHQUNmLE1BRGUsR0FFZixPQUZKO0FBR0EsTUFBTUMsV0FBV1AsUUFBUUUsTUFBUixHQUFpQk0sR0FBakIsR0FBdUI1QixFQUFFd0IsTUFBRixFQUFVSyxTQUFWLEVBQXZCLEdBQStDN0IsRUFBRXdCLE1BQUYsRUFBVU0sTUFBVixLQUFxQixDQUFwRSxHQUNiLEtBRGEsR0FFYixRQUZKOztBQUlBLFNBQU9ULGFBQWEsR0FBYixHQUFtQk0sUUFBMUI7QUFDQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNJLG1CQUFULENBQTZCWCxPQUE3QixFQUFzQztBQUNyQyxNQUFNQyxhQUFhRCxRQUFRRSxNQUFSLEdBQWlCQyxJQUFqQixHQUF3QnZCLEVBQUV3QixNQUFGLEVBQVVDLFVBQVYsRUFBeEIsR0FBaUR6QixFQUFFd0IsTUFBRixFQUFVRSxLQUFWLEtBQW9CLENBQXJFLEdBQ2YsT0FEZSxHQUVmLE1BRko7QUFHQSxNQUFNQyxXQUFXUCxRQUFRRSxNQUFSLEdBQWlCTSxHQUFqQixHQUF1QjVCLEVBQUV3QixNQUFGLEVBQVVLLFNBQVYsRUFBdkIsR0FBK0M3QixFQUFFd0IsTUFBRixFQUFVTSxNQUFWLEtBQXFCLENBQXBFLEdBQ2IsUUFEYSxHQUViLEtBRko7O0FBSUEsU0FBT1QsYUFBYSxHQUFiLEdBQW1CTSxRQUExQjtBQUNBOztBQUVEOzs7QUFHQSxVQUFTSyxxQkFBVCxHQUFpQztBQUNoQyxNQUFNakIsZ0JBQWdCZixFQUFFLElBQUYsRUFBUWlDLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JuQyxJQUF0QixDQUEyQixlQUEzQixDQUF0Qjs7QUFFQSxNQUFJaUIsY0FBY21CLE1BQWQsS0FBeUIsQ0FBN0IsRUFBZ0M7QUFDL0JWLFVBQU9XLElBQVAsQ0FBWXBCLGNBQWMsQ0FBZCxDQUFaLEVBQThCLFFBQTlCO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7O0FBTUEsVUFBU3FCLDZCQUFULEdBQXlDO0FBQ3hDckMsUUFBTXNDLElBQU4sQ0FBVyxlQUFYLEVBQTRCQyxJQUE1QixDQUFpQztBQUNoQ0MsVUFBTyxFQUFDQyxTQUFTLGNBQVY7QUFEeUIsR0FBakM7QUFHQTs7QUFFRDs7Ozs7Ozs7O0FBU0EsVUFBU0MsWUFBVCxDQUFzQkMsS0FBdEIsRUFBNkI7QUFDNUJBLFFBQU1DLGVBQU47O0FBRUEsTUFBTUMsVUFBVTVDLEVBQUUsSUFBRixFQUFRaUMsT0FBUixDQUFnQixJQUFoQixFQUFzQm5DLElBQXRCLENBQTJCLElBQTNCLENBQWhCOztBQUVBLE1BQUksQ0FBQ29CLFFBQUQsSUFBYSxDQUFDQSxTQUFTMEIsT0FBVCxDQUFsQixFQUFxQztBQUNwQyxVQURvQyxDQUM1QjtBQUNSOztBQUVELE1BQU1DLGtCQUFrQmQsb0JBQW9CL0IsRUFBRSxJQUFGLENBQXBCLENBQXhCO0FBQ0EsTUFBTThDLGlCQUFpQjNCLG1CQUFtQm5CLEVBQUUsSUFBRixDQUFuQixDQUF2Qjs7QUFFQUEsSUFBRSxJQUFGLEVBQVFzQyxJQUFSLENBQWE7QUFDWlMsWUFBUzdCLFNBQVMwQixPQUFULEVBQWtCRixNQUFNNUMsSUFBTixDQUFXa0QsSUFBN0IsQ0FERztBQUVaVCxVQUFPO0FBQ05DLGFBQVM7QUFESCxJQUZLO0FBS1pTLGFBQVU7QUFDVEMsUUFBSUwsZUFESztBQUVUTSxRQUFJTCxjQUZLO0FBR1RNLFlBQVEsS0FIQztBQUlUQyxjQUFVckQsRUFBRXdCLE1BQUYsQ0FKRDtBQUtUOEIsWUFBUTtBQUNQQyxhQUFRO0FBREQ7QUFMQyxJQUxFO0FBY1pDLFNBQU07QUFDTEMsV0FBTyxJQURGO0FBRUxDLFdBQU87QUFGRixJQWRNO0FBa0JaQyxTQUFNO0FBQ0xDLFdBQU8sSUFERjtBQUVMRixXQUFPO0FBRkYsSUFsQk07QUFzQlpHLFdBQVE7QUFDUEMsWUFBUSxnQkFBQ3BCLEtBQUQsRUFBUXFCLEdBQVIsRUFBZ0I7QUFDdkJBLFNBQUlDLE9BQUosQ0FBWSxJQUFaO0FBQ0E7QUFITTtBQXRCSSxHQUFiO0FBNEJBOztBQUVEOzs7OztBQUtBLFVBQVNDLFlBQVQsR0FBd0I7QUFDdkIvQyxhQUFXLEVBQVg7QUFDQSxNQUFNZ0QsMEJBQTBCbkUsTUFBTW9FLFNBQU4sR0FBa0JDLElBQWxCLENBQXVCQyxNQUF2QixFQUFoQztBQUNBckUsSUFBRXNFLElBQUYsQ0FBT3RELFFBQVFkLFNBQWYsRUFBMEJnRSx1QkFBMUIsRUFBbUQ7QUFBQSxVQUFZaEQsV0FBV3FELFFBQXZCO0FBQUEsR0FBbkQsRUFBb0YsTUFBcEY7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE1RSxRQUFPNkUsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjFFLFFBQ0UyRSxFQURGLENBQ0ssU0FETCxFQUNnQnRDLDZCQURoQixFQUVFc0MsRUFGRixDQUVLLFFBRkwsRUFFZVQsWUFGZixFQUdFUyxFQUhGLENBR0ssT0FITCxFQUdjLHlCQUhkLEVBR3lDMUMscUJBSHpDOztBQUtBaEMsSUFBRXdCLE1BQUYsRUFBVWtELEVBQVYsQ0FBYSx3QkFBYixFQUF1QyxZQUFNO0FBQzVDLE9BQUkzRSxNQUFNb0UsU0FBTixHQUFrQkMsSUFBbEIsQ0FBdUJPLElBQXZCLE9BQWtDQyxTQUFsQyxJQUErQzFELGFBQWEwRCxTQUFoRSxFQUEyRTtBQUMxRVg7QUFDQTtBQUNELEdBSkQ7O0FBTUEsT0FBSyxJQUFJdkIsS0FBVCxJQUFrQjFCLFFBQVFWLFNBQTFCLEVBQXFDO0FBQ3BDLFFBQUssSUFBSTBDLElBQVQsSUFBaUJoQyxRQUFRVixTQUFSLENBQWtCb0MsS0FBbEIsQ0FBakIsRUFBMkM7QUFDMUMzQyxVQUFNMkUsRUFBTixDQUFTaEMsS0FBVCxFQUFnQjFCLFFBQVFWLFNBQVIsQ0FBa0JvQyxLQUFsQixFQUF5Qk0sSUFBekIsQ0FBaEIsRUFBZ0QsRUFBQ0EsVUFBRCxFQUFoRCxFQUF3RFAsWUFBeEQ7QUFDQTtBQUNEOztBQUVEZ0M7QUFDQSxFQW5CRDs7QUFxQkEsUUFBTzlFLE1BQVA7QUFDQSxDQTVORiIsImZpbGUiOiJvcmRlcnMvb3ZlcnZpZXcvdG9vbHRpcHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHRvb2x0aXAuanMgMjAxOC0wMS0xNlxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTggR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogT3JkZXJzIFRhYmxlIFRvb2x0aXBcbiAqXG4gKiBUaGlzIGNvbnRyb2xsZXIgZGlzcGxheXMgdG9vbHRpcHMgZm9yIHRoZSBvcmRlcnMgb3ZlcnZpZXcgdGFibGUuIFRoZSB0b29sdGlwcyBhcmUgbG9hZGVkIGFmdGVyIHRoZVxuICogdGFibGUgZGF0YSByZXF1ZXN0IGlzIHJlYWR5IGZvciBvcHRpbWl6YXRpb24gcHVycG9zZXMuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3Rvb2x0aXBzJyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvcXRpcDIvanF1ZXJ5LnF0aXAuY3NzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvcXRpcDIvanF1ZXJ5LnF0aXAuanNgXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHZhciB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7XG5cdFx0XHRzb3VyY2VVcmw6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89T3JkZXJzT3ZlcnZpZXdBamF4L1Rvb2x0aXBzJyxcblx0XHRcdHNlbGVjdG9yczoge1xuXHRcdFx0XHRtb3VzZWVudGVyOiB7XG5cdFx0XHRcdFx0b3JkZXJJdGVtczogJy50b29sdGlwLW9yZGVyLWl0ZW1zJyxcblx0XHRcdFx0XHRpbnZvaWNlczogJy50b29sdGlwLWludm9pY2VzJyxcblx0XHRcdFx0XHRjdXN0b21lck1lbW9zOiAnLnRvb2x0aXAtY3VzdG9tZXItbWVtb3MnLFxuXHRcdFx0XHRcdGN1c3RvbWVyQWRkcmVzc2VzOiAnLnRvb2x0aXAtY3VzdG9tZXItYWRkcmVzc2VzJyxcblx0XHRcdFx0XHRvcmRlclN1bUJsb2NrOiAnLnRvb2x0aXAtb3JkZXItc3VtLWJsb2NrJyxcblx0XHRcdFx0XHRvcmRlclN0YXR1c0hpc3Rvcnk6ICcudG9vbHRpcC1vcmRlci1zdGF0dXMtaGlzdG9yeScsXG5cdFx0XHRcdFx0b3JkZXJDb21tZW50OiAnLnRvb2x0aXAtb3JkZXItY29tbWVudCcsXG5cdFx0XHRcdFx0dHJhY2tpbmdMaW5rczogJy50b29sdGlwLXRyYWNraW5nLWxpbmtzJyxcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHZhciB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVG9vbHRpcCBDb250ZW50c1xuXHRcdCAqXG5cdFx0ICogQ29udGFpbnMgdGhlIHJlbmRlcmVkIEhUTUwgb2YgdGhlIHRvb2x0aXBzLiBUaGUgSFRNTCBpcyByZW5kZXJlZCB3aXRoIGVhY2ggdGFibGUgZHJhdy5cblx0XHQgKlxuXHRcdCAqIGUuZy4gdG9vbHRpcHMuNDAwMjEwLm9yZGVySXRlbXMgPj4gSFRNTCBmb3Igb3JkZXIgaXRlbXMgdG9vbHRpcCBvZiBvcmRlciAjNDAwMjEwLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRsZXQgdG9vbHRpcHM7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IFRhcmdldCBQb3NpdGlvblxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICR0YXJnZXRcblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge1N0cmluZ31cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0VGFyZ2V0UG9zaXRpb24oJHRhcmdldCkge1xuXHRcdFx0Y29uc3QgaG9yaXpvbnRhbCA9ICR0YXJnZXQub2Zmc2V0KCkubGVmdCAtICQod2luZG93KS5zY3JvbGxMZWZ0KCkgPiAkKHdpbmRvdykud2lkdGgoKSAvIDJcblx0XHRcdFx0XHQ/ICdsZWZ0J1xuXHRcdFx0XHRcdDogJ3JpZ2h0Jztcblx0XHRcdGNvbnN0IHZlcnRpY2FsID0gJHRhcmdldC5vZmZzZXQoKS50b3AgLSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgPiAkKHdpbmRvdykuaGVpZ2h0KCkgLyAyXG5cdFx0XHRcdFx0PyAndG9wJ1xuXHRcdFx0XHRcdDogJ2JvdHRvbSc7XG5cdFx0XHRcblx0XHRcdHJldHVybiBob3Jpem9udGFsICsgJyAnICsgdmVydGljYWw7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdldCBUb29sdGlwIFBvc2l0aW9uXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRhcmdldFxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRUb29sdGlwUG9zaXRpb24oJHRhcmdldCkge1xuXHRcdFx0Y29uc3QgaG9yaXpvbnRhbCA9ICR0YXJnZXQub2Zmc2V0KCkubGVmdCAtICQod2luZG93KS5zY3JvbGxMZWZ0KCkgPiAkKHdpbmRvdykud2lkdGgoKSAvIDJcblx0XHRcdFx0XHQ/ICdyaWdodCdcblx0XHRcdFx0XHQ6ICdsZWZ0Jztcblx0XHRcdGNvbnN0IHZlcnRpY2FsID0gJHRhcmdldC5vZmZzZXQoKS50b3AgLSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgPiAkKHdpbmRvdykuaGVpZ2h0KCkgLyAyXG5cdFx0XHRcdFx0PyAnYm90dG9tJ1xuXHRcdFx0XHRcdDogJ3RvcCc7XG5cdFx0XHRcblx0XHRcdHJldHVybiBob3Jpem9udGFsICsgJyAnICsgdmVydGljYWw7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIElmIHRoZXJlIGlzIG9ubHkgb25lIGxpbmsgdGhlbiBvcGVuIGl0IGluIGEgbmV3IHRhYi4gXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uVHJhY2tpbmdMaW5rc0NsaWNrKCkge1xuXHRcdFx0Y29uc3QgdHJhY2tpbmdMaW5rcyA9ICQodGhpcykucGFyZW50cygndHInKS5kYXRhKCd0cmFja2luZ0xpbmtzJyk7IFxuXHRcdFx0XG5cdFx0XHRpZiAodHJhY2tpbmdMaW5rcy5sZW5ndGggPT09IDEpIHtcblx0XHRcdFx0d2luZG93Lm9wZW4odHJhY2tpbmdMaW5rc1swXSwgJ19ibGFuaycpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIHRvb2x0aXAgZm9yIHN0YXRpYyB0YWJsZSBkYXRhLlxuXHRcdCAqXG5cdFx0ICogUmVwbGFjZXMgdGhlIGJyb3dzZXJzIGRlZmF1bHQgdG9vbHRpcCB3aXRoIGEgcVRpcCBpbnN0YW5jZSBmb3IgZXZlcnkgZWxlbWVudCBvbiB0aGUgdGFibGUgd2hpY2ggaGFzXG5cdFx0ICogYSB0aXRsZSBhdHRyaWJ1dGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2luaXRUb29sdGlwc0ZvclN0YXRpY0NvbnRlbnQoKSB7XG5cdFx0XHQkdGhpcy5maW5kKCd0Ym9keSBbdGl0bGVdJykucXRpcCh7XG5cdFx0XHRcdHN0eWxlOiB7Y2xhc3NlczogJ2d4LXF0aXAgaW5mbyd9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2hvdyBUb29sdGlwXG5cdFx0ICpcblx0XHQgKiBEaXNwbGF5IHRoZSBRdGlwIGluc3RhbmNlIG9mIHRoZSB0YXJnZXQuIFRoZSB0b29sdGlwIGNvbnRlbnRzIGFyZSBmZXRjaGVkIGFmdGVyIHRoZSB0YWJsZSByZXF1ZXN0XG5cdFx0ICogaXMgZmluaXNoZWQgZm9yIHBlcmZvcm1hbmNlIHJlYXNvbnMuIFRoaXMgbWV0aG9kIHdpbGwgbm90IHNob3cgYW55dGhpbmcgdW50aWwgdGhlIHRvb2x0aXAgY29udGVudHNcblx0XHQgKiBhcmUgZmV0Y2hlZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9zaG93VG9vbHRpcChldmVudCkge1xuXHRcdFx0ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IG9yZGVySWQgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKTtcblx0XHRcdFxuXHRcdFx0aWYgKCF0b29sdGlwcyB8fCAhdG9vbHRpcHNbb3JkZXJJZF0pIHtcblx0XHRcdFx0cmV0dXJuOyAvLyBUaGUgcmVxdWVzdGVkIHRvb2x0aXAgaXMgbm90IGxvYWRlZCwgZG8gbm90IGNvbnRpbnVlLlxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRjb25zdCB0b29sdGlwUG9zaXRpb24gPSBfZ2V0VG9vbHRpcFBvc2l0aW9uKCQodGhpcykpO1xuXHRcdFx0Y29uc3QgdGFyZ2V0UG9zaXRpb24gPSBfZ2V0VGFyZ2V0UG9zaXRpb24oJCh0aGlzKSk7XG5cdFx0XHRcblx0XHRcdCQodGhpcykucXRpcCh7XG5cdFx0XHRcdGNvbnRlbnQ6IHRvb2x0aXBzW29yZGVySWRdW2V2ZW50LmRhdGEubmFtZV0sXG5cdFx0XHRcdHN0eWxlOiB7XG5cdFx0XHRcdFx0Y2xhc3NlczogJ2d4LXF0aXAgaW5mbydcblx0XHRcdFx0fSxcblx0XHRcdFx0cG9zaXRpb246IHtcblx0XHRcdFx0XHRteTogdG9vbHRpcFBvc2l0aW9uLFxuXHRcdFx0XHRcdGF0OiB0YXJnZXRQb3NpdGlvbixcblx0XHRcdFx0XHRlZmZlY3Q6IGZhbHNlLFxuXHRcdFx0XHRcdHZpZXdwb3J0OiAkKHdpbmRvdyksXG5cdFx0XHRcdFx0YWRqdXN0OiB7XG5cdFx0XHRcdFx0XHRtZXRob2Q6ICdub25lIHNoaWZ0J1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSxcblx0XHRcdFx0aGlkZToge1xuXHRcdFx0XHRcdGZpeGVkOiB0cnVlLFxuXHRcdFx0XHRcdGRlbGF5OiAzMDBcblx0XHRcdFx0fSxcblx0XHRcdFx0c2hvdzoge1xuXHRcdFx0XHRcdHJlYWR5OiB0cnVlLFxuXHRcdFx0XHRcdGRlbGF5OiAxMDBcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXZlbnRzOiB7XG5cdFx0XHRcdFx0aGlkZGVuOiAoZXZlbnQsIGFwaSkgPT4ge1xuXHRcdFx0XHRcdFx0YXBpLmRlc3Ryb3kodHJ1ZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IFRvb2x0aXBzIFxuXHRcdCAqIFxuXHRcdCAqIEZldGNoZXMgdGhlIHRvb2x0aXBzIHdpdGggYW4gQUpBWCByZXF1ZXN0LCBiYXNlZCBvbiB0aGUgY3VycmVudCBzdGF0ZSBvZiB0aGUgdGFibGUuICBcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0VG9vbHRpcHMoKSB7XG5cdFx0XHR0b29sdGlwcyA9IFtdO1xuXHRcdFx0Y29uc3QgZGF0YXRhYmxlc1hoclBhcmFtZXRlcnMgPSAkdGhpcy5EYXRhVGFibGUoKS5hamF4LnBhcmFtcygpO1xuXHRcdFx0JC5wb3N0KG9wdGlvbnMuc291cmNlVXJsLCBkYXRhdGFibGVzWGhyUGFyYW1ldGVycywgcmVzcG9uc2UgPT4gdG9vbHRpcHMgPSByZXNwb25zZSwgJ2pzb24nKVxuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdkcmF3LmR0JywgX2luaXRUb29sdGlwc0ZvclN0YXRpY0NvbnRlbnQpXG5cdFx0XHRcdC5vbigneGhyLmR0JywgX2dldFRvb2x0aXBzKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy50b29sdGlwLXRyYWNraW5nLWxpbmtzJywgX29uVHJhY2tpbmdMaW5rc0NsaWNrKTtcblx0XHRcdFxuXHRcdFx0JCh3aW5kb3cpLm9uKCdKU0VOR0lORV9JTklUX0ZJTklTSEVEJywgKCkgPT4ge1xuXHRcdFx0XHRpZiAoJHRoaXMuRGF0YVRhYmxlKCkuYWpheC5qc29uKCkgIT09IHVuZGVmaW5lZCAmJiB0b29sdGlwcyA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdFx0X2dldFRvb2x0aXBzKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBldmVudCBpbiBvcHRpb25zLnNlbGVjdG9ycykge1xuXHRcdFx0XHRmb3IgKGxldCBuYW1lIGluIG9wdGlvbnMuc2VsZWN0b3JzW2V2ZW50XSkge1xuXHRcdFx0XHRcdCR0aGlzLm9uKGV2ZW50LCBvcHRpb25zLnNlbGVjdG9yc1tldmVudF1bbmFtZV0sIHtuYW1lfSwgX3Nob3dUb29sdGlwKTtcblx0XHRcdFx0fVx0XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7Il19
