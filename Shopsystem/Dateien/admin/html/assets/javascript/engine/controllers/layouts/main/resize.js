'use strict';

/* --------------------------------------------------------------
 resize.js 2016-05-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Resize Layout Controller
 *
 * During the admin layout lifecycle there are events that will change the size of the document (not the window!)
 * and the layout must react to them. This controller will make sure that the layout will remain stable after such
 * changes are marked with the "data-resize-layout" attribute as in the following example.
 *
 * ```html
 * <!-- DataTable Instance -->
 * <table data-gx-widget="datatable" data-resize-layout="draw.dt">
 *   ...
 * </table>
 * ```
 *
 * After a table draw is performed, it is possible that there will be more rows to be displayed and thus the
 * #main-content element gets bigger. Once the datatable "draw.dt" event is executed this module will make
 * sure that the layout remains solid.
 *
 * The event must bubble up to the container this module is bound.
 *
 * ### Dynamic Elements
 *
 * It is possible that during the page lifecycle there will be dynamic elements that will need to register
 * an the "resize-layout" event. In this case apply the "data-resize-layout" attribute in the dynamic
 * element and trigger the "resize:bind" event from that element. The event must bubble up to the layout
 * container which will then register the dynamic elements.
 */
gx.controllers.module('resize', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Marks event listeners.
  *
  * @type {string}
  */

	var ATTRIBUTE_NAME = 'data-resize-layout';

	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Main Header Selector
  *
  * @type {jQuery}
  */
	var $mainHeader = $('#main-header');

	/**
  * Main Menu Selector
  *
  * @type {jQuery}
  */
	var $mainMenu = $('#main-menu');

	/**
  * Main Footer Selector
  *
  * @type {jQuery}
  */
	var $mainFooter = $('#main-footer');

	/**
  * Main Footer Info
  *
  * @type {jQuery}
  */
	var $mainFooterInfo = $mainFooter.find('.info');

	/**
  * Main Footer Copyright
  *
  * @type {jQuery}
  */
	var $mainFooterCopyright = $mainFooter.find('.copyright');

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Bind resize events.
  */
	function _bindResizeEvents() {
		$this.find('[' + ATTRIBUTE_NAME + ']').each(function () {
			var event = $(this).attr(ATTRIBUTE_NAME);
			$(this).removeAttr(ATTRIBUTE_NAME).on(event, _updateLayoutComponents);
		});
	}

	/**
  * Give initial min height to main menu.
  */
	function _updateLayoutComponents() {
		var mainMenuHeight = window.innerHeight - $mainHeader.outerHeight() - $mainFooter.outerHeight() + $mainFooterCopyright.outerHeight();
		$mainMenu.css('min-height', mainMenuHeight);
		_setFooterInfoPosition();
	}

	/**
  * Calculate the correct footer info position.
  */
	function _setFooterInfoPosition() {
		if ($(document).scrollTop() + window.innerHeight - $mainFooterInfo.outerHeight() <= $mainFooter.offset().top) {
			$mainFooter.addClass('fixed');
		} else if ($mainFooterInfo.offset().top + $mainFooterInfo.height() >= $mainFooter.offset().top) {
			$mainFooter.removeClass('fixed');
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(window).on('resize', _updateLayoutComponents).on('JSENGINE_INIT_FINISHED', _updateLayoutComponents).on('scroll', _setFooterInfoPosition).on('register:bind', _bindResizeEvents);

		_bindResizeEvents();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxheW91dHMvbWFpbi9yZXNpemUuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImRhdGEiLCJBVFRSSUJVVEVfTkFNRSIsIiR0aGlzIiwiJCIsIiRtYWluSGVhZGVyIiwiJG1haW5NZW51IiwiJG1haW5Gb290ZXIiLCIkbWFpbkZvb3RlckluZm8iLCJmaW5kIiwiJG1haW5Gb290ZXJDb3B5cmlnaHQiLCJfYmluZFJlc2l6ZUV2ZW50cyIsImVhY2giLCJldmVudCIsImF0dHIiLCJyZW1vdmVBdHRyIiwib24iLCJfdXBkYXRlTGF5b3V0Q29tcG9uZW50cyIsIm1haW5NZW51SGVpZ2h0Iiwid2luZG93IiwiaW5uZXJIZWlnaHQiLCJvdXRlckhlaWdodCIsImNzcyIsIl9zZXRGb290ZXJJbmZvUG9zaXRpb24iLCJkb2N1bWVudCIsInNjcm9sbFRvcCIsIm9mZnNldCIsInRvcCIsImFkZENsYXNzIiwiaGVpZ2h0IiwicmVtb3ZlQ2xhc3MiLCJpbml0IiwiZG9uZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQkFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUFzQixRQUF0QixFQUFnQyxFQUFoQyxFQUFvQyxVQUFTQyxJQUFULEVBQWU7O0FBRWxEOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsaUJBQWlCLG9CQUF2Qjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSixTQUFTLEVBQWY7O0FBRUE7Ozs7O0FBS0EsS0FBTUssY0FBY0QsRUFBRSxjQUFGLENBQXBCOztBQUVBOzs7OztBQUtBLEtBQU1FLFlBQVlGLEVBQUUsWUFBRixDQUFsQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRyxjQUFjSCxFQUFFLGNBQUYsQ0FBcEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUksa0JBQWtCRCxZQUFZRSxJQUFaLENBQWlCLE9BQWpCLENBQXhCOztBQUVBOzs7OztBQUtBLEtBQU1DLHVCQUF1QkgsWUFBWUUsSUFBWixDQUFpQixZQUFqQixDQUE3Qjs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBLFVBQVNFLGlCQUFULEdBQTZCO0FBQzVCUixRQUFNTSxJQUFOLE9BQWVQLGNBQWYsUUFBa0NVLElBQWxDLENBQXVDLFlBQVc7QUFDakQsT0FBSUMsUUFBUVQsRUFBRSxJQUFGLEVBQVFVLElBQVIsQ0FBYVosY0FBYixDQUFaO0FBQ0FFLEtBQUUsSUFBRixFQUNFVyxVQURGLENBQ2FiLGNBRGIsRUFFRWMsRUFGRixDQUVLSCxLQUZMLEVBRVlJLHVCQUZaO0FBR0EsR0FMRDtBQU1BOztBQUVEOzs7QUFHQSxVQUFTQSx1QkFBVCxHQUFtQztBQUNsQyxNQUFNQyxpQkFBaUJDLE9BQU9DLFdBQVAsR0FBcUJmLFlBQVlnQixXQUFaLEVBQXJCLEdBQWlEZCxZQUFZYyxXQUFaLEVBQWpELEdBQ3BCWCxxQkFBcUJXLFdBQXJCLEVBREg7QUFFQWYsWUFBVWdCLEdBQVYsQ0FBYyxZQUFkLEVBQTRCSixjQUE1QjtBQUNBSztBQUNBOztBQUVEOzs7QUFHQSxVQUFTQSxzQkFBVCxHQUFrQztBQUNqQyxNQUFLbkIsRUFBRW9CLFFBQUYsRUFBWUMsU0FBWixLQUEwQk4sT0FBT0MsV0FBakMsR0FBK0NaLGdCQUFnQmEsV0FBaEIsRUFBaEQsSUFBa0ZkLFlBQVltQixNQUFaLEdBQXFCQyxHQUEzRyxFQUFnSDtBQUMvR3BCLGVBQVlxQixRQUFaLENBQXFCLE9BQXJCO0FBQ0EsR0FGRCxNQUVPLElBQUlwQixnQkFBZ0JrQixNQUFoQixHQUF5QkMsR0FBekIsR0FBK0JuQixnQkFBZ0JxQixNQUFoQixFQUEvQixJQUEyRHRCLFlBQVltQixNQUFaLEdBQXFCQyxHQUFwRixFQUF5RjtBQUMvRnBCLGVBQVl1QixXQUFaLENBQXdCLE9BQXhCO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE5QixRQUFPK0IsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjVCLElBQUVlLE1BQUYsRUFDRUgsRUFERixDQUNLLFFBREwsRUFDZUMsdUJBRGYsRUFFRUQsRUFGRixDQUVLLHdCQUZMLEVBRStCQyx1QkFGL0IsRUFHRUQsRUFIRixDQUdLLFFBSEwsRUFHZU8sc0JBSGYsRUFJRVAsRUFKRixDQUlLLGVBSkwsRUFJc0JMLGlCQUp0Qjs7QUFNQUE7O0FBRUFxQjtBQUNBLEVBVkQ7O0FBWUEsUUFBT2hDLE1BQVA7QUFDQSxDQXZIRCIsImZpbGUiOiJsYXlvdXRzL21haW4vcmVzaXplLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIHJlc2l6ZS5qcyAyMDE2LTA1LTEyXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIFJlc2l6ZSBMYXlvdXQgQ29udHJvbGxlclxyXG4gKlxyXG4gKiBEdXJpbmcgdGhlIGFkbWluIGxheW91dCBsaWZlY3ljbGUgdGhlcmUgYXJlIGV2ZW50cyB0aGF0IHdpbGwgY2hhbmdlIHRoZSBzaXplIG9mIHRoZSBkb2N1bWVudCAobm90IHRoZSB3aW5kb3chKVxyXG4gKiBhbmQgdGhlIGxheW91dCBtdXN0IHJlYWN0IHRvIHRoZW0uIFRoaXMgY29udHJvbGxlciB3aWxsIG1ha2Ugc3VyZSB0aGF0IHRoZSBsYXlvdXQgd2lsbCByZW1haW4gc3RhYmxlIGFmdGVyIHN1Y2hcclxuICogY2hhbmdlcyBhcmUgbWFya2VkIHdpdGggdGhlIFwiZGF0YS1yZXNpemUtbGF5b3V0XCIgYXR0cmlidXRlIGFzIGluIHRoZSBmb2xsb3dpbmcgZXhhbXBsZS5cclxuICpcclxuICogYGBgaHRtbFxyXG4gKiA8IS0tIERhdGFUYWJsZSBJbnN0YW5jZSAtLT5cclxuICogPHRhYmxlIGRhdGEtZ3gtd2lkZ2V0PVwiZGF0YXRhYmxlXCIgZGF0YS1yZXNpemUtbGF5b3V0PVwiZHJhdy5kdFwiPlxyXG4gKiAgIC4uLlxyXG4gKiA8L3RhYmxlPlxyXG4gKiBgYGBcclxuICpcclxuICogQWZ0ZXIgYSB0YWJsZSBkcmF3IGlzIHBlcmZvcm1lZCwgaXQgaXMgcG9zc2libGUgdGhhdCB0aGVyZSB3aWxsIGJlIG1vcmUgcm93cyB0byBiZSBkaXNwbGF5ZWQgYW5kIHRodXMgdGhlXHJcbiAqICNtYWluLWNvbnRlbnQgZWxlbWVudCBnZXRzIGJpZ2dlci4gT25jZSB0aGUgZGF0YXRhYmxlIFwiZHJhdy5kdFwiIGV2ZW50IGlzIGV4ZWN1dGVkIHRoaXMgbW9kdWxlIHdpbGwgbWFrZVxyXG4gKiBzdXJlIHRoYXQgdGhlIGxheW91dCByZW1haW5zIHNvbGlkLlxyXG4gKlxyXG4gKiBUaGUgZXZlbnQgbXVzdCBidWJibGUgdXAgdG8gdGhlIGNvbnRhaW5lciB0aGlzIG1vZHVsZSBpcyBib3VuZC5cclxuICpcclxuICogIyMjIER5bmFtaWMgRWxlbWVudHNcclxuICpcclxuICogSXQgaXMgcG9zc2libGUgdGhhdCBkdXJpbmcgdGhlIHBhZ2UgbGlmZWN5Y2xlIHRoZXJlIHdpbGwgYmUgZHluYW1pYyBlbGVtZW50cyB0aGF0IHdpbGwgbmVlZCB0byByZWdpc3RlclxyXG4gKiBhbiB0aGUgXCJyZXNpemUtbGF5b3V0XCIgZXZlbnQuIEluIHRoaXMgY2FzZSBhcHBseSB0aGUgXCJkYXRhLXJlc2l6ZS1sYXlvdXRcIiBhdHRyaWJ1dGUgaW4gdGhlIGR5bmFtaWNcclxuICogZWxlbWVudCBhbmQgdHJpZ2dlciB0aGUgXCJyZXNpemU6YmluZFwiIGV2ZW50IGZyb20gdGhhdCBlbGVtZW50LiBUaGUgZXZlbnQgbXVzdCBidWJibGUgdXAgdG8gdGhlIGxheW91dFxyXG4gKiBjb250YWluZXIgd2hpY2ggd2lsbCB0aGVuIHJlZ2lzdGVyIHRoZSBkeW5hbWljIGVsZW1lbnRzLlxyXG4gKi9cclxuZ3guY29udHJvbGxlcnMubW9kdWxlKCdyZXNpemUnLCBbXSwgZnVuY3Rpb24oZGF0YSkge1xyXG5cdFxyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBWQVJJQUJMRVNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBNYXJrcyBldmVudCBsaXN0ZW5lcnMuXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7c3RyaW5nfVxyXG5cdCAqL1xyXG5cdGNvbnN0IEFUVFJJQlVURV9OQU1FID0gJ2RhdGEtcmVzaXplLWxheW91dCc7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdCAqL1xyXG5cdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0ICovXHJcblx0Y29uc3QgbW9kdWxlID0ge307XHJcblx0XHJcblx0LyoqXHJcblx0ICogTWFpbiBIZWFkZXIgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJG1haW5IZWFkZXIgPSAkKCcjbWFpbi1oZWFkZXInKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNYWluIE1lbnUgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJG1haW5NZW51ID0gJCgnI21haW4tbWVudScpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1haW4gRm9vdGVyIFNlbGVjdG9yXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdCAqL1xyXG5cdGNvbnN0ICRtYWluRm9vdGVyID0gJCgnI21haW4tZm9vdGVyJyk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTWFpbiBGb290ZXIgSW5mb1xyXG5cdCAqXHJcblx0ICogQHR5cGUge2pRdWVyeX1cclxuXHQgKi9cclxuXHRjb25zdCAkbWFpbkZvb3RlckluZm8gPSAkbWFpbkZvb3Rlci5maW5kKCcuaW5mbycpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1haW4gRm9vdGVyIENvcHlyaWdodFxyXG5cdCAqXHJcblx0ICogQHR5cGUge2pRdWVyeX1cclxuXHQgKi9cclxuXHRjb25zdCAkbWFpbkZvb3RlckNvcHlyaWdodCA9ICRtYWluRm9vdGVyLmZpbmQoJy5jb3B5cmlnaHQnKTtcclxuXHRcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBGVU5DVElPTlNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBCaW5kIHJlc2l6ZSBldmVudHMuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX2JpbmRSZXNpemVFdmVudHMoKSB7XHJcblx0XHQkdGhpcy5maW5kKGBbJHtBVFRSSUJVVEVfTkFNRX1dYCkuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0bGV0IGV2ZW50ID0gJCh0aGlzKS5hdHRyKEFUVFJJQlVURV9OQU1FKTtcclxuXHRcdFx0JCh0aGlzKVxyXG5cdFx0XHRcdC5yZW1vdmVBdHRyKEFUVFJJQlVURV9OQU1FKVxyXG5cdFx0XHRcdC5vbihldmVudCwgX3VwZGF0ZUxheW91dENvbXBvbmVudHMpO1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEdpdmUgaW5pdGlhbCBtaW4gaGVpZ2h0IHRvIG1haW4gbWVudS5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfdXBkYXRlTGF5b3V0Q29tcG9uZW50cygpIHtcclxuXHRcdGNvbnN0IG1haW5NZW51SGVpZ2h0ID0gd2luZG93LmlubmVySGVpZ2h0IC0gJG1haW5IZWFkZXIub3V0ZXJIZWlnaHQoKSAtICRtYWluRm9vdGVyLm91dGVySGVpZ2h0KClcclxuXHRcdFx0KyAkbWFpbkZvb3RlckNvcHlyaWdodC5vdXRlckhlaWdodCgpO1xyXG5cdFx0JG1haW5NZW51LmNzcygnbWluLWhlaWdodCcsIG1haW5NZW51SGVpZ2h0KTtcclxuXHRcdF9zZXRGb290ZXJJbmZvUG9zaXRpb24oKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsY3VsYXRlIHRoZSBjb3JyZWN0IGZvb3RlciBpbmZvIHBvc2l0aW9uLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9zZXRGb290ZXJJbmZvUG9zaXRpb24oKSB7XHJcblx0XHRpZiAoKCQoZG9jdW1lbnQpLnNjcm9sbFRvcCgpICsgd2luZG93LmlubmVySGVpZ2h0IC0gJG1haW5Gb290ZXJJbmZvLm91dGVySGVpZ2h0KCkpIDw9ICRtYWluRm9vdGVyLm9mZnNldCgpLnRvcCkge1xyXG5cdFx0XHQkbWFpbkZvb3Rlci5hZGRDbGFzcygnZml4ZWQnKTtcclxuXHRcdH0gZWxzZSBpZiAoJG1haW5Gb290ZXJJbmZvLm9mZnNldCgpLnRvcCArICRtYWluRm9vdGVySW5mby5oZWlnaHQoKSA+PSAkbWFpbkZvb3Rlci5vZmZzZXQoKS50b3ApIHtcclxuXHRcdFx0JG1haW5Gb290ZXIucmVtb3ZlQ2xhc3MoJ2ZpeGVkJyk7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHQkKHdpbmRvdylcclxuXHRcdFx0Lm9uKCdyZXNpemUnLCBfdXBkYXRlTGF5b3V0Q29tcG9uZW50cylcclxuXHRcdFx0Lm9uKCdKU0VOR0lORV9JTklUX0ZJTklTSEVEJywgX3VwZGF0ZUxheW91dENvbXBvbmVudHMpXHJcblx0XHRcdC5vbignc2Nyb2xsJywgX3NldEZvb3RlckluZm9Qb3NpdGlvbilcclxuXHRcdFx0Lm9uKCdyZWdpc3RlcjpiaW5kJywgX2JpbmRSZXNpemVFdmVudHMpO1xyXG5cdFx0XHJcblx0XHRfYmluZFJlc2l6ZUV2ZW50cygpO1xyXG5cdFx0XHJcblx0XHRkb25lKCk7XHJcblx0fTtcclxuXHRcclxuXHRyZXR1cm4gbW9kdWxlO1xyXG59KTtcclxuIl19
