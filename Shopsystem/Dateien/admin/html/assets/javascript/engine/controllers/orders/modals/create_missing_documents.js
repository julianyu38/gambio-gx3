'use strict';

/* --------------------------------------------------------------
 create_missing_documents.js 2016-10-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Create Missing Documents Modal
 *
 * This modal prompts the user for the creation of missing order documents (invoices or packing slips). Make sure
 * that you set the "orderIds", "orderIdsWithoutDocument", "type" data values to the modal before showing it.
 *
 * Example:
 *
 * ```
 * $modal
 *   .data({
 *     orderIds: [...], 
 *     orderIdsWithoutDocument: [...], 
 *     type: 'invoice'
 *   })
 *   .modal('show');
 * ```
 */
gx.controllers.module('create_missing_documents', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: {
			createMissingDocumentsCheckbox: $this.find('.create-missing-documents-checkbox')
		}
	};

	/**
  * Proceed Event Constant
  *
  * @type {String}
  */
	var PROCEED_EVENT = 'create_missing_documents:proceed';

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Create missing documents.
  *
  * @param {Number[]} orderIds
  * @param {Number[]} orderIdsWithoutDocument
  * @param {String} type
  */
	function _createMissingDocuments(orderIds, orderIdsWithoutDocument, type) {
		var _$;

		var requests = [];
		var downloadPdfWindow = window.open('about:blank');

		orderIdsWithoutDocument.forEach(function (oID) {
			var data = {
				type: type,
				oID: oID,
				ajax: '1'
			};

			requests.push($.getJSON(jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php', data));
		});

		(_$ = $).when.apply(_$, requests).done(function () {
			return $this.trigger(PROCEED_EVENT, [orderIds, type, downloadPdfWindow]);
		});
	}

	/**
  * Remove order IDs that do not have a document.
  *
  * @param {Number[]} orderIds
  * @param {Number[]} orderIdsWithoutDocument
  * @param {String} type
  */
	function _removeOrderIdsWithoutDocument(orderIds, orderIdsWithoutDocument, type) {
		orderIds.forEach(function (id, index) {
			if (orderIdsWithoutDocument.includes(String(id))) {
				orderIds.splice(index);
			}
		});

		if (!orderIds.length) {
			var title = jse.core.lang.translate('DOWNLOAD_DOCUMENTS', 'admin_orders');
			var message = jse.core.lang.translate('NO_DOCUMENTS_ERROR', 'orders');
			jse.libs.modal.showMessage(title, message);
		} else {
			$this.trigger(PROCEED_EVENT, [orderIds, type]);
		}
	}

	/**
  * On Bulk PDF Modal Confirmation Click
  *
  * Proceed with the generation of the concatenated PDF. This handler will trigger the "create_missing_document"
  */
	function _onOkButtonClick() {
		var createMissingDocuments = module.bindings.createMissingDocumentsCheckbox.get();

		var _$this$data = $this.data(),
		    orderIds = _$this$data.orderIds,
		    orderIdsWithoutDocument = _$this$data.orderIdsWithoutDocument,
		    type = _$this$data.type;

		if (createMissingDocuments) {
			_createMissingDocuments(orderIds, orderIdsWithoutDocument, type);
		} else {
			_removeOrderIdsWithoutDocument(orderIds, orderIdsWithoutDocument, type);
		}

		$this.modal('hide');
	}

	// ------------------------------------------------------------------------
	// INITIALIZE
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.ok', _onOkButtonClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvY3JlYXRlX21pc3NpbmdfZG9jdW1lbnRzLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiYmluZGluZ3MiLCJjcmVhdGVNaXNzaW5nRG9jdW1lbnRzQ2hlY2tib3giLCJmaW5kIiwiUFJPQ0VFRF9FVkVOVCIsIl9jcmVhdGVNaXNzaW5nRG9jdW1lbnRzIiwib3JkZXJJZHMiLCJvcmRlcklkc1dpdGhvdXREb2N1bWVudCIsInR5cGUiLCJyZXF1ZXN0cyIsImRvd25sb2FkUGRmV2luZG93Iiwid2luZG93Iiwib3BlbiIsImZvckVhY2giLCJvSUQiLCJhamF4IiwicHVzaCIsImdldEpTT04iLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0Iiwid2hlbiIsImRvbmUiLCJ0cmlnZ2VyIiwiX3JlbW92ZU9yZGVySWRzV2l0aG91dERvY3VtZW50IiwiaWQiLCJpbmRleCIsImluY2x1ZGVzIiwiU3RyaW5nIiwic3BsaWNlIiwibGVuZ3RoIiwidGl0bGUiLCJsYW5nIiwidHJhbnNsYXRlIiwibWVzc2FnZSIsImxpYnMiLCJtb2RhbCIsInNob3dNZXNzYWdlIiwiX29uT2tCdXR0b25DbGljayIsImNyZWF0ZU1pc3NpbmdEb2N1bWVudHMiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0IsMEJBQXRCLEVBQWtELENBQUMsT0FBRCxDQUFsRCxFQUE2RCxVQUFTQyxJQUFULEVBQWU7O0FBRTNFOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUgsU0FBUztBQUNkSSxZQUFVO0FBQ1RDLG1DQUFnQ0gsTUFBTUksSUFBTixDQUFXLG9DQUFYO0FBRHZCO0FBREksRUFBZjs7QUFNQTs7Ozs7QUFLQSxLQUFNQyxnQkFBZ0Isa0NBQXRCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVNDLHVCQUFULENBQWlDQyxRQUFqQyxFQUEyQ0MsdUJBQTNDLEVBQW9FQyxJQUFwRSxFQUEwRTtBQUFBOztBQUN6RSxNQUFNQyxXQUFXLEVBQWpCO0FBQ0EsTUFBTUMsb0JBQW9CQyxPQUFPQyxJQUFQLENBQVksYUFBWixDQUExQjs7QUFFQUwsMEJBQXdCTSxPQUF4QixDQUFnQyxlQUFPO0FBQ3RDLE9BQU1mLE9BQU87QUFDWlUsY0FEWTtBQUVaTSxZQUZZO0FBR1pDLFVBQU07QUFITSxJQUFiOztBQU1BTixZQUFTTyxJQUFULENBQWNoQixFQUFFaUIsT0FBRixDQUFVQyxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHlCQUExQyxFQUFxRXZCLElBQXJFLENBQWQ7QUFDQSxHQVJEOztBQVVBLFdBQUV3QixJQUFGLFdBQVViLFFBQVYsRUFBb0JjLElBQXBCLENBQXlCO0FBQUEsVUFBTXhCLE1BQU15QixPQUFOLENBQWNwQixhQUFkLEVBQTZCLENBQUNFLFFBQUQsRUFBV0UsSUFBWCxFQUFpQkUsaUJBQWpCLENBQTdCLENBQU47QUFBQSxHQUF6QjtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU2UsOEJBQVQsQ0FBd0NuQixRQUF4QyxFQUFrREMsdUJBQWxELEVBQTJFQyxJQUEzRSxFQUFpRjtBQUNoRkYsV0FBU08sT0FBVCxDQUFpQixVQUFDYSxFQUFELEVBQUtDLEtBQUwsRUFBZTtBQUMvQixPQUFJcEIsd0JBQXdCcUIsUUFBeEIsQ0FBaUNDLE9BQU9ILEVBQVAsQ0FBakMsQ0FBSixFQUFrRDtBQUNqRHBCLGFBQVN3QixNQUFULENBQWdCSCxLQUFoQjtBQUNBO0FBQ0QsR0FKRDs7QUFNQSxNQUFJLENBQUNyQixTQUFTeUIsTUFBZCxFQUFzQjtBQUNyQixPQUFNQyxRQUFRZCxJQUFJQyxJQUFKLENBQVNjLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixvQkFBeEIsRUFBOEMsY0FBOUMsQ0FBZDtBQUNBLE9BQU1DLFVBQVVqQixJQUFJQyxJQUFKLENBQVNjLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixvQkFBeEIsRUFBOEMsUUFBOUMsQ0FBaEI7QUFDQWhCLE9BQUlrQixJQUFKLENBQVNDLEtBQVQsQ0FBZUMsV0FBZixDQUEyQk4sS0FBM0IsRUFBa0NHLE9BQWxDO0FBQ0EsR0FKRCxNQUlPO0FBQ05wQyxTQUFNeUIsT0FBTixDQUFjcEIsYUFBZCxFQUE2QixDQUFDRSxRQUFELEVBQVdFLElBQVgsQ0FBN0I7QUFDQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVMrQixnQkFBVCxHQUE0QjtBQUMzQixNQUFNQyx5QkFBeUIzQyxPQUFPSSxRQUFQLENBQWdCQyw4QkFBaEIsQ0FBK0NtQixHQUEvQyxFQUEvQjs7QUFEMkIsb0JBRXVCdEIsTUFBTUQsSUFBTixFQUZ2QjtBQUFBLE1BRXBCUSxRQUZvQixlQUVwQkEsUUFGb0I7QUFBQSxNQUVWQyx1QkFGVSxlQUVWQSx1QkFGVTtBQUFBLE1BRWVDLElBRmYsZUFFZUEsSUFGZjs7QUFJM0IsTUFBSWdDLHNCQUFKLEVBQTRCO0FBQzNCbkMsMkJBQXdCQyxRQUF4QixFQUFrQ0MsdUJBQWxDLEVBQTJEQyxJQUEzRDtBQUNBLEdBRkQsTUFFTztBQUNOaUIsa0NBQStCbkIsUUFBL0IsRUFBeUNDLHVCQUF6QyxFQUFrRUMsSUFBbEU7QUFDQTs7QUFFRFQsUUFBTXNDLEtBQU4sQ0FBWSxNQUFaO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBeEMsUUFBTzRDLElBQVAsR0FBYyxVQUFTbEIsSUFBVCxFQUFlO0FBQzVCeEIsUUFBTTJDLEVBQU4sQ0FBUyxPQUFULEVBQWtCLFNBQWxCLEVBQTZCSCxnQkFBN0I7QUFDQWhCO0FBQ0EsRUFIRDs7QUFLQSxRQUFPMUIsTUFBUDtBQUNBLENBaEhEIiwiZmlsZSI6Im9yZGVycy9tb2RhbHMvY3JlYXRlX21pc3NpbmdfZG9jdW1lbnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGNyZWF0ZV9taXNzaW5nX2RvY3VtZW50cy5qcyAyMDE2LTEwLTE3XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIENyZWF0ZSBNaXNzaW5nIERvY3VtZW50cyBNb2RhbFxyXG4gKlxyXG4gKiBUaGlzIG1vZGFsIHByb21wdHMgdGhlIHVzZXIgZm9yIHRoZSBjcmVhdGlvbiBvZiBtaXNzaW5nIG9yZGVyIGRvY3VtZW50cyAoaW52b2ljZXMgb3IgcGFja2luZyBzbGlwcykuIE1ha2Ugc3VyZVxyXG4gKiB0aGF0IHlvdSBzZXQgdGhlIFwib3JkZXJJZHNcIiwgXCJvcmRlcklkc1dpdGhvdXREb2N1bWVudFwiLCBcInR5cGVcIiBkYXRhIHZhbHVlcyB0byB0aGUgbW9kYWwgYmVmb3JlIHNob3dpbmcgaXQuXHJcbiAqXHJcbiAqIEV4YW1wbGU6XHJcbiAqXHJcbiAqIGBgYFxyXG4gKiAkbW9kYWxcclxuICogICAuZGF0YSh7XHJcbiAqICAgICBvcmRlcklkczogWy4uLl0sIFxyXG4gKiAgICAgb3JkZXJJZHNXaXRob3V0RG9jdW1lbnQ6IFsuLi5dLCBcclxuICogICAgIHR5cGU6ICdpbnZvaWNlJ1xyXG4gKiAgIH0pXHJcbiAqICAgLm1vZGFsKCdzaG93Jyk7XHJcbiAqIGBgYFxyXG4gKi9cclxuZ3guY29udHJvbGxlcnMubW9kdWxlKCdjcmVhdGVfbWlzc2luZ19kb2N1bWVudHMnLCBbJ21vZGFsJ10sIGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gVkFSSUFCTEVTXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdCAqL1xyXG5cdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0ICovXHJcblx0Y29uc3QgbW9kdWxlID0ge1xyXG5cdFx0YmluZGluZ3M6IHtcclxuXHRcdFx0Y3JlYXRlTWlzc2luZ0RvY3VtZW50c0NoZWNrYm94OiAkdGhpcy5maW5kKCcuY3JlYXRlLW1pc3NpbmctZG9jdW1lbnRzLWNoZWNrYm94JylcclxuXHRcdH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFByb2NlZWQgRXZlbnQgQ29uc3RhbnRcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtTdHJpbmd9XHJcblx0ICovXHJcblx0Y29uc3QgUFJPQ0VFRF9FVkVOVCA9ICdjcmVhdGVfbWlzc2luZ19kb2N1bWVudHM6cHJvY2VlZCc7XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gRlVOQ1RJT05TXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0LyoqXHJcblx0ICogQ3JlYXRlIG1pc3NpbmcgZG9jdW1lbnRzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtOdW1iZXJbXX0gb3JkZXJJZHNcclxuXHQgKiBAcGFyYW0ge051bWJlcltdfSBvcmRlcklkc1dpdGhvdXREb2N1bWVudFxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX2NyZWF0ZU1pc3NpbmdEb2N1bWVudHMob3JkZXJJZHMsIG9yZGVySWRzV2l0aG91dERvY3VtZW50LCB0eXBlKSB7XHJcblx0XHRjb25zdCByZXF1ZXN0cyA9IFtdO1xyXG5cdFx0Y29uc3QgZG93bmxvYWRQZGZXaW5kb3cgPSB3aW5kb3cub3BlbignYWJvdXQ6YmxhbmsnKTtcclxuXHRcdFxyXG5cdFx0b3JkZXJJZHNXaXRob3V0RG9jdW1lbnQuZm9yRWFjaChvSUQgPT4ge1xyXG5cdFx0XHRjb25zdCBkYXRhID0ge1xyXG5cdFx0XHRcdHR5cGUsXHJcblx0XHRcdFx0b0lELFxyXG5cdFx0XHRcdGFqYXg6ICcxJ1xyXG5cdFx0XHR9O1xyXG5cdFx0XHRcclxuXHRcdFx0cmVxdWVzdHMucHVzaCgkLmdldEpTT04oanNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2dtX3BkZl9vcmRlci5waHAnLCBkYXRhKSk7XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0JC53aGVuKC4uLnJlcXVlc3RzKS5kb25lKCgpID0+ICR0aGlzLnRyaWdnZXIoUFJPQ0VFRF9FVkVOVCwgW29yZGVySWRzLCB0eXBlLCBkb3dubG9hZFBkZldpbmRvd10pKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogUmVtb3ZlIG9yZGVyIElEcyB0aGF0IGRvIG5vdCBoYXZlIGEgZG9jdW1lbnQuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge051bWJlcltdfSBvcmRlcklkc1xyXG5cdCAqIEBwYXJhbSB7TnVtYmVyW119IG9yZGVySWRzV2l0aG91dERvY3VtZW50XHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IHR5cGVcclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfcmVtb3ZlT3JkZXJJZHNXaXRob3V0RG9jdW1lbnQob3JkZXJJZHMsIG9yZGVySWRzV2l0aG91dERvY3VtZW50LCB0eXBlKSB7XHJcblx0XHRvcmRlcklkcy5mb3JFYWNoKChpZCwgaW5kZXgpID0+IHtcclxuXHRcdFx0aWYgKG9yZGVySWRzV2l0aG91dERvY3VtZW50LmluY2x1ZGVzKFN0cmluZyhpZCkpKSB7XHJcblx0XHRcdFx0b3JkZXJJZHMuc3BsaWNlKGluZGV4KTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGlmICghb3JkZXJJZHMubGVuZ3RoKSB7XHJcblx0XHRcdGNvbnN0IHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0RPV05MT0FEX0RPQ1VNRU5UUycsICdhZG1pbl9vcmRlcnMnKTtcclxuXHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdOT19ET0NVTUVOVFNfRVJST1InLCAnb3JkZXJzJyk7XHJcblx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKHRpdGxlLCBtZXNzYWdlKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdCR0aGlzLnRyaWdnZXIoUFJPQ0VFRF9FVkVOVCwgW29yZGVySWRzLCB0eXBlXSk7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE9uIEJ1bGsgUERGIE1vZGFsIENvbmZpcm1hdGlvbiBDbGlja1xyXG5cdCAqXHJcblx0ICogUHJvY2VlZCB3aXRoIHRoZSBnZW5lcmF0aW9uIG9mIHRoZSBjb25jYXRlbmF0ZWQgUERGLiBUaGlzIGhhbmRsZXIgd2lsbCB0cmlnZ2VyIHRoZSBcImNyZWF0ZV9taXNzaW5nX2RvY3VtZW50XCJcclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25Pa0J1dHRvbkNsaWNrKCkge1xyXG5cdFx0Y29uc3QgY3JlYXRlTWlzc2luZ0RvY3VtZW50cyA9IG1vZHVsZS5iaW5kaW5ncy5jcmVhdGVNaXNzaW5nRG9jdW1lbnRzQ2hlY2tib3guZ2V0KCk7XHJcblx0XHRjb25zdCB7b3JkZXJJZHMsIG9yZGVySWRzV2l0aG91dERvY3VtZW50LCB0eXBlfSA9ICR0aGlzLmRhdGEoKTtcclxuXHRcdFxyXG5cdFx0aWYgKGNyZWF0ZU1pc3NpbmdEb2N1bWVudHMpIHtcclxuXHRcdFx0X2NyZWF0ZU1pc3NpbmdEb2N1bWVudHMob3JkZXJJZHMsIG9yZGVySWRzV2l0aG91dERvY3VtZW50LCB0eXBlKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdF9yZW1vdmVPcmRlcklkc1dpdGhvdXREb2N1bWVudChvcmRlcklkcywgb3JkZXJJZHNXaXRob3V0RG9jdW1lbnQsIHR5cGUpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQkdGhpcy5tb2RhbCgnaGlkZScpO1xyXG5cdH1cclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBJTklUSUFMSVpFXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHQkdGhpcy5vbignY2xpY2snLCAnLmJ0bi5vaycsIF9vbk9rQnV0dG9uQ2xpY2spO1xyXG5cdFx0ZG9uZSgpO1xyXG5cdH07XHJcblx0XHJcblx0cmV0dXJuIG1vZHVsZTtcclxufSk7ICJdfQ==
