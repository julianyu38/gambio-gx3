'use strict';

/* --------------------------------------------------------------
 pages_form.js 2017-09-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('type_selection', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	$this = $(this),


	/**
  * Dropdown for the selection
  *
  * @type {jQuery}
  */
	$selection = $(this).find('.content-manager-type-selection'),
	    $contentContainer = $(this).find('.content-manager-type-selection-content'),


	/**
  * Corresponding contents for this selection
  *
  * @type {jQuery}
  */
	$contents = $contentContainer.children('div'),


	/**
  * Save buttons
  * 
  * @type {jQuery}
  */
	$saveButtons = $('#main-footer .bottom-save-bar').find('button[type="submit"]'),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * URLs for deleting different types of content
  * 
  * @type {{page: string, element: string, file: string, link: string}}
  */
	urls = {},


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Click handler for the tabs onClick the content gets switched.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	var _selectionChanged = function _selectionChanged() {
		$contents.hide();
		$contentContainer.find('.' + $selection.val()).show();
		$saveButtons.attr('form', $selection.val());
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		_selectionChanged();

		$selection.on('change', _selectionChanged);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRfbWFuYWdlci90eXBlX3NlbGVjdGlvbi5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRzZWxlY3Rpb24iLCJmaW5kIiwiJGNvbnRlbnRDb250YWluZXIiLCIkY29udGVudHMiLCJjaGlsZHJlbiIsIiRzYXZlQnV0dG9ucyIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsInVybHMiLCJfc2VsZWN0aW9uQ2hhbmdlZCIsImhpZGUiLCJ2YWwiLCJzaG93IiwiYXR0ciIsImluaXQiLCJkb25lIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsZ0JBREQsRUFHQyxFQUhELEVBTUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLGNBQWNELEVBQUUsSUFBRixFQUFRRSxJQUFSLENBQWEsaUNBQWIsQ0FiZjtBQUFBLEtBZUNDLG9CQUFvQkgsRUFBRSxJQUFGLEVBQVFFLElBQVIsQ0FBYSx5Q0FBYixDQWZyQjs7O0FBaUJDOzs7OztBQUtBRSxhQUFZRCxrQkFBa0JFLFFBQWxCLENBQTJCLEtBQTNCLENBdEJiOzs7QUF3QkM7Ozs7O0FBS0FDLGdCQUFlTixFQUFFLCtCQUFGLEVBQW1DRSxJQUFuQyxDQUF3Qyx1QkFBeEMsQ0E3QmhCOzs7QUErQkM7Ozs7O0FBS0FLLFlBQVcsRUFwQ1o7OztBQXVDQzs7Ozs7QUFLQUMsV0FBVVIsRUFBRVMsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QlQsSUFBN0IsQ0E1Q1g7OztBQStDQzs7Ozs7QUFLQVksUUFBTyxFQXBEUjs7O0FBdURDOzs7OztBQUtBYixVQUFTLEVBNURWOztBQThEQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsS0FBTWMsb0JBQW9CLFNBQXBCQSxpQkFBb0IsR0FBVztBQUNwQ1AsWUFBVVEsSUFBVjtBQUNBVCxvQkFDRUQsSUFERixDQUNPLE1BQU1ELFdBQVdZLEdBQVgsRUFEYixFQUVFQyxJQUZGO0FBR0FSLGVBQWFTLElBQWIsQ0FBa0IsTUFBbEIsRUFBMEJkLFdBQVdZLEdBQVgsRUFBMUI7QUFDQSxFQU5EOztBQVFBO0FBQ0E7QUFDQTs7QUFFQWhCLFFBQU9tQixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCTjs7QUFFQVYsYUFDRWlCLEVBREYsQ0FDSyxRQURMLEVBQ2VQLGlCQURmOztBQUdBTTtBQUNBLEVBUEQ7O0FBU0EsUUFBT3BCLE1BQVA7QUFDQSxDQTNHRiIsImZpbGUiOiJjb250ZW50X21hbmFnZXIvdHlwZV9zZWxlY3Rpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHBhZ2VzX2Zvcm0uanMgMjAxNy0wOS0xOVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3R5cGVfc2VsZWN0aW9uJyxcblx0XG5cdFtcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERyb3Bkb3duIGZvciB0aGUgc2VsZWN0aW9uXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0JHNlbGVjdGlvbiA9ICAkKHRoaXMpLmZpbmQoJy5jb250ZW50LW1hbmFnZXItdHlwZS1zZWxlY3Rpb24nKSxcblx0XHRcdFxuXHRcdFx0JGNvbnRlbnRDb250YWluZXIgPSAkKHRoaXMpLmZpbmQoJy5jb250ZW50LW1hbmFnZXItdHlwZS1zZWxlY3Rpb24tY29udGVudCcpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIENvcnJlc3BvbmRpbmcgY29udGVudHMgZm9yIHRoaXMgc2VsZWN0aW9uXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0JGNvbnRlbnRzID0gJGNvbnRlbnRDb250YWluZXIuY2hpbGRyZW4oJ2RpdicpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFNhdmUgYnV0dG9uc1xuXHRcdFx0ICogXG5cdFx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdFx0ICovXG5cdFx0XHQkc2F2ZUJ1dHRvbnMgPSAkKCcjbWFpbi1mb290ZXIgLmJvdHRvbS1zYXZlLWJhcicpLmZpbmQoJ2J1dHRvblt0eXBlPVwic3VibWl0XCJdJyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFVSTHMgZm9yIGRlbGV0aW5nIGRpZmZlcmVudCB0eXBlcyBvZiBjb250ZW50XG5cdFx0XHQgKiBcblx0XHRcdCAqIEB0eXBlIHt7cGFnZTogc3RyaW5nLCBlbGVtZW50OiBzdHJpbmcsIGZpbGU6IHN0cmluZywgbGluazogc3RyaW5nfX1cblx0XHRcdCAqL1xuXHRcdFx0dXJscyA9IHtcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciBmb3IgdGhlIHRhYnMgb25DbGljayB0aGUgY29udGVudCBnZXRzIHN3aXRjaGVkLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGNvbnN0IF9zZWxlY3Rpb25DaGFuZ2VkID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQkY29udGVudHMuaGlkZSgpO1xuXHRcdFx0JGNvbnRlbnRDb250YWluZXJcblx0XHRcdFx0LmZpbmQoJy4nICsgJHNlbGVjdGlvbi52YWwoKSlcblx0XHRcdFx0LnNob3coKTtcblx0XHRcdCRzYXZlQnV0dG9ucy5hdHRyKCdmb3JtJywgJHNlbGVjdGlvbi52YWwoKSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0X3NlbGVjdGlvbkNoYW5nZWQoKTtcblx0XHRcdFxuXHRcdFx0JHNlbGVjdGlvblxuXHRcdFx0XHQub24oJ2NoYW5nZScsIF9zZWxlY3Rpb25DaGFuZ2VkKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7Il19
