'use strict';

/* --------------------------------------------------------------
 orders_pdf_delete.js 2016-10-04
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Order PDF Delete Controller
 *
 * @module Controllers/orders_pdf_delete
 */
gx.controllers.module('orders_pdf_delete', ['xhr', 'fallback'],

/** @lends module:Controllers/orders_pdf_delete */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var $this = $(this),
	    defaults = { type: 'invoice' },
	    options = $.extend(true, {}, defaults, data),
	    module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	var _deleteHandler = function _deleteHandler(event) {
		event.preventDefault();
		event.stopPropagation();

		var $self = $(this),
		    dataset = $.extend({}, $this.data(), jse.libs.fallback._data($this, 'orders_pdf_delete'));

		var href = 'lightbox_confirm.html?section=admin_orders&amp;message=DELETE_PDF_CONFIRM_MESSAGE&amp;' + 'buttons=cancel-delete';

		var t_a_tag = $('<a href="' + href + '"></a>');
		var tmp_lightbox_identifier = $(t_a_tag).lightbox_plugin({
			'lightbox_width': '360px'
		});

		$('#lightbox_package_' + tmp_lightbox_identifier).on('click', '.delete', function () {
			$.lightbox_plugin('close', tmp_lightbox_identifier);
			if ($self.hasClass('active')) {
				return false;
			}
			$self.addClass('active');

			if (options.type === 'invoice') {
				var data = {
					'type': options.type,
					'id': $self.data('invoice-id')
				};
			} else {
				var data = {
					'type': options.type,
					'file': $self.attr('rel'),
					'id': $self.data('packing-slip-id'),
					'number': $self.data('packing-slip-number')
				};
			}

			jse.libs.xhr.post({
				'url': 'request_port.php?module=OrderAdmin&action=deletePdf',
				'data': data
			}).done(function (response) {
				$self.closest('tr').remove();
				if ($('tr.' + options.type).length === 1) {
					$('tr.' + options.type).show();
				}
				$('.page_token').val(response.page_token);
			});
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Init function of the widget
  */
	module.init = function (done) {
		$this.on('click', '.delete_pdf', _deleteHandler);
		done();
	};

	// Return data to widget engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vcmRlcnNfcGRmX2RlbGV0ZS5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwidHlwZSIsIm9wdGlvbnMiLCJleHRlbmQiLCJfZGVsZXRlSGFuZGxlciIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCIkc2VsZiIsImRhdGFzZXQiLCJqc2UiLCJsaWJzIiwiZmFsbGJhY2siLCJfZGF0YSIsImhyZWYiLCJ0X2FfdGFnIiwidG1wX2xpZ2h0Ym94X2lkZW50aWZpZXIiLCJsaWdodGJveF9wbHVnaW4iLCJvbiIsImhhc0NsYXNzIiwiYWRkQ2xhc3MiLCJhdHRyIiwieGhyIiwicG9zdCIsImRvbmUiLCJyZXNwb25zZSIsImNsb3Nlc3QiLCJyZW1vdmUiLCJsZW5ndGgiLCJzaG93IiwidmFsIiwicGFnZV90b2tlbiIsImluaXQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsbUJBREQsRUFHQyxDQUFDLEtBQUQsRUFBUSxVQUFSLENBSEQ7O0FBS0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxLQUFJQyxRQUFRQyxFQUFFLElBQUYsQ0FBWjtBQUFBLEtBQ0NDLFdBQVcsRUFBQ0MsTUFBTSxTQUFQLEVBRFo7QUFBQSxLQUVDQyxVQUFVSCxFQUFFSSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJILFFBQW5CLEVBQTZCSCxJQUE3QixDQUZYO0FBQUEsS0FHQ0QsU0FBUyxFQUhWOztBQUtBO0FBQ0E7QUFDQTs7QUFFQSxLQUFJUSxpQkFBaUIsU0FBakJBLGNBQWlCLENBQVNDLEtBQVQsRUFBZ0I7QUFDcENBLFFBQU1DLGNBQU47QUFDQUQsUUFBTUUsZUFBTjs7QUFFQSxNQUFJQyxRQUFRVCxFQUFFLElBQUYsQ0FBWjtBQUFBLE1BQ0NVLFVBQVVWLEVBQUVJLE1BQUYsQ0FBUyxFQUFULEVBQWFMLE1BQU1ELElBQU4sRUFBYixFQUEyQmEsSUFBSUMsSUFBSixDQUFTQyxRQUFULENBQWtCQyxLQUFsQixDQUF3QmYsS0FBeEIsRUFBK0IsbUJBQS9CLENBQTNCLENBRFg7O0FBR0EsTUFBSWdCLE9BQ0gsMkZBQ0EsdUJBRkQ7O0FBSUEsTUFBSUMsVUFBVWhCLEVBQ2IsY0FBY2UsSUFBZCxHQUFxQixRQURSLENBQWQ7QUFHQSxNQUFJRSwwQkFBMEJqQixFQUFFZ0IsT0FBRixFQUFXRSxlQUFYLENBQzdCO0FBQ0MscUJBQWtCO0FBRG5CLEdBRDZCLENBQTlCOztBQUtBbEIsSUFBRSx1QkFBdUJpQix1QkFBekIsRUFBa0RFLEVBQWxELENBQXFELE9BQXJELEVBQThELFNBQTlELEVBQXlFLFlBQVc7QUFDbkZuQixLQUFFa0IsZUFBRixDQUFrQixPQUFsQixFQUEyQkQsdUJBQTNCO0FBQ0EsT0FBSVIsTUFBTVcsUUFBTixDQUFlLFFBQWYsQ0FBSixFQUE4QjtBQUM3QixXQUFPLEtBQVA7QUFDQTtBQUNEWCxTQUFNWSxRQUFOLENBQWUsUUFBZjs7QUFFQSxPQUFJbEIsUUFBUUQsSUFBUixLQUFpQixTQUFyQixFQUFnQztBQUMvQixRQUFJSixPQUFPO0FBQ1YsYUFBUUssUUFBUUQsSUFETjtBQUVWLFdBQU1PLE1BQU1YLElBQU4sQ0FBVyxZQUFYO0FBRkksS0FBWDtBQUlBLElBTEQsTUFLTztBQUNOLFFBQUlBLE9BQU87QUFDVixhQUFRSyxRQUFRRCxJQUROO0FBRVYsYUFBUU8sTUFBTWEsSUFBTixDQUFXLEtBQVgsQ0FGRTtBQUdWLFdBQU1iLE1BQU1YLElBQU4sQ0FBVyxpQkFBWCxDQUhJO0FBSVYsZUFBVVcsTUFBTVgsSUFBTixDQUFXLHFCQUFYO0FBSkEsS0FBWDtBQU1BOztBQUdEYSxPQUFJQyxJQUFKLENBQVNXLEdBQVQsQ0FBYUMsSUFBYixDQUFrQjtBQUNqQixXQUFPLHFEQURVO0FBRWpCLFlBQVExQjtBQUZTLElBQWxCLEVBR0cyQixJQUhILENBR1EsVUFBU0MsUUFBVCxFQUFtQjtBQUMxQmpCLFVBQU1rQixPQUFOLENBQWMsSUFBZCxFQUFvQkMsTUFBcEI7QUFDQSxRQUFJNUIsRUFBRSxRQUFRRyxRQUFRRCxJQUFsQixFQUF3QjJCLE1BQXhCLEtBQW1DLENBQXZDLEVBQTBDO0FBQ3pDN0IsT0FBRSxRQUFRRyxRQUFRRCxJQUFsQixFQUF3QjRCLElBQXhCO0FBQ0E7QUFDRDlCLE1BQUUsYUFBRixFQUFpQitCLEdBQWpCLENBQXFCTCxTQUFTTSxVQUE5QjtBQUNBLElBVEQ7QUFVQSxHQWhDRDtBQWlDQSxFQXBERDs7QUFzREE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQW5DLFFBQU9vQyxJQUFQLEdBQWMsVUFBU1IsSUFBVCxFQUFlO0FBQzVCMUIsUUFBTW9CLEVBQU4sQ0FBUyxPQUFULEVBQWtCLGFBQWxCLEVBQWlDZCxjQUFqQztBQUNBb0I7QUFDQSxFQUhEOztBQUtBO0FBQ0EsUUFBTzVCLE1BQVA7QUFDQSxDQTVGRiIsImZpbGUiOiJvcmRlcnMvb3JkZXJzX3BkZl9kZWxldGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIG9yZGVyc19wZGZfZGVsZXRlLmpzIDIwMTYtMTAtMDRcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIE9yZGVyIFBERiBEZWxldGUgQ29udHJvbGxlclxuICpcbiAqIEBtb2R1bGUgQ29udHJvbGxlcnMvb3JkZXJzX3BkZl9kZWxldGVcbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnb3JkZXJzX3BkZl9kZWxldGUnLFxuXHRcblx0Wyd4aHInLCAnZmFsbGJhY2snXSxcblx0XG5cdC8qKiBAbGVuZHMgbW9kdWxlOkNvbnRyb2xsZXJzL29yZGVyc19wZGZfZGVsZXRlICovXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0ZGVmYXVsdHMgPSB7dHlwZTogJ2ludm9pY2UnfSxcblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXIgX2RlbGV0ZUhhbmRsZXIgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XG5cdFx0XHR2YXIgJHNlbGYgPSAkKHRoaXMpLFxuXHRcdFx0XHRkYXRhc2V0ID0gJC5leHRlbmQoe30sICR0aGlzLmRhdGEoKSwganNlLmxpYnMuZmFsbGJhY2suX2RhdGEoJHRoaXMsICdvcmRlcnNfcGRmX2RlbGV0ZScpKTtcblx0XHRcdFxuXHRcdFx0dmFyIGhyZWYgPVxuXHRcdFx0XHQnbGlnaHRib3hfY29uZmlybS5odG1sP3NlY3Rpb249YWRtaW5fb3JkZXJzJmFtcDttZXNzYWdlPURFTEVURV9QREZfQ09ORklSTV9NRVNTQUdFJmFtcDsnICtcblx0XHRcdFx0J2J1dHRvbnM9Y2FuY2VsLWRlbGV0ZSc7XG5cdFx0XHRcblx0XHRcdHZhciB0X2FfdGFnID0gJChcblx0XHRcdFx0JzxhIGhyZWY9XCInICsgaHJlZiArICdcIj48L2E+J1xuXHRcdFx0KTtcblx0XHRcdHZhciB0bXBfbGlnaHRib3hfaWRlbnRpZmllciA9ICQodF9hX3RhZykubGlnaHRib3hfcGx1Z2luKFxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0J2xpZ2h0Ym94X3dpZHRoJzogJzM2MHB4J1xuXHRcdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JCgnI2xpZ2h0Ym94X3BhY2thZ2VfJyArIHRtcF9saWdodGJveF9pZGVudGlmaWVyKS5vbignY2xpY2snLCAnLmRlbGV0ZScsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkLmxpZ2h0Ym94X3BsdWdpbignY2xvc2UnLCB0bXBfbGlnaHRib3hfaWRlbnRpZmllcik7XG5cdFx0XHRcdGlmICgkc2VsZi5oYXNDbGFzcygnYWN0aXZlJykpIHtcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdH1cblx0XHRcdFx0JHNlbGYuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKG9wdGlvbnMudHlwZSA9PT0gJ2ludm9pY2UnKSB7XG5cdFx0XHRcdFx0dmFyIGRhdGEgPSB7XG5cdFx0XHRcdFx0XHQndHlwZSc6IG9wdGlvbnMudHlwZSxcblx0XHRcdFx0XHRcdCdpZCc6ICRzZWxmLmRhdGEoJ2ludm9pY2UtaWQnKVxuXHRcdFx0XHRcdH07XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0dmFyIGRhdGEgPSB7XG5cdFx0XHRcdFx0XHQndHlwZSc6IG9wdGlvbnMudHlwZSxcblx0XHRcdFx0XHRcdCdmaWxlJzogJHNlbGYuYXR0cigncmVsJyksXG5cdFx0XHRcdFx0XHQnaWQnOiAkc2VsZi5kYXRhKCdwYWNraW5nLXNsaXAtaWQnKSxcblx0XHRcdFx0XHRcdCdudW1iZXInOiAkc2VsZi5kYXRhKCdwYWNraW5nLXNsaXAtbnVtYmVyJylcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRcblx0XHRcdFx0anNlLmxpYnMueGhyLnBvc3Qoe1xuXHRcdFx0XHRcdCd1cmwnOiAncmVxdWVzdF9wb3J0LnBocD9tb2R1bGU9T3JkZXJBZG1pbiZhY3Rpb249ZGVsZXRlUGRmJyxcblx0XHRcdFx0XHQnZGF0YSc6IGRhdGFcblx0XHRcdFx0fSkuZG9uZShmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdCRzZWxmLmNsb3Nlc3QoJ3RyJykucmVtb3ZlKCk7XG5cdFx0XHRcdFx0aWYgKCQoJ3RyLicgKyBvcHRpb25zLnR5cGUpLmxlbmd0aCA9PT0gMSkge1xuXHRcdFx0XHRcdFx0JCgndHIuJyArIG9wdGlvbnMudHlwZSkuc2hvdygpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQkKCcucGFnZV90b2tlbicpLnZhbChyZXNwb25zZS5wYWdlX3Rva2VuKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdCBmdW5jdGlvbiBvZiB0aGUgd2lkZ2V0XG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkdGhpcy5vbignY2xpY2snLCAnLmRlbGV0ZV9wZGYnLCBfZGVsZXRlSGFuZGxlcik7XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byB3aWRnZXQgZW5naW5lXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
