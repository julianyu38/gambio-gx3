'use strict';

/* --------------------------------------------------------------
 bulk_email_order.js 2016-05-25
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Bulk Email Order Modal Controller
 */
gx.controllers.module('bulk_email_order', [jse.source + '/vendor/momentjs/moment.min.js', 'modal', 'loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: { subject: $this.find('.subject') }
	};

	/**
  * Selector for the email list item.
  *
  * @type {String}
  */
	var emailListItemSelector = '.email-list-item';

	/**
  * Selector for the email list item ID.
  *
  * @type {String}
  */
	var emailListItemEmailSelector = '.email-input';

	/**
  * Selector for the modal content body layer.
  *
  * @type {String}
  */
	var modalContentSelector = '.modal-content';

	/**
  * Placeholder map.
  * Used to replace the placeholder with the respective variables.
  *
  * Format: '{Placeholder}' : 'Attribute'
  *
  * @type {Object}
  */
	var placeholderMap = {
		'{ORDER_ID}': 'id',
		'{ORDER_DATE}': 'purchaseDate'
	};

	/**
  * Loading spinner instance.
  *
  * @type {jQuery|null}
  */
	var $spinner = null;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Show/hide loading spinner.
  *
  * @param {Boolean} doShow Show the loading spinner?
  */
	function _toggleSpinner(doShow) {
		if (doShow) {
			$spinner = jse.libs.loading_spinner.show($this.find(modalContentSelector), $this.css('z-index'));
		} else {
			jse.libs.loading_spinner.hide($spinner);
		}
	}

	/**
  * Parse subject and replace the placeholders with the variables.
  *
  * @param {Object} orderData Order data.
  *
  * @return {String}
  */
	function _getParsedSubject(orderData) {
		// Subject.
		var subject = module.bindings.subject.get();

		// Iterate over the placeholders and replace the values.
		Object.keys(placeholderMap).forEach(function (placeholder) {
			return subject = subject.replace(placeholder, orderData[placeholderMap[placeholder]]);
		});

		return subject;
	}

	/**
  * Handles the successful delivery of all messages.
  */
	function _handleDeliverySuccess() {
		var message = jse.core.lang.translate('BULK_MAIL_SUCCESS', 'gm_send_order');

		// Show success message in the admin info box.
		jse.libs.info_box.addSuccessMessage(message);

		// Hide modal and loading spinner.
		_toggleSpinner(false);
		$this.modal('hide');
	}

	/**
  * Handles the failure of the message delivery.
  */
	function _handleDeliveryFail() {
		var title = jse.core.lang.translate('error', 'messages');
		var content = jse.core.lang.translate('BULK_MAIL_UNSUCCESS', 'gm_send_order');

		// Show error message in a modal.
		jse.libs.modal.message({ title: title, content: content });

		// Hide modal and the loading spinner and reenable the send button.
		_toggleSpinner(false);
		$this.modal('hide');
	}

	/**
  * Send the modal data to the form through an AJAX call.
  */
	function _onSendClick() {
		// Send type.
		var REQUEST_SEND_TYPE = 'send_order';

		// Request base URL.
		var REQUEST_URL = jse.core.config.get('appUrl') + '/admin/gm_send_order.php';

		// Collection of requests in promise format.
		var promises = [];

		// Email list item elements.
		var $emailListItems = $this.find(emailListItemSelector);

		// Abort and hide modal on empty email list entries.
		if (!$emailListItems.length) {
			$this.modal('hide');
			return;
		}

		// Show loading spinner.
		_toggleSpinner(true);

		// Fill orders array with data.
		$emailListItems.each(function (index, element) {
			// Order data.
			var orderData = $(element).data('order');

			// Format the purchase date.
			var dateFormat = jse.core.config.get('languageCode') === 'de' ? 'DD.MM.YY' : 'MM.DD.YY';
			orderData.purchaseDate = moment(orderData.purchaseDate.date).format(dateFormat);

			// Email address entered in input field.
			var enteredEmail = $(element).find(emailListItemEmailSelector).val();

			// Request GET parameters to send.
			var getParameters = {
				oID: orderData.id,
				type: REQUEST_SEND_TYPE
			};

			// Composed request URL.
			var url = REQUEST_URL + '?' + $.param(getParameters);

			// Data to send.
			var data = {
				gm_mail: enteredEmail,
				gm_subject: _getParsedSubject(orderData)
			};

			// Promise wrapper for AJAX response.
			var promise = new Promise(function (resolve, reject) {
				// Create AJAX request.
				var request = $.ajax({ method: 'POST', url: url, data: data });

				request.success(function () {
					var orderId = getParameters.oID;
					var $tableRow = $('tbody tr#' + orderId);

					// Remove the e-mail symbol
					$tableRow.find('td.actions i.tooltip-confirmation-not-sent').remove();
				});

				// Resolve promise on success.
				request.done(function (response) {
					return resolve(response);
				});

				// Reject promise on fail.
				request.fail(function () {
					return reject();
				});
			});

			// Add promise to array.
			promises.push(promise);
		});

		// Wait for all promise to respond and handle success/error.
		Promise.all(promises).then(_handleDeliverySuccess).catch(_handleDeliveryFail);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvYnVsa19lbWFpbF9vcmRlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImJpbmRpbmdzIiwic3ViamVjdCIsImZpbmQiLCJlbWFpbExpc3RJdGVtU2VsZWN0b3IiLCJlbWFpbExpc3RJdGVtRW1haWxTZWxlY3RvciIsIm1vZGFsQ29udGVudFNlbGVjdG9yIiwicGxhY2Vob2xkZXJNYXAiLCIkc3Bpbm5lciIsIl90b2dnbGVTcGlubmVyIiwiZG9TaG93IiwibGlicyIsImxvYWRpbmdfc3Bpbm5lciIsInNob3ciLCJjc3MiLCJoaWRlIiwiX2dldFBhcnNlZFN1YmplY3QiLCJvcmRlckRhdGEiLCJnZXQiLCJPYmplY3QiLCJrZXlzIiwiZm9yRWFjaCIsInJlcGxhY2UiLCJwbGFjZWhvbGRlciIsIl9oYW5kbGVEZWxpdmVyeVN1Y2Nlc3MiLCJtZXNzYWdlIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJpbmZvX2JveCIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwibW9kYWwiLCJfaGFuZGxlRGVsaXZlcnlGYWlsIiwidGl0bGUiLCJjb250ZW50IiwiX29uU2VuZENsaWNrIiwiUkVRVUVTVF9TRU5EX1RZUEUiLCJSRVFVRVNUX1VSTCIsImNvbmZpZyIsInByb21pc2VzIiwiJGVtYWlsTGlzdEl0ZW1zIiwibGVuZ3RoIiwiZWFjaCIsImluZGV4IiwiZWxlbWVudCIsImRhdGVGb3JtYXQiLCJwdXJjaGFzZURhdGUiLCJtb21lbnQiLCJkYXRlIiwiZm9ybWF0IiwiZW50ZXJlZEVtYWlsIiwidmFsIiwiZ2V0UGFyYW1ldGVycyIsIm9JRCIsImlkIiwidHlwZSIsInVybCIsInBhcmFtIiwiZ21fbWFpbCIsImdtX3N1YmplY3QiLCJwcm9taXNlIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJyZXF1ZXN0IiwiYWpheCIsIm1ldGhvZCIsInN1Y2Nlc3MiLCJvcmRlcklkIiwiJHRhYmxlUm93IiwicmVtb3ZlIiwiZG9uZSIsInJlc3BvbnNlIiwiZmFpbCIsInB1c2giLCJhbGwiLCJ0aGVuIiwiY2F0Y2giLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7O0FBR0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLGtCQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixxQ0FFQyxPQUZELEVBR0MsaUJBSEQsQ0FIRCxFQVNDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1MLFNBQVM7QUFDZE0sWUFBVSxFQUFDQyxTQUFTSCxNQUFNSSxJQUFOLENBQVcsVUFBWCxDQUFWO0FBREksRUFBZjs7QUFJQTs7Ozs7QUFLQSxLQUFNQyx3QkFBd0Isa0JBQTlCOztBQUVBOzs7OztBQUtBLEtBQU1DLDZCQUE2QixjQUFuQzs7QUFFQTs7Ozs7QUFLQSxLQUFNQyx1QkFBdUIsZ0JBQTdCOztBQUVBOzs7Ozs7OztBQVFBLEtBQU1DLGlCQUFpQjtBQUN0QixnQkFBYyxJQURRO0FBRXRCLGtCQUFnQjtBQUZNLEVBQXZCOztBQUtBOzs7OztBQUtBLEtBQUlDLFdBQVcsSUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0MsY0FBVCxDQUF3QkMsTUFBeEIsRUFBZ0M7QUFDL0IsTUFBSUEsTUFBSixFQUFZO0FBQ1hGLGNBQVdaLElBQUllLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsSUFBekIsQ0FBOEJkLE1BQU1JLElBQU4sQ0FBV0csb0JBQVgsQ0FBOUIsRUFBZ0VQLE1BQU1lLEdBQU4sQ0FBVSxTQUFWLENBQWhFLENBQVg7QUFDQSxHQUZELE1BRU87QUFDTmxCLE9BQUllLElBQUosQ0FBU0MsZUFBVCxDQUF5QkcsSUFBekIsQ0FBOEJQLFFBQTlCO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFVBQVNRLGlCQUFULENBQTJCQyxTQUEzQixFQUFzQztBQUNyQztBQUNBLE1BQUlmLFVBQVVQLE9BQU9NLFFBQVAsQ0FBZ0JDLE9BQWhCLENBQXdCZ0IsR0FBeEIsRUFBZDs7QUFFQTtBQUNBQyxTQUNFQyxJQURGLENBQ09iLGNBRFAsRUFFRWMsT0FGRixDQUVVO0FBQUEsVUFBZW5CLFVBQVVBLFFBQVFvQixPQUFSLENBQWdCQyxXQUFoQixFQUE2Qk4sVUFBVVYsZUFBZWdCLFdBQWYsQ0FBVixDQUE3QixDQUF6QjtBQUFBLEdBRlY7O0FBSUEsU0FBT3JCLE9BQVA7QUFDQTs7QUFFRDs7O0FBR0EsVUFBU3NCLHNCQUFULEdBQWtDO0FBQ2pDLE1BQU1DLFVBQVU3QixJQUFJOEIsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUJBQXhCLEVBQTZDLGVBQTdDLENBQWhCOztBQUVBO0FBQ0FoQyxNQUFJZSxJQUFKLENBQVNrQixRQUFULENBQWtCQyxpQkFBbEIsQ0FBb0NMLE9BQXBDOztBQUVBO0FBQ0FoQixpQkFBZSxLQUFmO0FBQ0FWLFFBQU1nQyxLQUFOLENBQVksTUFBWjtBQUNBOztBQUVEOzs7QUFHQSxVQUFTQyxtQkFBVCxHQUErQjtBQUM5QixNQUFNQyxRQUFRckMsSUFBSThCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBQWQ7QUFDQSxNQUFNTSxVQUFVdEMsSUFBSThCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHFCQUF4QixFQUErQyxlQUEvQyxDQUFoQjs7QUFFQTtBQUNBaEMsTUFBSWUsSUFBSixDQUFTb0IsS0FBVCxDQUFlTixPQUFmLENBQXVCLEVBQUNRLFlBQUQsRUFBUUMsZ0JBQVIsRUFBdkI7O0FBRUE7QUFDQXpCLGlCQUFlLEtBQWY7QUFDQVYsUUFBTWdDLEtBQU4sQ0FBWSxNQUFaO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNJLFlBQVQsR0FBd0I7QUFDdkI7QUFDQSxNQUFNQyxvQkFBb0IsWUFBMUI7O0FBRUE7QUFDQSxNQUFNQyxjQUFjekMsSUFBSThCLElBQUosQ0FBU1ksTUFBVCxDQUFnQnBCLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLDBCQUFwRDs7QUFFQTtBQUNBLE1BQU1xQixXQUFXLEVBQWpCOztBQUVBO0FBQ0EsTUFBTUMsa0JBQWtCekMsTUFBTUksSUFBTixDQUFXQyxxQkFBWCxDQUF4Qjs7QUFFQTtBQUNBLE1BQUksQ0FBQ29DLGdCQUFnQkMsTUFBckIsRUFBNkI7QUFDNUIxQyxTQUFNZ0MsS0FBTixDQUFZLE1BQVo7QUFDQTtBQUNBOztBQUVEO0FBQ0F0QixpQkFBZSxJQUFmOztBQUVBO0FBQ0ErQixrQkFBZ0JFLElBQWhCLENBQXFCLFVBQUNDLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUN4QztBQUNBLE9BQU0zQixZQUFZakIsRUFBRTRDLE9BQUYsRUFBVzlDLElBQVgsQ0FBZ0IsT0FBaEIsQ0FBbEI7O0FBRUE7QUFDQSxPQUFNK0MsYUFBYWpELElBQUk4QixJQUFKLENBQVNZLE1BQVQsQ0FBZ0JwQixHQUFoQixDQUFvQixjQUFwQixNQUF3QyxJQUF4QyxHQUErQyxVQUEvQyxHQUE0RCxVQUEvRTtBQUNBRCxhQUFVNkIsWUFBVixHQUF5QkMsT0FBTzlCLFVBQVU2QixZQUFWLENBQXVCRSxJQUE5QixFQUFvQ0MsTUFBcEMsQ0FBMkNKLFVBQTNDLENBQXpCOztBQUVBO0FBQ0EsT0FBTUssZUFBZWxELEVBQUU0QyxPQUFGLEVBQVd6QyxJQUFYLENBQWdCRSwwQkFBaEIsRUFBNEM4QyxHQUE1QyxFQUFyQjs7QUFFQTtBQUNBLE9BQU1DLGdCQUFnQjtBQUNyQkMsU0FBS3BDLFVBQVVxQyxFQURNO0FBRXJCQyxVQUFNbkI7QUFGZSxJQUF0Qjs7QUFLQTtBQUNBLE9BQU1vQixNQUFNbkIsY0FBYyxHQUFkLEdBQW9CckMsRUFBRXlELEtBQUYsQ0FBUUwsYUFBUixDQUFoQzs7QUFFQTtBQUNBLE9BQU10RCxPQUFPO0FBQ1o0RCxhQUFTUixZQURHO0FBRVpTLGdCQUFZM0Msa0JBQWtCQyxTQUFsQjtBQUZBLElBQWI7O0FBS0E7QUFDQSxPQUFNMkMsVUFBVSxJQUFJQyxPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ2hEO0FBQ0EsUUFBTUMsVUFBVWhFLEVBQUVpRSxJQUFGLENBQU8sRUFBQ0MsUUFBUSxNQUFULEVBQWlCVixRQUFqQixFQUFzQjFELFVBQXRCLEVBQVAsQ0FBaEI7O0FBRUFrRSxZQUFRRyxPQUFSLENBQWdCLFlBQU07QUFDckIsU0FBTUMsVUFBVWhCLGNBQWNDLEdBQTlCO0FBQ0EsU0FBTWdCLFlBQVlyRSxnQkFBY29FLE9BQWQsQ0FBbEI7O0FBRUE7QUFDQUMsZUFBVWxFLElBQVYsQ0FBZSw0Q0FBZixFQUE2RG1FLE1BQTdEO0FBQ0EsS0FORDs7QUFRQTtBQUNBTixZQUFRTyxJQUFSLENBQWE7QUFBQSxZQUFZVCxRQUFRVSxRQUFSLENBQVo7QUFBQSxLQUFiOztBQUVBO0FBQ0FSLFlBQVFTLElBQVIsQ0FBYTtBQUFBLFlBQU1WLFFBQU47QUFBQSxLQUFiO0FBQ0EsSUFqQmUsQ0FBaEI7O0FBbUJBO0FBQ0F4QixZQUFTbUMsSUFBVCxDQUFjZCxPQUFkO0FBQ0EsR0FoREQ7O0FBa0RBO0FBQ0FDLFVBQVFjLEdBQVIsQ0FBWXBDLFFBQVosRUFDRXFDLElBREYsQ0FDT3BELHNCQURQLEVBRUVxRCxLQUZGLENBRVE3QyxtQkFGUjtBQUdBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQXJDLFFBQU9tRixJQUFQLEdBQWMsVUFBU1AsSUFBVCxFQUFlO0FBQzVCeEUsUUFBTWdGLEVBQU4sQ0FBUyxPQUFULEVBQWtCLFdBQWxCLEVBQStCNUMsWUFBL0I7QUFDQW9DO0FBQ0EsRUFIRDs7QUFLQSxRQUFPNUUsTUFBUDtBQUNBLENBdk9GIiwiZmlsZSI6Im9yZGVycy9tb2RhbHMvYnVsa19lbWFpbF9vcmRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gYnVsa19lbWFpbF9vcmRlci5qcyAyMDE2LTA1LTI1XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBCdWxrIEVtYWlsIE9yZGVyIE1vZGFsIENvbnRyb2xsZXJcbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnYnVsa19lbWFpbF9vcmRlcicsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL21vbWVudGpzL21vbWVudC5taW4uanNgLFxuXHRcdCdtb2RhbCcsXG5cdFx0J2xvYWRpbmdfc3Bpbm5lcidcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge1xuXHRcdFx0YmluZGluZ3M6IHtzdWJqZWN0OiAkdGhpcy5maW5kKCcuc3ViamVjdCcpfVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2VsZWN0b3IgZm9yIHRoZSBlbWFpbCBsaXN0IGl0ZW0uXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGNvbnN0IGVtYWlsTGlzdEl0ZW1TZWxlY3RvciA9ICcuZW1haWwtbGlzdC1pdGVtJztcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZWxlY3RvciBmb3IgdGhlIGVtYWlsIGxpc3QgaXRlbSBJRC5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtTdHJpbmd9XG5cdFx0ICovXG5cdFx0Y29uc3QgZW1haWxMaXN0SXRlbUVtYWlsU2VsZWN0b3IgPSAnLmVtYWlsLWlucHV0Jztcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZWxlY3RvciBmb3IgdGhlIG1vZGFsIGNvbnRlbnQgYm9keSBsYXllci5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtTdHJpbmd9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kYWxDb250ZW50U2VsZWN0b3IgPSAnLm1vZGFsLWNvbnRlbnQnO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFBsYWNlaG9sZGVyIG1hcC5cblx0XHQgKiBVc2VkIHRvIHJlcGxhY2UgdGhlIHBsYWNlaG9sZGVyIHdpdGggdGhlIHJlc3BlY3RpdmUgdmFyaWFibGVzLlxuXHRcdCAqXG5cdFx0ICogRm9ybWF0OiAne1BsYWNlaG9sZGVyfScgOiAnQXR0cmlidXRlJ1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBwbGFjZWhvbGRlck1hcCA9IHtcblx0XHRcdCd7T1JERVJfSUR9JzogJ2lkJyxcblx0XHRcdCd7T1JERVJfREFURX0nOiAncHVyY2hhc2VEYXRlJ1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTG9hZGluZyBzcGlubmVyIGluc3RhbmNlLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeXxudWxsfVxuXHRcdCAqL1xuXHRcdGxldCAkc3Bpbm5lciA9IG51bGw7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2hvdy9oaWRlIGxvYWRpbmcgc3Bpbm5lci5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7Qm9vbGVhbn0gZG9TaG93IFNob3cgdGhlIGxvYWRpbmcgc3Bpbm5lcj9cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfdG9nZ2xlU3Bpbm5lcihkb1Nob3cpIHtcblx0XHRcdGlmIChkb1Nob3cpIHtcblx0XHRcdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcy5maW5kKG1vZGFsQ29udGVudFNlbGVjdG9yKSwgJHRoaXMuY3NzKCd6LWluZGV4JykpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0anNlLmxpYnMubG9hZGluZ19zcGlubmVyLmhpZGUoJHNwaW5uZXIpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBQYXJzZSBzdWJqZWN0IGFuZCByZXBsYWNlIHRoZSBwbGFjZWhvbGRlcnMgd2l0aCB0aGUgdmFyaWFibGVzLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IG9yZGVyRGF0YSBPcmRlciBkYXRhLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRQYXJzZWRTdWJqZWN0KG9yZGVyRGF0YSkge1xuXHRcdFx0Ly8gU3ViamVjdC5cblx0XHRcdGxldCBzdWJqZWN0ID0gbW9kdWxlLmJpbmRpbmdzLnN1YmplY3QuZ2V0KCk7XG5cdFx0XHRcblx0XHRcdC8vIEl0ZXJhdGUgb3ZlciB0aGUgcGxhY2Vob2xkZXJzIGFuZCByZXBsYWNlIHRoZSB2YWx1ZXMuXG5cdFx0XHRPYmplY3Rcblx0XHRcdFx0LmtleXMocGxhY2Vob2xkZXJNYXApXG5cdFx0XHRcdC5mb3JFYWNoKHBsYWNlaG9sZGVyID0+IHN1YmplY3QgPSBzdWJqZWN0LnJlcGxhY2UocGxhY2Vob2xkZXIsIG9yZGVyRGF0YVtwbGFjZWhvbGRlck1hcFtwbGFjZWhvbGRlcl1dKSk7XG5cdFx0XHRcblx0XHRcdHJldHVybiBzdWJqZWN0O1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBzdWNjZXNzZnVsIGRlbGl2ZXJ5IG9mIGFsbCBtZXNzYWdlcy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfaGFuZGxlRGVsaXZlcnlTdWNjZXNzKCkge1xuXHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVUxLX01BSUxfU1VDQ0VTUycsICdnbV9zZW5kX29yZGVyJyk7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgc3VjY2VzcyBtZXNzYWdlIGluIHRoZSBhZG1pbiBpbmZvIGJveC5cblx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKG1lc3NhZ2UpO1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRlIG1vZGFsIGFuZCBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHRfdG9nZ2xlU3Bpbm5lcihmYWxzZSk7XG5cdFx0XHQkdGhpcy5tb2RhbCgnaGlkZScpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBmYWlsdXJlIG9mIHRoZSBtZXNzYWdlIGRlbGl2ZXJ5LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9oYW5kbGVEZWxpdmVyeUZhaWwoKSB7XG5cdFx0XHRjb25zdCB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpO1xuXHRcdFx0Y29uc3QgY29udGVudCA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVUxLX01BSUxfVU5TVUNDRVNTJywgJ2dtX3NlbmRfb3JkZXInKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBlcnJvciBtZXNzYWdlIGluIGEgbW9kYWwuXG5cdFx0XHRqc2UubGlicy5tb2RhbC5tZXNzYWdlKHt0aXRsZSwgY29udGVudH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRlIG1vZGFsIGFuZCB0aGUgbG9hZGluZyBzcGlubmVyIGFuZCByZWVuYWJsZSB0aGUgc2VuZCBidXR0b24uXG5cdFx0XHRfdG9nZ2xlU3Bpbm5lcihmYWxzZSk7XG5cdFx0XHQkdGhpcy5tb2RhbCgnaGlkZScpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTZW5kIHRoZSBtb2RhbCBkYXRhIHRvIHRoZSBmb3JtIHRocm91Z2ggYW4gQUpBWCBjYWxsLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblNlbmRDbGljaygpIHtcblx0XHRcdC8vIFNlbmQgdHlwZS5cblx0XHRcdGNvbnN0IFJFUVVFU1RfU0VORF9UWVBFID0gJ3NlbmRfb3JkZXInO1xuXHRcdFx0XG5cdFx0XHQvLyBSZXF1ZXN0IGJhc2UgVVJMLlxuXHRcdFx0Y29uc3QgUkVRVUVTVF9VUkwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vZ21fc2VuZF9vcmRlci5waHAnO1xuXHRcdFx0XG5cdFx0XHQvLyBDb2xsZWN0aW9uIG9mIHJlcXVlc3RzIGluIHByb21pc2UgZm9ybWF0LlxuXHRcdFx0Y29uc3QgcHJvbWlzZXMgPSBbXTtcblx0XHRcdFxuXHRcdFx0Ly8gRW1haWwgbGlzdCBpdGVtIGVsZW1lbnRzLlxuXHRcdFx0Y29uc3QgJGVtYWlsTGlzdEl0ZW1zID0gJHRoaXMuZmluZChlbWFpbExpc3RJdGVtU2VsZWN0b3IpO1xuXHRcdFx0XG5cdFx0XHQvLyBBYm9ydCBhbmQgaGlkZSBtb2RhbCBvbiBlbXB0eSBlbWFpbCBsaXN0IGVudHJpZXMuXG5cdFx0XHRpZiAoISRlbWFpbExpc3RJdGVtcy5sZW5ndGgpIHtcblx0XHRcdFx0JHRoaXMubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTaG93IGxvYWRpbmcgc3Bpbm5lci5cblx0XHRcdF90b2dnbGVTcGlubmVyKHRydWUpO1xuXHRcdFx0XG5cdFx0XHQvLyBGaWxsIG9yZGVycyBhcnJheSB3aXRoIGRhdGEuXG5cdFx0XHQkZW1haWxMaXN0SXRlbXMuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcblx0XHRcdFx0Ly8gT3JkZXIgZGF0YS5cblx0XHRcdFx0Y29uc3Qgb3JkZXJEYXRhID0gJChlbGVtZW50KS5kYXRhKCdvcmRlcicpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRm9ybWF0IHRoZSBwdXJjaGFzZSBkYXRlLlxuXHRcdFx0XHRjb25zdCBkYXRlRm9ybWF0ID0ganNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJykgPT09ICdkZScgPyAnREQuTU0uWVknIDogJ01NLkRELllZJztcblx0XHRcdFx0b3JkZXJEYXRhLnB1cmNoYXNlRGF0ZSA9IG1vbWVudChvcmRlckRhdGEucHVyY2hhc2VEYXRlLmRhdGUpLmZvcm1hdChkYXRlRm9ybWF0KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEVtYWlsIGFkZHJlc3MgZW50ZXJlZCBpbiBpbnB1dCBmaWVsZC5cblx0XHRcdFx0Y29uc3QgZW50ZXJlZEVtYWlsID0gJChlbGVtZW50KS5maW5kKGVtYWlsTGlzdEl0ZW1FbWFpbFNlbGVjdG9yKS52YWwoKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFJlcXVlc3QgR0VUIHBhcmFtZXRlcnMgdG8gc2VuZC5cblx0XHRcdFx0Y29uc3QgZ2V0UGFyYW1ldGVycyA9IHtcblx0XHRcdFx0XHRvSUQ6IG9yZGVyRGF0YS5pZCxcblx0XHRcdFx0XHR0eXBlOiBSRVFVRVNUX1NFTkRfVFlQRVxuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQ29tcG9zZWQgcmVxdWVzdCBVUkwuXG5cdFx0XHRcdGNvbnN0IHVybCA9IFJFUVVFU1RfVVJMICsgJz8nICsgJC5wYXJhbShnZXRQYXJhbWV0ZXJzKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIERhdGEgdG8gc2VuZC5cblx0XHRcdFx0Y29uc3QgZGF0YSA9IHtcblx0XHRcdFx0XHRnbV9tYWlsOiBlbnRlcmVkRW1haWwsXG5cdFx0XHRcdFx0Z21fc3ViamVjdDogX2dldFBhcnNlZFN1YmplY3Qob3JkZXJEYXRhKVxuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUHJvbWlzZSB3cmFwcGVyIGZvciBBSkFYIHJlc3BvbnNlLlxuXHRcdFx0XHRjb25zdCBwcm9taXNlID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0XHRcdC8vIENyZWF0ZSBBSkFYIHJlcXVlc3QuXG5cdFx0XHRcdFx0Y29uc3QgcmVxdWVzdCA9ICQuYWpheCh7bWV0aG9kOiAnUE9TVCcsIHVybCwgZGF0YX0pO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdHJlcXVlc3Quc3VjY2VzcygoKSA9PiB7XG5cdFx0XHRcdFx0XHRjb25zdCBvcmRlcklkID0gZ2V0UGFyYW1ldGVycy5vSUQ7XG5cdFx0XHRcdFx0XHRjb25zdCAkdGFibGVSb3cgPSAkKGB0Ym9keSB0ciMke29yZGVySWR9YCk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdC8vIFJlbW92ZSB0aGUgZS1tYWlsIHN5bWJvbFxuXHRcdFx0XHRcdFx0JHRhYmxlUm93LmZpbmQoJ3RkLmFjdGlvbnMgaS50b29sdGlwLWNvbmZpcm1hdGlvbi1ub3Qtc2VudCcpLnJlbW92ZSgpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFJlc29sdmUgcHJvbWlzZSBvbiBzdWNjZXNzLlxuXHRcdFx0XHRcdHJlcXVlc3QuZG9uZShyZXNwb25zZSA9PiByZXNvbHZlKHJlc3BvbnNlKSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gUmVqZWN0IHByb21pc2Ugb24gZmFpbC5cblx0XHRcdFx0XHRyZXF1ZXN0LmZhaWwoKCkgPT4gcmVqZWN0KCkpO1xuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFkZCBwcm9taXNlIHRvIGFycmF5LlxuXHRcdFx0XHRwcm9taXNlcy5wdXNoKHByb21pc2UpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIFdhaXQgZm9yIGFsbCBwcm9taXNlIHRvIHJlc3BvbmQgYW5kIGhhbmRsZSBzdWNjZXNzL2Vycm9yLlxuXHRcdFx0UHJvbWlzZS5hbGwocHJvbWlzZXMpXG5cdFx0XHRcdC50aGVuKF9oYW5kbGVEZWxpdmVyeVN1Y2Nlc3MpXG5cdFx0XHRcdC5jYXRjaChfaGFuZGxlRGVsaXZlcnlGYWlsKTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCR0aGlzLm9uKCdjbGljaycsICcuYnRuLnNlbmQnLCBfb25TZW5kQ2xpY2spO1xuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7Il19
