'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/* --------------------------------------------------------------
 events.js 2018-06-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Events
 *
 * Handles the events of the main invoices table.
 */
gx.controllers.module('events', ['loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Loading spinner instance.
  *
  * @type {jQuery|null}
  */
	var $spinner = null;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * On Bulk Selection Change
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Boolean} propagate Whether to affect the body elements. We do not need this on "draw.dt" event.
  */
	function _onBulkSelectionChange(event) {
		var propagate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		if (propagate === false) {
			return; // Do not propagate on draw event because the body checkboxes are unchecked by default.
		}

		$this.find('tbody input:checkbox').single_checkbox('checked', $(this).prop('checked')).trigger('change');
	}

	/**
  * On Table Row Click
  *
  * When a row is clicked then the row-checkbox must be toggled.
  *
  * @param {jQuery.Event} event
  */
	function _onTableRowClick(event) {
		if (!$(event.target).is('td')) {
			return;
		}

		$(this).find('input:checkbox').prop('checked', !$(this).find('input:checkbox').prop('checked')).trigger('change');
	}

	/**
  * On Table Row Checkbox Change
  *
  * Adjust the bulk actions state whenever there are changes in the table checkboxes.
  */
	function _onTableRowCheckboxChange() {
		if ($this.find('input:checkbox:checked').length > 0) {
			$this.parents('.invoices').find('.bulk-action > button').removeClass('disabled');
		} else {
			$this.parents('.invoices').find('.bulk-action > button').addClass('disabled');
		}
	}

	/**
  * Collects the required data of the selected invoices in an array.
  *
  * @param {jQuery} $target Target element which triggered the data collection.
  *
  * @return {*[]} Returns an array of the requested invoice data.
  */
	function _getSelectedInvoicesData($target, dataKey) {
		var selectedInvoices = [];

		if ($target.parents('.bulk-action').length > 0) {
			// Fetch the selected order IDs.
			$this.find('tbody input:checkbox:checked').each(function (index, checkbox) {
				return selectedInvoices.push($(checkbox).parents('tr').data(dataKey));
			});
		} else {
			var rowId = $target.parents('tr').data(dataKey);

			if (!rowId) {
				return; // No invoice ID was found.
			}

			selectedInvoices.push(rowId);
		}

		return selectedInvoices;
	}

	/**
  * On Delete Invoice Click
  *
  * Display the delete-modal.
  *
  * @param {jQuery.Event} event
  */
	function _onDeleteInvoiceClick(event) {
		event.preventDefault();

		var selectedInvoiceIds = _getSelectedInvoicesData($(this), 'invoiceId');
		var selectedInvoiceNumbers = _getSelectedInvoicesData($(this), 'invoiceNumber');

		// Show the order delete modal.
		var $modal = $('.delete.modal');
		$modal.find('.selected-invoice-ids').val(selectedInvoiceIds);
		$modal.find('.selected-invoice-numbers').text(selectedInvoiceNumbers.join(', '));
		$modal.modal('show');
	}

	/**
  * On Change Order Status Click
  *
  * Display the change order status modal.
  *
  * @param {jQuery.Event} event
  */
	function _onChangeOrderStatusClick(event) {
		if ($(event.target).hasClass('order-status')) {
			event.stopPropagation();
		}

		var $modal = $('.status.modal');
		var rowData = $(this).parents('tr').data();
		var selectedOrders = _getSelectedInvoicesData($(this), 'orderId').filter(function (orderId) {
			return orderId !== 0;
		});

		if (!selectedOrders.length) {
			var title = jse.core.lang.translate('HEADING_GM_STATUS', 'orders');
			var message = jse.core.lang.translate('NO_RECORDS_ERROR', 'orders');
			jse.libs.modal.showMessage(title, message);
			return;
		}

		$modal.find('#status-dropdown').val(rowData ? rowData.statusId : '');
		$modal.find('#comment').val('');
		$modal.find('#notify-customer, #send-parcel-tracking-code, #send-comment').attr('checked', false).parents('.single-checkbox').removeClass('checked');

		// Show the order change status modal (remove duplicate entries from selectedOrders).
		$modal.find('.selected-orders').text(Array.from(new Set(selectedOrders)).join(', '));
		$modal.modal('show');
	}

	/**
  * Opens the URL which provide the bulk PDF's as download.
  *
  * @param {Number[]} invoiceIds The invoices to be concatenated.
  */
	function _openBulkPdfUrl(invoiceIds) {
		var parameters = {
			do: 'InvoicesModalsAjax/BulkPdfInvoices',
			pageToken: jse.core.config.get('pageToken'),
			i: invoiceIds
		};

		var url = jse.core.config.get('appUrl') + '/admin/admin.php?' + $.param(parameters);

		window.open(url, '_parent');

		jse.libs.loading_spinner.hide($spinner);
	}

	/**
  * Creates a bulk PDF with invoices.
  *
  * @param {Number[]} invoiceIds The invoices to be concatenated.
  */
	function _createBulkPdf(invoiceIds) {
		var zIndex = $('.table-fixed-header thead.fixed').css('z-index'); // Could be "undefined" as well.
		$spinner = jse.libs.loading_spinner.show($this, zIndex);
		_openBulkPdfUrl(invoiceIds);
	}

	/**
  * Opens the gm_pdf_order.php in a new tab with invoices as type $_GET argument.
  *
  * The order ids are passed as a serialized array to the oID $_GET argument.
  */
	function _onBulkDownloadInvoiceClick() {
		var invoiceIds = _getSelectedInvoicesData($(this), 'invoiceId');

		if (invoiceIds.length > data.maxAmountInvoicesBulkPdf) {
			var $modal = $('.bulk-error.modal');
			$modal.modal('show');

			var $invoiceMessageContainer = $modal.find('.invoices-message');
			$invoiceMessageContainer.removeClass('hidden');
			$modal.on('hide.bs.modal', function () {
				return $invoiceMessageContainer.addClass('hidden');
			});

			return;
		}

		_createBulkPdf(invoiceIds);
	}

	/**
  * On Single Checkbox Ready
  *
  * This callback will use the event.data.invoiceIds to set the checked checkboxes after a table re-render.
  *
  * @param {jQuery.Event} event
  */
	function _onSingleCheckboxReady(event) {
		event.data.invoiceIds.forEach(function (id) {
			$this.find('tr#' + id + ' input:checkbox').single_checkbox('checked', true).trigger('change');
		});

		// Bulk action button should't be disabled after a datatable reload.
		if ($('tr input:checkbox:checked').length) {
			$('.bulk-action').find('button').removeClass('disabled');
		}
	}

	/**
  * Cancellation Invoice Generation
  *
  * This method will create a new cancellation invoice for the selected invoices.
  */
	function _onCancellationInvoiceClick() {
		var invoices = [];

		var _$$parents$data = $(this).parents('tr').data(),
		    invoiceId = _$$parents$data.invoiceId,
		    invoiceNumber = _$$parents$data.invoiceNumber,
		    orderId = _$$parents$data.orderId,
		    isCancellationInvoice = _$$parents$data.isCancellationInvoice;

		if (isCancellationInvoice || orderId === 0) {
			return;
		}

		invoices.push({
			invoiceId: invoiceId,
			invoiceNumber: invoiceNumber,
			orderId: orderId
		});

		var title = jse.core.lang.translate('HEADING_MODAL_CANCELLATION_INVOICE', 'orders');
		var message = jse.core.lang.translate('TEXT_MODAL_CANCELLATION_INVOICE', 'orders').replace('%s', invoices.map(function (invoice) {
			return invoice.invoiceNumber;
		}).join(', '));
		var buttons = [{
			title: jse.core.lang.translate('no', 'lightbox_buttons'),
			callback: function callback(event) {
				return $(event.currentTarget).parents('.modal').modal('hide');
			}
		}, {
			title: jse.core.lang.translate('yes', 'lightbox_buttons'),
			callback: function callback() {
				var invoice = invoices.pop();
				var url = 'gm_pdf_order.php?oID=' + invoice.orderId + '&type=invoice' + ('&cancel_invoice_id=' + invoice.invoiceId);

				window.open(url, '_blank');

				var invoiceIds = invoices.map(function (invoice) {
					return invoice.invoiceId;
				});

				$this.DataTable().ajax.reload(function () {
					$this.off('single_checkbox:ready', _onSingleCheckboxReady).on('single_checkbox:ready', { invoiceIds: invoiceIds }, _onSingleCheckboxReady);
				});

				$(this).parents('.modal').modal('hide');
			}
		}];

		jse.libs.modal.showMessage(title, message, buttons);
	}

	/**
  * Bulk Cancellation Invoice Generation 
  * 
  * This method will create cancellation invoices for the selected invoices. 
  */
	function _onBulkCancellationInvoiceClick() {
		var invoices = [];

		$this.find('tbody input:checkbox:checked').each(function () {
			var _$$parents$data2 = $(this).parents('tr').data(),
			    invoiceId = _$$parents$data2.invoiceId,
			    invoiceNumber = _$$parents$data2.invoiceNumber,
			    orderId = _$$parents$data2.orderId,
			    isCancellationInvoice = _$$parents$data2.isCancellationInvoice;

			if (!isCancellationInvoice && orderId > 0) {
				invoices.push({
					invoiceId: invoiceId,
					invoiceNumber: invoiceNumber,
					orderId: orderId
				});
			}
		});

		if (!invoices.length) {
			var _title = jse.core.lang.translate('HEADING_MODAL_CANCELLATION_INVOICE', 'orders');
			var _message = jse.core.lang.translate('NO_RECORDS_ERROR', 'orders');
			jse.libs.modal.showMessage(_title, _message);
			return;
		}

		var title = jse.core.lang.translate('HEADING_MODAL_CANCELLATION_INVOICE', 'orders');
		var message = jse.core.lang.translate('TEXT_MODAL_CANCELLATION_INVOICE', 'orders').replace('%s', invoices.map(function (invoice) {
			return invoice.invoiceNumber;
		}).join(', '));
		var buttons = [{
			title: jse.core.lang.translate('no', 'lightbox_buttons'),
			callback: function callback(event) {
				return $(event.currentTarget).parents('.modal').modal('hide');
			}
		}, {
			title: jse.core.lang.translate('yes', 'lightbox_buttons'),
			callback: function callback() {
				var _$,
				    _this = this;

				// Create new cancellation invoices and refresh the table. 
				var requests = [];

				invoices.forEach(function (invoice) {
					var url = 'gm_pdf_order.php?oID=' + invoice.orderId + '&type=invoice' + ('&cancel_invoice_id=' + invoice.invoiceId + '&ajax=1');

					requests.push($.get(url));
				});

				(_$ = $).when.apply(_$, requests).done(function () {
					for (var _len = arguments.length, responses = Array(_len), _key = 0; _key < _len; _key++) {
						responses[_key] = arguments[_key];
					}

					var cancellationInvoiceIds = [];

					if (requests.length === 1) {
						responses = [responses]; // Always treat the responses as an array.
					}

					responses.forEach(function (response) {
						var _JSON$parse = JSON.parse(response[0]),
						    invoiceId = _JSON$parse.invoiceId;

						cancellationInvoiceIds.push(invoiceId);
					});

					_createBulkPdf(cancellationInvoiceIds);

					var invoiceIds = invoices.map(function (invoice) {
						return invoice.invoiceId;
					});

					$this.DataTable().ajax.reload(function () {
						$this.off('single_checkbox:ready', _onSingleCheckboxReady).on('single_checkbox:ready', { invoiceIds: invoiceIds }, _onSingleCheckboxReady);
					});
					$(_this).parents('.modal').modal('hide');
				});
			}
		}];

		jse.libs.modal.showMessage(title, message, buttons);
	}

	/**
  * On Email Invoice Click
  *
  * Display the email-invoice modal.
  */
	function _onEmailInvoiceClick() {
		var $modal = $('.email-invoice.modal');
		var rowData = $(this).parents('tr').data();
		var url = jse.core.config.get('appUrl') + '/admin/admin.php';
		var data = {
			do: 'InvoicesModalsAjax/GetEmailInvoiceInformation',
			pageToken: jse.core.config.get('pageToken'),
			o: [rowData.orderId]
		};

		$modal.data('invoiceId', rowData.invoiceId).data('orderId', rowData.orderId).modal('show');

		$.ajax({ url: url, data: data, dataType: 'json' }).done(function (response) {
			var dateFormat = jse.core.config.get('languageCode') === 'de' ? 'DD.MM.YY' : 'MM.DD.YY';
			var subject = response.subject.replace('{INVOICE_ID}', rowData.invoiceNumber).replace('{DATE}', moment(rowData.invoiceDate.date).format(dateFormat)).replace('{ORDER_ID}', rowData.orderId);

			$modal.find('.subject').val(subject);

			var customerInfo = '"' + rowData.customerName + '"';

			if (response.emails[rowData.orderId]) {
				customerInfo += ' "' + response.emails[rowData.orderId] + '"';
			}

			$modal.find('.customer-info').text(customerInfo);
			$modal.find('.email-address').val(response.emails[rowData.orderId] || '');
		});
	}

	/**
  * Generate email row markup.
  *
  * This method is used by the bulk email modal for dynamic email-row generation.
  *
  * @param {Object} invoice Contains the required row data.
  * @param {Object} emailInformation Contains the subject and email addresses for the invoices.
  *
  * @return {jQuery} Returns the row selector.
  */
	function _generateMailRowMarkup(invoice, emailInformation) {
		var $row = $('<div/>', { class: 'form-group email-list-item' });
		var $idColumn = $('<div/>', { class: 'col-sm-3' });
		var $dateColumn = $('<div/>', { class: 'col-sm-3' });
		var $emailColumn = $('<div/>', { class: 'col-sm-6' });

		var $idLabel = $('<label/>', {
			class: 'control-label id-label force-text-color-black force-text-normal-weight',
			html: '<a href="request_port.php?module=OrderAdmin&action=showPdf&type=invoice' + ('&invoice_number=' + invoice.invoiceNumber + '&order_id=' + invoice.orderId + '" target="_blank">') + (invoice.invoiceNumber + '</a>')
		});

		var dateFormat = jse.core.config.get('languageCode') === 'de' ? 'DD.MM.YYYY' : 'MM.DD.YYYY';
		var $dateLabel = $('<label/>', {
			class: 'control-label date-label force-text-color-black force-text-normal-weight',
			text: moment(invoice.invoiceDate.date).format(dateFormat)
		});

		var $emailInput = $('<input/>', {
			class: 'form-control email-input',
			type: 'text',
			value: emailInformation.emails[invoice.orderId]
		});

		$idLabel.appendTo($idColumn);
		$dateLabel.appendTo($dateColumn);
		$emailInput.appendTo($emailColumn);

		$row.append([$idColumn, $dateColumn, $emailColumn]);
		$row.data('invoice', invoice);

		return $row;
	}

	/**
  * Bulk Email Invoice Click.
  *
  * Sends emails with the selected invoices.
  *
  * @param {jQuery.Event} event
  */
	function _onBulkEmailInvoiceClick(event) {
		event.preventDefault();

		var $modal = $('.bulk-email-invoice.modal');
		var $mailList = $modal.find('.email-list');
		var selectedInvoices = [];

		$this.find('tbody input:checkbox:checked').each(function (index, checkbox) {
			var rowData = $(checkbox).parents('tr').data();
			selectedInvoices.push(rowData);
		});

		// Sort selected invoices by date (descending). 
		selectedInvoices.sort(function (invoice1, invoice2) {
			return invoice1.invoiceDate - invoice2.invoiceDate;
		}).reverse();

		if (selectedInvoices.length) {
			$mailList.empty();

			var url = jse.core.config.get('appUrl') + '/admin/admin.php';
			var _data = {
				do: 'InvoicesModalsAjax/GetEmailInvoiceInformation',
				pageToken: jse.core.config.get('pageToken'),
				o: [].concat(_toConsumableArray(new Set(selectedInvoices.map(function (invoice) {
					return invoice.orderId;
				})))) // Unique orders number array.
			};

			$.ajax({
				url: url,
				data: _data,
				dataType: 'json'
			}).done(function (response) {
				selectedInvoices.forEach(function (invoice) {
					return $mailList.append(_generateMailRowMarkup(invoice, response));
				});
			});

			$modal.modal('show');
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Bind table row actions.
		$this.on('click', 'tbody tr', _onTableRowClick).on('change', '.bulk-selection', _onBulkSelectionChange).on('change', 'input:checkbox', _onTableRowCheckboxChange).on('click', '.email-invoice', _onEmailInvoiceClick).on('click', '.cancellation-invoice', _onCancellationInvoiceClick);

		// Bind table row and bulk actions. 
		$this.parents('.invoices').on('click', '.order-status, .btn-group .change-status', _onChangeOrderStatusClick).on('click', '.btn-group .delete, .actions .delete', _onDeleteInvoiceClick).on('click', '.btn-group .bulk-download-invoice', _onBulkDownloadInvoiceClick).on('click', '.btn-group .bulk-cancellation-invoice', _onBulkCancellationInvoiceClick).on('click', '.btn-group .bulk-email-invoice', _onBulkEmailInvoiceClick);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL292ZXJ2aWV3L2V2ZW50cy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRzcGlubmVyIiwiX29uQnVsa1NlbGVjdGlvbkNoYW5nZSIsImV2ZW50IiwicHJvcGFnYXRlIiwiZmluZCIsInNpbmdsZV9jaGVja2JveCIsInByb3AiLCJ0cmlnZ2VyIiwiX29uVGFibGVSb3dDbGljayIsInRhcmdldCIsImlzIiwiX29uVGFibGVSb3dDaGVja2JveENoYW5nZSIsImxlbmd0aCIsInBhcmVudHMiLCJyZW1vdmVDbGFzcyIsImFkZENsYXNzIiwiX2dldFNlbGVjdGVkSW52b2ljZXNEYXRhIiwiJHRhcmdldCIsImRhdGFLZXkiLCJzZWxlY3RlZEludm9pY2VzIiwiZWFjaCIsImluZGV4IiwiY2hlY2tib3giLCJwdXNoIiwicm93SWQiLCJfb25EZWxldGVJbnZvaWNlQ2xpY2siLCJwcmV2ZW50RGVmYXVsdCIsInNlbGVjdGVkSW52b2ljZUlkcyIsInNlbGVjdGVkSW52b2ljZU51bWJlcnMiLCIkbW9kYWwiLCJ2YWwiLCJ0ZXh0Iiwiam9pbiIsIm1vZGFsIiwiX29uQ2hhbmdlT3JkZXJTdGF0dXNDbGljayIsImhhc0NsYXNzIiwic3RvcFByb3BhZ2F0aW9uIiwicm93RGF0YSIsInNlbGVjdGVkT3JkZXJzIiwiZmlsdGVyIiwib3JkZXJJZCIsInRpdGxlIiwianNlIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJtZXNzYWdlIiwibGlicyIsInNob3dNZXNzYWdlIiwic3RhdHVzSWQiLCJhdHRyIiwiQXJyYXkiLCJmcm9tIiwiU2V0IiwiX29wZW5CdWxrUGRmVXJsIiwiaW52b2ljZUlkcyIsInBhcmFtZXRlcnMiLCJkbyIsInBhZ2VUb2tlbiIsImNvbmZpZyIsImdldCIsImkiLCJ1cmwiLCJwYXJhbSIsIndpbmRvdyIsIm9wZW4iLCJsb2FkaW5nX3NwaW5uZXIiLCJoaWRlIiwiX2NyZWF0ZUJ1bGtQZGYiLCJ6SW5kZXgiLCJjc3MiLCJzaG93IiwiX29uQnVsa0Rvd25sb2FkSW52b2ljZUNsaWNrIiwibWF4QW1vdW50SW52b2ljZXNCdWxrUGRmIiwiJGludm9pY2VNZXNzYWdlQ29udGFpbmVyIiwib24iLCJfb25TaW5nbGVDaGVja2JveFJlYWR5IiwiZm9yRWFjaCIsImlkIiwiX29uQ2FuY2VsbGF0aW9uSW52b2ljZUNsaWNrIiwiaW52b2ljZXMiLCJpbnZvaWNlSWQiLCJpbnZvaWNlTnVtYmVyIiwiaXNDYW5jZWxsYXRpb25JbnZvaWNlIiwicmVwbGFjZSIsIm1hcCIsImludm9pY2UiLCJidXR0b25zIiwiY2FsbGJhY2siLCJjdXJyZW50VGFyZ2V0IiwicG9wIiwiRGF0YVRhYmxlIiwiYWpheCIsInJlbG9hZCIsIm9mZiIsIl9vbkJ1bGtDYW5jZWxsYXRpb25JbnZvaWNlQ2xpY2siLCJyZXF1ZXN0cyIsIndoZW4iLCJkb25lIiwicmVzcG9uc2VzIiwiY2FuY2VsbGF0aW9uSW52b2ljZUlkcyIsIkpTT04iLCJwYXJzZSIsInJlc3BvbnNlIiwiX29uRW1haWxJbnZvaWNlQ2xpY2siLCJvIiwiZGF0YVR5cGUiLCJkYXRlRm9ybWF0Iiwic3ViamVjdCIsIm1vbWVudCIsImludm9pY2VEYXRlIiwiZGF0ZSIsImZvcm1hdCIsImN1c3RvbWVySW5mbyIsImN1c3RvbWVyTmFtZSIsImVtYWlscyIsIl9nZW5lcmF0ZU1haWxSb3dNYXJrdXAiLCJlbWFpbEluZm9ybWF0aW9uIiwiJHJvdyIsImNsYXNzIiwiJGlkQ29sdW1uIiwiJGRhdGVDb2x1bW4iLCIkZW1haWxDb2x1bW4iLCIkaWRMYWJlbCIsImh0bWwiLCIkZGF0ZUxhYmVsIiwiJGVtYWlsSW5wdXQiLCJ0eXBlIiwidmFsdWUiLCJhcHBlbmRUbyIsImFwcGVuZCIsIl9vbkJ1bGtFbWFpbEludm9pY2VDbGljayIsIiRtYWlsTGlzdCIsInNvcnQiLCJpbnZvaWNlMSIsImludm9pY2UyIiwicmV2ZXJzZSIsImVtcHR5IiwiaW5pdCJdLCJtYXBwaW5ncyI6Ijs7OztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUFzQixRQUF0QixFQUFnQyxDQUFDLGlCQUFELENBQWhDLEVBQXFELFVBQVNDLElBQVQsRUFBZTs7QUFFbkU7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTLEVBQWY7O0FBRUE7Ozs7O0FBS0EsS0FBSUksV0FBVyxJQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUEsVUFBU0Msc0JBQVQsQ0FBZ0NDLEtBQWhDLEVBQXlEO0FBQUEsTUFBbEJDLFNBQWtCLHVFQUFOLElBQU07O0FBQ3hELE1BQUlBLGNBQWMsS0FBbEIsRUFBeUI7QUFDeEIsVUFEd0IsQ0FDaEI7QUFDUjs7QUFFREwsUUFBTU0sSUFBTixDQUFXLHNCQUFYLEVBQW1DQyxlQUFuQyxDQUFtRCxTQUFuRCxFQUE4RE4sRUFBRSxJQUFGLEVBQVFPLElBQVIsQ0FBYSxTQUFiLENBQTlELEVBQXVGQyxPQUF2RixDQUErRixRQUEvRjtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0MsZ0JBQVQsQ0FBMEJOLEtBQTFCLEVBQWlDO0FBQ2hDLE1BQUksQ0FBQ0gsRUFBRUcsTUFBTU8sTUFBUixFQUFnQkMsRUFBaEIsQ0FBbUIsSUFBbkIsQ0FBTCxFQUErQjtBQUM5QjtBQUNBOztBQUVEWCxJQUFFLElBQUYsRUFBUUssSUFBUixDQUFhLGdCQUFiLEVBQ0VFLElBREYsQ0FDTyxTQURQLEVBQ2tCLENBQUNQLEVBQUUsSUFBRixFQUFRSyxJQUFSLENBQWEsZ0JBQWIsRUFBK0JFLElBQS9CLENBQW9DLFNBQXBDLENBRG5CLEVBRUVDLE9BRkYsQ0FFVSxRQUZWO0FBR0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0kseUJBQVQsR0FBcUM7QUFDcEMsTUFBSWIsTUFBTU0sSUFBTixDQUFXLHdCQUFYLEVBQXFDUSxNQUFyQyxHQUE4QyxDQUFsRCxFQUFxRDtBQUNwRGQsU0FBTWUsT0FBTixDQUFjLFdBQWQsRUFBMkJULElBQTNCLENBQWdDLHVCQUFoQyxFQUF5RFUsV0FBekQsQ0FBcUUsVUFBckU7QUFDQSxHQUZELE1BRU87QUFDTmhCLFNBQU1lLE9BQU4sQ0FBYyxXQUFkLEVBQTJCVCxJQUEzQixDQUFnQyx1QkFBaEMsRUFBeURXLFFBQXpELENBQWtFLFVBQWxFO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFVBQVNDLHdCQUFULENBQWtDQyxPQUFsQyxFQUEyQ0MsT0FBM0MsRUFBb0Q7QUFDbkQsTUFBTUMsbUJBQW1CLEVBQXpCOztBQUVBLE1BQUlGLFFBQVFKLE9BQVIsQ0FBZ0IsY0FBaEIsRUFBZ0NELE1BQWhDLEdBQXlDLENBQTdDLEVBQWdEO0FBQy9DO0FBQ0FkLFNBQU1NLElBQU4sQ0FBVyw4QkFBWCxFQUEyQ2dCLElBQTNDLENBQWdELFVBQUNDLEtBQUQsRUFBUUMsUUFBUjtBQUFBLFdBQy9DSCxpQkFBaUJJLElBQWpCLENBQXNCeEIsRUFBRXVCLFFBQUYsRUFBWVQsT0FBWixDQUFvQixJQUFwQixFQUEwQmhCLElBQTFCLENBQStCcUIsT0FBL0IsQ0FBdEIsQ0FEK0M7QUFBQSxJQUFoRDtBQUVBLEdBSkQsTUFJTztBQUNOLE9BQU1NLFFBQVFQLFFBQVFKLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JoQixJQUF0QixDQUEyQnFCLE9BQTNCLENBQWQ7O0FBRUEsT0FBSSxDQUFDTSxLQUFMLEVBQVk7QUFDWCxXQURXLENBQ0g7QUFDUjs7QUFFREwsb0JBQWlCSSxJQUFqQixDQUFzQkMsS0FBdEI7QUFDQTs7QUFFRCxTQUFPTCxnQkFBUDtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU00scUJBQVQsQ0FBK0J2QixLQUEvQixFQUFzQztBQUNyQ0EsUUFBTXdCLGNBQU47O0FBRUEsTUFBTUMscUJBQXFCWCx5QkFBeUJqQixFQUFFLElBQUYsQ0FBekIsRUFBa0MsV0FBbEMsQ0FBM0I7QUFDQSxNQUFNNkIseUJBQXlCWix5QkFBeUJqQixFQUFFLElBQUYsQ0FBekIsRUFBa0MsZUFBbEMsQ0FBL0I7O0FBRUE7QUFDQSxNQUFNOEIsU0FBUzlCLEVBQUUsZUFBRixDQUFmO0FBQ0E4QixTQUFPekIsSUFBUCxDQUFZLHVCQUFaLEVBQXFDMEIsR0FBckMsQ0FBeUNILGtCQUF6QztBQUNBRSxTQUFPekIsSUFBUCxDQUFZLDJCQUFaLEVBQXlDMkIsSUFBekMsQ0FBOENILHVCQUF1QkksSUFBdkIsQ0FBNEIsSUFBNUIsQ0FBOUM7QUFDQUgsU0FBT0ksS0FBUCxDQUFhLE1BQWI7QUFDQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNDLHlCQUFULENBQW1DaEMsS0FBbkMsRUFBMEM7QUFDekMsTUFBSUgsRUFBRUcsTUFBTU8sTUFBUixFQUFnQjBCLFFBQWhCLENBQXlCLGNBQXpCLENBQUosRUFBOEM7QUFDN0NqQyxTQUFNa0MsZUFBTjtBQUNBOztBQUVELE1BQU1QLFNBQVM5QixFQUFFLGVBQUYsQ0FBZjtBQUNBLE1BQU1zQyxVQUFVdEMsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JoQixJQUF0QixFQUFoQjtBQUNBLE1BQU15QyxpQkFBaUJ0Qix5QkFBeUJqQixFQUFFLElBQUYsQ0FBekIsRUFBa0MsU0FBbEMsRUFBNkN3QyxNQUE3QyxDQUFvRDtBQUFBLFVBQVdDLFlBQVksQ0FBdkI7QUFBQSxHQUFwRCxDQUF2Qjs7QUFFQSxNQUFJLENBQUNGLGVBQWUxQixNQUFwQixFQUE0QjtBQUMzQixPQUFNNkIsUUFBUUMsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUJBQXhCLEVBQTZDLFFBQTdDLENBQWQ7QUFDQSxPQUFNQyxVQUFVSixJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQkFBeEIsRUFBNEMsUUFBNUMsQ0FBaEI7QUFDQUgsT0FBSUssSUFBSixDQUFTZCxLQUFULENBQWVlLFdBQWYsQ0FBMkJQLEtBQTNCLEVBQWtDSyxPQUFsQztBQUNBO0FBQ0E7O0FBRURqQixTQUFPekIsSUFBUCxDQUFZLGtCQUFaLEVBQWdDMEIsR0FBaEMsQ0FBcUNPLE9BQUQsR0FBWUEsUUFBUVksUUFBcEIsR0FBK0IsRUFBbkU7QUFDQXBCLFNBQU96QixJQUFQLENBQVksVUFBWixFQUF3QjBCLEdBQXhCLENBQTRCLEVBQTVCO0FBQ0FELFNBQU96QixJQUFQLENBQVksNkRBQVosRUFDRThDLElBREYsQ0FDTyxTQURQLEVBQ2tCLEtBRGxCLEVBRUVyQyxPQUZGLENBRVUsa0JBRlYsRUFHRUMsV0FIRixDQUdjLFNBSGQ7O0FBS0E7QUFDQWUsU0FBT3pCLElBQVAsQ0FBWSxrQkFBWixFQUFnQzJCLElBQWhDLENBQXFDb0IsTUFBTUMsSUFBTixDQUFXLElBQUlDLEdBQUosQ0FBUWYsY0FBUixDQUFYLEVBQW9DTixJQUFwQyxDQUF5QyxJQUF6QyxDQUFyQztBQUNBSCxTQUFPSSxLQUFQLENBQWEsTUFBYjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNxQixlQUFULENBQXlCQyxVQUF6QixFQUFxQztBQUNwQyxNQUFNQyxhQUFhO0FBQ2xCQyxPQUFJLG9DQURjO0FBRWxCQyxjQUFXaEIsSUFBSUMsSUFBSixDQUFTZ0IsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FGTztBQUdsQkMsTUFBR047QUFIZSxHQUFuQjs7QUFNQSxNQUFNTyxNQUFNcEIsSUFBSUMsSUFBSixDQUFTZ0IsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsbUJBQWhDLEdBQXNEN0QsRUFBRWdFLEtBQUYsQ0FBUVAsVUFBUixDQUFsRTs7QUFFQVEsU0FBT0MsSUFBUCxDQUFZSCxHQUFaLEVBQWlCLFNBQWpCOztBQUVBcEIsTUFBSUssSUFBSixDQUFTbUIsZUFBVCxDQUF5QkMsSUFBekIsQ0FBOEJuRSxRQUE5QjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNvRSxjQUFULENBQXdCYixVQUF4QixFQUFvQztBQUNuQyxNQUFNYyxTQUFTdEUsRUFBRSxpQ0FBRixFQUFxQ3VFLEdBQXJDLENBQXlDLFNBQXpDLENBQWYsQ0FEbUMsQ0FDaUM7QUFDcEV0RSxhQUFXMEMsSUFBSUssSUFBSixDQUFTbUIsZUFBVCxDQUF5QkssSUFBekIsQ0FBOEJ6RSxLQUE5QixFQUFxQ3VFLE1BQXJDLENBQVg7QUFDQWYsa0JBQWdCQyxVQUFoQjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNpQiwyQkFBVCxHQUF1QztBQUN0QyxNQUFNakIsYUFBYXZDLHlCQUF5QmpCLEVBQUUsSUFBRixDQUF6QixFQUFrQyxXQUFsQyxDQUFuQjs7QUFFQSxNQUFJd0QsV0FBVzNDLE1BQVgsR0FBb0JmLEtBQUs0RSx3QkFBN0IsRUFBdUQ7QUFDdEQsT0FBTTVDLFNBQVM5QixFQUFFLG1CQUFGLENBQWY7QUFDQThCLFVBQU9JLEtBQVAsQ0FBYSxNQUFiOztBQUVBLE9BQU15QywyQkFBMkI3QyxPQUFPekIsSUFBUCxDQUFZLG1CQUFaLENBQWpDO0FBQ0FzRSw0QkFBeUI1RCxXQUF6QixDQUFxQyxRQUFyQztBQUNBZSxVQUFPOEMsRUFBUCxDQUFVLGVBQVYsRUFBMkI7QUFBQSxXQUFNRCx5QkFBeUIzRCxRQUF6QixDQUFrQyxRQUFsQyxDQUFOO0FBQUEsSUFBM0I7O0FBRUE7QUFDQTs7QUFFRHFELGlCQUFlYixVQUFmO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTcUIsc0JBQVQsQ0FBZ0MxRSxLQUFoQyxFQUF1QztBQUN0Q0EsUUFBTUwsSUFBTixDQUFXMEQsVUFBWCxDQUFzQnNCLE9BQXRCLENBQThCLGNBQU07QUFDbkMvRSxTQUFNTSxJQUFOLFNBQWlCMEUsRUFBakIsc0JBQXNDekUsZUFBdEMsQ0FBc0QsU0FBdEQsRUFBaUUsSUFBakUsRUFBdUVFLE9BQXZFLENBQStFLFFBQS9FO0FBQ0EsR0FGRDs7QUFJQTtBQUNBLE1BQUlSLEVBQUUsMkJBQUYsRUFBK0JhLE1BQW5DLEVBQTJDO0FBQzFDYixLQUFFLGNBQUYsRUFBa0JLLElBQWxCLENBQXVCLFFBQXZCLEVBQWlDVSxXQUFqQyxDQUE2QyxVQUE3QztBQUNBO0FBQ0Q7O0FBRUQ7Ozs7O0FBS0EsVUFBU2lFLDJCQUFULEdBQXVDO0FBQ3RDLE1BQU1DLFdBQVcsRUFBakI7O0FBRHNDLHdCQUc2QmpGLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLEVBQXNCaEIsSUFBdEIsRUFIN0I7QUFBQSxNQUcvQm9GLFNBSCtCLG1CQUcvQkEsU0FIK0I7QUFBQSxNQUdwQkMsYUFIb0IsbUJBR3BCQSxhQUhvQjtBQUFBLE1BR0wxQyxPQUhLLG1CQUdMQSxPQUhLO0FBQUEsTUFHSTJDLHFCQUhKLG1CQUdJQSxxQkFISjs7QUFLdEMsTUFBSUEseUJBQXlCM0MsWUFBWSxDQUF6QyxFQUE0QztBQUMzQztBQUNBOztBQUVEd0MsV0FBU3pELElBQVQsQ0FBYztBQUNiMEQsdUJBRGE7QUFFYkMsK0JBRmE7QUFHYjFDO0FBSGEsR0FBZDs7QUFNQSxNQUFNQyxRQUFRQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixvQ0FBeEIsRUFBOEQsUUFBOUQsQ0FBZDtBQUNBLE1BQU1DLFVBQVVKLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGlDQUF4QixFQUEyRCxRQUEzRCxFQUNkdUMsT0FEYyxDQUNOLElBRE0sRUFDQUosU0FBU0ssR0FBVCxDQUFhO0FBQUEsVUFBV0MsUUFBUUosYUFBbkI7QUFBQSxHQUFiLEVBQStDbEQsSUFBL0MsQ0FBb0QsSUFBcEQsQ0FEQSxDQUFoQjtBQUVBLE1BQU11RCxVQUFVLENBQ2Y7QUFDQzlDLFVBQU9DLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLElBQXhCLEVBQThCLGtCQUE5QixDQURSO0FBRUMyQyxhQUFVO0FBQUEsV0FBU3pGLEVBQUVHLE1BQU11RixhQUFSLEVBQXVCNUUsT0FBdkIsQ0FBK0IsUUFBL0IsRUFBeUNvQixLQUF6QyxDQUErQyxNQUEvQyxDQUFUO0FBQUE7QUFGWCxHQURlLEVBS2Y7QUFDQ1EsVUFBT0MsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsS0FBeEIsRUFBK0Isa0JBQS9CLENBRFI7QUFFQzJDLFdBRkQsc0JBRVk7QUFDVixRQUFNRixVQUFVTixTQUFTVSxHQUFULEVBQWhCO0FBQ0EsUUFBTTVCLE1BQU0sMEJBQXdCd0IsUUFBUTlDLE9BQWhDLDhDQUNhOEMsUUFBUUwsU0FEckIsQ0FBWjs7QUFHQWpCLFdBQU9DLElBQVAsQ0FBWUgsR0FBWixFQUFpQixRQUFqQjs7QUFFQSxRQUFNUCxhQUFheUIsU0FBU0ssR0FBVCxDQUFhO0FBQUEsWUFBV0MsUUFBUUwsU0FBbkI7QUFBQSxLQUFiLENBQW5COztBQUVBbkYsVUFBTTZGLFNBQU4sR0FBa0JDLElBQWxCLENBQXVCQyxNQUF2QixDQUE4QixZQUFNO0FBQ25DL0YsV0FDRWdHLEdBREYsQ0FDTSx1QkFETixFQUMrQmxCLHNCQUQvQixFQUVFRCxFQUZGLENBRUssdUJBRkwsRUFFOEIsRUFBQ3BCLHNCQUFELEVBRjlCLEVBRTRDcUIsc0JBRjVDO0FBR0EsS0FKRDs7QUFNQTdFLE1BQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLFFBQWhCLEVBQTBCb0IsS0FBMUIsQ0FBZ0MsTUFBaEM7QUFDQTtBQWxCRixHQUxlLENBQWhCOztBQTJCQVMsTUFBSUssSUFBSixDQUFTZCxLQUFULENBQWVlLFdBQWYsQ0FBMkJQLEtBQTNCLEVBQWtDSyxPQUFsQyxFQUEyQ3lDLE9BQTNDO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU1EsK0JBQVQsR0FBMkM7QUFDMUMsTUFBTWYsV0FBVyxFQUFqQjs7QUFFQWxGLFFBQU1NLElBQU4sQ0FBVyw4QkFBWCxFQUEyQ2dCLElBQTNDLENBQWdELFlBQVc7QUFBQSwwQkFDU3JCLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLEVBQXNCaEIsSUFBdEIsRUFEVDtBQUFBLE9BQ25Eb0YsU0FEbUQsb0JBQ25EQSxTQURtRDtBQUFBLE9BQ3hDQyxhQUR3QyxvQkFDeENBLGFBRHdDO0FBQUEsT0FDekIxQyxPQUR5QixvQkFDekJBLE9BRHlCO0FBQUEsT0FDaEIyQyxxQkFEZ0Isb0JBQ2hCQSxxQkFEZ0I7O0FBRzFELE9BQUksQ0FBQ0EscUJBQUQsSUFBMEIzQyxVQUFVLENBQXhDLEVBQTJDO0FBQzFDd0MsYUFBU3pELElBQVQsQ0FBYztBQUNiMEQseUJBRGE7QUFFYkMsaUNBRmE7QUFHYjFDO0FBSGEsS0FBZDtBQUtBO0FBQ0QsR0FWRDs7QUFZQSxNQUFJLENBQUN3QyxTQUFTcEUsTUFBZCxFQUFzQjtBQUNyQixPQUFNNkIsU0FBUUMsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isb0NBQXhCLEVBQThELFFBQTlELENBQWQ7QUFDQSxPQUFNQyxXQUFVSixJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQkFBeEIsRUFBNEMsUUFBNUMsQ0FBaEI7QUFDQUgsT0FBSUssSUFBSixDQUFTZCxLQUFULENBQWVlLFdBQWYsQ0FBMkJQLE1BQTNCLEVBQWtDSyxRQUFsQztBQUNBO0FBQ0E7O0FBRUQsTUFBTUwsUUFBUUMsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isb0NBQXhCLEVBQThELFFBQTlELENBQWQ7QUFDQSxNQUFNQyxVQUFVSixJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixpQ0FBeEIsRUFBMkQsUUFBM0QsRUFDZHVDLE9BRGMsQ0FDTixJQURNLEVBQ0FKLFNBQVNLLEdBQVQsQ0FBYTtBQUFBLFVBQVdDLFFBQVFKLGFBQW5CO0FBQUEsR0FBYixFQUErQ2xELElBQS9DLENBQW9ELElBQXBELENBREEsQ0FBaEI7QUFFQSxNQUFNdUQsVUFBVSxDQUNmO0FBQ0M5QyxVQUFPQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixJQUF4QixFQUE4QixrQkFBOUIsQ0FEUjtBQUVDMkMsYUFBVTtBQUFBLFdBQVN6RixFQUFFRyxNQUFNdUYsYUFBUixFQUF1QjVFLE9BQXZCLENBQStCLFFBQS9CLEVBQXlDb0IsS0FBekMsQ0FBK0MsTUFBL0MsQ0FBVDtBQUFBO0FBRlgsR0FEZSxFQUtmO0FBQ0NRLFVBQU9DLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLEtBQXhCLEVBQStCLGtCQUEvQixDQURSO0FBRUMyQyxXQUZELHNCQUVZO0FBQUE7QUFBQTs7QUFDVjtBQUNBLFFBQU1RLFdBQVcsRUFBakI7O0FBRUFoQixhQUFTSCxPQUFULENBQWlCLG1CQUFXO0FBQzNCLFNBQU1mLE1BQU0sMEJBQXdCd0IsUUFBUTlDLE9BQWhDLDhDQUNhOEMsUUFBUUwsU0FEckIsYUFBWjs7QUFHQWUsY0FBU3pFLElBQVQsQ0FBY3hCLEVBQUU2RCxHQUFGLENBQU1FLEdBQU4sQ0FBZDtBQUNBLEtBTEQ7O0FBT0EsYUFBRW1DLElBQUYsV0FBVUQsUUFBVixFQUFvQkUsSUFBcEIsQ0FBeUIsWUFBa0I7QUFBQSx1Q0FBZEMsU0FBYztBQUFkQSxlQUFjO0FBQUE7O0FBQzFDLFNBQU1DLHlCQUF5QixFQUEvQjs7QUFFQSxTQUFJSixTQUFTcEYsTUFBVCxLQUFvQixDQUF4QixFQUEyQjtBQUMxQnVGLGtCQUFhLENBQUNBLFNBQUQsQ0FBYixDQUQwQixDQUNBO0FBQzFCOztBQUVEQSxlQUFVdEIsT0FBVixDQUFrQixvQkFBWTtBQUFBLHdCQUNUd0IsS0FBS0MsS0FBTCxDQUFXQyxTQUFTLENBQVQsQ0FBWCxDQURTO0FBQUEsVUFDdEJ0QixTQURzQixlQUN0QkEsU0FEc0I7O0FBRTdCbUIsNkJBQXVCN0UsSUFBdkIsQ0FBNEIwRCxTQUE1QjtBQUNBLE1BSEQ7O0FBS0FiLG9CQUFlZ0Msc0JBQWY7O0FBRUEsU0FBTTdDLGFBQWF5QixTQUFTSyxHQUFULENBQWE7QUFBQSxhQUFXQyxRQUFRTCxTQUFuQjtBQUFBLE1BQWIsQ0FBbkI7O0FBRUFuRixXQUFNNkYsU0FBTixHQUFrQkMsSUFBbEIsQ0FBdUJDLE1BQXZCLENBQThCLFlBQU07QUFDbkMvRixZQUNFZ0csR0FERixDQUNNLHVCQUROLEVBQytCbEIsc0JBRC9CLEVBRUVELEVBRkYsQ0FFSyx1QkFGTCxFQUU4QixFQUFDcEIsc0JBQUQsRUFGOUIsRUFFNENxQixzQkFGNUM7QUFHQSxNQUpEO0FBS0E3RSxPQUFFLEtBQUYsRUFBUWMsT0FBUixDQUFnQixRQUFoQixFQUEwQm9CLEtBQTFCLENBQWdDLE1BQWhDO0FBQ0EsS0F0QkQ7QUF1QkE7QUFwQ0YsR0FMZSxDQUFoQjs7QUE2Q0FTLE1BQUlLLElBQUosQ0FBU2QsS0FBVCxDQUFlZSxXQUFmLENBQTJCUCxLQUEzQixFQUFrQ0ssT0FBbEMsRUFBMkN5QyxPQUEzQztBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNpQixvQkFBVCxHQUFnQztBQUMvQixNQUFNM0UsU0FBUzlCLEVBQUUsc0JBQUYsQ0FBZjtBQUNBLE1BQU1zQyxVQUFVdEMsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JoQixJQUF0QixFQUFoQjtBQUNBLE1BQU1pRSxNQUFNcEIsSUFBSUMsSUFBSixDQUFTZ0IsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0Msa0JBQTVDO0FBQ0EsTUFBTS9ELE9BQU87QUFDWjRELE9BQUksK0NBRFE7QUFFWkMsY0FBV2hCLElBQUlDLElBQUosQ0FBU2dCLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFdBQXBCLENBRkM7QUFHWjZDLE1BQUcsQ0FBQ3BFLFFBQVFHLE9BQVQ7QUFIUyxHQUFiOztBQU1BWCxTQUNFaEMsSUFERixDQUNPLFdBRFAsRUFDb0J3QyxRQUFRNEMsU0FENUIsRUFFRXBGLElBRkYsQ0FFTyxTQUZQLEVBRWtCd0MsUUFBUUcsT0FGMUIsRUFHRVAsS0FIRixDQUdRLE1BSFI7O0FBS0FsQyxJQUFFNkYsSUFBRixDQUFPLEVBQUM5QixRQUFELEVBQU1qRSxVQUFOLEVBQVk2RyxVQUFVLE1BQXRCLEVBQVAsRUFBc0NSLElBQXRDLENBQTJDLG9CQUFZO0FBQ3RELE9BQU1TLGFBQWFqRSxJQUFJQyxJQUFKLENBQVNnQixNQUFULENBQWdCQyxHQUFoQixDQUFvQixjQUFwQixNQUF3QyxJQUF4QyxHQUErQyxVQUEvQyxHQUE0RCxVQUEvRTtBQUNBLE9BQU1nRCxVQUFVTCxTQUFTSyxPQUFULENBQ2R4QixPQURjLENBQ04sY0FETSxFQUNVL0MsUUFBUTZDLGFBRGxCLEVBRWRFLE9BRmMsQ0FFTixRQUZNLEVBRUl5QixPQUFPeEUsUUFBUXlFLFdBQVIsQ0FBb0JDLElBQTNCLEVBQWlDQyxNQUFqQyxDQUF3Q0wsVUFBeEMsQ0FGSixFQUdkdkIsT0FIYyxDQUdOLFlBSE0sRUFHUS9DLFFBQVFHLE9BSGhCLENBQWhCOztBQUtBWCxVQUFPekIsSUFBUCxDQUFZLFVBQVosRUFBd0IwQixHQUF4QixDQUE0QjhFLE9BQTVCOztBQUVBLE9BQUlLLHFCQUFtQjVFLFFBQVE2RSxZQUEzQixNQUFKOztBQUVBLE9BQUlYLFNBQVNZLE1BQVQsQ0FBZ0I5RSxRQUFRRyxPQUF4QixDQUFKLEVBQXNDO0FBQ3JDeUUsMkJBQXFCVixTQUFTWSxNQUFULENBQWdCOUUsUUFBUUcsT0FBeEIsQ0FBckI7QUFDQTs7QUFFRFgsVUFBT3pCLElBQVAsQ0FBWSxnQkFBWixFQUE4QjJCLElBQTlCLENBQW1Da0YsWUFBbkM7QUFDQXBGLFVBQU96QixJQUFQLENBQVksZ0JBQVosRUFBOEIwQixHQUE5QixDQUFrQ3lFLFNBQVNZLE1BQVQsQ0FBZ0I5RSxRQUFRRyxPQUF4QixLQUFvQyxFQUF0RTtBQUNBLEdBakJEO0FBa0JBOztBQUVEOzs7Ozs7Ozs7O0FBVUEsVUFBUzRFLHNCQUFULENBQWdDOUIsT0FBaEMsRUFBeUMrQixnQkFBekMsRUFBMkQ7QUFDMUQsTUFBTUMsT0FBT3ZILEVBQUUsUUFBRixFQUFZLEVBQUN3SCxPQUFPLDRCQUFSLEVBQVosQ0FBYjtBQUNBLE1BQU1DLFlBQVl6SCxFQUFFLFFBQUYsRUFBWSxFQUFDd0gsT0FBTyxVQUFSLEVBQVosQ0FBbEI7QUFDQSxNQUFNRSxjQUFjMUgsRUFBRSxRQUFGLEVBQVksRUFBQ3dILE9BQU8sVUFBUixFQUFaLENBQXBCO0FBQ0EsTUFBTUcsZUFBZTNILEVBQUUsUUFBRixFQUFZLEVBQUN3SCxPQUFPLFVBQVIsRUFBWixDQUFyQjs7QUFFQSxNQUFNSSxXQUFXNUgsRUFBRSxVQUFGLEVBQWM7QUFDOUJ3SCxVQUFPLHdFQUR1QjtBQUU5QkssU0FBTSxrR0FDZXRDLFFBQVFKLGFBRHZCLGtCQUNpREksUUFBUTlDLE9BRHpELDRCQUVGOEMsUUFBUUosYUFGTjtBQUZ3QixHQUFkLENBQWpCOztBQU9BLE1BQU15QixhQUFhakUsSUFBSUMsSUFBSixDQUFTZ0IsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsY0FBcEIsTUFBd0MsSUFBeEMsR0FBK0MsWUFBL0MsR0FBOEQsWUFBakY7QUFDQSxNQUFNaUUsYUFBYTlILEVBQUUsVUFBRixFQUFjO0FBQ2hDd0gsVUFBTywwRUFEeUI7QUFFaEN4RixTQUFNOEUsT0FBT3ZCLFFBQVF3QixXQUFSLENBQW9CQyxJQUEzQixFQUFpQ0MsTUFBakMsQ0FBd0NMLFVBQXhDO0FBRjBCLEdBQWQsQ0FBbkI7O0FBS0EsTUFBTW1CLGNBQWMvSCxFQUFFLFVBQUYsRUFBYztBQUNqQ3dILFVBQU8sMEJBRDBCO0FBRWpDUSxTQUFNLE1BRjJCO0FBR2pDQyxVQUFPWCxpQkFBaUJGLE1BQWpCLENBQXdCN0IsUUFBUTlDLE9BQWhDO0FBSDBCLEdBQWQsQ0FBcEI7O0FBTUFtRixXQUFTTSxRQUFULENBQWtCVCxTQUFsQjtBQUNBSyxhQUFXSSxRQUFYLENBQW9CUixXQUFwQjtBQUNBSyxjQUFZRyxRQUFaLENBQXFCUCxZQUFyQjs7QUFFQUosT0FBS1ksTUFBTCxDQUFZLENBQUNWLFNBQUQsRUFBWUMsV0FBWixFQUF5QkMsWUFBekIsQ0FBWjtBQUNBSixPQUFLekgsSUFBTCxDQUFVLFNBQVYsRUFBcUJ5RixPQUFyQjs7QUFFQSxTQUFPZ0MsSUFBUDtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU2Esd0JBQVQsQ0FBa0NqSSxLQUFsQyxFQUF5QztBQUN4Q0EsUUFBTXdCLGNBQU47O0FBRUEsTUFBTUcsU0FBUzlCLEVBQUUsMkJBQUYsQ0FBZjtBQUNBLE1BQU1xSSxZQUFZdkcsT0FBT3pCLElBQVAsQ0FBWSxhQUFaLENBQWxCO0FBQ0EsTUFBTWUsbUJBQW1CLEVBQXpCOztBQUVBckIsUUFBTU0sSUFBTixDQUFXLDhCQUFYLEVBQTJDZ0IsSUFBM0MsQ0FBZ0QsVUFBQ0MsS0FBRCxFQUFRQyxRQUFSLEVBQXFCO0FBQ3BFLE9BQU1lLFVBQVV0QyxFQUFFdUIsUUFBRixFQUFZVCxPQUFaLENBQW9CLElBQXBCLEVBQTBCaEIsSUFBMUIsRUFBaEI7QUFDQXNCLG9CQUFpQkksSUFBakIsQ0FBc0JjLE9BQXRCO0FBQ0EsR0FIRDs7QUFLQTtBQUNBbEIsbUJBQWlCa0gsSUFBakIsQ0FBc0IsVUFBQ0MsUUFBRCxFQUFXQyxRQUFYO0FBQUEsVUFBd0JELFNBQVN4QixXQUFULEdBQXVCeUIsU0FBU3pCLFdBQXhEO0FBQUEsR0FBdEIsRUFBMkYwQixPQUEzRjs7QUFFQSxNQUFJckgsaUJBQWlCUCxNQUFyQixFQUE2QjtBQUM1QndILGFBQVVLLEtBQVY7O0FBRUEsT0FBTTNFLE1BQU1wQixJQUFJQyxJQUFKLENBQVNnQixNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxrQkFBNUM7QUFDQSxPQUFNL0QsUUFBTztBQUNaNEQsUUFBSSwrQ0FEUTtBQUVaQyxlQUFXaEIsSUFBSUMsSUFBSixDQUFTZ0IsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FGQztBQUdaNkMsb0NBQU8sSUFBSXBELEdBQUosQ0FBUWxDLGlCQUFpQmtFLEdBQWpCLENBQXFCO0FBQUEsWUFBV0MsUUFBUTlDLE9BQW5CO0FBQUEsS0FBckIsQ0FBUixDQUFQLEVBSFksQ0FHc0Q7QUFIdEQsSUFBYjs7QUFNQXpDLEtBQUU2RixJQUFGLENBQU87QUFDTjlCLFlBRE07QUFFTmpFLGVBRk07QUFHTjZHLGNBQVU7QUFISixJQUFQLEVBS0VSLElBTEYsQ0FLTyxvQkFBWTtBQUNqQi9FLHFCQUFpQjBELE9BQWpCLENBQXlCO0FBQUEsWUFBV3VELFVBQVVGLE1BQVYsQ0FBaUJkLHVCQUF1QjlCLE9BQXZCLEVBQWdDaUIsUUFBaEMsQ0FBakIsQ0FBWDtBQUFBLEtBQXpCO0FBQ0EsSUFQRjs7QUFTQTFFLFVBQU9JLEtBQVAsQ0FBYSxNQUFiO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O0FBRUFyQyxRQUFPOEksSUFBUCxHQUFjLFVBQVN4QyxJQUFULEVBQWU7QUFDNUI7QUFDQXBHLFFBQ0U2RSxFQURGLENBQ0ssT0FETCxFQUNjLFVBRGQsRUFDMEJuRSxnQkFEMUIsRUFFRW1FLEVBRkYsQ0FFSyxRQUZMLEVBRWUsaUJBRmYsRUFFa0MxRSxzQkFGbEMsRUFHRTBFLEVBSEYsQ0FHSyxRQUhMLEVBR2UsZ0JBSGYsRUFHaUNoRSx5QkFIakMsRUFJRWdFLEVBSkYsQ0FJSyxPQUpMLEVBSWMsZ0JBSmQsRUFJZ0M2QixvQkFKaEMsRUFLRTdCLEVBTEYsQ0FLSyxPQUxMLEVBS2MsdUJBTGQsRUFLdUNJLDJCQUx2Qzs7QUFPQTtBQUNBakYsUUFBTWUsT0FBTixDQUFjLFdBQWQsRUFDRThELEVBREYsQ0FDSyxPQURMLEVBQ2MsMENBRGQsRUFDMER6Qyx5QkFEMUQsRUFFRXlDLEVBRkYsQ0FFSyxPQUZMLEVBRWMsc0NBRmQsRUFFc0RsRCxxQkFGdEQsRUFHRWtELEVBSEYsQ0FHSyxPQUhMLEVBR2MsbUNBSGQsRUFHbURILDJCQUhuRCxFQUlFRyxFQUpGLENBSUssT0FKTCxFQUljLHVDQUpkLEVBSXVEb0IsK0JBSnZELEVBS0VwQixFQUxGLENBS0ssT0FMTCxFQUtjLGdDQUxkLEVBS2dEd0Qsd0JBTGhEOztBQU9BakM7QUFDQSxFQWxCRDs7QUFvQkEsUUFBT3RHLE1BQVA7QUFDQSxDQW5nQkQiLCJmaWxlIjoiaW52b2ljZXMvb3ZlcnZpZXcvZXZlbnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGV2ZW50cy5qcyAyMDE4LTA2LTEyXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIE1haW4gVGFibGUgRXZlbnRzXHJcbiAqXHJcbiAqIEhhbmRsZXMgdGhlIGV2ZW50cyBvZiB0aGUgbWFpbiBpbnZvaWNlcyB0YWJsZS5cclxuICovXHJcbmd4LmNvbnRyb2xsZXJzLm1vZHVsZSgnZXZlbnRzJywgWydsb2FkaW5nX3NwaW5uZXInXSwgZnVuY3Rpb24oZGF0YSkge1xyXG5cdFxyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBWQVJJQUJMRVNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdCAqXHJcblx0ICogQHR5cGUge09iamVjdH1cclxuXHQgKi9cclxuXHRjb25zdCBtb2R1bGUgPSB7fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBMb2FkaW5nIHNwaW5uZXIgaW5zdGFuY2UuXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fG51bGx9XHJcblx0ICovXHJcblx0bGV0ICRzcGlubmVyID0gbnVsbDtcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBGVU5DVElPTlNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBCdWxrIFNlbGVjdGlvbiBDaGFuZ2VcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0LlxyXG5cdCAqIEBwYXJhbSB7Qm9vbGVhbn0gcHJvcGFnYXRlIFdoZXRoZXIgdG8gYWZmZWN0IHRoZSBib2R5IGVsZW1lbnRzLiBXZSBkbyBub3QgbmVlZCB0aGlzIG9uIFwiZHJhdy5kdFwiIGV2ZW50LlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vbkJ1bGtTZWxlY3Rpb25DaGFuZ2UoZXZlbnQsIHByb3BhZ2F0ZSA9IHRydWUpIHtcclxuXHRcdGlmIChwcm9wYWdhdGUgPT09IGZhbHNlKSB7XHJcblx0XHRcdHJldHVybjsgLy8gRG8gbm90IHByb3BhZ2F0ZSBvbiBkcmF3IGV2ZW50IGJlY2F1c2UgdGhlIGJvZHkgY2hlY2tib3hlcyBhcmUgdW5jaGVja2VkIGJ5IGRlZmF1bHQuXHJcblx0XHR9XHJcblx0XHRcclxuXHRcdCR0aGlzLmZpbmQoJ3Rib2R5IGlucHV0OmNoZWNrYm94Jykuc2luZ2xlX2NoZWNrYm94KCdjaGVja2VkJywgJCh0aGlzKS5wcm9wKCdjaGVja2VkJykpLnRyaWdnZXIoJ2NoYW5nZScpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBUYWJsZSBSb3cgQ2xpY2tcclxuXHQgKlxyXG5cdCAqIFdoZW4gYSByb3cgaXMgY2xpY2tlZCB0aGVuIHRoZSByb3ctY2hlY2tib3ggbXVzdCBiZSB0b2dnbGVkLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uVGFibGVSb3dDbGljayhldmVudCkge1xyXG5cdFx0aWYgKCEkKGV2ZW50LnRhcmdldCkuaXMoJ3RkJykpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQkKHRoaXMpLmZpbmQoJ2lucHV0OmNoZWNrYm94JylcclxuXHRcdFx0LnByb3AoJ2NoZWNrZWQnLCAhJCh0aGlzKS5maW5kKCdpbnB1dDpjaGVja2JveCcpLnByb3AoJ2NoZWNrZWQnKSlcclxuXHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBUYWJsZSBSb3cgQ2hlY2tib3ggQ2hhbmdlXHJcblx0ICpcclxuXHQgKiBBZGp1c3QgdGhlIGJ1bGsgYWN0aW9ucyBzdGF0ZSB3aGVuZXZlciB0aGVyZSBhcmUgY2hhbmdlcyBpbiB0aGUgdGFibGUgY2hlY2tib3hlcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25UYWJsZVJvd0NoZWNrYm94Q2hhbmdlKCkge1xyXG5cdFx0aWYgKCR0aGlzLmZpbmQoJ2lucHV0OmNoZWNrYm94OmNoZWNrZWQnKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdCR0aGlzLnBhcmVudHMoJy5pbnZvaWNlcycpLmZpbmQoJy5idWxrLWFjdGlvbiA+IGJ1dHRvbicpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JHRoaXMucGFyZW50cygnLmludm9pY2VzJykuZmluZCgnLmJ1bGstYWN0aW9uID4gYnV0dG9uJykuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENvbGxlY3RzIHRoZSByZXF1aXJlZCBkYXRhIG9mIHRoZSBzZWxlY3RlZCBpbnZvaWNlcyBpbiBhbiBhcnJheS5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGFyZ2V0IFRhcmdldCBlbGVtZW50IHdoaWNoIHRyaWdnZXJlZCB0aGUgZGF0YSBjb2xsZWN0aW9uLlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7KltdfSBSZXR1cm5zIGFuIGFycmF5IG9mIHRoZSByZXF1ZXN0ZWQgaW52b2ljZSBkYXRhLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9nZXRTZWxlY3RlZEludm9pY2VzRGF0YSgkdGFyZ2V0LCBkYXRhS2V5KSB7XHJcblx0XHRjb25zdCBzZWxlY3RlZEludm9pY2VzID0gW107XHJcblx0XHRcclxuXHRcdGlmICgkdGFyZ2V0LnBhcmVudHMoJy5idWxrLWFjdGlvbicpLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0Ly8gRmV0Y2ggdGhlIHNlbGVjdGVkIG9yZGVyIElEcy5cclxuXHRcdFx0JHRoaXMuZmluZCgndGJvZHkgaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZCcpLmVhY2goKGluZGV4LCBjaGVja2JveCkgPT5cclxuXHRcdFx0XHRzZWxlY3RlZEludm9pY2VzLnB1c2goJChjaGVja2JveCkucGFyZW50cygndHInKS5kYXRhKGRhdGFLZXkpKSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRjb25zdCByb3dJZCA9ICR0YXJnZXQucGFyZW50cygndHInKS5kYXRhKGRhdGFLZXkpO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKCFyb3dJZCkge1xyXG5cdFx0XHRcdHJldHVybjsgLy8gTm8gaW52b2ljZSBJRCB3YXMgZm91bmQuXHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdHNlbGVjdGVkSW52b2ljZXMucHVzaChyb3dJZCk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdHJldHVybiBzZWxlY3RlZEludm9pY2VzO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBEZWxldGUgSW52b2ljZSBDbGlja1xyXG5cdCAqXHJcblx0ICogRGlzcGxheSB0aGUgZGVsZXRlLW1vZGFsLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uRGVsZXRlSW52b2ljZUNsaWNrKGV2ZW50KSB7XHJcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHJcblx0XHRjb25zdCBzZWxlY3RlZEludm9pY2VJZHMgPSBfZ2V0U2VsZWN0ZWRJbnZvaWNlc0RhdGEoJCh0aGlzKSwgJ2ludm9pY2VJZCcpO1xyXG5cdFx0Y29uc3Qgc2VsZWN0ZWRJbnZvaWNlTnVtYmVycyA9IF9nZXRTZWxlY3RlZEludm9pY2VzRGF0YSgkKHRoaXMpLCAnaW52b2ljZU51bWJlcicpO1xyXG5cdFx0XHJcblx0XHQvLyBTaG93IHRoZSBvcmRlciBkZWxldGUgbW9kYWwuXHJcblx0XHRjb25zdCAkbW9kYWwgPSAkKCcuZGVsZXRlLm1vZGFsJyk7XHJcblx0XHQkbW9kYWwuZmluZCgnLnNlbGVjdGVkLWludm9pY2UtaWRzJykudmFsKHNlbGVjdGVkSW52b2ljZUlkcyk7XHJcblx0XHQkbW9kYWwuZmluZCgnLnNlbGVjdGVkLWludm9pY2UtbnVtYmVycycpLnRleHQoc2VsZWN0ZWRJbnZvaWNlTnVtYmVycy5qb2luKCcsICcpKTtcclxuXHRcdCRtb2RhbC5tb2RhbCgnc2hvdycpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBDaGFuZ2UgT3JkZXIgU3RhdHVzIENsaWNrXHJcblx0ICpcclxuXHQgKiBEaXNwbGF5IHRoZSBjaGFuZ2Ugb3JkZXIgc3RhdHVzIG1vZGFsLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uQ2hhbmdlT3JkZXJTdGF0dXNDbGljayhldmVudCkge1xyXG5cdFx0aWYgKCQoZXZlbnQudGFyZ2V0KS5oYXNDbGFzcygnb3JkZXItc3RhdHVzJykpIHtcclxuXHRcdFx0ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5zdGF0dXMubW9kYWwnKTtcclxuXHRcdGNvbnN0IHJvd0RhdGEgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgpO1xyXG5cdFx0Y29uc3Qgc2VsZWN0ZWRPcmRlcnMgPSBfZ2V0U2VsZWN0ZWRJbnZvaWNlc0RhdGEoJCh0aGlzKSwgJ29yZGVySWQnKS5maWx0ZXIob3JkZXJJZCA9PiBvcmRlcklkICE9PSAwKTtcclxuXHRcdFxyXG5cdFx0aWYgKCFzZWxlY3RlZE9yZGVycy5sZW5ndGgpIHtcclxuXHRcdFx0Y29uc3QgdGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnSEVBRElOR19HTV9TVEFUVVMnLCAnb3JkZXJzJyk7XHJcblx0XHRcdGNvbnN0IG1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnTk9fUkVDT1JEU19FUlJPUicsICdvcmRlcnMnKTtcclxuXHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UodGl0bGUsIG1lc3NhZ2UpO1xyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdCRtb2RhbC5maW5kKCcjc3RhdHVzLWRyb3Bkb3duJykudmFsKChyb3dEYXRhKSA/IHJvd0RhdGEuc3RhdHVzSWQgOiAnJyk7XHJcblx0XHQkbW9kYWwuZmluZCgnI2NvbW1lbnQnKS52YWwoJycpO1xyXG5cdFx0JG1vZGFsLmZpbmQoJyNub3RpZnktY3VzdG9tZXIsICNzZW5kLXBhcmNlbC10cmFja2luZy1jb2RlLCAjc2VuZC1jb21tZW50JylcclxuXHRcdFx0LmF0dHIoJ2NoZWNrZWQnLCBmYWxzZSlcclxuXHRcdFx0LnBhcmVudHMoJy5zaW5nbGUtY2hlY2tib3gnKVxyXG5cdFx0XHQucmVtb3ZlQ2xhc3MoJ2NoZWNrZWQnKTtcclxuXHRcdFxyXG5cdFx0Ly8gU2hvdyB0aGUgb3JkZXIgY2hhbmdlIHN0YXR1cyBtb2RhbCAocmVtb3ZlIGR1cGxpY2F0ZSBlbnRyaWVzIGZyb20gc2VsZWN0ZWRPcmRlcnMpLlxyXG5cdFx0JG1vZGFsLmZpbmQoJy5zZWxlY3RlZC1vcmRlcnMnKS50ZXh0KEFycmF5LmZyb20obmV3IFNldChzZWxlY3RlZE9yZGVycykpLmpvaW4oJywgJykpO1xyXG5cdFx0JG1vZGFsLm1vZGFsKCdzaG93Jyk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE9wZW5zIHRoZSBVUkwgd2hpY2ggcHJvdmlkZSB0aGUgYnVsayBQREYncyBhcyBkb3dubG9hZC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7TnVtYmVyW119IGludm9pY2VJZHMgVGhlIGludm9pY2VzIHRvIGJlIGNvbmNhdGVuYXRlZC5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb3BlbkJ1bGtQZGZVcmwoaW52b2ljZUlkcykge1xyXG5cdFx0Y29uc3QgcGFyYW1ldGVycyA9IHtcclxuXHRcdFx0ZG86ICdJbnZvaWNlc01vZGFsc0FqYXgvQnVsa1BkZkludm9pY2VzJyxcclxuXHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKSxcclxuXHRcdFx0aTogaW52b2ljZUlkc1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgdXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD8nICsgJC5wYXJhbShwYXJhbWV0ZXJzKTtcclxuXHRcdFxyXG5cdFx0d2luZG93Lm9wZW4odXJsLCAnX3BhcmVudCcpO1xyXG5cdFx0XHJcblx0XHRqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuaGlkZSgkc3Bpbm5lcik7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENyZWF0ZXMgYSBidWxrIFBERiB3aXRoIGludm9pY2VzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtOdW1iZXJbXX0gaW52b2ljZUlkcyBUaGUgaW52b2ljZXMgdG8gYmUgY29uY2F0ZW5hdGVkLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9jcmVhdGVCdWxrUGRmKGludm9pY2VJZHMpIHtcclxuXHRcdGNvbnN0IHpJbmRleCA9ICQoJy50YWJsZS1maXhlZC1oZWFkZXIgdGhlYWQuZml4ZWQnKS5jc3MoJ3otaW5kZXgnKTsgLy8gQ291bGQgYmUgXCJ1bmRlZmluZWRcIiBhcyB3ZWxsLlxyXG5cdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcywgekluZGV4KTtcclxuXHRcdF9vcGVuQnVsa1BkZlVybChpbnZvaWNlSWRzKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogT3BlbnMgdGhlIGdtX3BkZl9vcmRlci5waHAgaW4gYSBuZXcgdGFiIHdpdGggaW52b2ljZXMgYXMgdHlwZSAkX0dFVCBhcmd1bWVudC5cclxuXHQgKlxyXG5cdCAqIFRoZSBvcmRlciBpZHMgYXJlIHBhc3NlZCBhcyBhIHNlcmlhbGl6ZWQgYXJyYXkgdG8gdGhlIG9JRCAkX0dFVCBhcmd1bWVudC5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25CdWxrRG93bmxvYWRJbnZvaWNlQ2xpY2soKSB7XHJcblx0XHRjb25zdCBpbnZvaWNlSWRzID0gX2dldFNlbGVjdGVkSW52b2ljZXNEYXRhKCQodGhpcyksICdpbnZvaWNlSWQnKTtcclxuXHRcdFxyXG5cdFx0aWYgKGludm9pY2VJZHMubGVuZ3RoID4gZGF0YS5tYXhBbW91bnRJbnZvaWNlc0J1bGtQZGYpIHtcclxuXHRcdFx0Y29uc3QgJG1vZGFsID0gJCgnLmJ1bGstZXJyb3IubW9kYWwnKTtcclxuXHRcdFx0JG1vZGFsLm1vZGFsKCdzaG93Jyk7XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCAkaW52b2ljZU1lc3NhZ2VDb250YWluZXIgPSAkbW9kYWwuZmluZCgnLmludm9pY2VzLW1lc3NhZ2UnKTtcclxuXHRcdFx0JGludm9pY2VNZXNzYWdlQ29udGFpbmVyLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcclxuXHRcdFx0JG1vZGFsLm9uKCdoaWRlLmJzLm1vZGFsJywgKCkgPT4gJGludm9pY2VNZXNzYWdlQ29udGFpbmVyLmFkZENsYXNzKCdoaWRkZW4nKSk7XHJcblx0XHRcdFxyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdF9jcmVhdGVCdWxrUGRmKGludm9pY2VJZHMpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBTaW5nbGUgQ2hlY2tib3ggUmVhZHlcclxuXHQgKlxyXG5cdCAqIFRoaXMgY2FsbGJhY2sgd2lsbCB1c2UgdGhlIGV2ZW50LmRhdGEuaW52b2ljZUlkcyB0byBzZXQgdGhlIGNoZWNrZWQgY2hlY2tib3hlcyBhZnRlciBhIHRhYmxlIHJlLXJlbmRlci5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vblNpbmdsZUNoZWNrYm94UmVhZHkoZXZlbnQpIHtcclxuXHRcdGV2ZW50LmRhdGEuaW52b2ljZUlkcy5mb3JFYWNoKGlkID0+IHtcclxuXHRcdFx0JHRoaXMuZmluZChgdHIjJHtpZH0gaW5wdXQ6Y2hlY2tib3hgKS5zaW5nbGVfY2hlY2tib3goJ2NoZWNrZWQnLCB0cnVlKS50cmlnZ2VyKCdjaGFuZ2UnKTtcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHQvLyBCdWxrIGFjdGlvbiBidXR0b24gc2hvdWxkJ3QgYmUgZGlzYWJsZWQgYWZ0ZXIgYSBkYXRhdGFibGUgcmVsb2FkLlxyXG5cdFx0aWYgKCQoJ3RyIGlucHV0OmNoZWNrYm94OmNoZWNrZWQnKS5sZW5ndGgpIHtcclxuXHRcdFx0JCgnLmJ1bGstYWN0aW9uJykuZmluZCgnYnV0dG9uJykucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJylcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FuY2VsbGF0aW9uIEludm9pY2UgR2VuZXJhdGlvblxyXG5cdCAqXHJcblx0ICogVGhpcyBtZXRob2Qgd2lsbCBjcmVhdGUgYSBuZXcgY2FuY2VsbGF0aW9uIGludm9pY2UgZm9yIHRoZSBzZWxlY3RlZCBpbnZvaWNlcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25DYW5jZWxsYXRpb25JbnZvaWNlQ2xpY2soKSB7XHJcblx0XHRjb25zdCBpbnZvaWNlcyA9IFtdO1xyXG5cdFx0XHJcblx0XHRjb25zdCB7aW52b2ljZUlkLCBpbnZvaWNlTnVtYmVyLCBvcmRlcklkLCBpc0NhbmNlbGxhdGlvbkludm9pY2V9ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoKTtcclxuXHRcdFxyXG5cdFx0aWYgKGlzQ2FuY2VsbGF0aW9uSW52b2ljZSB8fCBvcmRlcklkID09PSAwKSB7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0aW52b2ljZXMucHVzaCh7XHJcblx0XHRcdGludm9pY2VJZCxcclxuXHRcdFx0aW52b2ljZU51bWJlcixcclxuXHRcdFx0b3JkZXJJZFxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGNvbnN0IHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0hFQURJTkdfTU9EQUxfQ0FOQ0VMTEFUSU9OX0lOVk9JQ0UnLCAnb3JkZXJzJyk7XHJcblx0XHRjb25zdCBtZXNzYWdlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfTU9EQUxfQ0FOQ0VMTEFUSU9OX0lOVk9JQ0UnLCAnb3JkZXJzJylcclxuXHRcdFx0LnJlcGxhY2UoJyVzJywgaW52b2ljZXMubWFwKGludm9pY2UgPT4gaW52b2ljZS5pbnZvaWNlTnVtYmVyKS5qb2luKCcsICcpKTtcclxuXHRcdGNvbnN0IGJ1dHRvbnMgPSBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ25vJywgJ2xpZ2h0Ym94X2J1dHRvbnMnKSxcclxuXHRcdFx0XHRjYWxsYmFjazogZXZlbnQgPT4gJChldmVudC5jdXJyZW50VGFyZ2V0KS5wYXJlbnRzKCcubW9kYWwnKS5tb2RhbCgnaGlkZScpXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3llcycsICdsaWdodGJveF9idXR0b25zJyksXHJcblx0XHRcdFx0Y2FsbGJhY2soKSB7XHJcblx0XHRcdFx0XHRjb25zdCBpbnZvaWNlID0gaW52b2ljZXMucG9wKCk7XHJcblx0XHRcdFx0XHRjb25zdCB1cmwgPSBgZ21fcGRmX29yZGVyLnBocD9vSUQ9JHtpbnZvaWNlLm9yZGVySWR9JnR5cGU9aW52b2ljZWBcclxuXHRcdFx0XHRcdFx0KyBgJmNhbmNlbF9pbnZvaWNlX2lkPSR7aW52b2ljZS5pbnZvaWNlSWR9YDtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0d2luZG93Lm9wZW4odXJsLCAnX2JsYW5rJyk7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGNvbnN0IGludm9pY2VJZHMgPSBpbnZvaWNlcy5tYXAoaW52b2ljZSA9PiBpbnZvaWNlLmludm9pY2VJZCk7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmFqYXgucmVsb2FkKCgpID0+IHtcclxuXHRcdFx0XHRcdFx0JHRoaXNcclxuXHRcdFx0XHRcdFx0XHQub2ZmKCdzaW5nbGVfY2hlY2tib3g6cmVhZHknLCBfb25TaW5nbGVDaGVja2JveFJlYWR5KVxyXG5cdFx0XHRcdFx0XHRcdC5vbignc2luZ2xlX2NoZWNrYm94OnJlYWR5Jywge2ludm9pY2VJZHN9LCBfb25TaW5nbGVDaGVja2JveFJlYWR5KTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQkKHRoaXMpLnBhcmVudHMoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRdO1xyXG5cdFx0XHJcblx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZSh0aXRsZSwgbWVzc2FnZSwgYnV0dG9ucyk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEJ1bGsgQ2FuY2VsbGF0aW9uIEludm9pY2UgR2VuZXJhdGlvbiBcclxuXHQgKiBcclxuXHQgKiBUaGlzIG1ldGhvZCB3aWxsIGNyZWF0ZSBjYW5jZWxsYXRpb24gaW52b2ljZXMgZm9yIHRoZSBzZWxlY3RlZCBpbnZvaWNlcy4gXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uQnVsa0NhbmNlbGxhdGlvbkludm9pY2VDbGljaygpIHtcclxuXHRcdGNvbnN0IGludm9pY2VzID0gW107XHJcblx0XHRcclxuXHRcdCR0aGlzLmZpbmQoJ3Rib2R5IGlucHV0OmNoZWNrYm94OmNoZWNrZWQnKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRjb25zdCB7aW52b2ljZUlkLCBpbnZvaWNlTnVtYmVyLCBvcmRlcklkLCBpc0NhbmNlbGxhdGlvbkludm9pY2V9ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoKTtcclxuXHRcdFx0XHJcblx0XHRcdGlmICghaXNDYW5jZWxsYXRpb25JbnZvaWNlICYmIG9yZGVySWQgPiAwKSB7XHJcblx0XHRcdFx0aW52b2ljZXMucHVzaCh7XHJcblx0XHRcdFx0XHRpbnZvaWNlSWQsXHJcblx0XHRcdFx0XHRpbnZvaWNlTnVtYmVyLFxyXG5cdFx0XHRcdFx0b3JkZXJJZFxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0aWYgKCFpbnZvaWNlcy5sZW5ndGgpIHtcclxuXHRcdFx0Y29uc3QgdGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnSEVBRElOR19NT0RBTF9DQU5DRUxMQVRJT05fSU5WT0lDRScsICdvcmRlcnMnKTsgXHJcblx0XHRcdGNvbnN0IG1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnTk9fUkVDT1JEU19FUlJPUicsICdvcmRlcnMnKTtcclxuXHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UodGl0bGUsIG1lc3NhZ2UpO1xyXG5cdFx0XHRyZXR1cm47IFxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRjb25zdCB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdIRUFESU5HX01PREFMX0NBTkNFTExBVElPTl9JTlZPSUNFJywgJ29yZGVycycpO1xyXG5cdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdURVhUX01PREFMX0NBTkNFTExBVElPTl9JTlZPSUNFJywgJ29yZGVycycpXHJcblx0XHRcdC5yZXBsYWNlKCclcycsIGludm9pY2VzLm1hcChpbnZvaWNlID0+IGludm9pY2UuaW52b2ljZU51bWJlcikuam9pbignLCAnKSk7XHJcblx0XHRjb25zdCBidXR0b25zID0gW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdubycsICdsaWdodGJveF9idXR0b25zJyksXHJcblx0XHRcdFx0Y2FsbGJhY2s6IGV2ZW50ID0+ICQoZXZlbnQuY3VycmVudFRhcmdldCkucGFyZW50cygnLm1vZGFsJykubW9kYWwoJ2hpZGUnKVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCd5ZXMnLCAnbGlnaHRib3hfYnV0dG9ucycpLFxyXG5cdFx0XHRcdGNhbGxiYWNrKCkge1xyXG5cdFx0XHRcdFx0Ly8gQ3JlYXRlIG5ldyBjYW5jZWxsYXRpb24gaW52b2ljZXMgYW5kIHJlZnJlc2ggdGhlIHRhYmxlLiBcclxuXHRcdFx0XHRcdGNvbnN0IHJlcXVlc3RzID0gW107XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGludm9pY2VzLmZvckVhY2goaW52b2ljZSA9PiB7XHJcblx0XHRcdFx0XHRcdGNvbnN0IHVybCA9IGBnbV9wZGZfb3JkZXIucGhwP29JRD0ke2ludm9pY2Uub3JkZXJJZH0mdHlwZT1pbnZvaWNlYFxyXG5cdFx0XHRcdFx0XHRcdCsgYCZjYW5jZWxfaW52b2ljZV9pZD0ke2ludm9pY2UuaW52b2ljZUlkfSZhamF4PTFgO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0cmVxdWVzdHMucHVzaCgkLmdldCh1cmwpKTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQkLndoZW4oLi4ucmVxdWVzdHMpLmRvbmUoKC4uLnJlc3BvbnNlcykgPT4ge1xyXG5cdFx0XHRcdFx0XHRjb25zdCBjYW5jZWxsYXRpb25JbnZvaWNlSWRzID0gW107XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRpZiAocmVxdWVzdHMubGVuZ3RoID09PSAxKSB7XHJcblx0XHRcdFx0XHRcdFx0cmVzcG9uc2VzID0gIFtyZXNwb25zZXNdOyAvLyBBbHdheXMgdHJlYXQgdGhlIHJlc3BvbnNlcyBhcyBhbiBhcnJheS5cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0cmVzcG9uc2VzLmZvckVhY2gocmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdGNvbnN0IHtpbnZvaWNlSWR9ID0gSlNPTi5wYXJzZShyZXNwb25zZVswXSk7XHJcblx0XHRcdFx0XHRcdFx0Y2FuY2VsbGF0aW9uSW52b2ljZUlkcy5wdXNoKGludm9pY2VJZCk7XHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0X2NyZWF0ZUJ1bGtQZGYoY2FuY2VsbGF0aW9uSW52b2ljZUlkcyk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRjb25zdCBpbnZvaWNlSWRzID0gaW52b2ljZXMubWFwKGludm9pY2UgPT4gaW52b2ljZS5pbnZvaWNlSWQpO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQoKCkgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdCR0aGlzXHJcblx0XHRcdFx0XHRcdFx0XHQub2ZmKCdzaW5nbGVfY2hlY2tib3g6cmVhZHknLCBfb25TaW5nbGVDaGVja2JveFJlYWR5KVxyXG5cdFx0XHRcdFx0XHRcdFx0Lm9uKCdzaW5nbGVfY2hlY2tib3g6cmVhZHknLCB7aW52b2ljZUlkc30sIF9vblNpbmdsZUNoZWNrYm94UmVhZHkpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0JCh0aGlzKS5wYXJlbnRzKCcubW9kYWwnKS5tb2RhbCgnaGlkZScpO1xyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRdO1xyXG5cdFx0XHJcblx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZSh0aXRsZSwgbWVzc2FnZSwgYnV0dG9ucyk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE9uIEVtYWlsIEludm9pY2UgQ2xpY2tcclxuXHQgKlxyXG5cdCAqIERpc3BsYXkgdGhlIGVtYWlsLWludm9pY2UgbW9kYWwuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uRW1haWxJbnZvaWNlQ2xpY2soKSB7XHJcblx0XHRjb25zdCAkbW9kYWwgPSAkKCcuZW1haWwtaW52b2ljZS5tb2RhbCcpO1xyXG5cdFx0Y29uc3Qgcm93RGF0YSA9ICQodGhpcykucGFyZW50cygndHInKS5kYXRhKCk7XHJcblx0XHRjb25zdCB1cmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwJztcclxuXHRcdGNvbnN0IGRhdGEgPSB7XHJcblx0XHRcdGRvOiAnSW52b2ljZXNNb2RhbHNBamF4L0dldEVtYWlsSW52b2ljZUluZm9ybWF0aW9uJyxcclxuXHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKSxcclxuXHRcdFx0bzogW3Jvd0RhdGEub3JkZXJJZF1cclxuXHRcdH07XHJcblx0XHRcclxuXHRcdCRtb2RhbFxyXG5cdFx0XHQuZGF0YSgnaW52b2ljZUlkJywgcm93RGF0YS5pbnZvaWNlSWQpXHJcblx0XHRcdC5kYXRhKCdvcmRlcklkJywgcm93RGF0YS5vcmRlcklkKVxyXG5cdFx0XHQubW9kYWwoJ3Nob3cnKTtcclxuXHRcdFxyXG5cdFx0JC5hamF4KHt1cmwsIGRhdGEsIGRhdGFUeXBlOiAnanNvbid9KS5kb25lKHJlc3BvbnNlID0+IHtcclxuXHRcdFx0Y29uc3QgZGF0ZUZvcm1hdCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpID09PSAnZGUnID8gJ0RELk1NLllZJyA6ICdNTS5ERC5ZWSc7XHJcblx0XHRcdGNvbnN0IHN1YmplY3QgPSByZXNwb25zZS5zdWJqZWN0XHJcblx0XHRcdFx0LnJlcGxhY2UoJ3tJTlZPSUNFX0lEfScsIHJvd0RhdGEuaW52b2ljZU51bWJlcilcclxuXHRcdFx0XHQucmVwbGFjZSgne0RBVEV9JywgbW9tZW50KHJvd0RhdGEuaW52b2ljZURhdGUuZGF0ZSkuZm9ybWF0KGRhdGVGb3JtYXQpKVxyXG5cdFx0XHRcdC5yZXBsYWNlKCd7T1JERVJfSUR9Jywgcm93RGF0YS5vcmRlcklkKTtcclxuXHRcdFx0XHJcblx0XHRcdCRtb2RhbC5maW5kKCcuc3ViamVjdCcpLnZhbChzdWJqZWN0KTtcclxuXHRcdFx0XHJcblx0XHRcdGxldCBjdXN0b21lckluZm8gPSBgXCIke3Jvd0RhdGEuY3VzdG9tZXJOYW1lfVwiYDtcclxuXHRcdFx0XHJcblx0XHRcdGlmIChyZXNwb25zZS5lbWFpbHNbcm93RGF0YS5vcmRlcklkXSkge1xyXG5cdFx0XHRcdGN1c3RvbWVySW5mbyArPSBgIFwiJHtyZXNwb25zZS5lbWFpbHNbcm93RGF0YS5vcmRlcklkXX1cImA7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdCRtb2RhbC5maW5kKCcuY3VzdG9tZXItaW5mbycpLnRleHQoY3VzdG9tZXJJbmZvKTtcclxuXHRcdFx0JG1vZGFsLmZpbmQoJy5lbWFpbC1hZGRyZXNzJykudmFsKHJlc3BvbnNlLmVtYWlsc1tyb3dEYXRhLm9yZGVySWRdIHx8ICcnKTtcclxuXHRcdH0pO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBHZW5lcmF0ZSBlbWFpbCByb3cgbWFya3VwLlxyXG5cdCAqXHJcblx0ICogVGhpcyBtZXRob2QgaXMgdXNlZCBieSB0aGUgYnVsayBlbWFpbCBtb2RhbCBmb3IgZHluYW1pYyBlbWFpbC1yb3cgZ2VuZXJhdGlvbi5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBpbnZvaWNlIENvbnRhaW5zIHRoZSByZXF1aXJlZCByb3cgZGF0YS5cclxuXHQgKiBAcGFyYW0ge09iamVjdH0gZW1haWxJbmZvcm1hdGlvbiBDb250YWlucyB0aGUgc3ViamVjdCBhbmQgZW1haWwgYWRkcmVzc2VzIGZvciB0aGUgaW52b2ljZXMuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtqUXVlcnl9IFJldHVybnMgdGhlIHJvdyBzZWxlY3Rvci5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfZ2VuZXJhdGVNYWlsUm93TWFya3VwKGludm9pY2UsIGVtYWlsSW5mb3JtYXRpb24pIHtcclxuXHRcdGNvbnN0ICRyb3cgPSAkKCc8ZGl2Lz4nLCB7Y2xhc3M6ICdmb3JtLWdyb3VwIGVtYWlsLWxpc3QtaXRlbSd9KTtcclxuXHRcdGNvbnN0ICRpZENvbHVtbiA9ICQoJzxkaXYvPicsIHtjbGFzczogJ2NvbC1zbS0zJ30pO1xyXG5cdFx0Y29uc3QgJGRhdGVDb2x1bW4gPSAkKCc8ZGl2Lz4nLCB7Y2xhc3M6ICdjb2wtc20tMyd9KTtcclxuXHRcdGNvbnN0ICRlbWFpbENvbHVtbiA9ICQoJzxkaXYvPicsIHtjbGFzczogJ2NvbC1zbS02J30pO1xyXG5cdFx0XHJcblx0XHRjb25zdCAkaWRMYWJlbCA9ICQoJzxsYWJlbC8+Jywge1xyXG5cdFx0XHRjbGFzczogJ2NvbnRyb2wtbGFiZWwgaWQtbGFiZWwgZm9yY2UtdGV4dC1jb2xvci1ibGFjayBmb3JjZS10ZXh0LW5vcm1hbC13ZWlnaHQnLFxyXG5cdFx0XHRodG1sOiBgPGEgaHJlZj1cInJlcXVlc3RfcG9ydC5waHA/bW9kdWxlPU9yZGVyQWRtaW4mYWN0aW9uPXNob3dQZGYmdHlwZT1pbnZvaWNlYFxyXG5cdFx0XHQrIGAmaW52b2ljZV9udW1iZXI9JHtpbnZvaWNlLmludm9pY2VOdW1iZXJ9Jm9yZGVyX2lkPSR7aW52b2ljZS5vcmRlcklkfVwiIHRhcmdldD1cIl9ibGFua1wiPmBcclxuXHRcdFx0K2Ake2ludm9pY2UuaW52b2ljZU51bWJlcn08L2E+YFxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGNvbnN0IGRhdGVGb3JtYXQgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdsYW5ndWFnZUNvZGUnKSA9PT0gJ2RlJyA/ICdERC5NTS5ZWVlZJyA6ICdNTS5ERC5ZWVlZJztcclxuXHRcdGNvbnN0ICRkYXRlTGFiZWwgPSAkKCc8bGFiZWwvPicsIHtcclxuXHRcdFx0Y2xhc3M6ICdjb250cm9sLWxhYmVsIGRhdGUtbGFiZWwgZm9yY2UtdGV4dC1jb2xvci1ibGFjayBmb3JjZS10ZXh0LW5vcm1hbC13ZWlnaHQnLCBcclxuXHRcdFx0dGV4dDogbW9tZW50KGludm9pY2UuaW52b2ljZURhdGUuZGF0ZSkuZm9ybWF0KGRhdGVGb3JtYXQpIFxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRlbWFpbElucHV0ID0gJCgnPGlucHV0Lz4nLCB7XHJcblx0XHRcdGNsYXNzOiAnZm9ybS1jb250cm9sIGVtYWlsLWlucHV0JyxcclxuXHRcdFx0dHlwZTogJ3RleHQnLFxyXG5cdFx0XHR2YWx1ZTogZW1haWxJbmZvcm1hdGlvbi5lbWFpbHNbaW52b2ljZS5vcmRlcklkXVxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdCRpZExhYmVsLmFwcGVuZFRvKCRpZENvbHVtbik7XHJcblx0XHQkZGF0ZUxhYmVsLmFwcGVuZFRvKCRkYXRlQ29sdW1uKTtcclxuXHRcdCRlbWFpbElucHV0LmFwcGVuZFRvKCRlbWFpbENvbHVtbik7XHJcblx0XHRcclxuXHRcdCRyb3cuYXBwZW5kKFskaWRDb2x1bW4sICRkYXRlQ29sdW1uLCAkZW1haWxDb2x1bW5dKTtcclxuXHRcdCRyb3cuZGF0YSgnaW52b2ljZScsIGludm9pY2UpO1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gJHJvdztcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogQnVsayBFbWFpbCBJbnZvaWNlIENsaWNrLlxyXG5cdCAqXHJcblx0ICogU2VuZHMgZW1haWxzIHdpdGggdGhlIHNlbGVjdGVkIGludm9pY2VzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX29uQnVsa0VtYWlsSW52b2ljZUNsaWNrKGV2ZW50KSB7XHJcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHJcblx0XHRjb25zdCAkbW9kYWwgPSAkKCcuYnVsay1lbWFpbC1pbnZvaWNlLm1vZGFsJyk7XHJcblx0XHRjb25zdCAkbWFpbExpc3QgPSAkbW9kYWwuZmluZCgnLmVtYWlsLWxpc3QnKTtcclxuXHRcdGNvbnN0IHNlbGVjdGVkSW52b2ljZXMgPSBbXTtcclxuXHRcdFxyXG5cdFx0JHRoaXMuZmluZCgndGJvZHkgaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZCcpLmVhY2goKGluZGV4LCBjaGVja2JveCkgPT4ge1xyXG5cdFx0XHRjb25zdCByb3dEYXRhID0gJChjaGVja2JveCkucGFyZW50cygndHInKS5kYXRhKCk7XHJcblx0XHRcdHNlbGVjdGVkSW52b2ljZXMucHVzaChyb3dEYXRhKTtcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHQvLyBTb3J0IHNlbGVjdGVkIGludm9pY2VzIGJ5IGRhdGUgKGRlc2NlbmRpbmcpLiBcclxuXHRcdHNlbGVjdGVkSW52b2ljZXMuc29ydCgoaW52b2ljZTEsIGludm9pY2UyKSA9PiBpbnZvaWNlMS5pbnZvaWNlRGF0ZSAtIGludm9pY2UyLmludm9pY2VEYXRlKS5yZXZlcnNlKCk7XHJcblx0XHRcclxuXHRcdGlmIChzZWxlY3RlZEludm9pY2VzLmxlbmd0aCkge1xyXG5cdFx0XHQkbWFpbExpc3QuZW1wdHkoKTtcclxuXHRcdFx0XHJcblx0XHRcdGNvbnN0IHVybCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHAnO1xyXG5cdFx0XHRjb25zdCBkYXRhID0ge1xyXG5cdFx0XHRcdGRvOiAnSW52b2ljZXNNb2RhbHNBamF4L0dldEVtYWlsSW52b2ljZUluZm9ybWF0aW9uJyxcclxuXHRcdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpLFxyXG5cdFx0XHRcdG86IFsuLi5uZXcgU2V0KHNlbGVjdGVkSW52b2ljZXMubWFwKGludm9pY2UgPT4gaW52b2ljZS5vcmRlcklkKSldIC8vIFVuaXF1ZSBvcmRlcnMgbnVtYmVyIGFycmF5LlxyXG5cdFx0XHR9O1xyXG5cdFx0XHRcclxuXHRcdFx0JC5hamF4KHtcclxuXHRcdFx0XHR1cmwsXHJcblx0XHRcdFx0ZGF0YSxcclxuXHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nXHJcblx0XHRcdH0pXHJcblx0XHRcdFx0LmRvbmUocmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdFx0c2VsZWN0ZWRJbnZvaWNlcy5mb3JFYWNoKGludm9pY2UgPT4gJG1haWxMaXN0LmFwcGVuZChfZ2VuZXJhdGVNYWlsUm93TWFya3VwKGludm9pY2UsIHJlc3BvbnNlKSkpXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQkbW9kYWwubW9kYWwoJ3Nob3cnKTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gSU5JVElBTElaQVRJT05cclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdC8vIEJpbmQgdGFibGUgcm93IGFjdGlvbnMuXHJcblx0XHQkdGhpc1xyXG5cdFx0XHQub24oJ2NsaWNrJywgJ3Rib2R5IHRyJywgX29uVGFibGVSb3dDbGljaylcclxuXHRcdFx0Lm9uKCdjaGFuZ2UnLCAnLmJ1bGstc2VsZWN0aW9uJywgX29uQnVsa1NlbGVjdGlvbkNoYW5nZSlcclxuXHRcdFx0Lm9uKCdjaGFuZ2UnLCAnaW5wdXQ6Y2hlY2tib3gnLCBfb25UYWJsZVJvd0NoZWNrYm94Q2hhbmdlKVxyXG5cdFx0XHQub24oJ2NsaWNrJywgJy5lbWFpbC1pbnZvaWNlJywgX29uRW1haWxJbnZvaWNlQ2xpY2spXHJcblx0XHRcdC5vbignY2xpY2snLCAnLmNhbmNlbGxhdGlvbi1pbnZvaWNlJywgX29uQ2FuY2VsbGF0aW9uSW52b2ljZUNsaWNrKTtcclxuXHRcdFxyXG5cdFx0Ly8gQmluZCB0YWJsZSByb3cgYW5kIGJ1bGsgYWN0aW9ucy4gXHJcblx0XHQkdGhpcy5wYXJlbnRzKCcuaW52b2ljZXMnKVxyXG5cdFx0XHQub24oJ2NsaWNrJywgJy5vcmRlci1zdGF0dXMsIC5idG4tZ3JvdXAgLmNoYW5nZS1zdGF0dXMnLCBfb25DaGFuZ2VPcmRlclN0YXR1c0NsaWNrKVxyXG5cdFx0XHQub24oJ2NsaWNrJywgJy5idG4tZ3JvdXAgLmRlbGV0ZSwgLmFjdGlvbnMgLmRlbGV0ZScsIF9vbkRlbGV0ZUludm9pY2VDbGljaylcclxuXHRcdFx0Lm9uKCdjbGljaycsICcuYnRuLWdyb3VwIC5idWxrLWRvd25sb2FkLWludm9pY2UnLCBfb25CdWxrRG93bmxvYWRJbnZvaWNlQ2xpY2spXHJcblx0XHRcdC5vbignY2xpY2snLCAnLmJ0bi1ncm91cCAuYnVsay1jYW5jZWxsYXRpb24taW52b2ljZScsIF9vbkJ1bGtDYW5jZWxsYXRpb25JbnZvaWNlQ2xpY2spXHJcblx0XHRcdC5vbignY2xpY2snLCAnLmJ0bi1ncm91cCAuYnVsay1lbWFpbC1pbnZvaWNlJywgX29uQnVsa0VtYWlsSW52b2ljZUNsaWNrKTtcclxuXHRcdFxyXG5cdFx0ZG9uZSgpO1xyXG5cdH07XHJcblx0XHJcblx0cmV0dXJuIG1vZHVsZTtcclxufSk7XHJcbiJdfQ==
