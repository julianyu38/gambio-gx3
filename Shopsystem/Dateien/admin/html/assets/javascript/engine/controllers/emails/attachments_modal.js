'use strict';

/* --------------------------------------------------------------
 emails_modal.js 2018-11-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 ----------------------------------------------------------------
 */

/**
 * ## Attachments Modal Controller
 *
 * This controller will handle the attachments modal dialog operations of the admin/emails page.
 *
 * @module Controllers/attachments_modal
 */
gx.controllers.module('attachments_modal', ['modal', gx.source + '/libs/emails', jse.source + '/vendor/DateJS/date'],

/** @lends module:Controllers/attachments_modal */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Emails Main Table Selector
  *
  * @type {object}
  */
	$table = $('#emails-table'),


	/**
  * Default Module Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Module Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Delete old attachments request.
  *
  * @param {object} event Contains the event information.
  */
	var _onDeleteOldAttachments = function _onDeleteOldAttachments(event) {
		// Validate selected date before making the request.
		if ($this.find('#removal-date').val() === '') {
			return; // do not proceed
		}

		// Display confirmation modal before proceeding.
		var modalOptions = {
			title: jse.core.lang.translate('delete', 'buttons') + ' - ' + $this.find('#removal-date').datepicker('getDate').toString('dd.MM.yyyy'),
			content: jse.core.lang.translate('prompt_delete_old_attachments', 'emails'),
			buttons: [{
				text: jse.core.lang.translate('no', 'lightbox_buttons'),
				click: function click() {
					$(this).dialog('close');
				}
			}, {
				text: jse.core.lang.translate('yes', 'lightbox_buttons'),
				click: function click() {
					jse.libs.emails.deleteOldAttachments($('#removal-date').datepicker('getDate').toString('yyyy-MM-dd HH:mm:ss')).done(function (response) {
						var size = response.size.megabytes !== 0 ? response.size.megabytes + ' Megabytes' : response.size.bytes + ' Bytes';

						var message = jse.core.lang.translate('message_delete_old_attachments_success', 'emails') + '<br/>' + jse.core.lang.translate('count', 'admin_labels') + ': ' + response.count + ', ' + jse.core.lang.translate('size', 'db_backup') + ': ' + size;

						jse.libs.modal.message({
							title: 'Info',
							content: message
						});

						jse.libs.emails.getAttachmentsSize($('#attachments-size'));
						$table.DataTable().ajax.reload();
						$this.dialog('close');
					}).fail(function (response) {
						var title = jse.core.lang.translate('error', 'messages');

						jse.libs.modal.message({
							title: title,
							content: response.message
						});
					});

					$(this).dialog('close');
				}
			}]
		};

		jse.libs.modal.message(modalOptions);
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the module, called by the engine.
  */
	module.init = function (done) {
		$this.on('click', '#delete-old-attachments', _onDeleteOldAttachments);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVtYWlscy9hdHRhY2htZW50c19tb2RhbC5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwianNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiR0YWJsZSIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9vbkRlbGV0ZU9sZEF0dGFjaG1lbnRzIiwiZXZlbnQiLCJmaW5kIiwidmFsIiwibW9kYWxPcHRpb25zIiwidGl0bGUiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsImRhdGVwaWNrZXIiLCJ0b1N0cmluZyIsImNvbnRlbnQiLCJidXR0b25zIiwidGV4dCIsImNsaWNrIiwiZGlhbG9nIiwibGlicyIsImVtYWlscyIsImRlbGV0ZU9sZEF0dGFjaG1lbnRzIiwiZG9uZSIsInJlc3BvbnNlIiwic2l6ZSIsIm1lZ2FieXRlcyIsImJ5dGVzIiwibWVzc2FnZSIsImNvdW50IiwibW9kYWwiLCJnZXRBdHRhY2htZW50c1NpemUiLCJEYXRhVGFibGUiLCJhamF4IiwicmVsb2FkIiwiZmFpbCIsImluaXQiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLG1CQURELEVBR0MsQ0FDQyxPQURELEVBRUNGLEdBQUdHLE1BQUgsR0FBWSxjQUZiLEVBR0NDLElBQUlELE1BQUosR0FBYSxxQkFIZCxDQUhEOztBQVNDOztBQUVBLFVBQVNFLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxVQUFTRCxFQUFFLGVBQUYsQ0FiVjs7O0FBZUM7Ozs7O0FBS0FFLFlBQVcsRUFwQlo7OztBQXNCQzs7Ozs7QUFLQUMsV0FBVUgsRUFBRUksTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkosSUFBN0IsQ0EzQlg7OztBQTZCQzs7Ozs7QUFLQUgsVUFBUyxFQWxDVjs7QUFvQ0E7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLEtBQUlVLDBCQUEwQixTQUExQkEsdUJBQTBCLENBQVNDLEtBQVQsRUFBZ0I7QUFDN0M7QUFDQSxNQUFJUCxNQUFNUSxJQUFOLENBQVcsZUFBWCxFQUE0QkMsR0FBNUIsT0FBc0MsRUFBMUMsRUFBOEM7QUFDN0MsVUFENkMsQ0FDckM7QUFDUjs7QUFFRDtBQUNBLE1BQUlDLGVBQWU7QUFDbEJDLFVBQU9iLElBQUljLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFFBQXhCLEVBQWtDLFNBQWxDLElBQStDLEtBQS9DLEdBQ0xkLE1BQU1RLElBQU4sQ0FBVyxlQUFYLEVBQTRCTyxVQUE1QixDQUF1QyxTQUF2QyxFQUFrREMsUUFBbEQsQ0FBMkQsWUFBM0QsQ0FGZ0I7QUFHbEJDLFlBQVNuQixJQUFJYyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwrQkFBeEIsRUFBeUQsUUFBekQsQ0FIUztBQUlsQkksWUFBUyxDQUNSO0FBQ0NDLFVBQU1yQixJQUFJYyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixJQUF4QixFQUE4QixrQkFBOUIsQ0FEUDtBQUVDTSxXQUFPLGlCQUFXO0FBQ2pCbkIsT0FBRSxJQUFGLEVBQVFvQixNQUFSLENBQWUsT0FBZjtBQUNBO0FBSkYsSUFEUSxFQU9SO0FBQ0NGLFVBQU1yQixJQUFJYyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixLQUF4QixFQUErQixrQkFBL0IsQ0FEUDtBQUVDTSxXQUFPLGlCQUFXO0FBQ2pCdEIsU0FBSXdCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsb0JBQWhCLENBQXFDdkIsRUFBRSxlQUFGLEVBQ25DYyxVQURtQyxDQUN4QixTQUR3QixFQUNiQyxRQURhLENBQ0oscUJBREksQ0FBckMsRUFFRVMsSUFGRixDQUVPLFVBQVNDLFFBQVQsRUFBbUI7QUFDeEIsVUFBSUMsT0FBUUQsU0FBU0MsSUFBVCxDQUFjQyxTQUFkLEtBQTRCLENBQTdCLEdBQ1JGLFNBQVNDLElBQVQsQ0FBY0MsU0FBZCxHQUEwQixZQURsQixHQUVSRixTQUFTQyxJQUFULENBQWNFLEtBQWQsR0FBc0IsUUFGekI7O0FBSUEsVUFBSUMsVUFDSGhDLElBQUljLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHdDQUF4QixFQUFrRSxRQUFsRSxJQUNFLE9BREYsR0FDWWhCLElBQUljLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLGNBQWpDLENBRFosR0FDK0QsSUFEL0QsR0FFRVksU0FBU0ssS0FGWCxHQUVtQixJQUZuQixHQUUwQmpDLElBQUljLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE1BQXhCLEVBQWdDLFdBQWhDLENBRjFCLEdBRXlFLElBRnpFLEdBR0VhLElBSkg7O0FBTUE3QixVQUFJd0IsSUFBSixDQUFTVSxLQUFULENBQWVGLE9BQWYsQ0FBdUI7QUFDdEJuQixjQUFPLE1BRGU7QUFFdEJNLGdCQUFTYTtBQUZhLE9BQXZCOztBQUtBaEMsVUFBSXdCLElBQUosQ0FBU0MsTUFBVCxDQUFnQlUsa0JBQWhCLENBQW1DaEMsRUFBRSxtQkFBRixDQUFuQztBQUNBQyxhQUFPZ0MsU0FBUCxHQUFtQkMsSUFBbkIsQ0FBd0JDLE1BQXhCO0FBQ0FwQyxZQUFNcUIsTUFBTixDQUFhLE9BQWI7QUFDQSxNQXJCRixFQXNCRWdCLElBdEJGLENBc0JPLFVBQVNYLFFBQVQsRUFBbUI7QUFDeEIsVUFBSWYsUUFBUWIsSUFBSWMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FBWjs7QUFFQWhCLFVBQUl3QixJQUFKLENBQVNVLEtBQVQsQ0FBZUYsT0FBZixDQUF1QjtBQUN0Qm5CLGNBQU9BLEtBRGU7QUFFdEJNLGdCQUFTUyxTQUFTSTtBQUZJLE9BQXZCO0FBSUEsTUE3QkY7O0FBK0JBN0IsT0FBRSxJQUFGLEVBQVFvQixNQUFSLENBQWUsT0FBZjtBQUNBO0FBbkNGLElBUFE7QUFKUyxHQUFuQjs7QUFtREF2QixNQUFJd0IsSUFBSixDQUFTVSxLQUFULENBQWVGLE9BQWYsQ0FBdUJwQixZQUF2QjtBQUNBLEVBM0REOztBQTZEQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBZCxRQUFPMEMsSUFBUCxHQUFjLFVBQVNiLElBQVQsRUFBZTtBQUM1QnpCLFFBQU11QyxFQUFOLENBQVMsT0FBVCxFQUFrQix5QkFBbEIsRUFBNkNqQyx1QkFBN0M7QUFDQW1CO0FBQ0EsRUFIRDs7QUFLQSxRQUFPN0IsTUFBUDtBQUNBLENBMUlGIiwiZmlsZSI6ImVtYWlscy9hdHRhY2htZW50c19tb2RhbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZW1haWxzX21vZGFsLmpzIDIwMTgtMTEtMjJcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE4IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgQXR0YWNobWVudHMgTW9kYWwgQ29udHJvbGxlclxuICpcbiAqIFRoaXMgY29udHJvbGxlciB3aWxsIGhhbmRsZSB0aGUgYXR0YWNobWVudHMgbW9kYWwgZGlhbG9nIG9wZXJhdGlvbnMgb2YgdGhlIGFkbWluL2VtYWlscyBwYWdlLlxuICpcbiAqIEBtb2R1bGUgQ29udHJvbGxlcnMvYXR0YWNobWVudHNfbW9kYWxcbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnYXR0YWNobWVudHNfbW9kYWwnLFxuXHRcblx0W1xuXHRcdCdtb2RhbCcsXG5cdFx0Z3guc291cmNlICsgJy9saWJzL2VtYWlscycsXG5cdFx0anNlLnNvdXJjZSArICcvdmVuZG9yL0RhdGVKUy9kYXRlJ1xuXHRdLFxuXHRcblx0LyoqIEBsZW5kcyBtb2R1bGU6Q29udHJvbGxlcnMvYXR0YWNobWVudHNfbW9kYWwgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEVtYWlscyBNYWluIFRhYmxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRhYmxlID0gJCgnI2VtYWlscy10YWJsZScpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgTW9kdWxlIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE1vZHVsZSBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVsZXRlIG9sZCBhdHRhY2htZW50cyByZXF1ZXN0LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IENvbnRhaW5zIHRoZSBldmVudCBpbmZvcm1hdGlvbi5cblx0XHQgKi9cblx0XHR2YXIgX29uRGVsZXRlT2xkQXR0YWNobWVudHMgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0Ly8gVmFsaWRhdGUgc2VsZWN0ZWQgZGF0ZSBiZWZvcmUgbWFraW5nIHRoZSByZXF1ZXN0LlxuXHRcdFx0aWYgKCR0aGlzLmZpbmQoJyNyZW1vdmFsLWRhdGUnKS52YWwoKSA9PT0gJycpIHtcblx0XHRcdFx0cmV0dXJuOyAvLyBkbyBub3QgcHJvY2VlZFxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBEaXNwbGF5IGNvbmZpcm1hdGlvbiBtb2RhbCBiZWZvcmUgcHJvY2VlZGluZy5cblx0XHRcdHZhciBtb2RhbE9wdGlvbnMgPSB7XG5cdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZGVsZXRlJywgJ2J1dHRvbnMnKSArICcgLSAnXG5cdFx0XHRcdCsgJHRoaXMuZmluZCgnI3JlbW92YWwtZGF0ZScpLmRhdGVwaWNrZXIoJ2dldERhdGUnKS50b1N0cmluZygnZGQuTU0ueXl5eScpLFxuXHRcdFx0XHRjb250ZW50OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgncHJvbXB0X2RlbGV0ZV9vbGRfYXR0YWNobWVudHMnLCAnZW1haWxzJyksXG5cdFx0XHRcdGJ1dHRvbnM6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnbm8nLCAnbGlnaHRib3hfYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0Y2xpY2s6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHQkKHRoaXMpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCd5ZXMnLCAnbGlnaHRib3hfYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0Y2xpY2s6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRqc2UubGlicy5lbWFpbHMuZGVsZXRlT2xkQXR0YWNobWVudHMoJCgnI3JlbW92YWwtZGF0ZScpXG5cdFx0XHRcdFx0XHRcdFx0LmRhdGVwaWNrZXIoJ2dldERhdGUnKS50b1N0cmluZygneXl5eS1NTS1kZCBISDptbTpzcycpKVxuXHRcdFx0XHRcdFx0XHRcdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHR2YXIgc2l6ZSA9IChyZXNwb25zZS5zaXplLm1lZ2FieXRlcyAhPT0gMClcblx0XHRcdFx0XHRcdFx0XHRcdFx0PyByZXNwb25zZS5zaXplLm1lZ2FieXRlcyArICcgTWVnYWJ5dGVzJ1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQ6IHJlc3BvbnNlLnNpemUuYnl0ZXMgKyAnIEJ5dGVzJztcblx0XHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdFx0dmFyIG1lc3NhZ2UgPVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnbWVzc2FnZV9kZWxldGVfb2xkX2F0dGFjaG1lbnRzX3N1Y2Nlc3MnLCAnZW1haWxzJylcblx0XHRcdFx0XHRcdFx0XHRcdFx0KyAnPGJyLz4nICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2NvdW50JywgJ2FkbWluX2xhYmVscycpICsgJzogJ1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQrIHJlc3BvbnNlLmNvdW50ICsgJywgJyArIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdzaXplJywgJ2RiX2JhY2t1cCcpICsgJzogJ1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQrIHNpemU7XG5cdFx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLm1lc3NhZ2Uoe1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR0aXRsZTogJ0luZm8nLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRjb250ZW50OiBtZXNzYWdlXG5cdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdFx0anNlLmxpYnMuZW1haWxzLmdldEF0dGFjaG1lbnRzU2l6ZSgkKCcjYXR0YWNobWVudHMtc2l6ZScpKTtcblx0XHRcdFx0XHRcdFx0XHRcdCR0YWJsZS5EYXRhVGFibGUoKS5hamF4LnJlbG9hZCgpO1xuXHRcdFx0XHRcdFx0XHRcdFx0JHRoaXMuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHRcdFx0LmZhaWwoZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRcdFx0XHRcdHZhciB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpO1xuXHRcdFx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdFx0XHRqc2UubGlicy5tb2RhbC5tZXNzYWdlKHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0dGl0bGU6IHRpdGxlLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRjb250ZW50OiByZXNwb25zZS5tZXNzYWdlXG5cdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XVxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZShtb2RhbE9wdGlvbnMpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIG1ldGhvZCBvZiB0aGUgbW9kdWxlLCBjYWxsZWQgYnkgdGhlIGVuZ2luZS5cblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCR0aGlzLm9uKCdjbGljaycsICcjZGVsZXRlLW9sZC1hdHRhY2htZW50cycsIF9vbkRlbGV0ZU9sZEF0dGFjaG1lbnRzKTtcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
