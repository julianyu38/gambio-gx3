'use strict';

/* --------------------------------------------------------------
 edit.js 2016-10-25
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Table Editing Controller
 *
 * Handles the editing functionality of the properties modal table.
 */
gx.controllers.module('properties_edit', ['modal', jse.source + '/vendor/sumoselect/jquery.sumoselect.min.js', jse.source + '/vendor/sumoselect/sumoselect.min.css'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Enter Key Code
  *
  * @type {Number}
  */

	var ENTER_KEY_CODE = 13;

	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	var $this = $(this);

	/**
  * Edit Row Selector
  *
  * @type {jQuery}
  */
	var $edit = $this.find('tr.properties-edit');

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = { bindings: {} };

	// Dynamically define the edit row data-bindings.
	$edit.find('th').each(function () {
		var columnName = $(this).data('columnName');

		if (columnName === 'checkbox' || columnName === 'actions') {
			return true;
		}

		module.bindings[columnName] = $(this).find('input, select').first();
	});

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Save modifications event handler.
  *
  * This method will look for modified values and send them to back-end with an AJAX request.
  */
	function _onSaveClick() {
		var $checkedSingleCheckboxes = $this.find('input:checkbox:checked.properties-row-selection');
		var edit = {};
		var data = {};

		if ($checkedSingleCheckboxes.length === 0) {
			var title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
			var message = jse.core.lang.translate('NO_PRODUCT_SELECTED', 'admin_quick_edit');
			var buttons = [{
				title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
				callback: function callback(event) {
					return $(event.currentTarget).parents('.modal').modal('hide');
				}
			}];

			jse.libs.modal.showMessage(title, message, buttons);

			return;
		}

		$edit.find('th').each(function () {
			var columnName = $(this).data('columnName');

			if (columnName === 'checkbox' || columnName === 'actions') {
				return true;
			}

			var value = module.bindings[columnName].get();

			if (value) {
				edit[columnName] = value;
			}
		});

		$checkedSingleCheckboxes.each(function () {
			var $tableRow = $(this).parents('tr');

			if ($tableRow.find('input:text.modified').length > 0) {
				var inlineEdit = {};

				$tableRow.find('input:text.modified').each(function () {
					var $cell = $(this).closest('td');
					var columnIndex = $this.DataTable().column($cell).index();
					var columnName = $edit.find('th').eq(columnIndex).data('columnName');

					inlineEdit[columnName] = $(this).val();
				});

				data[$tableRow.attr('id')] = inlineEdit;
			} else {
				data[$tableRow.attr('id')] = edit;
			}

			$(this).prop('checked', !$(this).prop('checked')).trigger('change');
		});

		if (_checkForModifications(data) === false) {
			$checkedSingleCheckboxes.each(function () {
				_resolveRowChanges($(this).parents('tr'));
			});

			return;
		}

		_save(data);
	}

	/**
  * Checks for value modifications.
  *
  * @param {Object} data Contains current row data.
  *
  * @return {boolean} Returns whether modifications were made or not.
  */
	function _checkForModifications(data) {
		var modificationExists = false;

		for (var property in data) {
			if (data.hasOwnProperty(property)) {
				if (!$.isEmptyObject(data[property])) {
					modificationExists = true;
				}
			}
		}

		return modificationExists;
	}

	/**
  * Resolves row changes.
  *
  * @param {jQuery} $row Selector of the row to be resolved.
  */
	function _resolveRowChanges($row) {
		var rowIndex = $this.DataTable().row($row).index();

		$row.find('input:text').each(function () {
			var $cell = $(this).closest('td');
			var columnIndex = $this.DataTable().column($cell).index();

			this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
		});
	}

	/**
  * Trigger filtering once the user presses the enter key inside a filter input.
  *
  * @param {jQuery.Event} event Contains event information.
  */
	function _onInputTextFilterKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$edit.find('.apply-properties-edits').trigger('click');
		}
	}

	/**
  * Trigger modifications submit once the user presses the enter key inside a edit input.
  *
  * @param {jQuery.Event} event Contains event information.
  */
	function _onInputTextRowKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$edit.parents('.properties.modal').find('.save-properties-bulk-row-edits').trigger('click');
		}
	}

	/**
  * Stores the change of a shipping time value.
  */
	function _onTableRowShippingTimeChange() {
		var propertiesId = $(this).parents('tr').attr('id');
		var shippingTimeId = $(this).val();
		var edit = {};
		var data = {};

		edit['combiShippingStatusName'] = shippingTimeId;
		data[propertiesId] = edit;

		_save(data, false);
	}

	/**
  * Stores the change of a price type value.
  */
	function _onTableRowPriceTypeChange() {
		var propertiesId = $(this).parents('tr').attr('id');
		var priceType = $(this).val();
		var edit = {};
		var data = {};

		edit['combiPriceType'] = priceType;
		data[propertiesId] = edit;

		_save(data, false);
	}

	/**
  * Save modified data with an AJAX request.
  *
  * @param {Object} data Contains the updated data.
  * @param {Boolean} [reload=true] Reload the page after the request is done.
  */
	function _save(data) {
		var reload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditProductPropertiesAjax/Update';
		var edit = {
			data: data,
			pageToken: jse.core.config.get('pageToken')
		};

		$.post(url, edit).done(function (response) {
			response = $.parseJSON(response);

			if (response.success) {
				$edit.find('input, select').not('.length, .select-page-mode').val('');
				$edit.find('select').not('.length, .select-page-mode').multi_select('refresh');

				if (reload) {
					$this.DataTable().ajax.reload();
				}

				return;
			}

			var title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
			var message = jse.core.lang.translate('EDIT_ERROR', 'admin_quick_edit');
			var buttons = [{
				title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
				callback: function callback(event) {
					return $(event.currentTarget).parents('.modal').modal('hide');
				}
			}];

			jse.libs.modal.showMessage(title, message, buttons);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('change', '.select-properties-shipping-time', _onTableRowShippingTimeChange).on('change', '.select-properties-price-type', _onTableRowPriceTypeChange);

		$edit.on('keyup', 'input:text', _onInputTextFilterKeyUp).on('click', '.apply-properties-edits', _onSaveClick);

		$edit.parents('.quick-edit.properties').on('keyup', 'td.editable', _onInputTextRowKeyUp);

		$edit.parents('.properties.modal').on('click', '.save-properties-edits', _onSaveClick).on('click', '.save-properties-bulk-row-edits', _onSaveClick);

		// Initialize the elements.
		$this.find('[data-single_select-instance]').each(function () {
			var $select = $(this);

			$select.removeAttr('data-single_select-instance');

			// Instantiate the widget without an AJAX request.
			$select.SumoSelect();
			$select[0].sumo.add('', '' + jse.core.lang.translate('TOOLTIP_NO_CHANGES', 'admin_quick_edit'), 0);
			$select[0].sumo.unSelectAll();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc19lZGl0LmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiRU5URVJfS0VZX0NPREUiLCIkdGhpcyIsIiQiLCIkZWRpdCIsImZpbmQiLCJiaW5kaW5ncyIsImVhY2giLCJjb2x1bW5OYW1lIiwiZmlyc3QiLCJfb25TYXZlQ2xpY2siLCIkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMiLCJlZGl0IiwibGVuZ3RoIiwidGl0bGUiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsIm1lc3NhZ2UiLCJidXR0b25zIiwiY2FsbGJhY2siLCJldmVudCIsImN1cnJlbnRUYXJnZXQiLCJwYXJlbnRzIiwibW9kYWwiLCJsaWJzIiwic2hvd01lc3NhZ2UiLCJ2YWx1ZSIsImdldCIsIiR0YWJsZVJvdyIsImlubGluZUVkaXQiLCIkY2VsbCIsImNsb3Nlc3QiLCJjb2x1bW5JbmRleCIsIkRhdGFUYWJsZSIsImNvbHVtbiIsImluZGV4IiwiZXEiLCJ2YWwiLCJhdHRyIiwicHJvcCIsInRyaWdnZXIiLCJfY2hlY2tGb3JNb2RpZmljYXRpb25zIiwiX3Jlc29sdmVSb3dDaGFuZ2VzIiwiX3NhdmUiLCJtb2RpZmljYXRpb25FeGlzdHMiLCJwcm9wZXJ0eSIsImhhc093blByb3BlcnR5IiwiaXNFbXB0eU9iamVjdCIsIiRyb3ciLCJyb3dJbmRleCIsInJvdyIsInBhcmVudEVsZW1lbnQiLCJpbm5lckhUTUwiLCJjZWxsIiwiX29uSW5wdXRUZXh0RmlsdGVyS2V5VXAiLCJ3aGljaCIsIl9vbklucHV0VGV4dFJvd0tleVVwIiwiX29uVGFibGVSb3dTaGlwcGluZ1RpbWVDaGFuZ2UiLCJwcm9wZXJ0aWVzSWQiLCJzaGlwcGluZ1RpbWVJZCIsIl9vblRhYmxlUm93UHJpY2VUeXBlQ2hhbmdlIiwicHJpY2VUeXBlIiwicmVsb2FkIiwidXJsIiwiY29uZmlnIiwicGFnZVRva2VuIiwicG9zdCIsImRvbmUiLCJyZXNwb25zZSIsInBhcnNlSlNPTiIsInN1Y2Nlc3MiLCJub3QiLCJtdWx0aV9zZWxlY3QiLCJhamF4IiwiaW5pdCIsIm9uIiwiJHNlbGVjdCIsInJlbW92ZUF0dHIiLCJTdW1vU2VsZWN0Iiwic3VtbyIsImFkZCIsInVuU2VsZWN0QWxsIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLGlCQURELEVBR0MsQ0FDQyxPQURELEVBRUlDLElBQUlDLE1BRlIsa0RBR0lELElBQUlDLE1BSFIsMkNBSEQsRUFTQyxVQUFTQyxJQUFULEVBQWU7O0FBRWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxpQkFBaUIsRUFBdkI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsUUFBUUYsTUFBTUcsSUFBTixDQUFXLG9CQUFYLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTVIsU0FBUyxFQUFDUyxVQUFVLEVBQVgsRUFBZjs7QUFFQTtBQUNBRixPQUFNQyxJQUFOLENBQVcsSUFBWCxFQUFpQkUsSUFBakIsQ0FBc0IsWUFBVztBQUNoQyxNQUFNQyxhQUFhTCxFQUFFLElBQUYsRUFBUUgsSUFBUixDQUFhLFlBQWIsQ0FBbkI7O0FBRUEsTUFBSVEsZUFBZSxVQUFmLElBQTZCQSxlQUFlLFNBQWhELEVBQTJEO0FBQzFELFVBQU8sSUFBUDtBQUNBOztBQUVEWCxTQUFPUyxRQUFQLENBQWdCRSxVQUFoQixJQUE4QkwsRUFBRSxJQUFGLEVBQVFFLElBQVIsQ0FBYSxlQUFiLEVBQThCSSxLQUE5QixFQUE5QjtBQUNBLEVBUkQ7O0FBVUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNDLFlBQVQsR0FBd0I7QUFDdkIsTUFBTUMsMkJBQTJCVCxNQUFNRyxJQUFOLENBQVcsaURBQVgsQ0FBakM7QUFDQSxNQUFNTyxPQUFPLEVBQWI7QUFDQSxNQUFNWixPQUFPLEVBQWI7O0FBRUEsTUFBSVcseUJBQXlCRSxNQUF6QixLQUFvQyxDQUF4QyxFQUEyQztBQUMxQyxPQUFNQyxRQUFRaEIsSUFBSWlCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGtCQUF4QixFQUE0QyxrQkFBNUMsQ0FBZDtBQUNBLE9BQU1DLFVBQVVwQixJQUFJaUIsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLGtCQUEvQyxDQUFoQjtBQUNBLE9BQU1FLFVBQVUsQ0FDZjtBQUNDTCxXQUFPaEIsSUFBSWlCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGNBQXhCLEVBQXdDLGtCQUF4QyxDQURSO0FBRUNHLGNBQVU7QUFBQSxZQUFTakIsRUFBRWtCLE1BQU1DLGFBQVIsRUFBdUJDLE9BQXZCLENBQStCLFFBQS9CLEVBQXlDQyxLQUF6QyxDQUErQyxNQUEvQyxDQUFUO0FBQUE7QUFGWCxJQURlLENBQWhCOztBQU9BMUIsT0FBSTJCLElBQUosQ0FBU0QsS0FBVCxDQUFlRSxXQUFmLENBQTJCWixLQUEzQixFQUFrQ0ksT0FBbEMsRUFBMkNDLE9BQTNDOztBQUVBO0FBQ0E7O0FBRURmLFFBQU1DLElBQU4sQ0FBVyxJQUFYLEVBQWlCRSxJQUFqQixDQUFzQixZQUFXO0FBQ2hDLE9BQU1DLGFBQWFMLEVBQUUsSUFBRixFQUFRSCxJQUFSLENBQWEsWUFBYixDQUFuQjs7QUFFQSxPQUFJUSxlQUFlLFVBQWYsSUFBNkJBLGVBQWUsU0FBaEQsRUFBMkQ7QUFDMUQsV0FBTyxJQUFQO0FBQ0E7O0FBRUQsT0FBSW1CLFFBQVE5QixPQUFPUyxRQUFQLENBQWdCRSxVQUFoQixFQUE0Qm9CLEdBQTVCLEVBQVo7O0FBRUEsT0FBSUQsS0FBSixFQUFXO0FBQ1ZmLFNBQUtKLFVBQUwsSUFBbUJtQixLQUFuQjtBQUNBO0FBQ0QsR0FaRDs7QUFjQWhCLDJCQUF5QkosSUFBekIsQ0FBOEIsWUFBVztBQUN4QyxPQUFNc0IsWUFBWTFCLEVBQUUsSUFBRixFQUFRb0IsT0FBUixDQUFnQixJQUFoQixDQUFsQjs7QUFFQSxPQUFJTSxVQUFVeEIsSUFBVixDQUFlLHFCQUFmLEVBQXNDUSxNQUF0QyxHQUErQyxDQUFuRCxFQUFzRDtBQUNyRCxRQUFJaUIsYUFBYSxFQUFqQjs7QUFFQUQsY0FBVXhCLElBQVYsQ0FBZSxxQkFBZixFQUFzQ0UsSUFBdEMsQ0FBMkMsWUFBVztBQUNyRCxTQUFNd0IsUUFBUTVCLEVBQUUsSUFBRixFQUFRNkIsT0FBUixDQUFnQixJQUFoQixDQUFkO0FBQ0EsU0FBTUMsY0FBYy9CLE1BQU1nQyxTQUFOLEdBQWtCQyxNQUFsQixDQUF5QkosS0FBekIsRUFBZ0NLLEtBQWhDLEVBQXBCO0FBQ0EsU0FBTTVCLGFBQWFKLE1BQU1DLElBQU4sQ0FBVyxJQUFYLEVBQWlCZ0MsRUFBakIsQ0FBb0JKLFdBQXBCLEVBQWlDakMsSUFBakMsQ0FBc0MsWUFBdEMsQ0FBbkI7O0FBRUE4QixnQkFBV3RCLFVBQVgsSUFBeUJMLEVBQUUsSUFBRixFQUFRbUMsR0FBUixFQUF6QjtBQUNBLEtBTkQ7O0FBUUF0QyxTQUFLNkIsVUFBVVUsSUFBVixDQUFlLElBQWYsQ0FBTCxJQUE2QlQsVUFBN0I7QUFDQSxJQVpELE1BWU87QUFDTjlCLFNBQUs2QixVQUFVVSxJQUFWLENBQWUsSUFBZixDQUFMLElBQTZCM0IsSUFBN0I7QUFDQTs7QUFFRFQsS0FBRSxJQUFGLEVBQVFxQyxJQUFSLENBQWEsU0FBYixFQUF3QixDQUFDckMsRUFBRSxJQUFGLEVBQ3ZCcUMsSUFEdUIsQ0FDbEIsU0FEa0IsQ0FBekIsRUFFRUMsT0FGRixDQUVVLFFBRlY7QUFHQSxHQXRCRDs7QUF3QkEsTUFBSUMsdUJBQXVCMUMsSUFBdkIsTUFBaUMsS0FBckMsRUFBNEM7QUFDM0NXLDRCQUF5QkosSUFBekIsQ0FBOEIsWUFBVztBQUN4Q29DLHVCQUFtQnhDLEVBQUUsSUFBRixFQUFRb0IsT0FBUixDQUFnQixJQUFoQixDQUFuQjtBQUNBLElBRkQ7O0FBSUE7QUFDQTs7QUFFRHFCLFFBQU01QyxJQUFOO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTMEMsc0JBQVQsQ0FBZ0MxQyxJQUFoQyxFQUFzQztBQUNyQyxNQUFJNkMscUJBQXFCLEtBQXpCOztBQUVBLE9BQUssSUFBSUMsUUFBVCxJQUFxQjlDLElBQXJCLEVBQTJCO0FBQzFCLE9BQUlBLEtBQUsrQyxjQUFMLENBQW9CRCxRQUFwQixDQUFKLEVBQW1DO0FBQ2xDLFFBQUksQ0FBQzNDLEVBQUU2QyxhQUFGLENBQWdCaEQsS0FBSzhDLFFBQUwsQ0FBaEIsQ0FBTCxFQUFzQztBQUNyQ0QsMEJBQXFCLElBQXJCO0FBQ0E7QUFDRDtBQUNEOztBQUVELFNBQU9BLGtCQUFQO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0Ysa0JBQVQsQ0FBNEJNLElBQTVCLEVBQWtDO0FBQ2pDLE1BQU1DLFdBQVdoRCxNQUFNZ0MsU0FBTixHQUFrQmlCLEdBQWxCLENBQXNCRixJQUF0QixFQUE0QmIsS0FBNUIsRUFBakI7O0FBRUFhLE9BQUs1QyxJQUFMLENBQVUsWUFBVixFQUF3QkUsSUFBeEIsQ0FBNkIsWUFBVztBQUN2QyxPQUFNd0IsUUFBUTVCLEVBQUUsSUFBRixFQUFRNkIsT0FBUixDQUFnQixJQUFoQixDQUFkO0FBQ0EsT0FBTUMsY0FBYy9CLE1BQU1nQyxTQUFOLEdBQWtCQyxNQUFsQixDQUF5QkosS0FBekIsRUFBZ0NLLEtBQWhDLEVBQXBCOztBQUVBLFFBQUtnQixhQUFMLENBQW1CQyxTQUFuQixHQUErQm5ELE1BQU1nQyxTQUFOLEdBQWtCb0IsSUFBbEIsQ0FBdUJKLFFBQXZCLEVBQWlDakIsV0FBakMsRUFBOENqQyxJQUE5QyxFQUEvQjtBQUNBLEdBTEQ7QUFNQTs7QUFFRDs7Ozs7QUFLQSxVQUFTdUQsdUJBQVQsQ0FBaUNsQyxLQUFqQyxFQUF3QztBQUN2QyxNQUFJQSxNQUFNbUMsS0FBTixLQUFnQnZELGNBQXBCLEVBQW9DO0FBQ25DRyxTQUFNQyxJQUFOLENBQVcseUJBQVgsRUFBc0NvQyxPQUF0QyxDQUE4QyxPQUE5QztBQUNBO0FBQ0Q7O0FBRUQ7Ozs7O0FBS0EsVUFBU2dCLG9CQUFULENBQThCcEMsS0FBOUIsRUFBcUM7QUFDcEMsTUFBSUEsTUFBTW1DLEtBQU4sS0FBZ0J2RCxjQUFwQixFQUFvQztBQUNuQ0csU0FBTW1CLE9BQU4sQ0FBYyxtQkFBZCxFQUFtQ2xCLElBQW5DLENBQXdDLGlDQUF4QyxFQUEyRW9DLE9BQTNFLENBQW1GLE9BQW5GO0FBQ0E7QUFDRDs7QUFFRDs7O0FBR0EsVUFBU2lCLDZCQUFULEdBQXlDO0FBQ3hDLE1BQU1DLGVBQWV4RCxFQUFFLElBQUYsRUFBUW9CLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JnQixJQUF0QixDQUEyQixJQUEzQixDQUFyQjtBQUNBLE1BQU1xQixpQkFBaUJ6RCxFQUFFLElBQUYsRUFBUW1DLEdBQVIsRUFBdkI7QUFDQSxNQUFNMUIsT0FBTyxFQUFiO0FBQ0EsTUFBTVosT0FBTyxFQUFiOztBQUVBWSxPQUFLLHlCQUFMLElBQWtDZ0QsY0FBbEM7QUFDQTVELE9BQUsyRCxZQUFMLElBQXFCL0MsSUFBckI7O0FBRUFnQyxRQUFNNUMsSUFBTixFQUFZLEtBQVo7QUFDQTs7QUFFRDs7O0FBR0EsVUFBUzZELDBCQUFULEdBQXNDO0FBQ3JDLE1BQU1GLGVBQWV4RCxFQUFFLElBQUYsRUFBUW9CLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JnQixJQUF0QixDQUEyQixJQUEzQixDQUFyQjtBQUNBLE1BQU11QixZQUFZM0QsRUFBRSxJQUFGLEVBQVFtQyxHQUFSLEVBQWxCO0FBQ0EsTUFBTTFCLE9BQU8sRUFBYjtBQUNBLE1BQU1aLE9BQU8sRUFBYjs7QUFFQVksT0FBSyxnQkFBTCxJQUF5QmtELFNBQXpCO0FBQ0E5RCxPQUFLMkQsWUFBTCxJQUFxQi9DLElBQXJCOztBQUVBZ0MsUUFBTTVDLElBQU4sRUFBWSxLQUFaO0FBQ0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVM0QyxLQUFULENBQWU1QyxJQUFmLEVBQW9DO0FBQUEsTUFBZitELE1BQWUsdUVBQU4sSUFBTTs7QUFDbkMsTUFBTUMsTUFBTWxFLElBQUlpQixJQUFKLENBQVNrRCxNQUFULENBQWdCckMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsMkRBQTVDO0FBQ0EsTUFBTWhCLE9BQU87QUFDWlosYUFEWTtBQUVaa0UsY0FBV3BFLElBQUlpQixJQUFKLENBQVNrRCxNQUFULENBQWdCckMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFGQyxHQUFiOztBQUtBekIsSUFBRWdFLElBQUYsQ0FBT0gsR0FBUCxFQUFZcEQsSUFBWixFQUNFd0QsSUFERixDQUNPLG9CQUFZO0FBQ2pCQyxjQUFXbEUsRUFBRW1FLFNBQUYsQ0FBWUQsUUFBWixDQUFYOztBQUVBLE9BQUlBLFNBQVNFLE9BQWIsRUFBc0I7QUFDckJuRSxVQUFNQyxJQUFOLENBQVcsZUFBWCxFQUE0Qm1FLEdBQTVCLENBQWdDLDRCQUFoQyxFQUE4RGxDLEdBQTlELENBQWtFLEVBQWxFO0FBQ0FsQyxVQUFNQyxJQUFOLENBQVcsUUFBWCxFQUFxQm1FLEdBQXJCLENBQXlCLDRCQUF6QixFQUF1REMsWUFBdkQsQ0FBb0UsU0FBcEU7O0FBRUEsUUFBSVYsTUFBSixFQUFZO0FBQ1g3RCxXQUFNZ0MsU0FBTixHQUFrQndDLElBQWxCLENBQXVCWCxNQUF2QjtBQUNBOztBQUVEO0FBQ0E7O0FBRUQsT0FBTWpELFFBQVFoQixJQUFJaUIsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isa0JBQXhCLEVBQTRDLGtCQUE1QyxDQUFkO0FBQ0EsT0FBTUMsVUFBVXBCLElBQUlpQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixZQUF4QixFQUFzQyxrQkFBdEMsQ0FBaEI7QUFDQSxPQUFNRSxVQUFVLENBQ2Y7QUFDQ0wsV0FBT2hCLElBQUlpQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxrQkFBeEMsQ0FEUjtBQUVDRyxjQUFVO0FBQUEsWUFBU2pCLEVBQUVrQixNQUFNQyxhQUFSLEVBQXVCQyxPQUF2QixDQUErQixRQUEvQixFQUF5Q0MsS0FBekMsQ0FBK0MsTUFBL0MsQ0FBVDtBQUFBO0FBRlgsSUFEZSxDQUFoQjs7QUFPQTFCLE9BQUkyQixJQUFKLENBQVNELEtBQVQsQ0FBZUUsV0FBZixDQUEyQlosS0FBM0IsRUFBa0NJLE9BQWxDLEVBQTJDQyxPQUEzQztBQUNBLEdBekJGO0FBMEJBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQXRCLFFBQU84RSxJQUFQLEdBQWMsVUFBU1AsSUFBVCxFQUFlO0FBQzVCbEUsUUFBTTBFLEVBQU4sQ0FBUyxRQUFULEVBQW1CLGtDQUFuQixFQUF1RGxCLDZCQUF2RCxFQUNFa0IsRUFERixDQUNLLFFBREwsRUFDZSwrQkFEZixFQUNnRGYsMEJBRGhEOztBQUdBekQsUUFDRXdFLEVBREYsQ0FDSyxPQURMLEVBQ2MsWUFEZCxFQUM0QnJCLHVCQUQ1QixFQUVFcUIsRUFGRixDQUVLLE9BRkwsRUFFYyx5QkFGZCxFQUV5Q2xFLFlBRnpDOztBQUlBTixRQUFNbUIsT0FBTixDQUFjLHdCQUFkLEVBQ0VxRCxFQURGLENBQ0ssT0FETCxFQUNjLGFBRGQsRUFDNkJuQixvQkFEN0I7O0FBR0FyRCxRQUFNbUIsT0FBTixDQUFjLG1CQUFkLEVBQ0VxRCxFQURGLENBQ0ssT0FETCxFQUNjLHdCQURkLEVBQ3dDbEUsWUFEeEMsRUFFRWtFLEVBRkYsQ0FFSyxPQUZMLEVBRWMsaUNBRmQsRUFFaURsRSxZQUZqRDs7QUFJQTtBQUNBUixRQUFNRyxJQUFOLENBQVcsK0JBQVgsRUFBNENFLElBQTVDLENBQWlELFlBQVc7QUFDM0QsT0FBTXNFLFVBQVUxRSxFQUFFLElBQUYsQ0FBaEI7O0FBRUEwRSxXQUFRQyxVQUFSLENBQW1CLDZCQUFuQjs7QUFFQTtBQUNBRCxXQUFRRSxVQUFSO0FBQ0FGLFdBQVEsQ0FBUixFQUFXRyxJQUFYLENBQWdCQyxHQUFoQixDQUFvQixFQUFwQixPQUEyQm5GLElBQUlpQixJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixvQkFBeEIsRUFBOEMsa0JBQTlDLENBQTNCLEVBQWdHLENBQWhHO0FBQ0E0RCxXQUFRLENBQVIsRUFBV0csSUFBWCxDQUFnQkUsV0FBaEI7QUFDQSxHQVREOztBQVdBZDtBQUNBLEVBNUJEOztBQThCQSxRQUFPdkUsTUFBUDtBQUNBLENBM1NEIiwiZmlsZSI6InF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc19lZGl0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBlZGl0LmpzIDIwMTYtMTAtMjVcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIFByb3BlcnRpZXMgVGFibGUgRWRpdGluZyBDb250cm9sbGVyXG4gKlxuICogSGFuZGxlcyB0aGUgZWRpdGluZyBmdW5jdGlvbmFsaXR5IG9mIHRoZSBwcm9wZXJ0aWVzIG1vZGFsIHRhYmxlLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdwcm9wZXJ0aWVzX2VkaXQnLCBcblx0XG5cdFtcblx0XHQnbW9kYWwnLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9zdW1vc2VsZWN0L2pxdWVyeS5zdW1vc2VsZWN0Lm1pbi5qc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL3N1bW9zZWxlY3Qvc3Vtb3NlbGVjdC5taW4uY3NzYFxuXHRdLCBcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBWQVJJQUJMRVNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogRW50ZXIgS2V5IENvZGVcblx0ICpcblx0ICogQHR5cGUge051bWJlcn1cblx0ICovXG5cdGNvbnN0IEVOVEVSX0tFWV9DT0RFID0gMTM7XG5cdFxuXHQvKipcblx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdCAqXG5cdCAqIEB0eXBlIHtqUXVlcnl9XG5cdCAqL1xuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFxuXHQvKipcblx0ICogRWRpdCBSb3cgU2VsZWN0b3Jcblx0ICpcblx0ICogQHR5cGUge2pRdWVyeX1cblx0ICovXG5cdGNvbnN0ICRlZGl0ID0gJHRoaXMuZmluZCgndHIucHJvcGVydGllcy1lZGl0Jyk7XG5cdFxuXHQvKipcblx0ICogTW9kdWxlIEluc3RhbmNlXG5cdCAqXG5cdCAqIEB0eXBlIHtPYmplY3R9XG5cdCAqL1xuXHRjb25zdCBtb2R1bGUgPSB7YmluZGluZ3M6IHt9fTtcblx0XG5cdC8vIER5bmFtaWNhbGx5IGRlZmluZSB0aGUgZWRpdCByb3cgZGF0YS1iaW5kaW5ncy5cblx0JGVkaXQuZmluZCgndGgnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdGNvbnN0IGNvbHVtbk5hbWUgPSAkKHRoaXMpLmRhdGEoJ2NvbHVtbk5hbWUnKTtcblx0XHRcblx0XHRpZiAoY29sdW1uTmFtZSA9PT0gJ2NoZWNrYm94JyB8fCBjb2x1bW5OYW1lID09PSAnYWN0aW9ucycpIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0XHRcblx0XHRtb2R1bGUuYmluZGluZ3NbY29sdW1uTmFtZV0gPSAkKHRoaXMpLmZpbmQoJ2lucHV0LCBzZWxlY3QnKS5maXJzdCgpO1xuXHR9KTtcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBGVU5DVElPTlNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogU2F2ZSBtb2RpZmljYXRpb25zIGV2ZW50IGhhbmRsZXIuXG5cdCAqXG5cdCAqIFRoaXMgbWV0aG9kIHdpbGwgbG9vayBmb3IgbW9kaWZpZWQgdmFsdWVzIGFuZCBzZW5kIHRoZW0gdG8gYmFjay1lbmQgd2l0aCBhbiBBSkFYIHJlcXVlc3QuXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25TYXZlQ2xpY2soKSB7XG5cdFx0Y29uc3QgJGNoZWNrZWRTaW5nbGVDaGVja2JveGVzID0gJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZC5wcm9wZXJ0aWVzLXJvdy1zZWxlY3Rpb24nKTtcblx0XHRjb25zdCBlZGl0ID0ge307XG5cdFx0Y29uc3QgZGF0YSA9IHt9O1xuXHRcdFxuXHRcdGlmICgkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMubGVuZ3RoID09PSAwKSB7XG5cdFx0XHRjb25zdCB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdNT0RBTF9USVRMRV9OT0RFJywgJ2FkbWluX3F1aWNrX2VkaXQnKTtcblx0XHRcdGNvbnN0IG1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnTk9fUFJPRFVDVF9TRUxFQ1RFRCcsICdhZG1pbl9xdWlja19lZGl0Jyk7XG5cdFx0XHRjb25zdCBidXR0b25zID0gW1xuXHRcdFx0XHR7XG5cdFx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fQ0xPU0UnLCAnYWRtaW5fcXVpY2tfZWRpdCcpLFxuXHRcdFx0XHRcdGNhbGxiYWNrOiBldmVudCA9PiAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLnBhcmVudHMoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJylcblx0XHRcdFx0fVxuXHRcdFx0XTtcblx0XHRcdFxuXHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UodGl0bGUsIG1lc3NhZ2UsIGJ1dHRvbnMpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXHRcdFxuXHRcdCRlZGl0LmZpbmQoJ3RoJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdGNvbnN0IGNvbHVtbk5hbWUgPSAkKHRoaXMpLmRhdGEoJ2NvbHVtbk5hbWUnKTtcblx0XHRcdFxuXHRcdFx0aWYgKGNvbHVtbk5hbWUgPT09ICdjaGVja2JveCcgfHwgY29sdW1uTmFtZSA9PT0gJ2FjdGlvbnMnKSB7XG5cdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRsZXQgdmFsdWUgPSBtb2R1bGUuYmluZGluZ3NbY29sdW1uTmFtZV0uZ2V0KCk7XG5cdFx0XHRcblx0XHRcdGlmICh2YWx1ZSkge1xuXHRcdFx0XHRlZGl0W2NvbHVtbk5hbWVdID0gdmFsdWU7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0XG5cdFx0JGNoZWNrZWRTaW5nbGVDaGVja2JveGVzLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRjb25zdCAkdGFibGVSb3cgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJyk7XG5cdFx0XHRcblx0XHRcdGlmICgkdGFibGVSb3cuZmluZCgnaW5wdXQ6dGV4dC5tb2RpZmllZCcpLmxlbmd0aCA+IDApIHtcblx0XHRcdFx0bGV0IGlubGluZUVkaXQgPSB7fTtcblx0XHRcdFx0XG5cdFx0XHRcdCR0YWJsZVJvdy5maW5kKCdpbnB1dDp0ZXh0Lm1vZGlmaWVkJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRjb25zdCAkY2VsbCA9ICQodGhpcykuY2xvc2VzdCgndGQnKTtcblx0XHRcdFx0XHRjb25zdCBjb2x1bW5JbmRleCA9ICR0aGlzLkRhdGFUYWJsZSgpLmNvbHVtbigkY2VsbCkuaW5kZXgoKTtcblx0XHRcdFx0XHRjb25zdCBjb2x1bW5OYW1lID0gJGVkaXQuZmluZCgndGgnKS5lcShjb2x1bW5JbmRleCkuZGF0YSgnY29sdW1uTmFtZScpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlubGluZUVkaXRbY29sdW1uTmFtZV0gPSAkKHRoaXMpLnZhbCgpO1xuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdGRhdGFbJHRhYmxlUm93LmF0dHIoJ2lkJyldID0gaW5saW5lRWRpdDtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdGRhdGFbJHRhYmxlUm93LmF0dHIoJ2lkJyldID0gZWRpdDtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JCh0aGlzKS5wcm9wKCdjaGVja2VkJywgISQodGhpcylcblx0XHRcdFx0LnByb3AoJ2NoZWNrZWQnKSlcblx0XHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScpO1xuXHRcdH0pO1xuXHRcdFxuXHRcdGlmIChfY2hlY2tGb3JNb2RpZmljYXRpb25zKGRhdGEpID09PSBmYWxzZSkge1xuXHRcdFx0JGNoZWNrZWRTaW5nbGVDaGVja2JveGVzLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdF9yZXNvbHZlUm93Q2hhbmdlcygkKHRoaXMpLnBhcmVudHMoJ3RyJykpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0XG5cdFx0X3NhdmUoZGF0YSk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBDaGVja3MgZm9yIHZhbHVlIG1vZGlmaWNhdGlvbnMuXG5cdCAqXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBkYXRhIENvbnRhaW5zIGN1cnJlbnQgcm93IGRhdGEuXG5cdCAqXG5cdCAqIEByZXR1cm4ge2Jvb2xlYW59IFJldHVybnMgd2hldGhlciBtb2RpZmljYXRpb25zIHdlcmUgbWFkZSBvciBub3QuXG5cdCAqL1xuXHRmdW5jdGlvbiBfY2hlY2tGb3JNb2RpZmljYXRpb25zKGRhdGEpIHtcblx0XHRsZXQgbW9kaWZpY2F0aW9uRXhpc3RzID0gZmFsc2U7XG5cdFx0XG5cdFx0Zm9yIChsZXQgcHJvcGVydHkgaW4gZGF0YSkge1xuXHRcdFx0aWYgKGRhdGEuaGFzT3duUHJvcGVydHkocHJvcGVydHkpKSB7XG5cdFx0XHRcdGlmICghJC5pc0VtcHR5T2JqZWN0KGRhdGFbcHJvcGVydHldKSkge1xuXHRcdFx0XHRcdG1vZGlmaWNhdGlvbkV4aXN0cyA9IHRydWU7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIG1vZGlmaWNhdGlvbkV4aXN0cztcblx0fVxuXHRcblx0LyoqXG5cdCAqIFJlc29sdmVzIHJvdyBjaGFuZ2VzLlxuXHQgKlxuXHQgKiBAcGFyYW0ge2pRdWVyeX0gJHJvdyBTZWxlY3RvciBvZiB0aGUgcm93IHRvIGJlIHJlc29sdmVkLlxuXHQgKi9cblx0ZnVuY3Rpb24gX3Jlc29sdmVSb3dDaGFuZ2VzKCRyb3cpIHtcblx0XHRjb25zdCByb3dJbmRleCA9ICR0aGlzLkRhdGFUYWJsZSgpLnJvdygkcm93KS5pbmRleCgpO1xuXHRcdFxuXHRcdCRyb3cuZmluZCgnaW5wdXQ6dGV4dCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRjb25zdCAkY2VsbCA9ICQodGhpcykuY2xvc2VzdCgndGQnKTtcblx0XHRcdGNvbnN0IGNvbHVtbkluZGV4ID0gJHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKCRjZWxsKS5pbmRleCgpO1xuXHRcdFx0XG5cdFx0XHR0aGlzLnBhcmVudEVsZW1lbnQuaW5uZXJIVE1MID0gJHRoaXMuRGF0YVRhYmxlKCkuY2VsbChyb3dJbmRleCwgY29sdW1uSW5kZXgpLmRhdGEoKTtcblx0XHR9KTtcblx0fVxuXHRcblx0LyoqXG5cdCAqIFRyaWdnZXIgZmlsdGVyaW5nIG9uY2UgdGhlIHVzZXIgcHJlc3NlcyB0aGUgZW50ZXIga2V5IGluc2lkZSBhIGZpbHRlciBpbnB1dC5cblx0ICpcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHQgKi9cblx0ZnVuY3Rpb24gX29uSW5wdXRUZXh0RmlsdGVyS2V5VXAoZXZlbnQpIHtcblx0XHRpZiAoZXZlbnQud2hpY2ggPT09IEVOVEVSX0tFWV9DT0RFKSB7XG5cdFx0XHQkZWRpdC5maW5kKCcuYXBwbHktcHJvcGVydGllcy1lZGl0cycpLnRyaWdnZXIoJ2NsaWNrJyk7XG5cdFx0fVxuXHR9XG5cdFxuXHQvKipcblx0ICogVHJpZ2dlciBtb2RpZmljYXRpb25zIHN1Ym1pdCBvbmNlIHRoZSB1c2VyIHByZXNzZXMgdGhlIGVudGVyIGtleSBpbnNpZGUgYSBlZGl0IGlucHV0LlxuXHQgKlxuXHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgQ29udGFpbnMgZXZlbnQgaW5mb3JtYXRpb24uXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25JbnB1dFRleHRSb3dLZXlVcChldmVudCkge1xuXHRcdGlmIChldmVudC53aGljaCA9PT0gRU5URVJfS0VZX0NPREUpIHtcblx0XHRcdCRlZGl0LnBhcmVudHMoJy5wcm9wZXJ0aWVzLm1vZGFsJykuZmluZCgnLnNhdmUtcHJvcGVydGllcy1idWxrLXJvdy1lZGl0cycpLnRyaWdnZXIoJ2NsaWNrJyk7XG5cdFx0fVxuXHR9XG5cdFxuXHQvKipcblx0ICogU3RvcmVzIHRoZSBjaGFuZ2Ugb2YgYSBzaGlwcGluZyB0aW1lIHZhbHVlLlxuXHQgKi9cblx0ZnVuY3Rpb24gX29uVGFibGVSb3dTaGlwcGluZ1RpbWVDaGFuZ2UoKSB7XG5cdFx0Y29uc3QgcHJvcGVydGllc0lkID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmF0dHIoJ2lkJyk7XG5cdFx0Y29uc3Qgc2hpcHBpbmdUaW1lSWQgPSAkKHRoaXMpLnZhbCgpO1xuXHRcdGNvbnN0IGVkaXQgPSB7fTtcblx0XHRjb25zdCBkYXRhID0ge307XG5cdFx0XG5cdFx0ZWRpdFsnY29tYmlTaGlwcGluZ1N0YXR1c05hbWUnXSA9IHNoaXBwaW5nVGltZUlkO1xuXHRcdGRhdGFbcHJvcGVydGllc0lkXSA9IGVkaXQ7XG5cdFx0XG5cdFx0X3NhdmUoZGF0YSwgZmFsc2UpO1xuXHR9XG5cdFxuXHQvKipcblx0ICogU3RvcmVzIHRoZSBjaGFuZ2Ugb2YgYSBwcmljZSB0eXBlIHZhbHVlLlxuXHQgKi9cblx0ZnVuY3Rpb24gX29uVGFibGVSb3dQcmljZVR5cGVDaGFuZ2UoKSB7XG5cdFx0Y29uc3QgcHJvcGVydGllc0lkID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmF0dHIoJ2lkJyk7XG5cdFx0Y29uc3QgcHJpY2VUeXBlID0gJCh0aGlzKS52YWwoKTtcblx0XHRjb25zdCBlZGl0ID0ge307XG5cdFx0Y29uc3QgZGF0YSA9IHt9O1xuXHRcdFxuXHRcdGVkaXRbJ2NvbWJpUHJpY2VUeXBlJ10gPSBwcmljZVR5cGU7XG5cdFx0ZGF0YVtwcm9wZXJ0aWVzSWRdID0gZWRpdDtcblx0XHRcblx0XHRfc2F2ZShkYXRhLCBmYWxzZSk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBTYXZlIG1vZGlmaWVkIGRhdGEgd2l0aCBhbiBBSkFYIHJlcXVlc3QuXG5cdCAqXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBkYXRhIENvbnRhaW5zIHRoZSB1cGRhdGVkIGRhdGEuXG5cdCAqIEBwYXJhbSB7Qm9vbGVhbn0gW3JlbG9hZD10cnVlXSBSZWxvYWQgdGhlIHBhZ2UgYWZ0ZXIgdGhlIHJlcXVlc3QgaXMgZG9uZS5cblx0ICovXG5cdGZ1bmN0aW9uIF9zYXZlKGRhdGEsIHJlbG9hZCA9IHRydWUpIHtcblx0XHRjb25zdCB1cmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVF1aWNrRWRpdFByb2R1Y3RQcm9wZXJ0aWVzQWpheC9VcGRhdGUnO1xuXHRcdGNvbnN0IGVkaXQgPSB7XG5cdFx0XHRkYXRhLFxuXHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxuXHRcdH07XG5cdFx0XG5cdFx0JC5wb3N0KHVybCwgZWRpdClcblx0XHRcdC5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0cmVzcG9uc2UgPSAkLnBhcnNlSlNPTihyZXNwb25zZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0XHRcdCRlZGl0LmZpbmQoJ2lucHV0LCBzZWxlY3QnKS5ub3QoJy5sZW5ndGgsIC5zZWxlY3QtcGFnZS1tb2RlJykudmFsKCcnKTtcblx0XHRcdFx0XHQkZWRpdC5maW5kKCdzZWxlY3QnKS5ub3QoJy5sZW5ndGgsIC5zZWxlY3QtcGFnZS1tb2RlJykubXVsdGlfc2VsZWN0KCdyZWZyZXNoJyk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKHJlbG9hZCkge1xuXHRcdFx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQoKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRjb25zdCB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdNT0RBTF9USVRMRV9OT0RFJywgJ2FkbWluX3F1aWNrX2VkaXQnKTtcblx0XHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdFRElUX0VSUk9SJywgJ2FkbWluX3F1aWNrX2VkaXQnKTtcblx0XHRcdFx0Y29uc3QgYnV0dG9ucyA9IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9DTE9TRScsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdFx0XHRjYWxsYmFjazogZXZlbnQgPT4gJChldmVudC5jdXJyZW50VGFyZ2V0KS5wYXJlbnRzKCcubW9kYWwnKS5tb2RhbCgnaGlkZScpXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdO1xuXHRcdFx0XHRcblx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UodGl0bGUsIG1lc3NhZ2UsIGJ1dHRvbnMpO1xuXHRcdFx0fSk7XG5cdH1cblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBJTklUSUFMSVpBVElPTlxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdCR0aGlzLm9uKCdjaGFuZ2UnLCAnLnNlbGVjdC1wcm9wZXJ0aWVzLXNoaXBwaW5nLXRpbWUnLCBfb25UYWJsZVJvd1NoaXBwaW5nVGltZUNoYW5nZSlcblx0XHRcdC5vbignY2hhbmdlJywgJy5zZWxlY3QtcHJvcGVydGllcy1wcmljZS10eXBlJywgX29uVGFibGVSb3dQcmljZVR5cGVDaGFuZ2UpO1xuXHRcdFxuXHRcdCRlZGl0XG5cdFx0XHQub24oJ2tleXVwJywgJ2lucHV0OnRleHQnLCBfb25JbnB1dFRleHRGaWx0ZXJLZXlVcClcblx0XHRcdC5vbignY2xpY2snLCAnLmFwcGx5LXByb3BlcnRpZXMtZWRpdHMnLCBfb25TYXZlQ2xpY2spO1xuXHRcdFxuXHRcdCRlZGl0LnBhcmVudHMoJy5xdWljay1lZGl0LnByb3BlcnRpZXMnKVxuXHRcdFx0Lm9uKCdrZXl1cCcsICd0ZC5lZGl0YWJsZScsIF9vbklucHV0VGV4dFJvd0tleVVwKTtcblx0XHRcblx0XHQkZWRpdC5wYXJlbnRzKCcucHJvcGVydGllcy5tb2RhbCcpXG5cdFx0XHQub24oJ2NsaWNrJywgJy5zYXZlLXByb3BlcnRpZXMtZWRpdHMnLCBfb25TYXZlQ2xpY2spXG5cdFx0XHQub24oJ2NsaWNrJywgJy5zYXZlLXByb3BlcnRpZXMtYnVsay1yb3ctZWRpdHMnLCBfb25TYXZlQ2xpY2spO1xuXHRcdFxuXHRcdC8vIEluaXRpYWxpemUgdGhlIGVsZW1lbnRzLlxuXHRcdCR0aGlzLmZpbmQoJ1tkYXRhLXNpbmdsZV9zZWxlY3QtaW5zdGFuY2VdJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdGNvbnN0ICRzZWxlY3QgPSAkKHRoaXMpO1xuXHRcdFx0XG5cdFx0XHQkc2VsZWN0LnJlbW92ZUF0dHIoJ2RhdGEtc2luZ2xlX3NlbGVjdC1pbnN0YW5jZScpO1xuXHRcdFx0XG5cdFx0XHQvLyBJbnN0YW50aWF0ZSB0aGUgd2lkZ2V0IHdpdGhvdXQgYW4gQUpBWCByZXF1ZXN0LlxuXHRcdFx0JHNlbGVjdC5TdW1vU2VsZWN0KCk7XG5cdFx0XHQkc2VsZWN0WzBdLnN1bW8uYWRkKCcnLCBgJHtqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVE9PTFRJUF9OT19DSEFOR0VTJywgJ2FkbWluX3F1aWNrX2VkaXQnKX1gLCAwKTtcblx0XHRcdCRzZWxlY3RbMF0uc3Vtby51blNlbGVjdEFsbCgpO1xuXHRcdH0pO1xuXHRcdFxuXHRcdGRvbmUoKTtcblx0fTtcblx0XG5cdHJldHVybiBtb2R1bGU7XG59KTsiXX0=
