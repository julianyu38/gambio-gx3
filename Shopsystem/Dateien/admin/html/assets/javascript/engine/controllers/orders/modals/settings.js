'use strict';

/* --------------------------------------------------------------
 settings.js 2016-10-04
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles the settings modal.
 *
 * It retrieves the settings data via the user configuration service and sets the values.
 * You are able to change the column sort order and the visibility of each column. Additionally
 * you can change the height of the table rows.
 */
gx.controllers.module('settings', ['user_configuration_service', 'loading_spinner', gx.source + '/libs/overview_settings_modal_controller'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		var SettingsModalController = jse.libs.overview_settings_modal_controller.class;

		var dependencies = {
			element: $this,
			userCfgService: jse.libs.user_configuration_service,
			loadingSpinner: jse.libs.loading_spinner,
			userId: data.userId,
			defaultColumnSettings: data.defaultColumnSettings,
			translator: jse.core.lang,
			page: 'orders'
		};

		var settingsModal = new SettingsModalController(dependencies.element, dependencies.userCfgService, dependencies.loadingSpinner, dependencies.userId, dependencies.defaultColumnSettings, dependencies.translator, dependencies.page);

		settingsModal.initialize();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvc2V0dGluZ3MuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJpbml0IiwiZG9uZSIsIlNldHRpbmdzTW9kYWxDb250cm9sbGVyIiwianNlIiwibGlicyIsIm92ZXJ2aWV3X3NldHRpbmdzX21vZGFsX2NvbnRyb2xsZXIiLCJjbGFzcyIsImRlcGVuZGVuY2llcyIsImVsZW1lbnQiLCJ1c2VyQ2ZnU2VydmljZSIsInVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlIiwibG9hZGluZ1NwaW5uZXIiLCJsb2FkaW5nX3NwaW5uZXIiLCJ1c2VySWQiLCJkZWZhdWx0Q29sdW1uU2V0dGluZ3MiLCJ0cmFuc2xhdG9yIiwiY29yZSIsImxhbmciLCJwYWdlIiwic2V0dGluZ3NNb2RhbCIsImluaXRpYWxpemUiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7OztBQU9BQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxVQURELEVBR0MsQ0FDQyw0QkFERCxFQUVDLGlCQUZELEVBR0lGLEdBQUdHLE1BSFAsOENBSEQsRUFTQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSixTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBQSxRQUFPSyxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCLE1BQU1DLDBCQUEwQkMsSUFBSUMsSUFBSixDQUFTQyxrQ0FBVCxDQUE0Q0MsS0FBNUU7O0FBRUEsTUFBTUMsZUFBZTtBQUNwQkMsWUFBU1YsS0FEVztBQUVwQlcsbUJBQWdCTixJQUFJQyxJQUFKLENBQVNNLDBCQUZMO0FBR3BCQyxtQkFBZ0JSLElBQUlDLElBQUosQ0FBU1EsZUFITDtBQUlwQkMsV0FBUWhCLEtBQUtnQixNQUpPO0FBS3BCQywwQkFBdUJqQixLQUFLaUIscUJBTFI7QUFNcEJDLGVBQVlaLElBQUlhLElBQUosQ0FBU0MsSUFORDtBQU9wQkMsU0FBTTtBQVBjLEdBQXJCOztBQVVBLE1BQU1DLGdCQUFnQixJQUFJakIsdUJBQUosQ0FDckJLLGFBQWFDLE9BRFEsRUFFckJELGFBQWFFLGNBRlEsRUFHckJGLGFBQWFJLGNBSFEsRUFJckJKLGFBQWFNLE1BSlEsRUFLckJOLGFBQWFPLHFCQUxRLEVBTXJCUCxhQUFhUSxVQU5RLEVBT3JCUixhQUFhVyxJQVBRLENBQXRCOztBQVVBQyxnQkFBY0MsVUFBZDs7QUFFQW5CO0FBQ0EsRUExQkQ7O0FBNEJBLFFBQU9OLE1BQVA7QUFDQSxDQWhFRiIsImZpbGUiOiJvcmRlcnMvbW9kYWxzL3NldHRpbmdzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzZXR0aW5ncy5qcyAyMDE2LTEwLTA0XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBIYW5kbGVzIHRoZSBzZXR0aW5ncyBtb2RhbC5cbiAqXG4gKiBJdCByZXRyaWV2ZXMgdGhlIHNldHRpbmdzIGRhdGEgdmlhIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZSBhbmQgc2V0cyB0aGUgdmFsdWVzLlxuICogWW91IGFyZSBhYmxlIHRvIGNoYW5nZSB0aGUgY29sdW1uIHNvcnQgb3JkZXIgYW5kIHRoZSB2aXNpYmlsaXR5IG9mIGVhY2ggY29sdW1uLiBBZGRpdGlvbmFsbHlcbiAqIHlvdSBjYW4gY2hhbmdlIHRoZSBoZWlnaHQgb2YgdGhlIHRhYmxlIHJvd3MuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3NldHRpbmdzJyxcblx0XG5cdFtcblx0XHQndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnLFxuXHRcdCdsb2FkaW5nX3NwaW5uZXInLFxuXHRcdGAke2d4LnNvdXJjZX0vbGlicy9vdmVydmlld19zZXR0aW5nc19tb2RhbF9jb250cm9sbGVyYFxuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRjb25zdCBTZXR0aW5nc01vZGFsQ29udHJvbGxlciA9IGpzZS5saWJzLm92ZXJ2aWV3X3NldHRpbmdzX21vZGFsX2NvbnRyb2xsZXIuY2xhc3M7XG5cdFx0XHRcblx0XHRcdGNvbnN0IGRlcGVuZGVuY2llcyA9IHtcblx0XHRcdFx0ZWxlbWVudDogJHRoaXMsXG5cdFx0XHRcdHVzZXJDZmdTZXJ2aWNlOiBqc2UubGlicy51c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSxcblx0XHRcdFx0bG9hZGluZ1NwaW5uZXI6IGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lcixcblx0XHRcdFx0dXNlcklkOiBkYXRhLnVzZXJJZCxcblx0XHRcdFx0ZGVmYXVsdENvbHVtblNldHRpbmdzOiBkYXRhLmRlZmF1bHRDb2x1bW5TZXR0aW5ncyxcblx0XHRcdFx0dHJhbnNsYXRvcjoganNlLmNvcmUubGFuZyxcblx0XHRcdFx0cGFnZTogJ29yZGVycydcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdGNvbnN0IHNldHRpbmdzTW9kYWwgPSBuZXcgU2V0dGluZ3NNb2RhbENvbnRyb2xsZXIoXG5cdFx0XHRcdGRlcGVuZGVuY2llcy5lbGVtZW50LFxuXHRcdFx0XHRkZXBlbmRlbmNpZXMudXNlckNmZ1NlcnZpY2UsXG5cdFx0XHRcdGRlcGVuZGVuY2llcy5sb2FkaW5nU3Bpbm5lcixcblx0XHRcdFx0ZGVwZW5kZW5jaWVzLnVzZXJJZCxcblx0XHRcdFx0ZGVwZW5kZW5jaWVzLmRlZmF1bHRDb2x1bW5TZXR0aW5ncyxcblx0XHRcdFx0ZGVwZW5kZW5jaWVzLnRyYW5zbGF0b3IsXG5cdFx0XHRcdGRlcGVuZGVuY2llcy5wYWdlXG5cdFx0XHQpO1xuXHRcdFx0XG5cdFx0XHRzZXR0aW5nc01vZGFsLmluaXRpYWxpemUoKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
