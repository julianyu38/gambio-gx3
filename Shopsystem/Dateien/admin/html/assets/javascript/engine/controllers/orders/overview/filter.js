'use strict';

/* --------------------------------------------------------------
 filter.js 2016-09-01
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles the orders table filtering.
 *
 * ### Methods
 *
 * **Reload Filtering Options**
 *
 * ```
 * // Reload the filter options with an AJAX request (optionally provide a second parameter for the AJAX URL).
 * $('.table-main').orders_overview_filter('reload');
 * ```
 */
gx.controllers.module('filter', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Enter Key Code
  *
  * @type {Number}
  */

	var ENTER_KEY_CODE = 13;

	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	var $this = $(this);

	/**
  * Filter Row Selector
  *
  * @type {jQuery}
  */
	var $filter = $this.find('tr.filter');

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = { bindings: {} };

	// Dynamically define the filter row data-bindings. 
	$filter.find('th').each(function () {
		var columnName = $(this).data('columnName');

		if (columnName === 'checkbox' || columnName === 'actions') {
			return true;
		}

		module.bindings[columnName] = $(this).find('input, select').first();
	});

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Reload filter options with an Ajax request.
  *
  * This function implements the $('.datatable').orders_overview_filter('reload') which will reload the filtering
  * "multi_select" instances will new options. It must be used after some table data are changed and the filtering
  * options need to be updated.
  *
  * @param {String} url Optional, the URL to be used for fetching the options. Do not add the "pageToken"
  * parameter to URL, it will be appended in this method.
  */
	function _reload(url) {
		url = url || jse.core.config.get('appUrl') + '/admin/admin.php?do=OrdersOverviewAjax/FilterOptions';
		var data = { pageToken: jse.core.config.get('pageToken') };

		$.getJSON(url, data).done(function (response) {
			for (var column in response) {
				var $select = $filter.find('.SumoSelect > select.' + column);
				var currentValueBackup = $select.val(); // Will try to set it back if it still exists. 

				if (!$select.length) {
					return; // The select element was not found.
				}

				$select.empty();

				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = response[column][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var option = _step.value;

						$select.append(new Option(option.text, option.value));
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				if (currentValueBackup !== null) {
					$select.val(currentValueBackup);
				}

				$select.multi_select('refresh');
			}
		});
	}

	/**
  * Add public "orders_overview_filter" method to jQuery in order.
  */
	function _addPublicMethod() {
		if ($.fn.orders_overview_filter) {
			return;
		}

		$.fn.extend({
			orders_overview_filter: function orders_overview_filter(action) {
				for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
					args[_key - 1] = arguments[_key];
				}

				$.each(this, function () {
					switch (action) {
						case 'reload':
							_reload.apply(this, args);
							break;
					}
				});
			}
		});
	}

	/**
  * On Filter Button Click
  *
  * Apply the provided filters and update the table rows.
  */
	function _onApplyFiltersClick() {
		// Prepare the object with the final filtering data.
		var filter = {};

		$filter.find('th').each(function () {
			var columnName = $(this).data('columnName');

			if (columnName === 'checkbox' || columnName === 'actions') {
				return true;
			}

			var value = module.bindings[columnName].get();

			if (value) {
				filter[columnName] = value;
				$this.DataTable().column(columnName + ':name').search(value);
			} else {
				$this.DataTable().column(columnName + ':name').search('');
			}
		});

		$this.trigger('orders_overview_filter:change', [filter]);
		$this.DataTable().draw();
	}

	/**
  * On Reset Button Click
  *
  * Reset the filter form and reload the table data without filtering.
  */
	function _onResetFiltersClick() {
		// Remove values from the input boxes.
		$filter.find('input, select').not('.length').val('');
		$filter.find('select').not('.length').multi_select('refresh');

		// Reset the filtering values.
		$this.DataTable().columns().search('').draw();

		// Trigger Event
		$this.trigger('orders_overview_filter:change', [{}]);
	}

	/**
  * Apply the filters when the user presses the Enter key.
  *
  * @param {jQuery.Event} event
  */
	function _onInputTextKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$filter.find('.apply-filters').trigger('click');
		}
	}

	/**
  * Parse the initial filtering parameters and apply them to the table.
  */
	function _parseFilteringParameters() {
		var _$$deparam = $.deparam(window.location.search.slice(1)),
		    filter = _$$deparam.filter;

		for (var name in filter) {
			var value = decodeURIComponent(filter[name]);

			if (module.bindings[name]) {
				module.bindings[name].set(value);
			}
		}
	}

	/**
  * Normalize array filtering values.
  *
  * By default datatables will concatenate array search values into a string separated with "," commas. This
  * is not acceptable though because some filtering elements may contain values with comma and thus the array
  * cannot be parsed from backend. This method will reset those cases back to arrays for a clearer transaction
  * with the backend.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {DataTables.Settings} settings DataTables settings object.
  * @param {Object} data Data that will be sent to the server in an object form.
  */
	function _normalizeArrayValues(event, settings, data) {
		var filter = {};

		for (var name in module.bindings) {
			var value = module.bindings[name].get();

			if (value && value.constructor === Array) {
				filter[name] = value;
			}
		}

		for (var entry in filter) {
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = data.columns[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var column = _step2.value;

					if (entry === column.name && filter[entry].constructor === Array) {
						column.search.value = filter[entry];
						break;
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Add public module method. 
		_addPublicMethod();

		// Parse filtering GET parameters. 
		_parseFilteringParameters();

		// Bind event handlers.
		$filter.on('keyup', 'input:text', _onInputTextKeyUp).on('click', '.apply-filters', _onApplyFiltersClick).on('click', '.reset-filters', _onResetFiltersClick);

		$this.on('preXhr.dt', _normalizeArrayValues);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vdmVydmlldy9maWx0ZXIuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCJFTlRFUl9LRVlfQ09ERSIsIiR0aGlzIiwiJCIsIiRmaWx0ZXIiLCJmaW5kIiwiYmluZGluZ3MiLCJlYWNoIiwiY29sdW1uTmFtZSIsImZpcnN0IiwiX3JlbG9hZCIsInVybCIsImNvcmUiLCJjb25maWciLCJnZXQiLCJwYWdlVG9rZW4iLCJnZXRKU09OIiwiZG9uZSIsInJlc3BvbnNlIiwiY29sdW1uIiwiJHNlbGVjdCIsImN1cnJlbnRWYWx1ZUJhY2t1cCIsInZhbCIsImxlbmd0aCIsImVtcHR5Iiwib3B0aW9uIiwiYXBwZW5kIiwiT3B0aW9uIiwidGV4dCIsInZhbHVlIiwibXVsdGlfc2VsZWN0IiwiX2FkZFB1YmxpY01ldGhvZCIsImZuIiwib3JkZXJzX292ZXJ2aWV3X2ZpbHRlciIsImV4dGVuZCIsImFjdGlvbiIsImFyZ3MiLCJhcHBseSIsIl9vbkFwcGx5RmlsdGVyc0NsaWNrIiwiZmlsdGVyIiwiRGF0YVRhYmxlIiwic2VhcmNoIiwidHJpZ2dlciIsImRyYXciLCJfb25SZXNldEZpbHRlcnNDbGljayIsIm5vdCIsImNvbHVtbnMiLCJfb25JbnB1dFRleHRLZXlVcCIsImV2ZW50Iiwid2hpY2giLCJfcGFyc2VGaWx0ZXJpbmdQYXJhbWV0ZXJzIiwiZGVwYXJhbSIsIndpbmRvdyIsImxvY2F0aW9uIiwic2xpY2UiLCJuYW1lIiwiZGVjb2RlVVJJQ29tcG9uZW50Iiwic2V0IiwiX25vcm1hbGl6ZUFycmF5VmFsdWVzIiwic2V0dGluZ3MiLCJjb25zdHJ1Y3RvciIsIkFycmF5IiwiZW50cnkiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7O0FBWUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFFBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLGtEQUhELEVBT0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsaUJBQWlCLEVBQXZCOztBQUVBOzs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVVGLE1BQU1HLElBQU4sQ0FBVyxXQUFYLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1SLFNBQVMsRUFBQ1MsVUFBVSxFQUFYLEVBQWY7O0FBRUE7QUFDQUYsU0FBUUMsSUFBUixDQUFhLElBQWIsRUFBbUJFLElBQW5CLENBQXdCLFlBQVc7QUFDbEMsTUFBTUMsYUFBYUwsRUFBRSxJQUFGLEVBQVFILElBQVIsQ0FBYSxZQUFiLENBQW5COztBQUVBLE1BQUlRLGVBQWUsVUFBZixJQUE2QkEsZUFBZSxTQUFoRCxFQUEyRDtBQUMxRCxVQUFPLElBQVA7QUFDQTs7QUFFRFgsU0FBT1MsUUFBUCxDQUFnQkUsVUFBaEIsSUFBOEJMLEVBQUUsSUFBRixFQUFRRSxJQUFSLENBQWEsZUFBYixFQUE4QkksS0FBOUIsRUFBOUI7QUFDQSxFQVJEOztBQVVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQVVBLFVBQVNDLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQ3JCQSxRQUFNQSxPQUFPYixJQUFJYyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHNEQUE3QztBQUNBLE1BQU1kLE9BQU8sRUFBQ2UsV0FBV2pCLElBQUljLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FBWixFQUFiOztBQUVBWCxJQUFFYSxPQUFGLENBQVVMLEdBQVYsRUFBZVgsSUFBZixFQUFxQmlCLElBQXJCLENBQTBCLFVBQUNDLFFBQUQsRUFBYztBQUN2QyxRQUFLLElBQUlDLE1BQVQsSUFBbUJELFFBQW5CLEVBQTZCO0FBQzVCLFFBQU1FLFVBQVVoQixRQUFRQyxJQUFSLENBQWEsMEJBQTBCYyxNQUF2QyxDQUFoQjtBQUNBLFFBQU1FLHFCQUFxQkQsUUFBUUUsR0FBUixFQUEzQixDQUY0QixDQUVjOztBQUUxQyxRQUFJLENBQUNGLFFBQVFHLE1BQWIsRUFBcUI7QUFDcEIsWUFEb0IsQ0FDWjtBQUNSOztBQUVESCxZQUFRSSxLQUFSOztBQVI0QjtBQUFBO0FBQUE7O0FBQUE7QUFVNUIsMEJBQW1CTixTQUFTQyxNQUFULENBQW5CLDhIQUFxQztBQUFBLFVBQTVCTSxNQUE0Qjs7QUFDcENMLGNBQVFNLE1BQVIsQ0FBZSxJQUFJQyxNQUFKLENBQVdGLE9BQU9HLElBQWxCLEVBQXdCSCxPQUFPSSxLQUEvQixDQUFmO0FBQ0E7QUFaMkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFjNUIsUUFBSVIsdUJBQXVCLElBQTNCLEVBQWlDO0FBQ2hDRCxhQUFRRSxHQUFSLENBQVlELGtCQUFaO0FBQ0E7O0FBRURELFlBQVFVLFlBQVIsQ0FBcUIsU0FBckI7QUFDQTtBQUNELEdBckJEO0FBc0JBOztBQUVEOzs7QUFHQSxVQUFTQyxnQkFBVCxHQUE0QjtBQUMzQixNQUFJNUIsRUFBRTZCLEVBQUYsQ0FBS0Msc0JBQVQsRUFBaUM7QUFDaEM7QUFDQTs7QUFFRDlCLElBQUU2QixFQUFGLENBQUtFLE1BQUwsQ0FBWTtBQUNYRCwyQkFBd0IsZ0NBQVNFLE1BQVQsRUFBMEI7QUFBQSxzQ0FBTkMsSUFBTTtBQUFOQSxTQUFNO0FBQUE7O0FBQ2pEakMsTUFBRUksSUFBRixDQUFPLElBQVAsRUFBYSxZQUFXO0FBQ3ZCLGFBQVE0QixNQUFSO0FBQ0MsV0FBSyxRQUFMO0FBQ0N6QixlQUFRMkIsS0FBUixDQUFjLElBQWQsRUFBb0JELElBQXBCO0FBQ0E7QUFIRjtBQUtBLEtBTkQ7QUFPQTtBQVRVLEdBQVo7QUFXQTs7QUFFRDs7Ozs7QUFLQSxVQUFTRSxvQkFBVCxHQUFnQztBQUMvQjtBQUNBLE1BQU1DLFNBQVMsRUFBZjs7QUFFQW5DLFVBQVFDLElBQVIsQ0FBYSxJQUFiLEVBQW1CRSxJQUFuQixDQUF3QixZQUFXO0FBQ2xDLE9BQU1DLGFBQWFMLEVBQUUsSUFBRixFQUFRSCxJQUFSLENBQWEsWUFBYixDQUFuQjs7QUFFQSxPQUFJUSxlQUFlLFVBQWYsSUFBNkJBLGVBQWUsU0FBaEQsRUFBMkQ7QUFDMUQsV0FBTyxJQUFQO0FBQ0E7O0FBRUQsT0FBSXFCLFFBQVFoQyxPQUFPUyxRQUFQLENBQWdCRSxVQUFoQixFQUE0Qk0sR0FBNUIsRUFBWjs7QUFFQSxPQUFJZSxLQUFKLEVBQVc7QUFDVlUsV0FBTy9CLFVBQVAsSUFBcUJxQixLQUFyQjtBQUNBM0IsVUFBTXNDLFNBQU4sR0FBa0JyQixNQUFsQixDQUE0QlgsVUFBNUIsWUFBK0NpQyxNQUEvQyxDQUFzRFosS0FBdEQ7QUFDQSxJQUhELE1BR087QUFDTjNCLFVBQU1zQyxTQUFOLEdBQWtCckIsTUFBbEIsQ0FBNEJYLFVBQTVCLFlBQStDaUMsTUFBL0MsQ0FBc0QsRUFBdEQ7QUFDQTtBQUNELEdBZkQ7O0FBaUJBdkMsUUFBTXdDLE9BQU4sQ0FBYywrQkFBZCxFQUErQyxDQUFDSCxNQUFELENBQS9DO0FBQ0FyQyxRQUFNc0MsU0FBTixHQUFrQkcsSUFBbEI7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxvQkFBVCxHQUFnQztBQUMvQjtBQUNBeEMsVUFBUUMsSUFBUixDQUFhLGVBQWIsRUFBOEJ3QyxHQUE5QixDQUFrQyxTQUFsQyxFQUE2Q3ZCLEdBQTdDLENBQWlELEVBQWpEO0FBQ0FsQixVQUFRQyxJQUFSLENBQWEsUUFBYixFQUF1QndDLEdBQXZCLENBQTJCLFNBQTNCLEVBQXNDZixZQUF0QyxDQUFtRCxTQUFuRDs7QUFFQTtBQUNBNUIsUUFBTXNDLFNBQU4sR0FBa0JNLE9BQWxCLEdBQTRCTCxNQUE1QixDQUFtQyxFQUFuQyxFQUF1Q0UsSUFBdkM7O0FBRUE7QUFDQXpDLFFBQU13QyxPQUFOLENBQWMsK0JBQWQsRUFBK0MsQ0FBQyxFQUFELENBQS9DO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0ssaUJBQVQsQ0FBMkJDLEtBQTNCLEVBQWtDO0FBQ2pDLE1BQUlBLE1BQU1DLEtBQU4sS0FBZ0JoRCxjQUFwQixFQUFvQztBQUNuQ0csV0FBUUMsSUFBUixDQUFhLGdCQUFiLEVBQStCcUMsT0FBL0IsQ0FBdUMsT0FBdkM7QUFDQTtBQUNEOztBQUVEOzs7QUFHQSxVQUFTUSx5QkFBVCxHQUFxQztBQUFBLG1CQUNuQi9DLEVBQUVnRCxPQUFGLENBQVVDLE9BQU9DLFFBQVAsQ0FBZ0JaLE1BQWhCLENBQXVCYSxLQUF2QixDQUE2QixDQUE3QixDQUFWLENBRG1CO0FBQUEsTUFDN0JmLE1BRDZCLGNBQzdCQSxNQUQ2Qjs7QUFHcEMsT0FBSyxJQUFJZ0IsSUFBVCxJQUFpQmhCLE1BQWpCLEVBQXlCO0FBQ3hCLE9BQU1WLFFBQVEyQixtQkFBbUJqQixPQUFPZ0IsSUFBUCxDQUFuQixDQUFkOztBQUVBLE9BQUkxRCxPQUFPUyxRQUFQLENBQWdCaUQsSUFBaEIsQ0FBSixFQUEyQjtBQUMxQjFELFdBQU9TLFFBQVAsQ0FBZ0JpRCxJQUFoQixFQUFzQkUsR0FBdEIsQ0FBMEI1QixLQUExQjtBQUNBO0FBQ0Q7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7O0FBWUEsVUFBUzZCLHFCQUFULENBQStCVixLQUEvQixFQUFzQ1csUUFBdEMsRUFBZ0QzRCxJQUFoRCxFQUFzRDtBQUNyRCxNQUFNdUMsU0FBUyxFQUFmOztBQUVBLE9BQUssSUFBSWdCLElBQVQsSUFBaUIxRCxPQUFPUyxRQUF4QixFQUFrQztBQUNqQyxPQUFNdUIsUUFBUWhDLE9BQU9TLFFBQVAsQ0FBZ0JpRCxJQUFoQixFQUFzQnpDLEdBQXRCLEVBQWQ7O0FBRUEsT0FBSWUsU0FBU0EsTUFBTStCLFdBQU4sS0FBc0JDLEtBQW5DLEVBQTBDO0FBQ3pDdEIsV0FBT2dCLElBQVAsSUFBZTFCLEtBQWY7QUFDQTtBQUNEOztBQUVELE9BQUssSUFBSWlDLEtBQVQsSUFBa0J2QixNQUFsQixFQUEwQjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUN6QiwwQkFBbUJ2QyxLQUFLOEMsT0FBeEIsbUlBQWlDO0FBQUEsU0FBeEIzQixNQUF3Qjs7QUFDaEMsU0FBSTJDLFVBQVUzQyxPQUFPb0MsSUFBakIsSUFBeUJoQixPQUFPdUIsS0FBUCxFQUFjRixXQUFkLEtBQThCQyxLQUEzRCxFQUFrRTtBQUNqRTFDLGFBQU9zQixNQUFQLENBQWNaLEtBQWQsR0FBc0JVLE9BQU91QixLQUFQLENBQXRCO0FBQ0E7QUFDQTtBQUNEO0FBTndCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPekI7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O0FBRUFqRSxRQUFPa0UsSUFBUCxHQUFjLFVBQVM5QyxJQUFULEVBQWU7QUFDNUI7QUFDQWM7O0FBRUE7QUFDQW1COztBQUVBO0FBQ0E5QyxVQUNFNEQsRUFERixDQUNLLE9BREwsRUFDYyxZQURkLEVBQzRCakIsaUJBRDVCLEVBRUVpQixFQUZGLENBRUssT0FGTCxFQUVjLGdCQUZkLEVBRWdDMUIsb0JBRmhDLEVBR0UwQixFQUhGLENBR0ssT0FITCxFQUdjLGdCQUhkLEVBR2dDcEIsb0JBSGhDOztBQUtBMUMsUUFBTThELEVBQU4sQ0FBUyxXQUFULEVBQXNCTixxQkFBdEI7O0FBRUF6QztBQUNBLEVBaEJEOztBQWtCQSxRQUFPcEIsTUFBUDtBQUNBLENBdFBGIiwiZmlsZSI6Im9yZGVycy9vdmVydmlldy9maWx0ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gZmlsdGVyLmpzIDIwMTYtMDktMDFcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogSGFuZGxlcyB0aGUgb3JkZXJzIHRhYmxlIGZpbHRlcmluZy5cclxuICpcclxuICogIyMjIE1ldGhvZHNcclxuICpcclxuICogKipSZWxvYWQgRmlsdGVyaW5nIE9wdGlvbnMqKlxyXG4gKlxyXG4gKiBgYGBcclxuICogLy8gUmVsb2FkIHRoZSBmaWx0ZXIgb3B0aW9ucyB3aXRoIGFuIEFKQVggcmVxdWVzdCAob3B0aW9uYWxseSBwcm92aWRlIGEgc2Vjb25kIHBhcmFtZXRlciBmb3IgdGhlIEFKQVggVVJMKS5cclxuICogJCgnLnRhYmxlLW1haW4nKS5vcmRlcnNfb3ZlcnZpZXdfZmlsdGVyKCdyZWxvYWQnKTtcclxuICogYGBgXHJcbiAqL1xyXG5neC5jb250cm9sbGVycy5tb2R1bGUoXHJcblx0J2ZpbHRlcicsXHJcblx0XHJcblx0W1xyXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS1kZXBhcmFtL2pxdWVyeS1kZXBhcmFtLm1pbi5qc2BcclxuXHRdLFxyXG5cdFxyXG5cdGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFxyXG5cdFx0J3VzZSBzdHJpY3QnO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIFZBUklBQkxFU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRW50ZXIgS2V5IENvZGVcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7TnVtYmVyfVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCBFTlRFUl9LRVlfQ09ERSA9IDEzO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBGaWx0ZXIgUm93IFNlbGVjdG9yXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgJGZpbHRlciA9ICR0aGlzLmZpbmQoJ3RyLmZpbHRlcicpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG1vZHVsZSA9IHtiaW5kaW5nczoge319O1xyXG5cdFx0XHJcblx0XHQvLyBEeW5hbWljYWxseSBkZWZpbmUgdGhlIGZpbHRlciByb3cgZGF0YS1iaW5kaW5ncy4gXHJcblx0XHQkZmlsdGVyLmZpbmQoJ3RoJykuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0Y29uc3QgY29sdW1uTmFtZSA9ICQodGhpcykuZGF0YSgnY29sdW1uTmFtZScpO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKGNvbHVtbk5hbWUgPT09ICdjaGVja2JveCcgfHwgY29sdW1uTmFtZSA9PT0gJ2FjdGlvbnMnKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdG1vZHVsZS5iaW5kaW5nc1tjb2x1bW5OYW1lXSA9ICQodGhpcykuZmluZCgnaW5wdXQsIHNlbGVjdCcpLmZpcnN0KCk7XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBGVU5DVElPTlNcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJlbG9hZCBmaWx0ZXIgb3B0aW9ucyB3aXRoIGFuIEFqYXggcmVxdWVzdC5cclxuXHRcdCAqXHJcblx0XHQgKiBUaGlzIGZ1bmN0aW9uIGltcGxlbWVudHMgdGhlICQoJy5kYXRhdGFibGUnKS5vcmRlcnNfb3ZlcnZpZXdfZmlsdGVyKCdyZWxvYWQnKSB3aGljaCB3aWxsIHJlbG9hZCB0aGUgZmlsdGVyaW5nXHJcblx0XHQgKiBcIm11bHRpX3NlbGVjdFwiIGluc3RhbmNlcyB3aWxsIG5ldyBvcHRpb25zLiBJdCBtdXN0IGJlIHVzZWQgYWZ0ZXIgc29tZSB0YWJsZSBkYXRhIGFyZSBjaGFuZ2VkIGFuZCB0aGUgZmlsdGVyaW5nXHJcblx0XHQgKiBvcHRpb25zIG5lZWQgdG8gYmUgdXBkYXRlZC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gdXJsIE9wdGlvbmFsLCB0aGUgVVJMIHRvIGJlIHVzZWQgZm9yIGZldGNoaW5nIHRoZSBvcHRpb25zLiBEbyBub3QgYWRkIHRoZSBcInBhZ2VUb2tlblwiXHJcblx0XHQgKiBwYXJhbWV0ZXIgdG8gVVJMLCBpdCB3aWxsIGJlIGFwcGVuZGVkIGluIHRoaXMgbWV0aG9kLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfcmVsb2FkKHVybCkge1xyXG5cdFx0XHR1cmwgPSB1cmwgfHwganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1PcmRlcnNPdmVydmlld0FqYXgvRmlsdGVyT3B0aW9ucyc7XHJcblx0XHRcdGNvbnN0IGRhdGEgPSB7cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKX07XHJcblx0XHRcdFxyXG5cdFx0XHQkLmdldEpTT04odXJsLCBkYXRhKS5kb25lKChyZXNwb25zZSkgPT4ge1xyXG5cdFx0XHRcdGZvciAobGV0IGNvbHVtbiBpbiByZXNwb25zZSkge1xyXG5cdFx0XHRcdFx0Y29uc3QgJHNlbGVjdCA9ICRmaWx0ZXIuZmluZCgnLlN1bW9TZWxlY3QgPiBzZWxlY3QuJyArIGNvbHVtbik7XHJcblx0XHRcdFx0XHRjb25zdCBjdXJyZW50VmFsdWVCYWNrdXAgPSAkc2VsZWN0LnZhbCgpOyAvLyBXaWxsIHRyeSB0byBzZXQgaXQgYmFjayBpZiBpdCBzdGlsbCBleGlzdHMuIFxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRpZiAoISRzZWxlY3QubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybjsgLy8gVGhlIHNlbGVjdCBlbGVtZW50IHdhcyBub3QgZm91bmQuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdCRzZWxlY3QuZW1wdHkoKTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0Zm9yIChsZXQgb3B0aW9uIG9mIHJlc3BvbnNlW2NvbHVtbl0pIHtcclxuXHRcdFx0XHRcdFx0JHNlbGVjdC5hcHBlbmQobmV3IE9wdGlvbihvcHRpb24udGV4dCwgb3B0aW9uLnZhbHVlKSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmIChjdXJyZW50VmFsdWVCYWNrdXAgIT09IG51bGwpIHtcclxuXHRcdFx0XHRcdFx0JHNlbGVjdC52YWwoY3VycmVudFZhbHVlQmFja3VwKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0JHNlbGVjdC5tdWx0aV9zZWxlY3QoJ3JlZnJlc2gnKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIEFkZCBwdWJsaWMgXCJvcmRlcnNfb3ZlcnZpZXdfZmlsdGVyXCIgbWV0aG9kIHRvIGpRdWVyeSBpbiBvcmRlci5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX2FkZFB1YmxpY01ldGhvZCgpIHtcclxuXHRcdFx0aWYgKCQuZm4ub3JkZXJzX292ZXJ2aWV3X2ZpbHRlcikge1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0JC5mbi5leHRlbmQoe1xyXG5cdFx0XHRcdG9yZGVyc19vdmVydmlld19maWx0ZXI6IGZ1bmN0aW9uKGFjdGlvbiwgLi4uYXJncykge1xyXG5cdFx0XHRcdFx0JC5lYWNoKHRoaXMsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRzd2l0Y2ggKGFjdGlvbikge1xyXG5cdFx0XHRcdFx0XHRcdGNhc2UgJ3JlbG9hZCc6XHJcblx0XHRcdFx0XHRcdFx0XHRfcmVsb2FkLmFwcGx5KHRoaXMsIGFyZ3MpO1xyXG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gRmlsdGVyIEJ1dHRvbiBDbGlja1xyXG5cdFx0ICpcclxuXHRcdCAqIEFwcGx5IHRoZSBwcm92aWRlZCBmaWx0ZXJzIGFuZCB1cGRhdGUgdGhlIHRhYmxlIHJvd3MuXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vbkFwcGx5RmlsdGVyc0NsaWNrKCkge1xyXG5cdFx0XHQvLyBQcmVwYXJlIHRoZSBvYmplY3Qgd2l0aCB0aGUgZmluYWwgZmlsdGVyaW5nIGRhdGEuXHJcblx0XHRcdGNvbnN0IGZpbHRlciA9IHt9O1xyXG5cdFx0XHRcclxuXHRcdFx0JGZpbHRlci5maW5kKCd0aCcpLmVhY2goZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0Y29uc3QgY29sdW1uTmFtZSA9ICQodGhpcykuZGF0YSgnY29sdW1uTmFtZScpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChjb2x1bW5OYW1lID09PSAnY2hlY2tib3gnIHx8IGNvbHVtbk5hbWUgPT09ICdhY3Rpb25zJykge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGxldCB2YWx1ZSA9IG1vZHVsZS5iaW5kaW5nc1tjb2x1bW5OYW1lXS5nZXQoKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAodmFsdWUpIHtcclxuXHRcdFx0XHRcdGZpbHRlcltjb2x1bW5OYW1lXSA9IHZhbHVlO1xyXG5cdFx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKGAke2NvbHVtbk5hbWV9Om5hbWVgKS5zZWFyY2godmFsdWUpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oYCR7Y29sdW1uTmFtZX06bmFtZWApLnNlYXJjaCgnJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdCR0aGlzLnRyaWdnZXIoJ29yZGVyc19vdmVydmlld19maWx0ZXI6Y2hhbmdlJywgW2ZpbHRlcl0pO1xyXG5cdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5kcmF3KCk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gUmVzZXQgQnV0dG9uIENsaWNrXHJcblx0XHQgKlxyXG5cdFx0ICogUmVzZXQgdGhlIGZpbHRlciBmb3JtIGFuZCByZWxvYWQgdGhlIHRhYmxlIGRhdGEgd2l0aG91dCBmaWx0ZXJpbmcuXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vblJlc2V0RmlsdGVyc0NsaWNrKCkge1xyXG5cdFx0XHQvLyBSZW1vdmUgdmFsdWVzIGZyb20gdGhlIGlucHV0IGJveGVzLlxyXG5cdFx0XHQkZmlsdGVyLmZpbmQoJ2lucHV0LCBzZWxlY3QnKS5ub3QoJy5sZW5ndGgnKS52YWwoJycpO1xyXG5cdFx0XHQkZmlsdGVyLmZpbmQoJ3NlbGVjdCcpLm5vdCgnLmxlbmd0aCcpLm11bHRpX3NlbGVjdCgncmVmcmVzaCcpO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gUmVzZXQgdGhlIGZpbHRlcmluZyB2YWx1ZXMuXHJcblx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmNvbHVtbnMoKS5zZWFyY2goJycpLmRyYXcoKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIFRyaWdnZXIgRXZlbnRcclxuXHRcdFx0JHRoaXMudHJpZ2dlcignb3JkZXJzX292ZXJ2aWV3X2ZpbHRlcjpjaGFuZ2UnLCBbe31dKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBBcHBseSB0aGUgZmlsdGVycyB3aGVuIHRoZSB1c2VyIHByZXNzZXMgdGhlIEVudGVyIGtleS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uSW5wdXRUZXh0S2V5VXAoZXZlbnQpIHtcclxuXHRcdFx0aWYgKGV2ZW50LndoaWNoID09PSBFTlRFUl9LRVlfQ09ERSkge1xyXG5cdFx0XHRcdCRmaWx0ZXIuZmluZCgnLmFwcGx5LWZpbHRlcnMnKS50cmlnZ2VyKCdjbGljaycpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogUGFyc2UgdGhlIGluaXRpYWwgZmlsdGVyaW5nIHBhcmFtZXRlcnMgYW5kIGFwcGx5IHRoZW0gdG8gdGhlIHRhYmxlLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfcGFyc2VGaWx0ZXJpbmdQYXJhbWV0ZXJzKCkge1xyXG5cdFx0XHRjb25zdCB7ZmlsdGVyfSA9ICQuZGVwYXJhbSh3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNsaWNlKDEpKTtcclxuXHRcdFx0XHJcblx0XHRcdGZvciAobGV0IG5hbWUgaW4gZmlsdGVyKSB7XHJcblx0XHRcdFx0Y29uc3QgdmFsdWUgPSBkZWNvZGVVUklDb21wb25lbnQoZmlsdGVyW25hbWVdKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAobW9kdWxlLmJpbmRpbmdzW25hbWVdKSB7XHJcblx0XHRcdFx0XHRtb2R1bGUuYmluZGluZ3NbbmFtZV0uc2V0KHZhbHVlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBOb3JtYWxpemUgYXJyYXkgZmlsdGVyaW5nIHZhbHVlcy5cclxuXHRcdCAqXHJcblx0XHQgKiBCeSBkZWZhdWx0IGRhdGF0YWJsZXMgd2lsbCBjb25jYXRlbmF0ZSBhcnJheSBzZWFyY2ggdmFsdWVzIGludG8gYSBzdHJpbmcgc2VwYXJhdGVkIHdpdGggXCIsXCIgY29tbWFzLiBUaGlzXHJcblx0XHQgKiBpcyBub3QgYWNjZXB0YWJsZSB0aG91Z2ggYmVjYXVzZSBzb21lIGZpbHRlcmluZyBlbGVtZW50cyBtYXkgY29udGFpbiB2YWx1ZXMgd2l0aCBjb21tYSBhbmQgdGh1cyB0aGUgYXJyYXlcclxuXHRcdCAqIGNhbm5vdCBiZSBwYXJzZWQgZnJvbSBiYWNrZW5kLiBUaGlzIG1ldGhvZCB3aWxsIHJlc2V0IHRob3NlIGNhc2VzIGJhY2sgdG8gYXJyYXlzIGZvciBhIGNsZWFyZXIgdHJhbnNhY3Rpb25cclxuXHRcdCAqIHdpdGggdGhlIGJhY2tlbmQuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXHJcblx0XHQgKiBAcGFyYW0ge0RhdGFUYWJsZXMuU2V0dGluZ3N9IHNldHRpbmdzIERhdGFUYWJsZXMgc2V0dGluZ3Mgb2JqZWN0LlxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGRhdGEgRGF0YSB0aGF0IHdpbGwgYmUgc2VudCB0byB0aGUgc2VydmVyIGluIGFuIG9iamVjdCBmb3JtLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfbm9ybWFsaXplQXJyYXlWYWx1ZXMoZXZlbnQsIHNldHRpbmdzLCBkYXRhKSB7XHJcblx0XHRcdGNvbnN0IGZpbHRlciA9IHt9O1xyXG5cdFx0XHRcclxuXHRcdFx0Zm9yIChsZXQgbmFtZSBpbiBtb2R1bGUuYmluZGluZ3MpIHtcclxuXHRcdFx0XHRjb25zdCB2YWx1ZSA9IG1vZHVsZS5iaW5kaW5nc1tuYW1lXS5nZXQoKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAodmFsdWUgJiYgdmFsdWUuY29uc3RydWN0b3IgPT09IEFycmF5KSB7XHJcblx0XHRcdFx0XHRmaWx0ZXJbbmFtZV0gPSB2YWx1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdGZvciAobGV0IGVudHJ5IGluIGZpbHRlcikge1xyXG5cdFx0XHRcdGZvciAobGV0IGNvbHVtbiBvZiBkYXRhLmNvbHVtbnMpIHtcclxuXHRcdFx0XHRcdGlmIChlbnRyeSA9PT0gY29sdW1uLm5hbWUgJiYgZmlsdGVyW2VudHJ5XS5jb25zdHJ1Y3RvciA9PT0gQXJyYXkpIHtcclxuXHRcdFx0XHRcdFx0Y29sdW1uLnNlYXJjaC52YWx1ZSA9IGZpbHRlcltlbnRyeV07XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdC8vIEFkZCBwdWJsaWMgbW9kdWxlIG1ldGhvZC4gXHJcblx0XHRcdF9hZGRQdWJsaWNNZXRob2QoKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIFBhcnNlIGZpbHRlcmluZyBHRVQgcGFyYW1ldGVycy4gXHJcblx0XHRcdF9wYXJzZUZpbHRlcmluZ1BhcmFtZXRlcnMoKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEJpbmQgZXZlbnQgaGFuZGxlcnMuXHJcblx0XHRcdCRmaWx0ZXJcclxuXHRcdFx0XHQub24oJ2tleXVwJywgJ2lucHV0OnRleHQnLCBfb25JbnB1dFRleHRLZXlVcClcclxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5hcHBseS1maWx0ZXJzJywgX29uQXBwbHlGaWx0ZXJzQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcucmVzZXQtZmlsdGVycycsIF9vblJlc2V0RmlsdGVyc0NsaWNrKTtcclxuXHRcdFx0XHJcblx0XHRcdCR0aGlzLm9uKCdwcmVYaHIuZHQnLCBfbm9ybWFsaXplQXJyYXlWYWx1ZXMpO1xyXG5cdFx0XHRcclxuXHRcdFx0ZG9uZSgpO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0cmV0dXJuIG1vZHVsZTtcclxuXHR9KTsiXX0=
