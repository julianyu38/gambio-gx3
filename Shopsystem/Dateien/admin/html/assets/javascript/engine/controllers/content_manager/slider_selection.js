'use strict';

/* --------------------------------------------------------------
 slider_selection.js 2017-11-27
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('slider_selection', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {
		contentSliderSelector: '.content-slider-selection',
		formSelector: 'form.content-manager-form',
		hiddenFieldSelector: 'input.content-manager-slider-id'
	};

	/**
  * Final Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Change handler for content slider selection.
  */
	var _selectionChanged = function _selectionChanged() {
		$(options.formSelector + ' ' + options.hiddenFieldSelector).val($this.find(options.contentSliderSelector).val());
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		_selectionChanged();

		$this.on('change', options.contentSliderSelector, _selectionChanged);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRfbWFuYWdlci9zbGlkZXJfc2VsZWN0aW9uLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJjb250ZW50U2xpZGVyU2VsZWN0b3IiLCJmb3JtU2VsZWN0b3IiLCJoaWRkZW5GaWVsZFNlbGVjdG9yIiwib3B0aW9ucyIsImV4dGVuZCIsIl9zZWxlY3Rpb25DaGFuZ2VkIiwidmFsIiwiZmluZCIsImluaXQiLCJkb25lIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msa0JBREQsRUFDcUIsRUFEckIsRUFHQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVDOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCQyx5QkFBdUIsMkJBRFA7QUFFaEJDLGdCQUFjLDJCQUZFO0FBR2hCQyx1QkFBcUI7QUFITCxFQUFqQjs7QUFNQTs7Ozs7QUFLQSxLQUFNQyxVQUFVTCxFQUFFTSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJMLFFBQW5CLEVBQTZCSCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRCxTQUFTLEVBQWY7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxLQUFNVSxvQkFBb0IsU0FBcEJBLGlCQUFvQixHQUFNO0FBQy9CUCxJQUFFSyxRQUFRRixZQUFSLEdBQXVCLEdBQXZCLEdBQTZCRSxRQUFRRCxtQkFBdkMsRUFBNERJLEdBQTVELENBQWdFVCxNQUFNVSxJQUFOLENBQVdKLFFBQVFILHFCQUFuQixFQUEwQ00sR0FBMUMsRUFBaEU7QUFDQSxFQUZEOztBQUlBO0FBQ0E7QUFDQTs7QUFFQVgsUUFBT2EsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1Qko7O0FBRUFSLFFBQU1hLEVBQU4sQ0FBUyxRQUFULEVBQW1CUCxRQUFRSCxxQkFBM0IsRUFBa0RLLGlCQUFsRDs7QUFFQUk7QUFDQSxFQU5EOztBQVFBLFFBQU9kLE1BQVA7QUFDQSxDQW5FRiIsImZpbGUiOiJjb250ZW50X21hbmFnZXIvc2xpZGVyX3NlbGVjdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gc2xpZGVyX3NlbGVjdGlvbi5qcyAyMDE3LTExLTI3XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnc2xpZGVyX3NlbGVjdGlvbicsIFtdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdFx0ICovXG5cdFx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0Y29uc3QgZGVmYXVsdHMgPSB7XG5cdFx0XHRcdGNvbnRlbnRTbGlkZXJTZWxlY3RvcjogJy5jb250ZW50LXNsaWRlci1zZWxlY3Rpb24nLFxuXHRcdFx0XHRmb3JtU2VsZWN0b3I6ICdmb3JtLmNvbnRlbnQtbWFuYWdlci1mb3JtJyxcblx0XHRcdFx0aGlkZGVuRmllbGRTZWxlY3RvcjogJ2lucHV0LmNvbnRlbnQtbWFuYWdlci1zbGlkZXItaWQnLFxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2hhbmdlIGhhbmRsZXIgZm9yIGNvbnRlbnQgc2xpZGVyIHNlbGVjdGlvbi5cblx0XHQgKi9cblx0XHRjb25zdCBfc2VsZWN0aW9uQ2hhbmdlZCA9ICgpID0+IHtcblx0XHRcdCQob3B0aW9ucy5mb3JtU2VsZWN0b3IgKyAnICcgKyBvcHRpb25zLmhpZGRlbkZpZWxkU2VsZWN0b3IpLnZhbCgkdGhpcy5maW5kKG9wdGlvbnMuY29udGVudFNsaWRlclNlbGVjdG9yKS52YWwoKSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0X3NlbGVjdGlvbkNoYW5nZWQoKTtcblx0XHRcdFxuXHRcdFx0JHRoaXMub24oJ2NoYW5nZScsIG9wdGlvbnMuY29udGVudFNsaWRlclNlbGVjdG9yLCBfc2VsZWN0aW9uQ2hhbmdlZCk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pOyJdfQ==
