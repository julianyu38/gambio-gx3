'use strict';

gx.controllers.module('overview',

// controller libraries
['xhr', gx.source + '/libs/info_messages', 'modal', gx.source + '/libs/info_box'],

// controller business logic
function (data) {
    'use strict';

    /**
     * Module Selector.
     *
     * @type {jQuery}
     */

    var $this = $(this);

    /**
     * Manufacturer creation modal.
     *
     * @type {*}
     */
    var $creationModal = $this.find('.creation-modal');

    /**
     * Manufacturer remove confirmation modal.
     *
     * @type {*}
     */
    var $removeConfirmationModal = $this.find('.remove-confirmation-modal');

    /**
     * Manufacturer edit modal.
     *
     * @type {*}
     */
    var $editModal = $this.find('.edit-modal');

    /**
     * Ajax object.
     *
     * @type {object}
     */
    var ajax = jse.libs.xhr;

    /**
     * Info box Object.
     *
     * @type {object}
     */
    var infoBox = jse.libs.info_box;

    /**
     * Language object.
     *
     * @type {object}
     */
    var lang = jse.core.lang;

    /**
     * Default options for controller,
     *
     * @type {object}
     */
    var defaults = {};

    /**
     * Final controller options.
     *
     * @type {object}
     */
    var options = $.extend(true, {}, defaults, data);

    /**
     * Module object.
     *
     * @type {{}}
     */
    var module = {};

    /**
     * Initializes the remove confirmation modal.
     *
     * @param eventObject Event object to fetch targets id.
     */
    var _initRemoveConfirmationModal = function _initRemoveConfirmationModal(eventObject) {
        ajax.get({
            url: './admin.php?do=ManufacturerAjax/getById&id=' + eventObject.target.dataset.id
        }).done(function (response) {
            $removeConfirmationModal.modal('show');
            _renderRemoveConfirmationModal(response);
        });
    };

    /**
     * Initializes the edit modal.
     *
     * @param eventObject Event object to fetch targets id.
     */
    var _initEditModal = function _initEditModal(eventObject) {
        ajax.get({
            url: './admin.php?do=ManufacturerAjax/getById&id=' + eventObject.target.dataset.id
        }).done(function (response) {
            $editModal.modal('show');
            _renderEditModal(response);
            if (response.image !== '') {
                _setImagePreview(response);
            }
        });
    };

    /**
     * Initializes the editing of an Manufacturer.
     *
     * @private
     */
    var _editManufacturer = function _editManufacturer() {
        if (_validateNameInput($editModal)) {
            _updateData();
        }
    };

    /**
     * Initializes the creation of an Manufacturer.
     *
     * @private
     */
    var _createManufacturer = function _createManufacturer() {
        if (_validateNameInput($creationModal)) {
            _storeData();
        }
    };

    /**
     * Initializes the cleaning process from create Modal.
     *
     * @private
     */
    var _cleanupCreationModal = function _cleanupCreationModal() {
        _truncateModalFields($creationModal);
        _truncateModalFieldClasses($creationModal);
        _resetModalMessages($creationModal);
    };

    /**
     * Initializes the cleaning process from edit Modal.
     *
     * @private
     */
    var _cleanupEditModal = function _cleanupEditModal() {
        _removeWarning();
        _truncateModalFields($editModal);
        _truncateModalFieldClasses($editModal);
        _resetModalMessages($editModal);
        _removeImagePreview();
        _removeImageCheckboxChecked();
        _removeImageCheckboxText();
        _removeImageCheckbox();
    };

    /**
     * Sends an ajax request to store a new manufacturer entity.
     *
     * @private
     */
    var _storeData = function _storeData() {
        ajax.post({
            url: './admin.php?do=ManufacturerAjax/save',
            data: _createInputData('create'),
            processData: false,
            contentType: false
        }).then(function (response) {
            if (response.success) {
                _renderTable();
                $creationModal.modal('hide');

                if (response.renamed) {
                    _createImageRenameInfo();
                } else {
                    infoBox.addSuccessMessage(lang.translate('TEXT_SAVE_SUCCESS', 'manufacturers'));
                }
            }
        });
    };

    /**
     * Sends an ajax request to edit an manufacturer entity.
     *
     * @private
     */
    var _updateData = function _updateData() {
        ajax.post({
            url: './admin.php?do=ManufacturerAjax/update',
            data: _createInputData('edit'),
            processData: false,
            contentType: false
        }).then(function (response) {
            if (response.success) {
                _renderTable();
                $editModal.modal('hide');

                $creationModal.modal('hide');
                if (response.renamed) {
                    _createImageRenameInfo();
                } else {
                    infoBox.addSuccessMessage(lang.translate('TEXT_EDIT_SUCCESS', 'manufacturers'));
                }
            }
        });
    };

    /**
     * Creates an info box with renamed image info.
     *
     * @private
     */
    var _createImageRenameInfo = function _createImageRenameInfo() {
        infoBox.addMessage({
            source: 'ajax',
            status: 'new',
            type: 'info',
            customerId: 1,
            visibility: 'removable',
            headline: lang.translate('TEXT_MANUFACTURERS_IMAGE', 'manufacturers'),
            message: lang.translate('TEXT_RENAMED_IMAGE', 'manufacturers')
        }).done(function () {
            return $('.info-box').trigger('refresh:messages');
        });
    };

    /**
     * Sends an ajax request to remove an manufacturer entity.
     *
     * @private
     */
    var _removeManufacturer = function _removeManufacturer() {
        ajax.post({
            url: './admin.php?do=ManufacturerAjax/remove',
            data: {
                id: $('.manufacturer-remove-id').val()
            }
        }).then(function (response) {
            if (response.success) {
                _renderTable();
                $removeConfirmationModal.modal('hide');
                infoBox.addSuccessMessage(lang.translate('TEXT_INFO_DELETE_SUCCESS', 'manufacturers'));
            }
        });
    };

    /**
     * Returns true if the name input not empty and false if empty.
     *
     * @param modal
     * @returns {boolean}
     * @private
     */
    var _validateNameInput = function _validateNameInput(modal) {
        var $nameInput = modal.find('input[name="manufacturer_name"]');

        if ($nameInput.val() === '') {
            $nameInput.parent().addClass('has-error');
            modal.find('p.manufacturer-modal-info').first().text(lang.translate('ERROR_MISSING_NAME', 'manufacturers')).addClass('text-danger');
            $('#input-manufacturers-name').focus();
            return false;
        }
        return true;
    };

    /**
     * Removes the has-error class from parent name input in modal.
     *
     * @param modal
     * @private
     */
    var _truncateModalFieldClasses = function _truncateModalFieldClasses(modal) {
        var $nameInput = modal.find('input[name="manufacturer_name"]');
        $nameInput.parent().removeClass('has-error');
    };

    /**
     * Removes the image delete checkbox text if one exists.
     *
     * @private
     */
    var _removeImageCheckboxText = function _removeImageCheckboxText() {
        if ($editModal.find('.checkbox-info-text')) {
            $editModal.find('.checkbox-info-text').remove('.checkbox-info-text');
        }
    };

    /**
     * Removes the image delete checkbox text if one exists.
     *
     * @private
     */
    var _removeImageCheckboxChecked = function _removeImageCheckboxChecked() {
        if ($editModal.find('.delete-image-checkbox').is(':checked')) {
            $editModal.find('.delete-image-checkbox').removeAttr('checked');
        }
    };
    /**
     * Removes the image delete checkbox text if one exists.
     *
     * @private
     */
    var _removeImagePreview = function _removeImagePreview() {
        if ($editModal.find('.manufacturer-image')) {
            $editModal.find('.manufacturer-image').remove();
        }
    };

    /**
     * Set the Checkbox type to hidden.
     *
     * @private
     */
    var _removeImageCheckbox = function _removeImageCheckbox() {
        if ($editModal.find('.single-checkbox')) {
            $editModal.find('.single-checkbox').remove();
        }
    };

    /**
     * Resets the info message from modal.
     *
     * @param modal
     * @private
     */
    var _resetModalMessages = function _resetModalMessages(modal) {
        modal.find('p.manufacturer-modal-info').first().text(lang.translate('TEXT_NEW_INTRO', 'manufacturers')).removeClass('text-danger');
    };

    /**
     * Truncate all input values from modal.
     *
     * @param modal
     * @private
     */
    var _truncateModalFields = function _truncateModalFields(modal) {
        modal.find('input').each(function (key, value) {
            value.value = '';
        });
    };

    /**
     * Renders overview table , to see changes immediately.
     *
     * @private
     */
    var _renderTable = function _renderTable() {
        ajax.get({
            url: './admin.php?do=ManufacturerAjax/getData'
        }).done(function (response) {
            var $body = $('.manufacturer-table tbody');
            $body.empty();

            for (var i = 0; i < response.length; i++) {
                var $row = $('<tr/>');
                var $nameColumn = $('<td/>', {
                    'text': response[i].name
                });
                var $actionsContainer = $('<div/>', {
                    'class': 'pull-right action-list visible-on-hover'
                });
                var $edit = $('<i/>', {
                    'data-id': response[i].id,
                    'data-toggle': 'modal',
                    'data-target': '.edit-modal',
                    'class': 'fa fa-pencil edit'
                });
                var $delete = $('<i/>', {
                    'data-id': response[i].id,
                    'data-toggle': 'modal',
                    'data-target': '.remove-confirmation-modal',
                    'class': 'fa fa-trash-o delete'
                });
                var $actionsCol = $('<td/>', {
                    'class': 'actions'
                });

                $actionsContainer.append($edit).append($delete).appendTo($actionsCol);
                $row.append($nameColumn).append($actionsCol).appendTo($body);
            }
            $this.find('.delete').on('click', _initRemoveConfirmationModal);
            $this.find('.edit').on('click', _initEditModal);
        });
    };

    /**
     * Renders remove confirmation modal with given data.
     *
     * @param response
     * @private
     */
    var _renderRemoveConfirmationModal = function _renderRemoveConfirmationModal(response) {
        var $info = $('.manufacturer-modal-remove-info');
        var $name = lang.translate('TEXT_MANUFACTURERS', 'manufacturers') + response.name + '<br><br>';

        $info.empty();
        $('.manufacturer-remove-id').val(response.id);
        $info.append($name);
    };

    /**
     * Renders edit modal with given data.
     *
     * @param response
     * @private
     */
    var _renderEditModal = function _renderEditModal(response) {
        $editModal.find('.manufacturer-id').val(response.id.toString());
        $editModal.find('#input-manufacturers-name').val(response.name);

        for (var languageCode in response.urls) {
            $editModal.find('.manufacturer-' + languageCode).val(response.urls[languageCode]);
        }

        if ($editModal.find('input[name="manufacturer_logo"]').attr('type') === 'text') {
            $editModal.find('input[name="manufacturer_logo"]').val(response.image);
        }

        if (response.image !== '') {
            _setDeleteImageCheckbox();
        }

        $editModal.find('.manufacturers-img-path').text(response.image);
        $editModal.find('.manufacturer-date-added').text('').append(response.dateAdded);

        $editModal.find('.manufacturer-last-modified').text('').append(response.lastModified);
    };

    /**
     * Sets an image preview in an manufacturer-image-container.
     *
     * @param response {{
     *                   imagePath : string
     *                   name      : string
     *                   image     : string
     *                 }}
     * @private
     */
    var _setImagePreview = function _setImagePreview(response) {
        var $manufacturerImage = $('<img/>', {
            'class': 'manufacturer-image ',
            'src': response.imagePath,
            'alt': response.name,
            'title': response.image
        });
        $editModal.find('.manufacturer-image-container').append($manufacturerImage);
    };

    /**
     * Sets the image delete Checkbox input.
     *
     * @private
     */
    var _setDeleteImageCheckbox = function _setDeleteImageCheckbox() {
        var $checkboxText = $('<span/>', {
            'class': 'checkbox-info-text',
            'text': ' ' + lang.translate('TEXT_DELETE_IMAGE', 'manufacturers')
        });
        var $checkboxInput = $('<input/>', {
            'class': 'delete-image-checkbox',
            'type': 'checkbox',
            'title': lang.translate('TEXT_DELETE_IMAGE', 'manufacturers')
        });

        if ($editModal.find('.single-checkbox')) {
            $editModal.find('.single-checkbox').remove();
        }
        $editModal.find('.manufacturer-image-data').append($checkboxInput).append($checkboxText).attr('data-gx-widget', 'single_checkbox');

        gx.widgets.init($editModal);
    };

    /**
     * Removes the warning text.
     *
     * @private
     */
    var _removeWarning = function _removeWarning() {
        $editModal.find('p.manufacturer-modal-info').text('').removeClass('text-danger');
    };

    /**
     * Creates and return data object from formula information's.
     *
     * @param modalType
     * @returns {{}}
     * @private
     */
    var _createInputData = function _createInputData(modalType) {
        var $modal = modalType === 'edit' ? $editModal : $creationModal;
        var $inputs = $modal.find('input[type="text"]');
        var $id = $modal.find('input[name="id"]').val();
        var $img = $modal.find('input[name="manufacturer_logo"]');
        var $imgPath = $img.val();
        var isFileManagerInput = $modal.find('.responsive-file-manager').length !== 0;
        var data = new FormData(document.querySelector('.manufacturer-form'));

        data.append('manufacturer_file', !isFileManagerInput);

        for (var i = 0; i < $inputs.length; i++) {
            data.append($inputs[i].getAttribute('name'), $inputs[i].value);
        }

        if (undefined !== $id) {
            data.append('id', $id);
        }

        if (!isFileManagerInput && undefined !== $img) {
            var file = $img[0].files[0] === null ? $imgPath : $img[0].files[0];
            if (file) {
                data.append('manufacturer_logo', file);
            }
        }

        if ($modal === $editModal) {
            data.append('manufacturer_checkbox', $editModal.find('.delete-image-checkbox').is(':checked'));

            if (data['manufacturer_logo'] === '' && $imgPath !== '') {
                data.append('manufacturer_logo', $imgPath);
            }
        }

        return data;
    };

    // event handler

    // initialization
    module.init = function (done) {
        // initialization logic

        $this.find('.delete').on('click', _initRemoveConfirmationModal);
        $creationModal.find('.btn-primary').on('click', _createManufacturer);
        $removeConfirmationModal.find('.btn-danger').on('click', _removeManufacturer);
        $editModal.find('.btn-primary').on('click', _editManufacturer);
        $this.find('.edit').on('click', _initEditModal);

        //actions
        $creationModal.on('hide.bs.modal', _cleanupCreationModal);
        $editModal.on('hide.bs.modal', _cleanupEditModal);

        done();
    };
    return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hbnVmYWN0dXJlci9vdmVydmlldy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRjcmVhdGlvbk1vZGFsIiwiZmluZCIsIiRyZW1vdmVDb25maXJtYXRpb25Nb2RhbCIsIiRlZGl0TW9kYWwiLCJhamF4IiwianNlIiwibGlicyIsInhociIsImluZm9Cb3giLCJpbmZvX2JveCIsImxhbmciLCJjb3JlIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwiX2luaXRSZW1vdmVDb25maXJtYXRpb25Nb2RhbCIsImdldCIsInVybCIsImV2ZW50T2JqZWN0IiwidGFyZ2V0IiwiZGF0YXNldCIsImlkIiwiZG9uZSIsIm1vZGFsIiwiX3JlbmRlclJlbW92ZUNvbmZpcm1hdGlvbk1vZGFsIiwicmVzcG9uc2UiLCJfaW5pdEVkaXRNb2RhbCIsIl9yZW5kZXJFZGl0TW9kYWwiLCJpbWFnZSIsIl9zZXRJbWFnZVByZXZpZXciLCJfZWRpdE1hbnVmYWN0dXJlciIsIl92YWxpZGF0ZU5hbWVJbnB1dCIsIl91cGRhdGVEYXRhIiwiX2NyZWF0ZU1hbnVmYWN0dXJlciIsIl9zdG9yZURhdGEiLCJfY2xlYW51cENyZWF0aW9uTW9kYWwiLCJfdHJ1bmNhdGVNb2RhbEZpZWxkcyIsIl90cnVuY2F0ZU1vZGFsRmllbGRDbGFzc2VzIiwiX3Jlc2V0TW9kYWxNZXNzYWdlcyIsIl9jbGVhbnVwRWRpdE1vZGFsIiwiX3JlbW92ZVdhcm5pbmciLCJfcmVtb3ZlSW1hZ2VQcmV2aWV3IiwiX3JlbW92ZUltYWdlQ2hlY2tib3hDaGVja2VkIiwiX3JlbW92ZUltYWdlQ2hlY2tib3hUZXh0IiwiX3JlbW92ZUltYWdlQ2hlY2tib3giLCJwb3N0IiwiX2NyZWF0ZUlucHV0RGF0YSIsInByb2Nlc3NEYXRhIiwiY29udGVudFR5cGUiLCJ0aGVuIiwic3VjY2VzcyIsIl9yZW5kZXJUYWJsZSIsInJlbmFtZWQiLCJfY3JlYXRlSW1hZ2VSZW5hbWVJbmZvIiwiYWRkU3VjY2Vzc01lc3NhZ2UiLCJ0cmFuc2xhdGUiLCJhZGRNZXNzYWdlIiwic3RhdHVzIiwidHlwZSIsImN1c3RvbWVySWQiLCJ2aXNpYmlsaXR5IiwiaGVhZGxpbmUiLCJtZXNzYWdlIiwidHJpZ2dlciIsIl9yZW1vdmVNYW51ZmFjdHVyZXIiLCJ2YWwiLCIkbmFtZUlucHV0IiwicGFyZW50IiwiYWRkQ2xhc3MiLCJmaXJzdCIsInRleHQiLCJmb2N1cyIsInJlbW92ZUNsYXNzIiwicmVtb3ZlIiwiaXMiLCJyZW1vdmVBdHRyIiwiZWFjaCIsImtleSIsInZhbHVlIiwiJGJvZHkiLCJlbXB0eSIsImkiLCJsZW5ndGgiLCIkcm93IiwiJG5hbWVDb2x1bW4iLCJuYW1lIiwiJGFjdGlvbnNDb250YWluZXIiLCIkZWRpdCIsIiRkZWxldGUiLCIkYWN0aW9uc0NvbCIsImFwcGVuZCIsImFwcGVuZFRvIiwib24iLCIkaW5mbyIsIiRuYW1lIiwidG9TdHJpbmciLCJsYW5ndWFnZUNvZGUiLCJ1cmxzIiwiYXR0ciIsIl9zZXREZWxldGVJbWFnZUNoZWNrYm94IiwiZGF0ZUFkZGVkIiwibGFzdE1vZGlmaWVkIiwiJG1hbnVmYWN0dXJlckltYWdlIiwiaW1hZ2VQYXRoIiwiJGNoZWNrYm94VGV4dCIsIiRjaGVja2JveElucHV0Iiwid2lkZ2V0cyIsImluaXQiLCIkbW9kYWwiLCJtb2RhbFR5cGUiLCIkaW5wdXRzIiwiJGlkIiwiJGltZyIsIiRpbWdQYXRoIiwiaXNGaWxlTWFuYWdlcklucHV0IiwiRm9ybURhdGEiLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3IiLCJnZXRBdHRyaWJ1dGUiLCJ1bmRlZmluZWQiLCJmaWxlIiwiZmlsZXMiXSwibWFwcGluZ3MiOiI7O0FBQUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNJLFVBREo7O0FBR0k7QUFDSCxDQUFDLEtBQUQsRUFBUUYsR0FBR0csTUFBSCxHQUFZLHFCQUFwQixFQUEyQyxPQUEzQyxFQUF1REgsR0FBR0csTUFBMUQsb0JBSkQ7O0FBTUk7QUFDQSxVQUFVQyxJQUFWLEVBQWdCO0FBQ1o7O0FBRUE7Ozs7OztBQUtBLFFBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLFFBQU1DLGlCQUFpQkYsTUFBTUcsSUFBTixDQUFXLGlCQUFYLENBQXZCOztBQUVBOzs7OztBQUtBLFFBQU1DLDJCQUEyQkosTUFBTUcsSUFBTixDQUFXLDRCQUFYLENBQWpDOztBQUVBOzs7OztBQUtBLFFBQU1FLGFBQWFMLE1BQU1HLElBQU4sQ0FBVyxhQUFYLENBQW5COztBQUVBOzs7OztBQUtBLFFBQU1HLE9BQU9DLElBQUlDLElBQUosQ0FBU0MsR0FBdEI7O0FBRUE7Ozs7O0FBS0EsUUFBTUMsVUFBVUgsSUFBSUMsSUFBSixDQUFTRyxRQUF6Qjs7QUFFQTs7Ozs7QUFLQSxRQUFNQyxPQUFPTCxJQUFJTSxJQUFKLENBQVNELElBQXRCOztBQUVBOzs7OztBQUtBLFFBQU1FLFdBQVcsRUFBakI7O0FBRUE7Ozs7O0FBS0EsUUFBTUMsVUFBVWQsRUFBRWUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QmYsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsUUFBTUYsU0FBUyxFQUFmOztBQUVBOzs7OztBQUtBLFFBQU1vQiwrQkFBK0IsU0FBL0JBLDRCQUErQixjQUFlO0FBQ2hEWCxhQUFLWSxHQUFMLENBQVM7QUFDTEMsaUJBQUssZ0RBQWdEQyxZQUFZQyxNQUFaLENBQW1CQyxPQUFuQixDQUEyQkM7QUFEM0UsU0FBVCxFQUVHQyxJQUZILENBRVEsb0JBQVk7QUFDaEJwQixxQ0FBeUJxQixLQUF6QixDQUErQixNQUEvQjtBQUNBQywyQ0FBK0JDLFFBQS9CO0FBQ0gsU0FMRDtBQU1ILEtBUEQ7O0FBU0E7Ozs7O0FBS0EsUUFBTUMsaUJBQWlCLFNBQWpCQSxjQUFpQixjQUFlO0FBQ2xDdEIsYUFBS1ksR0FBTCxDQUFTO0FBQ0xDLGlCQUFLLGdEQUFnREMsWUFBWUMsTUFBWixDQUFtQkMsT0FBbkIsQ0FBMkJDO0FBRDNFLFNBQVQsRUFFR0MsSUFGSCxDQUVRLG9CQUFZO0FBQ2hCbkIsdUJBQVdvQixLQUFYLENBQWlCLE1BQWpCO0FBQ0FJLDZCQUFpQkYsUUFBakI7QUFDQSxnQkFBSUEsU0FBU0csS0FBVCxLQUFtQixFQUF2QixFQUEyQjtBQUN2QkMsaUNBQWlCSixRQUFqQjtBQUNIO0FBQ0osU0FSRDtBQVNILEtBVkQ7O0FBWUE7Ozs7O0FBS0EsUUFBTUssb0JBQW9CLFNBQXBCQSxpQkFBb0IsR0FBTTtBQUM1QixZQUFJQyxtQkFBbUI1QixVQUFuQixDQUFKLEVBQW9DO0FBQ2hDNkI7QUFDSDtBQUNKLEtBSkQ7O0FBTUE7Ozs7O0FBS0EsUUFBTUMsc0JBQXNCLFNBQXRCQSxtQkFBc0IsR0FBTTtBQUM5QixZQUFJRixtQkFBbUIvQixjQUFuQixDQUFKLEVBQXdDO0FBQ3BDa0M7QUFDSDtBQUNKLEtBSkQ7O0FBTUE7Ozs7O0FBS0EsUUFBTUMsd0JBQXdCLFNBQXhCQSxxQkFBd0IsR0FBTTtBQUNoQ0MsNkJBQXFCcEMsY0FBckI7QUFDQXFDLG1DQUEyQnJDLGNBQTNCO0FBQ0FzQyw0QkFBb0J0QyxjQUFwQjtBQUNILEtBSkQ7O0FBTUE7Ozs7O0FBS0EsUUFBTXVDLG9CQUFvQixTQUFwQkEsaUJBQW9CLEdBQU07QUFDNUJDO0FBQ0FKLDZCQUFxQmpDLFVBQXJCO0FBQ0FrQyxtQ0FBMkJsQyxVQUEzQjtBQUNBbUMsNEJBQW9CbkMsVUFBcEI7QUFDQXNDO0FBQ0FDO0FBQ0FDO0FBQ0FDO0FBQ0gsS0FURDs7QUFXQTs7Ozs7QUFLQSxRQUFNVixhQUFhLFNBQWJBLFVBQWEsR0FBTTtBQUNyQjlCLGFBQUt5QyxJQUFMLENBQVU7QUFDTjVCLGlCQUFLLHNDQURDO0FBRU5wQixrQkFBTWlELGlCQUFpQixRQUFqQixDQUZBO0FBR05DLHlCQUFhLEtBSFA7QUFJTkMseUJBQWE7QUFKUCxTQUFWLEVBS0dDLElBTEgsQ0FLUSxvQkFBWTtBQUNoQixnQkFBSXhCLFNBQVN5QixPQUFiLEVBQXNCO0FBQ2xCQztBQUNBbkQsK0JBQWV1QixLQUFmLENBQXFCLE1BQXJCOztBQUVBLG9CQUFJRSxTQUFTMkIsT0FBYixFQUFzQjtBQUNsQkM7QUFDSCxpQkFGRCxNQUVPO0FBQ0g3Qyw0QkFBUThDLGlCQUFSLENBQTBCNUMsS0FBSzZDLFNBQUwsQ0FBZSxtQkFBZixFQUFvQyxlQUFwQyxDQUExQjtBQUNIO0FBQ0o7QUFDSixTQWhCRDtBQWlCSCxLQWxCRDs7QUFvQkE7Ozs7O0FBS0EsUUFBTXZCLGNBQWMsU0FBZEEsV0FBYyxHQUFNO0FBQ3RCNUIsYUFBS3lDLElBQUwsQ0FBVTtBQUNONUIsaUJBQUssd0NBREM7QUFFTnBCLGtCQUFNaUQsaUJBQWlCLE1BQWpCLENBRkE7QUFHTkMseUJBQWEsS0FIUDtBQUlOQyx5QkFBYTtBQUpQLFNBQVYsRUFLR0MsSUFMSCxDQUtRLG9CQUFZO0FBQ2hCLGdCQUFJeEIsU0FBU3lCLE9BQWIsRUFBc0I7QUFDbEJDO0FBQ0FoRCwyQkFBV29CLEtBQVgsQ0FBaUIsTUFBakI7O0FBRUF2QiwrQkFBZXVCLEtBQWYsQ0FBcUIsTUFBckI7QUFDQSxvQkFBSUUsU0FBUzJCLE9BQWIsRUFBc0I7QUFDbEJDO0FBQ0gsaUJBRkQsTUFFTztBQUNIN0MsNEJBQVE4QyxpQkFBUixDQUEwQjVDLEtBQUs2QyxTQUFMLENBQWUsbUJBQWYsRUFBb0MsZUFBcEMsQ0FBMUI7QUFDSDtBQUNKO0FBQ0osU0FqQkQ7QUFrQkgsS0FuQkQ7O0FBcUJBOzs7OztBQUtBLFFBQU1GLHlCQUF5QixTQUF6QkEsc0JBQXlCLEdBQU07QUFDakM3QyxnQkFBUWdELFVBQVIsQ0FBbUI7QUFDZjVELG9CQUFRLE1BRE87QUFFZjZELG9CQUFRLEtBRk87QUFHZkMsa0JBQU0sTUFIUztBQUlmQyx3QkFBWSxDQUpHO0FBS2ZDLHdCQUFZLFdBTEc7QUFNZkMsc0JBQVVuRCxLQUFLNkMsU0FBTCxDQUFlLDBCQUFmLEVBQTJDLGVBQTNDLENBTks7QUFPZk8scUJBQVNwRCxLQUFLNkMsU0FBTCxDQUFlLG9CQUFmLEVBQXFDLGVBQXJDO0FBUE0sU0FBbkIsRUFRR2pDLElBUkgsQ0FRUTtBQUFBLG1CQUFNdkIsRUFBRSxXQUFGLEVBQWVnRSxPQUFmLENBQXVCLGtCQUF2QixDQUFOO0FBQUEsU0FSUjtBQVNILEtBVkQ7O0FBWUE7Ozs7O0FBS0EsUUFBTUMsc0JBQXNCLFNBQXRCQSxtQkFBc0IsR0FBTTtBQUM5QjVELGFBQUt5QyxJQUFMLENBQVU7QUFDTjVCLGlCQUFLLHdDQURDO0FBRU5wQixrQkFBTTtBQUNGd0Isb0JBQUl0QixFQUFFLHlCQUFGLEVBQTZCa0UsR0FBN0I7QUFERjtBQUZBLFNBQVYsRUFLR2hCLElBTEgsQ0FLUSxvQkFBWTtBQUNoQixnQkFBSXhCLFNBQVN5QixPQUFiLEVBQXNCO0FBQ2xCQztBQUNBakQseUNBQXlCcUIsS0FBekIsQ0FBK0IsTUFBL0I7QUFDQWYsd0JBQVE4QyxpQkFBUixDQUEwQjVDLEtBQUs2QyxTQUFMLENBQWUsMEJBQWYsRUFBMkMsZUFBM0MsQ0FBMUI7QUFDSDtBQUNKLFNBWEQ7QUFZSCxLQWJEOztBQWVBOzs7Ozs7O0FBT0EsUUFBTXhCLHFCQUFxQixTQUFyQkEsa0JBQXFCLFFBQVM7QUFDaEMsWUFBTW1DLGFBQWEzQyxNQUFNdEIsSUFBTixDQUFXLGlDQUFYLENBQW5COztBQUVBLFlBQUlpRSxXQUFXRCxHQUFYLE9BQXFCLEVBQXpCLEVBQTZCO0FBQ3pCQyx1QkFBV0MsTUFBWCxHQUFvQkMsUUFBcEIsQ0FBNkIsV0FBN0I7QUFDQTdDLGtCQUFNdEIsSUFBTixDQUFXLDJCQUFYLEVBQ0tvRSxLQURMLEdBRUtDLElBRkwsQ0FFVTVELEtBQUs2QyxTQUFMLENBQWUsb0JBQWYsRUFBcUMsZUFBckMsQ0FGVixFQUdLYSxRQUhMLENBR2MsYUFIZDtBQUlBckUsY0FBRSwyQkFBRixFQUErQndFLEtBQS9CO0FBQ0EsbUJBQU8sS0FBUDtBQUNIO0FBQ0QsZUFBTyxJQUFQO0FBQ0gsS0FiRDs7QUFlQTs7Ozs7O0FBTUEsUUFBTWxDLDZCQUE2QixTQUE3QkEsMEJBQTZCLENBQUNkLEtBQUQsRUFBVztBQUMxQyxZQUFNMkMsYUFBYTNDLE1BQU10QixJQUFOLENBQVcsaUNBQVgsQ0FBbkI7QUFDQWlFLG1CQUFXQyxNQUFYLEdBQW9CSyxXQUFwQixDQUFnQyxXQUFoQztBQUNILEtBSEQ7O0FBS0E7Ozs7O0FBS0EsUUFBTTdCLDJCQUEyQixTQUEzQkEsd0JBQTJCLEdBQU07QUFDbkMsWUFBSXhDLFdBQVdGLElBQVgsQ0FBZ0IscUJBQWhCLENBQUosRUFBNEM7QUFDeENFLHVCQUFXRixJQUFYLENBQWdCLHFCQUFoQixFQUF1Q3dFLE1BQXZDLENBQThDLHFCQUE5QztBQUNIO0FBQ0osS0FKRDs7QUFNQTs7Ozs7QUFLQSxRQUFNL0IsOEJBQThCLFNBQTlCQSwyQkFBOEIsR0FBTTtBQUN0QyxZQUFJdkMsV0FBV0YsSUFBWCxDQUFnQix3QkFBaEIsRUFBMEN5RSxFQUExQyxDQUE2QyxVQUE3QyxDQUFKLEVBQThEO0FBQzFEdkUsdUJBQVdGLElBQVgsQ0FBZ0Isd0JBQWhCLEVBQTBDMEUsVUFBMUMsQ0FBcUQsU0FBckQ7QUFDSDtBQUNKLEtBSkQ7QUFLQTs7Ozs7QUFLQSxRQUFNbEMsc0JBQXNCLFNBQXRCQSxtQkFBc0IsR0FBTTtBQUM5QixZQUFJdEMsV0FBV0YsSUFBWCxDQUFnQixxQkFBaEIsQ0FBSixFQUE0QztBQUN4Q0UsdUJBQVdGLElBQVgsQ0FBZ0IscUJBQWhCLEVBQXVDd0UsTUFBdkM7QUFDSDtBQUNKLEtBSkQ7O0FBTUE7Ozs7O0FBS0EsUUFBTTdCLHVCQUF1QixTQUF2QkEsb0JBQXVCLEdBQU07QUFDL0IsWUFBSXpDLFdBQVdGLElBQVgsQ0FBZ0Isa0JBQWhCLENBQUosRUFBeUM7QUFDckNFLHVCQUFXRixJQUFYLENBQWdCLGtCQUFoQixFQUFvQ3dFLE1BQXBDO0FBQ0g7QUFDSixLQUpEOztBQU1BOzs7Ozs7QUFNQSxRQUFNbkMsc0JBQXNCLFNBQXRCQSxtQkFBc0IsQ0FBQ2YsS0FBRCxFQUFXO0FBQ25DQSxjQUFNdEIsSUFBTixDQUFXLDJCQUFYLEVBQ0tvRSxLQURMLEdBRUtDLElBRkwsQ0FFVTVELEtBQUs2QyxTQUFMLENBQWUsZ0JBQWYsRUFBaUMsZUFBakMsQ0FGVixFQUU2RGlCLFdBRjdELENBRXlFLGFBRnpFO0FBR0gsS0FKRDs7QUFNQTs7Ozs7O0FBTUEsUUFBTXBDLHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQUNiLEtBQUQsRUFBVztBQUNwQ0EsY0FBTXRCLElBQU4sQ0FBVyxPQUFYLEVBQW9CMkUsSUFBcEIsQ0FBeUIsVUFBQ0MsR0FBRCxFQUFNQyxLQUFOLEVBQWdCO0FBQ3JDQSxrQkFBTUEsS0FBTixHQUFjLEVBQWQ7QUFDSCxTQUZEO0FBR0gsS0FKRDs7QUFNQTs7Ozs7QUFLQSxRQUFNM0IsZUFBZSxTQUFmQSxZQUFlLEdBQU07QUFDdkIvQyxhQUFLWSxHQUFMLENBQVM7QUFDTEMsaUJBQUs7QUFEQSxTQUFULEVBRUdLLElBRkgsQ0FFUSxvQkFBWTtBQUNoQixnQkFBTXlELFFBQVFoRixFQUFFLDJCQUFGLENBQWQ7QUFDQWdGLGtCQUFNQyxLQUFOOztBQUVBLGlCQUFLLElBQUlDLElBQUksQ0FBYixFQUFnQkEsSUFBSXhELFNBQVN5RCxNQUE3QixFQUFxQ0QsR0FBckMsRUFBMEM7QUFDdEMsb0JBQU1FLE9BQU9wRixFQUFFLE9BQUYsQ0FBYjtBQUNBLG9CQUFNcUYsY0FBY3JGLEVBQUUsT0FBRixFQUFXO0FBQzNCLDRCQUFRMEIsU0FBU3dELENBQVQsRUFBWUk7QUFETyxpQkFBWCxDQUFwQjtBQUdBLG9CQUFNQyxvQkFBb0J2RixFQUFFLFFBQUYsRUFBWTtBQUNsQyw2QkFBUztBQUR5QixpQkFBWixDQUExQjtBQUdBLG9CQUFNd0YsUUFBUXhGLEVBQUUsTUFBRixFQUFVO0FBQ3BCLCtCQUFXMEIsU0FBU3dELENBQVQsRUFBWTVELEVBREg7QUFFcEIsbUNBQWUsT0FGSztBQUdwQixtQ0FBZSxhQUhLO0FBSXBCLDZCQUFTO0FBSlcsaUJBQVYsQ0FBZDtBQU1BLG9CQUFNbUUsVUFBVXpGLEVBQUUsTUFBRixFQUFVO0FBQ3RCLCtCQUFXMEIsU0FBU3dELENBQVQsRUFBWTVELEVBREQ7QUFFdEIsbUNBQWUsT0FGTztBQUd0QixtQ0FBZSw0QkFITztBQUl0Qiw2QkFBUztBQUphLGlCQUFWLENBQWhCO0FBTUEsb0JBQU1vRSxjQUFjMUYsRUFBRSxPQUFGLEVBQVc7QUFDM0IsNkJBQVM7QUFEa0IsaUJBQVgsQ0FBcEI7O0FBSUF1RixrQ0FBa0JJLE1BQWxCLENBQXlCSCxLQUF6QixFQUFnQ0csTUFBaEMsQ0FBdUNGLE9BQXZDLEVBQWdERyxRQUFoRCxDQUF5REYsV0FBekQ7QUFDQU4scUJBQUtPLE1BQUwsQ0FBWU4sV0FBWixFQUF5Qk0sTUFBekIsQ0FBZ0NELFdBQWhDLEVBQTZDRSxRQUE3QyxDQUFzRFosS0FBdEQ7QUFDSDtBQUNEakYsa0JBQU1HLElBQU4sQ0FBVyxTQUFYLEVBQXNCMkYsRUFBdEIsQ0FBeUIsT0FBekIsRUFBa0M3RSw0QkFBbEM7QUFDQWpCLGtCQUFNRyxJQUFOLENBQVcsT0FBWCxFQUFvQjJGLEVBQXBCLENBQXVCLE9BQXZCLEVBQWdDbEUsY0FBaEM7QUFDSCxTQW5DRDtBQW9DSCxLQXJDRDs7QUF1Q0E7Ozs7OztBQU1BLFFBQU1GLGlDQUFpQyxTQUFqQ0EsOEJBQWlDLENBQUNDLFFBQUQsRUFBYztBQUNqRCxZQUFNb0UsUUFBUTlGLEVBQUUsaUNBQUYsQ0FBZDtBQUNBLFlBQU0rRixRQUFRcEYsS0FBSzZDLFNBQUwsQ0FBZSxvQkFBZixFQUFxQyxlQUFyQyxJQUF3RDlCLFNBQVM0RCxJQUFqRSxHQUNSLFVBRE47O0FBR0FRLGNBQU1iLEtBQU47QUFDQWpGLFVBQUUseUJBQUYsRUFBNkJrRSxHQUE3QixDQUFpQ3hDLFNBQVNKLEVBQTFDO0FBQ0F3RSxjQUFNSCxNQUFOLENBQWFJLEtBQWI7QUFDSCxLQVJEOztBQVVBOzs7Ozs7QUFNQSxRQUFNbkUsbUJBQW1CLFNBQW5CQSxnQkFBbUIsV0FBWTtBQUNqQ3hCLG1CQUFXRixJQUFYLENBQWdCLGtCQUFoQixFQUFvQ2dFLEdBQXBDLENBQXdDeEMsU0FBU0osRUFBVCxDQUFZMEUsUUFBWixFQUF4QztBQUNBNUYsbUJBQVdGLElBQVgsQ0FBZ0IsMkJBQWhCLEVBQTZDZ0UsR0FBN0MsQ0FBaUR4QyxTQUFTNEQsSUFBMUQ7O0FBRUEsYUFBSyxJQUFJVyxZQUFULElBQXlCdkUsU0FBU3dFLElBQWxDLEVBQXdDO0FBQ3BDOUYsdUJBQVdGLElBQVgsQ0FBZ0IsbUJBQW1CK0YsWUFBbkMsRUFBaUQvQixHQUFqRCxDQUFxRHhDLFNBQVN3RSxJQUFULENBQWNELFlBQWQsQ0FBckQ7QUFDSDs7QUFFRCxZQUFJN0YsV0FBV0YsSUFBWCxDQUFnQixpQ0FBaEIsRUFBbURpRyxJQUFuRCxDQUF3RCxNQUF4RCxNQUFvRSxNQUF4RSxFQUFnRjtBQUM1RS9GLHVCQUFXRixJQUFYLENBQWdCLGlDQUFoQixFQUFtRGdFLEdBQW5ELENBQXVEeEMsU0FBU0csS0FBaEU7QUFDSDs7QUFFRCxZQUFJSCxTQUFTRyxLQUFULEtBQW1CLEVBQXZCLEVBQTJCO0FBQ3ZCdUU7QUFDSDs7QUFFRGhHLG1CQUFXRixJQUFYLENBQWdCLHlCQUFoQixFQUEyQ3FFLElBQTNDLENBQWdEN0MsU0FBU0csS0FBekQ7QUFDQXpCLG1CQUFXRixJQUFYLENBQWdCLDBCQUFoQixFQUNLcUUsSUFETCxDQUNVLEVBRFYsRUFFS29CLE1BRkwsQ0FFWWpFLFNBQVMyRSxTQUZyQjs7QUFJQWpHLG1CQUFXRixJQUFYLENBQWdCLDZCQUFoQixFQUNLcUUsSUFETCxDQUNVLEVBRFYsRUFFS29CLE1BRkwsQ0FFWWpFLFNBQVM0RSxZQUZyQjtBQUdILEtBeEJEOztBQTBCQTs7Ozs7Ozs7OztBQVVBLFFBQU14RSxtQkFBbUIsU0FBbkJBLGdCQUFtQixDQUFDSixRQUFELEVBQWM7QUFDbkMsWUFBTTZFLHFCQUFxQnZHLEVBQUUsUUFBRixFQUFZO0FBQ25DLHFCQUFTLHFCQUQwQjtBQUVuQyxtQkFBTzBCLFNBQVM4RSxTQUZtQjtBQUduQyxtQkFBTzlFLFNBQVM0RCxJQUhtQjtBQUluQyxxQkFBUzVELFNBQVNHO0FBSmlCLFNBQVosQ0FBM0I7QUFNQXpCLG1CQUFXRixJQUFYLENBQWdCLCtCQUFoQixFQUFpRHlGLE1BQWpELENBQXdEWSxrQkFBeEQ7QUFDSCxLQVJEOztBQVVBOzs7OztBQUtBLFFBQU1ILDBCQUEwQixTQUExQkEsdUJBQTBCLEdBQU07QUFDbEMsWUFBTUssZ0JBQWdCekcsRUFBRSxTQUFGLEVBQWE7QUFDL0IscUJBQVMsb0JBRHNCO0FBRS9CLG9CQUFRLE1BQU1XLEtBQUs2QyxTQUFMLENBQWUsbUJBQWYsRUFBb0MsZUFBcEM7QUFGaUIsU0FBYixDQUF0QjtBQUlBLFlBQU1rRCxpQkFBaUIxRyxFQUFFLFVBQUYsRUFBYztBQUNqQyxxQkFBUyx1QkFEd0I7QUFFakMsb0JBQVEsVUFGeUI7QUFHakMscUJBQVNXLEtBQUs2QyxTQUFMLENBQWUsbUJBQWYsRUFBb0MsZUFBcEM7QUFId0IsU0FBZCxDQUF2Qjs7QUFNQSxZQUFJcEQsV0FBV0YsSUFBWCxDQUFnQixrQkFBaEIsQ0FBSixFQUF5QztBQUNyQ0UsdUJBQVdGLElBQVgsQ0FBZ0Isa0JBQWhCLEVBQW9Dd0UsTUFBcEM7QUFFSDtBQUNEdEUsbUJBQVdGLElBQVgsQ0FBZ0IsMEJBQWhCLEVBQ0t5RixNQURMLENBQ1llLGNBRFosRUFFS2YsTUFGTCxDQUVZYyxhQUZaLEVBR0tOLElBSEwsQ0FHVSxnQkFIVixFQUc0QixpQkFINUI7O0FBS0F6RyxXQUFHaUgsT0FBSCxDQUFXQyxJQUFYLENBQWdCeEcsVUFBaEI7QUFDSCxLQXJCRDs7QUF1QkE7Ozs7O0FBS0EsUUFBTXFDLGlCQUFpQixTQUFqQkEsY0FBaUIsR0FBTTtBQUN6QnJDLG1CQUFXRixJQUFYLENBQWdCLDJCQUFoQixFQUNLcUUsSUFETCxDQUNVLEVBRFYsRUFFS0UsV0FGTCxDQUVpQixhQUZqQjtBQUdILEtBSkQ7O0FBT0E7Ozs7Ozs7QUFPQSxRQUFNMUIsbUJBQW1CLFNBQW5CQSxnQkFBbUIsWUFBYTtBQUNsQyxZQUFNOEQsU0FBU0MsY0FBYyxNQUFkLEdBQXVCMUcsVUFBdkIsR0FBb0NILGNBQW5EO0FBQ0EsWUFBTThHLFVBQVVGLE9BQU8zRyxJQUFQLENBQVksb0JBQVosQ0FBaEI7QUFDQSxZQUFNOEcsTUFBTUgsT0FBTzNHLElBQVAsQ0FBWSxrQkFBWixFQUFnQ2dFLEdBQWhDLEVBQVo7QUFDQSxZQUFNK0MsT0FBT0osT0FBTzNHLElBQVAsQ0FBWSxpQ0FBWixDQUFiO0FBQ0EsWUFBTWdILFdBQVdELEtBQUsvQyxHQUFMLEVBQWpCO0FBQ0EsWUFBTWlELHFCQUFxQk4sT0FBTzNHLElBQVAsQ0FBWSwwQkFBWixFQUF3Q2lGLE1BQXhDLEtBQW1ELENBQTlFO0FBQ0EsWUFBTXJGLE9BQU8sSUFBSXNILFFBQUosQ0FBYUMsU0FBU0MsYUFBVCxDQUF1QixvQkFBdkIsQ0FBYixDQUFiOztBQUVBeEgsYUFBSzZGLE1BQUwsQ0FBWSxtQkFBWixFQUFpQyxDQUFDd0Isa0JBQWxDOztBQUVBLGFBQUssSUFBSWpDLElBQUksQ0FBYixFQUFnQkEsSUFBSTZCLFFBQVE1QixNQUE1QixFQUFvQ0QsR0FBcEMsRUFBeUM7QUFDckNwRixpQkFBSzZGLE1BQUwsQ0FBWW9CLFFBQVE3QixDQUFSLEVBQVdxQyxZQUFYLENBQXdCLE1BQXhCLENBQVosRUFBNkNSLFFBQVE3QixDQUFSLEVBQVdILEtBQXhEO0FBQ0g7O0FBRUQsWUFBSXlDLGNBQWNSLEdBQWxCLEVBQXVCO0FBQ25CbEgsaUJBQUs2RixNQUFMLENBQVksSUFBWixFQUFrQnFCLEdBQWxCO0FBQ0g7O0FBRUQsWUFBSSxDQUFDRyxrQkFBRCxJQUF1QkssY0FBY1AsSUFBekMsRUFBK0M7QUFDM0MsZ0JBQU1RLE9BQU9SLEtBQUssQ0FBTCxFQUFRUyxLQUFSLENBQWMsQ0FBZCxNQUFxQixJQUFyQixHQUE0QlIsUUFBNUIsR0FBdUNELEtBQUssQ0FBTCxFQUFRUyxLQUFSLENBQWMsQ0FBZCxDQUFwRDtBQUNBLGdCQUFJRCxJQUFKLEVBQVU7QUFDTjNILHFCQUFLNkYsTUFBTCxDQUFZLG1CQUFaLEVBQWlDOEIsSUFBakM7QUFDSDtBQUNKOztBQUVELFlBQUlaLFdBQVd6RyxVQUFmLEVBQTJCO0FBQ3ZCTixpQkFBSzZGLE1BQUwsQ0FBWSx1QkFBWixFQUFxQ3ZGLFdBQVdGLElBQVgsQ0FBZ0Isd0JBQWhCLEVBQTBDeUUsRUFBMUMsQ0FBNkMsVUFBN0MsQ0FBckM7O0FBRUEsZ0JBQUk3RSxLQUFLLG1CQUFMLE1BQThCLEVBQTlCLElBQW9Db0gsYUFBYSxFQUFyRCxFQUF5RDtBQUNyRHBILHFCQUFLNkYsTUFBTCxDQUFZLG1CQUFaLEVBQWlDdUIsUUFBakM7QUFDSDtBQUNKOztBQUVELGVBQU9wSCxJQUFQO0FBQ0gsS0FuQ0Q7O0FBcUNBOztBQUVBO0FBQ0FGLFdBQU9nSCxJQUFQLEdBQWMsZ0JBQVE7QUFDbEI7O0FBRUE3RyxjQUFNRyxJQUFOLENBQVcsU0FBWCxFQUFzQjJGLEVBQXRCLENBQXlCLE9BQXpCLEVBQWtDN0UsNEJBQWxDO0FBQ0FmLHVCQUFlQyxJQUFmLENBQW9CLGNBQXBCLEVBQW9DMkYsRUFBcEMsQ0FBdUMsT0FBdkMsRUFBZ0QzRCxtQkFBaEQ7QUFDQS9CLGlDQUF5QkQsSUFBekIsQ0FBOEIsYUFBOUIsRUFBNkMyRixFQUE3QyxDQUFnRCxPQUFoRCxFQUF5RDVCLG1CQUF6RDtBQUNBN0QsbUJBQVdGLElBQVgsQ0FBZ0IsY0FBaEIsRUFBZ0MyRixFQUFoQyxDQUFtQyxPQUFuQyxFQUE0QzlELGlCQUE1QztBQUNBaEMsY0FBTUcsSUFBTixDQUFXLE9BQVgsRUFBb0IyRixFQUFwQixDQUF1QixPQUF2QixFQUFnQ2xFLGNBQWhDOztBQUVBO0FBQ0ExQix1QkFBZTRGLEVBQWYsQ0FBa0IsZUFBbEIsRUFBbUN6RCxxQkFBbkM7QUFDQWhDLG1CQUFXeUYsRUFBWCxDQUFjLGVBQWQsRUFBK0JyRCxpQkFBL0I7O0FBRUFqQjtBQUNILEtBZEQ7QUFlQSxXQUFPM0IsTUFBUDtBQUNILENBbmpCTCIsImZpbGUiOiJtYW51ZmFjdHVyZXIvb3ZlcnZpZXcuanMiLCJzb3VyY2VzQ29udGVudCI6WyJneC5jb250cm9sbGVycy5tb2R1bGUoXG4gICAgJ292ZXJ2aWV3JyxcblxuICAgIC8vIGNvbnRyb2xsZXIgbGlicmFyaWVzXG5cdFsneGhyJywgZ3guc291cmNlICsgJy9saWJzL2luZm9fbWVzc2FnZXMnLCAnbW9kYWwnLCBgJHtneC5zb3VyY2V9L2xpYnMvaW5mb19ib3hgXSxcblxuICAgIC8vIGNvbnRyb2xsZXIgYnVzaW5lc3MgbG9naWNcbiAgICBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAndXNlIHN0cmljdCc7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIE1vZHVsZSBTZWxlY3Rvci5cbiAgICAgICAgICpcbiAgICAgICAgICogQHR5cGUge2pRdWVyeX1cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogTWFudWZhY3R1cmVyIGNyZWF0aW9uIG1vZGFsLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAdHlwZSB7Kn1cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0ICRjcmVhdGlvbk1vZGFsID0gJHRoaXMuZmluZCgnLmNyZWF0aW9uLW1vZGFsJyk7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIE1hbnVmYWN0dXJlciByZW1vdmUgY29uZmlybWF0aW9uIG1vZGFsLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAdHlwZSB7Kn1cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0ICRyZW1vdmVDb25maXJtYXRpb25Nb2RhbCA9ICR0aGlzLmZpbmQoJy5yZW1vdmUtY29uZmlybWF0aW9uLW1vZGFsJyk7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIE1hbnVmYWN0dXJlciBlZGl0IG1vZGFsLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAdHlwZSB7Kn1cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0ICRlZGl0TW9kYWwgPSAkdGhpcy5maW5kKCcuZWRpdC1tb2RhbCcpO1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBBamF4IG9iamVjdC5cbiAgICAgICAgICpcbiAgICAgICAgICogQHR5cGUge29iamVjdH1cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IGFqYXggPSBqc2UubGlicy54aHI7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEluZm8gYm94IE9iamVjdC5cbiAgICAgICAgICpcbiAgICAgICAgICogQHR5cGUge29iamVjdH1cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IGluZm9Cb3ggPSBqc2UubGlicy5pbmZvX2JveDtcblxuICAgICAgICAvKipcbiAgICAgICAgICogTGFuZ3VhZ2Ugb2JqZWN0LlxuICAgICAgICAgKlxuICAgICAgICAgKiBAdHlwZSB7b2JqZWN0fVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgbGFuZyA9IGpzZS5jb3JlLmxhbmc7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIERlZmF1bHQgb3B0aW9ucyBmb3IgY29udHJvbGxlcixcbiAgICAgICAgICpcbiAgICAgICAgICogQHR5cGUge29iamVjdH1cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IGRlZmF1bHRzID0ge307XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEZpbmFsIGNvbnRyb2xsZXIgb3B0aW9ucy5cbiAgICAgICAgICpcbiAgICAgICAgICogQHR5cGUge29iamVjdH1cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBNb2R1bGUgb2JqZWN0LlxuICAgICAgICAgKlxuICAgICAgICAgKiBAdHlwZSB7e319XG4gICAgICAgICAqL1xuICAgICAgICBjb25zdCBtb2R1bGUgPSB7fTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogSW5pdGlhbGl6ZXMgdGhlIHJlbW92ZSBjb25maXJtYXRpb24gbW9kYWwuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSBldmVudE9iamVjdCBFdmVudCBvYmplY3QgdG8gZmV0Y2ggdGFyZ2V0cyBpZC5cbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IF9pbml0UmVtb3ZlQ29uZmlybWF0aW9uTW9kYWwgPSBldmVudE9iamVjdCA9PiB7XG4gICAgICAgICAgICBhamF4LmdldCh7XG4gICAgICAgICAgICAgICAgdXJsOiAnLi9hZG1pbi5waHA/ZG89TWFudWZhY3R1cmVyQWpheC9nZXRCeUlkJmlkPScgKyBldmVudE9iamVjdC50YXJnZXQuZGF0YXNldC5pZFxuICAgICAgICAgICAgfSkuZG9uZShyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgJHJlbW92ZUNvbmZpcm1hdGlvbk1vZGFsLm1vZGFsKCdzaG93Jyk7XG4gICAgICAgICAgICAgICAgX3JlbmRlclJlbW92ZUNvbmZpcm1hdGlvbk1vZGFsKHJlc3BvbnNlKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBJbml0aWFsaXplcyB0aGUgZWRpdCBtb2RhbC5cbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIGV2ZW50T2JqZWN0IEV2ZW50IG9iamVjdCB0byBmZXRjaCB0YXJnZXRzIGlkLlxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX2luaXRFZGl0TW9kYWwgPSBldmVudE9iamVjdCA9PiB7XG4gICAgICAgICAgICBhamF4LmdldCh7XG4gICAgICAgICAgICAgICAgdXJsOiAnLi9hZG1pbi5waHA/ZG89TWFudWZhY3R1cmVyQWpheC9nZXRCeUlkJmlkPScgKyBldmVudE9iamVjdC50YXJnZXQuZGF0YXNldC5pZFxuICAgICAgICAgICAgfSkuZG9uZShyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgJGVkaXRNb2RhbC5tb2RhbCgnc2hvdycpO1xuICAgICAgICAgICAgICAgIF9yZW5kZXJFZGl0TW9kYWwocmVzcG9uc2UpO1xuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5pbWFnZSAhPT0gJycpIHtcbiAgICAgICAgICAgICAgICAgICAgX3NldEltYWdlUHJldmlldyhyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEluaXRpYWxpemVzIHRoZSBlZGl0aW5nIG9mIGFuIE1hbnVmYWN0dXJlci5cbiAgICAgICAgICpcbiAgICAgICAgICogQHByaXZhdGVcbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IF9lZGl0TWFudWZhY3R1cmVyID0gKCkgPT4ge1xuICAgICAgICAgICAgaWYgKF92YWxpZGF0ZU5hbWVJbnB1dCgkZWRpdE1vZGFsKSkge1xuICAgICAgICAgICAgICAgIF91cGRhdGVEYXRhKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEluaXRpYWxpemVzIHRoZSBjcmVhdGlvbiBvZiBhbiBNYW51ZmFjdHVyZXIuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBjb25zdCBfY3JlYXRlTWFudWZhY3R1cmVyID0gKCkgPT4ge1xuICAgICAgICAgICAgaWYgKF92YWxpZGF0ZU5hbWVJbnB1dCgkY3JlYXRpb25Nb2RhbCkpIHtcbiAgICAgICAgICAgICAgICBfc3RvcmVEYXRhKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEluaXRpYWxpemVzIHRoZSBjbGVhbmluZyBwcm9jZXNzIGZyb20gY3JlYXRlIE1vZGFsLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX2NsZWFudXBDcmVhdGlvbk1vZGFsID0gKCkgPT4ge1xuICAgICAgICAgICAgX3RydW5jYXRlTW9kYWxGaWVsZHMoJGNyZWF0aW9uTW9kYWwpO1xuICAgICAgICAgICAgX3RydW5jYXRlTW9kYWxGaWVsZENsYXNzZXMoJGNyZWF0aW9uTW9kYWwpO1xuICAgICAgICAgICAgX3Jlc2V0TW9kYWxNZXNzYWdlcygkY3JlYXRpb25Nb2RhbCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEluaXRpYWxpemVzIHRoZSBjbGVhbmluZyBwcm9jZXNzIGZyb20gZWRpdCBNb2RhbC5cbiAgICAgICAgICpcbiAgICAgICAgICogQHByaXZhdGVcbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IF9jbGVhbnVwRWRpdE1vZGFsID0gKCkgPT4ge1xuICAgICAgICAgICAgX3JlbW92ZVdhcm5pbmcoKTtcbiAgICAgICAgICAgIF90cnVuY2F0ZU1vZGFsRmllbGRzKCRlZGl0TW9kYWwpO1xuICAgICAgICAgICAgX3RydW5jYXRlTW9kYWxGaWVsZENsYXNzZXMoJGVkaXRNb2RhbCk7XG4gICAgICAgICAgICBfcmVzZXRNb2RhbE1lc3NhZ2VzKCRlZGl0TW9kYWwpO1xuICAgICAgICAgICAgX3JlbW92ZUltYWdlUHJldmlldygpO1xuICAgICAgICAgICAgX3JlbW92ZUltYWdlQ2hlY2tib3hDaGVja2VkKCk7XG4gICAgICAgICAgICBfcmVtb3ZlSW1hZ2VDaGVja2JveFRleHQoKTtcbiAgICAgICAgICAgIF9yZW1vdmVJbWFnZUNoZWNrYm94KCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNlbmRzIGFuIGFqYXggcmVxdWVzdCB0byBzdG9yZSBhIG5ldyBtYW51ZmFjdHVyZXIgZW50aXR5LlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3N0b3JlRGF0YSA9ICgpID0+IHtcbiAgICAgICAgICAgIGFqYXgucG9zdCh7XG4gICAgICAgICAgICAgICAgdXJsOiAnLi9hZG1pbi5waHA/ZG89TWFudWZhY3R1cmVyQWpheC9zYXZlJyxcbiAgICAgICAgICAgICAgICBkYXRhOiBfY3JlYXRlSW5wdXREYXRhKCdjcmVhdGUnKSxcbiAgICAgICAgICAgICAgICBwcm9jZXNzRGF0YTogZmFsc2UsXG4gICAgICAgICAgICAgICAgY29udGVudFR5cGU6IGZhbHNlXG4gICAgICAgICAgICB9KS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgICAgICBfcmVuZGVyVGFibGUoKTtcbiAgICAgICAgICAgICAgICAgICAgJGNyZWF0aW9uTW9kYWwubW9kYWwoJ2hpZGUnKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UucmVuYW1lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgX2NyZWF0ZUltYWdlUmVuYW1lSW5mbygpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgaW5mb0JveC5hZGRTdWNjZXNzTWVzc2FnZShsYW5nLnRyYW5zbGF0ZSgnVEVYVF9TQVZFX1NVQ0NFU1MnLCAnbWFudWZhY3R1cmVycycpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBTZW5kcyBhbiBhamF4IHJlcXVlc3QgdG8gZWRpdCBhbiBtYW51ZmFjdHVyZXIgZW50aXR5LlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3VwZGF0ZURhdGEgPSAoKSA9PiB7XG4gICAgICAgICAgICBhamF4LnBvc3Qoe1xuICAgICAgICAgICAgICAgIHVybDogJy4vYWRtaW4ucGhwP2RvPU1hbnVmYWN0dXJlckFqYXgvdXBkYXRlJyxcbiAgICAgICAgICAgICAgICBkYXRhOiBfY3JlYXRlSW5wdXREYXRhKCdlZGl0JyksXG4gICAgICAgICAgICAgICAgcHJvY2Vzc0RhdGE6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlOiBmYWxzZVxuICAgICAgICAgICAgfSkudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgX3JlbmRlclRhYmxlKCk7XG4gICAgICAgICAgICAgICAgICAgICRlZGl0TW9kYWwubW9kYWwoJ2hpZGUnKTtcblxuICAgICAgICAgICAgICAgICAgICAkY3JlYXRpb25Nb2RhbC5tb2RhbCgnaGlkZScpO1xuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UucmVuYW1lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgX2NyZWF0ZUltYWdlUmVuYW1lSW5mbygpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgaW5mb0JveC5hZGRTdWNjZXNzTWVzc2FnZShsYW5nLnRyYW5zbGF0ZSgnVEVYVF9FRElUX1NVQ0NFU1MnLCAnbWFudWZhY3R1cmVycycpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENyZWF0ZXMgYW4gaW5mbyBib3ggd2l0aCByZW5hbWVkIGltYWdlIGluZm8uXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBjb25zdCBfY3JlYXRlSW1hZ2VSZW5hbWVJbmZvID0gKCkgPT4ge1xuICAgICAgICAgICAgaW5mb0JveC5hZGRNZXNzYWdlKHtcbiAgICAgICAgICAgICAgICBzb3VyY2U6ICdhamF4JyxcbiAgICAgICAgICAgICAgICBzdGF0dXM6ICduZXcnLFxuICAgICAgICAgICAgICAgIHR5cGU6ICdpbmZvJyxcbiAgICAgICAgICAgICAgICBjdXN0b21lcklkOiAxLFxuICAgICAgICAgICAgICAgIHZpc2liaWxpdHk6ICdyZW1vdmFibGUnLFxuICAgICAgICAgICAgICAgIGhlYWRsaW5lOiBsYW5nLnRyYW5zbGF0ZSgnVEVYVF9NQU5VRkFDVFVSRVJTX0lNQUdFJywgJ21hbnVmYWN0dXJlcnMnKSxcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiBsYW5nLnRyYW5zbGF0ZSgnVEVYVF9SRU5BTUVEX0lNQUdFJywgJ21hbnVmYWN0dXJlcnMnKSxcbiAgICAgICAgICAgIH0pLmRvbmUoKCkgPT4gJCgnLmluZm8tYm94JykudHJpZ2dlcigncmVmcmVzaDptZXNzYWdlcycpKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2VuZHMgYW4gYWpheCByZXF1ZXN0IHRvIHJlbW92ZSBhbiBtYW51ZmFjdHVyZXIgZW50aXR5LlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3JlbW92ZU1hbnVmYWN0dXJlciA9ICgpID0+IHtcbiAgICAgICAgICAgIGFqYXgucG9zdCh7XG4gICAgICAgICAgICAgICAgdXJsOiAnLi9hZG1pbi5waHA/ZG89TWFudWZhY3R1cmVyQWpheC9yZW1vdmUnLFxuICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6ICQoJy5tYW51ZmFjdHVyZXItcmVtb3ZlLWlkJykudmFsKClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgICAgICBfcmVuZGVyVGFibGUoKTtcbiAgICAgICAgICAgICAgICAgICAgJHJlbW92ZUNvbmZpcm1hdGlvbk1vZGFsLm1vZGFsKCdoaWRlJyk7XG4gICAgICAgICAgICAgICAgICAgIGluZm9Cb3guYWRkU3VjY2Vzc01lc3NhZ2UobGFuZy50cmFuc2xhdGUoJ1RFWFRfSU5GT19ERUxFVEVfU1VDQ0VTUycsICdtYW51ZmFjdHVyZXJzJykpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIG5hbWUgaW5wdXQgbm90IGVtcHR5IGFuZCBmYWxzZSBpZiBlbXB0eS5cbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIG1vZGFsXG4gICAgICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3ZhbGlkYXRlTmFtZUlucHV0ID0gbW9kYWwgPT4ge1xuICAgICAgICAgICAgY29uc3QgJG5hbWVJbnB1dCA9IG1vZGFsLmZpbmQoJ2lucHV0W25hbWU9XCJtYW51ZmFjdHVyZXJfbmFtZVwiXScpO1xuXG4gICAgICAgICAgICBpZiAoJG5hbWVJbnB1dC52YWwoKSA9PT0gJycpIHtcbiAgICAgICAgICAgICAgICAkbmFtZUlucHV0LnBhcmVudCgpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcbiAgICAgICAgICAgICAgICBtb2RhbC5maW5kKCdwLm1hbnVmYWN0dXJlci1tb2RhbC1pbmZvJylcbiAgICAgICAgICAgICAgICAgICAgLmZpcnN0KClcbiAgICAgICAgICAgICAgICAgICAgLnRleHQobGFuZy50cmFuc2xhdGUoJ0VSUk9SX01JU1NJTkdfTkFNRScsICdtYW51ZmFjdHVyZXJzJykpXG4gICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygndGV4dC1kYW5nZXInKTtcbiAgICAgICAgICAgICAgICAkKCcjaW5wdXQtbWFudWZhY3R1cmVycy1uYW1lJykuZm9jdXMoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVtb3ZlcyB0aGUgaGFzLWVycm9yIGNsYXNzIGZyb20gcGFyZW50IG5hbWUgaW5wdXQgaW4gbW9kYWwuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSBtb2RhbFxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3RydW5jYXRlTW9kYWxGaWVsZENsYXNzZXMgPSAobW9kYWwpID0+IHtcbiAgICAgICAgICAgIGNvbnN0ICRuYW1lSW5wdXQgPSBtb2RhbC5maW5kKCdpbnB1dFtuYW1lPVwibWFudWZhY3R1cmVyX25hbWVcIl0nKTtcbiAgICAgICAgICAgICRuYW1lSW5wdXQucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2hhcy1lcnJvcicpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZW1vdmVzIHRoZSBpbWFnZSBkZWxldGUgY2hlY2tib3ggdGV4dCBpZiBvbmUgZXhpc3RzLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3JlbW92ZUltYWdlQ2hlY2tib3hUZXh0ID0gKCkgPT4ge1xuICAgICAgICAgICAgaWYgKCRlZGl0TW9kYWwuZmluZCgnLmNoZWNrYm94LWluZm8tdGV4dCcpKSB7XG4gICAgICAgICAgICAgICAgJGVkaXRNb2RhbC5maW5kKCcuY2hlY2tib3gtaW5mby10ZXh0JykucmVtb3ZlKCcuY2hlY2tib3gtaW5mby10ZXh0Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFJlbW92ZXMgdGhlIGltYWdlIGRlbGV0ZSBjaGVja2JveCB0ZXh0IGlmIG9uZSBleGlzdHMuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBjb25zdCBfcmVtb3ZlSW1hZ2VDaGVja2JveENoZWNrZWQgPSAoKSA9PiB7XG4gICAgICAgICAgICBpZiAoJGVkaXRNb2RhbC5maW5kKCcuZGVsZXRlLWltYWdlLWNoZWNrYm94JykuaXMoJzpjaGVja2VkJykpIHtcbiAgICAgICAgICAgICAgICAkZWRpdE1vZGFsLmZpbmQoJy5kZWxldGUtaW1hZ2UtY2hlY2tib3gnKS5yZW1vdmVBdHRyKCdjaGVja2VkJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZW1vdmVzIHRoZSBpbWFnZSBkZWxldGUgY2hlY2tib3ggdGV4dCBpZiBvbmUgZXhpc3RzLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3JlbW92ZUltYWdlUHJldmlldyA9ICgpID0+IHtcbiAgICAgICAgICAgIGlmICgkZWRpdE1vZGFsLmZpbmQoJy5tYW51ZmFjdHVyZXItaW1hZ2UnKSkge1xuICAgICAgICAgICAgICAgICRlZGl0TW9kYWwuZmluZCgnLm1hbnVmYWN0dXJlci1pbWFnZScpLnJlbW92ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBTZXQgdGhlIENoZWNrYm94IHR5cGUgdG8gaGlkZGVuLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3JlbW92ZUltYWdlQ2hlY2tib3ggPSAoKSA9PiB7XG4gICAgICAgICAgICBpZiAoJGVkaXRNb2RhbC5maW5kKCcuc2luZ2xlLWNoZWNrYm94JykpIHtcbiAgICAgICAgICAgICAgICAkZWRpdE1vZGFsLmZpbmQoJy5zaW5nbGUtY2hlY2tib3gnKS5yZW1vdmUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVzZXRzIHRoZSBpbmZvIG1lc3NhZ2UgZnJvbSBtb2RhbC5cbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIG1vZGFsXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBjb25zdCBfcmVzZXRNb2RhbE1lc3NhZ2VzID0gKG1vZGFsKSA9PiB7XG4gICAgICAgICAgICBtb2RhbC5maW5kKCdwLm1hbnVmYWN0dXJlci1tb2RhbC1pbmZvJylcbiAgICAgICAgICAgICAgICAuZmlyc3QoKVxuICAgICAgICAgICAgICAgIC50ZXh0KGxhbmcudHJhbnNsYXRlKCdURVhUX05FV19JTlRSTycsICdtYW51ZmFjdHVyZXJzJykpLnJlbW92ZUNsYXNzKCd0ZXh0LWRhbmdlcicpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBUcnVuY2F0ZSBhbGwgaW5wdXQgdmFsdWVzIGZyb20gbW9kYWwuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSBtb2RhbFxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3RydW5jYXRlTW9kYWxGaWVsZHMgPSAobW9kYWwpID0+IHtcbiAgICAgICAgICAgIG1vZGFsLmZpbmQoJ2lucHV0JykuZWFjaCgoa2V5LCB2YWx1ZSkgPT4ge1xuICAgICAgICAgICAgICAgIHZhbHVlLnZhbHVlID0gJyc7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVuZGVycyBvdmVydmlldyB0YWJsZSAsIHRvIHNlZSBjaGFuZ2VzIGltbWVkaWF0ZWx5LlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc3QgX3JlbmRlclRhYmxlID0gKCkgPT4ge1xuICAgICAgICAgICAgYWpheC5nZXQoe1xuICAgICAgICAgICAgICAgIHVybDogJy4vYWRtaW4ucGhwP2RvPU1hbnVmYWN0dXJlckFqYXgvZ2V0RGF0YSdcbiAgICAgICAgICAgIH0pLmRvbmUocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0ICRib2R5ID0gJCgnLm1hbnVmYWN0dXJlci10YWJsZSB0Ym9keScpO1xuICAgICAgICAgICAgICAgICRib2R5LmVtcHR5KCk7XG5cbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlc3BvbnNlLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0ICRyb3cgPSAkKCc8dHIvPicpO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCAkbmFtZUNvbHVtbiA9ICQoJzx0ZC8+Jywge1xuICAgICAgICAgICAgICAgICAgICAgICAgJ3RleHQnOiByZXNwb25zZVtpXS5uYW1lXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCAkYWN0aW9uc0NvbnRhaW5lciA9ICQoJzxkaXYvPicsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICdjbGFzcyc6ICdwdWxsLXJpZ2h0IGFjdGlvbi1saXN0IHZpc2libGUtb24taG92ZXInXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCAkZWRpdCA9ICQoJzxpLz4nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAnZGF0YS1pZCc6IHJlc3BvbnNlW2ldLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2RhdGEtdG9nZ2xlJzogJ21vZGFsJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdkYXRhLXRhcmdldCc6ICcuZWRpdC1tb2RhbCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnY2xhc3MnOiAnZmEgZmEtcGVuY2lsIGVkaXQnLFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgJGRlbGV0ZSA9ICQoJzxpLz4nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAnZGF0YS1pZCc6IHJlc3BvbnNlW2ldLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2RhdGEtdG9nZ2xlJzogJ21vZGFsJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdkYXRhLXRhcmdldCc6ICcucmVtb3ZlLWNvbmZpcm1hdGlvbi1tb2RhbCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAnY2xhc3MnOiAnZmEgZmEtdHJhc2gtbyBkZWxldGUnXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCAkYWN0aW9uc0NvbCA9ICQoJzx0ZC8+Jywge1xuICAgICAgICAgICAgICAgICAgICAgICAgJ2NsYXNzJzogJ2FjdGlvbnMnXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICRhY3Rpb25zQ29udGFpbmVyLmFwcGVuZCgkZWRpdCkuYXBwZW5kKCRkZWxldGUpLmFwcGVuZFRvKCRhY3Rpb25zQ29sKTtcbiAgICAgICAgICAgICAgICAgICAgJHJvdy5hcHBlbmQoJG5hbWVDb2x1bW4pLmFwcGVuZCgkYWN0aW9uc0NvbCkuYXBwZW5kVG8oJGJvZHkpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAkdGhpcy5maW5kKCcuZGVsZXRlJykub24oJ2NsaWNrJywgX2luaXRSZW1vdmVDb25maXJtYXRpb25Nb2RhbCk7XG4gICAgICAgICAgICAgICAgJHRoaXMuZmluZCgnLmVkaXQnKS5vbignY2xpY2snLCBfaW5pdEVkaXRNb2RhbCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVuZGVycyByZW1vdmUgY29uZmlybWF0aW9uIG1vZGFsIHdpdGggZ2l2ZW4gZGF0YS5cbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHJlc3BvbnNlXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBjb25zdCBfcmVuZGVyUmVtb3ZlQ29uZmlybWF0aW9uTW9kYWwgPSAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGNvbnN0ICRpbmZvID0gJCgnLm1hbnVmYWN0dXJlci1tb2RhbC1yZW1vdmUtaW5mbycpO1xuICAgICAgICAgICAgY29uc3QgJG5hbWUgPSBsYW5nLnRyYW5zbGF0ZSgnVEVYVF9NQU5VRkFDVFVSRVJTJywgJ21hbnVmYWN0dXJlcnMnKSArIHJlc3BvbnNlLm5hbWVcbiAgICAgICAgICAgICAgICArICc8YnI+PGJyPic7XG5cbiAgICAgICAgICAgICRpbmZvLmVtcHR5KCk7XG4gICAgICAgICAgICAkKCcubWFudWZhY3R1cmVyLXJlbW92ZS1pZCcpLnZhbChyZXNwb25zZS5pZCk7XG4gICAgICAgICAgICAkaW5mby5hcHBlbmQoJG5hbWUpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZW5kZXJzIGVkaXQgbW9kYWwgd2l0aCBnaXZlbiBkYXRhLlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0gcmVzcG9uc2VcbiAgICAgICAgICogQHByaXZhdGVcbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IF9yZW5kZXJFZGl0TW9kYWwgPSByZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAkZWRpdE1vZGFsLmZpbmQoJy5tYW51ZmFjdHVyZXItaWQnKS52YWwocmVzcG9uc2UuaWQudG9TdHJpbmcoKSk7XG4gICAgICAgICAgICAkZWRpdE1vZGFsLmZpbmQoJyNpbnB1dC1tYW51ZmFjdHVyZXJzLW5hbWUnKS52YWwocmVzcG9uc2UubmFtZSk7XG5cbiAgICAgICAgICAgIGZvciAobGV0IGxhbmd1YWdlQ29kZSBpbiByZXNwb25zZS51cmxzKSB7XG4gICAgICAgICAgICAgICAgJGVkaXRNb2RhbC5maW5kKCcubWFudWZhY3R1cmVyLScgKyBsYW5ndWFnZUNvZGUpLnZhbChyZXNwb25zZS51cmxzW2xhbmd1YWdlQ29kZV0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoJGVkaXRNb2RhbC5maW5kKCdpbnB1dFtuYW1lPVwibWFudWZhY3R1cmVyX2xvZ29cIl0nKS5hdHRyKCd0eXBlJykgPT09ICd0ZXh0Jykge1xuICAgICAgICAgICAgICAgICRlZGl0TW9kYWwuZmluZCgnaW5wdXRbbmFtZT1cIm1hbnVmYWN0dXJlcl9sb2dvXCJdJykudmFsKHJlc3BvbnNlLmltYWdlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmltYWdlICE9PSAnJykge1xuICAgICAgICAgICAgICAgIF9zZXREZWxldGVJbWFnZUNoZWNrYm94KClcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJGVkaXRNb2RhbC5maW5kKCcubWFudWZhY3R1cmVycy1pbWctcGF0aCcpLnRleHQocmVzcG9uc2UuaW1hZ2UpO1xuICAgICAgICAgICAgJGVkaXRNb2RhbC5maW5kKCcubWFudWZhY3R1cmVyLWRhdGUtYWRkZWQnKVxuICAgICAgICAgICAgICAgIC50ZXh0KCcnKVxuICAgICAgICAgICAgICAgIC5hcHBlbmQocmVzcG9uc2UuZGF0ZUFkZGVkKTtcblxuICAgICAgICAgICAgJGVkaXRNb2RhbC5maW5kKCcubWFudWZhY3R1cmVyLWxhc3QtbW9kaWZpZWQnKVxuICAgICAgICAgICAgICAgIC50ZXh0KCcnKVxuICAgICAgICAgICAgICAgIC5hcHBlbmQocmVzcG9uc2UubGFzdE1vZGlmaWVkKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2V0cyBhbiBpbWFnZSBwcmV2aWV3IGluIGFuIG1hbnVmYWN0dXJlci1pbWFnZS1jb250YWluZXIuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSByZXNwb25zZSB7e1xuICAgICAgICAgKiAgICAgICAgICAgICAgICAgICBpbWFnZVBhdGggOiBzdHJpbmdcbiAgICAgICAgICogICAgICAgICAgICAgICAgICAgbmFtZSAgICAgIDogc3RyaW5nXG4gICAgICAgICAqICAgICAgICAgICAgICAgICAgIGltYWdlICAgICA6IHN0cmluZ1xuICAgICAgICAgKiAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICogQHByaXZhdGVcbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IF9zZXRJbWFnZVByZXZpZXcgPSAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGNvbnN0ICRtYW51ZmFjdHVyZXJJbWFnZSA9ICQoJzxpbWcvPicsIHtcbiAgICAgICAgICAgICAgICAnY2xhc3MnOiAnbWFudWZhY3R1cmVyLWltYWdlICcsXG4gICAgICAgICAgICAgICAgJ3NyYyc6IHJlc3BvbnNlLmltYWdlUGF0aCxcbiAgICAgICAgICAgICAgICAnYWx0JzogcmVzcG9uc2UubmFtZSxcbiAgICAgICAgICAgICAgICAndGl0bGUnOiByZXNwb25zZS5pbWFnZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkZWRpdE1vZGFsLmZpbmQoJy5tYW51ZmFjdHVyZXItaW1hZ2UtY29udGFpbmVyJykuYXBwZW5kKCRtYW51ZmFjdHVyZXJJbWFnZSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNldHMgdGhlIGltYWdlIGRlbGV0ZSBDaGVja2JveCBpbnB1dC5cbiAgICAgICAgICpcbiAgICAgICAgICogQHByaXZhdGVcbiAgICAgICAgICovXG4gICAgICAgIGNvbnN0IF9zZXREZWxldGVJbWFnZUNoZWNrYm94ID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgJGNoZWNrYm94VGV4dCA9ICQoJzxzcGFuLz4nLCB7XG4gICAgICAgICAgICAgICAgJ2NsYXNzJzogJ2NoZWNrYm94LWluZm8tdGV4dCcsXG4gICAgICAgICAgICAgICAgJ3RleHQnOiAnICcgKyBsYW5nLnRyYW5zbGF0ZSgnVEVYVF9ERUxFVEVfSU1BR0UnLCAnbWFudWZhY3R1cmVycycpXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGNvbnN0ICRjaGVja2JveElucHV0ID0gJCgnPGlucHV0Lz4nLCB7XG4gICAgICAgICAgICAgICAgJ2NsYXNzJzogJ2RlbGV0ZS1pbWFnZS1jaGVja2JveCcsXG4gICAgICAgICAgICAgICAgJ3R5cGUnOiAnY2hlY2tib3gnLFxuICAgICAgICAgICAgICAgICd0aXRsZSc6IGxhbmcudHJhbnNsYXRlKCdURVhUX0RFTEVURV9JTUFHRScsICdtYW51ZmFjdHVyZXJzJylcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBpZiAoJGVkaXRNb2RhbC5maW5kKCcuc2luZ2xlLWNoZWNrYm94JykpIHtcbiAgICAgICAgICAgICAgICAkZWRpdE1vZGFsLmZpbmQoJy5zaW5nbGUtY2hlY2tib3gnKS5yZW1vdmUoKTtcblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJGVkaXRNb2RhbC5maW5kKCcubWFudWZhY3R1cmVyLWltYWdlLWRhdGEnKVxuICAgICAgICAgICAgICAgIC5hcHBlbmQoJGNoZWNrYm94SW5wdXQpXG4gICAgICAgICAgICAgICAgLmFwcGVuZCgkY2hlY2tib3hUZXh0KVxuICAgICAgICAgICAgICAgIC5hdHRyKCdkYXRhLWd4LXdpZGdldCcsICdzaW5nbGVfY2hlY2tib3gnKTtcblxuICAgICAgICAgICAgZ3gud2lkZ2V0cy5pbml0KCRlZGl0TW9kYWwpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZW1vdmVzIHRoZSB3YXJuaW5nIHRleHQuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBjb25zdCBfcmVtb3ZlV2FybmluZyA9ICgpID0+IHtcbiAgICAgICAgICAgICRlZGl0TW9kYWwuZmluZCgncC5tYW51ZmFjdHVyZXItbW9kYWwtaW5mbycpXG4gICAgICAgICAgICAgICAgLnRleHQoJycpXG4gICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCd0ZXh0LWRhbmdlcicpO1xuICAgICAgICB9O1xuXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENyZWF0ZXMgYW5kIHJldHVybiBkYXRhIG9iamVjdCBmcm9tIGZvcm11bGEgaW5mb3JtYXRpb24ncy5cbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIG1vZGFsVHlwZVxuICAgICAgICAgKiBAcmV0dXJucyB7e319XG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBjb25zdCBfY3JlYXRlSW5wdXREYXRhID0gbW9kYWxUeXBlID0+IHtcbiAgICAgICAgICAgIGNvbnN0ICRtb2RhbCA9IG1vZGFsVHlwZSA9PT0gJ2VkaXQnID8gJGVkaXRNb2RhbCA6ICRjcmVhdGlvbk1vZGFsO1xuICAgICAgICAgICAgY29uc3QgJGlucHV0cyA9ICRtb2RhbC5maW5kKCdpbnB1dFt0eXBlPVwidGV4dFwiXScpO1xuICAgICAgICAgICAgY29uc3QgJGlkID0gJG1vZGFsLmZpbmQoJ2lucHV0W25hbWU9XCJpZFwiXScpLnZhbCgpO1xuICAgICAgICAgICAgY29uc3QgJGltZyA9ICRtb2RhbC5maW5kKCdpbnB1dFtuYW1lPVwibWFudWZhY3R1cmVyX2xvZ29cIl0nKTtcbiAgICAgICAgICAgIGNvbnN0ICRpbWdQYXRoID0gJGltZy52YWwoKTtcbiAgICAgICAgICAgIGNvbnN0IGlzRmlsZU1hbmFnZXJJbnB1dCA9ICRtb2RhbC5maW5kKCcucmVzcG9uc2l2ZS1maWxlLW1hbmFnZXInKS5sZW5ndGggIT09IDA7XG4gICAgICAgICAgICBjb25zdCBkYXRhID0gbmV3IEZvcm1EYXRhKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tYW51ZmFjdHVyZXItZm9ybScpKTtcblxuICAgICAgICAgICAgZGF0YS5hcHBlbmQoJ21hbnVmYWN0dXJlcl9maWxlJywgIWlzRmlsZU1hbmFnZXJJbnB1dCk7XG5cbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgJGlucHV0cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGRhdGEuYXBwZW5kKCRpbnB1dHNbaV0uZ2V0QXR0cmlidXRlKCduYW1lJyksICRpbnB1dHNbaV0udmFsdWUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodW5kZWZpbmVkICE9PSAkaWQpIHtcbiAgICAgICAgICAgICAgICBkYXRhLmFwcGVuZCgnaWQnLCAkaWQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIWlzRmlsZU1hbmFnZXJJbnB1dCAmJiB1bmRlZmluZWQgIT09ICRpbWcpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBmaWxlID0gJGltZ1swXS5maWxlc1swXSA9PT0gbnVsbCA/ICRpbWdQYXRoIDogJGltZ1swXS5maWxlc1swXTtcbiAgICAgICAgICAgICAgICBpZiAoZmlsZSkge1xuICAgICAgICAgICAgICAgICAgICBkYXRhLmFwcGVuZCgnbWFudWZhY3R1cmVyX2xvZ28nLCBmaWxlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICgkbW9kYWwgPT09ICRlZGl0TW9kYWwpIHtcbiAgICAgICAgICAgICAgICBkYXRhLmFwcGVuZCgnbWFudWZhY3R1cmVyX2NoZWNrYm94JywgJGVkaXRNb2RhbC5maW5kKCcuZGVsZXRlLWltYWdlLWNoZWNrYm94JykuaXMoJzpjaGVja2VkJykpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGRhdGFbJ21hbnVmYWN0dXJlcl9sb2dvJ10gPT09ICcnICYmICRpbWdQYXRoICE9PSAnJykge1xuICAgICAgICAgICAgICAgICAgICBkYXRhLmFwcGVuZCgnbWFudWZhY3R1cmVyX2xvZ28nLCAkaW1nUGF0aCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gZGF0YTtcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBldmVudCBoYW5kbGVyXG5cbiAgICAgICAgLy8gaW5pdGlhbGl6YXRpb25cbiAgICAgICAgbW9kdWxlLmluaXQgPSBkb25lID0+IHtcbiAgICAgICAgICAgIC8vIGluaXRpYWxpemF0aW9uIGxvZ2ljXG5cbiAgICAgICAgICAgICR0aGlzLmZpbmQoJy5kZWxldGUnKS5vbignY2xpY2snLCBfaW5pdFJlbW92ZUNvbmZpcm1hdGlvbk1vZGFsKTtcbiAgICAgICAgICAgICRjcmVhdGlvbk1vZGFsLmZpbmQoJy5idG4tcHJpbWFyeScpLm9uKCdjbGljaycsIF9jcmVhdGVNYW51ZmFjdHVyZXIpO1xuICAgICAgICAgICAgJHJlbW92ZUNvbmZpcm1hdGlvbk1vZGFsLmZpbmQoJy5idG4tZGFuZ2VyJykub24oJ2NsaWNrJywgX3JlbW92ZU1hbnVmYWN0dXJlcik7XG4gICAgICAgICAgICAkZWRpdE1vZGFsLmZpbmQoJy5idG4tcHJpbWFyeScpLm9uKCdjbGljaycsIF9lZGl0TWFudWZhY3R1cmVyKTtcbiAgICAgICAgICAgICR0aGlzLmZpbmQoJy5lZGl0Jykub24oJ2NsaWNrJywgX2luaXRFZGl0TW9kYWwpO1xuXG4gICAgICAgICAgICAvL2FjdGlvbnNcbiAgICAgICAgICAgICRjcmVhdGlvbk1vZGFsLm9uKCdoaWRlLmJzLm1vZGFsJywgX2NsZWFudXBDcmVhdGlvbk1vZGFsKTtcbiAgICAgICAgICAgICRlZGl0TW9kYWwub24oJ2hpZGUuYnMubW9kYWwnLCBfY2xlYW51cEVkaXRNb2RhbCk7XG5cbiAgICAgICAgICAgIGRvbmUoKTtcbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIG1vZHVsZTtcbiAgICB9KTtcbiJdfQ==
