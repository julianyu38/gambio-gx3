'use strict';

/* --------------------------------------------------------------
 shop_offline.js 2016-09-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * General Controller of Shop Online/Offline
 */
gx.controllers.module('shop_offline', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  * 
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance 
  * 
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	function _toggleLanguageSelection() {
		var $languagesButtonBar = $('.languages.buttonbar');

		if ($(this).attr('href') === '#status') {
			$languagesButtonBar.css('visibility', 'hidden');
		} else {
			$languagesButtonBar.css('visibility', 'visible');
		}
	}

	function _correctCodeMirrorDisplay() {
		setTimeout(function () {
			$('.CodeMirror:visible').each(function (index, codeMirror) {
				codeMirror.CodeMirror.refresh();
			});
		}, 100);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.find('.tab-headline-wrapper > a').on('click', _toggleLanguageSelection).on('click', _correctCodeMirrorDisplay);

		_toggleLanguageSelection.call($('.tab-headline-wrapper a')[0]);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNob3Bfb2ZmbGluZS9zaG9wX29mZmxpbmUuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJfdG9nZ2xlTGFuZ3VhZ2VTZWxlY3Rpb24iLCIkbGFuZ3VhZ2VzQnV0dG9uQmFyIiwiYXR0ciIsImNzcyIsIl9jb3JyZWN0Q29kZU1pcnJvckRpc3BsYXkiLCJzZXRUaW1lb3V0IiwiZWFjaCIsImluZGV4IiwiY29kZU1pcnJvciIsIkNvZGVNaXJyb3IiLCJyZWZyZXNoIiwiaW5pdCIsImRvbmUiLCJmaW5kIiwib24iLCJjYWxsIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7OztBQUdBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0IsY0FBdEIsRUFBc0MsRUFBdEMsRUFBMEMsVUFBU0MsSUFBVCxFQUFlOztBQUV4RDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsVUFBU0ksd0JBQVQsR0FBb0M7QUFDbkMsTUFBTUMsc0JBQXNCRixFQUFFLHNCQUFGLENBQTVCOztBQUVBLE1BQUlBLEVBQUUsSUFBRixFQUFRRyxJQUFSLENBQWEsTUFBYixNQUF5QixTQUE3QixFQUF3QztBQUN2Q0QsdUJBQW9CRSxHQUFwQixDQUF3QixZQUF4QixFQUFzQyxRQUF0QztBQUNBLEdBRkQsTUFFTztBQUNORix1QkFBb0JFLEdBQXBCLENBQXdCLFlBQXhCLEVBQXNDLFNBQXRDO0FBQ0E7QUFDRDs7QUFFRCxVQUFTQyx5QkFBVCxHQUFxQztBQUNwQ0MsYUFBVyxZQUFNO0FBQ2hCTixLQUFFLHFCQUFGLEVBQXlCTyxJQUF6QixDQUE4QixVQUFDQyxLQUFELEVBQVFDLFVBQVIsRUFBdUI7QUFDcERBLGVBQVdDLFVBQVgsQ0FBc0JDLE9BQXRCO0FBQ0EsSUFGRDtBQUdBLEdBSkQsRUFJRyxHQUpIO0FBS0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBZCxRQUFPZSxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCZCxRQUFNZSxJQUFOLENBQVcsMkJBQVgsRUFDRUMsRUFERixDQUNLLE9BREwsRUFDY2Qsd0JBRGQsRUFFRWMsRUFGRixDQUVLLE9BRkwsRUFFY1YseUJBRmQ7O0FBSUFKLDJCQUF5QmUsSUFBekIsQ0FBOEJoQixFQUFFLHlCQUFGLEVBQTZCLENBQTdCLENBQTlCOztBQUVBYTtBQUNBLEVBUkQ7O0FBVUEsUUFBT2hCLE1BQVA7QUFFQSxDQTVERCIsImZpbGUiOiJzaG9wX29mZmxpbmUvc2hvcF9vZmZsaW5lLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIHNob3Bfb2ZmbGluZS5qcyAyMDE2LTA5LTA2XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIEdlbmVyYWwgQ29udHJvbGxlciBvZiBTaG9wIE9ubGluZS9PZmZsaW5lXHJcbiAqL1xyXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ3Nob3Bfb2ZmbGluZScsIFtdLCBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIFZBUklBQkxFU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdCAqIFxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBJbnN0YW5jZSBcclxuXHQgKiBcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG1vZHVsZSA9IHt9OyBcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBGVU5DVElPTlNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRmdW5jdGlvbiBfdG9nZ2xlTGFuZ3VhZ2VTZWxlY3Rpb24oKSB7XHJcblx0XHRjb25zdCAkbGFuZ3VhZ2VzQnV0dG9uQmFyID0gJCgnLmxhbmd1YWdlcy5idXR0b25iYXInKTsgXHJcblx0XHRcclxuXHRcdGlmICgkKHRoaXMpLmF0dHIoJ2hyZWYnKSA9PT0gJyNzdGF0dXMnKSB7XHJcblx0XHRcdCRsYW5ndWFnZXNCdXR0b25CYXIuY3NzKCd2aXNpYmlsaXR5JywgJ2hpZGRlbicpOyBcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdCRsYW5ndWFnZXNCdXR0b25CYXIuY3NzKCd2aXNpYmlsaXR5JywgJ3Zpc2libGUnKTsgXHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdGZ1bmN0aW9uIF9jb3JyZWN0Q29kZU1pcnJvckRpc3BsYXkoKSB7XHJcblx0XHRzZXRUaW1lb3V0KCgpID0+IHtcclxuXHRcdFx0JCgnLkNvZGVNaXJyb3I6dmlzaWJsZScpLmVhY2goKGluZGV4LCBjb2RlTWlycm9yKSA9PiB7XHJcblx0XHRcdFx0Y29kZU1pcnJvci5Db2RlTWlycm9yLnJlZnJlc2goKTtcclxuXHRcdFx0fSlcclxuXHRcdH0sIDEwMCk7IFxyXG5cdH1cclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0JHRoaXMuZmluZCgnLnRhYi1oZWFkbGluZS13cmFwcGVyID4gYScpXHJcblx0XHRcdC5vbignY2xpY2snLCBfdG9nZ2xlTGFuZ3VhZ2VTZWxlY3Rpb24pIFxyXG5cdFx0XHQub24oJ2NsaWNrJywgX2NvcnJlY3RDb2RlTWlycm9yRGlzcGxheSk7IFxyXG5cdFx0XHJcblx0XHRfdG9nZ2xlTGFuZ3VhZ2VTZWxlY3Rpb24uY2FsbCgkKCcudGFiLWhlYWRsaW5lLXdyYXBwZXIgYScpWzBdKTtcclxuXHRcdFxyXG5cdFx0ZG9uZSgpOyBcclxuXHR9OyBcclxuXHRcclxuXHRyZXR1cm4gbW9kdWxlO1xyXG5cdFxyXG59KTsgIl19
