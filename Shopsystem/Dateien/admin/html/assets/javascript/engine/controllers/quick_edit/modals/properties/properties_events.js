'use strict';

/* --------------------------------------------------------------
 events.js 2016-10-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Table Events Controller
 *
 * Handles the events of the properties table.
 */
gx.controllers.module('properties_events', ['loading_spinner', gx.source + '/libs/button_dropdown'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Bulk selection change event handler.
  *
  * @param {jQuery.Event} event Contains event information.
  * @param {Boolean} [propagate = true] Whether to propagate the event or not.
  */
	function _onBulkSelectionChange(event) {
		var propagate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		if (propagate === false) {
			return;
		}

		$this.find('tbody input:checkbox.properties-row-selection').single_checkbox('checked', $(this).prop('checked')).trigger('change');
	}

	/**
  * Table row click event handler.
  *
  * @param {jQuery.Event} event Contains event information.
  */
	function _onTableRowClick(event) {
		if (!$(event.target).is('td')) {
			return;
		}

		var $singleCheckbox = $(this).find('input:checkbox.properties-row-selection');

		$singleCheckbox.prop('checked', !$(this).find('input:checkbox.properties-row-selection').prop('checked')).trigger('change');
	}

	/**
  * Enables row editing mode.
  */
	function _onTableRowEditClick() {
		var $tableRow = $(this).parents('tr');
		var $singleCheckbox = $(this).parents('tr').find('input:checkbox.properties-row-selection');

		$tableRow.find('td.editable').each(function () {
			if ($(this).find('input:text').length > 0) {
				return;
			}

			if ($(this).find('p.values_price').length > 0) {
				$(this).find('p.values_price').each(function () {
					var html = '<input data-properties-price-type="' + $(this).data('properties-price-type') + '"\n\t\t\t\t\t\t\t\t\t   type="text" \n\t\t\t\t\t\t\t\t\t   class="form-control values-price" \n\t\t\t\t\t\t\t\t\t   value="' + $(this).context.innerText + '" />';

					$(this).replaceWith(html);
				});
			} else {
				this.innerHTML = '<input type="text" class="form-control" value="' + this.innerText + '" />';
			}
		});

		$singleCheckbox.prop('checked', !$(this).find('input:checkbox.properties-row-selection').prop('checked')).trigger('change');

		var $rowActionsDropdown = $tableRow.find('.dropdown');
		var $editAction = $(this);
		var $saveAction = $rowActionsDropdown.find('.save-properties-edits');
		$editAction.addClass('hidden');
		$saveAction.removeClass('hidden');
		jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $saveAction);
	}

	/**
  * Bulk row edit event handler.
  *
  * Enables the edit mode for the selected rows.
  */
	function _onTableBulkRowEditClick() {
		var $bulkEditAction = $(this);
		var $checkedSingleCheckboxes = $this.find('input:checkbox:checked.properties-row-selection');

		if ($checkedSingleCheckboxes.length) {
			var $bulkActionsDropdown = $('.properties-bulk-action');
			var $bulkSaveAction = $bulkActionsDropdown.find('.save-properties-bulk-row-edits');
			$bulkEditAction.addClass('hidden');
			$bulkSaveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkSaveAction);
		}

		$checkedSingleCheckboxes.each(function () {
			var $tableRow = $(this).parents('tr');
			var $rowActionDropdown = $tableRow.find('.btn-group.dropdown');
			var $saveAction = $rowActionDropdown.find('.save-properties-edits');
			var $editAction = $rowActionDropdown.find('.row-properties-edit');

			$editAction.addClass('hidden');
			$saveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($rowActionDropdown, $saveAction);

			$tableRow.find('td.editable').each(function () {
				if ($(this).find('input:text').length > 0) {
					return;
				}

				if ($(this).find('p.values_price').length > 0) {
					$(this).find('p.values_price').each(function () {
						var html = '<input data-properties-price-type="' + $(this).data('properties-price-type') + '"\n\t\t\t\t\t\t\t\t\t\t   type="text" \n\t\t\t\t\t\t\t\t\t\t   class="form-control values-price" \n\t\t\t\t\t\t\t\t\t\t   value="' + $(this).context.innerText + '" />';

						$(this).replaceWith(html);
					});
				} else {
					this.innerHTML = '<input type="text" class="form-control" value="' + this.innerText + '" />';
				}
			});
		});
	}

	/**
  * Table row checkbox change event handler.
  */
	function _onTableRowCheckboxChange() {
		var $bulkActionDropdownButtons = $this.parents('.properties.modal').find('.properties-bulk-action > button');
		var $tableRow = $(this).parents('tr');

		if ($this.find('input:checkbox:checked.properties-row-selection').length > 0) {
			$bulkActionDropdownButtons.removeClass('disabled');
		} else {
			$bulkActionDropdownButtons.addClass('disabled');

			var $bulkActionsDropdown = $('.properties-bulk-action');
			var $bulkSaveAction = $bulkActionsDropdown.find('.save-properties-bulk-row-edits');

			if (!$bulkSaveAction.hasClass('hidden')) {
				var $bulkEditAction = $bulkActionsDropdown.find('.properties-bulk-row-edit');
				$bulkEditAction.removeClass('hidden');
				$bulkSaveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkEditAction);
			}
		}

		$(this).parents('tr').find('p.values_price').each(function () {
			if ($(this).data('properties-price-type') == 'fix') {
				$(this).parents('td').addClass('editable');
			}
		});

		if (!$(this).prop('checked')) {
			_resolveRowChanges($(this).parents('tr'));

			var $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
			var $saveAction = $rowActionsDropdown.find('.save-properties-edits');

			if (!$saveAction.hasClass('hidden')) {
				var $editAction = $tableRow.find('.row-properties-edit');
				$editAction.removeClass('hidden');
				$saveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $editAction.last());
			}
		}
	}

	/**
  * Cancel data modifications event handler.
  */
	function _onCancelClick() {
		var $pageMode = $(this).closest('thead').find('.select-properties-page-mode');
		var $checkedSingleCheckboxes = $this.find('input:checkbox:checked.properties-row-selection');

		if ($pageMode.val() == 'edit-mode') {
			$pageMode.val('filter-mode');
		} else {
			$pageMode.val('edit-mode');
		}

		$checkedSingleCheckboxes.each(function () {
			$(this).prop('checked', !$(this).prop('checked')).trigger('change');

			_resolveRowChanges($(this).parents('tr'));
		});

		_onPageModeChange();
	}

	/**
  * Page mode change between "edit" and "filtering".
  */
	function _onPageModeChange() {
		if ($(this).val() == 'edit-mode') {
			$this.find('tr.properties-filter').attr('hidden', true);
			$this.find('tr.properties-edit').attr('hidden', false);
			$this.find('thead tr:first-child th').addClass('edit-mode');
		} else {
			$this.find('tr.properties-filter').attr('hidden', false);
			$this.find('tr.properties-edit').attr('hidden', true);
			$this.find('thead tr:first-child th').removeClass('edit-mode');
		}
	}

	/**
  * Restores all the row data changes back to their original state.
  *
  * @param {jQuery.Event} $row Current row selector.
  */
	function _resolveRowChanges($row) {
		var rowIndex = $this.DataTable().row($row).index();

		$row.find('input:text:not(.values-price)').each(function () {
			var $cell = $(this).closest('td');
			var columnIndex = $this.DataTable().column($cell).index();

			this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
		});

		$row.find('input:text.values-price').each(function () {
			var $cell = $(this).closest('td');
			var columnIndex = $this.DataTable().column($cell).index();
			var cellData = $this.DataTable().cell(rowIndex, columnIndex).data();

			var html = '<div class="col-lg-12">\n\t\t\t\t\t\t\t<label class="control-label">' + cellData.values_name + '</label>\n\t\t\t\t\t\t\t<p data-properties-price-type="' + $(this).data('properties-price-type') + '"\n\t\t\t\t\t\t\t   class="form-control-static values_price">' + cellData.values_price + '</p>\n\t\t\t\t\t\t</div>';

			$(this).closest('div.form-group').replaceWith('<div class="form-group">' + html + '</div>');
		});
	}

	/**
  * Table row data change event handler.
  *
  * It's being triggered every time a row input/select field is changed.
  */
	function _onTableRowDataChange() {
		var $row = $(this).parents('tr');
		var rowIndex = $this.DataTable().row($row).index();

		$row.find('input:text').each(function () {
			var $cell = $(this).closest('td');
			var columnIndex = $this.DataTable().column($cell).index();

			if ($.trim($(this).val()) != $this.DataTable().cell(rowIndex, columnIndex).data()) {
				$(this).addClass('modified');

				return;
			}

			$(this).removeClass('modified');
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', 'tbody tr', _onTableRowClick).on('click', '.row-properties-edit', _onTableRowEditClick).on('change', '.properties-bulk-selection', _onBulkSelectionChange).on('keyup', 'tbody tr input:text', _onTableRowDataChange);

		$this.parents('.properties.modal').on('change', 'input:checkbox.properties-row-selection', _onTableRowCheckboxChange).on('change', '.select-properties-page-mode', _onPageModeChange).on('click', '.cancel-properties-edits', _onCancelClick).on('click', '.btn-group .properties-bulk-row-edit', _onTableBulkRowEditClick);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc19ldmVudHMuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJfb25CdWxrU2VsZWN0aW9uQ2hhbmdlIiwiZXZlbnQiLCJwcm9wYWdhdGUiLCJmaW5kIiwic2luZ2xlX2NoZWNrYm94IiwicHJvcCIsInRyaWdnZXIiLCJfb25UYWJsZVJvd0NsaWNrIiwidGFyZ2V0IiwiaXMiLCIkc2luZ2xlQ2hlY2tib3giLCJfb25UYWJsZVJvd0VkaXRDbGljayIsIiR0YWJsZVJvdyIsInBhcmVudHMiLCJlYWNoIiwibGVuZ3RoIiwiaHRtbCIsImNvbnRleHQiLCJpbm5lclRleHQiLCJyZXBsYWNlV2l0aCIsImlubmVySFRNTCIsIiRyb3dBY3Rpb25zRHJvcGRvd24iLCIkZWRpdEFjdGlvbiIsIiRzYXZlQWN0aW9uIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImpzZSIsImxpYnMiLCJidXR0b25fZHJvcGRvd24iLCJzZXREZWZhdWx0QWN0aW9uIiwiX29uVGFibGVCdWxrUm93RWRpdENsaWNrIiwiJGJ1bGtFZGl0QWN0aW9uIiwiJGNoZWNrZWRTaW5nbGVDaGVja2JveGVzIiwiJGJ1bGtBY3Rpb25zRHJvcGRvd24iLCIkYnVsa1NhdmVBY3Rpb24iLCIkcm93QWN0aW9uRHJvcGRvd24iLCJfb25UYWJsZVJvd0NoZWNrYm94Q2hhbmdlIiwiJGJ1bGtBY3Rpb25Ecm9wZG93bkJ1dHRvbnMiLCJoYXNDbGFzcyIsIl9yZXNvbHZlUm93Q2hhbmdlcyIsImxhc3QiLCJfb25DYW5jZWxDbGljayIsIiRwYWdlTW9kZSIsImNsb3Nlc3QiLCJ2YWwiLCJfb25QYWdlTW9kZUNoYW5nZSIsImF0dHIiLCIkcm93Iiwicm93SW5kZXgiLCJEYXRhVGFibGUiLCJyb3ciLCJpbmRleCIsIiRjZWxsIiwiY29sdW1uSW5kZXgiLCJjb2x1bW4iLCJwYXJlbnRFbGVtZW50IiwiY2VsbCIsImNlbGxEYXRhIiwidmFsdWVzX25hbWUiLCJ2YWx1ZXNfcHJpY2UiLCJfb25UYWJsZVJvd0RhdGFDaGFuZ2UiLCJ0cmltIiwiaW5pdCIsImRvbmUiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7OztBQUtBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0IsbUJBQXRCLEVBQTJDLENBQUMsaUJBQUQsRUFBdUJGLEdBQUdHLE1BQTFCLDJCQUEzQyxFQUFxRyxVQUFTQyxJQUFULEVBQWU7O0FBRW5IOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUosU0FBUyxFQUFmOztBQUdBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUEsVUFBU0ssc0JBQVQsQ0FBZ0NDLEtBQWhDLEVBQXlEO0FBQUEsTUFBbEJDLFNBQWtCLHVFQUFOLElBQU07O0FBQ3hELE1BQUlBLGNBQWMsS0FBbEIsRUFBeUI7QUFDeEI7QUFDQTs7QUFFREosUUFBTUssSUFBTixDQUFXLCtDQUFYLEVBQ0VDLGVBREYsQ0FDa0IsU0FEbEIsRUFDNkJMLEVBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsU0FBYixDQUQ3QixFQUVFQyxPQUZGLENBRVUsUUFGVjtBQUdBOztBQUVEOzs7OztBQUtBLFVBQVNDLGdCQUFULENBQTBCTixLQUExQixFQUFpQztBQUNoQyxNQUFJLENBQUNGLEVBQUVFLE1BQU1PLE1BQVIsRUFBZ0JDLEVBQWhCLENBQW1CLElBQW5CLENBQUwsRUFBK0I7QUFDOUI7QUFDQTs7QUFFRCxNQUFNQyxrQkFBa0JYLEVBQUUsSUFBRixFQUFRSSxJQUFSLENBQWEseUNBQWIsQ0FBeEI7O0FBRUFPLGtCQUFnQkwsSUFBaEIsQ0FBcUIsU0FBckIsRUFBZ0MsQ0FBQ04sRUFBRSxJQUFGLEVBQy9CSSxJQUQrQixDQUMxQix5Q0FEMEIsRUFFL0JFLElBRitCLENBRTFCLFNBRjBCLENBQWpDLEVBR0VDLE9BSEYsQ0FHVSxRQUhWO0FBSUE7O0FBRUQ7OztBQUdBLFVBQVNLLG9CQUFULEdBQWdDO0FBQy9CLE1BQU1DLFlBQVliLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLENBQWxCO0FBQ0EsTUFBTUgsa0JBQWtCWCxFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixFQUFzQlYsSUFBdEIsQ0FBMkIseUNBQTNCLENBQXhCOztBQUVBUyxZQUFVVCxJQUFWLENBQWUsYUFBZixFQUE4QlcsSUFBOUIsQ0FBbUMsWUFBVztBQUM3QyxPQUFJZixFQUFFLElBQUYsRUFBUUksSUFBUixDQUFhLFlBQWIsRUFBMkJZLE1BQTNCLEdBQW9DLENBQXhDLEVBQTJDO0FBQzFDO0FBQ0E7O0FBRUQsT0FBSWhCLEVBQUUsSUFBRixFQUFRSSxJQUFSLENBQWEsZ0JBQWIsRUFBK0JZLE1BQS9CLEdBQXdDLENBQTVDLEVBQStDO0FBQzlDaEIsTUFBRSxJQUFGLEVBQVFJLElBQVIsQ0FBYSxnQkFBYixFQUErQlcsSUFBL0IsQ0FBb0MsWUFBVztBQUM5QyxTQUFJRSwrQ0FBNkNqQixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLHVCQUFiLENBQTdDLG1JQUdZRSxFQUFFLElBQUYsRUFBUWtCLE9BQVIsQ0FBZ0JDLFNBSDVCLFNBQUo7O0FBS0FuQixPQUFFLElBQUYsRUFBUW9CLFdBQVIsQ0FBb0JILElBQXBCO0FBQ0EsS0FQRDtBQVFBLElBVEQsTUFTTztBQUNOLFNBQUtJLFNBQUwsdURBQW1FLEtBQUtGLFNBQXhFO0FBQ0E7QUFDRCxHQWpCRDs7QUFtQkFSLGtCQUFnQkwsSUFBaEIsQ0FBcUIsU0FBckIsRUFBZ0MsQ0FBQ04sRUFBRSxJQUFGLEVBQy9CSSxJQUQrQixDQUMxQix5Q0FEMEIsRUFFL0JFLElBRitCLENBRTFCLFNBRjBCLENBQWpDLEVBR0VDLE9BSEYsQ0FHVSxRQUhWOztBQUtBLE1BQU1lLHNCQUFzQlQsVUFBVVQsSUFBVixDQUFlLFdBQWYsQ0FBNUI7QUFDQSxNQUFNbUIsY0FBY3ZCLEVBQUUsSUFBRixDQUFwQjtBQUNBLE1BQU13QixjQUFjRixvQkFBb0JsQixJQUFwQixDQUF5Qix3QkFBekIsQ0FBcEI7QUFDQW1CLGNBQVlFLFFBQVosQ0FBcUIsUUFBckI7QUFDQUQsY0FBWUUsV0FBWixDQUF3QixRQUF4QjtBQUNBQyxNQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLGdCQUF6QixDQUEwQ1IsbUJBQTFDLEVBQStERSxXQUEvRDtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNPLHdCQUFULEdBQW9DO0FBQ25DLE1BQU1DLGtCQUFrQmhDLEVBQUUsSUFBRixDQUF4QjtBQUNBLE1BQU1pQywyQkFBMkJsQyxNQUFNSyxJQUFOLENBQVcsaURBQVgsQ0FBakM7O0FBRUEsTUFBSTZCLHlCQUF5QmpCLE1BQTdCLEVBQXFDO0FBQ3BDLE9BQU1rQix1QkFBdUJsQyxFQUFFLHlCQUFGLENBQTdCO0FBQ0EsT0FBTW1DLGtCQUFrQkQscUJBQXFCOUIsSUFBckIsQ0FBMEIsaUNBQTFCLENBQXhCO0FBQ0E0QixtQkFBZ0JQLFFBQWhCLENBQXlCLFFBQXpCO0FBQ0FVLG1CQUFnQlQsV0FBaEIsQ0FBNEIsUUFBNUI7QUFDQUMsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxnQkFBekIsQ0FBMENJLG9CQUExQyxFQUFnRUMsZUFBaEU7QUFDQTs7QUFFREYsMkJBQXlCbEIsSUFBekIsQ0FBOEIsWUFBVztBQUN4QyxPQUFNRixZQUFZYixFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixDQUFsQjtBQUNBLE9BQU1zQixxQkFBcUJ2QixVQUFVVCxJQUFWLENBQWUscUJBQWYsQ0FBM0I7QUFDQSxPQUFNb0IsY0FBY1ksbUJBQW1CaEMsSUFBbkIsQ0FBd0Isd0JBQXhCLENBQXBCO0FBQ0EsT0FBTW1CLGNBQWNhLG1CQUFtQmhDLElBQW5CLENBQXdCLHNCQUF4QixDQUFwQjs7QUFFQW1CLGVBQVlFLFFBQVosQ0FBcUIsUUFBckI7QUFDQUQsZUFBWUUsV0FBWixDQUF3QixRQUF4QjtBQUNBQyxPQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLGdCQUF6QixDQUEwQ00sa0JBQTFDLEVBQThEWixXQUE5RDs7QUFFQVgsYUFBVVQsSUFBVixDQUFlLGFBQWYsRUFBOEJXLElBQTlCLENBQW1DLFlBQVc7QUFDN0MsUUFBSWYsRUFBRSxJQUFGLEVBQVFJLElBQVIsQ0FBYSxZQUFiLEVBQTJCWSxNQUEzQixHQUFvQyxDQUF4QyxFQUEyQztBQUMxQztBQUNBOztBQUVELFFBQUloQixFQUFFLElBQUYsRUFBUUksSUFBUixDQUFhLGdCQUFiLEVBQStCWSxNQUEvQixHQUF3QyxDQUE1QyxFQUErQztBQUM5Q2hCLE9BQUUsSUFBRixFQUFRSSxJQUFSLENBQWEsZ0JBQWIsRUFBK0JXLElBQS9CLENBQW9DLFlBQVc7QUFDOUMsVUFBSUUsK0NBQTZDakIsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSx1QkFBYixDQUE3Qyx5SUFHWUUsRUFBRSxJQUFGLEVBQVFrQixPQUFSLENBQWdCQyxTQUg1QixTQUFKOztBQUtBbkIsUUFBRSxJQUFGLEVBQVFvQixXQUFSLENBQW9CSCxJQUFwQjtBQUNBLE1BUEQ7QUFRQSxLQVRELE1BU087QUFDTixVQUFLSSxTQUFMLHVEQUFtRSxLQUFLRixTQUF4RTtBQUNBO0FBQ0QsSUFqQkQ7QUFvQkEsR0E5QkQ7QUErQkE7O0FBRUQ7OztBQUdBLFVBQVNrQix5QkFBVCxHQUFxQztBQUNwQyxNQUFNQyw2QkFBNkJ2QyxNQUFNZSxPQUFOLENBQWMsbUJBQWQsRUFBbUNWLElBQW5DLENBQXdDLGtDQUF4QyxDQUFuQztBQUNBLE1BQU1TLFlBQVliLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLENBQWxCOztBQUVBLE1BQUlmLE1BQU1LLElBQU4sQ0FBVyxpREFBWCxFQUE4RFksTUFBOUQsR0FBdUUsQ0FBM0UsRUFBOEU7QUFDN0VzQiw4QkFBMkJaLFdBQTNCLENBQXVDLFVBQXZDO0FBQ0EsR0FGRCxNQUVPO0FBQ05ZLDhCQUEyQmIsUUFBM0IsQ0FBb0MsVUFBcEM7O0FBRUEsT0FBTVMsdUJBQXVCbEMsRUFBRSx5QkFBRixDQUE3QjtBQUNBLE9BQU1tQyxrQkFBa0JELHFCQUFxQjlCLElBQXJCLENBQTBCLGlDQUExQixDQUF4Qjs7QUFFQSxPQUFJLENBQUMrQixnQkFBZ0JJLFFBQWhCLENBQXlCLFFBQXpCLENBQUwsRUFBeUM7QUFDeEMsUUFBTVAsa0JBQWtCRSxxQkFBcUI5QixJQUFyQixDQUEwQiwyQkFBMUIsQ0FBeEI7QUFDQTRCLG9CQUFnQk4sV0FBaEIsQ0FBNEIsUUFBNUI7QUFDQVMsb0JBQWdCVixRQUFoQixDQUF5QixRQUF6QjtBQUNBRSxRQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLGdCQUF6QixDQUEwQ0ksb0JBQTFDLEVBQWdFRixlQUFoRTtBQUNBO0FBQ0Q7O0FBRURoQyxJQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixFQUFzQlYsSUFBdEIsQ0FBMkIsZ0JBQTNCLEVBQTZDVyxJQUE3QyxDQUFrRCxZQUFXO0FBQzVELE9BQUlmLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsdUJBQWIsS0FBeUMsS0FBN0MsRUFBb0Q7QUFDbkRFLE1BQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLEVBQXNCVyxRQUF0QixDQUErQixVQUEvQjtBQUNBO0FBQ0QsR0FKRDs7QUFNQSxNQUFJLENBQUN6QixFQUFFLElBQUYsRUFBUU0sSUFBUixDQUFhLFNBQWIsQ0FBTCxFQUE4QjtBQUM3QmtDLHNCQUFtQnhDLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLENBQW5COztBQUVBLE9BQU1RLHNCQUFzQlQsVUFBVVQsSUFBVixDQUFlLHFCQUFmLENBQTVCO0FBQ0EsT0FBTW9CLGNBQWNGLG9CQUFvQmxCLElBQXBCLENBQXlCLHdCQUF6QixDQUFwQjs7QUFFQSxPQUFJLENBQUNvQixZQUFZZSxRQUFaLENBQXFCLFFBQXJCLENBQUwsRUFBcUM7QUFDcEMsUUFBTWhCLGNBQWNWLFVBQVVULElBQVYsQ0FBZSxzQkFBZixDQUFwQjtBQUNBbUIsZ0JBQVlHLFdBQVosQ0FBd0IsUUFBeEI7QUFDQUYsZ0JBQVlDLFFBQVosQ0FBcUIsUUFBckI7QUFDQUUsUUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxnQkFBekIsQ0FBMENSLG1CQUExQyxFQUErREMsWUFBWWtCLElBQVosRUFBL0Q7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQ7OztBQUdBLFVBQVNDLGNBQVQsR0FBMEI7QUFDekIsTUFBTUMsWUFBWTNDLEVBQUUsSUFBRixFQUFRNEMsT0FBUixDQUFnQixPQUFoQixFQUF5QnhDLElBQXpCLENBQThCLDhCQUE5QixDQUFsQjtBQUNBLE1BQU02QiwyQkFBMkJsQyxNQUFNSyxJQUFOLENBQVcsaURBQVgsQ0FBakM7O0FBRUEsTUFBSXVDLFVBQVVFLEdBQVYsTUFBbUIsV0FBdkIsRUFBb0M7QUFDbkNGLGFBQVVFLEdBQVYsQ0FBYyxhQUFkO0FBQ0EsR0FGRCxNQUVPO0FBQ05GLGFBQVVFLEdBQVYsQ0FBYyxXQUFkO0FBQ0E7O0FBRURaLDJCQUF5QmxCLElBQXpCLENBQThCLFlBQVc7QUFDeENmLEtBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsU0FBYixFQUF3QixDQUFDTixFQUFFLElBQUYsRUFDdkJNLElBRHVCLENBQ2xCLFNBRGtCLENBQXpCLEVBRUVDLE9BRkYsQ0FFVSxRQUZWOztBQUlBaUMsc0JBQW1CeEMsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbkI7QUFDQSxHQU5EOztBQVFBZ0M7QUFDQTs7QUFFRDs7O0FBR0EsVUFBU0EsaUJBQVQsR0FBNkI7QUFDNUIsTUFBSTlDLEVBQUUsSUFBRixFQUFRNkMsR0FBUixNQUFpQixXQUFyQixFQUFrQztBQUNqQzlDLFNBQU1LLElBQU4sQ0FBVyxzQkFBWCxFQUFtQzJDLElBQW5DLENBQXdDLFFBQXhDLEVBQWtELElBQWxEO0FBQ0FoRCxTQUFNSyxJQUFOLENBQVcsb0JBQVgsRUFBaUMyQyxJQUFqQyxDQUFzQyxRQUF0QyxFQUFnRCxLQUFoRDtBQUNBaEQsU0FBTUssSUFBTixDQUFXLHlCQUFYLEVBQXNDcUIsUUFBdEMsQ0FBK0MsV0FBL0M7QUFDQSxHQUpELE1BSU87QUFDTjFCLFNBQU1LLElBQU4sQ0FBVyxzQkFBWCxFQUFtQzJDLElBQW5DLENBQXdDLFFBQXhDLEVBQWtELEtBQWxEO0FBQ0FoRCxTQUFNSyxJQUFOLENBQVcsb0JBQVgsRUFBaUMyQyxJQUFqQyxDQUFzQyxRQUF0QyxFQUFnRCxJQUFoRDtBQUNBaEQsU0FBTUssSUFBTixDQUFXLHlCQUFYLEVBQXNDc0IsV0FBdEMsQ0FBa0QsV0FBbEQ7QUFDQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVNjLGtCQUFULENBQTRCUSxJQUE1QixFQUFrQztBQUNqQyxNQUFNQyxXQUFXbEQsTUFBTW1ELFNBQU4sR0FBa0JDLEdBQWxCLENBQXNCSCxJQUF0QixFQUE0QkksS0FBNUIsRUFBakI7O0FBRUFKLE9BQUs1QyxJQUFMLENBQVUsK0JBQVYsRUFBMkNXLElBQTNDLENBQWdELFlBQVc7QUFDMUQsT0FBTXNDLFFBQVFyRCxFQUFFLElBQUYsRUFBUTRDLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBZDtBQUNBLE9BQU1VLGNBQWN2RCxNQUFNbUQsU0FBTixHQUFrQkssTUFBbEIsQ0FBeUJGLEtBQXpCLEVBQWdDRCxLQUFoQyxFQUFwQjs7QUFFQSxRQUFLSSxhQUFMLENBQW1CbkMsU0FBbkIsR0FBK0J0QixNQUFNbUQsU0FBTixHQUFrQk8sSUFBbEIsQ0FBdUJSLFFBQXZCLEVBQWlDSyxXQUFqQyxFQUE4Q3hELElBQTlDLEVBQS9CO0FBQ0EsR0FMRDs7QUFPQWtELE9BQUs1QyxJQUFMLENBQVUseUJBQVYsRUFBcUNXLElBQXJDLENBQTBDLFlBQVc7QUFDcEQsT0FBTXNDLFFBQVFyRCxFQUFFLElBQUYsRUFBUTRDLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBZDtBQUNBLE9BQU1VLGNBQWN2RCxNQUFNbUQsU0FBTixHQUFrQkssTUFBbEIsQ0FBeUJGLEtBQXpCLEVBQWdDRCxLQUFoQyxFQUFwQjtBQUNBLE9BQU1NLFdBQVczRCxNQUFNbUQsU0FBTixHQUFrQk8sSUFBbEIsQ0FBdUJSLFFBQXZCLEVBQWlDSyxXQUFqQyxFQUE4Q3hELElBQTlDLEVBQWpCOztBQUVBLE9BQUltQixnRkFDK0J5QyxTQUFTQyxXQUR4QywrREFFaUMzRCxFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLHVCQUFiLENBRmpDLHFFQUc4QzRELFNBQVNFLFlBSHZELDZCQUFKOztBQU9BNUQsS0FBRSxJQUFGLEVBQVE0QyxPQUFSLENBQWdCLGdCQUFoQixFQUFrQ3hCLFdBQWxDLDhCQUF5RUgsSUFBekU7QUFDQSxHQWJEO0FBY0E7O0FBRUQ7Ozs7O0FBS0EsVUFBUzRDLHFCQUFULEdBQWlDO0FBQ2hDLE1BQU1iLE9BQU9oRCxFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixDQUFiO0FBQ0EsTUFBTW1DLFdBQVdsRCxNQUFNbUQsU0FBTixHQUFrQkMsR0FBbEIsQ0FBc0JILElBQXRCLEVBQTRCSSxLQUE1QixFQUFqQjs7QUFFQUosT0FBSzVDLElBQUwsQ0FBVSxZQUFWLEVBQXdCVyxJQUF4QixDQUE2QixZQUFXO0FBQ3ZDLE9BQU1zQyxRQUFRckQsRUFBRSxJQUFGLEVBQVE0QyxPQUFSLENBQWdCLElBQWhCLENBQWQ7QUFDQSxPQUFNVSxjQUFjdkQsTUFBTW1ELFNBQU4sR0FBa0JLLE1BQWxCLENBQXlCRixLQUF6QixFQUFnQ0QsS0FBaEMsRUFBcEI7O0FBRUEsT0FBSXBELEVBQUU4RCxJQUFGLENBQU85RCxFQUFFLElBQUYsRUFBUTZDLEdBQVIsRUFBUCxLQUF5QjlDLE1BQU1tRCxTQUFOLEdBQWtCTyxJQUFsQixDQUF1QlIsUUFBdkIsRUFBaUNLLFdBQWpDLEVBQThDeEQsSUFBOUMsRUFBN0IsRUFBbUY7QUFDbEZFLE1BQUUsSUFBRixFQUFReUIsUUFBUixDQUFpQixVQUFqQjs7QUFFQTtBQUNBOztBQUVEekIsS0FBRSxJQUFGLEVBQVEwQixXQUFSLENBQW9CLFVBQXBCO0FBQ0EsR0FYRDtBQVlBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTlCLFFBQU9tRSxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCakUsUUFDRWtFLEVBREYsQ0FDSyxPQURMLEVBQ2MsVUFEZCxFQUMwQnpELGdCQUQxQixFQUVFeUQsRUFGRixDQUVLLE9BRkwsRUFFYyxzQkFGZCxFQUVzQ3JELG9CQUZ0QyxFQUdFcUQsRUFIRixDQUdLLFFBSEwsRUFHZSw0QkFIZixFQUc2Q2hFLHNCQUg3QyxFQUlFZ0UsRUFKRixDQUlLLE9BSkwsRUFJYyxxQkFKZCxFQUlxQ0oscUJBSnJDOztBQU1BOUQsUUFBTWUsT0FBTixDQUFjLG1CQUFkLEVBQ0VtRCxFQURGLENBQ0ssUUFETCxFQUNlLHlDQURmLEVBQzBENUIseUJBRDFELEVBRUU0QixFQUZGLENBRUssUUFGTCxFQUVlLDhCQUZmLEVBRStDbkIsaUJBRi9DLEVBR0VtQixFQUhGLENBR0ssT0FITCxFQUdjLDBCQUhkLEVBRzBDdkIsY0FIMUMsRUFJRXVCLEVBSkYsQ0FJSyxPQUpMLEVBSWMsc0NBSmQsRUFJc0RsQyx3QkFKdEQ7O0FBTUFpQztBQUNBLEVBZEQ7O0FBZ0JBLFFBQU9wRSxNQUFQO0FBQ0EsQ0FwVEQiLCJmaWxlIjoicXVpY2tfZWRpdC9tb2RhbHMvcHJvcGVydGllcy9wcm9wZXJ0aWVzX2V2ZW50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZXZlbnRzLmpzIDIwMTYtMTAtMTJcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIFByb3BlcnRpZXMgVGFibGUgRXZlbnRzIENvbnRyb2xsZXJcbiAqXG4gKiBIYW5kbGVzIHRoZSBldmVudHMgb2YgdGhlIHByb3BlcnRpZXMgdGFibGUuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZSgncHJvcGVydGllc19ldmVudHMnLCBbJ2xvYWRpbmdfc3Bpbm5lcicsIGAke2d4LnNvdXJjZX0vbGlicy9idXR0b25fZHJvcGRvd25gXSwgZnVuY3Rpb24oZGF0YSkge1xuXHRcblx0J3VzZSBzdHJpY3QnO1xuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIFZBUklBQkxFU1xuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XG5cdC8qKlxuXHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0ICpcblx0ICogQHR5cGUge2pRdWVyeX1cblx0ICovXG5cdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XG5cdC8qKlxuXHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0ICpcblx0ICogQHR5cGUge09iamVjdH1cblx0ICovXG5cdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBGVU5DVElPTlNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogQnVsayBzZWxlY3Rpb24gY2hhbmdlIGV2ZW50IGhhbmRsZXIuXG5cdCAqXG5cdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBDb250YWlucyBldmVudCBpbmZvcm1hdGlvbi5cblx0ICogQHBhcmFtIHtCb29sZWFufSBbcHJvcGFnYXRlID0gdHJ1ZV0gV2hldGhlciB0byBwcm9wYWdhdGUgdGhlIGV2ZW50IG9yIG5vdC5cblx0ICovXG5cdGZ1bmN0aW9uIF9vbkJ1bGtTZWxlY3Rpb25DaGFuZ2UoZXZlbnQsIHByb3BhZ2F0ZSA9IHRydWUpIHtcblx0XHRpZiAocHJvcGFnYXRlID09PSBmYWxzZSkge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblx0XHRcblx0XHQkdGhpcy5maW5kKCd0Ym9keSBpbnB1dDpjaGVja2JveC5wcm9wZXJ0aWVzLXJvdy1zZWxlY3Rpb24nKVxuXHRcdFx0LnNpbmdsZV9jaGVja2JveCgnY2hlY2tlZCcsICQodGhpcykucHJvcCgnY2hlY2tlZCcpKVxuXHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScpO1xuXHR9XG5cdFxuXHQvKipcblx0ICogVGFibGUgcm93IGNsaWNrIGV2ZW50IGhhbmRsZXIuXG5cdCAqXG5cdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBDb250YWlucyBldmVudCBpbmZvcm1hdGlvbi5cblx0ICovXG5cdGZ1bmN0aW9uIF9vblRhYmxlUm93Q2xpY2soZXZlbnQpIHtcblx0XHRpZiAoISQoZXZlbnQudGFyZ2V0KS5pcygndGQnKSkge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblx0XHRcblx0XHRjb25zdCAkc2luZ2xlQ2hlY2tib3ggPSAkKHRoaXMpLmZpbmQoJ2lucHV0OmNoZWNrYm94LnByb3BlcnRpZXMtcm93LXNlbGVjdGlvbicpO1xuXHRcdFxuXHRcdCRzaW5nbGVDaGVja2JveC5wcm9wKCdjaGVja2VkJywgISQodGhpcylcblx0XHRcdC5maW5kKCdpbnB1dDpjaGVja2JveC5wcm9wZXJ0aWVzLXJvdy1zZWxlY3Rpb24nKVxuXHRcdFx0LnByb3AoJ2NoZWNrZWQnKSlcblx0XHRcdC50cmlnZ2VyKCdjaGFuZ2UnKTtcblx0fVxuXHRcblx0LyoqXG5cdCAqIEVuYWJsZXMgcm93IGVkaXRpbmcgbW9kZS5cblx0ICovXG5cdGZ1bmN0aW9uIF9vblRhYmxlUm93RWRpdENsaWNrKCkge1xuXHRcdGNvbnN0ICR0YWJsZVJvdyA9ICQodGhpcykucGFyZW50cygndHInKTtcblx0XHRjb25zdCAkc2luZ2xlQ2hlY2tib3ggPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZmluZCgnaW5wdXQ6Y2hlY2tib3gucHJvcGVydGllcy1yb3ctc2VsZWN0aW9uJyk7XG5cdFx0XG5cdFx0JHRhYmxlUm93LmZpbmQoJ3RkLmVkaXRhYmxlJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdGlmICgkKHRoaXMpLmZpbmQoJ2lucHV0OnRleHQnKS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKCQodGhpcykuZmluZCgncC52YWx1ZXNfcHJpY2UnKS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdCQodGhpcykuZmluZCgncC52YWx1ZXNfcHJpY2UnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdGxldCBodG1sID0gYDxpbnB1dCBkYXRhLXByb3BlcnRpZXMtcHJpY2UtdHlwZT1cIiR7JCh0aGlzKS5kYXRhKCdwcm9wZXJ0aWVzLXByaWNlLXR5cGUnKX1cIlxuXHRcdFx0XHRcdFx0XHRcdFx0ICAgdHlwZT1cInRleHRcIiBcblx0XHRcdFx0XHRcdFx0XHRcdCAgIGNsYXNzPVwiZm9ybS1jb250cm9sIHZhbHVlcy1wcmljZVwiIFxuXHRcdFx0XHRcdFx0XHRcdFx0ICAgdmFsdWU9XCIkeyQodGhpcykuY29udGV4dC5pbm5lclRleHR9XCIgLz5gO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCQodGhpcykucmVwbGFjZVdpdGgoaHRtbCk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0dGhpcy5pbm5lckhUTUwgPSBgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiB2YWx1ZT1cIiR7dGhpcy5pbm5lclRleHR9XCIgLz5gO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdCRzaW5nbGVDaGVja2JveC5wcm9wKCdjaGVja2VkJywgISQodGhpcylcblx0XHRcdC5maW5kKCdpbnB1dDpjaGVja2JveC5wcm9wZXJ0aWVzLXJvdy1zZWxlY3Rpb24nKVxuXHRcdFx0LnByb3AoJ2NoZWNrZWQnKSlcblx0XHRcdC50cmlnZ2VyKCdjaGFuZ2UnKTtcblx0XHRcblx0XHRjb25zdCAkcm93QWN0aW9uc0Ryb3Bkb3duID0gJHRhYmxlUm93LmZpbmQoJy5kcm9wZG93bicpO1xuXHRcdGNvbnN0ICRlZGl0QWN0aW9uID0gJCh0aGlzKTtcblx0XHRjb25zdCAkc2F2ZUFjdGlvbiA9ICRyb3dBY3Rpb25zRHJvcGRvd24uZmluZCgnLnNhdmUtcHJvcGVydGllcy1lZGl0cycpO1xuXHRcdCRlZGl0QWN0aW9uLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHQkc2F2ZUFjdGlvbi5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLnNldERlZmF1bHRBY3Rpb24oJHJvd0FjdGlvbnNEcm9wZG93biwgJHNhdmVBY3Rpb24pO1xuXHR9XG5cdFxuXHQvKipcblx0ICogQnVsayByb3cgZWRpdCBldmVudCBoYW5kbGVyLlxuXHQgKlxuXHQgKiBFbmFibGVzIHRoZSBlZGl0IG1vZGUgZm9yIHRoZSBzZWxlY3RlZCByb3dzLlxuXHQgKi9cblx0ZnVuY3Rpb24gX29uVGFibGVCdWxrUm93RWRpdENsaWNrKCkge1xuXHRcdGNvbnN0ICRidWxrRWRpdEFjdGlvbiA9ICQodGhpcyk7XG5cdFx0Y29uc3QgJGNoZWNrZWRTaW5nbGVDaGVja2JveGVzID0gJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZC5wcm9wZXJ0aWVzLXJvdy1zZWxlY3Rpb24nKTtcblx0XHRcblx0XHRpZiAoJGNoZWNrZWRTaW5nbGVDaGVja2JveGVzLmxlbmd0aCkge1xuXHRcdFx0Y29uc3QgJGJ1bGtBY3Rpb25zRHJvcGRvd24gPSAkKCcucHJvcGVydGllcy1idWxrLWFjdGlvbicpO1xuXHRcdFx0Y29uc3QgJGJ1bGtTYXZlQWN0aW9uID0gJGJ1bGtBY3Rpb25zRHJvcGRvd24uZmluZCgnLnNhdmUtcHJvcGVydGllcy1idWxrLXJvdy1lZGl0cycpO1xuXHRcdFx0JGJ1bGtFZGl0QWN0aW9uLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHRcdCRidWxrU2F2ZUFjdGlvbi5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uc2V0RGVmYXVsdEFjdGlvbigkYnVsa0FjdGlvbnNEcm9wZG93biwgJGJ1bGtTYXZlQWN0aW9uKTtcblx0XHR9XG5cdFx0XG5cdFx0JGNoZWNrZWRTaW5nbGVDaGVja2JveGVzLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRjb25zdCAkdGFibGVSb3cgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJyk7XG5cdFx0XHRjb25zdCAkcm93QWN0aW9uRHJvcGRvd24gPSAkdGFibGVSb3cuZmluZCgnLmJ0bi1ncm91cC5kcm9wZG93bicpO1xuXHRcdFx0Y29uc3QgJHNhdmVBY3Rpb24gPSAkcm93QWN0aW9uRHJvcGRvd24uZmluZCgnLnNhdmUtcHJvcGVydGllcy1lZGl0cycpO1xuXHRcdFx0Y29uc3QgJGVkaXRBY3Rpb24gPSAkcm93QWN0aW9uRHJvcGRvd24uZmluZCgnLnJvdy1wcm9wZXJ0aWVzLWVkaXQnKTtcblx0XHRcdFxuXHRcdFx0JGVkaXRBY3Rpb24uYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0JHNhdmVBY3Rpb24ucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLnNldERlZmF1bHRBY3Rpb24oJHJvd0FjdGlvbkRyb3Bkb3duLCAkc2F2ZUFjdGlvbik7XG5cdFx0XHRcblx0XHRcdCR0YWJsZVJvdy5maW5kKCd0ZC5lZGl0YWJsZScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGlmICgkKHRoaXMpLmZpbmQoJ2lucHV0OnRleHQnKS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoJCh0aGlzKS5maW5kKCdwLnZhbHVlc19wcmljZScpLmxlbmd0aCA+IDApIHtcblx0XHRcdFx0XHQkKHRoaXMpLmZpbmQoJ3AudmFsdWVzX3ByaWNlJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdGxldCBodG1sID0gYDxpbnB1dCBkYXRhLXByb3BlcnRpZXMtcHJpY2UtdHlwZT1cIiR7JCh0aGlzKS5kYXRhKCdwcm9wZXJ0aWVzLXByaWNlLXR5cGUnKX1cIlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQgICB0eXBlPVwidGV4dFwiIFxuXHRcdFx0XHRcdFx0XHRcdFx0XHQgICBjbGFzcz1cImZvcm0tY29udHJvbCB2YWx1ZXMtcHJpY2VcIiBcblx0XHRcdFx0XHRcdFx0XHRcdFx0ICAgdmFsdWU9XCIkeyQodGhpcykuY29udGV4dC5pbm5lclRleHR9XCIgLz5gO1xuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHQkKHRoaXMpLnJlcGxhY2VXaXRoKGh0bWwpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHRoaXMuaW5uZXJIVE1MID0gYDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgdmFsdWU9XCIke3RoaXMuaW5uZXJUZXh0fVwiIC8+YDtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdFxuXHRcdH0pO1xuXHR9XG5cdFxuXHQvKipcblx0ICogVGFibGUgcm93IGNoZWNrYm94IGNoYW5nZSBldmVudCBoYW5kbGVyLlxuXHQgKi9cblx0ZnVuY3Rpb24gX29uVGFibGVSb3dDaGVja2JveENoYW5nZSgpIHtcblx0XHRjb25zdCAkYnVsa0FjdGlvbkRyb3Bkb3duQnV0dG9ucyA9ICR0aGlzLnBhcmVudHMoJy5wcm9wZXJ0aWVzLm1vZGFsJykuZmluZCgnLnByb3BlcnRpZXMtYnVsay1hY3Rpb24gPiBidXR0b24nKTtcblx0XHRjb25zdCAkdGFibGVSb3cgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJyk7XG5cdFx0XG5cdFx0aWYgKCR0aGlzLmZpbmQoJ2lucHV0OmNoZWNrYm94OmNoZWNrZWQucHJvcGVydGllcy1yb3ctc2VsZWN0aW9uJykubGVuZ3RoID4gMCkge1xuXHRcdFx0JGJ1bGtBY3Rpb25Ecm9wZG93bkJ1dHRvbnMucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdCRidWxrQWN0aW9uRHJvcGRvd25CdXR0b25zLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCAkYnVsa0FjdGlvbnNEcm9wZG93biA9ICQoJy5wcm9wZXJ0aWVzLWJ1bGstYWN0aW9uJyk7XG5cdFx0XHRjb25zdCAkYnVsa1NhdmVBY3Rpb24gPSAkYnVsa0FjdGlvbnNEcm9wZG93bi5maW5kKCcuc2F2ZS1wcm9wZXJ0aWVzLWJ1bGstcm93LWVkaXRzJyk7XG5cdFx0XHRcblx0XHRcdGlmICghJGJ1bGtTYXZlQWN0aW9uLmhhc0NsYXNzKCdoaWRkZW4nKSkge1xuXHRcdFx0XHRjb25zdCAkYnVsa0VkaXRBY3Rpb24gPSAkYnVsa0FjdGlvbnNEcm9wZG93bi5maW5kKCcucHJvcGVydGllcy1idWxrLXJvdy1lZGl0Jyk7XG5cdFx0XHRcdCRidWxrRWRpdEFjdGlvbi5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcdCRidWxrU2F2ZUFjdGlvbi5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5zZXREZWZhdWx0QWN0aW9uKCRidWxrQWN0aW9uc0Ryb3Bkb3duLCAkYnVsa0VkaXRBY3Rpb24pO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQkKHRoaXMpLnBhcmVudHMoJ3RyJykuZmluZCgncC52YWx1ZXNfcHJpY2UnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0aWYgKCQodGhpcykuZGF0YSgncHJvcGVydGllcy1wcmljZS10eXBlJykgPT0gJ2ZpeCcpIHtcblx0XHRcdFx0JCh0aGlzKS5wYXJlbnRzKCd0ZCcpLmFkZENsYXNzKCdlZGl0YWJsZScpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdGlmICghJCh0aGlzKS5wcm9wKCdjaGVja2VkJykpIHtcblx0XHRcdF9yZXNvbHZlUm93Q2hhbmdlcygkKHRoaXMpLnBhcmVudHMoJ3RyJykpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCAkcm93QWN0aW9uc0Ryb3Bkb3duID0gJHRhYmxlUm93LmZpbmQoJy5idG4tZ3JvdXAuZHJvcGRvd24nKTtcblx0XHRcdGNvbnN0ICRzYXZlQWN0aW9uID0gJHJvd0FjdGlvbnNEcm9wZG93bi5maW5kKCcuc2F2ZS1wcm9wZXJ0aWVzLWVkaXRzJyk7XG5cdFx0XHRcblx0XHRcdGlmICghJHNhdmVBY3Rpb24uaGFzQ2xhc3MoJ2hpZGRlbicpKSB7XG5cdFx0XHRcdGNvbnN0ICRlZGl0QWN0aW9uID0gJHRhYmxlUm93LmZpbmQoJy5yb3ctcHJvcGVydGllcy1lZGl0Jyk7XG5cdFx0XHRcdCRlZGl0QWN0aW9uLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0JHNhdmVBY3Rpb24uYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uc2V0RGVmYXVsdEFjdGlvbigkcm93QWN0aW9uc0Ryb3Bkb3duLCAkZWRpdEFjdGlvbi5sYXN0KCkpO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXHRcblx0LyoqXG5cdCAqIENhbmNlbCBkYXRhIG1vZGlmaWNhdGlvbnMgZXZlbnQgaGFuZGxlci5cblx0ICovXG5cdGZ1bmN0aW9uIF9vbkNhbmNlbENsaWNrKCkge1xuXHRcdGNvbnN0ICRwYWdlTW9kZSA9ICQodGhpcykuY2xvc2VzdCgndGhlYWQnKS5maW5kKCcuc2VsZWN0LXByb3BlcnRpZXMtcGFnZS1tb2RlJyk7XG5cdFx0Y29uc3QgJGNoZWNrZWRTaW5nbGVDaGVja2JveGVzID0gJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZC5wcm9wZXJ0aWVzLXJvdy1zZWxlY3Rpb24nKTtcblx0XHRcblx0XHRpZiAoJHBhZ2VNb2RlLnZhbCgpID09ICdlZGl0LW1vZGUnKSB7XG5cdFx0XHQkcGFnZU1vZGUudmFsKCdmaWx0ZXItbW9kZScpXG5cdFx0fSBlbHNlIHtcblx0XHRcdCRwYWdlTW9kZS52YWwoJ2VkaXQtbW9kZScpXG5cdFx0fVxuXHRcdFxuXHRcdCRjaGVja2VkU2luZ2xlQ2hlY2tib3hlcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0JCh0aGlzKS5wcm9wKCdjaGVja2VkJywgISQodGhpcylcblx0XHRcdFx0LnByb3AoJ2NoZWNrZWQnKSlcblx0XHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScpO1xuXHRcdFx0XG5cdFx0XHRfcmVzb2x2ZVJvd0NoYW5nZXMoJCh0aGlzKS5wYXJlbnRzKCd0cicpKTtcblx0XHR9KTtcblx0XHRcblx0XHRfb25QYWdlTW9kZUNoYW5nZSgpO1xuXHR9XG5cdFxuXHQvKipcblx0ICogUGFnZSBtb2RlIGNoYW5nZSBiZXR3ZWVuIFwiZWRpdFwiIGFuZCBcImZpbHRlcmluZ1wiLlxuXHQgKi9cblx0ZnVuY3Rpb24gX29uUGFnZU1vZGVDaGFuZ2UoKSB7XG5cdFx0aWYgKCQodGhpcykudmFsKCkgPT0gJ2VkaXQtbW9kZScpIHtcblx0XHRcdCR0aGlzLmZpbmQoJ3RyLnByb3BlcnRpZXMtZmlsdGVyJykuYXR0cignaGlkZGVuJywgdHJ1ZSk7XG5cdFx0XHQkdGhpcy5maW5kKCd0ci5wcm9wZXJ0aWVzLWVkaXQnKS5hdHRyKCdoaWRkZW4nLCBmYWxzZSk7XG5cdFx0XHQkdGhpcy5maW5kKCd0aGVhZCB0cjpmaXJzdC1jaGlsZCB0aCcpLmFkZENsYXNzKCdlZGl0LW1vZGUnKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0JHRoaXMuZmluZCgndHIucHJvcGVydGllcy1maWx0ZXInKS5hdHRyKCdoaWRkZW4nLCBmYWxzZSk7XG5cdFx0XHQkdGhpcy5maW5kKCd0ci5wcm9wZXJ0aWVzLWVkaXQnKS5hdHRyKCdoaWRkZW4nLCB0cnVlKTtcblx0XHRcdCR0aGlzLmZpbmQoJ3RoZWFkIHRyOmZpcnN0LWNoaWxkIHRoJykucmVtb3ZlQ2xhc3MoJ2VkaXQtbW9kZScpO1xuXHRcdH1cblx0fVxuXHRcblx0LyoqXG5cdCAqIFJlc3RvcmVzIGFsbCB0aGUgcm93IGRhdGEgY2hhbmdlcyBiYWNrIHRvIHRoZWlyIG9yaWdpbmFsIHN0YXRlLlxuXHQgKlxuXHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gJHJvdyBDdXJyZW50IHJvdyBzZWxlY3Rvci5cblx0ICovXG5cdGZ1bmN0aW9uIF9yZXNvbHZlUm93Q2hhbmdlcygkcm93KSB7XG5cdFx0Y29uc3Qgcm93SW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5yb3coJHJvdykuaW5kZXgoKTtcblx0XHRcblx0XHQkcm93LmZpbmQoJ2lucHV0OnRleHQ6bm90KC52YWx1ZXMtcHJpY2UpJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdGNvbnN0ICRjZWxsID0gJCh0aGlzKS5jbG9zZXN0KCd0ZCcpO1xuXHRcdFx0Y29uc3QgY29sdW1uSW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oJGNlbGwpLmluZGV4KCk7XG5cdFx0XHRcblx0XHRcdHRoaXMucGFyZW50RWxlbWVudC5pbm5lckhUTUwgPSAkdGhpcy5EYXRhVGFibGUoKS5jZWxsKHJvd0luZGV4LCBjb2x1bW5JbmRleCkuZGF0YSgpO1xuXHRcdH0pO1xuXHRcdFxuXHRcdCRyb3cuZmluZCgnaW5wdXQ6dGV4dC52YWx1ZXMtcHJpY2UnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0Y29uc3QgJGNlbGwgPSAkKHRoaXMpLmNsb3Nlc3QoJ3RkJyk7XG5cdFx0XHRjb25zdCBjb2x1bW5JbmRleCA9ICR0aGlzLkRhdGFUYWJsZSgpLmNvbHVtbigkY2VsbCkuaW5kZXgoKTtcblx0XHRcdGNvbnN0IGNlbGxEYXRhID0gJHRoaXMuRGF0YVRhYmxlKCkuY2VsbChyb3dJbmRleCwgY29sdW1uSW5kZXgpLmRhdGEoKTtcblx0XHRcdFxuXHRcdFx0bGV0IGh0bWwgPSBgPGRpdiBjbGFzcz1cImNvbC1sZy0xMlwiPlxuXHRcdFx0XHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJjb250cm9sLWxhYmVsXCI+JHtjZWxsRGF0YS52YWx1ZXNfbmFtZX08L2xhYmVsPlxuXHRcdFx0XHRcdFx0XHQ8cCBkYXRhLXByb3BlcnRpZXMtcHJpY2UtdHlwZT1cIiR7JCh0aGlzKS5kYXRhKCdwcm9wZXJ0aWVzLXByaWNlLXR5cGUnKX1cIlxuXHRcdFx0XHRcdFx0XHQgICBjbGFzcz1cImZvcm0tY29udHJvbC1zdGF0aWMgdmFsdWVzX3ByaWNlXCI+JHtjZWxsRGF0YS52YWx1ZXNfcHJpY2V9PC9wPlxuXHRcdFx0XHRcdFx0PC9kaXY+YDtcblx0XHRcdFxuXHRcdFx0XG5cdFx0XHQkKHRoaXMpLmNsb3Nlc3QoJ2Rpdi5mb3JtLWdyb3VwJykucmVwbGFjZVdpdGgoYDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+JHtodG1sfTwvZGl2PmApO1xuXHRcdH0pO1xuXHR9XG5cdFxuXHQvKipcblx0ICogVGFibGUgcm93IGRhdGEgY2hhbmdlIGV2ZW50IGhhbmRsZXIuXG5cdCAqXG5cdCAqIEl0J3MgYmVpbmcgdHJpZ2dlcmVkIGV2ZXJ5IHRpbWUgYSByb3cgaW5wdXQvc2VsZWN0IGZpZWxkIGlzIGNoYW5nZWQuXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25UYWJsZVJvd0RhdGFDaGFuZ2UoKSB7XG5cdFx0Y29uc3QgJHJvdyA9ICQodGhpcykucGFyZW50cygndHInKTtcblx0XHRjb25zdCByb3dJbmRleCA9ICR0aGlzLkRhdGFUYWJsZSgpLnJvdygkcm93KS5pbmRleCgpO1xuXHRcdFxuXHRcdCRyb3cuZmluZCgnaW5wdXQ6dGV4dCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRjb25zdCAkY2VsbCA9ICQodGhpcykuY2xvc2VzdCgndGQnKTtcblx0XHRcdGNvbnN0IGNvbHVtbkluZGV4ID0gJHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKCRjZWxsKS5pbmRleCgpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJC50cmltKCQodGhpcykudmFsKCkpICE9ICR0aGlzLkRhdGFUYWJsZSgpLmNlbGwocm93SW5kZXgsIGNvbHVtbkluZGV4KS5kYXRhKCkpIHtcblx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnbW9kaWZpZWQnKTtcblx0XHRcdFx0XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JCh0aGlzKS5yZW1vdmVDbGFzcygnbW9kaWZpZWQnKTtcblx0XHR9KTtcblx0fVxuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIElOSVRJQUxJWkFUSU9OXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0JHRoaXNcblx0XHRcdC5vbignY2xpY2snLCAndGJvZHkgdHInLCBfb25UYWJsZVJvd0NsaWNrKVxuXHRcdFx0Lm9uKCdjbGljaycsICcucm93LXByb3BlcnRpZXMtZWRpdCcsIF9vblRhYmxlUm93RWRpdENsaWNrKVxuXHRcdFx0Lm9uKCdjaGFuZ2UnLCAnLnByb3BlcnRpZXMtYnVsay1zZWxlY3Rpb24nLCBfb25CdWxrU2VsZWN0aW9uQ2hhbmdlKVxuXHRcdFx0Lm9uKCdrZXl1cCcsICd0Ym9keSB0ciBpbnB1dDp0ZXh0JywgX29uVGFibGVSb3dEYXRhQ2hhbmdlKTtcblx0XHRcblx0XHQkdGhpcy5wYXJlbnRzKCcucHJvcGVydGllcy5tb2RhbCcpXG5cdFx0XHQub24oJ2NoYW5nZScsICdpbnB1dDpjaGVja2JveC5wcm9wZXJ0aWVzLXJvdy1zZWxlY3Rpb24nLCBfb25UYWJsZVJvd0NoZWNrYm94Q2hhbmdlKVxuXHRcdFx0Lm9uKCdjaGFuZ2UnLCAnLnNlbGVjdC1wcm9wZXJ0aWVzLXBhZ2UtbW9kZScsIF9vblBhZ2VNb2RlQ2hhbmdlKVxuXHRcdFx0Lm9uKCdjbGljaycsICcuY2FuY2VsLXByb3BlcnRpZXMtZWRpdHMnLCBfb25DYW5jZWxDbGljaylcblx0XHRcdC5vbignY2xpY2snLCAnLmJ0bi1ncm91cCAucHJvcGVydGllcy1idWxrLXJvdy1lZGl0JywgX29uVGFibGVCdWxrUm93RWRpdENsaWNrKTtcblx0XHRcblx0XHRkb25lKCk7XG5cdH07XG5cdFxuXHRyZXR1cm4gbW9kdWxlO1xufSk7XG5cbiJdfQ==
