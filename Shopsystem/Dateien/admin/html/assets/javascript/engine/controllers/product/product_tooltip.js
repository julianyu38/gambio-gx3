'use strict';

/* --------------------------------------------------------------
 product_tooltip.js 2016-03-31
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Product Tooltip
 *
 * This controller displays a tooltip when hovering the product name.
 *
 * Use attribute 'data-product_tooltip-image-url' to set the image url to load.
 * Use attribute 'data-product_tooltip-description' to set the text content.
 *
 * @module Controllers/product_tooltip
 */
gx.controllers.module('product_tooltip', [jse.source + '/vendor/qtip2/jquery.qtip.min.css', jse.source + '/vendor/qtip2/jquery.qtip.min.js'],

/**  @lends module:Controllers/product_tooltip */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {},


	/**
  * qTip plugin options object.
  * @type {Object}
  */
	tooltipOptions = {
		style: {
			classes: 'gx-container gx-qtip info large'
		},
		position: {
			my: 'left top',
			at: 'right bottom'
		}
	},


	/**
  * Hover trigger.
  * @type {jQuery}
  */
	$trigger = $this.find('[data-tooltip-trigger]');

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Prepares the tooltip content by fetching the image and setting the description.
  * @param {jQuery.Event} event Hover event.
  * @param {Object} api qTip plugin internal API.
  * @private
  */
	var _getContent = function _getContent(event, api) {
		var $content = $('<div/>');

		var $description = $.parseHTML(data.description);
		$content.append($description);

		// Fetch image.
		var image = new Image();
		$(image).load(function () {
			var $imageContainer = $('<div class="text-center" style="margin-bottom: 24px;"></div>');

			$imageContainer.append($('<br>')).prepend(image);

			$content.prepend($imageContainer);

			// Set new tooltip content.
			api.set('content.text', $content);
		}).attr({ src: data.imageUrl });

		return $content;
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		if ($trigger.length) {
			var options = $.extend(true, tooltipOptions, { content: { text: _getContent } });
			$trigger.qtip(options);
		} else {
			throw new Error('Could not find trigger element');
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2R1Y3QvcHJvZHVjdF90b29sdGlwLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwidG9vbHRpcE9wdGlvbnMiLCJzdHlsZSIsImNsYXNzZXMiLCJwb3NpdGlvbiIsIm15IiwiYXQiLCIkdHJpZ2dlciIsImZpbmQiLCJfZ2V0Q29udGVudCIsImV2ZW50IiwiYXBpIiwiJGNvbnRlbnQiLCIkZGVzY3JpcHRpb24iLCJwYXJzZUhUTUwiLCJkZXNjcmlwdGlvbiIsImFwcGVuZCIsImltYWdlIiwiSW1hZ2UiLCJsb2FkIiwiJGltYWdlQ29udGFpbmVyIiwicHJlcGVuZCIsInNldCIsImF0dHIiLCJzcmMiLCJpbWFnZVVybCIsImluaXQiLCJkb25lIiwibGVuZ3RoIiwib3B0aW9ucyIsImV4dGVuZCIsImNvbnRlbnQiLCJ0ZXh0IiwicXRpcCIsIkVycm9yIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7QUFVQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsaUJBREQsRUFHQyxDQUNDQyxJQUFJQyxNQUFKLEdBQWEsbUNBRGQsRUFFQ0QsSUFBSUMsTUFBSixHQUFhLGtDQUZkLENBSEQ7O0FBUUM7O0FBRUEsVUFBVUMsSUFBVixFQUFnQjs7QUFFZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBTCxVQUFTLEVBYlY7OztBQWVDOzs7O0FBSUFNLGtCQUFpQjtBQUNoQkMsU0FBTztBQUNOQyxZQUFTO0FBREgsR0FEUztBQUloQkMsWUFBVTtBQUNUQyxPQUFJLFVBREs7QUFFVEMsT0FBSTtBQUZLO0FBSk0sRUFuQmxCOzs7QUE2QkM7Ozs7QUFJQUMsWUFBV1IsTUFBTVMsSUFBTixDQUFXLHdCQUFYLENBakNaOztBQW1DQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BLEtBQUlDLGNBQWMsU0FBZEEsV0FBYyxDQUFVQyxLQUFWLEVBQWlCQyxHQUFqQixFQUFzQjtBQUN2QyxNQUFJQyxXQUFXWixFQUFFLFFBQUYsQ0FBZjs7QUFFQSxNQUFJYSxlQUFlYixFQUFFYyxTQUFGLENBQVloQixLQUFLaUIsV0FBakIsQ0FBbkI7QUFDQUgsV0FBU0ksTUFBVCxDQUFnQkgsWUFBaEI7O0FBRUE7QUFDQSxNQUFJSSxRQUFRLElBQUlDLEtBQUosRUFBWjtBQUNBbEIsSUFBRWlCLEtBQUYsRUFDRUUsSUFERixDQUNPLFlBQVk7QUFDakIsT0FBSUMsa0JBQWtCcEIsRUFBRSw4REFBRixDQUF0Qjs7QUFFQW9CLG1CQUNFSixNQURGLENBQ1NoQixFQUFFLE1BQUYsQ0FEVCxFQUVFcUIsT0FGRixDQUVVSixLQUZWOztBQUlBTCxZQUFTUyxPQUFULENBQWlCRCxlQUFqQjs7QUFFQTtBQUNBVCxPQUFJVyxHQUFKLENBQVEsY0FBUixFQUF3QlYsUUFBeEI7QUFDQSxHQVpGLEVBYUVXLElBYkYsQ0FhTyxFQUFDQyxLQUFLMUIsS0FBSzJCLFFBQVgsRUFiUDs7QUFlQSxTQUFPYixRQUFQO0FBQ0EsRUF4QkQ7O0FBMEJBO0FBQ0E7QUFDQTs7QUFFQWpCLFFBQU8rQixJQUFQLEdBQWMsVUFBVUMsSUFBVixFQUFnQjtBQUM3QixNQUFJcEIsU0FBU3FCLE1BQWIsRUFBcUI7QUFDcEIsT0FBSUMsVUFBVTdCLEVBQUU4QixNQUFGLENBQVMsSUFBVCxFQUFlN0IsY0FBZixFQUErQixFQUFDOEIsU0FBUyxFQUFDQyxNQUFNdkIsV0FBUCxFQUFWLEVBQS9CLENBQWQ7QUFDQUYsWUFBUzBCLElBQVQsQ0FBY0osT0FBZDtBQUNBLEdBSEQsTUFHTztBQUNOLFNBQU0sSUFBSUssS0FBSixDQUFVLGdDQUFWLENBQU47QUFDQTs7QUFFRFA7QUFDQSxFQVREOztBQVdBLFFBQU9oQyxNQUFQO0FBQ0EsQ0F6R0YiLCJmaWxlIjoicHJvZHVjdC9wcm9kdWN0X3Rvb2x0aXAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHByb2R1Y3RfdG9vbHRpcC5qcyAyMDE2LTAzLTMxXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBQcm9kdWN0IFRvb2x0aXBcbiAqXG4gKiBUaGlzIGNvbnRyb2xsZXIgZGlzcGxheXMgYSB0b29sdGlwIHdoZW4gaG92ZXJpbmcgdGhlIHByb2R1Y3QgbmFtZS5cbiAqXG4gKiBVc2UgYXR0cmlidXRlICdkYXRhLXByb2R1Y3RfdG9vbHRpcC1pbWFnZS11cmwnIHRvIHNldCB0aGUgaW1hZ2UgdXJsIHRvIGxvYWQuXG4gKiBVc2UgYXR0cmlidXRlICdkYXRhLXByb2R1Y3RfdG9vbHRpcC1kZXNjcmlwdGlvbicgdG8gc2V0IHRoZSB0ZXh0IGNvbnRlbnQuXG4gKlxuICogQG1vZHVsZSBDb250cm9sbGVycy9wcm9kdWN0X3Rvb2x0aXBcbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQncHJvZHVjdF90b29sdGlwJyxcblx0XG5cdFtcblx0XHRqc2Uuc291cmNlICsgJy92ZW5kb3IvcXRpcDIvanF1ZXJ5LnF0aXAubWluLmNzcycsXG5cdFx0anNlLnNvdXJjZSArICcvdmVuZG9yL3F0aXAyL2pxdWVyeS5xdGlwLm1pbi5qcydcblx0XSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb250cm9sbGVycy9wcm9kdWN0X3Rvb2x0aXAgKi9cblxuXHRmdW5jdGlvbiAoZGF0YSkge1xuXG5cdFx0J3VzZSBzdHJpY3QnO1xuXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge30sXG5cblx0XHRcdC8qKlxuXHRcdFx0ICogcVRpcCBwbHVnaW4gb3B0aW9ucyBvYmplY3QuXG5cdFx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHR0b29sdGlwT3B0aW9ucyA9IHtcblx0XHRcdFx0c3R5bGU6IHtcblx0XHRcdFx0XHRjbGFzc2VzOiAnZ3gtY29udGFpbmVyIGd4LXF0aXAgaW5mbyBsYXJnZSdcblx0XHRcdFx0fSxcblx0XHRcdFx0cG9zaXRpb246IHtcblx0XHRcdFx0XHRteTogJ2xlZnQgdG9wJyxcblx0XHRcdFx0XHRhdDogJ3JpZ2h0IGJvdHRvbSdcblx0XHRcdFx0fVxuXHRcdFx0fSxcblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBIb3ZlciB0cmlnZ2VyLlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0JHRyaWdnZXIgPSAkdGhpcy5maW5kKCdbZGF0YS10b29sdGlwLXRyaWdnZXJdJyk7XG5cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBQUklWQVRFIE1FVEhPRFNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuXHRcdC8qKlxuXHRcdCAqIFByZXBhcmVzIHRoZSB0b29sdGlwIGNvbnRlbnQgYnkgZmV0Y2hpbmcgdGhlIGltYWdlIGFuZCBzZXR0aW5nIHRoZSBkZXNjcmlwdGlvbi5cblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgSG92ZXIgZXZlbnQuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGFwaSBxVGlwIHBsdWdpbiBpbnRlcm5hbCBBUEkuXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX2dldENvbnRlbnQgPSBmdW5jdGlvbiAoZXZlbnQsIGFwaSkge1xuXHRcdFx0dmFyICRjb250ZW50ID0gJCgnPGRpdi8+Jyk7XG5cblx0XHRcdHZhciAkZGVzY3JpcHRpb24gPSAkLnBhcnNlSFRNTChkYXRhLmRlc2NyaXB0aW9uKTtcblx0XHRcdCRjb250ZW50LmFwcGVuZCgkZGVzY3JpcHRpb24pO1xuXG5cdFx0XHQvLyBGZXRjaCBpbWFnZS5cblx0XHRcdHZhciBpbWFnZSA9IG5ldyBJbWFnZSgpO1xuXHRcdFx0JChpbWFnZSlcblx0XHRcdFx0LmxvYWQoZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdHZhciAkaW1hZ2VDb250YWluZXIgPSAkKCc8ZGl2IGNsYXNzPVwidGV4dC1jZW50ZXJcIiBzdHlsZT1cIm1hcmdpbi1ib3R0b206IDI0cHg7XCI+PC9kaXY+Jyk7XG5cblx0XHRcdFx0XHQkaW1hZ2VDb250YWluZXJcblx0XHRcdFx0XHRcdC5hcHBlbmQoJCgnPGJyPicpKVxuXHRcdFx0XHRcdFx0LnByZXBlbmQoaW1hZ2UpO1xuXG5cdFx0XHRcdFx0JGNvbnRlbnQucHJlcGVuZCgkaW1hZ2VDb250YWluZXIpO1xuXG5cdFx0XHRcdFx0Ly8gU2V0IG5ldyB0b29sdGlwIGNvbnRlbnQuXG5cdFx0XHRcdFx0YXBpLnNldCgnY29udGVudC50ZXh0JywgJGNvbnRlbnQpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuYXR0cih7c3JjOiBkYXRhLmltYWdlVXJsfSk7XG5cblx0XHRcdHJldHVybiAkY29udGVudDtcblx0XHR9O1xuXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24gKGRvbmUpIHtcblx0XHRcdGlmICgkdHJpZ2dlci5sZW5ndGgpIHtcblx0XHRcdFx0dmFyIG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB0b29sdGlwT3B0aW9ucywge2NvbnRlbnQ6IHt0ZXh0OiBfZ2V0Q29udGVudH19KTtcblx0XHRcdFx0JHRyaWdnZXIucXRpcChvcHRpb25zKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHRocm93IG5ldyBFcnJvcignQ291bGQgbm90IGZpbmQgdHJpZ2dlciBlbGVtZW50Jyk7XG5cdFx0XHR9XG5cblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
