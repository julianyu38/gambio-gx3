'use strict';

/* --------------------------------------------------------------
 actions.js 2016-10-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Actions
 *
 * This module creates the bulk and row actions for the table.
 */
gx.controllers.module('actions', ['user_configuration_service', gx.source + '/libs/button_dropdown'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Create Bulk Actions
  *
  * This callback can be called once during the initialization of this module.
  */
	function _createBulkActions() {
		// Add actions to the bulk-action dropdown.
		var $bulkActions = $('.bulk-action');
		var defaultBulkAction = $this.data('defaultBulkAction') || 'bulk-email-invoice';

		jse.libs.button_dropdown.bindDefaultAction($bulkActions, jse.core.registry.get('userId'), 'invoicesOverviewBulkAction', jse.libs.user_configuration_service);

		// Email
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_EMAIL', 'admin_buttons'),
			class: 'bulk-email-invoice',
			data: { configurationValue: 'bulk-email-invoice' },
			isDefault: defaultBulkAction === 'bulk-email-invoice',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Change status
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_MULTI_CHANGE_ORDER_STATUS', 'orders'),
			class: 'change-status',
			data: { configurationValue: 'change-status' },
			isDefault: defaultBulkAction === 'change-status',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Download invoices
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BULK_DOWNLOAD_INVOICES', 'admin_invoices'),
			class: 'bulk-download-invoice',
			data: { configurationValue: 'bulk-download-invoice' },
			isDefault: defaultBulkAction === 'bulk-download-invoice',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Delete
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_MULTI_DELETE', 'orders'),
			class: 'delete',
			data: { configurationValue: 'delete' },
			isDefault: defaultBulkAction === 'delete',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Cancellation Invoice
		if (!!data.isPdfCreatorInstalled) {
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('CANCELLATION_INVOICE', 'admin_invoices'),
				class: 'bulk-cancellation-invoice',
				data: { configurationValue: 'bulk-cancellation-invoice' },
				isDefault: defaultBulkAction === 'bulk-cancellation-invoice',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});
		}

		$this.datatable_default_actions('ensure', 'bulk');
	}

	/**
  * Create Table Row Actions
  *
  * This function must be call with every table draw.dt event.
  */
	function _createRowActions() {
		// Re-create the checkbox widgets and the row actions. 
		var defaultRowAction = $this.data('defaultRowAction') || 'view';

		jse.libs.button_dropdown.bindDefaultAction($this.find('.btn-group.dropdown'), jse.core.registry.get('userId'), 'invoicesOverviewRowAction', jse.libs.user_configuration_service);

		$this.find('.btn-group.dropdown').each(function () {
			var _$$parents$data = $(this).parents('tr').data(),
			    invoiceNumber = _$$parents$data.invoiceNumber,
			    orderId = _$$parents$data.orderId,
			    isCancellationInvoice = _$$parents$data.isCancellationInvoice,
			    country = _$$parents$data.country;

			// View


			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('TEXT_SHOW', 'orders'),
				href: 'request_port.php?module=OrderAdmin&action=showPdf&type=invoice' + ('&invoice_number=' + invoiceNumber + '&order_id=' + orderId),
				target: '_blank',
				class: 'view',
				data: { configurationValue: 'view' },
				isDefault: defaultRowAction === 'view'
			});

			// Download
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_DOWNLOAD', 'admin_buttons'),
				href: 'request_port.php?module=OrderAdmin&action=downloadPdf&type=invoice' + ('&invoice_number=' + invoiceNumber + '&order_id=' + orderId),
				target: '_blank',
				class: 'download',
				data: { configurationValue: 'download' },
				isDefault: defaultRowAction === 'download'
			});

			// Email
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_EMAIL', 'admin_buttons'),
				class: 'email-invoice',
				data: { configurationValue: 'email-invoice' },
				isDefault: defaultRowAction === 'email-invoice',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Change Status
			if (orderId > 0) {
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('TEXT_GM_STATUS', 'orders'),
					class: 'change-status',
					data: { configurationValue: 'change-status' },
					isDefault: defaultRowAction === 'change-status',
					callback: function callback(e) {
						return e.preventDefault();
					}
				});
			}

			// Delete
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_MULTI_DELETE', 'orders'),
				class: 'delete',
				data: { configurationValue: 'delete' },
				isDefault: defaultRowAction === 'delete',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Cancellation invoice
			if (isCancellationInvoice === false && orderId > 0 && country !== '' && !!data.isPdfCreatorInstalled) {
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('CANCELLATION_INVOICE', 'admin_invoices'),
					class: 'cancellation-invoice',
					data: { configurationValue: 'cancellation-invoice' },
					isDefault: defaultRowAction === 'cancellation-invoice',
					callback: function callback(e) {
						return e.preventDefault();
					}
				});
			}

			$this.datatable_default_actions('ensure', 'row');
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(window).on('JSENGINE_INIT_FINISHED', function () {
			$this.on('draw.dt', _createRowActions);
			_createRowActions();
			_createBulkActions();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL292ZXJ2aWV3L2FjdGlvbnMuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJfY3JlYXRlQnVsa0FjdGlvbnMiLCIkYnVsa0FjdGlvbnMiLCJkZWZhdWx0QnVsa0FjdGlvbiIsImpzZSIsImxpYnMiLCJidXR0b25fZHJvcGRvd24iLCJiaW5kRGVmYXVsdEFjdGlvbiIsImNvcmUiLCJyZWdpc3RyeSIsImdldCIsInVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlIiwiYWRkQWN0aW9uIiwidGV4dCIsImxhbmciLCJ0cmFuc2xhdGUiLCJjbGFzcyIsImNvbmZpZ3VyYXRpb25WYWx1ZSIsImlzRGVmYXVsdCIsImNhbGxiYWNrIiwiZSIsInByZXZlbnREZWZhdWx0IiwiaXNQZGZDcmVhdG9ySW5zdGFsbGVkIiwiZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucyIsIl9jcmVhdGVSb3dBY3Rpb25zIiwiZGVmYXVsdFJvd0FjdGlvbiIsImZpbmQiLCJlYWNoIiwicGFyZW50cyIsImludm9pY2VOdW1iZXIiLCJvcmRlcklkIiwiaXNDYW5jZWxsYXRpb25JbnZvaWNlIiwiY291bnRyeSIsImhyZWYiLCJ0YXJnZXQiLCJpbml0IiwiZG9uZSIsIndpbmRvdyIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFNBREQsRUFHQyxDQUFDLDRCQUFELEVBQWtDRixHQUFHRyxNQUFyQywyQkFIRCxFQUtDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1KLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0ssa0JBQVQsR0FBOEI7QUFDN0I7QUFDQSxNQUFNQyxlQUFlRixFQUFFLGNBQUYsQ0FBckI7QUFDQSxNQUFNRyxvQkFBb0JKLE1BQU1ELElBQU4sQ0FBVyxtQkFBWCxLQUFtQyxvQkFBN0Q7O0FBRUFNLE1BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsaUJBQXpCLENBQTJDTCxZQUEzQyxFQUF5REUsSUFBSUksSUFBSixDQUFTQyxRQUFULENBQWtCQyxHQUFsQixDQUFzQixRQUF0QixDQUF6RCxFQUNDLDRCQURELEVBQytCTixJQUFJQyxJQUFKLENBQVNNLDBCQUR4Qzs7QUFHQTtBQUNBUCxNQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DVixZQUFuQyxFQUFpRDtBQUNoRFcsU0FBTVQsSUFBSUksSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IsY0FBeEIsRUFBd0MsZUFBeEMsQ0FEMEM7QUFFaERDLFVBQU8sb0JBRnlDO0FBR2hEbEIsU0FBTSxFQUFDbUIsb0JBQW9CLG9CQUFyQixFQUgwQztBQUloREMsY0FBV2Ysc0JBQXNCLG9CQUplO0FBS2hEZ0IsYUFBVTtBQUFBLFdBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTHNDLEdBQWpEOztBQVFBO0FBQ0FqQixNQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DVixZQUFuQyxFQUFpRDtBQUNoRFcsU0FBTVQsSUFBSUksSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0Isa0NBQXhCLEVBQTRELFFBQTVELENBRDBDO0FBRWhEQyxVQUFPLGVBRnlDO0FBR2hEbEIsU0FBTSxFQUFDbUIsb0JBQW9CLGVBQXJCLEVBSDBDO0FBSWhEQyxjQUFXZixzQkFBc0IsZUFKZTtBQUtoRGdCLGFBQVU7QUFBQSxXQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxzQyxHQUFqRDs7QUFRQTtBQUNBakIsTUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1YsWUFBbkMsRUFBaUQ7QUFDaERXLFNBQU1ULElBQUlJLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLHdCQUF4QixFQUFrRCxnQkFBbEQsQ0FEMEM7QUFFaERDLFVBQU8sdUJBRnlDO0FBR2hEbEIsU0FBTSxFQUFDbUIsb0JBQW9CLHVCQUFyQixFQUgwQztBQUloREMsY0FBV2Ysc0JBQXNCLHVCQUplO0FBS2hEZ0IsYUFBVTtBQUFBLFdBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTHNDLEdBQWpEOztBQVFBO0FBQ0FqQixNQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DVixZQUFuQyxFQUFpRDtBQUNoRFcsU0FBTVQsSUFBSUksSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLFFBQS9DLENBRDBDO0FBRWhEQyxVQUFPLFFBRnlDO0FBR2hEbEIsU0FBTSxFQUFDbUIsb0JBQW9CLFFBQXJCLEVBSDBDO0FBSWhEQyxjQUFXZixzQkFBc0IsUUFKZTtBQUtoRGdCLGFBQVU7QUFBQSxXQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxzQyxHQUFqRDs7QUFRQTtBQUNBLE1BQUksQ0FBQyxDQUFDdkIsS0FBS3dCLHFCQUFYLEVBQWtDO0FBQ2pDbEIsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1YsWUFBbkMsRUFBaUQ7QUFDaERXLFVBQU1ULElBQUlJLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLHNCQUF4QixFQUFnRCxnQkFBaEQsQ0FEMEM7QUFFaERDLFdBQU8sMkJBRnlDO0FBR2hEbEIsVUFBTSxFQUFDbUIsb0JBQW9CLDJCQUFyQixFQUgwQztBQUloREMsZUFBV2Ysc0JBQXNCLDJCQUplO0FBS2hEZ0IsY0FBVTtBQUFBLFlBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTHNDLElBQWpEO0FBT0E7O0FBRUR0QixRQUFNd0IseUJBQU4sQ0FBZ0MsUUFBaEMsRUFBMEMsTUFBMUM7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxpQkFBVCxHQUE2QjtBQUM1QjtBQUNBLE1BQU1DLG1CQUFtQjFCLE1BQU1ELElBQU4sQ0FBVyxrQkFBWCxLQUFrQyxNQUEzRDs7QUFFQU0sTUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxpQkFBekIsQ0FBMkNSLE1BQU0yQixJQUFOLENBQVcscUJBQVgsQ0FBM0MsRUFDQ3RCLElBQUlJLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsR0FBbEIsQ0FBc0IsUUFBdEIsQ0FERCxFQUNrQywyQkFEbEMsRUFDK0ROLElBQUlDLElBQUosQ0FBU00sMEJBRHhFOztBQUdBWixRQUFNMkIsSUFBTixDQUFXLHFCQUFYLEVBQWtDQyxJQUFsQyxDQUF1QyxZQUFXO0FBQUEseUJBQ2dCM0IsRUFBRSxJQUFGLEVBQVE0QixPQUFSLENBQWdCLElBQWhCLEVBQXNCOUIsSUFBdEIsRUFEaEI7QUFBQSxPQUMxQytCLGFBRDBDLG1CQUMxQ0EsYUFEMEM7QUFBQSxPQUMzQkMsT0FEMkIsbUJBQzNCQSxPQUQyQjtBQUFBLE9BQ2xCQyxxQkFEa0IsbUJBQ2xCQSxxQkFEa0I7QUFBQSxPQUNLQyxPQURMLG1CQUNLQSxPQURMOztBQUdqRDs7O0FBQ0E1QixPQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWixFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NhLFVBQU1ULElBQUlJLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLFdBQXhCLEVBQXFDLFFBQXJDLENBRHFDO0FBRTNDa0IsVUFBTSx5RkFDZUosYUFEZixrQkFDeUNDLE9BRHpDLENBRnFDO0FBSTNDSSxZQUFRLFFBSm1DO0FBSzNDbEIsV0FBTyxNQUxvQztBQU0zQ2xCLFVBQU0sRUFBQ21CLG9CQUFvQixNQUFyQixFQU5xQztBQU8zQ0MsZUFBV08scUJBQXFCO0FBUFcsSUFBNUM7O0FBVUE7QUFDQXJCLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5Qk0sU0FBekIsQ0FBbUNaLEVBQUUsSUFBRixDQUFuQyxFQUE0QztBQUMzQ2EsVUFBTVQsSUFBSUksSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IsaUJBQXhCLEVBQTJDLGVBQTNDLENBRHFDO0FBRTNDa0IsVUFBTSw2RkFDZUosYUFEZixrQkFDeUNDLE9BRHpDLENBRnFDO0FBSTNDSSxZQUFRLFFBSm1DO0FBSzNDbEIsV0FBTyxVQUxvQztBQU0zQ2xCLFVBQU0sRUFBQ21CLG9CQUFvQixVQUFyQixFQU5xQztBQU8zQ0MsZUFBV08scUJBQXFCO0FBUFcsSUFBNUM7O0FBVUE7QUFDQXJCLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5Qk0sU0FBekIsQ0FBbUNaLEVBQUUsSUFBRixDQUFuQyxFQUE0QztBQUMzQ2EsVUFBTVQsSUFBSUksSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IsY0FBeEIsRUFBd0MsZUFBeEMsQ0FEcUM7QUFFM0NDLFdBQU8sZUFGb0M7QUFHM0NsQixVQUFNLEVBQUNtQixvQkFBb0IsZUFBckIsRUFIcUM7QUFJM0NDLGVBQVdPLHFCQUFxQixlQUpXO0FBSzNDTixjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsSUFBNUM7O0FBUUE7QUFDQSxPQUFJUyxVQUFVLENBQWQsRUFBaUI7QUFDaEIxQixRQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWixFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NhLFdBQU1ULElBQUlJLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLGdCQUF4QixFQUEwQyxRQUExQyxDQURxQztBQUUzQ0MsWUFBTyxlQUZvQztBQUczQ2xCLFdBQU0sRUFBQ21CLG9CQUFvQixlQUFyQixFQUhxQztBQUkzQ0MsZ0JBQVdPLHFCQUFxQixlQUpXO0FBSzNDTixlQUFVO0FBQUEsYUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsS0FBNUM7QUFPQTs7QUFFRDtBQUNBakIsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1osRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDYSxVQUFNVCxJQUFJSSxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixxQkFBeEIsRUFBK0MsUUFBL0MsQ0FEcUM7QUFFM0NDLFdBQU8sUUFGb0M7QUFHM0NsQixVQUFNLEVBQUNtQixvQkFBb0IsUUFBckIsRUFIcUM7QUFJM0NDLGVBQVdPLHFCQUFxQixRQUpXO0FBSzNDTixjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsSUFBNUM7O0FBUUE7QUFDQSxPQUFJVSwwQkFBMEIsS0FBMUIsSUFBbUNELFVBQVUsQ0FBN0MsSUFBa0RFLFlBQVksRUFBOUQsSUFBb0UsQ0FBQyxDQUFDbEMsS0FBS3dCLHFCQUEvRSxFQUFzRztBQUNyR2xCLFFBQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5Qk0sU0FBekIsQ0FBbUNaLEVBQUUsSUFBRixDQUFuQyxFQUE0QztBQUMzQ2EsV0FBTVQsSUFBSUksSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0Isc0JBQXhCLEVBQWdELGdCQUFoRCxDQURxQztBQUUzQ0MsWUFBTyxzQkFGb0M7QUFHM0NsQixXQUFNLEVBQUNtQixvQkFBb0Isc0JBQXJCLEVBSHFDO0FBSTNDQyxnQkFBV08scUJBQXFCLHNCQUpXO0FBSzNDTixlQUFVO0FBQUEsYUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsS0FBNUM7QUFPQTs7QUFFRHRCLFNBQU13Qix5QkFBTixDQUFnQyxRQUFoQyxFQUEwQyxLQUExQztBQUNBLEdBbEVEO0FBbUVBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTNCLFFBQU91QyxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCcEMsSUFBRXFDLE1BQUYsRUFBVUMsRUFBVixDQUFhLHdCQUFiLEVBQXVDLFlBQU07QUFDNUN2QyxTQUFNdUMsRUFBTixDQUFTLFNBQVQsRUFBb0JkLGlCQUFwQjtBQUNBQTtBQUNBdkI7QUFDQSxHQUpEOztBQU1BbUM7QUFDQSxFQVJEOztBQVVBLFFBQU94QyxNQUFQO0FBRUEsQ0EvTEYiLCJmaWxlIjoiaW52b2ljZXMvb3ZlcnZpZXcvYWN0aW9ucy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBhY3Rpb25zLmpzIDIwMTYtMTAtMTJcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogTWFpbiBUYWJsZSBBY3Rpb25zXHJcbiAqXHJcbiAqIFRoaXMgbW9kdWxlIGNyZWF0ZXMgdGhlIGJ1bGsgYW5kIHJvdyBhY3Rpb25zIGZvciB0aGUgdGFibGUuXHJcbiAqL1xyXG5neC5jb250cm9sbGVycy5tb2R1bGUoXHJcblx0J2FjdGlvbnMnLFxyXG5cdFxyXG5cdFsndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnLCBgJHtneC5zb3VyY2V9L2xpYnMvYnV0dG9uX2Ryb3Bkb3duYF0sXHJcblx0XHJcblx0ZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0XHJcblx0XHQndXNlIHN0cmljdCc7XHJcblx0XHRcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0Ly8gVkFSSUFCTEVTXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge09iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XHJcblx0XHRcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0Ly8gRlVOQ1RJT05TXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBDcmVhdGUgQnVsayBBY3Rpb25zXHJcblx0XHQgKlxyXG5cdFx0ICogVGhpcyBjYWxsYmFjayBjYW4gYmUgY2FsbGVkIG9uY2UgZHVyaW5nIHRoZSBpbml0aWFsaXphdGlvbiBvZiB0aGlzIG1vZHVsZS5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX2NyZWF0ZUJ1bGtBY3Rpb25zKCkge1xyXG5cdFx0XHQvLyBBZGQgYWN0aW9ucyB0byB0aGUgYnVsay1hY3Rpb24gZHJvcGRvd24uXHJcblx0XHRcdGNvbnN0ICRidWxrQWN0aW9ucyA9ICQoJy5idWxrLWFjdGlvbicpO1xyXG5cdFx0XHRjb25zdCBkZWZhdWx0QnVsa0FjdGlvbiA9ICR0aGlzLmRhdGEoJ2RlZmF1bHRCdWxrQWN0aW9uJykgfHwgJ2J1bGstZW1haWwtaW52b2ljZSc7XHJcblx0XHRcdFxyXG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYmluZERlZmF1bHRBY3Rpb24oJGJ1bGtBY3Rpb25zLCBqc2UuY29yZS5yZWdpc3RyeS5nZXQoJ3VzZXJJZCcpLFxyXG5cdFx0XHRcdCdpbnZvaWNlc092ZXJ2aWV3QnVsa0FjdGlvbicsIGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEVtYWlsXHJcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XHJcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9FTUFJTCcsICdhZG1pbl9idXR0b25zJyksXHJcblx0XHRcdFx0Y2xhc3M6ICdidWxrLWVtYWlsLWludm9pY2UnLFxyXG5cdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdidWxrLWVtYWlsLWludm9pY2UnfSxcclxuXHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRCdWxrQWN0aW9uID09PSAnYnVsay1lbWFpbC1pbnZvaWNlJyxcclxuXHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBDaGFuZ2Ugc3RhdHVzXHJcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XHJcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9NVUxUSV9DSEFOR0VfT1JERVJfU1RBVFVTJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdGNsYXNzOiAnY2hhbmdlLXN0YXR1cycsXHJcblx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2NoYW5nZS1zdGF0dXMnfSxcclxuXHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRCdWxrQWN0aW9uID09PSAnY2hhbmdlLXN0YXR1cycsXHJcblx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gRG93bmxvYWQgaW52b2ljZXNcclxuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkYnVsa0FjdGlvbnMsIHtcclxuXHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVMS19ET1dOTE9BRF9JTlZPSUNFUycsICdhZG1pbl9pbnZvaWNlcycpLFxyXG5cdFx0XHRcdGNsYXNzOiAnYnVsay1kb3dubG9hZC1pbnZvaWNlJyxcclxuXHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnYnVsay1kb3dubG9hZC1pbnZvaWNlJ30sXHJcblx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0QnVsa0FjdGlvbiA9PT0gJ2J1bGstZG93bmxvYWQtaW52b2ljZScsXHJcblx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gRGVsZXRlXHJcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XHJcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9NVUxUSV9ERUxFVEUnLCAnb3JkZXJzJyksXHJcblx0XHRcdFx0Y2xhc3M6ICdkZWxldGUnLFxyXG5cdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdkZWxldGUnfSxcclxuXHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRCdWxrQWN0aW9uID09PSAnZGVsZXRlJyxcclxuXHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBDYW5jZWxsYXRpb24gSW52b2ljZVxyXG5cdFx0XHRpZiAoISFkYXRhLmlzUGRmQ3JlYXRvckluc3RhbGxlZCkge1xyXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XHJcblx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQ0FOQ0VMTEFUSU9OX0lOVk9JQ0UnLCAnYWRtaW5faW52b2ljZXMnKSxcclxuXHRcdFx0XHRcdGNsYXNzOiAnYnVsay1jYW5jZWxsYXRpb24taW52b2ljZScsXHJcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnYnVsay1jYW5jZWxsYXRpb24taW52b2ljZSd9LFxyXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0QnVsa0FjdGlvbiA9PT0gJ2J1bGstY2FuY2VsbGF0aW9uLWludm9pY2UnLFxyXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdFx0fSk7XHRcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0JHRoaXMuZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucygnZW5zdXJlJywgJ2J1bGsnKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBDcmVhdGUgVGFibGUgUm93IEFjdGlvbnNcclxuXHRcdCAqXHJcblx0XHQgKiBUaGlzIGZ1bmN0aW9uIG11c3QgYmUgY2FsbCB3aXRoIGV2ZXJ5IHRhYmxlIGRyYXcuZHQgZXZlbnQuXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9jcmVhdGVSb3dBY3Rpb25zKCkge1xyXG5cdFx0XHQvLyBSZS1jcmVhdGUgdGhlIGNoZWNrYm94IHdpZGdldHMgYW5kIHRoZSByb3cgYWN0aW9ucy4gXHJcblx0XHRcdGNvbnN0IGRlZmF1bHRSb3dBY3Rpb24gPSAkdGhpcy5kYXRhKCdkZWZhdWx0Um93QWN0aW9uJykgfHwgJ3ZpZXcnO1xyXG5cdFx0XHRcclxuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmJpbmREZWZhdWx0QWN0aW9uKCR0aGlzLmZpbmQoJy5idG4tZ3JvdXAuZHJvcGRvd24nKSxcclxuXHRcdFx0XHRqc2UuY29yZS5yZWdpc3RyeS5nZXQoJ3VzZXJJZCcpLCAnaW52b2ljZXNPdmVydmlld1Jvd0FjdGlvbicsIGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlKTtcclxuXHRcdFx0XHJcblx0XHRcdCR0aGlzLmZpbmQoJy5idG4tZ3JvdXAuZHJvcGRvd24nKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGNvbnN0IHtpbnZvaWNlTnVtYmVyLCBvcmRlcklkLCBpc0NhbmNlbGxhdGlvbkludm9pY2UsIGNvdW50cnl9ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBWaWV3XHJcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XHJcblx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9TSE9XJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdFx0aHJlZjogYHJlcXVlc3RfcG9ydC5waHA/bW9kdWxlPU9yZGVyQWRtaW4mYWN0aW9uPXNob3dQZGYmdHlwZT1pbnZvaWNlYFxyXG5cdFx0XHRcdFx0KyBgJmludm9pY2VfbnVtYmVyPSR7aW52b2ljZU51bWJlcn0mb3JkZXJfaWQ9JHtvcmRlcklkfWAsXHJcblx0XHRcdFx0XHR0YXJnZXQ6ICdfYmxhbmsnLFxyXG5cdFx0XHRcdFx0Y2xhc3M6ICd2aWV3JyxcclxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICd2aWV3J30sXHJcblx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICd2aWV3J1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIERvd25sb2FkXHJcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XHJcblx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX0RPV05MT0FEJywgJ2FkbWluX2J1dHRvbnMnKSxcclxuXHRcdFx0XHRcdGhyZWY6IGByZXF1ZXN0X3BvcnQucGhwP21vZHVsZT1PcmRlckFkbWluJmFjdGlvbj1kb3dubG9hZFBkZiZ0eXBlPWludm9pY2VgXHJcblx0XHRcdFx0XHQrIGAmaW52b2ljZV9udW1iZXI9JHtpbnZvaWNlTnVtYmVyfSZvcmRlcl9pZD0ke29yZGVySWR9YCxcclxuXHRcdFx0XHRcdHRhcmdldDogJ19ibGFuaycsXHJcblx0XHRcdFx0XHRjbGFzczogJ2Rvd25sb2FkJyxcclxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdkb3dubG9hZCd9LFxyXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAnZG93bmxvYWQnXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gRW1haWxcclxuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcclxuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fRU1BSUwnLCAnYWRtaW5fYnV0dG9ucycpLFxyXG5cdFx0XHRcdFx0Y2xhc3M6ICdlbWFpbC1pbnZvaWNlJyxcclxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdlbWFpbC1pbnZvaWNlJ30sXHJcblx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdlbWFpbC1pbnZvaWNlJyxcclxuXHRcdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIENoYW5nZSBTdGF0dXNcclxuXHRcdFx0XHRpZiAob3JkZXJJZCA+IDApIHtcclxuXHRcdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xyXG5cdFx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9HTV9TVEFUVVMnLCAnb3JkZXJzJyksXHJcblx0XHRcdFx0XHRcdGNsYXNzOiAnY2hhbmdlLXN0YXR1cycsXHJcblx0XHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdjaGFuZ2Utc3RhdHVzJ30sXHJcblx0XHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ2NoYW5nZS1zdGF0dXMnLFxyXG5cdFx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBEZWxldGVcclxuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcclxuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fTVVMVElfREVMRVRFJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdFx0Y2xhc3M6ICdkZWxldGUnLFxyXG5cdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2RlbGV0ZSd9LFxyXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAnZGVsZXRlJyxcclxuXHRcdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIENhbmNlbGxhdGlvbiBpbnZvaWNlXHJcblx0XHRcdFx0aWYgKGlzQ2FuY2VsbGF0aW9uSW52b2ljZSA9PT0gZmFsc2UgJiYgb3JkZXJJZCA+IDAgJiYgY291bnRyeSAhPT0gJycgJiYgISFkYXRhLmlzUGRmQ3JlYXRvckluc3RhbGxlZCkge1xyXG5cdFx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XHJcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdDQU5DRUxMQVRJT05fSU5WT0lDRScsICdhZG1pbl9pbnZvaWNlcycpLFxyXG5cdFx0XHRcdFx0XHRjbGFzczogJ2NhbmNlbGxhdGlvbi1pbnZvaWNlJyxcclxuXHRcdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2NhbmNlbGxhdGlvbi1pbnZvaWNlJ30sXHJcblx0XHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ2NhbmNlbGxhdGlvbi1pbnZvaWNlJyxcclxuXHRcdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JHRoaXMuZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucygnZW5zdXJlJywgJ3JvdycpO1xyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0XHQkKHdpbmRvdykub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCAoKSA9PiB7XHJcblx0XHRcdFx0JHRoaXMub24oJ2RyYXcuZHQnLCBfY3JlYXRlUm93QWN0aW9ucyk7XHJcblx0XHRcdFx0X2NyZWF0ZVJvd0FjdGlvbnMoKTtcclxuXHRcdFx0XHRfY3JlYXRlQnVsa0FjdGlvbnMoKTtcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHRkb25lKCk7XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gbW9kdWxlO1xyXG5cdFx0XHJcblx0fSk7ICJdfQ==
