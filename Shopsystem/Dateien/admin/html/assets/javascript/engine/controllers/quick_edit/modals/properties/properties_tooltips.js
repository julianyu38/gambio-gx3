'use strict';

/* --------------------------------------------------------------
 tooltip.js 2017-05-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Table Tooltips Controller
 *
 * This controller displays tooltips for the QuickEdit overview table. The tooltips are loaded after the
 * table data request is ready for optimization purposes.
 */
gx.controllers.module('properties_tooltips', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @var {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		sourceUrl: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditProductPropertiesAjax/Tooltips',
		selectors: {
			mouseenter: {
				specials: '.tooltip-products-special-price'
			}
		}
	};

	/**
  * Final Options
  *
  * @var {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Tooltip Contents
  *
  * Contains the rendered HTML of the tooltips. The HTML is rendered with each table draw.
  *
  * e.g. tooltips.400210.orderItems >> HTML for order items tooltip of order #400210.
  *
  * @type {Object}
  */
	var tooltips = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Target Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTargetPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'left' : 'right';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'top' : 'bottom';

		return horizontal + ' ' + vertical;
	}

	/**
  * Get Tooltip Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTooltipPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'right' : 'left';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'bottom' : 'top';

		return horizontal + ' ' + vertical;
	}

	/**
  * Initialize tooltip for static table data.
  *
  * Replaces the browsers default tooltip with a qTip instance for every element on the table which has
  * a title attribute.
  */
	function _initTooltipsForStaticContent() {
		$this.find('[title]').qtip({
			style: { classes: 'gx-qtip info' }
		});
	}

	/**
  * Show Tooltip
  *
  * Display the Qtip instance of the target. The tooltip contents are fetched after the table request
  * is finished for performance reasons. This method will not show anything until the tooltip contents
  * are fetched.
  *
  * @param {jQuery.Event} event
  */
	function _showTooltip(event) {
		event.stopPropagation();

		var productId = $(this).parents('tr').data('id');

		if (!tooltips[productId]) {
			return; // The requested tooltip is not loaded, do not continue.
		}

		var tooltipPosition = _getTooltipPosition($(this));
		var targetPosition = _getTargetPosition($(this));

		$(this).qtip({
			content: tooltips[productId][event.data.name],
			style: {
				classes: 'gx-qtip info'
			},
			position: {
				my: tooltipPosition,
				at: targetPosition,
				effect: false,
				viewport: $(window),
				adjust: {
					method: 'none shift'
				}
			},
			hide: {
				fixed: true,
				delay: 100
			},
			show: {
				ready: true,
				delay: 300
			},
			events: {
				hidden: function hidden(event, api) {
					api.destroy(true);
				}
			}
		});
	}

	/**
  * Get Tooltips
  *
  * Fetches the tooltips with an AJAX request, based on the current state of the table.
  */
	function _getTooltips() {
		tooltips = [];
		var datatablesXhrParameters = $this.DataTable().ajax.params();
		$.post(options.sourceUrl, datatablesXhrParameters, function (response) {
			return tooltips = response;
		}, 'json');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('draw.dt', _initTooltipsForStaticContent).on('xhr.dt', _getTooltips);

		$(window).on('JSENGINE_INIT_FINISHED', function () {
			if ($this.DataTable().ajax.json() !== undefined && tooltips === undefined) {
				_getTooltips();
			}
		});

		for (var event in options.selectors) {
			for (var name in options.selectors[event]) {
				$this.on(event, options.selectors[event][name], { name: name }, _showTooltip);
			}
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc190b29sdGlwcy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwic291cmNlVXJsIiwianNlIiwiY29yZSIsImNvbmZpZyIsImdldCIsInNlbGVjdG9ycyIsIm1vdXNlZW50ZXIiLCJzcGVjaWFscyIsIm9wdGlvbnMiLCJleHRlbmQiLCJ0b29sdGlwcyIsIl9nZXRUYXJnZXRQb3NpdGlvbiIsIiR0YXJnZXQiLCJob3Jpem9udGFsIiwib2Zmc2V0IiwibGVmdCIsIndpbmRvdyIsInNjcm9sbExlZnQiLCJ3aWR0aCIsInZlcnRpY2FsIiwidG9wIiwic2Nyb2xsVG9wIiwiaGVpZ2h0IiwiX2dldFRvb2x0aXBQb3NpdGlvbiIsIl9pbml0VG9vbHRpcHNGb3JTdGF0aWNDb250ZW50IiwiZmluZCIsInF0aXAiLCJzdHlsZSIsImNsYXNzZXMiLCJfc2hvd1Rvb2x0aXAiLCJldmVudCIsInN0b3BQcm9wYWdhdGlvbiIsInByb2R1Y3RJZCIsInBhcmVudHMiLCJ0b29sdGlwUG9zaXRpb24iLCJ0YXJnZXRQb3NpdGlvbiIsImNvbnRlbnQiLCJuYW1lIiwicG9zaXRpb24iLCJteSIsImF0IiwiZWZmZWN0Iiwidmlld3BvcnQiLCJhZGp1c3QiLCJtZXRob2QiLCJoaWRlIiwiZml4ZWQiLCJkZWxheSIsInNob3ciLCJyZWFkeSIsImV2ZW50cyIsImhpZGRlbiIsImFwaSIsImRlc3Ryb3kiLCJfZ2V0VG9vbHRpcHMiLCJkYXRhdGFibGVzWGhyUGFyYW1ldGVycyIsIkRhdGFUYWJsZSIsImFqYXgiLCJwYXJhbXMiLCJwb3N0IiwicmVzcG9uc2UiLCJpbml0IiwiZG9uZSIsIm9uIiwianNvbiIsInVuZGVmaW5lZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7QUFNQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MscUJBREQsRUFHQyxFQUhELEVBS0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsV0FBVztBQUNoQkMsYUFBV0MsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyw2REFEM0I7QUFFaEJDLGFBQVc7QUFDVkMsZUFBWTtBQUNYQyxjQUFVO0FBREM7QUFERjtBQUZLLEVBQWpCOztBQVNBOzs7OztBQUtBLEtBQU1DLFVBQVVWLEVBQUVXLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQlYsUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTs7Ozs7Ozs7O0FBU0EsS0FBSWUsaUJBQUo7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O0FBT0EsVUFBU0Msa0JBQVQsQ0FBNEJDLE9BQTVCLEVBQXFDO0FBQ3BDLE1BQU1DLGFBQWFELFFBQVFFLE1BQVIsR0FBaUJDLElBQWpCLEdBQXdCakIsRUFBRWtCLE1BQUYsRUFBVUMsVUFBVixFQUF4QixHQUFpRG5CLEVBQUVrQixNQUFGLEVBQVVFLEtBQVYsS0FBb0IsQ0FBckUsR0FDaEIsTUFEZ0IsR0FFaEIsT0FGSDtBQUdBLE1BQU1DLFdBQVdQLFFBQVFFLE1BQVIsR0FBaUJNLEdBQWpCLEdBQXVCdEIsRUFBRWtCLE1BQUYsRUFBVUssU0FBVixFQUF2QixHQUErQ3ZCLEVBQUVrQixNQUFGLEVBQVVNLE1BQVYsS0FBcUIsQ0FBcEUsR0FDZCxLQURjLEdBRWQsUUFGSDs7QUFJQSxTQUFPVCxhQUFhLEdBQWIsR0FBbUJNLFFBQTFCO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTSSxtQkFBVCxDQUE2QlgsT0FBN0IsRUFBc0M7QUFDckMsTUFBTUMsYUFBYUQsUUFBUUUsTUFBUixHQUFpQkMsSUFBakIsR0FBd0JqQixFQUFFa0IsTUFBRixFQUFVQyxVQUFWLEVBQXhCLEdBQWlEbkIsRUFBRWtCLE1BQUYsRUFBVUUsS0FBVixLQUFvQixDQUFyRSxHQUNoQixPQURnQixHQUVoQixNQUZIO0FBR0EsTUFBTUMsV0FBV1AsUUFBUUUsTUFBUixHQUFpQk0sR0FBakIsR0FBdUJ0QixFQUFFa0IsTUFBRixFQUFVSyxTQUFWLEVBQXZCLEdBQStDdkIsRUFBRWtCLE1BQUYsRUFBVU0sTUFBVixLQUFxQixDQUFwRSxHQUNkLFFBRGMsR0FFZCxLQUZIOztBQUlBLFNBQU9ULGFBQWEsR0FBYixHQUFtQk0sUUFBMUI7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU0ssNkJBQVQsR0FBeUM7QUFDeEMzQixRQUFNNEIsSUFBTixDQUFXLFNBQVgsRUFBc0JDLElBQXRCLENBQTJCO0FBQzFCQyxVQUFPLEVBQUNDLFNBQVMsY0FBVjtBQURtQixHQUEzQjtBQUdBOztBQUVEOzs7Ozs7Ozs7QUFTQSxVQUFTQyxZQUFULENBQXNCQyxLQUF0QixFQUE2QjtBQUM1QkEsUUFBTUMsZUFBTjs7QUFFQSxNQUFNQyxZQUFZbEMsRUFBRSxJQUFGLEVBQVFtQyxPQUFSLENBQWdCLElBQWhCLEVBQXNCckMsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBbEI7O0FBRUEsTUFBSSxDQUFDYyxTQUFTc0IsU0FBVCxDQUFMLEVBQTBCO0FBQ3pCLFVBRHlCLENBQ2pCO0FBQ1I7O0FBRUQsTUFBTUUsa0JBQWtCWCxvQkFBb0J6QixFQUFFLElBQUYsQ0FBcEIsQ0FBeEI7QUFDQSxNQUFNcUMsaUJBQWlCeEIsbUJBQW1CYixFQUFFLElBQUYsQ0FBbkIsQ0FBdkI7O0FBRUFBLElBQUUsSUFBRixFQUFRNEIsSUFBUixDQUFhO0FBQ1pVLFlBQVMxQixTQUFTc0IsU0FBVCxFQUFvQkYsTUFBTWxDLElBQU4sQ0FBV3lDLElBQS9CLENBREc7QUFFWlYsVUFBTztBQUNOQyxhQUFTO0FBREgsSUFGSztBQUtaVSxhQUFVO0FBQ1RDLFFBQUlMLGVBREs7QUFFVE0sUUFBSUwsY0FGSztBQUdUTSxZQUFRLEtBSEM7QUFJVEMsY0FBVTVDLEVBQUVrQixNQUFGLENBSkQ7QUFLVDJCLFlBQVE7QUFDUEMsYUFBUTtBQUREO0FBTEMsSUFMRTtBQWNaQyxTQUFNO0FBQ0xDLFdBQU8sSUFERjtBQUVMQyxXQUFPO0FBRkYsSUFkTTtBQWtCWkMsU0FBTTtBQUNMQyxXQUFPLElBREY7QUFFTEYsV0FBTztBQUZGLElBbEJNO0FBc0JaRyxXQUFRO0FBQ1BDLFlBQVEsZ0JBQUNyQixLQUFELEVBQVFzQixHQUFSLEVBQWdCO0FBQ3ZCQSxTQUFJQyxPQUFKLENBQVksSUFBWjtBQUNBO0FBSE07QUF0QkksR0FBYjtBQTRCQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxZQUFULEdBQXdCO0FBQ3ZCNUMsYUFBVyxFQUFYO0FBQ0EsTUFBTTZDLDBCQUEwQjFELE1BQU0yRCxTQUFOLEdBQWtCQyxJQUFsQixDQUF1QkMsTUFBdkIsRUFBaEM7QUFDQTVELElBQUU2RCxJQUFGLENBQU9uRCxRQUFRUixTQUFmLEVBQTBCdUQsdUJBQTFCLEVBQW1EO0FBQUEsVUFBWTdDLFdBQVdrRCxRQUF2QjtBQUFBLEdBQW5ELEVBQW9GLE1BQXBGO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBakUsUUFBT2tFLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJqRSxRQUNFa0UsRUFERixDQUNLLFNBREwsRUFDZ0J2Qyw2QkFEaEIsRUFFRXVDLEVBRkYsQ0FFSyxRQUZMLEVBRWVULFlBRmY7O0FBSUF4RCxJQUFFa0IsTUFBRixFQUFVK0MsRUFBVixDQUFhLHdCQUFiLEVBQXVDLFlBQU07QUFDNUMsT0FBSWxFLE1BQU0yRCxTQUFOLEdBQWtCQyxJQUFsQixDQUF1Qk8sSUFBdkIsT0FBa0NDLFNBQWxDLElBQStDdkQsYUFBYXVELFNBQWhFLEVBQTJFO0FBQzFFWDtBQUNBO0FBQ0QsR0FKRDs7QUFNQSxPQUFLLElBQUl4QixLQUFULElBQWtCdEIsUUFBUUgsU0FBMUIsRUFBcUM7QUFDcEMsUUFBSyxJQUFJZ0MsSUFBVCxJQUFpQjdCLFFBQVFILFNBQVIsQ0FBa0J5QixLQUFsQixDQUFqQixFQUEyQztBQUMxQ2pDLFVBQU1rRSxFQUFOLENBQVNqQyxLQUFULEVBQWdCdEIsUUFBUUgsU0FBUixDQUFrQnlCLEtBQWxCLEVBQXlCTyxJQUF6QixDQUFoQixFQUFnRCxFQUFDQSxVQUFELEVBQWhELEVBQXdEUixZQUF4RDtBQUNBO0FBQ0Q7O0FBRURpQztBQUNBLEVBbEJEOztBQW9CQSxRQUFPbkUsTUFBUDtBQUNBLENBdE1GIiwiZmlsZSI6InF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc190b29sdGlwcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gdG9vbHRpcC5qcyAyMDE3LTA1LTE4XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBQcm9wZXJ0aWVzIFRhYmxlIFRvb2x0aXBzIENvbnRyb2xsZXJcbiAqXG4gKiBUaGlzIGNvbnRyb2xsZXIgZGlzcGxheXMgdG9vbHRpcHMgZm9yIHRoZSBRdWlja0VkaXQgb3ZlcnZpZXcgdGFibGUuIFRoZSB0b29sdGlwcyBhcmUgbG9hZGVkIGFmdGVyIHRoZVxuICogdGFibGUgZGF0YSByZXF1ZXN0IGlzIHJlYWR5IGZvciBvcHRpbWl6YXRpb24gcHVycG9zZXMuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3Byb3BlcnRpZXNfdG9vbHRpcHMnLFxuXHRcblx0W10sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHZhciB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7XG5cdFx0XHRzb3VyY2VVcmw6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89UXVpY2tFZGl0UHJvZHVjdFByb3BlcnRpZXNBamF4L1Rvb2x0aXBzJyxcblx0XHRcdHNlbGVjdG9yczoge1xuXHRcdFx0XHRtb3VzZWVudGVyOiB7XG5cdFx0XHRcdFx0c3BlY2lhbHM6ICcudG9vbHRpcC1wcm9kdWN0cy1zcGVjaWFsLXByaWNlJyxcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHZhciB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVG9vbHRpcCBDb250ZW50c1xuXHRcdCAqXG5cdFx0ICogQ29udGFpbnMgdGhlIHJlbmRlcmVkIEhUTUwgb2YgdGhlIHRvb2x0aXBzLiBUaGUgSFRNTCBpcyByZW5kZXJlZCB3aXRoIGVhY2ggdGFibGUgZHJhdy5cblx0XHQgKlxuXHRcdCAqIGUuZy4gdG9vbHRpcHMuNDAwMjEwLm9yZGVySXRlbXMgPj4gSFRNTCBmb3Igb3JkZXIgaXRlbXMgdG9vbHRpcCBvZiBvcmRlciAjNDAwMjEwLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRsZXQgdG9vbHRpcHM7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IFRhcmdldCBQb3NpdGlvblxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICR0YXJnZXRcblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge1N0cmluZ31cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0VGFyZ2V0UG9zaXRpb24oJHRhcmdldCkge1xuXHRcdFx0Y29uc3QgaG9yaXpvbnRhbCA9ICR0YXJnZXQub2Zmc2V0KCkubGVmdCAtICQod2luZG93KS5zY3JvbGxMZWZ0KCkgPiAkKHdpbmRvdykud2lkdGgoKSAvIDJcblx0XHRcdFx0PyAnbGVmdCdcblx0XHRcdFx0OiAncmlnaHQnO1xuXHRcdFx0Y29uc3QgdmVydGljYWwgPSAkdGFyZ2V0Lm9mZnNldCgpLnRvcCAtICQod2luZG93KS5zY3JvbGxUb3AoKSA+ICQod2luZG93KS5oZWlnaHQoKSAvIDJcblx0XHRcdFx0PyAndG9wJ1xuXHRcdFx0XHQ6ICdib3R0b20nO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gaG9yaXpvbnRhbCArICcgJyArIHZlcnRpY2FsO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgVG9vbHRpcCBQb3NpdGlvblxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICR0YXJnZXRcblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge1N0cmluZ31cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0VG9vbHRpcFBvc2l0aW9uKCR0YXJnZXQpIHtcblx0XHRcdGNvbnN0IGhvcml6b250YWwgPSAkdGFyZ2V0Lm9mZnNldCgpLmxlZnQgLSAkKHdpbmRvdykuc2Nyb2xsTGVmdCgpID4gJCh3aW5kb3cpLndpZHRoKCkgLyAyXG5cdFx0XHRcdD8gJ3JpZ2h0J1xuXHRcdFx0XHQ6ICdsZWZ0Jztcblx0XHRcdGNvbnN0IHZlcnRpY2FsID0gJHRhcmdldC5vZmZzZXQoKS50b3AgLSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgPiAkKHdpbmRvdykuaGVpZ2h0KCkgLyAyXG5cdFx0XHRcdD8gJ2JvdHRvbSdcblx0XHRcdFx0OiAndG9wJztcblx0XHRcdFxuXHRcdFx0cmV0dXJuIGhvcml6b250YWwgKyAnICcgKyB2ZXJ0aWNhbDtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSB0b29sdGlwIGZvciBzdGF0aWMgdGFibGUgZGF0YS5cblx0XHQgKlxuXHRcdCAqIFJlcGxhY2VzIHRoZSBicm93c2VycyBkZWZhdWx0IHRvb2x0aXAgd2l0aCBhIHFUaXAgaW5zdGFuY2UgZm9yIGV2ZXJ5IGVsZW1lbnQgb24gdGhlIHRhYmxlIHdoaWNoIGhhc1xuXHRcdCAqIGEgdGl0bGUgYXR0cmlidXRlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9pbml0VG9vbHRpcHNGb3JTdGF0aWNDb250ZW50KCkge1xuXHRcdFx0JHRoaXMuZmluZCgnW3RpdGxlXScpLnF0aXAoe1xuXHRcdFx0XHRzdHlsZToge2NsYXNzZXM6ICdneC1xdGlwIGluZm8nfVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNob3cgVG9vbHRpcFxuXHRcdCAqXG5cdFx0ICogRGlzcGxheSB0aGUgUXRpcCBpbnN0YW5jZSBvZiB0aGUgdGFyZ2V0LiBUaGUgdG9vbHRpcCBjb250ZW50cyBhcmUgZmV0Y2hlZCBhZnRlciB0aGUgdGFibGUgcmVxdWVzdFxuXHRcdCAqIGlzIGZpbmlzaGVkIGZvciBwZXJmb3JtYW5jZSByZWFzb25zLiBUaGlzIG1ldGhvZCB3aWxsIG5vdCBzaG93IGFueXRoaW5nIHVudGlsIHRoZSB0b29sdGlwIGNvbnRlbnRzXG5cdFx0ICogYXJlIGZldGNoZWQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2hvd1Rvb2x0aXAoZXZlbnQpIHtcblx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCBwcm9kdWN0SWQgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKTtcblx0XHRcdFxuXHRcdFx0aWYgKCF0b29sdGlwc1twcm9kdWN0SWRdKSB7XG5cdFx0XHRcdHJldHVybjsgLy8gVGhlIHJlcXVlc3RlZCB0b29sdGlwIGlzIG5vdCBsb2FkZWQsIGRvIG5vdCBjb250aW51ZS5cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Y29uc3QgdG9vbHRpcFBvc2l0aW9uID0gX2dldFRvb2x0aXBQb3NpdGlvbigkKHRoaXMpKTtcblx0XHRcdGNvbnN0IHRhcmdldFBvc2l0aW9uID0gX2dldFRhcmdldFBvc2l0aW9uKCQodGhpcykpO1xuXHRcdFx0XG5cdFx0XHQkKHRoaXMpLnF0aXAoe1xuXHRcdFx0XHRjb250ZW50OiB0b29sdGlwc1twcm9kdWN0SWRdW2V2ZW50LmRhdGEubmFtZV0sXG5cdFx0XHRcdHN0eWxlOiB7XG5cdFx0XHRcdFx0Y2xhc3NlczogJ2d4LXF0aXAgaW5mbydcblx0XHRcdFx0fSxcblx0XHRcdFx0cG9zaXRpb246IHtcblx0XHRcdFx0XHRteTogdG9vbHRpcFBvc2l0aW9uLFxuXHRcdFx0XHRcdGF0OiB0YXJnZXRQb3NpdGlvbixcblx0XHRcdFx0XHRlZmZlY3Q6IGZhbHNlLFxuXHRcdFx0XHRcdHZpZXdwb3J0OiAkKHdpbmRvdyksXG5cdFx0XHRcdFx0YWRqdXN0OiB7XG5cdFx0XHRcdFx0XHRtZXRob2Q6ICdub25lIHNoaWZ0J1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSxcblx0XHRcdFx0aGlkZToge1xuXHRcdFx0XHRcdGZpeGVkOiB0cnVlLFxuXHRcdFx0XHRcdGRlbGF5OiAxMDBcblx0XHRcdFx0fSxcblx0XHRcdFx0c2hvdzoge1xuXHRcdFx0XHRcdHJlYWR5OiB0cnVlLFxuXHRcdFx0XHRcdGRlbGF5OiAzMDBcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXZlbnRzOiB7XG5cdFx0XHRcdFx0aGlkZGVuOiAoZXZlbnQsIGFwaSkgPT4ge1xuXHRcdFx0XHRcdFx0YXBpLmRlc3Ryb3kodHJ1ZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IFRvb2x0aXBzXG5cdFx0ICpcblx0XHQgKiBGZXRjaGVzIHRoZSB0b29sdGlwcyB3aXRoIGFuIEFKQVggcmVxdWVzdCwgYmFzZWQgb24gdGhlIGN1cnJlbnQgc3RhdGUgb2YgdGhlIHRhYmxlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRUb29sdGlwcygpIHtcblx0XHRcdHRvb2x0aXBzID0gW107XG5cdFx0XHRjb25zdCBkYXRhdGFibGVzWGhyUGFyYW1ldGVycyA9ICR0aGlzLkRhdGFUYWJsZSgpLmFqYXgucGFyYW1zKCk7XG5cdFx0XHQkLnBvc3Qob3B0aW9ucy5zb3VyY2VVcmwsIGRhdGF0YWJsZXNYaHJQYXJhbWV0ZXJzLCByZXNwb25zZSA9PiB0b29sdGlwcyA9IHJlc3BvbnNlLCAnanNvbicpXG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQub24oJ2RyYXcuZHQnLCBfaW5pdFRvb2x0aXBzRm9yU3RhdGljQ29udGVudClcblx0XHRcdFx0Lm9uKCd4aHIuZHQnLCBfZ2V0VG9vbHRpcHMpO1xuXHRcdFx0XG5cdFx0XHQkKHdpbmRvdykub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCAoKSA9PiB7XG5cdFx0XHRcdGlmICgkdGhpcy5EYXRhVGFibGUoKS5hamF4Lmpzb24oKSAhPT0gdW5kZWZpbmVkICYmIHRvb2x0aXBzID09PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0XHRfZ2V0VG9vbHRpcHMoKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGZvciAobGV0IGV2ZW50IGluIG9wdGlvbnMuc2VsZWN0b3JzKSB7XG5cdFx0XHRcdGZvciAobGV0IG5hbWUgaW4gb3B0aW9ucy5zZWxlY3RvcnNbZXZlbnRdKSB7XG5cdFx0XHRcdFx0JHRoaXMub24oZXZlbnQsIG9wdGlvbnMuc2VsZWN0b3JzW2V2ZW50XVtuYW1lXSwge25hbWV9LCBfc2hvd1Rvb2x0aXApO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7Il19
