'use strict';

gx.controllers.module(
// controller name
'edit',

// controller libraries
[],

// controller business logic
function (data) {
	'use strict';

	// variables
	/**
  * Controller reference.
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default options for controller,
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final controller options.
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module object.
  *
  * @type {{}}
  */
	var module = {};

	// private methods

	/**
  * Sets all event handler for the product content forms.
  *
  * @private
  */
	var _setEventHandler = function _setEventHandler() {
		$this.find('form').on('submit', _formValidation);
	};

	// event handler

	/**
  * Validates the product contents creation and edit form.
  * Ensures that the internal_name value is set.
  *
  * @param {jQuery.Event} e Event object.
  * @private
  */
	var _formValidation = function _formValidation(e) {
		var $form = $(e.currentTarget);
		var $internalName = $form.find('input[name="content_manager[' + $form.attr('id') + '][internal_name]"]');

		if ($internalName.val() === '') {
			e.stopPropagation();
			e.preventDefault();

			$internalName.parents('.form-group').addClass('has-error');
			return;
		}
		$internalName.parents('.form-group').removeClass('has-error');
	};

	// initialization
	module.init = function (done) {
		_setEventHandler();
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRfbWFuYWdlci9wcm9kdWN0X2NvbnRlbnQvZWRpdC5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9zZXRFdmVudEhhbmRsZXIiLCJmaW5kIiwib24iLCJfZm9ybVZhbGlkYXRpb24iLCIkZm9ybSIsImUiLCJjdXJyZW50VGFyZ2V0IiwiJGludGVybmFsTmFtZSIsImF0dHIiLCJ2YWwiLCJzdG9wUHJvcGFnYXRpb24iLCJwcmV2ZW50RGVmYXVsdCIsInBhcmVudHMiLCJhZGRDbGFzcyIsInJlbW92ZUNsYXNzIiwiaW5pdCIsImRvbmUiXSwibWFwcGluZ3MiOiI7O0FBQUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZjtBQUNDO0FBQ0EsTUFGRDs7QUFJQztBQUNBLEVBTEQ7O0FBT0M7QUFDQSxVQUFTQyxJQUFULEVBQWU7QUFDZDs7QUFFQTtBQUNBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXLEVBQWpCOztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTs7QUFFQTs7Ozs7QUFLQSxLQUFNTyxtQkFBbUIsU0FBbkJBLGdCQUFtQixHQUFNO0FBQzlCTCxRQUFNTSxJQUFOLENBQVcsTUFBWCxFQUFtQkMsRUFBbkIsQ0FBc0IsUUFBdEIsRUFBZ0NDLGVBQWhDO0FBQ0EsRUFGRDs7QUFJQTs7QUFFQTs7Ozs7OztBQU9BLEtBQU1BLGtCQUFrQixTQUFsQkEsZUFBa0IsSUFBSztBQUM1QixNQUFNQyxRQUFRUixFQUFFUyxFQUFFQyxhQUFKLENBQWQ7QUFDQSxNQUFNQyxnQkFBZ0JILE1BQU1ILElBQU4sQ0FBVyxpQ0FBaUNHLE1BQU1JLElBQU4sQ0FBVyxJQUFYLENBQWpDLEdBQzlCLG9CQURtQixDQUF0Qjs7QUFHQSxNQUFJRCxjQUFjRSxHQUFkLE9BQXdCLEVBQTVCLEVBQWdDO0FBQy9CSixLQUFFSyxlQUFGO0FBQ0FMLEtBQUVNLGNBQUY7O0FBRUFKLGlCQUFjSyxPQUFkLENBQXNCLGFBQXRCLEVBQXFDQyxRQUFyQyxDQUE4QyxXQUE5QztBQUNBO0FBQ0E7QUFDRE4sZ0JBQWNLLE9BQWQsQ0FBc0IsYUFBdEIsRUFBcUNFLFdBQXJDLENBQWlELFdBQWpEO0FBQ0EsRUFiRDs7QUFlQTtBQUNBckIsUUFBT3NCLElBQVAsR0FBYyxnQkFBUTtBQUNyQmY7QUFDQWdCO0FBQ0EsRUFIRDs7QUFLQSxRQUFPdkIsTUFBUDtBQUNBLENBbEZGIiwiZmlsZSI6ImNvbnRlbnRfbWFuYWdlci9wcm9kdWN0X2NvbnRlbnQvZWRpdC5qcyIsInNvdXJjZXNDb250ZW50IjpbImd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0Ly8gY29udHJvbGxlciBuYW1lXG5cdCdlZGl0Jyxcblx0XG5cdC8vIGNvbnRyb2xsZXIgbGlicmFyaWVzXG5cdFtdLFxuXHRcblx0Ly8gY29udHJvbGxlciBidXNpbmVzcyBsb2dpY1xuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIHZhcmlhYmxlc1xuXHRcdC8qKlxuXHRcdCAqIENvbnRyb2xsZXIgcmVmZXJlbmNlLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBvcHRpb25zIGZvciBjb250cm9sbGVyLFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBkZWZhdWx0cyA9IHt9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmFsIGNvbnRyb2xsZXIgb3B0aW9ucy5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIG9iamVjdC5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHt7fX1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyBwcml2YXRlIG1ldGhvZHNcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZXRzIGFsbCBldmVudCBoYW5kbGVyIGZvciB0aGUgcHJvZHVjdCBjb250ZW50IGZvcm1zLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRjb25zdCBfc2V0RXZlbnRIYW5kbGVyID0gKCkgPT4ge1xuXHRcdFx0JHRoaXMuZmluZCgnZm9ybScpLm9uKCdzdWJtaXQnLCBfZm9ybVZhbGlkYXRpb24pO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gZXZlbnQgaGFuZGxlclxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFZhbGlkYXRlcyB0aGUgcHJvZHVjdCBjb250ZW50cyBjcmVhdGlvbiBhbmQgZWRpdCBmb3JtLlxuXHRcdCAqIEVuc3VyZXMgdGhhdCB0aGUgaW50ZXJuYWxfbmFtZSB2YWx1ZSBpcyBzZXQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZSBFdmVudCBvYmplY3QuXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRjb25zdCBfZm9ybVZhbGlkYXRpb24gPSBlID0+IHtcblx0XHRcdGNvbnN0ICRmb3JtID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuXHRcdFx0Y29uc3QgJGludGVybmFsTmFtZSA9ICRmb3JtLmZpbmQoJ2lucHV0W25hbWU9XCJjb250ZW50X21hbmFnZXJbJyArICRmb3JtLmF0dHIoJ2lkJylcblx0XHRcdFx0KyAnXVtpbnRlcm5hbF9uYW1lXVwiXScpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJGludGVybmFsTmFtZS52YWwoKSA9PT0gJycpIHtcblx0XHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0JGludGVybmFsTmFtZS5wYXJlbnRzKCcuZm9ybS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0JGludGVybmFsTmFtZS5wYXJlbnRzKCcuZm9ybS1ncm91cCcpLnJlbW92ZUNsYXNzKCdoYXMtZXJyb3InKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIGluaXRpYWxpemF0aW9uXG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdF9zZXRFdmVudEhhbmRsZXIoKTtcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7Il19
