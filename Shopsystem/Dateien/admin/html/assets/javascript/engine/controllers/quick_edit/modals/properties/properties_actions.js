'use strict';

/* --------------------------------------------------------------
 actions.js 2017-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Table Actions Controller
 *
 * This module creates the bulk and row actions for the properties table.
 */
gx.controllers.module('properties_actions', [gx.source + '/libs/button_dropdown', 'user_configuration_service'], function () {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Create Bulk Actions
  *
  * This callback can be called once during the initialization of this module.
  */
	function _createBulkActions() {
		// Add actions to the bulk-action dropdown.
		var $bulkActions = $('.properties-bulk-action');
		var defaultBulkAction = 'properties-bulk-row-edit';

		// Edit
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
			class: 'properties-bulk-row-edit',
			data: { configurationValue: 'properties-bulk-row-edit' },
			isDefault: defaultBulkAction === 'properties-bulk-row-edit' || defaultBulkAction === 'save-properties-bulk-row-edits',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Save
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
			class: 'save-properties-bulk-row-edits hidden',
			data: { configurationValue: 'save-properties-bulk-row-edits' },
			isDefault: false, // "Save" must not be shown as a default value. 
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		$this.datatable_default_actions('ensure', 'bulk');
	}

	function _createRowActions() {
		var defaultRowAction = $this.data('defaultRowAction') || 'row-properties-edit';

		$this.find('.btn-group.dropdown').each(function () {
			// Edit
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
				class: 'row-properties-edit',
				data: { configurationValue: 'row-properties-edit' },
				isDefault: defaultRowAction === 'row-properties-edit' || defaultRowAction === 'save-properties-edits',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Save
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
				class: 'save-properties-edits hidden',
				data: { configurationValue: 'save-properties-edits' },
				isDefault: false, // "Save" must not be shown as a default value. 
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			$this.datatable_default_actions('ensure', 'row');
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(window).on('JSENGINE_INIT_FINISHED', function () {
			$this.on('draw.dt', _createRowActions);
			_createRowActions();
			_createBulkActions();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc19hY3Rpb25zLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJzb3VyY2UiLCIkdGhpcyIsIiQiLCJfY3JlYXRlQnVsa0FjdGlvbnMiLCIkYnVsa0FjdGlvbnMiLCJkZWZhdWx0QnVsa0FjdGlvbiIsImpzZSIsImxpYnMiLCJidXR0b25fZHJvcGRvd24iLCJhZGRBY3Rpb24iLCJ0ZXh0IiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJjbGFzcyIsImRhdGEiLCJjb25maWd1cmF0aW9uVmFsdWUiLCJpc0RlZmF1bHQiLCJjYWxsYmFjayIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImRhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnMiLCJfY3JlYXRlUm93QWN0aW9ucyIsImRlZmF1bHRSb3dBY3Rpb24iLCJmaW5kIiwiZWFjaCIsImluaXQiLCJkb25lIiwid2luZG93Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msb0JBREQsRUFHQyxDQUFJRixHQUFHRyxNQUFQLDRCQUFzQyw0QkFBdEMsQ0FIRCxFQUtDLFlBQVc7O0FBRVY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNJLGtCQUFULEdBQThCO0FBQzdCO0FBQ0EsTUFBTUMsZUFBZUYsRUFBRSx5QkFBRixDQUFyQjtBQUNBLE1BQU1HLG9CQUFvQiwwQkFBMUI7O0FBRUE7QUFDQUMsTUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ0wsWUFBbkMsRUFBaUQ7QUFDaERNLFNBQU1KLElBQUlLLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGFBQXhCLEVBQXVDLGtCQUF2QyxDQUQwQztBQUVoREMsVUFBTywwQkFGeUM7QUFHaERDLFNBQU0sRUFBQ0Msb0JBQW9CLDBCQUFyQixFQUgwQztBQUloREMsY0FBV1osc0JBQXNCLDBCQUF0QixJQUNSQSxzQkFBc0IsZ0NBTHVCO0FBTWhEYSxhQUFVO0FBQUEsV0FBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFOc0MsR0FBakQ7O0FBU0E7QUFDQWQsTUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ0wsWUFBbkMsRUFBaUQ7QUFDaERNLFNBQU1KLElBQUlLLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGFBQXhCLEVBQXVDLGtCQUF2QyxDQUQwQztBQUVoREMsVUFBTyx1Q0FGeUM7QUFHaERDLFNBQU0sRUFBQ0Msb0JBQW9CLGdDQUFyQixFQUgwQztBQUloREMsY0FBVyxLQUpxQyxFQUk5QjtBQUNsQkMsYUFBVTtBQUFBLFdBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTHNDLEdBQWpEOztBQVFBbkIsUUFBTW9CLHlCQUFOLENBQWdDLFFBQWhDLEVBQTBDLE1BQTFDO0FBQ0E7O0FBRUQsVUFBU0MsaUJBQVQsR0FBNkI7QUFDNUIsTUFBTUMsbUJBQW1CdEIsTUFBTWMsSUFBTixDQUFXLGtCQUFYLEtBQWtDLHFCQUEzRDs7QUFFQWQsUUFBTXVCLElBQU4sQ0FBVyxxQkFBWCxFQUFrQ0MsSUFBbEMsQ0FBdUMsWUFBVztBQUNqRDtBQUNBbkIsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ1AsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDUSxVQUFNSixJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxrQkFBdkMsQ0FEcUM7QUFFM0NDLFdBQU8scUJBRm9DO0FBRzNDQyxVQUFNLEVBQUNDLG9CQUFvQixxQkFBckIsRUFIcUM7QUFJM0NDLGVBQVdNLHFCQUFxQixxQkFBckIsSUFDUkEscUJBQXFCLHVCQUxtQjtBQU0zQ0wsY0FBVTtBQUFBLFlBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTmlDLElBQTVDOztBQVNBO0FBQ0FkLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNQLEVBQUUsSUFBRixDQUFuQyxFQUE0QztBQUMzQ1EsVUFBTUosSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsYUFBeEIsRUFBdUMsa0JBQXZDLENBRHFDO0FBRTNDQyxXQUFPLDhCQUZvQztBQUczQ0MsVUFBTSxFQUFDQyxvQkFBb0IsdUJBQXJCLEVBSHFDO0FBSTNDQyxlQUFXLEtBSmdDLEVBSXpCO0FBQ2xCQyxjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsSUFBNUM7O0FBUUFuQixTQUFNb0IseUJBQU4sQ0FBZ0MsUUFBaEMsRUFBMEMsS0FBMUM7QUFDQSxHQXJCRDtBQXNCQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUF0QixRQUFPMkIsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QnpCLElBQUUwQixNQUFGLEVBQVVDLEVBQVYsQ0FBYSx3QkFBYixFQUF1QyxZQUFNO0FBQzVDNUIsU0FBTTRCLEVBQU4sQ0FBUyxTQUFULEVBQW9CUCxpQkFBcEI7QUFDQUE7QUFDQW5CO0FBQ0EsR0FKRDs7QUFNQXdCO0FBQ0EsRUFSRDs7QUFVQSxRQUFPNUIsTUFBUDtBQUVBLENBMUdGIiwiZmlsZSI6InF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc19hY3Rpb25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBhY3Rpb25zLmpzIDIwMTctMDUtMjlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIFByb3BlcnRpZXMgVGFibGUgQWN0aW9ucyBDb250cm9sbGVyXG4gKlxuICogVGhpcyBtb2R1bGUgY3JlYXRlcyB0aGUgYnVsayBhbmQgcm93IGFjdGlvbnMgZm9yIHRoZSBwcm9wZXJ0aWVzIHRhYmxlLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdwcm9wZXJ0aWVzX2FjdGlvbnMnLFxuXHRcblx0W2Ake2d4LnNvdXJjZX0vbGlicy9idXR0b25fZHJvcGRvd25gLCAndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnXSxcblx0XG5cdGZ1bmN0aW9uKCkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDcmVhdGUgQnVsayBBY3Rpb25zXG5cdFx0ICpcblx0XHQgKiBUaGlzIGNhbGxiYWNrIGNhbiBiZSBjYWxsZWQgb25jZSBkdXJpbmcgdGhlIGluaXRpYWxpemF0aW9uIG9mIHRoaXMgbW9kdWxlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9jcmVhdGVCdWxrQWN0aW9ucygpIHtcblx0XHRcdC8vIEFkZCBhY3Rpb25zIHRvIHRoZSBidWxrLWFjdGlvbiBkcm9wZG93bi5cblx0XHRcdGNvbnN0ICRidWxrQWN0aW9ucyA9ICQoJy5wcm9wZXJ0aWVzLWJ1bGstYWN0aW9uJyk7XG5cdFx0XHRjb25zdCBkZWZhdWx0QnVsa0FjdGlvbiA9ICdwcm9wZXJ0aWVzLWJ1bGstcm93LWVkaXQnO1xuXHRcdFx0XG5cdFx0XHQvLyBFZGl0XG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCRidWxrQWN0aW9ucywge1xuXHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX0VESVQnLCAnYWRtaW5fcXVpY2tfZWRpdCcpLFxuXHRcdFx0XHRjbGFzczogJ3Byb3BlcnRpZXMtYnVsay1yb3ctZWRpdCcsXG5cdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdwcm9wZXJ0aWVzLWJ1bGstcm93LWVkaXQnfSxcblx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0QnVsa0FjdGlvbiA9PT0gJ3Byb3BlcnRpZXMtYnVsay1yb3ctZWRpdCdcblx0XHRcdFx0fHwgZGVmYXVsdEJ1bGtBY3Rpb24gPT09ICdzYXZlLXByb3BlcnRpZXMtYnVsay1yb3ctZWRpdHMnLFxuXHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBTYXZlXG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCRidWxrQWN0aW9ucywge1xuXHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX1NBVkUnLCAnYWRtaW5fcXVpY2tfZWRpdCcpLFxuXHRcdFx0XHRjbGFzczogJ3NhdmUtcHJvcGVydGllcy1idWxrLXJvdy1lZGl0cyBoaWRkZW4nLFxuXHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnc2F2ZS1wcm9wZXJ0aWVzLWJ1bGstcm93LWVkaXRzJ30sXG5cdFx0XHRcdGlzRGVmYXVsdDogZmFsc2UsIC8vIFwiU2F2ZVwiIG11c3Qgbm90IGJlIHNob3duIGFzIGEgZGVmYXVsdCB2YWx1ZS4gXG5cdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmRhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnMoJ2Vuc3VyZScsICdidWxrJyk7XG5cdFx0fVxuXHRcdFxuXHRcdGZ1bmN0aW9uIF9jcmVhdGVSb3dBY3Rpb25zKCkge1xuXHRcdFx0Y29uc3QgZGVmYXVsdFJvd0FjdGlvbiA9ICR0aGlzLmRhdGEoJ2RlZmF1bHRSb3dBY3Rpb24nKSB8fCAncm93LXByb3BlcnRpZXMtZWRpdCc7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQoJy5idG4tZ3JvdXAuZHJvcGRvd24nKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQvLyBFZGl0XG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fRURJVCcsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdFx0Y2xhc3M6ICdyb3ctcHJvcGVydGllcy1lZGl0Jyxcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAncm93LXByb3BlcnRpZXMtZWRpdCd9LFxuXHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ3Jvdy1wcm9wZXJ0aWVzLWVkaXQnXG5cdFx0XHRcdFx0fHwgZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ3NhdmUtcHJvcGVydGllcy1lZGl0cycsXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2F2ZVxuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcblx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX1NBVkUnLCAnYWRtaW5fcXVpY2tfZWRpdCcpLFxuXHRcdFx0XHRcdGNsYXNzOiAnc2F2ZS1wcm9wZXJ0aWVzLWVkaXRzIGhpZGRlbicsXG5cdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ3NhdmUtcHJvcGVydGllcy1lZGl0cyd9LFxuXHRcdFx0XHRcdGlzRGVmYXVsdDogZmFsc2UsIC8vIFwiU2F2ZVwiIG11c3Qgbm90IGJlIHNob3duIGFzIGEgZGVmYXVsdCB2YWx1ZS4gXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0JHRoaXMuZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucygnZW5zdXJlJywgJ3JvdycpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkKHdpbmRvdykub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCAoKSA9PiB7XG5cdFx0XHRcdCR0aGlzLm9uKCdkcmF3LmR0JywgX2NyZWF0ZVJvd0FjdGlvbnMpO1xuXHRcdFx0XHRfY3JlYXRlUm93QWN0aW9ucygpO1xuXHRcdFx0XHRfY3JlYXRlQnVsa0FjdGlvbnMoKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHRcdFxuXHR9KTsgIl19
