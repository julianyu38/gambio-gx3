'use strict';

/* --------------------------------------------------------------
 tooltip.js 2017-05-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Special Prices Table Tooltips Controller
 *
 * This controller displays tooltips for the QuickEdit overview table. The tooltips are loaded after the
 * table data request is ready for optimization purposes.
 */
gx.controllers.module('special_prices_tooltips', [jse.source + '/vendor/qtip2/jquery.qtip.css', jse.source + '/vendor/qtip2/jquery.qtip.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @var {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		sourceUrl: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditSpecialPricesAjax/Tooltips',
		selectors: {
			mouseenter: {
				specials: '.tooltip-products-special-price'
			}
		}
	};

	/**
  * Final Options
  *
  * @var {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Tooltip Contents
  *
  * Contains the rendered HTML of the tooltips. The HTML is rendered with each table draw.
  *
  * e.g. tooltips.400210.orderItems >> HTML for order items tooltip of order #400210.
  *
  * @type {Object}
  */
	var tooltips = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Target Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTargetPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'left' : 'right';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'top' : 'bottom';

		return horizontal + ' ' + vertical;
	}

	/**
  * Get Tooltip Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTooltipPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'right' : 'left';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'bottom' : 'top';

		return horizontal + ' ' + vertical;
	}

	/**
  * Initialize tooltip for static table data.
  *
  * Replaces the browsers default tooltip with a qTip instance for every element on the table which has
  * a title attribute.
  */
	function _initTooltipsForStaticContent() {
		$this.find('[title]').qtip({
			style: { classes: 'gx-qtip info' }
		});
	}

	/**
  * Show Tooltip
  *
  * Display the Qtip instance of the target. The tooltip contents are fetched after the table request
  * is finished for performance reasons. This method will not show anything until the tooltip contents
  * are fetched.
  *
  * @param {jQuery.Event} event
  */
	function _showTooltip(event) {
		event.stopPropagation();

		var productId = $(this).parents('tr').data('id');

		if (!tooltips[productId]) {
			return; // The requested tooltip is not loaded, do not continue.
		}

		var tooltipPosition = _getTooltipPosition($(this));
		var targetPosition = _getTargetPosition($(this));

		$(this).qtip({
			content: tooltips[productId][event.data.name],
			style: {
				classes: 'gx-qtip info'
			},
			position: {
				my: tooltipPosition,
				at: targetPosition,
				effect: false,
				viewport: $(window),
				adjust: {
					method: 'none shift'
				}
			},
			hide: {
				fixed: true,
				delay: 100
			},
			show: {
				ready: true,
				delay: 300
			},
			events: {
				hidden: function hidden(event, api) {
					api.destroy(true);
				}
			}
		});
	}

	/**
  * Get Tooltips
  *
  * Fetches the tooltips with an AJAX request, based on the current state of the table.
  */
	function _getTooltips() {
		tooltips = [];
		var datatablesXhrParameters = $this.DataTable().ajax.params();
		$.post(options.sourceUrl, datatablesXhrParameters, function (response) {
			return tooltips = response;
		}, 'json');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('draw.dt', _initTooltipsForStaticContent).on('xhr.dt', _getTooltips);

		$(window).on('JSENGINE_INIT_FINISHED', function () {
			if ($this.DataTable().ajax.json() !== undefined && tooltips === undefined) {
				_getTooltips();
			}
		});

		for (var event in options.selectors) {
			for (var name in options.selectors[event]) {
				$this.on(event, options.selectors[event][name], { name: name }, _showTooltip);
			}
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3NwZWNpYWxfcHJpY2VzL3NwZWNpYWxfcHJpY2VzX3Rvb2x0aXBzLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJzb3VyY2VVcmwiLCJjb3JlIiwiY29uZmlnIiwiZ2V0Iiwic2VsZWN0b3JzIiwibW91c2VlbnRlciIsInNwZWNpYWxzIiwib3B0aW9ucyIsImV4dGVuZCIsInRvb2x0aXBzIiwiX2dldFRhcmdldFBvc2l0aW9uIiwiJHRhcmdldCIsImhvcml6b250YWwiLCJvZmZzZXQiLCJsZWZ0Iiwid2luZG93Iiwic2Nyb2xsTGVmdCIsIndpZHRoIiwidmVydGljYWwiLCJ0b3AiLCJzY3JvbGxUb3AiLCJoZWlnaHQiLCJfZ2V0VG9vbHRpcFBvc2l0aW9uIiwiX2luaXRUb29sdGlwc0ZvclN0YXRpY0NvbnRlbnQiLCJmaW5kIiwicXRpcCIsInN0eWxlIiwiY2xhc3NlcyIsIl9zaG93VG9vbHRpcCIsImV2ZW50Iiwic3RvcFByb3BhZ2F0aW9uIiwicHJvZHVjdElkIiwicGFyZW50cyIsInRvb2x0aXBQb3NpdGlvbiIsInRhcmdldFBvc2l0aW9uIiwiY29udGVudCIsIm5hbWUiLCJwb3NpdGlvbiIsIm15IiwiYXQiLCJlZmZlY3QiLCJ2aWV3cG9ydCIsImFkanVzdCIsIm1ldGhvZCIsImhpZGUiLCJmaXhlZCIsImRlbGF5Iiwic2hvdyIsInJlYWR5IiwiZXZlbnRzIiwiaGlkZGVuIiwiYXBpIiwiZGVzdHJveSIsIl9nZXRUb29sdGlwcyIsImRhdGF0YWJsZXNYaHJQYXJhbWV0ZXJzIiwiRGF0YVRhYmxlIiwiYWpheCIsInBhcmFtcyIsInBvc3QiLCJyZXNwb25zZSIsImluaXQiLCJkb25lIiwib24iLCJqc29uIiwidW5kZWZpbmVkIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7OztBQU1BQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyx5QkFERCxFQUdDLENBQ0lDLElBQUlDLE1BRFIsb0NBRUlELElBQUlDLE1BRlIsa0NBSEQsRUFRQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCQyxhQUFXTixJQUFJTyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHlEQUQzQjtBQUVoQkMsYUFBVztBQUNWQyxlQUFZO0FBQ1hDLGNBQVU7QUFEQztBQURGO0FBRkssRUFBakI7O0FBU0E7Ozs7O0FBS0EsS0FBTUMsVUFBVVQsRUFBRVUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CVCxRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUgsU0FBUyxFQUFmOztBQUVBOzs7Ozs7Ozs7QUFTQSxLQUFJZ0IsaUJBQUo7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O0FBT0EsVUFBU0Msa0JBQVQsQ0FBNEJDLE9BQTVCLEVBQXFDO0FBQ3BDLE1BQU1DLGFBQWFELFFBQVFFLE1BQVIsR0FBaUJDLElBQWpCLEdBQXdCaEIsRUFBRWlCLE1BQUYsRUFBVUMsVUFBVixFQUF4QixHQUFpRGxCLEVBQUVpQixNQUFGLEVBQVVFLEtBQVYsS0FBb0IsQ0FBckUsR0FDaEIsTUFEZ0IsR0FFaEIsT0FGSDtBQUdBLE1BQU1DLFdBQVdQLFFBQVFFLE1BQVIsR0FBaUJNLEdBQWpCLEdBQXVCckIsRUFBRWlCLE1BQUYsRUFBVUssU0FBVixFQUF2QixHQUErQ3RCLEVBQUVpQixNQUFGLEVBQVVNLE1BQVYsS0FBcUIsQ0FBcEUsR0FDZCxLQURjLEdBRWQsUUFGSDs7QUFJQSxTQUFPVCxhQUFhLEdBQWIsR0FBbUJNLFFBQTFCO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTSSxtQkFBVCxDQUE2QlgsT0FBN0IsRUFBc0M7QUFDckMsTUFBTUMsYUFBYUQsUUFBUUUsTUFBUixHQUFpQkMsSUFBakIsR0FBd0JoQixFQUFFaUIsTUFBRixFQUFVQyxVQUFWLEVBQXhCLEdBQWlEbEIsRUFBRWlCLE1BQUYsRUFBVUUsS0FBVixLQUFvQixDQUFyRSxHQUNoQixPQURnQixHQUVoQixNQUZIO0FBR0EsTUFBTUMsV0FBV1AsUUFBUUUsTUFBUixHQUFpQk0sR0FBakIsR0FBdUJyQixFQUFFaUIsTUFBRixFQUFVSyxTQUFWLEVBQXZCLEdBQStDdEIsRUFBRWlCLE1BQUYsRUFBVU0sTUFBVixLQUFxQixDQUFwRSxHQUNkLFFBRGMsR0FFZCxLQUZIOztBQUlBLFNBQU9ULGFBQWEsR0FBYixHQUFtQk0sUUFBMUI7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU0ssNkJBQVQsR0FBeUM7QUFDeEMxQixRQUFNMkIsSUFBTixDQUFXLFNBQVgsRUFBc0JDLElBQXRCLENBQTJCO0FBQzFCQyxVQUFPLEVBQUNDLFNBQVMsY0FBVjtBQURtQixHQUEzQjtBQUdBOztBQUVEOzs7Ozs7Ozs7QUFTQSxVQUFTQyxZQUFULENBQXNCQyxLQUF0QixFQUE2QjtBQUM1QkEsUUFBTUMsZUFBTjs7QUFFQSxNQUFNQyxZQUFZakMsRUFBRSxJQUFGLEVBQVFrQyxPQUFSLENBQWdCLElBQWhCLEVBQXNCcEMsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBbEI7O0FBRUEsTUFBSSxDQUFDYSxTQUFTc0IsU0FBVCxDQUFMLEVBQTBCO0FBQ3pCLFVBRHlCLENBQ2pCO0FBQ1I7O0FBRUQsTUFBTUUsa0JBQWtCWCxvQkFBb0J4QixFQUFFLElBQUYsQ0FBcEIsQ0FBeEI7QUFDQSxNQUFNb0MsaUJBQWlCeEIsbUJBQW1CWixFQUFFLElBQUYsQ0FBbkIsQ0FBdkI7O0FBRUFBLElBQUUsSUFBRixFQUFRMkIsSUFBUixDQUFhO0FBQ1pVLFlBQVMxQixTQUFTc0IsU0FBVCxFQUFvQkYsTUFBTWpDLElBQU4sQ0FBV3dDLElBQS9CLENBREc7QUFFWlYsVUFBTztBQUNOQyxhQUFTO0FBREgsSUFGSztBQUtaVSxhQUFVO0FBQ1RDLFFBQUlMLGVBREs7QUFFVE0sUUFBSUwsY0FGSztBQUdUTSxZQUFRLEtBSEM7QUFJVEMsY0FBVTNDLEVBQUVpQixNQUFGLENBSkQ7QUFLVDJCLFlBQVE7QUFDUEMsYUFBUTtBQUREO0FBTEMsSUFMRTtBQWNaQyxTQUFNO0FBQ0xDLFdBQU8sSUFERjtBQUVMQyxXQUFPO0FBRkYsSUFkTTtBQWtCWkMsU0FBTTtBQUNMQyxXQUFPLElBREY7QUFFTEYsV0FBTztBQUZGLElBbEJNO0FBc0JaRyxXQUFRO0FBQ1BDLFlBQVEsZ0JBQUNyQixLQUFELEVBQVFzQixHQUFSLEVBQWdCO0FBQ3ZCQSxTQUFJQyxPQUFKLENBQVksSUFBWjtBQUNBO0FBSE07QUF0QkksR0FBYjtBQTRCQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxZQUFULEdBQXdCO0FBQ3ZCNUMsYUFBVyxFQUFYO0FBQ0EsTUFBTTZDLDBCQUEwQnpELE1BQU0wRCxTQUFOLEdBQWtCQyxJQUFsQixDQUF1QkMsTUFBdkIsRUFBaEM7QUFDQTNELElBQUU0RCxJQUFGLENBQU9uRCxRQUFRUCxTQUFmLEVBQTBCc0QsdUJBQTFCLEVBQW1EO0FBQUEsVUFBWTdDLFdBQVdrRCxRQUF2QjtBQUFBLEdBQW5ELEVBQW9GLE1BQXBGO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBbEUsUUFBT21FLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJoRSxRQUNFaUUsRUFERixDQUNLLFNBREwsRUFDZ0J2Qyw2QkFEaEIsRUFFRXVDLEVBRkYsQ0FFSyxRQUZMLEVBRWVULFlBRmY7O0FBSUF2RCxJQUFFaUIsTUFBRixFQUFVK0MsRUFBVixDQUFhLHdCQUFiLEVBQXVDLFlBQU07QUFDNUMsT0FBSWpFLE1BQU0wRCxTQUFOLEdBQWtCQyxJQUFsQixDQUF1Qk8sSUFBdkIsT0FBa0NDLFNBQWxDLElBQStDdkQsYUFBYXVELFNBQWhFLEVBQTJFO0FBQzFFWDtBQUNBO0FBQ0QsR0FKRDs7QUFNQSxPQUFLLElBQUl4QixLQUFULElBQWtCdEIsUUFBUUgsU0FBMUIsRUFBcUM7QUFDcEMsUUFBSyxJQUFJZ0MsSUFBVCxJQUFpQjdCLFFBQVFILFNBQVIsQ0FBa0J5QixLQUFsQixDQUFqQixFQUEyQztBQUMxQ2hDLFVBQU1pRSxFQUFOLENBQVNqQyxLQUFULEVBQWdCdEIsUUFBUUgsU0FBUixDQUFrQnlCLEtBQWxCLEVBQXlCTyxJQUF6QixDQUFoQixFQUFnRCxFQUFDQSxVQUFELEVBQWhELEVBQXdEUixZQUF4RDtBQUNBO0FBQ0Q7O0FBRURpQztBQUNBLEVBbEJEOztBQW9CQSxRQUFPcEUsTUFBUDtBQUNBLENBek1GIiwiZmlsZSI6InF1aWNrX2VkaXQvbW9kYWxzL3NwZWNpYWxfcHJpY2VzL3NwZWNpYWxfcHJpY2VzX3Rvb2x0aXBzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiB0b29sdGlwLmpzIDIwMTctMDUtMThcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIFNwZWNpYWwgUHJpY2VzIFRhYmxlIFRvb2x0aXBzIENvbnRyb2xsZXJcbiAqXG4gKiBUaGlzIGNvbnRyb2xsZXIgZGlzcGxheXMgdG9vbHRpcHMgZm9yIHRoZSBRdWlja0VkaXQgb3ZlcnZpZXcgdGFibGUuIFRoZSB0b29sdGlwcyBhcmUgbG9hZGVkIGFmdGVyIHRoZVxuICogdGFibGUgZGF0YSByZXF1ZXN0IGlzIHJlYWR5IGZvciBvcHRpbWl6YXRpb24gcHVycG9zZXMuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3NwZWNpYWxfcHJpY2VzX3Rvb2x0aXBzJyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvcXRpcDIvanF1ZXJ5LnF0aXAuY3NzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvcXRpcDIvanF1ZXJ5LnF0aXAuanNgXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHZhciB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7XG5cdFx0XHRzb3VyY2VVcmw6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89UXVpY2tFZGl0U3BlY2lhbFByaWNlc0FqYXgvVG9vbHRpcHMnLFxuXHRcdFx0c2VsZWN0b3JzOiB7XG5cdFx0XHRcdG1vdXNlZW50ZXI6IHtcblx0XHRcdFx0XHRzcGVjaWFsczogJy50b29sdGlwLXByb2R1Y3RzLXNwZWNpYWwtcHJpY2UnLFxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdmFyIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBUb29sdGlwIENvbnRlbnRzXG5cdFx0ICpcblx0XHQgKiBDb250YWlucyB0aGUgcmVuZGVyZWQgSFRNTCBvZiB0aGUgdG9vbHRpcHMuIFRoZSBIVE1MIGlzIHJlbmRlcmVkIHdpdGggZWFjaCB0YWJsZSBkcmF3LlxuXHRcdCAqXG5cdFx0ICogZS5nLiB0b29sdGlwcy40MDAyMTAub3JkZXJJdGVtcyA+PiBIVE1MIGZvciBvcmRlciBpdGVtcyB0b29sdGlwIG9mIG9yZGVyICM0MDAyMTAuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGxldCB0b29sdGlwcztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgVGFyZ2V0IFBvc2l0aW9uXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRhcmdldFxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRUYXJnZXRQb3NpdGlvbigkdGFyZ2V0KSB7XG5cdFx0XHRjb25zdCBob3Jpem9udGFsID0gJHRhcmdldC5vZmZzZXQoKS5sZWZ0IC0gJCh3aW5kb3cpLnNjcm9sbExlZnQoKSA+ICQod2luZG93KS53aWR0aCgpIC8gMlxuXHRcdFx0XHQ/ICdsZWZ0J1xuXHRcdFx0XHQ6ICdyaWdodCc7XG5cdFx0XHRjb25zdCB2ZXJ0aWNhbCA9ICR0YXJnZXQub2Zmc2V0KCkudG9wIC0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpID4gJCh3aW5kb3cpLmhlaWdodCgpIC8gMlxuXHRcdFx0XHQ/ICd0b3AnXG5cdFx0XHRcdDogJ2JvdHRvbSc7XG5cdFx0XHRcblx0XHRcdHJldHVybiBob3Jpem9udGFsICsgJyAnICsgdmVydGljYWw7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdldCBUb29sdGlwIFBvc2l0aW9uXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRhcmdldFxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRUb29sdGlwUG9zaXRpb24oJHRhcmdldCkge1xuXHRcdFx0Y29uc3QgaG9yaXpvbnRhbCA9ICR0YXJnZXQub2Zmc2V0KCkubGVmdCAtICQod2luZG93KS5zY3JvbGxMZWZ0KCkgPiAkKHdpbmRvdykud2lkdGgoKSAvIDJcblx0XHRcdFx0PyAncmlnaHQnXG5cdFx0XHRcdDogJ2xlZnQnO1xuXHRcdFx0Y29uc3QgdmVydGljYWwgPSAkdGFyZ2V0Lm9mZnNldCgpLnRvcCAtICQod2luZG93KS5zY3JvbGxUb3AoKSA+ICQod2luZG93KS5oZWlnaHQoKSAvIDJcblx0XHRcdFx0PyAnYm90dG9tJ1xuXHRcdFx0XHQ6ICd0b3AnO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gaG9yaXpvbnRhbCArICcgJyArIHZlcnRpY2FsO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIHRvb2x0aXAgZm9yIHN0YXRpYyB0YWJsZSBkYXRhLlxuXHRcdCAqXG5cdFx0ICogUmVwbGFjZXMgdGhlIGJyb3dzZXJzIGRlZmF1bHQgdG9vbHRpcCB3aXRoIGEgcVRpcCBpbnN0YW5jZSBmb3IgZXZlcnkgZWxlbWVudCBvbiB0aGUgdGFibGUgd2hpY2ggaGFzXG5cdFx0ICogYSB0aXRsZSBhdHRyaWJ1dGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2luaXRUb29sdGlwc0ZvclN0YXRpY0NvbnRlbnQoKSB7XG5cdFx0XHQkdGhpcy5maW5kKCdbdGl0bGVdJykucXRpcCh7XG5cdFx0XHRcdHN0eWxlOiB7Y2xhc3NlczogJ2d4LXF0aXAgaW5mbyd9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2hvdyBUb29sdGlwXG5cdFx0ICpcblx0XHQgKiBEaXNwbGF5IHRoZSBRdGlwIGluc3RhbmNlIG9mIHRoZSB0YXJnZXQuIFRoZSB0b29sdGlwIGNvbnRlbnRzIGFyZSBmZXRjaGVkIGFmdGVyIHRoZSB0YWJsZSByZXF1ZXN0XG5cdFx0ICogaXMgZmluaXNoZWQgZm9yIHBlcmZvcm1hbmNlIHJlYXNvbnMuIFRoaXMgbWV0aG9kIHdpbGwgbm90IHNob3cgYW55dGhpbmcgdW50aWwgdGhlIHRvb2x0aXAgY29udGVudHNcblx0XHQgKiBhcmUgZmV0Y2hlZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9zaG93VG9vbHRpcChldmVudCkge1xuXHRcdFx0ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IHByb2R1Y3RJZCA9ICQodGhpcykucGFyZW50cygndHInKS5kYXRhKCdpZCcpO1xuXHRcdFx0XG5cdFx0XHRpZiAoIXRvb2x0aXBzW3Byb2R1Y3RJZF0pIHtcblx0XHRcdFx0cmV0dXJuOyAvLyBUaGUgcmVxdWVzdGVkIHRvb2x0aXAgaXMgbm90IGxvYWRlZCwgZG8gbm90IGNvbnRpbnVlLlxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRjb25zdCB0b29sdGlwUG9zaXRpb24gPSBfZ2V0VG9vbHRpcFBvc2l0aW9uKCQodGhpcykpO1xuXHRcdFx0Y29uc3QgdGFyZ2V0UG9zaXRpb24gPSBfZ2V0VGFyZ2V0UG9zaXRpb24oJCh0aGlzKSk7XG5cdFx0XHRcblx0XHRcdCQodGhpcykucXRpcCh7XG5cdFx0XHRcdGNvbnRlbnQ6IHRvb2x0aXBzW3Byb2R1Y3RJZF1bZXZlbnQuZGF0YS5uYW1lXSxcblx0XHRcdFx0c3R5bGU6IHtcblx0XHRcdFx0XHRjbGFzc2VzOiAnZ3gtcXRpcCBpbmZvJ1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRwb3NpdGlvbjoge1xuXHRcdFx0XHRcdG15OiB0b29sdGlwUG9zaXRpb24sXG5cdFx0XHRcdFx0YXQ6IHRhcmdldFBvc2l0aW9uLFxuXHRcdFx0XHRcdGVmZmVjdDogZmFsc2UsXG5cdFx0XHRcdFx0dmlld3BvcnQ6ICQod2luZG93KSxcblx0XHRcdFx0XHRhZGp1c3Q6IHtcblx0XHRcdFx0XHRcdG1ldGhvZDogJ25vbmUgc2hpZnQnXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9LFxuXHRcdFx0XHRoaWRlOiB7XG5cdFx0XHRcdFx0Zml4ZWQ6IHRydWUsXG5cdFx0XHRcdFx0ZGVsYXk6IDEwMFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRzaG93OiB7XG5cdFx0XHRcdFx0cmVhZHk6IHRydWUsXG5cdFx0XHRcdFx0ZGVsYXk6IDMwMFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRldmVudHM6IHtcblx0XHRcdFx0XHRoaWRkZW46IChldmVudCwgYXBpKSA9PiB7XG5cdFx0XHRcdFx0XHRhcGkuZGVzdHJveSh0cnVlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgVG9vbHRpcHNcblx0XHQgKlxuXHRcdCAqIEZldGNoZXMgdGhlIHRvb2x0aXBzIHdpdGggYW4gQUpBWCByZXF1ZXN0LCBiYXNlZCBvbiB0aGUgY3VycmVudCBzdGF0ZSBvZiB0aGUgdGFibGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldFRvb2x0aXBzKCkge1xuXHRcdFx0dG9vbHRpcHMgPSBbXTtcblx0XHRcdGNvbnN0IGRhdGF0YWJsZXNYaHJQYXJhbWV0ZXJzID0gJHRoaXMuRGF0YVRhYmxlKCkuYWpheC5wYXJhbXMoKTtcblx0XHRcdCQucG9zdChvcHRpb25zLnNvdXJjZVVybCwgZGF0YXRhYmxlc1hoclBhcmFtZXRlcnMsIHJlc3BvbnNlID0+IHRvb2x0aXBzID0gcmVzcG9uc2UsICdqc29uJylcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCR0aGlzXG5cdFx0XHRcdC5vbignZHJhdy5kdCcsIF9pbml0VG9vbHRpcHNGb3JTdGF0aWNDb250ZW50KVxuXHRcdFx0XHQub24oJ3hoci5kdCcsIF9nZXRUb29sdGlwcyk7XG5cdFx0XHRcblx0XHRcdCQod2luZG93KS5vbignSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsICgpID0+IHtcblx0XHRcdFx0aWYgKCR0aGlzLkRhdGFUYWJsZSgpLmFqYXguanNvbigpICE9PSB1bmRlZmluZWQgJiYgdG9vbHRpcHMgPT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRcdF9nZXRUb29sdGlwcygpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Zm9yIChsZXQgZXZlbnQgaW4gb3B0aW9ucy5zZWxlY3RvcnMpIHtcblx0XHRcdFx0Zm9yIChsZXQgbmFtZSBpbiBvcHRpb25zLnNlbGVjdG9yc1tldmVudF0pIHtcblx0XHRcdFx0XHQkdGhpcy5vbihldmVudCwgb3B0aW9ucy5zZWxlY3RvcnNbZXZlbnRdW25hbWVdLCB7bmFtZX0sIF9zaG93VG9vbHRpcCk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKTsiXX0=
