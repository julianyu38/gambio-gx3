'use strict';

/* --------------------------------------------------------------
 content_manager_delete.js 2017-09-07
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Content Manager Overview Delete
 *
 * Controller Module to delete Content Manger Entries and product contents.
 *
 * Handles the delete operation of the Content Manager overview pages.
 */
gx.controllers.module('content_manager_delete', ['modal', gx.source + '/libs/info_box'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Content Manager delete confirmation modal.
  *
  * @type {jQuery}
  */
	var $modal = $('.delete-content.modal');

	/**
  * Content Manager elements container
  * 
  * @type {jQuery}
  */
	var $elementContainer = $('.element-container');

	/**
  * Selectors for deleting different types of contents
  * 
  * @type {{basic: string, page: string, element: string}}
  */
	var deleteSelectors = {
		basic: '.delete-content',
		page: '.delete-content.page',
		element: '.delete-content.element'
	};

	/**
  * URLs for deleting different types of content
  * 
  * @type {{page: string, element: string}}
  */
	var urls = {
		page: jse.core.config.get('appUrl') + '/admin/admin.php?do=ContentManagerPagesAjax/delete',
		element: jse.core.config.get('appUrl') + '/admin/admin.php?do=ContentManagerElementsAjax/delete'

		/**
   * Module Instance
   *
   * @type {Object}
   */
	};var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Handles the delete click event by opening the delete confirmation modal.
  *
  * @param {jQuery.Event} event Triggered event.
  * @param {String} url URL for AjaxRequest.
  */
	function _onDeleteClick(event, url) {
		// Prevent default action.
		event.preventDefault();

		// Clicked content entry
		var $content_entry = $(event.target).closest('.content-action-icons').siblings('.content-name');

		// Id of the content that should be deleted
		var content_id = $(event.target).parents(deleteSelectors.basic).data('contentId');

		// Group Id of the content that should be deleted
		var group_id = $(event.target).parents(deleteSelectors.basic).data('groupId');

		// Delete confirmation modal button.
		var $confirmButton = $modal.find('button.confirm');

		// Empty modal body.
		$modal.find('.modal-body .form-group').remove();

		// Slider data.
		var contentData = {
			id: content_id,
			groupId: group_id,
			name: $content_entry.text()
		};

		// Put new slider information into modal body.
		$modal.find('.modal-body fieldset').html(_generateContentInfoMarkup(contentData));

		// Show modal.
		$modal.modal('show');

		// Handle delete confirmation modal button click event.
		$confirmButton.off('click').on('click', function () {
			return _onConfirmButtonClick(contentData.id, contentData.groupId, url);
		});
	}

	/**
  * Handles the delete confirmation button click event by removing the content through an AJAX request.
  *
  * @param {Number} contentId Content ID.
  * @param {Number} groupId Group ID.
  * @param {String} url URL for AjaxRequest.
  */
	function _onConfirmButtonClick(contentId, groupId, url) {
		// AJAX request options.
		var requestOptions = {
			type: 'POST',
			data: { id: groupId },
			url: url
		};

		// Perform request.
		$.ajax(requestOptions).done(function (response) {
			return _handleDeleteRequestResponse(response, contentId);
		}).always(function () {
			return $modal.modal('hide');
		});
	}

	/**
  * Handles content deletion AJAX action server response.
  *
  * @param {Object} response Server response.
  * @param {Number} contentId ID of deleted content.
  */
	function _handleDeleteRequestResponse(response, contentId) {
		// Error message phrases.
		var errorTitle = jse.core.lang.translate('DELETE_CONTENT_ERROR_TITLE', 'content_manager');
		var errorMessage = jse.core.lang.translate('DELETE_CONTENT_ERROR_TEXT', 'content_manager');

		// Element that triggers the delete action
		var $delete_trigger = $elementContainer.find('.delete-content' + ('[data-content-id="' + contentId + '"]'));

		// List element that should be removed
		var $element_to_delete = $delete_trigger.closest('li.content-manager-element');

		// List of all siblings of the removed element
		var $list_elements = $delete_trigger.closest('li.content-manager-element').siblings();

		// Element that contains all content list items
		var $list = $element_to_delete.closest('ul.content-manager-elements-list');

		// 'No results' message list element template.
		var $emptyListTemplate = $('#empty-list');

		// Check for action success.
		if (response.includes('success')) {
			// Delete respective table rows.
			$element_to_delete.remove();

			// Add success message to admin info box.
			jse.libs.info_box.addSuccessMessage();

			// If there are no rows, show 'No results' message row.
			if ($list_elements.length < 1) {
				$list.empty().append($emptyListTemplate.clone().html());
			}
		} else {
			// Show error message modal.
			jse.libs.modal.showMessage(errorTitle, errorMessage);
		}
	}

	/**
  * Generates HTML containing the content entry information for the delete confirmation modal.
  *
  * @param {Object} data Content Manager data.
  * @param {String} data.name Name of the Content Manager entry.
  * @param {Number} data.groupId Id of the Content Manager entry.
  *
  * @return {String} Created HTML string.
  */
	function _generateContentInfoMarkup(data) {
		// Label phrases.
		var contentNameLabel = jse.core.lang.translate('TEXT_TITLE', 'content_manager');
		var groupIdLabel = jse.core.lang.translate('TEXT_GROUP', 'content_manager');

		// Return markup.
		return '\n\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t<label class="col-md-5">' + contentNameLabel + '</label>\n\t\t\t\t\t\t<div class="col-md-7">' + data.name + '</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t<label class="col-md-5">' + groupIdLabel + '</label>\n\t\t\t\t\t\t<div class="col-md-7">' + data.groupId + '</div>\n\t\t\t\t\t</div>\n\t\t\t';
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Listen to click event on delete icon
		$this.on('click', deleteSelectors.page, function (event) {
			_onDeleteClick(event, urls.page);
		});

		$this.on('click', deleteSelectors.element, function (event) {
			_onDeleteClick(event, urls.element);
		});

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRfbWFuYWdlci9vdmVydmlldy9jb250ZW50X21hbmFnZXJfZGVsZXRlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJG1vZGFsIiwiJGVsZW1lbnRDb250YWluZXIiLCJkZWxldGVTZWxlY3RvcnMiLCJiYXNpYyIsInBhZ2UiLCJlbGVtZW50IiwidXJscyIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJfb25EZWxldGVDbGljayIsImV2ZW50IiwidXJsIiwicHJldmVudERlZmF1bHQiLCIkY29udGVudF9lbnRyeSIsInRhcmdldCIsImNsb3Nlc3QiLCJzaWJsaW5ncyIsImNvbnRlbnRfaWQiLCJwYXJlbnRzIiwiZ3JvdXBfaWQiLCIkY29uZmlybUJ1dHRvbiIsImZpbmQiLCJyZW1vdmUiLCJjb250ZW50RGF0YSIsImlkIiwiZ3JvdXBJZCIsIm5hbWUiLCJ0ZXh0IiwiaHRtbCIsIl9nZW5lcmF0ZUNvbnRlbnRJbmZvTWFya3VwIiwibW9kYWwiLCJvZmYiLCJvbiIsIl9vbkNvbmZpcm1CdXR0b25DbGljayIsImNvbnRlbnRJZCIsInJlcXVlc3RPcHRpb25zIiwidHlwZSIsImFqYXgiLCJkb25lIiwiX2hhbmRsZURlbGV0ZVJlcXVlc3RSZXNwb25zZSIsInJlc3BvbnNlIiwiYWx3YXlzIiwiZXJyb3JUaXRsZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJlcnJvck1lc3NhZ2UiLCIkZGVsZXRlX3RyaWdnZXIiLCIkZWxlbWVudF90b19kZWxldGUiLCIkbGlzdF9lbGVtZW50cyIsIiRsaXN0IiwiJGVtcHR5TGlzdFRlbXBsYXRlIiwiaW5jbHVkZXMiLCJsaWJzIiwiaW5mb19ib3giLCJhZGRTdWNjZXNzTWVzc2FnZSIsImxlbmd0aCIsImVtcHR5IiwiYXBwZW5kIiwiY2xvbmUiLCJzaG93TWVzc2FnZSIsImNvbnRlbnROYW1lTGFiZWwiLCJncm91cElkTGFiZWwiLCJpbml0Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msd0JBREQsRUFHQyxDQUNDLE9BREQsRUFFSUYsR0FBR0csTUFGUCxvQkFIRCxFQVFDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFNBQVNELEVBQUUsdUJBQUYsQ0FBZjs7QUFFQTs7Ozs7QUFLQSxLQUFNRSxvQkFBb0JGLEVBQUUsb0JBQUYsQ0FBMUI7O0FBRUE7Ozs7O0FBS0EsS0FBTUcsa0JBQWtCO0FBQ3ZCQyxTQUFPLGlCQURnQjtBQUV2QkMsUUFBTSxzQkFGaUI7QUFHdkJDLFdBQVM7QUFIYyxFQUF4Qjs7QUFNQTs7Ozs7QUFLQSxLQUFNQyxPQUFPO0FBQ1pGLFFBQVNHLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsQ0FBVCx1REFEWTtBQUVaTCxXQUFZRSxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQVo7O0FBR0Q7Ozs7O0FBTGEsRUFBYixDQVVBLElBQU1mLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BLFVBQVNnQixjQUFULENBQXdCQyxLQUF4QixFQUErQkMsR0FBL0IsRUFBb0M7QUFDbkM7QUFDQUQsUUFBTUUsY0FBTjs7QUFFQTtBQUNBLE1BQU1DLGlCQUFpQmhCLEVBQUVhLE1BQU1JLE1BQVIsRUFBZ0JDLE9BQWhCLENBQXdCLHVCQUF4QixFQUFpREMsUUFBakQsQ0FBMEQsZUFBMUQsQ0FBdkI7O0FBRUE7QUFDQSxNQUFNQyxhQUFhcEIsRUFBRWEsTUFBTUksTUFBUixFQUFnQkksT0FBaEIsQ0FBd0JsQixnQkFBZ0JDLEtBQXhDLEVBQStDTixJQUEvQyxDQUFvRCxXQUFwRCxDQUFuQjs7QUFFQTtBQUNBLE1BQU13QixXQUFXdEIsRUFBRWEsTUFBTUksTUFBUixFQUFnQkksT0FBaEIsQ0FBd0JsQixnQkFBZ0JDLEtBQXhDLEVBQStDTixJQUEvQyxDQUFvRCxTQUFwRCxDQUFqQjs7QUFFQTtBQUNBLE1BQU15QixpQkFBaUJ0QixPQUFPdUIsSUFBUCxDQUFZLGdCQUFaLENBQXZCOztBQUVBO0FBQ0F2QixTQUFPdUIsSUFBUCxDQUFZLHlCQUFaLEVBQXVDQyxNQUF2Qzs7QUFFQTtBQUNBLE1BQU1DLGNBQWM7QUFDbkJDLE9BQUlQLFVBRGU7QUFFbkJRLFlBQVNOLFFBRlU7QUFHbkJPLFNBQU1iLGVBQWVjLElBQWY7QUFIYSxHQUFwQjs7QUFNQTtBQUNBN0IsU0FDRXVCLElBREYsQ0FDTyxzQkFEUCxFQUVFTyxJQUZGLENBRU9DLDJCQUEyQk4sV0FBM0IsQ0FGUDs7QUFJQTtBQUNBekIsU0FBT2dDLEtBQVAsQ0FBYSxNQUFiOztBQUVBO0FBQ0FWLGlCQUNFVyxHQURGLENBQ00sT0FETixFQUVFQyxFQUZGLENBRUssT0FGTCxFQUVjO0FBQUEsVUFBTUMsc0JBQXNCVixZQUFZQyxFQUFsQyxFQUFzQ0QsWUFBWUUsT0FBbEQsRUFBMkRkLEdBQTNELENBQU47QUFBQSxHQUZkO0FBR0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTc0IscUJBQVQsQ0FBK0JDLFNBQS9CLEVBQTBDVCxPQUExQyxFQUFtRGQsR0FBbkQsRUFBd0Q7QUFDdkQ7QUFDQSxNQUFNd0IsaUJBQWlCO0FBQ3RCQyxTQUFNLE1BRGdCO0FBRXRCekMsU0FBTSxFQUFDNkIsSUFBSUMsT0FBTCxFQUZnQjtBQUd0QmQ7QUFIc0IsR0FBdkI7O0FBTUE7QUFDQWQsSUFBRXdDLElBQUYsQ0FBT0YsY0FBUCxFQUNFRyxJQURGLENBQ087QUFBQSxVQUFZQyw2QkFBNkJDLFFBQTdCLEVBQXVDTixTQUF2QyxDQUFaO0FBQUEsR0FEUCxFQUVFTyxNQUZGLENBRVM7QUFBQSxVQUFNM0MsT0FBT2dDLEtBQVAsQ0FBYSxNQUFiLENBQU47QUFBQSxHQUZUO0FBR0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNTLDRCQUFULENBQXNDQyxRQUF0QyxFQUFnRE4sU0FBaEQsRUFBMkQ7QUFDMUQ7QUFDQSxNQUFNUSxhQUFhckMsSUFBSUMsSUFBSixDQUFTcUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLDRCQUF4QixFQUFzRCxpQkFBdEQsQ0FBbkI7QUFDQSxNQUFNQyxlQUFleEMsSUFBSUMsSUFBSixDQUFTcUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLDJCQUF4QixFQUFxRCxpQkFBckQsQ0FBckI7O0FBRUE7QUFDQSxNQUFNRSxrQkFBa0IvQyxrQkFBa0JzQixJQUFsQixDQUF1Qiw0Q0FBdUNhLFNBQXZDLFFBQXZCLENBQXhCOztBQUVBO0FBQ0EsTUFBTWEscUJBQXFCRCxnQkFBZ0IvQixPQUFoQixDQUF3Qiw0QkFBeEIsQ0FBM0I7O0FBRUE7QUFDQSxNQUFNaUMsaUJBQWlCRixnQkFBZ0IvQixPQUFoQixDQUF3Qiw0QkFBeEIsRUFBc0RDLFFBQXRELEVBQXZCOztBQUVBO0FBQ0EsTUFBTWlDLFFBQVFGLG1CQUFtQmhDLE9BQW5CLENBQTJCLGtDQUEzQixDQUFkOztBQUVBO0FBQ0EsTUFBTW1DLHFCQUFxQnJELEVBQUUsYUFBRixDQUEzQjs7QUFFQTtBQUNBLE1BQUkyQyxTQUFTVyxRQUFULENBQWtCLFNBQWxCLENBQUosRUFBa0M7QUFDakM7QUFDQUosc0JBQW1CekIsTUFBbkI7O0FBRUE7QUFDQWpCLE9BQUkrQyxJQUFKLENBQVNDLFFBQVQsQ0FBa0JDLGlCQUFsQjs7QUFFQTtBQUNBLE9BQUtOLGVBQWVPLE1BQWhCLEdBQTBCLENBQTlCLEVBQWlDO0FBQ2hDTixVQUNFTyxLQURGLEdBRUVDLE1BRkYsQ0FFU1AsbUJBQW1CUSxLQUFuQixHQUEyQjlCLElBQTNCLEVBRlQ7QUFHQTtBQUNELEdBYkQsTUFhTztBQUNOO0FBQ0F2QixPQUFJK0MsSUFBSixDQUFTdEIsS0FBVCxDQUFlNkIsV0FBZixDQUEyQmpCLFVBQTNCLEVBQXVDRyxZQUF2QztBQUNBO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OztBQVNBLFVBQVNoQiwwQkFBVCxDQUFvQ2xDLElBQXBDLEVBQTBDO0FBQ3pDO0FBQ0EsTUFBTWlFLG1CQUFtQnZELElBQUlDLElBQUosQ0FBU3FDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixZQUF4QixFQUFzQyxpQkFBdEMsQ0FBekI7QUFDQSxNQUFNaUIsZUFBZXhELElBQUlDLElBQUosQ0FBU3FDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixZQUF4QixFQUFzQyxpQkFBdEMsQ0FBckI7O0FBRUE7QUFDQSx3RkFFNkJnQixnQkFGN0Isb0RBRzJCakUsS0FBSytCLElBSGhDLDRHQU82Qm1DLFlBUDdCLG9EQVEyQmxFLEtBQUs4QixPQVJoQztBQVdBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQWhDLFFBQU9xRSxJQUFQLEdBQWMsZ0JBQVE7QUFDckI7QUFDQWxFLFFBQU1vQyxFQUFOLENBQVMsT0FBVCxFQUFrQmhDLGdCQUFnQkUsSUFBbEMsRUFBd0MsVUFBU1EsS0FBVCxFQUFnQjtBQUN2REQsa0JBQWVDLEtBQWYsRUFBc0JOLEtBQUtGLElBQTNCO0FBQ0EsR0FGRDs7QUFJQU4sUUFBTW9DLEVBQU4sQ0FBUyxPQUFULEVBQWtCaEMsZ0JBQWdCRyxPQUFsQyxFQUEyQyxVQUFTTyxLQUFULEVBQWdCO0FBQzFERCxrQkFBZUMsS0FBZixFQUFzQk4sS0FBS0QsT0FBM0I7QUFDQSxHQUZEOztBQUlBO0FBQ0FtQztBQUNBLEVBWkQ7O0FBY0EsUUFBTzdDLE1BQVA7QUFDQSxDQXJPRiIsImZpbGUiOiJjb250ZW50X21hbmFnZXIvb3ZlcnZpZXcvY29udGVudF9tYW5hZ2VyX2RlbGV0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gY29udGVudF9tYW5hZ2VyX2RlbGV0ZS5qcyAyMDE3LTA5LTA3XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBDb250ZW50IE1hbmFnZXIgT3ZlcnZpZXcgRGVsZXRlXG4gKlxuICogQ29udHJvbGxlciBNb2R1bGUgdG8gZGVsZXRlIENvbnRlbnQgTWFuZ2VyIEVudHJpZXMgYW5kIHByb2R1Y3QgY29udGVudHMuXG4gKlxuICogSGFuZGxlcyB0aGUgZGVsZXRlIG9wZXJhdGlvbiBvZiB0aGUgQ29udGVudCBNYW5hZ2VyIG92ZXJ2aWV3IHBhZ2VzLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdjb250ZW50X21hbmFnZXJfZGVsZXRlJyxcblx0XG5cdFtcblx0XHQnbW9kYWwnLFxuXHRcdGAke2d4LnNvdXJjZX0vbGlicy9pbmZvX2JveGBcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ29udGVudCBNYW5hZ2VyIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5kZWxldGUtY29udGVudC5tb2RhbCcpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENvbnRlbnQgTWFuYWdlciBlbGVtZW50cyBjb250YWluZXJcblx0XHQgKiBcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRlbGVtZW50Q29udGFpbmVyID0gJCgnLmVsZW1lbnQtY29udGFpbmVyJyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2VsZWN0b3JzIGZvciBkZWxldGluZyBkaWZmZXJlbnQgdHlwZXMgb2YgY29udGVudHNcblx0XHQgKiBcblx0XHQgKiBAdHlwZSB7e2Jhc2ljOiBzdHJpbmcsIHBhZ2U6IHN0cmluZywgZWxlbWVudDogc3RyaW5nfX1cblx0XHQgKi9cblx0XHRjb25zdCBkZWxldGVTZWxlY3RvcnMgPSB7XG5cdFx0XHRiYXNpYzogJy5kZWxldGUtY29udGVudCcsXG5cdFx0XHRwYWdlOiAnLmRlbGV0ZS1jb250ZW50LnBhZ2UnLFxuXHRcdFx0ZWxlbWVudDogJy5kZWxldGUtY29udGVudC5lbGVtZW50Jyxcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVSTHMgZm9yIGRlbGV0aW5nIGRpZmZlcmVudCB0eXBlcyBvZiBjb250ZW50XG5cdFx0ICogXG5cdFx0ICogQHR5cGUge3twYWdlOiBzdHJpbmcsIGVsZW1lbnQ6IHN0cmluZ319XG5cdFx0ICovXG5cdFx0Y29uc3QgdXJscyA9IHtcblx0XHRcdHBhZ2U6IGAke2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpfS9hZG1pbi9hZG1pbi5waHA/ZG89Q29udGVudE1hbmFnZXJQYWdlc0FqYXgvZGVsZXRlYCxcblx0XHRcdGVsZW1lbnQ6IGAke2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpfS9hZG1pbi9hZG1pbi5waHA/ZG89Q29udGVudE1hbmFnZXJFbGVtZW50c0FqYXgvZGVsZXRlYCxcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBkZWxldGUgY2xpY2sgZXZlbnQgYnkgb3BlbmluZyB0aGUgZGVsZXRlIGNvbmZpcm1hdGlvbiBtb2RhbC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZXZlbnQuXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IHVybCBVUkwgZm9yIEFqYXhSZXF1ZXN0LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkRlbGV0ZUNsaWNrKGV2ZW50LCB1cmwpIHtcblx0XHRcdC8vIFByZXZlbnQgZGVmYXVsdCBhY3Rpb24uXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHQvLyBDbGlja2VkIGNvbnRlbnQgZW50cnlcblx0XHRcdGNvbnN0ICRjb250ZW50X2VudHJ5ID0gJChldmVudC50YXJnZXQpLmNsb3Nlc3QoJy5jb250ZW50LWFjdGlvbi1pY29ucycpLnNpYmxpbmdzKCcuY29udGVudC1uYW1lJyk7XG5cdFx0XHRcblx0XHRcdC8vIElkIG9mIHRoZSBjb250ZW50IHRoYXQgc2hvdWxkIGJlIGRlbGV0ZWRcblx0XHRcdGNvbnN0IGNvbnRlbnRfaWQgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cyhkZWxldGVTZWxlY3RvcnMuYmFzaWMpLmRhdGEoJ2NvbnRlbnRJZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBHcm91cCBJZCBvZiB0aGUgY29udGVudCB0aGF0IHNob3VsZCBiZSBkZWxldGVkXG5cdFx0XHRjb25zdCBncm91cF9pZCA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKGRlbGV0ZVNlbGVjdG9ycy5iYXNpYykuZGF0YSgnZ3JvdXBJZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBEZWxldGUgY29uZmlybWF0aW9uIG1vZGFsIGJ1dHRvbi5cblx0XHRcdGNvbnN0ICRjb25maXJtQnV0dG9uID0gJG1vZGFsLmZpbmQoJ2J1dHRvbi5jb25maXJtJyk7XG5cdFx0XHRcblx0XHRcdC8vIEVtcHR5IG1vZGFsIGJvZHkuXG5cdFx0XHQkbW9kYWwuZmluZCgnLm1vZGFsLWJvZHkgLmZvcm0tZ3JvdXAnKS5yZW1vdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2xpZGVyIGRhdGEuXG5cdFx0XHRjb25zdCBjb250ZW50RGF0YSA9IHtcblx0XHRcdFx0aWQ6IGNvbnRlbnRfaWQsXG5cdFx0XHRcdGdyb3VwSWQ6IGdyb3VwX2lkLFxuXHRcdFx0XHRuYW1lOiAkY29udGVudF9lbnRyeS50ZXh0KClcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIFB1dCBuZXcgc2xpZGVyIGluZm9ybWF0aW9uIGludG8gbW9kYWwgYm9keS5cblx0XHRcdCRtb2RhbFxuXHRcdFx0XHQuZmluZCgnLm1vZGFsLWJvZHkgZmllbGRzZXQnKVxuXHRcdFx0XHQuaHRtbChfZ2VuZXJhdGVDb250ZW50SW5mb01hcmt1cChjb250ZW50RGF0YSkpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0JG1vZGFsLm1vZGFsKCdzaG93Jyk7XG5cdFx0XHRcblx0XHRcdC8vIEhhbmRsZSBkZWxldGUgY29uZmlybWF0aW9uIG1vZGFsIGJ1dHRvbiBjbGljayBldmVudC5cblx0XHRcdCRjb25maXJtQnV0dG9uXG5cdFx0XHRcdC5vZmYoJ2NsaWNrJylcblx0XHRcdFx0Lm9uKCdjbGljaycsICgpID0+IF9vbkNvbmZpcm1CdXR0b25DbGljayhjb250ZW50RGF0YS5pZCwgY29udGVudERhdGEuZ3JvdXBJZCwgdXJsKSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIGRlbGV0ZSBjb25maXJtYXRpb24gYnV0dG9uIGNsaWNrIGV2ZW50IGJ5IHJlbW92aW5nIHRoZSBjb250ZW50IHRocm91Z2ggYW4gQUpBWCByZXF1ZXN0LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtOdW1iZXJ9IGNvbnRlbnRJZCBDb250ZW50IElELlxuXHRcdCAqIEBwYXJhbSB7TnVtYmVyfSBncm91cElkIEdyb3VwIElELlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSB1cmwgVVJMIGZvciBBamF4UmVxdWVzdC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25Db25maXJtQnV0dG9uQ2xpY2soY29udGVudElkLCBncm91cElkLCB1cmwpIHtcblx0XHRcdC8vIEFKQVggcmVxdWVzdCBvcHRpb25zLlxuXHRcdFx0Y29uc3QgcmVxdWVzdE9wdGlvbnMgPSB7XG5cdFx0XHRcdHR5cGU6ICdQT1NUJyxcblx0XHRcdFx0ZGF0YToge2lkOiBncm91cElkfSxcblx0XHRcdFx0dXJsXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBQZXJmb3JtIHJlcXVlc3QuXG5cdFx0XHQkLmFqYXgocmVxdWVzdE9wdGlvbnMpXG5cdFx0XHRcdC5kb25lKHJlc3BvbnNlID0+IF9oYW5kbGVEZWxldGVSZXF1ZXN0UmVzcG9uc2UocmVzcG9uc2UsIGNvbnRlbnRJZCkpXG5cdFx0XHRcdC5hbHdheXMoKCkgPT4gJG1vZGFsLm1vZGFsKCdoaWRlJykpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIGNvbnRlbnQgZGVsZXRpb24gQUpBWCBhY3Rpb24gc2VydmVyIHJlc3BvbnNlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHJlc3BvbnNlIFNlcnZlciByZXNwb25zZS5cblx0XHQgKiBAcGFyYW0ge051bWJlcn0gY29udGVudElkIElEIG9mIGRlbGV0ZWQgY29udGVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfaGFuZGxlRGVsZXRlUmVxdWVzdFJlc3BvbnNlKHJlc3BvbnNlLCBjb250ZW50SWQpIHtcblx0XHRcdC8vIEVycm9yIG1lc3NhZ2UgcGhyYXNlcy5cblx0XHRcdGNvbnN0IGVycm9yVGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnREVMRVRFX0NPTlRFTlRfRVJST1JfVElUTEUnLCAnY29udGVudF9tYW5hZ2VyJyk7XG5cdFx0XHRjb25zdCBlcnJvck1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnREVMRVRFX0NPTlRFTlRfRVJST1JfVEVYVCcsICdjb250ZW50X21hbmFnZXInKTtcblx0XHRcdFxuXHRcdFx0Ly8gRWxlbWVudCB0aGF0IHRyaWdnZXJzIHRoZSBkZWxldGUgYWN0aW9uXG5cdFx0XHRjb25zdCAkZGVsZXRlX3RyaWdnZXIgPSAkZWxlbWVudENvbnRhaW5lci5maW5kKCcuZGVsZXRlLWNvbnRlbnQnK2BbZGF0YS1jb250ZW50LWlkPVwiJHtjb250ZW50SWR9XCJdYCk7XG5cdFx0XHRcblx0XHRcdC8vIExpc3QgZWxlbWVudCB0aGF0IHNob3VsZCBiZSByZW1vdmVkXG5cdFx0XHRjb25zdCAkZWxlbWVudF90b19kZWxldGUgPSAkZGVsZXRlX3RyaWdnZXIuY2xvc2VzdCgnbGkuY29udGVudC1tYW5hZ2VyLWVsZW1lbnQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gTGlzdCBvZiBhbGwgc2libGluZ3Mgb2YgdGhlIHJlbW92ZWQgZWxlbWVudFxuXHRcdFx0Y29uc3QgJGxpc3RfZWxlbWVudHMgPSAkZGVsZXRlX3RyaWdnZXIuY2xvc2VzdCgnbGkuY29udGVudC1tYW5hZ2VyLWVsZW1lbnQnKS5zaWJsaW5ncygpO1xuXHRcdFx0XG5cdFx0XHQvLyBFbGVtZW50IHRoYXQgY29udGFpbnMgYWxsIGNvbnRlbnQgbGlzdCBpdGVtc1xuXHRcdFx0Y29uc3QgJGxpc3QgPSAkZWxlbWVudF90b19kZWxldGUuY2xvc2VzdCgndWwuY29udGVudC1tYW5hZ2VyLWVsZW1lbnRzLWxpc3QnKVxuXHRcdFx0XG5cdFx0XHQvLyAnTm8gcmVzdWx0cycgbWVzc2FnZSBsaXN0IGVsZW1lbnQgdGVtcGxhdGUuXG5cdFx0XHRjb25zdCAkZW1wdHlMaXN0VGVtcGxhdGUgPSAkKCcjZW1wdHktbGlzdCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBDaGVjayBmb3IgYWN0aW9uIHN1Y2Nlc3MuXG5cdFx0XHRpZiAocmVzcG9uc2UuaW5jbHVkZXMoJ3N1Y2Nlc3MnKSkge1xuXHRcdFx0XHQvLyBEZWxldGUgcmVzcGVjdGl2ZSB0YWJsZSByb3dzLlxuXHRcdFx0XHQkZWxlbWVudF90b19kZWxldGUucmVtb3ZlKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBZGQgc3VjY2VzcyBtZXNzYWdlIHRvIGFkbWluIGluZm8gYm94LlxuXHRcdFx0XHRqc2UubGlicy5pbmZvX2JveC5hZGRTdWNjZXNzTWVzc2FnZSgpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gSWYgdGhlcmUgYXJlIG5vIHJvd3MsIHNob3cgJ05vIHJlc3VsdHMnIG1lc3NhZ2Ugcm93LlxuXHRcdFx0XHRpZiAoKCRsaXN0X2VsZW1lbnRzLmxlbmd0aCkgPCAxKSB7XG5cdFx0XHRcdFx0JGxpc3Rcblx0XHRcdFx0XHRcdC5lbXB0eSgpXG5cdFx0XHRcdFx0XHQuYXBwZW5kKCRlbXB0eUxpc3RUZW1wbGF0ZS5jbG9uZSgpLmh0bWwoKSk7XG5cdFx0XHRcdH1cblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdC8vIFNob3cgZXJyb3IgbWVzc2FnZSBtb2RhbC5cblx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoZXJyb3JUaXRsZSwgZXJyb3JNZXNzYWdlKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2VuZXJhdGVzIEhUTUwgY29udGFpbmluZyB0aGUgY29udGVudCBlbnRyeSBpbmZvcm1hdGlvbiBmb3IgdGhlIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YSBDb250ZW50IE1hbmFnZXIgZGF0YS5cblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YS5uYW1lIE5hbWUgb2YgdGhlIENvbnRlbnQgTWFuYWdlciBlbnRyeS5cblx0XHQgKiBAcGFyYW0ge051bWJlcn0gZGF0YS5ncm91cElkIElkIG9mIHRoZSBDb250ZW50IE1hbmFnZXIgZW50cnkuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtTdHJpbmd9IENyZWF0ZWQgSFRNTCBzdHJpbmcuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dlbmVyYXRlQ29udGVudEluZm9NYXJrdXAoZGF0YSkge1xuXHRcdFx0Ly8gTGFiZWwgcGhyYXNlcy5cblx0XHRcdGNvbnN0IGNvbnRlbnROYW1lTGFiZWwgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9USVRMRScsICdjb250ZW50X21hbmFnZXInKTtcblx0XHRcdGNvbnN0IGdyb3VwSWRMYWJlbCA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdURVhUX0dST1VQJywgJ2NvbnRlbnRfbWFuYWdlcicpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZXR1cm4gbWFya3VwLlxuXHRcdFx0cmV0dXJuIGBcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHRcdFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTVcIj4ke2NvbnRlbnROYW1lTGFiZWx9PC9sYWJlbD5cblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtbWQtN1wiPiR7ZGF0YS5uYW1lfTwvZGl2PlxuXHRcdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC01XCI+JHtncm91cElkTGFiZWx9PC9sYWJlbD5cblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtbWQtN1wiPiR7ZGF0YS5ncm91cElkfTwvZGl2PlxuXHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0YDtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdC8vIExpc3RlbiB0byBjbGljayBldmVudCBvbiBkZWxldGUgaWNvblxuXHRcdFx0JHRoaXMub24oJ2NsaWNrJywgZGVsZXRlU2VsZWN0b3JzLnBhZ2UsIGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcdF9vbkRlbGV0ZUNsaWNrKGV2ZW50LCB1cmxzLnBhZ2UpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdjbGljaycsIGRlbGV0ZVNlbGVjdG9ycy5lbGVtZW50LCBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0XHRfb25EZWxldGVDbGljayhldmVudCwgdXJscy5lbGVtZW50KTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBGaW5pc2ggaW5pdGlhbGl6YXRpb24uXG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9XG4pOyJdfQ==
