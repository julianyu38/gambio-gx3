'use strict';

/* --------------------------------------------------------------
 filter.js 2016-10-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles the QuickEdit table filtering.
 *
 * ### Methods
 *
 * **Reload Filtering Options**
 *
 * ```
 * // Reload the filter options with an AJAX request (optionally provide a second parameter for the AJAX URL).
 * $('.table-main').quick_edit_filter('reload');
 * ```
 */
gx.controllers.module('filter', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js', jse.source + '/vendor/sumoselect/jquery.sumoselect.min.js', jse.source + '/vendor/sumoselect/sumoselect.min.css'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Enter Key Code
  *
  * @type {Number}
  */

	var ENTER_KEY_CODE = 13;

	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	var $this = $(this);

	/**
  * Filter Row Selector
  *
  * @type {jQuery}
  */
	var $filter = $this.find('tr.filter');

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = { bindings: {} };

	// Dynamically define the filter row data-bindings. 
	$filter.find('th').each(function () {
		var columnName = $(this).data('columnName');

		if (columnName === 'checkbox' || columnName === 'actions') {
			return true;
		}

		module.bindings[columnName] = $(this).find('input, select').first();
	});

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Reload filter options with an AJAX request.
  *
  * This function implements the $('.datatable').quick_edit_filter('reload') which will reload the filtering
  * "multi_select" instances will new options. It must be used after some table data are changed and the filtering
  * options need to be updated.
  *
  * @param {String} url Optional, the URL to be used for fetching the options. Do not add the "pageToken"
  * parameter to URL, it will be appended in this method.
  */
	function _reload(url) {
		url = url || jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/FilterOptions';
		var data = { pageToken: jse.core.config.get('pageToken') };

		$.getJSON(url, data).done(function (response) {
			for (var column in response) {
				var $select = $filter.find('.SumoSelect > select.' + column);
				var currentValueBackup = $select.val(); // Will try to set it back if it still exists. 

				if (!$select.length) {
					return; // The select element was not found.
				}

				$select.empty();

				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = response[column][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var option = _step.value;

						$select.append(new Option(option.text, option.value));
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				if (currentValueBackup !== null) {
					$select.val(currentValueBackup);
				}

				$select.multi_select('refresh');
			}
		});
	}

	/**
  * Add public "quick_edit_filter" method to jQuery in order.
  */
	function _addPublicMethod() {
		if ($.fn.quick_edit_filter) {
			return;
		}

		$.fn.extend({
			quick_edit_filter: function quick_edit_filter(action) {
				for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
					args[_key - 1] = arguments[_key];
				}

				$.each(this, function () {
					switch (action) {
						case 'reload':
							_reload.apply(this, args);
							break;
					}
				});
			}
		});
	}

	/**
  * On Filter Button Click
  *
  * Apply the provided filters and update the table rows.
  */
	function _onApplyFiltersClick() {
		// Prepare the object with the final filtering data.
		var filter = {};

		$filter.find('th').each(function () {
			var columnName = $(this).data('columnName');

			if (columnName === 'checkbox' || columnName === 'actions') {
				return true;
			}

			var value = module.bindings[columnName].get();

			if (value) {
				filter[columnName] = value;
				$this.DataTable().column(columnName + ':name').search(value);
			} else {
				$this.DataTable().column(columnName + ':name').search('');
			}
		});

		$this.trigger('quick_edit_filter:change', [filter]);
		$this.DataTable().draw();
	}

	/**
  * On Reset Button Click
  *
  * Reset the filter form and reload the table data without filtering.
  */
	function _onResetFiltersClick() {
		// Remove values from the input boxes.
		$filter.find('input, select').not('.length, .select-page-mode').val('');
		$filter.find('select').not('.length, .select-page-mode').multi_select('refresh');

		// Reset the filtering values.
		$this.DataTable().columns().search('').draw();

		// Trigger Event
		$this.trigger('quick_edit_filter:change', [{}]);
	}

	/**
  * Apply the filters when the user presses the Enter key.
  *
  * @param {jQuery.Event} event
  */
	function _onInputTextKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$filter.find('.apply-filters').trigger('click');
		}
	}

	/**
  * Parse the initial filtering parameters and apply them to the table.
  */
	function _parseFilteringParameters() {
		var _$$deparam = $.deparam(window.location.search.slice(1)),
		    filter = _$$deparam.filter;

		for (var name in filter) {
			var value = filter[name];

			if (module.bindings[name]) {
				module.bindings[name].set(value);
			}
		}
	}

	/**
  * Normalize array filtering values.
  *
  * By default datatables will concatenate array search values into a string separated with "," commas. This
  * is not acceptable though because some filtering elements may contain values with comma and thus the array
  * cannot be parsed from backend. This method will reset those cases back to arrays for a clearer transaction
  * with the backend.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {DataTables.Settings} settings DataTables settings object.
  * @param {Object} data Data that will be sent to the server in an object form.
  */
	function _normalizeArrayValues(event, settings, data) {
		var filter = {};

		for (var name in module.bindings) {
			var value = module.bindings[name].get();

			if (value && value.constructor === Array) {
				filter[name] = value;
			}
		}

		for (var entry in filter) {
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = data.columns[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var column = _step2.value;

					if (entry === column.name && filter[entry].constructor === Array) {
						column.search.value = filter[entry];
						break;
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Add public module method. 
		_addPublicMethod();

		// Parse filtering GET parameters. 
		_parseFilteringParameters();

		// Bind event handlers.
		$filter.on('keyup', 'input:text', _onInputTextKeyUp).on('click', '.apply-filters', _onApplyFiltersClick).on('click', '.reset-filters', _onResetFiltersClick);

		$('[data-multi_select-categoryinstance]').SumoSelect({
			placeholder: jse.core.lang.translate('SELECT', 'general'),
			csvDispCount: 2,
			captionFormat: '{0} ' + jse.core.lang.translate('selected', 'admin_labels'),
			locale: ['OK', jse.core.lang.translate('CANCEL', 'general'), jse.core.lang.translate('SELECT_ALL', 'general')],
			search: true,
			searchText: jse.core.lang.translate('SEARCH', 'admin_quick_edit') + ' ...',
			noMatch: jse.core.lang.translate('NO_RESULTS_FOR', 'admin_quick_edit') + ' "{0}"'
		});

		$this.on('preXhr.dt', _normalizeArrayValues);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvb3ZlcnZpZXcvZmlsdGVyLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiRU5URVJfS0VZX0NPREUiLCIkdGhpcyIsIiQiLCIkZmlsdGVyIiwiZmluZCIsImJpbmRpbmdzIiwiZWFjaCIsImNvbHVtbk5hbWUiLCJmaXJzdCIsIl9yZWxvYWQiLCJ1cmwiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwicGFnZVRva2VuIiwiZ2V0SlNPTiIsImRvbmUiLCJyZXNwb25zZSIsImNvbHVtbiIsIiRzZWxlY3QiLCJjdXJyZW50VmFsdWVCYWNrdXAiLCJ2YWwiLCJsZW5ndGgiLCJlbXB0eSIsIm9wdGlvbiIsImFwcGVuZCIsIk9wdGlvbiIsInRleHQiLCJ2YWx1ZSIsIm11bHRpX3NlbGVjdCIsIl9hZGRQdWJsaWNNZXRob2QiLCJmbiIsInF1aWNrX2VkaXRfZmlsdGVyIiwiZXh0ZW5kIiwiYWN0aW9uIiwiYXJncyIsImFwcGx5IiwiX29uQXBwbHlGaWx0ZXJzQ2xpY2siLCJmaWx0ZXIiLCJEYXRhVGFibGUiLCJzZWFyY2giLCJ0cmlnZ2VyIiwiZHJhdyIsIl9vblJlc2V0RmlsdGVyc0NsaWNrIiwibm90IiwiY29sdW1ucyIsIl9vbklucHV0VGV4dEtleVVwIiwiZXZlbnQiLCJ3aGljaCIsIl9wYXJzZUZpbHRlcmluZ1BhcmFtZXRlcnMiLCJkZXBhcmFtIiwid2luZG93IiwibG9jYXRpb24iLCJzbGljZSIsIm5hbWUiLCJzZXQiLCJfbm9ybWFsaXplQXJyYXlWYWx1ZXMiLCJzZXR0aW5ncyIsImNvbnN0cnVjdG9yIiwiQXJyYXkiLCJlbnRyeSIsImluaXQiLCJvbiIsIlN1bW9TZWxlY3QiLCJwbGFjZWhvbGRlciIsImxhbmciLCJ0cmFuc2xhdGUiLCJjc3ZEaXNwQ291bnQiLCJjYXB0aW9uRm9ybWF0IiwibG9jYWxlIiwic2VhcmNoVGV4dCIsIm5vTWF0Y2giXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7O0FBWUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFFBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLG1EQUVJRCxJQUFJQyxNQUZSLGtEQUdJRCxJQUFJQyxNQUhSLDJDQUhELEVBU0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsaUJBQWlCLEVBQXZCOztBQUVBOzs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVVGLE1BQU1HLElBQU4sQ0FBVyxXQUFYLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1SLFNBQVMsRUFBQ1MsVUFBVSxFQUFYLEVBQWY7O0FBRUE7QUFDQUYsU0FBUUMsSUFBUixDQUFhLElBQWIsRUFBbUJFLElBQW5CLENBQXdCLFlBQVc7QUFDbEMsTUFBTUMsYUFBYUwsRUFBRSxJQUFGLEVBQVFILElBQVIsQ0FBYSxZQUFiLENBQW5COztBQUVBLE1BQUlRLGVBQWUsVUFBZixJQUE2QkEsZUFBZSxTQUFoRCxFQUEyRDtBQUMxRCxVQUFPLElBQVA7QUFDQTs7QUFFRFgsU0FBT1MsUUFBUCxDQUFnQkUsVUFBaEIsSUFBOEJMLEVBQUUsSUFBRixFQUFRRSxJQUFSLENBQWEsZUFBYixFQUE4QkksS0FBOUIsRUFBOUI7QUFDQSxFQVJEOztBQVVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQVVBLFVBQVNDLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQ3JCQSxRQUFNQSxPQUFPYixJQUFJYyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHlEQUE3QztBQUNBLE1BQU1kLE9BQU8sRUFBQ2UsV0FBV2pCLElBQUljLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FBWixFQUFiOztBQUVBWCxJQUFFYSxPQUFGLENBQVVMLEdBQVYsRUFBZVgsSUFBZixFQUFxQmlCLElBQXJCLENBQTBCLFVBQUNDLFFBQUQsRUFBYztBQUN2QyxRQUFLLElBQUlDLE1BQVQsSUFBbUJELFFBQW5CLEVBQTZCO0FBQzVCLFFBQU1FLFVBQVVoQixRQUFRQyxJQUFSLENBQWEsMEJBQTBCYyxNQUF2QyxDQUFoQjtBQUNBLFFBQU1FLHFCQUFxQkQsUUFBUUUsR0FBUixFQUEzQixDQUY0QixDQUVjOztBQUUxQyxRQUFJLENBQUNGLFFBQVFHLE1BQWIsRUFBcUI7QUFDcEIsWUFEb0IsQ0FDWjtBQUNSOztBQUVESCxZQUFRSSxLQUFSOztBQVI0QjtBQUFBO0FBQUE7O0FBQUE7QUFVNUIsMEJBQW1CTixTQUFTQyxNQUFULENBQW5CLDhIQUFxQztBQUFBLFVBQTVCTSxNQUE0Qjs7QUFDcENMLGNBQVFNLE1BQVIsQ0FBZSxJQUFJQyxNQUFKLENBQVdGLE9BQU9HLElBQWxCLEVBQXdCSCxPQUFPSSxLQUEvQixDQUFmO0FBQ0E7QUFaMkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFjNUIsUUFBSVIsdUJBQXVCLElBQTNCLEVBQWlDO0FBQ2hDRCxhQUFRRSxHQUFSLENBQVlELGtCQUFaO0FBQ0E7O0FBRURELFlBQVFVLFlBQVIsQ0FBcUIsU0FBckI7QUFDQTtBQUNELEdBckJEO0FBc0JBOztBQUVEOzs7QUFHQSxVQUFTQyxnQkFBVCxHQUE0QjtBQUMzQixNQUFJNUIsRUFBRTZCLEVBQUYsQ0FBS0MsaUJBQVQsRUFBNEI7QUFDM0I7QUFDQTs7QUFFRDlCLElBQUU2QixFQUFGLENBQUtFLE1BQUwsQ0FBWTtBQUNYRCxzQkFBbUIsMkJBQVNFLE1BQVQsRUFBMEI7QUFBQSxzQ0FBTkMsSUFBTTtBQUFOQSxTQUFNO0FBQUE7O0FBQzVDakMsTUFBRUksSUFBRixDQUFPLElBQVAsRUFBYSxZQUFXO0FBQ3ZCLGFBQVE0QixNQUFSO0FBQ0MsV0FBSyxRQUFMO0FBQ0N6QixlQUFRMkIsS0FBUixDQUFjLElBQWQsRUFBb0JELElBQXBCO0FBQ0E7QUFIRjtBQUtBLEtBTkQ7QUFPQTtBQVRVLEdBQVo7QUFXQTs7QUFFRDs7Ozs7QUFLQSxVQUFTRSxvQkFBVCxHQUFnQztBQUMvQjtBQUNBLE1BQU1DLFNBQVMsRUFBZjs7QUFFQW5DLFVBQVFDLElBQVIsQ0FBYSxJQUFiLEVBQW1CRSxJQUFuQixDQUF3QixZQUFXO0FBQ2xDLE9BQU1DLGFBQWFMLEVBQUUsSUFBRixFQUFRSCxJQUFSLENBQWEsWUFBYixDQUFuQjs7QUFFQSxPQUFJUSxlQUFlLFVBQWYsSUFBNkJBLGVBQWUsU0FBaEQsRUFBMkQ7QUFDMUQsV0FBTyxJQUFQO0FBQ0E7O0FBRUQsT0FBSXFCLFFBQVFoQyxPQUFPUyxRQUFQLENBQWdCRSxVQUFoQixFQUE0Qk0sR0FBNUIsRUFBWjs7QUFFQSxPQUFJZSxLQUFKLEVBQVc7QUFDVlUsV0FBTy9CLFVBQVAsSUFBcUJxQixLQUFyQjtBQUNBM0IsVUFBTXNDLFNBQU4sR0FBa0JyQixNQUFsQixDQUE0QlgsVUFBNUIsWUFBK0NpQyxNQUEvQyxDQUFzRFosS0FBdEQ7QUFDQSxJQUhELE1BR087QUFDTjNCLFVBQU1zQyxTQUFOLEdBQWtCckIsTUFBbEIsQ0FBNEJYLFVBQTVCLFlBQStDaUMsTUFBL0MsQ0FBc0QsRUFBdEQ7QUFDQTtBQUNELEdBZkQ7O0FBaUJBdkMsUUFBTXdDLE9BQU4sQ0FBYywwQkFBZCxFQUEwQyxDQUFDSCxNQUFELENBQTFDO0FBQ0FyQyxRQUFNc0MsU0FBTixHQUFrQkcsSUFBbEI7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxvQkFBVCxHQUFnQztBQUMvQjtBQUNBeEMsVUFBUUMsSUFBUixDQUFhLGVBQWIsRUFBOEJ3QyxHQUE5QixDQUFrQyw0QkFBbEMsRUFBZ0V2QixHQUFoRSxDQUFvRSxFQUFwRTtBQUNBbEIsVUFBUUMsSUFBUixDQUFhLFFBQWIsRUFBdUJ3QyxHQUF2QixDQUEyQiw0QkFBM0IsRUFBeURmLFlBQXpELENBQXNFLFNBQXRFOztBQUVBO0FBQ0E1QixRQUFNc0MsU0FBTixHQUFrQk0sT0FBbEIsR0FBNEJMLE1BQTVCLENBQW1DLEVBQW5DLEVBQXVDRSxJQUF2Qzs7QUFFQTtBQUNBekMsUUFBTXdDLE9BQU4sQ0FBYywwQkFBZCxFQUEwQyxDQUFDLEVBQUQsQ0FBMUM7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTSyxpQkFBVCxDQUEyQkMsS0FBM0IsRUFBa0M7QUFDakMsTUFBSUEsTUFBTUMsS0FBTixLQUFnQmhELGNBQXBCLEVBQW9DO0FBQ25DRyxXQUFRQyxJQUFSLENBQWEsZ0JBQWIsRUFBK0JxQyxPQUEvQixDQUF1QyxPQUF2QztBQUNBO0FBQ0Q7O0FBRUQ7OztBQUdBLFVBQVNRLHlCQUFULEdBQXFDO0FBQUEsbUJBQ25CL0MsRUFBRWdELE9BQUYsQ0FBVUMsT0FBT0MsUUFBUCxDQUFnQlosTUFBaEIsQ0FBdUJhLEtBQXZCLENBQTZCLENBQTdCLENBQVYsQ0FEbUI7QUFBQSxNQUM3QmYsTUFENkIsY0FDN0JBLE1BRDZCOztBQUdwQyxPQUFLLElBQUlnQixJQUFULElBQWlCaEIsTUFBakIsRUFBeUI7QUFDeEIsT0FBTVYsUUFBUVUsT0FBT2dCLElBQVAsQ0FBZDs7QUFFQSxPQUFJMUQsT0FBT1MsUUFBUCxDQUFnQmlELElBQWhCLENBQUosRUFBMkI7QUFDMUIxRCxXQUFPUyxRQUFQLENBQWdCaUQsSUFBaEIsRUFBc0JDLEdBQXRCLENBQTBCM0IsS0FBMUI7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7OztBQVlBLFVBQVM0QixxQkFBVCxDQUErQlQsS0FBL0IsRUFBc0NVLFFBQXRDLEVBQWdEMUQsSUFBaEQsRUFBc0Q7QUFDckQsTUFBTXVDLFNBQVMsRUFBZjs7QUFFQSxPQUFLLElBQUlnQixJQUFULElBQWlCMUQsT0FBT1MsUUFBeEIsRUFBa0M7QUFDakMsT0FBTXVCLFFBQVFoQyxPQUFPUyxRQUFQLENBQWdCaUQsSUFBaEIsRUFBc0J6QyxHQUF0QixFQUFkOztBQUVBLE9BQUllLFNBQVNBLE1BQU04QixXQUFOLEtBQXNCQyxLQUFuQyxFQUEwQztBQUN6Q3JCLFdBQU9nQixJQUFQLElBQWUxQixLQUFmO0FBQ0E7QUFDRDs7QUFFRCxPQUFLLElBQUlnQyxLQUFULElBQWtCdEIsTUFBbEIsRUFBMEI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFDekIsMEJBQW1CdkMsS0FBSzhDLE9BQXhCLG1JQUFpQztBQUFBLFNBQXhCM0IsTUFBd0I7O0FBQ2hDLFNBQUkwQyxVQUFVMUMsT0FBT29DLElBQWpCLElBQXlCaEIsT0FBT3NCLEtBQVAsRUFBY0YsV0FBZCxLQUE4QkMsS0FBM0QsRUFBa0U7QUFDakV6QyxhQUFPc0IsTUFBUCxDQUFjWixLQUFkLEdBQXNCVSxPQUFPc0IsS0FBUCxDQUF0QjtBQUNBO0FBQ0E7QUFDRDtBQU53QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT3pCO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBaEUsUUFBT2lFLElBQVAsR0FBYyxVQUFTN0MsSUFBVCxFQUFlO0FBQzVCO0FBQ0FjOztBQUVBO0FBQ0FtQjs7QUFFQTtBQUNBOUMsVUFDRTJELEVBREYsQ0FDSyxPQURMLEVBQ2MsWUFEZCxFQUM0QmhCLGlCQUQ1QixFQUVFZ0IsRUFGRixDQUVLLE9BRkwsRUFFYyxnQkFGZCxFQUVnQ3pCLG9CQUZoQyxFQUdFeUIsRUFIRixDQUdLLE9BSEwsRUFHYyxnQkFIZCxFQUdnQ25CLG9CQUhoQzs7QUFLQXpDLElBQUUsc0NBQUYsRUFBMEM2RCxVQUExQyxDQUFxRDtBQUNwREMsZ0JBQWFuRSxJQUFJYyxJQUFKLENBQVNzRCxJQUFULENBQWNDLFNBQWQsQ0FBd0IsUUFBeEIsRUFBa0MsU0FBbEMsQ0FEdUM7QUFFcERDLGlCQUFjLENBRnNDO0FBR3BEQywyQkFBc0J2RSxJQUFJYyxJQUFKLENBQVNzRCxJQUFULENBQWNDLFNBQWQsQ0FBd0IsVUFBeEIsRUFBb0MsY0FBcEMsQ0FIOEI7QUFJcERHLFdBQVEsQ0FDUCxJQURPLEVBRVB4RSxJQUFJYyxJQUFKLENBQVNzRCxJQUFULENBQWNDLFNBQWQsQ0FBd0IsUUFBeEIsRUFBa0MsU0FBbEMsQ0FGTyxFQUdQckUsSUFBSWMsSUFBSixDQUFTc0QsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFlBQXhCLEVBQXNDLFNBQXRDLENBSE8sQ0FKNEM7QUFTcEQxQixXQUFRLElBVDRDO0FBVXBEOEIsZUFBWXpFLElBQUljLElBQUosQ0FBU3NELElBQVQsQ0FBY0MsU0FBZCxDQUF3QixRQUF4QixFQUFrQyxrQkFBbEMsSUFBd0QsTUFWaEI7QUFXcERLLFlBQVMxRSxJQUFJYyxJQUFKLENBQVNzRCxJQUFULENBQWNDLFNBQWQsQ0FBd0IsZ0JBQXhCLEVBQTBDLGtCQUExQyxJQUFnRTtBQVhyQixHQUFyRDs7QUFjQWpFLFFBQU02RCxFQUFOLENBQVMsV0FBVCxFQUFzQk4scUJBQXRCOztBQUVBeEM7QUFDQSxFQTlCRDs7QUFnQ0EsUUFBT3BCLE1BQVA7QUFDQSxDQXRRRiIsImZpbGUiOiJxdWlja19lZGl0L292ZXJ2aWV3L2ZpbHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZmlsdGVyLmpzIDIwMTYtMTAtMTlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIEhhbmRsZXMgdGhlIFF1aWNrRWRpdCB0YWJsZSBmaWx0ZXJpbmcuXG4gKlxuICogIyMjIE1ldGhvZHNcbiAqXG4gKiAqKlJlbG9hZCBGaWx0ZXJpbmcgT3B0aW9ucyoqXG4gKlxuICogYGBgXG4gKiAvLyBSZWxvYWQgdGhlIGZpbHRlciBvcHRpb25zIHdpdGggYW4gQUpBWCByZXF1ZXN0IChvcHRpb25hbGx5IHByb3ZpZGUgYSBzZWNvbmQgcGFyYW1ldGVyIGZvciB0aGUgQUpBWCBVUkwpLlxuICogJCgnLnRhYmxlLW1haW4nKS5xdWlja19lZGl0X2ZpbHRlcigncmVsb2FkJyk7XG4gKiBgYGBcbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnZmlsdGVyJyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LWRlcGFyYW0vanF1ZXJ5LWRlcGFyYW0ubWluLmpzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3Ivc3Vtb3NlbGVjdC9qcXVlcnkuc3Vtb3NlbGVjdC5taW4uanNgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9zdW1vc2VsZWN0L3N1bW9zZWxlY3QubWluLmNzc2Bcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRW50ZXIgS2V5IENvZGVcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtOdW1iZXJ9XG5cdFx0ICovXG5cdFx0Y29uc3QgRU5URVJfS0VZX0NPREUgPSAxMztcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbHRlciBSb3cgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJGZpbHRlciA9ICR0aGlzLmZpbmQoJ3RyLmZpbHRlcicpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7YmluZGluZ3M6IHt9fTtcblx0XHRcblx0XHQvLyBEeW5hbWljYWxseSBkZWZpbmUgdGhlIGZpbHRlciByb3cgZGF0YS1iaW5kaW5ncy4gXG5cdFx0JGZpbHRlci5maW5kKCd0aCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRjb25zdCBjb2x1bW5OYW1lID0gJCh0aGlzKS5kYXRhKCdjb2x1bW5OYW1lJyk7XG5cdFx0XHRcblx0XHRcdGlmIChjb2x1bW5OYW1lID09PSAnY2hlY2tib3gnIHx8IGNvbHVtbk5hbWUgPT09ICdhY3Rpb25zJykge1xuXHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0bW9kdWxlLmJpbmRpbmdzW2NvbHVtbk5hbWVdID0gJCh0aGlzKS5maW5kKCdpbnB1dCwgc2VsZWN0JykuZmlyc3QoKTtcblx0XHR9KTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZWxvYWQgZmlsdGVyIG9wdGlvbnMgd2l0aCBhbiBBSkFYIHJlcXVlc3QuXG5cdFx0ICpcblx0XHQgKiBUaGlzIGZ1bmN0aW9uIGltcGxlbWVudHMgdGhlICQoJy5kYXRhdGFibGUnKS5xdWlja19lZGl0X2ZpbHRlcigncmVsb2FkJykgd2hpY2ggd2lsbCByZWxvYWQgdGhlIGZpbHRlcmluZ1xuXHRcdCAqIFwibXVsdGlfc2VsZWN0XCIgaW5zdGFuY2VzIHdpbGwgbmV3IG9wdGlvbnMuIEl0IG11c3QgYmUgdXNlZCBhZnRlciBzb21lIHRhYmxlIGRhdGEgYXJlIGNoYW5nZWQgYW5kIHRoZSBmaWx0ZXJpbmdcblx0XHQgKiBvcHRpb25zIG5lZWQgdG8gYmUgdXBkYXRlZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSB1cmwgT3B0aW9uYWwsIHRoZSBVUkwgdG8gYmUgdXNlZCBmb3IgZmV0Y2hpbmcgdGhlIG9wdGlvbnMuIERvIG5vdCBhZGQgdGhlIFwicGFnZVRva2VuXCJcblx0XHQgKiBwYXJhbWV0ZXIgdG8gVVJMLCBpdCB3aWxsIGJlIGFwcGVuZGVkIGluIHRoaXMgbWV0aG9kLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9yZWxvYWQodXJsKSB7XG5cdFx0XHR1cmwgPSB1cmwgfHwganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1RdWlja0VkaXRPdmVydmlld0FqYXgvRmlsdGVyT3B0aW9ucyc7XG5cdFx0XHRjb25zdCBkYXRhID0ge3BhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJyl9O1xuXHRcdFx0XG5cdFx0XHQkLmdldEpTT04odXJsLCBkYXRhKS5kb25lKChyZXNwb25zZSkgPT4ge1xuXHRcdFx0XHRmb3IgKGxldCBjb2x1bW4gaW4gcmVzcG9uc2UpIHtcblx0XHRcdFx0XHRjb25zdCAkc2VsZWN0ID0gJGZpbHRlci5maW5kKCcuU3Vtb1NlbGVjdCA+IHNlbGVjdC4nICsgY29sdW1uKTtcblx0XHRcdFx0XHRjb25zdCBjdXJyZW50VmFsdWVCYWNrdXAgPSAkc2VsZWN0LnZhbCgpOyAvLyBXaWxsIHRyeSB0byBzZXQgaXQgYmFjayBpZiBpdCBzdGlsbCBleGlzdHMuIFxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmICghJHNlbGVjdC5sZW5ndGgpIHtcblx0XHRcdFx0XHRcdHJldHVybjsgLy8gVGhlIHNlbGVjdCBlbGVtZW50IHdhcyBub3QgZm91bmQuXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCRzZWxlY3QuZW1wdHkoKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRmb3IgKGxldCBvcHRpb24gb2YgcmVzcG9uc2VbY29sdW1uXSkge1xuXHRcdFx0XHRcdFx0JHNlbGVjdC5hcHBlbmQobmV3IE9wdGlvbihvcHRpb24udGV4dCwgb3B0aW9uLnZhbHVlKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmIChjdXJyZW50VmFsdWVCYWNrdXAgIT09IG51bGwpIHtcblx0XHRcdFx0XHRcdCRzZWxlY3QudmFsKGN1cnJlbnRWYWx1ZUJhY2t1cCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCRzZWxlY3QubXVsdGlfc2VsZWN0KCdyZWZyZXNoJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGQgcHVibGljIFwicXVpY2tfZWRpdF9maWx0ZXJcIiBtZXRob2QgdG8galF1ZXJ5IGluIG9yZGVyLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9hZGRQdWJsaWNNZXRob2QoKSB7XG5cdFx0XHRpZiAoJC5mbi5xdWlja19lZGl0X2ZpbHRlcikge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCQuZm4uZXh0ZW5kKHtcblx0XHRcdFx0cXVpY2tfZWRpdF9maWx0ZXI6IGZ1bmN0aW9uKGFjdGlvbiwgLi4uYXJncykge1xuXHRcdFx0XHRcdCQuZWFjaCh0aGlzLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHN3aXRjaCAoYWN0aW9uKSB7XG5cdFx0XHRcdFx0XHRcdGNhc2UgJ3JlbG9hZCc6XG5cdFx0XHRcdFx0XHRcdFx0X3JlbG9hZC5hcHBseSh0aGlzLCBhcmdzKTtcblx0XHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9uIEZpbHRlciBCdXR0b24gQ2xpY2tcblx0XHQgKlxuXHRcdCAqIEFwcGx5IHRoZSBwcm92aWRlZCBmaWx0ZXJzIGFuZCB1cGRhdGUgdGhlIHRhYmxlIHJvd3MuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uQXBwbHlGaWx0ZXJzQ2xpY2soKSB7XG5cdFx0XHQvLyBQcmVwYXJlIHRoZSBvYmplY3Qgd2l0aCB0aGUgZmluYWwgZmlsdGVyaW5nIGRhdGEuXG5cdFx0XHRjb25zdCBmaWx0ZXIgPSB7fTtcblx0XHRcdFxuXHRcdFx0JGZpbHRlci5maW5kKCd0aCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGNvbnN0IGNvbHVtbk5hbWUgPSAkKHRoaXMpLmRhdGEoJ2NvbHVtbk5hbWUnKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmIChjb2x1bW5OYW1lID09PSAnY2hlY2tib3gnIHx8IGNvbHVtbk5hbWUgPT09ICdhY3Rpb25zJykge1xuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRsZXQgdmFsdWUgPSBtb2R1bGUuYmluZGluZ3NbY29sdW1uTmFtZV0uZ2V0KCk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAodmFsdWUpIHtcblx0XHRcdFx0XHRmaWx0ZXJbY29sdW1uTmFtZV0gPSB2YWx1ZTtcblx0XHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oYCR7Y29sdW1uTmFtZX06bmFtZWApLnNlYXJjaCh2YWx1ZSk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKGAke2NvbHVtbk5hbWV9Om5hbWVgKS5zZWFyY2goJycpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JHRoaXMudHJpZ2dlcigncXVpY2tfZWRpdF9maWx0ZXI6Y2hhbmdlJywgW2ZpbHRlcl0pO1xuXHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuZHJhdygpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBPbiBSZXNldCBCdXR0b24gQ2xpY2tcblx0XHQgKlxuXHRcdCAqIFJlc2V0IHRoZSBmaWx0ZXIgZm9ybSBhbmQgcmVsb2FkIHRoZSB0YWJsZSBkYXRhIHdpdGhvdXQgZmlsdGVyaW5nLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblJlc2V0RmlsdGVyc0NsaWNrKCkge1xuXHRcdFx0Ly8gUmVtb3ZlIHZhbHVlcyBmcm9tIHRoZSBpbnB1dCBib3hlcy5cblx0XHRcdCRmaWx0ZXIuZmluZCgnaW5wdXQsIHNlbGVjdCcpLm5vdCgnLmxlbmd0aCwgLnNlbGVjdC1wYWdlLW1vZGUnKS52YWwoJycpO1xuXHRcdFx0JGZpbHRlci5maW5kKCdzZWxlY3QnKS5ub3QoJy5sZW5ndGgsIC5zZWxlY3QtcGFnZS1tb2RlJykubXVsdGlfc2VsZWN0KCdyZWZyZXNoJyk7XG5cdFx0XHRcblx0XHRcdC8vIFJlc2V0IHRoZSBmaWx0ZXJpbmcgdmFsdWVzLlxuXHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuY29sdW1ucygpLnNlYXJjaCgnJykuZHJhdygpO1xuXHRcdFx0XG5cdFx0XHQvLyBUcmlnZ2VyIEV2ZW50XG5cdFx0XHQkdGhpcy50cmlnZ2VyKCdxdWlja19lZGl0X2ZpbHRlcjpjaGFuZ2UnLCBbe31dKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQXBwbHkgdGhlIGZpbHRlcnMgd2hlbiB0aGUgdXNlciBwcmVzc2VzIHRoZSBFbnRlciBrZXkuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25JbnB1dFRleHRLZXlVcChldmVudCkge1xuXHRcdFx0aWYgKGV2ZW50LndoaWNoID09PSBFTlRFUl9LRVlfQ09ERSkge1xuXHRcdFx0XHQkZmlsdGVyLmZpbmQoJy5hcHBseS1maWx0ZXJzJykudHJpZ2dlcignY2xpY2snKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUGFyc2UgdGhlIGluaXRpYWwgZmlsdGVyaW5nIHBhcmFtZXRlcnMgYW5kIGFwcGx5IHRoZW0gdG8gdGhlIHRhYmxlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9wYXJzZUZpbHRlcmluZ1BhcmFtZXRlcnMoKSB7XG5cdFx0XHRjb25zdCB7ZmlsdGVyfSA9ICQuZGVwYXJhbSh3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNsaWNlKDEpKTtcblx0XHRcdFxuXHRcdFx0Zm9yIChsZXQgbmFtZSBpbiBmaWx0ZXIpIHtcblx0XHRcdFx0Y29uc3QgdmFsdWUgPSBmaWx0ZXJbbmFtZV07XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAobW9kdWxlLmJpbmRpbmdzW25hbWVdKSB7XG5cdFx0XHRcdFx0bW9kdWxlLmJpbmRpbmdzW25hbWVdLnNldCh2YWx1ZSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTm9ybWFsaXplIGFycmF5IGZpbHRlcmluZyB2YWx1ZXMuXG5cdFx0ICpcblx0XHQgKiBCeSBkZWZhdWx0IGRhdGF0YWJsZXMgd2lsbCBjb25jYXRlbmF0ZSBhcnJheSBzZWFyY2ggdmFsdWVzIGludG8gYSBzdHJpbmcgc2VwYXJhdGVkIHdpdGggXCIsXCIgY29tbWFzLiBUaGlzXG5cdFx0ICogaXMgbm90IGFjY2VwdGFibGUgdGhvdWdoIGJlY2F1c2Ugc29tZSBmaWx0ZXJpbmcgZWxlbWVudHMgbWF5IGNvbnRhaW4gdmFsdWVzIHdpdGggY29tbWEgYW5kIHRodXMgdGhlIGFycmF5XG5cdFx0ICogY2Fubm90IGJlIHBhcnNlZCBmcm9tIGJhY2tlbmQuIFRoaXMgbWV0aG9kIHdpbGwgcmVzZXQgdGhvc2UgY2FzZXMgYmFjayB0byBhcnJheXMgZm9yIGEgY2xlYXJlciB0cmFuc2FjdGlvblxuXHRcdCAqIHdpdGggdGhlIGJhY2tlbmQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdC5cblx0XHQgKiBAcGFyYW0ge0RhdGFUYWJsZXMuU2V0dGluZ3N9IHNldHRpbmdzIERhdGFUYWJsZXMgc2V0dGluZ3Mgb2JqZWN0LlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBkYXRhIERhdGEgdGhhdCB3aWxsIGJlIHNlbnQgdG8gdGhlIHNlcnZlciBpbiBhbiBvYmplY3QgZm9ybS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfbm9ybWFsaXplQXJyYXlWYWx1ZXMoZXZlbnQsIHNldHRpbmdzLCBkYXRhKSB7XG5cdFx0XHRjb25zdCBmaWx0ZXIgPSB7fTtcblx0XHRcdFxuXHRcdFx0Zm9yIChsZXQgbmFtZSBpbiBtb2R1bGUuYmluZGluZ3MpIHtcblx0XHRcdFx0Y29uc3QgdmFsdWUgPSBtb2R1bGUuYmluZGluZ3NbbmFtZV0uZ2V0KCk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAodmFsdWUgJiYgdmFsdWUuY29uc3RydWN0b3IgPT09IEFycmF5KSB7XG5cdFx0XHRcdFx0ZmlsdGVyW25hbWVdID0gdmFsdWU7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Zm9yIChsZXQgZW50cnkgaW4gZmlsdGVyKSB7XG5cdFx0XHRcdGZvciAobGV0IGNvbHVtbiBvZiBkYXRhLmNvbHVtbnMpIHtcblx0XHRcdFx0XHRpZiAoZW50cnkgPT09IGNvbHVtbi5uYW1lICYmIGZpbHRlcltlbnRyeV0uY29uc3RydWN0b3IgPT09IEFycmF5KSB7XG5cdFx0XHRcdFx0XHRjb2x1bW4uc2VhcmNoLnZhbHVlID0gZmlsdGVyW2VudHJ5XTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0Ly8gQWRkIHB1YmxpYyBtb2R1bGUgbWV0aG9kLiBcblx0XHRcdF9hZGRQdWJsaWNNZXRob2QoKTtcblx0XHRcdFxuXHRcdFx0Ly8gUGFyc2UgZmlsdGVyaW5nIEdFVCBwYXJhbWV0ZXJzLiBcblx0XHRcdF9wYXJzZUZpbHRlcmluZ1BhcmFtZXRlcnMoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQmluZCBldmVudCBoYW5kbGVycy5cblx0XHRcdCRmaWx0ZXJcblx0XHRcdFx0Lm9uKCdrZXl1cCcsICdpbnB1dDp0ZXh0JywgX29uSW5wdXRUZXh0S2V5VXApXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmFwcGx5LWZpbHRlcnMnLCBfb25BcHBseUZpbHRlcnNDbGljaylcblx0XHRcdFx0Lm9uKCdjbGljaycsICcucmVzZXQtZmlsdGVycycsIF9vblJlc2V0RmlsdGVyc0NsaWNrKTtcblx0XHRcdFxuXHRcdFx0JCgnW2RhdGEtbXVsdGlfc2VsZWN0LWNhdGVnb3J5aW5zdGFuY2VdJykuU3Vtb1NlbGVjdCh7XG5cdFx0XHRcdHBsYWNlaG9sZGVyOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnU0VMRUNUJywgJ2dlbmVyYWwnKSxcblx0XHRcdFx0Y3N2RGlzcENvdW50OiAyLFxuXHRcdFx0XHRjYXB0aW9uRm9ybWF0OiBgezB9ICR7anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3NlbGVjdGVkJywgJ2FkbWluX2xhYmVscycpfWAsXG5cdFx0XHRcdGxvY2FsZTogW1xuXHRcdFx0XHRcdCdPSycsXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0NBTkNFTCcsICdnZW5lcmFsJyksXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1NFTEVDVF9BTEwnLCAnZ2VuZXJhbCcpXG5cdFx0XHRcdF0sXG5cdFx0XHRcdHNlYXJjaDogdHJ1ZSxcblx0XHRcdFx0c2VhcmNoVGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1NFQVJDSCcsICdhZG1pbl9xdWlja19lZGl0JykgKyAnIC4uLicsXG5cdFx0XHRcdG5vTWF0Y2g6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdOT19SRVNVTFRTX0ZPUicsICdhZG1pbl9xdWlja19lZGl0JykgKyAnIFwiezB9XCInXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JHRoaXMub24oJ3ByZVhoci5kdCcsIF9ub3JtYWxpemVBcnJheVZhbHVlcyk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pOyJdfQ==
