'use strict';

/* --------------------------------------------------------------
 language_selection.js 2016-06-03
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('language_selection', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * On Language Link Click
  *
  * Prevent the default link behavior and regenerate the correct URL by taking into concern the dynamic
  * GET parameters (e.g. from table filtering).
  *
  * @param {jQuery.Event} event
  */
	function _onClickLanguageLink(event) {
		event.preventDefault();

		var currentUrlParameters = $.deparam(window.location.search.slice(1));

		currentUrlParameters.language = $(this).data('languageCode');

		window.location.href = window.location.pathname + '?' + $.param(currentUrlParameters);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', 'a', _onClickLanguageLink);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxheW91dHMvbWFpbi9mb290ZXIvbGFuZ3VhZ2Vfc2VsZWN0aW9uLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiX29uQ2xpY2tMYW5ndWFnZUxpbmsiLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiY3VycmVudFVybFBhcmFtZXRlcnMiLCJkZXBhcmFtIiwid2luZG93IiwibG9jYXRpb24iLCJzZWFyY2giLCJzbGljZSIsImxhbmd1YWdlIiwiaHJlZiIsInBhdGhuYW1lIiwicGFyYW0iLCJpbml0IiwiZG9uZSIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLG9CQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixrREFIRCxFQU9DLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1MLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FBUUEsVUFBU00sb0JBQVQsQ0FBOEJDLEtBQTlCLEVBQXFDO0FBQ3BDQSxRQUFNQyxjQUFOOztBQUVBLE1BQU1DLHVCQUF1QkosRUFBRUssT0FBRixDQUFVQyxPQUFPQyxRQUFQLENBQWdCQyxNQUFoQixDQUF1QkMsS0FBdkIsQ0FBNkIsQ0FBN0IsQ0FBVixDQUE3Qjs7QUFFQUwsdUJBQXFCTSxRQUFyQixHQUFnQ1YsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxjQUFiLENBQWhDOztBQUVBUSxTQUFPQyxRQUFQLENBQWdCSSxJQUFoQixHQUF1QkwsT0FBT0MsUUFBUCxDQUFnQkssUUFBaEIsR0FBMkIsR0FBM0IsR0FBaUNaLEVBQUVhLEtBQUYsQ0FBUVQsb0JBQVIsQ0FBeEQ7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUFULFFBQU9tQixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCaEIsUUFBTWlCLEVBQU4sQ0FBUyxPQUFULEVBQWtCLEdBQWxCLEVBQXVCZixvQkFBdkI7QUFDQWM7QUFDQSxFQUhEOztBQUtBLFFBQU9wQixNQUFQO0FBRUEsQ0E5REYiLCJmaWxlIjoibGF5b3V0cy9tYWluL2Zvb3Rlci9sYW5ndWFnZV9zZWxlY3Rpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gbGFuZ3VhZ2Vfc2VsZWN0aW9uLmpzIDIwMTYtMDYtMDNcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG5neC5jb250cm9sbGVycy5tb2R1bGUoXHJcblx0J2xhbmd1YWdlX3NlbGVjdGlvbicsXHJcblx0XHJcblx0W1xyXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS1kZXBhcmFtL2pxdWVyeS1kZXBhcmFtLm1pbi5qc2BcclxuXHRdLFxyXG5cdFxyXG5cdGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFxyXG5cdFx0J3VzZSBzdHJpY3QnO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIFZBUklBQkxFU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIEZVTkNUSU9OU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gTGFuZ3VhZ2UgTGluayBDbGlja1xyXG5cdFx0ICpcclxuXHRcdCAqIFByZXZlbnQgdGhlIGRlZmF1bHQgbGluayBiZWhhdmlvciBhbmQgcmVnZW5lcmF0ZSB0aGUgY29ycmVjdCBVUkwgYnkgdGFraW5nIGludG8gY29uY2VybiB0aGUgZHluYW1pY1xyXG5cdFx0ICogR0VUIHBhcmFtZXRlcnMgKGUuZy4gZnJvbSB0YWJsZSBmaWx0ZXJpbmcpLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25DbGlja0xhbmd1YWdlTGluayhldmVudCkge1xyXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3QgY3VycmVudFVybFBhcmFtZXRlcnMgPSAkLmRlcGFyYW0od2luZG93LmxvY2F0aW9uLnNlYXJjaC5zbGljZSgxKSk7XHJcblx0XHRcdFxyXG5cdFx0XHRjdXJyZW50VXJsUGFyYW1ldGVycy5sYW5ndWFnZSA9ICQodGhpcykuZGF0YSgnbGFuZ3VhZ2VDb2RlJyk7XHJcblx0XHRcdFxyXG5cdFx0XHR3aW5kb3cubG9jYXRpb24uaHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSArICc/JyArICQucGFyYW0oY3VycmVudFVybFBhcmFtZXRlcnMpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdCR0aGlzLm9uKCdjbGljaycsICdhJywgX29uQ2xpY2tMYW5ndWFnZUxpbmspO1xyXG5cdFx0XHRkb25lKCk7XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gbW9kdWxlO1xyXG5cdFx0XHJcblx0fSk7ICJdfQ==
