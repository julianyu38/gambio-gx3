'use strict';

/* --------------------------------------------------------------
 events.js 2018-04-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Events
 *
 * Handles the events of the main QuickEdit table.
 */
gx.controllers.module('events', ['loading_spinner', 'modal', gx.source + '/libs/button_dropdown', gx.source + '/libs/info_box'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Bulk selection change event handler.
  *
  * @param {jQuery.Event} event Contains event information.
  * @param {Boolean} [propagate = true] Whether to propagate the event or not.
  */
	function _onBulkSelectionChange(event) {
		var propagate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		if (propagate === false) {
			return;
		}

		$this.find('tbody input:checkbox.overview-row-selection').single_checkbox('checked', $(this).prop('checked')).trigger('change');
	}

	/**
  * Table row click event handler.
  *
  * @param {jQuery.Event} event Contains event information.
  */
	function _onTableRowClick(event) {
		if (!$(event.target).is('td')) {
			return;
		}

		var $singleCheckbox = $(this).find('input:checkbox.overview-row-selection');

		$singleCheckbox.prop('checked', !$(this).find('input:checkbox.overview-row-selection').prop('checked')).trigger('change');
	}

	/**
  * Enables row editing mode.
  */
	function _onTableRowEditClick() {
		var $tableRow = $(this).parents('tr');
		var $singleCheckbox = $(this).parents('tr').find('input:checkbox.overview-row-selection');

		var $dropdown = $tableRow.find('.btn-group.dropdown');
		var $saveAction = $dropdown.find('.save-row-edits');
		var $editAction = $tableRow.find('.row-edit');
		$editAction.addClass('hidden');
		$saveAction.removeClass('hidden');
		jse.libs.button_dropdown.setDefaultAction($dropdown, $saveAction);

		$tableRow.find('td.editable').each(function () {
			if ($(this).find('input:text').length > 0) {
				return;
			}

			if ($(this).hasClass('manufacturer')) {
				var rowIndex = $(this).parents('tr');
				var rowData = $this.DataTable().row(rowIndex).data();
				var options = rowData.DT_RowData.option.manufacturer;

				var html = '';

				options.forEach(function (option) {
					html += '<option value="' + option.id + '" ' + (rowData.manufacturer == option.value ? 'selected' : '') + '>\n\t\t\t\t\t\t\t' + option.value + '\n\t\t\t\t\t\t</option>';
				});

				this.innerHTML = '<select class="form-control">' + html + '</select>';
			} else {
				this.innerHTML = '<input type="text" class="form-control" value="' + this.innerText.replace(/"/g, '&quot;') + '" />';
			}
		});

		$singleCheckbox.prop('checked', !$(this).find('input:checkbox.overview-row-selection').prop('checked')).trigger('change');
	}

	/**
  * Show product icon event handler.
  *
  * Navigates the browser to the product details page.
  */
	function _onTableRowShowClick() {
		var productId = $(this).parents('tr').data('id');

		var parameters = {
			pID: productId,
			action: 'new_product'
		};

		window.open(jse.core.config.get('appUrl') + '/admin/categories.php?' + $.param(parameters), '_blank');
	}

	/**
  * Bulk row edit event handler.
  *
  * Enables the edit mode for the selected rows.
  */
	function _onTableBulkRowEditClick() {
		var $bulkEditAction = $(this);
		var $checkedSingleCheckboxes = $this.find('input:checkbox:checked.overview-row-selection');

		if ($checkedSingleCheckboxes.length) {
			var $bulkActionsDropdown = $('.overview-bulk-action');
			var $bulkSaveAction = $bulkActionsDropdown.find('.save-bulk-row-edits');
			$bulkEditAction.addClass('hidden');
			$bulkSaveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkSaveAction);
		}

		$checkedSingleCheckboxes.each(function () {
			var $tableRow = $(this).parents('tr');
			var $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
			var $saveAction = $rowActionsDropdown.find('.save-row-edits');
			var $editAction = $tableRow.find('.row-edit');

			$editAction.addClass('hidden');
			$saveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $saveAction);

			$tableRow.find('td.editable').each(function () {

				if ($(this).find('input:text').length > 0) {
					return;
				}

				if ($(this).hasClass('manufacturer')) {
					var rowIndex = $(this).parents('tr');
					var rowData = $this.DataTable().row(rowIndex).data();
					var options = rowData.DT_RowData.option.manufacturer;

					var html = '';

					options.forEach(function (option) {
						html += '<option value="' + option.id + '" ' + (rowData.manufacturer == option.value ? 'selected' : '') + '>\n\t\t\t\t\t\t\t' + option.value + '\n\t\t\t\t\t\t</option>';
					});

					this.innerHTML = '<select class="form-control">' + html + '</select>';
				} else {
					this.innerHTML = '<input type="text" class="form-control" value="' + this.innerText.replace(/"/g, '&quot;') + '" />';
				}
			});
		});
	}

	/**
  * Table row checkbox change event handler.
  */
	function _onTableRowCheckboxChange() {
		var $bulkActionDropdownButtons = $this.parents('.quick-edit.overview').find('.overview-bulk-action > button');
		var $tableRow = $(this).parents('tr');

		if ($this.find('input:checkbox:checked.overview-row-selection').length > 0) {
			$bulkActionDropdownButtons.removeClass('disabled');
		} else {
			$bulkActionDropdownButtons.addClass('disabled');

			var $bulkActionsDropdown = $('.overview-bulk-action');
			var $bulkSaveAction = $bulkActionsDropdown.find('.save-bulk-row-edits');

			if (!$bulkSaveAction.hasClass('hidden')) {
				var $bulkEditAction = $bulkActionsDropdown.find('.bulk-row-edit');
				$bulkEditAction.removeClass('hidden');
				$bulkSaveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkEditAction);
			}
		}

		if (!$(this).prop('checked')) {
			_resolveRowChanges($(this).parents('tr'));

			var $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
			var $saveAction = $rowActionsDropdown.find('.save-row-edits');

			if (!$saveAction.hasClass('hidden')) {
				var $editAction = $tableRow.find('.row-edit');
				$editAction.removeClass('hidden');
				$saveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $editAction.last());
			}
		}
	}

	/**
  * Page mode change between "edit" and "filtering".
  */
	function _onPageModeChange() {
		if ($(this).val() == 'edit-mode') {
			$this.find('tr.filter').attr('hidden', true);
			$this.find('tr.edit').attr('hidden', false);
			$this.find('thead tr:first-child th').addClass('edit-mode');
		} else {
			$this.find('tr.filter').attr('hidden', false);
			$this.find('tr.edit').attr('hidden', true);
			$this.find('thead tr:first-child th').removeClass('edit-mode');
		}
	}

	/**
  * Cancel data modifications event handler.
  */
	function _onCancelClick() {
		var $pageMode = $(this).closest('thead').find('.select-page-mode');
		var $checkedSingleCheckboxes = $this.find('input:checkbox:checked.overview-row-selection');

		if ($pageMode.val() == 'edit-mode') {
			$pageMode.val('filter-mode');
		} else {
			$pageMode.val('edit-mode');
		}

		$checkedSingleCheckboxes.each(function () {
			$(this).prop('checked', !$(this).prop('checked')).trigger('change');

			_resolveRowChanges($(this).parents('tr'));
		});

		_onPageModeChange();
	}

	/**
  * Restores all the row data changes back to their original state.
  *
  * @param {jQuery.Event} $row Current row selector.
  */
	function _resolveRowChanges($row) {
		var rowIndex = $this.DataTable().row($row).index();

		$row.find('input:text, select').not('.select-tax, .select-shipping-time').each(function () {
			var $cell = $(this).closest('td');
			var columnIndex = $this.DataTable().column($cell).index();
			this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
		});
	}

	/**
  * Table row data change event handler.
  *
  * It's being triggered every time a row input/select field is changed.
  */
	function _onTableRowDataChange() {
		var $row = $(this).closest('tr');
		var rowIndex = $this.DataTable().row($row).index();

		$row.find('input:text, select').not('.select-tax, .select-shipping-time').each(function () {
			var $cell = $(this).closest('td');
			var columnIndex = $this.DataTable().column($cell).index();

			if ($.trim($(this).val()) != $this.DataTable().cell(rowIndex, columnIndex).data()) {
				$(this).addClass('modified');
				return;
			}

			$(this).removeClass('modified');
		});
	}

	/**
  * Table row switcher change event handler.
  *
  * @param {HTMLElement} switcher Current switcher element.
  */
	function _onTableRowSwitcherChange() {
		var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/Update';
		var productId = $(this).parents('tr').data('id');
		var $cell = $(this).closest('td');
		var columnIndex = $this.DataTable().column($cell).index();
		var columnName = $this.find('tr.filter th').eq(columnIndex).data('columnName');
		var data = {};
		var value = {};

		value[columnName] = $(this).prop('checked') ? 1 : 0;
		data[productId] = value;

		$.post(url, {
			data: data,
			pageToken: jse.core.config.get('pageToken')
		}).done(function (response) {
			response = $.parseJSON(response);

			if (response.success) {
				var content = jse.core.lang.translate('SUCCESS_PRODUCT_UPDATED', 'admin_quick_edit');

				// Show success message in the admin info box.
				jse.libs.info_box.addSuccessMessage(content);

				return;
			}

			var title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
			var message = jse.core.lang.translate('EDIT_ERROR', 'admin_quick_edit');
			var buttons = [{
				title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
				callback: function callback(event) {
					return $(event.currentTarget).parents('.modal').modal('hide');
				}
			}];

			jse.libs.modal.showMessage(title, message, buttons);
		});
	}

	/**
  * Create inventory list click event handler.
  *
  * Generates an inventory list PDF from the selected rows or from all the records if no row is selected.
  */
	function _onCreateInventoryListClick() {
		var $checkedSingleCheckboxes = $this.find('input:checkbox:checked.overview-row-selection');
		var $modal = $this.parents('.quick-edit.overview').find('div.downloads');
		var $download = $modal.find('button.download-button');
		var generateUrl = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/CreateInventoryFile';
		var downloadUrl = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/DownloadInventoryFile&pageToken=' + jse.core.config.get('pageToken');
		var productsId = [];

		$checkedSingleCheckboxes.each(function () {
			var id = $(this).parents('tr').data('id');
			productsId.push(id);
		});

		var data = {
			data: 'inventoryList',
			products: productsId,
			pageToken: jse.core.config.get('pageToken')
		};

		$modal.find('.modal-body').empty().append('<p><i class="fa fa-clock-o"></i> ' + jse.core.lang.translate('TEXT_PLEASE_WAIT', 'admin_quick_edit') + '...</p>');

		$.post(generateUrl, data).done(function (response) {
			response = $.parseJSON(response);

			if (response.success) {
				var $documentTarget = '<a href="' + downloadUrl + '" class="btn btn-primary download-button" target="_blank">' + jse.core.lang.translate('BUTTON_DOWNLOAD', 'admin_quick_edit') + '</a>';

				$modal.find('.modal-body').html('<p><i class="fa fa-check"></i> ' + jse.core.lang.translate('TEXT_READY_TO_DOWNLOAD_DOCUMENT', 'admin_quick_edit') + '...</p>');

				$download.replaceWith($documentTarget);
				$modal.find('.download-button').on('click', function () {
					$modal.modal('hide');
				});
			}
		});

		$modal.on('hidden.bs.modal', function () {
			var $download = $modal.find('a.download-button');
			var $downloadButton = '<button type="button" class="btn btn-primary download-button" disabled>' + jse.core.lang.translate('BUTTON_DOWNLOAD', 'admin_quick_edit') + '</button>';

			$download.replaceWith($downloadButton);
		});
	}

	/**
  * Display graduated prices management modal.
  *
  * Note: Current product ID must be set as a data "productId" value in the modal container element.
  */
	function _onTableRowGraduatedPricesClick() {
		$('.graduated-prices.modal').data('productId', $(this).parents('tr').data('id')).modal('show');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', 'tbody tr', _onTableRowClick).on('click', '.row-edit', _onTableRowEditClick).on('click', '.show-product', _onTableRowShowClick).on('click', '.graduated-prices', _onTableRowGraduatedPricesClick).on('change', '.overview-bulk-selection', _onBulkSelectionChange).on('keyup', 'tbody tr input:text', _onTableRowDataChange).on('change', 'tbody tr select', _onTableRowDataChange);

		$this.parents('.quick-edit.overview').on('change', 'input:checkbox.overview-row-selection', _onTableRowCheckboxChange).on('change', '.select-page-mode', _onPageModeChange).on('click', '.cancel-edits', _onCancelClick).on('click', '.btn-group .bulk-row-edit', _onTableBulkRowEditClick);

		$('body').on('click', '.create-inventory-list', _onCreateInventoryListClick);

		$this.on('draw.dt', function () {
			$this.find('.convert-to-switcher').on('change', _onTableRowSwitcherChange);
			_onTableRowCheckboxChange();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvb3ZlcnZpZXcvZXZlbnRzLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiX29uQnVsa1NlbGVjdGlvbkNoYW5nZSIsImV2ZW50IiwicHJvcGFnYXRlIiwiZmluZCIsInNpbmdsZV9jaGVja2JveCIsInByb3AiLCJ0cmlnZ2VyIiwiX29uVGFibGVSb3dDbGljayIsInRhcmdldCIsImlzIiwiJHNpbmdsZUNoZWNrYm94IiwiX29uVGFibGVSb3dFZGl0Q2xpY2siLCIkdGFibGVSb3ciLCJwYXJlbnRzIiwiJGRyb3Bkb3duIiwiJHNhdmVBY3Rpb24iLCIkZWRpdEFjdGlvbiIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJqc2UiLCJsaWJzIiwiYnV0dG9uX2Ryb3Bkb3duIiwic2V0RGVmYXVsdEFjdGlvbiIsImVhY2giLCJsZW5ndGgiLCJoYXNDbGFzcyIsInJvd0luZGV4Iiwicm93RGF0YSIsIkRhdGFUYWJsZSIsInJvdyIsIm9wdGlvbnMiLCJEVF9Sb3dEYXRhIiwib3B0aW9uIiwibWFudWZhY3R1cmVyIiwiaHRtbCIsImZvckVhY2giLCJpZCIsInZhbHVlIiwiaW5uZXJIVE1MIiwiaW5uZXJUZXh0IiwicmVwbGFjZSIsIl9vblRhYmxlUm93U2hvd0NsaWNrIiwicHJvZHVjdElkIiwicGFyYW1ldGVycyIsInBJRCIsImFjdGlvbiIsIndpbmRvdyIsIm9wZW4iLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwicGFyYW0iLCJfb25UYWJsZUJ1bGtSb3dFZGl0Q2xpY2siLCIkYnVsa0VkaXRBY3Rpb24iLCIkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMiLCIkYnVsa0FjdGlvbnNEcm9wZG93biIsIiRidWxrU2F2ZUFjdGlvbiIsIiRyb3dBY3Rpb25zRHJvcGRvd24iLCJfb25UYWJsZVJvd0NoZWNrYm94Q2hhbmdlIiwiJGJ1bGtBY3Rpb25Ecm9wZG93bkJ1dHRvbnMiLCJfcmVzb2x2ZVJvd0NoYW5nZXMiLCJsYXN0IiwiX29uUGFnZU1vZGVDaGFuZ2UiLCJ2YWwiLCJhdHRyIiwiX29uQ2FuY2VsQ2xpY2siLCIkcGFnZU1vZGUiLCJjbG9zZXN0IiwiJHJvdyIsImluZGV4Iiwibm90IiwiJGNlbGwiLCJjb2x1bW5JbmRleCIsImNvbHVtbiIsInBhcmVudEVsZW1lbnQiLCJjZWxsIiwiX29uVGFibGVSb3dEYXRhQ2hhbmdlIiwidHJpbSIsIl9vblRhYmxlUm93U3dpdGNoZXJDaGFuZ2UiLCJ1cmwiLCJjb2x1bW5OYW1lIiwiZXEiLCJwb3N0IiwicGFnZVRva2VuIiwiZG9uZSIsInJlc3BvbnNlIiwicGFyc2VKU09OIiwic3VjY2VzcyIsImNvbnRlbnQiLCJsYW5nIiwidHJhbnNsYXRlIiwiaW5mb19ib3giLCJhZGRTdWNjZXNzTWVzc2FnZSIsInRpdGxlIiwibWVzc2FnZSIsImJ1dHRvbnMiLCJjYWxsYmFjayIsImN1cnJlbnRUYXJnZXQiLCJtb2RhbCIsInNob3dNZXNzYWdlIiwiX29uQ3JlYXRlSW52ZW50b3J5TGlzdENsaWNrIiwiJG1vZGFsIiwiJGRvd25sb2FkIiwiZ2VuZXJhdGVVcmwiLCJkb3dubG9hZFVybCIsInByb2R1Y3RzSWQiLCJwdXNoIiwicHJvZHVjdHMiLCJlbXB0eSIsImFwcGVuZCIsIiRkb2N1bWVudFRhcmdldCIsInJlcGxhY2VXaXRoIiwib24iLCIkZG93bmxvYWRCdXR0b24iLCJfb25UYWJsZVJvd0dyYWR1YXRlZFByaWNlc0NsaWNrIiwiaW5pdCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7OztBQUtBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0IsUUFBdEIsRUFDQyxDQUNDLGlCQURELEVBQ29CLE9BRHBCLEVBQ2dDRixHQUFHRyxNQURuQyw0QkFDcUVILEdBQUdHLE1BRHhFLG9CQURELEVBSUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUosU0FBUyxFQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUEsVUFBU0ssc0JBQVQsQ0FBZ0NDLEtBQWhDLEVBQXlEO0FBQUEsTUFBbEJDLFNBQWtCLHVFQUFOLElBQU07O0FBQ3hELE1BQUlBLGNBQWMsS0FBbEIsRUFBeUI7QUFDeEI7QUFDQTs7QUFFREosUUFBTUssSUFBTixDQUFXLDZDQUFYLEVBQ0VDLGVBREYsQ0FDa0IsU0FEbEIsRUFDNkJMLEVBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsU0FBYixDQUQ3QixFQUVFQyxPQUZGLENBRVUsUUFGVjtBQUdBOztBQUVEOzs7OztBQUtBLFVBQVNDLGdCQUFULENBQTBCTixLQUExQixFQUFpQztBQUNoQyxNQUFJLENBQUNGLEVBQUVFLE1BQU1PLE1BQVIsRUFBZ0JDLEVBQWhCLENBQW1CLElBQW5CLENBQUwsRUFBK0I7QUFDOUI7QUFDQTs7QUFFRCxNQUFNQyxrQkFBa0JYLEVBQUUsSUFBRixFQUFRSSxJQUFSLENBQWEsdUNBQWIsQ0FBeEI7O0FBRUFPLGtCQUFnQkwsSUFBaEIsQ0FBcUIsU0FBckIsRUFBZ0MsQ0FBQ04sRUFBRSxJQUFGLEVBQy9CSSxJQUQrQixDQUMxQix1Q0FEMEIsRUFFL0JFLElBRitCLENBRTFCLFNBRjBCLENBQWpDLEVBR0VDLE9BSEYsQ0FHVSxRQUhWO0FBSUE7O0FBRUQ7OztBQUdBLFVBQVNLLG9CQUFULEdBQWdDO0FBQy9CLE1BQU1DLFlBQVliLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLENBQWxCO0FBQ0EsTUFBTUgsa0JBQWtCWCxFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixFQUFzQlYsSUFBdEIsQ0FBMkIsdUNBQTNCLENBQXhCOztBQUVBLE1BQU1XLFlBQVlGLFVBQVVULElBQVYsQ0FBZSxxQkFBZixDQUFsQjtBQUNBLE1BQU1ZLGNBQWNELFVBQVVYLElBQVYsQ0FBZSxpQkFBZixDQUFwQjtBQUNBLE1BQU1hLGNBQWNKLFVBQVVULElBQVYsQ0FBZSxXQUFmLENBQXBCO0FBQ0FhLGNBQVlDLFFBQVosQ0FBcUIsUUFBckI7QUFDQUYsY0FBWUcsV0FBWixDQUF3QixRQUF4QjtBQUNBQyxNQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLGdCQUF6QixDQUEwQ1IsU0FBMUMsRUFBcURDLFdBQXJEOztBQUVBSCxZQUFVVCxJQUFWLENBQWUsYUFBZixFQUE4Qm9CLElBQTlCLENBQW1DLFlBQVc7QUFDN0MsT0FBSXhCLEVBQUUsSUFBRixFQUFRSSxJQUFSLENBQWEsWUFBYixFQUEyQnFCLE1BQTNCLEdBQW9DLENBQXhDLEVBQTJDO0FBQzFDO0FBQ0E7O0FBRUQsT0FBSXpCLEVBQUUsSUFBRixFQUFRMEIsUUFBUixDQUFpQixjQUFqQixDQUFKLEVBQXNDO0FBQ3JDLFFBQU1DLFdBQVczQixFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixDQUFqQjtBQUNBLFFBQU1jLFVBQVU3QixNQUFNOEIsU0FBTixHQUFrQkMsR0FBbEIsQ0FBc0JILFFBQXRCLEVBQWdDN0IsSUFBaEMsRUFBaEI7QUFDQSxRQUFNaUMsVUFBVUgsUUFBUUksVUFBUixDQUFtQkMsTUFBbkIsQ0FBMEJDLFlBQTFDOztBQUVBLFFBQUlDLE9BQU8sRUFBWDs7QUFFQUosWUFBUUssT0FBUixDQUFnQixrQkFBVTtBQUN6QkQsaUNBQTBCRixPQUFPSSxFQUFqQyxXQUF3Q1QsUUFBUU0sWUFBUixJQUF3QkQsT0FBT0ssS0FBL0IsR0FBdUMsVUFBdkMsR0FBb0QsRUFBNUYsMEJBQ0dMLE9BQU9LLEtBRFY7QUFHQSxLQUpEOztBQU1BLFNBQUtDLFNBQUwscUNBQWlESixJQUFqRDtBQUVBLElBZkQsTUFlTztBQUNOLFNBQUtJLFNBQUwsR0FDQyxvREFBb0QsS0FBS0MsU0FBTCxDQUFlQyxPQUFmLENBQXVCLElBQXZCLEVBQTZCLFFBQTdCLENBQXBELFNBREQ7QUFHQTtBQUVELEdBMUJEOztBQTRCQTlCLGtCQUFnQkwsSUFBaEIsQ0FBcUIsU0FBckIsRUFBZ0MsQ0FBQ04sRUFBRSxJQUFGLEVBQy9CSSxJQUQrQixDQUMxQix1Q0FEMEIsRUFFL0JFLElBRitCLENBRTFCLFNBRjBCLENBQWpDLEVBR0VDLE9BSEYsQ0FHVSxRQUhWO0FBSUE7O0FBRUQ7Ozs7O0FBS0EsVUFBU21DLG9CQUFULEdBQWdDO0FBQy9CLE1BQU1DLFlBQVkzQyxFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixFQUFzQmhCLElBQXRCLENBQTJCLElBQTNCLENBQWxCOztBQUVBLE1BQU04QyxhQUFhO0FBQ2xCQyxRQUFLRixTQURhO0FBRWxCRyxXQUFRO0FBRlUsR0FBbkI7O0FBS0FDLFNBQU9DLElBQVAsQ0FBZTVCLElBQUk2QixJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQUgsOEJBQTJEbkQsRUFBRW9ELEtBQUYsQ0FBUVIsVUFBUixDQUF2RSxFQUE0RixRQUE1RjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNTLHdCQUFULEdBQW9DO0FBQ25DLE1BQU1DLGtCQUFrQnRELEVBQUUsSUFBRixDQUF4QjtBQUNBLE1BQU11RCwyQkFBMkJ4RCxNQUFNSyxJQUFOLENBQVcsK0NBQVgsQ0FBakM7O0FBRUEsTUFBSW1ELHlCQUF5QjlCLE1BQTdCLEVBQXFDO0FBQ3BDLE9BQU0rQix1QkFBdUJ4RCxFQUFFLHVCQUFGLENBQTdCO0FBQ0EsT0FBTXlELGtCQUFrQkQscUJBQXFCcEQsSUFBckIsQ0FBMEIsc0JBQTFCLENBQXhCO0FBQ0FrRCxtQkFBZ0JwQyxRQUFoQixDQUF5QixRQUF6QjtBQUNBdUMsbUJBQWdCdEMsV0FBaEIsQ0FBNEIsUUFBNUI7QUFDQUMsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxnQkFBekIsQ0FBMENpQyxvQkFBMUMsRUFBZ0VDLGVBQWhFO0FBQ0E7O0FBRURGLDJCQUF5Qi9CLElBQXpCLENBQThCLFlBQVc7QUFDeEMsT0FBTVgsWUFBWWIsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbEI7QUFDQSxPQUFNNEMsc0JBQXNCN0MsVUFBVVQsSUFBVixDQUFlLHFCQUFmLENBQTVCO0FBQ0EsT0FBTVksY0FBYzBDLG9CQUFvQnRELElBQXBCLENBQXlCLGlCQUF6QixDQUFwQjtBQUNBLE9BQU1hLGNBQWNKLFVBQVVULElBQVYsQ0FBZSxXQUFmLENBQXBCOztBQUVBYSxlQUFZQyxRQUFaLENBQXFCLFFBQXJCO0FBQ0FGLGVBQVlHLFdBQVosQ0FBd0IsUUFBeEI7QUFDQUMsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxnQkFBekIsQ0FBMENtQyxtQkFBMUMsRUFBK0QxQyxXQUEvRDs7QUFFQUgsYUFBVVQsSUFBVixDQUFlLGFBQWYsRUFBOEJvQixJQUE5QixDQUFtQyxZQUFXOztBQUU3QyxRQUFJeEIsRUFBRSxJQUFGLEVBQVFJLElBQVIsQ0FBYSxZQUFiLEVBQTJCcUIsTUFBM0IsR0FBb0MsQ0FBeEMsRUFBMkM7QUFDMUM7QUFDQTs7QUFFRCxRQUFJekIsRUFBRSxJQUFGLEVBQVEwQixRQUFSLENBQWlCLGNBQWpCLENBQUosRUFBc0M7QUFDckMsU0FBTUMsV0FBVzNCLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLENBQWpCO0FBQ0EsU0FBTWMsVUFBVTdCLE1BQU04QixTQUFOLEdBQWtCQyxHQUFsQixDQUFzQkgsUUFBdEIsRUFBZ0M3QixJQUFoQyxFQUFoQjtBQUNBLFNBQU1pQyxVQUFVSCxRQUFRSSxVQUFSLENBQW1CQyxNQUFuQixDQUEwQkMsWUFBMUM7O0FBRUEsU0FBSUMsT0FBTyxFQUFYOztBQUVBSixhQUFRSyxPQUFSLENBQWdCLGtCQUFVO0FBQ3pCRCxrQ0FDbUJGLE9BQU9JLEVBRDFCLFdBQ2lDVCxRQUFRTSxZQUFSLElBQXdCRCxPQUFPSyxLQUEvQixHQUF1QyxVQUF2QyxHQUFvRCxFQURyRiwwQkFFRUwsT0FBT0ssS0FGVDtBQUlBLE1BTEQ7O0FBT0EsVUFBS0MsU0FBTCxxQ0FBaURKLElBQWpEO0FBRUEsS0FoQkQsTUFnQk87QUFDTixVQUFLSSxTQUFMLEdBQ0Msb0RBQW9ELEtBQUtDLFNBQUwsQ0FBZUMsT0FBZixDQUF1QixJQUF2QixFQUE2QixRQUE3QixDQUFwRCxTQUREO0FBR0E7QUFDRCxJQTNCRDtBQTRCQSxHQXRDRDtBQXVDQTs7QUFFRDs7O0FBR0EsVUFBU2tCLHlCQUFULEdBQXFDO0FBQ3BDLE1BQU1DLDZCQUE2QjdELE1BQU1lLE9BQU4sQ0FBYyxzQkFBZCxFQUNqQ1YsSUFEaUMsQ0FDNUIsZ0NBRDRCLENBQW5DO0FBRUEsTUFBTVMsWUFBWWIsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbEI7O0FBRUEsTUFBSWYsTUFBTUssSUFBTixDQUFXLCtDQUFYLEVBQTREcUIsTUFBNUQsR0FBcUUsQ0FBekUsRUFBNEU7QUFDM0VtQyw4QkFBMkJ6QyxXQUEzQixDQUF1QyxVQUF2QztBQUNBLEdBRkQsTUFFTztBQUNOeUMsOEJBQTJCMUMsUUFBM0IsQ0FBb0MsVUFBcEM7O0FBRUEsT0FBTXNDLHVCQUF1QnhELEVBQUUsdUJBQUYsQ0FBN0I7QUFDQSxPQUFNeUQsa0JBQWtCRCxxQkFBcUJwRCxJQUFyQixDQUEwQixzQkFBMUIsQ0FBeEI7O0FBRUEsT0FBSSxDQUFDcUQsZ0JBQWdCL0IsUUFBaEIsQ0FBeUIsUUFBekIsQ0FBTCxFQUF5QztBQUN4QyxRQUFNNEIsa0JBQWtCRSxxQkFBcUJwRCxJQUFyQixDQUEwQixnQkFBMUIsQ0FBeEI7QUFDQWtELG9CQUFnQm5DLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0FzQyxvQkFBZ0J2QyxRQUFoQixDQUF5QixRQUF6QjtBQUNBRSxRQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLGdCQUF6QixDQUEwQ2lDLG9CQUExQyxFQUFnRUYsZUFBaEU7QUFDQTtBQUNEOztBQUVELE1BQUksQ0FBQ3RELEVBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsU0FBYixDQUFMLEVBQThCO0FBQzdCdUQsc0JBQW1CN0QsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbkI7O0FBRUEsT0FBTTRDLHNCQUFzQjdDLFVBQVVULElBQVYsQ0FBZSxxQkFBZixDQUE1QjtBQUNBLE9BQU1ZLGNBQWMwQyxvQkFBb0J0RCxJQUFwQixDQUF5QixpQkFBekIsQ0FBcEI7O0FBRUEsT0FBSSxDQUFDWSxZQUFZVSxRQUFaLENBQXFCLFFBQXJCLENBQUwsRUFBcUM7QUFDcEMsUUFBTVQsY0FBY0osVUFBVVQsSUFBVixDQUFlLFdBQWYsQ0FBcEI7QUFDQWEsZ0JBQVlFLFdBQVosQ0FBd0IsUUFBeEI7QUFDQUgsZ0JBQVlFLFFBQVosQ0FBcUIsUUFBckI7QUFDQUUsUUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxnQkFBekIsQ0FBMENtQyxtQkFBMUMsRUFBK0R6QyxZQUFZNkMsSUFBWixFQUEvRDtBQUNBO0FBQ0Q7QUFDRDs7QUFFRDs7O0FBR0EsVUFBU0MsaUJBQVQsR0FBNkI7QUFDNUIsTUFBSS9ELEVBQUUsSUFBRixFQUFRZ0UsR0FBUixNQUFpQixXQUFyQixFQUFrQztBQUNqQ2pFLFNBQU1LLElBQU4sQ0FBVyxXQUFYLEVBQXdCNkQsSUFBeEIsQ0FBNkIsUUFBN0IsRUFBdUMsSUFBdkM7QUFDQWxFLFNBQU1LLElBQU4sQ0FBVyxTQUFYLEVBQXNCNkQsSUFBdEIsQ0FBMkIsUUFBM0IsRUFBcUMsS0FBckM7QUFDQWxFLFNBQU1LLElBQU4sQ0FBVyx5QkFBWCxFQUFzQ2MsUUFBdEMsQ0FBK0MsV0FBL0M7QUFDQSxHQUpELE1BSU87QUFDTm5CLFNBQU1LLElBQU4sQ0FBVyxXQUFYLEVBQXdCNkQsSUFBeEIsQ0FBNkIsUUFBN0IsRUFBdUMsS0FBdkM7QUFDQWxFLFNBQU1LLElBQU4sQ0FBVyxTQUFYLEVBQXNCNkQsSUFBdEIsQ0FBMkIsUUFBM0IsRUFBcUMsSUFBckM7QUFDQWxFLFNBQU1LLElBQU4sQ0FBVyx5QkFBWCxFQUFzQ2UsV0FBdEMsQ0FBa0QsV0FBbEQ7QUFDQTtBQUNEOztBQUVEOzs7QUFHQSxVQUFTK0MsY0FBVCxHQUEwQjtBQUN6QixNQUFNQyxZQUFZbkUsRUFBRSxJQUFGLEVBQVFvRSxPQUFSLENBQWdCLE9BQWhCLEVBQXlCaEUsSUFBekIsQ0FBOEIsbUJBQTlCLENBQWxCO0FBQ0EsTUFBTW1ELDJCQUEyQnhELE1BQU1LLElBQU4sQ0FBVywrQ0FBWCxDQUFqQzs7QUFFQSxNQUFJK0QsVUFBVUgsR0FBVixNQUFtQixXQUF2QixFQUFvQztBQUNuQ0csYUFBVUgsR0FBVixDQUFjLGFBQWQ7QUFDQSxHQUZELE1BRU87QUFDTkcsYUFBVUgsR0FBVixDQUFjLFdBQWQ7QUFDQTs7QUFFRFQsMkJBQXlCL0IsSUFBekIsQ0FBOEIsWUFBVztBQUN4Q3hCLEtBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsU0FBYixFQUF3QixDQUFDTixFQUFFLElBQUYsRUFDdkJNLElBRHVCLENBQ2xCLFNBRGtCLENBQXpCLEVBRUVDLE9BRkYsQ0FFVSxRQUZWOztBQUlBc0Qsc0JBQW1CN0QsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbkI7QUFDQSxHQU5EOztBQVFBaUQ7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTRixrQkFBVCxDQUE0QlEsSUFBNUIsRUFBa0M7QUFDakMsTUFBTTFDLFdBQVc1QixNQUFNOEIsU0FBTixHQUFrQkMsR0FBbEIsQ0FBc0J1QyxJQUF0QixFQUE0QkMsS0FBNUIsRUFBakI7O0FBRUFELE9BQUtqRSxJQUFMLENBQVUsb0JBQVYsRUFBZ0NtRSxHQUFoQyxDQUFvQyxvQ0FBcEMsRUFBMEUvQyxJQUExRSxDQUErRSxZQUFXO0FBQ3pGLE9BQU1nRCxRQUFReEUsRUFBRSxJQUFGLEVBQVFvRSxPQUFSLENBQWdCLElBQWhCLENBQWQ7QUFDQSxPQUFNSyxjQUFjMUUsTUFBTThCLFNBQU4sR0FBa0I2QyxNQUFsQixDQUF5QkYsS0FBekIsRUFBZ0NGLEtBQWhDLEVBQXBCO0FBQ0EsUUFBS0ssYUFBTCxDQUFtQnBDLFNBQW5CLEdBQStCeEMsTUFBTThCLFNBQU4sR0FBa0IrQyxJQUFsQixDQUF1QmpELFFBQXZCLEVBQWlDOEMsV0FBakMsRUFBOEMzRSxJQUE5QyxFQUEvQjtBQUNBLEdBSkQ7QUFLQTs7QUFFRDs7Ozs7QUFLQSxVQUFTK0UscUJBQVQsR0FBaUM7QUFDaEMsTUFBTVIsT0FBT3JFLEVBQUUsSUFBRixFQUFRb0UsT0FBUixDQUFnQixJQUFoQixDQUFiO0FBQ0EsTUFBTXpDLFdBQVc1QixNQUFNOEIsU0FBTixHQUFrQkMsR0FBbEIsQ0FBc0J1QyxJQUF0QixFQUE0QkMsS0FBNUIsRUFBakI7O0FBRUFELE9BQUtqRSxJQUFMLENBQVUsb0JBQVYsRUFBZ0NtRSxHQUFoQyxDQUFvQyxvQ0FBcEMsRUFBMEUvQyxJQUExRSxDQUErRSxZQUFXO0FBQ3pGLE9BQU1nRCxRQUFReEUsRUFBRSxJQUFGLEVBQVFvRSxPQUFSLENBQWdCLElBQWhCLENBQWQ7QUFDQSxPQUFNSyxjQUFjMUUsTUFBTThCLFNBQU4sR0FBa0I2QyxNQUFsQixDQUF5QkYsS0FBekIsRUFBZ0NGLEtBQWhDLEVBQXBCOztBQUVBLE9BQUl0RSxFQUFFOEUsSUFBRixDQUFPOUUsRUFBRSxJQUFGLEVBQVFnRSxHQUFSLEVBQVAsS0FBeUJqRSxNQUFNOEIsU0FBTixHQUFrQitDLElBQWxCLENBQXVCakQsUUFBdkIsRUFBaUM4QyxXQUFqQyxFQUE4QzNFLElBQTlDLEVBQTdCLEVBQW1GO0FBQ2xGRSxNQUFFLElBQUYsRUFBUWtCLFFBQVIsQ0FBaUIsVUFBakI7QUFDQTtBQUNBOztBQUVEbEIsS0FBRSxJQUFGLEVBQVFtQixXQUFSLENBQW9CLFVBQXBCO0FBQ0EsR0FWRDtBQVdBOztBQUVEOzs7OztBQUtBLFVBQVM0RCx5QkFBVCxHQUFxQztBQUNwQyxNQUFNQyxNQUFNNUQsSUFBSTZCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0Msa0RBQTVDO0FBQ0EsTUFBTVIsWUFBWTNDLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLEVBQXNCaEIsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBbEI7QUFDQSxNQUFNMEUsUUFBUXhFLEVBQUUsSUFBRixFQUFRb0UsT0FBUixDQUFnQixJQUFoQixDQUFkO0FBQ0EsTUFBTUssY0FBYzFFLE1BQU04QixTQUFOLEdBQWtCNkMsTUFBbEIsQ0FBeUJGLEtBQXpCLEVBQWdDRixLQUFoQyxFQUFwQjtBQUNBLE1BQU1XLGFBQWFsRixNQUFNSyxJQUFOLENBQVcsY0FBWCxFQUEyQjhFLEVBQTNCLENBQThCVCxXQUE5QixFQUEyQzNFLElBQTNDLENBQWdELFlBQWhELENBQW5CO0FBQ0EsTUFBTUEsT0FBTyxFQUFiO0FBQ0EsTUFBTXdDLFFBQVEsRUFBZDs7QUFFQUEsUUFBTTJDLFVBQU4sSUFBb0JqRixFQUFFLElBQUYsRUFBUU0sSUFBUixDQUFhLFNBQWIsSUFBMEIsQ0FBMUIsR0FBOEIsQ0FBbEQ7QUFDQVIsT0FBSzZDLFNBQUwsSUFBa0JMLEtBQWxCOztBQUVBdEMsSUFBRW1GLElBQUYsQ0FBT0gsR0FBUCxFQUFZO0FBQ1hsRixhQURXO0FBRVhzRixjQUFXaEUsSUFBSTZCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFGQSxHQUFaLEVBR0drQyxJQUhILENBR1Esb0JBQVk7QUFDbkJDLGNBQVd0RixFQUFFdUYsU0FBRixDQUFZRCxRQUFaLENBQVg7O0FBRUEsT0FBSUEsU0FBU0UsT0FBYixFQUFzQjtBQUNyQixRQUFNQyxVQUFVckUsSUFBSTZCLElBQUosQ0FBU3lDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix5QkFBeEIsRUFBbUQsa0JBQW5ELENBQWhCOztBQUVBO0FBQ0F2RSxRQUFJQyxJQUFKLENBQVN1RSxRQUFULENBQWtCQyxpQkFBbEIsQ0FBb0NKLE9BQXBDOztBQUVBO0FBQ0E7O0FBRUQsT0FBTUssUUFBUTFFLElBQUk2QixJQUFKLENBQVN5QyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isa0JBQXhCLEVBQTRDLGtCQUE1QyxDQUFkO0FBQ0EsT0FBTUksVUFBVTNFLElBQUk2QixJQUFKLENBQVN5QyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsWUFBeEIsRUFBc0Msa0JBQXRDLENBQWhCO0FBQ0EsT0FBTUssVUFBVSxDQUNmO0FBQ0NGLFdBQU8xRSxJQUFJNkIsSUFBSixDQUFTeUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGNBQXhCLEVBQXdDLGtCQUF4QyxDQURSO0FBRUNNLGNBQVU7QUFBQSxZQUFTakcsRUFBRUUsTUFBTWdHLGFBQVIsRUFBdUJwRixPQUF2QixDQUErQixRQUEvQixFQUF5Q3FGLEtBQXpDLENBQStDLE1BQS9DLENBQVQ7QUFBQTtBQUZYLElBRGUsQ0FBaEI7O0FBT0EvRSxPQUFJQyxJQUFKLENBQVM4RSxLQUFULENBQWVDLFdBQWYsQ0FBMkJOLEtBQTNCLEVBQWtDQyxPQUFsQyxFQUEyQ0MsT0FBM0M7QUFDQSxHQXpCRDtBQTBCQTs7QUFFRDs7Ozs7QUFLQSxVQUFTSywyQkFBVCxHQUF1QztBQUN0QyxNQUFNOUMsMkJBQTJCeEQsTUFBTUssSUFBTixDQUFXLCtDQUFYLENBQWpDO0FBQ0EsTUFBTWtHLFNBQVN2RyxNQUFNZSxPQUFOLENBQWMsc0JBQWQsRUFBc0NWLElBQXRDLENBQTJDLGVBQTNDLENBQWY7QUFDQSxNQUFNbUcsWUFBWUQsT0FBT2xHLElBQVAsQ0FBWSx3QkFBWixDQUFsQjtBQUNBLE1BQU1vRyxjQUFjcEYsSUFBSTZCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFDakIsK0RBREg7QUFFQSxNQUFNc0QsY0FBY3JGLElBQUk2QixJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQ2pCLDRFQURpQixHQUVqQi9CLElBQUk2QixJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFdBQXBCLENBRkg7QUFHQSxNQUFNdUQsYUFBYSxFQUFuQjs7QUFFQW5ELDJCQUF5Qi9CLElBQXpCLENBQThCLFlBQVc7QUFDeEMsT0FBSWEsS0FBS3JDLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLEVBQXNCaEIsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBVDtBQUNBNEcsY0FBV0MsSUFBWCxDQUFnQnRFLEVBQWhCO0FBQ0EsR0FIRDs7QUFLQSxNQUFNdkMsT0FBTztBQUNaQSxTQUFNLGVBRE07QUFFWjhHLGFBQVVGLFVBRkU7QUFHWnRCLGNBQVdoRSxJQUFJNkIsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUhDLEdBQWI7O0FBTUFtRCxTQUFPbEcsSUFBUCxDQUFZLGFBQVosRUFDRXlHLEtBREYsR0FFRUMsTUFGRix1Q0FFNkMxRixJQUFJNkIsSUFBSixDQUFTeUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGtCQUF4QixFQUE0QyxrQkFBNUMsQ0FGN0M7O0FBSUEzRixJQUFFbUYsSUFBRixDQUFPcUIsV0FBUCxFQUFvQjFHLElBQXBCLEVBQTBCdUYsSUFBMUIsQ0FBK0Isb0JBQVk7QUFDMUNDLGNBQVd0RixFQUFFdUYsU0FBRixDQUFZRCxRQUFaLENBQVg7O0FBRUEsT0FBSUEsU0FBU0UsT0FBYixFQUFzQjtBQUNyQixRQUFNdUIsZ0NBQThCTixXQUE5QixrRUFBc0dyRixJQUFJNkIsSUFBSixDQUFTeUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGlCQUF4QixFQUEyQyxrQkFBM0MsQ0FBdEcsU0FBTjs7QUFFQVcsV0FBT2xHLElBQVAsQ0FBWSxhQUFaLEVBQ0UrQixJQURGLHFDQUN5Q2YsSUFBSTZCLElBQUosQ0FBU3lDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixpQ0FBeEIsRUFBMkQsa0JBQTNELENBRHpDOztBQUdBWSxjQUFVUyxXQUFWLENBQXNCRCxlQUF0QjtBQUNBVCxXQUFPbEcsSUFBUCxDQUFZLGtCQUFaLEVBQWdDNkcsRUFBaEMsQ0FBbUMsT0FBbkMsRUFBNEMsWUFBVztBQUN0RFgsWUFBT0gsS0FBUCxDQUFhLE1BQWI7QUFDQSxLQUZEO0FBR0E7QUFFRCxHQWZEOztBQWlCQUcsU0FBT1csRUFBUCxDQUFVLGlCQUFWLEVBQTZCLFlBQVc7QUFDdkMsT0FBTVYsWUFBWUQsT0FBT2xHLElBQVAsQ0FBWSxtQkFBWixDQUFsQjtBQUNBLE9BQU04Ryw4RkFBNEY5RixJQUFJNkIsSUFBSixDQUFTeUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGlCQUF4QixFQUEyQyxrQkFBM0MsQ0FBNUYsY0FBTjs7QUFFQVksYUFBVVMsV0FBVixDQUFzQkUsZUFBdEI7QUFDQSxHQUxEO0FBTUE7O0FBRUQ7Ozs7O0FBS0EsVUFBU0MsK0JBQVQsR0FBMkM7QUFDMUNuSCxJQUFFLHlCQUFGLEVBQ0VGLElBREYsQ0FDTyxXQURQLEVBQ29CRSxFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixFQUFzQmhCLElBQXRCLENBQTJCLElBQTNCLENBRHBCLEVBRUVxRyxLQUZGLENBRVEsTUFGUjtBQUdBOztBQUdEO0FBQ0E7QUFDQTs7QUFFQXZHLFFBQU93SCxJQUFQLEdBQWMsVUFBUy9CLElBQVQsRUFBZTtBQUM1QnRGLFFBQ0VrSCxFQURGLENBQ0ssT0FETCxFQUNjLFVBRGQsRUFDMEJ6RyxnQkFEMUIsRUFFRXlHLEVBRkYsQ0FFSyxPQUZMLEVBRWMsV0FGZCxFQUUyQnJHLG9CQUYzQixFQUdFcUcsRUFIRixDQUdLLE9BSEwsRUFHYyxlQUhkLEVBRytCdkUsb0JBSC9CLEVBSUV1RSxFQUpGLENBSUssT0FKTCxFQUljLG1CQUpkLEVBSW1DRSwrQkFKbkMsRUFLRUYsRUFMRixDQUtLLFFBTEwsRUFLZSwwQkFMZixFQUsyQ2hILHNCQUwzQyxFQU1FZ0gsRUFORixDQU1LLE9BTkwsRUFNYyxxQkFOZCxFQU1xQ3BDLHFCQU5yQyxFQU9Fb0MsRUFQRixDQU9LLFFBUEwsRUFPZSxpQkFQZixFQU9rQ3BDLHFCQVBsQzs7QUFTQTlFLFFBQU1lLE9BQU4sQ0FBYyxzQkFBZCxFQUNFbUcsRUFERixDQUNLLFFBREwsRUFDZSx1Q0FEZixFQUN3RHRELHlCQUR4RCxFQUVFc0QsRUFGRixDQUVLLFFBRkwsRUFFZSxtQkFGZixFQUVvQ2xELGlCQUZwQyxFQUdFa0QsRUFIRixDQUdLLE9BSEwsRUFHYyxlQUhkLEVBRytCL0MsY0FIL0IsRUFJRStDLEVBSkYsQ0FJSyxPQUpMLEVBSWMsMkJBSmQsRUFJMkM1RCx3QkFKM0M7O0FBTUFyRCxJQUFFLE1BQUYsRUFDRWlILEVBREYsQ0FDSyxPQURMLEVBQ2Msd0JBRGQsRUFDd0NaLDJCQUR4Qzs7QUFHQXRHLFFBQU1rSCxFQUFOLENBQVMsU0FBVCxFQUFvQixZQUFNO0FBQ3pCbEgsU0FBTUssSUFBTixDQUFXLHNCQUFYLEVBQW1DNkcsRUFBbkMsQ0FBc0MsUUFBdEMsRUFBZ0RsQyx5QkFBaEQ7QUFDQXBCO0FBQ0EsR0FIRDs7QUFLQTBCO0FBQ0EsRUF6QkQ7O0FBMkJBLFFBQU96RixNQUFQO0FBQ0EsQ0E5YkYiLCJmaWxlIjoicXVpY2tfZWRpdC9vdmVydmlldy9ldmVudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGV2ZW50cy5qcyAyMDE4LTA0LTI0XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBNYWluIFRhYmxlIEV2ZW50c1xuICpcbiAqIEhhbmRsZXMgdGhlIGV2ZW50cyBvZiB0aGUgbWFpbiBRdWlja0VkaXQgdGFibGUuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZSgnZXZlbnRzJyxcblx0W1xuXHRcdCdsb2FkaW5nX3NwaW5uZXInLCAnbW9kYWwnLCBgJHtneC5zb3VyY2V9L2xpYnMvYnV0dG9uX2Ryb3Bkb3duYCwgYCR7Z3guc291cmNlfS9saWJzL2luZm9fYm94YFxuXHRdLFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEJ1bGsgc2VsZWN0aW9uIGNoYW5nZSBldmVudCBoYW5kbGVyLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqIEBwYXJhbSB7Qm9vbGVhbn0gW3Byb3BhZ2F0ZSA9IHRydWVdIFdoZXRoZXIgdG8gcHJvcGFnYXRlIHRoZSBldmVudCBvciBub3QuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uQnVsa1NlbGVjdGlvbkNoYW5nZShldmVudCwgcHJvcGFnYXRlID0gdHJ1ZSkge1xuXHRcdFx0aWYgKHByb3BhZ2F0ZSA9PT0gZmFsc2UpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkdGhpcy5maW5kKCd0Ym9keSBpbnB1dDpjaGVja2JveC5vdmVydmlldy1yb3ctc2VsZWN0aW9uJylcblx0XHRcdFx0LnNpbmdsZV9jaGVja2JveCgnY2hlY2tlZCcsICQodGhpcykucHJvcCgnY2hlY2tlZCcpKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFRhYmxlIHJvdyBjbGljayBldmVudCBoYW5kbGVyLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblRhYmxlUm93Q2xpY2soZXZlbnQpIHtcblx0XHRcdGlmICghJChldmVudC50YXJnZXQpLmlzKCd0ZCcpKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Y29uc3QgJHNpbmdsZUNoZWNrYm94ID0gJCh0aGlzKS5maW5kKCdpbnB1dDpjaGVja2JveC5vdmVydmlldy1yb3ctc2VsZWN0aW9uJyk7XG5cdFx0XHRcblx0XHRcdCRzaW5nbGVDaGVja2JveC5wcm9wKCdjaGVja2VkJywgISQodGhpcylcblx0XHRcdFx0LmZpbmQoJ2lucHV0OmNoZWNrYm94Lm92ZXJ2aWV3LXJvdy1zZWxlY3Rpb24nKVxuXHRcdFx0XHQucHJvcCgnY2hlY2tlZCcpKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEVuYWJsZXMgcm93IGVkaXRpbmcgbW9kZS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25UYWJsZVJvd0VkaXRDbGljaygpIHtcblx0XHRcdGNvbnN0ICR0YWJsZVJvdyA9ICQodGhpcykucGFyZW50cygndHInKTtcblx0XHRcdGNvbnN0ICRzaW5nbGVDaGVja2JveCA9ICQodGhpcykucGFyZW50cygndHInKS5maW5kKCdpbnB1dDpjaGVja2JveC5vdmVydmlldy1yb3ctc2VsZWN0aW9uJyk7XG5cdFx0XHRcblx0XHRcdGNvbnN0ICRkcm9wZG93biA9ICR0YWJsZVJvdy5maW5kKCcuYnRuLWdyb3VwLmRyb3Bkb3duJyk7XG5cdFx0XHRjb25zdCAkc2F2ZUFjdGlvbiA9ICRkcm9wZG93bi5maW5kKCcuc2F2ZS1yb3ctZWRpdHMnKTtcblx0XHRcdGNvbnN0ICRlZGl0QWN0aW9uID0gJHRhYmxlUm93LmZpbmQoJy5yb3ctZWRpdCcpO1xuXHRcdFx0JGVkaXRBY3Rpb24uYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0JHNhdmVBY3Rpb24ucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLnNldERlZmF1bHRBY3Rpb24oJGRyb3Bkb3duLCAkc2F2ZUFjdGlvbik7XG5cdFx0XHRcblx0XHRcdCR0YWJsZVJvdy5maW5kKCd0ZC5lZGl0YWJsZScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGlmICgkKHRoaXMpLmZpbmQoJ2lucHV0OnRleHQnKS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoJCh0aGlzKS5oYXNDbGFzcygnbWFudWZhY3R1cmVyJykpIHtcblx0XHRcdFx0XHRjb25zdCByb3dJbmRleCA9ICQodGhpcykucGFyZW50cygndHInKTtcblx0XHRcdFx0XHRjb25zdCByb3dEYXRhID0gJHRoaXMuRGF0YVRhYmxlKCkucm93KHJvd0luZGV4KS5kYXRhKCk7XG5cdFx0XHRcdFx0Y29uc3Qgb3B0aW9ucyA9IHJvd0RhdGEuRFRfUm93RGF0YS5vcHRpb24ubWFudWZhY3R1cmVyO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGxldCBodG1sID0gJyc7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0b3B0aW9ucy5mb3JFYWNoKG9wdGlvbiA9PiB7XG5cdFx0XHRcdFx0XHRodG1sICs9IGA8b3B0aW9uIHZhbHVlPVwiJHtvcHRpb24uaWR9XCIgJHtyb3dEYXRhLm1hbnVmYWN0dXJlciA9PSBvcHRpb24udmFsdWUgPyAnc2VsZWN0ZWQnIDogJyd9PlxuXHRcdFx0XHRcdFx0XHQke29wdGlvbi52YWx1ZX1cblx0XHRcdFx0XHRcdDwvb3B0aW9uPmA7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0dGhpcy5pbm5lckhUTUwgPSBgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbFwiPiR7aHRtbH08L3NlbGVjdD5gO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHRoaXMuaW5uZXJIVE1MID1cblx0XHRcdFx0XHRcdGA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHZhbHVlPVwiYCArIHRoaXMuaW5uZXJUZXh0LnJlcGxhY2UoL1wiL2csICcmcXVvdDsnKVxuXHRcdFx0XHRcdFx0KyBgXCIgLz5gO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCRzaW5nbGVDaGVja2JveC5wcm9wKCdjaGVja2VkJywgISQodGhpcylcblx0XHRcdFx0LmZpbmQoJ2lucHV0OmNoZWNrYm94Lm92ZXJ2aWV3LXJvdy1zZWxlY3Rpb24nKVxuXHRcdFx0XHQucHJvcCgnY2hlY2tlZCcpKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNob3cgcHJvZHVjdCBpY29uIGV2ZW50IGhhbmRsZXIuXG5cdFx0ICpcblx0XHQgKiBOYXZpZ2F0ZXMgdGhlIGJyb3dzZXIgdG8gdGhlIHByb2R1Y3QgZGV0YWlscyBwYWdlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblRhYmxlUm93U2hvd0NsaWNrKCkge1xuXHRcdFx0Y29uc3QgcHJvZHVjdElkID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoJ2lkJyk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IHBhcmFtZXRlcnMgPSB7XG5cdFx0XHRcdHBJRDogcHJvZHVjdElkLFxuXHRcdFx0XHRhY3Rpb246ICduZXdfcHJvZHVjdCcsXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHR3aW5kb3cub3BlbihgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYWRtaW4vY2F0ZWdvcmllcy5waHA/YCArICQucGFyYW0ocGFyYW1ldGVycyksICdfYmxhbmsnKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQnVsayByb3cgZWRpdCBldmVudCBoYW5kbGVyLlxuXHRcdCAqXG5cdFx0ICogRW5hYmxlcyB0aGUgZWRpdCBtb2RlIGZvciB0aGUgc2VsZWN0ZWQgcm93cy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25UYWJsZUJ1bGtSb3dFZGl0Q2xpY2soKSB7XG5cdFx0XHRjb25zdCAkYnVsa0VkaXRBY3Rpb24gPSAkKHRoaXMpO1xuXHRcdFx0Y29uc3QgJGNoZWNrZWRTaW5nbGVDaGVja2JveGVzID0gJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZC5vdmVydmlldy1yb3ctc2VsZWN0aW9uJyk7XG5cdFx0XHRcblx0XHRcdGlmICgkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMubGVuZ3RoKSB7XG5cdFx0XHRcdGNvbnN0ICRidWxrQWN0aW9uc0Ryb3Bkb3duID0gJCgnLm92ZXJ2aWV3LWJ1bGstYWN0aW9uJyk7XG5cdFx0XHRcdGNvbnN0ICRidWxrU2F2ZUFjdGlvbiA9ICRidWxrQWN0aW9uc0Ryb3Bkb3duLmZpbmQoJy5zYXZlLWJ1bGstcm93LWVkaXRzJyk7XG5cdFx0XHRcdCRidWxrRWRpdEFjdGlvbi5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcdCRidWxrU2F2ZUFjdGlvbi5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5zZXREZWZhdWx0QWN0aW9uKCRidWxrQWN0aW9uc0Ryb3Bkb3duLCAkYnVsa1NhdmVBY3Rpb24pO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0Y29uc3QgJHRhYmxlUm93ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpO1xuXHRcdFx0XHRjb25zdCAkcm93QWN0aW9uc0Ryb3Bkb3duID0gJHRhYmxlUm93LmZpbmQoJy5idG4tZ3JvdXAuZHJvcGRvd24nKTtcblx0XHRcdFx0Y29uc3QgJHNhdmVBY3Rpb24gPSAkcm93QWN0aW9uc0Ryb3Bkb3duLmZpbmQoJy5zYXZlLXJvdy1lZGl0cycpO1xuXHRcdFx0XHRjb25zdCAkZWRpdEFjdGlvbiA9ICR0YWJsZVJvdy5maW5kKCcucm93LWVkaXQnKTtcblx0XHRcdFx0XG5cdFx0XHRcdCRlZGl0QWN0aW9uLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0JHNhdmVBY3Rpb24ucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uc2V0RGVmYXVsdEFjdGlvbigkcm93QWN0aW9uc0Ryb3Bkb3duLCAkc2F2ZUFjdGlvbik7XG5cdFx0XHRcdFxuXHRcdFx0XHQkdGFibGVSb3cuZmluZCgndGQuZWRpdGFibGUnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmICgkKHRoaXMpLmZpbmQoJ2lucHV0OnRleHQnKS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmICgkKHRoaXMpLmhhc0NsYXNzKCdtYW51ZmFjdHVyZXInKSkge1xuXHRcdFx0XHRcdFx0Y29uc3Qgcm93SW5kZXggPSAkKHRoaXMpLnBhcmVudHMoJ3RyJyk7XG5cdFx0XHRcdFx0XHRjb25zdCByb3dEYXRhID0gJHRoaXMuRGF0YVRhYmxlKCkucm93KHJvd0luZGV4KS5kYXRhKCk7XG5cdFx0XHRcdFx0XHRjb25zdCBvcHRpb25zID0gcm93RGF0YS5EVF9Sb3dEYXRhLm9wdGlvbi5tYW51ZmFjdHVyZXI7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGxldCBodG1sID0gJyc7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdG9wdGlvbnMuZm9yRWFjaChvcHRpb24gPT4ge1xuXHRcdFx0XHRcdFx0XHRodG1sICs9XG5cdFx0XHRcdFx0XHRcdFx0YDxvcHRpb24gdmFsdWU9XCIke29wdGlvbi5pZH1cIiAke3Jvd0RhdGEubWFudWZhY3R1cmVyID09IG9wdGlvbi52YWx1ZSA/ICdzZWxlY3RlZCcgOiAnJ30+XG5cdFx0XHRcdFx0XHRcdCR7b3B0aW9uLnZhbHVlfVxuXHRcdFx0XHRcdFx0PC9vcHRpb24+YDtcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHR0aGlzLmlubmVySFRNTCA9IGA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sXCI+JHtodG1sfTwvc2VsZWN0PmA7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5pbm5lckhUTUwgPVxuXHRcdFx0XHRcdFx0XHRgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiB2YWx1ZT1cImAgKyB0aGlzLmlubmVyVGV4dC5yZXBsYWNlKC9cIi9nLCAnJnF1b3Q7Jylcblx0XHRcdFx0XHRcdFx0KyBgXCIgLz5gO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVGFibGUgcm93IGNoZWNrYm94IGNoYW5nZSBldmVudCBoYW5kbGVyLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblRhYmxlUm93Q2hlY2tib3hDaGFuZ2UoKSB7XG5cdFx0XHRjb25zdCAkYnVsa0FjdGlvbkRyb3Bkb3duQnV0dG9ucyA9ICR0aGlzLnBhcmVudHMoJy5xdWljay1lZGl0Lm92ZXJ2aWV3Jylcblx0XHRcdFx0LmZpbmQoJy5vdmVydmlldy1idWxrLWFjdGlvbiA+IGJ1dHRvbicpO1xuXHRcdFx0Y29uc3QgJHRhYmxlUm93ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZC5vdmVydmlldy1yb3ctc2VsZWN0aW9uJykubGVuZ3RoID4gMCkge1xuXHRcdFx0XHQkYnVsa0FjdGlvbkRyb3Bkb3duQnV0dG9ucy5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCRidWxrQWN0aW9uRHJvcGRvd25CdXR0b25zLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xuXHRcdFx0XHRcblx0XHRcdFx0Y29uc3QgJGJ1bGtBY3Rpb25zRHJvcGRvd24gPSAkKCcub3ZlcnZpZXctYnVsay1hY3Rpb24nKTtcblx0XHRcdFx0Y29uc3QgJGJ1bGtTYXZlQWN0aW9uID0gJGJ1bGtBY3Rpb25zRHJvcGRvd24uZmluZCgnLnNhdmUtYnVsay1yb3ctZWRpdHMnKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICghJGJ1bGtTYXZlQWN0aW9uLmhhc0NsYXNzKCdoaWRkZW4nKSkge1xuXHRcdFx0XHRcdGNvbnN0ICRidWxrRWRpdEFjdGlvbiA9ICRidWxrQWN0aW9uc0Ryb3Bkb3duLmZpbmQoJy5idWxrLXJvdy1lZGl0Jyk7XG5cdFx0XHRcdFx0JGJ1bGtFZGl0QWN0aW9uLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0XHQkYnVsa1NhdmVBY3Rpb24uYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5zZXREZWZhdWx0QWN0aW9uKCRidWxrQWN0aW9uc0Ryb3Bkb3duLCAkYnVsa0VkaXRBY3Rpb24pO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmICghJCh0aGlzKS5wcm9wKCdjaGVja2VkJykpIHtcblx0XHRcdFx0X3Jlc29sdmVSb3dDaGFuZ2VzKCQodGhpcykucGFyZW50cygndHInKSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRjb25zdCAkcm93QWN0aW9uc0Ryb3Bkb3duID0gJHRhYmxlUm93LmZpbmQoJy5idG4tZ3JvdXAuZHJvcGRvd24nKTtcblx0XHRcdFx0Y29uc3QgJHNhdmVBY3Rpb24gPSAkcm93QWN0aW9uc0Ryb3Bkb3duLmZpbmQoJy5zYXZlLXJvdy1lZGl0cycpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKCEkc2F2ZUFjdGlvbi5oYXNDbGFzcygnaGlkZGVuJykpIHtcblx0XHRcdFx0XHRjb25zdCAkZWRpdEFjdGlvbiA9ICR0YWJsZVJvdy5maW5kKCcucm93LWVkaXQnKTtcblx0XHRcdFx0XHQkZWRpdEFjdGlvbi5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcdFx0JHNhdmVBY3Rpb24uYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5zZXREZWZhdWx0QWN0aW9uKCRyb3dBY3Rpb25zRHJvcGRvd24sICRlZGl0QWN0aW9uLmxhc3QoKSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUGFnZSBtb2RlIGNoYW5nZSBiZXR3ZWVuIFwiZWRpdFwiIGFuZCBcImZpbHRlcmluZ1wiLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblBhZ2VNb2RlQ2hhbmdlKCkge1xuXHRcdFx0aWYgKCQodGhpcykudmFsKCkgPT0gJ2VkaXQtbW9kZScpIHtcblx0XHRcdFx0JHRoaXMuZmluZCgndHIuZmlsdGVyJykuYXR0cignaGlkZGVuJywgdHJ1ZSk7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3RyLmVkaXQnKS5hdHRyKCdoaWRkZW4nLCBmYWxzZSk7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3RoZWFkIHRyOmZpcnN0LWNoaWxkIHRoJykuYWRkQ2xhc3MoJ2VkaXQtbW9kZScpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0JHRoaXMuZmluZCgndHIuZmlsdGVyJykuYXR0cignaGlkZGVuJywgZmFsc2UpO1xuXHRcdFx0XHQkdGhpcy5maW5kKCd0ci5lZGl0JykuYXR0cignaGlkZGVuJywgdHJ1ZSk7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3RoZWFkIHRyOmZpcnN0LWNoaWxkIHRoJykucmVtb3ZlQ2xhc3MoJ2VkaXQtbW9kZScpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDYW5jZWwgZGF0YSBtb2RpZmljYXRpb25zIGV2ZW50IGhhbmRsZXIuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uQ2FuY2VsQ2xpY2soKSB7XG5cdFx0XHRjb25zdCAkcGFnZU1vZGUgPSAkKHRoaXMpLmNsb3Nlc3QoJ3RoZWFkJykuZmluZCgnLnNlbGVjdC1wYWdlLW1vZGUnKTtcblx0XHRcdGNvbnN0ICRjaGVja2VkU2luZ2xlQ2hlY2tib3hlcyA9ICR0aGlzLmZpbmQoJ2lucHV0OmNoZWNrYm94OmNoZWNrZWQub3ZlcnZpZXctcm93LXNlbGVjdGlvbicpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJHBhZ2VNb2RlLnZhbCgpID09ICdlZGl0LW1vZGUnKSB7XG5cdFx0XHRcdCRwYWdlTW9kZS52YWwoJ2ZpbHRlci1tb2RlJylcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCRwYWdlTW9kZS52YWwoJ2VkaXQtbW9kZScpXG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCRjaGVja2VkU2luZ2xlQ2hlY2tib3hlcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkKHRoaXMpLnByb3AoJ2NoZWNrZWQnLCAhJCh0aGlzKVxuXHRcdFx0XHRcdC5wcm9wKCdjaGVja2VkJykpXG5cdFx0XHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScpO1xuXHRcdFx0XHRcblx0XHRcdFx0X3Jlc29sdmVSb3dDaGFuZ2VzKCQodGhpcykucGFyZW50cygndHInKSk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0X29uUGFnZU1vZGVDaGFuZ2UoKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmVzdG9yZXMgYWxsIHRoZSByb3cgZGF0YSBjaGFuZ2VzIGJhY2sgdG8gdGhlaXIgb3JpZ2luYWwgc3RhdGUuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gJHJvdyBDdXJyZW50IHJvdyBzZWxlY3Rvci5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfcmVzb2x2ZVJvd0NoYW5nZXMoJHJvdykge1xuXHRcdFx0Y29uc3Qgcm93SW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5yb3coJHJvdykuaW5kZXgoKTtcblx0XHRcdFxuXHRcdFx0JHJvdy5maW5kKCdpbnB1dDp0ZXh0LCBzZWxlY3QnKS5ub3QoJy5zZWxlY3QtdGF4LCAuc2VsZWN0LXNoaXBwaW5nLXRpbWUnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRjb25zdCAkY2VsbCA9ICQodGhpcykuY2xvc2VzdCgndGQnKTtcblx0XHRcdFx0Y29uc3QgY29sdW1uSW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oJGNlbGwpLmluZGV4KCk7XG5cdFx0XHRcdHRoaXMucGFyZW50RWxlbWVudC5pbm5lckhUTUwgPSAkdGhpcy5EYXRhVGFibGUoKS5jZWxsKHJvd0luZGV4LCBjb2x1bW5JbmRleCkuZGF0YSgpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFRhYmxlIHJvdyBkYXRhIGNoYW5nZSBldmVudCBoYW5kbGVyLlxuXHRcdCAqXG5cdFx0ICogSXQncyBiZWluZyB0cmlnZ2VyZWQgZXZlcnkgdGltZSBhIHJvdyBpbnB1dC9zZWxlY3QgZmllbGQgaXMgY2hhbmdlZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25UYWJsZVJvd0RhdGFDaGFuZ2UoKSB7XG5cdFx0XHRjb25zdCAkcm93ID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpO1xuXHRcdFx0Y29uc3Qgcm93SW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5yb3coJHJvdykuaW5kZXgoKTtcblx0XHRcdFxuXHRcdFx0JHJvdy5maW5kKCdpbnB1dDp0ZXh0LCBzZWxlY3QnKS5ub3QoJy5zZWxlY3QtdGF4LCAuc2VsZWN0LXNoaXBwaW5nLXRpbWUnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRjb25zdCAkY2VsbCA9ICQodGhpcykuY2xvc2VzdCgndGQnKTtcblx0XHRcdFx0Y29uc3QgY29sdW1uSW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oJGNlbGwpLmluZGV4KCk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoJC50cmltKCQodGhpcykudmFsKCkpICE9ICR0aGlzLkRhdGFUYWJsZSgpLmNlbGwocm93SW5kZXgsIGNvbHVtbkluZGV4KS5kYXRhKCkpIHtcblx0XHRcdFx0XHQkKHRoaXMpLmFkZENsYXNzKCdtb2RpZmllZCcpO1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0JCh0aGlzKS5yZW1vdmVDbGFzcygnbW9kaWZpZWQnKTtcblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBUYWJsZSByb3cgc3dpdGNoZXIgY2hhbmdlIGV2ZW50IGhhbmRsZXIuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBzd2l0Y2hlciBDdXJyZW50IHN3aXRjaGVyIGVsZW1lbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uVGFibGVSb3dTd2l0Y2hlckNoYW5nZSgpIHtcblx0XHRcdGNvbnN0IHVybCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89UXVpY2tFZGl0T3ZlcnZpZXdBamF4L1VwZGF0ZSc7XG5cdFx0XHRjb25zdCBwcm9kdWN0SWQgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKTtcblx0XHRcdGNvbnN0ICRjZWxsID0gJCh0aGlzKS5jbG9zZXN0KCd0ZCcpO1xuXHRcdFx0Y29uc3QgY29sdW1uSW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oJGNlbGwpLmluZGV4KCk7XG5cdFx0XHRjb25zdCBjb2x1bW5OYW1lID0gJHRoaXMuZmluZCgndHIuZmlsdGVyIHRoJykuZXEoY29sdW1uSW5kZXgpLmRhdGEoJ2NvbHVtbk5hbWUnKTtcblx0XHRcdGNvbnN0IGRhdGEgPSB7fTtcblx0XHRcdGNvbnN0IHZhbHVlID0ge307XG5cdFx0XHRcblx0XHRcdHZhbHVlW2NvbHVtbk5hbWVdID0gJCh0aGlzKS5wcm9wKCdjaGVja2VkJykgPyAxIDogMDtcblx0XHRcdGRhdGFbcHJvZHVjdElkXSA9IHZhbHVlO1xuXHRcdFx0XG5cdFx0XHQkLnBvc3QodXJsLCB7XG5cdFx0XHRcdGRhdGEsXG5cdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJyksXG5cdFx0XHR9KS5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0cmVzcG9uc2UgPSAkLnBhcnNlSlNPTihyZXNwb25zZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0XHRcdGNvbnN0IGNvbnRlbnQgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnU1VDQ0VTU19QUk9EVUNUX1VQREFURUQnLCAnYWRtaW5fcXVpY2tfZWRpdCcpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFNob3cgc3VjY2VzcyBtZXNzYWdlIGluIHRoZSBhZG1pbiBpbmZvIGJveC5cblx0XHRcdFx0XHRqc2UubGlicy5pbmZvX2JveC5hZGRTdWNjZXNzTWVzc2FnZShjb250ZW50KTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGNvbnN0IHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ01PREFMX1RJVExFX05PREUnLCAnYWRtaW5fcXVpY2tfZWRpdCcpO1xuXHRcdFx0XHRjb25zdCBtZXNzYWdlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0VESVRfRVJST1InLCAnYWRtaW5fcXVpY2tfZWRpdCcpO1xuXHRcdFx0XHRjb25zdCBidXR0b25zID0gW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX0NMT1NFJywgJ2FkbWluX3F1aWNrX2VkaXQnKSxcblx0XHRcdFx0XHRcdGNhbGxiYWNrOiBldmVudCA9PiAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLnBhcmVudHMoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJylcblx0XHRcdFx0XHR9XG5cdFx0XHRcdF07XG5cdFx0XHRcdFxuXHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZSh0aXRsZSwgbWVzc2FnZSwgYnV0dG9ucyk7XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlIGludmVudG9yeSBsaXN0IGNsaWNrIGV2ZW50IGhhbmRsZXIuXG5cdFx0ICpcblx0XHQgKiBHZW5lcmF0ZXMgYW4gaW52ZW50b3J5IGxpc3QgUERGIGZyb20gdGhlIHNlbGVjdGVkIHJvd3Mgb3IgZnJvbSBhbGwgdGhlIHJlY29yZHMgaWYgbm8gcm93IGlzIHNlbGVjdGVkLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkNyZWF0ZUludmVudG9yeUxpc3RDbGljaygpIHtcblx0XHRcdGNvbnN0ICRjaGVja2VkU2luZ2xlQ2hlY2tib3hlcyA9ICR0aGlzLmZpbmQoJ2lucHV0OmNoZWNrYm94OmNoZWNrZWQub3ZlcnZpZXctcm93LXNlbGVjdGlvbicpO1xuXHRcdFx0Y29uc3QgJG1vZGFsID0gJHRoaXMucGFyZW50cygnLnF1aWNrLWVkaXQub3ZlcnZpZXcnKS5maW5kKCdkaXYuZG93bmxvYWRzJyk7XG5cdFx0XHRjb25zdCAkZG93bmxvYWQgPSAkbW9kYWwuZmluZCgnYnV0dG9uLmRvd25sb2FkLWJ1dHRvbicpO1xuXHRcdFx0Y29uc3QgZ2VuZXJhdGVVcmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKVxuXHRcdFx0XHQrICcvYWRtaW4vYWRtaW4ucGhwP2RvPVF1aWNrRWRpdE92ZXJ2aWV3QWpheC9DcmVhdGVJbnZlbnRvcnlGaWxlJztcblx0XHRcdGNvbnN0IGRvd25sb2FkVXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJylcblx0XHRcdFx0KyAnL2FkbWluL2FkbWluLnBocD9kbz1RdWlja0VkaXRPdmVydmlld0FqYXgvRG93bmxvYWRJbnZlbnRvcnlGaWxlJnBhZ2VUb2tlbj0nXG5cdFx0XHRcdCsganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJyk7XG5cdFx0XHRjb25zdCBwcm9kdWN0c0lkID0gW107XG5cdFx0XHRcblx0XHRcdCRjaGVja2VkU2luZ2xlQ2hlY2tib3hlcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRsZXQgaWQgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKTtcblx0XHRcdFx0cHJvZHVjdHNJZC5wdXNoKGlkKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHRkYXRhOiAnaW52ZW50b3J5TGlzdCcsXG5cdFx0XHRcdHByb2R1Y3RzOiBwcm9kdWN0c0lkLFxuXHRcdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQkbW9kYWwuZmluZCgnLm1vZGFsLWJvZHknKVxuXHRcdFx0XHQuZW1wdHkoKVxuXHRcdFx0XHQuYXBwZW5kKGA8cD48aSBjbGFzcz1cImZhIGZhLWNsb2NrLW9cIj48L2k+ICR7anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfUExFQVNFX1dBSVQnLCAnYWRtaW5fcXVpY2tfZWRpdCcpfS4uLjwvcD5gKTtcblx0XHRcdFxuXHRcdFx0JC5wb3N0KGdlbmVyYXRlVXJsLCBkYXRhKS5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0cmVzcG9uc2UgPSAkLnBhcnNlSlNPTihyZXNwb25zZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0XHRcdGNvbnN0ICRkb2N1bWVudFRhcmdldCA9IGA8YSBocmVmPVwiJHtkb3dubG9hZFVybH1cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBkb3dubG9hZC1idXR0b25cIiB0YXJnZXQ9XCJfYmxhbmtcIj4ke2pzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fRE9XTkxPQUQnLCAnYWRtaW5fcXVpY2tfZWRpdCcpfTwvYT5gO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcubW9kYWwtYm9keScpXG5cdFx0XHRcdFx0XHQuaHRtbChgPHA+PGkgY2xhc3M9XCJmYSBmYS1jaGVja1wiPjwvaT4gJHtqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9SRUFEWV9UT19ET1dOTE9BRF9ET0NVTUVOVCcsICdhZG1pbl9xdWlja19lZGl0Jyl9Li4uPC9wPmApO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCRkb3dubG9hZC5yZXBsYWNlV2l0aCgkZG9jdW1lbnRUYXJnZXQpO1xuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcuZG93bmxvYWQtYnV0dG9uJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHQkbW9kYWwubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCRtb2RhbC5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGNvbnN0ICRkb3dubG9hZCA9ICRtb2RhbC5maW5kKCdhLmRvd25sb2FkLWJ1dHRvbicpO1xuXHRcdFx0XHRjb25zdCAkZG93bmxvYWRCdXR0b24gPSBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgZG93bmxvYWQtYnV0dG9uXCIgZGlzYWJsZWQ+JHtqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX0RPV05MT0FEJywgJ2FkbWluX3F1aWNrX2VkaXQnKX08L2J1dHRvbj5gO1xuXHRcdFx0XHRcblx0XHRcdFx0JGRvd25sb2FkLnJlcGxhY2VXaXRoKCRkb3dubG9hZEJ1dHRvbik7XG5cdFx0XHR9KVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBEaXNwbGF5IGdyYWR1YXRlZCBwcmljZXMgbWFuYWdlbWVudCBtb2RhbC5cblx0XHQgKlxuXHRcdCAqIE5vdGU6IEN1cnJlbnQgcHJvZHVjdCBJRCBtdXN0IGJlIHNldCBhcyBhIGRhdGEgXCJwcm9kdWN0SWRcIiB2YWx1ZSBpbiB0aGUgbW9kYWwgY29udGFpbmVyIGVsZW1lbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uVGFibGVSb3dHcmFkdWF0ZWRQcmljZXNDbGljaygpIHtcblx0XHRcdCQoJy5ncmFkdWF0ZWQtcHJpY2VzLm1vZGFsJylcblx0XHRcdFx0LmRhdGEoJ3Byb2R1Y3RJZCcsICQodGhpcykucGFyZW50cygndHInKS5kYXRhKCdpZCcpKVxuXHRcdFx0XHQubW9kYWwoJ3Nob3cnKTtcblx0XHR9XG5cdFx0XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCR0aGlzXG5cdFx0XHRcdC5vbignY2xpY2snLCAndGJvZHkgdHInLCBfb25UYWJsZVJvd0NsaWNrKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5yb3ctZWRpdCcsIF9vblRhYmxlUm93RWRpdENsaWNrKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5zaG93LXByb2R1Y3QnLCBfb25UYWJsZVJvd1Nob3dDbGljaylcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuZ3JhZHVhdGVkLXByaWNlcycsIF9vblRhYmxlUm93R3JhZHVhdGVkUHJpY2VzQ2xpY2spXG5cdFx0XHRcdC5vbignY2hhbmdlJywgJy5vdmVydmlldy1idWxrLXNlbGVjdGlvbicsIF9vbkJ1bGtTZWxlY3Rpb25DaGFuZ2UpXG5cdFx0XHRcdC5vbigna2V5dXAnLCAndGJvZHkgdHIgaW5wdXQ6dGV4dCcsIF9vblRhYmxlUm93RGF0YUNoYW5nZSlcblx0XHRcdFx0Lm9uKCdjaGFuZ2UnLCAndGJvZHkgdHIgc2VsZWN0JywgX29uVGFibGVSb3dEYXRhQ2hhbmdlKTtcblx0XHRcdFxuXHRcdFx0JHRoaXMucGFyZW50cygnLnF1aWNrLWVkaXQub3ZlcnZpZXcnKVxuXHRcdFx0XHQub24oJ2NoYW5nZScsICdpbnB1dDpjaGVja2JveC5vdmVydmlldy1yb3ctc2VsZWN0aW9uJywgX29uVGFibGVSb3dDaGVja2JveENoYW5nZSlcblx0XHRcdFx0Lm9uKCdjaGFuZ2UnLCAnLnNlbGVjdC1wYWdlLW1vZGUnLCBfb25QYWdlTW9kZUNoYW5nZSlcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuY2FuY2VsLWVkaXRzJywgX29uQ2FuY2VsQ2xpY2spXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmJ0bi1ncm91cCAuYnVsay1yb3ctZWRpdCcsIF9vblRhYmxlQnVsa1Jvd0VkaXRDbGljayk7XG5cdFx0XHRcblx0XHRcdCQoJ2JvZHknKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5jcmVhdGUtaW52ZW50b3J5LWxpc3QnLCBfb25DcmVhdGVJbnZlbnRvcnlMaXN0Q2xpY2spO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5vbignZHJhdy5kdCcsICgpID0+IHtcblx0XHRcdFx0JHRoaXMuZmluZCgnLmNvbnZlcnQtdG8tc3dpdGNoZXInKS5vbignY2hhbmdlJywgX29uVGFibGVSb3dTd2l0Y2hlckNoYW5nZSk7XG5cdFx0XHRcdF9vblRhYmxlUm93Q2hlY2tib3hDaGFuZ2UoKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcblxuIl19
