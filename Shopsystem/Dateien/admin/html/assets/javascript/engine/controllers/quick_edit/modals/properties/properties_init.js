'use strict';

/* --------------------------------------------------------------
 init.js 2016-08-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Table Initialization Controller
 *
 * This controller initializes the main QuickEdit properties table with a new jQuery DataTables instance.
 */
gx.controllers.module('properties_init', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', 'datatable', gx.source + '/libs/quick_edit_properties_overview_columns'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Selected Product IDs
  *
  * @type {Number[]}
  */
	var productIds = [0];

	/**
  * Properties Modal Selector
  *
  * @type {jQuery}
  */
	var $modal = $this.parents('.modal');

	/**
  * DataTable Columns
  *
  * @type {Object[]}
  */
	var columns = jse.libs.datatable.prepareColumns($this, jse.libs.quick_edit_properties_overview_columns, data.propertiesActiveColumns);

	/**
  * DataTable Options
  *
  * @type {Object}
  */
	var options = {
		autoWidth: false,
		dom: 't',
		pageLength: data.pageLength,
		displayStart: 0,
		serverSide: true,
		language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
		ajax: {
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditProductPropertiesAjax/DataTable',
			type: 'POST',
			data: function data(_data) {
				_data.productId = _getProductIds();
				_data.pageToken = jse.core.config.get('pageToken');
				return _data;
			}
		},
		orderCellsTop: true,
		order: _getOrder(),
		columns: columns,
		createdRow: function createdRow(row, data, dataIndex) {
			if (data.combiPriceType === 'fix') {
				$(row).find('td.combi-price').addClass('editable');
			}
		}
	};

	/**
  * Loading Spinner 
  * 
  * @type {jQuery}
  */
	var $spinner = null;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Initial Table Order
  *
  * @return {Object[]} Returns the ordered column definitions.
  */
	function _getOrder() {
		var index = 1; // Order by first column by default.
		var direction = 'asc'; // Order ASC by default.
		var columnName = 'productsName'; // Order by products name by default.

		$this.on('click', 'th', function () {
			columnName = $(this).data('column-name');
			index = $this.DataTable().column(this).index();
		});

		// Apply initial table sort.
		if (data.propertiesActiveColumns.indexOf('productsName') > -1) {
			// Order by name if possible.
			index = data.propertiesActiveColumns.indexOf('productsName');
		}

		return [[index, direction]];
	}

	/**
  * Set current product IDs.
  *
  * @param {jQuery.Event} event Contains event information.
  */
	function _setProductIds(event) {
		productIds = [];

		if ($(event.target).is('a.products-properties')) {
			productIds.push($(this).parents('tr').data('id'));

			return;
		}

		var $singleCheckboxes = $this.parents('.quick-edit.overview').find('input:checkbox:checked.overview-row-selection');

		$singleCheckboxes.each(function () {
			productIds.push($(this).parents('tr').data('id'));
		});
	}

	/**
  * Get Product IDs.
  *
  * @return {Number[]} Returns the product IDs.
  */
	function _getProductIds() {
		return productIds;
	}

	/**
  * Properties modal shown event handler.
  */
	function _onModalShown() {
		$(window).trigger('resize');

		if (!$.fn.DataTable.isDataTable($this)) {
			jse.libs.datatable.create($this, options);

			return;
		}

		$this.DataTable().ajax.reload();
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$modal.on('show.bs.modal', function () {
			$(this).find('.modal-dialog').css('z-index', 1060);
		}).on('shown.bs.modal', _onModalShown).on('hide.bs.modal', function () {
			$(this).find('.modal-dialog').css('z-index', 0);
		});

		$this.parents('.quick-edit.overview').on('click', 'a.products-properties', _setProductIds).on('click', 'a.bulk-properties-edit', _setProductIds);

		$this.on('draw.dt', function () {
			$this.find('thead input:checkbox').prop('checked', false).trigger('change', [false]);
			$this.find('tbody').attr('data-gx-widget', 'single_checkbox');
			$this.find('tbody').attr('data-single_checkbox-selector', '.properties-row-selection');

			gx.widgets.init($this);
		});

		$this.on('preXhr.dt', function () {
			return $spinner = jse.libs.loading_spinner.show($modal.find('.modal-content'), $modal.css('z-index'));
		});
		$this.on('xhr.dt', function () {
			return jse.libs.loading_spinner.hide($spinner);
		});

		jse.libs.datatable.create($this, options);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc19pbml0LmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwicHJvZHVjdElkcyIsIiRtb2RhbCIsInBhcmVudHMiLCJjb2x1bW5zIiwibGlicyIsImRhdGF0YWJsZSIsInByZXBhcmVDb2x1bW5zIiwicXVpY2tfZWRpdF9wcm9wZXJ0aWVzX292ZXJ2aWV3X2NvbHVtbnMiLCJwcm9wZXJ0aWVzQWN0aXZlQ29sdW1ucyIsIm9wdGlvbnMiLCJhdXRvV2lkdGgiLCJkb20iLCJwYWdlTGVuZ3RoIiwiZGlzcGxheVN0YXJ0Iiwic2VydmVyU2lkZSIsImxhbmd1YWdlIiwiZ2V0VHJhbnNsYXRpb25zIiwiY29yZSIsImNvbmZpZyIsImdldCIsImFqYXgiLCJ1cmwiLCJ0eXBlIiwicHJvZHVjdElkIiwiX2dldFByb2R1Y3RJZHMiLCJwYWdlVG9rZW4iLCJvcmRlckNlbGxzVG9wIiwib3JkZXIiLCJfZ2V0T3JkZXIiLCJjcmVhdGVkUm93Iiwicm93IiwiZGF0YUluZGV4IiwiY29tYmlQcmljZVR5cGUiLCJmaW5kIiwiYWRkQ2xhc3MiLCIkc3Bpbm5lciIsImluZGV4IiwiZGlyZWN0aW9uIiwiY29sdW1uTmFtZSIsIm9uIiwiRGF0YVRhYmxlIiwiY29sdW1uIiwiaW5kZXhPZiIsIl9zZXRQcm9kdWN0SWRzIiwiZXZlbnQiLCJ0YXJnZXQiLCJpcyIsInB1c2giLCIkc2luZ2xlQ2hlY2tib3hlcyIsImVhY2giLCJfb25Nb2RhbFNob3duIiwid2luZG93IiwidHJpZ2dlciIsImZuIiwiaXNEYXRhVGFibGUiLCJjcmVhdGUiLCJyZWxvYWQiLCJpbml0IiwiZG9uZSIsImNzcyIsInByb3AiLCJhdHRyIiwid2lkZ2V0cyIsImxvYWRpbmdfc3Bpbm5lciIsInNob3ciLCJoaWRlIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLGlCQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixtREFFSUQsSUFBSUMsTUFGUixrREFHQyxXQUhELEVBSUlKLEdBQUdJLE1BSlAsa0RBSEQsRUFVQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNTCxTQUFTLEVBQWY7O0FBRUE7Ozs7O0FBS0EsS0FBSU0sYUFBYSxDQUFDLENBQUQsQ0FBakI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsU0FBU0gsTUFBTUksT0FBTixDQUFjLFFBQWQsQ0FBZjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxVQUFVUixJQUFJUyxJQUFKLENBQVNDLFNBQVQsQ0FBbUJDLGNBQW5CLENBQWtDUixLQUFsQyxFQUF5Q0gsSUFBSVMsSUFBSixDQUFTRyxzQ0FBbEQsRUFBMEZWLEtBQUtXLHVCQUEvRixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxVQUFVO0FBQ2ZDLGFBQVcsS0FESTtBQUVmQyxPQUFLLEdBRlU7QUFHZkMsY0FBWWYsS0FBS2UsVUFIRjtBQUlmQyxnQkFBYyxDQUpDO0FBS2ZDLGNBQVksSUFMRztBQU1mQyxZQUFVcEIsSUFBSVMsSUFBSixDQUFTQyxTQUFULENBQW1CVyxlQUFuQixDQUFtQ3JCLElBQUlzQixJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLENBQW5DLENBTks7QUFPZkMsUUFBTTtBQUNMQyxRQUFLMUIsSUFBSXNCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsOERBRGhDO0FBRUxHLFNBQU0sTUFGRDtBQUdMekIsU0FBTSxjQUFDQSxLQUFELEVBQVU7QUFDZkEsVUFBSzBCLFNBQUwsR0FBaUJDLGdCQUFqQjtBQUNBM0IsVUFBSzRCLFNBQUwsR0FBaUI5QixJQUFJc0IsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQixDQUFqQjtBQUNBLFdBQU90QixLQUFQO0FBQ0E7QUFQSSxHQVBTO0FBZ0JmNkIsaUJBQWUsSUFoQkE7QUFpQmZDLFNBQU9DLFdBakJRO0FBa0JmekIsV0FBU0EsT0FsQk07QUFtQmYwQixZQW5CZSxzQkFtQkpDLEdBbkJJLEVBbUJDakMsSUFuQkQsRUFtQk9rQyxTQW5CUCxFQW1Ca0I7QUFDaEMsT0FBSWxDLEtBQUttQyxjQUFMLEtBQXdCLEtBQTVCLEVBQW1DO0FBQ2xDakMsTUFBRStCLEdBQUYsRUFBT0csSUFBUCxDQUFZLGdCQUFaLEVBQThCQyxRQUE5QixDQUF1QyxVQUF2QztBQUNBO0FBQ0Q7QUF2QmMsRUFBaEI7O0FBMEJBOzs7OztBQUtBLEtBQUlDLFdBQVcsSUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU1AsU0FBVCxHQUFxQjtBQUNwQixNQUFJUSxRQUFRLENBQVosQ0FEb0IsQ0FDTDtBQUNmLE1BQUlDLFlBQVksS0FBaEIsQ0FGb0IsQ0FFRztBQUN2QixNQUFJQyxhQUFhLGNBQWpCLENBSG9CLENBR2E7O0FBRWpDeEMsUUFBTXlDLEVBQU4sQ0FBUyxPQUFULEVBQWtCLElBQWxCLEVBQXdCLFlBQVc7QUFDbENELGdCQUFhdkMsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxhQUFiLENBQWI7QUFDQXVDLFdBQVF0QyxNQUFNMEMsU0FBTixHQUFrQkMsTUFBbEIsQ0FBeUIsSUFBekIsRUFBK0JMLEtBQS9CLEVBQVI7QUFDQSxHQUhEOztBQUtBO0FBQ0EsTUFBSXZDLEtBQUtXLHVCQUFMLENBQTZCa0MsT0FBN0IsQ0FBcUMsY0FBckMsSUFBdUQsQ0FBQyxDQUE1RCxFQUErRDtBQUFFO0FBQ2hFTixXQUFRdkMsS0FBS1csdUJBQUwsQ0FBNkJrQyxPQUE3QixDQUFxQyxjQUFyQyxDQUFSO0FBQ0E7O0FBRUQsU0FBTyxDQUFDLENBQUNOLEtBQUQsRUFBUUMsU0FBUixDQUFELENBQVA7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTTSxjQUFULENBQXdCQyxLQUF4QixFQUErQjtBQUM5QjVDLGVBQWEsRUFBYjs7QUFFQSxNQUFJRCxFQUFFNkMsTUFBTUMsTUFBUixFQUFnQkMsRUFBaEIsQ0FBbUIsdUJBQW5CLENBQUosRUFBaUQ7QUFDaEQ5QyxjQUFXK0MsSUFBWCxDQUFnQmhELEVBQUUsSUFBRixFQUFRRyxPQUFSLENBQWdCLElBQWhCLEVBQXNCTCxJQUF0QixDQUEyQixJQUEzQixDQUFoQjs7QUFFQTtBQUNBOztBQUVELE1BQU1tRCxvQkFBb0JsRCxNQUFNSSxPQUFOLENBQWMsc0JBQWQsRUFDeEIrQixJQUR3QixDQUNuQiwrQ0FEbUIsQ0FBMUI7O0FBR0FlLG9CQUFrQkMsSUFBbEIsQ0FBdUIsWUFBVztBQUNqQ2pELGNBQVcrQyxJQUFYLENBQWdCaEQsRUFBRSxJQUFGLEVBQVFHLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JMLElBQXRCLENBQTJCLElBQTNCLENBQWhCO0FBQ0EsR0FGRDtBQUdBOztBQUVEOzs7OztBQUtBLFVBQVMyQixjQUFULEdBQTBCO0FBQ3pCLFNBQU94QixVQUFQO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNrRCxhQUFULEdBQXlCO0FBQ3hCbkQsSUFBRW9ELE1BQUYsRUFBVUMsT0FBVixDQUFrQixRQUFsQjs7QUFFQSxNQUFJLENBQUNyRCxFQUFFc0QsRUFBRixDQUFLYixTQUFMLENBQWVjLFdBQWYsQ0FBMkJ4RCxLQUEzQixDQUFMLEVBQXdDO0FBQ3ZDSCxPQUFJUyxJQUFKLENBQVNDLFNBQVQsQ0FBbUJrRCxNQUFuQixDQUEwQnpELEtBQTFCLEVBQWlDVyxPQUFqQzs7QUFFQTtBQUNBOztBQUVEWCxRQUFNMEMsU0FBTixHQUFrQnBCLElBQWxCLENBQXVCb0MsTUFBdkI7QUFDQTs7QUFHRDtBQUNBO0FBQ0E7O0FBRUE5RCxRQUFPK0QsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QnpELFNBQ0VzQyxFQURGLENBQ0ssZUFETCxFQUNzQixZQUFXO0FBQy9CeEMsS0FBRSxJQUFGLEVBQVFrQyxJQUFSLENBQWEsZUFBYixFQUE4QjBCLEdBQTlCLENBQWtDLFNBQWxDLEVBQTZDLElBQTdDO0FBQ0EsR0FIRixFQUlFcEIsRUFKRixDQUlLLGdCQUpMLEVBSXVCVyxhQUp2QixFQUtFWCxFQUxGLENBS0ssZUFMTCxFQUtzQixZQUFXO0FBQy9CeEMsS0FBRSxJQUFGLEVBQVFrQyxJQUFSLENBQWEsZUFBYixFQUE4QjBCLEdBQTlCLENBQWtDLFNBQWxDLEVBQTZDLENBQTdDO0FBQ0EsR0FQRjs7QUFTQTdELFFBQU1JLE9BQU4sQ0FBYyxzQkFBZCxFQUNFcUMsRUFERixDQUNLLE9BREwsRUFDYyx1QkFEZCxFQUN1Q0ksY0FEdkMsRUFFRUosRUFGRixDQUVLLE9BRkwsRUFFYyx3QkFGZCxFQUV3Q0ksY0FGeEM7O0FBSUE3QyxRQUFNeUMsRUFBTixDQUFTLFNBQVQsRUFBb0IsWUFBTTtBQUN6QnpDLFNBQU1tQyxJQUFOLENBQVcsc0JBQVgsRUFDRTJCLElBREYsQ0FDTyxTQURQLEVBQ2tCLEtBRGxCLEVBRUVSLE9BRkYsQ0FFVSxRQUZWLEVBRW9CLENBQUMsS0FBRCxDQUZwQjtBQUdBdEQsU0FBTW1DLElBQU4sQ0FBVyxPQUFYLEVBQW9CNEIsSUFBcEIsQ0FBeUIsZ0JBQXpCLEVBQTJDLGlCQUEzQztBQUNBL0QsU0FBTW1DLElBQU4sQ0FBVyxPQUFYLEVBQW9CNEIsSUFBcEIsQ0FBeUIsK0JBQXpCLEVBQTBELDJCQUExRDs7QUFFQXJFLE1BQUdzRSxPQUFILENBQVdMLElBQVgsQ0FBZ0IzRCxLQUFoQjtBQUNBLEdBUkQ7O0FBVUFBLFFBQU15QyxFQUFOLENBQVMsV0FBVCxFQUFzQjtBQUFBLFVBQU1KLFdBQzNCeEMsSUFBSVMsSUFBSixDQUFTMkQsZUFBVCxDQUF5QkMsSUFBekIsQ0FBOEIvRCxPQUFPZ0MsSUFBUCxDQUFZLGdCQUFaLENBQTlCLEVBQTZEaEMsT0FBTzBELEdBQVAsQ0FBVyxTQUFYLENBQTdELENBRHFCO0FBQUEsR0FBdEI7QUFFQTdELFFBQU15QyxFQUFOLENBQVMsUUFBVCxFQUFtQjtBQUFBLFVBQU01QyxJQUFJUyxJQUFKLENBQVMyRCxlQUFULENBQXlCRSxJQUF6QixDQUE4QjlCLFFBQTlCLENBQU47QUFBQSxHQUFuQjs7QUFFQXhDLE1BQUlTLElBQUosQ0FBU0MsU0FBVCxDQUFtQmtELE1BQW5CLENBQTBCekQsS0FBMUIsRUFBaUNXLE9BQWpDOztBQUVBaUQ7QUFDQSxFQS9CRDs7QUFpQ0EsUUFBT2hFLE1BQVA7QUFDQSxDQTNNRiIsImZpbGUiOiJxdWlja19lZGl0L21vZGFscy9wcm9wZXJ0aWVzL3Byb3BlcnRpZXNfaW5pdC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gaW5pdC5qcyAyMDE2LTA4LTIzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBQcm9wZXJ0aWVzIFRhYmxlIEluaXRpYWxpemF0aW9uIENvbnRyb2xsZXJcbiAqXG4gKiBUaGlzIGNvbnRyb2xsZXIgaW5pdGlhbGl6ZXMgdGhlIG1haW4gUXVpY2tFZGl0IHByb3BlcnRpZXMgdGFibGUgd2l0aCBhIG5ldyBqUXVlcnkgRGF0YVRhYmxlcyBpbnN0YW5jZS5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQncHJvcGVydGllc19pbml0Jyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvZGF0YXRhYmxlcy9qcXVlcnkuZGF0YVRhYmxlcy5taW4uY3NzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvZGF0YXRhYmxlcy9qcXVlcnkuZGF0YVRhYmxlcy5taW4uanNgLFxuXHRcdCdkYXRhdGFibGUnLFxuXHRcdGAke2d4LnNvdXJjZX0vbGlicy9xdWlja19lZGl0X3Byb3BlcnRpZXNfb3ZlcnZpZXdfY29sdW1uc2Bcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2VsZWN0ZWQgUHJvZHVjdCBJRHNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtOdW1iZXJbXX1cblx0XHQgKi9cblx0XHRsZXQgcHJvZHVjdElkcyA9IFswXTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBQcm9wZXJ0aWVzIE1vZGFsIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRtb2RhbCA9ICR0aGlzLnBhcmVudHMoJy5tb2RhbCcpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERhdGFUYWJsZSBDb2x1bW5zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0W119XG5cdFx0ICovXG5cdFx0Y29uc3QgY29sdW1ucyA9IGpzZS5saWJzLmRhdGF0YWJsZS5wcmVwYXJlQ29sdW1ucygkdGhpcywganNlLmxpYnMucXVpY2tfZWRpdF9wcm9wZXJ0aWVzX292ZXJ2aWV3X2NvbHVtbnMsIGRhdGEucHJvcGVydGllc0FjdGl2ZUNvbHVtbnMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERhdGFUYWJsZSBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSB7XG5cdFx0XHRhdXRvV2lkdGg6IGZhbHNlLFxuXHRcdFx0ZG9tOiAndCcsXG5cdFx0XHRwYWdlTGVuZ3RoOiBkYXRhLnBhZ2VMZW5ndGgsXG5cdFx0XHRkaXNwbGF5U3RhcnQ6IDAsXG5cdFx0XHRzZXJ2ZXJTaWRlOiB0cnVlLFxuXHRcdFx0bGFuZ3VhZ2U6IGpzZS5saWJzLmRhdGF0YWJsZS5nZXRUcmFuc2xhdGlvbnMoanNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJykpLFxuXHRcdFx0YWpheDoge1xuXHRcdFx0XHR1cmw6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89UXVpY2tFZGl0UHJvZHVjdFByb3BlcnRpZXNBamF4L0RhdGFUYWJsZScsXG5cdFx0XHRcdHR5cGU6ICdQT1NUJyxcblx0XHRcdFx0ZGF0YTogKGRhdGEpID0+IHtcblx0XHRcdFx0XHRkYXRhLnByb2R1Y3RJZCA9IF9nZXRQcm9kdWN0SWRzKCk7XG5cdFx0XHRcdFx0ZGF0YS5wYWdlVG9rZW4gPSBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKTtcblx0XHRcdFx0XHRyZXR1cm4gZGF0YTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG9yZGVyQ2VsbHNUb3A6IHRydWUsXG5cdFx0XHRvcmRlcjogX2dldE9yZGVyKCksXG5cdFx0XHRjb2x1bW5zOiBjb2x1bW5zLFxuXHRcdFx0Y3JlYXRlZFJvdyhyb3csIGRhdGEsIGRhdGFJbmRleCkge1xuXHRcdFx0XHRpZiAoZGF0YS5jb21iaVByaWNlVHlwZSA9PT0gJ2ZpeCcpIHtcblx0XHRcdFx0XHQkKHJvdykuZmluZCgndGQuY29tYmktcHJpY2UnKS5hZGRDbGFzcygnZWRpdGFibGUnKVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBMb2FkaW5nIFNwaW5uZXIgXG5cdFx0ICogXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRsZXQgJHNwaW5uZXIgPSBudWxsO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdldCBJbml0aWFsIFRhYmxlIE9yZGVyXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPYmplY3RbXX0gUmV0dXJucyB0aGUgb3JkZXJlZCBjb2x1bW4gZGVmaW5pdGlvbnMuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldE9yZGVyKCkge1xuXHRcdFx0bGV0IGluZGV4ID0gMTsgLy8gT3JkZXIgYnkgZmlyc3QgY29sdW1uIGJ5IGRlZmF1bHQuXG5cdFx0XHRsZXQgZGlyZWN0aW9uID0gJ2FzYyc7IC8vIE9yZGVyIEFTQyBieSBkZWZhdWx0LlxuXHRcdFx0bGV0IGNvbHVtbk5hbWUgPSAncHJvZHVjdHNOYW1lJzsgLy8gT3JkZXIgYnkgcHJvZHVjdHMgbmFtZSBieSBkZWZhdWx0LlxuXHRcdFx0XG5cdFx0XHQkdGhpcy5vbignY2xpY2snLCAndGgnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0Y29sdW1uTmFtZSA9ICQodGhpcykuZGF0YSgnY29sdW1uLW5hbWUnKTtcblx0XHRcdFx0aW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4odGhpcykuaW5kZXgoKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBcHBseSBpbml0aWFsIHRhYmxlIHNvcnQuXG5cdFx0XHRpZiAoZGF0YS5wcm9wZXJ0aWVzQWN0aXZlQ29sdW1ucy5pbmRleE9mKCdwcm9kdWN0c05hbWUnKSA+IC0xKSB7IC8vIE9yZGVyIGJ5IG5hbWUgaWYgcG9zc2libGUuXG5cdFx0XHRcdGluZGV4ID0gZGF0YS5wcm9wZXJ0aWVzQWN0aXZlQ29sdW1ucy5pbmRleE9mKCdwcm9kdWN0c05hbWUnKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0cmV0dXJuIFtbaW5kZXgsIGRpcmVjdGlvbl1dO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTZXQgY3VycmVudCBwcm9kdWN0IElEcy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBDb250YWlucyBldmVudCBpbmZvcm1hdGlvbi5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2V0UHJvZHVjdElkcyhldmVudCkge1xuXHRcdFx0cHJvZHVjdElkcyA9IFtdO1xuXHRcdFx0XG5cdFx0XHRpZiAoJChldmVudC50YXJnZXQpLmlzKCdhLnByb2R1Y3RzLXByb3BlcnRpZXMnKSkge1xuXHRcdFx0XHRwcm9kdWN0SWRzLnB1c2goJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoJ2lkJykpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRjb25zdCAkc2luZ2xlQ2hlY2tib3hlcyA9ICR0aGlzLnBhcmVudHMoJy5xdWljay1lZGl0Lm92ZXJ2aWV3Jylcblx0XHRcdFx0LmZpbmQoJ2lucHV0OmNoZWNrYm94OmNoZWNrZWQub3ZlcnZpZXctcm93LXNlbGVjdGlvbicpO1xuXHRcdFx0XG5cdFx0XHQkc2luZ2xlQ2hlY2tib3hlcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRwcm9kdWN0SWRzLnB1c2goJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoJ2lkJykpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdldCBQcm9kdWN0IElEcy5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge051bWJlcltdfSBSZXR1cm5zIHRoZSBwcm9kdWN0IElEcy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0UHJvZHVjdElkcygpIHtcblx0XHRcdHJldHVybiBwcm9kdWN0SWRzO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBQcm9wZXJ0aWVzIG1vZGFsIHNob3duIGV2ZW50IGhhbmRsZXIuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uTW9kYWxTaG93bigpIHtcblx0XHRcdCQod2luZG93KS50cmlnZ2VyKCdyZXNpemUnKTtcblx0XHRcdFxuXHRcdFx0aWYgKCEkLmZuLkRhdGFUYWJsZS5pc0RhdGFUYWJsZSgkdGhpcykpIHtcblx0XHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmNyZWF0ZSgkdGhpcywgb3B0aW9ucyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmFqYXgucmVsb2FkKCk7XG5cdFx0fVxuXHRcdFxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkbW9kYWxcblx0XHRcdFx0Lm9uKCdzaG93LmJzLm1vZGFsJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0JCh0aGlzKS5maW5kKCcubW9kYWwtZGlhbG9nJykuY3NzKCd6LWluZGV4JywgMTA2MClcblx0XHRcdFx0fSlcblx0XHRcdFx0Lm9uKCdzaG93bi5icy5tb2RhbCcsIF9vbk1vZGFsU2hvd24pXG5cdFx0XHRcdC5vbignaGlkZS5icy5tb2RhbCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQodGhpcykuZmluZCgnLm1vZGFsLWRpYWxvZycpLmNzcygnei1pbmRleCcsIDApXG5cdFx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5wYXJlbnRzKCcucXVpY2stZWRpdC5vdmVydmlldycpXG5cdFx0XHRcdC5vbignY2xpY2snLCAnYS5wcm9kdWN0cy1wcm9wZXJ0aWVzJywgX3NldFByb2R1Y3RJZHMpXG5cdFx0XHRcdC5vbignY2xpY2snLCAnYS5idWxrLXByb3BlcnRpZXMtZWRpdCcsIF9zZXRQcm9kdWN0SWRzKTtcblx0XHRcdFxuXHRcdFx0JHRoaXMub24oJ2RyYXcuZHQnLCAoKSA9PiB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3RoZWFkIGlucHV0OmNoZWNrYm94Jylcblx0XHRcdFx0XHQucHJvcCgnY2hlY2tlZCcsIGZhbHNlKVxuXHRcdFx0XHRcdC50cmlnZ2VyKCdjaGFuZ2UnLCBbZmFsc2VdKTtcblx0XHRcdFx0JHRoaXMuZmluZCgndGJvZHknKS5hdHRyKCdkYXRhLWd4LXdpZGdldCcsICdzaW5nbGVfY2hlY2tib3gnKTtcblx0XHRcdFx0JHRoaXMuZmluZCgndGJvZHknKS5hdHRyKCdkYXRhLXNpbmdsZV9jaGVja2JveC1zZWxlY3RvcicsICcucHJvcGVydGllcy1yb3ctc2VsZWN0aW9uJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRneC53aWRnZXRzLmluaXQoJHRoaXMpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdwcmVYaHIuZHQnLCAoKSA9PiAkc3Bpbm5lciA9XG5cdFx0XHRcdGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5zaG93KCRtb2RhbC5maW5kKCcubW9kYWwtY29udGVudCcpLCAkbW9kYWwuY3NzKCd6LWluZGV4JykpKTtcblx0XHRcdCR0aGlzLm9uKCd4aHIuZHQnLCAoKSA9PiBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuaGlkZSgkc3Bpbm5lcikpO1xuXHRcdFx0XG5cdFx0XHRqc2UubGlicy5kYXRhdGFibGUuY3JlYXRlKCR0aGlzLCBvcHRpb25zKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7Il19
