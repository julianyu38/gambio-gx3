'use strict';

/* --------------------------------------------------------------
 offline_layer_editor.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Site Online/Offline Layer Editor Controller
 *
 * @module Controllers/offline_layer_editor
 */
gx.controllers.module('offline_layer_editor', ['form', 'fallback', gx.source + '/libs/editor_values', gx.source + '/libs/editor_instances'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	var $this = $(this),
	    defaults = {},
	    options = $.extend(true, {}, defaults, data),
	    lightboxParameters = $this.data('lightboxParams'),
	    module = {},
	    $fields = null,
	    temporaryNamePostfix = '',
	    $layer = $('#lightbox_package_' + lightboxParameters.identifier),
	    $form = $this.find('.lightbox_content_container form'),
	    $parentForm = $(lightboxParameters.element).closest('tr');

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	var _modifyFormInputNames = function _modifyFormInputNames(revertOriginalNames) {
		$fields.each(function () {
			var $field = $(this),
			    name = $field.attr('name');
			name = revertOriginalNames ? name.replace(temporaryNamePostfix, '') : name + temporaryNamePostfix;
			$field.attr('name', name);
		});
	};

	var _onOkButtonClick = function _onOkButtonClick() {
		$form.find('.wysiwyg').each(function (index, textarea) {
			var $textarea = $(textarea),
			    value = jse.libs.editor_values.getValue($textarea);

			$textarea.val(value);
		});

		$layer.find('form').trigger('layerClose');

		_modifyFormInputNames(true);
		jse.libs.form.prefillForm($parentForm, jse.libs.fallback.getData($form), false);
		$.lightbox_plugin('close', lightboxParameters.identifier);
		_destroyEditorInstance();

		// Set editor-type hidden fields in the timer table (they'll be saved on save/insert from PHP).
		var $textarea = $this.find('.wysiwyg'),
		    editorIdentifier = $textarea.data('editorIdentifier'),
		    editorType = $textarea.data('editorType'),
		    $inputHidden = $form.find('[name="editor_identifiers[' + editorIdentifier + ']"]'),
		    $editIcon = $('.timer-table [name="editor_identifiers[' + editorIdentifier + ']"]:first').parent().find('i.open_lightbox');

		$inputHidden.val(editorType);

		if ($editIcon.length) {
			// $editIcon.attr('data-lightbox-editor-type', editorType); 
			$editIcon.data('lightboxEditorType', editorType);
		}
	};

	var _onCloseButtonClick = function _onCloseButtonClick() {
		_modifyFormInputNames(true);
		_destroyEditorInstance();
	};

	var _destroyEditorInstance = function _destroyEditorInstance() {
		var $textarea = $this.find('.wysiwyg');

		if ($textarea.length) {
			jse.libs.editor_instances.destroy($textarea);
		}

		$(window).off('editor:initialize editor:ready');
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// -----------------------------------------------------------------------

	module.init = function (done) {
		var dataset = jse.libs.fallback.getData($parentForm);
		$fields = $parentForm.find('[name]');
		temporaryNamePostfix = '_tmp_' + parseInt(Math.random() * new Date().getTime());

		_modifyFormInputNames();
		jse.libs.form.prefillForm($form, dataset, false);
		jse.libs.fallback.setupWidgetAttr($this);

		$.when(gx.extensions.init($this), gx.controllers.init($this), gx.widgets.init($this), gx.compatibility.init($this)).then(function () {
			// Delay the editor initialization until the lightbox is ready.
			$(window).trigger('editor:initialize');
		});

		$layer.on('click', '.ok', _onOkButtonClick).on('click', '.close', _onCloseButtonClick);

		$this.find('form').trigger('language_switcher.updateField');

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNob3Bfb2ZmbGluZS9vZmZsaW5lX2xheWVyX2VkaXRvci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsImxpZ2h0Ym94UGFyYW1ldGVycyIsIiRmaWVsZHMiLCJ0ZW1wb3JhcnlOYW1lUG9zdGZpeCIsIiRsYXllciIsImlkZW50aWZpZXIiLCIkZm9ybSIsImZpbmQiLCIkcGFyZW50Rm9ybSIsImVsZW1lbnQiLCJjbG9zZXN0IiwiX21vZGlmeUZvcm1JbnB1dE5hbWVzIiwicmV2ZXJ0T3JpZ2luYWxOYW1lcyIsImVhY2giLCIkZmllbGQiLCJuYW1lIiwiYXR0ciIsInJlcGxhY2UiLCJfb25Pa0J1dHRvbkNsaWNrIiwiaW5kZXgiLCJ0ZXh0YXJlYSIsIiR0ZXh0YXJlYSIsInZhbHVlIiwianNlIiwibGlicyIsImVkaXRvcl92YWx1ZXMiLCJnZXRWYWx1ZSIsInZhbCIsInRyaWdnZXIiLCJmb3JtIiwicHJlZmlsbEZvcm0iLCJmYWxsYmFjayIsImdldERhdGEiLCJsaWdodGJveF9wbHVnaW4iLCJfZGVzdHJveUVkaXRvckluc3RhbmNlIiwiZWRpdG9ySWRlbnRpZmllciIsImVkaXRvclR5cGUiLCIkaW5wdXRIaWRkZW4iLCIkZWRpdEljb24iLCJwYXJlbnQiLCJsZW5ndGgiLCJfb25DbG9zZUJ1dHRvbkNsaWNrIiwiZWRpdG9yX2luc3RhbmNlcyIsImRlc3Ryb3kiLCJ3aW5kb3ciLCJvZmYiLCJpbml0IiwiZG9uZSIsImRhdGFzZXQiLCJwYXJzZUludCIsIk1hdGgiLCJyYW5kb20iLCJEYXRlIiwiZ2V0VGltZSIsInNldHVwV2lkZ2V0QXR0ciIsIndoZW4iLCJleHRlbnNpb25zIiwid2lkZ2V0cyIsImNvbXBhdGliaWxpdHkiLCJ0aGVuIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msc0JBREQsRUFHQyxDQUFDLE1BQUQsRUFBUyxVQUFULEVBQXFCRixHQUFHRyxNQUFILEdBQVkscUJBQWpDLEVBQXdESCxHQUFHRyxNQUFILEdBQVksd0JBQXBFLENBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEtBQUlDLFFBQVFDLEVBQUUsSUFBRixDQUFaO0FBQUEsS0FDQ0MsV0FBVyxFQURaO0FBQUEsS0FFQ0MsVUFBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FGWDtBQUFBLEtBR0NNLHFCQUFxQkwsTUFBTUQsSUFBTixDQUFXLGdCQUFYLENBSHRCO0FBQUEsS0FJQ0YsU0FBUyxFQUpWO0FBQUEsS0FLQ1MsVUFBVSxJQUxYO0FBQUEsS0FNQ0MsdUJBQXVCLEVBTnhCO0FBQUEsS0FPQ0MsU0FBU1AsRUFBRSx1QkFBdUJJLG1CQUFtQkksVUFBNUMsQ0FQVjtBQUFBLEtBUUNDLFFBQVFWLE1BQU1XLElBQU4sQ0FBVyxrQ0FBWCxDQVJUO0FBQUEsS0FTQ0MsY0FBY1gsRUFBRUksbUJBQW1CUSxPQUFyQixFQUE4QkMsT0FBOUIsQ0FBc0MsSUFBdEMsQ0FUZjs7QUFXQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSUMsd0JBQXdCLFNBQXhCQSxxQkFBd0IsQ0FBU0MsbUJBQVQsRUFBOEI7QUFDekRWLFVBQVFXLElBQVIsQ0FBYSxZQUFXO0FBQ3ZCLE9BQUlDLFNBQVNqQixFQUFFLElBQUYsQ0FBYjtBQUFBLE9BQ0NrQixPQUFPRCxPQUFPRSxJQUFQLENBQVksTUFBWixDQURSO0FBRUFELFVBQU9ILHNCQUFzQkcsS0FBS0UsT0FBTCxDQUFhZCxvQkFBYixFQUFtQyxFQUFuQyxDQUF0QixHQUFnRVksT0FBT1osb0JBQTlFO0FBQ0FXLFVBQU9FLElBQVAsQ0FBWSxNQUFaLEVBQW9CRCxJQUFwQjtBQUNBLEdBTEQ7QUFNQSxFQVBEOztBQVNBLEtBQUlHLG1CQUFtQixTQUFuQkEsZ0JBQW1CLEdBQVc7QUFDakNaLFFBQ0VDLElBREYsQ0FDTyxVQURQLEVBRUVNLElBRkYsQ0FFTyxVQUFTTSxLQUFULEVBQWdCQyxRQUFoQixFQUEwQjtBQUMvQixPQUFJQyxZQUFZeEIsRUFBRXVCLFFBQUYsQ0FBaEI7QUFBQSxPQUNDRSxRQUFRQyxJQUFJQyxJQUFKLENBQVNDLGFBQVQsQ0FBdUJDLFFBQXZCLENBQWdDTCxTQUFoQyxDQURUOztBQUdBQSxhQUFVTSxHQUFWLENBQWNMLEtBQWQ7QUFDQSxHQVBGOztBQVNBbEIsU0FDRUcsSUFERixDQUNPLE1BRFAsRUFFRXFCLE9BRkYsQ0FFVSxZQUZWOztBQUlBakIsd0JBQXNCLElBQXRCO0FBQ0FZLE1BQUlDLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxXQUFkLENBQTBCdEIsV0FBMUIsRUFBdUNlLElBQUlDLElBQUosQ0FBU08sUUFBVCxDQUFrQkMsT0FBbEIsQ0FBMEIxQixLQUExQixDQUF2QyxFQUF5RSxLQUF6RTtBQUNBVCxJQUFFb0MsZUFBRixDQUFrQixPQUFsQixFQUEyQmhDLG1CQUFtQkksVUFBOUM7QUFDQTZCOztBQUVBO0FBQ0EsTUFBSWIsWUFBWXpCLE1BQU1XLElBQU4sQ0FBVyxVQUFYLENBQWhCO0FBQUEsTUFDQzRCLG1CQUFtQmQsVUFBVTFCLElBQVYsQ0FBZSxrQkFBZixDQURwQjtBQUFBLE1BRUN5QyxhQUFhZixVQUFVMUIsSUFBVixDQUFlLFlBQWYsQ0FGZDtBQUFBLE1BR0MwQyxlQUFlL0IsTUFBTUMsSUFBTixDQUFXLCtCQUErQjRCLGdCQUEvQixHQUFrRCxLQUE3RCxDQUhoQjtBQUFBLE1BSUNHLFlBQVl6QyxFQUFFLDRDQUE0Q3NDLGdCQUE1QyxHQUErRCxXQUFqRSxFQUE4RUksTUFBOUUsR0FBdUZoQyxJQUF2RixDQUE0RixpQkFBNUYsQ0FKYjs7QUFNQThCLGVBQWFWLEdBQWIsQ0FBaUJTLFVBQWpCOztBQUVBLE1BQUlFLFVBQVVFLE1BQWQsRUFBc0I7QUFDckI7QUFDQUYsYUFBVTNDLElBQVYsQ0FBZSxvQkFBZixFQUFxQ3lDLFVBQXJDO0FBQ0E7QUFDRCxFQWhDRDs7QUFrQ0EsS0FBSUssc0JBQXNCLFNBQXRCQSxtQkFBc0IsR0FBVztBQUNwQzlCLHdCQUFzQixJQUF0QjtBQUNBdUI7QUFDQSxFQUhEOztBQUtBLEtBQUlBLHlCQUF5QixTQUF6QkEsc0JBQXlCLEdBQVc7QUFDdkMsTUFBSWIsWUFBWXpCLE1BQU1XLElBQU4sQ0FBVyxVQUFYLENBQWhCOztBQUVBLE1BQUljLFVBQVVtQixNQUFkLEVBQXNCO0FBQ3JCakIsT0FBSUMsSUFBSixDQUFTa0IsZ0JBQVQsQ0FBMEJDLE9BQTFCLENBQWtDdEIsU0FBbEM7QUFDQTs7QUFFRHhCLElBQUUrQyxNQUFGLEVBQVVDLEdBQVYsQ0FBYyxnQ0FBZDtBQUNBLEVBUkQ7O0FBVUE7QUFDQTtBQUNBOztBQUVBcEQsUUFBT3FELElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUIsTUFBSUMsVUFBVXpCLElBQUlDLElBQUosQ0FBU08sUUFBVCxDQUFrQkMsT0FBbEIsQ0FBMEJ4QixXQUExQixDQUFkO0FBQ0FOLFlBQVVNLFlBQVlELElBQVosQ0FBaUIsUUFBakIsQ0FBVjtBQUNBSix5QkFBdUIsVUFBVThDLFNBQVNDLEtBQUtDLE1BQUwsS0FBZ0IsSUFBSUMsSUFBSixHQUFXQyxPQUFYLEVBQXpCLENBQWpDOztBQUVBMUM7QUFDQVksTUFBSUMsSUFBSixDQUFTSyxJQUFULENBQWNDLFdBQWQsQ0FBMEJ4QixLQUExQixFQUFpQzBDLE9BQWpDLEVBQTBDLEtBQTFDO0FBQ0F6QixNQUFJQyxJQUFKLENBQVNPLFFBQVQsQ0FBa0J1QixlQUFsQixDQUFrQzFELEtBQWxDOztBQUVBQyxJQUFFMEQsSUFBRixDQUNDaEUsR0FBR2lFLFVBQUgsQ0FBY1YsSUFBZCxDQUFtQmxELEtBQW5CLENBREQsRUFFQ0wsR0FBR0MsV0FBSCxDQUFlc0QsSUFBZixDQUFvQmxELEtBQXBCLENBRkQsRUFHQ0wsR0FBR2tFLE9BQUgsQ0FBV1gsSUFBWCxDQUFnQmxELEtBQWhCLENBSEQsRUFJQ0wsR0FBR21FLGFBQUgsQ0FBaUJaLElBQWpCLENBQXNCbEQsS0FBdEIsQ0FKRCxFQUtFK0QsSUFMRixDQUtPLFlBQVc7QUFDakI7QUFDQTlELEtBQUUrQyxNQUFGLEVBQVVoQixPQUFWLENBQWtCLG1CQUFsQjtBQUNBLEdBUkQ7O0FBVUF4QixTQUNFd0QsRUFERixDQUNLLE9BREwsRUFDYyxLQURkLEVBQ3FCMUMsZ0JBRHJCLEVBRUUwQyxFQUZGLENBRUssT0FGTCxFQUVjLFFBRmQsRUFFd0JuQixtQkFGeEI7O0FBSUE3QyxRQUFNVyxJQUFOLENBQVcsTUFBWCxFQUFtQnFCLE9BQW5CLENBQTJCLCtCQUEzQjs7QUFFQW1CO0FBQ0EsRUExQkQ7O0FBNEJBLFFBQU90RCxNQUFQO0FBQ0EsQ0F2SEYiLCJmaWxlIjoic2hvcF9vZmZsaW5lL29mZmxpbmVfbGF5ZXJfZWRpdG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBvZmZsaW5lX2xheWVyX2VkaXRvci5qcyAyMDE2LTA5LTA4XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBTaXRlIE9ubGluZS9PZmZsaW5lIExheWVyIEVkaXRvciBDb250cm9sbGVyXG4gKlxuICogQG1vZHVsZSBDb250cm9sbGVycy9vZmZsaW5lX2xheWVyX2VkaXRvclxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdvZmZsaW5lX2xheWVyX2VkaXRvcicsXG5cdFxuXHRbJ2Zvcm0nLCAnZmFsbGJhY2snLCBneC5zb3VyY2UgKyAnL2xpYnMvZWRpdG9yX3ZhbHVlcycsIGd4LnNvdXJjZSArICcvbGlicy9lZGl0b3JfaW5zdGFuY2VzJ10sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhciAkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRsaWdodGJveFBhcmFtZXRlcnMgPSAkdGhpcy5kYXRhKCdsaWdodGJveFBhcmFtcycpLFxuXHRcdFx0bW9kdWxlID0ge30sXG5cdFx0XHQkZmllbGRzID0gbnVsbCxcblx0XHRcdHRlbXBvcmFyeU5hbWVQb3N0Zml4ID0gJycsXG5cdFx0XHQkbGF5ZXIgPSAkKCcjbGlnaHRib3hfcGFja2FnZV8nICsgbGlnaHRib3hQYXJhbWV0ZXJzLmlkZW50aWZpZXIpLCBcblx0XHRcdCRmb3JtID0gJHRoaXMuZmluZCgnLmxpZ2h0Ym94X2NvbnRlbnRfY29udGFpbmVyIGZvcm0nKSxcblx0XHRcdCRwYXJlbnRGb3JtID0gJChsaWdodGJveFBhcmFtZXRlcnMuZWxlbWVudCkuY2xvc2VzdCgndHInKTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXIgX21vZGlmeUZvcm1JbnB1dE5hbWVzID0gZnVuY3Rpb24ocmV2ZXJ0T3JpZ2luYWxOYW1lcykge1xuXHRcdFx0JGZpZWxkcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgJGZpZWxkID0gJCh0aGlzKSxcblx0XHRcdFx0XHRuYW1lID0gJGZpZWxkLmF0dHIoJ25hbWUnKTtcblx0XHRcdFx0bmFtZSA9IHJldmVydE9yaWdpbmFsTmFtZXMgPyBuYW1lLnJlcGxhY2UodGVtcG9yYXJ5TmFtZVBvc3RmaXgsICcnKSA6IChuYW1lICsgdGVtcG9yYXJ5TmFtZVBvc3RmaXgpO1xuXHRcdFx0XHQkZmllbGQuYXR0cignbmFtZScsIG5hbWUpO1xuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX29uT2tCdXR0b25DbGljayA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0JGZvcm1cblx0XHRcdFx0LmZpbmQoJy53eXNpd3lnJylcblx0XHRcdFx0LmVhY2goZnVuY3Rpb24oaW5kZXgsIHRleHRhcmVhKSB7XG5cdFx0XHRcdFx0dmFyICR0ZXh0YXJlYSA9ICQodGV4dGFyZWEpLFxuXHRcdFx0XHRcdFx0dmFsdWUgPSBqc2UubGlicy5lZGl0b3JfdmFsdWVzLmdldFZhbHVlKCR0ZXh0YXJlYSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHRleHRhcmVhLnZhbCh2YWx1ZSk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkbGF5ZXJcblx0XHRcdFx0LmZpbmQoJ2Zvcm0nKVxuXHRcdFx0XHQudHJpZ2dlcignbGF5ZXJDbG9zZScpO1xuXHRcdFx0XG5cdFx0XHRfbW9kaWZ5Rm9ybUlucHV0TmFtZXModHJ1ZSk7XG5cdFx0XHRqc2UubGlicy5mb3JtLnByZWZpbGxGb3JtKCRwYXJlbnRGb3JtLCBqc2UubGlicy5mYWxsYmFjay5nZXREYXRhKCRmb3JtKSwgZmFsc2UpO1xuXHRcdFx0JC5saWdodGJveF9wbHVnaW4oJ2Nsb3NlJywgbGlnaHRib3hQYXJhbWV0ZXJzLmlkZW50aWZpZXIpO1xuXHRcdFx0X2Rlc3Ryb3lFZGl0b3JJbnN0YW5jZSgpO1xuXHRcdFx0XG5cdFx0XHQvLyBTZXQgZWRpdG9yLXR5cGUgaGlkZGVuIGZpZWxkcyBpbiB0aGUgdGltZXIgdGFibGUgKHRoZXknbGwgYmUgc2F2ZWQgb24gc2F2ZS9pbnNlcnQgZnJvbSBQSFApLlxuXHRcdFx0dmFyICR0ZXh0YXJlYSA9ICR0aGlzLmZpbmQoJy53eXNpd3lnJyksXG5cdFx0XHRcdGVkaXRvcklkZW50aWZpZXIgPSAkdGV4dGFyZWEuZGF0YSgnZWRpdG9ySWRlbnRpZmllcicpLFxuXHRcdFx0XHRlZGl0b3JUeXBlID0gJHRleHRhcmVhLmRhdGEoJ2VkaXRvclR5cGUnKSxcblx0XHRcdFx0JGlucHV0SGlkZGVuID0gJGZvcm0uZmluZCgnW25hbWU9XCJlZGl0b3JfaWRlbnRpZmllcnNbJyArIGVkaXRvcklkZW50aWZpZXIgKyAnXVwiXScpLFxuXHRcdFx0XHQkZWRpdEljb24gPSAkKCcudGltZXItdGFibGUgW25hbWU9XCJlZGl0b3JfaWRlbnRpZmllcnNbJyArIGVkaXRvcklkZW50aWZpZXIgKyAnXVwiXTpmaXJzdCcpLnBhcmVudCgpLmZpbmQoJ2kub3Blbl9saWdodGJveCcpOyBcblx0XHRcdFxuXHRcdFx0JGlucHV0SGlkZGVuLnZhbChlZGl0b3JUeXBlKTtcblx0XHRcdFxuXHRcdFx0aWYgKCRlZGl0SWNvbi5sZW5ndGgpIHtcblx0XHRcdFx0Ly8gJGVkaXRJY29uLmF0dHIoJ2RhdGEtbGlnaHRib3gtZWRpdG9yLXR5cGUnLCBlZGl0b3JUeXBlKTsgXG5cdFx0XHRcdCRlZGl0SWNvbi5kYXRhKCdsaWdodGJveEVkaXRvclR5cGUnLCBlZGl0b3JUeXBlKTsgXG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX29uQ2xvc2VCdXR0b25DbGljayA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0X21vZGlmeUZvcm1JbnB1dE5hbWVzKHRydWUpO1x0XG5cdFx0XHRfZGVzdHJveUVkaXRvckluc3RhbmNlKCk7IFxuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9kZXN0cm95RWRpdG9ySW5zdGFuY2UgPSBmdW5jdGlvbigpIHtcblx0XHRcdHZhciAkdGV4dGFyZWEgPSAkdGhpcy5maW5kKCcud3lzaXd5ZycpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJHRleHRhcmVhLmxlbmd0aCkge1xuXHRcdFx0XHRqc2UubGlicy5lZGl0b3JfaW5zdGFuY2VzLmRlc3Ryb3koJHRleHRhcmVhKTtcdFxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkKHdpbmRvdykub2ZmKCdlZGl0b3I6aW5pdGlhbGl6ZSBlZGl0b3I6cmVhZHknKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdHZhciBkYXRhc2V0ID0ganNlLmxpYnMuZmFsbGJhY2suZ2V0RGF0YSgkcGFyZW50Rm9ybSk7XG5cdFx0XHQkZmllbGRzID0gJHBhcmVudEZvcm0uZmluZCgnW25hbWVdJyk7XG5cdFx0XHR0ZW1wb3JhcnlOYW1lUG9zdGZpeCA9ICdfdG1wXycgKyBwYXJzZUludChNYXRoLnJhbmRvbSgpICogbmV3IERhdGUoKS5nZXRUaW1lKCkpO1xuXHRcdFx0XG5cdFx0XHRfbW9kaWZ5Rm9ybUlucHV0TmFtZXMoKTtcblx0XHRcdGpzZS5saWJzLmZvcm0ucHJlZmlsbEZvcm0oJGZvcm0sIGRhdGFzZXQsIGZhbHNlKTtcblx0XHRcdGpzZS5saWJzLmZhbGxiYWNrLnNldHVwV2lkZ2V0QXR0cigkdGhpcyk7XG5cdFx0XHRcblx0XHRcdCQud2hlbihcblx0XHRcdFx0Z3guZXh0ZW5zaW9ucy5pbml0KCR0aGlzKSwgXG5cdFx0XHRcdGd4LmNvbnRyb2xsZXJzLmluaXQoJHRoaXMpLCAgXG5cdFx0XHRcdGd4LndpZGdldHMuaW5pdCgkdGhpcyksICBcblx0XHRcdFx0Z3guY29tcGF0aWJpbGl0eS5pbml0KCR0aGlzKVxuXHRcdFx0KS50aGVuKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQvLyBEZWxheSB0aGUgZWRpdG9yIGluaXRpYWxpemF0aW9uIHVudGlsIHRoZSBsaWdodGJveCBpcyByZWFkeS5cblx0XHRcdFx0JCh3aW5kb3cpLnRyaWdnZXIoJ2VkaXRvcjppbml0aWFsaXplJyk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JGxheWVyXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLm9rJywgX29uT2tCdXR0b25DbGljaylcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuY2xvc2UnLCBfb25DbG9zZUJ1dHRvbkNsaWNrKTtcblx0XHRcdFx0XG5cdFx0XHQkdGhpcy5maW5kKCdmb3JtJykudHJpZ2dlcignbGFuZ3VhZ2Vfc3dpdGNoZXIudXBkYXRlRmllbGQnKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
