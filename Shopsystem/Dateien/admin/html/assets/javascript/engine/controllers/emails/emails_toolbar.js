'use strict';

/* --------------------------------------------------------------
 emails_toolbar.js 2016-10-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Emails Toolbar Controller
 *
 * This controller will handle the main toolbar operations of the admin/emails page.
 *
 * @module Controllers/emails_toolbar
 */
gx.controllers.module('emails_toolbar', [gx.source + '/libs/emails', 'url_arguments'],

/** @lends module:Controllers/emails_toolbar */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Modal Selector
  *
  * @type {object}
  */
	$modal = $('#emails-modal'),


	/**
  * Table Selector
  *
  * @type {object}
  */
	$table = $('#emails-table'),


	/**
  * Default Module Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Module Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {
		model: {
			settings: jse.core.config.get('appUrl') + '/admin/admin.php?do=Emails/GetEmailSettings'
		}
	};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Convert the line breaks to <br> elements.
  *
  * @param {string} text Text to be converted.
  *
  * @return {string} Returns the converted string.
  */
	var _nl2br = function _nl2br(text) {
		return (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
	};

	/**
  * Display create new email modal.
  *
  * @param {object} event Contains event information.
  */
	var _onCreateNewEmail = function _onCreateNewEmail(event) {
		// Reset modal elements to initial state.
		jse.libs.emails.resetModal($modal);

		// Apply Email Settings to the Email Modal
		if (typeof module.model.settings !== 'undefined' && module.model.settings !== null) {
			// Set the email signature (if not empty). We'll only set the signature to the CKEditor because
			// if the signature contains HTML markup it cannot be sanitized properly for the plain content.
			if (module.model.settings.signature !== null && module.model.settings.signature !== '') {
				var signatureHtml = '<br><p>-----<br>' + _nl2br(module.model.settings.signature) + '</p>';
				CKEDITOR.instances['content-html'].setData(signatureHtml);
				var signaturePlain = '\n\n-----\n' + module.model.settings.signature.replace(/(<([^>]+)>)/gi, '');
				$modal.find('#content-plain').val(signaturePlain);
			}

			// Disable the HTML content if the shop uses only plain content for the emails.
			if (module.model.settings.useHtml === false) {
				$modal.find('.content').find('.tab-headline:eq(0), .tab-content:eq(0)').hide();
				$modal.find('.content').find('.tab-headline:eq(1)').trigger('click');
			}

			// Preload sender and reply to contact data if provided.
			if (typeof module.model.settings.replyAddress !== 'undefined' && module.model.settings.replyAddress !== '') {
				$modal.find('#sender-email, #reply-to-email').val(module.model.settings.replyAddress);
			}
			if (typeof module.model.settings.replyName !== 'undefined' && module.model.settings.replyName !== '') {
				$modal.find('#sender-name, #reply-to-name').val(module.model.settings.replyName);
			}
		}

		// Prepare and display new modal window.
		$modal.dialog({
			title: jse.core.lang.translate('new_mail', 'buttons'),
			width: 1000,
			height: 800,
			modal: false,
			dialogClass: 'gx-container',
			closeOnEscape: false,
			buttons: jse.libs.emails.getDefaultModalButtons($modal, $table),
			open: jse.libs.emails.colorizeButtonsForEditMode
		});
	};

	/**
  * Perform search request on the DataTable instance.
  *
  * @param {object} event Contains the event data.
  */
	var _onTableSearchSubmit = function _onTableSearchSubmit(event) {
		event.preventDefault();
		var keyword = $this.find('#search-keyword').val();
		$table.DataTable().search(keyword).draw();
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the module, called by the engine.
  */
	module.init = function (done) {
		// Set default "#bulk-action" value.
		$this.find('#bulk-action').val('');

		// Bind Event Handlers
		$this.on('click', '#create-new-email', _onCreateNewEmail).on('submit', '#quick-search', _onTableSearchSubmit);

		// Check if the "mail_to" parameter is present and process its value within the new email modal layer.
		var getParameters = jse.libs.url_arguments.getUrlParameters();
		if (typeof getParameters.mailto !== 'undefined') {
			_onCreateNewEmail({}); // Display the new email modal.
			$modal.find('#recipient-email').val(getParameters.mailto);
		}

		done();
	};

	// Return module object to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVtYWlscy9lbWFpbHNfdG9vbGJhci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRtb2RhbCIsIiR0YWJsZSIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIm1vZGVsIiwic2V0dGluZ3MiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwiX25sMmJyIiwidGV4dCIsInJlcGxhY2UiLCJfb25DcmVhdGVOZXdFbWFpbCIsImV2ZW50IiwibGlicyIsImVtYWlscyIsInJlc2V0TW9kYWwiLCJzaWduYXR1cmUiLCJzaWduYXR1cmVIdG1sIiwiQ0tFRElUT1IiLCJpbnN0YW5jZXMiLCJzZXREYXRhIiwic2lnbmF0dXJlUGxhaW4iLCJmaW5kIiwidmFsIiwidXNlSHRtbCIsImhpZGUiLCJ0cmlnZ2VyIiwicmVwbHlBZGRyZXNzIiwicmVwbHlOYW1lIiwiZGlhbG9nIiwidGl0bGUiLCJsYW5nIiwidHJhbnNsYXRlIiwid2lkdGgiLCJoZWlnaHQiLCJtb2RhbCIsImRpYWxvZ0NsYXNzIiwiY2xvc2VPbkVzY2FwZSIsImJ1dHRvbnMiLCJnZXREZWZhdWx0TW9kYWxCdXR0b25zIiwib3BlbiIsImNvbG9yaXplQnV0dG9uc0ZvckVkaXRNb2RlIiwiX29uVGFibGVTZWFyY2hTdWJtaXQiLCJwcmV2ZW50RGVmYXVsdCIsImtleXdvcmQiLCJEYXRhVGFibGUiLCJzZWFyY2giLCJkcmF3IiwiaW5pdCIsImRvbmUiLCJvbiIsImdldFBhcmFtZXRlcnMiLCJ1cmxfYXJndW1lbnRzIiwiZ2V0VXJsUGFyYW1ldGVycyIsIm1haWx0byJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLGdCQURELEVBR0MsQ0FDQ0YsR0FBR0csTUFBSCxHQUFZLGNBRGIsRUFFQyxlQUZELENBSEQ7O0FBUUM7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFVBQVNELEVBQUUsZUFBRixDQWJWOzs7QUFlQzs7Ozs7QUFLQUUsVUFBU0YsRUFBRSxlQUFGLENBcEJWOzs7QUFzQkM7Ozs7O0FBS0FHLFlBQVcsRUEzQlo7OztBQTZCQzs7Ozs7QUFLQUMsV0FBVUosRUFBRUssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkwsSUFBN0IsQ0FsQ1g7OztBQW9DQzs7Ozs7QUFLQUYsVUFBUztBQUNSVSxTQUFPO0FBQ05DLGFBQVVDLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0M7QUFEcEM7QUFEQyxFQXpDVjs7QUErQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O0FBT0EsS0FBSUMsU0FBUyxTQUFUQSxNQUFTLENBQVNDLElBQVQsRUFBZTtBQUMzQixTQUFPLENBQUNBLE9BQU8sRUFBUixFQUFZQyxPQUFaLENBQW9CLCtCQUFwQixFQUFxRCxPQUFPLE1BQVAsR0FBZ0IsSUFBckUsQ0FBUDtBQUNBLEVBRkQ7O0FBSUE7Ozs7O0FBS0EsS0FBSUMsb0JBQW9CLFNBQXBCQSxpQkFBb0IsQ0FBU0MsS0FBVCxFQUFnQjtBQUN2QztBQUNBUixNQUFJUyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLFVBQWhCLENBQTJCbEIsTUFBM0I7O0FBRUE7QUFDQSxNQUFJLE9BQU9MLE9BQU9VLEtBQVAsQ0FBYUMsUUFBcEIsS0FBaUMsV0FBakMsSUFBZ0RYLE9BQU9VLEtBQVAsQ0FBYUMsUUFBYixLQUEwQixJQUE5RSxFQUFvRjtBQUNuRjtBQUNBO0FBQ0EsT0FBSVgsT0FBT1UsS0FBUCxDQUFhQyxRQUFiLENBQXNCYSxTQUF0QixLQUFvQyxJQUFwQyxJQUE0Q3hCLE9BQU9VLEtBQVAsQ0FBYUMsUUFBYixDQUFzQmEsU0FBdEIsS0FBb0MsRUFBcEYsRUFBd0Y7QUFDdkYsUUFBSUMsZ0JBQWdCLHFCQUFxQlQsT0FBT2hCLE9BQU9VLEtBQVAsQ0FBYUMsUUFBYixDQUFzQmEsU0FBN0IsQ0FBckIsR0FBK0QsTUFBbkY7QUFDQUUsYUFBU0MsU0FBVCxDQUFtQixjQUFuQixFQUFtQ0MsT0FBbkMsQ0FBMkNILGFBQTNDO0FBQ0EsUUFBSUksaUJBQWlCLGdCQUFnQjdCLE9BQU9VLEtBQVAsQ0FBYUMsUUFBYixDQUFzQmEsU0FBdEIsQ0FBZ0NOLE9BQWhDLENBQXdDLGVBQXhDLEVBQ25DLEVBRG1DLENBQXJDO0FBRUFiLFdBQU95QixJQUFQLENBQVksZ0JBQVosRUFBOEJDLEdBQTlCLENBQWtDRixjQUFsQztBQUNBOztBQUVEO0FBQ0EsT0FBSTdCLE9BQU9VLEtBQVAsQ0FBYUMsUUFBYixDQUFzQnFCLE9BQXRCLEtBQWtDLEtBQXRDLEVBQTZDO0FBQzVDM0IsV0FBT3lCLElBQVAsQ0FBWSxVQUFaLEVBQXdCQSxJQUF4QixDQUE2Qix5Q0FBN0IsRUFBd0VHLElBQXhFO0FBQ0E1QixXQUFPeUIsSUFBUCxDQUFZLFVBQVosRUFBd0JBLElBQXhCLENBQTZCLHFCQUE3QixFQUFvREksT0FBcEQsQ0FBNEQsT0FBNUQ7QUFDQTs7QUFFRDtBQUNBLE9BQUksT0FBT2xDLE9BQU9VLEtBQVAsQ0FBYUMsUUFBYixDQUFzQndCLFlBQTdCLEtBQThDLFdBQTlDLElBQTZEbkMsT0FBT1UsS0FBUCxDQUFhQyxRQUFiLENBQXNCd0IsWUFBdEIsS0FDaEUsRUFERCxFQUNLO0FBQ0o5QixXQUFPeUIsSUFBUCxDQUFZLGdDQUFaLEVBQThDQyxHQUE5QyxDQUFrRC9CLE9BQU9VLEtBQVAsQ0FBYUMsUUFBYixDQUFzQndCLFlBQXhFO0FBQ0E7QUFDRCxPQUFJLE9BQU9uQyxPQUFPVSxLQUFQLENBQWFDLFFBQWIsQ0FBc0J5QixTQUE3QixLQUEyQyxXQUEzQyxJQUEwRHBDLE9BQU9VLEtBQVAsQ0FBYUMsUUFBYixDQUFzQnlCLFNBQXRCLEtBQzdELEVBREQsRUFDSztBQUNKL0IsV0FBT3lCLElBQVAsQ0FBWSw4QkFBWixFQUE0Q0MsR0FBNUMsQ0FBZ0QvQixPQUFPVSxLQUFQLENBQWFDLFFBQWIsQ0FBc0J5QixTQUF0RTtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQS9CLFNBQU9nQyxNQUFQLENBQWM7QUFDYkMsVUFBTzFCLElBQUlDLElBQUosQ0FBUzBCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixVQUF4QixFQUFvQyxTQUFwQyxDQURNO0FBRWJDLFVBQU8sSUFGTTtBQUdiQyxXQUFRLEdBSEs7QUFJYkMsVUFBTyxLQUpNO0FBS2JDLGdCQUFhLGNBTEE7QUFNYkMsa0JBQWUsS0FORjtBQU9iQyxZQUFTbEMsSUFBSVMsSUFBSixDQUFTQyxNQUFULENBQWdCeUIsc0JBQWhCLENBQXVDMUMsTUFBdkMsRUFBK0NDLE1BQS9DLENBUEk7QUFRYjBDLFNBQU1wQyxJQUFJUyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0IyQjtBQVJULEdBQWQ7QUFVQSxFQTVDRDs7QUE4Q0E7Ozs7O0FBS0EsS0FBSUMsdUJBQXVCLFNBQXZCQSxvQkFBdUIsQ0FBUzlCLEtBQVQsRUFBZ0I7QUFDMUNBLFFBQU0rQixjQUFOO0FBQ0EsTUFBSUMsVUFBVWpELE1BQU0yQixJQUFOLENBQVcsaUJBQVgsRUFBOEJDLEdBQTlCLEVBQWQ7QUFDQXpCLFNBQU8rQyxTQUFQLEdBQW1CQyxNQUFuQixDQUEwQkYsT0FBMUIsRUFBbUNHLElBQW5DO0FBQ0EsRUFKRDs7QUFNQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBdkQsUUFBT3dELElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUI7QUFDQXRELFFBQU0yQixJQUFOLENBQVcsY0FBWCxFQUEyQkMsR0FBM0IsQ0FBK0IsRUFBL0I7O0FBRUE7QUFDQTVCLFFBQ0V1RCxFQURGLENBQ0ssT0FETCxFQUNjLG1CQURkLEVBQ21DdkMsaUJBRG5DLEVBRUV1QyxFQUZGLENBRUssUUFGTCxFQUVlLGVBRmYsRUFFZ0NSLG9CQUZoQzs7QUFJQTtBQUNBLE1BQUlTLGdCQUFnQi9DLElBQUlTLElBQUosQ0FBU3VDLGFBQVQsQ0FBdUJDLGdCQUF2QixFQUFwQjtBQUNBLE1BQUksT0FBT0YsY0FBY0csTUFBckIsS0FBZ0MsV0FBcEMsRUFBaUQ7QUFDaEQzQyxxQkFBa0IsRUFBbEIsRUFEZ0QsQ0FDekI7QUFDdkJkLFVBQU95QixJQUFQLENBQVksa0JBQVosRUFBZ0NDLEdBQWhDLENBQW9DNEIsY0FBY0csTUFBbEQ7QUFDQTs7QUFFREw7QUFDQSxFQWpCRDs7QUFtQkE7QUFDQSxRQUFPekQsTUFBUDtBQUNBLENBMUtGIiwiZmlsZSI6ImVtYWlscy9lbWFpbHNfdG9vbGJhci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZW1haWxzX3Rvb2xiYXIuanMgMjAxNi0xMC0xMVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgRW1haWxzIFRvb2xiYXIgQ29udHJvbGxlclxuICpcbiAqIFRoaXMgY29udHJvbGxlciB3aWxsIGhhbmRsZSB0aGUgbWFpbiB0b29sYmFyIG9wZXJhdGlvbnMgb2YgdGhlIGFkbWluL2VtYWlscyBwYWdlLlxuICpcbiAqIEBtb2R1bGUgQ29udHJvbGxlcnMvZW1haWxzX3Rvb2xiYXJcbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnZW1haWxzX3Rvb2xiYXInLFxuXHRcblx0W1xuXHRcdGd4LnNvdXJjZSArICcvbGlicy9lbWFpbHMnLFxuXHRcdCd1cmxfYXJndW1lbnRzJ1xuXHRdLFxuXHRcblx0LyoqIEBsZW5kcyBtb2R1bGU6Q29udHJvbGxlcnMvZW1haWxzX3Rvb2xiYXIgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZGFsIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JG1vZGFsID0gJCgnI2VtYWlscy1tb2RhbCcpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFRhYmxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRhYmxlID0gJCgnI2VtYWlscy10YWJsZScpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgTW9kdWxlIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE1vZHVsZSBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHtcblx0XHRcdFx0bW9kZWw6IHtcblx0XHRcdFx0XHRzZXR0aW5nczoganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1FbWFpbHMvR2V0RW1haWxTZXR0aW5ncydcblx0XHRcdFx0fVxuXHRcdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENvbnZlcnQgdGhlIGxpbmUgYnJlYWtzIHRvIDxicj4gZWxlbWVudHMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gdGV4dCBUZXh0IHRvIGJlIGNvbnZlcnRlZC5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge3N0cmluZ30gUmV0dXJucyB0aGUgY29udmVydGVkIHN0cmluZy5cblx0XHQgKi9cblx0XHR2YXIgX25sMmJyID0gZnVuY3Rpb24odGV4dCkge1xuXHRcdFx0cmV0dXJuICh0ZXh0ICsgJycpLnJlcGxhY2UoLyhbXj5cXHJcXG5dPykoXFxyXFxufFxcblxccnxcXHJ8XFxuKS9nLCAnJDEnICsgJzxicj4nICsgJyQyJyk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEaXNwbGF5IGNyZWF0ZSBuZXcgZW1haWwgbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgQ29udGFpbnMgZXZlbnQgaW5mb3JtYXRpb24uXG5cdFx0ICovXG5cdFx0dmFyIF9vbkNyZWF0ZU5ld0VtYWlsID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIFJlc2V0IG1vZGFsIGVsZW1lbnRzIHRvIGluaXRpYWwgc3RhdGUuXG5cdFx0XHRqc2UubGlicy5lbWFpbHMucmVzZXRNb2RhbCgkbW9kYWwpO1xuXHRcdFx0XG5cdFx0XHQvLyBBcHBseSBFbWFpbCBTZXR0aW5ncyB0byB0aGUgRW1haWwgTW9kYWxcblx0XHRcdGlmICh0eXBlb2YgbW9kdWxlLm1vZGVsLnNldHRpbmdzICE9PSAndW5kZWZpbmVkJyAmJiBtb2R1bGUubW9kZWwuc2V0dGluZ3MgIT09IG51bGwpIHtcblx0XHRcdFx0Ly8gU2V0IHRoZSBlbWFpbCBzaWduYXR1cmUgKGlmIG5vdCBlbXB0eSkuIFdlJ2xsIG9ubHkgc2V0IHRoZSBzaWduYXR1cmUgdG8gdGhlIENLRWRpdG9yIGJlY2F1c2Vcblx0XHRcdFx0Ly8gaWYgdGhlIHNpZ25hdHVyZSBjb250YWlucyBIVE1MIG1hcmt1cCBpdCBjYW5ub3QgYmUgc2FuaXRpemVkIHByb3Blcmx5IGZvciB0aGUgcGxhaW4gY29udGVudC5cblx0XHRcdFx0aWYgKG1vZHVsZS5tb2RlbC5zZXR0aW5ncy5zaWduYXR1cmUgIT09IG51bGwgJiYgbW9kdWxlLm1vZGVsLnNldHRpbmdzLnNpZ25hdHVyZSAhPT0gJycpIHtcblx0XHRcdFx0XHR2YXIgc2lnbmF0dXJlSHRtbCA9ICc8YnI+PHA+LS0tLS08YnI+JyArIF9ubDJicihtb2R1bGUubW9kZWwuc2V0dGluZ3Muc2lnbmF0dXJlKSArICc8L3A+Jztcblx0XHRcdFx0XHRDS0VESVRPUi5pbnN0YW5jZXNbJ2NvbnRlbnQtaHRtbCddLnNldERhdGEoc2lnbmF0dXJlSHRtbCk7XG5cdFx0XHRcdFx0dmFyIHNpZ25hdHVyZVBsYWluID0gJ1xcblxcbi0tLS0tXFxuJyArIG1vZHVsZS5tb2RlbC5zZXR0aW5ncy5zaWduYXR1cmUucmVwbGFjZSgvKDwoW14+XSspPikvZ2ksXG5cdFx0XHRcdFx0XHRcdCcnKTtcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnI2NvbnRlbnQtcGxhaW4nKS52YWwoc2lnbmF0dXJlUGxhaW4pO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBEaXNhYmxlIHRoZSBIVE1MIGNvbnRlbnQgaWYgdGhlIHNob3AgdXNlcyBvbmx5IHBsYWluIGNvbnRlbnQgZm9yIHRoZSBlbWFpbHMuXG5cdFx0XHRcdGlmIChtb2R1bGUubW9kZWwuc2V0dGluZ3MudXNlSHRtbCA9PT0gZmFsc2UpIHtcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLmNvbnRlbnQnKS5maW5kKCcudGFiLWhlYWRsaW5lOmVxKDApLCAudGFiLWNvbnRlbnQ6ZXEoMCknKS5oaWRlKCk7XG5cdFx0XHRcdFx0JG1vZGFsLmZpbmQoJy5jb250ZW50JykuZmluZCgnLnRhYi1oZWFkbGluZTplcSgxKScpLnRyaWdnZXIoJ2NsaWNrJyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdC8vIFByZWxvYWQgc2VuZGVyIGFuZCByZXBseSB0byBjb250YWN0IGRhdGEgaWYgcHJvdmlkZWQuXG5cdFx0XHRcdGlmICh0eXBlb2YgbW9kdWxlLm1vZGVsLnNldHRpbmdzLnJlcGx5QWRkcmVzcyAhPT0gJ3VuZGVmaW5lZCcgJiYgbW9kdWxlLm1vZGVsLnNldHRpbmdzLnJlcGx5QWRkcmVzcyAhPT1cblx0XHRcdFx0XHQnJykge1xuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcjc2VuZGVyLWVtYWlsLCAjcmVwbHktdG8tZW1haWwnKS52YWwobW9kdWxlLm1vZGVsLnNldHRpbmdzLnJlcGx5QWRkcmVzcyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKHR5cGVvZiBtb2R1bGUubW9kZWwuc2V0dGluZ3MucmVwbHlOYW1lICE9PSAndW5kZWZpbmVkJyAmJiBtb2R1bGUubW9kZWwuc2V0dGluZ3MucmVwbHlOYW1lICE9PVxuXHRcdFx0XHRcdCcnKSB7XG5cdFx0XHRcdFx0JG1vZGFsLmZpbmQoJyNzZW5kZXItbmFtZSwgI3JlcGx5LXRvLW5hbWUnKS52YWwobW9kdWxlLm1vZGVsLnNldHRpbmdzLnJlcGx5TmFtZSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gUHJlcGFyZSBhbmQgZGlzcGxheSBuZXcgbW9kYWwgd2luZG93LlxuXHRcdFx0JG1vZGFsLmRpYWxvZyh7XG5cdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnbmV3X21haWwnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHR3aWR0aDogMTAwMCxcblx0XHRcdFx0aGVpZ2h0OiA4MDAsXG5cdFx0XHRcdG1vZGFsOiBmYWxzZSxcblx0XHRcdFx0ZGlhbG9nQ2xhc3M6ICdneC1jb250YWluZXInLFxuXHRcdFx0XHRjbG9zZU9uRXNjYXBlOiBmYWxzZSxcblx0XHRcdFx0YnV0dG9uczoganNlLmxpYnMuZW1haWxzLmdldERlZmF1bHRNb2RhbEJ1dHRvbnMoJG1vZGFsLCAkdGFibGUpLFxuXHRcdFx0XHRvcGVuOiBqc2UubGlicy5lbWFpbHMuY29sb3JpemVCdXR0b25zRm9yRWRpdE1vZGVcblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUGVyZm9ybSBzZWFyY2ggcmVxdWVzdCBvbiB0aGUgRGF0YVRhYmxlIGluc3RhbmNlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IENvbnRhaW5zIHRoZSBldmVudCBkYXRhLlxuXHRcdCAqL1xuXHRcdHZhciBfb25UYWJsZVNlYXJjaFN1Ym1pdCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0dmFyIGtleXdvcmQgPSAkdGhpcy5maW5kKCcjc2VhcmNoLWtleXdvcmQnKS52YWwoKTtcblx0XHRcdCR0YWJsZS5EYXRhVGFibGUoKS5zZWFyY2goa2V5d29yZCkuZHJhdygpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIG1ldGhvZCBvZiB0aGUgbW9kdWxlLCBjYWxsZWQgYnkgdGhlIGVuZ2luZS5cblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdC8vIFNldCBkZWZhdWx0IFwiI2J1bGstYWN0aW9uXCIgdmFsdWUuXG5cdFx0XHQkdGhpcy5maW5kKCcjYnVsay1hY3Rpb24nKS52YWwoJycpO1xuXHRcdFx0XG5cdFx0XHQvLyBCaW5kIEV2ZW50IEhhbmRsZXJzXG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQub24oJ2NsaWNrJywgJyNjcmVhdGUtbmV3LWVtYWlsJywgX29uQ3JlYXRlTmV3RW1haWwpXG5cdFx0XHRcdC5vbignc3VibWl0JywgJyNxdWljay1zZWFyY2gnLCBfb25UYWJsZVNlYXJjaFN1Ym1pdCk7XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrIGlmIHRoZSBcIm1haWxfdG9cIiBwYXJhbWV0ZXIgaXMgcHJlc2VudCBhbmQgcHJvY2VzcyBpdHMgdmFsdWUgd2l0aGluIHRoZSBuZXcgZW1haWwgbW9kYWwgbGF5ZXIuXG5cdFx0XHR2YXIgZ2V0UGFyYW1ldGVycyA9IGpzZS5saWJzLnVybF9hcmd1bWVudHMuZ2V0VXJsUGFyYW1ldGVycygpO1xuXHRcdFx0aWYgKHR5cGVvZiBnZXRQYXJhbWV0ZXJzLm1haWx0byAhPT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdFx0X29uQ3JlYXRlTmV3RW1haWwoe30pOyAvLyBEaXNwbGF5IHRoZSBuZXcgZW1haWwgbW9kYWwuXG5cdFx0XHRcdCRtb2RhbC5maW5kKCcjcmVjaXBpZW50LWVtYWlsJykudmFsKGdldFBhcmFtZXRlcnMubWFpbHRvKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIG1vZHVsZSBvYmplY3QgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
