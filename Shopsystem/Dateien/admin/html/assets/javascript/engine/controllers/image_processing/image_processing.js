'use strict';

/* --------------------------------------------------------------
 image_processing.js 2017-03-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Image Processing
 *
 * This module will execute the image processing by sending POST-Requests to the
 * ImageProcessingController interface
 *
 * @module Controllers/image_processing
 */
gx.controllers.module('image_processing', [gx.source + '/libs/info_messages'],

/**  @lends module:Controllers/image_processing */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Flag if an error occurred during the image processing
  *
  * @type {boolean}
  */
	error = false,


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Reference to the info messages library
  * 
  * @type {object}
  */
	messages = jse.libs.info_messages,


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	var _onClick = function _onClick() {
		var title = jse.core.lang.translate('image_processing_title', 'image_processing');
		var startimageNr = parseInt($('#image-processing-startimage-nr').val());
		var startimageFile = $('#image-processing-startimage-file').val();

		$('.process-modal').dialog({
			'title': title,
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': [{
				'text': jse.core.lang.translate('close', 'buttons'),
				'class': 'btn',
				'click': function click() {
					$(this).dialog('close');
				}
			}],
			'width': 580
		});

		$('#image-processing-file').html(' ');

		if (startimageNr <= 0 || isNaN(startimageNr)) {
			startimageNr = 1;
		}

		if (startimageFile != '') {
			_processImage(0, startimageFile);
		} else {
			_processImage(startimageNr, '');
		}
	};

	// ------------------------------------------------------------------------
	// AJAX
	// ------------------------------------------------------------------------

	var _processImage = function _processImage(imageNumber, imageFile) {

		$.ajax({
			'type': 'POST',
			'url': 'admin.php?do=ImageProcessing/Process',
			'timeout': 30000,
			'dataType': 'json',
			'context': this,
			'data': {
				'image_number': imageNumber,
				'image_file': imageFile
			},
			success: function success(response) {
				if (response.payload.nextImageNr != 0) {
					imageNumber = response.payload.nextImageNr - 1;
				}

				var progress = 100 / response.payload.imagesCount * imageNumber;
				progress = Math.round(progress);

				$('.process-modal .progress-bar').attr('aria-valuenow', progress);
				$('.process-modal .progress-bar').css('min-width', '70px');
				$('.process-modal .progress-bar').css('width', progress + '%');
				$('.process-modal .progress-bar').html(imageNumber + ' / ' + response.payload.imagesCount);
				$('#image-processing-file').html(response.payload.imageName);

				if (!response.success) {
					error = true;
				}

				if (!response.payload.finished) {
					// check is there is nextImageNr not 0 (only if filename was given)
					if (response.payload.nextImageNr != 0) {
						_processImage(response.payload.nextImageNr, '');
					} else {
						imageNumber += 1;
						_processImage(imageNumber, '');
					}
				} else {
					$('.process-modal').dialog('close');
					$('.process-modal .progress-bar').attr('aria-valuenow', 0);
					$('.process-modal .progress-bar').css('width', '0%');
					$('.process-modal .progress-bar').html('');

					if (error) {
						if (response.payload.fileNotFound) {
							messages.addError(jse.core.lang.translate('image_processing_file_not_found', 'image_processing'));
						} else {
							messages.addError(jse.core.lang.translate('image_processing_error', 'image_processing'));
						}
					} else {
						messages.addSuccess(jse.core.lang.translate('image_processing_success', 'image_processing'));
					}

					error = false;
				}
			}
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.js-process', _onClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImltYWdlX3Byb2Nlc3NpbmcvaW1hZ2VfcHJvY2Vzc2luZy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwiZXJyb3IiLCJvcHRpb25zIiwiZXh0ZW5kIiwibWVzc2FnZXMiLCJqc2UiLCJsaWJzIiwiaW5mb19tZXNzYWdlcyIsIl9vbkNsaWNrIiwidGl0bGUiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsInN0YXJ0aW1hZ2VOciIsInBhcnNlSW50IiwidmFsIiwic3RhcnRpbWFnZUZpbGUiLCJkaWFsb2ciLCJodG1sIiwiaXNOYU4iLCJfcHJvY2Vzc0ltYWdlIiwiaW1hZ2VOdW1iZXIiLCJpbWFnZUZpbGUiLCJhamF4Iiwic3VjY2VzcyIsInJlc3BvbnNlIiwicGF5bG9hZCIsIm5leHRJbWFnZU5yIiwicHJvZ3Jlc3MiLCJpbWFnZXNDb3VudCIsIk1hdGgiLCJyb3VuZCIsImF0dHIiLCJjc3MiLCJpbWFnZU5hbWUiLCJmaW5pc2hlZCIsImZpbGVOb3RGb3VuZCIsImFkZEVycm9yIiwiYWRkU3VjY2VzcyIsImluaXQiLCJkb25lIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7QUFRQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msa0JBREQsRUFHQyxDQUNDRixHQUFHRyxNQUFILEdBQVkscUJBRGIsQ0FIRDs7QUFPQzs7QUFFQSxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsWUFBVyxFQWJaOzs7QUFlQzs7Ozs7QUFLQUMsU0FBUSxLQXBCVDs7O0FBc0JDOzs7OztBQUtBQyxXQUFVSCxFQUFFSSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJILFFBQW5CLEVBQTZCSCxJQUE3QixDQTNCWDs7O0FBNkJDOzs7OztBQUtBTyxZQUFXQyxJQUFJQyxJQUFKLENBQVNDLGFBbENyQjs7O0FBb0NDOzs7OztBQUtBWixVQUFTLEVBekNWOztBQTJDQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSWEsV0FBVyxTQUFYQSxRQUFXLEdBQVc7QUFDekIsTUFBSUMsUUFBUUosSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isd0JBQXhCLEVBQWtELGtCQUFsRCxDQUFaO0FBQ0EsTUFBSUMsZUFBZUMsU0FBU2YsRUFBRSxpQ0FBRixFQUFxQ2dCLEdBQXJDLEVBQVQsQ0FBbkI7QUFDQSxNQUFJQyxpQkFBaUJqQixFQUFFLG1DQUFGLEVBQXVDZ0IsR0FBdkMsRUFBckI7O0FBRUFoQixJQUFFLGdCQUFGLEVBQW9Ca0IsTUFBcEIsQ0FBMkI7QUFDMUIsWUFBU1IsS0FEaUI7QUFFMUIsWUFBUyxJQUZpQjtBQUcxQixrQkFBZSxjQUhXO0FBSTFCLGNBQVcsQ0FDVjtBQUNDLFlBQVFKLElBQUlLLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFNBQWpDLENBRFQ7QUFFQyxhQUFTLEtBRlY7QUFHQyxhQUFTLGlCQUFXO0FBQ25CYixPQUFFLElBQUYsRUFBUWtCLE1BQVIsQ0FBZSxPQUFmO0FBQ0E7QUFMRixJQURVLENBSmU7QUFhMUIsWUFBUztBQWJpQixHQUEzQjs7QUFnQkFsQixJQUFFLHdCQUFGLEVBQTRCbUIsSUFBNUIsQ0FBaUMsR0FBakM7O0FBRUEsTUFBSUwsZ0JBQWdCLENBQWhCLElBQXFCTSxNQUFNTixZQUFOLENBQXpCLEVBQThDO0FBQzdDQSxrQkFBZSxDQUFmO0FBQ0E7O0FBRUQsTUFBSUcsa0JBQWtCLEVBQXRCLEVBQTBCO0FBQ3pCSSxpQkFBYyxDQUFkLEVBQWlCSixjQUFqQjtBQUNBLEdBRkQsTUFFTztBQUNOSSxpQkFBY1AsWUFBZCxFQUE0QixFQUE1QjtBQUNBO0FBQ0QsRUFoQ0Q7O0FBa0NBO0FBQ0E7QUFDQTs7QUFFQSxLQUFJTyxnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVNDLFdBQVQsRUFBc0JDLFNBQXRCLEVBQWlDOztBQUVwRHZCLElBQUV3QixJQUFGLENBQU87QUFDTixXQUFRLE1BREY7QUFFTixVQUFPLHNDQUZEO0FBR04sY0FBVyxLQUhMO0FBSU4sZUFBWSxNQUpOO0FBS04sY0FBVyxJQUxMO0FBTU4sV0FBUTtBQUNQLG9CQUFnQkYsV0FEVDtBQUVQLGtCQUFjQztBQUZQLElBTkY7QUFVTkUsWUFBUyxpQkFBU0MsUUFBVCxFQUFtQjtBQUMzQixRQUFJQSxTQUFTQyxPQUFULENBQWlCQyxXQUFqQixJQUFnQyxDQUFwQyxFQUF1QztBQUN0Q04sbUJBQWNJLFNBQVNDLE9BQVQsQ0FBaUJDLFdBQWpCLEdBQStCLENBQTdDO0FBQ0E7O0FBRUQsUUFBSUMsV0FBWSxNQUFNSCxTQUFTQyxPQUFULENBQWlCRyxXQUF4QixHQUF1Q1IsV0FBdEQ7QUFDQU8sZUFBV0UsS0FBS0MsS0FBTCxDQUFXSCxRQUFYLENBQVg7O0FBRUE3QixNQUFFLDhCQUFGLEVBQWtDaUMsSUFBbEMsQ0FBdUMsZUFBdkMsRUFBd0RKLFFBQXhEO0FBQ0E3QixNQUFFLDhCQUFGLEVBQWtDa0MsR0FBbEMsQ0FBc0MsV0FBdEMsRUFBbUQsTUFBbkQ7QUFDQWxDLE1BQUUsOEJBQUYsRUFBa0NrQyxHQUFsQyxDQUFzQyxPQUF0QyxFQUErQ0wsV0FBVyxHQUExRDtBQUNlN0IsTUFBRSw4QkFBRixFQUFrQ21CLElBQWxDLENBQXVDRyxjQUFjLEtBQWQsR0FBc0JJLFNBQVNDLE9BQVQsQ0FBaUJHLFdBQTlFO0FBQ0E5QixNQUFFLHdCQUFGLEVBQTRCbUIsSUFBNUIsQ0FBaUNPLFNBQVNDLE9BQVQsQ0FBaUJRLFNBQWxEOztBQUVmLFFBQUksQ0FBQ1QsU0FBU0QsT0FBZCxFQUF1QjtBQUN0QnZCLGFBQVEsSUFBUjtBQUNBOztBQUVELFFBQUksQ0FBQ3dCLFNBQVNDLE9BQVQsQ0FBaUJTLFFBQXRCLEVBQWdDO0FBQy9CO0FBQ0EsU0FBSVYsU0FBU0MsT0FBVCxDQUFpQkMsV0FBakIsSUFBZ0MsQ0FBcEMsRUFBdUM7QUFDdENQLG9CQUFjSyxTQUFTQyxPQUFULENBQWlCQyxXQUEvQixFQUE0QyxFQUE1QztBQUNBLE1BRkQsTUFFTztBQUNOTixxQkFBZSxDQUFmO0FBQ0FELG9CQUFjQyxXQUFkLEVBQTJCLEVBQTNCO0FBQ0E7QUFDRCxLQVJELE1BUU87QUFDTnRCLE9BQUUsZ0JBQUYsRUFBb0JrQixNQUFwQixDQUEyQixPQUEzQjtBQUNBbEIsT0FBRSw4QkFBRixFQUFrQ2lDLElBQWxDLENBQXVDLGVBQXZDLEVBQXdELENBQXhEO0FBQ0FqQyxPQUFFLDhCQUFGLEVBQWtDa0MsR0FBbEMsQ0FBc0MsT0FBdEMsRUFBK0MsSUFBL0M7QUFDQWxDLE9BQUUsOEJBQUYsRUFBa0NtQixJQUFsQyxDQUF1QyxFQUF2Qzs7QUFFQSxTQUFJakIsS0FBSixFQUFXO0FBQ1YsVUFBSXdCLFNBQVNDLE9BQVQsQ0FBaUJVLFlBQXJCLEVBQW1DO0FBQ2xDaEMsZ0JBQVNpQyxRQUFULENBQWtCaEMsSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsaUNBQXhCLEVBQ2pCLGtCQURpQixDQUFsQjtBQUVBLE9BSEQsTUFHTztBQUNOUixnQkFBU2lDLFFBQVQsQ0FBa0JoQyxJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix3QkFBeEIsRUFDakIsa0JBRGlCLENBQWxCO0FBRUE7QUFDRCxNQVJELE1BUU87QUFDTlIsZUFBU2tDLFVBQVQsQ0FBb0JqQyxJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwwQkFBeEIsRUFDbkIsa0JBRG1CLENBQXBCO0FBRUE7O0FBRURYLGFBQVEsS0FBUjtBQUNBO0FBQ0Q7QUF6REssR0FBUDtBQTJEQSxFQTdERDs7QUErREE7QUFDQTtBQUNBOztBQUVBTixRQUFPNEMsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjFDLFFBQU0yQyxFQUFOLENBQVMsT0FBVCxFQUFrQixhQUFsQixFQUFpQ2pDLFFBQWpDO0FBQ0FnQztBQUNBLEVBSEQ7O0FBS0EsUUFBTzdDLE1BQVA7QUFDQSxDQS9LRiIsImZpbGUiOiJpbWFnZV9wcm9jZXNzaW5nL2ltYWdlX3Byb2Nlc3NpbmcuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGltYWdlX3Byb2Nlc3NpbmcuanMgMjAxNy0wMy0wOFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgSW1hZ2UgUHJvY2Vzc2luZ1xuICpcbiAqIFRoaXMgbW9kdWxlIHdpbGwgZXhlY3V0ZSB0aGUgaW1hZ2UgcHJvY2Vzc2luZyBieSBzZW5kaW5nIFBPU1QtUmVxdWVzdHMgdG8gdGhlXG4gKiBJbWFnZVByb2Nlc3NpbmdDb250cm9sbGVyIGludGVyZmFjZVxuICpcbiAqIEBtb2R1bGUgQ29udHJvbGxlcnMvaW1hZ2VfcHJvY2Vzc2luZ1xuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdpbWFnZV9wcm9jZXNzaW5nJyxcblx0XG5cdFtcblx0XHRneC5zb3VyY2UgKyAnL2xpYnMvaW5mb19tZXNzYWdlcydcblx0XSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb250cm9sbGVycy9pbWFnZV9wcm9jZXNzaW5nICovXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFUyBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0ge30sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmxhZyBpZiBhbiBlcnJvciBvY2N1cnJlZCBkdXJpbmcgdGhlIGltYWdlIHByb2Nlc3Npbmdcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7Ym9vbGVhbn1cblx0XHRcdCAqL1xuXHRcdFx0ZXJyb3IgPSBmYWxzZSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBSZWZlcmVuY2UgdG8gdGhlIGluZm8gbWVzc2FnZXMgbGlicmFyeVxuXHRcdFx0ICogXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtZXNzYWdlcyA9IGpzZS5saWJzLmluZm9fbWVzc2FnZXMsXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyIF9vbkNsaWNrID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgdGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnaW1hZ2VfcHJvY2Vzc2luZ190aXRsZScsICdpbWFnZV9wcm9jZXNzaW5nJyk7XG5cdFx0XHR2YXIgc3RhcnRpbWFnZU5yID0gcGFyc2VJbnQoJCgnI2ltYWdlLXByb2Nlc3Npbmctc3RhcnRpbWFnZS1ucicpLnZhbCgpKTtcblx0XHRcdHZhciBzdGFydGltYWdlRmlsZSA9ICQoJyNpbWFnZS1wcm9jZXNzaW5nLXN0YXJ0aW1hZ2UtZmlsZScpLnZhbCgpO1xuXHRcdFx0XG5cdFx0XHQkKCcucHJvY2Vzcy1tb2RhbCcpLmRpYWxvZyh7XG5cdFx0XHRcdCd0aXRsZSc6IHRpdGxlLFxuXHRcdFx0XHQnbW9kYWwnOiB0cnVlLFxuXHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0J2J1dHRvbnMnOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY2xvc2UnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0J2NsYXNzJzogJ2J0bicsXG5cdFx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdLFxuXHRcdFx0XHQnd2lkdGgnOiA1ODBcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkKCcjaW1hZ2UtcHJvY2Vzc2luZy1maWxlJykuaHRtbCgnICcpO1xuXHRcdFx0XG5cdFx0XHRpZiAoc3RhcnRpbWFnZU5yIDw9IDAgfHwgaXNOYU4oc3RhcnRpbWFnZU5yKSkge1xuXHRcdFx0XHRzdGFydGltYWdlTnIgPSAxO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAoc3RhcnRpbWFnZUZpbGUgIT0gJycpIHtcblx0XHRcdFx0X3Byb2Nlc3NJbWFnZSgwLCBzdGFydGltYWdlRmlsZSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRfcHJvY2Vzc0ltYWdlKHN0YXJ0aW1hZ2VOciwgJycpO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gQUpBWFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhciBfcHJvY2Vzc0ltYWdlID0gZnVuY3Rpb24oaW1hZ2VOdW1iZXIsIGltYWdlRmlsZSkge1xuXHRcdFx0XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHQndHlwZSc6ICdQT1NUJyxcblx0XHRcdFx0J3VybCc6ICdhZG1pbi5waHA/ZG89SW1hZ2VQcm9jZXNzaW5nL1Byb2Nlc3MnLFxuXHRcdFx0XHQndGltZW91dCc6IDMwMDAwLFxuXHRcdFx0XHQnZGF0YVR5cGUnOiAnanNvbicsXG5cdFx0XHRcdCdjb250ZXh0JzogdGhpcyxcblx0XHRcdFx0J2RhdGEnOiB7XG5cdFx0XHRcdFx0J2ltYWdlX251bWJlcic6IGltYWdlTnVtYmVyLFxuXHRcdFx0XHRcdCdpbWFnZV9maWxlJzogaW1hZ2VGaWxlXG5cdFx0XHRcdH0sXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnBheWxvYWQubmV4dEltYWdlTnIgIT0gMCkge1xuXHRcdFx0XHRcdFx0aW1hZ2VOdW1iZXIgPSByZXNwb25zZS5wYXlsb2FkLm5leHRJbWFnZU5yIC0gMTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0dmFyIHByb2dyZXNzID0gKDEwMCAvIHJlc3BvbnNlLnBheWxvYWQuaW1hZ2VzQ291bnQpICogaW1hZ2VOdW1iZXI7XG5cdFx0XHRcdFx0cHJvZ3Jlc3MgPSBNYXRoLnJvdW5kKHByb2dyZXNzKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkKCcucHJvY2Vzcy1tb2RhbCAucHJvZ3Jlc3MtYmFyJykuYXR0cignYXJpYS12YWx1ZW5vdycsIHByb2dyZXNzKTtcblx0XHRcdFx0XHQkKCcucHJvY2Vzcy1tb2RhbCAucHJvZ3Jlc3MtYmFyJykuY3NzKCdtaW4td2lkdGgnLCAnNzBweCcpO1xuXHRcdFx0XHRcdCQoJy5wcm9jZXNzLW1vZGFsIC5wcm9ncmVzcy1iYXInKS5jc3MoJ3dpZHRoJywgcHJvZ3Jlc3MgKyAnJScpO1xuICAgICAgICAgICAgICAgICAgICAkKCcucHJvY2Vzcy1tb2RhbCAucHJvZ3Jlc3MtYmFyJykuaHRtbChpbWFnZU51bWJlciArICcgLyAnICsgcmVzcG9uc2UucGF5bG9hZC5pbWFnZXNDb3VudCk7XG4gICAgICAgICAgICAgICAgICAgICQoJyNpbWFnZS1wcm9jZXNzaW5nLWZpbGUnKS5odG1sKHJlc3BvbnNlLnBheWxvYWQuaW1hZ2VOYW1lKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAoIXJlc3BvbnNlLnN1Y2Nlc3MpIHtcblx0XHRcdFx0XHRcdGVycm9yID0gdHJ1ZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKCFyZXNwb25zZS5wYXlsb2FkLmZpbmlzaGVkKSB7XG5cdFx0XHRcdFx0XHQvLyBjaGVjayBpcyB0aGVyZSBpcyBuZXh0SW1hZ2VOciBub3QgMCAob25seSBpZiBmaWxlbmFtZSB3YXMgZ2l2ZW4pXG5cdFx0XHRcdFx0XHRpZiAocmVzcG9uc2UucGF5bG9hZC5uZXh0SW1hZ2VOciAhPSAwKSB7XG5cdFx0XHRcdFx0XHRcdF9wcm9jZXNzSW1hZ2UocmVzcG9uc2UucGF5bG9hZC5uZXh0SW1hZ2VOciwgJycpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0aW1hZ2VOdW1iZXIgKz0gMTtcblx0XHRcdFx0XHRcdFx0X3Byb2Nlc3NJbWFnZShpbWFnZU51bWJlciwgJycpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHQkKCcucHJvY2Vzcy1tb2RhbCcpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHRcdCQoJy5wcm9jZXNzLW1vZGFsIC5wcm9ncmVzcy1iYXInKS5hdHRyKCdhcmlhLXZhbHVlbm93JywgMCk7XG5cdFx0XHRcdFx0XHQkKCcucHJvY2Vzcy1tb2RhbCAucHJvZ3Jlc3MtYmFyJykuY3NzKCd3aWR0aCcsICcwJScpO1xuXHRcdFx0XHRcdFx0JCgnLnByb2Nlc3MtbW9kYWwgLnByb2dyZXNzLWJhcicpLmh0bWwoJycpO1xuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRpZiAoZXJyb3IpIHtcblx0XHRcdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnBheWxvYWQuZmlsZU5vdEZvdW5kKSB7XG5cdFx0XHRcdFx0XHRcdFx0bWVzc2FnZXMuYWRkRXJyb3IoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2ltYWdlX3Byb2Nlc3NpbmdfZmlsZV9ub3RfZm91bmQnLFxuXHRcdFx0XHRcdFx0XHRcdFx0J2ltYWdlX3Byb2Nlc3NpbmcnKSk7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0bWVzc2FnZXMuYWRkRXJyb3IoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2ltYWdlX3Byb2Nlc3NpbmdfZXJyb3InLFxuXHRcdFx0XHRcdFx0XHRcdFx0J2ltYWdlX3Byb2Nlc3NpbmcnKSk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdG1lc3NhZ2VzLmFkZFN1Y2Nlc3MoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2ltYWdlX3Byb2Nlc3Npbmdfc3VjY2VzcycsXG5cdFx0XHRcdFx0XHRcdFx0J2ltYWdlX3Byb2Nlc3NpbmcnKSk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGVycm9yID0gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkdGhpcy5vbignY2xpY2snLCAnLmpzLXByb2Nlc3MnLCBfb25DbGljayk7XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
