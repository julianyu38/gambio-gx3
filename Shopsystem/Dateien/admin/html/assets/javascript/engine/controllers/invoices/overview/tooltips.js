'use strict';

/* --------------------------------------------------------------
 tooltip.js 2017-05-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Invoices Table Tooltip
 *
 * This controller displays tooltips for the invoices overview table. The tooltips are loaded after the
 * table data request is ready for optimization purposes.
 */
gx.controllers.module('tooltips', [jse.source + '/vendor/qtip2/jquery.qtip.css', jse.source + '/vendor/qtip2/jquery.qtip.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @var {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		sourceUrl: jse.core.config.get('appUrl') + '/admin/admin.php?do=InvoicesOverviewAjax/Tooltips',
		selectors: {
			mouseenter: {
				invoiceItems: '.tooltip-invoice-items',
				customerMemos: '.tooltip-customer-memos',
				customerAddresses: '.tooltip-customer-addresses',
				orderStatusHistory: '.tooltip-invoice-status-history'
			},
			click: {
				trackingLinks: '.tooltip-tracking-links'
			}
		}
	};

	/**
  * Final Options
  *
  * @var {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Tooltip Contents
  *
  * Contains the rendered HTML of the tooltips. The HTML is rendered with each table draw.
  *
  * e.g. tooltips.400210.invoiceItems >> HTML for invoice items tooltip of invoice #400210.
  *
  * @type {Object}
  */
	var tooltips = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Target Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTargetPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'left' : 'right';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'top' : 'bottom';

		return horizontal + ' ' + vertical;
	}

	/**
  * Get Tooltip Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTooltipPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'right' : 'left';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'bottom' : 'top';

		return horizontal + ' ' + vertical;
	}

	/**
  * If there is only one link then open it in a new tab. 
  */
	function _onTrackingLinksClick() {
		var trackingLinks = $(this).parents('tr').data('trackingLinks');

		if (trackingLinks.length === 1) {
			window.open(trackingLinks[0], '_blank');
		}
	}

	/**
  * Initialize tooltip for static table data.
  *
  * Replaces the browsers default tooltip with a qTip instance for every element on the table which has
  * a title attribute.
  */
	function _initTooltipsForStaticContent() {
		$this.find('tbody [title]').qtip({
			style: { classes: 'gx-qtip info' }
		});
	}

	/**
  * Show Tooltip
  *
  * Display the Qtip instance of the target. The tooltip contents are fetched after the table request
  * is finished for performance reasons. This method will not show anything until the tooltip contents
  * are fetched.
  *
  * @param {jQuery.Event} event
  */
	function _showTooltip(event) {
		event.stopPropagation();

		var invoiceId = $(this).parents('tr').data('invoiceId');

		if (!tooltips[invoiceId]) {
			return; // The requested tooltip is not loaded, do not continue.
		}

		var tooltipPosition = _getTooltipPosition($(this));
		var targetPosition = _getTargetPosition($(this));

		$(this).qtip({
			content: tooltips[invoiceId][event.data.name],
			style: {
				classes: 'gx-qtip info'
			},
			position: {
				my: tooltipPosition,
				at: targetPosition,
				effect: false,
				viewport: $(window),
				adjust: {
					method: 'none shift'
				}
			},
			hide: {
				fixed: true,
				delay: 300
			},
			show: {
				ready: true,
				delay: 100
			},
			events: {
				hidden: function hidden(event, api) {
					api.destroy(true);
				}
			}
		});
	}

	/**
  * Get Tooltips
  *
  * Fetches the tooltips with an AJAX request, based on the current state of the table.
  */
	function _getTooltips() {
		tooltips = [];
		var datatablesXhrParameters = $this.DataTable().ajax.params();
		$.post(options.sourceUrl, datatablesXhrParameters, function (response) {
			return tooltips = response;
		}, 'json');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('draw.dt', _initTooltipsForStaticContent).on('xhr.dt', _getTooltips).on('click', '.tooltip-tracking-links', _onTrackingLinksClick);

		$(window).on('JSENGINE_INIT_FINISHED', function () {
			if ($this.DataTable().ajax.json() !== undefined && tooltips === undefined) {
				_getTooltips();
			}
		});

		for (var event in options.selectors) {
			for (var name in options.selectors[event]) {
				$this.on(event, options.selectors[event][name], { name: name }, _showTooltip);
			}
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL292ZXJ2aWV3L3Rvb2x0aXBzLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJzb3VyY2VVcmwiLCJjb3JlIiwiY29uZmlnIiwiZ2V0Iiwic2VsZWN0b3JzIiwibW91c2VlbnRlciIsImludm9pY2VJdGVtcyIsImN1c3RvbWVyTWVtb3MiLCJjdXN0b21lckFkZHJlc3NlcyIsIm9yZGVyU3RhdHVzSGlzdG9yeSIsImNsaWNrIiwidHJhY2tpbmdMaW5rcyIsIm9wdGlvbnMiLCJleHRlbmQiLCJ0b29sdGlwcyIsIl9nZXRUYXJnZXRQb3NpdGlvbiIsIiR0YXJnZXQiLCJob3Jpem9udGFsIiwib2Zmc2V0IiwibGVmdCIsIndpbmRvdyIsInNjcm9sbExlZnQiLCJ3aWR0aCIsInZlcnRpY2FsIiwidG9wIiwic2Nyb2xsVG9wIiwiaGVpZ2h0IiwiX2dldFRvb2x0aXBQb3NpdGlvbiIsIl9vblRyYWNraW5nTGlua3NDbGljayIsInBhcmVudHMiLCJsZW5ndGgiLCJvcGVuIiwiX2luaXRUb29sdGlwc0ZvclN0YXRpY0NvbnRlbnQiLCJmaW5kIiwicXRpcCIsInN0eWxlIiwiY2xhc3NlcyIsIl9zaG93VG9vbHRpcCIsImV2ZW50Iiwic3RvcFByb3BhZ2F0aW9uIiwiaW52b2ljZUlkIiwidG9vbHRpcFBvc2l0aW9uIiwidGFyZ2V0UG9zaXRpb24iLCJjb250ZW50IiwibmFtZSIsInBvc2l0aW9uIiwibXkiLCJhdCIsImVmZmVjdCIsInZpZXdwb3J0IiwiYWRqdXN0IiwibWV0aG9kIiwiaGlkZSIsImZpeGVkIiwiZGVsYXkiLCJzaG93IiwicmVhZHkiLCJldmVudHMiLCJoaWRkZW4iLCJhcGkiLCJkZXN0cm95IiwiX2dldFRvb2x0aXBzIiwiZGF0YXRhYmxlc1hoclBhcmFtZXRlcnMiLCJEYXRhVGFibGUiLCJhamF4IiwicGFyYW1zIiwicG9zdCIsInJlc3BvbnNlIiwiaW5pdCIsImRvbmUiLCJvbiIsImpzb24iLCJ1bmRlZmluZWQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7O0FBTUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFVBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLG9DQUVJRCxJQUFJQyxNQUZSLGtDQUhELEVBUUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsV0FBVztBQUNoQkMsYUFBV04sSUFBSU8sSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxtREFEM0I7QUFFaEJDLGFBQVc7QUFDVkMsZUFBWTtBQUNYQyxrQkFBYyx3QkFESDtBQUVYQyxtQkFBZSx5QkFGSjtBQUdYQyx1QkFBbUIsNkJBSFI7QUFJWEMsd0JBQW9CO0FBSlQsSUFERjtBQU9WQyxVQUFPO0FBQ05DLG1CQUFlO0FBRFQ7QUFQRztBQUZLLEVBQWpCOztBQWVBOzs7OztBQUtBLEtBQU1DLFVBQVVkLEVBQUVlLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQmQsUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVMsRUFBZjs7QUFFQTs7Ozs7Ozs7O0FBU0EsS0FBSXFCLGlCQUFKOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVNDLGtCQUFULENBQTRCQyxPQUE1QixFQUFxQztBQUNwQyxNQUFNQyxhQUFhRCxRQUFRRSxNQUFSLEdBQWlCQyxJQUFqQixHQUF3QnJCLEVBQUVzQixNQUFGLEVBQVVDLFVBQVYsRUFBeEIsR0FBaUR2QixFQUFFc0IsTUFBRixFQUFVRSxLQUFWLEtBQW9CLENBQXJFLEdBQ2YsTUFEZSxHQUVmLE9BRko7QUFHQSxNQUFNQyxXQUFXUCxRQUFRRSxNQUFSLEdBQWlCTSxHQUFqQixHQUF1QjFCLEVBQUVzQixNQUFGLEVBQVVLLFNBQVYsRUFBdkIsR0FBK0MzQixFQUFFc0IsTUFBRixFQUFVTSxNQUFWLEtBQXFCLENBQXBFLEdBQ2IsS0FEYSxHQUViLFFBRko7O0FBSUEsU0FBT1QsYUFBYSxHQUFiLEdBQW1CTSxRQUExQjtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0ksbUJBQVQsQ0FBNkJYLE9BQTdCLEVBQXNDO0FBQ3JDLE1BQU1DLGFBQWFELFFBQVFFLE1BQVIsR0FBaUJDLElBQWpCLEdBQXdCckIsRUFBRXNCLE1BQUYsRUFBVUMsVUFBVixFQUF4QixHQUFpRHZCLEVBQUVzQixNQUFGLEVBQVVFLEtBQVYsS0FBb0IsQ0FBckUsR0FDZixPQURlLEdBRWYsTUFGSjtBQUdBLE1BQU1DLFdBQVdQLFFBQVFFLE1BQVIsR0FBaUJNLEdBQWpCLEdBQXVCMUIsRUFBRXNCLE1BQUYsRUFBVUssU0FBVixFQUF2QixHQUErQzNCLEVBQUVzQixNQUFGLEVBQVVNLE1BQVYsS0FBcUIsQ0FBcEUsR0FDYixRQURhLEdBRWIsS0FGSjs7QUFJQSxTQUFPVCxhQUFhLEdBQWIsR0FBbUJNLFFBQTFCO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNLLHFCQUFULEdBQWlDO0FBQ2hDLE1BQU1qQixnQkFBZ0JiLEVBQUUsSUFBRixFQUFRK0IsT0FBUixDQUFnQixJQUFoQixFQUFzQmpDLElBQXRCLENBQTJCLGVBQTNCLENBQXRCOztBQUVBLE1BQUllLGNBQWNtQixNQUFkLEtBQXlCLENBQTdCLEVBQWdDO0FBQy9CVixVQUFPVyxJQUFQLENBQVlwQixjQUFjLENBQWQsQ0FBWixFQUE4QixRQUE5QjtBQUNBO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFVBQVNxQiw2QkFBVCxHQUF5QztBQUN4Q25DLFFBQU1vQyxJQUFOLENBQVcsZUFBWCxFQUE0QkMsSUFBNUIsQ0FBaUM7QUFDaENDLFVBQU8sRUFBQ0MsU0FBUyxjQUFWO0FBRHlCLEdBQWpDO0FBR0E7O0FBRUQ7Ozs7Ozs7OztBQVNBLFVBQVNDLFlBQVQsQ0FBc0JDLEtBQXRCLEVBQTZCO0FBQzVCQSxRQUFNQyxlQUFOOztBQUVBLE1BQU1DLFlBQVkxQyxFQUFFLElBQUYsRUFBUStCLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JqQyxJQUF0QixDQUEyQixXQUEzQixDQUFsQjs7QUFFQSxNQUFJLENBQUNrQixTQUFTMEIsU0FBVCxDQUFMLEVBQTBCO0FBQ3pCLFVBRHlCLENBQ2pCO0FBQ1I7O0FBRUQsTUFBTUMsa0JBQWtCZCxvQkFBb0I3QixFQUFFLElBQUYsQ0FBcEIsQ0FBeEI7QUFDQSxNQUFNNEMsaUJBQWlCM0IsbUJBQW1CakIsRUFBRSxJQUFGLENBQW5CLENBQXZCOztBQUVBQSxJQUFFLElBQUYsRUFBUW9DLElBQVIsQ0FBYTtBQUNaUyxZQUFTN0IsU0FBUzBCLFNBQVQsRUFBb0JGLE1BQU0xQyxJQUFOLENBQVdnRCxJQUEvQixDQURHO0FBRVpULFVBQU87QUFDTkMsYUFBUztBQURILElBRks7QUFLWlMsYUFBVTtBQUNUQyxRQUFJTCxlQURLO0FBRVRNLFFBQUlMLGNBRks7QUFHVE0sWUFBUSxLQUhDO0FBSVRDLGNBQVVuRCxFQUFFc0IsTUFBRixDQUpEO0FBS1Q4QixZQUFRO0FBQ1BDLGFBQVE7QUFERDtBQUxDLElBTEU7QUFjWkMsU0FBTTtBQUNMQyxXQUFPLElBREY7QUFFTEMsV0FBTztBQUZGLElBZE07QUFrQlpDLFNBQU07QUFDTEMsV0FBTyxJQURGO0FBRUxGLFdBQU87QUFGRixJQWxCTTtBQXNCWkcsV0FBUTtBQUNQQyxZQUFRLGdCQUFDcEIsS0FBRCxFQUFRcUIsR0FBUixFQUFnQjtBQUN2QkEsU0FBSUMsT0FBSixDQUFZLElBQVo7QUFDQTtBQUhNO0FBdEJJLEdBQWI7QUE0QkE7O0FBRUQ7Ozs7O0FBS0EsVUFBU0MsWUFBVCxHQUF3QjtBQUN2Qi9DLGFBQVcsRUFBWDtBQUNBLE1BQU1nRCwwQkFBMEJqRSxNQUFNa0UsU0FBTixHQUFrQkMsSUFBbEIsQ0FBdUJDLE1BQXZCLEVBQWhDO0FBQ0FuRSxJQUFFb0UsSUFBRixDQUFPdEQsUUFBUVosU0FBZixFQUEwQjhELHVCQUExQixFQUFtRDtBQUFBLFVBQVloRCxXQUFXcUQsUUFBdkI7QUFBQSxHQUFuRCxFQUFvRixNQUFwRjtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTFFLFFBQU8yRSxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCeEUsUUFDRXlFLEVBREYsQ0FDSyxTQURMLEVBQ2dCdEMsNkJBRGhCLEVBRUVzQyxFQUZGLENBRUssUUFGTCxFQUVlVCxZQUZmLEVBR0VTLEVBSEYsQ0FHSyxPQUhMLEVBR2MseUJBSGQsRUFHeUMxQyxxQkFIekM7O0FBS0E5QixJQUFFc0IsTUFBRixFQUFVa0QsRUFBVixDQUFhLHdCQUFiLEVBQXVDLFlBQU07QUFDNUMsT0FBSXpFLE1BQU1rRSxTQUFOLEdBQWtCQyxJQUFsQixDQUF1Qk8sSUFBdkIsT0FBa0NDLFNBQWxDLElBQStDMUQsYUFBYTBELFNBQWhFLEVBQTJFO0FBQzFFWDtBQUNBO0FBQ0QsR0FKRDs7QUFNQSxPQUFLLElBQUl2QixLQUFULElBQWtCMUIsUUFBUVIsU0FBMUIsRUFBcUM7QUFDcEMsUUFBSyxJQUFJd0MsSUFBVCxJQUFpQmhDLFFBQVFSLFNBQVIsQ0FBa0JrQyxLQUFsQixDQUFqQixFQUEyQztBQUMxQ3pDLFVBQU15RSxFQUFOLENBQVNoQyxLQUFULEVBQWdCMUIsUUFBUVIsU0FBUixDQUFrQmtDLEtBQWxCLEVBQXlCTSxJQUF6QixDQUFoQixFQUFnRCxFQUFDQSxVQUFELEVBQWhELEVBQXdEUCxZQUF4RDtBQUNBO0FBQ0Q7O0FBRURnQztBQUNBLEVBbkJEOztBQXFCQSxRQUFPNUUsTUFBUDtBQUNBLENBM05GIiwiZmlsZSI6Imludm9pY2VzL292ZXJ2aWV3L3Rvb2x0aXBzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiB0b29sdGlwLmpzIDIwMTctMDUtMThcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIEludm9pY2VzIFRhYmxlIFRvb2x0aXBcbiAqXG4gKiBUaGlzIGNvbnRyb2xsZXIgZGlzcGxheXMgdG9vbHRpcHMgZm9yIHRoZSBpbnZvaWNlcyBvdmVydmlldyB0YWJsZS4gVGhlIHRvb2x0aXBzIGFyZSBsb2FkZWQgYWZ0ZXIgdGhlXG4gKiB0YWJsZSBkYXRhIHJlcXVlc3QgaXMgcmVhZHkgZm9yIG9wdGltaXphdGlvbiBwdXJwb3Nlcy5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQndG9vbHRpcHMnLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9xdGlwMi9qcXVlcnkucXRpcC5jc3NgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9xdGlwMi9qcXVlcnkucXRpcC5qc2Bcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdmFyIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBkZWZhdWx0cyA9IHtcblx0XHRcdHNvdXJjZVVybDoganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1JbnZvaWNlc092ZXJ2aWV3QWpheC9Ub29sdGlwcycsXG5cdFx0XHRzZWxlY3RvcnM6IHtcblx0XHRcdFx0bW91c2VlbnRlcjoge1xuXHRcdFx0XHRcdGludm9pY2VJdGVtczogJy50b29sdGlwLWludm9pY2UtaXRlbXMnLFxuXHRcdFx0XHRcdGN1c3RvbWVyTWVtb3M6ICcudG9vbHRpcC1jdXN0b21lci1tZW1vcycsXG5cdFx0XHRcdFx0Y3VzdG9tZXJBZGRyZXNzZXM6ICcudG9vbHRpcC1jdXN0b21lci1hZGRyZXNzZXMnLFxuXHRcdFx0XHRcdG9yZGVyU3RhdHVzSGlzdG9yeTogJy50b29sdGlwLWludm9pY2Utc3RhdHVzLWhpc3RvcnknLFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRjbGljazoge1xuXHRcdFx0XHRcdHRyYWNraW5nTGlua3M6ICcudG9vbHRpcC10cmFja2luZy1saW5rcydcdFxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdmFyIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBUb29sdGlwIENvbnRlbnRzXG5cdFx0ICpcblx0XHQgKiBDb250YWlucyB0aGUgcmVuZGVyZWQgSFRNTCBvZiB0aGUgdG9vbHRpcHMuIFRoZSBIVE1MIGlzIHJlbmRlcmVkIHdpdGggZWFjaCB0YWJsZSBkcmF3LlxuXHRcdCAqXG5cdFx0ICogZS5nLiB0b29sdGlwcy40MDAyMTAuaW52b2ljZUl0ZW1zID4+IEhUTUwgZm9yIGludm9pY2UgaXRlbXMgdG9vbHRpcCBvZiBpbnZvaWNlICM0MDAyMTAuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGxldCB0b29sdGlwcztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgVGFyZ2V0IFBvc2l0aW9uXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRhcmdldFxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRUYXJnZXRQb3NpdGlvbigkdGFyZ2V0KSB7XG5cdFx0XHRjb25zdCBob3Jpem9udGFsID0gJHRhcmdldC5vZmZzZXQoKS5sZWZ0IC0gJCh3aW5kb3cpLnNjcm9sbExlZnQoKSA+ICQod2luZG93KS53aWR0aCgpIC8gMlxuXHRcdFx0XHRcdD8gJ2xlZnQnXG5cdFx0XHRcdFx0OiAncmlnaHQnO1xuXHRcdFx0Y29uc3QgdmVydGljYWwgPSAkdGFyZ2V0Lm9mZnNldCgpLnRvcCAtICQod2luZG93KS5zY3JvbGxUb3AoKSA+ICQod2luZG93KS5oZWlnaHQoKSAvIDJcblx0XHRcdFx0XHQ/ICd0b3AnXG5cdFx0XHRcdFx0OiAnYm90dG9tJztcblx0XHRcdFxuXHRcdFx0cmV0dXJuIGhvcml6b250YWwgKyAnICcgKyB2ZXJ0aWNhbDtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IFRvb2x0aXAgUG9zaXRpb25cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGFyZ2V0XG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtTdHJpbmd9XG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldFRvb2x0aXBQb3NpdGlvbigkdGFyZ2V0KSB7XG5cdFx0XHRjb25zdCBob3Jpem9udGFsID0gJHRhcmdldC5vZmZzZXQoKS5sZWZ0IC0gJCh3aW5kb3cpLnNjcm9sbExlZnQoKSA+ICQod2luZG93KS53aWR0aCgpIC8gMlxuXHRcdFx0XHRcdD8gJ3JpZ2h0J1xuXHRcdFx0XHRcdDogJ2xlZnQnO1xuXHRcdFx0Y29uc3QgdmVydGljYWwgPSAkdGFyZ2V0Lm9mZnNldCgpLnRvcCAtICQod2luZG93KS5zY3JvbGxUb3AoKSA+ICQod2luZG93KS5oZWlnaHQoKSAvIDJcblx0XHRcdFx0XHQ/ICdib3R0b20nXG5cdFx0XHRcdFx0OiAndG9wJztcblx0XHRcdFxuXHRcdFx0cmV0dXJuIGhvcml6b250YWwgKyAnICcgKyB2ZXJ0aWNhbDtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSWYgdGhlcmUgaXMgb25seSBvbmUgbGluayB0aGVuIG9wZW4gaXQgaW4gYSBuZXcgdGFiLiBcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25UcmFja2luZ0xpbmtzQ2xpY2soKSB7XG5cdFx0XHRjb25zdCB0cmFja2luZ0xpbmtzID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoJ3RyYWNraW5nTGlua3MnKTsgXG5cdFx0XHRcblx0XHRcdGlmICh0cmFja2luZ0xpbmtzLmxlbmd0aCA9PT0gMSkge1xuXHRcdFx0XHR3aW5kb3cub3Blbih0cmFja2luZ0xpbmtzWzBdLCAnX2JsYW5rJyk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgdG9vbHRpcCBmb3Igc3RhdGljIHRhYmxlIGRhdGEuXG5cdFx0ICpcblx0XHQgKiBSZXBsYWNlcyB0aGUgYnJvd3NlcnMgZGVmYXVsdCB0b29sdGlwIHdpdGggYSBxVGlwIGluc3RhbmNlIGZvciBldmVyeSBlbGVtZW50IG9uIHRoZSB0YWJsZSB3aGljaCBoYXNcblx0XHQgKiBhIHRpdGxlIGF0dHJpYnV0ZS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfaW5pdFRvb2x0aXBzRm9yU3RhdGljQ29udGVudCgpIHtcblx0XHRcdCR0aGlzLmZpbmQoJ3Rib2R5IFt0aXRsZV0nKS5xdGlwKHtcblx0XHRcdFx0c3R5bGU6IHtjbGFzc2VzOiAnZ3gtcXRpcCBpbmZvJ31cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTaG93IFRvb2x0aXBcblx0XHQgKlxuXHRcdCAqIERpc3BsYXkgdGhlIFF0aXAgaW5zdGFuY2Ugb2YgdGhlIHRhcmdldC4gVGhlIHRvb2x0aXAgY29udGVudHMgYXJlIGZldGNoZWQgYWZ0ZXIgdGhlIHRhYmxlIHJlcXVlc3Rcblx0XHQgKiBpcyBmaW5pc2hlZCBmb3IgcGVyZm9ybWFuY2UgcmVhc29ucy4gVGhpcyBtZXRob2Qgd2lsbCBub3Qgc2hvdyBhbnl0aGluZyB1bnRpbCB0aGUgdG9vbHRpcCBjb250ZW50c1xuXHRcdCAqIGFyZSBmZXRjaGVkLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3Nob3dUb29sdGlwKGV2ZW50KSB7XG5cdFx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFxuXHRcdFx0Y29uc3QgaW52b2ljZUlkID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmRhdGEoJ2ludm9pY2VJZCcpO1xuXHRcdFx0XG5cdFx0XHRpZiAoIXRvb2x0aXBzW2ludm9pY2VJZF0pIHtcblx0XHRcdFx0cmV0dXJuOyAvLyBUaGUgcmVxdWVzdGVkIHRvb2x0aXAgaXMgbm90IGxvYWRlZCwgZG8gbm90IGNvbnRpbnVlLlxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRjb25zdCB0b29sdGlwUG9zaXRpb24gPSBfZ2V0VG9vbHRpcFBvc2l0aW9uKCQodGhpcykpO1xuXHRcdFx0Y29uc3QgdGFyZ2V0UG9zaXRpb24gPSBfZ2V0VGFyZ2V0UG9zaXRpb24oJCh0aGlzKSk7XG5cdFx0XHRcblx0XHRcdCQodGhpcykucXRpcCh7XG5cdFx0XHRcdGNvbnRlbnQ6IHRvb2x0aXBzW2ludm9pY2VJZF1bZXZlbnQuZGF0YS5uYW1lXSxcblx0XHRcdFx0c3R5bGU6IHtcblx0XHRcdFx0XHRjbGFzc2VzOiAnZ3gtcXRpcCBpbmZvJ1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRwb3NpdGlvbjoge1xuXHRcdFx0XHRcdG15OiB0b29sdGlwUG9zaXRpb24sXG5cdFx0XHRcdFx0YXQ6IHRhcmdldFBvc2l0aW9uLFxuXHRcdFx0XHRcdGVmZmVjdDogZmFsc2UsXG5cdFx0XHRcdFx0dmlld3BvcnQ6ICQod2luZG93KSxcblx0XHRcdFx0XHRhZGp1c3Q6IHtcblx0XHRcdFx0XHRcdG1ldGhvZDogJ25vbmUgc2hpZnQnXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9LFxuXHRcdFx0XHRoaWRlOiB7XG5cdFx0XHRcdFx0Zml4ZWQ6IHRydWUsXG5cdFx0XHRcdFx0ZGVsYXk6IDMwMFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRzaG93OiB7XG5cdFx0XHRcdFx0cmVhZHk6IHRydWUsXG5cdFx0XHRcdFx0ZGVsYXk6IDEwMFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRldmVudHM6IHtcblx0XHRcdFx0XHRoaWRkZW46IChldmVudCwgYXBpKSA9PiB7XG5cdFx0XHRcdFx0XHRhcGkuZGVzdHJveSh0cnVlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgVG9vbHRpcHNcblx0XHQgKlxuXHRcdCAqIEZldGNoZXMgdGhlIHRvb2x0aXBzIHdpdGggYW4gQUpBWCByZXF1ZXN0LCBiYXNlZCBvbiB0aGUgY3VycmVudCBzdGF0ZSBvZiB0aGUgdGFibGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldFRvb2x0aXBzKCkge1xuXHRcdFx0dG9vbHRpcHMgPSBbXTtcblx0XHRcdGNvbnN0IGRhdGF0YWJsZXNYaHJQYXJhbWV0ZXJzID0gJHRoaXMuRGF0YVRhYmxlKCkuYWpheC5wYXJhbXMoKTtcblx0XHRcdCQucG9zdChvcHRpb25zLnNvdXJjZVVybCwgZGF0YXRhYmxlc1hoclBhcmFtZXRlcnMsIHJlc3BvbnNlID0+IHRvb2x0aXBzID0gcmVzcG9uc2UsICdqc29uJylcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCR0aGlzXG5cdFx0XHRcdC5vbignZHJhdy5kdCcsIF9pbml0VG9vbHRpcHNGb3JTdGF0aWNDb250ZW50KVxuXHRcdFx0XHQub24oJ3hoci5kdCcsIF9nZXRUb29sdGlwcylcblx0XHRcdFx0Lm9uKCdjbGljaycsICcudG9vbHRpcC10cmFja2luZy1saW5rcycsIF9vblRyYWNraW5nTGlua3NDbGljayk7XG5cdFx0XHRcblx0XHRcdCQod2luZG93KS5vbignSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsICgpID0+IHtcblx0XHRcdFx0aWYgKCR0aGlzLkRhdGFUYWJsZSgpLmFqYXguanNvbigpICE9PSB1bmRlZmluZWQgJiYgdG9vbHRpcHMgPT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRcdF9nZXRUb29sdGlwcygpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Zm9yIChsZXQgZXZlbnQgaW4gb3B0aW9ucy5zZWxlY3RvcnMpIHtcblx0XHRcdFx0Zm9yIChsZXQgbmFtZSBpbiBvcHRpb25zLnNlbGVjdG9yc1tldmVudF0pIHtcblx0XHRcdFx0XHQkdGhpcy5vbihldmVudCwgb3B0aW9ucy5zZWxlY3RvcnNbZXZlbnRdW25hbWVdLCB7bmFtZX0sIF9zaG93VG9vbHRpcCk7XG5cdFx0XHRcdH1cdFxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9XG4pOyJdfQ==
