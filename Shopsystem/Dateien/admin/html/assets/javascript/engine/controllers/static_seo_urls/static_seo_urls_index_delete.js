'use strict';

/* --------------------------------------------------------------
 static_seo_urls_index_delete.js 2017-05-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Static Seo Urls Overview Delete
 *
 * Handles the delete operation of the static seo urls overview page.
 */
gx.controllers.module('static_seo_urls_index_delete', ['modal', gx.source + '/libs/info_box'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Static seo url delete confirmation modal.
  *
  * @type {jQuery}
  */
	var $modal = $('.delete-static-seo-url.modal');

	/**
  * Delete button selector string.
  *
  * @type {String}
  */
	var deleteButtonSelector = '.btn-delete';

	/**
  * Delete static seo url action URL.
  *
  * @type {String}
  */
	var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=StaticSeoUrlAjax/DeleteStaticSeoUrl';

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Shows the submit error message modal.
  */
	function _showFailMessage() {
		// Message texts.
		var errorTitle = jse.core.lang.translate('DELETE_STATIC_SEO_URL_ERROR_TITLE', 'static_seo_urls');
		var errorMessage = jse.core.lang.translate('DELETE_STATIC_SEO_URL_ERROR_TEXT', 'static_seo_urls');

		// Show modal.
		jse.libs.modal.showMessage(errorTitle, errorMessage);
	}

	/**
  * Handles the delete click event by opening the delete confirmation modal.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onDeleteClick(event) {
		// Prevent default action.
		event.preventDefault();

		// Table row.
		var $parentTableRow = $(event.target).parents('tr');

		// Delete confirmation modal button.
		var $confirmButton = $modal.find('button.confirm');

		// Empty modal body.
		$modal.find('.modal-body .form-group').remove();

		// Static seo url data.
		var staticSeoUrlData = {
			id: $parentTableRow.data('staticSeoUrlId'),
			name: $parentTableRow.data('staticSeoUrlName')
		};

		// Put new static seo url information into modal body.
		$modal.find('.modal-body fieldset').html(_generateStaticSeoUrlInfoMarkup(staticSeoUrlData));

		// Show modal.
		$modal.modal('show');

		// Handle delete confirmation modal button click event.
		$confirmButton.off('click').on('click', function () {
			return _onConfirmButtonClick(staticSeoUrlData.id);
		});
	}

	/**
  * Handles the delete confirmation button click event by removing the static seo url through an AJAX request.
  *
  * @param {Number} staticSeoUrlId Static Seo Url ID.
  */
	function _onConfirmButtonClick(staticSeoUrlId) {
		// AJAX request options.
		var requestOptions = {
			type: 'POST',
			data: { staticSeoUrlId: staticSeoUrlId },
			url: url
		};

		// Perform request.
		$.ajax(requestOptions).done(function (response) {
			return _handleDeleteRequestResponse(response, staticSeoUrlId);
		}).fail(_showFailMessage).always(function () {
			return $modal.modal('hide');
		});
	}

	/**
  * Handles static seo url deletion AJAX action server response.
  *
  * @param {Object} response Server response.
  * @param {Number} staticSeoUrlId ID of deleted static seo url.
  */
	function _handleDeleteRequestResponse(response, staticSeoUrlId) {
		// Error message phrases.
		var errorTitle = jse.core.lang.translate('DELETE_STATIC_SEO_URL_ERROR_TITLE', 'static_seo_urls');
		var errorMessage = jse.core.lang.translate('DELETE_STATIC_SEO_URL_ERROR_TEXT', 'static_seo_urls');

		// Table body.
		var $tableBody = $this.find('tbody');

		// Table rows.
		var $rows = $tableBody.find('[data-static-seo-url-id]');

		// Table rows that will be deleted.
		var $rowToDelete = $rows.filter('[data-static-seo-url-id="' + staticSeoUrlId + '"]');

		// 'No results' message table row template.
		var $emptyRowTemplate = $('#template-table-row-empty');

		// Check for action success.
		if (response.includes('success')) {
			// Delete respective table rows.
			$rowToDelete.remove();

			// Add success message to admin info box.
			jse.libs.info_box.addSuccessMessage();

			// If there are no rows, show 'No results' message row.
			if ($rows.length - 1 < 1) {
				$tableBody.empty().append($emptyRowTemplate.clone().html());
			}
		} else {
			// Show error message modal.
			jse.libs.modal.showMessage(errorTitle, errorMessage);
		}
	}

	/**
  * Generates HTML containing the static seo url information for the delete confirmation modal.
  *
  * @param {Object} data Static seo url data.
  *
  * @return {String} Created HTML string.
  */
	function _generateStaticSeoUrlInfoMarkup(data) {
		// Label phrases.
		var staticSeoUrlNameLabel = jse.core.lang.translate('NAME', 'static_seo_urls');

		// Return markup.
		return '\n\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t<label class="col-md-5">' + staticSeoUrlNameLabel + '</label>\n\t\t\t\t\t\t<div class="col-md-7">' + data.name + '</div>\n\t\t\t\t\t</div>\n\t\t\t';
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Listen to form submit event.
		$this.on('click', deleteButtonSelector, _onDeleteClick);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0YXRpY19zZW9fdXJscy9zdGF0aWNfc2VvX3VybHNfaW5kZXhfZGVsZXRlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJG1vZGFsIiwiZGVsZXRlQnV0dG9uU2VsZWN0b3IiLCJ1cmwiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwiX3Nob3dGYWlsTWVzc2FnZSIsImVycm9yVGl0bGUiLCJsYW5nIiwidHJhbnNsYXRlIiwiZXJyb3JNZXNzYWdlIiwibGlicyIsIm1vZGFsIiwic2hvd01lc3NhZ2UiLCJfb25EZWxldGVDbGljayIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCIkcGFyZW50VGFibGVSb3ciLCJ0YXJnZXQiLCJwYXJlbnRzIiwiJGNvbmZpcm1CdXR0b24iLCJmaW5kIiwicmVtb3ZlIiwic3RhdGljU2VvVXJsRGF0YSIsImlkIiwibmFtZSIsImh0bWwiLCJfZ2VuZXJhdGVTdGF0aWNTZW9VcmxJbmZvTWFya3VwIiwib2ZmIiwib24iLCJfb25Db25maXJtQnV0dG9uQ2xpY2siLCJzdGF0aWNTZW9VcmxJZCIsInJlcXVlc3RPcHRpb25zIiwidHlwZSIsImFqYXgiLCJkb25lIiwiX2hhbmRsZURlbGV0ZVJlcXVlc3RSZXNwb25zZSIsInJlc3BvbnNlIiwiZmFpbCIsImFsd2F5cyIsIiR0YWJsZUJvZHkiLCIkcm93cyIsIiRyb3dUb0RlbGV0ZSIsImZpbHRlciIsIiRlbXB0eVJvd1RlbXBsYXRlIiwiaW5jbHVkZXMiLCJpbmZvX2JveCIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwibGVuZ3RoIiwiZW1wdHkiLCJhcHBlbmQiLCJjbG9uZSIsInN0YXRpY1Nlb1VybE5hbWVMYWJlbCIsImluaXQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsOEJBREQsRUFHQyxDQUNDLE9BREQsRUFFSUYsR0FBR0csTUFGUCxvQkFIRCxFQVFDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFNBQVNELEVBQUUsOEJBQUYsQ0FBZjs7QUFFQTs7Ozs7QUFLQSxLQUFNRSx1QkFBdUIsYUFBN0I7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsTUFBU0MsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixDQUFULDREQUFOOztBQUVBOzs7OztBQUtBLEtBQU1YLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBLFVBQVNZLGdCQUFULEdBQTRCO0FBQzNCO0FBQ0EsTUFBTUMsYUFBYUwsSUFBSUMsSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUNBQXhCLEVBQTZELGlCQUE3RCxDQUFuQjtBQUNBLE1BQU1DLGVBQWVSLElBQUlDLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGtDQUF4QixFQUE0RCxpQkFBNUQsQ0FBckI7O0FBRUE7QUFDQVAsTUFBSVMsSUFBSixDQUFTQyxLQUFULENBQWVDLFdBQWYsQ0FBMkJOLFVBQTNCLEVBQXVDRyxZQUF2QztBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNJLGNBQVQsQ0FBd0JDLEtBQXhCLEVBQStCO0FBQzlCO0FBQ0FBLFFBQU1DLGNBQU47O0FBRUE7QUFDQSxNQUFNQyxrQkFBa0JuQixFQUFFaUIsTUFBTUcsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsSUFBeEIsQ0FBeEI7O0FBRUE7QUFDQSxNQUFNQyxpQkFBaUJyQixPQUFPc0IsSUFBUCxDQUFZLGdCQUFaLENBQXZCOztBQUVBO0FBQ0F0QixTQUFPc0IsSUFBUCxDQUFZLHlCQUFaLEVBQXVDQyxNQUF2Qzs7QUFFQTtBQUNBLE1BQU1DLG1CQUFtQjtBQUN4QkMsT0FBSVAsZ0JBQWdCckIsSUFBaEIsQ0FBcUIsZ0JBQXJCLENBRG9CO0FBRXhCNkIsU0FBTVIsZ0JBQWdCckIsSUFBaEIsQ0FBcUIsa0JBQXJCO0FBRmtCLEdBQXpCOztBQUtBO0FBQ0FHLFNBQ0VzQixJQURGLENBQ08sc0JBRFAsRUFFRUssSUFGRixDQUVPQyxnQ0FBZ0NKLGdCQUFoQyxDQUZQOztBQUlBO0FBQ0F4QixTQUFPYSxLQUFQLENBQWEsTUFBYjs7QUFFQTtBQUNBUSxpQkFDRVEsR0FERixDQUNNLE9BRE4sRUFFRUMsRUFGRixDQUVLLE9BRkwsRUFFYztBQUFBLFVBQU1DLHNCQUFzQlAsaUJBQWlCQyxFQUF2QyxDQUFOO0FBQUEsR0FGZDtBQUdBOztBQUVEOzs7OztBQUtBLFVBQVNNLHFCQUFULENBQStCQyxjQUEvQixFQUErQztBQUM5QztBQUNBLE1BQU1DLGlCQUFpQjtBQUN0QkMsU0FBTSxNQURnQjtBQUV0QnJDLFNBQU0sRUFBQ21DLDhCQUFELEVBRmdCO0FBR3RCOUI7QUFIc0IsR0FBdkI7O0FBTUE7QUFDQUgsSUFBRW9DLElBQUYsQ0FBT0YsY0FBUCxFQUNFRyxJQURGLENBQ087QUFBQSxVQUFZQyw2QkFBNkJDLFFBQTdCLEVBQXVDTixjQUF2QyxDQUFaO0FBQUEsR0FEUCxFQUVFTyxJQUZGLENBRU9oQyxnQkFGUCxFQUdFaUMsTUFIRixDQUdTO0FBQUEsVUFBTXhDLE9BQU9hLEtBQVAsQ0FBYSxNQUFiLENBQU47QUFBQSxHQUhUO0FBSUE7O0FBRUQ7Ozs7OztBQU1BLFVBQVN3Qiw0QkFBVCxDQUFzQ0MsUUFBdEMsRUFBZ0ROLGNBQWhELEVBQWdFO0FBQy9EO0FBQ0EsTUFBTXhCLGFBQWFMLElBQUlDLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1DQUF4QixFQUE2RCxpQkFBN0QsQ0FBbkI7QUFDQSxNQUFNQyxlQUFlUixJQUFJQyxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQ0FBeEIsRUFBNEQsaUJBQTVELENBQXJCOztBQUVBO0FBQ0EsTUFBTStCLGFBQWEzQyxNQUFNd0IsSUFBTixDQUFXLE9BQVgsQ0FBbkI7O0FBRUE7QUFDQSxNQUFNb0IsUUFBUUQsV0FBV25CLElBQVgsNEJBQWQ7O0FBRUE7QUFDQSxNQUFNcUIsZUFBZUQsTUFBTUUsTUFBTiwrQkFBeUNaLGNBQXpDLFFBQXJCOztBQUVBO0FBQ0EsTUFBTWEsb0JBQW9COUMsRUFBRSwyQkFBRixDQUExQjs7QUFFQTtBQUNBLE1BQUl1QyxTQUFTUSxRQUFULENBQWtCLFNBQWxCLENBQUosRUFBa0M7QUFDakM7QUFDQUgsZ0JBQWFwQixNQUFiOztBQUVBO0FBQ0FwQixPQUFJUyxJQUFKLENBQVNtQyxRQUFULENBQWtCQyxpQkFBbEI7O0FBRUE7QUFDQSxPQUFLTixNQUFNTyxNQUFOLEdBQWUsQ0FBaEIsR0FBcUIsQ0FBekIsRUFBNEI7QUFDM0JSLGVBQ0VTLEtBREYsR0FFRUMsTUFGRixDQUVTTixrQkFBa0JPLEtBQWxCLEdBQTBCekIsSUFBMUIsRUFGVDtBQUdBO0FBQ0QsR0FiRCxNQWFPO0FBQ047QUFDQXhCLE9BQUlTLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxXQUFmLENBQTJCTixVQUEzQixFQUF1Q0csWUFBdkM7QUFDQTtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsVUFBU2lCLCtCQUFULENBQXlDL0IsSUFBekMsRUFBK0M7QUFDOUM7QUFDQSxNQUFNd0Qsd0JBQXdCbEQsSUFBSUMsSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsTUFBeEIsRUFBZ0MsaUJBQWhDLENBQTlCOztBQUVBO0FBQ0Esd0ZBRTZCMkMscUJBRjdCLG9EQUcyQnhELEtBQUs2QixJQUhoQztBQU1BOztBQUVEO0FBQ0E7QUFDQTs7QUFFQS9CLFFBQU8yRCxJQUFQLEdBQWMsZ0JBQVE7QUFDckI7QUFDQXhELFFBQU1nQyxFQUFOLENBQVMsT0FBVCxFQUFrQjdCLG9CQUFsQixFQUF3Q2MsY0FBeEM7O0FBRUE7QUFDQXFCO0FBQ0EsRUFORDs7QUFRQSxRQUFPekMsTUFBUDtBQUNBLENBek1GIiwiZmlsZSI6InN0YXRpY19zZW9fdXJscy9zdGF0aWNfc2VvX3VybHNfaW5kZXhfZGVsZXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzdGF0aWNfc2VvX3VybHNfaW5kZXhfZGVsZXRlLmpzIDIwMTctMDUtMjRcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIFN0YXRpYyBTZW8gVXJscyBPdmVydmlldyBEZWxldGVcbiAqXG4gKiBIYW5kbGVzIHRoZSBkZWxldGUgb3BlcmF0aW9uIG9mIHRoZSBzdGF0aWMgc2VvIHVybHMgb3ZlcnZpZXcgcGFnZS5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnc3RhdGljX3Nlb191cmxzX2luZGV4X2RlbGV0ZScsXG5cdFxuXHRbXG5cdFx0J21vZGFsJyxcblx0XHRgJHtneC5zb3VyY2V9L2xpYnMvaW5mb19ib3hgXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFN0YXRpYyBzZW8gdXJsIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5kZWxldGUtc3RhdGljLXNlby11cmwubW9kYWwnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWxldGUgYnV0dG9uIHNlbGVjdG9yIHN0cmluZy5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtTdHJpbmd9XG5cdFx0ICovXG5cdFx0Y29uc3QgZGVsZXRlQnV0dG9uU2VsZWN0b3IgPSAnLmJ0bi1kZWxldGUnO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlbGV0ZSBzdGF0aWMgc2VvIHVybCBhY3Rpb24gVVJMLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge1N0cmluZ31cblx0XHQgKi9cblx0XHRjb25zdCB1cmwgPSBgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYWRtaW4vYWRtaW4ucGhwP2RvPVN0YXRpY1Nlb1VybEFqYXgvRGVsZXRlU3RhdGljU2VvVXJsYDtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNob3dzIHRoZSBzdWJtaXQgZXJyb3IgbWVzc2FnZSBtb2RhbC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2hvd0ZhaWxNZXNzYWdlKCkge1xuXHRcdFx0Ly8gTWVzc2FnZSB0ZXh0cy5cblx0XHRcdGNvbnN0IGVycm9yVGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnREVMRVRFX1NUQVRJQ19TRU9fVVJMX0VSUk9SX1RJVExFJywgJ3N0YXRpY19zZW9fdXJscycpO1xuXHRcdFx0Y29uc3QgZXJyb3JNZXNzYWdlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0RFTEVURV9TVEFUSUNfU0VPX1VSTF9FUlJPUl9URVhUJywgJ3N0YXRpY19zZW9fdXJscycpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoZXJyb3JUaXRsZSwgZXJyb3JNZXNzYWdlKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgZGVsZXRlIGNsaWNrIGV2ZW50IGJ5IG9wZW5pbmcgdGhlIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkRlbGV0ZUNsaWNrKGV2ZW50KSB7XG5cdFx0XHQvLyBQcmV2ZW50IGRlZmF1bHQgYWN0aW9uLlxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFxuXHRcdFx0Ly8gVGFibGUgcm93LlxuXHRcdFx0Y29uc3QgJHBhcmVudFRhYmxlUm93ID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoJ3RyJyk7XG5cdFx0XHRcblx0XHRcdC8vIERlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwgYnV0dG9uLlxuXHRcdFx0Y29uc3QgJGNvbmZpcm1CdXR0b24gPSAkbW9kYWwuZmluZCgnYnV0dG9uLmNvbmZpcm0nKTtcblx0XHRcdFxuXHRcdFx0Ly8gRW1wdHkgbW9kYWwgYm9keS5cblx0XHRcdCRtb2RhbC5maW5kKCcubW9kYWwtYm9keSAuZm9ybS1ncm91cCcpLnJlbW92ZSgpO1xuXHRcdFx0XG5cdFx0XHQvLyBTdGF0aWMgc2VvIHVybCBkYXRhLlxuXHRcdFx0Y29uc3Qgc3RhdGljU2VvVXJsRGF0YSA9IHtcblx0XHRcdFx0aWQ6ICRwYXJlbnRUYWJsZVJvdy5kYXRhKCdzdGF0aWNTZW9VcmxJZCcpLFxuXHRcdFx0XHRuYW1lOiAkcGFyZW50VGFibGVSb3cuZGF0YSgnc3RhdGljU2VvVXJsTmFtZScpXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBQdXQgbmV3IHN0YXRpYyBzZW8gdXJsIGluZm9ybWF0aW9uIGludG8gbW9kYWwgYm9keS5cblx0XHRcdCRtb2RhbFxuXHRcdFx0XHQuZmluZCgnLm1vZGFsLWJvZHkgZmllbGRzZXQnKVxuXHRcdFx0XHQuaHRtbChfZ2VuZXJhdGVTdGF0aWNTZW9VcmxJbmZvTWFya3VwKHN0YXRpY1Nlb1VybERhdGEpKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBtb2RhbC5cblx0XHRcdCRtb2RhbC5tb2RhbCgnc2hvdycpO1xuXHRcdFx0XG5cdFx0XHQvLyBIYW5kbGUgZGVsZXRlIGNvbmZpcm1hdGlvbiBtb2RhbCBidXR0b24gY2xpY2sgZXZlbnQuXG5cdFx0XHQkY29uZmlybUJ1dHRvblxuXHRcdFx0XHQub2ZmKCdjbGljaycpXG5cdFx0XHRcdC5vbignY2xpY2snLCAoKSA9PiBfb25Db25maXJtQnV0dG9uQ2xpY2soc3RhdGljU2VvVXJsRGF0YS5pZCkpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBkZWxldGUgY29uZmlybWF0aW9uIGJ1dHRvbiBjbGljayBldmVudCBieSByZW1vdmluZyB0aGUgc3RhdGljIHNlbyB1cmwgdGhyb3VnaCBhbiBBSkFYIHJlcXVlc3QuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge051bWJlcn0gc3RhdGljU2VvVXJsSWQgU3RhdGljIFNlbyBVcmwgSUQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uQ29uZmlybUJ1dHRvbkNsaWNrKHN0YXRpY1Nlb1VybElkKSB7XG5cdFx0XHQvLyBBSkFYIHJlcXVlc3Qgb3B0aW9ucy5cblx0XHRcdGNvbnN0IHJlcXVlc3RPcHRpb25zID0ge1xuXHRcdFx0XHR0eXBlOiAnUE9TVCcsXG5cdFx0XHRcdGRhdGE6IHtzdGF0aWNTZW9VcmxJZH0sXG5cdFx0XHRcdHVybFxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gUGVyZm9ybSByZXF1ZXN0LlxuXHRcdFx0JC5hamF4KHJlcXVlc3RPcHRpb25zKVxuXHRcdFx0XHQuZG9uZShyZXNwb25zZSA9PiBfaGFuZGxlRGVsZXRlUmVxdWVzdFJlc3BvbnNlKHJlc3BvbnNlLCBzdGF0aWNTZW9VcmxJZCkpXG5cdFx0XHRcdC5mYWlsKF9zaG93RmFpbE1lc3NhZ2UpXG5cdFx0XHRcdC5hbHdheXMoKCkgPT4gJG1vZGFsLm1vZGFsKCdoaWRlJykpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHN0YXRpYyBzZW8gdXJsIGRlbGV0aW9uIEFKQVggYWN0aW9uIHNlcnZlciByZXNwb25zZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSByZXNwb25zZSBTZXJ2ZXIgcmVzcG9uc2UuXG5cdFx0ICogQHBhcmFtIHtOdW1iZXJ9IHN0YXRpY1Nlb1VybElkIElEIG9mIGRlbGV0ZWQgc3RhdGljIHNlbyB1cmwuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2hhbmRsZURlbGV0ZVJlcXVlc3RSZXNwb25zZShyZXNwb25zZSwgc3RhdGljU2VvVXJsSWQpIHtcblx0XHRcdC8vIEVycm9yIG1lc3NhZ2UgcGhyYXNlcy5cblx0XHRcdGNvbnN0IGVycm9yVGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnREVMRVRFX1NUQVRJQ19TRU9fVVJMX0VSUk9SX1RJVExFJywgJ3N0YXRpY19zZW9fdXJscycpO1xuXHRcdFx0Y29uc3QgZXJyb3JNZXNzYWdlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0RFTEVURV9TVEFUSUNfU0VPX1VSTF9FUlJPUl9URVhUJywgJ3N0YXRpY19zZW9fdXJscycpO1xuXHRcdFx0XG5cdFx0XHQvLyBUYWJsZSBib2R5LlxuXHRcdFx0Y29uc3QgJHRhYmxlQm9keSA9ICR0aGlzLmZpbmQoJ3Rib2R5Jyk7XG5cdFx0XHRcblx0XHRcdC8vIFRhYmxlIHJvd3MuXG5cdFx0XHRjb25zdCAkcm93cyA9ICR0YWJsZUJvZHkuZmluZChgW2RhdGEtc3RhdGljLXNlby11cmwtaWRdYCk7XG5cdFx0XHRcblx0XHRcdC8vIFRhYmxlIHJvd3MgdGhhdCB3aWxsIGJlIGRlbGV0ZWQuXG5cdFx0XHRjb25zdCAkcm93VG9EZWxldGUgPSAkcm93cy5maWx0ZXIoYFtkYXRhLXN0YXRpYy1zZW8tdXJsLWlkPVwiJHtzdGF0aWNTZW9VcmxJZH1cIl1gKTtcblx0XHRcdFxuXHRcdFx0Ly8gJ05vIHJlc3VsdHMnIG1lc3NhZ2UgdGFibGUgcm93IHRlbXBsYXRlLlxuXHRcdFx0Y29uc3QgJGVtcHR5Um93VGVtcGxhdGUgPSAkKCcjdGVtcGxhdGUtdGFibGUtcm93LWVtcHR5Jyk7XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrIGZvciBhY3Rpb24gc3VjY2Vzcy5cblx0XHRcdGlmIChyZXNwb25zZS5pbmNsdWRlcygnc3VjY2VzcycpKSB7XG5cdFx0XHRcdC8vIERlbGV0ZSByZXNwZWN0aXZlIHRhYmxlIHJvd3MuXG5cdFx0XHRcdCRyb3dUb0RlbGV0ZS5yZW1vdmUoKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFkZCBzdWNjZXNzIG1lc3NhZ2UgdG8gYWRtaW4gaW5mbyBib3guXG5cdFx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBJZiB0aGVyZSBhcmUgbm8gcm93cywgc2hvdyAnTm8gcmVzdWx0cycgbWVzc2FnZSByb3cuXG5cdFx0XHRcdGlmICgoJHJvd3MubGVuZ3RoIC0gMSkgPCAxKSB7XG5cdFx0XHRcdFx0JHRhYmxlQm9keVxuXHRcdFx0XHRcdFx0LmVtcHR5KClcblx0XHRcdFx0XHRcdC5hcHBlbmQoJGVtcHR5Um93VGVtcGxhdGUuY2xvbmUoKS5odG1sKCkpO1xuXHRcdFx0XHR9XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQvLyBTaG93IGVycm9yIG1lc3NhZ2UgbW9kYWwuXG5cdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKGVycm9yVGl0bGUsIGVycm9yTWVzc2FnZSk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdlbmVyYXRlcyBIVE1MIGNvbnRhaW5pbmcgdGhlIHN0YXRpYyBzZW8gdXJsIGluZm9ybWF0aW9uIGZvciB0aGUgZGVsZXRlIGNvbmZpcm1hdGlvbiBtb2RhbC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBkYXRhIFN0YXRpYyBzZW8gdXJsIGRhdGEuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtTdHJpbmd9IENyZWF0ZWQgSFRNTCBzdHJpbmcuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dlbmVyYXRlU3RhdGljU2VvVXJsSW5mb01hcmt1cChkYXRhKSB7XG5cdFx0XHQvLyBMYWJlbCBwaHJhc2VzLlxuXHRcdFx0Y29uc3Qgc3RhdGljU2VvVXJsTmFtZUxhYmVsID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ05BTUUnLCAnc3RhdGljX3Nlb191cmxzJyk7XG5cdFx0XHRcblx0XHRcdC8vIFJldHVybiBtYXJrdXAuXG5cdFx0XHRyZXR1cm4gYFxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtNVwiPiR7c3RhdGljU2VvVXJsTmFtZUxhYmVsfTwvbGFiZWw+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTdcIj4ke2RhdGEubmFtZX08L2Rpdj5cblx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdGA7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZG9uZSA9PiB7XG5cdFx0XHQvLyBMaXN0ZW4gdG8gZm9ybSBzdWJtaXQgZXZlbnQuXG5cdFx0XHQkdGhpcy5vbignY2xpY2snLCBkZWxldGVCdXR0b25TZWxlY3RvciwgX29uRGVsZXRlQ2xpY2spO1xuXHRcdFx0XG5cdFx0XHQvLyBGaW5pc2ggaW5pdGlhbGl6YXRpb24uXG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9XG4pO1xuIl19
