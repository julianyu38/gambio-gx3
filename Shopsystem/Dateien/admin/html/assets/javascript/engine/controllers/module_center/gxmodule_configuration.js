'use strict';

/* --------------------------------------------------------------
 gxmodule_configuration.js 2018-01-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('gxmodule_configuration', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Standard button
  *
  * @type {jQuery}
  */
	var $button = $('.GXModuleConfigButton');

	/**
  * Button in modal dialog
  *
  * @type {jQuery}
  */
	var $modalButton = $('.GXModuleModalButton');

	/**
  * Modal dialog
  *
  * @type {jQuery}
  */
	var $modal = $('.modal');

	var $loadingSpinnder = $('.loading-spinner');

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Event handling of buttons
  */
	$button.on('click', function (event) {
		event.preventDefault();
		$loadingSpinnder.fadeIn();
		var action = $(this).data('action');
		var controller = $(this).data('controller');
		var message = $(this).data('message');
		var formData = $(this).closest('form').serialize();
		var request = $.ajax('admin.php?do=GXModuleCenterModuleButtonActionsAjax', { method: 'post', data: { action: action, controller: controller, formData: formData }, dataType: "json" });

		request.done(function (response) {
			$loadingSpinnder.fadeOut();
			if (response.success) {

				if (!message) {
					message = response.data;
				}

				jse.libs.info_box.addSuccessMessage(message);
			} else {
				jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'), response.data);
			}
		}).fail(function (jqxhr, textStatus, errorThrown) {
			$('.loading-spinner').fadeOut();
			jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'), errorThrown);
		});
	});

	/**
  * Event handling of buttons in modal dialogs
  */
	$modalButton.on('click', function (event) {
		event.preventDefault();
		var modal = $(this).data('target');
		$(modal).modal('show');
	});

	/**
  * Load HTML Code for modal dialog
  * on error show modal with error message
  */
	$modal.each(function () {
		var modalContent = $(this).data('content');
		var modalBody = $(this).find('.modal-body');
		if (modalContent) {
			$(modalBody).load('admin.php?do=GXModuleCenterModuleButtonActionsAjax/Modal', { content: modalContent }, function (response, status, xhr) {

				var IS_JSON = true;
				try {
					var json = $.parseJSON(response);
				} catch (err) {
					IS_JSON = false;
				}

				if (IS_JSON) {
					jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'), json.data);
				}
			});
		}
	});

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZHVsZV9jZW50ZXIvZ3htb2R1bGVfY29uZmlndXJhdGlvbi5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRidXR0b24iLCIkbW9kYWxCdXR0b24iLCIkbW9kYWwiLCIkbG9hZGluZ1NwaW5uZGVyIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwib24iLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiZmFkZUluIiwiYWN0aW9uIiwiY29udHJvbGxlciIsIm1lc3NhZ2UiLCJmb3JtRGF0YSIsImNsb3Nlc3QiLCJzZXJpYWxpemUiLCJyZXF1ZXN0IiwiYWpheCIsIm1ldGhvZCIsImRhdGFUeXBlIiwiZG9uZSIsInJlc3BvbnNlIiwiZmFkZU91dCIsInN1Y2Nlc3MiLCJqc2UiLCJsaWJzIiwiaW5mb19ib3giLCJhZGRTdWNjZXNzTWVzc2FnZSIsIm1vZGFsIiwic2hvd01lc3NhZ2UiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsImZhaWwiLCJqcXhociIsInRleHRTdGF0dXMiLCJlcnJvclRocm93biIsImVhY2giLCJtb2RhbENvbnRlbnQiLCJtb2RhbEJvZHkiLCJmaW5kIiwibG9hZCIsImNvbnRlbnQiLCJzdGF0dXMiLCJ4aHIiLCJJU19KU09OIiwianNvbiIsInBhcnNlSlNPTiIsImVyciIsImluaXQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQXNCLHdCQUF0QixFQUFnRCxDQUFDLE9BQUQsQ0FBaEQsRUFFQyxVQUFVQyxJQUFWLEVBQWdCOztBQUVmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVUQsRUFBRSx1QkFBRixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRSxlQUFlRixFQUFFLHNCQUFGLENBQXJCOztBQUVBOzs7OztBQUtBLEtBQU1HLFNBQVNILEVBQUUsUUFBRixDQUFmOztBQUVBLEtBQU1JLG1CQUFtQkosRUFBRSxrQkFBRixDQUF6Qjs7QUFFQTs7Ozs7QUFLQSxLQUFNSyxXQUFXLEVBQWpCOztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVVOLEVBQUVPLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJQLElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBSSxTQUFRTyxFQUFSLENBQVcsT0FBWCxFQUFvQixVQUFVQyxLQUFWLEVBQWlCO0FBQ3BDQSxRQUFNQyxjQUFOO0FBQ0FOLG1CQUFpQk8sTUFBakI7QUFDQSxNQUFJQyxTQUFTWixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLFFBQWIsQ0FBYjtBQUNBLE1BQUllLGFBQWFiLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsWUFBYixDQUFqQjtBQUNBLE1BQUlnQixVQUFVZCxFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLFNBQWIsQ0FBZDtBQUNBLE1BQUlpQixXQUFXZixFQUFFLElBQUYsRUFBUWdCLE9BQVIsQ0FBZ0IsTUFBaEIsRUFBd0JDLFNBQXhCLEVBQWY7QUFDQSxNQUFJQyxVQUFVbEIsRUFBRW1CLElBQUYsQ0FBTyxvREFBUCxFQUE0RCxFQUFDQyxRQUFRLE1BQVQsRUFBZ0J0QixNQUFNLEVBQUNjLGNBQUQsRUFBUUMsc0JBQVIsRUFBbUJFLGtCQUFuQixFQUF0QixFQUFtRE0sVUFBVSxNQUE3RCxFQUE1RCxDQUFkOztBQUVBSCxVQUFRSSxJQUFSLENBQWEsVUFBU0MsUUFBVCxFQUFrQjtBQUM5Qm5CLG9CQUFpQm9CLE9BQWpCO0FBQ0EsT0FBR0QsU0FBU0UsT0FBWixFQUFxQjs7QUFFcEIsUUFBRyxDQUFDWCxPQUFKLEVBQVk7QUFDWEEsZUFBVVMsU0FBU3pCLElBQW5CO0FBQ0E7O0FBRUQ0QixRQUFJQyxJQUFKLENBQVNDLFFBQVQsQ0FBa0JDLGlCQUFsQixDQUNDZixPQUREO0FBRUEsSUFSRCxNQVVBO0FBQ0NZLFFBQUlDLElBQUosQ0FBU0csS0FBVCxDQUFlQyxXQUFmLENBQTJCTCxJQUFJTSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQUEzQixFQUF3RVgsU0FBU3pCLElBQWpGO0FBQ0E7QUFDRCxHQWZELEVBZ0JFcUMsSUFoQkYsQ0FnQk8sVUFBU0MsS0FBVCxFQUFnQkMsVUFBaEIsRUFBNEJDLFdBQTVCLEVBQXlDO0FBQzlDdEMsS0FBRSxrQkFBRixFQUFzQndCLE9BQXRCO0FBQ0FFLE9BQUlDLElBQUosQ0FBU0csS0FBVCxDQUFlQyxXQUFmLENBQTJCTCxJQUFJTSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQUEzQixFQUMyQkksV0FEM0I7QUFFQSxHQXBCRjtBQXFCQSxFQTlCRDs7QUFnQ0E7OztBQUdBcEMsY0FBYU0sRUFBYixDQUFnQixPQUFoQixFQUF5QixVQUFVQyxLQUFWLEVBQWlCO0FBQ3pDQSxRQUFNQyxjQUFOO0FBQ0EsTUFBSW9CLFFBQVE5QixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLFFBQWIsQ0FBWjtBQUNBRSxJQUFFOEIsS0FBRixFQUFTQSxLQUFULENBQWUsTUFBZjtBQUNBLEVBSkQ7O0FBTUE7Ozs7QUFJQTNCLFFBQU9vQyxJQUFQLENBQVksWUFBWTtBQUN2QixNQUFJQyxlQUFleEMsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxTQUFiLENBQW5CO0FBQ0EsTUFBSTJDLFlBQVl6QyxFQUFFLElBQUYsRUFBUTBDLElBQVIsQ0FBYSxhQUFiLENBQWhCO0FBQ0EsTUFBSUYsWUFBSixFQUFrQjtBQUNqQnhDLEtBQUV5QyxTQUFGLEVBQWFFLElBQWIsQ0FBa0IsMERBQWxCLEVBQThFLEVBQUNDLFNBQVNKLFlBQVYsRUFBOUUsRUFBdUcsVUFBVWpCLFFBQVYsRUFBb0JzQixNQUFwQixFQUE0QkMsR0FBNUIsRUFBaUM7O0FBRXZJLFFBQUlDLFVBQVUsSUFBZDtBQUNBLFFBQUk7QUFDSCxTQUFJQyxPQUFPaEQsRUFBRWlELFNBQUYsQ0FBWTFCLFFBQVosQ0FBWDtBQUNBLEtBRkQsQ0FHQSxPQUFPMkIsR0FBUCxFQUFZO0FBQ1hILGVBQVUsS0FBVjtBQUNBOztBQUVELFFBQUlBLE9BQUosRUFBYTtBQUNackIsU0FBSUMsSUFBSixDQUFTRyxLQUFULENBQWVDLFdBQWYsQ0FBMkJMLElBQUlNLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBQTNCLEVBQzJCYyxLQUFLbEQsSUFEaEM7QUFFQTtBQUNELElBZEQ7QUFlQTtBQUNELEVBcEJEOztBQXNCQTtBQUNBO0FBQ0E7O0FBRUFELFFBQU9zRCxJQUFQLEdBQWMsVUFBVTdCLElBQVYsRUFBZ0I7QUFDN0JBO0FBQ0EsRUFGRDs7QUFJQSxRQUFPekIsTUFBUDtBQUNBLENBaEpGIiwiZmlsZSI6Im1vZHVsZV9jZW50ZXIvZ3htb2R1bGVfY29uZmlndXJhdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZ3htb2R1bGVfY29uZmlndXJhdGlvbi5qcyAyMDE4LTAxLTEzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuZ3guY29udHJvbGxlcnMubW9kdWxlKCdneG1vZHVsZV9jb25maWd1cmF0aW9uJywgWydtb2RhbCddLFxuXG5cdGZ1bmN0aW9uIChkYXRhKSB7XG5cdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFN0YW5kYXJkIGJ1dHRvblxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkYnV0dG9uID0gJCgnLkdYTW9kdWxlQ29uZmlnQnV0dG9uJyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQnV0dG9uIGluIG1vZGFsIGRpYWxvZ1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkbW9kYWxCdXR0b24gPSAkKCcuR1hNb2R1bGVNb2RhbEJ1dHRvbicpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZGFsIGRpYWxvZ1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkbW9kYWwgPSAkKCcubW9kYWwnKTtcblx0XHRcblx0XHRjb25zdCAkbG9hZGluZ1NwaW5uZGVyID0gJCgnLmxvYWRpbmctc3Bpbm5lcicpO1xuXHQgICAgICAgICAgICAgICAgICAgICAgXG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGRlZmF1bHRzID0ge307XG5cdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBFdmVudCBoYW5kbGluZyBvZiBidXR0b25zXG5cdFx0ICovXG5cdFx0JGJ1dHRvbi5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHQkbG9hZGluZ1NwaW5uZGVyLmZhZGVJbigpO1xuXHRcdFx0dmFyIGFjdGlvbiA9ICQodGhpcykuZGF0YSgnYWN0aW9uJyk7XG5cdFx0XHR2YXIgY29udHJvbGxlciA9ICQodGhpcykuZGF0YSgnY29udHJvbGxlcicpO1xuXHRcdFx0dmFyIG1lc3NhZ2UgPSAkKHRoaXMpLmRhdGEoJ21lc3NhZ2UnKTtcblx0XHRcdHZhciBmb3JtRGF0YSA9ICQodGhpcykuY2xvc2VzdCgnZm9ybScpLnNlcmlhbGl6ZSgpO1xuXHRcdFx0dmFyIHJlcXVlc3QgPSAkLmFqYXgoJ2FkbWluLnBocD9kbz1HWE1vZHVsZUNlbnRlck1vZHVsZUJ1dHRvbkFjdGlvbnNBamF4Jyx7bWV0aG9kOiAncG9zdCcsZGF0YToge2FjdGlvbixjb250cm9sbGVyLGZvcm1EYXRhfSxkYXRhVHlwZTogXCJqc29uXCJ9KTtcblx0XHRcdFxuXHRcdFx0cmVxdWVzdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKXtcblx0XHRcdFx0JGxvYWRpbmdTcGlubmRlci5mYWRlT3V0KCk7XG5cdFx0XHRcdGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZighbWVzc2FnZSl7XG5cdFx0XHRcdFx0XHRtZXNzYWdlID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0anNlLmxpYnMuaW5mb19ib3guYWRkU3VjY2Vzc01lc3NhZ2UoXG5cdFx0XHRcdFx0XHRtZXNzYWdlKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKSxyZXNwb25zZS5kYXRhKTtcblx0XHRcdFx0fVxuXHRcdFx0fSlcblx0XHRcdFx0LmZhaWwoZnVuY3Rpb24oanF4aHIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XG5cdFx0XHRcdFx0JCgnLmxvYWRpbmctc3Bpbm5lcicpLmZhZGVPdXQoKTtcblx0XHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKSxcblx0XHRcdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvclRocm93bik7XG5cdFx0XHRcdH0pO1xuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEV2ZW50IGhhbmRsaW5nIG9mIGJ1dHRvbnMgaW4gbW9kYWwgZGlhbG9nc1xuXHRcdCAqL1xuXHRcdCRtb2RhbEJ1dHRvbi5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHR2YXIgbW9kYWwgPSAkKHRoaXMpLmRhdGEoJ3RhcmdldCcpO1xuXHRcdFx0JChtb2RhbCkubW9kYWwoJ3Nob3cnKTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBMb2FkIEhUTUwgQ29kZSBmb3IgbW9kYWwgZGlhbG9nXG5cdFx0ICogb24gZXJyb3Igc2hvdyBtb2RhbCB3aXRoIGVycm9yIG1lc3NhZ2Vcblx0XHQgKi9cblx0XHQkbW9kYWwuZWFjaChmdW5jdGlvbiAoKSB7XG5cdFx0XHR2YXIgbW9kYWxDb250ZW50ID0gJCh0aGlzKS5kYXRhKCdjb250ZW50Jyk7XG5cdFx0XHR2YXIgbW9kYWxCb2R5ID0gJCh0aGlzKS5maW5kKCcubW9kYWwtYm9keScpO1xuXHRcdFx0aWYgKG1vZGFsQ29udGVudCkge1xuXHRcdFx0XHQkKG1vZGFsQm9keSkubG9hZCgnYWRtaW4ucGhwP2RvPUdYTW9kdWxlQ2VudGVyTW9kdWxlQnV0dG9uQWN0aW9uc0FqYXgvTW9kYWwnLCB7Y29udGVudDogbW9kYWxDb250ZW50fSwgZnVuY3Rpb24gKHJlc3BvbnNlLCBzdGF0dXMsIHhocikge1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdHZhciBJU19KU09OID0gdHJ1ZTtcblx0XHRcdFx0XHR0cnkge1xuXHRcdFx0XHRcdFx0dmFyIGpzb24gPSAkLnBhcnNlSlNPTihyZXNwb25zZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGNhdGNoIChlcnIpIHtcblx0XHRcdFx0XHRcdElTX0pTT04gPSBmYWxzZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKElTX0pTT04pIHtcblx0XHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpLFxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAganNvbi5kYXRhKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uIChkb25lKSB7XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7Il19
