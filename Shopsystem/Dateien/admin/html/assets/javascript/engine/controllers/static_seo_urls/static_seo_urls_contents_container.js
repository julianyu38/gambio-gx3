'use strict';

/* --------------------------------------------------------------
 static_seo_urls_contents_container.js 2017-05-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Static Seo Url Content Container (Tabs)
 *
 * Handles the static seo url content container functionality in the static seo url details page.
 */
gx.controllers.module('static_seo_urls_contents_container', [jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.min.js', 'modal'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Save Bar
  * @type {jQuery}
  */
	var saveBar = $('.bottom-save-bar');

	/**
  * Elements
  *
  * @type {Object}
  */
	var elements = {
		// Buttons.
		buttons: {

			// Submit button group.
			submit: saveBar.find('.submit-button-group'),

			// Submit button for save static seo url
			submitSave: saveBar.find('.save'),

			// Submit button for save and refresh static seo url
			submitRefresh: saveBar.find('.refresh')
		},

		// Template.
		templates: {},

		// Modals.
		modals: {},

		// Tabs.
		tabHeader: $this.find('.nav-tabs'),

		// Form.
		form: $('.static-seo-urls-details > form')
	};

	/**
  * Selector Strings
  *
  * @type {Object}
  */
	var selectors = {
		// Icon selector strings.
		icons: {
			// Delete button on the panel header.
			delete: '.icon.delete',

			// Drag button on the panel header.
			drag: '.drag-handle',

			// Collapser button on the panel header.
			collapser: '.collapser',

			// Image delete button.
			imageDelete: '.action-icon.delete',

			// Image map edit button.
			imageMap: '.action-icon.image-map',

			// Upload image button.
			upload: '.action-icon.upload'
		},

		// Inputs selector strings.
		inputs: {
			// General image select dropdowns.
			dropdown: '.dropdown-input',

			// Thumbnail dropdown.
			thumbnailImageDropdown: '[name="thumbnail"]',

			// Title.
			title: 'input[name="title"]',

			// File.
			file: '.file-input'
		},

		// Static seo url content panel.
		staticSeoUrlContentPanel: '.panel',

		// Tab body.
		tabBody: '.tab-pane',

		// Static seo url content panel title.
		staticSeoUrlContentPanelTitle: '.staticSeoUrlContent-title',

		// Setting row (form group).
		configurationRow: '.row.form-group',

		// Data list container for image map.
		imageMapDataList: '.image-map-data-list'
	};

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Handles the key-up event on the staticSeoUrlContent title input field.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onTitleKeyup(event) {
		// Title input field.
		var $input = $(event.target);

		// Static seo url content panel title element.
		var $title = $input.parents(selectors.staticSeoUrlContentPanel).find(selectors.staticSeoUrlContentPanelTitle);

		// Transfer input value to staticSeoUrlContent panel title.
		$title.text($input.val());
	}

	/**
  * Handles the mouse-enter event on a configuration row.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onConfigRowMouseEnter(event) {
		// Configuration row element.
		var $row = $(event.target);

		// Image map edit icon.
		var $imageMapIcon = $row.find(selectors.icons.imageMap);

		// Image map data container list element.
		var $list = $row.find(selectors.imageMapDataList);

		// Return immediately, if the image map edit icon does not exist.
		if (!$imageMapIcon.length || !$list.length) {
			return;
		}

		if ($list.children().length) {
			$imageMapIcon.removeClass('fa-external-link').addClass('fa-external-link-square');
		} else {
			$imageMapIcon.removeClass('fa-external-link-square').addClass('fa-external-link');
		}
	}

	/**
  * Handles the click event on the save button.
  */
	function _onSubmitSave() {
		elements.form.trigger('submit');
	}

	/**
  * Handles the click event on the refresh list item in the submit button group.
  */
	function _onSubmitRefresh() {
		elements.form.trigger('submit', { refresh: true });
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {

		// Attach event handlers to sort actions, static seo url content panel delete button and inputs fields.
		$this.on('keyup', selectors.inputs.title, _onTitleKeyup).on('mouseenter', selectors.configurationRow, _onConfigRowMouseEnter);

		// Attach event listeners to submit buttons.
		elements.buttons.submitSave.on('click', _onSubmitSave);
		elements.buttons.submitRefresh.on('click', _onSubmitRefresh);

		// Activate first tab.
		elements.tabHeader.children().first().addClass('active');

		// Activate first tab content.
		$this.find(selectors.tabBody).first().addClass('active in');

		// Trigger dropdown change event to hide the remove icon, if 'do not use' is selected.
		$this.find(selectors.inputs.dropdown).trigger('change', [false]);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0YXRpY19zZW9fdXJscy9zdGF0aWNfc2VvX3VybHNfY29udGVudHNfY29udGFpbmVyLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwic2F2ZUJhciIsImVsZW1lbnRzIiwiYnV0dG9ucyIsInN1Ym1pdCIsImZpbmQiLCJzdWJtaXRTYXZlIiwic3VibWl0UmVmcmVzaCIsInRlbXBsYXRlcyIsIm1vZGFscyIsInRhYkhlYWRlciIsImZvcm0iLCJzZWxlY3RvcnMiLCJpY29ucyIsImRlbGV0ZSIsImRyYWciLCJjb2xsYXBzZXIiLCJpbWFnZURlbGV0ZSIsImltYWdlTWFwIiwidXBsb2FkIiwiaW5wdXRzIiwiZHJvcGRvd24iLCJ0aHVtYm5haWxJbWFnZURyb3Bkb3duIiwidGl0bGUiLCJmaWxlIiwic3RhdGljU2VvVXJsQ29udGVudFBhbmVsIiwidGFiQm9keSIsInN0YXRpY1Nlb1VybENvbnRlbnRQYW5lbFRpdGxlIiwiY29uZmlndXJhdGlvblJvdyIsImltYWdlTWFwRGF0YUxpc3QiLCJfb25UaXRsZUtleXVwIiwiZXZlbnQiLCIkaW5wdXQiLCJ0YXJnZXQiLCIkdGl0bGUiLCJwYXJlbnRzIiwidGV4dCIsInZhbCIsIl9vbkNvbmZpZ1Jvd01vdXNlRW50ZXIiLCIkcm93IiwiJGltYWdlTWFwSWNvbiIsIiRsaXN0IiwibGVuZ3RoIiwiY2hpbGRyZW4iLCJyZW1vdmVDbGFzcyIsImFkZENsYXNzIiwiX29uU3VibWl0U2F2ZSIsInRyaWdnZXIiLCJfb25TdWJtaXRSZWZyZXNoIiwicmVmcmVzaCIsImluaXQiLCJvbiIsImZpcnN0IiwiZG9uZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7OztBQUtBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxvQ0FERCxFQUdDLENBQ0lDLElBQUlDLE1BRFIsMENBRUlELElBQUlDLE1BRlIseUNBR0MsT0FIRCxDQUhELEVBU0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7QUFJQSxLQUFNQyxVQUFVRCxFQUFFLGtCQUFGLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1FLFdBQVc7QUFDaEI7QUFDQUMsV0FBUzs7QUFFUjtBQUNBQyxXQUFRSCxRQUFRSSxJQUFSLENBQWEsc0JBQWIsQ0FIQTs7QUFLUjtBQUNBQyxlQUFZTCxRQUFRSSxJQUFSLENBQWEsT0FBYixDQU5KOztBQVFSO0FBQ0FFLGtCQUFlTixRQUFRSSxJQUFSLENBQWEsVUFBYjtBQVRQLEdBRk87O0FBY2hCO0FBQ0FHLGFBQVcsRUFmSzs7QUFpQmhCO0FBQ0FDLFVBQVEsRUFsQlE7O0FBb0JoQjtBQUNBQyxhQUFXWCxNQUFNTSxJQUFOLENBQVcsV0FBWCxDQXJCSzs7QUF1QmhCO0FBQ0FNLFFBQU1YLEVBQUUsaUNBQUY7QUF4QlUsRUFBakI7O0FBMkJBOzs7OztBQUtBLEtBQU1ZLFlBQVk7QUFDakI7QUFDQUMsU0FBTztBQUNOO0FBQ0FDLFdBQVEsY0FGRjs7QUFJTjtBQUNBQyxTQUFNLGNBTEE7O0FBT047QUFDQUMsY0FBVyxZQVJMOztBQVVOO0FBQ0FDLGdCQUFhLHFCQVhQOztBQWFOO0FBQ0FDLGFBQVUsd0JBZEo7O0FBZ0JOO0FBQ0FDLFdBQVE7QUFqQkYsR0FGVTs7QUFzQmpCO0FBQ0FDLFVBQVE7QUFDUDtBQUNBQyxhQUFVLGlCQUZIOztBQUlQO0FBQ0FDLDJCQUF3QixvQkFMakI7O0FBT1A7QUFDQUMsVUFBTyxxQkFSQTs7QUFVUDtBQUNBQyxTQUFNO0FBWEMsR0F2QlM7O0FBcUNqQjtBQUNBQyw0QkFBMEIsUUF0Q1Q7O0FBd0NqQjtBQUNBQyxXQUFTLFdBekNROztBQTJDakI7QUFDQUMsaUNBQStCLDRCQTVDZDs7QUE4Q2pCO0FBQ0FDLG9CQUFrQixpQkEvQ0Q7O0FBaURqQjtBQUNBQyxvQkFBa0I7QUFsREQsRUFBbEI7O0FBcURBOzs7OztBQUtBLEtBQU1sQyxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNtQyxhQUFULENBQXVCQyxLQUF2QixFQUE4QjtBQUM3QjtBQUNBLE1BQU1DLFNBQVNoQyxFQUFFK0IsTUFBTUUsTUFBUixDQUFmOztBQUVBO0FBQ0EsTUFBTUMsU0FBU0YsT0FDYkcsT0FEYSxDQUNMdkIsVUFBVWEsd0JBREwsRUFFYnBCLElBRmEsQ0FFUk8sVUFBVWUsNkJBRkYsQ0FBZjs7QUFJQTtBQUNBTyxTQUFPRSxJQUFQLENBQVlKLE9BQU9LLEdBQVAsRUFBWjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNDLHNCQUFULENBQWdDUCxLQUFoQyxFQUF1QztBQUN0QztBQUNBLE1BQU1RLE9BQU92QyxFQUFFK0IsTUFBTUUsTUFBUixDQUFiOztBQUVBO0FBQ0EsTUFBTU8sZ0JBQWdCRCxLQUFLbEMsSUFBTCxDQUFVTyxVQUFVQyxLQUFWLENBQWdCSyxRQUExQixDQUF0Qjs7QUFFQTtBQUNBLE1BQU11QixRQUFRRixLQUFLbEMsSUFBTCxDQUFVTyxVQUFVaUIsZ0JBQXBCLENBQWQ7O0FBRUE7QUFDQSxNQUFJLENBQUNXLGNBQWNFLE1BQWYsSUFBeUIsQ0FBQ0QsTUFBTUMsTUFBcEMsRUFBNEM7QUFDM0M7QUFDQTs7QUFFRCxNQUFJRCxNQUFNRSxRQUFOLEdBQWlCRCxNQUFyQixFQUE2QjtBQUM1QkYsaUJBQWNJLFdBQWQsQ0FBMEIsa0JBQTFCLEVBQThDQyxRQUE5QyxDQUF1RCx5QkFBdkQ7QUFDQSxHQUZELE1BRU87QUFDTkwsaUJBQWNJLFdBQWQsQ0FBMEIseUJBQTFCLEVBQXFEQyxRQUFyRCxDQUE4RCxrQkFBOUQ7QUFDQTtBQUNEOztBQUVEOzs7QUFHQSxVQUFTQyxhQUFULEdBQXlCO0FBQ3hCNUMsV0FDRVMsSUFERixDQUVFb0MsT0FGRixDQUVVLFFBRlY7QUFHQTs7QUFFRDs7O0FBR0EsVUFBU0MsZ0JBQVQsR0FBNEI7QUFDM0I5QyxXQUNFUyxJQURGLENBRUVvQyxPQUZGLENBRVUsUUFGVixFQUVvQixFQUFDRSxTQUFTLElBQVYsRUFGcEI7QUFHQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUF0RCxRQUFPdUQsSUFBUCxHQUFjLGdCQUFROztBQUVyQjtBQUNBbkQsUUFDRW9ELEVBREYsQ0FDSyxPQURMLEVBQ2N2QyxVQUFVUSxNQUFWLENBQWlCRyxLQUQvQixFQUNzQ08sYUFEdEMsRUFFRXFCLEVBRkYsQ0FFSyxZQUZMLEVBRW1CdkMsVUFBVWdCLGdCQUY3QixFQUUrQ1Usc0JBRi9DOztBQUlBO0FBQ0FwQyxXQUFTQyxPQUFULENBQWlCRyxVQUFqQixDQUE0QjZDLEVBQTVCLENBQStCLE9BQS9CLEVBQXdDTCxhQUF4QztBQUNBNUMsV0FBU0MsT0FBVCxDQUFpQkksYUFBakIsQ0FBK0I0QyxFQUEvQixDQUFrQyxPQUFsQyxFQUEyQ0gsZ0JBQTNDOztBQUVBO0FBQ0E5QyxXQUFTUSxTQUFULENBQ0VpQyxRQURGLEdBRUVTLEtBRkYsR0FHRVAsUUFIRixDQUdXLFFBSFg7O0FBS0E7QUFDQTlDLFFBQ0VNLElBREYsQ0FDT08sVUFBVWMsT0FEakIsRUFFRTBCLEtBRkYsR0FHRVAsUUFIRixDQUdXLFdBSFg7O0FBS0E7QUFDQTlDLFFBQ0VNLElBREYsQ0FDT08sVUFBVVEsTUFBVixDQUFpQkMsUUFEeEIsRUFFRTBCLE9BRkYsQ0FFVSxRQUZWLEVBRW9CLENBQUMsS0FBRCxDQUZwQjs7QUFJQTtBQUNBTTtBQUNBLEVBOUJEOztBQWdDQSxRQUFPMUQsTUFBUDtBQUNBLENBdk9GIiwiZmlsZSI6InN0YXRpY19zZW9fdXJscy9zdGF0aWNfc2VvX3VybHNfY29udGVudHNfY29udGFpbmVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzdGF0aWNfc2VvX3VybHNfY29udGVudHNfY29udGFpbmVyLmpzIDIwMTctMDUtMjRcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIENvbnRyb2xsZXIgTW9kdWxlIEZvciBTdGF0aWMgU2VvIFVybCBDb250ZW50IENvbnRhaW5lciAoVGFicylcbiAqXG4gKiBIYW5kbGVzIHRoZSBzdGF0aWMgc2VvIHVybCBjb250ZW50IGNvbnRhaW5lciBmdW5jdGlvbmFsaXR5IGluIHRoZSBzdGF0aWMgc2VvIHVybCBkZXRhaWxzIHBhZ2UuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3N0YXRpY19zZW9fdXJsc19jb250ZW50c19jb250YWluZXInLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5jc3NgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5qc2AsXG5cdFx0J21vZGFsJ1xuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTYXZlIEJhclxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3Qgc2F2ZUJhciA9ICQoJy5ib3R0b20tc2F2ZS1iYXInKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBFbGVtZW50c1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBlbGVtZW50cyA9IHtcblx0XHRcdC8vIEJ1dHRvbnMuXG5cdFx0XHRidXR0b25zOiB7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTdWJtaXQgYnV0dG9uIGdyb3VwLlxuXHRcdFx0XHRzdWJtaXQ6IHNhdmVCYXIuZmluZCgnLnN1Ym1pdC1idXR0b24tZ3JvdXAnKSxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFN1Ym1pdCBidXR0b24gZm9yIHNhdmUgc3RhdGljIHNlbyB1cmxcblx0XHRcdFx0c3VibWl0U2F2ZTogc2F2ZUJhci5maW5kKCcuc2F2ZScpLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU3VibWl0IGJ1dHRvbiBmb3Igc2F2ZSBhbmQgcmVmcmVzaCBzdGF0aWMgc2VvIHVybFxuXHRcdFx0XHRzdWJtaXRSZWZyZXNoOiBzYXZlQmFyLmZpbmQoJy5yZWZyZXNoJyksXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvLyBUZW1wbGF0ZS5cblx0XHRcdHRlbXBsYXRlczoge30sXG5cdFx0XHRcblx0XHRcdC8vIE1vZGFscy5cblx0XHRcdG1vZGFsczoge30sXG5cdFx0XHRcblx0XHRcdC8vIFRhYnMuXG5cdFx0XHR0YWJIZWFkZXI6ICR0aGlzLmZpbmQoJy5uYXYtdGFicycpLFxuXHRcdFx0XG5cdFx0XHQvLyBGb3JtLlxuXHRcdFx0Zm9ybTogJCgnLnN0YXRpYy1zZW8tdXJscy1kZXRhaWxzID4gZm9ybScpXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZWxlY3RvciBTdHJpbmdzXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IHNlbGVjdG9ycyA9IHtcblx0XHRcdC8vIEljb24gc2VsZWN0b3Igc3RyaW5ncy5cblx0XHRcdGljb25zOiB7XG5cdFx0XHRcdC8vIERlbGV0ZSBidXR0b24gb24gdGhlIHBhbmVsIGhlYWRlci5cblx0XHRcdFx0ZGVsZXRlOiAnLmljb24uZGVsZXRlJyxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIERyYWcgYnV0dG9uIG9uIHRoZSBwYW5lbCBoZWFkZXIuXG5cdFx0XHRcdGRyYWc6ICcuZHJhZy1oYW5kbGUnLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQ29sbGFwc2VyIGJ1dHRvbiBvbiB0aGUgcGFuZWwgaGVhZGVyLlxuXHRcdFx0XHRjb2xsYXBzZXI6ICcuY29sbGFwc2VyJyxcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEltYWdlIGRlbGV0ZSBidXR0b24uXG5cdFx0XHRcdGltYWdlRGVsZXRlOiAnLmFjdGlvbi1pY29uLmRlbGV0ZScsXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBJbWFnZSBtYXAgZWRpdCBidXR0b24uXG5cdFx0XHRcdGltYWdlTWFwOiAnLmFjdGlvbi1pY29uLmltYWdlLW1hcCcsXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBVcGxvYWQgaW1hZ2UgYnV0dG9uLlxuXHRcdFx0XHR1cGxvYWQ6ICcuYWN0aW9uLWljb24udXBsb2FkJ1xuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0Ly8gSW5wdXRzIHNlbGVjdG9yIHN0cmluZ3MuXG5cdFx0XHRpbnB1dHM6IHtcblx0XHRcdFx0Ly8gR2VuZXJhbCBpbWFnZSBzZWxlY3QgZHJvcGRvd25zLlxuXHRcdFx0XHRkcm9wZG93bjogJy5kcm9wZG93bi1pbnB1dCcsXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBUaHVtYm5haWwgZHJvcGRvd24uXG5cdFx0XHRcdHRodW1ibmFpbEltYWdlRHJvcGRvd246ICdbbmFtZT1cInRodW1ibmFpbFwiXScsXG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBUaXRsZS5cblx0XHRcdFx0dGl0bGU6ICdpbnB1dFtuYW1lPVwidGl0bGVcIl0nLFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRmlsZS5cblx0XHRcdFx0ZmlsZTogJy5maWxlLWlucHV0J1xuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0Ly8gU3RhdGljIHNlbyB1cmwgY29udGVudCBwYW5lbC5cblx0XHRcdHN0YXRpY1Nlb1VybENvbnRlbnRQYW5lbDogJy5wYW5lbCcsXG5cdFx0XHRcblx0XHRcdC8vIFRhYiBib2R5LlxuXHRcdFx0dGFiQm9keTogJy50YWItcGFuZScsXG5cdFx0XHRcblx0XHRcdC8vIFN0YXRpYyBzZW8gdXJsIGNvbnRlbnQgcGFuZWwgdGl0bGUuXG5cdFx0XHRzdGF0aWNTZW9VcmxDb250ZW50UGFuZWxUaXRsZTogJy5zdGF0aWNTZW9VcmxDb250ZW50LXRpdGxlJyxcblx0XHRcdFxuXHRcdFx0Ly8gU2V0dGluZyByb3cgKGZvcm0gZ3JvdXApLlxuXHRcdFx0Y29uZmlndXJhdGlvblJvdzogJy5yb3cuZm9ybS1ncm91cCcsXG5cdFx0XHRcblx0XHRcdC8vIERhdGEgbGlzdCBjb250YWluZXIgZm9yIGltYWdlIG1hcC5cblx0XHRcdGltYWdlTWFwRGF0YUxpc3Q6ICcuaW1hZ2UtbWFwLWRhdGEtbGlzdCcsXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIGtleS11cCBldmVudCBvbiB0aGUgc3RhdGljU2VvVXJsQ29udGVudCB0aXRsZSBpbnB1dCBmaWVsZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uVGl0bGVLZXl1cChldmVudCkge1xuXHRcdFx0Ly8gVGl0bGUgaW5wdXQgZmllbGQuXG5cdFx0XHRjb25zdCAkaW5wdXQgPSAkKGV2ZW50LnRhcmdldCk7XG5cdFx0XHRcblx0XHRcdC8vIFN0YXRpYyBzZW8gdXJsIGNvbnRlbnQgcGFuZWwgdGl0bGUgZWxlbWVudC5cblx0XHRcdGNvbnN0ICR0aXRsZSA9ICRpbnB1dFxuXHRcdFx0XHQucGFyZW50cyhzZWxlY3RvcnMuc3RhdGljU2VvVXJsQ29udGVudFBhbmVsKVxuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuc3RhdGljU2VvVXJsQ29udGVudFBhbmVsVGl0bGUpO1xuXHRcdFx0XG5cdFx0XHQvLyBUcmFuc2ZlciBpbnB1dCB2YWx1ZSB0byBzdGF0aWNTZW9VcmxDb250ZW50IHBhbmVsIHRpdGxlLlxuXHRcdFx0JHRpdGxlLnRleHQoJGlucHV0LnZhbCgpKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgbW91c2UtZW50ZXIgZXZlbnQgb24gYSBjb25maWd1cmF0aW9uIHJvdy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uQ29uZmlnUm93TW91c2VFbnRlcihldmVudCkge1xuXHRcdFx0Ly8gQ29uZmlndXJhdGlvbiByb3cgZWxlbWVudC5cblx0XHRcdGNvbnN0ICRyb3cgPSAkKGV2ZW50LnRhcmdldCk7XG5cdFx0XHRcblx0XHRcdC8vIEltYWdlIG1hcCBlZGl0IGljb24uXG5cdFx0XHRjb25zdCAkaW1hZ2VNYXBJY29uID0gJHJvdy5maW5kKHNlbGVjdG9ycy5pY29ucy5pbWFnZU1hcCk7XG5cdFx0XHRcblx0XHRcdC8vIEltYWdlIG1hcCBkYXRhIGNvbnRhaW5lciBsaXN0IGVsZW1lbnQuXG5cdFx0XHRjb25zdCAkbGlzdCA9ICRyb3cuZmluZChzZWxlY3RvcnMuaW1hZ2VNYXBEYXRhTGlzdCk7XG5cdFx0XHRcblx0XHRcdC8vIFJldHVybiBpbW1lZGlhdGVseSwgaWYgdGhlIGltYWdlIG1hcCBlZGl0IGljb24gZG9lcyBub3QgZXhpc3QuXG5cdFx0XHRpZiAoISRpbWFnZU1hcEljb24ubGVuZ3RoIHx8ICEkbGlzdC5sZW5ndGgpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAoJGxpc3QuY2hpbGRyZW4oKS5sZW5ndGgpIHtcblx0XHRcdFx0JGltYWdlTWFwSWNvbi5yZW1vdmVDbGFzcygnZmEtZXh0ZXJuYWwtbGluaycpLmFkZENsYXNzKCdmYS1leHRlcm5hbC1saW5rLXNxdWFyZScpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0JGltYWdlTWFwSWNvbi5yZW1vdmVDbGFzcygnZmEtZXh0ZXJuYWwtbGluay1zcXVhcmUnKS5hZGRDbGFzcygnZmEtZXh0ZXJuYWwtbGluaycpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBjbGljayBldmVudCBvbiB0aGUgc2F2ZSBidXR0b24uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uU3VibWl0U2F2ZSgpIHtcblx0XHRcdGVsZW1lbnRzXG5cdFx0XHRcdC5mb3JtXG5cdFx0XHRcdC50cmlnZ2VyKCdzdWJtaXQnKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgY2xpY2sgZXZlbnQgb24gdGhlIHJlZnJlc2ggbGlzdCBpdGVtIGluIHRoZSBzdWJtaXQgYnV0dG9uIGdyb3VwLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblN1Ym1pdFJlZnJlc2goKSB7XG5cdFx0XHRlbGVtZW50c1xuXHRcdFx0XHQuZm9ybVxuXHRcdFx0XHQudHJpZ2dlcignc3VibWl0Jywge3JlZnJlc2g6IHRydWV9KTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdFxuXHRcdFx0Ly8gQXR0YWNoIGV2ZW50IGhhbmRsZXJzIHRvIHNvcnQgYWN0aW9ucywgc3RhdGljIHNlbyB1cmwgY29udGVudCBwYW5lbCBkZWxldGUgYnV0dG9uIGFuZCBpbnB1dHMgZmllbGRzLlxuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdrZXl1cCcsIHNlbGVjdG9ycy5pbnB1dHMudGl0bGUsIF9vblRpdGxlS2V5dXApXG5cdFx0XHRcdC5vbignbW91c2VlbnRlcicsIHNlbGVjdG9ycy5jb25maWd1cmF0aW9uUm93LCBfb25Db25maWdSb3dNb3VzZUVudGVyKTtcblx0XHRcdFxuXHRcdFx0Ly8gQXR0YWNoIGV2ZW50IGxpc3RlbmVycyB0byBzdWJtaXQgYnV0dG9ucy5cblx0XHRcdGVsZW1lbnRzLmJ1dHRvbnMuc3VibWl0U2F2ZS5vbignY2xpY2snLCBfb25TdWJtaXRTYXZlKTtcblx0XHRcdGVsZW1lbnRzLmJ1dHRvbnMuc3VibWl0UmVmcmVzaC5vbignY2xpY2snLCBfb25TdWJtaXRSZWZyZXNoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWN0aXZhdGUgZmlyc3QgdGFiLlxuXHRcdFx0ZWxlbWVudHMudGFiSGVhZGVyXG5cdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdC5maXJzdCgpXG5cdFx0XHRcdC5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcblx0XHRcdC8vIEFjdGl2YXRlIGZpcnN0IHRhYiBjb250ZW50LlxuXHRcdFx0JHRoaXNcblx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLnRhYkJvZHkpXG5cdFx0XHRcdC5maXJzdCgpXG5cdFx0XHRcdC5hZGRDbGFzcygnYWN0aXZlIGluJyk7XG5cdFx0XHRcblx0XHRcdC8vIFRyaWdnZXIgZHJvcGRvd24gY2hhbmdlIGV2ZW50IHRvIGhpZGUgdGhlIHJlbW92ZSBpY29uLCBpZiAnZG8gbm90IHVzZScgaXMgc2VsZWN0ZWQuXG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuaW5wdXRzLmRyb3Bkb3duKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJywgW2ZhbHNlXSk7XG5cdFx0XHRcblx0XHRcdC8vIEZpbmlzaCBpbml0aWFsaXphdGlvbi5cblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7XG4iXX0=
