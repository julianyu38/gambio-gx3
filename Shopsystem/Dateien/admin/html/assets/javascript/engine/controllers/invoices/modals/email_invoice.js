'use strict';

/* --------------------------------------------------------------
 email_invoice.js 2016-05-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Email Invoice Modal Controller
 *
 * Handles the functionality of the Email Invoice modal.
 */
gx.controllers.module('email_invoice', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: {
			subject: $this.find('.subject'),
			emailAddress: $this.find('.email-address')
		}
	};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Send the modal data to the form through an AJAX call.
  *
  * @param {jQuery.Event} event
  */
	function _onSendClick(event) {
		$this.find('.has-error').removeClass('has-error');

		var _$this$data = $this.data(),
		    orderId = _$this$data.orderId,
		    invoiceId = _$this$data.invoiceId;

		var parameters = {
			oID: orderId,
			iID: invoiceId,
			type: 'invoice',
			mail: '1',
			gm_quick_mail: '1',
			preview: '1'
		};
		var url = jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php?' + $.param(parameters);
		var data = {
			gm_mail: module.bindings.emailAddress.get(),
			gm_subject: module.bindings.subject.get()
		};

		if (data.gm_subject === '') {
			$this.find('.subject').parents('.form-group').addClass('has-error');
		}

		if (data.gm_mail === '') {
			$this.find('.email-address').parents('.form-group').addClass('has-error');
		}

		if ($this.find('.has-error').length) {
			return;
		}

		var $sendButton = $(event.target);

		$sendButton.addClass('disabled').prop('disabled', true);

		$.ajax({
			url: url,
			data: data,
			method: 'POST'
		}).done(function (response) {
			var message = jse.core.lang.translate('MAIL_SUCCESS', 'gm_send_order');

			$('.invoices .table-main').DataTable().ajax.reload(null, false);
			$('.invoices .table-main').invoices_overview_filter('reload');

			// Show success message in the admin info box.
			jse.libs.info_box.addSuccessMessage(message);
			$this.modal('hide');
		}).fail(function (jqxhr, textStatus, errorThrown) {
			var title = jse.core.lang.translate('error', 'messages');
			var message = jse.core.lang.translate('MAIL_UNSUCCESS', 'gm_send_order');

			// Show error message in a modal.
			jse.libs.modal.showMessage(title, message);
		}).always(function () {
			$sendButton.removeClass('disabled').prop('disabled', false);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL21vZGFscy9lbWFpbF9pbnZvaWNlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiYmluZGluZ3MiLCJzdWJqZWN0IiwiZmluZCIsImVtYWlsQWRkcmVzcyIsIl9vblNlbmRDbGljayIsImV2ZW50IiwicmVtb3ZlQ2xhc3MiLCJvcmRlcklkIiwiaW52b2ljZUlkIiwicGFyYW1ldGVycyIsIm9JRCIsImlJRCIsInR5cGUiLCJtYWlsIiwiZ21fcXVpY2tfbWFpbCIsInByZXZpZXciLCJ1cmwiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwicGFyYW0iLCJnbV9tYWlsIiwiZ21fc3ViamVjdCIsInBhcmVudHMiLCJhZGRDbGFzcyIsImxlbmd0aCIsIiRzZW5kQnV0dG9uIiwidGFyZ2V0IiwicHJvcCIsImFqYXgiLCJtZXRob2QiLCJkb25lIiwibWVzc2FnZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJEYXRhVGFibGUiLCJyZWxvYWQiLCJpbnZvaWNlc19vdmVydmlld19maWx0ZXIiLCJsaWJzIiwiaW5mb19ib3giLCJhZGRTdWNjZXNzTWVzc2FnZSIsIm1vZGFsIiwiZmFpbCIsImpxeGhyIiwidGV4dFN0YXR1cyIsImVycm9yVGhyb3duIiwidGl0bGUiLCJzaG93TWVzc2FnZSIsImFsd2F5cyIsImluaXQiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7OztBQUtBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0IsZUFBdEIsRUFBdUMsQ0FBQyxPQUFELENBQXZDLEVBQWtELFVBQVNDLElBQVQsRUFBZTs7QUFFaEU7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTO0FBQ2RJLFlBQVU7QUFDVEMsWUFBU0gsTUFBTUksSUFBTixDQUFXLFVBQVgsQ0FEQTtBQUVUQyxpQkFBY0wsTUFBTUksSUFBTixDQUFXLGdCQUFYO0FBRkw7QUFESSxFQUFmOztBQU9BO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxVQUFTRSxZQUFULENBQXNCQyxLQUF0QixFQUE2QjtBQUM1QlAsUUFBTUksSUFBTixDQUFXLFlBQVgsRUFBeUJJLFdBQXpCLENBQXFDLFdBQXJDOztBQUQ0QixvQkFHQ1IsTUFBTUQsSUFBTixFQUhEO0FBQUEsTUFHckJVLE9BSHFCLGVBR3JCQSxPQUhxQjtBQUFBLE1BR1pDLFNBSFksZUFHWkEsU0FIWTs7QUFLNUIsTUFBTUMsYUFBYTtBQUNsQkMsUUFBS0gsT0FEYTtBQUVsQkksUUFBS0gsU0FGYTtBQUdsQkksU0FBTSxTQUhZO0FBSWxCQyxTQUFNLEdBSlk7QUFLbEJDLGtCQUFlLEdBTEc7QUFNbEJDLFlBQVM7QUFOUyxHQUFuQjtBQVFBLE1BQU1DLE1BQU1DLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsMEJBQWhDLEdBQTZEckIsRUFBRXNCLEtBQUYsQ0FBUVosVUFBUixDQUF6RTtBQUNBLE1BQU1aLE9BQU87QUFDWnlCLFlBQVMxQixPQUFPSSxRQUFQLENBQWdCRyxZQUFoQixDQUE2QmlCLEdBQTdCLEVBREc7QUFFWkcsZUFBWTNCLE9BQU9JLFFBQVAsQ0FBZ0JDLE9BQWhCLENBQXdCbUIsR0FBeEI7QUFGQSxHQUFiOztBQUtBLE1BQUl2QixLQUFLMEIsVUFBTCxLQUFvQixFQUF4QixFQUE0QjtBQUMzQnpCLFNBQU1JLElBQU4sQ0FBVyxVQUFYLEVBQXVCc0IsT0FBdkIsQ0FBK0IsYUFBL0IsRUFBOENDLFFBQTlDLENBQXVELFdBQXZEO0FBQ0E7O0FBRUQsTUFBSTVCLEtBQUt5QixPQUFMLEtBQWlCLEVBQXJCLEVBQXlCO0FBQ3hCeEIsU0FBTUksSUFBTixDQUFXLGdCQUFYLEVBQTZCc0IsT0FBN0IsQ0FBcUMsYUFBckMsRUFBb0RDLFFBQXBELENBQTZELFdBQTdEO0FBQ0E7O0FBRUQsTUFBSTNCLE1BQU1JLElBQU4sQ0FBVyxZQUFYLEVBQXlCd0IsTUFBN0IsRUFBcUM7QUFDcEM7QUFDQTs7QUFFRCxNQUFNQyxjQUFjNUIsRUFBRU0sTUFBTXVCLE1BQVIsQ0FBcEI7O0FBRUFELGNBQVlGLFFBQVosQ0FBcUIsVUFBckIsRUFBaUNJLElBQWpDLENBQXNDLFVBQXRDLEVBQWtELElBQWxEOztBQUVBOUIsSUFBRStCLElBQUYsQ0FBTztBQUNOZCxXQURNO0FBRU5uQixhQUZNO0FBR05rQyxXQUFRO0FBSEYsR0FBUCxFQUtFQyxJQUxGLENBS08sb0JBQVk7QUFDakIsT0FBTUMsVUFBVWhCLElBQUlDLElBQUosQ0FBU2dCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxlQUF4QyxDQUFoQjs7QUFFQXBDLEtBQUUsdUJBQUYsRUFBMkJxQyxTQUEzQixHQUF1Q04sSUFBdkMsQ0FBNENPLE1BQTVDLENBQW1ELElBQW5ELEVBQXlELEtBQXpEO0FBQ0F0QyxLQUFFLHVCQUFGLEVBQTJCdUMsd0JBQTNCLENBQW9ELFFBQXBEOztBQUVBO0FBQ0FyQixPQUFJc0IsSUFBSixDQUFTQyxRQUFULENBQWtCQyxpQkFBbEIsQ0FBb0NSLE9BQXBDO0FBQ0FuQyxTQUFNNEMsS0FBTixDQUFZLE1BQVo7QUFDQSxHQWRGLEVBZUVDLElBZkYsQ0FlTyxVQUFDQyxLQUFELEVBQVFDLFVBQVIsRUFBb0JDLFdBQXBCLEVBQW9DO0FBQ3pDLE9BQU1DLFFBQVE5QixJQUFJQyxJQUFKLENBQVNnQixJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FBZDtBQUNBLE9BQU1GLFVBQVVoQixJQUFJQyxJQUFKLENBQVNnQixJQUFULENBQWNDLFNBQWQsQ0FBd0IsZ0JBQXhCLEVBQTBDLGVBQTFDLENBQWhCOztBQUVBO0FBQ0FsQixPQUFJc0IsSUFBSixDQUFTRyxLQUFULENBQWVNLFdBQWYsQ0FBMkJELEtBQTNCLEVBQWtDZCxPQUFsQztBQUNBLEdBckJGLEVBc0JFZ0IsTUF0QkYsQ0FzQlMsWUFBTTtBQUNidEIsZUFBWXJCLFdBQVosQ0FBd0IsVUFBeEIsRUFBb0N1QixJQUFwQyxDQUF5QyxVQUF6QyxFQUFxRCxLQUFyRDtBQUNBLEdBeEJGO0FBeUJBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQWpDLFFBQU9zRCxJQUFQLEdBQWMsVUFBU2xCLElBQVQsRUFBZTtBQUM1QmxDLFFBQU1xRCxFQUFOLENBQVMsT0FBVCxFQUFrQixXQUFsQixFQUErQi9DLFlBQS9CO0FBQ0E0QjtBQUNBLEVBSEQ7O0FBS0EsUUFBT3BDLE1BQVA7QUFDQSxDQTVHRCIsImZpbGUiOiJpbnZvaWNlcy9tb2RhbHMvZW1haWxfaW52b2ljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZW1haWxfaW52b2ljZS5qcyAyMDE2LTA1LTA1XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBFbWFpbCBJbnZvaWNlIE1vZGFsIENvbnRyb2xsZXJcbiAqXG4gKiBIYW5kbGVzIHRoZSBmdW5jdGlvbmFsaXR5IG9mIHRoZSBFbWFpbCBJbnZvaWNlIG1vZGFsLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ2VtYWlsX2ludm9pY2UnLCBbJ21vZGFsJ10sIGZ1bmN0aW9uKGRhdGEpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBWQVJJQUJMRVNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdCAqXG5cdCAqIEB0eXBlIHtqUXVlcnl9XG5cdCAqL1xuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFxuXHQvKipcblx0ICogTW9kdWxlIEluc3RhbmNlXG5cdCAqXG5cdCAqIEB0eXBlIHtPYmplY3R9XG5cdCAqL1xuXHRjb25zdCBtb2R1bGUgPSB7XG5cdFx0YmluZGluZ3M6IHtcblx0XHRcdHN1YmplY3Q6ICR0aGlzLmZpbmQoJy5zdWJqZWN0JyksXG5cdFx0XHRlbWFpbEFkZHJlc3M6ICR0aGlzLmZpbmQoJy5lbWFpbC1hZGRyZXNzJylcblx0XHR9XG5cdH07XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gRlVOQ1RJT05TXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIFNlbmQgdGhlIG1vZGFsIGRhdGEgdG8gdGhlIGZvcm0gdGhyb3VnaCBhbiBBSkFYIGNhbGwuXG5cdCAqXG5cdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxuXHQgKi9cblx0ZnVuY3Rpb24gX29uU2VuZENsaWNrKGV2ZW50KSB7XG5cdFx0JHRoaXMuZmluZCgnLmhhcy1lcnJvcicpLnJlbW92ZUNsYXNzKCdoYXMtZXJyb3InKTtcblx0XHRcblx0XHRjb25zdCB7b3JkZXJJZCwgaW52b2ljZUlkfSA9ICR0aGlzLmRhdGEoKTtcblx0XHRcblx0XHRjb25zdCBwYXJhbWV0ZXJzID0ge1xuXHRcdFx0b0lEOiBvcmRlcklkLFxuXHRcdFx0aUlEOiBpbnZvaWNlSWQsXG5cdFx0XHR0eXBlOiAnaW52b2ljZScsXG5cdFx0XHRtYWlsOiAnMScsXG5cdFx0XHRnbV9xdWlja19tYWlsOiAnMScsXG5cdFx0XHRwcmV2aWV3OiAnMSdcblx0XHR9O1xuXHRcdGNvbnN0IHVybCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9nbV9wZGZfb3JkZXIucGhwPycgKyAkLnBhcmFtKHBhcmFtZXRlcnMpO1xuXHRcdGNvbnN0IGRhdGEgPSB7XG5cdFx0XHRnbV9tYWlsOiBtb2R1bGUuYmluZGluZ3MuZW1haWxBZGRyZXNzLmdldCgpLFxuXHRcdFx0Z21fc3ViamVjdDogbW9kdWxlLmJpbmRpbmdzLnN1YmplY3QuZ2V0KClcblx0XHR9O1xuXHRcdFxuXHRcdGlmIChkYXRhLmdtX3N1YmplY3QgPT09ICcnKSB7XG5cdFx0XHQkdGhpcy5maW5kKCcuc3ViamVjdCcpLnBhcmVudHMoJy5mb3JtLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpO1xuXHRcdH1cblx0XHRcblx0XHRpZiAoZGF0YS5nbV9tYWlsID09PSAnJykge1xuXHRcdFx0JHRoaXMuZmluZCgnLmVtYWlsLWFkZHJlc3MnKS5wYXJlbnRzKCcuZm9ybS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcblx0XHR9XG5cdFx0XG5cdFx0aWYgKCR0aGlzLmZpbmQoJy5oYXMtZXJyb3InKS5sZW5ndGgpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0XG5cdFx0Y29uc3QgJHNlbmRCdXR0b24gPSAkKGV2ZW50LnRhcmdldCk7XG5cdFx0XG5cdFx0JHNlbmRCdXR0b24uYWRkQ2xhc3MoJ2Rpc2FibGVkJykucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcblx0XHRcblx0XHQkLmFqYXgoe1xuXHRcdFx0dXJsLFxuXHRcdFx0ZGF0YSxcblx0XHRcdG1ldGhvZDogJ1BPU1QnXG5cdFx0fSlcblx0XHRcdC5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdNQUlMX1NVQ0NFU1MnLCAnZ21fc2VuZF9vcmRlcicpO1xuXHRcdFx0XHRcblx0XHRcdFx0JCgnLmludm9pY2VzIC50YWJsZS1tYWluJykuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQobnVsbCwgZmFsc2UpO1xuXHRcdFx0XHQkKCcuaW52b2ljZXMgLnRhYmxlLW1haW4nKS5pbnZvaWNlc19vdmVydmlld19maWx0ZXIoJ3JlbG9hZCcpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2hvdyBzdWNjZXNzIG1lc3NhZ2UgaW4gdGhlIGFkbWluIGluZm8gYm94LlxuXHRcdFx0XHRqc2UubGlicy5pbmZvX2JveC5hZGRTdWNjZXNzTWVzc2FnZShtZXNzYWdlKTtcblx0XHRcdFx0JHRoaXMubW9kYWwoJ2hpZGUnKTtcblx0XHRcdH0pXG5cdFx0XHQuZmFpbCgoanF4aHIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSA9PiB7XG5cdFx0XHRcdGNvbnN0IHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyk7XG5cdFx0XHRcdGNvbnN0IG1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnTUFJTF9VTlNVQ0NFU1MnLCAnZ21fc2VuZF9vcmRlcicpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2hvdyBlcnJvciBtZXNzYWdlIGluIGEgbW9kYWwuXG5cdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKHRpdGxlLCBtZXNzYWdlKTtcblx0XHRcdH0pXG5cdFx0XHQuYWx3YXlzKCgpID0+IHtcblx0XHRcdFx0JHNlbmRCdXR0b24ucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG5cdFx0XHR9KTtcblx0fVxuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIElOSVRJQUxJWkFUSU9OXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0JHRoaXMub24oJ2NsaWNrJywgJy5idG4uc2VuZCcsIF9vblNlbmRDbGljayk7XG5cdFx0ZG9uZSgpO1xuXHR9O1xuXHRcblx0cmV0dXJuIG1vZHVsZTtcbn0pOyJdfQ==
