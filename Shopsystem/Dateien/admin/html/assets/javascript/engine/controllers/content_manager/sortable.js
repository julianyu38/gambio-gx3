'use strict';

/* --------------------------------------------------------------
 sortable.js 2018-01-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Content manager controller to implement sortable functionality for pages.
 */
gx.controllers.module('sortable', [jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.js'], function () {
    'use strict';

    var SORTING_ID_ATTRIBUTE = 'data-sorting-id';

    var SAVE_SORTING_REQUEST_URL = 'admin.php?do=ContentManagerPagesAjax/SavePagesSorting';
    var SAVE_SORTING_REQUEST_METHOD = 'POST';

    var $errorModal = $('.sorting-failed.modal');
    var $emptyListTemplate = $('#empty-list');
    var $lists = $('#pages_main, #pages_secondary, #pages_info, #pages_info_box');

    var sortableOptions = {
        items: 'li.content-manager-element',
        axis: 'y',
        cursor: 'move',
        handle: '.sort-handle',
        containment: '#main-content',
        connectWith: '.content-manager-container',
        placeholder: 'col-md-12 content-manager-element sort-placeholder'
    };

    function init(done) {
        $lists.sortable(sortableOptions).on('sortupdate', saveSorting).disableSelection();

        done();
    }

    function handleError() {
        $errorModal.modal('show');
    }

    function handleResponses() {
        for (var _len = arguments.length, responses = Array(_len), _key = 0; _key < _len; _key++) {
            responses[_key] = arguments[_key];
        }

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = responses[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var response = _step.value;

                var parsed = JSON.parse(response[0]);

                if (parsed[0] !== 'success') {
                    handleError();
                }
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }
    }

    function saveSorting(event, ui) {
        var _$;

        if (!ui.item.parent().is('ul')) {
            $lists.sortable('cancel');
        }

        var requests = [];

        function performRequest(index, element) {
            var $list = $(element);

            var ajaxOptions = {
                url: SAVE_SORTING_REQUEST_URL,
                method: SAVE_SORTING_REQUEST_METHOD,
                data: {
                    position: $list.prop('id'),
                    pages: $list.sortable('toArray', { attribute: SORTING_ID_ATTRIBUTE })
                }
            };

            var request = $.ajax(ajaxOptions);

            requests.push(request);

            updateEntryCount($list);
        }

        if (!ui.item.parents('.ui-sortable').is(this)) {
            return;
        }

        $lists.each(performRequest);

        (_$ = $).when.apply(_$, requests).then(handleResponses).fail(handleError);
    }

    function updateEntryCount($list) {
        var $container = $list.find('.content-manager-elements-list');
        var $entries = $container.find('.content-manager-element');
        var $emptyListElement = $entries.not('[' + SORTING_ID_ATTRIBUTE + ']');

        if ($entries.length - $emptyListElement.length === 0) {
            $container.empty().append($emptyListTemplate.clone().html());
        } else {
            $emptyListElement.remove();
        }
    }

    return { init: init };
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRfbWFuYWdlci9zb3J0YWJsZS5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiU09SVElOR19JRF9BVFRSSUJVVEUiLCJTQVZFX1NPUlRJTkdfUkVRVUVTVF9VUkwiLCJTQVZFX1NPUlRJTkdfUkVRVUVTVF9NRVRIT0QiLCIkZXJyb3JNb2RhbCIsIiQiLCIkZW1wdHlMaXN0VGVtcGxhdGUiLCIkbGlzdHMiLCJzb3J0YWJsZU9wdGlvbnMiLCJpdGVtcyIsImF4aXMiLCJjdXJzb3IiLCJoYW5kbGUiLCJjb250YWlubWVudCIsImNvbm5lY3RXaXRoIiwicGxhY2Vob2xkZXIiLCJpbml0IiwiZG9uZSIsInNvcnRhYmxlIiwib24iLCJzYXZlU29ydGluZyIsImRpc2FibGVTZWxlY3Rpb24iLCJoYW5kbGVFcnJvciIsIm1vZGFsIiwiaGFuZGxlUmVzcG9uc2VzIiwicmVzcG9uc2VzIiwicmVzcG9uc2UiLCJwYXJzZWQiLCJKU09OIiwicGFyc2UiLCJldmVudCIsInVpIiwiaXRlbSIsInBhcmVudCIsImlzIiwicmVxdWVzdHMiLCJwZXJmb3JtUmVxdWVzdCIsImluZGV4IiwiZWxlbWVudCIsIiRsaXN0IiwiYWpheE9wdGlvbnMiLCJ1cmwiLCJtZXRob2QiLCJkYXRhIiwicG9zaXRpb24iLCJwcm9wIiwicGFnZXMiLCJhdHRyaWJ1dGUiLCJyZXF1ZXN0IiwiYWpheCIsInB1c2giLCJ1cGRhdGVFbnRyeUNvdW50IiwicGFyZW50cyIsImVhY2giLCJ3aGVuIiwidGhlbiIsImZhaWwiLCIkY29udGFpbmVyIiwiZmluZCIsIiRlbnRyaWVzIiwiJGVtcHR5TGlzdEVsZW1lbnQiLCJub3QiLCJsZW5ndGgiLCJlbXB0eSIsImFwcGVuZCIsImNsb25lIiwiaHRtbCIsInJlbW92ZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7QUFHQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0ksVUFESixFQUdJLENBQ09DLElBQUlDLE1BRFgsMENBRU9ELElBQUlDLE1BRlgsb0NBSEosRUFRSSxZQUFZO0FBQ1I7O0FBRUEsUUFBTUMsdUJBQXVCLGlCQUE3Qjs7QUFFQSxRQUFNQywyQkFBMkIsdURBQWpDO0FBQ0EsUUFBTUMsOEJBQThCLE1BQXBDOztBQUVBLFFBQU1DLGNBQWNDLEVBQUUsdUJBQUYsQ0FBcEI7QUFDQSxRQUFNQyxxQkFBcUJELEVBQUUsYUFBRixDQUEzQjtBQUNBLFFBQU1FLFNBQVNGLEVBQUUsNkRBQUYsQ0FBZjs7QUFFQSxRQUFNRyxrQkFBa0I7QUFDcEJDLGVBQU8sNEJBRGE7QUFFcEJDLGNBQU0sR0FGYztBQUdwQkMsZ0JBQVEsTUFIWTtBQUlwQkMsZ0JBQVEsY0FKWTtBQUtwQkMscUJBQWEsZUFMTztBQU1wQkMscUJBQWEsNEJBTk87QUFPcEJDLHFCQUFhO0FBUE8sS0FBeEI7O0FBVUEsYUFBU0MsSUFBVCxDQUFjQyxJQUFkLEVBQW9CO0FBQ2hCVixlQUNLVyxRQURMLENBQ2NWLGVBRGQsRUFFS1csRUFGTCxDQUVRLFlBRlIsRUFFc0JDLFdBRnRCLEVBR0tDLGdCQUhMOztBQUtBSjtBQUNIOztBQUVELGFBQVNLLFdBQVQsR0FBdUI7QUFDbkJsQixvQkFBWW1CLEtBQVosQ0FBa0IsTUFBbEI7QUFDSDs7QUFFRCxhQUFTQyxlQUFULEdBQXVDO0FBQUEsMENBQVhDLFNBQVc7QUFBWEEscUJBQVc7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFDbkMsaUNBQXVCQSxTQUF2Qiw4SEFBa0M7QUFBQSxvQkFBdkJDLFFBQXVCOztBQUM5QixvQkFBTUMsU0FBU0MsS0FBS0MsS0FBTCxDQUFXSCxTQUFTLENBQVQsQ0FBWCxDQUFmOztBQUVBLG9CQUFJQyxPQUFPLENBQVAsTUFBYyxTQUFsQixFQUE2QjtBQUN6Qkw7QUFDSDtBQUNKO0FBUGtDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRdEM7O0FBRUQsYUFBU0YsV0FBVCxDQUFxQlUsS0FBckIsRUFBNEJDLEVBQTVCLEVBQWdDO0FBQUE7O0FBQzVCLFlBQUksQ0FBQ0EsR0FBR0MsSUFBSCxDQUFRQyxNQUFSLEdBQWlCQyxFQUFqQixDQUFvQixJQUFwQixDQUFMLEVBQWdDO0FBQzVCM0IsbUJBQU9XLFFBQVAsQ0FBZ0IsUUFBaEI7QUFDSDs7QUFFRCxZQUFNaUIsV0FBVyxFQUFqQjs7QUFFQSxpQkFBU0MsY0FBVCxDQUF3QkMsS0FBeEIsRUFBK0JDLE9BQS9CLEVBQXdDO0FBQ3BDLGdCQUFNQyxRQUFRbEMsRUFBRWlDLE9BQUYsQ0FBZDs7QUFFQSxnQkFBTUUsY0FBYztBQUNoQkMscUJBQUt2Qyx3QkFEVztBQUVoQndDLHdCQUFRdkMsMkJBRlE7QUFHaEJ3QyxzQkFBTTtBQUNGQyw4QkFBVUwsTUFBTU0sSUFBTixDQUFXLElBQVgsQ0FEUjtBQUVGQywyQkFBT1AsTUFBTXJCLFFBQU4sQ0FBZSxTQUFmLEVBQTBCLEVBQUM2QixXQUFXOUMsb0JBQVosRUFBMUI7QUFGTDtBQUhVLGFBQXBCOztBQVNBLGdCQUFNK0MsVUFBVTNDLEVBQUU0QyxJQUFGLENBQU9ULFdBQVAsQ0FBaEI7O0FBRUFMLHFCQUFTZSxJQUFULENBQWNGLE9BQWQ7O0FBRUFHLDZCQUFpQlosS0FBakI7QUFDSDs7QUFFRCxZQUFJLENBQUNSLEdBQUdDLElBQUgsQ0FBUW9CLE9BQVIsQ0FBZ0IsY0FBaEIsRUFBZ0NsQixFQUFoQyxDQUFtQyxJQUFuQyxDQUFMLEVBQStDO0FBQzNDO0FBQ0g7O0FBRUQzQixlQUFPOEMsSUFBUCxDQUFZakIsY0FBWjs7QUFFQSxpQkFBRWtCLElBQUYsV0FBVW5CLFFBQVYsRUFDS29CLElBREwsQ0FDVS9CLGVBRFYsRUFFS2dDLElBRkwsQ0FFVWxDLFdBRlY7QUFHSDs7QUFFRCxhQUFTNkIsZ0JBQVQsQ0FBMEJaLEtBQTFCLEVBQWlDO0FBQzdCLFlBQU1rQixhQUFhbEIsTUFBTW1CLElBQU4sQ0FBVyxnQ0FBWCxDQUFuQjtBQUNBLFlBQU1DLFdBQVdGLFdBQVdDLElBQVgsQ0FBZ0IsMEJBQWhCLENBQWpCO0FBQ0EsWUFBTUUsb0JBQW9CRCxTQUFTRSxHQUFULE9BQWlCNUQsb0JBQWpCLE9BQTFCOztBQUVBLFlBQUkwRCxTQUFTRyxNQUFULEdBQWtCRixrQkFBa0JFLE1BQXBDLEtBQStDLENBQW5ELEVBQXNEO0FBQ2xETCx1QkFDS00sS0FETCxHQUVLQyxNQUZMLENBRVkxRCxtQkFBbUIyRCxLQUFuQixHQUEyQkMsSUFBM0IsRUFGWjtBQUdILFNBSkQsTUFJTztBQUNITiw4QkFBa0JPLE1BQWxCO0FBQ0g7QUFDSjs7QUFFRCxXQUFPLEVBQUNuRCxVQUFELEVBQVA7QUFDSCxDQXpHTCIsImZpbGUiOiJjb250ZW50X21hbmFnZXIvc29ydGFibGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNvcnRhYmxlLmpzIDIwMTgtMDEtMDVcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE4IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIENvbnRlbnQgbWFuYWdlciBjb250cm9sbGVyIHRvIGltcGxlbWVudCBzb3J0YWJsZSBmdW5jdGlvbmFsaXR5IGZvciBwYWdlcy5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuICAgICdzb3J0YWJsZScsXG5cbiAgICBbXG4gICAgICAgIGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5jc3NgLFxuICAgICAgICBgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LXVpL2pxdWVyeS11aS5qc2BcbiAgICBdLFxuXG4gICAgZnVuY3Rpb24gKCkge1xuICAgICAgICAndXNlIHN0cmljdCc7XG5cbiAgICAgICAgY29uc3QgU09SVElOR19JRF9BVFRSSUJVVEUgPSAnZGF0YS1zb3J0aW5nLWlkJztcblxuICAgICAgICBjb25zdCBTQVZFX1NPUlRJTkdfUkVRVUVTVF9VUkwgPSAnYWRtaW4ucGhwP2RvPUNvbnRlbnRNYW5hZ2VyUGFnZXNBamF4L1NhdmVQYWdlc1NvcnRpbmcnO1xuICAgICAgICBjb25zdCBTQVZFX1NPUlRJTkdfUkVRVUVTVF9NRVRIT0QgPSAnUE9TVCc7XG5cbiAgICAgICAgY29uc3QgJGVycm9yTW9kYWwgPSAkKCcuc29ydGluZy1mYWlsZWQubW9kYWwnKTtcbiAgICAgICAgY29uc3QgJGVtcHR5TGlzdFRlbXBsYXRlID0gJCgnI2VtcHR5LWxpc3QnKTtcbiAgICAgICAgY29uc3QgJGxpc3RzID0gJCgnI3BhZ2VzX21haW4sICNwYWdlc19zZWNvbmRhcnksICNwYWdlc19pbmZvLCAjcGFnZXNfaW5mb19ib3gnKTtcblxuICAgICAgICBjb25zdCBzb3J0YWJsZU9wdGlvbnMgPSB7XG4gICAgICAgICAgICBpdGVtczogJ2xpLmNvbnRlbnQtbWFuYWdlci1lbGVtZW50JyxcbiAgICAgICAgICAgIGF4aXM6ICd5JyxcbiAgICAgICAgICAgIGN1cnNvcjogJ21vdmUnLFxuICAgICAgICAgICAgaGFuZGxlOiAnLnNvcnQtaGFuZGxlJyxcbiAgICAgICAgICAgIGNvbnRhaW5tZW50OiAnI21haW4tY29udGVudCcsXG4gICAgICAgICAgICBjb25uZWN0V2l0aDogJy5jb250ZW50LW1hbmFnZXItY29udGFpbmVyJyxcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyOiAnY29sLW1kLTEyIGNvbnRlbnQtbWFuYWdlci1lbGVtZW50IHNvcnQtcGxhY2Vob2xkZXInXG4gICAgICAgIH07XG5cbiAgICAgICAgZnVuY3Rpb24gaW5pdChkb25lKSB7XG4gICAgICAgICAgICAkbGlzdHNcbiAgICAgICAgICAgICAgICAuc29ydGFibGUoc29ydGFibGVPcHRpb25zKVxuICAgICAgICAgICAgICAgIC5vbignc29ydHVwZGF0ZScsIHNhdmVTb3J0aW5nKVxuICAgICAgICAgICAgICAgIC5kaXNhYmxlU2VsZWN0aW9uKCk7XG5cbiAgICAgICAgICAgIGRvbmUoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGhhbmRsZUVycm9yKCkge1xuICAgICAgICAgICAgJGVycm9yTW9kYWwubW9kYWwoJ3Nob3cnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGhhbmRsZVJlc3BvbnNlcyguLi5yZXNwb25zZXMpIHtcbiAgICAgICAgICAgIGZvciAoY29uc3QgcmVzcG9uc2Ugb2YgcmVzcG9uc2VzKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgcGFyc2VkID0gSlNPTi5wYXJzZShyZXNwb25zZVswXSk7XG5cbiAgICAgICAgICAgICAgICBpZiAocGFyc2VkWzBdICE9PSAnc3VjY2VzcycpIHtcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlRXJyb3IoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBzYXZlU29ydGluZyhldmVudCwgdWkpIHtcbiAgICAgICAgICAgIGlmICghdWkuaXRlbS5wYXJlbnQoKS5pcygndWwnKSkge1xuICAgICAgICAgICAgICAgICRsaXN0cy5zb3J0YWJsZSgnY2FuY2VsJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNvbnN0IHJlcXVlc3RzID0gW107XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIHBlcmZvcm1SZXF1ZXN0KGluZGV4LCBlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgY29uc3QgJGxpc3QgPSAkKGVsZW1lbnQpO1xuXG4gICAgICAgICAgICAgICAgY29uc3QgYWpheE9wdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIHVybDogU0FWRV9TT1JUSU5HX1JFUVVFU1RfVVJMLFxuICAgICAgICAgICAgICAgICAgICBtZXRob2Q6IFNBVkVfU09SVElOR19SRVFVRVNUX01FVEhPRCxcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246ICRsaXN0LnByb3AoJ2lkJyksXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlczogJGxpc3Quc29ydGFibGUoJ3RvQXJyYXknLCB7YXR0cmlidXRlOiBTT1JUSU5HX0lEX0FUVFJJQlVURX0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgY29uc3QgcmVxdWVzdCA9ICQuYWpheChhamF4T3B0aW9ucyk7XG5cbiAgICAgICAgICAgICAgICByZXF1ZXN0cy5wdXNoKHJlcXVlc3QpO1xuXG4gICAgICAgICAgICAgICAgdXBkYXRlRW50cnlDb3VudCgkbGlzdCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICghdWkuaXRlbS5wYXJlbnRzKCcudWktc29ydGFibGUnKS5pcyh0aGlzKSkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJGxpc3RzLmVhY2gocGVyZm9ybVJlcXVlc3QpO1xuXG4gICAgICAgICAgICAkLndoZW4oLi4ucmVxdWVzdHMpXG4gICAgICAgICAgICAgICAgLnRoZW4oaGFuZGxlUmVzcG9uc2VzKVxuICAgICAgICAgICAgICAgIC5mYWlsKGhhbmRsZUVycm9yKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHVwZGF0ZUVudHJ5Q291bnQoJGxpc3QpIHtcbiAgICAgICAgICAgIGNvbnN0ICRjb250YWluZXIgPSAkbGlzdC5maW5kKCcuY29udGVudC1tYW5hZ2VyLWVsZW1lbnRzLWxpc3QnKTtcbiAgICAgICAgICAgIGNvbnN0ICRlbnRyaWVzID0gJGNvbnRhaW5lci5maW5kKCcuY29udGVudC1tYW5hZ2VyLWVsZW1lbnQnKTtcbiAgICAgICAgICAgIGNvbnN0ICRlbXB0eUxpc3RFbGVtZW50ID0gJGVudHJpZXMubm90KGBbJHtTT1JUSU5HX0lEX0FUVFJJQlVURX1dYCk7XG5cbiAgICAgICAgICAgIGlmICgkZW50cmllcy5sZW5ndGggLSAkZW1wdHlMaXN0RWxlbWVudC5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICAgICAkY29udGFpbmVyXG4gICAgICAgICAgICAgICAgICAgIC5lbXB0eSgpXG4gICAgICAgICAgICAgICAgICAgIC5hcHBlbmQoJGVtcHR5TGlzdFRlbXBsYXRlLmNsb25lKCkuaHRtbCgpKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJGVtcHR5TGlzdEVsZW1lbnQucmVtb3ZlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4ge2luaXR9O1xuICAgIH0pO1xuIl19
