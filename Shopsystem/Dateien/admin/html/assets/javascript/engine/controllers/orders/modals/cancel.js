'use strict';

/* --------------------------------------------------------------
 cancel_modal.js 2016-10-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Cancel Order Modal Controller
 */
gx.controllers.module('cancel', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: {
			selectedOrders: $this.find('.selected-orders'),
			reStock: $this.find('.re-stock'),
			reShip: $this.find('.re-ship'),
			reActivate: $this.find('.re-activate'),
			notifyCustomer: $this.find('.notify-customer'),
			sendComments: $this.find('.send-comments'),
			cancellationComments: $this.find('.cancellation-comments')
		}
	};

	if ($this.find('.cancel-invoice').length) {
		module.bindings.cancelInvoice = $this.find('.cancel-invoice');
	}

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Send the modal data to the form through an AJAX call.
  *
  * @param {jQuery.Event} event
  */
	function _onSendClick(event) {
		var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=OrdersModalsAjax/CancelOrder';
		var data = {
			selectedOrders: module.bindings.selectedOrders.get().split(', '),
			reStock: module.bindings.reStock.get(),
			reShip: module.bindings.reShip.get(),
			reActivate: module.bindings.reActivate.get(),
			notifyCustomer: module.bindings.notifyCustomer.get(),
			sendComments: module.bindings.sendComments.get(),
			cancellationComments: module.bindings.cancellationComments.get(),
			pageToken: jse.core.config.get('pageToken')
		};

		if (module.bindings.cancelInvoice) {
			data.cancelInvoice = module.bindings.cancelInvoice.get();
		}

		var $sendButton = $(event.target);

		$sendButton.addClass('disabled').prop('disabled', true);

		$.ajax({
			url: url,
			data: data,
			method: 'POST',
			dataType: 'json'
		}).done(function (response) {

			for (var key in response.urls) {
				$.ajax({
					type: "POST",
					url: response.urls[key],
					async: false
				});
			}

			jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('CANCEL_ORDERS_SUCCESS', 'admin_orders'));
			$('.orders .table-main').DataTable().ajax.reload(null, false);
			$('.orders .table-main').orders_overview_filter('reload');
		}).fail(function (jqxhr, textStatus, errorThrown) {
			jse.libs.modal.message({
				title: jse.core.lang.translate('error', 'messages'),
				content: jse.core.lang.translate('CANCEL_ORDERS_ERROR', 'admin_orders')
			});
		}).always(function () {
			$this.modal('hide');
			$sendButton.removeClass('disabled').prop('disabled', false);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvY2FuY2VsLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiYmluZGluZ3MiLCJzZWxlY3RlZE9yZGVycyIsImZpbmQiLCJyZVN0b2NrIiwicmVTaGlwIiwicmVBY3RpdmF0ZSIsIm5vdGlmeUN1c3RvbWVyIiwic2VuZENvbW1lbnRzIiwiY2FuY2VsbGF0aW9uQ29tbWVudHMiLCJsZW5ndGgiLCJjYW5jZWxJbnZvaWNlIiwiX29uU2VuZENsaWNrIiwiZXZlbnQiLCJ1cmwiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0Iiwic3BsaXQiLCJwYWdlVG9rZW4iLCIkc2VuZEJ1dHRvbiIsInRhcmdldCIsImFkZENsYXNzIiwicHJvcCIsImFqYXgiLCJtZXRob2QiLCJkYXRhVHlwZSIsImRvbmUiLCJyZXNwb25zZSIsImtleSIsInVybHMiLCJ0eXBlIiwiYXN5bmMiLCJsaWJzIiwiaW5mb19ib3giLCJhZGRTdWNjZXNzTWVzc2FnZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJEYXRhVGFibGUiLCJyZWxvYWQiLCJvcmRlcnNfb3ZlcnZpZXdfZmlsdGVyIiwiZmFpbCIsImpxeGhyIiwidGV4dFN0YXR1cyIsImVycm9yVGhyb3duIiwibW9kYWwiLCJtZXNzYWdlIiwidGl0bGUiLCJjb250ZW50IiwiYWx3YXlzIiwicmVtb3ZlQ2xhc3MiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7O0FBR0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUFzQixRQUF0QixFQUFnQyxDQUFDLE9BQUQsQ0FBaEMsRUFBMkMsVUFBU0MsSUFBVCxFQUFlOztBQUV6RDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVM7QUFDZEksWUFBVTtBQUNUQyxtQkFBZ0JILE1BQU1JLElBQU4sQ0FBVyxrQkFBWCxDQURQO0FBRVRDLFlBQVNMLE1BQU1JLElBQU4sQ0FBVyxXQUFYLENBRkE7QUFHVEUsV0FBUU4sTUFBTUksSUFBTixDQUFXLFVBQVgsQ0FIQztBQUlURyxlQUFZUCxNQUFNSSxJQUFOLENBQVcsY0FBWCxDQUpIO0FBS1RJLG1CQUFnQlIsTUFBTUksSUFBTixDQUFXLGtCQUFYLENBTFA7QUFNVEssaUJBQWNULE1BQU1JLElBQU4sQ0FBVyxnQkFBWCxDQU5MO0FBT1RNLHlCQUFzQlYsTUFBTUksSUFBTixDQUFXLHdCQUFYO0FBUGI7QUFESSxFQUFmOztBQVlBLEtBQUlKLE1BQU1JLElBQU4sQ0FBVyxpQkFBWCxFQUE4Qk8sTUFBbEMsRUFBMEM7QUFDekNiLFNBQU9JLFFBQVAsQ0FBZ0JVLGFBQWhCLEdBQWdDWixNQUFNSSxJQUFOLENBQVcsaUJBQVgsQ0FBaEM7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU1MsWUFBVCxDQUFzQkMsS0FBdEIsRUFBNkI7QUFDNUIsTUFBTUMsTUFBTUMsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxrREFBNUM7QUFDQSxNQUFNcEIsT0FBTztBQUNaSSxtQkFBZ0JMLE9BQU9JLFFBQVAsQ0FBZ0JDLGNBQWhCLENBQStCZ0IsR0FBL0IsR0FBcUNDLEtBQXJDLENBQTJDLElBQTNDLENBREo7QUFFWmYsWUFBU1AsT0FBT0ksUUFBUCxDQUFnQkcsT0FBaEIsQ0FBd0JjLEdBQXhCLEVBRkc7QUFHWmIsV0FBUVIsT0FBT0ksUUFBUCxDQUFnQkksTUFBaEIsQ0FBdUJhLEdBQXZCLEVBSEk7QUFJWlosZUFBWVQsT0FBT0ksUUFBUCxDQUFnQkssVUFBaEIsQ0FBMkJZLEdBQTNCLEVBSkE7QUFLWlgsbUJBQWdCVixPQUFPSSxRQUFQLENBQWdCTSxjQUFoQixDQUErQlcsR0FBL0IsRUFMSjtBQU1aVixpQkFBY1gsT0FBT0ksUUFBUCxDQUFnQk8sWUFBaEIsQ0FBNkJVLEdBQTdCLEVBTkY7QUFPWlQseUJBQXNCWixPQUFPSSxRQUFQLENBQWdCUSxvQkFBaEIsQ0FBcUNTLEdBQXJDLEVBUFY7QUFRWkUsY0FBV0wsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQVJDLEdBQWI7O0FBV0EsTUFBR3JCLE9BQU9JLFFBQVAsQ0FBZ0JVLGFBQW5CLEVBQWtDO0FBQ2pDYixRQUFLYSxhQUFMLEdBQXFCZCxPQUFPSSxRQUFQLENBQWdCVSxhQUFoQixDQUE4Qk8sR0FBOUIsRUFBckI7QUFDQTs7QUFFRCxNQUFNRyxjQUFjckIsRUFBRWEsTUFBTVMsTUFBUixDQUFwQjs7QUFFQUQsY0FBWUUsUUFBWixDQUFxQixVQUFyQixFQUFpQ0MsSUFBakMsQ0FBc0MsVUFBdEMsRUFBa0QsSUFBbEQ7O0FBRUF4QixJQUFFeUIsSUFBRixDQUFPO0FBQ05YLFdBRE07QUFFTmhCLGFBRk07QUFHTjRCLFdBQVEsTUFIRjtBQUlOQyxhQUFVO0FBSkosR0FBUCxFQU1FQyxJQU5GLENBTU8sVUFBU0MsUUFBVCxFQUFtQjs7QUFFeEIsUUFBSyxJQUFJQyxHQUFULElBQWdCRCxTQUFTRSxJQUF6QixFQUErQjtBQUM5Qi9CLE1BQUV5QixJQUFGLENBQU87QUFDTk8sV0FBTSxNQURBO0FBRU5sQixVQUFLZSxTQUFTRSxJQUFULENBQWNELEdBQWQsQ0FGQztBQUdORyxZQUFPO0FBSEQsS0FBUDtBQUtBOztBQUVEbEIsT0FBSW1CLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsaUJBQWxCLENBQ0NyQixJQUFJQyxJQUFKLENBQVNxQixJQUFULENBQWNDLFNBQWQsQ0FBd0IsdUJBQXhCLEVBQWlELGNBQWpELENBREQ7QUFFQXRDLEtBQUUscUJBQUYsRUFBeUJ1QyxTQUF6QixHQUFxQ2QsSUFBckMsQ0FBMENlLE1BQTFDLENBQWlELElBQWpELEVBQXVELEtBQXZEO0FBQ0F4QyxLQUFFLHFCQUFGLEVBQXlCeUMsc0JBQXpCLENBQWdELFFBQWhEO0FBQ0EsR0FwQkYsRUFxQkVDLElBckJGLENBcUJPLFVBQVNDLEtBQVQsRUFBZ0JDLFVBQWhCLEVBQTRCQyxXQUE1QixFQUF5QztBQUM5QzlCLE9BQUltQixJQUFKLENBQVNZLEtBQVQsQ0FBZUMsT0FBZixDQUF1QjtBQUN0QkMsV0FBT2pDLElBQUlDLElBQUosQ0FBU3FCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQURlO0FBRXRCVyxhQUFTbEMsSUFBSUMsSUFBSixDQUFTcUIsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHFCQUF4QixFQUErQyxjQUEvQztBQUZhLElBQXZCO0FBSUEsR0ExQkYsRUEyQkVZLE1BM0JGLENBMkJTLFlBQVc7QUFDbEJuRCxTQUFNK0MsS0FBTixDQUFZLE1BQVo7QUFDQXpCLGVBQVk4QixXQUFaLENBQXdCLFVBQXhCLEVBQW9DM0IsSUFBcEMsQ0FBeUMsVUFBekMsRUFBcUQsS0FBckQ7QUFDQSxHQTlCRjtBQStCQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUEzQixRQUFPdUQsSUFBUCxHQUFjLFVBQVN4QixJQUFULEVBQWU7QUFDNUI3QixRQUFNc0QsRUFBTixDQUFTLE9BQVQsRUFBa0IsV0FBbEIsRUFBK0J6QyxZQUEvQjtBQUNBZ0I7QUFDQSxFQUhEOztBQUtBLFFBQU8vQixNQUFQO0FBQ0EsQ0E3R0QiLCJmaWxlIjoib3JkZXJzL21vZGFscy9jYW5jZWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gY2FuY2VsX21vZGFsLmpzIDIwMTYtMTAtMTJcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogQ2FuY2VsIE9yZGVyIE1vZGFsIENvbnRyb2xsZXJcclxuICovXHJcbmd4LmNvbnRyb2xsZXJzLm1vZHVsZSgnY2FuY2VsJywgWydtb2RhbCddLCBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIFZBUklBQkxFU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdCAqXHJcblx0ICogQHR5cGUge2pRdWVyeX1cclxuXHQgKi9cclxuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIEluc3RhbmNlXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG1vZHVsZSA9IHtcclxuXHRcdGJpbmRpbmdzOiB7XHJcblx0XHRcdHNlbGVjdGVkT3JkZXJzOiAkdGhpcy5maW5kKCcuc2VsZWN0ZWQtb3JkZXJzJyksXHJcblx0XHRcdHJlU3RvY2s6ICR0aGlzLmZpbmQoJy5yZS1zdG9jaycpLFxyXG5cdFx0XHRyZVNoaXA6ICR0aGlzLmZpbmQoJy5yZS1zaGlwJyksXHJcblx0XHRcdHJlQWN0aXZhdGU6ICR0aGlzLmZpbmQoJy5yZS1hY3RpdmF0ZScpLFxyXG5cdFx0XHRub3RpZnlDdXN0b21lcjogJHRoaXMuZmluZCgnLm5vdGlmeS1jdXN0b21lcicpLFxyXG5cdFx0XHRzZW5kQ29tbWVudHM6ICR0aGlzLmZpbmQoJy5zZW5kLWNvbW1lbnRzJyksXHJcblx0XHRcdGNhbmNlbGxhdGlvbkNvbW1lbnRzOiAkdGhpcy5maW5kKCcuY2FuY2VsbGF0aW9uLWNvbW1lbnRzJylcclxuXHRcdH1cclxuXHR9O1xyXG5cdFxyXG5cdGlmICgkdGhpcy5maW5kKCcuY2FuY2VsLWludm9pY2UnKS5sZW5ndGgpIHtcclxuXHRcdG1vZHVsZS5iaW5kaW5ncy5jYW5jZWxJbnZvaWNlID0gJHRoaXMuZmluZCgnLmNhbmNlbC1pbnZvaWNlJyk7XHJcblx0fVxyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIEZVTkNUSU9OU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNlbmQgdGhlIG1vZGFsIGRhdGEgdG8gdGhlIGZvcm0gdGhyb3VnaCBhbiBBSkFYIGNhbGwuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25TZW5kQ2xpY2soZXZlbnQpIHtcclxuXHRcdGNvbnN0IHVybCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89T3JkZXJzTW9kYWxzQWpheC9DYW5jZWxPcmRlcic7XHJcblx0XHRjb25zdCBkYXRhID0ge1xyXG5cdFx0XHRzZWxlY3RlZE9yZGVyczogbW9kdWxlLmJpbmRpbmdzLnNlbGVjdGVkT3JkZXJzLmdldCgpLnNwbGl0KCcsICcpLFxyXG5cdFx0XHRyZVN0b2NrOiBtb2R1bGUuYmluZGluZ3MucmVTdG9jay5nZXQoKSxcclxuXHRcdFx0cmVTaGlwOiBtb2R1bGUuYmluZGluZ3MucmVTaGlwLmdldCgpLFxyXG5cdFx0XHRyZUFjdGl2YXRlOiBtb2R1bGUuYmluZGluZ3MucmVBY3RpdmF0ZS5nZXQoKSxcclxuXHRcdFx0bm90aWZ5Q3VzdG9tZXI6IG1vZHVsZS5iaW5kaW5ncy5ub3RpZnlDdXN0b21lci5nZXQoKSxcclxuXHRcdFx0c2VuZENvbW1lbnRzOiBtb2R1bGUuYmluZGluZ3Muc2VuZENvbW1lbnRzLmdldCgpLFxyXG5cdFx0XHRjYW5jZWxsYXRpb25Db21tZW50czogbW9kdWxlLmJpbmRpbmdzLmNhbmNlbGxhdGlvbkNvbW1lbnRzLmdldCgpLFxyXG5cdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpXHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHRpZihtb2R1bGUuYmluZGluZ3MuY2FuY2VsSW52b2ljZSkge1xyXG5cdFx0XHRkYXRhLmNhbmNlbEludm9pY2UgPSBtb2R1bGUuYmluZGluZ3MuY2FuY2VsSW52b2ljZS5nZXQoKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Y29uc3QgJHNlbmRCdXR0b24gPSAkKGV2ZW50LnRhcmdldCk7XHJcblx0XHRcclxuXHRcdCRzZW5kQnV0dG9uLmFkZENsYXNzKCdkaXNhYmxlZCcpLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XHJcblx0XHRcclxuXHRcdCQuYWpheCh7XHJcblx0XHRcdHVybCxcclxuXHRcdFx0ZGF0YSxcclxuXHRcdFx0bWV0aG9kOiAnUE9TVCcsXHJcblx0XHRcdGRhdGFUeXBlOiAnanNvbidcclxuXHRcdH0pXHJcblx0XHRcdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Zm9yIChsZXQga2V5IGluIHJlc3BvbnNlLnVybHMpIHtcclxuXHRcdFx0XHRcdCQuYWpheCh7XHJcblx0XHRcdFx0XHRcdHR5cGU6IFwiUE9TVFwiLFxyXG5cdFx0XHRcdFx0XHR1cmw6IHJlc3BvbnNlLnVybHNba2V5XSxcclxuXHRcdFx0XHRcdFx0YXN5bmM6IGZhbHNlXHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0anNlLmxpYnMuaW5mb19ib3guYWRkU3VjY2Vzc01lc3NhZ2UoXHJcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQ0FOQ0VMX09SREVSU19TVUNDRVNTJywgJ2FkbWluX29yZGVycycpKTtcclxuXHRcdFx0XHQkKCcub3JkZXJzIC50YWJsZS1tYWluJykuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQobnVsbCwgZmFsc2UpO1xyXG5cdFx0XHRcdCQoJy5vcmRlcnMgLnRhYmxlLW1haW4nKS5vcmRlcnNfb3ZlcnZpZXdfZmlsdGVyKCdyZWxvYWQnKTtcclxuXHRcdFx0fSlcclxuXHRcdFx0LmZhaWwoZnVuY3Rpb24oanF4aHIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XHJcblx0XHRcdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZSh7XHJcblx0XHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyksXHJcblx0XHRcdFx0XHRjb250ZW50OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQ0FOQ0VMX09SREVSU19FUlJPUicsICdhZG1pbl9vcmRlcnMnKVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQuYWx3YXlzKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdCR0aGlzLm1vZGFsKCdoaWRlJyk7XHJcblx0XHRcdFx0JHNlbmRCdXR0b24ucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcblx0XHRcdH0pO1xyXG5cdH1cclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0JHRoaXMub24oJ2NsaWNrJywgJy5idG4uc2VuZCcsIF9vblNlbmRDbGljayk7XHJcblx0XHRkb25lKCk7XHJcblx0fTtcclxuXHRcclxuXHRyZXR1cm4gbW9kdWxlO1xyXG59KTsiXX0=
