'use strict';

/* --------------------------------------------------------------
 state.js 2016-10-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles the table state for filtering, pagination and sorting.
 *
 * This controller will update the window history with the current state of the table. It reacts
 * to specific events such as filtering, pagination and sorting changes. After the window history
 * is updated the user will be able to navigate forth or backwards.
 *
 * Notice #1: This module must handle the window's pop-state events and not other modules because
 * this will lead to unnecessary code duplication and multiple AJAX requests.
 *
 * Notice #2: The window state must be always in sync with the URL for easier manipulation.
 */
gx.controllers.module('state', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Window History Support
  *
  * @type {Boolean}
  */
	var historySupport = jse.core.config.get('history');

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get parsed state from the URL GET parameters.
  *
  * @return {Object} Returns the table state.
  */
	function _getState() {
		return $.deparam(window.location.search.slice(1));
	}

	/**
  * Set the state to the browser's history.
  *
  * The state is stored for enabling back and forth navigation from the browser.
  *
  * @param {Object} state Contains the new table state.
  */
	function _setState(state) {
		var url = window.location.origin + window.location.pathname + '?' + $.param(state);
		window.history.pushState(state, '', url);
	}

	/**
  * Update page navigation state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} pagination Contains the DataTable pagination info.
  */
	function _onPageChange(event, pagination) {
		var state = _getState();

		state.page = pagination.page + 1;

		_setState(state);
	}

	/**
  * Update page length state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Number} length New page length.
  */
	function _onLengthChange(event, length) {
		var state = _getState();

		state.page = 1;
		state.length = length;

		_setState(state);
	}

	/**
  * Update filter state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} filter Contains the filtering values.
  */
	function _onFilterChange(event, filter) {
		var state = _getState();

		state.page = 1;
		state.filter = filter;

		_setState(state);
	}

	/**
  * Update sort state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} sort Contains column sorting info {index, name, direction}.
  */
	function _onSortChange(event, sort) {
		var state = _getState();

		state.sort = (sort.direction === 'desc' ? '-' : '+') + sort.name;

		_setState(state);
	}

	/**
  * Set the correct table state.
  *
  * This method will parse the new popped state and apply it on the table. It must be the only place where this
  * happens in order to avoid multiple AJAX requests and data collisions.
  *
  * @param {jQuery.Event} event jQuery event object.
  */
	function _onWindowPopState(event) {
		var state = event.originalEvent.state || {};

		if (state.page) {
			$this.find('.page-navigation select').val(state.page);
			$this.DataTable().page(parseInt(state.page) - 1);
		}

		if (state.length) {
			$this.find('.page-length select').val(state.length);
			$this.DataTable().page.len(parseInt(state.length));
		}

		if (state.sort) {
			var _$this$DataTable$init = $this.DataTable().init(),
			    columns = _$this$DataTable$init.columns;

			var direction = state.sort.charAt(0) === '-' ? 'desc' : 'asc';
			var name = state.sort.slice(1);
			var index = 1; // Default Value

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = columns[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var column = _step.value;

					if (column.name === name) {
						index = columns.indexOf(column);
						break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			$this.DataTable().order([index, direction]);
		}

		if (state.filter) {
			// Update the filtering input elements. 
			for (var _column in state.filter) {
				var value = state.filter[_column];

				if (value.constructor === Array) {
					value = value.join('||'); // Join arrays into a single string.
				}

				$this.DataTable().column(_column + ':name').search(value);
			}
		}

		$this.DataTable().draw(false);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		if (historySupport) {
			$this.on('datatable_custom_pagination:page_change', _onPageChange).on('datatable_custom_pagination:length_change', _onLengthChange).on('datatable_custom_sorting:change', _onSortChange).on('quick_edit_filter:change', _onFilterChange);

			$(window).on('popstate', _onWindowPopState);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvb3ZlcnZpZXcvc3RhdGUuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJoaXN0b3J5U3VwcG9ydCIsImNvcmUiLCJjb25maWciLCJnZXQiLCJfZ2V0U3RhdGUiLCJkZXBhcmFtIiwid2luZG93IiwibG9jYXRpb24iLCJzZWFyY2giLCJzbGljZSIsIl9zZXRTdGF0ZSIsInN0YXRlIiwidXJsIiwib3JpZ2luIiwicGF0aG5hbWUiLCJwYXJhbSIsImhpc3RvcnkiLCJwdXNoU3RhdGUiLCJfb25QYWdlQ2hhbmdlIiwiZXZlbnQiLCJwYWdpbmF0aW9uIiwicGFnZSIsIl9vbkxlbmd0aENoYW5nZSIsImxlbmd0aCIsIl9vbkZpbHRlckNoYW5nZSIsImZpbHRlciIsIl9vblNvcnRDaGFuZ2UiLCJzb3J0IiwiZGlyZWN0aW9uIiwibmFtZSIsIl9vbldpbmRvd1BvcFN0YXRlIiwib3JpZ2luYWxFdmVudCIsImZpbmQiLCJ2YWwiLCJEYXRhVGFibGUiLCJwYXJzZUludCIsImxlbiIsImluaXQiLCJjb2x1bW5zIiwiY2hhckF0IiwiaW5kZXgiLCJjb2x1bW4iLCJpbmRleE9mIiwib3JkZXIiLCJ2YWx1ZSIsImNvbnN0cnVjdG9yIiwiQXJyYXkiLCJqb2luIiwiZHJhdyIsImRvbmUiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7QUFZQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsT0FERCxFQUdDLENBQ0lDLElBQUlDLE1BRFIsa0RBSEQsRUFPQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNTCxTQUFTLEVBQWY7O0FBRUE7Ozs7O0FBS0EsS0FBTU0saUJBQWlCTCxJQUFJTSxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFNBQXBCLENBQXZCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxVQUFTQyxTQUFULEdBQXFCO0FBQ3BCLFNBQU9MLEVBQUVNLE9BQUYsQ0FBVUMsT0FBT0MsUUFBUCxDQUFnQkMsTUFBaEIsQ0FBdUJDLEtBQXZCLENBQTZCLENBQTdCLENBQVYsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0MsU0FBVCxDQUFtQkMsS0FBbkIsRUFBMEI7QUFDekIsTUFBTUMsTUFBTU4sT0FBT0MsUUFBUCxDQUFnQk0sTUFBaEIsR0FBeUJQLE9BQU9DLFFBQVAsQ0FBZ0JPLFFBQXpDLEdBQW9ELEdBQXBELEdBQTBEZixFQUFFZ0IsS0FBRixDQUFRSixLQUFSLENBQXRFO0FBQ0FMLFNBQU9VLE9BQVAsQ0FBZUMsU0FBZixDQUF5Qk4sS0FBekIsRUFBZ0MsRUFBaEMsRUFBb0NDLEdBQXBDO0FBQ0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNNLGFBQVQsQ0FBdUJDLEtBQXZCLEVBQThCQyxVQUE5QixFQUEwQztBQUN6QyxNQUFNVCxRQUFRUCxXQUFkOztBQUVBTyxRQUFNVSxJQUFOLEdBQWFELFdBQVdDLElBQVgsR0FBa0IsQ0FBL0I7O0FBRUFYLFlBQVVDLEtBQVY7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU1csZUFBVCxDQUF5QkgsS0FBekIsRUFBZ0NJLE1BQWhDLEVBQXdDO0FBQ3ZDLE1BQU1aLFFBQVFQLFdBQWQ7O0FBRUFPLFFBQU1VLElBQU4sR0FBYSxDQUFiO0FBQ0FWLFFBQU1ZLE1BQU4sR0FBZUEsTUFBZjs7QUFFQWIsWUFBVUMsS0FBVjtBQUNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTYSxlQUFULENBQXlCTCxLQUF6QixFQUFnQ00sTUFBaEMsRUFBd0M7QUFDdkMsTUFBTWQsUUFBUVAsV0FBZDs7QUFFQU8sUUFBTVUsSUFBTixHQUFhLENBQWI7QUFDQVYsUUFBTWMsTUFBTixHQUFlQSxNQUFmOztBQUVBZixZQUFVQyxLQUFWO0FBQ0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNlLGFBQVQsQ0FBdUJQLEtBQXZCLEVBQThCUSxJQUE5QixFQUFvQztBQUNuQyxNQUFNaEIsUUFBUVAsV0FBZDs7QUFFQU8sUUFBTWdCLElBQU4sR0FBYSxDQUFDQSxLQUFLQyxTQUFMLEtBQW1CLE1BQW5CLEdBQTRCLEdBQTVCLEdBQWtDLEdBQW5DLElBQTBDRCxLQUFLRSxJQUE1RDs7QUFFQW5CLFlBQVVDLEtBQVY7QUFDQTs7QUFFRDs7Ozs7Ozs7QUFRQSxVQUFTbUIsaUJBQVQsQ0FBMkJYLEtBQTNCLEVBQWtDO0FBQ2pDLE1BQU1SLFFBQVFRLE1BQU1ZLGFBQU4sQ0FBb0JwQixLQUFwQixJQUE2QixFQUEzQzs7QUFFQSxNQUFJQSxNQUFNVSxJQUFWLEVBQWdCO0FBQ2Z2QixTQUFNa0MsSUFBTixDQUFXLHlCQUFYLEVBQXNDQyxHQUF0QyxDQUEwQ3RCLE1BQU1VLElBQWhEO0FBQ0F2QixTQUFNb0MsU0FBTixHQUFrQmIsSUFBbEIsQ0FBdUJjLFNBQVN4QixNQUFNVSxJQUFmLElBQXVCLENBQTlDO0FBQ0E7O0FBRUQsTUFBSVYsTUFBTVksTUFBVixFQUFrQjtBQUNqQnpCLFNBQU1rQyxJQUFOLENBQVcscUJBQVgsRUFBa0NDLEdBQWxDLENBQXNDdEIsTUFBTVksTUFBNUM7QUFDQXpCLFNBQU1vQyxTQUFOLEdBQWtCYixJQUFsQixDQUF1QmUsR0FBdkIsQ0FBMkJELFNBQVN4QixNQUFNWSxNQUFmLENBQTNCO0FBQ0E7O0FBRUQsTUFBSVosTUFBTWdCLElBQVYsRUFBZ0I7QUFBQSwrQkFDRzdCLE1BQU1vQyxTQUFOLEdBQWtCRyxJQUFsQixFQURIO0FBQUEsT0FDUkMsT0FEUSx5QkFDUkEsT0FEUTs7QUFFZixPQUFNVixZQUFZakIsTUFBTWdCLElBQU4sQ0FBV1ksTUFBWCxDQUFrQixDQUFsQixNQUF5QixHQUF6QixHQUErQixNQUEvQixHQUF3QyxLQUExRDtBQUNBLE9BQU1WLE9BQU9sQixNQUFNZ0IsSUFBTixDQUFXbEIsS0FBWCxDQUFpQixDQUFqQixDQUFiO0FBQ0EsT0FBSStCLFFBQVEsQ0FBWixDQUplLENBSUE7O0FBSkE7QUFBQTtBQUFBOztBQUFBO0FBTWYseUJBQW1CRixPQUFuQiw4SEFBNEI7QUFBQSxTQUFuQkcsTUFBbUI7O0FBQzNCLFNBQUlBLE9BQU9aLElBQVAsS0FBZ0JBLElBQXBCLEVBQTBCO0FBQ3pCVyxjQUFRRixRQUFRSSxPQUFSLENBQWdCRCxNQUFoQixDQUFSO0FBQ0E7QUFDQTtBQUNEO0FBWGM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFhZjNDLFNBQU1vQyxTQUFOLEdBQWtCUyxLQUFsQixDQUF3QixDQUFDSCxLQUFELEVBQVFaLFNBQVIsQ0FBeEI7QUFDQTs7QUFFRCxNQUFJakIsTUFBTWMsTUFBVixFQUFrQjtBQUNqQjtBQUNBLFFBQUssSUFBSWdCLE9BQVQsSUFBbUI5QixNQUFNYyxNQUF6QixFQUFpQztBQUNoQyxRQUFJbUIsUUFBUWpDLE1BQU1jLE1BQU4sQ0FBYWdCLE9BQWIsQ0FBWjs7QUFFQSxRQUFJRyxNQUFNQyxXQUFOLEtBQXNCQyxLQUExQixFQUFpQztBQUNoQ0YsYUFBUUEsTUFBTUcsSUFBTixDQUFXLElBQVgsQ0FBUixDQURnQyxDQUNOO0FBQzFCOztBQUVEakQsVUFBTW9DLFNBQU4sR0FBa0JPLE1BQWxCLENBQTRCQSxPQUE1QixZQUEyQ2pDLE1BQTNDLENBQWtEb0MsS0FBbEQ7QUFDQTtBQUNEOztBQUVEOUMsUUFBTW9DLFNBQU4sR0FBa0JjLElBQWxCLENBQXVCLEtBQXZCO0FBRUE7O0FBRUQ7QUFDQTtBQUNBOztBQUVBdEQsUUFBTzJDLElBQVAsR0FBYyxVQUFTWSxJQUFULEVBQWU7QUFDNUIsTUFBSWpELGNBQUosRUFBb0I7QUFDbkJGLFNBQ0VvRCxFQURGLENBQ0sseUNBREwsRUFDZ0RoQyxhQURoRCxFQUVFZ0MsRUFGRixDQUVLLDJDQUZMLEVBRWtENUIsZUFGbEQsRUFHRTRCLEVBSEYsQ0FHSyxpQ0FITCxFQUd3Q3hCLGFBSHhDLEVBSUV3QixFQUpGLENBSUssMEJBSkwsRUFJaUMxQixlQUpqQzs7QUFNQXpCLEtBQUVPLE1BQUYsRUFDRTRDLEVBREYsQ0FDSyxVQURMLEVBQ2lCcEIsaUJBRGpCO0FBRUE7O0FBRURtQjtBQUNBLEVBYkQ7O0FBZUEsUUFBT3ZELE1BQVA7QUFFQSxDQWxNRiIsImZpbGUiOiJxdWlja19lZGl0L292ZXJ2aWV3L3N0YXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzdGF0ZS5qcyAyMDE2LTEwLTE5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBIYW5kbGVzIHRoZSB0YWJsZSBzdGF0ZSBmb3IgZmlsdGVyaW5nLCBwYWdpbmF0aW9uIGFuZCBzb3J0aW5nLlxuICpcbiAqIFRoaXMgY29udHJvbGxlciB3aWxsIHVwZGF0ZSB0aGUgd2luZG93IGhpc3Rvcnkgd2l0aCB0aGUgY3VycmVudCBzdGF0ZSBvZiB0aGUgdGFibGUuIEl0IHJlYWN0c1xuICogdG8gc3BlY2lmaWMgZXZlbnRzIHN1Y2ggYXMgZmlsdGVyaW5nLCBwYWdpbmF0aW9uIGFuZCBzb3J0aW5nIGNoYW5nZXMuIEFmdGVyIHRoZSB3aW5kb3cgaGlzdG9yeVxuICogaXMgdXBkYXRlZCB0aGUgdXNlciB3aWxsIGJlIGFibGUgdG8gbmF2aWdhdGUgZm9ydGggb3IgYmFja3dhcmRzLlxuICpcbiAqIE5vdGljZSAjMTogVGhpcyBtb2R1bGUgbXVzdCBoYW5kbGUgdGhlIHdpbmRvdydzIHBvcC1zdGF0ZSBldmVudHMgYW5kIG5vdCBvdGhlciBtb2R1bGVzIGJlY2F1c2VcbiAqIHRoaXMgd2lsbCBsZWFkIHRvIHVubmVjZXNzYXJ5IGNvZGUgZHVwbGljYXRpb24gYW5kIG11bHRpcGxlIEFKQVggcmVxdWVzdHMuXG4gKlxuICogTm90aWNlICMyOiBUaGUgd2luZG93IHN0YXRlIG11c3QgYmUgYWx3YXlzIGluIHN5bmMgd2l0aCB0aGUgVVJMIGZvciBlYXNpZXIgbWFuaXB1bGF0aW9uLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdzdGF0ZScsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS1kZXBhcmFtL2pxdWVyeS1kZXBhcmFtLm1pbi5qc2Bcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogV2luZG93IEhpc3RvcnkgU3VwcG9ydFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge0Jvb2xlYW59XG5cdFx0ICovXG5cdFx0Y29uc3QgaGlzdG9yeVN1cHBvcnQgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdoaXN0b3J5Jyk7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IHBhcnNlZCBzdGF0ZSBmcm9tIHRoZSBVUkwgR0VUIHBhcmFtZXRlcnMuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPYmplY3R9IFJldHVybnMgdGhlIHRhYmxlIHN0YXRlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRTdGF0ZSgpIHtcblx0XHRcdHJldHVybiAkLmRlcGFyYW0od2luZG93LmxvY2F0aW9uLnNlYXJjaC5zbGljZSgxKSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNldCB0aGUgc3RhdGUgdG8gdGhlIGJyb3dzZXIncyBoaXN0b3J5LlxuXHRcdCAqXG5cdFx0ICogVGhlIHN0YXRlIGlzIHN0b3JlZCBmb3IgZW5hYmxpbmcgYmFjayBhbmQgZm9ydGggbmF2aWdhdGlvbiBmcm9tIHRoZSBicm93c2VyLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHN0YXRlIENvbnRhaW5zIHRoZSBuZXcgdGFibGUgc3RhdGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3NldFN0YXRlKHN0YXRlKSB7XG5cdFx0XHRjb25zdCB1cmwgPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luICsgd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lICsgJz8nICsgJC5wYXJhbShzdGF0ZSk7XG5cdFx0XHR3aW5kb3cuaGlzdG9yeS5wdXNoU3RhdGUoc3RhdGUsICcnLCB1cmwpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBVcGRhdGUgcGFnZSBuYXZpZ2F0aW9uIHN0YXRlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBhZ2luYXRpb24gQ29udGFpbnMgdGhlIERhdGFUYWJsZSBwYWdpbmF0aW9uIGluZm8uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uUGFnZUNoYW5nZShldmVudCwgcGFnaW5hdGlvbikge1xuXHRcdFx0Y29uc3Qgc3RhdGUgPSBfZ2V0U3RhdGUoKTtcblx0XHRcdFxuXHRcdFx0c3RhdGUucGFnZSA9IHBhZ2luYXRpb24ucGFnZSArIDE7XG5cdFx0XHRcblx0XHRcdF9zZXRTdGF0ZShzdGF0ZSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZSBwYWdlIGxlbmd0aCBzdGF0ZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0LlxuXHRcdCAqIEBwYXJhbSB7TnVtYmVyfSBsZW5ndGggTmV3IHBhZ2UgbGVuZ3RoLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkxlbmd0aENoYW5nZShldmVudCwgbGVuZ3RoKSB7XG5cdFx0XHRjb25zdCBzdGF0ZSA9IF9nZXRTdGF0ZSgpO1xuXHRcdFx0XG5cdFx0XHRzdGF0ZS5wYWdlID0gMTtcblx0XHRcdHN0YXRlLmxlbmd0aCA9IGxlbmd0aDtcblx0XHRcdFxuXHRcdFx0X3NldFN0YXRlKHN0YXRlKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVXBkYXRlIGZpbHRlciBzdGF0ZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0LlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBmaWx0ZXIgQ29udGFpbnMgdGhlIGZpbHRlcmluZyB2YWx1ZXMuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uRmlsdGVyQ2hhbmdlKGV2ZW50LCBmaWx0ZXIpIHtcblx0XHRcdGNvbnN0IHN0YXRlID0gX2dldFN0YXRlKCk7XG5cdFx0XHRcblx0XHRcdHN0YXRlLnBhZ2UgPSAxO1xuXHRcdFx0c3RhdGUuZmlsdGVyID0gZmlsdGVyO1xuXHRcdFx0XG5cdFx0XHRfc2V0U3RhdGUoc3RhdGUpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBVcGRhdGUgc29ydCBzdGF0ZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0LlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBzb3J0IENvbnRhaW5zIGNvbHVtbiBzb3J0aW5nIGluZm8ge2luZGV4LCBuYW1lLCBkaXJlY3Rpb259LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblNvcnRDaGFuZ2UoZXZlbnQsIHNvcnQpIHtcblx0XHRcdGNvbnN0IHN0YXRlID0gX2dldFN0YXRlKCk7XG5cdFx0XHRcblx0XHRcdHN0YXRlLnNvcnQgPSAoc29ydC5kaXJlY3Rpb24gPT09ICdkZXNjJyA/ICctJyA6ICcrJykgKyBzb3J0Lm5hbWU7XG5cdFx0XHRcblx0XHRcdF9zZXRTdGF0ZShzdGF0ZSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNldCB0aGUgY29ycmVjdCB0YWJsZSBzdGF0ZS5cblx0XHQgKlxuXHRcdCAqIFRoaXMgbWV0aG9kIHdpbGwgcGFyc2UgdGhlIG5ldyBwb3BwZWQgc3RhdGUgYW5kIGFwcGx5IGl0IG9uIHRoZSB0YWJsZS4gSXQgbXVzdCBiZSB0aGUgb25seSBwbGFjZSB3aGVyZSB0aGlzXG5cdFx0ICogaGFwcGVucyBpbiBvcmRlciB0byBhdm9pZCBtdWx0aXBsZSBBSkFYIHJlcXVlc3RzIGFuZCBkYXRhIGNvbGxpc2lvbnMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25XaW5kb3dQb3BTdGF0ZShldmVudCkge1xuXHRcdFx0Y29uc3Qgc3RhdGUgPSBldmVudC5vcmlnaW5hbEV2ZW50LnN0YXRlIHx8IHt9O1xuXHRcdFx0XG5cdFx0XHRpZiAoc3RhdGUucGFnZSkge1xuXHRcdFx0XHQkdGhpcy5maW5kKCcucGFnZS1uYXZpZ2F0aW9uIHNlbGVjdCcpLnZhbChzdGF0ZS5wYWdlKTtcblx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkucGFnZShwYXJzZUludChzdGF0ZS5wYWdlKSAtIDEpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAoc3RhdGUubGVuZ3RoKSB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5wYWdlLWxlbmd0aCBzZWxlY3QnKS52YWwoc3RhdGUubGVuZ3RoKTtcblx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkucGFnZS5sZW4ocGFyc2VJbnQoc3RhdGUubGVuZ3RoKSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmIChzdGF0ZS5zb3J0KSB7XG5cdFx0XHRcdGNvbnN0IHtjb2x1bW5zfSA9ICR0aGlzLkRhdGFUYWJsZSgpLmluaXQoKTtcblx0XHRcdFx0Y29uc3QgZGlyZWN0aW9uID0gc3RhdGUuc29ydC5jaGFyQXQoMCkgPT09ICctJyA/ICdkZXNjJyA6ICdhc2MnO1xuXHRcdFx0XHRjb25zdCBuYW1lID0gc3RhdGUuc29ydC5zbGljZSgxKTtcblx0XHRcdFx0bGV0IGluZGV4ID0gMTsgLy8gRGVmYXVsdCBWYWx1ZVxuXHRcdFx0XHRcblx0XHRcdFx0Zm9yIChsZXQgY29sdW1uIG9mIGNvbHVtbnMpIHtcblx0XHRcdFx0XHRpZiAoY29sdW1uLm5hbWUgPT09IG5hbWUpIHtcblx0XHRcdFx0XHRcdGluZGV4ID0gY29sdW1ucy5pbmRleE9mKGNvbHVtbik7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLm9yZGVyKFtpbmRleCwgZGlyZWN0aW9uXSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmIChzdGF0ZS5maWx0ZXIpIHtcblx0XHRcdFx0Ly8gVXBkYXRlIHRoZSBmaWx0ZXJpbmcgaW5wdXQgZWxlbWVudHMuIFxuXHRcdFx0XHRmb3IgKGxldCBjb2x1bW4gaW4gc3RhdGUuZmlsdGVyKSB7XG5cdFx0XHRcdFx0bGV0IHZhbHVlID0gc3RhdGUuZmlsdGVyW2NvbHVtbl07XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKHZhbHVlLmNvbnN0cnVjdG9yID09PSBBcnJheSkge1xuXHRcdFx0XHRcdFx0dmFsdWUgPSB2YWx1ZS5qb2luKCd8fCcpOyAvLyBKb2luIGFycmF5cyBpbnRvIGEgc2luZ2xlIHN0cmluZy5cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKGAke2NvbHVtbn06bmFtZWApLnNlYXJjaCh2YWx1ZSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuZHJhdyhmYWxzZSk7XG5cdFx0XHRcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdGlmIChoaXN0b3J5U3VwcG9ydCkge1xuXHRcdFx0XHQkdGhpc1xuXHRcdFx0XHRcdC5vbignZGF0YXRhYmxlX2N1c3RvbV9wYWdpbmF0aW9uOnBhZ2VfY2hhbmdlJywgX29uUGFnZUNoYW5nZSlcblx0XHRcdFx0XHQub24oJ2RhdGF0YWJsZV9jdXN0b21fcGFnaW5hdGlvbjpsZW5ndGhfY2hhbmdlJywgX29uTGVuZ3RoQ2hhbmdlKVxuXHRcdFx0XHRcdC5vbignZGF0YXRhYmxlX2N1c3RvbV9zb3J0aW5nOmNoYW5nZScsIF9vblNvcnRDaGFuZ2UpXG5cdFx0XHRcdFx0Lm9uKCdxdWlja19lZGl0X2ZpbHRlcjpjaGFuZ2UnLCBfb25GaWx0ZXJDaGFuZ2UpO1xuXHRcdFx0XHRcblx0XHRcdFx0JCh3aW5kb3cpXG5cdFx0XHRcdFx0Lm9uKCdwb3BzdGF0ZScsIF9vbldpbmRvd1BvcFN0YXRlKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0XHRcblx0fSk7Il19
