'use strict';

/* --------------------------------------------------------------
 delete.js 2016-10-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Delete Order Modal Controller
 */
gx.controllers.module('delete', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: {
			selectedOrders: $this.find('.selected-orders'),
			reStock: $this.find('.re-stock'),
			reShip: $this.find('.re-ship'),
			reActivate: $this.find('.re-activate')
		}
	};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Send the modal data to the form through an AJAX call.
  *
  * @param {jQuery.Event} event
  */
	function _onDeleteClick(event) {
		var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=OrdersModalsAjax/DeleteOrder';
		var data = {
			selectedOrders: module.bindings.selectedOrders.get().split(', '),
			reStock: module.bindings.reStock.get(),
			reShip: module.bindings.reShip.get(),
			reActivate: module.bindings.reActivate.get(),
			pageToken: jse.core.config.get('pageToken')
		};
		var $deleteButton = $(event.target);

		$deleteButton.addClass('disabled').prop('disabled', true);

		$.ajax({
			url: url,
			data: data,
			method: 'POST',
			dataType: 'json'
		}).done(function (response) {
			jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('DELETE_ORDERS_SUCCESS', 'admin_orders'));
			$('.orders .table-main').DataTable().ajax.reload(null, false);
			$('.orders .table-main').orders_overview_filter('reload');
		}).fail(function (jqxhr, textStatus, errorThrown) {
			jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'), jse.core.lang.translate('DELETE_ORDERS_ERROR', 'admin_orders'));
		}).always(function () {
			$this.modal('hide');
			$deleteButton.removeClass('disabled').prop('disabled', false);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.delete', _onDeleteClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvZGVsZXRlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiYmluZGluZ3MiLCJzZWxlY3RlZE9yZGVycyIsImZpbmQiLCJyZVN0b2NrIiwicmVTaGlwIiwicmVBY3RpdmF0ZSIsIl9vbkRlbGV0ZUNsaWNrIiwiZXZlbnQiLCJ1cmwiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0Iiwic3BsaXQiLCJwYWdlVG9rZW4iLCIkZGVsZXRlQnV0dG9uIiwidGFyZ2V0IiwiYWRkQ2xhc3MiLCJwcm9wIiwiYWpheCIsIm1ldGhvZCIsImRhdGFUeXBlIiwiZG9uZSIsInJlc3BvbnNlIiwibGlicyIsImluZm9fYm94IiwiYWRkU3VjY2Vzc01lc3NhZ2UiLCJsYW5nIiwidHJhbnNsYXRlIiwiRGF0YVRhYmxlIiwicmVsb2FkIiwib3JkZXJzX292ZXJ2aWV3X2ZpbHRlciIsImZhaWwiLCJqcXhociIsInRleHRTdGF0dXMiLCJlcnJvclRocm93biIsIm1vZGFsIiwic2hvd01lc3NhZ2UiLCJhbHdheXMiLCJyZW1vdmVDbGFzcyIsImluaXQiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7QUFHQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQXNCLFFBQXRCLEVBQWdDLENBQUMsT0FBRCxDQUFoQyxFQUEyQyxVQUFTQyxJQUFULEVBQWU7O0FBRXpEOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUgsU0FBUztBQUNkSSxZQUFVO0FBQ1RDLG1CQUFnQkgsTUFBTUksSUFBTixDQUFXLGtCQUFYLENBRFA7QUFFVEMsWUFBU0wsTUFBTUksSUFBTixDQUFXLFdBQVgsQ0FGQTtBQUdURSxXQUFRTixNQUFNSSxJQUFOLENBQVcsVUFBWCxDQUhDO0FBSVRHLGVBQVlQLE1BQU1JLElBQU4sQ0FBVyxjQUFYO0FBSkg7QUFESSxFQUFmOztBQVNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxVQUFTSSxjQUFULENBQXdCQyxLQUF4QixFQUErQjtBQUM5QixNQUFNQyxNQUFNQyxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLGtEQUE1QztBQUNBLE1BQU1mLE9BQU87QUFDWkksbUJBQWdCTCxPQUFPSSxRQUFQLENBQWdCQyxjQUFoQixDQUErQlcsR0FBL0IsR0FBcUNDLEtBQXJDLENBQTJDLElBQTNDLENBREo7QUFFWlYsWUFBU1AsT0FBT0ksUUFBUCxDQUFnQkcsT0FBaEIsQ0FBd0JTLEdBQXhCLEVBRkc7QUFHWlIsV0FBUVIsT0FBT0ksUUFBUCxDQUFnQkksTUFBaEIsQ0FBdUJRLEdBQXZCLEVBSEk7QUFJWlAsZUFBWVQsT0FBT0ksUUFBUCxDQUFnQkssVUFBaEIsQ0FBMkJPLEdBQTNCLEVBSkE7QUFLWkUsY0FBV0wsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUxDLEdBQWI7QUFPQSxNQUFNRyxnQkFBZ0JoQixFQUFFUSxNQUFNUyxNQUFSLENBQXRCOztBQUVBRCxnQkFBY0UsUUFBZCxDQUF1QixVQUF2QixFQUFtQ0MsSUFBbkMsQ0FBd0MsVUFBeEMsRUFBb0QsSUFBcEQ7O0FBRUFuQixJQUFFb0IsSUFBRixDQUFPO0FBQ05YLFdBRE07QUFFTlgsYUFGTTtBQUdOdUIsV0FBUSxNQUhGO0FBSU5DLGFBQVU7QUFKSixHQUFQLEVBTUVDLElBTkYsQ0FNTyxVQUFTQyxRQUFULEVBQW1CO0FBQ3hCZCxPQUFJZSxJQUFKLENBQVNDLFFBQVQsQ0FBa0JDLGlCQUFsQixDQUNDakIsSUFBSUMsSUFBSixDQUFTaUIsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHVCQUF4QixFQUFpRCxjQUFqRCxDQUREO0FBRUE3QixLQUFFLHFCQUFGLEVBQXlCOEIsU0FBekIsR0FBcUNWLElBQXJDLENBQTBDVyxNQUExQyxDQUFpRCxJQUFqRCxFQUF1RCxLQUF2RDtBQUNBL0IsS0FBRSxxQkFBRixFQUF5QmdDLHNCQUF6QixDQUFnRCxRQUFoRDtBQUNBLEdBWEYsRUFZRUMsSUFaRixDQVlPLFVBQVNDLEtBQVQsRUFBZ0JDLFVBQWhCLEVBQTRCQyxXQUE1QixFQUF5QztBQUM5QzFCLE9BQUllLElBQUosQ0FBU1ksS0FBVCxDQUFlQyxXQUFmLENBQTJCNUIsSUFBSUMsSUFBSixDQUFTaUIsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBQTNCLEVBQ0NuQixJQUFJQyxJQUFKLENBQVNpQixJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLGNBQS9DLENBREQ7QUFFQSxHQWZGLEVBZ0JFVSxNQWhCRixDQWdCUyxZQUFXO0FBQ2xCeEMsU0FBTXNDLEtBQU4sQ0FBWSxNQUFaO0FBQ0FyQixpQkFBY3dCLFdBQWQsQ0FBMEIsVUFBMUIsRUFBc0NyQixJQUF0QyxDQUEyQyxVQUEzQyxFQUF1RCxLQUF2RDtBQUNBLEdBbkJGO0FBb0JBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQXRCLFFBQU80QyxJQUFQLEdBQWMsVUFBU2xCLElBQVQsRUFBZTtBQUM1QnhCLFFBQU0yQyxFQUFOLENBQVMsT0FBVCxFQUFrQixhQUFsQixFQUFpQ25DLGNBQWpDO0FBQ0FnQjtBQUNBLEVBSEQ7O0FBS0EsUUFBTzFCLE1BQVA7QUFDQSxDQW5GRCIsImZpbGUiOiJvcmRlcnMvbW9kYWxzL2RlbGV0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBkZWxldGUuanMgMjAxNi0xMC0xN1xyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBEZWxldGUgT3JkZXIgTW9kYWwgQ29udHJvbGxlclxyXG4gKi9cclxuZ3guY29udHJvbGxlcnMubW9kdWxlKCdkZWxldGUnLCBbJ21vZGFsJ10sIGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gVkFSSUFCTEVTXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdCAqL1xyXG5cdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0ICovXHJcblx0Y29uc3QgbW9kdWxlID0ge1xyXG5cdFx0YmluZGluZ3M6IHtcclxuXHRcdFx0c2VsZWN0ZWRPcmRlcnM6ICR0aGlzLmZpbmQoJy5zZWxlY3RlZC1vcmRlcnMnKSxcclxuXHRcdFx0cmVTdG9jazogJHRoaXMuZmluZCgnLnJlLXN0b2NrJyksXHJcblx0XHRcdHJlU2hpcDogJHRoaXMuZmluZCgnLnJlLXNoaXAnKSxcclxuXHRcdFx0cmVBY3RpdmF0ZTogJHRoaXMuZmluZCgnLnJlLWFjdGl2YXRlJylcclxuXHRcdH1cclxuXHR9O1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIEZVTkNUSU9OU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNlbmQgdGhlIG1vZGFsIGRhdGEgdG8gdGhlIGZvcm0gdGhyb3VnaCBhbiBBSkFYIGNhbGwuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25EZWxldGVDbGljayhldmVudCkge1xyXG5cdFx0Y29uc3QgdXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1PcmRlcnNNb2RhbHNBamF4L0RlbGV0ZU9yZGVyJztcclxuXHRcdGNvbnN0IGRhdGEgPSB7XHJcblx0XHRcdHNlbGVjdGVkT3JkZXJzOiBtb2R1bGUuYmluZGluZ3Muc2VsZWN0ZWRPcmRlcnMuZ2V0KCkuc3BsaXQoJywgJyksXHJcblx0XHRcdHJlU3RvY2s6IG1vZHVsZS5iaW5kaW5ncy5yZVN0b2NrLmdldCgpLFxyXG5cdFx0XHRyZVNoaXA6IG1vZHVsZS5iaW5kaW5ncy5yZVNoaXAuZ2V0KCksXHJcblx0XHRcdHJlQWN0aXZhdGU6IG1vZHVsZS5iaW5kaW5ncy5yZUFjdGl2YXRlLmdldCgpLFxyXG5cdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpXHJcblx0XHR9O1xyXG5cdFx0Y29uc3QgJGRlbGV0ZUJ1dHRvbiA9ICQoZXZlbnQudGFyZ2V0KTtcclxuXHRcdFxyXG5cdFx0JGRlbGV0ZUJ1dHRvbi5hZGRDbGFzcygnZGlzYWJsZWQnKS5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xyXG5cdFx0XHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR1cmwsXHJcblx0XHRcdGRhdGEsXHJcblx0XHRcdG1ldGhvZDogJ1BPU1QnLFxyXG5cdFx0XHRkYXRhVHlwZTogJ2pzb24nXHJcblx0XHR9KVxyXG5cdFx0XHQuZG9uZShmdW5jdGlvbihyZXNwb25zZSkge1xyXG5cdFx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0RFTEVURV9PUkRFUlNfU1VDQ0VTUycsICdhZG1pbl9vcmRlcnMnKSk7XHJcblx0XHRcdFx0JCgnLm9yZGVycyAudGFibGUtbWFpbicpLkRhdGFUYWJsZSgpLmFqYXgucmVsb2FkKG51bGwsIGZhbHNlKTtcclxuXHRcdFx0XHQkKCcub3JkZXJzIC50YWJsZS1tYWluJykub3JkZXJzX292ZXJ2aWV3X2ZpbHRlcigncmVsb2FkJyk7XHJcblx0XHRcdH0pXHJcblx0XHRcdC5mYWlsKGZ1bmN0aW9uKGpxeGhyLCB0ZXh0U3RhdHVzLCBlcnJvclRocm93bikge1xyXG5cdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0RFTEVURV9PUkRFUlNfRVJST1InLCAnYWRtaW5fb3JkZXJzJykpO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQuYWx3YXlzKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdCR0aGlzLm1vZGFsKCdoaWRlJyk7XHJcblx0XHRcdFx0JGRlbGV0ZUJ1dHRvbi5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcclxuXHRcdFx0fSk7XHJcblx0fVxyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHQkdGhpcy5vbignY2xpY2snLCAnLmJ0bi5kZWxldGUnLCBfb25EZWxldGVDbGljayk7XHJcblx0XHRkb25lKCk7XHJcblx0fTtcclxuXHRcclxuXHRyZXR1cm4gbW9kdWxlO1xyXG59KTsiXX0=
