'use strict';

/* --------------------------------------------------------------
 content_manager_preview.js 2017-09-07
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Content Manager Preview
 *
 * Controller Module to open preview modal
 *
 * Opens a modal with the preview of the selected Content Manager entry.
 */
gx.controllers.module('content_manager_preview', ['modal'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Preview modal.
  *
  * @type {jQuery}
  */
	var $modal = $('.preview.modal');

	/**
  * Open preview icon selector string.
  *
  * @type {String}
  */
	var openPreviewSelector = '.open-preview';

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	function _onPreviewClick(event) {
		event.preventDefault();

		// Iframe that should show the preview
		var $iframe = $modal.find('.modal-body iframe');

		// Id of the content that should be shown in preview modal
		var content_id = $(event.target).parents(openPreviewSelector).data('contentId');

		// Url of link element that should be shown in preview modal
		var link_url = $(event.target).parents(openPreviewSelector).attr('href');

		// Check if a link URL is set. If not use the content id to display a preview
		if (link_url !== '#' && link_url.indexOf('http') >= 0) {
			$iframe.css('display', 'none');
			$modal.find('.modal-body').append(_generateContentInfoMarkup(link_url));
		} else if (link_url !== '#' && link_url.indexOf('http') < 0) {
			$iframe.css('display', 'block');
			$iframe.attr('src', jse.core.config.get('appUrl') + '/' + link_url);
		} else {
			$iframe.css('display', 'block');
			$iframe.attr('src', jse.core.config.get('appUrl') + '/admin/content_preview.php?coID=' + content_id);
		}

		// Display the preview modal
		$modal.modal('show');
	}

	/**
  * Generates HTML containing the external link information.
  *
  * @param {String} data Link.
  *
  * @return {String} Created HTML string.
  */
	function _generateContentInfoMarkup(data) {
		// Label phrases.
		var externalLinkInfo = jse.core.lang.translate('TEXT_EXTERNAL_LINK_INFO', 'content_manager');

		// Return markup.
		return '\n\t\t\t\t\t<div class="col-md-12">\n\t\t\t\t\t\t' + data + ' ' + externalLinkInfo + '\n\t\t\t\t\t</div>\n\t\t\t';
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Listen to click event on preview icon
		$this.on('click', openPreviewSelector, _onPreviewClick);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRfbWFuYWdlci9vdmVydmlldy9jb250ZW50X21hbmFnZXJfcHJldmlldy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRtb2RhbCIsIm9wZW5QcmV2aWV3U2VsZWN0b3IiLCJfb25QcmV2aWV3Q2xpY2siLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiJGlmcmFtZSIsImZpbmQiLCJjb250ZW50X2lkIiwidGFyZ2V0IiwicGFyZW50cyIsImxpbmtfdXJsIiwiYXR0ciIsImluZGV4T2YiLCJjc3MiLCJhcHBlbmQiLCJfZ2VuZXJhdGVDb250ZW50SW5mb01hcmt1cCIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJtb2RhbCIsImV4dGVybmFsTGlua0luZm8iLCJsYW5nIiwidHJhbnNsYXRlIiwiaW5pdCIsIm9uIiwiZG9uZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLHlCQURELEVBR0MsQ0FDQyxPQURELENBSEQsRUFPQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxTQUFTRCxFQUFFLGdCQUFGLENBQWY7O0FBRUE7Ozs7O0FBS0EsS0FBTUUsc0JBQXNCLGVBQTVCOztBQUdBOzs7OztBQUtBLEtBQU1MLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsVUFBU00sZUFBVCxDQUF5QkMsS0FBekIsRUFBZ0M7QUFDL0JBLFFBQU1DLGNBQU47O0FBRUE7QUFDQSxNQUFNQyxVQUFVTCxPQUFPTSxJQUFQLENBQVksb0JBQVosQ0FBaEI7O0FBRUE7QUFDQSxNQUFNQyxhQUFhUixFQUFFSSxNQUFNSyxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QlIsbUJBQXhCLEVBQTZDSixJQUE3QyxDQUFrRCxXQUFsRCxDQUFuQjs7QUFFQTtBQUNBLE1BQU1hLFdBQVdYLEVBQUVJLE1BQU1LLE1BQVIsRUFBZ0JDLE9BQWhCLENBQXdCUixtQkFBeEIsRUFBNkNVLElBQTdDLENBQWtELE1BQWxELENBQWpCOztBQUVBO0FBQ0EsTUFBSUQsYUFBYSxHQUFiLElBQW9CQSxTQUFTRSxPQUFULENBQWlCLE1BQWpCLEtBQTRCLENBQXBELEVBQXVEO0FBQ3REUCxXQUFRUSxHQUFSLENBQVksU0FBWixFQUF1QixNQUF2QjtBQUNBYixVQUFPTSxJQUFQLENBQVksYUFBWixFQUEyQlEsTUFBM0IsQ0FBa0NDLDJCQUEyQkwsUUFBM0IsQ0FBbEM7QUFDQSxHQUhELE1BR08sSUFBSUEsYUFBYSxHQUFiLElBQW9CQSxTQUFTRSxPQUFULENBQWlCLE1BQWpCLElBQTJCLENBQW5ELEVBQXNEO0FBQzVEUCxXQUFRUSxHQUFSLENBQVksU0FBWixFQUF1QixPQUF2QjtBQUNBUixXQUFRTSxJQUFSLENBQWEsS0FBYixFQUF1QkssSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixDQUFILFNBQXNDVCxRQUExRDtBQUNBLEdBSE0sTUFHQTtBQUNOTCxXQUFRUSxHQUFSLENBQVksU0FBWixFQUF1QixPQUF2QjtBQUNBUixXQUFRTSxJQUFSLENBQWEsS0FBYixFQUF1QkssSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixDQUFILHdDQUFxRVosVUFBekY7QUFDQTs7QUFFRDtBQUNBUCxTQUFPb0IsS0FBUCxDQUFhLE1BQWI7QUFDQTs7QUFHRDs7Ozs7OztBQU9BLFVBQVNMLDBCQUFULENBQW9DbEIsSUFBcEMsRUFBMEM7QUFDekM7QUFDQSxNQUFNd0IsbUJBQW1CTCxJQUFJQyxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix5QkFBeEIsRUFBbUQsaUJBQW5ELENBQXpCOztBQUVBO0FBQ0EsK0RBRUsxQixJQUZMLFNBRWF3QixnQkFGYjtBQUtBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQXpCLFFBQU80QixJQUFQLEdBQWMsZ0JBQVE7QUFDckI7QUFDQTFCLFFBQU0yQixFQUFOLENBQVMsT0FBVCxFQUFrQnhCLG1CQUFsQixFQUF1Q0MsZUFBdkM7O0FBRUE7QUFDQXdCO0FBQ0EsRUFORDs7QUFRQSxRQUFPOUIsTUFBUDtBQUNBLENBN0dGIiwiZmlsZSI6ImNvbnRlbnRfbWFuYWdlci9vdmVydmlldy9jb250ZW50X21hbmFnZXJfcHJldmlldy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gY29udGVudF9tYW5hZ2VyX3ByZXZpZXcuanMgMjAxNy0wOS0wN1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogQ29udGVudCBNYW5hZ2VyIFByZXZpZXdcbiAqXG4gKiBDb250cm9sbGVyIE1vZHVsZSB0byBvcGVuIHByZXZpZXcgbW9kYWxcbiAqXG4gKiBPcGVucyBhIG1vZGFsIHdpdGggdGhlIHByZXZpZXcgb2YgdGhlIHNlbGVjdGVkIENvbnRlbnQgTWFuYWdlciBlbnRyeS5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnY29udGVudF9tYW5hZ2VyX3ByZXZpZXcnLFxuXHRcblx0W1xuXHRcdCdtb2RhbCdcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUHJldmlldyBtb2RhbC5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJG1vZGFsID0gJCgnLnByZXZpZXcubW9kYWwnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBPcGVuIHByZXZpZXcgaWNvbiBzZWxlY3RvciBzdHJpbmcuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wZW5QcmV2aWV3U2VsZWN0b3IgPSAnLm9wZW4tcHJldmlldyc7XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRmdW5jdGlvbiBfb25QcmV2aWV3Q2xpY2soZXZlbnQpIHtcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdC8vIElmcmFtZSB0aGF0IHNob3VsZCBzaG93IHRoZSBwcmV2aWV3XG5cdFx0XHRjb25zdCAkaWZyYW1lID0gJG1vZGFsLmZpbmQoJy5tb2RhbC1ib2R5IGlmcmFtZScpO1xuXHRcdFx0XG5cdFx0XHQvLyBJZCBvZiB0aGUgY29udGVudCB0aGF0IHNob3VsZCBiZSBzaG93biBpbiBwcmV2aWV3IG1vZGFsXG5cdFx0XHRjb25zdCBjb250ZW50X2lkID0gJChldmVudC50YXJnZXQpLnBhcmVudHMob3BlblByZXZpZXdTZWxlY3RvcikuZGF0YSgnY29udGVudElkJyk7XG5cdFx0XHRcblx0XHRcdC8vIFVybCBvZiBsaW5rIGVsZW1lbnQgdGhhdCBzaG91bGQgYmUgc2hvd24gaW4gcHJldmlldyBtb2RhbFxuXHRcdFx0Y29uc3QgbGlua191cmwgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cyhvcGVuUHJldmlld1NlbGVjdG9yKS5hdHRyKCdocmVmJyk7XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrIGlmIGEgbGluayBVUkwgaXMgc2V0LiBJZiBub3QgdXNlIHRoZSBjb250ZW50IGlkIHRvIGRpc3BsYXkgYSBwcmV2aWV3XG5cdFx0XHRpZiAobGlua191cmwgIT09ICcjJyAmJiBsaW5rX3VybC5pbmRleE9mKCdodHRwJykgPj0gMCkge1xuXHRcdFx0XHQkaWZyYW1lLmNzcygnZGlzcGxheScsICdub25lJyk7XG5cdFx0XHRcdCRtb2RhbC5maW5kKCcubW9kYWwtYm9keScpLmFwcGVuZChfZ2VuZXJhdGVDb250ZW50SW5mb01hcmt1cChsaW5rX3VybCkpO1xuXHRcdFx0fSBlbHNlIGlmIChsaW5rX3VybCAhPT0gJyMnICYmIGxpbmtfdXJsLmluZGV4T2YoJ2h0dHAnKSA8IDApIHtcblx0XHRcdFx0JGlmcmFtZS5jc3MoJ2Rpc3BsYXknLCAnYmxvY2snKTtcblx0XHRcdFx0JGlmcmFtZS5hdHRyKCdzcmMnLCBgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYCArIGxpbmtfdXJsKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCRpZnJhbWUuY3NzKCdkaXNwbGF5JywgJ2Jsb2NrJyk7XG5cdFx0XHRcdCRpZnJhbWUuYXR0cignc3JjJywgYCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2FkbWluL2NvbnRlbnRfcHJldmlldy5waHA/Y29JRD1gICsgY29udGVudF9pZCk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIERpc3BsYXkgdGhlIHByZXZpZXcgbW9kYWxcblx0XHRcdCRtb2RhbC5tb2RhbCgnc2hvdycpO1xuXHRcdH1cblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBHZW5lcmF0ZXMgSFRNTCBjb250YWluaW5nIHRoZSBleHRlcm5hbCBsaW5rIGluZm9ybWF0aW9uLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGRhdGEgTGluay5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge1N0cmluZ30gQ3JlYXRlZCBIVE1MIHN0cmluZy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2VuZXJhdGVDb250ZW50SW5mb01hcmt1cChkYXRhKSB7XG5cdFx0XHQvLyBMYWJlbCBwaHJhc2VzLlxuXHRcdFx0Y29uc3QgZXh0ZXJuYWxMaW5rSW5mbyA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdURVhUX0VYVEVSTkFMX0xJTktfSU5GTycsICdjb250ZW50X21hbmFnZXInKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmV0dXJuIG1hcmt1cC5cblx0XHRcdHJldHVybiBgXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxuXHRcdFx0XHRcdFx0JHtkYXRhfSAke2V4dGVybmFsTGlua0luZm99XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRgO1xuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGRvbmUgPT4ge1xuXHRcdFx0Ly8gTGlzdGVuIHRvIGNsaWNrIGV2ZW50IG9uIHByZXZpZXcgaWNvblxuXHRcdFx0JHRoaXMub24oJ2NsaWNrJywgb3BlblByZXZpZXdTZWxlY3RvciwgX29uUHJldmlld0NsaWNrKTtcblx0XHRcdFxuXHRcdFx0Ly8gRmluaXNoIGluaXRpYWxpemF0aW9uLlxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKTtcbiJdfQ==
