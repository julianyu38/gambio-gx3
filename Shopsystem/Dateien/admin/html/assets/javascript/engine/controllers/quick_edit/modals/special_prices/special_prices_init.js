'use strict';

/* --------------------------------------------------------------
 special_price_init 2016-12-14
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Special Prices Table Initializations Controller
 *
 * This controller initializes the main QuickEdit special prices table with a new jQuery DataTables instance.
 */
gx.controllers.module('special_prices_init', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', 'datatable', gx.source + '/libs/quick_edit_special_prices_overview_columns'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Selected Product IDs
  *
  * @type {Number[]}
  */
	var productIds = [0];

	/**
  * Properties Modal Selector
  *
  * @type {jQuery}
  */
	var $modal = $this.parents('.modal');

	/**
  * DataTable Columns
  *
  * @type {Object[]}
  */
	var columns = jse.libs.datatable.prepareColumns($this, jse.libs.quick_edit_special_prices_overview_columns, data.specialPriceActiveColumns);

	/**
  * DataTable Options
  *
  * @type {Object}
  */
	var options = {
		autoWidth: false,
		dom: 't',
		pageLength: data.pageLength,
		displayStart: 0,
		serverSide: true,
		language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
		ajax: {
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditSpecialPricesAjax/DataTable',
			type: 'POST',
			data: function data(_data) {
				_data.productId = _getProductIds();
				_data.pageToken = jse.core.config.get('pageToken');
				return _data;
			}
		},
		orderCellsTop: true,
		order: _getOrder(),
		columns: columns,
		createdRow: function createdRow(row, data, dataIndex) {
			if (data.specialPriceIsNewEntry === true) {
				$(row).addClass('special-new-entry');
			}
		}
	};

	/**
  * Loading Spinner 
  * 
  * @type {jQuery}
  */
	var $spinner = null;

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Get Initial Table Order
  *
  * @return {Object[]} Returns the ordered column definitions.
  */
	function _getOrder() {
		var index = 1; // Order by first column by default.
		var direction = 'asc'; // Order ASC by default.
		var columnName = 'productsName'; // Order by products name by default.

		$this.on('click', 'th', function () {
			columnName = $(this).data('column-name');
			index = $this.DataTable().column(this).index();
		});

		if (data.specialPriceActiveColumns.indexOf('productsName') > -1) {
			// Order by name if possible.
			index = data.specialPriceActiveColumns.indexOf('productsName');
		}

		return [[index, direction]];
	}

	/**
  * Set current product IDs.
  *
  * @param {jQuery.Event} event Contains event information.
  */
	function _setProductIds(event) {
		productIds = [];

		if ($(event.target).is('a.special-price')) {
			productIds.push($(this).parents('tr').data('id'));

			return;
		}

		var $singleCheckboxes = $this.parents('.quick-edit.overview').find('input:checkbox:checked.overview-row-selection');

		$singleCheckboxes.each(function () {
			productIds.push($(this).parents('tr').data('id'));
		});
	}

	/**
  * Get Product IDs.
  *
  * @return {Number[]} Returns the product IDs.
  */
	function _getProductIds() {
		return productIds;
	}

	/**
  * Special prices modal shown event handler.
  */
	function _onModalShown() {
		$this.parents('body').find('.comiseo-daterangepicker').css('z-index', 1070);
		$(window).trigger('resize');

		if (!$.fn.DataTable.isDataTable($this)) {
			jse.libs.datatable.create($this, options);

			return;
		}

		$this.DataTable().ajax.reload();
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		$modal.on('show.bs.modal', function () {
			$(this).find('.modal-dialog').css('z-index', 1060);
		}).on('shown.bs.modal', _onModalShown).on('hide.bs.modal', function () {
			$(this).find('.modal-dialog').css('z-index', 0);
		});

		$this.parents('.quick-edit.overview').on('click', 'a.special-price', _setProductIds).on('click', 'a.bulk-special-price-edit', _setProductIds);

		$this.on('draw.dt', function () {
			$this.find('thead input:checkbox').prop('checked', false).trigger('change', [false]);
			$this.find('tbody').attr('data-gx-widget', 'single_checkbox switcher');
			$this.find('tbody').attr('data-single_checkbox-selector', '.special-price-row-selection');
			$this.find('tbody').attr('data-switcher-selector', '.convert-to-switcher');

			gx.widgets.init($this);
		});

		$this.on('preXhr.dt', function () {
			return $spinner = jse.libs.loading_spinner.show($modal.find('.modal-content'), $modal.css('z-index'));
		});
		$this.on('xhr.dt', function () {
			return jse.libs.loading_spinner.hide($spinner);
		});

		jse.libs.datatable.create($this, options);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3NwZWNpYWxfcHJpY2VzL3NwZWNpYWxfcHJpY2VzX2luaXQuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJwcm9kdWN0SWRzIiwiJG1vZGFsIiwicGFyZW50cyIsImNvbHVtbnMiLCJsaWJzIiwiZGF0YXRhYmxlIiwicHJlcGFyZUNvbHVtbnMiLCJxdWlja19lZGl0X3NwZWNpYWxfcHJpY2VzX292ZXJ2aWV3X2NvbHVtbnMiLCJzcGVjaWFsUHJpY2VBY3RpdmVDb2x1bW5zIiwib3B0aW9ucyIsImF1dG9XaWR0aCIsImRvbSIsInBhZ2VMZW5ndGgiLCJkaXNwbGF5U3RhcnQiLCJzZXJ2ZXJTaWRlIiwibGFuZ3VhZ2UiLCJnZXRUcmFuc2xhdGlvbnMiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwiYWpheCIsInVybCIsInR5cGUiLCJwcm9kdWN0SWQiLCJfZ2V0UHJvZHVjdElkcyIsInBhZ2VUb2tlbiIsIm9yZGVyQ2VsbHNUb3AiLCJvcmRlciIsIl9nZXRPcmRlciIsImNyZWF0ZWRSb3ciLCJyb3ciLCJkYXRhSW5kZXgiLCJzcGVjaWFsUHJpY2VJc05ld0VudHJ5IiwiYWRkQ2xhc3MiLCIkc3Bpbm5lciIsImluZGV4IiwiZGlyZWN0aW9uIiwiY29sdW1uTmFtZSIsIm9uIiwiRGF0YVRhYmxlIiwiY29sdW1uIiwiaW5kZXhPZiIsIl9zZXRQcm9kdWN0SWRzIiwiZXZlbnQiLCJ0YXJnZXQiLCJpcyIsInB1c2giLCIkc2luZ2xlQ2hlY2tib3hlcyIsImZpbmQiLCJlYWNoIiwiX29uTW9kYWxTaG93biIsImNzcyIsIndpbmRvdyIsInRyaWdnZXIiLCJmbiIsImlzRGF0YVRhYmxlIiwiY3JlYXRlIiwicmVsb2FkIiwiaW5pdCIsImRvbmUiLCJwcm9wIiwiYXR0ciIsIndpZGdldHMiLCJsb2FkaW5nX3NwaW5uZXIiLCJzaG93IiwiaGlkZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7OztBQUtBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxxQkFERCxFQUdDLENBQ0lDLElBQUlDLE1BRFIsbURBRUlELElBQUlDLE1BRlIsa0RBR0MsV0FIRCxFQUlJSixHQUFHSSxNQUpQLHNEQUhELEVBVUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUwsU0FBUyxFQUFmOztBQUVBOzs7OztBQUtBLEtBQUlNLGFBQWEsQ0FBQyxDQUFELENBQWpCOztBQUVBOzs7OztBQUtBLEtBQU1DLFNBQVNILE1BQU1JLE9BQU4sQ0FBYyxRQUFkLENBQWY7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVVIsSUFBSVMsSUFBSixDQUFTQyxTQUFULENBQW1CQyxjQUFuQixDQUFrQ1IsS0FBbEMsRUFBeUNILElBQUlTLElBQUosQ0FBU0csMENBQWxELEVBQThGVixLQUFLVyx5QkFBbkcsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVTtBQUNmQyxhQUFXLEtBREk7QUFFZkMsT0FBSyxHQUZVO0FBR2ZDLGNBQVlmLEtBQUtlLFVBSEY7QUFJZkMsZ0JBQWMsQ0FKQztBQUtmQyxjQUFZLElBTEc7QUFNZkMsWUFBVXBCLElBQUlTLElBQUosQ0FBU0MsU0FBVCxDQUFtQlcsZUFBbkIsQ0FBbUNyQixJQUFJc0IsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixjQUFwQixDQUFuQyxDQU5LO0FBT2ZDLFFBQU07QUFDTEMsUUFBSzFCLElBQUlzQixJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLDBEQURoQztBQUVMRyxTQUFNLE1BRkQ7QUFHTHpCLFNBQU0sY0FBQ0EsS0FBRCxFQUFVO0FBQ2ZBLFVBQUswQixTQUFMLEdBQWlCQyxnQkFBakI7QUFDQTNCLFVBQUs0QixTQUFMLEdBQWlCOUIsSUFBSXNCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FBakI7QUFDQSxXQUFPdEIsS0FBUDtBQUNBO0FBUEksR0FQUztBQWdCZjZCLGlCQUFlLElBaEJBO0FBaUJmQyxTQUFPQyxXQWpCUTtBQWtCZnpCLFdBQVNBLE9BbEJNO0FBbUJmMEIsWUFuQmUsc0JBbUJKQyxHQW5CSSxFQW1CQ2pDLElBbkJELEVBbUJPa0MsU0FuQlAsRUFtQmtCO0FBQ2hDLE9BQUlsQyxLQUFLbUMsc0JBQUwsS0FBZ0MsSUFBcEMsRUFBMEM7QUFDekNqQyxNQUFFK0IsR0FBRixFQUFPRyxRQUFQLENBQWdCLG1CQUFoQjtBQUNBO0FBQ0Q7QUF2QmMsRUFBaEI7O0FBMEJBOzs7OztBQUtBLEtBQUlDLFdBQVcsSUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU04sU0FBVCxHQUFxQjtBQUNwQixNQUFJTyxRQUFRLENBQVosQ0FEb0IsQ0FDTDtBQUNmLE1BQUlDLFlBQVksS0FBaEIsQ0FGb0IsQ0FFRztBQUN2QixNQUFJQyxhQUFhLGNBQWpCLENBSG9CLENBR2E7O0FBRWpDdkMsUUFBTXdDLEVBQU4sQ0FBUyxPQUFULEVBQWtCLElBQWxCLEVBQXdCLFlBQVc7QUFDbENELGdCQUFhdEMsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxhQUFiLENBQWI7QUFDQXNDLFdBQVFyQyxNQUFNeUMsU0FBTixHQUFrQkMsTUFBbEIsQ0FBeUIsSUFBekIsRUFBK0JMLEtBQS9CLEVBQVI7QUFDQSxHQUhEOztBQUtBLE1BQUl0QyxLQUFLVyx5QkFBTCxDQUErQmlDLE9BQS9CLENBQXVDLGNBQXZDLElBQXlELENBQUMsQ0FBOUQsRUFBaUU7QUFBRTtBQUNsRU4sV0FBUXRDLEtBQUtXLHlCQUFMLENBQStCaUMsT0FBL0IsQ0FBdUMsY0FBdkMsQ0FBUjtBQUNBOztBQUVELFNBQU8sQ0FBQyxDQUFDTixLQUFELEVBQVFDLFNBQVIsQ0FBRCxDQUFQO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU00sY0FBVCxDQUF3QkMsS0FBeEIsRUFBK0I7QUFDOUIzQyxlQUFhLEVBQWI7O0FBRUEsTUFBSUQsRUFBRTRDLE1BQU1DLE1BQVIsRUFBZ0JDLEVBQWhCLENBQW1CLGlCQUFuQixDQUFKLEVBQTJDO0FBQzFDN0MsY0FBVzhDLElBQVgsQ0FBZ0IvQyxFQUFFLElBQUYsRUFBUUcsT0FBUixDQUFnQixJQUFoQixFQUFzQkwsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBaEI7O0FBRUE7QUFDQTs7QUFFRCxNQUFNa0Qsb0JBQW9CakQsTUFBTUksT0FBTixDQUFjLHNCQUFkLEVBQ3hCOEMsSUFEd0IsQ0FDbkIsK0NBRG1CLENBQTFCOztBQUdBRCxvQkFBa0JFLElBQWxCLENBQXVCLFlBQVc7QUFDakNqRCxjQUFXOEMsSUFBWCxDQUFnQi9DLEVBQUUsSUFBRixFQUFRRyxPQUFSLENBQWdCLElBQWhCLEVBQXNCTCxJQUF0QixDQUEyQixJQUEzQixDQUFoQjtBQUNBLEdBRkQ7QUFHQTs7QUFFRDs7Ozs7QUFLQSxVQUFTMkIsY0FBVCxHQUEwQjtBQUN6QixTQUFPeEIsVUFBUDtBQUNBOztBQUVEOzs7QUFHQSxVQUFTa0QsYUFBVCxHQUF5QjtBQUN4QnBELFFBQU1JLE9BQU4sQ0FBYyxNQUFkLEVBQXNCOEMsSUFBdEIsQ0FBMkIsMEJBQTNCLEVBQXVERyxHQUF2RCxDQUEyRCxTQUEzRCxFQUFzRSxJQUF0RTtBQUNBcEQsSUFBRXFELE1BQUYsRUFBVUMsT0FBVixDQUFrQixRQUFsQjs7QUFFQSxNQUFJLENBQUN0RCxFQUFFdUQsRUFBRixDQUFLZixTQUFMLENBQWVnQixXQUFmLENBQTJCekQsS0FBM0IsQ0FBTCxFQUF3QztBQUN2Q0gsT0FBSVMsSUFBSixDQUFTQyxTQUFULENBQW1CbUQsTUFBbkIsQ0FBMEIxRCxLQUExQixFQUFpQ1csT0FBakM7O0FBRUE7QUFDQTs7QUFFRFgsUUFBTXlDLFNBQU4sR0FBa0JuQixJQUFsQixDQUF1QnFDLE1BQXZCO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBL0QsUUFBT2dFLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUIxRCxTQUNFcUMsRUFERixDQUNLLGVBREwsRUFDc0IsWUFBVztBQUMvQnZDLEtBQUUsSUFBRixFQUFRaUQsSUFBUixDQUFhLGVBQWIsRUFBOEJHLEdBQTlCLENBQWtDLFNBQWxDLEVBQTZDLElBQTdDO0FBQ0EsR0FIRixFQUlFYixFQUpGLENBSUssZ0JBSkwsRUFJdUJZLGFBSnZCLEVBS0VaLEVBTEYsQ0FLSyxlQUxMLEVBS3NCLFlBQVc7QUFDL0J2QyxLQUFFLElBQUYsRUFBUWlELElBQVIsQ0FBYSxlQUFiLEVBQThCRyxHQUE5QixDQUFrQyxTQUFsQyxFQUE2QyxDQUE3QztBQUNBLEdBUEY7O0FBU0FyRCxRQUFNSSxPQUFOLENBQWMsc0JBQWQsRUFDRW9DLEVBREYsQ0FDSyxPQURMLEVBQ2MsaUJBRGQsRUFDaUNJLGNBRGpDLEVBRUVKLEVBRkYsQ0FFSyxPQUZMLEVBRWMsMkJBRmQsRUFFMkNJLGNBRjNDOztBQUlBNUMsUUFBTXdDLEVBQU4sQ0FBUyxTQUFULEVBQW9CLFlBQU07QUFDekJ4QyxTQUFNa0QsSUFBTixDQUFXLHNCQUFYLEVBQ0VZLElBREYsQ0FDTyxTQURQLEVBQ2tCLEtBRGxCLEVBRUVQLE9BRkYsQ0FFVSxRQUZWLEVBRW9CLENBQUMsS0FBRCxDQUZwQjtBQUdBdkQsU0FBTWtELElBQU4sQ0FBVyxPQUFYLEVBQW9CYSxJQUFwQixDQUF5QixnQkFBekIsRUFBMkMsMEJBQTNDO0FBQ0EvRCxTQUFNa0QsSUFBTixDQUFXLE9BQVgsRUFBb0JhLElBQXBCLENBQXlCLCtCQUF6QixFQUEwRCw4QkFBMUQ7QUFDQS9ELFNBQU1rRCxJQUFOLENBQVcsT0FBWCxFQUFvQmEsSUFBcEIsQ0FBeUIsd0JBQXpCLEVBQW1ELHNCQUFuRDs7QUFFQXJFLE1BQUdzRSxPQUFILENBQVdKLElBQVgsQ0FBZ0I1RCxLQUFoQjtBQUNBLEdBVEQ7O0FBV0FBLFFBQU13QyxFQUFOLENBQVMsV0FBVCxFQUFzQjtBQUFBLFVBQU1KLFdBQzNCdkMsSUFBSVMsSUFBSixDQUFTMkQsZUFBVCxDQUF5QkMsSUFBekIsQ0FBOEIvRCxPQUFPK0MsSUFBUCxDQUFZLGdCQUFaLENBQTlCLEVBQTZEL0MsT0FBT2tELEdBQVAsQ0FBVyxTQUFYLENBQTdELENBRHFCO0FBQUEsR0FBdEI7QUFFQXJELFFBQU13QyxFQUFOLENBQVMsUUFBVCxFQUFtQjtBQUFBLFVBQU0zQyxJQUFJUyxJQUFKLENBQVMyRCxlQUFULENBQXlCRSxJQUF6QixDQUE4Qi9CLFFBQTlCLENBQU47QUFBQSxHQUFuQjs7QUFFQXZDLE1BQUlTLElBQUosQ0FBU0MsU0FBVCxDQUFtQm1ELE1BQW5CLENBQTBCMUQsS0FBMUIsRUFBaUNXLE9BQWpDOztBQUVBa0Q7QUFDQSxFQWhDRDs7QUFrQ0EsUUFBT2pFLE1BQVA7QUFDQSxDQTNNRiIsImZpbGUiOiJxdWlja19lZGl0L21vZGFscy9zcGVjaWFsX3ByaWNlcy9zcGVjaWFsX3ByaWNlc19pbml0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzcGVjaWFsX3ByaWNlX2luaXQgMjAxNi0xMi0xNFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogU3BlY2lhbCBQcmljZXMgVGFibGUgSW5pdGlhbGl6YXRpb25zIENvbnRyb2xsZXJcbiAqXG4gKiBUaGlzIGNvbnRyb2xsZXIgaW5pdGlhbGl6ZXMgdGhlIG1haW4gUXVpY2tFZGl0IHNwZWNpYWwgcHJpY2VzIHRhYmxlIHdpdGggYSBuZXcgalF1ZXJ5IERhdGFUYWJsZXMgaW5zdGFuY2UuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3NwZWNpYWxfcHJpY2VzX2luaXQnLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9kYXRhdGFibGVzL2pxdWVyeS5kYXRhVGFibGVzLm1pbi5jc3NgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9kYXRhdGFibGVzL2pxdWVyeS5kYXRhVGFibGVzLm1pbi5qc2AsXG5cdFx0J2RhdGF0YWJsZScsXG5cdFx0YCR7Z3guc291cmNlfS9saWJzL3F1aWNrX2VkaXRfc3BlY2lhbF9wcmljZXNfb3ZlcnZpZXdfY29sdW1uc2Bcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNlbGVjdGVkIFByb2R1Y3QgSURzXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7TnVtYmVyW119XG5cdFx0ICovXG5cdFx0bGV0IHByb2R1Y3RJZHMgPSBbMF07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUHJvcGVydGllcyBNb2RhbCBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkbW9kYWwgPSAkdGhpcy5wYXJlbnRzKCcubW9kYWwnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEYXRhVGFibGUgQ29sdW1uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdFtdfVxuXHRcdCAqL1xuXHRcdGNvbnN0IGNvbHVtbnMgPSBqc2UubGlicy5kYXRhdGFibGUucHJlcGFyZUNvbHVtbnMoJHRoaXMsIGpzZS5saWJzLnF1aWNrX2VkaXRfc3BlY2lhbF9wcmljZXNfb3ZlcnZpZXdfY29sdW1ucywgZGF0YS5zcGVjaWFsUHJpY2VBY3RpdmVDb2x1bW5zKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEYXRhVGFibGUgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBvcHRpb25zID0ge1xuXHRcdFx0YXV0b1dpZHRoOiBmYWxzZSxcblx0XHRcdGRvbTogJ3QnLFxuXHRcdFx0cGFnZUxlbmd0aDogZGF0YS5wYWdlTGVuZ3RoLFxuXHRcdFx0ZGlzcGxheVN0YXJ0OiAwLFxuXHRcdFx0c2VydmVyU2lkZTogdHJ1ZSxcblx0XHRcdGxhbmd1YWdlOiBqc2UubGlicy5kYXRhdGFibGUuZ2V0VHJhbnNsYXRpb25zKGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpKSxcblx0XHRcdGFqYXg6IHtcblx0XHRcdFx0dXJsOiBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVF1aWNrRWRpdFNwZWNpYWxQcmljZXNBamF4L0RhdGFUYWJsZScsXG5cdFx0XHRcdHR5cGU6ICdQT1NUJyxcblx0XHRcdFx0ZGF0YTogKGRhdGEpID0+IHtcblx0XHRcdFx0XHRkYXRhLnByb2R1Y3RJZCA9IF9nZXRQcm9kdWN0SWRzKCk7XG5cdFx0XHRcdFx0ZGF0YS5wYWdlVG9rZW4gPSBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKTtcblx0XHRcdFx0XHRyZXR1cm4gZGF0YTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG9yZGVyQ2VsbHNUb3A6IHRydWUsXG5cdFx0XHRvcmRlcjogX2dldE9yZGVyKCksXG5cdFx0XHRjb2x1bW5zOiBjb2x1bW5zLFxuXHRcdFx0Y3JlYXRlZFJvdyhyb3csIGRhdGEsIGRhdGFJbmRleCkge1xuXHRcdFx0XHRpZiAoZGF0YS5zcGVjaWFsUHJpY2VJc05ld0VudHJ5ID09PSB0cnVlKSB7XG5cdFx0XHRcdFx0JChyb3cpLmFkZENsYXNzKCdzcGVjaWFsLW5ldy1lbnRyeScpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBMb2FkaW5nIFNwaW5uZXIgXG5cdFx0ICogXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRsZXQgJHNwaW5uZXIgPSBudWxsO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgSW5pdGlhbCBUYWJsZSBPcmRlclxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T2JqZWN0W119IFJldHVybnMgdGhlIG9yZGVyZWQgY29sdW1uIGRlZmluaXRpb25zLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRPcmRlcigpIHtcblx0XHRcdGxldCBpbmRleCA9IDE7IC8vIE9yZGVyIGJ5IGZpcnN0IGNvbHVtbiBieSBkZWZhdWx0LlxuXHRcdFx0bGV0IGRpcmVjdGlvbiA9ICdhc2MnOyAvLyBPcmRlciBBU0MgYnkgZGVmYXVsdC5cblx0XHRcdGxldCBjb2x1bW5OYW1lID0gJ3Byb2R1Y3RzTmFtZSc7IC8vIE9yZGVyIGJ5IHByb2R1Y3RzIG5hbWUgYnkgZGVmYXVsdC5cblx0XHRcdFxuXHRcdFx0JHRoaXMub24oJ2NsaWNrJywgJ3RoJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGNvbHVtbk5hbWUgPSAkKHRoaXMpLmRhdGEoJ2NvbHVtbi1uYW1lJyk7XG5cdFx0XHRcdGluZGV4ID0gJHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKHRoaXMpLmluZGV4KCk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0aWYgKGRhdGEuc3BlY2lhbFByaWNlQWN0aXZlQ29sdW1ucy5pbmRleE9mKCdwcm9kdWN0c05hbWUnKSA+IC0xKSB7IC8vIE9yZGVyIGJ5IG5hbWUgaWYgcG9zc2libGUuXG5cdFx0XHRcdGluZGV4ID0gZGF0YS5zcGVjaWFsUHJpY2VBY3RpdmVDb2x1bW5zLmluZGV4T2YoJ3Byb2R1Y3RzTmFtZScpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRyZXR1cm4gW1tpbmRleCwgZGlyZWN0aW9uXV07XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNldCBjdXJyZW50IHByb2R1Y3QgSURzLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9zZXRQcm9kdWN0SWRzKGV2ZW50KSB7XG5cdFx0XHRwcm9kdWN0SWRzID0gW107XG5cdFx0XHRcblx0XHRcdGlmICgkKGV2ZW50LnRhcmdldCkuaXMoJ2Euc3BlY2lhbC1wcmljZScpKSB7XG5cdFx0XHRcdHByb2R1Y3RJZHMucHVzaCgkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGNvbnN0ICRzaW5nbGVDaGVja2JveGVzID0gJHRoaXMucGFyZW50cygnLnF1aWNrLWVkaXQub3ZlcnZpZXcnKVxuXHRcdFx0XHQuZmluZCgnaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZC5vdmVydmlldy1yb3ctc2VsZWN0aW9uJyk7XG5cdFx0XHRcblx0XHRcdCRzaW5nbGVDaGVja2JveGVzLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHByb2R1Y3RJZHMucHVzaCgkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKSk7XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IFByb2R1Y3QgSURzLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7TnVtYmVyW119IFJldHVybnMgdGhlIHByb2R1Y3QgSURzLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRQcm9kdWN0SWRzKCkge1xuXHRcdFx0cmV0dXJuIHByb2R1Y3RJZHM7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNwZWNpYWwgcHJpY2VzIG1vZGFsIHNob3duIGV2ZW50IGhhbmRsZXIuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uTW9kYWxTaG93bigpIHtcblx0XHRcdCR0aGlzLnBhcmVudHMoJ2JvZHknKS5maW5kKCcuY29taXNlby1kYXRlcmFuZ2VwaWNrZXInKS5jc3MoJ3otaW5kZXgnLCAxMDcwKTtcblx0XHRcdCQod2luZG93KS50cmlnZ2VyKCdyZXNpemUnKTtcblx0XHRcdFxuXHRcdFx0aWYgKCEkLmZuLkRhdGFUYWJsZS5pc0RhdGFUYWJsZSgkdGhpcykpIHtcblx0XHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmNyZWF0ZSgkdGhpcywgb3B0aW9ucyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmFqYXgucmVsb2FkKCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JG1vZGFsXG5cdFx0XHRcdC5vbignc2hvdy5icy5tb2RhbCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQodGhpcykuZmluZCgnLm1vZGFsLWRpYWxvZycpLmNzcygnei1pbmRleCcsIDEwNjApXG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5vbignc2hvd24uYnMubW9kYWwnLCBfb25Nb2RhbFNob3duKVxuXHRcdFx0XHQub24oJ2hpZGUuYnMubW9kYWwnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHQkKHRoaXMpLmZpbmQoJy5tb2RhbC1kaWFsb2cnKS5jc3MoJ3otaW5kZXgnLCAwKTtcblx0XHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLnBhcmVudHMoJy5xdWljay1lZGl0Lm92ZXJ2aWV3Jylcblx0XHRcdFx0Lm9uKCdjbGljaycsICdhLnNwZWNpYWwtcHJpY2UnLCBfc2V0UHJvZHVjdElkcylcblx0XHRcdFx0Lm9uKCdjbGljaycsICdhLmJ1bGstc3BlY2lhbC1wcmljZS1lZGl0JywgX3NldFByb2R1Y3RJZHMpO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5vbignZHJhdy5kdCcsICgpID0+IHtcblx0XHRcdFx0JHRoaXMuZmluZCgndGhlYWQgaW5wdXQ6Y2hlY2tib3gnKVxuXHRcdFx0XHRcdC5wcm9wKCdjaGVja2VkJywgZmFsc2UpXG5cdFx0XHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScsIFtmYWxzZV0pO1xuXHRcdFx0XHQkdGhpcy5maW5kKCd0Ym9keScpLmF0dHIoJ2RhdGEtZ3gtd2lkZ2V0JywgJ3NpbmdsZV9jaGVja2JveCBzd2l0Y2hlcicpO1xuXHRcdFx0XHQkdGhpcy5maW5kKCd0Ym9keScpLmF0dHIoJ2RhdGEtc2luZ2xlX2NoZWNrYm94LXNlbGVjdG9yJywgJy5zcGVjaWFsLXByaWNlLXJvdy1zZWxlY3Rpb24nKTtcblx0XHRcdFx0JHRoaXMuZmluZCgndGJvZHknKS5hdHRyKCdkYXRhLXN3aXRjaGVyLXNlbGVjdG9yJywgJy5jb252ZXJ0LXRvLXN3aXRjaGVyJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRneC53aWRnZXRzLmluaXQoJHRoaXMpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdwcmVYaHIuZHQnLCAoKSA9PiAkc3Bpbm5lciA9XG5cdFx0XHRcdGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5zaG93KCRtb2RhbC5maW5kKCcubW9kYWwtY29udGVudCcpLCAkbW9kYWwuY3NzKCd6LWluZGV4JykpKTtcblx0XHRcdCR0aGlzLm9uKCd4aHIuZHQnLCAoKSA9PiBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuaGlkZSgkc3Bpbm5lcikpO1xuXHRcdFx0XG5cdFx0XHRqc2UubGlicy5kYXRhdGFibGUuY3JlYXRlKCR0aGlzLCBvcHRpb25zKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7Il19
