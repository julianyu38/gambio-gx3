'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* --------------------------------------------------------------
 settings.js 2016-10-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles the settings modal.
 *
 * It retrieves the settings data via the user configuration service and sets the values.
 * You are able to change the column sort order and the visibility of each column. Additionally
 * you can change the height of the table rows.
 */
gx.controllers.module('settings', [jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.min.js', 'user_configuration_service', 'loading_spinner'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Class representing a controller for the QuickEdit overview settings modal.
  */

	var SettingsModalController = function () {
		/**
   * Creates an instance of QuickEditOverviewSettingsModalController.
   *
   * @param {Function}  done            Module finish callback function.
   * @param {jQuery}    $element        Module element.
   * @param {Object}    userCfgService  User configuration service library.
   * @param {Object}    loadingSpinner  Loading spinner library.
   * @param {Number}    userId          ID of currently signed in user.
   * @param {Object}    translator      Translator library.
   */
		function SettingsModalController(done, $element, userCfgService, loadingSpinner, userId, translator) {
			_classCallCheck(this, SettingsModalController);

			// Elements
			this.$element = $element;
			this.$submitButton = $element.find('button.submit-button');
			this.$settings = $element.find('ul.settings');
			this.$modal = $element.parents('.modal');
			this.$modalFooter = $element.find('.modal-footer');
			this.$resetDefaultLink = $element.find('a.reset-action');

			// Loading spinner
			this.$spinner = null;

			// Selector strings
			this.sortableHandleSelector = 'span.sort-handle';
			this.rowHeightValueSelector = 'select#setting-value-row-height';

			// Class names
			this.errorMessageClassName = 'error-message';
			this.loadingClassName = 'loading';

			// Libraries
			this.userCfgService = userCfgService;
			this.loadingSpinner = loadingSpinner;
			this.translator = translator;

			// Prefixes
			this.settingListItemIdPrefix = 'setting-';
			this.settingValueIdPrefix = 'setting-value-';

			// User configuration keys
			this.CONFIG_KEY_COLUMN_SETTINGS = 'quickEditOverviewSettingsColumns';
			this.CONFIG_KEY_ROW_HEIGHT_SETTINGS = 'quickEditOverviewSettingsRowHeight';

			// Default values
			this.DEFAULT_ROW_HEIGHT_SETTING = 'large';
			this.DEFAULT_COLUMN_SETTINGS = ['category', 'name', 'model', 'quantity', 'price', 'discount', 'specialPrice', 'tax', 'shippingStatusName', 'weight', 'shippingCosts', 'status'];

			// ID of currently signed in user.
			this.userId = userId;

			// Call module finish callback.
			done();
		}

		/**
   * Binds the event handlers.
   *
   * @return {SettingsModalController} Same instance for method chaining.
   */


		_createClass(SettingsModalController, [{
			key: 'initialize',
			value: function initialize() {
				var _this = this;

				// Attach event handler for click action on the submit button.
				this.$submitButton.on('click', function (event) {
					return _this._onSubmitButtonClick();
				});

				// Attach event handler for click action on the reset-default link.
				this.$resetDefaultLink.on('click', function (event) {
					return _this._onResetSettingsLinkClick(event);
				});

				// Attach event handlers to modal.
				this.$modal.on('show.bs.modal', function (event) {
					return _this._onModalShow();
				}).on('shown.bs.modal', function (event) {
					return _this._onModalShown();
				});

				return this;
			}

			/**
    * Fades out the modal content.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onModalShow',
			value: function _onModalShow() {
				this.$element.addClass(this.loadingClassName);

				return this;
			}

			/**
    * Updates the settings, clears any error messages and initializes the sortable plugin.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onModalShown',
			value: function _onModalShown() {
				this._refreshSettings()._clearErrorMessage()._initSortable();

				return this;
			}

			/**
    * Activates the jQuery UI Sortable plugin on the setting list items element.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_initSortable',
			value: function _initSortable() {
				// jQuery UI Sortable plugin options.
				var options = {
					items: '> li',
					axis: 'y',
					cursor: 'move',
					handle: this.sortableHandleSelector,
					containment: 'parent'
				};

				// Activate sortable plugin.
				this.$settings.sortable(options).disableSelection();

				return this;
			}

			/**
    * Returns a sorted array containing the IDs of all activated settings.
    *
    * @return {Array}
    *
    * @private
    */

		}, {
			key: '_serializeColumnSettings',
			value: function _serializeColumnSettings() {
				var _this2 = this;

				// Map iterator function to remove the 'setting-' prefix from list item ID.
				var removePrefixIterator = function removePrefixIterator(item) {
					return item.replace(_this2.settingListItemIdPrefix, '');
				};

				// Filter iterator function, to accept only list items with activated checkboxes.
				var filterIterator = function filterIterator(item) {
					return _this2.$settings.find('#' + _this2.settingValueIdPrefix + item).is(':checked');
				};

				// Return array with sorted, only active columns.
				return this.$settings.sortable('toArray').map(removePrefixIterator).filter(filterIterator);
			}

			/**
    * Returns the value of the selected row height option.
    *
    * @return {String}
    *
    * @private
    */

		}, {
			key: '_serializeRowHeightSetting',
			value: function _serializeRowHeightSetting() {
				return this.$element.find(this.rowHeightValueSelector).val();
			}

			/**
    * Shows the loading spinner, saves the settings to the user configuration,
    * closes the modal to finally re-render the datatable.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onSubmitButtonClick',
			value: function _onSubmitButtonClick() {
				var _this3 = this;

				// Retrieve setting values.
				var columnSettings = this._serializeColumnSettings();
				var rowHeightSetting = this._serializeRowHeightSetting();

				// Remove any error message and save settings.
				this._toggleLoadingSpinner(true)._clearErrorMessage()._saveColumnSettings(columnSettings).then(function () {
					return _this3._saveRowHeightSetting(rowHeightSetting);
				}).then(function () {
					return _this3._onSaveSuccess();
				}).catch(function () {
					return _this3._onSaveError();
				});

				return this;
			}

			/**
    * Prevents the browser to apply the default behavoir and
    * resets the column order and row size to the default setting values.
    *
    * @param {jQuery.Event} event Fired event.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onResetSettingsLinkClick',
			value: function _onResetSettingsLinkClick(event) {
				// Prevent default behavior.
				event.preventDefault();
				event.stopPropagation();

				// Reset to default settings.
				this._setDefaultSettings();

				return this;
			}

			/**
    * Shows and hides the loading spinner.
    *
    * @param {Boolean} doShow Show the loading spinner?
    *
    * @return {SettingsModalController} Same instance for method chaining.
    */

		}, {
			key: '_toggleLoadingSpinner',
			value: function _toggleLoadingSpinner(doShow) {
				if (doShow) {
					// Fade out modal content.
					this.$element.addClass(this.loadingClassName);

					// Show loading spinner.
					this.$spinner = this.loadingSpinner.show(this.$element);

					// Fix spinner z-index.
					this.$spinner.css({ 'z-index': 9999 });
				} else {
					// Fade out modal content.
					this.$element.removeClass(this.loadingClassName);

					// Hide the loading spinner.
					this.loadingSpinner.hide(this.$spinner);
				}

				return this;
			}

			/**
    * Handles the behavior on successful setting save action.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onSaveSuccess',
			value: function _onSaveSuccess() {
				window.location.reload();
				return this;
			}

			/**
    * Removes any error message, if found.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_clearErrorMessage',
			value: function _clearErrorMessage() {
				// Error message.
				var $errorMessage = this.$modalFooter.find('.' + this.errorMessageClassName);

				// Remove if it exists.
				if ($errorMessage.length) {
					$errorMessage.remove();
				}

				return this;
			}

			/**
    * Handles the behavior on thrown error while saving settings.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onSaveError',
			value: function _onSaveError() {
				// Error message.
				var errorMessage = this.translator.translate('TXT_SAVE_ERROR', 'admin_general');

				// Define error message element.
				var $error = $('<span/>', { class: this.errorMessageClassName, text: errorMessage });

				// Hide the loading spinner.
				this._toggleLoadingSpinner(false);

				// Add error message to modal footer.
				this.$modalFooter.prepend($error).hide().fadeIn();

				return this;
			}

			/**
    * Returns the configuration value for the column settings.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_getColumnSettings',
			value: function _getColumnSettings() {
				// Configuration data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_COLUMN_SETTINGS
				};

				// Request data from user configuration service.
				return this._getFromUserCfgService(data);
			}

			/**
    * Returns the configuration value for the row heights.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_getRowHeightSetting',
			value: function _getRowHeightSetting() {
				// Configuration data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_ROW_HEIGHT_SETTINGS
				};

				// Request data from user configuration service.
				return this._getFromUserCfgService(data);
			}

			/**
    * Returns the value for the passed user configuration data.
    *
    * @param {Object} data                   User configuration data.
    * @param {Number} data.userId            User ID.
    * @param {String} data.configurationKey  User configuration key.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_getFromUserCfgService',
			value: function _getFromUserCfgService(data) {
				var _this4 = this;

				// Promise handler.
				var handler = function handler(resolve, reject) {
					// User configuration service request options.
					var options = {
						onError: function onError() {
							return reject();
						},
						onSuccess: function onSuccess(response) {
							return resolve(response.configurationValue);
						},
						data: data
					};

					// Get configuration value.
					_this4.userCfgService.get(options);
				};

				return new Promise(handler);
			}

			/**
    * Saves the data via the user configuration service.
    *
    * @param {Object} data                     User configuration data.
    * @param {Number} data.userId              User ID.
    * @param {String} data.configurationKey    User configuration key.
    * @param {String} data.configurationValue  User configuration value.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_setWithUserCfgService',
			value: function _setWithUserCfgService(data) {
				var _this5 = this;

				// Promise handler.
				var handler = function handler(resolve, reject) {
					// User configuration service request options.
					var options = {
						onError: function onError() {
							return reject();
						},
						onSuccess: function onSuccess(response) {
							return resolve();
						},
						data: data
					};

					// Set configuration value.
					_this5.userCfgService.set(options);
				};

				return new Promise(handler);
			}

			/**
    * Saves the column settings via the user configuration service.
    *
    * @param {String[]} columnSettings Sorted array with active column.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_saveColumnSettings',
			value: function _saveColumnSettings(columnSettings) {
				// Check argument.
				if (!columnSettings || !Array.isArray(columnSettings)) {
					throw new Error('Missing or invalid column settings');
				}

				// User configuration request data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_COLUMN_SETTINGS,
					configurationValue: JSON.stringify(columnSettings)
				};

				// Save via user configuration service.
				return this._setWithUserCfgService(data);
			}

			/**
    * Saves the row height setting via the user configuration service.
    *
    * @param {String} rowHeightSetting Value of the selected row height setting.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_saveRowHeightSetting',
			value: function _saveRowHeightSetting(rowHeightSetting) {
				// Check argument.
				if (!rowHeightSetting || typeof rowHeightSetting !== 'string') {
					throw new Error('Missing or invalid row height setting');
				}

				// User configuration request data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_ROW_HEIGHT_SETTINGS,
					configurationValue: rowHeightSetting
				};

				// Save via user configuration service.
				return this._setWithUserCfgService(data);
			}

			/**
    * Retrieves the saved setting configuration and reorders/updates the settings.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_refreshSettings',
			value: function _refreshSettings() {
				var _this6 = this;

				// Show loading spinner.
				this._toggleLoadingSpinner(true);

				// Error handler function to specify the behavior on errors while processing.
				var onRefreshSettingsError = function onRefreshSettingsError(error) {
					// Output warning.
					console.warn('Error while refreshing', error);

					// Hide the loading spinner.
					_this6._toggleLoadingSpinner(false);
				};

				// Remove any error message, set row height,
				// reorder and update the settings and hide the loading spinner.
				this._clearErrorMessage()._getRowHeightSetting().then(function (rowHeightValue) {
					return _this6._setRowHeight(rowHeightValue);
				}).then(function () {
					return _this6._getColumnSettings();
				}).then(function (columnSettings) {
					return _this6._setColumnSettings(columnSettings);
				}).then(function () {
					return _this6._toggleLoadingSpinner(false);
				}).catch(onRefreshSettingsError);

				return this;
			}

			/**
    * Sets the row height setting value.
    *
    * @param {String} value Row height value.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_setRowHeight',
			value: function _setRowHeight() {
				var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.DEFAULT_ROW_HEIGHT_SETTING;

				this.$element.find(this.rowHeightValueSelector).val(value);

				return this;
			}

			/**
    * Reorders and updates the column setting values.
    *
    * @param {String|Array} columnSettings Encoded JSON array containing the saved column settings.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_setColumnSettings',
			value: function _setColumnSettings() {
				var _this7 = this;

				var columnSettings = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.DEFAULT_COLUMN_SETTINGS;

				// Regex for escape character.
				var ESCAPE_CHAR = /\\/g;

				// No need to parse from JSON on default value as it is an array.
				if (!Array.isArray(columnSettings)) {
					// Remove escape characters from and parse array from JSON.
					columnSettings = columnSettings.replace(ESCAPE_CHAR, '');
					columnSettings = JSON.parse(columnSettings);
				}

				// Cache container to temporarily hold all active list items in sorted order.
				// The children of this element will be prepended to the setting list item container to retain the 
				// sorting order.
				var $sortedItems = $('<div/>');

				// Iterator function to prepend active list items to the top and activate the checkbox.
				var settingIterator = function settingIterator(setting) {
					// List item ID.
					var id = _this7.settingListItemIdPrefix + setting;

					// Affected setting list item.
					var $listItem = _this7.$settings.find('#' + id);

					// Checkbox of affected list item.
					var $checkbox = $listItem.find('#' + _this7.settingValueIdPrefix + setting);

					// Activate checkbox.
					if (!$checkbox.is(':checked')) {
						$checkbox.parent().trigger('click');
					}

					// Move to cache container.
					$listItem.appendTo($sortedItems);
				};

				// Move active list items to the top bearing the sorting order in mind.
				columnSettings.forEach(settingIterator);

				// Prepend cached elements to item list.
				$sortedItems.children().prependTo(this.$settings);

				return this;
			}

			/**
    * Resets the column order and row height settings to the default.
    *
    * @return {SettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_setDefaultSettings',
			value: function _setDefaultSettings() {
				var _this8 = this;

				// Default values.
				var columnSettings = this.DEFAULT_COLUMN_SETTINGS;
				var rowHeight = this.DEFAULT_ROW_HEIGHT_SETTING;

				// Set column settings.
				// Cache container to temporarily hold all active list items in sorted order.
				// The children of this element will be prepended to the setting list item container to retain the 
				// sorting order.
				var $sortedItems = $('<div/>');

				// Iterator function to prepend active list items to the top and activate the checkbox.
				var settingIterator = function settingIterator(setting) {
					// List item ID.
					var id = _this8.settingListItemIdPrefix + setting;

					// Affected setting list item.
					var $listItem = _this8.$settings.find('#' + id);

					// Checkbox of affected list item.
					var $checkbox = $listItem.find('#' + _this8.settingValueIdPrefix + setting);

					// Activate checkbox.
					if (!$checkbox.is(':checked')) {
						$checkbox.parent().trigger('click');
					}

					// Move to cache container.
					$listItem.appendTo($sortedItems);
				};

				// Deactivate all checkboxes.
				this.$settings.find(':checkbox').each(function (index, element) {
					var $checkbox = $(element);

					if ($checkbox.is(':checked')) {
						$checkbox.parent().trigger('click');
					}
				});

				// Move active list items to the top bearing the sorting order in mind.
				columnSettings.forEach(settingIterator);

				// Prepend cached elements to item list.
				$sortedItems.children().prependTo(this.$settings);

				// Set row height.
				this.$element.find(this.rowHeightValueSelector).val(rowHeight);

				return this;
			}
		}]);

		return SettingsModalController;
	}();

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Dependencies.
		var userCfgService = jse.libs.user_configuration_service;
		var loadingSpinner = jse.libs.loading_spinner;
		var userId = data.userId;
		var translator = jse.core.lang;

		// Create a new instance and load settings.
		var settingsModal = new SettingsModalController(done, $this, userCfgService, loadingSpinner, userId, translator);

		settingsModal.initialize();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3NldHRpbmdzLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiU2V0dGluZ3NNb2RhbENvbnRyb2xsZXIiLCJkb25lIiwiJGVsZW1lbnQiLCJ1c2VyQ2ZnU2VydmljZSIsImxvYWRpbmdTcGlubmVyIiwidXNlcklkIiwidHJhbnNsYXRvciIsIiRzdWJtaXRCdXR0b24iLCJmaW5kIiwiJHNldHRpbmdzIiwiJG1vZGFsIiwicGFyZW50cyIsIiRtb2RhbEZvb3RlciIsIiRyZXNldERlZmF1bHRMaW5rIiwiJHNwaW5uZXIiLCJzb3J0YWJsZUhhbmRsZVNlbGVjdG9yIiwicm93SGVpZ2h0VmFsdWVTZWxlY3RvciIsImVycm9yTWVzc2FnZUNsYXNzTmFtZSIsImxvYWRpbmdDbGFzc05hbWUiLCJzZXR0aW5nTGlzdEl0ZW1JZFByZWZpeCIsInNldHRpbmdWYWx1ZUlkUHJlZml4IiwiQ09ORklHX0tFWV9DT0xVTU5fU0VUVElOR1MiLCJDT05GSUdfS0VZX1JPV19IRUlHSFRfU0VUVElOR1MiLCJERUZBVUxUX1JPV19IRUlHSFRfU0VUVElORyIsIkRFRkFVTFRfQ09MVU1OX1NFVFRJTkdTIiwib24iLCJfb25TdWJtaXRCdXR0b25DbGljayIsIl9vblJlc2V0U2V0dGluZ3NMaW5rQ2xpY2siLCJldmVudCIsIl9vbk1vZGFsU2hvdyIsIl9vbk1vZGFsU2hvd24iLCJhZGRDbGFzcyIsIl9yZWZyZXNoU2V0dGluZ3MiLCJfY2xlYXJFcnJvck1lc3NhZ2UiLCJfaW5pdFNvcnRhYmxlIiwib3B0aW9ucyIsIml0ZW1zIiwiYXhpcyIsImN1cnNvciIsImhhbmRsZSIsImNvbnRhaW5tZW50Iiwic29ydGFibGUiLCJkaXNhYmxlU2VsZWN0aW9uIiwicmVtb3ZlUHJlZml4SXRlcmF0b3IiLCJpdGVtIiwicmVwbGFjZSIsImZpbHRlckl0ZXJhdG9yIiwiaXMiLCJtYXAiLCJmaWx0ZXIiLCJ2YWwiLCJjb2x1bW5TZXR0aW5ncyIsIl9zZXJpYWxpemVDb2x1bW5TZXR0aW5ncyIsInJvd0hlaWdodFNldHRpbmciLCJfc2VyaWFsaXplUm93SGVpZ2h0U2V0dGluZyIsIl90b2dnbGVMb2FkaW5nU3Bpbm5lciIsIl9zYXZlQ29sdW1uU2V0dGluZ3MiLCJ0aGVuIiwiX3NhdmVSb3dIZWlnaHRTZXR0aW5nIiwiX29uU2F2ZVN1Y2Nlc3MiLCJjYXRjaCIsIl9vblNhdmVFcnJvciIsInByZXZlbnREZWZhdWx0Iiwic3RvcFByb3BhZ2F0aW9uIiwiX3NldERlZmF1bHRTZXR0aW5ncyIsImRvU2hvdyIsInNob3ciLCJjc3MiLCJyZW1vdmVDbGFzcyIsImhpZGUiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInJlbG9hZCIsIiRlcnJvck1lc3NhZ2UiLCJsZW5ndGgiLCJyZW1vdmUiLCJlcnJvck1lc3NhZ2UiLCJ0cmFuc2xhdGUiLCIkZXJyb3IiLCJjbGFzcyIsInRleHQiLCJwcmVwZW5kIiwiZmFkZUluIiwiY29uZmlndXJhdGlvbktleSIsIl9nZXRGcm9tVXNlckNmZ1NlcnZpY2UiLCJoYW5kbGVyIiwicmVzb2x2ZSIsInJlamVjdCIsIm9uRXJyb3IiLCJvblN1Y2Nlc3MiLCJyZXNwb25zZSIsImNvbmZpZ3VyYXRpb25WYWx1ZSIsImdldCIsIlByb21pc2UiLCJzZXQiLCJBcnJheSIsImlzQXJyYXkiLCJFcnJvciIsIkpTT04iLCJzdHJpbmdpZnkiLCJfc2V0V2l0aFVzZXJDZmdTZXJ2aWNlIiwib25SZWZyZXNoU2V0dGluZ3NFcnJvciIsImNvbnNvbGUiLCJ3YXJuIiwiZXJyb3IiLCJfZ2V0Um93SGVpZ2h0U2V0dGluZyIsIl9zZXRSb3dIZWlnaHQiLCJyb3dIZWlnaHRWYWx1ZSIsIl9nZXRDb2x1bW5TZXR0aW5ncyIsIl9zZXRDb2x1bW5TZXR0aW5ncyIsInZhbHVlIiwiRVNDQVBFX0NIQVIiLCJwYXJzZSIsIiRzb3J0ZWRJdGVtcyIsInNldHRpbmdJdGVyYXRvciIsImlkIiwic2V0dGluZyIsIiRsaXN0SXRlbSIsIiRjaGVja2JveCIsInBhcmVudCIsInRyaWdnZXIiLCJhcHBlbmRUbyIsImZvckVhY2giLCJjaGlsZHJlbiIsInByZXBlbmRUbyIsInJvd0hlaWdodCIsImVhY2giLCJpbmRleCIsImVsZW1lbnQiLCJpbml0IiwibGlicyIsInVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlIiwibG9hZGluZ19zcGlubmVyIiwiY29yZSIsImxhbmciLCJzZXR0aW5nc01vZGFsIiwiaW5pdGlhbGl6ZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7OztBQU9BQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxVQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUiwwQ0FFSUQsSUFBSUMsTUFGUix5Q0FHQyw0QkFIRCxFQUlDLGlCQUpELENBSEQsRUFVQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNTCxTQUFTLEVBQWY7O0FBR0E7QUFDQTtBQUNBOztBQUVBOzs7O0FBM0JjLEtBOEJSTSx1QkE5QlE7QUErQmI7Ozs7Ozs7Ozs7QUFVQSxtQ0FBWUMsSUFBWixFQUFrQkMsUUFBbEIsRUFBNEJDLGNBQTVCLEVBQTRDQyxjQUE1QyxFQUE0REMsTUFBNUQsRUFBb0VDLFVBQXBFLEVBQWdGO0FBQUE7O0FBQy9FO0FBQ0EsUUFBS0osUUFBTCxHQUFnQkEsUUFBaEI7QUFDQSxRQUFLSyxhQUFMLEdBQXFCTCxTQUFTTSxJQUFULENBQWMsc0JBQWQsQ0FBckI7QUFDQSxRQUFLQyxTQUFMLEdBQWlCUCxTQUFTTSxJQUFULENBQWMsYUFBZCxDQUFqQjtBQUNBLFFBQUtFLE1BQUwsR0FBY1IsU0FBU1MsT0FBVCxDQUFpQixRQUFqQixDQUFkO0FBQ0EsUUFBS0MsWUFBTCxHQUFvQlYsU0FBU00sSUFBVCxDQUFjLGVBQWQsQ0FBcEI7QUFDQSxRQUFLSyxpQkFBTCxHQUF5QlgsU0FBU00sSUFBVCxDQUFjLGdCQUFkLENBQXpCOztBQUVBO0FBQ0EsUUFBS00sUUFBTCxHQUFnQixJQUFoQjs7QUFFQTtBQUNBLFFBQUtDLHNCQUFMLEdBQThCLGtCQUE5QjtBQUNBLFFBQUtDLHNCQUFMLEdBQThCLGlDQUE5Qjs7QUFFQTtBQUNBLFFBQUtDLHFCQUFMLEdBQTZCLGVBQTdCO0FBQ0EsUUFBS0MsZ0JBQUwsR0FBd0IsU0FBeEI7O0FBRUE7QUFDQSxRQUFLZixjQUFMLEdBQXNCQSxjQUF0QjtBQUNBLFFBQUtDLGNBQUwsR0FBc0JBLGNBQXRCO0FBQ0EsUUFBS0UsVUFBTCxHQUFrQkEsVUFBbEI7O0FBRUE7QUFDQSxRQUFLYSx1QkFBTCxHQUErQixVQUEvQjtBQUNBLFFBQUtDLG9CQUFMLEdBQTRCLGdCQUE1Qjs7QUFFQTtBQUNBLFFBQUtDLDBCQUFMLEdBQWtDLGtDQUFsQztBQUNBLFFBQUtDLDhCQUFMLEdBQXNDLG9DQUF0Qzs7QUFFQTtBQUNBLFFBQUtDLDBCQUFMLEdBQWtDLE9BQWxDO0FBQ0EsUUFBS0MsdUJBQUwsR0FBK0IsQ0FDOUIsVUFEOEIsRUFFOUIsTUFGOEIsRUFHOUIsT0FIOEIsRUFJOUIsVUFKOEIsRUFLOUIsT0FMOEIsRUFNOUIsVUFOOEIsRUFPOUIsY0FQOEIsRUFROUIsS0FSOEIsRUFTOUIsb0JBVDhCLEVBVTlCLFFBVjhCLEVBVzlCLGVBWDhCLEVBWTlCLFFBWjhCLENBQS9COztBQWVBO0FBQ0EsUUFBS25CLE1BQUwsR0FBY0EsTUFBZDs7QUFFQTtBQUNBSjtBQUNBOztBQUVEOzs7Ozs7O0FBbEdhO0FBQUE7QUFBQSxnQ0F1R0E7QUFBQTs7QUFDWjtBQUNBLFNBQUtNLGFBQUwsQ0FBbUJrQixFQUFuQixDQUFzQixPQUF0QixFQUErQjtBQUFBLFlBQVMsTUFBS0Msb0JBQUwsRUFBVDtBQUFBLEtBQS9COztBQUVBO0FBQ0EsU0FBS2IsaUJBQUwsQ0FBdUJZLEVBQXZCLENBQTBCLE9BQTFCLEVBQW1DO0FBQUEsWUFBUyxNQUFLRSx5QkFBTCxDQUErQkMsS0FBL0IsQ0FBVDtBQUFBLEtBQW5DOztBQUVBO0FBQ0EsU0FBS2xCLE1BQUwsQ0FDRWUsRUFERixDQUNLLGVBREwsRUFDc0I7QUFBQSxZQUFTLE1BQUtJLFlBQUwsRUFBVDtBQUFBLEtBRHRCLEVBRUVKLEVBRkYsQ0FFSyxnQkFGTCxFQUV1QjtBQUFBLFlBQVMsTUFBS0ssYUFBTCxFQUFUO0FBQUEsS0FGdkI7O0FBSUEsV0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBdEhhO0FBQUE7QUFBQSxrQ0E2SEU7QUFDZCxTQUFLNUIsUUFBTCxDQUFjNkIsUUFBZCxDQUF1QixLQUFLYixnQkFBNUI7O0FBRUEsV0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBbklhO0FBQUE7QUFBQSxtQ0EwSUc7QUFDZixTQUNFYyxnQkFERixHQUVFQyxrQkFGRixHQUdFQyxhQUhGOztBQUtBLFdBQU8sSUFBUDtBQUNBOztBQUVEOzs7Ozs7OztBQW5KYTtBQUFBO0FBQUEsbUNBMEpHO0FBQ2Y7QUFDQSxRQUFNQyxVQUFVO0FBQ2ZDLFlBQU8sTUFEUTtBQUVmQyxXQUFNLEdBRlM7QUFHZkMsYUFBUSxNQUhPO0FBSWZDLGFBQVEsS0FBS3hCLHNCQUpFO0FBS2Z5QixrQkFBYTtBQUxFLEtBQWhCOztBQVFBO0FBQ0EsU0FBSy9CLFNBQUwsQ0FDRWdDLFFBREYsQ0FDV04sT0FEWCxFQUVFTyxnQkFGRjs7QUFJQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUE1S2E7QUFBQTtBQUFBLDhDQW1MYztBQUFBOztBQUMxQjtBQUNBLFFBQU1DLHVCQUF1QixTQUF2QkEsb0JBQXVCO0FBQUEsWUFBUUMsS0FBS0MsT0FBTCxDQUFhLE9BQUsxQix1QkFBbEIsRUFBMkMsRUFBM0MsQ0FBUjtBQUFBLEtBQTdCOztBQUVBO0FBQ0EsUUFBTTJCLGlCQUFpQixTQUFqQkEsY0FBaUI7QUFBQSxZQUFRLE9BQUtyQyxTQUFMLENBQWVELElBQWYsQ0FBb0IsTUFBTSxPQUFLWSxvQkFBWCxHQUFrQ3dCLElBQXRELEVBQzdCRyxFQUQ2QixDQUMxQixVQUQwQixDQUFSO0FBQUEsS0FBdkI7O0FBR0E7QUFDQSxXQUFPLEtBQUt0QyxTQUFMLENBQ0xnQyxRQURLLENBQ0ksU0FESixFQUVMTyxHQUZLLENBRURMLG9CQUZDLEVBR0xNLE1BSEssQ0FHRUgsY0FIRixDQUFQO0FBSUE7O0FBRUQ7Ozs7Ozs7O0FBbE1hO0FBQUE7QUFBQSxnREF5TWdCO0FBQzVCLFdBQU8sS0FDTDVDLFFBREssQ0FFTE0sSUFGSyxDQUVBLEtBQUtRLHNCQUZMLEVBR0xrQyxHQUhLLEVBQVA7QUFJQTs7QUFFRDs7Ozs7Ozs7O0FBaE5hO0FBQUE7QUFBQSwwQ0F3TlU7QUFBQTs7QUFDdEI7QUFDQSxRQUFNQyxpQkFBaUIsS0FBS0Msd0JBQUwsRUFBdkI7QUFDQSxRQUFNQyxtQkFBbUIsS0FBS0MsMEJBQUwsRUFBekI7O0FBRUE7QUFDQSxTQUNFQyxxQkFERixDQUN3QixJQUR4QixFQUVFdEIsa0JBRkYsR0FHRXVCLG1CQUhGLENBR3NCTCxjQUh0QixFQUlFTSxJQUpGLENBSU87QUFBQSxZQUFNLE9BQUtDLHFCQUFMLENBQTJCTCxnQkFBM0IsQ0FBTjtBQUFBLEtBSlAsRUFLRUksSUFMRixDQUtPO0FBQUEsWUFBTSxPQUFLRSxjQUFMLEVBQU47QUFBQSxLQUxQLEVBTUVDLEtBTkYsQ0FNUTtBQUFBLFlBQU0sT0FBS0MsWUFBTCxFQUFOO0FBQUEsS0FOUjs7QUFRQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7Ozs7QUF6T2E7QUFBQTtBQUFBLDZDQW1QYWpDLEtBblBiLEVBbVBvQjtBQUNoQztBQUNBQSxVQUFNa0MsY0FBTjtBQUNBbEMsVUFBTW1DLGVBQU47O0FBRUE7QUFDQSxTQUFLQyxtQkFBTDs7QUFFQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUE5UGE7QUFBQTtBQUFBLHlDQXFRU0MsTUFyUVQsRUFxUWlCO0FBQzdCLFFBQUlBLE1BQUosRUFBWTtBQUNYO0FBQ0EsVUFBSy9ELFFBQUwsQ0FBYzZCLFFBQWQsQ0FBdUIsS0FBS2IsZ0JBQTVCOztBQUVBO0FBQ0EsVUFBS0osUUFBTCxHQUFnQixLQUFLVixjQUFMLENBQW9COEQsSUFBcEIsQ0FBeUIsS0FBS2hFLFFBQTlCLENBQWhCOztBQUVBO0FBQ0EsVUFBS1ksUUFBTCxDQUFjcUQsR0FBZCxDQUFrQixFQUFDLFdBQVcsSUFBWixFQUFsQjtBQUNBLEtBVEQsTUFTTztBQUNOO0FBQ0EsVUFBS2pFLFFBQUwsQ0FBY2tFLFdBQWQsQ0FBMEIsS0FBS2xELGdCQUEvQjs7QUFFQTtBQUNBLFVBQUtkLGNBQUwsQ0FBb0JpRSxJQUFwQixDQUF5QixLQUFLdkQsUUFBOUI7QUFDQTs7QUFFRCxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUExUmE7QUFBQTtBQUFBLG9DQWlTSTtBQUNoQndELFdBQU9DLFFBQVAsQ0FBZ0JDLE1BQWhCO0FBQ0EsV0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBdFNhO0FBQUE7QUFBQSx3Q0E2U1E7QUFDcEI7QUFDQSxRQUFNQyxnQkFBZ0IsS0FBSzdELFlBQUwsQ0FBa0JKLElBQWxCLE9BQTJCLEtBQUtTLHFCQUFoQyxDQUF0Qjs7QUFFQTtBQUNBLFFBQUl3RCxjQUFjQyxNQUFsQixFQUEwQjtBQUN6QkQsbUJBQWNFLE1BQWQ7QUFDQTs7QUFFRCxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUF6VGE7QUFBQTtBQUFBLGtDQWdVRTtBQUNkO0FBQ0EsUUFBTUMsZUFBZSxLQUFLdEUsVUFBTCxDQUFnQnVFLFNBQWhCLENBQTBCLGdCQUExQixFQUE0QyxlQUE1QyxDQUFyQjs7QUFFQTtBQUNBLFFBQU1DLFNBQVMvRSxFQUFFLFNBQUYsRUFBYSxFQUFDZ0YsT0FBTyxLQUFLOUQscUJBQWIsRUFBb0MrRCxNQUFNSixZQUExQyxFQUFiLENBQWY7O0FBRUE7QUFDQSxTQUFLckIscUJBQUwsQ0FBMkIsS0FBM0I7O0FBRUE7QUFDQSxTQUFLM0MsWUFBTCxDQUNFcUUsT0FERixDQUNVSCxNQURWLEVBRUVULElBRkYsR0FHRWEsTUFIRjs7QUFLQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUFuVmE7QUFBQTtBQUFBLHdDQTBWUTtBQUNwQjtBQUNBLFFBQU1yRixPQUFPO0FBQ1pRLGFBQVEsS0FBS0EsTUFERDtBQUVaOEUsdUJBQWtCLEtBQUs5RDtBQUZYLEtBQWI7O0FBS0E7QUFDQSxXQUFPLEtBQUsrRCxzQkFBTCxDQUE0QnZGLElBQTVCLENBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUFyV2E7QUFBQTtBQUFBLDBDQTRXVTtBQUN0QjtBQUNBLFFBQU1BLE9BQU87QUFDWlEsYUFBUSxLQUFLQSxNQUREO0FBRVo4RSx1QkFBa0IsS0FBSzdEO0FBRlgsS0FBYjs7QUFLQTtBQUNBLFdBQU8sS0FBSzhELHNCQUFMLENBQTRCdkYsSUFBNUIsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7Ozs7QUF2WGE7QUFBQTtBQUFBLDBDQWtZVUEsSUFsWVYsRUFrWWdCO0FBQUE7O0FBQzVCO0FBQ0EsUUFBTXdGLFVBQVUsU0FBVkEsT0FBVSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDcEM7QUFDQSxTQUFNcEQsVUFBVTtBQUNmcUQsZUFBUztBQUFBLGNBQU1ELFFBQU47QUFBQSxPQURNO0FBRWZFLGlCQUFXO0FBQUEsY0FBWUgsUUFBUUksU0FBU0Msa0JBQWpCLENBQVo7QUFBQSxPQUZJO0FBR2Y5RjtBQUhlLE1BQWhCOztBQU1BO0FBQ0EsWUFBS00sY0FBTCxDQUFvQnlGLEdBQXBCLENBQXdCekQsT0FBeEI7QUFDQSxLQVZEOztBQVlBLFdBQU8sSUFBSTBELE9BQUosQ0FBWVIsT0FBWixDQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7QUFuWmE7QUFBQTtBQUFBLDBDQStaVXhGLElBL1pWLEVBK1pnQjtBQUFBOztBQUM1QjtBQUNBLFFBQU13RixVQUFVLFNBQVZBLE9BQVUsQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3BDO0FBQ0EsU0FBTXBELFVBQVU7QUFDZnFELGVBQVM7QUFBQSxjQUFNRCxRQUFOO0FBQUEsT0FETTtBQUVmRSxpQkFBVztBQUFBLGNBQVlILFNBQVo7QUFBQSxPQUZJO0FBR2Z6RjtBQUhlLE1BQWhCOztBQU1BO0FBQ0EsWUFBS00sY0FBTCxDQUFvQjJGLEdBQXBCLENBQXdCM0QsT0FBeEI7QUFDQSxLQVZEOztBQVlBLFdBQU8sSUFBSTBELE9BQUosQ0FBWVIsT0FBWixDQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7QUFoYmE7QUFBQTtBQUFBLHVDQXliT2xDLGNBemJQLEVBeWJ1QjtBQUNuQztBQUNBLFFBQUksQ0FBQ0EsY0FBRCxJQUFtQixDQUFDNEMsTUFBTUMsT0FBTixDQUFjN0MsY0FBZCxDQUF4QixFQUF1RDtBQUN0RCxXQUFNLElBQUk4QyxLQUFKLENBQVUsb0NBQVYsQ0FBTjtBQUNBOztBQUVEO0FBQ0EsUUFBTXBHLE9BQU87QUFDWlEsYUFBUSxLQUFLQSxNQUREO0FBRVo4RSx1QkFBa0IsS0FBSzlELDBCQUZYO0FBR1pzRSx5QkFBb0JPLEtBQUtDLFNBQUwsQ0FBZWhELGNBQWY7QUFIUixLQUFiOztBQU1BO0FBQ0EsV0FBTyxLQUFLaUQsc0JBQUwsQ0FBNEJ2RyxJQUE1QixDQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7QUExY2E7QUFBQTtBQUFBLHlDQW1kU3dELGdCQW5kVCxFQW1kMkI7QUFDdkM7QUFDQSxRQUFJLENBQUNBLGdCQUFELElBQXFCLE9BQU9BLGdCQUFQLEtBQTRCLFFBQXJELEVBQStEO0FBQzlELFdBQU0sSUFBSTRDLEtBQUosQ0FBVSx1Q0FBVixDQUFOO0FBQ0E7O0FBRUQ7QUFDQSxRQUFNcEcsT0FBTztBQUNaUSxhQUFRLEtBQUtBLE1BREQ7QUFFWjhFLHVCQUFrQixLQUFLN0QsOEJBRlg7QUFHWnFFLHlCQUFvQnRDO0FBSFIsS0FBYjs7QUFNQTtBQUNBLFdBQU8sS0FBSytDLHNCQUFMLENBQTRCdkcsSUFBNUIsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7OztBQXBlYTtBQUFBO0FBQUEsc0NBMmVNO0FBQUE7O0FBQ2xCO0FBQ0EsU0FBSzBELHFCQUFMLENBQTJCLElBQTNCOztBQUVBO0FBQ0EsUUFBTThDLHlCQUF5QixTQUF6QkEsc0JBQXlCLFFBQVM7QUFDdkM7QUFDQUMsYUFBUUMsSUFBUixDQUFhLHdCQUFiLEVBQXVDQyxLQUF2Qzs7QUFFQTtBQUNBLFlBQUtqRCxxQkFBTCxDQUEyQixLQUEzQjtBQUNBLEtBTkQ7O0FBUUE7QUFDQTtBQUNBLFNBQ0V0QixrQkFERixHQUVFd0Usb0JBRkYsR0FHRWhELElBSEYsQ0FHTztBQUFBLFlBQWtCLE9BQUtpRCxhQUFMLENBQW1CQyxjQUFuQixDQUFsQjtBQUFBLEtBSFAsRUFJRWxELElBSkYsQ0FJTztBQUFBLFlBQU0sT0FBS21ELGtCQUFMLEVBQU47QUFBQSxLQUpQLEVBS0VuRCxJQUxGLENBS087QUFBQSxZQUFrQixPQUFLb0Qsa0JBQUwsQ0FBd0IxRCxjQUF4QixDQUFsQjtBQUFBLEtBTFAsRUFNRU0sSUFORixDQU1PO0FBQUEsWUFBTSxPQUFLRixxQkFBTCxDQUEyQixLQUEzQixDQUFOO0FBQUEsS0FOUCxFQU9FSyxLQVBGLENBT1F5QyxzQkFQUjs7QUFTQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7OztBQXRnQmE7QUFBQTtBQUFBLG1DQStnQjBDO0FBQUEsUUFBekNTLEtBQXlDLHVFQUFqQyxLQUFLdkYsMEJBQTRCOztBQUN0RCxTQUNFckIsUUFERixDQUVFTSxJQUZGLENBRU8sS0FBS1Esc0JBRlosRUFHRWtDLEdBSEYsQ0FHTTRELEtBSE47O0FBS0EsV0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7QUF4aEJhO0FBQUE7QUFBQSx3Q0FpaUJxRDtBQUFBOztBQUFBLFFBQS9DM0QsY0FBK0MsdUVBQTlCLEtBQUszQix1QkFBeUI7O0FBQ2pFO0FBQ0EsUUFBTXVGLGNBQWMsS0FBcEI7O0FBRUE7QUFDQSxRQUFJLENBQUNoQixNQUFNQyxPQUFOLENBQWM3QyxjQUFkLENBQUwsRUFBb0M7QUFDbkM7QUFDQUEsc0JBQWlCQSxlQUFlTixPQUFmLENBQXVCa0UsV0FBdkIsRUFBb0MsRUFBcEMsQ0FBakI7QUFDQTVELHNCQUFpQitDLEtBQUtjLEtBQUwsQ0FBVzdELGNBQVgsQ0FBakI7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxRQUFNOEQsZUFBZWxILEVBQUUsUUFBRixDQUFyQjs7QUFFQTtBQUNBLFFBQU1tSCxrQkFBa0IsU0FBbEJBLGVBQWtCLFVBQVc7QUFDbEM7QUFDQSxTQUFNQyxLQUFLLE9BQUtoRyx1QkFBTCxHQUErQmlHLE9BQTFDOztBQUVBO0FBQ0EsU0FBTUMsWUFBWSxPQUFLNUcsU0FBTCxDQUFlRCxJQUFmLE9BQXdCMkcsRUFBeEIsQ0FBbEI7O0FBRUE7QUFDQSxTQUFNRyxZQUFZRCxVQUFVN0csSUFBVixDQUFlLE1BQU0sT0FBS1ksb0JBQVgsR0FBa0NnRyxPQUFqRCxDQUFsQjs7QUFFQTtBQUNBLFNBQUksQ0FBQ0UsVUFBVXZFLEVBQVYsQ0FBYSxVQUFiLENBQUwsRUFBK0I7QUFDOUJ1RSxnQkFBVUMsTUFBVixHQUFtQkMsT0FBbkIsQ0FBMkIsT0FBM0I7QUFDQTs7QUFFRDtBQUNBSCxlQUFVSSxRQUFWLENBQW1CUixZQUFuQjtBQUNBLEtBakJEOztBQW1CQTtBQUNBOUQsbUJBQWV1RSxPQUFmLENBQXVCUixlQUF2Qjs7QUFFQTtBQUNBRCxpQkFDRVUsUUFERixHQUVFQyxTQUZGLENBRVksS0FBS25ILFNBRmpCOztBQUlBLFdBQU8sSUFBUDtBQUNBOztBQUVEOzs7Ozs7OztBQWhsQmE7QUFBQTtBQUFBLHlDQXVsQlM7QUFBQTs7QUFDckI7QUFDQSxRQUFNMEMsaUJBQWlCLEtBQUszQix1QkFBNUI7QUFDQSxRQUFNcUcsWUFBWSxLQUFLdEcsMEJBQXZCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBTTBGLGVBQWVsSCxFQUFFLFFBQUYsQ0FBckI7O0FBRUE7QUFDQSxRQUFNbUgsa0JBQWtCLFNBQWxCQSxlQUFrQixVQUFXO0FBQ2xDO0FBQ0EsU0FBTUMsS0FBSyxPQUFLaEcsdUJBQUwsR0FBK0JpRyxPQUExQzs7QUFFQTtBQUNBLFNBQU1DLFlBQVksT0FBSzVHLFNBQUwsQ0FBZUQsSUFBZixPQUF3QjJHLEVBQXhCLENBQWxCOztBQUVBO0FBQ0EsU0FBTUcsWUFBWUQsVUFBVTdHLElBQVYsQ0FBZSxNQUFNLE9BQUtZLG9CQUFYLEdBQWtDZ0csT0FBakQsQ0FBbEI7O0FBRUE7QUFDQSxTQUFJLENBQUNFLFVBQVV2RSxFQUFWLENBQWEsVUFBYixDQUFMLEVBQStCO0FBQzlCdUUsZ0JBQVVDLE1BQVYsR0FBbUJDLE9BQW5CLENBQTJCLE9BQTNCO0FBQ0E7O0FBRUQ7QUFDQUgsZUFBVUksUUFBVixDQUFtQlIsWUFBbkI7QUFDQSxLQWpCRDs7QUFtQkE7QUFDQSxTQUNFeEcsU0FERixDQUVFRCxJQUZGLENBRU8sV0FGUCxFQUdFc0gsSUFIRixDQUdPLFVBQUNDLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUN6QixTQUFNVixZQUFZdkgsRUFBRWlJLE9BQUYsQ0FBbEI7O0FBRUEsU0FBSVYsVUFBVXZFLEVBQVYsQ0FBYSxVQUFiLENBQUosRUFBOEI7QUFDN0J1RSxnQkFBVUMsTUFBVixHQUFtQkMsT0FBbkIsQ0FBMkIsT0FBM0I7QUFDQTtBQUNELEtBVEY7O0FBV0E7QUFDQXJFLG1CQUFldUUsT0FBZixDQUF1QlIsZUFBdkI7O0FBRUE7QUFDQUQsaUJBQ0VVLFFBREYsR0FFRUMsU0FGRixDQUVZLEtBQUtuSCxTQUZqQjs7QUFJQTtBQUNBLFNBQ0VQLFFBREYsQ0FFRU0sSUFGRixDQUVPLEtBQUtRLHNCQUZaLEVBR0VrQyxHQUhGLENBR00yRSxTQUhOOztBQUtBLFdBQU8sSUFBUDtBQUNBO0FBanBCWTs7QUFBQTtBQUFBOztBQW9wQmQ7QUFDQTtBQUNBOztBQUVBbkksUUFBT3VJLElBQVAsR0FBYyxVQUFTaEksSUFBVCxFQUFlO0FBQzVCO0FBQ0EsTUFBTUUsaUJBQWlCUixJQUFJdUksSUFBSixDQUFTQywwQkFBaEM7QUFDQSxNQUFNL0gsaUJBQWlCVCxJQUFJdUksSUFBSixDQUFTRSxlQUFoQztBQUNBLE1BQU0vSCxTQUFTUixLQUFLUSxNQUFwQjtBQUNBLE1BQU1DLGFBQWFYLElBQUkwSSxJQUFKLENBQVNDLElBQTVCOztBQUVBO0FBQ0EsTUFBTUMsZ0JBQWdCLElBQUl2SSx1QkFBSixDQUE0QkMsSUFBNUIsRUFBa0NILEtBQWxDLEVBQXlDSyxjQUF6QyxFQUF5REMsY0FBekQsRUFDckJDLE1BRHFCLEVBQ2JDLFVBRGEsQ0FBdEI7O0FBR0FpSSxnQkFBY0MsVUFBZDtBQUNBLEVBWkQ7O0FBY0EsUUFBTzlJLE1BQVA7QUFDQSxDQWpyQkYiLCJmaWxlIjoicXVpY2tfZWRpdC9tb2RhbHMvc2V0dGluZ3MuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNldHRpbmdzLmpzIDIwMTYtMTAtMTlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIEhhbmRsZXMgdGhlIHNldHRpbmdzIG1vZGFsLlxuICpcbiAqIEl0IHJldHJpZXZlcyB0aGUgc2V0dGluZ3MgZGF0YSB2aWEgdGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlIGFuZCBzZXRzIHRoZSB2YWx1ZXMuXG4gKiBZb3UgYXJlIGFibGUgdG8gY2hhbmdlIHRoZSBjb2x1bW4gc29ydCBvcmRlciBhbmQgdGhlIHZpc2liaWxpdHkgb2YgZWFjaCBjb2x1bW4uIEFkZGl0aW9uYWxseVxuICogeW91IGNhbiBjaGFuZ2UgdGhlIGhlaWdodCBvZiB0aGUgdGFibGUgcm93cy5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnc2V0dGluZ3MnLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5jc3NgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5qc2AsXG5cdFx0J3VzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlJyxcblx0XHQnbG9hZGluZ19zcGlubmVyJ1xuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsYXNzIHJlcHJlc2VudGluZyBhIGNvbnRyb2xsZXIgZm9yIHRoZSBRdWlja0VkaXQgb3ZlcnZpZXcgc2V0dGluZ3MgbW9kYWwuXG5cdFx0ICovXG5cdFx0Y2xhc3MgU2V0dGluZ3NNb2RhbENvbnRyb2xsZXIge1xuXHRcdFx0LyoqXG5cdFx0XHQgKiBDcmVhdGVzIGFuIGluc3RhbmNlIG9mIFF1aWNrRWRpdE92ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXIuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHBhcmFtIHtGdW5jdGlvbn0gIGRvbmUgICAgICAgICAgICBNb2R1bGUgZmluaXNoIGNhbGxiYWNrIGZ1bmN0aW9uLlxuXHRcdFx0ICogQHBhcmFtIHtqUXVlcnl9ICAgICRlbGVtZW50ICAgICAgICBNb2R1bGUgZWxlbWVudC5cblx0XHRcdCAqIEBwYXJhbSB7T2JqZWN0fSAgICB1c2VyQ2ZnU2VydmljZSAgVXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UgbGlicmFyeS5cblx0XHRcdCAqIEBwYXJhbSB7T2JqZWN0fSAgICBsb2FkaW5nU3Bpbm5lciAgTG9hZGluZyBzcGlubmVyIGxpYnJhcnkuXG5cdFx0XHQgKiBAcGFyYW0ge051bWJlcn0gICAgdXNlcklkICAgICAgICAgIElEIG9mIGN1cnJlbnRseSBzaWduZWQgaW4gdXNlci5cblx0XHRcdCAqIEBwYXJhbSB7T2JqZWN0fSAgICB0cmFuc2xhdG9yICAgICAgVHJhbnNsYXRvciBsaWJyYXJ5LlxuXHRcdFx0ICovXG5cdFx0XHRjb25zdHJ1Y3Rvcihkb25lLCAkZWxlbWVudCwgdXNlckNmZ1NlcnZpY2UsIGxvYWRpbmdTcGlubmVyLCB1c2VySWQsIHRyYW5zbGF0b3IpIHtcblx0XHRcdFx0Ly8gRWxlbWVudHNcblx0XHRcdFx0dGhpcy4kZWxlbWVudCA9ICRlbGVtZW50O1xuXHRcdFx0XHR0aGlzLiRzdWJtaXRCdXR0b24gPSAkZWxlbWVudC5maW5kKCdidXR0b24uc3VibWl0LWJ1dHRvbicpO1xuXHRcdFx0XHR0aGlzLiRzZXR0aW5ncyA9ICRlbGVtZW50LmZpbmQoJ3VsLnNldHRpbmdzJyk7XG5cdFx0XHRcdHRoaXMuJG1vZGFsID0gJGVsZW1lbnQucGFyZW50cygnLm1vZGFsJyk7XG5cdFx0XHRcdHRoaXMuJG1vZGFsRm9vdGVyID0gJGVsZW1lbnQuZmluZCgnLm1vZGFsLWZvb3RlcicpO1xuXHRcdFx0XHR0aGlzLiRyZXNldERlZmF1bHRMaW5rID0gJGVsZW1lbnQuZmluZCgnYS5yZXNldC1hY3Rpb24nKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIExvYWRpbmcgc3Bpbm5lclxuXHRcdFx0XHR0aGlzLiRzcGlubmVyID0gbnVsbDtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFNlbGVjdG9yIHN0cmluZ3Ncblx0XHRcdFx0dGhpcy5zb3J0YWJsZUhhbmRsZVNlbGVjdG9yID0gJ3NwYW4uc29ydC1oYW5kbGUnO1xuXHRcdFx0XHR0aGlzLnJvd0hlaWdodFZhbHVlU2VsZWN0b3IgPSAnc2VsZWN0I3NldHRpbmctdmFsdWUtcm93LWhlaWdodCc7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBDbGFzcyBuYW1lc1xuXHRcdFx0XHR0aGlzLmVycm9yTWVzc2FnZUNsYXNzTmFtZSA9ICdlcnJvci1tZXNzYWdlJztcblx0XHRcdFx0dGhpcy5sb2FkaW5nQ2xhc3NOYW1lID0gJ2xvYWRpbmcnO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gTGlicmFyaWVzXG5cdFx0XHRcdHRoaXMudXNlckNmZ1NlcnZpY2UgPSB1c2VyQ2ZnU2VydmljZTtcblx0XHRcdFx0dGhpcy5sb2FkaW5nU3Bpbm5lciA9IGxvYWRpbmdTcGlubmVyO1xuXHRcdFx0XHR0aGlzLnRyYW5zbGF0b3IgPSB0cmFuc2xhdG9yO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUHJlZml4ZXNcblx0XHRcdFx0dGhpcy5zZXR0aW5nTGlzdEl0ZW1JZFByZWZpeCA9ICdzZXR0aW5nLSc7XG5cdFx0XHRcdHRoaXMuc2V0dGluZ1ZhbHVlSWRQcmVmaXggPSAnc2V0dGluZy12YWx1ZS0nO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gVXNlciBjb25maWd1cmF0aW9uIGtleXNcblx0XHRcdFx0dGhpcy5DT05GSUdfS0VZX0NPTFVNTl9TRVRUSU5HUyA9ICdxdWlja0VkaXRPdmVydmlld1NldHRpbmdzQ29sdW1ucyc7XG5cdFx0XHRcdHRoaXMuQ09ORklHX0tFWV9ST1dfSEVJR0hUX1NFVFRJTkdTID0gJ3F1aWNrRWRpdE92ZXJ2aWV3U2V0dGluZ3NSb3dIZWlnaHQnO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRGVmYXVsdCB2YWx1ZXNcblx0XHRcdFx0dGhpcy5ERUZBVUxUX1JPV19IRUlHSFRfU0VUVElORyA9ICdsYXJnZSc7XG5cdFx0XHRcdHRoaXMuREVGQVVMVF9DT0xVTU5fU0VUVElOR1MgPSBbXG5cdFx0XHRcdFx0J2NhdGVnb3J5Jyxcblx0XHRcdFx0XHQnbmFtZScsXG5cdFx0XHRcdFx0J21vZGVsJyxcblx0XHRcdFx0XHQncXVhbnRpdHknLFxuXHRcdFx0XHRcdCdwcmljZScsXG5cdFx0XHRcdFx0J2Rpc2NvdW50Jyxcblx0XHRcdFx0XHQnc3BlY2lhbFByaWNlJyxcblx0XHRcdFx0XHQndGF4Jyxcblx0XHRcdFx0XHQnc2hpcHBpbmdTdGF0dXNOYW1lJyxcblx0XHRcdFx0XHQnd2VpZ2h0Jyxcblx0XHRcdFx0XHQnc2hpcHBpbmdDb3N0cycsXG5cdFx0XHRcdFx0J3N0YXR1cydcblx0XHRcdFx0XTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIElEIG9mIGN1cnJlbnRseSBzaWduZWQgaW4gdXNlci5cblx0XHRcdFx0dGhpcy51c2VySWQgPSB1c2VySWQ7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBDYWxsIG1vZHVsZSBmaW5pc2ggY2FsbGJhY2suXG5cdFx0XHRcdGRvbmUoKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBCaW5kcyB0aGUgZXZlbnQgaGFuZGxlcnMuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHRcdCAqL1xuXHRcdFx0aW5pdGlhbGl6ZSgpIHtcblx0XHRcdFx0Ly8gQXR0YWNoIGV2ZW50IGhhbmRsZXIgZm9yIGNsaWNrIGFjdGlvbiBvbiB0aGUgc3VibWl0IGJ1dHRvbi5cblx0XHRcdFx0dGhpcy4kc3VibWl0QnV0dG9uLm9uKCdjbGljaycsIGV2ZW50ID0+IHRoaXMuX29uU3VibWl0QnV0dG9uQ2xpY2soKSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBdHRhY2ggZXZlbnQgaGFuZGxlciBmb3IgY2xpY2sgYWN0aW9uIG9uIHRoZSByZXNldC1kZWZhdWx0IGxpbmsuXG5cdFx0XHRcdHRoaXMuJHJlc2V0RGVmYXVsdExpbmsub24oJ2NsaWNrJywgZXZlbnQgPT4gdGhpcy5fb25SZXNldFNldHRpbmdzTGlua0NsaWNrKGV2ZW50KSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBdHRhY2ggZXZlbnQgaGFuZGxlcnMgdG8gbW9kYWwuXG5cdFx0XHRcdHRoaXMuJG1vZGFsXG5cdFx0XHRcdFx0Lm9uKCdzaG93LmJzLm1vZGFsJywgZXZlbnQgPT4gdGhpcy5fb25Nb2RhbFNob3coKSlcblx0XHRcdFx0XHQub24oJ3Nob3duLmJzLm1vZGFsJywgZXZlbnQgPT4gdGhpcy5fb25Nb2RhbFNob3duKCkpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmFkZXMgb3V0IHRoZSBtb2RhbCBjb250ZW50LlxuXHRcdFx0ICpcblx0XHRcdCAqIEByZXR1cm4ge1NldHRpbmdzTW9kYWxDb250cm9sbGVyfSBTYW1lIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHByaXZhdGVcblx0XHRcdCAqL1xuXHRcdFx0X29uTW9kYWxTaG93KCkge1xuXHRcdFx0XHR0aGlzLiRlbGVtZW50LmFkZENsYXNzKHRoaXMubG9hZGluZ0NsYXNzTmFtZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gdGhpcztcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBVcGRhdGVzIHRoZSBzZXR0aW5ncywgY2xlYXJzIGFueSBlcnJvciBtZXNzYWdlcyBhbmQgaW5pdGlhbGl6ZXMgdGhlIHNvcnRhYmxlIHBsdWdpbi5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcmV0dXJuIHtTZXR0aW5nc01vZGFsQ29udHJvbGxlcn0gU2FtZSBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwcml2YXRlXG5cdFx0XHQgKi9cblx0XHRcdF9vbk1vZGFsU2hvd24oKSB7XG5cdFx0XHRcdHRoaXNcblx0XHRcdFx0XHQuX3JlZnJlc2hTZXR0aW5ncygpXG5cdFx0XHRcdFx0Ll9jbGVhckVycm9yTWVzc2FnZSgpXG5cdFx0XHRcdFx0Ll9pbml0U29ydGFibGUoKTtcblx0XHRcdFx0XG5cdFx0XHRcdHJldHVybiB0aGlzO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEFjdGl2YXRlcyB0aGUgalF1ZXJ5IFVJIFNvcnRhYmxlIHBsdWdpbiBvbiB0aGUgc2V0dGluZyBsaXN0IGl0ZW1zIGVsZW1lbnQuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHRfaW5pdFNvcnRhYmxlKCkge1xuXHRcdFx0XHQvLyBqUXVlcnkgVUkgU29ydGFibGUgcGx1Z2luIG9wdGlvbnMuXG5cdFx0XHRcdGNvbnN0IG9wdGlvbnMgPSB7XG5cdFx0XHRcdFx0aXRlbXM6ICc+IGxpJyxcblx0XHRcdFx0XHRheGlzOiAneScsXG5cdFx0XHRcdFx0Y3Vyc29yOiAnbW92ZScsXG5cdFx0XHRcdFx0aGFuZGxlOiB0aGlzLnNvcnRhYmxlSGFuZGxlU2VsZWN0b3IsXG5cdFx0XHRcdFx0Y29udGFpbm1lbnQ6ICdwYXJlbnQnXG5cdFx0XHRcdH07XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBY3RpdmF0ZSBzb3J0YWJsZSBwbHVnaW4uXG5cdFx0XHRcdHRoaXMuJHNldHRpbmdzXG5cdFx0XHRcdFx0LnNvcnRhYmxlKG9wdGlvbnMpXG5cdFx0XHRcdFx0LmRpc2FibGVTZWxlY3Rpb24oKTtcblx0XHRcdFx0XG5cdFx0XHRcdHJldHVybiB0aGlzO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFJldHVybnMgYSBzb3J0ZWQgYXJyYXkgY29udGFpbmluZyB0aGUgSURzIG9mIGFsbCBhY3RpdmF0ZWQgc2V0dGluZ3MuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7QXJyYXl9XG5cdFx0XHQgKlxuXHRcdFx0ICogQHByaXZhdGVcblx0XHRcdCAqL1xuXHRcdFx0X3NlcmlhbGl6ZUNvbHVtblNldHRpbmdzKCkge1xuXHRcdFx0XHQvLyBNYXAgaXRlcmF0b3IgZnVuY3Rpb24gdG8gcmVtb3ZlIHRoZSAnc2V0dGluZy0nIHByZWZpeCBmcm9tIGxpc3QgaXRlbSBJRC5cblx0XHRcdFx0Y29uc3QgcmVtb3ZlUHJlZml4SXRlcmF0b3IgPSBpdGVtID0+IGl0ZW0ucmVwbGFjZSh0aGlzLnNldHRpbmdMaXN0SXRlbUlkUHJlZml4LCAnJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBGaWx0ZXIgaXRlcmF0b3IgZnVuY3Rpb24sIHRvIGFjY2VwdCBvbmx5IGxpc3QgaXRlbXMgd2l0aCBhY3RpdmF0ZWQgY2hlY2tib3hlcy5cblx0XHRcdFx0Y29uc3QgZmlsdGVySXRlcmF0b3IgPSBpdGVtID0+IHRoaXMuJHNldHRpbmdzLmZpbmQoJyMnICsgdGhpcy5zZXR0aW5nVmFsdWVJZFByZWZpeCArIGl0ZW0pXG5cdFx0XHRcdFx0LmlzKCc6Y2hlY2tlZCcpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUmV0dXJuIGFycmF5IHdpdGggc29ydGVkLCBvbmx5IGFjdGl2ZSBjb2x1bW5zLlxuXHRcdFx0XHRyZXR1cm4gdGhpcy4kc2V0dGluZ3Ncblx0XHRcdFx0XHQuc29ydGFibGUoJ3RvQXJyYXknKVxuXHRcdFx0XHRcdC5tYXAocmVtb3ZlUHJlZml4SXRlcmF0b3IpXG5cdFx0XHRcdFx0LmZpbHRlcihmaWx0ZXJJdGVyYXRvcik7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogUmV0dXJucyB0aGUgdmFsdWUgb2YgdGhlIHNlbGVjdGVkIHJvdyBoZWlnaHQgb3B0aW9uLlxuXHRcdFx0ICpcblx0XHRcdCAqIEByZXR1cm4ge1N0cmluZ31cblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHRfc2VyaWFsaXplUm93SGVpZ2h0U2V0dGluZygpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXNcblx0XHRcdFx0XHQuJGVsZW1lbnRcblx0XHRcdFx0XHQuZmluZCh0aGlzLnJvd0hlaWdodFZhbHVlU2VsZWN0b3IpXG5cdFx0XHRcdFx0LnZhbCgpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFNob3dzIHRoZSBsb2FkaW5nIHNwaW5uZXIsIHNhdmVzIHRoZSBzZXR0aW5ncyB0byB0aGUgdXNlciBjb25maWd1cmF0aW9uLFxuXHRcdFx0ICogY2xvc2VzIHRoZSBtb2RhbCB0byBmaW5hbGx5IHJlLXJlbmRlciB0aGUgZGF0YXRhYmxlLlxuXHRcdFx0ICpcblx0XHRcdCAqIEByZXR1cm4ge1NldHRpbmdzTW9kYWxDb250cm9sbGVyfSBTYW1lIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHByaXZhdGVcblx0XHRcdCAqL1xuXHRcdFx0X29uU3VibWl0QnV0dG9uQ2xpY2soKSB7XG5cdFx0XHRcdC8vIFJldHJpZXZlIHNldHRpbmcgdmFsdWVzLlxuXHRcdFx0XHRjb25zdCBjb2x1bW5TZXR0aW5ncyA9IHRoaXMuX3NlcmlhbGl6ZUNvbHVtblNldHRpbmdzKCk7XG5cdFx0XHRcdGNvbnN0IHJvd0hlaWdodFNldHRpbmcgPSB0aGlzLl9zZXJpYWxpemVSb3dIZWlnaHRTZXR0aW5nKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBSZW1vdmUgYW55IGVycm9yIG1lc3NhZ2UgYW5kIHNhdmUgc2V0dGluZ3MuXG5cdFx0XHRcdHRoaXNcblx0XHRcdFx0XHQuX3RvZ2dsZUxvYWRpbmdTcGlubmVyKHRydWUpXG5cdFx0XHRcdFx0Ll9jbGVhckVycm9yTWVzc2FnZSgpXG5cdFx0XHRcdFx0Ll9zYXZlQ29sdW1uU2V0dGluZ3MoY29sdW1uU2V0dGluZ3MpXG5cdFx0XHRcdFx0LnRoZW4oKCkgPT4gdGhpcy5fc2F2ZVJvd0hlaWdodFNldHRpbmcocm93SGVpZ2h0U2V0dGluZykpXG5cdFx0XHRcdFx0LnRoZW4oKCkgPT4gdGhpcy5fb25TYXZlU3VjY2VzcygpKVxuXHRcdFx0XHRcdC5jYXRjaCgoKSA9PiB0aGlzLl9vblNhdmVFcnJvcigpKTtcblx0XHRcdFx0XG5cdFx0XHRcdHJldHVybiB0aGlzO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFByZXZlbnRzIHRoZSBicm93c2VyIHRvIGFwcGx5IHRoZSBkZWZhdWx0IGJlaGF2b2lyIGFuZFxuXHRcdFx0ICogcmVzZXRzIHRoZSBjb2x1bW4gb3JkZXIgYW5kIHJvdyBzaXplIHRvIHRoZSBkZWZhdWx0IHNldHRpbmcgdmFsdWVzLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBGaXJlZCBldmVudC5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcmV0dXJuIHtTZXR0aW5nc01vZGFsQ29udHJvbGxlcn0gU2FtZSBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwcml2YXRlXG5cdFx0XHQgKi9cblx0XHRcdF9vblJlc2V0U2V0dGluZ3NMaW5rQ2xpY2soZXZlbnQpIHtcblx0XHRcdFx0Ly8gUHJldmVudCBkZWZhdWx0IGJlaGF2aW9yLlxuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFJlc2V0IHRvIGRlZmF1bHQgc2V0dGluZ3MuXG5cdFx0XHRcdHRoaXMuX3NldERlZmF1bHRTZXR0aW5ncygpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogU2hvd3MgYW5kIGhpZGVzIHRoZSBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHBhcmFtIHtCb29sZWFufSBkb1Nob3cgU2hvdyB0aGUgbG9hZGluZyBzcGlubmVyP1xuXHRcdFx0ICpcblx0XHRcdCAqIEByZXR1cm4ge1NldHRpbmdzTW9kYWxDb250cm9sbGVyfSBTYW1lIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG5cdFx0XHQgKi9cblx0XHRcdF90b2dnbGVMb2FkaW5nU3Bpbm5lcihkb1Nob3cpIHtcblx0XHRcdFx0aWYgKGRvU2hvdykge1xuXHRcdFx0XHRcdC8vIEZhZGUgb3V0IG1vZGFsIGNvbnRlbnQuXG5cdFx0XHRcdFx0dGhpcy4kZWxlbWVudC5hZGRDbGFzcyh0aGlzLmxvYWRpbmdDbGFzc05hbWUpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFNob3cgbG9hZGluZyBzcGlubmVyLlxuXHRcdFx0XHRcdHRoaXMuJHNwaW5uZXIgPSB0aGlzLmxvYWRpbmdTcGlubmVyLnNob3codGhpcy4kZWxlbWVudCk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gRml4IHNwaW5uZXIgei1pbmRleC5cblx0XHRcdFx0XHR0aGlzLiRzcGlubmVyLmNzcyh7J3otaW5kZXgnOiA5OTk5fSk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0Ly8gRmFkZSBvdXQgbW9kYWwgY29udGVudC5cblx0XHRcdFx0XHR0aGlzLiRlbGVtZW50LnJlbW92ZUNsYXNzKHRoaXMubG9hZGluZ0NsYXNzTmFtZSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gSGlkZSB0aGUgbG9hZGluZyBzcGlubmVyLlxuXHRcdFx0XHRcdHRoaXMubG9hZGluZ1NwaW5uZXIuaGlkZSh0aGlzLiRzcGlubmVyKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogSGFuZGxlcyB0aGUgYmVoYXZpb3Igb24gc3VjY2Vzc2Z1bCBzZXR0aW5nIHNhdmUgYWN0aW9uLlxuXHRcdFx0ICpcblx0XHRcdCAqIEByZXR1cm4ge1NldHRpbmdzTW9kYWxDb250cm9sbGVyfSBTYW1lIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHByaXZhdGVcblx0XHRcdCAqL1xuXHRcdFx0X29uU2F2ZVN1Y2Nlc3MoKSB7XG5cdFx0XHRcdHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKTtcblx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogUmVtb3ZlcyBhbnkgZXJyb3IgbWVzc2FnZSwgaWYgZm91bmQuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHRfY2xlYXJFcnJvck1lc3NhZ2UoKSB7XG5cdFx0XHRcdC8vIEVycm9yIG1lc3NhZ2UuXG5cdFx0XHRcdGNvbnN0ICRlcnJvck1lc3NhZ2UgPSB0aGlzLiRtb2RhbEZvb3Rlci5maW5kKGAuJHt0aGlzLmVycm9yTWVzc2FnZUNsYXNzTmFtZX1gKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFJlbW92ZSBpZiBpdCBleGlzdHMuXG5cdFx0XHRcdGlmICgkZXJyb3JNZXNzYWdlLmxlbmd0aCkge1xuXHRcdFx0XHRcdCRlcnJvck1lc3NhZ2UucmVtb3ZlKCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdHJldHVybiB0aGlzO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEhhbmRsZXMgdGhlIGJlaGF2aW9yIG9uIHRocm93biBlcnJvciB3aGlsZSBzYXZpbmcgc2V0dGluZ3MuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHRfb25TYXZlRXJyb3IoKSB7XG5cdFx0XHRcdC8vIEVycm9yIG1lc3NhZ2UuXG5cdFx0XHRcdGNvbnN0IGVycm9yTWVzc2FnZSA9IHRoaXMudHJhbnNsYXRvci50cmFuc2xhdGUoJ1RYVF9TQVZFX0VSUk9SJywgJ2FkbWluX2dlbmVyYWwnKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIERlZmluZSBlcnJvciBtZXNzYWdlIGVsZW1lbnQuXG5cdFx0XHRcdGNvbnN0ICRlcnJvciA9ICQoJzxzcGFuLz4nLCB7Y2xhc3M6IHRoaXMuZXJyb3JNZXNzYWdlQ2xhc3NOYW1lLCB0ZXh0OiBlcnJvck1lc3NhZ2V9KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEhpZGUgdGhlIGxvYWRpbmcgc3Bpbm5lci5cblx0XHRcdFx0dGhpcy5fdG9nZ2xlTG9hZGluZ1NwaW5uZXIoZmFsc2UpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQWRkIGVycm9yIG1lc3NhZ2UgdG8gbW9kYWwgZm9vdGVyLlxuXHRcdFx0XHR0aGlzLiRtb2RhbEZvb3RlclxuXHRcdFx0XHRcdC5wcmVwZW5kKCRlcnJvcilcblx0XHRcdFx0XHQuaGlkZSgpXG5cdFx0XHRcdFx0LmZhZGVJbigpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogUmV0dXJucyB0aGUgY29uZmlndXJhdGlvbiB2YWx1ZSBmb3IgdGhlIGNvbHVtbiBzZXR0aW5ncy5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHRcdFx0ICpcblx0XHRcdCAqIEBwcml2YXRlXG5cdFx0XHQgKi9cblx0XHRcdF9nZXRDb2x1bW5TZXR0aW5ncygpIHtcblx0XHRcdFx0Ly8gQ29uZmlndXJhdGlvbiBkYXRhLlxuXHRcdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHRcdHVzZXJJZDogdGhpcy51c2VySWQsXG5cdFx0XHRcdFx0Y29uZmlndXJhdGlvbktleTogdGhpcy5DT05GSUdfS0VZX0NPTFVNTl9TRVRUSU5HU1xuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUmVxdWVzdCBkYXRhIGZyb20gdXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UuXG5cdFx0XHRcdHJldHVybiB0aGlzLl9nZXRGcm9tVXNlckNmZ1NlcnZpY2UoZGF0YSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogUmV0dXJucyB0aGUgY29uZmlndXJhdGlvbiB2YWx1ZSBmb3IgdGhlIHJvdyBoZWlnaHRzLlxuXHRcdFx0ICpcblx0XHRcdCAqIEByZXR1cm4ge1Byb21pc2V9XG5cdFx0XHQgKlxuXHRcdFx0ICogQHByaXZhdGVcblx0XHRcdCAqL1xuXHRcdFx0X2dldFJvd0hlaWdodFNldHRpbmcoKSB7XG5cdFx0XHRcdC8vIENvbmZpZ3VyYXRpb24gZGF0YS5cblx0XHRcdFx0Y29uc3QgZGF0YSA9IHtcblx0XHRcdFx0XHR1c2VySWQ6IHRoaXMudXNlcklkLFxuXHRcdFx0XHRcdGNvbmZpZ3VyYXRpb25LZXk6IHRoaXMuQ09ORklHX0tFWV9ST1dfSEVJR0hUX1NFVFRJTkdTXG5cdFx0XHRcdH07XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBSZXF1ZXN0IGRhdGEgZnJvbSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZS5cblx0XHRcdFx0cmV0dXJuIHRoaXMuX2dldEZyb21Vc2VyQ2ZnU2VydmljZShkYXRhKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBSZXR1cm5zIHRoZSB2YWx1ZSBmb3IgdGhlIHBhc3NlZCB1c2VyIGNvbmZpZ3VyYXRpb24gZGF0YS5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YSAgICAgICAgICAgICAgICAgICBVc2VyIGNvbmZpZ3VyYXRpb24gZGF0YS5cblx0XHRcdCAqIEBwYXJhbSB7TnVtYmVyfSBkYXRhLnVzZXJJZCAgICAgICAgICAgIFVzZXIgSUQuXG5cdFx0XHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YS5jb25maWd1cmF0aW9uS2V5ICBVc2VyIGNvbmZpZ3VyYXRpb24ga2V5LlxuXHRcdFx0ICpcblx0XHRcdCAqIEByZXR1cm4ge1Byb21pc2V9XG5cdFx0XHQgKlxuXHRcdFx0ICogQHByaXZhdGVcblx0XHRcdCAqL1xuXHRcdFx0X2dldEZyb21Vc2VyQ2ZnU2VydmljZShkYXRhKSB7XG5cdFx0XHRcdC8vIFByb21pc2UgaGFuZGxlci5cblx0XHRcdFx0Y29uc3QgaGFuZGxlciA9IChyZXNvbHZlLCByZWplY3QpID0+IHtcblx0XHRcdFx0XHQvLyBVc2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZSByZXF1ZXN0IG9wdGlvbnMuXG5cdFx0XHRcdFx0Y29uc3Qgb3B0aW9ucyA9IHtcblx0XHRcdFx0XHRcdG9uRXJyb3I6ICgpID0+IHJlamVjdCgpLFxuXHRcdFx0XHRcdFx0b25TdWNjZXNzOiByZXNwb25zZSA9PiByZXNvbHZlKHJlc3BvbnNlLmNvbmZpZ3VyYXRpb25WYWx1ZSksXG5cdFx0XHRcdFx0XHRkYXRhXG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBHZXQgY29uZmlndXJhdGlvbiB2YWx1ZS5cblx0XHRcdFx0XHR0aGlzLnVzZXJDZmdTZXJ2aWNlLmdldChvcHRpb25zKTtcblx0XHRcdFx0fTtcblx0XHRcdFx0XG5cdFx0XHRcdHJldHVybiBuZXcgUHJvbWlzZShoYW5kbGVyKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBTYXZlcyB0aGUgZGF0YSB2aWEgdGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBkYXRhICAgICAgICAgICAgICAgICAgICAgVXNlciBjb25maWd1cmF0aW9uIGRhdGEuXG5cdFx0XHQgKiBAcGFyYW0ge051bWJlcn0gZGF0YS51c2VySWQgICAgICAgICAgICAgIFVzZXIgSUQuXG5cdFx0XHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YS5jb25maWd1cmF0aW9uS2V5ICAgIFVzZXIgY29uZmlndXJhdGlvbiBrZXkuXG5cdFx0XHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YS5jb25maWd1cmF0aW9uVmFsdWUgIFVzZXIgY29uZmlndXJhdGlvbiB2YWx1ZS5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHRcdFx0ICpcblx0XHRcdCAqIEBwcml2YXRlXG5cdFx0XHQgKi9cblx0XHRcdF9zZXRXaXRoVXNlckNmZ1NlcnZpY2UoZGF0YSkge1xuXHRcdFx0XHQvLyBQcm9taXNlIGhhbmRsZXIuXG5cdFx0XHRcdGNvbnN0IGhhbmRsZXIgPSAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cdFx0XHRcdFx0Ly8gVXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UgcmVxdWVzdCBvcHRpb25zLlxuXHRcdFx0XHRcdGNvbnN0IG9wdGlvbnMgPSB7XG5cdFx0XHRcdFx0XHRvbkVycm9yOiAoKSA9PiByZWplY3QoKSxcblx0XHRcdFx0XHRcdG9uU3VjY2VzczogcmVzcG9uc2UgPT4gcmVzb2x2ZSgpLFxuXHRcdFx0XHRcdFx0ZGF0YVxuXHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gU2V0IGNvbmZpZ3VyYXRpb24gdmFsdWUuXG5cdFx0XHRcdFx0dGhpcy51c2VyQ2ZnU2VydmljZS5zZXQob3B0aW9ucyk7XG5cdFx0XHRcdH07XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gbmV3IFByb21pc2UoaGFuZGxlcik7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogU2F2ZXMgdGhlIGNvbHVtbiBzZXR0aW5ncyB2aWEgdGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwYXJhbSB7U3RyaW5nW119IGNvbHVtblNldHRpbmdzIFNvcnRlZCBhcnJheSB3aXRoIGFjdGl2ZSBjb2x1bW4uXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7UHJvbWlzZX1cblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHRfc2F2ZUNvbHVtblNldHRpbmdzKGNvbHVtblNldHRpbmdzKSB7XG5cdFx0XHRcdC8vIENoZWNrIGFyZ3VtZW50LlxuXHRcdFx0XHRpZiAoIWNvbHVtblNldHRpbmdzIHx8ICFBcnJheS5pc0FycmF5KGNvbHVtblNldHRpbmdzKSkge1xuXHRcdFx0XHRcdHRocm93IG5ldyBFcnJvcignTWlzc2luZyBvciBpbnZhbGlkIGNvbHVtbiBzZXR0aW5ncycpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBVc2VyIGNvbmZpZ3VyYXRpb24gcmVxdWVzdCBkYXRhLlxuXHRcdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHRcdHVzZXJJZDogdGhpcy51c2VySWQsXG5cdFx0XHRcdFx0Y29uZmlndXJhdGlvbktleTogdGhpcy5DT05GSUdfS0VZX0NPTFVNTl9TRVRUSU5HUyxcblx0XHRcdFx0XHRjb25maWd1cmF0aW9uVmFsdWU6IEpTT04uc3RyaW5naWZ5KGNvbHVtblNldHRpbmdzKVxuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2F2ZSB2aWEgdXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UuXG5cdFx0XHRcdHJldHVybiB0aGlzLl9zZXRXaXRoVXNlckNmZ1NlcnZpY2UoZGF0YSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogU2F2ZXMgdGhlIHJvdyBoZWlnaHQgc2V0dGluZyB2aWEgdGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwYXJhbSB7U3RyaW5nfSByb3dIZWlnaHRTZXR0aW5nIFZhbHVlIG9mIHRoZSBzZWxlY3RlZCByb3cgaGVpZ2h0IHNldHRpbmcuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7UHJvbWlzZX1cblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHRfc2F2ZVJvd0hlaWdodFNldHRpbmcocm93SGVpZ2h0U2V0dGluZykge1xuXHRcdFx0XHQvLyBDaGVjayBhcmd1bWVudC5cblx0XHRcdFx0aWYgKCFyb3dIZWlnaHRTZXR0aW5nIHx8IHR5cGVvZiByb3dIZWlnaHRTZXR0aW5nICE9PSAnc3RyaW5nJykge1xuXHRcdFx0XHRcdHRocm93IG5ldyBFcnJvcignTWlzc2luZyBvciBpbnZhbGlkIHJvdyBoZWlnaHQgc2V0dGluZycpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBVc2VyIGNvbmZpZ3VyYXRpb24gcmVxdWVzdCBkYXRhLlxuXHRcdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHRcdHVzZXJJZDogdGhpcy51c2VySWQsXG5cdFx0XHRcdFx0Y29uZmlndXJhdGlvbktleTogdGhpcy5DT05GSUdfS0VZX1JPV19IRUlHSFRfU0VUVElOR1MsXG5cdFx0XHRcdFx0Y29uZmlndXJhdGlvblZhbHVlOiByb3dIZWlnaHRTZXR0aW5nXG5cdFx0XHRcdH07XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTYXZlIHZpYSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZS5cblx0XHRcdFx0cmV0dXJuIHRoaXMuX3NldFdpdGhVc2VyQ2ZnU2VydmljZShkYXRhKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBSZXRyaWV2ZXMgdGhlIHNhdmVkIHNldHRpbmcgY29uZmlndXJhdGlvbiBhbmQgcmVvcmRlcnMvdXBkYXRlcyB0aGUgc2V0dGluZ3MuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHRfcmVmcmVzaFNldHRpbmdzKCkge1xuXHRcdFx0XHQvLyBTaG93IGxvYWRpbmcgc3Bpbm5lci5cblx0XHRcdFx0dGhpcy5fdG9nZ2xlTG9hZGluZ1NwaW5uZXIodHJ1ZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBFcnJvciBoYW5kbGVyIGZ1bmN0aW9uIHRvIHNwZWNpZnkgdGhlIGJlaGF2aW9yIG9uIGVycm9ycyB3aGlsZSBwcm9jZXNzaW5nLlxuXHRcdFx0XHRjb25zdCBvblJlZnJlc2hTZXR0aW5nc0Vycm9yID0gZXJyb3IgPT4ge1xuXHRcdFx0XHRcdC8vIE91dHB1dCB3YXJuaW5nLlxuXHRcdFx0XHRcdGNvbnNvbGUud2FybignRXJyb3Igd2hpbGUgcmVmcmVzaGluZycsIGVycm9yKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBIaWRlIHRoZSBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHRcdFx0dGhpcy5fdG9nZ2xlTG9hZGluZ1NwaW5uZXIoZmFsc2UpO1xuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUmVtb3ZlIGFueSBlcnJvciBtZXNzYWdlLCBzZXQgcm93IGhlaWdodCxcblx0XHRcdFx0Ly8gcmVvcmRlciBhbmQgdXBkYXRlIHRoZSBzZXR0aW5ncyBhbmQgaGlkZSB0aGUgbG9hZGluZyBzcGlubmVyLlxuXHRcdFx0XHR0aGlzXG5cdFx0XHRcdFx0Ll9jbGVhckVycm9yTWVzc2FnZSgpXG5cdFx0XHRcdFx0Ll9nZXRSb3dIZWlnaHRTZXR0aW5nKClcblx0XHRcdFx0XHQudGhlbihyb3dIZWlnaHRWYWx1ZSA9PiB0aGlzLl9zZXRSb3dIZWlnaHQocm93SGVpZ2h0VmFsdWUpKVxuXHRcdFx0XHRcdC50aGVuKCgpID0+IHRoaXMuX2dldENvbHVtblNldHRpbmdzKCkpXG5cdFx0XHRcdFx0LnRoZW4oY29sdW1uU2V0dGluZ3MgPT4gdGhpcy5fc2V0Q29sdW1uU2V0dGluZ3MoY29sdW1uU2V0dGluZ3MpKVxuXHRcdFx0XHRcdC50aGVuKCgpID0+IHRoaXMuX3RvZ2dsZUxvYWRpbmdTcGlubmVyKGZhbHNlKSlcblx0XHRcdFx0XHQuY2F0Y2gob25SZWZyZXNoU2V0dGluZ3NFcnJvcik7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gdGhpcztcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBTZXRzIHRoZSByb3cgaGVpZ2h0IHNldHRpbmcgdmFsdWUuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHBhcmFtIHtTdHJpbmd9IHZhbHVlIFJvdyBoZWlnaHQgdmFsdWUuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHJldHVybiB7U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHRfc2V0Um93SGVpZ2h0KHZhbHVlID0gdGhpcy5ERUZBVUxUX1JPV19IRUlHSFRfU0VUVElORykge1xuXHRcdFx0XHR0aGlzXG5cdFx0XHRcdFx0LiRlbGVtZW50XG5cdFx0XHRcdFx0LmZpbmQodGhpcy5yb3dIZWlnaHRWYWx1ZVNlbGVjdG9yKVxuXHRcdFx0XHRcdC52YWwodmFsdWUpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogUmVvcmRlcnMgYW5kIHVwZGF0ZXMgdGhlIGNvbHVtbiBzZXR0aW5nIHZhbHVlcy5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcGFyYW0ge1N0cmluZ3xBcnJheX0gY29sdW1uU2V0dGluZ3MgRW5jb2RlZCBKU09OIGFycmF5IGNvbnRhaW5pbmcgdGhlIHNhdmVkIGNvbHVtbiBzZXR0aW5ncy5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcmV0dXJuIHtTZXR0aW5nc01vZGFsQ29udHJvbGxlcn0gU2FtZSBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwcml2YXRlXG5cdFx0XHQgKi9cblx0XHRcdF9zZXRDb2x1bW5TZXR0aW5ncyhjb2x1bW5TZXR0aW5ncyA9IHRoaXMuREVGQVVMVF9DT0xVTU5fU0VUVElOR1MpIHtcblx0XHRcdFx0Ly8gUmVnZXggZm9yIGVzY2FwZSBjaGFyYWN0ZXIuXG5cdFx0XHRcdGNvbnN0IEVTQ0FQRV9DSEFSID0gL1xcXFwvZztcblx0XHRcdFx0XG5cdFx0XHRcdC8vIE5vIG5lZWQgdG8gcGFyc2UgZnJvbSBKU09OIG9uIGRlZmF1bHQgdmFsdWUgYXMgaXQgaXMgYW4gYXJyYXkuXG5cdFx0XHRcdGlmICghQXJyYXkuaXNBcnJheShjb2x1bW5TZXR0aW5ncykpIHtcblx0XHRcdFx0XHQvLyBSZW1vdmUgZXNjYXBlIGNoYXJhY3RlcnMgZnJvbSBhbmQgcGFyc2UgYXJyYXkgZnJvbSBKU09OLlxuXHRcdFx0XHRcdGNvbHVtblNldHRpbmdzID0gY29sdW1uU2V0dGluZ3MucmVwbGFjZShFU0NBUEVfQ0hBUiwgJycpO1xuXHRcdFx0XHRcdGNvbHVtblNldHRpbmdzID0gSlNPTi5wYXJzZShjb2x1bW5TZXR0aW5ncyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdC8vIENhY2hlIGNvbnRhaW5lciB0byB0ZW1wb3JhcmlseSBob2xkIGFsbCBhY3RpdmUgbGlzdCBpdGVtcyBpbiBzb3J0ZWQgb3JkZXIuXG5cdFx0XHRcdC8vIFRoZSBjaGlsZHJlbiBvZiB0aGlzIGVsZW1lbnQgd2lsbCBiZSBwcmVwZW5kZWQgdG8gdGhlIHNldHRpbmcgbGlzdCBpdGVtIGNvbnRhaW5lciB0byByZXRhaW4gdGhlIFxuXHRcdFx0XHQvLyBzb3J0aW5nIG9yZGVyLlxuXHRcdFx0XHRjb25zdCAkc29ydGVkSXRlbXMgPSAkKCc8ZGl2Lz4nKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEl0ZXJhdG9yIGZ1bmN0aW9uIHRvIHByZXBlbmQgYWN0aXZlIGxpc3QgaXRlbXMgdG8gdGhlIHRvcCBhbmQgYWN0aXZhdGUgdGhlIGNoZWNrYm94LlxuXHRcdFx0XHRjb25zdCBzZXR0aW5nSXRlcmF0b3IgPSBzZXR0aW5nID0+IHtcblx0XHRcdFx0XHQvLyBMaXN0IGl0ZW0gSUQuXG5cdFx0XHRcdFx0Y29uc3QgaWQgPSB0aGlzLnNldHRpbmdMaXN0SXRlbUlkUHJlZml4ICsgc2V0dGluZztcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBBZmZlY3RlZCBzZXR0aW5nIGxpc3QgaXRlbS5cblx0XHRcdFx0XHRjb25zdCAkbGlzdEl0ZW0gPSB0aGlzLiRzZXR0aW5ncy5maW5kKGAjJHtpZH1gKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBDaGVja2JveCBvZiBhZmZlY3RlZCBsaXN0IGl0ZW0uXG5cdFx0XHRcdFx0Y29uc3QgJGNoZWNrYm94ID0gJGxpc3RJdGVtLmZpbmQoJyMnICsgdGhpcy5zZXR0aW5nVmFsdWVJZFByZWZpeCArIHNldHRpbmcpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIEFjdGl2YXRlIGNoZWNrYm94LlxuXHRcdFx0XHRcdGlmICghJGNoZWNrYm94LmlzKCc6Y2hlY2tlZCcpKSB7XG5cdFx0XHRcdFx0XHQkY2hlY2tib3gucGFyZW50KCkudHJpZ2dlcignY2xpY2snKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gTW92ZSB0byBjYWNoZSBjb250YWluZXIuXG5cdFx0XHRcdFx0JGxpc3RJdGVtLmFwcGVuZFRvKCRzb3J0ZWRJdGVtcyk7XG5cdFx0XHRcdH07XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBNb3ZlIGFjdGl2ZSBsaXN0IGl0ZW1zIHRvIHRoZSB0b3AgYmVhcmluZyB0aGUgc29ydGluZyBvcmRlciBpbiBtaW5kLlxuXHRcdFx0XHRjb2x1bW5TZXR0aW5ncy5mb3JFYWNoKHNldHRpbmdJdGVyYXRvcik7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBQcmVwZW5kIGNhY2hlZCBlbGVtZW50cyB0byBpdGVtIGxpc3QuXG5cdFx0XHRcdCRzb3J0ZWRJdGVtc1xuXHRcdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdFx0LnByZXBlbmRUbyh0aGlzLiRzZXR0aW5ncyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gdGhpcztcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBSZXNldHMgdGhlIGNvbHVtbiBvcmRlciBhbmQgcm93IGhlaWdodCBzZXR0aW5ncyB0byB0aGUgZGVmYXVsdC5cblx0XHRcdCAqXG5cdFx0XHQgKiBAcmV0dXJuIHtTZXR0aW5nc01vZGFsQ29udHJvbGxlcn0gU2FtZSBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuXHRcdFx0ICpcblx0XHRcdCAqIEBwcml2YXRlXG5cdFx0XHQgKi9cblx0XHRcdF9zZXREZWZhdWx0U2V0dGluZ3MoKSB7XG5cdFx0XHRcdC8vIERlZmF1bHQgdmFsdWVzLlxuXHRcdFx0XHRjb25zdCBjb2x1bW5TZXR0aW5ncyA9IHRoaXMuREVGQVVMVF9DT0xVTU5fU0VUVElOR1M7XG5cdFx0XHRcdGNvbnN0IHJvd0hlaWdodCA9IHRoaXMuREVGQVVMVF9ST1dfSEVJR0hUX1NFVFRJTkc7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTZXQgY29sdW1uIHNldHRpbmdzLlxuXHRcdFx0XHQvLyBDYWNoZSBjb250YWluZXIgdG8gdGVtcG9yYXJpbHkgaG9sZCBhbGwgYWN0aXZlIGxpc3QgaXRlbXMgaW4gc29ydGVkIG9yZGVyLlxuXHRcdFx0XHQvLyBUaGUgY2hpbGRyZW4gb2YgdGhpcyBlbGVtZW50IHdpbGwgYmUgcHJlcGVuZGVkIHRvIHRoZSBzZXR0aW5nIGxpc3QgaXRlbSBjb250YWluZXIgdG8gcmV0YWluIHRoZSBcblx0XHRcdFx0Ly8gc29ydGluZyBvcmRlci5cblx0XHRcdFx0Y29uc3QgJHNvcnRlZEl0ZW1zID0gJCgnPGRpdi8+Jyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBJdGVyYXRvciBmdW5jdGlvbiB0byBwcmVwZW5kIGFjdGl2ZSBsaXN0IGl0ZW1zIHRvIHRoZSB0b3AgYW5kIGFjdGl2YXRlIHRoZSBjaGVja2JveC5cblx0XHRcdFx0Y29uc3Qgc2V0dGluZ0l0ZXJhdG9yID0gc2V0dGluZyA9PiB7XG5cdFx0XHRcdFx0Ly8gTGlzdCBpdGVtIElELlxuXHRcdFx0XHRcdGNvbnN0IGlkID0gdGhpcy5zZXR0aW5nTGlzdEl0ZW1JZFByZWZpeCArIHNldHRpbmc7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gQWZmZWN0ZWQgc2V0dGluZyBsaXN0IGl0ZW0uXG5cdFx0XHRcdFx0Y29uc3QgJGxpc3RJdGVtID0gdGhpcy4kc2V0dGluZ3MuZmluZChgIyR7aWR9YCk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gQ2hlY2tib3ggb2YgYWZmZWN0ZWQgbGlzdCBpdGVtLlxuXHRcdFx0XHRcdGNvbnN0ICRjaGVja2JveCA9ICRsaXN0SXRlbS5maW5kKCcjJyArIHRoaXMuc2V0dGluZ1ZhbHVlSWRQcmVmaXggKyBzZXR0aW5nKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBBY3RpdmF0ZSBjaGVja2JveC5cblx0XHRcdFx0XHRpZiAoISRjaGVja2JveC5pcygnOmNoZWNrZWQnKSkge1xuXHRcdFx0XHRcdFx0JGNoZWNrYm94LnBhcmVudCgpLnRyaWdnZXIoJ2NsaWNrJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIE1vdmUgdG8gY2FjaGUgY29udGFpbmVyLlxuXHRcdFx0XHRcdCRsaXN0SXRlbS5hcHBlbmRUbygkc29ydGVkSXRlbXMpO1xuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRGVhY3RpdmF0ZSBhbGwgY2hlY2tib3hlcy5cblx0XHRcdFx0dGhpc1xuXHRcdFx0XHRcdC4kc2V0dGluZ3Ncblx0XHRcdFx0XHQuZmluZCgnOmNoZWNrYm94Jylcblx0XHRcdFx0XHQuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcblx0XHRcdFx0XHRcdGNvbnN0ICRjaGVja2JveCA9ICQoZWxlbWVudCk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGlmICgkY2hlY2tib3guaXMoJzpjaGVja2VkJykpIHtcblx0XHRcdFx0XHRcdFx0JGNoZWNrYm94LnBhcmVudCgpLnRyaWdnZXIoJ2NsaWNrJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBNb3ZlIGFjdGl2ZSBsaXN0IGl0ZW1zIHRvIHRoZSB0b3AgYmVhcmluZyB0aGUgc29ydGluZyBvcmRlciBpbiBtaW5kLlxuXHRcdFx0XHRjb2x1bW5TZXR0aW5ncy5mb3JFYWNoKHNldHRpbmdJdGVyYXRvcik7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBQcmVwZW5kIGNhY2hlZCBlbGVtZW50cyB0byBpdGVtIGxpc3QuXG5cdFx0XHRcdCRzb3J0ZWRJdGVtc1xuXHRcdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdFx0LnByZXBlbmRUbyh0aGlzLiRzZXR0aW5ncyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTZXQgcm93IGhlaWdodC5cblx0XHRcdFx0dGhpc1xuXHRcdFx0XHRcdC4kZWxlbWVudFxuXHRcdFx0XHRcdC5maW5kKHRoaXMucm93SGVpZ2h0VmFsdWVTZWxlY3Rvcilcblx0XHRcdFx0XHQudmFsKHJvd0hlaWdodCk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gdGhpcztcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBEZXBlbmRlbmNpZXMuXG5cdFx0XHRjb25zdCB1c2VyQ2ZnU2VydmljZSA9IGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlO1xuXHRcdFx0Y29uc3QgbG9hZGluZ1NwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXI7XG5cdFx0XHRjb25zdCB1c2VySWQgPSBkYXRhLnVzZXJJZDtcblx0XHRcdGNvbnN0IHRyYW5zbGF0b3IgPSBqc2UuY29yZS5sYW5nO1xuXHRcdFx0XG5cdFx0XHQvLyBDcmVhdGUgYSBuZXcgaW5zdGFuY2UgYW5kIGxvYWQgc2V0dGluZ3MuXG5cdFx0XHRjb25zdCBzZXR0aW5nc01vZGFsID0gbmV3IFNldHRpbmdzTW9kYWxDb250cm9sbGVyKGRvbmUsICR0aGlzLCB1c2VyQ2ZnU2VydmljZSwgbG9hZGluZ1NwaW5uZXIsXG5cdFx0XHRcdHVzZXJJZCwgdHJhbnNsYXRvcik7XG5cdFx0XHRcblx0XHRcdHNldHRpbmdzTW9kYWwuaW5pdGlhbGl6ZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
