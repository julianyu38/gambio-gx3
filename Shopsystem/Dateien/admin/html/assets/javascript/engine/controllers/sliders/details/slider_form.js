'use strict';

/* --------------------------------------------------------------
 slider_form.js 2016-12-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Slider Edit Form
 *
 * Handles the sliders details form operations.
 */
gx.controllers.module('slider_form', ['xhr', 'modal', 'loading_spinner'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Master data panel selector.
  *
  * @type {jQuery}
  */
	var $masterDataPanel = $this.find('.panel.master-data');

	/**
  * Slide panel containers (each language holds a container, that contains the slide panels).
  *
  * @type {jQuery}
  */
	var $tabContents = $this.find('.tab-pane');

	/**
  * Deleter select box.
  *
  * @type {jQuery}
  */
	var $imageDeleteSelect = $this.find('#delete_images');

	/**
  * Spinner Selector
  *
  * @type {jQuery}
  */
	var $spinner = null;

	/**
  * Do a refresh instead of redirecting?
  *
  * @type {Boolean}
  */
	var doRefresh = false;

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {};

	/**
  * Final Options
  *
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Handles the form submit event.
  *
  * @param {jQuery.Event} event Triggered event.
  * @param {Object} data Event data.
  */
	function _onFormSubmit(event, data) {
		// Prevent the submit of the form.
		event.preventDefault();

		// Check refresh parameter.
		if (data && data.refresh) {
			doRefresh = true;
		}

		// Show loading spinner.
		$spinner = jse.libs.loading_spinner.show($this);

		// Upload files.
		_uploadFiles().then(_deleteFiles, _showFailMessage).then(_performSubmit, _showFailMessage).then(function () {
			return jse.libs.loading_spinner.hide($spinner);
		});
	}

	/**
  * Shows the submit error message modal.
  */
	function _showFailMessage() {
		// Message texts.
		var errorTitle = jse.core.lang.translate('FORM_SUBMIT_ERROR_MODAL_TITLE', 'sliders');
		var errorMessage = jse.core.lang.translate('FORM_SUBMIT_ERROR_MODAL_TEXT', 'sliders');

		// Show modal.
		jse.libs.modal.showMessage(errorTitle, errorMessage);

		// Hide loading spinner.
		if ($spinner) {
			jse.libs.loading_spinner.hide($spinner);
		}
	}

	/**
  * Performs the form submit AJAX request.
  */
	function _performSubmit() {
		// AJAX request options.
		var requestOptions = {
			url: $this.attr('action'),
			data: _getFormData()
		};

		jse.libs.xhr.post(requestOptions).done(function (response) {
			// URL to redirect to.
			var url = doRefresh ? 'admin.php?do=SlidersDetails&id=' + response.id : options.redirectUrl;

			// Prevent that the page unload prompt is displayed on save action.
			window.onbeforeunload = null;

			// Open overview page.
			window.open(url, '_self');
		}).fail(_showFailMessage);
	}

	/**
  * Returns the gathered form data.
  *
  * @return {Object} Form data.
  */
	function _getFormData() {
		// Form data object.
		var data = {
			id: $this.data('id')
		};

		// Extend form data object with all necessary properties.
		return $.extend(true, data, _getMasterData(), _getSlidesData());
	}

	/**
  * Returns the slider's master data.
  *
  * @return {Object} Slider master data.
  */
	function _getMasterData() {
		var name = $masterDataPanel.find('input[name="name"]').val();

		var speed = $masterDataPanel.find('input[name="speed"]').val();

		return { name: name, speed: speed };
	}

	/**
  * Returns the slides data by iterating over the tab content elements
  * which represent a container for each language.
  *
  * The returned object contains a property for each language.
  * The key is the language code and the value is an array containing
  * all slides information as collection.
  *
  * Example output:
  * {
  *   de: [{
  *     id: 1,
  *     thumbnail: 'My picture',
  *     ...
  *   }]
  * }
  *
  * @return {Object} Slides data.
  */
	function _getSlidesData() {
		// Slides data object.
		var slides = {};

		// Iterate over each slider panel container.
		$tabContents.each(function (index, element) {
			// Slide panel container element.
			var $slidePanelContainer = $(element);

			// Slide panel elements.
			var $slidePanels = $slidePanelContainer.find('.panel');

			// Get slide panel container language code.
			var languageCode = $slidePanelContainer.data('language');

			// Add property to slides data object,
			// which contains the language code as key and the slides data as value.
			slides[languageCode] = $.map($slidePanels, function (element) {
				return _getSlideData(element);
			});
		});

		return { slides: slides };
	}

	/**
  * Returns the data for a slide.
  *
  * @param {HTMLElement} slidePanel Slide panel element.
  *
  * @return {Object} Slide data.
  */
	function _getSlideData(slidePanel) {
		var $element = $(slidePanel);

		return {
			id: $element.data('id'),
			title: $element.find('input[name="title"]').val(),
			alt_text: $element.find('input[name="alt_text"]').val(),
			thumbnail: $element.find('select[name="thumbnail"]').val(),
			link: $element.find('input[name="link"]').val(),
			link_target: $element.find('select[name="link_target"]').val(),
			images: _getSlideImagesData(slidePanel)
		};
	}

	/**
  * Returns the slide images data.
  *
  * @param {HTMLElement} slidePanel Slide panel element.
  *
  * @return {Object} Slide images data.
  */
	function _getSlideImagesData(slidePanel) {
		// Image dropdown container elements (without thumbnail).
		var $imageDropdownContainers = $(slidePanel).find('.row.form-group').filter(function (index, element) {
			var $dropdown = $(element).find('.dropdown-input');

			return $dropdown.length && !$dropdown.is('[name="thumbnail"]');
		});

		// Iterate over each dropdown element and retrieve the data.
		return $.map($imageDropdownContainers, function (element) {
			// Dropdown container element.
			var $element = $(element);

			// Dropdown element.
			var $dropdown = $element.find('.dropdown-input');

			// Image data object.
			return {
				id: $element.data('id'),
				breakpoint: $dropdown.attr('name').replace('breakpoint_', ''),
				image: $dropdown.val(),
				areas: _getSlideImageAreaData(element)
			};
		});
	}

	/**
  * Returns the slide image area data.
  *
  * @param {HTMLElement} slideImageContainer Slide image configuration row element.
  *
  * @return {Object} Slide image area data.
  */
	function _getSlideImageAreaData(slideImageContainer) {
		// Slide image area data container list items.
		var $listItems = $(slideImageContainer).find('.image-map-data-list').children();

		// Iterate over each dropdown list item element and retrieve the data.
		return $.map($listItems, function (element) {
			// List item element.
			var $element = $(element);

			return {
				id: $element.data('id'),
				linkTitle: $element.data('linkTitle'),
				linkUrl: $element.data('linkUrl'),
				linkTarget: $element.data('linkTarget'),
				coordinates: $element.data('coordinates')
			};
		});
	}

	/**
  * Performs the images upload AJAX request.
  *
  * @return {jQuery.jqXHR} jQuery deferred object.
  */
	function _uploadFiles() {
		// Form data object.
		var formData = new FormData();

		// File inputs.
		var $fileInputs = $this.find(':file');

		// Append files to form data object.
		$fileInputs.each(function (index, element) {
			// File.
			var file = element.files[0];

			// Data entry key.
			var key = element.id + '[]';

			// Append file to form data object.
			if (file) {
				formData.append(key, file);
			}
		});

		// AJAX request options.
		var requestOptions = {
			url: options.imagesUploadUrl,
			data: formData,
			processData: false,
			contentType: false
		};

		// Perform AJAX request.
		return jse.libs.xhr.post(requestOptions);
	}

	/**
  * Performs the image deletion AJAX request.
  */
	function _deleteFiles() {
		// List of image file names.
		var fileNames = [];

		// List of thumbnail images.
		var thumbnailNames = [];

		// Fill the file names list.
		$imageDeleteSelect.children().each(function (index, element) {
			element.className === 'thumbnail' ? thumbnailNames.push(element.value) : fileNames.push(element.value);
		});

		// AJAX request options.
		var requestOptions = {
			url: options.imagesDeleteUrl,
			data: { file_names: fileNames, thumbnail_names: thumbnailNames }
		};

		return jse.libs.xhr.post(requestOptions);
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Listen to form submit event.
		$this.on('submit', _onFormSubmit);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNsaWRlcnMvZGV0YWlscy9zbGlkZXJfZm9ybS5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRtYXN0ZXJEYXRhUGFuZWwiLCJmaW5kIiwiJHRhYkNvbnRlbnRzIiwiJGltYWdlRGVsZXRlU2VsZWN0IiwiJHNwaW5uZXIiLCJkb1JlZnJlc2giLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfb25Gb3JtU3VibWl0IiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsInJlZnJlc2giLCJqc2UiLCJsaWJzIiwibG9hZGluZ19zcGlubmVyIiwic2hvdyIsIl91cGxvYWRGaWxlcyIsInRoZW4iLCJfZGVsZXRlRmlsZXMiLCJfc2hvd0ZhaWxNZXNzYWdlIiwiX3BlcmZvcm1TdWJtaXQiLCJoaWRlIiwiZXJyb3JUaXRsZSIsImNvcmUiLCJsYW5nIiwidHJhbnNsYXRlIiwiZXJyb3JNZXNzYWdlIiwibW9kYWwiLCJzaG93TWVzc2FnZSIsInJlcXVlc3RPcHRpb25zIiwidXJsIiwiYXR0ciIsIl9nZXRGb3JtRGF0YSIsInhociIsInBvc3QiLCJkb25lIiwicmVzcG9uc2UiLCJpZCIsInJlZGlyZWN0VXJsIiwid2luZG93Iiwib25iZWZvcmV1bmxvYWQiLCJvcGVuIiwiZmFpbCIsIl9nZXRNYXN0ZXJEYXRhIiwiX2dldFNsaWRlc0RhdGEiLCJuYW1lIiwidmFsIiwic3BlZWQiLCJzbGlkZXMiLCJlYWNoIiwiaW5kZXgiLCJlbGVtZW50IiwiJHNsaWRlUGFuZWxDb250YWluZXIiLCIkc2xpZGVQYW5lbHMiLCJsYW5ndWFnZUNvZGUiLCJtYXAiLCJfZ2V0U2xpZGVEYXRhIiwic2xpZGVQYW5lbCIsIiRlbGVtZW50IiwidGl0bGUiLCJhbHRfdGV4dCIsInRodW1ibmFpbCIsImxpbmsiLCJsaW5rX3RhcmdldCIsImltYWdlcyIsIl9nZXRTbGlkZUltYWdlc0RhdGEiLCIkaW1hZ2VEcm9wZG93bkNvbnRhaW5lcnMiLCJmaWx0ZXIiLCIkZHJvcGRvd24iLCJsZW5ndGgiLCJpcyIsImJyZWFrcG9pbnQiLCJyZXBsYWNlIiwiaW1hZ2UiLCJhcmVhcyIsIl9nZXRTbGlkZUltYWdlQXJlYURhdGEiLCJzbGlkZUltYWdlQ29udGFpbmVyIiwiJGxpc3RJdGVtcyIsImNoaWxkcmVuIiwibGlua1RpdGxlIiwibGlua1VybCIsImxpbmtUYXJnZXQiLCJjb29yZGluYXRlcyIsImZvcm1EYXRhIiwiRm9ybURhdGEiLCIkZmlsZUlucHV0cyIsImZpbGUiLCJmaWxlcyIsImtleSIsImFwcGVuZCIsImltYWdlc1VwbG9hZFVybCIsInByb2Nlc3NEYXRhIiwiY29udGVudFR5cGUiLCJmaWxlTmFtZXMiLCJ0aHVtYm5haWxOYW1lcyIsImNsYXNzTmFtZSIsInB1c2giLCJ2YWx1ZSIsImltYWdlc0RlbGV0ZVVybCIsImZpbGVfbmFtZXMiLCJ0aHVtYm5haWxfbmFtZXMiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsYUFERCxFQUdDLENBQ0MsS0FERCxFQUVDLE9BRkQsRUFHQyxpQkFIRCxDQUhELEVBU0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsbUJBQW1CRixNQUFNRyxJQUFOLENBQVcsb0JBQVgsQ0FBekI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsZUFBZUosTUFBTUcsSUFBTixDQUFXLFdBQVgsQ0FBckI7O0FBRUE7Ozs7O0FBS0EsS0FBTUUscUJBQXFCTCxNQUFNRyxJQUFOLENBQVcsZ0JBQVgsQ0FBM0I7O0FBRUE7Ozs7O0FBS0EsS0FBSUcsV0FBVyxJQUFmOztBQUVBOzs7OztBQUtBLEtBQUlDLFlBQVksS0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsV0FBVyxFQUFqQjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxVQUFVUixFQUFFUyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCVCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFNQSxVQUFTYSxhQUFULENBQXVCQyxLQUF2QixFQUE4QmIsSUFBOUIsRUFBb0M7QUFDbkM7QUFDQWEsUUFBTUMsY0FBTjs7QUFFQTtBQUNBLE1BQUlkLFFBQVFBLEtBQUtlLE9BQWpCLEVBQTBCO0FBQ3pCUCxlQUFZLElBQVo7QUFDQTs7QUFFRDtBQUNBRCxhQUFXUyxJQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLElBQXpCLENBQThCbEIsS0FBOUIsQ0FBWDs7QUFFQTtBQUNBbUIsaUJBQ0VDLElBREYsQ0FDT0MsWUFEUCxFQUNxQkMsZ0JBRHJCLEVBRUVGLElBRkYsQ0FFT0csY0FGUCxFQUV1QkQsZ0JBRnZCLEVBR0VGLElBSEYsQ0FHTztBQUFBLFVBQU1MLElBQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5Qk8sSUFBekIsQ0FBOEJsQixRQUE5QixDQUFOO0FBQUEsR0FIUDtBQUlBOztBQUVEOzs7QUFHQSxVQUFTZ0IsZ0JBQVQsR0FBNEI7QUFDM0I7QUFDQSxNQUFNRyxhQUFhVixJQUFJVyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwrQkFBeEIsRUFBeUQsU0FBekQsQ0FBbkI7QUFDQSxNQUFNQyxlQUFlZCxJQUFJVyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qiw4QkFBeEIsRUFBd0QsU0FBeEQsQ0FBckI7O0FBRUE7QUFDQWIsTUFBSUMsSUFBSixDQUFTYyxLQUFULENBQWVDLFdBQWYsQ0FBMkJOLFVBQTNCLEVBQXVDSSxZQUF2Qzs7QUFFQTtBQUNBLE1BQUl2QixRQUFKLEVBQWM7QUFDYlMsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCTyxJQUF6QixDQUE4QmxCLFFBQTlCO0FBQ0E7QUFDRDs7QUFFRDs7O0FBR0EsVUFBU2lCLGNBQVQsR0FBMEI7QUFDekI7QUFDQSxNQUFNUyxpQkFBaUI7QUFDdEJDLFFBQUtqQyxNQUFNa0MsSUFBTixDQUFXLFFBQVgsQ0FEaUI7QUFFdEJuQyxTQUFNb0M7QUFGZ0IsR0FBdkI7O0FBS0FwQixNQUFJQyxJQUFKLENBQVNvQixHQUFULENBQWFDLElBQWIsQ0FBa0JMLGNBQWxCLEVBQ0VNLElBREYsQ0FDTyxvQkFBWTtBQUNqQjtBQUNBLE9BQU1MLE1BQU0xQixnREFBOENnQyxTQUFTQyxFQUF2RCxHQUE4RC9CLFFBQVFnQyxXQUFsRjs7QUFFQTtBQUNBQyxVQUFPQyxjQUFQLEdBQXdCLElBQXhCOztBQUVBO0FBQ0FELFVBQU9FLElBQVAsQ0FBWVgsR0FBWixFQUFpQixPQUFqQjtBQUNBLEdBVkYsRUFXRVksSUFYRixDQVdPdkIsZ0JBWFA7QUFZQTs7QUFHRDs7Ozs7QUFLQSxVQUFTYSxZQUFULEdBQXdCO0FBQ3ZCO0FBQ0EsTUFBTXBDLE9BQU87QUFDWnlDLE9BQUl4QyxNQUFNRCxJQUFOLENBQVcsSUFBWDtBQURRLEdBQWI7O0FBSUE7QUFDQSxTQUFPRSxFQUFFUyxNQUFGLENBQVMsSUFBVCxFQUFlWCxJQUFmLEVBQXFCK0MsZ0JBQXJCLEVBQXVDQyxnQkFBdkMsQ0FBUDtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNELGNBQVQsR0FBMEI7QUFDekIsTUFBTUUsT0FBTzlDLGlCQUNYQyxJQURXLENBQ04sb0JBRE0sRUFFWDhDLEdBRlcsRUFBYjs7QUFJQSxNQUFNQyxRQUFRaEQsaUJBQ1pDLElBRFksQ0FDUCxxQkFETyxFQUVaOEMsR0FGWSxFQUFkOztBQUlBLFNBQU8sRUFBQ0QsVUFBRCxFQUFPRSxZQUFQLEVBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxVQUFTSCxjQUFULEdBQTBCO0FBQ3pCO0FBQ0EsTUFBTUksU0FBUyxFQUFmOztBQUVBO0FBQ0EvQyxlQUFhZ0QsSUFBYixDQUFrQixVQUFDQyxLQUFELEVBQVFDLE9BQVIsRUFBb0I7QUFDckM7QUFDQSxPQUFNQyx1QkFBdUJ0RCxFQUFFcUQsT0FBRixDQUE3Qjs7QUFFQTtBQUNBLE9BQU1FLGVBQWVELHFCQUFxQnBELElBQXJCLENBQTBCLFFBQTFCLENBQXJCOztBQUVBO0FBQ0EsT0FBTXNELGVBQWVGLHFCQUFxQnhELElBQXJCLENBQTBCLFVBQTFCLENBQXJCOztBQUVBO0FBQ0E7QUFDQW9ELFVBQU9NLFlBQVAsSUFBdUJ4RCxFQUFFeUQsR0FBRixDQUFNRixZQUFOLEVBQW9CO0FBQUEsV0FBV0csY0FBY0wsT0FBZCxDQUFYO0FBQUEsSUFBcEIsQ0FBdkI7QUFDQSxHQWJEOztBQWVBLFNBQU8sRUFBQ0gsY0FBRCxFQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTUSxhQUFULENBQXVCQyxVQUF2QixFQUFtQztBQUNsQyxNQUFNQyxXQUFXNUQsRUFBRTJELFVBQUYsQ0FBakI7O0FBRUEsU0FBTztBQUNOcEIsT0FBSXFCLFNBQVM5RCxJQUFULENBQWMsSUFBZCxDQURFO0FBRU4rRCxVQUFPRCxTQUFTMUQsSUFBVCxDQUFjLHFCQUFkLEVBQXFDOEMsR0FBckMsRUFGRDtBQUdOYyxhQUFVRixTQUFTMUQsSUFBVCxDQUFjLHdCQUFkLEVBQXdDOEMsR0FBeEMsRUFISjtBQUlOZSxjQUFXSCxTQUFTMUQsSUFBVCxDQUFjLDBCQUFkLEVBQTBDOEMsR0FBMUMsRUFKTDtBQUtOZ0IsU0FBTUosU0FBUzFELElBQVQsQ0FBYyxvQkFBZCxFQUFvQzhDLEdBQXBDLEVBTEE7QUFNTmlCLGdCQUFhTCxTQUFTMUQsSUFBVCxDQUFjLDRCQUFkLEVBQTRDOEMsR0FBNUMsRUFOUDtBQU9Oa0IsV0FBUUMsb0JBQW9CUixVQUFwQjtBQVBGLEdBQVA7QUFTQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNRLG1CQUFULENBQTZCUixVQUE3QixFQUF5QztBQUN4QztBQUNBLE1BQU1TLDJCQUEyQnBFLEVBQUUyRCxVQUFGLEVBQy9CekQsSUFEK0IsQ0FDMUIsaUJBRDBCLEVBRS9CbUUsTUFGK0IsQ0FFeEIsVUFBQ2pCLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUMzQixPQUFNaUIsWUFBWXRFLEVBQUVxRCxPQUFGLEVBQVduRCxJQUFYLENBQWdCLGlCQUFoQixDQUFsQjs7QUFFQSxVQUFPb0UsVUFBVUMsTUFBVixJQUFvQixDQUFDRCxVQUFVRSxFQUFWLENBQWEsb0JBQWIsQ0FBNUI7QUFDQSxHQU4rQixDQUFqQzs7QUFRQTtBQUNBLFNBQU94RSxFQUFFeUQsR0FBRixDQUFNVyx3QkFBTixFQUFnQyxtQkFBVztBQUNqRDtBQUNBLE9BQU1SLFdBQVc1RCxFQUFFcUQsT0FBRixDQUFqQjs7QUFFQTtBQUNBLE9BQU1pQixZQUFZVixTQUFTMUQsSUFBVCxDQUFjLGlCQUFkLENBQWxCOztBQUVBO0FBQ0EsVUFBTztBQUNOcUMsUUFBSXFCLFNBQVM5RCxJQUFULENBQWMsSUFBZCxDQURFO0FBRU4yRSxnQkFBWUgsVUFBVXJDLElBQVYsQ0FBZSxNQUFmLEVBQXVCeUMsT0FBdkIsQ0FBK0IsYUFBL0IsRUFBOEMsRUFBOUMsQ0FGTjtBQUdOQyxXQUFPTCxVQUFVdEIsR0FBVixFQUhEO0FBSU40QixXQUFPQyx1QkFBdUJ4QixPQUF2QjtBQUpELElBQVA7QUFNQSxHQWRNLENBQVA7QUFlQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVN3QixzQkFBVCxDQUFnQ0MsbUJBQWhDLEVBQXFEO0FBQ3BEO0FBQ0EsTUFBTUMsYUFBYS9FLEVBQUU4RSxtQkFBRixFQUNqQjVFLElBRGlCLENBQ1osc0JBRFksRUFFakI4RSxRQUZpQixFQUFuQjs7QUFJQTtBQUNBLFNBQU9oRixFQUFFeUQsR0FBRixDQUFNc0IsVUFBTixFQUFrQixtQkFBVztBQUNuQztBQUNBLE9BQU1uQixXQUFXNUQsRUFBRXFELE9BQUYsQ0FBakI7O0FBRUEsVUFBTztBQUNOZCxRQUFJcUIsU0FBUzlELElBQVQsQ0FBYyxJQUFkLENBREU7QUFFTm1GLGVBQVdyQixTQUFTOUQsSUFBVCxDQUFjLFdBQWQsQ0FGTDtBQUdOb0YsYUFBU3RCLFNBQVM5RCxJQUFULENBQWMsU0FBZCxDQUhIO0FBSU5xRixnQkFBWXZCLFNBQVM5RCxJQUFULENBQWMsWUFBZCxDQUpOO0FBS05zRixpQkFBYXhCLFNBQVM5RCxJQUFULENBQWMsYUFBZDtBQUxQLElBQVA7QUFPQSxHQVhNLENBQVA7QUFZQTs7QUFFRDs7Ozs7QUFLQSxVQUFTb0IsWUFBVCxHQUF3QjtBQUN2QjtBQUNBLE1BQU1tRSxXQUFXLElBQUlDLFFBQUosRUFBakI7O0FBRUE7QUFDQSxNQUFNQyxjQUFjeEYsTUFBTUcsSUFBTixDQUFXLE9BQVgsQ0FBcEI7O0FBRUE7QUFDQXFGLGNBQVlwQyxJQUFaLENBQWlCLFVBQUNDLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUNwQztBQUNBLE9BQU1tQyxPQUFPbkMsUUFBUW9DLEtBQVIsQ0FBYyxDQUFkLENBQWI7O0FBRUE7QUFDQSxPQUFNQyxNQUFNckMsUUFBUWQsRUFBUixHQUFhLElBQXpCOztBQUVBO0FBQ0EsT0FBSWlELElBQUosRUFBVTtBQUNUSCxhQUFTTSxNQUFULENBQWdCRCxHQUFoQixFQUFxQkYsSUFBckI7QUFDQTtBQUNELEdBWEQ7O0FBYUE7QUFDQSxNQUFNekQsaUJBQWlCO0FBQ3RCQyxRQUFLeEIsUUFBUW9GLGVBRFM7QUFFdEI5RixTQUFNdUYsUUFGZ0I7QUFHdEJRLGdCQUFhLEtBSFM7QUFJdEJDLGdCQUFhO0FBSlMsR0FBdkI7O0FBT0E7QUFDQSxTQUFPaEYsSUFBSUMsSUFBSixDQUFTb0IsR0FBVCxDQUFhQyxJQUFiLENBQWtCTCxjQUFsQixDQUFQO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNYLFlBQVQsR0FBd0I7QUFDdkI7QUFDQSxNQUFJMkUsWUFBWSxFQUFoQjs7QUFFQTtBQUNBLE1BQUlDLGlCQUFpQixFQUFyQjs7QUFFQTtBQUNBNUYscUJBQ0U0RSxRQURGLEdBRUU3QixJQUZGLENBRU8sVUFBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQW9CO0FBQ3pCQSxXQUFRNEMsU0FBUixLQUFzQixXQUF0QixHQUNBRCxlQUFlRSxJQUFmLENBQW9CN0MsUUFBUThDLEtBQTVCLENBREEsR0FFQUosVUFBVUcsSUFBVixDQUFlN0MsUUFBUThDLEtBQXZCLENBRkE7QUFHQSxHQU5GOztBQVFBO0FBQ0EsTUFBTXBFLGlCQUFpQjtBQUN0QkMsUUFBS3hCLFFBQVE0RixlQURTO0FBRXRCdEcsU0FBTSxFQUFDdUcsWUFBWU4sU0FBYixFQUF3Qk8saUJBQWlCTixjQUF6QztBQUZnQixHQUF2Qjs7QUFLQSxTQUFPbEYsSUFBSUMsSUFBSixDQUFTb0IsR0FBVCxDQUFhQyxJQUFiLENBQWtCTCxjQUFsQixDQUFQO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBbEMsUUFBTzBHLElBQVAsR0FBYyxnQkFBUTtBQUNyQjtBQUNBeEcsUUFBTXlHLEVBQU4sQ0FBUyxRQUFULEVBQW1COUYsYUFBbkI7O0FBRUE7QUFDQTJCO0FBQ0EsRUFORDs7QUFRQSxRQUFPeEMsTUFBUDtBQUNBLENBcFlGIiwiZmlsZSI6InNsaWRlcnMvZGV0YWlscy9zbGlkZXJfZm9ybS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gc2xpZGVyX2Zvcm0uanMgMjAxNi0xMi0xMlxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogQ29udHJvbGxlciBNb2R1bGUgRm9yIFNsaWRlciBFZGl0IEZvcm1cbiAqXG4gKiBIYW5kbGVzIHRoZSBzbGlkZXJzIGRldGFpbHMgZm9ybSBvcGVyYXRpb25zLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdzbGlkZXJfZm9ybScsXG5cdFxuXHRbXG5cdFx0J3hocicsXG5cdFx0J21vZGFsJyxcblx0XHQnbG9hZGluZ19zcGlubmVyJ1xuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNYXN0ZXIgZGF0YSBwYW5lbCBzZWxlY3Rvci5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJG1hc3RlckRhdGFQYW5lbCA9ICR0aGlzLmZpbmQoJy5wYW5lbC5tYXN0ZXItZGF0YScpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNsaWRlIHBhbmVsIGNvbnRhaW5lcnMgKGVhY2ggbGFuZ3VhZ2UgaG9sZHMgYSBjb250YWluZXIsIHRoYXQgY29udGFpbnMgdGhlIHNsaWRlIHBhbmVscykuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0YWJDb250ZW50cyA9ICR0aGlzLmZpbmQoJy50YWItcGFuZScpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlbGV0ZXIgc2VsZWN0IGJveC5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJGltYWdlRGVsZXRlU2VsZWN0ID0gJHRoaXMuZmluZCgnI2RlbGV0ZV9pbWFnZXMnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTcGlubmVyIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGxldCAkc3Bpbm5lciA9IG51bGw7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRG8gYSByZWZyZXNoIGluc3RlYWQgb2YgcmVkaXJlY3Rpbmc/XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7Qm9vbGVhbn1cblx0XHQgKi9cblx0XHRsZXQgZG9SZWZyZXNoID0gZmFsc2U7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGRlZmF1bHRzID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIGZvcm0gc3VibWl0IGV2ZW50LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IFRyaWdnZXJlZCBldmVudC5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YSBFdmVudCBkYXRhLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkZvcm1TdWJtaXQoZXZlbnQsIGRhdGEpIHtcblx0XHRcdC8vIFByZXZlbnQgdGhlIHN1Ym1pdCBvZiB0aGUgZm9ybS5cblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrIHJlZnJlc2ggcGFyYW1ldGVyLlxuXHRcdFx0aWYgKGRhdGEgJiYgZGF0YS5yZWZyZXNoKSB7XG5cdFx0XHRcdGRvUmVmcmVzaCA9IHRydWU7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgbG9hZGluZyBzcGlubmVyLlxuXHRcdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcyk7XG5cdFx0XHRcblx0XHRcdC8vIFVwbG9hZCBmaWxlcy5cblx0XHRcdF91cGxvYWRGaWxlcygpXG5cdFx0XHRcdC50aGVuKF9kZWxldGVGaWxlcywgX3Nob3dGYWlsTWVzc2FnZSlcblx0XHRcdFx0LnRoZW4oX3BlcmZvcm1TdWJtaXQsIF9zaG93RmFpbE1lc3NhZ2UpXG5cdFx0XHRcdC50aGVuKCgpID0+IGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5oaWRlKCRzcGlubmVyKSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNob3dzIHRoZSBzdWJtaXQgZXJyb3IgbWVzc2FnZSBtb2RhbC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2hvd0ZhaWxNZXNzYWdlKCkge1xuXHRcdFx0Ly8gTWVzc2FnZSB0ZXh0cy5cblx0XHRcdGNvbnN0IGVycm9yVGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRk9STV9TVUJNSVRfRVJST1JfTU9EQUxfVElUTEUnLCAnc2xpZGVycycpO1xuXHRcdFx0Y29uc3QgZXJyb3JNZXNzYWdlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0ZPUk1fU1VCTUlUX0VSUk9SX01PREFMX1RFWFQnLCAnc2xpZGVycycpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoZXJyb3JUaXRsZSwgZXJyb3JNZXNzYWdlKTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHRpZiAoJHNwaW5uZXIpIHtcblx0XHRcdFx0anNlLmxpYnMubG9hZGluZ19zcGlubmVyLmhpZGUoJHNwaW5uZXIpXG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFBlcmZvcm1zIHRoZSBmb3JtIHN1Ym1pdCBBSkFYIHJlcXVlc3QuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3BlcmZvcm1TdWJtaXQoKSB7XG5cdFx0XHQvLyBBSkFYIHJlcXVlc3Qgb3B0aW9ucy5cblx0XHRcdGNvbnN0IHJlcXVlc3RPcHRpb25zID0ge1xuXHRcdFx0XHR1cmw6ICR0aGlzLmF0dHIoJ2FjdGlvbicpLFxuXHRcdFx0XHRkYXRhOiBfZ2V0Rm9ybURhdGEoKVxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0anNlLmxpYnMueGhyLnBvc3QocmVxdWVzdE9wdGlvbnMpXG5cdFx0XHRcdC5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHQvLyBVUkwgdG8gcmVkaXJlY3QgdG8uXG5cdFx0XHRcdFx0Y29uc3QgdXJsID0gZG9SZWZyZXNoID8gYGFkbWluLnBocD9kbz1TbGlkZXJzRGV0YWlscyZpZD0ke3Jlc3BvbnNlLmlkfWAgOiBvcHRpb25zLnJlZGlyZWN0VXJsO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFByZXZlbnQgdGhhdCB0aGUgcGFnZSB1bmxvYWQgcHJvbXB0IGlzIGRpc3BsYXllZCBvbiBzYXZlIGFjdGlvbi5cblx0XHRcdFx0XHR3aW5kb3cub25iZWZvcmV1bmxvYWQgPSBudWxsO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIE9wZW4gb3ZlcnZpZXcgcGFnZS5cblx0XHRcdFx0XHR3aW5kb3cub3Blbih1cmwsICdfc2VsZicpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuZmFpbChfc2hvd0ZhaWxNZXNzYWdlKTtcblx0XHR9XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgZ2F0aGVyZWQgZm9ybSBkYXRhLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T2JqZWN0fSBGb3JtIGRhdGEuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldEZvcm1EYXRhKCkge1xuXHRcdFx0Ly8gRm9ybSBkYXRhIG9iamVjdC5cblx0XHRcdGNvbnN0IGRhdGEgPSB7XG5cdFx0XHRcdGlkOiAkdGhpcy5kYXRhKCdpZCcpXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBFeHRlbmQgZm9ybSBkYXRhIG9iamVjdCB3aXRoIGFsbCBuZWNlc3NhcnkgcHJvcGVydGllcy5cblx0XHRcdHJldHVybiAkLmV4dGVuZCh0cnVlLCBkYXRhLCBfZ2V0TWFzdGVyRGF0YSgpLCBfZ2V0U2xpZGVzRGF0YSgpKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgc2xpZGVyJ3MgbWFzdGVyIGRhdGEuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPYmplY3R9IFNsaWRlciBtYXN0ZXIgZGF0YS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0TWFzdGVyRGF0YSgpIHtcblx0XHRcdGNvbnN0IG5hbWUgPSAkbWFzdGVyRGF0YVBhbmVsXG5cdFx0XHRcdC5maW5kKCdpbnB1dFtuYW1lPVwibmFtZVwiXScpXG5cdFx0XHRcdC52YWwoKTtcblx0XHRcdFxuXHRcdFx0Y29uc3Qgc3BlZWQgPSAkbWFzdGVyRGF0YVBhbmVsXG5cdFx0XHRcdC5maW5kKCdpbnB1dFtuYW1lPVwic3BlZWRcIl0nKVxuXHRcdFx0XHQudmFsKCk7XG5cdFx0XHRcblx0XHRcdHJldHVybiB7bmFtZSwgc3BlZWR9O1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXR1cm5zIHRoZSBzbGlkZXMgZGF0YSBieSBpdGVyYXRpbmcgb3ZlciB0aGUgdGFiIGNvbnRlbnQgZWxlbWVudHNcblx0XHQgKiB3aGljaCByZXByZXNlbnQgYSBjb250YWluZXIgZm9yIGVhY2ggbGFuZ3VhZ2UuXG5cdFx0ICpcblx0XHQgKiBUaGUgcmV0dXJuZWQgb2JqZWN0IGNvbnRhaW5zIGEgcHJvcGVydHkgZm9yIGVhY2ggbGFuZ3VhZ2UuXG5cdFx0ICogVGhlIGtleSBpcyB0aGUgbGFuZ3VhZ2UgY29kZSBhbmQgdGhlIHZhbHVlIGlzIGFuIGFycmF5IGNvbnRhaW5pbmdcblx0XHQgKiBhbGwgc2xpZGVzIGluZm9ybWF0aW9uIGFzIGNvbGxlY3Rpb24uXG5cdFx0ICpcblx0XHQgKiBFeGFtcGxlIG91dHB1dDpcblx0XHQgKiB7XG5cdFx0ICogICBkZTogW3tcblx0XHQgKiAgICAgaWQ6IDEsXG5cdFx0ICogICAgIHRodW1ibmFpbDogJ015IHBpY3R1cmUnLFxuXHRcdCAqICAgICAuLi5cblx0XHQgKiAgIH1dXG5cdFx0ICogfVxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T2JqZWN0fSBTbGlkZXMgZGF0YS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0U2xpZGVzRGF0YSgpIHtcblx0XHRcdC8vIFNsaWRlcyBkYXRhIG9iamVjdC5cblx0XHRcdGNvbnN0IHNsaWRlcyA9IHt9O1xuXHRcdFx0XG5cdFx0XHQvLyBJdGVyYXRlIG92ZXIgZWFjaCBzbGlkZXIgcGFuZWwgY29udGFpbmVyLlxuXHRcdFx0JHRhYkNvbnRlbnRzLmVhY2goKGluZGV4LCBlbGVtZW50KSA9PiB7XG5cdFx0XHRcdC8vIFNsaWRlIHBhbmVsIGNvbnRhaW5lciBlbGVtZW50LlxuXHRcdFx0XHRjb25zdCAkc2xpZGVQYW5lbENvbnRhaW5lciA9ICQoZWxlbWVudCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTbGlkZSBwYW5lbCBlbGVtZW50cy5cblx0XHRcdFx0Y29uc3QgJHNsaWRlUGFuZWxzID0gJHNsaWRlUGFuZWxDb250YWluZXIuZmluZCgnLnBhbmVsJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBHZXQgc2xpZGUgcGFuZWwgY29udGFpbmVyIGxhbmd1YWdlIGNvZGUuXG5cdFx0XHRcdGNvbnN0IGxhbmd1YWdlQ29kZSA9ICRzbGlkZVBhbmVsQ29udGFpbmVyLmRhdGEoJ2xhbmd1YWdlJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBZGQgcHJvcGVydHkgdG8gc2xpZGVzIGRhdGEgb2JqZWN0LFxuXHRcdFx0XHQvLyB3aGljaCBjb250YWlucyB0aGUgbGFuZ3VhZ2UgY29kZSBhcyBrZXkgYW5kIHRoZSBzbGlkZXMgZGF0YSBhcyB2YWx1ZS5cblx0XHRcdFx0c2xpZGVzW2xhbmd1YWdlQ29kZV0gPSAkLm1hcCgkc2xpZGVQYW5lbHMsIGVsZW1lbnQgPT4gX2dldFNsaWRlRGF0YShlbGVtZW50KSk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIHtzbGlkZXN9O1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXR1cm5zIHRoZSBkYXRhIGZvciBhIHNsaWRlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gc2xpZGVQYW5lbCBTbGlkZSBwYW5lbCBlbGVtZW50LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T2JqZWN0fSBTbGlkZSBkYXRhLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRTbGlkZURhdGEoc2xpZGVQYW5lbCkge1xuXHRcdFx0Y29uc3QgJGVsZW1lbnQgPSAkKHNsaWRlUGFuZWwpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRpZDogJGVsZW1lbnQuZGF0YSgnaWQnKSxcblx0XHRcdFx0dGl0bGU6ICRlbGVtZW50LmZpbmQoJ2lucHV0W25hbWU9XCJ0aXRsZVwiXScpLnZhbCgpLFxuXHRcdFx0XHRhbHRfdGV4dDogJGVsZW1lbnQuZmluZCgnaW5wdXRbbmFtZT1cImFsdF90ZXh0XCJdJykudmFsKCksXG5cdFx0XHRcdHRodW1ibmFpbDogJGVsZW1lbnQuZmluZCgnc2VsZWN0W25hbWU9XCJ0aHVtYm5haWxcIl0nKS52YWwoKSxcblx0XHRcdFx0bGluazogJGVsZW1lbnQuZmluZCgnaW5wdXRbbmFtZT1cImxpbmtcIl0nKS52YWwoKSxcblx0XHRcdFx0bGlua190YXJnZXQ6ICRlbGVtZW50LmZpbmQoJ3NlbGVjdFtuYW1lPVwibGlua190YXJnZXRcIl0nKS52YWwoKSxcblx0XHRcdFx0aW1hZ2VzOiBfZ2V0U2xpZGVJbWFnZXNEYXRhKHNsaWRlUGFuZWwpXG5cdFx0XHR9O1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXR1cm5zIHRoZSBzbGlkZSBpbWFnZXMgZGF0YS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IHNsaWRlUGFuZWwgU2xpZGUgcGFuZWwgZWxlbWVudC5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge09iamVjdH0gU2xpZGUgaW1hZ2VzIGRhdGEuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldFNsaWRlSW1hZ2VzRGF0YShzbGlkZVBhbmVsKSB7XG5cdFx0XHQvLyBJbWFnZSBkcm9wZG93biBjb250YWluZXIgZWxlbWVudHMgKHdpdGhvdXQgdGh1bWJuYWlsKS5cblx0XHRcdGNvbnN0ICRpbWFnZURyb3Bkb3duQ29udGFpbmVycyA9ICQoc2xpZGVQYW5lbClcblx0XHRcdFx0LmZpbmQoJy5yb3cuZm9ybS1ncm91cCcpXG5cdFx0XHRcdC5maWx0ZXIoKGluZGV4LCBlbGVtZW50KSA9PiB7XG5cdFx0XHRcdFx0Y29uc3QgJGRyb3Bkb3duID0gJChlbGVtZW50KS5maW5kKCcuZHJvcGRvd24taW5wdXQnKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRyZXR1cm4gJGRyb3Bkb3duLmxlbmd0aCAmJiAhJGRyb3Bkb3duLmlzKCdbbmFtZT1cInRodW1ibmFpbFwiXScpO1xuXHRcdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gSXRlcmF0ZSBvdmVyIGVhY2ggZHJvcGRvd24gZWxlbWVudCBhbmQgcmV0cmlldmUgdGhlIGRhdGEuXG5cdFx0XHRyZXR1cm4gJC5tYXAoJGltYWdlRHJvcGRvd25Db250YWluZXJzLCBlbGVtZW50ID0+IHtcblx0XHRcdFx0Ly8gRHJvcGRvd24gY29udGFpbmVyIGVsZW1lbnQuXG5cdFx0XHRcdGNvbnN0ICRlbGVtZW50ID0gJChlbGVtZW50KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIERyb3Bkb3duIGVsZW1lbnQuXG5cdFx0XHRcdGNvbnN0ICRkcm9wZG93biA9ICRlbGVtZW50LmZpbmQoJy5kcm9wZG93bi1pbnB1dCcpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gSW1hZ2UgZGF0YSBvYmplY3QuXG5cdFx0XHRcdHJldHVybiB7XG5cdFx0XHRcdFx0aWQ6ICRlbGVtZW50LmRhdGEoJ2lkJyksXG5cdFx0XHRcdFx0YnJlYWtwb2ludDogJGRyb3Bkb3duLmF0dHIoJ25hbWUnKS5yZXBsYWNlKCdicmVha3BvaW50XycsICcnKSxcblx0XHRcdFx0XHRpbWFnZTogJGRyb3Bkb3duLnZhbCgpLFxuXHRcdFx0XHRcdGFyZWFzOiBfZ2V0U2xpZGVJbWFnZUFyZWFEYXRhKGVsZW1lbnQpXG5cdFx0XHRcdH07XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgc2xpZGUgaW1hZ2UgYXJlYSBkYXRhLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gc2xpZGVJbWFnZUNvbnRhaW5lciBTbGlkZSBpbWFnZSBjb25maWd1cmF0aW9uIHJvdyBlbGVtZW50LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T2JqZWN0fSBTbGlkZSBpbWFnZSBhcmVhIGRhdGEuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldFNsaWRlSW1hZ2VBcmVhRGF0YShzbGlkZUltYWdlQ29udGFpbmVyKSB7XG5cdFx0XHQvLyBTbGlkZSBpbWFnZSBhcmVhIGRhdGEgY29udGFpbmVyIGxpc3QgaXRlbXMuXG5cdFx0XHRjb25zdCAkbGlzdEl0ZW1zID0gJChzbGlkZUltYWdlQ29udGFpbmVyKVxuXHRcdFx0XHQuZmluZCgnLmltYWdlLW1hcC1kYXRhLWxpc3QnKVxuXHRcdFx0XHQuY2hpbGRyZW4oKTtcblx0XHRcdFxuXHRcdFx0Ly8gSXRlcmF0ZSBvdmVyIGVhY2ggZHJvcGRvd24gbGlzdCBpdGVtIGVsZW1lbnQgYW5kIHJldHJpZXZlIHRoZSBkYXRhLlxuXHRcdFx0cmV0dXJuICQubWFwKCRsaXN0SXRlbXMsIGVsZW1lbnQgPT4ge1xuXHRcdFx0XHQvLyBMaXN0IGl0ZW0gZWxlbWVudC5cblx0XHRcdFx0Y29uc3QgJGVsZW1lbnQgPSAkKGVsZW1lbnQpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0XHRpZDogJGVsZW1lbnQuZGF0YSgnaWQnKSxcblx0XHRcdFx0XHRsaW5rVGl0bGU6ICRlbGVtZW50LmRhdGEoJ2xpbmtUaXRsZScpLFxuXHRcdFx0XHRcdGxpbmtVcmw6ICRlbGVtZW50LmRhdGEoJ2xpbmtVcmwnKSxcblx0XHRcdFx0XHRsaW5rVGFyZ2V0OiAkZWxlbWVudC5kYXRhKCdsaW5rVGFyZ2V0JyksXG5cdFx0XHRcdFx0Y29vcmRpbmF0ZXM6ICRlbGVtZW50LmRhdGEoJ2Nvb3JkaW5hdGVzJylcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFBlcmZvcm1zIHRoZSBpbWFnZXMgdXBsb2FkIEFKQVggcmVxdWVzdC5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge2pRdWVyeS5qcVhIUn0galF1ZXJ5IGRlZmVycmVkIG9iamVjdC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfdXBsb2FkRmlsZXMoKSB7XG5cdFx0XHQvLyBGb3JtIGRhdGEgb2JqZWN0LlxuXHRcdFx0Y29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcblx0XHRcdFxuXHRcdFx0Ly8gRmlsZSBpbnB1dHMuXG5cdFx0XHRjb25zdCAkZmlsZUlucHV0cyA9ICR0aGlzLmZpbmQoJzpmaWxlJyk7XG5cdFx0XHRcblx0XHRcdC8vIEFwcGVuZCBmaWxlcyB0byBmb3JtIGRhdGEgb2JqZWN0LlxuXHRcdFx0JGZpbGVJbnB1dHMuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcblx0XHRcdFx0Ly8gRmlsZS5cblx0XHRcdFx0Y29uc3QgZmlsZSA9IGVsZW1lbnQuZmlsZXNbMF07XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBEYXRhIGVudHJ5IGtleS5cblx0XHRcdFx0Y29uc3Qga2V5ID0gZWxlbWVudC5pZCArICdbXSc7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBcHBlbmQgZmlsZSB0byBmb3JtIGRhdGEgb2JqZWN0LlxuXHRcdFx0XHRpZiAoZmlsZSkge1xuXHRcdFx0XHRcdGZvcm1EYXRhLmFwcGVuZChrZXksIGZpbGUpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gQUpBWCByZXF1ZXN0IG9wdGlvbnMuXG5cdFx0XHRjb25zdCByZXF1ZXN0T3B0aW9ucyA9IHtcblx0XHRcdFx0dXJsOiBvcHRpb25zLmltYWdlc1VwbG9hZFVybCxcblx0XHRcdFx0ZGF0YTogZm9ybURhdGEsXG5cdFx0XHRcdHByb2Nlc3NEYXRhOiBmYWxzZSxcblx0XHRcdFx0Y29udGVudFR5cGU6IGZhbHNlXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBQZXJmb3JtIEFKQVggcmVxdWVzdC5cblx0XHRcdHJldHVybiBqc2UubGlicy54aHIucG9zdChyZXF1ZXN0T3B0aW9ucyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFBlcmZvcm1zIHRoZSBpbWFnZSBkZWxldGlvbiBBSkFYIHJlcXVlc3QuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2RlbGV0ZUZpbGVzKCkge1xuXHRcdFx0Ly8gTGlzdCBvZiBpbWFnZSBmaWxlIG5hbWVzLlxuXHRcdFx0bGV0IGZpbGVOYW1lcyA9IFtdO1xuXHRcdFx0XG5cdFx0XHQvLyBMaXN0IG9mIHRodW1ibmFpbCBpbWFnZXMuXG5cdFx0XHRsZXQgdGh1bWJuYWlsTmFtZXMgPSBbXTtcblx0XHRcdFxuXHRcdFx0Ly8gRmlsbCB0aGUgZmlsZSBuYW1lcyBsaXN0LlxuXHRcdFx0JGltYWdlRGVsZXRlU2VsZWN0XG5cdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdC5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4ge1xuXHRcdFx0XHRcdGVsZW1lbnQuY2xhc3NOYW1lID09PSAndGh1bWJuYWlsJyA/XG5cdFx0XHRcdFx0dGh1bWJuYWlsTmFtZXMucHVzaChlbGVtZW50LnZhbHVlKSA6XG5cdFx0XHRcdFx0ZmlsZU5hbWVzLnB1c2goZWxlbWVudC52YWx1ZSk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBSkFYIHJlcXVlc3Qgb3B0aW9ucy5cblx0XHRcdGNvbnN0IHJlcXVlc3RPcHRpb25zID0ge1xuXHRcdFx0XHR1cmw6IG9wdGlvbnMuaW1hZ2VzRGVsZXRlVXJsLFxuXHRcdFx0XHRkYXRhOiB7ZmlsZV9uYW1lczogZmlsZU5hbWVzLCB0aHVtYm5haWxfbmFtZXM6IHRodW1ibmFpbE5hbWVzfVxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIGpzZS5saWJzLnhoci5wb3N0KHJlcXVlc3RPcHRpb25zKTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdC8vIExpc3RlbiB0byBmb3JtIHN1Ym1pdCBldmVudC5cblx0XHRcdCR0aGlzLm9uKCdzdWJtaXQnLCBfb25Gb3JtU3VibWl0KTtcblx0XHRcdFxuXHRcdFx0Ly8gRmluaXNoIGluaXRpYWxpemF0aW9uLlxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKVxuOyJdfQ==
