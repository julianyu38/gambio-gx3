'use strict';

/* --------------------------------------------------------------
 static_seo_urls_index_checkboxes.js 2017-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Static Seo Urls Overview Start Page Option
 *
 * Handles the switchers toggling.
 */
gx.controllers.module('static_seo_urls_index_checkboxes', ['modal', gx.source + '/libs/info_box'], function () {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * CSS class names.
  *
  * @type {Object}
  */
	var classes = {
		switcher: 'switcher'
	};

	/**
  * Selector Strings
  *
  * @type {Object}
  */
	var selectors = {
		switcher: '.' + classes.switcher,
		switcherCheckbox: '.' + classes.switcher + ' :checkbox'
	};

	/**
  * URI map.
  *
  * @type {Object}
  */
	var uris = {
		activate: jse.core.config.get('appUrl') + '/admin/admin.php?do=StaticSeoUrlAjax/Activate',
		deactivate: jse.core.config.get('appUrl') + '/admin/admin.php?do=StaticSeoUrlAjax/Deactivate'
	};

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Shows the submit error message modal.
  */
	function _showFailMessage() {
		// Message texts.
		var errorTitle = jse.core.lang.translate('STATIC_SEO_URL_CHECKBOX_ERROR_TITLE', 'static_seo_urls');
		var errorMessage = jse.core.lang.translate('STATIC_SEO_URL_CHECKBOX_ERROR_TEXT', 'static_seo_urls');

		// Show modal.
		jse.libs.modal.showMessage(errorTitle, errorMessage);
	}

	/**
  * Handles the switchers change events.
  *
  * @param {jQuery.Event} event Trigger event.
  */
	function _onSwitcherChange(event) {
		// Clicked element.
		var $target = $(event.target);

		// Clicked switcher element.
		var $clickedSwitcher = $target.hasClass(classes.switcher) ? $target : $target.parents(selectors.switcher);

		// Clicked static seo url ID.
		var staticSeoUrlId = $clickedSwitcher.parents('tr').data('static-seo-url-id');

		// Is staticPage set as start page static page?
		var isActive = !$clickedSwitcher.hasClass('checked');

		// Which field should be updated
		var fieldName = $target.attr('name');

		// Disable all switchers.
		_toggleSwitchers(false);

		// Activate or deactivate static seo url depending on the state.
		isActive ? _deactivate(staticSeoUrlId, $clickedSwitcher, fieldName) : _activate(staticSeoUrlId, $clickedSwitcher, fieldName);
	}

	/**
  * Deactivates the static seo url.
  *
  * @param {Number} staticSeoUrlId Static seo url ID.
  * @param {jQuery} $clickedSwitcher Clicked static seo url element.
  * @param {String} fieldName Field name to be deactivated.
  */
	function _deactivate(staticSeoUrlId, $clickedSwitcher, fieldName) {
		// Request options.
		var requestOptions = {
			type: 'POST',
			data: { staticSeoUrlId: staticSeoUrlId, fieldName: fieldName },
			url: uris.deactivate
		};

		// Handles the 'always' case by enabling the clicked static seo url.
		var handleAlways = function handleAlways() {
			return $clickedSwitcher.removeClass('disabled');
		};

		// Handles the 'done' case with the server response.
		var handleDone = function handleDone(response) {
			// Enable all switchers.
			_toggleSwitchers(true);

			// Notify user.
			_notify(response.includes('success'));
		};

		// Perform request.
		$.ajax(requestOptions).done(handleDone).fail(_showFailMessage).always(handleAlways);
	}

	/**
  * Activates the static seo url.
  *
  * @param {Number} staticSeoUrlId Static seo url ID.
  * @param {jQuery} $clickedSwitcher Clicked static seo url element.
  * @param {String} fieldName Field name to be activated.
  */
	function _activate(staticSeoUrlId, $clickedSwitcher, fieldName) {
		// Request options.
		var requestOptions = {
			type: 'POST',
			data: { staticSeoUrlId: staticSeoUrlId, fieldName: fieldName },
			url: uris.activate
		};

		// Handles the 'always' case by enabling the clicked static seo url.
		var handleAlways = function handleAlways() {
			return _toggleSwitchers(true);
		};

		// Handles the 'done' case with the server response.
		var handleDone = function handleDone(response) {
			// Clicked switcher's checkbox.
			var $checkbox = $clickedSwitcher.find(':checkbox');

			// Enable all switchers.
			_toggleSwitchers(true);

			// Check switcher.
			$checkbox.switcher('checked', true);

			// Notify user.
			_notify(response.includes('success'));
		};

		// Perform request.
		$.ajax(requestOptions).done(handleDone).fail(_showFailMessage).always(handleAlways);
	}

	/**
  * If the server response is successful, it removes any previous messages and
  * adds new success message to admin info box.
  *
  * Otherwise its shows an error message modal.
  *
  * @param {Boolean} isSuccessful Is the server response successful?
  */
	function _notify(isSuccessful) {
		if (isSuccessful) {
			jse.libs.info_box.deleteBySource('adminAction').then(function () {
				return jse.libs.info_box.addSuccessMessage();
			});
		} else {
			jse.libs.modal.showMessage(jse.core.lang.translate('STATIC_SEO_URL_CHECKBOX_ERROR_TITLE', 'static_seo_urls'), jse.core.lang.translate('STATIC_SEO_URL_CHECKBOX_ERROR_TEXT', 'static_seo_urls'));
		}
	}

	/**
  * Enables or disables the switchers.
  *
  * @param {Boolean} doEnable Enable the switchers?
  */
	function _toggleSwitchers(doEnable) {
		$this.find(selectors.switcher)[(doEnable ? 'remove' : 'add') + 'Class']('disabled');
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Listen to set start page event.
		$this.on('change', selectors.switcher, _onSwitcherChange);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0YXRpY19zZW9fdXJscy9zdGF0aWNfc2VvX3VybHNfaW5kZXhfY2hlY2tib3hlcy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwiJHRoaXMiLCIkIiwiY2xhc3NlcyIsInN3aXRjaGVyIiwic2VsZWN0b3JzIiwic3dpdGNoZXJDaGVja2JveCIsInVyaXMiLCJhY3RpdmF0ZSIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJkZWFjdGl2YXRlIiwiX3Nob3dGYWlsTWVzc2FnZSIsImVycm9yVGl0bGUiLCJsYW5nIiwidHJhbnNsYXRlIiwiZXJyb3JNZXNzYWdlIiwibGlicyIsIm1vZGFsIiwic2hvd01lc3NhZ2UiLCJfb25Td2l0Y2hlckNoYW5nZSIsImV2ZW50IiwiJHRhcmdldCIsInRhcmdldCIsIiRjbGlja2VkU3dpdGNoZXIiLCJoYXNDbGFzcyIsInBhcmVudHMiLCJzdGF0aWNTZW9VcmxJZCIsImRhdGEiLCJpc0FjdGl2ZSIsImZpZWxkTmFtZSIsImF0dHIiLCJfdG9nZ2xlU3dpdGNoZXJzIiwiX2RlYWN0aXZhdGUiLCJfYWN0aXZhdGUiLCJyZXF1ZXN0T3B0aW9ucyIsInR5cGUiLCJ1cmwiLCJoYW5kbGVBbHdheXMiLCJyZW1vdmVDbGFzcyIsImhhbmRsZURvbmUiLCJfbm90aWZ5IiwicmVzcG9uc2UiLCJpbmNsdWRlcyIsImFqYXgiLCJkb25lIiwiZmFpbCIsImFsd2F5cyIsIiRjaGVja2JveCIsImZpbmQiLCJpc1N1Y2Nlc3NmdWwiLCJpbmZvX2JveCIsImRlbGV0ZUJ5U291cmNlIiwidGhlbiIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwiZG9FbmFibGUiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msa0NBREQsRUFHQyxDQUNDLE9BREQsRUFFSUYsR0FBR0csTUFGUCxvQkFIRCxFQVFDLFlBQVc7O0FBRVY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFHQTs7Ozs7QUFLQSxLQUFNQyxVQUFVO0FBQ2ZDLFlBQVU7QUFESyxFQUFoQjs7QUFJQTs7Ozs7QUFLQSxLQUFNQyxZQUFZO0FBQ2pCRCxrQkFBY0QsUUFBUUMsUUFETDtBQUVqQkUsMEJBQXNCSCxRQUFRQyxRQUE5QjtBQUZpQixFQUFsQjs7QUFNQTs7Ozs7QUFLQSxLQUFNRyxPQUFPO0FBQ1pDLFlBQWFDLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsQ0FBYixrREFEWTtBQUVaQyxjQUFlSixJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQWY7QUFGWSxFQUFiOztBQUtBOzs7OztBQUtBLEtBQU1iLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBLFVBQVNlLGdCQUFULEdBQTRCO0FBQzNCO0FBQ0EsTUFBTUMsYUFBYU4sSUFBSUMsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUNBQXhCLEVBQStELGlCQUEvRCxDQUFuQjtBQUNBLE1BQU1DLGVBQWVULElBQUlDLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLG9DQUF4QixFQUE4RCxpQkFBOUQsQ0FBckI7O0FBRUE7QUFDQVIsTUFBSVUsSUFBSixDQUFTQyxLQUFULENBQWVDLFdBQWYsQ0FBMkJOLFVBQTNCLEVBQXVDRyxZQUF2QztBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNJLGlCQUFULENBQTJCQyxLQUEzQixFQUFrQztBQUNqQztBQUNBLE1BQU1DLFVBQVV0QixFQUFFcUIsTUFBTUUsTUFBUixDQUFoQjs7QUFFQTtBQUNBLE1BQU1DLG1CQUFtQkYsUUFBUUcsUUFBUixDQUFpQnhCLFFBQVFDLFFBQXpCLElBQXFDb0IsT0FBckMsR0FBK0NBLFFBQVFJLE9BQVIsQ0FBZ0J2QixVQUFVRCxRQUExQixDQUF4RTs7QUFFQTtBQUNBLE1BQU15QixpQkFBaUJILGlCQUFpQkUsT0FBakIsQ0FBeUIsSUFBekIsRUFBK0JFLElBQS9CLENBQW9DLG1CQUFwQyxDQUF2Qjs7QUFFQTtBQUNBLE1BQU1DLFdBQVcsQ0FBQ0wsaUJBQWlCQyxRQUFqQixDQUEwQixTQUExQixDQUFsQjs7QUFFQTtBQUNBLE1BQU1LLFlBQVlSLFFBQVFTLElBQVIsQ0FBYSxNQUFiLENBQWxCOztBQUVBO0FBQ0FDLG1CQUFpQixLQUFqQjs7QUFFQTtBQUNBSCxhQUFXSSxZQUFZTixjQUFaLEVBQTRCSCxnQkFBNUIsRUFBOENNLFNBQTlDLENBQVgsR0FBc0VJLFVBQVVQLGNBQVYsRUFBMEJILGdCQUExQixFQUE0Q00sU0FBNUMsQ0FBdEU7QUFDQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNHLFdBQVQsQ0FBcUJOLGNBQXJCLEVBQXFDSCxnQkFBckMsRUFBdURNLFNBQXZELEVBQWtFO0FBQ2pFO0FBQ0EsTUFBTUssaUJBQWlCO0FBQ3RCQyxTQUFNLE1BRGdCO0FBRXRCUixTQUFNLEVBQUNELDhCQUFELEVBQWlCRyxvQkFBakIsRUFGZ0I7QUFHdEJPLFFBQUtoQyxLQUFLTTtBQUhZLEdBQXZCOztBQU1BO0FBQ0EsTUFBTTJCLGVBQWUsU0FBZkEsWUFBZTtBQUFBLFVBQU1kLGlCQUFpQmUsV0FBakIsQ0FBNkIsVUFBN0IsQ0FBTjtBQUFBLEdBQXJCOztBQUVBO0FBQ0EsTUFBTUMsYUFBYSxTQUFiQSxVQUFhLFdBQVk7QUFDOUI7QUFDQVIsb0JBQWlCLElBQWpCOztBQUVBO0FBQ0FTLFdBQVFDLFNBQVNDLFFBQVQsQ0FBa0IsU0FBbEIsQ0FBUjtBQUNBLEdBTkQ7O0FBUUE7QUFDQTNDLElBQUU0QyxJQUFGLENBQU9ULGNBQVAsRUFDRVUsSUFERixDQUNPTCxVQURQLEVBRUVNLElBRkYsQ0FFT2xDLGdCQUZQLEVBR0VtQyxNQUhGLENBR1NULFlBSFQ7QUFJQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNKLFNBQVQsQ0FBbUJQLGNBQW5CLEVBQW1DSCxnQkFBbkMsRUFBcURNLFNBQXJELEVBQWdFO0FBQy9EO0FBQ0EsTUFBTUssaUJBQWlCO0FBQ3RCQyxTQUFNLE1BRGdCO0FBRXRCUixTQUFNLEVBQUNELDhCQUFELEVBQWlCRyxvQkFBakIsRUFGZ0I7QUFHdEJPLFFBQUtoQyxLQUFLQztBQUhZLEdBQXZCOztBQU1BO0FBQ0EsTUFBTWdDLGVBQWUsU0FBZkEsWUFBZTtBQUFBLFVBQU1OLGlCQUFpQixJQUFqQixDQUFOO0FBQUEsR0FBckI7O0FBRUE7QUFDQSxNQUFNUSxhQUFhLFNBQWJBLFVBQWEsV0FBWTtBQUM5QjtBQUNBLE9BQU1RLFlBQVl4QixpQkFBaUJ5QixJQUFqQixDQUFzQixXQUF0QixDQUFsQjs7QUFFQTtBQUNBakIsb0JBQWlCLElBQWpCOztBQUVBO0FBQ0FnQixhQUFVOUMsUUFBVixDQUFtQixTQUFuQixFQUE4QixJQUE5Qjs7QUFFQTtBQUNBdUMsV0FBUUMsU0FBU0MsUUFBVCxDQUFrQixTQUFsQixDQUFSO0FBQ0EsR0FaRDs7QUFjQTtBQUNBM0MsSUFBRTRDLElBQUYsQ0FBT1QsY0FBUCxFQUNFVSxJQURGLENBQ09MLFVBRFAsRUFFRU0sSUFGRixDQUVPbEMsZ0JBRlAsRUFHRW1DLE1BSEYsQ0FHU1QsWUFIVDtBQUlBOztBQUVEOzs7Ozs7OztBQVFBLFVBQVNHLE9BQVQsQ0FBaUJTLFlBQWpCLEVBQStCO0FBQzlCLE1BQUlBLFlBQUosRUFBa0I7QUFDakIzQyxPQUFJVSxJQUFKLENBQVNrQyxRQUFULENBQWtCQyxjQUFsQixDQUFpQyxhQUFqQyxFQUFnREMsSUFBaEQsQ0FBcUQ7QUFBQSxXQUFNOUMsSUFBSVUsSUFBSixDQUFTa0MsUUFBVCxDQUFrQkcsaUJBQWxCLEVBQU47QUFBQSxJQUFyRDtBQUNBLEdBRkQsTUFFTztBQUNOL0MsT0FBSVUsSUFBSixDQUFTQyxLQUFULENBQWVDLFdBQWYsQ0FDQ1osSUFBSUMsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUNBQXhCLEVBQStELGlCQUEvRCxDQURELEVBRUNSLElBQUlDLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLG9DQUF4QixFQUE4RCxpQkFBOUQsQ0FGRDtBQUlBO0FBQ0Q7O0FBRUQ7Ozs7O0FBS0EsVUFBU2lCLGdCQUFULENBQTBCdUIsUUFBMUIsRUFBb0M7QUFDbkN4RCxRQUFNa0QsSUFBTixDQUFXOUMsVUFBVUQsUUFBckIsR0FBa0NxRCxXQUFXLFFBQVgsR0FBc0IsS0FBeEQsYUFBc0UsVUFBdEU7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUExRCxRQUFPMkQsSUFBUCxHQUFjLGdCQUFRO0FBQ3JCO0FBQ0F6RCxRQUFNMEQsRUFBTixDQUFTLFFBQVQsRUFBbUJ0RCxVQUFVRCxRQUE3QixFQUF1Q2tCLGlCQUF2Qzs7QUFFQTtBQUNBeUI7QUFDQSxFQU5EOztBQVFBLFFBQU9oRCxNQUFQO0FBQ0EsQ0E1TkYiLCJmaWxlIjoic3RhdGljX3Nlb191cmxzL3N0YXRpY19zZW9fdXJsc19pbmRleF9jaGVja2JveGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzdGF0aWNfc2VvX3VybHNfaW5kZXhfY2hlY2tib3hlcy5qcyAyMDE3LTA1LTI5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBTdGF0aWMgU2VvIFVybHMgT3ZlcnZpZXcgU3RhcnQgUGFnZSBPcHRpb25cbiAqXG4gKiBIYW5kbGVzIHRoZSBzd2l0Y2hlcnMgdG9nZ2xpbmcuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3N0YXRpY19zZW9fdXJsc19pbmRleF9jaGVja2JveGVzJyxcblx0XG5cdFtcblx0XHQnbW9kYWwnLFxuXHRcdGAke2d4LnNvdXJjZX0vbGlicy9pbmZvX2JveGBcblx0XSxcblx0XG5cdGZ1bmN0aW9uKCkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBDU1MgY2xhc3MgbmFtZXMuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGNsYXNzZXMgPSB7XG5cdFx0XHRzd2l0Y2hlcjogJ3N3aXRjaGVyJyxcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNlbGVjdG9yIFN0cmluZ3Ncblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgc2VsZWN0b3JzID0ge1xuXHRcdFx0c3dpdGNoZXI6IGAuJHtjbGFzc2VzLnN3aXRjaGVyfWAsXG5cdFx0XHRzd2l0Y2hlckNoZWNrYm94OiBgLiR7Y2xhc3Nlcy5zd2l0Y2hlcn0gOmNoZWNrYm94YCxcblx0XHR9O1xuXHRcdFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVSSSBtYXAuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IHVyaXMgPSB7XG5cdFx0XHRhY3RpdmF0ZTogYCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2FkbWluL2FkbWluLnBocD9kbz1TdGF0aWNTZW9VcmxBamF4L0FjdGl2YXRlYCxcblx0XHRcdGRlYWN0aXZhdGU6IGAke2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpfS9hZG1pbi9hZG1pbi5waHA/ZG89U3RhdGljU2VvVXJsQWpheC9EZWFjdGl2YXRlYCxcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2hvd3MgdGhlIHN1Ym1pdCBlcnJvciBtZXNzYWdlIG1vZGFsLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9zaG93RmFpbE1lc3NhZ2UoKSB7XG5cdFx0XHQvLyBNZXNzYWdlIHRleHRzLlxuXHRcdFx0Y29uc3QgZXJyb3JUaXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTVEFUSUNfU0VPX1VSTF9DSEVDS0JPWF9FUlJPUl9USVRMRScsICdzdGF0aWNfc2VvX3VybHMnKTtcblx0XHRcdGNvbnN0IGVycm9yTWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTVEFUSUNfU0VPX1VSTF9DSEVDS0JPWF9FUlJPUl9URVhUJywgJ3N0YXRpY19zZW9fdXJscycpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoZXJyb3JUaXRsZSwgZXJyb3JNZXNzYWdlKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgc3dpdGNoZXJzIGNoYW5nZSBldmVudHMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlciBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25Td2l0Y2hlckNoYW5nZShldmVudCkge1xuXHRcdFx0Ly8gQ2xpY2tlZCBlbGVtZW50LlxuXHRcdFx0Y29uc3QgJHRhcmdldCA9ICQoZXZlbnQudGFyZ2V0KTtcblx0XHRcdFxuXHRcdFx0Ly8gQ2xpY2tlZCBzd2l0Y2hlciBlbGVtZW50LlxuXHRcdFx0Y29uc3QgJGNsaWNrZWRTd2l0Y2hlciA9ICR0YXJnZXQuaGFzQ2xhc3MoY2xhc3Nlcy5zd2l0Y2hlcikgPyAkdGFyZ2V0IDogJHRhcmdldC5wYXJlbnRzKHNlbGVjdG9ycy5zd2l0Y2hlcik7XG5cdFx0XHRcblx0XHRcdC8vIENsaWNrZWQgc3RhdGljIHNlbyB1cmwgSUQuXG5cdFx0XHRjb25zdCBzdGF0aWNTZW9VcmxJZCA9ICRjbGlja2VkU3dpdGNoZXIucGFyZW50cygndHInKS5kYXRhKCdzdGF0aWMtc2VvLXVybC1pZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBJcyBzdGF0aWNQYWdlIHNldCBhcyBzdGFydCBwYWdlIHN0YXRpYyBwYWdlP1xuXHRcdFx0Y29uc3QgaXNBY3RpdmUgPSAhJGNsaWNrZWRTd2l0Y2hlci5oYXNDbGFzcygnY2hlY2tlZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBXaGljaCBmaWVsZCBzaG91bGQgYmUgdXBkYXRlZFxuXHRcdFx0Y29uc3QgZmllbGROYW1lID0gJHRhcmdldC5hdHRyKCduYW1lJyk7XG5cdFx0XHRcblx0XHRcdC8vIERpc2FibGUgYWxsIHN3aXRjaGVycy5cblx0XHRcdF90b2dnbGVTd2l0Y2hlcnMoZmFsc2UpO1xuXHRcdFx0XG5cdFx0XHQvLyBBY3RpdmF0ZSBvciBkZWFjdGl2YXRlIHN0YXRpYyBzZW8gdXJsIGRlcGVuZGluZyBvbiB0aGUgc3RhdGUuXG5cdFx0XHRpc0FjdGl2ZSA/IF9kZWFjdGl2YXRlKHN0YXRpY1Nlb1VybElkLCAkY2xpY2tlZFN3aXRjaGVyLCBmaWVsZE5hbWUpIDogX2FjdGl2YXRlKHN0YXRpY1Nlb1VybElkLCAkY2xpY2tlZFN3aXRjaGVyLCBmaWVsZE5hbWUpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWFjdGl2YXRlcyB0aGUgc3RhdGljIHNlbyB1cmwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge051bWJlcn0gc3RhdGljU2VvVXJsSWQgU3RhdGljIHNlbyB1cmwgSUQuXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICRjbGlja2VkU3dpdGNoZXIgQ2xpY2tlZCBzdGF0aWMgc2VvIHVybCBlbGVtZW50LlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSBmaWVsZE5hbWUgRmllbGQgbmFtZSB0byBiZSBkZWFjdGl2YXRlZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZGVhY3RpdmF0ZShzdGF0aWNTZW9VcmxJZCwgJGNsaWNrZWRTd2l0Y2hlciwgZmllbGROYW1lKSB7XG5cdFx0XHQvLyBSZXF1ZXN0IG9wdGlvbnMuXG5cdFx0XHRjb25zdCByZXF1ZXN0T3B0aW9ucyA9IHtcblx0XHRcdFx0dHlwZTogJ1BPU1QnLFxuXHRcdFx0XHRkYXRhOiB7c3RhdGljU2VvVXJsSWQsIGZpZWxkTmFtZX0sXG5cdFx0XHRcdHVybDogdXJpcy5kZWFjdGl2YXRlLFxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gSGFuZGxlcyB0aGUgJ2Fsd2F5cycgY2FzZSBieSBlbmFibGluZyB0aGUgY2xpY2tlZCBzdGF0aWMgc2VvIHVybC5cblx0XHRcdGNvbnN0IGhhbmRsZUFsd2F5cyA9ICgpID0+ICRjbGlja2VkU3dpdGNoZXIucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0XHRcblx0XHRcdC8vIEhhbmRsZXMgdGhlICdkb25lJyBjYXNlIHdpdGggdGhlIHNlcnZlciByZXNwb25zZS5cblx0XHRcdGNvbnN0IGhhbmRsZURvbmUgPSByZXNwb25zZSA9PiB7XG5cdFx0XHRcdC8vIEVuYWJsZSBhbGwgc3dpdGNoZXJzLlxuXHRcdFx0XHRfdG9nZ2xlU3dpdGNoZXJzKHRydWUpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gTm90aWZ5IHVzZXIuXG5cdFx0XHRcdF9ub3RpZnkocmVzcG9uc2UuaW5jbHVkZXMoJ3N1Y2Nlc3MnKSk7XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBQZXJmb3JtIHJlcXVlc3QuXG5cdFx0XHQkLmFqYXgocmVxdWVzdE9wdGlvbnMpXG5cdFx0XHRcdC5kb25lKGhhbmRsZURvbmUpXG5cdFx0XHRcdC5mYWlsKF9zaG93RmFpbE1lc3NhZ2UpXG5cdFx0XHRcdC5hbHdheXMoaGFuZGxlQWx3YXlzKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQWN0aXZhdGVzIHRoZSBzdGF0aWMgc2VvIHVybC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7TnVtYmVyfSBzdGF0aWNTZW9VcmxJZCBTdGF0aWMgc2VvIHVybCBJRC5cblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gJGNsaWNrZWRTd2l0Y2hlciBDbGlja2VkIHN0YXRpYyBzZW8gdXJsIGVsZW1lbnQuXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGZpZWxkTmFtZSBGaWVsZCBuYW1lIHRvIGJlIGFjdGl2YXRlZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfYWN0aXZhdGUoc3RhdGljU2VvVXJsSWQsICRjbGlja2VkU3dpdGNoZXIsIGZpZWxkTmFtZSkge1xuXHRcdFx0Ly8gUmVxdWVzdCBvcHRpb25zLlxuXHRcdFx0Y29uc3QgcmVxdWVzdE9wdGlvbnMgPSB7XG5cdFx0XHRcdHR5cGU6ICdQT1NUJyxcblx0XHRcdFx0ZGF0YToge3N0YXRpY1Nlb1VybElkLCBmaWVsZE5hbWV9LFxuXHRcdFx0XHR1cmw6IHVyaXMuYWN0aXZhdGUsXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBIYW5kbGVzIHRoZSAnYWx3YXlzJyBjYXNlIGJ5IGVuYWJsaW5nIHRoZSBjbGlja2VkIHN0YXRpYyBzZW8gdXJsLlxuXHRcdFx0Y29uc3QgaGFuZGxlQWx3YXlzID0gKCkgPT4gX3RvZ2dsZVN3aXRjaGVycyh0cnVlKTtcblx0XHRcdFxuXHRcdFx0Ly8gSGFuZGxlcyB0aGUgJ2RvbmUnIGNhc2Ugd2l0aCB0aGUgc2VydmVyIHJlc3BvbnNlLlxuXHRcdFx0Y29uc3QgaGFuZGxlRG9uZSA9IHJlc3BvbnNlID0+IHtcblx0XHRcdFx0Ly8gQ2xpY2tlZCBzd2l0Y2hlcidzIGNoZWNrYm94LlxuXHRcdFx0XHRjb25zdCAkY2hlY2tib3ggPSAkY2xpY2tlZFN3aXRjaGVyLmZpbmQoJzpjaGVja2JveCcpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRW5hYmxlIGFsbCBzd2l0Y2hlcnMuXG5cdFx0XHRcdF90b2dnbGVTd2l0Y2hlcnModHJ1ZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBDaGVjayBzd2l0Y2hlci5cblx0XHRcdFx0JGNoZWNrYm94LnN3aXRjaGVyKCdjaGVja2VkJywgdHJ1ZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBOb3RpZnkgdXNlci5cblx0XHRcdFx0X25vdGlmeShyZXNwb25zZS5pbmNsdWRlcygnc3VjY2VzcycpKTtcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIFBlcmZvcm0gcmVxdWVzdC5cblx0XHRcdCQuYWpheChyZXF1ZXN0T3B0aW9ucylcblx0XHRcdFx0LmRvbmUoaGFuZGxlRG9uZSlcblx0XHRcdFx0LmZhaWwoX3Nob3dGYWlsTWVzc2FnZSlcblx0XHRcdFx0LmFsd2F5cyhoYW5kbGVBbHdheXMpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJZiB0aGUgc2VydmVyIHJlc3BvbnNlIGlzIHN1Y2Nlc3NmdWwsIGl0IHJlbW92ZXMgYW55IHByZXZpb3VzIG1lc3NhZ2VzIGFuZFxuXHRcdCAqIGFkZHMgbmV3IHN1Y2Nlc3MgbWVzc2FnZSB0byBhZG1pbiBpbmZvIGJveC5cblx0XHQgKlxuXHRcdCAqIE90aGVyd2lzZSBpdHMgc2hvd3MgYW4gZXJyb3IgbWVzc2FnZSBtb2RhbC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7Qm9vbGVhbn0gaXNTdWNjZXNzZnVsIElzIHRoZSBzZXJ2ZXIgcmVzcG9uc2Ugc3VjY2Vzc2Z1bD9cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfbm90aWZ5KGlzU3VjY2Vzc2Z1bCkge1xuXHRcdFx0aWYgKGlzU3VjY2Vzc2Z1bCkge1xuXHRcdFx0XHRqc2UubGlicy5pbmZvX2JveC5kZWxldGVCeVNvdXJjZSgnYWRtaW5BY3Rpb24nKS50aGVuKCgpID0+IGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKCkpXG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnU1RBVElDX1NFT19VUkxfQ0hFQ0tCT1hfRVJST1JfVElUTEUnLCAnc3RhdGljX3Nlb191cmxzJyksXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1NUQVRJQ19TRU9fVVJMX0NIRUNLQk9YX0VSUk9SX1RFWFQnLCAnc3RhdGljX3Nlb191cmxzJylcblx0XHRcdFx0KTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRW5hYmxlcyBvciBkaXNhYmxlcyB0aGUgc3dpdGNoZXJzLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtCb29sZWFufSBkb0VuYWJsZSBFbmFibGUgdGhlIHN3aXRjaGVycz9cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfdG9nZ2xlU3dpdGNoZXJzKGRvRW5hYmxlKSB7XG5cdFx0XHQkdGhpcy5maW5kKHNlbGVjdG9ycy5zd2l0Y2hlcilbYCR7ZG9FbmFibGUgPyAncmVtb3ZlJyA6ICdhZGQnfUNsYXNzYF0oJ2Rpc2FibGVkJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZG9uZSA9PiB7XG5cdFx0XHQvLyBMaXN0ZW4gdG8gc2V0IHN0YXJ0IHBhZ2UgZXZlbnQuXG5cdFx0XHQkdGhpcy5vbignY2hhbmdlJywgc2VsZWN0b3JzLnN3aXRjaGVyLCBfb25Td2l0Y2hlckNoYW5nZSk7XG5cdFx0XHRcblx0XHRcdC8vIEZpbmlzaCBpbml0aWFsaXphdGlvbi5cblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7XG4iXX0=
