'use strict';

/* --------------------------------------------------------------
 state.js 2016-06-20
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles the table state for filtering, pagination and sorting.
 *
 * This controller will update the window history with the current state of the table. It reacts
 * to specific events such as filtering, pagination and sorting changes. After the window history
 * is updated the user will be able to navigate forth or backwards.
 *
 * Notice #1: This module must handle the window's pop-state events and not other modules because
 * this will lead to unnecessary code duplication and multiple AJAX requests.
 *
 * Notice #1: The window state must be always in sync with the URL for easier manipulation.
 */
gx.controllers.module('state', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Window History Support
  *
  * @type {Boolean}
  */
	var historySupport = jse.core.config.get('history');

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get parsed state from the URL GET parameters.
  *
  * @return {Object} Returns the table state.
  */
	function _getState() {
		return $.deparam(window.location.search.slice(1));
	}

	/**
  * Set the state to the browser's history.
  *
  * The state is stored for enabling back and forth navigation from the browser.
  *
  * @param {Object} state Contains the new table state.
  */
	function _setState(state) {
		var url = window.location.origin + window.location.pathname + '?' + $.param(state);
		window.history.pushState(state, '', url);
	}

	/**
  * Update page navigation state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} pagination Contains the DataTable pagination info.
  */
	function _onPageChange(event, pagination) {
		var state = _getState();

		state.page = pagination.page + 1;

		_setState(state);
	}

	/**
  * Update page length state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Number} length New page length.
  */
	function _onLengthChange(event, length) {
		var state = _getState();

		state.page = 1;
		state.length = length;

		_setState(state);
	}

	/**
  * Update filter state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} filter Contains the filtering values.
  */
	function _onFilterChange(event, filter) {
		var state = _getState();

		state.page = 1;
		state.filter = filter;

		_setState(state);
	}

	/**
  * Update sort state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} sort Contains column sorting info {index, name, direction}.
  */
	function _onSortChange(event, sort) {
		var state = _getState();

		state.sort = (sort.direction === 'desc' ? '-' : '+') + sort.name;

		_setState(state);
	}

	/**
  * Set the correct table state.
  *
  * This method will parse the new popped state and apply it on the table. It must be the only place where this
  * happens in order to avoid multiple AJAX requests and data collisions.
  *
  * @param {jQuery.Event} event
  */
	function _onWindowPopState(event) {
		var state = event.originalEvent.state || {};

		if (state.page) {
			$this.find('.page-navigation select').val(state.page);
			$this.DataTable().page(parseInt(state.page) - 1);
		}

		if (state.length) {
			$this.find('.page-length select').val(state.length);
			$this.DataTable().page.len(parseInt(state.length));
		}

		if (state.sort) {
			var _$this$DataTable$init = $this.DataTable().init(),
			    columns = _$this$DataTable$init.columns;

			var direction = state.sort.charAt(0) === '-' ? 'desc' : 'asc';
			var name = state.sort.slice(1);
			var index = 1; // Default Value

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = columns[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var column = _step.value;

					if (column.name === name) {
						index = columns.indexOf(column);
						break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			$this.DataTable().order([index, direction]);
		}

		if (state.filter) {
			// Update the filtering input elements. 
			for (var _column in state.filter) {
				var value = state.filter[_column];

				if (value.constructor === Array) {
					value = value.join('||'); // Join arrays into a single string.
				}

				$this.DataTable().column(_column + ':name').search(value);
			}
		}

		$this.DataTable().draw(false);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		if (historySupport) {
			$this.on('datatable_custom_pagination:page_change', _onPageChange).on('datatable_custom_pagination:length_change', _onLengthChange).on('datatable_custom_sorting:change', _onSortChange).on('orders_overview_filter:change', _onFilterChange);

			$(window).on('popstate', _onWindowPopState);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vdmVydmlldy9zdGF0ZS5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImhpc3RvcnlTdXBwb3J0IiwiY29yZSIsImNvbmZpZyIsImdldCIsIl9nZXRTdGF0ZSIsImRlcGFyYW0iLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInNlYXJjaCIsInNsaWNlIiwiX3NldFN0YXRlIiwic3RhdGUiLCJ1cmwiLCJvcmlnaW4iLCJwYXRobmFtZSIsInBhcmFtIiwiaGlzdG9yeSIsInB1c2hTdGF0ZSIsIl9vblBhZ2VDaGFuZ2UiLCJldmVudCIsInBhZ2luYXRpb24iLCJwYWdlIiwiX29uTGVuZ3RoQ2hhbmdlIiwibGVuZ3RoIiwiX29uRmlsdGVyQ2hhbmdlIiwiZmlsdGVyIiwiX29uU29ydENoYW5nZSIsInNvcnQiLCJkaXJlY3Rpb24iLCJuYW1lIiwiX29uV2luZG93UG9wU3RhdGUiLCJvcmlnaW5hbEV2ZW50IiwiZmluZCIsInZhbCIsIkRhdGFUYWJsZSIsInBhcnNlSW50IiwibGVuIiwiaW5pdCIsImNvbHVtbnMiLCJjaGFyQXQiLCJpbmRleCIsImNvbHVtbiIsImluZGV4T2YiLCJvcmRlciIsInZhbHVlIiwiY29uc3RydWN0b3IiLCJBcnJheSIsImpvaW4iLCJkcmF3IiwiZG9uZSIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7OztBQVlBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxPQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixrREFIRCxFQU9DLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1MLFNBQVMsRUFBZjs7QUFFQTs7Ozs7QUFLQSxLQUFNTSxpQkFBaUJMLElBQUlNLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsU0FBcEIsQ0FBdkI7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNDLFNBQVQsR0FBcUI7QUFDcEIsU0FBT0wsRUFBRU0sT0FBRixDQUFVQyxPQUFPQyxRQUFQLENBQWdCQyxNQUFoQixDQUF1QkMsS0FBdkIsQ0FBNkIsQ0FBN0IsQ0FBVixDQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTQyxTQUFULENBQW1CQyxLQUFuQixFQUEwQjtBQUN6QixNQUFNQyxNQUFNTixPQUFPQyxRQUFQLENBQWdCTSxNQUFoQixHQUF5QlAsT0FBT0MsUUFBUCxDQUFnQk8sUUFBekMsR0FBb0QsR0FBcEQsR0FBMERmLEVBQUVnQixLQUFGLENBQVFKLEtBQVIsQ0FBdEU7QUFDQUwsU0FBT1UsT0FBUCxDQUFlQyxTQUFmLENBQXlCTixLQUF6QixFQUFnQyxFQUFoQyxFQUFvQ0MsR0FBcEM7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU00sYUFBVCxDQUF1QkMsS0FBdkIsRUFBOEJDLFVBQTlCLEVBQTBDO0FBQ3pDLE1BQU1ULFFBQVFQLFdBQWQ7O0FBRUFPLFFBQU1VLElBQU4sR0FBYUQsV0FBV0MsSUFBWCxHQUFrQixDQUEvQjs7QUFFQVgsWUFBVUMsS0FBVjtBQUNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTVyxlQUFULENBQXlCSCxLQUF6QixFQUFnQ0ksTUFBaEMsRUFBd0M7QUFDdkMsTUFBTVosUUFBUVAsV0FBZDs7QUFFQU8sUUFBTVUsSUFBTixHQUFhLENBQWI7QUFDQVYsUUFBTVksTUFBTixHQUFlQSxNQUFmOztBQUVBYixZQUFVQyxLQUFWO0FBQ0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNhLGVBQVQsQ0FBeUJMLEtBQXpCLEVBQWdDTSxNQUFoQyxFQUF3QztBQUN2QyxNQUFNZCxRQUFRUCxXQUFkOztBQUVBTyxRQUFNVSxJQUFOLEdBQWEsQ0FBYjtBQUNBVixRQUFNYyxNQUFOLEdBQWVBLE1BQWY7O0FBRUFmLFlBQVVDLEtBQVY7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU2UsYUFBVCxDQUF1QlAsS0FBdkIsRUFBOEJRLElBQTlCLEVBQW9DO0FBQ25DLE1BQU1oQixRQUFRUCxXQUFkOztBQUVBTyxRQUFNZ0IsSUFBTixHQUFhLENBQUNBLEtBQUtDLFNBQUwsS0FBbUIsTUFBbkIsR0FBNEIsR0FBNUIsR0FBa0MsR0FBbkMsSUFBMENELEtBQUtFLElBQTVEOztBQUVBbkIsWUFBVUMsS0FBVjtBQUNBOztBQUVEOzs7Ozs7OztBQVFBLFVBQVNtQixpQkFBVCxDQUEyQlgsS0FBM0IsRUFBa0M7QUFDakMsTUFBTVIsUUFBUVEsTUFBTVksYUFBTixDQUFvQnBCLEtBQXBCLElBQTZCLEVBQTNDOztBQUVBLE1BQUlBLE1BQU1VLElBQVYsRUFBZ0I7QUFDZnZCLFNBQU1rQyxJQUFOLENBQVcseUJBQVgsRUFBc0NDLEdBQXRDLENBQTBDdEIsTUFBTVUsSUFBaEQ7QUFDQXZCLFNBQU1vQyxTQUFOLEdBQWtCYixJQUFsQixDQUF1QmMsU0FBU3hCLE1BQU1VLElBQWYsSUFBdUIsQ0FBOUM7QUFDQTs7QUFFRCxNQUFJVixNQUFNWSxNQUFWLEVBQWtCO0FBQ2pCekIsU0FBTWtDLElBQU4sQ0FBVyxxQkFBWCxFQUFrQ0MsR0FBbEMsQ0FBc0N0QixNQUFNWSxNQUE1QztBQUNBekIsU0FBTW9DLFNBQU4sR0FBa0JiLElBQWxCLENBQXVCZSxHQUF2QixDQUEyQkQsU0FBU3hCLE1BQU1ZLE1BQWYsQ0FBM0I7QUFDQTs7QUFFRCxNQUFJWixNQUFNZ0IsSUFBVixFQUFnQjtBQUFBLCtCQUNHN0IsTUFBTW9DLFNBQU4sR0FBa0JHLElBQWxCLEVBREg7QUFBQSxPQUNSQyxPQURRLHlCQUNSQSxPQURROztBQUVmLE9BQU1WLFlBQVlqQixNQUFNZ0IsSUFBTixDQUFXWSxNQUFYLENBQWtCLENBQWxCLE1BQXlCLEdBQXpCLEdBQStCLE1BQS9CLEdBQXdDLEtBQTFEO0FBQ0EsT0FBTVYsT0FBT2xCLE1BQU1nQixJQUFOLENBQVdsQixLQUFYLENBQWlCLENBQWpCLENBQWI7QUFDQSxPQUFJK0IsUUFBUSxDQUFaLENBSmUsQ0FJQTs7QUFKQTtBQUFBO0FBQUE7O0FBQUE7QUFNZix5QkFBbUJGLE9BQW5CLDhIQUE0QjtBQUFBLFNBQW5CRyxNQUFtQjs7QUFDM0IsU0FBSUEsT0FBT1osSUFBUCxLQUFnQkEsSUFBcEIsRUFBMEI7QUFDekJXLGNBQVFGLFFBQVFJLE9BQVIsQ0FBZ0JELE1BQWhCLENBQVI7QUFDQTtBQUNBO0FBQ0Q7QUFYYztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWFmM0MsU0FBTW9DLFNBQU4sR0FBa0JTLEtBQWxCLENBQXdCLENBQUNILEtBQUQsRUFBUVosU0FBUixDQUF4QjtBQUNBOztBQUVELE1BQUlqQixNQUFNYyxNQUFWLEVBQWtCO0FBQ2pCO0FBQ0EsUUFBSyxJQUFJZ0IsT0FBVCxJQUFtQjlCLE1BQU1jLE1BQXpCLEVBQWlDO0FBQ2hDLFFBQUltQixRQUFRakMsTUFBTWMsTUFBTixDQUFhZ0IsT0FBYixDQUFaOztBQUVBLFFBQUlHLE1BQU1DLFdBQU4sS0FBc0JDLEtBQTFCLEVBQWlDO0FBQ2hDRixhQUFRQSxNQUFNRyxJQUFOLENBQVcsSUFBWCxDQUFSLENBRGdDLENBQ047QUFDMUI7O0FBRURqRCxVQUFNb0MsU0FBTixHQUFrQk8sTUFBbEIsQ0FBNEJBLE9BQTVCLFlBQTJDakMsTUFBM0MsQ0FBa0RvQyxLQUFsRDtBQUNBO0FBQ0Q7O0FBRUQ5QyxRQUFNb0MsU0FBTixHQUFrQmMsSUFBbEIsQ0FBdUIsS0FBdkI7QUFFQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUF0RCxRQUFPMkMsSUFBUCxHQUFjLFVBQVNZLElBQVQsRUFBZTtBQUM1QixNQUFJakQsY0FBSixFQUFvQjtBQUNuQkYsU0FDRW9ELEVBREYsQ0FDSyx5Q0FETCxFQUNnRGhDLGFBRGhELEVBRUVnQyxFQUZGLENBRUssMkNBRkwsRUFFa0Q1QixlQUZsRCxFQUdFNEIsRUFIRixDQUdLLGlDQUhMLEVBR3dDeEIsYUFIeEMsRUFJRXdCLEVBSkYsQ0FJSywrQkFKTCxFQUlzQzFCLGVBSnRDOztBQU1BekIsS0FBRU8sTUFBRixFQUNFNEMsRUFERixDQUNLLFVBREwsRUFDaUJwQixpQkFEakI7QUFFQTs7QUFFRG1CO0FBQ0EsRUFiRDs7QUFlQSxRQUFPdkQsTUFBUDtBQUVBLENBbE1GIiwiZmlsZSI6Im9yZGVycy9vdmVydmlldy9zdGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBzdGF0ZS5qcyAyMDE2LTA2LTIwXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIEhhbmRsZXMgdGhlIHRhYmxlIHN0YXRlIGZvciBmaWx0ZXJpbmcsIHBhZ2luYXRpb24gYW5kIHNvcnRpbmcuXHJcbiAqXHJcbiAqIFRoaXMgY29udHJvbGxlciB3aWxsIHVwZGF0ZSB0aGUgd2luZG93IGhpc3Rvcnkgd2l0aCB0aGUgY3VycmVudCBzdGF0ZSBvZiB0aGUgdGFibGUuIEl0IHJlYWN0c1xyXG4gKiB0byBzcGVjaWZpYyBldmVudHMgc3VjaCBhcyBmaWx0ZXJpbmcsIHBhZ2luYXRpb24gYW5kIHNvcnRpbmcgY2hhbmdlcy4gQWZ0ZXIgdGhlIHdpbmRvdyBoaXN0b3J5XHJcbiAqIGlzIHVwZGF0ZWQgdGhlIHVzZXIgd2lsbCBiZSBhYmxlIHRvIG5hdmlnYXRlIGZvcnRoIG9yIGJhY2t3YXJkcy5cclxuICpcclxuICogTm90aWNlICMxOiBUaGlzIG1vZHVsZSBtdXN0IGhhbmRsZSB0aGUgd2luZG93J3MgcG9wLXN0YXRlIGV2ZW50cyBhbmQgbm90IG90aGVyIG1vZHVsZXMgYmVjYXVzZVxyXG4gKiB0aGlzIHdpbGwgbGVhZCB0byB1bm5lY2Vzc2FyeSBjb2RlIGR1cGxpY2F0aW9uIGFuZCBtdWx0aXBsZSBBSkFYIHJlcXVlc3RzLlxyXG4gKlxyXG4gKiBOb3RpY2UgIzE6IFRoZSB3aW5kb3cgc3RhdGUgbXVzdCBiZSBhbHdheXMgaW4gc3luYyB3aXRoIHRoZSBVUkwgZm9yIGVhc2llciBtYW5pcHVsYXRpb24uXHJcbiAqL1xyXG5neC5jb250cm9sbGVycy5tb2R1bGUoXHJcblx0J3N0YXRlJyxcclxuXHRcclxuXHRbXHJcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LWRlcGFyYW0vanF1ZXJ5LWRlcGFyYW0ubWluLmpzYCxcclxuXHRdLFxyXG5cdFxyXG5cdGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFxyXG5cdFx0J3VzZSBzdHJpY3QnO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIFZBUklBQkxFU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIFdpbmRvdyBIaXN0b3J5IFN1cHBvcnRcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7Qm9vbGVhbn1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgaGlzdG9yeVN1cHBvcnQgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdoaXN0b3J5Jyk7XHJcblx0XHRcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0Ly8gRlVOQ1RJT05TXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBHZXQgcGFyc2VkIHN0YXRlIGZyb20gdGhlIFVSTCBHRVQgcGFyYW1ldGVycy5cclxuXHRcdCAqXHJcblx0XHQgKiBAcmV0dXJuIHtPYmplY3R9IFJldHVybnMgdGhlIHRhYmxlIHN0YXRlLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfZ2V0U3RhdGUoKSB7XHJcblx0XHRcdHJldHVybiAkLmRlcGFyYW0od2luZG93LmxvY2F0aW9uLnNlYXJjaC5zbGljZSgxKSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogU2V0IHRoZSBzdGF0ZSB0byB0aGUgYnJvd3NlcidzIGhpc3RvcnkuXHJcblx0XHQgKlxyXG5cdFx0ICogVGhlIHN0YXRlIGlzIHN0b3JlZCBmb3IgZW5hYmxpbmcgYmFjayBhbmQgZm9ydGggbmF2aWdhdGlvbiBmcm9tIHRoZSBicm93c2VyLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBzdGF0ZSBDb250YWlucyB0aGUgbmV3IHRhYmxlIHN0YXRlLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfc2V0U3RhdGUoc3RhdGUpIHtcclxuXHRcdFx0Y29uc3QgdXJsID0gd2luZG93LmxvY2F0aW9uLm9yaWdpbiArIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSArICc/JyArICQucGFyYW0oc3RhdGUpO1xyXG5cdFx0XHR3aW5kb3cuaGlzdG9yeS5wdXNoU3RhdGUoc3RhdGUsICcnLCB1cmwpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIFVwZGF0ZSBwYWdlIG5hdmlnYXRpb24gc3RhdGUuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXHJcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gcGFnaW5hdGlvbiBDb250YWlucyB0aGUgRGF0YVRhYmxlIHBhZ2luYXRpb24gaW5mby5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uUGFnZUNoYW5nZShldmVudCwgcGFnaW5hdGlvbikge1xyXG5cdFx0XHRjb25zdCBzdGF0ZSA9IF9nZXRTdGF0ZSgpO1xyXG5cdFx0XHRcclxuXHRcdFx0c3RhdGUucGFnZSA9IHBhZ2luYXRpb24ucGFnZSArIDE7XHJcblx0XHRcdFxyXG5cdFx0XHRfc2V0U3RhdGUoc3RhdGUpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIFVwZGF0ZSBwYWdlIGxlbmd0aCBzdGF0ZS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdC5cclxuXHRcdCAqIEBwYXJhbSB7TnVtYmVyfSBsZW5ndGggTmV3IHBhZ2UgbGVuZ3RoLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25MZW5ndGhDaGFuZ2UoZXZlbnQsIGxlbmd0aCkge1xyXG5cdFx0XHRjb25zdCBzdGF0ZSA9IF9nZXRTdGF0ZSgpO1xyXG5cdFx0XHRcclxuXHRcdFx0c3RhdGUucGFnZSA9IDE7XHJcblx0XHRcdHN0YXRlLmxlbmd0aCA9IGxlbmd0aDtcclxuXHRcdFx0XHJcblx0XHRcdF9zZXRTdGF0ZShzdGF0ZSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogVXBkYXRlIGZpbHRlciBzdGF0ZS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdC5cclxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBmaWx0ZXIgQ29udGFpbnMgdGhlIGZpbHRlcmluZyB2YWx1ZXMuXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vbkZpbHRlckNoYW5nZShldmVudCwgZmlsdGVyKSB7XHJcblx0XHRcdGNvbnN0IHN0YXRlID0gX2dldFN0YXRlKCk7XHJcblx0XHRcdFxyXG5cdFx0XHRzdGF0ZS5wYWdlID0gMTtcclxuXHRcdFx0c3RhdGUuZmlsdGVyID0gZmlsdGVyO1xyXG5cdFx0XHRcclxuXHRcdFx0X3NldFN0YXRlKHN0YXRlKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBVcGRhdGUgc29ydCBzdGF0ZS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdC5cclxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBzb3J0IENvbnRhaW5zIGNvbHVtbiBzb3J0aW5nIGluZm8ge2luZGV4LCBuYW1lLCBkaXJlY3Rpb259LlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25Tb3J0Q2hhbmdlKGV2ZW50LCBzb3J0KSB7XHJcblx0XHRcdGNvbnN0IHN0YXRlID0gX2dldFN0YXRlKCk7XHJcblx0XHRcdFxyXG5cdFx0XHRzdGF0ZS5zb3J0ID0gKHNvcnQuZGlyZWN0aW9uID09PSAnZGVzYycgPyAnLScgOiAnKycpICsgc29ydC5uYW1lO1xyXG5cdFx0XHRcclxuXHRcdFx0X3NldFN0YXRlKHN0YXRlKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBTZXQgdGhlIGNvcnJlY3QgdGFibGUgc3RhdGUuXHJcblx0XHQgKlxyXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCBwYXJzZSB0aGUgbmV3IHBvcHBlZCBzdGF0ZSBhbmQgYXBwbHkgaXQgb24gdGhlIHRhYmxlLiBJdCBtdXN0IGJlIHRoZSBvbmx5IHBsYWNlIHdoZXJlIHRoaXNcclxuXHRcdCAqIGhhcHBlbnMgaW4gb3JkZXIgdG8gYXZvaWQgbXVsdGlwbGUgQUpBWCByZXF1ZXN0cyBhbmQgZGF0YSBjb2xsaXNpb25zLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25XaW5kb3dQb3BTdGF0ZShldmVudCkge1xyXG5cdFx0XHRjb25zdCBzdGF0ZSA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQuc3RhdGUgfHwge307XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoc3RhdGUucGFnZSkge1xyXG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5wYWdlLW5hdmlnYXRpb24gc2VsZWN0JykudmFsKHN0YXRlLnBhZ2UpO1xyXG5cdFx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLnBhZ2UocGFyc2VJbnQoc3RhdGUucGFnZSkgLSAxKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0aWYgKHN0YXRlLmxlbmd0aCkge1xyXG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5wYWdlLWxlbmd0aCBzZWxlY3QnKS52YWwoc3RhdGUubGVuZ3RoKTtcclxuXHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5wYWdlLmxlbihwYXJzZUludChzdGF0ZS5sZW5ndGgpKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0aWYgKHN0YXRlLnNvcnQpIHtcclxuXHRcdFx0XHRjb25zdCB7Y29sdW1uc30gPSAkdGhpcy5EYXRhVGFibGUoKS5pbml0KCk7XHJcblx0XHRcdFx0Y29uc3QgZGlyZWN0aW9uID0gc3RhdGUuc29ydC5jaGFyQXQoMCkgPT09ICctJyA/ICdkZXNjJyA6ICdhc2MnO1xyXG5cdFx0XHRcdGNvbnN0IG5hbWUgPSBzdGF0ZS5zb3J0LnNsaWNlKDEpO1xyXG5cdFx0XHRcdGxldCBpbmRleCA9IDE7IC8vIERlZmF1bHQgVmFsdWVcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRmb3IgKGxldCBjb2x1bW4gb2YgY29sdW1ucykge1xyXG5cdFx0XHRcdFx0aWYgKGNvbHVtbi5uYW1lID09PSBuYW1lKSB7XHJcblx0XHRcdFx0XHRcdGluZGV4ID0gY29sdW1ucy5pbmRleE9mKGNvbHVtbik7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5vcmRlcihbaW5kZXgsIGRpcmVjdGlvbl0pO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoc3RhdGUuZmlsdGVyKSB7XHJcblx0XHRcdFx0Ly8gVXBkYXRlIHRoZSBmaWx0ZXJpbmcgaW5wdXQgZWxlbWVudHMuIFxyXG5cdFx0XHRcdGZvciAobGV0IGNvbHVtbiBpbiBzdGF0ZS5maWx0ZXIpIHtcclxuXHRcdFx0XHRcdGxldCB2YWx1ZSA9IHN0YXRlLmZpbHRlcltjb2x1bW5dO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRpZiAodmFsdWUuY29uc3RydWN0b3IgPT09IEFycmF5KSB7XHJcblx0XHRcdFx0XHRcdHZhbHVlID0gdmFsdWUuam9pbignfHwnKTsgLy8gSm9pbiBhcnJheXMgaW50byBhIHNpbmdsZSBzdHJpbmcuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmNvbHVtbihgJHtjb2x1bW59Om5hbWVgKS5zZWFyY2godmFsdWUpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuZHJhdyhmYWxzZSk7XHJcblx0XHRcdFxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdGlmIChoaXN0b3J5U3VwcG9ydCkge1xyXG5cdFx0XHRcdCR0aGlzXHJcblx0XHRcdFx0XHQub24oJ2RhdGF0YWJsZV9jdXN0b21fcGFnaW5hdGlvbjpwYWdlX2NoYW5nZScsIF9vblBhZ2VDaGFuZ2UpXHJcblx0XHRcdFx0XHQub24oJ2RhdGF0YWJsZV9jdXN0b21fcGFnaW5hdGlvbjpsZW5ndGhfY2hhbmdlJywgX29uTGVuZ3RoQ2hhbmdlKVxyXG5cdFx0XHRcdFx0Lm9uKCdkYXRhdGFibGVfY3VzdG9tX3NvcnRpbmc6Y2hhbmdlJywgX29uU29ydENoYW5nZSlcclxuXHRcdFx0XHRcdC5vbignb3JkZXJzX292ZXJ2aWV3X2ZpbHRlcjpjaGFuZ2UnLCBfb25GaWx0ZXJDaGFuZ2UpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdCQod2luZG93KVxyXG5cdFx0XHRcdFx0Lm9uKCdwb3BzdGF0ZScsIF9vbldpbmRvd1BvcFN0YXRlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0ZG9uZSgpO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0cmV0dXJuIG1vZHVsZTtcclxuXHRcdFxyXG5cdH0pOyJdfQ==
