'use strict';

/* --------------------------------------------------------------
 filter.js 2016-10-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Filter Controller
 *
 * Handles the QuickEdit properties table filtering.
 *
 * ### Methods
 *
 * **Reload Filtering Options**
 *
 * ```
 * // Reload the filter options with an AJAX request (optionally provide a second parameter for the AJAX URL).
 * $('.table-main').quick_edit_properties_filter('reload');
 * ```
 */
gx.controllers.module('properties_filter', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js', 'loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Enter Key Code
  *
  * @type {Number}
  */

	var ENTER_KEY_CODE = 13; // ENTER

	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	var $this = $(this);

	/**
  * Filter Row Selector
  *
  * @type {jQuery}
  */
	var $filter = $this.find('tr.properties-filter');

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = { bindings: {} };

	// Dynamically define the filter row data-bindings. 
	$filter.find('th').each(function () {
		var columnName = $(this).data('columnName');

		if (columnName === 'checkbox' || columnName === 'actions') {
			return true;
		}

		module.bindings[columnName] = $(this).find('input, select').first();
	});

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Reload filter options with an Ajax request.
  *
  * This function implements the $('.datatable').quick_edit_properties_filter('reload') which will reload the filtering
  * "multi_select" instances will new options. It must be used after some table data are changed and the filtering
  * options need to be updated.
  *
  * @param {String} url Optional, the URL to be used for fetching the options. Do not add the "pageToken"
  * parameter to URL, it will be appended in this method.
  */
	function _reload(url) {
		url = url || jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/FilterOptions';
		var data = { pageToken: jse.core.config.get('pageToken') };

		$.getJSON(url, data).done(function (response) {
			for (var column in response) {
				var $select = $filter.find('.SumoSelect > select.' + column);
				var currentValueBackup = $select.val(); // Will try to set it back if it still exists. 

				if (!$select.length) {
					return; // The select element was not found.
				}

				$select.empty();

				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = response[column][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var option = _step.value;

						$select.append(new Option(option.text, option.value));
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				if (currentValueBackup !== null) {
					$select.val(currentValueBackup);
				}

				$select.multi_select('refresh');
			}
		});
	}

	/**
  * Add public "quick_edit_properties_filter" method to jQuery in order.
  */
	function _addPublicMethod() {
		if ($.fn.quick_edit_properties_filter) {
			return;
		}

		$.fn.extend({
			quick_edit_properties_filter: function quick_edit_properties_filter(action) {
				for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
					args[_key - 1] = arguments[_key];
				}

				$.each(this, function () {
					switch (action) {
						case 'reload':
							_reload.apply(this, args);
							break;
					}
				});
			}
		});
	}

	/**
  * On Filter Button Click
  *
  * Apply the provided filters and update the table rows.
  */
	function _onApplyFiltersClick() {
		// Prepare the object with the final filtering data.
		var filter = {};

		$filter.find('th').each(function () {
			var columnName = $(this).data('columnName');

			if (columnName === 'checkbox' || columnName === 'actions') {
				return true;
			}

			var value = module.bindings[columnName].get();

			if (value) {
				filter[columnName] = value;
				$this.DataTable().column(columnName + ':name').search(value);
			} else {
				$this.DataTable().column(columnName + ':name').search('');
			}
		});

		$this.trigger('quick_edit_properties_filter:change', [filter]);
		$this.DataTable().draw();
	}

	/**
  * On Reset Button Click
  *
  * Reset the filter form and reload the table data without filtering.
  */
	function _onResetFiltersClick() {
		// Remove values from the input boxes.
		$filter.find('input, select').not('.length, .select-page-mode').val('');
		$filter.find('select').not('.length, .select-page-mode').multi_select('refresh');

		// Reset the filtering values.
		$this.DataTable().columns().search('').draw();

		// Trigger Event
		$this.trigger('quick_edit_properties_filter:change', [{}]);
	}

	/**
  * Apply the filters when the user presses the Enter key.
  *
  * @param {jQuery.Event} event
  */
	function _onInputTextKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$filter.find('.apply-properties-filters').trigger('click');
		}
	}

	/**
  * Parse the initial filtering parameters and apply them to the table.
  */
	function _parseFilteringParameters() {
		var _$$deparam = $.deparam(window.location.search.slice(1)),
		    filter = _$$deparam.filter;

		for (var name in filter) {
			var value = filter[name];

			if (module.bindings[name]) {
				module.bindings[name].set(value);
			}
		}
	}

	/**
  * Normalize array filtering values.
  *
  * By default datatables will concatenate array search values into a string separated with "," commas. This
  * is not acceptable though because some filtering elements may contain values with comma and thus the array
  * cannot be parsed from backend. This method will reset those cases back to arrays for a clearer transaction
  * with the backend.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {DataTables.Settings} settings DataTables settings object.
  * @param {Object} data Data that will be sent to the server in an object form.
  */
	function _normalizeArrayValues(event, settings, data) {
		var filter = {};

		for (var name in module.bindings) {
			var value = module.bindings[name].get();

			if (value && value.constructor === Array) {
				filter[name] = value;
			}
		}

		for (var entry in filter) {
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = data.columns[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var column = _step2.value;

					if (entry === column.name && filter[entry].constructor === Array) {
						column.search.value = filter[entry];
						break;
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Add public module method. 
		_addPublicMethod();

		// Parse filtering GET parameters. 
		_parseFilteringParameters();

		// Bind event handlers.
		$filter.on('keyup', 'input:text', _onInputTextKeyUp).on('click', '.apply-properties-filters', _onApplyFiltersClick);

		$filter.parents('.properties.modal').on('click', 'button.reset-properties-filters', _onResetFiltersClick);

		$this.on('preXhr.dt', _normalizeArrayValues);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc19maWx0ZXIuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCJFTlRFUl9LRVlfQ09ERSIsIiR0aGlzIiwiJCIsIiRmaWx0ZXIiLCJmaW5kIiwiYmluZGluZ3MiLCJlYWNoIiwiY29sdW1uTmFtZSIsImZpcnN0IiwiX3JlbG9hZCIsInVybCIsImNvcmUiLCJjb25maWciLCJnZXQiLCJwYWdlVG9rZW4iLCJnZXRKU09OIiwiZG9uZSIsInJlc3BvbnNlIiwiY29sdW1uIiwiJHNlbGVjdCIsImN1cnJlbnRWYWx1ZUJhY2t1cCIsInZhbCIsImxlbmd0aCIsImVtcHR5Iiwib3B0aW9uIiwiYXBwZW5kIiwiT3B0aW9uIiwidGV4dCIsInZhbHVlIiwibXVsdGlfc2VsZWN0IiwiX2FkZFB1YmxpY01ldGhvZCIsImZuIiwicXVpY2tfZWRpdF9wcm9wZXJ0aWVzX2ZpbHRlciIsImV4dGVuZCIsImFjdGlvbiIsImFyZ3MiLCJhcHBseSIsIl9vbkFwcGx5RmlsdGVyc0NsaWNrIiwiZmlsdGVyIiwiRGF0YVRhYmxlIiwic2VhcmNoIiwidHJpZ2dlciIsImRyYXciLCJfb25SZXNldEZpbHRlcnNDbGljayIsIm5vdCIsImNvbHVtbnMiLCJfb25JbnB1dFRleHRLZXlVcCIsImV2ZW50Iiwid2hpY2giLCJfcGFyc2VGaWx0ZXJpbmdQYXJhbWV0ZXJzIiwiZGVwYXJhbSIsIndpbmRvdyIsImxvY2F0aW9uIiwic2xpY2UiLCJuYW1lIiwic2V0IiwiX25vcm1hbGl6ZUFycmF5VmFsdWVzIiwic2V0dGluZ3MiLCJjb25zdHJ1Y3RvciIsIkFycmF5IiwiZW50cnkiLCJpbml0Iiwib24iLCJwYXJlbnRzIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7O0FBY0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLG1CQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixtREFFQyxpQkFGRCxDQUhELEVBUUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsaUJBQWlCLEVBQXZCLENBYmMsQ0FhYTs7QUFFM0I7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVUYsTUFBTUcsSUFBTixDQUFXLHNCQUFYLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1SLFNBQVMsRUFBQ1MsVUFBVSxFQUFYLEVBQWY7O0FBRUE7QUFDQUYsU0FBUUMsSUFBUixDQUFhLElBQWIsRUFBbUJFLElBQW5CLENBQXdCLFlBQVc7QUFDbEMsTUFBTUMsYUFBYUwsRUFBRSxJQUFGLEVBQVFILElBQVIsQ0FBYSxZQUFiLENBQW5COztBQUVBLE1BQUlRLGVBQWUsVUFBZixJQUE2QkEsZUFBZSxTQUFoRCxFQUEyRDtBQUMxRCxVQUFPLElBQVA7QUFDQTs7QUFFRFgsU0FBT1MsUUFBUCxDQUFnQkUsVUFBaEIsSUFBOEJMLEVBQUUsSUFBRixFQUFRRSxJQUFSLENBQWEsZUFBYixFQUE4QkksS0FBOUIsRUFBOUI7QUFDQSxFQVJEOztBQVVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQVVBLFVBQVNDLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQ3JCQSxRQUFNQSxPQUFPYixJQUFJYyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHlEQUE3QztBQUNBLE1BQU1kLE9BQU8sRUFBQ2UsV0FBV2pCLElBQUljLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FBWixFQUFiOztBQUVBWCxJQUFFYSxPQUFGLENBQVVMLEdBQVYsRUFBZVgsSUFBZixFQUFxQmlCLElBQXJCLENBQTBCLFVBQUNDLFFBQUQsRUFBYztBQUN2QyxRQUFLLElBQUlDLE1BQVQsSUFBbUJELFFBQW5CLEVBQTZCO0FBQzVCLFFBQU1FLFVBQVVoQixRQUFRQyxJQUFSLENBQWEsMEJBQTBCYyxNQUF2QyxDQUFoQjtBQUNBLFFBQU1FLHFCQUFxQkQsUUFBUUUsR0FBUixFQUEzQixDQUY0QixDQUVjOztBQUUxQyxRQUFJLENBQUNGLFFBQVFHLE1BQWIsRUFBcUI7QUFDcEIsWUFEb0IsQ0FDWjtBQUNSOztBQUVESCxZQUFRSSxLQUFSOztBQVI0QjtBQUFBO0FBQUE7O0FBQUE7QUFVNUIsMEJBQW1CTixTQUFTQyxNQUFULENBQW5CLDhIQUFxQztBQUFBLFVBQTVCTSxNQUE0Qjs7QUFDcENMLGNBQVFNLE1BQVIsQ0FBZSxJQUFJQyxNQUFKLENBQVdGLE9BQU9HLElBQWxCLEVBQXdCSCxPQUFPSSxLQUEvQixDQUFmO0FBQ0E7QUFaMkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFjNUIsUUFBSVIsdUJBQXVCLElBQTNCLEVBQWlDO0FBQ2hDRCxhQUFRRSxHQUFSLENBQVlELGtCQUFaO0FBQ0E7O0FBRURELFlBQVFVLFlBQVIsQ0FBcUIsU0FBckI7QUFDQTtBQUNELEdBckJEO0FBc0JBOztBQUVEOzs7QUFHQSxVQUFTQyxnQkFBVCxHQUE0QjtBQUMzQixNQUFJNUIsRUFBRTZCLEVBQUYsQ0FBS0MsNEJBQVQsRUFBdUM7QUFDdEM7QUFDQTs7QUFFRDlCLElBQUU2QixFQUFGLENBQUtFLE1BQUwsQ0FBWTtBQUNYRCxpQ0FBOEIsc0NBQVNFLE1BQVQsRUFBMEI7QUFBQSxzQ0FBTkMsSUFBTTtBQUFOQSxTQUFNO0FBQUE7O0FBQ3ZEakMsTUFBRUksSUFBRixDQUFPLElBQVAsRUFBYSxZQUFXO0FBQ3ZCLGFBQVE0QixNQUFSO0FBQ0MsV0FBSyxRQUFMO0FBQ0N6QixlQUFRMkIsS0FBUixDQUFjLElBQWQsRUFBb0JELElBQXBCO0FBQ0E7QUFIRjtBQUtBLEtBTkQ7QUFPQTtBQVRVLEdBQVo7QUFXQTs7QUFFRDs7Ozs7QUFLQSxVQUFTRSxvQkFBVCxHQUFnQztBQUMvQjtBQUNBLE1BQU1DLFNBQVMsRUFBZjs7QUFFQW5DLFVBQVFDLElBQVIsQ0FBYSxJQUFiLEVBQW1CRSxJQUFuQixDQUF3QixZQUFXO0FBQ2xDLE9BQU1DLGFBQWFMLEVBQUUsSUFBRixFQUFRSCxJQUFSLENBQWEsWUFBYixDQUFuQjs7QUFFQSxPQUFJUSxlQUFlLFVBQWYsSUFBNkJBLGVBQWUsU0FBaEQsRUFBMkQ7QUFDMUQsV0FBTyxJQUFQO0FBQ0E7O0FBRUQsT0FBSXFCLFFBQVFoQyxPQUFPUyxRQUFQLENBQWdCRSxVQUFoQixFQUE0Qk0sR0FBNUIsRUFBWjs7QUFFQSxPQUFJZSxLQUFKLEVBQVc7QUFDVlUsV0FBTy9CLFVBQVAsSUFBcUJxQixLQUFyQjtBQUNBM0IsVUFBTXNDLFNBQU4sR0FBa0JyQixNQUFsQixDQUE0QlgsVUFBNUIsWUFBK0NpQyxNQUEvQyxDQUFzRFosS0FBdEQ7QUFDQSxJQUhELE1BR087QUFDTjNCLFVBQU1zQyxTQUFOLEdBQWtCckIsTUFBbEIsQ0FBNEJYLFVBQTVCLFlBQStDaUMsTUFBL0MsQ0FBc0QsRUFBdEQ7QUFDQTtBQUNELEdBZkQ7O0FBaUJBdkMsUUFBTXdDLE9BQU4sQ0FBYyxxQ0FBZCxFQUFxRCxDQUFDSCxNQUFELENBQXJEO0FBQ0FyQyxRQUFNc0MsU0FBTixHQUFrQkcsSUFBbEI7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxvQkFBVCxHQUFnQztBQUMvQjtBQUNBeEMsVUFBUUMsSUFBUixDQUFhLGVBQWIsRUFBOEJ3QyxHQUE5QixDQUFrQyw0QkFBbEMsRUFBZ0V2QixHQUFoRSxDQUFvRSxFQUFwRTtBQUNBbEIsVUFBUUMsSUFBUixDQUFhLFFBQWIsRUFBdUJ3QyxHQUF2QixDQUEyQiw0QkFBM0IsRUFBeURmLFlBQXpELENBQXNFLFNBQXRFOztBQUVBO0FBQ0E1QixRQUFNc0MsU0FBTixHQUFrQk0sT0FBbEIsR0FBNEJMLE1BQTVCLENBQW1DLEVBQW5DLEVBQXVDRSxJQUF2Qzs7QUFFQTtBQUNBekMsUUFBTXdDLE9BQU4sQ0FBYyxxQ0FBZCxFQUFxRCxDQUFDLEVBQUQsQ0FBckQ7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTSyxpQkFBVCxDQUEyQkMsS0FBM0IsRUFBa0M7QUFDakMsTUFBSUEsTUFBTUMsS0FBTixLQUFnQmhELGNBQXBCLEVBQW9DO0FBQ25DRyxXQUFRQyxJQUFSLENBQWEsMkJBQWIsRUFBMENxQyxPQUExQyxDQUFrRCxPQUFsRDtBQUNBO0FBQ0Q7O0FBRUQ7OztBQUdBLFVBQVNRLHlCQUFULEdBQXFDO0FBQUEsbUJBQ25CL0MsRUFBRWdELE9BQUYsQ0FBVUMsT0FBT0MsUUFBUCxDQUFnQlosTUFBaEIsQ0FBdUJhLEtBQXZCLENBQTZCLENBQTdCLENBQVYsQ0FEbUI7QUFBQSxNQUM3QmYsTUFENkIsY0FDN0JBLE1BRDZCOztBQUdwQyxPQUFLLElBQUlnQixJQUFULElBQWlCaEIsTUFBakIsRUFBeUI7QUFDeEIsT0FBTVYsUUFBUVUsT0FBT2dCLElBQVAsQ0FBZDs7QUFFQSxPQUFJMUQsT0FBT1MsUUFBUCxDQUFnQmlELElBQWhCLENBQUosRUFBMkI7QUFDMUIxRCxXQUFPUyxRQUFQLENBQWdCaUQsSUFBaEIsRUFBc0JDLEdBQXRCLENBQTBCM0IsS0FBMUI7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7OztBQVlBLFVBQVM0QixxQkFBVCxDQUErQlQsS0FBL0IsRUFBc0NVLFFBQXRDLEVBQWdEMUQsSUFBaEQsRUFBc0Q7QUFDckQsTUFBTXVDLFNBQVMsRUFBZjs7QUFFQSxPQUFLLElBQUlnQixJQUFULElBQWlCMUQsT0FBT1MsUUFBeEIsRUFBa0M7QUFDakMsT0FBTXVCLFFBQVFoQyxPQUFPUyxRQUFQLENBQWdCaUQsSUFBaEIsRUFBc0J6QyxHQUF0QixFQUFkOztBQUVBLE9BQUllLFNBQVNBLE1BQU04QixXQUFOLEtBQXNCQyxLQUFuQyxFQUEwQztBQUN6Q3JCLFdBQU9nQixJQUFQLElBQWUxQixLQUFmO0FBQ0E7QUFDRDs7QUFFRCxPQUFLLElBQUlnQyxLQUFULElBQWtCdEIsTUFBbEIsRUFBMEI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFDekIsMEJBQW1CdkMsS0FBSzhDLE9BQXhCLG1JQUFpQztBQUFBLFNBQXhCM0IsTUFBd0I7O0FBQ2hDLFNBQUkwQyxVQUFVMUMsT0FBT29DLElBQWpCLElBQXlCaEIsT0FBT3NCLEtBQVAsRUFBY0YsV0FBZCxLQUE4QkMsS0FBM0QsRUFBa0U7QUFDakV6QyxhQUFPc0IsTUFBUCxDQUFjWixLQUFkLEdBQXNCVSxPQUFPc0IsS0FBUCxDQUF0QjtBQUNBO0FBQ0E7QUFDRDtBQU53QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT3pCO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBaEUsUUFBT2lFLElBQVAsR0FBYyxVQUFTN0MsSUFBVCxFQUFlO0FBQzVCO0FBQ0FjOztBQUVBO0FBQ0FtQjs7QUFFQTtBQUNBOUMsVUFDRTJELEVBREYsQ0FDSyxPQURMLEVBQ2MsWUFEZCxFQUM0QmhCLGlCQUQ1QixFQUVFZ0IsRUFGRixDQUVLLE9BRkwsRUFFYywyQkFGZCxFQUUyQ3pCLG9CQUYzQzs7QUFJQWxDLFVBQVE0RCxPQUFSLENBQWdCLG1CQUFoQixFQUNFRCxFQURGLENBQ0ssT0FETCxFQUNjLGlDQURkLEVBQ2lEbkIsb0JBRGpEOztBQUdBMUMsUUFBTTZELEVBQU4sQ0FBUyxXQUFULEVBQXNCTixxQkFBdEI7O0FBRUF4QztBQUNBLEVBbEJEOztBQW9CQSxRQUFPcEIsTUFBUDtBQUNBLENBelBGIiwiZmlsZSI6InF1aWNrX2VkaXQvbW9kYWxzL3Byb3BlcnRpZXMvcHJvcGVydGllc19maWx0ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGZpbHRlci5qcyAyMDE2LTEwLTE5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBQcm9wZXJ0aWVzIEZpbHRlciBDb250cm9sbGVyXG4gKlxuICogSGFuZGxlcyB0aGUgUXVpY2tFZGl0IHByb3BlcnRpZXMgdGFibGUgZmlsdGVyaW5nLlxuICpcbiAqICMjIyBNZXRob2RzXG4gKlxuICogKipSZWxvYWQgRmlsdGVyaW5nIE9wdGlvbnMqKlxuICpcbiAqIGBgYFxuICogLy8gUmVsb2FkIHRoZSBmaWx0ZXIgb3B0aW9ucyB3aXRoIGFuIEFKQVggcmVxdWVzdCAob3B0aW9uYWxseSBwcm92aWRlIGEgc2Vjb25kIHBhcmFtZXRlciBmb3IgdGhlIEFKQVggVVJMKS5cbiAqICQoJy50YWJsZS1tYWluJykucXVpY2tfZWRpdF9wcm9wZXJ0aWVzX2ZpbHRlcigncmVsb2FkJyk7XG4gKiBgYGBcbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQncHJvcGVydGllc19maWx0ZXInLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktZGVwYXJhbS9qcXVlcnktZGVwYXJhbS5taW4uanNgLFxuXHRcdCdsb2FkaW5nX3NwaW5uZXInXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEVudGVyIEtleSBDb2RlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7TnVtYmVyfVxuXHRcdCAqL1xuXHRcdGNvbnN0IEVOVEVSX0tFWV9DT0RFID0gMTM7IC8vIEVOVEVSXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaWx0ZXIgUm93IFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRmaWx0ZXIgPSAkdGhpcy5maW5kKCd0ci5wcm9wZXJ0aWVzLWZpbHRlcicpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7YmluZGluZ3M6IHt9fTtcblx0XHRcblx0XHQvLyBEeW5hbWljYWxseSBkZWZpbmUgdGhlIGZpbHRlciByb3cgZGF0YS1iaW5kaW5ncy4gXG5cdFx0JGZpbHRlci5maW5kKCd0aCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRjb25zdCBjb2x1bW5OYW1lID0gJCh0aGlzKS5kYXRhKCdjb2x1bW5OYW1lJyk7XG5cdFx0XHRcblx0XHRcdGlmIChjb2x1bW5OYW1lID09PSAnY2hlY2tib3gnIHx8IGNvbHVtbk5hbWUgPT09ICdhY3Rpb25zJykge1xuXHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0bW9kdWxlLmJpbmRpbmdzW2NvbHVtbk5hbWVdID0gJCh0aGlzKS5maW5kKCdpbnB1dCwgc2VsZWN0JykuZmlyc3QoKTtcblx0XHR9KTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZWxvYWQgZmlsdGVyIG9wdGlvbnMgd2l0aCBhbiBBamF4IHJlcXVlc3QuXG5cdFx0ICpcblx0XHQgKiBUaGlzIGZ1bmN0aW9uIGltcGxlbWVudHMgdGhlICQoJy5kYXRhdGFibGUnKS5xdWlja19lZGl0X3Byb3BlcnRpZXNfZmlsdGVyKCdyZWxvYWQnKSB3aGljaCB3aWxsIHJlbG9hZCB0aGUgZmlsdGVyaW5nXG5cdFx0ICogXCJtdWx0aV9zZWxlY3RcIiBpbnN0YW5jZXMgd2lsbCBuZXcgb3B0aW9ucy4gSXQgbXVzdCBiZSB1c2VkIGFmdGVyIHNvbWUgdGFibGUgZGF0YSBhcmUgY2hhbmdlZCBhbmQgdGhlIGZpbHRlcmluZ1xuXHRcdCAqIG9wdGlvbnMgbmVlZCB0byBiZSB1cGRhdGVkLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IHVybCBPcHRpb25hbCwgdGhlIFVSTCB0byBiZSB1c2VkIGZvciBmZXRjaGluZyB0aGUgb3B0aW9ucy4gRG8gbm90IGFkZCB0aGUgXCJwYWdlVG9rZW5cIlxuXHRcdCAqIHBhcmFtZXRlciB0byBVUkwsIGl0IHdpbGwgYmUgYXBwZW5kZWQgaW4gdGhpcyBtZXRob2QuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3JlbG9hZCh1cmwpIHtcblx0XHRcdHVybCA9IHVybCB8fCBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVF1aWNrRWRpdE92ZXJ2aWV3QWpheC9GaWx0ZXJPcHRpb25zJztcblx0XHRcdGNvbnN0IGRhdGEgPSB7cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKX07XG5cdFx0XHRcblx0XHRcdCQuZ2V0SlNPTih1cmwsIGRhdGEpLmRvbmUoKHJlc3BvbnNlKSA9PiB7XG5cdFx0XHRcdGZvciAobGV0IGNvbHVtbiBpbiByZXNwb25zZSkge1xuXHRcdFx0XHRcdGNvbnN0ICRzZWxlY3QgPSAkZmlsdGVyLmZpbmQoJy5TdW1vU2VsZWN0ID4gc2VsZWN0LicgKyBjb2x1bW4pO1xuXHRcdFx0XHRcdGNvbnN0IGN1cnJlbnRWYWx1ZUJhY2t1cCA9ICRzZWxlY3QudmFsKCk7IC8vIFdpbGwgdHJ5IHRvIHNldCBpdCBiYWNrIGlmIGl0IHN0aWxsIGV4aXN0cy4gXG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKCEkc2VsZWN0Lmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0cmV0dXJuOyAvLyBUaGUgc2VsZWN0IGVsZW1lbnQgd2FzIG5vdCBmb3VuZC5cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHNlbGVjdC5lbXB0eSgpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGZvciAobGV0IG9wdGlvbiBvZiByZXNwb25zZVtjb2x1bW5dKSB7XG5cdFx0XHRcdFx0XHQkc2VsZWN0LmFwcGVuZChuZXcgT3B0aW9uKG9wdGlvbi50ZXh0LCBvcHRpb24udmFsdWUpKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKGN1cnJlbnRWYWx1ZUJhY2t1cCAhPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0JHNlbGVjdC52YWwoY3VycmVudFZhbHVlQmFja3VwKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHNlbGVjdC5tdWx0aV9zZWxlY3QoJ3JlZnJlc2gnKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFkZCBwdWJsaWMgXCJxdWlja19lZGl0X3Byb3BlcnRpZXNfZmlsdGVyXCIgbWV0aG9kIHRvIGpRdWVyeSBpbiBvcmRlci5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfYWRkUHVibGljTWV0aG9kKCkge1xuXHRcdFx0aWYgKCQuZm4ucXVpY2tfZWRpdF9wcm9wZXJ0aWVzX2ZpbHRlcikge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCQuZm4uZXh0ZW5kKHtcblx0XHRcdFx0cXVpY2tfZWRpdF9wcm9wZXJ0aWVzX2ZpbHRlcjogZnVuY3Rpb24oYWN0aW9uLCAuLi5hcmdzKSB7XG5cdFx0XHRcdFx0JC5lYWNoKHRoaXMsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0c3dpdGNoIChhY3Rpb24pIHtcblx0XHRcdFx0XHRcdFx0Y2FzZSAncmVsb2FkJzpcblx0XHRcdFx0XHRcdFx0XHRfcmVsb2FkLmFwcGx5KHRoaXMsIGFyZ3MpO1xuXHRcdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogT24gRmlsdGVyIEJ1dHRvbiBDbGlja1xuXHRcdCAqXG5cdFx0ICogQXBwbHkgdGhlIHByb3ZpZGVkIGZpbHRlcnMgYW5kIHVwZGF0ZSB0aGUgdGFibGUgcm93cy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25BcHBseUZpbHRlcnNDbGljaygpIHtcblx0XHRcdC8vIFByZXBhcmUgdGhlIG9iamVjdCB3aXRoIHRoZSBmaW5hbCBmaWx0ZXJpbmcgZGF0YS5cblx0XHRcdGNvbnN0IGZpbHRlciA9IHt9O1xuXHRcdFx0XG5cdFx0XHQkZmlsdGVyLmZpbmQoJ3RoJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0Y29uc3QgY29sdW1uTmFtZSA9ICQodGhpcykuZGF0YSgnY29sdW1uTmFtZScpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKGNvbHVtbk5hbWUgPT09ICdjaGVja2JveCcgfHwgY29sdW1uTmFtZSA9PT0gJ2FjdGlvbnMnKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGxldCB2YWx1ZSA9IG1vZHVsZS5iaW5kaW5nc1tjb2x1bW5OYW1lXS5nZXQoKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICh2YWx1ZSkge1xuXHRcdFx0XHRcdGZpbHRlcltjb2x1bW5OYW1lXSA9IHZhbHVlO1xuXHRcdFx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmNvbHVtbihgJHtjb2x1bW5OYW1lfTpuYW1lYCkuc2VhcmNoKHZhbHVlKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oYCR7Y29sdW1uTmFtZX06bmFtZWApLnNlYXJjaCgnJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy50cmlnZ2VyKCdxdWlja19lZGl0X3Byb3BlcnRpZXNfZmlsdGVyOmNoYW5nZScsIFtmaWx0ZXJdKTtcblx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmRyYXcoKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogT24gUmVzZXQgQnV0dG9uIENsaWNrXG5cdFx0ICpcblx0XHQgKiBSZXNldCB0aGUgZmlsdGVyIGZvcm0gYW5kIHJlbG9hZCB0aGUgdGFibGUgZGF0YSB3aXRob3V0IGZpbHRlcmluZy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25SZXNldEZpbHRlcnNDbGljaygpIHtcblx0XHRcdC8vIFJlbW92ZSB2YWx1ZXMgZnJvbSB0aGUgaW5wdXQgYm94ZXMuXG5cdFx0XHQkZmlsdGVyLmZpbmQoJ2lucHV0LCBzZWxlY3QnKS5ub3QoJy5sZW5ndGgsIC5zZWxlY3QtcGFnZS1tb2RlJykudmFsKCcnKTtcblx0XHRcdCRmaWx0ZXIuZmluZCgnc2VsZWN0Jykubm90KCcubGVuZ3RoLCAuc2VsZWN0LXBhZ2UtbW9kZScpLm11bHRpX3NlbGVjdCgncmVmcmVzaCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZXNldCB0aGUgZmlsdGVyaW5nIHZhbHVlcy5cblx0XHRcdCR0aGlzLkRhdGFUYWJsZSgpLmNvbHVtbnMoKS5zZWFyY2goJycpLmRyYXcoKTtcblx0XHRcdFxuXHRcdFx0Ly8gVHJpZ2dlciBFdmVudFxuXHRcdFx0JHRoaXMudHJpZ2dlcigncXVpY2tfZWRpdF9wcm9wZXJ0aWVzX2ZpbHRlcjpjaGFuZ2UnLCBbe31dKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQXBwbHkgdGhlIGZpbHRlcnMgd2hlbiB0aGUgdXNlciBwcmVzc2VzIHRoZSBFbnRlciBrZXkuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25JbnB1dFRleHRLZXlVcChldmVudCkge1xuXHRcdFx0aWYgKGV2ZW50LndoaWNoID09PSBFTlRFUl9LRVlfQ09ERSkge1xuXHRcdFx0XHQkZmlsdGVyLmZpbmQoJy5hcHBseS1wcm9wZXJ0aWVzLWZpbHRlcnMnKS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBQYXJzZSB0aGUgaW5pdGlhbCBmaWx0ZXJpbmcgcGFyYW1ldGVycyBhbmQgYXBwbHkgdGhlbSB0byB0aGUgdGFibGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3BhcnNlRmlsdGVyaW5nUGFyYW1ldGVycygpIHtcblx0XHRcdGNvbnN0IHtmaWx0ZXJ9ID0gJC5kZXBhcmFtKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSkpO1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBuYW1lIGluIGZpbHRlcikge1xuXHRcdFx0XHRjb25zdCB2YWx1ZSA9IGZpbHRlcltuYW1lXTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmIChtb2R1bGUuYmluZGluZ3NbbmFtZV0pIHtcblx0XHRcdFx0XHRtb2R1bGUuYmluZGluZ3NbbmFtZV0uc2V0KHZhbHVlKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBOb3JtYWxpemUgYXJyYXkgZmlsdGVyaW5nIHZhbHVlcy5cblx0XHQgKlxuXHRcdCAqIEJ5IGRlZmF1bHQgZGF0YXRhYmxlcyB3aWxsIGNvbmNhdGVuYXRlIGFycmF5IHNlYXJjaCB2YWx1ZXMgaW50byBhIHN0cmluZyBzZXBhcmF0ZWQgd2l0aCBcIixcIiBjb21tYXMuIFRoaXNcblx0XHQgKiBpcyBub3QgYWNjZXB0YWJsZSB0aG91Z2ggYmVjYXVzZSBzb21lIGZpbHRlcmluZyBlbGVtZW50cyBtYXkgY29udGFpbiB2YWx1ZXMgd2l0aCBjb21tYSBhbmQgdGh1cyB0aGUgYXJyYXlcblx0XHQgKiBjYW5ub3QgYmUgcGFyc2VkIGZyb20gYmFja2VuZC4gVGhpcyBtZXRob2Qgd2lsbCByZXNldCB0aG9zZSBjYXNlcyBiYWNrIHRvIGFycmF5cyBmb3IgYSBjbGVhcmVyIHRyYW5zYWN0aW9uXG5cdFx0ICogd2l0aCB0aGUgYmFja2VuZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0LlxuXHRcdCAqIEBwYXJhbSB7RGF0YVRhYmxlcy5TZXR0aW5nc30gc2V0dGluZ3MgRGF0YVRhYmxlcyBzZXR0aW5ncyBvYmplY3QuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGRhdGEgRGF0YSB0aGF0IHdpbGwgYmUgc2VudCB0byB0aGUgc2VydmVyIGluIGFuIG9iamVjdCBmb3JtLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9ub3JtYWxpemVBcnJheVZhbHVlcyhldmVudCwgc2V0dGluZ3MsIGRhdGEpIHtcblx0XHRcdGNvbnN0IGZpbHRlciA9IHt9O1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBuYW1lIGluIG1vZHVsZS5iaW5kaW5ncykge1xuXHRcdFx0XHRjb25zdCB2YWx1ZSA9IG1vZHVsZS5iaW5kaW5nc1tuYW1lXS5nZXQoKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICh2YWx1ZSAmJiB2YWx1ZS5jb25zdHJ1Y3RvciA9PT0gQXJyYXkpIHtcblx0XHRcdFx0XHRmaWx0ZXJbbmFtZV0gPSB2YWx1ZTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBlbnRyeSBpbiBmaWx0ZXIpIHtcblx0XHRcdFx0Zm9yIChsZXQgY29sdW1uIG9mIGRhdGEuY29sdW1ucykge1xuXHRcdFx0XHRcdGlmIChlbnRyeSA9PT0gY29sdW1uLm5hbWUgJiYgZmlsdGVyW2VudHJ5XS5jb25zdHJ1Y3RvciA9PT0gQXJyYXkpIHtcblx0XHRcdFx0XHRcdGNvbHVtbi5zZWFyY2gudmFsdWUgPSBmaWx0ZXJbZW50cnldO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBBZGQgcHVibGljIG1vZHVsZSBtZXRob2QuIFxuXHRcdFx0X2FkZFB1YmxpY01ldGhvZCgpO1xuXHRcdFx0XG5cdFx0XHQvLyBQYXJzZSBmaWx0ZXJpbmcgR0VUIHBhcmFtZXRlcnMuIFxuXHRcdFx0X3BhcnNlRmlsdGVyaW5nUGFyYW1ldGVycygpO1xuXHRcdFx0XG5cdFx0XHQvLyBCaW5kIGV2ZW50IGhhbmRsZXJzLlxuXHRcdFx0JGZpbHRlclxuXHRcdFx0XHQub24oJ2tleXVwJywgJ2lucHV0OnRleHQnLCBfb25JbnB1dFRleHRLZXlVcClcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuYXBwbHktcHJvcGVydGllcy1maWx0ZXJzJywgX29uQXBwbHlGaWx0ZXJzQ2xpY2spO1xuXHRcdFx0XG5cdFx0XHQkZmlsdGVyLnBhcmVudHMoJy5wcm9wZXJ0aWVzLm1vZGFsJylcblx0XHRcdFx0Lm9uKCdjbGljaycsICdidXR0b24ucmVzZXQtcHJvcGVydGllcy1maWx0ZXJzJywgX29uUmVzZXRGaWx0ZXJzQ2xpY2spO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5vbigncHJlWGhyLmR0JywgX25vcm1hbGl6ZUFycmF5VmFsdWVzKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7Il19
