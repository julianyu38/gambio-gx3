'use strict';

/* --------------------------------------------------------------
 emails_modal.js 2016-10-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 ----------------------------------------------------------------
 */

/**
 * ## Emails Modal Controller
 *
 * This controller will handle the modal dialog operations of the admin/emails page.
 *
 * @module Controllers/emails_modal
 */
gx.controllers.module('emails_modal', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', gx.source + '/libs/emails', 'modal', 'datatable', 'normalize'],

/** @lends module:Controllers/emails_modal */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Table Selector
  *
  * @type {object}
  */
	$table = $('#emails-table'),


	/**
  * Toolbar Selector
  *
  * @type {object}
  */
	$toolbar = $('#emails-toolbar'),


	/**
  * Contacts Table Selector
  *
  * @type {object}
  */
	$contactsTable = $this.find('#contacts-table'),


	/**
  * Attachments Table Selector
  *
  * @type {object}
  */
	$attachmentsTable = $this.find('#attachments-table'),


	/**
  * Default Module Options
  *
  * @type {object}
  */
	defaults = {
		contactsTableActions: function contactsTableActions(data, type, row, meta) {
			return '<div class="row-actions">' + '<span class="delete-contact action-item" title="' + jse.core.lang.translate('delete', 'buttons') + '">' + '<i class="fa fa-trash-o"></i>' + '</span>' + '</div>';
		},

		attachmentsTableActions: function attachmentsTableActions(data, type, row, meta) {
			// Check if attachment file exists in the server and thus can be downloaded.
			var disabled, title;

			if (data.file_exists) {
				disabled = '';
				title = jse.core.lang.translate('download', 'buttons');
			} else {
				disabled = 'disabled="disabled"';
				title = jse.core.lang.translate('message_download_attachment_error', 'emails');
			}

			// Generate table actions html for table row.
			return '<div class="row-actions">' + '<span class="delete-attachment action-item" title="' + jse.core.lang.translate('delete', 'buttons') + '">' + '<i class="fa fa-trash-o"></i>' + '</span>' + '<span class="download-attachment action-item" title="' + title + '" ' + disabled + '>' + '<i class="fa fa-download"></i>' + '</span>' + '</div>';
		},

		convertUpperCase: function convertUpperCase(data, type, row, meta) {
			return data.toUpperCase();
		},

		lengthMenu: [[5, 10], [5, 10]]
	},


	/**
  * Final Module Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Add a contact with the provided data into the contacts table.
  *
  * @param {object} event Contains event information.
  */
	var _onAddContact = function _onAddContact(event) {
		// Validate Contact Form
		$this.find('.tab-content.bcc-cc').trigger('validator.validate'); // Trigger form validation
		if ($this.find('.tab-content.bcc-cc .error').length > 0) {
			return;
		}

		// Add contact to table.
		var contact = {
			name: jse.libs.normalize.escapeHtml($this.find('#contact-name').val()),
			email: jse.libs.normalize.escapeHtml($this.find('#contact-email').val()),
			type: jse.libs.normalize.escapeHtml($this.find('#contact-type').val())
		};
		$this.find('#contacts-table').DataTable().row.add(contact).draw();
		$this.find('#contact-name, #contact-email, #contact-type').removeClass('valid error');
		$this.find('#contact-name, #contact-email').val('');
		$this.find('#contact-type option:first').prop('selected', true);
		jse.libs.emails.updateTabCounters($this);
	};

	/**
  * Remove contact from contacts table.
  *
  * @param {object} event contains event information.
  */
	var _onDeleteContact = function _onDeleteContact(event) {
		var row = $(this).parents('tr');
		$this.find('#contacts-table').DataTable().row(row).remove().draw();
		jse.libs.emails.updateTabCounters($this);
	};

	/**
  * Called after the attachment is uploaded
  *
  * @param {object} event Contains event information.
  */
	var _onUploadAttachment = function _onUploadAttachment(event, response) {
		if (response.exception) {
			jse.libs.modal.message({
				title: jse.core.lang.translate('error', 'messages'),
				content: jse.core.lang.translate('message_upload_attachment_error', 'emails') + response.message
			});
			return;
		}

		$this.find('#attachments-table').DataTable().row.add({
			path: jse.libs.normalize.escapeHtml(response.path),
			file_exists: true
		}).draw();

		$this.find('#upload-attachment').val('');
		jse.libs.emails.updateTabCounters($this);
	};

	/**
  * Called after the attachment is uploaded
  *
  * @param {object} event Contains event information.
  */
	var _onUploadAttachmentWithFileManager = function _onUploadAttachmentWithFileManager(event, response) {
		if (response.exception) {
			jse.libs.modal.message({
				title: jse.core.lang.translate('error', 'messages'),
				content: jse.core.lang.translate('message_upload_attachment_error', 'emails') + response.message
			});
			return;
		}

		$this.find('#attachments-table').DataTable().row.add({
			path: jse.libs.normalize.escapeHtml(response.path),
			file_exists: true
		}).draw();

		jse.libs.emails.updateTabCounters($this);
	};

	/**
  * Remove selected attachment from email and from server.
  *
  * @param {object} event Contains event information.
  */
	var _onDeleteAttachment = function _onDeleteAttachment(event) {
		var row = $(this).parents('tr').get(0),
		    url = jse.core.config.get('appUrl') + '/admin/admin.php?do=Emails/DeleteAttachment',
		    data = {
			pageToken: jse.core.config.get('pageToken'),
			attachments: [$attachmentsTable.DataTable().row(row).data().path]
		};

		$.post(url, data, function (response) {
			jse.core.debug.info('AJAX File Remove Response', response);

			if (response.exception) {
				jse.libs.modal.message({
					title: jse.core.lang.translate('error', 'messages'),
					content: jse.core.lang.translate('message_remove_attachment_error') + response.message
				});
				return;
			}

			$this.find('#attachments-table').DataTable().row(row).remove().draw();
			jse.libs.emails.updateTabCounters($this);
		}, 'json');
	};

	/**
  * Download selected attachment.
  *
  * A new window tab will be opened and the file download will start immediately. If
  * there are any errors from the PHP code they will be displayed in the new tab and
  * they will not affect the current page.
  *
  * @param {object} event Contains event information.
  */
	var _onDownloadAttachment = function _onDownloadAttachment(event) {
		if ($(this).attr('disabled') === 'disabled') {
			return;
		}
		var row = $(this).parents('tr').get(0),
		    path = $attachmentsTable.DataTable().row(row).data().path,
		    url = jse.core.config.get('appUrl') + '/admin/admin.php?do=Emails/DownloadAttachment&path=' + path;
		window.open(url, '_blank');
	};

	/**
  * Callback to the validation of the first tab of the modal.
  *
  * Make the tab headline link red so that the user can see that there is an error
  * inside the elements of this tab.
  *
  * @param {object} event Contains event information.
  */
	var _onEmailDetailsValidation = function _onEmailDetailsValidation(event) {
		// Paint the parent tab so that the user knows that there is a problem in the form.
		if ($this.find('.tab-content.details .error').length > 0) {
			$this.find('.tab-headline.details').css('color', 'red');
		} else {
			$this.find('.tab-headline.details').css('color', '');
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the module, called by the engine.
  */
	module.init = function (done) {
		// Contacts DataTable
		jse.libs.datatable.create($contactsTable, {
			autoWidth: false,
			order: [[0, 'asc'] // Email ASC
			],
			language: jse.core.config.get('languageCode') === 'de' ? jse.libs.datatable.getGermanTranslation() : null,
			lengthMenu: options.lengthMenu,
			pageLength: 5,
			columns: [{
				data: 'email',
				width: '45%'
			}, {
				data: 'name',
				width: '35%'
			}, {
				data: 'type',
				render: options.convertUpperCase,
				width: '10%'
			}, {
				data: null,
				orderable: false,
				defaultContent: '',
				render: options.contactsTableActions,
				width: '10%',
				className: 'dt-head-center dt-body-center'
			}]
		});

		// Attachments DataTable
		jse.libs.datatable.create($attachmentsTable, {
			autoWidth: false,
			order: [[0, 'asc'] // Path ASC
			],
			language: jse.core.config.get('languageCode') === 'de' ? jse.libs.datatable.getGermanTranslation() : null,
			lengthMenu: options.lengthMenu,
			pageLength: 5,
			columns: [{
				data: 'path',
				width: '80%'
			}, {
				data: null,
				orderable: false,
				defaultContent: '',
				render: options.attachmentsTableActions,
				width: '20%',
				className: 'dt-head-center dt-body-center'
			}]
		});

		jse.libs.emails.updateTabCounters($this);

		// Bind event handlers of the modal.
		$this.on('click', '#add-contact', _onAddContact).on('click', '.delete-contact', _onDeleteContact).on('click', '.delete-attachment', _onDeleteAttachment).on('click', '.download-attachment', _onDownloadAttachment).on('validator.validate', _onEmailDetailsValidation);

		// Bind the event handler for the email attachments to the responsive file manager
		// if it is available, else use the old implementation.
		if (gx.widgets.cache.modules.filemanager) {
			window.responsive_filemanager_callback = function (field_id) {
				var $field = $('#' + field_id);
				var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=Emails/UploadAttachmentWithFileManager';
				var data = { pageToken: jse.core.config.get('pageToken'), attachmentPath: $field.val() };

				// Make the input field not editable
				$field.prop('readonly', 'readonly');

				$.ajax({
					url: url,
					data: data,
					dataType: 'json',
					method: 'POST'
				}).done(function (response) {
					$this.find('#attachments-table').DataTable().row.add({
						path: response.path,
						file_exists: true
					}).draw();
				});

				jse.libs.emails.updateTabCounters($this);
			};
		} else {
			$this.on('upload', '#upload-attachment', _onUploadAttachment);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVtYWlscy9lbWFpbHNfbW9kYWwuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkdGFibGUiLCIkdG9vbGJhciIsIiRjb250YWN0c1RhYmxlIiwiZmluZCIsIiRhdHRhY2htZW50c1RhYmxlIiwiZGVmYXVsdHMiLCJjb250YWN0c1RhYmxlQWN0aW9ucyIsInR5cGUiLCJyb3ciLCJtZXRhIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJhdHRhY2htZW50c1RhYmxlQWN0aW9ucyIsImRpc2FibGVkIiwidGl0bGUiLCJmaWxlX2V4aXN0cyIsImNvbnZlcnRVcHBlckNhc2UiLCJ0b1VwcGVyQ2FzZSIsImxlbmd0aE1lbnUiLCJvcHRpb25zIiwiZXh0ZW5kIiwiX29uQWRkQ29udGFjdCIsImV2ZW50IiwidHJpZ2dlciIsImxlbmd0aCIsImNvbnRhY3QiLCJuYW1lIiwibGlicyIsIm5vcm1hbGl6ZSIsImVzY2FwZUh0bWwiLCJ2YWwiLCJlbWFpbCIsIkRhdGFUYWJsZSIsImFkZCIsImRyYXciLCJyZW1vdmVDbGFzcyIsInByb3AiLCJlbWFpbHMiLCJ1cGRhdGVUYWJDb3VudGVycyIsIl9vbkRlbGV0ZUNvbnRhY3QiLCJwYXJlbnRzIiwicmVtb3ZlIiwiX29uVXBsb2FkQXR0YWNobWVudCIsInJlc3BvbnNlIiwiZXhjZXB0aW9uIiwibW9kYWwiLCJtZXNzYWdlIiwiY29udGVudCIsInBhdGgiLCJfb25VcGxvYWRBdHRhY2htZW50V2l0aEZpbGVNYW5hZ2VyIiwiX29uRGVsZXRlQXR0YWNobWVudCIsImdldCIsInVybCIsImNvbmZpZyIsInBhZ2VUb2tlbiIsImF0dGFjaG1lbnRzIiwicG9zdCIsImRlYnVnIiwiaW5mbyIsIl9vbkRvd25sb2FkQXR0YWNobWVudCIsImF0dHIiLCJ3aW5kb3ciLCJvcGVuIiwiX29uRW1haWxEZXRhaWxzVmFsaWRhdGlvbiIsImNzcyIsImluaXQiLCJkb25lIiwiZGF0YXRhYmxlIiwiY3JlYXRlIiwiYXV0b1dpZHRoIiwib3JkZXIiLCJsYW5ndWFnZSIsImdldEdlcm1hblRyYW5zbGF0aW9uIiwicGFnZUxlbmd0aCIsImNvbHVtbnMiLCJ3aWR0aCIsInJlbmRlciIsIm9yZGVyYWJsZSIsImRlZmF1bHRDb250ZW50IiwiY2xhc3NOYW1lIiwib24iLCJ3aWRnZXRzIiwiY2FjaGUiLCJtb2R1bGVzIiwiZmlsZW1hbmFnZXIiLCJyZXNwb25zaXZlX2ZpbGVtYW5hZ2VyX2NhbGxiYWNrIiwiZmllbGRfaWQiLCIkZmllbGQiLCJhdHRhY2htZW50UGF0aCIsImFqYXgiLCJkYXRhVHlwZSIsIm1ldGhvZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLGNBREQsRUFHQyxDQUNDQyxJQUFJQyxNQUFKLEdBQWEsOENBRGQsRUFFQ0QsSUFBSUMsTUFBSixHQUFhLDZDQUZkLEVBR0NKLEdBQUdJLE1BQUgsR0FBWSxjQUhiLEVBSUMsT0FKRCxFQUtDLFdBTEQsRUFNQyxXQU5ELENBSEQ7O0FBWUM7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFVBQVNELEVBQUUsZUFBRixDQWJWOzs7QUFlQzs7Ozs7QUFLQUUsWUFBV0YsRUFBRSxpQkFBRixDQXBCWjs7O0FBc0JDOzs7OztBQUtBRyxrQkFBaUJKLE1BQU1LLElBQU4sQ0FBVyxpQkFBWCxDQTNCbEI7OztBQTZCQzs7Ozs7QUFLQUMscUJBQW9CTixNQUFNSyxJQUFOLENBQVcsb0JBQVgsQ0FsQ3JCOzs7QUFvQ0M7Ozs7O0FBS0FFLFlBQVc7QUFDVkMsd0JBQXNCLDhCQUFTVCxJQUFULEVBQWVVLElBQWYsRUFBcUJDLEdBQXJCLEVBQTBCQyxJQUExQixFQUFnQztBQUNyRCxVQUFPLDhCQUE4QixrREFBOUIsR0FDTmQsSUFBSWUsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FDQyxRQURELEVBQ1csU0FEWCxDQURNLEdBRWtCLElBRmxCLEdBRXlCLCtCQUZ6QixHQUUyRCxTQUYzRCxHQUV1RSxRQUY5RTtBQUdBLEdBTFM7O0FBT1ZDLDJCQUF5QixpQ0FBU2hCLElBQVQsRUFBZVUsSUFBZixFQUFxQkMsR0FBckIsRUFBMEJDLElBQTFCLEVBQWdDO0FBQ3hEO0FBQ0EsT0FBSUssUUFBSixFQUFjQyxLQUFkOztBQUVBLE9BQUlsQixLQUFLbUIsV0FBVCxFQUFzQjtBQUNyQkYsZUFBVyxFQUFYO0FBQ0FDLFlBQVFwQixJQUFJZSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixVQUF4QixFQUFvQyxTQUFwQyxDQUFSO0FBQ0EsSUFIRCxNQUdPO0FBQ05FLGVBQVcscUJBQVg7QUFDQUMsWUFBUXBCLElBQUllLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1DQUF4QixFQUE2RCxRQUE3RCxDQUFSO0FBQ0E7O0FBRUQ7QUFDQSxVQUFPLDhCQUE4QixxREFBOUIsR0FDTmpCLElBQUllLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQ0MsUUFERCxFQUNXLFNBRFgsQ0FETSxHQUVrQixJQUZsQixHQUV5QiwrQkFGekIsR0FFMkQsU0FGM0QsR0FHTix1REFITSxHQUdvREcsS0FIcEQsR0FHNEQsSUFINUQsR0FHbUVELFFBSG5FLEdBRzhFLEdBSDlFLEdBSU4sZ0NBSk0sR0FJNkIsU0FKN0IsR0FJeUMsUUFKaEQ7QUFLQSxHQXpCUzs7QUEyQlZHLG9CQUFrQiwwQkFBU3BCLElBQVQsRUFBZVUsSUFBZixFQUFxQkMsR0FBckIsRUFBMEJDLElBQTFCLEVBQWdDO0FBQ2pELFVBQU9aLEtBQUtxQixXQUFMLEVBQVA7QUFDQSxHQTdCUzs7QUErQlZDLGNBQVksQ0FBQyxDQUFDLENBQUQsRUFBSSxFQUFKLENBQUQsRUFBVSxDQUFDLENBQUQsRUFBSSxFQUFKLENBQVY7QUEvQkYsRUF6Q1o7OztBQTJFQzs7Ozs7QUFLQUMsV0FBVXJCLEVBQUVzQixNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJoQixRQUFuQixFQUE2QlIsSUFBN0IsQ0FoRlg7OztBQWtGQzs7Ozs7QUFLQUgsVUFBUyxFQXZGVjs7QUF5RkE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLEtBQUk0QixnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVNDLEtBQVQsRUFBZ0I7QUFDbkM7QUFDQXpCLFFBQU1LLElBQU4sQ0FBVyxxQkFBWCxFQUFrQ3FCLE9BQWxDLENBQTBDLG9CQUExQyxFQUZtQyxDQUU4QjtBQUNqRSxNQUFJMUIsTUFBTUssSUFBTixDQUFXLDRCQUFYLEVBQXlDc0IsTUFBekMsR0FBa0QsQ0FBdEQsRUFBeUQ7QUFDeEQ7QUFDQTs7QUFFRDtBQUNBLE1BQUlDLFVBQVU7QUFDYkMsU0FBTWhDLElBQUlpQyxJQUFKLENBQVNDLFNBQVQsQ0FBbUJDLFVBQW5CLENBQThCaEMsTUFBTUssSUFBTixDQUFXLGVBQVgsRUFBNEI0QixHQUE1QixFQUE5QixDQURPO0FBRWJDLFVBQU9yQyxJQUFJaUMsSUFBSixDQUFTQyxTQUFULENBQW1CQyxVQUFuQixDQUE4QmhDLE1BQU1LLElBQU4sQ0FBVyxnQkFBWCxFQUE2QjRCLEdBQTdCLEVBQTlCLENBRk07QUFHYnhCLFNBQU1aLElBQUlpQyxJQUFKLENBQVNDLFNBQVQsQ0FBbUJDLFVBQW5CLENBQThCaEMsTUFBTUssSUFBTixDQUFXLGVBQVgsRUFBNEI0QixHQUE1QixFQUE5QjtBQUhPLEdBQWQ7QUFLQWpDLFFBQU1LLElBQU4sQ0FBVyxpQkFBWCxFQUE4QjhCLFNBQTlCLEdBQTBDekIsR0FBMUMsQ0FBOEMwQixHQUE5QyxDQUFrRFIsT0FBbEQsRUFBMkRTLElBQTNEO0FBQ0FyQyxRQUFNSyxJQUFOLENBQVcsOENBQVgsRUFBMkRpQyxXQUEzRCxDQUF1RSxhQUF2RTtBQUNBdEMsUUFBTUssSUFBTixDQUFXLCtCQUFYLEVBQTRDNEIsR0FBNUMsQ0FBZ0QsRUFBaEQ7QUFDQWpDLFFBQU1LLElBQU4sQ0FBVyw0QkFBWCxFQUF5Q2tDLElBQXpDLENBQThDLFVBQTlDLEVBQTBELElBQTFEO0FBQ0ExQyxNQUFJaUMsSUFBSixDQUFTVSxNQUFULENBQWdCQyxpQkFBaEIsQ0FBa0N6QyxLQUFsQztBQUNBLEVBbEJEOztBQW9CQTs7Ozs7QUFLQSxLQUFJMEMsbUJBQW1CLFNBQW5CQSxnQkFBbUIsQ0FBU2pCLEtBQVQsRUFBZ0I7QUFDdEMsTUFBSWYsTUFBTVQsRUFBRSxJQUFGLEVBQVEwQyxPQUFSLENBQWdCLElBQWhCLENBQVY7QUFDQTNDLFFBQU1LLElBQU4sQ0FBVyxpQkFBWCxFQUE4QjhCLFNBQTlCLEdBQTBDekIsR0FBMUMsQ0FBOENBLEdBQTlDLEVBQW1Ea0MsTUFBbkQsR0FBNERQLElBQTVEO0FBQ0F4QyxNQUFJaUMsSUFBSixDQUFTVSxNQUFULENBQWdCQyxpQkFBaEIsQ0FBa0N6QyxLQUFsQztBQUNBLEVBSkQ7O0FBTUE7Ozs7O0FBS0EsS0FBSTZDLHNCQUFzQixTQUF0QkEsbUJBQXNCLENBQVNwQixLQUFULEVBQWdCcUIsUUFBaEIsRUFBMEI7QUFDbkQsTUFBSUEsU0FBU0MsU0FBYixFQUF3QjtBQUN2QmxELE9BQUlpQyxJQUFKLENBQVNrQixLQUFULENBQWVDLE9BQWYsQ0FBdUI7QUFDdEJoQyxXQUFPcEIsSUFBSWUsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FEZTtBQUV0Qm9DLGFBQVNyRCxJQUFJZSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixpQ0FBeEIsRUFBMkQsUUFBM0QsSUFDUGdDLFNBQVNHO0FBSFcsSUFBdkI7QUFLQTtBQUNBOztBQUVEakQsUUFBTUssSUFBTixDQUFXLG9CQUFYLEVBQWlDOEIsU0FBakMsR0FBNkN6QixHQUE3QyxDQUFpRDBCLEdBQWpELENBQXFEO0FBQ3BEZSxTQUFNdEQsSUFBSWlDLElBQUosQ0FBU0MsU0FBVCxDQUFtQkMsVUFBbkIsQ0FBOEJjLFNBQVNLLElBQXZDLENBRDhDO0FBRXBEakMsZ0JBQWE7QUFGdUMsR0FBckQsRUFHR21CLElBSEg7O0FBS0FyQyxRQUFNSyxJQUFOLENBQVcsb0JBQVgsRUFBaUM0QixHQUFqQyxDQUFxQyxFQUFyQztBQUNBcEMsTUFBSWlDLElBQUosQ0FBU1UsTUFBVCxDQUFnQkMsaUJBQWhCLENBQWtDekMsS0FBbEM7QUFDQSxFQWpCRDs7QUFtQkE7Ozs7O0FBS0EsS0FBSW9ELHFDQUFxQyxTQUFyQ0Esa0NBQXFDLENBQVMzQixLQUFULEVBQWdCcUIsUUFBaEIsRUFBMEI7QUFDbEUsTUFBSUEsU0FBU0MsU0FBYixFQUF3QjtBQUN2QmxELE9BQUlpQyxJQUFKLENBQVNrQixLQUFULENBQWVDLE9BQWYsQ0FBdUI7QUFDQ2hDLFdBQU9wQixJQUFJZSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQURSO0FBRUNvQyxhQUFTckQsSUFBSWUsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsaUNBQXhCLEVBQTJELFFBQTNELElBQ0VnQyxTQUFTRztBQUhyQixJQUF2QjtBQUtBO0FBQ0E7O0FBRURqRCxRQUFNSyxJQUFOLENBQVcsb0JBQVgsRUFBaUM4QixTQUFqQyxHQUE2Q3pCLEdBQTdDLENBQWlEMEIsR0FBakQsQ0FBcUQ7QUFDQ2UsU0FBTXRELElBQUlpQyxJQUFKLENBQVNDLFNBQVQsQ0FBbUJDLFVBQW5CLENBQThCYyxTQUFTSyxJQUF2QyxDQURQO0FBRUNqQyxnQkFBYTtBQUZkLEdBQXJELEVBR3dEbUIsSUFIeEQ7O0FBS0F4QyxNQUFJaUMsSUFBSixDQUFTVSxNQUFULENBQWdCQyxpQkFBaEIsQ0FBa0N6QyxLQUFsQztBQUNBLEVBaEJEOztBQWtCQTs7Ozs7QUFLQSxLQUFJcUQsc0JBQXNCLFNBQXRCQSxtQkFBc0IsQ0FBUzVCLEtBQVQsRUFBZ0I7QUFDekMsTUFBSWYsTUFBTVQsRUFBRSxJQUFGLEVBQVEwQyxPQUFSLENBQWdCLElBQWhCLEVBQXNCVyxHQUF0QixDQUEwQixDQUExQixDQUFWO0FBQUEsTUFDQ0MsTUFBTTFELElBQUllLElBQUosQ0FBUzRDLE1BQVQsQ0FBZ0JGLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLDZDQUR2QztBQUFBLE1BRUN2RCxPQUFPO0FBQ04wRCxjQUFXNUQsSUFBSWUsSUFBSixDQUFTNEMsTUFBVCxDQUFnQkYsR0FBaEIsQ0FBb0IsV0FBcEIsQ0FETDtBQUVOSSxnQkFBYSxDQUFDcEQsa0JBQWtCNkIsU0FBbEIsR0FBOEJ6QixHQUE5QixDQUFrQ0EsR0FBbEMsRUFBdUNYLElBQXZDLEdBQThDb0QsSUFBL0M7QUFGUCxHQUZSOztBQU9BbEQsSUFBRTBELElBQUYsQ0FBT0osR0FBUCxFQUFZeEQsSUFBWixFQUFrQixVQUFTK0MsUUFBVCxFQUFtQjtBQUNwQ2pELE9BQUllLElBQUosQ0FBU2dELEtBQVQsQ0FBZUMsSUFBZixDQUFvQiwyQkFBcEIsRUFBaURmLFFBQWpEOztBQUVBLE9BQUlBLFNBQVNDLFNBQWIsRUFBd0I7QUFDdkJsRCxRQUFJaUMsSUFBSixDQUFTa0IsS0FBVCxDQUFlQyxPQUFmLENBQXVCO0FBQ3RCaEMsWUFBT3BCLElBQUllLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBRGU7QUFFdEJvQyxjQUFTckQsSUFBSWUsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsaUNBQXhCLElBQ1BnQyxTQUFTRztBQUhXLEtBQXZCO0FBS0E7QUFDQTs7QUFFRGpELFNBQU1LLElBQU4sQ0FBVyxvQkFBWCxFQUFpQzhCLFNBQWpDLEdBQTZDekIsR0FBN0MsQ0FBaURBLEdBQWpELEVBQXNEa0MsTUFBdEQsR0FBK0RQLElBQS9EO0FBQ0F4QyxPQUFJaUMsSUFBSixDQUFTVSxNQUFULENBQWdCQyxpQkFBaEIsQ0FBa0N6QyxLQUFsQztBQUNBLEdBZEQsRUFjRyxNQWRIO0FBZUEsRUF2QkQ7O0FBeUJBOzs7Ozs7Ozs7QUFTQSxLQUFJOEQsd0JBQXdCLFNBQXhCQSxxQkFBd0IsQ0FBU3JDLEtBQVQsRUFBZ0I7QUFDM0MsTUFBSXhCLEVBQUUsSUFBRixFQUFROEQsSUFBUixDQUFhLFVBQWIsTUFBNkIsVUFBakMsRUFBNkM7QUFDNUM7QUFDQTtBQUNELE1BQUlyRCxNQUFNVCxFQUFFLElBQUYsRUFBUTBDLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JXLEdBQXRCLENBQTBCLENBQTFCLENBQVY7QUFBQSxNQUNDSCxPQUFPN0Msa0JBQWtCNkIsU0FBbEIsR0FBOEJ6QixHQUE5QixDQUFrQ0EsR0FBbEMsRUFBdUNYLElBQXZDLEdBQThDb0QsSUFEdEQ7QUFBQSxNQUVDSSxNQUFNMUQsSUFBSWUsSUFBSixDQUFTNEMsTUFBVCxDQUFnQkYsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MscURBQWhDLEdBQXdGSCxJQUYvRjtBQUdBYSxTQUFPQyxJQUFQLENBQVlWLEdBQVosRUFBaUIsUUFBakI7QUFDQSxFQVJEOztBQVVBOzs7Ozs7OztBQVFBLEtBQUlXLDRCQUE0QixTQUE1QkEseUJBQTRCLENBQVN6QyxLQUFULEVBQWdCO0FBQy9DO0FBQ0EsTUFBSXpCLE1BQU1LLElBQU4sQ0FBVyw2QkFBWCxFQUEwQ3NCLE1BQTFDLEdBQW1ELENBQXZELEVBQTBEO0FBQ3pEM0IsU0FBTUssSUFBTixDQUFXLHVCQUFYLEVBQW9DOEQsR0FBcEMsQ0FBd0MsT0FBeEMsRUFBaUQsS0FBakQ7QUFDQSxHQUZELE1BRU87QUFDTm5FLFNBQU1LLElBQU4sQ0FBVyx1QkFBWCxFQUFvQzhELEdBQXBDLENBQXdDLE9BQXhDLEVBQWlELEVBQWpEO0FBQ0E7QUFDRCxFQVBEOztBQVNBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0F2RSxRQUFPd0UsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjtBQUNBeEUsTUFBSWlDLElBQUosQ0FBU3dDLFNBQVQsQ0FBbUJDLE1BQW5CLENBQTBCbkUsY0FBMUIsRUFBMEM7QUFDekNvRSxjQUFXLEtBRDhCO0FBRXpDQyxVQUFPLENBQ04sQ0FBQyxDQUFELEVBQUksS0FBSixDQURNLENBQ0s7QUFETCxJQUZrQztBQUt6Q0MsYUFBWTdFLElBQUllLElBQUosQ0FBUzRDLE1BQVQsQ0FBZ0JGLEdBQWhCLENBQW9CLGNBQXBCLE1BQXdDLElBQTFDLEdBQ1B6RCxJQUFJaUMsSUFBSixDQUFTd0MsU0FBVCxDQUFtQkssb0JBQW5CLEVBRE8sR0FFUCxJQVBzQztBQVF6Q3RELGVBQVlDLFFBQVFELFVBUnFCO0FBU3pDdUQsZUFBWSxDQVQ2QjtBQVV6Q0MsWUFBUyxDQUNSO0FBQ0M5RSxVQUFNLE9BRFA7QUFFQytFLFdBQU87QUFGUixJQURRLEVBS1I7QUFDQy9FLFVBQU0sTUFEUDtBQUVDK0UsV0FBTztBQUZSLElBTFEsRUFTUjtBQUNDL0UsVUFBTSxNQURQO0FBRUNnRixZQUFRekQsUUFBUUgsZ0JBRmpCO0FBR0MyRCxXQUFPO0FBSFIsSUFUUSxFQWNSO0FBQ0MvRSxVQUFNLElBRFA7QUFFQ2lGLGVBQVcsS0FGWjtBQUdDQyxvQkFBZ0IsRUFIakI7QUFJQ0YsWUFBUXpELFFBQVFkLG9CQUpqQjtBQUtDc0UsV0FBTyxLQUxSO0FBTUNJLGVBQVc7QUFOWixJQWRRO0FBVmdDLEdBQTFDOztBQW1DQTtBQUNBckYsTUFBSWlDLElBQUosQ0FBU3dDLFNBQVQsQ0FBbUJDLE1BQW5CLENBQTBCakUsaUJBQTFCLEVBQTZDO0FBQzVDa0UsY0FBVyxLQURpQztBQUU1Q0MsVUFBTyxDQUNOLENBQUMsQ0FBRCxFQUFJLEtBQUosQ0FETSxDQUNLO0FBREwsSUFGcUM7QUFLNUNDLGFBQVk3RSxJQUFJZSxJQUFKLENBQVM0QyxNQUFULENBQWdCRixHQUFoQixDQUFvQixjQUFwQixNQUF3QyxJQUExQyxHQUNQekQsSUFBSWlDLElBQUosQ0FBU3dDLFNBQVQsQ0FBbUJLLG9CQUFuQixFQURPLEdBRVAsSUFQeUM7QUFRNUN0RCxlQUFZQyxRQUFRRCxVQVJ3QjtBQVM1Q3VELGVBQVksQ0FUZ0M7QUFVNUNDLFlBQVMsQ0FDUjtBQUNDOUUsVUFBTSxNQURQO0FBRUMrRSxXQUFPO0FBRlIsSUFEUSxFQUtSO0FBQ0MvRSxVQUFNLElBRFA7QUFFQ2lGLGVBQVcsS0FGWjtBQUdDQyxvQkFBZ0IsRUFIakI7QUFJQ0YsWUFBUXpELFFBQVFQLHVCQUpqQjtBQUtDK0QsV0FBTyxLQUxSO0FBTUNJLGVBQVc7QUFOWixJQUxRO0FBVm1DLEdBQTdDOztBQTBCQXJGLE1BQUlpQyxJQUFKLENBQVNVLE1BQVQsQ0FBZ0JDLGlCQUFoQixDQUFrQ3pDLEtBQWxDOztBQUVBO0FBQ0FBLFFBQ0VtRixFQURGLENBQ0ssT0FETCxFQUNjLGNBRGQsRUFDOEIzRCxhQUQ5QixFQUVFMkQsRUFGRixDQUVLLE9BRkwsRUFFYyxpQkFGZCxFQUVpQ3pDLGdCQUZqQyxFQUdFeUMsRUFIRixDQUdLLE9BSEwsRUFHYyxvQkFIZCxFQUdvQzlCLG1CQUhwQyxFQUlFOEIsRUFKRixDQUlLLE9BSkwsRUFJYyxzQkFKZCxFQUlzQ3JCLHFCQUp0QyxFQUtFcUIsRUFMRixDQUtLLG9CQUxMLEVBSzJCakIseUJBTDNCOztBQU9BO0FBQ0E7QUFDQSxNQUFJeEUsR0FBRzBGLE9BQUgsQ0FBV0MsS0FBWCxDQUFpQkMsT0FBakIsQ0FBeUJDLFdBQTdCLEVBQTBDO0FBQ3pDdkIsVUFBT3dCLCtCQUFQLEdBQXlDLFVBQVVDLFFBQVYsRUFBb0I7QUFDNUQsUUFBSUMsU0FBU3pGLEVBQUUsTUFBTXdGLFFBQVIsQ0FBYjtBQUNBLFFBQUlsQyxNQUFNMUQsSUFBSWUsSUFBSixDQUFTNEMsTUFBVCxDQUFnQkYsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsNERBQTFDO0FBQ0EsUUFBSXZELE9BQU8sRUFBQzBELFdBQVc1RCxJQUFJZSxJQUFKLENBQVM0QyxNQUFULENBQWdCRixHQUFoQixDQUFvQixXQUFwQixDQUFaLEVBQThDcUMsZ0JBQWdCRCxPQUFPekQsR0FBUCxFQUE5RCxFQUFYOztBQUVBO0FBQ0F5RCxXQUFPbkQsSUFBUCxDQUFZLFVBQVosRUFBd0IsVUFBeEI7O0FBRUF0QyxNQUFFMkYsSUFBRixDQUNDO0FBQ0NyQyxhQUREO0FBRUN4RCxlQUZEO0FBR0M4RixlQUFVLE1BSFg7QUFJQ0MsYUFBUTtBQUpULEtBREQsRUFPRXpCLElBUEYsQ0FPTyxVQUFVdkIsUUFBVixFQUFvQjtBQUMxQjlDLFdBQU1LLElBQU4sQ0FBVyxvQkFBWCxFQUFpQzhCLFNBQWpDLEdBQTZDekIsR0FBN0MsQ0FBaUQwQixHQUFqRCxDQUNDO0FBQ0NlLFlBQU1MLFNBQVNLLElBRGhCO0FBRUNqQyxtQkFBYTtBQUZkLE1BREQsRUFLRW1CLElBTEY7QUFNQSxLQWREOztBQWdCQXhDLFFBQUlpQyxJQUFKLENBQVNVLE1BQVQsQ0FBZ0JDLGlCQUFoQixDQUFrQ3pDLEtBQWxDO0FBQ0EsSUF6QkQ7QUEyQkEsR0E1QkQsTUE0Qk87QUFDTkEsU0FBTW1GLEVBQU4sQ0FBUyxRQUFULEVBQW1CLG9CQUFuQixFQUF5Q3RDLG1CQUF6QztBQUNBOztBQUVEd0I7QUFDQSxFQTdHRDs7QUErR0EsUUFBT3pFLE1BQVA7QUFDQSxDQS9YRiIsImZpbGUiOiJlbWFpbHMvZW1haWxzX21vZGFsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBlbWFpbHNfbW9kYWwuanMgMjAxNi0xMC0xMVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBFbWFpbHMgTW9kYWwgQ29udHJvbGxlclxuICpcbiAqIFRoaXMgY29udHJvbGxlciB3aWxsIGhhbmRsZSB0aGUgbW9kYWwgZGlhbG9nIG9wZXJhdGlvbnMgb2YgdGhlIGFkbWluL2VtYWlscyBwYWdlLlxuICpcbiAqIEBtb2R1bGUgQ29udHJvbGxlcnMvZW1haWxzX21vZGFsXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J2VtYWlsc19tb2RhbCcsXG5cdFxuXHRbXG5cdFx0anNlLnNvdXJjZSArICcvdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmNzcycsXG5cdFx0anNlLnNvdXJjZSArICcvdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmpzJyxcblx0XHRneC5zb3VyY2UgKyAnL2xpYnMvZW1haWxzJyxcblx0XHQnbW9kYWwnLFxuXHRcdCdkYXRhdGFibGUnLFxuXHRcdCdub3JtYWxpemUnXG5cdF0sXG5cdFxuXHQvKiogQGxlbmRzIG1vZHVsZTpDb250cm9sbGVycy9lbWFpbHNfbW9kYWwgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFRhYmxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRhYmxlID0gJCgnI2VtYWlscy10YWJsZScpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFRvb2xiYXIgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdG9vbGJhciA9ICQoJyNlbWFpbHMtdG9vbGJhcicpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIENvbnRhY3RzIFRhYmxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JGNvbnRhY3RzVGFibGUgPSAkdGhpcy5maW5kKCcjY29udGFjdHMtdGFibGUnKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBBdHRhY2htZW50cyBUYWJsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCRhdHRhY2htZW50c1RhYmxlID0gJHRoaXMuZmluZCgnI2F0dGFjaG1lbnRzLXRhYmxlJyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBNb2R1bGUgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0ge1xuXHRcdFx0XHRjb250YWN0c1RhYmxlQWN0aW9uczogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93LCBtZXRhKSB7XG5cdFx0XHRcdFx0cmV0dXJuICc8ZGl2IGNsYXNzPVwicm93LWFjdGlvbnNcIj4nICsgJzxzcGFuIGNsYXNzPVwiZGVsZXRlLWNvbnRhY3QgYWN0aW9uLWl0ZW1cIiB0aXRsZT1cIicgK1xuXHRcdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoXG5cdFx0XHRcdFx0XHRcdCdkZWxldGUnLCAnYnV0dG9ucycpICsgJ1wiPicgKyAnPGkgY2xhc3M9XCJmYSBmYS10cmFzaC1vXCI+PC9pPicgKyAnPC9zcGFuPicgKyAnPC9kaXY+Jztcblx0XHRcdFx0fSxcblx0XHRcdFx0XG5cdFx0XHRcdGF0dGFjaG1lbnRzVGFibGVBY3Rpb25zOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHQvLyBDaGVjayBpZiBhdHRhY2htZW50IGZpbGUgZXhpc3RzIGluIHRoZSBzZXJ2ZXIgYW5kIHRodXMgY2FuIGJlIGRvd25sb2FkZWQuXG5cdFx0XHRcdFx0dmFyIGRpc2FibGVkLCB0aXRsZTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAoZGF0YS5maWxlX2V4aXN0cykge1xuXHRcdFx0XHRcdFx0ZGlzYWJsZWQgPSAnJztcblx0XHRcdFx0XHRcdHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Rvd25sb2FkJywgJ2J1dHRvbnMnKTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0ZGlzYWJsZWQgPSAnZGlzYWJsZWQ9XCJkaXNhYmxlZFwiJztcblx0XHRcdFx0XHRcdHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ21lc3NhZ2VfZG93bmxvYWRfYXR0YWNobWVudF9lcnJvcicsICdlbWFpbHMnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gR2VuZXJhdGUgdGFibGUgYWN0aW9ucyBodG1sIGZvciB0YWJsZSByb3cuXG5cdFx0XHRcdFx0cmV0dXJuICc8ZGl2IGNsYXNzPVwicm93LWFjdGlvbnNcIj4nICsgJzxzcGFuIGNsYXNzPVwiZGVsZXRlLWF0dGFjaG1lbnQgYWN0aW9uLWl0ZW1cIiB0aXRsZT1cIicgK1xuXHRcdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoXG5cdFx0XHRcdFx0XHRcdCdkZWxldGUnLCAnYnV0dG9ucycpICsgJ1wiPicgKyAnPGkgY2xhc3M9XCJmYSBmYS10cmFzaC1vXCI+PC9pPicgKyAnPC9zcGFuPicgK1xuXHRcdFx0XHRcdFx0JzxzcGFuIGNsYXNzPVwiZG93bmxvYWQtYXR0YWNobWVudCBhY3Rpb24taXRlbVwiIHRpdGxlPVwiJyArIHRpdGxlICsgJ1wiICcgKyBkaXNhYmxlZCArICc+JyArXG5cdFx0XHRcdFx0XHQnPGkgY2xhc3M9XCJmYSBmYS1kb3dubG9hZFwiPjwvaT4nICsgJzwvc3Bhbj4nICsgJzwvZGl2Pic7XG5cdFx0XHRcdH0sXG5cdFx0XHRcdFxuXHRcdFx0XHRjb252ZXJ0VXBwZXJDYXNlOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHRyZXR1cm4gZGF0YS50b1VwcGVyQ2FzZSgpO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRcblx0XHRcdFx0bGVuZ3RoTWVudTogW1s1LCAxMF0sIFs1LCAxMF1dXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE1vZHVsZSBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQWRkIGEgY29udGFjdCB3aXRoIHRoZSBwcm92aWRlZCBkYXRhIGludG8gdGhlIGNvbnRhY3RzIHRhYmxlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqL1xuXHRcdHZhciBfb25BZGRDb250YWN0ID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIFZhbGlkYXRlIENvbnRhY3QgRm9ybVxuXHRcdFx0JHRoaXMuZmluZCgnLnRhYi1jb250ZW50LmJjYy1jYycpLnRyaWdnZXIoJ3ZhbGlkYXRvci52YWxpZGF0ZScpOyAvLyBUcmlnZ2VyIGZvcm0gdmFsaWRhdGlvblxuXHRcdFx0aWYgKCR0aGlzLmZpbmQoJy50YWItY29udGVudC5iY2MtY2MgLmVycm9yJykubGVuZ3RoID4gMCkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIEFkZCBjb250YWN0IHRvIHRhYmxlLlxuXHRcdFx0dmFyIGNvbnRhY3QgPSB7XG5cdFx0XHRcdG5hbWU6IGpzZS5saWJzLm5vcm1hbGl6ZS5lc2NhcGVIdG1sKCR0aGlzLmZpbmQoJyNjb250YWN0LW5hbWUnKS52YWwoKSksXG5cdFx0XHRcdGVtYWlsOiBqc2UubGlicy5ub3JtYWxpemUuZXNjYXBlSHRtbCgkdGhpcy5maW5kKCcjY29udGFjdC1lbWFpbCcpLnZhbCgpKSxcblx0XHRcdFx0dHlwZToganNlLmxpYnMubm9ybWFsaXplLmVzY2FwZUh0bWwoJHRoaXMuZmluZCgnI2NvbnRhY3QtdHlwZScpLnZhbCgpKVxuXHRcdFx0fTtcblx0XHRcdCR0aGlzLmZpbmQoJyNjb250YWN0cy10YWJsZScpLkRhdGFUYWJsZSgpLnJvdy5hZGQoY29udGFjdCkuZHJhdygpO1xuXHRcdFx0JHRoaXMuZmluZCgnI2NvbnRhY3QtbmFtZSwgI2NvbnRhY3QtZW1haWwsICNjb250YWN0LXR5cGUnKS5yZW1vdmVDbGFzcygndmFsaWQgZXJyb3InKTtcblx0XHRcdCR0aGlzLmZpbmQoJyNjb250YWN0LW5hbWUsICNjb250YWN0LWVtYWlsJykudmFsKCcnKTtcblx0XHRcdCR0aGlzLmZpbmQoJyNjb250YWN0LXR5cGUgb3B0aW9uOmZpcnN0JykucHJvcCgnc2VsZWN0ZWQnLCB0cnVlKTtcblx0XHRcdGpzZS5saWJzLmVtYWlscy51cGRhdGVUYWJDb3VudGVycygkdGhpcyk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBSZW1vdmUgY29udGFjdCBmcm9tIGNvbnRhY3RzIHRhYmxlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGNvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqL1xuXHRcdHZhciBfb25EZWxldGVDb250YWN0ID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciByb3cgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJyk7XG5cdFx0XHQkdGhpcy5maW5kKCcjY29udGFjdHMtdGFibGUnKS5EYXRhVGFibGUoKS5yb3cocm93KS5yZW1vdmUoKS5kcmF3KCk7XG5cdFx0XHRqc2UubGlicy5lbWFpbHMudXBkYXRlVGFiQ291bnRlcnMoJHRoaXMpO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2FsbGVkIGFmdGVyIHRoZSBhdHRhY2htZW50IGlzIHVwbG9hZGVkXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgQ29udGFpbnMgZXZlbnQgaW5mb3JtYXRpb24uXG5cdFx0ICovXG5cdFx0dmFyIF9vblVwbG9hZEF0dGFjaG1lbnQgPSBmdW5jdGlvbihldmVudCwgcmVzcG9uc2UpIHtcblx0XHRcdGlmIChyZXNwb25zZS5leGNlcHRpb24pIHtcblx0XHRcdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZSh7XG5cdFx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpLFxuXHRcdFx0XHRcdGNvbnRlbnQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdtZXNzYWdlX3VwbG9hZF9hdHRhY2htZW50X2Vycm9yJywgJ2VtYWlscycpXG5cdFx0XHRcdFx0KyByZXNwb25zZS5tZXNzYWdlXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQoJyNhdHRhY2htZW50cy10YWJsZScpLkRhdGFUYWJsZSgpLnJvdy5hZGQoe1xuXHRcdFx0XHRwYXRoOiBqc2UubGlicy5ub3JtYWxpemUuZXNjYXBlSHRtbChyZXNwb25zZS5wYXRoKSxcblx0XHRcdFx0ZmlsZV9leGlzdHM6IHRydWVcblx0XHRcdH0pLmRyYXcoKTtcblx0XHRcdFxuXHRcdFx0JHRoaXMuZmluZCgnI3VwbG9hZC1hdHRhY2htZW50JykudmFsKCcnKTtcblx0XHRcdGpzZS5saWJzLmVtYWlscy51cGRhdGVUYWJDb3VudGVycygkdGhpcyk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDYWxsZWQgYWZ0ZXIgdGhlIGF0dGFjaG1lbnQgaXMgdXBsb2FkZWRcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBDb250YWlucyBldmVudCBpbmZvcm1hdGlvbi5cblx0XHQgKi9cblx0XHR2YXIgX29uVXBsb2FkQXR0YWNobWVudFdpdGhGaWxlTWFuYWdlciA9IGZ1bmN0aW9uKGV2ZW50LCByZXNwb25zZSkge1xuXHRcdFx0aWYgKHJlc3BvbnNlLmV4Y2VwdGlvbikge1xuXHRcdFx0XHRqc2UubGlicy5tb2RhbC5tZXNzYWdlKHtcblx0XHRcdFx0XHQgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKSxcblx0XHRcdFx0XHQgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdtZXNzYWdlX3VwbG9hZF9hdHRhY2htZW50X2Vycm9yJywgJ2VtYWlscycpXG5cdFx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArIHJlc3BvbnNlLm1lc3NhZ2Vcblx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICB9KTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkdGhpcy5maW5kKCcjYXR0YWNobWVudHMtdGFibGUnKS5EYXRhVGFibGUoKS5yb3cuYWRkKHtcblx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoOiBqc2UubGlicy5ub3JtYWxpemUuZXNjYXBlSHRtbChyZXNwb25zZS5wYXRoKSxcblx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlX2V4aXN0czogdHJ1ZVxuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5kcmF3KCk7XG5cdFx0XHRcblx0XHRcdGpzZS5saWJzLmVtYWlscy51cGRhdGVUYWJDb3VudGVycygkdGhpcyk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBSZW1vdmUgc2VsZWN0ZWQgYXR0YWNobWVudCBmcm9tIGVtYWlsIGFuZCBmcm9tIHNlcnZlci5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBDb250YWlucyBldmVudCBpbmZvcm1hdGlvbi5cblx0XHQgKi9cblx0XHR2YXIgX29uRGVsZXRlQXR0YWNobWVudCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgcm93ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmdldCgwKSxcblx0XHRcdFx0dXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1FbWFpbHMvRGVsZXRlQXR0YWNobWVudCcsXG5cdFx0XHRcdGRhdGEgPSB7XG5cdFx0XHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKSxcblx0XHRcdFx0XHRhdHRhY2htZW50czogWyRhdHRhY2htZW50c1RhYmxlLkRhdGFUYWJsZSgpLnJvdyhyb3cpLmRhdGEoKS5wYXRoXVxuXHRcdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQkLnBvc3QodXJsLCBkYXRhLCBmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5pbmZvKCdBSkFYIEZpbGUgUmVtb3ZlIFJlc3BvbnNlJywgcmVzcG9uc2UpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKHJlc3BvbnNlLmV4Y2VwdGlvbikge1xuXHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLm1lc3NhZ2Uoe1xuXHRcdFx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpLFxuXHRcdFx0XHRcdFx0Y29udGVudDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ21lc3NhZ2VfcmVtb3ZlX2F0dGFjaG1lbnRfZXJyb3InKVxuXHRcdFx0XHRcdFx0KyByZXNwb25zZS5tZXNzYWdlXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQkdGhpcy5maW5kKCcjYXR0YWNobWVudHMtdGFibGUnKS5EYXRhVGFibGUoKS5yb3cocm93KS5yZW1vdmUoKS5kcmF3KCk7XG5cdFx0XHRcdGpzZS5saWJzLmVtYWlscy51cGRhdGVUYWJDb3VudGVycygkdGhpcyk7XG5cdFx0XHR9LCAnanNvbicpO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRG93bmxvYWQgc2VsZWN0ZWQgYXR0YWNobWVudC5cblx0XHQgKlxuXHRcdCAqIEEgbmV3IHdpbmRvdyB0YWIgd2lsbCBiZSBvcGVuZWQgYW5kIHRoZSBmaWxlIGRvd25sb2FkIHdpbGwgc3RhcnQgaW1tZWRpYXRlbHkuIElmXG5cdFx0ICogdGhlcmUgYXJlIGFueSBlcnJvcnMgZnJvbSB0aGUgUEhQIGNvZGUgdGhleSB3aWxsIGJlIGRpc3BsYXllZCBpbiB0aGUgbmV3IHRhYiBhbmRcblx0XHQgKiB0aGV5IHdpbGwgbm90IGFmZmVjdCB0aGUgY3VycmVudCBwYWdlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqL1xuXHRcdHZhciBfb25Eb3dubG9hZEF0dGFjaG1lbnQgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0aWYgKCQodGhpcykuYXR0cignZGlzYWJsZWQnKSA9PT0gJ2Rpc2FibGVkJykge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHR2YXIgcm93ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmdldCgwKSxcblx0XHRcdFx0cGF0aCA9ICRhdHRhY2htZW50c1RhYmxlLkRhdGFUYWJsZSgpLnJvdyhyb3cpLmRhdGEoKS5wYXRoLFxuXHRcdFx0XHR1cmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPUVtYWlscy9Eb3dubG9hZEF0dGFjaG1lbnQmcGF0aD0nICsgcGF0aDtcblx0XHRcdHdpbmRvdy5vcGVuKHVybCwgJ19ibGFuaycpO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2FsbGJhY2sgdG8gdGhlIHZhbGlkYXRpb24gb2YgdGhlIGZpcnN0IHRhYiBvZiB0aGUgbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBNYWtlIHRoZSB0YWIgaGVhZGxpbmUgbGluayByZWQgc28gdGhhdCB0aGUgdXNlciBjYW4gc2VlIHRoYXQgdGhlcmUgaXMgYW4gZXJyb3Jcblx0XHQgKiBpbnNpZGUgdGhlIGVsZW1lbnRzIG9mIHRoaXMgdGFiLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IENvbnRhaW5zIGV2ZW50IGluZm9ybWF0aW9uLlxuXHRcdCAqL1xuXHRcdHZhciBfb25FbWFpbERldGFpbHNWYWxpZGF0aW9uID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIFBhaW50IHRoZSBwYXJlbnQgdGFiIHNvIHRoYXQgdGhlIHVzZXIga25vd3MgdGhhdCB0aGVyZSBpcyBhIHByb2JsZW0gaW4gdGhlIGZvcm0uXG5cdFx0XHRpZiAoJHRoaXMuZmluZCgnLnRhYi1jb250ZW50LmRldGFpbHMgLmVycm9yJykubGVuZ3RoID4gMCkge1xuXHRcdFx0XHQkdGhpcy5maW5kKCcudGFiLWhlYWRsaW5lLmRldGFpbHMnKS5jc3MoJ2NvbG9yJywgJ3JlZCcpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0JHRoaXMuZmluZCgnLnRhYi1oZWFkbGluZS5kZXRhaWxzJykuY3NzKCdjb2xvcicsICcnKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIG1vZHVsZSwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBDb250YWN0cyBEYXRhVGFibGVcblx0XHRcdGpzZS5saWJzLmRhdGF0YWJsZS5jcmVhdGUoJGNvbnRhY3RzVGFibGUsIHtcblx0XHRcdFx0YXV0b1dpZHRoOiBmYWxzZSxcblx0XHRcdFx0b3JkZXI6IFtcblx0XHRcdFx0XHRbMCwgJ2FzYyddIC8vIEVtYWlsIEFTQ1xuXHRcdFx0XHRdLFxuXHRcdFx0XHRsYW5ndWFnZTogKCBqc2UuY29yZS5jb25maWcuZ2V0KCdsYW5ndWFnZUNvZGUnKSA9PT0gJ2RlJylcblx0XHRcdFx0XHQ/IGpzZS5saWJzLmRhdGF0YWJsZS5nZXRHZXJtYW5UcmFuc2xhdGlvbigpXG5cdFx0XHRcdFx0OiBudWxsLFxuXHRcdFx0XHRsZW5ndGhNZW51OiBvcHRpb25zLmxlbmd0aE1lbnUsXG5cdFx0XHRcdHBhZ2VMZW5ndGg6IDUsXG5cdFx0XHRcdGNvbHVtbnM6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiAnZW1haWwnLFxuXHRcdFx0XHRcdFx0d2lkdGg6ICc0NSUnXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiAnbmFtZScsXG5cdFx0XHRcdFx0XHR3aWR0aDogJzM1JSdcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGRhdGE6ICd0eXBlJyxcblx0XHRcdFx0XHRcdHJlbmRlcjogb3B0aW9ucy5jb252ZXJ0VXBwZXJDYXNlLFxuXHRcdFx0XHRcdFx0d2lkdGg6ICcxMCUnXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiBudWxsLFxuXHRcdFx0XHRcdFx0b3JkZXJhYmxlOiBmYWxzZSxcblx0XHRcdFx0XHRcdGRlZmF1bHRDb250ZW50OiAnJyxcblx0XHRcdFx0XHRcdHJlbmRlcjogb3B0aW9ucy5jb250YWN0c1RhYmxlQWN0aW9ucyxcblx0XHRcdFx0XHRcdHdpZHRoOiAnMTAlJyxcblx0XHRcdFx0XHRcdGNsYXNzTmFtZTogJ2R0LWhlYWQtY2VudGVyIGR0LWJvZHktY2VudGVyJ1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIEF0dGFjaG1lbnRzIERhdGFUYWJsZVxuXHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmNyZWF0ZSgkYXR0YWNobWVudHNUYWJsZSwge1xuXHRcdFx0XHRhdXRvV2lkdGg6IGZhbHNlLFxuXHRcdFx0XHRvcmRlcjogW1xuXHRcdFx0XHRcdFswLCAnYXNjJ10gLy8gUGF0aCBBU0Ncblx0XHRcdFx0XSxcblx0XHRcdFx0bGFuZ3VhZ2U6ICgganNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJykgPT09ICdkZScpXG5cdFx0XHRcdFx0PyBqc2UubGlicy5kYXRhdGFibGUuZ2V0R2VybWFuVHJhbnNsYXRpb24oKVxuXHRcdFx0XHRcdDogbnVsbCxcblx0XHRcdFx0bGVuZ3RoTWVudTogb3B0aW9ucy5sZW5ndGhNZW51LFxuXHRcdFx0XHRwYWdlTGVuZ3RoOiA1LFxuXHRcdFx0XHRjb2x1bW5zOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0ZGF0YTogJ3BhdGgnLFxuXHRcdFx0XHRcdFx0d2lkdGg6ICc4MCUnXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiBudWxsLFxuXHRcdFx0XHRcdFx0b3JkZXJhYmxlOiBmYWxzZSxcblx0XHRcdFx0XHRcdGRlZmF1bHRDb250ZW50OiAnJyxcblx0XHRcdFx0XHRcdHJlbmRlcjogb3B0aW9ucy5hdHRhY2htZW50c1RhYmxlQWN0aW9ucyxcblx0XHRcdFx0XHRcdHdpZHRoOiAnMjAlJyxcblx0XHRcdFx0XHRcdGNsYXNzTmFtZTogJ2R0LWhlYWQtY2VudGVyIGR0LWJvZHktY2VudGVyJ1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGpzZS5saWJzLmVtYWlscy51cGRhdGVUYWJDb3VudGVycygkdGhpcyk7XG5cdFx0XHRcblx0XHRcdC8vIEJpbmQgZXZlbnQgaGFuZGxlcnMgb2YgdGhlIG1vZGFsLlxuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdjbGljaycsICcjYWRkLWNvbnRhY3QnLCBfb25BZGRDb250YWN0KVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5kZWxldGUtY29udGFjdCcsIF9vbkRlbGV0ZUNvbnRhY3QpXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmRlbGV0ZS1hdHRhY2htZW50JywgX29uRGVsZXRlQXR0YWNobWVudClcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuZG93bmxvYWQtYXR0YWNobWVudCcsIF9vbkRvd25sb2FkQXR0YWNobWVudClcblx0XHRcdFx0Lm9uKCd2YWxpZGF0b3IudmFsaWRhdGUnLCBfb25FbWFpbERldGFpbHNWYWxpZGF0aW9uKTtcblx0XHRcdFxuXHRcdFx0Ly8gQmluZCB0aGUgZXZlbnQgaGFuZGxlciBmb3IgdGhlIGVtYWlsIGF0dGFjaG1lbnRzIHRvIHRoZSByZXNwb25zaXZlIGZpbGUgbWFuYWdlclxuXHRcdFx0Ly8gaWYgaXQgaXMgYXZhaWxhYmxlLCBlbHNlIHVzZSB0aGUgb2xkIGltcGxlbWVudGF0aW9uLlxuXHRcdFx0aWYgKGd4LndpZGdldHMuY2FjaGUubW9kdWxlcy5maWxlbWFuYWdlcikge1xuXHRcdFx0XHR3aW5kb3cucmVzcG9uc2l2ZV9maWxlbWFuYWdlcl9jYWxsYmFjayA9IGZ1bmN0aW9uIChmaWVsZF9pZCkge1xuXHRcdFx0XHRcdHZhciAkZmllbGQgPSAkKCcjJyArIGZpZWxkX2lkKTtcblx0XHRcdFx0XHR2YXIgdXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1FbWFpbHMvVXBsb2FkQXR0YWNobWVudFdpdGhGaWxlTWFuYWdlcic7XG5cdFx0XHRcdFx0dmFyIGRhdGEgPSB7cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKSwgYXR0YWNobWVudFBhdGg6ICRmaWVsZC52YWwoKX07XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gTWFrZSB0aGUgaW5wdXQgZmllbGQgbm90IGVkaXRhYmxlXG5cdFx0XHRcdFx0JGZpZWxkLnByb3AoJ3JlYWRvbmx5JywgJ3JlYWRvbmx5Jyk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JC5hamF4KFxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHR1cmwsXG5cdFx0XHRcdFx0XHRcdGRhdGEsXG5cdFx0XHRcdFx0XHRcdGRhdGFUeXBlOiAnanNvbicsXG5cdFx0XHRcdFx0XHRcdG1ldGhvZDogJ1BPU1QnXG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0KS5kb25lKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuXHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnI2F0dGFjaG1lbnRzLXRhYmxlJykuRGF0YVRhYmxlKCkucm93LmFkZChcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdHBhdGg6IHJlc3BvbnNlLnBhdGgsXG5cdFx0XHRcdFx0XHRcdFx0ZmlsZV9leGlzdHM6IHRydWVcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0KS5kcmF3KCk7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0anNlLmxpYnMuZW1haWxzLnVwZGF0ZVRhYkNvdW50ZXJzKCR0aGlzKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCR0aGlzLm9uKCd1cGxvYWQnLCAnI3VwbG9hZC1hdHRhY2htZW50JywgX29uVXBsb2FkQXR0YWNobWVudCk7XG5cdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
