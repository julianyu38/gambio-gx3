'use strict';

/* --------------------------------------------------------------
 actions.js 2018-07-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Actions
 *
 * This module creates the bulk and row actions for the table.
 */
gx.controllers.module('actions', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js', 'user_configuration_service', gx.source + '/libs/button_dropdown'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Create Bulk Actions
  *
  * This callback can be called once during the initialization of this module.
  */
	function _createBulkActions() {
		// Add actions to the bulk-action dropdown.
		var $bulkActions = $('.bulk-action');
		var defaultBulkAction = $this.data('defaultBulkAction') || 'change-status';

		jse.libs.button_dropdown.bindDefaultAction($bulkActions, jse.core.registry.get('userId'), 'ordersOverviewBulkAction', jse.libs.user_configuration_service);

		// Change status
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_MULTI_CHANGE_ORDER_STATUS', 'orders'),
			class: 'change-status',
			data: { configurationValue: 'change-status' },
			isDefault: defaultBulkAction === 'change-status',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Delete
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_MULTI_DELETE', 'orders'),
			class: 'delete',
			data: { configurationValue: 'delete' },
			isDefault: defaultBulkAction === 'delete',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Cancel
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_MULTI_CANCEL', 'orders'),
			class: 'cancel',
			data: { configurationValue: 'cancel' },
			isDefault: defaultBulkAction === 'cancel',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Send order confirmation.
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_MULTI_SEND_ORDER', 'orders'),
			class: 'bulk-email-order',
			data: { configurationValue: 'bulk-email-order' },
			isDefault: defaultBulkAction === 'bulk-email-order',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		if (data.isPdfCreatorInstalled === 1) {
			if (data.invoicesGranted) {
				// Send invoice.
				jse.libs.button_dropdown.addAction($bulkActions, {
					text: jse.core.lang.translate('BUTTON_MULTI_SEND_INVOICE', 'orders'),
					class: 'bulk-email-invoice',
					data: { configurationValue: 'bulk-email-invoice' },
					isDefault: defaultBulkAction === 'bulk-email-invoice',
					callback: function callback(e) {
						return e.preventDefault();
					}
				});

				// Download invoices.
				jse.libs.button_dropdown.addAction($bulkActions, {
					text: jse.core.lang.translate('TITLE_DOWNLOAD_INVOICES', 'orders'),
					class: 'bulk-download-invoice',
					data: { configurationValue: 'bulk-download-invoice' },
					isDefault: defaultBulkAction === 'bulk-download-invoice',
					callback: function callback(e) {
						return e.preventDefault();
					}
				});

				// Download packing slips.
				jse.libs.button_dropdown.addAction($bulkActions, {
					text: jse.core.lang.translate('TITLE_DOWNLOAD_PACKINGSLIP', 'orders'),
					class: 'bulk-download-packing-slip',
					data: { configurationValue: 'bulk-download-packing-slip' },
					isDefault: defaultBulkAction === 'bulk-download-packing-slip',
					callback: function callback(e) {
						return e.preventDefault();
					}
				});
			}
		}

		$this.datatable_default_actions('ensure', 'bulk');
	}

	/**
  * Create Table Row Actions
  *
  * This function must be call with every table draw.dt event.
  */
	function _createRowActions() {
		// Re-create the checkbox widgets and the row actions. 
		var defaultRowAction = $this.data('defaultRowAction') || 'edit';

		jse.libs.button_dropdown.bindDefaultAction($this.find('.btn-group.dropdown'), jse.core.registry.get('userId'), 'ordersOverviewRowAction', jse.libs.user_configuration_service);

		$this.find('.btn-group.dropdown').each(function () {
			var orderId = $(this).parents('tr').data('id');
			var editUrl = 'orders.php?' + $.param({
				oID: orderId,
				action: 'edit',
				overview: $.deparam(window.location.search.slice(1))
			});

			// Edit
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('TEXT_SHOW', 'orders'),
				href: editUrl,
				class: 'edit',
				data: { configurationValue: 'edit' },
				isDefault: defaultRowAction === 'edit'
			});

			// Change Status
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('TEXT_GM_STATUS', 'orders'),
				class: 'change-status',
				data: { configurationValue: 'change-status' },
				isDefault: defaultRowAction === 'change-status',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Delete
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_MULTI_DELETE', 'orders'),
				class: 'delete',
				data: { configurationValue: 'delete' },
				isDefault: defaultRowAction === 'delete',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Cancel
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_GM_CANCEL', 'orders'),
				class: 'cancel',
				data: { configurationValue: 'cancel' },
				isDefault: defaultRowAction === 'cancel',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});
			if (data.isPdfCreatorInstalled === 1) {
				if (data.invoicesGranted) {
					// Email Invoice
					jse.libs.button_dropdown.addAction($(this), {
						text: jse.core.lang.translate('TITLE_INVOICE_MAIL', 'orders'),
						class: 'email-invoice',
						data: { configurationValue: 'email-invoice' },
						isDefault: defaultRowAction === 'email-invoice',
						callback: function callback(e) {
							return e.preventDefault();
						}
					});
					if (data.hasInvoices === undefined) {
						// Create Invoice
						jse.libs.button_dropdown.addAction($(this), {
							text: jse.core.lang.translate('TITLE_CREATE_INVOICE', 'orders'),
							href: 'gm_pdf_order.php?oID=' + orderId + '&type=invoice',
							target: '_blank',
							class: 'create-invoice',
							data: { configurationValue: 'create-invoice' },
							isDefault: defaultRowAction === 'create-invoice',
							callback: function callback(e) {
								return e.preventDefault();
							}
						});
					} else if (orderId in data.hasInvoices) {
						// Show Invoice
						jse.libs.button_dropdown.addAction($(this), {
							text: jse.core.lang.translate('TITLE_SHOW_INVOICE', 'orders'),
							class: 'show-invoice',
							data: { configurationValue: 'show-invoice' },
							isDefault: defaultRowAction === 'show-invoice',
							callback: function callback(e) {
								return e.preventDefault();
							}
						});
					} else {
						// Create Invoice
						jse.libs.button_dropdown.addAction($(this), {
							text: jse.core.lang.translate('TITLE_CREATE_INVOICE', 'orders'),
							href: 'gm_pdf_order.php?oID=' + orderId + '&type=invoice',
							target: '_blank',
							class: 'create-invoice',
							data: { configurationValue: 'create-invoice' },
							isDefault: defaultRowAction === 'create-invoice',
							callback: function callback(e) {
								return e.preventDefault();
							}
						});
					}

					// Show Packing Slip
					jse.libs.button_dropdown.addAction($(this), {
						text: jse.core.lang.translate('TITLE_SHOW_PACKINGSLIP', 'orders'),
						class: 'show-packing-slip',
						data: { configurationValue: 'show-packing-slip' },
						isDefault: defaultRowAction === 'show-packing-slip',
						callback: function callback(e) {
							return e.preventDefault();
						}
					});

					// Create Packing Slip
					jse.libs.button_dropdown.addAction($(this), {
						text: jse.core.lang.translate('TITLE_CREATE_PACKINGSLIP', 'orders'),
						href: 'gm_pdf_order.php?oID=' + orderId + '&type=packingslip',
						target: '_blank',
						class: 'packing-slip',
						data: { configurationValue: 'packing-slip' },
						isDefault: defaultRowAction === 'packing-slip'
					});
				}
			}

			// Show Order Acceptance
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('TITLE_ORDER', 'orders'),
				href: 'gm_send_order.php?oID=' + orderId + '&type=order',
				target: '_blank',
				class: 'show-acceptance',
				data: { configurationValue: 'show-acceptance' },
				isDefault: defaultRowAction === 'show-acceptance'
			});

			// Recreate Order Acceptance
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('TITLE_RECREATE_ORDER', 'orders'),
				href: 'gm_send_order.php?oID=' + orderId + '&type=recreate_order',
				target: '_blank',
				class: 'recreate-order-acceptance',
				data: { configurationValue: 'recreate-order-acceptance' },
				isDefault: defaultRowAction === 'recreate-order-acceptance'
			});

			// Email Order
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('TITLE_SEND_ORDER', 'orders'),
				class: 'email-order',
				data: { configurationValue: 'email-order' },
				isDefault: defaultRowAction === 'email-order',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			if (data.withdrawalsGranted) {
				// Create Withdrawal
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('TEXT_CREATE_WITHDRAWAL', 'orders'),
					href: '../withdrawal.php?order_id=' + orderId,
					target: '_blank',
					class: 'create-withdrawal',
					data: { configurationValue: 'create-withdrawal' },
					isDefault: defaultRowAction === 'create-withdrawal'
				});
			}

			// Add Tracking Code
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('TXT_PARCEL_TRACKING_SENDBUTTON_TITLE', 'parcel_services'),
				class: 'add-tracking-number',
				data: { configurationValue: 'add-tracking-number' },
				isDefault: defaultRowAction === 'add-tracking-number',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			$this.datatable_default_actions('ensure', 'row');
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(window).on('JSENGINE_INIT_FINISHED', function () {
			// If there is only a singular item, data.hasInvoices automatically becomes an array,
			// if there is more, it becomes a string(as this is an edge case, since arrays are
			// not natively supported by this implementation) so this is a fix for that scenario.
			if (typeof data.hasInvoices === 'string' && data.hasInvoices !== '') {
				data.hasInvoices = data.hasInvoices.replace(/\\/g, "");
				data.hasInvoices = JSON.parse(data.hasInvoices);
			}
			$this.on('draw.dt', _createRowActions);
			_createRowActions();
			_createBulkActions();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vdmVydmlldy9hY3Rpb25zLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiX2NyZWF0ZUJ1bGtBY3Rpb25zIiwiJGJ1bGtBY3Rpb25zIiwiZGVmYXVsdEJ1bGtBY3Rpb24iLCJsaWJzIiwiYnV0dG9uX2Ryb3Bkb3duIiwiYmluZERlZmF1bHRBY3Rpb24iLCJjb3JlIiwicmVnaXN0cnkiLCJnZXQiLCJ1c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSIsImFkZEFjdGlvbiIsInRleHQiLCJsYW5nIiwidHJhbnNsYXRlIiwiY2xhc3MiLCJjb25maWd1cmF0aW9uVmFsdWUiLCJpc0RlZmF1bHQiLCJjYWxsYmFjayIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImlzUGRmQ3JlYXRvckluc3RhbGxlZCIsImludm9pY2VzR3JhbnRlZCIsImRhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnMiLCJfY3JlYXRlUm93QWN0aW9ucyIsImRlZmF1bHRSb3dBY3Rpb24iLCJmaW5kIiwiZWFjaCIsIm9yZGVySWQiLCJwYXJlbnRzIiwiZWRpdFVybCIsInBhcmFtIiwib0lEIiwiYWN0aW9uIiwib3ZlcnZpZXciLCJkZXBhcmFtIiwid2luZG93IiwibG9jYXRpb24iLCJzZWFyY2giLCJzbGljZSIsImhyZWYiLCJoYXNJbnZvaWNlcyIsInVuZGVmaW5lZCIsInRhcmdldCIsIndpdGhkcmF3YWxzR3JhbnRlZCIsImluaXQiLCJkb25lIiwib24iLCJyZXBsYWNlIiwiSlNPTiIsInBhcnNlIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFNBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLG1EQUVDLDRCQUZELEVBR0lKLEdBQUdJLE1BSFAsMkJBSEQsRUFTQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNTCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNNLGtCQUFULEdBQThCO0FBQzdCO0FBQ0EsTUFBTUMsZUFBZUYsRUFBRSxjQUFGLENBQXJCO0FBQ0EsTUFBTUcsb0JBQW9CSixNQUFNRCxJQUFOLENBQVcsbUJBQVgsS0FBbUMsZUFBN0Q7O0FBRUFGLE1BQUlRLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsaUJBQXpCLENBQTJDSixZQUEzQyxFQUF5RE4sSUFBSVcsSUFBSixDQUFTQyxRQUFULENBQWtCQyxHQUFsQixDQUFzQixRQUF0QixDQUF6RCxFQUNDLDBCQURELEVBQzZCYixJQUFJUSxJQUFKLENBQVNNLDBCQUR0Qzs7QUFHQTtBQUNBZCxNQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DVCxZQUFuQyxFQUFpRDtBQUNoRFUsU0FBTWhCLElBQUlXLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLGtDQUF4QixFQUE0RCxRQUE1RCxDQUQwQztBQUVoREMsVUFBTyxlQUZ5QztBQUdoRGpCLFNBQU0sRUFBQ2tCLG9CQUFvQixlQUFyQixFQUgwQztBQUloREMsY0FBV2Qsc0JBQXNCLGVBSmU7QUFLaERlLGFBQVU7QUFBQSxXQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxzQyxHQUFqRDs7QUFRQTtBQUNBeEIsTUFBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1QsWUFBbkMsRUFBaUQ7QUFDaERVLFNBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixxQkFBeEIsRUFBK0MsUUFBL0MsQ0FEMEM7QUFFaERDLFVBQU8sUUFGeUM7QUFHaERqQixTQUFNLEVBQUNrQixvQkFBb0IsUUFBckIsRUFIMEM7QUFJaERDLGNBQVdkLHNCQUFzQixRQUplO0FBS2hEZSxhQUFVO0FBQUEsV0FBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMc0MsR0FBakQ7O0FBUUE7QUFDQXhCLE1BQUlRLElBQUosQ0FBU0MsZUFBVCxDQUF5Qk0sU0FBekIsQ0FBbUNULFlBQW5DLEVBQWlEO0FBQ2hEVSxTQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLFFBQS9DLENBRDBDO0FBRWhEQyxVQUFPLFFBRnlDO0FBR2hEakIsU0FBTSxFQUFDa0Isb0JBQW9CLFFBQXJCLEVBSDBDO0FBSWhEQyxjQUFXZCxzQkFBc0IsUUFKZTtBQUtoRGUsYUFBVTtBQUFBLFdBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTHNDLEdBQWpEOztBQVFBO0FBQ0F4QixNQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DVCxZQUFuQyxFQUFpRDtBQUNoRFUsU0FBTWhCLElBQUlXLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLHlCQUF4QixFQUFtRCxRQUFuRCxDQUQwQztBQUVoREMsVUFBTyxrQkFGeUM7QUFHaERqQixTQUFNLEVBQUNrQixvQkFBb0Isa0JBQXJCLEVBSDBDO0FBSWhEQyxjQUFXZCxzQkFBc0Isa0JBSmU7QUFLaERlLGFBQVU7QUFBQSxXQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxzQyxHQUFqRDs7QUFRQSxNQUFJdEIsS0FBS3VCLHFCQUFMLEtBQStCLENBQW5DLEVBQXNDO0FBQ3JDLE9BQUl2QixLQUFLd0IsZUFBVCxFQUEwQjtBQUN6QjtBQUNBMUIsUUFBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1QsWUFBbkMsRUFBaUQ7QUFDaERVLFdBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwyQkFBeEIsRUFBcUQsUUFBckQsQ0FEMEM7QUFFaERDLFlBQU8sb0JBRnlDO0FBR2hEakIsV0FBTSxFQUFDa0Isb0JBQW9CLG9CQUFyQixFQUgwQztBQUloREMsZ0JBQVdkLHNCQUFzQixvQkFKZTtBQUtoRGUsZUFBVTtBQUFBLGFBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTHNDLEtBQWpEOztBQVFBO0FBQ0F4QixRQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DVCxZQUFuQyxFQUFpRDtBQUNoRFUsV0FBTWhCLElBQUlXLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLHlCQUF4QixFQUFtRCxRQUFuRCxDQUQwQztBQUVoREMsWUFBTyx1QkFGeUM7QUFHaERqQixXQUFNLEVBQUNrQixvQkFBb0IsdUJBQXJCLEVBSDBDO0FBSWhEQyxnQkFBV2Qsc0JBQXNCLHVCQUplO0FBS2hEZSxlQUFVO0FBQUEsYUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMc0MsS0FBakQ7O0FBUUE7QUFDQXhCLFFBQUlRLElBQUosQ0FBU0MsZUFBVCxDQUF5Qk0sU0FBekIsQ0FBbUNULFlBQW5DLEVBQWlEO0FBQ2hEVSxXQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IsNEJBQXhCLEVBQXNELFFBQXRELENBRDBDO0FBRWhEQyxZQUFPLDRCQUZ5QztBQUdoRGpCLFdBQU0sRUFBQ2tCLG9CQUFvQiw0QkFBckIsRUFIMEM7QUFJaERDLGdCQUFXZCxzQkFBc0IsNEJBSmU7QUFLaERlLGVBQVU7QUFBQSxhQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxzQyxLQUFqRDtBQU9BO0FBQ0Q7O0FBRURyQixRQUFNd0IseUJBQU4sQ0FBZ0MsUUFBaEMsRUFBMEMsTUFBMUM7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxpQkFBVCxHQUE2QjtBQUM1QjtBQUNBLE1BQU1DLG1CQUFtQjFCLE1BQU1ELElBQU4sQ0FBVyxrQkFBWCxLQUFrQyxNQUEzRDs7QUFFQUYsTUFBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCQyxpQkFBekIsQ0FBMkNQLE1BQU0yQixJQUFOLENBQVcscUJBQVgsQ0FBM0MsRUFDQzlCLElBQUlXLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsR0FBbEIsQ0FBc0IsUUFBdEIsQ0FERCxFQUNrQyx5QkFEbEMsRUFDNkRiLElBQUlRLElBQUosQ0FBU00sMEJBRHRFOztBQUdBWCxRQUFNMkIsSUFBTixDQUFXLHFCQUFYLEVBQWtDQyxJQUFsQyxDQUF1QyxZQUFXO0FBQ2pELE9BQU1DLFVBQVU1QixFQUFFLElBQUYsRUFBUTZCLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0IvQixJQUF0QixDQUEyQixJQUEzQixDQUFoQjtBQUNBLE9BQU1nQyxVQUFVLGdCQUFnQjlCLEVBQUUrQixLQUFGLENBQVE7QUFDdkNDLFNBQUtKLE9BRGtDO0FBRXZDSyxZQUFRLE1BRitCO0FBR3ZDQyxjQUFVbEMsRUFBRW1DLE9BQUYsQ0FBVUMsT0FBT0MsUUFBUCxDQUFnQkMsTUFBaEIsQ0FBdUJDLEtBQXZCLENBQTZCLENBQTdCLENBQVY7QUFINkIsSUFBUixDQUFoQzs7QUFNQTtBQUNBM0MsT0FBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1gsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDWSxVQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IsV0FBeEIsRUFBcUMsUUFBckMsQ0FEcUM7QUFFM0MwQixVQUFNVixPQUZxQztBQUczQ2YsV0FBTyxNQUhvQztBQUkzQ2pCLFVBQU0sRUFBQ2tCLG9CQUFvQixNQUFyQixFQUpxQztBQUszQ0MsZUFBV1EscUJBQXFCO0FBTFcsSUFBNUM7O0FBUUE7QUFDQTdCLE9BQUlRLElBQUosQ0FBU0MsZUFBVCxDQUF5Qk0sU0FBekIsQ0FBbUNYLEVBQUUsSUFBRixDQUFuQyxFQUE0QztBQUMzQ1ksVUFBTWhCLElBQUlXLElBQUosQ0FBU00sSUFBVCxDQUFjQyxTQUFkLENBQXdCLGdCQUF4QixFQUEwQyxRQUExQyxDQURxQztBQUUzQ0MsV0FBTyxlQUZvQztBQUczQ2pCLFVBQU0sRUFBQ2tCLG9CQUFvQixlQUFyQixFQUhxQztBQUkzQ0MsZUFBV1EscUJBQXFCLGVBSlc7QUFLM0NQLGNBQVU7QUFBQSxZQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxpQyxJQUE1Qzs7QUFRQTtBQUNBeEIsT0FBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1gsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDWSxVQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLFFBQS9DLENBRHFDO0FBRTNDQyxXQUFPLFFBRm9DO0FBRzNDakIsVUFBTSxFQUFDa0Isb0JBQW9CLFFBQXJCLEVBSHFDO0FBSTNDQyxlQUFXUSxxQkFBcUIsUUFKVztBQUszQ1AsY0FBVTtBQUFBLFlBQUtDLEVBQUVDLGNBQUYsRUFBTDtBQUFBO0FBTGlDLElBQTVDOztBQVFBO0FBQ0F4QixPQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NZLFVBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQkFBeEIsRUFBNEMsUUFBNUMsQ0FEcUM7QUFFM0NDLFdBQU8sUUFGb0M7QUFHM0NqQixVQUFNLEVBQUNrQixvQkFBb0IsUUFBckIsRUFIcUM7QUFJM0NDLGVBQVdRLHFCQUFxQixRQUpXO0FBSzNDUCxjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsSUFBNUM7QUFPQSxPQUFJdEIsS0FBS3VCLHFCQUFMLEtBQStCLENBQW5DLEVBQXNDO0FBQ3JDLFFBQUl2QixLQUFLd0IsZUFBVCxFQUEwQjtBQUN6QjtBQUNBMUIsU0FBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1gsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDWSxZQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0Isb0JBQXhCLEVBQThDLFFBQTlDLENBRHFDO0FBRTNDQyxhQUFPLGVBRm9DO0FBRzNDakIsWUFBTSxFQUFDa0Isb0JBQW9CLGVBQXJCLEVBSHFDO0FBSTNDQyxpQkFBV1EscUJBQXFCLGVBSlc7QUFLM0NQLGdCQUFVO0FBQUEsY0FBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsTUFBNUM7QUFPQSxTQUFJdEIsS0FBSzJDLFdBQUwsS0FBcUJDLFNBQXpCLEVBQW9DO0FBQ25DO0FBQ0E5QyxVQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NZLGFBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixzQkFBeEIsRUFBZ0QsUUFBaEQsQ0FEcUM7QUFFM0MwQix1Q0FBOEJaLE9BQTlCLGtCQUYyQztBQUczQ2UsZUFBUSxRQUhtQztBQUkzQzVCLGNBQU8sZ0JBSm9DO0FBSzNDakIsYUFBTSxFQUFDa0Isb0JBQW9CLGdCQUFyQixFQUxxQztBQU0zQ0Msa0JBQVdRLHFCQUFxQixnQkFOVztBQU8zQ1AsaUJBQVU7QUFBQSxlQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQVBpQyxPQUE1QztBQVNBLE1BWEQsTUFZSyxJQUFJUSxXQUFXOUIsS0FBSzJDLFdBQXBCLEVBQWlDO0FBQ3JDO0FBQ0E3QyxVQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NZLGFBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixvQkFBeEIsRUFBOEMsUUFBOUMsQ0FEcUM7QUFFM0NDLGNBQU8sY0FGb0M7QUFHM0NqQixhQUFNLEVBQUNrQixvQkFBb0IsY0FBckIsRUFIcUM7QUFJM0NDLGtCQUFXUSxxQkFBcUIsY0FKVztBQUszQ1AsaUJBQVU7QUFBQSxlQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxpQyxPQUE1QztBQU9BLE1BVEksTUFTRTtBQUNOO0FBQ0F4QixVQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NZLGFBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixzQkFBeEIsRUFBZ0QsUUFBaEQsQ0FEcUM7QUFFM0MwQix1Q0FBOEJaLE9BQTlCLGtCQUYyQztBQUczQ2UsZUFBUSxRQUhtQztBQUkzQzVCLGNBQU8sZ0JBSm9DO0FBSzNDakIsYUFBTSxFQUFDa0Isb0JBQW9CLGdCQUFyQixFQUxxQztBQU0zQ0Msa0JBQVdRLHFCQUFxQixnQkFOVztBQU8zQ1AsaUJBQVU7QUFBQSxlQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQVBpQyxPQUE1QztBQVNBOztBQUVEO0FBQ0F4QixTQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NZLFlBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix3QkFBeEIsRUFBa0QsUUFBbEQsQ0FEcUM7QUFFM0NDLGFBQU8sbUJBRm9DO0FBRzNDakIsWUFBTSxFQUFDa0Isb0JBQW9CLG1CQUFyQixFQUhxQztBQUkzQ0MsaUJBQVdRLHFCQUFxQixtQkFKVztBQUszQ1AsZ0JBQVU7QUFBQSxjQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxpQyxNQUE1Qzs7QUFRQTtBQUNBeEIsU0FBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1gsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDWSxZQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0IsMEJBQXhCLEVBQW9ELFFBQXBELENBRHFDO0FBRTNDMEIsc0NBQThCWixPQUE5QixzQkFGMkM7QUFHM0NlLGNBQVEsUUFIbUM7QUFJM0M1QixhQUFPLGNBSm9DO0FBSzNDakIsWUFBTSxFQUFDa0Isb0JBQW9CLGNBQXJCLEVBTHFDO0FBTTNDQyxpQkFBV1EscUJBQXFCO0FBTlcsTUFBNUM7QUFRQTtBQUNEOztBQUVEO0FBQ0E3QixPQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NZLFVBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxRQUF2QyxDQURxQztBQUUzQzBCLHFDQUErQlosT0FBL0IsZ0JBRjJDO0FBRzNDZSxZQUFRLFFBSG1DO0FBSTNDNUIsV0FBTyxpQkFKb0M7QUFLM0NqQixVQUFNLEVBQUNrQixvQkFBb0IsaUJBQXJCLEVBTHFDO0FBTTNDQyxlQUFXUSxxQkFBcUI7QUFOVyxJQUE1Qzs7QUFTQTtBQUNBN0IsT0FBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1gsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDWSxVQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0Isc0JBQXhCLEVBQWdELFFBQWhELENBRHFDO0FBRTNDMEIscUNBQStCWixPQUEvQix5QkFGMkM7QUFHM0NlLFlBQVEsUUFIbUM7QUFJM0M1QixXQUFPLDJCQUpvQztBQUszQ2pCLFVBQU0sRUFBQ2tCLG9CQUFvQiwyQkFBckIsRUFMcUM7QUFNM0NDLGVBQVdRLHFCQUFxQjtBQU5XLElBQTVDOztBQVNBO0FBQ0E3QixPQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJNLFNBQXpCLENBQW1DWCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NZLFVBQU1oQixJQUFJVyxJQUFKLENBQVNNLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQkFBeEIsRUFBNEMsUUFBNUMsQ0FEcUM7QUFFM0NDLFdBQU8sYUFGb0M7QUFHM0NqQixVQUFNLEVBQUNrQixvQkFBb0IsYUFBckIsRUFIcUM7QUFJM0NDLGVBQVdRLHFCQUFxQixhQUpXO0FBSzNDUCxjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsSUFBNUM7O0FBUUEsT0FBSXRCLEtBQUs4QyxrQkFBVCxFQUE2QjtBQUM1QjtBQUNBaEQsUUFBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1gsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDWSxXQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0Isd0JBQXhCLEVBQWtELFFBQWxELENBRHFDO0FBRTNDMEIsMkNBQW9DWixPQUZPO0FBRzNDZSxhQUFRLFFBSG1DO0FBSTNDNUIsWUFBTyxtQkFKb0M7QUFLM0NqQixXQUFNLEVBQUNrQixvQkFBb0IsbUJBQXJCLEVBTHFDO0FBTTNDQyxnQkFBV1EscUJBQXFCO0FBTlcsS0FBNUM7QUFRQTs7QUFFRDtBQUNBN0IsT0FBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCTSxTQUF6QixDQUFtQ1gsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDWSxVQUFNaEIsSUFBSVcsSUFBSixDQUFTTSxJQUFULENBQWNDLFNBQWQsQ0FBd0Isc0NBQXhCLEVBQWdFLGlCQUFoRSxDQURxQztBQUUzQ0MsV0FBTyxxQkFGb0M7QUFHM0NqQixVQUFNLEVBQUNrQixvQkFBb0IscUJBQXJCLEVBSHFDO0FBSTNDQyxlQUFXUSxxQkFBcUIscUJBSlc7QUFLM0NQLGNBQVU7QUFBQSxZQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxpQyxJQUE1Qzs7QUFRQXJCLFNBQU13Qix5QkFBTixDQUFnQyxRQUFoQyxFQUEwQyxLQUExQztBQUNBLEdBL0pEO0FBZ0tBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTVCLFFBQU9rRCxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCOUMsSUFBRW9DLE1BQUYsRUFBVVcsRUFBVixDQUFhLHdCQUFiLEVBQXVDLFlBQU07QUFDNUM7QUFDQTtBQUNBO0FBQ0EsT0FBSSxPQUFPakQsS0FBSzJDLFdBQVosS0FBNEIsUUFBNUIsSUFBd0MzQyxLQUFLMkMsV0FBTCxLQUFxQixFQUFqRSxFQUFxRTtBQUNwRTNDLFNBQUsyQyxXQUFMLEdBQW1CM0MsS0FBSzJDLFdBQUwsQ0FBaUJPLE9BQWpCLENBQXlCLEtBQXpCLEVBQWdDLEVBQWhDLENBQW5CO0FBQ0FsRCxTQUFLMkMsV0FBTCxHQUFtQlEsS0FBS0MsS0FBTCxDQUFXcEQsS0FBSzJDLFdBQWhCLENBQW5CO0FBQ0E7QUFDRDFDLFNBQU1nRCxFQUFOLENBQVMsU0FBVCxFQUFvQnZCLGlCQUFwQjtBQUNBQTtBQUNBdkI7QUFDQSxHQVhEOztBQWFBNkM7QUFDQSxFQWZEOztBQWlCQSxRQUFPbkQsTUFBUDtBQUVBLENBM1RGIiwiZmlsZSI6Im9yZGVycy9vdmVydmlldy9hY3Rpb25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGFjdGlvbnMuanMgMjAxOC0wNy0wNlxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTggR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBNYWluIFRhYmxlIEFjdGlvbnNcclxuICpcclxuICogVGhpcyBtb2R1bGUgY3JlYXRlcyB0aGUgYnVsayBhbmQgcm93IGFjdGlvbnMgZm9yIHRoZSB0YWJsZS5cclxuICovXHJcbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcclxuXHQnYWN0aW9ucycsXHJcblx0XHJcblx0W1xyXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS1kZXBhcmFtL2pxdWVyeS1kZXBhcmFtLm1pbi5qc2AsXHJcblx0XHQndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnLFxyXG5cdFx0YCR7Z3guc291cmNlfS9saWJzL2J1dHRvbl9kcm9wZG93bmBcclxuXHRdLFxyXG5cdFxyXG5cdGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFxyXG5cdFx0J3VzZSBzdHJpY3QnO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIFZBUklBQkxFU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIEZVTkNUSU9OU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogQ3JlYXRlIEJ1bGsgQWN0aW9uc1xyXG5cdFx0ICpcclxuXHRcdCAqIFRoaXMgY2FsbGJhY2sgY2FuIGJlIGNhbGxlZCBvbmNlIGR1cmluZyB0aGUgaW5pdGlhbGl6YXRpb24gb2YgdGhpcyBtb2R1bGUuXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9jcmVhdGVCdWxrQWN0aW9ucygpIHtcclxuXHRcdFx0Ly8gQWRkIGFjdGlvbnMgdG8gdGhlIGJ1bGstYWN0aW9uIGRyb3Bkb3duLlxyXG5cdFx0XHRjb25zdCAkYnVsa0FjdGlvbnMgPSAkKCcuYnVsay1hY3Rpb24nKTtcclxuXHRcdFx0Y29uc3QgZGVmYXVsdEJ1bGtBY3Rpb24gPSAkdGhpcy5kYXRhKCdkZWZhdWx0QnVsa0FjdGlvbicpIHx8ICdjaGFuZ2Utc3RhdHVzJztcclxuXHRcdFx0XHJcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5iaW5kRGVmYXVsdEFjdGlvbigkYnVsa0FjdGlvbnMsIGpzZS5jb3JlLnJlZ2lzdHJ5LmdldCgndXNlcklkJyksXHJcblx0XHRcdFx0J29yZGVyc092ZXJ2aWV3QnVsa0FjdGlvbicsIGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIENoYW5nZSBzdGF0dXNcclxuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkYnVsa0FjdGlvbnMsIHtcclxuXHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX01VTFRJX0NIQU5HRV9PUkRFUl9TVEFUVVMnLCAnb3JkZXJzJyksXHJcblx0XHRcdFx0Y2xhc3M6ICdjaGFuZ2Utc3RhdHVzJyxcclxuXHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnY2hhbmdlLXN0YXR1cyd9LFxyXG5cdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdEJ1bGtBY3Rpb24gPT09ICdjaGFuZ2Utc3RhdHVzJyxcclxuXHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBEZWxldGVcclxuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkYnVsa0FjdGlvbnMsIHtcclxuXHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX01VTFRJX0RFTEVURScsICdvcmRlcnMnKSxcclxuXHRcdFx0XHRjbGFzczogJ2RlbGV0ZScsXHJcblx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2RlbGV0ZSd9LFxyXG5cdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdEJ1bGtBY3Rpb24gPT09ICdkZWxldGUnLFxyXG5cdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIENhbmNlbFxyXG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCRidWxrQWN0aW9ucywge1xyXG5cdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fTVVMVElfQ0FOQ0VMJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdGNsYXNzOiAnY2FuY2VsJyxcclxuXHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnY2FuY2VsJ30sXHJcblx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0QnVsa0FjdGlvbiA9PT0gJ2NhbmNlbCcsXHJcblx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gU2VuZCBvcmRlciBjb25maXJtYXRpb24uXHJcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XHJcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9NVUxUSV9TRU5EX09SREVSJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdGNsYXNzOiAnYnVsay1lbWFpbC1vcmRlcicsXHJcblx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2J1bGstZW1haWwtb3JkZXInfSxcclxuXHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRCdWxrQWN0aW9uID09PSAnYnVsay1lbWFpbC1vcmRlcicsXHJcblx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKGRhdGEuaXNQZGZDcmVhdG9ySW5zdGFsbGVkID09PSAxKSB7XHJcblx0XHRcdFx0aWYgKGRhdGEuaW52b2ljZXNHcmFudGVkKSB7XHJcblx0XHRcdFx0XHQvLyBTZW5kIGludm9pY2UuXHJcblx0XHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCRidWxrQWN0aW9ucywge1xyXG5cdFx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX01VTFRJX1NFTkRfSU5WT0lDRScsICdvcmRlcnMnKSxcclxuXHRcdFx0XHRcdFx0Y2xhc3M6ICdidWxrLWVtYWlsLWludm9pY2UnLFxyXG5cdFx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnYnVsay1lbWFpbC1pbnZvaWNlJ30sXHJcblx0XHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdEJ1bGtBY3Rpb24gPT09ICdidWxrLWVtYWlsLWludm9pY2UnLFxyXG5cdFx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBEb3dubG9hZCBpbnZvaWNlcy5cclxuXHRcdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XHJcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUSVRMRV9ET1dOTE9BRF9JTlZPSUNFUycsICdvcmRlcnMnKSxcclxuXHRcdFx0XHRcdFx0Y2xhc3M6ICdidWxrLWRvd25sb2FkLWludm9pY2UnLFxyXG5cdFx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnYnVsay1kb3dubG9hZC1pbnZvaWNlJ30sXHJcblx0XHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdEJ1bGtBY3Rpb24gPT09ICdidWxrLWRvd25sb2FkLWludm9pY2UnLFxyXG5cdFx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBEb3dubG9hZCBwYWNraW5nIHNsaXBzLlxyXG5cdFx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkYnVsa0FjdGlvbnMsIHtcclxuXHRcdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RJVExFX0RPV05MT0FEX1BBQ0tJTkdTTElQJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdFx0XHRjbGFzczogJ2J1bGstZG93bmxvYWQtcGFja2luZy1zbGlwJyxcclxuXHRcdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2J1bGstZG93bmxvYWQtcGFja2luZy1zbGlwJ30sXHJcblx0XHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdEJ1bGtBY3Rpb24gPT09ICdidWxrLWRvd25sb2FkLXBhY2tpbmctc2xpcCcsXHJcblx0XHRcdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHQkdGhpcy5kYXRhdGFibGVfZGVmYXVsdF9hY3Rpb25zKCdlbnN1cmUnLCAnYnVsaycpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIENyZWF0ZSBUYWJsZSBSb3cgQWN0aW9uc1xyXG5cdFx0ICpcclxuXHRcdCAqIFRoaXMgZnVuY3Rpb24gbXVzdCBiZSBjYWxsIHdpdGggZXZlcnkgdGFibGUgZHJhdy5kdCBldmVudC5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX2NyZWF0ZVJvd0FjdGlvbnMoKSB7XHJcblx0XHRcdC8vIFJlLWNyZWF0ZSB0aGUgY2hlY2tib3ggd2lkZ2V0cyBhbmQgdGhlIHJvdyBhY3Rpb25zLiBcclxuXHRcdFx0Y29uc3QgZGVmYXVsdFJvd0FjdGlvbiA9ICR0aGlzLmRhdGEoJ2RlZmF1bHRSb3dBY3Rpb24nKSB8fCAnZWRpdCc7XHJcblx0XHRcdFxyXG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYmluZERlZmF1bHRBY3Rpb24oJHRoaXMuZmluZCgnLmJ0bi1ncm91cC5kcm9wZG93bicpLFxyXG5cdFx0XHRcdGpzZS5jb3JlLnJlZ2lzdHJ5LmdldCgndXNlcklkJyksICdvcmRlcnNPdmVydmlld1Jvd0FjdGlvbicsIGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlKTtcclxuXHRcdFx0XHJcblx0XHRcdCR0aGlzLmZpbmQoJy5idG4tZ3JvdXAuZHJvcGRvd24nKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGNvbnN0IG9yZGVySWQgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKTtcclxuXHRcdFx0XHRjb25zdCBlZGl0VXJsID0gJ29yZGVycy5waHA/JyArICQucGFyYW0oe1xyXG5cdFx0XHRcdFx0b0lEOiBvcmRlcklkLFxyXG5cdFx0XHRcdFx0YWN0aW9uOiAnZWRpdCcsXHJcblx0XHRcdFx0XHRvdmVydmlldzogJC5kZXBhcmFtKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSkpXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gRWRpdFxyXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xyXG5cdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfU0hPVycsICdvcmRlcnMnKSxcclxuXHRcdFx0XHRcdGhyZWY6IGVkaXRVcmwsXHJcblx0XHRcdFx0XHRjbGFzczogJ2VkaXQnLFxyXG5cdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2VkaXQnfSxcclxuXHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ2VkaXQnXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gQ2hhbmdlIFN0YXR1c1xyXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xyXG5cdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfR01fU1RBVFVTJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdFx0Y2xhc3M6ICdjaGFuZ2Utc3RhdHVzJyxcclxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdjaGFuZ2Utc3RhdHVzJ30sXHJcblx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdjaGFuZ2Utc3RhdHVzJyxcclxuXHRcdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIERlbGV0ZVxyXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xyXG5cdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9NVUxUSV9ERUxFVEUnLCAnb3JkZXJzJyksXHJcblx0XHRcdFx0XHRjbGFzczogJ2RlbGV0ZScsXHJcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnZGVsZXRlJ30sXHJcblx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdkZWxldGUnLFxyXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gQ2FuY2VsXHJcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XHJcblx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX0dNX0NBTkNFTCcsICdvcmRlcnMnKSxcclxuXHRcdFx0XHRcdGNsYXNzOiAnY2FuY2VsJyxcclxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdjYW5jZWwnfSxcclxuXHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ2NhbmNlbCcsXHJcblx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRpZiAoZGF0YS5pc1BkZkNyZWF0b3JJbnN0YWxsZWQgPT09IDEpIHtcclxuXHRcdFx0XHRcdGlmIChkYXRhLmludm9pY2VzR3JhbnRlZCkge1xyXG5cdFx0XHRcdFx0XHQvLyBFbWFpbCBJbnZvaWNlXHJcblx0XHRcdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xyXG5cdFx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUSVRMRV9JTlZPSUNFX01BSUwnLCAnb3JkZXJzJyksXHJcblx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdlbWFpbC1pbnZvaWNlJyxcclxuXHRcdFx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnZW1haWwtaW52b2ljZSd9LFxyXG5cdFx0XHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ2VtYWlsLWludm9pY2UnLFxyXG5cdFx0XHRcdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0aWYgKGRhdGEuaGFzSW52b2ljZXMgPT09IHVuZGVmaW5lZCkge1xyXG5cdFx0XHRcdFx0XHRcdC8vIENyZWF0ZSBJbnZvaWNlXHJcblx0XHRcdFx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XHJcblx0XHRcdFx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVElUTEVfQ1JFQVRFX0lOVk9JQ0UnLCAnb3JkZXJzJyksXHJcblx0XHRcdFx0XHRcdFx0XHRocmVmOiBgZ21fcGRmX29yZGVyLnBocD9vSUQ9JHtvcmRlcklkfSZ0eXBlPWludm9pY2VgLFxyXG5cdFx0XHRcdFx0XHRcdFx0dGFyZ2V0OiAnX2JsYW5rJyxcclxuXHRcdFx0XHRcdFx0XHRcdGNsYXNzOiAnY3JlYXRlLWludm9pY2UnLFxyXG5cdFx0XHRcdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2NyZWF0ZS1pbnZvaWNlJ30sXHJcblx0XHRcdFx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdjcmVhdGUtaW52b2ljZScsXHJcblx0XHRcdFx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRlbHNlIGlmIChvcmRlcklkIGluIGRhdGEuaGFzSW52b2ljZXMpIHtcclxuXHRcdFx0XHRcdFx0XHQvLyBTaG93IEludm9pY2VcclxuXHRcdFx0XHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcclxuXHRcdFx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUSVRMRV9TSE9XX0lOVk9JQ0UnLCAnb3JkZXJzJyksXHJcblx0XHRcdFx0XHRcdFx0XHRjbGFzczogJ3Nob3ctaW52b2ljZScsXHJcblx0XHRcdFx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnc2hvdy1pbnZvaWNlJ30sXHJcblx0XHRcdFx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdzaG93LWludm9pY2UnLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0Ly8gQ3JlYXRlIEludm9pY2VcclxuXHRcdFx0XHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcclxuXHRcdFx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUSVRMRV9DUkVBVEVfSU5WT0lDRScsICdvcmRlcnMnKSxcclxuXHRcdFx0XHRcdFx0XHRcdGhyZWY6IGBnbV9wZGZfb3JkZXIucGhwP29JRD0ke29yZGVySWR9JnR5cGU9aW52b2ljZWAsXHJcblx0XHRcdFx0XHRcdFx0XHR0YXJnZXQ6ICdfYmxhbmsnLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdjcmVhdGUtaW52b2ljZScsXHJcblx0XHRcdFx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnY3JlYXRlLWludm9pY2UnfSxcclxuXHRcdFx0XHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ2NyZWF0ZS1pbnZvaWNlJyxcclxuXHRcdFx0XHRcdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxyXG5cdFx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHQvLyBTaG93IFBhY2tpbmcgU2xpcFxyXG5cdFx0XHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcclxuXHRcdFx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVElUTEVfU0hPV19QQUNLSU5HU0xJUCcsICdvcmRlcnMnKSxcclxuXHRcdFx0XHRcdFx0XHRjbGFzczogJ3Nob3ctcGFja2luZy1zbGlwJyxcclxuXHRcdFx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnc2hvdy1wYWNraW5nLXNsaXAnfSxcclxuXHRcdFx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdzaG93LXBhY2tpbmctc2xpcCcsXHJcblx0XHRcdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gQ3JlYXRlIFBhY2tpbmcgU2xpcFxyXG5cdFx0XHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcclxuXHRcdFx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVElUTEVfQ1JFQVRFX1BBQ0tJTkdTTElQJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdFx0XHRcdGhyZWY6IGBnbV9wZGZfb3JkZXIucGhwP29JRD0ke29yZGVySWR9JnR5cGU9cGFja2luZ3NsaXBgLFxyXG5cdFx0XHRcdFx0XHRcdHRhcmdldDogJ19ibGFuaycsXHJcblx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdwYWNraW5nLXNsaXAnLFxyXG5cdFx0XHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdwYWNraW5nLXNsaXAnfSxcclxuXHRcdFx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdwYWNraW5nLXNsaXAnXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBTaG93IE9yZGVyIEFjY2VwdGFuY2VcclxuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcclxuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUSVRMRV9PUkRFUicsICdvcmRlcnMnKSxcclxuXHRcdFx0XHRcdGhyZWY6IGBnbV9zZW5kX29yZGVyLnBocD9vSUQ9JHtvcmRlcklkfSZ0eXBlPW9yZGVyYCxcclxuXHRcdFx0XHRcdHRhcmdldDogJ19ibGFuaycsXHJcblx0XHRcdFx0XHRjbGFzczogJ3Nob3ctYWNjZXB0YW5jZScsXHJcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnc2hvdy1hY2NlcHRhbmNlJ30sXHJcblx0XHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRSb3dBY3Rpb24gPT09ICdzaG93LWFjY2VwdGFuY2UnXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gUmVjcmVhdGUgT3JkZXIgQWNjZXB0YW5jZVxyXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xyXG5cdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RJVExFX1JFQ1JFQVRFX09SREVSJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdFx0aHJlZjogYGdtX3NlbmRfb3JkZXIucGhwP29JRD0ke29yZGVySWR9JnR5cGU9cmVjcmVhdGVfb3JkZXJgLFxyXG5cdFx0XHRcdFx0dGFyZ2V0OiAnX2JsYW5rJyxcclxuXHRcdFx0XHRcdGNsYXNzOiAncmVjcmVhdGUtb3JkZXItYWNjZXB0YW5jZScsXHJcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAncmVjcmVhdGUtb3JkZXItYWNjZXB0YW5jZSd9LFxyXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAncmVjcmVhdGUtb3JkZXItYWNjZXB0YW5jZSdcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBFbWFpbCBPcmRlclxyXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xyXG5cdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RJVExFX1NFTkRfT1JERVInLCAnb3JkZXJzJyksXHJcblx0XHRcdFx0XHRjbGFzczogJ2VtYWlsLW9yZGVyJyxcclxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdlbWFpbC1vcmRlcid9LFxyXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAnZW1haWwtb3JkZXInLFxyXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKGRhdGEud2l0aGRyYXdhbHNHcmFudGVkKSB7XHJcblx0XHRcdFx0XHQvLyBDcmVhdGUgV2l0aGRyYXdhbFxyXG5cdFx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XHJcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdURVhUX0NSRUFURV9XSVRIRFJBV0FMJywgJ29yZGVycycpLFxyXG5cdFx0XHRcdFx0XHRocmVmOiBgLi4vd2l0aGRyYXdhbC5waHA/b3JkZXJfaWQ9JHtvcmRlcklkfWAsXHJcblx0XHRcdFx0XHRcdHRhcmdldDogJ19ibGFuaycsXHJcblx0XHRcdFx0XHRcdGNsYXNzOiAnY3JlYXRlLXdpdGhkcmF3YWwnLFxyXG5cdFx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnY3JlYXRlLXdpdGhkcmF3YWwnfSxcclxuXHRcdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAnY3JlYXRlLXdpdGhkcmF3YWwnXHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gQWRkIFRyYWNraW5nIENvZGVcclxuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uYWRkQWN0aW9uKCQodGhpcyksIHtcclxuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUWFRfUEFSQ0VMX1RSQUNLSU5HX1NFTkRCVVRUT05fVElUTEUnLCAncGFyY2VsX3NlcnZpY2VzJyksXHJcblx0XHRcdFx0XHRjbGFzczogJ2FkZC10cmFja2luZy1udW1iZXInLFxyXG5cdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ2FkZC10cmFja2luZy1udW1iZXInfSxcclxuXHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ2FkZC10cmFja2luZy1udW1iZXInLFxyXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JHRoaXMuZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucygnZW5zdXJlJywgJ3JvdycpO1xyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0XHQkKHdpbmRvdykub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCAoKSA9PiB7XHJcblx0XHRcdFx0Ly8gSWYgdGhlcmUgaXMgb25seSBhIHNpbmd1bGFyIGl0ZW0sIGRhdGEuaGFzSW52b2ljZXMgYXV0b21hdGljYWxseSBiZWNvbWVzIGFuIGFycmF5LFxyXG5cdFx0XHRcdC8vIGlmIHRoZXJlIGlzIG1vcmUsIGl0IGJlY29tZXMgYSBzdHJpbmcoYXMgdGhpcyBpcyBhbiBlZGdlIGNhc2UsIHNpbmNlIGFycmF5cyBhcmVcclxuXHRcdFx0XHQvLyBub3QgbmF0aXZlbHkgc3VwcG9ydGVkIGJ5IHRoaXMgaW1wbGVtZW50YXRpb24pIHNvIHRoaXMgaXMgYSBmaXggZm9yIHRoYXQgc2NlbmFyaW8uXHJcblx0XHRcdFx0aWYgKHR5cGVvZiBkYXRhLmhhc0ludm9pY2VzID09PSAnc3RyaW5nJyAmJiBkYXRhLmhhc0ludm9pY2VzICE9PSAnJykge1xyXG5cdFx0XHRcdFx0ZGF0YS5oYXNJbnZvaWNlcyA9IGRhdGEuaGFzSW52b2ljZXMucmVwbGFjZSgvXFxcXC9nLCBcIlwiKTtcclxuXHRcdFx0XHRcdGRhdGEuaGFzSW52b2ljZXMgPSBKU09OLnBhcnNlKGRhdGEuaGFzSW52b2ljZXMpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHQkdGhpcy5vbignZHJhdy5kdCcsIF9jcmVhdGVSb3dBY3Rpb25zKTtcclxuXHRcdFx0XHRfY3JlYXRlUm93QWN0aW9ucygpO1xyXG5cdFx0XHRcdF9jcmVhdGVCdWxrQWN0aW9ucygpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdGRvbmUoKTtcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdHJldHVybiBtb2R1bGU7XHJcblx0XHRcclxuXHR9KTsiXX0=
