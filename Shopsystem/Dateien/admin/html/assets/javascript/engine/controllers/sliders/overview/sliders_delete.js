'use strict';

/* --------------------------------------------------------------
 sliders_delete.js 2016-09-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Sliders Overview Delete
 * 
 * Controller Module To Delete Sliders.
 *
 * Handles the delete operation of the sliders overview page.
 */
gx.controllers.module('sliders_delete', ['modal', gx.source + '/libs/info_box'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector 
  * 
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Slider delete confirmation modal.
  * 
  * @type {jQuery}
  */
	var $modal = $('.delete-slider.modal');

	/**
  * Delete button selector string.
  * 
  * @type {String}
  */
	var deleteButtonSelector = '.btn-delete';

	/**
  * Delete slider action URL.
  * 
  * @type {String}
  */
	var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=SlidersOverviewAjax/DeleteSlider';

	/**
  * Module Instance 
  * 
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Handles the delete click event by opening the delete confirmation modal.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onDeleteClick(event) {
		// Prevent default action.
		event.preventDefault();

		// Table row.
		var $parentTableRow = $(event.target).parents('tr');

		// Delete confirmation modal button.
		var $confirmButton = $modal.find('button.confirm');

		// Empty modal body.
		$modal.find('.modal-body .form-group').remove();

		// Slider data.
		var sliderData = {
			id: $parentTableRow.data('sliderId'),
			name: $parentTableRow.data('sliderName'),
			slidesQuantity: $parentTableRow.data('slidesQuantity')
		};

		// Put new slider information into modal body.
		$modal.find('.modal-body fieldset').html(_generateSliderInfoMarkup(sliderData));

		// Show modal.
		$modal.modal('show');

		// Handle delete confirmation modal button click event.
		$confirmButton.off('click').on('click', function () {
			return _onConfirmButtonClick(sliderData.id);
		});
	}

	/**
  * Handles the delete confirmation button click event by removing the slider through an AJAX request.
  *
  * @param {Number} sliderId Slider ID.
  */
	function _onConfirmButtonClick(sliderId) {
		// AJAX request options.
		var requestOptions = {
			type: 'POST',
			data: { sliderId: sliderId },
			url: url
		};

		// Perform request.
		$.ajax(requestOptions).done(function (response) {
			return _handleDeleteRequestResponse(response, sliderId);
		}).always(function () {
			return $modal.modal('hide');
		});
	}

	/**
  * Handles slider deletion AJAX action server response.
  *
  * @param {Object} response Server response.
  * @param {Number} sliderId ID of deleted slider.
  */
	function _handleDeleteRequestResponse(response, sliderId) {
		// Error message phrases.
		var errorTitle = jse.core.lang.translate('DELETE_SLIDER_ERROR_TITLE', 'sliders');
		var errorMessage = jse.core.lang.translate('DELETE_SLIDER_ERROR_TEXT', 'sliders');

		// Table body.
		var $tableBody = $this.find('tbody');

		// Table rows.
		var $rows = $tableBody.find('[data-slider-id]');

		// Table rows that will be deleted.
		var $rowToDelete = $rows.filter('[data-slider-id="' + sliderId + '"]');

		// 'No results' message table row template.
		var $emptyRowTemplate = $('#template-table-row-empty');

		// Check for action success.
		if (response.includes('success')) {
			// Delete respective table rows.
			$rowToDelete.remove();

			// Add success message to admin info box.
			jse.libs.info_box.addSuccessMessage();

			// If there are no rows, show 'No results' message row.
			if ($rows.length - 1 < 1) {
				$tableBody.empty().append($emptyRowTemplate.clone().html());
			}
		} else {
			// Show error message modal.
			jse.libs.modal.showMessage(errorTitle, errorMessage);
		}
	}

	/**
  * Generates HTML containing the slider information for the delete confirmation modal.
  *
  * @param {Object} data Slider data.
  * @param {String} data.name Name of the slider.
  * @param {String} data.slidesQuantity Quantity of slides from slider.
  * 
  * @return {String} Created HTML string.
  */
	function _generateSliderInfoMarkup(data) {
		// Label phrases.
		var sliderNameLabel = jse.core.lang.translate('NAME', 'sliders');
		var slidesQuantityLabel = jse.core.lang.translate('AMOUNT_OF_SLIDES', 'sliders');

		// Return markup.
		return '\n\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t<label class="col-md-5">' + sliderNameLabel + '</label>\n\t\t\t\t\t\t<div class="col-md-7">' + data.name + '</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t<label class="col-md-5">' + slidesQuantityLabel + '</label>\n\t\t\t\t\t\t<div class="col-md-7">' + data.slidesQuantity + '</div>\n\t\t\t\t\t</div>\n\t\t\t';
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Listen to form submit event.
		$this.on('click', deleteButtonSelector, _onDeleteClick);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNsaWRlcnMvb3ZlcnZpZXcvc2xpZGVyc19kZWxldGUuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkbW9kYWwiLCJkZWxldGVCdXR0b25TZWxlY3RvciIsInVybCIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJfb25EZWxldGVDbGljayIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCIkcGFyZW50VGFibGVSb3ciLCJ0YXJnZXQiLCJwYXJlbnRzIiwiJGNvbmZpcm1CdXR0b24iLCJmaW5kIiwicmVtb3ZlIiwic2xpZGVyRGF0YSIsImlkIiwibmFtZSIsInNsaWRlc1F1YW50aXR5IiwiaHRtbCIsIl9nZW5lcmF0ZVNsaWRlckluZm9NYXJrdXAiLCJtb2RhbCIsIm9mZiIsIm9uIiwiX29uQ29uZmlybUJ1dHRvbkNsaWNrIiwic2xpZGVySWQiLCJyZXF1ZXN0T3B0aW9ucyIsInR5cGUiLCJhamF4IiwiZG9uZSIsIl9oYW5kbGVEZWxldGVSZXF1ZXN0UmVzcG9uc2UiLCJyZXNwb25zZSIsImFsd2F5cyIsImVycm9yVGl0bGUiLCJsYW5nIiwidHJhbnNsYXRlIiwiZXJyb3JNZXNzYWdlIiwiJHRhYmxlQm9keSIsIiRyb3dzIiwiJHJvd1RvRGVsZXRlIiwiZmlsdGVyIiwiJGVtcHR5Um93VGVtcGxhdGUiLCJpbmNsdWRlcyIsImxpYnMiLCJpbmZvX2JveCIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwibGVuZ3RoIiwiZW1wdHkiLCJhcHBlbmQiLCJjbG9uZSIsInNob3dNZXNzYWdlIiwic2xpZGVyTmFtZUxhYmVsIiwic2xpZGVzUXVhbnRpdHlMYWJlbCIsImluaXQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7OztBQU9BQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxnQkFERCxFQUdDLENBQ0MsT0FERCxFQUVJRixHQUFHRyxNQUZQLG9CQUhELEVBUUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsU0FBU0QsRUFBRSxzQkFBRixDQUFmOztBQUVBOzs7OztBQUtBLEtBQU1FLHVCQUF1QixhQUE3Qjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxNQUFTQyxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQVQseURBQU47O0FBRUE7Ozs7O0FBS0EsS0FBTVgsU0FBUyxFQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxVQUFTWSxjQUFULENBQXdCQyxLQUF4QixFQUErQjtBQUM5QjtBQUNBQSxRQUFNQyxjQUFOOztBQUVBO0FBQ0EsTUFBTUMsa0JBQWtCWCxFQUFFUyxNQUFNRyxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixDQUF4Qjs7QUFFQTtBQUNBLE1BQU1DLGlCQUFpQmIsT0FBT2MsSUFBUCxDQUFZLGdCQUFaLENBQXZCOztBQUVBO0FBQ0FkLFNBQU9jLElBQVAsQ0FBWSx5QkFBWixFQUF1Q0MsTUFBdkM7O0FBRUE7QUFDQSxNQUFNQyxhQUFhO0FBQ2xCQyxPQUFJUCxnQkFBZ0JiLElBQWhCLENBQXFCLFVBQXJCLENBRGM7QUFFbEJxQixTQUFNUixnQkFBZ0JiLElBQWhCLENBQXFCLFlBQXJCLENBRlk7QUFHbEJzQixtQkFBZ0JULGdCQUFnQmIsSUFBaEIsQ0FBcUIsZ0JBQXJCO0FBSEUsR0FBbkI7O0FBTUE7QUFDQUcsU0FDRWMsSUFERixDQUNPLHNCQURQLEVBRUVNLElBRkYsQ0FFT0MsMEJBQTBCTCxVQUExQixDQUZQOztBQUlBO0FBQ0FoQixTQUFPc0IsS0FBUCxDQUFhLE1BQWI7O0FBRUE7QUFDQVQsaUJBQ0VVLEdBREYsQ0FDTSxPQUROLEVBRUVDLEVBRkYsQ0FFSyxPQUZMLEVBRWM7QUFBQSxVQUFNQyxzQkFBc0JULFdBQVdDLEVBQWpDLENBQU47QUFBQSxHQUZkO0FBR0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU1EscUJBQVQsQ0FBK0JDLFFBQS9CLEVBQXlDO0FBQ3hDO0FBQ0EsTUFBTUMsaUJBQWlCO0FBQ3RCQyxTQUFNLE1BRGdCO0FBRXRCL0IsU0FBTSxFQUFDNkIsa0JBQUQsRUFGZ0I7QUFHdEJ4QjtBQUhzQixHQUF2Qjs7QUFNQTtBQUNBSCxJQUFFOEIsSUFBRixDQUFPRixjQUFQLEVBQ0VHLElBREYsQ0FDTztBQUFBLFVBQVlDLDZCQUE2QkMsUUFBN0IsRUFBdUNOLFFBQXZDLENBQVo7QUFBQSxHQURQLEVBRUVPLE1BRkYsQ0FFUztBQUFBLFVBQU1qQyxPQUFPc0IsS0FBUCxDQUFhLE1BQWIsQ0FBTjtBQUFBLEdBRlQ7QUFHQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU1MsNEJBQVQsQ0FBc0NDLFFBQXRDLEVBQWdETixRQUFoRCxFQUEwRDtBQUN6RDtBQUNBLE1BQU1RLGFBQWEvQixJQUFJQyxJQUFKLENBQVMrQixJQUFULENBQWNDLFNBQWQsQ0FBd0IsMkJBQXhCLEVBQXFELFNBQXJELENBQW5CO0FBQ0EsTUFBTUMsZUFBZWxDLElBQUlDLElBQUosQ0FBUytCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwwQkFBeEIsRUFBb0QsU0FBcEQsQ0FBckI7O0FBRUE7QUFDQSxNQUFNRSxhQUFheEMsTUFBTWdCLElBQU4sQ0FBVyxPQUFYLENBQW5COztBQUVBO0FBQ0EsTUFBTXlCLFFBQVFELFdBQVd4QixJQUFYLG9CQUFkOztBQUVBO0FBQ0EsTUFBTTBCLGVBQWVELE1BQU1FLE1BQU4sdUJBQWlDZixRQUFqQyxRQUFyQjs7QUFFQTtBQUNBLE1BQU1nQixvQkFBb0IzQyxFQUFFLDJCQUFGLENBQTFCOztBQUVBO0FBQ0EsTUFBSWlDLFNBQVNXLFFBQVQsQ0FBa0IsU0FBbEIsQ0FBSixFQUFrQztBQUNqQztBQUNBSCxnQkFBYXpCLE1BQWI7O0FBRUE7QUFDQVosT0FBSXlDLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsaUJBQWxCOztBQUVBO0FBQ0EsT0FBS1AsTUFBTVEsTUFBTixHQUFlLENBQWhCLEdBQXFCLENBQXpCLEVBQTRCO0FBQzNCVCxlQUNFVSxLQURGLEdBRUVDLE1BRkYsQ0FFU1Asa0JBQWtCUSxLQUFsQixHQUEwQjlCLElBQTFCLEVBRlQ7QUFHQTtBQUNELEdBYkQsTUFhTztBQUNOO0FBQ0FqQixPQUFJeUMsSUFBSixDQUFTdEIsS0FBVCxDQUFlNkIsV0FBZixDQUEyQmpCLFVBQTNCLEVBQXVDRyxZQUF2QztBQUNBO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OztBQVNBLFVBQVNoQix5QkFBVCxDQUFtQ3hCLElBQW5DLEVBQXlDO0FBQ3hDO0FBQ0EsTUFBTXVELGtCQUFrQmpELElBQUlDLElBQUosQ0FBUytCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixNQUF4QixFQUFnQyxTQUFoQyxDQUF4QjtBQUNBLE1BQU1pQixzQkFBc0JsRCxJQUFJQyxJQUFKLENBQVMrQixJQUFULENBQWNDLFNBQWQsQ0FBd0Isa0JBQXhCLEVBQTRDLFNBQTVDLENBQTVCOztBQUVBO0FBQ0Esd0ZBRTZCZ0IsZUFGN0Isb0RBRzJCdkQsS0FBS3FCLElBSGhDLDRHQU82Qm1DLG1CQVA3QixvREFRMkJ4RCxLQUFLc0IsY0FSaEM7QUFXQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUF4QixRQUFPMkQsSUFBUCxHQUFjLGdCQUFRO0FBQ3JCO0FBQ0F4RCxRQUFNMEIsRUFBTixDQUFTLE9BQVQsRUFBa0J2QixvQkFBbEIsRUFBd0NNLGNBQXhDOztBQUVBO0FBQ0F1QjtBQUNBLEVBTkQ7O0FBUUEsUUFBT25DLE1BQVA7QUFDQSxDQXJNRiIsImZpbGUiOiJzbGlkZXJzL292ZXJ2aWV3L3NsaWRlcnNfZGVsZXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzbGlkZXJzX2RlbGV0ZS5qcyAyMDE2LTA5LTEyXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBTbGlkZXJzIE92ZXJ2aWV3IERlbGV0ZVxuICogXG4gKiBDb250cm9sbGVyIE1vZHVsZSBUbyBEZWxldGUgU2xpZGVycy5cbiAqXG4gKiBIYW5kbGVzIHRoZSBkZWxldGUgb3BlcmF0aW9uIG9mIHRoZSBzbGlkZXJzIG92ZXJ2aWV3IHBhZ2UuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3NsaWRlcnNfZGVsZXRlJyxcblx0XG5cdFtcblx0XHQnbW9kYWwnLFxuXHRcdGAke2d4LnNvdXJjZX0vbGlicy9pbmZvX2JveGBcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvciBcblx0XHQgKiBcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTbGlkZXIgZGVsZXRlIGNvbmZpcm1hdGlvbiBtb2RhbC5cblx0XHQgKiBcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5kZWxldGUtc2xpZGVyLm1vZGFsJyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVsZXRlIGJ1dHRvbiBzZWxlY3RvciBzdHJpbmcuXG5cdFx0ICogXG5cdFx0ICogQHR5cGUge1N0cmluZ31cblx0XHQgKi9cblx0XHRjb25zdCBkZWxldGVCdXR0b25TZWxlY3RvciA9ICcuYnRuLWRlbGV0ZSc7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVsZXRlIHNsaWRlciBhY3Rpb24gVVJMLlxuXHRcdCAqIFxuXHRcdCAqIEB0eXBlIHtTdHJpbmd9XG5cdFx0ICovXG5cdFx0Y29uc3QgdXJsID0gYCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2FkbWluL2FkbWluLnBocD9kbz1TbGlkZXJzT3ZlcnZpZXdBamF4L0RlbGV0ZVNsaWRlcmA7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlIFxuXHRcdCAqIFxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIGRlbGV0ZSBjbGljayBldmVudCBieSBvcGVuaW5nIHRoZSBkZWxldGUgY29uZmlybWF0aW9uIG1vZGFsLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IFRyaWdnZXJlZCBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25EZWxldGVDbGljayhldmVudCkge1xuXHRcdFx0Ly8gUHJldmVudCBkZWZhdWx0IGFjdGlvbi5cblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdC8vIFRhYmxlIHJvdy5cblx0XHRcdGNvbnN0ICRwYXJlbnRUYWJsZVJvdyA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpO1xuXHRcdFx0XG5cdFx0XHQvLyBEZWxldGUgY29uZmlybWF0aW9uIG1vZGFsIGJ1dHRvbi5cblx0XHRcdGNvbnN0ICRjb25maXJtQnV0dG9uID0gJG1vZGFsLmZpbmQoJ2J1dHRvbi5jb25maXJtJyk7XG5cdFx0XHRcblx0XHRcdC8vIEVtcHR5IG1vZGFsIGJvZHkuXG5cdFx0XHQkbW9kYWwuZmluZCgnLm1vZGFsLWJvZHkgLmZvcm0tZ3JvdXAnKS5yZW1vdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2xpZGVyIGRhdGEuXG5cdFx0XHRjb25zdCBzbGlkZXJEYXRhID0ge1xuXHRcdFx0XHRpZDogJHBhcmVudFRhYmxlUm93LmRhdGEoJ3NsaWRlcklkJyksXG5cdFx0XHRcdG5hbWU6ICRwYXJlbnRUYWJsZVJvdy5kYXRhKCdzbGlkZXJOYW1lJyksXG5cdFx0XHRcdHNsaWRlc1F1YW50aXR5OiAkcGFyZW50VGFibGVSb3cuZGF0YSgnc2xpZGVzUXVhbnRpdHknKVxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gUHV0IG5ldyBzbGlkZXIgaW5mb3JtYXRpb24gaW50byBtb2RhbCBib2R5LlxuXHRcdFx0JG1vZGFsXG5cdFx0XHRcdC5maW5kKCcubW9kYWwtYm9keSBmaWVsZHNldCcpXG5cdFx0XHRcdC5odG1sKF9nZW5lcmF0ZVNsaWRlckluZm9NYXJrdXAoc2xpZGVyRGF0YSkpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0JG1vZGFsLm1vZGFsKCdzaG93Jyk7XG5cdFx0XHRcblx0XHRcdC8vIEhhbmRsZSBkZWxldGUgY29uZmlybWF0aW9uIG1vZGFsIGJ1dHRvbiBjbGljayBldmVudC5cblx0XHRcdCRjb25maXJtQnV0dG9uXG5cdFx0XHRcdC5vZmYoJ2NsaWNrJylcblx0XHRcdFx0Lm9uKCdjbGljaycsICgpID0+IF9vbkNvbmZpcm1CdXR0b25DbGljayhzbGlkZXJEYXRhLmlkKSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIGRlbGV0ZSBjb25maXJtYXRpb24gYnV0dG9uIGNsaWNrIGV2ZW50IGJ5IHJlbW92aW5nIHRoZSBzbGlkZXIgdGhyb3VnaCBhbiBBSkFYIHJlcXVlc3QuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge051bWJlcn0gc2xpZGVySWQgU2xpZGVyIElELlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkNvbmZpcm1CdXR0b25DbGljayhzbGlkZXJJZCkge1xuXHRcdFx0Ly8gQUpBWCByZXF1ZXN0IG9wdGlvbnMuXG5cdFx0XHRjb25zdCByZXF1ZXN0T3B0aW9ucyA9IHtcblx0XHRcdFx0dHlwZTogJ1BPU1QnLFxuXHRcdFx0XHRkYXRhOiB7c2xpZGVySWR9LFxuXHRcdFx0XHR1cmxcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIFBlcmZvcm0gcmVxdWVzdC5cblx0XHRcdCQuYWpheChyZXF1ZXN0T3B0aW9ucylcblx0XHRcdFx0LmRvbmUocmVzcG9uc2UgPT4gX2hhbmRsZURlbGV0ZVJlcXVlc3RSZXNwb25zZShyZXNwb25zZSwgc2xpZGVySWQpKVxuXHRcdFx0XHQuYWx3YXlzKCgpID0+ICRtb2RhbC5tb2RhbCgnaGlkZScpKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyBzbGlkZXIgZGVsZXRpb24gQUpBWCBhY3Rpb24gc2VydmVyIHJlc3BvbnNlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHJlc3BvbnNlIFNlcnZlciByZXNwb25zZS5cblx0XHQgKiBAcGFyYW0ge051bWJlcn0gc2xpZGVySWQgSUQgb2YgZGVsZXRlZCBzbGlkZXIuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2hhbmRsZURlbGV0ZVJlcXVlc3RSZXNwb25zZShyZXNwb25zZSwgc2xpZGVySWQpIHtcblx0XHRcdC8vIEVycm9yIG1lc3NhZ2UgcGhyYXNlcy5cblx0XHRcdGNvbnN0IGVycm9yVGl0bGUgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnREVMRVRFX1NMSURFUl9FUlJPUl9USVRMRScsICdzbGlkZXJzJyk7XG5cdFx0XHRjb25zdCBlcnJvck1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnREVMRVRFX1NMSURFUl9FUlJPUl9URVhUJywgJ3NsaWRlcnMnKTtcblx0XHRcdFxuXHRcdFx0Ly8gVGFibGUgYm9keS5cblx0XHRcdGNvbnN0ICR0YWJsZUJvZHkgPSAkdGhpcy5maW5kKCd0Ym9keScpO1xuXHRcdFx0XG5cdFx0XHQvLyBUYWJsZSByb3dzLlxuXHRcdFx0Y29uc3QgJHJvd3MgPSAkdGFibGVCb2R5LmZpbmQoYFtkYXRhLXNsaWRlci1pZF1gKTtcblx0XHRcdFxuXHRcdFx0Ly8gVGFibGUgcm93cyB0aGF0IHdpbGwgYmUgZGVsZXRlZC5cblx0XHRcdGNvbnN0ICRyb3dUb0RlbGV0ZSA9ICRyb3dzLmZpbHRlcihgW2RhdGEtc2xpZGVyLWlkPVwiJHtzbGlkZXJJZH1cIl1gKTtcblx0XHRcdFxuXHRcdFx0Ly8gJ05vIHJlc3VsdHMnIG1lc3NhZ2UgdGFibGUgcm93IHRlbXBsYXRlLlxuXHRcdFx0Y29uc3QgJGVtcHR5Um93VGVtcGxhdGUgPSAkKCcjdGVtcGxhdGUtdGFibGUtcm93LWVtcHR5Jyk7XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrIGZvciBhY3Rpb24gc3VjY2Vzcy5cblx0XHRcdGlmIChyZXNwb25zZS5pbmNsdWRlcygnc3VjY2VzcycpKSB7XG5cdFx0XHRcdC8vIERlbGV0ZSByZXNwZWN0aXZlIHRhYmxlIHJvd3MuXG5cdFx0XHRcdCRyb3dUb0RlbGV0ZS5yZW1vdmUoKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFkZCBzdWNjZXNzIG1lc3NhZ2UgdG8gYWRtaW4gaW5mbyBib3guXG5cdFx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBJZiB0aGVyZSBhcmUgbm8gcm93cywgc2hvdyAnTm8gcmVzdWx0cycgbWVzc2FnZSByb3cuXG5cdFx0XHRcdGlmICgoJHJvd3MubGVuZ3RoIC0gMSkgPCAxKSB7XG5cdFx0XHRcdFx0JHRhYmxlQm9keVxuXHRcdFx0XHRcdFx0LmVtcHR5KClcblx0XHRcdFx0XHRcdC5hcHBlbmQoJGVtcHR5Um93VGVtcGxhdGUuY2xvbmUoKS5odG1sKCkpO1xuXHRcdFx0XHR9XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQvLyBTaG93IGVycm9yIG1lc3NhZ2UgbW9kYWwuXG5cdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKGVycm9yVGl0bGUsIGVycm9yTWVzc2FnZSk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdlbmVyYXRlcyBIVE1MIGNvbnRhaW5pbmcgdGhlIHNsaWRlciBpbmZvcm1hdGlvbiBmb3IgdGhlIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YSBTbGlkZXIgZGF0YS5cblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YS5uYW1lIE5hbWUgb2YgdGhlIHNsaWRlci5cblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YS5zbGlkZXNRdWFudGl0eSBRdWFudGl0eSBvZiBzbGlkZXMgZnJvbSBzbGlkZXIuXG5cdFx0ICogXG5cdFx0ICogQHJldHVybiB7U3RyaW5nfSBDcmVhdGVkIEhUTUwgc3RyaW5nLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZW5lcmF0ZVNsaWRlckluZm9NYXJrdXAoZGF0YSkge1xuXHRcdFx0Ly8gTGFiZWwgcGhyYXNlcy5cblx0XHRcdGNvbnN0IHNsaWRlck5hbWVMYWJlbCA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdOQU1FJywgJ3NsaWRlcnMnKTtcblx0XHRcdGNvbnN0IHNsaWRlc1F1YW50aXR5TGFiZWwgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQU1PVU5UX09GX1NMSURFUycsICdzbGlkZXJzJyk7XG5cdFx0XHRcblx0XHRcdC8vIFJldHVybiBtYXJrdXAuXG5cdFx0XHRyZXR1cm4gYFxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtNVwiPiR7c2xpZGVyTmFtZUxhYmVsfTwvbGFiZWw+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTdcIj4ke2RhdGEubmFtZX08L2Rpdj5cblx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtNVwiPiR7c2xpZGVzUXVhbnRpdHlMYWJlbH08L2xhYmVsPlxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC03XCI+JHtkYXRhLnNsaWRlc1F1YW50aXR5fTwvZGl2PlxuXHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0YDtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdC8vIExpc3RlbiB0byBmb3JtIHN1Ym1pdCBldmVudC5cblx0XHRcdCR0aGlzLm9uKCdjbGljaycsIGRlbGV0ZUJ1dHRvblNlbGVjdG9yLCBfb25EZWxldGVDbGljayk7XG5cdFx0XHRcblx0XHRcdC8vIEZpbmlzaCBpbml0aWFsaXphdGlvbi5cblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7XG4iXX0=
