'use strict';

/* --------------------------------------------------------------
 info_box.js 2016-11-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Infobox Controller
 *
 * Handles the functionality of the info box component of the admin layout pages.
 */
gx.controllers.module('info_box', ['loading_spinner', gx.source + '/libs/info_box'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	// Module element, which represents the info box.

	var $this = $(this);

	// Notification counter icon element.
	var $counter = $this.find('.notification-count');

	// Module default parameters.
	var defaults = {
		// Popover steady time.
		closeDelay: 5000,

		// Roll on/out animation duration.
		animationDuration: 600,

		// Default open mode on links.
		linkOpenMode: '_self'
	};

	// Module options.
	var options = $.extend(true, {}, defaults, data);

	// Shortcut to info box library.
	var library = jse.libs.info_box;

	// Shortcut to spinner library.
	var spinner = jse.libs.loading_spinner;

	// Shortcut to language text service.
	var translator = jse.core.lang;

	// CSS classes.
	var classes = {
		// Active state of the message list container (for the roll in/out animation).
		containerVisible: 'visible',

		// Hidden message item.
		hidden: 'hidden'
	};

	// Selector strings.
	var selectors = {
		// Message list container.
		container: '.popover.message-list-container',

		// Message list container arrow.
		arrow: '.arrow',

		// Message list.
		list: '.popover-content',

		// Message list visibility checkbox.
		checkbox: '.visibility-checkbox',

		// Message item.
		item: '.message',

		// Hidden message item.
		hiddenItem: '[data-status="hidden"]',

		// Message item button.
		itemButton: '.message-button',

		// Message item action (remove, hide, etc.).
		itemAction: '.action-icon'
	};

	// Module object.
	var module = {};

	// Indicates whether the roll animation is still running.
	var isAnimating = false;

	// Indicates whether the message item list container is shown for a certain duration
	// Happens on new messages after a silent refresh.
	var isShownWithCloseDelay = false;

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Returns a space-separated CSS class name by replacing the periods with spaces.
  *
  * @param className CSS class.
  * 
  * @return {String} Class name without periods.
  */
	function _getClassNameWithoutPeriods(className) {
		return className.split('.').join(' ');
	}

	/**
  * Returns the markup for the popover template.
  *
  * @return {String} Generated HTML string.
  */
	function _getPopoverMarkup() {
		return '\n\t\t\t\t<div class="' + _getClassNameWithoutPeriods(selectors.container) + '" role="tooltip">\n\t\t\t\t\t<div class="' + _getClassNameWithoutPeriods(selectors.arrow) + '"></div>\n\t\t\t\t\t<div class="popover-title"></div>\n\t\t\t\t\t<div class="' + _getClassNameWithoutPeriods(selectors.list) + '"></div>\n\t\t\t\t</div>\n\t\t\t';
	}

	/**
  * Returns the generated markup for a message item.
  *
  * @param {Object} message Message item.
  * 
  * @return {jQuery} Markup as jQuery object.
  */
	function _getMessageItemMarkup(message) {
		// Is the message an admin action success message?
		var isSuccessMessage = message.identifier && message.identifier.includes(library.SUCCESS_MSG_IDENTIFIER_PREFIX);

		// Is the message hideable but not removable?
		var isHideableMessage = !isSuccessMessage && message.visibility && message.visibility === library.VISIBILITY_HIDEABLE;

		// Is the message always visible?
		var isAlwaysOnMessage = !isSuccessMessage && message.visibility && message.visibility === library.VISIBILITY_ALWAYS_ON;

		// Message item markup.
		var markup = '\n\t\t\t\t<div\n\t\t\t\t\tclass="message ' + message.type + ' ' + (isSuccessMessage ? 'admin-action-success' : '') + '"\n\t\t\t\t\tdata-status="' + message.status + '"\n\t\t\t\t\tdata-id="' + message.id + '"\n\t\t\t\t\tdata-visibility="' + message.visibility + '"\n\t\t\t\t\tdata-identifier="' + message.identifier + '"\n\t\t\t\t\t>\n\t\t\t\t\t<div class="action-icons">\n\t\t\t\t\t\t<span class="' + _getClassNameWithoutPeriods(selectors.itemAction) + ' do-hide fa fa-minus"></span>\n\t\t\t\t\t\t<span class="' + _getClassNameWithoutPeriods(selectors.itemAction) + ' do-remove fa fa-times"></span>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class="headline">\n\t\t\t\t\t\t' + message.headline + '\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class="content">\n\t\t\t\t\t\t' + message.message + '\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<a class="btn ' + _getClassNameWithoutPeriods(selectors.itemButton) + '" href="' + message.buttonLink + '">\n\t\t\t\t\t\t' + message.buttonLabel + '\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t';

		// Markup as jQuery object for manipulation purposes.
		var $markup = $(markup);

		// Remove button from markup, if no button label is defined.
		if (!message.buttonLabel) {
			$markup.find(selectors.itemButton).remove();
		}

		// Show remove/hide button, depending on the visibility value and kind of message.
		if (isAlwaysOnMessage) {
			$markup.find('' + selectors.itemAction).remove();
		} else if (isHideableMessage) {
			$markup.find(selectors.itemAction + ' do-remove').remove();
		} else if (isSuccessMessage) {
			$markup.find(selectors.itemAction + ' do-hide').remove();
		}

		return $markup;
	}

	/**
  * Returns the markup for the message items visibility checkbox.
  *
  * @return {String} Generated HTML string.
  */
	function _getVisibilityCheckboxMarkup() {
		return '\n\t\t\t\t<div class="visibility-checkbox-container">\n\t\t\t\t\t<input type="checkbox" class="' + _getClassNameWithoutPeriods(selectors.checkbox) + '">\n\t\t\t\t\t<label class="visibility-checkbox-label">' + translator.translate('SHOW_ALL', 'admin_info_boxes') + '</label>\n\t\t\t\t</div>\n\t\t\t';
	}

	/**
  * Marks each visible message item as read.
  */
	function _markMessageItemsAsRead() {
		// Message items.
		var $messageList = $(selectors.container).find(selectors.item);

		// Iterate over each message and mark as read.
		$messageList.each(function (index, element) {
			// Current iteration element.
			var $message = $(element);

			// Message data.
			var data = $message.data();

			// Indicate, if the message is declared as hidden.
			var isHidden = $message.hasClass(library.STATUS_HIDDEN);

			// Delete by ID if existent.
			if (!isHidden && data.id) {
				library.setStatus(data.id, library.STATUS_READ);
				$message.data('status', library.STATUS_READ);
			}

			// Delete success messages.
			if (data.identifier && data.identifier.includes(library.SUCCESS_MSG_IDENTIFIER_PREFIX)) {
				library.deleteByIdentifier(data.identifier);
			}
		});
	}

	/**
  * Sets the message item amount in the notification icon.
  * 
  * Admin action success messages will be excluded.
  *
  * @param {Array} messages Message items.
  */
	function _setMessageCounter(messages) {
		// Hidden CSS class.
		var hiddenClass = 'hidden';

		// Message item count.
		var count = 0;

		// Iterate over each message item and check message identifier.
		if (messages.length) {
			messages.forEach(function (message) {
				return count = message.identifier.includes(library.SUCCESS_MSG_IDENTIFIER_PREFIX) ? count : ++count;
			});
		}

		// The notification count will be hidden, if there are no messages.
		if (count) {
			$counter.removeClass(hiddenClass).text(count);
		} else {
			$counter.addClass(hiddenClass);
		}
	}

	/**
  * Handles the click event on the info box action button.
  */
	function _onClick() {
		_toggleContainer(!$(selectors.container).length);
	}

	/**
  * Toggles the message list container (popover) with an animation.
  *
  * @param {Boolean} doShow Show the message list container?
  */
	function _toggleContainer(doShow) {
		// Exit immediately when the animation process is still running.
		if (isAnimating) {
			return;
		}

		// Indicate animation process.
		isAnimating = true;

		// Switch animation process indicator to false after animation duration.
		setTimeout(function () {
			return isAnimating = false;
		}, options.animationDuration);

		if (doShow) {
			// Perform message item list container roll in animation.
			$this.popover('show');
		} else {
			// Perform message item list container roll out animation and then hide.
			$(selectors.container).removeClass(classes.containerVisible);
			setTimeout(function () {
				return $this.popover('hide');
			}, options.animationDuration);
		}
	}

	/**
  * Handles the popover (message container) 'shown' event by getting the messages from the server 
  * and displaying them in the container.
  */
	function _onPopoverShown() {
		// Message list container.
		var $container = $(selectors.container);

		// Indicate container visibility.
		$container.addClass(classes.containerVisible).css({ top: $container.height() * -1 });

		// Fix container and arrow position.
		_fixPositions();

		// Enable loading state.
		_toggleLoading(true);

		// Retrieve messages from server and show them in the container and mark them as read.
		library.getMessages().then(function (messages) {
			// Disable loading state.
			_toggleLoading(false);

			// Put messages into message list.
			_putMessagesIntoContainer(messages);

			// Hide hidden message items and show only the success message item if exists.
			_reviseMessageItemList();

			// Set notification count.
			_setMessageCounter(messages);

			// Mark visible message items as read.
			_markMessageItemsAsRead();
		});

		// Attach event handlers to popover.
		$container.off('click change').on('click', selectors.itemButton, _onMessageItemButtonClick).on('click', selectors.itemAction, _onMessageItemActionClick).on('change', selectors.checkbox, _onVisibilityCheckboxChange);
	}

	/**
  * Toggles the hidden message items.
  *
  * @param {Boolean} doShow Show the hidden message items?
  */
	function _toggleHiddenMessageItems(doShow) {
		// Message list container.
		var $container = $(selectors.container);

		// Hidden message items.
		var $hiddenMessageItems = $container.find(selectors.item).filter(selectors.hiddenItem);

		// Toggle visibility.
		if (doShow) {
			$hiddenMessageItems.removeClass(classes.hidden);
		} else {
			$hiddenMessageItems.addClass(classes.hidden);
		}
	}

	/**
  * Revises the message item list by hiding message items declared as hidden.
  * 
  * Additionally, if an admin action success message item is found it will be solely displayed.
  */
	function _reviseMessageItemList() {
		// Message list container.
		var $container = $(selectors.container);

		// Message list.
		var $messageList = $container.find(selectors.list);

		// Message items.
		var $messageItems = $messageList.find(selectors.item);

		// Hidden message items.
		var $hiddenMessageItems = $messageItems.filter(selectors.hiddenItem);

		// Admin action success message items.
		var $successMessageItems = $messageItems.filter('[data-identifier*=' + library.SUCCESS_MSG_IDENTIFIER_PREFIX + ']');

		// Hide messages declared as hidden and add visibility checkbox.
		if ($hiddenMessageItems.length) {
			_toggleHiddenMessageItems(false);
			$messageList.append(_getVisibilityCheckboxMarkup());
		}

		// Remove all other messages (including duplicate success messages) if a success message is present.
		if ($successMessageItems.length) {
			$messageItems.not($successMessageItems.first()).hide();
		}
	}

	/**
  * Fills the message list with message items.
  *
  * @param {Array} messages Message items.
  */
	function _putMessagesIntoContainer(messages) {
		// Message list container.
		var $container = $(selectors.container);

		// Message list.
		var $messageList = $container.find(selectors.list);

		// Array containing all generated message item markups.
		var messageItemsMarkups = [];

		// Clear message list.
		$messageList.empty();

		// Show messages.
		if (messages.length) {
			// Iterate over each message item and generate markups.
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = messages[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var message = _step.value;

					// Generate markup.
					var $markup = _getMessageItemMarkup(message);

					// Add markup to array.
					messageItemsMarkups.push($markup);
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		} else {
			// Generate markup for the missing entries info message item.
			var $noEntriesMessageItemMarkup = _getMessageItemMarkup({
				message: translator.translate('NO_MESSAGES', 'admin_info_boxes'),
				visibility: library.VISIBILITY_ALWAYS_ON,
				status: library.STATUS_READ,
				headline: '',
				type: '',
				id: '',
				identifier: ''
			});

			// Add markup to array.
			messageItemsMarkups.push($noEntriesMessageItemMarkup);
		}

		// Put render messages.
		messageItemsMarkups.forEach(function (element) {
			return $messageList.append(element);
		});

		// Fade the message items in smoothly.
		$messageList.children().each(function (index, element) {
			return $(element).hide().fadeIn();
		});
	}

	/**
  * Handles the click event to a message item button by opening the link.
  */
	function _onMessageItemButtonClick(event) {
		// Prevent default behavior.
		event.preventDefault();
		event.stopPropagation();

		// Link value from button.
		var href = $(this).attr('href');

		// Check if we need to perform a special action. 
		var $message = $(this).parents('.message');

		switch ($message.data('identifier')) {
			case 'clear_cache':
				$.get(href).done(function (response) {
					library.addSuccessMessage(response);
				});
				break;
			default:
				// Open link if exists.
				if (href && href.trim().length) {
					window.open(href, options.linkOpenMode);
				}
		}
	}

	/**
  * Handles the click event to a message item action.
  */
	function _onMessageItemActionClick() {
		// Clicked element.
		var $element = $(this);

		// Check if the clicked target indicates a message removal.
		var doRemove = $element.hasClass('do-remove');

		// ID of the message taken from the message item element.
		var id = $element.parents(selectors.item).data('id');

		// Delete/hide message depending on the clicked target.
		if (doRemove) {
			_deleteMessage(id);
		} else {
			_hideMessage(id);
		}
	}

	/**
  * Handles click event on the entire document.
  *
  * @param {jQuery.Event} event Triggered event.
  */
	function _onWindowClick(event) {
		// Clicked target.
		var $target = $(event.target);

		// Message list item container.
		var $container = $(selectors.container);

		// Conditions.
		var isClickedOnButton = $this.has($target).length || $this.is($target).length;
		var isContainerShown = $container.length;
		var isClickedOutsideOfPopover = !$container.has($target).length;

		// Only hide container, if clicked target is not within the container area.
		if (isClickedOutsideOfPopover && isContainerShown && !isClickedOnButton && !isShownWithCloseDelay) {
			_toggleContainer(false);
		}
	}

	/**
  * Fixes the container and arrow positions.
  */
	function _fixPositions() {
		// Offset correction values.
		var ARROW_OFFSET = 240;
		var POPOVER_OFFSET = 250;

		// Message list container (popover).
		var $container = $(selectors.container);

		// Arrow.
		var $arrow = $container.find(selectors.arrow);

		// Fix the offset for the affected elements, if popover is open.
		if ($container.length) {
			var arrowOffset = $container.offset().left + ARROW_OFFSET;
			var popoverOffset = $this.offset().left - POPOVER_OFFSET + $this.width() / 2;

			$arrow.offset({ left: arrowOffset });
			$container.offset({ left: popoverOffset });
		}
	}

	/**
  * Handles the visibility checkbox change event.
  */
	function _onVisibilityCheckboxChange() {
		// Indicates whether the checkbox is checked.
		var isCheckboxChecked = $(this).is(':checked');

		// Toggle hidden messages and mark shown messages as read.
		_toggleHiddenMessageItems(isCheckboxChecked);
		_markMessageItemsAsRead();
	}

	/**
  * Deletes a message item and refreshes the message item list.
  *
  * @param {Number} id Message ID.
  */
	function _deleteMessage(id) {
		library.deleteById(id).then(_refresh);
	}

	/**
  * Hides a message item and refreshes the message item list.
  *
  * @param {Number} id Message ID.
  */
	function _hideMessage(id) {
		library.setStatus(id, library.STATUS_HIDDEN).then(_refresh);
	}

	/**
  * Refreshes the message item list.
  */
	function _refresh() {
		// Message list container.
		var $container = $(selectors.container);

		// Show loading spinner on the message container.
		var $spinner = spinner.show($container, 9999);

		// Refresh list.
		_onPopoverShown();

		// Hide loading spinner.
		spinner.hide($spinner);
	}

	/**
  * Retrieves the messages and displays the popover if there are new messages.
  */
	function _refreshSilently() {
		// Retrieve messages from server and update the notification count.
		// Popover will we displayed if there is a new message.
		library.getMessages().then(function (messages) {
			// Set notification count.
			_setMessageCounter(messages);

			// Iterate over messages and try to find a message item declared as new.
			// If found, the container will be opened.
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = messages[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var message = _step2.value;

					if (message.status === library.STATUS_NEW) {
						// Exit immediately when the animation process is still running
						// or if the message list item container is already shown with a defined duration.
						if (isAnimating || isShownWithCloseDelay) {
							return;
						}

						// Open message item list container.
						_toggleContainer(true);

						// Indicate delayed closing.
						isShownWithCloseDelay = true;

						// Hide container and indicate delayed closing finish.
						setTimeout(function () {
							_toggleContainer(false);
							isShownWithCloseDelay = false;
						}, options.closeDelay);

						break;
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		});
	}

	/**
  * Toggles the loading state.
  *
  * @param {Boolean} isLoading Is the load process running?
  */
	function _toggleLoading(isLoading) {
		// Loading message class.
		var loadingMessageClass = '.loading';

		// Message item list container.
		var $messageList = $(selectors.container).find(selectors.list);

		// Loading message element.
		var $loadingMessage = $messageList.find(loadingMessageClass);

		// Loading message markup.
		var markup = '\n\t\t\t\t<div class="' + _getClassNameWithoutPeriods(loadingMessageClass) + '">\n\t\t\t\t\t' + translator.translate('LOADING', 'admin_info_boxes') + '\n\t\t\t\t</div>\n\t\t\t';

		// Remove existing loading message.
		$loadingMessage.remove();

		// Add new loading message if parameter is true.
		if (isLoading) {
			$messageList.append($(markup));
		}
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	// Module initialize function.
	module.init = function (done) {
		// Initialize popover plugin and attach event handlers.
		$this.popover({
			animation: false,
			placement: 'bottom',
			content: ' ',
			trigger: 'manual',
			template: _getPopoverMarkup()
		}).on('click', _onClick).on('shown.bs.popover', _onPopoverShown).on('show:popover', function () {
			return _toggleContainer(true);
		}).on('refresh:messages', _refreshSilently);

		// Attach event listeners to the window.
		$(window).on('resize', _fixPositions).on('click', _onWindowClick);

		// Initial message check.
		_refreshSilently();

		// Finish initialization.
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxheW91dHMvbWFpbi9oZWFkZXIvaW5mb19ib3guanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkY291bnRlciIsImZpbmQiLCJkZWZhdWx0cyIsImNsb3NlRGVsYXkiLCJhbmltYXRpb25EdXJhdGlvbiIsImxpbmtPcGVuTW9kZSIsIm9wdGlvbnMiLCJleHRlbmQiLCJsaWJyYXJ5IiwianNlIiwibGlicyIsImluZm9fYm94Iiwic3Bpbm5lciIsImxvYWRpbmdfc3Bpbm5lciIsInRyYW5zbGF0b3IiLCJjb3JlIiwibGFuZyIsImNsYXNzZXMiLCJjb250YWluZXJWaXNpYmxlIiwiaGlkZGVuIiwic2VsZWN0b3JzIiwiY29udGFpbmVyIiwiYXJyb3ciLCJsaXN0IiwiY2hlY2tib3giLCJpdGVtIiwiaGlkZGVuSXRlbSIsIml0ZW1CdXR0b24iLCJpdGVtQWN0aW9uIiwiaXNBbmltYXRpbmciLCJpc1Nob3duV2l0aENsb3NlRGVsYXkiLCJfZ2V0Q2xhc3NOYW1lV2l0aG91dFBlcmlvZHMiLCJjbGFzc05hbWUiLCJzcGxpdCIsImpvaW4iLCJfZ2V0UG9wb3Zlck1hcmt1cCIsIl9nZXRNZXNzYWdlSXRlbU1hcmt1cCIsIm1lc3NhZ2UiLCJpc1N1Y2Nlc3NNZXNzYWdlIiwiaWRlbnRpZmllciIsImluY2x1ZGVzIiwiU1VDQ0VTU19NU0dfSURFTlRJRklFUl9QUkVGSVgiLCJpc0hpZGVhYmxlTWVzc2FnZSIsInZpc2liaWxpdHkiLCJWSVNJQklMSVRZX0hJREVBQkxFIiwiaXNBbHdheXNPbk1lc3NhZ2UiLCJWSVNJQklMSVRZX0FMV0FZU19PTiIsIm1hcmt1cCIsInR5cGUiLCJzdGF0dXMiLCJpZCIsImhlYWRsaW5lIiwiYnV0dG9uTGluayIsImJ1dHRvbkxhYmVsIiwiJG1hcmt1cCIsInJlbW92ZSIsIl9nZXRWaXNpYmlsaXR5Q2hlY2tib3hNYXJrdXAiLCJ0cmFuc2xhdGUiLCJfbWFya01lc3NhZ2VJdGVtc0FzUmVhZCIsIiRtZXNzYWdlTGlzdCIsImVhY2giLCJpbmRleCIsImVsZW1lbnQiLCIkbWVzc2FnZSIsImlzSGlkZGVuIiwiaGFzQ2xhc3MiLCJTVEFUVVNfSElEREVOIiwic2V0U3RhdHVzIiwiU1RBVFVTX1JFQUQiLCJkZWxldGVCeUlkZW50aWZpZXIiLCJfc2V0TWVzc2FnZUNvdW50ZXIiLCJtZXNzYWdlcyIsImhpZGRlbkNsYXNzIiwiY291bnQiLCJsZW5ndGgiLCJmb3JFYWNoIiwicmVtb3ZlQ2xhc3MiLCJ0ZXh0IiwiYWRkQ2xhc3MiLCJfb25DbGljayIsIl90b2dnbGVDb250YWluZXIiLCJkb1Nob3ciLCJzZXRUaW1lb3V0IiwicG9wb3ZlciIsIl9vblBvcG92ZXJTaG93biIsIiRjb250YWluZXIiLCJjc3MiLCJ0b3AiLCJoZWlnaHQiLCJfZml4UG9zaXRpb25zIiwiX3RvZ2dsZUxvYWRpbmciLCJnZXRNZXNzYWdlcyIsInRoZW4iLCJfcHV0TWVzc2FnZXNJbnRvQ29udGFpbmVyIiwiX3JldmlzZU1lc3NhZ2VJdGVtTGlzdCIsIm9mZiIsIm9uIiwiX29uTWVzc2FnZUl0ZW1CdXR0b25DbGljayIsIl9vbk1lc3NhZ2VJdGVtQWN0aW9uQ2xpY2siLCJfb25WaXNpYmlsaXR5Q2hlY2tib3hDaGFuZ2UiLCJfdG9nZ2xlSGlkZGVuTWVzc2FnZUl0ZW1zIiwiJGhpZGRlbk1lc3NhZ2VJdGVtcyIsImZpbHRlciIsIiRtZXNzYWdlSXRlbXMiLCIkc3VjY2Vzc01lc3NhZ2VJdGVtcyIsImFwcGVuZCIsIm5vdCIsImZpcnN0IiwiaGlkZSIsIm1lc3NhZ2VJdGVtc01hcmt1cHMiLCJlbXB0eSIsInB1c2giLCIkbm9FbnRyaWVzTWVzc2FnZUl0ZW1NYXJrdXAiLCJjaGlsZHJlbiIsImZhZGVJbiIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJocmVmIiwiYXR0ciIsInBhcmVudHMiLCJnZXQiLCJkb25lIiwiYWRkU3VjY2Vzc01lc3NhZ2UiLCJyZXNwb25zZSIsInRyaW0iLCJ3aW5kb3ciLCJvcGVuIiwiJGVsZW1lbnQiLCJkb1JlbW92ZSIsIl9kZWxldGVNZXNzYWdlIiwiX2hpZGVNZXNzYWdlIiwiX29uV2luZG93Q2xpY2siLCIkdGFyZ2V0IiwidGFyZ2V0IiwiaXNDbGlja2VkT25CdXR0b24iLCJoYXMiLCJpcyIsImlzQ29udGFpbmVyU2hvd24iLCJpc0NsaWNrZWRPdXRzaWRlT2ZQb3BvdmVyIiwiQVJST1dfT0ZGU0VUIiwiUE9QT1ZFUl9PRkZTRVQiLCIkYXJyb3ciLCJhcnJvd09mZnNldCIsIm9mZnNldCIsImxlZnQiLCJwb3BvdmVyT2Zmc2V0Iiwid2lkdGgiLCJpc0NoZWNrYm94Q2hlY2tlZCIsImRlbGV0ZUJ5SWQiLCJfcmVmcmVzaCIsIiRzcGlubmVyIiwic2hvdyIsIl9yZWZyZXNoU2lsZW50bHkiLCJTVEFUVVNfTkVXIiwiaXNMb2FkaW5nIiwibG9hZGluZ01lc3NhZ2VDbGFzcyIsIiRsb2FkaW5nTWVzc2FnZSIsImluaXQiLCJhbmltYXRpb24iLCJwbGFjZW1lbnQiLCJjb250ZW50IiwidHJpZ2dlciIsInRlbXBsYXRlIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFVBREQsRUFHQyxDQUNDLGlCQURELEVBRUlGLEdBQUdHLE1BRlAsb0JBSEQsRUFRQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUNBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBO0FBQ0EsS0FBTUMsV0FBV0YsTUFBTUcsSUFBTixDQUFXLHFCQUFYLENBQWpCOztBQUVBO0FBQ0EsS0FBTUMsV0FBVztBQUNoQjtBQUNBQyxjQUFZLElBRkk7O0FBSWhCO0FBQ0FDLHFCQUFtQixHQUxIOztBQU9oQjtBQUNBQyxnQkFBYztBQVJFLEVBQWpCOztBQVdBO0FBQ0EsS0FBTUMsVUFBVVAsRUFBRVEsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CTCxRQUFuQixFQUE2QkwsSUFBN0IsQ0FBaEI7O0FBRUE7QUFDQSxLQUFNVyxVQUFVQyxJQUFJQyxJQUFKLENBQVNDLFFBQXpCOztBQUVBO0FBQ0EsS0FBTUMsVUFBVUgsSUFBSUMsSUFBSixDQUFTRyxlQUF6Qjs7QUFFQTtBQUNBLEtBQU1DLGFBQWFMLElBQUlNLElBQUosQ0FBU0MsSUFBNUI7O0FBRUE7QUFDQSxLQUFNQyxVQUFVO0FBQ2Y7QUFDQUMsb0JBQWtCLFNBRkg7O0FBSWY7QUFDQUMsVUFBUTtBQUxPLEVBQWhCOztBQVFBO0FBQ0EsS0FBTUMsWUFBWTtBQUNqQjtBQUNBQyxhQUFXLGlDQUZNOztBQUlqQjtBQUNBQyxTQUFPLFFBTFU7O0FBT2pCO0FBQ0FDLFFBQU0sa0JBUlc7O0FBVWpCO0FBQ0FDLFlBQVUsc0JBWE87O0FBYWpCO0FBQ0FDLFFBQU0sVUFkVzs7QUFnQmpCO0FBQ0FDLGNBQVksd0JBakJLOztBQW1CakI7QUFDQUMsY0FBWSxpQkFwQks7O0FBc0JqQjtBQUNBQyxjQUFZO0FBdkJLLEVBQWxCOztBQTBCQTtBQUNBLEtBQU1qQyxTQUFTLEVBQWY7O0FBRUE7QUFDQSxLQUFJa0MsY0FBYyxLQUFsQjs7QUFFQTtBQUNBO0FBQ0EsS0FBSUMsd0JBQXdCLEtBQTVCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVNDLDJCQUFULENBQXFDQyxTQUFyQyxFQUFnRDtBQUMvQyxTQUFPQSxVQUFVQyxLQUFWLENBQWdCLEdBQWhCLEVBQXFCQyxJQUFyQixDQUEwQixHQUExQixDQUFQO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0MsaUJBQVQsR0FBNkI7QUFDNUIsb0NBQ2VKLDRCQUE0QlgsVUFBVUMsU0FBdEMsQ0FEZixpREFFZ0JVLDRCQUE0QlgsVUFBVUUsS0FBdEMsQ0FGaEIscUZBSWdCUyw0QkFBNEJYLFVBQVVHLElBQXRDLENBSmhCO0FBT0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTYSxxQkFBVCxDQUErQkMsT0FBL0IsRUFBd0M7QUFDdkM7QUFDQSxNQUFNQyxtQkFBbUJELFFBQVFFLFVBQVIsSUFDckJGLFFBQVFFLFVBQVIsQ0FBbUJDLFFBQW5CLENBQTRCaEMsUUFBUWlDLDZCQUFwQyxDQURKOztBQUdBO0FBQ0EsTUFBTUMsb0JBQW9CLENBQUNKLGdCQUFELElBQ3RCRCxRQUFRTSxVQURjLElBQ0FOLFFBQVFNLFVBQVIsS0FBdUJuQyxRQUFRb0MsbUJBRHpEOztBQUdBO0FBQ0EsTUFBTUMsb0JBQW9CLENBQUNQLGdCQUFELElBQ3RCRCxRQUFRTSxVQURjLElBQ0FOLFFBQVFNLFVBQVIsS0FBdUJuQyxRQUFRc0Msb0JBRHpEOztBQUdBO0FBQ0EsTUFBTUMsdURBRWFWLFFBQVFXLElBRnJCLFVBRTZCVixtQkFBbUIsc0JBQW5CLEdBQTRDLEVBRnpFLG1DQUdXRCxRQUFRWSxNQUhuQiw4QkFJT1osUUFBUWEsRUFKZixzQ0FLZWIsUUFBUU0sVUFMdkIsc0NBTWVOLFFBQVFFLFVBTnZCLHVGQVNZUiw0QkFBNEJYLFVBQVVRLFVBQXRDLENBVFosZ0VBVVlHLDRCQUE0QlgsVUFBVVEsVUFBdEMsQ0FWWiwyR0FjRFMsUUFBUWMsUUFkUCwyRUFrQkRkLFFBQVFBLE9BbEJQLHNEQXFCWU4sNEJBQTRCWCxVQUFVTyxVQUF0QyxDQXJCWixnQkFxQndFVSxRQUFRZSxVQXJCaEYsd0JBc0JEZixRQUFRZ0IsV0F0QlAsNkNBQU47O0FBMkJBO0FBQ0EsTUFBTUMsVUFBVXZELEVBQUVnRCxNQUFGLENBQWhCOztBQUVBO0FBQ0EsTUFBSSxDQUFDVixRQUFRZ0IsV0FBYixFQUEwQjtBQUN6QkMsV0FBUXJELElBQVIsQ0FBYW1CLFVBQVVPLFVBQXZCLEVBQW1DNEIsTUFBbkM7QUFDQTs7QUFFRDtBQUNBLE1BQUlWLGlCQUFKLEVBQXVCO0FBQ3RCUyxXQUFRckQsSUFBUixNQUFnQm1CLFVBQVVRLFVBQTFCLEVBQXdDMkIsTUFBeEM7QUFDQSxHQUZELE1BRU8sSUFBSWIsaUJBQUosRUFBdUI7QUFDN0JZLFdBQVFyRCxJQUFSLENBQWdCbUIsVUFBVVEsVUFBMUIsaUJBQWtEMkIsTUFBbEQ7QUFDQSxHQUZNLE1BRUEsSUFBSWpCLGdCQUFKLEVBQXNCO0FBQzVCZ0IsV0FBUXJELElBQVIsQ0FBZ0JtQixVQUFVUSxVQUExQixlQUFnRDJCLE1BQWhEO0FBQ0E7O0FBRUQsU0FBT0QsT0FBUDtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNFLDRCQUFULEdBQXdDO0FBQ3ZDLDZHQUVrQ3pCLDRCQUE0QlgsVUFBVUksUUFBdEMsQ0FGbEMsK0RBRzZDVixXQUFXMkMsU0FBWCxDQUFxQixVQUFyQixFQUFpQyxrQkFBakMsQ0FIN0M7QUFNQTs7QUFFRDs7O0FBR0EsVUFBU0MsdUJBQVQsR0FBbUM7QUFDbEM7QUFDQSxNQUFNQyxlQUFlNUQsRUFBRXFCLFVBQVVDLFNBQVosRUFBdUJwQixJQUF2QixDQUE0Qm1CLFVBQVVLLElBQXRDLENBQXJCOztBQUVBO0FBQ0FrQyxlQUFhQyxJQUFiLENBQWtCLFVBQUNDLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUNyQztBQUNBLE9BQU1DLFdBQVdoRSxFQUFFK0QsT0FBRixDQUFqQjs7QUFFQTtBQUNBLE9BQU1qRSxPQUFPa0UsU0FBU2xFLElBQVQsRUFBYjs7QUFFQTtBQUNBLE9BQU1tRSxXQUFXRCxTQUFTRSxRQUFULENBQWtCekQsUUFBUTBELGFBQTFCLENBQWpCOztBQUVBO0FBQ0EsT0FBSSxDQUFDRixRQUFELElBQWFuRSxLQUFLcUQsRUFBdEIsRUFBMEI7QUFDekIxQyxZQUFRMkQsU0FBUixDQUFrQnRFLEtBQUtxRCxFQUF2QixFQUEyQjFDLFFBQVE0RCxXQUFuQztBQUNBTCxhQUFTbEUsSUFBVCxDQUFjLFFBQWQsRUFBd0JXLFFBQVE0RCxXQUFoQztBQUNBOztBQUVEO0FBQ0EsT0FBSXZFLEtBQUswQyxVQUFMLElBQW1CMUMsS0FBSzBDLFVBQUwsQ0FBZ0JDLFFBQWhCLENBQXlCaEMsUUFBUWlDLDZCQUFqQyxDQUF2QixFQUF3RjtBQUN2RmpDLFlBQVE2RCxrQkFBUixDQUEyQnhFLEtBQUswQyxVQUFoQztBQUNBO0FBQ0QsR0FwQkQ7QUFxQkE7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTK0Isa0JBQVQsQ0FBNEJDLFFBQTVCLEVBQXNDO0FBQ3JDO0FBQ0EsTUFBTUMsY0FBYyxRQUFwQjs7QUFFQTtBQUNBLE1BQUlDLFFBQVEsQ0FBWjs7QUFFQTtBQUNBLE1BQUlGLFNBQVNHLE1BQWIsRUFBcUI7QUFDcEJILFlBQVNJLE9BQVQsQ0FBaUI7QUFBQSxXQUFXRixRQUMzQnBDLFFBQVFFLFVBQVIsQ0FBbUJDLFFBQW5CLENBQTRCaEMsUUFBUWlDLDZCQUFwQyxJQUFxRWdDLEtBQXJFLEdBQTZFLEVBQUVBLEtBRC9EO0FBQUEsSUFBakI7QUFFQTs7QUFFRDtBQUNBLE1BQUlBLEtBQUosRUFBVztBQUNWekUsWUFDRTRFLFdBREYsQ0FDY0osV0FEZCxFQUVFSyxJQUZGLENBRU9KLEtBRlA7QUFHQSxHQUpELE1BSU87QUFDTnpFLFlBQVM4RSxRQUFULENBQWtCTixXQUFsQjtBQUNBO0FBQ0Q7O0FBRUQ7OztBQUdBLFVBQVNPLFFBQVQsR0FBb0I7QUFDbkJDLG1CQUFpQixDQUFDakYsRUFBRXFCLFVBQVVDLFNBQVosRUFBdUJxRCxNQUF6QztBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNNLGdCQUFULENBQTBCQyxNQUExQixFQUFrQztBQUNqQztBQUNBLE1BQUlwRCxXQUFKLEVBQWlCO0FBQ2hCO0FBQ0E7O0FBRUQ7QUFDQUEsZ0JBQWMsSUFBZDs7QUFFQTtBQUNBcUQsYUFBVztBQUFBLFVBQU1yRCxjQUFjLEtBQXBCO0FBQUEsR0FBWCxFQUFzQ3ZCLFFBQVFGLGlCQUE5Qzs7QUFFQSxNQUFJNkUsTUFBSixFQUFZO0FBQ1g7QUFDQW5GLFNBQU1xRixPQUFOLENBQWMsTUFBZDtBQUNBLEdBSEQsTUFHTztBQUNOO0FBQ0FwRixLQUFFcUIsVUFBVUMsU0FBWixFQUF1QnVELFdBQXZCLENBQW1DM0QsUUFBUUMsZ0JBQTNDO0FBQ0FnRSxjQUFXO0FBQUEsV0FBTXBGLE1BQU1xRixPQUFOLENBQWMsTUFBZCxDQUFOO0FBQUEsSUFBWCxFQUF3QzdFLFFBQVFGLGlCQUFoRDtBQUNBO0FBQ0Q7O0FBRUQ7Ozs7QUFJQSxVQUFTZ0YsZUFBVCxHQUEyQjtBQUMxQjtBQUNBLE1BQU1DLGFBQWF0RixFQUFFcUIsVUFBVUMsU0FBWixDQUFuQjs7QUFFQTtBQUNBZ0UsYUFDRVAsUUFERixDQUNXN0QsUUFBUUMsZ0JBRG5CLEVBRUVvRSxHQUZGLENBRU0sRUFBQ0MsS0FBS0YsV0FBV0csTUFBWCxLQUFzQixDQUFDLENBQTdCLEVBRk47O0FBSUE7QUFDQUM7O0FBRUE7QUFDQUMsaUJBQWUsSUFBZjs7QUFFQTtBQUNBbEYsVUFBUW1GLFdBQVIsR0FBc0JDLElBQXRCLENBQTJCLG9CQUFZO0FBQ3RDO0FBQ0FGLGtCQUFlLEtBQWY7O0FBRUE7QUFDQUcsNkJBQTBCdEIsUUFBMUI7O0FBRUE7QUFDQXVCOztBQUVBO0FBQ0F4QixzQkFBbUJDLFFBQW5COztBQUVBO0FBQ0FiO0FBQ0EsR0FmRDs7QUFpQkE7QUFDQTJCLGFBQ0VVLEdBREYsQ0FDTSxjQUROLEVBRUVDLEVBRkYsQ0FFSyxPQUZMLEVBRWM1RSxVQUFVTyxVQUZ4QixFQUVvQ3NFLHlCQUZwQyxFQUdFRCxFQUhGLENBR0ssT0FITCxFQUdjNUUsVUFBVVEsVUFIeEIsRUFHb0NzRSx5QkFIcEMsRUFJRUYsRUFKRixDQUlLLFFBSkwsRUFJZTVFLFVBQVVJLFFBSnpCLEVBSW1DMkUsMkJBSm5DO0FBS0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0MseUJBQVQsQ0FBbUNuQixNQUFuQyxFQUEyQztBQUMxQztBQUNBLE1BQU1JLGFBQWF0RixFQUFFcUIsVUFBVUMsU0FBWixDQUFuQjs7QUFFQTtBQUNBLE1BQU1nRixzQkFBc0JoQixXQUFXcEYsSUFBWCxDQUFnQm1CLFVBQVVLLElBQTFCLEVBQWdDNkUsTUFBaEMsQ0FBdUNsRixVQUFVTSxVQUFqRCxDQUE1Qjs7QUFFQTtBQUNBLE1BQUl1RCxNQUFKLEVBQVk7QUFDWG9CLHVCQUFvQnpCLFdBQXBCLENBQWdDM0QsUUFBUUUsTUFBeEM7QUFDQSxHQUZELE1BRU87QUFDTmtGLHVCQUFvQnZCLFFBQXBCLENBQTZCN0QsUUFBUUUsTUFBckM7QUFDQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVMyRSxzQkFBVCxHQUFrQztBQUNqQztBQUNBLE1BQU1ULGFBQWF0RixFQUFFcUIsVUFBVUMsU0FBWixDQUFuQjs7QUFFQTtBQUNBLE1BQU1zQyxlQUFlMEIsV0FBV3BGLElBQVgsQ0FBZ0JtQixVQUFVRyxJQUExQixDQUFyQjs7QUFFQTtBQUNBLE1BQU1nRixnQkFBZ0I1QyxhQUFhMUQsSUFBYixDQUFrQm1CLFVBQVVLLElBQTVCLENBQXRCOztBQUVBO0FBQ0EsTUFBTTRFLHNCQUFzQkUsY0FBY0QsTUFBZCxDQUFxQmxGLFVBQVVNLFVBQS9CLENBQTVCOztBQUVBO0FBQ0EsTUFBTThFLHVCQUF1QkQsY0FBY0QsTUFBZCx3QkFBMEM5RixRQUFRaUMsNkJBQWxELE9BQTdCOztBQUVBO0FBQ0EsTUFBSTRELG9CQUFvQjNCLE1BQXhCLEVBQWdDO0FBQy9CMEIsNkJBQTBCLEtBQTFCO0FBQ0F6QyxnQkFBYThDLE1BQWIsQ0FBb0JqRCw4QkFBcEI7QUFDQTs7QUFFRDtBQUNBLE1BQUlnRCxxQkFBcUI5QixNQUF6QixFQUFpQztBQUNoQzZCLGlCQUNFRyxHQURGLENBQ01GLHFCQUFxQkcsS0FBckIsRUFETixFQUVFQyxJQUZGO0FBR0E7QUFDRDs7QUFFRDs7Ozs7QUFLQSxVQUFTZix5QkFBVCxDQUFtQ3RCLFFBQW5DLEVBQTZDO0FBQzVDO0FBQ0EsTUFBTWMsYUFBYXRGLEVBQUVxQixVQUFVQyxTQUFaLENBQW5COztBQUVBO0FBQ0EsTUFBTXNDLGVBQWUwQixXQUFXcEYsSUFBWCxDQUFnQm1CLFVBQVVHLElBQTFCLENBQXJCOztBQUVBO0FBQ0EsTUFBTXNGLHNCQUFzQixFQUE1Qjs7QUFFQTtBQUNBbEQsZUFBYW1ELEtBQWI7O0FBRUE7QUFDQSxNQUFJdkMsU0FBU0csTUFBYixFQUFxQjtBQUNwQjtBQURvQjtBQUFBO0FBQUE7O0FBQUE7QUFFcEIseUJBQXNCSCxRQUF0Qiw4SEFBZ0M7QUFBQSxTQUFyQmxDLE9BQXFCOztBQUMvQjtBQUNBLFNBQU1pQixVQUFVbEIsc0JBQXNCQyxPQUF0QixDQUFoQjs7QUFFQTtBQUNBd0UseUJBQW9CRSxJQUFwQixDQUF5QnpELE9BQXpCO0FBQ0E7QUFSbUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNwQixHQVRELE1BU087QUFDTjtBQUNBLE9BQU0wRCw4QkFBOEI1RSxzQkFBc0I7QUFDekRDLGFBQVN2QixXQUFXMkMsU0FBWCxDQUFxQixhQUFyQixFQUFvQyxrQkFBcEMsQ0FEZ0Q7QUFFekRkLGdCQUFZbkMsUUFBUXNDLG9CQUZxQztBQUd6REcsWUFBUXpDLFFBQVE0RCxXQUh5QztBQUl6RGpCLGNBQVUsRUFKK0M7QUFLekRILFVBQU0sRUFMbUQ7QUFNekRFLFFBQUksRUFOcUQ7QUFPekRYLGdCQUFZO0FBUDZDLElBQXRCLENBQXBDOztBQVVBO0FBQ0FzRSx1QkFBb0JFLElBQXBCLENBQXlCQywyQkFBekI7QUFDQTs7QUFFRDtBQUNBSCxzQkFBb0JsQyxPQUFwQixDQUE0QjtBQUFBLFVBQVdoQixhQUFhOEMsTUFBYixDQUFvQjNDLE9BQXBCLENBQVg7QUFBQSxHQUE1Qjs7QUFFQTtBQUNBSCxlQUNFc0QsUUFERixHQUVFckQsSUFGRixDQUVPLFVBQUNDLEtBQUQsRUFBUUMsT0FBUjtBQUFBLFVBQW9CL0QsRUFBRStELE9BQUYsRUFBVzhDLElBQVgsR0FBa0JNLE1BQWxCLEVBQXBCO0FBQUEsR0FGUDtBQUdBOztBQUVEOzs7QUFHQSxVQUFTakIseUJBQVQsQ0FBbUNrQixLQUFuQyxFQUEwQztBQUN6QztBQUNBQSxRQUFNQyxjQUFOO0FBQ0FELFFBQU1FLGVBQU47O0FBRUE7QUFDQSxNQUFNQyxPQUFPdkgsRUFBRSxJQUFGLEVBQVF3SCxJQUFSLENBQWEsTUFBYixDQUFiOztBQUVBO0FBQ0EsTUFBTXhELFdBQVdoRSxFQUFFLElBQUYsRUFBUXlILE9BQVIsQ0FBZ0IsVUFBaEIsQ0FBakI7O0FBRUEsVUFBUXpELFNBQVNsRSxJQUFULENBQWMsWUFBZCxDQUFSO0FBQ0MsUUFBSyxhQUFMO0FBQ0NFLE1BQUUwSCxHQUFGLENBQU1ILElBQU4sRUFBWUksSUFBWixDQUFpQixvQkFBWTtBQUM1QmxILGFBQVFtSCxpQkFBUixDQUEwQkMsUUFBMUI7QUFDQSxLQUZEO0FBR0E7QUFDRDtBQUNDO0FBQ0EsUUFBSU4sUUFBUUEsS0FBS08sSUFBTCxHQUFZbkQsTUFBeEIsRUFBZ0M7QUFDL0JvRCxZQUFPQyxJQUFQLENBQVlULElBQVosRUFBa0JoSCxRQUFRRCxZQUExQjtBQUNBO0FBVkg7QUFZQTs7QUFFRDs7O0FBR0EsVUFBUzZGLHlCQUFULEdBQXFDO0FBQ3BDO0FBQ0EsTUFBTThCLFdBQVdqSSxFQUFFLElBQUYsQ0FBakI7O0FBRUE7QUFDQSxNQUFNa0ksV0FBV0QsU0FBUy9ELFFBQVQsQ0FBa0IsV0FBbEIsQ0FBakI7O0FBRUE7QUFDQSxNQUFNZixLQUFLOEUsU0FBU1IsT0FBVCxDQUFpQnBHLFVBQVVLLElBQTNCLEVBQWlDNUIsSUFBakMsQ0FBc0MsSUFBdEMsQ0FBWDs7QUFFQTtBQUNBLE1BQUlvSSxRQUFKLEVBQWM7QUFDYkMsa0JBQWVoRixFQUFmO0FBQ0EsR0FGRCxNQUVPO0FBQ05pRixnQkFBYWpGLEVBQWI7QUFDQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVNrRixjQUFULENBQXdCakIsS0FBeEIsRUFBK0I7QUFDOUI7QUFDQSxNQUFNa0IsVUFBVXRJLEVBQUVvSCxNQUFNbUIsTUFBUixDQUFoQjs7QUFFQTtBQUNBLE1BQU1qRCxhQUFhdEYsRUFBRXFCLFVBQVVDLFNBQVosQ0FBbkI7O0FBRUE7QUFDQSxNQUFNa0gsb0JBQW9CekksTUFBTTBJLEdBQU4sQ0FBVUgsT0FBVixFQUFtQjNELE1BQW5CLElBQTZCNUUsTUFBTTJJLEVBQU4sQ0FBU0osT0FBVCxFQUFrQjNELE1BQXpFO0FBQ0EsTUFBTWdFLG1CQUFtQnJELFdBQVdYLE1BQXBDO0FBQ0EsTUFBTWlFLDRCQUE0QixDQUFDdEQsV0FBV21ELEdBQVgsQ0FBZUgsT0FBZixFQUF3QjNELE1BQTNEOztBQUVBO0FBQ0EsTUFBSWlFLDZCQUE2QkQsZ0JBQTdCLElBQWlELENBQUNILGlCQUFsRCxJQUF1RSxDQUFDekcscUJBQTVFLEVBQW1HO0FBQ2xHa0Qsb0JBQWlCLEtBQWpCO0FBQ0E7QUFDRDs7QUFFRDs7O0FBR0EsVUFBU1MsYUFBVCxHQUF5QjtBQUN4QjtBQUNBLE1BQU1tRCxlQUFlLEdBQXJCO0FBQ0EsTUFBTUMsaUJBQWlCLEdBQXZCOztBQUVBO0FBQ0EsTUFBTXhELGFBQWF0RixFQUFFcUIsVUFBVUMsU0FBWixDQUFuQjs7QUFFQTtBQUNBLE1BQU15SCxTQUFTekQsV0FBV3BGLElBQVgsQ0FBZ0JtQixVQUFVRSxLQUExQixDQUFmOztBQUVBO0FBQ0EsTUFBSStELFdBQVdYLE1BQWYsRUFBdUI7QUFDdEIsT0FBTXFFLGNBQWMxRCxXQUFXMkQsTUFBWCxHQUFvQkMsSUFBcEIsR0FBMkJMLFlBQS9DO0FBQ0EsT0FBTU0sZ0JBQWdCcEosTUFBTWtKLE1BQU4sR0FBZUMsSUFBZixHQUFzQkosY0FBdEIsR0FBd0MvSSxNQUFNcUosS0FBTixLQUFnQixDQUE5RTs7QUFFQUwsVUFBT0UsTUFBUCxDQUFjLEVBQUNDLE1BQU1GLFdBQVAsRUFBZDtBQUNBMUQsY0FBVzJELE1BQVgsQ0FBa0IsRUFBQ0MsTUFBTUMsYUFBUCxFQUFsQjtBQUNBO0FBQ0Q7O0FBRUQ7OztBQUdBLFVBQVMvQywyQkFBVCxHQUF1QztBQUN0QztBQUNBLE1BQU1pRCxvQkFBb0JySixFQUFFLElBQUYsRUFBUTBJLEVBQVIsQ0FBVyxVQUFYLENBQTFCOztBQUVBO0FBQ0FyQyw0QkFBMEJnRCxpQkFBMUI7QUFDQTFGO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU3dFLGNBQVQsQ0FBd0JoRixFQUF4QixFQUE0QjtBQUMzQjFDLFVBQ0U2SSxVQURGLENBQ2FuRyxFQURiLEVBRUUwQyxJQUZGLENBRU8wRCxRQUZQO0FBR0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU25CLFlBQVQsQ0FBc0JqRixFQUF0QixFQUEwQjtBQUN6QjFDLFVBQ0UyRCxTQURGLENBQ1lqQixFQURaLEVBQ2dCMUMsUUFBUTBELGFBRHhCLEVBRUUwQixJQUZGLENBRU8wRCxRQUZQO0FBR0E7O0FBRUQ7OztBQUdBLFVBQVNBLFFBQVQsR0FBb0I7QUFDbkI7QUFDQSxNQUFNakUsYUFBYXRGLEVBQUVxQixVQUFVQyxTQUFaLENBQW5COztBQUVBO0FBQ0EsTUFBTWtJLFdBQVczSSxRQUFRNEksSUFBUixDQUFhbkUsVUFBYixFQUF5QixJQUF6QixDQUFqQjs7QUFFQTtBQUNBRDs7QUFFQTtBQUNBeEUsVUFBUWdHLElBQVIsQ0FBYTJDLFFBQWI7QUFDQTs7QUFFRDs7O0FBR0EsVUFBU0UsZ0JBQVQsR0FBNEI7QUFDM0I7QUFDQTtBQUNBakosVUFBUW1GLFdBQVIsR0FBc0JDLElBQXRCLENBQTJCLG9CQUFZO0FBQ3RDO0FBQ0F0QixzQkFBbUJDLFFBQW5COztBQUVBO0FBQ0E7QUFMc0M7QUFBQTtBQUFBOztBQUFBO0FBTXRDLDBCQUFzQkEsUUFBdEIsbUlBQWdDO0FBQUEsU0FBckJsQyxPQUFxQjs7QUFDL0IsU0FBSUEsUUFBUVksTUFBUixLQUFtQnpDLFFBQVFrSixVQUEvQixFQUEyQztBQUMxQztBQUNBO0FBQ0EsVUFBSTdILGVBQWVDLHFCQUFuQixFQUEwQztBQUN6QztBQUNBOztBQUVEO0FBQ0FrRCx1QkFBaUIsSUFBakI7O0FBRUE7QUFDQWxELDhCQUF3QixJQUF4Qjs7QUFFQTtBQUNBb0QsaUJBQVcsWUFBTTtBQUNoQkYsd0JBQWlCLEtBQWpCO0FBQ0FsRCwrQkFBd0IsS0FBeEI7QUFDQSxPQUhELEVBR0d4QixRQUFRSCxVQUhYOztBQUtBO0FBQ0E7QUFDRDtBQTVCcUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTZCdEMsR0E3QkQ7QUE4QkE7O0FBRUQ7Ozs7O0FBS0EsVUFBU3VGLGNBQVQsQ0FBd0JpRSxTQUF4QixFQUFtQztBQUNsQztBQUNBLE1BQU1DLHNCQUFzQixVQUE1Qjs7QUFFQTtBQUNBLE1BQU1qRyxlQUFlNUQsRUFBRXFCLFVBQVVDLFNBQVosRUFBdUJwQixJQUF2QixDQUE0Qm1CLFVBQVVHLElBQXRDLENBQXJCOztBQUVBO0FBQ0EsTUFBTXNJLGtCQUFrQmxHLGFBQWExRCxJQUFiLENBQWtCMkosbUJBQWxCLENBQXhCOztBQUVBO0FBQ0EsTUFBTTdHLG9DQUNTaEIsNEJBQTRCNkgsbUJBQTVCLENBRFQsc0JBRUY5SSxXQUFXMkMsU0FBWCxDQUFxQixTQUFyQixFQUFnQyxrQkFBaEMsQ0FGRSw2QkFBTjs7QUFNQTtBQUNBb0csa0JBQWdCdEcsTUFBaEI7O0FBRUE7QUFDQSxNQUFJb0csU0FBSixFQUFlO0FBQ2RoRyxnQkFBYThDLE1BQWIsQ0FBb0IxRyxFQUFFZ0QsTUFBRixDQUFwQjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBO0FBQ0FwRCxRQUFPbUssSUFBUCxHQUFjLGdCQUFRO0FBQ3JCO0FBQ0FoSyxRQUNFcUYsT0FERixDQUNVO0FBQ1I0RSxjQUFXLEtBREg7QUFFUkMsY0FBVyxRQUZIO0FBR1JDLFlBQVMsR0FIRDtBQUlSQyxZQUFTLFFBSkQ7QUFLUkMsYUFBVWhJO0FBTEYsR0FEVixFQVFFNkQsRUFSRixDQVFLLE9BUkwsRUFRY2pCLFFBUmQsRUFTRWlCLEVBVEYsQ0FTSyxrQkFUTCxFQVN5QlosZUFUekIsRUFVRVksRUFWRixDQVVLLGNBVkwsRUFVcUI7QUFBQSxVQUFNaEIsaUJBQWlCLElBQWpCLENBQU47QUFBQSxHQVZyQixFQVdFZ0IsRUFYRixDQVdLLGtCQVhMLEVBV3lCeUQsZ0JBWHpCOztBQWFBO0FBQ0ExSixJQUFFK0gsTUFBRixFQUNFOUIsRUFERixDQUNLLFFBREwsRUFDZVAsYUFEZixFQUVFTyxFQUZGLENBRUssT0FGTCxFQUVjb0MsY0FGZDs7QUFJQTtBQUNBcUI7O0FBRUE7QUFDQS9CO0FBQ0EsRUF6QkQ7O0FBMkJBO0FBQ0EsUUFBTy9ILE1BQVA7QUFDQSxDQTlyQkYiLCJmaWxlIjoibGF5b3V0cy9tYWluL2hlYWRlci9pbmZvX2JveC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gaW5mb19ib3guanMgMjAxNi0xMS0xMFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogSW5mb2JveCBDb250cm9sbGVyXG4gKlxuICogSGFuZGxlcyB0aGUgZnVuY3Rpb25hbGl0eSBvZiB0aGUgaW5mbyBib3ggY29tcG9uZW50IG9mIHRoZSBhZG1pbiBsYXlvdXQgcGFnZXMuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J2luZm9fYm94Jyxcblx0XG5cdFtcblx0XHQnbG9hZGluZ19zcGlubmVyJyxcblx0XHRgJHtneC5zb3VyY2V9L2xpYnMvaW5mb19ib3hgXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvLyBNb2R1bGUgZWxlbWVudCwgd2hpY2ggcmVwcmVzZW50cyB0aGUgaW5mbyBib3guXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8vIE5vdGlmaWNhdGlvbiBjb3VudGVyIGljb24gZWxlbWVudC5cblx0XHRjb25zdCAkY291bnRlciA9ICR0aGlzLmZpbmQoJy5ub3RpZmljYXRpb24tY291bnQnKTtcblx0XHRcblx0XHQvLyBNb2R1bGUgZGVmYXVsdCBwYXJhbWV0ZXJzLlxuXHRcdGNvbnN0IGRlZmF1bHRzID0ge1xuXHRcdFx0Ly8gUG9wb3ZlciBzdGVhZHkgdGltZS5cblx0XHRcdGNsb3NlRGVsYXk6IDUwMDAsXG5cdFx0XHRcblx0XHRcdC8vIFJvbGwgb24vb3V0IGFuaW1hdGlvbiBkdXJhdGlvbi5cblx0XHRcdGFuaW1hdGlvbkR1cmF0aW9uOiA2MDAsXG5cdFx0XHRcblx0XHRcdC8vIERlZmF1bHQgb3BlbiBtb2RlIG9uIGxpbmtzLlxuXHRcdFx0bGlua09wZW5Nb2RlOiAnX3NlbGYnXG5cdFx0fTtcblx0XHRcblx0XHQvLyBNb2R1bGUgb3B0aW9ucy5cblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcblx0XHRcblx0XHQvLyBTaG9ydGN1dCB0byBpbmZvIGJveCBsaWJyYXJ5LlxuXHRcdGNvbnN0IGxpYnJhcnkgPSBqc2UubGlicy5pbmZvX2JveDtcblx0XHRcblx0XHQvLyBTaG9ydGN1dCB0byBzcGlubmVyIGxpYnJhcnkuXG5cdFx0Y29uc3Qgc3Bpbm5lciA9IGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lcjtcblx0XHRcblx0XHQvLyBTaG9ydGN1dCB0byBsYW5ndWFnZSB0ZXh0IHNlcnZpY2UuXG5cdFx0Y29uc3QgdHJhbnNsYXRvciA9IGpzZS5jb3JlLmxhbmc7XG5cdFx0XG5cdFx0Ly8gQ1NTIGNsYXNzZXMuXG5cdFx0Y29uc3QgY2xhc3NlcyA9IHtcblx0XHRcdC8vIEFjdGl2ZSBzdGF0ZSBvZiB0aGUgbWVzc2FnZSBsaXN0IGNvbnRhaW5lciAoZm9yIHRoZSByb2xsIGluL291dCBhbmltYXRpb24pLlxuXHRcdFx0Y29udGFpbmVyVmlzaWJsZTogJ3Zpc2libGUnLFxuXHRcdFx0XG5cdFx0XHQvLyBIaWRkZW4gbWVzc2FnZSBpdGVtLlxuXHRcdFx0aGlkZGVuOiAnaGlkZGVuJ1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gU2VsZWN0b3Igc3RyaW5ncy5cblx0XHRjb25zdCBzZWxlY3RvcnMgPSB7XG5cdFx0XHQvLyBNZXNzYWdlIGxpc3QgY29udGFpbmVyLlxuXHRcdFx0Y29udGFpbmVyOiAnLnBvcG92ZXIubWVzc2FnZS1saXN0LWNvbnRhaW5lcicsXG5cdFx0XHRcblx0XHRcdC8vIE1lc3NhZ2UgbGlzdCBjb250YWluZXIgYXJyb3cuXG5cdFx0XHRhcnJvdzogJy5hcnJvdycsXG5cdFx0XHRcblx0XHRcdC8vIE1lc3NhZ2UgbGlzdC5cblx0XHRcdGxpc3Q6ICcucG9wb3Zlci1jb250ZW50Jyxcblx0XHRcdFxuXHRcdFx0Ly8gTWVzc2FnZSBsaXN0IHZpc2liaWxpdHkgY2hlY2tib3guXG5cdFx0XHRjaGVja2JveDogJy52aXNpYmlsaXR5LWNoZWNrYm94Jyxcblx0XHRcdFxuXHRcdFx0Ly8gTWVzc2FnZSBpdGVtLlxuXHRcdFx0aXRlbTogJy5tZXNzYWdlJyxcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZGVuIG1lc3NhZ2UgaXRlbS5cblx0XHRcdGhpZGRlbkl0ZW06ICdbZGF0YS1zdGF0dXM9XCJoaWRkZW5cIl0nLFxuXHRcdFx0XG5cdFx0XHQvLyBNZXNzYWdlIGl0ZW0gYnV0dG9uLlxuXHRcdFx0aXRlbUJ1dHRvbjogJy5tZXNzYWdlLWJ1dHRvbicsXG5cdFx0XHRcblx0XHRcdC8vIE1lc3NhZ2UgaXRlbSBhY3Rpb24gKHJlbW92ZSwgaGlkZSwgZXRjLikuXG5cdFx0XHRpdGVtQWN0aW9uOiAnLmFjdGlvbi1pY29uJ1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gTW9kdWxlIG9iamVjdC5cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyBJbmRpY2F0ZXMgd2hldGhlciB0aGUgcm9sbCBhbmltYXRpb24gaXMgc3RpbGwgcnVubmluZy5cblx0XHRsZXQgaXNBbmltYXRpbmcgPSBmYWxzZTtcblx0XHRcblx0XHQvLyBJbmRpY2F0ZXMgd2hldGhlciB0aGUgbWVzc2FnZSBpdGVtIGxpc3QgY29udGFpbmVyIGlzIHNob3duIGZvciBhIGNlcnRhaW4gZHVyYXRpb25cblx0XHQvLyBIYXBwZW5zIG9uIG5ldyBtZXNzYWdlcyBhZnRlciBhIHNpbGVudCByZWZyZXNoLlxuXHRcdGxldCBpc1Nob3duV2l0aENsb3NlRGVsYXkgPSBmYWxzZTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyBhIHNwYWNlLXNlcGFyYXRlZCBDU1MgY2xhc3MgbmFtZSBieSByZXBsYWNpbmcgdGhlIHBlcmlvZHMgd2l0aCBzcGFjZXMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0gY2xhc3NOYW1lIENTUyBjbGFzcy5cblx0XHQgKiBcblx0XHQgKiBAcmV0dXJuIHtTdHJpbmd9IENsYXNzIG5hbWUgd2l0aG91dCBwZXJpb2RzLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRDbGFzc05hbWVXaXRob3V0UGVyaW9kcyhjbGFzc05hbWUpIHtcblx0XHRcdHJldHVybiBjbGFzc05hbWUuc3BsaXQoJy4nKS5qb2luKCcgJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJldHVybnMgdGhlIG1hcmt1cCBmb3IgdGhlIHBvcG92ZXIgdGVtcGxhdGUuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtTdHJpbmd9IEdlbmVyYXRlZCBIVE1MIHN0cmluZy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0UG9wb3Zlck1hcmt1cCgpIHtcblx0XHRcdHJldHVybiBgXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCIke19nZXRDbGFzc05hbWVXaXRob3V0UGVyaW9kcyhzZWxlY3RvcnMuY29udGFpbmVyKX1cIiByb2xlPVwidG9vbHRpcFwiPlxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCIke19nZXRDbGFzc05hbWVXaXRob3V0UGVyaW9kcyhzZWxlY3RvcnMuYXJyb3cpfVwiPjwvZGl2PlxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJwb3BvdmVyLXRpdGxlXCI+PC9kaXY+XG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cIiR7X2dldENsYXNzTmFtZVdpdGhvdXRQZXJpb2RzKHNlbGVjdG9ycy5saXN0KX1cIj48L2Rpdj5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHRgO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXR1cm5zIHRoZSBnZW5lcmF0ZWQgbWFya3VwIGZvciBhIG1lc3NhZ2UgaXRlbS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBtZXNzYWdlIE1lc3NhZ2UgaXRlbS5cblx0XHQgKiBcblx0XHQgKiBAcmV0dXJuIHtqUXVlcnl9IE1hcmt1cCBhcyBqUXVlcnkgb2JqZWN0LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRNZXNzYWdlSXRlbU1hcmt1cChtZXNzYWdlKSB7XG5cdFx0XHQvLyBJcyB0aGUgbWVzc2FnZSBhbiBhZG1pbiBhY3Rpb24gc3VjY2VzcyBtZXNzYWdlP1xuXHRcdFx0Y29uc3QgaXNTdWNjZXNzTWVzc2FnZSA9IG1lc3NhZ2UuaWRlbnRpZmllclxuXHRcdFx0XHQmJiBtZXNzYWdlLmlkZW50aWZpZXIuaW5jbHVkZXMobGlicmFyeS5TVUNDRVNTX01TR19JREVOVElGSUVSX1BSRUZJWCk7XG5cdFx0XHRcblx0XHRcdC8vIElzIHRoZSBtZXNzYWdlIGhpZGVhYmxlIGJ1dCBub3QgcmVtb3ZhYmxlP1xuXHRcdFx0Y29uc3QgaXNIaWRlYWJsZU1lc3NhZ2UgPSAhaXNTdWNjZXNzTWVzc2FnZVxuXHRcdFx0XHQmJiBtZXNzYWdlLnZpc2liaWxpdHkgJiYgbWVzc2FnZS52aXNpYmlsaXR5ID09PSBsaWJyYXJ5LlZJU0lCSUxJVFlfSElERUFCTEU7XG5cdFx0XHRcblx0XHRcdC8vIElzIHRoZSBtZXNzYWdlIGFsd2F5cyB2aXNpYmxlP1xuXHRcdFx0Y29uc3QgaXNBbHdheXNPbk1lc3NhZ2UgPSAhaXNTdWNjZXNzTWVzc2FnZVxuXHRcdFx0XHQmJiBtZXNzYWdlLnZpc2liaWxpdHkgJiYgbWVzc2FnZS52aXNpYmlsaXR5ID09PSBsaWJyYXJ5LlZJU0lCSUxJVFlfQUxXQVlTX09OO1xuXHRcdFx0XG5cdFx0XHQvLyBNZXNzYWdlIGl0ZW0gbWFya3VwLlxuXHRcdFx0Y29uc3QgbWFya3VwID0gYFxuXHRcdFx0XHQ8ZGl2XG5cdFx0XHRcdFx0Y2xhc3M9XCJtZXNzYWdlICR7bWVzc2FnZS50eXBlfSAke2lzU3VjY2Vzc01lc3NhZ2UgPyAnYWRtaW4tYWN0aW9uLXN1Y2Nlc3MnIDogJyd9XCJcblx0XHRcdFx0XHRkYXRhLXN0YXR1cz1cIiR7bWVzc2FnZS5zdGF0dXN9XCJcblx0XHRcdFx0XHRkYXRhLWlkPVwiJHttZXNzYWdlLmlkfVwiXG5cdFx0XHRcdFx0ZGF0YS12aXNpYmlsaXR5PVwiJHttZXNzYWdlLnZpc2liaWxpdHl9XCJcblx0XHRcdFx0XHRkYXRhLWlkZW50aWZpZXI9XCIke21lc3NhZ2UuaWRlbnRpZmllcn1cIlxuXHRcdFx0XHRcdD5cblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiYWN0aW9uLWljb25zXCI+XG5cdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cIiR7X2dldENsYXNzTmFtZVdpdGhvdXRQZXJpb2RzKHNlbGVjdG9ycy5pdGVtQWN0aW9uKX0gZG8taGlkZSBmYSBmYS1taW51c1wiPjwvc3Bhbj5cblx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwiJHtfZ2V0Q2xhc3NOYW1lV2l0aG91dFBlcmlvZHMoc2VsZWN0b3JzLml0ZW1BY3Rpb24pfSBkby1yZW1vdmUgZmEgZmEtdGltZXNcIj48L3NwYW4+XG5cdFx0XHRcdFx0PC9kaXY+XG5cblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiaGVhZGxpbmVcIj5cblx0XHRcdFx0XHRcdCR7bWVzc2FnZS5oZWFkbGluZX1cblx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XG5cdFx0XHRcdFx0XHQke21lc3NhZ2UubWVzc2FnZX1cblx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdDxhIGNsYXNzPVwiYnRuICR7X2dldENsYXNzTmFtZVdpdGhvdXRQZXJpb2RzKHNlbGVjdG9ycy5pdGVtQnV0dG9uKX1cIiBocmVmPVwiJHttZXNzYWdlLmJ1dHRvbkxpbmt9XCI+XG5cdFx0XHRcdFx0XHQke21lc3NhZ2UuYnV0dG9uTGFiZWx9XG5cdFx0XHRcdFx0PC9hPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdGA7XG5cdFx0XHRcblx0XHRcdC8vIE1hcmt1cCBhcyBqUXVlcnkgb2JqZWN0IGZvciBtYW5pcHVsYXRpb24gcHVycG9zZXMuXG5cdFx0XHRjb25zdCAkbWFya3VwID0gJChtYXJrdXApO1xuXHRcdFx0XG5cdFx0XHQvLyBSZW1vdmUgYnV0dG9uIGZyb20gbWFya3VwLCBpZiBubyBidXR0b24gbGFiZWwgaXMgZGVmaW5lZC5cblx0XHRcdGlmICghbWVzc2FnZS5idXR0b25MYWJlbCkge1xuXHRcdFx0XHQkbWFya3VwLmZpbmQoc2VsZWN0b3JzLml0ZW1CdXR0b24pLnJlbW92ZSgpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTaG93IHJlbW92ZS9oaWRlIGJ1dHRvbiwgZGVwZW5kaW5nIG9uIHRoZSB2aXNpYmlsaXR5IHZhbHVlIGFuZCBraW5kIG9mIG1lc3NhZ2UuXG5cdFx0XHRpZiAoaXNBbHdheXNPbk1lc3NhZ2UpIHtcblx0XHRcdFx0JG1hcmt1cC5maW5kKGAke3NlbGVjdG9ycy5pdGVtQWN0aW9ufWApLnJlbW92ZSgpO1xuXHRcdFx0fSBlbHNlIGlmIChpc0hpZGVhYmxlTWVzc2FnZSkge1xuXHRcdFx0XHQkbWFya3VwLmZpbmQoYCR7c2VsZWN0b3JzLml0ZW1BY3Rpb259IGRvLXJlbW92ZWApLnJlbW92ZSgpO1xuXHRcdFx0fSBlbHNlIGlmIChpc1N1Y2Nlc3NNZXNzYWdlKSB7XG5cdFx0XHRcdCRtYXJrdXAuZmluZChgJHtzZWxlY3RvcnMuaXRlbUFjdGlvbn0gZG8taGlkZWApLnJlbW92ZSgpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRyZXR1cm4gJG1hcmt1cDtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgbWFya3VwIGZvciB0aGUgbWVzc2FnZSBpdGVtcyB2aXNpYmlsaXR5IGNoZWNrYm94LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7U3RyaW5nfSBHZW5lcmF0ZWQgSFRNTCBzdHJpbmcuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldFZpc2liaWxpdHlDaGVja2JveE1hcmt1cCgpIHtcblx0XHRcdHJldHVybiBgXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJ2aXNpYmlsaXR5LWNoZWNrYm94LWNvbnRhaW5lclwiPlxuXHRcdFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cIiR7X2dldENsYXNzTmFtZVdpdGhvdXRQZXJpb2RzKHNlbGVjdG9ycy5jaGVja2JveCl9XCI+XG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzPVwidmlzaWJpbGl0eS1jaGVja2JveC1sYWJlbFwiPiR7dHJhbnNsYXRvci50cmFuc2xhdGUoJ1NIT1dfQUxMJywgJ2FkbWluX2luZm9fYm94ZXMnKX08L2xhYmVsPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdGA7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1hcmtzIGVhY2ggdmlzaWJsZSBtZXNzYWdlIGl0ZW0gYXMgcmVhZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfbWFya01lc3NhZ2VJdGVtc0FzUmVhZCgpIHtcblx0XHRcdC8vIE1lc3NhZ2UgaXRlbXMuXG5cdFx0XHRjb25zdCAkbWVzc2FnZUxpc3QgPSAkKHNlbGVjdG9ycy5jb250YWluZXIpLmZpbmQoc2VsZWN0b3JzLml0ZW0pO1xuXHRcdFx0XG5cdFx0XHQvLyBJdGVyYXRlIG92ZXIgZWFjaCBtZXNzYWdlIGFuZCBtYXJrIGFzIHJlYWQuXG5cdFx0XHQkbWVzc2FnZUxpc3QuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcblx0XHRcdFx0Ly8gQ3VycmVudCBpdGVyYXRpb24gZWxlbWVudC5cblx0XHRcdFx0Y29uc3QgJG1lc3NhZ2UgPSAkKGVsZW1lbnQpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gTWVzc2FnZSBkYXRhLlxuXHRcdFx0XHRjb25zdCBkYXRhID0gJG1lc3NhZ2UuZGF0YSgpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gSW5kaWNhdGUsIGlmIHRoZSBtZXNzYWdlIGlzIGRlY2xhcmVkIGFzIGhpZGRlbi5cblx0XHRcdFx0Y29uc3QgaXNIaWRkZW4gPSAkbWVzc2FnZS5oYXNDbGFzcyhsaWJyYXJ5LlNUQVRVU19ISURERU4pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRGVsZXRlIGJ5IElEIGlmIGV4aXN0ZW50LlxuXHRcdFx0XHRpZiAoIWlzSGlkZGVuICYmIGRhdGEuaWQpIHtcblx0XHRcdFx0XHRsaWJyYXJ5LnNldFN0YXR1cyhkYXRhLmlkLCBsaWJyYXJ5LlNUQVRVU19SRUFEKTtcblx0XHRcdFx0XHQkbWVzc2FnZS5kYXRhKCdzdGF0dXMnLCBsaWJyYXJ5LlNUQVRVU19SRUFEKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRGVsZXRlIHN1Y2Nlc3MgbWVzc2FnZXMuXG5cdFx0XHRcdGlmIChkYXRhLmlkZW50aWZpZXIgJiYgZGF0YS5pZGVudGlmaWVyLmluY2x1ZGVzKGxpYnJhcnkuU1VDQ0VTU19NU0dfSURFTlRJRklFUl9QUkVGSVgpKSB7XG5cdFx0XHRcdFx0bGlicmFyeS5kZWxldGVCeUlkZW50aWZpZXIoZGF0YS5pZGVudGlmaWVyKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNldHMgdGhlIG1lc3NhZ2UgaXRlbSBhbW91bnQgaW4gdGhlIG5vdGlmaWNhdGlvbiBpY29uLlxuXHRcdCAqIFxuXHRcdCAqIEFkbWluIGFjdGlvbiBzdWNjZXNzIG1lc3NhZ2VzIHdpbGwgYmUgZXhjbHVkZWQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge0FycmF5fSBtZXNzYWdlcyBNZXNzYWdlIGl0ZW1zLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9zZXRNZXNzYWdlQ291bnRlcihtZXNzYWdlcykge1xuXHRcdFx0Ly8gSGlkZGVuIENTUyBjbGFzcy5cblx0XHRcdGNvbnN0IGhpZGRlbkNsYXNzID0gJ2hpZGRlbic7XG5cdFx0XHRcblx0XHRcdC8vIE1lc3NhZ2UgaXRlbSBjb3VudC5cblx0XHRcdGxldCBjb3VudCA9IDA7XG5cdFx0XHRcblx0XHRcdC8vIEl0ZXJhdGUgb3ZlciBlYWNoIG1lc3NhZ2UgaXRlbSBhbmQgY2hlY2sgbWVzc2FnZSBpZGVudGlmaWVyLlxuXHRcdFx0aWYgKG1lc3NhZ2VzLmxlbmd0aCkge1xuXHRcdFx0XHRtZXNzYWdlcy5mb3JFYWNoKG1lc3NhZ2UgPT4gY291bnQgPVxuXHRcdFx0XHRcdG1lc3NhZ2UuaWRlbnRpZmllci5pbmNsdWRlcyhsaWJyYXJ5LlNVQ0NFU1NfTVNHX0lERU5USUZJRVJfUFJFRklYKSA/IGNvdW50IDogKytjb3VudClcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gVGhlIG5vdGlmaWNhdGlvbiBjb3VudCB3aWxsIGJlIGhpZGRlbiwgaWYgdGhlcmUgYXJlIG5vIG1lc3NhZ2VzLlxuXHRcdFx0aWYgKGNvdW50KSB7XG5cdFx0XHRcdCRjb3VudGVyXG5cdFx0XHRcdFx0LnJlbW92ZUNsYXNzKGhpZGRlbkNsYXNzKVxuXHRcdFx0XHRcdC50ZXh0KGNvdW50KTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCRjb3VudGVyLmFkZENsYXNzKGhpZGRlbkNsYXNzKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgY2xpY2sgZXZlbnQgb24gdGhlIGluZm8gYm94IGFjdGlvbiBidXR0b24uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uQ2xpY2soKSB7XG5cdFx0XHRfdG9nZ2xlQ29udGFpbmVyKCEkKHNlbGVjdG9ycy5jb250YWluZXIpLmxlbmd0aCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFRvZ2dsZXMgdGhlIG1lc3NhZ2UgbGlzdCBjb250YWluZXIgKHBvcG92ZXIpIHdpdGggYW4gYW5pbWF0aW9uLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtCb29sZWFufSBkb1Nob3cgU2hvdyB0aGUgbWVzc2FnZSBsaXN0IGNvbnRhaW5lcj9cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfdG9nZ2xlQ29udGFpbmVyKGRvU2hvdykge1xuXHRcdFx0Ly8gRXhpdCBpbW1lZGlhdGVseSB3aGVuIHRoZSBhbmltYXRpb24gcHJvY2VzcyBpcyBzdGlsbCBydW5uaW5nLlxuXHRcdFx0aWYgKGlzQW5pbWF0aW5nKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gSW5kaWNhdGUgYW5pbWF0aW9uIHByb2Nlc3MuXG5cdFx0XHRpc0FuaW1hdGluZyA9IHRydWU7XG5cdFx0XHRcblx0XHRcdC8vIFN3aXRjaCBhbmltYXRpb24gcHJvY2VzcyBpbmRpY2F0b3IgdG8gZmFsc2UgYWZ0ZXIgYW5pbWF0aW9uIGR1cmF0aW9uLlxuXHRcdFx0c2V0VGltZW91dCgoKSA9PiBpc0FuaW1hdGluZyA9IGZhbHNlLCBvcHRpb25zLmFuaW1hdGlvbkR1cmF0aW9uKTtcblx0XHRcdFxuXHRcdFx0aWYgKGRvU2hvdykge1xuXHRcdFx0XHQvLyBQZXJmb3JtIG1lc3NhZ2UgaXRlbSBsaXN0IGNvbnRhaW5lciByb2xsIGluIGFuaW1hdGlvbi5cblx0XHRcdFx0JHRoaXMucG9wb3Zlcignc2hvdycpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Ly8gUGVyZm9ybSBtZXNzYWdlIGl0ZW0gbGlzdCBjb250YWluZXIgcm9sbCBvdXQgYW5pbWF0aW9uIGFuZCB0aGVuIGhpZGUuXG5cdFx0XHRcdCQoc2VsZWN0b3JzLmNvbnRhaW5lcikucmVtb3ZlQ2xhc3MoY2xhc3Nlcy5jb250YWluZXJWaXNpYmxlKTtcblx0XHRcdFx0c2V0VGltZW91dCgoKSA9PiAkdGhpcy5wb3BvdmVyKCdoaWRlJyksIG9wdGlvbnMuYW5pbWF0aW9uRHVyYXRpb24pO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBwb3BvdmVyIChtZXNzYWdlIGNvbnRhaW5lcikgJ3Nob3duJyBldmVudCBieSBnZXR0aW5nIHRoZSBtZXNzYWdlcyBmcm9tIHRoZSBzZXJ2ZXIgXG5cdFx0ICogYW5kIGRpc3BsYXlpbmcgdGhlbSBpbiB0aGUgY29udGFpbmVyLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblBvcG92ZXJTaG93bigpIHtcblx0XHRcdC8vIE1lc3NhZ2UgbGlzdCBjb250YWluZXIuXG5cdFx0XHRjb25zdCAkY29udGFpbmVyID0gJChzZWxlY3RvcnMuY29udGFpbmVyKTtcblx0XHRcdFxuXHRcdFx0Ly8gSW5kaWNhdGUgY29udGFpbmVyIHZpc2liaWxpdHkuXG5cdFx0XHQkY29udGFpbmVyXG5cdFx0XHRcdC5hZGRDbGFzcyhjbGFzc2VzLmNvbnRhaW5lclZpc2libGUpXG5cdFx0XHRcdC5jc3Moe3RvcDogJGNvbnRhaW5lci5oZWlnaHQoKSAqIC0xfSk7XG5cdFx0XHRcblx0XHRcdC8vIEZpeCBjb250YWluZXIgYW5kIGFycm93IHBvc2l0aW9uLlxuXHRcdFx0X2ZpeFBvc2l0aW9ucygpO1xuXHRcdFx0XG5cdFx0XHQvLyBFbmFibGUgbG9hZGluZyBzdGF0ZS5cblx0XHRcdF90b2dnbGVMb2FkaW5nKHRydWUpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZXRyaWV2ZSBtZXNzYWdlcyBmcm9tIHNlcnZlciBhbmQgc2hvdyB0aGVtIGluIHRoZSBjb250YWluZXIgYW5kIG1hcmsgdGhlbSBhcyByZWFkLlxuXHRcdFx0bGlicmFyeS5nZXRNZXNzYWdlcygpLnRoZW4obWVzc2FnZXMgPT4ge1xuXHRcdFx0XHQvLyBEaXNhYmxlIGxvYWRpbmcgc3RhdGUuXG5cdFx0XHRcdF90b2dnbGVMb2FkaW5nKGZhbHNlKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFB1dCBtZXNzYWdlcyBpbnRvIG1lc3NhZ2UgbGlzdC5cblx0XHRcdFx0X3B1dE1lc3NhZ2VzSW50b0NvbnRhaW5lcihtZXNzYWdlcyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBIaWRlIGhpZGRlbiBtZXNzYWdlIGl0ZW1zIGFuZCBzaG93IG9ubHkgdGhlIHN1Y2Nlc3MgbWVzc2FnZSBpdGVtIGlmIGV4aXN0cy5cblx0XHRcdFx0X3JldmlzZU1lc3NhZ2VJdGVtTGlzdCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2V0IG5vdGlmaWNhdGlvbiBjb3VudC5cblx0XHRcdFx0X3NldE1lc3NhZ2VDb3VudGVyKG1lc3NhZ2VzKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIE1hcmsgdmlzaWJsZSBtZXNzYWdlIGl0ZW1zIGFzIHJlYWQuXG5cdFx0XHRcdF9tYXJrTWVzc2FnZUl0ZW1zQXNSZWFkKCk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gQXR0YWNoIGV2ZW50IGhhbmRsZXJzIHRvIHBvcG92ZXIuXG5cdFx0XHQkY29udGFpbmVyXG5cdFx0XHRcdC5vZmYoJ2NsaWNrIGNoYW5nZScpXG5cdFx0XHRcdC5vbignY2xpY2snLCBzZWxlY3RvcnMuaXRlbUJ1dHRvbiwgX29uTWVzc2FnZUl0ZW1CdXR0b25DbGljaylcblx0XHRcdFx0Lm9uKCdjbGljaycsIHNlbGVjdG9ycy5pdGVtQWN0aW9uLCBfb25NZXNzYWdlSXRlbUFjdGlvbkNsaWNrKVxuXHRcdFx0XHQub24oJ2NoYW5nZScsIHNlbGVjdG9ycy5jaGVja2JveCwgX29uVmlzaWJpbGl0eUNoZWNrYm94Q2hhbmdlKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVG9nZ2xlcyB0aGUgaGlkZGVuIG1lc3NhZ2UgaXRlbXMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge0Jvb2xlYW59IGRvU2hvdyBTaG93IHRoZSBoaWRkZW4gbWVzc2FnZSBpdGVtcz9cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfdG9nZ2xlSGlkZGVuTWVzc2FnZUl0ZW1zKGRvU2hvdykge1xuXHRcdFx0Ly8gTWVzc2FnZSBsaXN0IGNvbnRhaW5lci5cblx0XHRcdGNvbnN0ICRjb250YWluZXIgPSAkKHNlbGVjdG9ycy5jb250YWluZXIpO1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRkZW4gbWVzc2FnZSBpdGVtcy5cblx0XHRcdGNvbnN0ICRoaWRkZW5NZXNzYWdlSXRlbXMgPSAkY29udGFpbmVyLmZpbmQoc2VsZWN0b3JzLml0ZW0pLmZpbHRlcihzZWxlY3RvcnMuaGlkZGVuSXRlbSk7XG5cdFx0XHRcblx0XHRcdC8vIFRvZ2dsZSB2aXNpYmlsaXR5LlxuXHRcdFx0aWYgKGRvU2hvdykge1xuXHRcdFx0XHQkaGlkZGVuTWVzc2FnZUl0ZW1zLnJlbW92ZUNsYXNzKGNsYXNzZXMuaGlkZGVuKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCRoaWRkZW5NZXNzYWdlSXRlbXMuYWRkQ2xhc3MoY2xhc3Nlcy5oaWRkZW4pO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXZpc2VzIHRoZSBtZXNzYWdlIGl0ZW0gbGlzdCBieSBoaWRpbmcgbWVzc2FnZSBpdGVtcyBkZWNsYXJlZCBhcyBoaWRkZW4uXG5cdFx0ICogXG5cdFx0ICogQWRkaXRpb25hbGx5LCBpZiBhbiBhZG1pbiBhY3Rpb24gc3VjY2VzcyBtZXNzYWdlIGl0ZW0gaXMgZm91bmQgaXQgd2lsbCBiZSBzb2xlbHkgZGlzcGxheWVkLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9yZXZpc2VNZXNzYWdlSXRlbUxpc3QoKSB7XG5cdFx0XHQvLyBNZXNzYWdlIGxpc3QgY29udGFpbmVyLlxuXHRcdFx0Y29uc3QgJGNvbnRhaW5lciA9ICQoc2VsZWN0b3JzLmNvbnRhaW5lcik7XG5cdFx0XHRcblx0XHRcdC8vIE1lc3NhZ2UgbGlzdC5cblx0XHRcdGNvbnN0ICRtZXNzYWdlTGlzdCA9ICRjb250YWluZXIuZmluZChzZWxlY3RvcnMubGlzdCk7XG5cdFx0XHRcblx0XHRcdC8vIE1lc3NhZ2UgaXRlbXMuXG5cdFx0XHRjb25zdCAkbWVzc2FnZUl0ZW1zID0gJG1lc3NhZ2VMaXN0LmZpbmQoc2VsZWN0b3JzLml0ZW0pO1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRkZW4gbWVzc2FnZSBpdGVtcy5cblx0XHRcdGNvbnN0ICRoaWRkZW5NZXNzYWdlSXRlbXMgPSAkbWVzc2FnZUl0ZW1zLmZpbHRlcihzZWxlY3RvcnMuaGlkZGVuSXRlbSk7XG5cdFx0XHRcblx0XHRcdC8vIEFkbWluIGFjdGlvbiBzdWNjZXNzIG1lc3NhZ2UgaXRlbXMuXG5cdFx0XHRjb25zdCAkc3VjY2Vzc01lc3NhZ2VJdGVtcyA9ICRtZXNzYWdlSXRlbXMuZmlsdGVyKGBbZGF0YS1pZGVudGlmaWVyKj0ke2xpYnJhcnkuU1VDQ0VTU19NU0dfSURFTlRJRklFUl9QUkVGSVh9XWApO1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRlIG1lc3NhZ2VzIGRlY2xhcmVkIGFzIGhpZGRlbiBhbmQgYWRkIHZpc2liaWxpdHkgY2hlY2tib3guXG5cdFx0XHRpZiAoJGhpZGRlbk1lc3NhZ2VJdGVtcy5sZW5ndGgpIHtcblx0XHRcdFx0X3RvZ2dsZUhpZGRlbk1lc3NhZ2VJdGVtcyhmYWxzZSk7XG5cdFx0XHRcdCRtZXNzYWdlTGlzdC5hcHBlbmQoX2dldFZpc2liaWxpdHlDaGVja2JveE1hcmt1cCgpKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gUmVtb3ZlIGFsbCBvdGhlciBtZXNzYWdlcyAoaW5jbHVkaW5nIGR1cGxpY2F0ZSBzdWNjZXNzIG1lc3NhZ2VzKSBpZiBhIHN1Y2Nlc3MgbWVzc2FnZSBpcyBwcmVzZW50LlxuXHRcdFx0aWYgKCRzdWNjZXNzTWVzc2FnZUl0ZW1zLmxlbmd0aCkge1xuXHRcdFx0XHQkbWVzc2FnZUl0ZW1zXG5cdFx0XHRcdFx0Lm5vdCgkc3VjY2Vzc01lc3NhZ2VJdGVtcy5maXJzdCgpKVxuXHRcdFx0XHRcdC5oaWRlKCk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbGxzIHRoZSBtZXNzYWdlIGxpc3Qgd2l0aCBtZXNzYWdlIGl0ZW1zLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gbWVzc2FnZXMgTWVzc2FnZSBpdGVtcy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfcHV0TWVzc2FnZXNJbnRvQ29udGFpbmVyKG1lc3NhZ2VzKSB7XG5cdFx0XHQvLyBNZXNzYWdlIGxpc3QgY29udGFpbmVyLlxuXHRcdFx0Y29uc3QgJGNvbnRhaW5lciA9ICQoc2VsZWN0b3JzLmNvbnRhaW5lcik7XG5cdFx0XHRcblx0XHRcdC8vIE1lc3NhZ2UgbGlzdC5cblx0XHRcdGNvbnN0ICRtZXNzYWdlTGlzdCA9ICRjb250YWluZXIuZmluZChzZWxlY3RvcnMubGlzdCk7XG5cdFx0XHRcblx0XHRcdC8vIEFycmF5IGNvbnRhaW5pbmcgYWxsIGdlbmVyYXRlZCBtZXNzYWdlIGl0ZW0gbWFya3Vwcy5cblx0XHRcdGNvbnN0IG1lc3NhZ2VJdGVtc01hcmt1cHMgPSBbXTtcblx0XHRcdFxuXHRcdFx0Ly8gQ2xlYXIgbWVzc2FnZSBsaXN0LlxuXHRcdFx0JG1lc3NhZ2VMaXN0LmVtcHR5KCk7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgbWVzc2FnZXMuXG5cdFx0XHRpZiAobWVzc2FnZXMubGVuZ3RoKSB7XG5cdFx0XHRcdC8vIEl0ZXJhdGUgb3ZlciBlYWNoIG1lc3NhZ2UgaXRlbSBhbmQgZ2VuZXJhdGUgbWFya3Vwcy5cblx0XHRcdFx0Zm9yIChjb25zdCBtZXNzYWdlIG9mIG1lc3NhZ2VzKSB7XG5cdFx0XHRcdFx0Ly8gR2VuZXJhdGUgbWFya3VwLlxuXHRcdFx0XHRcdGNvbnN0ICRtYXJrdXAgPSBfZ2V0TWVzc2FnZUl0ZW1NYXJrdXAobWVzc2FnZSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gQWRkIG1hcmt1cCB0byBhcnJheS5cblx0XHRcdFx0XHRtZXNzYWdlSXRlbXNNYXJrdXBzLnB1c2goJG1hcmt1cCk7XG5cdFx0XHRcdH1cblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdC8vIEdlbmVyYXRlIG1hcmt1cCBmb3IgdGhlIG1pc3NpbmcgZW50cmllcyBpbmZvIG1lc3NhZ2UgaXRlbS5cblx0XHRcdFx0Y29uc3QgJG5vRW50cmllc01lc3NhZ2VJdGVtTWFya3VwID0gX2dldE1lc3NhZ2VJdGVtTWFya3VwKHtcblx0XHRcdFx0XHRtZXNzYWdlOiB0cmFuc2xhdG9yLnRyYW5zbGF0ZSgnTk9fTUVTU0FHRVMnLCAnYWRtaW5faW5mb19ib3hlcycpLFxuXHRcdFx0XHRcdHZpc2liaWxpdHk6IGxpYnJhcnkuVklTSUJJTElUWV9BTFdBWVNfT04sXG5cdFx0XHRcdFx0c3RhdHVzOiBsaWJyYXJ5LlNUQVRVU19SRUFELFxuXHRcdFx0XHRcdGhlYWRsaW5lOiAnJyxcblx0XHRcdFx0XHR0eXBlOiAnJyxcblx0XHRcdFx0XHRpZDogJycsXG5cdFx0XHRcdFx0aWRlbnRpZmllcjogJydcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBZGQgbWFya3VwIHRvIGFycmF5LlxuXHRcdFx0XHRtZXNzYWdlSXRlbXNNYXJrdXBzLnB1c2goJG5vRW50cmllc01lc3NhZ2VJdGVtTWFya3VwKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gUHV0IHJlbmRlciBtZXNzYWdlcy5cblx0XHRcdG1lc3NhZ2VJdGVtc01hcmt1cHMuZm9yRWFjaChlbGVtZW50ID0+ICRtZXNzYWdlTGlzdC5hcHBlbmQoZWxlbWVudCkpO1xuXHRcdFx0XG5cdFx0XHQvLyBGYWRlIHRoZSBtZXNzYWdlIGl0ZW1zIGluIHNtb290aGx5LlxuXHRcdFx0JG1lc3NhZ2VMaXN0XG5cdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdC5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4gJChlbGVtZW50KS5oaWRlKCkuZmFkZUluKCkpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBjbGljayBldmVudCB0byBhIG1lc3NhZ2UgaXRlbSBidXR0b24gYnkgb3BlbmluZyB0aGUgbGluay5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25NZXNzYWdlSXRlbUJ1dHRvbkNsaWNrKGV2ZW50KSB7XG5cdFx0XHQvLyBQcmV2ZW50IGRlZmF1bHQgYmVoYXZpb3IuXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHRcblx0XHRcdC8vIExpbmsgdmFsdWUgZnJvbSBidXR0b24uXG5cdFx0XHRjb25zdCBocmVmID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrIGlmIHdlIG5lZWQgdG8gcGVyZm9ybSBhIHNwZWNpYWwgYWN0aW9uLiBcblx0XHRcdGNvbnN0ICRtZXNzYWdlID0gJCh0aGlzKS5wYXJlbnRzKCcubWVzc2FnZScpOyBcblx0XHRcdFxuXHRcdFx0c3dpdGNoICgkbWVzc2FnZS5kYXRhKCdpZGVudGlmaWVyJykpIHtcblx0XHRcdFx0Y2FzZSAnY2xlYXJfY2FjaGUnOiBcblx0XHRcdFx0XHQkLmdldChocmVmKS5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHRcdGxpYnJhcnkuYWRkU3VjY2Vzc01lc3NhZ2UocmVzcG9uc2UpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRcdC8vIE9wZW4gbGluayBpZiBleGlzdHMuXG5cdFx0XHRcdFx0aWYgKGhyZWYgJiYgaHJlZi50cmltKCkubGVuZ3RoKSB7XG5cdFx0XHRcdFx0XHR3aW5kb3cub3BlbihocmVmLCBvcHRpb25zLmxpbmtPcGVuTW9kZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBjbGljayBldmVudCB0byBhIG1lc3NhZ2UgaXRlbSBhY3Rpb24uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uTWVzc2FnZUl0ZW1BY3Rpb25DbGljaygpIHtcblx0XHRcdC8vIENsaWNrZWQgZWxlbWVudC5cblx0XHRcdGNvbnN0ICRlbGVtZW50ID0gJCh0aGlzKTtcblx0XHRcdFxuXHRcdFx0Ly8gQ2hlY2sgaWYgdGhlIGNsaWNrZWQgdGFyZ2V0IGluZGljYXRlcyBhIG1lc3NhZ2UgcmVtb3ZhbC5cblx0XHRcdGNvbnN0IGRvUmVtb3ZlID0gJGVsZW1lbnQuaGFzQ2xhc3MoJ2RvLXJlbW92ZScpO1xuXHRcdFx0XG5cdFx0XHQvLyBJRCBvZiB0aGUgbWVzc2FnZSB0YWtlbiBmcm9tIHRoZSBtZXNzYWdlIGl0ZW0gZWxlbWVudC5cblx0XHRcdGNvbnN0IGlkID0gJGVsZW1lbnQucGFyZW50cyhzZWxlY3RvcnMuaXRlbSkuZGF0YSgnaWQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gRGVsZXRlL2hpZGUgbWVzc2FnZSBkZXBlbmRpbmcgb24gdGhlIGNsaWNrZWQgdGFyZ2V0LlxuXHRcdFx0aWYgKGRvUmVtb3ZlKSB7XG5cdFx0XHRcdF9kZWxldGVNZXNzYWdlKGlkKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdF9oaWRlTWVzc2FnZShpZCk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgY2xpY2sgZXZlbnQgb24gdGhlIGVudGlyZSBkb2N1bWVudC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uV2luZG93Q2xpY2soZXZlbnQpIHtcblx0XHRcdC8vIENsaWNrZWQgdGFyZ2V0LlxuXHRcdFx0Y29uc3QgJHRhcmdldCA9ICQoZXZlbnQudGFyZ2V0KTtcblx0XHRcdFxuXHRcdFx0Ly8gTWVzc2FnZSBsaXN0IGl0ZW0gY29udGFpbmVyLlxuXHRcdFx0Y29uc3QgJGNvbnRhaW5lciA9ICQoc2VsZWN0b3JzLmNvbnRhaW5lcik7XG5cdFx0XHRcblx0XHRcdC8vIENvbmRpdGlvbnMuXG5cdFx0XHRjb25zdCBpc0NsaWNrZWRPbkJ1dHRvbiA9ICR0aGlzLmhhcygkdGFyZ2V0KS5sZW5ndGggfHwgJHRoaXMuaXMoJHRhcmdldCkubGVuZ3RoO1xuXHRcdFx0Y29uc3QgaXNDb250YWluZXJTaG93biA9ICRjb250YWluZXIubGVuZ3RoO1xuXHRcdFx0Y29uc3QgaXNDbGlja2VkT3V0c2lkZU9mUG9wb3ZlciA9ICEkY29udGFpbmVyLmhhcygkdGFyZ2V0KS5sZW5ndGg7XG5cdFx0XHRcblx0XHRcdC8vIE9ubHkgaGlkZSBjb250YWluZXIsIGlmIGNsaWNrZWQgdGFyZ2V0IGlzIG5vdCB3aXRoaW4gdGhlIGNvbnRhaW5lciBhcmVhLlxuXHRcdFx0aWYgKGlzQ2xpY2tlZE91dHNpZGVPZlBvcG92ZXIgJiYgaXNDb250YWluZXJTaG93biAmJiAhaXNDbGlja2VkT25CdXR0b24gJiYgIWlzU2hvd25XaXRoQ2xvc2VEZWxheSkge1xuXHRcdFx0XHRfdG9nZ2xlQ29udGFpbmVyKGZhbHNlKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRml4ZXMgdGhlIGNvbnRhaW5lciBhbmQgYXJyb3cgcG9zaXRpb25zLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9maXhQb3NpdGlvbnMoKSB7XG5cdFx0XHQvLyBPZmZzZXQgY29ycmVjdGlvbiB2YWx1ZXMuXG5cdFx0XHRjb25zdCBBUlJPV19PRkZTRVQgPSAyNDA7XG5cdFx0XHRjb25zdCBQT1BPVkVSX09GRlNFVCA9IDI1MDtcblx0XHRcdFxuXHRcdFx0Ly8gTWVzc2FnZSBsaXN0IGNvbnRhaW5lciAocG9wb3ZlcikuXG5cdFx0XHRjb25zdCAkY29udGFpbmVyID0gJChzZWxlY3RvcnMuY29udGFpbmVyKTtcblx0XHRcdFxuXHRcdFx0Ly8gQXJyb3cuXG5cdFx0XHRjb25zdCAkYXJyb3cgPSAkY29udGFpbmVyLmZpbmQoc2VsZWN0b3JzLmFycm93KTtcblx0XHRcdFxuXHRcdFx0Ly8gRml4IHRoZSBvZmZzZXQgZm9yIHRoZSBhZmZlY3RlZCBlbGVtZW50cywgaWYgcG9wb3ZlciBpcyBvcGVuLlxuXHRcdFx0aWYgKCRjb250YWluZXIubGVuZ3RoKSB7XG5cdFx0XHRcdGNvbnN0IGFycm93T2Zmc2V0ID0gJGNvbnRhaW5lci5vZmZzZXQoKS5sZWZ0ICsgQVJST1dfT0ZGU0VUO1xuXHRcdFx0XHRjb25zdCBwb3BvdmVyT2Zmc2V0ID0gJHRoaXMub2Zmc2V0KCkubGVmdCAtIFBPUE9WRVJfT0ZGU0VUICsgKCR0aGlzLndpZHRoKCkgLyAyKTtcblx0XHRcdFx0XG5cdFx0XHRcdCRhcnJvdy5vZmZzZXQoe2xlZnQ6IGFycm93T2Zmc2V0fSk7XG5cdFx0XHRcdCRjb250YWluZXIub2Zmc2V0KHtsZWZ0OiBwb3BvdmVyT2Zmc2V0fSk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIHZpc2liaWxpdHkgY2hlY2tib3ggY2hhbmdlIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblZpc2liaWxpdHlDaGVja2JveENoYW5nZSgpIHtcblx0XHRcdC8vIEluZGljYXRlcyB3aGV0aGVyIHRoZSBjaGVja2JveCBpcyBjaGVja2VkLlxuXHRcdFx0Y29uc3QgaXNDaGVja2JveENoZWNrZWQgPSAkKHRoaXMpLmlzKCc6Y2hlY2tlZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBUb2dnbGUgaGlkZGVuIG1lc3NhZ2VzIGFuZCBtYXJrIHNob3duIG1lc3NhZ2VzIGFzIHJlYWQuXG5cdFx0XHRfdG9nZ2xlSGlkZGVuTWVzc2FnZUl0ZW1zKGlzQ2hlY2tib3hDaGVja2VkKTtcblx0XHRcdF9tYXJrTWVzc2FnZUl0ZW1zQXNSZWFkKCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlbGV0ZXMgYSBtZXNzYWdlIGl0ZW0gYW5kIHJlZnJlc2hlcyB0aGUgbWVzc2FnZSBpdGVtIGxpc3QuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge051bWJlcn0gaWQgTWVzc2FnZSBJRC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZGVsZXRlTWVzc2FnZShpZCkge1xuXHRcdFx0bGlicmFyeVxuXHRcdFx0XHQuZGVsZXRlQnlJZChpZClcblx0XHRcdFx0LnRoZW4oX3JlZnJlc2gpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIaWRlcyBhIG1lc3NhZ2UgaXRlbSBhbmQgcmVmcmVzaGVzIHRoZSBtZXNzYWdlIGl0ZW0gbGlzdC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7TnVtYmVyfSBpZCBNZXNzYWdlIElELlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9oaWRlTWVzc2FnZShpZCkge1xuXHRcdFx0bGlicmFyeVxuXHRcdFx0XHQuc2V0U3RhdHVzKGlkLCBsaWJyYXJ5LlNUQVRVU19ISURERU4pXG5cdFx0XHRcdC50aGVuKF9yZWZyZXNoKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmVmcmVzaGVzIHRoZSBtZXNzYWdlIGl0ZW0gbGlzdC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfcmVmcmVzaCgpIHtcblx0XHRcdC8vIE1lc3NhZ2UgbGlzdCBjb250YWluZXIuXG5cdFx0XHRjb25zdCAkY29udGFpbmVyID0gJChzZWxlY3RvcnMuY29udGFpbmVyKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBsb2FkaW5nIHNwaW5uZXIgb24gdGhlIG1lc3NhZ2UgY29udGFpbmVyLlxuXHRcdFx0Y29uc3QgJHNwaW5uZXIgPSBzcGlubmVyLnNob3coJGNvbnRhaW5lciwgOTk5OSk7XG5cdFx0XHRcblx0XHRcdC8vIFJlZnJlc2ggbGlzdC5cblx0XHRcdF9vblBvcG92ZXJTaG93bigpO1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRlIGxvYWRpbmcgc3Bpbm5lci5cblx0XHRcdHNwaW5uZXIuaGlkZSgkc3Bpbm5lcik7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJldHJpZXZlcyB0aGUgbWVzc2FnZXMgYW5kIGRpc3BsYXlzIHRoZSBwb3BvdmVyIGlmIHRoZXJlIGFyZSBuZXcgbWVzc2FnZXMuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3JlZnJlc2hTaWxlbnRseSgpIHtcblx0XHRcdC8vIFJldHJpZXZlIG1lc3NhZ2VzIGZyb20gc2VydmVyIGFuZCB1cGRhdGUgdGhlIG5vdGlmaWNhdGlvbiBjb3VudC5cblx0XHRcdC8vIFBvcG92ZXIgd2lsbCB3ZSBkaXNwbGF5ZWQgaWYgdGhlcmUgaXMgYSBuZXcgbWVzc2FnZS5cblx0XHRcdGxpYnJhcnkuZ2V0TWVzc2FnZXMoKS50aGVuKG1lc3NhZ2VzID0+IHtcblx0XHRcdFx0Ly8gU2V0IG5vdGlmaWNhdGlvbiBjb3VudC5cblx0XHRcdFx0X3NldE1lc3NhZ2VDb3VudGVyKG1lc3NhZ2VzKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEl0ZXJhdGUgb3ZlciBtZXNzYWdlcyBhbmQgdHJ5IHRvIGZpbmQgYSBtZXNzYWdlIGl0ZW0gZGVjbGFyZWQgYXMgbmV3LlxuXHRcdFx0XHQvLyBJZiBmb3VuZCwgdGhlIGNvbnRhaW5lciB3aWxsIGJlIG9wZW5lZC5cblx0XHRcdFx0Zm9yIChjb25zdCBtZXNzYWdlIG9mIG1lc3NhZ2VzKSB7XG5cdFx0XHRcdFx0aWYgKG1lc3NhZ2Uuc3RhdHVzID09PSBsaWJyYXJ5LlNUQVRVU19ORVcpIHtcblx0XHRcdFx0XHRcdC8vIEV4aXQgaW1tZWRpYXRlbHkgd2hlbiB0aGUgYW5pbWF0aW9uIHByb2Nlc3MgaXMgc3RpbGwgcnVubmluZ1xuXHRcdFx0XHRcdFx0Ly8gb3IgaWYgdGhlIG1lc3NhZ2UgbGlzdCBpdGVtIGNvbnRhaW5lciBpcyBhbHJlYWR5IHNob3duIHdpdGggYSBkZWZpbmVkIGR1cmF0aW9uLlxuXHRcdFx0XHRcdFx0aWYgKGlzQW5pbWF0aW5nIHx8IGlzU2hvd25XaXRoQ2xvc2VEZWxheSkge1xuXHRcdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdC8vIE9wZW4gbWVzc2FnZSBpdGVtIGxpc3QgY29udGFpbmVyLlxuXHRcdFx0XHRcdFx0X3RvZ2dsZUNvbnRhaW5lcih0cnVlKTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0Ly8gSW5kaWNhdGUgZGVsYXllZCBjbG9zaW5nLlxuXHRcdFx0XHRcdFx0aXNTaG93bldpdGhDbG9zZURlbGF5ID0gdHJ1ZTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0Ly8gSGlkZSBjb250YWluZXIgYW5kIGluZGljYXRlIGRlbGF5ZWQgY2xvc2luZyBmaW5pc2guXG5cdFx0XHRcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0XHRcdFx0X3RvZ2dsZUNvbnRhaW5lcihmYWxzZSk7XG5cdFx0XHRcdFx0XHRcdGlzU2hvd25XaXRoQ2xvc2VEZWxheSA9IGZhbHNlO1xuXHRcdFx0XHRcdFx0fSwgb3B0aW9ucy5jbG9zZURlbGF5KTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVG9nZ2xlcyB0aGUgbG9hZGluZyBzdGF0ZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7Qm9vbGVhbn0gaXNMb2FkaW5nIElzIHRoZSBsb2FkIHByb2Nlc3MgcnVubmluZz9cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfdG9nZ2xlTG9hZGluZyhpc0xvYWRpbmcpIHtcblx0XHRcdC8vIExvYWRpbmcgbWVzc2FnZSBjbGFzcy5cblx0XHRcdGNvbnN0IGxvYWRpbmdNZXNzYWdlQ2xhc3MgPSAnLmxvYWRpbmcnO1xuXHRcdFx0XG5cdFx0XHQvLyBNZXNzYWdlIGl0ZW0gbGlzdCBjb250YWluZXIuXG5cdFx0XHRjb25zdCAkbWVzc2FnZUxpc3QgPSAkKHNlbGVjdG9ycy5jb250YWluZXIpLmZpbmQoc2VsZWN0b3JzLmxpc3QpO1xuXHRcdFx0XG5cdFx0XHQvLyBMb2FkaW5nIG1lc3NhZ2UgZWxlbWVudC5cblx0XHRcdGNvbnN0ICRsb2FkaW5nTWVzc2FnZSA9ICRtZXNzYWdlTGlzdC5maW5kKGxvYWRpbmdNZXNzYWdlQ2xhc3MpO1xuXHRcdFx0XG5cdFx0XHQvLyBMb2FkaW5nIG1lc3NhZ2UgbWFya3VwLlxuXHRcdFx0Y29uc3QgbWFya3VwID0gYFxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiJHtfZ2V0Q2xhc3NOYW1lV2l0aG91dFBlcmlvZHMobG9hZGluZ01lc3NhZ2VDbGFzcyl9XCI+XG5cdFx0XHRcdFx0JHt0cmFuc2xhdG9yLnRyYW5zbGF0ZSgnTE9BRElORycsICdhZG1pbl9pbmZvX2JveGVzJyl9XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0YDtcblx0XHRcdFxuXHRcdFx0Ly8gUmVtb3ZlIGV4aXN0aW5nIGxvYWRpbmcgbWVzc2FnZS5cblx0XHRcdCRsb2FkaW5nTWVzc2FnZS5yZW1vdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWRkIG5ldyBsb2FkaW5nIG1lc3NhZ2UgaWYgcGFyYW1ldGVyIGlzIHRydWUuXG5cdFx0XHRpZiAoaXNMb2FkaW5nKSB7XG5cdFx0XHRcdCRtZXNzYWdlTGlzdC5hcHBlbmQoJChtYXJrdXApKVxuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvLyBNb2R1bGUgaW5pdGlhbGl6ZSBmdW5jdGlvbi5cblx0XHRtb2R1bGUuaW5pdCA9IGRvbmUgPT4ge1xuXHRcdFx0Ly8gSW5pdGlhbGl6ZSBwb3BvdmVyIHBsdWdpbiBhbmQgYXR0YWNoIGV2ZW50IGhhbmRsZXJzLlxuXHRcdFx0JHRoaXNcblx0XHRcdFx0LnBvcG92ZXIoe1xuXHRcdFx0XHRcdGFuaW1hdGlvbjogZmFsc2UsXG5cdFx0XHRcdFx0cGxhY2VtZW50OiAnYm90dG9tJyxcblx0XHRcdFx0XHRjb250ZW50OiAnICcsXG5cdFx0XHRcdFx0dHJpZ2dlcjogJ21hbnVhbCcsXG5cdFx0XHRcdFx0dGVtcGxhdGU6IF9nZXRQb3BvdmVyTWFya3VwKClcblx0XHRcdFx0fSlcblx0XHRcdFx0Lm9uKCdjbGljaycsIF9vbkNsaWNrKVxuXHRcdFx0XHQub24oJ3Nob3duLmJzLnBvcG92ZXInLCBfb25Qb3BvdmVyU2hvd24pXG5cdFx0XHRcdC5vbignc2hvdzpwb3BvdmVyJywgKCkgPT4gX3RvZ2dsZUNvbnRhaW5lcih0cnVlKSlcblx0XHRcdFx0Lm9uKCdyZWZyZXNoOm1lc3NhZ2VzJywgX3JlZnJlc2hTaWxlbnRseSk7XG5cdFx0XHRcblx0XHRcdC8vIEF0dGFjaCBldmVudCBsaXN0ZW5lcnMgdG8gdGhlIHdpbmRvdy5cblx0XHRcdCQod2luZG93KVxuXHRcdFx0XHQub24oJ3Jlc2l6ZScsIF9maXhQb3NpdGlvbnMpXG5cdFx0XHRcdC5vbignY2xpY2snLCBfb25XaW5kb3dDbGljayk7XG5cdFx0XHRcblx0XHRcdC8vIEluaXRpYWwgbWVzc2FnZSBjaGVjay5cblx0XHRcdF9yZWZyZXNoU2lsZW50bHkoKTtcblx0XHRcdFxuXHRcdFx0Ly8gRmluaXNoIGluaXRpYWxpemF0aW9uLlxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
