'use strict';

/* --------------------------------------------------------------
 state.js 2016-09-30
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles the table state for filtering, pagination and sorting.
 *
 * This controller will update the window history with the current state of the table. It reacts
 * to specific events such as filtering, pagination and sorting changes. After the window history
 * is updated the user will be able to navigate forth or backwards.
 *
 * Notice #1: This module must handle the window's pop-state events and not other modules because
 * this will lead to unnecessary code duplication and multiple AJAX requests.
 *
 * Notice #1: The window state must be always in sync with the URL for easier manipulation.
 */
gx.controllers.module('state', [jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Window History Support
  *
  * @type {Boolean}
  */
	var historySupport = jse.core.config.get('history');

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get parsed state from the URL GET parameters.
  *
  * @return {Object} Returns the table state.
  */
	function _getState() {
		return $.deparam(window.location.search.slice(1));
	}

	/**
  * Set the state to the browser's history.
  *
  * The state is stored for enabling back and forth navigation from the browser.
  *
  * @param {Object} state Contains the new table state.
  */
	function _setState(state) {
		var url = window.location.origin + window.location.pathname + '?' + $.param(state);
		window.history.pushState(state, '', url);
	}

	/**
  * Update page navigation state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} pagination Contains the DataTable pagination info.
  */
	function _onPageChange(event, pagination) {
		var state = _getState();

		state.page = pagination.page + 1;

		_setState(state);
	}

	/**
  * Update page length state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Number} length New page length.
  */
	function _onLengthChange(event, length) {
		var state = _getState();

		state.page = 1;
		state.length = length;

		_setState(state);
	}

	/**
  * Update filter state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} filter Contains the filtering values.
  */
	function _onFilterChange(event, filter) {
		var state = _getState();

		state.page = 1;
		state.filter = filter;

		_setState(state);
	}

	/**
  * Update sort state.
  *
  * @param {jQuery.Event} event jQuery event object.
  * @param {Object} sort Contains column sorting info {index, name, direction}.
  */
	function _onSortChange(event, sort) {
		var state = _getState();

		state.sort = (sort.direction === 'desc' ? '-' : '+') + sort.name;

		_setState(state);
	}

	/**
  * Set the correct table state.
  *
  * This method will parse the new popped state and apply it on the table. It must be the only place where this
  * happens in order to avoid multiple AJAX requests and data collisions.
  *
  * @param {jQuery.Event} event
  */
	function _onWindowPopState(event) {
		var state = event.originalEvent.state || {};

		if (state.page) {
			$this.find('.page-navigation select').val(state.page);
			$this.DataTable().page(parseInt(state.page) - 1);
		}

		if (state.length) {
			$this.find('.page-length select').val(state.length);
			$this.DataTable().page.len(parseInt(state.length));
		}

		if (state.sort) {
			var _$this$DataTable$init = $this.DataTable().init(),
			    columns = _$this$DataTable$init.columns;

			var direction = state.sort.charAt(0) === '-' ? 'desc' : 'asc';
			var name = state.sort.slice(1);
			var index = 1; // Default Value

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = columns[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var column = _step.value;

					if (column.name === name) {
						index = columns.indexOf(column);
						break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			$this.DataTable().order([index, direction]);
		}

		if (state.filter) {
			// Update the filtering input elements. 
			for (var _column in state.filter) {
				var value = state.filter[_column];

				if (value.constructor === Array) {
					value = value.join('||'); // Join arrays into a single string.
				}

				$this.DataTable().column(_column + ':name').search(value);
			}
		}

		$this.DataTable().draw(false);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		if (historySupport) {
			$this.on('datatable_custom_pagination:page_change', _onPageChange).on('datatable_custom_pagination:length_change', _onLengthChange).on('datatable_custom_sorting:change', _onSortChange).on('invoices_overview_filter:change', _onFilterChange);

			$(window).on('popstate', _onWindowPopState);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL292ZXJ2aWV3L3N0YXRlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiaGlzdG9yeVN1cHBvcnQiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwiX2dldFN0YXRlIiwiZGVwYXJhbSIsIndpbmRvdyIsImxvY2F0aW9uIiwic2VhcmNoIiwic2xpY2UiLCJfc2V0U3RhdGUiLCJzdGF0ZSIsInVybCIsIm9yaWdpbiIsInBhdGhuYW1lIiwicGFyYW0iLCJoaXN0b3J5IiwicHVzaFN0YXRlIiwiX29uUGFnZUNoYW5nZSIsImV2ZW50IiwicGFnaW5hdGlvbiIsInBhZ2UiLCJfb25MZW5ndGhDaGFuZ2UiLCJsZW5ndGgiLCJfb25GaWx0ZXJDaGFuZ2UiLCJmaWx0ZXIiLCJfb25Tb3J0Q2hhbmdlIiwic29ydCIsImRpcmVjdGlvbiIsIm5hbWUiLCJfb25XaW5kb3dQb3BTdGF0ZSIsIm9yaWdpbmFsRXZlbnQiLCJmaW5kIiwidmFsIiwiRGF0YVRhYmxlIiwicGFyc2VJbnQiLCJsZW4iLCJpbml0IiwiY29sdW1ucyIsImNoYXJBdCIsImluZGV4IiwiY29sdW1uIiwiaW5kZXhPZiIsIm9yZGVyIiwidmFsdWUiLCJjb25zdHJ1Y3RvciIsIkFycmF5Iiwiam9pbiIsImRyYXciLCJkb25lIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7O0FBWUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLE9BREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLGtEQUhELEVBT0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUwsU0FBUyxFQUFmOztBQUVBOzs7OztBQUtBLEtBQU1NLGlCQUFpQkwsSUFBSU0sSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixTQUFwQixDQUF2Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0MsU0FBVCxHQUFxQjtBQUNwQixTQUFPTCxFQUFFTSxPQUFGLENBQVVDLE9BQU9DLFFBQVAsQ0FBZ0JDLE1BQWhCLENBQXVCQyxLQUF2QixDQUE2QixDQUE3QixDQUFWLENBQVA7QUFDQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNDLFNBQVQsQ0FBbUJDLEtBQW5CLEVBQTBCO0FBQ3pCLE1BQU1DLE1BQU1OLE9BQU9DLFFBQVAsQ0FBZ0JNLE1BQWhCLEdBQXlCUCxPQUFPQyxRQUFQLENBQWdCTyxRQUF6QyxHQUFvRCxHQUFwRCxHQUEwRGYsRUFBRWdCLEtBQUYsQ0FBUUosS0FBUixDQUF0RTtBQUNBTCxTQUFPVSxPQUFQLENBQWVDLFNBQWYsQ0FBeUJOLEtBQXpCLEVBQWdDLEVBQWhDLEVBQW9DQyxHQUFwQztBQUNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTTSxhQUFULENBQXVCQyxLQUF2QixFQUE4QkMsVUFBOUIsRUFBMEM7QUFDekMsTUFBTVQsUUFBUVAsV0FBZDs7QUFFQU8sUUFBTVUsSUFBTixHQUFhRCxXQUFXQyxJQUFYLEdBQWtCLENBQS9COztBQUVBWCxZQUFVQyxLQUFWO0FBQ0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNXLGVBQVQsQ0FBeUJILEtBQXpCLEVBQWdDSSxNQUFoQyxFQUF3QztBQUN2QyxNQUFNWixRQUFRUCxXQUFkOztBQUVBTyxRQUFNVSxJQUFOLEdBQWEsQ0FBYjtBQUNBVixRQUFNWSxNQUFOLEdBQWVBLE1BQWY7O0FBRUFiLFlBQVVDLEtBQVY7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU2EsZUFBVCxDQUF5QkwsS0FBekIsRUFBZ0NNLE1BQWhDLEVBQXdDO0FBQ3ZDLE1BQU1kLFFBQVFQLFdBQWQ7O0FBRUFPLFFBQU1VLElBQU4sR0FBYSxDQUFiO0FBQ0FWLFFBQU1jLE1BQU4sR0FBZUEsTUFBZjs7QUFFQWYsWUFBVUMsS0FBVjtBQUNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTZSxhQUFULENBQXVCUCxLQUF2QixFQUE4QlEsSUFBOUIsRUFBb0M7QUFDbkMsTUFBTWhCLFFBQVFQLFdBQWQ7O0FBRUFPLFFBQU1nQixJQUFOLEdBQWEsQ0FBQ0EsS0FBS0MsU0FBTCxLQUFtQixNQUFuQixHQUE0QixHQUE1QixHQUFrQyxHQUFuQyxJQUEwQ0QsS0FBS0UsSUFBNUQ7O0FBRUFuQixZQUFVQyxLQUFWO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBUUEsVUFBU21CLGlCQUFULENBQTJCWCxLQUEzQixFQUFrQztBQUNqQyxNQUFNUixRQUFRUSxNQUFNWSxhQUFOLENBQW9CcEIsS0FBcEIsSUFBNkIsRUFBM0M7O0FBRUEsTUFBSUEsTUFBTVUsSUFBVixFQUFnQjtBQUNmdkIsU0FBTWtDLElBQU4sQ0FBVyx5QkFBWCxFQUFzQ0MsR0FBdEMsQ0FBMEN0QixNQUFNVSxJQUFoRDtBQUNBdkIsU0FBTW9DLFNBQU4sR0FBa0JiLElBQWxCLENBQXVCYyxTQUFTeEIsTUFBTVUsSUFBZixJQUF1QixDQUE5QztBQUNBOztBQUVELE1BQUlWLE1BQU1ZLE1BQVYsRUFBa0I7QUFDakJ6QixTQUFNa0MsSUFBTixDQUFXLHFCQUFYLEVBQWtDQyxHQUFsQyxDQUFzQ3RCLE1BQU1ZLE1BQTVDO0FBQ0F6QixTQUFNb0MsU0FBTixHQUFrQmIsSUFBbEIsQ0FBdUJlLEdBQXZCLENBQTJCRCxTQUFTeEIsTUFBTVksTUFBZixDQUEzQjtBQUNBOztBQUVELE1BQUlaLE1BQU1nQixJQUFWLEVBQWdCO0FBQUEsK0JBQ0c3QixNQUFNb0MsU0FBTixHQUFrQkcsSUFBbEIsRUFESDtBQUFBLE9BQ1JDLE9BRFEseUJBQ1JBLE9BRFE7O0FBRWYsT0FBTVYsWUFBWWpCLE1BQU1nQixJQUFOLENBQVdZLE1BQVgsQ0FBa0IsQ0FBbEIsTUFBeUIsR0FBekIsR0FBK0IsTUFBL0IsR0FBd0MsS0FBMUQ7QUFDQSxPQUFNVixPQUFPbEIsTUFBTWdCLElBQU4sQ0FBV2xCLEtBQVgsQ0FBaUIsQ0FBakIsQ0FBYjtBQUNBLE9BQUkrQixRQUFRLENBQVosQ0FKZSxDQUlBOztBQUpBO0FBQUE7QUFBQTs7QUFBQTtBQU1mLHlCQUFtQkYsT0FBbkIsOEhBQTRCO0FBQUEsU0FBbkJHLE1BQW1COztBQUMzQixTQUFJQSxPQUFPWixJQUFQLEtBQWdCQSxJQUFwQixFQUEwQjtBQUN6QlcsY0FBUUYsUUFBUUksT0FBUixDQUFnQkQsTUFBaEIsQ0FBUjtBQUNBO0FBQ0E7QUFDRDtBQVhjO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBYWYzQyxTQUFNb0MsU0FBTixHQUFrQlMsS0FBbEIsQ0FBd0IsQ0FBQ0gsS0FBRCxFQUFRWixTQUFSLENBQXhCO0FBQ0E7O0FBRUQsTUFBSWpCLE1BQU1jLE1BQVYsRUFBa0I7QUFDakI7QUFDQSxRQUFLLElBQUlnQixPQUFULElBQW1COUIsTUFBTWMsTUFBekIsRUFBaUM7QUFDaEMsUUFBSW1CLFFBQVFqQyxNQUFNYyxNQUFOLENBQWFnQixPQUFiLENBQVo7O0FBRUEsUUFBSUcsTUFBTUMsV0FBTixLQUFzQkMsS0FBMUIsRUFBaUM7QUFDaENGLGFBQVFBLE1BQU1HLElBQU4sQ0FBVyxJQUFYLENBQVIsQ0FEZ0MsQ0FDTjtBQUMxQjs7QUFFRGpELFVBQU1vQyxTQUFOLEdBQWtCTyxNQUFsQixDQUE0QkEsT0FBNUIsWUFBMkNqQyxNQUEzQyxDQUFrRG9DLEtBQWxEO0FBQ0E7QUFDRDs7QUFFRDlDLFFBQU1vQyxTQUFOLEdBQWtCYyxJQUFsQixDQUF1QixLQUF2QjtBQUVBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQXRELFFBQU8yQyxJQUFQLEdBQWMsVUFBU1ksSUFBVCxFQUFlO0FBQzVCLE1BQUlqRCxjQUFKLEVBQW9CO0FBQ25CRixTQUNFb0QsRUFERixDQUNLLHlDQURMLEVBQ2dEaEMsYUFEaEQsRUFFRWdDLEVBRkYsQ0FFSywyQ0FGTCxFQUVrRDVCLGVBRmxELEVBR0U0QixFQUhGLENBR0ssaUNBSEwsRUFHd0N4QixhQUh4QyxFQUlFd0IsRUFKRixDQUlLLGlDQUpMLEVBSXdDMUIsZUFKeEM7O0FBTUF6QixLQUFFTyxNQUFGLEVBQ0U0QyxFQURGLENBQ0ssVUFETCxFQUNpQnBCLGlCQURqQjtBQUVBOztBQUVEbUI7QUFDQSxFQWJEOztBQWVBLFFBQU92RCxNQUFQO0FBRUEsQ0FsTUYiLCJmaWxlIjoiaW52b2ljZXMvb3ZlcnZpZXcvc3RhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHN0YXRlLmpzIDIwMTYtMDktMzBcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIEhhbmRsZXMgdGhlIHRhYmxlIHN0YXRlIGZvciBmaWx0ZXJpbmcsIHBhZ2luYXRpb24gYW5kIHNvcnRpbmcuXG4gKlxuICogVGhpcyBjb250cm9sbGVyIHdpbGwgdXBkYXRlIHRoZSB3aW5kb3cgaGlzdG9yeSB3aXRoIHRoZSBjdXJyZW50IHN0YXRlIG9mIHRoZSB0YWJsZS4gSXQgcmVhY3RzXG4gKiB0byBzcGVjaWZpYyBldmVudHMgc3VjaCBhcyBmaWx0ZXJpbmcsIHBhZ2luYXRpb24gYW5kIHNvcnRpbmcgY2hhbmdlcy4gQWZ0ZXIgdGhlIHdpbmRvdyBoaXN0b3J5XG4gKiBpcyB1cGRhdGVkIHRoZSB1c2VyIHdpbGwgYmUgYWJsZSB0byBuYXZpZ2F0ZSBmb3J0aCBvciBiYWNrd2FyZHMuXG4gKlxuICogTm90aWNlICMxOiBUaGlzIG1vZHVsZSBtdXN0IGhhbmRsZSB0aGUgd2luZG93J3MgcG9wLXN0YXRlIGV2ZW50cyBhbmQgbm90IG90aGVyIG1vZHVsZXMgYmVjYXVzZVxuICogdGhpcyB3aWxsIGxlYWQgdG8gdW5uZWNlc3NhcnkgY29kZSBkdXBsaWNhdGlvbiBhbmQgbXVsdGlwbGUgQUpBWCByZXF1ZXN0cy5cbiAqXG4gKiBOb3RpY2UgIzE6IFRoZSB3aW5kb3cgc3RhdGUgbXVzdCBiZSBhbHdheXMgaW4gc3luYyB3aXRoIHRoZSBVUkwgZm9yIGVhc2llciBtYW5pcHVsYXRpb24uXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3N0YXRlJyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LWRlcGFyYW0vanF1ZXJ5LWRlcGFyYW0ubWluLmpzYFxuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBXaW5kb3cgSGlzdG9yeSBTdXBwb3J0XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7Qm9vbGVhbn1cblx0XHQgKi9cblx0XHRjb25zdCBoaXN0b3J5U3VwcG9ydCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2hpc3RvcnknKTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgcGFyc2VkIHN0YXRlIGZyb20gdGhlIFVSTCBHRVQgcGFyYW1ldGVycy5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge09iamVjdH0gUmV0dXJucyB0aGUgdGFibGUgc3RhdGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldFN0YXRlKCkge1xuXHRcdFx0cmV0dXJuICQuZGVwYXJhbSh3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNsaWNlKDEpKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2V0IHRoZSBzdGF0ZSB0byB0aGUgYnJvd3NlcidzIGhpc3RvcnkuXG5cdFx0ICpcblx0XHQgKiBUaGUgc3RhdGUgaXMgc3RvcmVkIGZvciBlbmFibGluZyBiYWNrIGFuZCBmb3J0aCBuYXZpZ2F0aW9uIGZyb20gdGhlIGJyb3dzZXIuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gc3RhdGUgQ29udGFpbnMgdGhlIG5ldyB0YWJsZSBzdGF0ZS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2V0U3RhdGUoc3RhdGUpIHtcblx0XHRcdGNvbnN0IHVybCA9IHdpbmRvdy5sb2NhdGlvbi5vcmlnaW4gKyB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUgKyAnPycgKyAkLnBhcmFtKHN0YXRlKTtcblx0XHRcdHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZShzdGF0ZSwgJycsIHVybCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZSBwYWdlIG5hdmlnYXRpb24gc3RhdGUuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdC5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gcGFnaW5hdGlvbiBDb250YWlucyB0aGUgRGF0YVRhYmxlIHBhZ2luYXRpb24gaW5mby5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25QYWdlQ2hhbmdlKGV2ZW50LCBwYWdpbmF0aW9uKSB7XG5cdFx0XHRjb25zdCBzdGF0ZSA9IF9nZXRTdGF0ZSgpO1xuXHRcdFx0XG5cdFx0XHRzdGF0ZS5wYWdlID0gcGFnaW5hdGlvbi5wYWdlICsgMTtcblx0XHRcdFxuXHRcdFx0X3NldFN0YXRlKHN0YXRlKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVXBkYXRlIHBhZ2UgbGVuZ3RoIHN0YXRlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXG5cdFx0ICogQHBhcmFtIHtOdW1iZXJ9IGxlbmd0aCBOZXcgcGFnZSBsZW5ndGguXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uTGVuZ3RoQ2hhbmdlKGV2ZW50LCBsZW5ndGgpIHtcblx0XHRcdGNvbnN0IHN0YXRlID0gX2dldFN0YXRlKCk7XG5cdFx0XHRcblx0XHRcdHN0YXRlLnBhZ2UgPSAxO1xuXHRcdFx0c3RhdGUubGVuZ3RoID0gbGVuZ3RoO1xuXHRcdFx0XG5cdFx0XHRfc2V0U3RhdGUoc3RhdGUpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBVcGRhdGUgZmlsdGVyIHN0YXRlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGZpbHRlciBDb250YWlucyB0aGUgZmlsdGVyaW5nIHZhbHVlcy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25GaWx0ZXJDaGFuZ2UoZXZlbnQsIGZpbHRlcikge1xuXHRcdFx0Y29uc3Qgc3RhdGUgPSBfZ2V0U3RhdGUoKTtcblx0XHRcdFxuXHRcdFx0c3RhdGUucGFnZSA9IDE7XG5cdFx0XHRzdGF0ZS5maWx0ZXIgPSBmaWx0ZXI7XG5cdFx0XHRcblx0XHRcdF9zZXRTdGF0ZShzdGF0ZSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZSBzb3J0IHN0YXRlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHNvcnQgQ29udGFpbnMgY29sdW1uIHNvcnRpbmcgaW5mbyB7aW5kZXgsIG5hbWUsIGRpcmVjdGlvbn0uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uU29ydENoYW5nZShldmVudCwgc29ydCkge1xuXHRcdFx0Y29uc3Qgc3RhdGUgPSBfZ2V0U3RhdGUoKTtcblx0XHRcdFxuXHRcdFx0c3RhdGUuc29ydCA9IChzb3J0LmRpcmVjdGlvbiA9PT0gJ2Rlc2MnID8gJy0nIDogJysnKSArIHNvcnQubmFtZTtcblx0XHRcdFxuXHRcdFx0X3NldFN0YXRlKHN0YXRlKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2V0IHRoZSBjb3JyZWN0IHRhYmxlIHN0YXRlLlxuXHRcdCAqXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCBwYXJzZSB0aGUgbmV3IHBvcHBlZCBzdGF0ZSBhbmQgYXBwbHkgaXQgb24gdGhlIHRhYmxlLiBJdCBtdXN0IGJlIHRoZSBvbmx5IHBsYWNlIHdoZXJlIHRoaXNcblx0XHQgKiBoYXBwZW5zIGluIG9yZGVyIHRvIGF2b2lkIG11bHRpcGxlIEFKQVggcmVxdWVzdHMgYW5kIGRhdGEgY29sbGlzaW9ucy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbldpbmRvd1BvcFN0YXRlKGV2ZW50KSB7XG5cdFx0XHRjb25zdCBzdGF0ZSA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQuc3RhdGUgfHwge307XG5cdFx0XHRcblx0XHRcdGlmIChzdGF0ZS5wYWdlKSB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5wYWdlLW5hdmlnYXRpb24gc2VsZWN0JykudmFsKHN0YXRlLnBhZ2UpO1xuXHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5wYWdlKHBhcnNlSW50KHN0YXRlLnBhZ2UpIC0gMSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmIChzdGF0ZS5sZW5ndGgpIHtcblx0XHRcdFx0JHRoaXMuZmluZCgnLnBhZ2UtbGVuZ3RoIHNlbGVjdCcpLnZhbChzdGF0ZS5sZW5ndGgpO1xuXHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5wYWdlLmxlbihwYXJzZUludChzdGF0ZS5sZW5ndGgpKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKHN0YXRlLnNvcnQpIHtcblx0XHRcdFx0Y29uc3Qge2NvbHVtbnN9ID0gJHRoaXMuRGF0YVRhYmxlKCkuaW5pdCgpO1xuXHRcdFx0XHRjb25zdCBkaXJlY3Rpb24gPSBzdGF0ZS5zb3J0LmNoYXJBdCgwKSA9PT0gJy0nID8gJ2Rlc2MnIDogJ2FzYyc7XG5cdFx0XHRcdGNvbnN0IG5hbWUgPSBzdGF0ZS5zb3J0LnNsaWNlKDEpO1xuXHRcdFx0XHRsZXQgaW5kZXggPSAxOyAvLyBEZWZhdWx0IFZhbHVlXG5cdFx0XHRcdFxuXHRcdFx0XHRmb3IgKGxldCBjb2x1bW4gb2YgY29sdW1ucykge1xuXHRcdFx0XHRcdGlmIChjb2x1bW4ubmFtZSA9PT0gbmFtZSkge1xuXHRcdFx0XHRcdFx0aW5kZXggPSBjb2x1bW5zLmluZGV4T2YoY29sdW1uKTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkub3JkZXIoW2luZGV4LCBkaXJlY3Rpb25dKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKHN0YXRlLmZpbHRlcikge1xuXHRcdFx0XHQvLyBVcGRhdGUgdGhlIGZpbHRlcmluZyBpbnB1dCBlbGVtZW50cy4gXG5cdFx0XHRcdGZvciAobGV0IGNvbHVtbiBpbiBzdGF0ZS5maWx0ZXIpIHtcblx0XHRcdFx0XHRsZXQgdmFsdWUgPSBzdGF0ZS5maWx0ZXJbY29sdW1uXTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAodmFsdWUuY29uc3RydWN0b3IgPT09IEFycmF5KSB7XG5cdFx0XHRcdFx0XHR2YWx1ZSA9IHZhbHVlLmpvaW4oJ3x8Jyk7IC8vIEpvaW4gYXJyYXlzIGludG8gYSBzaW5nbGUgc3RyaW5nLlxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oYCR7Y29sdW1ufTpuYW1lYCkuc2VhcmNoKHZhbHVlKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5kcmF3KGZhbHNlKTtcblx0XHRcdFxuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0aWYgKGhpc3RvcnlTdXBwb3J0KSB7XG5cdFx0XHRcdCR0aGlzXG5cdFx0XHRcdFx0Lm9uKCdkYXRhdGFibGVfY3VzdG9tX3BhZ2luYXRpb246cGFnZV9jaGFuZ2UnLCBfb25QYWdlQ2hhbmdlKVxuXHRcdFx0XHRcdC5vbignZGF0YXRhYmxlX2N1c3RvbV9wYWdpbmF0aW9uOmxlbmd0aF9jaGFuZ2UnLCBfb25MZW5ndGhDaGFuZ2UpXG5cdFx0XHRcdFx0Lm9uKCdkYXRhdGFibGVfY3VzdG9tX3NvcnRpbmc6Y2hhbmdlJywgX29uU29ydENoYW5nZSlcblx0XHRcdFx0XHQub24oJ2ludm9pY2VzX292ZXJ2aWV3X2ZpbHRlcjpjaGFuZ2UnLCBfb25GaWx0ZXJDaGFuZ2UpO1xuXHRcdFx0XHRcblx0XHRcdFx0JCh3aW5kb3cpXG5cdFx0XHRcdFx0Lm9uKCdwb3BzdGF0ZScsIF9vbldpbmRvd1BvcFN0YXRlKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0XHRcblx0fSk7Il19
