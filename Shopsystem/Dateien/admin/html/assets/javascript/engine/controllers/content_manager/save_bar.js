'use strict';

/* --------------------------------------------------------------
 save_bar.js 2017-11-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('save_bar', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------
	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var _this = this;

	var $this = $(this);

	/**
  * Bottom save bar
  *
  * @type {jQuery}
  */
	var $bottomSaveBar = $('.bottom-save-bar');

	/**
  * Save button
  *
  * @type {jQuery}
  */
	var $saveButton = $bottomSaveBar.find('button[name="save"]');

	/**
  * Update button
  *
  * @type {jQuery}
  */
	var $updateButton = $bottomSaveBar.find('button[name="update"]');

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	// Handle save button click
	$saveButton.on('click', function (event) {
		event.preventDefault();
		var formName = $(_this).attr('form');
		var $form = $('form#' + formName);

		$form.submit();
	});

	// Handle update button click
	$updateButton.on('click', function (event) {
		event.preventDefault();
		var formName = $(_this).attr('form');
		var $form = $('form#' + formName);
		var formURL = $form.attr('action');

		// Add another parameter, so that we stay on the same page after
		// the form was submitted
		$form.attr('action', formURL + '&update=1');

		$form.submit();
	});

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRfbWFuYWdlci9zYXZlX2Jhci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRib3R0b21TYXZlQmFyIiwiJHNhdmVCdXR0b24iLCJmaW5kIiwiJHVwZGF0ZUJ1dHRvbiIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIm9uIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsImZvcm1OYW1lIiwiYXR0ciIsIiRmb3JtIiwic3VibWl0IiwiZm9ybVVSTCIsImluaXQiLCJkb25lIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUF1QixVQUF2QixFQUFtQyxFQUFuQyxFQUVDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7QUFDQzs7Ozs7O0FBUGE7O0FBWWIsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsaUJBQWlCRCxFQUFFLGtCQUFGLENBQXZCOztBQUVBOzs7OztBQUtBLEtBQU1FLGNBQWNELGVBQWVFLElBQWYsQ0FBb0IscUJBQXBCLENBQXBCOztBQUVEOzs7OztBQUtDLEtBQU1DLGdCQUFnQkgsZUFBZUUsSUFBZixDQUFvQix1QkFBcEIsQ0FBdEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUUsV0FBVyxFQUFqQjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxVQUFVTixFQUFFTyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCUCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRCxTQUFTLEVBQWY7O0FBRUQ7QUFDQTtBQUNBOztBQUVBO0FBQ0FLLGFBQVlNLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFVBQUNDLEtBQUQsRUFBVztBQUNsQ0EsUUFBTUMsY0FBTjtBQUNBLE1BQU1DLFdBQVdYLEVBQUUsS0FBRixFQUFRWSxJQUFSLENBQWEsTUFBYixDQUFqQjtBQUNBLE1BQU1DLFFBQVFiLEVBQUUsVUFBVVcsUUFBWixDQUFkOztBQUVBRSxRQUFNQyxNQUFOO0FBQ0EsRUFORDs7QUFRQTtBQUNBVixlQUFjSSxFQUFkLENBQWlCLE9BQWpCLEVBQTBCLFVBQUNDLEtBQUQsRUFBVztBQUNwQ0EsUUFBTUMsY0FBTjtBQUNBLE1BQU1DLFdBQVdYLEVBQUUsS0FBRixFQUFRWSxJQUFSLENBQWEsTUFBYixDQUFqQjtBQUNBLE1BQU1DLFFBQVFiLEVBQUUsVUFBVVcsUUFBWixDQUFkO0FBQ0EsTUFBTUksVUFBVUYsTUFBTUQsSUFBTixDQUFXLFFBQVgsQ0FBaEI7O0FBRUE7QUFDQTtBQUNBQyxRQUFNRCxJQUFOLENBQVcsUUFBWCxFQUFxQkcsVUFBVSxXQUEvQjs7QUFFQUYsUUFBTUMsTUFBTjtBQUNBLEVBWEQ7O0FBYUE7QUFDQTtBQUNBOztBQUVBakIsUUFBT21CLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJBO0FBQ0EsRUFGRDs7QUFJQSxRQUFPcEIsTUFBUDtBQUNBLENBOUZGIiwiZmlsZSI6ImNvbnRlbnRfbWFuYWdlci9zYXZlX2Jhci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBzYXZlX2Jhci5qcyAyMDE3LTExLTA2XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuZ3guY29udHJvbGxlcnMubW9kdWxlKCAnc2F2ZV9iYXInLCBbXSxcclxuXHRcclxuXHRmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcclxuXHRcdCd1c2Ugc3RyaWN0JztcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQgLyoqXHJcblx0XHQgICogTW9kdWxlIFNlbGVjdG9yXHJcblx0XHQgICpcclxuXHRcdCAgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdFx0ICAqL1xyXG5cdFx0IGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdFxyXG5cdFx0IC8qKlxyXG5cdFx0ICAqIEJvdHRvbSBzYXZlIGJhclxyXG5cdFx0ICAqXHJcblx0XHQgICogQHR5cGUge2pRdWVyeX1cclxuXHRcdCAgKi9cclxuXHRcdCBjb25zdCAkYm90dG9tU2F2ZUJhciA9ICQoJy5ib3R0b20tc2F2ZS1iYXInKTtcclxuXHRcdFxyXG5cdFx0IC8qKlxyXG5cdFx0ICAqIFNhdmUgYnV0dG9uXHJcblx0XHQgICpcclxuXHRcdCAgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdFx0ICAqL1xyXG5cdFx0IGNvbnN0ICRzYXZlQnV0dG9uID0gJGJvdHRvbVNhdmVCYXIuZmluZCgnYnV0dG9uW25hbWU9XCJzYXZlXCJdJyk7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogVXBkYXRlIGJ1dHRvblxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0XHQgKi9cclxuXHRcdFx0Y29uc3QgJHVwZGF0ZUJ1dHRvbiA9ICRib3R0b21TYXZlQmFyLmZpbmQoJ2J1dHRvbltuYW1lPVwidXBkYXRlXCJdJyk7XHJcblx0XHRcdFxyXG5cdFx0XHQvKipcclxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXHJcblx0XHRcdCAqXHJcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XHJcblx0XHRcdCAqL1xyXG5cdFx0XHRjb25zdCBkZWZhdWx0cyA9IHt9O1xyXG5cdFx0XHRcclxuXHRcdFx0LyoqXHJcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcclxuXHRcdFx0ICpcclxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cclxuXHRcdFx0ICovXHJcblx0XHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xyXG5cdFx0XHRcclxuXHRcdFx0LyoqXHJcblx0XHRcdCAqIE1vZHVsZSBPYmplY3RcclxuXHRcdFx0ICpcclxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cclxuXHRcdFx0ICovXHJcblx0XHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0Ly8gSGFuZGxlIHNhdmUgYnV0dG9uIGNsaWNrXHJcblx0XHQkc2F2ZUJ1dHRvbi5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcclxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0Y29uc3QgZm9ybU5hbWUgPSAkKHRoaXMpLmF0dHIoJ2Zvcm0nKTtcclxuXHRcdFx0Y29uc3QgJGZvcm0gPSAkKCdmb3JtIycgKyBmb3JtTmFtZSk7XHJcblx0XHRcdFxyXG5cdFx0XHQkZm9ybS5zdWJtaXQoKTtcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHQvLyBIYW5kbGUgdXBkYXRlIGJ1dHRvbiBjbGlja1xyXG5cdFx0JHVwZGF0ZUJ1dHRvbi5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcclxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0Y29uc3QgZm9ybU5hbWUgPSAkKHRoaXMpLmF0dHIoJ2Zvcm0nKTtcclxuXHRcdFx0Y29uc3QgJGZvcm0gPSAkKCdmb3JtIycgKyBmb3JtTmFtZSk7XHJcblx0XHRcdGNvbnN0IGZvcm1VUkwgPSAkZm9ybS5hdHRyKCdhY3Rpb24nKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEFkZCBhbm90aGVyIHBhcmFtZXRlciwgc28gdGhhdCB3ZSBzdGF5IG9uIHRoZSBzYW1lIHBhZ2UgYWZ0ZXJcclxuXHRcdFx0Ly8gdGhlIGZvcm0gd2FzIHN1Ym1pdHRlZFxyXG5cdFx0XHQkZm9ybS5hdHRyKCdhY3Rpb24nLCBmb3JtVVJMICsgJyZ1cGRhdGU9MScpO1xyXG5cdFx0XHRcclxuXHRcdFx0JGZvcm0uc3VibWl0KCk7XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0XHRkb25lKCk7XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gbW9kdWxlO1xyXG5cdH0pO1xyXG4iXX0=
