'use strict';

/* --------------------------------------------------------------
 init.js 2016-10-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Invoices Table Controller
 *
 * This controller initializes the main invoices table with a new jQuery DataTables instance.
 */
gx.controllers.module('init', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', jse.source + '/vendor/momentjs/moment.min.js', gx.source + '/libs/invoices_overview_columns', gx.source + '/libs/search', 'datatable', 'modal', 'user_configuration_service'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Initial Table Order
  *
  * @param {Object} parameters Contains the URL parameters.
  * @param {Object} columns Contains the column definitions.
  *
  * @return {Array[]}
  */
	function _getOrder(parameters, columns) {
		var index = 1; // Order by first column by default.
		var direction = 'desc'; // Order DESC by default. 

		// Apply initial table sort. 
		if (parameters.sort) {
			direction = parameters.sort.charAt(0) === '-' ? 'desc' : 'asc';
			var columnName = parameters.sort.slice(1);

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = columns[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var column = _step.value;

					if (column.name === columnName) {
						index = columns.indexOf(column);
						break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		} else if (data.activeColumns.indexOf('invoiceDate') > -1) {
			// Order by date if possible.
			index = data.activeColumns.indexOf('invoiceDate');
		}

		return [[index, direction]];
	}

	/**
  * Get Initial Search Cols
  *
  * @param {Object} parameters Contains the URL parameters.
  *
  * @returns {Object[]} Returns the initial filtering values.
  */
	function _getSearchCols(parameters, columns) {
		if (!parameters.filter) {
			return [];
		}

		var searchCols = [];

		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			for (var _iterator2 = columns[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				var column = _step2.value;

				var entry = null;
				var value = parameters.filter[column.name];

				if (value) {
					entry = { search: decodeURIComponent(value) };
				}

				searchCols.push(entry);
			}
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		return searchCols;
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		var columns = jse.libs.datatable.prepareColumns($this, jse.libs.invoices_overview_columns, data.activeColumns);
		var parameters = $.deparam(window.location.search.slice(1));
		var pageLength = parseInt(parameters.length || data.pageLength);

		jse.libs.datatable.create($this, {
			autoWidth: false,
			dom: 't',
			pageLength: pageLength,
			displayStart: parseInt(parameters.page) ? (parseInt(parameters.page) - 1) * pageLength : 0,
			serverSide: true,
			language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
			ajax: {
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=InvoicesOverviewAjax/DataTable',
				type: 'POST',
				data: {
					pageToken: jse.core.config.get('pageToken')
				}
			},
			orderCellsTop: true,
			order: _getOrder(parameters, columns),
			searchCols: _getSearchCols(parameters, columns),
			columns: columns
		});

		// Add table error handler.
		jse.libs.datatable.error($this, function (event, settings, techNote, message) {
			var title = 'DataTables ' + jse.core.lang.translate('error', 'messages');
			jse.libs.modal.showMessage(title, message);
		});

		// Add pagination change handler. 
		$this.on('datatable_custom_pagination:length_change', function (event, newPageLength) {
			jse.libs.user_configuration_service.set({
				data: {
					userId: jse.core.registry.get('userId'),
					configurationKey: 'invoicesOverviewPageLength',
					configurationValue: newPageLength
				}
			});
		});

		// Add draw event handler. 
		$this.on('draw.dt', function () {
			$this.find('thead input:checkbox').prop('checked', false).trigger('change', [false]); // No need to update the tbody checkboxes (event.js).
			$this.find('tbody').attr('data-gx-widget', 'single_checkbox');
			gx.widgets.init($this); // Initialize the checkbox widget.
		});

		// Set admin search value.
		if (Object.keys(parameters).includes('filter') && Object.keys(parameters.filter).includes('invoiceNumber')) {
			jse.libs.search.setValue(parameters.filter.invoiceNumber, true);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL292ZXJ2aWV3L2luaXQuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJfZ2V0T3JkZXIiLCJwYXJhbWV0ZXJzIiwiY29sdW1ucyIsImluZGV4IiwiZGlyZWN0aW9uIiwic29ydCIsImNoYXJBdCIsImNvbHVtbk5hbWUiLCJzbGljZSIsImNvbHVtbiIsIm5hbWUiLCJpbmRleE9mIiwiYWN0aXZlQ29sdW1ucyIsIl9nZXRTZWFyY2hDb2xzIiwiZmlsdGVyIiwic2VhcmNoQ29scyIsImVudHJ5IiwidmFsdWUiLCJzZWFyY2giLCJkZWNvZGVVUklDb21wb25lbnQiLCJwdXNoIiwiaW5pdCIsImRvbmUiLCJsaWJzIiwiZGF0YXRhYmxlIiwicHJlcGFyZUNvbHVtbnMiLCJpbnZvaWNlc19vdmVydmlld19jb2x1bW5zIiwiZGVwYXJhbSIsIndpbmRvdyIsImxvY2F0aW9uIiwicGFnZUxlbmd0aCIsInBhcnNlSW50IiwibGVuZ3RoIiwiY3JlYXRlIiwiYXV0b1dpZHRoIiwiZG9tIiwiZGlzcGxheVN0YXJ0IiwicGFnZSIsInNlcnZlclNpZGUiLCJsYW5ndWFnZSIsImdldFRyYW5zbGF0aW9ucyIsImNvcmUiLCJjb25maWciLCJnZXQiLCJhamF4IiwidXJsIiwidHlwZSIsInBhZ2VUb2tlbiIsIm9yZGVyQ2VsbHNUb3AiLCJvcmRlciIsImVycm9yIiwiZXZlbnQiLCJzZXR0aW5ncyIsInRlY2hOb3RlIiwibWVzc2FnZSIsInRpdGxlIiwibGFuZyIsInRyYW5zbGF0ZSIsIm1vZGFsIiwic2hvd01lc3NhZ2UiLCJvbiIsIm5ld1BhZ2VMZW5ndGgiLCJ1c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSIsInNldCIsInVzZXJJZCIsInJlZ2lzdHJ5IiwiY29uZmlndXJhdGlvbktleSIsImNvbmZpZ3VyYXRpb25WYWx1ZSIsImZpbmQiLCJwcm9wIiwidHJpZ2dlciIsImF0dHIiLCJ3aWRnZXRzIiwiT2JqZWN0Iiwia2V5cyIsImluY2x1ZGVzIiwic2V0VmFsdWUiLCJpbnZvaWNlTnVtYmVyIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLE1BREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLG1EQUVJRCxJQUFJQyxNQUZSLGtEQUdJRCxJQUFJQyxNQUhSLHFDQUlJSixHQUFHSSxNQUpQLHNDQUtJSixHQUFHSSxNQUxQLG1CQU1DLFdBTkQsRUFPQyxPQVBELEVBUUMsNEJBUkQsQ0FIRCxFQWNDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1MLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FBUUEsVUFBU00sU0FBVCxDQUFtQkMsVUFBbkIsRUFBK0JDLE9BQS9CLEVBQXdDO0FBQ3ZDLE1BQUlDLFFBQVEsQ0FBWixDQUR1QyxDQUN4QjtBQUNmLE1BQUlDLFlBQVksTUFBaEIsQ0FGdUMsQ0FFZjs7QUFFeEI7QUFDQSxNQUFJSCxXQUFXSSxJQUFmLEVBQXFCO0FBQ3BCRCxlQUFZSCxXQUFXSSxJQUFYLENBQWdCQyxNQUFoQixDQUF1QixDQUF2QixNQUE4QixHQUE5QixHQUFvQyxNQUFwQyxHQUE2QyxLQUF6RDtBQUNBLE9BQU1DLGFBQWNOLFdBQVdJLElBQVgsQ0FBZ0JHLEtBQWhCLENBQXNCLENBQXRCLENBQXBCOztBQUZvQjtBQUFBO0FBQUE7O0FBQUE7QUFJcEIseUJBQW1CTixPQUFuQiw4SEFBNEI7QUFBQSxTQUFuQk8sTUFBbUI7O0FBQzNCLFNBQUlBLE9BQU9DLElBQVAsS0FBZ0JILFVBQXBCLEVBQWdDO0FBQy9CSixjQUFRRCxRQUFRUyxPQUFSLENBQWdCRixNQUFoQixDQUFSO0FBQ0E7QUFDQTtBQUNEO0FBVG1CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVcEIsR0FWRCxNQVVPLElBQUlaLEtBQUtlLGFBQUwsQ0FBbUJELE9BQW5CLENBQTJCLGFBQTNCLElBQTRDLENBQUMsQ0FBakQsRUFBb0Q7QUFBRTtBQUM1RFIsV0FBUU4sS0FBS2UsYUFBTCxDQUFtQkQsT0FBbkIsQ0FBMkIsYUFBM0IsQ0FBUjtBQUNBOztBQUVELFNBQU8sQ0FBQyxDQUFDUixLQUFELEVBQVFDLFNBQVIsQ0FBRCxDQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTUyxjQUFULENBQXdCWixVQUF4QixFQUFvQ0MsT0FBcEMsRUFBNkM7QUFDNUMsTUFBSSxDQUFDRCxXQUFXYSxNQUFoQixFQUF3QjtBQUN2QixVQUFPLEVBQVA7QUFDQTs7QUFFRCxNQUFNQyxhQUFhLEVBQW5COztBQUw0QztBQUFBO0FBQUE7O0FBQUE7QUFPNUMseUJBQW1CYixPQUFuQixtSUFBNEI7QUFBQSxRQUFuQk8sTUFBbUI7O0FBQzNCLFFBQUlPLFFBQVEsSUFBWjtBQUNBLFFBQUlDLFFBQVFoQixXQUFXYSxNQUFYLENBQWtCTCxPQUFPQyxJQUF6QixDQUFaOztBQUVBLFFBQUlPLEtBQUosRUFBVztBQUNWRCxhQUFRLEVBQUVFLFFBQVFDLG1CQUFtQkYsS0FBbkIsQ0FBVixFQUFSO0FBQ0E7O0FBRURGLGVBQVdLLElBQVgsQ0FBZ0JKLEtBQWhCO0FBQ0E7QUFoQjJDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBa0I1QyxTQUFPRCxVQUFQO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBckIsUUFBTzJCLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUIsTUFBTXBCLFVBQVVQLElBQUk0QixJQUFKLENBQVNDLFNBQVQsQ0FBbUJDLGNBQW5CLENBQWtDM0IsS0FBbEMsRUFBeUNILElBQUk0QixJQUFKLENBQVNHLHlCQUFsRCxFQUNmN0IsS0FBS2UsYUFEVSxDQUFoQjtBQUVBLE1BQU1YLGFBQWFGLEVBQUU0QixPQUFGLENBQVVDLE9BQU9DLFFBQVAsQ0FBZ0JYLE1BQWhCLENBQXVCVixLQUF2QixDQUE2QixDQUE3QixDQUFWLENBQW5CO0FBQ0EsTUFBTXNCLGFBQWFDLFNBQVM5QixXQUFXK0IsTUFBWCxJQUFxQm5DLEtBQUtpQyxVQUFuQyxDQUFuQjs7QUFFQW5DLE1BQUk0QixJQUFKLENBQVNDLFNBQVQsQ0FBbUJTLE1BQW5CLENBQTBCbkMsS0FBMUIsRUFBaUM7QUFDaENvQyxjQUFXLEtBRHFCO0FBRWhDQyxRQUFLLEdBRjJCO0FBR2hDTCx5QkFIZ0M7QUFJaENNLGlCQUFjTCxTQUFTOUIsV0FBV29DLElBQXBCLElBQTRCLENBQUNOLFNBQVM5QixXQUFXb0MsSUFBcEIsSUFBNEIsQ0FBN0IsSUFBa0NQLFVBQTlELEdBQTJFLENBSnpEO0FBS2hDUSxlQUFZLElBTG9CO0FBTWhDQyxhQUFVNUMsSUFBSTRCLElBQUosQ0FBU0MsU0FBVCxDQUFtQmdCLGVBQW5CLENBQW1DN0MsSUFBSThDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsY0FBcEIsQ0FBbkMsQ0FOc0I7QUFPaENDLFNBQU07QUFDTEMsU0FBS2xELElBQUk4QyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLG9EQURoQztBQUVMRyxVQUFNLE1BRkQ7QUFHTGpELFVBQU07QUFDTGtELGdCQUFXcEQsSUFBSThDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFETjtBQUhELElBUDBCO0FBY2hDSyxrQkFBZSxJQWRpQjtBQWVoQ0MsVUFBT2pELFVBQVVDLFVBQVYsRUFBc0JDLE9BQXRCLENBZnlCO0FBZ0JoQ2EsZUFBWUYsZUFBZVosVUFBZixFQUEyQkMsT0FBM0IsQ0FoQm9CO0FBaUJoQ0E7QUFqQmdDLEdBQWpDOztBQW9CQTtBQUNBUCxNQUFJNEIsSUFBSixDQUFTQyxTQUFULENBQW1CMEIsS0FBbkIsQ0FBeUJwRCxLQUF6QixFQUFnQyxVQUFTcUQsS0FBVCxFQUFnQkMsUUFBaEIsRUFBMEJDLFFBQTFCLEVBQW9DQyxPQUFwQyxFQUE2QztBQUM1RSxPQUFNQyxRQUFRLGdCQUFnQjVELElBQUk4QyxJQUFKLENBQVNlLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxVQUFqQyxDQUE5QjtBQUNBOUQsT0FBSTRCLElBQUosQ0FBU21DLEtBQVQsQ0FBZUMsV0FBZixDQUEyQkosS0FBM0IsRUFBa0NELE9BQWxDO0FBQ0EsR0FIRDs7QUFLQTtBQUNBeEQsUUFBTThELEVBQU4sQ0FBUywyQ0FBVCxFQUFzRCxVQUFTVCxLQUFULEVBQWdCVSxhQUFoQixFQUErQjtBQUNwRmxFLE9BQUk0QixJQUFKLENBQVN1QywwQkFBVCxDQUFvQ0MsR0FBcEMsQ0FBd0M7QUFDdkNsRSxVQUFNO0FBQ0xtRSxhQUFRckUsSUFBSThDLElBQUosQ0FBU3dCLFFBQVQsQ0FBa0J0QixHQUFsQixDQUFzQixRQUF0QixDQURIO0FBRUx1Qix1QkFBa0IsNEJBRmI7QUFHTEMseUJBQW9CTjtBQUhmO0FBRGlDLElBQXhDO0FBT0EsR0FSRDs7QUFVQTtBQUNBL0QsUUFBTThELEVBQU4sQ0FBUyxTQUFULEVBQW9CLFlBQU07QUFDekI5RCxTQUFNc0UsSUFBTixDQUFXLHNCQUFYLEVBQ0VDLElBREYsQ0FDTyxTQURQLEVBQ2tCLEtBRGxCLEVBRUVDLE9BRkYsQ0FFVSxRQUZWLEVBRW9CLENBQUMsS0FBRCxDQUZwQixFQUR5QixDQUdLO0FBQzlCeEUsU0FBTXNFLElBQU4sQ0FBVyxPQUFYLEVBQW9CRyxJQUFwQixDQUF5QixnQkFBekIsRUFBMkMsaUJBQTNDO0FBQ0EvRSxNQUFHZ0YsT0FBSCxDQUFXbkQsSUFBWCxDQUFnQnZCLEtBQWhCLEVBTHlCLENBS0Q7QUFDeEIsR0FORDs7QUFRQTtBQUNBLE1BQUkyRSxPQUFPQyxJQUFQLENBQVl6RSxVQUFaLEVBQXdCMEUsUUFBeEIsQ0FBaUMsUUFBakMsS0FBOENGLE9BQU9DLElBQVAsQ0FBWXpFLFdBQVdhLE1BQXZCLEVBQStCNkQsUUFBL0IsQ0FBd0MsZUFBeEMsQ0FBbEQsRUFBNEc7QUFDM0doRixPQUFJNEIsSUFBSixDQUFTTCxNQUFULENBQWdCMEQsUUFBaEIsQ0FBeUIzRSxXQUFXYSxNQUFYLENBQWtCK0QsYUFBM0MsRUFBMEQsSUFBMUQ7QUFDQTs7QUFFRHZEO0FBQ0EsRUExREQ7O0FBNERBLFFBQU81QixNQUFQO0FBQ0EsQ0FuS0YiLCJmaWxlIjoiaW52b2ljZXMvb3ZlcnZpZXcvaW5pdC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBpbml0LmpzIDIwMTYtMTAtMTFcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogSW52b2ljZXMgVGFibGUgQ29udHJvbGxlclxyXG4gKlxyXG4gKiBUaGlzIGNvbnRyb2xsZXIgaW5pdGlhbGl6ZXMgdGhlIG1haW4gaW52b2ljZXMgdGFibGUgd2l0aCBhIG5ldyBqUXVlcnkgRGF0YVRhYmxlcyBpbnN0YW5jZS5cclxuICovXHJcbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcclxuXHQnaW5pdCcsXHJcblx0XHJcblx0W1xyXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmNzc2AsXHJcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvZGF0YXRhYmxlcy9qcXVlcnkuZGF0YVRhYmxlcy5taW4uanNgLFxyXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL21vbWVudGpzL21vbWVudC5taW4uanNgLFxyXG5cdFx0YCR7Z3guc291cmNlfS9saWJzL2ludm9pY2VzX292ZXJ2aWV3X2NvbHVtbnNgLFxyXG5cdFx0YCR7Z3guc291cmNlfS9saWJzL3NlYXJjaGAsXHJcblx0XHQnZGF0YXRhYmxlJyxcclxuXHRcdCdtb2RhbCcsXHJcblx0XHQndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnXHJcblx0XSxcclxuXHRcclxuXHRmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcclxuXHRcdCd1c2Ugc3RyaWN0JztcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBWQVJJQUJMRVNcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBGVU5DVElPTlNcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIEdldCBJbml0aWFsIFRhYmxlIE9yZGVyXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtZXRlcnMgQ29udGFpbnMgdGhlIFVSTCBwYXJhbWV0ZXJzLlxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGNvbHVtbnMgQ29udGFpbnMgdGhlIGNvbHVtbiBkZWZpbml0aW9ucy5cclxuXHRcdCAqXHJcblx0XHQgKiBAcmV0dXJuIHtBcnJheVtdfVxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfZ2V0T3JkZXIocGFyYW1ldGVycywgY29sdW1ucykge1xyXG5cdFx0XHRsZXQgaW5kZXggPSAxOyAvLyBPcmRlciBieSBmaXJzdCBjb2x1bW4gYnkgZGVmYXVsdC5cclxuXHRcdFx0bGV0IGRpcmVjdGlvbiA9ICdkZXNjJzsgLy8gT3JkZXIgREVTQyBieSBkZWZhdWx0LiBcclxuXHRcdFx0XHJcblx0XHRcdC8vIEFwcGx5IGluaXRpYWwgdGFibGUgc29ydC4gXHJcblx0XHRcdGlmIChwYXJhbWV0ZXJzLnNvcnQpIHtcclxuXHRcdFx0XHRkaXJlY3Rpb24gPSBwYXJhbWV0ZXJzLnNvcnQuY2hhckF0KDApID09PSAnLScgPyAnZGVzYycgOiAnYXNjJztcclxuXHRcdFx0XHRjb25zdCBjb2x1bW5OYW1lICA9IHBhcmFtZXRlcnMuc29ydC5zbGljZSgxKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRmb3IgKGxldCBjb2x1bW4gb2YgY29sdW1ucykge1xyXG5cdFx0XHRcdFx0aWYgKGNvbHVtbi5uYW1lID09PSBjb2x1bW5OYW1lKSB7XHJcblx0XHRcdFx0XHRcdGluZGV4ID0gY29sdW1ucy5pbmRleE9mKGNvbHVtbik7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSBlbHNlIGlmIChkYXRhLmFjdGl2ZUNvbHVtbnMuaW5kZXhPZignaW52b2ljZURhdGUnKSA+IC0xKSB7IC8vIE9yZGVyIGJ5IGRhdGUgaWYgcG9zc2libGUuXHJcblx0XHRcdFx0aW5kZXggPSBkYXRhLmFjdGl2ZUNvbHVtbnMuaW5kZXhPZignaW52b2ljZURhdGUnKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0cmV0dXJuIFtbaW5kZXgsIGRpcmVjdGlvbl1dO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIEdldCBJbml0aWFsIFNlYXJjaCBDb2xzXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtZXRlcnMgQ29udGFpbnMgdGhlIFVSTCBwYXJhbWV0ZXJzLlxyXG5cdFx0ICpcclxuXHRcdCAqIEByZXR1cm5zIHtPYmplY3RbXX0gUmV0dXJucyB0aGUgaW5pdGlhbCBmaWx0ZXJpbmcgdmFsdWVzLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfZ2V0U2VhcmNoQ29scyhwYXJhbWV0ZXJzLCBjb2x1bW5zKSB7XHJcblx0XHRcdGlmICghcGFyYW1ldGVycy5maWx0ZXIpIHtcclxuXHRcdFx0XHRyZXR1cm4gW107XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdGNvbnN0IHNlYXJjaENvbHMgPSBbXTtcclxuXHRcdFx0XHJcblx0XHRcdGZvciAobGV0IGNvbHVtbiBvZiBjb2x1bW5zKSB7XHJcblx0XHRcdFx0bGV0IGVudHJ5ID0gbnVsbDtcclxuXHRcdFx0XHRsZXQgdmFsdWUgPSBwYXJhbWV0ZXJzLmZpbHRlcltjb2x1bW4ubmFtZV07XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKHZhbHVlKSB7XHJcblx0XHRcdFx0XHRlbnRyeSA9IHsgc2VhcmNoOiBkZWNvZGVVUklDb21wb25lbnQodmFsdWUpIH07XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdHNlYXJjaENvbHMucHVzaChlbnRyeSk7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdHJldHVybiBzZWFyY2hDb2xzO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdGNvbnN0IGNvbHVtbnMgPSBqc2UubGlicy5kYXRhdGFibGUucHJlcGFyZUNvbHVtbnMoJHRoaXMsIGpzZS5saWJzLmludm9pY2VzX292ZXJ2aWV3X2NvbHVtbnMsXHJcblx0XHRcdFx0ZGF0YS5hY3RpdmVDb2x1bW5zKTtcclxuXHRcdFx0Y29uc3QgcGFyYW1ldGVycyA9ICQuZGVwYXJhbSh3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNsaWNlKDEpKTtcclxuXHRcdFx0Y29uc3QgcGFnZUxlbmd0aCA9IHBhcnNlSW50KHBhcmFtZXRlcnMubGVuZ3RoIHx8IGRhdGEucGFnZUxlbmd0aCk7XHJcblx0XHRcdFxyXG5cdFx0XHRqc2UubGlicy5kYXRhdGFibGUuY3JlYXRlKCR0aGlzLCB7XHJcblx0XHRcdFx0YXV0b1dpZHRoOiBmYWxzZSxcclxuXHRcdFx0XHRkb206ICd0JyxcclxuXHRcdFx0XHRwYWdlTGVuZ3RoLFxyXG5cdFx0XHRcdGRpc3BsYXlTdGFydDogcGFyc2VJbnQocGFyYW1ldGVycy5wYWdlKSA/IChwYXJzZUludChwYXJhbWV0ZXJzLnBhZ2UpIC0gMSkgKiBwYWdlTGVuZ3RoIDogMCxcclxuXHRcdFx0XHRzZXJ2ZXJTaWRlOiB0cnVlLFxyXG5cdFx0XHRcdGxhbmd1YWdlOiBqc2UubGlicy5kYXRhdGFibGUuZ2V0VHJhbnNsYXRpb25zKGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpKSxcclxuXHRcdFx0XHRhamF4OiB7XHJcblx0XHRcdFx0XHR1cmw6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89SW52b2ljZXNPdmVydmlld0FqYXgvRGF0YVRhYmxlJyxcclxuXHRcdFx0XHRcdHR5cGU6ICdQT1NUJyxcclxuXHRcdFx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0b3JkZXJDZWxsc1RvcDogdHJ1ZSxcclxuXHRcdFx0XHRvcmRlcjogX2dldE9yZGVyKHBhcmFtZXRlcnMsIGNvbHVtbnMpLFxyXG5cdFx0XHRcdHNlYXJjaENvbHM6IF9nZXRTZWFyY2hDb2xzKHBhcmFtZXRlcnMsIGNvbHVtbnMpLFxyXG5cdFx0XHRcdGNvbHVtbnNcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBBZGQgdGFibGUgZXJyb3IgaGFuZGxlci5cclxuXHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmVycm9yKCR0aGlzLCBmdW5jdGlvbihldmVudCwgc2V0dGluZ3MsIHRlY2hOb3RlLCBtZXNzYWdlKSB7XHJcblx0XHRcdFx0Y29uc3QgdGl0bGUgPSAnRGF0YVRhYmxlcyAnICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyk7XHJcblx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UodGl0bGUsIG1lc3NhZ2UpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEFkZCBwYWdpbmF0aW9uIGNoYW5nZSBoYW5kbGVyLiBcclxuXHRcdFx0JHRoaXMub24oJ2RhdGF0YWJsZV9jdXN0b21fcGFnaW5hdGlvbjpsZW5ndGhfY2hhbmdlJywgZnVuY3Rpb24oZXZlbnQsIG5ld1BhZ2VMZW5ndGgpIHtcclxuXHRcdFx0XHRqc2UubGlicy51c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZS5zZXQoe1xyXG5cdFx0XHRcdFx0ZGF0YToge1xyXG5cdFx0XHRcdFx0XHR1c2VySWQ6IGpzZS5jb3JlLnJlZ2lzdHJ5LmdldCgndXNlcklkJyksXHJcblx0XHRcdFx0XHRcdGNvbmZpZ3VyYXRpb25LZXk6ICdpbnZvaWNlc092ZXJ2aWV3UGFnZUxlbmd0aCcsXHJcblx0XHRcdFx0XHRcdGNvbmZpZ3VyYXRpb25WYWx1ZTogbmV3UGFnZUxlbmd0aFxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEFkZCBkcmF3IGV2ZW50IGhhbmRsZXIuIFxyXG5cdFx0XHQkdGhpcy5vbignZHJhdy5kdCcsICgpID0+IHtcclxuXHRcdFx0XHQkdGhpcy5maW5kKCd0aGVhZCBpbnB1dDpjaGVja2JveCcpXHJcblx0XHRcdFx0XHQucHJvcCgnY2hlY2tlZCcsIGZhbHNlKVxyXG5cdFx0XHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScsIFtmYWxzZV0pOyAvLyBObyBuZWVkIHRvIHVwZGF0ZSB0aGUgdGJvZHkgY2hlY2tib3hlcyAoZXZlbnQuanMpLlxyXG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3Rib2R5JykuYXR0cignZGF0YS1neC13aWRnZXQnLCAnc2luZ2xlX2NoZWNrYm94Jyk7XHJcblx0XHRcdFx0Z3gud2lkZ2V0cy5pbml0KCR0aGlzKTsgLy8gSW5pdGlhbGl6ZSB0aGUgY2hlY2tib3ggd2lkZ2V0LlxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdC8vIFNldCBhZG1pbiBzZWFyY2ggdmFsdWUuXHJcblx0XHRcdGlmIChPYmplY3Qua2V5cyhwYXJhbWV0ZXJzKS5pbmNsdWRlcygnZmlsdGVyJykgJiYgT2JqZWN0LmtleXMocGFyYW1ldGVycy5maWx0ZXIpLmluY2x1ZGVzKCdpbnZvaWNlTnVtYmVyJykpIHtcclxuXHRcdFx0XHRqc2UubGlicy5zZWFyY2guc2V0VmFsdWUocGFyYW1ldGVycy5maWx0ZXIuaW52b2ljZU51bWJlciwgdHJ1ZSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGRvbmUoKTtcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdHJldHVybiBtb2R1bGU7XHJcblx0fSk7XHJcbiJdfQ==
