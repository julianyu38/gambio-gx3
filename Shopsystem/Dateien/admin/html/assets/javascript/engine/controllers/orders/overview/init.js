'use strict';

/* --------------------------------------------------------------
 init.js 2016-09-01
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Orders Table Controller
 *
 * This controller initializes the main orders table with a new jQuery DataTables instance.
 */
gx.controllers.module('init', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js', jse.source + '/vendor/momentjs/moment.min.js', gx.source + '/libs/orders_overview_columns', gx.source + '/libs/search', 'datatable', 'modal', 'user_configuration_service'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Initial Table Order
  *
  * @param {Object} parameters Contains the URL parameters.
  * @param {Object} columns Contains the column definitions.
  *
  * @return {Array[]}
  */
	function _getOrder(parameters, columns) {
		var index = 1; // Order by first column by default.
		var direction = 'desc'; // Order DESC by default. 

		// Apply initial table sort. 
		if (parameters.sort) {
			direction = parameters.sort.charAt(0) === '-' ? 'desc' : 'asc';
			var columnName = parameters.sort.slice(1);

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = columns[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var column = _step.value;

					if (column.name === columnName) {
						index = columns.indexOf(column);
						break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		} else if (data.activeColumns.indexOf('number') > -1) {
			// Order by number if possible.
			index = data.activeColumns.indexOf('number');
		}

		return [[index, direction]];
	}

	/**
  * Get Initial Search Cols
  *
  * @param {Object} parameters Contains the URL parameters.
  *
  * @returns {Object[]} Returns the initial filtering values.
  */
	function _getSearchCols(parameters, columns) {
		if (!parameters.filter) {
			return [];
		}

		var searchCols = [];

		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			for (var _iterator2 = columns[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				var column = _step2.value;

				var entry = null;
				var value = parameters.filter[column.name];

				if (value) {
					entry = { search: decodeURIComponent(value) };
				}

				searchCols.push(entry);
			}
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		return searchCols;
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		var columns = jse.libs.datatable.prepareColumns($this, jse.libs.orders_overview_columns, data.activeColumns);
		var parameters = $.deparam(window.location.search.slice(1));
		var pageLength = parseInt(parameters.length || data.pageLength);

		jse.libs.datatable.create($this, {
			autoWidth: false,
			dom: 't',
			pageLength: pageLength,
			displayStart: parseInt(parameters.page) ? (parseInt(parameters.page) - 1) * pageLength : 0,
			serverSide: true,
			language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
			ajax: {
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=OrdersOverviewAjax/DataTable',
				type: 'POST',
				data: {
					pageToken: jse.core.config.get('pageToken')
				}
			},
			orderCellsTop: true,
			order: _getOrder(parameters, columns),
			searchCols: _getSearchCols(parameters, columns),
			columns: columns
		});

		// Add table error handler.
		jse.libs.datatable.error($this, function (event, settings, techNote, message) {
			var title = 'DataTables ' + jse.core.lang.translate('error', 'messages');
			jse.libs.modal.showMessage(title, message);
		});

		$this.on('datatable_custom_pagination:length_change', function (event, newPageLength) {
			jse.libs.user_configuration_service.set({
				data: {
					userId: jse.core.registry.get('userId'),
					configurationKey: 'ordersOverviewPageLength',
					configurationValue: newPageLength
				}
			});
		});

		$this.on('draw.dt', function () {
			$this.find('thead input:checkbox').prop('checked', false).trigger('change', [false]); // No need to update the tbody checkboxes (event.js).
			$this.find('tbody').attr('data-gx-widget', 'single_checkbox');
			gx.widgets.init($this); // Initialize the checkbox widget.
		});

		// Set admin search value.
		if (Object.keys(parameters).includes('filter') && Object.keys(parameters.filter).includes('number')) {
			jse.libs.search.setValue(parameters.filter.number, true);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vdmVydmlldy9pbml0LmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiX2dldE9yZGVyIiwicGFyYW1ldGVycyIsImNvbHVtbnMiLCJpbmRleCIsImRpcmVjdGlvbiIsInNvcnQiLCJjaGFyQXQiLCJjb2x1bW5OYW1lIiwic2xpY2UiLCJjb2x1bW4iLCJuYW1lIiwiaW5kZXhPZiIsImFjdGl2ZUNvbHVtbnMiLCJfZ2V0U2VhcmNoQ29scyIsImZpbHRlciIsInNlYXJjaENvbHMiLCJlbnRyeSIsInZhbHVlIiwic2VhcmNoIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwicHVzaCIsImluaXQiLCJkb25lIiwibGlicyIsImRhdGF0YWJsZSIsInByZXBhcmVDb2x1bW5zIiwib3JkZXJzX292ZXJ2aWV3X2NvbHVtbnMiLCJkZXBhcmFtIiwid2luZG93IiwibG9jYXRpb24iLCJwYWdlTGVuZ3RoIiwicGFyc2VJbnQiLCJsZW5ndGgiLCJjcmVhdGUiLCJhdXRvV2lkdGgiLCJkb20iLCJkaXNwbGF5U3RhcnQiLCJwYWdlIiwic2VydmVyU2lkZSIsImxhbmd1YWdlIiwiZ2V0VHJhbnNsYXRpb25zIiwiY29yZSIsImNvbmZpZyIsImdldCIsImFqYXgiLCJ1cmwiLCJ0eXBlIiwicGFnZVRva2VuIiwib3JkZXJDZWxsc1RvcCIsIm9yZGVyIiwiZXJyb3IiLCJldmVudCIsInNldHRpbmdzIiwidGVjaE5vdGUiLCJtZXNzYWdlIiwidGl0bGUiLCJsYW5nIiwidHJhbnNsYXRlIiwibW9kYWwiLCJzaG93TWVzc2FnZSIsIm9uIiwibmV3UGFnZUxlbmd0aCIsInVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlIiwic2V0IiwidXNlcklkIiwicmVnaXN0cnkiLCJjb25maWd1cmF0aW9uS2V5IiwiY29uZmlndXJhdGlvblZhbHVlIiwiZmluZCIsInByb3AiLCJ0cmlnZ2VyIiwiYXR0ciIsIndpZGdldHMiLCJPYmplY3QiLCJrZXlzIiwiaW5jbHVkZXMiLCJzZXRWYWx1ZSIsIm51bWJlciJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7OztBQUtBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyxNQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixtREFFSUQsSUFBSUMsTUFGUixrREFHSUQsSUFBSUMsTUFIUixtREFJSUQsSUFBSUMsTUFKUixxQ0FLSUosR0FBR0ksTUFMUCxvQ0FNSUosR0FBR0ksTUFOUCxtQkFPQyxXQVBELEVBUUMsT0FSRCxFQVNDLDRCQVRELENBSEQsRUFlQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNTCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQVFBLFVBQVNNLFNBQVQsQ0FBbUJDLFVBQW5CLEVBQStCQyxPQUEvQixFQUF3QztBQUN2QyxNQUFJQyxRQUFRLENBQVosQ0FEdUMsQ0FDeEI7QUFDZixNQUFJQyxZQUFZLE1BQWhCLENBRnVDLENBRWY7O0FBRXhCO0FBQ0EsTUFBSUgsV0FBV0ksSUFBZixFQUFxQjtBQUNwQkQsZUFBWUgsV0FBV0ksSUFBWCxDQUFnQkMsTUFBaEIsQ0FBdUIsQ0FBdkIsTUFBOEIsR0FBOUIsR0FBb0MsTUFBcEMsR0FBNkMsS0FBekQ7QUFDQSxPQUFNQyxhQUFjTixXQUFXSSxJQUFYLENBQWdCRyxLQUFoQixDQUFzQixDQUF0QixDQUFwQjs7QUFGb0I7QUFBQTtBQUFBOztBQUFBO0FBSXBCLHlCQUFtQk4sT0FBbkIsOEhBQTRCO0FBQUEsU0FBbkJPLE1BQW1COztBQUMzQixTQUFJQSxPQUFPQyxJQUFQLEtBQWdCSCxVQUFwQixFQUFnQztBQUMvQkosY0FBUUQsUUFBUVMsT0FBUixDQUFnQkYsTUFBaEIsQ0FBUjtBQUNBO0FBQ0E7QUFDRDtBQVRtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVXBCLEdBVkQsTUFVTyxJQUFJWixLQUFLZSxhQUFMLENBQW1CRCxPQUFuQixDQUEyQixRQUEzQixJQUF1QyxDQUFDLENBQTVDLEVBQStDO0FBQUU7QUFDdkRSLFdBQVFOLEtBQUtlLGFBQUwsQ0FBbUJELE9BQW5CLENBQTJCLFFBQTNCLENBQVI7QUFDQTs7QUFFRCxTQUFPLENBQUMsQ0FBQ1IsS0FBRCxFQUFRQyxTQUFSLENBQUQsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU1MsY0FBVCxDQUF3QlosVUFBeEIsRUFBb0NDLE9BQXBDLEVBQTZDO0FBQzVDLE1BQUksQ0FBQ0QsV0FBV2EsTUFBaEIsRUFBd0I7QUFDdkIsVUFBTyxFQUFQO0FBQ0E7O0FBRUQsTUFBTUMsYUFBYSxFQUFuQjs7QUFMNEM7QUFBQTtBQUFBOztBQUFBO0FBTzVDLHlCQUFtQmIsT0FBbkIsbUlBQTRCO0FBQUEsUUFBbkJPLE1BQW1COztBQUMzQixRQUFJTyxRQUFRLElBQVo7QUFDQSxRQUFJQyxRQUFRaEIsV0FBV2EsTUFBWCxDQUFrQkwsT0FBT0MsSUFBekIsQ0FBWjs7QUFFQSxRQUFJTyxLQUFKLEVBQVc7QUFDVkQsYUFBUSxFQUFFRSxRQUFRQyxtQkFBbUJGLEtBQW5CLENBQVYsRUFBUjtBQUNBOztBQUVERixlQUFXSyxJQUFYLENBQWdCSixLQUFoQjtBQUNBO0FBaEIyQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWtCNUMsU0FBT0QsVUFBUDtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQXJCLFFBQU8yQixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCLE1BQU1wQixVQUFVUCxJQUFJNEIsSUFBSixDQUFTQyxTQUFULENBQW1CQyxjQUFuQixDQUFrQzNCLEtBQWxDLEVBQXlDSCxJQUFJNEIsSUFBSixDQUFTRyx1QkFBbEQsRUFDZjdCLEtBQUtlLGFBRFUsQ0FBaEI7QUFFQSxNQUFNWCxhQUFhRixFQUFFNEIsT0FBRixDQUFVQyxPQUFPQyxRQUFQLENBQWdCWCxNQUFoQixDQUF1QlYsS0FBdkIsQ0FBNkIsQ0FBN0IsQ0FBVixDQUFuQjtBQUNBLE1BQU1zQixhQUFhQyxTQUFTOUIsV0FBVytCLE1BQVgsSUFBcUJuQyxLQUFLaUMsVUFBbkMsQ0FBbkI7O0FBRUFuQyxNQUFJNEIsSUFBSixDQUFTQyxTQUFULENBQW1CUyxNQUFuQixDQUEwQm5DLEtBQTFCLEVBQWlDO0FBQ2hDb0MsY0FBVyxLQURxQjtBQUVoQ0MsUUFBSyxHQUYyQjtBQUdoQ0wseUJBSGdDO0FBSWhDTSxpQkFBY0wsU0FBUzlCLFdBQVdvQyxJQUFwQixJQUE0QixDQUFDTixTQUFTOUIsV0FBV29DLElBQXBCLElBQTRCLENBQTdCLElBQWtDUCxVQUE5RCxHQUEyRSxDQUp6RDtBQUtoQ1EsZUFBWSxJQUxvQjtBQU1oQ0MsYUFBVTVDLElBQUk0QixJQUFKLENBQVNDLFNBQVQsQ0FBbUJnQixlQUFuQixDQUFtQzdDLElBQUk4QyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLENBQW5DLENBTnNCO0FBT2hDQyxTQUFNO0FBQ0xDLFNBQUtsRCxJQUFJOEMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxrREFEaEM7QUFFTEcsVUFBTSxNQUZEO0FBR0xqRCxVQUFNO0FBQ0xrRCxnQkFBV3BELElBQUk4QyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFdBQXBCO0FBRE47QUFIRCxJQVAwQjtBQWNoQ0ssa0JBQWUsSUFkaUI7QUFlaENDLFVBQU9qRCxVQUFVQyxVQUFWLEVBQXNCQyxPQUF0QixDQWZ5QjtBQWdCaENhLGVBQVlGLGVBQWVaLFVBQWYsRUFBMkJDLE9BQTNCLENBaEJvQjtBQWlCaENBO0FBakJnQyxHQUFqQzs7QUFvQkE7QUFDQVAsTUFBSTRCLElBQUosQ0FBU0MsU0FBVCxDQUFtQjBCLEtBQW5CLENBQXlCcEQsS0FBekIsRUFBZ0MsVUFBU3FELEtBQVQsRUFBZ0JDLFFBQWhCLEVBQTBCQyxRQUExQixFQUFvQ0MsT0FBcEMsRUFBNkM7QUFDNUUsT0FBTUMsUUFBUSxnQkFBZ0I1RCxJQUFJOEMsSUFBSixDQUFTZSxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FBOUI7QUFDQTlELE9BQUk0QixJQUFKLENBQVNtQyxLQUFULENBQWVDLFdBQWYsQ0FBMkJKLEtBQTNCLEVBQWtDRCxPQUFsQztBQUNBLEdBSEQ7O0FBS0F4RCxRQUFNOEQsRUFBTixDQUFTLDJDQUFULEVBQXNELFVBQVNULEtBQVQsRUFBZ0JVLGFBQWhCLEVBQStCO0FBQ3BGbEUsT0FBSTRCLElBQUosQ0FBU3VDLDBCQUFULENBQW9DQyxHQUFwQyxDQUF3QztBQUN2Q2xFLFVBQU07QUFDTG1FLGFBQVFyRSxJQUFJOEMsSUFBSixDQUFTd0IsUUFBVCxDQUFrQnRCLEdBQWxCLENBQXNCLFFBQXRCLENBREg7QUFFTHVCLHVCQUFrQiwwQkFGYjtBQUdMQyx5QkFBb0JOO0FBSGY7QUFEaUMsSUFBeEM7QUFPQSxHQVJEOztBQVVBL0QsUUFBTThELEVBQU4sQ0FBUyxTQUFULEVBQW9CLFlBQU07QUFDekI5RCxTQUFNc0UsSUFBTixDQUFXLHNCQUFYLEVBQ0VDLElBREYsQ0FDTyxTQURQLEVBQ2tCLEtBRGxCLEVBRUVDLE9BRkYsQ0FFVSxRQUZWLEVBRW9CLENBQUMsS0FBRCxDQUZwQixFQUR5QixDQUdLO0FBQzlCeEUsU0FBTXNFLElBQU4sQ0FBVyxPQUFYLEVBQW9CRyxJQUFwQixDQUF5QixnQkFBekIsRUFBMkMsaUJBQTNDO0FBQ0EvRSxNQUFHZ0YsT0FBSCxDQUFXbkQsSUFBWCxDQUFnQnZCLEtBQWhCLEVBTHlCLENBS0Q7QUFDeEIsR0FORDs7QUFRQTtBQUNBLE1BQUkyRSxPQUFPQyxJQUFQLENBQVl6RSxVQUFaLEVBQXdCMEUsUUFBeEIsQ0FBaUMsUUFBakMsS0FBOENGLE9BQU9DLElBQVAsQ0FBWXpFLFdBQVdhLE1BQXZCLEVBQStCNkQsUUFBL0IsQ0FBd0MsUUFBeEMsQ0FBbEQsRUFBcUc7QUFDcEdoRixPQUFJNEIsSUFBSixDQUFTTCxNQUFULENBQWdCMEQsUUFBaEIsQ0FBeUIzRSxXQUFXYSxNQUFYLENBQWtCK0QsTUFBM0MsRUFBbUQsSUFBbkQ7QUFDQTs7QUFFRHZEO0FBQ0EsRUF4REQ7O0FBMERBLFFBQU81QixNQUFQO0FBQ0EsQ0FsS0YiLCJmaWxlIjoib3JkZXJzL292ZXJ2aWV3L2luaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gaW5pdC5qcyAyMDE2LTA5LTAxXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIE9yZGVycyBUYWJsZSBDb250cm9sbGVyXHJcbiAqXHJcbiAqIFRoaXMgY29udHJvbGxlciBpbml0aWFsaXplcyB0aGUgbWFpbiBvcmRlcnMgdGFibGUgd2l0aCBhIG5ldyBqUXVlcnkgRGF0YVRhYmxlcyBpbnN0YW5jZS5cclxuICovXHJcbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcclxuXHQnaW5pdCcsXHJcblx0XHJcblx0W1xyXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmNzc2AsXHJcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvZGF0YXRhYmxlcy9qcXVlcnkuZGF0YVRhYmxlcy5taW4uanNgLFxyXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS1kZXBhcmFtL2pxdWVyeS1kZXBhcmFtLm1pbi5qc2AsXHJcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvbW9tZW50anMvbW9tZW50Lm1pbi5qc2AsXHJcblx0XHRgJHtneC5zb3VyY2V9L2xpYnMvb3JkZXJzX292ZXJ2aWV3X2NvbHVtbnNgLFxyXG5cdFx0YCR7Z3guc291cmNlfS9saWJzL3NlYXJjaGAsXHJcblx0XHQnZGF0YXRhYmxlJyxcclxuXHRcdCdtb2RhbCcsXHJcblx0XHQndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnXHJcblx0XSxcclxuXHRcclxuXHRmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcclxuXHRcdCd1c2Ugc3RyaWN0JztcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBWQVJJQUJMRVNcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBGVU5DVElPTlNcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIEdldCBJbml0aWFsIFRhYmxlIE9yZGVyXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtZXRlcnMgQ29udGFpbnMgdGhlIFVSTCBwYXJhbWV0ZXJzLlxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGNvbHVtbnMgQ29udGFpbnMgdGhlIGNvbHVtbiBkZWZpbml0aW9ucy5cclxuXHRcdCAqXHJcblx0XHQgKiBAcmV0dXJuIHtBcnJheVtdfVxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfZ2V0T3JkZXIocGFyYW1ldGVycywgY29sdW1ucykge1xyXG5cdFx0XHRsZXQgaW5kZXggPSAxOyAvLyBPcmRlciBieSBmaXJzdCBjb2x1bW4gYnkgZGVmYXVsdC5cclxuXHRcdFx0bGV0IGRpcmVjdGlvbiA9ICdkZXNjJzsgLy8gT3JkZXIgREVTQyBieSBkZWZhdWx0LiBcclxuXHRcdFx0XHJcblx0XHRcdC8vIEFwcGx5IGluaXRpYWwgdGFibGUgc29ydC4gXHJcblx0XHRcdGlmIChwYXJhbWV0ZXJzLnNvcnQpIHtcclxuXHRcdFx0XHRkaXJlY3Rpb24gPSBwYXJhbWV0ZXJzLnNvcnQuY2hhckF0KDApID09PSAnLScgPyAnZGVzYycgOiAnYXNjJztcclxuXHRcdFx0XHRjb25zdCBjb2x1bW5OYW1lICA9IHBhcmFtZXRlcnMuc29ydC5zbGljZSgxKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRmb3IgKGxldCBjb2x1bW4gb2YgY29sdW1ucykge1xyXG5cdFx0XHRcdFx0aWYgKGNvbHVtbi5uYW1lID09PSBjb2x1bW5OYW1lKSB7XHJcblx0XHRcdFx0XHRcdGluZGV4ID0gY29sdW1ucy5pbmRleE9mKGNvbHVtbik7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSBlbHNlIGlmIChkYXRhLmFjdGl2ZUNvbHVtbnMuaW5kZXhPZignbnVtYmVyJykgPiAtMSkgeyAvLyBPcmRlciBieSBudW1iZXIgaWYgcG9zc2libGUuXHJcblx0XHRcdFx0aW5kZXggPSBkYXRhLmFjdGl2ZUNvbHVtbnMuaW5kZXhPZignbnVtYmVyJyk7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdHJldHVybiBbW2luZGV4LCBkaXJlY3Rpb25dXTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBHZXQgSW5pdGlhbCBTZWFyY2ggQ29sc1xyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBwYXJhbWV0ZXJzIENvbnRhaW5zIHRoZSBVUkwgcGFyYW1ldGVycy5cclxuXHRcdCAqXHJcblx0XHQgKiBAcmV0dXJucyB7T2JqZWN0W119IFJldHVybnMgdGhlIGluaXRpYWwgZmlsdGVyaW5nIHZhbHVlcy5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX2dldFNlYXJjaENvbHMocGFyYW1ldGVycywgY29sdW1ucykge1xyXG5cdFx0XHRpZiAoIXBhcmFtZXRlcnMuZmlsdGVyKSB7XHJcblx0XHRcdFx0cmV0dXJuIFtdO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCBzZWFyY2hDb2xzID0gW107XHJcblx0XHRcdFxyXG5cdFx0XHRmb3IgKGxldCBjb2x1bW4gb2YgY29sdW1ucykge1xyXG5cdFx0XHRcdGxldCBlbnRyeSA9IG51bGw7XHJcblx0XHRcdFx0bGV0IHZhbHVlID0gcGFyYW1ldGVycy5maWx0ZXJbY29sdW1uLm5hbWVdO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmICh2YWx1ZSkge1xyXG5cdFx0XHRcdFx0ZW50cnkgPSB7IHNlYXJjaDogZGVjb2RlVVJJQ29tcG9uZW50KHZhbHVlKSB9O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRzZWFyY2hDb2xzLnB1c2goZW50cnkpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRyZXR1cm4gc2VhcmNoQ29scztcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0XHRjb25zdCBjb2x1bW5zID0ganNlLmxpYnMuZGF0YXRhYmxlLnByZXBhcmVDb2x1bW5zKCR0aGlzLCBqc2UubGlicy5vcmRlcnNfb3ZlcnZpZXdfY29sdW1ucyxcclxuXHRcdFx0XHRkYXRhLmFjdGl2ZUNvbHVtbnMpO1xyXG5cdFx0XHRjb25zdCBwYXJhbWV0ZXJzID0gJC5kZXBhcmFtKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSkpO1xyXG5cdFx0XHRjb25zdCBwYWdlTGVuZ3RoID0gcGFyc2VJbnQocGFyYW1ldGVycy5sZW5ndGggfHwgZGF0YS5wYWdlTGVuZ3RoKTtcclxuXHRcdFx0XHJcblx0XHRcdGpzZS5saWJzLmRhdGF0YWJsZS5jcmVhdGUoJHRoaXMsIHtcclxuXHRcdFx0XHRhdXRvV2lkdGg6IGZhbHNlLFxyXG5cdFx0XHRcdGRvbTogJ3QnLFxyXG5cdFx0XHRcdHBhZ2VMZW5ndGgsXHJcblx0XHRcdFx0ZGlzcGxheVN0YXJ0OiBwYXJzZUludChwYXJhbWV0ZXJzLnBhZ2UpID8gKHBhcnNlSW50KHBhcmFtZXRlcnMucGFnZSkgLSAxKSAqIHBhZ2VMZW5ndGggOiAwLFxyXG5cdFx0XHRcdHNlcnZlclNpZGU6IHRydWUsXHJcblx0XHRcdFx0bGFuZ3VhZ2U6IGpzZS5saWJzLmRhdGF0YWJsZS5nZXRUcmFuc2xhdGlvbnMoanNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJykpLFxyXG5cdFx0XHRcdGFqYXg6IHtcclxuXHRcdFx0XHRcdHVybDoganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1PcmRlcnNPdmVydmlld0FqYXgvRGF0YVRhYmxlJyxcclxuXHRcdFx0XHRcdHR5cGU6ICdQT1NUJyxcclxuXHRcdFx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0b3JkZXJDZWxsc1RvcDogdHJ1ZSxcclxuXHRcdFx0XHRvcmRlcjogX2dldE9yZGVyKHBhcmFtZXRlcnMsIGNvbHVtbnMpLFxyXG5cdFx0XHRcdHNlYXJjaENvbHM6IF9nZXRTZWFyY2hDb2xzKHBhcmFtZXRlcnMsIGNvbHVtbnMpLFxyXG5cdFx0XHRcdGNvbHVtbnNcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBBZGQgdGFibGUgZXJyb3IgaGFuZGxlci5cclxuXHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmVycm9yKCR0aGlzLCBmdW5jdGlvbihldmVudCwgc2V0dGluZ3MsIHRlY2hOb3RlLCBtZXNzYWdlKSB7XHJcblx0XHRcdFx0Y29uc3QgdGl0bGUgPSAnRGF0YVRhYmxlcyAnICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Vycm9yJywgJ21lc3NhZ2VzJyk7XHJcblx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UodGl0bGUsIG1lc3NhZ2UpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdCR0aGlzLm9uKCdkYXRhdGFibGVfY3VzdG9tX3BhZ2luYXRpb246bGVuZ3RoX2NoYW5nZScsIGZ1bmN0aW9uKGV2ZW50LCBuZXdQYWdlTGVuZ3RoKSB7XHJcblx0XHRcdFx0anNlLmxpYnMudXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2Uuc2V0KHtcclxuXHRcdFx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRcdFx0dXNlcklkOiBqc2UuY29yZS5yZWdpc3RyeS5nZXQoJ3VzZXJJZCcpLFxyXG5cdFx0XHRcdFx0XHRjb25maWd1cmF0aW9uS2V5OiAnb3JkZXJzT3ZlcnZpZXdQYWdlTGVuZ3RoJyxcclxuXHRcdFx0XHRcdFx0Y29uZmlndXJhdGlvblZhbHVlOiBuZXdQYWdlTGVuZ3RoXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0JHRoaXMub24oJ2RyYXcuZHQnLCAoKSA9PiB7XHJcblx0XHRcdFx0JHRoaXMuZmluZCgndGhlYWQgaW5wdXQ6Y2hlY2tib3gnKVxyXG5cdFx0XHRcdFx0LnByb3AoJ2NoZWNrZWQnLCBmYWxzZSlcclxuXHRcdFx0XHRcdC50cmlnZ2VyKCdjaGFuZ2UnLCBbZmFsc2VdKTsgLy8gTm8gbmVlZCB0byB1cGRhdGUgdGhlIHRib2R5IGNoZWNrYm94ZXMgKGV2ZW50LmpzKS5cclxuXHRcdFx0XHQkdGhpcy5maW5kKCd0Ym9keScpLmF0dHIoJ2RhdGEtZ3gtd2lkZ2V0JywgJ3NpbmdsZV9jaGVja2JveCcpO1xyXG5cdFx0XHRcdGd4LndpZGdldHMuaW5pdCgkdGhpcyk7IC8vIEluaXRpYWxpemUgdGhlIGNoZWNrYm94IHdpZGdldC5cclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHQvLyBTZXQgYWRtaW4gc2VhcmNoIHZhbHVlLlxyXG5cdFx0XHRpZiAoT2JqZWN0LmtleXMocGFyYW1ldGVycykuaW5jbHVkZXMoJ2ZpbHRlcicpICYmIE9iamVjdC5rZXlzKHBhcmFtZXRlcnMuZmlsdGVyKS5pbmNsdWRlcygnbnVtYmVyJykpIHtcclxuXHRcdFx0XHRqc2UubGlicy5zZWFyY2guc2V0VmFsdWUocGFyYW1ldGVycy5maWx0ZXIubnVtYmVyLCB0cnVlKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0ZG9uZSgpO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0cmV0dXJuIG1vZHVsZTtcclxuXHR9KTtcclxuIl19
