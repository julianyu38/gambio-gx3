'use strict';

/* --------------------------------------------------------------
 change_status.js 2016-05-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Change Order Status Modal Controller
 */
gx.controllers.module('change_status', ['modal'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: {
			selectedOrders: $this.find('.selected-orders'),
			status: $this.find('#status-dropdown'),
			notifyCustomer: $this.find('#notify-customer'),
			sendParcelTrackingCode: $this.find('#send-parcel-tracking-code'),
			sendComment: $this.find('#send-comment'),
			comment: $this.find('#comment')
		}
	};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Send the modal data to the form through an AJAX call.
  *
  * @param {jQuery.Event} event
  */
	function _changeStatus(event) {
		event.stopPropagation();

		if (module.bindings.status.get() === '') {
			return;
		}

		var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=OrdersModalsAjax/ChangeOrderStatus';
		var data = {
			selectedOrders: module.bindings.selectedOrders.get().split(', '),
			statusId: module.bindings.status.get(),
			notifyCustomer: module.bindings.notifyCustomer.get(),
			sendParcelTrackingCode: module.bindings.sendParcelTrackingCode.get(),
			sendComment: module.bindings.sendComment.get(),
			comment: module.bindings.comment.get(),
			pageToken: jse.core.config.get('pageToken')
		};
		var $saveButton = $(event.target);

		$saveButton.addClass('disabled').attr('disabled', true);

		$.ajax({
			url: url,
			data: data,
			method: 'POST'
		}).done(function (response) {
			var content = data.notifyCustomer ? jse.core.lang.translate('MAIL_SUCCESS', 'gm_send_order') : jse.core.lang.translate('SUCCESS_ORDER_UPDATED', 'orders');

			if ($('.orders.overview').length) {
				$('.orders .table-main').DataTable().ajax.reload(null, false);
				$('.orders .table-main').orders_overview_filter('reload');
			} else {
				$('.invoices .table-main').DataTable().ajax.reload(null, false);
				$('.invoices .table-main').invoices_overview_filter('reload');
			}

			// Show success message in the admin info box.
			jse.libs.info_box.addSuccessMessage(content);
		}).always(function () {
			$this.modal('hide');
			$saveButton.removeClass('disabled').attr('disabled', false);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.save', _changeStatus);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvY2hhbmdlX3N0YXR1cy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImJpbmRpbmdzIiwic2VsZWN0ZWRPcmRlcnMiLCJmaW5kIiwic3RhdHVzIiwibm90aWZ5Q3VzdG9tZXIiLCJzZW5kUGFyY2VsVHJhY2tpbmdDb2RlIiwic2VuZENvbW1lbnQiLCJjb21tZW50IiwiX2NoYW5nZVN0YXR1cyIsImV2ZW50Iiwic3RvcFByb3BhZ2F0aW9uIiwiZ2V0IiwidXJsIiwianNlIiwiY29yZSIsImNvbmZpZyIsInNwbGl0Iiwic3RhdHVzSWQiLCJwYWdlVG9rZW4iLCIkc2F2ZUJ1dHRvbiIsInRhcmdldCIsImFkZENsYXNzIiwiYXR0ciIsImFqYXgiLCJtZXRob2QiLCJkb25lIiwicmVzcG9uc2UiLCJjb250ZW50IiwibGFuZyIsInRyYW5zbGF0ZSIsImxlbmd0aCIsIkRhdGFUYWJsZSIsInJlbG9hZCIsIm9yZGVyc19vdmVydmlld19maWx0ZXIiLCJpbnZvaWNlc19vdmVydmlld19maWx0ZXIiLCJsaWJzIiwiaW5mb19ib3giLCJhZGRTdWNjZXNzTWVzc2FnZSIsImFsd2F5cyIsIm1vZGFsIiwicmVtb3ZlQ2xhc3MiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7O0FBR0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUFzQixlQUF0QixFQUF1QyxDQUFDLE9BQUQsQ0FBdkMsRUFBa0QsVUFBU0MsSUFBVCxFQUFlOztBQUVoRTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVM7QUFDZEksWUFBVTtBQUNUQyxtQkFBZ0JILE1BQU1JLElBQU4sQ0FBVyxrQkFBWCxDQURQO0FBRVRDLFdBQVFMLE1BQU1JLElBQU4sQ0FBVyxrQkFBWCxDQUZDO0FBR1RFLG1CQUFnQk4sTUFBTUksSUFBTixDQUFXLGtCQUFYLENBSFA7QUFJVEcsMkJBQXdCUCxNQUFNSSxJQUFOLENBQVcsNEJBQVgsQ0FKZjtBQUtUSSxnQkFBYVIsTUFBTUksSUFBTixDQUFXLGVBQVgsQ0FMSjtBQU1USyxZQUFTVCxNQUFNSSxJQUFOLENBQVcsVUFBWDtBQU5BO0FBREksRUFBZjs7QUFXQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU00sYUFBVCxDQUF1QkMsS0FBdkIsRUFBOEI7QUFDN0JBLFFBQU1DLGVBQU47O0FBRUEsTUFBSWQsT0FBT0ksUUFBUCxDQUFnQkcsTUFBaEIsQ0FBdUJRLEdBQXZCLE9BQWlDLEVBQXJDLEVBQXlDO0FBQ3hDO0FBQ0E7O0FBRUQsTUFBTUMsTUFBTUMsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCSixHQUFoQixDQUFvQixRQUFwQixJQUFnQyx3REFBNUM7QUFDQSxNQUFNZCxPQUFPO0FBQ1hJLG1CQUFnQkwsT0FBT0ksUUFBUCxDQUFnQkMsY0FBaEIsQ0FBK0JVLEdBQS9CLEdBQXFDSyxLQUFyQyxDQUEyQyxJQUEzQyxDQURMO0FBRVhDLGFBQVVyQixPQUFPSSxRQUFQLENBQWdCRyxNQUFoQixDQUF1QlEsR0FBdkIsRUFGQztBQUdYUCxtQkFBZ0JSLE9BQU9JLFFBQVAsQ0FBZ0JJLGNBQWhCLENBQStCTyxHQUEvQixFQUhMO0FBSVhOLDJCQUF3QlQsT0FBT0ksUUFBUCxDQUFnQkssc0JBQWhCLENBQXVDTSxHQUF2QyxFQUpiO0FBS1hMLGdCQUFhVixPQUFPSSxRQUFQLENBQWdCTSxXQUFoQixDQUE0QkssR0FBNUIsRUFMRjtBQU1YSixZQUFTWCxPQUFPSSxRQUFQLENBQWdCTyxPQUFoQixDQUF3QkksR0FBeEIsRUFORTtBQU9YTyxjQUFXTCxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JKLEdBQWhCLENBQW9CLFdBQXBCO0FBUEEsR0FBYjtBQVNBLE1BQU1RLGNBQWNwQixFQUFFVSxNQUFNVyxNQUFSLENBQXBCOztBQUVBRCxjQUFZRSxRQUFaLENBQXFCLFVBQXJCLEVBQWlDQyxJQUFqQyxDQUFzQyxVQUF0QyxFQUFrRCxJQUFsRDs7QUFFQXZCLElBQUV3QixJQUFGLENBQU87QUFDTlgsV0FETTtBQUVOZixhQUZNO0FBR04yQixXQUFRO0FBSEYsR0FBUCxFQUtFQyxJQUxGLENBS08sVUFBU0MsUUFBVCxFQUFtQjtBQUN4QixPQUFNQyxVQUFVOUIsS0FBS08sY0FBTCxHQUNBUyxJQUFJQyxJQUFKLENBQVNjLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxlQUF4QyxDQURBLEdBRUFoQixJQUFJQyxJQUFKLENBQVNjLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix1QkFBeEIsRUFBaUQsUUFBakQsQ0FGaEI7O0FBSUEsT0FBSTlCLEVBQUUsa0JBQUYsRUFBc0IrQixNQUExQixFQUFrQztBQUNqQy9CLE1BQUUscUJBQUYsRUFBeUJnQyxTQUF6QixHQUFxQ1IsSUFBckMsQ0FBMENTLE1BQTFDLENBQWlELElBQWpELEVBQXVELEtBQXZEO0FBQ0FqQyxNQUFFLHFCQUFGLEVBQXlCa0Msc0JBQXpCLENBQWdELFFBQWhEO0FBQ0EsSUFIRCxNQUdPO0FBQ05sQyxNQUFFLHVCQUFGLEVBQTJCZ0MsU0FBM0IsR0FBdUNSLElBQXZDLENBQTRDUyxNQUE1QyxDQUFtRCxJQUFuRCxFQUF5RCxLQUF6RDtBQUNBakMsTUFBRSx1QkFBRixFQUEyQm1DLHdCQUEzQixDQUFvRCxRQUFwRDtBQUNBOztBQUVEO0FBQ0FyQixPQUFJc0IsSUFBSixDQUFTQyxRQUFULENBQWtCQyxpQkFBbEIsQ0FBb0NWLE9BQXBDO0FBQ0EsR0FwQkYsRUFxQkVXLE1BckJGLENBcUJTLFlBQVc7QUFDbEJ4QyxTQUFNeUMsS0FBTixDQUFZLE1BQVo7QUFDQXBCLGVBQVlxQixXQUFaLENBQXdCLFVBQXhCLEVBQW9DbEIsSUFBcEMsQ0FBeUMsVUFBekMsRUFBcUQsS0FBckQ7QUFDQSxHQXhCRjtBQXlCQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUExQixRQUFPNkMsSUFBUCxHQUFjLFVBQVNoQixJQUFULEVBQWU7QUFDNUIzQixRQUFNNEMsRUFBTixDQUFTLE9BQVQsRUFBa0IsV0FBbEIsRUFBK0JsQyxhQUEvQjtBQUNBaUI7QUFDQSxFQUhEOztBQUtBLFFBQU83QixNQUFQO0FBQ0EsQ0FsR0QiLCJmaWxlIjoib3JkZXJzL21vZGFscy9jaGFuZ2Vfc3RhdHVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBjaGFuZ2Vfc3RhdHVzLmpzIDIwMTYtMDUtMDlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIENoYW5nZSBPcmRlciBTdGF0dXMgTW9kYWwgQ29udHJvbGxlclxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ2NoYW5nZV9zdGF0dXMnLCBbJ21vZGFsJ10sIGZ1bmN0aW9uKGRhdGEpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBWQVJJQUJMRVNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdCAqXG5cdCAqIEB0eXBlIHtqUXVlcnl9XG5cdCAqL1xuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFxuXHQvKipcblx0ICogTW9kdWxlIEluc3RhbmNlXG5cdCAqXG5cdCAqIEB0eXBlIHtPYmplY3R9XG5cdCAqL1xuXHRjb25zdCBtb2R1bGUgPSB7XG5cdFx0YmluZGluZ3M6IHtcblx0XHRcdHNlbGVjdGVkT3JkZXJzOiAkdGhpcy5maW5kKCcuc2VsZWN0ZWQtb3JkZXJzJyksXG5cdFx0XHRzdGF0dXM6ICR0aGlzLmZpbmQoJyNzdGF0dXMtZHJvcGRvd24nKSxcblx0XHRcdG5vdGlmeUN1c3RvbWVyOiAkdGhpcy5maW5kKCcjbm90aWZ5LWN1c3RvbWVyJyksXG5cdFx0XHRzZW5kUGFyY2VsVHJhY2tpbmdDb2RlOiAkdGhpcy5maW5kKCcjc2VuZC1wYXJjZWwtdHJhY2tpbmctY29kZScpLFxuXHRcdFx0c2VuZENvbW1lbnQ6ICR0aGlzLmZpbmQoJyNzZW5kLWNvbW1lbnQnKSxcblx0XHRcdGNvbW1lbnQ6ICR0aGlzLmZpbmQoJyNjb21tZW50Jylcblx0XHR9XG5cdH07XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gRlVOQ1RJT05TXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIFNlbmQgdGhlIG1vZGFsIGRhdGEgdG8gdGhlIGZvcm0gdGhyb3VnaCBhbiBBSkFYIGNhbGwuXG5cdCAqXG5cdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxuXHQgKi9cblx0ZnVuY3Rpb24gX2NoYW5nZVN0YXR1cyhldmVudCkge1xuXHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFxuXHRcdGlmIChtb2R1bGUuYmluZGluZ3Muc3RhdHVzLmdldCgpID09PSAnJykge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblx0XHRcblx0XHRjb25zdCB1cmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPU9yZGVyc01vZGFsc0FqYXgvQ2hhbmdlT3JkZXJTdGF0dXMnO1xuXHRcdGNvbnN0IGRhdGEgPSB7XG5cdFx0XHRcdHNlbGVjdGVkT3JkZXJzOiBtb2R1bGUuYmluZGluZ3Muc2VsZWN0ZWRPcmRlcnMuZ2V0KCkuc3BsaXQoJywgJyksXG5cdFx0XHRcdHN0YXR1c0lkOiBtb2R1bGUuYmluZGluZ3Muc3RhdHVzLmdldCgpLFxuXHRcdFx0XHRub3RpZnlDdXN0b21lcjogbW9kdWxlLmJpbmRpbmdzLm5vdGlmeUN1c3RvbWVyLmdldCgpLFxuXHRcdFx0XHRzZW5kUGFyY2VsVHJhY2tpbmdDb2RlOiBtb2R1bGUuYmluZGluZ3Muc2VuZFBhcmNlbFRyYWNraW5nQ29kZS5nZXQoKSxcblx0XHRcdFx0c2VuZENvbW1lbnQ6IG1vZHVsZS5iaW5kaW5ncy5zZW5kQ29tbWVudC5nZXQoKSxcblx0XHRcdFx0Y29tbWVudDogbW9kdWxlLmJpbmRpbmdzLmNvbW1lbnQuZ2V0KCksXG5cdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJylcblx0XHRcdH07XG5cdFx0Y29uc3QgJHNhdmVCdXR0b24gPSAkKGV2ZW50LnRhcmdldCk7XG5cdFx0XG5cdFx0JHNhdmVCdXR0b24uYWRkQ2xhc3MoJ2Rpc2FibGVkJykuYXR0cignZGlzYWJsZWQnLCB0cnVlKTtcblx0XHRcblx0XHQkLmFqYXgoe1xuXHRcdFx0dXJsLFxuXHRcdFx0ZGF0YSxcblx0XHRcdG1ldGhvZDogJ1BPU1QnXG5cdFx0fSlcblx0XHRcdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdGNvbnN0IGNvbnRlbnQgPSBkYXRhLm5vdGlmeUN1c3RvbWVyID9cblx0XHRcdFx0ICAgICAgICAgICAgICAgIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdNQUlMX1NVQ0NFU1MnLCAnZ21fc2VuZF9vcmRlcicpIDpcblx0XHRcdFx0ICAgICAgICAgICAgICAgIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTVUNDRVNTX09SREVSX1VQREFURUQnLCAnb3JkZXJzJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoJCgnLm9yZGVycy5vdmVydmlldycpLmxlbmd0aCkge1xuXHRcdFx0XHRcdCQoJy5vcmRlcnMgLnRhYmxlLW1haW4nKS5EYXRhVGFibGUoKS5hamF4LnJlbG9hZChudWxsLCBmYWxzZSk7XG5cdFx0XHRcdFx0JCgnLm9yZGVycyAudGFibGUtbWFpbicpLm9yZGVyc19vdmVydmlld19maWx0ZXIoJ3JlbG9hZCcpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdCQoJy5pbnZvaWNlcyAudGFibGUtbWFpbicpLkRhdGFUYWJsZSgpLmFqYXgucmVsb2FkKG51bGwsIGZhbHNlKTtcblx0XHRcdFx0XHQkKCcuaW52b2ljZXMgLnRhYmxlLW1haW4nKS5pbnZvaWNlc19vdmVydmlld19maWx0ZXIoJ3JlbG9hZCcpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTaG93IHN1Y2Nlc3MgbWVzc2FnZSBpbiB0aGUgYWRtaW4gaW5mbyBib3guXG5cdFx0XHRcdGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKGNvbnRlbnQpO1xuXHRcdFx0fSlcblx0XHRcdC5hbHdheXMoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCR0aGlzLm1vZGFsKCdoaWRlJyk7XG5cdFx0XHRcdCRzYXZlQnV0dG9uLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpLmF0dHIoJ2Rpc2FibGVkJywgZmFsc2UpO1xuXHRcdFx0fSk7XG5cdH1cblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBJTklUSUFMSVpBVElPTlxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdCR0aGlzLm9uKCdjbGljaycsICcuYnRuLnNhdmUnLCBfY2hhbmdlU3RhdHVzKTtcblx0XHRkb25lKCk7XG5cdH07XG5cdFxuXHRyZXR1cm4gbW9kdWxlO1xufSk7Il19
