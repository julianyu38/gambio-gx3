'use strict';

/* --------------------------------------------------------------
 events.js 2017-10-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Special Prices Table Events Controller
 *
 * Handles the events of the main QuickEdit table.
 */
gx.controllers.module('special_prices_events', ['modal', 'loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Bulk selection change event handler.
  *
  * @param {jQuery.Event} event Contains event information.
  * @param {Boolean} [propagate = true] Whether to propagate the event or not.
  */
	function _onBulkSelectionChange(event) {
		var propagate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		if (propagate === false) {
			return;
		}

		$this.find('tbody input:checkbox.special-price-row-selection').single_checkbox('checked', $(this).prop('checked')).trigger('change');
	}

	/**
  * Table row click event handler.
  *
  * @param {jQuery.Event} event Contains event information.
  */
	function _onTableRowClick(event) {
		if (!$(event.target).is('td')) {
			return;
		}

		var $singleCheckbox = $(this).find('input:checkbox.special-price-row-selection');

		$singleCheckbox.prop('checked', !$(this).find('input:checkbox.special-price-row-selection').prop('checked')).trigger('change');
	}

	/**
  * Enables row editing mode.
  */
	function _onTableRowEditClick() {
		var $tableRow = $(this).parents('tr');
		var $singleCheckbox = $(this).parents('tr').find('input:checkbox.special-price-row-selection');

		var $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
		var $saveAction = $rowActionsDropdown.find('.save-special-price-edits');
		var $editAction = $tableRow.find('.row-special-price-edit');
		$editAction.addClass('hidden');
		$saveAction.removeClass('hidden');
		jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $saveAction);

		$tableRow.find('td.editable').each(function () {
			if ($(this).find('input:text').length > 0) {
				return;
			}

			if ($(this).hasClass('date')) {
				this.innerHTML = '<input type="text" class="form-control datepicker" value="' + this.innerText + '" />';
			} else {
				this.innerHTML = '<input type="text" class="form-control" value="' + this.innerText + '" />';
			}
		});

		$singleCheckbox.prop('checked', !$(this).find('input:checkbox.special-price-row-selection').prop('checked')).trigger('change');

		$tableRow.find('.datepicker').datepicker({
			onSelect: _onTableRowDataChange,
			dateFormat: jse.core.config.get('languageCode') === 'de' ? 'dd.mm.yy' : 'mm.dd.yy'
		});
	}

	/**
  * Bulk row edit event handler.
  *
  * Enables the edit mode for the selected rows.
  */
	function _onTableBulkRowEditClick() {
		var $bulkEditAction = $(this);
		var $checkedSingleCheckboxes = $this.find('input:checkbox:checked.special-price-row-selection');

		if ($checkedSingleCheckboxes.length) {
			var $bulkActionsDropdown = $('.special-price-bulk-action');
			var $bulkSaveAction = $bulkActionsDropdown.find('.save-special-price-bulk-row-edits');
			$bulkEditAction.addClass('hidden');
			$bulkSaveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkSaveAction);
		}

		$checkedSingleCheckboxes.each(function () {
			var $tableRow = $(this).parents('tr');
			var $rowActionDropdown = $tableRow.find('.btn-group.dropdown');
			var $saveAction = $rowActionDropdown.find('.save-special-price-edits');
			var $editAction = $rowActionDropdown.find('.row-special-price-edit');

			$editAction.addClass('hidden');
			$saveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($rowActionDropdown, $saveAction);

			$tableRow.find('td.editable').each(function () {
				if ($(this).find('input:text').length > 0) {
					return;
				}

				if ($(this).hasClass('date')) {
					this.innerHTML = '<input type="text" class="form-control datepicker" value="' + this.innerText + '" />';
				} else {
					this.innerHTML = '<input type="text" class="form-control" value="' + this.innerText + '" />';
				}
			});

			$tableRow.find('.datepicker').datepicker({
				onSelect: _onTableRowDataChange
			});
		});
	}

	/**
  * Table row checkbox change event handler.
  */
	function _onTableRowCheckboxChange() {
		var $bulkActionDropdownButtons = $this.parents('.special-prices.modal').find('.special-price-bulk-action > button');
		var $tableRow = $(this).parents('tr');

		if ($this.find('input:checkbox:checked.special-price-row-selection').length > 0) {
			$bulkActionDropdownButtons.removeClass('disabled');
		} else {
			$bulkActionDropdownButtons.addClass('disabled');

			var $bulkActionsDropdown = $('.special-price-bulk-action');
			var $bulkSaveAction = $bulkActionsDropdown.find('.save-special-price-bulk-row-edits');

			if (!$bulkSaveAction.hasClass('hidden')) {
				var $bulkEditAction = $bulkActionsDropdown.find('.special-price-bulk-row-edit');
				$bulkEditAction.removeClass('hidden');
				$bulkSaveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkEditAction);
			}
		}

		$tableRow.find('p.values_price').each(function () {
			if ($(this).data('special-price-price-type') == 'fix') {
				$(this).parents('td').addClass('editable');
			}
		});

		if (!$(this).prop('checked')) {
			_resolveRowChanges($(this).parents('tr'));

			var $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
			var $saveAction = $rowActionsDropdown.find('.save-special-price-edits');

			if (!$saveAction.hasClass('hidden')) {
				var $editAction = $rowActionsDropdown.find('.row-special-price-edit');
				$editAction.removeClass('hidden');
				$saveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $editAction.last());
			}
		}
	}

	/**
  * Page mode change between "edit" and "filtering".
  */
	function _onPageModeChange() {
		if ($(this).val() == 'edit-mode') {
			$this.find('tr.special-price-filter').attr('hidden', true);
			$this.find('tr.special-price-edit').attr('hidden', false);
			$this.find('thead tr:first-child th').addClass('edit-mode');
		} else {
			$this.find('tr.special-price-filter').attr('hidden', false);
			$this.find('tr.special-price-edit').attr('hidden', true);
			$this.find('thead tr:first-child th').removeClass('edit-mode');
		}
	}

	/**
  * Restores all the row data changes back to their original state.
  *
  * @param {jQuery.Event} $row Current row selector.
  */
	function _resolveRowChanges($row) {
		var rowIndex = $this.DataTable().row($row).index();

		$row.find('input:text').each(function () {
			var $cell = $(this).closest('td');
			var columnIndex = $this.DataTable().column($cell).index();

			this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
		});
	}

	/**
  * Table row data change event handler.
  *
  * It's being triggered every time a row input/select field is changed.
  */
	function _onTableRowDataChange() {
		var $row = $(this).parents('tr');
		var rowIndex = $this.DataTable().row($row).index();

		$row.find('input:text').each(function () {
			var $cell = $(this).closest('td');
			var columnIndex = $this.DataTable().column($cell).index();

			if ($.trim($(this).val()) != $this.DataTable().cell(rowIndex, columnIndex).data()) {
				$(this).addClass('modified');

				return;
			}

			$(this).removeClass('modified');
		});
	}

	/**
  * Table row switcher change event handler.
  *
  * @param {HTMLElement} switcher Current switcher element.
  */
	function _onTableRowSwitcherChange(switcher) {
		var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditSpecialPricesAjax/Update';
		var specialPrice = $(switcher).parents('tr').attr('id');
		var $cell = $(switcher).closest('td');
		var columnIndex = $this.DataTable().column($cell).index();
		var columnName = $this.find('tr.special-price-filter th').eq(columnIndex).data('columnName');
		var data = {};
		var value = {};

		value[columnName] = $(switcher).prop('checked') ? 1 : 0;
		data[specialPrice] = value;

		$.post(url, {
			data: data,
			pageToken: jse.core.config.get('pageToken')
		}).done(function (response) {
			response = $.parseJSON(response);

			if (response.success) {
				return;
			}

			var title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
			var message = jse.core.lang.translate('EDIT_ERROR', 'admin_quick_edit');
			var buttons = [{
				title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
				callback: function callback(event) {
					return $(event.currentTarget).parents('.modal').modal('hide');
				}
			}];

			jse.libs.modal.showMessage(title, message, buttons);
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', 'tbody tr', _onTableRowClick).on('click', '.row-special-price-edit', _onTableRowEditClick).on('change', '.special-price-bulk-selection', _onBulkSelectionChange).on('keyup', 'tbody tr input:text', _onTableRowDataChange);

		$this.parents('.special-prices.modal').on('change', 'input:checkbox.special-price-row-selection', _onTableRowCheckboxChange).on('change', '.select-special-price-page-mode', _onPageModeChange).on('click', '.btn-group .special-price-bulk-row-edit', _onTableBulkRowEditClick);

		$this.on('draw.dt', function () {
			$this.find('.convert-to-switcher').each(function (index, switcher) {
				var skipInitialEvent = true;
				$(switcher).on('change', function () {
					if (skipInitialEvent) {
						skipInitialEvent = false;
						return;
					}

					_onTableRowSwitcherChange(switcher);
				});
			});

			_onTableRowCheckboxChange();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3NwZWNpYWxfcHJpY2VzL3NwZWNpYWxfcHJpY2VzX2V2ZW50cy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIl9vbkJ1bGtTZWxlY3Rpb25DaGFuZ2UiLCJldmVudCIsInByb3BhZ2F0ZSIsImZpbmQiLCJzaW5nbGVfY2hlY2tib3giLCJwcm9wIiwidHJpZ2dlciIsIl9vblRhYmxlUm93Q2xpY2siLCJ0YXJnZXQiLCJpcyIsIiRzaW5nbGVDaGVja2JveCIsIl9vblRhYmxlUm93RWRpdENsaWNrIiwiJHRhYmxlUm93IiwicGFyZW50cyIsIiRyb3dBY3Rpb25zRHJvcGRvd24iLCIkc2F2ZUFjdGlvbiIsIiRlZGl0QWN0aW9uIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImpzZSIsImxpYnMiLCJidXR0b25fZHJvcGRvd24iLCJzZXREZWZhdWx0QWN0aW9uIiwiZWFjaCIsImxlbmd0aCIsImhhc0NsYXNzIiwiaW5uZXJIVE1MIiwiaW5uZXJUZXh0IiwiZGF0ZXBpY2tlciIsIm9uU2VsZWN0IiwiX29uVGFibGVSb3dEYXRhQ2hhbmdlIiwiZGF0ZUZvcm1hdCIsImNvcmUiLCJjb25maWciLCJnZXQiLCJfb25UYWJsZUJ1bGtSb3dFZGl0Q2xpY2siLCIkYnVsa0VkaXRBY3Rpb24iLCIkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMiLCIkYnVsa0FjdGlvbnNEcm9wZG93biIsIiRidWxrU2F2ZUFjdGlvbiIsIiRyb3dBY3Rpb25Ecm9wZG93biIsIl9vblRhYmxlUm93Q2hlY2tib3hDaGFuZ2UiLCIkYnVsa0FjdGlvbkRyb3Bkb3duQnV0dG9ucyIsIl9yZXNvbHZlUm93Q2hhbmdlcyIsImxhc3QiLCJfb25QYWdlTW9kZUNoYW5nZSIsInZhbCIsImF0dHIiLCIkcm93Iiwicm93SW5kZXgiLCJEYXRhVGFibGUiLCJyb3ciLCJpbmRleCIsIiRjZWxsIiwiY2xvc2VzdCIsImNvbHVtbkluZGV4IiwiY29sdW1uIiwicGFyZW50RWxlbWVudCIsImNlbGwiLCJ0cmltIiwiX29uVGFibGVSb3dTd2l0Y2hlckNoYW5nZSIsInN3aXRjaGVyIiwidXJsIiwic3BlY2lhbFByaWNlIiwiY29sdW1uTmFtZSIsImVxIiwidmFsdWUiLCJwb3N0IiwicGFnZVRva2VuIiwiZG9uZSIsInJlc3BvbnNlIiwicGFyc2VKU09OIiwic3VjY2VzcyIsInRpdGxlIiwibGFuZyIsInRyYW5zbGF0ZSIsIm1lc3NhZ2UiLCJidXR0b25zIiwiY2FsbGJhY2siLCJjdXJyZW50VGFyZ2V0IiwibW9kYWwiLCJzaG93TWVzc2FnZSIsImluaXQiLCJvbiIsInNraXBJbml0aWFsRXZlbnQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQXNCLHVCQUF0QixFQUErQyxDQUFDLE9BQUQsRUFBVSxpQkFBVixDQUEvQyxFQUE2RSxVQUFTQyxJQUFULEVBQWU7O0FBRTNGOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUgsU0FBUyxFQUFmOztBQUdBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUEsVUFBU0ksc0JBQVQsQ0FBZ0NDLEtBQWhDLEVBQXlEO0FBQUEsTUFBbEJDLFNBQWtCLHVFQUFOLElBQU07O0FBQ3hELE1BQUlBLGNBQWMsS0FBbEIsRUFBeUI7QUFDeEI7QUFDQTs7QUFFREosUUFBTUssSUFBTixDQUFXLGtEQUFYLEVBQ0VDLGVBREYsQ0FDa0IsU0FEbEIsRUFDNkJMLEVBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsU0FBYixDQUQ3QixFQUVFQyxPQUZGLENBRVUsUUFGVjtBQUdBOztBQUVEOzs7OztBQUtBLFVBQVNDLGdCQUFULENBQTBCTixLQUExQixFQUFpQztBQUNoQyxNQUFJLENBQUNGLEVBQUVFLE1BQU1PLE1BQVIsRUFBZ0JDLEVBQWhCLENBQW1CLElBQW5CLENBQUwsRUFBK0I7QUFDOUI7QUFDQTs7QUFFRCxNQUFNQyxrQkFBa0JYLEVBQUUsSUFBRixFQUFRSSxJQUFSLENBQWEsNENBQWIsQ0FBeEI7O0FBRUFPLGtCQUFnQkwsSUFBaEIsQ0FBcUIsU0FBckIsRUFBZ0MsQ0FBQ04sRUFBRSxJQUFGLEVBQy9CSSxJQUQrQixDQUMxQiw0Q0FEMEIsRUFFL0JFLElBRitCLENBRTFCLFNBRjBCLENBQWpDLEVBR0VDLE9BSEYsQ0FHVSxRQUhWO0FBSUE7O0FBRUQ7OztBQUdBLFVBQVNLLG9CQUFULEdBQWdDO0FBQy9CLE1BQU1DLFlBQVliLEVBQUUsSUFBRixFQUFRYyxPQUFSLENBQWdCLElBQWhCLENBQWxCO0FBQ0EsTUFBTUgsa0JBQWtCWCxFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixFQUFzQlYsSUFBdEIsQ0FBMkIsNENBQTNCLENBQXhCOztBQUVBLE1BQU1XLHNCQUFzQkYsVUFBVVQsSUFBVixDQUFlLHFCQUFmLENBQTVCO0FBQ0EsTUFBTVksY0FBY0Qsb0JBQW9CWCxJQUFwQixDQUF5QiwyQkFBekIsQ0FBcEI7QUFDQSxNQUFNYSxjQUFjSixVQUFVVCxJQUFWLENBQWUseUJBQWYsQ0FBcEI7QUFDQWEsY0FBWUMsUUFBWixDQUFxQixRQUFyQjtBQUNBRixjQUFZRyxXQUFaLENBQXdCLFFBQXhCO0FBQ0FDLE1BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsZ0JBQXpCLENBQTBDUixtQkFBMUMsRUFBK0RDLFdBQS9EOztBQUVBSCxZQUFVVCxJQUFWLENBQWUsYUFBZixFQUE4Qm9CLElBQTlCLENBQW1DLFlBQVc7QUFDN0MsT0FBSXhCLEVBQUUsSUFBRixFQUFRSSxJQUFSLENBQWEsWUFBYixFQUEyQnFCLE1BQTNCLEdBQW9DLENBQXhDLEVBQTJDO0FBQzFDO0FBQ0E7O0FBRUQsT0FBSXpCLEVBQUUsSUFBRixFQUFRMEIsUUFBUixDQUFpQixNQUFqQixDQUFKLEVBQThCO0FBQzdCLFNBQUtDLFNBQUwsa0VBQThFLEtBQUtDLFNBQW5GO0FBQ0EsSUFGRCxNQUVPO0FBQ04sU0FBS0QsU0FBTCx1REFBbUUsS0FBS0MsU0FBeEU7QUFDQTtBQUNELEdBVkQ7O0FBWUFqQixrQkFBZ0JMLElBQWhCLENBQXFCLFNBQXJCLEVBQWdDLENBQUNOLEVBQUUsSUFBRixFQUMvQkksSUFEK0IsQ0FDMUIsNENBRDBCLEVBRS9CRSxJQUYrQixDQUUxQixTQUYwQixDQUFqQyxFQUdFQyxPQUhGLENBR1UsUUFIVjs7QUFLQU0sWUFBVVQsSUFBVixDQUFlLGFBQWYsRUFBOEJ5QixVQUE5QixDQUF5QztBQUN4Q0MsYUFBVUMscUJBRDhCO0FBRXhDQyxlQUFZWixJQUFJYSxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLE1BQXdDLElBQXhDLEdBQStDLFVBQS9DLEdBQTREO0FBRmhDLEdBQXpDO0FBSUE7O0FBRUQ7Ozs7O0FBS0EsVUFBU0Msd0JBQVQsR0FBb0M7QUFDbkMsTUFBTUMsa0JBQWtCckMsRUFBRSxJQUFGLENBQXhCO0FBQ0EsTUFBTXNDLDJCQUEyQnZDLE1BQU1LLElBQU4sQ0FBVyxvREFBWCxDQUFqQzs7QUFFQSxNQUFJa0MseUJBQXlCYixNQUE3QixFQUFxQztBQUNwQyxPQUFNYyx1QkFBdUJ2QyxFQUFFLDRCQUFGLENBQTdCO0FBQ0EsT0FBTXdDLGtCQUFrQkQscUJBQXFCbkMsSUFBckIsQ0FBMEIsb0NBQTFCLENBQXhCO0FBQ0FpQyxtQkFBZ0JuQixRQUFoQixDQUF5QixRQUF6QjtBQUNBc0IsbUJBQWdCckIsV0FBaEIsQ0FBNEIsUUFBNUI7QUFDQUMsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxnQkFBekIsQ0FBMENnQixvQkFBMUMsRUFBZ0VDLGVBQWhFO0FBQ0E7O0FBRURGLDJCQUF5QmQsSUFBekIsQ0FBOEIsWUFBVztBQUN4QyxPQUFNWCxZQUFZYixFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixDQUFsQjtBQUNBLE9BQU0yQixxQkFBcUI1QixVQUFVVCxJQUFWLENBQWUscUJBQWYsQ0FBM0I7QUFDQSxPQUFNWSxjQUFjeUIsbUJBQW1CckMsSUFBbkIsQ0FBd0IsMkJBQXhCLENBQXBCO0FBQ0EsT0FBTWEsY0FBY3dCLG1CQUFtQnJDLElBQW5CLENBQXdCLHlCQUF4QixDQUFwQjs7QUFFQWEsZUFBWUMsUUFBWixDQUFxQixRQUFyQjtBQUNBRixlQUFZRyxXQUFaLENBQXdCLFFBQXhCO0FBQ0FDLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsZ0JBQXpCLENBQTBDa0Isa0JBQTFDLEVBQThEekIsV0FBOUQ7O0FBRUFILGFBQVVULElBQVYsQ0FBZSxhQUFmLEVBQThCb0IsSUFBOUIsQ0FBbUMsWUFBVztBQUM3QyxRQUFJeEIsRUFBRSxJQUFGLEVBQVFJLElBQVIsQ0FBYSxZQUFiLEVBQTJCcUIsTUFBM0IsR0FBb0MsQ0FBeEMsRUFBMkM7QUFDMUM7QUFDQTs7QUFFRCxRQUFJekIsRUFBRSxJQUFGLEVBQVEwQixRQUFSLENBQWlCLE1BQWpCLENBQUosRUFBOEI7QUFDN0IsVUFBS0MsU0FBTCxrRUFBOEUsS0FBS0MsU0FBbkY7QUFDQSxLQUZELE1BRU87QUFDTixVQUFLRCxTQUFMLHVEQUFtRSxLQUFLQyxTQUF4RTtBQUNBO0FBQ0QsSUFWRDs7QUFZQWYsYUFBVVQsSUFBVixDQUFlLGFBQWYsRUFBOEJ5QixVQUE5QixDQUF5QztBQUN4Q0MsY0FBVUM7QUFEOEIsSUFBekM7QUFHQSxHQXpCRDtBQTBCQTs7QUFFRDs7O0FBR0EsVUFBU1cseUJBQVQsR0FBcUM7QUFDcEMsTUFBTUMsNkJBQTZCNUMsTUFBTWUsT0FBTixDQUFjLHVCQUFkLEVBQ2pDVixJQURpQyxDQUM1QixxQ0FENEIsQ0FBbkM7QUFFQSxNQUFNUyxZQUFZYixFQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixJQUFoQixDQUFsQjs7QUFFQSxNQUFJZixNQUFNSyxJQUFOLENBQVcsb0RBQVgsRUFBaUVxQixNQUFqRSxHQUEwRSxDQUE5RSxFQUFpRjtBQUNoRmtCLDhCQUEyQnhCLFdBQTNCLENBQXVDLFVBQXZDO0FBQ0EsR0FGRCxNQUVPO0FBQ053Qiw4QkFBMkJ6QixRQUEzQixDQUFvQyxVQUFwQzs7QUFFQSxPQUFNcUIsdUJBQXVCdkMsRUFBRSw0QkFBRixDQUE3QjtBQUNBLE9BQU13QyxrQkFBa0JELHFCQUFxQm5DLElBQXJCLENBQTBCLG9DQUExQixDQUF4Qjs7QUFFQSxPQUFJLENBQUNvQyxnQkFBZ0JkLFFBQWhCLENBQXlCLFFBQXpCLENBQUwsRUFBeUM7QUFDeEMsUUFBTVcsa0JBQWtCRSxxQkFBcUJuQyxJQUFyQixDQUEwQiw4QkFBMUIsQ0FBeEI7QUFDQWlDLG9CQUFnQmxCLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0FxQixvQkFBZ0J0QixRQUFoQixDQUF5QixRQUF6QjtBQUNBRSxRQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLGdCQUF6QixDQUEwQ2dCLG9CQUExQyxFQUFnRUYsZUFBaEU7QUFDQTtBQUNEOztBQUVEeEIsWUFBVVQsSUFBVixDQUFlLGdCQUFmLEVBQWlDb0IsSUFBakMsQ0FBc0MsWUFBVztBQUNoRCxPQUFJeEIsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSwwQkFBYixLQUE0QyxLQUFoRCxFQUF1RDtBQUN0REUsTUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JJLFFBQXRCLENBQStCLFVBQS9CO0FBQ0E7QUFDRCxHQUpEOztBQU1BLE1BQUksQ0FBQ2xCLEVBQUUsSUFBRixFQUFRTSxJQUFSLENBQWEsU0FBYixDQUFMLEVBQThCO0FBQzdCc0Msc0JBQW1CNUMsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbkI7O0FBRUEsT0FBTUMsc0JBQXNCRixVQUFVVCxJQUFWLENBQWUscUJBQWYsQ0FBNUI7QUFDQSxPQUFNWSxjQUFjRCxvQkFBb0JYLElBQXBCLENBQXlCLDJCQUF6QixDQUFwQjs7QUFFQSxPQUFJLENBQUNZLFlBQVlVLFFBQVosQ0FBcUIsUUFBckIsQ0FBTCxFQUFxQztBQUNwQyxRQUFNVCxjQUFjRixvQkFBb0JYLElBQXBCLENBQXlCLHlCQUF6QixDQUFwQjtBQUNBYSxnQkFBWUUsV0FBWixDQUF3QixRQUF4QjtBQUNBSCxnQkFBWUUsUUFBWixDQUFxQixRQUFyQjtBQUNBRSxRQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLGdCQUF6QixDQUEwQ1IsbUJBQTFDLEVBQStERSxZQUFZNEIsSUFBWixFQUEvRDtBQUNBO0FBQ0Q7QUFDRDs7QUFFRDs7O0FBR0EsVUFBU0MsaUJBQVQsR0FBNkI7QUFDNUIsTUFBSTlDLEVBQUUsSUFBRixFQUFRK0MsR0FBUixNQUFpQixXQUFyQixFQUFrQztBQUNqQ2hELFNBQU1LLElBQU4sQ0FBVyx5QkFBWCxFQUFzQzRDLElBQXRDLENBQTJDLFFBQTNDLEVBQXFELElBQXJEO0FBQ0FqRCxTQUFNSyxJQUFOLENBQVcsdUJBQVgsRUFBb0M0QyxJQUFwQyxDQUF5QyxRQUF6QyxFQUFtRCxLQUFuRDtBQUNBakQsU0FBTUssSUFBTixDQUFXLHlCQUFYLEVBQXNDYyxRQUF0QyxDQUErQyxXQUEvQztBQUNBLEdBSkQsTUFJTztBQUNObkIsU0FBTUssSUFBTixDQUFXLHlCQUFYLEVBQXNDNEMsSUFBdEMsQ0FBMkMsUUFBM0MsRUFBcUQsS0FBckQ7QUFDQWpELFNBQU1LLElBQU4sQ0FBVyx1QkFBWCxFQUFvQzRDLElBQXBDLENBQXlDLFFBQXpDLEVBQW1ELElBQW5EO0FBQ0FqRCxTQUFNSyxJQUFOLENBQVcseUJBQVgsRUFBc0NlLFdBQXRDLENBQWtELFdBQWxEO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7QUFLQSxVQUFTeUIsa0JBQVQsQ0FBNEJLLElBQTVCLEVBQWtDO0FBQ2pDLE1BQU1DLFdBQVduRCxNQUFNb0QsU0FBTixHQUFrQkMsR0FBbEIsQ0FBc0JILElBQXRCLEVBQTRCSSxLQUE1QixFQUFqQjs7QUFFQUosT0FBSzdDLElBQUwsQ0FBVSxZQUFWLEVBQXdCb0IsSUFBeEIsQ0FBNkIsWUFBVztBQUN2QyxPQUFNOEIsUUFBUXRELEVBQUUsSUFBRixFQUFRdUQsT0FBUixDQUFnQixJQUFoQixDQUFkO0FBQ0EsT0FBTUMsY0FBY3pELE1BQU1vRCxTQUFOLEdBQWtCTSxNQUFsQixDQUF5QkgsS0FBekIsRUFBZ0NELEtBQWhDLEVBQXBCOztBQUVBLFFBQUtLLGFBQUwsQ0FBbUIvQixTQUFuQixHQUErQjVCLE1BQU1vRCxTQUFOLEdBQWtCUSxJQUFsQixDQUF1QlQsUUFBdkIsRUFBaUNNLFdBQWpDLEVBQThDMUQsSUFBOUMsRUFBL0I7QUFDQSxHQUxEO0FBTUE7O0FBRUQ7Ozs7O0FBS0EsVUFBU2lDLHFCQUFULEdBQWlDO0FBQ2hDLE1BQU1rQixPQUFPakQsRUFBRSxJQUFGLEVBQVFjLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBYjtBQUNBLE1BQU1vQyxXQUFXbkQsTUFBTW9ELFNBQU4sR0FBa0JDLEdBQWxCLENBQXNCSCxJQUF0QixFQUE0QkksS0FBNUIsRUFBakI7O0FBRUFKLE9BQUs3QyxJQUFMLENBQVUsWUFBVixFQUF3Qm9CLElBQXhCLENBQTZCLFlBQVc7QUFDdkMsT0FBTThCLFFBQVF0RCxFQUFFLElBQUYsRUFBUXVELE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBZDtBQUNBLE9BQU1DLGNBQWN6RCxNQUFNb0QsU0FBTixHQUFrQk0sTUFBbEIsQ0FBeUJILEtBQXpCLEVBQWdDRCxLQUFoQyxFQUFwQjs7QUFFQSxPQUFJckQsRUFBRTRELElBQUYsQ0FBTzVELEVBQUUsSUFBRixFQUFRK0MsR0FBUixFQUFQLEtBQXlCaEQsTUFBTW9ELFNBQU4sR0FBa0JRLElBQWxCLENBQXVCVCxRQUF2QixFQUFpQ00sV0FBakMsRUFBOEMxRCxJQUE5QyxFQUE3QixFQUFtRjtBQUNsRkUsTUFBRSxJQUFGLEVBQVFrQixRQUFSLENBQWlCLFVBQWpCOztBQUVBO0FBQ0E7O0FBRURsQixLQUFFLElBQUYsRUFBUW1CLFdBQVIsQ0FBb0IsVUFBcEI7QUFDQSxHQVhEO0FBWUE7O0FBRUQ7Ozs7O0FBS0EsVUFBUzBDLHlCQUFULENBQW1DQyxRQUFuQyxFQUE2QztBQUM1QyxNQUFNQyxNQUFNM0MsSUFBSWEsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyx1REFBNUM7QUFDQSxNQUFNNkIsZUFBZWhFLEVBQUU4RCxRQUFGLEVBQVloRCxPQUFaLENBQW9CLElBQXBCLEVBQTBCa0MsSUFBMUIsQ0FBK0IsSUFBL0IsQ0FBckI7QUFDQSxNQUFNTSxRQUFRdEQsRUFBRThELFFBQUYsRUFBWVAsT0FBWixDQUFvQixJQUFwQixDQUFkO0FBQ0EsTUFBTUMsY0FBY3pELE1BQU1vRCxTQUFOLEdBQWtCTSxNQUFsQixDQUF5QkgsS0FBekIsRUFBZ0NELEtBQWhDLEVBQXBCO0FBQ0EsTUFBTVksYUFBYWxFLE1BQU1LLElBQU4sQ0FBVyw0QkFBWCxFQUF5QzhELEVBQXpDLENBQTRDVixXQUE1QyxFQUF5RDFELElBQXpELENBQThELFlBQTlELENBQW5CO0FBQ0EsTUFBTUEsT0FBTyxFQUFiO0FBQ0EsTUFBTXFFLFFBQVEsRUFBZDs7QUFFQUEsUUFBTUYsVUFBTixJQUFvQmpFLEVBQUU4RCxRQUFGLEVBQVl4RCxJQUFaLENBQWlCLFNBQWpCLElBQThCLENBQTlCLEdBQWtDLENBQXREO0FBQ0FSLE9BQUtrRSxZQUFMLElBQXFCRyxLQUFyQjs7QUFFQW5FLElBQUVvRSxJQUFGLENBQU9MLEdBQVAsRUFBWTtBQUNYakUsYUFEVztBQUVYdUUsY0FBV2pELElBQUlhLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFGQSxHQUFaLEVBR0dtQyxJQUhILENBR1Esb0JBQVk7QUFDbkJDLGNBQVd2RSxFQUFFd0UsU0FBRixDQUFZRCxRQUFaLENBQVg7O0FBRUEsT0FBSUEsU0FBU0UsT0FBYixFQUFzQjtBQUNyQjtBQUNBOztBQUVELE9BQU1DLFFBQVF0RCxJQUFJYSxJQUFKLENBQVMwQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isa0JBQXhCLEVBQTRDLGtCQUE1QyxDQUFkO0FBQ0EsT0FBTUMsVUFBVXpELElBQUlhLElBQUosQ0FBUzBDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixZQUF4QixFQUFzQyxrQkFBdEMsQ0FBaEI7QUFDQSxPQUFNRSxVQUFVLENBQ2Y7QUFDQ0osV0FBT3RELElBQUlhLElBQUosQ0FBUzBDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxrQkFBeEMsQ0FEUjtBQUVDRyxjQUFVO0FBQUEsWUFBUy9FLEVBQUVFLE1BQU04RSxhQUFSLEVBQXVCbEUsT0FBdkIsQ0FBK0IsUUFBL0IsRUFBeUNtRSxLQUF6QyxDQUErQyxNQUEvQyxDQUFUO0FBQUE7QUFGWCxJQURlLENBQWhCOztBQU9BN0QsT0FBSUMsSUFBSixDQUFTNEQsS0FBVCxDQUFlQyxXQUFmLENBQTJCUixLQUEzQixFQUFrQ0csT0FBbEMsRUFBMkNDLE9BQTNDO0FBQ0EsR0FwQkQ7QUFxQkE7O0FBRUQ7QUFDQTtBQUNBOztBQUVBakYsUUFBT3NGLElBQVAsR0FBYyxVQUFTYixJQUFULEVBQWU7QUFDNUJ2RSxRQUNFcUYsRUFERixDQUNLLE9BREwsRUFDYyxVQURkLEVBQzBCNUUsZ0JBRDFCLEVBRUU0RSxFQUZGLENBRUssT0FGTCxFQUVjLHlCQUZkLEVBRXlDeEUsb0JBRnpDLEVBR0V3RSxFQUhGLENBR0ssUUFITCxFQUdlLCtCQUhmLEVBR2dEbkYsc0JBSGhELEVBSUVtRixFQUpGLENBSUssT0FKTCxFQUljLHFCQUpkLEVBSXFDckQscUJBSnJDOztBQU1BaEMsUUFBTWUsT0FBTixDQUFjLHVCQUFkLEVBQ0VzRSxFQURGLENBQ0ssUUFETCxFQUNlLDRDQURmLEVBQzZEMUMseUJBRDdELEVBRUUwQyxFQUZGLENBRUssUUFGTCxFQUVlLGlDQUZmLEVBRWtEdEMsaUJBRmxELEVBR0VzQyxFQUhGLENBR0ssT0FITCxFQUdjLHlDQUhkLEVBR3lEaEQsd0JBSHpEOztBQUtBckMsUUFBTXFGLEVBQU4sQ0FBUyxTQUFULEVBQW9CLFlBQU07QUFDekJyRixTQUFNSyxJQUFOLENBQVcsc0JBQVgsRUFBbUNvQixJQUFuQyxDQUF3QyxVQUFDNkIsS0FBRCxFQUFRUyxRQUFSLEVBQXFCO0FBQzVELFFBQUl1QixtQkFBbUIsSUFBdkI7QUFDQXJGLE1BQUU4RCxRQUFGLEVBQVlzQixFQUFaLENBQWUsUUFBZixFQUF5QixZQUFNO0FBQzlCLFNBQUlDLGdCQUFKLEVBQXNCO0FBQ3JCQSx5QkFBbUIsS0FBbkI7QUFDQTtBQUNBOztBQUVEeEIsK0JBQTBCQyxRQUExQjtBQUNBLEtBUEQ7QUFRQSxJQVZEOztBQVlBcEI7QUFDQSxHQWREOztBQWdCQTRCO0FBQ0EsRUE3QkQ7O0FBK0JBLFFBQU96RSxNQUFQO0FBQ0EsQ0E5VEQiLCJmaWxlIjoicXVpY2tfZWRpdC9tb2RhbHMvc3BlY2lhbF9wcmljZXMvc3BlY2lhbF9wcmljZXNfZXZlbnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBldmVudHMuanMgMjAxNy0xMC0xMFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogU3BlY2lhbCBQcmljZXMgVGFibGUgRXZlbnRzIENvbnRyb2xsZXJcbiAqXG4gKiBIYW5kbGVzIHRoZSBldmVudHMgb2YgdGhlIG1haW4gUXVpY2tFZGl0IHRhYmxlLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ3NwZWNpYWxfcHJpY2VzX2V2ZW50cycsIFsnbW9kYWwnLCAnbG9hZGluZ19zcGlubmVyJ10sIGZ1bmN0aW9uKGRhdGEpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBWQVJJQUJMRVNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdCAqXG5cdCAqIEB0eXBlIHtqUXVlcnl9XG5cdCAqL1xuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFxuXHQvKipcblx0ICogTW9kdWxlIEluc3RhbmNlXG5cdCAqXG5cdCAqIEB0eXBlIHtPYmplY3R9XG5cdCAqL1xuXHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gRlVOQ1RJT05TXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIEJ1bGsgc2VsZWN0aW9uIGNoYW5nZSBldmVudCBoYW5kbGVyLlxuXHQgKlxuXHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgQ29udGFpbnMgZXZlbnQgaW5mb3JtYXRpb24uXG5cdCAqIEBwYXJhbSB7Qm9vbGVhbn0gW3Byb3BhZ2F0ZSA9IHRydWVdIFdoZXRoZXIgdG8gcHJvcGFnYXRlIHRoZSBldmVudCBvciBub3QuXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25CdWxrU2VsZWN0aW9uQ2hhbmdlKGV2ZW50LCBwcm9wYWdhdGUgPSB0cnVlKSB7XG5cdFx0aWYgKHByb3BhZ2F0ZSA9PT0gZmFsc2UpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0XG5cdFx0JHRoaXMuZmluZCgndGJvZHkgaW5wdXQ6Y2hlY2tib3guc3BlY2lhbC1wcmljZS1yb3ctc2VsZWN0aW9uJylcblx0XHRcdC5zaW5nbGVfY2hlY2tib3goJ2NoZWNrZWQnLCAkKHRoaXMpLnByb3AoJ2NoZWNrZWQnKSlcblx0XHRcdC50cmlnZ2VyKCdjaGFuZ2UnKTtcblx0fVxuXHRcblx0LyoqXG5cdCAqIFRhYmxlIHJvdyBjbGljayBldmVudCBoYW5kbGVyLlxuXHQgKlxuXHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgQ29udGFpbnMgZXZlbnQgaW5mb3JtYXRpb24uXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25UYWJsZVJvd0NsaWNrKGV2ZW50KSB7XG5cdFx0aWYgKCEkKGV2ZW50LnRhcmdldCkuaXMoJ3RkJykpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0XG5cdFx0Y29uc3QgJHNpbmdsZUNoZWNrYm94ID0gJCh0aGlzKS5maW5kKCdpbnB1dDpjaGVja2JveC5zcGVjaWFsLXByaWNlLXJvdy1zZWxlY3Rpb24nKTtcblx0XHRcblx0XHQkc2luZ2xlQ2hlY2tib3gucHJvcCgnY2hlY2tlZCcsICEkKHRoaXMpXG5cdFx0XHQuZmluZCgnaW5wdXQ6Y2hlY2tib3guc3BlY2lhbC1wcmljZS1yb3ctc2VsZWN0aW9uJylcblx0XHRcdC5wcm9wKCdjaGVja2VkJykpXG5cdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBFbmFibGVzIHJvdyBlZGl0aW5nIG1vZGUuXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25UYWJsZVJvd0VkaXRDbGljaygpIHtcblx0XHRjb25zdCAkdGFibGVSb3cgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJyk7XG5cdFx0Y29uc3QgJHNpbmdsZUNoZWNrYm94ID0gJCh0aGlzKS5wYXJlbnRzKCd0cicpLmZpbmQoJ2lucHV0OmNoZWNrYm94LnNwZWNpYWwtcHJpY2Utcm93LXNlbGVjdGlvbicpO1xuXHRcdFxuXHRcdGNvbnN0ICRyb3dBY3Rpb25zRHJvcGRvd24gPSAkdGFibGVSb3cuZmluZCgnLmJ0bi1ncm91cC5kcm9wZG93bicpO1xuXHRcdGNvbnN0ICRzYXZlQWN0aW9uID0gJHJvd0FjdGlvbnNEcm9wZG93bi5maW5kKCcuc2F2ZS1zcGVjaWFsLXByaWNlLWVkaXRzJyk7XG5cdFx0Y29uc3QgJGVkaXRBY3Rpb24gPSAkdGFibGVSb3cuZmluZCgnLnJvdy1zcGVjaWFsLXByaWNlLWVkaXQnKTtcblx0XHQkZWRpdEFjdGlvbi5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0JHNhdmVBY3Rpb24ucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5zZXREZWZhdWx0QWN0aW9uKCRyb3dBY3Rpb25zRHJvcGRvd24sICRzYXZlQWN0aW9uKTtcblx0XHRcblx0XHQkdGFibGVSb3cuZmluZCgndGQuZWRpdGFibGUnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0aWYgKCQodGhpcykuZmluZCgnaW5wdXQ6dGV4dCcpLmxlbmd0aCA+IDApIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAoJCh0aGlzKS5oYXNDbGFzcygnZGF0ZScpKSB7XG5cdFx0XHRcdHRoaXMuaW5uZXJIVE1MID0gYDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiB2YWx1ZT1cIiR7dGhpcy5pbm5lclRleHR9XCIgLz5gO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0dGhpcy5pbm5lckhUTUwgPSBgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiB2YWx1ZT1cIiR7dGhpcy5pbm5lclRleHR9XCIgLz5gO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdCRzaW5nbGVDaGVja2JveC5wcm9wKCdjaGVja2VkJywgISQodGhpcylcblx0XHRcdC5maW5kKCdpbnB1dDpjaGVja2JveC5zcGVjaWFsLXByaWNlLXJvdy1zZWxlY3Rpb24nKVxuXHRcdFx0LnByb3AoJ2NoZWNrZWQnKSlcblx0XHRcdC50cmlnZ2VyKCdjaGFuZ2UnKTtcblx0XHRcblx0XHQkdGFibGVSb3cuZmluZCgnLmRhdGVwaWNrZXInKS5kYXRlcGlja2VyKHtcblx0XHRcdG9uU2VsZWN0OiBfb25UYWJsZVJvd0RhdGFDaGFuZ2UsXG5cdFx0XHRkYXRlRm9ybWF0OiBqc2UuY29yZS5jb25maWcuZ2V0KCdsYW5ndWFnZUNvZGUnKSA9PT0gJ2RlJyA/ICdkZC5tbS55eScgOiAnbW0uZGQueXknXG5cdFx0fSk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBCdWxrIHJvdyBlZGl0IGV2ZW50IGhhbmRsZXIuXG5cdCAqXG5cdCAqIEVuYWJsZXMgdGhlIGVkaXQgbW9kZSBmb3IgdGhlIHNlbGVjdGVkIHJvd3MuXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25UYWJsZUJ1bGtSb3dFZGl0Q2xpY2soKSB7XG5cdFx0Y29uc3QgJGJ1bGtFZGl0QWN0aW9uID0gJCh0aGlzKTtcblx0XHRjb25zdCAkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMgPSAkdGhpcy5maW5kKCdpbnB1dDpjaGVja2JveDpjaGVja2VkLnNwZWNpYWwtcHJpY2Utcm93LXNlbGVjdGlvbicpO1xuXHRcdFxuXHRcdGlmICgkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMubGVuZ3RoKSB7XG5cdFx0XHRjb25zdCAkYnVsa0FjdGlvbnNEcm9wZG93biA9ICQoJy5zcGVjaWFsLXByaWNlLWJ1bGstYWN0aW9uJyk7XG5cdFx0XHRjb25zdCAkYnVsa1NhdmVBY3Rpb24gPSAkYnVsa0FjdGlvbnNEcm9wZG93bi5maW5kKCcuc2F2ZS1zcGVjaWFsLXByaWNlLWJ1bGstcm93LWVkaXRzJyk7XG5cdFx0XHQkYnVsa0VkaXRBY3Rpb24uYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0JGJ1bGtTYXZlQWN0aW9uLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5zZXREZWZhdWx0QWN0aW9uKCRidWxrQWN0aW9uc0Ryb3Bkb3duLCAkYnVsa1NhdmVBY3Rpb24pO1xuXHRcdH1cblx0XHRcblx0XHQkY2hlY2tlZFNpbmdsZUNoZWNrYm94ZXMuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdGNvbnN0ICR0YWJsZVJvdyA9ICQodGhpcykucGFyZW50cygndHInKTtcblx0XHRcdGNvbnN0ICRyb3dBY3Rpb25Ecm9wZG93biA9ICR0YWJsZVJvdy5maW5kKCcuYnRuLWdyb3VwLmRyb3Bkb3duJyk7XG5cdFx0XHRjb25zdCAkc2F2ZUFjdGlvbiA9ICRyb3dBY3Rpb25Ecm9wZG93bi5maW5kKCcuc2F2ZS1zcGVjaWFsLXByaWNlLWVkaXRzJyk7XG5cdFx0XHRjb25zdCAkZWRpdEFjdGlvbiA9ICRyb3dBY3Rpb25Ecm9wZG93bi5maW5kKCcucm93LXNwZWNpYWwtcHJpY2UtZWRpdCcpO1xuXHRcdFx0XG5cdFx0XHQkZWRpdEFjdGlvbi5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHQkc2F2ZUFjdGlvbi5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uc2V0RGVmYXVsdEFjdGlvbigkcm93QWN0aW9uRHJvcGRvd24sICRzYXZlQWN0aW9uKTtcblx0XHRcdFxuXHRcdFx0JHRhYmxlUm93LmZpbmQoJ3RkLmVkaXRhYmxlJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKCQodGhpcykuZmluZCgnaW5wdXQ6dGV4dCcpLmxlbmd0aCA+IDApIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGlmICgkKHRoaXMpLmhhc0NsYXNzKCdkYXRlJykpIHtcblx0XHRcdFx0XHR0aGlzLmlubmVySFRNTCA9IGA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIgdmFsdWU9XCIke3RoaXMuaW5uZXJUZXh0fVwiIC8+YDtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR0aGlzLmlubmVySFRNTCA9IGA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHZhbHVlPVwiJHt0aGlzLmlubmVyVGV4dH1cIiAvPmA7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkdGFibGVSb3cuZmluZCgnLmRhdGVwaWNrZXInKS5kYXRlcGlja2VyKHtcblx0XHRcdFx0b25TZWxlY3Q6IF9vblRhYmxlUm93RGF0YUNoYW5nZVxuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBUYWJsZSByb3cgY2hlY2tib3ggY2hhbmdlIGV2ZW50IGhhbmRsZXIuXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25UYWJsZVJvd0NoZWNrYm94Q2hhbmdlKCkge1xuXHRcdGNvbnN0ICRidWxrQWN0aW9uRHJvcGRvd25CdXR0b25zID0gJHRoaXMucGFyZW50cygnLnNwZWNpYWwtcHJpY2VzLm1vZGFsJylcblx0XHRcdC5maW5kKCcuc3BlY2lhbC1wcmljZS1idWxrLWFjdGlvbiA+IGJ1dHRvbicpO1xuXHRcdGNvbnN0ICR0YWJsZVJvdyA9ICQodGhpcykucGFyZW50cygndHInKTtcblx0XHRcblx0XHRpZiAoJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3g6Y2hlY2tlZC5zcGVjaWFsLXByaWNlLXJvdy1zZWxlY3Rpb24nKS5sZW5ndGggPiAwKSB7XG5cdFx0XHQkYnVsa0FjdGlvbkRyb3Bkb3duQnV0dG9ucy5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0JGJ1bGtBY3Rpb25Ecm9wZG93bkJ1dHRvbnMuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0XHRcblx0XHRcdGNvbnN0ICRidWxrQWN0aW9uc0Ryb3Bkb3duID0gJCgnLnNwZWNpYWwtcHJpY2UtYnVsay1hY3Rpb24nKTtcblx0XHRcdGNvbnN0ICRidWxrU2F2ZUFjdGlvbiA9ICRidWxrQWN0aW9uc0Ryb3Bkb3duLmZpbmQoJy5zYXZlLXNwZWNpYWwtcHJpY2UtYnVsay1yb3ctZWRpdHMnKTtcblx0XHRcdFxuXHRcdFx0aWYgKCEkYnVsa1NhdmVBY3Rpb24uaGFzQ2xhc3MoJ2hpZGRlbicpKSB7XG5cdFx0XHRcdGNvbnN0ICRidWxrRWRpdEFjdGlvbiA9ICRidWxrQWN0aW9uc0Ryb3Bkb3duLmZpbmQoJy5zcGVjaWFsLXByaWNlLWJ1bGstcm93LWVkaXQnKTtcblx0XHRcdFx0JGJ1bGtFZGl0QWN0aW9uLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0JGJ1bGtTYXZlQWN0aW9uLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLnNldERlZmF1bHRBY3Rpb24oJGJ1bGtBY3Rpb25zRHJvcGRvd24sICRidWxrRWRpdEFjdGlvbik7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdCR0YWJsZVJvdy5maW5kKCdwLnZhbHVlc19wcmljZScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRpZiAoJCh0aGlzKS5kYXRhKCdzcGVjaWFsLXByaWNlLXByaWNlLXR5cGUnKSA9PSAnZml4Jykge1xuXHRcdFx0XHQkKHRoaXMpLnBhcmVudHMoJ3RkJykuYWRkQ2xhc3MoJ2VkaXRhYmxlJyk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0XG5cdFx0aWYgKCEkKHRoaXMpLnByb3AoJ2NoZWNrZWQnKSkge1xuXHRcdFx0X3Jlc29sdmVSb3dDaGFuZ2VzKCQodGhpcykucGFyZW50cygndHInKSk7XG5cdFx0XHRcblx0XHRcdGNvbnN0ICRyb3dBY3Rpb25zRHJvcGRvd24gPSAkdGFibGVSb3cuZmluZCgnLmJ0bi1ncm91cC5kcm9wZG93bicpO1xuXHRcdFx0Y29uc3QgJHNhdmVBY3Rpb24gPSAkcm93QWN0aW9uc0Ryb3Bkb3duLmZpbmQoJy5zYXZlLXNwZWNpYWwtcHJpY2UtZWRpdHMnKTtcblx0XHRcdFxuXHRcdFx0aWYgKCEkc2F2ZUFjdGlvbi5oYXNDbGFzcygnaGlkZGVuJykpIHtcblx0XHRcdFx0Y29uc3QgJGVkaXRBY3Rpb24gPSAkcm93QWN0aW9uc0Ryb3Bkb3duLmZpbmQoJy5yb3ctc3BlY2lhbC1wcmljZS1lZGl0Jyk7XG5cdFx0XHRcdCRlZGl0QWN0aW9uLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0JHNhdmVBY3Rpb24uYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24uc2V0RGVmYXVsdEFjdGlvbigkcm93QWN0aW9uc0Ryb3Bkb3duLCAkZWRpdEFjdGlvbi5sYXN0KCkpO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXHRcblx0LyoqXG5cdCAqIFBhZ2UgbW9kZSBjaGFuZ2UgYmV0d2VlbiBcImVkaXRcIiBhbmQgXCJmaWx0ZXJpbmdcIi5cblx0ICovXG5cdGZ1bmN0aW9uIF9vblBhZ2VNb2RlQ2hhbmdlKCkge1xuXHRcdGlmICgkKHRoaXMpLnZhbCgpID09ICdlZGl0LW1vZGUnKSB7XG5cdFx0XHQkdGhpcy5maW5kKCd0ci5zcGVjaWFsLXByaWNlLWZpbHRlcicpLmF0dHIoJ2hpZGRlbicsIHRydWUpO1xuXHRcdFx0JHRoaXMuZmluZCgndHIuc3BlY2lhbC1wcmljZS1lZGl0JykuYXR0cignaGlkZGVuJywgZmFsc2UpO1xuXHRcdFx0JHRoaXMuZmluZCgndGhlYWQgdHI6Zmlyc3QtY2hpbGQgdGgnKS5hZGRDbGFzcygnZWRpdC1tb2RlJyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdCR0aGlzLmZpbmQoJ3RyLnNwZWNpYWwtcHJpY2UtZmlsdGVyJykuYXR0cignaGlkZGVuJywgZmFsc2UpO1xuXHRcdFx0JHRoaXMuZmluZCgndHIuc3BlY2lhbC1wcmljZS1lZGl0JykuYXR0cignaGlkZGVuJywgdHJ1ZSk7XG5cdFx0XHQkdGhpcy5maW5kKCd0aGVhZCB0cjpmaXJzdC1jaGlsZCB0aCcpLnJlbW92ZUNsYXNzKCdlZGl0LW1vZGUnKTtcblx0XHR9XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBSZXN0b3JlcyBhbGwgdGhlIHJvdyBkYXRhIGNoYW5nZXMgYmFjayB0byB0aGVpciBvcmlnaW5hbCBzdGF0ZS5cblx0ICpcblx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9ICRyb3cgQ3VycmVudCByb3cgc2VsZWN0b3IuXG5cdCAqL1xuXHRmdW5jdGlvbiBfcmVzb2x2ZVJvd0NoYW5nZXMoJHJvdykge1xuXHRcdGNvbnN0IHJvd0luZGV4ID0gJHRoaXMuRGF0YVRhYmxlKCkucm93KCRyb3cpLmluZGV4KCk7XG5cdFx0XG5cdFx0JHJvdy5maW5kKCdpbnB1dDp0ZXh0JykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdGNvbnN0ICRjZWxsID0gJCh0aGlzKS5jbG9zZXN0KCd0ZCcpO1xuXHRcdFx0Y29uc3QgY29sdW1uSW5kZXggPSAkdGhpcy5EYXRhVGFibGUoKS5jb2x1bW4oJGNlbGwpLmluZGV4KCk7XG5cdFx0XHRcblx0XHRcdHRoaXMucGFyZW50RWxlbWVudC5pbm5lckhUTUwgPSAkdGhpcy5EYXRhVGFibGUoKS5jZWxsKHJvd0luZGV4LCBjb2x1bW5JbmRleCkuZGF0YSgpO1xuXHRcdH0pO1xuXHR9XG5cdFxuXHQvKipcblx0ICogVGFibGUgcm93IGRhdGEgY2hhbmdlIGV2ZW50IGhhbmRsZXIuXG5cdCAqXG5cdCAqIEl0J3MgYmVpbmcgdHJpZ2dlcmVkIGV2ZXJ5IHRpbWUgYSByb3cgaW5wdXQvc2VsZWN0IGZpZWxkIGlzIGNoYW5nZWQuXG5cdCAqL1xuXHRmdW5jdGlvbiBfb25UYWJsZVJvd0RhdGFDaGFuZ2UoKSB7XG5cdFx0Y29uc3QgJHJvdyA9ICQodGhpcykucGFyZW50cygndHInKTtcblx0XHRjb25zdCByb3dJbmRleCA9ICR0aGlzLkRhdGFUYWJsZSgpLnJvdygkcm93KS5pbmRleCgpO1xuXHRcdFxuXHRcdCRyb3cuZmluZCgnaW5wdXQ6dGV4dCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRjb25zdCAkY2VsbCA9ICQodGhpcykuY2xvc2VzdCgndGQnKTtcblx0XHRcdGNvbnN0IGNvbHVtbkluZGV4ID0gJHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKCRjZWxsKS5pbmRleCgpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJC50cmltKCQodGhpcykudmFsKCkpICE9ICR0aGlzLkRhdGFUYWJsZSgpLmNlbGwocm93SW5kZXgsIGNvbHVtbkluZGV4KS5kYXRhKCkpIHtcblx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnbW9kaWZpZWQnKTtcblx0XHRcdFx0XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JCh0aGlzKS5yZW1vdmVDbGFzcygnbW9kaWZpZWQnKTtcblx0XHR9KTtcblx0fVxuXHRcblx0LyoqXG5cdCAqIFRhYmxlIHJvdyBzd2l0Y2hlciBjaGFuZ2UgZXZlbnQgaGFuZGxlci5cblx0ICpcblx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gc3dpdGNoZXIgQ3VycmVudCBzd2l0Y2hlciBlbGVtZW50LlxuXHQgKi9cblx0ZnVuY3Rpb24gX29uVGFibGVSb3dTd2l0Y2hlckNoYW5nZShzd2l0Y2hlcikge1xuXHRcdGNvbnN0IHVybCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89UXVpY2tFZGl0U3BlY2lhbFByaWNlc0FqYXgvVXBkYXRlJztcblx0XHRjb25zdCBzcGVjaWFsUHJpY2UgPSAkKHN3aXRjaGVyKS5wYXJlbnRzKCd0cicpLmF0dHIoJ2lkJyk7XG5cdFx0Y29uc3QgJGNlbGwgPSAkKHN3aXRjaGVyKS5jbG9zZXN0KCd0ZCcpO1xuXHRcdGNvbnN0IGNvbHVtbkluZGV4ID0gJHRoaXMuRGF0YVRhYmxlKCkuY29sdW1uKCRjZWxsKS5pbmRleCgpO1xuXHRcdGNvbnN0IGNvbHVtbk5hbWUgPSAkdGhpcy5maW5kKCd0ci5zcGVjaWFsLXByaWNlLWZpbHRlciB0aCcpLmVxKGNvbHVtbkluZGV4KS5kYXRhKCdjb2x1bW5OYW1lJyk7XG5cdFx0Y29uc3QgZGF0YSA9IHt9O1xuXHRcdGNvbnN0IHZhbHVlID0ge307XG5cdFx0XG5cdFx0dmFsdWVbY29sdW1uTmFtZV0gPSAkKHN3aXRjaGVyKS5wcm9wKCdjaGVja2VkJykgPyAxIDogMDtcblx0XHRkYXRhW3NwZWNpYWxQcmljZV0gPSB2YWx1ZTtcblx0XHRcblx0XHQkLnBvc3QodXJsLCB7XG5cdFx0XHRkYXRhLFxuXHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKSxcblx0XHR9KS5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdHJlc3BvbnNlID0gJC5wYXJzZUpTT04ocmVzcG9uc2UpO1xuXHRcdFx0XG5cdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGNvbnN0IHRpdGxlID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ01PREFMX1RJVExFX05PREUnLCAnYWRtaW5fcXVpY2tfZWRpdCcpO1xuXHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdFRElUX0VSUk9SJywgJ2FkbWluX3F1aWNrX2VkaXQnKTtcblx0XHRcdGNvbnN0IGJ1dHRvbnMgPSBbXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9DTE9TRScsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGV2ZW50ID0+ICQoZXZlbnQuY3VycmVudFRhcmdldCkucGFyZW50cygnLm1vZGFsJykubW9kYWwoJ2hpZGUnKVxuXHRcdFx0XHR9XG5cdFx0XHRdO1xuXHRcdFx0XG5cdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZSh0aXRsZSwgbWVzc2FnZSwgYnV0dG9ucyk7XG5cdFx0fSk7XG5cdH1cblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBJTklUSUFMSVpBVElPTlxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdCR0aGlzXG5cdFx0XHQub24oJ2NsaWNrJywgJ3Rib2R5IHRyJywgX29uVGFibGVSb3dDbGljaylcblx0XHRcdC5vbignY2xpY2snLCAnLnJvdy1zcGVjaWFsLXByaWNlLWVkaXQnLCBfb25UYWJsZVJvd0VkaXRDbGljaylcblx0XHRcdC5vbignY2hhbmdlJywgJy5zcGVjaWFsLXByaWNlLWJ1bGstc2VsZWN0aW9uJywgX29uQnVsa1NlbGVjdGlvbkNoYW5nZSlcblx0XHRcdC5vbigna2V5dXAnLCAndGJvZHkgdHIgaW5wdXQ6dGV4dCcsIF9vblRhYmxlUm93RGF0YUNoYW5nZSk7XG5cdFx0XG5cdFx0JHRoaXMucGFyZW50cygnLnNwZWNpYWwtcHJpY2VzLm1vZGFsJylcblx0XHRcdC5vbignY2hhbmdlJywgJ2lucHV0OmNoZWNrYm94LnNwZWNpYWwtcHJpY2Utcm93LXNlbGVjdGlvbicsIF9vblRhYmxlUm93Q2hlY2tib3hDaGFuZ2UpXG5cdFx0XHQub24oJ2NoYW5nZScsICcuc2VsZWN0LXNwZWNpYWwtcHJpY2UtcGFnZS1tb2RlJywgX29uUGFnZU1vZGVDaGFuZ2UpXG5cdFx0XHQub24oJ2NsaWNrJywgJy5idG4tZ3JvdXAgLnNwZWNpYWwtcHJpY2UtYnVsay1yb3ctZWRpdCcsIF9vblRhYmxlQnVsa1Jvd0VkaXRDbGljayk7XG5cdFx0XG5cdFx0JHRoaXMub24oJ2RyYXcuZHQnLCAoKSA9PiB7XG5cdFx0XHQkdGhpcy5maW5kKCcuY29udmVydC10by1zd2l0Y2hlcicpLmVhY2goKGluZGV4LCBzd2l0Y2hlcikgPT4ge1xuXHRcdFx0XHRsZXQgc2tpcEluaXRpYWxFdmVudCA9IHRydWU7XG5cdFx0XHRcdCQoc3dpdGNoZXIpLm9uKCdjaGFuZ2UnLCAoKSA9PiB7XG5cdFx0XHRcdFx0aWYgKHNraXBJbml0aWFsRXZlbnQpIHtcblx0XHRcdFx0XHRcdHNraXBJbml0aWFsRXZlbnQgPSBmYWxzZTtcblx0XHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0X29uVGFibGVSb3dTd2l0Y2hlckNoYW5nZShzd2l0Y2hlcik7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdF9vblRhYmxlUm93Q2hlY2tib3hDaGFuZ2UoKTtcblx0XHR9KTtcblx0XHRcblx0XHRkb25lKCk7XG5cdH07XG5cdFxuXHRyZXR1cm4gbW9kdWxlO1xufSk7XG5cbiJdfQ==
