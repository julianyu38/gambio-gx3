'use strict';

/* --------------------------------------------------------------
 tooltip.js 2017-05-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * QuickEdit Table Tooltip
 *
 * This controller displays tooltips for the QuickEdit overview table. The tooltips are loaded after the
 * table data request is ready for optimization purposes.
 */
gx.controllers.module('tooltips', [jse.source + '/vendor/qtip2/jquery.qtip.css', jse.source + '/vendor/qtip2/jquery.qtip.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @var {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		sourceUrl: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/Tooltips',
		selectors: {
			mouseenter: {
				special_price: '.tooltip-products-special-price'
			}
		}
	};

	/**
  * Final Options
  *
  * @var {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Tooltip Contents
  *
  * Contains the rendered HTML of the tooltips. The HTML is rendered with each table draw.
  *
  * e.g. tooltips.400210.orderItems >> HTML for order items tooltip of order #400210.
  *
  * @type {Object}
  */
	var tooltips = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Target Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTargetPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'left' : 'right';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'top' : 'bottom';

		return horizontal + ' ' + vertical;
	}

	/**
  * Get Tooltip Position
  *
  * @param {jQuery} $target
  *
  * @return {String}
  */
	function _getTooltipPosition($target) {
		var horizontal = $target.offset().left - $(window).scrollLeft() > $(window).width() / 2 ? 'right' : 'left';
		var vertical = $target.offset().top - $(window).scrollTop() > $(window).height() / 2 ? 'bottom' : 'top';

		return horizontal + ' ' + vertical;
	}

	/**
  * Initialize tooltip for static table data.
  *
  * Replaces the browsers default tooltip with a qTip instance for every element on the table which has
  * a title attribute.
  */
	function _initTooltipsForStaticContent() {
		$this.find('[title]').qtip({
			style: { classes: 'gx-qtip info' }
		});
	}

	/**
  * Show Tooltip
  *
  * Display the Qtip instance of the target. The tooltip contents are fetched after the table request
  * is finished for performance reasons. This method will not show anything until the tooltip contents
  * are fetched.
  *
  * @param {jQuery.Event} event
  */
	function _showTooltip(event) {
		event.stopPropagation();

		var productId = $(this).parents('tr').data('id');

		if (!tooltips[productId]) {
			return; // The requested tooltip is not loaded, do not continue.
		}

		var tooltipPosition = _getTooltipPosition($(this));
		var targetPosition = _getTargetPosition($(this));

		$(this).qtip({
			content: tooltips[productId][event.data.name],
			style: {
				classes: 'gx-qtip info'
			},
			position: {
				my: tooltipPosition,
				at: targetPosition,
				effect: false,
				viewport: $(window),
				adjust: {
					method: 'none shift'
				}
			},
			hide: {
				fixed: true,
				delay: 100
			},
			show: {
				ready: true,
				delay: 300
			},
			events: {
				hidden: function hidden(event, api) {
					api.destroy(true);
				}
			}
		});
	}

	/**
  * Get Tooltips
  *
  * Fetches the tooltips with an AJAX request, based on the current state of the table.
  */
	function _getTooltips() {
		tooltips = [];
		var datatablesXhrParameters = $this.DataTable().ajax.params();
		$.post(options.sourceUrl, datatablesXhrParameters, function (response) {
			return tooltips = response;
		}, 'json');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('draw.dt', _initTooltipsForStaticContent).on('xhr.dt', _getTooltips);

		$(window).on('JSENGINE_INIT_FINISHED', function () {
			if ($this.DataTable().ajax.json() !== undefined && tooltips === undefined) {
				_getTooltips();
			}
		});

		for (var event in options.selectors) {
			for (var name in options.selectors[event]) {
				$this.on(event, options.selectors[event][name], { name: name }, _showTooltip);
			}
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvb3ZlcnZpZXcvdG9vbHRpcHMuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsInNvdXJjZVVybCIsImNvcmUiLCJjb25maWciLCJnZXQiLCJzZWxlY3RvcnMiLCJtb3VzZWVudGVyIiwic3BlY2lhbF9wcmljZSIsIm9wdGlvbnMiLCJleHRlbmQiLCJ0b29sdGlwcyIsIl9nZXRUYXJnZXRQb3NpdGlvbiIsIiR0YXJnZXQiLCJob3Jpem9udGFsIiwib2Zmc2V0IiwibGVmdCIsIndpbmRvdyIsInNjcm9sbExlZnQiLCJ3aWR0aCIsInZlcnRpY2FsIiwidG9wIiwic2Nyb2xsVG9wIiwiaGVpZ2h0IiwiX2dldFRvb2x0aXBQb3NpdGlvbiIsIl9pbml0VG9vbHRpcHNGb3JTdGF0aWNDb250ZW50IiwiZmluZCIsInF0aXAiLCJzdHlsZSIsImNsYXNzZXMiLCJfc2hvd1Rvb2x0aXAiLCJldmVudCIsInN0b3BQcm9wYWdhdGlvbiIsInByb2R1Y3RJZCIsInBhcmVudHMiLCJ0b29sdGlwUG9zaXRpb24iLCJ0YXJnZXRQb3NpdGlvbiIsImNvbnRlbnQiLCJuYW1lIiwicG9zaXRpb24iLCJteSIsImF0IiwiZWZmZWN0Iiwidmlld3BvcnQiLCJhZGp1c3QiLCJtZXRob2QiLCJoaWRlIiwiZml4ZWQiLCJkZWxheSIsInNob3ciLCJyZWFkeSIsImV2ZW50cyIsImhpZGRlbiIsImFwaSIsImRlc3Ryb3kiLCJfZ2V0VG9vbHRpcHMiLCJkYXRhdGFibGVzWGhyUGFyYW1ldGVycyIsIkRhdGFUYWJsZSIsImFqYXgiLCJwYXJhbXMiLCJwb3N0IiwicmVzcG9uc2UiLCJpbml0IiwiZG9uZSIsIm9uIiwianNvbiIsInVuZGVmaW5lZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7QUFNQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsVUFERCxFQUdDLENBQ0lDLElBQUlDLE1BRFIsb0NBRUlELElBQUlDLE1BRlIsa0NBSEQsRUFRQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCQyxhQUFXTixJQUFJTyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLG9EQUQzQjtBQUVoQkMsYUFBVztBQUNWQyxlQUFZO0FBQ1hDLG1CQUFlO0FBREo7QUFERjtBQUZLLEVBQWpCOztBQVNBOzs7OztBQUtBLEtBQU1DLFVBQVVULEVBQUVVLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQlQsUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVMsRUFBZjs7QUFFQTs7Ozs7Ozs7O0FBU0EsS0FBSWdCLGlCQUFKOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVNDLGtCQUFULENBQTRCQyxPQUE1QixFQUFxQztBQUNwQyxNQUFNQyxhQUFhRCxRQUFRRSxNQUFSLEdBQWlCQyxJQUFqQixHQUF3QmhCLEVBQUVpQixNQUFGLEVBQVVDLFVBQVYsRUFBeEIsR0FBaURsQixFQUFFaUIsTUFBRixFQUFVRSxLQUFWLEtBQW9CLENBQXJFLEdBQ2hCLE1BRGdCLEdBRWhCLE9BRkg7QUFHQSxNQUFNQyxXQUFXUCxRQUFRRSxNQUFSLEdBQWlCTSxHQUFqQixHQUF1QnJCLEVBQUVpQixNQUFGLEVBQVVLLFNBQVYsRUFBdkIsR0FBK0N0QixFQUFFaUIsTUFBRixFQUFVTSxNQUFWLEtBQXFCLENBQXBFLEdBQ2QsS0FEYyxHQUVkLFFBRkg7O0FBSUEsU0FBT1QsYUFBYSxHQUFiLEdBQW1CTSxRQUExQjtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0ksbUJBQVQsQ0FBNkJYLE9BQTdCLEVBQXNDO0FBQ3JDLE1BQU1DLGFBQWFELFFBQVFFLE1BQVIsR0FBaUJDLElBQWpCLEdBQXdCaEIsRUFBRWlCLE1BQUYsRUFBVUMsVUFBVixFQUF4QixHQUFpRGxCLEVBQUVpQixNQUFGLEVBQVVFLEtBQVYsS0FBb0IsQ0FBckUsR0FDaEIsT0FEZ0IsR0FFaEIsTUFGSDtBQUdBLE1BQU1DLFdBQVdQLFFBQVFFLE1BQVIsR0FBaUJNLEdBQWpCLEdBQXVCckIsRUFBRWlCLE1BQUYsRUFBVUssU0FBVixFQUF2QixHQUErQ3RCLEVBQUVpQixNQUFGLEVBQVVNLE1BQVYsS0FBcUIsQ0FBcEUsR0FDZCxRQURjLEdBRWQsS0FGSDs7QUFJQSxTQUFPVCxhQUFhLEdBQWIsR0FBbUJNLFFBQTFCO0FBQ0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNLLDZCQUFULEdBQXlDO0FBQ3hDMUIsUUFBTTJCLElBQU4sQ0FBVyxTQUFYLEVBQXNCQyxJQUF0QixDQUEyQjtBQUMxQkMsVUFBTyxFQUFDQyxTQUFTLGNBQVY7QUFEbUIsR0FBM0I7QUFHQTs7QUFFRDs7Ozs7Ozs7O0FBU0EsVUFBU0MsWUFBVCxDQUFzQkMsS0FBdEIsRUFBNkI7QUFDNUJBLFFBQU1DLGVBQU47O0FBRUEsTUFBTUMsWUFBWWpDLEVBQUUsSUFBRixFQUFRa0MsT0FBUixDQUFnQixJQUFoQixFQUFzQnBDLElBQXRCLENBQTJCLElBQTNCLENBQWxCOztBQUVBLE1BQUksQ0FBQ2EsU0FBU3NCLFNBQVQsQ0FBTCxFQUEwQjtBQUN6QixVQUR5QixDQUNqQjtBQUNSOztBQUVELE1BQU1FLGtCQUFrQlgsb0JBQW9CeEIsRUFBRSxJQUFGLENBQXBCLENBQXhCO0FBQ0EsTUFBTW9DLGlCQUFpQnhCLG1CQUFtQlosRUFBRSxJQUFGLENBQW5CLENBQXZCOztBQUVBQSxJQUFFLElBQUYsRUFBUTJCLElBQVIsQ0FBYTtBQUNaVSxZQUFTMUIsU0FBU3NCLFNBQVQsRUFBb0JGLE1BQU1qQyxJQUFOLENBQVd3QyxJQUEvQixDQURHO0FBRVpWLFVBQU87QUFDTkMsYUFBUztBQURILElBRks7QUFLWlUsYUFBVTtBQUNUQyxRQUFJTCxlQURLO0FBRVRNLFFBQUlMLGNBRks7QUFHVE0sWUFBUSxLQUhDO0FBSVRDLGNBQVUzQyxFQUFFaUIsTUFBRixDQUpEO0FBS1QyQixZQUFRO0FBQ1BDLGFBQVE7QUFERDtBQUxDLElBTEU7QUFjWkMsU0FBTTtBQUNMQyxXQUFPLElBREY7QUFFTEMsV0FBTztBQUZGLElBZE07QUFrQlpDLFNBQU07QUFDTEMsV0FBTyxJQURGO0FBRUxGLFdBQU87QUFGRixJQWxCTTtBQXNCWkcsV0FBUTtBQUNQQyxZQUFRLGdCQUFDckIsS0FBRCxFQUFRc0IsR0FBUixFQUFnQjtBQUN2QkEsU0FBSUMsT0FBSixDQUFZLElBQVo7QUFDQTtBQUhNO0FBdEJJLEdBQWI7QUE0QkE7O0FBRUQ7Ozs7O0FBS0EsVUFBU0MsWUFBVCxHQUF3QjtBQUN2QjVDLGFBQVcsRUFBWDtBQUNBLE1BQU02QywwQkFBMEJ6RCxNQUFNMEQsU0FBTixHQUFrQkMsSUFBbEIsQ0FBdUJDLE1BQXZCLEVBQWhDO0FBQ0EzRCxJQUFFNEQsSUFBRixDQUFPbkQsUUFBUVAsU0FBZixFQUEwQnNELHVCQUExQixFQUFtRDtBQUFBLFVBQVk3QyxXQUFXa0QsUUFBdkI7QUFBQSxHQUFuRCxFQUFvRixNQUFwRjtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQWxFLFFBQU9tRSxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCaEUsUUFDRWlFLEVBREYsQ0FDSyxTQURMLEVBQ2dCdkMsNkJBRGhCLEVBRUV1QyxFQUZGLENBRUssUUFGTCxFQUVlVCxZQUZmOztBQUlBdkQsSUFBRWlCLE1BQUYsRUFBVStDLEVBQVYsQ0FBYSx3QkFBYixFQUF1QyxZQUFNO0FBQzVDLE9BQUlqRSxNQUFNMEQsU0FBTixHQUFrQkMsSUFBbEIsQ0FBdUJPLElBQXZCLE9BQWtDQyxTQUFsQyxJQUErQ3ZELGFBQWF1RCxTQUFoRSxFQUEyRTtBQUMxRVg7QUFDQTtBQUNELEdBSkQ7O0FBTUEsT0FBSyxJQUFJeEIsS0FBVCxJQUFrQnRCLFFBQVFILFNBQTFCLEVBQXFDO0FBQ3BDLFFBQUssSUFBSWdDLElBQVQsSUFBaUI3QixRQUFRSCxTQUFSLENBQWtCeUIsS0FBbEIsQ0FBakIsRUFBMkM7QUFDMUNoQyxVQUFNaUUsRUFBTixDQUFTakMsS0FBVCxFQUFnQnRCLFFBQVFILFNBQVIsQ0FBa0J5QixLQUFsQixFQUF5Qk8sSUFBekIsQ0FBaEIsRUFBZ0QsRUFBQ0EsVUFBRCxFQUFoRCxFQUF3RFIsWUFBeEQ7QUFDQTtBQUNEOztBQUVEaUM7QUFDQSxFQWxCRDs7QUFvQkEsUUFBT3BFLE1BQVA7QUFDQSxDQXpNRiIsImZpbGUiOiJxdWlja19lZGl0L292ZXJ2aWV3L3Rvb2x0aXBzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiB0b29sdGlwLmpzIDIwMTctMDUtMThcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIFF1aWNrRWRpdCBUYWJsZSBUb29sdGlwXG4gKlxuICogVGhpcyBjb250cm9sbGVyIGRpc3BsYXlzIHRvb2x0aXBzIGZvciB0aGUgUXVpY2tFZGl0IG92ZXJ2aWV3IHRhYmxlLiBUaGUgdG9vbHRpcHMgYXJlIGxvYWRlZCBhZnRlciB0aGVcbiAqIHRhYmxlIGRhdGEgcmVxdWVzdCBpcyByZWFkeSBmb3Igb3B0aW1pemF0aW9uIHB1cnBvc2VzLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCd0b29sdGlwcycsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL3F0aXAyL2pxdWVyeS5xdGlwLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL3F0aXAyL2pxdWVyeS5xdGlwLmpzYFxuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB2YXIge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGRlZmF1bHRzID0ge1xuXHRcdFx0c291cmNlVXJsOiBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVF1aWNrRWRpdE92ZXJ2aWV3QWpheC9Ub29sdGlwcycsXG5cdFx0XHRzZWxlY3RvcnM6IHtcblx0XHRcdFx0bW91c2VlbnRlcjoge1xuXHRcdFx0XHRcdHNwZWNpYWxfcHJpY2U6ICcudG9vbHRpcC1wcm9kdWN0cy1zcGVjaWFsLXByaWNlJyxcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHZhciB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVG9vbHRpcCBDb250ZW50c1xuXHRcdCAqXG5cdFx0ICogQ29udGFpbnMgdGhlIHJlbmRlcmVkIEhUTUwgb2YgdGhlIHRvb2x0aXBzLiBUaGUgSFRNTCBpcyByZW5kZXJlZCB3aXRoIGVhY2ggdGFibGUgZHJhdy5cblx0XHQgKlxuXHRcdCAqIGUuZy4gdG9vbHRpcHMuNDAwMjEwLm9yZGVySXRlbXMgPj4gSFRNTCBmb3Igb3JkZXIgaXRlbXMgdG9vbHRpcCBvZiBvcmRlciAjNDAwMjEwLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRsZXQgdG9vbHRpcHM7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IFRhcmdldCBQb3NpdGlvblxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICR0YXJnZXRcblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge1N0cmluZ31cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0VGFyZ2V0UG9zaXRpb24oJHRhcmdldCkge1xuXHRcdFx0Y29uc3QgaG9yaXpvbnRhbCA9ICR0YXJnZXQub2Zmc2V0KCkubGVmdCAtICQod2luZG93KS5zY3JvbGxMZWZ0KCkgPiAkKHdpbmRvdykud2lkdGgoKSAvIDJcblx0XHRcdFx0PyAnbGVmdCdcblx0XHRcdFx0OiAncmlnaHQnO1xuXHRcdFx0Y29uc3QgdmVydGljYWwgPSAkdGFyZ2V0Lm9mZnNldCgpLnRvcCAtICQod2luZG93KS5zY3JvbGxUb3AoKSA+ICQod2luZG93KS5oZWlnaHQoKSAvIDJcblx0XHRcdFx0PyAndG9wJ1xuXHRcdFx0XHQ6ICdib3R0b20nO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gaG9yaXpvbnRhbCArICcgJyArIHZlcnRpY2FsO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgVG9vbHRpcCBQb3NpdGlvblxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICR0YXJnZXRcblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge1N0cmluZ31cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0VG9vbHRpcFBvc2l0aW9uKCR0YXJnZXQpIHtcblx0XHRcdGNvbnN0IGhvcml6b250YWwgPSAkdGFyZ2V0Lm9mZnNldCgpLmxlZnQgLSAkKHdpbmRvdykuc2Nyb2xsTGVmdCgpID4gJCh3aW5kb3cpLndpZHRoKCkgLyAyXG5cdFx0XHRcdD8gJ3JpZ2h0J1xuXHRcdFx0XHQ6ICdsZWZ0Jztcblx0XHRcdGNvbnN0IHZlcnRpY2FsID0gJHRhcmdldC5vZmZzZXQoKS50b3AgLSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgPiAkKHdpbmRvdykuaGVpZ2h0KCkgLyAyXG5cdFx0XHRcdD8gJ2JvdHRvbSdcblx0XHRcdFx0OiAndG9wJztcblx0XHRcdFxuXHRcdFx0cmV0dXJuIGhvcml6b250YWwgKyAnICcgKyB2ZXJ0aWNhbDtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSB0b29sdGlwIGZvciBzdGF0aWMgdGFibGUgZGF0YS5cblx0XHQgKlxuXHRcdCAqIFJlcGxhY2VzIHRoZSBicm93c2VycyBkZWZhdWx0IHRvb2x0aXAgd2l0aCBhIHFUaXAgaW5zdGFuY2UgZm9yIGV2ZXJ5IGVsZW1lbnQgb24gdGhlIHRhYmxlIHdoaWNoIGhhc1xuXHRcdCAqIGEgdGl0bGUgYXR0cmlidXRlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9pbml0VG9vbHRpcHNGb3JTdGF0aWNDb250ZW50KCkge1xuXHRcdFx0JHRoaXMuZmluZCgnW3RpdGxlXScpLnF0aXAoe1xuXHRcdFx0XHRzdHlsZToge2NsYXNzZXM6ICdneC1xdGlwIGluZm8nfVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNob3cgVG9vbHRpcFxuXHRcdCAqXG5cdFx0ICogRGlzcGxheSB0aGUgUXRpcCBpbnN0YW5jZSBvZiB0aGUgdGFyZ2V0LiBUaGUgdG9vbHRpcCBjb250ZW50cyBhcmUgZmV0Y2hlZCBhZnRlciB0aGUgdGFibGUgcmVxdWVzdFxuXHRcdCAqIGlzIGZpbmlzaGVkIGZvciBwZXJmb3JtYW5jZSByZWFzb25zLiBUaGlzIG1ldGhvZCB3aWxsIG5vdCBzaG93IGFueXRoaW5nIHVudGlsIHRoZSB0b29sdGlwIGNvbnRlbnRzXG5cdFx0ICogYXJlIGZldGNoZWQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2hvd1Rvb2x0aXAoZXZlbnQpIHtcblx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCBwcm9kdWN0SWQgPSAkKHRoaXMpLnBhcmVudHMoJ3RyJykuZGF0YSgnaWQnKTtcblx0XHRcdFxuXHRcdFx0aWYgKCF0b29sdGlwc1twcm9kdWN0SWRdKSB7XG5cdFx0XHRcdHJldHVybjsgLy8gVGhlIHJlcXVlc3RlZCB0b29sdGlwIGlzIG5vdCBsb2FkZWQsIGRvIG5vdCBjb250aW51ZS5cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Y29uc3QgdG9vbHRpcFBvc2l0aW9uID0gX2dldFRvb2x0aXBQb3NpdGlvbigkKHRoaXMpKTtcblx0XHRcdGNvbnN0IHRhcmdldFBvc2l0aW9uID0gX2dldFRhcmdldFBvc2l0aW9uKCQodGhpcykpO1xuXHRcdFx0XG5cdFx0XHQkKHRoaXMpLnF0aXAoe1xuXHRcdFx0XHRjb250ZW50OiB0b29sdGlwc1twcm9kdWN0SWRdW2V2ZW50LmRhdGEubmFtZV0sXG5cdFx0XHRcdHN0eWxlOiB7XG5cdFx0XHRcdFx0Y2xhc3NlczogJ2d4LXF0aXAgaW5mbydcblx0XHRcdFx0fSxcblx0XHRcdFx0cG9zaXRpb246IHtcblx0XHRcdFx0XHRteTogdG9vbHRpcFBvc2l0aW9uLFxuXHRcdFx0XHRcdGF0OiB0YXJnZXRQb3NpdGlvbixcblx0XHRcdFx0XHRlZmZlY3Q6IGZhbHNlLFxuXHRcdFx0XHRcdHZpZXdwb3J0OiAkKHdpbmRvdyksXG5cdFx0XHRcdFx0YWRqdXN0OiB7XG5cdFx0XHRcdFx0XHRtZXRob2Q6ICdub25lIHNoaWZ0J1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSxcblx0XHRcdFx0aGlkZToge1xuXHRcdFx0XHRcdGZpeGVkOiB0cnVlLFxuXHRcdFx0XHRcdGRlbGF5OiAxMDBcblx0XHRcdFx0fSxcblx0XHRcdFx0c2hvdzoge1xuXHRcdFx0XHRcdHJlYWR5OiB0cnVlLFxuXHRcdFx0XHRcdGRlbGF5OiAzMDBcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXZlbnRzOiB7XG5cdFx0XHRcdFx0aGlkZGVuOiAoZXZlbnQsIGFwaSkgPT4ge1xuXHRcdFx0XHRcdFx0YXBpLmRlc3Ryb3kodHJ1ZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IFRvb2x0aXBzXG5cdFx0ICpcblx0XHQgKiBGZXRjaGVzIHRoZSB0b29sdGlwcyB3aXRoIGFuIEFKQVggcmVxdWVzdCwgYmFzZWQgb24gdGhlIGN1cnJlbnQgc3RhdGUgb2YgdGhlIHRhYmxlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRUb29sdGlwcygpIHtcblx0XHRcdHRvb2x0aXBzID0gW107XG5cdFx0XHRjb25zdCBkYXRhdGFibGVzWGhyUGFyYW1ldGVycyA9ICR0aGlzLkRhdGFUYWJsZSgpLmFqYXgucGFyYW1zKCk7XG5cdFx0XHQkLnBvc3Qob3B0aW9ucy5zb3VyY2VVcmwsIGRhdGF0YWJsZXNYaHJQYXJhbWV0ZXJzLCByZXNwb25zZSA9PiB0b29sdGlwcyA9IHJlc3BvbnNlLCAnanNvbicpXG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQub24oJ2RyYXcuZHQnLCBfaW5pdFRvb2x0aXBzRm9yU3RhdGljQ29udGVudClcblx0XHRcdFx0Lm9uKCd4aHIuZHQnLCBfZ2V0VG9vbHRpcHMpO1xuXHRcdFx0XG5cdFx0XHQkKHdpbmRvdykub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCAoKSA9PiB7XG5cdFx0XHRcdGlmICgkdGhpcy5EYXRhVGFibGUoKS5hamF4Lmpzb24oKSAhPT0gdW5kZWZpbmVkICYmIHRvb2x0aXBzID09PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0XHRfZ2V0VG9vbHRpcHMoKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGZvciAobGV0IGV2ZW50IGluIG9wdGlvbnMuc2VsZWN0b3JzKSB7XG5cdFx0XHRcdGZvciAobGV0IG5hbWUgaW4gb3B0aW9ucy5zZWxlY3RvcnNbZXZlbnRdKSB7XG5cdFx0XHRcdFx0JHRoaXMub24oZXZlbnQsIG9wdGlvbnMuc2VsZWN0b3JzW2V2ZW50XVtuYW1lXSwge25hbWV9LCBfc2hvd1Rvb2x0aXApO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7Il19
