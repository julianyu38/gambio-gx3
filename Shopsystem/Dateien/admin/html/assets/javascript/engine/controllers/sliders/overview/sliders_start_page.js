'use strict';

/* --------------------------------------------------------------
 sliders_start_page.js 2016-12-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Sliders Overview Start Page Option
 *
 * Handles the start page switcher toggling.
 */
gx.controllers.module('sliders_start_page', ['modal', gx.source + '/libs/info_box'], function () {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * CSS class names.
  *
  * @type {Object}
  */
	var classes = {
		switcher: 'switcher'
	};

	/**
  * Selector Strings
  *
  * @type {Object}
  */
	var selectors = {
		switcher: '.' + classes.switcher,
		switcherCheckbox: '.' + classes.switcher + ' :checkbox'
	};

	/**
  * URI map.
  *
  * @type {Object}
  */
	var uris = {
		activate: jse.core.config.get('appUrl') + '/admin/admin.php?do=SlidersOverviewAjax/SetStartPageSlider',
		deactivate: jse.core.config.get('appUrl') + '/admin/admin.php?do=SlidersOverviewAjax/DeactivateStartPageSlider'
	};

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Handles the slider start page switcher change event.
  *
  * @param {jQuery.Event} event Trigger event.
  */
	function _onSwitcherChange(event) {
		// Clicked element.
		var $target = $(event.target);

		// Clicked switcher element.
		var $clickedSwitcher = $target.hasClass(classes.switcher) ? $target : $target.parents(selectors.switcher);

		// Clicked slider ID.
		var sliderId = $clickedSwitcher.parents('tr').data('sliderId');

		// Is slider set as start page slider?
		var isActive = !$clickedSwitcher.hasClass('checked');

		// Disable all switchers.
		_toggleSwitchers(false);

		// Activate or deactivate slider depending on the state.
		isActive ? _deactivate(sliderId, $clickedSwitcher) : _activate(sliderId, $clickedSwitcher);
	}

	/**
  * Deactivates the slider.
  *
  * @param {Number} sliderId Slider ID.
  * @param {jQuery} $clickedSwitcher Clicked slider element.
  */
	function _deactivate(sliderId, $clickedSwitcher) {
		// Request options.
		var requestOptions = {
			type: 'POST',
			data: { sliderId: sliderId },
			url: uris.deactivate
		};

		// Handles the 'always' case by enabling the clicked slider.
		var handleAlways = function handleAlways() {
			return $clickedSwitcher.removeClass('disabled');
		};

		// Handles the 'done' case with the server response.
		var handleDone = function handleDone(response) {
			// Enable all switchers.
			_toggleSwitchers(true);

			// Deactivate each slider.
			$this.find(selectors.switcherCheckbox).each(function (index, checkbox) {
				return $(checkbox).switcher('checked', false);
			});

			// Notify user.
			_notify(response.includes('success'));
		};

		// Perform request.
		$.ajax(requestOptions).done(handleDone).always(handleAlways);
	}

	/**
  * Activates the slider.
  *
  * @param {Number} sliderId Slider ID.
  * @param {jQuery} $clickedSwitcher Clicked slider element.
  */
	function _activate(sliderId, $clickedSwitcher) {
		// Request options.
		var requestOptions = {
			type: 'POST',
			data: { sliderId: sliderId },
			url: uris.activate
		};

		// Handles the 'always' case by enabling the clicked slider.
		var handleAlways = function handleAlways() {
			return _toggleSwitchers(true);
		};

		// Handles the 'done' case with the server response.
		var handleDone = function handleDone(response) {
			// Clicked switcher's checkbox.
			var $checkbox = $clickedSwitcher.find(':checkbox');

			// Enable all switchers.
			_toggleSwitchers(true);

			// Check switcher.
			$checkbox.switcher('checked', true);

			// Deactivate each slider, except the clicked one.
			$this.find(selectors.switcherCheckbox).not($checkbox).each(function (index, checkbox) {
				return $(checkbox).switcher('checked', false);
			});

			// Notify user.
			_notify(response.includes('success'));
		};

		// Perform request.
		$.ajax(requestOptions).done(handleDone).always(handleAlways);
	}

	/**
  * If the server response is successful, it removes any previous messages and
  * adds new success message to admin info box.
  *
  * Otherwise its shows an error message modal.
  *
  * @param {Boolean} isSuccessful Is the server response successful?
  */
	function _notify(isSuccessful) {
		if (isSuccessful) {
			jse.libs.info_box.deleteBySource('adminAction').then(function () {
				return jse.libs.info_box.addSuccessMessage();
			});
		} else {
			jse.libs.modal.showMessage(jse.core.lang.translate('SLIDER_START_PAGE_ERROR_TITLE', 'sliders'), jse.core.lang.translate('SLIDER_START_PAGE_ERROR_TEXT', 'sliders'));
		}
	}

	/**
  * Enables or disables the switchers.
  *
  * @param {Boolean} doEnable Enable the switchers?
  */
	function _toggleSwitchers(doEnable) {
		$this.find(selectors.switcher)[(doEnable ? 'remove' : 'add') + 'Class']('disabled');
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Listen to set start page event.
		$this.on('change', selectors.switcher, _onSwitcherChange);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNsaWRlcnMvb3ZlcnZpZXcvc2xpZGVyc19zdGFydF9wYWdlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJzb3VyY2UiLCIkdGhpcyIsIiQiLCJjbGFzc2VzIiwic3dpdGNoZXIiLCJzZWxlY3RvcnMiLCJzd2l0Y2hlckNoZWNrYm94IiwidXJpcyIsImFjdGl2YXRlIiwianNlIiwiY29yZSIsImNvbmZpZyIsImdldCIsImRlYWN0aXZhdGUiLCJfb25Td2l0Y2hlckNoYW5nZSIsImV2ZW50IiwiJHRhcmdldCIsInRhcmdldCIsIiRjbGlja2VkU3dpdGNoZXIiLCJoYXNDbGFzcyIsInBhcmVudHMiLCJzbGlkZXJJZCIsImRhdGEiLCJpc0FjdGl2ZSIsIl90b2dnbGVTd2l0Y2hlcnMiLCJfZGVhY3RpdmF0ZSIsIl9hY3RpdmF0ZSIsInJlcXVlc3RPcHRpb25zIiwidHlwZSIsInVybCIsImhhbmRsZUFsd2F5cyIsInJlbW92ZUNsYXNzIiwiaGFuZGxlRG9uZSIsImZpbmQiLCJlYWNoIiwiaW5kZXgiLCJjaGVja2JveCIsIl9ub3RpZnkiLCJyZXNwb25zZSIsImluY2x1ZGVzIiwiYWpheCIsImRvbmUiLCJhbHdheXMiLCIkY2hlY2tib3giLCJub3QiLCJpc1N1Y2Nlc3NmdWwiLCJsaWJzIiwiaW5mb19ib3giLCJkZWxldGVCeVNvdXJjZSIsInRoZW4iLCJhZGRTdWNjZXNzTWVzc2FnZSIsIm1vZGFsIiwic2hvd01lc3NhZ2UiLCJsYW5nIiwidHJhbnNsYXRlIiwiZG9FbmFibGUiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0Msb0JBREQsRUFHQyxDQUNDLE9BREQsRUFFSUYsR0FBR0csTUFGUCxvQkFIRCxFQVFDLFlBQVk7O0FBRVg7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFHQTs7Ozs7QUFLQSxLQUFNQyxVQUFVO0FBQ2ZDLFlBQVU7QUFESyxFQUFoQjs7QUFJQTs7Ozs7QUFLQSxLQUFNQyxZQUFZO0FBQ2pCRCxrQkFBY0QsUUFBUUMsUUFETDtBQUVqQkUsMEJBQXNCSCxRQUFRQyxRQUE5QjtBQUZpQixFQUFsQjs7QUFNQTs7Ozs7QUFLQSxLQUFNRyxPQUFPO0FBQ1pDLFlBQWFDLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsQ0FBYiwrREFEWTtBQUVaQyxjQUFlSixJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQWY7QUFGWSxFQUFiOztBQUtBOzs7OztBQUtBLEtBQU1iLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU2UsaUJBQVQsQ0FBMkJDLEtBQTNCLEVBQWtDO0FBQ2pDO0FBQ0EsTUFBTUMsVUFBVWQsRUFBRWEsTUFBTUUsTUFBUixDQUFoQjs7QUFFQTtBQUNBLE1BQU1DLG1CQUFtQkYsUUFBUUcsUUFBUixDQUFpQmhCLFFBQVFDLFFBQXpCLElBQXFDWSxPQUFyQyxHQUErQ0EsUUFBUUksT0FBUixDQUFnQmYsVUFBVUQsUUFBMUIsQ0FBeEU7O0FBRUE7QUFDQSxNQUFNaUIsV0FBV0gsaUJBQWlCRSxPQUFqQixDQUF5QixJQUF6QixFQUErQkUsSUFBL0IsQ0FBb0MsVUFBcEMsQ0FBakI7O0FBRUE7QUFDQSxNQUFNQyxXQUFXLENBQUNMLGlCQUFpQkMsUUFBakIsQ0FBMEIsU0FBMUIsQ0FBbEI7O0FBRUE7QUFDQUssbUJBQWlCLEtBQWpCOztBQUVBO0FBQ0FELGFBQVdFLFlBQVlKLFFBQVosRUFBc0JILGdCQUF0QixDQUFYLEdBQXFEUSxVQUFVTCxRQUFWLEVBQW9CSCxnQkFBcEIsQ0FBckQ7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU08sV0FBVCxDQUFxQkosUUFBckIsRUFBK0JILGdCQUEvQixFQUFpRDtBQUNoRDtBQUNBLE1BQU1TLGlCQUFpQjtBQUN0QkMsU0FBTSxNQURnQjtBQUV0Qk4sU0FBTSxFQUFDRCxrQkFBRCxFQUZnQjtBQUd0QlEsUUFBS3RCLEtBQUtNO0FBSFksR0FBdkI7O0FBTUE7QUFDQSxNQUFNaUIsZUFBZSxTQUFmQSxZQUFlO0FBQUEsVUFBTVosaUJBQWlCYSxXQUFqQixDQUE2QixVQUE3QixDQUFOO0FBQUEsR0FBckI7O0FBRUE7QUFDQSxNQUFNQyxhQUFhLFNBQWJBLFVBQWEsV0FBWTtBQUM5QjtBQUNBUixvQkFBaUIsSUFBakI7O0FBRUE7QUFDQXZCLFNBQ0VnQyxJQURGLENBQ081QixVQUFVQyxnQkFEakIsRUFFRTRCLElBRkYsQ0FFTyxVQUFDQyxLQUFELEVBQVFDLFFBQVI7QUFBQSxXQUFxQmxDLEVBQUVrQyxRQUFGLEVBQVloQyxRQUFaLENBQXFCLFNBQXJCLEVBQWdDLEtBQWhDLENBQXJCO0FBQUEsSUFGUDs7QUFJQTtBQUNBaUMsV0FBUUMsU0FBU0MsUUFBVCxDQUFrQixTQUFsQixDQUFSO0FBQ0EsR0FYRDs7QUFhQTtBQUNBckMsSUFBRXNDLElBQUYsQ0FBT2IsY0FBUCxFQUNFYyxJQURGLENBQ09ULFVBRFAsRUFFRVUsTUFGRixDQUVTWixZQUZUO0FBR0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNKLFNBQVQsQ0FBbUJMLFFBQW5CLEVBQTZCSCxnQkFBN0IsRUFBK0M7QUFDOUM7QUFDQSxNQUFNUyxpQkFBaUI7QUFDdEJDLFNBQU0sTUFEZ0I7QUFFdEJOLFNBQU0sRUFBQ0Qsa0JBQUQsRUFGZ0I7QUFHdEJRLFFBQUt0QixLQUFLQztBQUhZLEdBQXZCOztBQU1BO0FBQ0EsTUFBTXNCLGVBQWUsU0FBZkEsWUFBZTtBQUFBLFVBQU1OLGlCQUFpQixJQUFqQixDQUFOO0FBQUEsR0FBckI7O0FBRUE7QUFDQSxNQUFNUSxhQUFhLFNBQWJBLFVBQWEsV0FBWTtBQUM5QjtBQUNBLE9BQU1XLFlBQVl6QixpQkFBaUJlLElBQWpCLENBQXNCLFdBQXRCLENBQWxCOztBQUVBO0FBQ0FULG9CQUFpQixJQUFqQjs7QUFFQTtBQUNBbUIsYUFBVXZDLFFBQVYsQ0FBbUIsU0FBbkIsRUFBOEIsSUFBOUI7O0FBRUE7QUFDQUgsU0FDRWdDLElBREYsQ0FDTzVCLFVBQVVDLGdCQURqQixFQUVFc0MsR0FGRixDQUVNRCxTQUZOLEVBR0VULElBSEYsQ0FHTyxVQUFDQyxLQUFELEVBQVFDLFFBQVI7QUFBQSxXQUFxQmxDLEVBQUVrQyxRQUFGLEVBQVloQyxRQUFaLENBQXFCLFNBQXJCLEVBQWdDLEtBQWhDLENBQXJCO0FBQUEsSUFIUDs7QUFLQTtBQUNBaUMsV0FBUUMsU0FBU0MsUUFBVCxDQUFrQixTQUFsQixDQUFSO0FBQ0EsR0FsQkQ7O0FBb0JBO0FBQ0FyQyxJQUFFc0MsSUFBRixDQUFPYixjQUFQLEVBQ0VjLElBREYsQ0FDT1QsVUFEUCxFQUVFVSxNQUZGLENBRVNaLFlBRlQ7QUFHQTs7QUFFRDs7Ozs7Ozs7QUFRQSxVQUFTTyxPQUFULENBQWlCUSxZQUFqQixFQUErQjtBQUM5QixNQUFJQSxZQUFKLEVBQWtCO0FBQ2pCcEMsT0FBSXFDLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsY0FBbEIsQ0FBaUMsYUFBakMsRUFBZ0RDLElBQWhELENBQXFEO0FBQUEsV0FBTXhDLElBQUlxQyxJQUFKLENBQVNDLFFBQVQsQ0FBa0JHLGlCQUFsQixFQUFOO0FBQUEsSUFBckQ7QUFDQSxHQUZELE1BRU87QUFDTnpDLE9BQUlxQyxJQUFKLENBQVNLLEtBQVQsQ0FBZUMsV0FBZixDQUNDM0MsSUFBSUMsSUFBSixDQUFTMkMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLCtCQUF4QixFQUF5RCxTQUF6RCxDQURELEVBRUM3QyxJQUFJQyxJQUFKLENBQVMyQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsOEJBQXhCLEVBQXdELFNBQXhELENBRkQ7QUFJQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVM5QixnQkFBVCxDQUEwQitCLFFBQTFCLEVBQW9DO0FBQ25DdEQsUUFBTWdDLElBQU4sQ0FBVzVCLFVBQVVELFFBQXJCLEdBQWtDbUQsV0FBVyxRQUFYLEdBQXNCLEtBQXhELGFBQXNFLFVBQXRFO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBeEQsUUFBT3lELElBQVAsR0FBYyxnQkFBUTtBQUNyQjtBQUNBdkQsUUFBTXdELEVBQU4sQ0FBUyxRQUFULEVBQW1CcEQsVUFBVUQsUUFBN0IsRUFBdUNVLGlCQUF2Qzs7QUFFQTtBQUNBMkI7QUFDQSxFQU5EOztBQVFBLFFBQU8xQyxNQUFQO0FBQ0EsQ0FwTkYiLCJmaWxlIjoic2xpZGVycy9vdmVydmlldy9zbGlkZXJzX3N0YXJ0X3BhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNsaWRlcnNfc3RhcnRfcGFnZS5qcyAyMDE2LTEyLTEzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBTbGlkZXJzIE92ZXJ2aWV3IFN0YXJ0IFBhZ2UgT3B0aW9uXG4gKlxuICogSGFuZGxlcyB0aGUgc3RhcnQgcGFnZSBzd2l0Y2hlciB0b2dnbGluZy5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnc2xpZGVyc19zdGFydF9wYWdlJyxcblx0XG5cdFtcblx0XHQnbW9kYWwnLFxuXHRcdGAke2d4LnNvdXJjZX0vbGlicy9pbmZvX2JveGBcblx0XSxcblx0XG5cdGZ1bmN0aW9uICgpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ1NTIGNsYXNzIG5hbWVzLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBjbGFzc2VzID0ge1xuXHRcdFx0c3dpdGNoZXI6ICdzd2l0Y2hlcicsXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZWxlY3RvciBTdHJpbmdzXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IHNlbGVjdG9ycyA9IHtcblx0XHRcdHN3aXRjaGVyOiBgLiR7Y2xhc3Nlcy5zd2l0Y2hlcn1gLFxuXHRcdFx0c3dpdGNoZXJDaGVja2JveDogYC4ke2NsYXNzZXMuc3dpdGNoZXJ9IDpjaGVja2JveGAsXG5cdFx0fTtcblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBVUkkgbWFwLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCB1cmlzID0ge1xuXHRcdFx0YWN0aXZhdGU6IGAke2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpfS9hZG1pbi9hZG1pbi5waHA/ZG89U2xpZGVyc092ZXJ2aWV3QWpheC9TZXRTdGFydFBhZ2VTbGlkZXJgLFxuXHRcdFx0ZGVhY3RpdmF0ZTogYCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2FkbWluL2FkbWluLnBocD9kbz1TbGlkZXJzT3ZlcnZpZXdBamF4L0RlYWN0aXZhdGVTdGFydFBhZ2VTbGlkZXJgLFxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBzbGlkZXIgc3RhcnQgcGFnZSBzd2l0Y2hlciBjaGFuZ2UgZXZlbnQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlciBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25Td2l0Y2hlckNoYW5nZShldmVudCkge1xuXHRcdFx0Ly8gQ2xpY2tlZCBlbGVtZW50LlxuXHRcdFx0Y29uc3QgJHRhcmdldCA9ICQoZXZlbnQudGFyZ2V0KTtcblx0XHRcdFxuXHRcdFx0Ly8gQ2xpY2tlZCBzd2l0Y2hlciBlbGVtZW50LlxuXHRcdFx0Y29uc3QgJGNsaWNrZWRTd2l0Y2hlciA9ICR0YXJnZXQuaGFzQ2xhc3MoY2xhc3Nlcy5zd2l0Y2hlcikgPyAkdGFyZ2V0IDogJHRhcmdldC5wYXJlbnRzKHNlbGVjdG9ycy5zd2l0Y2hlcik7XG5cdFx0XHRcblx0XHRcdC8vIENsaWNrZWQgc2xpZGVyIElELlxuXHRcdFx0Y29uc3Qgc2xpZGVySWQgPSAkY2xpY2tlZFN3aXRjaGVyLnBhcmVudHMoJ3RyJykuZGF0YSgnc2xpZGVySWQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gSXMgc2xpZGVyIHNldCBhcyBzdGFydCBwYWdlIHNsaWRlcj9cblx0XHRcdGNvbnN0IGlzQWN0aXZlID0gISRjbGlja2VkU3dpdGNoZXIuaGFzQ2xhc3MoJ2NoZWNrZWQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gRGlzYWJsZSBhbGwgc3dpdGNoZXJzLlxuXHRcdFx0X3RvZ2dsZVN3aXRjaGVycyhmYWxzZSk7XG5cdFx0XHRcblx0XHRcdC8vIEFjdGl2YXRlIG9yIGRlYWN0aXZhdGUgc2xpZGVyIGRlcGVuZGluZyBvbiB0aGUgc3RhdGUuXG5cdFx0XHRpc0FjdGl2ZSA/IF9kZWFjdGl2YXRlKHNsaWRlcklkLCAkY2xpY2tlZFN3aXRjaGVyKSA6IF9hY3RpdmF0ZShzbGlkZXJJZCwgJGNsaWNrZWRTd2l0Y2hlcik7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlYWN0aXZhdGVzIHRoZSBzbGlkZXIuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge051bWJlcn0gc2xpZGVySWQgU2xpZGVyIElELlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkY2xpY2tlZFN3aXRjaGVyIENsaWNrZWQgc2xpZGVyIGVsZW1lbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2RlYWN0aXZhdGUoc2xpZGVySWQsICRjbGlja2VkU3dpdGNoZXIpIHtcblx0XHRcdC8vIFJlcXVlc3Qgb3B0aW9ucy5cblx0XHRcdGNvbnN0IHJlcXVlc3RPcHRpb25zID0ge1xuXHRcdFx0XHR0eXBlOiAnUE9TVCcsXG5cdFx0XHRcdGRhdGE6IHtzbGlkZXJJZH0sXG5cdFx0XHRcdHVybDogdXJpcy5kZWFjdGl2YXRlLFxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gSGFuZGxlcyB0aGUgJ2Fsd2F5cycgY2FzZSBieSBlbmFibGluZyB0aGUgY2xpY2tlZCBzbGlkZXIuXG5cdFx0XHRjb25zdCBoYW5kbGVBbHdheXMgPSAoKSA9PiAkY2xpY2tlZFN3aXRjaGVyLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBIYW5kbGVzIHRoZSAnZG9uZScgY2FzZSB3aXRoIHRoZSBzZXJ2ZXIgcmVzcG9uc2UuXG5cdFx0XHRjb25zdCBoYW5kbGVEb25lID0gcmVzcG9uc2UgPT4ge1xuXHRcdFx0XHQvLyBFbmFibGUgYWxsIHN3aXRjaGVycy5cblx0XHRcdFx0X3RvZ2dsZVN3aXRjaGVycyh0cnVlKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIERlYWN0aXZhdGUgZWFjaCBzbGlkZXIuXG5cdFx0XHRcdCR0aGlzXG5cdFx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLnN3aXRjaGVyQ2hlY2tib3gpXG5cdFx0XHRcdFx0LmVhY2goKGluZGV4LCBjaGVja2JveCkgPT4gJChjaGVja2JveCkuc3dpdGNoZXIoJ2NoZWNrZWQnLCBmYWxzZSkpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gTm90aWZ5IHVzZXIuXG5cdFx0XHRcdF9ub3RpZnkocmVzcG9uc2UuaW5jbHVkZXMoJ3N1Y2Nlc3MnKSk7XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBQZXJmb3JtIHJlcXVlc3QuXG5cdFx0XHQkLmFqYXgocmVxdWVzdE9wdGlvbnMpXG5cdFx0XHRcdC5kb25lKGhhbmRsZURvbmUpXG5cdFx0XHRcdC5hbHdheXMoaGFuZGxlQWx3YXlzKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQWN0aXZhdGVzIHRoZSBzbGlkZXIuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge051bWJlcn0gc2xpZGVySWQgU2xpZGVyIElELlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkY2xpY2tlZFN3aXRjaGVyIENsaWNrZWQgc2xpZGVyIGVsZW1lbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2FjdGl2YXRlKHNsaWRlcklkLCAkY2xpY2tlZFN3aXRjaGVyKSB7XG5cdFx0XHQvLyBSZXF1ZXN0IG9wdGlvbnMuXG5cdFx0XHRjb25zdCByZXF1ZXN0T3B0aW9ucyA9IHtcblx0XHRcdFx0dHlwZTogJ1BPU1QnLFxuXHRcdFx0XHRkYXRhOiB7c2xpZGVySWR9LFxuXHRcdFx0XHR1cmw6IHVyaXMuYWN0aXZhdGUsXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBIYW5kbGVzIHRoZSAnYWx3YXlzJyBjYXNlIGJ5IGVuYWJsaW5nIHRoZSBjbGlja2VkIHNsaWRlci5cblx0XHRcdGNvbnN0IGhhbmRsZUFsd2F5cyA9ICgpID0+IF90b2dnbGVTd2l0Y2hlcnModHJ1ZSk7XG5cdFx0XHRcblx0XHRcdC8vIEhhbmRsZXMgdGhlICdkb25lJyBjYXNlIHdpdGggdGhlIHNlcnZlciByZXNwb25zZS5cblx0XHRcdGNvbnN0IGhhbmRsZURvbmUgPSByZXNwb25zZSA9PiB7XG5cdFx0XHRcdC8vIENsaWNrZWQgc3dpdGNoZXIncyBjaGVja2JveC5cblx0XHRcdFx0Y29uc3QgJGNoZWNrYm94ID0gJGNsaWNrZWRTd2l0Y2hlci5maW5kKCc6Y2hlY2tib3gnKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEVuYWJsZSBhbGwgc3dpdGNoZXJzLlxuXHRcdFx0XHRfdG9nZ2xlU3dpdGNoZXJzKHRydWUpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQ2hlY2sgc3dpdGNoZXIuXG5cdFx0XHRcdCRjaGVja2JveC5zd2l0Y2hlcignY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRGVhY3RpdmF0ZSBlYWNoIHNsaWRlciwgZXhjZXB0IHRoZSBjbGlja2VkIG9uZS5cblx0XHRcdFx0JHRoaXNcblx0XHRcdFx0XHQuZmluZChzZWxlY3RvcnMuc3dpdGNoZXJDaGVja2JveClcblx0XHRcdFx0XHQubm90KCRjaGVja2JveClcblx0XHRcdFx0XHQuZWFjaCgoaW5kZXgsIGNoZWNrYm94KSA9PiAkKGNoZWNrYm94KS5zd2l0Y2hlcignY2hlY2tlZCcsIGZhbHNlKSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBOb3RpZnkgdXNlci5cblx0XHRcdFx0X25vdGlmeShyZXNwb25zZS5pbmNsdWRlcygnc3VjY2VzcycpKTtcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIFBlcmZvcm0gcmVxdWVzdC5cblx0XHRcdCQuYWpheChyZXF1ZXN0T3B0aW9ucylcblx0XHRcdFx0LmRvbmUoaGFuZGxlRG9uZSlcblx0XHRcdFx0LmFsd2F5cyhoYW5kbGVBbHdheXMpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJZiB0aGUgc2VydmVyIHJlc3BvbnNlIGlzIHN1Y2Nlc3NmdWwsIGl0IHJlbW92ZXMgYW55IHByZXZpb3VzIG1lc3NhZ2VzIGFuZFxuXHRcdCAqIGFkZHMgbmV3IHN1Y2Nlc3MgbWVzc2FnZSB0byBhZG1pbiBpbmZvIGJveC5cblx0XHQgKlxuXHRcdCAqIE90aGVyd2lzZSBpdHMgc2hvd3MgYW4gZXJyb3IgbWVzc2FnZSBtb2RhbC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7Qm9vbGVhbn0gaXNTdWNjZXNzZnVsIElzIHRoZSBzZXJ2ZXIgcmVzcG9uc2Ugc3VjY2Vzc2Z1bD9cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfbm90aWZ5KGlzU3VjY2Vzc2Z1bCkge1xuXHRcdFx0aWYgKGlzU3VjY2Vzc2Z1bCkge1xuXHRcdFx0XHRqc2UubGlicy5pbmZvX2JveC5kZWxldGVCeVNvdXJjZSgnYWRtaW5BY3Rpb24nKS50aGVuKCgpID0+IGpzZS5saWJzLmluZm9fYm94LmFkZFN1Y2Nlc3NNZXNzYWdlKCkpXG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnU0xJREVSX1NUQVJUX1BBR0VfRVJST1JfVElUTEUnLCAnc2xpZGVycycpLFxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTTElERVJfU1RBUlRfUEFHRV9FUlJPUl9URVhUJywgJ3NsaWRlcnMnKVxuXHRcdFx0XHQpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBFbmFibGVzIG9yIGRpc2FibGVzIHRoZSBzd2l0Y2hlcnMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge0Jvb2xlYW59IGRvRW5hYmxlIEVuYWJsZSB0aGUgc3dpdGNoZXJzP1xuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF90b2dnbGVTd2l0Y2hlcnMoZG9FbmFibGUpIHtcblx0XHRcdCR0aGlzLmZpbmQoc2VsZWN0b3JzLnN3aXRjaGVyKVtgJHtkb0VuYWJsZSA/ICdyZW1vdmUnIDogJ2FkZCd9Q2xhc3NgXSgnZGlzYWJsZWQnKTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdC8vIExpc3RlbiB0byBzZXQgc3RhcnQgcGFnZSBldmVudC5cblx0XHRcdCR0aGlzLm9uKCdjaGFuZ2UnLCBzZWxlY3RvcnMuc3dpdGNoZXIsIF9vblN3aXRjaGVyQ2hhbmdlKTtcblx0XHRcdFxuXHRcdFx0Ly8gRmluaXNoIGluaXRpYWxpemF0aW9uLlxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKTtcbiJdfQ==
