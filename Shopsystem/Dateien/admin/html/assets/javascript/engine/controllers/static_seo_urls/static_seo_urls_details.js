'use strict';

/* --------------------------------------------------------------
 static_seo_urls_details.js 2017-05-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Static Seo Url Edit Form
 *
 * Handles the static seo url details form operations.
 */
gx.controllers.module('static_seo_urls_details', ['modal', 'loading_spinner'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Master data panel selector.
  *
  * @type {jQuery}
  */
	var $masterDataPanel = $this.find('.panel.master-data');

	/**
  * Static seo url panel containers (each language holds a container, that contains the static seo url panels).
  *
  * @type {jQuery}
  */
	var $tabContents = $this.find('.tab-pane');

	/**
  * Spinner Selector
  *
  * @type {jQuery}
  */
	var $spinner = null;

	/**
  * Do a refresh instead of redirecting?
  *
  * @type {Boolean}
  */
	var doRefresh = false;

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {};

	/**
  * Final Options
  *
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * Handles the form submit event.
  *
  * @param {jQuery.Event} event Triggered event.
  * @param {Object} data Event data.
  */
	function _onFormSubmit(event, data) {
		// Prevent the submit of the form.
		event.preventDefault();

		// Check refresh parameter.
		if (data && data.refresh) {
			doRefresh = true;
		}

		// Show loading spinner.
		$spinner = jse.libs.loading_spinner.show($this);

		// Upload files.
		_performSubmit().then(function () {
			return jse.libs.loading_spinner.hide($spinner);
		});
	}

	/**
  * Shows the submit error message modal.
  */
	function _showFailMessage() {
		// Message texts.
		var errorTitle = jse.core.lang.translate('FORM_SUBMIT_ERROR_MODAL_TITLE', 'static_seo_urls');
		var errorMessage = jse.core.lang.translate('FORM_SUBMIT_ERROR_MODAL_TEXT', 'static_seo_urls');

		// Show modal.
		jse.libs.modal.showMessage(errorTitle, errorMessage);

		// Hide loading spinner.
		if ($spinner) {
			jse.libs.loading_spinner.hide($spinner);
		}
	}

	/**
  * Performs the form submit AJAX request.
  *
  * @return {jQuery.jqXHR} jQuery XHR object.
  */
	function _performSubmit() {
		return $.ajax({
			method: 'POST',
			url: $this.attr('action'),
			data: _getFormData(),
			dataType: 'json'
		}).done(function (response) {
			// URL to redirect to.
			var url = doRefresh ? 'admin.php?do=StaticSeoUrl/details&id=' + response.id : options.redirectUrl;

			// Prevent that the page unload prompt is displayed on save action.
			window.onbeforeunload = null;

			// Open overview page.
			window.open(url, '_self');
		}, 'json').fail(_showFailMessage);
	}

	/**
  * Returns the gathered form data.
  *
  * @return {Object} Form data.
  */
	function _getFormData() {
		// Form data object.
		var data = {
			id: $this.data('id')
		};

		// Extend form data object with all necessary properties.
		return $.extend(true, data, _getMasterData(), _getStaticSeoUrlContentsData());
	}

	/**
  * Returns the static_seo_url's master data.
  *
  * @return {Object} Static seo urls master data.
  */
	function _getMasterData() {
		var name = $masterDataPanel.find('input[name="name"]').val();

		var sitemapEntry = $masterDataPanel.find('input[name="sitemap_entry"]').is(":checked");

		var changefreq = $masterDataPanel.find('select[name="changefreq"]').val();

		var priority = $masterDataPanel.find('select[name="priority"]').val();

		var robotsEntry = $masterDataPanel.find('input[name="robots_disallow_entry"]').is(":checked");

		return { name: name, sitemapEntry: sitemapEntry, changefreq: changefreq, priority: priority, robotsEntry: robotsEntry };
	}

	/**
  * Returns the static seo url contents data by iterating over the tab content elements
  * which represent a container for each language.
  *
  * The returned object contains a property for each language.
  * The key is the language code and the value is an array containing
  * all contents information as collection.
  *
  * Example output:
  * {
  *   de: [{
  *     id: 1,
  *     title: 'Welcome...',
  *     ...
  *   }]
  * }
  *
  * @return {Object} Static seo url contents data.
  */
	function _getStaticSeoUrlContentsData() {
		// Static seo url data object.
		var staticSeoUrlContents = {};

		// Iterate over each content panel container.
		$tabContents.each(function (index, element) {
			// Static seo url panel container element.
			var $staticSeoUrlContentPanelContainer = $(element);

			// Static seo url panel elements.
			var $staticSeoUrlContentPanels = $staticSeoUrlContentPanelContainer.find('.panel');

			// Get static seo url panel container language code.
			var languageCode = $staticSeoUrlContentPanelContainer.data('language');

			// Add property to static seo url contents data object,
			// which contains the language code as key and the static seo url contents data as value.
			staticSeoUrlContents[languageCode] = $.map($staticSeoUrlContentPanels, function (element) {
				return _getStaticSeoUrlContentData(element);
			});
		});

		return { staticSeoUrlContents: staticSeoUrlContents };
	}

	/**
  * Returns the data for a static seo url.
  *
  * @param {HTMLElement} staticSeoUrlContentPanel Static seo url content panel element.
  *
  * @return {Object} Static seo url data.
  */
	function _getStaticSeoUrlContentData(staticSeoUrlContentPanel) {
		var $element = $(staticSeoUrlContentPanel);
		return {
			id: $element.data('id'),
			title: $element.find('input[name="title"]').val(),
			description: $element.find('textarea[name="description"]').val(),
			keywords: $element.find('textarea[name="keywords"]').val()
		};
	}

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Listen to form submit event.
		$this.on('submit', _onFormSubmit);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0YXRpY19zZW9fdXJscy9zdGF0aWNfc2VvX3VybHNfZGV0YWlscy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRtYXN0ZXJEYXRhUGFuZWwiLCJmaW5kIiwiJHRhYkNvbnRlbnRzIiwiJHNwaW5uZXIiLCJkb1JlZnJlc2giLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfb25Gb3JtU3VibWl0IiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsInJlZnJlc2giLCJqc2UiLCJsaWJzIiwibG9hZGluZ19zcGlubmVyIiwic2hvdyIsIl9wZXJmb3JtU3VibWl0IiwidGhlbiIsImhpZGUiLCJfc2hvd0ZhaWxNZXNzYWdlIiwiZXJyb3JUaXRsZSIsImNvcmUiLCJsYW5nIiwidHJhbnNsYXRlIiwiZXJyb3JNZXNzYWdlIiwibW9kYWwiLCJzaG93TWVzc2FnZSIsImFqYXgiLCJtZXRob2QiLCJ1cmwiLCJhdHRyIiwiX2dldEZvcm1EYXRhIiwiZGF0YVR5cGUiLCJkb25lIiwicmVzcG9uc2UiLCJpZCIsInJlZGlyZWN0VXJsIiwid2luZG93Iiwib25iZWZvcmV1bmxvYWQiLCJvcGVuIiwiZmFpbCIsIl9nZXRNYXN0ZXJEYXRhIiwiX2dldFN0YXRpY1Nlb1VybENvbnRlbnRzRGF0YSIsIm5hbWUiLCJ2YWwiLCJzaXRlbWFwRW50cnkiLCJpcyIsImNoYW5nZWZyZXEiLCJwcmlvcml0eSIsInJvYm90c0VudHJ5Iiwic3RhdGljU2VvVXJsQ29udGVudHMiLCJlYWNoIiwiaW5kZXgiLCJlbGVtZW50IiwiJHN0YXRpY1Nlb1VybENvbnRlbnRQYW5lbENvbnRhaW5lciIsIiRzdGF0aWNTZW9VcmxDb250ZW50UGFuZWxzIiwibGFuZ3VhZ2VDb2RlIiwibWFwIiwiX2dldFN0YXRpY1Nlb1VybENvbnRlbnREYXRhIiwic3RhdGljU2VvVXJsQ29udGVudFBhbmVsIiwiJGVsZW1lbnQiLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwia2V5d29yZHMiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MseUJBREQsRUFFQyxDQUNDLE9BREQsRUFFQyxpQkFGRCxDQUZELEVBT0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsbUJBQW1CRixNQUFNRyxJQUFOLENBQVcsb0JBQVgsQ0FBekI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsZUFBZUosTUFBTUcsSUFBTixDQUFXLFdBQVgsQ0FBckI7O0FBRUE7Ozs7O0FBS0EsS0FBSUUsV0FBVyxJQUFmOztBQUVBOzs7OztBQUtBLEtBQUlDLFlBQVksS0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsV0FBVyxFQUFqQjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxVQUFVUCxFQUFFUSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCUixJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFNQSxVQUFTWSxhQUFULENBQXVCQyxLQUF2QixFQUE4QlosSUFBOUIsRUFBb0M7QUFDbkM7QUFDQVksUUFBTUMsY0FBTjs7QUFFQTtBQUNBLE1BQUliLFFBQVFBLEtBQUtjLE9BQWpCLEVBQTBCO0FBQ3pCUCxlQUFZLElBQVo7QUFDQTs7QUFFRDtBQUNBRCxhQUFXUyxJQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLElBQXpCLENBQThCakIsS0FBOUIsQ0FBWDs7QUFFQTtBQUNBa0IsbUJBQ0VDLElBREYsQ0FDTztBQUFBLFVBQU1MLElBQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkksSUFBekIsQ0FBOEJmLFFBQTlCLENBQU47QUFBQSxHQURQO0FBRUE7O0FBRUQ7OztBQUdBLFVBQVNnQixnQkFBVCxHQUE0QjtBQUMzQjtBQUNBLE1BQU1DLGFBQWFSLElBQUlTLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLCtCQUF4QixFQUF5RCxpQkFBekQsQ0FBbkI7QUFDQSxNQUFNQyxlQUFlWixJQUFJUyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qiw4QkFBeEIsRUFBd0QsaUJBQXhELENBQXJCOztBQUVBO0FBQ0FYLE1BQUlDLElBQUosQ0FBU1ksS0FBVCxDQUFlQyxXQUFmLENBQTJCTixVQUEzQixFQUF1Q0ksWUFBdkM7O0FBRUE7QUFDQSxNQUFJckIsUUFBSixFQUFjO0FBQ2JTLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkksSUFBekIsQ0FBOEJmLFFBQTlCO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7QUFLQSxVQUFTYSxjQUFULEdBQTBCO0FBQ3pCLFNBQU9qQixFQUFFNEIsSUFBRixDQUFPO0FBQ2JDLFdBQVEsTUFESztBQUViQyxRQUFLL0IsTUFBTWdDLElBQU4sQ0FBVyxRQUFYLENBRlE7QUFHYmpDLFNBQU1rQyxjQUhPO0FBSWJDLGFBQVU7QUFKRyxHQUFQLEVBTUxDLElBTkssQ0FNQSxvQkFBWTtBQUNqQjtBQUNBLE9BQU1KLE1BQU16QixzREFBb0Q4QixTQUFTQyxFQUE3RCxHQUFvRTdCLFFBQVE4QixXQUF4Rjs7QUFFQTtBQUNBQyxVQUFPQyxjQUFQLEdBQXdCLElBQXhCOztBQUVBO0FBQ0FELFVBQU9FLElBQVAsQ0FBWVYsR0FBWixFQUFpQixPQUFqQjtBQUNBLEdBZkssRUFlSCxNQWZHLEVBZ0JMVyxJQWhCSyxDQWdCQXJCLGdCQWhCQSxDQUFQO0FBaUJBOztBQUdEOzs7OztBQUtBLFVBQVNZLFlBQVQsR0FBd0I7QUFDdkI7QUFDQSxNQUFNbEMsT0FBTztBQUNac0MsT0FBSXJDLE1BQU1ELElBQU4sQ0FBVyxJQUFYO0FBRFEsR0FBYjs7QUFJQTtBQUNBLFNBQU9FLEVBQUVRLE1BQUYsQ0FBUyxJQUFULEVBQWVWLElBQWYsRUFBcUI0QyxnQkFBckIsRUFBdUNDLDhCQUF2QyxDQUFQO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0QsY0FBVCxHQUEwQjtBQUN6QixNQUFNRSxPQUFPM0MsaUJBQ1hDLElBRFcsQ0FDTixvQkFETSxFQUVYMkMsR0FGVyxFQUFiOztBQUlBLE1BQU1DLGVBQWU3QyxpQkFDbkJDLElBRG1CLENBQ2QsNkJBRGMsRUFFbkI2QyxFQUZtQixDQUVoQixVQUZnQixDQUFyQjs7QUFJQSxNQUFNQyxhQUFhL0MsaUJBQ2pCQyxJQURpQixDQUNaLDJCQURZLEVBRWpCMkMsR0FGaUIsRUFBbkI7O0FBSUEsTUFBTUksV0FBV2hELGlCQUNmQyxJQURlLENBQ1YseUJBRFUsRUFFZjJDLEdBRmUsRUFBakI7O0FBSUEsTUFBTUssY0FBY2pELGlCQUNsQkMsSUFEa0IsQ0FDYixxQ0FEYSxFQUVsQjZDLEVBRmtCLENBRWYsVUFGZSxDQUFwQjs7QUFJQSxTQUFPLEVBQUNILFVBQUQsRUFBT0UsMEJBQVAsRUFBcUJFLHNCQUFyQixFQUFpQ0Msa0JBQWpDLEVBQTJDQyx3QkFBM0MsRUFBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLFVBQVNQLDRCQUFULEdBQXdDO0FBQ3ZDO0FBQ0EsTUFBTVEsdUJBQXVCLEVBQTdCOztBQUVBO0FBQ0FoRCxlQUFhaUQsSUFBYixDQUFrQixVQUFDQyxLQUFELEVBQVFDLE9BQVIsRUFBb0I7QUFDckM7QUFDQSxPQUFNQyxxQ0FBcUN2RCxFQUFFc0QsT0FBRixDQUEzQzs7QUFFQTtBQUNBLE9BQU1FLDZCQUE2QkQsbUNBQW1DckQsSUFBbkMsQ0FBd0MsUUFBeEMsQ0FBbkM7O0FBRUE7QUFDQSxPQUFNdUQsZUFBZUYsbUNBQW1DekQsSUFBbkMsQ0FBd0MsVUFBeEMsQ0FBckI7O0FBRUE7QUFDQTtBQUNBcUQsd0JBQXFCTSxZQUFyQixJQUNDekQsRUFBRTBELEdBQUYsQ0FBTUYsMEJBQU4sRUFBa0M7QUFBQSxXQUFXRyw0QkFBNEJMLE9BQTVCLENBQVg7QUFBQSxJQUFsQyxDQUREO0FBRUEsR0FkRDs7QUFnQkEsU0FBTyxFQUFDSCwwQ0FBRCxFQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTUSwyQkFBVCxDQUFxQ0Msd0JBQXJDLEVBQStEO0FBQzlELE1BQU1DLFdBQVc3RCxFQUFFNEQsd0JBQUYsQ0FBakI7QUFDQSxTQUFPO0FBQ054QixPQUFJeUIsU0FBUy9ELElBQVQsQ0FBYyxJQUFkLENBREU7QUFFTmdFLFVBQU9ELFNBQVMzRCxJQUFULENBQWMscUJBQWQsRUFBcUMyQyxHQUFyQyxFQUZEO0FBR05rQixnQkFBYUYsU0FBUzNELElBQVQsQ0FBYyw4QkFBZCxFQUE4QzJDLEdBQTlDLEVBSFA7QUFJTm1CLGFBQVVILFNBQVMzRCxJQUFULENBQWMsMkJBQWQsRUFBMkMyQyxHQUEzQztBQUpKLEdBQVA7QUFNQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUFoRCxRQUFPb0UsSUFBUCxHQUFjLGdCQUFRO0FBQ3JCO0FBQ0FsRSxRQUFNbUUsRUFBTixDQUFTLFFBQVQsRUFBbUJ6RCxhQUFuQjs7QUFFQTtBQUNBeUI7QUFDQSxFQU5EOztBQVFBLFFBQU9yQyxNQUFQO0FBQ0EsQ0FsUUYiLCJmaWxlIjoic3RhdGljX3Nlb191cmxzL3N0YXRpY19zZW9fdXJsc19kZXRhaWxzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzdGF0aWNfc2VvX3VybHNfZGV0YWlscy5qcyAyMDE3LTA1LTI0XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBDb250cm9sbGVyIE1vZHVsZSBGb3IgU3RhdGljIFNlbyBVcmwgRWRpdCBGb3JtXG4gKlxuICogSGFuZGxlcyB0aGUgc3RhdGljIHNlbyB1cmwgZGV0YWlscyBmb3JtIG9wZXJhdGlvbnMuXG4gKi9cbmd4LmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3N0YXRpY19zZW9fdXJsc19kZXRhaWxzJyxcblx0W1xuXHRcdCdtb2RhbCcsXG5cdFx0J2xvYWRpbmdfc3Bpbm5lcidcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTWFzdGVyIGRhdGEgcGFuZWwgc2VsZWN0b3IuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRtYXN0ZXJEYXRhUGFuZWwgPSAkdGhpcy5maW5kKCcucGFuZWwubWFzdGVyLWRhdGEnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTdGF0aWMgc2VvIHVybCBwYW5lbCBjb250YWluZXJzIChlYWNoIGxhbmd1YWdlIGhvbGRzIGEgY29udGFpbmVyLCB0aGF0IGNvbnRhaW5zIHRoZSBzdGF0aWMgc2VvIHVybCBwYW5lbHMpLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGFiQ29udGVudHMgPSAkdGhpcy5maW5kKCcudGFiLXBhbmUnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTcGlubmVyIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGxldCAkc3Bpbm5lciA9IG51bGw7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRG8gYSByZWZyZXNoIGluc3RlYWQgb2YgcmVkaXJlY3Rpbmc/XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7Qm9vbGVhbn1cblx0XHQgKi9cblx0XHRsZXQgZG9SZWZyZXNoID0gZmFsc2U7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGRlZmF1bHRzID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIGZvcm0gc3VibWl0IGV2ZW50LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50IFRyaWdnZXJlZCBldmVudC5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YSBFdmVudCBkYXRhLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vbkZvcm1TdWJtaXQoZXZlbnQsIGRhdGEpIHtcblx0XHRcdC8vIFByZXZlbnQgdGhlIHN1Ym1pdCBvZiB0aGUgZm9ybS5cblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrIHJlZnJlc2ggcGFyYW1ldGVyLlxuXHRcdFx0aWYgKGRhdGEgJiYgZGF0YS5yZWZyZXNoKSB7XG5cdFx0XHRcdGRvUmVmcmVzaCA9IHRydWU7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgbG9hZGluZyBzcGlubmVyLlxuXHRcdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcyk7XG5cdFx0XHRcblx0XHRcdC8vIFVwbG9hZCBmaWxlcy5cblx0XHRcdF9wZXJmb3JtU3VibWl0KClcblx0XHRcdFx0LnRoZW4oKCkgPT4ganNlLmxpYnMubG9hZGluZ19zcGlubmVyLmhpZGUoJHNwaW5uZXIpKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2hvd3MgdGhlIHN1Ym1pdCBlcnJvciBtZXNzYWdlIG1vZGFsLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9zaG93RmFpbE1lc3NhZ2UoKSB7XG5cdFx0XHQvLyBNZXNzYWdlIHRleHRzLlxuXHRcdFx0Y29uc3QgZXJyb3JUaXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdGT1JNX1NVQk1JVF9FUlJPUl9NT0RBTF9USVRMRScsICdzdGF0aWNfc2VvX3VybHMnKTtcblx0XHRcdGNvbnN0IGVycm9yTWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdGT1JNX1NVQk1JVF9FUlJPUl9NT0RBTF9URVhUJywgJ3N0YXRpY19zZW9fdXJscycpO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsLlxuXHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoZXJyb3JUaXRsZSwgZXJyb3JNZXNzYWdlKTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHRpZiAoJHNwaW5uZXIpIHtcblx0XHRcdFx0anNlLmxpYnMubG9hZGluZ19zcGlubmVyLmhpZGUoJHNwaW5uZXIpXG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFBlcmZvcm1zIHRoZSBmb3JtIHN1Ym1pdCBBSkFYIHJlcXVlc3QuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtqUXVlcnkuanFYSFJ9IGpRdWVyeSBYSFIgb2JqZWN0LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9wZXJmb3JtU3VibWl0KCkge1xuXHRcdFx0cmV0dXJuICQuYWpheCh7XG5cdFx0XHRcdG1ldGhvZDogJ1BPU1QnLFxuXHRcdFx0XHR1cmw6ICR0aGlzLmF0dHIoJ2FjdGlvbicpLFxuXHRcdFx0XHRkYXRhOiBfZ2V0Rm9ybURhdGEoKSxcblx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJ1xuXHRcdFx0fSlcblx0XHRcdFx0LmRvbmUocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdC8vIFVSTCB0byByZWRpcmVjdCB0by5cblx0XHRcdFx0XHRjb25zdCB1cmwgPSBkb1JlZnJlc2ggPyBgYWRtaW4ucGhwP2RvPVN0YXRpY1Nlb1VybC9kZXRhaWxzJmlkPSR7cmVzcG9uc2UuaWR9YCA6IG9wdGlvbnMucmVkaXJlY3RVcmw7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gUHJldmVudCB0aGF0IHRoZSBwYWdlIHVubG9hZCBwcm9tcHQgaXMgZGlzcGxheWVkIG9uIHNhdmUgYWN0aW9uLlxuXHRcdFx0XHRcdHdpbmRvdy5vbmJlZm9yZXVubG9hZCA9IG51bGw7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gT3BlbiBvdmVydmlldyBwYWdlLlxuXHRcdFx0XHRcdHdpbmRvdy5vcGVuKHVybCwgJ19zZWxmJyk7XG5cdFx0XHRcdH0sICdqc29uJylcblx0XHRcdFx0LmZhaWwoX3Nob3dGYWlsTWVzc2FnZSk7XG5cdFx0fVxuXHRcdFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJldHVybnMgdGhlIGdhdGhlcmVkIGZvcm0gZGF0YS5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge09iamVjdH0gRm9ybSBkYXRhLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRGb3JtRGF0YSgpIHtcblx0XHRcdC8vIEZvcm0gZGF0YSBvYmplY3QuXG5cdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHRpZDogJHRoaXMuZGF0YSgnaWQnKVxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gRXh0ZW5kIGZvcm0gZGF0YSBvYmplY3Qgd2l0aCBhbGwgbmVjZXNzYXJ5IHByb3BlcnRpZXMuXG5cdFx0XHRyZXR1cm4gJC5leHRlbmQodHJ1ZSwgZGF0YSwgX2dldE1hc3RlckRhdGEoKSwgX2dldFN0YXRpY1Nlb1VybENvbnRlbnRzRGF0YSgpKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgc3RhdGljX3Nlb191cmwncyBtYXN0ZXIgZGF0YS5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge09iamVjdH0gU3RhdGljIHNlbyB1cmxzIG1hc3RlciBkYXRhLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRNYXN0ZXJEYXRhKCkge1xuXHRcdFx0Y29uc3QgbmFtZSA9ICRtYXN0ZXJEYXRhUGFuZWxcblx0XHRcdFx0LmZpbmQoJ2lucHV0W25hbWU9XCJuYW1lXCJdJylcblx0XHRcdFx0LnZhbCgpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCBzaXRlbWFwRW50cnkgPSAkbWFzdGVyRGF0YVBhbmVsXG5cdFx0XHRcdC5maW5kKCdpbnB1dFtuYW1lPVwic2l0ZW1hcF9lbnRyeVwiXScpXG5cdFx0XHRcdC5pcyhcIjpjaGVja2VkXCIpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCBjaGFuZ2VmcmVxID0gJG1hc3RlckRhdGFQYW5lbFxuXHRcdFx0XHQuZmluZCgnc2VsZWN0W25hbWU9XCJjaGFuZ2VmcmVxXCJdJylcblx0XHRcdFx0LnZhbCgpO1xuXHRcdFx0XG5cdFx0XHRjb25zdCBwcmlvcml0eSA9ICRtYXN0ZXJEYXRhUGFuZWxcblx0XHRcdFx0LmZpbmQoJ3NlbGVjdFtuYW1lPVwicHJpb3JpdHlcIl0nKVxuXHRcdFx0XHQudmFsKCk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IHJvYm90c0VudHJ5ID0gJG1hc3RlckRhdGFQYW5lbFxuXHRcdFx0XHQuZmluZCgnaW5wdXRbbmFtZT1cInJvYm90c19kaXNhbGxvd19lbnRyeVwiXScpXG5cdFx0XHRcdC5pcyhcIjpjaGVja2VkXCIpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4ge25hbWUsIHNpdGVtYXBFbnRyeSwgY2hhbmdlZnJlcSwgcHJpb3JpdHksIHJvYm90c0VudHJ5fTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgc3RhdGljIHNlbyB1cmwgY29udGVudHMgZGF0YSBieSBpdGVyYXRpbmcgb3ZlciB0aGUgdGFiIGNvbnRlbnQgZWxlbWVudHNcblx0XHQgKiB3aGljaCByZXByZXNlbnQgYSBjb250YWluZXIgZm9yIGVhY2ggbGFuZ3VhZ2UuXG5cdFx0ICpcblx0XHQgKiBUaGUgcmV0dXJuZWQgb2JqZWN0IGNvbnRhaW5zIGEgcHJvcGVydHkgZm9yIGVhY2ggbGFuZ3VhZ2UuXG5cdFx0ICogVGhlIGtleSBpcyB0aGUgbGFuZ3VhZ2UgY29kZSBhbmQgdGhlIHZhbHVlIGlzIGFuIGFycmF5IGNvbnRhaW5pbmdcblx0XHQgKiBhbGwgY29udGVudHMgaW5mb3JtYXRpb24gYXMgY29sbGVjdGlvbi5cblx0XHQgKlxuXHRcdCAqIEV4YW1wbGUgb3V0cHV0OlxuXHRcdCAqIHtcblx0XHQgKiAgIGRlOiBbe1xuXHRcdCAqICAgICBpZDogMSxcblx0XHQgKiAgICAgdGl0bGU6ICdXZWxjb21lLi4uJyxcblx0XHQgKiAgICAgLi4uXG5cdFx0ICogICB9XVxuXHRcdCAqIH1cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge09iamVjdH0gU3RhdGljIHNlbyB1cmwgY29udGVudHMgZGF0YS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0U3RhdGljU2VvVXJsQ29udGVudHNEYXRhKCkge1xuXHRcdFx0Ly8gU3RhdGljIHNlbyB1cmwgZGF0YSBvYmplY3QuXG5cdFx0XHRjb25zdCBzdGF0aWNTZW9VcmxDb250ZW50cyA9IHt9O1xuXHRcdFx0XG5cdFx0XHQvLyBJdGVyYXRlIG92ZXIgZWFjaCBjb250ZW50IHBhbmVsIGNvbnRhaW5lci5cblx0XHRcdCR0YWJDb250ZW50cy5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4ge1xuXHRcdFx0XHQvLyBTdGF0aWMgc2VvIHVybCBwYW5lbCBjb250YWluZXIgZWxlbWVudC5cblx0XHRcdFx0Y29uc3QgJHN0YXRpY1Nlb1VybENvbnRlbnRQYW5lbENvbnRhaW5lciA9ICQoZWxlbWVudCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTdGF0aWMgc2VvIHVybCBwYW5lbCBlbGVtZW50cy5cblx0XHRcdFx0Y29uc3QgJHN0YXRpY1Nlb1VybENvbnRlbnRQYW5lbHMgPSAkc3RhdGljU2VvVXJsQ29udGVudFBhbmVsQ29udGFpbmVyLmZpbmQoJy5wYW5lbCcpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gR2V0IHN0YXRpYyBzZW8gdXJsIHBhbmVsIGNvbnRhaW5lciBsYW5ndWFnZSBjb2RlLlxuXHRcdFx0XHRjb25zdCBsYW5ndWFnZUNvZGUgPSAkc3RhdGljU2VvVXJsQ29udGVudFBhbmVsQ29udGFpbmVyLmRhdGEoJ2xhbmd1YWdlJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBZGQgcHJvcGVydHkgdG8gc3RhdGljIHNlbyB1cmwgY29udGVudHMgZGF0YSBvYmplY3QsXG5cdFx0XHRcdC8vIHdoaWNoIGNvbnRhaW5zIHRoZSBsYW5ndWFnZSBjb2RlIGFzIGtleSBhbmQgdGhlIHN0YXRpYyBzZW8gdXJsIGNvbnRlbnRzIGRhdGEgYXMgdmFsdWUuXG5cdFx0XHRcdHN0YXRpY1Nlb1VybENvbnRlbnRzW2xhbmd1YWdlQ29kZV0gPVxuXHRcdFx0XHRcdCQubWFwKCRzdGF0aWNTZW9VcmxDb250ZW50UGFuZWxzLCBlbGVtZW50ID0+IF9nZXRTdGF0aWNTZW9VcmxDb250ZW50RGF0YShlbGVtZW50KSk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIHtzdGF0aWNTZW9VcmxDb250ZW50c307XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJldHVybnMgdGhlIGRhdGEgZm9yIGEgc3RhdGljIHNlbyB1cmwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBzdGF0aWNTZW9VcmxDb250ZW50UGFuZWwgU3RhdGljIHNlbyB1cmwgY29udGVudCBwYW5lbCBlbGVtZW50LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T2JqZWN0fSBTdGF0aWMgc2VvIHVybCBkYXRhLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRTdGF0aWNTZW9VcmxDb250ZW50RGF0YShzdGF0aWNTZW9VcmxDb250ZW50UGFuZWwpIHtcblx0XHRcdGNvbnN0ICRlbGVtZW50ID0gJChzdGF0aWNTZW9VcmxDb250ZW50UGFuZWwpO1xuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0aWQ6ICRlbGVtZW50LmRhdGEoJ2lkJyksXG5cdFx0XHRcdHRpdGxlOiAkZWxlbWVudC5maW5kKCdpbnB1dFtuYW1lPVwidGl0bGVcIl0nKS52YWwoKSxcblx0XHRcdFx0ZGVzY3JpcHRpb246ICRlbGVtZW50LmZpbmQoJ3RleHRhcmVhW25hbWU9XCJkZXNjcmlwdGlvblwiXScpLnZhbCgpLFxuXHRcdFx0XHRrZXl3b3JkczogJGVsZW1lbnQuZmluZCgndGV4dGFyZWFbbmFtZT1cImtleXdvcmRzXCJdJykudmFsKClcblx0XHRcdH07XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZG9uZSA9PiB7XG5cdFx0XHQvLyBMaXN0ZW4gdG8gZm9ybSBzdWJtaXQgZXZlbnQuXG5cdFx0XHQkdGhpcy5vbignc3VibWl0JywgX29uRm9ybVN1Ym1pdCk7XG5cdFx0XHRcblx0XHRcdC8vIEZpbmlzaCBpbml0aWFsaXphdGlvbi5cblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbilcbjsiXX0=
