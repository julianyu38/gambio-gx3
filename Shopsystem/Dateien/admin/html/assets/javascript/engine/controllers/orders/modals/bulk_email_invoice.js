'use strict';

/* --------------------------------------------------------------
 bulk_email_invoice.js 2017-10-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Bulk Email Invoice Modal Controller
 *
 * This controller handles the orders bulk invoice emails modal where the user can send emails for the
 * selected orders. The controller is able to create missing invoices if needed.
 */
gx.controllers.module('bulk_email_invoice', ['modal', 'loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {
		bindings: {
			subject: $this.find('.subject')
		}
	};

	/**
  * Selector for the email list item.
  *
  * @type {String}
  */
	var emailListItemSelector = '.email-list-item';

	/**
  * Selector for the email list item ID.
  *
  * @type {String}
  */
	var emailListItemEmailSelector = '.email-input';

	/**
  * Selector for the latest invoice number of the list item
  *
  * @type {string}
  */
	var latestInvoiceIdSelector = '.latest-invoice-id';

	/**
  * Selector for the option that indicates if missing invoices should be created
  *
  * @type {String}
  */
	var createMissingInvoicesSelector = '.create-missing-invoices';

	/**
  * Selector for the modal content body layer.
  *
  * @type {String}
  */
	var modalContentSelector = '.modal-content';

	/**
  * GM PDF Order URL
  *
  * @type {String}
  */
	var gmPdfOrderUrl = jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php';

	/**
  * Admin Request URL
  *
  * @type {String}
  */
	var adminRequestUrl = jse.core.config.get('appUrl') + '/admin/admin.php';

	/**
  * Loading Spinner Selector
  *
  * @type {jQuery|null}
  */
	var $spinner = null;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Show/hide loading spinner.
  *
  * @param {Boolean} spinnerVisibility Whether to show or hide the spinner.
  */
	function _toggleSpinner(spinnerVisibility) {
		if (spinnerVisibility) {
			$spinner = jse.libs.loading_spinner.show($this.find(modalContentSelector), $this.css('z-index'));
		} else {
			jse.libs.loading_spinner.hide($spinner);
		}
	}

	/**
  * Parse subject and replace the placeholders with the variables.
  *
  * @param {Object} orderData Contains the order's data.
  * @param {Object} subjectData Contains the required subject's data.
  *
  * @return {String} Returns the final email subject string.
  */
	function _getParsedSubject(orderData, subjectData) {
		return module.bindings.subject.get().replace('{INVOICE_NUM}', subjectData.invoiceNumber).replace('{INVOICE_ID}', subjectData.invoiceNumber).replace('{INVOICE_DATE}', subjectData.invoiceDate).replace('{DATE}', subjectData.invoiceDate).replace('{ORDER_ID}', orderData.id);
	}

	/**
  * Handles the successful delivery of all messages.
  */
	function _handleDeliverySuccess() {
		var message = jse.core.lang.translate('BULK_MAIL_SUCCESS', 'gm_send_order');

		// Show success message in the admin info box.
		jse.libs.info_box.addSuccessMessage(message);

		$('.orders .table-main').DataTable().ajax.reload(null, false);
		$('.orders .table-main').orders_overview_filter('reload');

		// Hide modal and loading spinner.
		_toggleSpinner(false);
		$this.modal('hide');
	}

	/**
  * Handles the failure of the message delivery.
  */
	function _handleDeliveryFailure() {
		var title = jse.core.lang.translate('error', 'messages');
		var content = jse.core.lang.translate('BULK_MAIL_UNSUCCESS', 'gm_send_order');

		// Show error message in a modal.
		jse.libs.modal.message({ title: title, content: content });

		// Hide modal and the loading spinner and re-enable the send button.
		_toggleSpinner(false);
		$this.modal('hide');
	}

	/**
  * Get the IDs of the orders that do not have an invoice.
  *
  * @param {Number[]} orderIds The orders to be validated.
  *
  * @return {Promise}
  */
	function _getOrdersWithoutDocuments(orderIds) {
		return new Promise(function (resolve, reject) {
			var data = {
				do: 'OrdersOverviewAjax/GetOrdersWithoutDocuments',
				pageToken: jse.core.config.get('pageToken'),
				type: 'invoice',
				orderIds: orderIds
			};

			$.getJSON(adminRequestUrl, data).done(function (orderIdsWithoutDocument) {
				return resolve({ orderIds: orderIds, orderIdsWithoutDocument: orderIdsWithoutDocument });
			}).fail(reject);
		});
	}

	/**
  * Validate selected orders and generate/remove the orders without documents.
  *
  * @param {Object} selection Contains the "orderIds" and "orderIdsWithoutDocument" properties.
  *
  * @return {Promise} Returns the promises of the delegated methods.
  */
	function _handleMissingDocuments(selection) {
		// Indicates if missing invoices should be created
		var createMissingInvoices = $this.find(createMissingInvoicesSelector).prop('checked');

		if (createMissingInvoices) {
			return _createMissingDocuments(selection.orderIds, selection.orderIdsWithoutDocument);
		} else {
			return _removeOrderIdsWithoutDocument(selection.orderIds, selection.orderIdsWithoutDocument);
		}
	}

	/**
  * Create Missing Order Documents and set the new latest invoice id selector for which no invoice has yet existed.
  *
  * @param {Number[]} orderIds Selected order IDs.
  * @param {Number[]} orderIdsWithoutDocument Order IDs that do not have a document.
  *
  * @return {Promise} Returns a promise that will be resolved with all the order IDs.
  */
	function _createMissingDocuments(orderIds, orderIdsWithoutDocument) {
		return new Promise(function (resolve) {
			var _$;

			var requests = [];

			orderIdsWithoutDocument.forEach(function (id) {
				var url = gmPdfOrderUrl + ('?oID=' + id + '&type=invoice&ajax=1');
				var request = $.getJSON(url);

				request.done(function (response) {
					$(emailListItemSelector).each(function (index, emailListItem) {
						var $emailListItem = $(emailListItem);

						if ($emailListItem.data('order').id === parseInt(id)) {
							$emailListItem.find(latestInvoiceIdSelector).val(response.invoiceId);
							return false;
						}
					});
				});

				requests.push(request);
			});

			return (_$ = $).when.apply(_$, requests).done(function () {
				return resolve(orderIds);
			});
		});
	}

	/**
  * Remove order IDs that do not have a document.
  *
  * @param {Number[]} orderIds Selected order IDs.
  * @param {Number[]} orderIdsWithoutDocument Order IDs that do not have a document.
  *
  * @return {Promise} Returns a promise that will be resolved with the orders that do have a document.
  */
	function _removeOrderIdsWithoutDocument(orderIds, orderIdsWithoutDocument) {
		return new Promise(function (resolve) {
			var orderIdsWithDocument = orderIds.filter(function (orderId) {
				return !orderIdsWithoutDocument.includes(String(orderId));
			});
			resolve(orderIdsWithDocument);
		});
	}

	/**
  * Get the required data for the email subject of the provided invoice.
  *
  * @param {Number} invoiceId Get the subject information for the selected invoice.
  *
  * @return {jQuery.jqXHR} The request will be resolved with the subject data.
  */
	function _getSubjectData(invoiceId) {
		return $.ajax({
			url: adminRequestUrl + '?do=OrdersModalsAjax/GetEmailInvoiceSubjectData',
			method: 'POST',
			dataType: 'json',
			data: {
				invoiceId: invoiceId,
				pageToken: jse.core.config.get('pageToken')
			}
		});
	}

	/**
  * Send Invoice Emails
  *
  * @param {Number[]} orderIds Contains the IDs of the orders to be finally sent with an email.
  */
	function _sendInvoiceEmails(orderIds) {
		return new Promise(function (resolve, reject) {
			var _$2;

			var createMissingInvoices = $this.find(createMissingInvoicesSelector).prop('checked');
			var $emailListItems = $this.find(emailListItemSelector);

			// Abort and hide modal on empty email list entries.
			if (!$emailListItems.length || !orderIds.length) {
				var title = jse.core.lang.translate('TITLE_INVOICE', 'gm_order_menu');
				var message = jse.core.lang.translate('NO_RECORDS_ERROR', 'orders');
				jse.libs.modal.showMessage(title, message);
				$this.modal('hide');
				return;
			}

			// Show loading spinner.
			_toggleSpinner(true);

			// Collection of requests in promise format.
			var requests = [];

			// Fill orders array with data.
			$emailListItems.each(function (index, emailListItem) {
				var orderData = $(emailListItem).data('order');

				if (!orderIds.includes(orderData.id)) {
					return true; // Current order does not have an invoice document.
				}

				// Email address entered in input field.
				var email = $(emailListItem).find(emailListItemEmailSelector).val();

				// The latest invoice id of the order
				var invoiceId = $(emailListItem).find(latestInvoiceIdSelector).val();

				_getSubjectData(invoiceId).then(function (subjectData) {
					// Request GET parameters to send.
					var parameters = {
						oID: orderData.id,
						type: 'invoice',
						mail: '1',
						gm_quick_mail: '1'
					};

					var url = gmPdfOrderUrl + '?' + $.param(parameters);
					var data = {
						gm_mail: email,
						gm_subject: _getParsedSubject(orderData, subjectData),
						create_missing_invoices: Number(createMissingInvoices) // 1 or 0
					};

					if (invoiceId !== '0') {
						data.invoice_ids = [invoiceId];
					}

					// Create AJAX request.
					requests.push($.ajax({ method: 'POST', url: url, data: data }));
				});
			});

			(_$2 = $).when.apply(_$2, requests).done(resolve).fail(reject);
		});
	}

	/**
  * Send the invoice emails.
  *
  * This method will only send emails for the orders that do have an invoice document. If the
  * "create-missing-invoices" checkbox is active, new invoices will be generated for the orders that
  * are do not have one.
  */
	function _onSendClick() {
		var orderIds = [];

		$this.find(emailListItemSelector).each(function (index, emailListItem) {
			orderIds.push($(emailListItem).data('order').id);
		});

		_getOrdersWithoutDocuments(orderIds).then(_handleMissingDocuments).then(_sendInvoiceEmails).then(_handleDeliverySuccess).catch(_handleDeliveryFailure);
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9tb2RhbHMvYnVsa19lbWFpbF9pbnZvaWNlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiYmluZGluZ3MiLCJzdWJqZWN0IiwiZmluZCIsImVtYWlsTGlzdEl0ZW1TZWxlY3RvciIsImVtYWlsTGlzdEl0ZW1FbWFpbFNlbGVjdG9yIiwibGF0ZXN0SW52b2ljZUlkU2VsZWN0b3IiLCJjcmVhdGVNaXNzaW5nSW52b2ljZXNTZWxlY3RvciIsIm1vZGFsQ29udGVudFNlbGVjdG9yIiwiZ21QZGZPcmRlclVybCIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJhZG1pblJlcXVlc3RVcmwiLCIkc3Bpbm5lciIsIl90b2dnbGVTcGlubmVyIiwic3Bpbm5lclZpc2liaWxpdHkiLCJsaWJzIiwibG9hZGluZ19zcGlubmVyIiwic2hvdyIsImNzcyIsImhpZGUiLCJfZ2V0UGFyc2VkU3ViamVjdCIsIm9yZGVyRGF0YSIsInN1YmplY3REYXRhIiwicmVwbGFjZSIsImludm9pY2VOdW1iZXIiLCJpbnZvaWNlRGF0ZSIsImlkIiwiX2hhbmRsZURlbGl2ZXJ5U3VjY2VzcyIsIm1lc3NhZ2UiLCJsYW5nIiwidHJhbnNsYXRlIiwiaW5mb19ib3giLCJhZGRTdWNjZXNzTWVzc2FnZSIsIkRhdGFUYWJsZSIsImFqYXgiLCJyZWxvYWQiLCJvcmRlcnNfb3ZlcnZpZXdfZmlsdGVyIiwibW9kYWwiLCJfaGFuZGxlRGVsaXZlcnlGYWlsdXJlIiwidGl0bGUiLCJjb250ZW50IiwiX2dldE9yZGVyc1dpdGhvdXREb2N1bWVudHMiLCJvcmRlcklkcyIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwiZG8iLCJwYWdlVG9rZW4iLCJ0eXBlIiwiZ2V0SlNPTiIsImRvbmUiLCJvcmRlcklkc1dpdGhvdXREb2N1bWVudCIsImZhaWwiLCJfaGFuZGxlTWlzc2luZ0RvY3VtZW50cyIsInNlbGVjdGlvbiIsImNyZWF0ZU1pc3NpbmdJbnZvaWNlcyIsInByb3AiLCJfY3JlYXRlTWlzc2luZ0RvY3VtZW50cyIsIl9yZW1vdmVPcmRlcklkc1dpdGhvdXREb2N1bWVudCIsInJlcXVlc3RzIiwiZm9yRWFjaCIsInVybCIsInJlcXVlc3QiLCJlYWNoIiwiaW5kZXgiLCJlbWFpbExpc3RJdGVtIiwiJGVtYWlsTGlzdEl0ZW0iLCJwYXJzZUludCIsInZhbCIsInJlc3BvbnNlIiwiaW52b2ljZUlkIiwicHVzaCIsIndoZW4iLCJvcmRlcklkc1dpdGhEb2N1bWVudCIsImZpbHRlciIsImluY2x1ZGVzIiwiU3RyaW5nIiwib3JkZXJJZCIsIl9nZXRTdWJqZWN0RGF0YSIsIm1ldGhvZCIsImRhdGFUeXBlIiwiX3NlbmRJbnZvaWNlRW1haWxzIiwiJGVtYWlsTGlzdEl0ZW1zIiwibGVuZ3RoIiwic2hvd01lc3NhZ2UiLCJlbWFpbCIsInRoZW4iLCJwYXJhbWV0ZXJzIiwib0lEIiwibWFpbCIsImdtX3F1aWNrX21haWwiLCJwYXJhbSIsImdtX21haWwiLCJnbV9zdWJqZWN0IiwiY3JlYXRlX21pc3NpbmdfaW52b2ljZXMiLCJOdW1iZXIiLCJpbnZvaWNlX2lkcyIsIl9vblNlbmRDbGljayIsImNhdGNoIiwiaW5pdCIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7OztBQU1BQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FBc0Isb0JBQXRCLEVBQTRDLENBQUMsT0FBRCxFQUFVLGlCQUFWLENBQTVDLEVBQTBFLFVBQVNDLElBQVQsRUFBZTs7QUFFeEY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTO0FBQ2RJLFlBQVU7QUFDVEMsWUFBU0gsTUFBTUksSUFBTixDQUFXLFVBQVg7QUFEQTtBQURJLEVBQWY7O0FBTUE7Ozs7O0FBS0EsS0FBTUMsd0JBQXdCLGtCQUE5Qjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyw2QkFBNkIsY0FBbkM7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsMEJBQTBCLG9CQUFoQzs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxnQ0FBZ0MsMEJBQXRDOztBQUVBOzs7OztBQUtBLEtBQU1DLHVCQUF1QixnQkFBN0I7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsZ0JBQWdCQyxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHlCQUF0RDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxrQkFBa0JKLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0Msa0JBQXhEOztBQUVBOzs7OztBQUtBLEtBQUlFLFdBQVcsSUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0MsY0FBVCxDQUF3QkMsaUJBQXhCLEVBQTJDO0FBQzFDLE1BQUlBLGlCQUFKLEVBQXVCO0FBQ3RCRixjQUFXTCxJQUFJUSxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLElBQXpCLENBQThCckIsTUFBTUksSUFBTixDQUFXSyxvQkFBWCxDQUE5QixFQUFnRVQsTUFBTXNCLEdBQU4sQ0FBVSxTQUFWLENBQWhFLENBQVg7QUFDQSxHQUZELE1BRU87QUFDTlgsT0FBSVEsSUFBSixDQUFTQyxlQUFULENBQXlCRyxJQUF6QixDQUE4QlAsUUFBOUI7QUFDQTtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFVBQVNRLGlCQUFULENBQTJCQyxTQUEzQixFQUFzQ0MsV0FBdEMsRUFBbUQ7QUFDbEQsU0FBTzVCLE9BQU9JLFFBQVAsQ0FBZ0JDLE9BQWhCLENBQXdCVyxHQUF4QixHQUNMYSxPQURLLENBQ0csZUFESCxFQUNvQkQsWUFBWUUsYUFEaEMsRUFFTEQsT0FGSyxDQUVHLGNBRkgsRUFFbUJELFlBQVlFLGFBRi9CLEVBR0xELE9BSEssQ0FHRyxnQkFISCxFQUdxQkQsWUFBWUcsV0FIakMsRUFJTEYsT0FKSyxDQUlHLFFBSkgsRUFJYUQsWUFBWUcsV0FKekIsRUFLTEYsT0FMSyxDQUtHLFlBTEgsRUFLaUJGLFVBQVVLLEVBTDNCLENBQVA7QUFNQTs7QUFFRDs7O0FBR0EsVUFBU0Msc0JBQVQsR0FBa0M7QUFDakMsTUFBTUMsVUFBVXJCLElBQUlDLElBQUosQ0FBU3FCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixtQkFBeEIsRUFBNkMsZUFBN0MsQ0FBaEI7O0FBRUE7QUFDQXZCLE1BQUlRLElBQUosQ0FBU2dCLFFBQVQsQ0FBa0JDLGlCQUFsQixDQUFvQ0osT0FBcEM7O0FBRUEvQixJQUFFLHFCQUFGLEVBQXlCb0MsU0FBekIsR0FBcUNDLElBQXJDLENBQTBDQyxNQUExQyxDQUFpRCxJQUFqRCxFQUF1RCxLQUF2RDtBQUNBdEMsSUFBRSxxQkFBRixFQUF5QnVDLHNCQUF6QixDQUFnRCxRQUFoRDs7QUFFQTtBQUNBdkIsaUJBQWUsS0FBZjtBQUNBakIsUUFBTXlDLEtBQU4sQ0FBWSxNQUFaO0FBQ0E7O0FBRUQ7OztBQUdBLFVBQVNDLHNCQUFULEdBQWtDO0FBQ2pDLE1BQU1DLFFBQVFoQyxJQUFJQyxJQUFKLENBQVNxQixJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBakMsQ0FBZDtBQUNBLE1BQU1VLFVBQVVqQyxJQUFJQyxJQUFKLENBQVNxQixJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLGVBQS9DLENBQWhCOztBQUVBO0FBQ0F2QixNQUFJUSxJQUFKLENBQVNzQixLQUFULENBQWVULE9BQWYsQ0FBdUIsRUFBQ1csWUFBRCxFQUFRQyxnQkFBUixFQUF2Qjs7QUFFQTtBQUNBM0IsaUJBQWUsS0FBZjtBQUNBakIsUUFBTXlDLEtBQU4sQ0FBWSxNQUFaO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTSSwwQkFBVCxDQUFvQ0MsUUFBcEMsRUFBOEM7QUFDN0MsU0FBTyxJQUFJQyxPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3ZDLE9BQU1sRCxPQUFPO0FBQ1ptRCxRQUFJLDhDQURRO0FBRVpDLGVBQVd4QyxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFdBQXBCLENBRkM7QUFHWnNDLFVBQU0sU0FITTtBQUlaTjtBQUpZLElBQWI7O0FBT0E3QyxLQUFFb0QsT0FBRixDQUFVdEMsZUFBVixFQUEyQmhCLElBQTNCLEVBQ0V1RCxJQURGLENBQ087QUFBQSxXQUEyQk4sUUFBUSxFQUFDRixrQkFBRCxFQUFXUyxnREFBWCxFQUFSLENBQTNCO0FBQUEsSUFEUCxFQUVFQyxJQUZGLENBRU9QLE1BRlA7QUFHQSxHQVhNLENBQVA7QUFZQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNRLHVCQUFULENBQWlDQyxTQUFqQyxFQUE0QztBQUMzQztBQUNBLE1BQU1DLHdCQUF3QjNELE1BQU1JLElBQU4sQ0FBV0ksNkJBQVgsRUFBMENvRCxJQUExQyxDQUErQyxTQUEvQyxDQUE5Qjs7QUFFQSxNQUFJRCxxQkFBSixFQUEyQjtBQUMxQixVQUFPRSx3QkFBd0JILFVBQVVaLFFBQWxDLEVBQTRDWSxVQUFVSCx1QkFBdEQsQ0FBUDtBQUNBLEdBRkQsTUFFTztBQUNOLFVBQU9PLCtCQUErQkosVUFBVVosUUFBekMsRUFBbURZLFVBQVVILHVCQUE3RCxDQUFQO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxVQUFTTSx1QkFBVCxDQUFpQ2YsUUFBakMsRUFBMkNTLHVCQUEzQyxFQUFvRTtBQUNuRSxTQUFPLElBQUlSLE9BQUosQ0FBWSxtQkFBVztBQUFBOztBQUM3QixPQUFNZ0IsV0FBVyxFQUFqQjs7QUFFQVIsMkJBQXdCUyxPQUF4QixDQUFnQyxjQUFNO0FBQ3JDLFFBQU1DLE1BQU12RCwyQkFBd0JvQixFQUF4QiwwQkFBWjtBQUNBLFFBQU1vQyxVQUFVakUsRUFBRW9ELE9BQUYsQ0FBVVksR0FBVixDQUFoQjs7QUFFQUMsWUFBUVosSUFBUixDQUFhLG9CQUFZO0FBQ3hCckQsT0FBRUkscUJBQUYsRUFBeUI4RCxJQUF6QixDQUE4QixVQUFDQyxLQUFELEVBQVFDLGFBQVIsRUFBMEI7QUFDdkQsVUFBTUMsaUJBQWlCckUsRUFBRW9FLGFBQUYsQ0FBdkI7O0FBRUEsVUFBSUMsZUFBZXZFLElBQWYsQ0FBb0IsT0FBcEIsRUFBNkIrQixFQUE3QixLQUFvQ3lDLFNBQVN6QyxFQUFULENBQXhDLEVBQXNEO0FBQ3JEd0Msc0JBQWVsRSxJQUFmLENBQW9CRyx1QkFBcEIsRUFBNkNpRSxHQUE3QyxDQUFpREMsU0FBU0MsU0FBMUQ7QUFDQSxjQUFPLEtBQVA7QUFDQTtBQUNELE1BUEQ7QUFRQSxLQVREOztBQVdBWCxhQUFTWSxJQUFULENBQWNULE9BQWQ7QUFDQSxJQWhCRDs7QUFrQkEsVUFBTyxTQUFFVSxJQUFGLFdBQVViLFFBQVYsRUFBb0JULElBQXBCLENBQXlCO0FBQUEsV0FBTU4sUUFBUUYsUUFBUixDQUFOO0FBQUEsSUFBekIsQ0FBUDtBQUNBLEdBdEJNLENBQVA7QUF1QkE7O0FBRUQ7Ozs7Ozs7O0FBUUEsVUFBU2dCLDhCQUFULENBQXdDaEIsUUFBeEMsRUFBa0RTLHVCQUFsRCxFQUEyRTtBQUMxRSxTQUFPLElBQUlSLE9BQUosQ0FBWSxtQkFBVztBQUM3QixPQUFNOEIsdUJBQXVCL0IsU0FBU2dDLE1BQVQsQ0FBZ0I7QUFBQSxXQUFXLENBQUN2Qix3QkFBd0J3QixRQUF4QixDQUFpQ0MsT0FBT0MsT0FBUCxDQUFqQyxDQUFaO0FBQUEsSUFBaEIsQ0FBN0I7QUFDQWpDLFdBQVE2QixvQkFBUjtBQUNBLEdBSE0sQ0FBUDtBQUlBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0ssZUFBVCxDQUF5QlIsU0FBekIsRUFBb0M7QUFDbkMsU0FBT3pFLEVBQUVxQyxJQUFGLENBQU87QUFDYjJCLFFBQVFsRCxlQUFSLG9EQURhO0FBRWJvRSxXQUFRLE1BRks7QUFHYkMsYUFBVSxNQUhHO0FBSWJyRixTQUFNO0FBQ0wyRSx3QkFESztBQUVMdkIsZUFBV3hDLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFGTjtBQUpPLEdBQVAsQ0FBUDtBQVNBOztBQUVEOzs7OztBQUtBLFVBQVN1RSxrQkFBVCxDQUE0QnZDLFFBQTVCLEVBQXNDO0FBQ3JDLFNBQU8sSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUFBOztBQUN2QyxPQUFNVSx3QkFBd0IzRCxNQUFNSSxJQUFOLENBQVdJLDZCQUFYLEVBQTBDb0QsSUFBMUMsQ0FBK0MsU0FBL0MsQ0FBOUI7QUFDQSxPQUFNMEIsa0JBQWtCdEYsTUFBTUksSUFBTixDQUFXQyxxQkFBWCxDQUF4Qjs7QUFFQTtBQUNBLE9BQUksQ0FBQ2lGLGdCQUFnQkMsTUFBakIsSUFBMkIsQ0FBQ3pDLFNBQVN5QyxNQUF6QyxFQUFpRDtBQUNoRCxRQUFNNUMsUUFBUWhDLElBQUlDLElBQUosQ0FBU3FCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixlQUF4QixFQUF5QyxlQUF6QyxDQUFkO0FBQ0EsUUFBTUYsVUFBVXJCLElBQUlDLElBQUosQ0FBU3FCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQkFBeEIsRUFBNEMsUUFBNUMsQ0FBaEI7QUFDQXZCLFFBQUlRLElBQUosQ0FBU3NCLEtBQVQsQ0FBZStDLFdBQWYsQ0FBMkI3QyxLQUEzQixFQUFrQ1gsT0FBbEM7QUFDQWhDLFVBQU15QyxLQUFOLENBQVksTUFBWjtBQUNBO0FBQ0E7O0FBRUQ7QUFDQXhCLGtCQUFlLElBQWY7O0FBRUE7QUFDQSxPQUFNOEMsV0FBVyxFQUFqQjs7QUFFQTtBQUNBdUIsbUJBQWdCbkIsSUFBaEIsQ0FBcUIsVUFBQ0MsS0FBRCxFQUFRQyxhQUFSLEVBQTBCO0FBQzlDLFFBQU01QyxZQUFZeEIsRUFBRW9FLGFBQUYsRUFBaUJ0RSxJQUFqQixDQUFzQixPQUF0QixDQUFsQjs7QUFFQSxRQUFJLENBQUMrQyxTQUFTaUMsUUFBVCxDQUFrQnRELFVBQVVLLEVBQTVCLENBQUwsRUFBc0M7QUFDckMsWUFBTyxJQUFQLENBRHFDLENBQ3hCO0FBQ2I7O0FBRUQ7QUFDQSxRQUFNMkQsUUFBUXhGLEVBQUVvRSxhQUFGLEVBQWlCakUsSUFBakIsQ0FBc0JFLDBCQUF0QixFQUFrRGtFLEdBQWxELEVBQWQ7O0FBRUE7QUFDQSxRQUFNRSxZQUFZekUsRUFBRW9FLGFBQUYsRUFBaUJqRSxJQUFqQixDQUFzQkcsdUJBQXRCLEVBQStDaUUsR0FBL0MsRUFBbEI7O0FBRUFVLG9CQUFnQlIsU0FBaEIsRUFDRWdCLElBREYsQ0FDTyx1QkFBZTtBQUNwQjtBQUNBLFNBQU1DLGFBQWE7QUFDbEJDLFdBQUtuRSxVQUFVSyxFQURHO0FBRWxCc0IsWUFBTSxTQUZZO0FBR2xCeUMsWUFBTSxHQUhZO0FBSWxCQyxxQkFBZTtBQUpHLE1BQW5COztBQU9BLFNBQU03QixNQUFNdkQsZ0JBQWdCLEdBQWhCLEdBQXNCVCxFQUFFOEYsS0FBRixDQUFRSixVQUFSLENBQWxDO0FBQ0EsU0FBTTVGLE9BQU87QUFDWmlHLGVBQVNQLEtBREc7QUFFWlEsa0JBQVl6RSxrQkFBa0JDLFNBQWxCLEVBQTZCQyxXQUE3QixDQUZBO0FBR1p3RSwrQkFBeUJDLE9BQU94QyxxQkFBUCxDQUhiLENBRzJDO0FBSDNDLE1BQWI7O0FBTUEsU0FBSWUsY0FBYyxHQUFsQixFQUF1QjtBQUN0QjNFLFdBQUtxRyxXQUFMLEdBQW1CLENBQUMxQixTQUFELENBQW5CO0FBQ0E7O0FBRUQ7QUFDQVgsY0FBU1ksSUFBVCxDQUFjMUUsRUFBRXFDLElBQUYsQ0FBTyxFQUFDNkMsUUFBUSxNQUFULEVBQWlCbEIsUUFBakIsRUFBc0JsRSxVQUF0QixFQUFQLENBQWQ7QUFDQSxLQXZCRjtBQXdCQSxJQXJDRDs7QUF1Q0EsYUFBRTZFLElBQUYsWUFBVWIsUUFBVixFQUNFVCxJQURGLENBQ09OLE9BRFAsRUFFRVEsSUFGRixDQUVPUCxNQUZQO0FBR0EsR0E5RE0sQ0FBUDtBQStEQTs7QUFFRDs7Ozs7OztBQU9BLFVBQVNvRCxZQUFULEdBQXdCO0FBQ3ZCLE1BQU12RCxXQUFXLEVBQWpCOztBQUVBOUMsUUFBTUksSUFBTixDQUFXQyxxQkFBWCxFQUFrQzhELElBQWxDLENBQXVDLFVBQUNDLEtBQUQsRUFBUUMsYUFBUixFQUEwQjtBQUNoRXZCLFlBQVM2QixJQUFULENBQWMxRSxFQUFFb0UsYUFBRixFQUFpQnRFLElBQWpCLENBQXNCLE9BQXRCLEVBQStCK0IsRUFBN0M7QUFDQSxHQUZEOztBQUlBZSw2QkFBMkJDLFFBQTNCLEVBQ0U0QyxJQURGLENBQ09qQyx1QkFEUCxFQUVFaUMsSUFGRixDQUVPTCxrQkFGUCxFQUdFSyxJQUhGLENBR08zRCxzQkFIUCxFQUlFdUUsS0FKRixDQUlRNUQsc0JBSlI7QUFLQTs7QUFHRDtBQUNBO0FBQ0E7O0FBRUE1QyxRQUFPeUcsSUFBUCxHQUFjLFVBQVNqRCxJQUFULEVBQWU7QUFDNUJ0RCxRQUFNd0csRUFBTixDQUFTLE9BQVQsRUFBa0IsV0FBbEIsRUFBK0JILFlBQS9CO0FBQ0EvQztBQUNBLEVBSEQ7O0FBS0EsUUFBT3hELE1BQVA7QUFDQSxDQXZXRCIsImZpbGUiOiJvcmRlcnMvbW9kYWxzL2J1bGtfZW1haWxfaW52b2ljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gYnVsa19lbWFpbF9pbnZvaWNlLmpzIDIwMTctMTAtMThcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIEJ1bGsgRW1haWwgSW52b2ljZSBNb2RhbCBDb250cm9sbGVyXG4gKlxuICogVGhpcyBjb250cm9sbGVyIGhhbmRsZXMgdGhlIG9yZGVycyBidWxrIGludm9pY2UgZW1haWxzIG1vZGFsIHdoZXJlIHRoZSB1c2VyIGNhbiBzZW5kIGVtYWlscyBmb3IgdGhlXG4gKiBzZWxlY3RlZCBvcmRlcnMuIFRoZSBjb250cm9sbGVyIGlzIGFibGUgdG8gY3JlYXRlIG1pc3NpbmcgaW52b2ljZXMgaWYgbmVlZGVkLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoJ2J1bGtfZW1haWxfaW52b2ljZScsIFsnbW9kYWwnLCAnbG9hZGluZ19zcGlubmVyJ10sIGZ1bmN0aW9uKGRhdGEpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBWQVJJQUJMRVNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdCAqXG5cdCAqIEB0eXBlIHtqUXVlcnl9XG5cdCAqL1xuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFxuXHQvKipcblx0ICogTW9kdWxlIEluc3RhbmNlXG5cdCAqXG5cdCAqIEB0eXBlIHtPYmplY3R9XG5cdCAqL1xuXHRjb25zdCBtb2R1bGUgPSB7XG5cdFx0YmluZGluZ3M6IHtcblx0XHRcdHN1YmplY3Q6ICR0aGlzLmZpbmQoJy5zdWJqZWN0Jylcblx0XHR9XG5cdH07XG5cdFxuXHQvKipcblx0ICogU2VsZWN0b3IgZm9yIHRoZSBlbWFpbCBsaXN0IGl0ZW0uXG5cdCAqXG5cdCAqIEB0eXBlIHtTdHJpbmd9XG5cdCAqL1xuXHRjb25zdCBlbWFpbExpc3RJdGVtU2VsZWN0b3IgPSAnLmVtYWlsLWxpc3QtaXRlbSc7XG5cdFxuXHQvKipcblx0ICogU2VsZWN0b3IgZm9yIHRoZSBlbWFpbCBsaXN0IGl0ZW0gSUQuXG5cdCAqXG5cdCAqIEB0eXBlIHtTdHJpbmd9XG5cdCAqL1xuXHRjb25zdCBlbWFpbExpc3RJdGVtRW1haWxTZWxlY3RvciA9ICcuZW1haWwtaW5wdXQnO1xuXHRcblx0LyoqXG5cdCAqIFNlbGVjdG9yIGZvciB0aGUgbGF0ZXN0IGludm9pY2UgbnVtYmVyIG9mIHRoZSBsaXN0IGl0ZW1cblx0ICpcblx0ICogQHR5cGUge3N0cmluZ31cblx0ICovXG5cdGNvbnN0IGxhdGVzdEludm9pY2VJZFNlbGVjdG9yID0gJy5sYXRlc3QtaW52b2ljZS1pZCc7XG5cdFxuXHQvKipcblx0ICogU2VsZWN0b3IgZm9yIHRoZSBvcHRpb24gdGhhdCBpbmRpY2F0ZXMgaWYgbWlzc2luZyBpbnZvaWNlcyBzaG91bGQgYmUgY3JlYXRlZFxuXHQgKlxuXHQgKiBAdHlwZSB7U3RyaW5nfVxuXHQgKi9cblx0Y29uc3QgY3JlYXRlTWlzc2luZ0ludm9pY2VzU2VsZWN0b3IgPSAnLmNyZWF0ZS1taXNzaW5nLWludm9pY2VzJztcblx0XG5cdC8qKlxuXHQgKiBTZWxlY3RvciBmb3IgdGhlIG1vZGFsIGNvbnRlbnQgYm9keSBsYXllci5cblx0ICpcblx0ICogQHR5cGUge1N0cmluZ31cblx0ICovXG5cdGNvbnN0IG1vZGFsQ29udGVudFNlbGVjdG9yID0gJy5tb2RhbC1jb250ZW50Jztcblx0XG5cdC8qKlxuXHQgKiBHTSBQREYgT3JkZXIgVVJMXG5cdCAqXG5cdCAqIEB0eXBlIHtTdHJpbmd9XG5cdCAqL1xuXHRjb25zdCBnbVBkZk9yZGVyVXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2dtX3BkZl9vcmRlci5waHAnO1xuXHRcblx0LyoqXG5cdCAqIEFkbWluIFJlcXVlc3QgVVJMXG5cdCAqXG5cdCAqIEB0eXBlIHtTdHJpbmd9XG5cdCAqL1xuXHRjb25zdCBhZG1pblJlcXVlc3RVcmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwJztcblx0XG5cdC8qKlxuXHQgKiBMb2FkaW5nIFNwaW5uZXIgU2VsZWN0b3Jcblx0ICpcblx0ICogQHR5cGUge2pRdWVyeXxudWxsfVxuXHQgKi9cblx0bGV0ICRzcGlubmVyID0gbnVsbDtcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBGVU5DVElPTlNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogU2hvdy9oaWRlIGxvYWRpbmcgc3Bpbm5lci5cblx0ICpcblx0ICogQHBhcmFtIHtCb29sZWFufSBzcGlubmVyVmlzaWJpbGl0eSBXaGV0aGVyIHRvIHNob3cgb3IgaGlkZSB0aGUgc3Bpbm5lci5cblx0ICovXG5cdGZ1bmN0aW9uIF90b2dnbGVTcGlubmVyKHNwaW5uZXJWaXNpYmlsaXR5KSB7XG5cdFx0aWYgKHNwaW5uZXJWaXNpYmlsaXR5KSB7XG5cdFx0XHQkc3Bpbm5lciA9IGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5zaG93KCR0aGlzLmZpbmQobW9kYWxDb250ZW50U2VsZWN0b3IpLCAkdGhpcy5jc3MoJ3otaW5kZXgnKSk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5oaWRlKCRzcGlubmVyKTtcblx0XHR9XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBQYXJzZSBzdWJqZWN0IGFuZCByZXBsYWNlIHRoZSBwbGFjZWhvbGRlcnMgd2l0aCB0aGUgdmFyaWFibGVzLlxuXHQgKlxuXHQgKiBAcGFyYW0ge09iamVjdH0gb3JkZXJEYXRhIENvbnRhaW5zIHRoZSBvcmRlcidzIGRhdGEuXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBzdWJqZWN0RGF0YSBDb250YWlucyB0aGUgcmVxdWlyZWQgc3ViamVjdCdzIGRhdGEuXG5cdCAqXG5cdCAqIEByZXR1cm4ge1N0cmluZ30gUmV0dXJucyB0aGUgZmluYWwgZW1haWwgc3ViamVjdCBzdHJpbmcuXG5cdCAqL1xuXHRmdW5jdGlvbiBfZ2V0UGFyc2VkU3ViamVjdChvcmRlckRhdGEsIHN1YmplY3REYXRhKSB7XG5cdFx0cmV0dXJuIG1vZHVsZS5iaW5kaW5ncy5zdWJqZWN0LmdldCgpXG5cdFx0XHQucmVwbGFjZSgne0lOVk9JQ0VfTlVNfScsIHN1YmplY3REYXRhLmludm9pY2VOdW1iZXIpXG5cdFx0XHQucmVwbGFjZSgne0lOVk9JQ0VfSUR9Jywgc3ViamVjdERhdGEuaW52b2ljZU51bWJlcilcblx0XHRcdC5yZXBsYWNlKCd7SU5WT0lDRV9EQVRFfScsIHN1YmplY3REYXRhLmludm9pY2VEYXRlKVxuXHRcdFx0LnJlcGxhY2UoJ3tEQVRFfScsIHN1YmplY3REYXRhLmludm9pY2VEYXRlKVxuXHRcdFx0LnJlcGxhY2UoJ3tPUkRFUl9JRH0nLCBvcmRlckRhdGEuaWQpO1xuXHR9XG5cdFxuXHQvKipcblx0ICogSGFuZGxlcyB0aGUgc3VjY2Vzc2Z1bCBkZWxpdmVyeSBvZiBhbGwgbWVzc2FnZXMuXG5cdCAqL1xuXHRmdW5jdGlvbiBfaGFuZGxlRGVsaXZlcnlTdWNjZXNzKCkge1xuXHRcdGNvbnN0IG1lc3NhZ2UgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVMS19NQUlMX1NVQ0NFU1MnLCAnZ21fc2VuZF9vcmRlcicpO1xuXHRcdFxuXHRcdC8vIFNob3cgc3VjY2VzcyBtZXNzYWdlIGluIHRoZSBhZG1pbiBpbmZvIGJveC5cblx0XHRqc2UubGlicy5pbmZvX2JveC5hZGRTdWNjZXNzTWVzc2FnZShtZXNzYWdlKTtcblx0XHRcblx0XHQkKCcub3JkZXJzIC50YWJsZS1tYWluJykuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQobnVsbCwgZmFsc2UpO1xuXHRcdCQoJy5vcmRlcnMgLnRhYmxlLW1haW4nKS5vcmRlcnNfb3ZlcnZpZXdfZmlsdGVyKCdyZWxvYWQnKTtcblx0XHRcblx0XHQvLyBIaWRlIG1vZGFsIGFuZCBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0X3RvZ2dsZVNwaW5uZXIoZmFsc2UpO1xuXHRcdCR0aGlzLm1vZGFsKCdoaWRlJyk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBIYW5kbGVzIHRoZSBmYWlsdXJlIG9mIHRoZSBtZXNzYWdlIGRlbGl2ZXJ5LlxuXHQgKi9cblx0ZnVuY3Rpb24gX2hhbmRsZURlbGl2ZXJ5RmFpbHVyZSgpIHtcblx0XHRjb25zdCB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdlcnJvcicsICdtZXNzYWdlcycpO1xuXHRcdGNvbnN0IGNvbnRlbnQgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVMS19NQUlMX1VOU1VDQ0VTUycsICdnbV9zZW5kX29yZGVyJyk7XG5cdFx0XG5cdFx0Ly8gU2hvdyBlcnJvciBtZXNzYWdlIGluIGEgbW9kYWwuXG5cdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZSh7dGl0bGUsIGNvbnRlbnR9KTtcblx0XHRcblx0XHQvLyBIaWRlIG1vZGFsIGFuZCB0aGUgbG9hZGluZyBzcGlubmVyIGFuZCByZS1lbmFibGUgdGhlIHNlbmQgYnV0dG9uLlxuXHRcdF90b2dnbGVTcGlubmVyKGZhbHNlKTtcblx0XHQkdGhpcy5tb2RhbCgnaGlkZScpO1xuXHR9XG5cdFxuXHQvKipcblx0ICogR2V0IHRoZSBJRHMgb2YgdGhlIG9yZGVycyB0aGF0IGRvIG5vdCBoYXZlIGFuIGludm9pY2UuXG5cdCAqXG5cdCAqIEBwYXJhbSB7TnVtYmVyW119IG9yZGVySWRzIFRoZSBvcmRlcnMgdG8gYmUgdmFsaWRhdGVkLlxuXHQgKlxuXHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHQgKi9cblx0ZnVuY3Rpb24gX2dldE9yZGVyc1dpdGhvdXREb2N1bWVudHMob3JkZXJJZHMpIHtcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0Y29uc3QgZGF0YSA9IHtcblx0XHRcdFx0ZG86ICdPcmRlcnNPdmVydmlld0FqYXgvR2V0T3JkZXJzV2l0aG91dERvY3VtZW50cycsXG5cdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJyksXG5cdFx0XHRcdHR5cGU6ICdpbnZvaWNlJyxcblx0XHRcdFx0b3JkZXJJZHNcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdCQuZ2V0SlNPTihhZG1pblJlcXVlc3RVcmwsIGRhdGEpXG5cdFx0XHRcdC5kb25lKG9yZGVySWRzV2l0aG91dERvY3VtZW50ID0+IHJlc29sdmUoe29yZGVySWRzLCBvcmRlcklkc1dpdGhvdXREb2N1bWVudH0pKVxuXHRcdFx0XHQuZmFpbChyZWplY3QpO1xuXHRcdH0pO1xuXHR9XG5cdFxuXHQvKipcblx0ICogVmFsaWRhdGUgc2VsZWN0ZWQgb3JkZXJzIGFuZCBnZW5lcmF0ZS9yZW1vdmUgdGhlIG9yZGVycyB3aXRob3V0IGRvY3VtZW50cy5cblx0ICpcblx0ICogQHBhcmFtIHtPYmplY3R9IHNlbGVjdGlvbiBDb250YWlucyB0aGUgXCJvcmRlcklkc1wiIGFuZCBcIm9yZGVySWRzV2l0aG91dERvY3VtZW50XCIgcHJvcGVydGllcy5cblx0ICpcblx0ICogQHJldHVybiB7UHJvbWlzZX0gUmV0dXJucyB0aGUgcHJvbWlzZXMgb2YgdGhlIGRlbGVnYXRlZCBtZXRob2RzLlxuXHQgKi9cblx0ZnVuY3Rpb24gX2hhbmRsZU1pc3NpbmdEb2N1bWVudHMoc2VsZWN0aW9uKSB7XG5cdFx0Ly8gSW5kaWNhdGVzIGlmIG1pc3NpbmcgaW52b2ljZXMgc2hvdWxkIGJlIGNyZWF0ZWRcblx0XHRjb25zdCBjcmVhdGVNaXNzaW5nSW52b2ljZXMgPSAkdGhpcy5maW5kKGNyZWF0ZU1pc3NpbmdJbnZvaWNlc1NlbGVjdG9yKS5wcm9wKCdjaGVja2VkJyk7XG5cdFx0XG5cdFx0aWYgKGNyZWF0ZU1pc3NpbmdJbnZvaWNlcykge1xuXHRcdFx0cmV0dXJuIF9jcmVhdGVNaXNzaW5nRG9jdW1lbnRzKHNlbGVjdGlvbi5vcmRlcklkcywgc2VsZWN0aW9uLm9yZGVySWRzV2l0aG91dERvY3VtZW50KVxuXHRcdH0gZWxzZSB7XG5cdFx0XHRyZXR1cm4gX3JlbW92ZU9yZGVySWRzV2l0aG91dERvY3VtZW50KHNlbGVjdGlvbi5vcmRlcklkcywgc2VsZWN0aW9uLm9yZGVySWRzV2l0aG91dERvY3VtZW50KVxuXHRcdH1cblx0fVxuXHRcblx0LyoqXG5cdCAqIENyZWF0ZSBNaXNzaW5nIE9yZGVyIERvY3VtZW50cyBhbmQgc2V0IHRoZSBuZXcgbGF0ZXN0IGludm9pY2UgaWQgc2VsZWN0b3IgZm9yIHdoaWNoIG5vIGludm9pY2UgaGFzIHlldCBleGlzdGVkLlxuXHQgKlxuXHQgKiBAcGFyYW0ge051bWJlcltdfSBvcmRlcklkcyBTZWxlY3RlZCBvcmRlciBJRHMuXG5cdCAqIEBwYXJhbSB7TnVtYmVyW119IG9yZGVySWRzV2l0aG91dERvY3VtZW50IE9yZGVyIElEcyB0aGF0IGRvIG5vdCBoYXZlIGEgZG9jdW1lbnQuXG5cdCAqXG5cdCAqIEByZXR1cm4ge1Byb21pc2V9IFJldHVybnMgYSBwcm9taXNlIHRoYXQgd2lsbCBiZSByZXNvbHZlZCB3aXRoIGFsbCB0aGUgb3JkZXIgSURzLlxuXHQgKi9cblx0ZnVuY3Rpb24gX2NyZWF0ZU1pc3NpbmdEb2N1bWVudHMob3JkZXJJZHMsIG9yZGVySWRzV2l0aG91dERvY3VtZW50KSB7XG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlKHJlc29sdmUgPT4ge1xuXHRcdFx0Y29uc3QgcmVxdWVzdHMgPSBbXTtcblx0XHRcdFxuXHRcdFx0b3JkZXJJZHNXaXRob3V0RG9jdW1lbnQuZm9yRWFjaChpZCA9PiB7XG5cdFx0XHRcdGNvbnN0IHVybCA9IGdtUGRmT3JkZXJVcmwgKyBgP29JRD0ke2lkfSZ0eXBlPWludm9pY2UmYWpheD0xYDtcblx0XHRcdFx0Y29uc3QgcmVxdWVzdCA9ICQuZ2V0SlNPTih1cmwpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmVxdWVzdC5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHQkKGVtYWlsTGlzdEl0ZW1TZWxlY3RvcikuZWFjaCgoaW5kZXgsIGVtYWlsTGlzdEl0ZW0pID0+IHtcblx0XHRcdFx0XHRcdGNvbnN0ICRlbWFpbExpc3RJdGVtID0gJChlbWFpbExpc3RJdGVtKTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0aWYgKCRlbWFpbExpc3RJdGVtLmRhdGEoJ29yZGVyJykuaWQgPT09IHBhcnNlSW50KGlkKSkge1xuXHRcdFx0XHRcdFx0XHQkZW1haWxMaXN0SXRlbS5maW5kKGxhdGVzdEludm9pY2VJZFNlbGVjdG9yKS52YWwocmVzcG9uc2UuaW52b2ljZUlkKTtcblx0XHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdHJlcXVlc3RzLnB1c2gocmVxdWVzdCk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuICQud2hlbiguLi5yZXF1ZXN0cykuZG9uZSgoKSA9PiByZXNvbHZlKG9yZGVySWRzKSk7XG5cdFx0fSk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBSZW1vdmUgb3JkZXIgSURzIHRoYXQgZG8gbm90IGhhdmUgYSBkb2N1bWVudC5cblx0ICpcblx0ICogQHBhcmFtIHtOdW1iZXJbXX0gb3JkZXJJZHMgU2VsZWN0ZWQgb3JkZXIgSURzLlxuXHQgKiBAcGFyYW0ge051bWJlcltdfSBvcmRlcklkc1dpdGhvdXREb2N1bWVudCBPcmRlciBJRHMgdGhhdCBkbyBub3QgaGF2ZSBhIGRvY3VtZW50LlxuXHQgKlxuXHQgKiBAcmV0dXJuIHtQcm9taXNlfSBSZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHdpbGwgYmUgcmVzb2x2ZWQgd2l0aCB0aGUgb3JkZXJzIHRoYXQgZG8gaGF2ZSBhIGRvY3VtZW50LlxuXHQgKi9cblx0ZnVuY3Rpb24gX3JlbW92ZU9yZGVySWRzV2l0aG91dERvY3VtZW50KG9yZGVySWRzLCBvcmRlcklkc1dpdGhvdXREb2N1bWVudCkge1xuXHRcdHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcblx0XHRcdGNvbnN0IG9yZGVySWRzV2l0aERvY3VtZW50ID0gb3JkZXJJZHMuZmlsdGVyKG9yZGVySWQgPT4gIW9yZGVySWRzV2l0aG91dERvY3VtZW50LmluY2x1ZGVzKFN0cmluZyhvcmRlcklkKSkpO1xuXHRcdFx0cmVzb2x2ZShvcmRlcklkc1dpdGhEb2N1bWVudCk7XG5cdFx0fSk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBHZXQgdGhlIHJlcXVpcmVkIGRhdGEgZm9yIHRoZSBlbWFpbCBzdWJqZWN0IG9mIHRoZSBwcm92aWRlZCBpbnZvaWNlLlxuXHQgKlxuXHQgKiBAcGFyYW0ge051bWJlcn0gaW52b2ljZUlkIEdldCB0aGUgc3ViamVjdCBpbmZvcm1hdGlvbiBmb3IgdGhlIHNlbGVjdGVkIGludm9pY2UuXG5cdCAqXG5cdCAqIEByZXR1cm4ge2pRdWVyeS5qcVhIUn0gVGhlIHJlcXVlc3Qgd2lsbCBiZSByZXNvbHZlZCB3aXRoIHRoZSBzdWJqZWN0IGRhdGEuXG5cdCAqL1xuXHRmdW5jdGlvbiBfZ2V0U3ViamVjdERhdGEoaW52b2ljZUlkKSB7XG5cdFx0cmV0dXJuICQuYWpheCh7XG5cdFx0XHR1cmw6IGAke2FkbWluUmVxdWVzdFVybH0/ZG89T3JkZXJzTW9kYWxzQWpheC9HZXRFbWFpbEludm9pY2VTdWJqZWN0RGF0YWAsXG5cdFx0XHRtZXRob2Q6ICdQT1NUJyxcblx0XHRcdGRhdGFUeXBlOiAnanNvbicsXG5cdFx0XHRkYXRhOiB7XG5cdFx0XHRcdGludm9pY2VJZCxcblx0XHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG5cdFxuXHQvKipcblx0ICogU2VuZCBJbnZvaWNlIEVtYWlsc1xuXHQgKlxuXHQgKiBAcGFyYW0ge051bWJlcltdfSBvcmRlcklkcyBDb250YWlucyB0aGUgSURzIG9mIHRoZSBvcmRlcnMgdG8gYmUgZmluYWxseSBzZW50IHdpdGggYW4gZW1haWwuXG5cdCAqL1xuXHRmdW5jdGlvbiBfc2VuZEludm9pY2VFbWFpbHMob3JkZXJJZHMpIHtcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0Y29uc3QgY3JlYXRlTWlzc2luZ0ludm9pY2VzID0gJHRoaXMuZmluZChjcmVhdGVNaXNzaW5nSW52b2ljZXNTZWxlY3RvcikucHJvcCgnY2hlY2tlZCcpO1xuXHRcdFx0Y29uc3QgJGVtYWlsTGlzdEl0ZW1zID0gJHRoaXMuZmluZChlbWFpbExpc3RJdGVtU2VsZWN0b3IpO1xuXHRcdFx0XG5cdFx0XHQvLyBBYm9ydCBhbmQgaGlkZSBtb2RhbCBvbiBlbXB0eSBlbWFpbCBsaXN0IGVudHJpZXMuXG5cdFx0XHRpZiAoISRlbWFpbExpc3RJdGVtcy5sZW5ndGggfHwgIW9yZGVySWRzLmxlbmd0aCkge1xuXHRcdFx0XHRjb25zdCB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUSVRMRV9JTlZPSUNFJywgJ2dtX29yZGVyX21lbnUnKTtcblx0XHRcdFx0Y29uc3QgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdOT19SRUNPUkRTX0VSUk9SJywgJ29yZGVycycpO1xuXHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZSh0aXRsZSwgbWVzc2FnZSk7XG5cdFx0XHRcdCR0aGlzLm1vZGFsKCdoaWRlJyk7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHRfdG9nZ2xlU3Bpbm5lcih0cnVlKTtcblx0XHRcdFxuXHRcdFx0Ly8gQ29sbGVjdGlvbiBvZiByZXF1ZXN0cyBpbiBwcm9taXNlIGZvcm1hdC5cblx0XHRcdGNvbnN0IHJlcXVlc3RzID0gW107XG5cdFx0XHRcblx0XHRcdC8vIEZpbGwgb3JkZXJzIGFycmF5IHdpdGggZGF0YS5cblx0XHRcdCRlbWFpbExpc3RJdGVtcy5lYWNoKChpbmRleCwgZW1haWxMaXN0SXRlbSkgPT4ge1xuXHRcdFx0XHRjb25zdCBvcmRlckRhdGEgPSAkKGVtYWlsTGlzdEl0ZW0pLmRhdGEoJ29yZGVyJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoIW9yZGVySWRzLmluY2x1ZGVzKG9yZGVyRGF0YS5pZCkpIHtcblx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTsgLy8gQ3VycmVudCBvcmRlciBkb2VzIG5vdCBoYXZlIGFuIGludm9pY2UgZG9jdW1lbnQuXG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdC8vIEVtYWlsIGFkZHJlc3MgZW50ZXJlZCBpbiBpbnB1dCBmaWVsZC5cblx0XHRcdFx0Y29uc3QgZW1haWwgPSAkKGVtYWlsTGlzdEl0ZW0pLmZpbmQoZW1haWxMaXN0SXRlbUVtYWlsU2VsZWN0b3IpLnZhbCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gVGhlIGxhdGVzdCBpbnZvaWNlIGlkIG9mIHRoZSBvcmRlclxuXHRcdFx0XHRjb25zdCBpbnZvaWNlSWQgPSAkKGVtYWlsTGlzdEl0ZW0pLmZpbmQobGF0ZXN0SW52b2ljZUlkU2VsZWN0b3IpLnZhbCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0X2dldFN1YmplY3REYXRhKGludm9pY2VJZClcblx0XHRcdFx0XHQudGhlbihzdWJqZWN0RGF0YSA9PiB7XG5cdFx0XHRcdFx0XHQvLyBSZXF1ZXN0IEdFVCBwYXJhbWV0ZXJzIHRvIHNlbmQuXG5cdFx0XHRcdFx0XHRjb25zdCBwYXJhbWV0ZXJzID0ge1xuXHRcdFx0XHRcdFx0XHRvSUQ6IG9yZGVyRGF0YS5pZCxcblx0XHRcdFx0XHRcdFx0dHlwZTogJ2ludm9pY2UnLFxuXHRcdFx0XHRcdFx0XHRtYWlsOiAnMScsXG5cdFx0XHRcdFx0XHRcdGdtX3F1aWNrX21haWw6ICcxJ1xuXHRcdFx0XHRcdFx0fTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0Y29uc3QgdXJsID0gZ21QZGZPcmRlclVybCArICc/JyArICQucGFyYW0ocGFyYW1ldGVycyk7XG5cdFx0XHRcdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHRcdFx0XHRnbV9tYWlsOiBlbWFpbCxcblx0XHRcdFx0XHRcdFx0Z21fc3ViamVjdDogX2dldFBhcnNlZFN1YmplY3Qob3JkZXJEYXRhLCBzdWJqZWN0RGF0YSksXG5cdFx0XHRcdFx0XHRcdGNyZWF0ZV9taXNzaW5nX2ludm9pY2VzOiBOdW1iZXIoY3JlYXRlTWlzc2luZ0ludm9pY2VzKSAvLyAxIG9yIDBcblx0XHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGlmIChpbnZvaWNlSWQgIT09ICcwJykge1xuXHRcdFx0XHRcdFx0XHRkYXRhLmludm9pY2VfaWRzID0gW2ludm9pY2VJZF07XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdC8vIENyZWF0ZSBBSkFYIHJlcXVlc3QuXG5cdFx0XHRcdFx0XHRyZXF1ZXN0cy5wdXNoKCQuYWpheCh7bWV0aG9kOiAnUE9TVCcsIHVybCwgZGF0YX0pKTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkLndoZW4oLi4ucmVxdWVzdHMpXG5cdFx0XHRcdC5kb25lKHJlc29sdmUpXG5cdFx0XHRcdC5mYWlsKHJlamVjdCk7XG5cdFx0fSk7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBTZW5kIHRoZSBpbnZvaWNlIGVtYWlscy5cblx0ICpcblx0ICogVGhpcyBtZXRob2Qgd2lsbCBvbmx5IHNlbmQgZW1haWxzIGZvciB0aGUgb3JkZXJzIHRoYXQgZG8gaGF2ZSBhbiBpbnZvaWNlIGRvY3VtZW50LiBJZiB0aGVcblx0ICogXCJjcmVhdGUtbWlzc2luZy1pbnZvaWNlc1wiIGNoZWNrYm94IGlzIGFjdGl2ZSwgbmV3IGludm9pY2VzIHdpbGwgYmUgZ2VuZXJhdGVkIGZvciB0aGUgb3JkZXJzIHRoYXRcblx0ICogYXJlIGRvIG5vdCBoYXZlIG9uZS5cblx0ICovXG5cdGZ1bmN0aW9uIF9vblNlbmRDbGljaygpIHtcblx0XHRjb25zdCBvcmRlcklkcyA9IFtdO1xuXHRcdFxuXHRcdCR0aGlzLmZpbmQoZW1haWxMaXN0SXRlbVNlbGVjdG9yKS5lYWNoKChpbmRleCwgZW1haWxMaXN0SXRlbSkgPT4ge1xuXHRcdFx0b3JkZXJJZHMucHVzaCgkKGVtYWlsTGlzdEl0ZW0pLmRhdGEoJ29yZGVyJykuaWQpO1xuXHRcdH0pO1xuXHRcdFxuXHRcdF9nZXRPcmRlcnNXaXRob3V0RG9jdW1lbnRzKG9yZGVySWRzKVxuXHRcdFx0LnRoZW4oX2hhbmRsZU1pc3NpbmdEb2N1bWVudHMpXG5cdFx0XHQudGhlbihfc2VuZEludm9pY2VFbWFpbHMpXG5cdFx0XHQudGhlbihfaGFuZGxlRGVsaXZlcnlTdWNjZXNzKVxuXHRcdFx0LmNhdGNoKF9oYW5kbGVEZWxpdmVyeUZhaWx1cmUpO1xuXHR9XG5cdFxuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIElOSVRJQUxJWkFUSU9OXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0JHRoaXMub24oJ2NsaWNrJywgJy5idG4uc2VuZCcsIF9vblNlbmRDbGljayk7XG5cdFx0ZG9uZSgpO1xuXHR9O1xuXHRcblx0cmV0dXJuIG1vZHVsZTtcbn0pOyJdfQ==
