'use strict';

/* --------------------------------------------------------------
 settings.js 2016-10-04
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Invoices Overview Settings 
 *
 * It retrieves the settings data via the user configuration service and sets the values.
 * You are able to change the column sort order and the visibility of each column. Additionally
 * you can change the height of the table rows.
 */
gx.controllers.module('settings', ['user_configuration_service', 'loading_spinner', gx.source + '/libs/overview_settings_modal_controller'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		var SettingsModalController = jse.libs.overview_settings_modal_controller.class;

		var dependencies = {
			element: $this,
			userCfgService: jse.libs.user_configuration_service,
			loadingSpinner: jse.libs.loading_spinner,
			userId: data.userId,
			defaultColumnSettings: data.defaultColumnSettings,
			translator: jse.core.lang,
			page: 'invoices'
		};

		var settingsModal = new SettingsModalController(dependencies.element, dependencies.userCfgService, dependencies.loadingSpinner, dependencies.userId, dependencies.defaultColumnSettings, dependencies.translator, dependencies.page);

		settingsModal.initialize();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzL21vZGFscy9zZXR0aW5ncy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImluaXQiLCJkb25lIiwiU2V0dGluZ3NNb2RhbENvbnRyb2xsZXIiLCJqc2UiLCJsaWJzIiwib3ZlcnZpZXdfc2V0dGluZ3NfbW9kYWxfY29udHJvbGxlciIsImNsYXNzIiwiZGVwZW5kZW5jaWVzIiwiZWxlbWVudCIsInVzZXJDZmdTZXJ2aWNlIiwidXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UiLCJsb2FkaW5nU3Bpbm5lciIsImxvYWRpbmdfc3Bpbm5lciIsInVzZXJJZCIsImRlZmF1bHRDb2x1bW5TZXR0aW5ncyIsInRyYW5zbGF0b3IiLCJjb3JlIiwibGFuZyIsInBhZ2UiLCJzZXR0aW5nc01vZGFsIiwiaW5pdGlhbGl6ZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLFdBQUgsQ0FBZUMsTUFBZixDQUNDLFVBREQsRUFHQyxDQUNDLDRCQURELEVBRUMsaUJBRkQsRUFHSUYsR0FBR0csTUFIUCw4Q0FIRCxFQVNDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1KLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUFBLFFBQU9LLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUIsTUFBTUMsMEJBQTBCQyxJQUFJQyxJQUFKLENBQVNDLGtDQUFULENBQTRDQyxLQUE1RTs7QUFFQSxNQUFNQyxlQUFlO0FBQ3BCQyxZQUFTVixLQURXO0FBRXBCVyxtQkFBZ0JOLElBQUlDLElBQUosQ0FBU00sMEJBRkw7QUFHcEJDLG1CQUFnQlIsSUFBSUMsSUFBSixDQUFTUSxlQUhMO0FBSXBCQyxXQUFRaEIsS0FBS2dCLE1BSk87QUFLcEJDLDBCQUF1QmpCLEtBQUtpQixxQkFMUjtBQU1wQkMsZUFBWVosSUFBSWEsSUFBSixDQUFTQyxJQU5EO0FBT3BCQyxTQUFNO0FBUGMsR0FBckI7O0FBVUEsTUFBTUMsZ0JBQWdCLElBQUlqQix1QkFBSixDQUNyQkssYUFBYUMsT0FEUSxFQUVyQkQsYUFBYUUsY0FGUSxFQUdyQkYsYUFBYUksY0FIUSxFQUlyQkosYUFBYU0sTUFKUSxFQUtyQk4sYUFBYU8scUJBTFEsRUFNckJQLGFBQWFRLFVBTlEsRUFPckJSLGFBQWFXLElBUFEsQ0FBdEI7O0FBVUFDLGdCQUFjQyxVQUFkOztBQUVBbkI7QUFDQSxFQTFCRDs7QUE0QkEsUUFBT04sTUFBUDtBQUNBLENBaEVGIiwiZmlsZSI6Imludm9pY2VzL21vZGFscy9zZXR0aW5ncy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBzZXR0aW5ncy5qcyAyMDE2LTEwLTA0XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIEludm9pY2VzIE92ZXJ2aWV3IFNldHRpbmdzIFxyXG4gKlxyXG4gKiBJdCByZXRyaWV2ZXMgdGhlIHNldHRpbmdzIGRhdGEgdmlhIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZSBhbmQgc2V0cyB0aGUgdmFsdWVzLlxyXG4gKiBZb3UgYXJlIGFibGUgdG8gY2hhbmdlIHRoZSBjb2x1bW4gc29ydCBvcmRlciBhbmQgdGhlIHZpc2liaWxpdHkgb2YgZWFjaCBjb2x1bW4uIEFkZGl0aW9uYWxseVxyXG4gKiB5b3UgY2FuIGNoYW5nZSB0aGUgaGVpZ2h0IG9mIHRoZSB0YWJsZSByb3dzLlxyXG4gKi9cclxuZ3guY29udHJvbGxlcnMubW9kdWxlKFxyXG5cdCdzZXR0aW5ncycsXHJcblx0XHJcblx0W1xyXG5cdFx0J3VzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlJyxcclxuXHRcdCdsb2FkaW5nX3NwaW5uZXInLFxyXG5cdFx0YCR7Z3guc291cmNlfS9saWJzL292ZXJ2aWV3X3NldHRpbmdzX21vZGFsX2NvbnRyb2xsZXJgXHJcblx0XSxcclxuXHRcclxuXHRmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcclxuXHRcdCd1c2Ugc3RyaWN0JztcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIFZBUklBQkxFU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge09iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XHJcblx0XHRcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdGNvbnN0IFNldHRpbmdzTW9kYWxDb250cm9sbGVyID0ganNlLmxpYnMub3ZlcnZpZXdfc2V0dGluZ3NfbW9kYWxfY29udHJvbGxlci5jbGFzcztcclxuXHRcdFx0XHJcblx0XHRcdGNvbnN0IGRlcGVuZGVuY2llcyA9IHtcclxuXHRcdFx0XHRlbGVtZW50OiAkdGhpcyxcclxuXHRcdFx0XHR1c2VyQ2ZnU2VydmljZToganNlLmxpYnMudXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UsXHJcblx0XHRcdFx0bG9hZGluZ1NwaW5uZXI6IGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lcixcclxuXHRcdFx0XHR1c2VySWQ6IGRhdGEudXNlcklkLFxyXG5cdFx0XHRcdGRlZmF1bHRDb2x1bW5TZXR0aW5nczogZGF0YS5kZWZhdWx0Q29sdW1uU2V0dGluZ3MsXHJcblx0XHRcdFx0dHJhbnNsYXRvcjoganNlLmNvcmUubGFuZyxcclxuXHRcdFx0XHRwYWdlOiAnaW52b2ljZXMnXHJcblx0XHRcdH07XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCBzZXR0aW5nc01vZGFsID0gbmV3IFNldHRpbmdzTW9kYWxDb250cm9sbGVyKFxyXG5cdFx0XHRcdGRlcGVuZGVuY2llcy5lbGVtZW50LFxyXG5cdFx0XHRcdGRlcGVuZGVuY2llcy51c2VyQ2ZnU2VydmljZSxcclxuXHRcdFx0XHRkZXBlbmRlbmNpZXMubG9hZGluZ1NwaW5uZXIsXHJcblx0XHRcdFx0ZGVwZW5kZW5jaWVzLnVzZXJJZCxcclxuXHRcdFx0XHRkZXBlbmRlbmNpZXMuZGVmYXVsdENvbHVtblNldHRpbmdzLFxyXG5cdFx0XHRcdGRlcGVuZGVuY2llcy50cmFuc2xhdG9yLFxyXG5cdFx0XHRcdGRlcGVuZGVuY2llcy5wYWdlXHJcblx0XHRcdCk7XHJcblx0XHRcdFxyXG5cdFx0XHRzZXR0aW5nc01vZGFsLmluaXRpYWxpemUoKTtcclxuXHRcdFx0XHJcblx0XHRcdGRvbmUoKTtcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdHJldHVybiBtb2R1bGU7XHJcblx0fSk7XHJcbiJdfQ==
