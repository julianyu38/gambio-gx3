'use strict';

/* --------------------------------------------------------------
 init.js 2017-03-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * QuickEdit Table Controller
 *
 * This controller initializes the main QuickEdit table with a new jQuery DataTables instance.
 */
gx.controllers.module('init', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', 'datatable', jse.source + '/vendor/jquery-deparam/jquery-deparam.min.js', gx.source + '/libs/quick_edit_overview_columns'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Special Price Modal
  *
  * @type {jQuery}
  */
	var $specialPriceModal = $this.parents('.quick-edit.overview').find('.special-prices.modal');

	/**
  * Search Parameters
  *
  * @type {Object}
  */
	var parameters = $.deparam(window.location.search.slice(1));

	/**
  * DataTable Columns
  *
  * @type {Object[]}
  */
	var columns = jse.libs.datatable.prepareColumns($this, jse.libs.quick_edit_overview_columns, data.activeColumns);

	/**
  * DataTable Options
  *
  * @type {Object}
  */
	var options = {
		autoWidth: false,
		dom: 't',
		pageLength: parseInt(parameters.length || data.pageLength),
		displayStart: parseInt(parameters.page) ? (parseInt(parameters.page) - 1) * 25 : 0,
		serverSide: true,
		language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
		ajax: {
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/DataTable',
			type: 'POST',
			data: {
				pageToken: jse.core.config.get('pageToken')
			}
		},
		orderCellsTop: true,
		order: _getOrder(parameters, columns),
		searchCols: _getSearchCols(parameters, columns),
		columns: columns
	};

	var dataTable = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Initial Table Order
  *
  * @param {Object} parameters Contains the URL parameters.
  * @param {Object} columns Contains the column definitions.
  *
  * @return {Object[]} Returns the ordered column definitions.
  */
	function _getOrder(parameters, columns) {
		var index = 1; // Order by first column by default.
		var direction = 'asc'; // Order ASC by default.
		var columnName = 'name'; // Order by products name by default.

		$this.on('click', 'th', function () {
			columnName = $(this).data('column-name');
			index = dataTable.column(this).index();
		});

		// Apply initial table sort.
		if (parameters.sort) {
			direction = parameters.sort.charAt(0) === '-' ? 'desc' : 'asc';
			var _columnName = parameters.sort.slice(1);

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = columns[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var column = _step.value;

					if (column.name === _columnName) {
						index = columns.indexOf(column);
						break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		} else if (data.activeColumns.indexOf('name') > -1) {
			// Order by name if possible.
			index = data.activeColumns.indexOf('name');
		}

		return [[index, direction]];
	}

	/**
  * Get Initial Search Cols
  *
  * @param {Object} parameters Contains the URL parameters.
  * @param {Object} columns Contains the column definitions.
  *
  * @returns {Object[]} Returns the initial filtering values.
  */
	function _getSearchCols(parameters, columns) {
		if (!parameters.filter) {
			return [];
		}

		var searchCols = [];

		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			for (var _iterator2 = columns[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				var column = _step2.value;

				var entry = null;
				var value = parameters.filter[column.name];

				if (value) {
					entry = { search: value };
				}

				searchCols.push(entry);
			}
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		return searchCols;
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$specialPriceModal.on('hide.bs.modal', function () {
			$this.DataTable().ajax.reload();
		});

		$this.on('draw.dt', function () {
			$this.find('thead input:checkbox').prop('checked', false).trigger('change', [false]);
			$this.find('tbody').attr('data-gx-widget', 'single_checkbox switcher');
			$this.find('tbody').attr('data-single_checkbox-selector', '.overview-row-selection');
			$this.find('tbody').attr('data-switcher-selector', '.convert-to-switcher');

			gx.widgets.init($this);
		});

		dataTable = jse.libs.datatable.create($this, options);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvb3ZlcnZpZXcvaW5pdC5qcyJdLCJuYW1lcyI6WyJneCIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRzcGVjaWFsUHJpY2VNb2RhbCIsInBhcmVudHMiLCJmaW5kIiwicGFyYW1ldGVycyIsImRlcGFyYW0iLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInNlYXJjaCIsInNsaWNlIiwiY29sdW1ucyIsImxpYnMiLCJkYXRhdGFibGUiLCJwcmVwYXJlQ29sdW1ucyIsInF1aWNrX2VkaXRfb3ZlcnZpZXdfY29sdW1ucyIsImFjdGl2ZUNvbHVtbnMiLCJvcHRpb25zIiwiYXV0b1dpZHRoIiwiZG9tIiwicGFnZUxlbmd0aCIsInBhcnNlSW50IiwibGVuZ3RoIiwiZGlzcGxheVN0YXJ0IiwicGFnZSIsInNlcnZlclNpZGUiLCJsYW5ndWFnZSIsImdldFRyYW5zbGF0aW9ucyIsImNvcmUiLCJjb25maWciLCJnZXQiLCJhamF4IiwidXJsIiwidHlwZSIsInBhZ2VUb2tlbiIsIm9yZGVyQ2VsbHNUb3AiLCJvcmRlciIsIl9nZXRPcmRlciIsInNlYXJjaENvbHMiLCJfZ2V0U2VhcmNoQ29scyIsImRhdGFUYWJsZSIsImluZGV4IiwiZGlyZWN0aW9uIiwiY29sdW1uTmFtZSIsIm9uIiwiY29sdW1uIiwic29ydCIsImNoYXJBdCIsIm5hbWUiLCJpbmRleE9mIiwiZmlsdGVyIiwiZW50cnkiLCJ2YWx1ZSIsInB1c2giLCJpbml0IiwiZG9uZSIsIkRhdGFUYWJsZSIsInJlbG9hZCIsInByb3AiLCJ0cmlnZ2VyIiwiYXR0ciIsIndpZGdldHMiLCJjcmVhdGUiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsV0FBSCxDQUFlQyxNQUFmLENBQ0MsTUFERCxFQUdDLENBQ0lDLElBQUlDLE1BRFIsbURBRUlELElBQUlDLE1BRlIsa0RBR0MsV0FIRCxFQUlJRCxJQUFJQyxNQUpSLG1EQUtJSixHQUFHSSxNQUxQLHVDQUhELEVBV0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUwsU0FBUyxFQUFmOztBQUVBOzs7OztBQUtBLEtBQU1NLHFCQUFxQkYsTUFBTUcsT0FBTixDQUFjLHNCQUFkLEVBQXNDQyxJQUF0QyxDQUEyQyx1QkFBM0MsQ0FBM0I7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsYUFBYUosRUFBRUssT0FBRixDQUFVQyxPQUFPQyxRQUFQLENBQWdCQyxNQUFoQixDQUF1QkMsS0FBdkIsQ0FBNkIsQ0FBN0IsQ0FBVixDQUFuQjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxVQUFVZCxJQUFJZSxJQUFKLENBQVNDLFNBQVQsQ0FBbUJDLGNBQW5CLENBQWtDZCxLQUFsQyxFQUF5Q0gsSUFBSWUsSUFBSixDQUFTRywyQkFBbEQsRUFBK0VoQixLQUFLaUIsYUFBcEYsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVTtBQUNmQyxhQUFXLEtBREk7QUFFZkMsT0FBSyxHQUZVO0FBR2ZDLGNBQVlDLFNBQVNoQixXQUFXaUIsTUFBWCxJQUFxQnZCLEtBQUtxQixVQUFuQyxDQUhHO0FBSWZHLGdCQUFjRixTQUFTaEIsV0FBV21CLElBQXBCLElBQTRCLENBQUNILFNBQVNoQixXQUFXbUIsSUFBcEIsSUFBNEIsQ0FBN0IsSUFBa0MsRUFBOUQsR0FBbUUsQ0FKbEU7QUFLZkMsY0FBWSxJQUxHO0FBTWZDLFlBQVU3QixJQUFJZSxJQUFKLENBQVNDLFNBQVQsQ0FBbUJjLGVBQW5CLENBQW1DOUIsSUFBSStCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsY0FBcEIsQ0FBbkMsQ0FOSztBQU9mQyxRQUFNO0FBQ0xDLFFBQUtuQyxJQUFJK0IsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxxREFEaEM7QUFFTEcsU0FBTSxNQUZEO0FBR0xsQyxTQUFNO0FBQ0xtQyxlQUFXckMsSUFBSStCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFETjtBQUhELEdBUFM7QUFjZkssaUJBQWUsSUFkQTtBQWVmQyxTQUFPQyxVQUFVaEMsVUFBVixFQUFzQk0sT0FBdEIsQ0FmUTtBQWdCZjJCLGNBQVlDLGVBQWVsQyxVQUFmLEVBQTJCTSxPQUEzQixDQWhCRztBQWlCZkEsV0FBU0E7QUFqQk0sRUFBaEI7O0FBb0JBLEtBQUk2QixrQkFBSjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FBUUEsVUFBU0gsU0FBVCxDQUFtQmhDLFVBQW5CLEVBQStCTSxPQUEvQixFQUF3QztBQUN2QyxNQUFJOEIsUUFBUSxDQUFaLENBRHVDLENBQ3hCO0FBQ2YsTUFBSUMsWUFBWSxLQUFoQixDQUZ1QyxDQUVoQjtBQUN2QixNQUFJQyxhQUFhLE1BQWpCLENBSHVDLENBR2Q7O0FBRXpCM0MsUUFBTTRDLEVBQU4sQ0FBUyxPQUFULEVBQWtCLElBQWxCLEVBQXdCLFlBQVc7QUFDbENELGdCQUFhMUMsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxhQUFiLENBQWI7QUFDQTBDLFdBQVFELFVBQVVLLE1BQVYsQ0FBaUIsSUFBakIsRUFBdUJKLEtBQXZCLEVBQVI7QUFDQSxHQUhEOztBQUtBO0FBQ0EsTUFBSXBDLFdBQVd5QyxJQUFmLEVBQXFCO0FBQ3BCSixlQUFZckMsV0FBV3lDLElBQVgsQ0FBZ0JDLE1BQWhCLENBQXVCLENBQXZCLE1BQThCLEdBQTlCLEdBQW9DLE1BQXBDLEdBQTZDLEtBQXpEO0FBQ0EsT0FBTUosY0FBYXRDLFdBQVd5QyxJQUFYLENBQWdCcEMsS0FBaEIsQ0FBc0IsQ0FBdEIsQ0FBbkI7O0FBRm9CO0FBQUE7QUFBQTs7QUFBQTtBQUlwQix5QkFBbUJDLE9BQW5CLDhIQUE0QjtBQUFBLFNBQW5Ca0MsTUFBbUI7O0FBQzNCLFNBQUlBLE9BQU9HLElBQVAsS0FBZ0JMLFdBQXBCLEVBQWdDO0FBQy9CRixjQUFROUIsUUFBUXNDLE9BQVIsQ0FBZ0JKLE1BQWhCLENBQVI7QUFDQTtBQUNBO0FBQ0Q7QUFUbUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVwQixHQVZELE1BVU8sSUFBSTlDLEtBQUtpQixhQUFMLENBQW1CaUMsT0FBbkIsQ0FBMkIsTUFBM0IsSUFBcUMsQ0FBQyxDQUExQyxFQUE2QztBQUFFO0FBQ3JEUixXQUFRMUMsS0FBS2lCLGFBQUwsQ0FBbUJpQyxPQUFuQixDQUEyQixNQUEzQixDQUFSO0FBQ0E7O0FBRUQsU0FBTyxDQUFDLENBQUNSLEtBQUQsRUFBUUMsU0FBUixDQUFELENBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUFRQSxVQUFTSCxjQUFULENBQXdCbEMsVUFBeEIsRUFBb0NNLE9BQXBDLEVBQTZDO0FBQzVDLE1BQUksQ0FBQ04sV0FBVzZDLE1BQWhCLEVBQXdCO0FBQ3ZCLFVBQU8sRUFBUDtBQUNBOztBQUVELE1BQU1aLGFBQWEsRUFBbkI7O0FBTDRDO0FBQUE7QUFBQTs7QUFBQTtBQU81Qyx5QkFBbUIzQixPQUFuQixtSUFBNEI7QUFBQSxRQUFuQmtDLE1BQW1COztBQUMzQixRQUFJTSxRQUFRLElBQVo7QUFDQSxRQUFJQyxRQUFRL0MsV0FBVzZDLE1BQVgsQ0FBa0JMLE9BQU9HLElBQXpCLENBQVo7O0FBRUEsUUFBSUksS0FBSixFQUFXO0FBQ1ZELGFBQVEsRUFBQzFDLFFBQVEyQyxLQUFULEVBQVI7QUFDQTs7QUFFRGQsZUFBV2UsSUFBWCxDQUFnQkYsS0FBaEI7QUFDQTtBQWhCMkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFrQjVDLFNBQU9iLFVBQVA7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUExQyxRQUFPMEQsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QnJELHFCQUNFMEMsRUFERixDQUNLLGVBREwsRUFDc0IsWUFBVztBQUMvQjVDLFNBQU13RCxTQUFOLEdBQWtCekIsSUFBbEIsQ0FBdUIwQixNQUF2QjtBQUNBLEdBSEY7O0FBS0F6RCxRQUFNNEMsRUFBTixDQUFTLFNBQVQsRUFBb0IsWUFBTTtBQUN6QjVDLFNBQU1JLElBQU4sQ0FBVyxzQkFBWCxFQUNFc0QsSUFERixDQUNPLFNBRFAsRUFDa0IsS0FEbEIsRUFFRUMsT0FGRixDQUVVLFFBRlYsRUFFb0IsQ0FBQyxLQUFELENBRnBCO0FBR0EzRCxTQUFNSSxJQUFOLENBQVcsT0FBWCxFQUFvQndELElBQXBCLENBQXlCLGdCQUF6QixFQUEyQywwQkFBM0M7QUFDQTVELFNBQU1JLElBQU4sQ0FBVyxPQUFYLEVBQW9Cd0QsSUFBcEIsQ0FBeUIsK0JBQXpCLEVBQTBELHlCQUExRDtBQUNBNUQsU0FBTUksSUFBTixDQUFXLE9BQVgsRUFBb0J3RCxJQUFwQixDQUF5Qix3QkFBekIsRUFBbUQsc0JBQW5EOztBQUVBbEUsTUFBR21FLE9BQUgsQ0FBV1AsSUFBWCxDQUFnQnRELEtBQWhCO0FBQ0EsR0FURDs7QUFXQXdDLGNBQVkzQyxJQUFJZSxJQUFKLENBQVNDLFNBQVQsQ0FBbUJpRCxNQUFuQixDQUEwQjlELEtBQTFCLEVBQWlDaUIsT0FBakMsQ0FBWjs7QUFFQXNDO0FBQ0EsRUFwQkQ7O0FBc0JBLFFBQU8zRCxNQUFQO0FBQ0EsQ0FqTEYiLCJmaWxlIjoicXVpY2tfZWRpdC9vdmVydmlldy9pbml0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBpbml0LmpzIDIwMTctMDMtMDZcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIFF1aWNrRWRpdCBUYWJsZSBDb250cm9sbGVyXG4gKlxuICogVGhpcyBjb250cm9sbGVyIGluaXRpYWxpemVzIHRoZSBtYWluIFF1aWNrRWRpdCB0YWJsZSB3aXRoIGEgbmV3IGpRdWVyeSBEYXRhVGFibGVzIGluc3RhbmNlLlxuICovXG5neC5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdpbml0Jyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvZGF0YXRhYmxlcy9qcXVlcnkuZGF0YVRhYmxlcy5taW4uY3NzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvZGF0YXRhYmxlcy9qcXVlcnkuZGF0YVRhYmxlcy5taW4uanNgLFxuXHRcdCdkYXRhdGFibGUnLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktZGVwYXJhbS9qcXVlcnktZGVwYXJhbS5taW4uanNgLFxuXHRcdGAke2d4LnNvdXJjZX0vbGlicy9xdWlja19lZGl0X292ZXJ2aWV3X2NvbHVtbnNgXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNwZWNpYWwgUHJpY2UgTW9kYWxcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHNwZWNpYWxQcmljZU1vZGFsID0gJHRoaXMucGFyZW50cygnLnF1aWNrLWVkaXQub3ZlcnZpZXcnKS5maW5kKCcuc3BlY2lhbC1wcmljZXMubW9kYWwnKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZWFyY2ggUGFyYW1ldGVyc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBwYXJhbWV0ZXJzID0gJC5kZXBhcmFtKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSkpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERhdGFUYWJsZSBDb2x1bW5zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0W119XG5cdFx0ICovXG5cdFx0Y29uc3QgY29sdW1ucyA9IGpzZS5saWJzLmRhdGF0YWJsZS5wcmVwYXJlQ29sdW1ucygkdGhpcywganNlLmxpYnMucXVpY2tfZWRpdF9vdmVydmlld19jb2x1bW5zLCBkYXRhLmFjdGl2ZUNvbHVtbnMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERhdGFUYWJsZSBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSB7XG5cdFx0XHRhdXRvV2lkdGg6IGZhbHNlLFxuXHRcdFx0ZG9tOiAndCcsXG5cdFx0XHRwYWdlTGVuZ3RoOiBwYXJzZUludChwYXJhbWV0ZXJzLmxlbmd0aCB8fCBkYXRhLnBhZ2VMZW5ndGgpLFxuXHRcdFx0ZGlzcGxheVN0YXJ0OiBwYXJzZUludChwYXJhbWV0ZXJzLnBhZ2UpID8gKHBhcnNlSW50KHBhcmFtZXRlcnMucGFnZSkgLSAxKSAqIDI1IDogMCxcblx0XHRcdHNlcnZlclNpZGU6IHRydWUsXG5cdFx0XHRsYW5ndWFnZToganNlLmxpYnMuZGF0YXRhYmxlLmdldFRyYW5zbGF0aW9ucyhqc2UuY29yZS5jb25maWcuZ2V0KCdsYW5ndWFnZUNvZGUnKSksXG5cdFx0XHRhamF4OiB7XG5cdFx0XHRcdHVybDoganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1RdWlja0VkaXRPdmVydmlld0FqYXgvRGF0YVRhYmxlJyxcblx0XHRcdFx0dHlwZTogJ1BPU1QnLFxuXHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0b3JkZXJDZWxsc1RvcDogdHJ1ZSxcblx0XHRcdG9yZGVyOiBfZ2V0T3JkZXIocGFyYW1ldGVycywgY29sdW1ucyksXG5cdFx0XHRzZWFyY2hDb2xzOiBfZ2V0U2VhcmNoQ29scyhwYXJhbWV0ZXJzLCBjb2x1bW5zKSxcblx0XHRcdGNvbHVtbnM6IGNvbHVtbnMsXG5cdFx0fTtcblx0XHRcblx0XHRsZXQgZGF0YVRhYmxlOyBcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgSW5pdGlhbCBUYWJsZSBPcmRlclxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtZXRlcnMgQ29udGFpbnMgdGhlIFVSTCBwYXJhbWV0ZXJzLlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBjb2x1bW5zIENvbnRhaW5zIHRoZSBjb2x1bW4gZGVmaW5pdGlvbnMuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPYmplY3RbXX0gUmV0dXJucyB0aGUgb3JkZXJlZCBjb2x1bW4gZGVmaW5pdGlvbnMuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldE9yZGVyKHBhcmFtZXRlcnMsIGNvbHVtbnMpIHtcblx0XHRcdGxldCBpbmRleCA9IDE7IC8vIE9yZGVyIGJ5IGZpcnN0IGNvbHVtbiBieSBkZWZhdWx0LlxuXHRcdFx0bGV0IGRpcmVjdGlvbiA9ICdhc2MnOyAvLyBPcmRlciBBU0MgYnkgZGVmYXVsdC5cblx0XHRcdGxldCBjb2x1bW5OYW1lID0gJ25hbWUnOyAvLyBPcmRlciBieSBwcm9kdWN0cyBuYW1lIGJ5IGRlZmF1bHQuXG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdjbGljaycsICd0aCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRjb2x1bW5OYW1lID0gJCh0aGlzKS5kYXRhKCdjb2x1bW4tbmFtZScpO1xuXHRcdFx0XHRpbmRleCA9IGRhdGFUYWJsZS5jb2x1bW4odGhpcykuaW5kZXgoKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBcHBseSBpbml0aWFsIHRhYmxlIHNvcnQuXG5cdFx0XHRpZiAocGFyYW1ldGVycy5zb3J0KSB7XG5cdFx0XHRcdGRpcmVjdGlvbiA9IHBhcmFtZXRlcnMuc29ydC5jaGFyQXQoMCkgPT09ICctJyA/ICdkZXNjJyA6ICdhc2MnO1xuXHRcdFx0XHRjb25zdCBjb2x1bW5OYW1lID0gcGFyYW1ldGVycy5zb3J0LnNsaWNlKDEpO1xuXHRcdFx0XHRcblx0XHRcdFx0Zm9yIChsZXQgY29sdW1uIG9mIGNvbHVtbnMpIHtcblx0XHRcdFx0XHRpZiAoY29sdW1uLm5hbWUgPT09IGNvbHVtbk5hbWUpIHtcblx0XHRcdFx0XHRcdGluZGV4ID0gY29sdW1ucy5pbmRleE9mKGNvbHVtbik7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0gZWxzZSBpZiAoZGF0YS5hY3RpdmVDb2x1bW5zLmluZGV4T2YoJ25hbWUnKSA+IC0xKSB7IC8vIE9yZGVyIGJ5IG5hbWUgaWYgcG9zc2libGUuXG5cdFx0XHRcdGluZGV4ID0gZGF0YS5hY3RpdmVDb2x1bW5zLmluZGV4T2YoJ25hbWUnKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0cmV0dXJuIFtbaW5kZXgsIGRpcmVjdGlvbl1dO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgSW5pdGlhbCBTZWFyY2ggQ29sc1xuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtZXRlcnMgQ29udGFpbnMgdGhlIFVSTCBwYXJhbWV0ZXJzLlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBjb2x1bW5zIENvbnRhaW5zIHRoZSBjb2x1bW4gZGVmaW5pdGlvbnMuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJucyB7T2JqZWN0W119IFJldHVybnMgdGhlIGluaXRpYWwgZmlsdGVyaW5nIHZhbHVlcy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0U2VhcmNoQ29scyhwYXJhbWV0ZXJzLCBjb2x1bW5zKSB7XG5cdFx0XHRpZiAoIXBhcmFtZXRlcnMuZmlsdGVyKSB7XG5cdFx0XHRcdHJldHVybiBbXTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Y29uc3Qgc2VhcmNoQ29scyA9IFtdO1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBjb2x1bW4gb2YgY29sdW1ucykge1xuXHRcdFx0XHRsZXQgZW50cnkgPSBudWxsO1xuXHRcdFx0XHRsZXQgdmFsdWUgPSBwYXJhbWV0ZXJzLmZpbHRlcltjb2x1bW4ubmFtZV07XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAodmFsdWUpIHtcblx0XHRcdFx0XHRlbnRyeSA9IHtzZWFyY2g6IHZhbHVlfTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0c2VhcmNoQ29scy5wdXNoKGVudHJ5KTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0cmV0dXJuIHNlYXJjaENvbHM7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkc3BlY2lhbFByaWNlTW9kYWxcblx0XHRcdFx0Lm9uKCdoaWRlLmJzLm1vZGFsJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuYWpheC5yZWxvYWQoKTtcblx0XHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdkcmF3LmR0JywgKCkgPT4ge1xuXHRcdFx0XHQkdGhpcy5maW5kKCd0aGVhZCBpbnB1dDpjaGVja2JveCcpXG5cdFx0XHRcdFx0LnByb3AoJ2NoZWNrZWQnLCBmYWxzZSlcblx0XHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJywgW2ZhbHNlXSk7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3Rib2R5JykuYXR0cignZGF0YS1neC13aWRnZXQnLCAnc2luZ2xlX2NoZWNrYm94IHN3aXRjaGVyJyk7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3Rib2R5JykuYXR0cignZGF0YS1zaW5nbGVfY2hlY2tib3gtc2VsZWN0b3InLCAnLm92ZXJ2aWV3LXJvdy1zZWxlY3Rpb24nKTtcblx0XHRcdFx0JHRoaXMuZmluZCgndGJvZHknKS5hdHRyKCdkYXRhLXN3aXRjaGVyLXNlbGVjdG9yJywgJy5jb252ZXJ0LXRvLXN3aXRjaGVyJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRneC53aWRnZXRzLmluaXQoJHRoaXMpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGRhdGFUYWJsZSA9IGpzZS5saWJzLmRhdGF0YWJsZS5jcmVhdGUoJHRoaXMsIG9wdGlvbnMpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTsiXX0=
