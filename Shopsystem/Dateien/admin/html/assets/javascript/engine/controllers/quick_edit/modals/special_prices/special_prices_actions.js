'use strict';

/* --------------------------------------------------------------
 actions.js 2017-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Special Prices Table Actions Controller
 *
 * This module creates the bulk and row actions for the table.
 */
gx.controllers.module('special_prices_actions', [gx.source + '/libs/button_dropdown', 'user_configuration_service'], function () {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Create Bulk Actions
  *
  * This callback can be called once during the initialization of this module.
  */
	function _createBulkActions() {
		// Add actions to the bulk-action dropdown.
		var $bulkActions = $('.special-price-bulk-action');
		var defaultBulkAction = 'special-price-bulk-row-edit';

		// Edit
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
			class: 'special-price-bulk-row-edit',
			data: { configurationValue: 'special-price-bulk-row-edit' },
			isDefault: defaultBulkAction === 'special-price-bulk-row-edit' || defaultBulkAction === 'save-special-price-bulk-row-edits',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Save
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
			class: 'save-special-price-bulk-row-edits hidden',
			data: { configurationValue: 'save-special-price-bulk-row-edits' },
			isDefault: false, // "Save" must not be shown as a default value. 
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		// Delete
		jse.libs.button_dropdown.addAction($bulkActions, {
			text: jse.core.lang.translate('BUTTON_DELETE', 'admin_quick_edit'),
			class: 'bulk-delete-special-price',
			data: { configurationValue: 'delete-special-price' },
			isDefault: defaultBulkAction === 'delete-special-price',
			callback: function callback(e) {
				return e.preventDefault();
			}
		});

		$this.datatable_default_actions('ensure', 'bulk');
	}

	function _createRowActions() {
		var defaultRowAction = $this.data('defaultRowAction') || 'row-special-price-edit';

		$this.find('.btn-group.dropdown').each(function () {
			// Edit
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
				class: 'row-special-price-edit',
				data: { configurationValue: 'row-special-price-edit' },
				isDefault: defaultRowAction === 'row-special-price-edit' || defaultRowAction === 'save-special-price-edits',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Save
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
				class: 'save-special-price-edits hidden',
				data: { configurationValue: 'save-special-price-edits' },
				isDefault: false, // "Save" must not be shown as a default value. 
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			// Delete
			jse.libs.button_dropdown.addAction($(this), {
				text: jse.core.lang.translate('BUTTON_DELETE', 'admin_quick_edit'),
				class: 'row-delete-special-price',
				data: { configurationValue: 'row-delete-special-price' },
				isDefault: defaultRowAction === 'row-delete-special-price',
				callback: function callback(e) {
					return e.preventDefault();
				}
			});

			$this.datatable_default_actions('ensure', 'row');
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(window).on('JSENGINE_INIT_FINISHED', function () {
			$this.on('draw.dt', _createRowActions);
			_createRowActions();
			_createBulkActions();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXQvbW9kYWxzL3NwZWNpYWxfcHJpY2VzL3NwZWNpYWxfcHJpY2VzX2FjdGlvbnMuanMiXSwibmFtZXMiOlsiZ3giLCJjb250cm9sbGVycyIsIm1vZHVsZSIsInNvdXJjZSIsIiR0aGlzIiwiJCIsIl9jcmVhdGVCdWxrQWN0aW9ucyIsIiRidWxrQWN0aW9ucyIsImRlZmF1bHRCdWxrQWN0aW9uIiwianNlIiwibGlicyIsImJ1dHRvbl9kcm9wZG93biIsImFkZEFjdGlvbiIsInRleHQiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsImNsYXNzIiwiZGF0YSIsImNvbmZpZ3VyYXRpb25WYWx1ZSIsImlzRGVmYXVsdCIsImNhbGxiYWNrIiwiZSIsInByZXZlbnREZWZhdWx0IiwiZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucyIsIl9jcmVhdGVSb3dBY3Rpb25zIiwiZGVmYXVsdFJvd0FjdGlvbiIsImZpbmQiLCJlYWNoIiwiaW5pdCIsImRvbmUiLCJ3aW5kb3ciLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7OztBQUtBQSxHQUFHQyxXQUFILENBQWVDLE1BQWYsQ0FDQyx3QkFERCxFQUdDLENBQUlGLEdBQUdHLE1BQVAsNEJBQXNDLDRCQUF0QyxDQUhELEVBS0MsWUFBVzs7QUFFVjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0ksa0JBQVQsR0FBOEI7QUFDN0I7QUFDQSxNQUFNQyxlQUFlRixFQUFFLDRCQUFGLENBQXJCO0FBQ0EsTUFBTUcsb0JBQW9CLDZCQUExQjs7QUFFQTtBQUNBQyxNQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLFNBQXpCLENBQW1DTCxZQUFuQyxFQUFpRDtBQUNoRE0sU0FBTUosSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsYUFBeEIsRUFBdUMsa0JBQXZDLENBRDBDO0FBRWhEQyxVQUFPLDZCQUZ5QztBQUdoREMsU0FBTSxFQUFDQyxvQkFBb0IsNkJBQXJCLEVBSDBDO0FBSWhEQyxjQUFXWixzQkFBc0IsNkJBQXRCLElBQ1JBLHNCQUFzQixtQ0FMdUI7QUFNaERhLGFBQVU7QUFBQSxXQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQU5zQyxHQUFqRDs7QUFTQTtBQUNBZCxNQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLFNBQXpCLENBQW1DTCxZQUFuQyxFQUFpRDtBQUNoRE0sU0FBTUosSUFBSUssSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsYUFBeEIsRUFBdUMsa0JBQXZDLENBRDBDO0FBRWhEQyxVQUFPLDBDQUZ5QztBQUdoREMsU0FBTSxFQUFDQyxvQkFBb0IsbUNBQXJCLEVBSDBDO0FBSWhEQyxjQUFXLEtBSnFDLEVBSTdCO0FBQ25CQyxhQUFVO0FBQUEsV0FBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMc0MsR0FBakQ7O0FBUUE7QUFDQWQsTUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ0wsWUFBbkMsRUFBaUQ7QUFDaERNLFNBQU1KLElBQUlLLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGVBQXhCLEVBQXlDLGtCQUF6QyxDQUQwQztBQUVoREMsVUFBTywyQkFGeUM7QUFHaERDLFNBQU0sRUFBQ0Msb0JBQW9CLHNCQUFyQixFQUgwQztBQUloREMsY0FBV1osc0JBQXNCLHNCQUplO0FBS2hEYSxhQUFVO0FBQUEsV0FBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMc0MsR0FBakQ7O0FBUUFuQixRQUFNb0IseUJBQU4sQ0FBZ0MsUUFBaEMsRUFBMEMsTUFBMUM7QUFDQTs7QUFFRCxVQUFTQyxpQkFBVCxHQUE2QjtBQUM1QixNQUFNQyxtQkFBbUJ0QixNQUFNYyxJQUFOLENBQVcsa0JBQVgsS0FBa0Msd0JBQTNEOztBQUVBZCxRQUFNdUIsSUFBTixDQUFXLHFCQUFYLEVBQWtDQyxJQUFsQyxDQUF1QyxZQUFXO0FBQ2pEO0FBQ0FuQixPQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLFNBQXpCLENBQW1DUCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NRLFVBQU1KLElBQUlLLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGFBQXhCLEVBQXVDLGtCQUF2QyxDQURxQztBQUUzQ0MsV0FBTyx3QkFGb0M7QUFHM0NDLFVBQU0sRUFBQ0Msb0JBQW9CLHdCQUFyQixFQUhxQztBQUkzQ0MsZUFBV00scUJBQXFCLHdCQUFyQixJQUNSQSxxQkFBcUIsMEJBTG1CO0FBTTNDTCxjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFOaUMsSUFBNUM7O0FBU0E7QUFDQWQsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ1AsRUFBRSxJQUFGLENBQW5DLEVBQTRDO0FBQzNDUSxVQUFNSixJQUFJSyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxrQkFBdkMsQ0FEcUM7QUFFM0NDLFdBQU8saUNBRm9DO0FBRzNDQyxVQUFNLEVBQUNDLG9CQUFvQiwwQkFBckIsRUFIcUM7QUFJM0NDLGVBQVcsS0FKZ0MsRUFJeEI7QUFDbkJDLGNBQVU7QUFBQSxZQUFLQyxFQUFFQyxjQUFGLEVBQUw7QUFBQTtBQUxpQyxJQUE1Qzs7QUFRQTtBQUNBZCxPQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLFNBQXpCLENBQW1DUCxFQUFFLElBQUYsQ0FBbkMsRUFBNEM7QUFDM0NRLFVBQU1KLElBQUlLLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGVBQXhCLEVBQXlDLGtCQUF6QyxDQURxQztBQUUzQ0MsV0FBTywwQkFGb0M7QUFHM0NDLFVBQU0sRUFBQ0Msb0JBQW9CLDBCQUFyQixFQUhxQztBQUkzQ0MsZUFBV00scUJBQXFCLDBCQUpXO0FBSzNDTCxjQUFVO0FBQUEsWUFBS0MsRUFBRUMsY0FBRixFQUFMO0FBQUE7QUFMaUMsSUFBNUM7O0FBUUFuQixTQUFNb0IseUJBQU4sQ0FBZ0MsUUFBaEMsRUFBMEMsS0FBMUM7QUFDQSxHQTlCRDtBQStCQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUF0QixRQUFPMkIsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QnpCLElBQUUwQixNQUFGLEVBQVVDLEVBQVYsQ0FBYSx3QkFBYixFQUF1QyxZQUFNO0FBQzVDNUIsU0FBTTRCLEVBQU4sQ0FBUyxTQUFULEVBQW9CUCxpQkFBcEI7QUFDQUE7QUFDQW5CO0FBQ0EsR0FKRDs7QUFNQXdCO0FBQ0EsRUFSRDs7QUFVQSxRQUFPNUIsTUFBUDtBQUVBLENBNUhGIiwiZmlsZSI6InF1aWNrX2VkaXQvbW9kYWxzL3NwZWNpYWxfcHJpY2VzL3NwZWNpYWxfcHJpY2VzX2FjdGlvbnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGFjdGlvbnMuanMgMjAxNy0wNS0yOVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogU3BlY2lhbCBQcmljZXMgVGFibGUgQWN0aW9ucyBDb250cm9sbGVyXG4gKlxuICogVGhpcyBtb2R1bGUgY3JlYXRlcyB0aGUgYnVsayBhbmQgcm93IGFjdGlvbnMgZm9yIHRoZSB0YWJsZS5cbiAqL1xuZ3guY29udHJvbGxlcnMubW9kdWxlKFxuXHQnc3BlY2lhbF9wcmljZXNfYWN0aW9ucycsXG5cdFxuXHRbYCR7Z3guc291cmNlfS9saWJzL2J1dHRvbl9kcm9wZG93bmAsICd1c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSddLFxuXHRcblx0ZnVuY3Rpb24oKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENyZWF0ZSBCdWxrIEFjdGlvbnNcblx0XHQgKlxuXHRcdCAqIFRoaXMgY2FsbGJhY2sgY2FuIGJlIGNhbGxlZCBvbmNlIGR1cmluZyB0aGUgaW5pdGlhbGl6YXRpb24gb2YgdGhpcyBtb2R1bGUuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2NyZWF0ZUJ1bGtBY3Rpb25zKCkge1xuXHRcdFx0Ly8gQWRkIGFjdGlvbnMgdG8gdGhlIGJ1bGstYWN0aW9uIGRyb3Bkb3duLlxuXHRcdFx0Y29uc3QgJGJ1bGtBY3Rpb25zID0gJCgnLnNwZWNpYWwtcHJpY2UtYnVsay1hY3Rpb24nKTtcblx0XHRcdGNvbnN0IGRlZmF1bHRCdWxrQWN0aW9uID0gJ3NwZWNpYWwtcHJpY2UtYnVsay1yb3ctZWRpdCc7XG5cdFx0XHRcblx0XHRcdC8vIEVkaXRcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XG5cdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fRURJVCcsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdGNsYXNzOiAnc3BlY2lhbC1wcmljZS1idWxrLXJvdy1lZGl0Jyxcblx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ3NwZWNpYWwtcHJpY2UtYnVsay1yb3ctZWRpdCd9LFxuXHRcdFx0XHRpc0RlZmF1bHQ6IGRlZmF1bHRCdWxrQWN0aW9uID09PSAnc3BlY2lhbC1wcmljZS1idWxrLXJvdy1lZGl0J1xuXHRcdFx0XHR8fCBkZWZhdWx0QnVsa0FjdGlvbiA9PT0gJ3NhdmUtc3BlY2lhbC1wcmljZS1idWxrLXJvdy1lZGl0cycsXG5cdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIFNhdmVcblx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJGJ1bGtBY3Rpb25zLCB7XG5cdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fU0FWRScsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdGNsYXNzOiAnc2F2ZS1zcGVjaWFsLXByaWNlLWJ1bGstcm93LWVkaXRzIGhpZGRlbicsXG5cdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdzYXZlLXNwZWNpYWwtcHJpY2UtYnVsay1yb3ctZWRpdHMnfSxcblx0XHRcdFx0aXNEZWZhdWx0OiBmYWxzZSwgIC8vIFwiU2F2ZVwiIG11c3Qgbm90IGJlIHNob3duIGFzIGEgZGVmYXVsdCB2YWx1ZS4gXG5cdFx0XHRcdGNhbGxiYWNrOiBlID0+IGUucHJldmVudERlZmF1bHQoKVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIERlbGV0ZVxuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkYnVsa0FjdGlvbnMsIHtcblx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9ERUxFVEUnLCAnYWRtaW5fcXVpY2tfZWRpdCcpLFxuXHRcdFx0XHRjbGFzczogJ2J1bGstZGVsZXRlLXNwZWNpYWwtcHJpY2UnLFxuXHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnZGVsZXRlLXNwZWNpYWwtcHJpY2UnfSxcblx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0QnVsa0FjdGlvbiA9PT0gJ2RlbGV0ZS1zcGVjaWFsLXByaWNlJyxcblx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JHRoaXMuZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucygnZW5zdXJlJywgJ2J1bGsnKTtcblx0XHR9XG5cdFx0XG5cdFx0ZnVuY3Rpb24gX2NyZWF0ZVJvd0FjdGlvbnMoKSB7XG5cdFx0XHRjb25zdCBkZWZhdWx0Um93QWN0aW9uID0gJHRoaXMuZGF0YSgnZGVmYXVsdFJvd0FjdGlvbicpIHx8ICdyb3ctc3BlY2lhbC1wcmljZS1lZGl0Jztcblx0XHRcdFxuXHRcdFx0JHRoaXMuZmluZCgnLmJ0bi1ncm91cC5kcm9wZG93bicpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdC8vIEVkaXRcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLmFkZEFjdGlvbigkKHRoaXMpLCB7XG5cdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9FRElUJywgJ2FkbWluX3F1aWNrX2VkaXQnKSxcblx0XHRcdFx0XHRjbGFzczogJ3Jvdy1zcGVjaWFsLXByaWNlLWVkaXQnLFxuXHRcdFx0XHRcdGRhdGE6IHtjb25maWd1cmF0aW9uVmFsdWU6ICdyb3ctc3BlY2lhbC1wcmljZS1lZGl0J30sXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBkZWZhdWx0Um93QWN0aW9uID09PSAncm93LXNwZWNpYWwtcHJpY2UtZWRpdCdcblx0XHRcdFx0XHR8fCBkZWZhdWx0Um93QWN0aW9uID09PSAnc2F2ZS1zcGVjaWFsLXByaWNlLWVkaXRzJyxcblx0XHRcdFx0XHRjYWxsYmFjazogZSA9PiBlLnByZXZlbnREZWZhdWx0KClcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTYXZlXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fU0FWRScsICdhZG1pbl9xdWlja19lZGl0JyksXG5cdFx0XHRcdFx0Y2xhc3M6ICdzYXZlLXNwZWNpYWwtcHJpY2UtZWRpdHMgaGlkZGVuJyxcblx0XHRcdFx0XHRkYXRhOiB7Y29uZmlndXJhdGlvblZhbHVlOiAnc2F2ZS1zcGVjaWFsLXByaWNlLWVkaXRzJ30sXG5cdFx0XHRcdFx0aXNEZWZhdWx0OiBmYWxzZSwgIC8vIFwiU2F2ZVwiIG11c3Qgbm90IGJlIHNob3duIGFzIGEgZGVmYXVsdCB2YWx1ZS4gXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRGVsZXRlXG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5hZGRBY3Rpb24oJCh0aGlzKSwge1xuXHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fREVMRVRFJywgJ2FkbWluX3F1aWNrX2VkaXQnKSxcblx0XHRcdFx0XHRjbGFzczogJ3Jvdy1kZWxldGUtc3BlY2lhbC1wcmljZScsXG5cdFx0XHRcdFx0ZGF0YToge2NvbmZpZ3VyYXRpb25WYWx1ZTogJ3Jvdy1kZWxldGUtc3BlY2lhbC1wcmljZSd9LFxuXHRcdFx0XHRcdGlzRGVmYXVsdDogZGVmYXVsdFJvd0FjdGlvbiA9PT0gJ3Jvdy1kZWxldGUtc3BlY2lhbC1wcmljZScsXG5cdFx0XHRcdFx0Y2FsbGJhY2s6IGUgPT4gZS5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0JHRoaXMuZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucygnZW5zdXJlJywgJ3JvdycpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkKHdpbmRvdykub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCAoKSA9PiB7XG5cdFx0XHRcdCR0aGlzLm9uKCdkcmF3LmR0JywgX2NyZWF0ZVJvd0FjdGlvbnMpO1xuXHRcdFx0XHRfY3JlYXRlUm93QWN0aW9ucygpO1xuXHRcdFx0XHRfY3JlYXRlQnVsa0FjdGlvbnMoKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHRcdFxuXHR9KTsgIl19
