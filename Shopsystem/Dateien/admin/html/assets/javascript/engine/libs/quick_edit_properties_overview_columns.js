'use strict';

/* --------------------------------------------------------------
 quick_edit_properties_overview_columns.js 2016-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.quick_edit_properties_overview_columns = jse.libs.quick_edit_properties_overview_columns || {};

(function (exports) {

	'use strict';

	exports.checkbox = exports.checkbox || {
		data: null,
		minWidth: '50px',
		widthFactor: 0.01,
		orderable: false,
		searchable: false,
		render: function render() {
			return '<input type="checkbox" class="properties-row-selection" />';
		}
	};

	exports.productsName = exports.productsName || {
		data: 'productsName',
		minWidth: '150px',
		widthFactor: 1.6
	};

	exports.combiName = exports.combiName || {
		data: 'combiName',
		minWidth: '150px',
		widthFactor: 1.6
	};

	exports.combiModel = exports.combiModel || {
		data: 'combiModel',
		minWidth: '120px',
		widthFactor: 1,
		className: 'editable'
	};

	exports.combiQuantity = exports.combiQuantity || {
		data: 'combiQuantity',
		minWidth: '90px',
		widthFactor: 1,
		className: 'numeric editable'
	};

	exports.combiPrice = exports.combiPrice || {
		data: 'combiPrice',
		minWidth: '90px',
		widthFactor: 1,
		className: 'numeric combi-price',
		render: function render(data, type, full, meta) {
			var html = '<div class="col-lg-12">\n\t\t\t\t\t\t\t\t<label class="control-label">' + full.combiName + '</label>\n\t\t\t\t\t\t\t\t<p data-properties-price-type="' + full.combiPriceType + '" \n\t\t\t\t\t\t\t\t   class="form-control-static values_price">\n\t\t\t\t\t\t\t\t\t\t' + full.combiPrice.values_price + '\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t </div>';

			return '<div class="container">\n\t\t\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t\t\t<div class="form-horizontal">\n\t\t\t\t\t\t\t\t\t<div class="form-group">' + html + '</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>';
		}
	};

	exports.combiPriceType = exports.combiPriceType || {
		data: 'combiPriceType',
		minWidth: '120px',
		widthFactor: 1.1,
		render: function render(data, type, full, meta) {
			var html = '';
			var options = full.option.priceType;

			options.forEach(function (option) {
				html += '<option value="' + option.id + '" ' + (full.combiPriceType == option.id ? 'selected' : '') + '>\n\t\t\t\t\t\t\t' + option.value + '\n\t\t\t\t\t\t</option>';
			});

			return '<select class="form-control select-properties-price-type">' + html + '</select>';
		}
	};

	exports.combiEan = exports.combiEan || {
		data: 'combiEan',
		minWidth: '90px',
		widthFactor: 1,
		className: 'editable'
	};

	exports.combiWeight = exports.combiWeight || {
		data: 'combiWeight',
		minWidth: '90px',
		widthFactor: 1,
		className: 'numeric editable'
	};

	exports.combiShippingStatusName = exports.combiShippingStatusName || {
		data: 'combiShippingTimeId',
		minWidth: '120px',
		widthFactor: 1.1,
		render: function render(data, type, full, meta) {
			var html = '';
			var options = full.option.shipment;

			options.forEach(function (option) {
				html += '<option value="' + option.id + '" ' + (full.combiShippingTimeId == option.id ? 'selected' : '') + '>\n\t\t\t\t\t\t\t' + option.value + '\n\t\t\t\t\t\t</option>';
			});

			return '<select class="form-control select-properties-shipping-time">' + html + '</select>';
		}
	};

	exports.actions = exports.actions || {
		data: null,
		minWidth: '400px',
		widthFactor: 3.2,
		className: 'actions',
		orderable: false,
		searchable: false,
		render: function render(data, type, full, meta) {
			return '\t\t\t\t\t\n\t\t\t\t\t<div class="pull-left"></div>\n\t\t\t\t\t<div class="pull-right action-list visible-on-hover">\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class="btn-group dropdown">\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default"></button>\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default dropdown-toggle"\n\t\t\t\t\t\t\t\t\tdata-toggle="dropdown"\n\t\t\t\t\t\t\t\t\taria-haspopup="true"\n\t\t\t\t\t\t\t\t\taria-expanded="false">\n\t\t\t\t\t\t\t\t<span class="caret"></span>\n\t\t\t\t\t\t\t\t<span class="sr-only">Toggle Dropdown</span>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t<ul class="dropdown-menu dropdown-menu-right"></ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t';
		}
	};
})(jse.libs.quick_edit_properties_overview_columns);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXRfcHJvcGVydGllc19vdmVydmlld19jb2x1bW5zLmpzIl0sIm5hbWVzIjpbImpzZSIsImxpYnMiLCJxdWlja19lZGl0X3Byb3BlcnRpZXNfb3ZlcnZpZXdfY29sdW1ucyIsImV4cG9ydHMiLCJjaGVja2JveCIsImRhdGEiLCJtaW5XaWR0aCIsIndpZHRoRmFjdG9yIiwib3JkZXJhYmxlIiwic2VhcmNoYWJsZSIsInJlbmRlciIsInByb2R1Y3RzTmFtZSIsImNvbWJpTmFtZSIsImNvbWJpTW9kZWwiLCJjbGFzc05hbWUiLCJjb21iaVF1YW50aXR5IiwiY29tYmlQcmljZSIsInR5cGUiLCJmdWxsIiwibWV0YSIsImh0bWwiLCJjb21iaVByaWNlVHlwZSIsInZhbHVlc19wcmljZSIsIm9wdGlvbnMiLCJvcHRpb24iLCJwcmljZVR5cGUiLCJmb3JFYWNoIiwiaWQiLCJ2YWx1ZSIsImNvbWJpRWFuIiwiY29tYmlXZWlnaHQiLCJjb21iaVNoaXBwaW5nU3RhdHVzTmFtZSIsInNoaXBtZW50IiwiY29tYmlTaGlwcGluZ1RpbWVJZCIsImFjdGlvbnMiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQyxzQ0FBVCxHQUFrREYsSUFBSUMsSUFBSixDQUFTQyxzQ0FBVCxJQUFtRCxFQUFyRzs7QUFFQSxDQUFDLFVBQVNDLE9BQVQsRUFBa0I7O0FBRWxCOztBQUVBQSxTQUFRQyxRQUFSLEdBQW1CRCxRQUFRQyxRQUFSLElBQW9CO0FBQ3JDQyxRQUFNLElBRCtCO0FBRXJDQyxZQUFVLE1BRjJCO0FBR3JDQyxlQUFhLElBSHdCO0FBSXJDQyxhQUFXLEtBSjBCO0FBS3JDQyxjQUFZLEtBTHlCO0FBTXJDQyxRQU5xQyxvQkFNNUI7QUFDUjtBQUNBO0FBUm9DLEVBQXZDOztBQVdBUCxTQUFRUSxZQUFSLEdBQXVCUixRQUFRUSxZQUFSLElBQXdCO0FBQzdDTixRQUFNLGNBRHVDO0FBRTdDQyxZQUFVLE9BRm1DO0FBRzdDQyxlQUFhO0FBSGdDLEVBQS9DOztBQU1BSixTQUFRUyxTQUFSLEdBQW9CVCxRQUFRUyxTQUFSLElBQXFCO0FBQ3ZDUCxRQUFNLFdBRGlDO0FBRXZDQyxZQUFVLE9BRjZCO0FBR3ZDQyxlQUFhO0FBSDBCLEVBQXpDOztBQU1BSixTQUFRVSxVQUFSLEdBQXFCVixRQUFRVSxVQUFSLElBQXNCO0FBQ3pDUixRQUFNLFlBRG1DO0FBRXpDQyxZQUFVLE9BRitCO0FBR3pDQyxlQUFhLENBSDRCO0FBSXpDTyxhQUFXO0FBSjhCLEVBQTNDOztBQU9BWCxTQUFRWSxhQUFSLEdBQXdCWixRQUFRWSxhQUFSLElBQXlCO0FBQy9DVixRQUFNLGVBRHlDO0FBRS9DQyxZQUFVLE1BRnFDO0FBRy9DQyxlQUFhLENBSGtDO0FBSS9DTyxhQUFXO0FBSm9DLEVBQWpEOztBQU9BWCxTQUFRYSxVQUFSLEdBQXFCYixRQUFRYSxVQUFSLElBQXNCO0FBQ3pDWCxRQUFNLFlBRG1DO0FBRXpDQyxZQUFVLE1BRitCO0FBR3pDQyxlQUFhLENBSDRCO0FBSXpDTyxhQUFXLHFCQUo4QjtBQUt6Q0osUUFMeUMsa0JBS2xDTCxJQUxrQyxFQUs1QlksSUFMNEIsRUFLdEJDLElBTHNCLEVBS2hCQyxJQUxnQixFQUtYO0FBQzdCLE9BQUlDLGtGQUMrQkYsS0FBS04sU0FEcEMsaUVBRWlDTSxLQUFLRyxjQUZ0Qyw4RkFJSUgsS0FBS0YsVUFBTCxDQUFnQk0sWUFKcEIsa0RBQUo7O0FBU0Esa0tBRytCRixJQUgvQjtBQU9BO0FBdEJ3QyxFQUEzQzs7QUF5QkFqQixTQUFRa0IsY0FBUixHQUF5QmxCLFFBQVFrQixjQUFSLElBQTBCO0FBQ2pEaEIsUUFBTSxnQkFEMkM7QUFFakRDLFlBQVUsT0FGdUM7QUFHakRDLGVBQWEsR0FIb0M7QUFJakRHLFFBSmlELGtCQUkxQ0wsSUFKMEMsRUFJcENZLElBSm9DLEVBSTlCQyxJQUo4QixFQUl4QkMsSUFKd0IsRUFJbkI7QUFDN0IsT0FBSUMsT0FBTyxFQUFYO0FBQ0EsT0FBTUcsVUFBVUwsS0FBS00sTUFBTCxDQUFZQyxTQUE1Qjs7QUFFQUYsV0FBUUcsT0FBUixDQUFnQixrQkFBVTtBQUN6Qk4sZ0NBQTBCSSxPQUFPRyxFQUFqQyxXQUF3Q1QsS0FBS0csY0FBTCxJQUF1QkcsT0FBT0csRUFBOUIsR0FBbUMsVUFBbkMsR0FBZ0QsRUFBeEYsMEJBQ0lILE9BQU9JLEtBRFg7QUFHQSxJQUpEOztBQU1BLHlFQUFvRVIsSUFBcEU7QUFDQTtBQWZnRCxFQUFuRDs7QUFrQkFqQixTQUFRMEIsUUFBUixHQUFtQjFCLFFBQVEwQixRQUFSLElBQW9CO0FBQ3JDeEIsUUFBTSxVQUQrQjtBQUVyQ0MsWUFBVSxNQUYyQjtBQUdyQ0MsZUFBYSxDQUh3QjtBQUlyQ08sYUFBVztBQUowQixFQUF2Qzs7QUFPQVgsU0FBUTJCLFdBQVIsR0FBc0IzQixRQUFRMkIsV0FBUixJQUF1QjtBQUMzQ3pCLFFBQU0sYUFEcUM7QUFFM0NDLFlBQVUsTUFGaUM7QUFHM0NDLGVBQWEsQ0FIOEI7QUFJM0NPLGFBQVc7QUFKZ0MsRUFBN0M7O0FBT0FYLFNBQVE0Qix1QkFBUixHQUFrQzVCLFFBQVE0Qix1QkFBUixJQUFtQztBQUNuRTFCLFFBQU0scUJBRDZEO0FBRW5FQyxZQUFVLE9BRnlEO0FBR25FQyxlQUFhLEdBSHNEO0FBSW5FRyxRQUptRSxrQkFJNURMLElBSjRELEVBSXREWSxJQUpzRCxFQUloREMsSUFKZ0QsRUFJMUNDLElBSjBDLEVBSXJDO0FBQzdCLE9BQUlDLE9BQU8sRUFBWDtBQUNBLE9BQU1HLFVBQVVMLEtBQUtNLE1BQUwsQ0FBWVEsUUFBNUI7O0FBRUFULFdBQVFHLE9BQVIsQ0FBZ0Isa0JBQVU7QUFDekJOLGdDQUEwQkksT0FBT0csRUFBakMsV0FBd0NULEtBQUtlLG1CQUFMLElBQTRCVCxPQUFPRyxFQUFuQyxHQUF3QyxVQUF4QyxHQUFxRCxFQUE3RiwwQkFDSUgsT0FBT0ksS0FEWDtBQUdBLElBSkQ7O0FBTUEsNEVBQXVFUixJQUF2RTtBQUNBO0FBZmtFLEVBQXJFOztBQW1CQWpCLFNBQVErQixPQUFSLEdBQWtCL0IsUUFBUStCLE9BQVIsSUFBbUI7QUFDbkM3QixRQUFNLElBRDZCO0FBRW5DQyxZQUFVLE9BRnlCO0FBR25DQyxlQUFhLEdBSHNCO0FBSW5DTyxhQUFXLFNBSndCO0FBS25DTixhQUFXLEtBTHdCO0FBTW5DQyxjQUFZLEtBTnVCO0FBT25DQyxRQVBtQyxrQkFPNUJMLElBUDRCLEVBT3RCWSxJQVBzQixFQU9oQkMsSUFQZ0IsRUFPVkMsSUFQVSxFQU9KO0FBQzlCO0FBbUJBO0FBM0JrQyxFQUFyQztBQTZCQSxDQWxKRCxFQWtKR25CLElBQUlDLElBQUosQ0FBU0Msc0NBbEpaIiwiZmlsZSI6InF1aWNrX2VkaXRfcHJvcGVydGllc19vdmVydmlld19jb2x1bW5zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBxdWlja19lZGl0X3Byb3BlcnRpZXNfb3ZlcnZpZXdfY29sdW1ucy5qcyAyMDE2LTA5LTI5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuanNlLmxpYnMucXVpY2tfZWRpdF9wcm9wZXJ0aWVzX292ZXJ2aWV3X2NvbHVtbnMgPSBqc2UubGlicy5xdWlja19lZGl0X3Byb3BlcnRpZXNfb3ZlcnZpZXdfY29sdW1ucyB8fCB7fTtcblxuKGZ1bmN0aW9uKGV4cG9ydHMpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdGV4cG9ydHMuY2hlY2tib3ggPSBleHBvcnRzLmNoZWNrYm94IHx8IHtcblx0XHRcdGRhdGE6IG51bGwsXG5cdFx0XHRtaW5XaWR0aDogJzUwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDAuMDEsXG5cdFx0XHRvcmRlcmFibGU6IGZhbHNlLFxuXHRcdFx0c2VhcmNoYWJsZTogZmFsc2UsXG5cdFx0XHRyZW5kZXIoKSB7XG5cdFx0XHRcdHJldHVybiBgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGNsYXNzPVwicHJvcGVydGllcy1yb3ctc2VsZWN0aW9uXCIgLz5gXG5cdFx0XHR9XG5cdFx0fTtcblx0XG5cdGV4cG9ydHMucHJvZHVjdHNOYW1lID0gZXhwb3J0cy5wcm9kdWN0c05hbWUgfHwge1xuXHRcdFx0ZGF0YTogJ3Byb2R1Y3RzTmFtZScsXG5cdFx0XHRtaW5XaWR0aDogJzE1MHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjZcblx0XHR9O1xuXHRcblx0ZXhwb3J0cy5jb21iaU5hbWUgPSBleHBvcnRzLmNvbWJpTmFtZSB8fCB7XG5cdFx0XHRkYXRhOiAnY29tYmlOYW1lJyxcblx0XHRcdG1pbldpZHRoOiAnMTUwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEuNixcblx0XHR9O1xuXHRcblx0ZXhwb3J0cy5jb21iaU1vZGVsID0gZXhwb3J0cy5jb21iaU1vZGVsIHx8IHtcblx0XHRcdGRhdGE6ICdjb21iaU1vZGVsJyxcblx0XHRcdG1pbldpZHRoOiAnMTIwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEsXG5cdFx0XHRjbGFzc05hbWU6ICdlZGl0YWJsZSdcblx0XHR9O1xuXHRcblx0ZXhwb3J0cy5jb21iaVF1YW50aXR5ID0gZXhwb3J0cy5jb21iaVF1YW50aXR5IHx8IHtcblx0XHRcdGRhdGE6ICdjb21iaVF1YW50aXR5Jyxcblx0XHRcdG1pbldpZHRoOiAnOTBweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMSxcblx0XHRcdGNsYXNzTmFtZTogJ251bWVyaWMgZWRpdGFibGUnXG5cdFx0fTtcblx0XG5cdGV4cG9ydHMuY29tYmlQcmljZSA9IGV4cG9ydHMuY29tYmlQcmljZSB8fCB7XG5cdFx0XHRkYXRhOiAnY29tYmlQcmljZScsXG5cdFx0XHRtaW5XaWR0aDogJzkwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEsXG5cdFx0XHRjbGFzc05hbWU6ICdudW1lcmljIGNvbWJpLXByaWNlJyxcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKXtcblx0XHRcdFx0bGV0IGh0bWwgPSBgPGRpdiBjbGFzcz1cImNvbC1sZy0xMlwiPlxuXHRcdFx0XHRcdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbnRyb2wtbGFiZWxcIj4ke2Z1bGwuY29tYmlOYW1lfTwvbGFiZWw+XG5cdFx0XHRcdFx0XHRcdFx0PHAgZGF0YS1wcm9wZXJ0aWVzLXByaWNlLXR5cGU9XCIke2Z1bGwuY29tYmlQcmljZVR5cGV9XCIgXG5cdFx0XHRcdFx0XHRcdFx0ICAgY2xhc3M9XCJmb3JtLWNvbnRyb2wtc3RhdGljIHZhbHVlc19wcmljZVwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQke2Z1bGwuY29tYmlQcmljZS52YWx1ZXNfcHJpY2V9XG5cdFx0XHRcdFx0XHRcdFx0PC9wPlxuXHRcdFx0XHRcdFx0XHQgPC9kaXY+YDtcblx0XHRcdFx0XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gYDxkaXYgY2xhc3M9XCJjb250YWluZXJcIj5cblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInJvd1wiPlxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIj5cblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+JHtodG1sfTwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHRcdDwvZGl2PmA7XG5cdFx0XHR9XG5cdFx0fTtcblx0XG5cdGV4cG9ydHMuY29tYmlQcmljZVR5cGUgPSBleHBvcnRzLmNvbWJpUHJpY2VUeXBlIHx8IHtcblx0XHRcdGRhdGE6ICdjb21iaVByaWNlVHlwZScsXG5cdFx0XHRtaW5XaWR0aDogJzEyMHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjEsXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSl7XG5cdFx0XHRcdGxldCBodG1sID0gJyc7XG5cdFx0XHRcdGNvbnN0IG9wdGlvbnMgPSBmdWxsLm9wdGlvbi5wcmljZVR5cGU7XG5cdFx0XHRcdFxuXHRcdFx0XHRvcHRpb25zLmZvckVhY2gob3B0aW9uID0+IHtcblx0XHRcdFx0XHRodG1sICs9IGA8b3B0aW9uIHZhbHVlPVwiJHtvcHRpb24uaWR9XCIgJHtmdWxsLmNvbWJpUHJpY2VUeXBlID09IG9wdGlvbi5pZCA/ICdzZWxlY3RlZCcgOiAnJ30+XG5cdFx0XHRcdFx0XHRcdCR7b3B0aW9uLnZhbHVlfVxuXHRcdFx0XHRcdFx0PC9vcHRpb24+YDtcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gYDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2wgc2VsZWN0LXByb3BlcnRpZXMtcHJpY2UtdHlwZVwiPiR7aHRtbH08L3NlbGVjdD5gO1xuXHRcdFx0fVxuXHRcdH07XG5cdFxuXHRleHBvcnRzLmNvbWJpRWFuID0gZXhwb3J0cy5jb21iaUVhbiB8fCB7XG5cdFx0XHRkYXRhOiAnY29tYmlFYW4nLFxuXHRcdFx0bWluV2lkdGg6ICc5MHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLFxuXHRcdFx0Y2xhc3NOYW1lOiAnZWRpdGFibGUnXG5cdFx0fTtcblx0XG5cdGV4cG9ydHMuY29tYmlXZWlnaHQgPSBleHBvcnRzLmNvbWJpV2VpZ2h0IHx8IHtcblx0XHRcdGRhdGE6ICdjb21iaVdlaWdodCcsXG5cdFx0XHRtaW5XaWR0aDogJzkwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEsXG5cdFx0XHRjbGFzc05hbWU6ICdudW1lcmljIGVkaXRhYmxlJ1xuXHRcdH07XG5cdFxuXHRleHBvcnRzLmNvbWJpU2hpcHBpbmdTdGF0dXNOYW1lID0gZXhwb3J0cy5jb21iaVNoaXBwaW5nU3RhdHVzTmFtZSB8fCB7XG5cdFx0XHRkYXRhOiAnY29tYmlTaGlwcGluZ1RpbWVJZCcsXG5cdFx0XHRtaW5XaWR0aDogJzEyMHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjEsXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSl7XG5cdFx0XHRcdGxldCBodG1sID0gJyc7XG5cdFx0XHRcdGNvbnN0IG9wdGlvbnMgPSBmdWxsLm9wdGlvbi5zaGlwbWVudDtcblx0XHRcdFx0XG5cdFx0XHRcdG9wdGlvbnMuZm9yRWFjaChvcHRpb24gPT4ge1xuXHRcdFx0XHRcdGh0bWwgKz0gYDxvcHRpb24gdmFsdWU9XCIke29wdGlvbi5pZH1cIiAke2Z1bGwuY29tYmlTaGlwcGluZ1RpbWVJZCA9PSBvcHRpb24uaWQgPyAnc2VsZWN0ZWQnIDogJyd9PlxuXHRcdFx0XHRcdFx0XHQke29wdGlvbi52YWx1ZX1cblx0XHRcdFx0XHRcdDwvb3B0aW9uPmA7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuIGA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sIHNlbGVjdC1wcm9wZXJ0aWVzLXNoaXBwaW5nLXRpbWVcIj4ke2h0bWx9PC9zZWxlY3Q+YDtcblx0XHRcdH1cblx0XHR9O1xuXHRcblx0XG5cdGV4cG9ydHMuYWN0aW9ucyA9IGV4cG9ydHMuYWN0aW9ucyB8fCB7XG5cdFx0XHRkYXRhOiBudWxsLFxuXHRcdFx0bWluV2lkdGg6ICc0MDBweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMy4yLFxuXHRcdFx0Y2xhc3NOYW1lOiAnYWN0aW9ucycsXG5cdFx0XHRvcmRlcmFibGU6IGZhbHNlLFxuXHRcdFx0c2VhcmNoYWJsZTogZmFsc2UsXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xuXHRcdFx0XHRyZXR1cm4gYFx0XHRcdFx0XHRcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicHVsbC1sZWZ0XCI+PC9kaXY+XG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cInB1bGwtcmlnaHQgYWN0aW9uLWxpc3QgdmlzaWJsZS1vbi1ob3ZlclwiPlxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwIGRyb3Bkb3duXCI+XG5cdFx0XHRcdFx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiXG5cdFx0XHRcdFx0XHRcdFx0XHRjbGFzcz1cImJ0biBidG4tZGVmYXVsdFwiPjwvYnV0dG9uPlxuXHRcdFx0XHRcdFx0XHQ8YnV0dG9uIHR5cGU9XCJidXR0b25cIlxuXHRcdFx0XHRcdFx0XHRcdFx0Y2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgZHJvcGRvd24tdG9nZ2xlXCJcblx0XHRcdFx0XHRcdFx0XHRcdGRhdGEtdG9nZ2xlPVwiZHJvcGRvd25cIlxuXHRcdFx0XHRcdFx0XHRcdFx0YXJpYS1oYXNwb3B1cD1cInRydWVcIlxuXHRcdFx0XHRcdFx0XHRcdFx0YXJpYS1leHBhbmRlZD1cImZhbHNlXCI+XG5cdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJjYXJldFwiPjwvc3Bhbj5cblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cInNyLW9ubHlcIj5Ub2dnbGUgRHJvcGRvd248L3NwYW4+XG5cdFx0XHRcdFx0XHRcdDwvYnV0dG9uPlxuXHRcdFx0XHRcdFx0XHQ8dWwgY2xhc3M9XCJkcm9wZG93bi1tZW51IGRyb3Bkb3duLW1lbnUtcmlnaHRcIj48L3VsPlxuXHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdGA7XG5cdFx0XHR9XG5cdFx0fTtcbn0pKGpzZS5saWJzLnF1aWNrX2VkaXRfcHJvcGVydGllc19vdmVydmlld19jb2x1bW5zKTsiXX0=
