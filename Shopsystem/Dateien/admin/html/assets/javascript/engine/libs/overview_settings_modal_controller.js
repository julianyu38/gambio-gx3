'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* --------------------------------------------------------------
 overview_settings_modal_controller.js 2017-12-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.overview_settings_modal_controller = jse.libs.overview_settings_modal_controller || {};

/**
 * Overview settings modal controller class.
 *
 * @module Admin/Libs/overview_settings_modal
 * @exports jse.libs.overview_settings_modal
 */
(function (exports) {
	/**
  * Class representing a controller for the orders overview settings modal.
  */
	var OverviewSettingsModalController = function () {
		/**
   * Creates an instance of OrdersOverviewSettingsModalController.
   *
   * @param {jQuery}    $element              Module element.
   * @param {Object}    userCfgService        User configuration service library.
   * @param {Object}    loadingSpinner        Loading spinner library.
   * @param {Number}    userId                ID of currently signed in user.
   * @param {String}    defaultColumnSettings Default column settings.
   * @param {Object}    translator            Translator library.
   * @param {String}    page                  Page name (e.g.: 'orders', 'invoices').
   */
		function OverviewSettingsModalController($element, userCfgService, loadingSpinner, userId, defaultColumnSettings, translator, page) {
			_classCallCheck(this, OverviewSettingsModalController);

			// Elements
			this.$element = $element;
			this.$submitButton = $element.find('button.submit-button');
			this.$settings = $element.find('ul.settings');
			this.$modal = $element.parents('.modal');
			this.$modalFooter = $element.find('.modal-footer');
			this.$resetDefaultLink = $element.find('a.reset-action');

			// Loading spinner
			this.$spinner = null;

			// Selector strings
			this.sortableHandleSelector = 'span.sort-handle';
			this.rowHeightValueSelector = 'select#setting-value-row-height';
			this.displayTooltipValueSelector = 'input#setting-value-display-tooltips';

			// Class names
			this.errorMessageClassName = 'error-message';
			this.loadingClassName = 'loading';

			// Libraries
			this.userCfgService = userCfgService;
			this.loadingSpinner = loadingSpinner;
			this.translator = translator;

			// Prefixes
			this.settingListItemIdPrefix = 'setting-';
			this.settingValueIdPrefix = 'setting-value-';

			// User configuration keys
			this.CONFIG_KEY_COLUMN_SETTINGS = page + 'OverviewSettingsColumns';
			this.CONFIG_KEY_ROW_HEIGHT_SETTINGS = page + 'OverviewSettingsRowHeight';
			this.CONFIG_KEY_DISPLAY_TOOLTIPS_SETTINGS = page + 'OverviewSettingsDisplayTooltips';

			// Default values
			this.DEFAULT_ROW_HEIGHT_SETTING = 'large';
			this.DEFAULT_COLUMN_SETTINGS = defaultColumnSettings.split(',');
			this.DEFAULT_DISPLAY_TOOLTIPS_SETTINGS = 'true';

			// ID of currently signed in user.
			this.userId = userId;
		}

		/**
   * Binds the event handlers.
   *
   * @return {OverviewSettingsModalController} Same instance for method chaining.
   */


		_createClass(OverviewSettingsModalController, [{
			key: 'initialize',
			value: function initialize() {
				var _this = this;

				// Attach event handler for click action on the submit button.
				this.$submitButton.on('click', function (event) {
					return _this._onSubmitButtonClick(event);
				});

				// Attach event handler for click action on the reset-default link.
				this.$resetDefaultLink.on('click', function (event) {
					return _this._onResetSettingsLinkClick(event);
				});

				// Attach event handlers to modal.
				this.$modal.on('show.bs.modal', function (event) {
					return _this._onModalShow(event);
				}).on('shown.bs.modal', function (event) {
					return _this._onModalShown(event);
				});

				return this;
			}

			/**
    * Fades out the modal content.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onModalShow',
			value: function _onModalShow() {
				this.$element.addClass(this.loadingClassName);

				return this;
			}

			/**
    * Updates the settings, clears any error messages and initializes the sortable plugin.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onModalShown',
			value: function _onModalShown() {
				this._refreshSettings()._clearErrorMessage()._initSortable();

				return this;
			}

			/**
    * Activates the jQuery UI Sortable plugin on the setting list items element.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_initSortable',
			value: function _initSortable() {
				// jQuery UI Sortable plugin options.
				var options = {
					items: '> li',
					axis: 'y',
					cursor: 'move',
					handle: this.sortableHandleSelector,
					containment: 'parent'
				};

				// Activate sortable plugin.
				this.$settings.sortable(options).disableSelection();

				return this;
			}

			/**
    * Returns a sorted array containing the IDs of all activated settings.
    *
    * @return {Array}
    *
    * @private
    */

		}, {
			key: '_serializeColumnSettings',
			value: function _serializeColumnSettings() {
				var _this2 = this;

				// Map iterator function to remove the 'setting-' prefix from list item ID.
				var removePrefixIterator = function removePrefixIterator(item) {
					return item.replace(_this2.settingListItemIdPrefix, '');
				};

				// Filter iterator function, to accept only list items with activated checkboxes.
				var filterIterator = function filterIterator(item) {
					return _this2.$settings.find('#' + _this2.settingValueIdPrefix + item).is(':checked');
				};

				// Return array with sorted, only active columns.
				return this.$settings.sortable('toArray').map(removePrefixIterator).filter(filterIterator);
			}

			/**
    * Returns the value of the selected row height option.
    *
    * @return {String}
    *
    * @private
    */

		}, {
			key: '_serializeRowHeightSetting',
			value: function _serializeRowHeightSetting() {
				return this.$element.find(this.rowHeightValueSelector).val();
			}

			/**
    * Returns the value of the selected tooltip display option.
    * 
    * @return {String}
    * 
    * @private
    */

		}, {
			key: '_serializeDisplayTooltipSetting',
			value: function _serializeDisplayTooltipSetting() {
				return this.$element.find(this.displayTooltipValueSelector).prop('checked');
			}

			/**
    * Shows the loading spinner, saves the settings to the user configuration,
    * closes the modal to finally re-render the datatable.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onSubmitButtonClick',
			value: function _onSubmitButtonClick() {
				var _this3 = this;

				// Retrieve setting values.
				var columnSettings = this._serializeColumnSettings();
				var rowHeightSetting = this._serializeRowHeightSetting();
				var displayTooltipSetting = this._serializeDisplayTooltipSetting();

				// Remove any error message and save settings.
				this._toggleLoadingSpinner(true)._clearErrorMessage()._saveColumnSettings(columnSettings).then(function () {
					return _this3._saveDisplayTooltipSetting(displayTooltipSetting);
				}).then(function () {
					return _this3._saveRowHeightSetting(rowHeightSetting);
				}).then(function () {
					return _this3._onSaveSuccess();
				}).catch(function () {
					return _this3._onSaveError();
				});

				return this;
			}

			/**
    * Prevents the browser to apply the default behavoir and
    * resets the column order and row size to the default setting values.
    *
    * @param {jQuery.Event} event Fired event.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onResetSettingsLinkClick',
			value: function _onResetSettingsLinkClick(event) {
				// Prevent default behavior.
				event.preventDefault();
				event.stopPropagation();

				// Reset to default settings.
				this._setDefaultSettings();

				return this;
			}

			/**
    * Shows and hides the loading spinner.
    *
    * @param {Boolean} doShow Show the loading spinner?
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    */

		}, {
			key: '_toggleLoadingSpinner',
			value: function _toggleLoadingSpinner(doShow) {
				if (doShow) {
					// Fade out modal content.
					this.$element.addClass(this.loadingClassName);

					// Show loading spinner.
					this.$spinner = this.loadingSpinner.show(this.$element);

					// Fix spinner z-index.
					this.$spinner.css({ 'z-index': 9999 });
				} else {
					// Fade out modal content.
					this.$element.removeClass(this.loadingClassName);

					// Hide the loading spinner.
					this.loadingSpinner.hide(this.$spinner);
				}

				return this;
			}

			/**
    * Handles the behavior on successful setting save action.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onSaveSuccess',
			value: function _onSaveSuccess() {
				window.location.reload();
				return this;
			}

			/**
    * Removes any error message, if found.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_clearErrorMessage',
			value: function _clearErrorMessage() {
				// Error message.
				var $errorMessage = this.$modalFooter.find('.' + this.errorMessageClassName);

				// Remove if it exists.
				if ($errorMessage.length) {
					$errorMessage.remove();
				}

				return this;
			}

			/**
    * Handles the behavior on thrown error while saving settings.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_onSaveError',
			value: function _onSaveError() {
				// Error message.
				var errorMessage = this.translator.translate('TXT_SAVE_ERROR', 'admin_general');

				// Define error message element.
				var $error = $('<span/>', { class: this.errorMessageClassName, text: errorMessage });

				// Hide the loading spinner.
				this._toggleLoadingSpinner(false);

				// Add error message to modal footer.
				this.$modalFooter.prepend($error).hide().fadeIn();

				return this;
			}

			/**
    * Returns the configuration value for the column settings.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_getColumnSettings',
			value: function _getColumnSettings() {
				// Configuration data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_COLUMN_SETTINGS
				};

				// Request data from user configuration service.
				return this._getFromUserCfgService(data);
			}

			/**
    * Returns the configuration value for the row heights.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_getRowHeightSetting',
			value: function _getRowHeightSetting() {
				// Configuration data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_ROW_HEIGHT_SETTINGS
				};

				// Request data from user configuration service.
				return this._getFromUserCfgService(data);
			}

			/**
    * Returns the configuration value for the tooltip display option.
    * 
    * @return {Promise}
    * 
    * @private
    */

		}, {
			key: '_getDisplayTooltipSetting',
			value: function _getDisplayTooltipSetting() {
				// Configuration data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_DISPLAY_TOOLTIPS_SETTINGS
				};

				// Request data from user configuration service.
				return this._getFromUserCfgService(data);
			}

			/**
    * Returns the value for the passed user configuration data.
    *
    * @param {Object} data                   User configuration data.
    * @param {Number} data.userId            User ID.
    * @param {String} data.configurationKey  User configuration key.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_getFromUserCfgService',
			value: function _getFromUserCfgService(data) {
				var _this4 = this;

				// Promise handler.
				var handler = function handler(resolve, reject) {
					// User configuration service request options.
					var options = {
						onError: function onError() {
							return reject();
						},
						onSuccess: function onSuccess(response) {
							return resolve(response.configurationValue);
						},
						data: data
					};

					// Get configuration value.
					_this4.userCfgService.get(options);
				};

				return new Promise(handler);
			}

			/**
    * Saves the data via the user configuration service.
    *
    * @param {Object} data                     User configuration data.
    * @param {Number} data.userId              User ID.
    * @param {String} data.configurationKey    User configuration key.
    * @param {String} data.configurationValue  User configuration value.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_setWithUserCfgService',
			value: function _setWithUserCfgService(data) {
				var _this5 = this;

				// Promise handler.
				var handler = function handler(resolve, reject) {
					// User configuration service request options.
					var options = {
						onError: function onError() {
							return reject();
						},
						onSuccess: function onSuccess(response) {
							return resolve();
						},
						data: data
					};

					// Set configuration value.
					_this5.userCfgService.set(options);
				};

				return new Promise(handler);
			}

			/**
    * Saves the column settings via the user configuration service.
    *
    * @param {String[]} columnSettings Sorted array with active column.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_saveColumnSettings',
			value: function _saveColumnSettings(columnSettings) {
				// Check argument.
				if (!columnSettings || !Array.isArray(columnSettings)) {
					throw new Error('Missing or invalid column settings');
				}

				// User configuration request data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_COLUMN_SETTINGS,
					configurationValue: JSON.stringify(columnSettings)
				};

				// Save via user configuration service.
				return this._setWithUserCfgService(data);
			}

			/**
    * Saves the row height setting via the user configuration service.
    *
    * @param {String} rowHeightSetting Value of the selected row height setting.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_saveRowHeightSetting',
			value: function _saveRowHeightSetting(rowHeightSetting) {
				// Check argument.
				if (!rowHeightSetting || typeof rowHeightSetting !== 'string') {
					throw new Error('Missing or invalid row height setting');
				}

				// User configuration request data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_ROW_HEIGHT_SETTINGS,
					configurationValue: rowHeightSetting
				};

				// Save via user configuration service.
				return this._setWithUserCfgService(data);
			}

			/**
    * Saves the display tooltip setting via the user configuration service.
    *
    * @param {String} displayTooltipSetting Value.
    *
    * @return {Promise}
    *
    * @private
    */

		}, {
			key: '_saveDisplayTooltipSetting',
			value: function _saveDisplayTooltipSetting(displayTooltipSetting) {
				// User configuration request data.
				var data = {
					userId: this.userId,
					configurationKey: this.CONFIG_KEY_DISPLAY_TOOLTIPS_SETTINGS,
					configurationValue: displayTooltipSetting
				};

				// Save via user configuration service.
				return this._setWithUserCfgService(data);
			}

			/**
    * Retrieves the saved setting configuration and reorders/updates the settings.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_refreshSettings',
			value: function _refreshSettings() {
				var _this6 = this;

				// Show loading spinner.
				this._toggleLoadingSpinner(true);

				// Error handler function to specify the behavior on errors while processing.
				var onRefreshSettingsError = function onRefreshSettingsError(error) {
					// Output warning.
					console.warn('Error while refreshing', error);

					// Hide the loading spinner.
					_this6._toggleLoadingSpinner(false);
				};

				// Remove any error message, set row height,
				// reorder and update the settings and hide the loading spinner.
				this._clearErrorMessage()._getRowHeightSetting().then(function (rowHeightValue) {
					return _this6._setRowHeight(rowHeightValue);
				}).then(function () {
					return _this6._getDisplayTooltipSetting();
				}).then(function (displayTooltipSetting) {
					return _this6._setDisplayTooltipSetting(displayTooltipSetting);
				}).then(function () {
					return _this6._getColumnSettings();
				}).then(function (columnSettings) {
					return _this6._setColumnSettings(columnSettings);
				}).then(function () {
					return _this6._toggleLoadingSpinner(false);
				}).catch(onRefreshSettingsError);

				return this;
			}

			/**
    * Sets the row height setting value.
    *
    * @param {String} value Row height value.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_setRowHeight',
			value: function _setRowHeight() {
				var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.DEFAULT_ROW_HEIGHT_SETTING;

				this.$element.find(this.rowHeightValueSelector).val(value);

				return this;
			}

			/**
    * Sets the display tooltips setting value.
    * 
    * @param {String} value Display tooltips value.
    * 
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    * 
    * @private
    */

		}, {
			key: '_setDisplayTooltipSetting',
			value: function _setDisplayTooltipSetting() {
				var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.DEFAULT_DISPLAY_TOOLTIPS_SETTINGS;

				this.$element.find(this.displayTooltipValueSelector).prop('checked', value === 'true').trigger('change');

				return this;
			}

			/**
    * Reorders and updates the column setting values.
    *
    * @param {String|Array} columnSettings Stringified JSON array containing the saved column settings.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_setColumnSettings',
			value: function _setColumnSettings() {
				var _this7 = this;

				var columnSettings = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.DEFAULT_COLUMN_SETTINGS;

				// Regex for escape character.
				var ESCAPE_CHAR = /\\/g;

				// No need to parse from JSON on default value as it is an array.
				if (!Array.isArray(columnSettings)) {
					// Remove escape characters from and parse array from JSON.
					columnSettings = columnSettings.replace(ESCAPE_CHAR, '');
					columnSettings = JSON.parse(columnSettings);
				}

				// Cache container to temporarily hold all active list items in sorted order.
				// The children of this element will be prepended to the setting list item container to retain the 
				// sorting order.
				var $sortedItems = $('<div/>');

				// Iterator function to prepend active list items to the top and activate the checkbox.
				var settingIterator = function settingIterator(setting) {
					// List item ID.
					var id = _this7.settingListItemIdPrefix + setting;

					// Affected setting list item.
					var $listItem = _this7.$settings.find('#' + id);

					// Checkbox of affected list item.
					var $checkbox = $listItem.find('#' + _this7.settingValueIdPrefix + setting);

					// Activate checkbox.
					if (!$checkbox.is(':checked')) {
						$checkbox.parent().trigger('click');
					}

					// Move to cache container.
					$listItem.appendTo($sortedItems);
				};

				// Move active list items to the top bearing the sorting order in mind.
				columnSettings.forEach(settingIterator);

				// Prepend cached elements to item list.
				$sortedItems.children().prependTo(this.$settings);

				return this;
			}

			/**
    * Resets the column order and row height settings to the default.
    *
    * @return {OverviewSettingsModalController} Same instance for method chaining.
    *
    * @private
    */

		}, {
			key: '_setDefaultSettings',
			value: function _setDefaultSettings() {
				var _this8 = this;

				// Default values.
				var columnSettings = this.DEFAULT_COLUMN_SETTINGS;
				var rowHeight = this.DEFAULT_ROW_HEIGHT_SETTING;

				// Set column settings.
				// Cache container to temporarily hold all active list items in sorted order.
				// The children of this element will be prepended to the setting list item container to retain the 
				// sorting order.
				var $sortedItems = $('<div/>');

				// Iterator function to prepend active list items to the top and activate the checkbox.
				var settingIterator = function settingIterator(setting) {
					// List item ID.
					var id = _this8.settingListItemIdPrefix + setting;

					// Affected setting list item.
					var $listItem = _this8.$settings.find('#' + id);

					// Checkbox of affected list item.
					var $checkbox = $listItem.find('#' + _this8.settingValueIdPrefix + setting);

					// Activate checkbox.
					if (!$checkbox.is(':checked')) {
						$checkbox.parent().trigger('click');
					}

					// Move to cache container.
					$listItem.appendTo($sortedItems);
				};

				// Deactivate all checkboxes.
				this.$settings.find(':checkbox').each(function (index, element) {
					var $checkbox = $(element);

					if ($checkbox.is(':checked')) {
						$checkbox.parent().trigger('click');
					}
				});

				// Move active list items to the top bearing the sorting order in mind.
				columnSettings.forEach(settingIterator);

				// Prepend cached elements to item list.
				$sortedItems.children().prependTo(this.$settings);

				// Set row height.
				this.$element.find(this.rowHeightValueSelector).val(rowHeight);

				return this;
			}
		}]);

		return OverviewSettingsModalController;
	}();

	exports.class = OverviewSettingsModalController;
})(jse.libs.overview_settings_modal_controller);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm92ZXJ2aWV3X3NldHRpbmdzX21vZGFsX2NvbnRyb2xsZXIuanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsIm92ZXJ2aWV3X3NldHRpbmdzX21vZGFsX2NvbnRyb2xsZXIiLCJleHBvcnRzIiwiT3ZlcnZpZXdTZXR0aW5nc01vZGFsQ29udHJvbGxlciIsIiRlbGVtZW50IiwidXNlckNmZ1NlcnZpY2UiLCJsb2FkaW5nU3Bpbm5lciIsInVzZXJJZCIsImRlZmF1bHRDb2x1bW5TZXR0aW5ncyIsInRyYW5zbGF0b3IiLCJwYWdlIiwiJHN1Ym1pdEJ1dHRvbiIsImZpbmQiLCIkc2V0dGluZ3MiLCIkbW9kYWwiLCJwYXJlbnRzIiwiJG1vZGFsRm9vdGVyIiwiJHJlc2V0RGVmYXVsdExpbmsiLCIkc3Bpbm5lciIsInNvcnRhYmxlSGFuZGxlU2VsZWN0b3IiLCJyb3dIZWlnaHRWYWx1ZVNlbGVjdG9yIiwiZGlzcGxheVRvb2x0aXBWYWx1ZVNlbGVjdG9yIiwiZXJyb3JNZXNzYWdlQ2xhc3NOYW1lIiwibG9hZGluZ0NsYXNzTmFtZSIsInNldHRpbmdMaXN0SXRlbUlkUHJlZml4Iiwic2V0dGluZ1ZhbHVlSWRQcmVmaXgiLCJDT05GSUdfS0VZX0NPTFVNTl9TRVRUSU5HUyIsIkNPTkZJR19LRVlfUk9XX0hFSUdIVF9TRVRUSU5HUyIsIkNPTkZJR19LRVlfRElTUExBWV9UT09MVElQU19TRVRUSU5HUyIsIkRFRkFVTFRfUk9XX0hFSUdIVF9TRVRUSU5HIiwiREVGQVVMVF9DT0xVTU5fU0VUVElOR1MiLCJzcGxpdCIsIkRFRkFVTFRfRElTUExBWV9UT09MVElQU19TRVRUSU5HUyIsIm9uIiwiX29uU3VibWl0QnV0dG9uQ2xpY2siLCJldmVudCIsIl9vblJlc2V0U2V0dGluZ3NMaW5rQ2xpY2siLCJfb25Nb2RhbFNob3ciLCJfb25Nb2RhbFNob3duIiwiYWRkQ2xhc3MiLCJfcmVmcmVzaFNldHRpbmdzIiwiX2NsZWFyRXJyb3JNZXNzYWdlIiwiX2luaXRTb3J0YWJsZSIsIm9wdGlvbnMiLCJpdGVtcyIsImF4aXMiLCJjdXJzb3IiLCJoYW5kbGUiLCJjb250YWlubWVudCIsInNvcnRhYmxlIiwiZGlzYWJsZVNlbGVjdGlvbiIsInJlbW92ZVByZWZpeEl0ZXJhdG9yIiwiaXRlbSIsInJlcGxhY2UiLCJmaWx0ZXJJdGVyYXRvciIsImlzIiwibWFwIiwiZmlsdGVyIiwidmFsIiwicHJvcCIsImNvbHVtblNldHRpbmdzIiwiX3NlcmlhbGl6ZUNvbHVtblNldHRpbmdzIiwicm93SGVpZ2h0U2V0dGluZyIsIl9zZXJpYWxpemVSb3dIZWlnaHRTZXR0aW5nIiwiZGlzcGxheVRvb2x0aXBTZXR0aW5nIiwiX3NlcmlhbGl6ZURpc3BsYXlUb29sdGlwU2V0dGluZyIsIl90b2dnbGVMb2FkaW5nU3Bpbm5lciIsIl9zYXZlQ29sdW1uU2V0dGluZ3MiLCJ0aGVuIiwiX3NhdmVEaXNwbGF5VG9vbHRpcFNldHRpbmciLCJfc2F2ZVJvd0hlaWdodFNldHRpbmciLCJfb25TYXZlU3VjY2VzcyIsImNhdGNoIiwiX29uU2F2ZUVycm9yIiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJfc2V0RGVmYXVsdFNldHRpbmdzIiwiZG9TaG93Iiwic2hvdyIsImNzcyIsInJlbW92ZUNsYXNzIiwiaGlkZSIsIndpbmRvdyIsImxvY2F0aW9uIiwicmVsb2FkIiwiJGVycm9yTWVzc2FnZSIsImxlbmd0aCIsInJlbW92ZSIsImVycm9yTWVzc2FnZSIsInRyYW5zbGF0ZSIsIiRlcnJvciIsIiQiLCJjbGFzcyIsInRleHQiLCJwcmVwZW5kIiwiZmFkZUluIiwiZGF0YSIsImNvbmZpZ3VyYXRpb25LZXkiLCJfZ2V0RnJvbVVzZXJDZmdTZXJ2aWNlIiwiaGFuZGxlciIsInJlc29sdmUiLCJyZWplY3QiLCJvbkVycm9yIiwib25TdWNjZXNzIiwicmVzcG9uc2UiLCJjb25maWd1cmF0aW9uVmFsdWUiLCJnZXQiLCJQcm9taXNlIiwic2V0IiwiQXJyYXkiLCJpc0FycmF5IiwiRXJyb3IiLCJKU09OIiwic3RyaW5naWZ5IiwiX3NldFdpdGhVc2VyQ2ZnU2VydmljZSIsIm9uUmVmcmVzaFNldHRpbmdzRXJyb3IiLCJjb25zb2xlIiwid2FybiIsImVycm9yIiwiX2dldFJvd0hlaWdodFNldHRpbmciLCJfc2V0Um93SGVpZ2h0Iiwicm93SGVpZ2h0VmFsdWUiLCJfZ2V0RGlzcGxheVRvb2x0aXBTZXR0aW5nIiwiX3NldERpc3BsYXlUb29sdGlwU2V0dGluZyIsIl9nZXRDb2x1bW5TZXR0aW5ncyIsIl9zZXRDb2x1bW5TZXR0aW5ncyIsInZhbHVlIiwidHJpZ2dlciIsIkVTQ0FQRV9DSEFSIiwicGFyc2UiLCIkc29ydGVkSXRlbXMiLCJzZXR0aW5nSXRlcmF0b3IiLCJpZCIsInNldHRpbmciLCIkbGlzdEl0ZW0iLCIkY2hlY2tib3giLCJwYXJlbnQiLCJhcHBlbmRUbyIsImZvckVhY2giLCJjaGlsZHJlbiIsInByZXBlbmRUbyIsInJvd0hlaWdodCIsImVhY2giLCJpbmRleCIsImVsZW1lbnQiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOzs7Ozs7Ozs7O0FBVUFBLElBQUlDLElBQUosQ0FBU0Msa0NBQVQsR0FBOENGLElBQUlDLElBQUosQ0FBU0Msa0NBQVQsSUFBK0MsRUFBN0Y7O0FBRUE7Ozs7OztBQU1DLFdBQVNDLE9BQVQsRUFBa0I7QUFDbEI7OztBQURrQixLQUlaQywrQkFKWTtBQUtqQjs7Ozs7Ozs7Ozs7QUFXQSwyQ0FBWUMsUUFBWixFQUFzQkMsY0FBdEIsRUFBc0NDLGNBQXRDLEVBQXNEQyxNQUF0RCxFQUE4REMscUJBQTlELEVBQXFGQyxVQUFyRixFQUFpR0MsSUFBakcsRUFBdUc7QUFBQTs7QUFDdEc7QUFDQSxRQUFLTixRQUFMLEdBQWdCQSxRQUFoQjtBQUNBLFFBQUtPLGFBQUwsR0FBcUJQLFNBQVNRLElBQVQsQ0FBYyxzQkFBZCxDQUFyQjtBQUNBLFFBQUtDLFNBQUwsR0FBaUJULFNBQVNRLElBQVQsQ0FBYyxhQUFkLENBQWpCO0FBQ0EsUUFBS0UsTUFBTCxHQUFjVixTQUFTVyxPQUFULENBQWlCLFFBQWpCLENBQWQ7QUFDQSxRQUFLQyxZQUFMLEdBQW9CWixTQUFTUSxJQUFULENBQWMsZUFBZCxDQUFwQjtBQUNBLFFBQUtLLGlCQUFMLEdBQXlCYixTQUFTUSxJQUFULENBQWMsZ0JBQWQsQ0FBekI7O0FBRUE7QUFDQSxRQUFLTSxRQUFMLEdBQWdCLElBQWhCOztBQUVBO0FBQ0EsUUFBS0Msc0JBQUwsR0FBOEIsa0JBQTlCO0FBQ0EsUUFBS0Msc0JBQUwsR0FBOEIsaUNBQTlCO0FBQ0EsUUFBS0MsMkJBQUwsR0FBbUMsc0NBQW5DOztBQUVBO0FBQ0EsUUFBS0MscUJBQUwsR0FBNkIsZUFBN0I7QUFDQSxRQUFLQyxnQkFBTCxHQUF3QixTQUF4Qjs7QUFFQTtBQUNBLFFBQUtsQixjQUFMLEdBQXNCQSxjQUF0QjtBQUNBLFFBQUtDLGNBQUwsR0FBc0JBLGNBQXRCO0FBQ0EsUUFBS0csVUFBTCxHQUFrQkEsVUFBbEI7O0FBRUE7QUFDQSxRQUFLZSx1QkFBTCxHQUErQixVQUEvQjtBQUNBLFFBQUtDLG9CQUFMLEdBQTRCLGdCQUE1Qjs7QUFFQTtBQUNBLFFBQUtDLDBCQUFMLEdBQXFDaEIsSUFBckM7QUFDQSxRQUFLaUIsOEJBQUwsR0FBeUNqQixJQUF6QztBQUNBLFFBQUtrQixvQ0FBTCxHQUErQ2xCLElBQS9DOztBQUVBO0FBQ0EsUUFBS21CLDBCQUFMLEdBQWtDLE9BQWxDO0FBQ0EsUUFBS0MsdUJBQUwsR0FBK0J0QixzQkFBc0J1QixLQUF0QixDQUE0QixHQUE1QixDQUEvQjtBQUNBLFFBQUtDLGlDQUFMLEdBQXlDLE1BQXpDOztBQUVBO0FBQ0EsUUFBS3pCLE1BQUwsR0FBY0EsTUFBZDtBQUNBOztBQUVEOzs7Ozs7O0FBNURpQjtBQUFBO0FBQUEsZ0NBaUVKO0FBQUE7O0FBQ1o7QUFDQSxTQUFLSSxhQUFMLENBQW1Cc0IsRUFBbkIsQ0FBc0IsT0FBdEIsRUFBK0I7QUFBQSxZQUFTLE1BQUtDLG9CQUFMLENBQTBCQyxLQUExQixDQUFUO0FBQUEsS0FBL0I7O0FBRUE7QUFDQSxTQUFLbEIsaUJBQUwsQ0FBdUJnQixFQUF2QixDQUEwQixPQUExQixFQUFtQztBQUFBLFlBQVMsTUFBS0cseUJBQUwsQ0FBK0JELEtBQS9CLENBQVQ7QUFBQSxLQUFuQzs7QUFFQTtBQUNBLFNBQUtyQixNQUFMLENBQ0VtQixFQURGLENBQ0ssZUFETCxFQUNzQjtBQUFBLFlBQVMsTUFBS0ksWUFBTCxDQUFrQkYsS0FBbEIsQ0FBVDtBQUFBLEtBRHRCLEVBRUVGLEVBRkYsQ0FFSyxnQkFGTCxFQUV1QjtBQUFBLFlBQVMsTUFBS0ssYUFBTCxDQUFtQkgsS0FBbkIsQ0FBVDtBQUFBLEtBRnZCOztBQUlBLFdBQU8sSUFBUDtBQUNBOztBQUVEOzs7Ozs7OztBQWhGaUI7QUFBQTtBQUFBLGtDQXVGRjtBQUNkLFNBQUsvQixRQUFMLENBQWNtQyxRQUFkLENBQXVCLEtBQUtoQixnQkFBNUI7O0FBRUEsV0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBN0ZpQjtBQUFBO0FBQUEsbUNBb0dEO0FBQ2YsU0FDRWlCLGdCQURGLEdBRUVDLGtCQUZGLEdBR0VDLGFBSEY7O0FBS0EsV0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBN0dpQjtBQUFBO0FBQUEsbUNBb0hEO0FBQ2Y7QUFDQSxRQUFNQyxVQUFVO0FBQ2ZDLFlBQU8sTUFEUTtBQUVmQyxXQUFNLEdBRlM7QUFHZkMsYUFBUSxNQUhPO0FBSWZDLGFBQVEsS0FBSzVCLHNCQUpFO0FBS2Y2QixrQkFBYTtBQUxFLEtBQWhCOztBQVFBO0FBQ0EsU0FBS25DLFNBQUwsQ0FDRW9DLFFBREYsQ0FDV04sT0FEWCxFQUVFTyxnQkFGRjs7QUFJQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUF0SWlCO0FBQUE7QUFBQSw4Q0E2SVU7QUFBQTs7QUFDMUI7QUFDQSxRQUFNQyx1QkFBdUIsU0FBdkJBLG9CQUF1QjtBQUFBLFlBQVFDLEtBQUtDLE9BQUwsQ0FBYSxPQUFLN0IsdUJBQWxCLEVBQTJDLEVBQTNDLENBQVI7QUFBQSxLQUE3Qjs7QUFFQTtBQUNBLFFBQU04QixpQkFBaUIsU0FBakJBLGNBQWlCO0FBQUEsWUFBUSxPQUFLekMsU0FBTCxDQUFlRCxJQUFmLENBQW9CLE1BQU0sT0FBS2Esb0JBQVgsR0FBa0MyQixJQUF0RCxFQUM3QkcsRUFENkIsQ0FDMUIsVUFEMEIsQ0FBUjtBQUFBLEtBQXZCOztBQUdBO0FBQ0EsV0FBTyxLQUFLMUMsU0FBTCxDQUNMb0MsUUFESyxDQUNJLFNBREosRUFFTE8sR0FGSyxDQUVETCxvQkFGQyxFQUdMTSxNQUhLLENBR0VILGNBSEYsQ0FBUDtBQUlBOztBQUVEOzs7Ozs7OztBQTVKaUI7QUFBQTtBQUFBLGdEQW1LWTtBQUM1QixXQUFPLEtBQ0xsRCxRQURLLENBRUxRLElBRkssQ0FFQSxLQUFLUSxzQkFGTCxFQUdMc0MsR0FISyxFQUFQO0FBSUE7O0FBRUQ7Ozs7Ozs7O0FBMUtpQjtBQUFBO0FBQUEscURBaUxpQjtBQUNqQyxXQUFPLEtBQ0x0RCxRQURLLENBRUxRLElBRkssQ0FFQSxLQUFLUywyQkFGTCxFQUdMc0MsSUFISyxDQUdBLFNBSEEsQ0FBUDtBQUlBOztBQUVEOzs7Ozs7Ozs7QUF4TGlCO0FBQUE7QUFBQSwwQ0FnTU07QUFBQTs7QUFDdEI7QUFDQSxRQUFNQyxpQkFBaUIsS0FBS0Msd0JBQUwsRUFBdkI7QUFDQSxRQUFNQyxtQkFBbUIsS0FBS0MsMEJBQUwsRUFBekI7QUFDQSxRQUFNQyx3QkFBd0IsS0FBS0MsK0JBQUwsRUFBOUI7O0FBRUE7QUFDQSxTQUNFQyxxQkFERixDQUN3QixJQUR4QixFQUVFekIsa0JBRkYsR0FHRTBCLG1CQUhGLENBR3NCUCxjQUh0QixFQUlFUSxJQUpGLENBSU87QUFBQSxZQUFNLE9BQUtDLDBCQUFMLENBQWdDTCxxQkFBaEMsQ0FBTjtBQUFBLEtBSlAsRUFLRUksSUFMRixDQUtPO0FBQUEsWUFBTSxPQUFLRSxxQkFBTCxDQUEyQlIsZ0JBQTNCLENBQU47QUFBQSxLQUxQLEVBTUVNLElBTkYsQ0FNTztBQUFBLFlBQU0sT0FBS0csY0FBTCxFQUFOO0FBQUEsS0FOUCxFQU9FQyxLQVBGLENBT1E7QUFBQSxZQUFNLE9BQUtDLFlBQUwsRUFBTjtBQUFBLEtBUFI7O0FBU0EsV0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7O0FBbk5pQjtBQUFBO0FBQUEsNkNBNk5TdEMsS0E3TlQsRUE2TmdCO0FBQ2hDO0FBQ0FBLFVBQU11QyxjQUFOO0FBQ0F2QyxVQUFNd0MsZUFBTjs7QUFFQTtBQUNBLFNBQUtDLG1CQUFMOztBQUVBLFdBQU8sSUFBUDtBQUNBOztBQUVEOzs7Ozs7OztBQXhPaUI7QUFBQTtBQUFBLHlDQStPS0MsTUEvT0wsRUErT2E7QUFDN0IsUUFBSUEsTUFBSixFQUFZO0FBQ1g7QUFDQSxVQUFLekUsUUFBTCxDQUFjbUMsUUFBZCxDQUF1QixLQUFLaEIsZ0JBQTVCOztBQUVBO0FBQ0EsVUFBS0wsUUFBTCxHQUFnQixLQUFLWixjQUFMLENBQW9Cd0UsSUFBcEIsQ0FBeUIsS0FBSzFFLFFBQTlCLENBQWhCOztBQUVBO0FBQ0EsVUFBS2MsUUFBTCxDQUFjNkQsR0FBZCxDQUFrQixFQUFDLFdBQVcsSUFBWixFQUFsQjtBQUNBLEtBVEQsTUFTTztBQUNOO0FBQ0EsVUFBSzNFLFFBQUwsQ0FBYzRFLFdBQWQsQ0FBMEIsS0FBS3pELGdCQUEvQjs7QUFFQTtBQUNBLFVBQUtqQixjQUFMLENBQW9CMkUsSUFBcEIsQ0FBeUIsS0FBSy9ELFFBQTlCO0FBQ0E7O0FBRUQsV0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBcFFpQjtBQUFBO0FBQUEsb0NBMlFBO0FBQ2hCZ0UsV0FBT0MsUUFBUCxDQUFnQkMsTUFBaEI7QUFDQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUFoUmlCO0FBQUE7QUFBQSx3Q0F1Ukk7QUFDcEI7QUFDQSxRQUFNQyxnQkFBZ0IsS0FBS3JFLFlBQUwsQ0FBa0JKLElBQWxCLE9BQTJCLEtBQUtVLHFCQUFoQyxDQUF0Qjs7QUFFQTtBQUNBLFFBQUkrRCxjQUFjQyxNQUFsQixFQUEwQjtBQUN6QkQsbUJBQWNFLE1BQWQ7QUFDQTs7QUFFRCxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUFuU2lCO0FBQUE7QUFBQSxrQ0EwU0Y7QUFDZDtBQUNBLFFBQU1DLGVBQWUsS0FBSy9FLFVBQUwsQ0FBZ0JnRixTQUFoQixDQUEwQixnQkFBMUIsRUFBNEMsZUFBNUMsQ0FBckI7O0FBRUE7QUFDQSxRQUFNQyxTQUFTQyxFQUFFLFNBQUYsRUFBYSxFQUFDQyxPQUFPLEtBQUt0RSxxQkFBYixFQUFvQ3VFLE1BQU1MLFlBQTFDLEVBQWIsQ0FBZjs7QUFFQTtBQUNBLFNBQUt0QixxQkFBTCxDQUEyQixLQUEzQjs7QUFFQTtBQUNBLFNBQUtsRCxZQUFMLENBQ0U4RSxPQURGLENBQ1VKLE1BRFYsRUFFRVQsSUFGRixHQUdFYyxNQUhGOztBQUtBLFdBQU8sSUFBUDtBQUNBOztBQUVEOzs7Ozs7OztBQTdUaUI7QUFBQTtBQUFBLHdDQW9VSTtBQUNwQjtBQUNBLFFBQU1DLE9BQU87QUFDWnpGLGFBQVEsS0FBS0EsTUFERDtBQUVaMEYsdUJBQWtCLEtBQUt2RTtBQUZYLEtBQWI7O0FBS0E7QUFDQSxXQUFPLEtBQUt3RSxzQkFBTCxDQUE0QkYsSUFBNUIsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7OztBQS9VaUI7QUFBQTtBQUFBLDBDQXNWTTtBQUN0QjtBQUNBLFFBQU1BLE9BQU87QUFDWnpGLGFBQVEsS0FBS0EsTUFERDtBQUVaMEYsdUJBQWtCLEtBQUt0RTtBQUZYLEtBQWI7O0FBS0E7QUFDQSxXQUFPLEtBQUt1RSxzQkFBTCxDQUE0QkYsSUFBNUIsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7OztBQWpXaUI7QUFBQTtBQUFBLCtDQXdXVztBQUMzQjtBQUNBLFFBQU1BLE9BQU87QUFDWnpGLGFBQVEsS0FBS0EsTUFERDtBQUVaMEYsdUJBQWtCLEtBQUtyRTtBQUZYLEtBQWI7O0FBS0E7QUFDQSxXQUFPLEtBQUtzRSxzQkFBTCxDQUE0QkYsSUFBNUIsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7Ozs7QUFuWGlCO0FBQUE7QUFBQSwwQ0E4WE1BLElBOVhOLEVBOFhZO0FBQUE7O0FBQzVCO0FBQ0EsUUFBTUcsVUFBVSxTQUFWQSxPQUFVLENBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNwQztBQUNBLFNBQU0xRCxVQUFVO0FBQ2YyRCxlQUFTO0FBQUEsY0FBTUQsUUFBTjtBQUFBLE9BRE07QUFFZkUsaUJBQVc7QUFBQSxjQUFZSCxRQUFRSSxTQUFTQyxrQkFBakIsQ0FBWjtBQUFBLE9BRkk7QUFHZlQ7QUFIZSxNQUFoQjs7QUFNQTtBQUNBLFlBQUszRixjQUFMLENBQW9CcUcsR0FBcEIsQ0FBd0IvRCxPQUF4QjtBQUNBLEtBVkQ7O0FBWUEsV0FBTyxJQUFJZ0UsT0FBSixDQUFZUixPQUFaLENBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7Ozs7OztBQS9ZaUI7QUFBQTtBQUFBLDBDQTJaTUgsSUEzWk4sRUEyWlk7QUFBQTs7QUFDNUI7QUFDQSxRQUFNRyxVQUFVLFNBQVZBLE9BQVUsQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3BDO0FBQ0EsU0FBTTFELFVBQVU7QUFDZjJELGVBQVM7QUFBQSxjQUFNRCxRQUFOO0FBQUEsT0FETTtBQUVmRSxpQkFBVztBQUFBLGNBQVlILFNBQVo7QUFBQSxPQUZJO0FBR2ZKO0FBSGUsTUFBaEI7O0FBTUE7QUFDQSxZQUFLM0YsY0FBTCxDQUFvQnVHLEdBQXBCLENBQXdCakUsT0FBeEI7QUFDQSxLQVZEOztBQVlBLFdBQU8sSUFBSWdFLE9BQUosQ0FBWVIsT0FBWixDQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7QUE1YWlCO0FBQUE7QUFBQSx1Q0FxYkd2QyxjQXJiSCxFQXFibUI7QUFDbkM7QUFDQSxRQUFJLENBQUNBLGNBQUQsSUFBbUIsQ0FBQ2lELE1BQU1DLE9BQU4sQ0FBY2xELGNBQWQsQ0FBeEIsRUFBdUQ7QUFDdEQsV0FBTSxJQUFJbUQsS0FBSixDQUFVLG9DQUFWLENBQU47QUFDQTs7QUFFRDtBQUNBLFFBQU1mLE9BQU87QUFDWnpGLGFBQVEsS0FBS0EsTUFERDtBQUVaMEYsdUJBQWtCLEtBQUt2RSwwQkFGWDtBQUdaK0UseUJBQW9CTyxLQUFLQyxTQUFMLENBQWVyRCxjQUFmO0FBSFIsS0FBYjs7QUFNQTtBQUNBLFdBQU8sS0FBS3NELHNCQUFMLENBQTRCbEIsSUFBNUIsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7O0FBdGNpQjtBQUFBO0FBQUEseUNBK2NLbEMsZ0JBL2NMLEVBK2N1QjtBQUN2QztBQUNBLFFBQUksQ0FBQ0EsZ0JBQUQsSUFBcUIsT0FBT0EsZ0JBQVAsS0FBNEIsUUFBckQsRUFBK0Q7QUFDOUQsV0FBTSxJQUFJaUQsS0FBSixDQUFVLHVDQUFWLENBQU47QUFDQTs7QUFFRDtBQUNBLFFBQU1mLE9BQU87QUFDWnpGLGFBQVEsS0FBS0EsTUFERDtBQUVaMEYsdUJBQWtCLEtBQUt0RSw4QkFGWDtBQUdaOEUseUJBQW9CM0M7QUFIUixLQUFiOztBQU1BO0FBQ0EsV0FBTyxLQUFLb0Qsc0JBQUwsQ0FBNEJsQixJQUE1QixDQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7QUFoZWlCO0FBQUE7QUFBQSw4Q0F5ZVVoQyxxQkF6ZVYsRUF5ZWlDO0FBQ2pEO0FBQ0EsUUFBTWdDLE9BQU87QUFDWnpGLGFBQVEsS0FBS0EsTUFERDtBQUVaMEYsdUJBQWtCLEtBQUtyRSxvQ0FGWDtBQUdaNkUseUJBQW9CekM7QUFIUixLQUFiOztBQU1BO0FBQ0EsV0FBTyxLQUFLa0Qsc0JBQUwsQ0FBNEJsQixJQUE1QixDQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBcmZpQjtBQUFBO0FBQUEsc0NBNGZFO0FBQUE7O0FBQ2xCO0FBQ0EsU0FBSzlCLHFCQUFMLENBQTJCLElBQTNCOztBQUVBO0FBQ0EsUUFBTWlELHlCQUF5QixTQUF6QkEsc0JBQXlCLFFBQVM7QUFDdkM7QUFDQUMsYUFBUUMsSUFBUixDQUFhLHdCQUFiLEVBQXVDQyxLQUF2Qzs7QUFFQTtBQUNBLFlBQUtwRCxxQkFBTCxDQUEyQixLQUEzQjtBQUNBLEtBTkQ7O0FBUUE7QUFDQTtBQUNBLFNBQ0V6QixrQkFERixHQUVFOEUsb0JBRkYsR0FHRW5ELElBSEYsQ0FHTztBQUFBLFlBQWtCLE9BQUtvRCxhQUFMLENBQW1CQyxjQUFuQixDQUFsQjtBQUFBLEtBSFAsRUFJRXJELElBSkYsQ0FJTztBQUFBLFlBQU0sT0FBS3NELHlCQUFMLEVBQU47QUFBQSxLQUpQLEVBS0V0RCxJQUxGLENBS087QUFBQSxZQUF5QixPQUFLdUQseUJBQUwsQ0FBK0IzRCxxQkFBL0IsQ0FBekI7QUFBQSxLQUxQLEVBTUVJLElBTkYsQ0FNTztBQUFBLFlBQU0sT0FBS3dELGtCQUFMLEVBQU47QUFBQSxLQU5QLEVBT0V4RCxJQVBGLENBT087QUFBQSxZQUFrQixPQUFLeUQsa0JBQUwsQ0FBd0JqRSxjQUF4QixDQUFsQjtBQUFBLEtBUFAsRUFRRVEsSUFSRixDQVFPO0FBQUEsWUFBTSxPQUFLRixxQkFBTCxDQUEyQixLQUEzQixDQUFOO0FBQUEsS0FSUCxFQVNFTSxLQVRGLENBU1EyQyxzQkFUUjs7QUFXQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7OztBQXpoQmlCO0FBQUE7QUFBQSxtQ0FraUJzQztBQUFBLFFBQXpDVyxLQUF5Qyx1RUFBakMsS0FBS2pHLDBCQUE0Qjs7QUFDdEQsU0FDRXpCLFFBREYsQ0FFRVEsSUFGRixDQUVPLEtBQUtRLHNCQUZaLEVBR0VzQyxHQUhGLENBR01vRSxLQUhOOztBQUtBLFdBQU8sSUFBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7O0FBM2lCaUI7QUFBQTtBQUFBLCtDQW9qQnlEO0FBQUEsUUFBaERBLEtBQWdELHVFQUF4QyxLQUFLOUYsaUNBQW1DOztBQUN6RSxTQUNFNUIsUUFERixDQUVFUSxJQUZGLENBRU8sS0FBS1MsMkJBRlosRUFHRXNDLElBSEYsQ0FHTyxTQUhQLEVBR2tCbUUsVUFBVSxNQUg1QixFQUlFQyxPQUpGLENBSVUsUUFKVjs7QUFNQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7OztBQTlqQmlCO0FBQUE7QUFBQSx3Q0F1a0JpRDtBQUFBOztBQUFBLFFBQS9DbkUsY0FBK0MsdUVBQTlCLEtBQUs5Qix1QkFBeUI7O0FBQ2pFO0FBQ0EsUUFBTWtHLGNBQWMsS0FBcEI7O0FBRUE7QUFDQSxRQUFJLENBQUNuQixNQUFNQyxPQUFOLENBQWNsRCxjQUFkLENBQUwsRUFBb0M7QUFDbkM7QUFDQUEsc0JBQWlCQSxlQUFlUCxPQUFmLENBQXVCMkUsV0FBdkIsRUFBb0MsRUFBcEMsQ0FBakI7QUFDQXBFLHNCQUFpQm9ELEtBQUtpQixLQUFMLENBQVdyRSxjQUFYLENBQWpCO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EsUUFBTXNFLGVBQWV2QyxFQUFFLFFBQUYsQ0FBckI7O0FBRUE7QUFDQSxRQUFNd0Msa0JBQWtCLFNBQWxCQSxlQUFrQixVQUFXO0FBQ2xDO0FBQ0EsU0FBTUMsS0FBSyxPQUFLNUcsdUJBQUwsR0FBK0I2RyxPQUExQzs7QUFFQTtBQUNBLFNBQU1DLFlBQVksT0FBS3pILFNBQUwsQ0FBZUQsSUFBZixPQUF3QndILEVBQXhCLENBQWxCOztBQUVBO0FBQ0EsU0FBTUcsWUFBWUQsVUFBVTFILElBQVYsQ0FBZSxNQUFNLE9BQUthLG9CQUFYLEdBQWtDNEcsT0FBakQsQ0FBbEI7O0FBRUE7QUFDQSxTQUFJLENBQUNFLFVBQVVoRixFQUFWLENBQWEsVUFBYixDQUFMLEVBQStCO0FBQzlCZ0YsZ0JBQVVDLE1BQVYsR0FBbUJULE9BQW5CLENBQTJCLE9BQTNCO0FBQ0E7O0FBRUQ7QUFDQU8sZUFBVUcsUUFBVixDQUFtQlAsWUFBbkI7QUFDQSxLQWpCRDs7QUFtQkE7QUFDQXRFLG1CQUFlOEUsT0FBZixDQUF1QlAsZUFBdkI7O0FBRUE7QUFDQUQsaUJBQ0VTLFFBREYsR0FFRUMsU0FGRixDQUVZLEtBQUsvSCxTQUZqQjs7QUFJQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUF0bkJpQjtBQUFBO0FBQUEseUNBNm5CSztBQUFBOztBQUNyQjtBQUNBLFFBQU0rQyxpQkFBaUIsS0FBSzlCLHVCQUE1QjtBQUNBLFFBQU0rRyxZQUFZLEtBQUtoSCwwQkFBdkI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFNcUcsZUFBZXZDLEVBQUUsUUFBRixDQUFyQjs7QUFFQTtBQUNBLFFBQU13QyxrQkFBa0IsU0FBbEJBLGVBQWtCLFVBQVc7QUFDbEM7QUFDQSxTQUFNQyxLQUFLLE9BQUs1Ryx1QkFBTCxHQUErQjZHLE9BQTFDOztBQUVBO0FBQ0EsU0FBTUMsWUFBWSxPQUFLekgsU0FBTCxDQUFlRCxJQUFmLE9BQXdCd0gsRUFBeEIsQ0FBbEI7O0FBRUE7QUFDQSxTQUFNRyxZQUFZRCxVQUFVMUgsSUFBVixDQUFlLE1BQU0sT0FBS2Esb0JBQVgsR0FBa0M0RyxPQUFqRCxDQUFsQjs7QUFFQTtBQUNBLFNBQUksQ0FBQ0UsVUFBVWhGLEVBQVYsQ0FBYSxVQUFiLENBQUwsRUFBK0I7QUFDOUJnRixnQkFBVUMsTUFBVixHQUFtQlQsT0FBbkIsQ0FBMkIsT0FBM0I7QUFDQTs7QUFFRDtBQUNBTyxlQUFVRyxRQUFWLENBQW1CUCxZQUFuQjtBQUNBLEtBakJEOztBQW1CQTtBQUNBLFNBQ0VySCxTQURGLENBRUVELElBRkYsQ0FFTyxXQUZQLEVBR0VrSSxJQUhGLENBR08sVUFBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQW9CO0FBQ3pCLFNBQU1ULFlBQVk1QyxFQUFFcUQsT0FBRixDQUFsQjs7QUFFQSxTQUFJVCxVQUFVaEYsRUFBVixDQUFhLFVBQWIsQ0FBSixFQUE4QjtBQUM3QmdGLGdCQUFVQyxNQUFWLEdBQW1CVCxPQUFuQixDQUEyQixPQUEzQjtBQUNBO0FBQ0QsS0FURjs7QUFXQTtBQUNBbkUsbUJBQWU4RSxPQUFmLENBQXVCUCxlQUF2Qjs7QUFFQTtBQUNBRCxpQkFDRVMsUUFERixHQUVFQyxTQUZGLENBRVksS0FBSy9ILFNBRmpCOztBQUlBO0FBQ0EsU0FDRVQsUUFERixDQUVFUSxJQUZGLENBRU8sS0FBS1Esc0JBRlosRUFHRXNDLEdBSEYsQ0FHTW1GLFNBSE47O0FBS0EsV0FBTyxJQUFQO0FBQ0E7QUF2ckJnQjs7QUFBQTtBQUFBOztBQTByQmxCM0ksU0FBUTBGLEtBQVIsR0FBZ0J6RiwrQkFBaEI7QUFDQSxDQTNyQkEsRUEyckJDSixJQUFJQyxJQUFKLENBQVNDLGtDQTNyQlYsQ0FBRCIsImZpbGUiOiJvdmVydmlld19zZXR0aW5nc19tb2RhbF9jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBvdmVydmlld19zZXR0aW5nc19tb2RhbF9jb250cm9sbGVyLmpzIDIwMTctMTItMDZcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UubGlicy5vdmVydmlld19zZXR0aW5nc19tb2RhbF9jb250cm9sbGVyID0ganNlLmxpYnMub3ZlcnZpZXdfc2V0dGluZ3NfbW9kYWxfY29udHJvbGxlciB8fCB7fTtcblxuLyoqXG4gKiBPdmVydmlldyBzZXR0aW5ncyBtb2RhbCBjb250cm9sbGVyIGNsYXNzLlxuICpcbiAqIEBtb2R1bGUgQWRtaW4vTGlicy9vdmVydmlld19zZXR0aW5nc19tb2RhbFxuICogQGV4cG9ydHMganNlLmxpYnMub3ZlcnZpZXdfc2V0dGluZ3NfbW9kYWxcbiAqL1xuKGZ1bmN0aW9uKGV4cG9ydHMpIHtcblx0LyoqXG5cdCAqIENsYXNzIHJlcHJlc2VudGluZyBhIGNvbnRyb2xsZXIgZm9yIHRoZSBvcmRlcnMgb3ZlcnZpZXcgc2V0dGluZ3MgbW9kYWwuXG5cdCAqL1xuXHRjbGFzcyBPdmVydmlld1NldHRpbmdzTW9kYWxDb250cm9sbGVyIHtcblx0XHQvKipcblx0XHQgKiBDcmVhdGVzIGFuIGluc3RhbmNlIG9mIE9yZGVyc092ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXIuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gICAgJGVsZW1lbnQgICAgICAgICAgICAgIE1vZHVsZSBlbGVtZW50LlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSAgICB1c2VyQ2ZnU2VydmljZSAgICAgICAgVXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UgbGlicmFyeS5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gICAgbG9hZGluZ1NwaW5uZXIgICAgICAgIExvYWRpbmcgc3Bpbm5lciBsaWJyYXJ5LlxuXHRcdCAqIEBwYXJhbSB7TnVtYmVyfSAgICB1c2VySWQgICAgICAgICAgICAgICAgSUQgb2YgY3VycmVudGx5IHNpZ25lZCBpbiB1c2VyLlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSAgICBkZWZhdWx0Q29sdW1uU2V0dGluZ3MgRGVmYXVsdCBjb2x1bW4gc2V0dGluZ3MuXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9ICAgIHRyYW5zbGF0b3IgICAgICAgICAgICBUcmFuc2xhdG9yIGxpYnJhcnkuXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9ICAgIHBhZ2UgICAgICAgICAgICAgICAgICBQYWdlIG5hbWUgKGUuZy46ICdvcmRlcnMnLCAnaW52b2ljZXMnKS5cblx0XHQgKi9cblx0XHRjb25zdHJ1Y3RvcigkZWxlbWVudCwgdXNlckNmZ1NlcnZpY2UsIGxvYWRpbmdTcGlubmVyLCB1c2VySWQsIGRlZmF1bHRDb2x1bW5TZXR0aW5ncywgdHJhbnNsYXRvciwgcGFnZSkge1xuXHRcdFx0Ly8gRWxlbWVudHNcblx0XHRcdHRoaXMuJGVsZW1lbnQgPSAkZWxlbWVudDtcblx0XHRcdHRoaXMuJHN1Ym1pdEJ1dHRvbiA9ICRlbGVtZW50LmZpbmQoJ2J1dHRvbi5zdWJtaXQtYnV0dG9uJyk7XG5cdFx0XHR0aGlzLiRzZXR0aW5ncyA9ICRlbGVtZW50LmZpbmQoJ3VsLnNldHRpbmdzJyk7XG5cdFx0XHR0aGlzLiRtb2RhbCA9ICRlbGVtZW50LnBhcmVudHMoJy5tb2RhbCcpO1xuXHRcdFx0dGhpcy4kbW9kYWxGb290ZXIgPSAkZWxlbWVudC5maW5kKCcubW9kYWwtZm9vdGVyJyk7XG5cdFx0XHR0aGlzLiRyZXNldERlZmF1bHRMaW5rID0gJGVsZW1lbnQuZmluZCgnYS5yZXNldC1hY3Rpb24nKTtcblx0XHRcdFxuXHRcdFx0Ly8gTG9hZGluZyBzcGlubmVyXG5cdFx0XHR0aGlzLiRzcGlubmVyID0gbnVsbDtcblx0XHRcdFxuXHRcdFx0Ly8gU2VsZWN0b3Igc3RyaW5nc1xuXHRcdFx0dGhpcy5zb3J0YWJsZUhhbmRsZVNlbGVjdG9yID0gJ3NwYW4uc29ydC1oYW5kbGUnO1xuXHRcdFx0dGhpcy5yb3dIZWlnaHRWYWx1ZVNlbGVjdG9yID0gJ3NlbGVjdCNzZXR0aW5nLXZhbHVlLXJvdy1oZWlnaHQnO1xuXHRcdFx0dGhpcy5kaXNwbGF5VG9vbHRpcFZhbHVlU2VsZWN0b3IgPSAnaW5wdXQjc2V0dGluZy12YWx1ZS1kaXNwbGF5LXRvb2x0aXBzJztcblx0XHRcdFxuXHRcdFx0Ly8gQ2xhc3MgbmFtZXNcblx0XHRcdHRoaXMuZXJyb3JNZXNzYWdlQ2xhc3NOYW1lID0gJ2Vycm9yLW1lc3NhZ2UnO1xuXHRcdFx0dGhpcy5sb2FkaW5nQ2xhc3NOYW1lID0gJ2xvYWRpbmcnO1xuXHRcdFx0XG5cdFx0XHQvLyBMaWJyYXJpZXNcblx0XHRcdHRoaXMudXNlckNmZ1NlcnZpY2UgPSB1c2VyQ2ZnU2VydmljZTtcblx0XHRcdHRoaXMubG9hZGluZ1NwaW5uZXIgPSBsb2FkaW5nU3Bpbm5lcjtcblx0XHRcdHRoaXMudHJhbnNsYXRvciA9IHRyYW5zbGF0b3I7XG5cdFx0XHRcblx0XHRcdC8vIFByZWZpeGVzXG5cdFx0XHR0aGlzLnNldHRpbmdMaXN0SXRlbUlkUHJlZml4ID0gJ3NldHRpbmctJztcblx0XHRcdHRoaXMuc2V0dGluZ1ZhbHVlSWRQcmVmaXggPSAnc2V0dGluZy12YWx1ZS0nO1xuXHRcdFx0XG5cdFx0XHQvLyBVc2VyIGNvbmZpZ3VyYXRpb24ga2V5c1xuXHRcdFx0dGhpcy5DT05GSUdfS0VZX0NPTFVNTl9TRVRUSU5HUyA9IGAke3BhZ2V9T3ZlcnZpZXdTZXR0aW5nc0NvbHVtbnNgO1xuXHRcdFx0dGhpcy5DT05GSUdfS0VZX1JPV19IRUlHSFRfU0VUVElOR1MgPSBgJHtwYWdlfU92ZXJ2aWV3U2V0dGluZ3NSb3dIZWlnaHRgO1xuXHRcdFx0dGhpcy5DT05GSUdfS0VZX0RJU1BMQVlfVE9PTFRJUFNfU0VUVElOR1MgPSBgJHtwYWdlfU92ZXJ2aWV3U2V0dGluZ3NEaXNwbGF5VG9vbHRpcHNgO1xuXHRcdFx0XG5cdFx0XHQvLyBEZWZhdWx0IHZhbHVlc1xuXHRcdFx0dGhpcy5ERUZBVUxUX1JPV19IRUlHSFRfU0VUVElORyA9ICdsYXJnZSc7XG5cdFx0XHR0aGlzLkRFRkFVTFRfQ09MVU1OX1NFVFRJTkdTID0gZGVmYXVsdENvbHVtblNldHRpbmdzLnNwbGl0KCcsJyk7XG5cdFx0XHR0aGlzLkRFRkFVTFRfRElTUExBWV9UT09MVElQU19TRVRUSU5HUyA9ICd0cnVlJztcblx0XHRcdFxuXHRcdFx0Ly8gSUQgb2YgY3VycmVudGx5IHNpZ25lZCBpbiB1c2VyLlxuXHRcdFx0dGhpcy51c2VySWQgPSB1c2VySWQ7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEJpbmRzIHRoZSBldmVudCBoYW5kbGVycy5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge092ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHQgKi9cblx0XHRpbml0aWFsaXplKCkge1xuXHRcdFx0Ly8gQXR0YWNoIGV2ZW50IGhhbmRsZXIgZm9yIGNsaWNrIGFjdGlvbiBvbiB0aGUgc3VibWl0IGJ1dHRvbi5cblx0XHRcdHRoaXMuJHN1Ym1pdEJ1dHRvbi5vbignY2xpY2snLCBldmVudCA9PiB0aGlzLl9vblN1Ym1pdEJ1dHRvbkNsaWNrKGV2ZW50KSk7XG5cdFx0XHRcblx0XHRcdC8vIEF0dGFjaCBldmVudCBoYW5kbGVyIGZvciBjbGljayBhY3Rpb24gb24gdGhlIHJlc2V0LWRlZmF1bHQgbGluay5cblx0XHRcdHRoaXMuJHJlc2V0RGVmYXVsdExpbmsub24oJ2NsaWNrJywgZXZlbnQgPT4gdGhpcy5fb25SZXNldFNldHRpbmdzTGlua0NsaWNrKGV2ZW50KSk7XG5cdFx0XHRcblx0XHRcdC8vIEF0dGFjaCBldmVudCBoYW5kbGVycyB0byBtb2RhbC5cblx0XHRcdHRoaXMuJG1vZGFsXG5cdFx0XHRcdC5vbignc2hvdy5icy5tb2RhbCcsIGV2ZW50ID0+IHRoaXMuX29uTW9kYWxTaG93KGV2ZW50KSlcblx0XHRcdFx0Lm9uKCdzaG93bi5icy5tb2RhbCcsIGV2ZW50ID0+IHRoaXMuX29uTW9kYWxTaG93bihldmVudCkpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmFkZXMgb3V0IHRoZSBtb2RhbCBjb250ZW50LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T3ZlcnZpZXdTZXR0aW5nc01vZGFsQ29udHJvbGxlcn0gU2FtZSBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRfb25Nb2RhbFNob3coKSB7XG5cdFx0XHR0aGlzLiRlbGVtZW50LmFkZENsYXNzKHRoaXMubG9hZGluZ0NsYXNzTmFtZSk7XG5cdFx0XHRcblx0XHRcdHJldHVybiB0aGlzO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBVcGRhdGVzIHRoZSBzZXR0aW5ncywgY2xlYXJzIGFueSBlcnJvciBtZXNzYWdlcyBhbmQgaW5pdGlhbGl6ZXMgdGhlIHNvcnRhYmxlIHBsdWdpbi5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge092ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X29uTW9kYWxTaG93bigpIHtcblx0XHRcdHRoaXNcblx0XHRcdFx0Ll9yZWZyZXNoU2V0dGluZ3MoKVxuXHRcdFx0XHQuX2NsZWFyRXJyb3JNZXNzYWdlKClcblx0XHRcdFx0Ll9pbml0U29ydGFibGUoKTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFjdGl2YXRlcyB0aGUgalF1ZXJ5IFVJIFNvcnRhYmxlIHBsdWdpbiBvbiB0aGUgc2V0dGluZyBsaXN0IGl0ZW1zIGVsZW1lbnQuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPdmVydmlld1NldHRpbmdzTW9kYWxDb250cm9sbGVyfSBTYW1lIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdF9pbml0U29ydGFibGUoKSB7XG5cdFx0XHQvLyBqUXVlcnkgVUkgU29ydGFibGUgcGx1Z2luIG9wdGlvbnMuXG5cdFx0XHRjb25zdCBvcHRpb25zID0ge1xuXHRcdFx0XHRpdGVtczogJz4gbGknLFxuXHRcdFx0XHRheGlzOiAneScsXG5cdFx0XHRcdGN1cnNvcjogJ21vdmUnLFxuXHRcdFx0XHRoYW5kbGU6IHRoaXMuc29ydGFibGVIYW5kbGVTZWxlY3Rvcixcblx0XHRcdFx0Y29udGFpbm1lbnQ6ICdwYXJlbnQnXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBBY3RpdmF0ZSBzb3J0YWJsZSBwbHVnaW4uXG5cdFx0XHR0aGlzLiRzZXR0aW5nc1xuXHRcdFx0XHQuc29ydGFibGUob3B0aW9ucylcblx0XHRcdFx0LmRpc2FibGVTZWxlY3Rpb24oKTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJldHVybnMgYSBzb3J0ZWQgYXJyYXkgY29udGFpbmluZyB0aGUgSURzIG9mIGFsbCBhY3RpdmF0ZWQgc2V0dGluZ3MuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtBcnJheX1cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X3NlcmlhbGl6ZUNvbHVtblNldHRpbmdzKCkge1xuXHRcdFx0Ly8gTWFwIGl0ZXJhdG9yIGZ1bmN0aW9uIHRvIHJlbW92ZSB0aGUgJ3NldHRpbmctJyBwcmVmaXggZnJvbSBsaXN0IGl0ZW0gSUQuXG5cdFx0XHRjb25zdCByZW1vdmVQcmVmaXhJdGVyYXRvciA9IGl0ZW0gPT4gaXRlbS5yZXBsYWNlKHRoaXMuc2V0dGluZ0xpc3RJdGVtSWRQcmVmaXgsICcnKTtcblx0XHRcdFxuXHRcdFx0Ly8gRmlsdGVyIGl0ZXJhdG9yIGZ1bmN0aW9uLCB0byBhY2NlcHQgb25seSBsaXN0IGl0ZW1zIHdpdGggYWN0aXZhdGVkIGNoZWNrYm94ZXMuXG5cdFx0XHRjb25zdCBmaWx0ZXJJdGVyYXRvciA9IGl0ZW0gPT4gdGhpcy4kc2V0dGluZ3MuZmluZCgnIycgKyB0aGlzLnNldHRpbmdWYWx1ZUlkUHJlZml4ICsgaXRlbSlcblx0XHRcdFx0LmlzKCc6Y2hlY2tlZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZXR1cm4gYXJyYXkgd2l0aCBzb3J0ZWQsIG9ubHkgYWN0aXZlIGNvbHVtbnMuXG5cdFx0XHRyZXR1cm4gdGhpcy4kc2V0dGluZ3Ncblx0XHRcdFx0LnNvcnRhYmxlKCd0b0FycmF5Jylcblx0XHRcdFx0Lm1hcChyZW1vdmVQcmVmaXhJdGVyYXRvcilcblx0XHRcdFx0LmZpbHRlcihmaWx0ZXJJdGVyYXRvcik7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJldHVybnMgdGhlIHZhbHVlIG9mIHRoZSBzZWxlY3RlZCByb3cgaGVpZ2h0IG9wdGlvbi5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge1N0cmluZ31cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X3NlcmlhbGl6ZVJvd0hlaWdodFNldHRpbmcoKSB7XG5cdFx0XHRyZXR1cm4gdGhpc1xuXHRcdFx0XHQuJGVsZW1lbnRcblx0XHRcdFx0LmZpbmQodGhpcy5yb3dIZWlnaHRWYWx1ZVNlbGVjdG9yKVxuXHRcdFx0XHQudmFsKCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJldHVybnMgdGhlIHZhbHVlIG9mIHRoZSBzZWxlY3RlZCB0b29sdGlwIGRpc3BsYXkgb3B0aW9uLlxuXHRcdCAqIFxuXHRcdCAqIEByZXR1cm4ge1N0cmluZ31cblx0XHQgKiBcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdF9zZXJpYWxpemVEaXNwbGF5VG9vbHRpcFNldHRpbmcoKSB7XG5cdFx0XHRyZXR1cm4gdGhpc1xuXHRcdFx0XHQuJGVsZW1lbnRcblx0XHRcdFx0LmZpbmQodGhpcy5kaXNwbGF5VG9vbHRpcFZhbHVlU2VsZWN0b3IpXG5cdFx0XHRcdC5wcm9wKCdjaGVja2VkJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNob3dzIHRoZSBsb2FkaW5nIHNwaW5uZXIsIHNhdmVzIHRoZSBzZXR0aW5ncyB0byB0aGUgdXNlciBjb25maWd1cmF0aW9uLFxuXHRcdCAqIGNsb3NlcyB0aGUgbW9kYWwgdG8gZmluYWxseSByZS1yZW5kZXIgdGhlIGRhdGF0YWJsZS5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge092ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X29uU3VibWl0QnV0dG9uQ2xpY2soKSB7XG5cdFx0XHQvLyBSZXRyaWV2ZSBzZXR0aW5nIHZhbHVlcy5cblx0XHRcdGNvbnN0IGNvbHVtblNldHRpbmdzID0gdGhpcy5fc2VyaWFsaXplQ29sdW1uU2V0dGluZ3MoKTtcblx0XHRcdGNvbnN0IHJvd0hlaWdodFNldHRpbmcgPSB0aGlzLl9zZXJpYWxpemVSb3dIZWlnaHRTZXR0aW5nKCk7XG5cdFx0XHRjb25zdCBkaXNwbGF5VG9vbHRpcFNldHRpbmcgPSB0aGlzLl9zZXJpYWxpemVEaXNwbGF5VG9vbHRpcFNldHRpbmcoKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVtb3ZlIGFueSBlcnJvciBtZXNzYWdlIGFuZCBzYXZlIHNldHRpbmdzLlxuXHRcdFx0dGhpc1xuXHRcdFx0XHQuX3RvZ2dsZUxvYWRpbmdTcGlubmVyKHRydWUpXG5cdFx0XHRcdC5fY2xlYXJFcnJvck1lc3NhZ2UoKVxuXHRcdFx0XHQuX3NhdmVDb2x1bW5TZXR0aW5ncyhjb2x1bW5TZXR0aW5ncylcblx0XHRcdFx0LnRoZW4oKCkgPT4gdGhpcy5fc2F2ZURpc3BsYXlUb29sdGlwU2V0dGluZyhkaXNwbGF5VG9vbHRpcFNldHRpbmcpKVxuXHRcdFx0XHQudGhlbigoKSA9PiB0aGlzLl9zYXZlUm93SGVpZ2h0U2V0dGluZyhyb3dIZWlnaHRTZXR0aW5nKSlcblx0XHRcdFx0LnRoZW4oKCkgPT4gdGhpcy5fb25TYXZlU3VjY2VzcygpKVxuXHRcdFx0XHQuY2F0Y2goKCkgPT4gdGhpcy5fb25TYXZlRXJyb3IoKSk7XG5cdFx0XHRcblx0XHRcdHJldHVybiB0aGlzO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBQcmV2ZW50cyB0aGUgYnJvd3NlciB0byBhcHBseSB0aGUgZGVmYXVsdCBiZWhhdm9pciBhbmRcblx0XHQgKiByZXNldHMgdGhlIGNvbHVtbiBvcmRlciBhbmQgcm93IHNpemUgdG8gdGhlIGRlZmF1bHQgc2V0dGluZyB2YWx1ZXMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgRmlyZWQgZXZlbnQuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPdmVydmlld1NldHRpbmdzTW9kYWxDb250cm9sbGVyfSBTYW1lIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdF9vblJlc2V0U2V0dGluZ3NMaW5rQ2xpY2soZXZlbnQpIHtcblx0XHRcdC8vIFByZXZlbnQgZGVmYXVsdCBiZWhhdmlvci5cblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVzZXQgdG8gZGVmYXVsdCBzZXR0aW5ncy5cblx0XHRcdHRoaXMuX3NldERlZmF1bHRTZXR0aW5ncygpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2hvd3MgYW5kIGhpZGVzIHRoZSBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge0Jvb2xlYW59IGRvU2hvdyBTaG93IHRoZSBsb2FkaW5nIHNwaW5uZXI/XG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPdmVydmlld1NldHRpbmdzTW9kYWxDb250cm9sbGVyfSBTYW1lIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG5cdFx0ICovXG5cdFx0X3RvZ2dsZUxvYWRpbmdTcGlubmVyKGRvU2hvdykge1xuXHRcdFx0aWYgKGRvU2hvdykge1xuXHRcdFx0XHQvLyBGYWRlIG91dCBtb2RhbCBjb250ZW50LlxuXHRcdFx0XHR0aGlzLiRlbGVtZW50LmFkZENsYXNzKHRoaXMubG9hZGluZ0NsYXNzTmFtZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTaG93IGxvYWRpbmcgc3Bpbm5lci5cblx0XHRcdFx0dGhpcy4kc3Bpbm5lciA9IHRoaXMubG9hZGluZ1NwaW5uZXIuc2hvdyh0aGlzLiRlbGVtZW50KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEZpeCBzcGlubmVyIHotaW5kZXguXG5cdFx0XHRcdHRoaXMuJHNwaW5uZXIuY3NzKHsnei1pbmRleCc6IDk5OTl9KTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdC8vIEZhZGUgb3V0IG1vZGFsIGNvbnRlbnQuXG5cdFx0XHRcdHRoaXMuJGVsZW1lbnQucmVtb3ZlQ2xhc3ModGhpcy5sb2FkaW5nQ2xhc3NOYW1lKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEhpZGUgdGhlIGxvYWRpbmcgc3Bpbm5lci5cblx0XHRcdFx0dGhpcy5sb2FkaW5nU3Bpbm5lci5oaWRlKHRoaXMuJHNwaW5uZXIpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgYmVoYXZpb3Igb24gc3VjY2Vzc2Z1bCBzZXR0aW5nIHNhdmUgYWN0aW9uLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T3ZlcnZpZXdTZXR0aW5nc01vZGFsQ29udHJvbGxlcn0gU2FtZSBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRfb25TYXZlU3VjY2VzcygpIHtcblx0XHRcdHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKTtcblx0XHRcdHJldHVybiB0aGlzO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZW1vdmVzIGFueSBlcnJvciBtZXNzYWdlLCBpZiBmb3VuZC5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge092ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X2NsZWFyRXJyb3JNZXNzYWdlKCkge1xuXHRcdFx0Ly8gRXJyb3IgbWVzc2FnZS5cblx0XHRcdGNvbnN0ICRlcnJvck1lc3NhZ2UgPSB0aGlzLiRtb2RhbEZvb3Rlci5maW5kKGAuJHt0aGlzLmVycm9yTWVzc2FnZUNsYXNzTmFtZX1gKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVtb3ZlIGlmIGl0IGV4aXN0cy5cblx0XHRcdGlmICgkZXJyb3JNZXNzYWdlLmxlbmd0aCkge1xuXHRcdFx0XHQkZXJyb3JNZXNzYWdlLnJlbW92ZSgpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUgYmVoYXZpb3Igb24gdGhyb3duIGVycm9yIHdoaWxlIHNhdmluZyBzZXR0aW5ncy5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge092ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X29uU2F2ZUVycm9yKCkge1xuXHRcdFx0Ly8gRXJyb3IgbWVzc2FnZS5cblx0XHRcdGNvbnN0IGVycm9yTWVzc2FnZSA9IHRoaXMudHJhbnNsYXRvci50cmFuc2xhdGUoJ1RYVF9TQVZFX0VSUk9SJywgJ2FkbWluX2dlbmVyYWwnKTtcblx0XHRcdFxuXHRcdFx0Ly8gRGVmaW5lIGVycm9yIG1lc3NhZ2UgZWxlbWVudC5cblx0XHRcdGNvbnN0ICRlcnJvciA9ICQoJzxzcGFuLz4nLCB7Y2xhc3M6IHRoaXMuZXJyb3JNZXNzYWdlQ2xhc3NOYW1lLCB0ZXh0OiBlcnJvck1lc3NhZ2V9KTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSB0aGUgbG9hZGluZyBzcGlubmVyLlxuXHRcdFx0dGhpcy5fdG9nZ2xlTG9hZGluZ1NwaW5uZXIoZmFsc2UpO1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgZXJyb3IgbWVzc2FnZSB0byBtb2RhbCBmb290ZXIuXG5cdFx0XHR0aGlzLiRtb2RhbEZvb3RlclxuXHRcdFx0XHQucHJlcGVuZCgkZXJyb3IpXG5cdFx0XHRcdC5oaWRlKClcblx0XHRcdFx0LmZhZGVJbigpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgY29uZmlndXJhdGlvbiB2YWx1ZSBmb3IgdGhlIGNvbHVtbiBzZXR0aW5ncy5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge1Byb21pc2V9XG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdF9nZXRDb2x1bW5TZXR0aW5ncygpIHtcblx0XHRcdC8vIENvbmZpZ3VyYXRpb24gZGF0YS5cblx0XHRcdGNvbnN0IGRhdGEgPSB7XG5cdFx0XHRcdHVzZXJJZDogdGhpcy51c2VySWQsXG5cdFx0XHRcdGNvbmZpZ3VyYXRpb25LZXk6IHRoaXMuQ09ORklHX0tFWV9DT0xVTU5fU0VUVElOR1Ncblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIFJlcXVlc3QgZGF0YSBmcm9tIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlLlxuXHRcdFx0cmV0dXJuIHRoaXMuX2dldEZyb21Vc2VyQ2ZnU2VydmljZShkYXRhKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgY29uZmlndXJhdGlvbiB2YWx1ZSBmb3IgdGhlIHJvdyBoZWlnaHRzLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7UHJvbWlzZX1cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X2dldFJvd0hlaWdodFNldHRpbmcoKSB7XG5cdFx0XHQvLyBDb25maWd1cmF0aW9uIGRhdGEuXG5cdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHR1c2VySWQ6IHRoaXMudXNlcklkLFxuXHRcdFx0XHRjb25maWd1cmF0aW9uS2V5OiB0aGlzLkNPTkZJR19LRVlfUk9XX0hFSUdIVF9TRVRUSU5HU1xuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVxdWVzdCBkYXRhIGZyb20gdXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UuXG5cdFx0XHRyZXR1cm4gdGhpcy5fZ2V0RnJvbVVzZXJDZmdTZXJ2aWNlKGRhdGEpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXR1cm5zIHRoZSBjb25maWd1cmF0aW9uIHZhbHVlIGZvciB0aGUgdG9vbHRpcCBkaXNwbGF5IG9wdGlvbi5cblx0XHQgKiBcblx0XHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHRcdCAqIFxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X2dldERpc3BsYXlUb29sdGlwU2V0dGluZygpIHtcblx0XHRcdC8vIENvbmZpZ3VyYXRpb24gZGF0YS5cblx0XHRcdGNvbnN0IGRhdGEgPSB7XG5cdFx0XHRcdHVzZXJJZDogdGhpcy51c2VySWQsXG5cdFx0XHRcdGNvbmZpZ3VyYXRpb25LZXk6IHRoaXMuQ09ORklHX0tFWV9ESVNQTEFZX1RPT0xUSVBTX1NFVFRJTkdTXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBSZXF1ZXN0IGRhdGEgZnJvbSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZS5cblx0XHRcdHJldHVybiB0aGlzLl9nZXRGcm9tVXNlckNmZ1NlcnZpY2UoZGF0YSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJldHVybnMgdGhlIHZhbHVlIGZvciB0aGUgcGFzc2VkIHVzZXIgY29uZmlndXJhdGlvbiBkYXRhLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGRhdGEgICAgICAgICAgICAgICAgICAgVXNlciBjb25maWd1cmF0aW9uIGRhdGEuXG5cdFx0ICogQHBhcmFtIHtOdW1iZXJ9IGRhdGEudXNlcklkICAgICAgICAgICAgVXNlciBJRC5cblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YS5jb25maWd1cmF0aW9uS2V5ICBVc2VyIGNvbmZpZ3VyYXRpb24ga2V5LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7UHJvbWlzZX1cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X2dldEZyb21Vc2VyQ2ZnU2VydmljZShkYXRhKSB7XG5cdFx0XHQvLyBQcm9taXNlIGhhbmRsZXIuXG5cdFx0XHRjb25zdCBoYW5kbGVyID0gKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0XHQvLyBVc2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZSByZXF1ZXN0IG9wdGlvbnMuXG5cdFx0XHRcdGNvbnN0IG9wdGlvbnMgPSB7XG5cdFx0XHRcdFx0b25FcnJvcjogKCkgPT4gcmVqZWN0KCksXG5cdFx0XHRcdFx0b25TdWNjZXNzOiByZXNwb25zZSA9PiByZXNvbHZlKHJlc3BvbnNlLmNvbmZpZ3VyYXRpb25WYWx1ZSksXG5cdFx0XHRcdFx0ZGF0YVxuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gR2V0IGNvbmZpZ3VyYXRpb24gdmFsdWUuXG5cdFx0XHRcdHRoaXMudXNlckNmZ1NlcnZpY2UuZ2V0KG9wdGlvbnMpO1xuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIG5ldyBQcm9taXNlKGhhbmRsZXIpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTYXZlcyB0aGUgZGF0YSB2aWEgdGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IGRhdGEgICAgICAgICAgICAgICAgICAgICBVc2VyIGNvbmZpZ3VyYXRpb24gZGF0YS5cblx0XHQgKiBAcGFyYW0ge051bWJlcn0gZGF0YS51c2VySWQgICAgICAgICAgICAgIFVzZXIgSUQuXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGRhdGEuY29uZmlndXJhdGlvbktleSAgICBVc2VyIGNvbmZpZ3VyYXRpb24ga2V5LlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSBkYXRhLmNvbmZpZ3VyYXRpb25WYWx1ZSAgVXNlciBjb25maWd1cmF0aW9uIHZhbHVlLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7UHJvbWlzZX1cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X3NldFdpdGhVc2VyQ2ZnU2VydmljZShkYXRhKSB7XG5cdFx0XHQvLyBQcm9taXNlIGhhbmRsZXIuXG5cdFx0XHRjb25zdCBoYW5kbGVyID0gKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0XHQvLyBVc2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZSByZXF1ZXN0IG9wdGlvbnMuXG5cdFx0XHRcdGNvbnN0IG9wdGlvbnMgPSB7XG5cdFx0XHRcdFx0b25FcnJvcjogKCkgPT4gcmVqZWN0KCksXG5cdFx0XHRcdFx0b25TdWNjZXNzOiByZXNwb25zZSA9PiByZXNvbHZlKCksXG5cdFx0XHRcdFx0ZGF0YVxuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2V0IGNvbmZpZ3VyYXRpb24gdmFsdWUuXG5cdFx0XHRcdHRoaXMudXNlckNmZ1NlcnZpY2Uuc2V0KG9wdGlvbnMpO1xuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIG5ldyBQcm9taXNlKGhhbmRsZXIpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTYXZlcyB0aGUgY29sdW1uIHNldHRpbmdzIHZpYSB0aGUgdXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge1N0cmluZ1tdfSBjb2x1bW5TZXR0aW5ncyBTb3J0ZWQgYXJyYXkgd2l0aCBhY3RpdmUgY29sdW1uLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7UHJvbWlzZX1cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X3NhdmVDb2x1bW5TZXR0aW5ncyhjb2x1bW5TZXR0aW5ncykge1xuXHRcdFx0Ly8gQ2hlY2sgYXJndW1lbnQuXG5cdFx0XHRpZiAoIWNvbHVtblNldHRpbmdzIHx8ICFBcnJheS5pc0FycmF5KGNvbHVtblNldHRpbmdzKSkge1xuXHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ01pc3Npbmcgb3IgaW52YWxpZCBjb2x1bW4gc2V0dGluZ3MnKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gVXNlciBjb25maWd1cmF0aW9uIHJlcXVlc3QgZGF0YS5cblx0XHRcdGNvbnN0IGRhdGEgPSB7XG5cdFx0XHRcdHVzZXJJZDogdGhpcy51c2VySWQsXG5cdFx0XHRcdGNvbmZpZ3VyYXRpb25LZXk6IHRoaXMuQ09ORklHX0tFWV9DT0xVTU5fU0VUVElOR1MsXG5cdFx0XHRcdGNvbmZpZ3VyYXRpb25WYWx1ZTogSlNPTi5zdHJpbmdpZnkoY29sdW1uU2V0dGluZ3MpXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBTYXZlIHZpYSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZS5cblx0XHRcdHJldHVybiB0aGlzLl9zZXRXaXRoVXNlckNmZ1NlcnZpY2UoZGF0YSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNhdmVzIHRoZSByb3cgaGVpZ2h0IHNldHRpbmcgdmlhIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSByb3dIZWlnaHRTZXR0aW5nIFZhbHVlIG9mIHRoZSBzZWxlY3RlZCByb3cgaGVpZ2h0IHNldHRpbmcuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRfc2F2ZVJvd0hlaWdodFNldHRpbmcocm93SGVpZ2h0U2V0dGluZykge1xuXHRcdFx0Ly8gQ2hlY2sgYXJndW1lbnQuXG5cdFx0XHRpZiAoIXJvd0hlaWdodFNldHRpbmcgfHwgdHlwZW9mIHJvd0hlaWdodFNldHRpbmcgIT09ICdzdHJpbmcnKSB7XG5cdFx0XHRcdHRocm93IG5ldyBFcnJvcignTWlzc2luZyBvciBpbnZhbGlkIHJvdyBoZWlnaHQgc2V0dGluZycpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBVc2VyIGNvbmZpZ3VyYXRpb24gcmVxdWVzdCBkYXRhLlxuXHRcdFx0Y29uc3QgZGF0YSA9IHtcblx0XHRcdFx0dXNlcklkOiB0aGlzLnVzZXJJZCxcblx0XHRcdFx0Y29uZmlndXJhdGlvbktleTogdGhpcy5DT05GSUdfS0VZX1JPV19IRUlHSFRfU0VUVElOR1MsXG5cdFx0XHRcdGNvbmZpZ3VyYXRpb25WYWx1ZTogcm93SGVpZ2h0U2V0dGluZ1xuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gU2F2ZSB2aWEgdXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UuXG5cdFx0XHRyZXR1cm4gdGhpcy5fc2V0V2l0aFVzZXJDZmdTZXJ2aWNlKGRhdGEpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTYXZlcyB0aGUgZGlzcGxheSB0b29sdGlwIHNldHRpbmcgdmlhIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSBkaXNwbGF5VG9vbHRpcFNldHRpbmcgVmFsdWUuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRfc2F2ZURpc3BsYXlUb29sdGlwU2V0dGluZyhkaXNwbGF5VG9vbHRpcFNldHRpbmcpIHtcblx0XHRcdC8vIFVzZXIgY29uZmlndXJhdGlvbiByZXF1ZXN0IGRhdGEuXG5cdFx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0XHR1c2VySWQ6IHRoaXMudXNlcklkLFxuXHRcdFx0XHRjb25maWd1cmF0aW9uS2V5OiB0aGlzLkNPTkZJR19LRVlfRElTUExBWV9UT09MVElQU19TRVRUSU5HUyxcblx0XHRcdFx0Y29uZmlndXJhdGlvblZhbHVlOiBkaXNwbGF5VG9vbHRpcFNldHRpbmdcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIFNhdmUgdmlhIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlLlxuXHRcdFx0cmV0dXJuIHRoaXMuX3NldFdpdGhVc2VyQ2ZnU2VydmljZShkYXRhKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0cmlldmVzIHRoZSBzYXZlZCBzZXR0aW5nIGNvbmZpZ3VyYXRpb24gYW5kIHJlb3JkZXJzL3VwZGF0ZXMgdGhlIHNldHRpbmdzLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T3ZlcnZpZXdTZXR0aW5nc01vZGFsQ29udHJvbGxlcn0gU2FtZSBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRfcmVmcmVzaFNldHRpbmdzKCkge1xuXHRcdFx0Ly8gU2hvdyBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHR0aGlzLl90b2dnbGVMb2FkaW5nU3Bpbm5lcih0cnVlKTtcblx0XHRcdFxuXHRcdFx0Ly8gRXJyb3IgaGFuZGxlciBmdW5jdGlvbiB0byBzcGVjaWZ5IHRoZSBiZWhhdmlvciBvbiBlcnJvcnMgd2hpbGUgcHJvY2Vzc2luZy5cblx0XHRcdGNvbnN0IG9uUmVmcmVzaFNldHRpbmdzRXJyb3IgPSBlcnJvciA9PiB7XG5cdFx0XHRcdC8vIE91dHB1dCB3YXJuaW5nLlxuXHRcdFx0XHRjb25zb2xlLndhcm4oJ0Vycm9yIHdoaWxlIHJlZnJlc2hpbmcnLCBlcnJvcik7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBIaWRlIHRoZSBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHRcdHRoaXMuX3RvZ2dsZUxvYWRpbmdTcGlubmVyKGZhbHNlKTtcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIFJlbW92ZSBhbnkgZXJyb3IgbWVzc2FnZSwgc2V0IHJvdyBoZWlnaHQsXG5cdFx0XHQvLyByZW9yZGVyIGFuZCB1cGRhdGUgdGhlIHNldHRpbmdzIGFuZCBoaWRlIHRoZSBsb2FkaW5nIHNwaW5uZXIuXG5cdFx0XHR0aGlzXG5cdFx0XHRcdC5fY2xlYXJFcnJvck1lc3NhZ2UoKVxuXHRcdFx0XHQuX2dldFJvd0hlaWdodFNldHRpbmcoKVxuXHRcdFx0XHQudGhlbihyb3dIZWlnaHRWYWx1ZSA9PiB0aGlzLl9zZXRSb3dIZWlnaHQocm93SGVpZ2h0VmFsdWUpKVxuXHRcdFx0XHQudGhlbigoKSA9PiB0aGlzLl9nZXREaXNwbGF5VG9vbHRpcFNldHRpbmcoKSlcblx0XHRcdFx0LnRoZW4oZGlzcGxheVRvb2x0aXBTZXR0aW5nID0+IHRoaXMuX3NldERpc3BsYXlUb29sdGlwU2V0dGluZyhkaXNwbGF5VG9vbHRpcFNldHRpbmcpKVxuXHRcdFx0XHQudGhlbigoKSA9PiB0aGlzLl9nZXRDb2x1bW5TZXR0aW5ncygpKVxuXHRcdFx0XHQudGhlbihjb2x1bW5TZXR0aW5ncyA9PiB0aGlzLl9zZXRDb2x1bW5TZXR0aW5ncyhjb2x1bW5TZXR0aW5ncykpXG5cdFx0XHRcdC50aGVuKCgpID0+IHRoaXMuX3RvZ2dsZUxvYWRpbmdTcGlubmVyKGZhbHNlKSlcblx0XHRcdFx0LmNhdGNoKG9uUmVmcmVzaFNldHRpbmdzRXJyb3IpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2V0cyB0aGUgcm93IGhlaWdodCBzZXR0aW5nIHZhbHVlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IHZhbHVlIFJvdyBoZWlnaHQgdmFsdWUuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtPdmVydmlld1NldHRpbmdzTW9kYWxDb250cm9sbGVyfSBTYW1lIGluc3RhbmNlIGZvciBtZXRob2QgY2hhaW5pbmcuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdF9zZXRSb3dIZWlnaHQodmFsdWUgPSB0aGlzLkRFRkFVTFRfUk9XX0hFSUdIVF9TRVRUSU5HKSB7XG5cdFx0XHR0aGlzXG5cdFx0XHRcdC4kZWxlbWVudFxuXHRcdFx0XHQuZmluZCh0aGlzLnJvd0hlaWdodFZhbHVlU2VsZWN0b3IpXG5cdFx0XHRcdC52YWwodmFsdWUpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2V0cyB0aGUgZGlzcGxheSB0b29sdGlwcyBzZXR0aW5nIHZhbHVlLlxuXHRcdCAqIFxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSB2YWx1ZSBEaXNwbGF5IHRvb2x0aXBzIHZhbHVlLlxuXHRcdCAqIFxuXHRcdCAqIEByZXR1cm4ge092ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHQgKiBcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdF9zZXREaXNwbGF5VG9vbHRpcFNldHRpbmcodmFsdWUgPSB0aGlzLkRFRkFVTFRfRElTUExBWV9UT09MVElQU19TRVRUSU5HUykge1xuXHRcdFx0dGhpc1xuXHRcdFx0XHQuJGVsZW1lbnRcblx0XHRcdFx0LmZpbmQodGhpcy5kaXNwbGF5VG9vbHRpcFZhbHVlU2VsZWN0b3IpXG5cdFx0XHRcdC5wcm9wKCdjaGVja2VkJywgdmFsdWUgPT09ICd0cnVlJylcblx0XHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmVvcmRlcnMgYW5kIHVwZGF0ZXMgdGhlIGNvbHVtbiBzZXR0aW5nIHZhbHVlcy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfEFycmF5fSBjb2x1bW5TZXR0aW5ncyBTdHJpbmdpZmllZCBKU09OIGFycmF5IGNvbnRhaW5pbmcgdGhlIHNhdmVkIGNvbHVtbiBzZXR0aW5ncy5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge092ZXJ2aWV3U2V0dGluZ3NNb2RhbENvbnRyb2xsZXJ9IFNhbWUgaW5zdGFuY2UgZm9yIG1ldGhvZCBjaGFpbmluZy5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0X3NldENvbHVtblNldHRpbmdzKGNvbHVtblNldHRpbmdzID0gdGhpcy5ERUZBVUxUX0NPTFVNTl9TRVRUSU5HUykge1xuXHRcdFx0Ly8gUmVnZXggZm9yIGVzY2FwZSBjaGFyYWN0ZXIuXG5cdFx0XHRjb25zdCBFU0NBUEVfQ0hBUiA9IC9cXFxcL2c7XG5cdFx0XHRcblx0XHRcdC8vIE5vIG5lZWQgdG8gcGFyc2UgZnJvbSBKU09OIG9uIGRlZmF1bHQgdmFsdWUgYXMgaXQgaXMgYW4gYXJyYXkuXG5cdFx0XHRpZiAoIUFycmF5LmlzQXJyYXkoY29sdW1uU2V0dGluZ3MpKSB7XG5cdFx0XHRcdC8vIFJlbW92ZSBlc2NhcGUgY2hhcmFjdGVycyBmcm9tIGFuZCBwYXJzZSBhcnJheSBmcm9tIEpTT04uXG5cdFx0XHRcdGNvbHVtblNldHRpbmdzID0gY29sdW1uU2V0dGluZ3MucmVwbGFjZShFU0NBUEVfQ0hBUiwgJycpO1xuXHRcdFx0XHRjb2x1bW5TZXR0aW5ncyA9IEpTT04ucGFyc2UoY29sdW1uU2V0dGluZ3MpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBDYWNoZSBjb250YWluZXIgdG8gdGVtcG9yYXJpbHkgaG9sZCBhbGwgYWN0aXZlIGxpc3QgaXRlbXMgaW4gc29ydGVkIG9yZGVyLlxuXHRcdFx0Ly8gVGhlIGNoaWxkcmVuIG9mIHRoaXMgZWxlbWVudCB3aWxsIGJlIHByZXBlbmRlZCB0byB0aGUgc2V0dGluZyBsaXN0IGl0ZW0gY29udGFpbmVyIHRvIHJldGFpbiB0aGUgXG5cdFx0XHQvLyBzb3J0aW5nIG9yZGVyLlxuXHRcdFx0Y29uc3QgJHNvcnRlZEl0ZW1zID0gJCgnPGRpdi8+Jyk7XG5cdFx0XHRcblx0XHRcdC8vIEl0ZXJhdG9yIGZ1bmN0aW9uIHRvIHByZXBlbmQgYWN0aXZlIGxpc3QgaXRlbXMgdG8gdGhlIHRvcCBhbmQgYWN0aXZhdGUgdGhlIGNoZWNrYm94LlxuXHRcdFx0Y29uc3Qgc2V0dGluZ0l0ZXJhdG9yID0gc2V0dGluZyA9PiB7XG5cdFx0XHRcdC8vIExpc3QgaXRlbSBJRC5cblx0XHRcdFx0Y29uc3QgaWQgPSB0aGlzLnNldHRpbmdMaXN0SXRlbUlkUHJlZml4ICsgc2V0dGluZztcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFmZmVjdGVkIHNldHRpbmcgbGlzdCBpdGVtLlxuXHRcdFx0XHRjb25zdCAkbGlzdEl0ZW0gPSB0aGlzLiRzZXR0aW5ncy5maW5kKGAjJHtpZH1gKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIENoZWNrYm94IG9mIGFmZmVjdGVkIGxpc3QgaXRlbS5cblx0XHRcdFx0Y29uc3QgJGNoZWNrYm94ID0gJGxpc3RJdGVtLmZpbmQoJyMnICsgdGhpcy5zZXR0aW5nVmFsdWVJZFByZWZpeCArIHNldHRpbmcpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQWN0aXZhdGUgY2hlY2tib3guXG5cdFx0XHRcdGlmICghJGNoZWNrYm94LmlzKCc6Y2hlY2tlZCcpKSB7XG5cdFx0XHRcdFx0JGNoZWNrYm94LnBhcmVudCgpLnRyaWdnZXIoJ2NsaWNrJyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdC8vIE1vdmUgdG8gY2FjaGUgY29udGFpbmVyLlxuXHRcdFx0XHQkbGlzdEl0ZW0uYXBwZW5kVG8oJHNvcnRlZEl0ZW1zKTtcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIE1vdmUgYWN0aXZlIGxpc3QgaXRlbXMgdG8gdGhlIHRvcCBiZWFyaW5nIHRoZSBzb3J0aW5nIG9yZGVyIGluIG1pbmQuXG5cdFx0XHRjb2x1bW5TZXR0aW5ncy5mb3JFYWNoKHNldHRpbmdJdGVyYXRvcik7XG5cdFx0XHRcblx0XHRcdC8vIFByZXBlbmQgY2FjaGVkIGVsZW1lbnRzIHRvIGl0ZW0gbGlzdC5cblx0XHRcdCRzb3J0ZWRJdGVtc1xuXHRcdFx0XHQuY2hpbGRyZW4oKVxuXHRcdFx0XHQucHJlcGVuZFRvKHRoaXMuJHNldHRpbmdzKTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlc2V0cyB0aGUgY29sdW1uIG9yZGVyIGFuZCByb3cgaGVpZ2h0IHNldHRpbmdzIHRvIHRoZSBkZWZhdWx0LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T3ZlcnZpZXdTZXR0aW5nc01vZGFsQ29udHJvbGxlcn0gU2FtZSBpbnN0YW5jZSBmb3IgbWV0aG9kIGNoYWluaW5nLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRfc2V0RGVmYXVsdFNldHRpbmdzKCkge1xuXHRcdFx0Ly8gRGVmYXVsdCB2YWx1ZXMuXG5cdFx0XHRjb25zdCBjb2x1bW5TZXR0aW5ncyA9IHRoaXMuREVGQVVMVF9DT0xVTU5fU0VUVElOR1M7XG5cdFx0XHRjb25zdCByb3dIZWlnaHQgPSB0aGlzLkRFRkFVTFRfUk9XX0hFSUdIVF9TRVRUSU5HO1xuXHRcdFx0XG5cdFx0XHQvLyBTZXQgY29sdW1uIHNldHRpbmdzLlxuXHRcdFx0Ly8gQ2FjaGUgY29udGFpbmVyIHRvIHRlbXBvcmFyaWx5IGhvbGQgYWxsIGFjdGl2ZSBsaXN0IGl0ZW1zIGluIHNvcnRlZCBvcmRlci5cblx0XHRcdC8vIFRoZSBjaGlsZHJlbiBvZiB0aGlzIGVsZW1lbnQgd2lsbCBiZSBwcmVwZW5kZWQgdG8gdGhlIHNldHRpbmcgbGlzdCBpdGVtIGNvbnRhaW5lciB0byByZXRhaW4gdGhlIFxuXHRcdFx0Ly8gc29ydGluZyBvcmRlci5cblx0XHRcdGNvbnN0ICRzb3J0ZWRJdGVtcyA9ICQoJzxkaXYvPicpO1xuXHRcdFx0XG5cdFx0XHQvLyBJdGVyYXRvciBmdW5jdGlvbiB0byBwcmVwZW5kIGFjdGl2ZSBsaXN0IGl0ZW1zIHRvIHRoZSB0b3AgYW5kIGFjdGl2YXRlIHRoZSBjaGVja2JveC5cblx0XHRcdGNvbnN0IHNldHRpbmdJdGVyYXRvciA9IHNldHRpbmcgPT4ge1xuXHRcdFx0XHQvLyBMaXN0IGl0ZW0gSUQuXG5cdFx0XHRcdGNvbnN0IGlkID0gdGhpcy5zZXR0aW5nTGlzdEl0ZW1JZFByZWZpeCArIHNldHRpbmc7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBZmZlY3RlZCBzZXR0aW5nIGxpc3QgaXRlbS5cblx0XHRcdFx0Y29uc3QgJGxpc3RJdGVtID0gdGhpcy4kc2V0dGluZ3MuZmluZChgIyR7aWR9YCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBDaGVja2JveCBvZiBhZmZlY3RlZCBsaXN0IGl0ZW0uXG5cdFx0XHRcdGNvbnN0ICRjaGVja2JveCA9ICRsaXN0SXRlbS5maW5kKCcjJyArIHRoaXMuc2V0dGluZ1ZhbHVlSWRQcmVmaXggKyBzZXR0aW5nKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFjdGl2YXRlIGNoZWNrYm94LlxuXHRcdFx0XHRpZiAoISRjaGVja2JveC5pcygnOmNoZWNrZWQnKSkge1xuXHRcdFx0XHRcdCRjaGVja2JveC5wYXJlbnQoKS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBNb3ZlIHRvIGNhY2hlIGNvbnRhaW5lci5cblx0XHRcdFx0JGxpc3RJdGVtLmFwcGVuZFRvKCRzb3J0ZWRJdGVtcyk7XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBEZWFjdGl2YXRlIGFsbCBjaGVja2JveGVzLlxuXHRcdFx0dGhpc1xuXHRcdFx0XHQuJHNldHRpbmdzXG5cdFx0XHRcdC5maW5kKCc6Y2hlY2tib3gnKVxuXHRcdFx0XHQuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcblx0XHRcdFx0XHRjb25zdCAkY2hlY2tib3ggPSAkKGVsZW1lbnQpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmICgkY2hlY2tib3guaXMoJzpjaGVja2VkJykpIHtcblx0XHRcdFx0XHRcdCRjaGVja2JveC5wYXJlbnQoKS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIE1vdmUgYWN0aXZlIGxpc3QgaXRlbXMgdG8gdGhlIHRvcCBiZWFyaW5nIHRoZSBzb3J0aW5nIG9yZGVyIGluIG1pbmQuXG5cdFx0XHRjb2x1bW5TZXR0aW5ncy5mb3JFYWNoKHNldHRpbmdJdGVyYXRvcik7XG5cdFx0XHRcblx0XHRcdC8vIFByZXBlbmQgY2FjaGVkIGVsZW1lbnRzIHRvIGl0ZW0gbGlzdC5cblx0XHRcdCRzb3J0ZWRJdGVtc1xuXHRcdFx0XHQuY2hpbGRyZW4oKVxuXHRcdFx0XHQucHJlcGVuZFRvKHRoaXMuJHNldHRpbmdzKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2V0IHJvdyBoZWlnaHQuXG5cdFx0XHR0aGlzXG5cdFx0XHRcdC4kZWxlbWVudFxuXHRcdFx0XHQuZmluZCh0aGlzLnJvd0hlaWdodFZhbHVlU2VsZWN0b3IpXG5cdFx0XHRcdC52YWwocm93SGVpZ2h0KTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fVxuXHR9XG5cdFxuXHRleHBvcnRzLmNsYXNzID0gT3ZlcnZpZXdTZXR0aW5nc01vZGFsQ29udHJvbGxlcjtcbn0oanNlLmxpYnMub3ZlcnZpZXdfc2V0dGluZ3NfbW9kYWxfY29udHJvbGxlcikpO1xuIl19
