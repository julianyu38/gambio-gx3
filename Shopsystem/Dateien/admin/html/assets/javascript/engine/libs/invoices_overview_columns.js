'use strict';

/* --------------------------------------------------------------
 invocies_overview_columns.js 2016-10-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.invoices_overview_columns = jse.libs.invoices_overview_columns || {};

/**
 * ## Invoices Table Column Definitions
 *
 * This module defines the column definition of the invoices overview table. They can be overridden by other
 * scripts by modifying the array with new columns, or by replacing the property values of the contained
 * fields.
 *
 * @module Admin/Libs/invoices_overview_columns
 * @exports jse.libs.invoices_overview_columns
 */
(function (exports) {

	'use strict';

	exports.checkbox = exports.checkbox || {
		data: null,
		minWidth: '50px',
		widthFactor: 0.01,
		orderable: false,
		searchable: false,
		defaultContent: '<input type="checkbox" />'
	};

	exports.invoiceNumber = exports.invoiceNumber || {
		data: 'invoiceNumber',
		minWidth: '130px',
		widthFactor: 1.6,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			// Create a link element.
			var link = document.createElement('a');

			// Invoice link parameters.
			var parameters = {
				module: 'OrderAdmin',
				action: 'showPdf',
				type: 'invoice',
				invoice_id: full.DT_RowData.invoiceId
			};

			// Compound invoice link.
			var url = jse.core.config.get('appUrl') + '/admin/request_port.php?' + $.param(parameters);

			// Set element's link attribute.
			link.setAttribute('href', url);
			link.setAttribute('target', '_blank');
			link.setAttribute('title', data);

			// Set element's text content.
			link.textContent = data;

			// Return element's entire HTML.
			return link.outerHTML;
		}
	};

	exports.invoiceDate = exports.invoiceDate || {
		data: 'invoiceDate',
		minWidth: '100px',
		widthFactor: 1.6,
		render: function render(data, type, full, meta) {
			return moment(data).format('DD.MM.YY - HH:mm');
		}
	};

	exports.sum = exports.sum || {
		data: 'sum',
		minWidth: '90px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			return '<span class="tooltip-invoice-sum-block">' + data + '</span>';
		}
	};

	exports.customer = exports.customer || {
		data: 'customer',
		minWidth: '190px',
		widthFactor: 1.5,
		render: function render(data, type, full, meta) {
			var linkElement = full.DT_RowData.customerId ? '<a class="tooltip-customer-addresses" \n\t\t\t\t\t\t\thref="customers.php?cID=' + full.DT_RowData.customerId + '&action=edit">' + data + '</a>' : '<span class="tooltip-customer-addresses">' + data + '</span>';

			if (full.DT_RowData.customerMemos.length > 0) {
				linkElement += ' <i class="fa fa-sticky-note-o tooltip-customer-memos tooltip-trigger"\n                                aria-hidden="true"></i>';
			}

			return linkElement;
		}
	};

	exports.group = exports.group || {
		data: 'group',
		minWidth: '85px',
		widthFactor: 1.2
	};

	exports.countryIsoCode = exports.countryIsoCode || {
		data: 'countryIsoCode',
		minWidth: '75px',
		widthFactor: 1.4,
		render: function render(data, type, full, meta) {
			var html = '';

			if (data) {
				html = '<img src="' + jse.core.config.get('appUrl') + '/images/icons/flags/' + data.toLowerCase() + '.png" />&nbsp;';
			}

			var title = jse.core.lang.translate('SHIPPING_ORIGIN_COUNTRY_TITLE', 'configuration') + ': ' + full.DT_RowData.country;

			html += '<span title="' + title + '">' + data + '</span>';

			return html;
		}
	};

	exports.orderId = exports.orderId || {
		data: 'orderId',
		minWidth: '75px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			if (data === 0) {
				return '';
			}

			// Create a link element.
			var $link = document.createElement('a');

			// URL parameters.
			var parameters = {
				oID: data,
				action: 'edit'
			};

			// Compound order link.
			var url = jse.core.config.get('appUrl') + '/admin/orders.php?' + $.param(parameters);

			// Set element's link attribute.
			$link.setAttribute('href', url);

			// Set element's text content.
			$link.textContent = data;

			// Return element's entire HTML.
			return $link.outerHTML;
		}
	};

	exports.orderDate = exports.orderDate || {
		data: 'orderDate',
		minWidth: '100px',
		widthFactor: 1.6,
		render: function render(data, type, full, meta) {
			return moment(data).format('DD.MM.YY - HH:mm');
		}
	};

	exports.paymentMethod = exports.paymentMethod || {
		data: 'paymentMethod',
		minWidth: '110px',
		widthFactor: 2,
		render: function render(data, type, full, meta) {
			return '<span title="' + full.DT_RowData.paymentMethod + '">' + data + '</span>';
		}
	};

	exports.status = exports.status || {
		data: 'status',
		minWidth: '120px',
		widthFactor: 2,
		render: function render(data, type, full, meta) {
			return !data ? '' : '\n\t\t\t\t\t<span class="order-status tooltip-invoice-status-history label label-' + full.DT_RowData.statusId + '">\n\t\t\t\t\t\t' + data + '\n\t\t\t\t\t</span>\n\t\t\t\t';
		}
	};

	exports.actions = exports.actions || {
		data: null,
		minWidth: '400px',
		widthFactor: 4.6,
		className: 'actions',
		orderable: false,
		searchable: false,
		render: function render(data, type, full, meta) {
			return '\t\t\t\t\t\n\t\t\t\t\t<div class="pull-left"></div>\n\t\t\t\t\t<div class="pull-right action-list visible-on-hover">\n\t\t\t\t\t\t<a href="request_port.php?module=OrderAdmin&action=showPdf&type=invoice' + ('&invoice_number=' + full.DT_RowData.invoiceNumber + '&order_id=' + full.DT_RowData.orderId + '" \n\t\t\t\t\t\t\ttarget="_blank">\n\t\t\t\t\t\t\t<i class="fa fa-eye view"></i>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href="request_port.php?module=OrderAdmin&action=downloadPdf&type=invoice') + ('&invoice_number=' + full.DT_RowData.invoiceNumber + '&order_id=' + full.DT_RowData.orderId + '" \n\t\t\t\t\t\ttarget="_blank">\n\t\t\t\t\t\t\t<i class="fa fa-download download"></i>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<i class="fa fa-envelope-o email-invoice"></i>\n\t\t\t\t\t\t<i class="fa fa-trash-o delete"></i>\n\t\t\t\t\t\n\t\t\t\t\t\t<div class="btn-group dropdown">\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default"></button>\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default dropdown-toggle"\n\t\t\t\t\t\t\t\t\tdata-toggle="dropdown"\n\t\t\t\t\t\t\t\t\taria-haspopup="true"\n\t\t\t\t\t\t\t\t\taria-expanded="false">\n\t\t\t\t\t\t\t\t<span class="caret"></span>\n\t\t\t\t\t\t\t\t<span class="sr-only">Toggle Dropdown</span>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t<ul class="dropdown-menu dropdown-menu-right"></ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t');
		}
	};
})(jse.libs.invoices_overview_columns);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImludm9pY2VzX292ZXJ2aWV3X2NvbHVtbnMuanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsImludm9pY2VzX292ZXJ2aWV3X2NvbHVtbnMiLCJleHBvcnRzIiwiY2hlY2tib3giLCJkYXRhIiwibWluV2lkdGgiLCJ3aWR0aEZhY3RvciIsIm9yZGVyYWJsZSIsInNlYXJjaGFibGUiLCJkZWZhdWx0Q29udGVudCIsImludm9pY2VOdW1iZXIiLCJjbGFzc05hbWUiLCJyZW5kZXIiLCJ0eXBlIiwiZnVsbCIsIm1ldGEiLCJsaW5rIiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50IiwicGFyYW1ldGVycyIsIm1vZHVsZSIsImFjdGlvbiIsImludm9pY2VfaWQiLCJEVF9Sb3dEYXRhIiwiaW52b2ljZUlkIiwidXJsIiwiY29yZSIsImNvbmZpZyIsImdldCIsIiQiLCJwYXJhbSIsInNldEF0dHJpYnV0ZSIsInRleHRDb250ZW50Iiwib3V0ZXJIVE1MIiwiaW52b2ljZURhdGUiLCJtb21lbnQiLCJmb3JtYXQiLCJzdW0iLCJjdXN0b21lciIsImxpbmtFbGVtZW50IiwiY3VzdG9tZXJJZCIsImN1c3RvbWVyTWVtb3MiLCJsZW5ndGgiLCJncm91cCIsImNvdW50cnlJc29Db2RlIiwiaHRtbCIsInRvTG93ZXJDYXNlIiwidGl0bGUiLCJsYW5nIiwidHJhbnNsYXRlIiwiY291bnRyeSIsIm9yZGVySWQiLCIkbGluayIsIm9JRCIsIm9yZGVyRGF0ZSIsInBheW1lbnRNZXRob2QiLCJzdGF0dXMiLCJzdGF0dXNJZCIsImFjdGlvbnMiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQyx5QkFBVCxHQUFxQ0YsSUFBSUMsSUFBSixDQUFTQyx5QkFBVCxJQUFzQyxFQUEzRTs7QUFFQTs7Ozs7Ozs7OztBQVVBLENBQUMsVUFBU0MsT0FBVCxFQUFrQjs7QUFFbEI7O0FBRUFBLFNBQVFDLFFBQVIsR0FBbUJELFFBQVFDLFFBQVIsSUFBb0I7QUFDckNDLFFBQU0sSUFEK0I7QUFFckNDLFlBQVUsTUFGMkI7QUFHckNDLGVBQWEsSUFId0I7QUFJckNDLGFBQVcsS0FKMEI7QUFLckNDLGNBQVksS0FMeUI7QUFNckNDLGtCQUFnQjtBQU5xQixFQUF2Qzs7QUFTQVAsU0FBUVEsYUFBUixHQUF3QlIsUUFBUVEsYUFBUixJQUF5QjtBQUMvQ04sUUFBTSxlQUR5QztBQUUvQ0MsWUFBVSxPQUZxQztBQUcvQ0MsZUFBYSxHQUhrQztBQUkvQ0ssYUFBVyxTQUpvQztBQUsvQ0MsUUFMK0Msa0JBS3hDUixJQUx3QyxFQUtsQ1MsSUFMa0MsRUFLNUJDLElBTDRCLEVBS3RCQyxJQUxzQixFQUtoQjtBQUM5QjtBQUNBLE9BQU1DLE9BQU9DLFNBQVNDLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBYjs7QUFFQTtBQUNBLE9BQU1DLGFBQWE7QUFDbEJDLFlBQVEsWUFEVTtBQUVsQkMsWUFBUSxTQUZVO0FBR2xCUixVQUFNLFNBSFk7QUFJbEJTLGdCQUFZUixLQUFLUyxVQUFMLENBQWdCQztBQUpWLElBQW5COztBQU9BO0FBQ0EsT0FBTUMsTUFBUzFCLElBQUkyQixJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQVQsZ0NBQWlFQyxFQUFFQyxLQUFGLENBQVFYLFVBQVIsQ0FBdkU7O0FBRUE7QUFDQUgsUUFBS2UsWUFBTCxDQUFrQixNQUFsQixFQUEwQk4sR0FBMUI7QUFDQVQsUUFBS2UsWUFBTCxDQUFrQixRQUFsQixFQUE0QixRQUE1QjtBQUNBZixRQUFLZSxZQUFMLENBQWtCLE9BQWxCLEVBQTJCM0IsSUFBM0I7O0FBRUE7QUFDQVksUUFBS2dCLFdBQUwsR0FBbUI1QixJQUFuQjs7QUFFQTtBQUNBLFVBQU9ZLEtBQUtpQixTQUFaO0FBQ0E7QUE5QjhDLEVBQWpEOztBQWlDQS9CLFNBQVFnQyxXQUFSLEdBQXNCaEMsUUFBUWdDLFdBQVIsSUFBdUI7QUFDM0M5QixRQUFNLGFBRHFDO0FBRTNDQyxZQUFVLE9BRmlDO0FBRzNDQyxlQUFhLEdBSDhCO0FBSTNDTSxRQUoyQyxrQkFJcENSLElBSm9DLEVBSTlCUyxJQUo4QixFQUl4QkMsSUFKd0IsRUFJbEJDLElBSmtCLEVBSVo7QUFDOUIsVUFBT29CLE9BQU8vQixJQUFQLEVBQWFnQyxNQUFiLENBQW9CLGtCQUFwQixDQUFQO0FBQ0E7QUFOMEMsRUFBN0M7O0FBU0FsQyxTQUFRbUMsR0FBUixHQUFjbkMsUUFBUW1DLEdBQVIsSUFBZTtBQUMzQmpDLFFBQU0sS0FEcUI7QUFFM0JDLFlBQVUsTUFGaUI7QUFHM0JDLGVBQWEsQ0FIYztBQUkzQkssYUFBVyxTQUpnQjtBQUszQkMsUUFMMkIsa0JBS3BCUixJQUxvQixFQUtkUyxJQUxjLEVBS1JDLElBTFEsRUFLRkMsSUFMRSxFQUtJO0FBQzlCLHVEQUFrRFgsSUFBbEQ7QUFDQTtBQVAwQixFQUE3Qjs7QUFVQUYsU0FBUW9DLFFBQVIsR0FBbUJwQyxRQUFRb0MsUUFBUixJQUFvQjtBQUNyQ2xDLFFBQU0sVUFEK0I7QUFFckNDLFlBQVUsT0FGMkI7QUFHckNDLGVBQWEsR0FId0I7QUFJckNNLFFBSnFDLGtCQUk5QlIsSUFKOEIsRUFJeEJTLElBSndCLEVBSWxCQyxJQUprQixFQUlaQyxJQUpZLEVBSU47QUFDOUIsT0FBSXdCLGNBQWN6QixLQUFLUyxVQUFMLENBQWdCaUIsVUFBaEIsc0ZBRVcxQixLQUFLUyxVQUFMLENBQWdCaUIsVUFGM0Isc0JBRXNEcEMsSUFGdEQsMERBRzZCQSxJQUg3QixZQUFsQjs7QUFLQSxPQUFJVSxLQUFLUyxVQUFMLENBQWdCa0IsYUFBaEIsQ0FBOEJDLE1BQTlCLEdBQXVDLENBQTNDLEVBQThDO0FBQzdDSDtBQUdBOztBQUVELFVBQU9BLFdBQVA7QUFDQTtBQWpCb0MsRUFBdkM7O0FBb0JBckMsU0FBUXlDLEtBQVIsR0FBZ0J6QyxRQUFReUMsS0FBUixJQUFpQjtBQUMvQnZDLFFBQU0sT0FEeUI7QUFFL0JDLFlBQVUsTUFGcUI7QUFHL0JDLGVBQWE7QUFIa0IsRUFBakM7O0FBTUFKLFNBQVEwQyxjQUFSLEdBQXlCMUMsUUFBUTBDLGNBQVIsSUFBMEI7QUFDakR4QyxRQUFNLGdCQUQyQztBQUVqREMsWUFBVSxNQUZ1QztBQUdqREMsZUFBYSxHQUhvQztBQUlqRE0sUUFKaUQsa0JBSTFDUixJQUowQyxFQUlwQ1MsSUFKb0MsRUFJOUJDLElBSjhCLEVBSXhCQyxJQUp3QixFQUlsQjtBQUM5QixPQUFJOEIsT0FBTyxFQUFYOztBQUVBLE9BQUl6QyxJQUFKLEVBQVU7QUFDVHlDLDBCQUNjOUMsSUFBSTJCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsQ0FEZCw0QkFDa0V4QixLQUFLMEMsV0FBTCxFQURsRTtBQUVBOztBQUVELE9BQU1DLFFBQVFoRCxJQUFJMkIsSUFBSixDQUFTc0IsSUFBVCxDQUFjQyxTQUFkLENBQXdCLCtCQUF4QixFQUF5RCxlQUF6RCxJQUNYLElBRFcsR0FDSm5DLEtBQUtTLFVBQUwsQ0FBZ0IyQixPQUQxQjs7QUFHQUwsNkJBQXdCRSxLQUF4QixVQUFrQzNDLElBQWxDOztBQUVBLFVBQU95QyxJQUFQO0FBQ0E7QUFsQmdELEVBQW5EOztBQXFCQTNDLFNBQVFpRCxPQUFSLEdBQWtCakQsUUFBUWlELE9BQVIsSUFBbUI7QUFDbkMvQyxRQUFNLFNBRDZCO0FBRW5DQyxZQUFVLE1BRnlCO0FBR25DQyxlQUFhLENBSHNCO0FBSW5DSyxhQUFXLFNBSndCO0FBS25DQyxRQUxtQyxrQkFLNUJSLElBTDRCLEVBS3RCUyxJQUxzQixFQUtoQkMsSUFMZ0IsRUFLVkMsSUFMVSxFQUtKO0FBQzlCLE9BQUlYLFNBQVMsQ0FBYixFQUFnQjtBQUNmLFdBQU8sRUFBUDtBQUNBOztBQUVEO0FBQ0EsT0FBTWdELFFBQVFuQyxTQUFTQyxhQUFULENBQXVCLEdBQXZCLENBQWQ7O0FBRUE7QUFDQSxPQUFNQyxhQUFhO0FBQ2xCa0MsU0FBS2pELElBRGE7QUFFbEJpQixZQUFRO0FBRlUsSUFBbkI7O0FBS0E7QUFDQSxPQUFNSSxNQUFTMUIsSUFBSTJCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsQ0FBVCwwQkFBMkRDLEVBQUVDLEtBQUYsQ0FBUVgsVUFBUixDQUFqRTs7QUFFQTtBQUNBaUMsU0FBTXJCLFlBQU4sQ0FBbUIsTUFBbkIsRUFBMkJOLEdBQTNCOztBQUVBO0FBQ0EyQixTQUFNcEIsV0FBTixHQUFvQjVCLElBQXBCOztBQUVBO0FBQ0EsVUFBT2dELE1BQU1uQixTQUFiO0FBQ0E7QUE5QmtDLEVBQXJDOztBQWlDQS9CLFNBQVFvRCxTQUFSLEdBQW9CcEQsUUFBUW9ELFNBQVIsSUFBcUI7QUFDdkNsRCxRQUFNLFdBRGlDO0FBRXZDQyxZQUFVLE9BRjZCO0FBR3ZDQyxlQUFhLEdBSDBCO0FBSXZDTSxRQUp1QyxrQkFJaENSLElBSmdDLEVBSTFCUyxJQUowQixFQUlwQkMsSUFKb0IsRUFJZEMsSUFKYyxFQUlSO0FBQzlCLFVBQU9vQixPQUFPL0IsSUFBUCxFQUFhZ0MsTUFBYixDQUFvQixrQkFBcEIsQ0FBUDtBQUNBO0FBTnNDLEVBQXpDOztBQVNBbEMsU0FBUXFELGFBQVIsR0FBd0JyRCxRQUFRcUQsYUFBUixJQUF5QjtBQUMvQ25ELFFBQU0sZUFEeUM7QUFFL0NDLFlBQVUsT0FGcUM7QUFHL0NDLGVBQWEsQ0FIa0M7QUFJL0NNLFFBSitDLGtCQUl4Q1IsSUFKd0MsRUFJbENTLElBSmtDLEVBSTVCQyxJQUo0QixFQUl0QkMsSUFKc0IsRUFJaEI7QUFDOUIsNEJBQXVCRCxLQUFLUyxVQUFMLENBQWdCZ0MsYUFBdkMsVUFBeURuRCxJQUF6RDtBQUNBO0FBTjhDLEVBQWpEOztBQVNBRixTQUFRc0QsTUFBUixHQUFpQnRELFFBQVFzRCxNQUFSLElBQWtCO0FBQ2pDcEQsUUFBTSxRQUQyQjtBQUVqQ0MsWUFBVSxPQUZ1QjtBQUdqQ0MsZUFBYSxDQUhvQjtBQUlqQ00sUUFKaUMsa0JBSTFCUixJQUowQixFQUlwQlMsSUFKb0IsRUFJZEMsSUFKYyxFQUlSQyxJQUpRLEVBSUY7QUFDOUIsVUFBTyxDQUFDWCxJQUFELEdBQVEsRUFBUix5RkFDaUVVLEtBQUtTLFVBQUwsQ0FBZ0JrQyxRQURqRix3QkFFSHJELElBRkcsa0NBQVA7QUFLQTtBQVZnQyxFQUFuQzs7QUFhQUYsU0FBUXdELE9BQVIsR0FBa0J4RCxRQUFRd0QsT0FBUixJQUFtQjtBQUNuQ3RELFFBQU0sSUFENkI7QUFFbkNDLFlBQVUsT0FGeUI7QUFHbkNDLGVBQWEsR0FIc0I7QUFJbkNLLGFBQVcsU0FKd0I7QUFLbkNKLGFBQVcsS0FMd0I7QUFNbkNDLGNBQVksS0FOdUI7QUFPbkNJLFFBUG1DLGtCQU81QlIsSUFQNEIsRUFPdEJTLElBUHNCLEVBT2hCQyxJQVBnQixFQU9WQyxJQVBVLEVBT0o7QUFDOUIsVUFBTyxvT0FJZUQsS0FBS1MsVUFBTCxDQUFnQmIsYUFKL0Isa0JBSXlESSxLQUFLUyxVQUFMLENBQWdCNEIsT0FKekUsME5BU2VyQyxLQUFLUyxVQUFMLENBQWdCYixhQVQvQixrQkFTeURJLEtBQUtTLFVBQUwsQ0FBZ0I0QixPQVR6RSxvMEJBQVA7QUErQkE7QUF2Q2tDLEVBQXJDO0FBeUNBLENBek5ELEVBeU5HcEQsSUFBSUMsSUFBSixDQUFTQyx5QkF6TloiLCJmaWxlIjoiaW52b2ljZXNfb3ZlcnZpZXdfY29sdW1ucy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBpbnZvY2llc19vdmVydmlld19jb2x1bW5zLmpzIDIwMTYtMTAtMTdcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG5qc2UubGlicy5pbnZvaWNlc19vdmVydmlld19jb2x1bW5zID0ganNlLmxpYnMuaW52b2ljZXNfb3ZlcnZpZXdfY29sdW1ucyB8fCB7fTtcclxuXHJcbi8qKlxyXG4gKiAjIyBJbnZvaWNlcyBUYWJsZSBDb2x1bW4gRGVmaW5pdGlvbnNcclxuICpcclxuICogVGhpcyBtb2R1bGUgZGVmaW5lcyB0aGUgY29sdW1uIGRlZmluaXRpb24gb2YgdGhlIGludm9pY2VzIG92ZXJ2aWV3IHRhYmxlLiBUaGV5IGNhbiBiZSBvdmVycmlkZGVuIGJ5IG90aGVyXHJcbiAqIHNjcmlwdHMgYnkgbW9kaWZ5aW5nIHRoZSBhcnJheSB3aXRoIG5ldyBjb2x1bW5zLCBvciBieSByZXBsYWNpbmcgdGhlIHByb3BlcnR5IHZhbHVlcyBvZiB0aGUgY29udGFpbmVkXHJcbiAqIGZpZWxkcy5cclxuICpcclxuICogQG1vZHVsZSBBZG1pbi9MaWJzL2ludm9pY2VzX292ZXJ2aWV3X2NvbHVtbnNcclxuICogQGV4cG9ydHMganNlLmxpYnMuaW52b2ljZXNfb3ZlcnZpZXdfY29sdW1uc1xyXG4gKi9cclxuKGZ1bmN0aW9uKGV4cG9ydHMpIHtcclxuXHRcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0ZXhwb3J0cy5jaGVja2JveCA9IGV4cG9ydHMuY2hlY2tib3ggfHwge1xyXG5cdFx0XHRkYXRhOiBudWxsLFxyXG5cdFx0XHRtaW5XaWR0aDogJzUwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMC4wMSxcclxuXHRcdFx0b3JkZXJhYmxlOiBmYWxzZSxcclxuXHRcdFx0c2VhcmNoYWJsZTogZmFsc2UsXHJcblx0XHRcdGRlZmF1bHRDb250ZW50OiAnPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIC8+J1xyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLmludm9pY2VOdW1iZXIgPSBleHBvcnRzLmludm9pY2VOdW1iZXIgfHwge1xyXG5cdFx0XHRkYXRhOiAnaW52b2ljZU51bWJlcicsXHJcblx0XHRcdG1pbldpZHRoOiAnMTMwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMS42LFxyXG5cdFx0XHRjbGFzc05hbWU6ICdudW1lcmljJyxcclxuXHRcdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuXHRcdFx0XHQvLyBDcmVhdGUgYSBsaW5rIGVsZW1lbnQuXHJcblx0XHRcdFx0Y29uc3QgbGluayA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBJbnZvaWNlIGxpbmsgcGFyYW1ldGVycy5cclxuXHRcdFx0XHRjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG5cdFx0XHRcdFx0bW9kdWxlOiAnT3JkZXJBZG1pbicsXHJcblx0XHRcdFx0XHRhY3Rpb246ICdzaG93UGRmJyxcclxuXHRcdFx0XHRcdHR5cGU6ICdpbnZvaWNlJyxcclxuXHRcdFx0XHRcdGludm9pY2VfaWQ6IGZ1bGwuRFRfUm93RGF0YS5pbnZvaWNlSWRcclxuXHRcdFx0XHR9O1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIENvbXBvdW5kIGludm9pY2UgbGluay5cclxuXHRcdFx0XHRjb25zdCB1cmwgPSBgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYWRtaW4vcmVxdWVzdF9wb3J0LnBocD8keyQucGFyYW0ocGFyYW1ldGVycyl9YDtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBTZXQgZWxlbWVudCdzIGxpbmsgYXR0cmlidXRlLlxyXG5cdFx0XHRcdGxpbmsuc2V0QXR0cmlidXRlKCdocmVmJywgdXJsKTtcclxuXHRcdFx0XHRsaW5rLnNldEF0dHJpYnV0ZSgndGFyZ2V0JywgJ19ibGFuaycpO1xyXG5cdFx0XHRcdGxpbmsuc2V0QXR0cmlidXRlKCd0aXRsZScsIGRhdGEpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIFNldCBlbGVtZW50J3MgdGV4dCBjb250ZW50LlxyXG5cdFx0XHRcdGxpbmsudGV4dENvbnRlbnQgPSBkYXRhO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIFJldHVybiBlbGVtZW50J3MgZW50aXJlIEhUTUwuXHJcblx0XHRcdFx0cmV0dXJuIGxpbmsub3V0ZXJIVE1MO1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFxyXG5cdGV4cG9ydHMuaW52b2ljZURhdGUgPSBleHBvcnRzLmludm9pY2VEYXRlIHx8IHtcclxuXHRcdFx0ZGF0YTogJ2ludm9pY2VEYXRlJyxcclxuXHRcdFx0bWluV2lkdGg6ICcxMDBweCcsXHJcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjYsXHJcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XHJcblx0XHRcdFx0cmV0dXJuIG1vbWVudChkYXRhKS5mb3JtYXQoJ0RELk1NLllZIC0gSEg6bW0nKVxyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFxyXG5cdGV4cG9ydHMuc3VtID0gZXhwb3J0cy5zdW0gfHwge1xyXG5cdFx0XHRkYXRhOiAnc3VtJyxcclxuXHRcdFx0bWluV2lkdGg6ICc5MHB4JyxcclxuXHRcdFx0d2lkdGhGYWN0b3I6IDEsXHJcblx0XHRcdGNsYXNzTmFtZTogJ251bWVyaWMnLFxyXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xyXG5cdFx0XHRcdHJldHVybiBgPHNwYW4gY2xhc3M9XCJ0b29sdGlwLWludm9pY2Utc3VtLWJsb2NrXCI+JHtkYXRhfTwvc3Bhbj5gO1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFxyXG5cdGV4cG9ydHMuY3VzdG9tZXIgPSBleHBvcnRzLmN1c3RvbWVyIHx8IHtcclxuXHRcdFx0ZGF0YTogJ2N1c3RvbWVyJyxcclxuXHRcdFx0bWluV2lkdGg6ICcxOTBweCcsXHJcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjUsXHJcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XHJcblx0XHRcdFx0bGV0IGxpbmtFbGVtZW50ID0gZnVsbC5EVF9Sb3dEYXRhLmN1c3RvbWVySWRcclxuXHRcdFx0XHRcdD8gYDxhIGNsYXNzPVwidG9vbHRpcC1jdXN0b21lci1hZGRyZXNzZXNcIiBcclxuXHRcdFx0XHRcdFx0XHRocmVmPVwiY3VzdG9tZXJzLnBocD9jSUQ9JHtmdWxsLkRUX1Jvd0RhdGEuY3VzdG9tZXJJZH0mYWN0aW9uPWVkaXRcIj4ke2RhdGF9PC9hPmBcclxuXHRcdFx0XHRcdDogYDxzcGFuIGNsYXNzPVwidG9vbHRpcC1jdXN0b21lci1hZGRyZXNzZXNcIj4ke2RhdGF9PC9zcGFuPmA7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKGZ1bGwuRFRfUm93RGF0YS5jdXN0b21lck1lbW9zLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0XHRcdGxpbmtFbGVtZW50ICs9XHJcblx0XHRcdFx0XHRcdGAgPGkgY2xhc3M9XCJmYSBmYS1zdGlja3ktbm90ZS1vIHRvb2x0aXAtY3VzdG9tZXItbWVtb3MgdG9vbHRpcC10cmlnZ2VyXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+YDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0cmV0dXJuIGxpbmtFbGVtZW50O1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFxyXG5cdGV4cG9ydHMuZ3JvdXAgPSBleHBvcnRzLmdyb3VwIHx8IHtcclxuXHRcdFx0ZGF0YTogJ2dyb3VwJyxcclxuXHRcdFx0bWluV2lkdGg6ICc4NXB4JyxcclxuXHRcdFx0d2lkdGhGYWN0b3I6IDEuMlxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLmNvdW50cnlJc29Db2RlID0gZXhwb3J0cy5jb3VudHJ5SXNvQ29kZSB8fCB7XHJcblx0XHRcdGRhdGE6ICdjb3VudHJ5SXNvQ29kZScsXHJcblx0XHRcdG1pbldpZHRoOiAnNzVweCcsXHJcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjQsXHJcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XHJcblx0XHRcdFx0bGV0IGh0bWwgPSAnJztcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoZGF0YSkge1xyXG5cdFx0XHRcdFx0aHRtbCA9XHJcblx0XHRcdFx0XHRcdGA8aW1nIHNyYz1cIiR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2ltYWdlcy9pY29ucy9mbGFncy8ke2RhdGEudG9Mb3dlckNhc2UoKX0ucG5nXCIgLz4mbmJzcDtgO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRjb25zdCB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTSElQUElOR19PUklHSU5fQ09VTlRSWV9USVRMRScsICdjb25maWd1cmF0aW9uJylcclxuXHRcdFx0XHRcdCsgJzogJyArIGZ1bGwuRFRfUm93RGF0YS5jb3VudHJ5O1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGh0bWwgKz0gYDxzcGFuIHRpdGxlPVwiJHt0aXRsZX1cIj4ke2RhdGF9PC9zcGFuPmA7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0cmV0dXJuIGh0bWw7XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblx0XHJcblx0ZXhwb3J0cy5vcmRlcklkID0gZXhwb3J0cy5vcmRlcklkIHx8IHtcclxuXHRcdFx0ZGF0YTogJ29yZGVySWQnLFxyXG5cdFx0XHRtaW5XaWR0aDogJzc1cHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMSxcclxuXHRcdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYycsXHJcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XHJcblx0XHRcdFx0aWYgKGRhdGEgPT09IDApIHtcclxuXHRcdFx0XHRcdHJldHVybiAnJztcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gQ3JlYXRlIGEgbGluayBlbGVtZW50LlxyXG5cdFx0XHRcdGNvbnN0ICRsaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIFVSTCBwYXJhbWV0ZXJzLlxyXG5cdFx0XHRcdGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcblx0XHRcdFx0XHRvSUQ6IGRhdGEsXHJcblx0XHRcdFx0XHRhY3Rpb246ICdlZGl0J1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gQ29tcG91bmQgb3JkZXIgbGluay5cclxuXHRcdFx0XHRjb25zdCB1cmwgPSBgJHtqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKX0vYWRtaW4vb3JkZXJzLnBocD8keyQucGFyYW0ocGFyYW1ldGVycyl9YDtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBTZXQgZWxlbWVudCdzIGxpbmsgYXR0cmlidXRlLlxyXG5cdFx0XHRcdCRsaW5rLnNldEF0dHJpYnV0ZSgnaHJlZicsIHVybCk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gU2V0IGVsZW1lbnQncyB0ZXh0IGNvbnRlbnQuXHJcblx0XHRcdFx0JGxpbmsudGV4dENvbnRlbnQgPSBkYXRhO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIFJldHVybiBlbGVtZW50J3MgZW50aXJlIEhUTUwuXHJcblx0XHRcdFx0cmV0dXJuICRsaW5rLm91dGVySFRNTDtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLm9yZGVyRGF0ZSA9IGV4cG9ydHMub3JkZXJEYXRlIHx8IHtcclxuXHRcdFx0ZGF0YTogJ29yZGVyRGF0ZScsXHJcblx0XHRcdG1pbldpZHRoOiAnMTAwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMS42LFxyXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xyXG5cdFx0XHRcdHJldHVybiBtb21lbnQoZGF0YSkuZm9ybWF0KCdERC5NTS5ZWSAtIEhIOm1tJylcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLnBheW1lbnRNZXRob2QgPSBleHBvcnRzLnBheW1lbnRNZXRob2QgfHwge1xyXG5cdFx0XHRkYXRhOiAncGF5bWVudE1ldGhvZCcsXHJcblx0XHRcdG1pbldpZHRoOiAnMTEwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMixcclxuXHRcdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuXHRcdFx0XHRyZXR1cm4gYDxzcGFuIHRpdGxlPVwiJHtmdWxsLkRUX1Jvd0RhdGEucGF5bWVudE1ldGhvZH1cIj4ke2RhdGF9PC9zcGFuPmBcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLnN0YXR1cyA9IGV4cG9ydHMuc3RhdHVzIHx8IHtcclxuXHRcdFx0ZGF0YTogJ3N0YXR1cycsXHJcblx0XHRcdG1pbldpZHRoOiAnMTIwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMixcclxuXHRcdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuXHRcdFx0XHRyZXR1cm4gIWRhdGEgPyAnJyA6IGBcclxuXHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwib3JkZXItc3RhdHVzIHRvb2x0aXAtaW52b2ljZS1zdGF0dXMtaGlzdG9yeSBsYWJlbCBsYWJlbC0ke2Z1bGwuRFRfUm93RGF0YS5zdGF0dXNJZH1cIj5cclxuXHRcdFx0XHRcdFx0JHtkYXRhfVxyXG5cdFx0XHRcdFx0PC9zcGFuPlxyXG5cdFx0XHRcdGA7XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblx0XHJcblx0ZXhwb3J0cy5hY3Rpb25zID0gZXhwb3J0cy5hY3Rpb25zIHx8IHtcclxuXHRcdFx0ZGF0YTogbnVsbCxcclxuXHRcdFx0bWluV2lkdGg6ICc0MDBweCcsXHJcblx0XHRcdHdpZHRoRmFjdG9yOiA0LjYsXHJcblx0XHRcdGNsYXNzTmFtZTogJ2FjdGlvbnMnLFxyXG5cdFx0XHRvcmRlcmFibGU6IGZhbHNlLFxyXG5cdFx0XHRzZWFyY2hhYmxlOiBmYWxzZSxcclxuXHRcdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuXHRcdFx0XHRyZXR1cm4gYFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJwdWxsLWxlZnRcIj48L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJwdWxsLXJpZ2h0IGFjdGlvbi1saXN0IHZpc2libGUtb24taG92ZXJcIj5cclxuXHRcdFx0XHRcdFx0PGEgaHJlZj1cInJlcXVlc3RfcG9ydC5waHA/bW9kdWxlPU9yZGVyQWRtaW4mYWN0aW9uPXNob3dQZGYmdHlwZT1pbnZvaWNlYFxyXG5cdFx0XHRcdFx0KyBgJmludm9pY2VfbnVtYmVyPSR7ZnVsbC5EVF9Sb3dEYXRhLmludm9pY2VOdW1iZXJ9Jm9yZGVyX2lkPSR7ZnVsbC5EVF9Sb3dEYXRhLm9yZGVySWR9XCIgXHJcblx0XHRcdFx0XHRcdFx0dGFyZ2V0PVwiX2JsYW5rXCI+XHJcblx0XHRcdFx0XHRcdFx0PGkgY2xhc3M9XCJmYSBmYS1leWUgdmlld1wiPjwvaT5cclxuXHRcdFx0XHRcdFx0PC9hPlxyXG5cdFx0XHRcdFx0XHQ8YSBocmVmPVwicmVxdWVzdF9wb3J0LnBocD9tb2R1bGU9T3JkZXJBZG1pbiZhY3Rpb249ZG93bmxvYWRQZGYmdHlwZT1pbnZvaWNlYFxyXG5cdFx0XHRcdFx0KyBgJmludm9pY2VfbnVtYmVyPSR7ZnVsbC5EVF9Sb3dEYXRhLmludm9pY2VOdW1iZXJ9Jm9yZGVyX2lkPSR7ZnVsbC5EVF9Sb3dEYXRhLm9yZGVySWR9XCIgXHJcblx0XHRcdFx0XHRcdHRhcmdldD1cIl9ibGFua1wiPlxyXG5cdFx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtZG93bmxvYWQgZG93bmxvYWRcIj48L2k+XHJcblx0XHRcdFx0XHRcdDwvYT5cclxuXHRcdFx0XHRcdFx0PGkgY2xhc3M9XCJmYSBmYS1lbnZlbG9wZS1vIGVtYWlsLWludm9pY2VcIj48L2k+XHJcblx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtdHJhc2gtbyBkZWxldGVcIj48L2k+XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImJ0bi1ncm91cCBkcm9wZG93blwiPlxyXG5cdFx0XHRcdFx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiXHJcblx0XHRcdFx0XHRcdFx0XHRcdGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0XCI+PC9idXR0b24+XHJcblx0XHRcdFx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0Y2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgZHJvcGRvd24tdG9nZ2xlXCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0ZGF0YS10b2dnbGU9XCJkcm9wZG93blwiXHJcblx0XHRcdFx0XHRcdFx0XHRcdGFyaWEtaGFzcG9wdXA9XCJ0cnVlXCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0YXJpYS1leHBhbmRlZD1cImZhbHNlXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cImNhcmV0XCI+PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJzci1vbmx5XCI+VG9nZ2xlIERyb3Bkb3duPC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdDwvYnV0dG9uPlxyXG5cdFx0XHRcdFx0XHRcdDx1bCBjbGFzcz1cImRyb3Bkb3duLW1lbnUgZHJvcGRvd24tbWVudS1yaWdodFwiPjwvdWw+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0YDtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxufSkoanNlLmxpYnMuaW52b2ljZXNfb3ZlcnZpZXdfY29sdW1ucyk7ICJdfQ==
