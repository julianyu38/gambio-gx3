'use strict';

/* --------------------------------------------------------------
 editor_instances.js 2016-09-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.editor_instances = jse.libs.editor_instances || {};

/**
 * ## Editor Instances Library
 *
 * This library provides a common API for editor widget instances manipulation. 
 * 
 * @module Admin/Libs/editor_instances
 * @exports jse.libs.editor_instances
 */
(function (exports) {

	'use strict';

	/**
  * Editor construct methods
  *
  * @type {Object}
  */

	var constructors = {
		ckeditor: function ckeditor($textarea) {
			var tags = ['p', 'div', 'script', 'style', 'form'];

			var formattingRules = {
				indent: true,
				breakBeforeOpen: true,
				breakAfterOpen: true,
				breakBeforeClose: true,
				breakAfterClose: true
			};

			var config = {
				filebrowserBrowseUrl: 'includes/ckeditor/filemanager/index.html',
				baseHref: jse.core.config.get('appUrl') + '/admin',
				enterMode: CKEDITOR.ENTER_BR,
				shiftEnterMode: CKEDITOR.ENTER_P,
				language: jse.core.config.get('languageCode'),
				useRelPath: true,
				on: {
					instanceReady: function instanceReady(event) {
						var _iteratorNormalCompletion = true;
						var _didIteratorError = false;
						var _iteratorError = undefined;

						try {
							for (var _iterator = tags[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
								var tag = _step.value;

								this.dataProcessor.writer.setRules(tag, formattingRules);
							}
						} catch (err) {
							_didIteratorError = true;
							_iteratorError = err;
						} finally {
							try {
								if (!_iteratorNormalCompletion && _iterator.return) {
									_iterator.return();
								}
							} finally {
								if (_didIteratorError) {
									throw _iteratorError;
								}
							}
						}
					}
				}
			};

			var name = $textarea.attr('name');
			CKEDITOR.replace(name, config);
			return CKEDITOR.instances[name];
		},
		codemirror: function codemirror($textarea) {
			var config = {
				mode: 'htmlmixed',
				lineNumbers: true,
				lineWrapping: true
			};

			return CodeMirror.fromTextArea($textarea[0], config);
		}
	};

	/**
  * Editor destruct methods
  *
  * @type {Object}
  */
	var destructors = {
		ckeditor: function ckeditor($textarea) {
			var name = $textarea.attr('name');
			CKEDITOR.instances[name].destroy();
		},
		codemirror: function codemirror($textarea) {
			var instance = $textarea.siblings('.CodeMirror')[0].CodeMirror;
			instance.toTextArea();
		}
	};

	/**
  * Editor instance methods
  * 
  * @type {Object}
  */
	var getInstance = {
		ckeditor: function ckeditor($textarea) {
			return CKEDITOR.instances[$textarea.attr('name')];
		},
		codemirror: function codemirror($textarea) {
			return $textarea.siblings('.CodeMirror')[0].CodeMirror;
		}
	};

	/**
  * Create new editor instance. 
  * 
  * @param {jQuery} $textarea Textarea selector to be modified.
  * @param {String} type Provide a type that is supported by the widget. 
  * 
  * @return {CKEditor|CodeMirror} Returns the create editor instance.
  */
	exports.create = function ($textarea, type) {
		if (constructors[type] === undefined) {
			throw new Error('Provided editor type is not supported: ' + type);
		}

		var instance = constructors[type]($textarea);
		instance.type = type;

		return instance;
	};

	/**
  * Switch to a new editor type. 
  * 
  * @param {jQuery} $textarea Textarea selector to be modified.
  * @param {String} currentType Provide the current editor type. 
  * @param {String} newType Provide the new editor type.
  * 
  * @return {CKEditor|CodeMirror} Returns the new editor instance. 
  */
	exports.switch = function ($textarea, currentType, newType) {
		if (destructors[currentType] === undefined) {
			throw new Error('Provided editor type is not supported: ' + currentType);
		}

		destructors[currentType]($textarea);

		return exports.create($textarea, newType);
	};

	/**
  * Destroy an existing editor instance.
  * 
  * @param {jQuery} $textarea Textarea selector to be destroyed.
  * @param {String} customInitEventType Optional (''), if the editor was initialized with a custom init-event-type, 
  * then this must be unbound by editor destroy.
  */
	exports.destroy = function ($textarea) {
		var customInitEventType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

		if (customInitEventType && customInitEventType !== 'JSENGINE_INIT_FINISHED') {
			$(window).off(customInitEventType);
		}

		var type = $textarea.data('editorType');

		if (destructors[type] === undefined) {
			throw new Error('Provided editor type is not supported: ' + type);
		}

		destructors[type]($textarea);
	};

	/**
  * Get an editor instance. 
  * 
  * @param {jQuery} $textarea Textarea selector from which the instance will be retrieved.
  * 
  * @return {CKEditor|CodeMirror} Returns the corresponding editor instance. 
  */
	exports.getInstance = function ($textarea) {
		var type = $textarea.data('editorType');

		if (destructors[type] === undefined) {
			throw new Error('Provided editor type is not supported: ' + type);
		}

		return getInstance[type]($textarea);
	};
})(jse.libs.editor_instances);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVkaXRvcl9pbnN0YW5jZXMuanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsImVkaXRvcl9pbnN0YW5jZXMiLCJleHBvcnRzIiwiY29uc3RydWN0b3JzIiwiY2tlZGl0b3IiLCIkdGV4dGFyZWEiLCJ0YWdzIiwiZm9ybWF0dGluZ1J1bGVzIiwiaW5kZW50IiwiYnJlYWtCZWZvcmVPcGVuIiwiYnJlYWtBZnRlck9wZW4iLCJicmVha0JlZm9yZUNsb3NlIiwiYnJlYWtBZnRlckNsb3NlIiwiY29uZmlnIiwiZmlsZWJyb3dzZXJCcm93c2VVcmwiLCJiYXNlSHJlZiIsImNvcmUiLCJnZXQiLCJlbnRlck1vZGUiLCJDS0VESVRPUiIsIkVOVEVSX0JSIiwic2hpZnRFbnRlck1vZGUiLCJFTlRFUl9QIiwibGFuZ3VhZ2UiLCJ1c2VSZWxQYXRoIiwib24iLCJpbnN0YW5jZVJlYWR5IiwiZXZlbnQiLCJ0YWciLCJkYXRhUHJvY2Vzc29yIiwid3JpdGVyIiwic2V0UnVsZXMiLCJuYW1lIiwiYXR0ciIsInJlcGxhY2UiLCJpbnN0YW5jZXMiLCJjb2RlbWlycm9yIiwibW9kZSIsImxpbmVOdW1iZXJzIiwibGluZVdyYXBwaW5nIiwiQ29kZU1pcnJvciIsImZyb21UZXh0QXJlYSIsImRlc3RydWN0b3JzIiwiZGVzdHJveSIsImluc3RhbmNlIiwic2libGluZ3MiLCJ0b1RleHRBcmVhIiwiZ2V0SW5zdGFuY2UiLCJjcmVhdGUiLCJ0eXBlIiwidW5kZWZpbmVkIiwiRXJyb3IiLCJzd2l0Y2giLCJjdXJyZW50VHlwZSIsIm5ld1R5cGUiLCJjdXN0b21Jbml0RXZlbnRUeXBlIiwiJCIsIndpbmRvdyIsIm9mZiIsImRhdGEiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQyxnQkFBVCxHQUE0QkYsSUFBSUMsSUFBSixDQUFTQyxnQkFBVCxJQUE2QixFQUF6RDs7QUFFQTs7Ozs7Ozs7QUFRQSxDQUFDLFVBQVNDLE9BQVQsRUFBa0I7O0FBRWxCOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxlQUFlO0FBQ3BCQyxVQURvQixvQkFDWEMsU0FEVyxFQUNBO0FBQ25CLE9BQU1DLE9BQU8sQ0FDWixHQURZLEVBRVosS0FGWSxFQUdaLFFBSFksRUFJWixPQUpZLEVBS1osTUFMWSxDQUFiOztBQVFBLE9BQU1DLGtCQUFrQjtBQUN2QkMsWUFBUyxJQURjO0FBRXZCQyxxQkFBa0IsSUFGSztBQUd2QkMsb0JBQWlCLElBSE07QUFJdkJDLHNCQUFtQixJQUpJO0FBS3ZCQyxxQkFBa0I7QUFMSyxJQUF4Qjs7QUFRQSxPQUFNQyxTQUFTO0FBQ2RDLDBCQUFzQiwwQ0FEUjtBQUVkQyxjQUFVaEIsSUFBSWlCLElBQUosQ0FBU0gsTUFBVCxDQUFnQkksR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsUUFGNUI7QUFHZEMsZUFBV0MsU0FBU0MsUUFITjtBQUlkQyxvQkFBZ0JGLFNBQVNHLE9BSlg7QUFLZEMsY0FBVXhCLElBQUlpQixJQUFKLENBQVNILE1BQVQsQ0FBZ0JJLEdBQWhCLENBQW9CLGNBQXBCLENBTEk7QUFNZE8sZ0JBQVksSUFORTtBQU9kQyxRQUFJO0FBQ0hDLGtCQURHLHlCQUNXQyxLQURYLEVBQ2tCO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQ3BCLDRCQUFnQnJCLElBQWhCLDhIQUFzQjtBQUFBLFlBQWJzQixHQUFhOztBQUNyQixhQUFLQyxhQUFMLENBQW1CQyxNQUFuQixDQUEwQkMsUUFBMUIsQ0FBbUNILEdBQW5DLEVBQXdDckIsZUFBeEM7QUFDQTtBQUhtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSXBCO0FBTEU7QUFQVSxJQUFmOztBQWdCQSxPQUFNeUIsT0FBTzNCLFVBQVU0QixJQUFWLENBQWUsTUFBZixDQUFiO0FBQ0FkLFlBQVNlLE9BQVQsQ0FBaUJGLElBQWpCLEVBQXVCbkIsTUFBdkI7QUFDQSxVQUFPTSxTQUFTZ0IsU0FBVCxDQUFtQkgsSUFBbkIsQ0FBUDtBQUNBLEdBckNtQjtBQXNDcEJJLFlBdENvQixzQkFzQ1QvQixTQXRDUyxFQXNDRTtBQUNyQixPQUFNUSxTQUFTO0FBQ2R3QixVQUFNLFdBRFE7QUFFZEMsaUJBQWEsSUFGQztBQUdkQyxrQkFBYztBQUhBLElBQWY7O0FBTUEsVUFBT0MsV0FBV0MsWUFBWCxDQUF3QnBDLFVBQVUsQ0FBVixDQUF4QixFQUFzQ1EsTUFBdEMsQ0FBUDtBQUNBO0FBOUNtQixFQUFyQjs7QUFpREE7Ozs7O0FBS0EsS0FBTTZCLGNBQWM7QUFDbkJ0QyxVQURtQixvQkFDVkMsU0FEVSxFQUNDO0FBQ25CLE9BQU0yQixPQUFPM0IsVUFBVTRCLElBQVYsQ0FBZSxNQUFmLENBQWI7QUFDQWQsWUFBU2dCLFNBQVQsQ0FBbUJILElBQW5CLEVBQXlCVyxPQUF6QjtBQUNBLEdBSmtCO0FBS25CUCxZQUxtQixzQkFLUi9CLFNBTFEsRUFLRztBQUNyQixPQUFNdUMsV0FBV3ZDLFVBQVV3QyxRQUFWLENBQW1CLGFBQW5CLEVBQWtDLENBQWxDLEVBQXFDTCxVQUF0RDtBQUNBSSxZQUFTRSxVQUFUO0FBQ0E7QUFSa0IsRUFBcEI7O0FBV0E7Ozs7O0FBS0EsS0FBTUMsY0FBYztBQUNuQjNDLFVBRG1CLG9CQUNWQyxTQURVLEVBQ0M7QUFDbkIsVUFBT2MsU0FBU2dCLFNBQVQsQ0FBbUI5QixVQUFVNEIsSUFBVixDQUFlLE1BQWYsQ0FBbkIsQ0FBUDtBQUNBLEdBSGtCO0FBSW5CRyxZQUptQixzQkFJUi9CLFNBSlEsRUFJRztBQUNyQixVQUFPQSxVQUFVd0MsUUFBVixDQUFtQixhQUFuQixFQUFrQyxDQUFsQyxFQUFxQ0wsVUFBNUM7QUFDQTtBQU5rQixFQUFwQjs7QUFTQTs7Ozs7Ozs7QUFRQXRDLFNBQVE4QyxNQUFSLEdBQWlCLFVBQVMzQyxTQUFULEVBQW9CNEMsSUFBcEIsRUFBMEI7QUFDMUMsTUFBSTlDLGFBQWE4QyxJQUFiLE1BQXVCQyxTQUEzQixFQUFzQztBQUNyQyxTQUFNLElBQUlDLEtBQUosQ0FBVSw0Q0FBNENGLElBQXRELENBQU47QUFDQTs7QUFFRCxNQUFNTCxXQUFXekMsYUFBYThDLElBQWIsRUFBbUI1QyxTQUFuQixDQUFqQjtBQUNBdUMsV0FBU0ssSUFBVCxHQUFnQkEsSUFBaEI7O0FBRUEsU0FBT0wsUUFBUDtBQUNBLEVBVEQ7O0FBV0E7Ozs7Ozs7OztBQVNBMUMsU0FBUWtELE1BQVIsR0FBaUIsVUFBUy9DLFNBQVQsRUFBb0JnRCxXQUFwQixFQUFpQ0MsT0FBakMsRUFBMEM7QUFDMUQsTUFBSVosWUFBWVcsV0FBWixNQUE2QkgsU0FBakMsRUFBNEM7QUFDM0MsU0FBTSxJQUFJQyxLQUFKLENBQVUsNENBQTRDRSxXQUF0RCxDQUFOO0FBQ0E7O0FBRURYLGNBQVlXLFdBQVosRUFBeUJoRCxTQUF6Qjs7QUFFQSxTQUFPSCxRQUFROEMsTUFBUixDQUFlM0MsU0FBZixFQUEwQmlELE9BQTFCLENBQVA7QUFDQSxFQVJEOztBQVVBOzs7Ozs7O0FBT0FwRCxTQUFReUMsT0FBUixHQUFrQixVQUFTdEMsU0FBVCxFQUE4QztBQUFBLE1BQTFCa0QsbUJBQTBCLHVFQUFKLEVBQUk7O0FBQy9ELE1BQUlBLHVCQUF1QkEsd0JBQXdCLHdCQUFuRCxFQUE2RTtBQUM1RUMsS0FBRUMsTUFBRixFQUFVQyxHQUFWLENBQWNILG1CQUFkO0FBQ0E7O0FBRUQsTUFBTU4sT0FBTzVDLFVBQVVzRCxJQUFWLENBQWUsWUFBZixDQUFiOztBQUVBLE1BQUlqQixZQUFZTyxJQUFaLE1BQXNCQyxTQUExQixFQUFxQztBQUNwQyxTQUFNLElBQUlDLEtBQUosQ0FBVSw0Q0FBNENGLElBQXRELENBQU47QUFDQTs7QUFFRFAsY0FBWU8sSUFBWixFQUFrQjVDLFNBQWxCO0FBQ0EsRUFaRDs7QUFjQTs7Ozs7OztBQU9BSCxTQUFRNkMsV0FBUixHQUFzQixVQUFTMUMsU0FBVCxFQUFvQjtBQUN6QyxNQUFNNEMsT0FBTzVDLFVBQVVzRCxJQUFWLENBQWUsWUFBZixDQUFiOztBQUVBLE1BQUlqQixZQUFZTyxJQUFaLE1BQXNCQyxTQUExQixFQUFxQztBQUNwQyxTQUFNLElBQUlDLEtBQUosQ0FBVSw0Q0FBNENGLElBQXRELENBQU47QUFDQTs7QUFFRCxTQUFPRixZQUFZRSxJQUFaLEVBQWtCNUMsU0FBbEIsQ0FBUDtBQUNBLEVBUkQ7QUFVQSxDQXBLRCxFQW9LR04sSUFBSUMsSUFBSixDQUFTQyxnQkFwS1oiLCJmaWxlIjoiZWRpdG9yX2luc3RhbmNlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZWRpdG9yX2luc3RhbmNlcy5qcyAyMDE2LTA5LTA5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuanNlLmxpYnMuZWRpdG9yX2luc3RhbmNlcyA9IGpzZS5saWJzLmVkaXRvcl9pbnN0YW5jZXMgfHwge307XG5cbi8qKlxuICogIyMgRWRpdG9yIEluc3RhbmNlcyBMaWJyYXJ5XG4gKlxuICogVGhpcyBsaWJyYXJ5IHByb3ZpZGVzIGEgY29tbW9uIEFQSSBmb3IgZWRpdG9yIHdpZGdldCBpbnN0YW5jZXMgbWFuaXB1bGF0aW9uLiBcbiAqIFxuICogQG1vZHVsZSBBZG1pbi9MaWJzL2VkaXRvcl9pbnN0YW5jZXNcbiAqIEBleHBvcnRzIGpzZS5saWJzLmVkaXRvcl9pbnN0YW5jZXNcbiAqL1xuKGZ1bmN0aW9uKGV4cG9ydHMpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8qKlxuXHQgKiBFZGl0b3IgY29uc3RydWN0IG1ldGhvZHNcblx0ICpcblx0ICogQHR5cGUge09iamVjdH1cblx0ICovXG5cdGNvbnN0IGNvbnN0cnVjdG9ycyA9IHtcblx0XHRja2VkaXRvcigkdGV4dGFyZWEpIHtcblx0XHRcdGNvbnN0IHRhZ3MgPSBbXG5cdFx0XHRcdCdwJyxcblx0XHRcdFx0J2RpdicsIFxuXHRcdFx0XHQnc2NyaXB0JywgXG5cdFx0XHRcdCdzdHlsZScsIFxuXHRcdFx0XHQnZm9ybSdcblx0XHRcdF07XG5cdFx0XHRcblx0XHRcdGNvbnN0IGZvcm1hdHRpbmdSdWxlcyA9IHtcblx0XHRcdFx0aW5kZW50IDogdHJ1ZSxcblx0XHRcdFx0YnJlYWtCZWZvcmVPcGVuIDogdHJ1ZSxcblx0XHRcdFx0YnJlYWtBZnRlck9wZW4gOiB0cnVlLFxuXHRcdFx0XHRicmVha0JlZm9yZUNsb3NlIDogdHJ1ZSxcblx0XHRcdFx0YnJlYWtBZnRlckNsb3NlIDogdHJ1ZVxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Y29uc3QgY29uZmlnID0ge1xuXHRcdFx0XHRmaWxlYnJvd3NlckJyb3dzZVVybDogJ2luY2x1ZGVzL2NrZWRpdG9yL2ZpbGVtYW5hZ2VyL2luZGV4Lmh0bWwnLFxuXHRcdFx0XHRiYXNlSHJlZjoganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluJyxcblx0XHRcdFx0ZW50ZXJNb2RlOiBDS0VESVRPUi5FTlRFUl9CUixcblx0XHRcdFx0c2hpZnRFbnRlck1vZGU6IENLRURJVE9SLkVOVEVSX1AsXG5cdFx0XHRcdGxhbmd1YWdlOiBqc2UuY29yZS5jb25maWcuZ2V0KCdsYW5ndWFnZUNvZGUnKSxcblx0XHRcdFx0dXNlUmVsUGF0aDogdHJ1ZSxcblx0XHRcdFx0b246IHtcblx0XHRcdFx0XHRpbnN0YW5jZVJlYWR5KGV2ZW50KSB7XG5cdFx0XHRcdFx0XHRmb3IgKGxldCB0YWcgb2YgdGFncykge1xuXHRcdFx0XHRcdFx0XHR0aGlzLmRhdGFQcm9jZXNzb3Iud3JpdGVyLnNldFJ1bGVzKHRhZywgZm9ybWF0dGluZ1J1bGVzKTsgXG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHRjb25zdCBuYW1lID0gJHRleHRhcmVhLmF0dHIoJ25hbWUnKTtcblx0XHRcdENLRURJVE9SLnJlcGxhY2UobmFtZSwgY29uZmlnKTtcblx0XHRcdHJldHVybiBDS0VESVRPUi5pbnN0YW5jZXNbbmFtZV07XG5cdFx0fSxcblx0XHRjb2RlbWlycm9yKCR0ZXh0YXJlYSkge1xuXHRcdFx0Y29uc3QgY29uZmlnID0ge1xuXHRcdFx0XHRtb2RlOiAnaHRtbG1peGVkJyxcblx0XHRcdFx0bGluZU51bWJlcnM6IHRydWUsXG5cdFx0XHRcdGxpbmVXcmFwcGluZzogdHJ1ZVxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIENvZGVNaXJyb3IuZnJvbVRleHRBcmVhKCR0ZXh0YXJlYVswXSwgY29uZmlnKTtcblx0XHR9XG5cdH07XG5cdFxuXHQvKipcblx0ICogRWRpdG9yIGRlc3RydWN0IG1ldGhvZHNcblx0ICpcblx0ICogQHR5cGUge09iamVjdH1cblx0ICovXG5cdGNvbnN0IGRlc3RydWN0b3JzID0ge1xuXHRcdGNrZWRpdG9yKCR0ZXh0YXJlYSkge1xuXHRcdFx0Y29uc3QgbmFtZSA9ICR0ZXh0YXJlYS5hdHRyKCduYW1lJyk7XG5cdFx0XHRDS0VESVRPUi5pbnN0YW5jZXNbbmFtZV0uZGVzdHJveSgpO1xuXHRcdH0sXG5cdFx0Y29kZW1pcnJvcigkdGV4dGFyZWEpIHtcblx0XHRcdGNvbnN0IGluc3RhbmNlID0gJHRleHRhcmVhLnNpYmxpbmdzKCcuQ29kZU1pcnJvcicpWzBdLkNvZGVNaXJyb3I7XG5cdFx0XHRpbnN0YW5jZS50b1RleHRBcmVhKCk7XG5cdFx0fVxuXHR9O1xuXHRcblx0LyoqXG5cdCAqIEVkaXRvciBpbnN0YW5jZSBtZXRob2RzXG5cdCAqIFxuXHQgKiBAdHlwZSB7T2JqZWN0fVxuXHQgKi9cblx0Y29uc3QgZ2V0SW5zdGFuY2UgPSB7XG5cdFx0Y2tlZGl0b3IoJHRleHRhcmVhKSB7XG5cdFx0XHRyZXR1cm4gQ0tFRElUT1IuaW5zdGFuY2VzWyR0ZXh0YXJlYS5hdHRyKCduYW1lJyldO1xuXHRcdH0sIFxuXHRcdGNvZGVtaXJyb3IoJHRleHRhcmVhKSB7XG5cdFx0XHRyZXR1cm4gJHRleHRhcmVhLnNpYmxpbmdzKCcuQ29kZU1pcnJvcicpWzBdLkNvZGVNaXJyb3I7XG5cdFx0fVxuXHR9O1xuXHRcblx0LyoqXG5cdCAqIENyZWF0ZSBuZXcgZWRpdG9yIGluc3RhbmNlLiBcblx0ICogXG5cdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGV4dGFyZWEgVGV4dGFyZWEgc2VsZWN0b3IgdG8gYmUgbW9kaWZpZWQuXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlIFByb3ZpZGUgYSB0eXBlIHRoYXQgaXMgc3VwcG9ydGVkIGJ5IHRoZSB3aWRnZXQuIFxuXHQgKiBcblx0ICogQHJldHVybiB7Q0tFZGl0b3J8Q29kZU1pcnJvcn0gUmV0dXJucyB0aGUgY3JlYXRlIGVkaXRvciBpbnN0YW5jZS5cblx0ICovXG5cdGV4cG9ydHMuY3JlYXRlID0gZnVuY3Rpb24oJHRleHRhcmVhLCB0eXBlKSB7XG5cdFx0aWYgKGNvbnN0cnVjdG9yc1t0eXBlXSA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1Byb3ZpZGVkIGVkaXRvciB0eXBlIGlzIG5vdCBzdXBwb3J0ZWQ6ICcgKyB0eXBlKTtcblx0XHR9XG5cdFx0XG5cdFx0Y29uc3QgaW5zdGFuY2UgPSBjb25zdHJ1Y3RvcnNbdHlwZV0oJHRleHRhcmVhKTtcblx0XHRpbnN0YW5jZS50eXBlID0gdHlwZTtcblx0XHRcblx0XHRyZXR1cm4gaW5zdGFuY2U7XG5cdH07XG5cdFxuXHQvKipcblx0ICogU3dpdGNoIHRvIGEgbmV3IGVkaXRvciB0eXBlLiBcblx0ICogXG5cdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGV4dGFyZWEgVGV4dGFyZWEgc2VsZWN0b3IgdG8gYmUgbW9kaWZpZWQuXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBjdXJyZW50VHlwZSBQcm92aWRlIHRoZSBjdXJyZW50IGVkaXRvciB0eXBlLiBcblx0ICogQHBhcmFtIHtTdHJpbmd9IG5ld1R5cGUgUHJvdmlkZSB0aGUgbmV3IGVkaXRvciB0eXBlLlxuXHQgKiBcblx0ICogQHJldHVybiB7Q0tFZGl0b3J8Q29kZU1pcnJvcn0gUmV0dXJucyB0aGUgbmV3IGVkaXRvciBpbnN0YW5jZS4gXG5cdCAqL1xuXHRleHBvcnRzLnN3aXRjaCA9IGZ1bmN0aW9uKCR0ZXh0YXJlYSwgY3VycmVudFR5cGUsIG5ld1R5cGUpIHtcblx0XHRpZiAoZGVzdHJ1Y3RvcnNbY3VycmVudFR5cGVdID09PSB1bmRlZmluZWQpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignUHJvdmlkZWQgZWRpdG9yIHR5cGUgaXMgbm90IHN1cHBvcnRlZDogJyArIGN1cnJlbnRUeXBlKTtcblx0XHR9XG5cdFx0XG5cdFx0ZGVzdHJ1Y3RvcnNbY3VycmVudFR5cGVdKCR0ZXh0YXJlYSk7XG5cdFx0XG5cdFx0cmV0dXJuIGV4cG9ydHMuY3JlYXRlKCR0ZXh0YXJlYSwgbmV3VHlwZSk7XG5cdH07XG5cdFxuXHQvKipcblx0ICogRGVzdHJveSBhbiBleGlzdGluZyBlZGl0b3IgaW5zdGFuY2UuXG5cdCAqIFxuXHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRleHRhcmVhIFRleHRhcmVhIHNlbGVjdG9yIHRvIGJlIGRlc3Ryb3llZC5cblx0ICogQHBhcmFtIHtTdHJpbmd9IGN1c3RvbUluaXRFdmVudFR5cGUgT3B0aW9uYWwgKCcnKSwgaWYgdGhlIGVkaXRvciB3YXMgaW5pdGlhbGl6ZWQgd2l0aCBhIGN1c3RvbSBpbml0LWV2ZW50LXR5cGUsIFxuXHQgKiB0aGVuIHRoaXMgbXVzdCBiZSB1bmJvdW5kIGJ5IGVkaXRvciBkZXN0cm95LlxuXHQgKi9cblx0ZXhwb3J0cy5kZXN0cm95ID0gZnVuY3Rpb24oJHRleHRhcmVhLCBjdXN0b21Jbml0RXZlbnRUeXBlID0gJycpIHtcblx0XHRpZiAoY3VzdG9tSW5pdEV2ZW50VHlwZSAmJiBjdXN0b21Jbml0RXZlbnRUeXBlICE9PSAnSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcpIHtcblx0XHRcdCQod2luZG93KS5vZmYoY3VzdG9tSW5pdEV2ZW50VHlwZSk7XG5cdFx0fVxuXHRcdFxuXHRcdGNvbnN0IHR5cGUgPSAkdGV4dGFyZWEuZGF0YSgnZWRpdG9yVHlwZScpOyBcblx0XHRcblx0XHRpZiAoZGVzdHJ1Y3RvcnNbdHlwZV0gPT09IHVuZGVmaW5lZCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdQcm92aWRlZCBlZGl0b3IgdHlwZSBpcyBub3Qgc3VwcG9ydGVkOiAnICsgdHlwZSk7XG5cdFx0fVxuXHRcdFxuXHRcdGRlc3RydWN0b3JzW3R5cGVdKCR0ZXh0YXJlYSk7XG5cdH07XG5cdFxuXHQvKipcblx0ICogR2V0IGFuIGVkaXRvciBpbnN0YW5jZS4gXG5cdCAqIFxuXHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRleHRhcmVhIFRleHRhcmVhIHNlbGVjdG9yIGZyb20gd2hpY2ggdGhlIGluc3RhbmNlIHdpbGwgYmUgcmV0cmlldmVkLlxuXHQgKiBcblx0ICogQHJldHVybiB7Q0tFZGl0b3J8Q29kZU1pcnJvcn0gUmV0dXJucyB0aGUgY29ycmVzcG9uZGluZyBlZGl0b3IgaW5zdGFuY2UuIFxuXHQgKi9cblx0ZXhwb3J0cy5nZXRJbnN0YW5jZSA9IGZ1bmN0aW9uKCR0ZXh0YXJlYSkge1xuXHRcdGNvbnN0IHR5cGUgPSAkdGV4dGFyZWEuZGF0YSgnZWRpdG9yVHlwZScpO1xuXHRcdFxuXHRcdGlmIChkZXN0cnVjdG9yc1t0eXBlXSA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1Byb3ZpZGVkIGVkaXRvciB0eXBlIGlzIG5vdCBzdXBwb3J0ZWQ6ICcgKyB0eXBlKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIGdldEluc3RhbmNlW3R5cGVdKCR0ZXh0YXJlYSk7XG5cdH07XG5cdFxufSkoanNlLmxpYnMuZWRpdG9yX2luc3RhbmNlcyk7Il19
