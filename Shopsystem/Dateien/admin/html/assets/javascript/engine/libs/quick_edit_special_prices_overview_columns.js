'use strict';

/* --------------------------------------------------------------
 quick_edit_special_prices_overview_columns.js 2016-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.quick_edit_special_prices_overview_columns = jse.libs.quick_edit_special_prices_overview_columns || {};

(function (exports) {

	'use strict';

	exports.checkbox = exports.checkbox || {
		data: null,
		minWidth: '50px',
		widthFactor: 0.01,
		orderable: false,
		searchable: false,
		render: function render() {
			return '<input type="checkbox" class="special-price-row-selection" />';
		}
	};

	exports.productsName = exports.productsName || {
		data: 'productsName',
		minWidth: '160px',
		widthFactor: 1.6
	};

	exports.productsModel = exports.productsModel || {
		data: 'productsModel',
		minWidth: '140px',
		widthFactor: 1.3
	};

	exports.productsPrice = exports.productsPrice || {
		data: 'productsPrice',
		minWidth: '100px',
		widthFactor: 1,
		className: 'numeric'
	};

	exports.specialPrice = exports.specialPrice || {
		data: 'specialPrice',
		minWidth: '110px',
		widthFactor: 1,
		className: 'numeric editable'
	};

	exports.specialPriceQuantity = exports.specialPriceQuantity || {
		data: 'specialPriceQuantity',
		minWidth: '90px',
		widthFactor: 1,
		className: 'numeric editable'
	};

	exports.specialPriceExpiresDate = exports.specialPriceExpiresDate || {
		data: 'specialPriceExpiresDate',
		minWidth: '110px',
		widthFactor: 1.1,
		className: 'numeric editable date'
	};

	exports.specialPriceStatus = exports.specialPriceStatus || {
		data: 'specialPriceStatus',
		minWidth: '90px',
		widthFactor: 1.2,
		className: 'status',
		searchable: false,
		render: function render(data) {
			return '<input type="checkbox" ' + (data ? 'checked' : '') + ' class="convert-to-switcher" />';
		}
	};

	exports.actions = exports.actions || {
		data: null,
		minWidth: '400px',
		widthFactor: 3.2,
		className: 'actions',
		orderable: false,
		searchable: false,
		render: function render(data, type, full, meta) {
			return '\t\t\t\t\t\n\t\t\t\t\t<div class="pull-left"></div>\n\t\t\t\t\t<div class="pull-right action-list visible-on-hover">\n\t\t\t\t\t\t<div class="btn-group dropdown">\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default"></button>\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default dropdown-toggle"\n\t\t\t\t\t\t\t\t\tdata-toggle="dropdown"\n\t\t\t\t\t\t\t\t\taria-haspopup="true"\n\t\t\t\t\t\t\t\t\taria-expanded="false">\n\t\t\t\t\t\t\t\t<span class="caret"></span>\n\t\t\t\t\t\t\t\t<span class="sr-only">Toggle Dropdown</span>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t<ul class="dropdown-menu dropdown-menu-right"></ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t';
		}
	};
})(jse.libs.quick_edit_special_prices_overview_columns);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXRfc3BlY2lhbF9wcmljZXNfb3ZlcnZpZXdfY29sdW1ucy5qcyJdLCJuYW1lcyI6WyJqc2UiLCJsaWJzIiwicXVpY2tfZWRpdF9zcGVjaWFsX3ByaWNlc19vdmVydmlld19jb2x1bW5zIiwiZXhwb3J0cyIsImNoZWNrYm94IiwiZGF0YSIsIm1pbldpZHRoIiwid2lkdGhGYWN0b3IiLCJvcmRlcmFibGUiLCJzZWFyY2hhYmxlIiwicmVuZGVyIiwicHJvZHVjdHNOYW1lIiwicHJvZHVjdHNNb2RlbCIsInByb2R1Y3RzUHJpY2UiLCJjbGFzc05hbWUiLCJzcGVjaWFsUHJpY2UiLCJzcGVjaWFsUHJpY2VRdWFudGl0eSIsInNwZWNpYWxQcmljZUV4cGlyZXNEYXRlIiwic3BlY2lhbFByaWNlU3RhdHVzIiwiYWN0aW9ucyIsInR5cGUiLCJmdWxsIiwibWV0YSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxJQUFJQyxJQUFKLENBQVNDLDBDQUFULEdBQXNERixJQUFJQyxJQUFKLENBQVNDLDBDQUFULElBQXVELEVBQTdHOztBQUVBLENBQUMsVUFBU0MsT0FBVCxFQUFrQjs7QUFFbEI7O0FBRUFBLFNBQVFDLFFBQVIsR0FBbUJELFFBQVFDLFFBQVIsSUFBb0I7QUFDckNDLFFBQU0sSUFEK0I7QUFFckNDLFlBQVUsTUFGMkI7QUFHckNDLGVBQWEsSUFId0I7QUFJckNDLGFBQVcsS0FKMEI7QUFLckNDLGNBQVksS0FMeUI7QUFNckNDLFFBTnFDLG9CQU01QjtBQUNSO0FBQ0E7QUFSb0MsRUFBdkM7O0FBV0FQLFNBQVFRLFlBQVIsR0FBdUJSLFFBQVFRLFlBQVIsSUFBd0I7QUFDN0NOLFFBQU0sY0FEdUM7QUFFN0NDLFlBQVUsT0FGbUM7QUFHN0NDLGVBQWE7QUFIZ0MsRUFBL0M7O0FBTUFKLFNBQVFTLGFBQVIsR0FBd0JULFFBQVFTLGFBQVIsSUFBeUI7QUFDL0NQLFFBQU0sZUFEeUM7QUFFL0NDLFlBQVUsT0FGcUM7QUFHL0NDLGVBQWE7QUFIa0MsRUFBakQ7O0FBTUFKLFNBQVFVLGFBQVIsR0FBd0JWLFFBQVFVLGFBQVIsSUFBeUI7QUFDL0NSLFFBQU0sZUFEeUM7QUFFL0NDLFlBQVUsT0FGcUM7QUFHL0NDLGVBQWEsQ0FIa0M7QUFJL0NPLGFBQVc7QUFKb0MsRUFBakQ7O0FBT0FYLFNBQVFZLFlBQVIsR0FBdUJaLFFBQVFZLFlBQVIsSUFBd0I7QUFDN0NWLFFBQU0sY0FEdUM7QUFFN0NDLFlBQVUsT0FGbUM7QUFHN0NDLGVBQWEsQ0FIZ0M7QUFJN0NPLGFBQVc7QUFKa0MsRUFBL0M7O0FBT0FYLFNBQVFhLG9CQUFSLEdBQStCYixRQUFRYSxvQkFBUixJQUFnQztBQUM3RFgsUUFBTSxzQkFEdUQ7QUFFN0RDLFlBQVUsTUFGbUQ7QUFHN0RDLGVBQWEsQ0FIZ0Q7QUFJN0RPLGFBQVc7QUFKa0QsRUFBL0Q7O0FBT0FYLFNBQVFjLHVCQUFSLEdBQWtDZCxRQUFRYyx1QkFBUixJQUFtQztBQUNuRVosUUFBTSx5QkFENkQ7QUFFbkVDLFlBQVUsT0FGeUQ7QUFHbkVDLGVBQWEsR0FIc0Q7QUFJbkVPLGFBQVc7QUFKd0QsRUFBckU7O0FBT0FYLFNBQVFlLGtCQUFSLEdBQTZCZixRQUFRZSxrQkFBUixJQUE4QjtBQUN6RGIsUUFBTSxvQkFEbUQ7QUFFekRDLFlBQVUsTUFGK0M7QUFHekRDLGVBQWEsR0FINEM7QUFJekRPLGFBQVcsUUFKOEM7QUFLekRMLGNBQVksS0FMNkM7QUFNekRDLFFBTnlELGtCQU1sREwsSUFOa0QsRUFNNUM7QUFDWix1Q0FBaUNBLE9BQU8sU0FBUCxHQUFtQixFQUFwRDtBQUNBO0FBUndELEVBQTNEOztBQVdBRixTQUFRZ0IsT0FBUixHQUFrQmhCLFFBQVFnQixPQUFSLElBQW1CO0FBQ25DZCxRQUFNLElBRDZCO0FBRW5DQyxZQUFVLE9BRnlCO0FBR25DQyxlQUFhLEdBSHNCO0FBSW5DTyxhQUFXLFNBSndCO0FBS25DTixhQUFXLEtBTHdCO0FBTW5DQyxjQUFZLEtBTnVCO0FBT25DQyxRQVBtQyxrQkFPNUJMLElBUDRCLEVBT3RCZSxJQVBzQixFQU9oQkMsSUFQZ0IsRUFPVkMsSUFQVSxFQU9KO0FBQzlCO0FBa0JBO0FBMUJrQyxFQUFyQztBQTZCQSxDQS9GRCxFQStGR3RCLElBQUlDLElBQUosQ0FBU0MsMENBL0ZaIiwiZmlsZSI6InF1aWNrX2VkaXRfc3BlY2lhbF9wcmljZXNfb3ZlcnZpZXdfY29sdW1ucy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gcXVpY2tfZWRpdF9zcGVjaWFsX3ByaWNlc19vdmVydmlld19jb2x1bW5zLmpzIDIwMTYtMDktMjlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UubGlicy5xdWlja19lZGl0X3NwZWNpYWxfcHJpY2VzX292ZXJ2aWV3X2NvbHVtbnMgPSBqc2UubGlicy5xdWlja19lZGl0X3NwZWNpYWxfcHJpY2VzX292ZXJ2aWV3X2NvbHVtbnMgfHwge307XG5cbihmdW5jdGlvbihleHBvcnRzKSB7XG5cdFxuXHQndXNlIHN0cmljdCc7XG5cdFxuXHRleHBvcnRzLmNoZWNrYm94ID0gZXhwb3J0cy5jaGVja2JveCB8fCB7XG5cdFx0XHRkYXRhOiBudWxsLFxuXHRcdFx0bWluV2lkdGg6ICc1MHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAwLjAxLFxuXHRcdFx0b3JkZXJhYmxlOiBmYWxzZSxcblx0XHRcdHNlYXJjaGFibGU6IGZhbHNlLFxuXHRcdFx0cmVuZGVyKCkge1xuXHRcdFx0XHRyZXR1cm4gYDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cInNwZWNpYWwtcHJpY2Utcm93LXNlbGVjdGlvblwiIC8+YFxuXHRcdFx0fVxuXHRcdH07XG5cdFxuXHRleHBvcnRzLnByb2R1Y3RzTmFtZSA9IGV4cG9ydHMucHJvZHVjdHNOYW1lIHx8IHtcblx0XHRcdGRhdGE6ICdwcm9kdWN0c05hbWUnLFxuXHRcdFx0bWluV2lkdGg6ICcxNjBweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMS42XG5cdFx0fTtcblx0XG5cdGV4cG9ydHMucHJvZHVjdHNNb2RlbCA9IGV4cG9ydHMucHJvZHVjdHNNb2RlbCB8fCB7XG5cdFx0XHRkYXRhOiAncHJvZHVjdHNNb2RlbCcsXG5cdFx0XHRtaW5XaWR0aDogJzE0MHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjMsXG5cdFx0fTtcblx0XG5cdGV4cG9ydHMucHJvZHVjdHNQcmljZSA9IGV4cG9ydHMucHJvZHVjdHNQcmljZSB8fCB7XG5cdFx0XHRkYXRhOiAncHJvZHVjdHNQcmljZScsXG5cdFx0XHRtaW5XaWR0aDogJzEwMHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLFxuXHRcdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYydcblx0XHR9O1xuXHRcblx0ZXhwb3J0cy5zcGVjaWFsUHJpY2UgPSBleHBvcnRzLnNwZWNpYWxQcmljZSB8fCB7XG5cdFx0XHRkYXRhOiAnc3BlY2lhbFByaWNlJyxcblx0XHRcdG1pbldpZHRoOiAnMTEwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEsXG5cdFx0XHRjbGFzc05hbWU6ICdudW1lcmljIGVkaXRhYmxlJ1xuXHRcdH07XG5cdFxuXHRleHBvcnRzLnNwZWNpYWxQcmljZVF1YW50aXR5ID0gZXhwb3J0cy5zcGVjaWFsUHJpY2VRdWFudGl0eSB8fCB7XG5cdFx0XHRkYXRhOiAnc3BlY2lhbFByaWNlUXVhbnRpdHknLFxuXHRcdFx0bWluV2lkdGg6ICc5MHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLFxuXHRcdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYyBlZGl0YWJsZScsXG5cdFx0fTtcblx0XG5cdGV4cG9ydHMuc3BlY2lhbFByaWNlRXhwaXJlc0RhdGUgPSBleHBvcnRzLnNwZWNpYWxQcmljZUV4cGlyZXNEYXRlIHx8IHtcblx0XHRcdGRhdGE6ICdzcGVjaWFsUHJpY2VFeHBpcmVzRGF0ZScsXG5cdFx0XHRtaW5XaWR0aDogJzExMHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjEsXG5cdFx0XHRjbGFzc05hbWU6ICdudW1lcmljIGVkaXRhYmxlIGRhdGUnXG5cdFx0fTtcblx0XG5cdGV4cG9ydHMuc3BlY2lhbFByaWNlU3RhdHVzID0gZXhwb3J0cy5zcGVjaWFsUHJpY2VTdGF0dXMgfHwge1xuXHRcdFx0ZGF0YTogJ3NwZWNpYWxQcmljZVN0YXR1cycsXG5cdFx0XHRtaW5XaWR0aDogJzkwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEuMixcblx0XHRcdGNsYXNzTmFtZTogJ3N0YXR1cycsXG5cdFx0XHRzZWFyY2hhYmxlOiBmYWxzZSxcblx0XHRcdHJlbmRlcihkYXRhKSB7XG5cdFx0XHRcdHJldHVybiBgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiICR7ZGF0YSA/ICdjaGVja2VkJyA6ICcnfSBjbGFzcz1cImNvbnZlcnQtdG8tc3dpdGNoZXJcIiAvPmBcblx0XHRcdH1cblx0XHR9O1xuXHRcblx0ZXhwb3J0cy5hY3Rpb25zID0gZXhwb3J0cy5hY3Rpb25zIHx8IHtcblx0XHRcdGRhdGE6IG51bGwsXG5cdFx0XHRtaW5XaWR0aDogJzQwMHB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAzLjIsXG5cdFx0XHRjbGFzc05hbWU6ICdhY3Rpb25zJyxcblx0XHRcdG9yZGVyYWJsZTogZmFsc2UsXG5cdFx0XHRzZWFyY2hhYmxlOiBmYWxzZSxcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XG5cdFx0XHRcdHJldHVybiBgXHRcdFx0XHRcdFxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJwdWxsLWxlZnRcIj48L2Rpdj5cblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicHVsbC1yaWdodCBhY3Rpb24tbGlzdCB2aXNpYmxlLW9uLWhvdmVyXCI+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwIGRyb3Bkb3duXCI+XG5cdFx0XHRcdFx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiXG5cdFx0XHRcdFx0XHRcdFx0XHRjbGFzcz1cImJ0biBidG4tZGVmYXVsdFwiPjwvYnV0dG9uPlxuXHRcdFx0XHRcdFx0XHQ8YnV0dG9uIHR5cGU9XCJidXR0b25cIlxuXHRcdFx0XHRcdFx0XHRcdFx0Y2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgZHJvcGRvd24tdG9nZ2xlXCJcblx0XHRcdFx0XHRcdFx0XHRcdGRhdGEtdG9nZ2xlPVwiZHJvcGRvd25cIlxuXHRcdFx0XHRcdFx0XHRcdFx0YXJpYS1oYXNwb3B1cD1cInRydWVcIlxuXHRcdFx0XHRcdFx0XHRcdFx0YXJpYS1leHBhbmRlZD1cImZhbHNlXCI+XG5cdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJjYXJldFwiPjwvc3Bhbj5cblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cInNyLW9ubHlcIj5Ub2dnbGUgRHJvcGRvd248L3NwYW4+XG5cdFx0XHRcdFx0XHRcdDwvYnV0dG9uPlxuXHRcdFx0XHRcdFx0XHQ8dWwgY2xhc3M9XCJkcm9wZG93bi1tZW51IGRyb3Bkb3duLW1lbnUtcmlnaHRcIj48L3VsPlxuXHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdGA7XG5cdFx0XHR9XG5cdFx0fTtcblx0XG59KShqc2UubGlicy5xdWlja19lZGl0X3NwZWNpYWxfcHJpY2VzX292ZXJ2aWV3X2NvbHVtbnMpOyJdfQ==
