'use strict';

/* --------------------------------------------------------------
 orders_overview_columns.js 2018-01-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.orders_overview_columns = jse.libs.orders_overview_columns || {};

/**
 * ## Orders Table Column Definitions
 *
 * This module defines the column definition of the order overview table. They can be overridden by other
 * scripts by modifying the array with new columns, or by replacing the property values of the contained
 * fields.
 *
 * @module Admin/Libs/orders_overview_columns
 * @exports jse.libs.orders_overview_columns
 * @requires momentjs
 */
(function (exports) {

	'use strict';

	exports.checkbox = exports.checkbox || {
		data: null,
		minWidth: '50px',
		widthFactor: 0.01,
		orderable: false,
		searchable: false,
		defaultContent: '<input type="checkbox" />'
	};

	exports.number = exports.number || {
		data: 'number',
		minWidth: '75px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			var linkElement = '';

			if (full.DT_RowData.comment !== '') {
				// Remove double quotes to avoid broken tooltips
				var escapedComment = full.DT_RowData.comment.replace(/"/, '');

				linkElement += '\n\t\t\t\t\t\t\t\t\t<i class="fa fa-comment-o tooltip-order-comment tooltip-trigger"\n\t\t\t\t\t\t\t\t\t\taria-hidden="true" title="' + escapedComment + '"></i>&nbsp;\n\t\t\t\t\t\t\t\t';
			}

			var editUrl = 'orders.php?' + $.param({
				oID: full.DT_RowData.id,
				action: 'edit',
				overview: $.deparam(window.location.search.slice(1))
			});

			linkElement += '\n\t\t\t\t\t\t\t\t<a class="tooltip-order-items" href="' + editUrl + '">\n\t\t\t\t\t\t\t\t\t' + full.DT_RowData.id + '\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t';

			return linkElement;
		}
	};

	exports.customer = exports.customer || {
		data: 'customer',
		minWidth: '190px',
		widthFactor: 1.5,
		render: function render(data, type, full, meta) {
			var linkElement = full.DT_RowData.customerId ? '<a class="tooltip-customer-addresses" \n\t\t\t\t\t\t\thref="customers.php?cID=' + full.DT_RowData.customerId + '&action=edit">' + data + '</a>' : '<span class="tooltip-customer-addresses">' + data + '</span>';

			if (full.DT_RowData.customerMemos.length > 0) {
				linkElement += ' <i class="fa fa-sticky-note-o tooltip-customer-memos tooltip-trigger" \n                                aria-hidden="true"></i>';
			}

			return linkElement;
		}
	};

	exports.group = exports.group || {
		data: 'group',
		minWidth: '85px',
		widthFactor: 1.2
	};

	exports.sum = exports.sum || {
		data: 'sum',
		minWidth: '90px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			return '<span class="tooltip-order-sum-block">' + data + '</span>';
		}
	};

	exports.paymentMethod = exports.paymentMethod || {
		data: 'paymentMethod',
		minWidth: '110px',
		widthFactor: 2,
		render: function render(data, type, full, meta) {
			return '<span title="' + full.DT_RowData.paymentMethod + '">' + data + '</span>';
		}
	};

	exports.shippingMethod = exports.shippingMethod || {
		data: 'shippingMethod',
		minWidth: '110px',
		widthFactor: 2,
		className: 'shipping-method',
		render: function render(data, type, full, meta) {
			return '<span title="' + full.DT_RowData.shippingMethod + '">' + data + '</span>' + (full.DT_RowData.trackingLinks.length ? ' <i class="fa fa-truck fa-lg tooltip-tracking-links tooltip-trigger"></i>' : '');
		},
		createdCell: function createdCell(td, cellData, rowData, row, col) {
			rowData.DT_RowData.trackingLinks.length ? $(td).children(':first').data('orderId', rowData.DT_RowData.id).attr('data-toggle', 'modal').attr('data-target', '.add-tracking-number.modal') : $(td).data('orderId', rowData.DT_RowData.id).attr('data-toggle', 'modal').attr('data-target', '.add-tracking-number.modal');
		}
	};

	exports.countryIsoCode = exports.countryIsoCode || {
		data: 'countryIsoCode',
		minWidth: '75px',
		widthFactor: 1.4,
		render: function render(data, type, full, meta) {
			var html = '';

			if (data) {
				html = '<img src="' + jse.core.config.get('appUrl') + '/images/icons/flags/' + data.toLowerCase() + '.png" />&nbsp;';
			}

			var title = jse.core.lang.translate('SHIPPING_ORIGIN_COUNTRY_TITLE', 'configuration') + ': ' + full.DT_RowData.country;

			html += '<span title="' + title + '">' + data + '</span>';

			return html;
		}
	};

	exports.date = exports.date || {
		data: 'date',
		minWidth: '100px',
		widthFactor: 1.6,
		render: function render(data, type, full, meta) {
			return moment(data).format('DD.MM.YY - HH:mm');
		}
	};

	exports.status = exports.status || {
		data: 'status',
		minWidth: '120px',
		widthFactor: 2,
		render: function render(data, type, full, meta) {
			return '\n\t\t\t\t\t<span data-toggle="modal" data-target=".status.modal"\n\t\t\t\t\t\t\tclass="order-status tooltip-order-status-history label label-' + full.DT_RowData.statusId + '">\n\t\t\t\t\t\t' + data + '\n\t\t\t\t\t</span>\n\t\t\t\t';
		}
	};

	exports.totalWeight = exports.totalWeight || {
		data: 'totalWeight',
		minWidth: '50px',
		widthFactor: 0.6,
		className: 'numeric'
	};

	exports.invoiceNumber = exports.invoiceNumber || {
		data: 'invoiceNumber',
		minWidth: '75px',
		widthFactor: 1,
		render: function render(data, type, full, meta) {
			// Create a 'span' container element.
			var $html = document.createElement('span');

			// Iterator function to add a link element into container.
			var _addLinkElement = function _addLinkElement(invoiceNumber, index, array) {
				// Is the current iteration the last one?
				var isLastIteration = index === array.length - 1;

				// Invoice link parameters.
				var parameters = {
					module: 'OrderAdmin',
					action: 'showPdf',
					type: 'invoice',
					order_id: full.DT_RowData.id,
					invoice_number: invoiceNumber
				};

				// Compound invoice link.
				var url = jse.core.config.get('appUrl') + '/admin/request_port.php?' + $.param(parameters);

				// Create link element.
				var $link = document.createElement('a');

				// Set link on element.
				$link.setAttribute('href', url);
				$link.setAttribute('target', '_blank');

				// Set invoice number as text on element.
				$link.textContent = invoiceNumber + (isLastIteration ? '' : ', ');

				// Append element to container.
				$html.appendChild($link);
			};

			// Add tooltip classes to element.
			$html.classList.add('tooltip-invoices', 'tooltip-trigger');

			// Iterate over each invoice number and create link.
			full.DT_RowData.invoiceNumbers.forEach(_addLinkElement);

			return $html.outerHTML;
		}
	};

	exports.actions = exports.actions || {
		data: null,
		minWidth: '350px',
		widthFactor: 4.6,
		className: 'actions',
		orderable: false,
		searchable: false,
		render: function render(data, type, full, meta) {
			var withdrawalIdsHtml = '';

			full.DT_RowData.withdrawalIds.forEach(function (withdrawalId) {
				withdrawalIdsHtml += '\n\t\t\t\t\t\t<a href="withdrawals.php?id=' + withdrawalId + '&action=edit" \n\t\t\t\t\t\t\t\ttitle="' + jse.core.lang.translate('TABLE_HEADING_WITHDRAWAL_ID', 'orders') + ' ' + withdrawalId + '">\n\t\t\t\t\t\t\t<img src="html/assets/images/legacy/icons/withdrawal-on.png" \n\t\t\t\t\t\t\t\tclass="tooltip-withdrawal tooltip-trigger meta-icon" \n\t\t\t\t\t\t\t\tdata-withdrawal-id="' + withdrawalId + '" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t';
			});

			var editUrl = 'orders.php?' + $.param({
				oID: full.DT_RowData.id,
				action: 'edit',
				overview: $.deparam(window.location.search.slice(1))
			});

			var mailStatusHtml = !full.DT_RowData.mailStatus ? '<i class="fa fa-envelope-o meta-icon tooltip-confirmation-not-sent email-order tooltip-trigger"\n\t\t\t\t\t\ttitle="' + jse.core.lang.translate('TEXT_CONFIRMATION_NOT_SENT', 'orders') + '"></i>' : '';

			return '\n\t\t\t\t\t<div class="pull-left">\n\t\t\t\t\t\t' + withdrawalIdsHtml + '\n\t\t\t\t\t\t' + mailStatusHtml + '\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t\t<div class="pull-right action-list visible-on-hover">\n\t\t\t\t\t\t<a href="' + editUrl + '">\n\t\t\t\t\t\t\t<i class="fa fa-eye edit"></i>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<i class="fa fa-trash-o delete"></i>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class="btn-group dropdown">\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default"></button>\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default dropdown-toggle"\n\t\t\t\t\t\t\t\t\tdata-toggle="dropdown"\n\t\t\t\t\t\t\t\t\taria-haspopup="true"\n\t\t\t\t\t\t\t\t\taria-expanded="false">\n\t\t\t\t\t\t\t\t<span class="caret"></span>\n\t\t\t\t\t\t\t\t<span class="sr-only">Toggle Dropdown</span>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t<ul class="dropdown-menu dropdown-menu-right"></ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t';
		}
	};
})(jse.libs.orders_overview_columns);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVyc19vdmVydmlld19jb2x1bW5zLmpzIl0sIm5hbWVzIjpbImpzZSIsImxpYnMiLCJvcmRlcnNfb3ZlcnZpZXdfY29sdW1ucyIsImV4cG9ydHMiLCJjaGVja2JveCIsImRhdGEiLCJtaW5XaWR0aCIsIndpZHRoRmFjdG9yIiwib3JkZXJhYmxlIiwic2VhcmNoYWJsZSIsImRlZmF1bHRDb250ZW50IiwibnVtYmVyIiwiY2xhc3NOYW1lIiwicmVuZGVyIiwidHlwZSIsImZ1bGwiLCJtZXRhIiwibGlua0VsZW1lbnQiLCJEVF9Sb3dEYXRhIiwiY29tbWVudCIsImVzY2FwZWRDb21tZW50IiwicmVwbGFjZSIsImVkaXRVcmwiLCIkIiwicGFyYW0iLCJvSUQiLCJpZCIsImFjdGlvbiIsIm92ZXJ2aWV3IiwiZGVwYXJhbSIsIndpbmRvdyIsImxvY2F0aW9uIiwic2VhcmNoIiwic2xpY2UiLCJjdXN0b21lciIsImN1c3RvbWVySWQiLCJjdXN0b21lck1lbW9zIiwibGVuZ3RoIiwiZ3JvdXAiLCJzdW0iLCJwYXltZW50TWV0aG9kIiwic2hpcHBpbmdNZXRob2QiLCJ0cmFja2luZ0xpbmtzIiwiY3JlYXRlZENlbGwiLCJ0ZCIsImNlbGxEYXRhIiwicm93RGF0YSIsInJvdyIsImNvbCIsImNoaWxkcmVuIiwiYXR0ciIsImNvdW50cnlJc29Db2RlIiwiaHRtbCIsImNvcmUiLCJjb25maWciLCJnZXQiLCJ0b0xvd2VyQ2FzZSIsInRpdGxlIiwibGFuZyIsInRyYW5zbGF0ZSIsImNvdW50cnkiLCJkYXRlIiwibW9tZW50IiwiZm9ybWF0Iiwic3RhdHVzIiwic3RhdHVzSWQiLCJ0b3RhbFdlaWdodCIsImludm9pY2VOdW1iZXIiLCIkaHRtbCIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsIl9hZGRMaW5rRWxlbWVudCIsImluZGV4IiwiYXJyYXkiLCJpc0xhc3RJdGVyYXRpb24iLCJwYXJhbWV0ZXJzIiwibW9kdWxlIiwib3JkZXJfaWQiLCJpbnZvaWNlX251bWJlciIsInVybCIsIiRsaW5rIiwic2V0QXR0cmlidXRlIiwidGV4dENvbnRlbnQiLCJhcHBlbmRDaGlsZCIsImNsYXNzTGlzdCIsImFkZCIsImludm9pY2VOdW1iZXJzIiwiZm9yRWFjaCIsIm91dGVySFRNTCIsImFjdGlvbnMiLCJ3aXRoZHJhd2FsSWRzSHRtbCIsIndpdGhkcmF3YWxJZHMiLCJ3aXRoZHJhd2FsSWQiLCJtYWlsU3RhdHVzSHRtbCIsIm1haWxTdGF0dXMiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQyx1QkFBVCxHQUFtQ0YsSUFBSUMsSUFBSixDQUFTQyx1QkFBVCxJQUFvQyxFQUF2RTs7QUFFQTs7Ozs7Ozs7Ozs7QUFXQSxDQUFDLFVBQVNDLE9BQVQsRUFBa0I7O0FBRWxCOztBQUVBQSxTQUFRQyxRQUFSLEdBQW1CRCxRQUFRQyxRQUFSLElBQW9CO0FBQ3JDQyxRQUFNLElBRCtCO0FBRXJDQyxZQUFVLE1BRjJCO0FBR3JDQyxlQUFhLElBSHdCO0FBSXJDQyxhQUFXLEtBSjBCO0FBS3JDQyxjQUFZLEtBTHlCO0FBTXJDQyxrQkFBZ0I7QUFOcUIsRUFBdkM7O0FBU0FQLFNBQVFRLE1BQVIsR0FBaUJSLFFBQVFRLE1BQVIsSUFBa0I7QUFDakNOLFFBQU0sUUFEMkI7QUFFakNDLFlBQVUsTUFGdUI7QUFHakNDLGVBQWEsQ0FIb0I7QUFJakNLLGFBQVcsU0FKc0I7QUFLakNDLFFBTGlDLGtCQUsxQlIsSUFMMEIsRUFLcEJTLElBTG9CLEVBS2RDLElBTGMsRUFLUkMsSUFMUSxFQUtGO0FBQzlCLE9BQUlDLGNBQWMsRUFBbEI7O0FBRUEsT0FBSUYsS0FBS0csVUFBTCxDQUFnQkMsT0FBaEIsS0FBNEIsRUFBaEMsRUFBb0M7QUFDbkM7QUFDQSxRQUFJQyxpQkFBaUJMLEtBQUtHLFVBQUwsQ0FBZ0JDLE9BQWhCLENBQXdCRSxPQUF4QixDQUFnQyxHQUFoQyxFQUFvQyxFQUFwQyxDQUFyQjs7QUFFQUosNEpBRWlDRyxjQUZqQztBQUlBOztBQUVELE9BQU1FLFVBQVUsZ0JBQWdCQyxFQUFFQyxLQUFGLENBQVE7QUFDdkNDLFNBQUtWLEtBQUtHLFVBQUwsQ0FBZ0JRLEVBRGtCO0FBRXZDQyxZQUFRLE1BRitCO0FBR3ZDQyxjQUFVTCxFQUFFTSxPQUFGLENBQVVDLE9BQU9DLFFBQVAsQ0FBZ0JDLE1BQWhCLENBQXVCQyxLQUF2QixDQUE2QixDQUE3QixDQUFWO0FBSDZCLElBQVIsQ0FBaEM7O0FBTUFoQiw4RUFDMkNLLE9BRDNDLDhCQUVPUCxLQUFLRyxVQUFMLENBQWdCUSxFQUZ2Qjs7QUFNQSxVQUFPVCxXQUFQO0FBQ0E7QUEvQmdDLEVBQW5DOztBQWtDQWQsU0FBUStCLFFBQVIsR0FBbUIvQixRQUFRK0IsUUFBUixJQUFvQjtBQUNyQzdCLFFBQU0sVUFEK0I7QUFFckNDLFlBQVUsT0FGMkI7QUFHckNDLGVBQWEsR0FId0I7QUFJckNNLFFBSnFDLGtCQUk5QlIsSUFKOEIsRUFJeEJTLElBSndCLEVBSWxCQyxJQUprQixFQUlaQyxJQUpZLEVBSU47QUFDOUIsT0FBSUMsY0FBY0YsS0FBS0csVUFBTCxDQUFnQmlCLFVBQWhCLHNGQUVXcEIsS0FBS0csVUFBTCxDQUFnQmlCLFVBRjNCLHNCQUVzRDlCLElBRnRELDBEQUc2QkEsSUFIN0IsWUFBbEI7O0FBS0EsT0FBSVUsS0FBS0csVUFBTCxDQUFnQmtCLGFBQWhCLENBQThCQyxNQUE5QixHQUF1QyxDQUEzQyxFQUE4QztBQUM3Q3BCO0FBR0E7O0FBRUQsVUFBT0EsV0FBUDtBQUNBO0FBakJvQyxFQUF2Qzs7QUFvQkFkLFNBQVFtQyxLQUFSLEdBQWdCbkMsUUFBUW1DLEtBQVIsSUFBaUI7QUFDL0JqQyxRQUFNLE9BRHlCO0FBRS9CQyxZQUFVLE1BRnFCO0FBRy9CQyxlQUFhO0FBSGtCLEVBQWpDOztBQU1BSixTQUFRb0MsR0FBUixHQUFjcEMsUUFBUW9DLEdBQVIsSUFBZTtBQUMzQmxDLFFBQU0sS0FEcUI7QUFFM0JDLFlBQVUsTUFGaUI7QUFHM0JDLGVBQWEsQ0FIYztBQUkzQkssYUFBVyxTQUpnQjtBQUszQkMsUUFMMkIsa0JBS3BCUixJQUxvQixFQUtkUyxJQUxjLEVBS1JDLElBTFEsRUFLRkMsSUFMRSxFQUtJO0FBQzlCLHFEQUFnRFgsSUFBaEQ7QUFDQTtBQVAwQixFQUE3Qjs7QUFVQUYsU0FBUXFDLGFBQVIsR0FBd0JyQyxRQUFRcUMsYUFBUixJQUF5QjtBQUMvQ25DLFFBQU0sZUFEeUM7QUFFL0NDLFlBQVUsT0FGcUM7QUFHL0NDLGVBQWEsQ0FIa0M7QUFJL0NNLFFBSitDLGtCQUl4Q1IsSUFKd0MsRUFJbENTLElBSmtDLEVBSTVCQyxJQUo0QixFQUl0QkMsSUFKc0IsRUFJaEI7QUFDOUIsNEJBQXVCRCxLQUFLRyxVQUFMLENBQWdCc0IsYUFBdkMsVUFBeURuQyxJQUF6RDtBQUNBO0FBTjhDLEVBQWpEOztBQVNBRixTQUFRc0MsY0FBUixHQUF5QnRDLFFBQVFzQyxjQUFSLElBQTBCO0FBQ2pEcEMsUUFBTSxnQkFEMkM7QUFFakRDLFlBQVUsT0FGdUM7QUFHakRDLGVBQWEsQ0FIb0M7QUFJakRLLGFBQVcsaUJBSnNDO0FBS2pEQyxRQUxpRCxrQkFLMUNSLElBTDBDLEVBS3BDUyxJQUxvQyxFQUs5QkMsSUFMOEIsRUFLeEJDLElBTHdCLEVBS2xCO0FBQzlCLFVBQU8sa0JBQWdCRCxLQUFLRyxVQUFMLENBQWdCdUIsY0FBaEMsVUFBbURwQyxJQUFuRCxnQkFDSFUsS0FBS0csVUFBTCxDQUFnQndCLGFBQWhCLENBQThCTCxNQUE5QixHQUNBLDJFQURBLEdBQzhFLEVBRjNFLENBQVA7QUFHQSxHQVRnRDtBQVVqRE0sYUFWaUQsdUJBVXJDQyxFQVZxQyxFQVVqQ0MsUUFWaUMsRUFVdkJDLE9BVnVCLEVBVWRDLEdBVmMsRUFVVEMsR0FWUyxFQVVKO0FBQzNDRixXQUFRNUIsVUFBUixDQUFtQndCLGFBQW5CLENBQWlDTCxNQUFsQyxHQUNBZCxFQUFFcUIsRUFBRixFQUFNSyxRQUFOLENBQWUsUUFBZixFQUNFNUMsSUFERixDQUNPLFNBRFAsRUFDa0J5QyxRQUFRNUIsVUFBUixDQUFtQlEsRUFEckMsRUFFRXdCLElBRkYsQ0FFTyxhQUZQLEVBRXNCLE9BRnRCLEVBR0VBLElBSEYsQ0FHTyxhQUhQLEVBR3NCLDRCQUh0QixDQURBLEdBS0EzQixFQUFFcUIsRUFBRixFQUNFdkMsSUFERixDQUNPLFNBRFAsRUFDa0J5QyxRQUFRNUIsVUFBUixDQUFtQlEsRUFEckMsRUFFRXdCLElBRkYsQ0FFTyxhQUZQLEVBRXNCLE9BRnRCLEVBR0VBLElBSEYsQ0FHTyxhQUhQLEVBR3NCLDRCQUh0QixDQUxBO0FBU0E7QUFwQmdELEVBQW5EOztBQXVCQS9DLFNBQVFnRCxjQUFSLEdBQXlCaEQsUUFBUWdELGNBQVIsSUFBMEI7QUFDakQ5QyxRQUFNLGdCQUQyQztBQUVqREMsWUFBVSxNQUZ1QztBQUdqREMsZUFBYSxHQUhvQztBQUlqRE0sUUFKaUQsa0JBSTFDUixJQUowQyxFQUlwQ1MsSUFKb0MsRUFJOUJDLElBSjhCLEVBSXhCQyxJQUp3QixFQUlsQjtBQUM5QixPQUFJb0MsT0FBTyxFQUFYOztBQUVBLE9BQUkvQyxJQUFKLEVBQVU7QUFDVCtDLDBCQUNjcEQsSUFBSXFELElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsQ0FEZCw0QkFDa0VsRCxLQUFLbUQsV0FBTCxFQURsRTtBQUVBOztBQUVELE9BQU1DLFFBQVF6RCxJQUFJcUQsSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsK0JBQXhCLEVBQXlELGVBQXpELElBQ1gsSUFEVyxHQUNKNUMsS0FBS0csVUFBTCxDQUFnQjBDLE9BRDFCOztBQUdBUiw2QkFBd0JLLEtBQXhCLFVBQWtDcEQsSUFBbEM7O0FBRUEsVUFBTytDLElBQVA7QUFDQTtBQWxCZ0QsRUFBbkQ7O0FBcUJBakQsU0FBUTBELElBQVIsR0FBZTFELFFBQVEwRCxJQUFSLElBQWdCO0FBQzdCeEQsUUFBTSxNQUR1QjtBQUU3QkMsWUFBVSxPQUZtQjtBQUc3QkMsZUFBYSxHQUhnQjtBQUk3Qk0sUUFKNkIsa0JBSXRCUixJQUpzQixFQUloQlMsSUFKZ0IsRUFJVkMsSUFKVSxFQUlKQyxJQUpJLEVBSUU7QUFDOUIsVUFBTzhDLE9BQU96RCxJQUFQLEVBQWEwRCxNQUFiLENBQW9CLGtCQUFwQixDQUFQO0FBQ0E7QUFONEIsRUFBL0I7O0FBU0E1RCxTQUFRNkQsTUFBUixHQUFpQjdELFFBQVE2RCxNQUFSLElBQWtCO0FBQ2pDM0QsUUFBTSxRQUQyQjtBQUVqQ0MsWUFBVSxPQUZ1QjtBQUdqQ0MsZUFBYSxDQUhvQjtBQUlqQ00sUUFKaUMsa0JBSTFCUixJQUowQixFQUlwQlMsSUFKb0IsRUFJZEMsSUFKYyxFQUlSQyxJQUpRLEVBSUY7QUFDOUIsNkpBRWtFRCxLQUFLRyxVQUFMLENBQWdCK0MsUUFGbEYsd0JBR0k1RCxJQUhKO0FBTUE7QUFYZ0MsRUFBbkM7O0FBY0FGLFNBQVErRCxXQUFSLEdBQXNCL0QsUUFBUStELFdBQVIsSUFBdUI7QUFDM0M3RCxRQUFNLGFBRHFDO0FBRTNDQyxZQUFVLE1BRmlDO0FBRzNDQyxlQUFhLEdBSDhCO0FBSTNDSyxhQUFXO0FBSmdDLEVBQTdDOztBQU9BVCxTQUFRZ0UsYUFBUixHQUF3QmhFLFFBQVFnRSxhQUFSLElBQXlCO0FBQy9DOUQsUUFBTSxlQUR5QztBQUUvQ0MsWUFBVSxNQUZxQztBQUcvQ0MsZUFBYSxDQUhrQztBQUkvQ00sUUFKK0Msa0JBSXhDUixJQUp3QyxFQUlsQ1MsSUFKa0MsRUFJNUJDLElBSjRCLEVBSXRCQyxJQUpzQixFQUloQjtBQUM5QjtBQUNBLE9BQU1vRCxRQUFRQyxTQUFTQyxhQUFULENBQXVCLE1BQXZCLENBQWQ7O0FBRUE7QUFDQSxPQUFNQyxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQUNKLGFBQUQsRUFBZ0JLLEtBQWhCLEVBQXVCQyxLQUF2QixFQUFpQztBQUN4RDtBQUNBLFFBQU1DLGtCQUFtQkYsVUFBV0MsTUFBTXBDLE1BQU4sR0FBZSxDQUFuRDs7QUFFQTtBQUNBLFFBQU1zQyxhQUFhO0FBQ2xCQyxhQUFRLFlBRFU7QUFFbEJqRCxhQUFRLFNBRlU7QUFHbEJiLFdBQU0sU0FIWTtBQUlsQitELGVBQVU5RCxLQUFLRyxVQUFMLENBQWdCUSxFQUpSO0FBS2xCb0QscUJBQWdCWDtBQUxFLEtBQW5COztBQVFBO0FBQ0EsUUFBTVksTUFBUy9FLElBQUlxRCxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQVQsZ0NBQWlFaEMsRUFBRUMsS0FBRixDQUFRbUQsVUFBUixDQUF2RTs7QUFFQTtBQUNBLFFBQU1LLFFBQVFYLFNBQVNDLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBZDs7QUFFQTtBQUNBVSxVQUFNQyxZQUFOLENBQW1CLE1BQW5CLEVBQTJCRixHQUEzQjtBQUNBQyxVQUFNQyxZQUFOLENBQW1CLFFBQW5CLEVBQTZCLFFBQTdCOztBQUVBO0FBQ0FELFVBQU1FLFdBQU4sR0FBb0JmLGlCQUFpQk8sa0JBQWtCLEVBQWxCLEdBQXVCLElBQXhDLENBQXBCOztBQUVBO0FBQ0FOLFVBQU1lLFdBQU4sQ0FBa0JILEtBQWxCO0FBQ0EsSUE1QkQ7O0FBOEJBO0FBQ0FaLFNBQU1nQixTQUFOLENBQWdCQyxHQUFoQixDQUFvQixrQkFBcEIsRUFBd0MsaUJBQXhDOztBQUVBO0FBQ0F0RSxRQUFLRyxVQUFMLENBQWdCb0UsY0FBaEIsQ0FBK0JDLE9BQS9CLENBQXVDaEIsZUFBdkM7O0FBRUEsVUFBT0gsTUFBTW9CLFNBQWI7QUFDQTtBQTlDOEMsRUFBakQ7O0FBaURBckYsU0FBUXNGLE9BQVIsR0FBa0J0RixRQUFRc0YsT0FBUixJQUFtQjtBQUNuQ3BGLFFBQU0sSUFENkI7QUFFbkNDLFlBQVUsT0FGeUI7QUFHbkNDLGVBQWEsR0FIc0I7QUFJbkNLLGFBQVcsU0FKd0I7QUFLbkNKLGFBQVcsS0FMd0I7QUFNbkNDLGNBQVksS0FOdUI7QUFPbkNJLFFBUG1DLGtCQU81QlIsSUFQNEIsRUFPdEJTLElBUHNCLEVBT2hCQyxJQVBnQixFQU9WQyxJQVBVLEVBT0o7QUFDOUIsT0FBSTBFLG9CQUFvQixFQUF4Qjs7QUFFQTNFLFFBQUtHLFVBQUwsQ0FBZ0J5RSxhQUFoQixDQUE4QkosT0FBOUIsQ0FBc0MsVUFBQ0ssWUFBRCxFQUFrQjtBQUN2REYsd0VBQytCRSxZQUQvQiwrQ0FFWTVGLElBQUlxRCxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qiw2QkFBeEIsRUFBdUQsUUFBdkQsQ0FGWixTQUVnRmlDLFlBRmhGLG9NQUt5QkEsWUFMekI7QUFRQSxJQVREOztBQVdBLE9BQU10RSxVQUFVLGdCQUFnQkMsRUFBRUMsS0FBRixDQUFRO0FBQ3ZDQyxTQUFLVixLQUFLRyxVQUFMLENBQWdCUSxFQURrQjtBQUV2Q0MsWUFBUSxNQUYrQjtBQUd2Q0MsY0FBVUwsRUFBRU0sT0FBRixDQUFVQyxPQUFPQyxRQUFQLENBQWdCQyxNQUFoQixDQUF1QkMsS0FBdkIsQ0FBNkIsQ0FBN0IsQ0FBVjtBQUg2QixJQUFSLENBQWhDOztBQU1BLE9BQUk0RCxpQkFBaUIsQ0FBQzlFLEtBQUtHLFVBQUwsQ0FBZ0I0RSxVQUFqQiw0SEFFVjlGLElBQUlxRCxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qiw0QkFBeEIsRUFBc0QsUUFBdEQsQ0FGVSxjQUVnRSxFQUZyRjs7QUFJQSxnRUFFSStCLGlCQUZKLHNCQUdJRyxjQUhKLDhIQU9hdkUsT0FQYjtBQTJCQTtBQTFEa0MsRUFBckM7QUE0REEsQ0FuUkQsRUFtUkd0QixJQUFJQyxJQUFKLENBQVNDLHVCQW5SWiIsImZpbGUiOiJvcmRlcnNfb3ZlcnZpZXdfY29sdW1ucy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBvcmRlcnNfb3ZlcnZpZXdfY29sdW1ucy5qcyAyMDE4LTAxLTExXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuanNlLmxpYnMub3JkZXJzX292ZXJ2aWV3X2NvbHVtbnMgPSBqc2UubGlicy5vcmRlcnNfb3ZlcnZpZXdfY29sdW1ucyB8fCB7fTtcclxuXHJcbi8qKlxyXG4gKiAjIyBPcmRlcnMgVGFibGUgQ29sdW1uIERlZmluaXRpb25zXHJcbiAqXHJcbiAqIFRoaXMgbW9kdWxlIGRlZmluZXMgdGhlIGNvbHVtbiBkZWZpbml0aW9uIG9mIHRoZSBvcmRlciBvdmVydmlldyB0YWJsZS4gVGhleSBjYW4gYmUgb3ZlcnJpZGRlbiBieSBvdGhlclxyXG4gKiBzY3JpcHRzIGJ5IG1vZGlmeWluZyB0aGUgYXJyYXkgd2l0aCBuZXcgY29sdW1ucywgb3IgYnkgcmVwbGFjaW5nIHRoZSBwcm9wZXJ0eSB2YWx1ZXMgb2YgdGhlIGNvbnRhaW5lZFxyXG4gKiBmaWVsZHMuXHJcbiAqXHJcbiAqIEBtb2R1bGUgQWRtaW4vTGlicy9vcmRlcnNfb3ZlcnZpZXdfY29sdW1uc1xyXG4gKiBAZXhwb3J0cyBqc2UubGlicy5vcmRlcnNfb3ZlcnZpZXdfY29sdW1uc1xyXG4gKiBAcmVxdWlyZXMgbW9tZW50anNcclxuICovXHJcbihmdW5jdGlvbihleHBvcnRzKSB7XHJcblx0XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdGV4cG9ydHMuY2hlY2tib3ggPSBleHBvcnRzLmNoZWNrYm94IHx8IHtcclxuXHRcdFx0ZGF0YTogbnVsbCxcclxuXHRcdFx0bWluV2lkdGg6ICc1MHB4JyxcclxuXHRcdFx0d2lkdGhGYWN0b3I6IDAuMDEsXHJcblx0XHRcdG9yZGVyYWJsZTogZmFsc2UsXHJcblx0XHRcdHNlYXJjaGFibGU6IGZhbHNlLFxyXG5cdFx0XHRkZWZhdWx0Q29udGVudDogJzxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiAvPidcclxuXHRcdH07XHJcblx0XHJcblx0ZXhwb3J0cy5udW1iZXIgPSBleHBvcnRzLm51bWJlciB8fCB7XHJcblx0XHRcdGRhdGE6ICdudW1iZXInLFxyXG5cdFx0XHRtaW5XaWR0aDogJzc1cHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMSxcclxuXHRcdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYycsXHJcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XHJcblx0XHRcdFx0bGV0IGxpbmtFbGVtZW50ID0gJyc7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKGZ1bGwuRFRfUm93RGF0YS5jb21tZW50ICE9PSAnJykge1xyXG5cdFx0XHRcdFx0Ly8gUmVtb3ZlIGRvdWJsZSBxdW90ZXMgdG8gYXZvaWQgYnJva2VuIHRvb2x0aXBzXHJcblx0XHRcdFx0XHRsZXQgZXNjYXBlZENvbW1lbnQgPSBmdWxsLkRUX1Jvd0RhdGEuY29tbWVudC5yZXBsYWNlKC9cIi8sJycpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRsaW5rRWxlbWVudCArPSBgXHJcblx0XHRcdFx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtY29tbWVudC1vIHRvb2x0aXAtb3JkZXItY29tbWVudCB0b29sdGlwLXRyaWdnZXJcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGFyaWEtaGlkZGVuPVwidHJ1ZVwiIHRpdGxlPVwiJHtlc2NhcGVkQ29tbWVudH1cIj48L2k+Jm5ic3A7XHJcblx0XHRcdFx0XHRcdFx0XHRgO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRjb25zdCBlZGl0VXJsID0gJ29yZGVycy5waHA/JyArICQucGFyYW0oe1xyXG5cdFx0XHRcdFx0b0lEOiBmdWxsLkRUX1Jvd0RhdGEuaWQsXHJcblx0XHRcdFx0XHRhY3Rpb246ICdlZGl0JyxcclxuXHRcdFx0XHRcdG92ZXJ2aWV3OiAkLmRlcGFyYW0od2luZG93LmxvY2F0aW9uLnNlYXJjaC5zbGljZSgxKSlcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRsaW5rRWxlbWVudCArPSBgXHJcblx0XHRcdFx0XHRcdFx0XHQ8YSBjbGFzcz1cInRvb2x0aXAtb3JkZXItaXRlbXNcIiBocmVmPVwiJHtlZGl0VXJsfVwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQke2Z1bGwuRFRfUm93RGF0YS5pZH1cclxuXHRcdFx0XHRcdFx0XHRcdDwvYT5cclxuXHRcdFx0XHRcdFx0XHRgO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdHJldHVybiBsaW5rRWxlbWVudDtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLmN1c3RvbWVyID0gZXhwb3J0cy5jdXN0b21lciB8fCB7XHJcblx0XHRcdGRhdGE6ICdjdXN0b21lcicsXHJcblx0XHRcdG1pbldpZHRoOiAnMTkwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMS41LFxyXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xyXG5cdFx0XHRcdGxldCBsaW5rRWxlbWVudCA9IGZ1bGwuRFRfUm93RGF0YS5jdXN0b21lcklkXHJcblx0XHRcdFx0XHQ/IGA8YSBjbGFzcz1cInRvb2x0aXAtY3VzdG9tZXItYWRkcmVzc2VzXCIgXHJcblx0XHRcdFx0XHRcdFx0aHJlZj1cImN1c3RvbWVycy5waHA/Y0lEPSR7ZnVsbC5EVF9Sb3dEYXRhLmN1c3RvbWVySWR9JmFjdGlvbj1lZGl0XCI+JHtkYXRhfTwvYT5gXHJcblx0XHRcdFx0XHQ6IGA8c3BhbiBjbGFzcz1cInRvb2x0aXAtY3VzdG9tZXItYWRkcmVzc2VzXCI+JHtkYXRhfTwvc3Bhbj5gO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChmdWxsLkRUX1Jvd0RhdGEuY3VzdG9tZXJNZW1vcy5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0XHRsaW5rRWxlbWVudCArPVxyXG5cdFx0XHRcdFx0XHRgIDxpIGNsYXNzPVwiZmEgZmEtc3RpY2t5LW5vdGUtbyB0b29sdGlwLWN1c3RvbWVyLW1lbW9zIHRvb2x0aXAtdHJpZ2dlclwiIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5gO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRyZXR1cm4gbGlua0VsZW1lbnQ7XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblx0XHJcblx0ZXhwb3J0cy5ncm91cCA9IGV4cG9ydHMuZ3JvdXAgfHwge1xyXG5cdFx0XHRkYXRhOiAnZ3JvdXAnLFxyXG5cdFx0XHRtaW5XaWR0aDogJzg1cHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMS4yXHJcblx0XHR9O1xyXG5cdFxyXG5cdGV4cG9ydHMuc3VtID0gZXhwb3J0cy5zdW0gfHwge1xyXG5cdFx0XHRkYXRhOiAnc3VtJyxcclxuXHRcdFx0bWluV2lkdGg6ICc5MHB4JyxcclxuXHRcdFx0d2lkdGhGYWN0b3I6IDEsXHJcblx0XHRcdGNsYXNzTmFtZTogJ251bWVyaWMnLFxyXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xyXG5cdFx0XHRcdHJldHVybiBgPHNwYW4gY2xhc3M9XCJ0b29sdGlwLW9yZGVyLXN1bS1ibG9ja1wiPiR7ZGF0YX08L3NwYW4+YDtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLnBheW1lbnRNZXRob2QgPSBleHBvcnRzLnBheW1lbnRNZXRob2QgfHwge1xyXG5cdFx0XHRkYXRhOiAncGF5bWVudE1ldGhvZCcsXHJcblx0XHRcdG1pbldpZHRoOiAnMTEwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMixcclxuXHRcdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuXHRcdFx0XHRyZXR1cm4gYDxzcGFuIHRpdGxlPVwiJHtmdWxsLkRUX1Jvd0RhdGEucGF5bWVudE1ldGhvZH1cIj4ke2RhdGF9PC9zcGFuPmBcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLnNoaXBwaW5nTWV0aG9kID0gZXhwb3J0cy5zaGlwcGluZ01ldGhvZCB8fCB7XHJcblx0XHRcdGRhdGE6ICdzaGlwcGluZ01ldGhvZCcsXHJcblx0XHRcdG1pbldpZHRoOiAnMTEwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogMixcclxuXHRcdFx0Y2xhc3NOYW1lOiAnc2hpcHBpbmctbWV0aG9kJyxcclxuXHRcdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuXHRcdFx0XHRyZXR1cm4gYDxzcGFuIHRpdGxlPVwiJHtmdWxsLkRUX1Jvd0RhdGEuc2hpcHBpbmdNZXRob2R9XCI+JHtkYXRhfTwvc3Bhbj5gXHJcblx0XHRcdFx0XHQrIChmdWxsLkRUX1Jvd0RhdGEudHJhY2tpbmdMaW5rcy5sZW5ndGhcclxuXHRcdFx0XHRcdFx0PyAnIDxpIGNsYXNzPVwiZmEgZmEtdHJ1Y2sgZmEtbGcgdG9vbHRpcC10cmFja2luZy1saW5rcyB0b29sdGlwLXRyaWdnZXJcIj48L2k+JyA6ICcnKTtcclxuXHRcdFx0fSxcclxuXHRcdFx0Y3JlYXRlZENlbGwodGQsIGNlbGxEYXRhLCByb3dEYXRhLCByb3csIGNvbCkge1xyXG5cdFx0XHRcdChyb3dEYXRhLkRUX1Jvd0RhdGEudHJhY2tpbmdMaW5rcy5sZW5ndGgpID9cclxuXHRcdFx0XHQkKHRkKS5jaGlsZHJlbignOmZpcnN0JylcclxuXHRcdFx0XHRcdC5kYXRhKCdvcmRlcklkJywgcm93RGF0YS5EVF9Sb3dEYXRhLmlkKVxyXG5cdFx0XHRcdFx0LmF0dHIoJ2RhdGEtdG9nZ2xlJywgJ21vZGFsJylcclxuXHRcdFx0XHRcdC5hdHRyKCdkYXRhLXRhcmdldCcsICcuYWRkLXRyYWNraW5nLW51bWJlci5tb2RhbCcpIDpcclxuXHRcdFx0XHQkKHRkKVxyXG5cdFx0XHRcdFx0LmRhdGEoJ29yZGVySWQnLCByb3dEYXRhLkRUX1Jvd0RhdGEuaWQpXHJcblx0XHRcdFx0XHQuYXR0cignZGF0YS10b2dnbGUnLCAnbW9kYWwnKVxyXG5cdFx0XHRcdFx0LmF0dHIoJ2RhdGEtdGFyZ2V0JywgJy5hZGQtdHJhY2tpbmctbnVtYmVyLm1vZGFsJylcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLmNvdW50cnlJc29Db2RlID0gZXhwb3J0cy5jb3VudHJ5SXNvQ29kZSB8fCB7XHJcblx0XHRcdGRhdGE6ICdjb3VudHJ5SXNvQ29kZScsXHJcblx0XHRcdG1pbldpZHRoOiAnNzVweCcsXHJcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjQsXHJcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XHJcblx0XHRcdFx0bGV0IGh0bWwgPSAnJztcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoZGF0YSkge1xyXG5cdFx0XHRcdFx0aHRtbCA9XHJcblx0XHRcdFx0XHRcdGA8aW1nIHNyYz1cIiR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2ltYWdlcy9pY29ucy9mbGFncy8ke2RhdGEudG9Mb3dlckNhc2UoKX0ucG5nXCIgLz4mbmJzcDtgO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRjb25zdCB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTSElQUElOR19PUklHSU5fQ09VTlRSWV9USVRMRScsICdjb25maWd1cmF0aW9uJylcclxuXHRcdFx0XHRcdCsgJzogJyArIGZ1bGwuRFRfUm93RGF0YS5jb3VudHJ5O1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGh0bWwgKz0gYDxzcGFuIHRpdGxlPVwiJHt0aXRsZX1cIj4ke2RhdGF9PC9zcGFuPmA7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0cmV0dXJuIGh0bWw7XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblx0XHJcblx0ZXhwb3J0cy5kYXRlID0gZXhwb3J0cy5kYXRlIHx8IHtcclxuXHRcdFx0ZGF0YTogJ2RhdGUnLFxyXG5cdFx0XHRtaW5XaWR0aDogJzEwMHB4JyxcclxuXHRcdFx0d2lkdGhGYWN0b3I6IDEuNixcclxuXHRcdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuXHRcdFx0XHRyZXR1cm4gbW9tZW50KGRhdGEpLmZvcm1hdCgnREQuTU0uWVkgLSBISDptbScpO1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFxyXG5cdGV4cG9ydHMuc3RhdHVzID0gZXhwb3J0cy5zdGF0dXMgfHwge1xyXG5cdFx0XHRkYXRhOiAnc3RhdHVzJyxcclxuXHRcdFx0bWluV2lkdGg6ICcxMjBweCcsXHJcblx0XHRcdHdpZHRoRmFjdG9yOiAyLFxyXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xyXG5cdFx0XHRcdHJldHVybiBgXHJcblx0XHRcdFx0XHQ8c3BhbiBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgZGF0YS10YXJnZXQ9XCIuc3RhdHVzLm1vZGFsXCJcclxuXHRcdFx0XHRcdFx0XHRjbGFzcz1cIm9yZGVyLXN0YXR1cyB0b29sdGlwLW9yZGVyLXN0YXR1cy1oaXN0b3J5IGxhYmVsIGxhYmVsLSR7ZnVsbC5EVF9Sb3dEYXRhLnN0YXR1c0lkfVwiPlxyXG5cdFx0XHRcdFx0XHQke2RhdGF9XHJcblx0XHRcdFx0XHQ8L3NwYW4+XHJcblx0XHRcdFx0YDtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRleHBvcnRzLnRvdGFsV2VpZ2h0ID0gZXhwb3J0cy50b3RhbFdlaWdodCB8fCB7XHJcblx0XHRcdGRhdGE6ICd0b3RhbFdlaWdodCcsXHJcblx0XHRcdG1pbldpZHRoOiAnNTBweCcsXHJcblx0XHRcdHdpZHRoRmFjdG9yOiAwLjYsXHJcblx0XHRcdGNsYXNzTmFtZTogJ251bWVyaWMnXHJcblx0XHR9O1xyXG5cdFxyXG5cdGV4cG9ydHMuaW52b2ljZU51bWJlciA9IGV4cG9ydHMuaW52b2ljZU51bWJlciB8fCB7XHJcblx0XHRcdGRhdGE6ICdpbnZvaWNlTnVtYmVyJyxcclxuXHRcdFx0bWluV2lkdGg6ICc3NXB4JyxcclxuXHRcdFx0d2lkdGhGYWN0b3I6IDEsXHJcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XHJcblx0XHRcdFx0Ly8gQ3JlYXRlIGEgJ3NwYW4nIGNvbnRhaW5lciBlbGVtZW50LlxyXG5cdFx0XHRcdGNvbnN0ICRodG1sID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIEl0ZXJhdG9yIGZ1bmN0aW9uIHRvIGFkZCBhIGxpbmsgZWxlbWVudCBpbnRvIGNvbnRhaW5lci5cclxuXHRcdFx0XHRjb25zdCBfYWRkTGlua0VsZW1lbnQgPSAoaW52b2ljZU51bWJlciwgaW5kZXgsIGFycmF5KSA9PiB7XHJcblx0XHRcdFx0XHQvLyBJcyB0aGUgY3VycmVudCBpdGVyYXRpb24gdGhlIGxhc3Qgb25lP1xyXG5cdFx0XHRcdFx0Y29uc3QgaXNMYXN0SXRlcmF0aW9uID0gKGluZGV4ID09PSAoYXJyYXkubGVuZ3RoIC0gMSkpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBJbnZvaWNlIGxpbmsgcGFyYW1ldGVycy5cclxuXHRcdFx0XHRcdGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcblx0XHRcdFx0XHRcdG1vZHVsZTogJ09yZGVyQWRtaW4nLFxyXG5cdFx0XHRcdFx0XHRhY3Rpb246ICdzaG93UGRmJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ2ludm9pY2UnLFxyXG5cdFx0XHRcdFx0XHRvcmRlcl9pZDogZnVsbC5EVF9Sb3dEYXRhLmlkLFxyXG5cdFx0XHRcdFx0XHRpbnZvaWNlX251bWJlcjogaW52b2ljZU51bWJlclxyXG5cdFx0XHRcdFx0fTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0Ly8gQ29tcG91bmQgaW52b2ljZSBsaW5rLlxyXG5cdFx0XHRcdFx0Y29uc3QgdXJsID0gYCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L2FkbWluL3JlcXVlc3RfcG9ydC5waHA/JHskLnBhcmFtKHBhcmFtZXRlcnMpfWA7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdC8vIENyZWF0ZSBsaW5rIGVsZW1lbnQuXHJcblx0XHRcdFx0XHRjb25zdCAkbGluayA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0Ly8gU2V0IGxpbmsgb24gZWxlbWVudC5cclxuXHRcdFx0XHRcdCRsaW5rLnNldEF0dHJpYnV0ZSgnaHJlZicsIHVybCk7XHJcblx0XHRcdFx0XHQkbGluay5zZXRBdHRyaWJ1dGUoJ3RhcmdldCcsICdfYmxhbmsnKTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0Ly8gU2V0IGludm9pY2UgbnVtYmVyIGFzIHRleHQgb24gZWxlbWVudC5cclxuXHRcdFx0XHRcdCRsaW5rLnRleHRDb250ZW50ID0gaW52b2ljZU51bWJlciArIChpc0xhc3RJdGVyYXRpb24gPyAnJyA6ICcsICcpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBBcHBlbmQgZWxlbWVudCB0byBjb250YWluZXIuXHJcblx0XHRcdFx0XHQkaHRtbC5hcHBlbmRDaGlsZCgkbGluayk7XHJcblx0XHRcdFx0fTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBBZGQgdG9vbHRpcCBjbGFzc2VzIHRvIGVsZW1lbnQuXHJcblx0XHRcdFx0JGh0bWwuY2xhc3NMaXN0LmFkZCgndG9vbHRpcC1pbnZvaWNlcycsICd0b29sdGlwLXRyaWdnZXInKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBJdGVyYXRlIG92ZXIgZWFjaCBpbnZvaWNlIG51bWJlciBhbmQgY3JlYXRlIGxpbmsuXHJcblx0XHRcdFx0ZnVsbC5EVF9Sb3dEYXRhLmludm9pY2VOdW1iZXJzLmZvckVhY2goX2FkZExpbmtFbGVtZW50KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRyZXR1cm4gJGh0bWwub3V0ZXJIVE1MO1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFxyXG5cdGV4cG9ydHMuYWN0aW9ucyA9IGV4cG9ydHMuYWN0aW9ucyB8fCB7XHJcblx0XHRcdGRhdGE6IG51bGwsXHJcblx0XHRcdG1pbldpZHRoOiAnMzUwcHgnLFxyXG5cdFx0XHR3aWR0aEZhY3RvcjogNC42LFxyXG5cdFx0XHRjbGFzc05hbWU6ICdhY3Rpb25zJyxcclxuXHRcdFx0b3JkZXJhYmxlOiBmYWxzZSxcclxuXHRcdFx0c2VhcmNoYWJsZTogZmFsc2UsXHJcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XHJcblx0XHRcdFx0bGV0IHdpdGhkcmF3YWxJZHNIdG1sID0gJyc7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0ZnVsbC5EVF9Sb3dEYXRhLndpdGhkcmF3YWxJZHMuZm9yRWFjaCgod2l0aGRyYXdhbElkKSA9PiB7XHJcblx0XHRcdFx0XHR3aXRoZHJhd2FsSWRzSHRtbCArPSBgXHJcblx0XHRcdFx0XHRcdDxhIGhyZWY9XCJ3aXRoZHJhd2Fscy5waHA/aWQ9JHt3aXRoZHJhd2FsSWR9JmFjdGlvbj1lZGl0XCIgXHJcblx0XHRcdFx0XHRcdFx0XHR0aXRsZT1cIiR7anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RBQkxFX0hFQURJTkdfV0lUSERSQVdBTF9JRCcsICdvcmRlcnMnKX0gJHt3aXRoZHJhd2FsSWR9XCI+XHJcblx0XHRcdFx0XHRcdFx0PGltZyBzcmM9XCJodG1sL2Fzc2V0cy9pbWFnZXMvbGVnYWN5L2ljb25zL3dpdGhkcmF3YWwtb24ucG5nXCIgXHJcblx0XHRcdFx0XHRcdFx0XHRjbGFzcz1cInRvb2x0aXAtd2l0aGRyYXdhbCB0b29sdGlwLXRyaWdnZXIgbWV0YS1pY29uXCIgXHJcblx0XHRcdFx0XHRcdFx0XHRkYXRhLXdpdGhkcmF3YWwtaWQ9XCIke3dpdGhkcmF3YWxJZH1cIiAvPlxyXG5cdFx0XHRcdFx0XHQ8L2E+XHJcblx0XHRcdFx0XHRgO1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGNvbnN0IGVkaXRVcmwgPSAnb3JkZXJzLnBocD8nICsgJC5wYXJhbSh7XHJcblx0XHRcdFx0XHRvSUQ6IGZ1bGwuRFRfUm93RGF0YS5pZCxcclxuXHRcdFx0XHRcdGFjdGlvbjogJ2VkaXQnLFxyXG5cdFx0XHRcdFx0b3ZlcnZpZXc6ICQuZGVwYXJhbSh3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNsaWNlKDEpKVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGxldCBtYWlsU3RhdHVzSHRtbCA9ICFmdWxsLkRUX1Jvd0RhdGEubWFpbFN0YXR1c1xyXG5cdFx0XHRcdFx0PyBgPGkgY2xhc3M9XCJmYSBmYS1lbnZlbG9wZS1vIG1ldGEtaWNvbiB0b29sdGlwLWNvbmZpcm1hdGlvbi1ub3Qtc2VudCBlbWFpbC1vcmRlciB0b29sdGlwLXRyaWdnZXJcIlxyXG5cdFx0XHRcdFx0XHR0aXRsZT1cIiR7anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfQ09ORklSTUFUSU9OX05PVF9TRU5UJywgJ29yZGVycycpfVwiPjwvaT5gIDogJyc7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0cmV0dXJuIGBcclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJwdWxsLWxlZnRcIj5cclxuXHRcdFx0XHRcdFx0JHt3aXRoZHJhd2FsSWRzSHRtbH1cclxuXHRcdFx0XHRcdFx0JHttYWlsU3RhdHVzSHRtbH1cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicHVsbC1yaWdodCBhY3Rpb24tbGlzdCB2aXNpYmxlLW9uLWhvdmVyXCI+XHJcblx0XHRcdFx0XHRcdDxhIGhyZWY9XCIke2VkaXRVcmx9XCI+XHJcblx0XHRcdFx0XHRcdFx0PGkgY2xhc3M9XCJmYSBmYS1leWUgZWRpdFwiPjwvaT5cclxuXHRcdFx0XHRcdFx0PC9hPlxyXG5cdFx0XHRcdFx0XHQ8aSBjbGFzcz1cImZhIGZhLXRyYXNoLW8gZGVsZXRlXCI+PC9pPlxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImJ0bi1ncm91cCBkcm9wZG93blwiPlxyXG5cdFx0XHRcdFx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiXHJcblx0XHRcdFx0XHRcdFx0XHRcdGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0XCI+PC9idXR0b24+XHJcblx0XHRcdFx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0Y2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgZHJvcGRvd24tdG9nZ2xlXCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0ZGF0YS10b2dnbGU9XCJkcm9wZG93blwiXHJcblx0XHRcdFx0XHRcdFx0XHRcdGFyaWEtaGFzcG9wdXA9XCJ0cnVlXCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0YXJpYS1leHBhbmRlZD1cImZhbHNlXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cImNhcmV0XCI+PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJzci1vbmx5XCI+VG9nZ2xlIERyb3Bkb3duPC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdDwvYnV0dG9uPlxyXG5cdFx0XHRcdFx0XHRcdDx1bCBjbGFzcz1cImRyb3Bkb3duLW1lbnUgZHJvcGRvd24tbWVudS1yaWdodFwiPjwvdWw+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0YDtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxufSkoanNlLmxpYnMub3JkZXJzX292ZXJ2aWV3X2NvbHVtbnMpOyAiXX0=
