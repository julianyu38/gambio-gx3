'use strict';

/* --------------------------------------------------------------
 editor_values.js 2016-09-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.editor_values = jse.core.editor_values || {};

/**
 * ## Editor Values Library
 *
 * This library provides a common API for editor widget values manipulation.
 *
 * @module Admin/Libs/editor_values
 * @exports jse.libs.editor_values
 */
(function (exports) {

	'use strict';

	/**
  * Editor Get Value Methods 
  * 
  * @type {Object}
  */

	var getValue = {
		ckeditor: function ckeditor($textarea) {
			var name = $textarea.attr('name');
			var instance = CKEDITOR.instances[name];
			return instance.getData();
		},
		codemirror: function codemirror($textarea) {
			var instance = $textarea.siblings('.CodeMirror')[0].CodeMirror;
			return instance.getDoc().getValue();
		}
	};

	/**
  * Editor Set Value Methods 
  * 
  * @type {Object}
  */
	var setValue = {
		ckeditor: function ckeditor($textarea, value) {
			var name = $textarea.attr('name');
			var instance = CKEDITOR.instances[name];
			instance.setData(value);
		},
		codemirror: function codemirror($textarea, value) {
			var instance = $textarea.siblings('.CodeMirror')[0].CodeMirror;
			instance.getDoc().setValue(value);
		}
	};

	/**
  * Get Editor Value 
  * 
  * @param {jQuery} $textarea Textarea selector from which the value will be returned.
  * 
  * @return {String} Returns the editor value. 
  */
	exports.getValue = function ($textarea) {
		var type = $textarea.data('editorType');

		if (!getValue[type]) {
			throw new Error('Provided editor element does not have the supported types: ' + type);
		}

		return getValue[type]($textarea);
	};

	/**
  * Set Editor Value 
  * 
  * @param {jQuery} $textarea Textarea selector to which the value will be set.
  * @param {String} value The new value of the editor. 
  */
	exports.setValue = function ($textarea, value) {
		var type = $textarea.data('editorType');

		if (!getValue[type]) {
			throw new Error('Provided editor element does not have the supported types: ' + type);
		}

		setValue[type]($textarea, value);
	};
})(jse.libs.editor_values);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVkaXRvcl92YWx1ZXMuanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsImVkaXRvcl92YWx1ZXMiLCJjb3JlIiwiZXhwb3J0cyIsImdldFZhbHVlIiwiY2tlZGl0b3IiLCIkdGV4dGFyZWEiLCJuYW1lIiwiYXR0ciIsImluc3RhbmNlIiwiQ0tFRElUT1IiLCJpbnN0YW5jZXMiLCJnZXREYXRhIiwiY29kZW1pcnJvciIsInNpYmxpbmdzIiwiQ29kZU1pcnJvciIsImdldERvYyIsInNldFZhbHVlIiwidmFsdWUiLCJzZXREYXRhIiwidHlwZSIsImRhdGEiLCJFcnJvciJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxJQUFJQyxJQUFKLENBQVNDLGFBQVQsR0FBeUJGLElBQUlHLElBQUosQ0FBU0QsYUFBVCxJQUEwQixFQUFuRDs7QUFFQTs7Ozs7Ozs7QUFRQSxDQUFDLFVBQVNFLE9BQVQsRUFBa0I7O0FBRWxCOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCQyxVQURnQixvQkFDUEMsU0FETyxFQUNJO0FBQ25CLE9BQU1DLE9BQU9ELFVBQVVFLElBQVYsQ0FBZSxNQUFmLENBQWI7QUFDQSxPQUFNQyxXQUFXQyxTQUFTQyxTQUFULENBQW1CSixJQUFuQixDQUFqQjtBQUNBLFVBQU9FLFNBQVNHLE9BQVQsRUFBUDtBQUNBLEdBTGU7QUFPaEJDLFlBUGdCLHNCQU9MUCxTQVBLLEVBT007QUFDckIsT0FBTUcsV0FBV0gsVUFBVVEsUUFBVixDQUFtQixhQUFuQixFQUFrQyxDQUFsQyxFQUFxQ0MsVUFBdEQ7QUFDQSxVQUFPTixTQUFTTyxNQUFULEdBQWtCWixRQUFsQixFQUFQO0FBQ0E7QUFWZSxFQUFqQjs7QUFhQTs7Ozs7QUFLQSxLQUFNYSxXQUFXO0FBQ2hCWixVQURnQixvQkFDUEMsU0FETyxFQUNJWSxLQURKLEVBQ1c7QUFDMUIsT0FBTVgsT0FBT0QsVUFBVUUsSUFBVixDQUFlLE1BQWYsQ0FBYjtBQUNBLE9BQU1DLFdBQVdDLFNBQVNDLFNBQVQsQ0FBbUJKLElBQW5CLENBQWpCO0FBQ0FFLFlBQVNVLE9BQVQsQ0FBaUJELEtBQWpCO0FBQ0EsR0FMZTtBQU9oQkwsWUFQZ0Isc0JBT0xQLFNBUEssRUFPTVksS0FQTixFQU9hO0FBQzVCLE9BQU1ULFdBQVdILFVBQVVRLFFBQVYsQ0FBbUIsYUFBbkIsRUFBa0MsQ0FBbEMsRUFBcUNDLFVBQXREO0FBQ0FOLFlBQVNPLE1BQVQsR0FBa0JDLFFBQWxCLENBQTJCQyxLQUEzQjtBQUNBO0FBVmUsRUFBakI7O0FBYUE7Ozs7Ozs7QUFPQWYsU0FBUUMsUUFBUixHQUFtQixVQUFTRSxTQUFULEVBQW9CO0FBQ3RDLE1BQU1jLE9BQU9kLFVBQVVlLElBQVYsQ0FBZSxZQUFmLENBQWI7O0FBRUEsTUFBSSxDQUFDakIsU0FBU2dCLElBQVQsQ0FBTCxFQUFxQjtBQUNwQixTQUFNLElBQUlFLEtBQUosQ0FBVSxnRUFBZ0VGLElBQTFFLENBQU47QUFDQTs7QUFFRCxTQUFPaEIsU0FBU2dCLElBQVQsRUFBZWQsU0FBZixDQUFQO0FBQ0EsRUFSRDs7QUFVQTs7Ozs7O0FBTUFILFNBQVFjLFFBQVIsR0FBbUIsVUFBU1gsU0FBVCxFQUFvQlksS0FBcEIsRUFBMkI7QUFDN0MsTUFBTUUsT0FBT2QsVUFBVWUsSUFBVixDQUFlLFlBQWYsQ0FBYjs7QUFFQSxNQUFJLENBQUNqQixTQUFTZ0IsSUFBVCxDQUFMLEVBQXFCO0FBQ3BCLFNBQU0sSUFBSUUsS0FBSixDQUFVLGdFQUFnRUYsSUFBMUUsQ0FBTjtBQUNBOztBQUVESCxXQUFTRyxJQUFULEVBQWVkLFNBQWYsRUFBMEJZLEtBQTFCO0FBQ0EsRUFSRDtBQVVBLENBekVELEVBeUVHbkIsSUFBSUMsSUFBSixDQUFTQyxhQXpFWiIsImZpbGUiOiJlZGl0b3JfdmFsdWVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGVkaXRvcl92YWx1ZXMuanMgMjAxNi0wOS0wNlxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbmpzZS5saWJzLmVkaXRvcl92YWx1ZXMgPSBqc2UuY29yZS5lZGl0b3JfdmFsdWVzIHx8IHt9O1xyXG5cclxuLyoqXHJcbiAqICMjIEVkaXRvciBWYWx1ZXMgTGlicmFyeVxyXG4gKlxyXG4gKiBUaGlzIGxpYnJhcnkgcHJvdmlkZXMgYSBjb21tb24gQVBJIGZvciBlZGl0b3Igd2lkZ2V0IHZhbHVlcyBtYW5pcHVsYXRpb24uXHJcbiAqXHJcbiAqIEBtb2R1bGUgQWRtaW4vTGlicy9lZGl0b3JfdmFsdWVzXHJcbiAqIEBleHBvcnRzIGpzZS5saWJzLmVkaXRvcl92YWx1ZXNcclxuICovXHJcbihmdW5jdGlvbihleHBvcnRzKSB7XHJcblx0XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEVkaXRvciBHZXQgVmFsdWUgTWV0aG9kcyBcclxuXHQgKiBcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IGdldFZhbHVlID0ge1xyXG5cdFx0Y2tlZGl0b3IoJHRleHRhcmVhKSB7XHJcblx0XHRcdGNvbnN0IG5hbWUgPSAkdGV4dGFyZWEuYXR0cignbmFtZScpOyBcclxuXHRcdFx0Y29uc3QgaW5zdGFuY2UgPSBDS0VESVRPUi5pbnN0YW5jZXNbbmFtZV07IFxyXG5cdFx0XHRyZXR1cm4gaW5zdGFuY2UuZ2V0RGF0YSgpO1xyXG5cdFx0fSwgXHJcblx0XHRcclxuXHRcdGNvZGVtaXJyb3IoJHRleHRhcmVhKSB7XHJcblx0XHRcdGNvbnN0IGluc3RhbmNlID0gJHRleHRhcmVhLnNpYmxpbmdzKCcuQ29kZU1pcnJvcicpWzBdLkNvZGVNaXJyb3I7IFxyXG5cdFx0XHRyZXR1cm4gaW5zdGFuY2UuZ2V0RG9jKCkuZ2V0VmFsdWUoKTtcclxuXHRcdH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEVkaXRvciBTZXQgVmFsdWUgTWV0aG9kcyBcclxuXHQgKiBcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IHNldFZhbHVlID0ge1xyXG5cdFx0Y2tlZGl0b3IoJHRleHRhcmVhLCB2YWx1ZSkge1xyXG5cdFx0XHRjb25zdCBuYW1lID0gJHRleHRhcmVhLmF0dHIoJ25hbWUnKTtcclxuXHRcdFx0Y29uc3QgaW5zdGFuY2UgPSBDS0VESVRPUi5pbnN0YW5jZXNbbmFtZV07XHJcblx0XHRcdGluc3RhbmNlLnNldERhdGEodmFsdWUpO1xyXG5cdFx0fSxcclxuXHRcdFxyXG5cdFx0Y29kZW1pcnJvcigkdGV4dGFyZWEsIHZhbHVlKSB7XHJcblx0XHRcdGNvbnN0IGluc3RhbmNlID0gJHRleHRhcmVhLnNpYmxpbmdzKCcuQ29kZU1pcnJvcicpWzBdLkNvZGVNaXJyb3I7XHJcblx0XHRcdGluc3RhbmNlLmdldERvYygpLnNldFZhbHVlKHZhbHVlKTtcclxuXHRcdH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEdldCBFZGl0b3IgVmFsdWUgXHJcblx0ICogXHJcblx0ICogQHBhcmFtIHtqUXVlcnl9ICR0ZXh0YXJlYSBUZXh0YXJlYSBzZWxlY3RvciBmcm9tIHdoaWNoIHRoZSB2YWx1ZSB3aWxsIGJlIHJldHVybmVkLlxyXG5cdCAqIFxyXG5cdCAqIEByZXR1cm4ge1N0cmluZ30gUmV0dXJucyB0aGUgZWRpdG9yIHZhbHVlLiBcclxuXHQgKi9cclxuXHRleHBvcnRzLmdldFZhbHVlID0gZnVuY3Rpb24oJHRleHRhcmVhKSB7XHJcblx0XHRjb25zdCB0eXBlID0gJHRleHRhcmVhLmRhdGEoJ2VkaXRvclR5cGUnKTsgXHJcblx0XHRcclxuXHRcdGlmICghZ2V0VmFsdWVbdHlwZV0pIHtcclxuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdQcm92aWRlZCBlZGl0b3IgZWxlbWVudCBkb2VzIG5vdCBoYXZlIHRoZSBzdXBwb3J0ZWQgdHlwZXM6ICcgKyB0eXBlKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0cmV0dXJuIGdldFZhbHVlW3R5cGVdKCR0ZXh0YXJlYSk7IFxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2V0IEVkaXRvciBWYWx1ZSBcclxuXHQgKiBcclxuXHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRleHRhcmVhIFRleHRhcmVhIHNlbGVjdG9yIHRvIHdoaWNoIHRoZSB2YWx1ZSB3aWxsIGJlIHNldC5cclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gdmFsdWUgVGhlIG5ldyB2YWx1ZSBvZiB0aGUgZWRpdG9yLiBcclxuXHQgKi9cclxuXHRleHBvcnRzLnNldFZhbHVlID0gZnVuY3Rpb24oJHRleHRhcmVhLCB2YWx1ZSkge1xyXG5cdFx0Y29uc3QgdHlwZSA9ICR0ZXh0YXJlYS5kYXRhKCdlZGl0b3JUeXBlJyk7XHJcblx0XHRcclxuXHRcdGlmICghZ2V0VmFsdWVbdHlwZV0pIHtcclxuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdQcm92aWRlZCBlZGl0b3IgZWxlbWVudCBkb2VzIG5vdCBoYXZlIHRoZSBzdXBwb3J0ZWQgdHlwZXM6ICcgKyB0eXBlKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0c2V0VmFsdWVbdHlwZV0oJHRleHRhcmVhLCB2YWx1ZSk7XHJcblx0fTtcclxuXHRcclxufSkoanNlLmxpYnMuZWRpdG9yX3ZhbHVlcyk7ICJdfQ==
