'use strict';

/* --------------------------------------------------------------
 search.js 2016-10-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.search = jse.libs.search || {};

/**
 * ## Admin search library.
 *
 * This module provides the search URLs and the configuration key for the admin search controller.
 * Additionally you can set a pre-defined search value by overriding the default search value.
 *
 * @module Admin/Libs/search
 * @exports jse.libs.search
 */
(function (exports) {
	// User configuration key.
	exports.configurationKey = 'recent_search_area';

	// Search areas URLs.
	exports.urls = {
		// Customers
		customers: 'customers.php?' + $.param({ search: '' }),

		// Categories and products
		categories: 'categories.php?' + $.param({ search: '' }),

		// Orders
		orders: 'admin.php?' + $.param({
			do: 'OrdersOverview',
			filter: {
				number: ''
			}
		}),

		// Invoices
		invoices: 'admin.php?' + $.param({
			do: 'InvoicesOverview',
			filter: {
				invoiceNumber: ''
			}
		})
	};

	/**
  * Replaces the admin search input value with the given one.
  *
  * @param {String} term Search term.
  * @param {Boolean} doFocus Do focus on the input field?
  */
	exports.setValue = function (term, doFocus) {
		return $('#search-controller').trigger('set:value', [term, doFocus]);
	};
})(jse.libs.search);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlYXJjaC5qcyJdLCJuYW1lcyI6WyJqc2UiLCJsaWJzIiwic2VhcmNoIiwiZXhwb3J0cyIsImNvbmZpZ3VyYXRpb25LZXkiLCJ1cmxzIiwiY3VzdG9tZXJzIiwiJCIsInBhcmFtIiwiY2F0ZWdvcmllcyIsIm9yZGVycyIsImRvIiwiZmlsdGVyIiwibnVtYmVyIiwiaW52b2ljZXMiLCJpbnZvaWNlTnVtYmVyIiwic2V0VmFsdWUiLCJ0ZXJtIiwiZG9Gb2N1cyIsInRyaWdnZXIiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQyxNQUFULEdBQWtCRixJQUFJQyxJQUFKLENBQVNDLE1BQVQsSUFBbUIsRUFBckM7O0FBRUE7Ozs7Ozs7OztBQVNDLFdBQVNDLE9BQVQsRUFBa0I7QUFDbEI7QUFDQUEsU0FBUUMsZ0JBQVIsR0FBMkIsb0JBQTNCOztBQUVBO0FBQ0FELFNBQVFFLElBQVIsR0FBZTtBQUNkO0FBQ0FDLGFBQVcsbUJBQW1CQyxFQUFFQyxLQUFGLENBQVEsRUFBQ04sUUFBUSxFQUFULEVBQVIsQ0FGaEI7O0FBSWQ7QUFDQU8sY0FBWSxvQkFBb0JGLEVBQUVDLEtBQUYsQ0FBUSxFQUFDTixRQUFRLEVBQVQsRUFBUixDQUxsQjs7QUFPZDtBQUNBUSxVQUFRLGVBQWVILEVBQUVDLEtBQUYsQ0FBUTtBQUM5QkcsT0FBSSxnQkFEMEI7QUFFOUJDLFdBQVE7QUFDUEMsWUFBUTtBQUREO0FBRnNCLEdBQVIsQ0FSVDs7QUFlZDtBQUNBQyxZQUFVLGVBQWVQLEVBQUVDLEtBQUYsQ0FBUTtBQUNoQ0csT0FBSSxrQkFENEI7QUFFaENDLFdBQVE7QUFDUEcsbUJBQWU7QUFEUjtBQUZ3QixHQUFSO0FBaEJYLEVBQWY7O0FBd0JBOzs7Ozs7QUFNQVosU0FBUWEsUUFBUixHQUFtQixVQUFDQyxJQUFELEVBQU9DLE9BQVA7QUFBQSxTQUFtQlgsRUFBRSxvQkFBRixFQUF3QlksT0FBeEIsQ0FBZ0MsV0FBaEMsRUFBNkMsQ0FBQ0YsSUFBRCxFQUFPQyxPQUFQLENBQTdDLENBQW5CO0FBQUEsRUFBbkI7QUFDQSxDQXBDQSxFQW9DQ2xCLElBQUlDLElBQUosQ0FBU0MsTUFwQ1YsQ0FBRCIsImZpbGUiOiJzZWFyY2guanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNlYXJjaC5qcyAyMDE2LTEwLTExXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuanNlLmxpYnMuc2VhcmNoID0ganNlLmxpYnMuc2VhcmNoIHx8IHt9O1xuXG4vKipcbiAqICMjIEFkbWluIHNlYXJjaCBsaWJyYXJ5LlxuICpcbiAqIFRoaXMgbW9kdWxlIHByb3ZpZGVzIHRoZSBzZWFyY2ggVVJMcyBhbmQgdGhlIGNvbmZpZ3VyYXRpb24ga2V5IGZvciB0aGUgYWRtaW4gc2VhcmNoIGNvbnRyb2xsZXIuXG4gKiBBZGRpdGlvbmFsbHkgeW91IGNhbiBzZXQgYSBwcmUtZGVmaW5lZCBzZWFyY2ggdmFsdWUgYnkgb3ZlcnJpZGluZyB0aGUgZGVmYXVsdCBzZWFyY2ggdmFsdWUuXG4gKlxuICogQG1vZHVsZSBBZG1pbi9MaWJzL3NlYXJjaFxuICogQGV4cG9ydHMganNlLmxpYnMuc2VhcmNoXG4gKi9cbihmdW5jdGlvbihleHBvcnRzKSB7XG5cdC8vIFVzZXIgY29uZmlndXJhdGlvbiBrZXkuXG5cdGV4cG9ydHMuY29uZmlndXJhdGlvbktleSA9ICdyZWNlbnRfc2VhcmNoX2FyZWEnO1xuXG5cdC8vIFNlYXJjaCBhcmVhcyBVUkxzLlxuXHRleHBvcnRzLnVybHMgPSB7XG5cdFx0Ly8gQ3VzdG9tZXJzXG5cdFx0Y3VzdG9tZXJzOiAnY3VzdG9tZXJzLnBocD8nICsgJC5wYXJhbSh7c2VhcmNoOiAnJ30pLFxuXG5cdFx0Ly8gQ2F0ZWdvcmllcyBhbmQgcHJvZHVjdHNcblx0XHRjYXRlZ29yaWVzOiAnY2F0ZWdvcmllcy5waHA/JyArICQucGFyYW0oe3NlYXJjaDogJyd9KSxcblxuXHRcdC8vIE9yZGVyc1xuXHRcdG9yZGVyczogJ2FkbWluLnBocD8nICsgJC5wYXJhbSh7XG5cdFx0XHRkbzogJ09yZGVyc092ZXJ2aWV3Jyxcblx0XHRcdGZpbHRlcjoge1xuXHRcdFx0XHRudW1iZXI6ICcnXG5cdFx0XHR9XG5cdFx0fSksXG5cdFx0XG5cdFx0Ly8gSW52b2ljZXNcblx0XHRpbnZvaWNlczogJ2FkbWluLnBocD8nICsgJC5wYXJhbSh7XG5cdFx0XHRkbzogJ0ludm9pY2VzT3ZlcnZpZXcnLFxuXHRcdFx0ZmlsdGVyOiB7XG5cdFx0XHRcdGludm9pY2VOdW1iZXI6ICcnXG5cdFx0XHR9XG5cdFx0fSlcblx0fTtcblxuXHQvKipcblx0ICogUmVwbGFjZXMgdGhlIGFkbWluIHNlYXJjaCBpbnB1dCB2YWx1ZSB3aXRoIHRoZSBnaXZlbiBvbmUuXG5cdCAqXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSB0ZXJtIFNlYXJjaCB0ZXJtLlxuXHQgKiBAcGFyYW0ge0Jvb2xlYW59IGRvRm9jdXMgRG8gZm9jdXMgb24gdGhlIGlucHV0IGZpZWxkP1xuXHQgKi9cblx0ZXhwb3J0cy5zZXRWYWx1ZSA9ICh0ZXJtLCBkb0ZvY3VzKSA9PiAkKCcjc2VhcmNoLWNvbnRyb2xsZXInKS50cmlnZ2VyKCdzZXQ6dmFsdWUnLCBbdGVybSwgZG9Gb2N1c10pO1xufShqc2UubGlicy5zZWFyY2gpKTtcbiJdfQ==
