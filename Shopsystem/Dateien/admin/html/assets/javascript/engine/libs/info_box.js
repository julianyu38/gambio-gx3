'use strict';

/* --------------------------------------------------------------
 info_box.js 2016-09-21
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.info_box = jse.libs.info_box || {};

/**
 * ## Info Box Messages Library
 *
 * This module provides an API to the new admin layout pages info box.
 *
 * @module Admin/Libs/info_box
 * @exports jse.libs.info_box
 */
(function (exports) {

	'use strict';

	// Admin info box element selector.

	var infoboxSelector = '.info-box';

	/**
  * Performs an ajax request to the server.
  *
  * @param {String} action URL action part.
  * @param {String} method HTTP request method.
  * @param {Object} data   Request data.
  * 
  * @return {Deferred|Promise}
  * 
  * @private
  */
	function _performRequest(action, method, data) {
		var URL_BASE = 'admin.php?do=AdminInfoBoxAjax';

		// AJAX request options.
		var ajaxOptions = {
			url: URL_BASE + action,
			data: data,
			method: method
		};

		// Returns deferred object.
		return $.ajax(ajaxOptions);
	}

	// Message status.
	exports.STATUS_NEW = 'new';
	exports.STATUS_READ = 'read';
	exports.STATUS_HIDDEN = 'hidden';
	exports.STATUS_DELETED = 'deleted';

	// Message types.
	exports.TYPE_INFO = 'info';
	exports.TYPE_WARNING = 'warning';
	exports.TYPE_SUCCESS = 'success';

	// Message visibility.
	exports.VISIBILITY_ALWAYS_ON = 'alwayson';
	exports.VISIBILITY_HIDEABLE = 'hideable';
	exports.VISIBILITY_REMOVABLE = 'removable';

	// Admin action success message identifier prefix.
	exports.SUCCESS_MSG_IDENTIFIER_PREFIX = 'adminActionSuccess-';

	/**
  * Returns the messages from the server (visible only).
  *
  * @return {Promise}
  */
	exports.getMessages = function () {
		return _performRequest('/GetAllMessages', 'GET').then(JSON.parse);
	};

	/**
  * Sets the status of a message.
  *
  * @param {Number} id Message ID.
  * @param {String} status Message status to set ('new', 'read', 'hidden', 'deleted').
  * 
  * @return {Promise}
  */
	exports.setStatus = function (id, status) {
		// Valid message status.
		var validStatus = [exports.STATUS_NEW, exports.STATUS_READ, exports.STATUS_HIDDEN, exports.STATUS_DELETED];

		// Check arguments.
		if (!id || !status) {
			throw new Error('Missing ID or status');
		} else if (validStatus.indexOf(status) === -1) {
			throw new Error('Invalid status provided');
		}

		return _performRequest('/SetMessageStatus', 'GET', { id: id, status: status });
	};

	/**
  * Deletes a message.
  *
  * @param {Number} id Message ID.
  * 
  * @return {Promise}
  */
	exports.deleteById = function (id) {
		if (!id) {
			throw new Error('Missing ID');
		}

		return _performRequest('/DeleteById', 'GET', { id: id });
	};

	/**
  * Deletes a message by source.
  * 
  * @param {String} source Message source.
  * 
  * @return {Promise}
  */
	exports.deleteBySource = function (source) {
		if (!source) {
			throw new Error('Missing source');
		}

		return _performRequest('/DeleteBySource', 'GET', { source: source });
	};

	/**
  * Deletes a messages by the identifier.
  * 
  * @param {String} identifier Message identifier.
  * 
  * @return {Promise}
  */
	exports.deleteByIdentifier = function (identifier) {
		if (!identifier) {
			throw new Error('Missing identifier');
		}

		return _performRequest('/DeleteByIdentifier', 'GET', { identifier: identifier });
	};

	/**
  * Reactivates the messages.
  * @return {Promise}
  * @static
  */
	exports.Reactivates = function () {
		return _performRequest('/ReactivateMessages', 'GET');
	};

	/**
  * Saves a new message.
  * 
  * @param {Object} message The new message to save.
  * 
  * @example
  * jse.libs.info_box.addMessage({
     *   source: 'ajax',
     *   identifier: 'asdas',
     *   status: 'new',
     *   type: 'success',
     *   visibility: 'removable',
     *   customerId: 0,
     *   headline: 'My Headline',
     *   buttonLabel: 'asdas',
     *   buttonLink: 'http://google.com', // optional
  *	 buttonLink: 'customers.php', // optional
     *   message: 'Hallo!',
     * });
  * 
  * @return {Promise}
  */
	exports.addMessage = function (message) {
		if (!message) {
			throw new Error('Missing message object');
		} else if (!message.source) {
			throw new Error('Missing source');
		} else if (!message.identifier) {
			message.identifier = 'generated-' + Date.now();
		} else if (!message.status || [exports.STATUS_NEW, exports.STATUS_READ, exports.STATUS_HIDDEN, exports.STATUS_DELETED].indexOf(message.status) === -1) {
			throw new Error('Missing or invalid status');
		} else if (!message.type || [exports.TYPE_INFO, exports.TYPE_WARNING, exports.TYPE_SUCCESS].indexOf(message.type) === -1) {
			throw new Error('Missing or invalid type');
		} else if (!message.visibility || [exports.VISIBILITY_ALWAYS_ON, exports.VISIBILITY_HIDEABLE, exports.VISIBILITY_REMOVABLE].indexOf(message.visibility) === -1) {
			throw new Error('Missing or invalid visibility');
		} else if (typeof message.customerId === 'undefined') {
			throw new Error('Missing customer ID');
		} else if (!message.message) {
			throw new Error('Missing message');
		} else if (!message.headline) {
			throw new Error('Missing headline');
		} else if (!message.message) {
			throw new Error('Missing message');
		}

		return _performRequest('/AddMessage', 'GET', message);
	};

	/**
  * Adds a success message to the admin info box.
  * 
  * @param {String} [message] Message to show.
  * @param {Boolean} [skipRefresh = false] Refresh the admin info box to show the message?
  */
	exports.addSuccessMessage = function (message) {
		var skipRefresh = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

		// Add message.
		var request = _performRequest('/AddSuccessMessage', 'GET', message ? { message: message } : {});

		// Optionally refresh the admin info box to show the message.
		if (!skipRefresh) {
			request.done(function () {
				return $(infoboxSelector).trigger('refresh:messages');
			});
		}
	};
})(jse.libs.info_box);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZm9fYm94LmpzIl0sIm5hbWVzIjpbImpzZSIsImxpYnMiLCJpbmZvX2JveCIsImV4cG9ydHMiLCJpbmZvYm94U2VsZWN0b3IiLCJfcGVyZm9ybVJlcXVlc3QiLCJhY3Rpb24iLCJtZXRob2QiLCJkYXRhIiwiVVJMX0JBU0UiLCJhamF4T3B0aW9ucyIsInVybCIsIiQiLCJhamF4IiwiU1RBVFVTX05FVyIsIlNUQVRVU19SRUFEIiwiU1RBVFVTX0hJRERFTiIsIlNUQVRVU19ERUxFVEVEIiwiVFlQRV9JTkZPIiwiVFlQRV9XQVJOSU5HIiwiVFlQRV9TVUNDRVNTIiwiVklTSUJJTElUWV9BTFdBWVNfT04iLCJWSVNJQklMSVRZX0hJREVBQkxFIiwiVklTSUJJTElUWV9SRU1PVkFCTEUiLCJTVUNDRVNTX01TR19JREVOVElGSUVSX1BSRUZJWCIsImdldE1lc3NhZ2VzIiwidGhlbiIsIkpTT04iLCJwYXJzZSIsInNldFN0YXR1cyIsImlkIiwic3RhdHVzIiwidmFsaWRTdGF0dXMiLCJFcnJvciIsImluZGV4T2YiLCJkZWxldGVCeUlkIiwiZGVsZXRlQnlTb3VyY2UiLCJzb3VyY2UiLCJkZWxldGVCeUlkZW50aWZpZXIiLCJpZGVudGlmaWVyIiwiUmVhY3RpdmF0ZXMiLCJhZGRNZXNzYWdlIiwibWVzc2FnZSIsIkRhdGUiLCJub3ciLCJ0eXBlIiwidmlzaWJpbGl0eSIsImN1c3RvbWVySWQiLCJoZWFkbGluZSIsImFkZFN1Y2Nlc3NNZXNzYWdlIiwic2tpcFJlZnJlc2giLCJyZXF1ZXN0IiwiZG9uZSIsInRyaWdnZXIiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQyxRQUFULEdBQW9CRixJQUFJQyxJQUFKLENBQVNDLFFBQVQsSUFBcUIsRUFBekM7O0FBRUE7Ozs7Ozs7O0FBUUMsV0FBU0MsT0FBVCxFQUFrQjs7QUFFbEI7O0FBRUE7O0FBQ0EsS0FBTUMsa0JBQWtCLFdBQXhCOztBQUVBOzs7Ozs7Ozs7OztBQVdBLFVBQVNDLGVBQVQsQ0FBeUJDLE1BQXpCLEVBQWlDQyxNQUFqQyxFQUF5Q0MsSUFBekMsRUFBK0M7QUFDOUMsTUFBTUMsV0FBVywrQkFBakI7O0FBRUE7QUFDQSxNQUFNQyxjQUFjO0FBQ25CQyxRQUFLRixXQUFXSCxNQURHO0FBRW5CRSxhQUZtQjtBQUduQkQ7QUFIbUIsR0FBcEI7O0FBTUE7QUFDQSxTQUFPSyxFQUFFQyxJQUFGLENBQU9ILFdBQVAsQ0FBUDtBQUNBOztBQUVEO0FBQ0FQLFNBQVFXLFVBQVIsR0FBcUIsS0FBckI7QUFDQVgsU0FBUVksV0FBUixHQUFzQixNQUF0QjtBQUNBWixTQUFRYSxhQUFSLEdBQXdCLFFBQXhCO0FBQ0FiLFNBQVFjLGNBQVIsR0FBeUIsU0FBekI7O0FBRUE7QUFDQWQsU0FBUWUsU0FBUixHQUFvQixNQUFwQjtBQUNBZixTQUFRZ0IsWUFBUixHQUF1QixTQUF2QjtBQUNBaEIsU0FBUWlCLFlBQVIsR0FBdUIsU0FBdkI7O0FBRUE7QUFDQWpCLFNBQVFrQixvQkFBUixHQUErQixVQUEvQjtBQUNBbEIsU0FBUW1CLG1CQUFSLEdBQThCLFVBQTlCO0FBQ0FuQixTQUFRb0Isb0JBQVIsR0FBK0IsV0FBL0I7O0FBRUE7QUFDQXBCLFNBQVFxQiw2QkFBUixHQUF3QyxxQkFBeEM7O0FBRUE7Ozs7O0FBS0FyQixTQUFRc0IsV0FBUixHQUFzQixZQUFXO0FBQ2hDLFNBQU9wQixnQkFBZ0IsaUJBQWhCLEVBQW1DLEtBQW5DLEVBQTBDcUIsSUFBMUMsQ0FBK0NDLEtBQUtDLEtBQXBELENBQVA7QUFDQSxFQUZEOztBQUlBOzs7Ozs7OztBQVFBekIsU0FBUTBCLFNBQVIsR0FBb0IsVUFBU0MsRUFBVCxFQUFhQyxNQUFiLEVBQXFCO0FBQ3hDO0FBQ0EsTUFBTUMsY0FBYyxDQUFDN0IsUUFBUVcsVUFBVCxFQUFxQlgsUUFBUVksV0FBN0IsRUFBMENaLFFBQVFhLGFBQWxELEVBQWlFYixRQUFRYyxjQUF6RSxDQUFwQjs7QUFFQTtBQUNBLE1BQUksQ0FBQ2EsRUFBRCxJQUFPLENBQUNDLE1BQVosRUFBb0I7QUFDbkIsU0FBTSxJQUFJRSxLQUFKLENBQVUsc0JBQVYsQ0FBTjtBQUNBLEdBRkQsTUFFTyxJQUFJRCxZQUFZRSxPQUFaLENBQW9CSCxNQUFwQixNQUFnQyxDQUFDLENBQXJDLEVBQXdDO0FBQzlDLFNBQU0sSUFBSUUsS0FBSixDQUFVLHlCQUFWLENBQU47QUFDQTs7QUFFRCxTQUFPNUIsZ0JBQWdCLG1CQUFoQixFQUFxQyxLQUFyQyxFQUE0QyxFQUFDeUIsTUFBRCxFQUFLQyxjQUFMLEVBQTVDLENBQVA7QUFDQSxFQVpEOztBQWNBOzs7Ozs7O0FBT0E1QixTQUFRZ0MsVUFBUixHQUFxQixVQUFTTCxFQUFULEVBQWE7QUFDakMsTUFBSSxDQUFDQSxFQUFMLEVBQVM7QUFDUixTQUFNLElBQUlHLEtBQUosQ0FBVSxZQUFWLENBQU47QUFDQTs7QUFFRCxTQUFPNUIsZ0JBQWdCLGFBQWhCLEVBQStCLEtBQS9CLEVBQXNDLEVBQUN5QixNQUFELEVBQXRDLENBQVA7QUFDQSxFQU5EOztBQVFBOzs7Ozs7O0FBT0EzQixTQUFRaUMsY0FBUixHQUF5QixVQUFTQyxNQUFULEVBQWlCO0FBQ3pDLE1BQUksQ0FBQ0EsTUFBTCxFQUFhO0FBQ1osU0FBTSxJQUFJSixLQUFKLENBQVUsZ0JBQVYsQ0FBTjtBQUNBOztBQUVELFNBQU81QixnQkFBZ0IsaUJBQWhCLEVBQW1DLEtBQW5DLEVBQTBDLEVBQUNnQyxjQUFELEVBQTFDLENBQVA7QUFDQSxFQU5EOztBQVFBOzs7Ozs7O0FBT0FsQyxTQUFRbUMsa0JBQVIsR0FBNkIsVUFBU0MsVUFBVCxFQUFxQjtBQUNqRCxNQUFJLENBQUNBLFVBQUwsRUFBaUI7QUFDaEIsU0FBTSxJQUFJTixLQUFKLENBQVUsb0JBQVYsQ0FBTjtBQUNBOztBQUVELFNBQU81QixnQkFBZ0IscUJBQWhCLEVBQXVDLEtBQXZDLEVBQThDLEVBQUNrQyxzQkFBRCxFQUE5QyxDQUFQO0FBQ0EsRUFORDs7QUFRQTs7Ozs7QUFLQXBDLFNBQVFxQyxXQUFSLEdBQXNCLFlBQVc7QUFDaEMsU0FBT25DLGdCQUFnQixxQkFBaEIsRUFBdUMsS0FBdkMsQ0FBUDtBQUNBLEVBRkQ7O0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFzQkFGLFNBQVFzQyxVQUFSLEdBQXFCLFVBQVNDLE9BQVQsRUFBa0I7QUFDdEMsTUFBSSxDQUFDQSxPQUFMLEVBQWM7QUFDYixTQUFNLElBQUlULEtBQUosQ0FBVSx3QkFBVixDQUFOO0FBQ0EsR0FGRCxNQUVPLElBQUksQ0FBQ1MsUUFBUUwsTUFBYixFQUFxQjtBQUMzQixTQUFNLElBQUlKLEtBQUosQ0FBVSxnQkFBVixDQUFOO0FBQ0EsR0FGTSxNQUVBLElBQUksQ0FBQ1MsUUFBUUgsVUFBYixFQUF5QjtBQUMvQkcsV0FBUUgsVUFBUixHQUFxQixlQUFlSSxLQUFLQyxHQUFMLEVBQXBDO0FBQ0EsR0FGTSxNQUVBLElBQUksQ0FBQ0YsUUFBUVgsTUFBVCxJQUFtQixDQUM1QjVCLFFBQVFXLFVBRG9CLEVBQ1JYLFFBQVFZLFdBREEsRUFDYVosUUFBUWEsYUFEckIsRUFFNUJiLFFBQVFjLGNBRm9CLEVBRzNCaUIsT0FIMkIsQ0FHbkJRLFFBQVFYLE1BSFcsTUFHQyxDQUFDLENBSHpCLEVBRzRCO0FBQ2xDLFNBQU0sSUFBSUUsS0FBSixDQUFVLDJCQUFWLENBQU47QUFDQSxHQUxNLE1BS0EsSUFBSSxDQUFDUyxRQUFRRyxJQUFULElBQWlCLENBQzFCMUMsUUFBUWUsU0FEa0IsRUFDUGYsUUFBUWdCLFlBREQsRUFFMUJoQixRQUFRaUIsWUFGa0IsRUFHekJjLE9BSHlCLENBR2pCUSxRQUFRRyxJQUhTLE1BR0MsQ0FBQyxDQUh2QixFQUcwQjtBQUNoQyxTQUFNLElBQUlaLEtBQUosQ0FBVSx5QkFBVixDQUFOO0FBQ0EsR0FMTSxNQUtBLElBQUksQ0FBQ1MsUUFBUUksVUFBVCxJQUF1QixDQUNoQzNDLFFBQVFrQixvQkFEd0IsRUFDRmxCLFFBQVFtQixtQkFETixFQUVoQ25CLFFBQVFvQixvQkFGd0IsRUFHL0JXLE9BSCtCLENBR3ZCUSxRQUFRSSxVQUhlLE1BR0MsQ0FBQyxDQUg3QixFQUdnQztBQUN0QyxTQUFNLElBQUliLEtBQUosQ0FBVSwrQkFBVixDQUFOO0FBQ0EsR0FMTSxNQUtBLElBQUksT0FBT1MsUUFBUUssVUFBZixLQUE4QixXQUFsQyxFQUErQztBQUNyRCxTQUFNLElBQUlkLEtBQUosQ0FBVSxxQkFBVixDQUFOO0FBQ0EsR0FGTSxNQUVBLElBQUksQ0FBQ1MsUUFBUUEsT0FBYixFQUFzQjtBQUM1QixTQUFNLElBQUlULEtBQUosQ0FBVSxpQkFBVixDQUFOO0FBQ0EsR0FGTSxNQUVBLElBQUksQ0FBQ1MsUUFBUU0sUUFBYixFQUF1QjtBQUM3QixTQUFNLElBQUlmLEtBQUosQ0FBVSxrQkFBVixDQUFOO0FBQ0EsR0FGTSxNQUVBLElBQUksQ0FBQ1MsUUFBUUEsT0FBYixFQUFzQjtBQUM1QixTQUFNLElBQUlULEtBQUosQ0FBVSxpQkFBVixDQUFOO0FBQ0E7O0FBRUQsU0FBTzVCLGdCQUFnQixhQUFoQixFQUErQixLQUEvQixFQUFzQ3FDLE9BQXRDLENBQVA7QUFDQSxFQWpDRDs7QUFtQ0E7Ozs7OztBQU1BdkMsU0FBUThDLGlCQUFSLEdBQTRCLFVBQVNQLE9BQVQsRUFBdUM7QUFBQSxNQUFyQlEsV0FBcUIsdUVBQVAsS0FBTzs7QUFDbEU7QUFDQSxNQUFNQyxVQUFVOUMsZ0JBQWdCLG9CQUFoQixFQUFzQyxLQUF0QyxFQUE2Q3FDLFVBQVUsRUFBQ0EsZ0JBQUQsRUFBVixHQUFzQixFQUFuRSxDQUFoQjs7QUFFQTtBQUNBLE1BQUksQ0FBQ1EsV0FBTCxFQUFrQjtBQUNqQkMsV0FBUUMsSUFBUixDQUFhO0FBQUEsV0FBTXhDLEVBQUVSLGVBQUYsRUFBbUJpRCxPQUFuQixDQUEyQixrQkFBM0IsQ0FBTjtBQUFBLElBQWI7QUFDQTtBQUNELEVBUkQ7QUFTQSxDQWhOQSxFQWdOQ3JELElBQUlDLElBQUosQ0FBU0MsUUFoTlYsQ0FBRCIsImZpbGUiOiJpbmZvX2JveC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gaW5mb19ib3guanMgMjAxNi0wOS0yMVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmpzZS5saWJzLmluZm9fYm94ID0ganNlLmxpYnMuaW5mb19ib3ggfHwge307XG5cbi8qKlxuICogIyMgSW5mbyBCb3ggTWVzc2FnZXMgTGlicmFyeVxuICpcbiAqIFRoaXMgbW9kdWxlIHByb3ZpZGVzIGFuIEFQSSB0byB0aGUgbmV3IGFkbWluIGxheW91dCBwYWdlcyBpbmZvIGJveC5cbiAqXG4gKiBAbW9kdWxlIEFkbWluL0xpYnMvaW5mb19ib3hcbiAqIEBleHBvcnRzIGpzZS5saWJzLmluZm9fYm94XG4gKi9cbihmdW5jdGlvbihleHBvcnRzKSB7XG5cdFxuXHQndXNlIHN0cmljdCc7XG5cdFxuXHQvLyBBZG1pbiBpbmZvIGJveCBlbGVtZW50IHNlbGVjdG9yLlxuXHRjb25zdCBpbmZvYm94U2VsZWN0b3IgPSAnLmluZm8tYm94JztcblxuXHQvKipcblx0ICogUGVyZm9ybXMgYW4gYWpheCByZXF1ZXN0IHRvIHRoZSBzZXJ2ZXIuXG5cdCAqXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBhY3Rpb24gVVJMIGFjdGlvbiBwYXJ0LlxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbWV0aG9kIEhUVFAgcmVxdWVzdCBtZXRob2QuXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBkYXRhICAgUmVxdWVzdCBkYXRhLlxuXHQgKiBcblx0ICogQHJldHVybiB7RGVmZXJyZWR8UHJvbWlzZX1cblx0ICogXG5cdCAqIEBwcml2YXRlXG5cdCAqL1xuXHRmdW5jdGlvbiBfcGVyZm9ybVJlcXVlc3QoYWN0aW9uLCBtZXRob2QsIGRhdGEpIHtcblx0XHRjb25zdCBVUkxfQkFTRSA9ICdhZG1pbi5waHA/ZG89QWRtaW5JbmZvQm94QWpheCc7XG5cblx0XHQvLyBBSkFYIHJlcXVlc3Qgb3B0aW9ucy5cblx0XHRjb25zdCBhamF4T3B0aW9ucyA9IHtcblx0XHRcdHVybDogVVJMX0JBU0UgKyBhY3Rpb24sXG5cdFx0XHRkYXRhLFxuXHRcdFx0bWV0aG9kXG5cdFx0fTtcblxuXHRcdC8vIFJldHVybnMgZGVmZXJyZWQgb2JqZWN0LlxuXHRcdHJldHVybiAkLmFqYXgoYWpheE9wdGlvbnMpO1xuXHR9XG5cblx0Ly8gTWVzc2FnZSBzdGF0dXMuXG5cdGV4cG9ydHMuU1RBVFVTX05FVyA9ICduZXcnO1xuXHRleHBvcnRzLlNUQVRVU19SRUFEID0gJ3JlYWQnO1xuXHRleHBvcnRzLlNUQVRVU19ISURERU4gPSAnaGlkZGVuJztcblx0ZXhwb3J0cy5TVEFUVVNfREVMRVRFRCA9ICdkZWxldGVkJztcblx0XG5cdC8vIE1lc3NhZ2UgdHlwZXMuXG5cdGV4cG9ydHMuVFlQRV9JTkZPID0gJ2luZm8nO1xuXHRleHBvcnRzLlRZUEVfV0FSTklORyA9ICd3YXJuaW5nJztcblx0ZXhwb3J0cy5UWVBFX1NVQ0NFU1MgPSAnc3VjY2Vzcyc7XG5cdFxuXHQvLyBNZXNzYWdlIHZpc2liaWxpdHkuXG5cdGV4cG9ydHMuVklTSUJJTElUWV9BTFdBWVNfT04gPSAnYWx3YXlzb24nO1xuXHRleHBvcnRzLlZJU0lCSUxJVFlfSElERUFCTEUgPSAnaGlkZWFibGUnO1xuXHRleHBvcnRzLlZJU0lCSUxJVFlfUkVNT1ZBQkxFID0gJ3JlbW92YWJsZSc7XG5cdFxuXHQvLyBBZG1pbiBhY3Rpb24gc3VjY2VzcyBtZXNzYWdlIGlkZW50aWZpZXIgcHJlZml4LlxuXHRleHBvcnRzLlNVQ0NFU1NfTVNHX0lERU5USUZJRVJfUFJFRklYID0gJ2FkbWluQWN0aW9uU3VjY2Vzcy0nO1xuXHRcblx0LyoqXG5cdCAqIFJldHVybnMgdGhlIG1lc3NhZ2VzIGZyb20gdGhlIHNlcnZlciAodmlzaWJsZSBvbmx5KS5cblx0ICpcblx0ICogQHJldHVybiB7UHJvbWlzZX1cblx0ICovXG5cdGV4cG9ydHMuZ2V0TWVzc2FnZXMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gX3BlcmZvcm1SZXF1ZXN0KCcvR2V0QWxsTWVzc2FnZXMnLCAnR0VUJykudGhlbihKU09OLnBhcnNlKTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBTZXRzIHRoZSBzdGF0dXMgb2YgYSBtZXNzYWdlLlxuXHQgKlxuXHQgKiBAcGFyYW0ge051bWJlcn0gaWQgTWVzc2FnZSBJRC5cblx0ICogQHBhcmFtIHtTdHJpbmd9IHN0YXR1cyBNZXNzYWdlIHN0YXR1cyB0byBzZXQgKCduZXcnLCAncmVhZCcsICdoaWRkZW4nLCAnZGVsZXRlZCcpLlxuXHQgKiBcblx0ICogQHJldHVybiB7UHJvbWlzZX1cblx0ICovXG5cdGV4cG9ydHMuc2V0U3RhdHVzID0gZnVuY3Rpb24oaWQsIHN0YXR1cykge1xuXHRcdC8vIFZhbGlkIG1lc3NhZ2Ugc3RhdHVzLlxuXHRcdGNvbnN0IHZhbGlkU3RhdHVzID0gW2V4cG9ydHMuU1RBVFVTX05FVywgZXhwb3J0cy5TVEFUVVNfUkVBRCwgZXhwb3J0cy5TVEFUVVNfSElEREVOLCBleHBvcnRzLlNUQVRVU19ERUxFVEVEXTtcblx0XHRcblx0XHQvLyBDaGVjayBhcmd1bWVudHMuXG5cdFx0aWYgKCFpZCB8fCAhc3RhdHVzKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgSUQgb3Igc3RhdHVzJyk7XG5cdFx0fSBlbHNlIGlmICh2YWxpZFN0YXR1cy5pbmRleE9mKHN0YXR1cykgPT09IC0xKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgc3RhdHVzIHByb3ZpZGVkJyk7XG5cdFx0fVxuXHRcdFxuXHRcdHJldHVybiBfcGVyZm9ybVJlcXVlc3QoJy9TZXRNZXNzYWdlU3RhdHVzJywgJ0dFVCcsIHtpZCwgc3RhdHVzfSlcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBEZWxldGVzIGEgbWVzc2FnZS5cblx0ICpcblx0ICogQHBhcmFtIHtOdW1iZXJ9IGlkIE1lc3NhZ2UgSUQuXG5cdCAqIFxuXHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHQgKi9cblx0ZXhwb3J0cy5kZWxldGVCeUlkID0gZnVuY3Rpb24oaWQpIHtcblx0XHRpZiAoIWlkKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgSUQnKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIF9wZXJmb3JtUmVxdWVzdCgnL0RlbGV0ZUJ5SWQnLCAnR0VUJywge2lkfSk7XG5cdH07XG5cdFxuXHQvKipcblx0ICogRGVsZXRlcyBhIG1lc3NhZ2UgYnkgc291cmNlLlxuXHQgKiBcblx0ICogQHBhcmFtIHtTdHJpbmd9IHNvdXJjZSBNZXNzYWdlIHNvdXJjZS5cblx0ICogXG5cdCAqIEByZXR1cm4ge1Byb21pc2V9XG5cdCAqL1xuXHRleHBvcnRzLmRlbGV0ZUJ5U291cmNlID0gZnVuY3Rpb24oc291cmNlKSB7XG5cdFx0aWYgKCFzb3VyY2UpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignTWlzc2luZyBzb3VyY2UnKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIF9wZXJmb3JtUmVxdWVzdCgnL0RlbGV0ZUJ5U291cmNlJywgJ0dFVCcsIHtzb3VyY2V9KTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBEZWxldGVzIGEgbWVzc2FnZXMgYnkgdGhlIGlkZW50aWZpZXIuXG5cdCAqIFxuXHQgKiBAcGFyYW0ge1N0cmluZ30gaWRlbnRpZmllciBNZXNzYWdlIGlkZW50aWZpZXIuXG5cdCAqIFxuXHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHQgKi9cblx0ZXhwb3J0cy5kZWxldGVCeUlkZW50aWZpZXIgPSBmdW5jdGlvbihpZGVudGlmaWVyKSB7XG5cdFx0aWYgKCFpZGVudGlmaWVyKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgaWRlbnRpZmllcicpO1xuXHRcdH1cblx0XHRcblx0XHRyZXR1cm4gX3BlcmZvcm1SZXF1ZXN0KCcvRGVsZXRlQnlJZGVudGlmaWVyJywgJ0dFVCcsIHtpZGVudGlmaWVyfSk7XG5cdH07XG5cdFxuXHQvKipcblx0ICogUmVhY3RpdmF0ZXMgdGhlIG1lc3NhZ2VzLlxuXHQgKiBAcmV0dXJuIHtQcm9taXNlfVxuXHQgKiBAc3RhdGljXG5cdCAqL1xuXHRleHBvcnRzLlJlYWN0aXZhdGVzID0gZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIF9wZXJmb3JtUmVxdWVzdCgnL1JlYWN0aXZhdGVNZXNzYWdlcycsICdHRVQnKTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBTYXZlcyBhIG5ldyBtZXNzYWdlLlxuXHQgKiBcblx0ICogQHBhcmFtIHtPYmplY3R9IG1lc3NhZ2UgVGhlIG5ldyBtZXNzYWdlIHRvIHNhdmUuXG5cdCAqIFxuXHQgKiBAZXhhbXBsZVxuXHQgKiBqc2UubGlicy5pbmZvX2JveC5hZGRNZXNzYWdlKHtcbiAgICAgKiAgIHNvdXJjZTogJ2FqYXgnLFxuICAgICAqICAgaWRlbnRpZmllcjogJ2FzZGFzJyxcbiAgICAgKiAgIHN0YXR1czogJ25ldycsXG4gICAgICogICB0eXBlOiAnc3VjY2VzcycsXG4gICAgICogICB2aXNpYmlsaXR5OiAncmVtb3ZhYmxlJyxcbiAgICAgKiAgIGN1c3RvbWVySWQ6IDAsXG4gICAgICogICBoZWFkbGluZTogJ015IEhlYWRsaW5lJyxcbiAgICAgKiAgIGJ1dHRvbkxhYmVsOiAnYXNkYXMnLFxuICAgICAqICAgYnV0dG9uTGluazogJ2h0dHA6Ly9nb29nbGUuY29tJywgLy8gb3B0aW9uYWxcblx0ICpcdCBidXR0b25MaW5rOiAnY3VzdG9tZXJzLnBocCcsIC8vIG9wdGlvbmFsXG4gICAgICogICBtZXNzYWdlOiAnSGFsbG8hJyxcbiAgICAgKiB9KTtcblx0ICogXG5cdCAqIEByZXR1cm4ge1Byb21pc2V9XG5cdCAqL1xuXHRleHBvcnRzLmFkZE1lc3NhZ2UgPSBmdW5jdGlvbihtZXNzYWdlKSB7XG5cdFx0aWYgKCFtZXNzYWdlKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgbWVzc2FnZSBvYmplY3QnKTtcblx0XHR9IGVsc2UgaWYgKCFtZXNzYWdlLnNvdXJjZSkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdNaXNzaW5nIHNvdXJjZScpO1xuXHRcdH0gZWxzZSBpZiAoIW1lc3NhZ2UuaWRlbnRpZmllcikge1xuXHRcdFx0bWVzc2FnZS5pZGVudGlmaWVyID0gJ2dlbmVyYXRlZC0nICsgRGF0ZS5ub3coKTtcblx0XHR9IGVsc2UgaWYgKCFtZXNzYWdlLnN0YXR1cyB8fCBbXG5cdFx0XHRcdGV4cG9ydHMuU1RBVFVTX05FVywgZXhwb3J0cy5TVEFUVVNfUkVBRCwgZXhwb3J0cy5TVEFUVVNfSElEREVOLFxuXHRcdFx0XHRleHBvcnRzLlNUQVRVU19ERUxFVEVEXG5cdFx0XHRdLmluZGV4T2YobWVzc2FnZS5zdGF0dXMpID09PSAtMSkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdNaXNzaW5nIG9yIGludmFsaWQgc3RhdHVzJyk7XG5cdFx0fSBlbHNlIGlmICghbWVzc2FnZS50eXBlIHx8IFtcblx0XHRcdFx0ZXhwb3J0cy5UWVBFX0lORk8sIGV4cG9ydHMuVFlQRV9XQVJOSU5HLFxuXHRcdFx0XHRleHBvcnRzLlRZUEVfU1VDQ0VTU1xuXHRcdFx0XS5pbmRleE9mKG1lc3NhZ2UudHlwZSkgPT09IC0xKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ01pc3Npbmcgb3IgaW52YWxpZCB0eXBlJyk7XG5cdFx0fSBlbHNlIGlmICghbWVzc2FnZS52aXNpYmlsaXR5IHx8IFtcblx0XHRcdFx0ZXhwb3J0cy5WSVNJQklMSVRZX0FMV0FZU19PTiwgZXhwb3J0cy5WSVNJQklMSVRZX0hJREVBQkxFLFxuXHRcdFx0XHRleHBvcnRzLlZJU0lCSUxJVFlfUkVNT1ZBQkxFXG5cdFx0XHRdLmluZGV4T2YobWVzc2FnZS52aXNpYmlsaXR5KSA9PT0gLTEpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignTWlzc2luZyBvciBpbnZhbGlkIHZpc2liaWxpdHknKTtcblx0XHR9IGVsc2UgaWYgKHR5cGVvZiBtZXNzYWdlLmN1c3RvbWVySWQgPT09ICd1bmRlZmluZWQnKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgY3VzdG9tZXIgSUQnKTtcblx0XHR9IGVsc2UgaWYgKCFtZXNzYWdlLm1lc3NhZ2UpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignTWlzc2luZyBtZXNzYWdlJyk7XG5cdFx0fSBlbHNlIGlmICghbWVzc2FnZS5oZWFkbGluZSkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdNaXNzaW5nIGhlYWRsaW5lJyk7XG5cdFx0fSBlbHNlIGlmICghbWVzc2FnZS5tZXNzYWdlKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgbWVzc2FnZScpO1xuXHRcdH1cblx0XHRcblx0XHRyZXR1cm4gX3BlcmZvcm1SZXF1ZXN0KCcvQWRkTWVzc2FnZScsICdHRVQnLCBtZXNzYWdlKTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBBZGRzIGEgc3VjY2VzcyBtZXNzYWdlIHRvIHRoZSBhZG1pbiBpbmZvIGJveC5cblx0ICogXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBbbWVzc2FnZV0gTWVzc2FnZSB0byBzaG93LlxuXHQgKiBAcGFyYW0ge0Jvb2xlYW59IFtza2lwUmVmcmVzaCA9IGZhbHNlXSBSZWZyZXNoIHRoZSBhZG1pbiBpbmZvIGJveCB0byBzaG93IHRoZSBtZXNzYWdlP1xuXHQgKi9cblx0ZXhwb3J0cy5hZGRTdWNjZXNzTWVzc2FnZSA9IGZ1bmN0aW9uKG1lc3NhZ2UsIHNraXBSZWZyZXNoID0gZmFsc2UpIHtcblx0XHQvLyBBZGQgbWVzc2FnZS5cblx0XHRjb25zdCByZXF1ZXN0ID0gX3BlcmZvcm1SZXF1ZXN0KCcvQWRkU3VjY2Vzc01lc3NhZ2UnLCAnR0VUJywgbWVzc2FnZSA/IHttZXNzYWdlfSA6IHt9KTtcblx0XHRcblx0XHQvLyBPcHRpb25hbGx5IHJlZnJlc2ggdGhlIGFkbWluIGluZm8gYm94IHRvIHNob3cgdGhlIG1lc3NhZ2UuXG5cdFx0aWYgKCFza2lwUmVmcmVzaCkge1xuXHRcdFx0cmVxdWVzdC5kb25lKCgpID0+ICQoaW5mb2JveFNlbGVjdG9yKS50cmlnZ2VyKCdyZWZyZXNoOm1lc3NhZ2VzJykpO1xuXHRcdH1cblx0fTtcbn0oanNlLmxpYnMuaW5mb19ib3gpKTtcbiJdfQ==
