"use strict";

/* --------------------------------------------------------------
 form_images_validator.js 2016-08-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.form_images_validator = jse.libs.form_images_validator || {};

/**
 * Form Images Validator Library.
 *
 * Provides callback methods that can be overridden to provide custom functionality.
 *
 * @module Admin/Libs/form_images_validator
 * @exports jse.libs.form_images_validator
 */
(function (exports) {
	/**
  * Provides callback methods, that can be overridden.
  *
  * @type {Object}
  */
	exports.callbackMethods = {
		/**
   * Invoked callback method on validation errors.
   *
   * @param {jQuery.Event} event Triggered form submit event.
   * @param {HTMLElement[]} errors Array containing the input fields that failed on the validation.
   * @abstract
   */
		onValidationError: function onValidationError(event, errors) {},


		/**
   * Invoked callback method on validation success.
   *
   * @param {jQuery.Event} event Triggered form submit event.
   * @abstract
   */
		onValidationSuccess: function onValidationSuccess(event) {}
	};
})(jse.libs.form_images_validator);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvcm1faW1hZ2VzX3ZhbGlkYXRvci5qcyJdLCJuYW1lcyI6WyJqc2UiLCJsaWJzIiwiZm9ybV9pbWFnZXNfdmFsaWRhdG9yIiwiZXhwb3J0cyIsImNhbGxiYWNrTWV0aG9kcyIsIm9uVmFsaWRhdGlvbkVycm9yIiwiZXZlbnQiLCJlcnJvcnMiLCJvblZhbGlkYXRpb25TdWNjZXNzIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLElBQUlDLElBQUosQ0FBU0MscUJBQVQsR0FBaUNGLElBQUlDLElBQUosQ0FBU0MscUJBQVQsSUFBa0MsRUFBbkU7O0FBRUE7Ozs7Ozs7O0FBUUMsV0FBU0MsT0FBVCxFQUFrQjtBQUNsQjs7Ozs7QUFLQUEsU0FBUUMsZUFBUixHQUEwQjtBQUN6Qjs7Ozs7OztBQU9BQyxtQkFSeUIsNkJBUVBDLEtBUk8sRUFRQUMsTUFSQSxFQVFRLENBQ2hDLENBVHdCOzs7QUFXekI7Ozs7OztBQU1BQyxxQkFqQnlCLCtCQWlCTEYsS0FqQkssRUFpQkUsQ0FDMUI7QUFsQndCLEVBQTFCO0FBb0JBLENBMUJBLEVBMEJDTixJQUFJQyxJQUFKLENBQVNDLHFCQTFCVixDQUFEIiwiZmlsZSI6ImZvcm1faW1hZ2VzX3ZhbGlkYXRvci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZm9ybV9pbWFnZXNfdmFsaWRhdG9yLmpzIDIwMTYtMDgtMjlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UubGlicy5mb3JtX2ltYWdlc192YWxpZGF0b3IgPSBqc2UubGlicy5mb3JtX2ltYWdlc192YWxpZGF0b3IgfHwge307XG5cbi8qKlxuICogRm9ybSBJbWFnZXMgVmFsaWRhdG9yIExpYnJhcnkuXG4gKlxuICogUHJvdmlkZXMgY2FsbGJhY2sgbWV0aG9kcyB0aGF0IGNhbiBiZSBvdmVycmlkZGVuIHRvIHByb3ZpZGUgY3VzdG9tIGZ1bmN0aW9uYWxpdHkuXG4gKlxuICogQG1vZHVsZSBBZG1pbi9MaWJzL2Zvcm1faW1hZ2VzX3ZhbGlkYXRvclxuICogQGV4cG9ydHMganNlLmxpYnMuZm9ybV9pbWFnZXNfdmFsaWRhdG9yXG4gKi9cbihmdW5jdGlvbihleHBvcnRzKSB7XG5cdC8qKlxuXHQgKiBQcm92aWRlcyBjYWxsYmFjayBtZXRob2RzLCB0aGF0IGNhbiBiZSBvdmVycmlkZGVuLlxuXHQgKlxuXHQgKiBAdHlwZSB7T2JqZWN0fVxuXHQgKi9cblx0ZXhwb3J0cy5jYWxsYmFja01ldGhvZHMgPSB7XG5cdFx0LyoqXG5cdFx0ICogSW52b2tlZCBjYWxsYmFjayBtZXRob2Qgb24gdmFsaWRhdGlvbiBlcnJvcnMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnQgVHJpZ2dlcmVkIGZvcm0gc3VibWl0IGV2ZW50LlxuXHRcdCAqIEBwYXJhbSB7SFRNTEVsZW1lbnRbXX0gZXJyb3JzIEFycmF5IGNvbnRhaW5pbmcgdGhlIGlucHV0IGZpZWxkcyB0aGF0IGZhaWxlZCBvbiB0aGUgdmFsaWRhdGlvbi5cblx0XHQgKiBAYWJzdHJhY3Rcblx0XHQgKi9cblx0XHRvblZhbGlkYXRpb25FcnJvcihldmVudCwgZXJyb3JzKSB7XG5cdFx0fSxcblxuXHRcdC8qKlxuXHRcdCAqIEludm9rZWQgY2FsbGJhY2sgbWV0aG9kIG9uIHZhbGlkYXRpb24gc3VjY2Vzcy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZm9ybSBzdWJtaXQgZXZlbnQuXG5cdFx0ICogQGFic3RyYWN0XG5cdFx0ICovXG5cdFx0b25WYWxpZGF0aW9uU3VjY2VzcyhldmVudCkge1xuXHRcdH1cblx0fTtcbn0oanNlLmxpYnMuZm9ybV9pbWFnZXNfdmFsaWRhdG9yKSk7XG4iXX0=
