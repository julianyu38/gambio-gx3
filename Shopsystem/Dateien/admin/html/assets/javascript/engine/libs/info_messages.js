'use strict';

/* --------------------------------------------------------------
 info_messages.js 2016-07-27
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.info_messages = jse.libs.info_messages || {};

/**
 * ## Info Messages library
 *
 * Use this library to add messages into admin's notification system (top right corner). There are multiple 
 * types of notification entries 'error', 'info', 'warning' and 'success'. Use the respective method for 
 * each one of them. 
 * 
 * You will need to provide the full URL in order to load this library as a dependency to a module:
 * 
 * ```javascript
 * gx.controller.module(
 *   'my_custom_page',
 *
 *   [
 *      gx.source + '/libs/info_messages'
 *   ],
 *
 *   function(data) {
 *      // Module code ... 
 *   });
 *```
 *
 * @todo This library does not yet support the new admin layout pages. 
 * 
 * @module Admin/Libs/info_messages
 * @exports jse.libs.info_messages
 */
(function (exports) {

	'use strict';

	/**
  * Container element for info messages
  *
  * @type {object}
  */

	var $messagesContainer = $('.message_stack_container, .message-stack');

	/**
  * Appends a message box to the info messages container and displays it
  *
  * @param {string} message Message to be displayed.
  * @param {string} type Message type can be one of the "info", "warning", "error" & "success".
  *
  * @private
  */
	var _add = function _add(message, type) {
		var $alert = $('<div class="alert alert-' + type + '" data-gx-compatibility="close_alert_box">' + '<button type="button" class="close" data-dismuss="alert">×</button>' + message + '</div>');

		$alert.find('.close').on('click', function () {
			$(this).parent('.alert').hide();
		});

		$messagesContainer.append($alert);
		$messagesContainer.show();
	};

	/**
  * Removes all messages inside the message container.
  */
	exports.truncate = function () {
		$messagesContainer.empty();
	};

	/**
  * Adds a red error message.
  *
  * @param {string} message Message to be displayed.
  */
	exports.addError = function (message) {
		_add(message, 'danger');
	};

	/**
  * Adds a blue info message.
  *
  * @param {string} message Message to be displayed.
  */
	exports.addInfo = function (message) {
		_add(message, 'info');
	};

	/**
  * Adds a green success message.
  *
  * @param {string} message Message to be displayed.
  */
	exports.addSuccess = function (message) {
		_add(message, 'success');
	};

	/**
  * Adds a yellow warning message.
  *
  * @param {string} message Message to be displayed.
  */
	exports.addWarning = function (message) {
		_add(message, 'warning');
	};
})(jse.libs.info_messages);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZm9fbWVzc2FnZXMuanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsImluZm9fbWVzc2FnZXMiLCJleHBvcnRzIiwiJG1lc3NhZ2VzQ29udGFpbmVyIiwiJCIsIl9hZGQiLCJtZXNzYWdlIiwidHlwZSIsIiRhbGVydCIsImZpbmQiLCJvbiIsInBhcmVudCIsImhpZGUiLCJhcHBlbmQiLCJzaG93IiwidHJ1bmNhdGUiLCJlbXB0eSIsImFkZEVycm9yIiwiYWRkSW5mbyIsImFkZFN1Y2Nlc3MiLCJhZGRXYXJuaW5nIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLElBQUlDLElBQUosQ0FBU0MsYUFBVCxHQUF5QkYsSUFBSUMsSUFBSixDQUFTQyxhQUFULElBQTBCLEVBQW5EOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQkEsQ0FBQyxVQUFTQyxPQUFULEVBQWtCOztBQUVsQjs7QUFFQTs7Ozs7O0FBS0EsS0FBSUMscUJBQXFCQyxFQUFFLDBDQUFGLENBQXpCOztBQUVBOzs7Ozs7OztBQVFBLEtBQUlDLE9BQU8sU0FBUEEsSUFBTyxDQUFTQyxPQUFULEVBQWtCQyxJQUFsQixFQUF3QjtBQUNsQyxNQUFJQyxTQUFTSixFQUFFLDZCQUE2QkcsSUFBN0IsR0FBb0MsNENBQXBDLEdBQ2QscUVBRGMsR0FDMERELE9BRDFELEdBQ29FLFFBRHRFLENBQWI7O0FBR0FFLFNBQU9DLElBQVAsQ0FBWSxRQUFaLEVBQXNCQyxFQUF0QixDQUF5QixPQUF6QixFQUFrQyxZQUFXO0FBQzVDTixLQUFFLElBQUYsRUFBUU8sTUFBUixDQUFlLFFBQWYsRUFBeUJDLElBQXpCO0FBQ0EsR0FGRDs7QUFJQVQscUJBQW1CVSxNQUFuQixDQUEwQkwsTUFBMUI7QUFDQUwscUJBQW1CVyxJQUFuQjtBQUNBLEVBVkQ7O0FBWUE7OztBQUdBWixTQUFRYSxRQUFSLEdBQW1CLFlBQVc7QUFDN0JaLHFCQUFtQmEsS0FBbkI7QUFDQSxFQUZEOztBQUlBOzs7OztBQUtBZCxTQUFRZSxRQUFSLEdBQW1CLFVBQVNYLE9BQVQsRUFBa0I7QUFDcENELE9BQUtDLE9BQUwsRUFBYyxRQUFkO0FBQ0EsRUFGRDs7QUFJQTs7Ozs7QUFLQUosU0FBUWdCLE9BQVIsR0FBa0IsVUFBU1osT0FBVCxFQUFrQjtBQUNuQ0QsT0FBS0MsT0FBTCxFQUFjLE1BQWQ7QUFDQSxFQUZEOztBQUlBOzs7OztBQUtBSixTQUFRaUIsVUFBUixHQUFxQixVQUFTYixPQUFULEVBQWtCO0FBQ3RDRCxPQUFLQyxPQUFMLEVBQWMsU0FBZDtBQUNBLEVBRkQ7O0FBSUE7Ozs7O0FBS0FKLFNBQVFrQixVQUFSLEdBQXFCLFVBQVNkLE9BQVQsRUFBa0I7QUFDdENELE9BQUtDLE9BQUwsRUFBYyxTQUFkO0FBQ0EsRUFGRDtBQUlBLENBMUVELEVBMEVHUCxJQUFJQyxJQUFKLENBQVNDLGFBMUVaIiwiZmlsZSI6ImluZm9fbWVzc2FnZXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGluZm9fbWVzc2FnZXMuanMgMjAxNi0wNy0yN1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmpzZS5saWJzLmluZm9fbWVzc2FnZXMgPSBqc2UubGlicy5pbmZvX21lc3NhZ2VzIHx8IHt9O1xuXG4vKipcbiAqICMjIEluZm8gTWVzc2FnZXMgbGlicmFyeVxuICpcbiAqIFVzZSB0aGlzIGxpYnJhcnkgdG8gYWRkIG1lc3NhZ2VzIGludG8gYWRtaW4ncyBub3RpZmljYXRpb24gc3lzdGVtICh0b3AgcmlnaHQgY29ybmVyKS4gVGhlcmUgYXJlIG11bHRpcGxlIFxuICogdHlwZXMgb2Ygbm90aWZpY2F0aW9uIGVudHJpZXMgJ2Vycm9yJywgJ2luZm8nLCAnd2FybmluZycgYW5kICdzdWNjZXNzJy4gVXNlIHRoZSByZXNwZWN0aXZlIG1ldGhvZCBmb3IgXG4gKiBlYWNoIG9uZSBvZiB0aGVtLiBcbiAqIFxuICogWW91IHdpbGwgbmVlZCB0byBwcm92aWRlIHRoZSBmdWxsIFVSTCBpbiBvcmRlciB0byBsb2FkIHRoaXMgbGlicmFyeSBhcyBhIGRlcGVuZGVuY3kgdG8gYSBtb2R1bGU6XG4gKiBcbiAqIGBgYGphdmFzY3JpcHRcbiAqIGd4LmNvbnRyb2xsZXIubW9kdWxlKFxuICogICAnbXlfY3VzdG9tX3BhZ2UnLFxuICpcbiAqICAgW1xuICogICAgICBneC5zb3VyY2UgKyAnL2xpYnMvaW5mb19tZXNzYWdlcydcbiAqICAgXSxcbiAqXG4gKiAgIGZ1bmN0aW9uKGRhdGEpIHtcbiAqICAgICAgLy8gTW9kdWxlIGNvZGUgLi4uIFxuICogICB9KTtcbiAqYGBgXG4gKlxuICogQHRvZG8gVGhpcyBsaWJyYXJ5IGRvZXMgbm90IHlldCBzdXBwb3J0IHRoZSBuZXcgYWRtaW4gbGF5b3V0IHBhZ2VzLiBcbiAqIFxuICogQG1vZHVsZSBBZG1pbi9MaWJzL2luZm9fbWVzc2FnZXNcbiAqIEBleHBvcnRzIGpzZS5saWJzLmluZm9fbWVzc2FnZXNcbiAqL1xuKGZ1bmN0aW9uKGV4cG9ydHMpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8qKlxuXHQgKiBDb250YWluZXIgZWxlbWVudCBmb3IgaW5mbyBtZXNzYWdlc1xuXHQgKlxuXHQgKiBAdHlwZSB7b2JqZWN0fVxuXHQgKi9cblx0dmFyICRtZXNzYWdlc0NvbnRhaW5lciA9ICQoJy5tZXNzYWdlX3N0YWNrX2NvbnRhaW5lciwgLm1lc3NhZ2Utc3RhY2snKTtcblx0XG5cdC8qKlxuXHQgKiBBcHBlbmRzIGEgbWVzc2FnZSBib3ggdG8gdGhlIGluZm8gbWVzc2FnZXMgY29udGFpbmVyIGFuZCBkaXNwbGF5cyBpdFxuXHQgKlxuXHQgKiBAcGFyYW0ge3N0cmluZ30gbWVzc2FnZSBNZXNzYWdlIHRvIGJlIGRpc3BsYXllZC5cblx0ICogQHBhcmFtIHtzdHJpbmd9IHR5cGUgTWVzc2FnZSB0eXBlIGNhbiBiZSBvbmUgb2YgdGhlIFwiaW5mb1wiLCBcIndhcm5pbmdcIiwgXCJlcnJvclwiICYgXCJzdWNjZXNzXCIuXG5cdCAqXG5cdCAqIEBwcml2YXRlXG5cdCAqL1xuXHR2YXIgX2FkZCA9IGZ1bmN0aW9uKG1lc3NhZ2UsIHR5cGUpIHtcblx0XHR2YXIgJGFsZXJ0ID0gJCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LScgKyB0eXBlICsgJ1wiIGRhdGEtZ3gtY29tcGF0aWJpbGl0eT1cImNsb3NlX2FsZXJ0X2JveFwiPicgK1xuXHRcdFx0JzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIiBkYXRhLWRpc211c3M9XCJhbGVydFwiPsOXPC9idXR0b24+JyArIG1lc3NhZ2UgKyAnPC9kaXY+Jyk7XG5cdFx0XG5cdFx0JGFsZXJ0LmZpbmQoJy5jbG9zZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuXHRcdFx0JCh0aGlzKS5wYXJlbnQoJy5hbGVydCcpLmhpZGUoKTtcblx0XHR9KTtcblx0XHRcblx0XHQkbWVzc2FnZXNDb250YWluZXIuYXBwZW5kKCRhbGVydCk7XG5cdFx0JG1lc3NhZ2VzQ29udGFpbmVyLnNob3coKTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBSZW1vdmVzIGFsbCBtZXNzYWdlcyBpbnNpZGUgdGhlIG1lc3NhZ2UgY29udGFpbmVyLlxuXHQgKi9cblx0ZXhwb3J0cy50cnVuY2F0ZSA9IGZ1bmN0aW9uKCkge1xuXHRcdCRtZXNzYWdlc0NvbnRhaW5lci5lbXB0eSgpO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIEFkZHMgYSByZWQgZXJyb3IgbWVzc2FnZS5cblx0ICpcblx0ICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2UgTWVzc2FnZSB0byBiZSBkaXNwbGF5ZWQuXG5cdCAqL1xuXHRleHBvcnRzLmFkZEVycm9yID0gZnVuY3Rpb24obWVzc2FnZSkge1xuXHRcdF9hZGQobWVzc2FnZSwgJ2RhbmdlcicpO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIEFkZHMgYSBibHVlIGluZm8gbWVzc2FnZS5cblx0ICpcblx0ICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2UgTWVzc2FnZSB0byBiZSBkaXNwbGF5ZWQuXG5cdCAqL1xuXHRleHBvcnRzLmFkZEluZm8gPSBmdW5jdGlvbihtZXNzYWdlKSB7XG5cdFx0X2FkZChtZXNzYWdlLCAnaW5mbycpO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIEFkZHMgYSBncmVlbiBzdWNjZXNzIG1lc3NhZ2UuXG5cdCAqXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBtZXNzYWdlIE1lc3NhZ2UgdG8gYmUgZGlzcGxheWVkLlxuXHQgKi9cblx0ZXhwb3J0cy5hZGRTdWNjZXNzID0gZnVuY3Rpb24obWVzc2FnZSkge1xuXHRcdF9hZGQobWVzc2FnZSwgJ3N1Y2Nlc3MnKTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBBZGRzIGEgeWVsbG93IHdhcm5pbmcgbWVzc2FnZS5cblx0ICpcblx0ICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2UgTWVzc2FnZSB0byBiZSBkaXNwbGF5ZWQuXG5cdCAqL1xuXHRleHBvcnRzLmFkZFdhcm5pbmcgPSBmdW5jdGlvbihtZXNzYWdlKSB7XG5cdFx0X2FkZChtZXNzYWdlLCAnd2FybmluZycpO1xuXHR9O1xuXHRcbn0pKGpzZS5saWJzLmluZm9fbWVzc2FnZXMpO1xuIl19
