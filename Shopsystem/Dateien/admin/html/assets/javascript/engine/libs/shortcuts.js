'use strict';

/* --------------------------------------------------------------
 shortcuts.js 2016-09-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.shortcuts = jse.libs.shortcuts || {};

/**
 * ## Shortcuts library.
 *
 * This module holds the registered shortcuts and provides predefined constants for event key codes.
 * Additionally, you are allowed to register a custom shortcut by using the `registerShortcut()` function.
 *
 * Example on how to register a custom shortcut:
 *  jse.libs.shortcuts.registerShortcut('myShortcut', [17, 16, 70], _doThis);
 *
 * @module Admin/Libs/shortcuts
 * @exports jse.libs.shortcuts
 */
(function (exports) {
	// Event key codes map.
	exports.KEY_CODES = {
		// Left control key.
		CTRL_L: 17,

		// Left shift key.
		SHIFT_L: 16,

		// Above number key 1.
		NUM_1: 49,
		NUM_2: 50,
		NUM_3: 51,
		NUM_4: 52,
		NUM_5: 53,
		NUM_6: 54,
		NUM_7: 55,
		NUM_8: 56,
		NUM_9: 57
	};

	// List of registered shortcuts.
	// Structure:
	// [
	//      {
	//          name: 'openSearch',
	//          combination: [17, 16, 70],
	//          callback: () => 'Hello world!'
	//      }
	// ]
	exports.registeredShortcuts = [];

	/**
  * Registers a shortcut by adding the key combination and the respective callback function to the register object.
  *
  * @param {String} name Name of the key combination.
  * @param {Number[]} combination Array of event key numbers that represent a key combination.
  * @param {Function} callback Function to be invoked on key combination match.
  */
	exports.registerShortcut = function (name, combination, callback) {
		// Check combination name.
		if (!name || typeof name !== 'string') {
			throw new Error('Key combination name parameter is missing or invalid');
		}

		// Check combination.
		if (!combination || !Array.isArray(combination)) {
			throw new Error('Key combination parameter is missing or invalid');
		}

		// Check callback function.
		if (!callback || typeof callback !== 'function') {
			throw new Error('Callback function parameter is missing or invalid');
		}

		// Register shortcut.
		exports.registeredShortcuts.push({ name: name, combination: combination, callback: callback });
	};
})(jse.libs.shortcuts);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNob3J0Y3V0cy5qcyJdLCJuYW1lcyI6WyJqc2UiLCJsaWJzIiwic2hvcnRjdXRzIiwiZXhwb3J0cyIsIktFWV9DT0RFUyIsIkNUUkxfTCIsIlNISUZUX0wiLCJOVU1fMSIsIk5VTV8yIiwiTlVNXzMiLCJOVU1fNCIsIk5VTV81IiwiTlVNXzYiLCJOVU1fNyIsIk5VTV84IiwiTlVNXzkiLCJyZWdpc3RlcmVkU2hvcnRjdXRzIiwicmVnaXN0ZXJTaG9ydGN1dCIsIm5hbWUiLCJjb21iaW5hdGlvbiIsImNhbGxiYWNrIiwiRXJyb3IiLCJBcnJheSIsImlzQXJyYXkiLCJwdXNoIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLElBQUlDLElBQUosQ0FBU0MsU0FBVCxHQUFxQkYsSUFBSUMsSUFBSixDQUFTQyxTQUFULElBQXNCLEVBQTNDOztBQUVBOzs7Ozs7Ozs7Ozs7QUFZQyxXQUFTQyxPQUFULEVBQWtCO0FBQ2xCO0FBQ0FBLFNBQVFDLFNBQVIsR0FBb0I7QUFDbkI7QUFDQUMsVUFBUSxFQUZXOztBQUluQjtBQUNBQyxXQUFTLEVBTFU7O0FBT25CO0FBQ0FDLFNBQU8sRUFSWTtBQVNuQkMsU0FBTyxFQVRZO0FBVW5CQyxTQUFPLEVBVlk7QUFXbkJDLFNBQU8sRUFYWTtBQVluQkMsU0FBTyxFQVpZO0FBYW5CQyxTQUFPLEVBYlk7QUFjbkJDLFNBQU8sRUFkWTtBQWVuQkMsU0FBTyxFQWZZO0FBZ0JuQkMsU0FBTztBQWhCWSxFQUFwQjs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FaLFNBQVFhLG1CQUFSLEdBQThCLEVBQTlCOztBQUVBOzs7Ozs7O0FBT0FiLFNBQVFjLGdCQUFSLEdBQTJCLFVBQUNDLElBQUQsRUFBT0MsV0FBUCxFQUFvQkMsUUFBcEIsRUFBaUM7QUFDM0Q7QUFDQSxNQUFJLENBQUNGLElBQUQsSUFBUyxPQUFPQSxJQUFQLEtBQWdCLFFBQTdCLEVBQXVDO0FBQ3RDLFNBQU0sSUFBSUcsS0FBSixDQUFVLHNEQUFWLENBQU47QUFDQTs7QUFFRDtBQUNBLE1BQUksQ0FBQ0YsV0FBRCxJQUFnQixDQUFDRyxNQUFNQyxPQUFOLENBQWNKLFdBQWQsQ0FBckIsRUFBaUQ7QUFDaEQsU0FBTSxJQUFJRSxLQUFKLENBQVUsaURBQVYsQ0FBTjtBQUNBOztBQUVEO0FBQ0EsTUFBSSxDQUFDRCxRQUFELElBQWEsT0FBT0EsUUFBUCxLQUFvQixVQUFyQyxFQUFpRDtBQUNoRCxTQUFNLElBQUlDLEtBQUosQ0FBVSxtREFBVixDQUFOO0FBQ0E7O0FBRUQ7QUFDQWxCLFVBQVFhLG1CQUFSLENBQTRCUSxJQUE1QixDQUFpQyxFQUFDTixVQUFELEVBQU9DLHdCQUFQLEVBQW9CQyxrQkFBcEIsRUFBakM7QUFDQSxFQWxCRDtBQW1CQSxDQTFEQSxFQTBEQ3BCLElBQUlDLElBQUosQ0FBU0MsU0ExRFYsQ0FBRCIsImZpbGUiOiJzaG9ydGN1dHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNob3J0Y3V0cy5qcyAyMDE2LTA5LTEzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuanNlLmxpYnMuc2hvcnRjdXRzID0ganNlLmxpYnMuc2hvcnRjdXRzIHx8IHt9O1xuXG4vKipcbiAqICMjIFNob3J0Y3V0cyBsaWJyYXJ5LlxuICpcbiAqIFRoaXMgbW9kdWxlIGhvbGRzIHRoZSByZWdpc3RlcmVkIHNob3J0Y3V0cyBhbmQgcHJvdmlkZXMgcHJlZGVmaW5lZCBjb25zdGFudHMgZm9yIGV2ZW50IGtleSBjb2Rlcy5cbiAqIEFkZGl0aW9uYWxseSwgeW91IGFyZSBhbGxvd2VkIHRvIHJlZ2lzdGVyIGEgY3VzdG9tIHNob3J0Y3V0IGJ5IHVzaW5nIHRoZSBgcmVnaXN0ZXJTaG9ydGN1dCgpYCBmdW5jdGlvbi5cbiAqXG4gKiBFeGFtcGxlIG9uIGhvdyB0byByZWdpc3RlciBhIGN1c3RvbSBzaG9ydGN1dDpcbiAqICBqc2UubGlicy5zaG9ydGN1dHMucmVnaXN0ZXJTaG9ydGN1dCgnbXlTaG9ydGN1dCcsIFsxNywgMTYsIDcwXSwgX2RvVGhpcyk7XG4gKlxuICogQG1vZHVsZSBBZG1pbi9MaWJzL3Nob3J0Y3V0c1xuICogQGV4cG9ydHMganNlLmxpYnMuc2hvcnRjdXRzXG4gKi9cbihmdW5jdGlvbihleHBvcnRzKSB7XG5cdC8vIEV2ZW50IGtleSBjb2RlcyBtYXAuXG5cdGV4cG9ydHMuS0VZX0NPREVTID0ge1xuXHRcdC8vIExlZnQgY29udHJvbCBrZXkuXG5cdFx0Q1RSTF9MOiAxNyxcblxuXHRcdC8vIExlZnQgc2hpZnQga2V5LlxuXHRcdFNISUZUX0w6IDE2LFxuXG5cdFx0Ly8gQWJvdmUgbnVtYmVyIGtleSAxLlxuXHRcdE5VTV8xOiA0OSxcblx0XHROVU1fMjogNTAsXG5cdFx0TlVNXzM6IDUxLFxuXHRcdE5VTV80OiA1Mixcblx0XHROVU1fNTogNTMsXG5cdFx0TlVNXzY6IDU0LFxuXHRcdE5VTV83OiA1NSxcblx0XHROVU1fODogNTYsXG5cdFx0TlVNXzk6IDU3XG5cdH07XG5cblx0Ly8gTGlzdCBvZiByZWdpc3RlcmVkIHNob3J0Y3V0cy5cblx0Ly8gU3RydWN0dXJlOlxuXHQvLyBbXG5cdC8vICAgICAge1xuXHQvLyAgICAgICAgICBuYW1lOiAnb3BlblNlYXJjaCcsXG5cdC8vICAgICAgICAgIGNvbWJpbmF0aW9uOiBbMTcsIDE2LCA3MF0sXG5cdC8vICAgICAgICAgIGNhbGxiYWNrOiAoKSA9PiAnSGVsbG8gd29ybGQhJ1xuXHQvLyAgICAgIH1cblx0Ly8gXVxuXHRleHBvcnRzLnJlZ2lzdGVyZWRTaG9ydGN1dHMgPSBbXTtcblxuXHQvKipcblx0ICogUmVnaXN0ZXJzIGEgc2hvcnRjdXQgYnkgYWRkaW5nIHRoZSBrZXkgY29tYmluYXRpb24gYW5kIHRoZSByZXNwZWN0aXZlIGNhbGxiYWNrIGZ1bmN0aW9uIHRvIHRoZSByZWdpc3RlciBvYmplY3QuXG5cdCAqXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIE5hbWUgb2YgdGhlIGtleSBjb21iaW5hdGlvbi5cblx0ICogQHBhcmFtIHtOdW1iZXJbXX0gY29tYmluYXRpb24gQXJyYXkgb2YgZXZlbnQga2V5IG51bWJlcnMgdGhhdCByZXByZXNlbnQgYSBrZXkgY29tYmluYXRpb24uXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrIEZ1bmN0aW9uIHRvIGJlIGludm9rZWQgb24ga2V5IGNvbWJpbmF0aW9uIG1hdGNoLlxuXHQgKi9cblx0ZXhwb3J0cy5yZWdpc3RlclNob3J0Y3V0ID0gKG5hbWUsIGNvbWJpbmF0aW9uLCBjYWxsYmFjaykgPT4ge1xuXHRcdC8vIENoZWNrIGNvbWJpbmF0aW9uIG5hbWUuXG5cdFx0aWYgKCFuYW1lIHx8IHR5cGVvZiBuYW1lICE9PSAnc3RyaW5nJykge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdLZXkgY29tYmluYXRpb24gbmFtZSBwYXJhbWV0ZXIgaXMgbWlzc2luZyBvciBpbnZhbGlkJyk7XG5cdFx0fVxuXG5cdFx0Ly8gQ2hlY2sgY29tYmluYXRpb24uXG5cdFx0aWYgKCFjb21iaW5hdGlvbiB8fCAhQXJyYXkuaXNBcnJheShjb21iaW5hdGlvbikpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignS2V5IGNvbWJpbmF0aW9uIHBhcmFtZXRlciBpcyBtaXNzaW5nIG9yIGludmFsaWQnKTtcblx0XHR9XG5cblx0XHQvLyBDaGVjayBjYWxsYmFjayBmdW5jdGlvbi5cblx0XHRpZiAoIWNhbGxiYWNrIHx8IHR5cGVvZiBjYWxsYmFjayAhPT0gJ2Z1bmN0aW9uJykge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdDYWxsYmFjayBmdW5jdGlvbiBwYXJhbWV0ZXIgaXMgbWlzc2luZyBvciBpbnZhbGlkJyk7XG5cdFx0fVxuXG5cdFx0Ly8gUmVnaXN0ZXIgc2hvcnRjdXQuXG5cdFx0ZXhwb3J0cy5yZWdpc3RlcmVkU2hvcnRjdXRzLnB1c2goe25hbWUsIGNvbWJpbmF0aW9uLCBjYWxsYmFja30pO1xuXHR9XG59KGpzZS5saWJzLnNob3J0Y3V0cykpO1xuIl19
