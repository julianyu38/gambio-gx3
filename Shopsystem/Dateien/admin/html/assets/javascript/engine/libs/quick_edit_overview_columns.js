'use strict';

/* --------------------------------------------------------------
 quick_edit_overview_columns.js 2016-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.quick_edit_overview_columns = jse.libs.quick_edit_overview_columns || {};

(function (exports) {

	'use strict';

	exports.checkbox = exports.checkbox || {
		data: null,
		minWidth: '50px',
		widthFactor: 0.01,
		orderable: false,
		searchable: false,
		render: function render() {
			return '<input type="checkbox" class="overview-row-selection" />';
		}
	};

	exports.id = exports.id || {
		data: 'id',
		minWidth: '65px',
		widthFactor: 0.6
	};

	exports.category = exports.category || {
		data: 'category',
		minWidth: '150px',
		widthFactor: 1.5
	};

	exports.name = exports.name || {
		data: 'name',
		minWidth: '190px',
		widthFactor: 1.5,
		className: 'editable'
	};

	exports.model = exports.model || {
		data: 'model',
		minWidth: '90px',
		widthFactor: 1.2,
		className: 'editable'
	};

	exports.manufacturer = exports.manufacturer || {
		data: 'manufacturer',
		minWidth: '150px',
		widthFactor: 1.5,
		className: 'editable manufacturer'
	};

	exports.quantity = exports.quantity || {
		data: 'quantity',
		minWidth: '75px',
		widthFactor: 1,
		className: 'numeric editable'
	};

	exports.price = exports.price || {
		data: 'price',
		minWidth: '85px',
		widthFactor: 1.2,
		className: 'numeric editable product-price'
	};

	exports.discount = exports.discount || {
		data: 'discount',
		minWidth: '75px',
		widthFactor: 1,
		className: 'numeric editable'
	};

	exports.specialPrice = exports.specialPrice || {
		data: 'specialPrice',
		minWidth: '85px',
		widthFactor: 1.2,
		className: 'numeric editable tooltip-products-special-price'
	};

	exports.tax = exports.tax || {
		data: 'tax',
		minWidth: '85px',
		widthFactor: 1.2,
		render: function render(data, type, full, meta) {
			var html = '';
			var options = full.DT_RowData.option.tax;

			options.forEach(function (option) {
				html += '<option value="' + option.id + '" ' + (full.taxClassId == option.id ? 'selected' : '') + '>\n\t\t\t\t\t\t\t' + option.value + '\n\t\t\t\t\t\t</option>';
			});

			return '<select class="form-control select-tax">' + html + '</select>';
		}
	};

	exports.shippingStatusName = exports.shippingStatusName || {
		data: 'shippingStatusName',
		minWidth: '100px',
		widthFactor: 1.2,
		render: function render(data, type, full, meta) {
			var html = '';
			var options = full.DT_RowData.option.shipment;

			options.forEach(function (option) {
				html += '<option value="' + option.id + '" ' + (full.shippingTimeId == option.id ? 'selected' : '') + '>\n\t\t\t\t\t\t\t' + option.value + '\n\t\t\t\t\t\t</option>';
			});

			return '<select class="form-control select-shipping-time">' + html + '</select>';
		}
	};

	exports.weight = exports.weight || {
		data: 'weight',
		minWidth: '75px',
		widthFactor: 1.2,
		className: 'numeric editable'
	};

	exports.shippingCosts = exports.shippingCosts || {
		data: 'shippingCosts',
		minWidth: '85px',
		widthFactor: 1.2,
		className: 'numeric editable'
	};

	exports.status = exports.status || {
		data: 'status',
		minWidth: '85px',
		widthFactor: 1.2,
		className: 'status',
		searchable: false,
		render: function render(data) {
			return '<input type="checkbox" ' + (data ? "checked" : "") + ' class="convert-to-switcher" />';
		}
	};

	exports.actions = exports.actions || {
		data: null,
		minWidth: '450px',
		widthFactor: 5.2,
		className: 'actions',
		orderable: false,
		searchable: false,
		render: function render(data, type, full, meta) {
			return '\t\t\t\t\t\n\t\t\t\t\t<div class="pull-left"></div>\n\t\t\t\t\t<div class="pull-right action-list visible-on-hover">\n\t\t\t\t\t\t<i class="fa fa-pencil row-edit"></i>\n\t\t\t\t\t\t<i class="fa fa-eye show-product"></i>\n\t\t\t\t\t\n\t\t\t\t\t\t<div class="btn-group dropdown">\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default"></button>\n\t\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\t\tclass="btn btn-default dropdown-toggle"\n\t\t\t\t\t\t\t\t\tdata-toggle="dropdown"\n\t\t\t\t\t\t\t\t\taria-haspopup="true"\n\t\t\t\t\t\t\t\t\taria-expanded="false">\n\t\t\t\t\t\t\t\t<span class="caret"></span>\n\t\t\t\t\t\t\t\t<span class="sr-only">Toggle Dropdown</span>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t<ul class="dropdown-menu dropdown-menu-right"></ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t';
		}
	};
})(jse.libs.quick_edit_overview_columns);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrX2VkaXRfb3ZlcnZpZXdfY29sdW1ucy5qcyJdLCJuYW1lcyI6WyJqc2UiLCJsaWJzIiwicXVpY2tfZWRpdF9vdmVydmlld19jb2x1bW5zIiwiZXhwb3J0cyIsImNoZWNrYm94IiwiZGF0YSIsIm1pbldpZHRoIiwid2lkdGhGYWN0b3IiLCJvcmRlcmFibGUiLCJzZWFyY2hhYmxlIiwicmVuZGVyIiwiaWQiLCJjYXRlZ29yeSIsIm5hbWUiLCJjbGFzc05hbWUiLCJtb2RlbCIsIm1hbnVmYWN0dXJlciIsInF1YW50aXR5IiwicHJpY2UiLCJkaXNjb3VudCIsInNwZWNpYWxQcmljZSIsInRheCIsInR5cGUiLCJmdWxsIiwibWV0YSIsImh0bWwiLCJvcHRpb25zIiwiRFRfUm93RGF0YSIsIm9wdGlvbiIsImZvckVhY2giLCJ0YXhDbGFzc0lkIiwidmFsdWUiLCJzaGlwcGluZ1N0YXR1c05hbWUiLCJzaGlwbWVudCIsInNoaXBwaW5nVGltZUlkIiwid2VpZ2h0Iiwic2hpcHBpbmdDb3N0cyIsInN0YXR1cyIsImFjdGlvbnMiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQywyQkFBVCxHQUF1Q0YsSUFBSUMsSUFBSixDQUFTQywyQkFBVCxJQUF3QyxFQUEvRTs7QUFFQSxDQUFDLFVBQVNDLE9BQVQsRUFBa0I7O0FBRWxCOztBQUVBQSxTQUFRQyxRQUFSLEdBQW1CRCxRQUFRQyxRQUFSLElBQW9CO0FBQ3JDQyxRQUFNLElBRCtCO0FBRXJDQyxZQUFVLE1BRjJCO0FBR3JDQyxlQUFhLElBSHdCO0FBSXJDQyxhQUFXLEtBSjBCO0FBS3JDQyxjQUFZLEtBTHlCO0FBTXJDQyxRQU5xQyxvQkFNNUI7QUFDUjtBQUNBO0FBUm9DLEVBQXZDOztBQVdBUCxTQUFRUSxFQUFSLEdBQWFSLFFBQVFRLEVBQVIsSUFBYztBQUN6Qk4sUUFBTSxJQURtQjtBQUV6QkMsWUFBVSxNQUZlO0FBR3pCQyxlQUFhO0FBSFksRUFBM0I7O0FBTUFKLFNBQVFTLFFBQVIsR0FBbUJULFFBQVFTLFFBQVIsSUFBb0I7QUFDckNQLFFBQU0sVUFEK0I7QUFFckNDLFlBQVUsT0FGMkI7QUFHckNDLGVBQWE7QUFId0IsRUFBdkM7O0FBTUFKLFNBQVFVLElBQVIsR0FBZVYsUUFBUVUsSUFBUixJQUFnQjtBQUM3QlIsUUFBTSxNQUR1QjtBQUU3QkMsWUFBVSxPQUZtQjtBQUc3QkMsZUFBYSxHQUhnQjtBQUk3Qk8sYUFBVztBQUprQixFQUEvQjs7QUFPQVgsU0FBUVksS0FBUixHQUFnQlosUUFBUVksS0FBUixJQUFpQjtBQUMvQlYsUUFBTSxPQUR5QjtBQUUvQkMsWUFBVSxNQUZxQjtBQUcvQkMsZUFBYSxHQUhrQjtBQUkvQk8sYUFBVztBQUpvQixFQUFqQzs7QUFPQVgsU0FBUWEsWUFBUixHQUF1QmIsUUFBUWEsWUFBUixJQUF3QjtBQUM3Q1gsUUFBTSxjQUR1QztBQUU3Q0MsWUFBVSxPQUZtQztBQUc3Q0MsZUFBYSxHQUhnQztBQUk3Q08sYUFBVztBQUprQyxFQUEvQzs7QUFPQVgsU0FBUWMsUUFBUixHQUFtQmQsUUFBUWMsUUFBUixJQUFvQjtBQUNyQ1osUUFBTSxVQUQrQjtBQUVyQ0MsWUFBVSxNQUYyQjtBQUdyQ0MsZUFBYSxDQUh3QjtBQUlyQ08sYUFBVztBQUowQixFQUF2Qzs7QUFPQVgsU0FBUWUsS0FBUixHQUFnQmYsUUFBUWUsS0FBUixJQUFpQjtBQUMvQmIsUUFBTSxPQUR5QjtBQUUvQkMsWUFBVSxNQUZxQjtBQUcvQkMsZUFBYSxHQUhrQjtBQUkvQk8sYUFBVztBQUpvQixFQUFqQzs7QUFPQVgsU0FBUWdCLFFBQVIsR0FBbUJoQixRQUFRZ0IsUUFBUixJQUFvQjtBQUNyQ2QsUUFBTSxVQUQrQjtBQUVyQ0MsWUFBVSxNQUYyQjtBQUdyQ0MsZUFBYSxDQUh3QjtBQUlyQ08sYUFBVztBQUowQixFQUF2Qzs7QUFPQVgsU0FBUWlCLFlBQVIsR0FBdUJqQixRQUFRaUIsWUFBUixJQUF3QjtBQUM3Q2YsUUFBTSxjQUR1QztBQUU3Q0MsWUFBVSxNQUZtQztBQUc3Q0MsZUFBYSxHQUhnQztBQUk3Q08sYUFBVztBQUprQyxFQUEvQzs7QUFPQVgsU0FBUWtCLEdBQVIsR0FBY2xCLFFBQVFrQixHQUFSLElBQWU7QUFDM0JoQixRQUFNLEtBRHFCO0FBRTNCQyxZQUFVLE1BRmlCO0FBRzNCQyxlQUFhLEdBSGM7QUFJM0JHLFFBSjJCLGtCQUlwQkwsSUFKb0IsRUFJZGlCLElBSmMsRUFJUkMsSUFKUSxFQUlGQyxJQUpFLEVBSUc7QUFDN0IsT0FBSUMsT0FBTyxFQUFYO0FBQ0EsT0FBTUMsVUFBVUgsS0FBS0ksVUFBTCxDQUFnQkMsTUFBaEIsQ0FBdUJQLEdBQXZDOztBQUVBSyxXQUFRRyxPQUFSLENBQWdCLGtCQUFVO0FBQ3pCSixnQ0FBMEJHLE9BQU9qQixFQUFqQyxXQUF3Q1ksS0FBS08sVUFBTCxJQUFtQkYsT0FBT2pCLEVBQTFCLEdBQStCLFVBQS9CLEdBQTRDLEVBQXBGLDBCQUNJaUIsT0FBT0csS0FEWDtBQUdBLElBSkQ7O0FBTUEsdURBQWtETixJQUFsRDtBQUNBO0FBZjBCLEVBQTdCOztBQWtCQXRCLFNBQVE2QixrQkFBUixHQUE2QjdCLFFBQVE2QixrQkFBUixJQUE4QjtBQUN6RDNCLFFBQU0sb0JBRG1EO0FBRXpEQyxZQUFVLE9BRitDO0FBR3pEQyxlQUFhLEdBSDRDO0FBSXpERyxRQUp5RCxrQkFJbERMLElBSmtELEVBSTVDaUIsSUFKNEMsRUFJdENDLElBSnNDLEVBSWhDQyxJQUpnQyxFQUkzQjtBQUM3QixPQUFJQyxPQUFPLEVBQVg7QUFDQSxPQUFNQyxVQUFVSCxLQUFLSSxVQUFMLENBQWdCQyxNQUFoQixDQUF1QkssUUFBdkM7O0FBRUFQLFdBQVFHLE9BQVIsQ0FBZ0Isa0JBQVU7QUFDekJKLGdDQUEwQkcsT0FBT2pCLEVBQWpDLFdBQXdDWSxLQUFLVyxjQUFMLElBQXVCTixPQUFPakIsRUFBOUIsR0FBbUMsVUFBbkMsR0FBZ0QsRUFBeEYsMEJBQ0lpQixPQUFPRyxLQURYO0FBR0EsSUFKRDs7QUFNQSxpRUFBNEROLElBQTVEO0FBQ0E7QUFmd0QsRUFBM0Q7O0FBa0JBdEIsU0FBUWdDLE1BQVIsR0FBaUJoQyxRQUFRZ0MsTUFBUixJQUFrQjtBQUNqQzlCLFFBQU0sUUFEMkI7QUFFakNDLFlBQVUsTUFGdUI7QUFHakNDLGVBQWEsR0FIb0I7QUFJakNPLGFBQVc7QUFKc0IsRUFBbkM7O0FBT0FYLFNBQVFpQyxhQUFSLEdBQXdCakMsUUFBUWlDLGFBQVIsSUFBeUI7QUFDL0MvQixRQUFNLGVBRHlDO0FBRS9DQyxZQUFVLE1BRnFDO0FBRy9DQyxlQUFhLEdBSGtDO0FBSS9DTyxhQUFXO0FBSm9DLEVBQWpEOztBQU9BWCxTQUFRa0MsTUFBUixHQUFpQmxDLFFBQVFrQyxNQUFSLElBQWtCO0FBQ2pDaEMsUUFBTSxRQUQyQjtBQUVqQ0MsWUFBVSxNQUZ1QjtBQUdqQ0MsZUFBYSxHQUhvQjtBQUlqQ08sYUFBVyxRQUpzQjtBQUtqQ0wsY0FBWSxLQUxxQjtBQU1qQ0MsUUFOaUMsa0JBTTFCTCxJQU4wQixFQU1wQjtBQUNaLHVDQUFrQ0EsT0FBTyxTQUFQLEdBQW1CLEVBQXJEO0FBQ0E7QUFSZ0MsRUFBbkM7O0FBV0FGLFNBQVFtQyxPQUFSLEdBQWtCbkMsUUFBUW1DLE9BQVIsSUFBbUI7QUFDbkNqQyxRQUFNLElBRDZCO0FBRW5DQyxZQUFVLE9BRnlCO0FBR25DQyxlQUFhLEdBSHNCO0FBSW5DTyxhQUFXLFNBSndCO0FBS25DTixhQUFXLEtBTHdCO0FBTW5DQyxjQUFZLEtBTnVCO0FBT25DQyxRQVBtQyxrQkFPNUJMLElBUDRCLEVBT3RCaUIsSUFQc0IsRUFPaEJDLElBUGdCLEVBT1ZDLElBUFUsRUFPSjtBQUM5QjtBQXFCQTtBQTdCa0MsRUFBckM7QUErQkEsQ0F4S0QsRUF3S0d4QixJQUFJQyxJQUFKLENBQVNDLDJCQXhLWiIsImZpbGUiOiJxdWlja19lZGl0X292ZXJ2aWV3X2NvbHVtbnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHF1aWNrX2VkaXRfb3ZlcnZpZXdfY29sdW1ucy5qcyAyMDE2LTA5LTI5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuanNlLmxpYnMucXVpY2tfZWRpdF9vdmVydmlld19jb2x1bW5zID0ganNlLmxpYnMucXVpY2tfZWRpdF9vdmVydmlld19jb2x1bW5zIHx8IHt9O1xuXG4oZnVuY3Rpb24oZXhwb3J0cykge1xuXHRcblx0J3VzZSBzdHJpY3QnO1xuXHRcblx0ZXhwb3J0cy5jaGVja2JveCA9IGV4cG9ydHMuY2hlY2tib3ggfHwge1xuXHRcdFx0ZGF0YTogbnVsbCxcblx0XHRcdG1pbldpZHRoOiAnNTBweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMC4wMSxcblx0XHRcdG9yZGVyYWJsZTogZmFsc2UsXG5cdFx0XHRzZWFyY2hhYmxlOiBmYWxzZSxcblx0XHRcdHJlbmRlcigpIHtcblx0XHRcdFx0cmV0dXJuIGA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgY2xhc3M9XCJvdmVydmlldy1yb3ctc2VsZWN0aW9uXCIgLz5gXG5cdFx0XHR9XG5cdFx0fTtcblx0XG5cdGV4cG9ydHMuaWQgPSBleHBvcnRzLmlkIHx8IHtcblx0XHRcdGRhdGE6ICdpZCcsXG5cdFx0XHRtaW5XaWR0aDogJzY1cHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDAuNixcblx0XHR9O1xuXHRcblx0ZXhwb3J0cy5jYXRlZ29yeSA9IGV4cG9ydHMuY2F0ZWdvcnkgfHwge1xuXHRcdFx0ZGF0YTogJ2NhdGVnb3J5Jyxcblx0XHRcdG1pbldpZHRoOiAnMTUwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEuNSxcblx0XHR9O1xuXHRcdFxuXHRleHBvcnRzLm5hbWUgPSBleHBvcnRzLm5hbWUgfHwge1xuXHRcdFx0ZGF0YTogJ25hbWUnLFxuXHRcdFx0bWluV2lkdGg6ICcxOTBweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMS41LFxuXHRcdFx0Y2xhc3NOYW1lOiAnZWRpdGFibGUnXG5cdFx0fTtcblx0XG5cdGV4cG9ydHMubW9kZWwgPSBleHBvcnRzLm1vZGVsIHx8IHtcblx0XHRcdGRhdGE6ICdtb2RlbCcsXG5cdFx0XHRtaW5XaWR0aDogJzkwcHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEuMixcblx0XHRcdGNsYXNzTmFtZTogJ2VkaXRhYmxlJ1xuXHRcdH07XG5cdFxuXHRleHBvcnRzLm1hbnVmYWN0dXJlciA9IGV4cG9ydHMubWFudWZhY3R1cmVyIHx8IHtcblx0XHRcdGRhdGE6ICdtYW51ZmFjdHVyZXInLFxuXHRcdFx0bWluV2lkdGg6ICcxNTBweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMS41LFxuXHRcdFx0Y2xhc3NOYW1lOiAnZWRpdGFibGUgbWFudWZhY3R1cmVyJ1xuXHRcdH07XG5cdFxuXHRleHBvcnRzLnF1YW50aXR5ID0gZXhwb3J0cy5xdWFudGl0eSB8fCB7XG5cdFx0XHRkYXRhOiAncXVhbnRpdHknLFxuXHRcdFx0bWluV2lkdGg6ICc3NXB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLFxuXHRcdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYyBlZGl0YWJsZSdcblx0XHR9O1xuXHRcblx0ZXhwb3J0cy5wcmljZSA9IGV4cG9ydHMucHJpY2UgfHwge1xuXHRcdFx0ZGF0YTogJ3ByaWNlJyxcblx0XHRcdG1pbldpZHRoOiAnODVweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMS4yLFxuXHRcdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYyBlZGl0YWJsZSBwcm9kdWN0LXByaWNlJ1xuXHRcdH07XG5cdFxuXHRleHBvcnRzLmRpc2NvdW50ID0gZXhwb3J0cy5kaXNjb3VudCB8fCB7XG5cdFx0XHRkYXRhOiAnZGlzY291bnQnLFxuXHRcdFx0bWluV2lkdGg6ICc3NXB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLFxuXHRcdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYyBlZGl0YWJsZSdcblx0XHR9O1xuXHRcblx0ZXhwb3J0cy5zcGVjaWFsUHJpY2UgPSBleHBvcnRzLnNwZWNpYWxQcmljZSB8fCB7XG5cdFx0XHRkYXRhOiAnc3BlY2lhbFByaWNlJyxcblx0XHRcdG1pbldpZHRoOiAnODVweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMS4yLFxuXHRcdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYyBlZGl0YWJsZSB0b29sdGlwLXByb2R1Y3RzLXNwZWNpYWwtcHJpY2UnXG5cdFx0fTtcblx0XG5cdGV4cG9ydHMudGF4ID0gZXhwb3J0cy50YXggfHwge1xuXHRcdFx0ZGF0YTogJ3RheCcsXG5cdFx0XHRtaW5XaWR0aDogJzg1cHgnLFxuXHRcdFx0d2lkdGhGYWN0b3I6IDEuMixcblx0XHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKXtcblx0XHRcdFx0bGV0IGh0bWwgPSAnJztcblx0XHRcdFx0Y29uc3Qgb3B0aW9ucyA9IGZ1bGwuRFRfUm93RGF0YS5vcHRpb24udGF4O1xuXHRcdFx0XHRcblx0XHRcdFx0b3B0aW9ucy5mb3JFYWNoKG9wdGlvbiA9PiB7XG5cdFx0XHRcdFx0aHRtbCArPSBgPG9wdGlvbiB2YWx1ZT1cIiR7b3B0aW9uLmlkfVwiICR7ZnVsbC50YXhDbGFzc0lkID09IG9wdGlvbi5pZCA/ICdzZWxlY3RlZCcgOiAnJ30+XG5cdFx0XHRcdFx0XHRcdCR7b3B0aW9uLnZhbHVlfVxuXHRcdFx0XHRcdFx0PC9vcHRpb24+YDtcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gYDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2wgc2VsZWN0LXRheFwiPiR7aHRtbH08L3NlbGVjdD5gO1xuXHRcdFx0fVxuXHRcdH07XG5cdFxuXHRleHBvcnRzLnNoaXBwaW5nU3RhdHVzTmFtZSA9IGV4cG9ydHMuc2hpcHBpbmdTdGF0dXNOYW1lIHx8IHtcblx0XHRcdGRhdGE6ICdzaGlwcGluZ1N0YXR1c05hbWUnLFxuXHRcdFx0bWluV2lkdGg6ICcxMDBweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogMS4yLFxuXHRcdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpe1xuXHRcdFx0XHRsZXQgaHRtbCA9ICcnO1xuXHRcdFx0XHRjb25zdCBvcHRpb25zID0gZnVsbC5EVF9Sb3dEYXRhLm9wdGlvbi5zaGlwbWVudDtcblx0XHRcdFx0XG5cdFx0XHRcdG9wdGlvbnMuZm9yRWFjaChvcHRpb24gPT4ge1xuXHRcdFx0XHRcdGh0bWwgKz0gYDxvcHRpb24gdmFsdWU9XCIke29wdGlvbi5pZH1cIiAke2Z1bGwuc2hpcHBpbmdUaW1lSWQgPT0gb3B0aW9uLmlkID8gJ3NlbGVjdGVkJyA6ICcnfT5cblx0XHRcdFx0XHRcdFx0JHtvcHRpb24udmFsdWV9XG5cdFx0XHRcdFx0XHQ8L29wdGlvbj5gO1xuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdHJldHVybiBgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbCBzZWxlY3Qtc2hpcHBpbmctdGltZVwiPiR7aHRtbH08L3NlbGVjdD5gO1xuXHRcdFx0fVxuXHRcdH07XG5cdFxuXHRleHBvcnRzLndlaWdodCA9IGV4cG9ydHMud2VpZ2h0IHx8IHtcblx0XHRcdGRhdGE6ICd3ZWlnaHQnLFxuXHRcdFx0bWluV2lkdGg6ICc3NXB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjIsXG5cdFx0XHRjbGFzc05hbWU6ICdudW1lcmljIGVkaXRhYmxlJ1xuXHRcdH07XG5cdFxuXHRleHBvcnRzLnNoaXBwaW5nQ29zdHMgPSBleHBvcnRzLnNoaXBwaW5nQ29zdHMgfHwge1xuXHRcdFx0ZGF0YTogJ3NoaXBwaW5nQ29zdHMnLFxuXHRcdFx0bWluV2lkdGg6ICc4NXB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjIsXG5cdFx0XHRjbGFzc05hbWU6ICdudW1lcmljIGVkaXRhYmxlJ1xuXHRcdH07XG5cdFxuXHRleHBvcnRzLnN0YXR1cyA9IGV4cG9ydHMuc3RhdHVzIHx8IHtcblx0XHRcdGRhdGE6ICdzdGF0dXMnLFxuXHRcdFx0bWluV2lkdGg6ICc4NXB4Jyxcblx0XHRcdHdpZHRoRmFjdG9yOiAxLjIsXG5cdFx0XHRjbGFzc05hbWU6ICdzdGF0dXMnLFxuXHRcdFx0c2VhcmNoYWJsZTogZmFsc2UsXG5cdFx0XHRyZW5kZXIoZGF0YSkge1xuXHRcdFx0XHRyZXR1cm4gYDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiAkeyhkYXRhID8gXCJjaGVja2VkXCIgOiBcIlwiKX0gY2xhc3M9XCJjb252ZXJ0LXRvLXN3aXRjaGVyXCIgLz5gXG5cdFx0XHR9XG5cdFx0fTtcblx0XG5cdGV4cG9ydHMuYWN0aW9ucyA9IGV4cG9ydHMuYWN0aW9ucyB8fCB7XG5cdFx0XHRkYXRhOiBudWxsLFxuXHRcdFx0bWluV2lkdGg6ICc0NTBweCcsXG5cdFx0XHR3aWR0aEZhY3RvcjogNS4yLFxuXHRcdFx0Y2xhc3NOYW1lOiAnYWN0aW9ucycsXG5cdFx0XHRvcmRlcmFibGU6IGZhbHNlLFxuXHRcdFx0c2VhcmNoYWJsZTogZmFsc2UsXG5cdFx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xuXHRcdFx0XHRyZXR1cm4gYFx0XHRcdFx0XHRcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicHVsbC1sZWZ0XCI+PC9kaXY+XG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cInB1bGwtcmlnaHQgYWN0aW9uLWxpc3QgdmlzaWJsZS1vbi1ob3ZlclwiPlxuXHRcdFx0XHRcdFx0PGkgY2xhc3M9XCJmYSBmYS1wZW5jaWwgcm93LWVkaXRcIj48L2k+XG5cdFx0XHRcdFx0XHQ8aSBjbGFzcz1cImZhIGZhLWV5ZSBzaG93LXByb2R1Y3RcIj48L2k+XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwIGRyb3Bkb3duXCI+XG5cdFx0XHRcdFx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiXG5cdFx0XHRcdFx0XHRcdFx0XHRjbGFzcz1cImJ0biBidG4tZGVmYXVsdFwiPjwvYnV0dG9uPlxuXHRcdFx0XHRcdFx0XHQ8YnV0dG9uIHR5cGU9XCJidXR0b25cIlxuXHRcdFx0XHRcdFx0XHRcdFx0Y2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgZHJvcGRvd24tdG9nZ2xlXCJcblx0XHRcdFx0XHRcdFx0XHRcdGRhdGEtdG9nZ2xlPVwiZHJvcGRvd25cIlxuXHRcdFx0XHRcdFx0XHRcdFx0YXJpYS1oYXNwb3B1cD1cInRydWVcIlxuXHRcdFx0XHRcdFx0XHRcdFx0YXJpYS1leHBhbmRlZD1cImZhbHNlXCI+XG5cdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJjYXJldFwiPjwvc3Bhbj5cblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cInNyLW9ubHlcIj5Ub2dnbGUgRHJvcGRvd248L3NwYW4+XG5cdFx0XHRcdFx0XHRcdDwvYnV0dG9uPlxuXHRcdFx0XHRcdFx0XHQ8dWwgY2xhc3M9XCJkcm9wZG93bi1tZW51IGRyb3Bkb3duLW1lbnUtcmlnaHRcIj48L3VsPlxuXHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdGA7XG5cdFx0XHR9XG5cdFx0fTtcbn0pKGpzZS5saWJzLnF1aWNrX2VkaXRfb3ZlcnZpZXdfY29sdW1ucyk7ICJdfQ==
