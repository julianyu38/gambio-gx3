'use strict';

/* --------------------------------------------------------------
 tooltip.js 2016-09-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Tooltip Widget
 *
 * Enables qTip2 tooltips for child elements with a title attribute. You can change the default tooltip 
 * position and other options, if you set a data-tooltip-position attribute to the parent element.
 *
 * **Important:** If you use this widgets on elements inside a modal then it will not work,
 * because the modal elements are reset before they are displayed.
 *
 * ### Example
 *  
 * ```html
 * <form data-gx-widget="tooltip">
 *   <input type="text" title="This is a tooltip widget" />
 * </form>
 * ```
 * 
 * @module Admin/Widgets/tooltip
 * @requires jQuery-qTip2-Plugin
 */
gx.widgets.module('tooltip', [jse.source + '/vendor/qtip2/jquery.qtip.css', jse.source + '/vendor/qtip2/jquery.qtip.js'],

/** @lends module:Widgets/tooltip */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Widget Options
  *
  * @type {object}
  */
	defaults = {
		style: {
			classes: 'qtip-tipsy'
		},
		position: {
			my: 'bottom+200 top center',
			at: 'top center'
		}
	},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		$this.find('[title]').qtip(options);
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRvb2x0aXAuanMiXSwibmFtZXMiOlsiZ3giLCJ3aWRnZXRzIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwic3R5bGUiLCJjbGFzc2VzIiwicG9zaXRpb24iLCJteSIsImF0Iiwib3B0aW9ucyIsImV4dGVuZCIsImluaXQiLCJkb25lIiwiZmluZCIsInF0aXAiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkFBLEdBQUdDLE9BQUgsQ0FBV0MsTUFBWCxDQUNDLFNBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLG9DQUVJRCxJQUFJQyxNQUZSLGtDQUhEOztBQVFDOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXO0FBQ1ZDLFNBQU87QUFDTkMsWUFBUztBQURILEdBREc7QUFJVkMsWUFBVTtBQUNUQyxPQUFJLHVCQURLO0FBRVRDLE9BQUk7QUFGSztBQUpBLEVBYlo7OztBQXVCQzs7Ozs7QUFLQUMsV0FBVVAsRUFBRVEsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CUCxRQUFuQixFQUE2QkgsSUFBN0IsQ0E1Qlg7OztBQThCQzs7Ozs7QUFLQUgsVUFBUyxFQW5DVjs7QUFxQ0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQUEsUUFBT2MsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QlgsUUFBTVksSUFBTixDQUFXLFNBQVgsRUFBc0JDLElBQXRCLENBQTJCTCxPQUEzQjtBQUNBRztBQUNBLEVBSEQ7O0FBS0E7QUFDQSxRQUFPZixNQUFQO0FBQ0EsQ0FyRUYiLCJmaWxlIjoidG9vbHRpcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gdG9vbHRpcC5qcyAyMDE2LTA5LTA5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBUb29sdGlwIFdpZGdldFxuICpcbiAqIEVuYWJsZXMgcVRpcDIgdG9vbHRpcHMgZm9yIGNoaWxkIGVsZW1lbnRzIHdpdGggYSB0aXRsZSBhdHRyaWJ1dGUuIFlvdSBjYW4gY2hhbmdlIHRoZSBkZWZhdWx0IHRvb2x0aXAgXG4gKiBwb3NpdGlvbiBhbmQgb3RoZXIgb3B0aW9ucywgaWYgeW91IHNldCBhIGRhdGEtdG9vbHRpcC1wb3NpdGlvbiBhdHRyaWJ1dGUgdG8gdGhlIHBhcmVudCBlbGVtZW50LlxuICpcbiAqICoqSW1wb3J0YW50OioqIElmIHlvdSB1c2UgdGhpcyB3aWRnZXRzIG9uIGVsZW1lbnRzIGluc2lkZSBhIG1vZGFsIHRoZW4gaXQgd2lsbCBub3Qgd29yayxcbiAqIGJlY2F1c2UgdGhlIG1vZGFsIGVsZW1lbnRzIGFyZSByZXNldCBiZWZvcmUgdGhleSBhcmUgZGlzcGxheWVkLlxuICpcbiAqICMjIyBFeGFtcGxlXG4gKiAgXG4gKiBgYGBodG1sXG4gKiA8Zm9ybSBkYXRhLWd4LXdpZGdldD1cInRvb2x0aXBcIj5cbiAqICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgdGl0bGU9XCJUaGlzIGlzIGEgdG9vbHRpcCB3aWRnZXRcIiAvPlxuICogPC9mb3JtPlxuICogYGBgXG4gKiBcbiAqIEBtb2R1bGUgQWRtaW4vV2lkZ2V0cy90b29sdGlwXG4gKiBAcmVxdWlyZXMgalF1ZXJ5LXFUaXAyLVBsdWdpblxuICovXG5neC53aWRnZXRzLm1vZHVsZShcblx0J3Rvb2x0aXAnLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9xdGlwMi9qcXVlcnkucXRpcC5jc3NgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9xdGlwMi9qcXVlcnkucXRpcC5qc2Bcblx0XSxcblx0XG5cdC8qKiBAbGVuZHMgbW9kdWxlOldpZGdldHMvdG9vbHRpcCAqL1xuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIFdpZGdldCBSZWZlcmVuY2Vcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBXaWRnZXQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0ge1xuXHRcdFx0XHRzdHlsZToge1xuXHRcdFx0XHRcdGNsYXNzZXM6ICdxdGlwLXRpcHN5J1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRwb3NpdGlvbjoge1xuXHRcdFx0XHRcdG15OiAnYm90dG9tKzIwMCB0b3AgY2VudGVyJyxcblx0XHRcdFx0XHRhdDogJ3RvcCBjZW50ZXInXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgV2lkZ2V0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIG1ldGhvZCBvZiB0aGUgd2lkZ2V0LCBjYWxsZWQgYnkgdGhlIGVuZ2luZS5cblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCR0aGlzLmZpbmQoJ1t0aXRsZV0nKS5xdGlwKG9wdGlvbnMpO1xuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
