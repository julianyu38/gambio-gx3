'use strict';

/* --------------------------------------------------------------
 catalog_selector.js 2017-11-30
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## CatalogSelector Widget
 *
 * This Widgets generates a selection for products (categories at not implemented yet, but possibil).
 * The given HTML code will be cloned to and modified to create this selection.
 *
 *
 * ### Parent Container Options
 *
 * **Headline text | `data-catalog_selector-headline_text` | String | Required**
 *
 * Textphrase for the collapse headline.
 *
 * **Add button text | `data-catalog_selector-add_button_text` | String | Required**
 *
 * Textphrase for the add new dropdown button.
 *
 * **Form group selector | `data-catalog_selector-group_selector` | String | Optional**
 *
 * This selector is required to select the form group that must be contained in the given HTML.
 *
 * **Label selector | `data-catalog_selector-label_selector` | String | Optional**
 *
 * This selector is required to select the label of the form group.
 *
 * **Dropdown selector | `data-catalog_selector-dropdown_selector` | String | Optional**
 *
 * This selector is required to select dropdown of the form group.
 *
 * **Remove icon column selector | `data-catalog_selector-remove_selector` | String | Optional**
 *
 * This selector is required to select the column for the remove icon of the form group.
 *
 * **Selected data | `data-catalog_selector-selected_data` | String | Optional**
 *
 * Already selected data for the selection.
 *
 *
 * ### Example
 * ```html
 * <div data-gx-widget="catalog_selector"
 *     data-catalog_selector-group_selector=".form-group"
 *     data-catalog_selector-label_selector="label"
 *     data-catalog_selector-dropdown_selector=".catalog-selector-dropdown"
 *     data-catalog_selector-remove_selector=".catalog-selector-remove"
 *     data-catalog_selector-headline_text="HEADLINE"
 *     data-catalog_selector-add_button_text="Add dropdown"
 *     data-catalog_selector-selected_data="1,2,3,4"
 * >
 *     <div class="form-group">
 *         <label class="col-md-4">{$txt.LABEL_SHOW_FOR_PRODUCT}:</label>
 *         <div class="col-md-3">
 *             <select class="form-control catalog-selector-dropdown"
 *             name="content_manager[infopage][sitemap_changefreq][{$languageCode}]"></select>
 *         </div>
 *         <div class="col-md-1 catalog-selector-remove"></div>
 *     </div>
 * </div>
 * ```
 *
 *
 * @module Admin/Widgets/catalog_selector
 */
gx.widgets.module('catalog_selector', [gx.source + '/widgets/collapser'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Copy of the given form group
  *
  * @type {object}
  */
	$groupObject = {},


	/**
  * Data that's used to fill the dropdown.
  * Will be loaded by ajax from the widget ajax controller
  *
  * @type string
  */
	dataTreeHtml = {},


	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	defaults = {
		'group_selector': '.form-group',
		'label_selector': 'label',
		'dropdown_selector': '.catalog-selector-dropdown',
		'remove_selector': '.catalog-selector-remove',
		'selected_data': ''
	},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// INITIALIZE
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		// Perform ajax request to collect products and categories
		_performRequest('getProductsTreeAsOptgroups').done(function (response) {
			dataTreeHtml = JSON.parse(response)['html'];
		});

		// Initialize widget html
		$this.append('\n\t\t\t\t\t<fieldset>\n\t                    <div class="frame-wrapper default">\n\t\t\t\t\t\t\t<div class="frame-head"\n\t\t\t\t\t\t\t\tdata-gx-widget="collapser"\n\t\t\t\t\t\t\t\tdata-collapser-target_selector=".frame-content">\n\t\t\t\t\t\t\t\t' + options.headline_text + '\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class="frame-content">\n\t\t\t\t\t\t\t\t<div class="catalog-selection-data"></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</fieldset>\n\t\t\t');
		$this.find('.catalog-selection-data').parent().append('\n\t\t\t\t\t<button type="button" class="btn catalog-selection-new-dropdown">\n\t\t\t\t\t\t' + options.add_button_text + '\n\t\t\t\t\t</button>\n\t\t\t\t').find('.catalog-selection-new-dropdown').on('click', _onNewDropdown);
		gx.widgets.init($this.parent());

		// Backup form group
		$groupObject = $this.find(options.group_selector);
		$this.find(options.group_selector).remove();

		// Fill dropdown menu of backuped form group with options
		$groupObject.find(options.dropdown_selector).append(dataTreeHtml);

		// Delete given html (It is will not be used anymore)
		$groupObject.clone().appendTo($this.find('.catalog-selection-data'));

		// Add pre-selected data
		_addSelectedData();

		done();
	};

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	/**
  * Performs the ajax request to collec tthe products and categories
  *
  * @param {String} action
  * @returns {String} JSON
  */
	function _performRequest(action) {
		var URL_BASE = 'admin.php?do=CatalogSelectWidgetAjax/';

		// AJAX request options.
		var ajaxOptions = {
			url: URL_BASE + action,
			async: false
		};

		// Returns deferred object.
		return $.ajax(ajaxOptions);
	}

	/**
  * Create a new form group with another dropdown
  */
	function _onNewDropdown() {
		$groupObject.clone().appendTo($this.find('.catalog-selection-data')).find(options.label_selector).empty().parent().find(options.remove_selector).append('<i class="fa fa-trash-o" aria-hidden="true"></i>').find('i').on('click', _removeDropdown);
	}

	/**
  * Removes a form group. Will be initialize by a click event on the remove icon.
  */
	function _removeDropdown() {
		$(this).closest(options.group_selector).remove();
	}

	/**
  * Adds the pre selected data.
  */
	function _addSelectedData() {
		if (options.selected_data == '') {
			return;
		}

		if (String(options.selected_data).indexOf(",") == -1) {
			$this.find(options.dropdown_selector + ':last').val(options.selected_data);
			_onNewDropdown();
		} else {
			var selectedData = options.selected_data.split(',');
			console.log(selectedData);
			for (var i in selectedData) {
				$this.find(options.dropdown_selector + ':last').val(selectedData[i]);
				_onNewDropdown();
			}
		}
	}

	// Return data to module engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhdGFsb2dfc2VsZWN0b3IuanMiXSwibmFtZXMiOlsiZ3giLCJ3aWRnZXRzIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRncm91cE9iamVjdCIsImRhdGFUcmVlSHRtbCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsImluaXQiLCJkb25lIiwiX3BlcmZvcm1SZXF1ZXN0IiwicmVzcG9uc2UiLCJKU09OIiwicGFyc2UiLCJhcHBlbmQiLCJoZWFkbGluZV90ZXh0IiwiZmluZCIsInBhcmVudCIsImFkZF9idXR0b25fdGV4dCIsIm9uIiwiX29uTmV3RHJvcGRvd24iLCJncm91cF9zZWxlY3RvciIsInJlbW92ZSIsImRyb3Bkb3duX3NlbGVjdG9yIiwiY2xvbmUiLCJhcHBlbmRUbyIsIl9hZGRTZWxlY3RlZERhdGEiLCJhY3Rpb24iLCJVUkxfQkFTRSIsImFqYXhPcHRpb25zIiwidXJsIiwiYXN5bmMiLCJhamF4IiwibGFiZWxfc2VsZWN0b3IiLCJlbXB0eSIsInJlbW92ZV9zZWxlY3RvciIsIl9yZW1vdmVEcm9wZG93biIsImNsb3Nlc3QiLCJzZWxlY3RlZF9kYXRhIiwiU3RyaW5nIiwiaW5kZXhPZiIsInZhbCIsInNlbGVjdGVkRGF0YSIsInNwbGl0IiwiY29uc29sZSIsImxvZyIsImkiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBK0RBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxrQkFERCxFQUdDLENBQ0lGLEdBQUdHLE1BRFAsd0JBSEQsRUFPQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsZ0JBQWUsRUFiaEI7OztBQWVDOzs7Ozs7QUFNQUMsZ0JBQWUsRUFyQmhCOzs7QUF1QkM7Ozs7O0FBS0FDLFlBQVc7QUFDVixvQkFBa0IsYUFEUjtBQUVWLG9CQUFrQixPQUZSO0FBR1YsdUJBQXFCLDRCQUhYO0FBSVYscUJBQW1CLDBCQUpUO0FBS1YsbUJBQWlCO0FBTFAsRUE1Qlo7OztBQW9DQzs7Ozs7QUFLQUMsV0FBVUosRUFBRUssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkwsSUFBN0IsQ0F6Q1g7OztBQTJDQzs7Ozs7QUFLQUYsVUFBUyxFQWhEVjs7QUFrREE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQUEsUUFBT1UsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjtBQUNBQyxrQkFBZ0IsNEJBQWhCLEVBQ0VELElBREYsQ0FDTyxVQUFTRSxRQUFULEVBQW1CO0FBQ3hCUCxrQkFBZVEsS0FBS0MsS0FBTCxDQUFXRixRQUFYLEVBQXFCLE1BQXJCLENBQWY7QUFDQSxHQUhGOztBQUtBO0FBQ0FWLFFBQ0VhLE1BREYsQ0FDUyw2UEFNQVIsUUFBUVMsYUFOUiwyTUFEVDtBQWVBZCxRQUNFZSxJQURGLENBQ08seUJBRFAsRUFFRUMsTUFGRixHQUdFSCxNQUhGLENBR1MsZ0dBRUpSLFFBQVFZLGVBRkosb0NBSFQsRUFRRUYsSUFSRixDQVFPLGlDQVJQLEVBU0VHLEVBVEYsQ0FTSyxPQVRMLEVBU2NDLGNBVGQ7QUFVQXhCLEtBQUdDLE9BQUgsQ0FBV1csSUFBWCxDQUFnQlAsTUFBTWdCLE1BQU4sRUFBaEI7O0FBRUE7QUFDQWQsaUJBQWVGLE1BQU1lLElBQU4sQ0FBV1YsUUFBUWUsY0FBbkIsQ0FBZjtBQUNBcEIsUUFBTWUsSUFBTixDQUFXVixRQUFRZSxjQUFuQixFQUFtQ0MsTUFBbkM7O0FBRUE7QUFDQW5CLGVBQ0VhLElBREYsQ0FDT1YsUUFBUWlCLGlCQURmLEVBRUVULE1BRkYsQ0FFU1YsWUFGVDs7QUFJQTtBQUNBRCxlQUFhcUIsS0FBYixHQUFxQkMsUUFBckIsQ0FBOEJ4QixNQUFNZSxJQUFOLENBQVcseUJBQVgsQ0FBOUI7O0FBRUE7QUFDQVU7O0FBRUFqQjtBQUNBLEVBbkREOztBQXNEQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BLFVBQVNDLGVBQVQsQ0FBeUJpQixNQUF6QixFQUFpQztBQUNoQyxNQUFNQyxXQUFXLHVDQUFqQjs7QUFFQTtBQUNBLE1BQU1DLGNBQWM7QUFDbkJDLFFBQUtGLFdBQVdELE1BREc7QUFFbkJJLFVBQU87QUFGWSxHQUFwQjs7QUFLQTtBQUNBLFNBQU83QixFQUFFOEIsSUFBRixDQUFPSCxXQUFQLENBQVA7QUFDQTs7QUFFRDs7O0FBR0EsVUFBU1QsY0FBVCxHQUEwQjtBQUN6QmpCLGVBQ0VxQixLQURGLEdBRUVDLFFBRkYsQ0FFV3hCLE1BQU1lLElBQU4sQ0FBVyx5QkFBWCxDQUZYLEVBR0VBLElBSEYsQ0FHT1YsUUFBUTJCLGNBSGYsRUFJRUMsS0FKRixHQUtFakIsTUFMRixHQU1FRCxJQU5GLENBTU9WLFFBQVE2QixlQU5mLEVBT0VyQixNQVBGLENBT1Msa0RBUFQsRUFRRUUsSUFSRixDQVFPLEdBUlAsRUFTRUcsRUFURixDQVNLLE9BVEwsRUFTY2lCLGVBVGQ7QUFVQTs7QUFFRDs7O0FBR0EsVUFBU0EsZUFBVCxHQUEyQjtBQUMxQmxDLElBQUUsSUFBRixFQUFRbUMsT0FBUixDQUFnQi9CLFFBQVFlLGNBQXhCLEVBQXdDQyxNQUF4QztBQUNBOztBQUVEOzs7QUFHQSxVQUFTSSxnQkFBVCxHQUE0QjtBQUMzQixNQUFHcEIsUUFBUWdDLGFBQVIsSUFBeUIsRUFBNUIsRUFBK0I7QUFDOUI7QUFDQTs7QUFFRCxNQUFHQyxPQUFPakMsUUFBUWdDLGFBQWYsRUFBOEJFLE9BQTlCLENBQXNDLEdBQXRDLEtBQThDLENBQUMsQ0FBbEQsRUFBb0Q7QUFDbkR2QyxTQUFNZSxJQUFOLENBQVdWLFFBQVFpQixpQkFBUixHQUEwQixPQUFyQyxFQUE4Q2tCLEdBQTlDLENBQWtEbkMsUUFBUWdDLGFBQTFEO0FBQ0FsQjtBQUNBLEdBSEQsTUFHSztBQUNKLE9BQUlzQixlQUFlcEMsUUFBUWdDLGFBQVIsQ0FBc0JLLEtBQXRCLENBQTRCLEdBQTVCLENBQW5CO0FBQ0FDLFdBQVFDLEdBQVIsQ0FBWUgsWUFBWjtBQUNBLFFBQUksSUFBSUksQ0FBUixJQUFhSixZQUFiLEVBQ0E7QUFDQ3pDLFVBQU1lLElBQU4sQ0FBV1YsUUFBUWlCLGlCQUFSLEdBQTBCLE9BQXJDLEVBQThDa0IsR0FBOUMsQ0FBa0RDLGFBQWFJLENBQWIsQ0FBbEQ7QUFDQTFCO0FBQ0E7QUFDRDtBQUNEOztBQUVEO0FBQ0EsUUFBT3RCLE1BQVA7QUFDQSxDQXBNRiIsImZpbGUiOiJjYXRhbG9nX3NlbGVjdG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBjYXRhbG9nX3NlbGVjdG9yLmpzIDIwMTctMTEtMzBcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIENhdGFsb2dTZWxlY3RvciBXaWRnZXRcbiAqXG4gKiBUaGlzIFdpZGdldHMgZ2VuZXJhdGVzIGEgc2VsZWN0aW9uIGZvciBwcm9kdWN0cyAoY2F0ZWdvcmllcyBhdCBub3QgaW1wbGVtZW50ZWQgeWV0LCBidXQgcG9zc2liaWwpLlxuICogVGhlIGdpdmVuIEhUTUwgY29kZSB3aWxsIGJlIGNsb25lZCB0byBhbmQgbW9kaWZpZWQgdG8gY3JlYXRlIHRoaXMgc2VsZWN0aW9uLlxuICpcbiAqXG4gKiAjIyMgUGFyZW50IENvbnRhaW5lciBPcHRpb25zXG4gKlxuICogKipIZWFkbGluZSB0ZXh0IHwgYGRhdGEtY2F0YWxvZ19zZWxlY3Rvci1oZWFkbGluZV90ZXh0YCB8IFN0cmluZyB8IFJlcXVpcmVkKipcbiAqXG4gKiBUZXh0cGhyYXNlIGZvciB0aGUgY29sbGFwc2UgaGVhZGxpbmUuXG4gKlxuICogKipBZGQgYnV0dG9uIHRleHQgfCBgZGF0YS1jYXRhbG9nX3NlbGVjdG9yLWFkZF9idXR0b25fdGV4dGAgfCBTdHJpbmcgfCBSZXF1aXJlZCoqXG4gKlxuICogVGV4dHBocmFzZSBmb3IgdGhlIGFkZCBuZXcgZHJvcGRvd24gYnV0dG9uLlxuICpcbiAqICoqRm9ybSBncm91cCBzZWxlY3RvciB8IGBkYXRhLWNhdGFsb2dfc2VsZWN0b3ItZ3JvdXBfc2VsZWN0b3JgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICpcbiAqIFRoaXMgc2VsZWN0b3IgaXMgcmVxdWlyZWQgdG8gc2VsZWN0IHRoZSBmb3JtIGdyb3VwIHRoYXQgbXVzdCBiZSBjb250YWluZWQgaW4gdGhlIGdpdmVuIEhUTUwuXG4gKlxuICogKipMYWJlbCBzZWxlY3RvciB8IGBkYXRhLWNhdGFsb2dfc2VsZWN0b3ItbGFiZWxfc2VsZWN0b3JgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICpcbiAqIFRoaXMgc2VsZWN0b3IgaXMgcmVxdWlyZWQgdG8gc2VsZWN0IHRoZSBsYWJlbCBvZiB0aGUgZm9ybSBncm91cC5cbiAqXG4gKiAqKkRyb3Bkb3duIHNlbGVjdG9yIHwgYGRhdGEtY2F0YWxvZ19zZWxlY3Rvci1kcm9wZG93bl9zZWxlY3RvcmAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogVGhpcyBzZWxlY3RvciBpcyByZXF1aXJlZCB0byBzZWxlY3QgZHJvcGRvd24gb2YgdGhlIGZvcm0gZ3JvdXAuXG4gKlxuICogKipSZW1vdmUgaWNvbiBjb2x1bW4gc2VsZWN0b3IgfCBgZGF0YS1jYXRhbG9nX3NlbGVjdG9yLXJlbW92ZV9zZWxlY3RvcmAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogVGhpcyBzZWxlY3RvciBpcyByZXF1aXJlZCB0byBzZWxlY3QgdGhlIGNvbHVtbiBmb3IgdGhlIHJlbW92ZSBpY29uIG9mIHRoZSBmb3JtIGdyb3VwLlxuICpcbiAqICoqU2VsZWN0ZWQgZGF0YSB8IGBkYXRhLWNhdGFsb2dfc2VsZWN0b3Itc2VsZWN0ZWRfZGF0YWAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogQWxyZWFkeSBzZWxlY3RlZCBkYXRhIGZvciB0aGUgc2VsZWN0aW9uLlxuICpcbiAqXG4gKiAjIyMgRXhhbXBsZVxuICogYGBgaHRtbFxuICogPGRpdiBkYXRhLWd4LXdpZGdldD1cImNhdGFsb2dfc2VsZWN0b3JcIlxuICogICAgIGRhdGEtY2F0YWxvZ19zZWxlY3Rvci1ncm91cF9zZWxlY3Rvcj1cIi5mb3JtLWdyb3VwXCJcbiAqICAgICBkYXRhLWNhdGFsb2dfc2VsZWN0b3ItbGFiZWxfc2VsZWN0b3I9XCJsYWJlbFwiXG4gKiAgICAgZGF0YS1jYXRhbG9nX3NlbGVjdG9yLWRyb3Bkb3duX3NlbGVjdG9yPVwiLmNhdGFsb2ctc2VsZWN0b3ItZHJvcGRvd25cIlxuICogICAgIGRhdGEtY2F0YWxvZ19zZWxlY3Rvci1yZW1vdmVfc2VsZWN0b3I9XCIuY2F0YWxvZy1zZWxlY3Rvci1yZW1vdmVcIlxuICogICAgIGRhdGEtY2F0YWxvZ19zZWxlY3Rvci1oZWFkbGluZV90ZXh0PVwiSEVBRExJTkVcIlxuICogICAgIGRhdGEtY2F0YWxvZ19zZWxlY3Rvci1hZGRfYnV0dG9uX3RleHQ9XCJBZGQgZHJvcGRvd25cIlxuICogICAgIGRhdGEtY2F0YWxvZ19zZWxlY3Rvci1zZWxlY3RlZF9kYXRhPVwiMSwyLDMsNFwiXG4gKiA+XG4gKiAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cbiAqICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTRcIj57JHR4dC5MQUJFTF9TSE9XX0ZPUl9QUk9EVUNUfTo8L2xhYmVsPlxuICogICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTNcIj5cbiAqICAgICAgICAgICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2wgY2F0YWxvZy1zZWxlY3Rvci1kcm9wZG93blwiXG4gKiAgICAgICAgICAgICBuYW1lPVwiY29udGVudF9tYW5hZ2VyW2luZm9wYWdlXVtzaXRlbWFwX2NoYW5nZWZyZXFdW3skbGFuZ3VhZ2VDb2RlfV1cIj48L3NlbGVjdD5cbiAqICAgICAgICAgPC9kaXY+XG4gKiAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMSBjYXRhbG9nLXNlbGVjdG9yLXJlbW92ZVwiPjwvZGl2PlxuICogICAgIDwvZGl2PlxuICogPC9kaXY+XG4gKiBgYGBcbiAqXG4gKlxuICogQG1vZHVsZSBBZG1pbi9XaWRnZXRzL2NhdGFsb2dfc2VsZWN0b3JcbiAqL1xuZ3gud2lkZ2V0cy5tb2R1bGUoXG5cdCdjYXRhbG9nX3NlbGVjdG9yJyxcblx0XG5cdFtcblx0XHRgJHtneC5zb3VyY2V9L3dpZGdldHMvY29sbGFwc2VyYFxuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIFdpZGdldCBSZWZlcmVuY2Vcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogQ29weSBvZiB0aGUgZ2l2ZW4gZm9ybSBncm91cFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCRncm91cE9iamVjdCA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERhdGEgdGhhdCdzIHVzZWQgdG8gZmlsbCB0aGUgZHJvcGRvd24uXG5cdFx0XHQgKiBXaWxsIGJlIGxvYWRlZCBieSBhamF4IGZyb20gdGhlIHdpZGdldCBhamF4IGNvbnRyb2xsZXJcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSBzdHJpbmdcblx0XHRcdCAqL1xuXHRcdFx0ZGF0YVRyZWVIdG1sID0ge30sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zIGZvciBXaWRnZXRcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0J2dyb3VwX3NlbGVjdG9yJzogJy5mb3JtLWdyb3VwJyxcblx0XHRcdFx0J2xhYmVsX3NlbGVjdG9yJzogJ2xhYmVsJyxcblx0XHRcdFx0J2Ryb3Bkb3duX3NlbGVjdG9yJzogJy5jYXRhbG9nLXNlbGVjdG9yLWRyb3Bkb3duJyxcblx0XHRcdFx0J3JlbW92ZV9zZWxlY3Rvcic6ICcuY2F0YWxvZy1zZWxlY3Rvci1yZW1vdmUnLFxuXHRcdFx0XHQnc2VsZWN0ZWRfZGF0YSc6ICcnXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIFdpZGdldCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpFXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIHdpZGdldCwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBQZXJmb3JtIGFqYXggcmVxdWVzdCB0byBjb2xsZWN0IHByb2R1Y3RzIGFuZCBjYXRlZ29yaWVzXG5cdFx0XHRfcGVyZm9ybVJlcXVlc3QoJ2dldFByb2R1Y3RzVHJlZUFzT3B0Z3JvdXBzJylcblx0XHRcdFx0LmRvbmUoZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRkYXRhVHJlZUh0bWwgPSBKU09OLnBhcnNlKHJlc3BvbnNlKVsnaHRtbCddO1xuXHRcdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gSW5pdGlhbGl6ZSB3aWRnZXQgaHRtbFxuXHRcdFx0JHRoaXNcblx0XHRcdFx0LmFwcGVuZChgXG5cdFx0XHRcdFx0PGZpZWxkc2V0PlxuXHQgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmcmFtZS13cmFwcGVyIGRlZmF1bHRcIj5cblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImZyYW1lLWhlYWRcIlxuXHRcdFx0XHRcdFx0XHRcdGRhdGEtZ3gtd2lkZ2V0PVwiY29sbGFwc2VyXCJcblx0XHRcdFx0XHRcdFx0XHRkYXRhLWNvbGxhcHNlci10YXJnZXRfc2VsZWN0b3I9XCIuZnJhbWUtY29udGVudFwiPlxuXHRcdFx0XHRcdFx0XHRcdGAgKyBvcHRpb25zLmhlYWRsaW5lX3RleHQgKyBgXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZnJhbWUtY29udGVudFwiPlxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXRhbG9nLXNlbGVjdGlvbi1kYXRhXCI+PC9kaXY+XG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0PC9maWVsZHNldD5cblx0XHRcdGApO1xuXHRcdFx0JHRoaXNcblx0XHRcdFx0LmZpbmQoJy5jYXRhbG9nLXNlbGVjdGlvbi1kYXRhJylcblx0XHRcdFx0LnBhcmVudCgpXG5cdFx0XHRcdC5hcHBlbmQoYFxuXHRcdFx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGNhdGFsb2ctc2VsZWN0aW9uLW5ldy1kcm9wZG93blwiPlxuXHRcdFx0XHRcdFx0YCtvcHRpb25zLmFkZF9idXR0b25fdGV4dCtgXG5cdFx0XHRcdFx0PC9idXR0b24+XG5cdFx0XHRcdGApXG5cdFx0XHRcdC5maW5kKCcuY2F0YWxvZy1zZWxlY3Rpb24tbmV3LWRyb3Bkb3duJylcblx0XHRcdFx0Lm9uKCdjbGljaycsIF9vbk5ld0Ryb3Bkb3duKTtcblx0XHRcdGd4LndpZGdldHMuaW5pdCgkdGhpcy5wYXJlbnQoKSk7XG5cdFx0XHRcblx0XHRcdC8vIEJhY2t1cCBmb3JtIGdyb3VwXG5cdFx0XHQkZ3JvdXBPYmplY3QgPSAkdGhpcy5maW5kKG9wdGlvbnMuZ3JvdXBfc2VsZWN0b3IpO1xuXHRcdFx0JHRoaXMuZmluZChvcHRpb25zLmdyb3VwX3NlbGVjdG9yKS5yZW1vdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gRmlsbCBkcm9wZG93biBtZW51IG9mIGJhY2t1cGVkIGZvcm0gZ3JvdXAgd2l0aCBvcHRpb25zXG5cdFx0XHQkZ3JvdXBPYmplY3Rcblx0XHRcdFx0LmZpbmQob3B0aW9ucy5kcm9wZG93bl9zZWxlY3Rvcilcblx0XHRcdFx0LmFwcGVuZChkYXRhVHJlZUh0bWwpO1xuXHRcdFx0XG5cdFx0XHQvLyBEZWxldGUgZ2l2ZW4gaHRtbCAoSXQgaXMgd2lsbCBub3QgYmUgdXNlZCBhbnltb3JlKVxuXHRcdFx0JGdyb3VwT2JqZWN0LmNsb25lKCkuYXBwZW5kVG8oJHRoaXMuZmluZCgnLmNhdGFsb2ctc2VsZWN0aW9uLWRhdGEnKSk7XG5cdFx0XHRcblx0XHRcdC8vIEFkZCBwcmUtc2VsZWN0ZWQgZGF0YVxuXHRcdFx0X2FkZFNlbGVjdGVkRGF0YSgpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBIRUxQRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUGVyZm9ybXMgdGhlIGFqYXggcmVxdWVzdCB0byBjb2xsZWMgdHRoZSBwcm9kdWN0cyBhbmQgY2F0ZWdvcmllc1xuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGFjdGlvblxuXHRcdCAqIEByZXR1cm5zIHtTdHJpbmd9IEpTT05cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfcGVyZm9ybVJlcXVlc3QoYWN0aW9uKSB7XG5cdFx0XHRjb25zdCBVUkxfQkFTRSA9ICdhZG1pbi5waHA/ZG89Q2F0YWxvZ1NlbGVjdFdpZGdldEFqYXgvJztcblx0XHRcdFxuXHRcdFx0Ly8gQUpBWCByZXF1ZXN0IG9wdGlvbnMuXG5cdFx0XHRjb25zdCBhamF4T3B0aW9ucyA9IHtcblx0XHRcdFx0dXJsOiBVUkxfQkFTRSArIGFjdGlvbixcblx0XHRcdFx0YXN5bmM6IGZhbHNlLFxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gUmV0dXJucyBkZWZlcnJlZCBvYmplY3QuXG5cdFx0XHRyZXR1cm4gJC5hamF4KGFqYXhPcHRpb25zKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlIGEgbmV3IGZvcm0gZ3JvdXAgd2l0aCBhbm90aGVyIGRyb3Bkb3duXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX29uTmV3RHJvcGRvd24oKSB7XG5cdFx0XHQkZ3JvdXBPYmplY3Rcblx0XHRcdFx0LmNsb25lKClcblx0XHRcdFx0LmFwcGVuZFRvKCR0aGlzLmZpbmQoJy5jYXRhbG9nLXNlbGVjdGlvbi1kYXRhJykpXG5cdFx0XHRcdC5maW5kKG9wdGlvbnMubGFiZWxfc2VsZWN0b3IpXG5cdFx0XHRcdC5lbXB0eSgpXG5cdFx0XHRcdC5wYXJlbnQoKVxuXHRcdFx0XHQuZmluZChvcHRpb25zLnJlbW92ZV9zZWxlY3Rvcilcblx0XHRcdFx0LmFwcGVuZCgnPGkgY2xhc3M9XCJmYSBmYS10cmFzaC1vXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPicpXG5cdFx0XHRcdC5maW5kKCdpJylcblx0XHRcdFx0Lm9uKCdjbGljaycsIF9yZW1vdmVEcm9wZG93bik7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlbW92ZXMgYSBmb3JtIGdyb3VwLiBXaWxsIGJlIGluaXRpYWxpemUgYnkgYSBjbGljayBldmVudCBvbiB0aGUgcmVtb3ZlIGljb24uXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3JlbW92ZURyb3Bkb3duKCkge1xuXHRcdFx0JCh0aGlzKS5jbG9zZXN0KG9wdGlvbnMuZ3JvdXBfc2VsZWN0b3IpLnJlbW92ZSgpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGRzIHRoZSBwcmUgc2VsZWN0ZWQgZGF0YS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfYWRkU2VsZWN0ZWREYXRhKCkge1xuXHRcdFx0aWYob3B0aW9ucy5zZWxlY3RlZF9kYXRhID09ICcnKXtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZihTdHJpbmcob3B0aW9ucy5zZWxlY3RlZF9kYXRhKS5pbmRleE9mKFwiLFwiKSA9PSAtMSl7XG5cdFx0XHRcdCR0aGlzLmZpbmQob3B0aW9ucy5kcm9wZG93bl9zZWxlY3RvcisnOmxhc3QnKS52YWwob3B0aW9ucy5zZWxlY3RlZF9kYXRhKTtcblx0XHRcdFx0X29uTmV3RHJvcGRvd24oKTtcblx0XHRcdH1lbHNle1xuXHRcdFx0XHR2YXIgc2VsZWN0ZWREYXRhID0gb3B0aW9ucy5zZWxlY3RlZF9kYXRhLnNwbGl0KCcsJyk7XG5cdFx0XHRcdGNvbnNvbGUubG9nKHNlbGVjdGVkRGF0YSk7XG5cdFx0XHRcdGZvcih2YXIgaSBpbiBzZWxlY3RlZERhdGEpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQkdGhpcy5maW5kKG9wdGlvbnMuZHJvcGRvd25fc2VsZWN0b3IrJzpsYXN0JykudmFsKHNlbGVjdGVkRGF0YVtpXSk7XG5cdFx0XHRcdFx0X29uTmV3RHJvcGRvd24oKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byBtb2R1bGUgZW5naW5lXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
