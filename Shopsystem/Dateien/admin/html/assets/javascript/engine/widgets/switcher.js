'use strict';

/* --------------------------------------------------------------
 switcher.js 2017-11-21
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Switcher Widget
 *
 * This widget originates from the "switcher" mode of the existing checkbox widget. Because of the increased
 * complexity of the old widget code, the switcher mode is now server by this file. Apply the widget in a parent
 * container and it will search and convert all the checkbox/radio instances into switchers.
 *
 * ### Options
 *
 * **On State | `data-switcher-on-state` | String | Optional**
 *
 * Define the content of the "on" state.
 *
 * **Off State | `data-switcher-off-state` | String | Optional**
 *
 * Define the content of the "off" state.
 *
 * **Selector | `data-switcher-selector` | String | Optional**
 *
 * Set the selector of the checkboxes to be converted to switcher instances. It defaults to **input:checkbox**.
 *
 * ### Methods
 *
 * **Checked**
 *
 * ```js
 * // Set the checked value of the single checkbox selection (no change event will be triggered!).
 * $('table input:checkbox').switcher('checked', true);
 * ```
 *
 * ### Examples
 *
 * In the following example the checkbox element will be converted into a single-checkbox instance.
 *
 * ```html
 * <div class="wrapper" data-gx-widget="switcher">
 *   <input type="checkbox" />
 * </div>
 * ```
 *
 * @todo Add method for disabling the switcher widget (e.g. $('#my-switcher').switcher('disabled', true));
 *
 * @module Admin/Widgets/switcher
 */
gx.widgets.module('switcher', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		onState: '<span class="fa fa-check"></span>',
		offState: '<span class="fa fa-times"></span>',
		selector: 'input:checkbox'
	};

	/**
  * Final Options
  *
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Set the "checked" property of the single checkbox instances.
  *
  * This method will update the value and display of the widgets without triggering a "change" event.
  *
  * @param {Boolean} isChecked The checkbox values will be updated along with their representation.
  *
  * @return {jQuery} Returns the jQuery selector for chained calls.
  */
	function _checked(isChecked) {
		$(this).prop('checked', isChecked);
		_onCheckboxChange.call(this);
		return $(this);
	}

	/**
  * Add Public Module Methods
  *
  * Example: $('input:checkbox').switcher('checked', false);
  */
	function _addPublicMethod() {
		if ($.fn.switcher) {
			return; // Method is already registered.
		}

		$.fn.extend({
			switcher: function switcher(action) {
				switch (action) {
					case 'checked':
						for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
							args[_key - 1] = arguments[_key];
						}

						return _checked.apply(this, args);
				}
			}
		});
	}

	/**
  * On Switcher Click Event
  *
  * Delegate the click event to the checkbox elements which will update the DOM accordingly.
  *
  * @param {object} event
  */
	function _onSwitcherClick(event) {
		event.stopPropagation();

		if ($(this).hasClass('disabled')) {
			return false; // The switcher is disabled.
		}

		var $checkbox = $(this).find('input:checkbox');

		$checkbox.prop('checked', !$checkbox.prop('checked')).trigger('change');
	}

	/**
  * On Checkbox Change
  *
  * This callback will update the display of the widget. It will perform the required animations and set the
  * respective state classes.
  */
	function _onCheckboxChange() {
		var $checkbox = $(this);
		var $switcher = $checkbox.parent();

		if (!$switcher.hasClass('checked') && $checkbox.prop('checked')) {
			$switcher.addClass('checked');
		} else if ($switcher.hasClass('checked') && !$checkbox.prop('checked')) {
			$switcher.removeClass('checked');
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		_addPublicMethod();
		$this.find(options.selector).each(function () {
			var $checkbox = $(this);
			var title = $checkbox.prop('title') ? 'title="' + $checkbox.prop('title') + '"' : '';

			$checkbox.wrap('<div class="switcher" ' + title + '></div>').parent().append('\n\t\t\t\t\t<div class="switcher-toggler"></div>\n\t\t\t\t\t<div class="switcher-inner">\n\t\t\t\t\t\t<div class="switcher-state-on">' + options.onState + '</div>\n\t\t\t\t\t\t<div class="switcher-state-off">' + options.offState + '</div>\n\t\t\t\t\t</div>\n\t\t\t\t');

			// Bind the switcher event handlers.  
			$checkbox.parent().on('click', _onSwitcherClick).on('change', 'input:checkbox', _onCheckboxChange);

			// Trigger the change event to update the checkbox display.
			_onCheckboxChange.call($checkbox[0]);
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN3aXRjaGVyLmpzIl0sIm5hbWVzIjpbImd4Iiwid2lkZ2V0cyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9uU3RhdGUiLCJvZmZTdGF0ZSIsInNlbGVjdG9yIiwib3B0aW9ucyIsImV4dGVuZCIsIl9jaGVja2VkIiwiaXNDaGVja2VkIiwicHJvcCIsIl9vbkNoZWNrYm94Q2hhbmdlIiwiY2FsbCIsIl9hZGRQdWJsaWNNZXRob2QiLCJmbiIsInN3aXRjaGVyIiwiYWN0aW9uIiwiYXJncyIsImFwcGx5IiwiX29uU3dpdGNoZXJDbGljayIsImV2ZW50Iiwic3RvcFByb3BhZ2F0aW9uIiwiaGFzQ2xhc3MiLCIkY2hlY2tib3giLCJmaW5kIiwidHJpZ2dlciIsIiRzd2l0Y2hlciIsInBhcmVudCIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJpbml0IiwiZG9uZSIsImVhY2giLCJ0aXRsZSIsIndyYXAiLCJhcHBlbmQiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTRDQUEsR0FBR0MsT0FBSCxDQUFXQyxNQUFYLENBQWtCLFVBQWxCLEVBQThCLEVBQTlCLEVBQWtDLFVBQVVDLElBQVYsRUFBZ0I7O0FBRWpEOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsV0FBVztBQUNoQkMsV0FBUyxtQ0FETztBQUVoQkMsWUFBVSxtQ0FGTTtBQUdoQkMsWUFBVTtBQUhNLEVBQWpCOztBQU1BOzs7OztBQUtBLEtBQU1DLFVBQVVMLEVBQUVNLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkwsUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7OztBQVNBLFVBQVNVLFFBQVQsQ0FBa0JDLFNBQWxCLEVBQTZCO0FBQzVCUixJQUFFLElBQUYsRUFBUVMsSUFBUixDQUFhLFNBQWIsRUFBd0JELFNBQXhCO0FBQ0FFLG9CQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkI7QUFDQSxTQUFPWCxFQUFFLElBQUYsQ0FBUDtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNZLGdCQUFULEdBQTRCO0FBQzNCLE1BQUlaLEVBQUVhLEVBQUYsQ0FBS0MsUUFBVCxFQUFtQjtBQUNsQixVQURrQixDQUNWO0FBQ1I7O0FBRURkLElBQUVhLEVBQUYsQ0FBS1AsTUFBTCxDQUFZO0FBQ1hRLGFBQVUsa0JBQVVDLE1BQVYsRUFBMkI7QUFDM0IsWUFBUUEsTUFBUjtBQUNDLFVBQUssU0FBTDtBQUFBLHdDQUZvQkMsSUFFcEI7QUFGb0JBLFdBRXBCO0FBQUE7O0FBQ0MsYUFBT1QsU0FBU1UsS0FBVCxDQUFlLElBQWYsRUFBcUJELElBQXJCLENBQVA7QUFGRjtBQUlBO0FBTkMsR0FBWjtBQVFBOztBQUVEOzs7Ozs7O0FBT0EsVUFBU0UsZ0JBQVQsQ0FBMEJDLEtBQTFCLEVBQWlDO0FBQ2hDQSxRQUFNQyxlQUFOOztBQUVBLE1BQUlwQixFQUFFLElBQUYsRUFBUXFCLFFBQVIsQ0FBaUIsVUFBakIsQ0FBSixFQUFrQztBQUNqQyxVQUFPLEtBQVAsQ0FEaUMsQ0FDbkI7QUFDZDs7QUFFRCxNQUFNQyxZQUFZdEIsRUFBRSxJQUFGLEVBQVF1QixJQUFSLENBQWEsZ0JBQWIsQ0FBbEI7O0FBRUFELFlBQ0ViLElBREYsQ0FDTyxTQURQLEVBQ2tCLENBQUNhLFVBQVViLElBQVYsQ0FBZSxTQUFmLENBRG5CLEVBRUVlLE9BRkYsQ0FFVSxRQUZWO0FBR0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNkLGlCQUFULEdBQTZCO0FBQzVCLE1BQU1ZLFlBQVl0QixFQUFFLElBQUYsQ0FBbEI7QUFDQSxNQUFNeUIsWUFBWUgsVUFBVUksTUFBVixFQUFsQjs7QUFFQSxNQUFJLENBQUNELFVBQVVKLFFBQVYsQ0FBbUIsU0FBbkIsQ0FBRCxJQUFrQ0MsVUFBVWIsSUFBVixDQUFlLFNBQWYsQ0FBdEMsRUFBaUU7QUFDaEVnQixhQUFVRSxRQUFWLENBQW1CLFNBQW5CO0FBQ0EsR0FGRCxNQUVPLElBQUlGLFVBQVVKLFFBQVYsQ0FBbUIsU0FBbkIsS0FBaUMsQ0FBQ0MsVUFBVWIsSUFBVixDQUFlLFNBQWYsQ0FBdEMsRUFBaUU7QUFDdkVnQixhQUFVRyxXQUFWLENBQXNCLFNBQXRCO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O0FBRUEvQixRQUFPZ0MsSUFBUCxHQUFjLFVBQVVDLElBQVYsRUFBZ0I7QUFDN0JsQjtBQUNBYixRQUFNd0IsSUFBTixDQUFXbEIsUUFBUUQsUUFBbkIsRUFBNkIyQixJQUE3QixDQUFrQyxZQUFZO0FBQzdDLE9BQU1ULFlBQVl0QixFQUFFLElBQUYsQ0FBbEI7QUFDQSxPQUFNZ0MsUUFBUVYsVUFBVWIsSUFBVixDQUFlLE9BQWYsZ0JBQW9DYSxVQUFVYixJQUFWLENBQWUsT0FBZixDQUFwQyxTQUFpRSxFQUEvRTs7QUFFQWEsYUFDRVcsSUFERiw0QkFDZ0NELEtBRGhDLGNBRUVOLE1BRkYsR0FHRVEsTUFIRiwySUFNb0M3QixRQUFRSCxPQU41Qyw0REFPcUNHLFFBQVFGLFFBUDdDOztBQVdBO0FBQ0FtQixhQUNFSSxNQURGLEdBRUVTLEVBRkYsQ0FFSyxPQUZMLEVBRWNqQixnQkFGZCxFQUdFaUIsRUFIRixDQUdLLFFBSEwsRUFHZSxnQkFIZixFQUdpQ3pCLGlCQUhqQzs7QUFLQTtBQUNBQSxxQkFBa0JDLElBQWxCLENBQXVCVyxVQUFVLENBQVYsQ0FBdkI7QUFDQSxHQXZCRDs7QUF5QkFRO0FBQ0EsRUE1QkQ7O0FBOEJBLFFBQU9qQyxNQUFQO0FBRUEsQ0F6SkQiLCJmaWxlIjoic3dpdGNoZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gc3dpdGNoZXIuanMgMjAxNy0xMS0yMVxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiAjIyBTd2l0Y2hlciBXaWRnZXRcclxuICpcclxuICogVGhpcyB3aWRnZXQgb3JpZ2luYXRlcyBmcm9tIHRoZSBcInN3aXRjaGVyXCIgbW9kZSBvZiB0aGUgZXhpc3RpbmcgY2hlY2tib3ggd2lkZ2V0LiBCZWNhdXNlIG9mIHRoZSBpbmNyZWFzZWRcclxuICogY29tcGxleGl0eSBvZiB0aGUgb2xkIHdpZGdldCBjb2RlLCB0aGUgc3dpdGNoZXIgbW9kZSBpcyBub3cgc2VydmVyIGJ5IHRoaXMgZmlsZS4gQXBwbHkgdGhlIHdpZGdldCBpbiBhIHBhcmVudFxyXG4gKiBjb250YWluZXIgYW5kIGl0IHdpbGwgc2VhcmNoIGFuZCBjb252ZXJ0IGFsbCB0aGUgY2hlY2tib3gvcmFkaW8gaW5zdGFuY2VzIGludG8gc3dpdGNoZXJzLlxyXG4gKlxyXG4gKiAjIyMgT3B0aW9uc1xyXG4gKlxyXG4gKiAqKk9uIFN0YXRlIHwgYGRhdGEtc3dpdGNoZXItb24tc3RhdGVgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxyXG4gKlxyXG4gKiBEZWZpbmUgdGhlIGNvbnRlbnQgb2YgdGhlIFwib25cIiBzdGF0ZS5cclxuICpcclxuICogKipPZmYgU3RhdGUgfCBgZGF0YS1zd2l0Y2hlci1vZmYtc3RhdGVgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxyXG4gKlxyXG4gKiBEZWZpbmUgdGhlIGNvbnRlbnQgb2YgdGhlIFwib2ZmXCIgc3RhdGUuXHJcbiAqXHJcbiAqICoqU2VsZWN0b3IgfCBgZGF0YS1zd2l0Y2hlci1zZWxlY3RvcmAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXHJcbiAqXHJcbiAqIFNldCB0aGUgc2VsZWN0b3Igb2YgdGhlIGNoZWNrYm94ZXMgdG8gYmUgY29udmVydGVkIHRvIHN3aXRjaGVyIGluc3RhbmNlcy4gSXQgZGVmYXVsdHMgdG8gKippbnB1dDpjaGVja2JveCoqLlxyXG4gKlxyXG4gKiAjIyMgTWV0aG9kc1xyXG4gKlxyXG4gKiAqKkNoZWNrZWQqKlxyXG4gKlxyXG4gKiBgYGBqc1xyXG4gKiAvLyBTZXQgdGhlIGNoZWNrZWQgdmFsdWUgb2YgdGhlIHNpbmdsZSBjaGVja2JveCBzZWxlY3Rpb24gKG5vIGNoYW5nZSBldmVudCB3aWxsIGJlIHRyaWdnZXJlZCEpLlxyXG4gKiAkKCd0YWJsZSBpbnB1dDpjaGVja2JveCcpLnN3aXRjaGVyKCdjaGVja2VkJywgdHJ1ZSk7XHJcbiAqIGBgYFxyXG4gKlxyXG4gKiAjIyMgRXhhbXBsZXNcclxuICpcclxuICogSW4gdGhlIGZvbGxvd2luZyBleGFtcGxlIHRoZSBjaGVja2JveCBlbGVtZW50IHdpbGwgYmUgY29udmVydGVkIGludG8gYSBzaW5nbGUtY2hlY2tib3ggaW5zdGFuY2UuXHJcbiAqXHJcbiAqIGBgYGh0bWxcclxuICogPGRpdiBjbGFzcz1cIndyYXBwZXJcIiBkYXRhLWd4LXdpZGdldD1cInN3aXRjaGVyXCI+XHJcbiAqICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIC8+XHJcbiAqIDwvZGl2PlxyXG4gKiBgYGBcclxuICpcclxuICogQHRvZG8gQWRkIG1ldGhvZCBmb3IgZGlzYWJsaW5nIHRoZSBzd2l0Y2hlciB3aWRnZXQgKGUuZy4gJCgnI215LXN3aXRjaGVyJykuc3dpdGNoZXIoJ2Rpc2FibGVkJywgdHJ1ZSkpO1xyXG4gKlxyXG4gKiBAbW9kdWxlIEFkbWluL1dpZGdldHMvc3dpdGNoZXJcclxuICovXHJcbmd4LndpZGdldHMubW9kdWxlKCdzd2l0Y2hlcicsIFtdLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdFxyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBWQVJJQUJMRVNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIERlZmF1bHQgT3B0aW9uc1xyXG5cdCAqXHJcblx0ICogQHR5cGUge09iamVjdH1cclxuXHQgKi9cclxuXHRjb25zdCBkZWZhdWx0cyA9IHtcclxuXHRcdG9uU3RhdGU6ICc8c3BhbiBjbGFzcz1cImZhIGZhLWNoZWNrXCI+PC9zcGFuPicsXHJcblx0XHRvZmZTdGF0ZTogJzxzcGFuIGNsYXNzPVwiZmEgZmEtdGltZXNcIj48L3NwYW4+JyxcclxuXHRcdHNlbGVjdG9yOiAnaW5wdXQ6Y2hlY2tib3gnXHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBGaW5hbCBPcHRpb25zXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdCAqXHJcblx0ICogQHR5cGUge09iamVjdH1cclxuXHQgKi9cclxuXHRjb25zdCBtb2R1bGUgPSB7fTtcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBGVU5DVElPTlNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBTZXQgdGhlIFwiY2hlY2tlZFwiIHByb3BlcnR5IG9mIHRoZSBzaW5nbGUgY2hlY2tib3ggaW5zdGFuY2VzLlxyXG5cdCAqXHJcblx0ICogVGhpcyBtZXRob2Qgd2lsbCB1cGRhdGUgdGhlIHZhbHVlIGFuZCBkaXNwbGF5IG9mIHRoZSB3aWRnZXRzIHdpdGhvdXQgdHJpZ2dlcmluZyBhIFwiY2hhbmdlXCIgZXZlbnQuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge0Jvb2xlYW59IGlzQ2hlY2tlZCBUaGUgY2hlY2tib3ggdmFsdWVzIHdpbGwgYmUgdXBkYXRlZCBhbG9uZyB3aXRoIHRoZWlyIHJlcHJlc2VudGF0aW9uLlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7alF1ZXJ5fSBSZXR1cm5zIHRoZSBqUXVlcnkgc2VsZWN0b3IgZm9yIGNoYWluZWQgY2FsbHMuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX2NoZWNrZWQoaXNDaGVja2VkKSB7XHJcblx0XHQkKHRoaXMpLnByb3AoJ2NoZWNrZWQnLCBpc0NoZWNrZWQpO1xyXG5cdFx0X29uQ2hlY2tib3hDaGFuZ2UuY2FsbCh0aGlzKTtcclxuXHRcdHJldHVybiAkKHRoaXMpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBBZGQgUHVibGljIE1vZHVsZSBNZXRob2RzXHJcblx0ICpcclxuXHQgKiBFeGFtcGxlOiAkKCdpbnB1dDpjaGVja2JveCcpLnN3aXRjaGVyKCdjaGVja2VkJywgZmFsc2UpO1xyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9hZGRQdWJsaWNNZXRob2QoKSB7XHJcblx0XHRpZiAoJC5mbi5zd2l0Y2hlcikge1xyXG5cdFx0XHRyZXR1cm47IC8vIE1ldGhvZCBpcyBhbHJlYWR5IHJlZ2lzdGVyZWQuXHJcblx0XHR9XHJcblx0XHRcclxuXHRcdCQuZm4uZXh0ZW5kKHtcclxuXHRcdFx0c3dpdGNoZXI6IGZ1bmN0aW9uIChhY3Rpb24sIC4uLmFyZ3MpIHtcclxuXHQgICAgICAgICAgICBzd2l0Y2ggKGFjdGlvbikge1xyXG5cdFx0ICAgICAgICAgICAgY2FzZSAnY2hlY2tlZCc6XHJcblx0XHRcdCAgICAgICAgICAgIHJldHVybiBfY2hlY2tlZC5hcHBseSh0aGlzLCBhcmdzKTtcclxuXHQgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogT24gU3dpdGNoZXIgQ2xpY2sgRXZlbnRcclxuXHQgKlxyXG5cdCAqIERlbGVnYXRlIHRoZSBjbGljayBldmVudCB0byB0aGUgY2hlY2tib3ggZWxlbWVudHMgd2hpY2ggd2lsbCB1cGRhdGUgdGhlIERPTSBhY2NvcmRpbmdseS5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudFxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vblN3aXRjaGVyQ2xpY2soZXZlbnQpIHtcclxuXHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cdFx0XHJcblx0XHRpZiAoJCh0aGlzKS5oYXNDbGFzcygnZGlzYWJsZWQnKSkge1xyXG5cdFx0XHRyZXR1cm4gZmFsc2U7IC8vIFRoZSBzd2l0Y2hlciBpcyBkaXNhYmxlZC5cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Y29uc3QgJGNoZWNrYm94ID0gJCh0aGlzKS5maW5kKCdpbnB1dDpjaGVja2JveCcpO1xyXG5cdFx0XHJcblx0XHQkY2hlY2tib3hcclxuXHRcdFx0LnByb3AoJ2NoZWNrZWQnLCAhJGNoZWNrYm94LnByb3AoJ2NoZWNrZWQnKSlcclxuXHRcdFx0LnRyaWdnZXIoJ2NoYW5nZScpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBDaGVja2JveCBDaGFuZ2VcclxuXHQgKlxyXG5cdCAqIFRoaXMgY2FsbGJhY2sgd2lsbCB1cGRhdGUgdGhlIGRpc3BsYXkgb2YgdGhlIHdpZGdldC4gSXQgd2lsbCBwZXJmb3JtIHRoZSByZXF1aXJlZCBhbmltYXRpb25zIGFuZCBzZXQgdGhlXHJcblx0ICogcmVzcGVjdGl2ZSBzdGF0ZSBjbGFzc2VzLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vbkNoZWNrYm94Q2hhbmdlKCkge1xyXG5cdFx0Y29uc3QgJGNoZWNrYm94ID0gJCh0aGlzKTtcclxuXHRcdGNvbnN0ICRzd2l0Y2hlciA9ICRjaGVja2JveC5wYXJlbnQoKTtcclxuXHRcdFxyXG5cdFx0aWYgKCEkc3dpdGNoZXIuaGFzQ2xhc3MoJ2NoZWNrZWQnKSAmJiAkY2hlY2tib3gucHJvcCgnY2hlY2tlZCcpKSB7XHJcblx0XHRcdCRzd2l0Y2hlci5hZGRDbGFzcygnY2hlY2tlZCcpO1xyXG5cdFx0fSBlbHNlIGlmICgkc3dpdGNoZXIuaGFzQ2xhc3MoJ2NoZWNrZWQnKSAmJiAhJGNoZWNrYm94LnByb3AoJ2NoZWNrZWQnKSkge1xyXG5cdFx0XHQkc3dpdGNoZXIucmVtb3ZlQ2xhc3MoJ2NoZWNrZWQnKTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gSU5JVElBTElaQVRJT05cclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uIChkb25lKSB7XHJcblx0XHRfYWRkUHVibGljTWV0aG9kKCk7XHJcblx0XHQkdGhpcy5maW5kKG9wdGlvbnMuc2VsZWN0b3IpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRjb25zdCAkY2hlY2tib3ggPSAkKHRoaXMpO1xyXG5cdFx0XHRjb25zdCB0aXRsZSA9ICRjaGVja2JveC5wcm9wKCd0aXRsZScpID8gYHRpdGxlPVwiJHskY2hlY2tib3gucHJvcCgndGl0bGUnKX1cImAgOiAnJztcclxuXHRcdFx0XHJcblx0XHRcdCRjaGVja2JveFxyXG5cdFx0XHRcdC53cmFwKGA8ZGl2IGNsYXNzPVwic3dpdGNoZXJcIiAke3RpdGxlfT48L2Rpdj5gKVxyXG5cdFx0XHRcdC5wYXJlbnQoKVxyXG5cdFx0XHRcdC5hcHBlbmQoYFxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cInN3aXRjaGVyLXRvZ2dsZXJcIj48L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJzd2l0Y2hlci1pbm5lclwiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwic3dpdGNoZXItc3RhdGUtb25cIj4ke29wdGlvbnMub25TdGF0ZX08L2Rpdj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInN3aXRjaGVyLXN0YXRlLW9mZlwiPiR7b3B0aW9ucy5vZmZTdGF0ZX08L2Rpdj5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdGApO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gQmluZCB0aGUgc3dpdGNoZXIgZXZlbnQgaGFuZGxlcnMuICBcclxuXHRcdFx0JGNoZWNrYm94XHJcblx0XHRcdFx0LnBhcmVudCgpXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsIF9vblN3aXRjaGVyQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjaGFuZ2UnLCAnaW5wdXQ6Y2hlY2tib3gnLCBfb25DaGVja2JveENoYW5nZSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBUcmlnZ2VyIHRoZSBjaGFuZ2UgZXZlbnQgdG8gdXBkYXRlIHRoZSBjaGVja2JveCBkaXNwbGF5LlxyXG5cdFx0XHRfb25DaGVja2JveENoYW5nZS5jYWxsKCRjaGVja2JveFswXSk7XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0ZG9uZSgpO1xyXG5cdH07XHJcblx0XHJcblx0cmV0dXJuIG1vZHVsZTtcclxuXHRcclxufSk7ICJdfQ==
