'use strict';

/* --------------------------------------------------------------
 statistic_chart.js 2016-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Statistic Chart Widget
 *
 * Widget for showing statistics
 *
 * Markup:
 * ```html
 * <div
 *     id="dashboard-chart"
 *     data-gx-widget="statistic_chart"
 *     data-statistic_chart-user-id="1"
 * ></div>
 * ```
 * Data-Attributes:
 * - `data-statistic_chart-user-id` is the userId from current logged in user
 *
 * Events:
 * - `get:data` loads the data from server (requires parameter)
 *
 * Example:
 * ```js
 * $('#dashboard-chart').trigger('get:data', {
        item: 'orders,' //  which statistic
 *      interval: 'today' // which value
 * });
 * ```
 * Retrieve data from server
 *
 * ```js
 * {
 *     item            : 'orders'  // Passed in via event trigger
 *     interval        : 1231232,  // Passed in via event trigger
 *     userId          : 1,        // Passed in via data attribute
 * }
 * ```
 *
 * The data returned from server should look like this:
 *
 * ```js
 * [
 *     { period: '2008', amount: 20 },
 *     { period: '2009', amount: 10 }
 * ]
 * ```
 *
 * @module Admin/Widgets/statistic_chart
 * @requires jQueryUI-Library
 * @ignore
 */
gx.widgets.module(
// Module name
'statistic_chart',

// Dependencies
[jse.source + '/vendor/raphael/raphael.js', jse.source + '/vendor/morris.js/morris.min.css', jse.source + '/vendor/morris.js/morris.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	// Widget Reference

	var $this = $(this);

	// User ID
	var myUserId = data.userId;

	// Item dropdown
	var $itemDropdown = $('.statistic-chart-dropdown');

	/**
  * Map item related values
  * Each items contains key values for use in this widget
  * @type {object}
  */
	var itemMap = {

		// Sales (Umsatz)
		sales: {
			apiUrl: './admin.php?do=Dashboard/getSalesStatisticsData',
			title: jse.core.lang.translate('STATISTICS_SALES', 'start')
		},

		// Visitors (Besucher)
		visitors: {
			apiUrl: './admin.php?do=Dashboard/getVisitorsStatisticsData',
			title: jse.core.lang.translate('STATISTICS_VISITORS', 'start')
		},

		// New Customers (Neue Kunden)
		newCustomers: {
			apiUrl: './admin.php?do=Dashboard/getNewCustomerStatisticsData',
			title: jse.core.lang.translate('STATISTICS_NEW_CUSTOMERS', 'start')
		},

		// Orders (Bestellungen)
		orders: {
			apiUrl: './admin.php?do=Dashboard/getOrderStatisticsData',
			title: jse.core.lang.translate('STATISTICS_ORDERS_COUNT', 'start')
		}
	};

	// Meta Object
	var module = {};

	// ------------------------------------------------------------------------
	// LOADING STATE
	// ------------------------------------------------------------------------

	/**
  * Turns on/off loading state
  * @param {boolean} isLoading - If true, the loading state will be triggered
  * @private
  */
	var _toggleLoading = function _toggleLoading(isLoading) {
		// Existant spinner element
		var $existantSpinner = $this.find('.loader');
		var isSpinnerAlreadyExists = $existantSpinner.length;

		// New spinner element
		var spinnerClass = 'loader fa fa-fw fa-spinner fa-spin';
		var $newSpinner = $('<i class="' + spinnerClass + '"></i>');

		// Look for existant spinner element and remove it
		if (isSpinnerAlreadyExists) {
			$existantSpinner.remove();
		}

		// Show new one if 'isLoading' argument is true
		if (isLoading) {
			$this.append($newSpinner);
		}

		return;
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Retrieve data from server and builds the chart
  * The server request parameters should look like this:
  * @requires MorrisJS
  * @param {Event | jQuery} event
  * @param {string} interval
  * @param {string} [item] - If undefined, it will get value from dropdown
  * @private
  */
	var _buildChart = function _buildChart(event, interval, item) {

		// Clear element
		$this.empty();

		// Get item value from dropdown if not passed via argument
		if (!item) {
			item = $itemDropdown.find('option:selected').val();
		}

		// Throw error if item is not defined in map
		if (item && item in itemMap === false) {
			throw new Error('Invalid item!');
		}

		// Show loading spinner
		_toggleLoading(true);

		// Perform Request
		var request = $.ajax({
			url: itemMap[item].apiUrl,
			type: 'GET',
			dataType: 'JSON',
			data: {
				userId: myUserId,
				item: item,
				interval: interval
			}
		});

		// On success
		request.done(function (response) {

			// Hide loading spinner
			_toggleLoading(false);

			$this.empty();

			$.each(response.data, function () {
				this.amount = parseInt(this.amount);
			});

			// Draw chart
			Morris.Area({
				element: $this,
				data: response.data,
				xkey: 'period',
				ykeys: ['amount'],
				xLabels: response.type,
				labels: [itemMap[item].title],
				lineWidth: 2,
				eventStrokeWidth: 1,
				goalStrokeWidth: 1,
				fillOpacity: 0.25,
				behaveLikeLine: true,
				hideHover: 'auto',
				lineColors: ['#2196F3'],
				dateFormat: function dateFormat(timestamp) {
					var date = new Date(timestamp);
					var day = date.getDate().toString();
					var month = (date.getMonth() + 1).toString();
					var year = date.getFullYear().toString();
					return (day[1] ? day : '0' + day[0]) + '.' + (month[1] ? month : '0' + month[0]) + '.' + year;
				}
			});
		});
	};

	// Initialize method of the widget, called by the engine.
	module.init = function (done) {
		// Delegate event
		$this.on('get:data', _buildChart);
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0YXRpc3RpY19jaGFydC5qcyJdLCJuYW1lcyI6WyJneCIsIndpZGdldHMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwibXlVc2VySWQiLCJ1c2VySWQiLCIkaXRlbURyb3Bkb3duIiwiaXRlbU1hcCIsInNhbGVzIiwiYXBpVXJsIiwidGl0bGUiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsInZpc2l0b3JzIiwibmV3Q3VzdG9tZXJzIiwib3JkZXJzIiwiX3RvZ2dsZUxvYWRpbmciLCJpc0xvYWRpbmciLCIkZXhpc3RhbnRTcGlubmVyIiwiZmluZCIsImlzU3Bpbm5lckFscmVhZHlFeGlzdHMiLCJsZW5ndGgiLCJzcGlubmVyQ2xhc3MiLCIkbmV3U3Bpbm5lciIsInJlbW92ZSIsImFwcGVuZCIsIl9idWlsZENoYXJ0IiwiZXZlbnQiLCJpbnRlcnZhbCIsIml0ZW0iLCJlbXB0eSIsInZhbCIsIkVycm9yIiwicmVxdWVzdCIsImFqYXgiLCJ1cmwiLCJ0eXBlIiwiZGF0YVR5cGUiLCJkb25lIiwicmVzcG9uc2UiLCJlYWNoIiwiYW1vdW50IiwicGFyc2VJbnQiLCJNb3JyaXMiLCJBcmVhIiwiZWxlbWVudCIsInhrZXkiLCJ5a2V5cyIsInhMYWJlbHMiLCJsYWJlbHMiLCJsaW5lV2lkdGgiLCJldmVudFN0cm9rZVdpZHRoIiwiZ29hbFN0cm9rZVdpZHRoIiwiZmlsbE9wYWNpdHkiLCJiZWhhdmVMaWtlTGluZSIsImhpZGVIb3ZlciIsImxpbmVDb2xvcnMiLCJkYXRlRm9ybWF0IiwidGltZXN0YW1wIiwiZGF0ZSIsIkRhdGUiLCJkYXkiLCJnZXREYXRlIiwidG9TdHJpbmciLCJtb250aCIsImdldE1vbnRoIiwieWVhciIsImdldEZ1bGxZZWFyIiwiaW5pdCIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpREFBLEdBQUdDLE9BQUgsQ0FBV0MsTUFBWDtBQUNDO0FBQ0EsaUJBRkQ7O0FBSUM7QUFDQSxDQUNJQyxJQUFJQyxNQURSLGlDQUVJRCxJQUFJQyxNQUZSLHVDQUdJRCxJQUFJQyxNQUhSLHFDQUxELEVBV0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFDQSxLQUFJQyxRQUFRQyxFQUFFLElBQUYsQ0FBWjs7QUFFQTtBQUNBLEtBQUlDLFdBQVdILEtBQUtJLE1BQXBCOztBQUVBO0FBQ0EsS0FBSUMsZ0JBQWdCSCxFQUFFLDJCQUFGLENBQXBCOztBQUVBOzs7OztBQUtBLEtBQUlJLFVBQVU7O0FBRWI7QUFDQUMsU0FBTztBQUNOQyxXQUFRLGlEQURGO0FBRU5DLFVBQU9YLElBQUlZLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGtCQUF4QixFQUE0QyxPQUE1QztBQUZELEdBSE07O0FBUWI7QUFDQUMsWUFBVTtBQUNUTCxXQUFRLG9EQURDO0FBRVRDLFVBQU9YLElBQUlZLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHFCQUF4QixFQUErQyxPQUEvQztBQUZFLEdBVEc7O0FBY2I7QUFDQUUsZ0JBQWM7QUFDYk4sV0FBUSx1REFESztBQUViQyxVQUFPWCxJQUFJWSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwwQkFBeEIsRUFBb0QsT0FBcEQ7QUFGTSxHQWZEOztBQW9CYjtBQUNBRyxVQUFRO0FBQ1BQLFdBQVEsaURBREQ7QUFFUEMsVUFBT1gsSUFBSVksSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IseUJBQXhCLEVBQW1ELE9BQW5EO0FBRkE7QUFyQkssRUFBZDs7QUEyQkE7QUFDQSxLQUFJZixTQUFTLEVBQWI7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLEtBQUltQixpQkFBaUIsU0FBakJBLGNBQWlCLENBQVNDLFNBQVQsRUFBb0I7QUFDeEM7QUFDQSxNQUFJQyxtQkFBbUJqQixNQUFNa0IsSUFBTixDQUFXLFNBQVgsQ0FBdkI7QUFDQSxNQUFJQyx5QkFBeUJGLGlCQUFpQkcsTUFBOUM7O0FBRUE7QUFDQSxNQUFJQyxlQUFlLG9DQUFuQjtBQUNBLE1BQUlDLGNBQWNyQixFQUFFLGVBQWVvQixZQUFmLEdBQThCLFFBQWhDLENBQWxCOztBQUVBO0FBQ0EsTUFBSUYsc0JBQUosRUFBNEI7QUFDM0JGLG9CQUFpQk0sTUFBakI7QUFDQTs7QUFFRDtBQUNBLE1BQUlQLFNBQUosRUFBZTtBQUNkaEIsU0FBTXdCLE1BQU4sQ0FBYUYsV0FBYjtBQUNBOztBQUVEO0FBQ0EsRUFwQkQ7O0FBc0JBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7O0FBU0EsS0FBSUcsY0FBYyxTQUFkQSxXQUFjLENBQVNDLEtBQVQsRUFBZ0JDLFFBQWhCLEVBQTBCQyxJQUExQixFQUFnQzs7QUFFakQ7QUFDQTVCLFFBQU02QixLQUFOOztBQUVBO0FBQ0EsTUFBSSxDQUFDRCxJQUFMLEVBQVc7QUFDVkEsVUFBT3hCLGNBQWNjLElBQWQsQ0FBbUIsaUJBQW5CLEVBQXNDWSxHQUF0QyxFQUFQO0FBQ0E7O0FBRUQ7QUFDQSxNQUFJRixRQUFRQSxRQUFRdkIsT0FBUixLQUFvQixLQUFoQyxFQUF1QztBQUN0QyxTQUFNLElBQUkwQixLQUFKLENBQVUsZUFBVixDQUFOO0FBQ0E7O0FBRUQ7QUFDQWhCLGlCQUFlLElBQWY7O0FBRUE7QUFDQSxNQUFJaUIsVUFBVS9CLEVBQUVnQyxJQUFGLENBQU87QUFDcEJDLFFBQUs3QixRQUFRdUIsSUFBUixFQUFjckIsTUFEQztBQUVwQjRCLFNBQU0sS0FGYztBQUdwQkMsYUFBVSxNQUhVO0FBSXBCckMsU0FBTTtBQUNMSSxZQUFRRCxRQURIO0FBRUwwQixVQUFNQSxJQUZEO0FBR0xELGNBQVVBO0FBSEw7QUFKYyxHQUFQLENBQWQ7O0FBV0E7QUFDQUssVUFBUUssSUFBUixDQUFhLFVBQVNDLFFBQVQsRUFBbUI7O0FBRS9CO0FBQ0F2QixrQkFBZSxLQUFmOztBQUVBZixTQUFNNkIsS0FBTjs7QUFFQTVCLEtBQUVzQyxJQUFGLENBQU9ELFNBQVN2QyxJQUFoQixFQUFzQixZQUFXO0FBQ2hDLFNBQUt5QyxNQUFMLEdBQWNDLFNBQVMsS0FBS0QsTUFBZCxDQUFkO0FBQ0EsSUFGRDs7QUFJQTtBQUNBRSxVQUFPQyxJQUFQLENBQVk7QUFDWEMsYUFBUzVDLEtBREU7QUFFWEQsVUFBTXVDLFNBQVN2QyxJQUZKO0FBR1g4QyxVQUFNLFFBSEs7QUFJWEMsV0FBTyxDQUFDLFFBQUQsQ0FKSTtBQUtYQyxhQUFTVCxTQUFTSCxJQUxQO0FBTVhhLFlBQVEsQ0FBQzNDLFFBQVF1QixJQUFSLEVBQWNwQixLQUFmLENBTkc7QUFPWHlDLGVBQVcsQ0FQQTtBQVFYQyxzQkFBa0IsQ0FSUDtBQVNYQyxxQkFBaUIsQ0FUTjtBQVVYQyxpQkFBYSxJQVZGO0FBV1hDLG9CQUFnQixJQVhMO0FBWVhDLGVBQVcsTUFaQTtBQWFYQyxnQkFBWSxDQUFDLFNBQUQsQ0FiRDtBQWNYQyxnQkFBWSxvQkFBU0MsU0FBVCxFQUFvQjtBQUMvQixTQUFJQyxPQUFPLElBQUlDLElBQUosQ0FBU0YsU0FBVCxDQUFYO0FBQ0EsU0FBSUcsTUFBTUYsS0FBS0csT0FBTCxHQUFlQyxRQUFmLEVBQVY7QUFDQSxTQUFJQyxRQUFRLENBQUNMLEtBQUtNLFFBQUwsS0FBa0IsQ0FBbkIsRUFBc0JGLFFBQXRCLEVBQVo7QUFDQSxTQUFJRyxPQUFPUCxLQUFLUSxXQUFMLEdBQW1CSixRQUFuQixFQUFYO0FBQ0EsWUFBTyxDQUFDRixJQUFJLENBQUosSUFBU0EsR0FBVCxHQUFlLE1BQU1BLElBQUksQ0FBSixDQUF0QixJQUFnQyxHQUFoQyxJQUNMRyxNQUFNLENBQU4sSUFBV0EsS0FBWCxHQUFtQixNQUFNQSxNQUFNLENBQU4sQ0FEcEIsSUFDZ0MsR0FEaEMsR0FDc0NFLElBRDdDO0FBRUE7QUFyQlUsSUFBWjtBQXVCQSxHQW5DRDtBQW9DQSxFQW5FRDs7QUFxRUE7QUFDQXJFLFFBQU91RSxJQUFQLEdBQWMsVUFBUzlCLElBQVQsRUFBZTtBQUM1QjtBQUNBckMsUUFBTW9FLEVBQU4sQ0FBUyxVQUFULEVBQXFCM0MsV0FBckI7QUFDQVk7QUFDQSxFQUpEOztBQU1BO0FBQ0EsUUFBT3pDLE1BQVA7QUFDQSxDQXpMRiIsImZpbGUiOiJzdGF0aXN0aWNfY2hhcnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHN0YXRpc3RpY19jaGFydC5qcyAyMDE2LTAyLTIzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBTdGF0aXN0aWMgQ2hhcnQgV2lkZ2V0XG4gKlxuICogV2lkZ2V0IGZvciBzaG93aW5nIHN0YXRpc3RpY3NcbiAqXG4gKiBNYXJrdXA6XG4gKiBgYGBodG1sXG4gKiA8ZGl2XG4gKiAgICAgaWQ9XCJkYXNoYm9hcmQtY2hhcnRcIlxuICogICAgIGRhdGEtZ3gtd2lkZ2V0PVwic3RhdGlzdGljX2NoYXJ0XCJcbiAqICAgICBkYXRhLXN0YXRpc3RpY19jaGFydC11c2VyLWlkPVwiMVwiXG4gKiA+PC9kaXY+XG4gKiBgYGBcbiAqIERhdGEtQXR0cmlidXRlczpcbiAqIC0gYGRhdGEtc3RhdGlzdGljX2NoYXJ0LXVzZXItaWRgIGlzIHRoZSB1c2VySWQgZnJvbSBjdXJyZW50IGxvZ2dlZCBpbiB1c2VyXG4gKlxuICogRXZlbnRzOlxuICogLSBgZ2V0OmRhdGFgIGxvYWRzIHRoZSBkYXRhIGZyb20gc2VydmVyIChyZXF1aXJlcyBwYXJhbWV0ZXIpXG4gKlxuICogRXhhbXBsZTpcbiAqIGBgYGpzXG4gKiAkKCcjZGFzaGJvYXJkLWNoYXJ0JykudHJpZ2dlcignZ2V0OmRhdGEnLCB7XG4gICAgICAgIGl0ZW06ICdvcmRlcnMsJyAvLyAgd2hpY2ggc3RhdGlzdGljXG4gKiAgICAgIGludGVydmFsOiAndG9kYXknIC8vIHdoaWNoIHZhbHVlXG4gKiB9KTtcbiAqIGBgYFxuICogUmV0cmlldmUgZGF0YSBmcm9tIHNlcnZlclxuICpcbiAqIGBgYGpzXG4gKiB7XG4gKiAgICAgaXRlbSAgICAgICAgICAgIDogJ29yZGVycycgIC8vIFBhc3NlZCBpbiB2aWEgZXZlbnQgdHJpZ2dlclxuICogICAgIGludGVydmFsICAgICAgICA6IDEyMzEyMzIsICAvLyBQYXNzZWQgaW4gdmlhIGV2ZW50IHRyaWdnZXJcbiAqICAgICB1c2VySWQgICAgICAgICAgOiAxLCAgICAgICAgLy8gUGFzc2VkIGluIHZpYSBkYXRhIGF0dHJpYnV0ZVxuICogfVxuICogYGBgXG4gKlxuICogVGhlIGRhdGEgcmV0dXJuZWQgZnJvbSBzZXJ2ZXIgc2hvdWxkIGxvb2sgbGlrZSB0aGlzOlxuICpcbiAqIGBgYGpzXG4gKiBbXG4gKiAgICAgeyBwZXJpb2Q6ICcyMDA4JywgYW1vdW50OiAyMCB9LFxuICogICAgIHsgcGVyaW9kOiAnMjAwOScsIGFtb3VudDogMTAgfVxuICogXVxuICogYGBgXG4gKlxuICogQG1vZHVsZSBBZG1pbi9XaWRnZXRzL3N0YXRpc3RpY19jaGFydFxuICogQHJlcXVpcmVzIGpRdWVyeVVJLUxpYnJhcnlcbiAqIEBpZ25vcmVcbiAqL1xuZ3gud2lkZ2V0cy5tb2R1bGUoXG5cdC8vIE1vZHVsZSBuYW1lXG5cdCdzdGF0aXN0aWNfY2hhcnQnLFxuXHRcblx0Ly8gRGVwZW5kZW5jaWVzXG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvcmFwaGFlbC9yYXBoYWVsLmpzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvbW9ycmlzLmpzL21vcnJpcy5taW4uY3NzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvbW9ycmlzLmpzL21vcnJpcy5taW4uanNgLFxuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0Ly8gV2lkZ2V0IFJlZmVyZW5jZVxuXHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0Ly8gVXNlciBJRFxuXHRcdHZhciBteVVzZXJJZCA9IGRhdGEudXNlcklkO1xuXHRcdFxuXHRcdC8vIEl0ZW0gZHJvcGRvd25cblx0XHR2YXIgJGl0ZW1Ecm9wZG93biA9ICQoJy5zdGF0aXN0aWMtY2hhcnQtZHJvcGRvd24nKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNYXAgaXRlbSByZWxhdGVkIHZhbHVlc1xuXHRcdCAqIEVhY2ggaXRlbXMgY29udGFpbnMga2V5IHZhbHVlcyBmb3IgdXNlIGluIHRoaXMgd2lkZ2V0XG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHR2YXIgaXRlbU1hcCA9IHtcblx0XHRcdFxuXHRcdFx0Ly8gU2FsZXMgKFVtc2F0eilcblx0XHRcdHNhbGVzOiB7XG5cdFx0XHRcdGFwaVVybDogJy4vYWRtaW4ucGhwP2RvPURhc2hib2FyZC9nZXRTYWxlc1N0YXRpc3RpY3NEYXRhJyxcblx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTVEFUSVNUSUNTX1NBTEVTJywgJ3N0YXJ0Jylcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8vIFZpc2l0b3JzIChCZXN1Y2hlcilcblx0XHRcdHZpc2l0b3JzOiB7XG5cdFx0XHRcdGFwaVVybDogJy4vYWRtaW4ucGhwP2RvPURhc2hib2FyZC9nZXRWaXNpdG9yc1N0YXRpc3RpY3NEYXRhJyxcblx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTVEFUSVNUSUNTX1ZJU0lUT1JTJywgJ3N0YXJ0Jylcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8vIE5ldyBDdXN0b21lcnMgKE5ldWUgS3VuZGVuKVxuXHRcdFx0bmV3Q3VzdG9tZXJzOiB7XG5cdFx0XHRcdGFwaVVybDogJy4vYWRtaW4ucGhwP2RvPURhc2hib2FyZC9nZXROZXdDdXN0b21lclN0YXRpc3RpY3NEYXRhJyxcblx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdTVEFUSVNUSUNTX05FV19DVVNUT01FUlMnLCAnc3RhcnQnKVxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0Ly8gT3JkZXJzIChCZXN0ZWxsdW5nZW4pXG5cdFx0XHRvcmRlcnM6IHtcblx0XHRcdFx0YXBpVXJsOiAnLi9hZG1pbi5waHA/ZG89RGFzaGJvYXJkL2dldE9yZGVyU3RhdGlzdGljc0RhdGEnLFxuXHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1NUQVRJU1RJQ1NfT1JERVJTX0NPVU5UJywgJ3N0YXJ0Jylcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8vIE1ldGEgT2JqZWN0XG5cdFx0dmFyIG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIExPQURJTkcgU1RBVEVcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBUdXJucyBvbi9vZmYgbG9hZGluZyBzdGF0ZVxuXHRcdCAqIEBwYXJhbSB7Ym9vbGVhbn0gaXNMb2FkaW5nIC0gSWYgdHJ1ZSwgdGhlIGxvYWRpbmcgc3RhdGUgd2lsbCBiZSB0cmlnZ2VyZWRcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfdG9nZ2xlTG9hZGluZyA9IGZ1bmN0aW9uKGlzTG9hZGluZykge1xuXHRcdFx0Ly8gRXhpc3RhbnQgc3Bpbm5lciBlbGVtZW50XG5cdFx0XHR2YXIgJGV4aXN0YW50U3Bpbm5lciA9ICR0aGlzLmZpbmQoJy5sb2FkZXInKTtcblx0XHRcdHZhciBpc1NwaW5uZXJBbHJlYWR5RXhpc3RzID0gJGV4aXN0YW50U3Bpbm5lci5sZW5ndGg7XG5cdFx0XHRcblx0XHRcdC8vIE5ldyBzcGlubmVyIGVsZW1lbnRcblx0XHRcdHZhciBzcGlubmVyQ2xhc3MgPSAnbG9hZGVyIGZhIGZhLWZ3IGZhLXNwaW5uZXIgZmEtc3Bpbic7XG5cdFx0XHR2YXIgJG5ld1NwaW5uZXIgPSAkKCc8aSBjbGFzcz1cIicgKyBzcGlubmVyQ2xhc3MgKyAnXCI+PC9pPicpO1xuXHRcdFx0XG5cdFx0XHQvLyBMb29rIGZvciBleGlzdGFudCBzcGlubmVyIGVsZW1lbnQgYW5kIHJlbW92ZSBpdFxuXHRcdFx0aWYgKGlzU3Bpbm5lckFscmVhZHlFeGlzdHMpIHtcblx0XHRcdFx0JGV4aXN0YW50U3Bpbm5lci5yZW1vdmUoKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBuZXcgb25lIGlmICdpc0xvYWRpbmcnIGFyZ3VtZW50IGlzIHRydWVcblx0XHRcdGlmIChpc0xvYWRpbmcpIHtcblx0XHRcdFx0JHRoaXMuYXBwZW5kKCRuZXdTcGlubmVyKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0cmV0dXJuO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXRyaWV2ZSBkYXRhIGZyb20gc2VydmVyIGFuZCBidWlsZHMgdGhlIGNoYXJ0XG5cdFx0ICogVGhlIHNlcnZlciByZXF1ZXN0IHBhcmFtZXRlcnMgc2hvdWxkIGxvb2sgbGlrZSB0aGlzOlxuXHRcdCAqIEByZXF1aXJlcyBNb3JyaXNKU1xuXHRcdCAqIEBwYXJhbSB7RXZlbnQgfCBqUXVlcnl9IGV2ZW50XG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGludGVydmFsXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IFtpdGVtXSAtIElmIHVuZGVmaW5lZCwgaXQgd2lsbCBnZXQgdmFsdWUgZnJvbSBkcm9wZG93blxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9idWlsZENoYXJ0ID0gZnVuY3Rpb24oZXZlbnQsIGludGVydmFsLCBpdGVtKSB7XG5cdFx0XHRcblx0XHRcdC8vIENsZWFyIGVsZW1lbnRcblx0XHRcdCR0aGlzLmVtcHR5KCk7XG5cdFx0XHRcblx0XHRcdC8vIEdldCBpdGVtIHZhbHVlIGZyb20gZHJvcGRvd24gaWYgbm90IHBhc3NlZCB2aWEgYXJndW1lbnRcblx0XHRcdGlmICghaXRlbSkge1xuXHRcdFx0XHRpdGVtID0gJGl0ZW1Ecm9wZG93bi5maW5kKCdvcHRpb246c2VsZWN0ZWQnKS52YWwoKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gVGhyb3cgZXJyb3IgaWYgaXRlbSBpcyBub3QgZGVmaW5lZCBpbiBtYXBcblx0XHRcdGlmIChpdGVtICYmIGl0ZW0gaW4gaXRlbU1hcCA9PT0gZmFsc2UpIHtcblx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGl0ZW0hJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgbG9hZGluZyBzcGlubmVyXG5cdFx0XHRfdG9nZ2xlTG9hZGluZyh0cnVlKTtcblx0XHRcdFxuXHRcdFx0Ly8gUGVyZm9ybSBSZXF1ZXN0XG5cdFx0XHR2YXIgcmVxdWVzdCA9ICQuYWpheCh7XG5cdFx0XHRcdHVybDogaXRlbU1hcFtpdGVtXS5hcGlVcmwsXG5cdFx0XHRcdHR5cGU6ICdHRVQnLFxuXHRcdFx0XHRkYXRhVHlwZTogJ0pTT04nLFxuXHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0dXNlcklkOiBteVVzZXJJZCxcblx0XHRcdFx0XHRpdGVtOiBpdGVtLFxuXHRcdFx0XHRcdGludGVydmFsOiBpbnRlcnZhbFxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gT24gc3VjY2Vzc1xuXHRcdFx0cmVxdWVzdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBIaWRlIGxvYWRpbmcgc3Bpbm5lclxuXHRcdFx0XHRfdG9nZ2xlTG9hZGluZyhmYWxzZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkdGhpcy5lbXB0eSgpO1xuXHRcdFx0XHRcblx0XHRcdFx0JC5lYWNoKHJlc3BvbnNlLmRhdGEsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdHRoaXMuYW1vdW50ID0gcGFyc2VJbnQodGhpcy5hbW91bnQpO1xuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIERyYXcgY2hhcnRcblx0XHRcdFx0TW9ycmlzLkFyZWEoe1xuXHRcdFx0XHRcdGVsZW1lbnQ6ICR0aGlzLFxuXHRcdFx0XHRcdGRhdGE6IHJlc3BvbnNlLmRhdGEsXG5cdFx0XHRcdFx0eGtleTogJ3BlcmlvZCcsXG5cdFx0XHRcdFx0eWtleXM6IFsnYW1vdW50J10sXG5cdFx0XHRcdFx0eExhYmVsczogcmVzcG9uc2UudHlwZSxcblx0XHRcdFx0XHRsYWJlbHM6IFtpdGVtTWFwW2l0ZW1dLnRpdGxlXSxcblx0XHRcdFx0XHRsaW5lV2lkdGg6IDIsXG5cdFx0XHRcdFx0ZXZlbnRTdHJva2VXaWR0aDogMSxcblx0XHRcdFx0XHRnb2FsU3Ryb2tlV2lkdGg6IDEsXG5cdFx0XHRcdFx0ZmlsbE9wYWNpdHk6IDAuMjUsXG5cdFx0XHRcdFx0YmVoYXZlTGlrZUxpbmU6IHRydWUsXG5cdFx0XHRcdFx0aGlkZUhvdmVyOiAnYXV0bycsXG5cdFx0XHRcdFx0bGluZUNvbG9yczogWycjMjE5NkYzJ10sXG5cdFx0XHRcdFx0ZGF0ZUZvcm1hdDogZnVuY3Rpb24odGltZXN0YW1wKSB7XG5cdFx0XHRcdFx0XHR2YXIgZGF0ZSA9IG5ldyBEYXRlKHRpbWVzdGFtcCk7XG5cdFx0XHRcdFx0XHR2YXIgZGF5ID0gZGF0ZS5nZXREYXRlKCkudG9TdHJpbmcoKTtcblx0XHRcdFx0XHRcdHZhciBtb250aCA9IChkYXRlLmdldE1vbnRoKCkgKyAxKS50b1N0cmluZygpO1xuXHRcdFx0XHRcdFx0dmFyIHllYXIgPSBkYXRlLmdldEZ1bGxZZWFyKCkudG9TdHJpbmcoKTtcblx0XHRcdFx0XHRcdHJldHVybiAoZGF5WzFdID8gZGF5IDogJzAnICsgZGF5WzBdKSArICcuJyArXG5cdFx0XHRcdFx0XHRcdChtb250aFsxXSA/IG1vbnRoIDogJzAnICsgbW9udGhbMF0pICsgJy4nICsgeWVhcjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBJbml0aWFsaXplIG1ldGhvZCBvZiB0aGUgd2lkZ2V0LCBjYWxsZWQgYnkgdGhlIGVuZ2luZS5cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdC8vIERlbGVnYXRlIGV2ZW50XG5cdFx0XHQkdGhpcy5vbignZ2V0OmRhdGEnLCBfYnVpbGRDaGFydCk7XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byBtb2R1bGUgZW5naW5lLlxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
