'use strict';

/* --------------------------------------------------------------
 button_dropdown.js 2016-07-15
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Button Dropdown Widget
 *
 * Adds the dropdown functionality to multiple elements inside a parent container. You can add new HTML 
 * options to each dropdown instance manually or dynamically through the Admin/Libs/button_dropdown library. 
 * 
 * Optionally, the widget has also the ability to store the last clicked option and display it as the default 
 * action the next time the page is loaded. This is very useful whenever there are many options inside the 
 * dropdown list.
 * 
 * ### Parent Container Options
 * 
 * **Configuration Keys | `data-button_dropdown-config_keys` | String | Optional**
 * 
 * Provide a unique key which will be used to store the latest user selection. Prefer to prefix your config key 
 * in order to avoid collisions with other instances of the widget.
 * 
 * **User ID | `data-button_dropdown-user_id` | Number | Optional** 
 * 
 * Give the current user database ID that will be used to associate his latest selection with the corresponding 
 * button dropdown widget.
 * 
 * ### Widget Instance Options
 * 
 * **Use Button Dropdown | `data-use-button_dropdown` | Boolean | Required** 
 * 
 * This option-flag will mark the elements inside the parent container, that will be converted into 
 * button-dropdown widgets.
 * 
 * **Configuration Key | `data-config_key` | String | Required**
 * 
 * Provide the configuration key for the single button-dropdown instance.
 * 
 * **Configuration Value | `data-config_key` | String | Optional**
 *
 * Provide directly the configuration value in order to avoid extra AJAX requests.
 * 
 * **Custom Caret Button Class | `data-custom_caret_btn_class` | String | Optional**
 * 
 * Attach additional classes to the caret button element (the one with the arrow). Use this option if you 
 * want to add a class that the primary button already has so that both share the same style (e.g. btn-primary).
 * 
 * ### Example
 * ```html
 * <!-- This element represents the parent container. -->
 * <div
 *   data-gx-widget="button_dropdown"
 *   data-button_dropdown-config_keys="order-single order-multi"
 *   data-button_dropdown-user_id="2">
 * 
 *   <!-- This element represents the button dropdown widget. -->
 *   <div
 *       data-use-button_dropdown="true" 
 *       data-config_key="order-single"
 *       data-custom_caret_btn_class="class1">
 *     <button>Primary Button</button>
 *     <ul>
 *       <li><span>Change status</span></li>
 *       <li><span>Delete</span></li>
 *     </ul>
 *   </div>
 * </div>
 * ```
 *
 * **Notice:** This widget was built for usage in compatibility mode. The new admin pages use the Bootstrap
 * button dropdown markup which already functions like this module. Do not use it on new admin pages. 
 * 
 * @module Admin/Widgets/button_dropdown
 */
gx.widgets.module('button_dropdown', ['user_configuration_service'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  * @type {object}
  */
	$this = $(this),


	/**
  * UserConfigurationService alias.
  * @type {object}
  */
	userConfigurationService = jse.libs.user_configuration_service,


	/**
  * Caret button template.
  * @type {string}
  */
	caretButtonTemplate = '<button class="btn" type="button"><i class="fa fa-caret-down"></i></button>',


	/**
  * Default Widget Options
  * @type {object}
  */
	defaults = {
		/**
   * Fade animation options.
   * @type {object}
   */
		fade: {
			duration: 300,
			easing: 'swing'
		},

		/**
   * String for dropdown selector.
   * This selector is used to find and activate all button dropdowns.
   *
   * @type {string}
   */
		dropdown_selector: '[data-use-button_dropdown]',

		/**
   * Attribute which represents the user configuration value.
   * The value of this attribute will be set.
   *
   * @type {string}
   */
		config_value_attribute: 'data-configuration_value'
	},


	/**
  * Final Widget Options
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Element selector shortcuts.
  * @type {object}
  */
	selectors = {
		element: options.dropdown_selector,
		mainButton: 'button:nth-child(1)',
		caretButton: 'button:nth-child(2)'
	},


	/**
  * Module Object
  * @type {object}
  */
	module = {};

	/**
  * Split space-separated entries to array values.
  * @type {array}
  */
	options.config_keys = options.config_keys ? options.config_keys.split(' ') : [];

	// ------------------------------------------------------------------------
	// PRIVATE METHODS - INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Loads the user configuration values for each provided key.
  * Returns a Deferred object with an object with configuration
  * as key and respective values or null if no request conditions are set.
  *
  * @returns {jQuery.Deferred}
  * @private
  */
	var _loadConfigurations = function _loadConfigurations() {

		/**
   * Main deferred object which will be returned.
   * @type {jQuery.Deferred}
   */
		var deferred = $.Deferred();

		/**
   * This array will contain all deferred ajax request to the user configuration service.
   * @example
   *      [Deferred, Deferred]
   * @type {array}
   */
		var configDeferreds = [];

		/**
   * User configuration key and values storage.
   * @example
   *      {
   *          configKey: 'configValue'
   *      }
   * @type {object}
   */
		var configValues = {};

		// Return immediately if the user configuration service is not needed.
		if (!options.user_id || !options.config_keys.length) {
			return deferred.resolve(null);
		}

		// Iterate over each configuration value provided in the element
		$.each(options.config_keys, function (index, configKey) {
			// Create deferred object for configuration value fetch.
			var configDeferred = $.Deferred();

			// Fetch configuration value from service.
			// Adds the fetched value to the `configValues` object and resolves the promise.
			userConfigurationService.get({
				data: {
					userId: options.user_id,
					configurationKey: configKey
				},
				onSuccess: function onSuccess(response) {
					configValues[configKey] = response.configurationValue;
					configDeferred.resolve();
				},
				onError: function onError() {
					configDeferred.resolve();
				}
			});

			configDeferreds.push(configDeferred);
		});

		// If all requests for the configuration values has been processed
		// then the main promise will be resolved with all configuration values as given parameter.
		$.when.apply(null, configDeferreds).done(function () {
			deferred.resolve(configValues);
		});

		// Return deferred object.
		return deferred;
	};

	/**
  * Finds all dropdown elements.
  * Returns a deferred object with an element list object.
  * This function hides the dropdown elements.
  *
  * @return {jQuery.Deferred}
  * @private
  */
	var _findDropdownElements = function _findDropdownElements() {
		/**
   * Deferred object which will be returned.
   * @type {jQuery.Deferred}
   */
		var deferred = $.Deferred();

		/**
   * Elements with element and data attribute informations.
   * @example
   *      [{
   *          element: <div>,
   *          custom_caret_btn_class: 'btn-primary'
   *          configKey: 'orderMultiSelect'
   *      }]
   * @type {array}
   */
		var elements = [];

		/**
   * Array of data attributes for the dropdown elements which will be checked.
   * @type {array}
   */
		var dataAttributes = ['custom_caret_btn_class', 'config_key', 'config_value'];

		// Find dropdown elements when DOM is ready
		// and resolve promise passing found elements as parameter.
		$(document).ready(function () {
			$this.find(options.dropdown_selector).each(function (index, element) {
				/**
     * jQuery wrapped element shortcut.
     * @type {jQuery}
     */
				var $element = $(element);

				/**
     * Element info object.
     * Will be pushed to `elements` array.
     * @example
     *      {
     *          element: <div>,
     *          custom_caret_btn_class: 'btn-primary'
     *          configKey: 'orderMultiSelect'
     *      }
     * @type {object}
     */
				var elementObject = {};

				// Add element to element info object.
				elementObject.element = element;

				// Iterate over each data attribute key and check for data attribute existence.
				// If data-attribute exists, the key and value will be added to element info object.
				$.each(dataAttributes, function (index, attribute) {
					if (attribute in $element.data()) {
						elementObject[attribute] = $element.data(attribute);
					}
				});

				// Push this element info object to `elements` array.
				elements.push(elementObject);

				// Hide element
				$element.hide();
			});

			// Resolve the promise passing in the elements as argument.
			deferred.resolve(elements);
		});

		// Return deferred object.
		return deferred;
	};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS - DROPDOWN TOGGLE
	// ------------------------------------------------------------------------

	/**
  * Shows dropdown action list.
  *
  * @param {HTMLElement} element Dropdown action list element.
  * @private
  */
	var _showDropdown = function _showDropdown(element) {
		// Perform fade in.
		$(element).stop().addClass('hover').fadeIn(options.fade);

		// Fix position.
		_repositionDropdown(element);
	};

	/**
  * Hides dropdown action list.
  *
  * @param {HTMLElement} element Dropdown action list element.
  * @private
  */
	var _hideDropdown = function _hideDropdown(element) {
		// Perform fade out.
		$(element).stop().removeClass('hover').fadeOut(options.fade);
	};

	/**
  * Fixes the dropdown action list to ensure that the action list is always visible.
  *
  * Sometimes when the button dropdown widget is near the window borders the list might
  * not be visible. This function will change its position in order to always be visible.
  *
  * @param {HTMLElement} element Dropdown action list element.
  * @private
  */
	var _repositionDropdown = function _repositionDropdown(element) {
		// Wrap element in jQuery and save shortcut to dropdown action list element.
		var $list = $(element);

		// Reference to button element.
		var $button = $list.closest(options.dropdown_selector);

		// Reset any possible CSS position modifications.
		$list.css({ left: '', top: '' });

		// Check dropdown position and perform reposition if needed.
		if ($list.offset().left + $list.width() > window.innerWidth) {
			var toMoveLeftPixels = $list.width() - $button.width();
			$list.css('margin-left', '-' + toMoveLeftPixels + 'px');
		}

		if ($list.offset().top + $list.height() > window.innerHeight) {
			var toMoveUpPixels = $list.height() + 10; // 10px fine-tuning
			$list.css('margin-top', '-' + toMoveUpPixels + 'px');
		}
	};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS - EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Handles click events on the main button (action button).
  * Performs main button action.
  *
  * @param {jQuery.Event} event
  * @private
  */
	var _mainButtonClickHandler = function _mainButtonClickHandler(event) {
		event.preventDefault();
		event.stopPropagation();

		$(this).trigger('perform:action');
	};

	/**
  * Handles click events on the dropdown button (caret button).
  * Shows or hides the dropdown action list.
  *
  * @param {jQuery.Event} event
  * @private
  */
	var _caretButtonClickHandler = function _caretButtonClickHandler(event) {
		event.preventDefault();
		event.stopPropagation();

		/**
   * Shortcut reference to dropdown action list element.
   * @type {jQuery}
   */
		var $list = $(this).siblings('ul');

		/**
   * Determines whether the dropdown action list is visible.
   * @type {boolean}
   */
		var listIsVisible = $list.hasClass('hover');

		// Hide or show dropdown, dependent on its visibility state.
		if (listIsVisible) {
			_hideDropdown($list);
		} else {
			_showDropdown($list);
		}
	};

	/**
  * Handles click events on the dropdown action list.
  * Hides the dropdown, saves the chosen value through
  * the user configuration service and perform the selected action.
  *
  * @param {jQuery.Event} event
  * @private
  */
	var _listItemClickHandler = function _listItemClickHandler(event) {
		event.preventDefault();
		event.stopPropagation();

		/**
   * Reference to `this` element, wrapped in jQuery.
   * @type {jQuery}
   */
		var $self = $(this);

		/**
   * Reference to dropdown action list element.
   * @type {jQuery}
   */
		var $list = $self.closest('ul');

		/**
   * Reference to button dropdown element.
   * @type {jQuery}
   */
		var $button = $self.closest(options.dropdown_selector);

		// Hide dropdown.
		_hideDropdown($list);

		// Save user configuration data.
		var configKey = $button.data('config_key'),
		    configValue = $self.data('value');

		if (configKey && configValue) {
			_saveUserConfiguration(configKey, configValue);
		}

		// Perform action.
		$self.trigger('perform:action');
	};

	/**
  * Handles click events outside of the button area.
  * Hides multiple opened dropdowns.
  * @param {jQuery.Event} event
  * @private
  */
	var _outsideClickHandler = function _outsideClickHandler(event) {
		/**
   * Element shortcut to all opened dropdown action lists.
   * @type {jQuery}
   */
		var $list = $('ul.hover');

		// Hide all opened dropdowns.
		_hideDropdown($list);
	};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS - CREATE WIDGETS
	// ------------------------------------------------------------------------

	/**
  * Adds the dropdown functionality to the buttons.
  *
  * Developers can manually add new `<li>` items to the list in order to display more options
  * to the users.
  *
  * This function fades the dropdown elements in.
  *
  * @param {array} elements List of elements infos object which contains the element itself and data attributes.
  * @param {object} configuration Object with fetched configuration key and values.
  *
  * @return {jQuery.Deferred}
  * @private
  */
	var _makeWidgets = function _makeWidgets(elements, configuration) {

		/**
   * Deferred object which will be returned.
   * @type {jQuery.Deferred}
   */
		var deferred = $.Deferred();

		/**
   * The secondary button which will toggle the list visibility.
   * @type {jQuery}
   */
		var $secondaryButton = $(caretButtonTemplate);

		// Iterate over each element and create dropdown widget.
		$.each(elements, function (index, elementObject) {
			/**
    * Button dropdown element.
    * @type {jQuery}
    */
			var $element = $(elementObject.element);

			/**
    * Button dropdown element's buttons.
    * @type {jQuery}
    */
			var $button = $element.find('button:first');

			/**
    * Cloned caret button template.
    * @type {jQuery}
    */
			var $caretButton = $secondaryButton.clone();

			// Add custom class to template, if defined.
			if (elementObject.custom_caret_btn_class) {
				$caretButton.addClass(elementObject.custom_caret_btn_class);
			}

			// Add CSS class to button and place the caret button.
			$button.addClass('btn').after($caretButton);

			// Add class to dropdown button element.
			$element.addClass('js-button-dropdown');

			// Add configuration value to container, if key and value exist.
			if (configuration && elementObject.config_key && configuration[elementObject.config_key] || elementObject.config_value) {
				var value = elementObject.config_value || configuration[elementObject.config_key];
				$element.attr(options.config_value_attribute, value);
			}

			// Attach event handler: Click on first button (main action button).
			$element.on('click', selectors.mainButton, _mainButtonClickHandler);

			// Attach event handler: Click on dropdown button (caret button).
			$element.on('click', selectors.caretButton, _caretButtonClickHandler);

			// Attach event handler: Click on dropdown action list item.
			$element.on('click', 'ul span, ul a', _listItemClickHandler);

			// Fade in element.
			$element.fadeIn(options.fade.duration, function () {
				$element.css('display', '');
			});
		});

		// Attach event handler: Close dropdown on outside click.
		$(document).on('click', _outsideClickHandler);

		// Resolve promise.
		deferred.resolve();

		// Return deferred object.
		return deferred;
	};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS - SAVE USER CONFIGURATION
	// ------------------------------------------------------------------------

	/**
  * Saves a user configuration value.
  *
  * @param {string} key Configuration key.
  * @param {string} value Configuration value.
  * @private
  */
	var _saveUserConfiguration = function _saveUserConfiguration(key, value) {
		// Throw error if no complete data has been provided.
		if (!key || !value) {
			throw new Error('No configuration data passed');
		}

		// Save value to database via user configuration service.
		userConfigurationService.set({
			data: {
				userId: options.user_id,
				configurationKey: key,
				configurationValue: value
			}
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the module, called by the engine.
  */
	module.init = function (done) {
		$.when(_findDropdownElements(), _loadConfigurations()).then(_makeWidgets).then(done);
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJ1dHRvbl9kcm9wZG93bi5qcyJdLCJuYW1lcyI6WyJneCIsIndpZGdldHMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwidXNlckNvbmZpZ3VyYXRpb25TZXJ2aWNlIiwianNlIiwibGlicyIsInVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlIiwiY2FyZXRCdXR0b25UZW1wbGF0ZSIsImRlZmF1bHRzIiwiZmFkZSIsImR1cmF0aW9uIiwiZWFzaW5nIiwiZHJvcGRvd25fc2VsZWN0b3IiLCJjb25maWdfdmFsdWVfYXR0cmlidXRlIiwib3B0aW9ucyIsImV4dGVuZCIsInNlbGVjdG9ycyIsImVsZW1lbnQiLCJtYWluQnV0dG9uIiwiY2FyZXRCdXR0b24iLCJjb25maWdfa2V5cyIsInNwbGl0IiwiX2xvYWRDb25maWd1cmF0aW9ucyIsImRlZmVycmVkIiwiRGVmZXJyZWQiLCJjb25maWdEZWZlcnJlZHMiLCJjb25maWdWYWx1ZXMiLCJ1c2VyX2lkIiwibGVuZ3RoIiwicmVzb2x2ZSIsImVhY2giLCJpbmRleCIsImNvbmZpZ0tleSIsImNvbmZpZ0RlZmVycmVkIiwiZ2V0IiwidXNlcklkIiwiY29uZmlndXJhdGlvbktleSIsIm9uU3VjY2VzcyIsInJlc3BvbnNlIiwiY29uZmlndXJhdGlvblZhbHVlIiwib25FcnJvciIsInB1c2giLCJ3aGVuIiwiYXBwbHkiLCJkb25lIiwiX2ZpbmREcm9wZG93bkVsZW1lbnRzIiwiZWxlbWVudHMiLCJkYXRhQXR0cmlidXRlcyIsImRvY3VtZW50IiwicmVhZHkiLCJmaW5kIiwiJGVsZW1lbnQiLCJlbGVtZW50T2JqZWN0IiwiYXR0cmlidXRlIiwiaGlkZSIsIl9zaG93RHJvcGRvd24iLCJzdG9wIiwiYWRkQ2xhc3MiLCJmYWRlSW4iLCJfcmVwb3NpdGlvbkRyb3Bkb3duIiwiX2hpZGVEcm9wZG93biIsInJlbW92ZUNsYXNzIiwiZmFkZU91dCIsIiRsaXN0IiwiJGJ1dHRvbiIsImNsb3Nlc3QiLCJjc3MiLCJsZWZ0IiwidG9wIiwib2Zmc2V0Iiwid2lkdGgiLCJ3aW5kb3ciLCJpbm5lcldpZHRoIiwidG9Nb3ZlTGVmdFBpeGVscyIsImhlaWdodCIsImlubmVySGVpZ2h0IiwidG9Nb3ZlVXBQaXhlbHMiLCJfbWFpbkJ1dHRvbkNsaWNrSGFuZGxlciIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJ0cmlnZ2VyIiwiX2NhcmV0QnV0dG9uQ2xpY2tIYW5kbGVyIiwic2libGluZ3MiLCJsaXN0SXNWaXNpYmxlIiwiaGFzQ2xhc3MiLCJfbGlzdEl0ZW1DbGlja0hhbmRsZXIiLCIkc2VsZiIsImNvbmZpZ1ZhbHVlIiwiX3NhdmVVc2VyQ29uZmlndXJhdGlvbiIsIl9vdXRzaWRlQ2xpY2tIYW5kbGVyIiwiX21ha2VXaWRnZXRzIiwiY29uZmlndXJhdGlvbiIsIiRzZWNvbmRhcnlCdXR0b24iLCIkY2FyZXRCdXR0b24iLCJjbG9uZSIsImN1c3RvbV9jYXJldF9idG5fY2xhc3MiLCJhZnRlciIsImNvbmZpZ19rZXkiLCJjb25maWdfdmFsdWUiLCJ2YWx1ZSIsImF0dHIiLCJvbiIsImtleSIsIkVycm9yIiwic2V0IiwiaW5pdCIsInRoZW4iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUVBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxpQkFERCxFQUdDLENBQUMsNEJBQUQsQ0FIRCxFQUtDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7OztBQUlBQyxTQUFRQyxFQUFFLElBQUYsQ0FMVDs7O0FBT0M7Ozs7QUFJQUMsNEJBQTJCQyxJQUFJQyxJQUFKLENBQVNDLDBCQVhyQzs7O0FBYUM7Ozs7QUFJQUMsdUJBQXNCLDZFQWpCdkI7OztBQW1CQzs7OztBQUlBQyxZQUFXO0FBQ1Y7Ozs7QUFJQUMsUUFBTTtBQUNMQyxhQUFVLEdBREw7QUFFTEMsV0FBUTtBQUZILEdBTEk7O0FBVVY7Ozs7OztBQU1BQyxxQkFBbUIsNEJBaEJUOztBQWtCVjs7Ozs7O0FBTUFDLDBCQUF3QjtBQXhCZCxFQXZCWjs7O0FBa0RDOzs7O0FBSUFDLFdBQVVaLEVBQUVhLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQlAsUUFBbkIsRUFBNkJSLElBQTdCLENBdERYOzs7QUF3REM7Ozs7QUFJQWdCLGFBQVk7QUFDWEMsV0FBU0gsUUFBUUYsaUJBRE47QUFFWE0sY0FBWSxxQkFGRDtBQUdYQyxlQUFhO0FBSEYsRUE1RGI7OztBQWtFQzs7OztBQUlBcEIsVUFBUyxFQXRFVjs7QUF3RUE7Ozs7QUFJQWUsU0FBUU0sV0FBUixHQUFzQk4sUUFBUU0sV0FBUixHQUFzQk4sUUFBUU0sV0FBUixDQUFvQkMsS0FBcEIsQ0FBMEIsR0FBMUIsQ0FBdEIsR0FBdUQsRUFBN0U7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQVFBLEtBQUlDLHNCQUFzQixTQUF0QkEsbUJBQXNCLEdBQVc7O0FBRXBDOzs7O0FBSUEsTUFBSUMsV0FBV3JCLEVBQUVzQixRQUFGLEVBQWY7O0FBRUE7Ozs7OztBQU1BLE1BQUlDLGtCQUFrQixFQUF0Qjs7QUFFQTs7Ozs7Ozs7QUFRQSxNQUFJQyxlQUFlLEVBQW5COztBQUVBO0FBQ0EsTUFBSSxDQUFDWixRQUFRYSxPQUFULElBQW9CLENBQUNiLFFBQVFNLFdBQVIsQ0FBb0JRLE1BQTdDLEVBQXFEO0FBQ3BELFVBQU9MLFNBQVNNLE9BQVQsQ0FBaUIsSUFBakIsQ0FBUDtBQUNBOztBQUVEO0FBQ0EzQixJQUFFNEIsSUFBRixDQUFPaEIsUUFBUU0sV0FBZixFQUE0QixVQUFTVyxLQUFULEVBQWdCQyxTQUFoQixFQUEyQjtBQUN0RDtBQUNBLE9BQUlDLGlCQUFpQi9CLEVBQUVzQixRQUFGLEVBQXJCOztBQUVBO0FBQ0E7QUFDQXJCLDRCQUF5QitCLEdBQXpCLENBQTZCO0FBQzVCbEMsVUFBTTtBQUNMbUMsYUFBUXJCLFFBQVFhLE9BRFg7QUFFTFMsdUJBQWtCSjtBQUZiLEtBRHNCO0FBSzVCSyxlQUFXLG1CQUFTQyxRQUFULEVBQW1CO0FBQzdCWixrQkFBYU0sU0FBYixJQUEwQk0sU0FBU0Msa0JBQW5DO0FBQ0FOLG9CQUFlSixPQUFmO0FBQ0EsS0FSMkI7QUFTNUJXLGFBQVMsbUJBQVc7QUFDbkJQLG9CQUFlSixPQUFmO0FBQ0E7QUFYMkIsSUFBN0I7O0FBY0FKLG1CQUFnQmdCLElBQWhCLENBQXFCUixjQUFyQjtBQUNBLEdBckJEOztBQXVCQTtBQUNBO0FBQ0EvQixJQUFFd0MsSUFBRixDQUFPQyxLQUFQLENBQWEsSUFBYixFQUFtQmxCLGVBQW5CLEVBQW9DbUIsSUFBcEMsQ0FBeUMsWUFBVztBQUNuRHJCLFlBQVNNLE9BQVQsQ0FBaUJILFlBQWpCO0FBQ0EsR0FGRDs7QUFJQTtBQUNBLFNBQU9ILFFBQVA7QUFDQSxFQS9ERDs7QUFpRUE7Ozs7Ozs7O0FBUUEsS0FBSXNCLHdCQUF3QixTQUF4QkEscUJBQXdCLEdBQVc7QUFDdEM7Ozs7QUFJQSxNQUFJdEIsV0FBV3JCLEVBQUVzQixRQUFGLEVBQWY7O0FBRUE7Ozs7Ozs7Ozs7QUFVQSxNQUFJc0IsV0FBVyxFQUFmOztBQUVBOzs7O0FBSUEsTUFBSUMsaUJBQWlCLENBQUMsd0JBQUQsRUFBMkIsWUFBM0IsRUFBeUMsY0FBekMsQ0FBckI7O0FBRUE7QUFDQTtBQUNBN0MsSUFBRThDLFFBQUYsRUFBWUMsS0FBWixDQUFrQixZQUFXO0FBQzVCaEQsU0FBTWlELElBQU4sQ0FBV3BDLFFBQVFGLGlCQUFuQixFQUFzQ2tCLElBQXRDLENBQTJDLFVBQVNDLEtBQVQsRUFBZ0JkLE9BQWhCLEVBQXlCO0FBQ25FOzs7O0FBSUEsUUFBSWtDLFdBQVdqRCxFQUFFZSxPQUFGLENBQWY7O0FBRUE7Ozs7Ozs7Ozs7O0FBV0EsUUFBSW1DLGdCQUFnQixFQUFwQjs7QUFFQTtBQUNBQSxrQkFBY25DLE9BQWQsR0FBd0JBLE9BQXhCOztBQUVBO0FBQ0E7QUFDQWYsTUFBRTRCLElBQUYsQ0FBT2lCLGNBQVAsRUFBdUIsVUFBU2hCLEtBQVQsRUFBZ0JzQixTQUFoQixFQUEyQjtBQUNqRCxTQUFJQSxhQUFhRixTQUFTbkQsSUFBVCxFQUFqQixFQUFrQztBQUNqQ29ELG9CQUFjQyxTQUFkLElBQTJCRixTQUFTbkQsSUFBVCxDQUFjcUQsU0FBZCxDQUEzQjtBQUNBO0FBQ0QsS0FKRDs7QUFNQTtBQUNBUCxhQUFTTCxJQUFULENBQWNXLGFBQWQ7O0FBRUE7QUFDQUQsYUFBU0csSUFBVDtBQUNBLElBcENEOztBQXNDQTtBQUNBL0IsWUFBU00sT0FBVCxDQUFpQmlCLFFBQWpCO0FBQ0EsR0F6Q0Q7O0FBMkNBO0FBQ0EsU0FBT3ZCLFFBQVA7QUFDQSxFQXhFRDs7QUEwRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFNQSxLQUFJZ0MsZ0JBQWdCLFNBQWhCQSxhQUFnQixDQUFTdEMsT0FBVCxFQUFrQjtBQUNyQztBQUNBZixJQUFFZSxPQUFGLEVBQ0V1QyxJQURGLEdBRUVDLFFBRkYsQ0FFVyxPQUZYLEVBR0VDLE1BSEYsQ0FHUzVDLFFBQVFMLElBSGpCOztBQUtBO0FBQ0FrRCxzQkFBb0IxQyxPQUFwQjtBQUNBLEVBVEQ7O0FBV0E7Ozs7OztBQU1BLEtBQUkyQyxnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVMzQyxPQUFULEVBQWtCO0FBQ3JDO0FBQ0FmLElBQUVlLE9BQUYsRUFDRXVDLElBREYsR0FFRUssV0FGRixDQUVjLE9BRmQsRUFHRUMsT0FIRixDQUdVaEQsUUFBUUwsSUFIbEI7QUFJQSxFQU5EOztBQVFBOzs7Ozs7Ozs7QUFTQSxLQUFJa0Qsc0JBQXNCLFNBQXRCQSxtQkFBc0IsQ0FBUzFDLE9BQVQsRUFBa0I7QUFDM0M7QUFDQSxNQUFJOEMsUUFBUTdELEVBQUVlLE9BQUYsQ0FBWjs7QUFFQTtBQUNBLE1BQUkrQyxVQUFVRCxNQUFNRSxPQUFOLENBQWNuRCxRQUFRRixpQkFBdEIsQ0FBZDs7QUFFQTtBQUNBbUQsUUFBTUcsR0FBTixDQUFVLEVBQUNDLE1BQU0sRUFBUCxFQUFXQyxLQUFLLEVBQWhCLEVBQVY7O0FBRUE7QUFDQSxNQUFJTCxNQUFNTSxNQUFOLEdBQWVGLElBQWYsR0FBc0JKLE1BQU1PLEtBQU4sRUFBdEIsR0FBc0NDLE9BQU9DLFVBQWpELEVBQTZEO0FBQzVELE9BQUlDLG1CQUFtQlYsTUFBTU8sS0FBTixLQUFnQk4sUUFBUU0sS0FBUixFQUF2QztBQUNBUCxTQUFNRyxHQUFOLENBQVUsYUFBVixFQUF5QixNQUFPTyxnQkFBUCxHQUEyQixJQUFwRDtBQUNBOztBQUVELE1BQUlWLE1BQU1NLE1BQU4sR0FBZUQsR0FBZixHQUFxQkwsTUFBTVcsTUFBTixFQUFyQixHQUFzQ0gsT0FBT0ksV0FBakQsRUFBOEQ7QUFDN0QsT0FBSUMsaUJBQWlCYixNQUFNVyxNQUFOLEtBQWlCLEVBQXRDLENBRDZELENBQ25CO0FBQzFDWCxTQUFNRyxHQUFOLENBQVUsWUFBVixFQUF3QixNQUFPVSxjQUFQLEdBQXlCLElBQWpEO0FBQ0E7QUFDRCxFQXBCRDs7QUFzQkE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O0FBT0EsS0FBSUMsMEJBQTBCLFNBQTFCQSx1QkFBMEIsQ0FBU0MsS0FBVCxFQUFnQjtBQUM3Q0EsUUFBTUMsY0FBTjtBQUNBRCxRQUFNRSxlQUFOOztBQUVBOUUsSUFBRSxJQUFGLEVBQVErRSxPQUFSLENBQWdCLGdCQUFoQjtBQUNBLEVBTEQ7O0FBT0E7Ozs7Ozs7QUFPQSxLQUFJQywyQkFBMkIsU0FBM0JBLHdCQUEyQixDQUFTSixLQUFULEVBQWdCO0FBQzlDQSxRQUFNQyxjQUFOO0FBQ0FELFFBQU1FLGVBQU47O0FBRUE7Ozs7QUFJQSxNQUFJakIsUUFBUTdELEVBQUUsSUFBRixFQUFRaUYsUUFBUixDQUFpQixJQUFqQixDQUFaOztBQUVBOzs7O0FBSUEsTUFBSUMsZ0JBQWdCckIsTUFBTXNCLFFBQU4sQ0FBZSxPQUFmLENBQXBCOztBQUVBO0FBQ0EsTUFBSUQsYUFBSixFQUFtQjtBQUNsQnhCLGlCQUFjRyxLQUFkO0FBQ0EsR0FGRCxNQUVPO0FBQ05SLGlCQUFjUSxLQUFkO0FBQ0E7QUFDRCxFQXRCRDs7QUF3QkE7Ozs7Ozs7O0FBUUEsS0FBSXVCLHdCQUF3QixTQUF4QkEscUJBQXdCLENBQVNSLEtBQVQsRUFBZ0I7QUFDM0NBLFFBQU1DLGNBQU47QUFDQUQsUUFBTUUsZUFBTjs7QUFFQTs7OztBQUlBLE1BQUlPLFFBQVFyRixFQUFFLElBQUYsQ0FBWjs7QUFFQTs7OztBQUlBLE1BQUk2RCxRQUFRd0IsTUFBTXRCLE9BQU4sQ0FBYyxJQUFkLENBQVo7O0FBRUE7Ozs7QUFJQSxNQUFJRCxVQUFVdUIsTUFBTXRCLE9BQU4sQ0FBY25ELFFBQVFGLGlCQUF0QixDQUFkOztBQUVBO0FBQ0FnRCxnQkFBY0csS0FBZDs7QUFFQTtBQUNBLE1BQUkvQixZQUFZZ0MsUUFBUWhFLElBQVIsQ0FBYSxZQUFiLENBQWhCO0FBQUEsTUFDQ3dGLGNBQWNELE1BQU12RixJQUFOLENBQVcsT0FBWCxDQURmOztBQUdBLE1BQUlnQyxhQUFhd0QsV0FBakIsRUFBOEI7QUFDN0JDLDBCQUF1QnpELFNBQXZCLEVBQWtDd0QsV0FBbEM7QUFDQTs7QUFFRDtBQUNBRCxRQUFNTixPQUFOLENBQWMsZ0JBQWQ7QUFDQSxFQW5DRDs7QUFxQ0E7Ozs7OztBQU1BLEtBQUlTLHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQVNaLEtBQVQsRUFBZ0I7QUFDMUM7Ozs7QUFJQSxNQUFJZixRQUFRN0QsRUFBRSxVQUFGLENBQVo7O0FBRUE7QUFDQTBELGdCQUFjRyxLQUFkO0FBQ0EsRUFURDs7QUFXQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7O0FBY0EsS0FBSTRCLGVBQWUsU0FBZkEsWUFBZSxDQUFTN0MsUUFBVCxFQUFtQjhDLGFBQW5CLEVBQWtDOztBQUVwRDs7OztBQUlBLE1BQUlyRSxXQUFXckIsRUFBRXNCLFFBQUYsRUFBZjs7QUFFQTs7OztBQUlBLE1BQUlxRSxtQkFBbUIzRixFQUFFSyxtQkFBRixDQUF2Qjs7QUFFQTtBQUNBTCxJQUFFNEIsSUFBRixDQUFPZ0IsUUFBUCxFQUFpQixVQUFTZixLQUFULEVBQWdCcUIsYUFBaEIsRUFBK0I7QUFDL0M7Ozs7QUFJQSxPQUFJRCxXQUFXakQsRUFBRWtELGNBQWNuQyxPQUFoQixDQUFmOztBQUVBOzs7O0FBSUEsT0FBSStDLFVBQVViLFNBQVNELElBQVQsQ0FBYyxjQUFkLENBQWQ7O0FBRUE7Ozs7QUFJQSxPQUFJNEMsZUFBZUQsaUJBQWlCRSxLQUFqQixFQUFuQjs7QUFFQTtBQUNBLE9BQUkzQyxjQUFjNEMsc0JBQWxCLEVBQTBDO0FBQ3pDRixpQkFBYXJDLFFBQWIsQ0FBc0JMLGNBQWM0QyxzQkFBcEM7QUFDQTs7QUFFRDtBQUNBaEMsV0FDRVAsUUFERixDQUNXLEtBRFgsRUFFRXdDLEtBRkYsQ0FFUUgsWUFGUjs7QUFJQTtBQUNBM0MsWUFDRU0sUUFERixDQUNXLG9CQURYOztBQUdBO0FBQ0EsT0FBSW1DLGlCQUFpQnhDLGNBQWM4QyxVQUEvQixJQUE2Q04sY0FBY3hDLGNBQWM4QyxVQUE1QixDQUE3QyxJQUF3RjlDLGNBQWMrQyxZQUExRyxFQUF3SDtBQUN2SCxRQUFJQyxRQUFRaEQsY0FBYytDLFlBQWQsSUFBOEJQLGNBQWN4QyxjQUFjOEMsVUFBNUIsQ0FBMUM7QUFDQS9DLGFBQVNrRCxJQUFULENBQWN2RixRQUFRRCxzQkFBdEIsRUFBOEN1RixLQUE5QztBQUNBOztBQUVEO0FBQ0FqRCxZQUFTbUQsRUFBVCxDQUFZLE9BQVosRUFBcUJ0RixVQUFVRSxVQUEvQixFQUEyQzJELHVCQUEzQzs7QUFFQTtBQUNBMUIsWUFBU21ELEVBQVQsQ0FBWSxPQUFaLEVBQXFCdEYsVUFBVUcsV0FBL0IsRUFBNEMrRCx3QkFBNUM7O0FBRUE7QUFDQS9CLFlBQVNtRCxFQUFULENBQVksT0FBWixFQUFxQixlQUFyQixFQUFzQ2hCLHFCQUF0Qzs7QUFFQTtBQUNBbkMsWUFBU08sTUFBVCxDQUFnQjVDLFFBQVFMLElBQVIsQ0FBYUMsUUFBN0IsRUFBdUMsWUFBVztBQUNqRHlDLGFBQVNlLEdBQVQsQ0FBYSxTQUFiLEVBQXdCLEVBQXhCO0FBQ0EsSUFGRDtBQUdBLEdBcEREOztBQXNEQTtBQUNBaEUsSUFBRThDLFFBQUYsRUFBWXNELEVBQVosQ0FBZSxPQUFmLEVBQXdCWixvQkFBeEI7O0FBRUE7QUFDQW5FLFdBQVNNLE9BQVQ7O0FBRUE7QUFDQSxTQUFPTixRQUFQO0FBQ0EsRUE3RUQ7O0FBK0VBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLEtBQUlrRSx5QkFBeUIsU0FBekJBLHNCQUF5QixDQUFTYyxHQUFULEVBQWNILEtBQWQsRUFBcUI7QUFDakQ7QUFDQSxNQUFJLENBQUNHLEdBQUQsSUFBUSxDQUFDSCxLQUFiLEVBQW9CO0FBQ25CLFNBQU0sSUFBSUksS0FBSixDQUFVLDhCQUFWLENBQU47QUFDQTs7QUFFRDtBQUNBckcsMkJBQXlCc0csR0FBekIsQ0FBNkI7QUFDNUJ6RyxTQUFNO0FBQ0xtQyxZQUFRckIsUUFBUWEsT0FEWDtBQUVMUyxzQkFBa0JtRSxHQUZiO0FBR0xoRSx3QkFBb0I2RDtBQUhmO0FBRHNCLEdBQTdCO0FBT0EsRUFkRDs7QUFnQkE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQXJHLFFBQU8yRyxJQUFQLEdBQWMsVUFBUzlELElBQVQsRUFBZTtBQUM1QjFDLElBQUV3QyxJQUFGLENBQU9HLHVCQUFQLEVBQWdDdkIscUJBQWhDLEVBQ0VxRixJQURGLENBQ09oQixZQURQLEVBRUVnQixJQUZGLENBRU8vRCxJQUZQO0FBR0EsRUFKRDs7QUFNQTtBQUNBLFFBQU83QyxNQUFQO0FBQ0EsQ0F0akJGIiwiZmlsZSI6ImJ1dHRvbl9kcm9wZG93bi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gYnV0dG9uX2Ryb3Bkb3duLmpzIDIwMTYtMDctMTVcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIEJ1dHRvbiBEcm9wZG93biBXaWRnZXRcbiAqXG4gKiBBZGRzIHRoZSBkcm9wZG93biBmdW5jdGlvbmFsaXR5IHRvIG11bHRpcGxlIGVsZW1lbnRzIGluc2lkZSBhIHBhcmVudCBjb250YWluZXIuIFlvdSBjYW4gYWRkIG5ldyBIVE1MIFxuICogb3B0aW9ucyB0byBlYWNoIGRyb3Bkb3duIGluc3RhbmNlIG1hbnVhbGx5IG9yIGR5bmFtaWNhbGx5IHRocm91Z2ggdGhlIEFkbWluL0xpYnMvYnV0dG9uX2Ryb3Bkb3duIGxpYnJhcnkuIFxuICogXG4gKiBPcHRpb25hbGx5LCB0aGUgd2lkZ2V0IGhhcyBhbHNvIHRoZSBhYmlsaXR5IHRvIHN0b3JlIHRoZSBsYXN0IGNsaWNrZWQgb3B0aW9uIGFuZCBkaXNwbGF5IGl0IGFzIHRoZSBkZWZhdWx0IFxuICogYWN0aW9uIHRoZSBuZXh0IHRpbWUgdGhlIHBhZ2UgaXMgbG9hZGVkLiBUaGlzIGlzIHZlcnkgdXNlZnVsIHdoZW5ldmVyIHRoZXJlIGFyZSBtYW55IG9wdGlvbnMgaW5zaWRlIHRoZSBcbiAqIGRyb3Bkb3duIGxpc3QuXG4gKiBcbiAqICMjIyBQYXJlbnQgQ29udGFpbmVyIE9wdGlvbnNcbiAqIFxuICogKipDb25maWd1cmF0aW9uIEtleXMgfCBgZGF0YS1idXR0b25fZHJvcGRvd24tY29uZmlnX2tleXNgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICogXG4gKiBQcm92aWRlIGEgdW5pcXVlIGtleSB3aGljaCB3aWxsIGJlIHVzZWQgdG8gc3RvcmUgdGhlIGxhdGVzdCB1c2VyIHNlbGVjdGlvbi4gUHJlZmVyIHRvIHByZWZpeCB5b3VyIGNvbmZpZyBrZXkgXG4gKiBpbiBvcmRlciB0byBhdm9pZCBjb2xsaXNpb25zIHdpdGggb3RoZXIgaW5zdGFuY2VzIG9mIHRoZSB3aWRnZXQuXG4gKiBcbiAqICoqVXNlciBJRCB8IGBkYXRhLWJ1dHRvbl9kcm9wZG93bi11c2VyX2lkYCB8IE51bWJlciB8IE9wdGlvbmFsKiogXG4gKiBcbiAqIEdpdmUgdGhlIGN1cnJlbnQgdXNlciBkYXRhYmFzZSBJRCB0aGF0IHdpbGwgYmUgdXNlZCB0byBhc3NvY2lhdGUgaGlzIGxhdGVzdCBzZWxlY3Rpb24gd2l0aCB0aGUgY29ycmVzcG9uZGluZyBcbiAqIGJ1dHRvbiBkcm9wZG93biB3aWRnZXQuXG4gKiBcbiAqICMjIyBXaWRnZXQgSW5zdGFuY2UgT3B0aW9uc1xuICogXG4gKiAqKlVzZSBCdXR0b24gRHJvcGRvd24gfCBgZGF0YS11c2UtYnV0dG9uX2Ryb3Bkb3duYCB8IEJvb2xlYW4gfCBSZXF1aXJlZCoqIFxuICogXG4gKiBUaGlzIG9wdGlvbi1mbGFnIHdpbGwgbWFyayB0aGUgZWxlbWVudHMgaW5zaWRlIHRoZSBwYXJlbnQgY29udGFpbmVyLCB0aGF0IHdpbGwgYmUgY29udmVydGVkIGludG8gXG4gKiBidXR0b24tZHJvcGRvd24gd2lkZ2V0cy5cbiAqIFxuICogKipDb25maWd1cmF0aW9uIEtleSB8IGBkYXRhLWNvbmZpZ19rZXlgIHwgU3RyaW5nIHwgUmVxdWlyZWQqKlxuICogXG4gKiBQcm92aWRlIHRoZSBjb25maWd1cmF0aW9uIGtleSBmb3IgdGhlIHNpbmdsZSBidXR0b24tZHJvcGRvd24gaW5zdGFuY2UuXG4gKiBcbiAqICoqQ29uZmlndXJhdGlvbiBWYWx1ZSB8IGBkYXRhLWNvbmZpZ19rZXlgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICpcbiAqIFByb3ZpZGUgZGlyZWN0bHkgdGhlIGNvbmZpZ3VyYXRpb24gdmFsdWUgaW4gb3JkZXIgdG8gYXZvaWQgZXh0cmEgQUpBWCByZXF1ZXN0cy5cbiAqIFxuICogKipDdXN0b20gQ2FyZXQgQnV0dG9uIENsYXNzIHwgYGRhdGEtY3VzdG9tX2NhcmV0X2J0bl9jbGFzc2AgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKiBcbiAqIEF0dGFjaCBhZGRpdGlvbmFsIGNsYXNzZXMgdG8gdGhlIGNhcmV0IGJ1dHRvbiBlbGVtZW50ICh0aGUgb25lIHdpdGggdGhlIGFycm93KS4gVXNlIHRoaXMgb3B0aW9uIGlmIHlvdSBcbiAqIHdhbnQgdG8gYWRkIGEgY2xhc3MgdGhhdCB0aGUgcHJpbWFyeSBidXR0b24gYWxyZWFkeSBoYXMgc28gdGhhdCBib3RoIHNoYXJlIHRoZSBzYW1lIHN0eWxlIChlLmcuIGJ0bi1wcmltYXJ5KS5cbiAqIFxuICogIyMjIEV4YW1wbGVcbiAqIGBgYGh0bWxcbiAqIDwhLS0gVGhpcyBlbGVtZW50IHJlcHJlc2VudHMgdGhlIHBhcmVudCBjb250YWluZXIuIC0tPlxuICogPGRpdlxuICogICBkYXRhLWd4LXdpZGdldD1cImJ1dHRvbl9kcm9wZG93blwiXG4gKiAgIGRhdGEtYnV0dG9uX2Ryb3Bkb3duLWNvbmZpZ19rZXlzPVwib3JkZXItc2luZ2xlIG9yZGVyLW11bHRpXCJcbiAqICAgZGF0YS1idXR0b25fZHJvcGRvd24tdXNlcl9pZD1cIjJcIj5cbiAqIFxuICogICA8IS0tIFRoaXMgZWxlbWVudCByZXByZXNlbnRzIHRoZSBidXR0b24gZHJvcGRvd24gd2lkZ2V0LiAtLT5cbiAqICAgPGRpdlxuICogICAgICAgZGF0YS11c2UtYnV0dG9uX2Ryb3Bkb3duPVwidHJ1ZVwiIFxuICogICAgICAgZGF0YS1jb25maWdfa2V5PVwib3JkZXItc2luZ2xlXCJcbiAqICAgICAgIGRhdGEtY3VzdG9tX2NhcmV0X2J0bl9jbGFzcz1cImNsYXNzMVwiPlxuICogICAgIDxidXR0b24+UHJpbWFyeSBCdXR0b248L2J1dHRvbj5cbiAqICAgICA8dWw+XG4gKiAgICAgICA8bGk+PHNwYW4+Q2hhbmdlIHN0YXR1czwvc3Bhbj48L2xpPlxuICogICAgICAgPGxpPjxzcGFuPkRlbGV0ZTwvc3Bhbj48L2xpPlxuICogICAgIDwvdWw+XG4gKiAgIDwvZGl2PlxuICogPC9kaXY+XG4gKiBgYGBcbiAqXG4gKiAqKk5vdGljZToqKiBUaGlzIHdpZGdldCB3YXMgYnVpbHQgZm9yIHVzYWdlIGluIGNvbXBhdGliaWxpdHkgbW9kZS4gVGhlIG5ldyBhZG1pbiBwYWdlcyB1c2UgdGhlIEJvb3RzdHJhcFxuICogYnV0dG9uIGRyb3Bkb3duIG1hcmt1cCB3aGljaCBhbHJlYWR5IGZ1bmN0aW9ucyBsaWtlIHRoaXMgbW9kdWxlLiBEbyBub3QgdXNlIGl0IG9uIG5ldyBhZG1pbiBwYWdlcy4gXG4gKiBcbiAqIEBtb2R1bGUgQWRtaW4vV2lkZ2V0cy9idXR0b25fZHJvcGRvd25cbiAqL1xuZ3gud2lkZ2V0cy5tb2R1bGUoXG5cdCdidXR0b25fZHJvcGRvd24nLFxuXHRcblx0Wyd1c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSddLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIFdpZGdldCBSZWZlcmVuY2Vcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBVc2VyQ29uZmlndXJhdGlvblNlcnZpY2UgYWxpYXMuXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHR1c2VyQ29uZmlndXJhdGlvblNlcnZpY2UgPSBqc2UubGlicy51c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBDYXJldCBidXR0b24gdGVtcGxhdGUuXG5cdFx0XHQgKiBAdHlwZSB7c3RyaW5nfVxuXHRcdFx0ICovXG5cdFx0XHRjYXJldEJ1dHRvblRlbXBsYXRlID0gJzxidXR0b24gY2xhc3M9XCJidG5cIiB0eXBlPVwiYnV0dG9uXCI+PGkgY2xhc3M9XCJmYSBmYS1jYXJldC1kb3duXCI+PC9pPjwvYnV0dG9uPicsXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBXaWRnZXQgT3B0aW9uc1xuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBGYWRlIGFuaW1hdGlvbiBvcHRpb25zLlxuXHRcdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0XHQgKi9cblx0XHRcdFx0ZmFkZToge1xuXHRcdFx0XHRcdGR1cmF0aW9uOiAzMDAsXG5cdFx0XHRcdFx0ZWFzaW5nOiAnc3dpbmcnXG5cdFx0XHRcdH0sXG5cdFx0XHRcdFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICogU3RyaW5nIGZvciBkcm9wZG93biBzZWxlY3Rvci5cblx0XHRcdFx0ICogVGhpcyBzZWxlY3RvciBpcyB1c2VkIHRvIGZpbmQgYW5kIGFjdGl2YXRlIGFsbCBidXR0b24gZHJvcGRvd25zLlxuXHRcdFx0XHQgKlxuXHRcdFx0XHQgKiBAdHlwZSB7c3RyaW5nfVxuXHRcdFx0XHQgKi9cblx0XHRcdFx0ZHJvcGRvd25fc2VsZWN0b3I6ICdbZGF0YS11c2UtYnV0dG9uX2Ryb3Bkb3duXScsXG5cdFx0XHRcdFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICogQXR0cmlidXRlIHdoaWNoIHJlcHJlc2VudHMgdGhlIHVzZXIgY29uZmlndXJhdGlvbiB2YWx1ZS5cblx0XHRcdFx0ICogVGhlIHZhbHVlIG9mIHRoaXMgYXR0cmlidXRlIHdpbGwgYmUgc2V0LlxuXHRcdFx0XHQgKlxuXHRcdFx0XHQgKiBAdHlwZSB7c3RyaW5nfVxuXHRcdFx0XHQgKi9cblx0XHRcdFx0Y29uZmlnX3ZhbHVlX2F0dHJpYnV0ZTogJ2RhdGEtY29uZmlndXJhdGlvbl92YWx1ZSdcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgV2lkZ2V0IE9wdGlvbnNcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEVsZW1lbnQgc2VsZWN0b3Igc2hvcnRjdXRzLlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0c2VsZWN0b3JzID0ge1xuXHRcdFx0XHRlbGVtZW50OiBvcHRpb25zLmRyb3Bkb3duX3NlbGVjdG9yLFxuXHRcdFx0XHRtYWluQnV0dG9uOiAnYnV0dG9uOm50aC1jaGlsZCgxKScsXG5cdFx0XHRcdGNhcmV0QnV0dG9uOiAnYnV0dG9uOm50aC1jaGlsZCgyKSdcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU3BsaXQgc3BhY2Utc2VwYXJhdGVkIGVudHJpZXMgdG8gYXJyYXkgdmFsdWVzLlxuXHRcdCAqIEB0eXBlIHthcnJheX1cblx0XHQgKi9cblx0XHRvcHRpb25zLmNvbmZpZ19rZXlzID0gb3B0aW9ucy5jb25maWdfa2V5cyA/IG9wdGlvbnMuY29uZmlnX2tleXMuc3BsaXQoJyAnKSA6IFtdO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFBSSVZBVEUgTUVUSE9EUyAtIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTG9hZHMgdGhlIHVzZXIgY29uZmlndXJhdGlvbiB2YWx1ZXMgZm9yIGVhY2ggcHJvdmlkZWQga2V5LlxuXHRcdCAqIFJldHVybnMgYSBEZWZlcnJlZCBvYmplY3Qgd2l0aCBhbiBvYmplY3Qgd2l0aCBjb25maWd1cmF0aW9uXG5cdFx0ICogYXMga2V5IGFuZCByZXNwZWN0aXZlIHZhbHVlcyBvciBudWxsIGlmIG5vIHJlcXVlc3QgY29uZGl0aW9ucyBhcmUgc2V0LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybnMge2pRdWVyeS5EZWZlcnJlZH1cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfbG9hZENvbmZpZ3VyYXRpb25zID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTWFpbiBkZWZlcnJlZCBvYmplY3Qgd2hpY2ggd2lsbCBiZSByZXR1cm5lZC5cblx0XHRcdCAqIEB0eXBlIHtqUXVlcnkuRGVmZXJyZWR9XG5cdFx0XHQgKi9cblx0XHRcdHZhciBkZWZlcnJlZCA9ICQuRGVmZXJyZWQoKTtcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBUaGlzIGFycmF5IHdpbGwgY29udGFpbiBhbGwgZGVmZXJyZWQgYWpheCByZXF1ZXN0IHRvIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZS5cblx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHQgKiAgICAgIFtEZWZlcnJlZCwgRGVmZXJyZWRdXG5cdFx0XHQgKiBAdHlwZSB7YXJyYXl9XG5cdFx0XHQgKi9cblx0XHRcdHZhciBjb25maWdEZWZlcnJlZHMgPSBbXTtcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBVc2VyIGNvbmZpZ3VyYXRpb24ga2V5IGFuZCB2YWx1ZXMgc3RvcmFnZS5cblx0XHRcdCAqIEBleGFtcGxlXG5cdFx0XHQgKiAgICAgIHtcblx0XHRcdCAqICAgICAgICAgIGNvbmZpZ0tleTogJ2NvbmZpZ1ZhbHVlJ1xuXHRcdFx0ICogICAgICB9XG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHR2YXIgY29uZmlnVmFsdWVzID0ge307XG5cdFx0XHRcblx0XHRcdC8vIFJldHVybiBpbW1lZGlhdGVseSBpZiB0aGUgdXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UgaXMgbm90IG5lZWRlZC5cblx0XHRcdGlmICghb3B0aW9ucy51c2VyX2lkIHx8ICFvcHRpb25zLmNvbmZpZ19rZXlzLmxlbmd0aCkge1xuXHRcdFx0XHRyZXR1cm4gZGVmZXJyZWQucmVzb2x2ZShudWxsKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gSXRlcmF0ZSBvdmVyIGVhY2ggY29uZmlndXJhdGlvbiB2YWx1ZSBwcm92aWRlZCBpbiB0aGUgZWxlbWVudFxuXHRcdFx0JC5lYWNoKG9wdGlvbnMuY29uZmlnX2tleXMsIGZ1bmN0aW9uKGluZGV4LCBjb25maWdLZXkpIHtcblx0XHRcdFx0Ly8gQ3JlYXRlIGRlZmVycmVkIG9iamVjdCBmb3IgY29uZmlndXJhdGlvbiB2YWx1ZSBmZXRjaC5cblx0XHRcdFx0dmFyIGNvbmZpZ0RlZmVycmVkID0gJC5EZWZlcnJlZCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gRmV0Y2ggY29uZmlndXJhdGlvbiB2YWx1ZSBmcm9tIHNlcnZpY2UuXG5cdFx0XHRcdC8vIEFkZHMgdGhlIGZldGNoZWQgdmFsdWUgdG8gdGhlIGBjb25maWdWYWx1ZXNgIG9iamVjdCBhbmQgcmVzb2x2ZXMgdGhlIHByb21pc2UuXG5cdFx0XHRcdHVzZXJDb25maWd1cmF0aW9uU2VydmljZS5nZXQoe1xuXHRcdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHRcdHVzZXJJZDogb3B0aW9ucy51c2VyX2lkLFxuXHRcdFx0XHRcdFx0Y29uZmlndXJhdGlvbktleTogY29uZmlnS2V5XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRvblN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0XHRjb25maWdWYWx1ZXNbY29uZmlnS2V5XSA9IHJlc3BvbnNlLmNvbmZpZ3VyYXRpb25WYWx1ZTtcblx0XHRcdFx0XHRcdGNvbmZpZ0RlZmVycmVkLnJlc29sdmUoKTtcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdG9uRXJyb3I6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0Y29uZmlnRGVmZXJyZWQucmVzb2x2ZSgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRjb25maWdEZWZlcnJlZHMucHVzaChjb25maWdEZWZlcnJlZCk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gSWYgYWxsIHJlcXVlc3RzIGZvciB0aGUgY29uZmlndXJhdGlvbiB2YWx1ZXMgaGFzIGJlZW4gcHJvY2Vzc2VkXG5cdFx0XHQvLyB0aGVuIHRoZSBtYWluIHByb21pc2Ugd2lsbCBiZSByZXNvbHZlZCB3aXRoIGFsbCBjb25maWd1cmF0aW9uIHZhbHVlcyBhcyBnaXZlbiBwYXJhbWV0ZXIuXG5cdFx0XHQkLndoZW4uYXBwbHkobnVsbCwgY29uZmlnRGVmZXJyZWRzKS5kb25lKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRkZWZlcnJlZC5yZXNvbHZlKGNvbmZpZ1ZhbHVlcyk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gUmV0dXJuIGRlZmVycmVkIG9iamVjdC5cblx0XHRcdHJldHVybiBkZWZlcnJlZDtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmRzIGFsbCBkcm9wZG93biBlbGVtZW50cy5cblx0XHQgKiBSZXR1cm5zIGEgZGVmZXJyZWQgb2JqZWN0IHdpdGggYW4gZWxlbWVudCBsaXN0IG9iamVjdC5cblx0XHQgKiBUaGlzIGZ1bmN0aW9uIGhpZGVzIHRoZSBkcm9wZG93biBlbGVtZW50cy5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge2pRdWVyeS5EZWZlcnJlZH1cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfZmluZERyb3Bkb3duRWxlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmZXJyZWQgb2JqZWN0IHdoaWNoIHdpbGwgYmUgcmV0dXJuZWQuXG5cdFx0XHQgKiBAdHlwZSB7alF1ZXJ5LkRlZmVycmVkfVxuXHRcdFx0ICovXG5cdFx0XHR2YXIgZGVmZXJyZWQgPSAkLkRlZmVycmVkKCk7XG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRWxlbWVudHMgd2l0aCBlbGVtZW50IGFuZCBkYXRhIGF0dHJpYnV0ZSBpbmZvcm1hdGlvbnMuXG5cdFx0XHQgKiBAZXhhbXBsZVxuXHRcdFx0ICogICAgICBbe1xuXHRcdFx0ICogICAgICAgICAgZWxlbWVudDogPGRpdj4sXG5cdFx0XHQgKiAgICAgICAgICBjdXN0b21fY2FyZXRfYnRuX2NsYXNzOiAnYnRuLXByaW1hcnknXG5cdFx0XHQgKiAgICAgICAgICBjb25maWdLZXk6ICdvcmRlck11bHRpU2VsZWN0J1xuXHRcdFx0ICogICAgICB9XVxuXHRcdFx0ICogQHR5cGUge2FycmF5fVxuXHRcdFx0ICovXG5cdFx0XHR2YXIgZWxlbWVudHMgPSBbXTtcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBBcnJheSBvZiBkYXRhIGF0dHJpYnV0ZXMgZm9yIHRoZSBkcm9wZG93biBlbGVtZW50cyB3aGljaCB3aWxsIGJlIGNoZWNrZWQuXG5cdFx0XHQgKiBAdHlwZSB7YXJyYXl9XG5cdFx0XHQgKi9cblx0XHRcdHZhciBkYXRhQXR0cmlidXRlcyA9IFsnY3VzdG9tX2NhcmV0X2J0bl9jbGFzcycsICdjb25maWdfa2V5JywgJ2NvbmZpZ192YWx1ZSddO1xuXHRcdFx0XG5cdFx0XHQvLyBGaW5kIGRyb3Bkb3duIGVsZW1lbnRzIHdoZW4gRE9NIGlzIHJlYWR5XG5cdFx0XHQvLyBhbmQgcmVzb2x2ZSBwcm9taXNlIHBhc3NpbmcgZm91bmQgZWxlbWVudHMgYXMgcGFyYW1ldGVyLlxuXHRcdFx0JChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCR0aGlzLmZpbmQob3B0aW9ucy5kcm9wZG93bl9zZWxlY3RvcikuZWFjaChmdW5jdGlvbihpbmRleCwgZWxlbWVudCkge1xuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIGpRdWVyeSB3cmFwcGVkIGVsZW1lbnQgc2hvcnRjdXQuXG5cdFx0XHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdFx0XHQgKi9cblx0XHRcdFx0XHR2YXIgJGVsZW1lbnQgPSAkKGVsZW1lbnQpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIEVsZW1lbnQgaW5mbyBvYmplY3QuXG5cdFx0XHRcdFx0ICogV2lsbCBiZSBwdXNoZWQgdG8gYGVsZW1lbnRzYCBhcnJheS5cblx0XHRcdFx0XHQgKiBAZXhhbXBsZVxuXHRcdFx0XHRcdCAqICAgICAge1xuXHRcdFx0XHRcdCAqICAgICAgICAgIGVsZW1lbnQ6IDxkaXY+LFxuXHRcdFx0XHRcdCAqICAgICAgICAgIGN1c3RvbV9jYXJldF9idG5fY2xhc3M6ICdidG4tcHJpbWFyeSdcblx0XHRcdFx0XHQgKiAgICAgICAgICBjb25maWdLZXk6ICdvcmRlck11bHRpU2VsZWN0J1xuXHRcdFx0XHRcdCAqICAgICAgfVxuXHRcdFx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHRcdFx0ICovXG5cdFx0XHRcdFx0dmFyIGVsZW1lbnRPYmplY3QgPSB7fTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBBZGQgZWxlbWVudCB0byBlbGVtZW50IGluZm8gb2JqZWN0LlxuXHRcdFx0XHRcdGVsZW1lbnRPYmplY3QuZWxlbWVudCA9IGVsZW1lbnQ7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gSXRlcmF0ZSBvdmVyIGVhY2ggZGF0YSBhdHRyaWJ1dGUga2V5IGFuZCBjaGVjayBmb3IgZGF0YSBhdHRyaWJ1dGUgZXhpc3RlbmNlLlxuXHRcdFx0XHRcdC8vIElmIGRhdGEtYXR0cmlidXRlIGV4aXN0cywgdGhlIGtleSBhbmQgdmFsdWUgd2lsbCBiZSBhZGRlZCB0byBlbGVtZW50IGluZm8gb2JqZWN0LlxuXHRcdFx0XHRcdCQuZWFjaChkYXRhQXR0cmlidXRlcywgZnVuY3Rpb24oaW5kZXgsIGF0dHJpYnV0ZSkge1xuXHRcdFx0XHRcdFx0aWYgKGF0dHJpYnV0ZSBpbiAkZWxlbWVudC5kYXRhKCkpIHtcblx0XHRcdFx0XHRcdFx0ZWxlbWVudE9iamVjdFthdHRyaWJ1dGVdID0gJGVsZW1lbnQuZGF0YShhdHRyaWJ1dGUpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFB1c2ggdGhpcyBlbGVtZW50IGluZm8gb2JqZWN0IHRvIGBlbGVtZW50c2AgYXJyYXkuXG5cdFx0XHRcdFx0ZWxlbWVudHMucHVzaChlbGVtZW50T2JqZWN0KTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBIaWRlIGVsZW1lbnRcblx0XHRcdFx0XHQkZWxlbWVudC5oaWRlKCk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUmVzb2x2ZSB0aGUgcHJvbWlzZSBwYXNzaW5nIGluIHRoZSBlbGVtZW50cyBhcyBhcmd1bWVudC5cblx0XHRcdFx0ZGVmZXJyZWQucmVzb2x2ZShlbGVtZW50cyk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gUmV0dXJuIGRlZmVycmVkIG9iamVjdC5cblx0XHRcdHJldHVybiBkZWZlcnJlZDtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFBSSVZBVEUgTUVUSE9EUyAtIERST1BET1dOIFRPR0dMRVxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNob3dzIGRyb3Bkb3duIGFjdGlvbiBsaXN0LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZWxlbWVudCBEcm9wZG93biBhY3Rpb24gbGlzdCBlbGVtZW50LlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9zaG93RHJvcGRvd24gPSBmdW5jdGlvbihlbGVtZW50KSB7XG5cdFx0XHQvLyBQZXJmb3JtIGZhZGUgaW4uXG5cdFx0XHQkKGVsZW1lbnQpXG5cdFx0XHRcdC5zdG9wKClcblx0XHRcdFx0LmFkZENsYXNzKCdob3ZlcicpXG5cdFx0XHRcdC5mYWRlSW4ob3B0aW9ucy5mYWRlKTtcblx0XHRcdFxuXHRcdFx0Ly8gRml4IHBvc2l0aW9uLlxuXHRcdFx0X3JlcG9zaXRpb25Ecm9wZG93bihlbGVtZW50KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhpZGVzIGRyb3Bkb3duIGFjdGlvbiBsaXN0LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZWxlbWVudCBEcm9wZG93biBhY3Rpb24gbGlzdCBlbGVtZW50LlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9oaWRlRHJvcGRvd24gPSBmdW5jdGlvbihlbGVtZW50KSB7XG5cdFx0XHQvLyBQZXJmb3JtIGZhZGUgb3V0LlxuXHRcdFx0JChlbGVtZW50KVxuXHRcdFx0XHQuc3RvcCgpXG5cdFx0XHRcdC5yZW1vdmVDbGFzcygnaG92ZXInKVxuXHRcdFx0XHQuZmFkZU91dChvcHRpb25zLmZhZGUpO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRml4ZXMgdGhlIGRyb3Bkb3duIGFjdGlvbiBsaXN0IHRvIGVuc3VyZSB0aGF0IHRoZSBhY3Rpb24gbGlzdCBpcyBhbHdheXMgdmlzaWJsZS5cblx0XHQgKlxuXHRcdCAqIFNvbWV0aW1lcyB3aGVuIHRoZSBidXR0b24gZHJvcGRvd24gd2lkZ2V0IGlzIG5lYXIgdGhlIHdpbmRvdyBib3JkZXJzIHRoZSBsaXN0IG1pZ2h0XG5cdFx0ICogbm90IGJlIHZpc2libGUuIFRoaXMgZnVuY3Rpb24gd2lsbCBjaGFuZ2UgaXRzIHBvc2l0aW9uIGluIG9yZGVyIHRvIGFsd2F5cyBiZSB2aXNpYmxlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZWxlbWVudCBEcm9wZG93biBhY3Rpb24gbGlzdCBlbGVtZW50LlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9yZXBvc2l0aW9uRHJvcGRvd24gPSBmdW5jdGlvbihlbGVtZW50KSB7XG5cdFx0XHQvLyBXcmFwIGVsZW1lbnQgaW4galF1ZXJ5IGFuZCBzYXZlIHNob3J0Y3V0IHRvIGRyb3Bkb3duIGFjdGlvbiBsaXN0IGVsZW1lbnQuXG5cdFx0XHR2YXIgJGxpc3QgPSAkKGVsZW1lbnQpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZWZlcmVuY2UgdG8gYnV0dG9uIGVsZW1lbnQuXG5cdFx0XHR2YXIgJGJ1dHRvbiA9ICRsaXN0LmNsb3Nlc3Qob3B0aW9ucy5kcm9wZG93bl9zZWxlY3Rvcik7XG5cdFx0XHRcblx0XHRcdC8vIFJlc2V0IGFueSBwb3NzaWJsZSBDU1MgcG9zaXRpb24gbW9kaWZpY2F0aW9ucy5cblx0XHRcdCRsaXN0LmNzcyh7bGVmdDogJycsIHRvcDogJyd9KTtcblx0XHRcdFxuXHRcdFx0Ly8gQ2hlY2sgZHJvcGRvd24gcG9zaXRpb24gYW5kIHBlcmZvcm0gcmVwb3NpdGlvbiBpZiBuZWVkZWQuXG5cdFx0XHRpZiAoJGxpc3Qub2Zmc2V0KCkubGVmdCArICRsaXN0LndpZHRoKCkgPiB3aW5kb3cuaW5uZXJXaWR0aCkge1xuXHRcdFx0XHR2YXIgdG9Nb3ZlTGVmdFBpeGVscyA9ICRsaXN0LndpZHRoKCkgLSAkYnV0dG9uLndpZHRoKCk7XG5cdFx0XHRcdCRsaXN0LmNzcygnbWFyZ2luLWxlZnQnLCAnLScgKyAodG9Nb3ZlTGVmdFBpeGVscykgKyAncHgnKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKCRsaXN0Lm9mZnNldCgpLnRvcCArICRsaXN0LmhlaWdodCgpID4gd2luZG93LmlubmVySGVpZ2h0KSB7XG5cdFx0XHRcdHZhciB0b01vdmVVcFBpeGVscyA9ICRsaXN0LmhlaWdodCgpICsgMTA7IC8vIDEwcHggZmluZS10dW5pbmdcblx0XHRcdFx0JGxpc3QuY3NzKCdtYXJnaW4tdG9wJywgJy0nICsgKHRvTW92ZVVwUGl4ZWxzKSArICdweCcpO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBNRVRIT0RTIC0gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIGNsaWNrIGV2ZW50cyBvbiB0aGUgbWFpbiBidXR0b24gKGFjdGlvbiBidXR0b24pLlxuXHRcdCAqIFBlcmZvcm1zIG1haW4gYnV0dG9uIGFjdGlvbi5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudFxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9tYWluQnV0dG9uQ2xpY2tIYW5kbGVyID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFxuXHRcdFx0JCh0aGlzKS50cmlnZ2VyKCdwZXJmb3JtOmFjdGlvbicpO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyBjbGljayBldmVudHMgb24gdGhlIGRyb3Bkb3duIGJ1dHRvbiAoY2FyZXQgYnV0dG9uKS5cblx0XHQgKiBTaG93cyBvciBoaWRlcyB0aGUgZHJvcGRvd24gYWN0aW9uIGxpc3QuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfY2FyZXRCdXR0b25DbGlja0hhbmRsZXIgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFNob3J0Y3V0IHJlZmVyZW5jZSB0byBkcm9wZG93biBhY3Rpb24gbGlzdCBlbGVtZW50LlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0dmFyICRsaXN0ID0gJCh0aGlzKS5zaWJsaW5ncygndWwnKTtcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZXRlcm1pbmVzIHdoZXRoZXIgdGhlIGRyb3Bkb3duIGFjdGlvbiBsaXN0IGlzIHZpc2libGUuXG5cdFx0XHQgKiBAdHlwZSB7Ym9vbGVhbn1cblx0XHRcdCAqL1xuXHRcdFx0dmFyIGxpc3RJc1Zpc2libGUgPSAkbGlzdC5oYXNDbGFzcygnaG92ZXInKTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSBvciBzaG93IGRyb3Bkb3duLCBkZXBlbmRlbnQgb24gaXRzIHZpc2liaWxpdHkgc3RhdGUuXG5cdFx0XHRpZiAobGlzdElzVmlzaWJsZSkge1xuXHRcdFx0XHRfaGlkZURyb3Bkb3duKCRsaXN0KTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdF9zaG93RHJvcGRvd24oJGxpc3QpO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyBjbGljayBldmVudHMgb24gdGhlIGRyb3Bkb3duIGFjdGlvbiBsaXN0LlxuXHRcdCAqIEhpZGVzIHRoZSBkcm9wZG93biwgc2F2ZXMgdGhlIGNob3NlbiB2YWx1ZSB0aHJvdWdoXG5cdFx0ICogdGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlIGFuZCBwZXJmb3JtIHRoZSBzZWxlY3RlZCBhY3Rpb24uXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfbGlzdEl0ZW1DbGlja0hhbmRsZXIgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFJlZmVyZW5jZSB0byBgdGhpc2AgZWxlbWVudCwgd3JhcHBlZCBpbiBqUXVlcnkuXG5cdFx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdFx0ICovXG5cdFx0XHR2YXIgJHNlbGYgPSAkKHRoaXMpO1xuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFJlZmVyZW5jZSB0byBkcm9wZG93biBhY3Rpb24gbGlzdCBlbGVtZW50LlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0dmFyICRsaXN0ID0gJHNlbGYuY2xvc2VzdCgndWwnKTtcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBSZWZlcmVuY2UgdG8gYnV0dG9uIGRyb3Bkb3duIGVsZW1lbnQuXG5cdFx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdFx0ICovXG5cdFx0XHR2YXIgJGJ1dHRvbiA9ICRzZWxmLmNsb3Nlc3Qob3B0aW9ucy5kcm9wZG93bl9zZWxlY3Rvcik7XG5cdFx0XHRcblx0XHRcdC8vIEhpZGUgZHJvcGRvd24uXG5cdFx0XHRfaGlkZURyb3Bkb3duKCRsaXN0KTtcblx0XHRcdFxuXHRcdFx0Ly8gU2F2ZSB1c2VyIGNvbmZpZ3VyYXRpb24gZGF0YS5cblx0XHRcdHZhciBjb25maWdLZXkgPSAkYnV0dG9uLmRhdGEoJ2NvbmZpZ19rZXknKSxcblx0XHRcdFx0Y29uZmlnVmFsdWUgPSAkc2VsZi5kYXRhKCd2YWx1ZScpO1xuXHRcdFx0XG5cdFx0XHRpZiAoY29uZmlnS2V5ICYmIGNvbmZpZ1ZhbHVlKSB7XG5cdFx0XHRcdF9zYXZlVXNlckNvbmZpZ3VyYXRpb24oY29uZmlnS2V5LCBjb25maWdWYWx1ZSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIFBlcmZvcm0gYWN0aW9uLlxuXHRcdFx0JHNlbGYudHJpZ2dlcigncGVyZm9ybTphY3Rpb24nKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgY2xpY2sgZXZlbnRzIG91dHNpZGUgb2YgdGhlIGJ1dHRvbiBhcmVhLlxuXHRcdCAqIEhpZGVzIG11bHRpcGxlIG9wZW5lZCBkcm9wZG93bnMuXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX291dHNpZGVDbGlja0hhbmRsZXIgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0LyoqXG5cdFx0XHQgKiBFbGVtZW50IHNob3J0Y3V0IHRvIGFsbCBvcGVuZWQgZHJvcGRvd24gYWN0aW9uIGxpc3RzLlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0dmFyICRsaXN0ID0gJCgndWwuaG92ZXInKTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSBhbGwgb3BlbmVkIGRyb3Bkb3ducy5cblx0XHRcdF9oaWRlRHJvcGRvd24oJGxpc3QpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBNRVRIT0RTIC0gQ1JFQVRFIFdJREdFVFNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGRzIHRoZSBkcm9wZG93biBmdW5jdGlvbmFsaXR5IHRvIHRoZSBidXR0b25zLlxuXHRcdCAqXG5cdFx0ICogRGV2ZWxvcGVycyBjYW4gbWFudWFsbHkgYWRkIG5ldyBgPGxpPmAgaXRlbXMgdG8gdGhlIGxpc3QgaW4gb3JkZXIgdG8gZGlzcGxheSBtb3JlIG9wdGlvbnNcblx0XHQgKiB0byB0aGUgdXNlcnMuXG5cdFx0ICpcblx0XHQgKiBUaGlzIGZ1bmN0aW9uIGZhZGVzIHRoZSBkcm9wZG93biBlbGVtZW50cyBpbi5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7YXJyYXl9IGVsZW1lbnRzIExpc3Qgb2YgZWxlbWVudHMgaW5mb3Mgb2JqZWN0IHdoaWNoIGNvbnRhaW5zIHRoZSBlbGVtZW50IGl0c2VsZiBhbmQgZGF0YSBhdHRyaWJ1dGVzLlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBjb25maWd1cmF0aW9uIE9iamVjdCB3aXRoIGZldGNoZWQgY29uZmlndXJhdGlvbiBrZXkgYW5kIHZhbHVlcy5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge2pRdWVyeS5EZWZlcnJlZH1cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfbWFrZVdpZGdldHMgPSBmdW5jdGlvbihlbGVtZW50cywgY29uZmlndXJhdGlvbikge1xuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmVycmVkIG9iamVjdCB3aGljaCB3aWxsIGJlIHJldHVybmVkLlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeS5EZWZlcnJlZH1cblx0XHRcdCAqL1xuXHRcdFx0dmFyIGRlZmVycmVkID0gJC5EZWZlcnJlZCgpO1xuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFRoZSBzZWNvbmRhcnkgYnV0dG9uIHdoaWNoIHdpbGwgdG9nZ2xlIHRoZSBsaXN0IHZpc2liaWxpdHkuXG5cdFx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdFx0ICovXG5cdFx0XHR2YXIgJHNlY29uZGFyeUJ1dHRvbiA9ICQoY2FyZXRCdXR0b25UZW1wbGF0ZSk7XG5cdFx0XHRcblx0XHRcdC8vIEl0ZXJhdGUgb3ZlciBlYWNoIGVsZW1lbnQgYW5kIGNyZWF0ZSBkcm9wZG93biB3aWRnZXQuXG5cdFx0XHQkLmVhY2goZWxlbWVudHMsIGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50T2JqZWN0KSB7XG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBCdXR0b24gZHJvcGRvd24gZWxlbWVudC5cblx0XHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdFx0ICovXG5cdFx0XHRcdHZhciAkZWxlbWVudCA9ICQoZWxlbWVudE9iamVjdC5lbGVtZW50KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBCdXR0b24gZHJvcGRvd24gZWxlbWVudCdzIGJ1dHRvbnMuXG5cdFx0XHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0XHRcdCAqL1xuXHRcdFx0XHR2YXIgJGJ1dHRvbiA9ICRlbGVtZW50LmZpbmQoJ2J1dHRvbjpmaXJzdCcpO1xuXHRcdFx0XHRcblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIENsb25lZCBjYXJldCBidXR0b24gdGVtcGxhdGUuXG5cdFx0XHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0XHRcdCAqL1xuXHRcdFx0XHR2YXIgJGNhcmV0QnV0dG9uID0gJHNlY29uZGFyeUJ1dHRvbi5jbG9uZSgpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQWRkIGN1c3RvbSBjbGFzcyB0byB0ZW1wbGF0ZSwgaWYgZGVmaW5lZC5cblx0XHRcdFx0aWYgKGVsZW1lbnRPYmplY3QuY3VzdG9tX2NhcmV0X2J0bl9jbGFzcykge1xuXHRcdFx0XHRcdCRjYXJldEJ1dHRvbi5hZGRDbGFzcyhlbGVtZW50T2JqZWN0LmN1c3RvbV9jYXJldF9idG5fY2xhc3MpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBZGQgQ1NTIGNsYXNzIHRvIGJ1dHRvbiBhbmQgcGxhY2UgdGhlIGNhcmV0IGJ1dHRvbi5cblx0XHRcdFx0JGJ1dHRvblxuXHRcdFx0XHRcdC5hZGRDbGFzcygnYnRuJylcblx0XHRcdFx0XHQuYWZ0ZXIoJGNhcmV0QnV0dG9uKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEFkZCBjbGFzcyB0byBkcm9wZG93biBidXR0b24gZWxlbWVudC5cblx0XHRcdFx0JGVsZW1lbnRcblx0XHRcdFx0XHQuYWRkQ2xhc3MoJ2pzLWJ1dHRvbi1kcm9wZG93bicpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQWRkIGNvbmZpZ3VyYXRpb24gdmFsdWUgdG8gY29udGFpbmVyLCBpZiBrZXkgYW5kIHZhbHVlIGV4aXN0LlxuXHRcdFx0XHRpZiAoY29uZmlndXJhdGlvbiAmJiBlbGVtZW50T2JqZWN0LmNvbmZpZ19rZXkgJiYgY29uZmlndXJhdGlvbltlbGVtZW50T2JqZWN0LmNvbmZpZ19rZXldIHx8IGVsZW1lbnRPYmplY3QuY29uZmlnX3ZhbHVlKSB7XG5cdFx0XHRcdFx0dmFyIHZhbHVlID0gZWxlbWVudE9iamVjdC5jb25maWdfdmFsdWUgfHwgY29uZmlndXJhdGlvbltlbGVtZW50T2JqZWN0LmNvbmZpZ19rZXldOyBcblx0XHRcdFx0XHQkZWxlbWVudC5hdHRyKG9wdGlvbnMuY29uZmlnX3ZhbHVlX2F0dHJpYnV0ZSwgdmFsdWUpO1xuXHRcdFx0XHR9IFxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQXR0YWNoIGV2ZW50IGhhbmRsZXI6IENsaWNrIG9uIGZpcnN0IGJ1dHRvbiAobWFpbiBhY3Rpb24gYnV0dG9uKS5cblx0XHRcdFx0JGVsZW1lbnQub24oJ2NsaWNrJywgc2VsZWN0b3JzLm1haW5CdXR0b24sIF9tYWluQnV0dG9uQ2xpY2tIYW5kbGVyKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEF0dGFjaCBldmVudCBoYW5kbGVyOiBDbGljayBvbiBkcm9wZG93biBidXR0b24gKGNhcmV0IGJ1dHRvbikuXG5cdFx0XHRcdCRlbGVtZW50Lm9uKCdjbGljaycsIHNlbGVjdG9ycy5jYXJldEJ1dHRvbiwgX2NhcmV0QnV0dG9uQ2xpY2tIYW5kbGVyKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEF0dGFjaCBldmVudCBoYW5kbGVyOiBDbGljayBvbiBkcm9wZG93biBhY3Rpb24gbGlzdCBpdGVtLlxuXHRcdFx0XHQkZWxlbWVudC5vbignY2xpY2snLCAndWwgc3BhbiwgdWwgYScsIF9saXN0SXRlbUNsaWNrSGFuZGxlcik7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBGYWRlIGluIGVsZW1lbnQuXG5cdFx0XHRcdCRlbGVtZW50LmZhZGVJbihvcHRpb25zLmZhZGUuZHVyYXRpb24sIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCRlbGVtZW50LmNzcygnZGlzcGxheScsICcnKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gQXR0YWNoIGV2ZW50IGhhbmRsZXI6IENsb3NlIGRyb3Bkb3duIG9uIG91dHNpZGUgY2xpY2suXG5cdFx0XHQkKGRvY3VtZW50KS5vbignY2xpY2snLCBfb3V0c2lkZUNsaWNrSGFuZGxlcik7XG5cdFx0XHRcblx0XHRcdC8vIFJlc29sdmUgcHJvbWlzZS5cblx0XHRcdGRlZmVycmVkLnJlc29sdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmV0dXJuIGRlZmVycmVkIG9iamVjdC5cblx0XHRcdHJldHVybiBkZWZlcnJlZDtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFBSSVZBVEUgTUVUSE9EUyAtIFNBVkUgVVNFUiBDT05GSUdVUkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2F2ZXMgYSB1c2VyIGNvbmZpZ3VyYXRpb24gdmFsdWUuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30ga2V5IENvbmZpZ3VyYXRpb24ga2V5LlxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZSBDb25maWd1cmF0aW9uIHZhbHVlLlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9zYXZlVXNlckNvbmZpZ3VyYXRpb24gPSBmdW5jdGlvbihrZXksIHZhbHVlKSB7XG5cdFx0XHQvLyBUaHJvdyBlcnJvciBpZiBubyBjb21wbGV0ZSBkYXRhIGhhcyBiZWVuIHByb3ZpZGVkLlxuXHRcdFx0aWYgKCFrZXkgfHwgIXZhbHVlKSB7XG5cdFx0XHRcdHRocm93IG5ldyBFcnJvcignTm8gY29uZmlndXJhdGlvbiBkYXRhIHBhc3NlZCcpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTYXZlIHZhbHVlIHRvIGRhdGFiYXNlIHZpYSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZS5cblx0XHRcdHVzZXJDb25maWd1cmF0aW9uU2VydmljZS5zZXQoe1xuXHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0dXNlcklkOiBvcHRpb25zLnVzZXJfaWQsXG5cdFx0XHRcdFx0Y29uZmlndXJhdGlvbktleToga2V5LFxuXHRcdFx0XHRcdGNvbmZpZ3VyYXRpb25WYWx1ZTogdmFsdWVcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSBtb2R1bGUsIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JC53aGVuKF9maW5kRHJvcGRvd25FbGVtZW50cygpLCBfbG9hZENvbmZpZ3VyYXRpb25zKCkpXG5cdFx0XHRcdC50aGVuKF9tYWtlV2lkZ2V0cylcblx0XHRcdFx0LnRoZW4oZG9uZSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byBtb2R1bGUgZW5naW5lLlxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
