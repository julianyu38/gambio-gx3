'use strict';

/* --------------------------------------------------------------
 multi_select.js 2016-11-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Multi Select Widget
 *
 * This module serves as a wrapper of SumoSelect, a jQuery plugin that provides enhanced select-box functionality.
 * Bind this widget to a parent container and mark each child select-box element with the `data-multi_select-instance`
 * attribute.
 *
 * After the initialization of the widget all the marked elements will be converted into SumoSelect instances.
 *
 * ### Options
 *
 * **Options Source | `data-multi_select-source` | String | Optional**
 *
 * Provide a URL that will be used to fetch the options of the select box. The widget will perform a GET request to
 * the provided destination and expects a JSON array with the options:
 *
 * [
 *   {
 *     "value": "1", 
 *     "text": "Option #1"
 *   },
 *   {
 *     "value": "2", 
 *     "text": "Option #2"
 *   }
 * ]
 *
 * You can also pass other configuration directly in the parent element which will be used for every child instance.
 *
 *
 * ### Methods
 *
 * **Reload Options [AJAX]**
 *
 * You can use this method to refresh the options from the already provided data-multi_select-source or by providing
 * a new URL which will also be set as the data-source of the element. If the multi select has no URL then it will just
 * sync its values with the select element.
 *
 * * ```js
 * $('#my-multi-select').multi_select('reload', 'http://shop.de/options/source/url');
 * ```
 *
 * **Refresh Options**
 *
 * Update the multi-select widget with the state of the original select element. This method is useful after performing
 * changes in the original element and need to display them in the multi-select widget.
 *
 * ```js
 * $('#my-multi-select').multi_select('refresh');
 * ```
 *
 * ### Events
 * ```javascript
 * // Triggered when the multi-select widget has performed a "reload" method (after the AJAX call).
 * $('#my-multi-select').on('reload', function(event) {});
 * ```
 *
 * ### Example
 *
 * ```html
 * <form data-gx-widget="multi_select">
 *   <select data-multi_select-instance data-multi_select-source="http://shop.de/options-source-url"></select>
 * </form>
 * ```
 *
 * {@link http://hemantnegi.github.io/jquery.sumoselect}
 *
 * @module Admin/Widgets/multi_select
 * @requires jQuery-SumoSelect
 */
gx.widgets.module('multi_select', [jse.source + '/vendor/sumoselect/sumoselect.min.css', jse.source + '/vendor/sumoselect/jquery.sumoselect.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		placeholder: jse.core.lang.translate('SELECT', 'general'),
		selectAll: true,
		csvDispCount: 2,
		captionFormat: '{0} ' + jse.core.lang.translate('selected', 'admin_labels'),
		locale: ['OK', jse.core.lang.translate('CANCEL', 'general'), jse.core.lang.translate('SELECT_ALL', 'general')]
	};

	/**
  * Final Options
  *
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Add the "multi_select" method to the jQuery prototype.
  */
	function _addPublicMethod() {
		if ($.fn.multi_select) {
			return;
		}

		$.fn.extend({
			multi_select: function multi_select(action) {
				for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
					args[_key - 1] = arguments[_key];
				}

				if (!$(this).is('select')) {
					throw new Error('Called the "multi_select" method on an invalid object (select box expected).');
				}

				$.each(this, function () {
					switch (action) {
						case 'reload':
							_reload.apply(undefined, [$(this)].concat(args));
							break;

						case 'refresh':
							_refresh(this);
					}
				});
			}
		});
	}

	/**
  * Fill a select box with the provided options.
  *
  * @param {jQuery} $select The select box to be filled.
  * @param {Object} options Array with { value: "...", "text": "..." } entries.
  */
	function _fillSelect($select, options) {
		$select.empty();
		$.each(options, function (index, option) {
			$select.append(new Option(option.text, option.value));
		});
	}

	/**
  * Reload the options from the source (data property) or the provided URL,
  *
  * @param {string} url Optional, if provided it will be used as the source of the data and will also update the
  * data-source property of the element.
  */
	function _reload($select, url) {
		url = url || $select.data('source');

		if (!url) {
			throw new Error('Multi Select Reload: Neither URL nor data-source contain a URL value.');
		}

		$select.data('source', url);

		$.getJSON(url).done(function (response) {
			_fillSelect($select, response);
			$select[0].sumo.reload();
			$select.trigger('reload');
		}).fail(function (jqxhr, textStatus, errorThrown) {
			jse.core.debug.error('Multi Select AJAX Error: ', jqxhr, textStatus, errorThrown);
		});
	}

	/**
  * Refresh the multi select instance depending the state of the original select element.
  *
  * @param {Node} select The DOM element to be refreshed.
  */
	function _refresh(select) {
		if (select.sumo === undefined) {
			throw new Error('Multi Select Refresh: The provided select element is not an instance of SumoSelect.', select);
		}

		select.sumo.reload();

		// Update the caption by simulating a click in an ".opt" element.  
		_overrideSelectAllCaption.apply($(select.parentNode).find('.opt')[0]);
	}

	/**
  * Override the multi select caption when all elements are selected.
  *
  * This callback will override the caption because SumoSelect does not provide a setting for this text.
  */
	function _overrideSelectAllCaption() {
		var $optWrapper = $(this).parents('.optWrapper');
		var allCheckboxesChecked = $optWrapper.find('.opt.selected').length === $optWrapper.find('.opt').length;
		var atLeastOneCheckboxChecked = $optWrapper.find('.opt.selected').length > 0;
		var $selectAllCheckbox = $optWrapper.find('.select-all');

		$selectAllCheckbox.removeClass('partial-select');

		if (allCheckboxesChecked) {
			$optWrapper.siblings('.CaptionCont').children('span').text(jse.core.lang.translate('all_selected', 'admin_labels'));
		} else if (atLeastOneCheckboxChecked) {
			$selectAllCheckbox.addClass('partial-select');
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Add public module method.  
		_addPublicMethod();

		// Initialize the elements. 
		$this.find('[data-multi_select-instance]').each(function () {
			var $select = $(this);

			$select.removeAttr('data-multi_select-instance');

			// Instantiate the widget without an AJAX request.
			$select.SumoSelect(options);

			if ($select.data('multi_selectSource') !== undefined) {
				// Remove the data attribute and store the value internally with the 'source' key. 
				$select.data('source', $select.data('multi_selectSource'));
				$select.removeAttr('data-multi_select-source');

				// Fetch the options with an AJAX request.
				$.getJSON($select.data('multi_selectSource')).done(function (response) {
					_fillSelect($select, response);
					$select[0].sumo.unload();
					$select.SumoSelect(options);
					$select.trigger('reload');
				}).fail(function (jqxhr, textStatus, errorThrown) {
					jse.core.debug.error('Multi Select AJAX Error: ', jqxhr, textStatus, errorThrown);
				});
			}
		});

		done();
	};

	// When the user clicks on the "Select All" option update the text with a custom translations. This has to 
	// be done manually because there is no option for this text in SumoSelect. 
	$this.on('click', '.select-all, .opt', _overrideSelectAllCaption);

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm11bHRpX3NlbGVjdC5qcyJdLCJuYW1lcyI6WyJneCIsIndpZGdldHMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJwbGFjZWhvbGRlciIsImNvcmUiLCJsYW5nIiwidHJhbnNsYXRlIiwic2VsZWN0QWxsIiwiY3N2RGlzcENvdW50IiwiY2FwdGlvbkZvcm1hdCIsImxvY2FsZSIsIm9wdGlvbnMiLCJleHRlbmQiLCJfYWRkUHVibGljTWV0aG9kIiwiZm4iLCJtdWx0aV9zZWxlY3QiLCJhY3Rpb24iLCJhcmdzIiwiaXMiLCJFcnJvciIsImVhY2giLCJfcmVsb2FkIiwiX3JlZnJlc2giLCJfZmlsbFNlbGVjdCIsIiRzZWxlY3QiLCJlbXB0eSIsImluZGV4Iiwib3B0aW9uIiwiYXBwZW5kIiwiT3B0aW9uIiwidGV4dCIsInZhbHVlIiwidXJsIiwiZ2V0SlNPTiIsImRvbmUiLCJyZXNwb25zZSIsInN1bW8iLCJyZWxvYWQiLCJ0cmlnZ2VyIiwiZmFpbCIsImpxeGhyIiwidGV4dFN0YXR1cyIsImVycm9yVGhyb3duIiwiZGVidWciLCJlcnJvciIsInNlbGVjdCIsInVuZGVmaW5lZCIsIl9vdmVycmlkZVNlbGVjdEFsbENhcHRpb24iLCJhcHBseSIsInBhcmVudE5vZGUiLCJmaW5kIiwiJG9wdFdyYXBwZXIiLCJwYXJlbnRzIiwiYWxsQ2hlY2tib3hlc0NoZWNrZWQiLCJsZW5ndGgiLCJhdExlYXN0T25lQ2hlY2tib3hDaGVja2VkIiwiJHNlbGVjdEFsbENoZWNrYm94IiwicmVtb3ZlQ2xhc3MiLCJzaWJsaW5ncyIsImNoaWxkcmVuIiwiYWRkQ2xhc3MiLCJpbml0IiwicmVtb3ZlQXR0ciIsIlN1bW9TZWxlY3QiLCJ1bmxvYWQiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBc0VBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxjQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUiw0Q0FFSUQsSUFBSUMsTUFGUixpREFIRCxFQVFDLFVBQVNDLElBQVQsRUFBZTs7QUFFZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVc7QUFDaEJDLGVBQWFOLElBQUlPLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFFBQXhCLEVBQWtDLFNBQWxDLENBREc7QUFFaEJDLGFBQVcsSUFGSztBQUdoQkMsZ0JBQWMsQ0FIRTtBQUloQkMsMEJBQXNCWixJQUFJTyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixVQUF4QixFQUFvQyxjQUFwQyxDQUpOO0FBS2hCSSxVQUFRLENBQ1AsSUFETyxFQUVQYixJQUFJTyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixRQUF4QixFQUFrQyxTQUFsQyxDQUZPLEVBR1BULElBQUlPLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFlBQXhCLEVBQXNDLFNBQXRDLENBSE87QUFMUSxFQUFqQjs7QUFZQTs7Ozs7QUFLQSxLQUFNSyxVQUFVVixFQUFFVyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJWLFFBQW5CLEVBQTZCSCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxVQUFTaUIsZ0JBQVQsR0FBNEI7QUFDM0IsTUFBSVosRUFBRWEsRUFBRixDQUFLQyxZQUFULEVBQXVCO0FBQ3RCO0FBQ0E7O0FBRURkLElBQUVhLEVBQUYsQ0FBS0YsTUFBTCxDQUFZO0FBQ1hHLGlCQUFjLHNCQUFTQyxNQUFULEVBQTBCO0FBQUEsc0NBQU5DLElBQU07QUFBTkEsU0FBTTtBQUFBOztBQUN2QyxRQUFJLENBQUNoQixFQUFFLElBQUYsRUFBUWlCLEVBQVIsQ0FBVyxRQUFYLENBQUwsRUFBMkI7QUFDMUIsV0FBTSxJQUFJQyxLQUFKLENBQVUsOEVBQVYsQ0FBTjtBQUNBOztBQUVEbEIsTUFBRW1CLElBQUYsQ0FBTyxJQUFQLEVBQWEsWUFBVztBQUN2QixhQUFRSixNQUFSO0FBQ0MsV0FBSyxRQUFMO0FBQ0NLLGlDQUFRcEIsRUFBRSxJQUFGLENBQVIsU0FBb0JnQixJQUFwQjtBQUNBOztBQUVELFdBQUssU0FBTDtBQUNDSyxnQkFBUyxJQUFUO0FBTkY7QUFRQSxLQVREO0FBVUE7QUFoQlUsR0FBWjtBQWtCQTs7QUFFRDs7Ozs7O0FBTUEsVUFBU0MsV0FBVCxDQUFxQkMsT0FBckIsRUFBOEJiLE9BQTlCLEVBQXVDO0FBQ3RDYSxVQUFRQyxLQUFSO0FBQ0F4QixJQUFFbUIsSUFBRixDQUFPVCxPQUFQLEVBQWdCLFVBQUNlLEtBQUQsRUFBUUMsTUFBUixFQUFtQjtBQUNsQ0gsV0FBUUksTUFBUixDQUFlLElBQUlDLE1BQUosQ0FBV0YsT0FBT0csSUFBbEIsRUFBd0JILE9BQU9JLEtBQS9CLENBQWY7QUFDQSxHQUZEO0FBR0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNWLE9BQVQsQ0FBaUJHLE9BQWpCLEVBQTBCUSxHQUExQixFQUErQjtBQUM5QkEsUUFBTUEsT0FBT1IsUUFBUXpCLElBQVIsQ0FBYSxRQUFiLENBQWI7O0FBRUEsTUFBSSxDQUFDaUMsR0FBTCxFQUFVO0FBQ1QsU0FBTSxJQUFJYixLQUFKLENBQVUsdUVBQVYsQ0FBTjtBQUNBOztBQUVESyxVQUFRekIsSUFBUixDQUFhLFFBQWIsRUFBdUJpQyxHQUF2Qjs7QUFFQS9CLElBQUVnQyxPQUFGLENBQVVELEdBQVYsRUFDRUUsSUFERixDQUNPLFVBQVNDLFFBQVQsRUFBbUI7QUFDeEJaLGVBQVlDLE9BQVosRUFBcUJXLFFBQXJCO0FBQ0FYLFdBQVEsQ0FBUixFQUFXWSxJQUFYLENBQWdCQyxNQUFoQjtBQUNBYixXQUFRYyxPQUFSLENBQWdCLFFBQWhCO0FBQ0EsR0FMRixFQU1FQyxJQU5GLENBTU8sVUFBU0MsS0FBVCxFQUFnQkMsVUFBaEIsRUFBNEJDLFdBQTVCLEVBQXlDO0FBQzlDN0MsT0FBSU8sSUFBSixDQUFTdUMsS0FBVCxDQUFlQyxLQUFmLENBQXFCLDJCQUFyQixFQUFrREosS0FBbEQsRUFBeURDLFVBQXpELEVBQXFFQyxXQUFyRTtBQUNBLEdBUkY7QUFTQTs7QUFFRDs7Ozs7QUFLQSxVQUFTcEIsUUFBVCxDQUFrQnVCLE1BQWxCLEVBQTBCO0FBQ3pCLE1BQUlBLE9BQU9ULElBQVAsS0FBZ0JVLFNBQXBCLEVBQStCO0FBQzlCLFNBQU0sSUFBSTNCLEtBQUosQ0FBVSxxRkFBVixFQUFpRzBCLE1BQWpHLENBQU47QUFDQTs7QUFFREEsU0FBT1QsSUFBUCxDQUFZQyxNQUFaOztBQUVBO0FBQ0FVLDRCQUEwQkMsS0FBMUIsQ0FBZ0MvQyxFQUFFNEMsT0FBT0ksVUFBVCxFQUFxQkMsSUFBckIsQ0FBMEIsTUFBMUIsRUFBa0MsQ0FBbEMsQ0FBaEM7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTSCx5QkFBVCxHQUFxQztBQUNwQyxNQUFNSSxjQUFjbEQsRUFBRSxJQUFGLEVBQVFtRCxPQUFSLENBQWdCLGFBQWhCLENBQXBCO0FBQ0EsTUFBTUMsdUJBQXVCRixZQUFZRCxJQUFaLENBQWlCLGVBQWpCLEVBQWtDSSxNQUFsQyxLQUE2Q0gsWUFBWUQsSUFBWixDQUFpQixNQUFqQixFQUF5QkksTUFBbkc7QUFDQSxNQUFNQyw0QkFBNEJKLFlBQVlELElBQVosQ0FBaUIsZUFBakIsRUFBa0NJLE1BQWxDLEdBQTJDLENBQTdFO0FBQ0EsTUFBTUUscUJBQXFCTCxZQUFZRCxJQUFaLENBQWlCLGFBQWpCLENBQTNCOztBQUVBTSxxQkFBbUJDLFdBQW5CLENBQStCLGdCQUEvQjs7QUFFQSxNQUFJSixvQkFBSixFQUEwQjtBQUN6QkYsZUFDRU8sUUFERixDQUNXLGNBRFgsRUFFRUMsUUFGRixDQUVXLE1BRlgsRUFHRTdCLElBSEYsQ0FHT2pDLElBQUlPLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGNBQXhCLEVBQXdDLGNBQXhDLENBSFA7QUFJQSxHQUxELE1BS08sSUFBSWlELHlCQUFKLEVBQStCO0FBQ3JDQyxzQkFBbUJJLFFBQW5CLENBQTRCLGdCQUE1QjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBaEUsUUFBT2lFLElBQVAsR0FBYyxVQUFTM0IsSUFBVCxFQUFlO0FBQzVCO0FBQ0FyQjs7QUFFQTtBQUNBYixRQUFNa0QsSUFBTixDQUFXLDhCQUFYLEVBQTJDOUIsSUFBM0MsQ0FBZ0QsWUFBVztBQUMxRCxPQUFNSSxVQUFVdkIsRUFBRSxJQUFGLENBQWhCOztBQUVBdUIsV0FBUXNDLFVBQVIsQ0FBbUIsNEJBQW5COztBQUVBO0FBQ0F0QyxXQUFRdUMsVUFBUixDQUFtQnBELE9BQW5COztBQUVBLE9BQUlhLFFBQVF6QixJQUFSLENBQWEsb0JBQWIsTUFBdUMrQyxTQUEzQyxFQUFzRDtBQUNyRDtBQUNBdEIsWUFBUXpCLElBQVIsQ0FBYSxRQUFiLEVBQXVCeUIsUUFBUXpCLElBQVIsQ0FBYSxvQkFBYixDQUF2QjtBQUNBeUIsWUFBUXNDLFVBQVIsQ0FBbUIsMEJBQW5COztBQUVBO0FBQ0E3RCxNQUFFZ0MsT0FBRixDQUFVVCxRQUFRekIsSUFBUixDQUFhLG9CQUFiLENBQVYsRUFDRW1DLElBREYsQ0FDTyxVQUFTQyxRQUFULEVBQW1CO0FBQ3hCWixpQkFBWUMsT0FBWixFQUFxQlcsUUFBckI7QUFDQVgsYUFBUSxDQUFSLEVBQVdZLElBQVgsQ0FBZ0I0QixNQUFoQjtBQUNBeEMsYUFBUXVDLFVBQVIsQ0FBbUJwRCxPQUFuQjtBQUNBYSxhQUFRYyxPQUFSLENBQWdCLFFBQWhCO0FBQ0EsS0FORixFQU9FQyxJQVBGLENBT08sVUFBU0MsS0FBVCxFQUFnQkMsVUFBaEIsRUFBNEJDLFdBQTVCLEVBQXlDO0FBQzlDN0MsU0FBSU8sSUFBSixDQUFTdUMsS0FBVCxDQUFlQyxLQUFmLENBQXFCLDJCQUFyQixFQUFrREosS0FBbEQsRUFBeURDLFVBQXpELEVBQXFFQyxXQUFyRTtBQUNBLEtBVEY7QUFVQTtBQUNELEdBekJEOztBQTJCQVI7QUFDQSxFQWpDRDs7QUFtQ0E7QUFDQTtBQUNBbEMsT0FBTWlFLEVBQU4sQ0FBUyxPQUFULEVBQWtCLG1CQUFsQixFQUF1Q2xCLHlCQUF2Qzs7QUFFQSxRQUFPbkQsTUFBUDtBQUVBLENBak5EIiwiZmlsZSI6Im11bHRpX3NlbGVjdC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBtdWx0aV9zZWxlY3QuanMgMjAxNi0xMS0wOFxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiAjIyBNdWx0aSBTZWxlY3QgV2lkZ2V0XHJcbiAqXHJcbiAqIFRoaXMgbW9kdWxlIHNlcnZlcyBhcyBhIHdyYXBwZXIgb2YgU3Vtb1NlbGVjdCwgYSBqUXVlcnkgcGx1Z2luIHRoYXQgcHJvdmlkZXMgZW5oYW5jZWQgc2VsZWN0LWJveCBmdW5jdGlvbmFsaXR5LlxyXG4gKiBCaW5kIHRoaXMgd2lkZ2V0IHRvIGEgcGFyZW50IGNvbnRhaW5lciBhbmQgbWFyayBlYWNoIGNoaWxkIHNlbGVjdC1ib3ggZWxlbWVudCB3aXRoIHRoZSBgZGF0YS1tdWx0aV9zZWxlY3QtaW5zdGFuY2VgXHJcbiAqIGF0dHJpYnV0ZS5cclxuICpcclxuICogQWZ0ZXIgdGhlIGluaXRpYWxpemF0aW9uIG9mIHRoZSB3aWRnZXQgYWxsIHRoZSBtYXJrZWQgZWxlbWVudHMgd2lsbCBiZSBjb252ZXJ0ZWQgaW50byBTdW1vU2VsZWN0IGluc3RhbmNlcy5cclxuICpcclxuICogIyMjIE9wdGlvbnNcclxuICpcclxuICogKipPcHRpb25zIFNvdXJjZSB8IGBkYXRhLW11bHRpX3NlbGVjdC1zb3VyY2VgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxyXG4gKlxyXG4gKiBQcm92aWRlIGEgVVJMIHRoYXQgd2lsbCBiZSB1c2VkIHRvIGZldGNoIHRoZSBvcHRpb25zIG9mIHRoZSBzZWxlY3QgYm94LiBUaGUgd2lkZ2V0IHdpbGwgcGVyZm9ybSBhIEdFVCByZXF1ZXN0IHRvXHJcbiAqIHRoZSBwcm92aWRlZCBkZXN0aW5hdGlvbiBhbmQgZXhwZWN0cyBhIEpTT04gYXJyYXkgd2l0aCB0aGUgb3B0aW9uczpcclxuICpcclxuICogW1xyXG4gKiAgIHtcclxuICogICAgIFwidmFsdWVcIjogXCIxXCIsIFxyXG4gKiAgICAgXCJ0ZXh0XCI6IFwiT3B0aW9uICMxXCJcclxuICogICB9LFxyXG4gKiAgIHtcclxuICogICAgIFwidmFsdWVcIjogXCIyXCIsIFxyXG4gKiAgICAgXCJ0ZXh0XCI6IFwiT3B0aW9uICMyXCJcclxuICogICB9XHJcbiAqIF1cclxuICpcclxuICogWW91IGNhbiBhbHNvIHBhc3Mgb3RoZXIgY29uZmlndXJhdGlvbiBkaXJlY3RseSBpbiB0aGUgcGFyZW50IGVsZW1lbnQgd2hpY2ggd2lsbCBiZSB1c2VkIGZvciBldmVyeSBjaGlsZCBpbnN0YW5jZS5cclxuICpcclxuICpcclxuICogIyMjIE1ldGhvZHNcclxuICpcclxuICogKipSZWxvYWQgT3B0aW9ucyBbQUpBWF0qKlxyXG4gKlxyXG4gKiBZb3UgY2FuIHVzZSB0aGlzIG1ldGhvZCB0byByZWZyZXNoIHRoZSBvcHRpb25zIGZyb20gdGhlIGFscmVhZHkgcHJvdmlkZWQgZGF0YS1tdWx0aV9zZWxlY3Qtc291cmNlIG9yIGJ5IHByb3ZpZGluZ1xyXG4gKiBhIG5ldyBVUkwgd2hpY2ggd2lsbCBhbHNvIGJlIHNldCBhcyB0aGUgZGF0YS1zb3VyY2Ugb2YgdGhlIGVsZW1lbnQuIElmIHRoZSBtdWx0aSBzZWxlY3QgaGFzIG5vIFVSTCB0aGVuIGl0IHdpbGwganVzdFxyXG4gKiBzeW5jIGl0cyB2YWx1ZXMgd2l0aCB0aGUgc2VsZWN0IGVsZW1lbnQuXHJcbiAqXHJcbiAqICogYGBganNcclxuICogJCgnI215LW11bHRpLXNlbGVjdCcpLm11bHRpX3NlbGVjdCgncmVsb2FkJywgJ2h0dHA6Ly9zaG9wLmRlL29wdGlvbnMvc291cmNlL3VybCcpO1xyXG4gKiBgYGBcclxuICpcclxuICogKipSZWZyZXNoIE9wdGlvbnMqKlxyXG4gKlxyXG4gKiBVcGRhdGUgdGhlIG11bHRpLXNlbGVjdCB3aWRnZXQgd2l0aCB0aGUgc3RhdGUgb2YgdGhlIG9yaWdpbmFsIHNlbGVjdCBlbGVtZW50LiBUaGlzIG1ldGhvZCBpcyB1c2VmdWwgYWZ0ZXIgcGVyZm9ybWluZ1xyXG4gKiBjaGFuZ2VzIGluIHRoZSBvcmlnaW5hbCBlbGVtZW50IGFuZCBuZWVkIHRvIGRpc3BsYXkgdGhlbSBpbiB0aGUgbXVsdGktc2VsZWN0IHdpZGdldC5cclxuICpcclxuICogYGBganNcclxuICogJCgnI215LW11bHRpLXNlbGVjdCcpLm11bHRpX3NlbGVjdCgncmVmcmVzaCcpO1xyXG4gKiBgYGBcclxuICpcclxuICogIyMjIEV2ZW50c1xyXG4gKiBgYGBqYXZhc2NyaXB0XHJcbiAqIC8vIFRyaWdnZXJlZCB3aGVuIHRoZSBtdWx0aS1zZWxlY3Qgd2lkZ2V0IGhhcyBwZXJmb3JtZWQgYSBcInJlbG9hZFwiIG1ldGhvZCAoYWZ0ZXIgdGhlIEFKQVggY2FsbCkuXHJcbiAqICQoJyNteS1tdWx0aS1zZWxlY3QnKS5vbigncmVsb2FkJywgZnVuY3Rpb24oZXZlbnQpIHt9KTtcclxuICogYGBgXHJcbiAqXHJcbiAqICMjIyBFeGFtcGxlXHJcbiAqXHJcbiAqIGBgYGh0bWxcclxuICogPGZvcm0gZGF0YS1neC13aWRnZXQ9XCJtdWx0aV9zZWxlY3RcIj5cclxuICogICA8c2VsZWN0IGRhdGEtbXVsdGlfc2VsZWN0LWluc3RhbmNlIGRhdGEtbXVsdGlfc2VsZWN0LXNvdXJjZT1cImh0dHA6Ly9zaG9wLmRlL29wdGlvbnMtc291cmNlLXVybFwiPjwvc2VsZWN0PlxyXG4gKiA8L2Zvcm0+XHJcbiAqIGBgYFxyXG4gKlxyXG4gKiB7QGxpbmsgaHR0cDovL2hlbWFudG5lZ2kuZ2l0aHViLmlvL2pxdWVyeS5zdW1vc2VsZWN0fVxyXG4gKlxyXG4gKiBAbW9kdWxlIEFkbWluL1dpZGdldHMvbXVsdGlfc2VsZWN0XHJcbiAqIEByZXF1aXJlcyBqUXVlcnktU3Vtb1NlbGVjdFxyXG4gKi9cclxuZ3gud2lkZ2V0cy5tb2R1bGUoXHJcblx0J211bHRpX3NlbGVjdCcsIFxyXG5cdFxyXG5cdFtcclxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9zdW1vc2VsZWN0L3N1bW9zZWxlY3QubWluLmNzc2AsXHJcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3Ivc3Vtb3NlbGVjdC9qcXVlcnkuc3Vtb3NlbGVjdC5taW4uanNgXHJcblx0XSwgXHJcblx0XHJcblx0ZnVuY3Rpb24oZGF0YSkge1xyXG5cdFxyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBWQVJJQUJMRVNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIERlZmF1bHQgT3B0aW9uc1xyXG5cdCAqXHJcblx0ICogQHR5cGUge09iamVjdH1cclxuXHQgKi9cclxuXHRjb25zdCBkZWZhdWx0cyA9IHtcclxuXHRcdHBsYWNlaG9sZGVyOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnU0VMRUNUJywgJ2dlbmVyYWwnKSxcclxuXHRcdHNlbGVjdEFsbDogdHJ1ZSxcclxuXHRcdGNzdkRpc3BDb3VudDogMixcclxuXHRcdGNhcHRpb25Gb3JtYXQ6IGB7MH0gJHtqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc2VsZWN0ZWQnLCAnYWRtaW5fbGFiZWxzJyl9YCxcclxuXHRcdGxvY2FsZTogW1xyXG5cdFx0XHQnT0snLFxyXG5cdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQ0FOQ0VMJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1NFTEVDVF9BTEwnLCAnZ2VuZXJhbCcpXHJcblx0XHRdXHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBGaW5hbCBPcHRpb25zXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdCAqXHJcblx0ICogQHR5cGUge09iamVjdH1cclxuXHQgKi9cclxuXHRjb25zdCBtb2R1bGUgPSB7fTtcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBGVU5DVElPTlNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBBZGQgdGhlIFwibXVsdGlfc2VsZWN0XCIgbWV0aG9kIHRvIHRoZSBqUXVlcnkgcHJvdG90eXBlLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9hZGRQdWJsaWNNZXRob2QoKSB7XHJcblx0XHRpZiAoJC5mbi5tdWx0aV9zZWxlY3QpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQkLmZuLmV4dGVuZCh7XHJcblx0XHRcdG11bHRpX3NlbGVjdDogZnVuY3Rpb24oYWN0aW9uLCAuLi5hcmdzKSB7XHJcblx0XHRcdFx0aWYgKCEkKHRoaXMpLmlzKCdzZWxlY3QnKSkge1xyXG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdDYWxsZWQgdGhlIFwibXVsdGlfc2VsZWN0XCIgbWV0aG9kIG9uIGFuIGludmFsaWQgb2JqZWN0IChzZWxlY3QgYm94IGV4cGVjdGVkKS4nKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JC5lYWNoKHRoaXMsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0c3dpdGNoIChhY3Rpb24pIHtcclxuXHRcdFx0XHRcdFx0Y2FzZSAncmVsb2FkJzpcclxuXHRcdFx0XHRcdFx0XHRfcmVsb2FkKCQodGhpcyksIC4uLmFyZ3MpO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Y2FzZSAncmVmcmVzaCc6XHJcblx0XHRcdFx0XHRcdFx0X3JlZnJlc2godGhpcyk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBGaWxsIGEgc2VsZWN0IGJveCB3aXRoIHRoZSBwcm92aWRlZCBvcHRpb25zLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtqUXVlcnl9ICRzZWxlY3QgVGhlIHNlbGVjdCBib3ggdG8gYmUgZmlsbGVkLlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zIEFycmF5IHdpdGggeyB2YWx1ZTogXCIuLi5cIiwgXCJ0ZXh0XCI6IFwiLi4uXCIgfSBlbnRyaWVzLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9maWxsU2VsZWN0KCRzZWxlY3QsIG9wdGlvbnMpIHtcclxuXHRcdCRzZWxlY3QuZW1wdHkoKTtcclxuXHRcdCQuZWFjaChvcHRpb25zLCAoaW5kZXgsIG9wdGlvbikgPT4ge1xyXG5cdFx0XHQkc2VsZWN0LmFwcGVuZChuZXcgT3B0aW9uKG9wdGlvbi50ZXh0LCBvcHRpb24udmFsdWUpKTtcclxuXHRcdH0pO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBSZWxvYWQgdGhlIG9wdGlvbnMgZnJvbSB0aGUgc291cmNlIChkYXRhIHByb3BlcnR5KSBvciB0aGUgcHJvdmlkZWQgVVJMLFxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHVybCBPcHRpb25hbCwgaWYgcHJvdmlkZWQgaXQgd2lsbCBiZSB1c2VkIGFzIHRoZSBzb3VyY2Ugb2YgdGhlIGRhdGEgYW5kIHdpbGwgYWxzbyB1cGRhdGUgdGhlXHJcblx0ICogZGF0YS1zb3VyY2UgcHJvcGVydHkgb2YgdGhlIGVsZW1lbnQuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gX3JlbG9hZCgkc2VsZWN0LCB1cmwpIHtcclxuXHRcdHVybCA9IHVybCB8fCAkc2VsZWN0LmRhdGEoJ3NvdXJjZScpO1xyXG5cdFx0XHJcblx0XHRpZiAoIXVybCkge1xyXG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ011bHRpIFNlbGVjdCBSZWxvYWQ6IE5laXRoZXIgVVJMIG5vciBkYXRhLXNvdXJjZSBjb250YWluIGEgVVJMIHZhbHVlLicpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQkc2VsZWN0LmRhdGEoJ3NvdXJjZScsIHVybCk7XHJcblx0XHRcclxuXHRcdCQuZ2V0SlNPTih1cmwpXHJcblx0XHRcdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XHJcblx0XHRcdFx0X2ZpbGxTZWxlY3QoJHNlbGVjdCwgcmVzcG9uc2UpO1xyXG5cdFx0XHRcdCRzZWxlY3RbMF0uc3Vtby5yZWxvYWQoKTtcclxuXHRcdFx0XHQkc2VsZWN0LnRyaWdnZXIoJ3JlbG9hZCcpO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQuZmFpbChmdW5jdGlvbihqcXhociwgdGV4dFN0YXR1cywgZXJyb3JUaHJvd24pIHtcclxuXHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5lcnJvcignTXVsdGkgU2VsZWN0IEFKQVggRXJyb3I6ICcsIGpxeGhyLCB0ZXh0U3RhdHVzLCBlcnJvclRocm93bik7XHJcblx0XHRcdH0pO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBSZWZyZXNoIHRoZSBtdWx0aSBzZWxlY3QgaW5zdGFuY2UgZGVwZW5kaW5nIHRoZSBzdGF0ZSBvZiB0aGUgb3JpZ2luYWwgc2VsZWN0IGVsZW1lbnQuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge05vZGV9IHNlbGVjdCBUaGUgRE9NIGVsZW1lbnQgdG8gYmUgcmVmcmVzaGVkLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9yZWZyZXNoKHNlbGVjdCkge1xyXG5cdFx0aWYgKHNlbGVjdC5zdW1vID09PSB1bmRlZmluZWQpIHtcclxuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdNdWx0aSBTZWxlY3QgUmVmcmVzaDogVGhlIHByb3ZpZGVkIHNlbGVjdCBlbGVtZW50IGlzIG5vdCBhbiBpbnN0YW5jZSBvZiBTdW1vU2VsZWN0LicsIHNlbGVjdCk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdHNlbGVjdC5zdW1vLnJlbG9hZCgpO1xyXG5cdFx0XHJcblx0XHQvLyBVcGRhdGUgdGhlIGNhcHRpb24gYnkgc2ltdWxhdGluZyBhIGNsaWNrIGluIGFuIFwiLm9wdFwiIGVsZW1lbnQuICBcclxuXHRcdF9vdmVycmlkZVNlbGVjdEFsbENhcHRpb24uYXBwbHkoJChzZWxlY3QucGFyZW50Tm9kZSkuZmluZCgnLm9wdCcpWzBdKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogT3ZlcnJpZGUgdGhlIG11bHRpIHNlbGVjdCBjYXB0aW9uIHdoZW4gYWxsIGVsZW1lbnRzIGFyZSBzZWxlY3RlZC5cclxuXHQgKlxyXG5cdCAqIFRoaXMgY2FsbGJhY2sgd2lsbCBvdmVycmlkZSB0aGUgY2FwdGlvbiBiZWNhdXNlIFN1bW9TZWxlY3QgZG9lcyBub3QgcHJvdmlkZSBhIHNldHRpbmcgZm9yIHRoaXMgdGV4dC5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb3ZlcnJpZGVTZWxlY3RBbGxDYXB0aW9uKCkge1xyXG5cdFx0Y29uc3QgJG9wdFdyYXBwZXIgPSAkKHRoaXMpLnBhcmVudHMoJy5vcHRXcmFwcGVyJyk7XHJcblx0XHRjb25zdCBhbGxDaGVja2JveGVzQ2hlY2tlZCA9ICRvcHRXcmFwcGVyLmZpbmQoJy5vcHQuc2VsZWN0ZWQnKS5sZW5ndGggPT09ICRvcHRXcmFwcGVyLmZpbmQoJy5vcHQnKS5sZW5ndGg7XHJcblx0XHRjb25zdCBhdExlYXN0T25lQ2hlY2tib3hDaGVja2VkID0gJG9wdFdyYXBwZXIuZmluZCgnLm9wdC5zZWxlY3RlZCcpLmxlbmd0aCA+IDA7XHJcblx0XHRjb25zdCAkc2VsZWN0QWxsQ2hlY2tib3ggPSAkb3B0V3JhcHBlci5maW5kKCcuc2VsZWN0LWFsbCcpO1xyXG5cdFx0XHJcblx0XHQkc2VsZWN0QWxsQ2hlY2tib3gucmVtb3ZlQ2xhc3MoJ3BhcnRpYWwtc2VsZWN0Jyk7XHJcblx0XHRcclxuXHRcdGlmIChhbGxDaGVja2JveGVzQ2hlY2tlZCkge1xyXG5cdFx0XHQkb3B0V3JhcHBlclxyXG5cdFx0XHRcdC5zaWJsaW5ncygnLkNhcHRpb25Db250JylcclxuXHRcdFx0XHQuY2hpbGRyZW4oJ3NwYW4nKVxyXG5cdFx0XHRcdC50ZXh0KGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdhbGxfc2VsZWN0ZWQnLCAnYWRtaW5fbGFiZWxzJykpO1xyXG5cdFx0fSBlbHNlIGlmIChhdExlYXN0T25lQ2hlY2tib3hDaGVja2VkKSB7XHJcblx0XHRcdCRzZWxlY3RBbGxDaGVja2JveC5hZGRDbGFzcygncGFydGlhbC1zZWxlY3QnKTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gSU5JVElBTElaQVRJT05cclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdC8vIEFkZCBwdWJsaWMgbW9kdWxlIG1ldGhvZC4gIFxyXG5cdFx0X2FkZFB1YmxpY01ldGhvZCgpO1xyXG5cdFx0XHJcblx0XHQvLyBJbml0aWFsaXplIHRoZSBlbGVtZW50cy4gXHJcblx0XHQkdGhpcy5maW5kKCdbZGF0YS1tdWx0aV9zZWxlY3QtaW5zdGFuY2VdJykuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0Y29uc3QgJHNlbGVjdCA9ICQodGhpcyk7XHJcblx0XHRcdFxyXG5cdFx0XHQkc2VsZWN0LnJlbW92ZUF0dHIoJ2RhdGEtbXVsdGlfc2VsZWN0LWluc3RhbmNlJyk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBJbnN0YW50aWF0ZSB0aGUgd2lkZ2V0IHdpdGhvdXQgYW4gQUpBWCByZXF1ZXN0LlxyXG5cdFx0XHQkc2VsZWN0LlN1bW9TZWxlY3Qob3B0aW9ucyk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoJHNlbGVjdC5kYXRhKCdtdWx0aV9zZWxlY3RTb3VyY2UnKSAhPT0gdW5kZWZpbmVkKSB7XHJcblx0XHRcdFx0Ly8gUmVtb3ZlIHRoZSBkYXRhIGF0dHJpYnV0ZSBhbmQgc3RvcmUgdGhlIHZhbHVlIGludGVybmFsbHkgd2l0aCB0aGUgJ3NvdXJjZScga2V5LiBcclxuXHRcdFx0XHQkc2VsZWN0LmRhdGEoJ3NvdXJjZScsICRzZWxlY3QuZGF0YSgnbXVsdGlfc2VsZWN0U291cmNlJykpO1xyXG5cdFx0XHRcdCRzZWxlY3QucmVtb3ZlQXR0cignZGF0YS1tdWx0aV9zZWxlY3Qtc291cmNlJyk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gRmV0Y2ggdGhlIG9wdGlvbnMgd2l0aCBhbiBBSkFYIHJlcXVlc3QuXHJcblx0XHRcdFx0JC5nZXRKU09OKCRzZWxlY3QuZGF0YSgnbXVsdGlfc2VsZWN0U291cmNlJykpXHJcblx0XHRcdFx0XHQuZG9uZShmdW5jdGlvbihyZXNwb25zZSkge1xyXG5cdFx0XHRcdFx0XHRfZmlsbFNlbGVjdCgkc2VsZWN0LCByZXNwb25zZSk7XHJcblx0XHRcdFx0XHRcdCRzZWxlY3RbMF0uc3Vtby51bmxvYWQoKTtcclxuXHRcdFx0XHRcdFx0JHNlbGVjdC5TdW1vU2VsZWN0KG9wdGlvbnMpO1xyXG5cdFx0XHRcdFx0XHQkc2VsZWN0LnRyaWdnZXIoJ3JlbG9hZCcpO1xyXG5cdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdC5mYWlsKGZ1bmN0aW9uKGpxeGhyLCB0ZXh0U3RhdHVzLCBlcnJvclRocm93bikge1xyXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5lcnJvcignTXVsdGkgU2VsZWN0IEFKQVggRXJyb3I6ICcsIGpxeGhyLCB0ZXh0U3RhdHVzLCBlcnJvclRocm93bik7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGRvbmUoKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8vIFdoZW4gdGhlIHVzZXIgY2xpY2tzIG9uIHRoZSBcIlNlbGVjdCBBbGxcIiBvcHRpb24gdXBkYXRlIHRoZSB0ZXh0IHdpdGggYSBjdXN0b20gdHJhbnNsYXRpb25zLiBUaGlzIGhhcyB0byBcclxuXHQvLyBiZSBkb25lIG1hbnVhbGx5IGJlY2F1c2UgdGhlcmUgaXMgbm8gb3B0aW9uIGZvciB0aGlzIHRleHQgaW4gU3Vtb1NlbGVjdC4gXHJcblx0JHRoaXMub24oJ2NsaWNrJywgJy5zZWxlY3QtYWxsLCAub3B0JywgX292ZXJyaWRlU2VsZWN0QWxsQ2FwdGlvbik7XHJcblx0XHJcblx0cmV0dXJuIG1vZHVsZTtcclxuXHRcclxufSk7ICJdfQ==
