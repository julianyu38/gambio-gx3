'use strict';

/* --------------------------------------------------------------
 datatable.js 2016-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## DataTable Widget
 *
 * Wrapper widget for the jquery datatables plugin. You can create a whole
 * data table with sort, search, pagination and other useful utilities.
 *
 * Official DataTables Website: {@link http://www.datatables.net}
 * 
 * ### Options
 *
 * **Language | `data-datatable-language` | Object | Optional**
 *
 * Provide the default language for the data table. If no language is provided, the language
 * defaults to german. [Click here](https://datatables.net/reference/option/language) to see
 * how the language object should look like.
 *
 * ### Example
 *
 * ```html
 * <table data-gx-widget="datatable">
 *   <thead>
 *     <tr>
 *       <th>Column 1</th>
 *       <th>Column 2</th>
 *     </tr>
 *   </thead>
 *   <tbody>
 *     <tr>
 *       <td>Cell 1</td>
 *       <td>Cell 2</td>
 *     </tr>
 *   </tbody>
 * </table>
 * ```
 *
 * *Place the ".disable-sort" class to <th> elements that shouldn't be sorted.*
 *
 * @module Admin/Widgets/datatable
 * @requires jQuery-DataTables-Plugin
 */
gx.widgets.module('datatable', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', 'datatable'],

/** @lends module:Widgets/datatable */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * DataTable plugin handler used for triggering API operations.
  *
  * @type {object}
  */
	$table = {},


	/**
  * Default options of Widget
  *
  * @type {object}
  */
	defaults = {
		language: jse.libs.datatable.getGermanTranslation()
	},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**  Define Views Data */
	module.view = {};

	/** Define Models Data */
	module.model = {};

	/** Define Dependencies */
	module.dependencies = {};

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		$table = $this.DataTable(options);
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGF0YWJsZS5qcyJdLCJuYW1lcyI6WyJneCIsIndpZGdldHMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJHRhYmxlIiwiZGVmYXVsdHMiLCJsYW5ndWFnZSIsImxpYnMiLCJkYXRhdGFibGUiLCJnZXRHZXJtYW5UcmFuc2xhdGlvbiIsIm9wdGlvbnMiLCJleHRlbmQiLCJ2aWV3IiwibW9kZWwiLCJkZXBlbmRlbmNpZXMiLCJpbml0IiwiZG9uZSIsIkRhdGFUYWJsZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0NBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxXQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixtREFFSUQsSUFBSUMsTUFGUixrREFHQyxXQUhELENBSEQ7O0FBU0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFVBQVMsRUFiVjs7O0FBZUM7Ozs7O0FBS0FDLFlBQVc7QUFDVkMsWUFBVVAsSUFBSVEsSUFBSixDQUFTQyxTQUFULENBQW1CQyxvQkFBbkI7QUFEQSxFQXBCWjs7O0FBd0JDOzs7OztBQUtBQyxXQUFVUCxFQUFFUSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJOLFFBQW5CLEVBQTZCSixJQUE3QixDQTdCWDs7O0FBK0JDOzs7OztBQUtBSCxVQUFTLEVBcENWOztBQXNDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQUEsUUFBT2MsSUFBUCxHQUFjLEVBQWQ7O0FBRUE7QUFDQWQsUUFBT2UsS0FBUCxHQUFlLEVBQWY7O0FBRUE7QUFDQWYsUUFBT2dCLFlBQVAsR0FBc0IsRUFBdEI7O0FBRUE7OztBQUdBaEIsUUFBT2lCLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJaLFdBQVNGLE1BQU1lLFNBQU4sQ0FBZ0JQLE9BQWhCLENBQVQ7QUFDQU07QUFDQSxFQUhEOztBQUtBO0FBQ0EsUUFBT2xCLE1BQVA7QUFDQSxDQWhGRiIsImZpbGUiOiJkYXRhdGFibGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGRhdGF0YWJsZS5qcyAyMDE2LTAyLTIzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBEYXRhVGFibGUgV2lkZ2V0XG4gKlxuICogV3JhcHBlciB3aWRnZXQgZm9yIHRoZSBqcXVlcnkgZGF0YXRhYmxlcyBwbHVnaW4uIFlvdSBjYW4gY3JlYXRlIGEgd2hvbGVcbiAqIGRhdGEgdGFibGUgd2l0aCBzb3J0LCBzZWFyY2gsIHBhZ2luYXRpb24gYW5kIG90aGVyIHVzZWZ1bCB1dGlsaXRpZXMuXG4gKlxuICogT2ZmaWNpYWwgRGF0YVRhYmxlcyBXZWJzaXRlOiB7QGxpbmsgaHR0cDovL3d3dy5kYXRhdGFibGVzLm5ldH1cbiAqIFxuICogIyMjIE9wdGlvbnNcbiAqXG4gKiAqKkxhbmd1YWdlIHwgYGRhdGEtZGF0YXRhYmxlLWxhbmd1YWdlYCB8IE9iamVjdCB8IE9wdGlvbmFsKipcbiAqXG4gKiBQcm92aWRlIHRoZSBkZWZhdWx0IGxhbmd1YWdlIGZvciB0aGUgZGF0YSB0YWJsZS4gSWYgbm8gbGFuZ3VhZ2UgaXMgcHJvdmlkZWQsIHRoZSBsYW5ndWFnZVxuICogZGVmYXVsdHMgdG8gZ2VybWFuLiBbQ2xpY2sgaGVyZV0oaHR0cHM6Ly9kYXRhdGFibGVzLm5ldC9yZWZlcmVuY2Uvb3B0aW9uL2xhbmd1YWdlKSB0byBzZWVcbiAqIGhvdyB0aGUgbGFuZ3VhZ2Ugb2JqZWN0IHNob3VsZCBsb29rIGxpa2UuXG4gKlxuICogIyMjIEV4YW1wbGVcbiAqXG4gKiBgYGBodG1sXG4gKiA8dGFibGUgZGF0YS1neC13aWRnZXQ9XCJkYXRhdGFibGVcIj5cbiAqICAgPHRoZWFkPlxuICogICAgIDx0cj5cbiAqICAgICAgIDx0aD5Db2x1bW4gMTwvdGg+XG4gKiAgICAgICA8dGg+Q29sdW1uIDI8L3RoPlxuICogICAgIDwvdHI+XG4gKiAgIDwvdGhlYWQ+XG4gKiAgIDx0Ym9keT5cbiAqICAgICA8dHI+XG4gKiAgICAgICA8dGQ+Q2VsbCAxPC90ZD5cbiAqICAgICAgIDx0ZD5DZWxsIDI8L3RkPlxuICogICAgIDwvdHI+XG4gKiAgIDwvdGJvZHk+XG4gKiA8L3RhYmxlPlxuICogYGBgXG4gKlxuICogKlBsYWNlIHRoZSBcIi5kaXNhYmxlLXNvcnRcIiBjbGFzcyB0byA8dGg+IGVsZW1lbnRzIHRoYXQgc2hvdWxkbid0IGJlIHNvcnRlZC4qXG4gKlxuICogQG1vZHVsZSBBZG1pbi9XaWRnZXRzL2RhdGF0YWJsZVxuICogQHJlcXVpcmVzIGpRdWVyeS1EYXRhVGFibGVzLVBsdWdpblxuICovXG5neC53aWRnZXRzLm1vZHVsZShcblx0J2RhdGF0YWJsZScsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmpzYCxcblx0XHQnZGF0YXRhYmxlJ1xuXHRdLFxuXHRcblx0LyoqIEBsZW5kcyBtb2R1bGU6V2lkZ2V0cy9kYXRhdGFibGUgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIFdpZGdldCBSZWZlcmVuY2UgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGF0YVRhYmxlIHBsdWdpbiBoYW5kbGVyIHVzZWQgZm9yIHRyaWdnZXJpbmcgQVBJIG9wZXJhdGlvbnMuXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRhYmxlID0ge30sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBvcHRpb25zIG9mIFdpZGdldFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0ge1xuXHRcdFx0XHRsYW5ndWFnZToganNlLmxpYnMuZGF0YXRhYmxlLmdldEdlcm1hblRyYW5zbGF0aW9uKClcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgV2lkZ2V0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKiogIERlZmluZSBWaWV3cyBEYXRhICovXG5cdFx0bW9kdWxlLnZpZXcgPSB7fTtcblx0XHRcblx0XHQvKiogRGVmaW5lIE1vZGVscyBEYXRhICovXG5cdFx0bW9kdWxlLm1vZGVsID0ge307XG5cdFx0XG5cdFx0LyoqIERlZmluZSBEZXBlbmRlbmNpZXMgKi9cblx0XHRtb2R1bGUuZGVwZW5kZW5jaWVzID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIHdpZGdldCwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkdGFibGUgPSAkdGhpcy5EYXRhVGFibGUob3B0aW9ucyk7XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byBtb2R1bGUgZW5naW5lLlxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
