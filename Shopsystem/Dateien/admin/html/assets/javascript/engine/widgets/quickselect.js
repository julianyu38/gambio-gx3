'use strict';

/* --------------------------------------------------------------
 quickselect.js 2017-09-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## QuickSelect Widget
 *
 * This widget is a custom implementation of tabs functionality.
 *
 * The actual `<div>` which contains the tabs, has to have a CSS-Class named **quickselect-headline-wrapper**.
 * The tabs will be identified by this CSS-Class. The content of the tabs has to be in a `<div>` which has to have
 * a CSS-Class called **quickselect-content-wrapper**. The elements inside, have to be in the same order as the tabs.
 *
 * ### Widget Instance Options
 *
 * **Tab alignment | `data-quickselect-align` | string | Optional**
 *
 * This option can be used to align the tabs (not the content). In some cases it is necessary to align th tabs itself
 * to righ right.
 *
 * **Start tab | `data-quickselect-start` | integer | Optional**
 *
 * This option can be used to choose a starting tab. Mind that 0 is associated with the first tab content.
 *
 * ### Example
 *
 * ```html
 * <div data-gx-widget="quickselect" data-quickselect-align="left" data-quickselect-start="1">
 *     <!-- Tabs -->
 *     <div class="quickselect-headline-wrapper">
 *         <a href="#tab1">Tab #1</a>
 *         <a href="#tab2">Tab #2</a>
 *         <a href="#tab3">Tab #3</a>
 *     </div>
 *
 *     <!-- Content -->
 *     <div class="quickselect-content-wrapper">
 *         <div>Content of tab #1.</div>
 *         <div>Content of tab #2.</div>
 *         <div>Content of tab #3.</div>
 *     </div>
 * </div>
 * ```
 *
 * @module Admin/Widgets/quickselect
 */
gx.widgets.module('quickselect', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Headline Tags Selector
  *
  * @type {object}
  */
	$headlineTags = null,


	/**
  * Content Tags Selector
  *
  * @type {object}
  */
	$contentTags = null,


	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Click handler for the tabs onClick the content gets switched.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	var _clickHandler = function _clickHandler(event) {
		event.preventDefault();
		event.stopPropagation();

		$headlineTags.removeClass('active');

		var index = $(this).addClass('active').index();

		$contentTags.hide().eq(index).show();

		$this.trigger('shown:tab', { index: index });
	};

	/**
  * Handles external "show" event
  *
  * @param {object} event jQuery event object contains information of the event.
  * @param {number} tab index to show
  */
	var _showHandler = function _showHandler(event, index) {
		event.preventDefault();
		event.stopPropagation();
		$headlineTags.eq(index).trigger('click');
	};

	// ------------------------------------------------------------------------
	// INITIALIZE
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		$headlineTags = $this.children('.quickselect-headline-wrapper').children('a');

		$contentTags = $this.children('.quickselect-content-wrapper').children('div');

		$this.addClass('ui-tabs');
		$this.on('click', '.quickselect-headline-wrapper > a', _clickHandler);
		$this.on('show:tab', _showHandler);

		if (options.align == 'right') {
			$this.children('.quickselect-headline-wrapper').css("float", "right");
		}

		if (Number.isInteger(options.start)) {
			$headlineTags.eq(options.start).trigger('click');
		} else {
			$headlineTags.eq(0).trigger('click');
		}

		done();
	};

	// Return data to module engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInF1aWNrc2VsZWN0LmpzIl0sIm5hbWVzIjpbImd4Iiwid2lkZ2V0cyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkaGVhZGxpbmVUYWdzIiwiJGNvbnRlbnRUYWdzIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwiX2NsaWNrSGFuZGxlciIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJyZW1vdmVDbGFzcyIsImluZGV4IiwiYWRkQ2xhc3MiLCJoaWRlIiwiZXEiLCJzaG93IiwidHJpZ2dlciIsIl9zaG93SGFuZGxlciIsImluaXQiLCJkb25lIiwiY2hpbGRyZW4iLCJvbiIsImFsaWduIiwiY3NzIiwiTnVtYmVyIiwiaXNJbnRlZ2VyIiwic3RhcnQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMENBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxhQURELEVBR0MsRUFIRCxFQUtDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxpQkFBZ0IsSUFiakI7OztBQWVDOzs7OztBQUtBQyxnQkFBZSxJQXBCaEI7OztBQXNCQzs7Ozs7QUFLQUMsWUFBVyxFQTNCWjs7O0FBNkJDOzs7OztBQUtBQyxXQUFVSixFQUFFSyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCTCxJQUE3QixDQWxDWDs7O0FBb0NDOzs7OztBQUtBRCxVQUFTLEVBekNWOztBQTJDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsS0FBSVMsZ0JBQWdCLFNBQWhCQSxhQUFnQixDQUFTQyxLQUFULEVBQWdCO0FBQ25DQSxRQUFNQyxjQUFOO0FBQ0FELFFBQU1FLGVBQU47O0FBRUFSLGdCQUFjUyxXQUFkLENBQTBCLFFBQTFCOztBQUVBLE1BQUlDLFFBQVFYLEVBQUUsSUFBRixFQUNWWSxRQURVLENBQ0QsUUFEQyxFQUVWRCxLQUZVLEVBQVo7O0FBSUFULGVBQ0VXLElBREYsR0FFRUMsRUFGRixDQUVLSCxLQUZMLEVBR0VJLElBSEY7O0FBS0FoQixRQUFNaUIsT0FBTixDQUFjLFdBQWQsRUFBMkIsRUFBQ0wsWUFBRCxFQUEzQjtBQUNBLEVBaEJEOztBQWtCQTs7Ozs7O0FBTUEsS0FBSU0sZUFBZSxTQUFmQSxZQUFlLENBQVNWLEtBQVQsRUFBZ0JJLEtBQWhCLEVBQXVCO0FBQ3pDSixRQUFNQyxjQUFOO0FBQ0FELFFBQU1FLGVBQU47QUFDQVIsZ0JBQWNhLEVBQWQsQ0FBaUJILEtBQWpCLEVBQXdCSyxPQUF4QixDQUFnQyxPQUFoQztBQUNBLEVBSkQ7O0FBTUE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQW5CLFFBQU9xQixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCbEIsa0JBQWdCRixNQUNkcUIsUUFEYyxDQUNMLCtCQURLLEVBRWRBLFFBRmMsQ0FFTCxHQUZLLENBQWhCOztBQUlBbEIsaUJBQWVILE1BQ2JxQixRQURhLENBQ0osOEJBREksRUFFYkEsUUFGYSxDQUVKLEtBRkksQ0FBZjs7QUFJQXJCLFFBQU1hLFFBQU4sQ0FBZSxTQUFmO0FBQ0FiLFFBQU1zQixFQUFOLENBQVMsT0FBVCxFQUFrQixtQ0FBbEIsRUFBdURmLGFBQXZEO0FBQ0FQLFFBQU1zQixFQUFOLENBQVMsVUFBVCxFQUFxQkosWUFBckI7O0FBRUEsTUFBSWIsUUFBUWtCLEtBQVIsSUFBaUIsT0FBckIsRUFBOEI7QUFDN0J2QixTQUFNcUIsUUFBTixDQUFlLCtCQUFmLEVBQWdERyxHQUFoRCxDQUFvRCxPQUFwRCxFQUE2RCxPQUE3RDtBQUNBOztBQUVELE1BQUlDLE9BQU9DLFNBQVAsQ0FBaUJyQixRQUFRc0IsS0FBekIsQ0FBSixFQUFxQztBQUNwQ3pCLGlCQUNFYSxFQURGLENBQ0tWLFFBQVFzQixLQURiLEVBRUVWLE9BRkYsQ0FFVSxPQUZWO0FBR0EsR0FKRCxNQUtLO0FBQ0pmLGlCQUNFYSxFQURGLENBQ0ssQ0FETCxFQUVFRSxPQUZGLENBRVUsT0FGVjtBQUdBOztBQUVERztBQUNBLEVBN0JEOztBQStCQTtBQUNBLFFBQU90QixNQUFQO0FBQ0EsQ0F2SUYiLCJmaWxlIjoicXVpY2tzZWxlY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHF1aWNrc2VsZWN0LmpzIDIwMTctMDktMDVcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIFF1aWNrU2VsZWN0IFdpZGdldFxuICpcbiAqIFRoaXMgd2lkZ2V0IGlzIGEgY3VzdG9tIGltcGxlbWVudGF0aW9uIG9mIHRhYnMgZnVuY3Rpb25hbGl0eS5cbiAqXG4gKiBUaGUgYWN0dWFsIGA8ZGl2PmAgd2hpY2ggY29udGFpbnMgdGhlIHRhYnMsIGhhcyB0byBoYXZlIGEgQ1NTLUNsYXNzIG5hbWVkICoqcXVpY2tzZWxlY3QtaGVhZGxpbmUtd3JhcHBlcioqLlxuICogVGhlIHRhYnMgd2lsbCBiZSBpZGVudGlmaWVkIGJ5IHRoaXMgQ1NTLUNsYXNzLiBUaGUgY29udGVudCBvZiB0aGUgdGFicyBoYXMgdG8gYmUgaW4gYSBgPGRpdj5gIHdoaWNoIGhhcyB0byBoYXZlXG4gKiBhIENTUy1DbGFzcyBjYWxsZWQgKipxdWlja3NlbGVjdC1jb250ZW50LXdyYXBwZXIqKi4gVGhlIGVsZW1lbnRzIGluc2lkZSwgaGF2ZSB0byBiZSBpbiB0aGUgc2FtZSBvcmRlciBhcyB0aGUgdGFicy5cbiAqXG4gKiAjIyMgV2lkZ2V0IEluc3RhbmNlIE9wdGlvbnNcbiAqXG4gKiAqKlRhYiBhbGlnbm1lbnQgfCBgZGF0YS1xdWlja3NlbGVjdC1hbGlnbmAgfCBzdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogVGhpcyBvcHRpb24gY2FuIGJlIHVzZWQgdG8gYWxpZ24gdGhlIHRhYnMgKG5vdCB0aGUgY29udGVudCkuIEluIHNvbWUgY2FzZXMgaXQgaXMgbmVjZXNzYXJ5IHRvIGFsaWduIHRoIHRhYnMgaXRzZWxmXG4gKiB0byByaWdoIHJpZ2h0LlxuICpcbiAqICoqU3RhcnQgdGFiIHwgYGRhdGEtcXVpY2tzZWxlY3Qtc3RhcnRgIHwgaW50ZWdlciB8IE9wdGlvbmFsKipcbiAqXG4gKiBUaGlzIG9wdGlvbiBjYW4gYmUgdXNlZCB0byBjaG9vc2UgYSBzdGFydGluZyB0YWIuIE1pbmQgdGhhdCAwIGlzIGFzc29jaWF0ZWQgd2l0aCB0aGUgZmlyc3QgdGFiIGNvbnRlbnQuXG4gKlxuICogIyMjIEV4YW1wbGVcbiAqXG4gKiBgYGBodG1sXG4gKiA8ZGl2IGRhdGEtZ3gtd2lkZ2V0PVwicXVpY2tzZWxlY3RcIiBkYXRhLXF1aWNrc2VsZWN0LWFsaWduPVwibGVmdFwiIGRhdGEtcXVpY2tzZWxlY3Qtc3RhcnQ9XCIxXCI+XG4gKiAgICAgPCEtLSBUYWJzIC0tPlxuICogICAgIDxkaXYgY2xhc3M9XCJxdWlja3NlbGVjdC1oZWFkbGluZS13cmFwcGVyXCI+XG4gKiAgICAgICAgIDxhIGhyZWY9XCIjdGFiMVwiPlRhYiAjMTwvYT5cbiAqICAgICAgICAgPGEgaHJlZj1cIiN0YWIyXCI+VGFiICMyPC9hPlxuICogICAgICAgICA8YSBocmVmPVwiI3RhYjNcIj5UYWIgIzM8L2E+XG4gKiAgICAgPC9kaXY+XG4gKlxuICogICAgIDwhLS0gQ29udGVudCAtLT5cbiAqICAgICA8ZGl2IGNsYXNzPVwicXVpY2tzZWxlY3QtY29udGVudC13cmFwcGVyXCI+XG4gKiAgICAgICAgIDxkaXY+Q29udGVudCBvZiB0YWIgIzEuPC9kaXY+XG4gKiAgICAgICAgIDxkaXY+Q29udGVudCBvZiB0YWIgIzIuPC9kaXY+XG4gKiAgICAgICAgIDxkaXY+Q29udGVudCBvZiB0YWIgIzMuPC9kaXY+XG4gKiAgICAgPC9kaXY+XG4gKiA8L2Rpdj5cbiAqIGBgYFxuICpcbiAqIEBtb2R1bGUgQWRtaW4vV2lkZ2V0cy9xdWlja3NlbGVjdFxuICovXG5neC53aWRnZXRzLm1vZHVsZShcblx0J3F1aWNrc2VsZWN0Jyxcblx0XG5cdFtdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIFdpZGdldCBSZWZlcmVuY2Vcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogSGVhZGxpbmUgVGFncyBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCRoZWFkbGluZVRhZ3MgPSBudWxsLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIENvbnRlbnQgVGFncyBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCRjb250ZW50VGFncyA9IG51bGwsXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zIGZvciBXaWRnZXRcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIFdpZGdldCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciBmb3IgdGhlIHRhYnMgb25DbGljayB0aGUgY29udGVudCBnZXRzIHN3aXRjaGVkLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxuXHRcdCAqL1xuXHRcdHZhciBfY2xpY2tIYW5kbGVyID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFxuXHRcdFx0JGhlYWRsaW5lVGFncy5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcblx0XHRcdHZhciBpbmRleCA9ICQodGhpcylcblx0XHRcdFx0LmFkZENsYXNzKCdhY3RpdmUnKVxuXHRcdFx0XHQuaW5kZXgoKTtcblx0XHRcdFxuXHRcdFx0JGNvbnRlbnRUYWdzXG5cdFx0XHRcdC5oaWRlKClcblx0XHRcdFx0LmVxKGluZGV4KVxuXHRcdFx0XHQuc2hvdygpO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy50cmlnZ2VyKCdzaG93bjp0YWInLCB7aW5kZXh9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgZXh0ZXJuYWwgXCJzaG93XCIgZXZlbnRcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKiBAcGFyYW0ge251bWJlcn0gdGFiIGluZGV4IHRvIHNob3dcblx0XHQgKi9cblx0XHR2YXIgX3Nob3dIYW5kbGVyID0gZnVuY3Rpb24oZXZlbnQsIGluZGV4KSB7XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHQkaGVhZGxpbmVUYWdzLmVxKGluZGV4KS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaRVxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSB3aWRnZXQsIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JGhlYWRsaW5lVGFncyA9ICR0aGlzXG5cdFx0XHRcdC5jaGlsZHJlbignLnF1aWNrc2VsZWN0LWhlYWRsaW5lLXdyYXBwZXInKVxuXHRcdFx0XHQuY2hpbGRyZW4oJ2EnKTtcblx0XHRcdFxuXHRcdFx0JGNvbnRlbnRUYWdzID0gJHRoaXNcblx0XHRcdFx0LmNoaWxkcmVuKCcucXVpY2tzZWxlY3QtY29udGVudC13cmFwcGVyJylcblx0XHRcdFx0LmNoaWxkcmVuKCdkaXYnKTtcblx0XHRcdFxuXHRcdFx0JHRoaXMuYWRkQ2xhc3MoJ3VpLXRhYnMnKTtcblx0XHRcdCR0aGlzLm9uKCdjbGljaycsICcucXVpY2tzZWxlY3QtaGVhZGxpbmUtd3JhcHBlciA+IGEnLCBfY2xpY2tIYW5kbGVyKTtcblx0XHRcdCR0aGlzLm9uKCdzaG93OnRhYicsIF9zaG93SGFuZGxlcik7XG5cdFx0XHRcblx0XHRcdGlmIChvcHRpb25zLmFsaWduID09ICdyaWdodCcpIHtcblx0XHRcdFx0JHRoaXMuY2hpbGRyZW4oJy5xdWlja3NlbGVjdC1oZWFkbGluZS13cmFwcGVyJykuY3NzKFwiZmxvYXRcIiwgXCJyaWdodFwiKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKE51bWJlci5pc0ludGVnZXIob3B0aW9ucy5zdGFydCkpIHtcblx0XHRcdFx0JGhlYWRsaW5lVGFnc1xuXHRcdFx0XHRcdC5lcShvcHRpb25zLnN0YXJ0KVxuXHRcdFx0XHRcdC50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZSB7XG5cdFx0XHRcdCRoZWFkbGluZVRhZ3Ncblx0XHRcdFx0XHQuZXEoMClcblx0XHRcdFx0XHQudHJpZ2dlcignY2xpY2snKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZVxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
