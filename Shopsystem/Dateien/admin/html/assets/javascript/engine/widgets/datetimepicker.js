'use strict';

/* --------------------------------------------------------------
 datetimepicker.js 2016-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Datetimepicker Widget
 *
 * This widget will convert itself or multiple elements into datetimepicker instances. Check the defaults object for a
 * list of available options.
 *
 * You can also set this module in a container element and provide the "data-datetimepicker-container" attribute and
 * this plugin will initialize all the child elements that have the "datetimepicker" class into datetimepicker widgets.
 *
 * jQuery Datetimepicker Website: {@link http://xdsoft.net/jqplugins/datetimepicker}
 * 
 * ### Options
 *
 * In addition to the options stated below, you could also add many more options shown in the
 * jQuery Datetimepicker documentation.
 *
 * **Format | `data-datetimepicker-format` | String | Optional**
 *
 * Provide the default date format. If no value is provided, the default format will be set
 * to `'d.m.Y H:i'`.
 *
 * **Lang | `data-datetimepicker-lang` | String | Optional**
 *
 * Provide the default language code. If the current language is set to english, the default
 * language code will be set to `'en-GB'`, else the language code will be set to `'de'`.
 *
 * ### Examples
 * 
 * ```html
 * <input type="text" placeholder="##.##.#### ##:##" data-gx-widget="datetimepicker" />
 * ```
 *
 * @deprecated Since v1.4, will be removed in v1.7. Use the one from JSE/Widgets namespace.
 * 
 * @module Admin/Widgets/datetimepicker
 * @requires jQuery-Datetimepicker-Plugin
 */
gx.widgets.module('datetimepicker', [jse.source + '/vendor/datetimepicker/jquery.datetimepicker.full.min.js', jse.source + '/vendor/datetimepicker/jquery.datetimepicker.css'], function (data) {

	'use strict';

	var
	/**
  * Module Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Module Options
  *
  * @type {object}
  */
	defaults = {
		format: 'd.m.Y H:i',
		lang: jse.core.config.get('languageCode') === 'en' ? 'en-GB' : 'de'
	},


	/**
  * Final Module Options
  * 
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Instance
  *
  * @type {object}
  */
	module = {};

	/**
  * Initialize Module
  *
  * @param {function} done Call this method once the module is initialized.
  */
	module.init = function (done) {
		// Check if the datetimepicker plugin is already loaded. 
		if ($.fn.datetimepicker === undefined) {
			throw new Error('The $.fn.datetimepicker plugin must be loaded before the module is initialized.');
		}

		// Check if the current element is a container and thus need to initialize the children elements. 
		if (options.container !== undefined) {
			$this.find('.datetimepicker').datetimepicker(options);
		} else {
			$this.datetimepicker(options);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGV0aW1lcGlja2VyLmpzIl0sIm5hbWVzIjpbImd4Iiwid2lkZ2V0cyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsImZvcm1hdCIsImxhbmciLCJjb3JlIiwiY29uZmlnIiwiZ2V0Iiwib3B0aW9ucyIsImV4dGVuZCIsImluaXQiLCJkb25lIiwiZm4iLCJkYXRldGltZXBpY2tlciIsInVuZGVmaW5lZCIsIkVycm9yIiwiY29udGFpbmVyIiwiZmluZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUNBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxnQkFERCxFQUdDLENBQUlDLElBQUlDLE1BQVIsK0RBQ0lELElBQUlDLE1BRFIsc0RBSEQsRUFNQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXO0FBQ1ZDLFVBQVEsV0FERTtBQUVWQyxRQUFNUCxJQUFJUSxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLE1BQXdDLElBQXhDLEdBQStDLE9BQS9DLEdBQXlEO0FBRnJELEVBYlo7OztBQWtCQzs7Ozs7QUFLQUMsV0FBVVAsRUFBRVEsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CUCxRQUFuQixFQUE2QkgsSUFBN0IsQ0F2Qlg7OztBQXlCQzs7Ozs7QUFLQUgsVUFBUyxFQTlCVjs7QUFnQ0E7Ozs7O0FBS0FBLFFBQU9jLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUI7QUFDQSxNQUFJVixFQUFFVyxFQUFGLENBQUtDLGNBQUwsS0FBd0JDLFNBQTVCLEVBQXVDO0FBQ3RDLFNBQU0sSUFBSUMsS0FBSixDQUFVLGlGQUFWLENBQU47QUFDQTs7QUFFRDtBQUNBLE1BQUlQLFFBQVFRLFNBQVIsS0FBc0JGLFNBQTFCLEVBQXFDO0FBQ3BDZCxTQUFNaUIsSUFBTixDQUFXLGlCQUFYLEVBQThCSixjQUE5QixDQUE2Q0wsT0FBN0M7QUFDQSxHQUZELE1BRU87QUFDTlIsU0FBTWEsY0FBTixDQUFxQkwsT0FBckI7QUFDQTs7QUFFREc7QUFDQSxFQWREOztBQWdCQSxRQUFPZixNQUFQO0FBQ0EsQ0FoRUYiLCJmaWxlIjoiZGF0ZXRpbWVwaWNrZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGRhdGV0aW1lcGlja2VyLmpzIDIwMTYtMDItMjNcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIERhdGV0aW1lcGlja2VyIFdpZGdldFxuICpcbiAqIFRoaXMgd2lkZ2V0IHdpbGwgY29udmVydCBpdHNlbGYgb3IgbXVsdGlwbGUgZWxlbWVudHMgaW50byBkYXRldGltZXBpY2tlciBpbnN0YW5jZXMuIENoZWNrIHRoZSBkZWZhdWx0cyBvYmplY3QgZm9yIGFcbiAqIGxpc3Qgb2YgYXZhaWxhYmxlIG9wdGlvbnMuXG4gKlxuICogWW91IGNhbiBhbHNvIHNldCB0aGlzIG1vZHVsZSBpbiBhIGNvbnRhaW5lciBlbGVtZW50IGFuZCBwcm92aWRlIHRoZSBcImRhdGEtZGF0ZXRpbWVwaWNrZXItY29udGFpbmVyXCIgYXR0cmlidXRlIGFuZFxuICogdGhpcyBwbHVnaW4gd2lsbCBpbml0aWFsaXplIGFsbCB0aGUgY2hpbGQgZWxlbWVudHMgdGhhdCBoYXZlIHRoZSBcImRhdGV0aW1lcGlja2VyXCIgY2xhc3MgaW50byBkYXRldGltZXBpY2tlciB3aWRnZXRzLlxuICpcbiAqIGpRdWVyeSBEYXRldGltZXBpY2tlciBXZWJzaXRlOiB7QGxpbmsgaHR0cDovL3hkc29mdC5uZXQvanFwbHVnaW5zL2RhdGV0aW1lcGlja2VyfVxuICogXG4gKiAjIyMgT3B0aW9uc1xuICpcbiAqIEluIGFkZGl0aW9uIHRvIHRoZSBvcHRpb25zIHN0YXRlZCBiZWxvdywgeW91IGNvdWxkIGFsc28gYWRkIG1hbnkgbW9yZSBvcHRpb25zIHNob3duIGluIHRoZVxuICogalF1ZXJ5IERhdGV0aW1lcGlja2VyIGRvY3VtZW50YXRpb24uXG4gKlxuICogKipGb3JtYXQgfCBgZGF0YS1kYXRldGltZXBpY2tlci1mb3JtYXRgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICpcbiAqIFByb3ZpZGUgdGhlIGRlZmF1bHQgZGF0ZSBmb3JtYXQuIElmIG5vIHZhbHVlIGlzIHByb3ZpZGVkLCB0aGUgZGVmYXVsdCBmb3JtYXQgd2lsbCBiZSBzZXRcbiAqIHRvIGAnZC5tLlkgSDppJ2AuXG4gKlxuICogKipMYW5nIHwgYGRhdGEtZGF0ZXRpbWVwaWNrZXItbGFuZ2AgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogUHJvdmlkZSB0aGUgZGVmYXVsdCBsYW5ndWFnZSBjb2RlLiBJZiB0aGUgY3VycmVudCBsYW5ndWFnZSBpcyBzZXQgdG8gZW5nbGlzaCwgdGhlIGRlZmF1bHRcbiAqIGxhbmd1YWdlIGNvZGUgd2lsbCBiZSBzZXQgdG8gYCdlbi1HQidgLCBlbHNlIHRoZSBsYW5ndWFnZSBjb2RlIHdpbGwgYmUgc2V0IHRvIGAnZGUnYC5cbiAqXG4gKiAjIyMgRXhhbXBsZXNcbiAqIFxuICogYGBgaHRtbFxuICogPGlucHV0IHR5cGU9XCJ0ZXh0XCIgcGxhY2Vob2xkZXI9XCIjIy4jIy4jIyMjICMjOiMjXCIgZGF0YS1neC13aWRnZXQ9XCJkYXRldGltZXBpY2tlclwiIC8+XG4gKiBgYGBcbiAqXG4gKiBAZGVwcmVjYXRlZCBTaW5jZSB2MS40LCB3aWxsIGJlIHJlbW92ZWQgaW4gdjEuNy4gVXNlIHRoZSBvbmUgZnJvbSBKU0UvV2lkZ2V0cyBuYW1lc3BhY2UuXG4gKiBcbiAqIEBtb2R1bGUgQWRtaW4vV2lkZ2V0cy9kYXRldGltZXBpY2tlclxuICogQHJlcXVpcmVzIGpRdWVyeS1EYXRldGltZXBpY2tlci1QbHVnaW5cbiAqL1xuZ3gud2lkZ2V0cy5tb2R1bGUoXG5cdCdkYXRldGltZXBpY2tlcicsXG5cdFxuXHRbYCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGV0aW1lcGlja2VyL2pxdWVyeS5kYXRldGltZXBpY2tlci5mdWxsLm1pbi5qc2AsXG5cdCBgJHtqc2Uuc291cmNlfS92ZW5kb3IvZGF0ZXRpbWVwaWNrZXIvanF1ZXJ5LmRhdGV0aW1lcGlja2VyLmNzc2BdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgTW9kdWxlIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0Zm9ybWF0OiAnZC5tLlkgSDppJyxcblx0XHRcdFx0bGFuZzoganNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJykgPT09ICdlbicgPyAnZW4tR0InIDogJ2RlJ1xuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBNb2R1bGUgT3B0aW9uc1xuXHRcdFx0ICogXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgSW5zdGFuY2Vcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIE1vZHVsZVxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtmdW5jdGlvbn0gZG9uZSBDYWxsIHRoaXMgbWV0aG9kIG9uY2UgdGhlIG1vZHVsZSBpcyBpbml0aWFsaXplZC5cblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdC8vIENoZWNrIGlmIHRoZSBkYXRldGltZXBpY2tlciBwbHVnaW4gaXMgYWxyZWFkeSBsb2FkZWQuIFxuXHRcdFx0aWYgKCQuZm4uZGF0ZXRpbWVwaWNrZXIgPT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1RoZSAkLmZuLmRhdGV0aW1lcGlja2VyIHBsdWdpbiBtdXN0IGJlIGxvYWRlZCBiZWZvcmUgdGhlIG1vZHVsZSBpcyBpbml0aWFsaXplZC4nKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gQ2hlY2sgaWYgdGhlIGN1cnJlbnQgZWxlbWVudCBpcyBhIGNvbnRhaW5lciBhbmQgdGh1cyBuZWVkIHRvIGluaXRpYWxpemUgdGhlIGNoaWxkcmVuIGVsZW1lbnRzLiBcblx0XHRcdGlmIChvcHRpb25zLmNvbnRhaW5lciAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5kYXRldGltZXBpY2tlcicpLmRhdGV0aW1lcGlja2VyKG9wdGlvbnMpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0JHRoaXMuZGF0ZXRpbWVwaWNrZXIob3B0aW9ucyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pOyAiXX0=
