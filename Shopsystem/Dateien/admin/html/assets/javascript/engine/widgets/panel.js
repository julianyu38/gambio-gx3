'use strict';

/* --------------------------------------------------------------
 panel.js 2017-10-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Panel widget
 *
 * This widget creates a panel container and wraps the inner html inside the panel.
 * The user configuration service is used by this widget, so the state (whether collapsed or expanded) is stored
 * by user and will be used on future usages. To make this functionality possible, the options "user" and "section"
 * are required.
 *
 * ### Options (Required)
 *
 * **Title | `data-panel-title` | String | Required**
 *
 * Panels title value.
 *
 * ### Options (Additional)
 *
 * **Collapsed icon class | `data-panel-collapsed_icon_class` | String | Optional**
 *
 * Font awesome class for collapsed icon. If no value is provided, it defaults to **'fa fa-plus-square-o'**.
 *
 * **Expanded icon class | `data-panel-expanded_icon_class` | String | Optional**
 *
 * Font awesome class for expanded icon. If no value is provided, it defaults to **'fa fa-minus-square-o'**.
 *
 * **Container class | `data-panel-container_class` | String | Optional**
 *
 * Additional class attributes. The values will be append to the .panel element.
 *
 * **Toggle time | `data-panel-toggle_time` | String | Optional**
 *
 * Toggle time for collapsing/expanding the panel. If no value is provided, it defaults to **'200'**.
 *
 * **Collapsed | `data-panel-collapsed` | Boolean | Optional **
 *
 * Determines the panels default state. Collapsed if set to true and expanded otherwise. If no value is provided,
 * the user configuration will be used. If no user configuration was found, the default state is expanded.
 *
 * **User | `data-panel-user` | Integer | Optional**
 *
 * Customer id of user, used by user configuration service to store the collapsed state. This option should be set
 * with the "section" option and will be ignored, if the "collapsed" option is set.
 *
 * **Section | `data-panel-section` | String | Optional**
 *
 * Panel section, used by user configuration service 'configuration_key'. This option should be set with the "user"
 * option and will be ignored, if the "collapsed" option is set.
 *
 * ### Example
 *
 * ```html
 * <!-- usage of user configuration -->
 * <div data-gx-widget="panel"
 *      data-panel-title="Panel Title"
 *      data-panel-user="{$smarty.session.customer_id}"
 *      data-panel-section="panel-sample-section"
 *      data-panel-container_class="additional-sample-class">
 *     <div class="sample-class">
 *         <p>Sample Content</p>!
 *     </div>
 * </div>
 *
 * <!-- usage of collapsed option -->
 * <div data-gx-widget="panel"
 *      data-panel-title="Panel Title"
 *      data-panel-container_class="additional-sample-class"
 *      data-panel-collapsed="false|true">
 *     <div class="sample-class">
 *         <p>Sample Content</p>!
 *     </div>
 * </div>
 * ```
 *
 * @module Admin/Widgets/panel
 */
gx.widgets.module('panel',

// external libraries, used by widget
['user_configuration_service'], function (data) {
	'use strict';

	/**
  * Widget reference.
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default options for widget,
  *
  * @type {object}
  */
	var defaults = {
		collapsed_icon_class: 'fa fa-plus-square-o',
		expanded_icon_class: 'fa fa-minus-square-o',
		container_class: '',
		toggle_time: 200
	};

	/**
  * Final widget options.
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module object.
  *
  * @type {{}}
  */
	var module = {};

	/**
  * Required widget options, should passed from markup.
  *
  * @type {String[]}
  */
	var requiredOptions = ['title', 'user', 'section'];

	/**
  * User configuration service to store collapsed/expanded configuration.
  *
  * @type {jse.libs.user_configuration_service}
  */
	var userConfig = jse.libs.user_configuration_service;

	/**
  * Property like values.
  */
	var $iconContainer = void 0;
	var $collapseIcon = void 0;
	var $panelBody = void 0;

	// private methods

	/**
  * Widget initialization.
  *
  * @private
  */
	var _init = function _init() {
		var collapsed = void 0;

		if (undefined !== options.collapsed) {
			_renderPanel(options.collapsed);
		} else {
			if (undefined === options.user || undefined === options.section) {
				throw new Error('Required widget options are not set. Set whether the "collapsed" option, or use ' + 'the user configuration service with the options "user" and "section".');
			}

			userConfig.get({
				data: {
					userId: options.user,
					configurationKey: options.section + '_collapsed'
				},
				onSuccess: function onSuccess(result) {
					collapsed = result.length === 0 ? false : result.configurationValue === 'true';
					_renderPanel(collapsed);
				}
			});
		}
	};

	/**
  * Checks if all required options are passed.
  *
  * @private
  */
	var _checkRequiredOptions = function _checkRequiredOptions() {
		var i = 0;
		for (; i < requiredOptions.length; i++) {
			if (undefined === options[requiredOptions[i]]) {
				throw new Error('Required widget option "' + requiredOptions[i] + '" is no set!');
			}
		}
	};

	/**
  * Renders the panel.
  *
  * @param {boolean} collapsed If true, the panel will be collapsed.
  * @private
  */
	var _renderPanel = function _renderPanel(collapsed) {
		var $panelHeading = $('<div/>', { 'class': 'panel-heading' });
		var $panelTitle = $('<span/>', {
			'class': 'title',
			'text': options.title
		});

		$iconContainer = $('<span/>', { 'class': 'collapser pull-right cursor-pointer' });
		$collapseIcon = $('<i/>', {
			'class': collapsed ? options.collapsed_icon_class : options.expanded_icon_class
		});
		$panelBody = $('<div/>', { 'class': 'panel-body' });
		$this.children().detach().appendTo($panelBody);

		$this.addClass('panel panel-default ' + options.container_class);
		$panelHeading.append($panelTitle);
		$iconContainer.append($collapseIcon).appendTo($panelHeading);
		$this.append($panelHeading);

		if (collapsed) {
			$panelBody.css({ 'display': 'none' });
		}
		$this.append($panelBody);

		// set event handler
		$iconContainer.on('click', _toggle);
	};

	/**
  * Toggle event listener for clicks on panel heading.
  *
  * @private
  */
	var _toggle = function _toggle() {
		var isCollapsed = $collapseIcon.hasClass(options.expanded_icon_class);

		$collapseIcon.toggleClass(options.collapsed_icon_class);
		$collapseIcon.toggleClass(options.expanded_icon_class);
		$panelBody.toggle(options.toggle_time);

		userConfig.set({
			data: {
				userId: options.user,
				configurationKey: options.section + '_collapsed',
				configurationValue: isCollapsed
			}
		});
	};

	/**
  * Widget initialization function.
  *
  * @param done
  */
	module.init = function (done) {
		_checkRequiredOptions();
		_init();
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhbmVsLmpzIl0sIm5hbWVzIjpbImd4Iiwid2lkZ2V0cyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsImNvbGxhcHNlZF9pY29uX2NsYXNzIiwiZXhwYW5kZWRfaWNvbl9jbGFzcyIsImNvbnRhaW5lcl9jbGFzcyIsInRvZ2dsZV90aW1lIiwib3B0aW9ucyIsImV4dGVuZCIsInJlcXVpcmVkT3B0aW9ucyIsInVzZXJDb25maWciLCJqc2UiLCJsaWJzIiwidXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UiLCIkaWNvbkNvbnRhaW5lciIsIiRjb2xsYXBzZUljb24iLCIkcGFuZWxCb2R5IiwiX2luaXQiLCJjb2xsYXBzZWQiLCJ1bmRlZmluZWQiLCJfcmVuZGVyUGFuZWwiLCJ1c2VyIiwic2VjdGlvbiIsIkVycm9yIiwiZ2V0IiwidXNlcklkIiwiY29uZmlndXJhdGlvbktleSIsIm9uU3VjY2VzcyIsInJlc3VsdCIsImxlbmd0aCIsImNvbmZpZ3VyYXRpb25WYWx1ZSIsIl9jaGVja1JlcXVpcmVkT3B0aW9ucyIsImkiLCIkcGFuZWxIZWFkaW5nIiwiJHBhbmVsVGl0bGUiLCJ0aXRsZSIsImNoaWxkcmVuIiwiZGV0YWNoIiwiYXBwZW5kVG8iLCJhZGRDbGFzcyIsImFwcGVuZCIsImNzcyIsIm9uIiwiX3RvZ2dsZSIsImlzQ29sbGFwc2VkIiwiaGFzQ2xhc3MiLCJ0b2dnbGVDbGFzcyIsInRvZ2dsZSIsInNldCIsImluaXQiLCJkb25lIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEVBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxPQUREOztBQUdDO0FBQ0EsQ0FBQyw0QkFBRCxDQUpELEVBTUMsVUFBU0MsSUFBVCxFQUFlO0FBQ2Q7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVc7QUFDaEJDLHdCQUFzQixxQkFETjtBQUVoQkMsdUJBQXFCLHNCQUZMO0FBR2hCQyxtQkFBaUIsRUFIRDtBQUloQkMsZUFBYTtBQUpHLEVBQWpCOztBQU9BOzs7OztBQUtBLEtBQU1DLFVBQVVOLEVBQUVPLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQk4sUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTs7Ozs7QUFLQSxLQUFNVyxrQkFBa0IsQ0FBQyxPQUFELEVBQVUsTUFBVixFQUFrQixTQUFsQixDQUF4Qjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxhQUFhQyxJQUFJQyxJQUFKLENBQVNDLDBCQUE1Qjs7QUFFQTs7O0FBR0EsS0FBSUMsdUJBQUo7QUFDQSxLQUFJQyxzQkFBSjtBQUNBLEtBQUlDLG1CQUFKOztBQUVBOztBQUVBOzs7OztBQUtBLEtBQU1DLFFBQVEsU0FBUkEsS0FBUSxHQUFNO0FBQ25CLE1BQUlDLGtCQUFKOztBQUVBLE1BQUlDLGNBQWNaLFFBQVFXLFNBQTFCLEVBQXFDO0FBQ3BDRSxnQkFBYWIsUUFBUVcsU0FBckI7QUFDQSxHQUZELE1BRU87QUFDTixPQUFJQyxjQUFjWixRQUFRYyxJQUF0QixJQUE4QkYsY0FBY1osUUFBUWUsT0FBeEQsRUFBaUU7QUFDaEUsVUFBTSxJQUFJQyxLQUFKLENBQVUscUZBQ2IsdUVBREcsQ0FBTjtBQUVBOztBQUVEYixjQUFXYyxHQUFYLENBQWU7QUFDZHpCLFVBQU07QUFDTDBCLGFBQVFsQixRQUFRYyxJQURYO0FBRUxLLHVCQUFrQm5CLFFBQVFlLE9BQVIsR0FBa0I7QUFGL0IsS0FEUTtBQUtkSyxlQUFXLDJCQUFVO0FBQ3BCVCxpQkFBWVUsT0FBT0MsTUFBUCxLQUFrQixDQUFsQixHQUFzQixLQUF0QixHQUE4QkQsT0FBT0Usa0JBQVAsS0FBOEIsTUFBeEU7QUFDQVYsa0JBQWFGLFNBQWI7QUFDQTtBQVJhLElBQWY7QUFVQTtBQUNELEVBdEJEOztBQXdCQTs7Ozs7QUFLQSxLQUFNYSx3QkFBd0IsU0FBeEJBLHFCQUF3QixHQUFNO0FBQ25DLE1BQUlDLElBQUksQ0FBUjtBQUNBLFNBQU9BLElBQUl2QixnQkFBZ0JvQixNQUEzQixFQUFtQ0csR0FBbkMsRUFBd0M7QUFDdkMsT0FBSWIsY0FBY1osUUFBUUUsZ0JBQWdCdUIsQ0FBaEIsQ0FBUixDQUFsQixFQUErQztBQUM5QyxVQUFNLElBQUlULEtBQUosQ0FBVSw2QkFBNkJkLGdCQUFnQnVCLENBQWhCLENBQTdCLEdBQWtELGNBQTVELENBQU47QUFDQTtBQUNEO0FBQ0QsRUFQRDs7QUFTQTs7Ozs7O0FBTUEsS0FBTVosZUFBZSxTQUFmQSxZQUFlLFlBQWE7QUFDakMsTUFBTWEsZ0JBQWdCaEMsRUFBRSxRQUFGLEVBQVksRUFBQyxTQUFTLGVBQVYsRUFBWixDQUF0QjtBQUNBLE1BQU1pQyxjQUFjakMsRUFBRSxTQUFGLEVBQWE7QUFDaEMsWUFBUyxPQUR1QjtBQUVoQyxXQUFRTSxRQUFRNEI7QUFGZ0IsR0FBYixDQUFwQjs7QUFLQXJCLG1CQUFpQmIsRUFBRSxTQUFGLEVBQWEsRUFBQyxTQUFTLHFDQUFWLEVBQWIsQ0FBakI7QUFDQWMsa0JBQWdCZCxFQUFFLE1BQUYsRUFBVTtBQUN6QixZQUFTaUIsWUFBWVgsUUFBUUosb0JBQXBCLEdBQTJDSSxRQUFRSDtBQURuQyxHQUFWLENBQWhCO0FBR0FZLGVBQWFmLEVBQUUsUUFBRixFQUFZLEVBQUMsU0FBUyxZQUFWLEVBQVosQ0FBYjtBQUNBRCxRQUFNb0MsUUFBTixHQUFpQkMsTUFBakIsR0FBMEJDLFFBQTFCLENBQW1DdEIsVUFBbkM7O0FBRUFoQixRQUFNdUMsUUFBTixDQUFlLHlCQUF5QmhDLFFBQVFGLGVBQWhEO0FBQ0E0QixnQkFBY08sTUFBZCxDQUFxQk4sV0FBckI7QUFDQXBCLGlCQUFlMEIsTUFBZixDQUFzQnpCLGFBQXRCLEVBQXFDdUIsUUFBckMsQ0FBOENMLGFBQTlDO0FBQ0FqQyxRQUFNd0MsTUFBTixDQUFhUCxhQUFiOztBQUVBLE1BQUlmLFNBQUosRUFBZTtBQUNkRixjQUFXeUIsR0FBWCxDQUFlLEVBQUMsV0FBVyxNQUFaLEVBQWY7QUFDQTtBQUNEekMsUUFBTXdDLE1BQU4sQ0FBYXhCLFVBQWI7O0FBRUE7QUFDQUYsaUJBQWU0QixFQUFmLENBQWtCLE9BQWxCLEVBQTJCQyxPQUEzQjtBQUNBLEVBMUJEOztBQTRCQTs7Ozs7QUFLQSxLQUFNQSxVQUFVLFNBQVZBLE9BQVUsR0FBTTtBQUNyQixNQUFNQyxjQUFjN0IsY0FBYzhCLFFBQWQsQ0FBdUJ0QyxRQUFRSCxtQkFBL0IsQ0FBcEI7O0FBRUFXLGdCQUFjK0IsV0FBZCxDQUEwQnZDLFFBQVFKLG9CQUFsQztBQUNBWSxnQkFBYytCLFdBQWQsQ0FBMEJ2QyxRQUFRSCxtQkFBbEM7QUFDQVksYUFBVytCLE1BQVgsQ0FBa0J4QyxRQUFRRCxXQUExQjs7QUFFQUksYUFBV3NDLEdBQVgsQ0FBZTtBQUNkakQsU0FBTTtBQUNMMEIsWUFBUWxCLFFBQVFjLElBRFg7QUFFTEssc0JBQWtCbkIsUUFBUWUsT0FBUixHQUFrQixZQUYvQjtBQUdMUSx3QkFBb0JjO0FBSGY7QUFEUSxHQUFmO0FBT0EsRUFkRDs7QUFnQkE7Ozs7O0FBS0E5QyxRQUFPbUQsSUFBUCxHQUFjLGdCQUFRO0FBQ3JCbEI7QUFDQWQ7QUFDQWlDO0FBQ0EsRUFKRDs7QUFNQSxRQUFPcEQsTUFBUDtBQUNBLENBL0tGIiwiZmlsZSI6InBhbmVsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBwYW5lbC5qcyAyMDE3LTEwLTEwXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBQYW5lbCB3aWRnZXRcbiAqXG4gKiBUaGlzIHdpZGdldCBjcmVhdGVzIGEgcGFuZWwgY29udGFpbmVyIGFuZCB3cmFwcyB0aGUgaW5uZXIgaHRtbCBpbnNpZGUgdGhlIHBhbmVsLlxuICogVGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlIGlzIHVzZWQgYnkgdGhpcyB3aWRnZXQsIHNvIHRoZSBzdGF0ZSAod2hldGhlciBjb2xsYXBzZWQgb3IgZXhwYW5kZWQpIGlzIHN0b3JlZFxuICogYnkgdXNlciBhbmQgd2lsbCBiZSB1c2VkIG9uIGZ1dHVyZSB1c2FnZXMuIFRvIG1ha2UgdGhpcyBmdW5jdGlvbmFsaXR5IHBvc3NpYmxlLCB0aGUgb3B0aW9ucyBcInVzZXJcIiBhbmQgXCJzZWN0aW9uXCJcbiAqIGFyZSByZXF1aXJlZC5cbiAqXG4gKiAjIyMgT3B0aW9ucyAoUmVxdWlyZWQpXG4gKlxuICogKipUaXRsZSB8IGBkYXRhLXBhbmVsLXRpdGxlYCB8IFN0cmluZyB8IFJlcXVpcmVkKipcbiAqXG4gKiBQYW5lbHMgdGl0bGUgdmFsdWUuXG4gKlxuICogIyMjIE9wdGlvbnMgKEFkZGl0aW9uYWwpXG4gKlxuICogKipDb2xsYXBzZWQgaWNvbiBjbGFzcyB8IGBkYXRhLXBhbmVsLWNvbGxhcHNlZF9pY29uX2NsYXNzYCB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqXG4gKiBGb250IGF3ZXNvbWUgY2xhc3MgZm9yIGNvbGxhcHNlZCBpY29uLiBJZiBubyB2YWx1ZSBpcyBwcm92aWRlZCwgaXQgZGVmYXVsdHMgdG8gKionZmEgZmEtcGx1cy1zcXVhcmUtbycqKi5cbiAqXG4gKiAqKkV4cGFuZGVkIGljb24gY2xhc3MgfCBgZGF0YS1wYW5lbC1leHBhbmRlZF9pY29uX2NsYXNzYCB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqXG4gKiBGb250IGF3ZXNvbWUgY2xhc3MgZm9yIGV4cGFuZGVkIGljb24uIElmIG5vIHZhbHVlIGlzIHByb3ZpZGVkLCBpdCBkZWZhdWx0cyB0byAqKidmYSBmYS1taW51cy1zcXVhcmUtbycqKi5cbiAqXG4gKiAqKkNvbnRhaW5lciBjbGFzcyB8IGBkYXRhLXBhbmVsLWNvbnRhaW5lcl9jbGFzc2AgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogQWRkaXRpb25hbCBjbGFzcyBhdHRyaWJ1dGVzLiBUaGUgdmFsdWVzIHdpbGwgYmUgYXBwZW5kIHRvIHRoZSAucGFuZWwgZWxlbWVudC5cbiAqXG4gKiAqKlRvZ2dsZSB0aW1lIHwgYGRhdGEtcGFuZWwtdG9nZ2xlX3RpbWVgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICpcbiAqIFRvZ2dsZSB0aW1lIGZvciBjb2xsYXBzaW5nL2V4cGFuZGluZyB0aGUgcGFuZWwuIElmIG5vIHZhbHVlIGlzIHByb3ZpZGVkLCBpdCBkZWZhdWx0cyB0byAqKicyMDAnKiouXG4gKlxuICogKipDb2xsYXBzZWQgfCBgZGF0YS1wYW5lbC1jb2xsYXBzZWRgIHwgQm9vbGVhbiB8IE9wdGlvbmFsICoqXG4gKlxuICogRGV0ZXJtaW5lcyB0aGUgcGFuZWxzIGRlZmF1bHQgc3RhdGUuIENvbGxhcHNlZCBpZiBzZXQgdG8gdHJ1ZSBhbmQgZXhwYW5kZWQgb3RoZXJ3aXNlLiBJZiBubyB2YWx1ZSBpcyBwcm92aWRlZCxcbiAqIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb24gd2lsbCBiZSB1c2VkLiBJZiBubyB1c2VyIGNvbmZpZ3VyYXRpb24gd2FzIGZvdW5kLCB0aGUgZGVmYXVsdCBzdGF0ZSBpcyBleHBhbmRlZC5cbiAqXG4gKiAqKlVzZXIgfCBgZGF0YS1wYW5lbC11c2VyYCB8IEludGVnZXIgfCBPcHRpb25hbCoqXG4gKlxuICogQ3VzdG9tZXIgaWQgb2YgdXNlciwgdXNlZCBieSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZSB0byBzdG9yZSB0aGUgY29sbGFwc2VkIHN0YXRlLiBUaGlzIG9wdGlvbiBzaG91bGQgYmUgc2V0XG4gKiB3aXRoIHRoZSBcInNlY3Rpb25cIiBvcHRpb24gYW5kIHdpbGwgYmUgaWdub3JlZCwgaWYgdGhlIFwiY29sbGFwc2VkXCIgb3B0aW9uIGlzIHNldC5cbiAqXG4gKiAqKlNlY3Rpb24gfCBgZGF0YS1wYW5lbC1zZWN0aW9uYCB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqXG4gKiBQYW5lbCBzZWN0aW9uLCB1c2VkIGJ5IHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlICdjb25maWd1cmF0aW9uX2tleScuIFRoaXMgb3B0aW9uIHNob3VsZCBiZSBzZXQgd2l0aCB0aGUgXCJ1c2VyXCJcbiAqIG9wdGlvbiBhbmQgd2lsbCBiZSBpZ25vcmVkLCBpZiB0aGUgXCJjb2xsYXBzZWRcIiBvcHRpb24gaXMgc2V0LlxuICpcbiAqICMjIyBFeGFtcGxlXG4gKlxuICogYGBgaHRtbFxuICogPCEtLSB1c2FnZSBvZiB1c2VyIGNvbmZpZ3VyYXRpb24gLS0+XG4gKiA8ZGl2IGRhdGEtZ3gtd2lkZ2V0PVwicGFuZWxcIlxuICogICAgICBkYXRhLXBhbmVsLXRpdGxlPVwiUGFuZWwgVGl0bGVcIlxuICogICAgICBkYXRhLXBhbmVsLXVzZXI9XCJ7JHNtYXJ0eS5zZXNzaW9uLmN1c3RvbWVyX2lkfVwiXG4gKiAgICAgIGRhdGEtcGFuZWwtc2VjdGlvbj1cInBhbmVsLXNhbXBsZS1zZWN0aW9uXCJcbiAqICAgICAgZGF0YS1wYW5lbC1jb250YWluZXJfY2xhc3M9XCJhZGRpdGlvbmFsLXNhbXBsZS1jbGFzc1wiPlxuICogICAgIDxkaXYgY2xhc3M9XCJzYW1wbGUtY2xhc3NcIj5cbiAqICAgICAgICAgPHA+U2FtcGxlIENvbnRlbnQ8L3A+IVxuICogICAgIDwvZGl2PlxuICogPC9kaXY+XG4gKlxuICogPCEtLSB1c2FnZSBvZiBjb2xsYXBzZWQgb3B0aW9uIC0tPlxuICogPGRpdiBkYXRhLWd4LXdpZGdldD1cInBhbmVsXCJcbiAqICAgICAgZGF0YS1wYW5lbC10aXRsZT1cIlBhbmVsIFRpdGxlXCJcbiAqICAgICAgZGF0YS1wYW5lbC1jb250YWluZXJfY2xhc3M9XCJhZGRpdGlvbmFsLXNhbXBsZS1jbGFzc1wiXG4gKiAgICAgIGRhdGEtcGFuZWwtY29sbGFwc2VkPVwiZmFsc2V8dHJ1ZVwiPlxuICogICAgIDxkaXYgY2xhc3M9XCJzYW1wbGUtY2xhc3NcIj5cbiAqICAgICAgICAgPHA+U2FtcGxlIENvbnRlbnQ8L3A+IVxuICogICAgIDwvZGl2PlxuICogPC9kaXY+XG4gKiBgYGBcbiAqXG4gKiBAbW9kdWxlIEFkbWluL1dpZGdldHMvcGFuZWxcbiAqL1xuZ3gud2lkZ2V0cy5tb2R1bGUoXG5cdCdwYW5lbCcsXG5cdFxuXHQvLyBleHRlcm5hbCBsaWJyYXJpZXMsIHVzZWQgYnkgd2lkZ2V0XG5cdFsndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogV2lkZ2V0IHJlZmVyZW5jZS5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmF1bHQgb3B0aW9ucyBmb3Igd2lkZ2V0LFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBkZWZhdWx0cyA9IHtcblx0XHRcdGNvbGxhcHNlZF9pY29uX2NsYXNzOiAnZmEgZmEtcGx1cy1zcXVhcmUtbycsXG5cdFx0XHRleHBhbmRlZF9pY29uX2NsYXNzOiAnZmEgZmEtbWludXMtc3F1YXJlLW8nLFxuXHRcdFx0Y29udGFpbmVyX2NsYXNzOiAnJyxcblx0XHRcdHRvZ2dsZV90aW1lOiAyMDBcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmFsIHdpZGdldCBvcHRpb25zLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgb2JqZWN0LlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge3t9fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlcXVpcmVkIHdpZGdldCBvcHRpb25zLCBzaG91bGQgcGFzc2VkIGZyb20gbWFya3VwLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge1N0cmluZ1tdfVxuXHRcdCAqL1xuXHRcdGNvbnN0IHJlcXVpcmVkT3B0aW9ucyA9IFsndGl0bGUnLCAndXNlcicsICdzZWN0aW9uJ107XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVXNlciBjb25maWd1cmF0aW9uIHNlcnZpY2UgdG8gc3RvcmUgY29sbGFwc2VkL2V4cGFuZGVkIGNvbmZpZ3VyYXRpb24uXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7anNlLmxpYnMudXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2V9XG5cdFx0ICovXG5cdFx0Y29uc3QgdXNlckNvbmZpZyA9IGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFByb3BlcnR5IGxpa2UgdmFsdWVzLlxuXHRcdCAqL1xuXHRcdGxldCAkaWNvbkNvbnRhaW5lcjtcblx0XHRsZXQgJGNvbGxhcHNlSWNvbjtcblx0XHRsZXQgJHBhbmVsQm9keTtcblx0XHRcblx0XHQvLyBwcml2YXRlIG1ldGhvZHNcblx0XHRcblx0XHQvKipcblx0XHQgKiBXaWRnZXQgaW5pdGlhbGl6YXRpb24uXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGNvbnN0IF9pbml0ID0gKCkgPT4ge1xuXHRcdFx0bGV0IGNvbGxhcHNlZDtcblx0XHRcdFxuXHRcdFx0aWYgKHVuZGVmaW5lZCAhPT0gb3B0aW9ucy5jb2xsYXBzZWQpIHtcblx0XHRcdFx0X3JlbmRlclBhbmVsKG9wdGlvbnMuY29sbGFwc2VkKVxuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0aWYgKHVuZGVmaW5lZCA9PT0gb3B0aW9ucy51c2VyIHx8IHVuZGVmaW5lZCA9PT0gb3B0aW9ucy5zZWN0aW9uKSB7XG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdSZXF1aXJlZCB3aWRnZXQgb3B0aW9ucyBhcmUgbm90IHNldC4gU2V0IHdoZXRoZXIgdGhlIFwiY29sbGFwc2VkXCIgb3B0aW9uLCBvciB1c2UgJ1xuXHRcdFx0XHRcdFx0KyAndGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlIHdpdGggdGhlIG9wdGlvbnMgXCJ1c2VyXCIgYW5kIFwic2VjdGlvblwiLicpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHR1c2VyQ29uZmlnLmdldCh7XG5cdFx0XHRcdFx0ZGF0YToge1xuXHRcdFx0XHRcdFx0dXNlcklkOiBvcHRpb25zLnVzZXIsXG5cdFx0XHRcdFx0XHRjb25maWd1cmF0aW9uS2V5OiBvcHRpb25zLnNlY3Rpb24gKyAnX2NvbGxhcHNlZCdcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdG9uU3VjY2VzczogcmVzdWx0ID0+IHtcblx0XHRcdFx0XHRcdGNvbGxhcHNlZCA9IHJlc3VsdC5sZW5ndGggPT09IDAgPyBmYWxzZSA6IHJlc3VsdC5jb25maWd1cmF0aW9uVmFsdWUgPT09ICd0cnVlJztcblx0XHRcdFx0XHRcdF9yZW5kZXJQYW5lbChjb2xsYXBzZWQpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDaGVja3MgaWYgYWxsIHJlcXVpcmVkIG9wdGlvbnMgYXJlIHBhc3NlZC5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0Y29uc3QgX2NoZWNrUmVxdWlyZWRPcHRpb25zID0gKCkgPT4ge1xuXHRcdFx0bGV0IGkgPSAwO1xuXHRcdFx0Zm9yICg7IGkgPCByZXF1aXJlZE9wdGlvbnMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0aWYgKHVuZGVmaW5lZCA9PT0gb3B0aW9uc1tyZXF1aXJlZE9wdGlvbnNbaV1dKSB7XG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdSZXF1aXJlZCB3aWRnZXQgb3B0aW9uIFwiJyArIHJlcXVpcmVkT3B0aW9uc1tpXSArICdcIiBpcyBubyBzZXQhJyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlbmRlcnMgdGhlIHBhbmVsLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtib29sZWFufSBjb2xsYXBzZWQgSWYgdHJ1ZSwgdGhlIHBhbmVsIHdpbGwgYmUgY29sbGFwc2VkLlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0Y29uc3QgX3JlbmRlclBhbmVsID0gY29sbGFwc2VkID0+IHtcblx0XHRcdGNvbnN0ICRwYW5lbEhlYWRpbmcgPSAkKCc8ZGl2Lz4nLCB7J2NsYXNzJzogJ3BhbmVsLWhlYWRpbmcnfSk7XG5cdFx0XHRjb25zdCAkcGFuZWxUaXRsZSA9ICQoJzxzcGFuLz4nLCB7XG5cdFx0XHRcdCdjbGFzcyc6ICd0aXRsZScsXG5cdFx0XHRcdCd0ZXh0Jzogb3B0aW9ucy50aXRsZVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCRpY29uQ29udGFpbmVyID0gJCgnPHNwYW4vPicsIHsnY2xhc3MnOiAnY29sbGFwc2VyIHB1bGwtcmlnaHQgY3Vyc29yLXBvaW50ZXInfSlcblx0XHRcdCRjb2xsYXBzZUljb24gPSAkKCc8aS8+Jywge1xuXHRcdFx0XHQnY2xhc3MnOiBjb2xsYXBzZWQgPyBvcHRpb25zLmNvbGxhcHNlZF9pY29uX2NsYXNzIDogb3B0aW9ucy5leHBhbmRlZF9pY29uX2NsYXNzXG5cdFx0XHR9KTtcblx0XHRcdCRwYW5lbEJvZHkgPSAkKCc8ZGl2Lz4nLCB7J2NsYXNzJzogJ3BhbmVsLWJvZHknfSk7XG5cdFx0XHQkdGhpcy5jaGlsZHJlbigpLmRldGFjaCgpLmFwcGVuZFRvKCRwYW5lbEJvZHkpO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5hZGRDbGFzcygncGFuZWwgcGFuZWwtZGVmYXVsdCAnICsgb3B0aW9ucy5jb250YWluZXJfY2xhc3MpO1xuXHRcdFx0JHBhbmVsSGVhZGluZy5hcHBlbmQoJHBhbmVsVGl0bGUpO1xuXHRcdFx0JGljb25Db250YWluZXIuYXBwZW5kKCRjb2xsYXBzZUljb24pLmFwcGVuZFRvKCRwYW5lbEhlYWRpbmcpO1xuXHRcdFx0JHRoaXMuYXBwZW5kKCRwYW5lbEhlYWRpbmcpO1xuXHRcdFx0XG5cdFx0XHRpZiAoY29sbGFwc2VkKSB7XG5cdFx0XHRcdCRwYW5lbEJvZHkuY3NzKHsnZGlzcGxheSc6ICdub25lJ30pO1xuXHRcdFx0fVxuXHRcdFx0JHRoaXMuYXBwZW5kKCRwYW5lbEJvZHkpO1xuXHRcdFx0XG5cdFx0XHQvLyBzZXQgZXZlbnQgaGFuZGxlclxuXHRcdFx0JGljb25Db250YWluZXIub24oJ2NsaWNrJywgX3RvZ2dsZSlcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFRvZ2dsZSBldmVudCBsaXN0ZW5lciBmb3IgY2xpY2tzIG9uIHBhbmVsIGhlYWRpbmcuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGNvbnN0IF90b2dnbGUgPSAoKSA9PiB7XG5cdFx0XHRjb25zdCBpc0NvbGxhcHNlZCA9ICRjb2xsYXBzZUljb24uaGFzQ2xhc3Mob3B0aW9ucy5leHBhbmRlZF9pY29uX2NsYXNzKTtcblx0XHRcdFxuXHRcdFx0JGNvbGxhcHNlSWNvbi50b2dnbGVDbGFzcyhvcHRpb25zLmNvbGxhcHNlZF9pY29uX2NsYXNzKTtcblx0XHRcdCRjb2xsYXBzZUljb24udG9nZ2xlQ2xhc3Mob3B0aW9ucy5leHBhbmRlZF9pY29uX2NsYXNzKTtcblx0XHRcdCRwYW5lbEJvZHkudG9nZ2xlKG9wdGlvbnMudG9nZ2xlX3RpbWUpO1xuXHRcdFx0XG5cdFx0XHR1c2VyQ29uZmlnLnNldCh7XG5cdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHR1c2VySWQ6IG9wdGlvbnMudXNlcixcblx0XHRcdFx0XHRjb25maWd1cmF0aW9uS2V5OiBvcHRpb25zLnNlY3Rpb24gKyAnX2NvbGxhcHNlZCcsXG5cdFx0XHRcdFx0Y29uZmlndXJhdGlvblZhbHVlOiBpc0NvbGxhcHNlZFxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFdpZGdldCBpbml0aWFsaXphdGlvbiBmdW5jdGlvbi5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSBkb25lXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdF9jaGVja1JlcXVpcmVkT3B0aW9ucygpO1xuXHRcdFx0X2luaXQoKTtcblx0XHRcdGRvbmUoKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKTtcbiJdfQ==
