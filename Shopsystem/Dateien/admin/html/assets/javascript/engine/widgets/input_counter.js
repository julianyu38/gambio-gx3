'use strict';

/* --------------------------------------------------------------
 input_counter.js 2016-08-25
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Input Counter Widget
 *
 * Adds a counter element to input and textarea elements.
 *
 * ### Options:
 *
 * **Persistence | `data-input_counter-persistent` | bool | Optional**
 *
 * Omits to hide the counter element. This option is optional and the default value is true, so
 * the counter element is permanent displayed.
 *
 * **Pull | `data-input_counter-persistent` | bool/string | Optional**
 *
 * The option gives the possibility to pull the counter element to whether the right or left side.
 *
 * ### Example
 * ```html
 * <!-- Default input counter element -->
 * <input type="text" data-gx-widget="input_counter">
 * <textarea data-gx-widget="input_counter"></textarea>
 *
 * <!-- Show element on focus and hide on blur -->
 * <input type="text" data-input_counter-persistent="false">
 *
 * <!-- Disable counter pull -->
 * <input type="text" data-input_counter-pull="false">
 *
 * <!-- Pull counter to left side -->
 * <input type="text" data-input_counter-pull="left">
 * ```
 *
 * @module Admin/Widgets/input_counter
 */

gx.widgets.module('input_counter', [], function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Widget Options
  *
  * @type {object}
  */
	defaults = {
		persistent: true,
		pull: 'right',
		label: false
	},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {},
	    counterLabel = jse.core.lang.translate('COUNTER', 'admin_general'),
	    $counter = $('<span/>');

	var _showCharCounter = function _showCharCounter(event) {
		$this.parent().append($counter);
		if (options.max) {
			$counter.text($this.val().length + '/' + options.max);
			options.label ? $counter.append(' - ' + counterLabel) : '';
		} else {
			$counter.text($this.val().length);
			options.label ? $counter.append(' - ' + counterLabel) : '';
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		// check and set handling of persistent pull
		if (options.pull) {
			$counter.addClass('pull-' + options.pull);
		}

		// check and set handling of persistent option
		if (options.persistent) {
			_showCharCounter();
		} else {
			$this.focus(_showCharCounter);
			$this.blur(function () {
				$counter.remove();
			});
		}

		$this.on('keyup', _showCharCounter);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImlucHV0X2NvdW50ZXIuanMiXSwibmFtZXMiOlsiZ3giLCJ3aWRnZXRzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwicGVyc2lzdGVudCIsInB1bGwiLCJsYWJlbCIsIm9wdGlvbnMiLCJleHRlbmQiLCJjb3VudGVyTGFiZWwiLCJqc2UiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsIiRjb3VudGVyIiwiX3Nob3dDaGFyQ291bnRlciIsImV2ZW50IiwicGFyZW50IiwiYXBwZW5kIiwibWF4IiwidGV4dCIsInZhbCIsImxlbmd0aCIsImluaXQiLCJkb25lIiwiYWRkQ2xhc3MiLCJmb2N1cyIsImJsdXIiLCJyZW1vdmUiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQUEsR0FBR0MsT0FBSCxDQUFXQyxNQUFYLENBQ0MsZUFERCxFQUdDLEVBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7QUFDZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXO0FBQ1ZDLGNBQVksSUFERjtBQUVWQyxRQUFNLE9BRkk7QUFHVkMsU0FBTztBQUhHLEVBYlo7OztBQW1CQzs7Ozs7QUFLQUMsV0FBVUwsRUFBRU0sTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CTCxRQUFuQixFQUE2QkgsSUFBN0IsQ0F4Qlg7OztBQTBCQzs7Ozs7QUFLQUQsVUFBUyxFQS9CVjtBQUFBLEtBaUNDVSxlQUFlQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixTQUF4QixFQUFtQyxlQUFuQyxDQWpDaEI7QUFBQSxLQW1DQ0MsV0FBV1osRUFBRSxTQUFGLENBbkNaOztBQXFDQSxLQUFJYSxtQkFBbUIsU0FBbkJBLGdCQUFtQixDQUFTQyxLQUFULEVBQWdCO0FBQ3RDZixRQUFNZ0IsTUFBTixHQUFlQyxNQUFmLENBQXNCSixRQUF0QjtBQUNBLE1BQUlQLFFBQVFZLEdBQVosRUFBaUI7QUFDaEJMLFlBQVNNLElBQVQsQ0FBY25CLE1BQU1vQixHQUFOLEdBQVlDLE1BQVosR0FBcUIsR0FBckIsR0FBMkJmLFFBQVFZLEdBQWpEO0FBQ0FaLFdBQVFELEtBQVIsR0FBZ0JRLFNBQVNJLE1BQVQsU0FBc0JULFlBQXRCLENBQWhCLEdBQXdELEVBQXhEO0FBQ0EsR0FIRCxNQUlLO0FBQ0pLLFlBQVNNLElBQVQsQ0FBY25CLE1BQU1vQixHQUFOLEdBQVlDLE1BQTFCO0FBQ0FmLFdBQVFELEtBQVIsR0FBZ0JRLFNBQVNJLE1BQVQsU0FBc0JULFlBQXRCLENBQWhCLEdBQXdELEVBQXhEO0FBQ0E7QUFDRCxFQVZEOztBQVlBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0FWLFFBQU93QixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCO0FBQ0EsTUFBSWpCLFFBQVFGLElBQVosRUFBa0I7QUFDakJTLFlBQVNXLFFBQVQsQ0FBa0IsVUFBVWxCLFFBQVFGLElBQXBDO0FBQ0E7O0FBRUQ7QUFDQSxNQUFJRSxRQUFRSCxVQUFaLEVBQXdCO0FBQ3ZCVztBQUNBLEdBRkQsTUFHSztBQUNKZCxTQUFNeUIsS0FBTixDQUFZWCxnQkFBWjtBQUNBZCxTQUFNMEIsSUFBTixDQUFXLFlBQVc7QUFDckJiLGFBQVNjLE1BQVQ7QUFDQSxJQUZEO0FBR0E7O0FBRUQzQixRQUFNNEIsRUFBTixDQUFTLE9BQVQsRUFBa0JkLGdCQUFsQjtBQUNBUztBQUNBLEVBbkJEOztBQXFCQSxRQUFPekIsTUFBUDtBQUNBLENBMUZGIiwiZmlsZSI6ImlucHV0X2NvdW50ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGlucHV0X2NvdW50ZXIuanMgMjAxNi0wOC0yNVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgSW5wdXQgQ291bnRlciBXaWRnZXRcbiAqXG4gKiBBZGRzIGEgY291bnRlciBlbGVtZW50IHRvIGlucHV0IGFuZCB0ZXh0YXJlYSBlbGVtZW50cy5cbiAqXG4gKiAjIyMgT3B0aW9uczpcbiAqXG4gKiAqKlBlcnNpc3RlbmNlIHwgYGRhdGEtaW5wdXRfY291bnRlci1wZXJzaXN0ZW50YCB8IGJvb2wgfCBPcHRpb25hbCoqXG4gKlxuICogT21pdHMgdG8gaGlkZSB0aGUgY291bnRlciBlbGVtZW50LiBUaGlzIG9wdGlvbiBpcyBvcHRpb25hbCBhbmQgdGhlIGRlZmF1bHQgdmFsdWUgaXMgdHJ1ZSwgc29cbiAqIHRoZSBjb3VudGVyIGVsZW1lbnQgaXMgcGVybWFuZW50IGRpc3BsYXllZC5cbiAqXG4gKiAqKlB1bGwgfCBgZGF0YS1pbnB1dF9jb3VudGVyLXBlcnNpc3RlbnRgIHwgYm9vbC9zdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogVGhlIG9wdGlvbiBnaXZlcyB0aGUgcG9zc2liaWxpdHkgdG8gcHVsbCB0aGUgY291bnRlciBlbGVtZW50IHRvIHdoZXRoZXIgdGhlIHJpZ2h0IG9yIGxlZnQgc2lkZS5cbiAqXG4gKiAjIyMgRXhhbXBsZVxuICogYGBgaHRtbFxuICogPCEtLSBEZWZhdWx0IGlucHV0IGNvdW50ZXIgZWxlbWVudCAtLT5cbiAqIDxpbnB1dCB0eXBlPVwidGV4dFwiIGRhdGEtZ3gtd2lkZ2V0PVwiaW5wdXRfY291bnRlclwiPlxuICogPHRleHRhcmVhIGRhdGEtZ3gtd2lkZ2V0PVwiaW5wdXRfY291bnRlclwiPjwvdGV4dGFyZWE+XG4gKlxuICogPCEtLSBTaG93IGVsZW1lbnQgb24gZm9jdXMgYW5kIGhpZGUgb24gYmx1ciAtLT5cbiAqIDxpbnB1dCB0eXBlPVwidGV4dFwiIGRhdGEtaW5wdXRfY291bnRlci1wZXJzaXN0ZW50PVwiZmFsc2VcIj5cbiAqXG4gKiA8IS0tIERpc2FibGUgY291bnRlciBwdWxsIC0tPlxuICogPGlucHV0IHR5cGU9XCJ0ZXh0XCIgZGF0YS1pbnB1dF9jb3VudGVyLXB1bGw9XCJmYWxzZVwiPlxuICpcbiAqIDwhLS0gUHVsbCBjb3VudGVyIHRvIGxlZnQgc2lkZSAtLT5cbiAqIDxpbnB1dCB0eXBlPVwidGV4dFwiIGRhdGEtaW5wdXRfY291bnRlci1wdWxsPVwibGVmdFwiPlxuICogYGBgXG4gKlxuICogQG1vZHVsZSBBZG1pbi9XaWRnZXRzL2lucHV0X2NvdW50ZXJcbiAqL1xuXG5neC53aWRnZXRzLm1vZHVsZShcblx0J2lucHV0X2NvdW50ZXInLFxuXG5cdFtdLFxuXG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHQndXNlIHN0cmljdCc7XG5cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogV2lkZ2V0IFJlZmVyZW5jZVxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IFdpZGdldCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdHBlcnNpc3RlbnQ6IHRydWUsXG5cdFx0XHRcdHB1bGw6ICdyaWdodCcsXG5cdFx0XHRcdGxhYmVsOiBmYWxzZVxuXHRcdFx0fSxcblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBXaWRnZXQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fSxcblx0XHRcdFxuXHRcdFx0Y291bnRlckxhYmVsID0ganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0NPVU5URVInLCAnYWRtaW5fZ2VuZXJhbCcpLFxuXG5cdFx0XHQkY291bnRlciA9ICQoJzxzcGFuLz4nKTtcblxuXHRcdHZhciBfc2hvd0NoYXJDb3VudGVyID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdCR0aGlzLnBhcmVudCgpLmFwcGVuZCgkY291bnRlcik7XG5cdFx0XHRpZiAob3B0aW9ucy5tYXgpIHtcblx0XHRcdFx0JGNvdW50ZXIudGV4dCgkdGhpcy52YWwoKS5sZW5ndGggKyAnLycgKyBvcHRpb25zLm1heCk7XG5cdFx0XHRcdG9wdGlvbnMubGFiZWwgPyAkY291bnRlci5hcHBlbmQoYCAtICR7Y291bnRlckxhYmVsfWApIDogJyc7XG5cdFx0XHR9XG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0JGNvdW50ZXIudGV4dCgkdGhpcy52YWwoKS5sZW5ndGgpO1xuXHRcdFx0XHRvcHRpb25zLmxhYmVsID8gJGNvdW50ZXIuYXBwZW5kKGAgLSAke2NvdW50ZXJMYWJlbH1gKSA6ICcnO1xuXHRcdFx0fVxuXHRcdH07XG5cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIHdpZGdldCwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBjaGVjayBhbmQgc2V0IGhhbmRsaW5nIG9mIHBlcnNpc3RlbnQgcHVsbFxuXHRcdFx0aWYgKG9wdGlvbnMucHVsbCkge1xuXHRcdFx0XHQkY291bnRlci5hZGRDbGFzcygncHVsbC0nICsgb3B0aW9ucy5wdWxsKTtcblx0XHRcdH1cblxuXHRcdFx0Ly8gY2hlY2sgYW5kIHNldCBoYW5kbGluZyBvZiBwZXJzaXN0ZW50IG9wdGlvblxuXHRcdFx0aWYgKG9wdGlvbnMucGVyc2lzdGVudCkge1xuXHRcdFx0XHRfc2hvd0NoYXJDb3VudGVyKCk7XG5cdFx0XHR9XG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0JHRoaXMuZm9jdXMoX3Nob3dDaGFyQ291bnRlcik7XG5cdFx0XHRcdCR0aGlzLmJsdXIoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0JGNvdW50ZXIucmVtb3ZlKCk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXG5cdFx0XHQkdGhpcy5vbigna2V5dXAnLCBfc2hvd0NoYXJDb3VudGVyKTtcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKTtcbiJdfQ==
