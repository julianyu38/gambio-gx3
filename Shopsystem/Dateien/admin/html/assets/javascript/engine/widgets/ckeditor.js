'use strict';

/* --------------------------------------------------------------
 ckeditor.js 2017-07-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## CKEditor Widget
 *
 * Use this widget on a parent container to convert all the textareas with the "wysiwyg" class into 
 * CKEditor instances at once. 
 * 
 * Official CKEditor Website: {@link http://ckeditor.com}
 * 
 * ### Options 
 * 
 * **File Browser URL | `data-ckeditor-filebrowser-browse-url` | String | Optional**
 * 
 * Provide the default URL of the file browser that is integrated within the CKEditor instance. The default
 * value points is 'includes/ckeditor/filemanager/index.html'.
 * 
 * **Base URL | `data-ckeditor-base-href` | String | Optional** 
 * 
 * The base URL of the CKEditor instance. The default value points to the `http://shop.de/admin` directory.
 * 
 * **Enter Mode | `data-ckeditor-enter-mode` | Number | Optional**
 * 
 * Define the enter mode of the CKEditor instance. The default value of this option is CKEDITOR.ENTER_BR which
 * means that the editor will use the `<br>` element for every line break. For a list of possible values visit 
 * this [CKEditor API reference page](http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html#.enterMode).
 * 
 * **Shift Enter Mode | `data-ckeditor-shift-enter-mode` | Number| Optional**
 * 
 * Define the shift-enter mode of the CKEditor instance. The default value of this option is CKEDITOR.ENTER_P which
 * means that the editor will use the `<p>` element for every line break. For a list of possible values visit this
 * [CKEditor API reference page](http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html#.shiftEnterMode).
 * 
 * **Language Code | `data-ckeditor-language` | String | Optional**
 * 
 * Provide a language code for the CKEditor instance. The default value comes from the 
 * `jse.core.config.get('languageCode')` value which has the active language setting of the current page. 
 * 
 * ### Example
 * 
 * When the page loads the textarea element will be converted into a CKEditor instance.
 * 
 * ```html
 * <div data-gx-widget="ckeditor"> 
 *   <textarea name="my-textarea" class="wysiwyg"></textarea>
 * </div>    
 * ```
 * 
 * **Important: For CKEditor to work correctly the textarea elements need to have a `name` attribute.**
 *
 * @module Admin/Widgets/ckeditor
 * @requires CKEditor-Library
 */
gx.widgets.module('ckeditor', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	defaults = { // Configuration gets passed to the ckeditor.
		'filebrowserBrowseUrl': 'includes/ckeditor/filemanager/index.html',
		'baseHref': jse.core.config.get('appUrl') + '/admin',
		'enterMode': CKEDITOR.ENTER_BR,
		'shiftEnterMode': CKEDITOR.ENTER_P,
		'language': jse.core.config.get('languageCode'),
		'useRelPath': true
	},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {},


	/**
  * Editors Selector Object
  *
  * @type {object}
  */
	$editors = null;

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		if (!options.useRelPath) {
			options.filebrowserBrowseUrl += '?mode=mail';
		}

		$editors = $this.filter('.wysiwyg').add($this.find('.wysiwyg'));

		$editors.each(function () {
			var $self = $(this),
			    dataset = $.extend({}, options, $self.data()),
			    // Get textarea specific configuration.
			name = $self.attr('name');
			$self.removeClass('wysiwyg');
			CKEDITOR.replace(name, dataset);
		});

		// Event handler for the update event, which is updating the ckeditor with the value
		// of the textarea.
		$this.on('ckeditor.update', function () {
			$editors.each(function () {
				var $self = $(this),
				    name = $self.attr('name'),
				    editor = CKEDITOR ? CKEDITOR.instances[name] : null;

				if (editor) {
					editor.setData($self.val());
				}
			});
		});

		$this.trigger('widget.initialized', 'ckeditor');

		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNrZWRpdG9yLmpzIl0sIm5hbWVzIjpbImd4Iiwid2lkZ2V0cyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJDS0VESVRPUiIsIkVOVEVSX0JSIiwiRU5URVJfUCIsIm9wdGlvbnMiLCJleHRlbmQiLCIkZWRpdG9ycyIsImluaXQiLCJkb25lIiwidXNlUmVsUGF0aCIsImZpbGVicm93c2VyQnJvd3NlVXJsIiwiZmlsdGVyIiwiYWRkIiwiZmluZCIsImVhY2giLCIkc2VsZiIsImRhdGFzZXQiLCJuYW1lIiwiYXR0ciIsInJlbW92ZUNsYXNzIiwicmVwbGFjZSIsIm9uIiwiZWRpdG9yIiwiaW5zdGFuY2VzIiwic2V0RGF0YSIsInZhbCIsInRyaWdnZXIiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbURBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxVQURELEVBR0MsRUFIRCxFQUtDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXLEVBQUU7QUFDWiwwQkFBd0IsMENBRGQ7QUFFVixjQUFZQyxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLFFBRmxDO0FBR1YsZUFBYUMsU0FBU0MsUUFIWjtBQUlWLG9CQUFrQkQsU0FBU0UsT0FKakI7QUFLVixjQUFZTixJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLENBTEY7QUFNVixnQkFBYztBQU5KLEVBYlo7OztBQXNCQzs7Ozs7QUFLQUksV0FBVVQsRUFBRVUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CVCxRQUFuQixFQUE2QkgsSUFBN0IsQ0EzQlg7OztBQTZCQzs7Ozs7QUFLQUQsVUFBUyxFQWxDVjs7O0FBb0NDOzs7OztBQUtBYyxZQUFXLElBekNaOztBQTJDQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBZCxRQUFPZSxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCLE1BQUksQ0FBQ0osUUFBUUssVUFBYixFQUF5QjtBQUN4QkwsV0FBUU0sb0JBQVIsSUFBZ0MsWUFBaEM7QUFDQTs7QUFFREosYUFBV1osTUFDVGlCLE1BRFMsQ0FDRixVQURFLEVBRVRDLEdBRlMsQ0FFTGxCLE1BQU1tQixJQUFOLENBQVcsVUFBWCxDQUZLLENBQVg7O0FBSUFQLFdBQ0VRLElBREYsQ0FDTyxZQUFXO0FBQ2hCLE9BQUlDLFFBQVFwQixFQUFFLElBQUYsQ0FBWjtBQUFBLE9BQ0NxQixVQUFVckIsRUFBRVUsTUFBRixDQUFTLEVBQVQsRUFBYUQsT0FBYixFQUFzQlcsTUFBTXRCLElBQU4sRUFBdEIsQ0FEWDtBQUFBLE9BQ2dEO0FBQy9Dd0IsVUFBT0YsTUFBTUcsSUFBTixDQUFXLE1BQVgsQ0FGUjtBQUdBSCxTQUFNSSxXQUFOLENBQWtCLFNBQWxCO0FBQ0FsQixZQUFTbUIsT0FBVCxDQUFpQkgsSUFBakIsRUFBdUJELE9BQXZCO0FBQ0EsR0FQRjs7QUFTQTtBQUNBO0FBQ0F0QixRQUFNMkIsRUFBTixDQUFTLGlCQUFULEVBQTRCLFlBQVc7QUFDdENmLFlBQ0VRLElBREYsQ0FDTyxZQUFXO0FBQ2hCLFFBQUlDLFFBQVFwQixFQUFFLElBQUYsQ0FBWjtBQUFBLFFBQ0NzQixPQUFPRixNQUFNRyxJQUFOLENBQVcsTUFBWCxDQURSO0FBQUEsUUFFQ0ksU0FBVXJCLFFBQUQsR0FBYUEsU0FBU3NCLFNBQVQsQ0FBbUJOLElBQW5CLENBQWIsR0FBd0MsSUFGbEQ7O0FBSUEsUUFBSUssTUFBSixFQUFZO0FBQ1hBLFlBQU9FLE9BQVAsQ0FBZVQsTUFBTVUsR0FBTixFQUFmO0FBQ0E7QUFDRCxJQVRGO0FBVUEsR0FYRDs7QUFhQS9CLFFBQU1nQyxPQUFOLENBQWMsb0JBQWQsRUFBb0MsVUFBcEM7O0FBRUFsQjtBQUNBLEVBcENEOztBQXNDQTtBQUNBLFFBQU9oQixNQUFQO0FBQ0EsQ0F2R0YiLCJmaWxlIjoiY2tlZGl0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGNrZWRpdG9yLmpzIDIwMTctMDctMTlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIENLRWRpdG9yIFdpZGdldFxuICpcbiAqIFVzZSB0aGlzIHdpZGdldCBvbiBhIHBhcmVudCBjb250YWluZXIgdG8gY29udmVydCBhbGwgdGhlIHRleHRhcmVhcyB3aXRoIHRoZSBcInd5c2l3eWdcIiBjbGFzcyBpbnRvIFxuICogQ0tFZGl0b3IgaW5zdGFuY2VzIGF0IG9uY2UuIFxuICogXG4gKiBPZmZpY2lhbCBDS0VkaXRvciBXZWJzaXRlOiB7QGxpbmsgaHR0cDovL2NrZWRpdG9yLmNvbX1cbiAqIFxuICogIyMjIE9wdGlvbnMgXG4gKiBcbiAqICoqRmlsZSBCcm93c2VyIFVSTCB8IGBkYXRhLWNrZWRpdG9yLWZpbGVicm93c2VyLWJyb3dzZS11cmxgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICogXG4gKiBQcm92aWRlIHRoZSBkZWZhdWx0IFVSTCBvZiB0aGUgZmlsZSBicm93c2VyIHRoYXQgaXMgaW50ZWdyYXRlZCB3aXRoaW4gdGhlIENLRWRpdG9yIGluc3RhbmNlLiBUaGUgZGVmYXVsdFxuICogdmFsdWUgcG9pbnRzIGlzICdpbmNsdWRlcy9ja2VkaXRvci9maWxlbWFuYWdlci9pbmRleC5odG1sJy5cbiAqIFxuICogKipCYXNlIFVSTCB8IGBkYXRhLWNrZWRpdG9yLWJhc2UtaHJlZmAgfCBTdHJpbmcgfCBPcHRpb25hbCoqIFxuICogXG4gKiBUaGUgYmFzZSBVUkwgb2YgdGhlIENLRWRpdG9yIGluc3RhbmNlLiBUaGUgZGVmYXVsdCB2YWx1ZSBwb2ludHMgdG8gdGhlIGBodHRwOi8vc2hvcC5kZS9hZG1pbmAgZGlyZWN0b3J5LlxuICogXG4gKiAqKkVudGVyIE1vZGUgfCBgZGF0YS1ja2VkaXRvci1lbnRlci1tb2RlYCB8IE51bWJlciB8IE9wdGlvbmFsKipcbiAqIFxuICogRGVmaW5lIHRoZSBlbnRlciBtb2RlIG9mIHRoZSBDS0VkaXRvciBpbnN0YW5jZS4gVGhlIGRlZmF1bHQgdmFsdWUgb2YgdGhpcyBvcHRpb24gaXMgQ0tFRElUT1IuRU5URVJfQlIgd2hpY2hcbiAqIG1lYW5zIHRoYXQgdGhlIGVkaXRvciB3aWxsIHVzZSB0aGUgYDxicj5gIGVsZW1lbnQgZm9yIGV2ZXJ5IGxpbmUgYnJlYWsuIEZvciBhIGxpc3Qgb2YgcG9zc2libGUgdmFsdWVzIHZpc2l0IFxuICogdGhpcyBbQ0tFZGl0b3IgQVBJIHJlZmVyZW5jZSBwYWdlXShodHRwOi8vZG9jcy5ja3NvdXJjZS5jb20vY2tlZGl0b3JfYXBpL3N5bWJvbHMvQ0tFRElUT1IuY29uZmlnLmh0bWwjLmVudGVyTW9kZSkuXG4gKiBcbiAqICoqU2hpZnQgRW50ZXIgTW9kZSB8IGBkYXRhLWNrZWRpdG9yLXNoaWZ0LWVudGVyLW1vZGVgIHwgTnVtYmVyfCBPcHRpb25hbCoqXG4gKiBcbiAqIERlZmluZSB0aGUgc2hpZnQtZW50ZXIgbW9kZSBvZiB0aGUgQ0tFZGl0b3IgaW5zdGFuY2UuIFRoZSBkZWZhdWx0IHZhbHVlIG9mIHRoaXMgb3B0aW9uIGlzIENLRURJVE9SLkVOVEVSX1Agd2hpY2hcbiAqIG1lYW5zIHRoYXQgdGhlIGVkaXRvciB3aWxsIHVzZSB0aGUgYDxwPmAgZWxlbWVudCBmb3IgZXZlcnkgbGluZSBicmVhay4gRm9yIGEgbGlzdCBvZiBwb3NzaWJsZSB2YWx1ZXMgdmlzaXQgdGhpc1xuICogW0NLRWRpdG9yIEFQSSByZWZlcmVuY2UgcGFnZV0oaHR0cDovL2RvY3MuY2tzb3VyY2UuY29tL2NrZWRpdG9yX2FwaS9zeW1ib2xzL0NLRURJVE9SLmNvbmZpZy5odG1sIy5zaGlmdEVudGVyTW9kZSkuXG4gKiBcbiAqICoqTGFuZ3VhZ2UgQ29kZSB8IGBkYXRhLWNrZWRpdG9yLWxhbmd1YWdlYCB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqIFxuICogUHJvdmlkZSBhIGxhbmd1YWdlIGNvZGUgZm9yIHRoZSBDS0VkaXRvciBpbnN0YW5jZS4gVGhlIGRlZmF1bHQgdmFsdWUgY29tZXMgZnJvbSB0aGUgXG4gKiBganNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJylgIHZhbHVlIHdoaWNoIGhhcyB0aGUgYWN0aXZlIGxhbmd1YWdlIHNldHRpbmcgb2YgdGhlIGN1cnJlbnQgcGFnZS4gXG4gKiBcbiAqICMjIyBFeGFtcGxlXG4gKiBcbiAqIFdoZW4gdGhlIHBhZ2UgbG9hZHMgdGhlIHRleHRhcmVhIGVsZW1lbnQgd2lsbCBiZSBjb252ZXJ0ZWQgaW50byBhIENLRWRpdG9yIGluc3RhbmNlLlxuICogXG4gKiBgYGBodG1sXG4gKiA8ZGl2IGRhdGEtZ3gtd2lkZ2V0PVwiY2tlZGl0b3JcIj4gXG4gKiAgIDx0ZXh0YXJlYSBuYW1lPVwibXktdGV4dGFyZWFcIiBjbGFzcz1cInd5c2l3eWdcIj48L3RleHRhcmVhPlxuICogPC9kaXY+ICAgIFxuICogYGBgXG4gKiBcbiAqICoqSW1wb3J0YW50OiBGb3IgQ0tFZGl0b3IgdG8gd29yayBjb3JyZWN0bHkgdGhlIHRleHRhcmVhIGVsZW1lbnRzIG5lZWQgdG8gaGF2ZSBhIGBuYW1lYCBhdHRyaWJ1dGUuKipcbiAqXG4gKiBAbW9kdWxlIEFkbWluL1dpZGdldHMvY2tlZGl0b3JcbiAqIEByZXF1aXJlcyBDS0VkaXRvci1MaWJyYXJ5XG4gKi9cbmd4LndpZGdldHMubW9kdWxlKFxuXHQnY2tlZGl0b3InLFxuXHRcblx0W10sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogV2lkZ2V0IFJlZmVyZW5jZVxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnMgZm9yIFdpZGdldFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0geyAvLyBDb25maWd1cmF0aW9uIGdldHMgcGFzc2VkIHRvIHRoZSBja2VkaXRvci5cblx0XHRcdFx0J2ZpbGVicm93c2VyQnJvd3NlVXJsJzogJ2luY2x1ZGVzL2NrZWRpdG9yL2ZpbGVtYW5hZ2VyL2luZGV4Lmh0bWwnLFxuXHRcdFx0XHQnYmFzZUhyZWYnOiBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4nLFxuXHRcdFx0XHQnZW50ZXJNb2RlJzogQ0tFRElUT1IuRU5URVJfQlIsXG5cdFx0XHRcdCdzaGlmdEVudGVyTW9kZSc6IENLRURJVE9SLkVOVEVSX1AsXG5cdFx0XHRcdCdsYW5ndWFnZSc6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpLFxuXHRcdFx0XHQndXNlUmVsUGF0aCc6IHRydWVcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgV2lkZ2V0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge30sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRWRpdG9ycyBTZWxlY3RvciBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkZWRpdG9ycyA9IG51bGw7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIG1ldGhvZCBvZiB0aGUgd2lkZ2V0LCBjYWxsZWQgYnkgdGhlIGVuZ2luZS5cblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdGlmICghb3B0aW9ucy51c2VSZWxQYXRoKSB7XG5cdFx0XHRcdG9wdGlvbnMuZmlsZWJyb3dzZXJCcm93c2VVcmwgKz0gJz9tb2RlPW1haWwnO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkZWRpdG9ycyA9ICR0aGlzXG5cdFx0XHRcdC5maWx0ZXIoJy53eXNpd3lnJylcblx0XHRcdFx0LmFkZCgkdGhpcy5maW5kKCcud3lzaXd5ZycpKTtcblx0XHRcdFxuXHRcdFx0JGVkaXRvcnNcblx0XHRcdFx0LmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0dmFyICRzZWxmID0gJCh0aGlzKSxcblx0XHRcdFx0XHRcdGRhdGFzZXQgPSAkLmV4dGVuZCh7fSwgb3B0aW9ucywgJHNlbGYuZGF0YSgpKSwgLy8gR2V0IHRleHRhcmVhIHNwZWNpZmljIGNvbmZpZ3VyYXRpb24uXG5cdFx0XHRcdFx0XHRuYW1lID0gJHNlbGYuYXR0cignbmFtZScpO1xuXHRcdFx0XHRcdCRzZWxmLnJlbW92ZUNsYXNzKCd3eXNpd3lnJyk7XG5cdFx0XHRcdFx0Q0tFRElUT1IucmVwbGFjZShuYW1lLCBkYXRhc2V0KTtcblx0XHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIEV2ZW50IGhhbmRsZXIgZm9yIHRoZSB1cGRhdGUgZXZlbnQsIHdoaWNoIGlzIHVwZGF0aW5nIHRoZSBja2VkaXRvciB3aXRoIHRoZSB2YWx1ZVxuXHRcdFx0Ly8gb2YgdGhlIHRleHRhcmVhLlxuXHRcdFx0JHRoaXMub24oJ2NrZWRpdG9yLnVwZGF0ZScsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkZWRpdG9yc1xuXHRcdFx0XHRcdC5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0dmFyICRzZWxmID0gJCh0aGlzKSxcblx0XHRcdFx0XHRcdFx0bmFtZSA9ICRzZWxmLmF0dHIoJ25hbWUnKSxcblx0XHRcdFx0XHRcdFx0ZWRpdG9yID0gKENLRURJVE9SKSA/IENLRURJVE9SLmluc3RhbmNlc1tuYW1lXSA6IG51bGw7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGlmIChlZGl0b3IpIHtcblx0XHRcdFx0XHRcdFx0ZWRpdG9yLnNldERhdGEoJHNlbGYudmFsKCkpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLnRyaWdnZXIoJ3dpZGdldC5pbml0aWFsaXplZCcsICdja2VkaXRvcicpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byBtb2R1bGUgZW5naW5lLlxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
