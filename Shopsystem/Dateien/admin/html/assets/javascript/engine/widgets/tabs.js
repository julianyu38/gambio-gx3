'use strict';

/* --------------------------------------------------------------
 tabs.js 2016-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Tabs Widget
 *
 * This widget is a custom implementation of tabs functionality and must not be confused with
 * jQueryUI's tab widget.
 * 
 * The actual `<div>` which contains the tabs, has to have a CSS-Class named **tab-headline-wrapper**.
 * The tabs will be identified by this CSS-Class. The content of the tabs has to be in a `<div>` which has to have
 * a CSS-Class called **tab-content-wrapper**. The elements inside, have to be in the same order as the tabs.
 *
 * ### Example
 *
 * ```html
 * <div data-gx-widget="tabs">
 *   <!-- Tabs -->
 *   <div class="tab-headline-wrapper">
 *     <a href="#tab1">Tab #1</a>
 *     <a href="#tab2">Tab #2</a>
 *   </div>
 *   
 *   <!-- Content -->
 *   <div class="tab-content-wrapper">
 *     <div>Content of tab #1.</div>
 *     <div>Content of tab #2.</div>
 *   </div>
 * </div>
 * ```
 *
 * @module Admin/Widgets/tabs
 */
gx.widgets.module('tabs', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Headline Tags Selector
  *
  * @type {object}
  */
	$headlineTags = null,


	/**
  * Content Tags Selector
  *
  * @type {object}
  */
	$contentTags = null,


	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Click handler for the tabs onClick the content gets switched.
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	var _clickHandler = function _clickHandler(event) {
		event.preventDefault();
		event.stopPropagation();

		$headlineTags.removeClass('active');

		var index = $(this).addClass('active').index();

		$contentTags.hide().eq(index).show();

		$this.trigger('shown:tab', { index: index });
	};

	/**
  * Handles external "show" event
  *
  * @param {object} event jQuery event object contains information of the event.
  * @param {number} tab index to show
  */
	var _showHandler = function _showHandler(event, index) {
		event.preventDefault();
		event.stopPropagation();
		$headlineTags.eq(index).trigger('click');
	};

	// ------------------------------------------------------------------------
	// INITIALIZE
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		$headlineTags = $this.children('.tab-headline-wrapper').children('a');

		$contentTags = $this.children('.tab-content-wrapper').children('div');

		$this.addClass('ui-tabs');
		$this.on('click', '.tab-headline-wrapper > a', _clickHandler);
		$this.on('show:tab', _showHandler);

		// Set first tab as selected.
		$headlineTags.eq(0).trigger('click');

		done();
	};

	// Return data to module engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYnMuanMiXSwibmFtZXMiOlsiZ3giLCJ3aWRnZXRzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRoZWFkbGluZVRhZ3MiLCIkY29udGVudFRhZ3MiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfY2xpY2tIYW5kbGVyIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsInN0b3BQcm9wYWdhdGlvbiIsInJlbW92ZUNsYXNzIiwiaW5kZXgiLCJhZGRDbGFzcyIsImhpZGUiLCJlcSIsInNob3ciLCJ0cmlnZ2VyIiwiX3Nob3dIYW5kbGVyIiwiaW5pdCIsImRvbmUiLCJjaGlsZHJlbiIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQThCQUEsR0FBR0MsT0FBSCxDQUFXQyxNQUFYLENBQ0MsTUFERCxFQUdDLEVBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsaUJBQWdCLElBYmpCOzs7QUFlQzs7Ozs7QUFLQUMsZ0JBQWUsSUFwQmhCOzs7QUFzQkM7Ozs7O0FBS0FDLFlBQVcsRUEzQlo7OztBQTZCQzs7Ozs7QUFLQUMsV0FBVUosRUFBRUssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkwsSUFBN0IsQ0FsQ1g7OztBQW9DQzs7Ozs7QUFLQUQsVUFBUyxFQXpDVjs7QUEyQ0E7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLEtBQUlTLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBU0MsS0FBVCxFQUFnQjtBQUNuQ0EsUUFBTUMsY0FBTjtBQUNBRCxRQUFNRSxlQUFOOztBQUVBUixnQkFBY1MsV0FBZCxDQUEwQixRQUExQjs7QUFFQSxNQUFJQyxRQUFRWCxFQUFFLElBQUYsRUFDVlksUUFEVSxDQUNELFFBREMsRUFFVkQsS0FGVSxFQUFaOztBQUlBVCxlQUNFVyxJQURGLEdBRUVDLEVBRkYsQ0FFS0gsS0FGTCxFQUdFSSxJQUhGOztBQUtBaEIsUUFBTWlCLE9BQU4sQ0FBYyxXQUFkLEVBQTJCLEVBQUVMLFlBQUYsRUFBM0I7QUFDQSxFQWhCRDs7QUFrQkE7Ozs7OztBQU1BLEtBQUlNLGVBQWUsU0FBZkEsWUFBZSxDQUFTVixLQUFULEVBQWdCSSxLQUFoQixFQUF1QjtBQUN6Q0osUUFBTUMsY0FBTjtBQUNBRCxRQUFNRSxlQUFOO0FBQ0FSLGdCQUFjYSxFQUFkLENBQWlCSCxLQUFqQixFQUF3QkssT0FBeEIsQ0FBZ0MsT0FBaEM7QUFDQSxFQUpEOztBQU1BO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0FuQixRQUFPcUIsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QmxCLGtCQUFnQkYsTUFDZHFCLFFBRGMsQ0FDTCx1QkFESyxFQUVkQSxRQUZjLENBRUwsR0FGSyxDQUFoQjs7QUFJQWxCLGlCQUFlSCxNQUNicUIsUUFEYSxDQUNKLHNCQURJLEVBRWJBLFFBRmEsQ0FFSixLQUZJLENBQWY7O0FBSUFyQixRQUFNYSxRQUFOLENBQWUsU0FBZjtBQUNBYixRQUFNc0IsRUFBTixDQUFTLE9BQVQsRUFBa0IsMkJBQWxCLEVBQStDZixhQUEvQztBQUNBUCxRQUFNc0IsRUFBTixDQUFTLFVBQVQsRUFBcUJKLFlBQXJCOztBQUVBO0FBQ0FoQixnQkFDRWEsRUFERixDQUNLLENBREwsRUFFRUUsT0FGRixDQUVVLE9BRlY7O0FBSUFHO0FBQ0EsRUFuQkQ7O0FBcUJBO0FBQ0EsUUFBT3RCLE1BQVA7QUFDQSxDQTdIRiIsImZpbGUiOiJ0YWJzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiB0YWJzLmpzIDIwMTYtMDItMjNcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIFRhYnMgV2lkZ2V0XG4gKlxuICogVGhpcyB3aWRnZXQgaXMgYSBjdXN0b20gaW1wbGVtZW50YXRpb24gb2YgdGFicyBmdW5jdGlvbmFsaXR5IGFuZCBtdXN0IG5vdCBiZSBjb25mdXNlZCB3aXRoXG4gKiBqUXVlcnlVSSdzIHRhYiB3aWRnZXQuXG4gKiBcbiAqIFRoZSBhY3R1YWwgYDxkaXY+YCB3aGljaCBjb250YWlucyB0aGUgdGFicywgaGFzIHRvIGhhdmUgYSBDU1MtQ2xhc3MgbmFtZWQgKip0YWItaGVhZGxpbmUtd3JhcHBlcioqLlxuICogVGhlIHRhYnMgd2lsbCBiZSBpZGVudGlmaWVkIGJ5IHRoaXMgQ1NTLUNsYXNzLiBUaGUgY29udGVudCBvZiB0aGUgdGFicyBoYXMgdG8gYmUgaW4gYSBgPGRpdj5gIHdoaWNoIGhhcyB0byBoYXZlXG4gKiBhIENTUy1DbGFzcyBjYWxsZWQgKip0YWItY29udGVudC13cmFwcGVyKiouIFRoZSBlbGVtZW50cyBpbnNpZGUsIGhhdmUgdG8gYmUgaW4gdGhlIHNhbWUgb3JkZXIgYXMgdGhlIHRhYnMuXG4gKlxuICogIyMjIEV4YW1wbGVcbiAqXG4gKiBgYGBodG1sXG4gKiA8ZGl2IGRhdGEtZ3gtd2lkZ2V0PVwidGFic1wiPlxuICogICA8IS0tIFRhYnMgLS0+XG4gKiAgIDxkaXYgY2xhc3M9XCJ0YWItaGVhZGxpbmUtd3JhcHBlclwiPlxuICogICAgIDxhIGhyZWY9XCIjdGFiMVwiPlRhYiAjMTwvYT5cbiAqICAgICA8YSBocmVmPVwiI3RhYjJcIj5UYWIgIzI8L2E+XG4gKiAgIDwvZGl2PlxuICogICBcbiAqICAgPCEtLSBDb250ZW50IC0tPlxuICogICA8ZGl2IGNsYXNzPVwidGFiLWNvbnRlbnQtd3JhcHBlclwiPlxuICogICAgIDxkaXY+Q29udGVudCBvZiB0YWIgIzEuPC9kaXY+XG4gKiAgICAgPGRpdj5Db250ZW50IG9mIHRhYiAjMi48L2Rpdj5cbiAqICAgPC9kaXY+XG4gKiA8L2Rpdj5cbiAqIGBgYFxuICpcbiAqIEBtb2R1bGUgQWRtaW4vV2lkZ2V0cy90YWJzXG4gKi9cbmd4LndpZGdldHMubW9kdWxlKFxuXHQndGFicycsXG5cdFxuXHRbXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBXaWRnZXQgUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEhlYWRsaW5lIFRhZ3MgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkaGVhZGxpbmVUYWdzID0gbnVsbCxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBDb250ZW50IFRhZ3MgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkY29udGVudFRhZ3MgPSBudWxsLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9ucyBmb3IgV2lkZ2V0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBXaWRnZXQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsaWNrIGhhbmRsZXIgZm9yIHRoZSB0YWJzIG9uQ2xpY2sgdGhlIGNvbnRlbnQgZ2V0cyBzd2l0Y2hlZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHR2YXIgX2NsaWNrSGFuZGxlciA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHRcblx0XHRcdCRoZWFkbGluZVRhZ3MucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XG5cdFx0XHR2YXIgaW5kZXggPSAkKHRoaXMpXG5cdFx0XHRcdC5hZGRDbGFzcygnYWN0aXZlJylcblx0XHRcdFx0LmluZGV4KCk7XG5cdFx0XHRcblx0XHRcdCRjb250ZW50VGFnc1xuXHRcdFx0XHQuaGlkZSgpXG5cdFx0XHRcdC5lcShpbmRleClcblx0XHRcdFx0LnNob3coKTtcblxuXHRcdFx0JHRoaXMudHJpZ2dlcignc2hvd246dGFiJywgeyBpbmRleCB9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgZXh0ZXJuYWwgXCJzaG93XCIgZXZlbnRcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKiBAcGFyYW0ge251bWJlcn0gdGFiIGluZGV4IHRvIHNob3dcblx0XHQgKi9cblx0XHR2YXIgX3Nob3dIYW5kbGVyID0gZnVuY3Rpb24oZXZlbnQsIGluZGV4KSB7XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHQkaGVhZGxpbmVUYWdzLmVxKGluZGV4KS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaRVxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSB3aWRnZXQsIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JGhlYWRsaW5lVGFncyA9ICR0aGlzXG5cdFx0XHRcdC5jaGlsZHJlbignLnRhYi1oZWFkbGluZS13cmFwcGVyJylcblx0XHRcdFx0LmNoaWxkcmVuKCdhJyk7XG5cdFx0XHRcblx0XHRcdCRjb250ZW50VGFncyA9ICR0aGlzXG5cdFx0XHRcdC5jaGlsZHJlbignLnRhYi1jb250ZW50LXdyYXBwZXInKVxuXHRcdFx0XHQuY2hpbGRyZW4oJ2RpdicpO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5hZGRDbGFzcygndWktdGFicycpO1xuXHRcdFx0JHRoaXMub24oJ2NsaWNrJywgJy50YWItaGVhZGxpbmUtd3JhcHBlciA+IGEnLCBfY2xpY2tIYW5kbGVyKTtcblx0XHRcdCR0aGlzLm9uKCdzaG93OnRhYicsIF9zaG93SGFuZGxlcik7XG5cdFx0XHRcblx0XHRcdC8vIFNldCBmaXJzdCB0YWIgYXMgc2VsZWN0ZWQuXG5cdFx0XHQkaGVhZGxpbmVUYWdzXG5cdFx0XHRcdC5lcSgwKVxuXHRcdFx0XHQudHJpZ2dlcignY2xpY2snKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZVxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
