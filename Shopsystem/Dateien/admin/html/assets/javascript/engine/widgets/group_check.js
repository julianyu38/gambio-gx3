'use strict';

/* --------------------------------------------------------------
 group_check.js 2017-10-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Group check widget
 *
 * The widget creates a panel with switcher fields for all customer groups. If the options "user" and "section" are
 * provided, the user configuration service will be used to store the panels collapsed/expanded state.
 *
 * ### Options (Required)
 *
 * **User | `data-panel-user` | Integer | Optional**
 *
 * Customer id of user, used by user configuration service to store the collapsed state.
 *
 * **Section | `data-panel-section` | String | Optional**
 *
 * Panel section, used by user configuration service 'configuration_key'. The value get a "**'group_check_'**"-prefix.
 *
 * ### Options (Additional)
 *
 * **Selected | `data-group_check-selected` | String | Additional**
 *
 * Comma separated list of customer status ids. If an switcher value is equal to one of the customer ids, the
 * switcher state is active. Alternatively you can set the value "all" to set all switchers to an active state.
 *
 * **Name | `data-group_check-name` | String | Additional**
 *
 * Name attribute of switchers hidden input:checkbox field. If no value is provided, it defaults to **'group_check'**
 *
 * ### Example
 *
 * * ```html
 * <div data-gx-widget="group_check"
 *      data-group_check-user="{$smarty.session.customer_id}"
 *      data-group_check-section="group-check-sample-section"
 *      data-group_check-active="[1,2,3]|[all]"
 *      data-group_check-name="custom-switcher-name"></div>
 * ```
 *
 * @module Admin/Widgets/group_check
 */
gx.widgets.module('group_check', ['xhr'],

/** @lends module:Widgets/group_check */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Widget Reference
  *
  * @type {object}
  */

	var $this = $(this);

	/**
  * Default Widget Options
  *
  * @type {object}
  */
	var defaults = {
		name: 'group_check'
	};

	/**
  * Final Widget Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	/**
  * Required options.
  *
  * @type {String[]}
  */
	var requiredOptions = ['user', 'section'];

	/**
  * Element for whether selecting or deselecting all other switcher.
  */
	var $allSwitcher = void 0;

	/**
  * Renders the group check box.
  *
  * @private
  */
	var _renderGroupCheck = function _renderGroupCheck() {
		jse.libs.xhr.get({
			url: './admin.php?do=JSWidgetsAjax/isGroupCheckEnabled'
		}).done(function (r) {
			if (r.status) {
				var $container = $('<div/>', {
					'data-gx-widget': 'panel switcher',
					'data-panel-title': jse.core.lang.translate('HEADING_GROUP_CHECK', 'content_manager'),
					'data-panel-user': options.user,
					'data-panel-section': 'group_check_' + options.section,
					'data-panel-container_class': 'group-check'
				});

				$container.append(_renderBody(r.customerGroups)).appendTo($this);
				gx.widgets.init($container).then(function () {
					_setEventListener();
					_setSwitcherDefaultStates();
				});
			}
		});
	};

	/**
  * Sets default state of switcher elements.
  * The "selected" option is used to determine the default state.
  *
  * @private
  */
	var _setSwitcherDefaultStates = function _setSwitcherDefaultStates() {
		// just continue if option is not set
		if (undefined === options.selected || options.selected === '') {
			return;
		}

		// use bulk switcher action if option value is set to "all"
		if (options.selected === 'all') {
			$allSwitcher.find('input:checkbox').switcher('checked', true);
			_bulkSwitcherAction(true);
			return;
		}

		// activate switcher programmatically
		var preselection = void 0;
		if (Number.isInteger(options.selected)) {
			preselection = [options.selected];
		} else {
			preselection = options.selected.split(',').map(Number);
		}

		var switcher = $this.find('input:checkbox');
		var i = 0;

		for (; i < switcher.length; i++) {
			if (switcher[i].value !== 'all') {
				if (preselection.indexOf(parseInt(switcher[i].value, 10)) !== -1) {
					$(switcher[i]).switcher('checked', true);
				}
			}
		}
		_tryActiveAllSwitcher();
	};

	/**
  * Renders the panel body.
  * @param {Object} customerGroups Serialized customer group collection, provided by ajax request.
  * @returns {jQuery} Panel body html.
  * @private
  */
	var _renderBody = function _renderBody(customerGroups) {
		var $gxContainer = $('<div/>', {
			'class': 'gx-container'
		});
		var $fieldSet = $('<fieldset/>');

		$allSwitcher = _renderFormGroup(jse.core.lang.translate('LABLE_GROUPCHECK_ALL', 'content_manager'), 'all');
		$fieldSet.append($allSwitcher);

		var i = 0;
		for (; i < customerGroups.length; i++) {
			$fieldSet.append(_renderFormGroup(customerGroups[i].names[jse.core.config.get('languageCode').toUpperCase()], customerGroups[i].id));
		}
		$gxContainer.append($fieldSet);

		return $gxContainer;
	};

	/**
  * Renders the form group elements of the group check.
  *
  * @param {string} label Label name.
  * @param {string} value Switchers value attribute.
  * @returns {jQuery} Form group html.
  * @private
  */
	var _renderFormGroup = function _renderFormGroup(label, value) {
		var $formGroup = $('<div/>', { 'class': 'form-group' });
		var $label = $('<label/>', {
			'for': 'customer-groups-' + label.toLowerCase(),
			'class': 'col-md-4',
			'text': label
		});
		var $inputContainer = $('<div/>', {
			'class': 'col-md-6'
		});
		var $input = $('<input/>', {
			'type': 'checkbox',
			'id': 'customer-groups-' + label.toLowerCase(),
			'name': options.name + '[]',
			'value': value,
			'checked': options.all_checked
		});
		$inputContainer.append($input);

		return $formGroup.append($label).append($inputContainer);
	};

	/**
  * Checks if all required options are passed.
  *
  * @private
  */
	var _checkRequiredOptions = function _checkRequiredOptions() {
		var i = 0;
		for (; i < requiredOptions.length; i++) {
			if (undefined === options[requiredOptions[i]]) {
				throw new Error('Required widget option "' + requiredOptions[i] + '" is no set!');
			}
		}
	};

	/**
  * Sets the event listener for the group check widget.
  *
  * @private
  */
	var _setEventListener = function _setEventListener() {
		var switcher = $this.find('input:checkbox');

		// check all switcher if the "all" switcher is clicked
		$allSwitcher.on('change', function () {
			if ($allSwitcher.find('input:checkbox').prop('checked')) {
				_bulkSwitcherAction(true);
			} else {
				_bulkSwitcherAction(false);
			}
		});

		switcher.on('change', function (e) {
			if (!$allSwitcher.find('input:checkbox').is(e.currentTarget)) {
				// remove checked attribute from "all" switcher if one is deselected
				if (!$(e.currentTarget).prop('checked')) {
					$allSwitcher.find('input:checkbox').switcher('checked', false);
				}

				_tryActiveAllSwitcher();
			}
		});
	};

	/**
  * This method checks if all other switcher fields instead of "all" are active.
  * If so, the "all" switcher will be activated.
  *
  * @private
  */
	var _tryActiveAllSwitcher = function _tryActiveAllSwitcher() {
		var switcher = $this.find('input:checkbox');
		var allChecked = true;
		var i = 0;

		for (; i < switcher.length; i++) {
			if (!$allSwitcher.find('input:checkbox').is($(switcher[i]))) {
				allChecked = allChecked && $(switcher[i]).prop('checked');
			}
		}
		if (allChecked) {
			$allSwitcher.find('input:checkbox').switcher('checked', true);
		}
	};

	/**
  * Bulk action for whether selecting or deselecting all switcher elements.
  *
  * @param {boolean} checked Status of "checked" property.
  * @private
  */
	var _bulkSwitcherAction = function _bulkSwitcherAction(checked) {
		var switcher = $this.find('input:checkbox');
		var i = 0;
		for (; i < switcher.length; i++) {
			if (!$allSwitcher.find('input:checkbox').is($(switcher[i]))) {
				$(switcher[i]).switcher('checked', checked);
			}
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		_checkRequiredOptions();
		_renderGroupCheck();
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdyb3VwX2NoZWNrLmpzIl0sIm5hbWVzIjpbImd4Iiwid2lkZ2V0cyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm5hbWUiLCJvcHRpb25zIiwiZXh0ZW5kIiwicmVxdWlyZWRPcHRpb25zIiwiJGFsbFN3aXRjaGVyIiwiX3JlbmRlckdyb3VwQ2hlY2siLCJqc2UiLCJsaWJzIiwieGhyIiwiZ2V0IiwidXJsIiwiZG9uZSIsInIiLCJzdGF0dXMiLCIkY29udGFpbmVyIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJ1c2VyIiwic2VjdGlvbiIsImFwcGVuZCIsIl9yZW5kZXJCb2R5IiwiY3VzdG9tZXJHcm91cHMiLCJhcHBlbmRUbyIsImluaXQiLCJ0aGVuIiwiX3NldEV2ZW50TGlzdGVuZXIiLCJfc2V0U3dpdGNoZXJEZWZhdWx0U3RhdGVzIiwidW5kZWZpbmVkIiwic2VsZWN0ZWQiLCJmaW5kIiwic3dpdGNoZXIiLCJfYnVsa1N3aXRjaGVyQWN0aW9uIiwicHJlc2VsZWN0aW9uIiwiTnVtYmVyIiwiaXNJbnRlZ2VyIiwic3BsaXQiLCJtYXAiLCJpIiwibGVuZ3RoIiwidmFsdWUiLCJpbmRleE9mIiwicGFyc2VJbnQiLCJfdHJ5QWN0aXZlQWxsU3dpdGNoZXIiLCIkZ3hDb250YWluZXIiLCIkZmllbGRTZXQiLCJfcmVuZGVyRm9ybUdyb3VwIiwibmFtZXMiLCJjb25maWciLCJ0b1VwcGVyQ2FzZSIsImlkIiwibGFiZWwiLCIkZm9ybUdyb3VwIiwiJGxhYmVsIiwidG9Mb3dlckNhc2UiLCIkaW5wdXRDb250YWluZXIiLCIkaW5wdXQiLCJhbGxfY2hlY2tlZCIsIl9jaGVja1JlcXVpcmVkT3B0aW9ucyIsIkVycm9yIiwib24iLCJwcm9wIiwiaXMiLCJlIiwiY3VycmVudFRhcmdldCIsImFsbENoZWNrZWQiLCJjaGVja2VkIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXVDQUEsR0FBR0MsT0FBSCxDQUFXQyxNQUFYLENBQ0MsYUFERCxFQUdDLENBQUMsS0FBRCxDQUhEOztBQUtDOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVc7QUFDaEJDLFFBQU07QUFEVSxFQUFqQjs7QUFJQTs7Ozs7QUFLQSxLQUFNQyxVQUFVSCxFQUFFSSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJILFFBQW5CLEVBQTZCSCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRCxTQUFTLEVBQWY7O0FBRUE7Ozs7O0FBS0EsS0FBTVEsa0JBQWtCLENBQUMsTUFBRCxFQUFTLFNBQVQsQ0FBeEI7O0FBRUE7OztBQUdBLEtBQUlDLHFCQUFKOztBQUVBOzs7OztBQUtBLEtBQU1DLG9CQUFvQixTQUFwQkEsaUJBQW9CLEdBQU07QUFDL0JDLE1BQUlDLElBQUosQ0FBU0MsR0FBVCxDQUFhQyxHQUFiLENBQWlCO0FBQ2hCQyxRQUFLO0FBRFcsR0FBakIsRUFFR0MsSUFGSCxDQUVRLGFBQUs7QUFDWixPQUFJQyxFQUFFQyxNQUFOLEVBQWM7QUFDYixRQUFNQyxhQUFhaEIsRUFBRSxRQUFGLEVBQVk7QUFDOUIsdUJBQWtCLGdCQURZO0FBRTlCLHlCQUFvQlEsSUFBSVMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IscUJBQXhCLEVBQStDLGlCQUEvQyxDQUZVO0FBRzlCLHdCQUFtQmhCLFFBQVFpQixJQUhHO0FBSTlCLDJCQUFzQixpQkFBaUJqQixRQUFRa0IsT0FKakI7QUFLOUIsbUNBQThCO0FBTEEsS0FBWixDQUFuQjs7QUFRQUwsZUFBV00sTUFBWCxDQUFrQkMsWUFBWVQsRUFBRVUsY0FBZCxDQUFsQixFQUFpREMsUUFBakQsQ0FBMEQxQixLQUExRDtBQUNBSixPQUFHQyxPQUFILENBQVc4QixJQUFYLENBQWdCVixVQUFoQixFQUE0QlcsSUFBNUIsQ0FBaUMsWUFBTTtBQUN0Q0M7QUFDQUM7QUFDQSxLQUhEO0FBSUE7QUFDRCxHQWxCRDtBQW1CQSxFQXBCRDs7QUFzQkE7Ozs7OztBQU1BLEtBQU1BLDRCQUE0QixTQUE1QkEseUJBQTRCLEdBQU07QUFDdkM7QUFDQSxNQUFJQyxjQUFjM0IsUUFBUTRCLFFBQXRCLElBQWtDNUIsUUFBUTRCLFFBQVIsS0FBcUIsRUFBM0QsRUFBK0Q7QUFDOUQ7QUFDQTs7QUFFRDtBQUNBLE1BQUk1QixRQUFRNEIsUUFBUixLQUFxQixLQUF6QixFQUFnQztBQUMvQnpCLGdCQUFhMEIsSUFBYixDQUFrQixnQkFBbEIsRUFBb0NDLFFBQXBDLENBQTZDLFNBQTdDLEVBQXdELElBQXhEO0FBQ0FDLHVCQUFvQixJQUFwQjtBQUNBO0FBQ0E7O0FBRUQ7QUFDQSxNQUFJQyxxQkFBSjtBQUNBLE1BQUlDLE9BQU9DLFNBQVAsQ0FBaUJsQyxRQUFRNEIsUUFBekIsQ0FBSixFQUF3QztBQUN2Q0ksa0JBQWUsQ0FBQ2hDLFFBQVE0QixRQUFULENBQWY7QUFDQSxHQUZELE1BRU87QUFDTkksa0JBQWVoQyxRQUFRNEIsUUFBUixDQUFpQk8sS0FBakIsQ0FBdUIsR0FBdkIsRUFBNEJDLEdBQTVCLENBQWdDSCxNQUFoQyxDQUFmO0FBQ0E7O0FBRUQsTUFBTUgsV0FBV2xDLE1BQU1pQyxJQUFOLENBQVcsZ0JBQVgsQ0FBakI7QUFDQSxNQUFJUSxJQUFJLENBQVI7O0FBRUEsU0FBT0EsSUFBSVAsU0FBU1EsTUFBcEIsRUFBNEJELEdBQTVCLEVBQWlDO0FBQ2hDLE9BQUlQLFNBQVNPLENBQVQsRUFBWUUsS0FBWixLQUFzQixLQUExQixFQUFpQztBQUNoQyxRQUFJUCxhQUFhUSxPQUFiLENBQXFCQyxTQUFTWCxTQUFTTyxDQUFULEVBQVlFLEtBQXJCLEVBQTRCLEVBQTVCLENBQXJCLE1BQTBELENBQUMsQ0FBL0QsRUFBa0U7QUFDakUxQyxPQUFFaUMsU0FBU08sQ0FBVCxDQUFGLEVBQWVQLFFBQWYsQ0FBd0IsU0FBeEIsRUFBbUMsSUFBbkM7QUFDQTtBQUNEO0FBQ0Q7QUFDRFk7QUFDQSxFQWhDRDs7QUFrQ0E7Ozs7OztBQU1BLEtBQU10QixjQUFjLFNBQWRBLFdBQWMsaUJBQWtCO0FBQ3JDLE1BQU11QixlQUFlOUMsRUFBRSxRQUFGLEVBQVk7QUFDaEMsWUFBUztBQUR1QixHQUFaLENBQXJCO0FBR0EsTUFBTStDLFlBQVkvQyxFQUFFLGFBQUYsQ0FBbEI7O0FBRUFNLGlCQUFlMEMsaUJBQWlCeEMsSUFBSVMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isc0JBQXhCLEVBQWdELGlCQUFoRCxDQUFqQixFQUFxRixLQUFyRixDQUFmO0FBQ0E0QixZQUFVekIsTUFBVixDQUFpQmhCLFlBQWpCOztBQUVBLE1BQUlrQyxJQUFJLENBQVI7QUFDQSxTQUFPQSxJQUFJaEIsZUFBZWlCLE1BQTFCLEVBQWtDRCxHQUFsQyxFQUF1QztBQUN0Q08sYUFBVXpCLE1BQVYsQ0FBaUIwQixpQkFBaUJ4QixlQUFlZ0IsQ0FBZixFQUFrQlMsS0FBbEIsQ0FBd0J6QyxJQUFJUyxJQUFKLENBQVNpQyxNQUFULENBQWdCdkMsR0FBaEIsQ0FBb0IsY0FBcEIsRUFDeER3QyxXQUR3RCxFQUF4QixDQUFqQixFQUNDM0IsZUFBZWdCLENBQWYsRUFBa0JZLEVBRG5CLENBQWpCO0FBRUE7QUFDRE4sZUFBYXhCLE1BQWIsQ0FBb0J5QixTQUFwQjs7QUFFQSxTQUFPRCxZQUFQO0FBQ0EsRUFqQkQ7O0FBbUJBOzs7Ozs7OztBQVFBLEtBQU1FLG1CQUFtQixTQUFuQkEsZ0JBQW1CLENBQUNLLEtBQUQsRUFBUVgsS0FBUixFQUFrQjtBQUMxQyxNQUFNWSxhQUFhdEQsRUFBRSxRQUFGLEVBQVksRUFBQyxTQUFTLFlBQVYsRUFBWixDQUFuQjtBQUNBLE1BQU11RCxTQUFTdkQsRUFBRSxVQUFGLEVBQWM7QUFDNUIsVUFBTyxxQkFBcUJxRCxNQUFNRyxXQUFOLEVBREE7QUFFNUIsWUFBUyxVQUZtQjtBQUc1QixXQUFRSDtBQUhvQixHQUFkLENBQWY7QUFLQSxNQUFNSSxrQkFBa0J6RCxFQUFFLFFBQUYsRUFBWTtBQUNuQyxZQUFTO0FBRDBCLEdBQVosQ0FBeEI7QUFHQSxNQUFNMEQsU0FBUzFELEVBQUUsVUFBRixFQUFjO0FBQzVCLFdBQVEsVUFEb0I7QUFFNUIsU0FBTSxxQkFBcUJxRCxNQUFNRyxXQUFOLEVBRkM7QUFHNUIsV0FBUXJELFFBQVFELElBQVIsR0FBZSxJQUhLO0FBSTVCLFlBQVN3QyxLQUptQjtBQUs1QixjQUFXdkMsUUFBUXdEO0FBTFMsR0FBZCxDQUFmO0FBT0FGLGtCQUFnQm5DLE1BQWhCLENBQXVCb0MsTUFBdkI7O0FBRUEsU0FBT0osV0FBV2hDLE1BQVgsQ0FBa0JpQyxNQUFsQixFQUEwQmpDLE1BQTFCLENBQWlDbUMsZUFBakMsQ0FBUDtBQUNBLEVBcEJEOztBQXNCQTs7Ozs7QUFLQSxLQUFNRyx3QkFBd0IsU0FBeEJBLHFCQUF3QixHQUFNO0FBQ25DLE1BQUlwQixJQUFJLENBQVI7QUFDQSxTQUFPQSxJQUFJbkMsZ0JBQWdCb0MsTUFBM0IsRUFBbUNELEdBQW5DLEVBQXdDO0FBQ3ZDLE9BQUlWLGNBQWMzQixRQUFRRSxnQkFBZ0JtQyxDQUFoQixDQUFSLENBQWxCLEVBQStDO0FBQzlDLFVBQU0sSUFBSXFCLEtBQUosQ0FBVSw2QkFBNkJ4RCxnQkFBZ0JtQyxDQUFoQixDQUE3QixHQUFrRCxjQUE1RCxDQUFOO0FBQ0E7QUFDRDtBQUNELEVBUEQ7O0FBU0E7Ozs7O0FBS0EsS0FBTVosb0JBQW9CLFNBQXBCQSxpQkFBb0IsR0FBTTtBQUMvQixNQUFNSyxXQUFXbEMsTUFBTWlDLElBQU4sQ0FBVyxnQkFBWCxDQUFqQjs7QUFFQTtBQUNBMUIsZUFBYXdELEVBQWIsQ0FBZ0IsUUFBaEIsRUFBMEIsWUFBTTtBQUMvQixPQUFJeEQsYUFBYTBCLElBQWIsQ0FBa0IsZ0JBQWxCLEVBQW9DK0IsSUFBcEMsQ0FBeUMsU0FBekMsQ0FBSixFQUF5RDtBQUN4RDdCLHdCQUFvQixJQUFwQjtBQUNBLElBRkQsTUFFTztBQUNOQSx3QkFBb0IsS0FBcEI7QUFDQTtBQUNELEdBTkQ7O0FBUUFELFdBQVM2QixFQUFULENBQVksUUFBWixFQUFzQixhQUFLO0FBQzFCLE9BQUksQ0FBQ3hELGFBQWEwQixJQUFiLENBQWtCLGdCQUFsQixFQUFvQ2dDLEVBQXBDLENBQXVDQyxFQUFFQyxhQUF6QyxDQUFMLEVBQThEO0FBQzdEO0FBQ0EsUUFBSSxDQUFDbEUsRUFBRWlFLEVBQUVDLGFBQUosRUFBbUJILElBQW5CLENBQXdCLFNBQXhCLENBQUwsRUFBeUM7QUFDeEN6RCxrQkFBYTBCLElBQWIsQ0FBa0IsZ0JBQWxCLEVBQW9DQyxRQUFwQyxDQUE2QyxTQUE3QyxFQUF3RCxLQUF4RDtBQUNBOztBQUVEWTtBQUNBO0FBQ0QsR0FURDtBQVVBLEVBdEJEOztBQXdCQTs7Ozs7O0FBTUEsS0FBTUEsd0JBQXdCLFNBQXhCQSxxQkFBd0IsR0FBTTtBQUNuQyxNQUFNWixXQUFXbEMsTUFBTWlDLElBQU4sQ0FBVyxnQkFBWCxDQUFqQjtBQUNBLE1BQUltQyxhQUFhLElBQWpCO0FBQ0EsTUFBSTNCLElBQUksQ0FBUjs7QUFFQSxTQUFPQSxJQUFJUCxTQUFTUSxNQUFwQixFQUE0QkQsR0FBNUIsRUFBaUM7QUFDaEMsT0FBSSxDQUFDbEMsYUFBYTBCLElBQWIsQ0FBa0IsZ0JBQWxCLEVBQW9DZ0MsRUFBcEMsQ0FBdUNoRSxFQUFFaUMsU0FBU08sQ0FBVCxDQUFGLENBQXZDLENBQUwsRUFBNkQ7QUFDNUQyQixpQkFBYUEsY0FBY25FLEVBQUVpQyxTQUFTTyxDQUFULENBQUYsRUFBZXVCLElBQWYsQ0FBb0IsU0FBcEIsQ0FBM0I7QUFDQTtBQUNEO0FBQ0QsTUFBSUksVUFBSixFQUFnQjtBQUNmN0QsZ0JBQWEwQixJQUFiLENBQWtCLGdCQUFsQixFQUFvQ0MsUUFBcEMsQ0FBNkMsU0FBN0MsRUFBd0QsSUFBeEQ7QUFDQTtBQUNELEVBYkQ7O0FBZUE7Ozs7OztBQU1BLEtBQU1DLHNCQUFzQixTQUF0QkEsbUJBQXNCLFVBQVc7QUFDdEMsTUFBTUQsV0FBV2xDLE1BQU1pQyxJQUFOLENBQVcsZ0JBQVgsQ0FBakI7QUFDQSxNQUFJUSxJQUFJLENBQVI7QUFDQSxTQUFPQSxJQUFJUCxTQUFTUSxNQUFwQixFQUE0QkQsR0FBNUIsRUFBaUM7QUFDaEMsT0FBSSxDQUFDbEMsYUFBYTBCLElBQWIsQ0FBa0IsZ0JBQWxCLEVBQW9DZ0MsRUFBcEMsQ0FBdUNoRSxFQUFFaUMsU0FBU08sQ0FBVCxDQUFGLENBQXZDLENBQUwsRUFBNkQ7QUFDNUR4QyxNQUFFaUMsU0FBU08sQ0FBVCxDQUFGLEVBQWVQLFFBQWYsQ0FBd0IsU0FBeEIsRUFBbUNtQyxPQUFuQztBQUNBO0FBQ0Q7QUFDRCxFQVJEOztBQVVBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0F2RSxRQUFPNkIsSUFBUCxHQUFjLGdCQUFRO0FBQ3JCa0M7QUFDQXJEO0FBQ0FNO0FBQ0EsRUFKRDs7QUFNQSxRQUFPaEIsTUFBUDtBQUNBLENBalJGIiwiZmlsZSI6Imdyb3VwX2NoZWNrLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBncm91cF9jaGVjay5qcyAyMDE3LTEwLTEwXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBHcm91cCBjaGVjayB3aWRnZXRcbiAqXG4gKiBUaGUgd2lkZ2V0IGNyZWF0ZXMgYSBwYW5lbCB3aXRoIHN3aXRjaGVyIGZpZWxkcyBmb3IgYWxsIGN1c3RvbWVyIGdyb3Vwcy4gSWYgdGhlIG9wdGlvbnMgXCJ1c2VyXCIgYW5kIFwic2VjdGlvblwiIGFyZVxuICogcHJvdmlkZWQsIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZSB3aWxsIGJlIHVzZWQgdG8gc3RvcmUgdGhlIHBhbmVscyBjb2xsYXBzZWQvZXhwYW5kZWQgc3RhdGUuXG4gKlxuICogIyMjIE9wdGlvbnMgKFJlcXVpcmVkKVxuICpcbiAqICoqVXNlciB8IGBkYXRhLXBhbmVsLXVzZXJgIHwgSW50ZWdlciB8IE9wdGlvbmFsKipcbiAqXG4gKiBDdXN0b21lciBpZCBvZiB1c2VyLCB1c2VkIGJ5IHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlIHRvIHN0b3JlIHRoZSBjb2xsYXBzZWQgc3RhdGUuXG4gKlxuICogKipTZWN0aW9uIHwgYGRhdGEtcGFuZWwtc2VjdGlvbmAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogUGFuZWwgc2VjdGlvbiwgdXNlZCBieSB1c2VyIGNvbmZpZ3VyYXRpb24gc2VydmljZSAnY29uZmlndXJhdGlvbl9rZXknLiBUaGUgdmFsdWUgZ2V0IGEgXCIqKidncm91cF9jaGVja18nKipcIi1wcmVmaXguXG4gKlxuICogIyMjIE9wdGlvbnMgKEFkZGl0aW9uYWwpXG4gKlxuICogKipTZWxlY3RlZCB8IGBkYXRhLWdyb3VwX2NoZWNrLXNlbGVjdGVkYCB8IFN0cmluZyB8IEFkZGl0aW9uYWwqKlxuICpcbiAqIENvbW1hIHNlcGFyYXRlZCBsaXN0IG9mIGN1c3RvbWVyIHN0YXR1cyBpZHMuIElmIGFuIHN3aXRjaGVyIHZhbHVlIGlzIGVxdWFsIHRvIG9uZSBvZiB0aGUgY3VzdG9tZXIgaWRzLCB0aGVcbiAqIHN3aXRjaGVyIHN0YXRlIGlzIGFjdGl2ZS4gQWx0ZXJuYXRpdmVseSB5b3UgY2FuIHNldCB0aGUgdmFsdWUgXCJhbGxcIiB0byBzZXQgYWxsIHN3aXRjaGVycyB0byBhbiBhY3RpdmUgc3RhdGUuXG4gKlxuICogKipOYW1lIHwgYGRhdGEtZ3JvdXBfY2hlY2stbmFtZWAgfCBTdHJpbmcgfCBBZGRpdGlvbmFsKipcbiAqXG4gKiBOYW1lIGF0dHJpYnV0ZSBvZiBzd2l0Y2hlcnMgaGlkZGVuIGlucHV0OmNoZWNrYm94IGZpZWxkLiBJZiBubyB2YWx1ZSBpcyBwcm92aWRlZCwgaXQgZGVmYXVsdHMgdG8gKionZ3JvdXBfY2hlY2snKipcbiAqXG4gKiAjIyMgRXhhbXBsZVxuICpcbiAqICogYGBgaHRtbFxuICogPGRpdiBkYXRhLWd4LXdpZGdldD1cImdyb3VwX2NoZWNrXCJcbiAqICAgICAgZGF0YS1ncm91cF9jaGVjay11c2VyPVwieyRzbWFydHkuc2Vzc2lvbi5jdXN0b21lcl9pZH1cIlxuICogICAgICBkYXRhLWdyb3VwX2NoZWNrLXNlY3Rpb249XCJncm91cC1jaGVjay1zYW1wbGUtc2VjdGlvblwiXG4gKiAgICAgIGRhdGEtZ3JvdXBfY2hlY2stYWN0aXZlPVwiWzEsMiwzXXxbYWxsXVwiXG4gKiAgICAgIGRhdGEtZ3JvdXBfY2hlY2stbmFtZT1cImN1c3RvbS1zd2l0Y2hlci1uYW1lXCI+PC9kaXY+XG4gKiBgYGBcbiAqXG4gKiBAbW9kdWxlIEFkbWluL1dpZGdldHMvZ3JvdXBfY2hlY2tcbiAqL1xuZ3gud2lkZ2V0cy5tb2R1bGUoXG5cdCdncm91cF9jaGVjaycsXG5cdFxuXHRbJ3hociddLFxuXHRcblx0LyoqIEBsZW5kcyBtb2R1bGU6V2lkZ2V0cy9ncm91cF9jaGVjayAqL1xuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogV2lkZ2V0IFJlZmVyZW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBXaWRnZXQgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBkZWZhdWx0cyA9IHtcblx0XHRcdG5hbWU6ICdncm91cF9jaGVjaydcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmFsIFdpZGdldCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmVxdWlyZWQgb3B0aW9ucy5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtTdHJpbmdbXX1cblx0XHQgKi9cblx0XHRjb25zdCByZXF1aXJlZE9wdGlvbnMgPSBbJ3VzZXInLCAnc2VjdGlvbiddO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEVsZW1lbnQgZm9yIHdoZXRoZXIgc2VsZWN0aW5nIG9yIGRlc2VsZWN0aW5nIGFsbCBvdGhlciBzd2l0Y2hlci5cblx0XHQgKi9cblx0XHRsZXQgJGFsbFN3aXRjaGVyO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlbmRlcnMgdGhlIGdyb3VwIGNoZWNrIGJveC5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0Y29uc3QgX3JlbmRlckdyb3VwQ2hlY2sgPSAoKSA9PiB7XG5cdFx0XHRqc2UubGlicy54aHIuZ2V0KHtcblx0XHRcdFx0dXJsOiAnLi9hZG1pbi5waHA/ZG89SlNXaWRnZXRzQWpheC9pc0dyb3VwQ2hlY2tFbmFibGVkJ1xuXHRcdFx0fSkuZG9uZShyID0+IHtcblx0XHRcdFx0aWYgKHIuc3RhdHVzKSB7XG5cdFx0XHRcdFx0Y29uc3QgJGNvbnRhaW5lciA9ICQoJzxkaXYvPicsIHtcblx0XHRcdFx0XHRcdCdkYXRhLWd4LXdpZGdldCc6ICdwYW5lbCBzd2l0Y2hlcicsXG5cdFx0XHRcdFx0XHQnZGF0YS1wYW5lbC10aXRsZSc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdIRUFESU5HX0dST1VQX0NIRUNLJywgJ2NvbnRlbnRfbWFuYWdlcicpLFxuXHRcdFx0XHRcdFx0J2RhdGEtcGFuZWwtdXNlcic6IG9wdGlvbnMudXNlcixcblx0XHRcdFx0XHRcdCdkYXRhLXBhbmVsLXNlY3Rpb24nOiAnZ3JvdXBfY2hlY2tfJyArIG9wdGlvbnMuc2VjdGlvbixcblx0XHRcdFx0XHRcdCdkYXRhLXBhbmVsLWNvbnRhaW5lcl9jbGFzcyc6ICdncm91cC1jaGVjaydcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkY29udGFpbmVyLmFwcGVuZChfcmVuZGVyQm9keShyLmN1c3RvbWVyR3JvdXBzKSkuYXBwZW5kVG8oJHRoaXMpO1xuXHRcdFx0XHRcdGd4LndpZGdldHMuaW5pdCgkY29udGFpbmVyKS50aGVuKCgpID0+IHtcblx0XHRcdFx0XHRcdF9zZXRFdmVudExpc3RlbmVyKCk7XG5cdFx0XHRcdFx0XHRfc2V0U3dpdGNoZXJEZWZhdWx0U3RhdGVzKCk7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2V0cyBkZWZhdWx0IHN0YXRlIG9mIHN3aXRjaGVyIGVsZW1lbnRzLlxuXHRcdCAqIFRoZSBcInNlbGVjdGVkXCIgb3B0aW9uIGlzIHVzZWQgdG8gZGV0ZXJtaW5lIHRoZSBkZWZhdWx0IHN0YXRlLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRjb25zdCBfc2V0U3dpdGNoZXJEZWZhdWx0U3RhdGVzID0gKCkgPT4ge1xuXHRcdFx0Ly8ganVzdCBjb250aW51ZSBpZiBvcHRpb24gaXMgbm90IHNldFxuXHRcdFx0aWYgKHVuZGVmaW5lZCA9PT0gb3B0aW9ucy5zZWxlY3RlZCB8fCBvcHRpb25zLnNlbGVjdGVkID09PSAnJykge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIHVzZSBidWxrIHN3aXRjaGVyIGFjdGlvbiBpZiBvcHRpb24gdmFsdWUgaXMgc2V0IHRvIFwiYWxsXCJcblx0XHRcdGlmIChvcHRpb25zLnNlbGVjdGVkID09PSAnYWxsJykge1xuXHRcdFx0XHQkYWxsU3dpdGNoZXIuZmluZCgnaW5wdXQ6Y2hlY2tib3gnKS5zd2l0Y2hlcignY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0XHRfYnVsa1N3aXRjaGVyQWN0aW9uKHRydWUpO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIGFjdGl2YXRlIHN3aXRjaGVyIHByb2dyYW1tYXRpY2FsbHlcblx0XHRcdGxldCBwcmVzZWxlY3Rpb247XG5cdFx0XHRpZiAoTnVtYmVyLmlzSW50ZWdlcihvcHRpb25zLnNlbGVjdGVkKSkge1xuXHRcdFx0XHRwcmVzZWxlY3Rpb24gPSBbb3B0aW9ucy5zZWxlY3RlZF07XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRwcmVzZWxlY3Rpb24gPSBvcHRpb25zLnNlbGVjdGVkLnNwbGl0KCcsJykubWFwKE51bWJlcik7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGNvbnN0IHN3aXRjaGVyID0gJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3gnKTtcblx0XHRcdGxldCBpID0gMDtcblx0XHRcdFxuXHRcdFx0Zm9yICg7IGkgPCBzd2l0Y2hlci5sZW5ndGg7IGkrKykge1xuXHRcdFx0XHRpZiAoc3dpdGNoZXJbaV0udmFsdWUgIT09ICdhbGwnKSB7XG5cdFx0XHRcdFx0aWYgKHByZXNlbGVjdGlvbi5pbmRleE9mKHBhcnNlSW50KHN3aXRjaGVyW2ldLnZhbHVlLCAxMCkpICE9PSAtMSkge1xuXHRcdFx0XHRcdFx0JChzd2l0Y2hlcltpXSkuc3dpdGNoZXIoJ2NoZWNrZWQnLCB0cnVlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdF90cnlBY3RpdmVBbGxTd2l0Y2hlcigpO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmVuZGVycyB0aGUgcGFuZWwgYm9keS5cblx0XHQgKiBAcGFyYW0ge09iamVjdH0gY3VzdG9tZXJHcm91cHMgU2VyaWFsaXplZCBjdXN0b21lciBncm91cCBjb2xsZWN0aW9uLCBwcm92aWRlZCBieSBhamF4IHJlcXVlc3QuXG5cdFx0ICogQHJldHVybnMge2pRdWVyeX0gUGFuZWwgYm9keSBodG1sLlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0Y29uc3QgX3JlbmRlckJvZHkgPSBjdXN0b21lckdyb3VwcyA9PiB7XG5cdFx0XHRjb25zdCAkZ3hDb250YWluZXIgPSAkKCc8ZGl2Lz4nLCB7XG5cdFx0XHRcdCdjbGFzcyc6ICdneC1jb250YWluZXInXG5cdFx0XHR9KTtcblx0XHRcdGNvbnN0ICRmaWVsZFNldCA9ICQoJzxmaWVsZHNldC8+Jyk7XG5cdFx0XHRcblx0XHRcdCRhbGxTd2l0Y2hlciA9IF9yZW5kZXJGb3JtR3JvdXAoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0xBQkxFX0dST1VQQ0hFQ0tfQUxMJywgJ2NvbnRlbnRfbWFuYWdlcicpLCAnYWxsJyk7XG5cdFx0XHQkZmllbGRTZXQuYXBwZW5kKCRhbGxTd2l0Y2hlcik7XG5cdFx0XHRcblx0XHRcdGxldCBpID0gMDtcblx0XHRcdGZvciAoOyBpIDwgY3VzdG9tZXJHcm91cHMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0JGZpZWxkU2V0LmFwcGVuZChfcmVuZGVyRm9ybUdyb3VwKGN1c3RvbWVyR3JvdXBzW2ldLm5hbWVzW2pzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpXG5cdFx0XHRcdFx0LnRvVXBwZXJDYXNlKCldLCBjdXN0b21lckdyb3Vwc1tpXS5pZCkpO1xuXHRcdFx0fVxuXHRcdFx0JGd4Q29udGFpbmVyLmFwcGVuZCgkZmllbGRTZXQpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gJGd4Q29udGFpbmVyO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmVuZGVycyB0aGUgZm9ybSBncm91cCBlbGVtZW50cyBvZiB0aGUgZ3JvdXAgY2hlY2suXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gbGFiZWwgTGFiZWwgbmFtZS5cblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWUgU3dpdGNoZXJzIHZhbHVlIGF0dHJpYnV0ZS5cblx0XHQgKiBAcmV0dXJucyB7alF1ZXJ5fSBGb3JtIGdyb3VwIGh0bWwuXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRjb25zdCBfcmVuZGVyRm9ybUdyb3VwID0gKGxhYmVsLCB2YWx1ZSkgPT4ge1xuXHRcdFx0Y29uc3QgJGZvcm1Hcm91cCA9ICQoJzxkaXYvPicsIHsnY2xhc3MnOiAnZm9ybS1ncm91cCd9KTtcblx0XHRcdGNvbnN0ICRsYWJlbCA9ICQoJzxsYWJlbC8+Jywge1xuXHRcdFx0XHQnZm9yJzogJ2N1c3RvbWVyLWdyb3Vwcy0nICsgbGFiZWwudG9Mb3dlckNhc2UoKSxcblx0XHRcdFx0J2NsYXNzJzogJ2NvbC1tZC00Jyxcblx0XHRcdFx0J3RleHQnOiBsYWJlbFxuXHRcdFx0fSk7XG5cdFx0XHRjb25zdCAkaW5wdXRDb250YWluZXIgPSAkKCc8ZGl2Lz4nLCB7XG5cdFx0XHRcdCdjbGFzcyc6ICdjb2wtbWQtNidcblx0XHRcdH0pO1xuXHRcdFx0Y29uc3QgJGlucHV0ID0gJCgnPGlucHV0Lz4nLCB7XG5cdFx0XHRcdCd0eXBlJzogJ2NoZWNrYm94Jyxcblx0XHRcdFx0J2lkJzogJ2N1c3RvbWVyLWdyb3Vwcy0nICsgbGFiZWwudG9Mb3dlckNhc2UoKSxcblx0XHRcdFx0J25hbWUnOiBvcHRpb25zLm5hbWUgKyAnW10nLFxuXHRcdFx0XHQndmFsdWUnOiB2YWx1ZSxcblx0XHRcdFx0J2NoZWNrZWQnOiBvcHRpb25zLmFsbF9jaGVja2VkXG5cdFx0XHR9KTtcblx0XHRcdCRpbnB1dENvbnRhaW5lci5hcHBlbmQoJGlucHV0KTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuICRmb3JtR3JvdXAuYXBwZW5kKCRsYWJlbCkuYXBwZW5kKCRpbnB1dENvbnRhaW5lcik7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDaGVja3MgaWYgYWxsIHJlcXVpcmVkIG9wdGlvbnMgYXJlIHBhc3NlZC5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0Y29uc3QgX2NoZWNrUmVxdWlyZWRPcHRpb25zID0gKCkgPT4ge1xuXHRcdFx0bGV0IGkgPSAwO1xuXHRcdFx0Zm9yICg7IGkgPCByZXF1aXJlZE9wdGlvbnMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0aWYgKHVuZGVmaW5lZCA9PT0gb3B0aW9uc1tyZXF1aXJlZE9wdGlvbnNbaV1dKSB7XG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdSZXF1aXJlZCB3aWRnZXQgb3B0aW9uIFwiJyArIHJlcXVpcmVkT3B0aW9uc1tpXSArICdcIiBpcyBubyBzZXQhJyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNldHMgdGhlIGV2ZW50IGxpc3RlbmVyIGZvciB0aGUgZ3JvdXAgY2hlY2sgd2lkZ2V0LlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRjb25zdCBfc2V0RXZlbnRMaXN0ZW5lciA9ICgpID0+IHtcblx0XHRcdGNvbnN0IHN3aXRjaGVyID0gJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3gnKTtcblx0XHRcdFxuXHRcdFx0Ly8gY2hlY2sgYWxsIHN3aXRjaGVyIGlmIHRoZSBcImFsbFwiIHN3aXRjaGVyIGlzIGNsaWNrZWRcblx0XHRcdCRhbGxTd2l0Y2hlci5vbignY2hhbmdlJywgKCkgPT4ge1xuXHRcdFx0XHRpZiAoJGFsbFN3aXRjaGVyLmZpbmQoJ2lucHV0OmNoZWNrYm94JykucHJvcCgnY2hlY2tlZCcpKSB7XG5cdFx0XHRcdFx0X2J1bGtTd2l0Y2hlckFjdGlvbih0cnVlKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRfYnVsa1N3aXRjaGVyQWN0aW9uKGZhbHNlKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdHN3aXRjaGVyLm9uKCdjaGFuZ2UnLCBlID0+IHtcblx0XHRcdFx0aWYgKCEkYWxsU3dpdGNoZXIuZmluZCgnaW5wdXQ6Y2hlY2tib3gnKS5pcyhlLmN1cnJlbnRUYXJnZXQpKSB7XG5cdFx0XHRcdFx0Ly8gcmVtb3ZlIGNoZWNrZWQgYXR0cmlidXRlIGZyb20gXCJhbGxcIiBzd2l0Y2hlciBpZiBvbmUgaXMgZGVzZWxlY3RlZFxuXHRcdFx0XHRcdGlmICghJChlLmN1cnJlbnRUYXJnZXQpLnByb3AoJ2NoZWNrZWQnKSkge1xuXHRcdFx0XHRcdFx0JGFsbFN3aXRjaGVyLmZpbmQoJ2lucHV0OmNoZWNrYm94Jykuc3dpdGNoZXIoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdF90cnlBY3RpdmVBbGxTd2l0Y2hlcigpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFRoaXMgbWV0aG9kIGNoZWNrcyBpZiBhbGwgb3RoZXIgc3dpdGNoZXIgZmllbGRzIGluc3RlYWQgb2YgXCJhbGxcIiBhcmUgYWN0aXZlLlxuXHRcdCAqIElmIHNvLCB0aGUgXCJhbGxcIiBzd2l0Y2hlciB3aWxsIGJlIGFjdGl2YXRlZC5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0Y29uc3QgX3RyeUFjdGl2ZUFsbFN3aXRjaGVyID0gKCkgPT4ge1xuXHRcdFx0Y29uc3Qgc3dpdGNoZXIgPSAkdGhpcy5maW5kKCdpbnB1dDpjaGVja2JveCcpO1xuXHRcdFx0bGV0IGFsbENoZWNrZWQgPSB0cnVlO1xuXHRcdFx0bGV0IGkgPSAwO1xuXHRcdFx0XG5cdFx0XHRmb3IgKDsgaSA8IHN3aXRjaGVyLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRcdGlmICghJGFsbFN3aXRjaGVyLmZpbmQoJ2lucHV0OmNoZWNrYm94JykuaXMoJChzd2l0Y2hlcltpXSkpKSB7XG5cdFx0XHRcdFx0YWxsQ2hlY2tlZCA9IGFsbENoZWNrZWQgJiYgJChzd2l0Y2hlcltpXSkucHJvcCgnY2hlY2tlZCcpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRpZiAoYWxsQ2hlY2tlZCkge1xuXHRcdFx0XHQkYWxsU3dpdGNoZXIuZmluZCgnaW5wdXQ6Y2hlY2tib3gnKS5zd2l0Y2hlcignY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBCdWxrIGFjdGlvbiBmb3Igd2hldGhlciBzZWxlY3Rpbmcgb3IgZGVzZWxlY3RpbmcgYWxsIHN3aXRjaGVyIGVsZW1lbnRzLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtib29sZWFufSBjaGVja2VkIFN0YXR1cyBvZiBcImNoZWNrZWRcIiBwcm9wZXJ0eS5cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGNvbnN0IF9idWxrU3dpdGNoZXJBY3Rpb24gPSBjaGVja2VkID0+IHtcblx0XHRcdGNvbnN0IHN3aXRjaGVyID0gJHRoaXMuZmluZCgnaW5wdXQ6Y2hlY2tib3gnKTtcblx0XHRcdGxldCBpID0gMDtcblx0XHRcdGZvciAoOyBpIDwgc3dpdGNoZXIubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0aWYgKCEkYWxsU3dpdGNoZXIuZmluZCgnaW5wdXQ6Y2hlY2tib3gnKS5pcygkKHN3aXRjaGVyW2ldKSkpIHtcblx0XHRcdFx0XHQkKHN3aXRjaGVyW2ldKS5zd2l0Y2hlcignY2hlY2tlZCcsIGNoZWNrZWQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSB3aWRnZXQsIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZG9uZSA9PiB7XG5cdFx0XHRfY2hlY2tSZXF1aXJlZE9wdGlvbnMoKTtcblx0XHRcdF9yZW5kZXJHcm91cENoZWNrKCk7XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
