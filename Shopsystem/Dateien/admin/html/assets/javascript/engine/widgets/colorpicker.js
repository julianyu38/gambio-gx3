'use strict';

/* --------------------------------------------------------------
 colorpicker.js 2016-04-01
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Colorpicker Widget
 *
 * Use this widget to add a colorpicker to a specific `<div>` element.
 * 
 * jQuery Colpick Website: {@link https://github.com/mrgrain/colpick}
 *
 * ### Options
 *
 * **Color | `data-colorpicker-color` | String | Optional**
 *
 * Provide the default color for the color picker. If no value is provided, it defaults
 * to `'#ffffff'`. 
 *
 * ### Example
 *
 * ```html
 * <div data-gx-widget="colorpicker"
 *     data-colorpicker-color="#555dfa">
 *   <button class="btn picker">Select Color</button>
 *   <strong class="color-preview">Color Preview</strong>
 *   <input type="hidden" id="color-value" />
 * </div>
 * ```
 *
 * @module Admin/Widgets/colorpicker
 * @requires jQuery-Colpick-Plugin
 *
 * @todo Replace the global-colorpicker.css with the one from bower components
 * @todo The $preview selector must be set dynamically through an option.
 *
 */
gx.widgets.module('colorpicker', [jse.source + '/vendor/jquery-colpick/colpick.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Button Element Selector
  *
  * @type {object}
  */
	$button = null,


	/**
  * Preview Element Selector
  *
  * @type {object}
  */
	$preview = null,


	/**
  * Input Element Selector
  *
  * @type {object}
  */
	$input = null,


	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	defaults = {
		'color': '#ffffff' // Default color
	},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		$button = $this.find('.picker');
		$preview = $this.find('.color-preview');
		$input = $this.find('input[type="hidden"]');

		if ($input.val()) {
			options.color = $input.val();
		}

		// Enables the colorpicker.
		$button.colpick({
			'submitText': jse.core.lang.translate('ok', 'buttons'),
			'color': options.color,
			'onSubmit': function onSubmit(result) {
				var hex = '#' + $.colpick.hsbToHex(result);
				$preview.css('background-color', hex);
				$input.val(hex).trigger('change');
				$button.colpickHide();
			}
		});

		// Sets the default values in view.
		$preview.css('background-color', options.color);
		$input.val(options.color);

		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbG9ycGlja2VyLmpzIl0sIm5hbWVzIjpbImd4Iiwid2lkZ2V0cyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkYnV0dG9uIiwiJHByZXZpZXciLCIkaW5wdXQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJpbml0IiwiZG9uZSIsImZpbmQiLCJ2YWwiLCJjb2xvciIsImNvbHBpY2siLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsInJlc3VsdCIsImhleCIsImhzYlRvSGV4IiwiY3NzIiwidHJpZ2dlciIsImNvbHBpY2tIaWRlIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0NBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxhQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUiwyQ0FIRCxFQU9DLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxXQUFVLElBYlg7OztBQWVDOzs7OztBQUtBQyxZQUFXLElBcEJaOzs7QUFzQkM7Ozs7O0FBS0FDLFVBQVMsSUEzQlY7OztBQTZCQzs7Ozs7QUFLQUMsWUFBVztBQUNWLFdBQVMsU0FEQyxDQUNTO0FBRFQsRUFsQ1o7OztBQXNDQzs7Ozs7QUFLQUMsV0FBVUwsRUFBRU0sTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2Qk4sSUFBN0IsQ0EzQ1g7OztBQTZDQzs7Ozs7QUFLQUgsVUFBUyxFQWxEVjs7QUFvREE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQUEsUUFBT1ksSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QlAsWUFBVUYsTUFBTVUsSUFBTixDQUFXLFNBQVgsQ0FBVjtBQUNBUCxhQUFXSCxNQUFNVSxJQUFOLENBQVcsZ0JBQVgsQ0FBWDtBQUNBTixXQUFTSixNQUFNVSxJQUFOLENBQVcsc0JBQVgsQ0FBVDs7QUFFQSxNQUFJTixPQUFPTyxHQUFQLEVBQUosRUFBa0I7QUFDakJMLFdBQVFNLEtBQVIsR0FBZ0JSLE9BQU9PLEdBQVAsRUFBaEI7QUFDQTs7QUFFRDtBQUNBVCxVQUFRVyxPQUFSLENBQWdCO0FBQ2YsaUJBQWNoQixJQUFJaUIsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsSUFBeEIsRUFBOEIsU0FBOUIsQ0FEQztBQUVmLFlBQVNWLFFBQVFNLEtBRkY7QUFHZixlQUFZLGtCQUFTSyxNQUFULEVBQWlCO0FBQzVCLFFBQUlDLE1BQU0sTUFBTWpCLEVBQUVZLE9BQUYsQ0FBVU0sUUFBVixDQUFtQkYsTUFBbkIsQ0FBaEI7QUFDQWQsYUFBU2lCLEdBQVQsQ0FBYSxrQkFBYixFQUFpQ0YsR0FBakM7QUFDQWQsV0FBT08sR0FBUCxDQUFXTyxHQUFYLEVBQWdCRyxPQUFoQixDQUF3QixRQUF4QjtBQUNBbkIsWUFBUW9CLFdBQVI7QUFDQTtBQVJjLEdBQWhCOztBQVdBO0FBQ0FuQixXQUFTaUIsR0FBVCxDQUFhLGtCQUFiLEVBQWlDZCxRQUFRTSxLQUF6QztBQUNBUixTQUFPTyxHQUFQLENBQVdMLFFBQVFNLEtBQW5COztBQUVBSDtBQUNBLEVBMUJEOztBQTRCQTtBQUNBLFFBQU9iLE1BQVA7QUFDQSxDQXhHRiIsImZpbGUiOiJjb2xvcnBpY2tlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gY29sb3JwaWNrZXIuanMgMjAxNi0wNC0wMVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgQ29sb3JwaWNrZXIgV2lkZ2V0XG4gKlxuICogVXNlIHRoaXMgd2lkZ2V0IHRvIGFkZCBhIGNvbG9ycGlja2VyIHRvIGEgc3BlY2lmaWMgYDxkaXY+YCBlbGVtZW50LlxuICogXG4gKiBqUXVlcnkgQ29scGljayBXZWJzaXRlOiB7QGxpbmsgaHR0cHM6Ly9naXRodWIuY29tL21yZ3JhaW4vY29scGlja31cbiAqXG4gKiAjIyMgT3B0aW9uc1xuICpcbiAqICoqQ29sb3IgfCBgZGF0YS1jb2xvcnBpY2tlci1jb2xvcmAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogUHJvdmlkZSB0aGUgZGVmYXVsdCBjb2xvciBmb3IgdGhlIGNvbG9yIHBpY2tlci4gSWYgbm8gdmFsdWUgaXMgcHJvdmlkZWQsIGl0IGRlZmF1bHRzXG4gKiB0byBgJyNmZmZmZmYnYC4gXG4gKlxuICogIyMjIEV4YW1wbGVcbiAqXG4gKiBgYGBodG1sXG4gKiA8ZGl2IGRhdGEtZ3gtd2lkZ2V0PVwiY29sb3JwaWNrZXJcIlxuICogICAgIGRhdGEtY29sb3JwaWNrZXItY29sb3I9XCIjNTU1ZGZhXCI+XG4gKiAgIDxidXR0b24gY2xhc3M9XCJidG4gcGlja2VyXCI+U2VsZWN0IENvbG9yPC9idXR0b24+XG4gKiAgIDxzdHJvbmcgY2xhc3M9XCJjb2xvci1wcmV2aWV3XCI+Q29sb3IgUHJldmlldzwvc3Ryb25nPlxuICogICA8aW5wdXQgdHlwZT1cImhpZGRlblwiIGlkPVwiY29sb3ItdmFsdWVcIiAvPlxuICogPC9kaXY+XG4gKiBgYGBcbiAqXG4gKiBAbW9kdWxlIEFkbWluL1dpZGdldHMvY29sb3JwaWNrZXJcbiAqIEByZXF1aXJlcyBqUXVlcnktQ29scGljay1QbHVnaW5cbiAqXG4gKiBAdG9kbyBSZXBsYWNlIHRoZSBnbG9iYWwtY29sb3JwaWNrZXIuY3NzIHdpdGggdGhlIG9uZSBmcm9tIGJvd2VyIGNvbXBvbmVudHNcbiAqIEB0b2RvIFRoZSAkcHJldmlldyBzZWxlY3RvciBtdXN0IGJlIHNldCBkeW5hbWljYWxseSB0aHJvdWdoIGFuIG9wdGlvbi5cbiAqXG4gKi9cbmd4LndpZGdldHMubW9kdWxlKFxuXHQnY29sb3JwaWNrZXInLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktY29scGljay9jb2xwaWNrLm1pbi5qc2Bcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBXaWRnZXQgUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEJ1dHRvbiBFbGVtZW50IFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JGJ1dHRvbiA9IG51bGwsXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogUHJldmlldyBFbGVtZW50IFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHByZXZpZXcgPSBudWxsLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIElucHV0IEVsZW1lbnQgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkaW5wdXQgPSBudWxsLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9ucyBmb3IgV2lkZ2V0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdCdjb2xvcic6ICcjZmZmZmZmJyAvLyBEZWZhdWx0IGNvbG9yXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIFdpZGdldCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIHdpZGdldCwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkYnV0dG9uID0gJHRoaXMuZmluZCgnLnBpY2tlcicpO1xuXHRcdFx0JHByZXZpZXcgPSAkdGhpcy5maW5kKCcuY29sb3ItcHJldmlldycpO1xuXHRcdFx0JGlucHV0ID0gJHRoaXMuZmluZCgnaW5wdXRbdHlwZT1cImhpZGRlblwiXScpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJGlucHV0LnZhbCgpKSB7XG5cdFx0XHRcdG9wdGlvbnMuY29sb3IgPSAkaW5wdXQudmFsKCk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIEVuYWJsZXMgdGhlIGNvbG9ycGlja2VyLlxuXHRcdFx0JGJ1dHRvbi5jb2xwaWNrKHtcblx0XHRcdFx0J3N1Ym1pdFRleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnb2snLCAnYnV0dG9ucycpLFxuXHRcdFx0XHQnY29sb3InOiBvcHRpb25zLmNvbG9yLFxuXHRcdFx0XHQnb25TdWJtaXQnOiBmdW5jdGlvbihyZXN1bHQpIHtcblx0XHRcdFx0XHR2YXIgaGV4ID0gJyMnICsgJC5jb2xwaWNrLmhzYlRvSGV4KHJlc3VsdCk7XG5cdFx0XHRcdFx0JHByZXZpZXcuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJywgaGV4KTtcblx0XHRcdFx0XHQkaW5wdXQudmFsKGhleCkudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0XHRcdFx0JGJ1dHRvbi5jb2xwaWNrSGlkZSgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gU2V0cyB0aGUgZGVmYXVsdCB2YWx1ZXMgaW4gdmlldy5cblx0XHRcdCRwcmV2aWV3LmNzcygnYmFja2dyb3VuZC1jb2xvcicsIG9wdGlvbnMuY29sb3IpO1xuXHRcdFx0JGlucHV0LnZhbChvcHRpb25zLmNvbG9yKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
