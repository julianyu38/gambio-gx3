'use strict';

/* --------------------------------------------------------------
 tooltip_icon.js 2018-08-09 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Tooltip Icon Widget
 *
 * This widget will automatically transform the following markup to an icon widget.
 *
 * ### Options
 *
 * **Type | `data-tooltip_icon-type` | String | Optional**
 *
 * The type of the tooltip icon. Possible options are `'info'` and `'warning'`.
 *
 * ### Example
 *
 * ```html
 * <div class="gx-container" style="width:50px">
 *   <span data-gx-widget="tooltip_icon" data-tooltip_icon-type="warning">
 *     This is the tooltip content of the warning tooltip icon.
 *   </span>
 *   <span data-gx-widget="tooltip_icon" data-tooltip_icon-type="info">
 *     This is the tooltip content of the info tooltip icon.
 *   </span>
 * </div>
 * ```
 * **Note:** Currently, the wrapping `<div>` of the tooltip icon widget, has to have a CSS-Style
 * of `50px`.
 * 
 * @todo Make sure to set the width automatically. Currently, a style of 50px has to be applied manually.
 * @module Admin/Widgets/tooltip_icon
 */
gx.widgets.module('tooltip_icon', [jse.source + '/vendor/qtip2/jquery.qtip.css', jse.source + '/vendor/qtip2/jquery.qtip.js'],

/**  @lends module:Widgets/tooltip_icon */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		type: 'info'
	},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Gets the content and tries to add the
  * images at "Configuration > Image-Options" to the content.
  * @returns {String | HTML}
  */
	var _getContent = function _getContent() {
		// Is this from a configuration.php row?
		var $parentConfigRow = $this.parents('[data-config-key]:first');
		var isConfigRow = !!$parentConfigRow.length;

		// Try to get image and append it to the tooltip description
		if (isConfigRow) {
			var $image = $parentConfigRow.find('img:first');
			var hasImage = !!$image.length;

			if (hasImage) {
				$this.append('<br><br>');
				$this.append($image);
			}
		}

		return $this.html();
	};

	/**
  * Get the image tag element selector for the widget.
  *
  * This method will return a different image depending on the provided type option.
  */
	var _getImageElement = function _getImageElement() {
		var $icon;

		switch (options.type) {
			case 'warning':
				$icon = $('<span class="gx-container tooltip-icon pull-left ' + options.type + '">' + '<i class="fa fa-exclamation-triangle"></i>' + '</span>');
				break;
			case 'info':
				$icon = $('<span class="gx-container tooltip-icon ' + options.type + '">' + '<i class="fa fa-info-circle"></i>' + '</span>');
				break;
		}

		$icon.qtip({
			content: _getContent(),
			style: {
				classes: 'gx-container gx-qtip ' + options.type // use the type as a class for styling
			},
			position: {
				my: options.type === 'warning' ? 'bottom left' : 'left center',
				at: options.type === 'warning' ? 'top left' : 'right center',
				viewport: $(window)
			},
			hide: { // Delay the tooltip hide by 300ms so that users can interact with it.
				fixed: true,
				delay: 300
			}
		});

		return $icon;
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {

		if ($this.text().replace(/\s+/, '') !== '') {
			var $icon = _getImageElement();

			$this.text('');

			$icon.appendTo($this);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRvb2x0aXBfaWNvbi5qcyJdLCJuYW1lcyI6WyJneCIsIndpZGdldHMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJ0eXBlIiwib3B0aW9ucyIsImV4dGVuZCIsIl9nZXRDb250ZW50IiwiJHBhcmVudENvbmZpZ1JvdyIsInBhcmVudHMiLCJpc0NvbmZpZ1JvdyIsImxlbmd0aCIsIiRpbWFnZSIsImZpbmQiLCJoYXNJbWFnZSIsImFwcGVuZCIsImh0bWwiLCJfZ2V0SW1hZ2VFbGVtZW50IiwiJGljb24iLCJxdGlwIiwiY29udGVudCIsInN0eWxlIiwiY2xhc3NlcyIsInBvc2l0aW9uIiwibXkiLCJhdCIsInZpZXdwb3J0Iiwid2luZG93IiwiaGlkZSIsImZpeGVkIiwiZGVsYXkiLCJpbml0IiwiZG9uZSIsInRleHQiLCJyZXBsYWNlIiwiYXBwZW5kVG8iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE2QkFBLEdBQUdDLE9BQUgsQ0FBV0MsTUFBWCxDQUNDLGNBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLG9DQUVJRCxJQUFJQyxNQUZSLGtDQUhEOztBQVFDOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXO0FBQ1ZDLFFBQU07QUFESSxFQWJaOzs7QUFpQkM7Ozs7O0FBS0FDLFdBQVVILEVBQUVJLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkgsUUFBbkIsRUFBNkJILElBQTdCLENBdEJYOzs7QUF3QkM7Ozs7O0FBS0FILFVBQVMsRUE3QlY7O0FBK0JBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxLQUFJVSxjQUFjLFNBQWRBLFdBQWMsR0FBVztBQUM1QjtBQUNBLE1BQUlDLG1CQUFtQlAsTUFBTVEsT0FBTixDQUFjLHlCQUFkLENBQXZCO0FBQ0EsTUFBSUMsY0FBYyxDQUFDLENBQUNGLGlCQUFpQkcsTUFBckM7O0FBRUE7QUFDQSxNQUFJRCxXQUFKLEVBQWlCO0FBQ2hCLE9BQUlFLFNBQVNKLGlCQUFpQkssSUFBakIsQ0FBc0IsV0FBdEIsQ0FBYjtBQUNBLE9BQUlDLFdBQVcsQ0FBQyxDQUFDRixPQUFPRCxNQUF4Qjs7QUFFQSxPQUFJRyxRQUFKLEVBQWM7QUFDYmIsVUFBTWMsTUFBTixDQUFhLFVBQWI7QUFDQWQsVUFBTWMsTUFBTixDQUFhSCxNQUFiO0FBQ0E7QUFDRDs7QUFFRCxTQUFPWCxNQUFNZSxJQUFOLEVBQVA7QUFFQSxFQWxCRDs7QUFvQkE7Ozs7O0FBS0EsS0FBSUMsbUJBQW1CLFNBQW5CQSxnQkFBbUIsR0FBVztBQUNqQyxNQUFJQyxLQUFKOztBQUVBLFVBQVFiLFFBQVFELElBQWhCO0FBQ0MsUUFBSyxTQUFMO0FBQ0NjLFlBQVFoQixFQUFFLHNEQUFzREcsUUFBUUQsSUFBOUQsR0FBcUUsSUFBckUsR0FDVCw0Q0FEUyxHQUVULFNBRk8sQ0FBUjtBQUdBO0FBQ0QsUUFBSyxNQUFMO0FBQ0NjLFlBQVFoQixFQUFFLDRDQUE0Q0csUUFBUUQsSUFBcEQsR0FBMkQsSUFBM0QsR0FDVCxtQ0FEUyxHQUVULFNBRk8sQ0FBUjtBQUdBO0FBVkY7O0FBYUFjLFFBQU1DLElBQU4sQ0FBVztBQUNWQyxZQUFTYixhQURDO0FBRVZjLFVBQU87QUFDTkMsYUFBUywwQkFBMEJqQixRQUFRRCxJQURyQyxDQUMwQztBQUQxQyxJQUZHO0FBS1ZtQixhQUFVO0FBQ1RDLFFBQUluQixRQUFRRCxJQUFSLEtBQWlCLFNBQWpCLEdBQTZCLGFBQTdCLEdBQTZDLGFBRHhDO0FBRVRxQixRQUFJcEIsUUFBUUQsSUFBUixLQUFpQixTQUFqQixHQUE2QixVQUE3QixHQUEwQyxjQUZyQztBQUdUc0IsY0FBVXhCLEVBQUV5QixNQUFGO0FBSEQsSUFMQTtBQVVWQyxTQUFNLEVBQUU7QUFDUEMsV0FBTyxJQURGO0FBRUxDLFdBQU87QUFGRjtBQVZJLEdBQVg7O0FBZ0JBLFNBQU9aLEtBQVA7QUFDQSxFQWpDRDs7QUFtQ0E7QUFDQTtBQUNBOztBQUVBckIsUUFBT2tDLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7O0FBRTVCLE1BQUkvQixNQUFNZ0MsSUFBTixHQUFhQyxPQUFiLENBQXFCLEtBQXJCLEVBQTRCLEVBQTVCLE1BQW9DLEVBQXhDLEVBQTRDO0FBQzNDLE9BQUloQixRQUFRRCxrQkFBWjs7QUFFQWhCLFNBQU1nQyxJQUFOLENBQVcsRUFBWDs7QUFFQWYsU0FBTWlCLFFBQU4sQ0FBZWxDLEtBQWY7QUFDQTs7QUFFRCtCO0FBQ0EsRUFYRDs7QUFhQSxRQUFPbkMsTUFBUDtBQUNBLENBeElGIiwiZmlsZSI6InRvb2x0aXBfaWNvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gdG9vbHRpcF9pY29uLmpzIDIwMTgtMDgtMDkgZ21cbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE4IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIFRvb2x0aXAgSWNvbiBXaWRnZXRcbiAqXG4gKiBUaGlzIHdpZGdldCB3aWxsIGF1dG9tYXRpY2FsbHkgdHJhbnNmb3JtIHRoZSBmb2xsb3dpbmcgbWFya3VwIHRvIGFuIGljb24gd2lkZ2V0LlxuICpcbiAqICMjIyBPcHRpb25zXG4gKlxuICogKipUeXBlIHwgYGRhdGEtdG9vbHRpcF9pY29uLXR5cGVgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICpcbiAqIFRoZSB0eXBlIG9mIHRoZSB0b29sdGlwIGljb24uIFBvc3NpYmxlIG9wdGlvbnMgYXJlIGAnaW5mbydgIGFuZCBgJ3dhcm5pbmcnYC5cbiAqXG4gKiAjIyMgRXhhbXBsZVxuICpcbiAqIGBgYGh0bWxcbiAqIDxkaXYgY2xhc3M9XCJneC1jb250YWluZXJcIiBzdHlsZT1cIndpZHRoOjUwcHhcIj5cbiAqICAgPHNwYW4gZGF0YS1neC13aWRnZXQ9XCJ0b29sdGlwX2ljb25cIiBkYXRhLXRvb2x0aXBfaWNvbi10eXBlPVwid2FybmluZ1wiPlxuICogICAgIFRoaXMgaXMgdGhlIHRvb2x0aXAgY29udGVudCBvZiB0aGUgd2FybmluZyB0b29sdGlwIGljb24uXG4gKiAgIDwvc3Bhbj5cbiAqICAgPHNwYW4gZGF0YS1neC13aWRnZXQ9XCJ0b29sdGlwX2ljb25cIiBkYXRhLXRvb2x0aXBfaWNvbi10eXBlPVwiaW5mb1wiPlxuICogICAgIFRoaXMgaXMgdGhlIHRvb2x0aXAgY29udGVudCBvZiB0aGUgaW5mbyB0b29sdGlwIGljb24uXG4gKiAgIDwvc3Bhbj5cbiAqIDwvZGl2PlxuICogYGBgXG4gKiAqKk5vdGU6KiogQ3VycmVudGx5LCB0aGUgd3JhcHBpbmcgYDxkaXY+YCBvZiB0aGUgdG9vbHRpcCBpY29uIHdpZGdldCwgaGFzIHRvIGhhdmUgYSBDU1MtU3R5bGVcbiAqIG9mIGA1MHB4YC5cbiAqIFxuICogQHRvZG8gTWFrZSBzdXJlIHRvIHNldCB0aGUgd2lkdGggYXV0b21hdGljYWxseS4gQ3VycmVudGx5LCBhIHN0eWxlIG9mIDUwcHggaGFzIHRvIGJlIGFwcGxpZWQgbWFudWFsbHkuXG4gKiBAbW9kdWxlIEFkbWluL1dpZGdldHMvdG9vbHRpcF9pY29uXG4gKi9cbmd4LndpZGdldHMubW9kdWxlKFxuXHQndG9vbHRpcF9pY29uJyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvcXRpcDIvanF1ZXJ5LnF0aXAuY3NzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvcXRpcDIvanF1ZXJ5LnF0aXAuanNgXG5cdF0sXG5cdFxuXHQvKiogIEBsZW5kcyBtb2R1bGU6V2lkZ2V0cy90b29sdGlwX2ljb24gKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdHR5cGU6ICdpbmZvJ1xuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBNRVRIT0RTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0cyB0aGUgY29udGVudCBhbmQgdHJpZXMgdG8gYWRkIHRoZVxuXHRcdCAqIGltYWdlcyBhdCBcIkNvbmZpZ3VyYXRpb24gPiBJbWFnZS1PcHRpb25zXCIgdG8gdGhlIGNvbnRlbnQuXG5cdFx0ICogQHJldHVybnMge1N0cmluZyB8IEhUTUx9XG5cdFx0ICovXG5cdFx0dmFyIF9nZXRDb250ZW50ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQvLyBJcyB0aGlzIGZyb20gYSBjb25maWd1cmF0aW9uLnBocCByb3c/XG5cdFx0XHR2YXIgJHBhcmVudENvbmZpZ1JvdyA9ICR0aGlzLnBhcmVudHMoJ1tkYXRhLWNvbmZpZy1rZXldOmZpcnN0Jyk7XG5cdFx0XHR2YXIgaXNDb25maWdSb3cgPSAhISRwYXJlbnRDb25maWdSb3cubGVuZ3RoO1xuXHRcdFx0XG5cdFx0XHQvLyBUcnkgdG8gZ2V0IGltYWdlIGFuZCBhcHBlbmQgaXQgdG8gdGhlIHRvb2x0aXAgZGVzY3JpcHRpb25cblx0XHRcdGlmIChpc0NvbmZpZ1Jvdykge1xuXHRcdFx0XHR2YXIgJGltYWdlID0gJHBhcmVudENvbmZpZ1Jvdy5maW5kKCdpbWc6Zmlyc3QnKTtcblx0XHRcdFx0dmFyIGhhc0ltYWdlID0gISEkaW1hZ2UubGVuZ3RoO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKGhhc0ltYWdlKSB7XG5cdFx0XHRcdFx0JHRoaXMuYXBwZW5kKCc8YnI+PGJyPicpO1xuXHRcdFx0XHRcdCR0aGlzLmFwcGVuZCgkaW1hZ2UpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdHJldHVybiAkdGhpcy5odG1sKCk7XG5cdFx0XHRcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdldCB0aGUgaW1hZ2UgdGFnIGVsZW1lbnQgc2VsZWN0b3IgZm9yIHRoZSB3aWRnZXQuXG5cdFx0ICpcblx0XHQgKiBUaGlzIG1ldGhvZCB3aWxsIHJldHVybiBhIGRpZmZlcmVudCBpbWFnZSBkZXBlbmRpbmcgb24gdGhlIHByb3ZpZGVkIHR5cGUgb3B0aW9uLlxuXHRcdCAqL1xuXHRcdHZhciBfZ2V0SW1hZ2VFbGVtZW50ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJGljb247XG5cdFx0XHRcblx0XHRcdHN3aXRjaCAob3B0aW9ucy50eXBlKSB7XG5cdFx0XHRcdGNhc2UgJ3dhcm5pbmcnOlxuXHRcdFx0XHRcdCRpY29uID0gJCgnPHNwYW4gY2xhc3M9XCJneC1jb250YWluZXIgdG9vbHRpcC1pY29uIHB1bGwtbGVmdCAnICsgb3B0aW9ucy50eXBlICsgJ1wiPicgK1xuXHRcdFx0XHRcdFx0JzxpIGNsYXNzPVwiZmEgZmEtZXhjbGFtYXRpb24tdHJpYW5nbGVcIj48L2k+JyArXG5cdFx0XHRcdFx0XHQnPC9zcGFuPicpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdpbmZvJzpcblx0XHRcdFx0XHQkaWNvbiA9ICQoJzxzcGFuIGNsYXNzPVwiZ3gtY29udGFpbmVyIHRvb2x0aXAtaWNvbiAnICsgb3B0aW9ucy50eXBlICsgJ1wiPicgK1xuXHRcdFx0XHRcdFx0JzxpIGNsYXNzPVwiZmEgZmEtaW5mby1jaXJjbGVcIj48L2k+JyArXG5cdFx0XHRcdFx0XHQnPC9zcGFuPicpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkaWNvbi5xdGlwKHtcblx0XHRcdFx0Y29udGVudDogX2dldENvbnRlbnQoKSxcblx0XHRcdFx0c3R5bGU6IHtcblx0XHRcdFx0XHRjbGFzc2VzOiAnZ3gtY29udGFpbmVyIGd4LXF0aXAgJyArIG9wdGlvbnMudHlwZSAvLyB1c2UgdGhlIHR5cGUgYXMgYSBjbGFzcyBmb3Igc3R5bGluZ1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRwb3NpdGlvbjoge1xuXHRcdFx0XHRcdG15OiBvcHRpb25zLnR5cGUgPT09ICd3YXJuaW5nJyA/ICdib3R0b20gbGVmdCcgOiAnbGVmdCBjZW50ZXInLFxuXHRcdFx0XHRcdGF0OiBvcHRpb25zLnR5cGUgPT09ICd3YXJuaW5nJyA/ICd0b3AgbGVmdCcgOiAncmlnaHQgY2VudGVyJyxcblx0XHRcdFx0XHR2aWV3cG9ydDogJCh3aW5kb3cpXG5cdFx0XHRcdH0sXG5cdFx0XHRcdGhpZGU6IHsgLy8gRGVsYXkgdGhlIHRvb2x0aXAgaGlkZSBieSAzMDBtcyBzbyB0aGF0IHVzZXJzIGNhbiBpbnRlcmFjdCB3aXRoIGl0LlxuXHRcdFx0XHRcdGZpeGVkOiB0cnVlLFxuXHRcdFx0XHRcdGRlbGF5OiAzMDBcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdHJldHVybiAkaWNvbjtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRcblx0XHRcdGlmICgkdGhpcy50ZXh0KCkucmVwbGFjZSgvXFxzKy8sICcnKSAhPT0gJycpIHtcblx0XHRcdFx0dmFyICRpY29uID0gX2dldEltYWdlRWxlbWVudCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0JHRoaXMudGV4dCgnJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkaWNvbi5hcHBlbmRUbygkdGhpcyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
