'use strict';

/* --------------------------------------------------------------
 editor.js 2017-09-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Editor Widget
 *
 * This widget will initialize instances of CKEditor or CodeMirror depending the provided data attribute of
 * each textarea, within the container the widget is bound to. Purpose of this module is to provide a common
 * wrapper of the textarea and record specific editor which means that the user will be able to set an editor
 * for a specific record and textarea and store this preference in the database.
 *
 * **Currently the available editors are "ckeditor" and "codemirror".**
 *
 * Important: Make sure that you provide the required options as described below. The module is flexible enough
 * to provide a solution for each page code base.
 *
 *
 * ### Options (Container)
 *
 * The following options are bound as data attributes to the element where the module is bound on (most of the times
 * a container that includes textarea elements).
 *
 * **Selector | `data-editor-selector` | String | Optional**
 *
 * Provide a selector for the textareas to be converted to editor instances. This option defaults to "textarea" and
 * will match all the textarea elements inside the container.
 *
 * **Event Target | `data-editor-event-target` | String | Optional**
 *
 * Provide a selector that will mark the element which will start the submit/save process of the page. If provided
 * the selected editor preference will be saved through the user configuration service with an AJAX request.
 *
 * Important: There is no default value for this option.
 *
 * **Event Type | `data-editor-event-type` | String | Optional**
 *
 * Provide a JavaScript event that will mark the submit/save process of the page. If provided an event handler
 * will be bound on the element marked by the "event-target" option and AJAX requests will save the current
 * editor preference in the user configuration table.
 *
 * Important: There is no default value for this option.
 *
 * **AutoHide | `data-editor-auto-hide` | Boolean | Optional**
 *
 * Provide "true" or "false" in order to auto hide the editors, if the textareas are not visible at the beginning.
 * Defaults value is "false"
 *
 * **AutoUpdate | `data-editor-auto-update` | Boolean | Optional**
 *
 * Indicates if the corresponding textarea of the editor should be updated automatically.
 * Default value is "true"
 *
 * **Initialization Event Type | `data-editor-init-event-type` | String | Optional**
 *
 * Provide a custom initialization event which will trigger the start of the editor conversion. By default the
 * editor instances will be created once the engine is ready 'JSENGINE_INIT_FINISHED', but there are cases where
 * a custom event is required (e.g. initialization of editor widget dynamically within a dialog).
 *
 *
 * ### Options (Textarea)
 *
 * The following options are bound as data attributes to each textarea element within the parent container.
 *
 * **Editor Identifier | `data-editor-identifier` | String | Required**
 *
 * Each child textarea element needs to have a unique identifier string which needs to apply with the following
 * naming convention the "editor-{record type}-{id}-{textarea name}-{language code}
 * (e.g. editor-products-2-short_description-de). In cases where the record ID is not set yet (new record creation),
 * it is advised that you leave the {id} placeholder and replace it later on whenever the record is generated into
 * the database (more information about this edge case in the examples below).
 *
 * **Editor Type | `data-editor-type` | String | Optional**
 *
 * This option can have one of the available editor values which will also state the selected editor upon
 * initialization. It is optional and the default value is "ckeditor".
 *
 *
 * ### Events
 *
 * The '#editor-container' element is where the widget is bound on.
 *
 * ```javascript
 * // Fires up when all textareas are ready.
 * $('#editor-container').on('editor:ready', (event, $textareas) => { ... });
 *
 * // Fires up each time a single textarea is ready.
 * $('#editor-container').on('editor:textarea_ready', (event, $textarea) => { ... });
 * ```
 *
 *
 * ### Examples
 *
 * **Simple Usage**
 *
 * Notice that this example showcases the creation of a new customer which means that the customer's ID is not known
 * yet. After its initialization, the widget will create a hidden field in the form with the
 * "editor_identifiers[textarea-identifier]" name. This hidden field will have the selected editor type as value which
 * be used by the backend callback to store the correct editor identifier value, once the customer's ID is generated
 * (record inserted). Use the "UserConfigurationService" in backend for adding the value to the database.
 *
 * ```html
 * <div data-gx-widget="editor" data-editor-event-target="#customer-form"  data-editor-event-type="submit">
 *   <form id="customer-form">
 *     <!-- Other Fields ... ->
 *     <textarea class="wysiwyg" data-editor-identifier="editor-customers-{id}-notes-de"></textarea>
 *   </form>
 * </div>
 * ```
 *
 * @module Admin/Widgets/editor
 * @requires CKEditor, CodeMirror
 */
gx.widgets.module('editor', [jse.source + '/vendor/codemirror/codemirror.min.css', jse.source + '/vendor/codemirror/codemirror.min.js', gx.source + '/libs/editor_instances', gx.source + '/libs/editor_values', gx.source + '/widgets/quickselect', 'user_configuration_service'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		selector: 'textarea',
		autoHide: 'false',
		initEventType: 'JSENGINE_INIT_FINISHED',
		autoUpdate: 'true'
	};

	/**
  * Final Options
  *
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Editor Instances
  *
  * Identifier -> instance mapping
  *
  * @type {Object}
  */
	var editors = {};

	/**
  * Available Editor Types
  *
  * @type {String[]}
  */
	var editorTypes = ['ckeditor', 'codemirror'];

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Add Editor Switch Button
  *
  * This method will add the editor switch button and bind the click event handler.
  *
  * @param {jQuery} $textarea Textarea selector to be modified.
  */
	function _addEditorSwitchButton($textarea) {
		var start = 0;
		if ($textarea.data('editorType') === 'codemirror') {
			start = 1;
		}

		$textarea.wrap('<div class="editor-wrapper" />').parent().prepend('<div data-gx-widget="quickselect" data-quickselect-align="right" data-quickselect-start="' + start + ('">\n\t\t\t\t\t\t\t<div class="quickselect-headline-wrapper">\n\t\t\t\t\t\t\t\t<a class="editor-switch editor-switch-html" href="#html">\n\t\t\t\t\t\t\t\t\t' + jse.core.lang.translate('BUTTON_SWITCH_EDITOR_TEXT', 'admin_buttons') + '\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t<a class="editor-switch editor-switch-text" href="#text">\n\t\t\t\t\t\t\t\t\t' + jse.core.lang.translate('BUTTON_SWITCH_EDITOR_HTML', 'admin_buttons') + '\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>')).find('.editor-switch').on('click', _onSwitchButtonClick);

		if (!$textarea.is(':visible') && options.autoHide === 'true') {
			$textarea.parent().hide();
		}

		gx.widgets.init($textarea.parent());
	}

	/**
  * Add a hidden editor type field.
  *
  * This field will contain the type of the current editor and can be used by submit callbacks whenever the
  * record ID is not known yet and the user configuration entry is generated by the server.
  *
  * @param {jQuery} $textarea Textarea selector to be modified.
  */
	function _addEditorHiddenField($textarea) {
		$textarea.parent().append('\n\t\t\t\t\t<input type="hidden" \n\t\t\t\t\t\tname="editor_identifiers[' + $textarea.data('editorIdentifier') + ']" \n\t\t\t\t\t\tvalue="' + ($textarea.data('editorType') || 'ckeditor') + '" />\n\t\t\t\t');
	}

	/**
  * Create Editor Instance
  *
  * This method will use the "editor" library to create the appropriate editor instance, depending the textarea
  * type attribute.
  *
  * @param {jQuery} $textarea Textarea selector to be modified.
  */
	function _createEditorInstance($textarea) {
		var type = $textarea.data('editorType') || 'ckeditor';
		var identifier = $textarea.data('editorIdentifier');

		editors[identifier] = jse.libs.editor_instances.create($textarea, type);
	}

	/**
  * On Switch Button Click Event Handler
  *
  * This method will use the "editor" library to change the current editor type and update the hidden input
  * field and data attributes of the textarea. It will try to set the next available editor type.
  */
	function _onSwitchButtonClick() {
		var $switchButton = $(this);
		var $textarea = $switchButton.parents('.editor-wrapper').find('textarea');
		var identifier = $textarea.data('editorIdentifier');
		var currentType = $textarea.data('editorType');
		var newType = $switchButton.hasClass('editor-switch-text') ? editorTypes[1] : editorTypes[0];

		$textarea.siblings('[name="editor_identifiers[' + identifier + ']"]').val(newType);
		$textarea.data('editorType', newType);

		editors[identifier] = jse.libs.editor_instances.switch($textarea, currentType, newType);
		_bindAutoUpdate($textarea);
		_updateTextArea($textarea);
	}

	/**
  * On Page Submit Handler
  *
  * If the event target and type are provided this method will be triggered to save the user configuration
  * values in the database with AJAX requests.
  */
	function _onPageSubmit() {
		for (var identifier in editors) {
			jse.libs.user_configuration_service.set({
				data: {
					userId: 0,
					configurationKey: identifier,
					configurationValue: editors[identifier].type
				}
			});
		}
	}

	/**
  * Bind Auto Update
  *
  * Binds an event handler to an editor instance to automatically update the
  * corresponding textarea.
  *
  * @param {jQuery} $textarea Textarea the auto update should be bound to
  */
	function _bindAutoUpdate($textarea) {
		if (options.autoUpdate === 'false') {
			return;
		}

		var instance = editors[$textarea.data('editorIdentifier')];

		instance.on('change', function () {
			return _updateTextArea($textarea);
		});
	}

	/**
  * Update Text Area Value
  *
  * Transfers the value of the editor instance of the given textarea to its corresponding textarea.
  *
  * @param {jQuery} $textarea The textarea to be updated.
  */
	function _updateTextArea($textarea) {
		var editorType = $textarea.data('editorType');
		var instance = editors[$textarea.data('editorIdentifier')];

		switch (editorType) {
			case 'ckeditor':
				instance.updateElement();
				break;

			case 'codemirror':
				$textarea.val(jse.libs.editor_values.getValue($textarea));
				break;

			default:
				throw new Error('Editor type not recognized.', editorType);
		}

		$textarea.trigger('change');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(document).on('JSENGINE_INIT_FINISHED', function () {
			var dependencies = [jse.source + '/vendor/codemirror/css.min.js', jse.source + '/vendor/codemirror/htmlmixed.min.js', jse.source + '/vendor/codemirror/javascript.min.js', jse.source + '/vendor/codemirror/xml.min.js'];

			jse.core.module_loader.require(dependencies);
		});

		// Initialize the editors after a specific event in order to make sure that other modules will be
		// already initialized and nothing else will change the markup.
		$(window).on(options.initEventType, function () {
			var $textareas = $this.find(options.selector);

			$textareas.each(function (index, textarea) {
				var $textarea = $(textarea);

				if (editorTypes.indexOf($textarea.data('editorType')) === -1) {
					$textarea.data('editorType', editorTypes[0]);
				}

				_addEditorSwitchButton($textarea);
				_addEditorHiddenField($textarea);
				_createEditorInstance($textarea);
				_bindAutoUpdate($textarea);

				$this.trigger('editor:textarea_ready', [$textarea]);
			});

			$this.trigger('editor:ready', [$textareas]);
		});

		// If the event target and type options are available, bind the page submit handler. 
		if (options.eventTarget !== undefined && options.eventType !== undefined) {
			$(options.eventTarget).on(options.eventType, _onPageSubmit);
		}

		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVkaXRvci5qcyJdLCJuYW1lcyI6WyJneCIsIndpZGdldHMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJzZWxlY3RvciIsImF1dG9IaWRlIiwiaW5pdEV2ZW50VHlwZSIsImF1dG9VcGRhdGUiLCJvcHRpb25zIiwiZXh0ZW5kIiwiZWRpdG9ycyIsImVkaXRvclR5cGVzIiwiX2FkZEVkaXRvclN3aXRjaEJ1dHRvbiIsIiR0ZXh0YXJlYSIsInN0YXJ0Iiwid3JhcCIsInBhcmVudCIsInByZXBlbmQiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsImZpbmQiLCJvbiIsIl9vblN3aXRjaEJ1dHRvbkNsaWNrIiwiaXMiLCJoaWRlIiwiaW5pdCIsIl9hZGRFZGl0b3JIaWRkZW5GaWVsZCIsImFwcGVuZCIsIl9jcmVhdGVFZGl0b3JJbnN0YW5jZSIsInR5cGUiLCJpZGVudGlmaWVyIiwibGlicyIsImVkaXRvcl9pbnN0YW5jZXMiLCJjcmVhdGUiLCIkc3dpdGNoQnV0dG9uIiwicGFyZW50cyIsImN1cnJlbnRUeXBlIiwibmV3VHlwZSIsImhhc0NsYXNzIiwic2libGluZ3MiLCJ2YWwiLCJzd2l0Y2giLCJfYmluZEF1dG9VcGRhdGUiLCJfdXBkYXRlVGV4dEFyZWEiLCJfb25QYWdlU3VibWl0IiwidXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UiLCJzZXQiLCJ1c2VySWQiLCJjb25maWd1cmF0aW9uS2V5IiwiY29uZmlndXJhdGlvblZhbHVlIiwiaW5zdGFuY2UiLCJlZGl0b3JUeXBlIiwidXBkYXRlRWxlbWVudCIsImVkaXRvcl92YWx1ZXMiLCJnZXRWYWx1ZSIsIkVycm9yIiwidHJpZ2dlciIsImRvbmUiLCJkb2N1bWVudCIsImRlcGVuZGVuY2llcyIsIm1vZHVsZV9sb2FkZXIiLCJyZXF1aXJlIiwid2luZG93IiwiJHRleHRhcmVhcyIsImVhY2giLCJpbmRleCIsInRleHRhcmVhIiwiaW5kZXhPZiIsImV2ZW50VGFyZ2V0IiwidW5kZWZpbmVkIiwiZXZlbnRUeXBlIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE2R0FBLEdBQUdDLE9BQUgsQ0FBV0MsTUFBWCxDQUNDLFFBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLDRDQUVJRCxJQUFJQyxNQUZSLDJDQUdJSixHQUFHSSxNQUhQLDZCQUlJSixHQUFHSSxNQUpQLDBCQUtJSixHQUFHSSxNQUxQLDJCQU1DLDRCQU5ELENBSEQsRUFZQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCQyxZQUFVLFVBRE07QUFFaEJDLFlBQVUsT0FGTTtBQUdoQkMsaUJBQWUsd0JBSEM7QUFJaEJDLGNBQVk7QUFKSSxFQUFqQjs7QUFPQTs7Ozs7QUFLQSxLQUFNQyxVQUFVTixFQUFFTyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJOLFFBQW5CLEVBQTZCSCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNSCxTQUFTLEVBQWY7O0FBRUE7Ozs7Ozs7QUFPQSxLQUFNYSxVQUFVLEVBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1DLGNBQWMsQ0FBQyxVQUFELEVBQWEsWUFBYixDQUFwQjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUFPQSxVQUFTQyxzQkFBVCxDQUFnQ0MsU0FBaEMsRUFBMkM7QUFDMUMsTUFBSUMsUUFBUSxDQUFaO0FBQ0EsTUFBSUQsVUFBVWIsSUFBVixDQUFlLFlBQWYsTUFBaUMsWUFBckMsRUFBbUQ7QUFDbERjLFdBQVEsQ0FBUjtBQUNBOztBQUVERCxZQUNFRSxJQURGLENBQ08sZ0NBRFAsRUFFRUMsTUFGRixHQUdFQyxPQUhGLENBR1UsOEZBQ05ILEtBRE0sb0tBSUZoQixJQUFJb0IsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsMkJBQXhCLEVBQXFELGVBQXJELENBSkUsNkhBT0Z0QixJQUFJb0IsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsMkJBQXhCLEVBQXFELGVBQXJELENBUEUsc0VBSFYsRUFjRUMsSUFkRixDQWNPLGdCQWRQLEVBZUVDLEVBZkYsQ0FlSyxPQWZMLEVBZWNDLG9CQWZkOztBQWlCQSxNQUFJLENBQUNWLFVBQVVXLEVBQVYsQ0FBYSxVQUFiLENBQUQsSUFBNkJoQixRQUFRSCxRQUFSLEtBQXFCLE1BQXRELEVBQThEO0FBQzdEUSxhQUFVRyxNQUFWLEdBQW1CUyxJQUFuQjtBQUNBOztBQUVEOUIsS0FBR0MsT0FBSCxDQUFXOEIsSUFBWCxDQUFnQmIsVUFBVUcsTUFBVixFQUFoQjtBQUNBOztBQUVEOzs7Ozs7OztBQVFBLFVBQVNXLHFCQUFULENBQStCZCxTQUEvQixFQUEwQztBQUN6Q0EsWUFDRUcsTUFERixHQUVFWSxNQUZGLDhFQUk4QmYsVUFBVWIsSUFBVixDQUFlLGtCQUFmLENBSjlCLGlDQUtZYSxVQUFVYixJQUFWLENBQWUsWUFBZixLQUFnQyxVQUw1QztBQU9BOztBQUVEOzs7Ozs7OztBQVFBLFVBQVM2QixxQkFBVCxDQUErQmhCLFNBQS9CLEVBQTBDO0FBQ3pDLE1BQU1pQixPQUFPakIsVUFBVWIsSUFBVixDQUFlLFlBQWYsS0FBZ0MsVUFBN0M7QUFDQSxNQUFNK0IsYUFBYWxCLFVBQVViLElBQVYsQ0FBZSxrQkFBZixDQUFuQjs7QUFFQVUsVUFBUXFCLFVBQVIsSUFBc0JqQyxJQUFJa0MsSUFBSixDQUFTQyxnQkFBVCxDQUEwQkMsTUFBMUIsQ0FBaUNyQixTQUFqQyxFQUE0Q2lCLElBQTVDLENBQXRCO0FBQ0E7O0FBRUQ7Ozs7OztBQU1BLFVBQVNQLG9CQUFULEdBQWdDO0FBQy9CLE1BQU1ZLGdCQUFnQmpDLEVBQUUsSUFBRixDQUF0QjtBQUNBLE1BQU1XLFlBQVlzQixjQUFjQyxPQUFkLENBQXNCLGlCQUF0QixFQUF5Q2YsSUFBekMsQ0FBOEMsVUFBOUMsQ0FBbEI7QUFDQSxNQUFNVSxhQUFhbEIsVUFBVWIsSUFBVixDQUFlLGtCQUFmLENBQW5CO0FBQ0EsTUFBTXFDLGNBQWN4QixVQUFVYixJQUFWLENBQWUsWUFBZixDQUFwQjtBQUNBLE1BQU1zQyxVQUFVSCxjQUFjSSxRQUFkLENBQXVCLG9CQUF2QixJQUErQzVCLFlBQVksQ0FBWixDQUEvQyxHQUFnRUEsWUFBWSxDQUFaLENBQWhGOztBQUVBRSxZQUFVMkIsUUFBVixnQ0FBZ0RULFVBQWhELFVBQWlFVSxHQUFqRSxDQUFxRUgsT0FBckU7QUFDQXpCLFlBQVViLElBQVYsQ0FBZSxZQUFmLEVBQTZCc0MsT0FBN0I7O0FBRUE1QixVQUFRcUIsVUFBUixJQUFzQmpDLElBQUlrQyxJQUFKLENBQVNDLGdCQUFULENBQTBCUyxNQUExQixDQUFpQzdCLFNBQWpDLEVBQTRDd0IsV0FBNUMsRUFBeURDLE9BQXpELENBQXRCO0FBQ0FLLGtCQUFnQjlCLFNBQWhCO0FBQ0ErQixrQkFBZ0IvQixTQUFoQjtBQUNBOztBQUVEOzs7Ozs7QUFNQSxVQUFTZ0MsYUFBVCxHQUF5QjtBQUN4QixPQUFLLElBQUlkLFVBQVQsSUFBdUJyQixPQUF2QixFQUFnQztBQUMvQlosT0FBSWtDLElBQUosQ0FBU2MsMEJBQVQsQ0FBb0NDLEdBQXBDLENBQXdDO0FBQ3ZDL0MsVUFBTTtBQUNMZ0QsYUFBUSxDQURIO0FBRUxDLHVCQUFrQmxCLFVBRmI7QUFHTG1CLHlCQUFvQnhDLFFBQVFxQixVQUFSLEVBQW9CRDtBQUhuQztBQURpQyxJQUF4QztBQU9BO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsVUFBU2EsZUFBVCxDQUF5QjlCLFNBQXpCLEVBQW9DO0FBQ25DLE1BQUlMLFFBQVFELFVBQVIsS0FBdUIsT0FBM0IsRUFBb0M7QUFDbkM7QUFDQTs7QUFFRCxNQUFNNEMsV0FBV3pDLFFBQVFHLFVBQVViLElBQVYsQ0FBZSxrQkFBZixDQUFSLENBQWpCOztBQUVBbUQsV0FBUzdCLEVBQVQsQ0FBWSxRQUFaLEVBQXNCO0FBQUEsVUFBTXNCLGdCQUFnQi9CLFNBQWhCLENBQU47QUFBQSxHQUF0QjtBQUNBOztBQUVEOzs7Ozs7O0FBT0EsVUFBUytCLGVBQVQsQ0FBeUIvQixTQUF6QixFQUFvQztBQUNuQyxNQUFNdUMsYUFBYXZDLFVBQVViLElBQVYsQ0FBZSxZQUFmLENBQW5CO0FBQ0EsTUFBTW1ELFdBQVd6QyxRQUFRRyxVQUFVYixJQUFWLENBQWUsa0JBQWYsQ0FBUixDQUFqQjs7QUFFQSxVQUFRb0QsVUFBUjtBQUNDLFFBQUssVUFBTDtBQUNDRCxhQUFTRSxhQUFUO0FBQ0E7O0FBRUQsUUFBSyxZQUFMO0FBQ0N4QyxjQUFVNEIsR0FBVixDQUFjM0MsSUFBSWtDLElBQUosQ0FBU3NCLGFBQVQsQ0FBdUJDLFFBQXZCLENBQWdDMUMsU0FBaEMsQ0FBZDtBQUNBOztBQUVEO0FBQ0MsVUFBTSxJQUFJMkMsS0FBSixDQUFVLDZCQUFWLEVBQXlDSixVQUF6QyxDQUFOO0FBVkY7O0FBYUF2QyxZQUFVNEMsT0FBVixDQUFrQixRQUFsQjtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTVELFFBQU82QixJQUFQLEdBQWMsVUFBU2dDLElBQVQsRUFBZTtBQUM1QnhELElBQUV5RCxRQUFGLEVBQVlyQyxFQUFaLENBQWUsd0JBQWYsRUFBeUMsWUFBTTtBQUM5QyxPQUFNc0MsZUFBZSxDQUNqQjlELElBQUlDLE1BRGEsb0NBRWpCRCxJQUFJQyxNQUZhLDBDQUdqQkQsSUFBSUMsTUFIYSwyQ0FJakJELElBQUlDLE1BSmEsbUNBQXJCOztBQU9BRCxPQUFJb0IsSUFBSixDQUFTMkMsYUFBVCxDQUF1QkMsT0FBdkIsQ0FBK0JGLFlBQS9CO0FBQ0EsR0FURDs7QUFXQTtBQUNBO0FBQ0ExRCxJQUFFNkQsTUFBRixFQUFVekMsRUFBVixDQUFhZCxRQUFRRixhQUFyQixFQUFvQyxZQUFNO0FBQ3pDLE9BQU0wRCxhQUFhL0QsTUFBTW9CLElBQU4sQ0FBV2IsUUFBUUosUUFBbkIsQ0FBbkI7O0FBRUE0RCxjQUFXQyxJQUFYLENBQWdCLFVBQUNDLEtBQUQsRUFBUUMsUUFBUixFQUFxQjtBQUNwQyxRQUFNdEQsWUFBWVgsRUFBRWlFLFFBQUYsQ0FBbEI7O0FBRUEsUUFBSXhELFlBQVl5RCxPQUFaLENBQW9CdkQsVUFBVWIsSUFBVixDQUFlLFlBQWYsQ0FBcEIsTUFBc0QsQ0FBQyxDQUEzRCxFQUE4RDtBQUM3RGEsZUFBVWIsSUFBVixDQUFlLFlBQWYsRUFBNkJXLFlBQVksQ0FBWixDQUE3QjtBQUNBOztBQUVEQywyQkFBdUJDLFNBQXZCO0FBQ0FjLDBCQUFzQmQsU0FBdEI7QUFDQWdCLDBCQUFzQmhCLFNBQXRCO0FBQ0E4QixvQkFBZ0I5QixTQUFoQjs7QUFFQVosVUFBTXdELE9BQU4sQ0FBYyx1QkFBZCxFQUF1QyxDQUFDNUMsU0FBRCxDQUF2QztBQUNBLElBYkQ7O0FBZUFaLFNBQU13RCxPQUFOLENBQWMsY0FBZCxFQUE4QixDQUFDTyxVQUFELENBQTlCO0FBQ0EsR0FuQkQ7O0FBcUJBO0FBQ0EsTUFBSXhELFFBQVE2RCxXQUFSLEtBQXdCQyxTQUF4QixJQUFxQzlELFFBQVErRCxTQUFSLEtBQXNCRCxTQUEvRCxFQUEwRTtBQUN6RXBFLEtBQUVNLFFBQVE2RCxXQUFWLEVBQXVCL0MsRUFBdkIsQ0FBMEJkLFFBQVErRCxTQUFsQyxFQUE2QzFCLGFBQTdDO0FBQ0E7O0FBRURhO0FBQ0EsRUF6Q0Q7O0FBMkNBO0FBQ0EsUUFBTzdELE1BQVA7QUFDQSxDQXBSRiIsImZpbGUiOiJlZGl0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGVkaXRvci5qcyAyMDE3LTA5LTA1XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBFZGl0b3IgV2lkZ2V0XG4gKlxuICogVGhpcyB3aWRnZXQgd2lsbCBpbml0aWFsaXplIGluc3RhbmNlcyBvZiBDS0VkaXRvciBvciBDb2RlTWlycm9yIGRlcGVuZGluZyB0aGUgcHJvdmlkZWQgZGF0YSBhdHRyaWJ1dGUgb2ZcbiAqIGVhY2ggdGV4dGFyZWEsIHdpdGhpbiB0aGUgY29udGFpbmVyIHRoZSB3aWRnZXQgaXMgYm91bmQgdG8uIFB1cnBvc2Ugb2YgdGhpcyBtb2R1bGUgaXMgdG8gcHJvdmlkZSBhIGNvbW1vblxuICogd3JhcHBlciBvZiB0aGUgdGV4dGFyZWEgYW5kIHJlY29yZCBzcGVjaWZpYyBlZGl0b3Igd2hpY2ggbWVhbnMgdGhhdCB0aGUgdXNlciB3aWxsIGJlIGFibGUgdG8gc2V0IGFuIGVkaXRvclxuICogZm9yIGEgc3BlY2lmaWMgcmVjb3JkIGFuZCB0ZXh0YXJlYSBhbmQgc3RvcmUgdGhpcyBwcmVmZXJlbmNlIGluIHRoZSBkYXRhYmFzZS5cbiAqXG4gKiAqKkN1cnJlbnRseSB0aGUgYXZhaWxhYmxlIGVkaXRvcnMgYXJlIFwiY2tlZGl0b3JcIiBhbmQgXCJjb2RlbWlycm9yXCIuKipcbiAqXG4gKiBJbXBvcnRhbnQ6IE1ha2Ugc3VyZSB0aGF0IHlvdSBwcm92aWRlIHRoZSByZXF1aXJlZCBvcHRpb25zIGFzIGRlc2NyaWJlZCBiZWxvdy4gVGhlIG1vZHVsZSBpcyBmbGV4aWJsZSBlbm91Z2hcbiAqIHRvIHByb3ZpZGUgYSBzb2x1dGlvbiBmb3IgZWFjaCBwYWdlIGNvZGUgYmFzZS5cbiAqXG4gKlxuICogIyMjIE9wdGlvbnMgKENvbnRhaW5lcilcbiAqXG4gKiBUaGUgZm9sbG93aW5nIG9wdGlvbnMgYXJlIGJvdW5kIGFzIGRhdGEgYXR0cmlidXRlcyB0byB0aGUgZWxlbWVudCB3aGVyZSB0aGUgbW9kdWxlIGlzIGJvdW5kIG9uIChtb3N0IG9mIHRoZSB0aW1lc1xuICogYSBjb250YWluZXIgdGhhdCBpbmNsdWRlcyB0ZXh0YXJlYSBlbGVtZW50cykuXG4gKlxuICogKipTZWxlY3RvciB8IGBkYXRhLWVkaXRvci1zZWxlY3RvcmAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogUHJvdmlkZSBhIHNlbGVjdG9yIGZvciB0aGUgdGV4dGFyZWFzIHRvIGJlIGNvbnZlcnRlZCB0byBlZGl0b3IgaW5zdGFuY2VzLiBUaGlzIG9wdGlvbiBkZWZhdWx0cyB0byBcInRleHRhcmVhXCIgYW5kXG4gKiB3aWxsIG1hdGNoIGFsbCB0aGUgdGV4dGFyZWEgZWxlbWVudHMgaW5zaWRlIHRoZSBjb250YWluZXIuXG4gKlxuICogKipFdmVudCBUYXJnZXQgfCBgZGF0YS1lZGl0b3ItZXZlbnQtdGFyZ2V0YCB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqXG4gKiBQcm92aWRlIGEgc2VsZWN0b3IgdGhhdCB3aWxsIG1hcmsgdGhlIGVsZW1lbnQgd2hpY2ggd2lsbCBzdGFydCB0aGUgc3VibWl0L3NhdmUgcHJvY2VzcyBvZiB0aGUgcGFnZS4gSWYgcHJvdmlkZWRcbiAqIHRoZSBzZWxlY3RlZCBlZGl0b3IgcHJlZmVyZW5jZSB3aWxsIGJlIHNhdmVkIHRocm91Z2ggdGhlIHVzZXIgY29uZmlndXJhdGlvbiBzZXJ2aWNlIHdpdGggYW4gQUpBWCByZXF1ZXN0LlxuICpcbiAqIEltcG9ydGFudDogVGhlcmUgaXMgbm8gZGVmYXVsdCB2YWx1ZSBmb3IgdGhpcyBvcHRpb24uXG4gKlxuICogKipFdmVudCBUeXBlIHwgYGRhdGEtZWRpdG9yLWV2ZW50LXR5cGVgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICpcbiAqIFByb3ZpZGUgYSBKYXZhU2NyaXB0IGV2ZW50IHRoYXQgd2lsbCBtYXJrIHRoZSBzdWJtaXQvc2F2ZSBwcm9jZXNzIG9mIHRoZSBwYWdlLiBJZiBwcm92aWRlZCBhbiBldmVudCBoYW5kbGVyXG4gKiB3aWxsIGJlIGJvdW5kIG9uIHRoZSBlbGVtZW50IG1hcmtlZCBieSB0aGUgXCJldmVudC10YXJnZXRcIiBvcHRpb24gYW5kIEFKQVggcmVxdWVzdHMgd2lsbCBzYXZlIHRoZSBjdXJyZW50XG4gKiBlZGl0b3IgcHJlZmVyZW5jZSBpbiB0aGUgdXNlciBjb25maWd1cmF0aW9uIHRhYmxlLlxuICpcbiAqIEltcG9ydGFudDogVGhlcmUgaXMgbm8gZGVmYXVsdCB2YWx1ZSBmb3IgdGhpcyBvcHRpb24uXG4gKlxuICogKipBdXRvSGlkZSB8IGBkYXRhLWVkaXRvci1hdXRvLWhpZGVgIHwgQm9vbGVhbiB8IE9wdGlvbmFsKipcbiAqXG4gKiBQcm92aWRlIFwidHJ1ZVwiIG9yIFwiZmFsc2VcIiBpbiBvcmRlciB0byBhdXRvIGhpZGUgdGhlIGVkaXRvcnMsIGlmIHRoZSB0ZXh0YXJlYXMgYXJlIG5vdCB2aXNpYmxlIGF0IHRoZSBiZWdpbm5pbmcuXG4gKiBEZWZhdWx0cyB2YWx1ZSBpcyBcImZhbHNlXCJcbiAqXG4gKiAqKkF1dG9VcGRhdGUgfCBgZGF0YS1lZGl0b3ItYXV0by11cGRhdGVgIHwgQm9vbGVhbiB8IE9wdGlvbmFsKipcbiAqXG4gKiBJbmRpY2F0ZXMgaWYgdGhlIGNvcnJlc3BvbmRpbmcgdGV4dGFyZWEgb2YgdGhlIGVkaXRvciBzaG91bGQgYmUgdXBkYXRlZCBhdXRvbWF0aWNhbGx5LlxuICogRGVmYXVsdCB2YWx1ZSBpcyBcInRydWVcIlxuICpcbiAqICoqSW5pdGlhbGl6YXRpb24gRXZlbnQgVHlwZSB8IGBkYXRhLWVkaXRvci1pbml0LWV2ZW50LXR5cGVgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICpcbiAqIFByb3ZpZGUgYSBjdXN0b20gaW5pdGlhbGl6YXRpb24gZXZlbnQgd2hpY2ggd2lsbCB0cmlnZ2VyIHRoZSBzdGFydCBvZiB0aGUgZWRpdG9yIGNvbnZlcnNpb24uIEJ5IGRlZmF1bHQgdGhlXG4gKiBlZGl0b3IgaW5zdGFuY2VzIHdpbGwgYmUgY3JlYXRlZCBvbmNlIHRoZSBlbmdpbmUgaXMgcmVhZHkgJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCBidXQgdGhlcmUgYXJlIGNhc2VzIHdoZXJlXG4gKiBhIGN1c3RvbSBldmVudCBpcyByZXF1aXJlZCAoZS5nLiBpbml0aWFsaXphdGlvbiBvZiBlZGl0b3Igd2lkZ2V0IGR5bmFtaWNhbGx5IHdpdGhpbiBhIGRpYWxvZykuXG4gKlxuICpcbiAqICMjIyBPcHRpb25zIChUZXh0YXJlYSlcbiAqXG4gKiBUaGUgZm9sbG93aW5nIG9wdGlvbnMgYXJlIGJvdW5kIGFzIGRhdGEgYXR0cmlidXRlcyB0byBlYWNoIHRleHRhcmVhIGVsZW1lbnQgd2l0aGluIHRoZSBwYXJlbnQgY29udGFpbmVyLlxuICpcbiAqICoqRWRpdG9yIElkZW50aWZpZXIgfCBgZGF0YS1lZGl0b3ItaWRlbnRpZmllcmAgfCBTdHJpbmcgfCBSZXF1aXJlZCoqXG4gKlxuICogRWFjaCBjaGlsZCB0ZXh0YXJlYSBlbGVtZW50IG5lZWRzIHRvIGhhdmUgYSB1bmlxdWUgaWRlbnRpZmllciBzdHJpbmcgd2hpY2ggbmVlZHMgdG8gYXBwbHkgd2l0aCB0aGUgZm9sbG93aW5nXG4gKiBuYW1pbmcgY29udmVudGlvbiB0aGUgXCJlZGl0b3Ite3JlY29yZCB0eXBlfS17aWR9LXt0ZXh0YXJlYSBuYW1lfS17bGFuZ3VhZ2UgY29kZX1cbiAqIChlLmcuIGVkaXRvci1wcm9kdWN0cy0yLXNob3J0X2Rlc2NyaXB0aW9uLWRlKS4gSW4gY2FzZXMgd2hlcmUgdGhlIHJlY29yZCBJRCBpcyBub3Qgc2V0IHlldCAobmV3IHJlY29yZCBjcmVhdGlvbiksXG4gKiBpdCBpcyBhZHZpc2VkIHRoYXQgeW91IGxlYXZlIHRoZSB7aWR9IHBsYWNlaG9sZGVyIGFuZCByZXBsYWNlIGl0IGxhdGVyIG9uIHdoZW5ldmVyIHRoZSByZWNvcmQgaXMgZ2VuZXJhdGVkIGludG9cbiAqIHRoZSBkYXRhYmFzZSAobW9yZSBpbmZvcm1hdGlvbiBhYm91dCB0aGlzIGVkZ2UgY2FzZSBpbiB0aGUgZXhhbXBsZXMgYmVsb3cpLlxuICpcbiAqICoqRWRpdG9yIFR5cGUgfCBgZGF0YS1lZGl0b3ItdHlwZWAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogVGhpcyBvcHRpb24gY2FuIGhhdmUgb25lIG9mIHRoZSBhdmFpbGFibGUgZWRpdG9yIHZhbHVlcyB3aGljaCB3aWxsIGFsc28gc3RhdGUgdGhlIHNlbGVjdGVkIGVkaXRvciB1cG9uXG4gKiBpbml0aWFsaXphdGlvbi4gSXQgaXMgb3B0aW9uYWwgYW5kIHRoZSBkZWZhdWx0IHZhbHVlIGlzIFwiY2tlZGl0b3JcIi5cbiAqXG4gKlxuICogIyMjIEV2ZW50c1xuICpcbiAqIFRoZSAnI2VkaXRvci1jb250YWluZXInIGVsZW1lbnQgaXMgd2hlcmUgdGhlIHdpZGdldCBpcyBib3VuZCBvbi5cbiAqXG4gKiBgYGBqYXZhc2NyaXB0XG4gKiAvLyBGaXJlcyB1cCB3aGVuIGFsbCB0ZXh0YXJlYXMgYXJlIHJlYWR5LlxuICogJCgnI2VkaXRvci1jb250YWluZXInKS5vbignZWRpdG9yOnJlYWR5JywgKGV2ZW50LCAkdGV4dGFyZWFzKSA9PiB7IC4uLiB9KTtcbiAqXG4gKiAvLyBGaXJlcyB1cCBlYWNoIHRpbWUgYSBzaW5nbGUgdGV4dGFyZWEgaXMgcmVhZHkuXG4gKiAkKCcjZWRpdG9yLWNvbnRhaW5lcicpLm9uKCdlZGl0b3I6dGV4dGFyZWFfcmVhZHknLCAoZXZlbnQsICR0ZXh0YXJlYSkgPT4geyAuLi4gfSk7XG4gKiBgYGBcbiAqXG4gKlxuICogIyMjIEV4YW1wbGVzXG4gKlxuICogKipTaW1wbGUgVXNhZ2UqKlxuICpcbiAqIE5vdGljZSB0aGF0IHRoaXMgZXhhbXBsZSBzaG93Y2FzZXMgdGhlIGNyZWF0aW9uIG9mIGEgbmV3IGN1c3RvbWVyIHdoaWNoIG1lYW5zIHRoYXQgdGhlIGN1c3RvbWVyJ3MgSUQgaXMgbm90IGtub3duXG4gKiB5ZXQuIEFmdGVyIGl0cyBpbml0aWFsaXphdGlvbiwgdGhlIHdpZGdldCB3aWxsIGNyZWF0ZSBhIGhpZGRlbiBmaWVsZCBpbiB0aGUgZm9ybSB3aXRoIHRoZVxuICogXCJlZGl0b3JfaWRlbnRpZmllcnNbdGV4dGFyZWEtaWRlbnRpZmllcl1cIiBuYW1lLiBUaGlzIGhpZGRlbiBmaWVsZCB3aWxsIGhhdmUgdGhlIHNlbGVjdGVkIGVkaXRvciB0eXBlIGFzIHZhbHVlIHdoaWNoXG4gKiBiZSB1c2VkIGJ5IHRoZSBiYWNrZW5kIGNhbGxiYWNrIHRvIHN0b3JlIHRoZSBjb3JyZWN0IGVkaXRvciBpZGVudGlmaWVyIHZhbHVlLCBvbmNlIHRoZSBjdXN0b21lcidzIElEIGlzIGdlbmVyYXRlZFxuICogKHJlY29yZCBpbnNlcnRlZCkuIFVzZSB0aGUgXCJVc2VyQ29uZmlndXJhdGlvblNlcnZpY2VcIiBpbiBiYWNrZW5kIGZvciBhZGRpbmcgdGhlIHZhbHVlIHRvIHRoZSBkYXRhYmFzZS5cbiAqXG4gKiBgYGBodG1sXG4gKiA8ZGl2IGRhdGEtZ3gtd2lkZ2V0PVwiZWRpdG9yXCIgZGF0YS1lZGl0b3ItZXZlbnQtdGFyZ2V0PVwiI2N1c3RvbWVyLWZvcm1cIiAgZGF0YS1lZGl0b3ItZXZlbnQtdHlwZT1cInN1Ym1pdFwiPlxuICogICA8Zm9ybSBpZD1cImN1c3RvbWVyLWZvcm1cIj5cbiAqICAgICA8IS0tIE90aGVyIEZpZWxkcyAuLi4gLT5cbiAqICAgICA8dGV4dGFyZWEgY2xhc3M9XCJ3eXNpd3lnXCIgZGF0YS1lZGl0b3ItaWRlbnRpZmllcj1cImVkaXRvci1jdXN0b21lcnMte2lkfS1ub3Rlcy1kZVwiPjwvdGV4dGFyZWE+XG4gKiAgIDwvZm9ybT5cbiAqIDwvZGl2PlxuICogYGBgXG4gKlxuICogQG1vZHVsZSBBZG1pbi9XaWRnZXRzL2VkaXRvclxuICogQHJlcXVpcmVzIENLRWRpdG9yLCBDb2RlTWlycm9yXG4gKi9cbmd4LndpZGdldHMubW9kdWxlKFxuXHQnZWRpdG9yJyxcblx0XG5cdFtcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvY29kZW1pcnJvci9jb2RlbWlycm9yLm1pbi5jc3NgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9jb2RlbWlycm9yL2NvZGVtaXJyb3IubWluLmpzYCxcblx0XHRgJHtneC5zb3VyY2V9L2xpYnMvZWRpdG9yX2luc3RhbmNlc2AsXG5cdFx0YCR7Z3guc291cmNlfS9saWJzL2VkaXRvcl92YWx1ZXNgLFxuXHRcdGAke2d4LnNvdXJjZX0vd2lkZ2V0cy9xdWlja3NlbGVjdGAsXG5cdFx0J3VzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlJ1xuXHRdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBkZWZhdWx0cyA9IHtcblx0XHRcdHNlbGVjdG9yOiAndGV4dGFyZWEnLFxuXHRcdFx0YXV0b0hpZGU6ICdmYWxzZScsXG5cdFx0XHRpbml0RXZlbnRUeXBlOiAnSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsXG5cdFx0XHRhdXRvVXBkYXRlOiAndHJ1ZSdcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBFZGl0b3IgSW5zdGFuY2VzXG5cdFx0ICpcblx0XHQgKiBJZGVudGlmaWVyIC0+IGluc3RhbmNlIG1hcHBpbmdcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZWRpdG9ycyA9IHt9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEF2YWlsYWJsZSBFZGl0b3IgVHlwZXNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtTdHJpbmdbXX1cblx0XHQgKi9cblx0XHRjb25zdCBlZGl0b3JUeXBlcyA9IFsnY2tlZGl0b3InLCAnY29kZW1pcnJvciddO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFkZCBFZGl0b3IgU3dpdGNoIEJ1dHRvblxuXHRcdCAqXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCBhZGQgdGhlIGVkaXRvciBzd2l0Y2ggYnV0dG9uIGFuZCBiaW5kIHRoZSBjbGljayBldmVudCBoYW5kbGVyLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICR0ZXh0YXJlYSBUZXh0YXJlYSBzZWxlY3RvciB0byBiZSBtb2RpZmllZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfYWRkRWRpdG9yU3dpdGNoQnV0dG9uKCR0ZXh0YXJlYSkge1xuXHRcdFx0bGV0IHN0YXJ0ID0gMDtcblx0XHRcdGlmICgkdGV4dGFyZWEuZGF0YSgnZWRpdG9yVHlwZScpID09PSAnY29kZW1pcnJvcicpIHtcblx0XHRcdFx0c3RhcnQgPSAxO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkdGV4dGFyZWFcblx0XHRcdFx0LndyYXAoJzxkaXYgY2xhc3M9XCJlZGl0b3Itd3JhcHBlclwiIC8+Jylcblx0XHRcdFx0LnBhcmVudCgpXG5cdFx0XHRcdC5wcmVwZW5kKGA8ZGl2IGRhdGEtZ3gtd2lkZ2V0PVwicXVpY2tzZWxlY3RcIiBkYXRhLXF1aWNrc2VsZWN0LWFsaWduPVwicmlnaHRcIiBkYXRhLXF1aWNrc2VsZWN0LXN0YXJ0PVwiYFxuXHRcdFx0XHRcdCsgc3RhcnQgKyBgXCI+XG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJxdWlja3NlbGVjdC1oZWFkbGluZS13cmFwcGVyXCI+XG5cdFx0XHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJlZGl0b3Itc3dpdGNoIGVkaXRvci1zd2l0Y2gtaHRtbFwiIGhyZWY9XCIjaHRtbFwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0JHtqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX1NXSVRDSF9FRElUT1JfVEVYVCcsICdhZG1pbl9idXR0b25zJyl9XG5cdFx0XHRcdFx0XHRcdFx0PC9hPlxuXHRcdFx0XHRcdFx0XHRcdDxhIGNsYXNzPVwiZWRpdG9yLXN3aXRjaCBlZGl0b3Itc3dpdGNoLXRleHRcIiBocmVmPVwiI3RleHRcIj5cblx0XHRcdFx0XHRcdFx0XHRcdCR7anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9TV0lUQ0hfRURJVE9SX0hUTUwnLCAnYWRtaW5fYnV0dG9ucycpfVxuXHRcdFx0XHRcdFx0XHRcdDwvYT5cblx0XHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5gKVxuXHRcdFx0XHQuZmluZCgnLmVkaXRvci1zd2l0Y2gnKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgX29uU3dpdGNoQnV0dG9uQ2xpY2spO1xuXHRcdFx0XG5cdFx0XHRpZiAoISR0ZXh0YXJlYS5pcygnOnZpc2libGUnKSAmJiBvcHRpb25zLmF1dG9IaWRlID09PSAndHJ1ZScpIHtcblx0XHRcdFx0JHRleHRhcmVhLnBhcmVudCgpLmhpZGUoKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Z3gud2lkZ2V0cy5pbml0KCR0ZXh0YXJlYS5wYXJlbnQoKSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFkZCBhIGhpZGRlbiBlZGl0b3IgdHlwZSBmaWVsZC5cblx0XHQgKlxuXHRcdCAqIFRoaXMgZmllbGQgd2lsbCBjb250YWluIHRoZSB0eXBlIG9mIHRoZSBjdXJyZW50IGVkaXRvciBhbmQgY2FuIGJlIHVzZWQgYnkgc3VibWl0IGNhbGxiYWNrcyB3aGVuZXZlciB0aGVcblx0XHQgKiByZWNvcmQgSUQgaXMgbm90IGtub3duIHlldCBhbmQgdGhlIHVzZXIgY29uZmlndXJhdGlvbiBlbnRyeSBpcyBnZW5lcmF0ZWQgYnkgdGhlIHNlcnZlci5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGV4dGFyZWEgVGV4dGFyZWEgc2VsZWN0b3IgdG8gYmUgbW9kaWZpZWQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2FkZEVkaXRvckhpZGRlbkZpZWxkKCR0ZXh0YXJlYSkge1xuXHRcdFx0JHRleHRhcmVhXG5cdFx0XHRcdC5wYXJlbnQoKVxuXHRcdFx0XHQuYXBwZW5kKGBcblx0XHRcdFx0XHQ8aW5wdXQgdHlwZT1cImhpZGRlblwiIFxuXHRcdFx0XHRcdFx0bmFtZT1cImVkaXRvcl9pZGVudGlmaWVyc1skeyR0ZXh0YXJlYS5kYXRhKCdlZGl0b3JJZGVudGlmaWVyJyl9XVwiIFxuXHRcdFx0XHRcdFx0dmFsdWU9XCIkeyR0ZXh0YXJlYS5kYXRhKCdlZGl0b3JUeXBlJykgfHwgJ2NrZWRpdG9yJ31cIiAvPlxuXHRcdFx0XHRgKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlIEVkaXRvciBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCB1c2UgdGhlIFwiZWRpdG9yXCIgbGlicmFyeSB0byBjcmVhdGUgdGhlIGFwcHJvcHJpYXRlIGVkaXRvciBpbnN0YW5jZSwgZGVwZW5kaW5nIHRoZSB0ZXh0YXJlYVxuXHRcdCAqIHR5cGUgYXR0cmlidXRlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICR0ZXh0YXJlYSBUZXh0YXJlYSBzZWxlY3RvciB0byBiZSBtb2RpZmllZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfY3JlYXRlRWRpdG9ySW5zdGFuY2UoJHRleHRhcmVhKSB7XG5cdFx0XHRjb25zdCB0eXBlID0gJHRleHRhcmVhLmRhdGEoJ2VkaXRvclR5cGUnKSB8fCAnY2tlZGl0b3InO1xuXHRcdFx0Y29uc3QgaWRlbnRpZmllciA9ICR0ZXh0YXJlYS5kYXRhKCdlZGl0b3JJZGVudGlmaWVyJyk7XG5cdFx0XHRcblx0XHRcdGVkaXRvcnNbaWRlbnRpZmllcl0gPSBqc2UubGlicy5lZGl0b3JfaW5zdGFuY2VzLmNyZWF0ZSgkdGV4dGFyZWEsIHR5cGUpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBPbiBTd2l0Y2ggQnV0dG9uIENsaWNrIEV2ZW50IEhhbmRsZXJcblx0XHQgKlxuXHRcdCAqIFRoaXMgbWV0aG9kIHdpbGwgdXNlIHRoZSBcImVkaXRvclwiIGxpYnJhcnkgdG8gY2hhbmdlIHRoZSBjdXJyZW50IGVkaXRvciB0eXBlIGFuZCB1cGRhdGUgdGhlIGhpZGRlbiBpbnB1dFxuXHRcdCAqIGZpZWxkIGFuZCBkYXRhIGF0dHJpYnV0ZXMgb2YgdGhlIHRleHRhcmVhLiBJdCB3aWxsIHRyeSB0byBzZXQgdGhlIG5leHQgYXZhaWxhYmxlIGVkaXRvciB0eXBlLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9vblN3aXRjaEJ1dHRvbkNsaWNrKCkge1xuXHRcdFx0Y29uc3QgJHN3aXRjaEJ1dHRvbiA9ICQodGhpcyk7XG5cdFx0XHRjb25zdCAkdGV4dGFyZWEgPSAkc3dpdGNoQnV0dG9uLnBhcmVudHMoJy5lZGl0b3Itd3JhcHBlcicpLmZpbmQoJ3RleHRhcmVhJyk7XG5cdFx0XHRjb25zdCBpZGVudGlmaWVyID0gJHRleHRhcmVhLmRhdGEoJ2VkaXRvcklkZW50aWZpZXInKTtcblx0XHRcdGNvbnN0IGN1cnJlbnRUeXBlID0gJHRleHRhcmVhLmRhdGEoJ2VkaXRvclR5cGUnKTtcblx0XHRcdGNvbnN0IG5ld1R5cGUgPSAkc3dpdGNoQnV0dG9uLmhhc0NsYXNzKCdlZGl0b3Itc3dpdGNoLXRleHQnKSA/IGVkaXRvclR5cGVzWzFdIDogZWRpdG9yVHlwZXNbMF07XG5cdFx0XHRcblx0XHRcdCR0ZXh0YXJlYS5zaWJsaW5ncyhgW25hbWU9XCJlZGl0b3JfaWRlbnRpZmllcnNbJHtpZGVudGlmaWVyfV1cIl1gKS52YWwobmV3VHlwZSk7XG5cdFx0XHQkdGV4dGFyZWEuZGF0YSgnZWRpdG9yVHlwZScsIG5ld1R5cGUpO1xuXHRcdFx0XG5cdFx0XHRlZGl0b3JzW2lkZW50aWZpZXJdID0ganNlLmxpYnMuZWRpdG9yX2luc3RhbmNlcy5zd2l0Y2goJHRleHRhcmVhLCBjdXJyZW50VHlwZSwgbmV3VHlwZSk7XG5cdFx0XHRfYmluZEF1dG9VcGRhdGUoJHRleHRhcmVhKTtcblx0XHRcdF91cGRhdGVUZXh0QXJlYSgkdGV4dGFyZWEpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBPbiBQYWdlIFN1Ym1pdCBIYW5kbGVyXG5cdFx0ICpcblx0XHQgKiBJZiB0aGUgZXZlbnQgdGFyZ2V0IGFuZCB0eXBlIGFyZSBwcm92aWRlZCB0aGlzIG1ldGhvZCB3aWxsIGJlIHRyaWdnZXJlZCB0byBzYXZlIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb25cblx0XHQgKiB2YWx1ZXMgaW4gdGhlIGRhdGFiYXNlIHdpdGggQUpBWCByZXF1ZXN0cy5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25QYWdlU3VibWl0KCkge1xuXHRcdFx0Zm9yIChsZXQgaWRlbnRpZmllciBpbiBlZGl0b3JzKSB7XG5cdFx0XHRcdGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlLnNldCh7XG5cdFx0XHRcdFx0ZGF0YToge1xuXHRcdFx0XHRcdFx0dXNlcklkOiAwLFxuXHRcdFx0XHRcdFx0Y29uZmlndXJhdGlvbktleTogaWRlbnRpZmllcixcblx0XHRcdFx0XHRcdGNvbmZpZ3VyYXRpb25WYWx1ZTogZWRpdG9yc1tpZGVudGlmaWVyXS50eXBlXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQmluZCBBdXRvIFVwZGF0ZVxuXHRcdCAqXG5cdFx0ICogQmluZHMgYW4gZXZlbnQgaGFuZGxlciB0byBhbiBlZGl0b3IgaW5zdGFuY2UgdG8gYXV0b21hdGljYWxseSB1cGRhdGUgdGhlXG5cdFx0ICogY29ycmVzcG9uZGluZyB0ZXh0YXJlYS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGV4dGFyZWEgVGV4dGFyZWEgdGhlIGF1dG8gdXBkYXRlIHNob3VsZCBiZSBib3VuZCB0b1xuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9iaW5kQXV0b1VwZGF0ZSgkdGV4dGFyZWEpIHtcblx0XHRcdGlmIChvcHRpb25zLmF1dG9VcGRhdGUgPT09ICdmYWxzZScpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRjb25zdCBpbnN0YW5jZSA9IGVkaXRvcnNbJHRleHRhcmVhLmRhdGEoJ2VkaXRvcklkZW50aWZpZXInKV07XG5cdFx0XHRcblx0XHRcdGluc3RhbmNlLm9uKCdjaGFuZ2UnLCAoKSA9PiBfdXBkYXRlVGV4dEFyZWEoJHRleHRhcmVhKSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZSBUZXh0IEFyZWEgVmFsdWVcblx0XHQgKlxuXHRcdCAqIFRyYW5zZmVycyB0aGUgdmFsdWUgb2YgdGhlIGVkaXRvciBpbnN0YW5jZSBvZiB0aGUgZ2l2ZW4gdGV4dGFyZWEgdG8gaXRzIGNvcnJlc3BvbmRpbmcgdGV4dGFyZWEuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRleHRhcmVhIFRoZSB0ZXh0YXJlYSB0byBiZSB1cGRhdGVkLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF91cGRhdGVUZXh0QXJlYSgkdGV4dGFyZWEpIHtcblx0XHRcdGNvbnN0IGVkaXRvclR5cGUgPSAkdGV4dGFyZWEuZGF0YSgnZWRpdG9yVHlwZScpO1xuXHRcdFx0Y29uc3QgaW5zdGFuY2UgPSBlZGl0b3JzWyR0ZXh0YXJlYS5kYXRhKCdlZGl0b3JJZGVudGlmaWVyJyldO1xuXHRcdFx0XG5cdFx0XHRzd2l0Y2ggKGVkaXRvclR5cGUpIHtcblx0XHRcdFx0Y2FzZSAnY2tlZGl0b3InOlxuXHRcdFx0XHRcdGluc3RhbmNlLnVwZGF0ZUVsZW1lbnQoKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XG5cdFx0XHRcdGNhc2UgJ2NvZGVtaXJyb3InOlxuXHRcdFx0XHRcdCR0ZXh0YXJlYS52YWwoanNlLmxpYnMuZWRpdG9yX3ZhbHVlcy5nZXRWYWx1ZSgkdGV4dGFyZWEpKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XG5cdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdFZGl0b3IgdHlwZSBub3QgcmVjb2duaXplZC4nLCBlZGl0b3JUeXBlKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JHRleHRhcmVhLnRyaWdnZXIoJ2NoYW5nZScpO1xuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JChkb2N1bWVudCkub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCAoKSA9PiB7XG5cdFx0XHRcdGNvbnN0IGRlcGVuZGVuY2llcyA9IFtcblx0XHRcdFx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvY29kZW1pcnJvci9jc3MubWluLmpzYCxcblx0XHRcdFx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvY29kZW1pcnJvci9odG1sbWl4ZWQubWluLmpzYCxcblx0XHRcdFx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvY29kZW1pcnJvci9qYXZhc2NyaXB0Lm1pbi5qc2AsXG5cdFx0XHRcdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2NvZGVtaXJyb3IveG1sLm1pbi5qc2Bcblx0XHRcdFx0XTtcblx0XHRcdFx0XG5cdFx0XHRcdGpzZS5jb3JlLm1vZHVsZV9sb2FkZXIucmVxdWlyZShkZXBlbmRlbmNpZXMpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIEluaXRpYWxpemUgdGhlIGVkaXRvcnMgYWZ0ZXIgYSBzcGVjaWZpYyBldmVudCBpbiBvcmRlciB0byBtYWtlIHN1cmUgdGhhdCBvdGhlciBtb2R1bGVzIHdpbGwgYmVcblx0XHRcdC8vIGFscmVhZHkgaW5pdGlhbGl6ZWQgYW5kIG5vdGhpbmcgZWxzZSB3aWxsIGNoYW5nZSB0aGUgbWFya3VwLlxuXHRcdFx0JCh3aW5kb3cpLm9uKG9wdGlvbnMuaW5pdEV2ZW50VHlwZSwgKCkgPT4ge1xuXHRcdFx0XHRjb25zdCAkdGV4dGFyZWFzID0gJHRoaXMuZmluZChvcHRpb25zLnNlbGVjdG9yKTtcblx0XHRcdFx0XG5cdFx0XHRcdCR0ZXh0YXJlYXMuZWFjaCgoaW5kZXgsIHRleHRhcmVhKSA9PiB7XG5cdFx0XHRcdFx0Y29uc3QgJHRleHRhcmVhID0gJCh0ZXh0YXJlYSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKGVkaXRvclR5cGVzLmluZGV4T2YoJHRleHRhcmVhLmRhdGEoJ2VkaXRvclR5cGUnKSkgPT09IC0xKSB7XG5cdFx0XHRcdFx0XHQkdGV4dGFyZWEuZGF0YSgnZWRpdG9yVHlwZScsIGVkaXRvclR5cGVzWzBdKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0X2FkZEVkaXRvclN3aXRjaEJ1dHRvbigkdGV4dGFyZWEpO1xuXHRcdFx0XHRcdF9hZGRFZGl0b3JIaWRkZW5GaWVsZCgkdGV4dGFyZWEpO1xuXHRcdFx0XHRcdF9jcmVhdGVFZGl0b3JJbnN0YW5jZSgkdGV4dGFyZWEpO1xuXHRcdFx0XHRcdF9iaW5kQXV0b1VwZGF0ZSgkdGV4dGFyZWEpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCR0aGlzLnRyaWdnZXIoJ2VkaXRvcjp0ZXh0YXJlYV9yZWFkeScsIFskdGV4dGFyZWFdKTtcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkdGhpcy50cmlnZ2VyKCdlZGl0b3I6cmVhZHknLCBbJHRleHRhcmVhc10pO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIElmIHRoZSBldmVudCB0YXJnZXQgYW5kIHR5cGUgb3B0aW9ucyBhcmUgYXZhaWxhYmxlLCBiaW5kIHRoZSBwYWdlIHN1Ym1pdCBoYW5kbGVyLiBcblx0XHRcdGlmIChvcHRpb25zLmV2ZW50VGFyZ2V0ICE9PSB1bmRlZmluZWQgJiYgb3B0aW9ucy5ldmVudFR5cGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHQkKG9wdGlvbnMuZXZlbnRUYXJnZXQpLm9uKG9wdGlvbnMuZXZlbnRUeXBlLCBfb25QYWdlU3VibWl0KTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
