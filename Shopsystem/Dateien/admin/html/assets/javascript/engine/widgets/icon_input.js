'use strict';

/* --------------------------------------------------------------
 icon_input.js 2016-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Icon Input Widget
 *
 * Turns normal input fields into input fields with a provided background image.
 *
 * ### Example
 *
 * The "icon-input" activates the widget and attaches the needed styles for the background image
 * which is provided by the `data-icon` attribute.
 * 
 * ```html
 * <input data-gx-widget="icon_input" data-icon="url/to/image-file.png"/>
 * ```
 * 
 * @todo Add automatic image dimension adjustment. Images - for example if they are too big in dimensions - won't scale correctly at the moment. 
 * 
 * @module Admin/Widgets/icon_input
 */
gx.widgets.module('icon_input', ['xhr'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Widget Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Adds the dropdown functionality to the button.
  *
  * Developers can manually add new <li> items to the list in order to display more options to
  * the users.
  *
  * @private
  */
	var _setBackgroundImage = function _setBackgroundImage() {
		var iconValue = $this.attr('data-icon');
		var langId = $this.attr('data-lang-id');

		if (undefined === iconValue && undefined === langId) {
			throw new Error('Whether data-lang-id or data-icon attribute is required!');
		}

		if (undefined !== langId) {
			jse.libs.xhr.get({
				url: './admin.php?do=JSWidgetsAjax/iconInput&language_id=' + langId
			}).done(function (r) {
				return $this.css('background', 'url(' + r.iconUrl + ')' + ' no-repeat right 8px center white');
			});
		} else {
			$this.css('background', 'url(' + iconValue + ')' + ' no-repeat right 8px center white');
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		_setBackgroundImage();
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImljb25faW5wdXQuanMiXSwibmFtZXMiOlsiZ3giLCJ3aWRnZXRzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9zZXRCYWNrZ3JvdW5kSW1hZ2UiLCJpY29uVmFsdWUiLCJhdHRyIiwibGFuZ0lkIiwidW5kZWZpbmVkIiwiRXJyb3IiLCJqc2UiLCJsaWJzIiwieGhyIiwiZ2V0IiwidXJsIiwiZG9uZSIsImNzcyIsInIiLCJpY29uVXJsIiwiaW5pdCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkFBLEdBQUdDLE9BQUgsQ0FBV0MsTUFBWCxDQUVDLFlBRkQsRUFJQyxDQUFDLEtBQUQsQ0FKRCxFQU1DLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXLEVBYlo7OztBQWVDOzs7OztBQUtBQyxXQUFVRixFQUFFRyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCSCxJQUE3QixDQXBCWDs7O0FBc0JDOzs7OztBQUtBRCxVQUFTLEVBM0JWOztBQTZCQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FBUUEsS0FBTU8sc0JBQXNCLFNBQXRCQSxtQkFBc0IsR0FBTTtBQUNqQyxNQUFNQyxZQUFZTixNQUFNTyxJQUFOLENBQVcsV0FBWCxDQUFsQjtBQUNBLE1BQU1DLFNBQVNSLE1BQU1PLElBQU4sQ0FBVyxjQUFYLENBQWY7O0FBRUEsTUFBSUUsY0FBY0gsU0FBZCxJQUEyQkcsY0FBY0QsTUFBN0MsRUFBcUQ7QUFDcEQsU0FBTSxJQUFJRSxLQUFKLENBQVUsMERBQVYsQ0FBTjtBQUNBOztBQUVELE1BQUlELGNBQWNELE1BQWxCLEVBQTBCO0FBQ3pCRyxPQUFJQyxJQUFKLENBQVNDLEdBQVQsQ0FBYUMsR0FBYixDQUFpQjtBQUNoQkMsU0FBSyx3REFBd0RQO0FBRDdDLElBQWpCLEVBRUdRLElBRkgsQ0FFUTtBQUFBLFdBQUtoQixNQUFNaUIsR0FBTixDQUFVLFlBQVYsRUFBd0IsU0FBU0MsRUFBRUMsT0FBWCxHQUFxQixHQUFyQixHQUEyQixtQ0FBbkQsQ0FBTDtBQUFBLElBRlI7QUFHQSxHQUpELE1BSU87QUFDTm5CLFNBQU1pQixHQUFOLENBQVUsWUFBVixFQUF3QixTQUFTWCxTQUFULEdBQXFCLEdBQXJCLEdBQTJCLG1DQUFuRDtBQUNBO0FBQ0QsRUFmRDs7QUFpQkE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQVIsUUFBT3NCLElBQVAsR0FBYyxVQUFTSixJQUFULEVBQWU7QUFDNUJYO0FBQ0FXO0FBQ0EsRUFIRDs7QUFLQTtBQUNBLFFBQU9sQixNQUFQO0FBQ0EsQ0F0RkYiLCJmaWxlIjoiaWNvbl9pbnB1dC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gaWNvbl9pbnB1dC5qcyAyMDE2LTAyLTIzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBJY29uIElucHV0IFdpZGdldFxuICpcbiAqIFR1cm5zIG5vcm1hbCBpbnB1dCBmaWVsZHMgaW50byBpbnB1dCBmaWVsZHMgd2l0aCBhIHByb3ZpZGVkIGJhY2tncm91bmQgaW1hZ2UuXG4gKlxuICogIyMjIEV4YW1wbGVcbiAqXG4gKiBUaGUgXCJpY29uLWlucHV0XCIgYWN0aXZhdGVzIHRoZSB3aWRnZXQgYW5kIGF0dGFjaGVzIHRoZSBuZWVkZWQgc3R5bGVzIGZvciB0aGUgYmFja2dyb3VuZCBpbWFnZVxuICogd2hpY2ggaXMgcHJvdmlkZWQgYnkgdGhlIGBkYXRhLWljb25gIGF0dHJpYnV0ZS5cbiAqIFxuICogYGBgaHRtbFxuICogPGlucHV0IGRhdGEtZ3gtd2lkZ2V0PVwiaWNvbl9pbnB1dFwiIGRhdGEtaWNvbj1cInVybC90by9pbWFnZS1maWxlLnBuZ1wiLz5cbiAqIGBgYFxuICogXG4gKiBAdG9kbyBBZGQgYXV0b21hdGljIGltYWdlIGRpbWVuc2lvbiBhZGp1c3RtZW50LiBJbWFnZXMgLSBmb3IgZXhhbXBsZSBpZiB0aGV5IGFyZSB0b28gYmlnIGluIGRpbWVuc2lvbnMgLSB3b24ndCBzY2FsZSBjb3JyZWN0bHkgYXQgdGhlIG1vbWVudC4gXG4gKiBcbiAqIEBtb2R1bGUgQWRtaW4vV2lkZ2V0cy9pY29uX2lucHV0XG4gKi9cbmd4LndpZGdldHMubW9kdWxlKFxuXHRcblx0J2ljb25faW5wdXQnLFxuXHRcblx0Wyd4aHInXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBXaWRnZXQgUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgV2lkZ2V0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIFdpZGdldCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFBSSVZBVEUgTUVUSE9EU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFkZHMgdGhlIGRyb3Bkb3duIGZ1bmN0aW9uYWxpdHkgdG8gdGhlIGJ1dHRvbi5cblx0XHQgKlxuXHRcdCAqIERldmVsb3BlcnMgY2FuIG1hbnVhbGx5IGFkZCBuZXcgPGxpPiBpdGVtcyB0byB0aGUgbGlzdCBpbiBvcmRlciB0byBkaXNwbGF5IG1vcmUgb3B0aW9ucyB0b1xuXHRcdCAqIHRoZSB1c2Vycy5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0Y29uc3QgX3NldEJhY2tncm91bmRJbWFnZSA9ICgpID0+IHtcblx0XHRcdGNvbnN0IGljb25WYWx1ZSA9ICR0aGlzLmF0dHIoJ2RhdGEtaWNvbicpO1xuXHRcdFx0Y29uc3QgbGFuZ0lkID0gJHRoaXMuYXR0cignZGF0YS1sYW5nLWlkJylcblx0XHRcdFxuXHRcdFx0aWYgKHVuZGVmaW5lZCA9PT0gaWNvblZhbHVlICYmIHVuZGVmaW5lZCA9PT0gbGFuZ0lkKSB7XG5cdFx0XHRcdHRocm93IG5ldyBFcnJvcignV2hldGhlciBkYXRhLWxhbmctaWQgb3IgZGF0YS1pY29uIGF0dHJpYnV0ZSBpcyByZXF1aXJlZCEnKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKHVuZGVmaW5lZCAhPT0gbGFuZ0lkKSB7XG5cdFx0XHRcdGpzZS5saWJzLnhoci5nZXQoe1xuXHRcdFx0XHRcdHVybDogJy4vYWRtaW4ucGhwP2RvPUpTV2lkZ2V0c0FqYXgvaWNvbklucHV0Jmxhbmd1YWdlX2lkPScgKyBsYW5nSWRcblx0XHRcdFx0fSkuZG9uZShyID0+ICR0aGlzLmNzcygnYmFja2dyb3VuZCcsICd1cmwoJyArIHIuaWNvblVybCArICcpJyArICcgbm8tcmVwZWF0IHJpZ2h0IDhweCBjZW50ZXIgd2hpdGUnKSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQkdGhpcy5jc3MoJ2JhY2tncm91bmQnLCAndXJsKCcgKyBpY29uVmFsdWUgKyAnKScgKyAnIG5vLXJlcGVhdCByaWdodCA4cHggY2VudGVyIHdoaXRlJyk7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSB3aWRnZXQsIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0X3NldEJhY2tncm91bmRJbWFnZSgpO1xuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
