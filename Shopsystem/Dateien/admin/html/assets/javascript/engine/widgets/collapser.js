'use strict';

/* --------------------------------------------------------------
 collapser.js 2016-02-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Collapser Widget
 *
 * This widget expands or collapses the target element. It is not visible when collapsed but visible when expanded.
 *
 * ### Options
 *
 * **Collapsed | `data-collapser-collapsed` | Boolean | Optional**
 *
 * Default state of the collapser. If no value is provided, it defaults to `false`.
 *
 * **Collapsed Icon Class | `data-collapser-collapsed_icon_class` | String | Optional**
 *
 * Default Font Awesome icon when the collapser is collapsed. If no value is provided, it defaults to **'fa-plus-square-o'**.
 *
 * **Expanded Icon Class | `data-collapser-expanded_icon_class` | String | Optional**
 *
 * Default Font Awesome icon when the collapser is expanded. If no value is provided, it defaults to **'fa-minus-square-o'**.
 *
 * **Additional Classes | `data-collapser-additional_classes` | String | Optional**
 *
 * Provide additional CSS-Classes which should be added. If no value is provided, it defaults to **'pull-right'**,
 * which applies a CSS *'float: right'* style.
 *
 * **Target Selector | `data-collapser-target_selector` | String | Required**
 *
 * Provide the target selector, which is the element thath will be collapsed or expanded.
 *
 * **Parent Selector | `data-collapser-parent_selector` | String | Optional**
 *
 * Provide a parent selector for the collapser. It's empty by default.
 *
 * ### Example
 *
 * When the page loads, the **collapser** widget will be added to the `<div>` element.
 * On click, the target selector will be shown or hidden.
 *
 * ```html
 *  <div class="headline-wrapper"
 *      data-gx-widget="collapser"
 *      data-collapser-target_selector=".content-wrapper"
 *      data-collapser-section="category_base_data"
 *      data-collapser-user_id="1"
 *      data-collapser-collapsed="true">
 *    Click This Headline
 *  </div>
 *  <div class="content-wrapper">
 *    Toggled content
 *  </div>
 * ```
 *
 * @module Admin/Widgets/collapser
 *
 * @todo Make the styling for this widget (like it is on the products site) more general. Currently, the "div" element
 * has to be wrapped in another "div" with specific CSS-Classes like ".gx-container" and ".frame-head".
 */
gx.widgets.module('collapser', ['user_configuration_service'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Widget Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * UserConfigurationService Alias
  *
  * @type {object}
  */
	userConfigurationService = jse.libs.user_configuration_service,


	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	defaults = {
		collapsed: false,
		collapsed_icon_class: 'fa-plus-square-o',
		expanded_icon_class: 'fa-minus-square-o',
		additional_classes: 'pull-right',
		parent_selector: '',
		is_button_trigger: false
	},


	/**
  * Final Widget Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Sets the cursor to pointer
  * @private
  */
	var _setMouseCursorPointer = function _setMouseCursorPointer() {
		$this.addClass('cursor-pointer').children().addClass('cursor-pointer');
	};

	/**
  * Sets the initial visibility according to the 'collapsed' value
  * @private
  */
	var _setInitialVisibilityState = function _setInitialVisibilityState() {
		if (options.collapsed) {
			if (options.parent_selector) {
				$this.parents(options.parent_selector).next(options.target_selector).hide();
			} else {
				$this.next(options.target_selector).hide();
			}
		}
	};

	/**
  * Creates the markup for the collapser and adds the click event handler
  * @private
  */
	var _createCollapser = function _createCollapser() {
		var $button = $('<span>', { class: 'collapser ' + options.additional_classes });
		var $icon = $('<i>', { class: 'fa ' + (options.collapsed ? options.collapsed_icon_class : options.expanded_icon_class) });

		$button.append($icon).appendTo($this);

		if (options.is_button_trigger) {
			$button.on('click', _toggleVisibilityState);
		} else {
			$this.on('click', _toggleVisibilityState);
		}
	};

	/**
  * Saves the current visibility state.
  *
  * @private
  */
	var _saveVisibilityState = function _saveVisibilityState() {
		var collapseState = $this.find('.collapser > i.fa').hasClass(options.collapsed_icon_class);

		userConfigurationService.set({
			data: {
				userId: options.user_id,
				configurationKey: options.section + '_collapse',
				configurationValue: collapseState
			}
		});
	};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Toggles the visibility state and switches between plus and minus icon.
  *
  * @private
  */
	var _toggleVisibilityState = function _toggleVisibilityState() {
		if (options.parent_selector) {
			$this.parents(options.parent_selector).next(options.target_selector).toggle();
		} else {
			$this.next(options.target_selector).toggle();
		}

		$this.find('.collapser > i.fa').toggleClass(options.collapsed_icon_class);
		$this.find('.collapser > i.fa').toggleClass(options.expanded_icon_class);

		_saveVisibilityState();
	};

	// ------------------------------------------------------------------------
	// INITIALIZE
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the module, called by the engine.
  */
	module.init = function (done) {
		_setMouseCursorPointer();
		_setInitialVisibilityState();
		_createCollapser();
		done();
	};

	// Return data to module engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbGxhcHNlci5qcyJdLCJuYW1lcyI6WyJneCIsIndpZGdldHMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwidXNlckNvbmZpZ3VyYXRpb25TZXJ2aWNlIiwianNlIiwibGlicyIsInVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlIiwiZGVmYXVsdHMiLCJjb2xsYXBzZWQiLCJjb2xsYXBzZWRfaWNvbl9jbGFzcyIsImV4cGFuZGVkX2ljb25fY2xhc3MiLCJhZGRpdGlvbmFsX2NsYXNzZXMiLCJwYXJlbnRfc2VsZWN0b3IiLCJpc19idXR0b25fdHJpZ2dlciIsIm9wdGlvbnMiLCJleHRlbmQiLCJfc2V0TW91c2VDdXJzb3JQb2ludGVyIiwiYWRkQ2xhc3MiLCJjaGlsZHJlbiIsIl9zZXRJbml0aWFsVmlzaWJpbGl0eVN0YXRlIiwicGFyZW50cyIsIm5leHQiLCJ0YXJnZXRfc2VsZWN0b3IiLCJoaWRlIiwiX2NyZWF0ZUNvbGxhcHNlciIsIiRidXR0b24iLCJjbGFzcyIsIiRpY29uIiwiYXBwZW5kIiwiYXBwZW5kVG8iLCJvbiIsIl90b2dnbGVWaXNpYmlsaXR5U3RhdGUiLCJfc2F2ZVZpc2liaWxpdHlTdGF0ZSIsImNvbGxhcHNlU3RhdGUiLCJmaW5kIiwiaGFzQ2xhc3MiLCJzZXQiLCJ1c2VySWQiLCJ1c2VyX2lkIiwiY29uZmlndXJhdGlvbktleSIsInNlY3Rpb24iLCJjb25maWd1cmF0aW9uVmFsdWUiLCJ0b2dnbGUiLCJ0b2dnbGVDbGFzcyIsImluaXQiLCJkb25lIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0RBQSxHQUFHQyxPQUFILENBQVdDLE1BQVgsQ0FDQyxXQURELEVBR0MsQ0FBQyw0QkFBRCxDQUhELEVBS0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLDRCQUEyQkMsSUFBSUMsSUFBSixDQUFTQywwQkFickM7OztBQWVDOzs7OztBQUtBQyxZQUFXO0FBQ1ZDLGFBQVcsS0FERDtBQUVWQyx3QkFBc0Isa0JBRlo7QUFHVkMsdUJBQXFCLG1CQUhYO0FBSVZDLHNCQUFvQixZQUpWO0FBS1ZDLG1CQUFpQixFQUxQO0FBTVZDLHFCQUFtQjtBQU5ULEVBcEJaOzs7QUE2QkM7Ozs7O0FBS0FDLFdBQVVaLEVBQUVhLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQlIsUUFBbkIsRUFBNkJQLElBQTdCLENBbENYOzs7QUFvQ0M7Ozs7O0FBS0FELFVBQVMsRUF6Q1Y7O0FBMkNBO0FBQ0E7QUFDQTs7QUFFQTs7OztBQUlBLEtBQUlpQix5QkFBeUIsU0FBekJBLHNCQUF5QixHQUFXO0FBQ3ZDZixRQUFNZ0IsUUFBTixDQUFlLGdCQUFmLEVBQWlDQyxRQUFqQyxHQUE0Q0QsUUFBNUMsQ0FBcUQsZ0JBQXJEO0FBQ0EsRUFGRDs7QUFJQTs7OztBQUlBLEtBQUlFLDZCQUE2QixTQUE3QkEsMEJBQTZCLEdBQVc7QUFDM0MsTUFBSUwsUUFBUU4sU0FBWixFQUF1QjtBQUN0QixPQUFJTSxRQUFRRixlQUFaLEVBQTZCO0FBQzVCWCxVQUFNbUIsT0FBTixDQUFjTixRQUFRRixlQUF0QixFQUF1Q1MsSUFBdkMsQ0FBNENQLFFBQVFRLGVBQXBELEVBQXFFQyxJQUFyRTtBQUNBLElBRkQsTUFHSztBQUNKdEIsVUFBTW9CLElBQU4sQ0FBV1AsUUFBUVEsZUFBbkIsRUFBb0NDLElBQXBDO0FBQ0E7QUFDRDtBQUNELEVBVEQ7O0FBV0E7Ozs7QUFJQSxLQUFJQyxtQkFBbUIsU0FBbkJBLGdCQUFtQixHQUFXO0FBQ2pDLE1BQUlDLFVBQVV2QixFQUFFLFFBQUYsRUFBWSxFQUFDd0Isc0JBQW9CWixRQUFRSCxrQkFBN0IsRUFBWixDQUFkO0FBQ0EsTUFBSWdCLFFBQVF6QixFQUFFLEtBQUYsRUFBUyxFQUFDd0IsZ0JBQWFaLFFBQVFOLFNBQVIsR0FBb0JNLFFBQVFMLG9CQUE1QixHQUFtREssUUFBUUosbUJBQXhFLENBQUQsRUFBVCxDQUFaOztBQUVBZSxVQUNFRyxNQURGLENBQ1NELEtBRFQsRUFFRUUsUUFGRixDQUVXNUIsS0FGWDs7QUFJQSxNQUFJYSxRQUFRRCxpQkFBWixFQUErQjtBQUM5QlksV0FBUUssRUFBUixDQUFXLE9BQVgsRUFBb0JDLHNCQUFwQjtBQUNBLEdBRkQsTUFFTztBQUNOOUIsU0FBTTZCLEVBQU4sQ0FBUyxPQUFULEVBQWtCQyxzQkFBbEI7QUFDQTtBQUNELEVBYkQ7O0FBZUE7Ozs7O0FBS0EsS0FBSUMsdUJBQXVCLFNBQXZCQSxvQkFBdUIsR0FBVztBQUNyQyxNQUFJQyxnQkFBZ0JoQyxNQUFNaUMsSUFBTixDQUFXLG1CQUFYLEVBQWdDQyxRQUFoQyxDQUF5Q3JCLFFBQVFMLG9CQUFqRCxDQUFwQjs7QUFFQU4sMkJBQXlCaUMsR0FBekIsQ0FBNkI7QUFDNUJwQyxTQUFNO0FBQ0xxQyxZQUFRdkIsUUFBUXdCLE9BRFg7QUFFTEMsc0JBQWtCekIsUUFBUTBCLE9BQVIsR0FBa0IsV0FGL0I7QUFHTEMsd0JBQW9CUjtBQUhmO0FBRHNCLEdBQTdCO0FBT0EsRUFWRDs7QUFZQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsS0FBSUYseUJBQXlCLFNBQXpCQSxzQkFBeUIsR0FBVztBQUN2QyxNQUFJakIsUUFBUUYsZUFBWixFQUE2QjtBQUM1QlgsU0FBTW1CLE9BQU4sQ0FBY04sUUFBUUYsZUFBdEIsRUFBdUNTLElBQXZDLENBQTRDUCxRQUFRUSxlQUFwRCxFQUFxRW9CLE1BQXJFO0FBQ0EsR0FGRCxNQUdLO0FBQ0p6QyxTQUFNb0IsSUFBTixDQUFXUCxRQUFRUSxlQUFuQixFQUFvQ29CLE1BQXBDO0FBQ0E7O0FBRUR6QyxRQUFNaUMsSUFBTixDQUFXLG1CQUFYLEVBQWdDUyxXQUFoQyxDQUE0QzdCLFFBQVFMLG9CQUFwRDtBQUNBUixRQUFNaUMsSUFBTixDQUFXLG1CQUFYLEVBQWdDUyxXQUFoQyxDQUE0QzdCLFFBQVFKLG1CQUFwRDs7QUFHQXNCO0FBQ0EsRUFiRDs7QUFlQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBakMsUUFBTzZDLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUI3QjtBQUNBRztBQUNBSztBQUNBcUI7QUFDQSxFQUxEOztBQU9BO0FBQ0EsUUFBTzlDLE1BQVA7QUFDQSxDQS9KRiIsImZpbGUiOiJjb2xsYXBzZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGNvbGxhcHNlci5qcyAyMDE2LTAyLTE5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBDb2xsYXBzZXIgV2lkZ2V0XG4gKlxuICogVGhpcyB3aWRnZXQgZXhwYW5kcyBvciBjb2xsYXBzZXMgdGhlIHRhcmdldCBlbGVtZW50LiBJdCBpcyBub3QgdmlzaWJsZSB3aGVuIGNvbGxhcHNlZCBidXQgdmlzaWJsZSB3aGVuIGV4cGFuZGVkLlxuICpcbiAqICMjIyBPcHRpb25zXG4gKlxuICogKipDb2xsYXBzZWQgfCBgZGF0YS1jb2xsYXBzZXItY29sbGFwc2VkYCB8IEJvb2xlYW4gfCBPcHRpb25hbCoqXG4gKlxuICogRGVmYXVsdCBzdGF0ZSBvZiB0aGUgY29sbGFwc2VyLiBJZiBubyB2YWx1ZSBpcyBwcm92aWRlZCwgaXQgZGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAqXG4gKiAqKkNvbGxhcHNlZCBJY29uIENsYXNzIHwgYGRhdGEtY29sbGFwc2VyLWNvbGxhcHNlZF9pY29uX2NsYXNzYCB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqXG4gKiBEZWZhdWx0IEZvbnQgQXdlc29tZSBpY29uIHdoZW4gdGhlIGNvbGxhcHNlciBpcyBjb2xsYXBzZWQuIElmIG5vIHZhbHVlIGlzIHByb3ZpZGVkLCBpdCBkZWZhdWx0cyB0byAqKidmYS1wbHVzLXNxdWFyZS1vJyoqLlxuICpcbiAqICoqRXhwYW5kZWQgSWNvbiBDbGFzcyB8IGBkYXRhLWNvbGxhcHNlci1leHBhbmRlZF9pY29uX2NsYXNzYCB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqXG4gKiBEZWZhdWx0IEZvbnQgQXdlc29tZSBpY29uIHdoZW4gdGhlIGNvbGxhcHNlciBpcyBleHBhbmRlZC4gSWYgbm8gdmFsdWUgaXMgcHJvdmlkZWQsIGl0IGRlZmF1bHRzIHRvICoqJ2ZhLW1pbnVzLXNxdWFyZS1vJyoqLlxuICpcbiAqICoqQWRkaXRpb25hbCBDbGFzc2VzIHwgYGRhdGEtY29sbGFwc2VyLWFkZGl0aW9uYWxfY2xhc3Nlc2AgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogUHJvdmlkZSBhZGRpdGlvbmFsIENTUy1DbGFzc2VzIHdoaWNoIHNob3VsZCBiZSBhZGRlZC4gSWYgbm8gdmFsdWUgaXMgcHJvdmlkZWQsIGl0IGRlZmF1bHRzIHRvICoqJ3B1bGwtcmlnaHQnKiosXG4gKiB3aGljaCBhcHBsaWVzIGEgQ1NTIConZmxvYXQ6IHJpZ2h0Jyogc3R5bGUuXG4gKlxuICogKipUYXJnZXQgU2VsZWN0b3IgfCBgZGF0YS1jb2xsYXBzZXItdGFyZ2V0X3NlbGVjdG9yYCB8IFN0cmluZyB8IFJlcXVpcmVkKipcbiAqXG4gKiBQcm92aWRlIHRoZSB0YXJnZXQgc2VsZWN0b3IsIHdoaWNoIGlzIHRoZSBlbGVtZW50IHRoYXRoIHdpbGwgYmUgY29sbGFwc2VkIG9yIGV4cGFuZGVkLlxuICpcbiAqICoqUGFyZW50IFNlbGVjdG9yIHwgYGRhdGEtY29sbGFwc2VyLXBhcmVudF9zZWxlY3RvcmAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogUHJvdmlkZSBhIHBhcmVudCBzZWxlY3RvciBmb3IgdGhlIGNvbGxhcHNlci4gSXQncyBlbXB0eSBieSBkZWZhdWx0LlxuICpcbiAqICMjIyBFeGFtcGxlXG4gKlxuICogV2hlbiB0aGUgcGFnZSBsb2FkcywgdGhlICoqY29sbGFwc2VyKiogd2lkZ2V0IHdpbGwgYmUgYWRkZWQgdG8gdGhlIGA8ZGl2PmAgZWxlbWVudC5cbiAqIE9uIGNsaWNrLCB0aGUgdGFyZ2V0IHNlbGVjdG9yIHdpbGwgYmUgc2hvd24gb3IgaGlkZGVuLlxuICpcbiAqIGBgYGh0bWxcbiAqICA8ZGl2IGNsYXNzPVwiaGVhZGxpbmUtd3JhcHBlclwiXG4gKiAgICAgIGRhdGEtZ3gtd2lkZ2V0PVwiY29sbGFwc2VyXCJcbiAqICAgICAgZGF0YS1jb2xsYXBzZXItdGFyZ2V0X3NlbGVjdG9yPVwiLmNvbnRlbnQtd3JhcHBlclwiXG4gKiAgICAgIGRhdGEtY29sbGFwc2VyLXNlY3Rpb249XCJjYXRlZ29yeV9iYXNlX2RhdGFcIlxuICogICAgICBkYXRhLWNvbGxhcHNlci11c2VyX2lkPVwiMVwiXG4gKiAgICAgIGRhdGEtY29sbGFwc2VyLWNvbGxhcHNlZD1cInRydWVcIj5cbiAqICAgIENsaWNrIFRoaXMgSGVhZGxpbmVcbiAqICA8L2Rpdj5cbiAqICA8ZGl2IGNsYXNzPVwiY29udGVudC13cmFwcGVyXCI+XG4gKiAgICBUb2dnbGVkIGNvbnRlbnRcbiAqICA8L2Rpdj5cbiAqIGBgYFxuICpcbiAqIEBtb2R1bGUgQWRtaW4vV2lkZ2V0cy9jb2xsYXBzZXJcbiAqXG4gKiBAdG9kbyBNYWtlIHRoZSBzdHlsaW5nIGZvciB0aGlzIHdpZGdldCAobGlrZSBpdCBpcyBvbiB0aGUgcHJvZHVjdHMgc2l0ZSkgbW9yZSBnZW5lcmFsLiBDdXJyZW50bHksIHRoZSBcImRpdlwiIGVsZW1lbnRcbiAqIGhhcyB0byBiZSB3cmFwcGVkIGluIGFub3RoZXIgXCJkaXZcIiB3aXRoIHNwZWNpZmljIENTUy1DbGFzc2VzIGxpa2UgXCIuZ3gtY29udGFpbmVyXCIgYW5kIFwiLmZyYW1lLWhlYWRcIi5cbiAqL1xuZ3gud2lkZ2V0cy5tb2R1bGUoXG5cdCdjb2xsYXBzZXInLFxuXHRcblx0Wyd1c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSddLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIFdpZGdldCBSZWZlcmVuY2Vcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogVXNlckNvbmZpZ3VyYXRpb25TZXJ2aWNlIEFsaWFzXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0dXNlckNvbmZpZ3VyYXRpb25TZXJ2aWNlID0ganNlLmxpYnMudXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UsXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zIGZvciBXaWRnZXRcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0Y29sbGFwc2VkOiBmYWxzZSxcblx0XHRcdFx0Y29sbGFwc2VkX2ljb25fY2xhc3M6ICdmYS1wbHVzLXNxdWFyZS1vJyxcblx0XHRcdFx0ZXhwYW5kZWRfaWNvbl9jbGFzczogJ2ZhLW1pbnVzLXNxdWFyZS1vJyxcblx0XHRcdFx0YWRkaXRpb25hbF9jbGFzc2VzOiAncHVsbC1yaWdodCcsXG5cdFx0XHRcdHBhcmVudF9zZWxlY3RvcjogJycsXG5cdFx0XHRcdGlzX2J1dHRvbl90cmlnZ2VyOiBmYWxzZVxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBXaWRnZXQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBQUklWQVRFIE1FVEhPRFNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTZXRzIHRoZSBjdXJzb3IgdG8gcG9pbnRlclxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9zZXRNb3VzZUN1cnNvclBvaW50ZXIgPSBmdW5jdGlvbigpIHtcblx0XHRcdCR0aGlzLmFkZENsYXNzKCdjdXJzb3ItcG9pbnRlcicpLmNoaWxkcmVuKCkuYWRkQ2xhc3MoJ2N1cnNvci1wb2ludGVyJyk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZXRzIHRoZSBpbml0aWFsIHZpc2liaWxpdHkgYWNjb3JkaW5nIHRvIHRoZSAnY29sbGFwc2VkJyB2YWx1ZVxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9zZXRJbml0aWFsVmlzaWJpbGl0eVN0YXRlID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRpZiAob3B0aW9ucy5jb2xsYXBzZWQpIHtcblx0XHRcdFx0aWYgKG9wdGlvbnMucGFyZW50X3NlbGVjdG9yKSB7XG5cdFx0XHRcdFx0JHRoaXMucGFyZW50cyhvcHRpb25zLnBhcmVudF9zZWxlY3RvcikubmV4dChvcHRpb25zLnRhcmdldF9zZWxlY3RvcikuaGlkZSgpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Uge1xuXHRcdFx0XHRcdCR0aGlzLm5leHQob3B0aW9ucy50YXJnZXRfc2VsZWN0b3IpLmhpZGUoKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlcyB0aGUgbWFya3VwIGZvciB0aGUgY29sbGFwc2VyIGFuZCBhZGRzIHRoZSBjbGljayBldmVudCBoYW5kbGVyXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX2NyZWF0ZUNvbGxhcHNlciA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyICRidXR0b24gPSAkKCc8c3Bhbj4nLCB7Y2xhc3M6IGBjb2xsYXBzZXIgJHtvcHRpb25zLmFkZGl0aW9uYWxfY2xhc3Nlc31gfSk7XG5cdFx0XHR2YXIgJGljb24gPSAkKCc8aT4nLCB7Y2xhc3M6IGBmYSAke29wdGlvbnMuY29sbGFwc2VkID8gb3B0aW9ucy5jb2xsYXBzZWRfaWNvbl9jbGFzcyA6IG9wdGlvbnMuZXhwYW5kZWRfaWNvbl9jbGFzc31gfSk7XG5cblx0XHRcdCRidXR0b25cblx0XHRcdFx0LmFwcGVuZCgkaWNvbilcblx0XHRcdFx0LmFwcGVuZFRvKCR0aGlzKTtcblxuXHRcdFx0aWYgKG9wdGlvbnMuaXNfYnV0dG9uX3RyaWdnZXIpIHtcblx0XHRcdFx0JGJ1dHRvbi5vbignY2xpY2snLCBfdG9nZ2xlVmlzaWJpbGl0eVN0YXRlKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCR0aGlzLm9uKCdjbGljaycsIF90b2dnbGVWaXNpYmlsaXR5U3RhdGUpO1xuXHRcdFx0fVxuXHRcdH07XG5cblx0XHQvKipcblx0XHQgKiBTYXZlcyB0aGUgY3VycmVudCB2aXNpYmlsaXR5IHN0YXRlLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX3NhdmVWaXNpYmlsaXR5U3RhdGUgPSBmdW5jdGlvbigpIHtcblx0XHRcdHZhciBjb2xsYXBzZVN0YXRlID0gJHRoaXMuZmluZCgnLmNvbGxhcHNlciA+IGkuZmEnKS5oYXNDbGFzcyhvcHRpb25zLmNvbGxhcHNlZF9pY29uX2NsYXNzKTtcblx0XHRcdFxuXHRcdFx0dXNlckNvbmZpZ3VyYXRpb25TZXJ2aWNlLnNldCh7XG5cdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHR1c2VySWQ6IG9wdGlvbnMudXNlcl9pZCxcblx0XHRcdFx0XHRjb25maWd1cmF0aW9uS2V5OiBvcHRpb25zLnNlY3Rpb24gKyAnX2NvbGxhcHNlJyxcblx0XHRcdFx0XHRjb25maWd1cmF0aW9uVmFsdWU6IGNvbGxhcHNlU3RhdGVcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFRvZ2dsZXMgdGhlIHZpc2liaWxpdHkgc3RhdGUgYW5kIHN3aXRjaGVzIGJldHdlZW4gcGx1cyBhbmQgbWludXMgaWNvbi5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF90b2dnbGVWaXNpYmlsaXR5U3RhdGUgPSBmdW5jdGlvbigpIHtcblx0XHRcdGlmIChvcHRpb25zLnBhcmVudF9zZWxlY3Rvcikge1xuXHRcdFx0XHQkdGhpcy5wYXJlbnRzKG9wdGlvbnMucGFyZW50X3NlbGVjdG9yKS5uZXh0KG9wdGlvbnMudGFyZ2V0X3NlbGVjdG9yKS50b2dnbGUoKTtcblx0XHRcdH1cblx0XHRcdGVsc2Uge1xuXHRcdFx0XHQkdGhpcy5uZXh0KG9wdGlvbnMudGFyZ2V0X3NlbGVjdG9yKS50b2dnbGUoKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JHRoaXMuZmluZCgnLmNvbGxhcHNlciA+IGkuZmEnKS50b2dnbGVDbGFzcyhvcHRpb25zLmNvbGxhcHNlZF9pY29uX2NsYXNzKTtcblx0XHRcdCR0aGlzLmZpbmQoJy5jb2xsYXBzZXIgPiBpLmZhJykudG9nZ2xlQ2xhc3Mob3B0aW9ucy5leHBhbmRlZF9pY29uX2NsYXNzKTtcblxuXHRcdFx0XG5cdFx0XHRfc2F2ZVZpc2liaWxpdHlTdGF0ZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaRVxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSBtb2R1bGUsIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0X3NldE1vdXNlQ3Vyc29yUG9pbnRlcigpO1xuXHRcdFx0X3NldEluaXRpYWxWaXNpYmlsaXR5U3RhdGUoKTtcblx0XHRcdF9jcmVhdGVDb2xsYXBzZXIoKTtcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIFJldHVybiBkYXRhIHRvIG1vZHVsZSBlbmdpbmVcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
