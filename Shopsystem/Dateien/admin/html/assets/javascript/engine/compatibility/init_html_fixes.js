'use strict';

/* --------------------------------------------------------------
 init_html_fixes.js 2016-02-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Admin HTML Fixes
 *
 * This module will be executed in page load and will perform minor HTML fixes for each pages
 * so that they don't have to be performed manually. Apply this module to the page wrapper element.
 *
 * @module Compatibility/init_html_fixes
 */
gx.compatibility.module('init_html_fixes', ['url_arguments'],

/**  @lends module:Compatibility/init_html_fixes */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Array of callbacks.
  *
  * @type {array}
  */
	fixes = [],


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// HTML FIXES
	// ------------------------------------------------------------------------

	/**
  * Wrap main page content into container.
  */
	fixes.push(function () {
		$('.pageHeading').eq(1).removeClass('pageHeading');
		$('.boxCenter:not(.no-wrap)').wrapInner('<div class="boxCenterWrapper"></div>');
		$('.pageHeading:first-child').prependTo('.boxCenter');
		$('.pageHeading').css('float', 'none');

		var $firstChild = $($('.boxCenterWrapper').children()[0]);

		if ($firstChild.is('br')) {
			$firstChild.remove();
		}

		if ($('div.gx-configuration-box').length) {
			$('.boxCenterWrapper').wrap('<div class="boxCenterAndConfigurationWrapper" style="overflow: auto"></div>');
			$('.boxCenterAndConfigurationWrapper').append($('div.gx-configuration-box'));
		}
	});

	/**
  * Remove unnecessary <br> tag after page wrapper element.
  */
	fixes.push(function () {
		var $nextElement = $this.next(),
		    tagName = $nextElement.prop('tagName');

		if (tagName === 'BR') {
			$nextElement.remove();
		}
	});

	/**
  * Ensure that the left menu parent has the columnLeft2 class because there
  * are some pages where this class is not defined and it will lead to styling issues.
  */
	fixes.push(function () {
		var $columnLeft2 = $('.main-left-menu').parent('td');
		if (!$columnLeft2.hasClass('columnLeft2')) {
			$columnLeft2.addClass('columnLeft2');
		}
	});

	/**
  * Remove width attribute from ".columnLeft2" element.
  */
	fixes.push(function () {
		$('.columnLeft2').removeAttr('width');
	});

	/**
  * Move message stack container to correct place.
  */
	fixes.push(function () {
		var $messageStackContainer = $('.message_stack_container'),
		    $message = $messageStackContainer.find('.alert'),
		    $createNewWrapper = $('.create-new-wrapper');

		if ($('.boxCenterWrapper').length > 0) {
			$messageStackContainer.prependTo('.boxCenterWrapper').show();
		} else {
			$messageStackContainer.prependTo('.order-edit-content').show();
			$messageStackContainer.prependTo('.dashboard-content').show();
		}

		/**
   * Fix if there are more than one message stack container classes.
   * This fix only work, if there are two containers.
   * Improve it if you recognize pages with more than two container classes.
   */
		if ($messageStackContainer.length > 1) {
			$($messageStackContainer[0]).remove();
		}

		if ($message.length > 0 && $createNewWrapper.length > 0) {
			$createNewWrapper.addClass('message-stack-active');
		}
	});

	/**
  * Changing behavior in the orders page.
  */
	fixes.push(function () {
		// Checks if current page is order and return immediately if its not the case
		var isCurrentPage = window.location.href.indexOf('orders.php') > -1;

		if (!isCurrentPage) {
			return;
		}

		// Prepare customer link
		var customerLinkPrefix = window.location.href.replace(window.location.search, '').replace('orders.php', 'customers.php?action=edit&cID=');

		// Do the modifications on the table rows
		var rowsSelectors = ['tr.gx-container.dataTableRowSelected', 'tr.gx-container.dataTableRow'].join(', ');
		var $rows = $this.find(rowsSelectors);

		// Remove the on click event on the entire row add special events
		$rows.each(function (index, element) {
			// Extract order link from element
			var orderLink = $(element).find('td[onclick]:first').attr('onclick');
			if (typeof orderLink !== 'undefined') {
				orderLink.replace('document.location.href="', '').replace('&action=edit', '').slice(0, -1);
			}

			// Customer ID
			var customerId = $(this).find('a[data-customer-id]').data('customerId');

			// Remove onclick attributes from elements
			$(element).find('[onclick]').removeAttr('onclick');
		});
	});

	/**
  * Remove inline class javascript changes.
  */
	fixes.push(function () {
		var selectors = ['.dataTableRow', '.dataTableRowSelected'];

		$.each(selectors, function () {
			$(this).each(function () {
				if ($(this).attr('onmouseover') && $(this).attr('onmouseover').indexOf('this.className') > -1) {
					$(this).removeAttr('onmouseover');
				}

				if ($(this).attr('onmouseout') && $(this).attr('onmouseout').indexOf('this.className') > -1) {
					$(this).removeAttr('onmouseout');
				}
			});
		});
	});

	/**
  * Remove the old markup for editing or creating a new category
  */
	fixes.push(function () {
		$('#old-category-table').remove();
	});

	/**
  * Orders form fix.
  */
	fixes.push(function () {
		var $headingBoxContainer = $('.orders_form');
		$.each($headingBoxContainer.children(), function (index, element) {
			$(element).addClass('hidden');
		});
	});

	/**
  * Fix margins and cell spacing of left menu
  */
	fixes.push(function () {
		$('.columnLeft2').parents('table:first').css({
			'border-spacing': 0
		});
	});

	fixes.push(function () {
		var urlHelper = jse.libs.url_arguments;

		if (urlHelper.getCurrentFile() === 'categories.php') {
			$('.columnLeft2').parents('table:first').css('width', '');
		}
	});

	fixes.push(function () {
		var urlHelper = jse.libs.url_arguments;
		var file = urlHelper.getCurrentFile(),
		    doParameter = urlHelper.getUrlParameters().do || '',
		    largePages = ['gm_emails.php'],
		    smallPages = ['gm_seo_boost.php', 'parcel_services.php'];

		if ($.inArray(file, largePages) > -1 || file === 'admin.php' && $.inArray(doParameter, largePages) > -1) {
			$('.boxCenterWrapper').addClass('breakpoint-large');
		}

		if ($.inArray(file, smallPages) > -1 || file === 'admin.php' && $.inArray(doParameter, smallPages) > -1) {
			$('.boxCenterWrapper').addClass('breakpoint-small');
		}
	});

	/**
  * Helper to add css breakpoint classes to pages which use the controller mechanism.
  * Extend whether the array 'largePages' or 'smallPages' to add the breakpoint class.
  * Add as element the controller name (like in the url behind do=) and the action with trailing slash.
  * (the action is the string in the 'do' argument behind the slash)
  */
	fixes.push(function () {
		var urlHelper = jse.libs.url_arguments,
		    currentFile = urlHelper.getCurrentFile(),
		    controllerAction = urlHelper.getUrlParameters().do,
		    largePages = [],
		    smallPages = ['JanolawModuleCenterModule/Config'];

		if (currentFile === 'admin.php') {

			if ($.inArray(controllerAction, largePages) > -1) {
				$('#container').addClass('breakpoint-large');
			}

			if ($.inArray(controllerAction, smallPages) > -1) {
				$('#container').addClass('breakpoint-small');
			}
		}
	});

	/**
  * Cleans the header of the configuration box from tables
  */
	fixes.push(function () {
		var $contents = $('div.configuration-box-header h2 table.contentTable tr td > *');
		$contents.each(function (index, elem) {
			$('div.configuration-box-header h2').append(elem);
			$('div.configuration-box-header h2').find('table.contentTable').remove();
		});
	});

	/**
  * Convert all the simple checkboxes to the JS Engine widget.
  *
  * This fix will fine-tune the html markup of the checkbox and then it will dynamically
  * initialize the checkbox widget.
  */
	fixes.push(function () {
		var selectors = ['table .categories_view_data input:checkbox', 'table .dataTableHeadingRow td input:checkbox', 'table thead tr th:first input:checkbox', 'table.gx-orders-table tr:not(.dataTableHeadingRow) td:first-child input:checkbox', 'form[name="quantity_units"] input:checkbox', 'form[name="sliderset"] input:checkbox', 'form[name="featurecontrol"] input:checkbox:not(.checkbox-switcher)', '.feature-table tr td:last-child input:checkbox'];

		if ($(selectors).length > 120) {
			return;
		}

		$.each(selectors, function () {
			$(this).each(function () {
				if (!$(this).parent().hasClass('single-checkbox')) {
					$(this).attr('data-single_checkbox', '').parent().attr('data-gx-widget', 'checkbox');
					gx.widgets.init($(this).parent());
				}
			});
		});
	});

	/**
  * Make the top header bar clickable to activate the search bar
  */
	fixes.push(function () {
		var $topHeader = $('.top-header'),
		    $searchInput = $('input[name="admin_search"]');

		$topHeader.on('click', function (event) {
			if ($topHeader.is(event.target)) {
				$searchInput.trigger('click');
			}
		});
	});

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Execute all the existing fixes.
		$.each(fixes, function () {
			this();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluaXRfaHRtbF9maXhlcy5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZml4ZXMiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJwdXNoIiwiZXEiLCJyZW1vdmVDbGFzcyIsIndyYXBJbm5lciIsInByZXBlbmRUbyIsImNzcyIsIiRmaXJzdENoaWxkIiwiY2hpbGRyZW4iLCJpcyIsInJlbW92ZSIsImxlbmd0aCIsIndyYXAiLCJhcHBlbmQiLCIkbmV4dEVsZW1lbnQiLCJuZXh0IiwidGFnTmFtZSIsInByb3AiLCIkY29sdW1uTGVmdDIiLCJwYXJlbnQiLCJoYXNDbGFzcyIsImFkZENsYXNzIiwicmVtb3ZlQXR0ciIsIiRtZXNzYWdlU3RhY2tDb250YWluZXIiLCIkbWVzc2FnZSIsImZpbmQiLCIkY3JlYXRlTmV3V3JhcHBlciIsInNob3ciLCJpc0N1cnJlbnRQYWdlIiwid2luZG93IiwibG9jYXRpb24iLCJocmVmIiwiaW5kZXhPZiIsImN1c3RvbWVyTGlua1ByZWZpeCIsInJlcGxhY2UiLCJzZWFyY2giLCJyb3dzU2VsZWN0b3JzIiwiam9pbiIsIiRyb3dzIiwiZWFjaCIsImluZGV4IiwiZWxlbWVudCIsIm9yZGVyTGluayIsImF0dHIiLCJzbGljZSIsImN1c3RvbWVySWQiLCJzZWxlY3RvcnMiLCIkaGVhZGluZ0JveENvbnRhaW5lciIsInBhcmVudHMiLCJ1cmxIZWxwZXIiLCJqc2UiLCJsaWJzIiwidXJsX2FyZ3VtZW50cyIsImdldEN1cnJlbnRGaWxlIiwiZmlsZSIsImRvUGFyYW1ldGVyIiwiZ2V0VXJsUGFyYW1ldGVycyIsImRvIiwibGFyZ2VQYWdlcyIsInNtYWxsUGFnZXMiLCJpbkFycmF5IiwiY3VycmVudEZpbGUiLCJjb250cm9sbGVyQWN0aW9uIiwiJGNvbnRlbnRzIiwiZWxlbSIsIndpZGdldHMiLCJpbml0IiwiJHRvcEhlYWRlciIsIiRzZWFyY2hJbnB1dCIsIm9uIiwiZXZlbnQiLCJ0YXJnZXQiLCJ0cmlnZ2VyIiwiZG9uZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7OztBQVFBQSxHQUFHQyxhQUFILENBQWlCQyxNQUFqQixDQUNDLGlCQURELEVBR0MsQ0FBQyxlQUFELENBSEQ7O0FBS0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFNBQVEsRUFiVDs7O0FBZUM7Ozs7O0FBS0FDLFlBQVcsRUFwQlo7OztBQXNCQzs7Ozs7QUFLQUMsV0FBVUgsRUFBRUksTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkosSUFBN0IsQ0EzQlg7OztBQTZCQzs7Ozs7QUFLQUQsVUFBUyxFQWxDVjs7QUFvQ0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQUksT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckJMLElBQUUsY0FBRixFQUFrQk0sRUFBbEIsQ0FBcUIsQ0FBckIsRUFBd0JDLFdBQXhCLENBQW9DLGFBQXBDO0FBQ0FQLElBQUUsMEJBQUYsRUFBOEJRLFNBQTlCLENBQXdDLHNDQUF4QztBQUNBUixJQUFFLDBCQUFGLEVBQThCUyxTQUE5QixDQUF3QyxZQUF4QztBQUNBVCxJQUFFLGNBQUYsRUFBa0JVLEdBQWxCLENBQXNCLE9BQXRCLEVBQStCLE1BQS9COztBQUVBLE1BQUlDLGNBQWNYLEVBQUVBLEVBQUUsbUJBQUYsRUFBdUJZLFFBQXZCLEdBQWtDLENBQWxDLENBQUYsQ0FBbEI7O0FBRUEsTUFBSUQsWUFBWUUsRUFBWixDQUFlLElBQWYsQ0FBSixFQUEwQjtBQUN6QkYsZUFBWUcsTUFBWjtBQUNBOztBQUVELE1BQUlkLEVBQUUsMEJBQUYsRUFBOEJlLE1BQWxDLEVBQTBDO0FBQ3pDZixLQUFFLG1CQUFGLEVBQXVCZ0IsSUFBdkIsQ0FBNEIsNkVBQTVCO0FBQ0FoQixLQUFFLG1DQUFGLEVBQXVDaUIsTUFBdkMsQ0FBOENqQixFQUFFLDBCQUFGLENBQTlDO0FBQ0E7QUFDRCxFQWhCRDs7QUFrQkE7OztBQUdBQyxPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQixNQUFJYSxlQUFlbkIsTUFBTW9CLElBQU4sRUFBbkI7QUFBQSxNQUNDQyxVQUFVRixhQUFhRyxJQUFiLENBQWtCLFNBQWxCLENBRFg7O0FBR0EsTUFBSUQsWUFBWSxJQUFoQixFQUFzQjtBQUNyQkYsZ0JBQWFKLE1BQWI7QUFDQTtBQUNELEVBUEQ7O0FBU0E7Ozs7QUFJQWIsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSWlCLGVBQWV0QixFQUFFLGlCQUFGLEVBQXFCdUIsTUFBckIsQ0FBNEIsSUFBNUIsQ0FBbkI7QUFDQSxNQUFJLENBQUNELGFBQWFFLFFBQWIsQ0FBc0IsYUFBdEIsQ0FBTCxFQUEyQztBQUMxQ0YsZ0JBQWFHLFFBQWIsQ0FBc0IsYUFBdEI7QUFDQTtBQUNELEVBTEQ7O0FBT0E7OztBQUdBeEIsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckJMLElBQUUsY0FBRixFQUFrQjBCLFVBQWxCLENBQTZCLE9BQTdCO0FBQ0EsRUFGRDs7QUFJQTs7O0FBR0F6QixPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQixNQUFJc0IseUJBQXlCM0IsRUFBRSwwQkFBRixDQUE3QjtBQUFBLE1BQ0M0QixXQUFXRCx1QkFBdUJFLElBQXZCLENBQTRCLFFBQTVCLENBRFo7QUFBQSxNQUVDQyxvQkFBb0I5QixFQUFFLHFCQUFGLENBRnJCOztBQUlBLE1BQUlBLEVBQUUsbUJBQUYsRUFBdUJlLE1BQXZCLEdBQWdDLENBQXBDLEVBQXVDO0FBQ3RDWSwwQkFBdUJsQixTQUF2QixDQUFpQyxtQkFBakMsRUFBc0RzQixJQUF0RDtBQUNBLEdBRkQsTUFFTztBQUNOSiwwQkFBdUJsQixTQUF2QixDQUFpQyxxQkFBakMsRUFBd0RzQixJQUF4RDtBQUNBSiwwQkFBdUJsQixTQUF2QixDQUFpQyxvQkFBakMsRUFBdURzQixJQUF2RDtBQUNBOztBQUVEOzs7OztBQUtBLE1BQUlKLHVCQUF1QlosTUFBdkIsR0FBZ0MsQ0FBcEMsRUFBdUM7QUFDdENmLEtBQUUyQix1QkFBdUIsQ0FBdkIsQ0FBRixFQUE2QmIsTUFBN0I7QUFDQTs7QUFFRCxNQUFJYyxTQUFTYixNQUFULEdBQWtCLENBQWxCLElBQXVCZSxrQkFBa0JmLE1BQWxCLEdBQTJCLENBQXRELEVBQXlEO0FBQ3hEZSxxQkFBa0JMLFFBQWxCLENBQTJCLHNCQUEzQjtBQUNBO0FBQ0QsRUF4QkQ7O0FBMEJBOzs7QUFHQXhCLE9BQU1JLElBQU4sQ0FBVyxZQUFXO0FBQ3JCO0FBQ0EsTUFBSTJCLGdCQUFpQkMsT0FBT0MsUUFBUCxDQUFnQkMsSUFBaEIsQ0FBcUJDLE9BQXJCLENBQTZCLFlBQTdCLElBQTZDLENBQUMsQ0FBbkU7O0FBRUEsTUFBSSxDQUFDSixhQUFMLEVBQW9CO0FBQ25CO0FBQ0E7O0FBRUQ7QUFDQSxNQUFJSyxxQkFBcUJKLE9BQU9DLFFBQVAsQ0FBZ0JDLElBQWhCLENBQ3ZCRyxPQUR1QixDQUNmTCxPQUFPQyxRQUFQLENBQWdCSyxNQURELEVBQ1MsRUFEVCxFQUV2QkQsT0FGdUIsQ0FFZixZQUZlLEVBRUQsZ0NBRkMsQ0FBekI7O0FBSUE7QUFDQSxNQUFJRSxnQkFBZ0IsQ0FDbkIsc0NBRG1CLEVBRW5CLDhCQUZtQixFQUdsQkMsSUFIa0IsQ0FHYixJQUhhLENBQXBCO0FBSUEsTUFBSUMsUUFBUTNDLE1BQU04QixJQUFOLENBQVdXLGFBQVgsQ0FBWjs7QUFFQTtBQUNBRSxRQUFNQyxJQUFOLENBQVcsVUFBU0MsS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUI7QUFDbkM7QUFDQSxPQUFJQyxZQUFZOUMsRUFBRTZDLE9BQUYsRUFDZGhCLElBRGMsQ0FDVCxtQkFEUyxFQUVka0IsSUFGYyxDQUVULFNBRlMsQ0FBaEI7QUFHQSxPQUFJLE9BQU9ELFNBQVAsS0FBcUIsV0FBekIsRUFBc0M7QUFDckNBLGNBQ0VSLE9BREYsQ0FDVSwwQkFEVixFQUNzQyxFQUR0QyxFQUVFQSxPQUZGLENBRVUsY0FGVixFQUUwQixFQUYxQixFQUdFVSxLQUhGLENBR1EsQ0FIUixFQUdXLENBQUMsQ0FIWjtBQUlBOztBQUVEO0FBQ0EsT0FBSUMsYUFBYWpELEVBQUUsSUFBRixFQUNmNkIsSUFEZSxDQUNWLHFCQURVLEVBRWYvQixJQUZlLENBRVYsWUFGVSxDQUFqQjs7QUFJQTtBQUNBRSxLQUFFNkMsT0FBRixFQUNFaEIsSUFERixDQUNPLFdBRFAsRUFFRUgsVUFGRixDQUVhLFNBRmI7QUFHQSxHQXJCRDtBQXNCQSxFQTNDRDs7QUE2Q0E7OztBQUdBekIsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSTZDLFlBQVksQ0FDZixlQURlLEVBRWYsdUJBRmUsQ0FBaEI7O0FBS0FsRCxJQUFFMkMsSUFBRixDQUFPTyxTQUFQLEVBQWtCLFlBQVc7QUFDNUJsRCxLQUFFLElBQUYsRUFBUTJDLElBQVIsQ0FBYSxZQUFXO0FBQ3ZCLFFBQUkzQyxFQUFFLElBQUYsRUFBUStDLElBQVIsQ0FBYSxhQUFiLEtBQStCL0MsRUFBRSxJQUFGLEVBQVErQyxJQUFSLENBQWEsYUFBYixFQUE0QlgsT0FBNUIsQ0FBb0MsZ0JBQXBDLElBQXdELENBQUMsQ0FBNUYsRUFBK0Y7QUFDOUZwQyxPQUFFLElBQUYsRUFBUTBCLFVBQVIsQ0FBbUIsYUFBbkI7QUFDQTs7QUFFRCxRQUFJMUIsRUFBRSxJQUFGLEVBQVErQyxJQUFSLENBQWEsWUFBYixLQUE4Qi9DLEVBQUUsSUFBRixFQUFRK0MsSUFBUixDQUFhLFlBQWIsRUFBMkJYLE9BQTNCLENBQW1DLGdCQUFuQyxJQUF1RCxDQUFDLENBQTFGLEVBQTZGO0FBQzVGcEMsT0FBRSxJQUFGLEVBQVEwQixVQUFSLENBQW1CLFlBQW5CO0FBQ0E7QUFDRCxJQVJEO0FBU0EsR0FWRDtBQVdBLEVBakJEOztBQW1CQTs7O0FBR0F6QixPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQkwsSUFBRSxxQkFBRixFQUF5QmMsTUFBekI7QUFDQSxFQUZEOztBQUlBOzs7QUFHQWIsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSThDLHVCQUF1Qm5ELEVBQUUsY0FBRixDQUEzQjtBQUNBQSxJQUFFMkMsSUFBRixDQUFPUSxxQkFBcUJ2QyxRQUFyQixFQUFQLEVBQXdDLFVBQVNnQyxLQUFULEVBQWdCQyxPQUFoQixFQUF5QjtBQUNoRTdDLEtBQUU2QyxPQUFGLEVBQVdwQixRQUFYLENBQW9CLFFBQXBCO0FBQ0EsR0FGRDtBQUdBLEVBTEQ7O0FBT0E7OztBQUdBeEIsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckJMLElBQUUsY0FBRixFQUNFb0QsT0FERixDQUNVLGFBRFYsRUFFRTFDLEdBRkYsQ0FFTTtBQUNKLHFCQUFrQjtBQURkLEdBRk47QUFLQSxFQU5EOztBQVFBVCxPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQixNQUFJZ0QsWUFBWUMsSUFBSUMsSUFBSixDQUFTQyxhQUF6Qjs7QUFFQSxNQUFJSCxVQUFVSSxjQUFWLE9BQStCLGdCQUFuQyxFQUFxRDtBQUNwRHpELEtBQUUsY0FBRixFQUNFb0QsT0FERixDQUNVLGFBRFYsRUFFRTFDLEdBRkYsQ0FFTSxPQUZOLEVBRWUsRUFGZjtBQUdBO0FBQ0QsRUFSRDs7QUFVQVQsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSWdELFlBQVlDLElBQUlDLElBQUosQ0FBU0MsYUFBekI7QUFDQSxNQUFJRSxPQUFPTCxVQUFVSSxjQUFWLEVBQVg7QUFBQSxNQUNDRSxjQUFjTixVQUFVTyxnQkFBVixHQUE2QkMsRUFBN0IsSUFBbUMsRUFEbEQ7QUFBQSxNQUVDQyxhQUFhLENBQ1osZUFEWSxDQUZkO0FBQUEsTUFLQ0MsYUFBYSxDQUNaLGtCQURZLEVBRVoscUJBRlksQ0FMZDs7QUFVQSxNQUFJL0QsRUFBRWdFLE9BQUYsQ0FBVU4sSUFBVixFQUFnQkksVUFBaEIsSUFBOEIsQ0FBQyxDQUEvQixJQUNDSixTQUFTLFdBQVQsSUFBd0IxRCxFQUFFZ0UsT0FBRixDQUFVTCxXQUFWLEVBQXVCRyxVQUF2QixJQUFxQyxDQUFDLENBRG5FLEVBQ3VFO0FBQ3RFOUQsS0FBRSxtQkFBRixFQUNFeUIsUUFERixDQUNXLGtCQURYO0FBRUE7O0FBRUQsTUFBSXpCLEVBQUVnRSxPQUFGLENBQVVOLElBQVYsRUFBZ0JLLFVBQWhCLElBQThCLENBQUMsQ0FBL0IsSUFDQ0wsU0FBUyxXQUFULElBQXdCMUQsRUFBRWdFLE9BQUYsQ0FBVUwsV0FBVixFQUF1QkksVUFBdkIsSUFBcUMsQ0FBQyxDQURuRSxFQUN1RTtBQUN0RS9ELEtBQUUsbUJBQUYsRUFDRXlCLFFBREYsQ0FDVyxrQkFEWDtBQUVBO0FBQ0QsRUF2QkQ7O0FBeUJBOzs7Ozs7QUFNQXhCLE9BQU1JLElBQU4sQ0FBVyxZQUFXO0FBQ3JCLE1BQUlnRCxZQUFZQyxJQUFJQyxJQUFKLENBQVNDLGFBQXpCO0FBQUEsTUFDQ1MsY0FBY1osVUFBVUksY0FBVixFQURmO0FBQUEsTUFFQ1MsbUJBQW1CYixVQUFVTyxnQkFBVixHQUE2QkMsRUFGakQ7QUFBQSxNQUdDQyxhQUFhLEVBSGQ7QUFBQSxNQUlDQyxhQUFhLENBQUMsa0NBQUQsQ0FKZDs7QUFNQSxNQUFJRSxnQkFBZ0IsV0FBcEIsRUFBaUM7O0FBRWhDLE9BQUlqRSxFQUFFZ0UsT0FBRixDQUFVRSxnQkFBVixFQUE0QkosVUFBNUIsSUFBMEMsQ0FBQyxDQUEvQyxFQUFrRDtBQUNqRDlELE1BQUUsWUFBRixFQUNFeUIsUUFERixDQUNXLGtCQURYO0FBRUE7O0FBRUQsT0FBSXpCLEVBQUVnRSxPQUFGLENBQVVFLGdCQUFWLEVBQTRCSCxVQUE1QixJQUEwQyxDQUFDLENBQS9DLEVBQWtEO0FBQ2pEL0QsTUFBRSxZQUFGLEVBQ0V5QixRQURGLENBQ1csa0JBRFg7QUFFQTtBQUNEO0FBQ0QsRUFuQkQ7O0FBcUJBOzs7QUFHQXhCLE9BQU1JLElBQU4sQ0FBVyxZQUFXO0FBQ3JCLE1BQUk4RCxZQUFZbkUsRUFBRSw4REFBRixDQUFoQjtBQUNBbUUsWUFBVXhCLElBQVYsQ0FBZSxVQUFTQyxLQUFULEVBQWdCd0IsSUFBaEIsRUFBc0I7QUFDcENwRSxLQUFFLGlDQUFGLEVBQXFDaUIsTUFBckMsQ0FBNENtRCxJQUE1QztBQUNBcEUsS0FBRSxpQ0FBRixFQUFxQzZCLElBQXJDLENBQTBDLG9CQUExQyxFQUFnRWYsTUFBaEU7QUFDQSxHQUhEO0FBSUEsRUFORDs7QUFRQTs7Ozs7O0FBTUFiLE9BQU1JLElBQU4sQ0FBVyxZQUFXO0FBQ3JCLE1BQUk2QyxZQUFZLENBQ2YsNENBRGUsRUFFZiw4Q0FGZSxFQUdmLHdDQUhlLEVBSWYsa0ZBSmUsRUFLZiw0Q0FMZSxFQU1mLHVDQU5lLEVBT2Ysb0VBUGUsRUFRZixnREFSZSxDQUFoQjs7QUFXQSxNQUFJbEQsRUFBRWtELFNBQUYsRUFBYW5DLE1BQWIsR0FBc0IsR0FBMUIsRUFBK0I7QUFDOUI7QUFDQTs7QUFFRGYsSUFBRTJDLElBQUYsQ0FBT08sU0FBUCxFQUFrQixZQUFXO0FBQzVCbEQsS0FBRSxJQUFGLEVBQVEyQyxJQUFSLENBQWEsWUFBVztBQUN2QixRQUFJLENBQUMzQyxFQUFFLElBQUYsRUFBUXVCLE1BQVIsR0FBaUJDLFFBQWpCLENBQTBCLGlCQUExQixDQUFMLEVBQW1EO0FBQ2xEeEIsT0FBRSxJQUFGLEVBQ0UrQyxJQURGLENBQ08sc0JBRFAsRUFDK0IsRUFEL0IsRUFFRXhCLE1BRkYsR0FFV3dCLElBRlgsQ0FFZ0IsZ0JBRmhCLEVBRWtDLFVBRmxDO0FBR0FwRCxRQUFHMEUsT0FBSCxDQUFXQyxJQUFYLENBQWdCdEUsRUFBRSxJQUFGLEVBQVF1QixNQUFSLEVBQWhCO0FBQ0E7QUFDRCxJQVBEO0FBUUEsR0FURDtBQVVBLEVBMUJEOztBQTRCQTs7O0FBR0F0QixPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQixNQUFJa0UsYUFBYXZFLEVBQUUsYUFBRixDQUFqQjtBQUFBLE1BQ0N3RSxlQUFleEUsRUFBRSw0QkFBRixDQURoQjs7QUFHQXVFLGFBQVdFLEVBQVgsQ0FBYyxPQUFkLEVBQXVCLFVBQVNDLEtBQVQsRUFBZ0I7QUFDdEMsT0FBSUgsV0FBVzFELEVBQVgsQ0FBYzZELE1BQU1DLE1BQXBCLENBQUosRUFBaUM7QUFDaENILGlCQUFhSSxPQUFiLENBQXFCLE9BQXJCO0FBQ0E7QUFDRCxHQUpEO0FBTUEsRUFWRDs7QUFZQTtBQUNBO0FBQ0E7O0FBRUEvRSxRQUFPeUUsSUFBUCxHQUFjLFVBQVNPLElBQVQsRUFBZTtBQUM1QjtBQUNBN0UsSUFBRTJDLElBQUYsQ0FBTzFDLEtBQVAsRUFBYyxZQUFXO0FBQ3hCO0FBQ0EsR0FGRDs7QUFJQTRFO0FBQ0EsRUFQRDs7QUFTQSxRQUFPaEYsTUFBUDtBQUNBLENBalhGIiwiZmlsZSI6ImluaXRfaHRtbF9maXhlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gaW5pdF9odG1sX2ZpeGVzLmpzIDIwMTYtMDItMTBcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIEFkbWluIEhUTUwgRml4ZXNcbiAqXG4gKiBUaGlzIG1vZHVsZSB3aWxsIGJlIGV4ZWN1dGVkIGluIHBhZ2UgbG9hZCBhbmQgd2lsbCBwZXJmb3JtIG1pbm9yIEhUTUwgZml4ZXMgZm9yIGVhY2ggcGFnZXNcbiAqIHNvIHRoYXQgdGhleSBkb24ndCBoYXZlIHRvIGJlIHBlcmZvcm1lZCBtYW51YWxseS4gQXBwbHkgdGhpcyBtb2R1bGUgdG8gdGhlIHBhZ2Ugd3JhcHBlciBlbGVtZW50LlxuICpcbiAqIEBtb2R1bGUgQ29tcGF0aWJpbGl0eS9pbml0X2h0bWxfZml4ZXNcbiAqL1xuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdpbml0X2h0bWxfZml4ZXMnLFxuXHRcblx0Wyd1cmxfYXJndW1lbnRzJ10sXG5cdFxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9pbml0X2h0bWxfZml4ZXMgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogQXJyYXkgb2YgY2FsbGJhY2tzLlxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHthcnJheX1cblx0XHRcdCAqL1xuXHRcdFx0Zml4ZXMgPSBbXSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBIVE1MIEZJWEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogV3JhcCBtYWluIHBhZ2UgY29udGVudCBpbnRvIGNvbnRhaW5lci5cblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0JCgnLnBhZ2VIZWFkaW5nJykuZXEoMSkucmVtb3ZlQ2xhc3MoJ3BhZ2VIZWFkaW5nJyk7XG5cdFx0XHQkKCcuYm94Q2VudGVyOm5vdCgubm8td3JhcCknKS53cmFwSW5uZXIoJzxkaXYgY2xhc3M9XCJib3hDZW50ZXJXcmFwcGVyXCI+PC9kaXY+Jyk7XG5cdFx0XHQkKCcucGFnZUhlYWRpbmc6Zmlyc3QtY2hpbGQnKS5wcmVwZW5kVG8oJy5ib3hDZW50ZXInKTtcblx0XHRcdCQoJy5wYWdlSGVhZGluZycpLmNzcygnZmxvYXQnLCAnbm9uZScpO1xuXHRcdFx0XG5cdFx0XHR2YXIgJGZpcnN0Q2hpbGQgPSAkKCQoJy5ib3hDZW50ZXJXcmFwcGVyJykuY2hpbGRyZW4oKVswXSk7XG5cdFx0XHRcblx0XHRcdGlmICgkZmlyc3RDaGlsZC5pcygnYnInKSkge1xuXHRcdFx0XHQkZmlyc3RDaGlsZC5yZW1vdmUoKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKCQoJ2Rpdi5neC1jb25maWd1cmF0aW9uLWJveCcpLmxlbmd0aCkge1xuXHRcdFx0XHQkKCcuYm94Q2VudGVyV3JhcHBlcicpLndyYXAoJzxkaXYgY2xhc3M9XCJib3hDZW50ZXJBbmRDb25maWd1cmF0aW9uV3JhcHBlclwiIHN0eWxlPVwib3ZlcmZsb3c6IGF1dG9cIj48L2Rpdj4nKTtcblx0XHRcdFx0JCgnLmJveENlbnRlckFuZENvbmZpZ3VyYXRpb25XcmFwcGVyJykuYXBwZW5kKCQoJ2Rpdi5neC1jb25maWd1cmF0aW9uLWJveCcpKTtcblx0XHRcdH1cblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBSZW1vdmUgdW5uZWNlc3NhcnkgPGJyPiB0YWcgYWZ0ZXIgcGFnZSB3cmFwcGVyIGVsZW1lbnQuXG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdHZhciAkbmV4dEVsZW1lbnQgPSAkdGhpcy5uZXh0KCksXG5cdFx0XHRcdHRhZ05hbWUgPSAkbmV4dEVsZW1lbnQucHJvcCgndGFnTmFtZScpO1xuXHRcdFx0XG5cdFx0XHRpZiAodGFnTmFtZSA9PT0gJ0JSJykge1xuXHRcdFx0XHQkbmV4dEVsZW1lbnQucmVtb3ZlKCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRW5zdXJlIHRoYXQgdGhlIGxlZnQgbWVudSBwYXJlbnQgaGFzIHRoZSBjb2x1bW5MZWZ0MiBjbGFzcyBiZWNhdXNlIHRoZXJlXG5cdFx0ICogYXJlIHNvbWUgcGFnZXMgd2hlcmUgdGhpcyBjbGFzcyBpcyBub3QgZGVmaW5lZCBhbmQgaXQgd2lsbCBsZWFkIHRvIHN0eWxpbmcgaXNzdWVzLlxuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJGNvbHVtbkxlZnQyID0gJCgnLm1haW4tbGVmdC1tZW51JykucGFyZW50KCd0ZCcpO1xuXHRcdFx0aWYgKCEkY29sdW1uTGVmdDIuaGFzQ2xhc3MoJ2NvbHVtbkxlZnQyJykpIHtcblx0XHRcdFx0JGNvbHVtbkxlZnQyLmFkZENsYXNzKCdjb2x1bW5MZWZ0MicpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlbW92ZSB3aWR0aCBhdHRyaWJ1dGUgZnJvbSBcIi5jb2x1bW5MZWZ0MlwiIGVsZW1lbnQuXG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdCQoJy5jb2x1bW5MZWZ0MicpLnJlbW92ZUF0dHIoJ3dpZHRoJyk7XG5cdFx0fSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW92ZSBtZXNzYWdlIHN0YWNrIGNvbnRhaW5lciB0byBjb3JyZWN0IHBsYWNlLlxuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJG1lc3NhZ2VTdGFja0NvbnRhaW5lciA9ICQoJy5tZXNzYWdlX3N0YWNrX2NvbnRhaW5lcicpLFxuXHRcdFx0XHQkbWVzc2FnZSA9ICRtZXNzYWdlU3RhY2tDb250YWluZXIuZmluZCgnLmFsZXJ0JyksXG5cdFx0XHRcdCRjcmVhdGVOZXdXcmFwcGVyID0gJCgnLmNyZWF0ZS1uZXctd3JhcHBlcicpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJCgnLmJveENlbnRlcldyYXBwZXInKS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdCRtZXNzYWdlU3RhY2tDb250YWluZXIucHJlcGVuZFRvKCcuYm94Q2VudGVyV3JhcHBlcicpLnNob3coKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCRtZXNzYWdlU3RhY2tDb250YWluZXIucHJlcGVuZFRvKCcub3JkZXItZWRpdC1jb250ZW50Jykuc2hvdygpO1xuXHRcdFx0XHQkbWVzc2FnZVN0YWNrQ29udGFpbmVyLnByZXBlbmRUbygnLmRhc2hib2FyZC1jb250ZW50Jykuc2hvdygpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpeCBpZiB0aGVyZSBhcmUgbW9yZSB0aGFuIG9uZSBtZXNzYWdlIHN0YWNrIGNvbnRhaW5lciBjbGFzc2VzLlxuXHRcdFx0ICogVGhpcyBmaXggb25seSB3b3JrLCBpZiB0aGVyZSBhcmUgdHdvIGNvbnRhaW5lcnMuXG5cdFx0XHQgKiBJbXByb3ZlIGl0IGlmIHlvdSByZWNvZ25pemUgcGFnZXMgd2l0aCBtb3JlIHRoYW4gdHdvIGNvbnRhaW5lciBjbGFzc2VzLlxuXHRcdFx0ICovXG5cdFx0XHRpZiAoJG1lc3NhZ2VTdGFja0NvbnRhaW5lci5sZW5ndGggPiAxKSB7XG5cdFx0XHRcdCQoJG1lc3NhZ2VTdGFja0NvbnRhaW5lclswXSkucmVtb3ZlKCk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmICgkbWVzc2FnZS5sZW5ndGggPiAwICYmICRjcmVhdGVOZXdXcmFwcGVyLmxlbmd0aCA+IDApIHtcblx0XHRcdFx0JGNyZWF0ZU5ld1dyYXBwZXIuYWRkQ2xhc3MoJ21lc3NhZ2Utc3RhY2stYWN0aXZlJyk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2hhbmdpbmcgYmVoYXZpb3IgaW4gdGhlIG9yZGVycyBwYWdlLlxuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHQvLyBDaGVja3MgaWYgY3VycmVudCBwYWdlIGlzIG9yZGVyIGFuZCByZXR1cm4gaW1tZWRpYXRlbHkgaWYgaXRzIG5vdCB0aGUgY2FzZVxuXHRcdFx0dmFyIGlzQ3VycmVudFBhZ2UgPSAod2luZG93LmxvY2F0aW9uLmhyZWYuaW5kZXhPZignb3JkZXJzLnBocCcpID4gLTEpO1xuXHRcdFx0XG5cdFx0XHRpZiAoIWlzQ3VycmVudFBhZ2UpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBQcmVwYXJlIGN1c3RvbWVyIGxpbmtcblx0XHRcdHZhciBjdXN0b21lckxpbmtQcmVmaXggPSB3aW5kb3cubG9jYXRpb24uaHJlZlxuXHRcdFx0XHQucmVwbGFjZSh3aW5kb3cubG9jYXRpb24uc2VhcmNoLCAnJylcblx0XHRcdFx0LnJlcGxhY2UoJ29yZGVycy5waHAnLCAnY3VzdG9tZXJzLnBocD9hY3Rpb249ZWRpdCZjSUQ9Jyk7XG5cdFx0XHRcblx0XHRcdC8vIERvIHRoZSBtb2RpZmljYXRpb25zIG9uIHRoZSB0YWJsZSByb3dzXG5cdFx0XHR2YXIgcm93c1NlbGVjdG9ycyA9IFtcblx0XHRcdFx0J3RyLmd4LWNvbnRhaW5lci5kYXRhVGFibGVSb3dTZWxlY3RlZCcsXG5cdFx0XHRcdCd0ci5neC1jb250YWluZXIuZGF0YVRhYmxlUm93J1xuXHRcdFx0XS5qb2luKCcsICcpO1xuXHRcdFx0dmFyICRyb3dzID0gJHRoaXMuZmluZChyb3dzU2VsZWN0b3JzKTtcblx0XHRcdFxuXHRcdFx0Ly8gUmVtb3ZlIHRoZSBvbiBjbGljayBldmVudCBvbiB0aGUgZW50aXJlIHJvdyBhZGQgc3BlY2lhbCBldmVudHNcblx0XHRcdCRyb3dzLmVhY2goZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcblx0XHRcdFx0Ly8gRXh0cmFjdCBvcmRlciBsaW5rIGZyb20gZWxlbWVudFxuXHRcdFx0XHR2YXIgb3JkZXJMaW5rID0gJChlbGVtZW50KVxuXHRcdFx0XHRcdC5maW5kKCd0ZFtvbmNsaWNrXTpmaXJzdCcpXG5cdFx0XHRcdFx0LmF0dHIoJ29uY2xpY2snKTtcblx0XHRcdFx0aWYgKHR5cGVvZiBvcmRlckxpbmsgIT09ICd1bmRlZmluZWQnKSB7XG5cdFx0XHRcdFx0b3JkZXJMaW5rXG5cdFx0XHRcdFx0XHQucmVwbGFjZSgnZG9jdW1lbnQubG9jYXRpb24uaHJlZj1cIicsICcnKVxuXHRcdFx0XHRcdFx0LnJlcGxhY2UoJyZhY3Rpb249ZWRpdCcsICcnKVxuXHRcdFx0XHRcdFx0LnNsaWNlKDAsIC0xKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gQ3VzdG9tZXIgSURcblx0XHRcdFx0dmFyIGN1c3RvbWVySWQgPSAkKHRoaXMpXG5cdFx0XHRcdFx0LmZpbmQoJ2FbZGF0YS1jdXN0b21lci1pZF0nKVxuXHRcdFx0XHRcdC5kYXRhKCdjdXN0b21lcklkJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBSZW1vdmUgb25jbGljayBhdHRyaWJ1dGVzIGZyb20gZWxlbWVudHNcblx0XHRcdFx0JChlbGVtZW50KVxuXHRcdFx0XHRcdC5maW5kKCdbb25jbGlja10nKVxuXHRcdFx0XHRcdC5yZW1vdmVBdHRyKCdvbmNsaWNrJyk7XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBSZW1vdmUgaW5saW5lIGNsYXNzIGphdmFzY3JpcHQgY2hhbmdlcy5cblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIHNlbGVjdG9ycyA9IFtcblx0XHRcdFx0Jy5kYXRhVGFibGVSb3cnLFxuXHRcdFx0XHQnLmRhdGFUYWJsZVJvd1NlbGVjdGVkJ1xuXHRcdFx0XTtcblx0XHRcdFxuXHRcdFx0JC5lYWNoKHNlbGVjdG9ycywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCQodGhpcykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRpZiAoJCh0aGlzKS5hdHRyKCdvbm1vdXNlb3ZlcicpICYmICQodGhpcykuYXR0cignb25tb3VzZW92ZXInKS5pbmRleE9mKCd0aGlzLmNsYXNzTmFtZScpID4gLTEpIHtcblx0XHRcdFx0XHRcdCQodGhpcykucmVtb3ZlQXR0cignb25tb3VzZW92ZXInKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAoJCh0aGlzKS5hdHRyKCdvbm1vdXNlb3V0JykgJiYgJCh0aGlzKS5hdHRyKCdvbm1vdXNlb3V0JykuaW5kZXhPZigndGhpcy5jbGFzc05hbWUnKSA+IC0xKSB7XG5cdFx0XHRcdFx0XHQkKHRoaXMpLnJlbW92ZUF0dHIoJ29ubW91c2VvdXQnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmVtb3ZlIHRoZSBvbGQgbWFya3VwIGZvciBlZGl0aW5nIG9yIGNyZWF0aW5nIGEgbmV3IGNhdGVnb3J5XG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdCQoJyNvbGQtY2F0ZWdvcnktdGFibGUnKS5yZW1vdmUoKTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBPcmRlcnMgZm9ybSBmaXguXG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdHZhciAkaGVhZGluZ0JveENvbnRhaW5lciA9ICQoJy5vcmRlcnNfZm9ybScpO1xuXHRcdFx0JC5lYWNoKCRoZWFkaW5nQm94Q29udGFpbmVyLmNoaWxkcmVuKCksIGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KSB7XG5cdFx0XHRcdCQoZWxlbWVudCkuYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRml4IG1hcmdpbnMgYW5kIGNlbGwgc3BhY2luZyBvZiBsZWZ0IG1lbnVcblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0JCgnLmNvbHVtbkxlZnQyJylcblx0XHRcdFx0LnBhcmVudHMoJ3RhYmxlOmZpcnN0Jylcblx0XHRcdFx0LmNzcyh7XG5cdFx0XHRcdFx0J2JvcmRlci1zcGFjaW5nJzogMFxuXHRcdFx0XHR9KTtcblx0XHR9KTtcblx0XHRcblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIHVybEhlbHBlciA9IGpzZS5saWJzLnVybF9hcmd1bWVudHM7XG5cdFx0XHRcblx0XHRcdGlmICh1cmxIZWxwZXIuZ2V0Q3VycmVudEZpbGUoKSA9PT0gJ2NhdGVnb3JpZXMucGhwJykge1xuXHRcdFx0XHQkKCcuY29sdW1uTGVmdDInKVxuXHRcdFx0XHRcdC5wYXJlbnRzKCd0YWJsZTpmaXJzdCcpXG5cdFx0XHRcdFx0LmNzcygnd2lkdGgnLCAnJyk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0XG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdHZhciB1cmxIZWxwZXIgPSBqc2UubGlicy51cmxfYXJndW1lbnRzO1xuXHRcdFx0dmFyIGZpbGUgPSB1cmxIZWxwZXIuZ2V0Q3VycmVudEZpbGUoKSxcblx0XHRcdFx0ZG9QYXJhbWV0ZXIgPSB1cmxIZWxwZXIuZ2V0VXJsUGFyYW1ldGVycygpLmRvIHx8ICcnLFxuXHRcdFx0XHRsYXJnZVBhZ2VzID0gW1xuXHRcdFx0XHRcdCdnbV9lbWFpbHMucGhwJ1xuXHRcdFx0XHRdLFxuXHRcdFx0XHRzbWFsbFBhZ2VzID0gW1xuXHRcdFx0XHRcdCdnbV9zZW9fYm9vc3QucGhwJyxcblx0XHRcdFx0XHQncGFyY2VsX3NlcnZpY2VzLnBocCdcblx0XHRcdFx0XTtcblx0XHRcdFxuXHRcdFx0aWYgKCQuaW5BcnJheShmaWxlLCBsYXJnZVBhZ2VzKSA+IC0xXG5cdFx0XHRcdHx8IChmaWxlID09PSAnYWRtaW4ucGhwJyAmJiAkLmluQXJyYXkoZG9QYXJhbWV0ZXIsIGxhcmdlUGFnZXMpID4gLTEpKSB7XG5cdFx0XHRcdCQoJy5ib3hDZW50ZXJXcmFwcGVyJylcblx0XHRcdFx0XHQuYWRkQ2xhc3MoJ2JyZWFrcG9pbnQtbGFyZ2UnKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0aWYgKCQuaW5BcnJheShmaWxlLCBzbWFsbFBhZ2VzKSA+IC0xXG5cdFx0XHRcdHx8IChmaWxlID09PSAnYWRtaW4ucGhwJyAmJiAkLmluQXJyYXkoZG9QYXJhbWV0ZXIsIHNtYWxsUGFnZXMpID4gLTEpKSB7XG5cdFx0XHRcdCQoJy5ib3hDZW50ZXJXcmFwcGVyJylcblx0XHRcdFx0XHQuYWRkQ2xhc3MoJ2JyZWFrcG9pbnQtc21hbGwnKTtcblx0XHRcdH1cblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBIZWxwZXIgdG8gYWRkIGNzcyBicmVha3BvaW50IGNsYXNzZXMgdG8gcGFnZXMgd2hpY2ggdXNlIHRoZSBjb250cm9sbGVyIG1lY2hhbmlzbS5cblx0XHQgKiBFeHRlbmQgd2hldGhlciB0aGUgYXJyYXkgJ2xhcmdlUGFnZXMnIG9yICdzbWFsbFBhZ2VzJyB0byBhZGQgdGhlIGJyZWFrcG9pbnQgY2xhc3MuXG5cdFx0ICogQWRkIGFzIGVsZW1lbnQgdGhlIGNvbnRyb2xsZXIgbmFtZSAobGlrZSBpbiB0aGUgdXJsIGJlaGluZCBkbz0pIGFuZCB0aGUgYWN0aW9uIHdpdGggdHJhaWxpbmcgc2xhc2guXG5cdFx0ICogKHRoZSBhY3Rpb24gaXMgdGhlIHN0cmluZyBpbiB0aGUgJ2RvJyBhcmd1bWVudCBiZWhpbmQgdGhlIHNsYXNoKVxuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgdXJsSGVscGVyID0ganNlLmxpYnMudXJsX2FyZ3VtZW50cyxcblx0XHRcdFx0Y3VycmVudEZpbGUgPSB1cmxIZWxwZXIuZ2V0Q3VycmVudEZpbGUoKSxcblx0XHRcdFx0Y29udHJvbGxlckFjdGlvbiA9IHVybEhlbHBlci5nZXRVcmxQYXJhbWV0ZXJzKCkuZG8sXG5cdFx0XHRcdGxhcmdlUGFnZXMgPSBbXSxcblx0XHRcdFx0c21hbGxQYWdlcyA9IFsnSmFub2xhd01vZHVsZUNlbnRlck1vZHVsZS9Db25maWcnXTtcblx0XHRcdFxuXHRcdFx0aWYgKGN1cnJlbnRGaWxlID09PSAnYWRtaW4ucGhwJykge1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKCQuaW5BcnJheShjb250cm9sbGVyQWN0aW9uLCBsYXJnZVBhZ2VzKSA+IC0xKSB7XG5cdFx0XHRcdFx0JCgnI2NvbnRhaW5lcicpXG5cdFx0XHRcdFx0XHQuYWRkQ2xhc3MoJ2JyZWFrcG9pbnQtbGFyZ2UnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0aWYgKCQuaW5BcnJheShjb250cm9sbGVyQWN0aW9uLCBzbWFsbFBhZ2VzKSA+IC0xKSB7XG5cdFx0XHRcdFx0JCgnI2NvbnRhaW5lcicpXG5cdFx0XHRcdFx0XHQuYWRkQ2xhc3MoJ2JyZWFrcG9pbnQtc21hbGwnKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsZWFucyB0aGUgaGVhZGVyIG9mIHRoZSBjb25maWd1cmF0aW9uIGJveCBmcm9tIHRhYmxlc1xuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJGNvbnRlbnRzID0gJCgnZGl2LmNvbmZpZ3VyYXRpb24tYm94LWhlYWRlciBoMiB0YWJsZS5jb250ZW50VGFibGUgdHIgdGQgPiAqJyk7XG5cdFx0XHQkY29udGVudHMuZWFjaChmdW5jdGlvbihpbmRleCwgZWxlbSkge1xuXHRcdFx0XHQkKCdkaXYuY29uZmlndXJhdGlvbi1ib3gtaGVhZGVyIGgyJykuYXBwZW5kKGVsZW0pO1xuXHRcdFx0XHQkKCdkaXYuY29uZmlndXJhdGlvbi1ib3gtaGVhZGVyIGgyJykuZmluZCgndGFibGUuY29udGVudFRhYmxlJykucmVtb3ZlKCk7XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDb252ZXJ0IGFsbCB0aGUgc2ltcGxlIGNoZWNrYm94ZXMgdG8gdGhlIEpTIEVuZ2luZSB3aWRnZXQuXG5cdFx0ICpcblx0XHQgKiBUaGlzIGZpeCB3aWxsIGZpbmUtdHVuZSB0aGUgaHRtbCBtYXJrdXAgb2YgdGhlIGNoZWNrYm94IGFuZCB0aGVuIGl0IHdpbGwgZHluYW1pY2FsbHlcblx0XHQgKiBpbml0aWFsaXplIHRoZSBjaGVja2JveCB3aWRnZXQuXG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdHZhciBzZWxlY3RvcnMgPSBbXG5cdFx0XHRcdCd0YWJsZSAuY2F0ZWdvcmllc192aWV3X2RhdGEgaW5wdXQ6Y2hlY2tib3gnLFxuXHRcdFx0XHQndGFibGUgLmRhdGFUYWJsZUhlYWRpbmdSb3cgdGQgaW5wdXQ6Y2hlY2tib3gnLFxuXHRcdFx0XHQndGFibGUgdGhlYWQgdHIgdGg6Zmlyc3QgaW5wdXQ6Y2hlY2tib3gnLFxuXHRcdFx0XHQndGFibGUuZ3gtb3JkZXJzLXRhYmxlIHRyOm5vdCguZGF0YVRhYmxlSGVhZGluZ1JvdykgdGQ6Zmlyc3QtY2hpbGQgaW5wdXQ6Y2hlY2tib3gnLFxuXHRcdFx0XHQnZm9ybVtuYW1lPVwicXVhbnRpdHlfdW5pdHNcIl0gaW5wdXQ6Y2hlY2tib3gnLFxuXHRcdFx0XHQnZm9ybVtuYW1lPVwic2xpZGVyc2V0XCJdIGlucHV0OmNoZWNrYm94Jyxcblx0XHRcdFx0J2Zvcm1bbmFtZT1cImZlYXR1cmVjb250cm9sXCJdIGlucHV0OmNoZWNrYm94Om5vdCguY2hlY2tib3gtc3dpdGNoZXIpJyxcblx0XHRcdFx0Jy5mZWF0dXJlLXRhYmxlIHRyIHRkOmxhc3QtY2hpbGQgaW5wdXQ6Y2hlY2tib3gnXG5cdFx0XHRdO1xuXHRcdFx0XG5cdFx0XHRpZiAoJChzZWxlY3RvcnMpLmxlbmd0aCA+IDEyMCkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCQuZWFjaChzZWxlY3RvcnMsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkKHRoaXMpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0aWYgKCEkKHRoaXMpLnBhcmVudCgpLmhhc0NsYXNzKCdzaW5nbGUtY2hlY2tib3gnKSkge1xuXHRcdFx0XHRcdFx0JCh0aGlzKVxuXHRcdFx0XHRcdFx0XHQuYXR0cignZGF0YS1zaW5nbGVfY2hlY2tib3gnLCAnJylcblx0XHRcdFx0XHRcdFx0LnBhcmVudCgpLmF0dHIoJ2RhdGEtZ3gtd2lkZ2V0JywgJ2NoZWNrYm94Jyk7XG5cdFx0XHRcdFx0XHRneC53aWRnZXRzLmluaXQoJCh0aGlzKS5wYXJlbnQoKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1ha2UgdGhlIHRvcCBoZWFkZXIgYmFyIGNsaWNrYWJsZSB0byBhY3RpdmF0ZSB0aGUgc2VhcmNoIGJhclxuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJHRvcEhlYWRlciA9ICQoJy50b3AtaGVhZGVyJyksXG5cdFx0XHRcdCRzZWFyY2hJbnB1dCA9ICQoJ2lucHV0W25hbWU9XCJhZG1pbl9zZWFyY2hcIl0nKTtcblx0XHRcdFxuXHRcdFx0JHRvcEhlYWRlci5vbignY2xpY2snLCBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0XHRpZiAoJHRvcEhlYWRlci5pcyhldmVudC50YXJnZXQpKSB7XG5cdFx0XHRcdFx0JHNlYXJjaElucHV0LnRyaWdnZXIoJ2NsaWNrJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0fSk7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdC8vIEV4ZWN1dGUgYWxsIHRoZSBleGlzdGluZyBmaXhlcy5cblx0XHRcdCQuZWFjaChmaXhlcywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHRoaXMoKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
