'use strict';

/* --------------------------------------------------------------
 categories_multi_action_controller.js 2018-04-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Orders Table Controller
 *
 * This controller contains the mapping logic of the categories/articles multi select actions for the button
 * dropdown (on the bottom).
 *
 * @module Compatibility/categories_multi_action_controller
 */
gx.compatibility.module('categories_multi_action_controller', [gx.source + '/libs/button_dropdown'],

/**  @lends module:Compatibility/categories_multi_action_controller */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Dropdown button selector
  * @var {object}
  */
	$dropdown = $this.find('.js-bottom-dropdown'),


	/**
  * Input fields
  * @type {*|jQuery|HTMLElement}
  */
	$inputs = $('tr[data-id] input[type="checkbox"]'),


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------


	/**
  * Get Url Parameter
  *
  * Gets a specific URL get parameter from the address bar,
  * which name should be provided as an argument.
  * @param {string} parameterName
  * @returns {object}
  * @private
  */
	var _getUrlParameter = function _getUrlParameter(parameterName) {
		var results = new RegExp('[\?&]' + parameterName + '=([^&#]*)').exec(window.location.href);
		if (results == null) {
			return null;
		} else {
			return results[1] || 0;
		}
	};

	/**
  * Prepare Form
  *
  * @param {string} action
  *
  * @return {object | jQuery}
  */
	var _$prepareForm = function _$prepareForm(action) {
		var cPath;
		try {
			cPath = window.location.href.match(/cPath=(.*)/)[1];
		} catch (e) {
			cPath = $('[data-cpath]:first').data().cpath;
		}

		var page_token = $('input[name="page_token"]:first').attr('value');

		var formUrl = [_getSourcePath(), 'categories.php', '?action=multi_action', '&cPath=' + cPath].join('');

		var search = _getUrlParameter('search');
		if (search !== 0 && search !== null) {
			formUrl += '&search=' + search;
		}

		var page = _getUrlParameter('page');
		if (page !== 0 && page !== null && formUrl.indexOf('page=') === -1) {
			formUrl += '&page=' + page;
		}

		var sorting = _getUrlParameter('sorting');
		if (sorting !== 0 && sorting !== null) {
			formUrl += '&sorting=' + sorting;
		}

		var $form = $('<form name="multi_action_form" method="post" action=' + formUrl + '></form>');
		$form.append('<input type="hidden" name="cpath" value=' + cPath + '>');
		$form.append('<input type="hidden" name="page_token" value=' + page_token + '>');
		$form.append('<input type="hidden" name=' + action + ' value="Action">');
		$form.appendTo('body');
		return $form;
	};

	var _sectionMapping = {
		delete: 'buttons',
		BUTTON_MOVE: 'admin_buttons',
		BUTTON_COPY: 'admin_buttons',
		BUTTON_STATUS_ON: 'admin_buttons',
		BUTTON_STATUS_OFF: 'admin_buttons'
	};

	/**
  * Map actions for the dropdown button
  *
  * This method will map the actions for multiple selects.
  */
	var _mapMultiActions = function _mapMultiActions() {
		var actions = ['delete', 'BUTTON_MOVE', 'BUTTON_COPY', 'BUTTON_STATUS_ON', 'BUTTON_STATUS_OFF'];

		for (var index in actions) {
			_mapAction(actions[index]);
		}
	};

	var _mapAction = function _mapAction(action) {
		var section = _sectionMapping[action],
		    callback = _getActionCallback(action);
		jse.libs.button_dropdown.mapAction($dropdown, action, section, callback);
	};

	var _callbackDelete = function _callbackDelete(event) {
		// Do not do anything when no product/category is checked
		if (!$inputs.filter(':checked').length) {
			return;
		}

		// Submit cached form
		var $form = _$prepareForm('multi_delete');
		$inputs.filter(':checked').appendTo($form);
		$form.submit();
	};

	var _callbackMove = function _callbackMove(event) {
		// Do not do anything when no product/category is checked
		if (!$inputs.filter(':checked').length) {
			return;
		}

		// Submit cached form
		var $form = _$prepareForm('multi_move');
		$inputs.filter(':checked').appendTo($form);
		$form.submit();
	};

	var _callbackCopy = function _callbackCopy(event) {
		// Do not do anything when no product/category is checked
		if (!$inputs.filter(':checked').length) {
			return;
		}

		// Submit cached form
		var $form = _$prepareForm('multi_copy');
		$inputs.filter(':checked').appendTo($form);
		$form.submit();
	};

	var _callbackStatusOn = function _callbackStatusOn(event) {
		// Do not do anything when no product/category is checked
		if (!$inputs.filter(':checked').length) {
			return;
		}

		// Submit cached form
		var $form = _$prepareForm('multi_status_on');
		$inputs.filter(':checked').appendTo($form);
		$form.submit();
	};

	var _callbackStatusOff = function _callbackStatusOff(event) {
		// Do not do anything when no product/category is checked
		if (!$inputs.filter(':checked').length) {
			return;
		}

		// Submit cached form
		var $form = _$prepareForm('multi_status_off');
		$inputs.filter(':checked').appendTo($form);
		$form.submit();
	};

	var _getActionCallback = function _getActionCallback(action) {
		switch (action) {
			case 'delete':
				return _callbackDelete;
			case 'BUTTON_MOVE':
				return _callbackMove;
			case 'BUTTON_COPY':
				return _callbackCopy;
			case 'BUTTON_STATUS_ON':
				return _callbackStatusOn;
			case 'BUTTON_STATUS_OFF':
				return _callbackStatusOff;
			default:
				console.alert('_getActionCallback: Action not found');
		}
		return null;
	};

	/**
  * Get path of the admin folder
  *
  * @returns {string}
  */
	var _getSourcePath = function _getSourcePath() {
		var url = window.location.origin,
		    path = window.location.pathname;

		var splittedPath = path.split('/');
		splittedPath.pop();

		var joinedPath = splittedPath.join('/');

		return url + joinedPath + '/';
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Wait until the dropdown is filled
		var interval = setInterval(function () {
			if ($('.js-button-dropdown').length > 0) {
				clearInterval(interval);
				_mapMultiActions();
			}
		}, 200);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhdGVnb3JpZXMvY2F0ZWdvcmllc19tdWx0aV9hY3Rpb25fY29udHJvbGxlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCIkZHJvcGRvd24iLCJmaW5kIiwiJGlucHV0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfZ2V0VXJsUGFyYW1ldGVyIiwicGFyYW1ldGVyTmFtZSIsInJlc3VsdHMiLCJSZWdFeHAiLCJleGVjIiwid2luZG93IiwibG9jYXRpb24iLCJocmVmIiwiXyRwcmVwYXJlRm9ybSIsImFjdGlvbiIsImNQYXRoIiwibWF0Y2giLCJlIiwiY3BhdGgiLCJwYWdlX3Rva2VuIiwiYXR0ciIsImZvcm1VcmwiLCJfZ2V0U291cmNlUGF0aCIsImpvaW4iLCJzZWFyY2giLCJwYWdlIiwiaW5kZXhPZiIsInNvcnRpbmciLCIkZm9ybSIsImFwcGVuZCIsImFwcGVuZFRvIiwiX3NlY3Rpb25NYXBwaW5nIiwiZGVsZXRlIiwiQlVUVE9OX01PVkUiLCJCVVRUT05fQ09QWSIsIkJVVFRPTl9TVEFUVVNfT04iLCJCVVRUT05fU1RBVFVTX09GRiIsIl9tYXBNdWx0aUFjdGlvbnMiLCJhY3Rpb25zIiwiaW5kZXgiLCJfbWFwQWN0aW9uIiwic2VjdGlvbiIsImNhbGxiYWNrIiwiX2dldEFjdGlvbkNhbGxiYWNrIiwianNlIiwibGlicyIsImJ1dHRvbl9kcm9wZG93biIsIm1hcEFjdGlvbiIsIl9jYWxsYmFja0RlbGV0ZSIsImV2ZW50IiwiZmlsdGVyIiwibGVuZ3RoIiwic3VibWl0IiwiX2NhbGxiYWNrTW92ZSIsIl9jYWxsYmFja0NvcHkiLCJfY2FsbGJhY2tTdGF0dXNPbiIsIl9jYWxsYmFja1N0YXR1c09mZiIsImNvbnNvbGUiLCJhbGVydCIsInVybCIsIm9yaWdpbiIsInBhdGgiLCJwYXRobmFtZSIsInNwbGl0dGVkUGF0aCIsInNwbGl0IiwicG9wIiwiam9pbmVkUGF0aCIsImluaXQiLCJkb25lIiwiaW50ZXJ2YWwiLCJzZXRJbnRlcnZhbCIsImNsZWFySW50ZXJ2YWwiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7QUFRQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyxvQ0FERCxFQUdDLENBQ0NGLEdBQUdHLE1BQUgsR0FBWSx1QkFEYixDQUhEOztBQU9DOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXLEVBYlo7OztBQWVDOzs7O0FBSUFDLGFBQVlILE1BQU1JLElBQU4sQ0FBVyxxQkFBWCxDQW5CYjs7O0FBcUJDOzs7O0FBSUFDLFdBQVVKLEVBQUUsb0NBQUYsQ0F6Qlg7OztBQTJCQzs7Ozs7QUFLQUssV0FBVUwsRUFBRU0sTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CTCxRQUFuQixFQUE2QkgsSUFBN0IsQ0FoQ1g7OztBQWtDQzs7Ozs7QUFLQUYsVUFBUyxFQXZDVjs7QUF5Q0E7QUFDQTtBQUNBOzs7QUFHQTs7Ozs7Ozs7O0FBU0EsS0FBSVcsbUJBQW1CLFNBQW5CQSxnQkFBbUIsQ0FBU0MsYUFBVCxFQUF3QjtBQUM5QyxNQUFJQyxVQUFVLElBQUlDLE1BQUosQ0FBVyxVQUFVRixhQUFWLEdBQTBCLFdBQXJDLEVBQWtERyxJQUFsRCxDQUF1REMsT0FBT0MsUUFBUCxDQUFnQkMsSUFBdkUsQ0FBZDtBQUNBLE1BQUlMLFdBQVcsSUFBZixFQUFxQjtBQUNwQixVQUFPLElBQVA7QUFDQSxHQUZELE1BRU87QUFDTixVQUFPQSxRQUFRLENBQVIsS0FBYyxDQUFyQjtBQUNBO0FBQ0QsRUFQRDs7QUFVQTs7Ozs7OztBQU9BLEtBQUlNLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBU0MsTUFBVCxFQUFpQjtBQUNwQyxNQUFJQyxLQUFKO0FBQ0EsTUFBSTtBQUNIQSxXQUFRTCxPQUFPQyxRQUFQLENBQWdCQyxJQUFoQixDQUFxQkksS0FBckIsQ0FBMkIsWUFBM0IsRUFBeUMsQ0FBekMsQ0FBUjtBQUNBLEdBRkQsQ0FHQSxPQUFPQyxDQUFQLEVBQVU7QUFDVEYsV0FBUWpCLEVBQUUsb0JBQUYsRUFBd0JGLElBQXhCLEdBQStCc0IsS0FBdkM7QUFDQTs7QUFFRCxNQUFJQyxhQUFhckIsRUFBRSxnQ0FBRixFQUFvQ3NCLElBQXBDLENBQXlDLE9BQXpDLENBQWpCOztBQUVBLE1BQUlDLFVBQVUsQ0FDYkMsZ0JBRGEsRUFFYixnQkFGYSxFQUdiLHNCQUhhLEVBSWIsWUFBWVAsS0FKQyxFQUtaUSxJQUxZLENBS1AsRUFMTyxDQUFkOztBQU9BLE1BQUlDLFNBQVNuQixpQkFBaUIsUUFBakIsQ0FBYjtBQUNBLE1BQUltQixXQUFXLENBQVgsSUFBZ0JBLFdBQVcsSUFBL0IsRUFBcUM7QUFDcENILGNBQVksYUFBYUcsTUFBekI7QUFDQTs7QUFFRCxNQUFJQyxPQUFPcEIsaUJBQWlCLE1BQWpCLENBQVg7QUFDQSxNQUFJb0IsU0FBUyxDQUFULElBQWNBLFNBQVMsSUFBdkIsSUFBK0JKLFFBQVFLLE9BQVIsQ0FBZ0IsT0FBaEIsTUFBNkIsQ0FBQyxDQUFqRSxFQUFvRTtBQUNuRUwsY0FBVyxXQUFXSSxJQUF0QjtBQUNBOztBQUVELE1BQUlFLFVBQVV0QixpQkFBaUIsU0FBakIsQ0FBZDtBQUNBLE1BQUlzQixZQUFZLENBQVosSUFBaUJBLFlBQVksSUFBakMsRUFBdUM7QUFDdENOLGNBQVcsY0FBY00sT0FBekI7QUFDQTs7QUFFRCxNQUFJQyxRQUFROUIsRUFBRSx5REFBeUR1QixPQUF6RCxHQUFtRSxVQUFyRSxDQUFaO0FBQ0FPLFFBQU1DLE1BQU4sQ0FBYSw2Q0FBNkNkLEtBQTdDLEdBQXFELEdBQWxFO0FBQ0FhLFFBQU1DLE1BQU4sQ0FBYSxrREFBa0RWLFVBQWxELEdBQStELEdBQTVFO0FBQ0FTLFFBQU1DLE1BQU4sQ0FBYSwrQkFBK0JmLE1BQS9CLEdBQXdDLGtCQUFyRDtBQUNBYyxRQUFNRSxRQUFOLENBQWUsTUFBZjtBQUNBLFNBQU9GLEtBQVA7QUFDQSxFQXZDRDs7QUF5Q0EsS0FBSUcsa0JBQWtCO0FBQ3JCQyxVQUFRLFNBRGE7QUFFckJDLGVBQWEsZUFGUTtBQUdyQkMsZUFBYSxlQUhRO0FBSXJCQyxvQkFBa0IsZUFKRztBQUtyQkMscUJBQW1CO0FBTEUsRUFBdEI7O0FBUUE7Ozs7O0FBS0EsS0FBSUMsbUJBQW1CLFNBQW5CQSxnQkFBbUIsR0FBVztBQUNqQyxNQUFJQyxVQUFVLENBQ2IsUUFEYSxFQUViLGFBRmEsRUFHYixhQUhhLEVBSWIsa0JBSmEsRUFLYixtQkFMYSxDQUFkOztBQVFBLE9BQUssSUFBSUMsS0FBVCxJQUFrQkQsT0FBbEIsRUFBMkI7QUFDMUJFLGNBQVdGLFFBQVFDLEtBQVIsQ0FBWDtBQUNBO0FBQ0QsRUFaRDs7QUFjQSxLQUFJQyxhQUFhLFNBQWJBLFVBQWEsQ0FBUzFCLE1BQVQsRUFBaUI7QUFDakMsTUFBSTJCLFVBQVVWLGdCQUFnQmpCLE1BQWhCLENBQWQ7QUFBQSxNQUNDNEIsV0FBV0MsbUJBQW1CN0IsTUFBbkIsQ0FEWjtBQUVBOEIsTUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQy9DLFNBQW5DLEVBQThDYyxNQUE5QyxFQUFzRDJCLE9BQXRELEVBQStEQyxRQUEvRDtBQUNBLEVBSkQ7O0FBTUEsS0FBSU0sa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFTQyxLQUFULEVBQWdCO0FBQ3JDO0FBQ0EsTUFBSSxDQUFDL0MsUUFBUWdELE1BQVIsQ0FBZSxVQUFmLEVBQTJCQyxNQUFoQyxFQUF3QztBQUN2QztBQUNBOztBQUVEO0FBQ0EsTUFBSXZCLFFBQVFmLGNBQWMsY0FBZCxDQUFaO0FBQ0FYLFVBQVFnRCxNQUFSLENBQWUsVUFBZixFQUEyQnBCLFFBQTNCLENBQW9DRixLQUFwQztBQUNBQSxRQUFNd0IsTUFBTjtBQUNBLEVBVkQ7O0FBWUEsS0FBSUMsZ0JBQWdCLFNBQWhCQSxhQUFnQixDQUFTSixLQUFULEVBQWdCO0FBQ25DO0FBQ0EsTUFBSSxDQUFDL0MsUUFBUWdELE1BQVIsQ0FBZSxVQUFmLEVBQTJCQyxNQUFoQyxFQUF3QztBQUN2QztBQUNBOztBQUVEO0FBQ0EsTUFBSXZCLFFBQVFmLGNBQWMsWUFBZCxDQUFaO0FBQ0FYLFVBQVFnRCxNQUFSLENBQWUsVUFBZixFQUEyQnBCLFFBQTNCLENBQW9DRixLQUFwQztBQUNBQSxRQUFNd0IsTUFBTjtBQUNBLEVBVkQ7O0FBWUEsS0FBSUUsZ0JBQWdCLFNBQWhCQSxhQUFnQixDQUFTTCxLQUFULEVBQWdCO0FBQ25DO0FBQ0EsTUFBSSxDQUFDL0MsUUFBUWdELE1BQVIsQ0FBZSxVQUFmLEVBQTJCQyxNQUFoQyxFQUF3QztBQUN2QztBQUNBOztBQUVEO0FBQ0EsTUFBSXZCLFFBQVFmLGNBQWMsWUFBZCxDQUFaO0FBQ0FYLFVBQVFnRCxNQUFSLENBQWUsVUFBZixFQUEyQnBCLFFBQTNCLENBQW9DRixLQUFwQztBQUNBQSxRQUFNd0IsTUFBTjtBQUNBLEVBVkQ7O0FBWUEsS0FBSUcsb0JBQW9CLFNBQXBCQSxpQkFBb0IsQ0FBU04sS0FBVCxFQUFnQjtBQUN2QztBQUNBLE1BQUksQ0FBQy9DLFFBQVFnRCxNQUFSLENBQWUsVUFBZixFQUEyQkMsTUFBaEMsRUFBd0M7QUFDdkM7QUFDQTs7QUFFRDtBQUNBLE1BQUl2QixRQUFRZixjQUFjLGlCQUFkLENBQVo7QUFDQVgsVUFBUWdELE1BQVIsQ0FBZSxVQUFmLEVBQTJCcEIsUUFBM0IsQ0FBb0NGLEtBQXBDO0FBQ0FBLFFBQU13QixNQUFOO0FBQ0EsRUFWRDs7QUFZQSxLQUFJSSxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFTUCxLQUFULEVBQWdCO0FBQ3hDO0FBQ0EsTUFBSSxDQUFDL0MsUUFBUWdELE1BQVIsQ0FBZSxVQUFmLEVBQTJCQyxNQUFoQyxFQUF3QztBQUN2QztBQUNBOztBQUVEO0FBQ0EsTUFBSXZCLFFBQVFmLGNBQWMsa0JBQWQsQ0FBWjtBQUNBWCxVQUFRZ0QsTUFBUixDQUFlLFVBQWYsRUFBMkJwQixRQUEzQixDQUFvQ0YsS0FBcEM7QUFDQUEsUUFBTXdCLE1BQU47QUFDQSxFQVZEOztBQVlBLEtBQUlULHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQVM3QixNQUFULEVBQWlCO0FBQ3pDLFVBQVFBLE1BQVI7QUFDQyxRQUFLLFFBQUw7QUFDQyxXQUFPa0MsZUFBUDtBQUNELFFBQUssYUFBTDtBQUNDLFdBQU9LLGFBQVA7QUFDRCxRQUFLLGFBQUw7QUFDQyxXQUFPQyxhQUFQO0FBQ0QsUUFBSyxrQkFBTDtBQUNDLFdBQU9DLGlCQUFQO0FBQ0QsUUFBSyxtQkFBTDtBQUNDLFdBQU9DLGtCQUFQO0FBQ0Q7QUFDQ0MsWUFBUUMsS0FBUixDQUFjLHNDQUFkO0FBWkY7QUFjQSxTQUFPLElBQVA7QUFDQSxFQWhCRDs7QUFrQkE7Ozs7O0FBS0EsS0FBSXBDLGlCQUFpQixTQUFqQkEsY0FBaUIsR0FBVztBQUMvQixNQUFJcUMsTUFBTWpELE9BQU9DLFFBQVAsQ0FBZ0JpRCxNQUExQjtBQUFBLE1BQ0NDLE9BQU9uRCxPQUFPQyxRQUFQLENBQWdCbUQsUUFEeEI7O0FBR0EsTUFBSUMsZUFBZUYsS0FBS0csS0FBTCxDQUFXLEdBQVgsQ0FBbkI7QUFDQUQsZUFBYUUsR0FBYjs7QUFFQSxNQUFJQyxhQUFhSCxhQUFheEMsSUFBYixDQUFrQixHQUFsQixDQUFqQjs7QUFFQSxTQUFPb0MsTUFBTU8sVUFBTixHQUFtQixHQUExQjtBQUNBLEVBVkQ7O0FBWUE7QUFDQTtBQUNBOztBQUVBeEUsUUFBT3lFLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUI7QUFDQSxNQUFJQyxXQUFXQyxZQUFZLFlBQVc7QUFDckMsT0FBSXhFLEVBQUUscUJBQUYsRUFBeUJxRCxNQUF6QixHQUFrQyxDQUF0QyxFQUF5QztBQUN4Q29CLGtCQUFjRixRQUFkO0FBQ0FoQztBQUNBO0FBQ0QsR0FMYyxFQUtaLEdBTFksQ0FBZjs7QUFPQStCO0FBQ0EsRUFWRDs7QUFZQSxRQUFPMUUsTUFBUDtBQUNBLENBblJGIiwiZmlsZSI6ImNhdGVnb3JpZXMvY2F0ZWdvcmllc19tdWx0aV9hY3Rpb25fY29udHJvbGxlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gY2F0ZWdvcmllc19tdWx0aV9hY3Rpb25fY29udHJvbGxlci5qcyAyMDE4LTA0LTE4XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBPcmRlcnMgVGFibGUgQ29udHJvbGxlclxuICpcbiAqIFRoaXMgY29udHJvbGxlciBjb250YWlucyB0aGUgbWFwcGluZyBsb2dpYyBvZiB0aGUgY2F0ZWdvcmllcy9hcnRpY2xlcyBtdWx0aSBzZWxlY3QgYWN0aW9ucyBmb3IgdGhlIGJ1dHRvblxuICogZHJvcGRvd24gKG9uIHRoZSBib3R0b20pLlxuICpcbiAqIEBtb2R1bGUgQ29tcGF0aWJpbGl0eS9jYXRlZ29yaWVzX211bHRpX2FjdGlvbl9jb250cm9sbGVyXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnY2F0ZWdvcmllc19tdWx0aV9hY3Rpb25fY29udHJvbGxlcicsXG5cdFxuXHRbXG5cdFx0Z3guc291cmNlICsgJy9saWJzL2J1dHRvbl9kcm9wZG93bidcblx0XSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb21wYXRpYmlsaXR5L2NhdGVnb3JpZXNfbXVsdGlfYWN0aW9uX2NvbnRyb2xsZXIgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEcm9wZG93biBidXR0b24gc2VsZWN0b3Jcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JGRyb3Bkb3duID0gJHRoaXMuZmluZCgnLmpzLWJvdHRvbS1kcm9wZG93bicpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIElucHV0IGZpZWxkc1xuXHRcdFx0ICogQHR5cGUgeyp8alF1ZXJ5fEhUTUxFbGVtZW50fVxuXHRcdFx0ICovXG5cdFx0XHQkaW5wdXRzID0gJCgndHJbZGF0YS1pZF0gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdJyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFBSSVZBVEUgTUVUSE9EU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdldCBVcmwgUGFyYW1ldGVyXG5cdFx0ICpcblx0XHQgKiBHZXRzIGEgc3BlY2lmaWMgVVJMIGdldCBwYXJhbWV0ZXIgZnJvbSB0aGUgYWRkcmVzcyBiYXIsXG5cdFx0ICogd2hpY2ggbmFtZSBzaG91bGQgYmUgcHJvdmlkZWQgYXMgYW4gYXJndW1lbnQuXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHBhcmFtZXRlck5hbWVcblx0XHQgKiBAcmV0dXJucyB7b2JqZWN0fVxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9nZXRVcmxQYXJhbWV0ZXIgPSBmdW5jdGlvbihwYXJhbWV0ZXJOYW1lKSB7XG5cdFx0XHR2YXIgcmVzdWx0cyA9IG5ldyBSZWdFeHAoJ1tcXD8mXScgKyBwYXJhbWV0ZXJOYW1lICsgJz0oW14mI10qKScpLmV4ZWMod2luZG93LmxvY2F0aW9uLmhyZWYpO1xuXHRcdFx0aWYgKHJlc3VsdHMgPT0gbnVsbCkge1xuXHRcdFx0XHRyZXR1cm4gbnVsbDtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHJldHVybiByZXN1bHRzWzFdIHx8IDA7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBQcmVwYXJlIEZvcm1cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBhY3Rpb25cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge29iamVjdCB8IGpRdWVyeX1cblx0XHQgKi9cblx0XHR2YXIgXyRwcmVwYXJlRm9ybSA9IGZ1bmN0aW9uKGFjdGlvbikge1xuXHRcdFx0dmFyIGNQYXRoO1xuXHRcdFx0dHJ5IHtcblx0XHRcdFx0Y1BhdGggPSB3aW5kb3cubG9jYXRpb24uaHJlZi5tYXRjaCgvY1BhdGg9KC4qKS8pWzFdO1xuXHRcdFx0fVxuXHRcdFx0Y2F0Y2ggKGUpIHtcblx0XHRcdFx0Y1BhdGggPSAkKCdbZGF0YS1jcGF0aF06Zmlyc3QnKS5kYXRhKCkuY3BhdGg7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdHZhciBwYWdlX3Rva2VuID0gJCgnaW5wdXRbbmFtZT1cInBhZ2VfdG9rZW5cIl06Zmlyc3QnKS5hdHRyKCd2YWx1ZScpO1xuXHRcdFx0XG5cdFx0XHR2YXIgZm9ybVVybCA9IFtcblx0XHRcdFx0X2dldFNvdXJjZVBhdGgoKSxcblx0XHRcdFx0J2NhdGVnb3JpZXMucGhwJyxcblx0XHRcdFx0Jz9hY3Rpb249bXVsdGlfYWN0aW9uJyxcblx0XHRcdFx0JyZjUGF0aD0nICsgY1BhdGhcblx0XHRcdF0uam9pbignJyk7XG5cdFx0XHRcblx0XHRcdHZhciBzZWFyY2ggPSBfZ2V0VXJsUGFyYW1ldGVyKCdzZWFyY2gnKTtcblx0XHRcdGlmIChzZWFyY2ggIT09IDAgJiYgc2VhcmNoICE9PSBudWxsKSB7XG5cdFx0XHRcdGZvcm1VcmwgKz0gKCcmc2VhcmNoPScgKyBzZWFyY2gpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHR2YXIgcGFnZSA9IF9nZXRVcmxQYXJhbWV0ZXIoJ3BhZ2UnKTtcblx0XHRcdGlmIChwYWdlICE9PSAwICYmIHBhZ2UgIT09IG51bGwgJiYgZm9ybVVybC5pbmRleE9mKCdwYWdlPScpID09PSAtMSkge1xuXHRcdFx0XHRmb3JtVXJsICs9ICcmcGFnZT0nICsgcGFnZTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0dmFyIHNvcnRpbmcgPSBfZ2V0VXJsUGFyYW1ldGVyKCdzb3J0aW5nJyk7XG5cdFx0XHRpZiAoc29ydGluZyAhPT0gMCAmJiBzb3J0aW5nICE9PSBudWxsKSB7XG5cdFx0XHRcdGZvcm1VcmwgKz0gJyZzb3J0aW5nPScgKyBzb3J0aW5nO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHR2YXIgJGZvcm0gPSAkKCc8Zm9ybSBuYW1lPVwibXVsdGlfYWN0aW9uX2Zvcm1cIiBtZXRob2Q9XCJwb3N0XCIgYWN0aW9uPScgKyBmb3JtVXJsICsgJz48L2Zvcm0+Jyk7XG5cdFx0XHQkZm9ybS5hcHBlbmQoJzxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgbmFtZT1cImNwYXRoXCIgdmFsdWU9JyArIGNQYXRoICsgJz4nKTtcblx0XHRcdCRmb3JtLmFwcGVuZCgnPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBuYW1lPVwicGFnZV90b2tlblwiIHZhbHVlPScgKyBwYWdlX3Rva2VuICsgJz4nKTtcblx0XHRcdCRmb3JtLmFwcGVuZCgnPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBuYW1lPScgKyBhY3Rpb24gKyAnIHZhbHVlPVwiQWN0aW9uXCI+Jyk7XG5cdFx0XHQkZm9ybS5hcHBlbmRUbygnYm9keScpO1xuXHRcdFx0cmV0dXJuICRmb3JtO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9zZWN0aW9uTWFwcGluZyA9IHtcblx0XHRcdGRlbGV0ZTogJ2J1dHRvbnMnLFxuXHRcdFx0QlVUVE9OX01PVkU6ICdhZG1pbl9idXR0b25zJyxcblx0XHRcdEJVVFRPTl9DT1BZOiAnYWRtaW5fYnV0dG9ucycsXG5cdFx0XHRCVVRUT05fU1RBVFVTX09OOiAnYWRtaW5fYnV0dG9ucycsXG5cdFx0XHRCVVRUT05fU1RBVFVTX09GRjogJ2FkbWluX2J1dHRvbnMnXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNYXAgYWN0aW9ucyBmb3IgdGhlIGRyb3Bkb3duIGJ1dHRvblxuXHRcdCAqXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCBtYXAgdGhlIGFjdGlvbnMgZm9yIG11bHRpcGxlIHNlbGVjdHMuXG5cdFx0ICovXG5cdFx0dmFyIF9tYXBNdWx0aUFjdGlvbnMgPSBmdW5jdGlvbigpIHtcblx0XHRcdHZhciBhY3Rpb25zID0gW1xuXHRcdFx0XHQnZGVsZXRlJyxcblx0XHRcdFx0J0JVVFRPTl9NT1ZFJyxcblx0XHRcdFx0J0JVVFRPTl9DT1BZJyxcblx0XHRcdFx0J0JVVFRPTl9TVEFUVVNfT04nLFxuXHRcdFx0XHQnQlVUVE9OX1NUQVRVU19PRkYnXG5cdFx0XHRdO1xuXHRcdFx0XG5cdFx0XHRmb3IgKHZhciBpbmRleCBpbiBhY3Rpb25zKSB7XG5cdFx0XHRcdF9tYXBBY3Rpb24oYWN0aW9uc1tpbmRleF0pO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9tYXBBY3Rpb24gPSBmdW5jdGlvbihhY3Rpb24pIHtcblx0XHRcdHZhciBzZWN0aW9uID0gX3NlY3Rpb25NYXBwaW5nW2FjdGlvbl0sXG5cdFx0XHRcdGNhbGxiYWNrID0gX2dldEFjdGlvbkNhbGxiYWNrKGFjdGlvbik7XG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24ubWFwQWN0aW9uKCRkcm9wZG93biwgYWN0aW9uLCBzZWN0aW9uLCBjYWxsYmFjayk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2NhbGxiYWNrRGVsZXRlID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIERvIG5vdCBkbyBhbnl0aGluZyB3aGVuIG5vIHByb2R1Y3QvY2F0ZWdvcnkgaXMgY2hlY2tlZFxuXHRcdFx0aWYgKCEkaW5wdXRzLmZpbHRlcignOmNoZWNrZWQnKS5sZW5ndGgpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTdWJtaXQgY2FjaGVkIGZvcm1cblx0XHRcdHZhciAkZm9ybSA9IF8kcHJlcGFyZUZvcm0oJ211bHRpX2RlbGV0ZScpO1xuXHRcdFx0JGlucHV0cy5maWx0ZXIoJzpjaGVja2VkJykuYXBwZW5kVG8oJGZvcm0pO1xuXHRcdFx0JGZvcm0uc3VibWl0KCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2NhbGxiYWNrTW92ZSA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHQvLyBEbyBub3QgZG8gYW55dGhpbmcgd2hlbiBubyBwcm9kdWN0L2NhdGVnb3J5IGlzIGNoZWNrZWRcblx0XHRcdGlmICghJGlucHV0cy5maWx0ZXIoJzpjaGVja2VkJykubGVuZ3RoKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gU3VibWl0IGNhY2hlZCBmb3JtXG5cdFx0XHR2YXIgJGZvcm0gPSBfJHByZXBhcmVGb3JtKCdtdWx0aV9tb3ZlJyk7XG5cdFx0XHQkaW5wdXRzLmZpbHRlcignOmNoZWNrZWQnKS5hcHBlbmRUbygkZm9ybSk7XG5cdFx0XHQkZm9ybS5zdWJtaXQoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfY2FsbGJhY2tDb3B5ID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIERvIG5vdCBkbyBhbnl0aGluZyB3aGVuIG5vIHByb2R1Y3QvY2F0ZWdvcnkgaXMgY2hlY2tlZFxuXHRcdFx0aWYgKCEkaW5wdXRzLmZpbHRlcignOmNoZWNrZWQnKS5sZW5ndGgpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTdWJtaXQgY2FjaGVkIGZvcm1cblx0XHRcdHZhciAkZm9ybSA9IF8kcHJlcGFyZUZvcm0oJ211bHRpX2NvcHknKTtcblx0XHRcdCRpbnB1dHMuZmlsdGVyKCc6Y2hlY2tlZCcpLmFwcGVuZFRvKCRmb3JtKTtcblx0XHRcdCRmb3JtLnN1Ym1pdCgpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9jYWxsYmFja1N0YXR1c09uID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIERvIG5vdCBkbyBhbnl0aGluZyB3aGVuIG5vIHByb2R1Y3QvY2F0ZWdvcnkgaXMgY2hlY2tlZFxuXHRcdFx0aWYgKCEkaW5wdXRzLmZpbHRlcignOmNoZWNrZWQnKS5sZW5ndGgpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTdWJtaXQgY2FjaGVkIGZvcm1cblx0XHRcdHZhciAkZm9ybSA9IF8kcHJlcGFyZUZvcm0oJ211bHRpX3N0YXR1c19vbicpO1xuXHRcdFx0JGlucHV0cy5maWx0ZXIoJzpjaGVja2VkJykuYXBwZW5kVG8oJGZvcm0pO1xuXHRcdFx0JGZvcm0uc3VibWl0KCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2NhbGxiYWNrU3RhdHVzT2ZmID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIERvIG5vdCBkbyBhbnl0aGluZyB3aGVuIG5vIHByb2R1Y3QvY2F0ZWdvcnkgaXMgY2hlY2tlZFxuXHRcdFx0aWYgKCEkaW5wdXRzLmZpbHRlcignOmNoZWNrZWQnKS5sZW5ndGgpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTdWJtaXQgY2FjaGVkIGZvcm1cblx0XHRcdHZhciAkZm9ybSA9IF8kcHJlcGFyZUZvcm0oJ211bHRpX3N0YXR1c19vZmYnKTtcblx0XHRcdCRpbnB1dHMuZmlsdGVyKCc6Y2hlY2tlZCcpLmFwcGVuZFRvKCRmb3JtKTtcblx0XHRcdCRmb3JtLnN1Ym1pdCgpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9nZXRBY3Rpb25DYWxsYmFjayA9IGZ1bmN0aW9uKGFjdGlvbikge1xuXHRcdFx0c3dpdGNoIChhY3Rpb24pIHtcblx0XHRcdFx0Y2FzZSAnZGVsZXRlJzpcblx0XHRcdFx0XHRyZXR1cm4gX2NhbGxiYWNrRGVsZXRlO1xuXHRcdFx0XHRjYXNlICdCVVRUT05fTU9WRSc6XG5cdFx0XHRcdFx0cmV0dXJuIF9jYWxsYmFja01vdmU7XG5cdFx0XHRcdGNhc2UgJ0JVVFRPTl9DT1BZJzpcblx0XHRcdFx0XHRyZXR1cm4gX2NhbGxiYWNrQ29weTtcblx0XHRcdFx0Y2FzZSAnQlVUVE9OX1NUQVRVU19PTic6XG5cdFx0XHRcdFx0cmV0dXJuIF9jYWxsYmFja1N0YXR1c09uO1xuXHRcdFx0XHRjYXNlICdCVVRUT05fU1RBVFVTX09GRic6XG5cdFx0XHRcdFx0cmV0dXJuIF9jYWxsYmFja1N0YXR1c09mZjtcblx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRjb25zb2xlLmFsZXJ0KCdfZ2V0QWN0aW9uQ2FsbGJhY2s6IEFjdGlvbiBub3QgZm91bmQnKTtcblx0XHRcdH1cblx0XHRcdHJldHVybiBudWxsO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IHBhdGggb2YgdGhlIGFkbWluIGZvbGRlclxuXHRcdCAqXG5cdFx0ICogQHJldHVybnMge3N0cmluZ31cblx0XHQgKi9cblx0XHR2YXIgX2dldFNvdXJjZVBhdGggPSBmdW5jdGlvbigpIHtcblx0XHRcdHZhciB1cmwgPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luLFxuXHRcdFx0XHRwYXRoID0gd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lO1xuXHRcdFx0XG5cdFx0XHR2YXIgc3BsaXR0ZWRQYXRoID0gcGF0aC5zcGxpdCgnLycpO1xuXHRcdFx0c3BsaXR0ZWRQYXRoLnBvcCgpO1xuXHRcdFx0XG5cdFx0XHR2YXIgam9pbmVkUGF0aCA9IHNwbGl0dGVkUGF0aC5qb2luKCcvJyk7XG5cdFx0XHRcblx0XHRcdHJldHVybiB1cmwgKyBqb2luZWRQYXRoICsgJy8nO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdC8vIFdhaXQgdW50aWwgdGhlIGRyb3Bkb3duIGlzIGZpbGxlZFxuXHRcdFx0dmFyIGludGVydmFsID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGlmICgkKCcuanMtYnV0dG9uLWRyb3Bkb3duJykubGVuZ3RoID4gMCkge1xuXHRcdFx0XHRcdGNsZWFySW50ZXJ2YWwoaW50ZXJ2YWwpO1xuXHRcdFx0XHRcdF9tYXBNdWx0aUFjdGlvbnMoKTtcblx0XHRcdFx0fVxuXHRcdFx0fSwgMjAwKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
