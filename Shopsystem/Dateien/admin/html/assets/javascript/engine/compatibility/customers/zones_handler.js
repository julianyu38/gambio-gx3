'use strict';

/* --------------------------------------------------------------
 zones_handler.js 2017-05-31
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * The Component for handling the federal state dropdown depending on the country.
 * The field will be blacked out if there are no federal states for the selected
 * country.
 */
gx.compatibility.module('zones_handler', ['form', 'xhr'], function (data) {

	'use strict';

	// ########## VARIABLE INITIALIZATION ##########

	var $this = $(this),
	    $states = $('select[name=entry_state]'),
	    $selectedState = $('input[name=selected_zone_id]'),
	    $statesFormGroup = $('select[name=entry_state]').closest('tr.no-hover'),
	    defaults = {
		loadStates: 'admin.php?do=Zones',
		country: 'select[name=entry_country_id]'
	},
	    options = $.extend(true, {}, defaults, data),
	    module = {};

	var _changeHandler = function _changeHandler() {

		var dataset = jse.libs.form.getData($this);

		jse.libs.xhr.ajax({ url: options.loadStates, data: dataset }, true).done(function (result) {

			if (result.success) {

				$states.children('option').remove();
				$selectedState.prop("disabled", false);
				$states.prop("disabled", false);

				$.each(result.data, function (key, value) {
					if (options.nameinsteadofid == true) {
						value.id = value.name;
					}

					if (value.selected) {
						$states.append($("<option selected/>").val(value.id).text(value.name));
					} else {
						$states.append($("<option />").val(value.id).text(value.name));
					}
				});

				$statesFormGroup.show();
			} else {

				$statesFormGroup.hide().prop("disabled", true);
				$selectedState.prop("disabled", true);
				$states.prop("disabled", true);
			}
		});
	};

	// ########## INITIALIZATION ##########

	/**
  * Init function of the widget
  * @constructor
  */
	module.init = function (done) {

		_changeHandler();

		$this.on('change', options.country, _changeHandler);

		done();
	};

	// Return data to widget engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbWVycy96b25lc19oYW5kbGVyLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkc3RhdGVzIiwiJHNlbGVjdGVkU3RhdGUiLCIkc3RhdGVzRm9ybUdyb3VwIiwiY2xvc2VzdCIsImRlZmF1bHRzIiwibG9hZFN0YXRlcyIsImNvdW50cnkiLCJvcHRpb25zIiwiZXh0ZW5kIiwiX2NoYW5nZUhhbmRsZXIiLCJkYXRhc2V0IiwianNlIiwibGlicyIsImZvcm0iLCJnZXREYXRhIiwieGhyIiwiYWpheCIsInVybCIsImRvbmUiLCJyZXN1bHQiLCJzdWNjZXNzIiwiY2hpbGRyZW4iLCJyZW1vdmUiLCJwcm9wIiwiZWFjaCIsImtleSIsInZhbHVlIiwibmFtZWluc3RlYWRvZmlkIiwiaWQiLCJuYW1lIiwic2VsZWN0ZWQiLCJhcHBlbmQiLCJ2YWwiLCJ0ZXh0Iiwic2hvdyIsImhpZGUiLCJpbml0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyxlQURELEVBR0MsQ0FDQyxNQURELEVBRUMsS0FGRCxDQUhELEVBUUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBOztBQUVBLEtBQUlDLFFBQW1CQyxFQUFFLElBQUYsQ0FBdkI7QUFBQSxLQUNDQyxVQUFtQkQsRUFBRSwwQkFBRixDQURwQjtBQUFBLEtBRUNFLGlCQUFtQkYsRUFBRSw4QkFBRixDQUZwQjtBQUFBLEtBR0NHLG1CQUFtQkgsRUFBRSwwQkFBRixFQUE4QkksT0FBOUIsQ0FBc0MsYUFBdEMsQ0FIcEI7QUFBQSxLQUtDQyxXQUFXO0FBQ1ZDLGNBQVksb0JBREY7QUFFVkMsV0FBWTtBQUZGLEVBTFo7QUFBQSxLQVNDQyxVQUFVUixFQUFFUyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJKLFFBQW5CLEVBQTZCUCxJQUE3QixDQVRYO0FBQUEsS0FVQ0QsU0FBUyxFQVZWOztBQVlBLEtBQUlhLGlCQUFpQixTQUFqQkEsY0FBaUIsR0FBVzs7QUFFL0IsTUFBSUMsVUFBVUMsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLE9BQWQsQ0FBc0JoQixLQUF0QixDQUFkOztBQUVBYSxNQUFJQyxJQUFKLENBQVNHLEdBQVQsQ0FBYUMsSUFBYixDQUFrQixFQUFDQyxLQUFLVixRQUFRRixVQUFkLEVBQTBCUixNQUFNYSxPQUFoQyxFQUFsQixFQUE0RCxJQUE1RCxFQUFrRVEsSUFBbEUsQ0FBdUUsVUFBU0MsTUFBVCxFQUFpQjs7QUFFdkYsT0FBSUEsT0FBT0MsT0FBWCxFQUFvQjs7QUFFbkJwQixZQUFRcUIsUUFBUixDQUFpQixRQUFqQixFQUEyQkMsTUFBM0I7QUFDQXJCLG1CQUFlc0IsSUFBZixDQUFvQixVQUFwQixFQUFnQyxLQUFoQztBQUNBdkIsWUFBUXVCLElBQVIsQ0FBYSxVQUFiLEVBQXlCLEtBQXpCOztBQUVBeEIsTUFBRXlCLElBQUYsQ0FBT0wsT0FBT3RCLElBQWQsRUFBb0IsVUFBUzRCLEdBQVQsRUFBY0MsS0FBZCxFQUFxQjtBQUN4QyxTQUFHbkIsUUFBUW9CLGVBQVIsSUFBMkIsSUFBOUIsRUFDQTtBQUNDRCxZQUFNRSxFQUFOLEdBQVdGLE1BQU1HLElBQWpCO0FBQ0E7O0FBRUQsU0FBR0gsTUFBTUksUUFBVCxFQUNBO0FBQ0M5QixjQUFRK0IsTUFBUixDQUFlaEMsRUFBRSxvQkFBRixFQUF3QmlDLEdBQXhCLENBQTRCTixNQUFNRSxFQUFsQyxFQUFzQ0ssSUFBdEMsQ0FBMkNQLE1BQU1HLElBQWpELENBQWY7QUFDQSxNQUhELE1BS0E7QUFDQzdCLGNBQVErQixNQUFSLENBQWVoQyxFQUFFLFlBQUYsRUFBZ0JpQyxHQUFoQixDQUFvQk4sTUFBTUUsRUFBMUIsRUFBOEJLLElBQTlCLENBQW1DUCxNQUFNRyxJQUF6QyxDQUFmO0FBQ0E7QUFDRCxLQWREOztBQWdCQTNCLHFCQUFpQmdDLElBQWpCO0FBRUEsSUF4QkQsTUF5Qks7O0FBRUpoQyxxQkFBaUJpQyxJQUFqQixHQUF3QlosSUFBeEIsQ0FBNkIsVUFBN0IsRUFBeUMsSUFBekM7QUFDQXRCLG1CQUFlc0IsSUFBZixDQUFvQixVQUFwQixFQUFnQyxJQUFoQztBQUNBdkIsWUFBUXVCLElBQVIsQ0FBYSxVQUFiLEVBQXlCLElBQXpCO0FBRUE7QUFFRCxHQW5DRDtBQXFDQSxFQXpDRDs7QUEyQ0E7O0FBRUE7Ozs7QUFJQTNCLFFBQU93QyxJQUFQLEdBQWMsVUFBU2xCLElBQVQsRUFBZTs7QUFFNUJUOztBQUVBWCxRQUFNdUMsRUFBTixDQUFTLFFBQVQsRUFBbUI5QixRQUFRRCxPQUEzQixFQUFvQ0csY0FBcEM7O0FBRUFTO0FBQ0EsRUFQRDs7QUFTQTtBQUNBLFFBQU90QixNQUFQO0FBQ0EsQ0F0RkYiLCJmaWxlIjoiY3VzdG9tZXJzL3pvbmVzX2hhbmRsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHpvbmVzX2hhbmRsZXIuanMgMjAxNy0wNS0zMVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogVGhlIENvbXBvbmVudCBmb3IgaGFuZGxpbmcgdGhlIGZlZGVyYWwgc3RhdGUgZHJvcGRvd24gZGVwZW5kaW5nIG9uIHRoZSBjb3VudHJ5LlxuICogVGhlIGZpZWxkIHdpbGwgYmUgYmxhY2tlZCBvdXQgaWYgdGhlcmUgYXJlIG5vIGZlZGVyYWwgc3RhdGVzIGZvciB0aGUgc2VsZWN0ZWRcbiAqIGNvdW50cnkuXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnem9uZXNfaGFuZGxlcicsXG5cdFxuXHRbXG5cdFx0J2Zvcm0nLFxuXHRcdCd4aHInXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vICMjIyMjIyMjIyMgVkFSSUFCTEUgSU5JVElBTElaQVRJT04gIyMjIyMjIyMjI1xuXHRcdFxuXHRcdHZhciAkdGhpcyAgICAgICAgICAgID0gJCh0aGlzKSxcblx0XHRcdCRzdGF0ZXMgICAgICAgICAgPSAkKCdzZWxlY3RbbmFtZT1lbnRyeV9zdGF0ZV0nKSxcblx0XHRcdCRzZWxlY3RlZFN0YXRlICAgPSAkKCdpbnB1dFtuYW1lPXNlbGVjdGVkX3pvbmVfaWRdJyksXG5cdFx0XHQkc3RhdGVzRm9ybUdyb3VwID0gJCgnc2VsZWN0W25hbWU9ZW50cnlfc3RhdGVdJykuY2xvc2VzdCgndHIubm8taG92ZXInKSxcblx0XHRcdFxuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdGxvYWRTdGF0ZXM6ICdhZG1pbi5waHA/ZG89Wm9uZXMnLFxuXHRcdFx0XHRjb3VudHJ5OiAgICAnc2VsZWN0W25hbWU9ZW50cnlfY291bnRyeV9pZF0nLFxuXHRcdFx0fSxcblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0dmFyIF9jaGFuZ2VIYW5kbGVyID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcblx0XHRcdHZhciBkYXRhc2V0ID0ganNlLmxpYnMuZm9ybS5nZXREYXRhKCR0aGlzKTtcblx0XHRcdFxuXHRcdFx0anNlLmxpYnMueGhyLmFqYXgoe3VybDogb3B0aW9ucy5sb2FkU3RhdGVzLCBkYXRhOiBkYXRhc2V0fSwgdHJ1ZSkuZG9uZShmdW5jdGlvbihyZXN1bHQpIHtcblx0XHRcdFx0XG5cdFx0XHRcdGlmIChyZXN1bHQuc3VjY2Vzcykge1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCRzdGF0ZXMuY2hpbGRyZW4oJ29wdGlvbicpLnJlbW92ZSgpO1xuXHRcdFx0XHRcdCRzZWxlY3RlZFN0YXRlLnByb3AoXCJkaXNhYmxlZFwiLCBmYWxzZSk7XG5cdFx0XHRcdFx0JHN0YXRlcy5wcm9wKFwiZGlzYWJsZWRcIiwgZmFsc2UpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCQuZWFjaChyZXN1bHQuZGF0YSwgZnVuY3Rpb24oa2V5LCB2YWx1ZSkge1xuXHRcdFx0XHRcdFx0aWYob3B0aW9ucy5uYW1laW5zdGVhZG9maWQgPT0gdHJ1ZSlcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0dmFsdWUuaWQgPSB2YWx1ZS5uYW1lO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRpZih2YWx1ZS5zZWxlY3RlZClcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0JHN0YXRlcy5hcHBlbmQoJChcIjxvcHRpb24gc2VsZWN0ZWQvPlwiKS52YWwodmFsdWUuaWQpLnRleHQodmFsdWUubmFtZSkpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHQkc3RhdGVzLmFwcGVuZCgkKFwiPG9wdGlvbiAvPlwiKS52YWwodmFsdWUuaWQpLnRleHQodmFsdWUubmFtZSkpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdFx0JHN0YXRlc0Zvcm1Hcm91cC5zaG93KCk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHN0YXRlc0Zvcm1Hcm91cC5oaWRlKCkucHJvcChcImRpc2FibGVkXCIsIHRydWUpO1xuXHRcdFx0XHRcdCRzZWxlY3RlZFN0YXRlLnByb3AoXCJkaXNhYmxlZFwiLCB0cnVlKTtcblx0XHRcdFx0XHQkc3RhdGVzLnByb3AoXCJkaXNhYmxlZFwiLCB0cnVlKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0fTtcblx0XHRcblx0XHQvLyAjIyMjIyMjIyMjIElOSVRJQUxJWkFUSU9OICMjIyMjIyMjIyNcblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0IGZ1bmN0aW9uIG9mIHRoZSB3aWRnZXRcblx0XHQgKiBAY29uc3RydWN0b3Jcblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdFxuXHRcdFx0X2NoYW5nZUhhbmRsZXIoKTtcblxuXHRcdFx0JHRoaXMub24oJ2NoYW5nZScsIG9wdGlvbnMuY291bnRyeSwgX2NoYW5nZUhhbmRsZXIpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byB3aWRnZXQgZW5naW5lXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
