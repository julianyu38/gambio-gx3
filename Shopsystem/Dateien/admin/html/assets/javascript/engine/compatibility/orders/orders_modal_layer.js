'use strict';

/* --------------------------------------------------------------
 orders_modal_layer.js 2016-03-16
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Orders Modal Layer Module
 *
 * This module will open a modal layer for order actions like deleting or changing the oder status.
 *
 * @module Compatibility/orders_modal_layer
 */
gx.compatibility.module('orders_modal_layer', ['xhr', 'fallback'],

/**  @lends module:Compatibility/orders_modal_layer */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Modal Selector
  *
  * @type {object}
  */
	$modal = $('#modal_layer_container'),


	/**
  * Checkboxes Selector
  *
  * @type {object}
  */
	$checkboxes = $('.gx-orders-table tr:not(.dataTableHeadingRow) input'),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		detail_page: false,
		comment: ''
	},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ------------------------------------------------------------------------

	var _openDeleteDialog = function _openDeleteDialog(event) {

		var $form = $('#delete_confirm_form');
		$form.attr('action', $form.attr('action') + '&oID=' + $this.data('order_id'));

		event.preventDefault();

		var title = jse.core.lang.translate('TEXT_INFO_HEADING_DELETE_ORDER', 'orders').replace('%s', $this.data('order_id'));

		$form.dialog({
			'title': title,
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($form),
			'width': 420
		});
	};

	var _openTrackingCodeDeleteDialog = function _openTrackingCodeDeleteDialog(event) {
		var $form = $('#delete_tracking_code_confirm_form');
		var data_set = jse.libs.fallback._data($(this), 'orders_modal_layer');
		$form.dialog({
			'title': jse.core.lang.translate('TXT_PARCEL_TRACKING_DELETE_BUTTON', 'parcel_services').replace('%s', data_set.tracking_code),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': [{
				'text': jse.core.lang.translate('close', 'buttons'),
				'class': 'btn',
				'click': function click() {
					$(this).dialog('close');
				}
			}, {
				'text': jse.core.lang.translate('delete', 'buttons'),
				'class': 'btn btn-primary',
				'click': function click() {
					$(this).dialog('close');

					var url = 'request_port.php?module=ParcelServices&action=delete_tracking_code';

					jse.libs.xhr.post({
						'url': url,
						'data': {
							'tracking_code_id': data_set.tracking_code_id,
							'order_id': data_set.order_id,
							'page_token': data_set.page_token
						}
					}).done(function (response) {
						$('#tracking_code_wrapper > .frame-content > table').html(response.html);
					});
				}
			}],
			'width': 420
		});
	};

	var _openMultiDeleteDialog = function _openMultiDeleteDialog(event) {

		var $form = $('#multi_delete_confirm_form'),
		    orderId = 0;

		event.preventDefault();

		if ($checkboxes.filter(':checked').length === 0) {
			return false;
		}

		_readSelectedOrders($form);

		$form.attr('action', $form.attr('action') + '&oID=' + $this.data('order_id'));

		$form.dialog({
			'title': jse.core.lang.translate('TEXT_INFO_HEADING_MULTI_DELETE_ORDER', 'orders').replace('%s', $this.data('order_id')),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($form),
			'width': 420
		});
	};

	var _openMultiCancelDialog = function _openMultiCancelDialog(event) {
		var $form = $('#multi_cancel_confirm_form');
		event.preventDefault();

		if (options.detail_page) {
			// Orders detail page
			$form.append('<input type="hidden" name="gm_multi_status[]" value="' + options.order_id + '" />');
			$form.find('.selected_orders').text(options.order_id);
			$form.find('textarea[name="gm_comments"]').html(options.comment);
		} else {
			// Orders page
			if ($checkboxes.filter(':checked').length === 0) {
				return false;
			}
			_readSelectedOrders($form);
		}

		$form.attr('action', $form.attr('action') + '?oID=' + $this.data('order_id') + '&origin=old_orders_overview');

		$form.dialog({
			'title': jse.core.lang.translate('TEXT_INFO_HEADING_MULTI_CANCEL_ORDER', 'orders').replace('%s', $this.data('order_id')),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($form),
			'width': 420
		});
	};

	var _openUpdateOrdersStatusDialog = function _openUpdateOrdersStatusDialog(event) {
		var $form = $('#update_orders_status_form');

		event.preventDefault();

		if (options.detail_page) {
			// Orders detail page
			$form.append('<input type="hidden" name="gm_multi_status[]" value="' + options.order_id + '" />');
			$form.find('.selected_orders').text(options.order_id);
			$form.find('textarea[name="gm_comments"]').html(options.comment);
		} else {
			// Orders page
			if ($checkboxes.filter(':checked').length === 0) {
				return false;
			}
			_readSelectedOrders($form);
		}

		$form.dialog({
			'title': jse.core.lang.translate('HEADING_GM_STATUS', 'orders'),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($form),
			'width': 580
		});
	};

	var _openTrackingCodeDialog = function _openTrackingCodeDialog(event) {

		var $form = $('#add_tracking_code_form');

		event.preventDefault();
		$form.dialog({
			'title': jse.core.lang.translate('TXT_PARCEL_TRACKING_HEADING', 'parcel_services').replace('%s', $this.data('order_id')),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($form),
			'width': 420
		});
	};

	var _openEmailInvoiceDialog = function _openEmailInvoiceDialog(event) {

		var $modal = $('.email-invoice-modal-body');
		var url = jse.core.config.get('appUrl') + '/admin/admin.php';
		var data = {
			id: options.order_id,
			do: 'OrdersModalsAjax/GetEmailInvoiceSubject',
			pageToken: jse.core.config.get('pageToken')
		};
		var invoiceNumbersHtml = '';

		$('.email-invoice-form').show();
		$('.email-invoice-success').hide();

		if ($modal.find('.message_stack_container').length < 1) {
			$modal.find('.alert').wrap('<div class="message_stack_container"></div>');
		}

		$modal.find('.customer-info').text('"' + options.name + '"');
		$modal.find('.email-address').val(options.email);

		$modal.data('orderId', options.order_id);

		$.ajax({ url: url, data: data, dataType: 'json' }).done(function (response) {
			$modal.attr('data-gx-widget', 'single_checkbox');

			$modal.find('.subject').val(response.subject);
			if (response.invoiceIdExists) {
				$modal.find('.invoice-num-info').addClass('hidden');
				$modal.find('.no-invoice').removeClass('hidden');
				$modal.find('.email-invoice-form .message_stack_container').removeClass('hidden');
			} else {
				$modal.find('.invoice-num-info').removeClass('hidden');
				$modal.find('.no-invoice').addClass('hidden');
				$modal.find('.email-invoice-form .message_stack_container').addClass('hidden');
			}

			if (Object.keys(response.invoiceNumbers).length <= 1) {
				$modal.find('.invoice-numbers').addClass('hidden');
			} else {
				$modal.find('.invoice-numbers').removeClass('hidden');
			}

			for (var invoiceId in response.invoiceNumbers) {
				invoiceNumbersHtml += '<p><input type="checkbox" name="invoice_ids[]" value="' + invoiceId + '" checked="checked" class="invoice-numbers-checkbox" /> ' + response.invoiceNumbers[invoiceId] + '</p>';
			}

			$modal.find('.invoice-numbers-checkboxes').html(invoiceNumbersHtml);

			gx.widgets.init($modal);

			$modal.dialog({
				'title': jse.core.lang.translate('TITLE_INVOICE', 'gm_order_menu'),
				'modal': true,
				'dialogClass': 'gx-container',
				'buttons': _getModalButtons($modal),
				'width': 600
			});

			$modal.find('.invoice-numbers-checkbox').on('change', function () {
				_onChangeEmailInvoiceCheckbox($modal);
			});
		});

		event.preventDefault();
	};

	/**
  * On Email Invoice Checkbox Change
  *
  * Disable send button if all invoice number checkboxes are unchecked. Otherwise enable the send button again.
  */
	var _onChangeEmailInvoiceCheckbox = function _onChangeEmailInvoiceCheckbox($modal) {
		if ($modal.find('.invoice-numbers-checkbox').length > 0) {
			if ($modal.find('.invoice-numbers-checkbox:checked').length > 0) {
				$('.btn-send-invoice-mail').prop('disabled', false);
			} else {
				$('.btn-send-invoice-mail').prop('disabled', true);
			}
		} else {
			$('.btn-send-invoice-mail').prop('disabled', false);
		}
	};

	var _getModalButtons = function _getModalButtons($form) {
		var buttons = [{
			'text': jse.core.lang.translate('close', 'buttons'),
			'class': 'btn',
			'click': function click() {
				$(this).dialog('close');
			}
		}];
		switch (options.action) {
			case 'delete':
			case 'multi_delete':
				buttons.push({
					'text': jse.core.lang.translate('delete', 'buttons'),
					'class': 'btn btn-primary',
					'click': function click() {
						$form.submit();
					}
				});
				break;
			case 'add_tracking_code':
				buttons.push({
					'text': jse.core.lang.translate('add', 'buttons'),
					'class': 'btn btn-primary',
					'click': function click(event) {
						_addTrackingCodeFromOverview(event);
					}
				});
				break;
			case 'update_orders_status':
				buttons.push({
					'text': jse.core.lang.translate('execute', 'buttons'),
					'class': 'btn btn-primary',
					'click': function click(event) {
						$form.submit();
					}
				});
				break;
			case 'multi_cancel':
				buttons.push({
					'text': jse.core.lang.translate('send', 'buttons'),
					'class': 'btn btn-primary',
					'click': function click(event) {
						//event.preventDefault();
						//gm_cancel('gm_send_order.php', '&type=cancel', 'CANCEL');
						$form.submit();
					}
				});
				break;
			case 'email_invoice':
				buttons.push({
					'text': jse.core.lang.translate('send', 'buttons'),
					'class': 'btn btn-primary btn-send-invoice-mail',
					'click': function click(event) {
						event.preventDefault();

						var url = 'gm_pdf_order.php?oID=' + $form.data('orderId') + '&type=invoice&mail=1&gm_quick_mail=1';
						var data = $form.find('form').serialize();

						$.ajax({ url: url, data: data, type: 'POST', dataType: 'html' }).success(function (response) {
							$('.email-invoice-form').hide();
							$('.email-invoice-success').show();

							$('.btn-send-invoice-mail').hide();
						});
					}
				});
		}

		return buttons;
	};

	var _addTrackingCodeFromOverview = function _addTrackingCodeFromOverview(event) {
		event.stopPropagation();

		var tracking_code = $('#parcel_service_tracking_code').val();
		if (tracking_code === '') {
			return false;
		}

		$.ajax({
			'type': 'POST',
			'url': 'request_port.php?module=ParcelServices&action=add_tracking_code',
			'timeout': 30000,
			'dataType': 'json',
			'context': this,
			'data': {

				'tracking_code': tracking_code,
				'service_id': $('#parcel_services_dropdown option:selected').val(),
				'order_id': $this.data('order_id'),
				'page_token': $('.page_token').val()
			},
			success: function success() {
				document.location.reload();
			}
		});

		return false;
	};

	var _readSelectedOrders = function _readSelectedOrders($form) {
		var orderIds = [];

		$checkboxes.filter(':checked').each(function () {
			$form.append('<input type="hidden" name="gm_multi_status[]" value="' + $(this).val() + '" />');

			orderIds.push($(this).val());
		});

		$form.find('.selected_orders').text(orderIds.join(', '));
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		switch (options.action) {
			case 'delete':
				$this.on('click', _openDeleteDialog);
				break;
			case 'multi_delete':
				$this.on('click', _openMultiDeleteDialog);
				break;
			case 'add_tracking_code':
				$this.on('click', _openTrackingCodeDialog);
				break;
			case 'update_orders_status':
				$this.on('click', _openUpdateOrdersStatusDialog);
				break;
			case 'multi_cancel':
				$this.on('click', _openMultiCancelDialog);
				break;
			case 'email_invoice':
				$this.on('click', _openEmailInvoiceDialog);
				break;
		}

		if (options.container === 'tracking_code_wrapper') {
			$this.on('click', '.btn-delete', _openTrackingCodeDeleteDialog);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vcmRlcnNfbW9kYWxfbGF5ZXIuanMiXSwibmFtZXMiOlsiZ3giLCJjb21wYXRpYmlsaXR5IiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRtb2RhbCIsIiRjaGVja2JveGVzIiwiZGVmYXVsdHMiLCJkZXRhaWxfcGFnZSIsImNvbW1lbnQiLCJvcHRpb25zIiwiZXh0ZW5kIiwiX29wZW5EZWxldGVEaWFsb2ciLCJldmVudCIsIiRmb3JtIiwiYXR0ciIsInByZXZlbnREZWZhdWx0IiwidGl0bGUiLCJqc2UiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsInJlcGxhY2UiLCJkaWFsb2ciLCJfZ2V0TW9kYWxCdXR0b25zIiwiX29wZW5UcmFja2luZ0NvZGVEZWxldGVEaWFsb2ciLCJkYXRhX3NldCIsImxpYnMiLCJmYWxsYmFjayIsIl9kYXRhIiwidHJhY2tpbmdfY29kZSIsInVybCIsInhociIsInBvc3QiLCJ0cmFja2luZ19jb2RlX2lkIiwib3JkZXJfaWQiLCJwYWdlX3Rva2VuIiwiZG9uZSIsInJlc3BvbnNlIiwiaHRtbCIsIl9vcGVuTXVsdGlEZWxldGVEaWFsb2ciLCJvcmRlcklkIiwiZmlsdGVyIiwibGVuZ3RoIiwiX3JlYWRTZWxlY3RlZE9yZGVycyIsIl9vcGVuTXVsdGlDYW5jZWxEaWFsb2ciLCJhcHBlbmQiLCJmaW5kIiwidGV4dCIsIl9vcGVuVXBkYXRlT3JkZXJzU3RhdHVzRGlhbG9nIiwiX29wZW5UcmFja2luZ0NvZGVEaWFsb2ciLCJfb3BlbkVtYWlsSW52b2ljZURpYWxvZyIsImNvbmZpZyIsImdldCIsImlkIiwiZG8iLCJwYWdlVG9rZW4iLCJpbnZvaWNlTnVtYmVyc0h0bWwiLCJzaG93IiwiaGlkZSIsIndyYXAiLCJuYW1lIiwidmFsIiwiZW1haWwiLCJhamF4IiwiZGF0YVR5cGUiLCJzdWJqZWN0IiwiaW52b2ljZUlkRXhpc3RzIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsIk9iamVjdCIsImtleXMiLCJpbnZvaWNlTnVtYmVycyIsImludm9pY2VJZCIsIndpZGdldHMiLCJpbml0Iiwib24iLCJfb25DaGFuZ2VFbWFpbEludm9pY2VDaGVja2JveCIsInByb3AiLCJidXR0b25zIiwiYWN0aW9uIiwicHVzaCIsInN1Ym1pdCIsIl9hZGRUcmFja2luZ0NvZGVGcm9tT3ZlcnZpZXciLCJzZXJpYWxpemUiLCJ0eXBlIiwic3VjY2VzcyIsInN0b3BQcm9wYWdhdGlvbiIsImRvY3VtZW50IiwibG9jYXRpb24iLCJyZWxvYWQiLCJvcmRlcklkcyIsImVhY2giLCJqb2luIiwiY29udGFpbmVyIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyxvQkFERCxFQUdDLENBQUMsS0FBRCxFQUFRLFVBQVIsQ0FIRDs7QUFLQzs7QUFFQSxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsVUFBU0QsRUFBRSx3QkFBRixDQWJWOzs7QUFlQzs7Ozs7QUFLQUUsZUFBY0YsRUFBRSxxREFBRixDQXBCZjs7O0FBc0JDOzs7OztBQUtBRyxZQUFXO0FBQ1ZDLGVBQWEsS0FESDtBQUVWQyxXQUFTO0FBRkMsRUEzQlo7OztBQWdDQzs7Ozs7QUFLQUMsV0FBVU4sRUFBRU8sTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CSixRQUFuQixFQUE2QkwsSUFBN0IsQ0FyQ1g7OztBQXVDQzs7Ozs7QUFLQUQsVUFBUyxFQTVDVjs7QUE4Q0E7QUFDQTtBQUNBOztBQUVBLEtBQUlXLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQVNDLEtBQVQsRUFBZ0I7O0FBRXZDLE1BQUlDLFFBQVFWLEVBQUUsc0JBQUYsQ0FBWjtBQUNBVSxRQUFNQyxJQUFOLENBQVcsUUFBWCxFQUFxQkQsTUFBTUMsSUFBTixDQUFXLFFBQVgsSUFBdUIsT0FBdkIsR0FBaUNaLE1BQU1ELElBQU4sQ0FBVyxVQUFYLENBQXREOztBQUVBVyxRQUFNRyxjQUFOOztBQUVBLE1BQUlDLFFBQVFDLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGdDQUF4QixFQUEwRCxRQUExRCxFQUNWQyxPQURVLENBQ0YsSUFERSxFQUNJbkIsTUFBTUQsSUFBTixDQUFXLFVBQVgsQ0FESixDQUFaOztBQUdBWSxRQUFNUyxNQUFOLENBQWE7QUFDWixZQUFTTixLQURHO0FBRVosWUFBUyxJQUZHO0FBR1osa0JBQWUsY0FISDtBQUlaLGNBQVdPLGlCQUFpQlYsS0FBakIsQ0FKQztBQUtaLFlBQVM7QUFMRyxHQUFiO0FBUUEsRUFsQkQ7O0FBb0JBLEtBQUlXLGdDQUFnQyxTQUFoQ0EsNkJBQWdDLENBQVNaLEtBQVQsRUFBZ0I7QUFDbkQsTUFBSUMsUUFBUVYsRUFBRSxvQ0FBRixDQUFaO0FBQ0EsTUFBSXNCLFdBQVdSLElBQUlTLElBQUosQ0FBU0MsUUFBVCxDQUFrQkMsS0FBbEIsQ0FBd0J6QixFQUFFLElBQUYsQ0FBeEIsRUFBaUMsb0JBQWpDLENBQWY7QUFDQVUsUUFBTVMsTUFBTixDQUFhO0FBQ1osWUFBU0wsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUNBQXhCLEVBQTZELGlCQUE3RCxFQUNQQyxPQURPLENBRVAsSUFGTyxFQUVESSxTQUFTSSxhQUZSLENBREc7QUFJWixZQUFTLElBSkc7QUFLWixrQkFBZSxjQUxIO0FBTVosY0FBVyxDQUNWO0FBQ0MsWUFBUVosSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsU0FBakMsQ0FEVDtBQUVDLGFBQVMsS0FGVjtBQUdDLGFBQVMsaUJBQVc7QUFDbkJqQixPQUFFLElBQUYsRUFBUW1CLE1BQVIsQ0FBZSxPQUFmO0FBQ0E7QUFMRixJQURVLEVBUVY7QUFDQyxZQUFRTCxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixRQUF4QixFQUFrQyxTQUFsQyxDQURUO0FBRUMsYUFBUyxpQkFGVjtBQUdDLGFBQVMsaUJBQVc7QUFDbkJqQixPQUFFLElBQUYsRUFBUW1CLE1BQVIsQ0FBZSxPQUFmOztBQUVBLFNBQUlRLE1BQU0sb0VBQVY7O0FBRUFiLFNBQUlTLElBQUosQ0FBU0ssR0FBVCxDQUFhQyxJQUFiLENBQWtCO0FBQ2pCLGFBQU9GLEdBRFU7QUFFakIsY0FBUTtBQUNQLDJCQUFvQkwsU0FBU1EsZ0JBRHRCO0FBRVAsbUJBQVlSLFNBQVNTLFFBRmQ7QUFHUCxxQkFBY1QsU0FBU1U7QUFIaEI7QUFGUyxNQUFsQixFQU9HQyxJQVBILENBT1EsVUFBU0MsUUFBVCxFQUFtQjtBQUMxQmxDLFFBQUUsaURBQUYsRUFBcURtQyxJQUFyRCxDQUEwREQsU0FBU0MsSUFBbkU7QUFDQSxNQVREO0FBVUE7QUFsQkYsSUFSVSxDQU5DO0FBbUNaLFlBQVM7QUFuQ0csR0FBYjtBQXNDQSxFQXpDRDs7QUEyQ0EsS0FBSUMseUJBQXlCLFNBQXpCQSxzQkFBeUIsQ0FBUzNCLEtBQVQsRUFBZ0I7O0FBRTVDLE1BQUlDLFFBQVFWLEVBQUUsNEJBQUYsQ0FBWjtBQUFBLE1BQ0NxQyxVQUFVLENBRFg7O0FBR0E1QixRQUFNRyxjQUFOOztBQUVBLE1BQUlWLFlBQVlvQyxNQUFaLENBQW1CLFVBQW5CLEVBQStCQyxNQUEvQixLQUEwQyxDQUE5QyxFQUFpRDtBQUNoRCxVQUFPLEtBQVA7QUFDQTs7QUFFREMsc0JBQW9COUIsS0FBcEI7O0FBRUFBLFFBQU1DLElBQU4sQ0FBVyxRQUFYLEVBQXFCRCxNQUFNQyxJQUFOLENBQVcsUUFBWCxJQUF1QixPQUF2QixHQUFpQ1osTUFBTUQsSUFBTixDQUFXLFVBQVgsQ0FBdEQ7O0FBRUFZLFFBQU1TLE1BQU4sQ0FBYTtBQUNaLFlBQVNMLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHNDQUF4QixFQUFnRSxRQUFoRSxFQUNQQyxPQURPLENBQ0MsSUFERCxFQUVQbkIsTUFBTUQsSUFBTixDQUFXLFVBQVgsQ0FGTyxDQURHO0FBSVosWUFBUyxJQUpHO0FBS1osa0JBQWUsY0FMSDtBQU1aLGNBQVdzQixpQkFBaUJWLEtBQWpCLENBTkM7QUFPWixZQUFTO0FBUEcsR0FBYjtBQVNBLEVBeEJEOztBQTBCQSxLQUFJK0IseUJBQXlCLFNBQXpCQSxzQkFBeUIsQ0FBU2hDLEtBQVQsRUFBZ0I7QUFDNUMsTUFBSUMsUUFBUVYsRUFBRSw0QkFBRixDQUFaO0FBQ0FTLFFBQU1HLGNBQU47O0FBRUEsTUFBSU4sUUFBUUYsV0FBWixFQUF5QjtBQUN4QjtBQUNBTSxTQUFNZ0MsTUFBTixDQUFhLDBEQUEwRHBDLFFBQVF5QixRQUFsRSxHQUNaLE1BREQ7QUFFQXJCLFNBQU1pQyxJQUFOLENBQVcsa0JBQVgsRUFBK0JDLElBQS9CLENBQW9DdEMsUUFBUXlCLFFBQTVDO0FBQ0FyQixTQUFNaUMsSUFBTixDQUFXLDhCQUFYLEVBQTJDUixJQUEzQyxDQUFnRDdCLFFBQVFELE9BQXhEO0FBQ0EsR0FORCxNQU9LO0FBQ0o7QUFDQSxPQUFJSCxZQUFZb0MsTUFBWixDQUFtQixVQUFuQixFQUErQkMsTUFBL0IsS0FBMEMsQ0FBOUMsRUFBaUQ7QUFDaEQsV0FBTyxLQUFQO0FBQ0E7QUFDREMsdUJBQW9COUIsS0FBcEI7QUFDQTs7QUFFREEsUUFBTUMsSUFBTixDQUFXLFFBQVgsRUFBcUJELE1BQU1DLElBQU4sQ0FBVyxRQUFYLElBQXVCLE9BQXZCLEdBQWlDWixNQUFNRCxJQUFOLENBQVcsVUFBWCxDQUFqQyxHQUEwRCw2QkFBL0U7O0FBRUFZLFFBQU1TLE1BQU4sQ0FBYTtBQUNaLFlBQVNMLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHNDQUF4QixFQUFnRSxRQUFoRSxFQUNQQyxPQURPLENBQ0MsSUFERCxFQUVQbkIsTUFBTUQsSUFBTixDQUFXLFVBQVgsQ0FGTyxDQURHO0FBSVosWUFBUyxJQUpHO0FBS1osa0JBQWUsY0FMSDtBQU1aLGNBQVdzQixpQkFBaUJWLEtBQWpCLENBTkM7QUFPWixZQUFTO0FBUEcsR0FBYjtBQVNBLEVBOUJEOztBQWdDQSxLQUFJbUMsZ0NBQWdDLFNBQWhDQSw2QkFBZ0MsQ0FBU3BDLEtBQVQsRUFBZ0I7QUFDbkQsTUFBSUMsUUFBUVYsRUFBRSw0QkFBRixDQUFaOztBQUVBUyxRQUFNRyxjQUFOOztBQUVBLE1BQUlOLFFBQVFGLFdBQVosRUFBeUI7QUFDeEI7QUFDQU0sU0FBTWdDLE1BQU4sQ0FBYSwwREFBMERwQyxRQUFReUIsUUFBbEUsR0FDWixNQUREO0FBRUFyQixTQUFNaUMsSUFBTixDQUFXLGtCQUFYLEVBQStCQyxJQUEvQixDQUFvQ3RDLFFBQVF5QixRQUE1QztBQUNBckIsU0FBTWlDLElBQU4sQ0FBVyw4QkFBWCxFQUEyQ1IsSUFBM0MsQ0FBZ0Q3QixRQUFRRCxPQUF4RDtBQUNBLEdBTkQsTUFPSztBQUNKO0FBQ0EsT0FBSUgsWUFBWW9DLE1BQVosQ0FBbUIsVUFBbkIsRUFBK0JDLE1BQS9CLEtBQTBDLENBQTlDLEVBQWlEO0FBQ2hELFdBQU8sS0FBUDtBQUNBO0FBQ0RDLHVCQUFvQjlCLEtBQXBCO0FBQ0E7O0FBRURBLFFBQU1TLE1BQU4sQ0FBYTtBQUNaLFlBQVNMLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1CQUF4QixFQUE2QyxRQUE3QyxDQURHO0FBRVosWUFBUyxJQUZHO0FBR1osa0JBQWUsY0FISDtBQUlaLGNBQVdHLGlCQUFpQlYsS0FBakIsQ0FKQztBQUtaLFlBQVM7QUFMRyxHQUFiO0FBT0EsRUEzQkQ7O0FBNkJBLEtBQUlvQywwQkFBMEIsU0FBMUJBLHVCQUEwQixDQUFTckMsS0FBVCxFQUFnQjs7QUFFN0MsTUFBSUMsUUFBUVYsRUFBRSx5QkFBRixDQUFaOztBQUVBUyxRQUFNRyxjQUFOO0FBQ0FGLFFBQU1TLE1BQU4sQ0FBYTtBQUNaLFlBQVNMLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLDZCQUF4QixFQUF1RCxpQkFBdkQsRUFDUEMsT0FETyxDQUNDLElBREQsRUFDT25CLE1BQU1ELElBQU4sQ0FBVyxVQUFYLENBRFAsQ0FERztBQUdaLFlBQVMsSUFIRztBQUlaLGtCQUFlLGNBSkg7QUFLWixjQUFXc0IsaUJBQWlCVixLQUFqQixDQUxDO0FBTVosWUFBUztBQU5HLEdBQWI7QUFTQSxFQWREOztBQWdCQSxLQUFJcUMsMEJBQTBCLFNBQTFCQSx1QkFBMEIsQ0FBU3RDLEtBQVQsRUFBZ0I7O0FBRTdDLE1BQU1SLFNBQVNELEVBQUUsMkJBQUYsQ0FBZjtBQUNBLE1BQU0yQixNQUFNYixJQUFJQyxJQUFKLENBQVNpQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxrQkFBNUM7QUFDQSxNQUFNbkQsT0FBTztBQUNab0QsT0FBSTVDLFFBQVF5QixRQURBO0FBRVpvQixPQUFJLHlDQUZRO0FBR1pDLGNBQVd0QyxJQUFJQyxJQUFKLENBQVNpQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUhDLEdBQWI7QUFLQSxNQUFJSSxxQkFBcUIsRUFBekI7O0FBRUFyRCxJQUFFLHFCQUFGLEVBQXlCc0QsSUFBekI7QUFDQXRELElBQUUsd0JBQUYsRUFBNEJ1RCxJQUE1Qjs7QUFFQSxNQUFJdEQsT0FBTzBDLElBQVAsQ0FBWSwwQkFBWixFQUF3Q0osTUFBeEMsR0FBaUQsQ0FBckQsRUFBd0Q7QUFDdkR0QyxVQUFPMEMsSUFBUCxDQUFZLFFBQVosRUFBc0JhLElBQXRCLENBQTJCLDZDQUEzQjtBQUNBOztBQUVEdkQsU0FBTzBDLElBQVAsQ0FBWSxnQkFBWixFQUE4QkMsSUFBOUIsT0FBdUN0QyxRQUFRbUQsSUFBL0M7QUFDQXhELFNBQU8wQyxJQUFQLENBQVksZ0JBQVosRUFBOEJlLEdBQTlCLENBQWtDcEQsUUFBUXFELEtBQTFDOztBQUVBMUQsU0FDRUgsSUFERixDQUNPLFNBRFAsRUFDa0JRLFFBQVF5QixRQUQxQjs7QUFHQS9CLElBQUU0RCxJQUFGLENBQU8sRUFBQ2pDLFFBQUQsRUFBTTdCLFVBQU4sRUFBWStELFVBQVUsTUFBdEIsRUFBUCxFQUFzQzVCLElBQXRDLENBQTJDLFVBQUNDLFFBQUQsRUFBYztBQUN4RGpDLFVBQU9VLElBQVAsQ0FBWSxnQkFBWixFQUE4QixpQkFBOUI7O0FBRUFWLFVBQU8wQyxJQUFQLENBQVksVUFBWixFQUF3QmUsR0FBeEIsQ0FBNEJ4QixTQUFTNEIsT0FBckM7QUFDQSxPQUFJNUIsU0FBUzZCLGVBQWIsRUFBOEI7QUFDN0I5RCxXQUFPMEMsSUFBUCxDQUFZLG1CQUFaLEVBQWlDcUIsUUFBakMsQ0FBMEMsUUFBMUM7QUFDQS9ELFdBQU8wQyxJQUFQLENBQVksYUFBWixFQUEyQnNCLFdBQTNCLENBQXVDLFFBQXZDO0FBQ0FoRSxXQUFPMEMsSUFBUCxDQUFZLDhDQUFaLEVBQTREc0IsV0FBNUQsQ0FBd0UsUUFBeEU7QUFDQSxJQUpELE1BSU87QUFDTmhFLFdBQU8wQyxJQUFQLENBQVksbUJBQVosRUFBaUNzQixXQUFqQyxDQUE2QyxRQUE3QztBQUNBaEUsV0FBTzBDLElBQVAsQ0FBWSxhQUFaLEVBQTJCcUIsUUFBM0IsQ0FBb0MsUUFBcEM7QUFDQS9ELFdBQU8wQyxJQUFQLENBQVksOENBQVosRUFBNERxQixRQUE1RCxDQUFxRSxRQUFyRTtBQUNBOztBQUVELE9BQUlFLE9BQU9DLElBQVAsQ0FBWWpDLFNBQVNrQyxjQUFyQixFQUFxQzdCLE1BQXJDLElBQStDLENBQW5ELEVBQXNEO0FBQ3JEdEMsV0FBTzBDLElBQVAsQ0FBWSxrQkFBWixFQUFnQ3FCLFFBQWhDLENBQXlDLFFBQXpDO0FBQ0EsSUFGRCxNQUVPO0FBQ04vRCxXQUFPMEMsSUFBUCxDQUFZLGtCQUFaLEVBQWdDc0IsV0FBaEMsQ0FBNEMsUUFBNUM7QUFDQTs7QUFFRCxRQUFLLElBQUlJLFNBQVQsSUFBc0JuQyxTQUFTa0MsY0FBL0IsRUFBK0M7QUFDOUNmLDBCQUNDLDJEQUEyRGdCLFNBQTNELEdBQ0UsMERBREYsR0FFRW5DLFNBQVNrQyxjQUFULENBQXdCQyxTQUF4QixDQUZGLEdBRXVDLE1BSHhDO0FBSUE7O0FBRURwRSxVQUFPMEMsSUFBUCxDQUFZLDZCQUFaLEVBQTJDUixJQUEzQyxDQUFnRGtCLGtCQUFoRDs7QUFFQTFELE1BQUcyRSxPQUFILENBQVdDLElBQVgsQ0FBZ0J0RSxNQUFoQjs7QUFFQUEsVUFBT2tCLE1BQVAsQ0FBYztBQUNiLGFBQVNMLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGVBQXhCLEVBQXlDLGVBQXpDLENBREk7QUFFYixhQUFTLElBRkk7QUFHYixtQkFBZSxjQUhGO0FBSWIsZUFBV0csaUJBQWlCbkIsTUFBakIsQ0FKRTtBQUtiLGFBQVM7QUFMSSxJQUFkOztBQVFBQSxVQUFPMEMsSUFBUCxDQUFZLDJCQUFaLEVBQXlDNkIsRUFBekMsQ0FBNEMsUUFBNUMsRUFBc0QsWUFBVztBQUNoRUMsa0NBQThCeEUsTUFBOUI7QUFDQSxJQUZEO0FBR0EsR0ExQ0Q7O0FBNENBUSxRQUFNRyxjQUFOO0FBQ0EsRUFyRUQ7O0FBdUVBOzs7OztBQUtBLEtBQUk2RCxnQ0FBZ0MsU0FBaENBLDZCQUFnQyxDQUFTeEUsTUFBVCxFQUFpQjtBQUNwRCxNQUFJQSxPQUFPMEMsSUFBUCxDQUFZLDJCQUFaLEVBQXlDSixNQUF6QyxHQUFrRCxDQUF0RCxFQUF5RDtBQUN4RCxPQUFJdEMsT0FBTzBDLElBQVAsQ0FBWSxtQ0FBWixFQUFpREosTUFBakQsR0FBMEQsQ0FBOUQsRUFBaUU7QUFDaEV2QyxNQUFFLHdCQUFGLEVBQTRCMEUsSUFBNUIsQ0FBaUMsVUFBakMsRUFBNkMsS0FBN0M7QUFDQSxJQUZELE1BRU87QUFDTjFFLE1BQUUsd0JBQUYsRUFBNEIwRSxJQUE1QixDQUFpQyxVQUFqQyxFQUE2QyxJQUE3QztBQUNBO0FBQ0QsR0FORCxNQU1PO0FBQ04xRSxLQUFFLHdCQUFGLEVBQTRCMEUsSUFBNUIsQ0FBaUMsVUFBakMsRUFBNkMsS0FBN0M7QUFDQTtBQUNELEVBVkQ7O0FBWUEsS0FBSXRELG1CQUFtQixTQUFuQkEsZ0JBQW1CLENBQVNWLEtBQVQsRUFBZ0I7QUFDdEMsTUFBSWlFLFVBQVUsQ0FDYjtBQUNDLFdBQVE3RCxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxTQUFqQyxDQURUO0FBRUMsWUFBUyxLQUZWO0FBR0MsWUFBUyxpQkFBVztBQUNuQmpCLE1BQUUsSUFBRixFQUFRbUIsTUFBUixDQUFlLE9BQWY7QUFDQTtBQUxGLEdBRGEsQ0FBZDtBQVNBLFVBQVFiLFFBQVFzRSxNQUFoQjtBQUNDLFFBQUssUUFBTDtBQUNBLFFBQUssY0FBTDtBQUNDRCxZQUFRRSxJQUFSLENBQWE7QUFDWixhQUFRL0QsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsUUFBeEIsRUFBa0MsU0FBbEMsQ0FESTtBQUVaLGNBQVMsaUJBRkc7QUFHWixjQUFTLGlCQUFXO0FBQ25CUCxZQUFNb0UsTUFBTjtBQUNBO0FBTFcsS0FBYjtBQU9BO0FBQ0QsUUFBSyxtQkFBTDtBQUNDSCxZQUFRRSxJQUFSLENBQWE7QUFDWixhQUFRL0QsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsS0FBeEIsRUFBK0IsU0FBL0IsQ0FESTtBQUVaLGNBQVMsaUJBRkc7QUFHWixjQUFTLGVBQVNSLEtBQVQsRUFBZ0I7QUFDeEJzRSxtQ0FBNkJ0RSxLQUE3QjtBQUNBO0FBTFcsS0FBYjtBQU9BO0FBQ0QsUUFBSyxzQkFBTDtBQUNDa0UsWUFBUUUsSUFBUixDQUFhO0FBQ1osYUFBUS9ELElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFNBQXhCLEVBQW1DLFNBQW5DLENBREk7QUFFWixjQUFTLGlCQUZHO0FBR1osY0FBUyxlQUFTUixLQUFULEVBQWdCO0FBQ3hCQyxZQUFNb0UsTUFBTjtBQUNBO0FBTFcsS0FBYjtBQU9BO0FBQ0QsUUFBSyxjQUFMO0FBQ0NILFlBQVFFLElBQVIsQ0FBYTtBQUNaLGFBQVEvRCxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixNQUF4QixFQUFnQyxTQUFoQyxDQURJO0FBRVosY0FBUyxpQkFGRztBQUdaLGNBQVMsZUFBU1IsS0FBVCxFQUFnQjtBQUN4QjtBQUNBO0FBQ0FDLFlBQU1vRSxNQUFOO0FBQ0E7QUFQVyxLQUFiO0FBU0E7QUFDRCxRQUFLLGVBQUw7QUFDQ0gsWUFBUUUsSUFBUixDQUFhO0FBQ1osYUFBUS9ELElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE1BQXhCLEVBQWdDLFNBQWhDLENBREk7QUFFWixjQUFTLHVDQUZHO0FBR1osY0FBUyxlQUFTUixLQUFULEVBQWdCO0FBQ3hCQSxZQUFNRyxjQUFOOztBQUVBLFVBQUllLE1BQU0sMEJBQTBCakIsTUFBTVosSUFBTixDQUFXLFNBQVgsQ0FBMUIsR0FDUCxzQ0FESDtBQUVBLFVBQUlBLE9BQU9ZLE1BQU1pQyxJQUFOLENBQVcsTUFBWCxFQUFtQnFDLFNBQW5CLEVBQVg7O0FBRUFoRixRQUFFNEQsSUFBRixDQUFPLEVBQUNqQyxRQUFELEVBQU03QixVQUFOLEVBQVltRixNQUFNLE1BQWxCLEVBQTBCcEIsVUFBVSxNQUFwQyxFQUFQLEVBQW9EcUIsT0FBcEQsQ0FBNEQsVUFBQ2hELFFBQUQsRUFBYztBQUN6RWxDLFNBQUUscUJBQUYsRUFBeUJ1RCxJQUF6QjtBQUNBdkQsU0FBRSx3QkFBRixFQUE0QnNELElBQTVCOztBQUVBdEQsU0FBRSx3QkFBRixFQUE0QnVELElBQTVCO0FBQ0EsT0FMRDtBQU1BO0FBaEJXLEtBQWI7QUF6Q0Y7O0FBNkRBLFNBQU9vQixPQUFQO0FBQ0EsRUF4RUQ7O0FBMEVBLEtBQUlJLCtCQUErQixTQUEvQkEsNEJBQStCLENBQVN0RSxLQUFULEVBQWdCO0FBQ2xEQSxRQUFNMEUsZUFBTjs7QUFFQSxNQUFJekQsZ0JBQWdCMUIsRUFBRSwrQkFBRixFQUFtQzBELEdBQW5DLEVBQXBCO0FBQ0EsTUFBSWhDLGtCQUFrQixFQUF0QixFQUEwQjtBQUN6QixVQUFPLEtBQVA7QUFDQTs7QUFFRDFCLElBQUU0RCxJQUFGLENBQU87QUFDTixXQUFRLE1BREY7QUFFTixVQUFPLGlFQUZEO0FBR04sY0FBVyxLQUhMO0FBSU4sZUFBWSxNQUpOO0FBS04sY0FBVyxJQUxMO0FBTU4sV0FBUTs7QUFFUCxxQkFBaUJsQyxhQUZWO0FBR1Asa0JBQWMxQixFQUFFLDJDQUFGLEVBQStDMEQsR0FBL0MsRUFIUDtBQUlQLGdCQUFZM0QsTUFBTUQsSUFBTixDQUFXLFVBQVgsQ0FKTDtBQUtQLGtCQUFjRSxFQUFFLGFBQUYsRUFBaUIwRCxHQUFqQjtBQUxQLElBTkY7QUFhTndCLFlBQVMsbUJBQVc7QUFDbkJFLGFBQVNDLFFBQVQsQ0FBa0JDLE1BQWxCO0FBQ0E7QUFmSyxHQUFQOztBQWtCQSxTQUFPLEtBQVA7QUFDQSxFQTNCRDs7QUE2QkEsS0FBSTlDLHNCQUFzQixTQUF0QkEsbUJBQXNCLENBQVM5QixLQUFULEVBQWdCO0FBQ3pDLE1BQUk2RSxXQUFXLEVBQWY7O0FBRUFyRixjQUFZb0MsTUFBWixDQUFtQixVQUFuQixFQUErQmtELElBQS9CLENBQW9DLFlBQVc7QUFDOUM5RSxTQUFNZ0MsTUFBTixDQUFhLDBEQUEwRDFDLEVBQUUsSUFBRixFQUFRMEQsR0FBUixFQUExRCxHQUNaLE1BREQ7O0FBR0E2QixZQUFTVixJQUFULENBQWM3RSxFQUFFLElBQUYsRUFBUTBELEdBQVIsRUFBZDtBQUNBLEdBTEQ7O0FBT0FoRCxRQUFNaUMsSUFBTixDQUFXLGtCQUFYLEVBQStCQyxJQUEvQixDQUFvQzJDLFNBQVNFLElBQVQsQ0FBYyxJQUFkLENBQXBDO0FBQ0EsRUFYRDs7QUFhQTtBQUNBO0FBQ0E7O0FBRUE1RixRQUFPMEUsSUFBUCxHQUFjLFVBQVN0QyxJQUFULEVBQWU7QUFDNUIsVUFBUTNCLFFBQVFzRSxNQUFoQjtBQUNDLFFBQUssUUFBTDtBQUNDN0UsVUFBTXlFLEVBQU4sQ0FBUyxPQUFULEVBQWtCaEUsaUJBQWxCO0FBQ0E7QUFDRCxRQUFLLGNBQUw7QUFDQ1QsVUFBTXlFLEVBQU4sQ0FBUyxPQUFULEVBQWtCcEMsc0JBQWxCO0FBQ0E7QUFDRCxRQUFLLG1CQUFMO0FBQ0NyQyxVQUFNeUUsRUFBTixDQUFTLE9BQVQsRUFBa0IxQix1QkFBbEI7QUFDQTtBQUNELFFBQUssc0JBQUw7QUFDQy9DLFVBQU15RSxFQUFOLENBQVMsT0FBVCxFQUFrQjNCLDZCQUFsQjtBQUNBO0FBQ0QsUUFBSyxjQUFMO0FBQ0M5QyxVQUFNeUUsRUFBTixDQUFTLE9BQVQsRUFBa0IvQixzQkFBbEI7QUFDQTtBQUNELFFBQUssZUFBTDtBQUNDMUMsVUFBTXlFLEVBQU4sQ0FBUyxPQUFULEVBQWtCekIsdUJBQWxCO0FBQ0E7QUFsQkY7O0FBcUJBLE1BQUl6QyxRQUFRb0YsU0FBUixLQUFzQix1QkFBMUIsRUFBbUQ7QUFDbEQzRixTQUFNeUUsRUFBTixDQUFTLE9BQVQsRUFBa0IsYUFBbEIsRUFBaUNuRCw2QkFBakM7QUFDQTs7QUFFRFk7QUFDQSxFQTNCRDs7QUE2QkEsUUFBT3BDLE1BQVA7QUFDQSxDQXJkRiIsImZpbGUiOiJvcmRlcnMvb3JkZXJzX21vZGFsX2xheWVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBvcmRlcnNfbW9kYWxfbGF5ZXIuanMgMjAxNi0wMy0xNlxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgT3JkZXJzIE1vZGFsIExheWVyIE1vZHVsZVxuICpcbiAqIFRoaXMgbW9kdWxlIHdpbGwgb3BlbiBhIG1vZGFsIGxheWVyIGZvciBvcmRlciBhY3Rpb25zIGxpa2UgZGVsZXRpbmcgb3IgY2hhbmdpbmcgdGhlIG9kZXIgc3RhdHVzLlxuICpcbiAqIEBtb2R1bGUgQ29tcGF0aWJpbGl0eS9vcmRlcnNfbW9kYWxfbGF5ZXJcbiAqL1xuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdvcmRlcnNfbW9kYWxfbGF5ZXInLFxuXHRcblx0Wyd4aHInLCAnZmFsbGJhY2snXSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb21wYXRpYmlsaXR5L29yZGVyc19tb2RhbF9sYXllciAqL1xuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2RhbCBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCRtb2RhbCA9ICQoJyNtb2RhbF9sYXllcl9jb250YWluZXInKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBDaGVja2JveGVzIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JGNoZWNrYm94ZXMgPSAkKCcuZ3gtb3JkZXJzLXRhYmxlIHRyOm5vdCguZGF0YVRhYmxlSGVhZGluZ1JvdykgaW5wdXQnKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0ZGV0YWlsX3BhZ2U6IGZhbHNlLFxuXHRcdFx0XHRjb21tZW50OiAnJ1xuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXIgX29wZW5EZWxldGVEaWFsb2cgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0XG5cdFx0XHR2YXIgJGZvcm0gPSAkKCcjZGVsZXRlX2NvbmZpcm1fZm9ybScpO1xuXHRcdFx0JGZvcm0uYXR0cignYWN0aW9uJywgJGZvcm0uYXR0cignYWN0aW9uJykgKyAnJm9JRD0nICsgJHRoaXMuZGF0YSgnb3JkZXJfaWQnKSk7XG5cdFx0XHRcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdHZhciB0aXRsZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdURVhUX0lORk9fSEVBRElOR19ERUxFVEVfT1JERVInLCAnb3JkZXJzJylcblx0XHRcdFx0LnJlcGxhY2UoJyVzJywgJHRoaXMuZGF0YSgnb3JkZXJfaWQnKSk7XG5cdFx0XHRcblx0XHRcdCRmb3JtLmRpYWxvZyh7XG5cdFx0XHRcdCd0aXRsZSc6IHRpdGxlLFxuXHRcdFx0XHQnbW9kYWwnOiB0cnVlLFxuXHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0J2J1dHRvbnMnOiBfZ2V0TW9kYWxCdXR0b25zKCRmb3JtKSxcblx0XHRcdFx0J3dpZHRoJzogNDIwXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9vcGVuVHJhY2tpbmdDb2RlRGVsZXRlRGlhbG9nID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciAkZm9ybSA9ICQoJyNkZWxldGVfdHJhY2tpbmdfY29kZV9jb25maXJtX2Zvcm0nKTtcblx0XHRcdHZhciBkYXRhX3NldCA9IGpzZS5saWJzLmZhbGxiYWNrLl9kYXRhKCQodGhpcyksICdvcmRlcnNfbW9kYWxfbGF5ZXInKTtcblx0XHRcdCRmb3JtLmRpYWxvZyh7XG5cdFx0XHRcdCd0aXRsZSc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUWFRfUEFSQ0VMX1RSQUNLSU5HX0RFTEVURV9CVVRUT04nLCAncGFyY2VsX3NlcnZpY2VzJylcblx0XHRcdFx0XHQucmVwbGFjZShcblx0XHRcdFx0XHRcdCclcycsIGRhdGFfc2V0LnRyYWNraW5nX2NvZGUpLFxuXHRcdFx0XHQnbW9kYWwnOiB0cnVlLFxuXHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0J2J1dHRvbnMnOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY2xvc2UnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0J2NsYXNzJzogJ2J0bicsXG5cdFx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHQndGV4dCc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdkZWxldGUnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0J2NsYXNzJzogJ2J0biBidG4tcHJpbWFyeScsXG5cdFx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHR2YXIgdXJsID0gJ3JlcXVlc3RfcG9ydC5waHA/bW9kdWxlPVBhcmNlbFNlcnZpY2VzJmFjdGlvbj1kZWxldGVfdHJhY2tpbmdfY29kZSc7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRqc2UubGlicy54aHIucG9zdCh7XG5cdFx0XHRcdFx0XHRcdFx0J3VybCc6IHVybCxcblx0XHRcdFx0XHRcdFx0XHQnZGF0YSc6IHtcblx0XHRcdFx0XHRcdFx0XHRcdCd0cmFja2luZ19jb2RlX2lkJzogZGF0YV9zZXQudHJhY2tpbmdfY29kZV9pZCxcblx0XHRcdFx0XHRcdFx0XHRcdCdvcmRlcl9pZCc6IGRhdGFfc2V0Lm9yZGVyX2lkLFxuXHRcdFx0XHRcdFx0XHRcdFx0J3BhZ2VfdG9rZW4nOiBkYXRhX3NldC5wYWdlX3Rva2VuXG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHR9KS5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0XHRcdFx0JCgnI3RyYWNraW5nX2NvZGVfd3JhcHBlciA+IC5mcmFtZS1jb250ZW50ID4gdGFibGUnKS5odG1sKHJlc3BvbnNlLmh0bWwpO1xuXHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdF0sXG5cdFx0XHRcdCd3aWR0aCc6IDQyMFxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfb3Blbk11bHRpRGVsZXRlRGlhbG9nID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFxuXHRcdFx0dmFyICRmb3JtID0gJCgnI211bHRpX2RlbGV0ZV9jb25maXJtX2Zvcm0nKSxcblx0XHRcdFx0b3JkZXJJZCA9IDA7XG5cdFx0XHRcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdGlmICgkY2hlY2tib3hlcy5maWx0ZXIoJzpjaGVja2VkJykubGVuZ3RoID09PSAwKSB7XG5cdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0X3JlYWRTZWxlY3RlZE9yZGVycygkZm9ybSk7XG5cdFx0XHRcblx0XHRcdCRmb3JtLmF0dHIoJ2FjdGlvbicsICRmb3JtLmF0dHIoJ2FjdGlvbicpICsgJyZvSUQ9JyArICR0aGlzLmRhdGEoJ29yZGVyX2lkJykpO1xuXHRcdFx0XG5cdFx0XHQkZm9ybS5kaWFsb2coe1xuXHRcdFx0XHQndGl0bGUnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9JTkZPX0hFQURJTkdfTVVMVElfREVMRVRFX09SREVSJywgJ29yZGVycycpXG5cdFx0XHRcdFx0LnJlcGxhY2UoJyVzJyxcblx0XHRcdFx0XHRcdCR0aGlzLmRhdGEoJ29yZGVyX2lkJykpLFxuXHRcdFx0XHQnbW9kYWwnOiB0cnVlLFxuXHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0J2J1dHRvbnMnOiBfZ2V0TW9kYWxCdXR0b25zKCRmb3JtKSxcblx0XHRcdFx0J3dpZHRoJzogNDIwXG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfb3Blbk11bHRpQ2FuY2VsRGlhbG9nID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciAkZm9ybSA9ICQoJyNtdWx0aV9jYW5jZWxfY29uZmlybV9mb3JtJyk7XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHRpZiAob3B0aW9ucy5kZXRhaWxfcGFnZSkge1xuXHRcdFx0XHQvLyBPcmRlcnMgZGV0YWlsIHBhZ2Vcblx0XHRcdFx0JGZvcm0uYXBwZW5kKCc8aW5wdXQgdHlwZT1cImhpZGRlblwiIG5hbWU9XCJnbV9tdWx0aV9zdGF0dXNbXVwiIHZhbHVlPVwiJyArIG9wdGlvbnMub3JkZXJfaWQgK1xuXHRcdFx0XHRcdCdcIiAvPicpO1xuXHRcdFx0XHQkZm9ybS5maW5kKCcuc2VsZWN0ZWRfb3JkZXJzJykudGV4dChvcHRpb25zLm9yZGVyX2lkKTtcblx0XHRcdFx0JGZvcm0uZmluZCgndGV4dGFyZWFbbmFtZT1cImdtX2NvbW1lbnRzXCJdJykuaHRtbChvcHRpb25zLmNvbW1lbnQpO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZSB7XG5cdFx0XHRcdC8vIE9yZGVycyBwYWdlXG5cdFx0XHRcdGlmICgkY2hlY2tib3hlcy5maWx0ZXIoJzpjaGVja2VkJykubGVuZ3RoID09PSAwKSB7XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdF9yZWFkU2VsZWN0ZWRPcmRlcnMoJGZvcm0pO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkZm9ybS5hdHRyKCdhY3Rpb24nLCAkZm9ybS5hdHRyKCdhY3Rpb24nKSArICc/b0lEPScgKyAkdGhpcy5kYXRhKCdvcmRlcl9pZCcpICsgJyZvcmlnaW49b2xkX29yZGVyc19vdmVydmlldycpO1xuXHRcdFx0XG5cdFx0XHQkZm9ybS5kaWFsb2coe1xuXHRcdFx0XHQndGl0bGUnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9JTkZPX0hFQURJTkdfTVVMVElfQ0FOQ0VMX09SREVSJywgJ29yZGVycycpXG5cdFx0XHRcdFx0LnJlcGxhY2UoJyVzJyxcblx0XHRcdFx0XHRcdCR0aGlzLmRhdGEoJ29yZGVyX2lkJykpLFxuXHRcdFx0XHQnbW9kYWwnOiB0cnVlLFxuXHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0J2J1dHRvbnMnOiBfZ2V0TW9kYWxCdXR0b25zKCRmb3JtKSxcblx0XHRcdFx0J3dpZHRoJzogNDIwXG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfb3BlblVwZGF0ZU9yZGVyc1N0YXR1c0RpYWxvZyA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgJGZvcm0gPSAkKCcjdXBkYXRlX29yZGVyc19zdGF0dXNfZm9ybScpO1xuXHRcdFx0XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHRpZiAob3B0aW9ucy5kZXRhaWxfcGFnZSkge1xuXHRcdFx0XHQvLyBPcmRlcnMgZGV0YWlsIHBhZ2Vcblx0XHRcdFx0JGZvcm0uYXBwZW5kKCc8aW5wdXQgdHlwZT1cImhpZGRlblwiIG5hbWU9XCJnbV9tdWx0aV9zdGF0dXNbXVwiIHZhbHVlPVwiJyArIG9wdGlvbnMub3JkZXJfaWQgK1xuXHRcdFx0XHRcdCdcIiAvPicpO1xuXHRcdFx0XHQkZm9ybS5maW5kKCcuc2VsZWN0ZWRfb3JkZXJzJykudGV4dChvcHRpb25zLm9yZGVyX2lkKTtcblx0XHRcdFx0JGZvcm0uZmluZCgndGV4dGFyZWFbbmFtZT1cImdtX2NvbW1lbnRzXCJdJykuaHRtbChvcHRpb25zLmNvbW1lbnQpO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZSB7XG5cdFx0XHRcdC8vIE9yZGVycyBwYWdlXG5cdFx0XHRcdGlmICgkY2hlY2tib3hlcy5maWx0ZXIoJzpjaGVja2VkJykubGVuZ3RoID09PSAwKSB7XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdF9yZWFkU2VsZWN0ZWRPcmRlcnMoJGZvcm0pO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkZm9ybS5kaWFsb2coe1xuXHRcdFx0XHQndGl0bGUnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnSEVBRElOR19HTV9TVEFUVVMnLCAnb3JkZXJzJyksXG5cdFx0XHRcdCdtb2RhbCc6IHRydWUsXG5cdFx0XHRcdCdkaWFsb2dDbGFzcyc6ICdneC1jb250YWluZXInLFxuXHRcdFx0XHQnYnV0dG9ucyc6IF9nZXRNb2RhbEJ1dHRvbnMoJGZvcm0pLFxuXHRcdFx0XHQnd2lkdGgnOiA1ODBcblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9vcGVuVHJhY2tpbmdDb2RlRGlhbG9nID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFxuXHRcdFx0dmFyICRmb3JtID0gJCgnI2FkZF90cmFja2luZ19jb2RlX2Zvcm0nKTtcblx0XHRcdFxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdCRmb3JtLmRpYWxvZyh7XG5cdFx0XHRcdCd0aXRsZSc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUWFRfUEFSQ0VMX1RSQUNLSU5HX0hFQURJTkcnLCAncGFyY2VsX3NlcnZpY2VzJylcblx0XHRcdFx0XHQucmVwbGFjZSgnJXMnLCAkdGhpcy5kYXRhKCdvcmRlcl9pZCcpKSxcblx0XHRcdFx0J21vZGFsJzogdHJ1ZSxcblx0XHRcdFx0J2RpYWxvZ0NsYXNzJzogJ2d4LWNvbnRhaW5lcicsXG5cdFx0XHRcdCdidXR0b25zJzogX2dldE1vZGFsQnV0dG9ucygkZm9ybSksXG5cdFx0XHRcdCd3aWR0aCc6IDQyMFxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfb3BlbkVtYWlsSW52b2ljZURpYWxvZyA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcblx0XHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5lbWFpbC1pbnZvaWNlLW1vZGFsLWJvZHknKTtcblx0XHRcdGNvbnN0IHVybCA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHAnO1xuXHRcdFx0Y29uc3QgZGF0YSA9IHtcblx0XHRcdFx0aWQ6IG9wdGlvbnMub3JkZXJfaWQsXG5cdFx0XHRcdGRvOiAnT3JkZXJzTW9kYWxzQWpheC9HZXRFbWFpbEludm9pY2VTdWJqZWN0Jyxcblx0XHRcdFx0cGFnZVRva2VuOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxuXHRcdFx0fTtcblx0XHRcdGxldCBpbnZvaWNlTnVtYmVyc0h0bWwgPSAnJztcblx0XHRcdFxuXHRcdFx0JCgnLmVtYWlsLWludm9pY2UtZm9ybScpLnNob3coKTtcblx0XHRcdCQoJy5lbWFpbC1pbnZvaWNlLXN1Y2Nlc3MnKS5oaWRlKCk7XG5cdFx0XHRcblx0XHRcdGlmICgkbW9kYWwuZmluZCgnLm1lc3NhZ2Vfc3RhY2tfY29udGFpbmVyJykubGVuZ3RoIDwgMSkge1xuXHRcdFx0XHQkbW9kYWwuZmluZCgnLmFsZXJ0Jykud3JhcCgnPGRpdiBjbGFzcz1cIm1lc3NhZ2Vfc3RhY2tfY29udGFpbmVyXCI+PC9kaXY+Jyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCRtb2RhbC5maW5kKCcuY3VzdG9tZXItaW5mbycpLnRleHQoYFwiJHtvcHRpb25zLm5hbWV9XCJgKTtcblx0XHRcdCRtb2RhbC5maW5kKCcuZW1haWwtYWRkcmVzcycpLnZhbChvcHRpb25zLmVtYWlsKTtcblx0XHRcdFxuXHRcdFx0JG1vZGFsXG5cdFx0XHRcdC5kYXRhKCdvcmRlcklkJywgb3B0aW9ucy5vcmRlcl9pZCk7XG5cdFx0XHRcblx0XHRcdCQuYWpheCh7dXJsLCBkYXRhLCBkYXRhVHlwZTogJ2pzb24nfSkuZG9uZSgocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0JG1vZGFsLmF0dHIoJ2RhdGEtZ3gtd2lkZ2V0JywgJ3NpbmdsZV9jaGVja2JveCcpO1xuXHRcdFx0XHRcblx0XHRcdFx0JG1vZGFsLmZpbmQoJy5zdWJqZWN0JykudmFsKHJlc3BvbnNlLnN1YmplY3QpO1xuXHRcdFx0XHRpZiAocmVzcG9uc2UuaW52b2ljZUlkRXhpc3RzKSB7XG5cdFx0XHRcdFx0JG1vZGFsLmZpbmQoJy5pbnZvaWNlLW51bS1pbmZvJykuYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcubm8taW52b2ljZScpLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLmVtYWlsLWludm9pY2UtZm9ybSAubWVzc2FnZV9zdGFja19jb250YWluZXInKS5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JG1vZGFsLmZpbmQoJy5pbnZvaWNlLW51bS1pbmZvJykucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcubm8taW52b2ljZScpLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLmVtYWlsLWludm9pY2UtZm9ybSAubWVzc2FnZV9zdGFja19jb250YWluZXInKS5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGlmIChPYmplY3Qua2V5cyhyZXNwb25zZS5pbnZvaWNlTnVtYmVycykubGVuZ3RoIDw9IDEpIHtcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtYmVycycpLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtYmVycycpLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0Zm9yIChsZXQgaW52b2ljZUlkIGluIHJlc3BvbnNlLmludm9pY2VOdW1iZXJzKSB7XG5cdFx0XHRcdFx0aW52b2ljZU51bWJlcnNIdG1sICs9XG5cdFx0XHRcdFx0XHQnPHA+PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIG5hbWU9XCJpbnZvaWNlX2lkc1tdXCIgdmFsdWU9XCInICsgaW52b2ljZUlkXG5cdFx0XHRcdFx0XHQrICdcIiBjaGVja2VkPVwiY2hlY2tlZFwiIGNsYXNzPVwiaW52b2ljZS1udW1iZXJzLWNoZWNrYm94XCIgLz4gJ1xuXHRcdFx0XHRcdFx0KyByZXNwb25zZS5pbnZvaWNlTnVtYmVyc1tpbnZvaWNlSWRdICsgJzwvcD4nO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtYmVycy1jaGVja2JveGVzJykuaHRtbChpbnZvaWNlTnVtYmVyc0h0bWwpO1xuXHRcdFx0XHRcblx0XHRcdFx0Z3gud2lkZ2V0cy5pbml0KCRtb2RhbCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkbW9kYWwuZGlhbG9nKHtcblx0XHRcdFx0XHQndGl0bGUnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVElUTEVfSU5WT0lDRScsICdnbV9vcmRlcl9tZW51JyksXG5cdFx0XHRcdFx0J21vZGFsJzogdHJ1ZSxcblx0XHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0XHQnYnV0dG9ucyc6IF9nZXRNb2RhbEJ1dHRvbnMoJG1vZGFsKSxcblx0XHRcdFx0XHQnd2lkdGgnOiA2MDBcblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtYmVycy1jaGVja2JveCcpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRfb25DaGFuZ2VFbWFpbEludm9pY2VDaGVja2JveCgkbW9kYWwpXG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBPbiBFbWFpbCBJbnZvaWNlIENoZWNrYm94IENoYW5nZVxuXHRcdCAqXG5cdFx0ICogRGlzYWJsZSBzZW5kIGJ1dHRvbiBpZiBhbGwgaW52b2ljZSBudW1iZXIgY2hlY2tib3hlcyBhcmUgdW5jaGVja2VkLiBPdGhlcndpc2UgZW5hYmxlIHRoZSBzZW5kIGJ1dHRvbiBhZ2Fpbi5cblx0XHQgKi9cblx0XHR2YXIgX29uQ2hhbmdlRW1haWxJbnZvaWNlQ2hlY2tib3ggPSBmdW5jdGlvbigkbW9kYWwpIHtcblx0XHRcdGlmICgkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtYmVycy1jaGVja2JveCcpLmxlbmd0aCA+IDApIHtcblx0XHRcdFx0aWYgKCRtb2RhbC5maW5kKCcuaW52b2ljZS1udW1iZXJzLWNoZWNrYm94OmNoZWNrZWQnKS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdFx0JCgnLmJ0bi1zZW5kLWludm9pY2UtbWFpbCcpLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdCQoJy5idG4tc2VuZC1pbnZvaWNlLW1haWwnKS5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xuXHRcdFx0XHR9XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQkKCcuYnRuLXNlbmQtaW52b2ljZS1tYWlsJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2dldE1vZGFsQnV0dG9ucyA9IGZ1bmN0aW9uKCRmb3JtKSB7XG5cdFx0XHR2YXIgYnV0dG9ucyA9IFtcblx0XHRcdFx0e1xuXHRcdFx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Nsb3NlJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuJyxcblx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XTtcblx0XHRcdHN3aXRjaCAob3B0aW9ucy5hY3Rpb24pIHtcblx0XHRcdFx0Y2FzZSAnZGVsZXRlJzpcblx0XHRcdFx0Y2FzZSAnbXVsdGlfZGVsZXRlJzpcblx0XHRcdFx0XHRidXR0b25zLnB1c2goe1xuXHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZGVsZXRlJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHRcdCdjbGFzcyc6ICdidG4gYnRuLXByaW1hcnknLFxuXHRcdFx0XHRcdFx0J2NsaWNrJzogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdCRmb3JtLnN1Ym1pdCgpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdhZGRfdHJhY2tpbmdfY29kZSc6XG5cdFx0XHRcdFx0YnV0dG9ucy5wdXNoKHtcblx0XHRcdFx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2FkZCcsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuIGJ0bi1wcmltYXJ5Jyxcblx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcdFx0XHRcdF9hZGRUcmFja2luZ0NvZGVGcm9tT3ZlcnZpZXcoZXZlbnQpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICd1cGRhdGVfb3JkZXJzX3N0YXR1cyc6XG5cdFx0XHRcdFx0YnV0dG9ucy5wdXNoKHtcblx0XHRcdFx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2V4ZWN1dGUnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0J2NsYXNzJzogJ2J0biBidG4tcHJpbWFyeScsXG5cdFx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0XHRcdFx0XHQkZm9ybS5zdWJtaXQoKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnbXVsdGlfY2FuY2VsJzpcblx0XHRcdFx0XHRidXR0b25zLnB1c2goe1xuXHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc2VuZCcsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuIGJ0bi1wcmltYXJ5Jyxcblx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcdFx0XHRcdC8vZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHRcdFx0Ly9nbV9jYW5jZWwoJ2dtX3NlbmRfb3JkZXIucGhwJywgJyZ0eXBlPWNhbmNlbCcsICdDQU5DRUwnKTtcblx0XHRcdFx0XHRcdFx0JGZvcm0uc3VibWl0KCk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UgJ2VtYWlsX2ludm9pY2UnOlxuXHRcdFx0XHRcdGJ1dHRvbnMucHVzaCh7XG5cdFx0XHRcdFx0XHQndGV4dCc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdzZW5kJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHRcdCdjbGFzcyc6ICdidG4gYnRuLXByaW1hcnkgYnRuLXNlbmQtaW52b2ljZS1tYWlsJyxcblx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHR2YXIgdXJsID0gJ2dtX3BkZl9vcmRlci5waHA/b0lEPScgKyAkZm9ybS5kYXRhKCdvcmRlcklkJylcblx0XHRcdFx0XHRcdFx0XHQrICcmdHlwZT1pbnZvaWNlJm1haWw9MSZnbV9xdWlja19tYWlsPTEnO1xuXHRcdFx0XHRcdFx0XHR2YXIgZGF0YSA9ICRmb3JtLmZpbmQoJ2Zvcm0nKS5zZXJpYWxpemUoKTtcblx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdCQuYWpheCh7dXJsLCBkYXRhLCB0eXBlOiAnUE9TVCcsIGRhdGFUeXBlOiAnaHRtbCd9KS5zdWNjZXNzKChyZXNwb25zZSkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdCQoJy5lbWFpbC1pbnZvaWNlLWZvcm0nKS5oaWRlKCk7XG5cdFx0XHRcdFx0XHRcdFx0JCgnLmVtYWlsLWludm9pY2Utc3VjY2VzcycpLnNob3coKTtcblx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHQkKCcuYnRuLXNlbmQtaW52b2ljZS1tYWlsJykuaGlkZSgpO1xuXHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0cmV0dXJuIGJ1dHRvbnM7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2FkZFRyYWNraW5nQ29kZUZyb21PdmVydmlldyA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFxuXHRcdFx0dmFyIHRyYWNraW5nX2NvZGUgPSAkKCcjcGFyY2VsX3NlcnZpY2VfdHJhY2tpbmdfY29kZScpLnZhbCgpO1xuXHRcdFx0aWYgKHRyYWNraW5nX2NvZGUgPT09ICcnKSB7XG5cdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JC5hamF4KHtcblx0XHRcdFx0J3R5cGUnOiAnUE9TVCcsXG5cdFx0XHRcdCd1cmwnOiAncmVxdWVzdF9wb3J0LnBocD9tb2R1bGU9UGFyY2VsU2VydmljZXMmYWN0aW9uPWFkZF90cmFja2luZ19jb2RlJyxcblx0XHRcdFx0J3RpbWVvdXQnOiAzMDAwMCxcblx0XHRcdFx0J2RhdGFUeXBlJzogJ2pzb24nLFxuXHRcdFx0XHQnY29udGV4dCc6IHRoaXMsXG5cdFx0XHRcdCdkYXRhJzoge1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCd0cmFja2luZ19jb2RlJzogdHJhY2tpbmdfY29kZSxcblx0XHRcdFx0XHQnc2VydmljZV9pZCc6ICQoJyNwYXJjZWxfc2VydmljZXNfZHJvcGRvd24gb3B0aW9uOnNlbGVjdGVkJykudmFsKCksXG5cdFx0XHRcdFx0J29yZGVyX2lkJzogJHRoaXMuZGF0YSgnb3JkZXJfaWQnKSxcblx0XHRcdFx0XHQncGFnZV90b2tlbic6ICQoJy5wYWdlX3Rva2VuJykudmFsKClcblx0XHRcdFx0fSxcblx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0ZG9jdW1lbnQubG9jYXRpb24ucmVsb2FkKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX3JlYWRTZWxlY3RlZE9yZGVycyA9IGZ1bmN0aW9uKCRmb3JtKSB7XG5cdFx0XHR2YXIgb3JkZXJJZHMgPSBbXTtcblx0XHRcdFxuXHRcdFx0JGNoZWNrYm94ZXMuZmlsdGVyKCc6Y2hlY2tlZCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCRmb3JtLmFwcGVuZCgnPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBuYW1lPVwiZ21fbXVsdGlfc3RhdHVzW11cIiB2YWx1ZT1cIicgKyAkKHRoaXMpLnZhbCgpICtcblx0XHRcdFx0XHQnXCIgLz4nKTtcblx0XHRcdFx0XG5cdFx0XHRcdG9yZGVySWRzLnB1c2goJCh0aGlzKS52YWwoKSk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JGZvcm0uZmluZCgnLnNlbGVjdGVkX29yZGVycycpLnRleHQob3JkZXJJZHMuam9pbignLCAnKSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0c3dpdGNoIChvcHRpb25zLmFjdGlvbikge1xuXHRcdFx0XHRjYXNlICdkZWxldGUnOlxuXHRcdFx0XHRcdCR0aGlzLm9uKCdjbGljaycsIF9vcGVuRGVsZXRlRGlhbG9nKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnbXVsdGlfZGVsZXRlJzpcblx0XHRcdFx0XHQkdGhpcy5vbignY2xpY2snLCBfb3Blbk11bHRpRGVsZXRlRGlhbG9nKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnYWRkX3RyYWNraW5nX2NvZGUnOlxuXHRcdFx0XHRcdCR0aGlzLm9uKCdjbGljaycsIF9vcGVuVHJhY2tpbmdDb2RlRGlhbG9nKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAndXBkYXRlX29yZGVyc19zdGF0dXMnOlxuXHRcdFx0XHRcdCR0aGlzLm9uKCdjbGljaycsIF9vcGVuVXBkYXRlT3JkZXJzU3RhdHVzRGlhbG9nKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnbXVsdGlfY2FuY2VsJzpcblx0XHRcdFx0XHQkdGhpcy5vbignY2xpY2snLCBfb3Blbk11bHRpQ2FuY2VsRGlhbG9nKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnZW1haWxfaW52b2ljZSc6XG5cdFx0XHRcdFx0JHRoaXMub24oJ2NsaWNrJywgX29wZW5FbWFpbEludm9pY2VEaWFsb2cpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAob3B0aW9ucy5jb250YWluZXIgPT09ICd0cmFja2luZ19jb2RlX3dyYXBwZXInKSB7XG5cdFx0XHRcdCR0aGlzLm9uKCdjbGljaycsICcuYnRuLWRlbGV0ZScsIF9vcGVuVHJhY2tpbmdDb2RlRGVsZXRlRGlhbG9nKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
