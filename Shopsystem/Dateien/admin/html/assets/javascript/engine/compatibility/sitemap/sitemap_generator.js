'use strict';

/* --------------------------------------------------------------
 sitemap_generator.js 2016-08-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Sitemap Generator Controller
 *
 * This module will execute the sitemap generation
 *
 * @module Compatibility/sitemap_generator
 */
gx.compatibility.module('sitemap_generator', [gx.source + '/libs/info_messages', 'loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES 
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Loading Spinner Selector 
  * 
  * @type {object}
  */
	$spinner,


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = { url: 'gm_sitemap_creator.php' },


	/**
  * Final Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Reference to the info messages library
  * 
  * @type {object}
  */
	messages = jse.libs.info_messages,


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	var _createSitemapXml = function _createSitemapXml() {
		$.ajax({
			url: options.url,
			data: options.params
		})
		// On success
		.done(function (response) {
			messages.addSuccess(response);
			jse.libs.loading_spinner.hide($spinner);
		})
		// On Failure
		.fail(function (response) {
			jse.core.debug.error('Prepare Content Error: ', response);
		});
	};

	var _prepareCategories = function _prepareCategories(deferred) {
		var deferred = deferred || $.Deferred();

		$.ajax({
			url: options.url,
			data: {
				action: 'prepare_categories',
				page_token: jse.core.config.get('pageToken')
			},
			dataType: 'json'
		})
		// On success
		.done(function (response) {
			if (response.repeat === true) {
				_prepareCategories(deferred);
			} else {
				deferred.resolve();
			}
		})
		// On Failure
		.fail(function (response) {
			jse.core.debug.error('Prepare Categories Error: ', response);
			deferred.reject();
		});

		return deferred.promise();
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', function () {
			$spinner = jse.libs.loading_spinner.show($this.parents().eq(2));
			$.when(_prepareCategories()).done(_createSitemapXml);
			$this.blur();
			return false;
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNpdGVtYXAvc2l0ZW1hcF9nZW5lcmF0b3IuanMiXSwibmFtZXMiOlsiZ3giLCJjb21wYXRpYmlsaXR5IiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRzcGlubmVyIiwiZGVmYXVsdHMiLCJ1cmwiLCJvcHRpb25zIiwiZXh0ZW5kIiwibWVzc2FnZXMiLCJqc2UiLCJsaWJzIiwiaW5mb19tZXNzYWdlcyIsIl9jcmVhdGVTaXRlbWFwWG1sIiwiYWpheCIsInBhcmFtcyIsImRvbmUiLCJyZXNwb25zZSIsImFkZFN1Y2Nlc3MiLCJsb2FkaW5nX3NwaW5uZXIiLCJoaWRlIiwiZmFpbCIsImNvcmUiLCJkZWJ1ZyIsImVycm9yIiwiX3ByZXBhcmVDYXRlZ29yaWVzIiwiZGVmZXJyZWQiLCJEZWZlcnJlZCIsImFjdGlvbiIsInBhZ2VfdG9rZW4iLCJjb25maWciLCJnZXQiLCJkYXRhVHlwZSIsInJlcGVhdCIsInJlc29sdmUiLCJyZWplY3QiLCJwcm9taXNlIiwiaW5pdCIsIm9uIiwic2hvdyIsInBhcmVudHMiLCJlcSIsIndoZW4iLCJibHVyIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyxtQkFERCxFQUdDLENBQ0NGLEdBQUdHLE1BQUgsR0FBWSxxQkFEYixFQUVDLGlCQUZELENBSEQsRUFRQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsU0FiRDs7O0FBZUM7Ozs7O0FBS0FDLFlBQVcsRUFBQ0MsS0FBSyx3QkFBTixFQXBCWjs7O0FBc0JDOzs7OztBQUtBQyxXQUFVSixFQUFFSyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJILFFBQW5CLEVBQTZCSixJQUE3QixDQTNCWDs7O0FBNkJDOzs7OztBQUtBUSxZQUFXQyxJQUFJQyxJQUFKLENBQVNDLGFBbENyQjs7O0FBb0NDOzs7OztBQUtBYixVQUFTLEVBekNWOztBQTJDQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSWMsb0JBQW9CLFNBQXBCQSxpQkFBb0IsR0FBVztBQUNsQ1YsSUFBRVcsSUFBRixDQUFPO0FBQ0xSLFFBQUtDLFFBQVFELEdBRFI7QUFFTEwsU0FBTU0sUUFBUVE7QUFGVCxHQUFQO0FBSUM7QUFKRCxHQUtFQyxJQUxGLENBS08sVUFBU0MsUUFBVCxFQUFtQjtBQUN4QlIsWUFBU1MsVUFBVCxDQUFvQkQsUUFBcEI7QUFDQVAsT0FBSUMsSUFBSixDQUFTUSxlQUFULENBQXlCQyxJQUF6QixDQUE4QmhCLFFBQTlCO0FBQ0EsR0FSRjtBQVNDO0FBVEQsR0FVRWlCLElBVkYsQ0FVTyxVQUFTSixRQUFULEVBQW1CO0FBQ3hCUCxPQUFJWSxJQUFKLENBQVNDLEtBQVQsQ0FBZUMsS0FBZixDQUFxQix5QkFBckIsRUFBZ0RQLFFBQWhEO0FBQ0EsR0FaRjtBQWFBLEVBZEQ7O0FBZ0JBLEtBQUlRLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQVNDLFFBQVQsRUFBbUI7QUFDM0MsTUFBSUEsV0FBV0EsWUFBWXZCLEVBQUV3QixRQUFGLEVBQTNCOztBQUVBeEIsSUFBRVcsSUFBRixDQUFPO0FBQ0xSLFFBQUtDLFFBQVFELEdBRFI7QUFFTEwsU0FBTTtBQUNMMkIsWUFBUSxvQkFESDtBQUVMQyxnQkFBWW5CLElBQUlZLElBQUosQ0FBU1EsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFGUCxJQUZEO0FBTUxDLGFBQVU7QUFOTCxHQUFQO0FBUUM7QUFSRCxHQVNFaEIsSUFURixDQVNPLFVBQVNDLFFBQVQsRUFBbUI7QUFDeEIsT0FBSUEsU0FBU2dCLE1BQVQsS0FBb0IsSUFBeEIsRUFBOEI7QUFDN0JSLHVCQUFtQkMsUUFBbkI7QUFDQSxJQUZELE1BRU87QUFDTkEsYUFBU1EsT0FBVDtBQUNBO0FBQ0QsR0FmRjtBQWdCQztBQWhCRCxHQWlCRWIsSUFqQkYsQ0FpQk8sVUFBU0osUUFBVCxFQUFtQjtBQUN4QlAsT0FBSVksSUFBSixDQUFTQyxLQUFULENBQWVDLEtBQWYsQ0FBcUIsNEJBQXJCLEVBQW1EUCxRQUFuRDtBQUNBUyxZQUFTUyxNQUFUO0FBQ0EsR0FwQkY7O0FBc0JBLFNBQU9ULFNBQVNVLE9BQVQsRUFBUDtBQUNBLEVBMUJEOztBQTRCQTtBQUNBO0FBQ0E7O0FBRUFyQyxRQUFPc0MsSUFBUCxHQUFjLFVBQVNyQixJQUFULEVBQWU7QUFDNUJkLFFBQU1vQyxFQUFOLENBQVMsT0FBVCxFQUFrQixZQUFXO0FBQzVCbEMsY0FBV00sSUFBSUMsSUFBSixDQUFTUSxlQUFULENBQXlCb0IsSUFBekIsQ0FBOEJyQyxNQUFNc0MsT0FBTixHQUFnQkMsRUFBaEIsQ0FBbUIsQ0FBbkIsQ0FBOUIsQ0FBWDtBQUNBdEMsS0FBRXVDLElBQUYsQ0FBT2pCLG9CQUFQLEVBQTZCVCxJQUE3QixDQUFrQ0gsaUJBQWxDO0FBQ0FYLFNBQU15QyxJQUFOO0FBQ0EsVUFBTyxLQUFQO0FBQ0EsR0FMRDs7QUFPQTNCO0FBQ0EsRUFURDs7QUFXQSxRQUFPakIsTUFBUDtBQUNBLENBM0hGIiwiZmlsZSI6InNpdGVtYXAvc2l0ZW1hcF9nZW5lcmF0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNpdGVtYXBfZ2VuZXJhdG9yLmpzIDIwMTYtMDgtMjJcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIFNpdGVtYXAgR2VuZXJhdG9yIENvbnRyb2xsZXJcbiAqXG4gKiBUaGlzIG1vZHVsZSB3aWxsIGV4ZWN1dGUgdGhlIHNpdGVtYXAgZ2VuZXJhdGlvblxuICpcbiAqIEBtb2R1bGUgQ29tcGF0aWJpbGl0eS9zaXRlbWFwX2dlbmVyYXRvclxuICovXG5neC5jb21wYXRpYmlsaXR5Lm1vZHVsZShcblx0J3NpdGVtYXBfZ2VuZXJhdG9yJyxcblx0XG5cdFtcblx0XHRneC5zb3VyY2UgKyAnL2xpYnMvaW5mb19tZXNzYWdlcycsXG5cdFx0J2xvYWRpbmdfc3Bpbm5lcidcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTG9hZGluZyBTcGlubmVyIFNlbGVjdG9yIFxuXHRcdFx0ICogXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkc3Bpbm5lcixcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt1cmw6ICdnbV9zaXRlbWFwX2NyZWF0b3IucGhwJ30sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFJlZmVyZW5jZSB0byB0aGUgaW5mbyBtZXNzYWdlcyBsaWJyYXJ5XG5cdFx0XHQgKiBcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1lc3NhZ2VzID0ganNlLmxpYnMuaW5mb19tZXNzYWdlcyxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyIF9jcmVhdGVTaXRlbWFwWG1sID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHRcdHVybDogb3B0aW9ucy51cmwsXG5cdFx0XHRcdFx0ZGF0YTogb3B0aW9ucy5wYXJhbXNcblx0XHRcdFx0fSlcblx0XHRcdFx0Ly8gT24gc3VjY2Vzc1xuXHRcdFx0XHQuZG9uZShmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdG1lc3NhZ2VzLmFkZFN1Y2Nlc3MocmVzcG9uc2UpO1xuXHRcdFx0XHRcdGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5oaWRlKCRzcGlubmVyKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0Ly8gT24gRmFpbHVyZVxuXHRcdFx0XHQuZmFpbChmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdGpzZS5jb3JlLmRlYnVnLmVycm9yKCdQcmVwYXJlIENvbnRlbnQgRXJyb3I6ICcsIHJlc3BvbnNlKTtcblx0XHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX3ByZXBhcmVDYXRlZ29yaWVzID0gZnVuY3Rpb24oZGVmZXJyZWQpIHtcblx0XHRcdHZhciBkZWZlcnJlZCA9IGRlZmVycmVkIHx8ICQuRGVmZXJyZWQoKTsgXG5cdFx0XHRcblx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdFx0dXJsOiBvcHRpb25zLnVybCxcblx0XHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0XHRhY3Rpb246ICdwcmVwYXJlX2NhdGVnb3JpZXMnLFxuXHRcdFx0XHRcdFx0cGFnZV90b2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJylcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdGRhdGFUeXBlOiAnanNvbidcblx0XHRcdFx0fSlcblx0XHRcdFx0Ly8gT24gc3VjY2Vzc1xuXHRcdFx0XHQuZG9uZShmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdGlmIChyZXNwb25zZS5yZXBlYXQgPT09IHRydWUpIHtcblx0XHRcdFx0XHRcdF9wcmVwYXJlQ2F0ZWdvcmllcyhkZWZlcnJlZCk7IFxuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRkZWZlcnJlZC5yZXNvbHZlKCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KVxuXHRcdFx0XHQvLyBPbiBGYWlsdXJlXG5cdFx0XHRcdC5mYWlsKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0anNlLmNvcmUuZGVidWcuZXJyb3IoJ1ByZXBhcmUgQ2F0ZWdvcmllcyBFcnJvcjogJywgcmVzcG9uc2UpO1xuXHRcdFx0XHRcdGRlZmVycmVkLnJlamVjdCgpO1xuXHRcdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIGRlZmVycmVkLnByb21pc2UoKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkdGhpcy5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcy5wYXJlbnRzKCkuZXEoMikpO1xuXHRcdFx0XHQkLndoZW4oX3ByZXBhcmVDYXRlZ29yaWVzKCkpLmRvbmUoX2NyZWF0ZVNpdGVtYXBYbWwpOyBcblx0XHRcdFx0JHRoaXMuYmx1cigpO1xuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
