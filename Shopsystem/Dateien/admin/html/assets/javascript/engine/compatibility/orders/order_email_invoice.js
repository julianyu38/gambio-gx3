'use strict';

/* --------------------------------------------------------------
 order_email_invoice.js 2016-12-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * This module handels the click event of the invoice email button
 *
 * @module Compatibility/order_email_invoice
 */
gx.compatibility.module('order_email_invoice', ['modal'],

/**  @lends module:Compatibility/order_email_invoice */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @var {object}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final Options
  *
  * @var {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	/**
  * On Email Invoice Click
  *
  * Display the email-invoice modal.
  */
	function _onEmailInvoiceClick() {
		var $modal = $('.email-invoice.modal');
		var url = jse.core.config.get('appUrl') + '/admin/admin.php';
		var data = {
			id: options.id,
			do: 'OrdersModalsAjax/GetEmailInvoiceSubject',
			pageToken: jse.core.config.get('pageToken')
		};
		var invoiceNumbersHtml = '';

		$modal.find('.customer-info').text('"' + options.name + '"');
		$modal.find('.email-address').val(options.email);

		$modal.data('orderId', options.id).modal('show');

		$.ajax({ url: url, data: data, dataType: 'json' }).done(function (response) {
			$modal.attr('data-gx-widget', 'single_checkbox');

			$modal.find('.subject').val(response.subject);
			if (response.invoiceIdExists) {
				$modal.find('.invoice-num-info').addClass('hidden');
				$modal.find('.no-invoice').removeClass('hidden');
			} else {
				$modal.find('.invoice-num-info').removeClass('hidden');
				$modal.find('.no-invoice').addClass('hidden');
			}

			if (Object.keys(response.invoiceNumbers).length <= 1) {
				$modal.find('.invoice-numbers').addClass('hidden');
			} else {
				$modal.find('.invoice-numbers').removeClass('hidden');
			}

			for (var invoiceId in response.invoiceNumbers) {
				invoiceNumbersHtml += '<p><input type="checkbox" name="invoice_ids[]" value="' + invoiceId + '" checked="checked" class="invoice-numbers-checkbox" /> ' + response.invoiceNumbers[invoiceId] + '</p>';
			}

			$modal.find('.invoice-numbers-checkboxes').html(invoiceNumbersHtml);

			$modal.find('.invoice-numbers-checkbox').on('change', _onChangeEmailInvoiceCheckbox);

			gx.widgets.init($modal);
		});
	}

	/**
  * On Email Invoice Checkbox Change
  *
  * Disable send button if all invoice number checkboxes are unchecked. Otherwise enable the send button again.
  */
	function _onChangeEmailInvoiceCheckbox() {
		var $modal = $('.email-invoice.modal');

		if ($modal.find('.invoice-numbers-checkbox').length > 0) {
			if ($modal.find('.invoice-numbers-checkbox:checked').length > 0) {
				$modal.find('.send').prop('disabled', false);
			} else {
				$modal.find('.send').prop('disabled', true);
			}
		} else {
			$modal.find('.send').prop('disabled', false);
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', _onEmailInvoiceClick);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vcmRlcl9lbWFpbF9pbnZvaWNlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfb25FbWFpbEludm9pY2VDbGljayIsIiRtb2RhbCIsInVybCIsImpzZSIsImNvcmUiLCJjb25maWciLCJnZXQiLCJpZCIsImRvIiwicGFnZVRva2VuIiwiaW52b2ljZU51bWJlcnNIdG1sIiwiZmluZCIsInRleHQiLCJuYW1lIiwidmFsIiwiZW1haWwiLCJtb2RhbCIsImFqYXgiLCJkYXRhVHlwZSIsImRvbmUiLCJyZXNwb25zZSIsImF0dHIiLCJzdWJqZWN0IiwiaW52b2ljZUlkRXhpc3RzIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsIk9iamVjdCIsImtleXMiLCJpbnZvaWNlTnVtYmVycyIsImxlbmd0aCIsImludm9pY2VJZCIsImh0bWwiLCJvbiIsIl9vbkNoYW5nZUVtYWlsSW52b2ljZUNoZWNrYm94Iiwid2lkZ2V0cyIsImluaXQiLCJwcm9wIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCLENBQ0MscUJBREQsRUFHQyxDQUFDLE9BQUQsQ0FIRDs7QUFLQzs7QUFFQSxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXLEVBQWpCOztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTs7Ozs7QUFLQSxVQUFTTyxvQkFBVCxHQUFnQztBQUMvQixNQUFNQyxTQUFTTCxFQUFFLHNCQUFGLENBQWY7QUFDQSxNQUFNTSxNQUFNQyxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLGtCQUE1QztBQUNBLE1BQU1aLE9BQU87QUFDWmEsT0FBSVQsUUFBUVMsRUFEQTtBQUVaQyxPQUFJLHlDQUZRO0FBR1pDLGNBQVdOLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFIQyxHQUFiO0FBS0EsTUFBSUkscUJBQXFCLEVBQXpCOztBQUVBVCxTQUFPVSxJQUFQLENBQVksZ0JBQVosRUFBOEJDLElBQTlCLE9BQXVDZCxRQUFRZSxJQUEvQztBQUNBWixTQUFPVSxJQUFQLENBQVksZ0JBQVosRUFBOEJHLEdBQTlCLENBQWtDaEIsUUFBUWlCLEtBQTFDOztBQUVBZCxTQUNFUCxJQURGLENBQ08sU0FEUCxFQUNrQkksUUFBUVMsRUFEMUIsRUFFRVMsS0FGRixDQUVRLE1BRlI7O0FBSUFwQixJQUFFcUIsSUFBRixDQUFPLEVBQUNmLFFBQUQsRUFBTVIsVUFBTixFQUFZd0IsVUFBVSxNQUF0QixFQUFQLEVBQXNDQyxJQUF0QyxDQUEyQyxVQUFDQyxRQUFELEVBQWM7QUFDeERuQixVQUFPb0IsSUFBUCxDQUFZLGdCQUFaLEVBQThCLGlCQUE5Qjs7QUFFQXBCLFVBQU9VLElBQVAsQ0FBWSxVQUFaLEVBQXdCRyxHQUF4QixDQUE0Qk0sU0FBU0UsT0FBckM7QUFDQSxPQUFJRixTQUFTRyxlQUFiLEVBQThCO0FBQzdCdEIsV0FBT1UsSUFBUCxDQUFZLG1CQUFaLEVBQWlDYSxRQUFqQyxDQUEwQyxRQUExQztBQUNBdkIsV0FBT1UsSUFBUCxDQUFZLGFBQVosRUFBMkJjLFdBQTNCLENBQXVDLFFBQXZDO0FBQ0EsSUFIRCxNQUdPO0FBQ054QixXQUFPVSxJQUFQLENBQVksbUJBQVosRUFBaUNjLFdBQWpDLENBQTZDLFFBQTdDO0FBQ0F4QixXQUFPVSxJQUFQLENBQVksYUFBWixFQUEyQmEsUUFBM0IsQ0FBb0MsUUFBcEM7QUFDQTs7QUFFRCxPQUFJRSxPQUFPQyxJQUFQLENBQVlQLFNBQVNRLGNBQXJCLEVBQXFDQyxNQUFyQyxJQUErQyxDQUFuRCxFQUFzRDtBQUNyRDVCLFdBQU9VLElBQVAsQ0FBWSxrQkFBWixFQUFnQ2EsUUFBaEMsQ0FBeUMsUUFBekM7QUFDQSxJQUZELE1BRU87QUFDTnZCLFdBQU9VLElBQVAsQ0FBWSxrQkFBWixFQUFnQ2MsV0FBaEMsQ0FBNEMsUUFBNUM7QUFDQTs7QUFFRCxRQUFLLElBQUlLLFNBQVQsSUFBc0JWLFNBQVNRLGNBQS9CLEVBQStDO0FBQzlDbEIsMEJBQ0MsMkRBQTJEb0IsU0FBM0QsR0FDRSwwREFERixHQUVFVixTQUFTUSxjQUFULENBQXdCRSxTQUF4QixDQUZGLEdBRXVDLE1BSHhDO0FBSUE7O0FBRUQ3QixVQUFPVSxJQUFQLENBQVksNkJBQVosRUFBMkNvQixJQUEzQyxDQUFnRHJCLGtCQUFoRDs7QUFFQVQsVUFBT1UsSUFBUCxDQUFZLDJCQUFaLEVBQXlDcUIsRUFBekMsQ0FBNEMsUUFBNUMsRUFBc0RDLDZCQUF0RDs7QUFFQTFDLE1BQUcyQyxPQUFILENBQVdDLElBQVgsQ0FBZ0JsQyxNQUFoQjtBQUNBLEdBOUJEO0FBK0JBOztBQUVEOzs7OztBQUtBLFVBQVNnQyw2QkFBVCxHQUF5QztBQUN4QyxNQUFNaEMsU0FBU0wsRUFBRSxzQkFBRixDQUFmOztBQUVBLE1BQUlLLE9BQU9VLElBQVAsQ0FBWSwyQkFBWixFQUF5Q2tCLE1BQXpDLEdBQWtELENBQXRELEVBQXlEO0FBQ3hELE9BQUk1QixPQUFPVSxJQUFQLENBQVksbUNBQVosRUFBaURrQixNQUFqRCxHQUEwRCxDQUE5RCxFQUFpRTtBQUNoRTVCLFdBQU9VLElBQVAsQ0FBWSxPQUFaLEVBQXFCeUIsSUFBckIsQ0FBMEIsVUFBMUIsRUFBc0MsS0FBdEM7QUFDQSxJQUZELE1BRU87QUFDTm5DLFdBQU9VLElBQVAsQ0FBWSxPQUFaLEVBQXFCeUIsSUFBckIsQ0FBMEIsVUFBMUIsRUFBc0MsSUFBdEM7QUFDQTtBQUNELEdBTkQsTUFNTztBQUNObkMsVUFBT1UsSUFBUCxDQUFZLE9BQVosRUFBcUJ5QixJQUFyQixDQUEwQixVQUExQixFQUFzQyxLQUF0QztBQUNBO0FBQ0Q7O0FBR0Q7QUFDQTtBQUNBOztBQUVBM0MsUUFBTzBDLElBQVAsR0FBYyxVQUFTaEIsSUFBVCxFQUFlO0FBQzVCeEIsUUFBTXFDLEVBQU4sQ0FBUyxPQUFULEVBQWtCaEMsb0JBQWxCOztBQUVBbUI7QUFDQSxFQUpEOztBQU1BLFFBQU8xQixNQUFQO0FBQ0EsQ0FqSUYiLCJmaWxlIjoib3JkZXJzL29yZGVyX2VtYWlsX2ludm9pY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gb3JkZXJfZW1haWxfaW52b2ljZS5qcyAyMDE2LTEyLTA4XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIFRoaXMgbW9kdWxlIGhhbmRlbHMgdGhlIGNsaWNrIGV2ZW50IG9mIHRoZSBpbnZvaWNlIGVtYWlsIGJ1dHRvblxyXG4gKlxyXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvb3JkZXJfZW1haWxfaW52b2ljZVxyXG4gKi9cclxuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXHJcblx0J29yZGVyX2VtYWlsX2ludm9pY2UnLFxyXG5cdFxyXG5cdFsnbW9kYWwnXSxcclxuXHRcclxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9vcmRlcl9lbWFpbF9pbnZvaWNlICovXHJcblx0XHJcblx0ZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0XHJcblx0XHQndXNlIHN0cmljdCc7XHJcblx0XHRcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdFx0ICpcclxuXHRcdCAqIEB2YXIge29iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIERlZmF1bHQgT3B0aW9uc1xyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IGRlZmF1bHRzID0ge307XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRmluYWwgT3B0aW9uc1xyXG5cdFx0ICpcclxuXHRcdCAqIEB2YXIge29iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW9kdWxlIE9iamVjdFxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIEVtYWlsIEludm9pY2UgQ2xpY2tcclxuXHRcdCAqXHJcblx0XHQgKiBEaXNwbGF5IHRoZSBlbWFpbC1pbnZvaWNlIG1vZGFsLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25FbWFpbEludm9pY2VDbGljaygpIHtcclxuXHRcdFx0Y29uc3QgJG1vZGFsID0gJCgnLmVtYWlsLWludm9pY2UubW9kYWwnKTtcclxuXHRcdFx0Y29uc3QgdXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocCc7XHJcblx0XHRcdGNvbnN0IGRhdGEgPSB7XHJcblx0XHRcdFx0aWQ6IG9wdGlvbnMuaWQsXHJcblx0XHRcdFx0ZG86ICdPcmRlcnNNb2RhbHNBamF4L0dldEVtYWlsSW52b2ljZVN1YmplY3QnLFxyXG5cdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJylcclxuXHRcdFx0fTtcclxuXHRcdFx0bGV0IGludm9pY2VOdW1iZXJzSHRtbCA9ICcnO1xyXG5cdFx0XHRcclxuXHRcdFx0JG1vZGFsLmZpbmQoJy5jdXN0b21lci1pbmZvJykudGV4dChgXCIke29wdGlvbnMubmFtZX1cImApO1xyXG5cdFx0XHQkbW9kYWwuZmluZCgnLmVtYWlsLWFkZHJlc3MnKS52YWwob3B0aW9ucy5lbWFpbCk7XHJcblx0XHRcdFxyXG5cdFx0XHQkbW9kYWxcclxuXHRcdFx0XHQuZGF0YSgnb3JkZXJJZCcsIG9wdGlvbnMuaWQpXHJcblx0XHRcdFx0Lm1vZGFsKCdzaG93Jyk7XHJcblx0XHRcdFxyXG5cdFx0XHQkLmFqYXgoe3VybCwgZGF0YSwgZGF0YVR5cGU6ICdqc29uJ30pLmRvbmUoKHJlc3BvbnNlKSA9PiB7XHJcblx0XHRcdFx0JG1vZGFsLmF0dHIoJ2RhdGEtZ3gtd2lkZ2V0JywgJ3NpbmdsZV9jaGVja2JveCcpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdCRtb2RhbC5maW5kKCcuc3ViamVjdCcpLnZhbChyZXNwb25zZS5zdWJqZWN0KTtcclxuXHRcdFx0XHRpZiAocmVzcG9uc2UuaW52b2ljZUlkRXhpc3RzKSB7XHJcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtLWluZm8nKS5hZGRDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLm5vLWludm9pY2UnKS5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcuaW52b2ljZS1udW0taW5mbycpLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcclxuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcubm8taW52b2ljZScpLmFkZENsYXNzKCdoaWRkZW4nKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKE9iamVjdC5rZXlzKHJlc3BvbnNlLmludm9pY2VOdW1iZXJzKS5sZW5ndGggPD0gMSkge1xyXG5cdFx0XHRcdFx0JG1vZGFsLmZpbmQoJy5pbnZvaWNlLW51bWJlcnMnKS5hZGRDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdCRtb2RhbC5maW5kKCcuaW52b2ljZS1udW1iZXJzJykucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRmb3IgKGxldCBpbnZvaWNlSWQgaW4gcmVzcG9uc2UuaW52b2ljZU51bWJlcnMpIHtcclxuXHRcdFx0XHRcdGludm9pY2VOdW1iZXJzSHRtbCArPVxyXG5cdFx0XHRcdFx0XHQnPHA+PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIG5hbWU9XCJpbnZvaWNlX2lkc1tdXCIgdmFsdWU9XCInICsgaW52b2ljZUlkXHJcblx0XHRcdFx0XHRcdCsgJ1wiIGNoZWNrZWQ9XCJjaGVja2VkXCIgY2xhc3M9XCJpbnZvaWNlLW51bWJlcnMtY2hlY2tib3hcIiAvPiAnXHJcblx0XHRcdFx0XHRcdCsgcmVzcG9uc2UuaW52b2ljZU51bWJlcnNbaW52b2ljZUlkXSArICc8L3A+JztcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JG1vZGFsLmZpbmQoJy5pbnZvaWNlLW51bWJlcnMtY2hlY2tib3hlcycpLmh0bWwoaW52b2ljZU51bWJlcnNIdG1sKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQkbW9kYWwuZmluZCgnLmludm9pY2UtbnVtYmVycy1jaGVja2JveCcpLm9uKCdjaGFuZ2UnLCBfb25DaGFuZ2VFbWFpbEludm9pY2VDaGVja2JveCk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Z3gud2lkZ2V0cy5pbml0KCRtb2RhbCk7XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIEVtYWlsIEludm9pY2UgQ2hlY2tib3ggQ2hhbmdlXHJcblx0XHQgKlxyXG5cdFx0ICogRGlzYWJsZSBzZW5kIGJ1dHRvbiBpZiBhbGwgaW52b2ljZSBudW1iZXIgY2hlY2tib3hlcyBhcmUgdW5jaGVja2VkLiBPdGhlcndpc2UgZW5hYmxlIHRoZSBzZW5kIGJ1dHRvbiBhZ2Fpbi5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uQ2hhbmdlRW1haWxJbnZvaWNlQ2hlY2tib3goKSB7XHJcblx0XHRcdGNvbnN0ICRtb2RhbCA9ICQoJy5lbWFpbC1pbnZvaWNlLm1vZGFsJyk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoJG1vZGFsLmZpbmQoJy5pbnZvaWNlLW51bWJlcnMtY2hlY2tib3gnKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0aWYgKCRtb2RhbC5maW5kKCcuaW52b2ljZS1udW1iZXJzLWNoZWNrYm94OmNoZWNrZWQnKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0XHQkbW9kYWwuZmluZCgnLnNlbmQnKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0JG1vZGFsLmZpbmQoJy5zZW5kJykucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0JG1vZGFsLmZpbmQoJy5zZW5kJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdCR0aGlzLm9uKCdjbGljaycsIF9vbkVtYWlsSW52b2ljZUNsaWNrKTtcclxuXHRcdFx0XHJcblx0XHRcdGRvbmUoKTtcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdHJldHVybiBtb2R1bGU7XHJcblx0fSk7XHJcbiJdfQ==
