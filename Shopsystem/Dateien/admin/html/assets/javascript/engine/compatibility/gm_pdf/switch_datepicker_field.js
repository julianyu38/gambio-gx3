'use strict';

/* --------------------------------------------------------------
 switch_datepicker_field.js 2016-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.compatibility.module('switch_datepicker_field', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		current_select: '#GM_PDF_INVOICE_USE_CURRENT_DATE',
		notice: '.manual-invoice-date-notice'
	},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ------------------------------------------------------------------------

	var _checkVisibility = function _checkVisibility() {
		if ($(options.current_select).val() == 1) {
			$this.attr('disabled', 'disabled');
			$(options.notice).hide();
		} else {
			$this.removeAttr('disabled');
			$(options.notice).show();
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {

		_checkVisibility();

		$(options.current_select).on('change', function () {
			_checkVisibility();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdtX3BkZi9zd2l0Y2hfZGF0ZXBpY2tlcl9maWVsZC5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJjdXJyZW50X3NlbGVjdCIsIm5vdGljZSIsIm9wdGlvbnMiLCJleHRlbmQiLCJfY2hlY2tWaXNpYmlsaXR5IiwidmFsIiwiYXR0ciIsImhpZGUiLCJyZW1vdmVBdHRyIiwic2hvdyIsImluaXQiLCJkb25lIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyx5QkFERCxFQUdDLEVBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsWUFBVztBQUNWQyxrQkFBZ0Isa0NBRE47QUFFVkMsVUFBUTtBQUZFLEVBYlo7OztBQWtCQzs7Ozs7QUFLQUMsV0FBVUosRUFBRUssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CSixRQUFuQixFQUE2QkgsSUFBN0IsQ0F2Qlg7OztBQXlCQzs7Ozs7QUFLQUQsVUFBUyxFQTlCVjs7QUFnQ0E7QUFDQTtBQUNBOztBQUVBLEtBQUlTLG1CQUFtQixTQUFuQkEsZ0JBQW1CLEdBQVc7QUFDakMsTUFBSU4sRUFBRUksUUFBUUYsY0FBVixFQUEwQkssR0FBMUIsTUFBbUMsQ0FBdkMsRUFBMEM7QUFDekNSLFNBQU1TLElBQU4sQ0FBVyxVQUFYLEVBQXVCLFVBQXZCO0FBQ0FSLEtBQUVJLFFBQVFELE1BQVYsRUFBa0JNLElBQWxCO0FBQ0EsR0FIRCxNQUdPO0FBQ05WLFNBQU1XLFVBQU4sQ0FBaUIsVUFBakI7QUFDQVYsS0FBRUksUUFBUUQsTUFBVixFQUFrQlEsSUFBbEI7QUFDQTtBQUNELEVBUkQ7O0FBVUE7QUFDQTtBQUNBOztBQUVBZCxRQUFPZSxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlOztBQUU1QlA7O0FBRUFOLElBQUVJLFFBQVFGLGNBQVYsRUFBMEJZLEVBQTFCLENBQTZCLFFBQTdCLEVBQXVDLFlBQVc7QUFDakRSO0FBQ0EsR0FGRDs7QUFJQU87QUFDQSxFQVREOztBQVdBLFFBQU9oQixNQUFQO0FBQ0EsQ0EzRUYiLCJmaWxlIjoiZ21fcGRmL3N3aXRjaF9kYXRlcGlja2VyX2ZpZWxkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzd2l0Y2hfZGF0ZXBpY2tlcl9maWVsZC5qcyAyMDE2LTA5LTI5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdzd2l0Y2hfZGF0ZXBpY2tlcl9maWVsZCcsXG5cdFxuXHRbXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdGN1cnJlbnRfc2VsZWN0OiAnI0dNX1BERl9JTlZPSUNFX1VTRV9DVVJSRU5UX0RBVEUnLFxuXHRcdFx0XHRub3RpY2U6ICcubWFudWFsLWludm9pY2UtZGF0ZS1ub3RpY2UnXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBQUklWQVRFIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhciBfY2hlY2tWaXNpYmlsaXR5ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRpZiAoJChvcHRpb25zLmN1cnJlbnRfc2VsZWN0KS52YWwoKSA9PSAxKSB7XG5cdFx0XHRcdCR0aGlzLmF0dHIoJ2Rpc2FibGVkJywgJ2Rpc2FibGVkJyk7XG5cdFx0XHRcdCQob3B0aW9ucy5ub3RpY2UpLmhpZGUoKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCR0aGlzLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG5cdFx0XHRcdCQob3B0aW9ucy5ub3RpY2UpLnNob3coKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRcblx0XHRcdF9jaGVja1Zpc2liaWxpdHkoKTtcblx0XHRcdFxuXHRcdFx0JChvcHRpb25zLmN1cnJlbnRfc2VsZWN0KS5vbignY2hhbmdlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdF9jaGVja1Zpc2liaWxpdHkoKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTsiXX0=
