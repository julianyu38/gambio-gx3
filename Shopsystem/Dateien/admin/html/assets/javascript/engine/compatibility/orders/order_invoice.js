'use strict';

/* --------------------------------------------------------------
 order_invoice.js 2015-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## PayPal3 Payment Details on Order Page
 *
 * This module add the paypal3 payment informationen to the order details page.
 *
 * @module Compatibility/order_invoice
 */
gx.compatibility.module('order_invoice', ['modal'],

/**  @lends module:Compatibility/order_invoice */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	var _cancelInvoiceClickHandler = function _cancelInvoiceClickHandler() {
		var confirm = false;
		var $link = $(this);

		jse.libs.modal.message({
			minWidth: 400,
			title: jse.core.lang.translate('HEADING_MODAL_CANCELLATION_INVOICE', 'orders'),
			content: jse.core.lang.translate('TEXT_MODAL_CANCELLATION_INVOICE', 'orders').replace('%s', $link.data('invoice-number')),
			buttons: [{
				text: jse.core.lang.translate('no', 'lightbox_buttons'),
				click: function click() {
					$(this).dialog('close');
				}
			}, {
				text: jse.core.lang.translate('yes', 'lightbox_buttons'),
				click: function click() {
					$(this).dialog('close');
					window.open($link.attr('href'), '_blank');
				}
			}]
		});

		return false;
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.append($('table.invoice'));
		$this.find('.cancel-invoice').on('click', _cancelInvoiceClickHandler);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vcmRlcl9pbnZvaWNlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfY2FuY2VsSW52b2ljZUNsaWNrSGFuZGxlciIsImNvbmZpcm0iLCIkbGluayIsImpzZSIsImxpYnMiLCJtb2RhbCIsIm1lc3NhZ2UiLCJtaW5XaWR0aCIsInRpdGxlIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJjb250ZW50IiwicmVwbGFjZSIsImJ1dHRvbnMiLCJ0ZXh0IiwiY2xpY2siLCJkaWFsb2ciLCJ3aW5kb3ciLCJvcGVuIiwiYXR0ciIsImluaXQiLCJkb25lIiwiYXBwZW5kIiwiZmluZCIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyxlQURELEVBR0MsQ0FBQyxPQUFELENBSEQ7O0FBS0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFlBQVcsRUFiWjs7O0FBZUM7Ozs7O0FBS0FDLFdBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBcEJYOzs7QUFzQkM7Ozs7O0FBS0FELFVBQVMsRUEzQlY7O0FBNkJBLEtBQUlPLDZCQUE2QixTQUE3QkEsMEJBQTZCLEdBQVc7QUFDM0MsTUFBSUMsVUFBVSxLQUFkO0FBQ0EsTUFBSUMsUUFBUU4sRUFBRSxJQUFGLENBQVo7O0FBRUFPLE1BQUlDLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxPQUFmLENBQXVCO0FBQ3RCQyxhQUFVLEdBRFk7QUFFdEJDLFVBQU9MLElBQUlNLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG9DQUF4QixFQUE4RCxRQUE5RCxDQUZlO0FBR3RCQyxZQUFTVCxJQUFJTSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixpQ0FBeEIsRUFBMkQsUUFBM0QsRUFBcUVFLE9BQXJFLENBQTZFLElBQTdFLEVBQW1GWCxNQUFNUixJQUFOLENBQVcsZ0JBQVgsQ0FBbkYsQ0FIYTtBQUl0Qm9CLFlBQVMsQ0FDUjtBQUNDQyxVQUFNWixJQUFJTSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixJQUF4QixFQUE4QixrQkFBOUIsQ0FEUDtBQUVDSyxXQUFPLGlCQUFXO0FBQ2pCcEIsT0FBRSxJQUFGLEVBQVFxQixNQUFSLENBQWUsT0FBZjtBQUNBO0FBSkYsSUFEUSxFQU9SO0FBQ0NGLFVBQU1aLElBQUlNLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLEtBQXhCLEVBQStCLGtCQUEvQixDQURQO0FBRUNLLFdBQU8saUJBQVc7QUFDakJwQixPQUFFLElBQUYsRUFBUXFCLE1BQVIsQ0FBZSxPQUFmO0FBQ0FDLFlBQU9DLElBQVAsQ0FBWWpCLE1BQU1rQixJQUFOLENBQVcsTUFBWCxDQUFaLEVBQWdDLFFBQWhDO0FBQ0E7QUFMRixJQVBRO0FBSmEsR0FBdkI7O0FBcUJBLFNBQU8sS0FBUDtBQUNBLEVBMUJEOztBQTZCQTtBQUNBO0FBQ0E7O0FBRUEzQixRQUFPNEIsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjNCLFFBQU00QixNQUFOLENBQWEzQixFQUFFLGVBQUYsQ0FBYjtBQUNBRCxRQUFNNkIsSUFBTixDQUFXLGlCQUFYLEVBQThCQyxFQUE5QixDQUFpQyxPQUFqQyxFQUEwQ3pCLDBCQUExQzs7QUFFQXNCO0FBQ0EsRUFMRDs7QUFPQSxRQUFPN0IsTUFBUDtBQUNBLENBckZGIiwiZmlsZSI6Im9yZGVycy9vcmRlcl9pbnZvaWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBvcmRlcl9pbnZvaWNlLmpzIDIwMTUtMDktMjlcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE1IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIFBheVBhbDMgUGF5bWVudCBEZXRhaWxzIG9uIE9yZGVyIFBhZ2VcbiAqXG4gKiBUaGlzIG1vZHVsZSBhZGQgdGhlIHBheXBhbDMgcGF5bWVudCBpbmZvcm1hdGlvbmVuIHRvIHRoZSBvcmRlciBkZXRhaWxzIHBhZ2UuXG4gKlxuICogQG1vZHVsZSBDb21wYXRpYmlsaXR5L29yZGVyX2ludm9pY2VcbiAqL1xuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdvcmRlcl9pbnZvaWNlJyxcblx0XG5cdFsnbW9kYWwnXSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb21wYXRpYmlsaXR5L29yZGVyX2ludm9pY2UgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0dmFyIF9jYW5jZWxJbnZvaWNlQ2xpY2tIYW5kbGVyID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgY29uZmlybSA9IGZhbHNlO1xuXHRcdFx0dmFyICRsaW5rID0gJCh0aGlzKTtcblx0XHRcdFxuXHRcdFx0anNlLmxpYnMubW9kYWwubWVzc2FnZSh7XG5cdFx0XHRcdG1pbldpZHRoOiA0MDAsXG5cdFx0XHRcdHRpdGxlOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnSEVBRElOR19NT0RBTF9DQU5DRUxMQVRJT05fSU5WT0lDRScsICdvcmRlcnMnKSxcblx0XHRcdFx0Y29udGVudDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfTU9EQUxfQ0FOQ0VMTEFUSU9OX0lOVk9JQ0UnLCAnb3JkZXJzJykucmVwbGFjZSgnJXMnLCAkbGluay5kYXRhKCdpbnZvaWNlLW51bWJlcicpKSxcblx0XHRcdFx0YnV0dG9uczogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdubycsICdsaWdodGJveF9idXR0b25zJyksXG5cdFx0XHRcdFx0XHRjbGljazogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0dGV4dDoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3llcycsICdsaWdodGJveF9idXR0b25zJyksXG5cdFx0XHRcdFx0XHRjbGljazogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0XHR3aW5kb3cub3BlbigkbGluay5hdHRyKCdocmVmJyksICdfYmxhbmsnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdF1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fTtcblx0XHRcdFxuXHRcdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCR0aGlzLmFwcGVuZCgkKCd0YWJsZS5pbnZvaWNlJykpO1xuXHRcdFx0JHRoaXMuZmluZCgnLmNhbmNlbC1pbnZvaWNlJykub24oJ2NsaWNrJywgX2NhbmNlbEludm9pY2VDbGlja0hhbmRsZXIpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
