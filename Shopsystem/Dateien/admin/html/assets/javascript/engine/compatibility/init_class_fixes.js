'use strict';

/* --------------------------------------------------------------
 init_class_fixes.js 2017-05-30 
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Initialize Class Fixes
 *
 * This module must set as many compatibility classes as possible. Wherever it is
 * certain that an HTML class will be present it must be automatically set by this
 * module.
 *
 * @module Compatibility/init_class_fixes
 */
gx.compatibility.module('init_class_fixes', ['url_arguments'],

/**  @lends module:Compatibility/init_class_fixes */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Callbacks for checking common patterns.
  *
  * @var {array}
  */
	fixes = [],


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// OPERATIONS
	// ------------------------------------------------------------------------

	/**
  * Add gx-compatibility class to body element.
  */
	fixes.push(function () {
		if (!$('body').hasClass('gx-compatibility')) {
			$('body').addClass('gx-compatibility');
		}
	});

	/**
  * Add the gx-container custom predefined selectors.
  */
	fixes.push(function () {
		// Append the following array with extra custom selectors.
		var customSelectors = ['.dataTableRow', '.dataTableHeadingRow', '.dataTableRowSelected', '.pdf_menu', '#log_content', '.contentTable', '.infoBoxHeading'];

		$.each(customSelectors, function () {
			if (!$(this).hasClass('gx-container')) {
				$(this).addClass('gx-container');
			}
		});
	});

	/**
  * Normalize tables by custom selectors.
  */
	fixes.push(function () {
		// Append the following array with extra custom selectors.
		var normalizeTables = ['#gm_box_content > table', '#gm_box_content > form > table'];

		$.each(normalizeTables, function () {
			if (!$(this).hasClass('normalize-table')) {
				$(this).addClass('normalize-table');
			}
		});
	});

	/**
  * Add extra classes to the table structure of configuration.php pages.
  */
	fixes.push(function () {
		var tablesArray = $('form[name="configuration"]').children();

		// set $saveBtn only if there is exactly one input[type="submit"]-Button
		if ($('input[type="submit"]').length === 1) {
			var $saveBtn = $('input[type="submit"]');
			$saveBtn.removeClass('button');
			if (!$saveBtn.hasClass('btn')) {
				$saveBtn.addClass('btn');
				$saveBtn.addClass('btn-primary');
			}
		}

		$.each(tablesArray, function (index, element) {
			var labelText = $(element).find('.dataTableContent_gm').first().children().first().text(),
			    $elementObj = $(element),
			    rightDataTableContent = $($elementObj.find('.dataTableContent_gm')[1]);
			$elementObj.find('tr[bgcolor]').removeAttr('bgcolor');
			$elementObj.find('.dataTableContent_gm').first().addClass('configuration-label');
			$elementObj.find('.dataTableContent_gm').first().children().first().replaceWith(labelText);

			rightDataTableContent.find('br').remove();

			$elementObj.addClass('main-table');

			if (index % 2) {
				$elementObj.addClass('even');
			} else {
				$elementObj.addClass('odd');
			}
		});
		$('.error-logging-select').removeClass('pull-left');
	});

	/**
  * Fixes for the orders table.
  *
  * Some columns swapped or hide, classes was added and some elements will be removed.
  */
	fixes.push(function () {
		var $headingBoxContainer = $('.orders_form'),
		    $orderInfoBox = $('#gm_orders');

		$.each($headingBoxContainer.children(), function (index, element) {
			$(element).addClass('hidden');
		});

		$orderInfoBox.addClass('hidden');
	});

	/**
  * Fixes for customer overview page.
  */
	fixes.push(function () {
		var $compatibilityTable = $('.gx-compatibility-table.gx-customer-overview'),
		    $pagerRow = $compatibilityTable.find('tr').last();

		$('.info-box').addClass('hidden');
		$('.customer-sort-links').addClass('hidden');
		$pagerRow.find('td').first().parent().parent().parent().appendTo($compatibilityTable.parent());
		$compatibilityTable.find('.arrow-icon').addClass('hidden');
		$compatibilityTable.find('tr').last().remove();

		// Delete guest accounts
		$('#delete-guest-accounts').on('click', function () {
			// Create confirmation dialog
			var $confirmation = $('<div>');
			var $content = $('<span>');

			$content.text(jse.core.lang.translate('CONFIRM_DELETE_GUEST_ACCOUNTS', 'admin_customers'));

			$confirmation.appendTo('body').append($content).addClass('gx-container').dialog({
				'title': jse.core.lang.translate('BUTTON_DELETE_GUEST_ACCOUNTS', 'admin_customers'),
				'modal': true,
				'dialogClass': 'gx-container',
				'buttons': [{
					'text': jse.core.lang.translate('close', 'buttons'),
					'class': 'btn',
					'click': function click() {
						$(this).dialog('close');
					}
				}, {
					'text': jse.core.lang.translate('delete', 'buttons'),
					'class': 'btn-primary',
					'click': function click() {
						$.ajax({
							url: [window.location.origin + window.location.pathname.replace('customers.php', ''), 'request_port.php', '?module=DeleteGuestAccounts', '&token=' + $('#delete-guest-accounts').data('token')].join(''),
							type: 'GET',
							dataType: 'json',
							data: '',
							success: function success(p_result_json) {
								var t_url = window.location.href;
								if (window.location.search.search('cID=') !== -1) {
									t_url = window.location.href.replace(/[&]?cID=[\d]+/g, '');
								}

								window.location.href = t_url;

								return false;
							}
						});
					}
				}],
				'width': 420,
				'closeOnEscape': false,
				'open': function open() {
					$('.ui-dialog-titlebar-close').hide();
				}
			});
		});
	});

	/**
  * Class fixes for the products and categories overview page.
  */
	fixes.push(function () {
		var $infoBox = $('.gx-categories').find('.info-box'),
		    $sortBarRow = $('.dataTableHeadingRow_sortbar'),
		    $createNewContainer = $('.create-new-container'),
		    pageHeadingElementsArray = $('.pageSubHeading').children(),
		    tableCellArray = $('.categories_view_data'),
		    $pagerContainer = $('.articles-pager');
		$infoBox.addClass('hidden');
		$sortBarRow.addClass('hidden');
		$.each(tableCellArray, function (index, element) {
			// Replace double '-' with single one.
			var cellObj = $(element);
			if (cellObj.text() === '--') {
				cellObj.text('-');
			}
		});
		$.each(pageHeadingElementsArray, function (index, element) {
			// Page heading actions.
			$(element).addClass('hidden');
		});
		$createNewContainer.removeClass('hidden');

		$.each($pagerContainer.find('.button'), function (index, element) {
			var elementObj = $(element);
			elementObj.addClass('hidden');
			elementObj.removeClass('button');
		});
	});

	/**
  * Add Pagination styles
  */
	fixes.push(function () {
		// Define pagination area where all the pagination stuff is
		var $paginationArea = $this.find('.pagination-control').parents('table:first');

		// Add compatibility classes
		$paginationArea.addClass('gx-container paginator');
	});

	/**
  * Add extra classes to the table structure of configuration.php pages.
  */
	fixes.push(function () {
		var tablesArray = $('form[name="configuration"]').children();
		$.each(tablesArray, function (index, element) {
			var labelText = $(element).find('.dataTableContent_gm').first().children().first().text(),
			    $elementObj = $(element),
			    rightDataTableContent = $($elementObj.find('.dataTableContent_gm')[1]);
			$elementObj.find('tr[bgcolor]').removeAttr('bgcolor');
			$elementObj.find('.dataTableContent_gm').first().addClass('configuration-label');
			$elementObj.find('.dataTableContent_gm').first().children().first().replaceWith(labelText);

			rightDataTableContent.find('br').remove();

			$elementObj.addClass('main-table');

			if (index % 2) {
				$elementObj.addClass('even');
			} else {
				$elementObj.addClass('odd');
			}
		});
		$('.error-logging-select').removeClass('pull-left');
	});

	/**
  * Change class of all buttons from "button" and "admin_button_green" to "btn"
  */
	fixes.push(function () {
		var selectors = ['.button', '.admin_button_green'];

		$.each(selectors, function () {
			$(this).each(function () {
				if (!$(this).hasClass('btn')) {
					$(this).addClass('btn');
				}

				$(this).removeClass('button');
				$(this).removeClass('admin_button_green');
			});
		});
	});

	/**
  * Remove img in anchor tags with class btn
  */
	fixes.push(function () {
		$('a.btn').each(function (index, element) {
			if ($(element).find('img').length) {
				$(element).find('img').remove();
			}
		});
	});

	/**
  * Hides an empty container, that takes up space
  */
	fixes.push(function () {
		if (!$('div.orders_form :visible').text().trim().length) {
			$('div.orders_form').parents('table:first').removeProp('cellpadding');
			$('div.orders_form').parents('tr:first').find('br').remove();
			$('div.orders_form').parents('td:first').css('padding', '0');
		}
	});

	/**
  *
  */
	fixes.push(function () {
		$('table.paginator').removeProp('cellspacing').removeProp('cellpadding');
	});

	/**
  * Add extra class for the modal box when a customer group should edit.
  */
	fixes.push(function () {
		var urlHelper = jse.libs.url_arguments,
		    // alias
		$form = $('form[name="customers"]');

		if (urlHelper.getCurrentFile() === 'customers.php' && urlHelper.getUrlParameters().action === 'editstatus') {
			$form.find('table').addClass('edit-customer-group-table').attr('cellpadding', '0');
		}
	});

	/**
  * Fix the warning icon element in case a checkbox is next to it
  */
	fixes.push(function () {
		var warningIcon = $('.tooltip_icon.warning');
		if ($(warningIcon).parent().parent().prev('.checkbox-switch-wrapper').length) {
			warningIcon.css('margin-left', '12px');
		}
	});

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Execute the registered fixes.
		$.each(fixes, function () {
			this();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluaXRfY2xhc3NfZml4ZXMuanMiXSwibmFtZXMiOlsiZ3giLCJjb21wYXRpYmlsaXR5IiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImZpeGVzIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwicHVzaCIsImhhc0NsYXNzIiwiYWRkQ2xhc3MiLCJjdXN0b21TZWxlY3RvcnMiLCJlYWNoIiwibm9ybWFsaXplVGFibGVzIiwidGFibGVzQXJyYXkiLCJjaGlsZHJlbiIsImxlbmd0aCIsIiRzYXZlQnRuIiwicmVtb3ZlQ2xhc3MiLCJpbmRleCIsImVsZW1lbnQiLCJsYWJlbFRleHQiLCJmaW5kIiwiZmlyc3QiLCJ0ZXh0IiwiJGVsZW1lbnRPYmoiLCJyaWdodERhdGFUYWJsZUNvbnRlbnQiLCJyZW1vdmVBdHRyIiwicmVwbGFjZVdpdGgiLCJyZW1vdmUiLCIkaGVhZGluZ0JveENvbnRhaW5lciIsIiRvcmRlckluZm9Cb3giLCIkY29tcGF0aWJpbGl0eVRhYmxlIiwiJHBhZ2VyUm93IiwibGFzdCIsInBhcmVudCIsImFwcGVuZFRvIiwib24iLCIkY29uZmlybWF0aW9uIiwiJGNvbnRlbnQiLCJqc2UiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsImFwcGVuZCIsImRpYWxvZyIsImFqYXgiLCJ1cmwiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsIm9yaWdpbiIsInBhdGhuYW1lIiwicmVwbGFjZSIsImpvaW4iLCJ0eXBlIiwiZGF0YVR5cGUiLCJzdWNjZXNzIiwicF9yZXN1bHRfanNvbiIsInRfdXJsIiwiaHJlZiIsInNlYXJjaCIsImhpZGUiLCIkaW5mb0JveCIsIiRzb3J0QmFyUm93IiwiJGNyZWF0ZU5ld0NvbnRhaW5lciIsInBhZ2VIZWFkaW5nRWxlbWVudHNBcnJheSIsInRhYmxlQ2VsbEFycmF5IiwiJHBhZ2VyQ29udGFpbmVyIiwiY2VsbE9iaiIsImVsZW1lbnRPYmoiLCIkcGFnaW5hdGlvbkFyZWEiLCJwYXJlbnRzIiwic2VsZWN0b3JzIiwidHJpbSIsInJlbW92ZVByb3AiLCJjc3MiLCJ1cmxIZWxwZXIiLCJsaWJzIiwidXJsX2FyZ3VtZW50cyIsIiRmb3JtIiwiZ2V0Q3VycmVudEZpbGUiLCJnZXRVcmxQYXJhbWV0ZXJzIiwiYWN0aW9uIiwiYXR0ciIsIndhcm5pbmdJY29uIiwicHJldiIsImluaXQiLCJkb25lIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7OztBQVNBQSxHQUFHQyxhQUFILENBQWlCQyxNQUFqQixDQUNDLGtCQURELEVBR0MsQ0FBQyxlQUFELENBSEQ7O0FBS0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFNBQVEsRUFiVDs7O0FBZUM7Ozs7O0FBS0FDLFlBQVcsRUFwQlo7OztBQXNCQzs7Ozs7QUFLQUMsV0FBVUgsRUFBRUksTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkosSUFBN0IsQ0EzQlg7OztBQTZCQzs7Ozs7QUFLQUQsVUFBUyxFQWxDVjs7QUFvQ0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQUksT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSSxDQUFDTCxFQUFFLE1BQUYsRUFBVU0sUUFBVixDQUFtQixrQkFBbkIsQ0FBTCxFQUE2QztBQUM1Q04sS0FBRSxNQUFGLEVBQVVPLFFBQVYsQ0FBbUIsa0JBQW5CO0FBQ0E7QUFDRCxFQUpEOztBQU1BOzs7QUFHQU4sT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckI7QUFDQSxNQUFJRyxrQkFBa0IsQ0FDckIsZUFEcUIsRUFFckIsc0JBRnFCLEVBR3JCLHVCQUhxQixFQUlyQixXQUpxQixFQUtyQixjQUxxQixFQU1yQixlQU5xQixFQU9yQixpQkFQcUIsQ0FBdEI7O0FBVUFSLElBQUVTLElBQUYsQ0FBT0QsZUFBUCxFQUF3QixZQUFXO0FBQ2xDLE9BQUksQ0FBQ1IsRUFBRSxJQUFGLEVBQVFNLFFBQVIsQ0FBaUIsY0FBakIsQ0FBTCxFQUF1QztBQUN0Q04sTUFBRSxJQUFGLEVBQVFPLFFBQVIsQ0FBaUIsY0FBakI7QUFDQTtBQUNELEdBSkQ7QUFLQSxFQWpCRDs7QUFtQkE7OztBQUdBTixPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQjtBQUNBLE1BQUlLLGtCQUFrQixDQUNyQix5QkFEcUIsRUFFckIsZ0NBRnFCLENBQXRCOztBQUtBVixJQUFFUyxJQUFGLENBQU9DLGVBQVAsRUFBd0IsWUFBVztBQUNsQyxPQUFJLENBQUNWLEVBQUUsSUFBRixFQUFRTSxRQUFSLENBQWlCLGlCQUFqQixDQUFMLEVBQTBDO0FBQ3pDTixNQUFFLElBQUYsRUFBUU8sUUFBUixDQUFpQixpQkFBakI7QUFDQTtBQUNELEdBSkQ7QUFLQSxFQVpEOztBQWNBOzs7QUFHQU4sT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSU0sY0FBY1gsRUFBRSw0QkFBRixFQUFnQ1ksUUFBaEMsRUFBbEI7O0FBRUE7QUFDQSxNQUFJWixFQUFFLHNCQUFGLEVBQTBCYSxNQUExQixLQUFxQyxDQUF6QyxFQUE0QztBQUMzQyxPQUFJQyxXQUFXZCxFQUFFLHNCQUFGLENBQWY7QUFDQWMsWUFBU0MsV0FBVCxDQUFxQixRQUFyQjtBQUNBLE9BQUksQ0FBQ0QsU0FBU1IsUUFBVCxDQUFrQixLQUFsQixDQUFMLEVBQStCO0FBQzlCUSxhQUFTUCxRQUFULENBQWtCLEtBQWxCO0FBQ0FPLGFBQVNQLFFBQVQsQ0FBa0IsYUFBbEI7QUFDQTtBQUNEOztBQUVEUCxJQUFFUyxJQUFGLENBQU9FLFdBQVAsRUFBb0IsVUFBU0ssS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUI7QUFDNUMsT0FBSUMsWUFBWWxCLEVBQUVpQixPQUFGLEVBQVdFLElBQVgsQ0FBZ0Isc0JBQWhCLEVBQXdDQyxLQUF4QyxHQUFnRFIsUUFBaEQsR0FBMkRRLEtBQTNELEdBQW1FQyxJQUFuRSxFQUFoQjtBQUFBLE9BQ0NDLGNBQWN0QixFQUFFaUIsT0FBRixDQURmO0FBQUEsT0FFQ00sd0JBQXdCdkIsRUFBRXNCLFlBQVlILElBQVosQ0FBaUIsc0JBQWpCLEVBQXlDLENBQXpDLENBQUYsQ0FGekI7QUFHQUcsZUFBWUgsSUFBWixDQUFpQixhQUFqQixFQUFnQ0ssVUFBaEMsQ0FBMkMsU0FBM0M7QUFDQUYsZUFBWUgsSUFBWixDQUFpQixzQkFBakIsRUFBeUNDLEtBQXpDLEdBQWlEYixRQUFqRCxDQUEwRCxxQkFBMUQ7QUFDQWUsZUFBWUgsSUFBWixDQUFpQixzQkFBakIsRUFBeUNDLEtBQXpDLEdBQWlEUixRQUFqRCxHQUE0RFEsS0FBNUQsR0FBb0VLLFdBQXBFLENBQWdGUCxTQUFoRjs7QUFFQUsseUJBQXNCSixJQUF0QixDQUEyQixJQUEzQixFQUFpQ08sTUFBakM7O0FBRUFKLGVBQVlmLFFBQVosQ0FBcUIsWUFBckI7O0FBRUEsT0FBSVMsUUFBUSxDQUFaLEVBQWU7QUFDZE0sZ0JBQVlmLFFBQVosQ0FBcUIsTUFBckI7QUFDQSxJQUZELE1BRU87QUFDTmUsZ0JBQVlmLFFBQVosQ0FBcUIsS0FBckI7QUFDQTtBQUNELEdBakJEO0FBa0JBUCxJQUFFLHVCQUFGLEVBQTJCZSxXQUEzQixDQUF1QyxXQUF2QztBQUNBLEVBaENEOztBQWtDQTs7Ozs7QUFLQWQsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSXNCLHVCQUF1QjNCLEVBQUUsY0FBRixDQUEzQjtBQUFBLE1BQ0M0QixnQkFBZ0I1QixFQUFFLFlBQUYsQ0FEakI7O0FBR0FBLElBQUVTLElBQUYsQ0FBT2tCLHFCQUFxQmYsUUFBckIsRUFBUCxFQUF3QyxVQUFTSSxLQUFULEVBQWdCQyxPQUFoQixFQUF5QjtBQUNoRWpCLEtBQUVpQixPQUFGLEVBQVdWLFFBQVgsQ0FBb0IsUUFBcEI7QUFDQSxHQUZEOztBQUlBcUIsZ0JBQWNyQixRQUFkLENBQXVCLFFBQXZCO0FBQ0EsRUFURDs7QUFXQTs7O0FBR0FOLE9BQU1JLElBQU4sQ0FBVyxZQUFXO0FBQ3JCLE1BQUl3QixzQkFBc0I3QixFQUFFLDhDQUFGLENBQTFCO0FBQUEsTUFDQzhCLFlBQVlELG9CQUFvQlYsSUFBcEIsQ0FBeUIsSUFBekIsRUFBK0JZLElBQS9CLEVBRGI7O0FBR0EvQixJQUFFLFdBQUYsRUFBZU8sUUFBZixDQUF3QixRQUF4QjtBQUNBUCxJQUFFLHNCQUFGLEVBQTBCTyxRQUExQixDQUFtQyxRQUFuQztBQUNBdUIsWUFBVVgsSUFBVixDQUFlLElBQWYsRUFBcUJDLEtBQXJCLEdBQTZCWSxNQUE3QixHQUFzQ0EsTUFBdEMsR0FBK0NBLE1BQS9DLEdBQXdEQyxRQUF4RCxDQUFpRUosb0JBQW9CRyxNQUFwQixFQUFqRTtBQUNBSCxzQkFBb0JWLElBQXBCLENBQXlCLGFBQXpCLEVBQXdDWixRQUF4QyxDQUFpRCxRQUFqRDtBQUNBc0Isc0JBQW9CVixJQUFwQixDQUF5QixJQUF6QixFQUErQlksSUFBL0IsR0FBc0NMLE1BQXRDOztBQUVBO0FBQ0ExQixJQUFFLHdCQUFGLEVBQTRCa0MsRUFBNUIsQ0FBK0IsT0FBL0IsRUFBd0MsWUFBVztBQUNsRDtBQUNBLE9BQUlDLGdCQUFnQm5DLEVBQUUsT0FBRixDQUFwQjtBQUNBLE9BQUlvQyxXQUFXcEMsRUFBRSxRQUFGLENBQWY7O0FBRUFvQyxZQUNFZixJQURGLENBQ09nQixJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwrQkFBeEIsRUFBeUQsaUJBQXpELENBRFA7O0FBR0FMLGlCQUNFRixRQURGLENBQ1csTUFEWCxFQUVFUSxNQUZGLENBRVNMLFFBRlQsRUFHRTdCLFFBSEYsQ0FHVyxjQUhYLEVBSUVtQyxNQUpGLENBSVM7QUFDUCxhQUFTTCxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qiw4QkFBeEIsRUFBd0QsaUJBQXhELENBREY7QUFFUCxhQUFTLElBRkY7QUFHUCxtQkFBZSxjQUhSO0FBSVAsZUFBVyxDQUNWO0FBQ0MsYUFBUUgsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsU0FBakMsQ0FEVDtBQUVDLGNBQVMsS0FGVjtBQUdDLGNBQVMsaUJBQVc7QUFDbkJ4QyxRQUFFLElBQUYsRUFBUTBDLE1BQVIsQ0FBZSxPQUFmO0FBQ0E7QUFMRixLQURVLEVBUVY7QUFDQyxhQUFRTCxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixRQUF4QixFQUFrQyxTQUFsQyxDQURUO0FBRUMsY0FBUyxhQUZWO0FBR0MsY0FBUyxpQkFBVztBQUNuQnhDLFFBQUUyQyxJQUFGLENBQU87QUFDTkMsWUFBSyxDQUNIQyxPQUFPQyxRQUFQLENBQWdCQyxNQUFoQixHQUNDRixPQUFPQyxRQUFQLENBQWdCRSxRQUFoQixDQUF5QkMsT0FBekIsQ0FBaUMsZUFBakMsRUFDRCxFQURDLENBRkUsRUFJSixrQkFKSSxFQUtKLDZCQUxJLEVBTUosWUFBWWpELEVBQUUsd0JBQUYsRUFBNEJGLElBQTVCLENBQWlDLE9BQWpDLENBTlIsRUFPSG9ELElBUEcsQ0FPRSxFQVBGLENBREM7QUFTTkMsYUFBTSxLQVRBO0FBVU5DLGlCQUFVLE1BVko7QUFXTnRELGFBQU0sRUFYQTtBQVlOdUQsZ0JBQVMsaUJBQVNDLGFBQVQsRUFBd0I7QUFDaEMsWUFBSUMsUUFBUVYsT0FBT0MsUUFBUCxDQUFnQlUsSUFBNUI7QUFDQSxZQUFJWCxPQUFPQyxRQUFQLENBQWdCVyxNQUFoQixDQUF1QkEsTUFBdkIsQ0FBOEIsTUFBOUIsTUFBMEMsQ0FBQyxDQUEvQyxFQUFrRDtBQUNqREYsaUJBQ0NWLE9BQU9DLFFBQVAsQ0FBZ0JVLElBQWhCLENBQXFCUCxPQUFyQixDQUE2QixnQkFBN0IsRUFBK0MsRUFBL0MsQ0FERDtBQUVBOztBQUVESixlQUFPQyxRQUFQLENBQWdCVSxJQUFoQixHQUF1QkQsS0FBdkI7O0FBRUEsZUFBTyxLQUFQO0FBQ0E7QUF0QkssT0FBUDtBQXdCQTtBQTVCRixLQVJVLENBSko7QUEyQ1AsYUFBUyxHQTNDRjtBQTRDUCxxQkFBaUIsS0E1Q1Y7QUE2Q1AsWUFBUSxnQkFBVztBQUNsQnZELE9BQUUsMkJBQUYsRUFBK0IwRCxJQUEvQjtBQUNBO0FBL0NNLElBSlQ7QUFxREEsR0E3REQ7QUE4REEsRUF6RUQ7O0FBMkVBOzs7QUFHQXpELE9BQU1JLElBQU4sQ0FBVyxZQUFXO0FBQ3JCLE1BQUlzRCxXQUFXM0QsRUFBRSxnQkFBRixFQUFvQm1CLElBQXBCLENBQXlCLFdBQXpCLENBQWY7QUFBQSxNQUNDeUMsY0FBYzVELEVBQUUsOEJBQUYsQ0FEZjtBQUFBLE1BRUM2RCxzQkFBc0I3RCxFQUFFLHVCQUFGLENBRnZCO0FBQUEsTUFHQzhELDJCQUEyQjlELEVBQUUsaUJBQUYsRUFBcUJZLFFBQXJCLEVBSDVCO0FBQUEsTUFJQ21ELGlCQUFpQi9ELEVBQUUsdUJBQUYsQ0FKbEI7QUFBQSxNQUtDZ0Usa0JBQWtCaEUsRUFBRSxpQkFBRixDQUxuQjtBQU1BMkQsV0FBU3BELFFBQVQsQ0FBa0IsUUFBbEI7QUFDQXFELGNBQVlyRCxRQUFaLENBQXFCLFFBQXJCO0FBQ0FQLElBQUVTLElBQUYsQ0FBT3NELGNBQVAsRUFBdUIsVUFBUy9DLEtBQVQsRUFBZ0JDLE9BQWhCLEVBQXlCO0FBQUU7QUFDakQsT0FBSWdELFVBQVVqRSxFQUFFaUIsT0FBRixDQUFkO0FBQ0EsT0FBSWdELFFBQVE1QyxJQUFSLE9BQW1CLElBQXZCLEVBQTZCO0FBQzVCNEMsWUFBUTVDLElBQVIsQ0FBYSxHQUFiO0FBQ0E7QUFDRCxHQUxEO0FBTUFyQixJQUFFUyxJQUFGLENBQU9xRCx3QkFBUCxFQUFpQyxVQUFTOUMsS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUI7QUFBRTtBQUMzRGpCLEtBQUVpQixPQUFGLEVBQVdWLFFBQVgsQ0FBb0IsUUFBcEI7QUFDQSxHQUZEO0FBR0FzRCxzQkFBb0I5QyxXQUFwQixDQUFnQyxRQUFoQzs7QUFFQWYsSUFBRVMsSUFBRixDQUFPdUQsZ0JBQWdCN0MsSUFBaEIsQ0FBcUIsU0FBckIsQ0FBUCxFQUF3QyxVQUFTSCxLQUFULEVBQWdCQyxPQUFoQixFQUF5QjtBQUNoRSxPQUFJaUQsYUFBYWxFLEVBQUVpQixPQUFGLENBQWpCO0FBQ0FpRCxjQUFXM0QsUUFBWCxDQUFvQixRQUFwQjtBQUNBMkQsY0FBV25ELFdBQVgsQ0FBdUIsUUFBdkI7QUFDQSxHQUpEO0FBS0EsRUF6QkQ7O0FBMkJBOzs7QUFHQWQsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckI7QUFDQSxNQUFJOEQsa0JBQWtCcEUsTUFDcEJvQixJQURvQixDQUNmLHFCQURlLEVBRXBCaUQsT0FGb0IsQ0FFWixhQUZZLENBQXRCOztBQUlBO0FBQ0FELGtCQUFnQjVELFFBQWhCLENBQXlCLHdCQUF6QjtBQUVBLEVBVEQ7O0FBV0E7OztBQUdBTixPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQixNQUFJTSxjQUFjWCxFQUFFLDRCQUFGLEVBQWdDWSxRQUFoQyxFQUFsQjtBQUNBWixJQUFFUyxJQUFGLENBQU9FLFdBQVAsRUFBb0IsVUFBU0ssS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUI7QUFDNUMsT0FBSUMsWUFBWWxCLEVBQUVpQixPQUFGLEVBQVdFLElBQVgsQ0FBZ0Isc0JBQWhCLEVBQXdDQyxLQUF4QyxHQUFnRFIsUUFBaEQsR0FBMkRRLEtBQTNELEdBQW1FQyxJQUFuRSxFQUFoQjtBQUFBLE9BQ0NDLGNBQWN0QixFQUFFaUIsT0FBRixDQURmO0FBQUEsT0FFQ00sd0JBQXdCdkIsRUFBRXNCLFlBQVlILElBQVosQ0FBaUIsc0JBQWpCLEVBQXlDLENBQXpDLENBQUYsQ0FGekI7QUFHQUcsZUFBWUgsSUFBWixDQUFpQixhQUFqQixFQUFnQ0ssVUFBaEMsQ0FBMkMsU0FBM0M7QUFDQUYsZUFBWUgsSUFBWixDQUFpQixzQkFBakIsRUFBeUNDLEtBQXpDLEdBQWlEYixRQUFqRCxDQUEwRCxxQkFBMUQ7QUFDQWUsZUFBWUgsSUFBWixDQUFpQixzQkFBakIsRUFBeUNDLEtBQXpDLEdBQWlEUixRQUFqRCxHQUE0RFEsS0FBNUQsR0FBb0VLLFdBQXBFLENBQWdGUCxTQUFoRjs7QUFFQUsseUJBQXNCSixJQUF0QixDQUEyQixJQUEzQixFQUFpQ08sTUFBakM7O0FBRUFKLGVBQVlmLFFBQVosQ0FBcUIsWUFBckI7O0FBRUEsT0FBSVMsUUFBUSxDQUFaLEVBQWU7QUFDZE0sZ0JBQVlmLFFBQVosQ0FBcUIsTUFBckI7QUFDQSxJQUZELE1BRU87QUFDTmUsZ0JBQVlmLFFBQVosQ0FBcUIsS0FBckI7QUFDQTtBQUNELEdBakJEO0FBa0JBUCxJQUFFLHVCQUFGLEVBQTJCZSxXQUEzQixDQUF1QyxXQUF2QztBQUNBLEVBckJEOztBQXVCQTs7O0FBR0FkLE9BQU1JLElBQU4sQ0FBVyxZQUFXO0FBQ3JCLE1BQUlnRSxZQUFZLENBQ2YsU0FEZSxFQUVmLHFCQUZlLENBQWhCOztBQUtBckUsSUFBRVMsSUFBRixDQUFPNEQsU0FBUCxFQUFrQixZQUFXO0FBQzVCckUsS0FBRSxJQUFGLEVBQVFTLElBQVIsQ0FBYSxZQUFXO0FBQ3ZCLFFBQUksQ0FBQ1QsRUFBRSxJQUFGLEVBQVFNLFFBQVIsQ0FBaUIsS0FBakIsQ0FBTCxFQUE4QjtBQUM3Qk4sT0FBRSxJQUFGLEVBQVFPLFFBQVIsQ0FBaUIsS0FBakI7QUFDQTs7QUFFRFAsTUFBRSxJQUFGLEVBQVFlLFdBQVIsQ0FBb0IsUUFBcEI7QUFDQWYsTUFBRSxJQUFGLEVBQVFlLFdBQVIsQ0FBb0Isb0JBQXBCO0FBQ0EsSUFQRDtBQVFBLEdBVEQ7QUFVQSxFQWhCRDs7QUFrQkE7OztBQUdBZCxPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQkwsSUFBRSxPQUFGLEVBQVdTLElBQVgsQ0FBZ0IsVUFBU08sS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUI7QUFDeEMsT0FBSWpCLEVBQUVpQixPQUFGLEVBQVdFLElBQVgsQ0FBZ0IsS0FBaEIsRUFBdUJOLE1BQTNCLEVBQW1DO0FBQ2xDYixNQUFFaUIsT0FBRixFQUNFRSxJQURGLENBQ08sS0FEUCxFQUVFTyxNQUZGO0FBR0E7QUFDRCxHQU5EO0FBT0EsRUFSRDs7QUFVQTs7O0FBR0F6QixPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQixNQUFJLENBQUNMLEVBQUUsMEJBQUYsRUFBOEJxQixJQUE5QixHQUFxQ2lELElBQXJDLEdBQTRDekQsTUFBakQsRUFBeUQ7QUFDeERiLEtBQUUsaUJBQUYsRUFBcUJvRSxPQUFyQixDQUE2QixhQUE3QixFQUE0Q0csVUFBNUMsQ0FBdUQsYUFBdkQ7QUFDQXZFLEtBQUUsaUJBQUYsRUFBcUJvRSxPQUFyQixDQUE2QixVQUE3QixFQUF5Q2pELElBQXpDLENBQThDLElBQTlDLEVBQW9ETyxNQUFwRDtBQUNBMUIsS0FBRSxpQkFBRixFQUFxQm9FLE9BQXJCLENBQTZCLFVBQTdCLEVBQXlDSSxHQUF6QyxDQUE2QyxTQUE3QyxFQUF3RCxHQUF4RDtBQUNBO0FBQ0QsRUFORDs7QUFRQTs7O0FBR0F2RSxPQUFNSSxJQUFOLENBQVcsWUFBVztBQUNyQkwsSUFBRSxpQkFBRixFQUFxQnVFLFVBQXJCLENBQWdDLGFBQWhDLEVBQStDQSxVQUEvQyxDQUEwRCxhQUExRDtBQUNBLEVBRkQ7O0FBSUE7OztBQUdBdEUsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSW9FLFlBQVlwQyxJQUFJcUMsSUFBSixDQUFTQyxhQUF6QjtBQUFBLE1BQXdDO0FBQ3ZDQyxVQUFRNUUsRUFBRSx3QkFBRixDQURUOztBQUdBLE1BQUl5RSxVQUFVSSxjQUFWLE9BQStCLGVBQS9CLElBQWtESixVQUFVSyxnQkFBVixHQUE2QkMsTUFBN0IsS0FDckQsWUFERCxFQUNlO0FBQ2RILFNBQU16RCxJQUFOLENBQVcsT0FBWCxFQUFvQlosUUFBcEIsQ0FBNkIsMkJBQTdCLEVBQTBEeUUsSUFBMUQsQ0FBK0QsYUFBL0QsRUFBOEUsR0FBOUU7QUFDQTtBQUNELEVBUkQ7O0FBVUE7OztBQUdBL0UsT0FBTUksSUFBTixDQUFXLFlBQVc7QUFDckIsTUFBSTRFLGNBQWNqRixFQUFFLHVCQUFGLENBQWxCO0FBQ0EsTUFBSUEsRUFBRWlGLFdBQUYsRUFBZWpELE1BQWYsR0FBd0JBLE1BQXhCLEdBQWlDa0QsSUFBakMsQ0FBc0MsMEJBQXRDLEVBQWtFckUsTUFBdEUsRUFBOEU7QUFDN0VvRSxlQUFZVCxHQUFaLENBQWdCLGFBQWhCLEVBQStCLE1BQS9CO0FBQ0E7QUFDRCxFQUxEOztBQU9BO0FBQ0E7QUFDQTs7QUFFQTNFLFFBQU9zRixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCO0FBQ0FwRixJQUFFUyxJQUFGLENBQU9SLEtBQVAsRUFBYyxZQUFXO0FBQ3hCO0FBQ0EsR0FGRDs7QUFJQW1GO0FBQ0EsRUFQRDs7QUFTQSxRQUFPdkYsTUFBUDtBQUNBLENBellGIiwiZmlsZSI6ImluaXRfY2xhc3NfZml4ZXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGluaXRfY2xhc3NfZml4ZXMuanMgMjAxNy0wNS0zMCBcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIEluaXRpYWxpemUgQ2xhc3MgRml4ZXNcbiAqXG4gKiBUaGlzIG1vZHVsZSBtdXN0IHNldCBhcyBtYW55IGNvbXBhdGliaWxpdHkgY2xhc3NlcyBhcyBwb3NzaWJsZS4gV2hlcmV2ZXIgaXQgaXNcbiAqIGNlcnRhaW4gdGhhdCBhbiBIVE1MIGNsYXNzIHdpbGwgYmUgcHJlc2VudCBpdCBtdXN0IGJlIGF1dG9tYXRpY2FsbHkgc2V0IGJ5IHRoaXNcbiAqIG1vZHVsZS5cbiAqXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvaW5pdF9jbGFzc19maXhlc1xuICovXG5neC5jb21wYXRpYmlsaXR5Lm1vZHVsZShcblx0J2luaXRfY2xhc3NfZml4ZXMnLFxuXHRcblx0Wyd1cmxfYXJndW1lbnRzJ10sXG5cdFxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9pbml0X2NsYXNzX2ZpeGVzICovXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFUyBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIENhbGxiYWNrcyBmb3IgY2hlY2tpbmcgY29tbW9uIHBhdHRlcm5zLlxuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge2FycmF5fVxuXHRcdFx0ICovXG5cdFx0XHRmaXhlcyA9IFtdLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0ge30sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIE9QRVJBVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGQgZ3gtY29tcGF0aWJpbGl0eSBjbGFzcyB0byBib2R5IGVsZW1lbnQuXG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdGlmICghJCgnYm9keScpLmhhc0NsYXNzKCdneC1jb21wYXRpYmlsaXR5JykpIHtcblx0XHRcdFx0JCgnYm9keScpLmFkZENsYXNzKCdneC1jb21wYXRpYmlsaXR5Jyk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQWRkIHRoZSBneC1jb250YWluZXIgY3VzdG9tIHByZWRlZmluZWQgc2VsZWN0b3JzLlxuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHQvLyBBcHBlbmQgdGhlIGZvbGxvd2luZyBhcnJheSB3aXRoIGV4dHJhIGN1c3RvbSBzZWxlY3RvcnMuXG5cdFx0XHR2YXIgY3VzdG9tU2VsZWN0b3JzID0gW1xuXHRcdFx0XHQnLmRhdGFUYWJsZVJvdycsXG5cdFx0XHRcdCcuZGF0YVRhYmxlSGVhZGluZ1JvdycsXG5cdFx0XHRcdCcuZGF0YVRhYmxlUm93U2VsZWN0ZWQnLFxuXHRcdFx0XHQnLnBkZl9tZW51Jyxcblx0XHRcdFx0JyNsb2dfY29udGVudCcsXG5cdFx0XHRcdCcuY29udGVudFRhYmxlJyxcblx0XHRcdFx0Jy5pbmZvQm94SGVhZGluZydcblx0XHRcdF07XG5cdFx0XHRcblx0XHRcdCQuZWFjaChjdXN0b21TZWxlY3RvcnMsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRpZiAoISQodGhpcykuaGFzQ2xhc3MoJ2d4LWNvbnRhaW5lcicpKSB7XG5cdFx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnZ3gtY29udGFpbmVyJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE5vcm1hbGl6ZSB0YWJsZXMgYnkgY3VzdG9tIHNlbGVjdG9ycy5cblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0Ly8gQXBwZW5kIHRoZSBmb2xsb3dpbmcgYXJyYXkgd2l0aCBleHRyYSBjdXN0b20gc2VsZWN0b3JzLlxuXHRcdFx0dmFyIG5vcm1hbGl6ZVRhYmxlcyA9IFtcblx0XHRcdFx0JyNnbV9ib3hfY29udGVudCA+IHRhYmxlJyxcblx0XHRcdFx0JyNnbV9ib3hfY29udGVudCA+IGZvcm0gPiB0YWJsZSdcblx0XHRcdF07XG5cdFx0XHRcblx0XHRcdCQuZWFjaChub3JtYWxpemVUYWJsZXMsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRpZiAoISQodGhpcykuaGFzQ2xhc3MoJ25vcm1hbGl6ZS10YWJsZScpKSB7XG5cdFx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnbm9ybWFsaXplLXRhYmxlJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFkZCBleHRyYSBjbGFzc2VzIHRvIHRoZSB0YWJsZSBzdHJ1Y3R1cmUgb2YgY29uZmlndXJhdGlvbi5waHAgcGFnZXMuXG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdHZhciB0YWJsZXNBcnJheSA9ICQoJ2Zvcm1bbmFtZT1cImNvbmZpZ3VyYXRpb25cIl0nKS5jaGlsZHJlbigpO1xuXHRcdFx0XG5cdFx0XHQvLyBzZXQgJHNhdmVCdG4gb25seSBpZiB0aGVyZSBpcyBleGFjdGx5IG9uZSBpbnB1dFt0eXBlPVwic3VibWl0XCJdLUJ1dHRvblxuXHRcdFx0aWYgKCQoJ2lucHV0W3R5cGU9XCJzdWJtaXRcIl0nKS5sZW5ndGggPT09IDEpIHtcblx0XHRcdFx0dmFyICRzYXZlQnRuID0gJCgnaW5wdXRbdHlwZT1cInN1Ym1pdFwiXScpO1xuXHRcdFx0XHQkc2F2ZUJ0bi5yZW1vdmVDbGFzcygnYnV0dG9uJyk7XG5cdFx0XHRcdGlmICghJHNhdmVCdG4uaGFzQ2xhc3MoJ2J0bicpKSB7XG5cdFx0XHRcdFx0JHNhdmVCdG4uYWRkQ2xhc3MoJ2J0bicpO1xuXHRcdFx0XHRcdCRzYXZlQnRuLmFkZENsYXNzKCdidG4tcHJpbWFyeScpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCQuZWFjaCh0YWJsZXNBcnJheSwgZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcblx0XHRcdFx0dmFyIGxhYmVsVGV4dCA9ICQoZWxlbWVudCkuZmluZCgnLmRhdGFUYWJsZUNvbnRlbnRfZ20nKS5maXJzdCgpLmNoaWxkcmVuKCkuZmlyc3QoKS50ZXh0KCksXG5cdFx0XHRcdFx0JGVsZW1lbnRPYmogPSAkKGVsZW1lbnQpLFxuXHRcdFx0XHRcdHJpZ2h0RGF0YVRhYmxlQ29udGVudCA9ICQoJGVsZW1lbnRPYmouZmluZCgnLmRhdGFUYWJsZUNvbnRlbnRfZ20nKVsxXSk7XG5cdFx0XHRcdCRlbGVtZW50T2JqLmZpbmQoJ3RyW2JnY29sb3JdJykucmVtb3ZlQXR0cignYmdjb2xvcicpO1xuXHRcdFx0XHQkZWxlbWVudE9iai5maW5kKCcuZGF0YVRhYmxlQ29udGVudF9nbScpLmZpcnN0KCkuYWRkQ2xhc3MoJ2NvbmZpZ3VyYXRpb24tbGFiZWwnKTtcblx0XHRcdFx0JGVsZW1lbnRPYmouZmluZCgnLmRhdGFUYWJsZUNvbnRlbnRfZ20nKS5maXJzdCgpLmNoaWxkcmVuKCkuZmlyc3QoKS5yZXBsYWNlV2l0aChsYWJlbFRleHQpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmlnaHREYXRhVGFibGVDb250ZW50LmZpbmQoJ2JyJykucmVtb3ZlKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkZWxlbWVudE9iai5hZGRDbGFzcygnbWFpbi10YWJsZScpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKGluZGV4ICUgMikge1xuXHRcdFx0XHRcdCRlbGVtZW50T2JqLmFkZENsYXNzKCdldmVuJyk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JGVsZW1lbnRPYmouYWRkQ2xhc3MoJ29kZCcpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdCQoJy5lcnJvci1sb2dnaW5nLXNlbGVjdCcpLnJlbW92ZUNsYXNzKCdwdWxsLWxlZnQnKTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaXhlcyBmb3IgdGhlIG9yZGVycyB0YWJsZS5cblx0XHQgKlxuXHRcdCAqIFNvbWUgY29sdW1ucyBzd2FwcGVkIG9yIGhpZGUsIGNsYXNzZXMgd2FzIGFkZGVkIGFuZCBzb21lIGVsZW1lbnRzIHdpbGwgYmUgcmVtb3ZlZC5cblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyICRoZWFkaW5nQm94Q29udGFpbmVyID0gJCgnLm9yZGVyc19mb3JtJyksXG5cdFx0XHRcdCRvcmRlckluZm9Cb3ggPSAkKCcjZ21fb3JkZXJzJyk7XG5cdFx0XHRcblx0XHRcdCQuZWFjaCgkaGVhZGluZ0JveENvbnRhaW5lci5jaGlsZHJlbigpLCBmdW5jdGlvbihpbmRleCwgZWxlbWVudCkge1xuXHRcdFx0XHQkKGVsZW1lbnQpLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkb3JkZXJJbmZvQm94LmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaXhlcyBmb3IgY3VzdG9tZXIgb3ZlcnZpZXcgcGFnZS5cblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyICRjb21wYXRpYmlsaXR5VGFibGUgPSAkKCcuZ3gtY29tcGF0aWJpbGl0eS10YWJsZS5neC1jdXN0b21lci1vdmVydmlldycpLFxuXHRcdFx0XHQkcGFnZXJSb3cgPSAkY29tcGF0aWJpbGl0eVRhYmxlLmZpbmQoJ3RyJykubGFzdCgpO1xuXHRcdFx0XG5cdFx0XHQkKCcuaW5mby1ib3gnKS5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHQkKCcuY3VzdG9tZXItc29ydC1saW5rcycpLmFkZENsYXNzKCdoaWRkZW4nKTtcblx0XHRcdCRwYWdlclJvdy5maW5kKCd0ZCcpLmZpcnN0KCkucGFyZW50KCkucGFyZW50KCkucGFyZW50KCkuYXBwZW5kVG8oJGNvbXBhdGliaWxpdHlUYWJsZS5wYXJlbnQoKSk7XG5cdFx0XHQkY29tcGF0aWJpbGl0eVRhYmxlLmZpbmQoJy5hcnJvdy1pY29uJykuYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0JGNvbXBhdGliaWxpdHlUYWJsZS5maW5kKCd0cicpLmxhc3QoKS5yZW1vdmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gRGVsZXRlIGd1ZXN0IGFjY291bnRzXG5cdFx0XHQkKCcjZGVsZXRlLWd1ZXN0LWFjY291bnRzJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdC8vIENyZWF0ZSBjb25maXJtYXRpb24gZGlhbG9nXG5cdFx0XHRcdHZhciAkY29uZmlybWF0aW9uID0gJCgnPGRpdj4nKTtcblx0XHRcdFx0dmFyICRjb250ZW50ID0gJCgnPHNwYW4+Jyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkY29udGVudFxuXHRcdFx0XHRcdC50ZXh0KGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdDT05GSVJNX0RFTEVURV9HVUVTVF9BQ0NPVU5UUycsICdhZG1pbl9jdXN0b21lcnMnKSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkY29uZmlybWF0aW9uXG5cdFx0XHRcdFx0LmFwcGVuZFRvKCdib2R5Jylcblx0XHRcdFx0XHQuYXBwZW5kKCRjb250ZW50KVxuXHRcdFx0XHRcdC5hZGRDbGFzcygnZ3gtY29udGFpbmVyJylcblx0XHRcdFx0XHQuZGlhbG9nKHtcblx0XHRcdFx0XHRcdCd0aXRsZSc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fREVMRVRFX0dVRVNUX0FDQ09VTlRTJywgJ2FkbWluX2N1c3RvbWVycycpLFxuXHRcdFx0XHRcdFx0J21vZGFsJzogdHJ1ZSxcblx0XHRcdFx0XHRcdCdkaWFsb2dDbGFzcyc6ICdneC1jb250YWluZXInLFxuXHRcdFx0XHRcdFx0J2J1dHRvbnMnOiBbXG5cdFx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0XHQndGV4dCc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdjbG9zZScsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHRcdFx0J2NsYXNzJzogJ2J0bicsXG5cdFx0XHRcdFx0XHRcdFx0J2NsaWNrJzogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHQkKHRoaXMpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0XHQndGV4dCc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdkZWxldGUnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0XHRcdCdjbGFzcyc6ICdidG4tcHJpbWFyeScsXG5cdFx0XHRcdFx0XHRcdFx0J2NsaWNrJzogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR1cmw6IFtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQod2luZG93LmxvY2F0aW9uLm9yaWdpblxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdCsgd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lLnJlcGxhY2UoJ2N1c3RvbWVycy5waHAnLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0JycpKSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQncmVxdWVzdF9wb3J0LnBocCcsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0Jz9tb2R1bGU9RGVsZXRlR3Vlc3RBY2NvdW50cycsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0JyZ0b2tlbj0nICsgJCgnI2RlbGV0ZS1ndWVzdC1hY2NvdW50cycpLmRhdGEoJ3Rva2VuJyksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdF0uam9pbignJyksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6ICdHRVQnLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRkYXRhOiAnJyxcblx0XHRcdFx0XHRcdFx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24ocF9yZXN1bHRfanNvbikge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHZhciB0X3VybCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGlmICh3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNlYXJjaCgnY0lEPScpICE9PSAtMSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0dF91cmwgPVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR3aW5kb3cubG9jYXRpb24uaHJlZi5yZXBsYWNlKC9bJl0/Y0lEPVtcXGRdKy9nLCAnJyk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gdF91cmw7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdF0sXG5cdFx0XHRcdFx0XHQnd2lkdGgnOiA0MjAsXG5cdFx0XHRcdFx0XHQnY2xvc2VPbkVzY2FwZSc6IGZhbHNlLFxuXHRcdFx0XHRcdFx0J29wZW4nOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0JCgnLnVpLWRpYWxvZy10aXRsZWJhci1jbG9zZScpLmhpZGUoKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsYXNzIGZpeGVzIGZvciB0aGUgcHJvZHVjdHMgYW5kIGNhdGVnb3JpZXMgb3ZlcnZpZXcgcGFnZS5cblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyICRpbmZvQm94ID0gJCgnLmd4LWNhdGVnb3JpZXMnKS5maW5kKCcuaW5mby1ib3gnKSxcblx0XHRcdFx0JHNvcnRCYXJSb3cgPSAkKCcuZGF0YVRhYmxlSGVhZGluZ1Jvd19zb3J0YmFyJyksXG5cdFx0XHRcdCRjcmVhdGVOZXdDb250YWluZXIgPSAkKCcuY3JlYXRlLW5ldy1jb250YWluZXInKSxcblx0XHRcdFx0cGFnZUhlYWRpbmdFbGVtZW50c0FycmF5ID0gJCgnLnBhZ2VTdWJIZWFkaW5nJykuY2hpbGRyZW4oKSxcblx0XHRcdFx0dGFibGVDZWxsQXJyYXkgPSAkKCcuY2F0ZWdvcmllc192aWV3X2RhdGEnKSxcblx0XHRcdFx0JHBhZ2VyQ29udGFpbmVyID0gJCgnLmFydGljbGVzLXBhZ2VyJyk7XG5cdFx0XHQkaW5mb0JveC5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHQkc29ydEJhclJvdy5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHQkLmVhY2godGFibGVDZWxsQXJyYXksIGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KSB7IC8vIFJlcGxhY2UgZG91YmxlICctJyB3aXRoIHNpbmdsZSBvbmUuXG5cdFx0XHRcdHZhciBjZWxsT2JqID0gJChlbGVtZW50KTtcblx0XHRcdFx0aWYgKGNlbGxPYmoudGV4dCgpID09PSAnLS0nKSB7XG5cdFx0XHRcdFx0Y2VsbE9iai50ZXh0KCctJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0JC5lYWNoKHBhZ2VIZWFkaW5nRWxlbWVudHNBcnJheSwgZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHsgLy8gUGFnZSBoZWFkaW5nIGFjdGlvbnMuXG5cdFx0XHRcdCQoZWxlbWVudCkuYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0fSk7XG5cdFx0XHQkY3JlYXRlTmV3Q29udGFpbmVyLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcblx0XHRcdFxuXHRcdFx0JC5lYWNoKCRwYWdlckNvbnRhaW5lci5maW5kKCcuYnV0dG9uJyksIGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KSB7XG5cdFx0XHRcdHZhciBlbGVtZW50T2JqID0gJChlbGVtZW50KTtcblx0XHRcdFx0ZWxlbWVudE9iai5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcdGVsZW1lbnRPYmoucmVtb3ZlQ2xhc3MoJ2J1dHRvbicpO1xuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQWRkIFBhZ2luYXRpb24gc3R5bGVzXG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdC8vIERlZmluZSBwYWdpbmF0aW9uIGFyZWEgd2hlcmUgYWxsIHRoZSBwYWdpbmF0aW9uIHN0dWZmIGlzXG5cdFx0XHR2YXIgJHBhZ2luYXRpb25BcmVhID0gJHRoaXNcblx0XHRcdFx0LmZpbmQoJy5wYWdpbmF0aW9uLWNvbnRyb2wnKVxuXHRcdFx0XHQucGFyZW50cygndGFibGU6Zmlyc3QnKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWRkIGNvbXBhdGliaWxpdHkgY2xhc3Nlc1xuXHRcdFx0JHBhZ2luYXRpb25BcmVhLmFkZENsYXNzKCdneC1jb250YWluZXIgcGFnaW5hdG9yJyk7XG5cdFx0XHRcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGQgZXh0cmEgY2xhc3NlcyB0byB0aGUgdGFibGUgc3RydWN0dXJlIG9mIGNvbmZpZ3VyYXRpb24ucGhwIHBhZ2VzLlxuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgdGFibGVzQXJyYXkgPSAkKCdmb3JtW25hbWU9XCJjb25maWd1cmF0aW9uXCJdJykuY2hpbGRyZW4oKTtcblx0XHRcdCQuZWFjaCh0YWJsZXNBcnJheSwgZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcblx0XHRcdFx0dmFyIGxhYmVsVGV4dCA9ICQoZWxlbWVudCkuZmluZCgnLmRhdGFUYWJsZUNvbnRlbnRfZ20nKS5maXJzdCgpLmNoaWxkcmVuKCkuZmlyc3QoKS50ZXh0KCksXG5cdFx0XHRcdFx0JGVsZW1lbnRPYmogPSAkKGVsZW1lbnQpLFxuXHRcdFx0XHRcdHJpZ2h0RGF0YVRhYmxlQ29udGVudCA9ICQoJGVsZW1lbnRPYmouZmluZCgnLmRhdGFUYWJsZUNvbnRlbnRfZ20nKVsxXSk7XG5cdFx0XHRcdCRlbGVtZW50T2JqLmZpbmQoJ3RyW2JnY29sb3JdJykucmVtb3ZlQXR0cignYmdjb2xvcicpO1xuXHRcdFx0XHQkZWxlbWVudE9iai5maW5kKCcuZGF0YVRhYmxlQ29udGVudF9nbScpLmZpcnN0KCkuYWRkQ2xhc3MoJ2NvbmZpZ3VyYXRpb24tbGFiZWwnKTtcblx0XHRcdFx0JGVsZW1lbnRPYmouZmluZCgnLmRhdGFUYWJsZUNvbnRlbnRfZ20nKS5maXJzdCgpLmNoaWxkcmVuKCkuZmlyc3QoKS5yZXBsYWNlV2l0aChsYWJlbFRleHQpO1xuXHRcdFx0XHRcblx0XHRcdFx0cmlnaHREYXRhVGFibGVDb250ZW50LmZpbmQoJ2JyJykucmVtb3ZlKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkZWxlbWVudE9iai5hZGRDbGFzcygnbWFpbi10YWJsZScpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKGluZGV4ICUgMikge1xuXHRcdFx0XHRcdCRlbGVtZW50T2JqLmFkZENsYXNzKCdldmVuJyk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JGVsZW1lbnRPYmouYWRkQ2xhc3MoJ29kZCcpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdCQoJy5lcnJvci1sb2dnaW5nLXNlbGVjdCcpLnJlbW92ZUNsYXNzKCdwdWxsLWxlZnQnKTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDaGFuZ2UgY2xhc3Mgb2YgYWxsIGJ1dHRvbnMgZnJvbSBcImJ1dHRvblwiIGFuZCBcImFkbWluX2J1dHRvbl9ncmVlblwiIHRvIFwiYnRuXCJcblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIHNlbGVjdG9ycyA9IFtcblx0XHRcdFx0Jy5idXR0b24nLFxuXHRcdFx0XHQnLmFkbWluX2J1dHRvbl9ncmVlbidcblx0XHRcdF07XG5cdFx0XHRcblx0XHRcdCQuZWFjaChzZWxlY3RvcnMsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkKHRoaXMpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0aWYgKCEkKHRoaXMpLmhhc0NsYXNzKCdidG4nKSkge1xuXHRcdFx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnYnRuJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCQodGhpcykucmVtb3ZlQ2xhc3MoJ2J1dHRvbicpO1xuXHRcdFx0XHRcdCQodGhpcykucmVtb3ZlQ2xhc3MoJ2FkbWluX2J1dHRvbl9ncmVlbicpO1xuXHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlbW92ZSBpbWcgaW4gYW5jaG9yIHRhZ3Mgd2l0aCBjbGFzcyBidG5cblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0JCgnYS5idG4nKS5lYWNoKGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KSB7XG5cdFx0XHRcdGlmICgkKGVsZW1lbnQpLmZpbmQoJ2ltZycpLmxlbmd0aCkge1xuXHRcdFx0XHRcdCQoZWxlbWVudClcblx0XHRcdFx0XHRcdC5maW5kKCdpbWcnKVxuXHRcdFx0XHRcdFx0LnJlbW92ZSgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBIaWRlcyBhbiBlbXB0eSBjb250YWluZXIsIHRoYXQgdGFrZXMgdXAgc3BhY2Vcblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0aWYgKCEkKCdkaXYub3JkZXJzX2Zvcm0gOnZpc2libGUnKS50ZXh0KCkudHJpbSgpLmxlbmd0aCkge1xuXHRcdFx0XHQkKCdkaXYub3JkZXJzX2Zvcm0nKS5wYXJlbnRzKCd0YWJsZTpmaXJzdCcpLnJlbW92ZVByb3AoJ2NlbGxwYWRkaW5nJyk7XG5cdFx0XHRcdCQoJ2Rpdi5vcmRlcnNfZm9ybScpLnBhcmVudHMoJ3RyOmZpcnN0JykuZmluZCgnYnInKS5yZW1vdmUoKTtcblx0XHRcdFx0JCgnZGl2Lm9yZGVyc19mb3JtJykucGFyZW50cygndGQ6Zmlyc3QnKS5jc3MoJ3BhZGRpbmcnLCAnMCcpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqXG5cdFx0ICovXG5cdFx0Zml4ZXMucHVzaChmdW5jdGlvbigpIHtcblx0XHRcdCQoJ3RhYmxlLnBhZ2luYXRvcicpLnJlbW92ZVByb3AoJ2NlbGxzcGFjaW5nJykucmVtb3ZlUHJvcCgnY2VsbHBhZGRpbmcnKTtcblx0XHR9KTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGQgZXh0cmEgY2xhc3MgZm9yIHRoZSBtb2RhbCBib3ggd2hlbiBhIGN1c3RvbWVyIGdyb3VwIHNob3VsZCBlZGl0LlxuXHRcdCAqL1xuXHRcdGZpeGVzLnB1c2goZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgdXJsSGVscGVyID0ganNlLmxpYnMudXJsX2FyZ3VtZW50cywgLy8gYWxpYXNcblx0XHRcdFx0JGZvcm0gPSAkKCdmb3JtW25hbWU9XCJjdXN0b21lcnNcIl0nKTtcblx0XHRcdFxuXHRcdFx0aWYgKHVybEhlbHBlci5nZXRDdXJyZW50RmlsZSgpID09PSAnY3VzdG9tZXJzLnBocCcgJiYgdXJsSGVscGVyLmdldFVybFBhcmFtZXRlcnMoKS5hY3Rpb24gPT09XG5cdFx0XHRcdCdlZGl0c3RhdHVzJykge1xuXHRcdFx0XHQkZm9ybS5maW5kKCd0YWJsZScpLmFkZENsYXNzKCdlZGl0LWN1c3RvbWVyLWdyb3VwLXRhYmxlJykuYXR0cignY2VsbHBhZGRpbmcnLCAnMCcpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpeCB0aGUgd2FybmluZyBpY29uIGVsZW1lbnQgaW4gY2FzZSBhIGNoZWNrYm94IGlzIG5leHQgdG8gaXRcblx0XHQgKi9cblx0XHRmaXhlcy5wdXNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIHdhcm5pbmdJY29uID0gJCgnLnRvb2x0aXBfaWNvbi53YXJuaW5nJyk7XG5cdFx0XHRpZiAoJCh3YXJuaW5nSWNvbikucGFyZW50KCkucGFyZW50KCkucHJldignLmNoZWNrYm94LXN3aXRjaC13cmFwcGVyJykubGVuZ3RoKSB7XG5cdFx0XHRcdHdhcm5pbmdJY29uLmNzcygnbWFyZ2luLWxlZnQnLCAnMTJweCcpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBFeGVjdXRlIHRoZSByZWdpc3RlcmVkIGZpeGVzLlxuXHRcdFx0JC5lYWNoKGZpeGVzLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dGhpcygpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
