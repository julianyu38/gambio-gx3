'use strict';

/* --------------------------------------------------------------
 customers_zones_controller.js 2017-03-21
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * The Component for handling the federal state dropdown depending on the country.
 * The field will be blacked out if there are no federal states for the selected
 * country.
 */
gx.compatibility.module('customers_zones_controller', ['form', 'xhr'], function (data) {

	'use strict';

	// ########## VARIABLE INITIALIZATION ##########

	var $this = $(this),
	    $customerStates = $('select[name=customers_state]'),
	    $deliveryStates = $('select[name=delivery_state]'),
	    $billingStates = $('select[name=billing_state]'),
	    $customerFormGroup = $('select[name=customers_state]').closest('div.grid'),
	    $deliveryFormGroup = $('select[name=delivery_state]').closest('div.grid'),
	    $billingFormGroup = $('select[name=billing_state]').closest('div.grid'),
	    defaults = {
		loadStates: 'admin.php?do=Zones/OrderAddressEdit',
		customersCountry: 'select[name=customers_country]',
		deliveryCountry: 'select[name=delivery_country_iso_code_2]',
		billingCountry: 'select[name=billing_country_iso_code_2]'

	},
	    options = $.extend(true, {}, defaults, data),
	    module = {};

	var _changeHandler = function _changeHandler(e) {
		var dataset = jse.libs.form.getData($this);
		dataset.selectors = e.data.selectors;

		jse.libs.xhr.ajax({ url: options.loadStates, data: dataset }, true).done(function (result) {

			var $selector;
			var $selectorFormGroup;

			switch (result.selector) {
				case 'customers_country':
					$selector = $customerStates;
					$selectorFormGroup = $customerFormGroup;
					break;
				case 'delivery_country':
					$selector = $deliveryStates;
					$selectorFormGroup = $deliveryFormGroup;
					break;
				case 'billing_country':
					$selector = $billingStates;
					$selectorFormGroup = $billingFormGroup;
					break;
			}

			if (result.success) {

				$selector.children('option').remove();
				$selector.prop("disabled", false);

				$.each(result.data, function (key, value) {
					if (value.selected) {
						$selector.append($("<option selected/>").val(value.name).text(value.name));
					} else {
						$selector.append($("<option />").val(value.name).text(value.name));
					}
				});

				$selectorFormGroup.show();
			} else {
				$selectorFormGroup.hide();
				$selector.prop("disabled", true);
			}
		});
	};

	// ########## INITIALIZATION ##########

	/**
  * Init function of the widget
  * @constructor
  */
	module.init = function (done) {

		$this.on('change', options.customersCountry, { 'selectors': { 'country': 'customers_country', 'state': 'customers_state', 'selected': 'select_customers_state' } }, _changeHandler).on('change', options.deliveryCountry, { 'selectors': { 'country': 'delivery_country', 'state': 'delivery_state', 'selected': 'select_delivery_state' } }, _changeHandler).on('change', options.billingCountry, { 'selectors': { 'country': 'billing_country', 'state': 'billing_state', 'selected': 'select_billing_state' } }, _changeHandler);

		$this.find(options.customersCountry).trigger('change', { 'selectors': { 'country': 'customers_country', 'state': 'customers_state', 'selected': 'select_customers_state' } });
		$this.find(options.deliveryCountry).trigger('change', { 'selectors': { 'country': 'delivery_country', 'state': 'delivery_state', 'selected': 'select_delivery_state' } });
		$this.find(options.billingCountry).trigger('change', { 'selectors': { 'country': 'billing_country', 'state': 'billing_state', 'selected': 'select_billing_state' } });

		done();
	};

	// Return data to widget engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbWVycy9jdXN0b21lcnNfem9uZXNfY29udHJvbGxlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJGN1c3RvbWVyU3RhdGVzIiwiJGRlbGl2ZXJ5U3RhdGVzIiwiJGJpbGxpbmdTdGF0ZXMiLCIkY3VzdG9tZXJGb3JtR3JvdXAiLCJjbG9zZXN0IiwiJGRlbGl2ZXJ5Rm9ybUdyb3VwIiwiJGJpbGxpbmdGb3JtR3JvdXAiLCJkZWZhdWx0cyIsImxvYWRTdGF0ZXMiLCJjdXN0b21lcnNDb3VudHJ5IiwiZGVsaXZlcnlDb3VudHJ5IiwiYmlsbGluZ0NvdW50cnkiLCJvcHRpb25zIiwiZXh0ZW5kIiwiX2NoYW5nZUhhbmRsZXIiLCJlIiwiZGF0YXNldCIsImpzZSIsImxpYnMiLCJmb3JtIiwiZ2V0RGF0YSIsInNlbGVjdG9ycyIsInhociIsImFqYXgiLCJ1cmwiLCJkb25lIiwicmVzdWx0IiwiJHNlbGVjdG9yIiwiJHNlbGVjdG9yRm9ybUdyb3VwIiwic2VsZWN0b3IiLCJzdWNjZXNzIiwiY2hpbGRyZW4iLCJyZW1vdmUiLCJwcm9wIiwiZWFjaCIsImtleSIsInZhbHVlIiwic2VsZWN0ZWQiLCJhcHBlbmQiLCJ2YWwiLCJuYW1lIiwidGV4dCIsInNob3ciLCJoaWRlIiwiaW5pdCIsIm9uIiwiZmluZCIsInRyaWdnZXIiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyw0QkFERCxFQUdDLENBQ0MsTUFERCxFQUVDLEtBRkQsQ0FIRCxFQVFDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTs7QUFFQSxLQUFJQyxRQUFtQkMsRUFBRSxJQUFGLENBQXZCO0FBQUEsS0FDQ0Msa0JBQW1CRCxFQUFFLDhCQUFGLENBRHBCO0FBQUEsS0FFQ0Usa0JBQW1CRixFQUFFLDZCQUFGLENBRnBCO0FBQUEsS0FHQ0csaUJBQW1CSCxFQUFFLDRCQUFGLENBSHBCO0FBQUEsS0FLQ0kscUJBQXFCSixFQUFFLDhCQUFGLEVBQWtDSyxPQUFsQyxDQUEwQyxVQUExQyxDQUx0QjtBQUFBLEtBTUNDLHFCQUFxQk4sRUFBRSw2QkFBRixFQUFpQ0ssT0FBakMsQ0FBeUMsVUFBekMsQ0FOdEI7QUFBQSxLQU9DRSxvQkFBcUJQLEVBQUUsNEJBQUYsRUFBZ0NLLE9BQWhDLENBQXdDLFVBQXhDLENBUHRCO0FBQUEsS0FTQ0csV0FBVztBQUNWQyxjQUFZLHFDQURGO0FBRVZDLG9CQUFtQixnQ0FGVDtBQUdWQyxtQkFBbUIsMENBSFQ7QUFJVkMsa0JBQW1COztBQUpULEVBVFo7QUFBQSxLQWlCQ0MsVUFBVWIsRUFBRWMsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CTixRQUFuQixFQUE2QlYsSUFBN0IsQ0FqQlg7QUFBQSxLQWtCQ0QsU0FBUyxFQWxCVjs7QUFxQkEsS0FBSWtCLGlCQUFpQixTQUFqQkEsY0FBaUIsQ0FBU0MsQ0FBVCxFQUFZO0FBQ2hDLE1BQUlDLFVBQWVDLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxPQUFkLENBQXNCdEIsS0FBdEIsQ0FBbkI7QUFDQWtCLFVBQVFLLFNBQVIsR0FBb0JOLEVBQUVsQixJQUFGLENBQU93QixTQUEzQjs7QUFFQUosTUFBSUMsSUFBSixDQUFTSSxHQUFULENBQWFDLElBQWIsQ0FBa0IsRUFBQ0MsS0FBS1osUUFBUUosVUFBZCxFQUEwQlgsTUFBTW1CLE9BQWhDLEVBQWxCLEVBQTRELElBQTVELEVBQWtFUyxJQUFsRSxDQUF1RSxVQUFTQyxNQUFULEVBQWlCOztBQUV2RixPQUFJQyxTQUFKO0FBQ0EsT0FBSUMsa0JBQUo7O0FBRUEsV0FBUUYsT0FBT0csUUFBZjtBQUNDLFNBQUssbUJBQUw7QUFDQ0YsaUJBQVkzQixlQUFaO0FBQ0E0QiwwQkFBcUJ6QixrQkFBckI7QUFDQTtBQUNELFNBQUssa0JBQUw7QUFDQ3dCLGlCQUFZMUIsZUFBWjtBQUNBMkIsMEJBQXFCdkIsa0JBQXJCO0FBQ0E7QUFDRCxTQUFLLGlCQUFMO0FBQ0NzQixpQkFBWXpCLGNBQVo7QUFDQTBCLDBCQUFxQnRCLGlCQUFyQjtBQUNBO0FBWkY7O0FBZUEsT0FBSW9CLE9BQU9JLE9BQVgsRUFBb0I7O0FBRW5CSCxjQUFVSSxRQUFWLENBQW1CLFFBQW5CLEVBQTZCQyxNQUE3QjtBQUNBTCxjQUFVTSxJQUFWLENBQWUsVUFBZixFQUEyQixLQUEzQjs7QUFFQWxDLE1BQUVtQyxJQUFGLENBQU9SLE9BQU83QixJQUFkLEVBQW9CLFVBQVNzQyxHQUFULEVBQWNDLEtBQWQsRUFBcUI7QUFDeEMsU0FBR0EsTUFBTUMsUUFBVCxFQUNBO0FBQ0NWLGdCQUFVVyxNQUFWLENBQWlCdkMsRUFBRSxvQkFBRixFQUF3QndDLEdBQXhCLENBQTRCSCxNQUFNSSxJQUFsQyxFQUF3Q0MsSUFBeEMsQ0FBNkNMLE1BQU1JLElBQW5ELENBQWpCO0FBQ0EsTUFIRCxNQUtBO0FBQ0NiLGdCQUFVVyxNQUFWLENBQWlCdkMsRUFBRSxZQUFGLEVBQWdCd0MsR0FBaEIsQ0FBb0JILE1BQU1JLElBQTFCLEVBQWdDQyxJQUFoQyxDQUFxQ0wsTUFBTUksSUFBM0MsQ0FBakI7QUFDQTtBQUNELEtBVEQ7O0FBV0FaLHVCQUFtQmMsSUFBbkI7QUFFQSxJQWxCRCxNQW1CSztBQUNKZCx1QkFBbUJlLElBQW5CO0FBQ0FoQixjQUFVTSxJQUFWLENBQWUsVUFBZixFQUEyQixJQUEzQjtBQUNBO0FBRUQsR0E1Q0Q7QUE4Q0EsRUFsREQ7O0FBb0RBOztBQUVBOzs7O0FBSUFyQyxRQUFPZ0QsSUFBUCxHQUFjLFVBQVNuQixJQUFULEVBQWU7O0FBRTVCM0IsUUFBTStDLEVBQU4sQ0FBUyxRQUFULEVBQW1CakMsUUFBUUgsZ0JBQTNCLEVBQTZDLEVBQUMsYUFBYSxFQUFDLFdBQVcsbUJBQVosRUFBaUMsU0FBUyxpQkFBMUMsRUFBNkQsWUFBWSx3QkFBekUsRUFBZCxFQUE3QyxFQUFnS0ssY0FBaEssRUFDRStCLEVBREYsQ0FDSyxRQURMLEVBQ2VqQyxRQUFRRixlQUR2QixFQUN3QyxFQUFDLGFBQWEsRUFBQyxXQUFXLGtCQUFaLEVBQWdDLFNBQVMsZ0JBQXpDLEVBQTJELFlBQVksdUJBQXZFLEVBQWQsRUFEeEMsRUFDd0pJLGNBRHhKLEVBRUUrQixFQUZGLENBRUssUUFGTCxFQUVlakMsUUFBUUQsY0FGdkIsRUFFdUMsRUFBQyxhQUFhLEVBQUMsV0FBVyxpQkFBWixFQUErQixTQUFTLGVBQXhDLEVBQXlELFlBQVksc0JBQXJFLEVBQWQsRUFGdkMsRUFFb0pHLGNBRnBKOztBQUlBaEIsUUFBTWdELElBQU4sQ0FBV2xDLFFBQVFILGdCQUFuQixFQUFxQ3NDLE9BQXJDLENBQTZDLFFBQTdDLEVBQXVELEVBQUMsYUFBYSxFQUFDLFdBQVcsbUJBQVosRUFBaUMsU0FBUyxpQkFBMUMsRUFBNkQsWUFBWSx3QkFBekUsRUFBZCxFQUF2RDtBQUNBakQsUUFBTWdELElBQU4sQ0FBV2xDLFFBQVFGLGVBQW5CLEVBQW9DcUMsT0FBcEMsQ0FBNEMsUUFBNUMsRUFBc0QsRUFBQyxhQUFhLEVBQUMsV0FBVyxrQkFBWixFQUFnQyxTQUFTLGdCQUF6QyxFQUEyRCxZQUFZLHVCQUF2RSxFQUFkLEVBQXREO0FBQ0FqRCxRQUFNZ0QsSUFBTixDQUFXbEMsUUFBUUQsY0FBbkIsRUFBbUNvQyxPQUFuQyxDQUEyQyxRQUEzQyxFQUFxRCxFQUFDLGFBQWEsRUFBQyxXQUFXLGlCQUFaLEVBQStCLFNBQVMsZUFBeEMsRUFBeUQsWUFBWSxzQkFBckUsRUFBZCxFQUFyRDs7QUFFQXRCO0FBQ0EsRUFYRDs7QUFhQTtBQUNBLFFBQU83QixNQUFQO0FBQ0EsQ0E1R0YiLCJmaWxlIjoiY3VzdG9tZXJzL2N1c3RvbWVyc196b25lc19jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBjdXN0b21lcnNfem9uZXNfY29udHJvbGxlci5qcyAyMDE3LTAzLTIxXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBUaGUgQ29tcG9uZW50IGZvciBoYW5kbGluZyB0aGUgZmVkZXJhbCBzdGF0ZSBkcm9wZG93biBkZXBlbmRpbmcgb24gdGhlIGNvdW50cnkuXG4gKiBUaGUgZmllbGQgd2lsbCBiZSBibGFja2VkIG91dCBpZiB0aGVyZSBhcmUgbm8gZmVkZXJhbCBzdGF0ZXMgZm9yIHRoZSBzZWxlY3RlZFxuICogY291bnRyeS5cbiAqL1xuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdjdXN0b21lcnNfem9uZXNfY29udHJvbGxlcicsXG5cdFxuXHRbXG5cdFx0J2Zvcm0nLFxuXHRcdCd4aHInXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vICMjIyMjIyMjIyMgVkFSSUFCTEUgSU5JVElBTElaQVRJT04gIyMjIyMjIyMjI1xuXHRcdFxuXHRcdHZhciAkdGhpcyAgICAgICAgICAgID0gJCh0aGlzKSxcblx0XHRcdCRjdXN0b21lclN0YXRlcyAgPSAkKCdzZWxlY3RbbmFtZT1jdXN0b21lcnNfc3RhdGVdJyksXG5cdFx0XHQkZGVsaXZlcnlTdGF0ZXMgID0gJCgnc2VsZWN0W25hbWU9ZGVsaXZlcnlfc3RhdGVdJyksXG5cdFx0XHQkYmlsbGluZ1N0YXRlcyAgID0gJCgnc2VsZWN0W25hbWU9YmlsbGluZ19zdGF0ZV0nKSxcblx0XHRcdFxuXHRcdFx0JGN1c3RvbWVyRm9ybUdyb3VwID0gJCgnc2VsZWN0W25hbWU9Y3VzdG9tZXJzX3N0YXRlXScpLmNsb3Nlc3QoJ2Rpdi5ncmlkJyksXG5cdFx0XHQkZGVsaXZlcnlGb3JtR3JvdXAgPSAkKCdzZWxlY3RbbmFtZT1kZWxpdmVyeV9zdGF0ZV0nKS5jbG9zZXN0KCdkaXYuZ3JpZCcpLFxuXHRcdFx0JGJpbGxpbmdGb3JtR3JvdXAgID0gJCgnc2VsZWN0W25hbWU9YmlsbGluZ19zdGF0ZV0nKS5jbG9zZXN0KCdkaXYuZ3JpZCcpLFxuXHRcdFx0XG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0bG9hZFN0YXRlczogJ2FkbWluLnBocD9kbz1ab25lcy9PcmRlckFkZHJlc3NFZGl0Jyxcblx0XHRcdFx0Y3VzdG9tZXJzQ291bnRyeTogICdzZWxlY3RbbmFtZT1jdXN0b21lcnNfY291bnRyeV0nLFxuXHRcdFx0XHRkZWxpdmVyeUNvdW50cnk6ICAgJ3NlbGVjdFtuYW1lPWRlbGl2ZXJ5X2NvdW50cnlfaXNvX2NvZGVfMl0nLFxuXHRcdFx0XHRiaWxsaW5nQ291bnRyeTogICAgJ3NlbGVjdFtuYW1lPWJpbGxpbmdfY291bnRyeV9pc29fY29kZV8yXScsXG5cdFx0XHRcdFxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHRcblx0XHR2YXIgX2NoYW5nZUhhbmRsZXIgPSBmdW5jdGlvbihlKSB7XG5cdFx0XHR2YXIgZGF0YXNldCAgICAgID0ganNlLmxpYnMuZm9ybS5nZXREYXRhKCR0aGlzKTtcblx0XHRcdGRhdGFzZXQuc2VsZWN0b3JzID0gZS5kYXRhLnNlbGVjdG9ycztcblxuXHRcdFx0anNlLmxpYnMueGhyLmFqYXgoe3VybDogb3B0aW9ucy5sb2FkU3RhdGVzLCBkYXRhOiBkYXRhc2V0fSwgdHJ1ZSkuZG9uZShmdW5jdGlvbihyZXN1bHQpIHtcblx0XHRcdFx0XG5cdFx0XHRcdHZhciAkc2VsZWN0b3I7XG5cdFx0XHRcdHZhciAkc2VsZWN0b3JGb3JtR3JvdXA7XG5cblx0XHRcdFx0c3dpdGNoIChyZXN1bHQuc2VsZWN0b3IpIHtcblx0XHRcdFx0XHRjYXNlICdjdXN0b21lcnNfY291bnRyeSc6XG5cdFx0XHRcdFx0XHQkc2VsZWN0b3IgPSAkY3VzdG9tZXJTdGF0ZXM7XG5cdFx0XHRcdFx0XHQkc2VsZWN0b3JGb3JtR3JvdXAgPSAkY3VzdG9tZXJGb3JtR3JvdXA7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlICdkZWxpdmVyeV9jb3VudHJ5Jzpcblx0XHRcdFx0XHRcdCRzZWxlY3RvciA9ICRkZWxpdmVyeVN0YXRlcztcblx0XHRcdFx0XHRcdCRzZWxlY3RvckZvcm1Hcm91cCA9ICRkZWxpdmVyeUZvcm1Hcm91cDtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGNhc2UgJ2JpbGxpbmdfY291bnRyeSc6XG5cdFx0XHRcdFx0XHQkc2VsZWN0b3IgPSAkYmlsbGluZ1N0YXRlcztcblx0XHRcdFx0XHRcdCRzZWxlY3RvckZvcm1Hcm91cCA9ICRiaWxsaW5nRm9ybUdyb3VwO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZiAocmVzdWx0LnN1Y2Nlc3MpIHtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkc2VsZWN0b3IuY2hpbGRyZW4oJ29wdGlvbicpLnJlbW92ZSgpO1xuXHRcdFx0XHRcdCRzZWxlY3Rvci5wcm9wKFwiZGlzYWJsZWRcIiwgZmFsc2UpO1xuXG5cdFx0XHRcdFx0JC5lYWNoKHJlc3VsdC5kYXRhLCBmdW5jdGlvbihrZXksIHZhbHVlKSB7XG5cdFx0XHRcdFx0XHRpZih2YWx1ZS5zZWxlY3RlZClcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0JHNlbGVjdG9yLmFwcGVuZCgkKFwiPG9wdGlvbiBzZWxlY3RlZC8+XCIpLnZhbCh2YWx1ZS5uYW1lKS50ZXh0KHZhbHVlLm5hbWUpKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0JHNlbGVjdG9yLmFwcGVuZCgkKFwiPG9wdGlvbiAvPlwiKS52YWwodmFsdWUubmFtZSkudGV4dCh2YWx1ZS5uYW1lKSk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHNlbGVjdG9yRm9ybUdyb3VwLnNob3coKTtcblxuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Uge1xuXHRcdFx0XHRcdCRzZWxlY3RvckZvcm1Hcm91cC5oaWRlKCk7XG5cdFx0XHRcdFx0JHNlbGVjdG9yLnByb3AoXCJkaXNhYmxlZFwiLCB0cnVlKTtcblx0XHRcdFx0fVxuXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdH07XG5cdFx0XG5cdFx0Ly8gIyMjIyMjIyMjIyBJTklUSUFMSVpBVElPTiAjIyMjIyMjIyMjXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdCBmdW5jdGlvbiBvZiB0aGUgd2lkZ2V0XG5cdFx0ICogQGNvbnN0cnVjdG9yXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdjaGFuZ2UnLCBvcHRpb25zLmN1c3RvbWVyc0NvdW50cnksIHsnc2VsZWN0b3JzJzogeydjb3VudHJ5JzogJ2N1c3RvbWVyc19jb3VudHJ5JywgJ3N0YXRlJzogJ2N1c3RvbWVyc19zdGF0ZScsICdzZWxlY3RlZCc6ICdzZWxlY3RfY3VzdG9tZXJzX3N0YXRlJ319LCBfY2hhbmdlSGFuZGxlcilcblx0XHRcdFx0Lm9uKCdjaGFuZ2UnLCBvcHRpb25zLmRlbGl2ZXJ5Q291bnRyeSwgeydzZWxlY3RvcnMnOiB7J2NvdW50cnknOiAnZGVsaXZlcnlfY291bnRyeScsICdzdGF0ZSc6ICdkZWxpdmVyeV9zdGF0ZScsICdzZWxlY3RlZCc6ICdzZWxlY3RfZGVsaXZlcnlfc3RhdGUnfX0sIF9jaGFuZ2VIYW5kbGVyKVxuXHRcdFx0XHQub24oJ2NoYW5nZScsIG9wdGlvbnMuYmlsbGluZ0NvdW50cnksIHsnc2VsZWN0b3JzJzogeydjb3VudHJ5JzogJ2JpbGxpbmdfY291bnRyeScsICdzdGF0ZSc6ICdiaWxsaW5nX3N0YXRlJywgJ3NlbGVjdGVkJzogJ3NlbGVjdF9iaWxsaW5nX3N0YXRlJ319LCBfY2hhbmdlSGFuZGxlcik7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQob3B0aW9ucy5jdXN0b21lcnNDb3VudHJ5KS50cmlnZ2VyKCdjaGFuZ2UnLCB7J3NlbGVjdG9ycyc6IHsnY291bnRyeSc6ICdjdXN0b21lcnNfY291bnRyeScsICdzdGF0ZSc6ICdjdXN0b21lcnNfc3RhdGUnLCAnc2VsZWN0ZWQnOiAnc2VsZWN0X2N1c3RvbWVyc19zdGF0ZSd9fSk7XG5cdFx0XHQkdGhpcy5maW5kKG9wdGlvbnMuZGVsaXZlcnlDb3VudHJ5KS50cmlnZ2VyKCdjaGFuZ2UnLCB7J3NlbGVjdG9ycyc6IHsnY291bnRyeSc6ICdkZWxpdmVyeV9jb3VudHJ5JywgJ3N0YXRlJzogJ2RlbGl2ZXJ5X3N0YXRlJywgJ3NlbGVjdGVkJzogJ3NlbGVjdF9kZWxpdmVyeV9zdGF0ZSd9fSk7XG5cdFx0XHQkdGhpcy5maW5kKG9wdGlvbnMuYmlsbGluZ0NvdW50cnkpLnRyaWdnZXIoJ2NoYW5nZScsIHsnc2VsZWN0b3JzJzogeydjb3VudHJ5JzogJ2JpbGxpbmdfY291bnRyeScsICdzdGF0ZSc6ICdiaWxsaW5nX3N0YXRlJywgJ3NlbGVjdGVkJzogJ3NlbGVjdF9iaWxsaW5nX3N0YXRlJ319KTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gd2lkZ2V0IGVuZ2luZVxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
