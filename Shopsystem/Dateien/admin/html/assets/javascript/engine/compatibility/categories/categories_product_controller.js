'use strict';

/* --------------------------------------------------------------
 categories_product_controller.js 2015-10-15 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Categories Product Controller
 *
 * This controller contains the mapping logic of the categories save/update buttons.
 *
 * @module Compatibility/categories_product_controller
 */
gx.compatibility.module('categories_product_controller', [gx.source + '/libs/button_dropdown'],

/**  @lends module:Compatibility/categories_product_controller */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// OPERATIONS
	// ------------------------------------------------------------------------
	var _init = function _init() {
		// Hide the original buttons
		$('[name="gm_update"]').hide();
		$('[name="save_original"]').hide();

		// Map the new save option to the old save button
		jse.libs.button_dropdown.mapAction($this, 'BUTTON_SAVE', 'admin_buttons', function (event) {
			$('[name="save_original"]').trigger('click');
		});

		// Map the new update option to the old update button
		jse.libs.button_dropdown.mapAction($this, 'BUTTON_UPDATE', 'admin_buttons', function (event) {
			$('[name="gm_update"]').trigger('click');
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		_init();
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhdGVnb3JpZXMvY2F0ZWdvcmllc19wcm9kdWN0X2NvbnRyb2xsZXIuanMiXSwibmFtZXMiOlsiZ3giLCJjb21wYXRpYmlsaXR5IiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9pbml0IiwiaGlkZSIsImpzZSIsImxpYnMiLCJidXR0b25fZHJvcGRvd24iLCJtYXBBY3Rpb24iLCJldmVudCIsInRyaWdnZXIiLCJpbml0IiwiZG9uZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCLENBQ0MsK0JBREQsRUFHQyxDQUNDRixHQUFHRyxNQUFILEdBQVksdUJBRGIsQ0FIRDs7QUFPQzs7QUFFQSxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsWUFBVyxFQWJaOzs7QUFlQzs7Ozs7QUFLQUMsV0FBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FwQlg7OztBQXNCQzs7Ozs7QUFLQUYsVUFBUyxFQTNCVjs7QUE2QkE7QUFDQTtBQUNBO0FBQ0EsS0FBSVEsUUFBUSxTQUFSQSxLQUFRLEdBQVc7QUFDdEI7QUFDQUosSUFBRSxvQkFBRixFQUF3QkssSUFBeEI7QUFDQUwsSUFBRSx3QkFBRixFQUE0QkssSUFBNUI7O0FBRUE7QUFDQUMsTUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxTQUF6QixDQUFtQ1YsS0FBbkMsRUFBMEMsYUFBMUMsRUFBeUQsZUFBekQsRUFBMEUsVUFBU1csS0FBVCxFQUFnQjtBQUN6RlYsS0FBRSx3QkFBRixFQUE0QlcsT0FBNUIsQ0FBb0MsT0FBcEM7QUFDQSxHQUZEOztBQUlBO0FBQ0FMLE1BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNWLEtBQW5DLEVBQTBDLGVBQTFDLEVBQTJELGVBQTNELEVBQTRFLFVBQVNXLEtBQVQsRUFBZ0I7QUFDM0ZWLEtBQUUsb0JBQUYsRUFBd0JXLE9BQXhCLENBQWdDLE9BQWhDO0FBQ0EsR0FGRDtBQUdBLEVBZEQ7O0FBZ0JBO0FBQ0E7QUFDQTs7QUFFQWYsUUFBT2dCLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJUO0FBQ0FTO0FBQ0EsRUFIRDs7QUFLQSxRQUFPakIsTUFBUDtBQUNBLENBM0VGIiwiZmlsZSI6ImNhdGVnb3JpZXMvY2F0ZWdvcmllc19wcm9kdWN0X2NvbnRyb2xsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGNhdGVnb3JpZXNfcHJvZHVjdF9jb250cm9sbGVyLmpzIDIwMTUtMTAtMTUgZ21cbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE1IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIENhdGVnb3JpZXMgUHJvZHVjdCBDb250cm9sbGVyXG4gKlxuICogVGhpcyBjb250cm9sbGVyIGNvbnRhaW5zIHRoZSBtYXBwaW5nIGxvZ2ljIG9mIHRoZSBjYXRlZ29yaWVzIHNhdmUvdXBkYXRlIGJ1dHRvbnMuXG4gKlxuICogQG1vZHVsZSBDb21wYXRpYmlsaXR5L2NhdGVnb3JpZXNfcHJvZHVjdF9jb250cm9sbGVyXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnY2F0ZWdvcmllc19wcm9kdWN0X2NvbnRyb2xsZXInLFxuXHRcblx0W1xuXHRcdGd4LnNvdXJjZSArICcvbGlicy9idXR0b25fZHJvcGRvd24nXG5cdF0sXG5cdFxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9jYXRlZ29yaWVzX3Byb2R1Y3RfY29udHJvbGxlciAqL1xuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBPUEVSQVRJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0dmFyIF9pbml0ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQvLyBIaWRlIHRoZSBvcmlnaW5hbCBidXR0b25zXG5cdFx0XHQkKCdbbmFtZT1cImdtX3VwZGF0ZVwiXScpLmhpZGUoKTtcblx0XHRcdCQoJ1tuYW1lPVwic2F2ZV9vcmlnaW5hbFwiXScpLmhpZGUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gTWFwIHRoZSBuZXcgc2F2ZSBvcHRpb24gdG8gdGhlIG9sZCBzYXZlIGJ1dHRvblxuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLm1hcEFjdGlvbigkdGhpcywgJ0JVVFRPTl9TQVZFJywgJ2FkbWluX2J1dHRvbnMnLCBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0XHQkKCdbbmFtZT1cInNhdmVfb3JpZ2luYWxcIl0nKS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIE1hcCB0aGUgbmV3IHVwZGF0ZSBvcHRpb24gdG8gdGhlIG9sZCB1cGRhdGUgYnV0dG9uXG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24ubWFwQWN0aW9uKCR0aGlzLCAnQlVUVE9OX1VQREFURScsICdhZG1pbl9idXR0b25zJywgZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0JCgnW25hbWU9XCJnbV91cGRhdGVcIl0nKS50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0X2luaXQoKTtcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
