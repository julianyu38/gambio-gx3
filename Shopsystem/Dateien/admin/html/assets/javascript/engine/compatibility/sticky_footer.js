'use strict';

/* --------------------------------------------------------------
 sticky_footer.js 2015-09-14 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Handle footer position for backend.
 *
 * This module will handle the footer position on scrolling or whenever the page window size changes.
 *
 * @module Compatibility/sticky_footer
 */
gx.compatibility.module('sticky_footer', [],

/**  @lends module:Compatibility/sticky_footer */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Copyright Element Selector
  *
  * @var {object}
  */
	$copyright = $('.main-bottom-copyright'),


	/**
  * Footer Offset Top
  *
  * @var {int}
  */
	initialOffsetTop = $this.offset().top,


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	var _checkOffset = function _checkOffset() {
		var copyrightOffsetTop = $copyright.offset().top;
		if (copyrightOffsetTop == 0) {
			copyrightOffsetTop = 50; // default value to avoid a bug refs #55075
		}
		if ($(document).scrollTop() + window.innerHeight < copyrightOffsetTop) {
			$this.css('position', 'fixed');
		} else if ($this.offset().top + $this.height() >= copyrightOffsetTop) {
			$this.css('position', 'absolute');
		}
	};

	var _fixMainContentHeight = function _fixMainContentHeight() {
		if (initialOffsetTop + $this.height() <= window.innerHeight) {
			var newContentHeight = window.innerHeight - $('.main-page-content').offset().top;
			$('.main-page-content').css('min-height', newContentHeight + 'px');
			// First table of the page needs to be also resized.
			$('td.columnLeft2').parents('table:first').css('min-height', newContentHeight + 'px');
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Delay the footer position by some time until so that most elements are rendered
		// properly. Adjust the timeout interval approximately.
		setTimeout(function () {
			_fixMainContentHeight();

			$(window).on('scroll', _checkOffset).on('resize', _checkOffset).on('resize', _fixMainContentHeight);
			_checkOffset();
		}, 300);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0aWNreV9mb290ZXIuanMiXSwibmFtZXMiOlsiZ3giLCJjb21wYXRpYmlsaXR5IiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRjb3B5cmlnaHQiLCJpbml0aWFsT2Zmc2V0VG9wIiwib2Zmc2V0IiwidG9wIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwiX2NoZWNrT2Zmc2V0IiwiY29weXJpZ2h0T2Zmc2V0VG9wIiwiZG9jdW1lbnQiLCJzY3JvbGxUb3AiLCJ3aW5kb3ciLCJpbm5lckhlaWdodCIsImNzcyIsImhlaWdodCIsIl9maXhNYWluQ29udGVudEhlaWdodCIsIm5ld0NvbnRlbnRIZWlnaHQiLCJwYXJlbnRzIiwiaW5pdCIsImRvbmUiLCJzZXRUaW1lb3V0Iiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7OztBQU9BQSxHQUFHQyxhQUFILENBQWlCQyxNQUFqQixDQUNDLGVBREQsRUFHQyxFQUhEOztBQUtDOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxjQUFhRCxFQUFFLHdCQUFGLENBYmQ7OztBQWVDOzs7OztBQUtBRSxvQkFBbUJILE1BQU1JLE1BQU4sR0FBZUMsR0FwQm5DOzs7QUFzQkM7Ozs7O0FBS0FDLFlBQVcsRUEzQlo7OztBQTZCQzs7Ozs7QUFLQUMsV0FBVU4sRUFBRU8sTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QlAsSUFBN0IsQ0FsQ1g7OztBQW9DQzs7Ozs7QUFLQUQsVUFBUyxFQXpDVjs7QUEyQ0E7QUFDQTtBQUNBOztBQUVBLEtBQUlXLGVBQWUsU0FBZkEsWUFBZSxHQUFXO0FBQzdCLE1BQUlDLHFCQUFxQlIsV0FBV0UsTUFBWCxHQUFvQkMsR0FBN0M7QUFDQSxNQUFJSyxzQkFBc0IsQ0FBMUIsRUFBNkI7QUFDNUJBLHdCQUFxQixFQUFyQixDQUQ0QixDQUNIO0FBQ3pCO0FBQ0QsTUFBS1QsRUFBRVUsUUFBRixFQUFZQyxTQUFaLEtBQTBCQyxPQUFPQyxXQUFsQyxHQUFpREosa0JBQXJELEVBQXlFO0FBQ3hFVixTQUFNZSxHQUFOLENBQVUsVUFBVixFQUFzQixPQUF0QjtBQUNBLEdBRkQsTUFFTyxJQUFJZixNQUFNSSxNQUFOLEdBQWVDLEdBQWYsR0FBcUJMLE1BQU1nQixNQUFOLEVBQXJCLElBQXVDTixrQkFBM0MsRUFBK0Q7QUFDckVWLFNBQU1lLEdBQU4sQ0FBVSxVQUFWLEVBQXNCLFVBQXRCO0FBQ0E7QUFDRCxFQVZEOztBQVlBLEtBQUlFLHdCQUF3QixTQUF4QkEscUJBQXdCLEdBQVc7QUFDdEMsTUFBSWQsbUJBQW1CSCxNQUFNZ0IsTUFBTixFQUFuQixJQUFxQ0gsT0FBT0MsV0FBaEQsRUFBNkQ7QUFDNUQsT0FBSUksbUJBQW1CTCxPQUFPQyxXQUFQLEdBQXFCYixFQUFFLG9CQUFGLEVBQXdCRyxNQUF4QixHQUFpQ0MsR0FBN0U7QUFDQUosS0FBRSxvQkFBRixFQUF3QmMsR0FBeEIsQ0FBNEIsWUFBNUIsRUFBMENHLG1CQUFtQixJQUE3RDtBQUNBO0FBQ0FqQixLQUFFLGdCQUFGLEVBQW9Ca0IsT0FBcEIsQ0FBNEIsYUFBNUIsRUFBMkNKLEdBQTNDLENBQStDLFlBQS9DLEVBQTZERyxtQkFBbUIsSUFBaEY7QUFDQTtBQUNELEVBUEQ7O0FBU0E7QUFDQTtBQUNBOztBQUVBcEIsUUFBT3NCLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUI7QUFDQTtBQUNBQyxhQUFXLFlBQVc7QUFDckJMOztBQUVBaEIsS0FBRVksTUFBRixFQUNFVSxFQURGLENBQ0ssUUFETCxFQUNlZCxZQURmLEVBRUVjLEVBRkYsQ0FFSyxRQUZMLEVBRWVkLFlBRmYsRUFHRWMsRUFIRixDQUdLLFFBSEwsRUFHZU4scUJBSGY7QUFJQVI7QUFDQSxHQVJELEVBUUcsR0FSSDs7QUFVQVk7QUFDQSxFQWREOztBQWdCQSxRQUFPdkIsTUFBUDtBQUNBLENBeEdGIiwiZmlsZSI6InN0aWNreV9mb290ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHN0aWNreV9mb290ZXIuanMgMjAxNS0wOS0xNCBnbVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTUgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgSGFuZGxlIGZvb3RlciBwb3NpdGlvbiBmb3IgYmFja2VuZC5cbiAqXG4gKiBUaGlzIG1vZHVsZSB3aWxsIGhhbmRsZSB0aGUgZm9vdGVyIHBvc2l0aW9uIG9uIHNjcm9sbGluZyBvciB3aGVuZXZlciB0aGUgcGFnZSB3aW5kb3cgc2l6ZSBjaGFuZ2VzLlxuICpcbiAqIEBtb2R1bGUgQ29tcGF0aWJpbGl0eS9zdGlja3lfZm9vdGVyXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnc3RpY2t5X2Zvb3RlcicsXG5cdFxuXHRbXSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb21wYXRpYmlsaXR5L3N0aWNreV9mb290ZXIgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogQ29weXJpZ2h0IEVsZW1lbnQgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCRjb3B5cmlnaHQgPSAkKCcubWFpbi1ib3R0b20tY29weXJpZ2h0JyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRm9vdGVyIE9mZnNldCBUb3Bcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtpbnR9XG5cdFx0XHQgKi9cblx0XHRcdGluaXRpYWxPZmZzZXRUb3AgPSAkdGhpcy5vZmZzZXQoKS50b3AsXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXIgX2NoZWNrT2Zmc2V0ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgY29weXJpZ2h0T2Zmc2V0VG9wID0gJGNvcHlyaWdodC5vZmZzZXQoKS50b3A7XG5cdFx0XHRpZiAoY29weXJpZ2h0T2Zmc2V0VG9wID09IDApIHtcblx0XHRcdFx0Y29weXJpZ2h0T2Zmc2V0VG9wID0gNTA7IC8vIGRlZmF1bHQgdmFsdWUgdG8gYXZvaWQgYSBidWcgcmVmcyAjNTUwNzVcblx0XHRcdH1cblx0XHRcdGlmICgoJChkb2N1bWVudCkuc2Nyb2xsVG9wKCkgKyB3aW5kb3cuaW5uZXJIZWlnaHQpIDwgY29weXJpZ2h0T2Zmc2V0VG9wKSB7XG5cdFx0XHRcdCR0aGlzLmNzcygncG9zaXRpb24nLCAnZml4ZWQnKTtcblx0XHRcdH0gZWxzZSBpZiAoJHRoaXMub2Zmc2V0KCkudG9wICsgJHRoaXMuaGVpZ2h0KCkgPj0gY29weXJpZ2h0T2Zmc2V0VG9wKSB7XG5cdFx0XHRcdCR0aGlzLmNzcygncG9zaXRpb24nLCAnYWJzb2x1dGUnKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfZml4TWFpbkNvbnRlbnRIZWlnaHQgPSBmdW5jdGlvbigpIHtcblx0XHRcdGlmIChpbml0aWFsT2Zmc2V0VG9wICsgJHRoaXMuaGVpZ2h0KCkgPD0gd2luZG93LmlubmVySGVpZ2h0KSB7XG5cdFx0XHRcdHZhciBuZXdDb250ZW50SGVpZ2h0ID0gd2luZG93LmlubmVySGVpZ2h0IC0gJCgnLm1haW4tcGFnZS1jb250ZW50Jykub2Zmc2V0KCkudG9wO1xuXHRcdFx0XHQkKCcubWFpbi1wYWdlLWNvbnRlbnQnKS5jc3MoJ21pbi1oZWlnaHQnLCBuZXdDb250ZW50SGVpZ2h0ICsgJ3B4Jyk7XG5cdFx0XHRcdC8vIEZpcnN0IHRhYmxlIG9mIHRoZSBwYWdlIG5lZWRzIHRvIGJlIGFsc28gcmVzaXplZC5cblx0XHRcdFx0JCgndGQuY29sdW1uTGVmdDInKS5wYXJlbnRzKCd0YWJsZTpmaXJzdCcpLmNzcygnbWluLWhlaWdodCcsIG5ld0NvbnRlbnRIZWlnaHQgKyAncHgnKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBEZWxheSB0aGUgZm9vdGVyIHBvc2l0aW9uIGJ5IHNvbWUgdGltZSB1bnRpbCBzbyB0aGF0IG1vc3QgZWxlbWVudHMgYXJlIHJlbmRlcmVkXG5cdFx0XHQvLyBwcm9wZXJseS4gQWRqdXN0IHRoZSB0aW1lb3V0IGludGVydmFsIGFwcHJveGltYXRlbHkuXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRfZml4TWFpbkNvbnRlbnRIZWlnaHQoKTtcblx0XHRcdFx0XG5cdFx0XHRcdCQod2luZG93KVxuXHRcdFx0XHRcdC5vbignc2Nyb2xsJywgX2NoZWNrT2Zmc2V0KVxuXHRcdFx0XHRcdC5vbigncmVzaXplJywgX2NoZWNrT2Zmc2V0KVxuXHRcdFx0XHRcdC5vbigncmVzaXplJywgX2ZpeE1haW5Db250ZW50SGVpZ2h0KTtcblx0XHRcdFx0X2NoZWNrT2Zmc2V0KCk7XG5cdFx0XHR9LCAzMDApO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
