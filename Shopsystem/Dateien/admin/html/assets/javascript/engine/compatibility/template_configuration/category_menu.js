'use strict';

/* --------------------------------------------------------------
 category_menu 2017-05-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.compatibility.module('category_menu', [], function (data) {

	'use strict';

	var $this = $(this),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	var $catMenuTopSwitcher = $('input:checkbox[name="CAT_MENU_TOP"]');
	var $catMenuLeftSwitcher = $('input:checkbox[name="CAT_MENU_LEFT"]');
	var $showSubcategoriesSwitcher = $('input:checkbox[name="SHOW_SUBCATEGORIES"]');
	var $useAccordionEffectSwitcher = $('input:checkbox[name="CATEGORY_ACCORDION_EFFECT"]');

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	function _onCatMenuTopSwitcherChange() {
		if ($catMenuTopSwitcher.prop('checked') === false) {
			$catMenuLeftSwitcher.parent().addClass('checked disabled');
			$showSubcategoriesSwitcher.parent().addClass('disabled').removeClass('checked');
			$useAccordionEffectSwitcher.parent().removeClass('disabled');

			$catMenuLeftSwitcher.prop('checked', true);
			$showSubcategoriesSwitcher.prop('checked', false);
		} else {
			$catMenuLeftSwitcher.parent().removeClass('disabled');
			$showSubcategoriesSwitcher.parent().removeClass('disabled');
			$useAccordionEffectSwitcher.parent().removeClass('disabled');
		}
	}

	function _onSubcategoriesSwitcherChange() {
		if ($showSubcategoriesSwitcher.prop('checked') === true) {
			$useAccordionEffectSwitcher.parent().addClass('disabled').removeClass('checked');

			$useAccordionEffectSwitcher.prop('checked', false);
		} else {
			$useAccordionEffectSwitcher.parent().removeClass('disabled');
		}
	}

	function _onAccordionSwitcherChange() {
		if ($useAccordionEffectSwitcher.prop('checked') === true || $catMenuTopSwitcher.prop('checked') === false) {
			$showSubcategoriesSwitcher.parent().addClass('disabled').removeClass('checked');

			$showSubcategoriesSwitcher.prop('checked', false);
		} else {
			$showSubcategoriesSwitcher.parent().removeClass('disabled');
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------


	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		$this.on('checkbox:change', $catMenuTopSwitcher, _onCatMenuTopSwitcherChange);
		$this.on('checkbox:change', $showSubcategoriesSwitcher, _onSubcategoriesSwitcherChange);
		$this.on('checkbox:change', $useAccordionEffectSwitcher, _onAccordionSwitcherChange);

		$(document).on('JSENGINE_INIT_FINISHED', function () {
			_onCatMenuTopSwitcherChange();
			_onSubcategoriesSwitcherChange();
			_onAccordionSwitcherChange();
		});

		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlbXBsYXRlX2NvbmZpZ3VyYXRpb24vY2F0ZWdvcnlfbWVudS5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJGNhdE1lbnVUb3BTd2l0Y2hlciIsIiRjYXRNZW51TGVmdFN3aXRjaGVyIiwiJHNob3dTdWJjYXRlZ29yaWVzU3dpdGNoZXIiLCIkdXNlQWNjb3JkaW9uRWZmZWN0U3dpdGNoZXIiLCJfb25DYXRNZW51VG9wU3dpdGNoZXJDaGFuZ2UiLCJwcm9wIiwicGFyZW50IiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsIl9vblN1YmNhdGVnb3JpZXNTd2l0Y2hlckNoYW5nZSIsIl9vbkFjY29yZGlvblN3aXRjaGVyQ2hhbmdlIiwiaW5pdCIsImRvbmUiLCJvbiIsImRvY3VtZW50Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCLENBQXdCLGVBQXhCLEVBQXlDLEVBQXpDLEVBQTZDLFVBQVNDLElBQVQsRUFBZTs7QUFFM0Q7O0FBRUEsS0FBSUMsUUFBUUMsRUFBRSxJQUFGLENBQVo7OztBQUVDOzs7OztBQUtBSCxVQUFTLEVBUFY7O0FBU0EsS0FBTUksc0JBQXNCRCxFQUFFLHFDQUFGLENBQTVCO0FBQ0EsS0FBTUUsdUJBQXVCRixFQUFFLHNDQUFGLENBQTdCO0FBQ0EsS0FBTUcsNkJBQTZCSCxFQUFFLDJDQUFGLENBQW5DO0FBQ0EsS0FBTUksOEJBQThCSixFQUFFLGtEQUFGLENBQXBDOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxVQUFTSywyQkFBVCxHQUF1QztBQUN0QyxNQUFJSixvQkFBb0JLLElBQXBCLENBQXlCLFNBQXpCLE1BQXdDLEtBQTVDLEVBQW1EO0FBQ2xESix3QkFBcUJLLE1BQXJCLEdBQThCQyxRQUE5QixDQUF1QyxrQkFBdkM7QUFDQUwsOEJBQTJCSSxNQUEzQixHQUFvQ0MsUUFBcEMsQ0FBNkMsVUFBN0MsRUFBeURDLFdBQXpELENBQXFFLFNBQXJFO0FBQ0FMLCtCQUE0QkcsTUFBNUIsR0FBcUNFLFdBQXJDLENBQWlELFVBQWpEOztBQUVBUCx3QkFBcUJJLElBQXJCLENBQTBCLFNBQTFCLEVBQXFDLElBQXJDO0FBQ0FILDhCQUEyQkcsSUFBM0IsQ0FBZ0MsU0FBaEMsRUFBMkMsS0FBM0M7QUFDQSxHQVBELE1BT087QUFDTkosd0JBQXFCSyxNQUFyQixHQUE4QkUsV0FBOUIsQ0FBMEMsVUFBMUM7QUFDQU4sOEJBQTJCSSxNQUEzQixHQUFvQ0UsV0FBcEMsQ0FBZ0QsVUFBaEQ7QUFDQUwsK0JBQTRCRyxNQUE1QixHQUFxQ0UsV0FBckMsQ0FBaUQsVUFBakQ7QUFDQTtBQUNEOztBQUVELFVBQVNDLDhCQUFULEdBQTBDO0FBQ3pDLE1BQUlQLDJCQUEyQkcsSUFBM0IsQ0FBZ0MsU0FBaEMsTUFBK0MsSUFBbkQsRUFBeUQ7QUFDeERGLCtCQUE0QkcsTUFBNUIsR0FBcUNDLFFBQXJDLENBQThDLFVBQTlDLEVBQTBEQyxXQUExRCxDQUFzRSxTQUF0RTs7QUFFQUwsK0JBQTRCRSxJQUE1QixDQUFpQyxTQUFqQyxFQUE0QyxLQUE1QztBQUNBLEdBSkQsTUFJTztBQUNORiwrQkFBNEJHLE1BQTVCLEdBQXFDRSxXQUFyQyxDQUFpRCxVQUFqRDtBQUNBO0FBQ0Q7O0FBRUQsVUFBU0UsMEJBQVQsR0FBc0M7QUFDckMsTUFBSVAsNEJBQTRCRSxJQUE1QixDQUFpQyxTQUFqQyxNQUFnRCxJQUFoRCxJQUF3REwsb0JBQW9CSyxJQUFwQixDQUF5QixTQUF6QixNQUF3QyxLQUFwRyxFQUEyRztBQUMxR0gsOEJBQTJCSSxNQUEzQixHQUFvQ0MsUUFBcEMsQ0FBNkMsVUFBN0MsRUFBeURDLFdBQXpELENBQXFFLFNBQXJFOztBQUVBTiw4QkFBMkJHLElBQTNCLENBQWdDLFNBQWhDLEVBQTJDLEtBQTNDO0FBQ0EsR0FKRCxNQUlPO0FBQ05ILDhCQUEyQkksTUFBM0IsR0FBb0NFLFdBQXBDLENBQWdELFVBQWhEO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7OztBQUdBOzs7QUFHQVosUUFBT2UsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QmQsUUFBTWUsRUFBTixDQUFTLGlCQUFULEVBQTRCYixtQkFBNUIsRUFBaURJLDJCQUFqRDtBQUNBTixRQUFNZSxFQUFOLENBQVMsaUJBQVQsRUFBNEJYLDBCQUE1QixFQUF3RE8sOEJBQXhEO0FBQ0FYLFFBQU1lLEVBQU4sQ0FBUyxpQkFBVCxFQUE0QlYsMkJBQTVCLEVBQXlETywwQkFBekQ7O0FBRUFYLElBQUVlLFFBQUYsRUFBWUQsRUFBWixDQUFlLHdCQUFmLEVBQXlDLFlBQVc7QUFDbkRUO0FBQ0FLO0FBQ0FDO0FBQ0EsR0FKRDs7QUFPQUU7QUFDQSxFQWJEOztBQWVBO0FBQ0EsUUFBT2hCLE1BQVA7QUFDQSxDQWxGRCIsImZpbGUiOiJ0ZW1wbGF0ZV9jb25maWd1cmF0aW9uL2NhdGVnb3J5X21lbnUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGNhdGVnb3J5X21lbnUgMjAxNy0wNS0xMFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKCdjYXRlZ29yeV9tZW51JywgW10sIGZ1bmN0aW9uKGRhdGEpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdHZhciAkdGhpcyA9ICQodGhpcyksXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRtb2R1bGUgPSB7fTtcblx0XG5cdGNvbnN0ICRjYXRNZW51VG9wU3dpdGNoZXIgPSAkKCdpbnB1dDpjaGVja2JveFtuYW1lPVwiQ0FUX01FTlVfVE9QXCJdJyk7XG5cdGNvbnN0ICRjYXRNZW51TGVmdFN3aXRjaGVyID0gJCgnaW5wdXQ6Y2hlY2tib3hbbmFtZT1cIkNBVF9NRU5VX0xFRlRcIl0nKTtcblx0Y29uc3QgJHNob3dTdWJjYXRlZ29yaWVzU3dpdGNoZXIgPSAkKCdpbnB1dDpjaGVja2JveFtuYW1lPVwiU0hPV19TVUJDQVRFR09SSUVTXCJdJyk7XG5cdGNvbnN0ICR1c2VBY2NvcmRpb25FZmZlY3RTd2l0Y2hlciA9ICQoJ2lucHV0OmNoZWNrYm94W25hbWU9XCJDQVRFR09SWV9BQ0NPUkRJT05fRUZGRUNUXCJdJyk7XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gRVZFTlQgSEFORExFUlNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHRmdW5jdGlvbiBfb25DYXRNZW51VG9wU3dpdGNoZXJDaGFuZ2UoKSB7XG5cdFx0aWYgKCRjYXRNZW51VG9wU3dpdGNoZXIucHJvcCgnY2hlY2tlZCcpID09PSBmYWxzZSkge1xuXHRcdFx0JGNhdE1lbnVMZWZ0U3dpdGNoZXIucGFyZW50KCkuYWRkQ2xhc3MoJ2NoZWNrZWQgZGlzYWJsZWQnKTtcblx0XHRcdCRzaG93U3ViY2F0ZWdvcmllc1N3aXRjaGVyLnBhcmVudCgpLmFkZENsYXNzKCdkaXNhYmxlZCcpLnJlbW92ZUNsYXNzKCdjaGVja2VkJyk7XG5cdFx0XHQkdXNlQWNjb3JkaW9uRWZmZWN0U3dpdGNoZXIucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0XHRcblx0XHRcdCRjYXRNZW51TGVmdFN3aXRjaGVyLnByb3AoJ2NoZWNrZWQnLCB0cnVlKTtcblx0XHRcdCRzaG93U3ViY2F0ZWdvcmllc1N3aXRjaGVyLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdCRjYXRNZW51TGVmdFN3aXRjaGVyLnBhcmVudCgpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuXHRcdFx0JHNob3dTdWJjYXRlZ29yaWVzU3dpdGNoZXIucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0XHQkdXNlQWNjb3JkaW9uRWZmZWN0U3dpdGNoZXIucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0fVxuXHR9XG5cdFxuXHRmdW5jdGlvbiBfb25TdWJjYXRlZ29yaWVzU3dpdGNoZXJDaGFuZ2UoKSB7XG5cdFx0aWYgKCRzaG93U3ViY2F0ZWdvcmllc1N3aXRjaGVyLnByb3AoJ2NoZWNrZWQnKSA9PT0gdHJ1ZSkge1xuXHRcdFx0JHVzZUFjY29yZGlvbkVmZmVjdFN3aXRjaGVyLnBhcmVudCgpLmFkZENsYXNzKCdkaXNhYmxlZCcpLnJlbW92ZUNsYXNzKCdjaGVja2VkJyk7XG5cdFx0XHRcblx0XHRcdCR1c2VBY2NvcmRpb25FZmZlY3RTd2l0Y2hlci5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHQkdXNlQWNjb3JkaW9uRWZmZWN0U3dpdGNoZXIucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0fVxuXHR9XG5cdFxuXHRmdW5jdGlvbiBfb25BY2NvcmRpb25Td2l0Y2hlckNoYW5nZSgpIHtcblx0XHRpZiAoJHVzZUFjY29yZGlvbkVmZmVjdFN3aXRjaGVyLnByb3AoJ2NoZWNrZWQnKSA9PT0gdHJ1ZSB8fCAkY2F0TWVudVRvcFN3aXRjaGVyLnByb3AoJ2NoZWNrZWQnKSA9PT0gZmFsc2UpIHtcblx0XHRcdCRzaG93U3ViY2F0ZWdvcmllc1N3aXRjaGVyLnBhcmVudCgpLmFkZENsYXNzKCdkaXNhYmxlZCcpLnJlbW92ZUNsYXNzKCdjaGVja2VkJyk7XG5cdFx0XHRcblx0XHRcdCRzaG93U3ViY2F0ZWdvcmllc1N3aXRjaGVyLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdCRzaG93U3ViY2F0ZWdvcmllc1N3aXRjaGVyLnBhcmVudCgpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuXHRcdH1cblx0fVxuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIElOSVRJQUxJWkFUSU9OXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0XG5cdC8qKlxuXHQgKiBJbml0aWFsaXplIG1ldGhvZCBvZiB0aGUgd2lkZ2V0LCBjYWxsZWQgYnkgdGhlIGVuZ2luZS5cblx0ICovXG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdCR0aGlzLm9uKCdjaGVja2JveDpjaGFuZ2UnLCAkY2F0TWVudVRvcFN3aXRjaGVyLCBfb25DYXRNZW51VG9wU3dpdGNoZXJDaGFuZ2UpO1xuXHRcdCR0aGlzLm9uKCdjaGVja2JveDpjaGFuZ2UnLCAkc2hvd1N1YmNhdGVnb3JpZXNTd2l0Y2hlciwgX29uU3ViY2F0ZWdvcmllc1N3aXRjaGVyQ2hhbmdlKTtcblx0XHQkdGhpcy5vbignY2hlY2tib3g6Y2hhbmdlJywgJHVzZUFjY29yZGlvbkVmZmVjdFN3aXRjaGVyLCBfb25BY2NvcmRpb25Td2l0Y2hlckNoYW5nZSk7XG5cdFx0XG5cdFx0JChkb2N1bWVudCkub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCBmdW5jdGlvbigpIHtcblx0XHRcdF9vbkNhdE1lbnVUb3BTd2l0Y2hlckNoYW5nZSgpO1xuXHRcdFx0X29uU3ViY2F0ZWdvcmllc1N3aXRjaGVyQ2hhbmdlKCk7XG5cdFx0XHRfb25BY2NvcmRpb25Td2l0Y2hlckNoYW5nZSgpO1xuXHRcdH0pO1xuXHRcdFxuXHRcdFxuXHRcdGRvbmUoKTtcblx0fTtcblx0XG5cdC8vIFJldHVybiBkYXRhIHRvIG1vZHVsZSBlbmdpbmUuXG5cdHJldHVybiBtb2R1bGU7XG59KTsiXX0=
