'use strict';

/* --------------------------------------------------------------
 customers_table_controller.js 2018-05-16
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Customers Table Controller
 *
 * This controller contains the mapping logic of the customers table.
 *
 * @module Compatibility/customers_table_controller
 */
gx.compatibility.module('customers_table_controller', [gx.source + '/libs/button_dropdown'],

/**  @lends module:Compatibility/customers_table_controller */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		'adminAccessGranted': false,
		'customerGroupsGranted': false,
		'mailbeezGranted': false,
		'mediafinanzGranted': false,
		'ordersGranted': false
	},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {},


	/**
  * Reference to the actual file
  *
  * @var {string}
  */
	srcPath = window.location.origin + window.location.pathname,


	/**
  * Query parameter string
  * 
  * @type {string}
  */
	queryString = '?' + window.location.search.replace(/\?/, '').replace(/cID=[\d]+/g, '').replace(/action=[\w]+/g, '').concat('&').replace(/&[&]+/g, '&').replace(/^&/g, '');

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Map actions for every row in the table.
  *
  * This method will map the actions for each
  * row of the table.
  *
  * @private
  */
	var _mapRowActions = function _mapRowActions() {
		// Iterate over table rows, except the header row
		$('.gx-customer-overview tr').not('.dataTableHeadingRow').each(function () {

			/**
    * Save that "this" scope here
    * @var {object | jQuery}
    */
			var $that = $(this);

			/**
    * Data attributes of current row
    * @var {object}
    */
			var data = $that.data();

			/**
    * Reference to the row action dropdown
    * @var {object | jQuery}
    */
			var $dropdown = $that.find('.js-button-dropdown');

			if ($dropdown.length) {

				// Add click event to the table row and open the
				// customer detail view
				$that.find('.btn-edit').closest('tr').css({
					cursor: 'pointer'
				}).on('click', function (event) {
					// Compose the URL and open it
					var url = [srcPath, '?cID=' + data.rowId, '&action=edit'].join('');
					if ($(event.target).prop('tagName') === 'TD') {
						window.open(url, '_self');
					}
				});

				// Icon behavior - Edit
				$that.find('.btn-edit').css({
					cursor: 'pointer'
				}).prop('title', jse.core.lang.translate('edit', 'buttons')).on('click', function () {
					// Compose the URL and open it
					var url = [srcPath, '?cID=' + data.rowId, '&action=edit'].join('');
					window.open(url, '_self');
				});

				// Icon behavior - Delete
				if (data.rowId !== 1) {
					$that.find('.btn-delete').css({
						cursor: 'pointer'
					}).prop('title', jse.core.lang.translate('delete', 'buttons')).on('click', function () {
						// Compose the URL and open it
						var url = [srcPath, queryString, 'cID=' + data.rowId, '&action=confirm'].join('');
						window.open(url, '_self');
					});
				}

				if (data.rowId === 1) {
					$that.find('.btn-delete').css({
						opacity: '0.2'
					});
				}

				// Icon behavior - Orders
				if (options.ordersGranted) {
					$that.find('.btn-order').css({
						cursor: 'pointer'
					}).prop('title', jse.core.lang.translate('BUTTON_ORDERS', 'admin_buttons')).on('click', function () {
						// Compose the URL and open it
						var url = [srcPath.replace('customers.php', 'admin.php'), '?' + $.param({
							do: 'OrdersOverview',
							filter: {
								customer: '#' + data.rowId
							}
						})].join('');
						window.open(url, '_self');
					});
				}

				_mapButtonDropdown($that, $dropdown, data);
			}
		});
	};

	var _mapButtonDropdown = function _mapButtonDropdown($that, $dropdown, data) {
		var actions = ['edit'];

		// Bind dropdown option - Delete
		if (data.rowId !== 1) {
			actions.push('delete');
		}

		actions = actions.concat(['BUTTON_EMAIL', 'BUTTON_IPLOG', 'export_personal_data']);

		if ($that.find('[data-cust-group]').data('custGroup') !== 0) {
			actions.push('delete_personal_data');
		}

		if ($that.find('[data-cust-group]').data('custGroup') !== 0) {
			actions.push('BUTTON_LOGIN_AS_CUSTOMER');
		}

		if (options.customerGroupsGranted) {
			actions.push('BUTTON_STATUS');
		}

		if (options.ordersGranted) {
			actions.push('BUTTON_ORDERS');
			actions.push('BUTTON_NEW_ORDER');
		}

		// Admin rights button
		if ($that.find('[data-cust-group]').data('custGroup') === 0 && options.adminAccessGranted) {
			actions.push('BUTTON_ACCOUNTING');
		}

		// Bind MailBeez dropdown options.
		var mailBeezConversationsSelector = '.contentTable .infoBoxContent a.context_view_button.btn_right';
		if ($(mailBeezConversationsSelector).length && options.mailbeezGranted) {
			actions.push('MAILBEEZ_OVERVIEW');
			actions.push('MAILBEEZ_NOTIFICATIONS');
			actions.push('MAILBEEZ_CONVERSATIONS');
		}

		// Bind Mediafinanz dropdown options.
		var $mediafinanzAction = $('.mediafinanz-creditworthiness');
		if ($mediafinanzAction.length && options.mediafinanzGranted) {
			actions.push('BUTTON_MEDIAFINANZ_CREDITWORTHINESS');
		}

		for (var index in actions) {
			_mapCustomerAction($dropdown, actions[index], data);
		}
	};

	var _mapCustomerAction = function _mapCustomerAction($dropdown, action, data) {
		jse.libs.button_dropdown.mapAction($dropdown, action, _sectionMapping[action], function (event) {
			_executeActionCallback(action, data);
		});
	};

	var _sectionMapping = {
		edit: 'buttons',
		delete: 'buttons',
		BUTTON_STATUS: 'admin_buttons',
		BUTTON_ORDERS: 'admin_buttons',
		BUTTON_EMAIL: 'admin_buttons',
		BUTTON_IPLOG: 'admin_buttons',
		MAILBEEZ_OVERVIEW: 'admin_customers',
		MAILBEEZ_NOTIFICATIONS: 'admin_customers',
		MAILBEEZ_CONVERSATIONS: 'admin_customers',
		BUTTON_MEDIAFINANZ_CREDITWORTHINESS: 'admin_buttons',
		BUTTON_NEW_ORDER: 'admin_buttons',
		BUTTON_ACCOUNTING: 'admin_buttons',
		BUTTON_LOGIN_AS_CUSTOMER: 'admin_customers',
		delete_personal_data: 'admin_customers',
		export_personal_data: 'admin_customers'
	};

	/**
  * Get the corresponding callback
  *
  * @param action
  * @private
  */
	var _executeActionCallback = function _executeActionCallback(action, data) {
		switch (action) {
			case 'edit':
				_editCallback(data);
				break;
			case 'delete':
				_deleteCallback(data);
				break;
			case 'BUTTON_STATUS':
				_customerGroupCallBack(data);
				break;
			case 'BUTTON_ORDERS':
				_ordersCallback(data);
				break;
			case 'BUTTON_EMAIL':
				_emailCallback(data);
				break;
			case 'BUTTON_IPLOG':
				_ipLogCallback(data);
				break;
			case 'MAILBEEZ_OVERVIEW':
				_mailBeezOverviewCallback(data);
				break;
			case 'MAILBEEZ_NOTIFICATIONS':
				_mailBeezNotificationsCallback(data);
				break;
			case 'MAILBEEZ_CONVERSATIONS':
				_mailBeezConversationsCallback(data);
				break;
			case 'BUTTON_MEDIAFINANZ_CREDITWORTHINESS':
				_mediafinanzCreditworthinessCallback(data);
				break;
			case 'BUTTON_NEW_ORDER':
				_newOrderCallback(data);
				break;
			case 'BUTTON_ACCOUNTING':
				_adminRightsCallback(data);
				break;
			case 'BUTTON_LOGIN_AS_CUSTOMER':
				_loginAsCustomerCallback(data);
				break;
			case 'delete_personal_data':
				_deletePersonalDataCallback(data);
				break;
			case 'export_personal_data':
				_exportPersonalDataCallback(data);
				break;
			default:
				throw new Error('Callback not found.');
				break;
		}
	};

	var _editCallback = function _editCallback(data) {
		// Compose the URL and open it
		var url = [srcPath, '?cID=' + data.rowId, '&action=edit'].join('');
		window.open(url, '_self');
	};

	var _deleteCallback = function _deleteCallback(data) {
		// Compose the URL and open it
		var url = [srcPath, queryString, 'cID=' + data.rowId, '&action=confirm'].join('');
		window.open(url, '_self');
	};

	var _customerGroupCallBack = function _customerGroupCallBack(data) {
		// Compose the URL and open it
		var url = [srcPath, queryString, 'cID=' + data.rowId, '&action=editstatus'].join('');
		window.open(url, '_self');
	};

	var _ordersCallback = function _ordersCallback(data) {
		// Compose the URL and open it
		var url = [srcPath.replace('customers.php', 'admin.php'), '?' + $.param({
			do: 'OrdersOverview',
			filter: {
				customer: '#' + data.rowId
			}
		})].join('');
		window.open(url, '_self');
	};

	var _emailCallback = function _emailCallback(data) {
		// Compose the URL and open it
		var url = [srcPath.replace('customers.php', 'mail.php'), '?selected_box=tools', '&customer=' + data.custEmail].join('');
		window.open(url, '_self');
	};

	var _ipLogCallback = function _ipLogCallback(data) {
		// Compose the URL and open it
		var url = [srcPath, queryString, 'cID=' + data.rowId, '&action=iplog'].join('');
		window.open(url, '_self');
	};

	var _mailBeezOverviewCallback = function _mailBeezOverviewCallback(data) {
		var $target = $('.contentTable .infoBoxContent a.context_view_button.btn_left');
		var url = $('.contentTable .infoBoxContent a.context_view_button.btn_left').attr('onclick');
		url = url.replace(/cID=(.*)&/, 'cID=' + data.rowId + '&');
		$('.contentTable .infoBoxContent a.context_view_button.btn_left').attr('onclick', url);
		$target.get(0).click();
	};

	var _mailBeezNotificationsCallback = function _mailBeezNotificationsCallback(data) {
		var $target = $('.contentTable .infoBoxContent a.context_view_button.btn_middle');
		var url = $('.contentTable .infoBoxContent a.context_view_button.btn_middle').attr('onclick');
		url = url.replace(/cID=(.*)&/, 'cID=' + data.rowId + '&');
		$('.contentTable .infoBoxContent a.context_view_button.btn_middle').attr('onclick', url);
		$target.get(0).click();
	};

	var _mailBeezConversationsCallback = function _mailBeezConversationsCallback(data) {
		var $target = $('.contentTable .infoBoxContent a.context_view_button.btn_right');
		var url = $('.contentTable .infoBoxContent a.context_view_button.btn_right').attr('onclick');
		url = url.replace(/cID=(.*)&/, 'cID=' + data.rowId + '&');
		$('.contentTable .infoBoxContent a.context_view_button.btn_right').attr('onclick', url);
		$target.get(0).click();
	};

	var _mediafinanzCreditworthinessCallback = function _mediafinanzCreditworthinessCallback(data) {
		var $target = $('.mediafinanz-creditworthiness');
		var onclickAttribute = $target.attr('onclick');
		// Replace the customer number in the onclick attribute. 
		onclickAttribute = onclickAttribute.replace(/cID=(.*', 'popup')/, 'cID=' + data.rowId + '\', \'popup\'');
		$target.attr('onclick', onclickAttribute);
		$target.trigger('click'); // Trigger the click event in the <a> element. 
	};

	var _newOrderCallback = function _newOrderCallback(data) {
		// Compose the URL and open it
		var url = [srcPath, '?cID=' + data.rowId, '&action=new_order'].join('');
		window.open(url, '_self');
	};

	var _adminRightsCallback = function _adminRightsCallback(data) {
		// Compose the URL and open it
		var url = 'admin.php?do=AdminAccess/editAdmin&id=' + data.rowId;
		window.open(url, '_self');
	};

	var _loginAsCustomerCallback = function _loginAsCustomerCallback(data) {
		// Compose the URL and open it
		var url = [srcPath.replace('customers.php', 'admin.php'), '?do=CustomerLogin&customerId=' + data.rowId + '&pageToken=' + jse.core.config.get('pageToken')].join('');
		window.open(url, '_self');
	};

	var _deletePersonalDataCallback = function _deletePersonalDataCallback(data) {
		// Compose the URL and open it
		var url = [srcPath, queryString, 'cID=' + data.rowId, '&action=delete_personal_data'].join('');
		window.open(url, '_self');
	};

	var _exportPersonalDataCallback = function _exportPersonalDataCallback(data) {
		// Compose the URL and open it
		var url = [srcPath, queryString, 'cID=' + data.rowId, '&action=export_personal_data'].join('');
		window.open(url, '_self');
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Wait until the buttons are converted to dropdown for every row.
		var interval = setInterval(function () {
			if ($('.js-button-dropdown').length) {
				clearInterval(interval);
				_mapRowActions();
			}
		}, 500);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbWVycy9jdXN0b21lcnNfdGFibGVfY29udHJvbGxlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwic3JjUGF0aCIsIndpbmRvdyIsImxvY2F0aW9uIiwib3JpZ2luIiwicGF0aG5hbWUiLCJxdWVyeVN0cmluZyIsInNlYXJjaCIsInJlcGxhY2UiLCJjb25jYXQiLCJfbWFwUm93QWN0aW9ucyIsIm5vdCIsImVhY2giLCIkdGhhdCIsIiRkcm9wZG93biIsImZpbmQiLCJsZW5ndGgiLCJjbG9zZXN0IiwiY3NzIiwiY3Vyc29yIiwib24iLCJldmVudCIsInVybCIsInJvd0lkIiwiam9pbiIsInRhcmdldCIsInByb3AiLCJvcGVuIiwianNlIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJvcGFjaXR5Iiwib3JkZXJzR3JhbnRlZCIsInBhcmFtIiwiZG8iLCJmaWx0ZXIiLCJjdXN0b21lciIsIl9tYXBCdXR0b25Ecm9wZG93biIsImFjdGlvbnMiLCJwdXNoIiwiY3VzdG9tZXJHcm91cHNHcmFudGVkIiwiYWRtaW5BY2Nlc3NHcmFudGVkIiwibWFpbEJlZXpDb252ZXJzYXRpb25zU2VsZWN0b3IiLCJtYWlsYmVlekdyYW50ZWQiLCIkbWVkaWFmaW5hbnpBY3Rpb24iLCJtZWRpYWZpbmFuekdyYW50ZWQiLCJpbmRleCIsIl9tYXBDdXN0b21lckFjdGlvbiIsImFjdGlvbiIsImxpYnMiLCJidXR0b25fZHJvcGRvd24iLCJtYXBBY3Rpb24iLCJfc2VjdGlvbk1hcHBpbmciLCJfZXhlY3V0ZUFjdGlvbkNhbGxiYWNrIiwiZWRpdCIsImRlbGV0ZSIsIkJVVFRPTl9TVEFUVVMiLCJCVVRUT05fT1JERVJTIiwiQlVUVE9OX0VNQUlMIiwiQlVUVE9OX0lQTE9HIiwiTUFJTEJFRVpfT1ZFUlZJRVciLCJNQUlMQkVFWl9OT1RJRklDQVRJT05TIiwiTUFJTEJFRVpfQ09OVkVSU0FUSU9OUyIsIkJVVFRPTl9NRURJQUZJTkFOWl9DUkVESVRXT1JUSElORVNTIiwiQlVUVE9OX05FV19PUkRFUiIsIkJVVFRPTl9BQ0NPVU5USU5HIiwiQlVUVE9OX0xPR0lOX0FTX0NVU1RPTUVSIiwiZGVsZXRlX3BlcnNvbmFsX2RhdGEiLCJleHBvcnRfcGVyc29uYWxfZGF0YSIsIl9lZGl0Q2FsbGJhY2siLCJfZGVsZXRlQ2FsbGJhY2siLCJfY3VzdG9tZXJHcm91cENhbGxCYWNrIiwiX29yZGVyc0NhbGxiYWNrIiwiX2VtYWlsQ2FsbGJhY2siLCJfaXBMb2dDYWxsYmFjayIsIl9tYWlsQmVlek92ZXJ2aWV3Q2FsbGJhY2siLCJfbWFpbEJlZXpOb3RpZmljYXRpb25zQ2FsbGJhY2siLCJfbWFpbEJlZXpDb252ZXJzYXRpb25zQ2FsbGJhY2siLCJfbWVkaWFmaW5hbnpDcmVkaXR3b3J0aGluZXNzQ2FsbGJhY2siLCJfbmV3T3JkZXJDYWxsYmFjayIsIl9hZG1pblJpZ2h0c0NhbGxiYWNrIiwiX2xvZ2luQXNDdXN0b21lckNhbGxiYWNrIiwiX2RlbGV0ZVBlcnNvbmFsRGF0YUNhbGxiYWNrIiwiX2V4cG9ydFBlcnNvbmFsRGF0YUNhbGxiYWNrIiwiRXJyb3IiLCJjdXN0RW1haWwiLCIkdGFyZ2V0IiwiYXR0ciIsImdldCIsImNsaWNrIiwib25jbGlja0F0dHJpYnV0ZSIsInRyaWdnZXIiLCJjb25maWciLCJpbml0IiwiZG9uZSIsImludGVydmFsIiwic2V0SW50ZXJ2YWwiLCJjbGVhckludGVydmFsIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyw0QkFERCxFQUdDLENBQ0NGLEdBQUdHLE1BQUgsR0FBWSx1QkFEYixDQUhEOztBQU9DOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXO0FBQ1Ysd0JBQXNCLEtBRFo7QUFFViwyQkFBeUIsS0FGZjtBQUdWLHFCQUFtQixLQUhUO0FBSVYsd0JBQXNCLEtBSlo7QUFLVixtQkFBaUI7QUFMUCxFQWJaOzs7QUFxQkM7Ozs7O0FBS0FDLFdBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBMUJYOzs7QUE0QkM7Ozs7O0FBS0FGLFVBQVMsRUFqQ1Y7OztBQW1DQzs7Ozs7QUFLQVEsV0FBVUMsT0FBT0MsUUFBUCxDQUFnQkMsTUFBaEIsR0FBeUJGLE9BQU9DLFFBQVAsQ0FBZ0JFLFFBeENwRDs7O0FBMENDOzs7OztBQUtBQyxlQUFjLE1BQU9KLE9BQU9DLFFBQVAsQ0FBZ0JJLE1BQWhCLENBQ2ZDLE9BRGUsQ0FDUCxJQURPLEVBQ0QsRUFEQyxFQUVmQSxPQUZlLENBRVAsWUFGTyxFQUVPLEVBRlAsRUFHZkEsT0FIZSxDQUdQLGVBSE8sRUFHVSxFQUhWLEVBSWZDLE1BSmUsQ0FJUixHQUpRLEVBS2ZELE9BTGUsQ0FLUCxRQUxPLEVBS0csR0FMSCxFQU1mQSxPQU5lLENBTVAsS0FOTyxFQU1BLEVBTkEsQ0EvQ3RCOztBQXVEQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FBUUEsS0FBSUUsaUJBQWlCLFNBQWpCQSxjQUFpQixHQUFXO0FBQy9CO0FBQ0FiLElBQUUsMEJBQUYsRUFBOEJjLEdBQTlCLENBQWtDLHNCQUFsQyxFQUEwREMsSUFBMUQsQ0FBK0QsWUFBVzs7QUFFekU7Ozs7QUFJQSxPQUFJQyxRQUFRaEIsRUFBRSxJQUFGLENBQVo7O0FBRUE7Ozs7QUFJQSxPQUFJRixPQUFPa0IsTUFBTWxCLElBQU4sRUFBWDs7QUFFQTs7OztBQUlBLE9BQUltQixZQUFZRCxNQUFNRSxJQUFOLENBQVcscUJBQVgsQ0FBaEI7O0FBRUEsT0FBSUQsVUFBVUUsTUFBZCxFQUFzQjs7QUFFckI7QUFDQTtBQUNBSCxVQUNFRSxJQURGLENBQ08sV0FEUCxFQUNvQkUsT0FEcEIsQ0FDNEIsSUFENUIsRUFFRUMsR0FGRixDQUVNO0FBQ0pDLGFBQVE7QUFESixLQUZOLEVBS0VDLEVBTEYsQ0FLSyxPQUxMLEVBS2MsVUFBU0MsS0FBVCxFQUFnQjtBQUM1QjtBQUNBLFNBQUlDLE1BQU0sQ0FDVHJCLE9BRFMsRUFFVCxVQUFVTixLQUFLNEIsS0FGTixFQUdULGNBSFMsRUFJUkMsSUFKUSxDQUlILEVBSkcsQ0FBVjtBQUtBLFNBQUkzQixFQUFFd0IsTUFBTUksTUFBUixFQUFnQkMsSUFBaEIsQ0FBcUIsU0FBckIsTUFBb0MsSUFBeEMsRUFBOEM7QUFDN0N4QixhQUFPeUIsSUFBUCxDQUFZTCxHQUFaLEVBQWlCLE9BQWpCO0FBQ0E7QUFDRCxLQWZGOztBQWlCQTtBQUNBVCxVQUNFRSxJQURGLENBQ08sV0FEUCxFQUVFRyxHQUZGLENBRU07QUFDSkMsYUFBUTtBQURKLEtBRk4sRUFLRU8sSUFMRixDQUtPLE9BTFAsRUFLZ0JFLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE1BQXhCLEVBQWdDLFNBQWhDLENBTGhCLEVBTUVYLEVBTkYsQ0FNSyxPQU5MLEVBTWMsWUFBVztBQUN2QjtBQUNBLFNBQUlFLE1BQU0sQ0FDVHJCLE9BRFMsRUFFVCxVQUFVTixLQUFLNEIsS0FGTixFQUdULGNBSFMsRUFJUkMsSUFKUSxDQUlILEVBSkcsQ0FBVjtBQUtBdEIsWUFBT3lCLElBQVAsQ0FBWUwsR0FBWixFQUFpQixPQUFqQjtBQUNBLEtBZEY7O0FBZ0JBO0FBQ0EsUUFBSTNCLEtBQUs0QixLQUFMLEtBQWUsQ0FBbkIsRUFBc0I7QUFDckJWLFdBQ0VFLElBREYsQ0FDTyxhQURQLEVBRUVHLEdBRkYsQ0FFTTtBQUNKQyxjQUFRO0FBREosTUFGTixFQUtFTyxJQUxGLENBS08sT0FMUCxFQUtnQkUsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsUUFBeEIsRUFBa0MsU0FBbEMsQ0FMaEIsRUFNRVgsRUFORixDQU1LLE9BTkwsRUFNYyxZQUFXO0FBQ3ZCO0FBQ0EsVUFBSUUsTUFBTSxDQUNUckIsT0FEUyxFQUVUSyxXQUZTLEVBR1QsU0FBU1gsS0FBSzRCLEtBSEwsRUFJVCxpQkFKUyxFQUtSQyxJQUxRLENBS0gsRUFMRyxDQUFWO0FBTUF0QixhQUFPeUIsSUFBUCxDQUFZTCxHQUFaLEVBQWlCLE9BQWpCO0FBQ0EsTUFmRjtBQWdCQTs7QUFFRCxRQUFJM0IsS0FBSzRCLEtBQUwsS0FBZSxDQUFuQixFQUFzQjtBQUNyQlYsV0FBTUUsSUFBTixDQUFXLGFBQVgsRUFBMEJHLEdBQTFCLENBQThCO0FBQzdCYyxlQUFTO0FBRG9CLE1BQTlCO0FBR0E7O0FBRUQ7QUFDQSxRQUFHakMsUUFBUWtDLGFBQVgsRUFBMEI7QUFDekJwQixXQUNFRSxJQURGLENBQ08sWUFEUCxFQUVFRyxHQUZGLENBRU07QUFDSkMsY0FBUTtBQURKLE1BRk4sRUFLRU8sSUFMRixDQUtPLE9BTFAsRUFLZ0JFLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGVBQXhCLEVBQXlDLGVBQXpDLENBTGhCLEVBTUVYLEVBTkYsQ0FNSyxPQU5MLEVBTWMsWUFBVztBQUN2QjtBQUNBLFVBQUlFLE1BQU0sQ0FDVHJCLFFBQVFPLE9BQVIsQ0FBZ0IsZUFBaEIsRUFBaUMsV0FBakMsQ0FEUyxFQUVULE1BQU1YLEVBQUVxQyxLQUFGLENBQVE7QUFDYkMsV0FBSSxnQkFEUztBQUViQyxlQUFRO0FBQ1BDLGtCQUFVLE1BQU0xQyxLQUFLNEI7QUFEZDtBQUZLLE9BQVIsQ0FGRyxFQVFSQyxJQVJRLENBUUgsRUFSRyxDQUFWO0FBU0F0QixhQUFPeUIsSUFBUCxDQUFZTCxHQUFaLEVBQWlCLE9BQWpCO0FBQ0EsTUFsQkY7QUFtQkE7O0FBRURnQix1QkFBbUJ6QixLQUFuQixFQUEwQkMsU0FBMUIsRUFBcUNuQixJQUFyQztBQUNBO0FBQ0QsR0E3R0Q7QUE4R0EsRUFoSEQ7O0FBa0hBLEtBQUkyQyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFTekIsS0FBVCxFQUFnQkMsU0FBaEIsRUFBMkJuQixJQUEzQixFQUFpQztBQUN6RCxNQUFJNEMsVUFBVSxDQUFDLE1BQUQsQ0FBZDs7QUFFQTtBQUNBLE1BQUk1QyxLQUFLNEIsS0FBTCxLQUFlLENBQW5CLEVBQXNCO0FBQ3JCZ0IsV0FBUUMsSUFBUixDQUFhLFFBQWI7QUFDQTs7QUFFREQsWUFBVUEsUUFBUTlCLE1BQVIsQ0FBZSxDQUN4QixjQUR3QixFQUV4QixjQUZ3QixFQUd4QixzQkFId0IsQ0FBZixDQUFWOztBQU1BLE1BQUlJLE1BQU1FLElBQU4sQ0FBVyxtQkFBWCxFQUFnQ3BCLElBQWhDLENBQXFDLFdBQXJDLE1BQXNELENBQTFELEVBQTZEO0FBQ2hENEMsV0FBUUMsSUFBUixDQUFhLHNCQUFiO0FBQ1o7O0FBRUQsTUFBSTNCLE1BQU1FLElBQU4sQ0FBVyxtQkFBWCxFQUFnQ3BCLElBQWhDLENBQXFDLFdBQXJDLE1BQXNELENBQTFELEVBQTZEO0FBQzVENEMsV0FBUUMsSUFBUixDQUFhLDBCQUFiO0FBQ0E7O0FBRUQsTUFBSXpDLFFBQVEwQyxxQkFBWixFQUFtQztBQUNsQ0YsV0FBUUMsSUFBUixDQUFhLGVBQWI7QUFDQTs7QUFFRCxNQUFJekMsUUFBUWtDLGFBQVosRUFBMkI7QUFDMUJNLFdBQVFDLElBQVIsQ0FBYSxlQUFiO0FBQ0FELFdBQVFDLElBQVIsQ0FBYSxrQkFBYjtBQUNBOztBQUVEO0FBQ0EsTUFBSTNCLE1BQU1FLElBQU4sQ0FBVyxtQkFBWCxFQUFnQ3BCLElBQWhDLENBQXFDLFdBQXJDLE1BQXNELENBQXRELElBQTJESSxRQUFRMkMsa0JBQXZFLEVBQTJGO0FBQzFGSCxXQUFRQyxJQUFSLENBQWEsbUJBQWI7QUFDQTs7QUFFRDtBQUNBLE1BQUlHLGdDQUNILCtEQUREO0FBRUEsTUFBSTlDLEVBQUU4Qyw2QkFBRixFQUFpQzNCLE1BQWpDLElBQTJDakIsUUFBUTZDLGVBQXZELEVBQXdFO0FBQ3ZFTCxXQUFRQyxJQUFSLENBQWEsbUJBQWI7QUFDQUQsV0FBUUMsSUFBUixDQUFhLHdCQUFiO0FBQ0FELFdBQVFDLElBQVIsQ0FBYSx3QkFBYjtBQUNBOztBQUVEO0FBQ0EsTUFBSUsscUJBQXFCaEQsRUFBRSwrQkFBRixDQUF6QjtBQUNBLE1BQUlnRCxtQkFBbUI3QixNQUFuQixJQUE2QmpCLFFBQVErQyxrQkFBekMsRUFBNkQ7QUFDNURQLFdBQVFDLElBQVIsQ0FBYSxxQ0FBYjtBQUNBOztBQUVELE9BQUssSUFBSU8sS0FBVCxJQUFrQlIsT0FBbEIsRUFBMkI7QUFDMUJTLHNCQUFtQmxDLFNBQW5CLEVBQThCeUIsUUFBUVEsS0FBUixDQUE5QixFQUE4Q3BELElBQTlDO0FBQ0E7QUFDRCxFQXRERDs7QUF3REEsS0FBSXFELHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQVNsQyxTQUFULEVBQW9CbUMsTUFBcEIsRUFBNEJ0RCxJQUE1QixFQUFrQztBQUMxRGlDLE1BQUlzQixJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLFNBQXpCLENBQW1DdEMsU0FBbkMsRUFBOENtQyxNQUE5QyxFQUFzREksZ0JBQWdCSixNQUFoQixDQUF0RCxFQUErRSxVQUFTNUIsS0FBVCxFQUFnQjtBQUM5RmlDLDBCQUF1QkwsTUFBdkIsRUFBK0J0RCxJQUEvQjtBQUNBLEdBRkQ7QUFHQSxFQUpEOztBQU1BLEtBQUkwRCxrQkFBa0I7QUFDckJFLFFBQU0sU0FEZTtBQUVyQkMsVUFBUSxTQUZhO0FBR3JCQyxpQkFBZSxlQUhNO0FBSXJCQyxpQkFBZSxlQUpNO0FBS3JCQyxnQkFBYyxlQUxPO0FBTXJCQyxnQkFBYyxlQU5PO0FBT3JCQyxxQkFBbUIsaUJBUEU7QUFRckJDLDBCQUF3QixpQkFSSDtBQVNyQkMsMEJBQXdCLGlCQVRIO0FBVXJCQyx1Q0FBcUMsZUFWaEI7QUFXckJDLG9CQUFrQixlQVhHO0FBWXJCQyxxQkFBbUIsZUFaRTtBQWFyQkMsNEJBQTBCLGlCQWJMO0FBY3JCQyx3QkFBc0IsaUJBZEQ7QUFlckJDLHdCQUFzQjtBQWZELEVBQXRCOztBQWtCQTs7Ozs7O0FBTUEsS0FBSWYseUJBQXlCLFNBQXpCQSxzQkFBeUIsQ0FBU0wsTUFBVCxFQUFpQnRELElBQWpCLEVBQXVCO0FBQ25ELFVBQVFzRCxNQUFSO0FBQ0MsUUFBSyxNQUFMO0FBQ0NxQixrQkFBYzNFLElBQWQ7QUFDQTtBQUNELFFBQUssUUFBTDtBQUNDNEUsb0JBQWdCNUUsSUFBaEI7QUFDQTtBQUNELFFBQUssZUFBTDtBQUNDNkUsMkJBQXVCN0UsSUFBdkI7QUFDQTtBQUNELFFBQUssZUFBTDtBQUNDOEUsb0JBQWdCOUUsSUFBaEI7QUFDQTtBQUNELFFBQUssY0FBTDtBQUNDK0UsbUJBQWUvRSxJQUFmO0FBQ0E7QUFDRCxRQUFLLGNBQUw7QUFDQ2dGLG1CQUFlaEYsSUFBZjtBQUNBO0FBQ0QsUUFBSyxtQkFBTDtBQUNDaUYsOEJBQTBCakYsSUFBMUI7QUFDQTtBQUNELFFBQUssd0JBQUw7QUFDQ2tGLG1DQUErQmxGLElBQS9CO0FBQ0E7QUFDRCxRQUFLLHdCQUFMO0FBQ0NtRixtQ0FBK0JuRixJQUEvQjtBQUNBO0FBQ0QsUUFBSyxxQ0FBTDtBQUNDb0YseUNBQXFDcEYsSUFBckM7QUFDQTtBQUNELFFBQUssa0JBQUw7QUFDQ3FGLHNCQUFrQnJGLElBQWxCO0FBQ0E7QUFDRCxRQUFLLG1CQUFMO0FBQ0NzRix5QkFBcUJ0RixJQUFyQjtBQUNBO0FBQ0QsUUFBSywwQkFBTDtBQUNDdUYsNkJBQXlCdkYsSUFBekI7QUFDQTtBQUNELFFBQUssc0JBQUw7QUFDQ3dGLGdDQUE0QnhGLElBQTVCO0FBQ0E7QUFDRCxRQUFLLHNCQUFMO0FBQ0N5RixnQ0FBNEJ6RixJQUE1QjtBQUNBO0FBQ0Q7QUFDQyxVQUFNLElBQUkwRixLQUFKLENBQVUscUJBQVYsQ0FBTjtBQUNBO0FBaERGO0FBa0RBLEVBbkREOztBQXFEQSxLQUFJZixnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVMzRSxJQUFULEVBQWU7QUFDbEM7QUFDQSxNQUFJMkIsTUFBTSxDQUNUckIsT0FEUyxFQUVULFVBQVVOLEtBQUs0QixLQUZOLEVBR1QsY0FIUyxFQUlSQyxJQUpRLENBSUgsRUFKRyxDQUFWO0FBS0F0QixTQUFPeUIsSUFBUCxDQUFZTCxHQUFaLEVBQWlCLE9BQWpCO0FBQ0EsRUFSRDs7QUFVQSxLQUFJaUQsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFTNUUsSUFBVCxFQUFlO0FBQ3BDO0FBQ0EsTUFBSTJCLE1BQU0sQ0FDVHJCLE9BRFMsRUFFVEssV0FGUyxFQUdULFNBQVNYLEtBQUs0QixLQUhMLEVBSVQsaUJBSlMsRUFLUkMsSUFMUSxDQUtILEVBTEcsQ0FBVjtBQU1BdEIsU0FBT3lCLElBQVAsQ0FBWUwsR0FBWixFQUFpQixPQUFqQjtBQUNBLEVBVEQ7O0FBV0EsS0FBSWtELHlCQUF5QixTQUF6QkEsc0JBQXlCLENBQVM3RSxJQUFULEVBQWU7QUFDM0M7QUFDQSxNQUFJMkIsTUFBTSxDQUNUckIsT0FEUyxFQUVUSyxXQUZTLEVBR1QsU0FBU1gsS0FBSzRCLEtBSEwsRUFJVCxvQkFKUyxFQUtSQyxJQUxRLENBS0gsRUFMRyxDQUFWO0FBTUF0QixTQUFPeUIsSUFBUCxDQUFZTCxHQUFaLEVBQWlCLE9BQWpCO0FBQ0EsRUFURDs7QUFXQSxLQUFJbUQsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFTOUUsSUFBVCxFQUFlO0FBQ3BDO0FBQ0EsTUFBSTJCLE1BQU0sQ0FDVHJCLFFBQVFPLE9BQVIsQ0FBZ0IsZUFBaEIsRUFBaUMsV0FBakMsQ0FEUyxFQUVULE1BQU1YLEVBQUVxQyxLQUFGLENBQVE7QUFDYkMsT0FBSSxnQkFEUztBQUViQyxXQUFRO0FBQ1BDLGNBQVUsTUFBTTFDLEtBQUs0QjtBQURkO0FBRkssR0FBUixDQUZHLEVBUVJDLElBUlEsQ0FRSCxFQVJHLENBQVY7QUFTQXRCLFNBQU95QixJQUFQLENBQVlMLEdBQVosRUFBaUIsT0FBakI7QUFDQSxFQVpEOztBQWNBLEtBQUlvRCxpQkFBaUIsU0FBakJBLGNBQWlCLENBQVMvRSxJQUFULEVBQWU7QUFDbkM7QUFDQSxNQUFJMkIsTUFBTSxDQUNUckIsUUFBUU8sT0FBUixDQUFnQixlQUFoQixFQUFpQyxVQUFqQyxDQURTLEVBRVQscUJBRlMsRUFHVCxlQUFlYixLQUFLMkYsU0FIWCxFQUlSOUQsSUFKUSxDQUlILEVBSkcsQ0FBVjtBQUtBdEIsU0FBT3lCLElBQVAsQ0FBWUwsR0FBWixFQUFpQixPQUFqQjtBQUNBLEVBUkQ7O0FBVUEsS0FBSXFELGlCQUFpQixTQUFqQkEsY0FBaUIsQ0FBU2hGLElBQVQsRUFBZTtBQUNuQztBQUNBLE1BQUkyQixNQUFNLENBQ1RyQixPQURTLEVBRVRLLFdBRlMsRUFHVCxTQUFTWCxLQUFLNEIsS0FITCxFQUlULGVBSlMsRUFLUkMsSUFMUSxDQUtILEVBTEcsQ0FBVjtBQU1BdEIsU0FBT3lCLElBQVAsQ0FBWUwsR0FBWixFQUFpQixPQUFqQjtBQUNBLEVBVEQ7O0FBV0EsS0FBSXNELDRCQUE0QixTQUE1QkEseUJBQTRCLENBQVNqRixJQUFULEVBQWU7QUFDOUMsTUFBSTRGLFVBQVUxRixFQUFFLDhEQUFGLENBQWQ7QUFDQSxNQUFJeUIsTUFBTXpCLEVBQUUsOERBQUYsRUFBa0UyRixJQUFsRSxDQUNULFNBRFMsQ0FBVjtBQUVBbEUsUUFBTUEsSUFBSWQsT0FBSixDQUFZLFdBQVosRUFBeUIsU0FBU2IsS0FBSzRCLEtBQWQsR0FBc0IsR0FBL0MsQ0FBTjtBQUNBMUIsSUFBRSw4REFBRixFQUFrRTJGLElBQWxFLENBQXVFLFNBQXZFLEVBQWtGbEUsR0FBbEY7QUFDQWlFLFVBQVFFLEdBQVIsQ0FBWSxDQUFaLEVBQWVDLEtBQWY7QUFDQSxFQVBEOztBQVNBLEtBQUliLGlDQUFpQyxTQUFqQ0EsOEJBQWlDLENBQVNsRixJQUFULEVBQWU7QUFDbkQsTUFBSTRGLFVBQVUxRixFQUFFLGdFQUFGLENBQWQ7QUFDQSxNQUFJeUIsTUFBTXpCLEVBQUUsZ0VBQUYsRUFBb0UyRixJQUFwRSxDQUNULFNBRFMsQ0FBVjtBQUVBbEUsUUFBTUEsSUFBSWQsT0FBSixDQUFZLFdBQVosRUFBeUIsU0FBU2IsS0FBSzRCLEtBQWQsR0FBc0IsR0FBL0MsQ0FBTjtBQUNBMUIsSUFBRSxnRUFBRixFQUFvRTJGLElBQXBFLENBQXlFLFNBQXpFLEVBQ0NsRSxHQUREO0FBRUFpRSxVQUFRRSxHQUFSLENBQVksQ0FBWixFQUFlQyxLQUFmO0FBQ0EsRUFSRDs7QUFVQSxLQUFJWixpQ0FBaUMsU0FBakNBLDhCQUFpQyxDQUFTbkYsSUFBVCxFQUFlO0FBQ25ELE1BQUk0RixVQUFVMUYsRUFBRSwrREFBRixDQUFkO0FBQ0EsTUFBSXlCLE1BQU16QixFQUFFLCtEQUFGLEVBQW1FMkYsSUFBbkUsQ0FDVCxTQURTLENBQVY7QUFFQWxFLFFBQU1BLElBQUlkLE9BQUosQ0FBWSxXQUFaLEVBQXlCLFNBQVNiLEtBQUs0QixLQUFkLEdBQXNCLEdBQS9DLENBQU47QUFDQTFCLElBQUUsK0RBQUYsRUFBbUUyRixJQUFuRSxDQUF3RSxTQUF4RSxFQUNDbEUsR0FERDtBQUVBaUUsVUFBUUUsR0FBUixDQUFZLENBQVosRUFBZUMsS0FBZjtBQUNBLEVBUkQ7O0FBVUEsS0FBSVgsdUNBQXVDLFNBQXZDQSxvQ0FBdUMsQ0FBU3BGLElBQVQsRUFBZTtBQUN6RCxNQUFJNEYsVUFBVTFGLEVBQUUsK0JBQUYsQ0FBZDtBQUNBLE1BQUk4RixtQkFBbUJKLFFBQVFDLElBQVIsQ0FBYSxTQUFiLENBQXZCO0FBQ0E7QUFDQUcscUJBQW1CQSxpQkFBaUJuRixPQUFqQixDQUF5QixvQkFBekIsRUFBK0MsU0FBU2IsS0FBSzRCLEtBQWQsR0FBc0IsZUFBckUsQ0FBbkI7QUFDQWdFLFVBQVFDLElBQVIsQ0FBYSxTQUFiLEVBQXdCRyxnQkFBeEI7QUFDQUosVUFBUUssT0FBUixDQUFnQixPQUFoQixFQU55RCxDQU0vQjtBQUMxQixFQVBEOztBQVNBLEtBQUlaLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQVNyRixJQUFULEVBQWU7QUFDdEM7QUFDQSxNQUFJMkIsTUFBTSxDQUNUckIsT0FEUyxFQUVULFVBQVVOLEtBQUs0QixLQUZOLEVBR1QsbUJBSFMsRUFJUkMsSUFKUSxDQUlILEVBSkcsQ0FBVjtBQUtBdEIsU0FBT3lCLElBQVAsQ0FBWUwsR0FBWixFQUFpQixPQUFqQjtBQUNBLEVBUkQ7O0FBVUEsS0FBSTJELHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQVN0RixJQUFULEVBQWU7QUFDekM7QUFDQSxNQUFJMkIsTUFBTSwyQ0FBMkMzQixLQUFLNEIsS0FBMUQ7QUFDQXJCLFNBQU95QixJQUFQLENBQVlMLEdBQVosRUFBaUIsT0FBakI7QUFDQSxFQUpEOztBQU1BLEtBQUk0RCwyQkFBMkIsU0FBM0JBLHdCQUEyQixDQUFTdkYsSUFBVCxFQUFlO0FBQzdDO0FBQ0EsTUFBSTJCLE1BQU0sQ0FDVHJCLFFBQVFPLE9BQVIsQ0FBZ0IsZUFBaEIsRUFBaUMsV0FBakMsQ0FEUyxFQUVULGtDQUFrQ2IsS0FBSzRCLEtBQXZDLEdBQStDLGFBQS9DLEdBQStESyxJQUFJQyxJQUFKLENBQVNnRSxNQUFULENBQWdCSixHQUFoQixDQUFvQixXQUFwQixDQUZ0RCxFQUdSakUsSUFIUSxDQUdILEVBSEcsQ0FBVjtBQUlBdEIsU0FBT3lCLElBQVAsQ0FBWUwsR0FBWixFQUFpQixPQUFqQjtBQUNBLEVBUEQ7O0FBU0EsS0FBSTZELDhCQUE4QixTQUE5QkEsMkJBQThCLENBQVN4RixJQUFULEVBQWU7QUFDaEQ7QUFDQSxNQUFJMkIsTUFBTSxDQUNUckIsT0FEUyxFQUVUSyxXQUZTLEVBR1QsU0FBU1gsS0FBSzRCLEtBSEwsRUFJVCw4QkFKUyxFQUtSQyxJQUxRLENBS0gsRUFMRyxDQUFWO0FBTUF0QixTQUFPeUIsSUFBUCxDQUFZTCxHQUFaLEVBQWlCLE9BQWpCO0FBQ0EsRUFURDs7QUFXTSxLQUFJOEQsOEJBQThCLFNBQTlCQSwyQkFBOEIsQ0FBU3pGLElBQVQsRUFBZTtBQUM3QztBQUNBLE1BQUkyQixNQUFNLENBQ05yQixPQURNLEVBRU5LLFdBRk0sRUFHTixTQUFTWCxLQUFLNEIsS0FIUixFQUlOLDhCQUpNLEVBS1JDLElBTFEsQ0FLSCxFQUxHLENBQVY7QUFNQXRCLFNBQU95QixJQUFQLENBQVlMLEdBQVosRUFBaUIsT0FBakI7QUFDSCxFQVREOztBQVdOO0FBQ0E7QUFDQTs7QUFFQTdCLFFBQU9xRyxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCO0FBQ0EsTUFBSUMsV0FBV0MsWUFBWSxZQUFXO0FBQ3JDLE9BQUlwRyxFQUFFLHFCQUFGLEVBQXlCbUIsTUFBN0IsRUFBcUM7QUFDcENrRixrQkFBY0YsUUFBZDtBQUNBdEY7QUFDQTtBQUNELEdBTGMsRUFLWixHQUxZLENBQWY7O0FBT0FxRjtBQUNBLEVBVkQ7O0FBWUEsUUFBT3RHLE1BQVA7QUFDQSxDQTFmRiIsImZpbGUiOiJjdXN0b21lcnMvY3VzdG9tZXJzX3RhYmxlX2NvbnRyb2xsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGN1c3RvbWVyc190YWJsZV9jb250cm9sbGVyLmpzIDIwMTgtMDUtMTZcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIEN1c3RvbWVycyBUYWJsZSBDb250cm9sbGVyXG4gKlxuICogVGhpcyBjb250cm9sbGVyIGNvbnRhaW5zIHRoZSBtYXBwaW5nIGxvZ2ljIG9mIHRoZSBjdXN0b21lcnMgdGFibGUuXG4gKlxuICogQG1vZHVsZSBDb21wYXRpYmlsaXR5L2N1c3RvbWVyc190YWJsZV9jb250cm9sbGVyXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnY3VzdG9tZXJzX3RhYmxlX2NvbnRyb2xsZXInLFxuXHRcblx0W1xuXHRcdGd4LnNvdXJjZSArICcvbGlicy9idXR0b25fZHJvcGRvd24nXG5cdF0sXG5cdFxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9jdXN0b21lcnNfdGFibGVfY29udHJvbGxlciAqL1xuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0J2FkbWluQWNjZXNzR3JhbnRlZCc6IGZhbHNlLFxuXHRcdFx0XHQnY3VzdG9tZXJHcm91cHNHcmFudGVkJzogZmFsc2UsXG5cdFx0XHRcdCdtYWlsYmVlekdyYW50ZWQnOiBmYWxzZSxcblx0XHRcdFx0J21lZGlhZmluYW56R3JhbnRlZCc6IGZhbHNlLFxuXHRcdFx0XHQnb3JkZXJzR3JhbnRlZCc6IGZhbHNlXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBSZWZlcmVuY2UgdG8gdGhlIGFjdHVhbCBmaWxlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7c3RyaW5nfVxuXHRcdFx0ICovXG5cdFx0XHRzcmNQYXRoID0gd2luZG93LmxvY2F0aW9uLm9yaWdpbiArIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSxcblx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogUXVlcnkgcGFyYW1ldGVyIHN0cmluZ1xuXHRcdFx0ICogXG5cdFx0XHQgKiBAdHlwZSB7c3RyaW5nfVxuXHRcdFx0ICovXG5cdFx0XHRxdWVyeVN0cmluZyA9ICc/JyArICh3aW5kb3cubG9jYXRpb24uc2VhcmNoXG5cdFx0XHRcdFx0XHRcdFx0LnJlcGxhY2UoL1xcPy8sICcnKVxuXHRcdFx0XHRcdFx0XHRcdC5yZXBsYWNlKC9jSUQ9W1xcZF0rL2csICcnKVxuXHRcdFx0XHRcdFx0XHRcdC5yZXBsYWNlKC9hY3Rpb249W1xcd10rL2csICcnKVxuXHRcdFx0XHRcdFx0XHRcdC5jb25jYXQoJyYnKVxuXHRcdFx0XHRcdFx0XHRcdC5yZXBsYWNlKC8mWyZdKy9nLCAnJicpXG5cdFx0XHRcdFx0XHRcdFx0LnJlcGxhY2UoL14mL2csICcnKSk7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBNRVRIT0RTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTWFwIGFjdGlvbnMgZm9yIGV2ZXJ5IHJvdyBpbiB0aGUgdGFibGUuXG5cdFx0ICpcblx0XHQgKiBUaGlzIG1ldGhvZCB3aWxsIG1hcCB0aGUgYWN0aW9ucyBmb3IgZWFjaFxuXHRcdCAqIHJvdyBvZiB0aGUgdGFibGUuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfbWFwUm93QWN0aW9ucyA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0Ly8gSXRlcmF0ZSBvdmVyIHRhYmxlIHJvd3MsIGV4Y2VwdCB0aGUgaGVhZGVyIHJvd1xuXHRcdFx0JCgnLmd4LWN1c3RvbWVyLW92ZXJ2aWV3IHRyJykubm90KCcuZGF0YVRhYmxlSGVhZGluZ1JvdycpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICogU2F2ZSB0aGF0IFwidGhpc1wiIHNjb3BlIGhlcmVcblx0XHRcdFx0ICogQHZhciB7b2JqZWN0IHwgalF1ZXJ5fVxuXHRcdFx0XHQgKi9cblx0XHRcdFx0dmFyICR0aGF0ID0gJCh0aGlzKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBEYXRhIGF0dHJpYnV0ZXMgb2YgY3VycmVudCByb3dcblx0XHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0XHQgKi9cblx0XHRcdFx0dmFyIGRhdGEgPSAkdGhhdC5kYXRhKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICogUmVmZXJlbmNlIHRvIHRoZSByb3cgYWN0aW9uIGRyb3Bkb3duXG5cdFx0XHRcdCAqIEB2YXIge29iamVjdCB8IGpRdWVyeX1cblx0XHRcdFx0ICovXG5cdFx0XHRcdHZhciAkZHJvcGRvd24gPSAkdGhhdC5maW5kKCcuanMtYnV0dG9uLWRyb3Bkb3duJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoJGRyb3Bkb3duLmxlbmd0aCkge1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIEFkZCBjbGljayBldmVudCB0byB0aGUgdGFibGUgcm93IGFuZCBvcGVuIHRoZVxuXHRcdFx0XHRcdC8vIGN1c3RvbWVyIGRldGFpbCB2aWV3XG5cdFx0XHRcdFx0JHRoYXRcblx0XHRcdFx0XHRcdC5maW5kKCcuYnRuLWVkaXQnKS5jbG9zZXN0KCd0cicpXG5cdFx0XHRcdFx0XHQuY3NzKHtcblx0XHRcdFx0XHRcdFx0Y3Vyc29yOiAncG9pbnRlcidcblx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHQub24oJ2NsaWNrJywgZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0XHRcdFx0Ly8gQ29tcG9zZSB0aGUgVVJMIGFuZCBvcGVuIGl0XG5cdFx0XHRcdFx0XHRcdHZhciB1cmwgPSBbXG5cdFx0XHRcdFx0XHRcdFx0c3JjUGF0aCxcblx0XHRcdFx0XHRcdFx0XHQnP2NJRD0nICsgZGF0YS5yb3dJZCxcblx0XHRcdFx0XHRcdFx0XHQnJmFjdGlvbj1lZGl0J1xuXHRcdFx0XHRcdFx0XHRdLmpvaW4oJycpO1xuXHRcdFx0XHRcdFx0XHRpZiAoJChldmVudC50YXJnZXQpLnByb3AoJ3RhZ05hbWUnKSA9PT0gJ1REJykge1xuXHRcdFx0XHRcdFx0XHRcdHdpbmRvdy5vcGVuKHVybCwgJ19zZWxmJyk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIEljb24gYmVoYXZpb3IgLSBFZGl0XG5cdFx0XHRcdFx0JHRoYXRcblx0XHRcdFx0XHRcdC5maW5kKCcuYnRuLWVkaXQnKVxuXHRcdFx0XHRcdFx0LmNzcyh7XG5cdFx0XHRcdFx0XHRcdGN1cnNvcjogJ3BvaW50ZXInXG5cdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0LnByb3AoJ3RpdGxlJywganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2VkaXQnLCAnYnV0dG9ucycpKVxuXHRcdFx0XHRcdFx0Lm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHQvLyBDb21wb3NlIHRoZSBVUkwgYW5kIG9wZW4gaXRcblx0XHRcdFx0XHRcdFx0dmFyIHVybCA9IFtcblx0XHRcdFx0XHRcdFx0XHRzcmNQYXRoLFxuXHRcdFx0XHRcdFx0XHRcdCc/Y0lEPScgKyBkYXRhLnJvd0lkLFxuXHRcdFx0XHRcdFx0XHRcdCcmYWN0aW9uPWVkaXQnXG5cdFx0XHRcdFx0XHRcdF0uam9pbignJyk7XG5cdFx0XHRcdFx0XHRcdHdpbmRvdy5vcGVuKHVybCwgJ19zZWxmJyk7XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBJY29uIGJlaGF2aW9yIC0gRGVsZXRlXG5cdFx0XHRcdFx0aWYgKGRhdGEucm93SWQgIT09IDEpIHtcblx0XHRcdFx0XHRcdCR0aGF0XG5cdFx0XHRcdFx0XHRcdC5maW5kKCcuYnRuLWRlbGV0ZScpXG5cdFx0XHRcdFx0XHRcdC5jc3Moe1xuXHRcdFx0XHRcdFx0XHRcdGN1cnNvcjogJ3BvaW50ZXInXG5cdFx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHRcdC5wcm9wKCd0aXRsZScsIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdkZWxldGUnLCAnYnV0dG9ucycpKVxuXHRcdFx0XHRcdFx0XHQub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdFx0Ly8gQ29tcG9zZSB0aGUgVVJMIGFuZCBvcGVuIGl0XG5cdFx0XHRcdFx0XHRcdFx0dmFyIHVybCA9IFtcblx0XHRcdFx0XHRcdFx0XHRcdHNyY1BhdGgsXG5cdFx0XHRcdFx0XHRcdFx0XHRxdWVyeVN0cmluZyxcblx0XHRcdFx0XHRcdFx0XHRcdCdjSUQ9JyArIGRhdGEucm93SWQsXG5cdFx0XHRcdFx0XHRcdFx0XHQnJmFjdGlvbj1jb25maXJtJ1xuXHRcdFx0XHRcdFx0XHRcdF0uam9pbignJyk7XG5cdFx0XHRcdFx0XHRcdFx0d2luZG93Lm9wZW4odXJsLCAnX3NlbGYnKTtcblx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmIChkYXRhLnJvd0lkID09PSAxKSB7XG5cdFx0XHRcdFx0XHQkdGhhdC5maW5kKCcuYnRuLWRlbGV0ZScpLmNzcyh7XG5cdFx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwLjInXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gSWNvbiBiZWhhdmlvciAtIE9yZGVyc1xuXHRcdFx0XHRcdGlmKG9wdGlvbnMub3JkZXJzR3JhbnRlZCkge1xuXHRcdFx0XHRcdFx0JHRoYXRcblx0XHRcdFx0XHRcdFx0LmZpbmQoJy5idG4tb3JkZXInKVxuXHRcdFx0XHRcdFx0XHQuY3NzKHtcblx0XHRcdFx0XHRcdFx0XHRjdXJzb3I6ICdwb2ludGVyJ1xuXHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHQucHJvcCgndGl0bGUnLCBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX09SREVSUycsICdhZG1pbl9idXR0b25zJykpXG5cdFx0XHRcdFx0XHRcdC5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0XHQvLyBDb21wb3NlIHRoZSBVUkwgYW5kIG9wZW4gaXRcblx0XHRcdFx0XHRcdFx0XHR2YXIgdXJsID0gW1xuXHRcdFx0XHRcdFx0XHRcdFx0c3JjUGF0aC5yZXBsYWNlKCdjdXN0b21lcnMucGhwJywgJ2FkbWluLnBocCcpLFxuXHRcdFx0XHRcdFx0XHRcdFx0Jz8nICsgJC5wYXJhbSh7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdGRvOiAnT3JkZXJzT3ZlcnZpZXcnLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRmaWx0ZXI6IHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRjdXN0b21lcjogJyMnICsgZGF0YS5yb3dJZFxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHRcdF0uam9pbignJyk7XG5cdFx0XHRcdFx0XHRcdFx0d2luZG93Lm9wZW4odXJsLCAnX3NlbGYnKTtcblx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdF9tYXBCdXR0b25Ecm9wZG93bigkdGhhdCwgJGRyb3Bkb3duLCBkYXRhKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX21hcEJ1dHRvbkRyb3Bkb3duID0gZnVuY3Rpb24oJHRoYXQsICRkcm9wZG93biwgZGF0YSkge1xuXHRcdFx0dmFyIGFjdGlvbnMgPSBbJ2VkaXQnXTtcblx0XHRcdFxuXHRcdFx0Ly8gQmluZCBkcm9wZG93biBvcHRpb24gLSBEZWxldGVcblx0XHRcdGlmIChkYXRhLnJvd0lkICE9PSAxKSB7XG5cdFx0XHRcdGFjdGlvbnMucHVzaCgnZGVsZXRlJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGFjdGlvbnMgPSBhY3Rpb25zLmNvbmNhdChbXG5cdFx0XHRcdCdCVVRUT05fRU1BSUwnLFxuXHRcdFx0XHQnQlVUVE9OX0lQTE9HJyxcblx0XHRcdFx0J2V4cG9ydF9wZXJzb25hbF9kYXRhJ1xuXHRcdFx0XSk7XG5cblx0XHRcdGlmICgkdGhhdC5maW5kKCdbZGF0YS1jdXN0LWdyb3VwXScpLmRhdGEoJ2N1c3RHcm91cCcpICE9PSAwKSB7XG4gICAgICAgICAgICAgICAgYWN0aW9ucy5wdXNoKCdkZWxldGVfcGVyc29uYWxfZGF0YScpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAoJHRoYXQuZmluZCgnW2RhdGEtY3VzdC1ncm91cF0nKS5kYXRhKCdjdXN0R3JvdXAnKSAhPT0gMCkge1xuXHRcdFx0XHRhY3Rpb25zLnB1c2goJ0JVVFRPTl9MT0dJTl9BU19DVVNUT01FUicpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAob3B0aW9ucy5jdXN0b21lckdyb3Vwc0dyYW50ZWQpIHtcblx0XHRcdFx0YWN0aW9ucy5wdXNoKCdCVVRUT05fU1RBVFVTJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmIChvcHRpb25zLm9yZGVyc0dyYW50ZWQpIHtcblx0XHRcdFx0YWN0aW9ucy5wdXNoKCdCVVRUT05fT1JERVJTJyk7XG5cdFx0XHRcdGFjdGlvbnMucHVzaCgnQlVUVE9OX05FV19PUkRFUicpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBBZG1pbiByaWdodHMgYnV0dG9uXG5cdFx0XHRpZiAoJHRoYXQuZmluZCgnW2RhdGEtY3VzdC1ncm91cF0nKS5kYXRhKCdjdXN0R3JvdXAnKSA9PT0gMCAmJiBvcHRpb25zLmFkbWluQWNjZXNzR3JhbnRlZCkge1xuXHRcdFx0XHRhY3Rpb25zLnB1c2goJ0JVVFRPTl9BQ0NPVU5USU5HJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIEJpbmQgTWFpbEJlZXogZHJvcGRvd24gb3B0aW9ucy5cblx0XHRcdHZhciBtYWlsQmVlekNvbnZlcnNhdGlvbnNTZWxlY3RvciA9XG5cdFx0XHRcdCcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmNvbnRleHRfdmlld19idXR0b24uYnRuX3JpZ2h0Jztcblx0XHRcdGlmICgkKG1haWxCZWV6Q29udmVyc2F0aW9uc1NlbGVjdG9yKS5sZW5ndGggJiYgb3B0aW9ucy5tYWlsYmVlekdyYW50ZWQpIHtcblx0XHRcdFx0YWN0aW9ucy5wdXNoKCdNQUlMQkVFWl9PVkVSVklFVycpO1xuXHRcdFx0XHRhY3Rpb25zLnB1c2goJ01BSUxCRUVaX05PVElGSUNBVElPTlMnKTtcblx0XHRcdFx0YWN0aW9ucy5wdXNoKCdNQUlMQkVFWl9DT05WRVJTQVRJT05TJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIEJpbmQgTWVkaWFmaW5hbnogZHJvcGRvd24gb3B0aW9ucy5cblx0XHRcdHZhciAkbWVkaWFmaW5hbnpBY3Rpb24gPSAkKCcubWVkaWFmaW5hbnotY3JlZGl0d29ydGhpbmVzcycpOyBcblx0XHRcdGlmICgkbWVkaWFmaW5hbnpBY3Rpb24ubGVuZ3RoICYmIG9wdGlvbnMubWVkaWFmaW5hbnpHcmFudGVkKSB7XG5cdFx0XHRcdGFjdGlvbnMucHVzaCgnQlVUVE9OX01FRElBRklOQU5aX0NSRURJVFdPUlRISU5FU1MnKTsgXG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGZvciAodmFyIGluZGV4IGluIGFjdGlvbnMpIHtcblx0XHRcdFx0X21hcEN1c3RvbWVyQWN0aW9uKCRkcm9wZG93biwgYWN0aW9uc1tpbmRleF0sIGRhdGEpO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9tYXBDdXN0b21lckFjdGlvbiA9IGZ1bmN0aW9uKCRkcm9wZG93biwgYWN0aW9uLCBkYXRhKSB7XG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24ubWFwQWN0aW9uKCRkcm9wZG93biwgYWN0aW9uLCBfc2VjdGlvbk1hcHBpbmdbYWN0aW9uXSwgZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0X2V4ZWN1dGVBY3Rpb25DYWxsYmFjayhhY3Rpb24sIGRhdGEpO1xuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX3NlY3Rpb25NYXBwaW5nID0ge1xuXHRcdFx0ZWRpdDogJ2J1dHRvbnMnLFxuXHRcdFx0ZGVsZXRlOiAnYnV0dG9ucycsXG5cdFx0XHRCVVRUT05fU1RBVFVTOiAnYWRtaW5fYnV0dG9ucycsXG5cdFx0XHRCVVRUT05fT1JERVJTOiAnYWRtaW5fYnV0dG9ucycsXG5cdFx0XHRCVVRUT05fRU1BSUw6ICdhZG1pbl9idXR0b25zJyxcblx0XHRcdEJVVFRPTl9JUExPRzogJ2FkbWluX2J1dHRvbnMnLFxuXHRcdFx0TUFJTEJFRVpfT1ZFUlZJRVc6ICdhZG1pbl9jdXN0b21lcnMnLFxuXHRcdFx0TUFJTEJFRVpfTk9USUZJQ0FUSU9OUzogJ2FkbWluX2N1c3RvbWVycycsXG5cdFx0XHRNQUlMQkVFWl9DT05WRVJTQVRJT05TOiAnYWRtaW5fY3VzdG9tZXJzJyxcblx0XHRcdEJVVFRPTl9NRURJQUZJTkFOWl9DUkVESVRXT1JUSElORVNTOiAnYWRtaW5fYnV0dG9ucycsXG5cdFx0XHRCVVRUT05fTkVXX09SREVSOiAnYWRtaW5fYnV0dG9ucycsXG5cdFx0XHRCVVRUT05fQUNDT1VOVElORzogJ2FkbWluX2J1dHRvbnMnLFxuXHRcdFx0QlVUVE9OX0xPR0lOX0FTX0NVU1RPTUVSOiAnYWRtaW5fY3VzdG9tZXJzJyxcblx0XHRcdGRlbGV0ZV9wZXJzb25hbF9kYXRhOiAnYWRtaW5fY3VzdG9tZXJzJyxcblx0XHRcdGV4cG9ydF9wZXJzb25hbF9kYXRhOiAnYWRtaW5fY3VzdG9tZXJzJ1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IHRoZSBjb3JyZXNwb25kaW5nIGNhbGxiYWNrXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0gYWN0aW9uXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX2V4ZWN1dGVBY3Rpb25DYWxsYmFjayA9IGZ1bmN0aW9uKGFjdGlvbiwgZGF0YSkge1xuXHRcdFx0c3dpdGNoIChhY3Rpb24pIHtcblx0XHRcdFx0Y2FzZSAnZWRpdCc6XG5cdFx0XHRcdFx0X2VkaXRDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnZGVsZXRlJzpcblx0XHRcdFx0XHRfZGVsZXRlQ2FsbGJhY2soZGF0YSk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UgJ0JVVFRPTl9TVEFUVVMnOlxuXHRcdFx0XHRcdF9jdXN0b21lckdyb3VwQ2FsbEJhY2soZGF0YSk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UgJ0JVVFRPTl9PUkRFUlMnOlxuXHRcdFx0XHRcdF9vcmRlcnNDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnQlVUVE9OX0VNQUlMJzpcblx0XHRcdFx0XHRfZW1haWxDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnQlVUVE9OX0lQTE9HJzpcblx0XHRcdFx0XHRfaXBMb2dDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnTUFJTEJFRVpfT1ZFUlZJRVcnOlxuXHRcdFx0XHRcdF9tYWlsQmVlek92ZXJ2aWV3Q2FsbGJhY2soZGF0YSk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UgJ01BSUxCRUVaX05PVElGSUNBVElPTlMnOlxuXHRcdFx0XHRcdF9tYWlsQmVlek5vdGlmaWNhdGlvbnNDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnTUFJTEJFRVpfQ09OVkVSU0FUSU9OUyc6XG5cdFx0XHRcdFx0X21haWxCZWV6Q29udmVyc2F0aW9uc0NhbGxiYWNrKGRhdGEpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdCVVRUT05fTUVESUFGSU5BTlpfQ1JFRElUV09SVEhJTkVTUyc6XG5cdFx0XHRcdFx0X21lZGlhZmluYW56Q3JlZGl0d29ydGhpbmVzc0NhbGxiYWNrKGRhdGEpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdCVVRUT05fTkVXX09SREVSJzpcblx0XHRcdFx0XHRfbmV3T3JkZXJDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnQlVUVE9OX0FDQ09VTlRJTkcnOlxuXHRcdFx0XHRcdF9hZG1pblJpZ2h0c0NhbGxiYWNrKGRhdGEpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdCVVRUT05fTE9HSU5fQVNfQ1VTVE9NRVInOlxuXHRcdFx0XHRcdF9sb2dpbkFzQ3VzdG9tZXJDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnZGVsZXRlX3BlcnNvbmFsX2RhdGEnOlxuXHRcdFx0XHRcdF9kZWxldGVQZXJzb25hbERhdGFDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnZXhwb3J0X3BlcnNvbmFsX2RhdGEnOlxuXHRcdFx0XHRcdF9leHBvcnRQZXJzb25hbERhdGFDYWxsYmFjayhkYXRhKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0NhbGxiYWNrIG5vdCBmb3VuZC4nKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfZWRpdENhbGxiYWNrID0gZnVuY3Rpb24oZGF0YSkge1xuXHRcdFx0Ly8gQ29tcG9zZSB0aGUgVVJMIGFuZCBvcGVuIGl0XG5cdFx0XHR2YXIgdXJsID0gW1xuXHRcdFx0XHRzcmNQYXRoLFxuXHRcdFx0XHQnP2NJRD0nICsgZGF0YS5yb3dJZCxcblx0XHRcdFx0JyZhY3Rpb249ZWRpdCdcblx0XHRcdF0uam9pbignJyk7XG5cdFx0XHR3aW5kb3cub3Blbih1cmwsICdfc2VsZicpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9kZWxldGVDYWxsYmFjayA9IGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdC8vIENvbXBvc2UgdGhlIFVSTCBhbmQgb3BlbiBpdFxuXHRcdFx0dmFyIHVybCA9IFtcblx0XHRcdFx0c3JjUGF0aCxcblx0XHRcdFx0cXVlcnlTdHJpbmcsXG5cdFx0XHRcdCdjSUQ9JyArIGRhdGEucm93SWQsXG5cdFx0XHRcdCcmYWN0aW9uPWNvbmZpcm0nXG5cdFx0XHRdLmpvaW4oJycpO1xuXHRcdFx0d2luZG93Lm9wZW4odXJsLCAnX3NlbGYnKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfY3VzdG9tZXJHcm91cENhbGxCYWNrID0gZnVuY3Rpb24oZGF0YSkge1xuXHRcdFx0Ly8gQ29tcG9zZSB0aGUgVVJMIGFuZCBvcGVuIGl0XG5cdFx0XHR2YXIgdXJsID0gW1xuXHRcdFx0XHRzcmNQYXRoLFxuXHRcdFx0XHRxdWVyeVN0cmluZyxcblx0XHRcdFx0J2NJRD0nICsgZGF0YS5yb3dJZCxcblx0XHRcdFx0JyZhY3Rpb249ZWRpdHN0YXR1cydcblx0XHRcdF0uam9pbignJyk7XG5cdFx0XHR3aW5kb3cub3Blbih1cmwsICdfc2VsZicpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9vcmRlcnNDYWxsYmFjayA9IGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdC8vIENvbXBvc2UgdGhlIFVSTCBhbmQgb3BlbiBpdFxuXHRcdFx0dmFyIHVybCA9IFtcblx0XHRcdFx0c3JjUGF0aC5yZXBsYWNlKCdjdXN0b21lcnMucGhwJywgJ2FkbWluLnBocCcpLFxuXHRcdFx0XHQnPycgKyAkLnBhcmFtKHtcblx0XHRcdFx0XHRkbzogJ09yZGVyc092ZXJ2aWV3Jyxcblx0XHRcdFx0XHRmaWx0ZXI6IHtcblx0XHRcdFx0XHRcdGN1c3RvbWVyOiAnIycgKyBkYXRhLnJvd0lkXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KVxuXHRcdFx0XS5qb2luKCcnKTtcblx0XHRcdHdpbmRvdy5vcGVuKHVybCwgJ19zZWxmJyk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2VtYWlsQ2FsbGJhY2sgPSBmdW5jdGlvbihkYXRhKSB7XG5cdFx0XHQvLyBDb21wb3NlIHRoZSBVUkwgYW5kIG9wZW4gaXRcblx0XHRcdHZhciB1cmwgPSBbXG5cdFx0XHRcdHNyY1BhdGgucmVwbGFjZSgnY3VzdG9tZXJzLnBocCcsICdtYWlsLnBocCcpLFxuXHRcdFx0XHQnP3NlbGVjdGVkX2JveD10b29scycsXG5cdFx0XHRcdCcmY3VzdG9tZXI9JyArIGRhdGEuY3VzdEVtYWlsLFxuXHRcdFx0XS5qb2luKCcnKTtcblx0XHRcdHdpbmRvdy5vcGVuKHVybCwgJ19zZWxmJyk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2lwTG9nQ2FsbGJhY2sgPSBmdW5jdGlvbihkYXRhKSB7XG5cdFx0XHQvLyBDb21wb3NlIHRoZSBVUkwgYW5kIG9wZW4gaXRcblx0XHRcdHZhciB1cmwgPSBbXG5cdFx0XHRcdHNyY1BhdGgsXG5cdFx0XHRcdHF1ZXJ5U3RyaW5nLFxuXHRcdFx0XHQnY0lEPScgKyBkYXRhLnJvd0lkLFxuXHRcdFx0XHQnJmFjdGlvbj1pcGxvZydcblx0XHRcdF0uam9pbignJyk7XG5cdFx0XHR3aW5kb3cub3Blbih1cmwsICdfc2VsZicpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9tYWlsQmVlek92ZXJ2aWV3Q2FsbGJhY2sgPSBmdW5jdGlvbihkYXRhKSB7XG5cdFx0XHR2YXIgJHRhcmdldCA9ICQoJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuY29udGV4dF92aWV3X2J1dHRvbi5idG5fbGVmdCcpO1xuXHRcdFx0dmFyIHVybCA9ICQoJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuY29udGV4dF92aWV3X2J1dHRvbi5idG5fbGVmdCcpLmF0dHIoXG5cdFx0XHRcdCdvbmNsaWNrJyk7XG5cdFx0XHR1cmwgPSB1cmwucmVwbGFjZSgvY0lEPSguKikmLywgJ2NJRD0nICsgZGF0YS5yb3dJZCArICcmJyk7XG5cdFx0XHQkKCcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmNvbnRleHRfdmlld19idXR0b24uYnRuX2xlZnQnKS5hdHRyKCdvbmNsaWNrJywgdXJsKTtcblx0XHRcdCR0YXJnZXQuZ2V0KDApLmNsaWNrKCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX21haWxCZWV6Tm90aWZpY2F0aW9uc0NhbGxiYWNrID0gZnVuY3Rpb24oZGF0YSkge1xuXHRcdFx0dmFyICR0YXJnZXQgPSAkKCcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmNvbnRleHRfdmlld19idXR0b24uYnRuX21pZGRsZScpO1xuXHRcdFx0dmFyIHVybCA9ICQoJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuY29udGV4dF92aWV3X2J1dHRvbi5idG5fbWlkZGxlJykuYXR0cihcblx0XHRcdFx0J29uY2xpY2snKTtcblx0XHRcdHVybCA9IHVybC5yZXBsYWNlKC9jSUQ9KC4qKSYvLCAnY0lEPScgKyBkYXRhLnJvd0lkICsgJyYnKTtcblx0XHRcdCQoJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuY29udGV4dF92aWV3X2J1dHRvbi5idG5fbWlkZGxlJykuYXR0cignb25jbGljaycsXG5cdFx0XHRcdHVybCk7XG5cdFx0XHQkdGFyZ2V0LmdldCgwKS5jbGljaygpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9tYWlsQmVlekNvbnZlcnNhdGlvbnNDYWxsYmFjayA9IGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdHZhciAkdGFyZ2V0ID0gJCgnLmNvbnRlbnRUYWJsZSAuaW5mb0JveENvbnRlbnQgYS5jb250ZXh0X3ZpZXdfYnV0dG9uLmJ0bl9yaWdodCcpO1xuXHRcdFx0dmFyIHVybCA9ICQoJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuY29udGV4dF92aWV3X2J1dHRvbi5idG5fcmlnaHQnKS5hdHRyKFxuXHRcdFx0XHQnb25jbGljaycpO1xuXHRcdFx0dXJsID0gdXJsLnJlcGxhY2UoL2NJRD0oLiopJi8sICdjSUQ9JyArIGRhdGEucm93SWQgKyAnJicpO1xuXHRcdFx0JCgnLmNvbnRlbnRUYWJsZSAuaW5mb0JveENvbnRlbnQgYS5jb250ZXh0X3ZpZXdfYnV0dG9uLmJ0bl9yaWdodCcpLmF0dHIoJ29uY2xpY2snLFxuXHRcdFx0XHR1cmwpO1xuXHRcdFx0JHRhcmdldC5nZXQoMCkuY2xpY2soKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfbWVkaWFmaW5hbnpDcmVkaXR3b3J0aGluZXNzQ2FsbGJhY2sgPSBmdW5jdGlvbihkYXRhKSB7XG5cdFx0XHR2YXIgJHRhcmdldCA9ICQoJy5tZWRpYWZpbmFuei1jcmVkaXR3b3J0aGluZXNzJyk7XG5cdFx0XHR2YXIgb25jbGlja0F0dHJpYnV0ZSA9ICR0YXJnZXQuYXR0cignb25jbGljaycpO1xuXHRcdFx0Ly8gUmVwbGFjZSB0aGUgY3VzdG9tZXIgbnVtYmVyIGluIHRoZSBvbmNsaWNrIGF0dHJpYnV0ZS4gXG5cdFx0XHRvbmNsaWNrQXR0cmlidXRlID0gb25jbGlja0F0dHJpYnV0ZS5yZXBsYWNlKC9jSUQ9KC4qJywgJ3BvcHVwJykvLCAnY0lEPScgKyBkYXRhLnJvd0lkICsgJ1xcJywgXFwncG9wdXBcXCcnKTsgXG5cdFx0XHQkdGFyZ2V0LmF0dHIoJ29uY2xpY2snLCBvbmNsaWNrQXR0cmlidXRlKTtcblx0XHRcdCR0YXJnZXQudHJpZ2dlcignY2xpY2snKTsgLy8gVHJpZ2dlciB0aGUgY2xpY2sgZXZlbnQgaW4gdGhlIDxhPiBlbGVtZW50LiBcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfbmV3T3JkZXJDYWxsYmFjayA9IGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdC8vIENvbXBvc2UgdGhlIFVSTCBhbmQgb3BlbiBpdFxuXHRcdFx0dmFyIHVybCA9IFtcblx0XHRcdFx0c3JjUGF0aCxcblx0XHRcdFx0Jz9jSUQ9JyArIGRhdGEucm93SWQsXG5cdFx0XHRcdCcmYWN0aW9uPW5ld19vcmRlcidcblx0XHRcdF0uam9pbignJyk7XG5cdFx0XHR3aW5kb3cub3Blbih1cmwsICdfc2VsZicpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9hZG1pblJpZ2h0c0NhbGxiYWNrID0gZnVuY3Rpb24oZGF0YSkge1xuXHRcdFx0Ly8gQ29tcG9zZSB0aGUgVVJMIGFuZCBvcGVuIGl0XG5cdFx0XHR2YXIgdXJsID0gJ2FkbWluLnBocD9kbz1BZG1pbkFjY2Vzcy9lZGl0QWRtaW4maWQ9JyArIGRhdGEucm93SWQ7XG5cdFx0XHR3aW5kb3cub3Blbih1cmwsICdfc2VsZicpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9sb2dpbkFzQ3VzdG9tZXJDYWxsYmFjayA9IGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdC8vIENvbXBvc2UgdGhlIFVSTCBhbmQgb3BlbiBpdFxuXHRcdFx0dmFyIHVybCA9IFtcblx0XHRcdFx0c3JjUGF0aC5yZXBsYWNlKCdjdXN0b21lcnMucGhwJywgJ2FkbWluLnBocCcpLFxuXHRcdFx0XHQnP2RvPUN1c3RvbWVyTG9naW4mY3VzdG9tZXJJZD0nICsgZGF0YS5yb3dJZCArICcmcGFnZVRva2VuPScgKyBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxuXHRcdFx0XS5qb2luKCcnKTtcblx0XHRcdHdpbmRvdy5vcGVuKHVybCwgJ19zZWxmJyk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2RlbGV0ZVBlcnNvbmFsRGF0YUNhbGxiYWNrID0gZnVuY3Rpb24oZGF0YSkge1xuXHRcdFx0Ly8gQ29tcG9zZSB0aGUgVVJMIGFuZCBvcGVuIGl0XG5cdFx0XHR2YXIgdXJsID0gW1xuXHRcdFx0XHRzcmNQYXRoLFxuXHRcdFx0XHRxdWVyeVN0cmluZyxcblx0XHRcdFx0J2NJRD0nICsgZGF0YS5yb3dJZCxcblx0XHRcdFx0JyZhY3Rpb249ZGVsZXRlX3BlcnNvbmFsX2RhdGEnXG5cdFx0XHRdLmpvaW4oJycpO1xuXHRcdFx0d2luZG93Lm9wZW4odXJsLCAnX3NlbGYnKTtcblx0XHR9O1xuXG4gICAgICAgIHZhciBfZXhwb3J0UGVyc29uYWxEYXRhQ2FsbGJhY2sgPSBmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgICAgICAvLyBDb21wb3NlIHRoZSBVUkwgYW5kIG9wZW4gaXRcbiAgICAgICAgICAgIHZhciB1cmwgPSBbXG4gICAgICAgICAgICAgICAgc3JjUGF0aCxcbiAgICAgICAgICAgICAgICBxdWVyeVN0cmluZyxcbiAgICAgICAgICAgICAgICAnY0lEPScgKyBkYXRhLnJvd0lkLFxuICAgICAgICAgICAgICAgICcmYWN0aW9uPWV4cG9ydF9wZXJzb25hbF9kYXRhJ1xuICAgICAgICAgICAgXS5qb2luKCcnKTtcbiAgICAgICAgICAgIHdpbmRvdy5vcGVuKHVybCwgJ19zZWxmJyk7XG4gICAgICAgIH07XG5cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0Ly8gV2FpdCB1bnRpbCB0aGUgYnV0dG9ucyBhcmUgY29udmVydGVkIHRvIGRyb3Bkb3duIGZvciBldmVyeSByb3cuXG5cdFx0XHR2YXIgaW50ZXJ2YWwgPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKCQoJy5qcy1idXR0b24tZHJvcGRvd24nKS5sZW5ndGgpIHtcblx0XHRcdFx0XHRjbGVhckludGVydmFsKGludGVydmFsKTtcblx0XHRcdFx0XHRfbWFwUm93QWN0aW9ucygpO1xuXHRcdFx0XHR9XG5cdFx0XHR9LCA1MDApO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
