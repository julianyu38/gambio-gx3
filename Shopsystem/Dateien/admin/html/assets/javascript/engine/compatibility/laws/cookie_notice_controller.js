'use strict';

/* --------------------------------------------------------------
 cookies_notice_controller.js 2016-09-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Cookie Notice Controller
 *
 * Compatibility module that handles the "Cookie Notice" page under the "Rights" menu of "Shop Settings" section.
 * The data of the form are updated upon change and this module will just post them to LawsController. Check out
 * the fields that are language dependent, they will be changed when the user selects a language from the language
 * switcher component.
 *
 * @module Compatibility/cookie_notice_controller
 */
gx.compatibility.module('cookie_notice_controller', ['loading_spinner', gx.source + '/libs/editor_values', gx.source + '/libs/info_messages'], function (data) {

	'use strict';

	// --------------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Module Instance
  *
  * @type {object}
  */
	module = {
		model: {
			formData: jse.core.config.get('appUrl') + '/admin/admin.php?do=Laws/GetCookiePreferences&pageToken=' + jse.core.config.get('pageToken')
		}
	};

	// --------------------------------------------------------------------
	// FUNCTIONS
	// --------------------------------------------------------------------

	/**
  * On Form Submit Event
  *
  * @param {object} event jQuery Event Object
  */
	var _onFormSubmit = function _onFormSubmit(event) {
		event.preventDefault();

		// Prepare form data and send them to the LawsController class. 
		var postUrl = jse.core.config.get('appUrl') + '/admin/admin.php?do=Laws/SaveCookiePreferences',
		    postData = $.extend({ pageToken: jse.core.config.get('pageToken') }, module.model.formData),
		    $spinner;

		$.ajax({
			url: postUrl,
			type: 'POST',
			data: postData,
			dataType: 'json',
			beforeSend: function beforeSend() {
				$spinner = jse.libs.loading_spinner.show($this, '4');
			}
		}).done(function () {
			// Display success message.
			jse.libs.info_messages.addSuccess(jse.core.lang.translate('TXT_SAVE_SUCCESS', 'admin_general'));
		}).fail(function (jqxhr, textStatus, errorThrown) {
			// Display failure message.
			jse.libs.info_messages.addError(jse.core.lang.translate('TXT_SAVE_ERROR', 'admin_general'));
			jse.core.debug.error('Could not save Cookie Notice preferences.', jqxhr, textStatus, errorThrown);
		}).always(function () {
			jse.libs.loading_spinner.hide($spinner);

			// Scroll to the top, so that the user sees the appropriate message.
			$('html, body').animate({ scrollTop: 0 });
		});
	};

	/**
  * On Language Flag Click Event
  *
  * @param {object} event jQuery event object.
  */
	var _onLanguageClick = function _onLanguageClick(event) {
		event.preventDefault();

		$(this).siblings().removeClass('active');
		$(this).addClass('active');

		// Load the language specific fields.
		$.each(module.model.formData, function (name, value) {
			var $element = $this.find('[name="' + name + '"]');

			if ($element.data('multilingual') !== undefined) {
				var selectedLanguageCode = $('.languages a.active').data('code');
				$element.val(value[selectedLanguageCode]);
				if ($element.is('textarea') && $element.parents('.editor-wrapper').length) {
					jse.libs.editor_values.setValue($element, value[selectedLanguageCode]);
				}
			} else {
				$element.val(value);

				if ($element.is(':checkbox') && value === 'true') {
					$element.parent().addClass('checked');
					$element.prop('checked', true);
				}

				if (name === 'position' && !value) {
					$element.find('option[value="top"]').prop('selected', true).trigger('change');
				}
			}
		});
	};

	/**
  * On Input Element Change Event
  */
	var _onInputChange = function _onInputChange() {
		var $element = $(this);

		if ($element.data('multilingual') !== undefined) {
			var selectedLanguageCode = $('.languages a.active').data('code');
			module.model.formData[$element.attr('name')][selectedLanguageCode] = $element.val();
		} else {
			module.model.formData[$element.attr('name')] = $element.val();
		}
	};

	/**
  * On Switcher Widget Click Event
  */
	var _onSwitcherClick = function _onSwitcherClick() {
		module.model.formData[$(this).find('input:checkbox').attr('name')] = $(this).hasClass('checked');
	};

	// --------------------------------------------------------------------
	// INITIALIZATION
	// --------------------------------------------------------------------

	module.init = function (done) {
		// Bind form event handlers. 
		$this.on('submit', _onFormSubmit).on('click', '.languages a', _onLanguageClick).on('click', '.switcher', _onSwitcherClick);

		$this.find('input:hidden, input:text, select, textarea').on('change', _onInputChange);

		// Select active language.
		$('.languages').find('.active').trigger('click');

		// Set the color-preview colors.
		$this.find('.color-preview').each(function () {
			$(this).css('background-color', $(this).siblings('input:hidden').val());
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxhd3MvY29va2llX25vdGljZV9jb250cm9sbGVyLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJtb2RlbCIsImZvcm1EYXRhIiwianNlIiwiY29yZSIsImNvbmZpZyIsImdldCIsIl9vbkZvcm1TdWJtaXQiLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwicG9zdFVybCIsInBvc3REYXRhIiwiZXh0ZW5kIiwicGFnZVRva2VuIiwiJHNwaW5uZXIiLCJhamF4IiwidXJsIiwidHlwZSIsImRhdGFUeXBlIiwiYmVmb3JlU2VuZCIsImxpYnMiLCJsb2FkaW5nX3NwaW5uZXIiLCJzaG93IiwiZG9uZSIsImluZm9fbWVzc2FnZXMiLCJhZGRTdWNjZXNzIiwibGFuZyIsInRyYW5zbGF0ZSIsImZhaWwiLCJqcXhociIsInRleHRTdGF0dXMiLCJlcnJvclRocm93biIsImFkZEVycm9yIiwiZGVidWciLCJlcnJvciIsImFsd2F5cyIsImhpZGUiLCJhbmltYXRlIiwic2Nyb2xsVG9wIiwiX29uTGFuZ3VhZ2VDbGljayIsInNpYmxpbmdzIiwicmVtb3ZlQ2xhc3MiLCJhZGRDbGFzcyIsImVhY2giLCJuYW1lIiwidmFsdWUiLCIkZWxlbWVudCIsImZpbmQiLCJ1bmRlZmluZWQiLCJzZWxlY3RlZExhbmd1YWdlQ29kZSIsInZhbCIsImlzIiwicGFyZW50cyIsImxlbmd0aCIsImVkaXRvcl92YWx1ZXMiLCJzZXRWYWx1ZSIsInBhcmVudCIsInByb3AiLCJ0cmlnZ2VyIiwiX29uSW5wdXRDaGFuZ2UiLCJhdHRyIiwiX29uU3dpdGNoZXJDbGljayIsImhhc0NsYXNzIiwiaW5pdCIsIm9uIiwiY3NzIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7QUFVQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQywwQkFERCxFQUdDLENBQUMsaUJBQUQsRUFBdUJGLEdBQUdHLE1BQTFCLDBCQUEwREgsR0FBR0csTUFBN0QseUJBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUosVUFBUztBQUNSSyxTQUFPO0FBQ05DLGFBQVVDLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFDViwwREFEVSxHQUNtREgsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUZ2RDtBQURDLEVBYlY7O0FBb0JBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxLQUFJQyxnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVNDLEtBQVQsRUFBZ0I7QUFDbkNBLFFBQU1DLGNBQU47O0FBRUE7QUFDQSxNQUFJQyxVQUFVUCxJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLGdEQUE5QztBQUFBLE1BQ0NLLFdBQVdYLEVBQUVZLE1BQUYsQ0FBUyxFQUFDQyxXQUFXVixJQUFJQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFdBQXBCLENBQVosRUFBVCxFQUF3RFYsT0FBT0ssS0FBUCxDQUFhQyxRQUFyRSxDQURaO0FBQUEsTUFFQ1ksUUFGRDs7QUFJQWQsSUFBRWUsSUFBRixDQUFPO0FBQ05DLFFBQUtOLE9BREM7QUFFTk8sU0FBTSxNQUZBO0FBR05uQixTQUFNYSxRQUhBO0FBSU5PLGFBQVUsTUFKSjtBQUtOQyxlQUFZLHNCQUFXO0FBQ3RCTCxlQUFXWCxJQUFJaUIsSUFBSixDQUFTQyxlQUFULENBQXlCQyxJQUF6QixDQUE4QnZCLEtBQTlCLEVBQXFDLEdBQXJDLENBQVg7QUFDQTtBQVBLLEdBQVAsRUFTRXdCLElBVEYsQ0FTTyxZQUFXO0FBQUU7QUFDbEJwQixPQUFJaUIsSUFBSixDQUFTSSxhQUFULENBQXVCQyxVQUF2QixDQUFrQ3RCLElBQUlDLElBQUosQ0FBU3NCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQkFBeEIsRUFBNEMsZUFBNUMsQ0FBbEM7QUFDQSxHQVhGLEVBWUVDLElBWkYsQ0FZTyxVQUFTQyxLQUFULEVBQWdCQyxVQUFoQixFQUE0QkMsV0FBNUIsRUFBeUM7QUFBRTtBQUNoRDVCLE9BQUlpQixJQUFKLENBQVNJLGFBQVQsQ0FBdUJRLFFBQXZCLENBQWdDN0IsSUFBSUMsSUFBSixDQUFTc0IsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGdCQUF4QixFQUEwQyxlQUExQyxDQUFoQztBQUNBeEIsT0FBSUMsSUFBSixDQUFTNkIsS0FBVCxDQUFlQyxLQUFmLENBQXFCLDJDQUFyQixFQUFrRUwsS0FBbEUsRUFBeUVDLFVBQXpFLEVBQXFGQyxXQUFyRjtBQUNBLEdBZkYsRUFnQkVJLE1BaEJGLENBZ0JTLFlBQVc7QUFDbEJoQyxPQUFJaUIsSUFBSixDQUFTQyxlQUFULENBQXlCZSxJQUF6QixDQUE4QnRCLFFBQTlCOztBQUVBO0FBQ0FkLEtBQUUsWUFBRixFQUFnQnFDLE9BQWhCLENBQXdCLEVBQUNDLFdBQVcsQ0FBWixFQUF4QjtBQUNBLEdBckJGO0FBc0JBLEVBOUJEOztBQWdDQTs7Ozs7QUFLQSxLQUFJQyxtQkFBbUIsU0FBbkJBLGdCQUFtQixDQUFTL0IsS0FBVCxFQUFnQjtBQUN0Q0EsUUFBTUMsY0FBTjs7QUFFQVQsSUFBRSxJQUFGLEVBQVF3QyxRQUFSLEdBQW1CQyxXQUFuQixDQUErQixRQUEvQjtBQUNBekMsSUFBRSxJQUFGLEVBQVEwQyxRQUFSLENBQWlCLFFBQWpCOztBQUVBO0FBQ0ExQyxJQUFFMkMsSUFBRixDQUFPL0MsT0FBT0ssS0FBUCxDQUFhQyxRQUFwQixFQUE4QixVQUFTMEMsSUFBVCxFQUFlQyxLQUFmLEVBQXNCO0FBQ25ELE9BQUlDLFdBQVcvQyxNQUFNZ0QsSUFBTixDQUFXLFlBQVlILElBQVosR0FBbUIsSUFBOUIsQ0FBZjs7QUFFQSxPQUFJRSxTQUFTaEQsSUFBVCxDQUFjLGNBQWQsTUFBa0NrRCxTQUF0QyxFQUFpRDtBQUNoRCxRQUFJQyx1QkFBdUJqRCxFQUFFLHFCQUFGLEVBQXlCRixJQUF6QixDQUE4QixNQUE5QixDQUEzQjtBQUNBZ0QsYUFBU0ksR0FBVCxDQUFhTCxNQUFNSSxvQkFBTixDQUFiO0FBQ0EsUUFBSUgsU0FBU0ssRUFBVCxDQUFZLFVBQVosS0FBMkJMLFNBQVNNLE9BQVQsQ0FBaUIsaUJBQWpCLEVBQW9DQyxNQUFuRSxFQUEyRTtBQUMxRWxELFNBQUlpQixJQUFKLENBQVNrQyxhQUFULENBQXVCQyxRQUF2QixDQUFnQ1QsUUFBaEMsRUFBMENELE1BQU1JLG9CQUFOLENBQTFDO0FBQ0E7QUFDRCxJQU5ELE1BTU87QUFDTkgsYUFBU0ksR0FBVCxDQUFhTCxLQUFiOztBQUVBLFFBQUlDLFNBQVNLLEVBQVQsQ0FBWSxXQUFaLEtBQTRCTixVQUFVLE1BQTFDLEVBQWtEO0FBQ2pEQyxjQUFTVSxNQUFULEdBQWtCZCxRQUFsQixDQUEyQixTQUEzQjtBQUNBSSxjQUFTVyxJQUFULENBQWMsU0FBZCxFQUF5QixJQUF6QjtBQUNBOztBQUVELFFBQUliLFNBQVMsVUFBVCxJQUF1QixDQUFDQyxLQUE1QixFQUFtQztBQUNsQ0MsY0FBU0MsSUFBVCxDQUFjLHFCQUFkLEVBQXFDVSxJQUFyQyxDQUEwQyxVQUExQyxFQUFzRCxJQUF0RCxFQUE0REMsT0FBNUQsQ0FBb0UsUUFBcEU7QUFDQTtBQUNEO0FBQ0QsR0FyQkQ7QUFzQkEsRUE3QkQ7O0FBK0JBOzs7QUFHQSxLQUFJQyxpQkFBaUIsU0FBakJBLGNBQWlCLEdBQVc7QUFDL0IsTUFBSWIsV0FBVzlDLEVBQUUsSUFBRixDQUFmOztBQUVBLE1BQUk4QyxTQUFTaEQsSUFBVCxDQUFjLGNBQWQsTUFBa0NrRCxTQUF0QyxFQUFpRDtBQUNoRCxPQUFJQyx1QkFBdUJqRCxFQUFFLHFCQUFGLEVBQXlCRixJQUF6QixDQUE4QixNQUE5QixDQUEzQjtBQUNBRixVQUFPSyxLQUFQLENBQWFDLFFBQWIsQ0FBc0I0QyxTQUFTYyxJQUFULENBQWMsTUFBZCxDQUF0QixFQUE2Q1gsb0JBQTdDLElBQXFFSCxTQUFTSSxHQUFULEVBQXJFO0FBQ0EsR0FIRCxNQUdPO0FBQ050RCxVQUFPSyxLQUFQLENBQWFDLFFBQWIsQ0FBc0I0QyxTQUFTYyxJQUFULENBQWMsTUFBZCxDQUF0QixJQUErQ2QsU0FBU0ksR0FBVCxFQUEvQztBQUNBO0FBQ0QsRUFURDs7QUFXQTs7O0FBR0EsS0FBSVcsbUJBQW1CLFNBQW5CQSxnQkFBbUIsR0FBVztBQUNqQ2pFLFNBQU9LLEtBQVAsQ0FBYUMsUUFBYixDQUFzQkYsRUFBRSxJQUFGLEVBQVErQyxJQUFSLENBQWEsZ0JBQWIsRUFBK0JhLElBQS9CLENBQW9DLE1BQXBDLENBQXRCLElBQXFFNUQsRUFBRSxJQUFGLEVBQVE4RCxRQUFSLENBQWlCLFNBQWpCLENBQXJFO0FBQ0EsRUFGRDs7QUFJQTtBQUNBO0FBQ0E7O0FBRUFsRSxRQUFPbUUsSUFBUCxHQUFjLFVBQVN4QyxJQUFULEVBQWU7QUFDNUI7QUFDQXhCLFFBQ0VpRSxFQURGLENBQ0ssUUFETCxFQUNlekQsYUFEZixFQUVFeUQsRUFGRixDQUVLLE9BRkwsRUFFYyxjQUZkLEVBRThCekIsZ0JBRjlCLEVBR0V5QixFQUhGLENBR0ssT0FITCxFQUdjLFdBSGQsRUFHMkJILGdCQUgzQjs7QUFLQTlELFFBQ0VnRCxJQURGLENBQ08sNENBRFAsRUFFRWlCLEVBRkYsQ0FFSyxRQUZMLEVBRWVMLGNBRmY7O0FBSUE7QUFDQTNELElBQUUsWUFBRixFQUFnQitDLElBQWhCLENBQXFCLFNBQXJCLEVBQWdDVyxPQUFoQyxDQUF3QyxPQUF4Qzs7QUFFQTtBQUNBM0QsUUFBTWdELElBQU4sQ0FBVyxnQkFBWCxFQUE2QkosSUFBN0IsQ0FBa0MsWUFBVztBQUM1QzNDLEtBQUUsSUFBRixFQUFRaUUsR0FBUixDQUFZLGtCQUFaLEVBQWdDakUsRUFBRSxJQUFGLEVBQVF3QyxRQUFSLENBQWlCLGNBQWpCLEVBQWlDVSxHQUFqQyxFQUFoQztBQUNBLEdBRkQ7O0FBSUEzQjtBQUNBLEVBcEJEOztBQXNCQSxRQUFPM0IsTUFBUDtBQUNBLENBOUpGIiwiZmlsZSI6Imxhd3MvY29va2llX25vdGljZV9jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGNvb2tpZXNfbm90aWNlX2NvbnRyb2xsZXIuanMgMjAxNi0wOS0wOVxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiAjIyBDb29raWUgTm90aWNlIENvbnRyb2xsZXJcclxuICpcclxuICogQ29tcGF0aWJpbGl0eSBtb2R1bGUgdGhhdCBoYW5kbGVzIHRoZSBcIkNvb2tpZSBOb3RpY2VcIiBwYWdlIHVuZGVyIHRoZSBcIlJpZ2h0c1wiIG1lbnUgb2YgXCJTaG9wIFNldHRpbmdzXCIgc2VjdGlvbi5cclxuICogVGhlIGRhdGEgb2YgdGhlIGZvcm0gYXJlIHVwZGF0ZWQgdXBvbiBjaGFuZ2UgYW5kIHRoaXMgbW9kdWxlIHdpbGwganVzdCBwb3N0IHRoZW0gdG8gTGF3c0NvbnRyb2xsZXIuIENoZWNrIG91dFxyXG4gKiB0aGUgZmllbGRzIHRoYXQgYXJlIGxhbmd1YWdlIGRlcGVuZGVudCwgdGhleSB3aWxsIGJlIGNoYW5nZWQgd2hlbiB0aGUgdXNlciBzZWxlY3RzIGEgbGFuZ3VhZ2UgZnJvbSB0aGUgbGFuZ3VhZ2VcclxuICogc3dpdGNoZXIgY29tcG9uZW50LlxyXG4gKlxyXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvY29va2llX25vdGljZV9jb250cm9sbGVyXHJcbiAqL1xyXG5neC5jb21wYXRpYmlsaXR5Lm1vZHVsZShcclxuXHQnY29va2llX25vdGljZV9jb250cm9sbGVyJyxcclxuXHRcclxuXHRbJ2xvYWRpbmdfc3Bpbm5lcicsIGAke2d4LnNvdXJjZX0vbGlicy9lZGl0b3JfdmFsdWVzYCwgYCR7Z3guc291cmNlfS9saWJzL2luZm9fbWVzc2FnZXNgXSxcclxuXHRcclxuXHRmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcclxuXHRcdCd1c2Ugc3RyaWN0JztcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIFZBUklBQkxFU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0dmFyXHJcblx0XHRcdC8qKlxyXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHRcdFx0ICpcclxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cclxuXHRcdFx0ICovXHJcblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcclxuXHRcdFx0XHJcblx0XHRcdC8qKlxyXG5cdFx0XHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHRcdFx0ICpcclxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cclxuXHRcdFx0ICovXHJcblx0XHRcdG1vZHVsZSA9IHtcclxuXHRcdFx0XHRtb2RlbDoge1xyXG5cdFx0XHRcdFx0Zm9ybURhdGE6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICtcclxuXHRcdFx0XHRcdCcvYWRtaW4vYWRtaW4ucGhwP2RvPUxhd3MvR2V0Q29va2llUHJlZmVyZW5jZXMmcGFnZVRva2VuPScgKyBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIEZVTkNUSU9OU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBGb3JtIFN1Ym1pdCBFdmVudFxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgRXZlbnQgT2JqZWN0XHJcblx0XHQgKi9cclxuXHRcdHZhciBfb25Gb3JtU3VibWl0ID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIFByZXBhcmUgZm9ybSBkYXRhIGFuZCBzZW5kIHRoZW0gdG8gdGhlIExhd3NDb250cm9sbGVyIGNsYXNzLiBcclxuXHRcdFx0dmFyIHBvc3RVcmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPUxhd3MvU2F2ZUNvb2tpZVByZWZlcmVuY2VzJyxcclxuXHRcdFx0XHRwb3N0RGF0YSA9ICQuZXh0ZW5kKHtwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpfSwgbW9kdWxlLm1vZGVsLmZvcm1EYXRhKSxcclxuXHRcdFx0XHQkc3Bpbm5lcjtcclxuXHRcdFx0XHJcblx0XHRcdCQuYWpheCh7XHJcblx0XHRcdFx0dXJsOiBwb3N0VXJsLFxyXG5cdFx0XHRcdHR5cGU6ICdQT1NUJyxcclxuXHRcdFx0XHRkYXRhOiBwb3N0RGF0YSxcclxuXHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRcdGJlZm9yZVNlbmQ6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcywgJzQnKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHRcdFx0LmRvbmUoZnVuY3Rpb24oKSB7IC8vIERpc3BsYXkgc3VjY2VzcyBtZXNzYWdlLlxyXG5cdFx0XHRcdFx0anNlLmxpYnMuaW5mb19tZXNzYWdlcy5hZGRTdWNjZXNzKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUWFRfU0FWRV9TVUNDRVNTJywgJ2FkbWluX2dlbmVyYWwnKSk7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQuZmFpbChmdW5jdGlvbihqcXhociwgdGV4dFN0YXR1cywgZXJyb3JUaHJvd24pIHsgLy8gRGlzcGxheSBmYWlsdXJlIG1lc3NhZ2UuXHJcblx0XHRcdFx0XHRqc2UubGlicy5pbmZvX21lc3NhZ2VzLmFkZEVycm9yKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUWFRfU0FWRV9FUlJPUicsICdhZG1pbl9nZW5lcmFsJykpO1xyXG5cdFx0XHRcdFx0anNlLmNvcmUuZGVidWcuZXJyb3IoJ0NvdWxkIG5vdCBzYXZlIENvb2tpZSBOb3RpY2UgcHJlZmVyZW5jZXMuJywganF4aHIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKTtcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdC5hbHdheXMoZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuaGlkZSgkc3Bpbm5lcik7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdC8vIFNjcm9sbCB0byB0aGUgdG9wLCBzbyB0aGF0IHRoZSB1c2VyIHNlZXMgdGhlIGFwcHJvcHJpYXRlIG1lc3NhZ2UuXHJcblx0XHRcdFx0XHQkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7c2Nyb2xsVG9wOiAwfSk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIExhbmd1YWdlIEZsYWcgQ2xpY2sgRXZlbnRcclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdC5cclxuXHRcdCAqL1xyXG5cdFx0dmFyIF9vbkxhbmd1YWdlQ2xpY2sgPSBmdW5jdGlvbihldmVudCkge1xyXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcclxuXHRcdFx0JCh0aGlzKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBMb2FkIHRoZSBsYW5ndWFnZSBzcGVjaWZpYyBmaWVsZHMuXHJcblx0XHRcdCQuZWFjaChtb2R1bGUubW9kZWwuZm9ybURhdGEsIGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XHJcblx0XHRcdFx0dmFyICRlbGVtZW50ID0gJHRoaXMuZmluZCgnW25hbWU9XCInICsgbmFtZSArICdcIl0nKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoJGVsZW1lbnQuZGF0YSgnbXVsdGlsaW5ndWFsJykgIT09IHVuZGVmaW5lZCkge1xyXG5cdFx0XHRcdFx0dmFyIHNlbGVjdGVkTGFuZ3VhZ2VDb2RlID0gJCgnLmxhbmd1YWdlcyBhLmFjdGl2ZScpLmRhdGEoJ2NvZGUnKTtcclxuXHRcdFx0XHRcdCRlbGVtZW50LnZhbCh2YWx1ZVtzZWxlY3RlZExhbmd1YWdlQ29kZV0pO1xyXG5cdFx0XHRcdFx0aWYgKCRlbGVtZW50LmlzKCd0ZXh0YXJlYScpICYmICRlbGVtZW50LnBhcmVudHMoJy5lZGl0b3Itd3JhcHBlcicpLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHRqc2UubGlicy5lZGl0b3JfdmFsdWVzLnNldFZhbHVlKCRlbGVtZW50LCB2YWx1ZVtzZWxlY3RlZExhbmd1YWdlQ29kZV0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHQkZWxlbWVudC52YWwodmFsdWUpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRpZiAoJGVsZW1lbnQuaXMoJzpjaGVja2JveCcpICYmIHZhbHVlID09PSAndHJ1ZScpIHtcclxuXHRcdFx0XHRcdFx0JGVsZW1lbnQucGFyZW50KCkuYWRkQ2xhc3MoJ2NoZWNrZWQnKTtcclxuXHRcdFx0XHRcdFx0JGVsZW1lbnQucHJvcCgnY2hlY2tlZCcsIHRydWUpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRpZiAobmFtZSA9PT0gJ3Bvc2l0aW9uJyAmJiAhdmFsdWUpIHtcclxuXHRcdFx0XHRcdFx0JGVsZW1lbnQuZmluZCgnb3B0aW9uW3ZhbHVlPVwidG9wXCJdJykucHJvcCgnc2VsZWN0ZWQnLCB0cnVlKS50cmlnZ2VyKCdjaGFuZ2UnKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBJbnB1dCBFbGVtZW50IENoYW5nZSBFdmVudFxyXG5cdFx0ICovXHJcblx0XHR2YXIgX29uSW5wdXRDaGFuZ2UgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyICRlbGVtZW50ID0gJCh0aGlzKTtcclxuXHRcdFx0XHJcblx0XHRcdGlmICgkZWxlbWVudC5kYXRhKCdtdWx0aWxpbmd1YWwnKSAhPT0gdW5kZWZpbmVkKSB7XHJcblx0XHRcdFx0dmFyIHNlbGVjdGVkTGFuZ3VhZ2VDb2RlID0gJCgnLmxhbmd1YWdlcyBhLmFjdGl2ZScpLmRhdGEoJ2NvZGUnKTtcclxuXHRcdFx0XHRtb2R1bGUubW9kZWwuZm9ybURhdGFbJGVsZW1lbnQuYXR0cignbmFtZScpXVtzZWxlY3RlZExhbmd1YWdlQ29kZV0gPSAkZWxlbWVudC52YWwoKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRtb2R1bGUubW9kZWwuZm9ybURhdGFbJGVsZW1lbnQuYXR0cignbmFtZScpXSA9ICRlbGVtZW50LnZhbCgpO1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIFN3aXRjaGVyIFdpZGdldCBDbGljayBFdmVudFxyXG5cdFx0ICovXHJcblx0XHR2YXIgX29uU3dpdGNoZXJDbGljayA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRtb2R1bGUubW9kZWwuZm9ybURhdGFbJCh0aGlzKS5maW5kKCdpbnB1dDpjaGVja2JveCcpLmF0dHIoJ25hbWUnKV0gPSAkKHRoaXMpLmhhc0NsYXNzKCdjaGVja2VkJyk7XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0XHQvLyBCaW5kIGZvcm0gZXZlbnQgaGFuZGxlcnMuIFxyXG5cdFx0XHQkdGhpc1xyXG5cdFx0XHRcdC5vbignc3VibWl0JywgX29uRm9ybVN1Ym1pdClcclxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5sYW5ndWFnZXMgYScsIF9vbkxhbmd1YWdlQ2xpY2spXHJcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuc3dpdGNoZXInLCBfb25Td2l0Y2hlckNsaWNrKTtcclxuXHRcdFx0XHJcblx0XHRcdCR0aGlzXHJcblx0XHRcdFx0LmZpbmQoJ2lucHV0OmhpZGRlbiwgaW5wdXQ6dGV4dCwgc2VsZWN0LCB0ZXh0YXJlYScpXHJcblx0XHRcdFx0Lm9uKCdjaGFuZ2UnLCBfb25JbnB1dENoYW5nZSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdC8vIFNlbGVjdCBhY3RpdmUgbGFuZ3VhZ2UuXHJcblx0XHRcdCQoJy5sYW5ndWFnZXMnKS5maW5kKCcuYWN0aXZlJykudHJpZ2dlcignY2xpY2snKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIFNldCB0aGUgY29sb3ItcHJldmlldyBjb2xvcnMuXHJcblx0XHRcdCR0aGlzLmZpbmQoJy5jb2xvci1wcmV2aWV3JykuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHQkKHRoaXMpLmNzcygnYmFja2dyb3VuZC1jb2xvcicsICQodGhpcykuc2libGluZ3MoJ2lucHV0OmhpZGRlbicpLnZhbCgpKTtcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHRkb25lKCk7XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gbW9kdWxlO1xyXG5cdH0pOyJdfQ==
