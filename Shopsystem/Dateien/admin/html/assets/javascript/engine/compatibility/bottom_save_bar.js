'use strict';

/* --------------------------------------------------------------
 bottom_save_bar.js 2017-09-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Bottom Save Bar Module
 *
 * This module will move all content, that is contained in any element that has the class "bottom-save-bar-content" to
 * the Bottom Save Bar.
 *
 * @module Compatibility/bottom_save_bar
 */
gx.compatibility.module(
// Module name
'bottom_save_bar',

// Module dependencies
[],

/**  @lends module:Compatibility/bottom_save_bar */

function () {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var module = {},
	    $target = $('.bottom-save-bar');

	// ------------------------------------------------------------------------
	// EVENT HANDLER
	// ------------------------------------------------------------------------

	var _initialize = function _initialize() {
		$('.bottom-save-bar-content > *:not([type="hidden"]):not(.btn-primary)').each(function (index, element) {
			var $element = $(element).css('float', 'none');
			_handleContentType($element);
		});

		$('.bottom-save-bar-content > *.btn-primary:not([type="hidden"])').each(function (index, element) {
			var $element = $(element).css('float', 'none');
			_handleContentType($element);
		});
	};

	var _handleContentType = function _handleContentType($element) {
		if ($element.is('input:not([type="button"])') || $element.is('button[type="submit"]')) {
			_handleFormElement($element);
		} else {
			_handleOtherContent($element);
		}
	};

	var _handleFormElement = function _handleFormElement($element) {
		var $clone = $element.clone();
		$element.hide();

		if ($element.is('[type="submit"]')) {
			$clone.attr('type', 'button');
		}

		$clone.on('click', function () {
			$element.trigger('click');
		});

		$target.append($clone);
	};

	var _handleOtherContent = function _handleOtherContent($element) {
		$target.append($element);
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		_initialize();
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvdHRvbV9zYXZlX2Jhci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCIkdGFyZ2V0IiwiJCIsIl9pbml0aWFsaXplIiwiZWFjaCIsImluZGV4IiwiZWxlbWVudCIsIiRlbGVtZW50IiwiY3NzIiwiX2hhbmRsZUNvbnRlbnRUeXBlIiwiaXMiLCJfaGFuZGxlRm9ybUVsZW1lbnQiLCJfaGFuZGxlT3RoZXJDb250ZW50IiwiJGNsb25lIiwiY2xvbmUiLCJoaWRlIiwiYXR0ciIsIm9uIiwidHJpZ2dlciIsImFwcGVuZCIsImluaXQiLCJkb25lIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7O0FBUUFBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCO0FBQ0M7QUFDQSxpQkFGRDs7QUFJQztBQUNBLEVBTEQ7O0FBT0M7O0FBRUEsWUFBVzs7QUFFVjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsS0FBTUEsU0FBUyxFQUFmO0FBQUEsS0FDQ0MsVUFBVUMsRUFBRSxrQkFBRixDQURYOztBQUdBO0FBQ0E7QUFDQTs7QUFFQSxLQUFNQyxjQUFjLFNBQWRBLFdBQWMsR0FBVztBQUM5QkQsSUFBRSxxRUFBRixFQUF5RUUsSUFBekUsQ0FBOEUsVUFBU0MsS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUI7QUFDdEcsT0FBTUMsV0FBV0wsRUFBRUksT0FBRixFQUFXRSxHQUFYLENBQWUsT0FBZixFQUF3QixNQUF4QixDQUFqQjtBQUNBQyxzQkFBbUJGLFFBQW5CO0FBQ0EsR0FIRDs7QUFLQUwsSUFBRSwrREFBRixFQUFtRUUsSUFBbkUsQ0FBd0UsVUFBU0MsS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUI7QUFDaEcsT0FBTUMsV0FBV0wsRUFBRUksT0FBRixFQUFXRSxHQUFYLENBQWUsT0FBZixFQUF3QixNQUF4QixDQUFqQjtBQUNBQyxzQkFBbUJGLFFBQW5CO0FBQ0EsR0FIRDtBQUlBLEVBVkQ7O0FBWUEsS0FBTUUscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBU0YsUUFBVCxFQUFtQjtBQUM3QyxNQUFJQSxTQUFTRyxFQUFULENBQVksNEJBQVosS0FBNkNILFNBQVNHLEVBQVQsQ0FBWSx1QkFBWixDQUFqRCxFQUF1RjtBQUN0RkMsc0JBQW1CSixRQUFuQjtBQUNBLEdBRkQsTUFHSztBQUNKSyx1QkFBb0JMLFFBQXBCO0FBQ0E7QUFDRCxFQVBEOztBQVNBLEtBQU1JLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQVNKLFFBQVQsRUFBbUI7QUFDN0MsTUFBSU0sU0FBU04sU0FBU08sS0FBVCxFQUFiO0FBQ0FQLFdBQVNRLElBQVQ7O0FBRUEsTUFBSVIsU0FBU0csRUFBVCxDQUFZLGlCQUFaLENBQUosRUFBb0M7QUFDbkNHLFVBQU9HLElBQVAsQ0FBWSxNQUFaLEVBQW9CLFFBQXBCO0FBQ0E7O0FBRURILFNBQU9JLEVBQVAsQ0FBVSxPQUFWLEVBQW1CLFlBQVc7QUFDN0JWLFlBQVNXLE9BQVQsQ0FBaUIsT0FBakI7QUFDQSxHQUZEOztBQUlBakIsVUFBUWtCLE1BQVIsQ0FBZU4sTUFBZjtBQUNBLEVBYkQ7O0FBZUEsS0FBTUQsc0JBQXNCLFNBQXRCQSxtQkFBc0IsQ0FBU0wsUUFBVCxFQUFtQjtBQUM5Q04sVUFBUWtCLE1BQVIsQ0FBZVosUUFBZjtBQUNBLEVBRkQ7O0FBSUE7QUFDQTtBQUNBOztBQUVBUCxRQUFPb0IsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QmxCO0FBQ0FrQjtBQUNBLEVBSEQ7O0FBS0EsUUFBT3JCLE1BQVA7QUFDQSxDQTFFRiIsImZpbGUiOiJib3R0b21fc2F2ZV9iYXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gYm90dG9tX3NhdmVfYmFyLmpzIDIwMTctMDktMDZcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogIyMgQm90dG9tIFNhdmUgQmFyIE1vZHVsZVxyXG4gKlxyXG4gKiBUaGlzIG1vZHVsZSB3aWxsIG1vdmUgYWxsIGNvbnRlbnQsIHRoYXQgaXMgY29udGFpbmVkIGluIGFueSBlbGVtZW50IHRoYXQgaGFzIHRoZSBjbGFzcyBcImJvdHRvbS1zYXZlLWJhci1jb250ZW50XCIgdG9cclxuICogdGhlIEJvdHRvbSBTYXZlIEJhci5cclxuICpcclxuICogQG1vZHVsZSBDb21wYXRpYmlsaXR5L2JvdHRvbV9zYXZlX2JhclxyXG4gKi9cclxuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXHJcblx0Ly8gTW9kdWxlIG5hbWVcclxuXHQnYm90dG9tX3NhdmVfYmFyJyxcclxuXHRcclxuXHQvLyBNb2R1bGUgZGVwZW5kZW5jaWVzXHJcblx0W10sXHJcblx0XHJcblx0LyoqICBAbGVuZHMgbW9kdWxlOkNvbXBhdGliaWxpdHkvYm90dG9tX3NhdmVfYmFyICovXHJcblx0XHJcblx0ZnVuY3Rpb24oKSB7XHJcblx0XHRcclxuXHRcdCd1c2Ugc3RyaWN0JztcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdGNvbnN0IG1vZHVsZSA9IHt9LFxyXG5cdFx0XHQkdGFyZ2V0ID0gJCgnLmJvdHRvbS1zYXZlLWJhcicpO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIEVWRU5UIEhBTkRMRVJcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHRjb25zdCBfaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHQkKCcuYm90dG9tLXNhdmUtYmFyLWNvbnRlbnQgPiAqOm5vdChbdHlwZT1cImhpZGRlblwiXSk6bm90KC5idG4tcHJpbWFyeSknKS5lYWNoKGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KSB7XHJcblx0XHRcdFx0Y29uc3QgJGVsZW1lbnQgPSAkKGVsZW1lbnQpLmNzcygnZmxvYXQnLCAnbm9uZScpO1xyXG5cdFx0XHRcdF9oYW5kbGVDb250ZW50VHlwZSgkZWxlbWVudCk7XHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0JCgnLmJvdHRvbS1zYXZlLWJhci1jb250ZW50ID4gKi5idG4tcHJpbWFyeTpub3QoW3R5cGU9XCJoaWRkZW5cIl0pJykuZWFjaChmdW5jdGlvbihpbmRleCwgZWxlbWVudCkge1xyXG5cdFx0XHRcdGNvbnN0ICRlbGVtZW50ID0gJChlbGVtZW50KS5jc3MoJ2Zsb2F0JywgJ25vbmUnKTtcclxuXHRcdFx0XHRfaGFuZGxlQ29udGVudFR5cGUoJGVsZW1lbnQpO1xyXG5cdFx0XHR9KTtcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdGNvbnN0IF9oYW5kbGVDb250ZW50VHlwZSA9IGZ1bmN0aW9uKCRlbGVtZW50KSB7XHJcblx0XHRcdGlmICgkZWxlbWVudC5pcygnaW5wdXQ6bm90KFt0eXBlPVwiYnV0dG9uXCJdKScpIHx8ICRlbGVtZW50LmlzKCdidXR0b25bdHlwZT1cInN1Ym1pdFwiXScpKSB7XHJcblx0XHRcdFx0X2hhbmRsZUZvcm1FbGVtZW50KCRlbGVtZW50KTtcclxuXHRcdFx0fVxyXG5cdFx0XHRlbHNlIHtcclxuXHRcdFx0XHRfaGFuZGxlT3RoZXJDb250ZW50KCRlbGVtZW50KTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgX2hhbmRsZUZvcm1FbGVtZW50ID0gZnVuY3Rpb24oJGVsZW1lbnQpIHtcclxuXHRcdFx0bGV0ICRjbG9uZSA9ICRlbGVtZW50LmNsb25lKCk7XHJcblx0XHRcdCRlbGVtZW50LmhpZGUoKTtcclxuXHRcdFx0XHJcblx0XHRcdGlmICgkZWxlbWVudC5pcygnW3R5cGU9XCJzdWJtaXRcIl0nKSkge1xyXG5cdFx0XHRcdCRjbG9uZS5hdHRyKCd0eXBlJywgJ2J1dHRvbicpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHQkY2xvbmUub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0JGVsZW1lbnQudHJpZ2dlcignY2xpY2snKTtcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQkdGFyZ2V0LmFwcGVuZCgkY2xvbmUpO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgX2hhbmRsZU90aGVyQ29udGVudCA9IGZ1bmN0aW9uKCRlbGVtZW50KSB7XHJcblx0XHRcdCR0YXJnZXQuYXBwZW5kKCRlbGVtZW50KTtcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdFx0X2luaXRpYWxpemUoKTtcclxuXHRcdFx0ZG9uZSgpO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0cmV0dXJuIG1vZHVsZTtcclxuXHR9KTtcclxuIl19
