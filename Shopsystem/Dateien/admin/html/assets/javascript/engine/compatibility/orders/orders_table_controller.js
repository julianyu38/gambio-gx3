'use strict';

/* --------------------------------------------------------------
 orders_table_controller.js 2016-10-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Orders Table Controller
 *
 * This controller contains the mapping logic of the orders table.
 *
 * @module Compatibility/orders_table_controller
 */
gx.compatibility.module('orders_table_controller', [gx.source + '/libs/action_mapper', gx.source + '/libs/button_dropdown'],

/**  @lends module:Compatibility/orders_table_controller */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Array of mapped buttons
  *
  * @var Array
  */
	mappedButtons = [],


	/**
  * The mapper library
  *
  * @var {object}
  */
	mapper = jse.libs.action_mapper,


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Disable/Enable the buttons on the bottom button-dropdown
  * dependent on the checkboxes selection
  * @private
  */
	var _toggleMultiActionButton = function _toggleMultiActionButton() {
		var $checked = $('tr[data-row-id] input[type="checkbox"]:checked');
		$('.js-bottom-dropdown button').prop('disabled', !$checked.length);
	};

	/**
  * Map actions for every row in the table.
  *
  * This method will map the actions for each
  * row of the table.
  *
  * @private
  */
	var _mapRowAction = function _mapRowAction($that) {
		/**
   * Reference to the row action dropdown
   * @var {object | jQuery}
   */
		var $dropdown = $that.find('.js-button-dropdown');

		if ($dropdown.length) {
			_mapRowButtonDropdown($dropdown);
		}
	};

	var _mapRowButtonDropdown = function _mapRowButtonDropdown($dropdown) {
		var actions = ['TEXT_SHOW', 'TEXT_GM_STATUS', 'delete', 'BUTTON_GM_CANCEL', 'BUTTON_CREATE_INVOICE', 'TITLE_INVOICE_MAIL', 'BUTTON_CREATE_PACKING_SLIP', 'TITLE_ORDER', 'TITLE_RECREATE_ORDER', 'TITLE_SEND_ORDER', 'TEXT_CREATE_WITHDRAWAL', 'TXT_PARCEL_TRACKING_SENDBUTTON_TITLE', 'BUTTON_DHL_LABEL', 'MAILBEEZ_OVERVIEW', 'MAILBEEZ_NOTIFICATIONS', 'MAILBEEZ_CONVERSATIONS', 'BUTTON_HERMES'];

		for (var index in actions) {
			_bindEventHandler($dropdown, actions[index], '.single-order-dropdown');
		}
	};

	/**
  * Defines the language section for each text tile
  *
  * @type {object}
  * @private
  */
	var _sectionMapping = {
		'TEXT_SHOW': 'orders',
		'TEXT_GM_STATUS': 'orders',
		'delete': 'buttons',
		'BUTTON_GM_CANCEL': 'orders',
		'BUTTON_CREATE_INVOICE': 'orders',
		'TITLE_INVOICE_MAIL': 'orders',
		'BUTTON_CREATE_PACKING_SLIP': 'orders',
		'TITLE_ORDER': 'orders',
		'TITLE_RECREATE_ORDER': 'orders',
		'TITLE_SEND_ORDER': 'orders',
		'TEXT_CREATE_WITHDRAWAL': 'orders',
		'TXT_PARCEL_TRACKING_SENDBUTTON_TITLE': 'parcel_services',
		'BUTTON_DHL_LABEL': 'orders',
		'MAILBEEZ_OVERVIEW': 'orders',
		'MAILBEEZ_NOTIFICATIONS': 'orders',
		'MAILBEEZ_CONVERSATIONS': 'orders',
		'BUTTON_MULTI_CANCEL': 'orders',
		'BUTTON_MULTI_CHANGE_ORDER_STATUS': 'orders',
		'BUTTON_MULTI_DELETE': 'orders',
		'BUTTON_HERMES': 'orders',
		'get_labels': 'iloxx'
	};

	/**
  * Defines target selectors
  *
  * @type {object}
  * @private
  */
	var _selectorMapping = {
		'TEXT_SHOW': '.contentTable .infoBoxContent a.btn-details',
		'TEXT_GM_STATUS': '.contentTable .infoBoxContent a.btn-update_order_status',
		'delete': '.contentTable .infoBoxContent a.btn-delete',
		'BUTTON_GM_CANCEL': '.contentTable .infoBoxContent .GM_CANCEL',
		'BUTTON_CREATE_INVOICE': '.contentTable .infoBoxContent a.btn-invoice',
		'TITLE_INVOICE_MAIL': '.contentTable .infoBoxContent .GM_INVOICE_MAIL',
		'BUTTON_CREATE_PACKING_SLIP': '.contentTable .infoBoxContent a.btn-packing_slip',
		'TITLE_ORDER': '.contentTable .infoBoxContent a.btn-order_confirmation',
		'TITLE_RECREATE_ORDER': '.contentTable .infoBoxContent a.btn-recreate_order_confirmation',
		'TITLE_SEND_ORDER': '.contentTable .infoBoxContent .GM_SEND_ORDER',
		'TEXT_CREATE_WITHDRAWAL': '.contentTable .infoBoxContent a.btn-create_withdrawal',
		'TXT_PARCEL_TRACKING_SENDBUTTON_TITLE': '.contentTable .infoBoxContent a.btn-add_tracking_code',
		'BUTTON_DHL_LABEL': '.contentTable .infoBoxContent a.btn-dhl_label',
		'MAILBEEZ_OVERVIEW': '.contentTable .infoBoxContent a.context_view_button.btn_left',
		'MAILBEEZ_NOTIFICATIONS': '.contentTable .infoBoxContent a.context_view_button.btn_middle',
		'MAILBEEZ_CONVERSATIONS': '.contentTable .infoBoxContent a.context_view_button.btn_right',
		'BUTTON_MULTI_CANCEL': '.contentTable .infoBoxContent a.btn-multi_cancel',
		'BUTTON_MULTI_CHANGE_ORDER_STATUS': '.contentTable .infoBoxContent a.btn-update_order_status',
		'BUTTON_MULTI_DELETE': '.contentTable .infoBoxContent a.btn-multi_delete',
		'BUTTON_HERMES': '.contentTable .infoBoxContent a.btn-hermes',
		'get_labels': '#iloxx_orders'
	};

	var _getActionCallback = function _getActionCallback(action) {
		switch (action) {
			case 'TEXT_SHOW':
				return _showOrderCallback;
			case 'TEXT_GM_STATUS':
				return _changeOrderStatusCallback;
			case 'delete':
				return _deleteCallback;
			case 'BUTTON_GM_CANCEL':
				return _cancelCallback;
			case 'BUTTON_CREATE_INVOICE':
				return _invoiceCallback;
			case 'TITLE_INVOICE_MAIL':
				return _emailInvoiceCallback;
			case 'BUTTON_CREATE_PACKING_SLIP':
				return _packingSlipCallback;
			case 'TITLE_ORDER':
				return _orderConfirmationCallback;
			case 'TITLE_RECREATE_ORDER':
				return _recreateOrderConfirmationCallback;
			case 'TITLE_SEND_ORDER':
				return _sendOrderConfirmationCallback;
			case 'TEXT_CREATE_WITHDRAWAL':
				return _withdrawalCallback;
			case 'TXT_PARCEL_TRACKING_SENDBUTTON_TITLE':
				return _addTrackingCodeCallback;
			case 'BUTTON_DHL_LABEL':
				return _dhlLabelCallback;
			case 'MAILBEEZ_OVERVIEW':
				return _mailBeezOverviewCallback;
			case 'MAILBEEZ_NOTIFICATIONS':
				return _mailBeezNotificationsCallback;
			case 'MAILBEEZ_CONVERSATIONS':
				return _mailBeezConversationsCallback;
			case 'BUTTON_MULTI_CANCEL':
				return _multiCancelCallback;
			case 'BUTTON_MULTI_CHANGE_ORDER_STATUS':
				return _multiChangeOrderStatusCallback;
			case 'BUTTON_MULTI_DELETE':
				return _multiDeleteCallback;
			case 'BUTTON_HERMES':
				return _hermesCallback;
			case 'get_labels':
				return _iloxxCallback;
		}
	};

	var _showOrderCallback = function _showOrderCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var url = $(_selectorMapping.TEXT_SHOW).attr('href');
		window.open(url.replace(/oID=(.*)&/, 'oID=' + orderId + '&'), '_self');
	};

	var _changeOrderStatusCallback = function _changeOrderStatusCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		$('#gm_order_id').val(orderId);
		$('.gx-orders-table .single-checkbox').removeClass('checked');
		$('.gx-orders-table input:checkbox').prop('checked', false);
		$(event.target).parents('tr').eq(0).find('.single-checkbox').addClass('checked');
		$(event.target).parents('tr').eq(0).find('input:checkbox').prop('checked', true);
		$(_selectorMapping.TEXT_GM_STATUS).click();
	};

	var _deleteCallback = function _deleteCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var $delete = $(_selectorMapping.delete);
		$delete.data('order_id', orderId);
		$delete.get(0).click();
	};

	var _cancelCallback = function _cancelCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		$('#gm_order_id').val(orderId);
		$('.gx-orders-table .single-checkbox').removeClass('checked');
		$('.gx-orders-table input:checkbox').prop('checked', false);
		$(event.target).parents('tr').eq(0).find('.single-checkbox').addClass('checked');
		$(event.target).parents('tr').eq(0).find('input:checkbox').prop('checked', true);
		$(_selectorMapping.BUTTON_MULTI_CANCEL).click();
	};

	var _invoiceCallback = function _invoiceCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var url = $(_selectorMapping.BUTTON_CREATE_INVOICE).attr('href');
		window.open(url.replace(/oID=(.*)&/, 'oID=' + orderId + '&'), '_blank');
	};

	var _emailInvoiceCallback = function _emailInvoiceCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		$('#gm_order_id').val(orderId);
		$('.GM_INVOICE_MAIL').click();
	};

	var _packingSlipCallback = function _packingSlipCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var url = $(_selectorMapping.BUTTON_CREATE_PACKING_SLIP).attr('href');
		window.open(url.replace(/oID=(.*)&/, 'oID=' + orderId + '&'), '_blank');
	};

	var _orderConfirmationCallback = function _orderConfirmationCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var url = $(_selectorMapping.TITLE_ORDER).attr('href');
		window.open(url.replace(/oID=(.*)&/, 'oID=' + orderId + '&'), '_blank');
	};

	var _recreateOrderConfirmationCallback = function _recreateOrderConfirmationCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var url = $(_selectorMapping.TITLE_RECREATE_ORDER).attr('href');
		window.open(url.replace(/oID=(.*)&/, 'oID=' + orderId + '&'), '_blank');
	};

	var _sendOrderConfirmationCallback = function _sendOrderConfirmationCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		$('#gm_order_id').val(orderId);
		$('.GM_SEND_ORDER').click();
	};

	var _withdrawalCallback = function _withdrawalCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var url = $(_selectorMapping.TEXT_CREATE_WITHDRAWAL).attr('href');
		window.open(url.replace(/order=[^&]*/, 'order_id=' + orderId), '_blank');
	};

	var _addTrackingCodeCallback = function _addTrackingCodeCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var $target = $(_selectorMapping.TXT_PARCEL_TRACKING_SENDBUTTON_TITLE);
		$target.data('order_id', orderId);
		$target.get(0).click();
	};

	var _dhlLabelCallback = function _dhlLabelCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var url = $(_selectorMapping.BUTTON_DHL_LABEL).attr('href');
		window.open(url.replace(/oID=(.*)/, 'oID=' + orderId), '_blank');
	};

	var _mailBeezOverviewCallback = function _mailBeezOverviewCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var $target = $(_selectorMapping.MAILBEEZ_OVERVIEW);
		var url = $target.attr('onclick');
		url = url.replace(/oID=(.*)&/, 'oID=' + orderId + '&');
		$target.attr('onclick', url);
		$target.get(0).click();
	};

	var _mailBeezNotificationsCallback = function _mailBeezNotificationsCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var $target = $(_selectorMapping.MAILBEEZ_NOTIFICATIONS);
		var url = $target.attr('onclick');
		url = url.replace(/oID=(.*)&/, 'oID=' + orderId + '&');
		$target.attr('onclick', url);
		$target.get(0).click();
	};

	var _mailBeezConversationsCallback = function _mailBeezConversationsCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var $target = $(_selectorMapping.MAILBEEZ_CONVERSATIONS);
		var url = $target.attr('onclick');
		url = url.replace(/oID=(.*)&/, 'oID=' + orderId + '&');
		$target.attr('onclick', url);
		$target.get(0).click();
	};

	var _hermesCallback = function _hermesCallback(event) {
		var orderId = $(event.target).parents('tr').data('row-id');
		var $target = $(_selectorMapping.BUTTON_HERMES);
		var url = $target.attr('href');
		url = url.replace(/orders_id=(.*)/, 'orders_id=' + orderId);
		$target.attr('href', url);
		$target.get(0).click();
	};

	var _iloxxCallback = function _iloxxCallback(event) {
		var $target = $(_selectorMapping.get_labels);
		$target.click();
	};

	var _multiChangeOrderStatusCallback = function _multiChangeOrderStatusCallback(event) {
		$(_selectorMapping.BUTTON_MULTI_CHANGE_ORDER_STATUS).get(0).click();
	};

	var _multiDeleteCallback = function _multiDeleteCallback(event) {
		$(_selectorMapping.BUTTON_MULTI_DELETE).get(0).click();
	};

	var _multiCancelCallback = function _multiCancelCallback(event) {
		$(_selectorMapping.BUTTON_MULTI_CANCEL).get(0).click();
	};

	/**
  * Map table actions to bottom dropdown button.
  *
  * @private
  */
	var _mapTableActions = function _mapTableActions() {
		var $dropdown = $('#orders-table-dropdown');

		_bindEventHandler($dropdown, 'BUTTON_MULTI_CHANGE_ORDER_STATUS');

		if ($(_selectorMapping.get_labels).length) {
			_bindEventHandler($dropdown, 'get_labels');
		}

		_bindEventHandler($dropdown, 'BUTTON_MULTI_DELETE');
		_bindEventHandler($dropdown, 'BUTTON_MULTI_CANCEL');
	};

	/**
  * Map actions for every row in the table generically.
  *
  * This method will use the action_mapper library to map the actions for each
  * row of the table. It maps only those buttons, that haven't already explicitly
  * mapped by the _mapRowActions function.
  *
  * @private
  */
	var _mapUnmappedRowActions = function _mapUnmappedRowActions($this) {
		var unmappedRowActions = [];
		$('.action_buttons .extended_single_actions a,' + '.action_buttons .extended_single_actions button,' + '.action_buttons .extended_single_actions input[type="button"],' + '.action_buttons .extended_single_actions input[type="submit"]').each(function () {
			if (!_alreadyMapped($(this))) {
				unmappedRowActions.push($(this));
			}
		});

		var orderId = $this.data('row-id'),
		    $dropdown = $this.find('.js-button-dropdown');

		$.each(unmappedRowActions, function () {
			var $button = $(this);
			var callback = function callback() {
				if ($button.prop('href') !== undefined) {
					$button.prop('href', $button.prop('href').replace(/oID=(.*)\d(?=&)?/, 'oID=' + orderId));
				}
				$button.get(0).click();
			};

			jse.libs.button_dropdown.mapAction($dropdown, $button.text(), '', callback);
			mappedButtons.push($button);
		});
	};

	var _mapUnmappedMultiActions = function _mapUnmappedMultiActions() {
		var unmappedMultiActions = [];
		$('.action_buttons .extended_multi_actions a,' + '.action_buttons .extended_multi_actions button,' + '.action_buttons .extended_multi_actions input[type="button"],' + '.action_buttons .extended_multi_actions input[type="submit"]').each(function () {
			if (!_alreadyMapped($(this))) {
				unmappedMultiActions.push($(this));
			}
		});

		var $dropdown = $('#orders-table-dropdown');
		$.each(unmappedMultiActions, function () {
			var $button = $(this);
			var callback = function callback() {
				$button.get(0).click();
			};

			jse.libs.button_dropdown.mapAction($dropdown, $button.text(), '', callback);
			mappedButtons.push($button);
		});
	};

	/**
  * Checks if the button was already mapped
  *
  * @private
  */
	var _alreadyMapped = function _alreadyMapped($button) {
		for (var index in mappedButtons) {
			if ($button.is(mappedButtons[index])) {
				return true;
			}
		}
		return false;
	};

	/**
  * Add Button to Mapped Array
  *
  * @param buttonSelector
  * @returns {boolean}
  *
  * @private
  */
	var _addButtonToMappedArray = function _addButtonToMappedArray(buttonSelector) {
		if (mappedButtons[buttonSelector] !== undefined) {
			return true;
		}
		mappedButtons[buttonSelector] = $(buttonSelector);
	};

	/**
  * Bind Event handler
  *
  * @param $dropdown
  * @param action
  * @param customRecentButtonSelector
  *
  * @private
  */
	var _bindEventHandler = function _bindEventHandler($dropdown, action, customRecentButtonSelector) {
		var targetSelector = _selectorMapping[action],
		    section = _sectionMapping[action],
		    callback = _getActionCallback(action),
		    customElement = $(customRecentButtonSelector).length ? $(customRecentButtonSelector) : $dropdown;
		if ($(targetSelector).length) {
			_addButtonToMappedArray(targetSelector);
			jse.libs.button_dropdown.mapAction($dropdown, action, section, callback, customElement);
		}
	};

	/**
  * Fix for row selection controls.
  *
  * @private
  */
	var _fixRowSelectionForControlElements = function _fixRowSelectionForControlElements() {
		$('input.checkbox[name="gm_multi_status[]"]').add('.single-checkbox').add('a.action-icon').add('.js-button-dropdown').add('tr.dataTableRow a').on('click', function (event) {
			event.stopPropagation();
			_toggleMultiActionButton();
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Wait until the buttons are converted to dropdown for every row.
		var interval = setInterval(function () {
			if ($('.js-button-dropdown').length) {
				clearInterval(interval);

				_mapTableActions();
				_mapUnmappedMultiActions();

				var tableActions = mappedButtons;

				// Remove Mailbeez conversations badge.
				_addButtonToMappedArray('.contentTable .infoBoxContent a.context_view_button.btn_right');

				$('.gx-orders-table tr').not('.dataTableHeadingRow').each(function () {
					mappedButtons = [];

					for (var index in tableActions) {
						mappedButtons[index] = tableActions[index];
					}

					_mapRowAction($(this));
					_mapUnmappedRowActions($(this));
				});

				_fixRowSelectionForControlElements();

				// Initialize checkboxes
				_toggleMultiActionButton();
			}
		}, 300);

		// Check for selected checkboxes also
		// before all rows and their dropdown widgets have been initialized.
		_toggleMultiActionButton();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vcmRlcnNfdGFibGVfY29udHJvbGxlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwibWFwcGVkQnV0dG9ucyIsIm1hcHBlciIsImpzZSIsImxpYnMiLCJhY3Rpb25fbWFwcGVyIiwiX3RvZ2dsZU11bHRpQWN0aW9uQnV0dG9uIiwiJGNoZWNrZWQiLCJwcm9wIiwibGVuZ3RoIiwiX21hcFJvd0FjdGlvbiIsIiR0aGF0IiwiJGRyb3Bkb3duIiwiZmluZCIsIl9tYXBSb3dCdXR0b25Ecm9wZG93biIsImFjdGlvbnMiLCJpbmRleCIsIl9iaW5kRXZlbnRIYW5kbGVyIiwiX3NlY3Rpb25NYXBwaW5nIiwiX3NlbGVjdG9yTWFwcGluZyIsIl9nZXRBY3Rpb25DYWxsYmFjayIsImFjdGlvbiIsIl9zaG93T3JkZXJDYWxsYmFjayIsIl9jaGFuZ2VPcmRlclN0YXR1c0NhbGxiYWNrIiwiX2RlbGV0ZUNhbGxiYWNrIiwiX2NhbmNlbENhbGxiYWNrIiwiX2ludm9pY2VDYWxsYmFjayIsIl9lbWFpbEludm9pY2VDYWxsYmFjayIsIl9wYWNraW5nU2xpcENhbGxiYWNrIiwiX29yZGVyQ29uZmlybWF0aW9uQ2FsbGJhY2siLCJfcmVjcmVhdGVPcmRlckNvbmZpcm1hdGlvbkNhbGxiYWNrIiwiX3NlbmRPcmRlckNvbmZpcm1hdGlvbkNhbGxiYWNrIiwiX3dpdGhkcmF3YWxDYWxsYmFjayIsIl9hZGRUcmFja2luZ0NvZGVDYWxsYmFjayIsIl9kaGxMYWJlbENhbGxiYWNrIiwiX21haWxCZWV6T3ZlcnZpZXdDYWxsYmFjayIsIl9tYWlsQmVlek5vdGlmaWNhdGlvbnNDYWxsYmFjayIsIl9tYWlsQmVlekNvbnZlcnNhdGlvbnNDYWxsYmFjayIsIl9tdWx0aUNhbmNlbENhbGxiYWNrIiwiX211bHRpQ2hhbmdlT3JkZXJTdGF0dXNDYWxsYmFjayIsIl9tdWx0aURlbGV0ZUNhbGxiYWNrIiwiX2hlcm1lc0NhbGxiYWNrIiwiX2lsb3h4Q2FsbGJhY2siLCJldmVudCIsIm9yZGVySWQiLCJ0YXJnZXQiLCJwYXJlbnRzIiwidXJsIiwiVEVYVF9TSE9XIiwiYXR0ciIsIndpbmRvdyIsIm9wZW4iLCJyZXBsYWNlIiwidmFsIiwicmVtb3ZlQ2xhc3MiLCJlcSIsImFkZENsYXNzIiwiVEVYVF9HTV9TVEFUVVMiLCJjbGljayIsIiRkZWxldGUiLCJkZWxldGUiLCJnZXQiLCJCVVRUT05fTVVMVElfQ0FOQ0VMIiwiQlVUVE9OX0NSRUFURV9JTlZPSUNFIiwiQlVUVE9OX0NSRUFURV9QQUNLSU5HX1NMSVAiLCJUSVRMRV9PUkRFUiIsIlRJVExFX1JFQ1JFQVRFX09SREVSIiwiVEVYVF9DUkVBVEVfV0lUSERSQVdBTCIsIiR0YXJnZXQiLCJUWFRfUEFSQ0VMX1RSQUNLSU5HX1NFTkRCVVRUT05fVElUTEUiLCJCVVRUT05fREhMX0xBQkVMIiwiTUFJTEJFRVpfT1ZFUlZJRVciLCJNQUlMQkVFWl9OT1RJRklDQVRJT05TIiwiTUFJTEJFRVpfQ09OVkVSU0FUSU9OUyIsIkJVVFRPTl9IRVJNRVMiLCJnZXRfbGFiZWxzIiwiQlVUVE9OX01VTFRJX0NIQU5HRV9PUkRFUl9TVEFUVVMiLCJCVVRUT05fTVVMVElfREVMRVRFIiwiX21hcFRhYmxlQWN0aW9ucyIsIl9tYXBVbm1hcHBlZFJvd0FjdGlvbnMiLCJ1bm1hcHBlZFJvd0FjdGlvbnMiLCJlYWNoIiwiX2FscmVhZHlNYXBwZWQiLCJwdXNoIiwiJGJ1dHRvbiIsImNhbGxiYWNrIiwidW5kZWZpbmVkIiwiYnV0dG9uX2Ryb3Bkb3duIiwibWFwQWN0aW9uIiwidGV4dCIsIl9tYXBVbm1hcHBlZE11bHRpQWN0aW9ucyIsInVubWFwcGVkTXVsdGlBY3Rpb25zIiwiaXMiLCJfYWRkQnV0dG9uVG9NYXBwZWRBcnJheSIsImJ1dHRvblNlbGVjdG9yIiwiY3VzdG9tUmVjZW50QnV0dG9uU2VsZWN0b3IiLCJ0YXJnZXRTZWxlY3RvciIsInNlY3Rpb24iLCJjdXN0b21FbGVtZW50IiwiX2ZpeFJvd1NlbGVjdGlvbkZvckNvbnRyb2xFbGVtZW50cyIsImFkZCIsIm9uIiwic3RvcFByb3BhZ2F0aW9uIiwiaW5pdCIsImRvbmUiLCJpbnRlcnZhbCIsInNldEludGVydmFsIiwiY2xlYXJJbnRlcnZhbCIsInRhYmxlQWN0aW9ucyIsIm5vdCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCLENBQ0MseUJBREQsRUFHQyxDQUNDRixHQUFHRyxNQUFILEdBQVkscUJBRGIsRUFFQ0gsR0FBR0csTUFBSCxHQUFZLHVCQUZiLENBSEQ7O0FBUUM7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFlBQVcsRUFiWjs7O0FBZUM7Ozs7O0FBS0FDLFdBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBcEJYOzs7QUFzQkM7Ozs7O0FBS0FNLGlCQUFnQixFQTNCakI7OztBQTZCQzs7Ozs7QUFLQUMsVUFBU0MsSUFBSUMsSUFBSixDQUFTQyxhQWxDbkI7OztBQW9DQzs7Ozs7QUFLQVosVUFBUyxFQXpDVjs7QUEyQ0E7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLEtBQUlhLDJCQUEyQixTQUEzQkEsd0JBQTJCLEdBQVc7QUFDekMsTUFBSUMsV0FBV1YsRUFBRSxnREFBRixDQUFmO0FBQ0FBLElBQUUsNEJBQUYsRUFBZ0NXLElBQWhDLENBQXFDLFVBQXJDLEVBQWlELENBQUNELFNBQVNFLE1BQTNEO0FBQ0EsRUFIRDs7QUFLQTs7Ozs7Ozs7QUFRQSxLQUFJQyxnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVNDLEtBQVQsRUFBZ0I7QUFDbkM7Ozs7QUFJQSxNQUFJQyxZQUFZRCxNQUFNRSxJQUFOLENBQVcscUJBQVgsQ0FBaEI7O0FBRUEsTUFBSUQsVUFBVUgsTUFBZCxFQUFzQjtBQUNyQksseUJBQXNCRixTQUF0QjtBQUNBO0FBQ0QsRUFWRDs7QUFZQSxLQUFJRSx3QkFBd0IsU0FBeEJBLHFCQUF3QixDQUFTRixTQUFULEVBQW9CO0FBQy9DLE1BQUlHLFVBQVUsQ0FDYixXQURhLEVBRWIsZ0JBRmEsRUFHYixRQUhhLEVBSWIsa0JBSmEsRUFLYix1QkFMYSxFQU1iLG9CQU5hLEVBT2IsNEJBUGEsRUFRYixhQVJhLEVBU2Isc0JBVGEsRUFVYixrQkFWYSxFQVdiLHdCQVhhLEVBWWIsc0NBWmEsRUFhYixrQkFiYSxFQWNiLG1CQWRhLEVBZWIsd0JBZmEsRUFnQmIsd0JBaEJhLEVBaUJiLGVBakJhLENBQWQ7O0FBb0JBLE9BQUssSUFBSUMsS0FBVCxJQUFrQkQsT0FBbEIsRUFBMkI7QUFDMUJFLHFCQUFrQkwsU0FBbEIsRUFBNkJHLFFBQVFDLEtBQVIsQ0FBN0IsRUFBNkMsd0JBQTdDO0FBQ0E7QUFDRCxFQXhCRDs7QUEwQkE7Ozs7OztBQU1BLEtBQUlFLGtCQUFrQjtBQUNyQixlQUFhLFFBRFE7QUFFckIsb0JBQWtCLFFBRkc7QUFHckIsWUFBVSxTQUhXO0FBSXJCLHNCQUFvQixRQUpDO0FBS3JCLDJCQUF5QixRQUxKO0FBTXJCLHdCQUFzQixRQU5EO0FBT3JCLGdDQUE4QixRQVBUO0FBUXJCLGlCQUFlLFFBUk07QUFTckIsMEJBQXdCLFFBVEg7QUFVckIsc0JBQW9CLFFBVkM7QUFXckIsNEJBQTBCLFFBWEw7QUFZckIsMENBQXdDLGlCQVpuQjtBQWFyQixzQkFBb0IsUUFiQztBQWNyQix1QkFBcUIsUUFkQTtBQWVyQiw0QkFBMEIsUUFmTDtBQWdCckIsNEJBQTBCLFFBaEJMO0FBaUJyQix5QkFBdUIsUUFqQkY7QUFrQnJCLHNDQUFvQyxRQWxCZjtBQW1CckIseUJBQXVCLFFBbkJGO0FBb0JyQixtQkFBaUIsUUFwQkk7QUFxQnJCLGdCQUFjO0FBckJPLEVBQXRCOztBQXdCQTs7Ozs7O0FBTUEsS0FBSUMsbUJBQW1CO0FBQ3RCLGVBQWEsNkNBRFM7QUFFdEIsb0JBQWtCLHlEQUZJO0FBR3RCLFlBQVUsNENBSFk7QUFJdEIsc0JBQW9CLDBDQUpFO0FBS3RCLDJCQUF5Qiw2Q0FMSDtBQU10Qix3QkFBc0IsZ0RBTkE7QUFPdEIsZ0NBQThCLGtEQVBSO0FBUXRCLGlCQUFlLHdEQVJPO0FBU3RCLDBCQUF3QixpRUFURjtBQVV0QixzQkFBb0IsOENBVkU7QUFXdEIsNEJBQTBCLHVEQVhKO0FBWXRCLDBDQUF3Qyx1REFabEI7QUFhdEIsc0JBQW9CLCtDQWJFO0FBY3RCLHVCQUFxQiw4REFkQztBQWV0Qiw0QkFBMEIsZ0VBZko7QUFnQnRCLDRCQUEwQiwrREFoQko7QUFpQnRCLHlCQUF1QixrREFqQkQ7QUFrQnRCLHNDQUFvQyx5REFsQmQ7QUFtQnRCLHlCQUF1QixrREFuQkQ7QUFvQnRCLG1CQUFpQiw0Q0FwQks7QUFxQnRCLGdCQUFjO0FBckJRLEVBQXZCOztBQXdCQSxLQUFJQyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFTQyxNQUFULEVBQWlCO0FBQ3pDLFVBQVFBLE1BQVI7QUFDQyxRQUFLLFdBQUw7QUFDQyxXQUFPQyxrQkFBUDtBQUNELFFBQUssZ0JBQUw7QUFDQyxXQUFPQywwQkFBUDtBQUNELFFBQUssUUFBTDtBQUNDLFdBQU9DLGVBQVA7QUFDRCxRQUFLLGtCQUFMO0FBQ0MsV0FBT0MsZUFBUDtBQUNELFFBQUssdUJBQUw7QUFDQyxXQUFPQyxnQkFBUDtBQUNELFFBQUssb0JBQUw7QUFDQyxXQUFPQyxxQkFBUDtBQUNELFFBQUssNEJBQUw7QUFDQyxXQUFPQyxvQkFBUDtBQUNELFFBQUssYUFBTDtBQUNDLFdBQU9DLDBCQUFQO0FBQ0QsUUFBSyxzQkFBTDtBQUNDLFdBQU9DLGtDQUFQO0FBQ0QsUUFBSyxrQkFBTDtBQUNDLFdBQU9DLDhCQUFQO0FBQ0QsUUFBSyx3QkFBTDtBQUNDLFdBQU9DLG1CQUFQO0FBQ0QsUUFBSyxzQ0FBTDtBQUNDLFdBQU9DLHdCQUFQO0FBQ0QsUUFBSyxrQkFBTDtBQUNDLFdBQU9DLGlCQUFQO0FBQ0QsUUFBSyxtQkFBTDtBQUNDLFdBQU9DLHlCQUFQO0FBQ0QsUUFBSyx3QkFBTDtBQUNDLFdBQU9DLDhCQUFQO0FBQ0QsUUFBSyx3QkFBTDtBQUNDLFdBQU9DLDhCQUFQO0FBQ0QsUUFBSyxxQkFBTDtBQUNDLFdBQU9DLG9CQUFQO0FBQ0QsUUFBSyxrQ0FBTDtBQUNDLFdBQU9DLCtCQUFQO0FBQ0QsUUFBSyxxQkFBTDtBQUNDLFdBQU9DLG9CQUFQO0FBQ0QsUUFBSyxlQUFMO0FBQ0MsV0FBT0MsZUFBUDtBQUNELFFBQUssWUFBTDtBQUNDLFdBQU9DLGNBQVA7QUExQ0Y7QUE0Q0EsRUE3Q0Q7O0FBK0NBLEtBQUlwQixxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFTcUIsS0FBVCxFQUFnQjtBQUN4QyxNQUFJQyxVQUFVL0MsRUFBRThDLE1BQU1FLE1BQVIsRUFBZ0JDLE9BQWhCLENBQXdCLElBQXhCLEVBQThCbkQsSUFBOUIsQ0FBbUMsUUFBbkMsQ0FBZDtBQUNBLE1BQUlvRCxNQUFNbEQsRUFBRXNCLGlCQUFpQjZCLFNBQW5CLEVBQThCQyxJQUE5QixDQUFtQyxNQUFuQyxDQUFWO0FBQ0FDLFNBQU9DLElBQVAsQ0FBWUosSUFBSUssT0FBSixDQUFZLFdBQVosRUFBeUIsU0FBU1IsT0FBVCxHQUFtQixHQUE1QyxDQUFaLEVBQThELE9BQTlEO0FBQ0EsRUFKRDs7QUFNQSxLQUFJckIsNkJBQTZCLFNBQTdCQSwwQkFBNkIsQ0FBU29CLEtBQVQsRUFBZ0I7QUFDaEQsTUFBSUMsVUFBVS9DLEVBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4Qm5ELElBQTlCLENBQW1DLFFBQW5DLENBQWQ7QUFDQUUsSUFBRSxjQUFGLEVBQWtCd0QsR0FBbEIsQ0FBc0JULE9BQXRCO0FBQ0EvQyxJQUFFLG1DQUFGLEVBQXVDeUQsV0FBdkMsQ0FBbUQsU0FBbkQ7QUFDQXpELElBQUUsaUNBQUYsRUFBcUNXLElBQXJDLENBQTBDLFNBQTFDLEVBQXFELEtBQXJEO0FBQ0FYLElBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4QlMsRUFBOUIsQ0FBaUMsQ0FBakMsRUFBb0MxQyxJQUFwQyxDQUF5QyxrQkFBekMsRUFBNkQyQyxRQUE3RCxDQUFzRSxTQUF0RTtBQUNBM0QsSUFBRThDLE1BQU1FLE1BQVIsRUFBZ0JDLE9BQWhCLENBQXdCLElBQXhCLEVBQThCUyxFQUE5QixDQUFpQyxDQUFqQyxFQUFvQzFDLElBQXBDLENBQXlDLGdCQUF6QyxFQUEyREwsSUFBM0QsQ0FBZ0UsU0FBaEUsRUFBMkUsSUFBM0U7QUFDQVgsSUFBRXNCLGlCQUFpQnNDLGNBQW5CLEVBQW1DQyxLQUFuQztBQUNBLEVBUkQ7O0FBVUEsS0FBSWxDLGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBU21CLEtBQVQsRUFBZ0I7QUFDckMsTUFBSUMsVUFBVS9DLEVBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4Qm5ELElBQTlCLENBQW1DLFFBQW5DLENBQWQ7QUFDQSxNQUFJZ0UsVUFBVTlELEVBQUVzQixpQkFBaUJ5QyxNQUFuQixDQUFkO0FBQ0FELFVBQVFoRSxJQUFSLENBQWEsVUFBYixFQUF5QmlELE9BQXpCO0FBQ0FlLFVBQVFFLEdBQVIsQ0FBWSxDQUFaLEVBQWVILEtBQWY7QUFDQSxFQUxEOztBQU9BLEtBQUlqQyxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQVNrQixLQUFULEVBQWdCO0FBQ3JDLE1BQUlDLFVBQVUvQyxFQUFFOEMsTUFBTUUsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsSUFBeEIsRUFBOEJuRCxJQUE5QixDQUFtQyxRQUFuQyxDQUFkO0FBQ0FFLElBQUUsY0FBRixFQUFrQndELEdBQWxCLENBQXNCVCxPQUF0QjtBQUNBL0MsSUFBRSxtQ0FBRixFQUF1Q3lELFdBQXZDLENBQW1ELFNBQW5EO0FBQ0F6RCxJQUFFLGlDQUFGLEVBQXFDVyxJQUFyQyxDQUEwQyxTQUExQyxFQUFxRCxLQUFyRDtBQUNBWCxJQUFFOEMsTUFBTUUsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsSUFBeEIsRUFBOEJTLEVBQTlCLENBQWlDLENBQWpDLEVBQW9DMUMsSUFBcEMsQ0FBeUMsa0JBQXpDLEVBQTZEMkMsUUFBN0QsQ0FBc0UsU0FBdEU7QUFDQTNELElBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4QlMsRUFBOUIsQ0FBaUMsQ0FBakMsRUFBb0MxQyxJQUFwQyxDQUF5QyxnQkFBekMsRUFBMkRMLElBQTNELENBQWdFLFNBQWhFLEVBQTJFLElBQTNFO0FBQ0FYLElBQUVzQixpQkFBaUIyQyxtQkFBbkIsRUFBd0NKLEtBQXhDO0FBQ0EsRUFSRDs7QUFVQSxLQUFJaEMsbUJBQW1CLFNBQW5CQSxnQkFBbUIsQ0FBU2lCLEtBQVQsRUFBZ0I7QUFDdEMsTUFBSUMsVUFBVS9DLEVBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4Qm5ELElBQTlCLENBQW1DLFFBQW5DLENBQWQ7QUFDQSxNQUFJb0QsTUFBTWxELEVBQUVzQixpQkFBaUI0QyxxQkFBbkIsRUFBMENkLElBQTFDLENBQStDLE1BQS9DLENBQVY7QUFDQUMsU0FBT0MsSUFBUCxDQUFZSixJQUFJSyxPQUFKLENBQVksV0FBWixFQUF5QixTQUFTUixPQUFULEdBQW1CLEdBQTVDLENBQVosRUFBOEQsUUFBOUQ7QUFDQSxFQUpEOztBQU1BLEtBQUlqQix3QkFBd0IsU0FBeEJBLHFCQUF3QixDQUFTZ0IsS0FBVCxFQUFnQjtBQUMzQyxNQUFJQyxVQUFVL0MsRUFBRThDLE1BQU1FLE1BQVIsRUFBZ0JDLE9BQWhCLENBQXdCLElBQXhCLEVBQThCbkQsSUFBOUIsQ0FBbUMsUUFBbkMsQ0FBZDtBQUNBRSxJQUFFLGNBQUYsRUFBa0J3RCxHQUFsQixDQUFzQlQsT0FBdEI7QUFDQS9DLElBQUUsa0JBQUYsRUFBc0I2RCxLQUF0QjtBQUNBLEVBSkQ7O0FBTUEsS0FBSTlCLHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQVNlLEtBQVQsRUFBZ0I7QUFDMUMsTUFBSUMsVUFBVS9DLEVBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4Qm5ELElBQTlCLENBQW1DLFFBQW5DLENBQWQ7QUFDQSxNQUFJb0QsTUFBTWxELEVBQUVzQixpQkFBaUI2QywwQkFBbkIsRUFBK0NmLElBQS9DLENBQW9ELE1BQXBELENBQVY7QUFDQUMsU0FBT0MsSUFBUCxDQUFZSixJQUFJSyxPQUFKLENBQVksV0FBWixFQUF5QixTQUFTUixPQUFULEdBQW1CLEdBQTVDLENBQVosRUFBOEQsUUFBOUQ7QUFDQSxFQUpEOztBQU1BLEtBQUlmLDZCQUE2QixTQUE3QkEsMEJBQTZCLENBQVNjLEtBQVQsRUFBZ0I7QUFDaEQsTUFBSUMsVUFBVS9DLEVBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4Qm5ELElBQTlCLENBQW1DLFFBQW5DLENBQWQ7QUFDQSxNQUFJb0QsTUFBTWxELEVBQUVzQixpQkFBaUI4QyxXQUFuQixFQUFnQ2hCLElBQWhDLENBQXFDLE1BQXJDLENBQVY7QUFDQUMsU0FBT0MsSUFBUCxDQUFZSixJQUFJSyxPQUFKLENBQVksV0FBWixFQUF5QixTQUFTUixPQUFULEdBQW1CLEdBQTVDLENBQVosRUFBOEQsUUFBOUQ7QUFDQSxFQUpEOztBQU1BLEtBQUlkLHFDQUFxQyxTQUFyQ0Esa0NBQXFDLENBQVNhLEtBQVQsRUFBZ0I7QUFDeEQsTUFBSUMsVUFBVS9DLEVBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4Qm5ELElBQTlCLENBQW1DLFFBQW5DLENBQWQ7QUFDQSxNQUFJb0QsTUFBTWxELEVBQUVzQixpQkFBaUIrQyxvQkFBbkIsRUFBeUNqQixJQUF6QyxDQUE4QyxNQUE5QyxDQUFWO0FBQ0FDLFNBQU9DLElBQVAsQ0FBWUosSUFBSUssT0FBSixDQUFZLFdBQVosRUFBeUIsU0FBU1IsT0FBVCxHQUFtQixHQUE1QyxDQUFaLEVBQThELFFBQTlEO0FBQ0EsRUFKRDs7QUFNQSxLQUFJYixpQ0FBaUMsU0FBakNBLDhCQUFpQyxDQUFTWSxLQUFULEVBQWdCO0FBQ3BELE1BQUlDLFVBQVUvQyxFQUFFOEMsTUFBTUUsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsSUFBeEIsRUFBOEJuRCxJQUE5QixDQUFtQyxRQUFuQyxDQUFkO0FBQ0FFLElBQUUsY0FBRixFQUFrQndELEdBQWxCLENBQXNCVCxPQUF0QjtBQUNBL0MsSUFBRSxnQkFBRixFQUFvQjZELEtBQXBCO0FBQ0EsRUFKRDs7QUFNQSxLQUFJMUIsc0JBQXNCLFNBQXRCQSxtQkFBc0IsQ0FBU1csS0FBVCxFQUFnQjtBQUN6QyxNQUFJQyxVQUFVL0MsRUFBRThDLE1BQU1FLE1BQVIsRUFBZ0JDLE9BQWhCLENBQXdCLElBQXhCLEVBQThCbkQsSUFBOUIsQ0FBbUMsUUFBbkMsQ0FBZDtBQUNBLE1BQUlvRCxNQUFNbEQsRUFBRXNCLGlCQUFpQmdELHNCQUFuQixFQUEyQ2xCLElBQTNDLENBQWdELE1BQWhELENBQVY7QUFDQUMsU0FBT0MsSUFBUCxDQUFZSixJQUFJSyxPQUFKLENBQVksYUFBWixFQUEyQixjQUFjUixPQUF6QyxDQUFaLEVBQStELFFBQS9EO0FBQ0EsRUFKRDs7QUFNQSxLQUFJWCwyQkFBMkIsU0FBM0JBLHdCQUEyQixDQUFTVSxLQUFULEVBQWdCO0FBQzlDLE1BQUlDLFVBQVUvQyxFQUFFOEMsTUFBTUUsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsSUFBeEIsRUFBOEJuRCxJQUE5QixDQUFtQyxRQUFuQyxDQUFkO0FBQ0EsTUFBSXlFLFVBQVV2RSxFQUFFc0IsaUJBQWlCa0Qsb0NBQW5CLENBQWQ7QUFDQUQsVUFBUXpFLElBQVIsQ0FBYSxVQUFiLEVBQXlCaUQsT0FBekI7QUFDQXdCLFVBQVFQLEdBQVIsQ0FBWSxDQUFaLEVBQWVILEtBQWY7QUFDQSxFQUxEOztBQU9BLEtBQUl4QixvQkFBb0IsU0FBcEJBLGlCQUFvQixDQUFTUyxLQUFULEVBQWdCO0FBQ3ZDLE1BQUlDLFVBQVUvQyxFQUFFOEMsTUFBTUUsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsSUFBeEIsRUFBOEJuRCxJQUE5QixDQUFtQyxRQUFuQyxDQUFkO0FBQ0EsTUFBSW9ELE1BQU1sRCxFQUFFc0IsaUJBQWlCbUQsZ0JBQW5CLEVBQXFDckIsSUFBckMsQ0FBMEMsTUFBMUMsQ0FBVjtBQUNBQyxTQUFPQyxJQUFQLENBQVlKLElBQUlLLE9BQUosQ0FBWSxVQUFaLEVBQXdCLFNBQVNSLE9BQWpDLENBQVosRUFBdUQsUUFBdkQ7QUFDQSxFQUpEOztBQU1BLEtBQUlULDRCQUE0QixTQUE1QkEseUJBQTRCLENBQVNRLEtBQVQsRUFBZ0I7QUFDL0MsTUFBSUMsVUFBVS9DLEVBQUU4QyxNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4Qm5ELElBQTlCLENBQW1DLFFBQW5DLENBQWQ7QUFDQSxNQUFJeUUsVUFBVXZFLEVBQUVzQixpQkFBaUJvRCxpQkFBbkIsQ0FBZDtBQUNBLE1BQUl4QixNQUFNcUIsUUFBUW5CLElBQVIsQ0FBYSxTQUFiLENBQVY7QUFDQUYsUUFBTUEsSUFBSUssT0FBSixDQUFZLFdBQVosRUFBeUIsU0FBU1IsT0FBVCxHQUFtQixHQUE1QyxDQUFOO0FBQ0F3QixVQUFRbkIsSUFBUixDQUFhLFNBQWIsRUFBd0JGLEdBQXhCO0FBQ0FxQixVQUFRUCxHQUFSLENBQVksQ0FBWixFQUFlSCxLQUFmO0FBQ0EsRUFQRDs7QUFTQSxLQUFJdEIsaUNBQWlDLFNBQWpDQSw4QkFBaUMsQ0FBU08sS0FBVCxFQUFnQjtBQUNwRCxNQUFJQyxVQUFVL0MsRUFBRThDLE1BQU1FLE1BQVIsRUFBZ0JDLE9BQWhCLENBQXdCLElBQXhCLEVBQThCbkQsSUFBOUIsQ0FBbUMsUUFBbkMsQ0FBZDtBQUNBLE1BQUl5RSxVQUFVdkUsRUFBRXNCLGlCQUFpQnFELHNCQUFuQixDQUFkO0FBQ0EsTUFBSXpCLE1BQU1xQixRQUFRbkIsSUFBUixDQUFhLFNBQWIsQ0FBVjtBQUNBRixRQUFNQSxJQUFJSyxPQUFKLENBQVksV0FBWixFQUF5QixTQUFTUixPQUFULEdBQW1CLEdBQTVDLENBQU47QUFDQXdCLFVBQVFuQixJQUFSLENBQWEsU0FBYixFQUF3QkYsR0FBeEI7QUFDQXFCLFVBQVFQLEdBQVIsQ0FBWSxDQUFaLEVBQWVILEtBQWY7QUFDQSxFQVBEOztBQVNBLEtBQUlyQixpQ0FBaUMsU0FBakNBLDhCQUFpQyxDQUFTTSxLQUFULEVBQWdCO0FBQ3BELE1BQUlDLFVBQVUvQyxFQUFFOEMsTUFBTUUsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsSUFBeEIsRUFBOEJuRCxJQUE5QixDQUFtQyxRQUFuQyxDQUFkO0FBQ0EsTUFBSXlFLFVBQVV2RSxFQUFFc0IsaUJBQWlCc0Qsc0JBQW5CLENBQWQ7QUFDQSxNQUFJMUIsTUFBTXFCLFFBQVFuQixJQUFSLENBQWEsU0FBYixDQUFWO0FBQ0FGLFFBQU1BLElBQUlLLE9BQUosQ0FBWSxXQUFaLEVBQXlCLFNBQVNSLE9BQVQsR0FBbUIsR0FBNUMsQ0FBTjtBQUNBd0IsVUFBUW5CLElBQVIsQ0FBYSxTQUFiLEVBQXdCRixHQUF4QjtBQUNBcUIsVUFBUVAsR0FBUixDQUFZLENBQVosRUFBZUgsS0FBZjtBQUNBLEVBUEQ7O0FBU0EsS0FBSWpCLGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBU0UsS0FBVCxFQUFnQjtBQUNyQyxNQUFJQyxVQUFVL0MsRUFBRThDLE1BQU1FLE1BQVIsRUFBZ0JDLE9BQWhCLENBQXdCLElBQXhCLEVBQThCbkQsSUFBOUIsQ0FBbUMsUUFBbkMsQ0FBZDtBQUNBLE1BQUl5RSxVQUFVdkUsRUFBRXNCLGlCQUFpQnVELGFBQW5CLENBQWQ7QUFDQSxNQUFJM0IsTUFBTXFCLFFBQVFuQixJQUFSLENBQWEsTUFBYixDQUFWO0FBQ0FGLFFBQU1BLElBQUlLLE9BQUosQ0FBWSxnQkFBWixFQUE4QixlQUFlUixPQUE3QyxDQUFOO0FBQ0F3QixVQUFRbkIsSUFBUixDQUFhLE1BQWIsRUFBcUJGLEdBQXJCO0FBQ0FxQixVQUFRUCxHQUFSLENBQVksQ0FBWixFQUFlSCxLQUFmO0FBQ0EsRUFQRDs7QUFTQSxLQUFJaEIsaUJBQWlCLFNBQWpCQSxjQUFpQixDQUFTQyxLQUFULEVBQWdCO0FBQ3BDLE1BQUl5QixVQUFVdkUsRUFBRXNCLGlCQUFpQndELFVBQW5CLENBQWQ7QUFDQVAsVUFBUVYsS0FBUjtBQUNBLEVBSEQ7O0FBS0EsS0FBSW5CLGtDQUFrQyxTQUFsQ0EsK0JBQWtDLENBQVNJLEtBQVQsRUFBZ0I7QUFDckQ5QyxJQUFFc0IsaUJBQWlCeUQsZ0NBQW5CLEVBQXFEZixHQUFyRCxDQUF5RCxDQUF6RCxFQUE0REgsS0FBNUQ7QUFDQSxFQUZEOztBQUlBLEtBQUlsQix1QkFBdUIsU0FBdkJBLG9CQUF1QixDQUFTRyxLQUFULEVBQWdCO0FBQzFDOUMsSUFBRXNCLGlCQUFpQjBELG1CQUFuQixFQUF3Q2hCLEdBQXhDLENBQTRDLENBQTVDLEVBQStDSCxLQUEvQztBQUNBLEVBRkQ7O0FBSUEsS0FBSXBCLHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQVNLLEtBQVQsRUFBZ0I7QUFDMUM5QyxJQUFFc0IsaUJBQWlCMkMsbUJBQW5CLEVBQXdDRCxHQUF4QyxDQUE0QyxDQUE1QyxFQUErQ0gsS0FBL0M7QUFDQSxFQUZEOztBQUlBOzs7OztBQUtBLEtBQUlvQixtQkFBbUIsU0FBbkJBLGdCQUFtQixHQUFXO0FBQ2pDLE1BQUlsRSxZQUFZZixFQUFFLHdCQUFGLENBQWhCOztBQUVBb0Isb0JBQWtCTCxTQUFsQixFQUE2QixrQ0FBN0I7O0FBRUEsTUFBSWYsRUFBRXNCLGlCQUFpQndELFVBQW5CLEVBQStCbEUsTUFBbkMsRUFBMkM7QUFDMUNRLHFCQUFrQkwsU0FBbEIsRUFBNkIsWUFBN0I7QUFDQTs7QUFFREssb0JBQWtCTCxTQUFsQixFQUE2QixxQkFBN0I7QUFDQUssb0JBQWtCTCxTQUFsQixFQUE2QixxQkFBN0I7QUFDQSxFQVhEOztBQWFBOzs7Ozs7Ozs7QUFTQSxLQUFJbUUseUJBQXlCLFNBQXpCQSxzQkFBeUIsQ0FBU25GLEtBQVQsRUFBZ0I7QUFDNUMsTUFBSW9GLHFCQUFxQixFQUF6QjtBQUNBbkYsSUFBRSxnREFDRCxrREFEQyxHQUVELGdFQUZDLEdBR0QsK0RBSEQsRUFHa0VvRixJQUhsRSxDQUd1RSxZQUFXO0FBQ2pGLE9BQUksQ0FBQ0MsZUFBZXJGLEVBQUUsSUFBRixDQUFmLENBQUwsRUFBOEI7QUFDN0JtRix1QkFBbUJHLElBQW5CLENBQXdCdEYsRUFBRSxJQUFGLENBQXhCO0FBQ0E7QUFDRCxHQVBEOztBQVNBLE1BQUkrQyxVQUFVaEQsTUFBTUQsSUFBTixDQUFXLFFBQVgsQ0FBZDtBQUFBLE1BQ0NpQixZQUFZaEIsTUFBTWlCLElBQU4sQ0FBVyxxQkFBWCxDQURiOztBQUdBaEIsSUFBRW9GLElBQUYsQ0FBT0Qsa0JBQVAsRUFBMkIsWUFBVztBQUNyQyxPQUFJSSxVQUFVdkYsRUFBRSxJQUFGLENBQWQ7QUFDQSxPQUFJd0YsV0FBVyxTQUFYQSxRQUFXLEdBQVc7QUFDekIsUUFBSUQsUUFBUTVFLElBQVIsQ0FBYSxNQUFiLE1BQXlCOEUsU0FBN0IsRUFBd0M7QUFDdkNGLGFBQVE1RSxJQUFSLENBQWEsTUFBYixFQUFxQjRFLFFBQVE1RSxJQUFSLENBQWEsTUFBYixFQUFxQjRDLE9BQXJCLENBQTZCLGtCQUE3QixFQUFpRCxTQUFTUixPQUExRCxDQUFyQjtBQUNBO0FBQ0R3QyxZQUFRdkIsR0FBUixDQUFZLENBQVosRUFBZUgsS0FBZjtBQUNBLElBTEQ7O0FBT0F2RCxPQUFJQyxJQUFKLENBQVNtRixlQUFULENBQXlCQyxTQUF6QixDQUFtQzVFLFNBQW5DLEVBQThDd0UsUUFBUUssSUFBUixFQUE5QyxFQUE4RCxFQUE5RCxFQUFrRUosUUFBbEU7QUFDQXBGLGlCQUFja0YsSUFBZCxDQUFtQkMsT0FBbkI7QUFDQSxHQVhEO0FBWUEsRUExQkQ7O0FBNEJBLEtBQUlNLDJCQUEyQixTQUEzQkEsd0JBQTJCLEdBQVc7QUFDekMsTUFBSUMsdUJBQXVCLEVBQTNCO0FBQ0E5RixJQUFFLCtDQUNELGlEQURDLEdBRUQsK0RBRkMsR0FHRCw4REFIRCxFQUdpRW9GLElBSGpFLENBR3NFLFlBQVc7QUFDaEYsT0FBSSxDQUFDQyxlQUFlckYsRUFBRSxJQUFGLENBQWYsQ0FBTCxFQUE4QjtBQUM3QjhGLHlCQUFxQlIsSUFBckIsQ0FBMEJ0RixFQUFFLElBQUYsQ0FBMUI7QUFDQTtBQUNELEdBUEQ7O0FBU0EsTUFBSWUsWUFBWWYsRUFBRSx3QkFBRixDQUFoQjtBQUNBQSxJQUFFb0YsSUFBRixDQUFPVSxvQkFBUCxFQUE2QixZQUFXO0FBQ3ZDLE9BQUlQLFVBQVV2RixFQUFFLElBQUYsQ0FBZDtBQUNBLE9BQUl3RixXQUFXLFNBQVhBLFFBQVcsR0FBVztBQUN6QkQsWUFBUXZCLEdBQVIsQ0FBWSxDQUFaLEVBQWVILEtBQWY7QUFDQSxJQUZEOztBQUlBdkQsT0FBSUMsSUFBSixDQUFTbUYsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUM1RSxTQUFuQyxFQUE4Q3dFLFFBQVFLLElBQVIsRUFBOUMsRUFBOEQsRUFBOUQsRUFBa0VKLFFBQWxFO0FBQ0FwRixpQkFBY2tGLElBQWQsQ0FBbUJDLE9BQW5CO0FBQ0EsR0FSRDtBQVNBLEVBckJEOztBQXVCQTs7Ozs7QUFLQSxLQUFJRixpQkFBaUIsU0FBakJBLGNBQWlCLENBQVNFLE9BQVQsRUFBa0I7QUFDdEMsT0FBSyxJQUFJcEUsS0FBVCxJQUFrQmYsYUFBbEIsRUFBaUM7QUFDaEMsT0FBSW1GLFFBQVFRLEVBQVIsQ0FBVzNGLGNBQWNlLEtBQWQsQ0FBWCxDQUFKLEVBQXNDO0FBQ3JDLFdBQU8sSUFBUDtBQUNBO0FBQ0Q7QUFDRCxTQUFPLEtBQVA7QUFDQSxFQVBEOztBQVNBOzs7Ozs7OztBQVFBLEtBQUk2RSwwQkFBMEIsU0FBMUJBLHVCQUEwQixDQUFTQyxjQUFULEVBQXlCO0FBQ3RELE1BQUk3RixjQUFjNkYsY0FBZCxNQUFrQ1IsU0FBdEMsRUFBaUQ7QUFDaEQsVUFBTyxJQUFQO0FBQ0E7QUFDRHJGLGdCQUFjNkYsY0FBZCxJQUFnQ2pHLEVBQUVpRyxjQUFGLENBQWhDO0FBQ0EsRUFMRDs7QUFPQTs7Ozs7Ozs7O0FBU0EsS0FBSTdFLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQVNMLFNBQVQsRUFBb0JTLE1BQXBCLEVBQTRCMEUsMEJBQTVCLEVBQXdEO0FBQy9FLE1BQUlDLGlCQUFpQjdFLGlCQUFpQkUsTUFBakIsQ0FBckI7QUFBQSxNQUNDNEUsVUFBVS9FLGdCQUFnQkcsTUFBaEIsQ0FEWDtBQUFBLE1BRUNnRSxXQUFXakUsbUJBQW1CQyxNQUFuQixDQUZaO0FBQUEsTUFHQzZFLGdCQUFnQnJHLEVBQUVrRywwQkFBRixFQUE4QnRGLE1BQTlCLEdBQXVDWixFQUFFa0csMEJBQUYsQ0FBdkMsR0FDQW5GLFNBSmpCO0FBS0EsTUFBSWYsRUFBRW1HLGNBQUYsRUFBa0J2RixNQUF0QixFQUE4QjtBQUM3Qm9GLDJCQUF3QkcsY0FBeEI7QUFDQTdGLE9BQUlDLElBQUosQ0FBU21GLGVBQVQsQ0FBeUJDLFNBQXpCLENBQW1DNUUsU0FBbkMsRUFBOENTLE1BQTlDLEVBQXNENEUsT0FBdEQsRUFBK0RaLFFBQS9ELEVBQXlFYSxhQUF6RTtBQUNBO0FBQ0QsRUFWRDs7QUFZQTs7Ozs7QUFLQSxLQUFJQyxxQ0FBcUMsU0FBckNBLGtDQUFxQyxHQUFXO0FBQ25EdEcsSUFBRSwwQ0FBRixFQUNFdUcsR0FERixDQUNNLGtCQUROLEVBRUVBLEdBRkYsQ0FFTSxlQUZOLEVBR0VBLEdBSEYsQ0FHTSxxQkFITixFQUlFQSxHQUpGLENBSU0sbUJBSk4sRUFLRUMsRUFMRixDQUtLLE9BTEwsRUFLYyxVQUFTMUQsS0FBVCxFQUFnQjtBQUM1QkEsU0FBTTJELGVBQU47QUFDQWhHO0FBQ0EsR0FSRjtBQVNBLEVBVkQ7O0FBWUE7QUFDQTtBQUNBOztBQUVBYixRQUFPOEcsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjtBQUNBLE1BQUlDLFdBQVdDLFlBQVksWUFBVztBQUNyQyxPQUFJN0csRUFBRSxxQkFBRixFQUF5QlksTUFBN0IsRUFBcUM7QUFDcENrRyxrQkFBY0YsUUFBZDs7QUFFQTNCO0FBQ0FZOztBQUVBLFFBQUlrQixlQUFlM0csYUFBbkI7O0FBRUE7QUFDQTRGLDRCQUF3QiwrREFBeEI7O0FBRUFoRyxNQUFFLHFCQUFGLEVBQXlCZ0gsR0FBekIsQ0FBNkIsc0JBQTdCLEVBQXFENUIsSUFBckQsQ0FBMEQsWUFBVztBQUNwRWhGLHFCQUFnQixFQUFoQjs7QUFFQSxVQUFLLElBQUllLEtBQVQsSUFBa0I0RixZQUFsQixFQUFnQztBQUMvQjNHLG9CQUFjZSxLQUFkLElBQXVCNEYsYUFBYTVGLEtBQWIsQ0FBdkI7QUFDQTs7QUFFRE4sbUJBQWNiLEVBQUUsSUFBRixDQUFkO0FBQ0FrRiw0QkFBdUJsRixFQUFFLElBQUYsQ0FBdkI7QUFDQSxLQVREOztBQVdBc0c7O0FBRUE7QUFDQTdGO0FBQ0E7QUFDRCxHQTVCYyxFQTRCWixHQTVCWSxDQUFmOztBQThCQTtBQUNBO0FBQ0FBOztBQUVBa0c7QUFDQSxFQXJDRDs7QUF1Q0EsUUFBTy9HLE1BQVA7QUFDQSxDQTlpQkYiLCJmaWxlIjoib3JkZXJzL29yZGVyc190YWJsZV9jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBvcmRlcnNfdGFibGVfY29udHJvbGxlci5qcyAyMDE2LTEwLTIzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBPcmRlcnMgVGFibGUgQ29udHJvbGxlclxuICpcbiAqIFRoaXMgY29udHJvbGxlciBjb250YWlucyB0aGUgbWFwcGluZyBsb2dpYyBvZiB0aGUgb3JkZXJzIHRhYmxlLlxuICpcbiAqIEBtb2R1bGUgQ29tcGF0aWJpbGl0eS9vcmRlcnNfdGFibGVfY29udHJvbGxlclxuICovXG5neC5jb21wYXRpYmlsaXR5Lm1vZHVsZShcblx0J29yZGVyc190YWJsZV9jb250cm9sbGVyJyxcblx0XG5cdFtcblx0XHRneC5zb3VyY2UgKyAnL2xpYnMvYWN0aW9uX21hcHBlcicsXG5cdFx0Z3guc291cmNlICsgJy9saWJzL2J1dHRvbl9kcm9wZG93bidcblx0XSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb21wYXRpYmlsaXR5L29yZGVyc190YWJsZV9jb250cm9sbGVyICovXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFUyBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0ge30sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogQXJyYXkgb2YgbWFwcGVkIGJ1dHRvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIEFycmF5XG5cdFx0XHQgKi9cblx0XHRcdG1hcHBlZEJ1dHRvbnMgPSBbXSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBUaGUgbWFwcGVyIGxpYnJhcnlcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1hcHBlciA9IGpzZS5saWJzLmFjdGlvbl9tYXBwZXIsXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFBSSVZBVEUgTUVUSE9EU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERpc2FibGUvRW5hYmxlIHRoZSBidXR0b25zIG9uIHRoZSBib3R0b20gYnV0dG9uLWRyb3Bkb3duXG5cdFx0ICogZGVwZW5kZW50IG9uIHRoZSBjaGVja2JveGVzIHNlbGVjdGlvblxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF90b2dnbGVNdWx0aUFjdGlvbkJ1dHRvbiA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyICRjaGVja2VkID0gJCgndHJbZGF0YS1yb3ctaWRdIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkJyk7XG5cdFx0XHQkKCcuanMtYm90dG9tLWRyb3Bkb3duIGJ1dHRvbicpLnByb3AoJ2Rpc2FibGVkJywgISRjaGVja2VkLmxlbmd0aCk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNYXAgYWN0aW9ucyBmb3IgZXZlcnkgcm93IGluIHRoZSB0YWJsZS5cblx0XHQgKlxuXHRcdCAqIFRoaXMgbWV0aG9kIHdpbGwgbWFwIHRoZSBhY3Rpb25zIGZvciBlYWNoXG5cdFx0ICogcm93IG9mIHRoZSB0YWJsZS5cblx0XHQgKlxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9tYXBSb3dBY3Rpb24gPSBmdW5jdGlvbigkdGhhdCkge1xuXHRcdFx0LyoqXG5cdFx0XHQgKiBSZWZlcmVuY2UgdG8gdGhlIHJvdyBhY3Rpb24gZHJvcGRvd25cblx0XHRcdCAqIEB2YXIge29iamVjdCB8IGpRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0dmFyICRkcm9wZG93biA9ICR0aGF0LmZpbmQoJy5qcy1idXR0b24tZHJvcGRvd24nKTtcblx0XHRcdFxuXHRcdFx0aWYgKCRkcm9wZG93bi5sZW5ndGgpIHtcblx0XHRcdFx0X21hcFJvd0J1dHRvbkRyb3Bkb3duKCRkcm9wZG93bik7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX21hcFJvd0J1dHRvbkRyb3Bkb3duID0gZnVuY3Rpb24oJGRyb3Bkb3duKSB7XG5cdFx0XHR2YXIgYWN0aW9ucyA9IFtcblx0XHRcdFx0J1RFWFRfU0hPVycsXG5cdFx0XHRcdCdURVhUX0dNX1NUQVRVUycsXG5cdFx0XHRcdCdkZWxldGUnLFxuXHRcdFx0XHQnQlVUVE9OX0dNX0NBTkNFTCcsXG5cdFx0XHRcdCdCVVRUT05fQ1JFQVRFX0lOVk9JQ0UnLFxuXHRcdFx0XHQnVElUTEVfSU5WT0lDRV9NQUlMJyxcblx0XHRcdFx0J0JVVFRPTl9DUkVBVEVfUEFDS0lOR19TTElQJyxcblx0XHRcdFx0J1RJVExFX09SREVSJyxcblx0XHRcdFx0J1RJVExFX1JFQ1JFQVRFX09SREVSJyxcblx0XHRcdFx0J1RJVExFX1NFTkRfT1JERVInLFxuXHRcdFx0XHQnVEVYVF9DUkVBVEVfV0lUSERSQVdBTCcsXG5cdFx0XHRcdCdUWFRfUEFSQ0VMX1RSQUNLSU5HX1NFTkRCVVRUT05fVElUTEUnLFxuXHRcdFx0XHQnQlVUVE9OX0RITF9MQUJFTCcsXG5cdFx0XHRcdCdNQUlMQkVFWl9PVkVSVklFVycsXG5cdFx0XHRcdCdNQUlMQkVFWl9OT1RJRklDQVRJT05TJyxcblx0XHRcdFx0J01BSUxCRUVaX0NPTlZFUlNBVElPTlMnLFxuXHRcdFx0XHQnQlVUVE9OX0hFUk1FUydcblx0XHRcdF07XG5cdFx0XHRcblx0XHRcdGZvciAodmFyIGluZGV4IGluIGFjdGlvbnMpIHtcblx0XHRcdFx0X2JpbmRFdmVudEhhbmRsZXIoJGRyb3Bkb3duLCBhY3Rpb25zW2luZGV4XSwgJy5zaW5nbGUtb3JkZXItZHJvcGRvd24nKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmluZXMgdGhlIGxhbmd1YWdlIHNlY3Rpb24gZm9yIGVhY2ggdGV4dCB0aWxlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9zZWN0aW9uTWFwcGluZyA9IHtcblx0XHRcdCdURVhUX1NIT1cnOiAnb3JkZXJzJyxcblx0XHRcdCdURVhUX0dNX1NUQVRVUyc6ICdvcmRlcnMnLFxuXHRcdFx0J2RlbGV0ZSc6ICdidXR0b25zJyxcblx0XHRcdCdCVVRUT05fR01fQ0FOQ0VMJzogJ29yZGVycycsXG5cdFx0XHQnQlVUVE9OX0NSRUFURV9JTlZPSUNFJzogJ29yZGVycycsXG5cdFx0XHQnVElUTEVfSU5WT0lDRV9NQUlMJzogJ29yZGVycycsXG5cdFx0XHQnQlVUVE9OX0NSRUFURV9QQUNLSU5HX1NMSVAnOiAnb3JkZXJzJyxcblx0XHRcdCdUSVRMRV9PUkRFUic6ICdvcmRlcnMnLFxuXHRcdFx0J1RJVExFX1JFQ1JFQVRFX09SREVSJzogJ29yZGVycycsXG5cdFx0XHQnVElUTEVfU0VORF9PUkRFUic6ICdvcmRlcnMnLFxuXHRcdFx0J1RFWFRfQ1JFQVRFX1dJVEhEUkFXQUwnOiAnb3JkZXJzJyxcblx0XHRcdCdUWFRfUEFSQ0VMX1RSQUNLSU5HX1NFTkRCVVRUT05fVElUTEUnOiAncGFyY2VsX3NlcnZpY2VzJyxcblx0XHRcdCdCVVRUT05fREhMX0xBQkVMJzogJ29yZGVycycsXG5cdFx0XHQnTUFJTEJFRVpfT1ZFUlZJRVcnOiAnb3JkZXJzJyxcblx0XHRcdCdNQUlMQkVFWl9OT1RJRklDQVRJT05TJzogJ29yZGVycycsXG5cdFx0XHQnTUFJTEJFRVpfQ09OVkVSU0FUSU9OUyc6ICdvcmRlcnMnLFxuXHRcdFx0J0JVVFRPTl9NVUxUSV9DQU5DRUwnOiAnb3JkZXJzJyxcblx0XHRcdCdCVVRUT05fTVVMVElfQ0hBTkdFX09SREVSX1NUQVRVUyc6ICdvcmRlcnMnLFxuXHRcdFx0J0JVVFRPTl9NVUxUSV9ERUxFVEUnOiAnb3JkZXJzJyxcblx0XHRcdCdCVVRUT05fSEVSTUVTJzogJ29yZGVycycsXG5cdFx0XHQnZ2V0X2xhYmVscyc6ICdpbG94eCdcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmluZXMgdGFyZ2V0IHNlbGVjdG9yc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfc2VsZWN0b3JNYXBwaW5nID0ge1xuXHRcdFx0J1RFWFRfU0hPVyc6ICcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmJ0bi1kZXRhaWxzJyxcblx0XHRcdCdURVhUX0dNX1NUQVRVUyc6ICcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmJ0bi11cGRhdGVfb3JkZXJfc3RhdHVzJyxcblx0XHRcdCdkZWxldGUnOiAnLmNvbnRlbnRUYWJsZSAuaW5mb0JveENvbnRlbnQgYS5idG4tZGVsZXRlJyxcblx0XHRcdCdCVVRUT05fR01fQ0FOQ0VMJzogJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IC5HTV9DQU5DRUwnLFxuXHRcdFx0J0JVVFRPTl9DUkVBVEVfSU5WT0lDRSc6ICcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmJ0bi1pbnZvaWNlJyxcblx0XHRcdCdUSVRMRV9JTlZPSUNFX01BSUwnOiAnLmNvbnRlbnRUYWJsZSAuaW5mb0JveENvbnRlbnQgLkdNX0lOVk9JQ0VfTUFJTCcsXG5cdFx0XHQnQlVUVE9OX0NSRUFURV9QQUNLSU5HX1NMSVAnOiAnLmNvbnRlbnRUYWJsZSAuaW5mb0JveENvbnRlbnQgYS5idG4tcGFja2luZ19zbGlwJyxcblx0XHRcdCdUSVRMRV9PUkRFUic6ICcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmJ0bi1vcmRlcl9jb25maXJtYXRpb24nLFxuXHRcdFx0J1RJVExFX1JFQ1JFQVRFX09SREVSJzogJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuYnRuLXJlY3JlYXRlX29yZGVyX2NvbmZpcm1hdGlvbicsXG5cdFx0XHQnVElUTEVfU0VORF9PUkRFUic6ICcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCAuR01fU0VORF9PUkRFUicsXG5cdFx0XHQnVEVYVF9DUkVBVEVfV0lUSERSQVdBTCc6ICcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmJ0bi1jcmVhdGVfd2l0aGRyYXdhbCcsXG5cdFx0XHQnVFhUX1BBUkNFTF9UUkFDS0lOR19TRU5EQlVUVE9OX1RJVExFJzogJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuYnRuLWFkZF90cmFja2luZ19jb2RlJyxcblx0XHRcdCdCVVRUT05fREhMX0xBQkVMJzogJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuYnRuLWRobF9sYWJlbCcsXG5cdFx0XHQnTUFJTEJFRVpfT1ZFUlZJRVcnOiAnLmNvbnRlbnRUYWJsZSAuaW5mb0JveENvbnRlbnQgYS5jb250ZXh0X3ZpZXdfYnV0dG9uLmJ0bl9sZWZ0Jyxcblx0XHRcdCdNQUlMQkVFWl9OT1RJRklDQVRJT05TJzogJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuY29udGV4dF92aWV3X2J1dHRvbi5idG5fbWlkZGxlJyxcblx0XHRcdCdNQUlMQkVFWl9DT05WRVJTQVRJT05TJzogJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuY29udGV4dF92aWV3X2J1dHRvbi5idG5fcmlnaHQnLFxuXHRcdFx0J0JVVFRPTl9NVUxUSV9DQU5DRUwnOiAnLmNvbnRlbnRUYWJsZSAuaW5mb0JveENvbnRlbnQgYS5idG4tbXVsdGlfY2FuY2VsJyxcblx0XHRcdCdCVVRUT05fTVVMVElfQ0hBTkdFX09SREVSX1NUQVRVUyc6ICcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmJ0bi11cGRhdGVfb3JkZXJfc3RhdHVzJyxcblx0XHRcdCdCVVRUT05fTVVMVElfREVMRVRFJzogJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuYnRuLW11bHRpX2RlbGV0ZScsXG5cdFx0XHQnQlVUVE9OX0hFUk1FUyc6ICcuY29udGVudFRhYmxlIC5pbmZvQm94Q29udGVudCBhLmJ0bi1oZXJtZXMnLFxuXHRcdFx0J2dldF9sYWJlbHMnOiAnI2lsb3h4X29yZGVycydcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfZ2V0QWN0aW9uQ2FsbGJhY2sgPSBmdW5jdGlvbihhY3Rpb24pIHtcblx0XHRcdHN3aXRjaCAoYWN0aW9uKSB7XG5cdFx0XHRcdGNhc2UgJ1RFWFRfU0hPVyc6XG5cdFx0XHRcdFx0cmV0dXJuIF9zaG93T3JkZXJDYWxsYmFjaztcblx0XHRcdFx0Y2FzZSAnVEVYVF9HTV9TVEFUVVMnOlxuXHRcdFx0XHRcdHJldHVybiBfY2hhbmdlT3JkZXJTdGF0dXNDYWxsYmFjaztcblx0XHRcdFx0Y2FzZSAnZGVsZXRlJzpcblx0XHRcdFx0XHRyZXR1cm4gX2RlbGV0ZUNhbGxiYWNrO1xuXHRcdFx0XHRjYXNlICdCVVRUT05fR01fQ0FOQ0VMJzpcblx0XHRcdFx0XHRyZXR1cm4gX2NhbmNlbENhbGxiYWNrO1xuXHRcdFx0XHRjYXNlICdCVVRUT05fQ1JFQVRFX0lOVk9JQ0UnOlxuXHRcdFx0XHRcdHJldHVybiBfaW52b2ljZUNhbGxiYWNrO1xuXHRcdFx0XHRjYXNlICdUSVRMRV9JTlZPSUNFX01BSUwnOlxuXHRcdFx0XHRcdHJldHVybiBfZW1haWxJbnZvaWNlQ2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ0JVVFRPTl9DUkVBVEVfUEFDS0lOR19TTElQJzpcblx0XHRcdFx0XHRyZXR1cm4gX3BhY2tpbmdTbGlwQ2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ1RJVExFX09SREVSJzpcblx0XHRcdFx0XHRyZXR1cm4gX29yZGVyQ29uZmlybWF0aW9uQ2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ1RJVExFX1JFQ1JFQVRFX09SREVSJzpcblx0XHRcdFx0XHRyZXR1cm4gX3JlY3JlYXRlT3JkZXJDb25maXJtYXRpb25DYWxsYmFjaztcblx0XHRcdFx0Y2FzZSAnVElUTEVfU0VORF9PUkRFUic6XG5cdFx0XHRcdFx0cmV0dXJuIF9zZW5kT3JkZXJDb25maXJtYXRpb25DYWxsYmFjaztcblx0XHRcdFx0Y2FzZSAnVEVYVF9DUkVBVEVfV0lUSERSQVdBTCc6XG5cdFx0XHRcdFx0cmV0dXJuIF93aXRoZHJhd2FsQ2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ1RYVF9QQVJDRUxfVFJBQ0tJTkdfU0VOREJVVFRPTl9USVRMRSc6XG5cdFx0XHRcdFx0cmV0dXJuIF9hZGRUcmFja2luZ0NvZGVDYWxsYmFjaztcblx0XHRcdFx0Y2FzZSAnQlVUVE9OX0RITF9MQUJFTCc6XG5cdFx0XHRcdFx0cmV0dXJuIF9kaGxMYWJlbENhbGxiYWNrO1xuXHRcdFx0XHRjYXNlICdNQUlMQkVFWl9PVkVSVklFVyc6XG5cdFx0XHRcdFx0cmV0dXJuIF9tYWlsQmVlek92ZXJ2aWV3Q2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ01BSUxCRUVaX05PVElGSUNBVElPTlMnOlxuXHRcdFx0XHRcdHJldHVybiBfbWFpbEJlZXpOb3RpZmljYXRpb25zQ2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ01BSUxCRUVaX0NPTlZFUlNBVElPTlMnOlxuXHRcdFx0XHRcdHJldHVybiBfbWFpbEJlZXpDb252ZXJzYXRpb25zQ2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ0JVVFRPTl9NVUxUSV9DQU5DRUwnOlxuXHRcdFx0XHRcdHJldHVybiBfbXVsdGlDYW5jZWxDYWxsYmFjaztcblx0XHRcdFx0Y2FzZSAnQlVUVE9OX01VTFRJX0NIQU5HRV9PUkRFUl9TVEFUVVMnOlxuXHRcdFx0XHRcdHJldHVybiBfbXVsdGlDaGFuZ2VPcmRlclN0YXR1c0NhbGxiYWNrO1xuXHRcdFx0XHRjYXNlICdCVVRUT05fTVVMVElfREVMRVRFJzpcblx0XHRcdFx0XHRyZXR1cm4gX211bHRpRGVsZXRlQ2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ0JVVFRPTl9IRVJNRVMnOlxuXHRcdFx0XHRcdHJldHVybiBfaGVybWVzQ2FsbGJhY2s7XG5cdFx0XHRcdGNhc2UgJ2dldF9sYWJlbHMnOlxuXHRcdFx0XHRcdHJldHVybiBfaWxveHhDYWxsYmFjaztcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfc2hvd09yZGVyQ2FsbGJhY2sgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0dmFyIG9yZGVySWQgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cygndHInKS5kYXRhKCdyb3ctaWQnKTtcblx0XHRcdHZhciB1cmwgPSAkKF9zZWxlY3Rvck1hcHBpbmcuVEVYVF9TSE9XKS5hdHRyKCdocmVmJyk7XG5cdFx0XHR3aW5kb3cub3Blbih1cmwucmVwbGFjZSgvb0lEPSguKikmLywgJ29JRD0nICsgb3JkZXJJZCArICcmJyksICdfc2VsZicpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9jaGFuZ2VPcmRlclN0YXR1c0NhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBvcmRlcklkID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoJ3RyJykuZGF0YSgncm93LWlkJyk7XG5cdFx0XHQkKCcjZ21fb3JkZXJfaWQnKS52YWwob3JkZXJJZCk7XG5cdFx0XHQkKCcuZ3gtb3JkZXJzLXRhYmxlIC5zaW5nbGUtY2hlY2tib3gnKS5yZW1vdmVDbGFzcygnY2hlY2tlZCcpO1xuXHRcdFx0JCgnLmd4LW9yZGVycy10YWJsZSBpbnB1dDpjaGVja2JveCcpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHQkKGV2ZW50LnRhcmdldCkucGFyZW50cygndHInKS5lcSgwKS5maW5kKCcuc2luZ2xlLWNoZWNrYm94JykuYWRkQ2xhc3MoJ2NoZWNrZWQnKTtcblx0XHRcdCQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmVxKDApLmZpbmQoJ2lucHV0OmNoZWNrYm94JykucHJvcCgnY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0JChfc2VsZWN0b3JNYXBwaW5nLlRFWFRfR01fU1RBVFVTKS5jbGljaygpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9kZWxldGVDYWxsYmFjayA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgb3JkZXJJZCA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmRhdGEoJ3Jvdy1pZCcpO1xuXHRcdFx0dmFyICRkZWxldGUgPSAkKF9zZWxlY3Rvck1hcHBpbmcuZGVsZXRlKTtcblx0XHRcdCRkZWxldGUuZGF0YSgnb3JkZXJfaWQnLCBvcmRlcklkKTtcblx0XHRcdCRkZWxldGUuZ2V0KDApLmNsaWNrKCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2NhbmNlbENhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBvcmRlcklkID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoJ3RyJykuZGF0YSgncm93LWlkJyk7XG5cdFx0XHQkKCcjZ21fb3JkZXJfaWQnKS52YWwob3JkZXJJZCk7XG5cdFx0XHQkKCcuZ3gtb3JkZXJzLXRhYmxlIC5zaW5nbGUtY2hlY2tib3gnKS5yZW1vdmVDbGFzcygnY2hlY2tlZCcpO1xuXHRcdFx0JCgnLmd4LW9yZGVycy10YWJsZSBpbnB1dDpjaGVja2JveCcpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHQkKGV2ZW50LnRhcmdldCkucGFyZW50cygndHInKS5lcSgwKS5maW5kKCcuc2luZ2xlLWNoZWNrYm94JykuYWRkQ2xhc3MoJ2NoZWNrZWQnKTtcblx0XHRcdCQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmVxKDApLmZpbmQoJ2lucHV0OmNoZWNrYm94JykucHJvcCgnY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0JChfc2VsZWN0b3JNYXBwaW5nLkJVVFRPTl9NVUxUSV9DQU5DRUwpLmNsaWNrKCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2ludm9pY2VDYWxsYmFjayA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgb3JkZXJJZCA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmRhdGEoJ3Jvdy1pZCcpO1xuXHRcdFx0dmFyIHVybCA9ICQoX3NlbGVjdG9yTWFwcGluZy5CVVRUT05fQ1JFQVRFX0lOVk9JQ0UpLmF0dHIoJ2hyZWYnKTtcblx0XHRcdHdpbmRvdy5vcGVuKHVybC5yZXBsYWNlKC9vSUQ9KC4qKSYvLCAnb0lEPScgKyBvcmRlcklkICsgJyYnKSwgJ19ibGFuaycpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9lbWFpbEludm9pY2VDYWxsYmFjayA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgb3JkZXJJZCA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmRhdGEoJ3Jvdy1pZCcpO1xuXHRcdFx0JCgnI2dtX29yZGVyX2lkJykudmFsKG9yZGVySWQpO1xuXHRcdFx0JCgnLkdNX0lOVk9JQ0VfTUFJTCcpLmNsaWNrKCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX3BhY2tpbmdTbGlwQ2FsbGJhY2sgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0dmFyIG9yZGVySWQgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cygndHInKS5kYXRhKCdyb3ctaWQnKTtcblx0XHRcdHZhciB1cmwgPSAkKF9zZWxlY3Rvck1hcHBpbmcuQlVUVE9OX0NSRUFURV9QQUNLSU5HX1NMSVApLmF0dHIoJ2hyZWYnKTtcblx0XHRcdHdpbmRvdy5vcGVuKHVybC5yZXBsYWNlKC9vSUQ9KC4qKSYvLCAnb0lEPScgKyBvcmRlcklkICsgJyYnKSwgJ19ibGFuaycpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9vcmRlckNvbmZpcm1hdGlvbkNhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBvcmRlcklkID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoJ3RyJykuZGF0YSgncm93LWlkJyk7XG5cdFx0XHR2YXIgdXJsID0gJChfc2VsZWN0b3JNYXBwaW5nLlRJVExFX09SREVSKS5hdHRyKCdocmVmJyk7XG5cdFx0XHR3aW5kb3cub3Blbih1cmwucmVwbGFjZSgvb0lEPSguKikmLywgJ29JRD0nICsgb3JkZXJJZCArICcmJyksICdfYmxhbmsnKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfcmVjcmVhdGVPcmRlckNvbmZpcm1hdGlvbkNhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBvcmRlcklkID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoJ3RyJykuZGF0YSgncm93LWlkJyk7XG5cdFx0XHR2YXIgdXJsID0gJChfc2VsZWN0b3JNYXBwaW5nLlRJVExFX1JFQ1JFQVRFX09SREVSKS5hdHRyKCdocmVmJyk7XG5cdFx0XHR3aW5kb3cub3Blbih1cmwucmVwbGFjZSgvb0lEPSguKikmLywgJ29JRD0nICsgb3JkZXJJZCArICcmJyksICdfYmxhbmsnKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfc2VuZE9yZGVyQ29uZmlybWF0aW9uQ2FsbGJhY2sgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0dmFyIG9yZGVySWQgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cygndHInKS5kYXRhKCdyb3ctaWQnKTtcblx0XHRcdCQoJyNnbV9vcmRlcl9pZCcpLnZhbChvcmRlcklkKTtcblx0XHRcdCQoJy5HTV9TRU5EX09SREVSJykuY2xpY2soKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfd2l0aGRyYXdhbENhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBvcmRlcklkID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoJ3RyJykuZGF0YSgncm93LWlkJyk7XG5cdFx0XHR2YXIgdXJsID0gJChfc2VsZWN0b3JNYXBwaW5nLlRFWFRfQ1JFQVRFX1dJVEhEUkFXQUwpLmF0dHIoJ2hyZWYnKTtcblx0XHRcdHdpbmRvdy5vcGVuKHVybC5yZXBsYWNlKC9vcmRlcj1bXiZdKi8sICdvcmRlcl9pZD0nICsgb3JkZXJJZCksICdfYmxhbmsnKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfYWRkVHJhY2tpbmdDb2RlQ2FsbGJhY2sgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0dmFyIG9yZGVySWQgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cygndHInKS5kYXRhKCdyb3ctaWQnKTtcblx0XHRcdHZhciAkdGFyZ2V0ID0gJChfc2VsZWN0b3JNYXBwaW5nLlRYVF9QQVJDRUxfVFJBQ0tJTkdfU0VOREJVVFRPTl9USVRMRSk7XG5cdFx0XHQkdGFyZ2V0LmRhdGEoJ29yZGVyX2lkJywgb3JkZXJJZCk7XG5cdFx0XHQkdGFyZ2V0LmdldCgwKS5jbGljaygpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9kaGxMYWJlbENhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBvcmRlcklkID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoJ3RyJykuZGF0YSgncm93LWlkJyk7XG5cdFx0XHR2YXIgdXJsID0gJChfc2VsZWN0b3JNYXBwaW5nLkJVVFRPTl9ESExfTEFCRUwpLmF0dHIoJ2hyZWYnKTtcblx0XHRcdHdpbmRvdy5vcGVuKHVybC5yZXBsYWNlKC9vSUQ9KC4qKS8sICdvSUQ9JyArIG9yZGVySWQpLCAnX2JsYW5rJyk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX21haWxCZWV6T3ZlcnZpZXdDYWxsYmFjayA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgb3JkZXJJZCA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmRhdGEoJ3Jvdy1pZCcpO1xuXHRcdFx0dmFyICR0YXJnZXQgPSAkKF9zZWxlY3Rvck1hcHBpbmcuTUFJTEJFRVpfT1ZFUlZJRVcpO1xuXHRcdFx0dmFyIHVybCA9ICR0YXJnZXQuYXR0cignb25jbGljaycpO1xuXHRcdFx0dXJsID0gdXJsLnJlcGxhY2UoL29JRD0oLiopJi8sICdvSUQ9JyArIG9yZGVySWQgKyAnJicpO1xuXHRcdFx0JHRhcmdldC5hdHRyKCdvbmNsaWNrJywgdXJsKTtcblx0XHRcdCR0YXJnZXQuZ2V0KDApLmNsaWNrKCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX21haWxCZWV6Tm90aWZpY2F0aW9uc0NhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBvcmRlcklkID0gJChldmVudC50YXJnZXQpLnBhcmVudHMoJ3RyJykuZGF0YSgncm93LWlkJyk7XG5cdFx0XHR2YXIgJHRhcmdldCA9ICQoX3NlbGVjdG9yTWFwcGluZy5NQUlMQkVFWl9OT1RJRklDQVRJT05TKTtcblx0XHRcdHZhciB1cmwgPSAkdGFyZ2V0LmF0dHIoJ29uY2xpY2snKTtcblx0XHRcdHVybCA9IHVybC5yZXBsYWNlKC9vSUQ9KC4qKSYvLCAnb0lEPScgKyBvcmRlcklkICsgJyYnKTtcblx0XHRcdCR0YXJnZXQuYXR0cignb25jbGljaycsIHVybCk7XG5cdFx0XHQkdGFyZ2V0LmdldCgwKS5jbGljaygpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9tYWlsQmVlekNvbnZlcnNhdGlvbnNDYWxsYmFjayA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgb3JkZXJJZCA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmRhdGEoJ3Jvdy1pZCcpO1xuXHRcdFx0dmFyICR0YXJnZXQgPSAkKF9zZWxlY3Rvck1hcHBpbmcuTUFJTEJFRVpfQ09OVkVSU0FUSU9OUyk7XG5cdFx0XHR2YXIgdXJsID0gJHRhcmdldC5hdHRyKCdvbmNsaWNrJyk7XG5cdFx0XHR1cmwgPSB1cmwucmVwbGFjZSgvb0lEPSguKikmLywgJ29JRD0nICsgb3JkZXJJZCArICcmJyk7XG5cdFx0XHQkdGFyZ2V0LmF0dHIoJ29uY2xpY2snLCB1cmwpO1xuXHRcdFx0JHRhcmdldC5nZXQoMCkuY2xpY2soKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfaGVybWVzQ2FsbGJhY2sgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0dmFyIG9yZGVySWQgPSAkKGV2ZW50LnRhcmdldCkucGFyZW50cygndHInKS5kYXRhKCdyb3ctaWQnKTtcblx0XHRcdHZhciAkdGFyZ2V0ID0gJChfc2VsZWN0b3JNYXBwaW5nLkJVVFRPTl9IRVJNRVMpO1xuXHRcdFx0dmFyIHVybCA9ICR0YXJnZXQuYXR0cignaHJlZicpO1xuXHRcdFx0dXJsID0gdXJsLnJlcGxhY2UoL29yZGVyc19pZD0oLiopLywgJ29yZGVyc19pZD0nICsgb3JkZXJJZCk7XG5cdFx0XHQkdGFyZ2V0LmF0dHIoJ2hyZWYnLCB1cmwpO1xuXHRcdFx0JHRhcmdldC5nZXQoMCkuY2xpY2soKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfaWxveHhDYWxsYmFjayA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHR2YXIgJHRhcmdldCA9ICQoX3NlbGVjdG9yTWFwcGluZy5nZXRfbGFiZWxzKTtcblx0XHRcdCR0YXJnZXQuY2xpY2soKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfbXVsdGlDaGFuZ2VPcmRlclN0YXR1c0NhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdCQoX3NlbGVjdG9yTWFwcGluZy5CVVRUT05fTVVMVElfQ0hBTkdFX09SREVSX1NUQVRVUykuZ2V0KDApLmNsaWNrKCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX211bHRpRGVsZXRlQ2FsbGJhY2sgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0JChfc2VsZWN0b3JNYXBwaW5nLkJVVFRPTl9NVUxUSV9ERUxFVEUpLmdldCgwKS5jbGljaygpO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9tdWx0aUNhbmNlbENhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdCQoX3NlbGVjdG9yTWFwcGluZy5CVVRUT05fTVVMVElfQ0FOQ0VMKS5nZXQoMCkuY2xpY2soKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1hcCB0YWJsZSBhY3Rpb25zIHRvIGJvdHRvbSBkcm9wZG93biBidXR0b24uXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfbWFwVGFibGVBY3Rpb25zID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJGRyb3Bkb3duID0gJCgnI29yZGVycy10YWJsZS1kcm9wZG93bicpO1xuXHRcdFx0XG5cdFx0XHRfYmluZEV2ZW50SGFuZGxlcigkZHJvcGRvd24sICdCVVRUT05fTVVMVElfQ0hBTkdFX09SREVSX1NUQVRVUycpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJChfc2VsZWN0b3JNYXBwaW5nLmdldF9sYWJlbHMpLmxlbmd0aCkge1xuXHRcdFx0XHRfYmluZEV2ZW50SGFuZGxlcigkZHJvcGRvd24sICdnZXRfbGFiZWxzJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdF9iaW5kRXZlbnRIYW5kbGVyKCRkcm9wZG93biwgJ0JVVFRPTl9NVUxUSV9ERUxFVEUnKTtcblx0XHRcdF9iaW5kRXZlbnRIYW5kbGVyKCRkcm9wZG93biwgJ0JVVFRPTl9NVUxUSV9DQU5DRUwnKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1hcCBhY3Rpb25zIGZvciBldmVyeSByb3cgaW4gdGhlIHRhYmxlIGdlbmVyaWNhbGx5LlxuXHRcdCAqXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCB1c2UgdGhlIGFjdGlvbl9tYXBwZXIgbGlicmFyeSB0byBtYXAgdGhlIGFjdGlvbnMgZm9yIGVhY2hcblx0XHQgKiByb3cgb2YgdGhlIHRhYmxlLiBJdCBtYXBzIG9ubHkgdGhvc2UgYnV0dG9ucywgdGhhdCBoYXZlbid0IGFscmVhZHkgZXhwbGljaXRseVxuXHRcdCAqIG1hcHBlZCBieSB0aGUgX21hcFJvd0FjdGlvbnMgZnVuY3Rpb24uXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfbWFwVW5tYXBwZWRSb3dBY3Rpb25zID0gZnVuY3Rpb24oJHRoaXMpIHtcblx0XHRcdHZhciB1bm1hcHBlZFJvd0FjdGlvbnMgPSBbXTtcblx0XHRcdCQoJy5hY3Rpb25fYnV0dG9ucyAuZXh0ZW5kZWRfc2luZ2xlX2FjdGlvbnMgYSwnICtcblx0XHRcdFx0Jy5hY3Rpb25fYnV0dG9ucyAuZXh0ZW5kZWRfc2luZ2xlX2FjdGlvbnMgYnV0dG9uLCcgK1xuXHRcdFx0XHQnLmFjdGlvbl9idXR0b25zIC5leHRlbmRlZF9zaW5nbGVfYWN0aW9ucyBpbnB1dFt0eXBlPVwiYnV0dG9uXCJdLCcgK1xuXHRcdFx0XHQnLmFjdGlvbl9idXR0b25zIC5leHRlbmRlZF9zaW5nbGVfYWN0aW9ucyBpbnB1dFt0eXBlPVwic3VibWl0XCJdJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKCFfYWxyZWFkeU1hcHBlZCgkKHRoaXMpKSkge1xuXHRcdFx0XHRcdHVubWFwcGVkUm93QWN0aW9ucy5wdXNoKCQodGhpcykpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0dmFyIG9yZGVySWQgPSAkdGhpcy5kYXRhKCdyb3ctaWQnKSxcblx0XHRcdFx0JGRyb3Bkb3duID0gJHRoaXMuZmluZCgnLmpzLWJ1dHRvbi1kcm9wZG93bicpO1xuXHRcdFx0XG5cdFx0XHQkLmVhY2godW5tYXBwZWRSb3dBY3Rpb25zLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyICRidXR0b24gPSAkKHRoaXMpO1xuXHRcdFx0XHR2YXIgY2FsbGJhY2sgPSBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRpZiAoJGJ1dHRvbi5wcm9wKCdocmVmJykgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRcdFx0JGJ1dHRvbi5wcm9wKCdocmVmJywgJGJ1dHRvbi5wcm9wKCdocmVmJykucmVwbGFjZSgvb0lEPSguKilcXGQoPz0mKT8vLCAnb0lEPScgKyBvcmRlcklkKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdCRidXR0b24uZ2V0KDApLmNsaWNrKCk7XG5cdFx0XHRcdH07XG5cdFx0XHRcdFxuXHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24ubWFwQWN0aW9uKCRkcm9wZG93biwgJGJ1dHRvbi50ZXh0KCksICcnLCBjYWxsYmFjayk7XG5cdFx0XHRcdG1hcHBlZEJ1dHRvbnMucHVzaCgkYnV0dG9uKTtcblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9tYXBVbm1hcHBlZE11bHRpQWN0aW9ucyA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIHVubWFwcGVkTXVsdGlBY3Rpb25zID0gW107XG5cdFx0XHQkKCcuYWN0aW9uX2J1dHRvbnMgLmV4dGVuZGVkX211bHRpX2FjdGlvbnMgYSwnICtcblx0XHRcdFx0Jy5hY3Rpb25fYnV0dG9ucyAuZXh0ZW5kZWRfbXVsdGlfYWN0aW9ucyBidXR0b24sJyArXG5cdFx0XHRcdCcuYWN0aW9uX2J1dHRvbnMgLmV4dGVuZGVkX211bHRpX2FjdGlvbnMgaW5wdXRbdHlwZT1cImJ1dHRvblwiXSwnICtcblx0XHRcdFx0Jy5hY3Rpb25fYnV0dG9ucyAuZXh0ZW5kZWRfbXVsdGlfYWN0aW9ucyBpbnB1dFt0eXBlPVwic3VibWl0XCJdJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKCFfYWxyZWFkeU1hcHBlZCgkKHRoaXMpKSkge1xuXHRcdFx0XHRcdHVubWFwcGVkTXVsdGlBY3Rpb25zLnB1c2goJCh0aGlzKSk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHR2YXIgJGRyb3Bkb3duID0gJCgnI29yZGVycy10YWJsZS1kcm9wZG93bicpO1xuXHRcdFx0JC5lYWNoKHVubWFwcGVkTXVsdGlBY3Rpb25zLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyICRidXR0b24gPSAkKHRoaXMpO1xuXHRcdFx0XHR2YXIgY2FsbGJhY2sgPSBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHQkYnV0dG9uLmdldCgwKS5jbGljaygpO1xuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLm1hcEFjdGlvbigkZHJvcGRvd24sICRidXR0b24udGV4dCgpLCAnJywgY2FsbGJhY2spO1xuXHRcdFx0XHRtYXBwZWRCdXR0b25zLnB1c2goJGJ1dHRvbik7XG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENoZWNrcyBpZiB0aGUgYnV0dG9uIHdhcyBhbHJlYWR5IG1hcHBlZFxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX2FscmVhZHlNYXBwZWQgPSBmdW5jdGlvbigkYnV0dG9uKSB7XG5cdFx0XHRmb3IgKHZhciBpbmRleCBpbiBtYXBwZWRCdXR0b25zKSB7XG5cdFx0XHRcdGlmICgkYnV0dG9uLmlzKG1hcHBlZEJ1dHRvbnNbaW5kZXhdKSkge1xuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBBZGQgQnV0dG9uIHRvIE1hcHBlZCBBcnJheVxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIGJ1dHRvblNlbGVjdG9yXG5cdFx0ICogQHJldHVybnMge2Jvb2xlYW59XG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfYWRkQnV0dG9uVG9NYXBwZWRBcnJheSA9IGZ1bmN0aW9uKGJ1dHRvblNlbGVjdG9yKSB7XG5cdFx0XHRpZiAobWFwcGVkQnV0dG9uc1tidXR0b25TZWxlY3Rvcl0gIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdH1cblx0XHRcdG1hcHBlZEJ1dHRvbnNbYnV0dG9uU2VsZWN0b3JdID0gJChidXR0b25TZWxlY3Rvcik7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBCaW5kIEV2ZW50IGhhbmRsZXJcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSAkZHJvcGRvd25cblx0XHQgKiBAcGFyYW0gYWN0aW9uXG5cdFx0ICogQHBhcmFtIGN1c3RvbVJlY2VudEJ1dHRvblNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfYmluZEV2ZW50SGFuZGxlciA9IGZ1bmN0aW9uKCRkcm9wZG93biwgYWN0aW9uLCBjdXN0b21SZWNlbnRCdXR0b25TZWxlY3Rvcikge1xuXHRcdFx0dmFyIHRhcmdldFNlbGVjdG9yID0gX3NlbGVjdG9yTWFwcGluZ1thY3Rpb25dLFxuXHRcdFx0XHRzZWN0aW9uID0gX3NlY3Rpb25NYXBwaW5nW2FjdGlvbl0sXG5cdFx0XHRcdGNhbGxiYWNrID0gX2dldEFjdGlvbkNhbGxiYWNrKGFjdGlvbiksXG5cdFx0XHRcdGN1c3RvbUVsZW1lbnQgPSAkKGN1c3RvbVJlY2VudEJ1dHRvblNlbGVjdG9yKS5sZW5ndGggPyAkKGN1c3RvbVJlY2VudEJ1dHRvblNlbGVjdG9yKSA6XG5cdFx0XHRcdCAgICAgICAgICAgICAgICAkZHJvcGRvd247XG5cdFx0XHRpZiAoJCh0YXJnZXRTZWxlY3RvcikubGVuZ3RoKSB7XG5cdFx0XHRcdF9hZGRCdXR0b25Ub01hcHBlZEFycmF5KHRhcmdldFNlbGVjdG9yKTtcblx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLm1hcEFjdGlvbigkZHJvcGRvd24sIGFjdGlvbiwgc2VjdGlvbiwgY2FsbGJhY2ssIGN1c3RvbUVsZW1lbnQpO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRml4IGZvciByb3cgc2VsZWN0aW9uIGNvbnRyb2xzLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX2ZpeFJvd1NlbGVjdGlvbkZvckNvbnRyb2xFbGVtZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0JCgnaW5wdXQuY2hlY2tib3hbbmFtZT1cImdtX211bHRpX3N0YXR1c1tdXCJdJylcblx0XHRcdFx0LmFkZCgnLnNpbmdsZS1jaGVja2JveCcpXG5cdFx0XHRcdC5hZGQoJ2EuYWN0aW9uLWljb24nKVxuXHRcdFx0XHQuYWRkKCcuanMtYnV0dG9uLWRyb3Bkb3duJylcblx0XHRcdFx0LmFkZCgndHIuZGF0YVRhYmxlUm93IGEnKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFx0XHRfdG9nZ2xlTXVsdGlBY3Rpb25CdXR0b24oKTtcblx0XHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0Ly8gV2FpdCB1bnRpbCB0aGUgYnV0dG9ucyBhcmUgY29udmVydGVkIHRvIGRyb3Bkb3duIGZvciBldmVyeSByb3cuXG5cdFx0XHR2YXIgaW50ZXJ2YWwgPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKCQoJy5qcy1idXR0b24tZHJvcGRvd24nKS5sZW5ndGgpIHtcblx0XHRcdFx0XHRjbGVhckludGVydmFsKGludGVydmFsKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRfbWFwVGFibGVBY3Rpb25zKCk7XG5cdFx0XHRcdFx0X21hcFVubWFwcGVkTXVsdGlBY3Rpb25zKCk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0dmFyIHRhYmxlQWN0aW9ucyA9IG1hcHBlZEJ1dHRvbnM7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gUmVtb3ZlIE1haWxiZWV6IGNvbnZlcnNhdGlvbnMgYmFkZ2UuXG5cdFx0XHRcdFx0X2FkZEJ1dHRvblRvTWFwcGVkQXJyYXkoJy5jb250ZW50VGFibGUgLmluZm9Cb3hDb250ZW50IGEuY29udGV4dF92aWV3X2J1dHRvbi5idG5fcmlnaHQnKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkKCcuZ3gtb3JkZXJzLXRhYmxlIHRyJykubm90KCcuZGF0YVRhYmxlSGVhZGluZ1JvdycpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRtYXBwZWRCdXR0b25zID0gW107XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGZvciAodmFyIGluZGV4IGluIHRhYmxlQWN0aW9ucykge1xuXHRcdFx0XHRcdFx0XHRtYXBwZWRCdXR0b25zW2luZGV4XSA9IHRhYmxlQWN0aW9uc1tpbmRleF07XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdF9tYXBSb3dBY3Rpb24oJCh0aGlzKSk7XG5cdFx0XHRcdFx0XHRfbWFwVW5tYXBwZWRSb3dBY3Rpb25zKCQodGhpcykpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdF9maXhSb3dTZWxlY3Rpb25Gb3JDb250cm9sRWxlbWVudHMoKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBJbml0aWFsaXplIGNoZWNrYm94ZXNcblx0XHRcdFx0XHRfdG9nZ2xlTXVsdGlBY3Rpb25CdXR0b24oKTtcblx0XHRcdFx0fVxuXHRcdFx0fSwgMzAwKTtcblx0XHRcdFxuXHRcdFx0Ly8gQ2hlY2sgZm9yIHNlbGVjdGVkIGNoZWNrYm94ZXMgYWxzb1xuXHRcdFx0Ly8gYmVmb3JlIGFsbCByb3dzIGFuZCB0aGVpciBkcm9wZG93biB3aWRnZXRzIGhhdmUgYmVlbiBpbml0aWFsaXplZC5cblx0XHRcdF90b2dnbGVNdWx0aUFjdGlvbkJ1dHRvbigpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
