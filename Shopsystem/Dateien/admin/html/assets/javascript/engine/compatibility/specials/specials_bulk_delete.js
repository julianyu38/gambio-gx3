'use strict';

/* --------------------------------------------------------------
 specials_bulk_delete.js 2018-04-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Specials Bulk Delete Module
 *
 * This module handels the bulk deletion for the specials.
 *
 * @module Compatibility/specials_bulk_delete
 */
gx.compatibility.module('specials_bulk_delete', [],

/**  @lends module:Compatibility/specials_bulk_delete */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Module Reference
  *
  * @var {object}
  */

	var $this = $(this);

	/**
  * Bulk delete modal object
  *
  * @type {object}
  */
	var $modal = $('#modal_layer_container');

	/**
  * Bulk delete form object
  *
  * @type {object}
  */
	var $form = $('#bulk_delete_confirm_form');

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final Options
  *
  * @var {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ------------------------------------------------------------------------


	/**
  * Handler for the click event, that's triggered by the delete button.
  */
	var _handleDeleteButton = function _handleDeleteButton(event) {
		event.preventDefault();

		$form.find('.products-to-delete').html('');
		$.each($this.find('.delete-special:checked'), _addSpecialsToModal);

		$form.dialog({
			'title': jse.core.lang.translate('TEXT_INFO_HEADING_BULK_DELETE_SPECIALS', 'admin_specials'),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($form),
			'width': 420
		});
	};

	/**
  * Returns the modal buttons, that are used in the delete confirmation modal.
  *
  * @return {object} Buttons fpr the delete confirmation modal
  */
	var _getModalButtons = function _getModalButtons($form) {
		return [{
			'text': jse.core.lang.translate('close', 'buttons'),
			'class': 'btn',
			'click': function click() {
				$(this).dialog('close');
			}
		}, {
			'text': jse.core.lang.translate('delete', 'buttons'),
			'class': 'btn btn-primary',
			'click': function click() {
				$form.submit();
			}
		}];
	};

	/**
  * Adds the informations of the selected specials to the delete confirmation modal.
  */
	var _addSpecialsToModal = function _addSpecialsToModal(key, checkbox) {
		$form.find('.products-to-delete').append($('<li/>', {
			'text': $(checkbox).data('product-name')
		}));

		$form.find('.products-to-delete').append($('<input/>', {
			'name': 'deleteSpecial[]',
			'value': $(checkbox).data('special-id')
		}).hide());
	};

	/**
  * Handler for the click event, that's triggered by a normal checkbox in the specials overview table.
  */
	var _handleNormalCheckbox = function _handleNormalCheckbox() {
		var numChecked = $this.find('.delete-special:checked').length;
		var numNotChecked = $this.find('.delete-special').length;

		if (numChecked === numNotChecked && numChecked > 0) {
			$this.find('.select-all-checkbox').prop('checked', true);
			$this.find('.select-all-checkbox').closest('.single-checkbox').addClass('checked');
		} else {
			$this.find('.select-all-checkbox').prop('checked', false);
			$this.find('.select-all-checkbox').closest('.single-checkbox').removeClass('checked');
		}

		if (numChecked === 0) {
			$this.find('.bulk-delete').prop('disabled', true);
		} else {
			$this.find('.bulk-delete').prop('disabled', false);
		}
	};

	/**
  * Handler for the click event, that's triggered by the checkbox in the head of the specials overview table.
  */
	var _handleSelectAllCheckbox = function _handleSelectAllCheckbox() {
		var allChecked = $this.find('.select-all-checkbox').is(":checked");

		if (allChecked) {
			$this.find('.delete-special').prop('checked', true);
			$this.find('.delete-special').parent('.single-checkbox').addClass('checked');
		} else {
			$this.find('.delete-special').prop('checked', false);
			$this.find('.delete-special').parent('.single-checkbox').removeClass('checked');
		}

		if ($this.find('.select-all-checkbox').is(":checked") && $this.find('.delete-special:checked').length > 0) {
			$this.find('.bulk-delete').prop('disabled', false);
		} else {
			$this.find('.bulk-delete').prop('disabled', true);
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.find('.select-all-checkbox').on('click', _handleSelectAllCheckbox);
		$this.find('.delete-special').on('click', _handleNormalCheckbox);
		$this.find('.bulk-delete').on('click', _handleDeleteButton);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNwZWNpYWxzL3NwZWNpYWxzX2J1bGtfZGVsZXRlLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkbW9kYWwiLCIkZm9ybSIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9oYW5kbGVEZWxldGVCdXR0b24iLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiZmluZCIsImh0bWwiLCJlYWNoIiwiX2FkZFNwZWNpYWxzVG9Nb2RhbCIsImRpYWxvZyIsImpzZSIsImNvcmUiLCJsYW5nIiwidHJhbnNsYXRlIiwiX2dldE1vZGFsQnV0dG9ucyIsInN1Ym1pdCIsImtleSIsImNoZWNrYm94IiwiYXBwZW5kIiwiaGlkZSIsIl9oYW5kbGVOb3JtYWxDaGVja2JveCIsIm51bUNoZWNrZWQiLCJsZW5ndGgiLCJudW1Ob3RDaGVja2VkIiwicHJvcCIsImNsb3Nlc3QiLCJhZGRDbGFzcyIsInJlbW92ZUNsYXNzIiwiX2hhbmRsZVNlbGVjdEFsbENoZWNrYm94IiwiYWxsQ2hlY2tlZCIsImlzIiwicGFyZW50IiwiaW5pdCIsImRvbmUiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCLENBQ0Msc0JBREQsRUFHQyxFQUhEOztBQUtDOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFNBQVNELEVBQUUsd0JBQUYsQ0FBZjs7QUFFQTs7Ozs7QUFLQSxLQUFNRSxRQUFRRixFQUFFLDJCQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUcsV0FBVyxFQUFqQjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxVQUFVSixFQUFFSyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCTCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTs7O0FBR0EsS0FBTVMsc0JBQXNCLFNBQXRCQSxtQkFBc0IsQ0FBQ0MsS0FBRCxFQUFXO0FBQ3RDQSxRQUFNQyxjQUFOOztBQUVBTixRQUFNTyxJQUFOLENBQVcscUJBQVgsRUFBa0NDLElBQWxDLENBQXVDLEVBQXZDO0FBQ0FWLElBQUVXLElBQUYsQ0FBT1osTUFBTVUsSUFBTixDQUFXLHlCQUFYLENBQVAsRUFBOENHLG1CQUE5Qzs7QUFFQVYsUUFBTVcsTUFBTixDQUFhO0FBQ1osWUFBU0MsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isd0NBQXhCLEVBQWtFLGdCQUFsRSxDQURHO0FBRVosWUFBUyxJQUZHO0FBR1osa0JBQWUsY0FISDtBQUlaLGNBQVdDLGlCQUFpQmhCLEtBQWpCLENBSkM7QUFLWixZQUFTO0FBTEcsR0FBYjtBQU9BLEVBYkQ7O0FBZUE7Ozs7O0FBS0EsS0FBTWdCLG1CQUFtQixTQUFuQkEsZ0JBQW1CLENBQUNoQixLQUFELEVBQVc7QUFDbkMsU0FBTyxDQUNOO0FBQ0MsV0FBUVksSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsU0FBakMsQ0FEVDtBQUVDLFlBQVMsS0FGVjtBQUdDLFlBQVMsaUJBQVc7QUFDbkJqQixNQUFFLElBQUYsRUFBUWEsTUFBUixDQUFlLE9BQWY7QUFDQTtBQUxGLEdBRE0sRUFRTjtBQUNDLFdBQVFDLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFFBQXhCLEVBQWtDLFNBQWxDLENBRFQ7QUFFQyxZQUFTLGlCQUZWO0FBR0MsWUFBUyxpQkFBVztBQUNuQmYsVUFBTWlCLE1BQU47QUFDQTtBQUxGLEdBUk0sQ0FBUDtBQWdCQSxFQWpCRDs7QUFtQkE7OztBQUdBLEtBQU1QLHNCQUFzQixTQUF0QkEsbUJBQXNCLENBQUNRLEdBQUQsRUFBTUMsUUFBTixFQUFtQjtBQUM5Q25CLFFBQU1PLElBQU4sQ0FBVyxxQkFBWCxFQUFrQ2EsTUFBbEMsQ0FBeUN0QixFQUFFLE9BQUYsRUFBVztBQUNuRCxXQUFRQSxFQUFFcUIsUUFBRixFQUFZdkIsSUFBWixDQUFpQixjQUFqQjtBQUQyQyxHQUFYLENBQXpDOztBQUlBSSxRQUFNTyxJQUFOLENBQVcscUJBQVgsRUFBa0NhLE1BQWxDLENBQXlDdEIsRUFBRSxVQUFGLEVBQWM7QUFDdEQsV0FBUSxpQkFEOEM7QUFFdEQsWUFBU0EsRUFBRXFCLFFBQUYsRUFBWXZCLElBQVosQ0FBaUIsWUFBakI7QUFGNkMsR0FBZCxFQUd0Q3lCLElBSHNDLEVBQXpDO0FBSUEsRUFURDs7QUFXQTs7O0FBR0EsS0FBTUMsd0JBQXdCLFNBQXhCQSxxQkFBd0IsR0FBTTtBQUNuQyxNQUFNQyxhQUFhMUIsTUFBTVUsSUFBTixDQUFXLHlCQUFYLEVBQXNDaUIsTUFBekQ7QUFDQSxNQUFNQyxnQkFBZ0I1QixNQUFNVSxJQUFOLENBQVcsaUJBQVgsRUFBOEJpQixNQUFwRDs7QUFFQSxNQUFJRCxlQUFlRSxhQUFmLElBQWdDRixhQUFhLENBQWpELEVBQW9EO0FBQ25EMUIsU0FBTVUsSUFBTixDQUFXLHNCQUFYLEVBQW1DbUIsSUFBbkMsQ0FBd0MsU0FBeEMsRUFBbUQsSUFBbkQ7QUFDQTdCLFNBQU1VLElBQU4sQ0FBVyxzQkFBWCxFQUFtQ29CLE9BQW5DLENBQTJDLGtCQUEzQyxFQUErREMsUUFBL0QsQ0FBd0UsU0FBeEU7QUFDQSxHQUhELE1BR087QUFDTi9CLFNBQU1VLElBQU4sQ0FBVyxzQkFBWCxFQUFtQ21CLElBQW5DLENBQXdDLFNBQXhDLEVBQW1ELEtBQW5EO0FBQ0E3QixTQUFNVSxJQUFOLENBQVcsc0JBQVgsRUFBbUNvQixPQUFuQyxDQUEyQyxrQkFBM0MsRUFBK0RFLFdBQS9ELENBQTJFLFNBQTNFO0FBQ0E7O0FBRUQsTUFBSU4sZUFBZSxDQUFuQixFQUFzQjtBQUNyQjFCLFNBQU1VLElBQU4sQ0FBVyxjQUFYLEVBQTJCbUIsSUFBM0IsQ0FBZ0MsVUFBaEMsRUFBNEMsSUFBNUM7QUFDQSxHQUZELE1BRU87QUFDTjdCLFNBQU1VLElBQU4sQ0FBVyxjQUFYLEVBQTJCbUIsSUFBM0IsQ0FBZ0MsVUFBaEMsRUFBNEMsS0FBNUM7QUFDQTtBQUNELEVBakJEOztBQW1CQTs7O0FBR0EsS0FBTUksMkJBQTJCLFNBQTNCQSx3QkFBMkIsR0FBTTtBQUN0QyxNQUFNQyxhQUFhbEMsTUFBTVUsSUFBTixDQUFXLHNCQUFYLEVBQW1DeUIsRUFBbkMsQ0FBc0MsVUFBdEMsQ0FBbkI7O0FBRUEsTUFBSUQsVUFBSixFQUFnQjtBQUNmbEMsU0FBTVUsSUFBTixDQUFXLGlCQUFYLEVBQThCbUIsSUFBOUIsQ0FBbUMsU0FBbkMsRUFBOEMsSUFBOUM7QUFDQTdCLFNBQU1VLElBQU4sQ0FBVyxpQkFBWCxFQUE4QjBCLE1BQTlCLENBQXFDLGtCQUFyQyxFQUF5REwsUUFBekQsQ0FBa0UsU0FBbEU7QUFDQSxHQUhELE1BR087QUFDTi9CLFNBQU1VLElBQU4sQ0FBVyxpQkFBWCxFQUE4Qm1CLElBQTlCLENBQW1DLFNBQW5DLEVBQThDLEtBQTlDO0FBQ0E3QixTQUFNVSxJQUFOLENBQVcsaUJBQVgsRUFBOEIwQixNQUE5QixDQUFxQyxrQkFBckMsRUFBeURKLFdBQXpELENBQXFFLFNBQXJFO0FBQ0E7O0FBRUQsTUFBSWhDLE1BQU1VLElBQU4sQ0FBVyxzQkFBWCxFQUFtQ3lCLEVBQW5DLENBQXNDLFVBQXRDLEtBQXFEbkMsTUFBTVUsSUFBTixDQUFXLHlCQUFYLEVBQXNDaUIsTUFBdEMsR0FBK0MsQ0FBeEcsRUFBMkc7QUFDMUczQixTQUFNVSxJQUFOLENBQVcsY0FBWCxFQUEyQm1CLElBQTNCLENBQWdDLFVBQWhDLEVBQTRDLEtBQTVDO0FBQ0EsR0FGRCxNQUVPO0FBQ043QixTQUFNVSxJQUFOLENBQVcsY0FBWCxFQUEyQm1CLElBQTNCLENBQWdDLFVBQWhDLEVBQTRDLElBQTVDO0FBQ0E7QUFDRCxFQWhCRDs7QUFrQkE7QUFDQTtBQUNBOztBQUVBL0IsUUFBT3VDLElBQVAsR0FBYyxVQUFDQyxJQUFELEVBQVU7QUFDdkJ0QyxRQUFNVSxJQUFOLENBQVcsc0JBQVgsRUFBbUM2QixFQUFuQyxDQUFzQyxPQUF0QyxFQUErQ04sd0JBQS9DO0FBQ0FqQyxRQUFNVSxJQUFOLENBQVcsaUJBQVgsRUFBOEI2QixFQUE5QixDQUFpQyxPQUFqQyxFQUEwQ2QscUJBQTFDO0FBQ0F6QixRQUFNVSxJQUFOLENBQVcsY0FBWCxFQUEyQjZCLEVBQTNCLENBQThCLE9BQTlCLEVBQXVDaEMsbUJBQXZDOztBQUVBK0I7QUFDQSxFQU5EOztBQVFBLFFBQU94QyxNQUFQO0FBQ0EsQ0E5S0YiLCJmaWxlIjoic3BlY2lhbHMvc3BlY2lhbHNfYnVsa19kZWxldGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNwZWNpYWxzX2J1bGtfZGVsZXRlLmpzIDIwMTgtMDQtMTBcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIFNwZWNpYWxzIEJ1bGsgRGVsZXRlIE1vZHVsZVxuICpcbiAqIFRoaXMgbW9kdWxlIGhhbmRlbHMgdGhlIGJ1bGsgZGVsZXRpb24gZm9yIHRoZSBzcGVjaWFscy5cbiAqXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvc3BlY2lhbHNfYnVsa19kZWxldGVcbiAqL1xuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdzcGVjaWFsc19idWxrX2RlbGV0ZScsXG5cdFxuXHRbXSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb21wYXRpYmlsaXR5L3NwZWNpYWxzX2J1bGtfZGVsZXRlICovXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFUyBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFJlZmVyZW5jZVxuXHRcdCAqXG5cdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBCdWxrIGRlbGV0ZSBtb2RhbCBvYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgJG1vZGFsID0gJCgnI21vZGFsX2xheWVyX2NvbnRhaW5lcicpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEJ1bGsgZGVsZXRlIGZvcm0gb2JqZWN0XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRmb3JtID0gJCgnI2J1bGtfZGVsZXRlX2NvbmZpcm1fZm9ybScpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBkZWZhdWx0cyA9IHt9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHQgKlxuXHRcdCAqIEB2YXIge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFBSSVZBVEUgRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlciBmb3IgdGhlIGNsaWNrIGV2ZW50LCB0aGF0J3MgdHJpZ2dlcmVkIGJ5IHRoZSBkZWxldGUgYnV0dG9uLlxuXHRcdCAqL1xuXHRcdGNvbnN0IF9oYW5kbGVEZWxldGVCdXR0b24gPSAoZXZlbnQpID0+IHtcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdCRmb3JtLmZpbmQoJy5wcm9kdWN0cy10by1kZWxldGUnKS5odG1sKCcnKTtcblx0XHRcdCQuZWFjaCgkdGhpcy5maW5kKCcuZGVsZXRlLXNwZWNpYWw6Y2hlY2tlZCcpLCBfYWRkU3BlY2lhbHNUb01vZGFsKTtcblx0XHRcdFxuXHRcdFx0JGZvcm0uZGlhbG9nKHtcblx0XHRcdFx0J3RpdGxlJzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfSU5GT19IRUFESU5HX0JVTEtfREVMRVRFX1NQRUNJQUxTJywgJ2FkbWluX3NwZWNpYWxzJyksXG5cdFx0XHRcdCdtb2RhbCc6IHRydWUsXG5cdFx0XHRcdCdkaWFsb2dDbGFzcyc6ICdneC1jb250YWluZXInLFxuXHRcdFx0XHQnYnV0dG9ucyc6IF9nZXRNb2RhbEJ1dHRvbnMoJGZvcm0pLFxuXHRcdFx0XHQnd2lkdGgnOiA0MjBcblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgbW9kYWwgYnV0dG9ucywgdGhhdCBhcmUgdXNlZCBpbiB0aGUgZGVsZXRlIGNvbmZpcm1hdGlvbiBtb2RhbC5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge29iamVjdH0gQnV0dG9ucyBmcHIgdGhlIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWxcblx0XHQgKi9cblx0XHRjb25zdCBfZ2V0TW9kYWxCdXR0b25zID0gKCRmb3JtKSA9PiB7XG5cdFx0XHRyZXR1cm4gW1xuXHRcdFx0XHR7XG5cdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY2xvc2UnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdCdjbGFzcyc6ICdidG4nLFxuXHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9LFxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZGVsZXRlJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuIGJ0bi1wcmltYXJ5Jyxcblx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdCRmb3JtLnN1Ym1pdCgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFkZHMgdGhlIGluZm9ybWF0aW9ucyBvZiB0aGUgc2VsZWN0ZWQgc3BlY2lhbHMgdG8gdGhlIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwuXG5cdFx0ICovXG5cdFx0Y29uc3QgX2FkZFNwZWNpYWxzVG9Nb2RhbCA9IChrZXksIGNoZWNrYm94KSA9PiB7XG5cdFx0XHQkZm9ybS5maW5kKCcucHJvZHVjdHMtdG8tZGVsZXRlJykuYXBwZW5kKCQoJzxsaS8+Jywge1xuXHRcdFx0XHQndGV4dCc6ICQoY2hlY2tib3gpLmRhdGEoJ3Byb2R1Y3QtbmFtZScpXG5cdFx0XHR9KSk7XG5cdFx0XHRcblx0XHRcdCRmb3JtLmZpbmQoJy5wcm9kdWN0cy10by1kZWxldGUnKS5hcHBlbmQoJCgnPGlucHV0Lz4nLCB7XG5cdFx0XHRcdCduYW1lJzogJ2RlbGV0ZVNwZWNpYWxbXScsXG5cdFx0XHRcdCd2YWx1ZSc6ICQoY2hlY2tib3gpLmRhdGEoJ3NwZWNpYWwtaWQnKVxuXHRcdFx0fSkuaGlkZSgpKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXIgZm9yIHRoZSBjbGljayBldmVudCwgdGhhdCdzIHRyaWdnZXJlZCBieSBhIG5vcm1hbCBjaGVja2JveCBpbiB0aGUgc3BlY2lhbHMgb3ZlcnZpZXcgdGFibGUuXG5cdFx0ICovXG5cdFx0Y29uc3QgX2hhbmRsZU5vcm1hbENoZWNrYm94ID0gKCkgPT4ge1xuXHRcdFx0Y29uc3QgbnVtQ2hlY2tlZCA9ICR0aGlzLmZpbmQoJy5kZWxldGUtc3BlY2lhbDpjaGVja2VkJykubGVuZ3RoO1xuXHRcdFx0Y29uc3QgbnVtTm90Q2hlY2tlZCA9ICR0aGlzLmZpbmQoJy5kZWxldGUtc3BlY2lhbCcpLmxlbmd0aDtcblx0XHRcdFxuXHRcdFx0aWYgKG51bUNoZWNrZWQgPT09IG51bU5vdENoZWNrZWQgJiYgbnVtQ2hlY2tlZCA+IDApIHtcblx0XHRcdFx0JHRoaXMuZmluZCgnLnNlbGVjdC1hbGwtY2hlY2tib3gnKS5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5zZWxlY3QtYWxsLWNoZWNrYm94JykuY2xvc2VzdCgnLnNpbmdsZS1jaGVja2JveCcpLmFkZENsYXNzKCdjaGVja2VkJyk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQkdGhpcy5maW5kKCcuc2VsZWN0LWFsbC1jaGVja2JveCcpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5zZWxlY3QtYWxsLWNoZWNrYm94JykuY2xvc2VzdCgnLnNpbmdsZS1jaGVja2JveCcpLnJlbW92ZUNsYXNzKCdjaGVja2VkJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmIChudW1DaGVja2VkID09PSAwKSB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5idWxrLWRlbGV0ZScpLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQkdGhpcy5maW5kKCcuYnVsay1kZWxldGUnKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXIgZm9yIHRoZSBjbGljayBldmVudCwgdGhhdCdzIHRyaWdnZXJlZCBieSB0aGUgY2hlY2tib3ggaW4gdGhlIGhlYWQgb2YgdGhlIHNwZWNpYWxzIG92ZXJ2aWV3IHRhYmxlLlxuXHRcdCAqL1xuXHRcdGNvbnN0IF9oYW5kbGVTZWxlY3RBbGxDaGVja2JveCA9ICgpID0+IHtcblx0XHRcdGNvbnN0IGFsbENoZWNrZWQgPSAkdGhpcy5maW5kKCcuc2VsZWN0LWFsbC1jaGVja2JveCcpLmlzKFwiOmNoZWNrZWRcIik7XG5cdFx0XHRcblx0XHRcdGlmIChhbGxDaGVja2VkKSB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5kZWxldGUtc3BlY2lhbCcpLnByb3AoJ2NoZWNrZWQnLCB0cnVlKTtcblx0XHRcdFx0JHRoaXMuZmluZCgnLmRlbGV0ZS1zcGVjaWFsJykucGFyZW50KCcuc2luZ2xlLWNoZWNrYm94JykuYWRkQ2xhc3MoJ2NoZWNrZWQnKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5kZWxldGUtc3BlY2lhbCcpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5kZWxldGUtc3BlY2lhbCcpLnBhcmVudCgnLnNpbmdsZS1jaGVja2JveCcpLnJlbW92ZUNsYXNzKCdjaGVja2VkJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmICgkdGhpcy5maW5kKCcuc2VsZWN0LWFsbC1jaGVja2JveCcpLmlzKFwiOmNoZWNrZWRcIikgJiYgJHRoaXMuZmluZCgnLmRlbGV0ZS1zcGVjaWFsOmNoZWNrZWQnKS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJy5idWxrLWRlbGV0ZScpLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0JHRoaXMuZmluZCgnLmJ1bGstZGVsZXRlJykucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSAoZG9uZSkgPT4ge1xuXHRcdFx0JHRoaXMuZmluZCgnLnNlbGVjdC1hbGwtY2hlY2tib3gnKS5vbignY2xpY2snLCBfaGFuZGxlU2VsZWN0QWxsQ2hlY2tib3gpO1xuXHRcdFx0JHRoaXMuZmluZCgnLmRlbGV0ZS1zcGVjaWFsJykub24oJ2NsaWNrJywgX2hhbmRsZU5vcm1hbENoZWNrYm94KTtcblx0XHRcdCR0aGlzLmZpbmQoJy5idWxrLWRlbGV0ZScpLm9uKCdjbGljaycsIF9oYW5kbGVEZWxldGVCdXR0b24pO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9XG4pOyJdfQ==
