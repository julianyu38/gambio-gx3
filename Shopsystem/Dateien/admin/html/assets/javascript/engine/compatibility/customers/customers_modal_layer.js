'use strict';

/* --------------------------------------------------------------
 customers_modal_layer.js 2018-05-16
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Customers Modal Layer Module
 *
 * This module will open a modal layer for
 * customers actions like deleting the article.
 *
 * @module Compatibility/customers_modal_layer
 */
gx.compatibility.module('customers_modal_layer', ['xhr'],

/**  @lends module:Compatibility/customers_modal_layer */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Modal Selector
  *
  * @type {object}
  */
	$modal = $('#modal_layer_container'),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {},


	/**
  * Reference to the actual file
  *
  * @var {string}
  */
	srcPath = window.location.origin + window.location.pathname,


	/**
  * Query parameter string
  *
  * @type {string}
  */
	queryString = '?' + window.location.search.replace(/\?/, '').replace(/cID=[\d]+/g, '').replace(/action=[\w]+/g, '').replace(/pageToken=[\w]+/g, '').concat('&').replace(/&[&]+/g, '&').replace(/^&/g, '');

	// ------------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Prepares buttons for the modal
  * @param {object | jQuery} $that
  * @returns {Array}
  * @private
  */
	var _getModalButtons = function _getModalButtons($that) {
		var buttons = [];

		var submitBtn, abortBtn;

		switch (options.action) {
			case 'delete':
				submitBtn = $that.find('input:first');
				abortBtn = $that.find('a.btn');

				$(submitBtn).hide();
				$(abortBtn).hide();

				buttons.push({
					'text': jse.core.lang.translate('close', 'buttons'),
					'class': 'btn',
					'click': function click() {
						$(this).dialog('close');
						abortBtn.trigger('click');
					}
				}, {
					'text': jse.core.lang.translate('delete', 'buttons'),
					'class': 'btn btn-primary',
					'click': function click() {
						var obj = {
							pageToken: $('input[name="page_token"]:first').attr('value'),
							cID: window.location.href.match(/cID=\d+/)[0]
						};

						obj.url = [srcPath, queryString, 'action=deleteconfirm', '&' + obj.cID].join('');

						var $form = $('<form name="customers" method="post" action=' + obj.url + '></form>');
						$form.append('<input type="hidden" name="page_token" value=' + obj.pageToken + '>');
						$form.append('<input type="hidden" name="deleteconfirm" value="DeleteConfirm">');
						$form.appendTo('body');
						$form.submit();
					}
				});
				break;
			case 'editstatus':
				submitBtn = $that.find('input:eq(1)');
				abortBtn = $that.find('a.btn');

				$(submitBtn).hide();
				$(abortBtn).hide();

				buttons.push({
					'text': jse.core.lang.translate('close', 'buttons'),
					'class': 'btn',
					'click': function click() {
						$(this).dialog('close');
						window.open(abortBtn.attr('href'), '_self');
					}
				}, {
					'text': jse.core.lang.translate('update', 'buttons'),
					'class': 'btn btn-primary',
					'click': function click() {
						var obj = {
							pageToken: $('input[name="page_token"]:first').attr('value'),
							cID: window.location.href.match(/cID=\d+/)[0],
							status: $that.find('select').val()
						};

						obj.url = [srcPath, queryString, 'action=statusconfirm', '&' + obj.cID].join('');

						var $form = $('<form name="customers" method="post" action=' + obj.url + '></form>');
						$form.append('<input type="hidden" name="page_token" value=' + obj.pageToken + '>');
						$form.append('<input type="hidden" name="status" value=' + obj.status + '>');
						$form.append('<input type="hidden" name="statusconfirm" value="Update">');
						$form.appendTo('body');
						$form.submit();
					}
				});
				break;
			case 'iplog':
				buttons.push({
					'text': jse.core.lang.translate('close', 'buttons'),
					'class': 'btn',
					'click': function click() {
						$(this).dialog('close');
					}
				});
				break;
			case 'new_memo':
				console.log(submitBtn);
				buttons.push({
					'text': jse.core.lang.translate('close', 'buttons'),
					'class': 'btn',
					'click': function click() {
						$(this).dialog('close');
					}
				});
				buttons.push({
					'text': jse.core.lang.translate('send', 'buttons'),
					'class': 'btn btn-primary',
					'click': function click(event) {
						//event.preventDefault();
						//gm_cancel('gm_send_order.php', '&type=cancel', 'CANCEL');
						$that.submit();
					}
				});
				break;
			case 'delete_personal_data':
				submitBtn = $that.find('input:submit').first();
				submitBtn.hide();

				buttons.push({
					'text': jse.core.lang.translate('close', 'buttons'),
					'class': 'btn',
					'click': function click() {
						$(this).dialog('close');
					}
				}, {
					'text': jse.core.lang.translate('delete', 'buttons'),
					'class': 'btn btn-primary',
					'click': function click() {
						submitBtn.click();
					}
				});

				break;
			case 'export_personal_data':
				submitBtn = $that.find('input:submit').first();
				submitBtn.hide();

				buttons.push({
					'text': jse.core.lang.translate('close', 'buttons'),
					'class': 'btn',
					'click': function click() {
						$(this).dialog('close');
					}
				}, {
					'text': jse.core.lang.translate('BUTTON_EXPORT', 'admin_buttons'),
					'class': 'btn btn-primary',
					'click': function click() {
						var url = $that.attr('action') + '&' + $that.serialize();
						window.open(url, '_blank');
					}
				});

				break;
		}

		return buttons;
	};

	/**
  * Creates dialog for single removal
  * @private
  */
	var _openDeleteDialog = function _openDeleteDialog() {
		$this.dialog({
			'title': jse.core.lang.translate('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'admin_customers'),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($this),
			'width': 420,
			'closeOnEscape': false,
			'open': function open() {
				$('.ui-dialog-titlebar-close').hide();
			}
		});
	};

	/**
  * Creates dialog for single status change
  * @private
  */
	var _openEditStatusDialog = function _openEditStatusDialog() {
		$this.dialog({
			'title': jse.core.lang.translate('TEXT_INFO_HEADING_STATUS_CUSTOMER', 'admin_customers'),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($this),
			'width': 420,
			'closeOnEscape': false,
			'open': function open() {
				// Make Some Fixes
				$('.ui-dialog-titlebar-close').hide();
				$(this).find('select[name="status"]').css({
					width: '100%',
					height: '35px',
					fontSize: '12px'
				});
			}
		});
	};

	/**
  * Creates dialog for single IP log
  * @private
  */
	var _openIpLogDialog = function _openIpLogDialog() {
		$this = $('<div></div>');

		$('[data-iplog]').each(function () {
			$this.append(this);
			$this.append('<br><br>');
		});

		$this.appendTo('body');
		$this.dialog({
			'title': 'IP-Log',
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($this),
			'width': 420,
			'closeOnEscape': false
		});
	};

	var _openNewMemoDialog = function _openNewMemoDialog(event) {
		var $form = $('#customer_memo_form');

		event.preventDefault();

		$form.dialog({
			'title': jse.core.lang.translate('TEXT_NEW_MEMO', 'admin_customers'),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($form),
			'width': 580
		});
	};

	/**
  * Creates dialog for the personal data deletion
  * @private
  */
	var _openDeletePersonalDataDialog = function _openDeletePersonalDataDialog() {
		$this.dialog({
			'title': jse.core.lang.translate('delete_personal_data', 'admin_customers'),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($this),
			'width': 420,
			'closeOnEscape': false,
			'open': function open() {
				$('.ui-dialog-titlebar-close').hide();
			}
		});
	};

	/**
  * Creates dialog for the personal data export
  * @private
  */
	var _openExportPersonalDataDialog = function _openExportPersonalDataDialog() {
		$this.dialog({
			'title': jse.core.lang.translate('export_personal_data', 'admin_customers'),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': _getModalButtons($this),
			'width': 420,
			'closeOnEscape': false,
			'open': function open() {
				$('.ui-dialog-titlebar-close').hide();
			}
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {

		switch (options.action) {
			case 'delete':
				_openDeleteDialog();
				break;
			case 'editstatus':
				_openEditStatusDialog();
				break;
			case 'iplog':
				_openIpLogDialog();
				break;
			case 'new_memo':
				$this.on('click', _openNewMemoDialog);
				break;
			case 'delete_personal_data':
				_openDeletePersonalDataDialog();
				break;
			case 'export_personal_data':
				_openExportPersonalDataDialog();
				break;
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbWVycy9jdXN0b21lcnNfbW9kYWxfbGF5ZXIuanMiXSwibmFtZXMiOlsiZ3giLCJjb21wYXRpYmlsaXR5IiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiRtb2RhbCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsInNyY1BhdGgiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsIm9yaWdpbiIsInBhdGhuYW1lIiwicXVlcnlTdHJpbmciLCJzZWFyY2giLCJyZXBsYWNlIiwiY29uY2F0IiwiX2dldE1vZGFsQnV0dG9ucyIsIiR0aGF0IiwiYnV0dG9ucyIsInN1Ym1pdEJ0biIsImFib3J0QnRuIiwiYWN0aW9uIiwiZmluZCIsImhpZGUiLCJwdXNoIiwianNlIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJkaWFsb2ciLCJ0cmlnZ2VyIiwib2JqIiwicGFnZVRva2VuIiwiYXR0ciIsImNJRCIsImhyZWYiLCJtYXRjaCIsInVybCIsImpvaW4iLCIkZm9ybSIsImFwcGVuZCIsImFwcGVuZFRvIiwic3VibWl0Iiwib3BlbiIsInN0YXR1cyIsInZhbCIsImNvbnNvbGUiLCJsb2ciLCJldmVudCIsImZpcnN0IiwiY2xpY2siLCJzZXJpYWxpemUiLCJfb3BlbkRlbGV0ZURpYWxvZyIsIl9vcGVuRWRpdFN0YXR1c0RpYWxvZyIsImNzcyIsIndpZHRoIiwiaGVpZ2h0IiwiZm9udFNpemUiLCJfb3BlbklwTG9nRGlhbG9nIiwiZWFjaCIsIl9vcGVuTmV3TWVtb0RpYWxvZyIsInByZXZlbnREZWZhdWx0IiwiX29wZW5EZWxldGVQZXJzb25hbERhdGFEaWFsb2ciLCJfb3BlbkV4cG9ydFBlcnNvbmFsRGF0YURpYWxvZyIsImluaXQiLCJkb25lIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7QUFRQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyx1QkFERCxFQUdDLENBQUMsS0FBRCxDQUhEOztBQUtDOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxVQUFTRCxFQUFFLHdCQUFGLENBYlY7OztBQWVDOzs7OztBQUtBRSxZQUFXLEVBcEJaOzs7QUFzQkM7Ozs7O0FBS0FDLFdBQVVILEVBQUVJLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJKLElBQTdCLENBM0JYOzs7QUE2QkM7Ozs7O0FBS0FELFVBQVMsRUFsQ1Y7OztBQW9DQzs7Ozs7QUFLQVEsV0FBVUMsT0FBT0MsUUFBUCxDQUFnQkMsTUFBaEIsR0FBeUJGLE9BQU9DLFFBQVAsQ0FBZ0JFLFFBekNwRDs7O0FBMkNDOzs7OztBQUtBQyxlQUFjLE1BQU9KLE9BQU9DLFFBQVAsQ0FBZ0JJLE1BQWhCLENBQ2xCQyxPQURrQixDQUNWLElBRFUsRUFDSixFQURJLEVBRWxCQSxPQUZrQixDQUVWLFlBRlUsRUFFSSxFQUZKLEVBR2xCQSxPQUhrQixDQUdWLGVBSFUsRUFHTyxFQUhQLEVBSWxCQSxPQUprQixDQUlWLGtCQUpVLEVBSVUsRUFKVixFQUtsQkMsTUFMa0IsQ0FLWCxHQUxXLEVBTWxCRCxPQU5rQixDQU1WLFFBTlUsRUFNQSxHQU5BLEVBT2xCQSxPQVBrQixDQU9WLEtBUFUsRUFPSCxFQVBHLENBaER0Qjs7QUF5REE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFNQSxLQUFJRSxtQkFBbUIsU0FBbkJBLGdCQUFtQixDQUFTQyxLQUFULEVBQWdCO0FBQ3RDLE1BQUlDLFVBQVUsRUFBZDs7QUFFQSxNQUFJQyxTQUFKLEVBQWVDLFFBQWY7O0FBRUEsVUFBUWYsUUFBUWdCLE1BQWhCO0FBQ0MsUUFBSyxRQUFMO0FBQ0NGLGdCQUFZRixNQUFNSyxJQUFOLENBQVcsYUFBWCxDQUFaO0FBQ0FGLGVBQVdILE1BQU1LLElBQU4sQ0FBVyxPQUFYLENBQVg7O0FBRUFwQixNQUFFaUIsU0FBRixFQUFhSSxJQUFiO0FBQ0FyQixNQUFFa0IsUUFBRixFQUFZRyxJQUFaOztBQUVBTCxZQUFRTSxJQUFSLENBQ0M7QUFDQyxhQUFRQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxTQUFqQyxDQURUO0FBRUMsY0FBUyxLQUZWO0FBR0MsY0FBUyxpQkFBVztBQUNuQjFCLFFBQUUsSUFBRixFQUFRMkIsTUFBUixDQUFlLE9BQWY7QUFDQVQsZUFBU1UsT0FBVCxDQUFpQixPQUFqQjtBQUNBO0FBTkYsS0FERCxFQVNDO0FBQ0MsYUFBUUwsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsUUFBeEIsRUFBa0MsU0FBbEMsQ0FEVDtBQUVDLGNBQVMsaUJBRlY7QUFHQyxjQUFTLGlCQUFXO0FBQ25CLFVBQUlHLE1BQU07QUFDVEMsa0JBQVc5QixFQUFFLGdDQUFGLEVBQ1QrQixJQURTLENBQ0osT0FESSxDQURGO0FBR1RDLFlBQUsxQixPQUFPQyxRQUFQLENBQWdCMEIsSUFBaEIsQ0FBcUJDLEtBQXJCLENBQTJCLFNBQTNCLEVBQXNDLENBQXRDO0FBSEksT0FBVjs7QUFNQUwsVUFBSU0sR0FBSixHQUFVLENBQ1Q5QixPQURTLEVBRVRLLFdBRlMsRUFHVCxzQkFIUyxFQUlULE1BQU1tQixJQUFJRyxHQUpELEVBS1JJLElBTFEsQ0FLSCxFQUxHLENBQVY7O0FBT0EsVUFBSUMsUUFBUXJDLEVBQUUsaURBQWlENkIsSUFBSU0sR0FBckQsR0FBMkQsVUFBN0QsQ0FBWjtBQUNBRSxZQUFNQyxNQUFOLENBQWEsa0RBQWtEVCxJQUFJQyxTQUF0RCxHQUFrRSxHQUEvRTtBQUNBTyxZQUFNQyxNQUFOLENBQWEsa0VBQWI7QUFDQUQsWUFBTUUsUUFBTixDQUFlLE1BQWY7QUFDQUYsWUFBTUcsTUFBTjtBQUNBO0FBdEJGLEtBVEQ7QUFpQ0E7QUFDRCxRQUFLLFlBQUw7QUFDQ3ZCLGdCQUFZRixNQUFNSyxJQUFOLENBQVcsYUFBWCxDQUFaO0FBQ0FGLGVBQVdILE1BQU1LLElBQU4sQ0FBVyxPQUFYLENBQVg7O0FBRUFwQixNQUFFaUIsU0FBRixFQUFhSSxJQUFiO0FBQ0FyQixNQUFFa0IsUUFBRixFQUFZRyxJQUFaOztBQUVBTCxZQUFRTSxJQUFSLENBQ0M7QUFDQyxhQUFRQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxTQUFqQyxDQURUO0FBRUMsY0FBUyxLQUZWO0FBR0MsY0FBUyxpQkFBVztBQUNuQjFCLFFBQUUsSUFBRixFQUFRMkIsTUFBUixDQUFlLE9BQWY7QUFDQXJCLGFBQU9tQyxJQUFQLENBQVl2QixTQUFTYSxJQUFULENBQWMsTUFBZCxDQUFaLEVBQW1DLE9BQW5DO0FBQ0E7QUFORixLQURELEVBU0M7QUFDQyxhQUFRUixJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixRQUF4QixFQUFrQyxTQUFsQyxDQURUO0FBRUMsY0FBUyxpQkFGVjtBQUdDLGNBQVMsaUJBQVc7QUFDbkIsVUFBSUcsTUFBTTtBQUNUQyxrQkFBVzlCLEVBQUUsZ0NBQUYsRUFDVCtCLElBRFMsQ0FDSixPQURJLENBREY7QUFHVEMsWUFBSzFCLE9BQU9DLFFBQVAsQ0FBZ0IwQixJQUFoQixDQUFxQkMsS0FBckIsQ0FBMkIsU0FBM0IsRUFBc0MsQ0FBdEMsQ0FISTtBQUlUUSxlQUFRM0IsTUFBTUssSUFBTixDQUFXLFFBQVgsRUFBcUJ1QixHQUFyQjtBQUpDLE9BQVY7O0FBT0FkLFVBQUlNLEdBQUosR0FBVSxDQUNUOUIsT0FEUyxFQUVUSyxXQUZTLEVBR1Qsc0JBSFMsRUFJVCxNQUFNbUIsSUFBSUcsR0FKRCxFQUtSSSxJQUxRLENBS0gsRUFMRyxDQUFWOztBQU9BLFVBQUlDLFFBQVFyQyxFQUFFLGlEQUFpRDZCLElBQUlNLEdBQXJELEdBQTJELFVBQTdELENBQVo7QUFDQUUsWUFBTUMsTUFBTixDQUFhLGtEQUFrRFQsSUFBSUMsU0FBdEQsR0FBa0UsR0FBL0U7QUFDQU8sWUFBTUMsTUFBTixDQUFhLDhDQUE4Q1QsSUFBSWEsTUFBbEQsR0FBMkQsR0FBeEU7QUFDQUwsWUFBTUMsTUFBTixDQUFhLDJEQUFiO0FBQ0FELFlBQU1FLFFBQU4sQ0FBZSxNQUFmO0FBQ0FGLFlBQU1HLE1BQU47QUFDQTtBQXhCRixLQVREO0FBbUNBO0FBQ0QsUUFBSyxPQUFMO0FBQ0N4QixZQUFRTSxJQUFSLENBQWE7QUFDWixhQUFRQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxTQUFqQyxDQURJO0FBRVosY0FBUyxLQUZHO0FBR1osY0FBUyxpQkFBVztBQUNuQjFCLFFBQUUsSUFBRixFQUFRMkIsTUFBUixDQUFlLE9BQWY7QUFDQTtBQUxXLEtBQWI7QUFPQTtBQUNELFFBQUssVUFBTDtBQUNDaUIsWUFBUUMsR0FBUixDQUFZNUIsU0FBWjtBQUNBRCxZQUFRTSxJQUFSLENBQWE7QUFDWixhQUFRQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxTQUFqQyxDQURJO0FBRVosY0FBUyxLQUZHO0FBR1osY0FBUyxpQkFBVztBQUNuQjFCLFFBQUUsSUFBRixFQUFRMkIsTUFBUixDQUFlLE9BQWY7QUFDQTtBQUxXLEtBQWI7QUFPQVgsWUFBUU0sSUFBUixDQUFhO0FBQ1osYUFBUUMsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsTUFBeEIsRUFBZ0MsU0FBaEMsQ0FESTtBQUVaLGNBQVMsaUJBRkc7QUFHWixjQUFTLGVBQVNvQixLQUFULEVBQWdCO0FBQ3hCO0FBQ0E7QUFDQS9CLFlBQU15QixNQUFOO0FBQ0E7QUFQVyxLQUFiO0FBU0E7QUFDVyxRQUFLLHNCQUFMO0FBQ0N2QixnQkFBWUYsTUFBTUssSUFBTixDQUFXLGNBQVgsRUFBMkIyQixLQUEzQixFQUFaO0FBQ0c5QixjQUFVSSxJQUFWOztBQUVBTCxZQUFRTSxJQUFSLENBQ2Q7QUFDQyxhQUFRQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxTQUFqQyxDQURUO0FBRUMsY0FBUyxLQUZWO0FBR0MsY0FBUyxpQkFBVztBQUNuQjFCLFFBQUUsSUFBRixFQUFRMkIsTUFBUixDQUFlLE9BQWY7QUFDQTtBQUxGLEtBRGMsRUFRZDtBQUNDLGFBQVFKLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFFBQXhCLEVBQWtDLFNBQWxDLENBRFQ7QUFFQyxjQUFTLGlCQUZWO0FBR0MsY0FBUyxpQkFBVztBQUNLVCxnQkFBVStCLEtBQVY7QUFDeEI7QUFMRixLQVJjOztBQWlCSDtBQUNELFFBQUssc0JBQUw7QUFDSS9CLGdCQUFZRixNQUFNSyxJQUFOLENBQVcsY0FBWCxFQUEyQjJCLEtBQTNCLEVBQVo7QUFDQTlCLGNBQVVJLElBQVY7O0FBRUFMLFlBQVFNLElBQVIsQ0FDSTtBQUNJLGFBQVFDLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFNBQWpDLENBRFo7QUFFSSxjQUFTLEtBRmI7QUFHSSxjQUFTLGlCQUFXO0FBQ2hCMUIsUUFBRSxJQUFGLEVBQVEyQixNQUFSLENBQWUsT0FBZjtBQUNIO0FBTEwsS0FESixFQVFJO0FBQ0ksYUFBUUosSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsZUFBeEIsRUFBeUMsZUFBekMsQ0FEWjtBQUVJLGNBQVMsaUJBRmI7QUFHSSxjQUFTLGlCQUFXO0FBQ2hCLFVBQU1TLE1BQVNwQixNQUFNZ0IsSUFBTixDQUFXLFFBQVgsQ0FBVCxTQUFpQ2hCLE1BQU1rQyxTQUFOLEVBQXZDO0FBQ0EzQyxhQUFPbUMsSUFBUCxDQUFZTixHQUFaLEVBQWlCLFFBQWpCO0FBQ0g7QUFOTCxLQVJKOztBQWtCQTtBQTdKakI7O0FBZ0tBLFNBQU9uQixPQUFQO0FBQ0EsRUF0S0Q7O0FBd0tBOzs7O0FBSUEsS0FBSWtDLG9CQUFvQixTQUFwQkEsaUJBQW9CLEdBQVc7QUFDbENuRCxRQUFNNEIsTUFBTixDQUFhO0FBQ1osWUFBU0osSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUNBQXhCLEVBQTZELGlCQUE3RCxDQURHO0FBRVosWUFBUyxJQUZHO0FBR1osa0JBQWUsY0FISDtBQUlaLGNBQVdaLGlCQUFpQmYsS0FBakIsQ0FKQztBQUtaLFlBQVMsR0FMRztBQU1aLG9CQUFpQixLQU5MO0FBT1osV0FBUSxnQkFBVztBQUNsQkMsTUFBRSwyQkFBRixFQUErQnFCLElBQS9CO0FBQ0E7QUFUVyxHQUFiO0FBV0EsRUFaRDs7QUFjQTs7OztBQUlBLEtBQUk4Qix3QkFBd0IsU0FBeEJBLHFCQUF3QixHQUFXO0FBQ3RDcEQsUUFBTTRCLE1BQU4sQ0FBYTtBQUNaLFlBQVNKLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1DQUF4QixFQUE2RCxpQkFBN0QsQ0FERztBQUVaLFlBQVMsSUFGRztBQUdaLGtCQUFlLGNBSEg7QUFJWixjQUFXWixpQkFBaUJmLEtBQWpCLENBSkM7QUFLWixZQUFTLEdBTEc7QUFNWixvQkFBaUIsS0FOTDtBQU9aLFdBQVEsZ0JBQVc7QUFDbEI7QUFDQUMsTUFBRSwyQkFBRixFQUErQnFCLElBQS9CO0FBQ0FyQixNQUFFLElBQUYsRUFDRW9CLElBREYsQ0FDTyx1QkFEUCxFQUVFZ0MsR0FGRixDQUVNO0FBQ0pDLFlBQU8sTUFESDtBQUVKQyxhQUFRLE1BRko7QUFHSkMsZUFBVTtBQUhOLEtBRk47QUFPQTtBQWpCVyxHQUFiO0FBbUJBLEVBcEJEOztBQXNCQTs7OztBQUlBLEtBQUlDLG1CQUFtQixTQUFuQkEsZ0JBQW1CLEdBQVc7QUFDakN6RCxVQUFRQyxFQUFFLGFBQUYsQ0FBUjs7QUFFQUEsSUFBRSxjQUFGLEVBQWtCeUQsSUFBbEIsQ0FBdUIsWUFBVztBQUNqQzFELFNBQU11QyxNQUFOLENBQWEsSUFBYjtBQUNBdkMsU0FBTXVDLE1BQU4sQ0FBYSxVQUFiO0FBQ0EsR0FIRDs7QUFLQXZDLFFBQU13QyxRQUFOLENBQWUsTUFBZjtBQUNBeEMsUUFBTTRCLE1BQU4sQ0FBYTtBQUNaLFlBQVMsUUFERztBQUVaLFlBQVMsSUFGRztBQUdaLGtCQUFlLGNBSEg7QUFJWixjQUFXYixpQkFBaUJmLEtBQWpCLENBSkM7QUFLWixZQUFTLEdBTEc7QUFNWixvQkFBaUI7QUFOTCxHQUFiO0FBUUEsRUFqQkQ7O0FBbUJBLEtBQUkyRCxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFTWixLQUFULEVBQWdCO0FBQ3hDLE1BQUlULFFBQVFyQyxFQUFFLHFCQUFGLENBQVo7O0FBRUE4QyxRQUFNYSxjQUFOOztBQUVBdEIsUUFBTVYsTUFBTixDQUFhO0FBQ1osWUFBU0osSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsZUFBeEIsRUFBeUMsaUJBQXpDLENBREc7QUFFWixZQUFTLElBRkc7QUFHWixrQkFBZSxjQUhIO0FBSVosY0FBV1osaUJBQWlCdUIsS0FBakIsQ0FKQztBQUtaLFlBQVM7QUFMRyxHQUFiO0FBT0EsRUFaRDs7QUFjTTs7OztBQUlBLEtBQUl1QixnQ0FBZ0MsU0FBaENBLDZCQUFnQyxHQUFXO0FBQzNDN0QsUUFBTTRCLE1BQU4sQ0FBYTtBQUNULFlBQVNKLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHNCQUF4QixFQUFnRCxpQkFBaEQsQ0FEQTtBQUVULFlBQVMsSUFGQTtBQUdULGtCQUFlLGNBSE47QUFJVCxjQUFXWixpQkFBaUJmLEtBQWpCLENBSkY7QUFLVCxZQUFTLEdBTEE7QUFNVCxvQkFBaUIsS0FOUjtBQU9ULFdBQVEsZ0JBQVc7QUFDZkMsTUFBRSwyQkFBRixFQUErQnFCLElBQS9CO0FBQ0g7QUFUUSxHQUFiO0FBV0gsRUFaRDs7QUFjQTs7OztBQUlBLEtBQUl3QyxnQ0FBZ0MsU0FBaENBLDZCQUFnQyxHQUFXO0FBQzNDOUQsUUFBTTRCLE1BQU4sQ0FBYTtBQUNULFlBQVNKLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHNCQUF4QixFQUFnRCxpQkFBaEQsQ0FEQTtBQUVULFlBQVMsSUFGQTtBQUdULGtCQUFlLGNBSE47QUFJVCxjQUFXWixpQkFBaUJmLEtBQWpCLENBSkY7QUFLVCxZQUFTLEdBTEE7QUFNVCxvQkFBaUIsS0FOUjtBQU9ULFdBQVEsZ0JBQVc7QUFDZkMsTUFBRSwyQkFBRixFQUErQnFCLElBQS9CO0FBQ0g7QUFUUSxHQUFiO0FBV0gsRUFaRDs7QUFjTjtBQUNBO0FBQ0E7O0FBRUF4QixRQUFPaUUsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTs7QUFFNUIsVUFBUTVELFFBQVFnQixNQUFoQjtBQUNDLFFBQUssUUFBTDtBQUNDK0I7QUFDQTtBQUNELFFBQUssWUFBTDtBQUNDQztBQUNBO0FBQ0QsUUFBSyxPQUFMO0FBQ0NLO0FBQ0E7QUFDRCxRQUFLLFVBQUw7QUFDQ3pELFVBQU1pRSxFQUFOLENBQVMsT0FBVCxFQUFrQk4sa0JBQWxCO0FBQ0E7QUFDVyxRQUFLLHNCQUFMO0FBQ0lFO0FBQ0E7QUFDSixRQUFLLHNCQUFMO0FBQ0lDO0FBQ0E7QUFsQmpCOztBQXFCQUU7QUFDQSxFQXhCRDs7QUEwQkEsUUFBT2xFLE1BQVA7QUFDQSxDQTlZRiIsImZpbGUiOiJjdXN0b21lcnMvY3VzdG9tZXJzX21vZGFsX2xheWVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBjdXN0b21lcnNfbW9kYWxfbGF5ZXIuanMgMjAxOC0wNS0xNlxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgQ3VzdG9tZXJzIE1vZGFsIExheWVyIE1vZHVsZVxuICpcbiAqIFRoaXMgbW9kdWxlIHdpbGwgb3BlbiBhIG1vZGFsIGxheWVyIGZvclxuICogY3VzdG9tZXJzIGFjdGlvbnMgbGlrZSBkZWxldGluZyB0aGUgYXJ0aWNsZS5cbiAqXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvY3VzdG9tZXJzX21vZGFsX2xheWVyXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnY3VzdG9tZXJzX21vZGFsX2xheWVyJyxcblx0XG5cdFsneGhyJ10sXG5cdFxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9jdXN0b21lcnNfbW9kYWxfbGF5ZXIgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kYWwgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkbW9kYWwgPSAkKCcjbW9kYWxfbGF5ZXJfY29udGFpbmVyJyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge30sXG5cdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFJlZmVyZW5jZSB0byB0aGUgYWN0dWFsIGZpbGVcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtzdHJpbmd9XG5cdFx0XHQgKi9cblx0XHRcdHNyY1BhdGggPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luICsgd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFF1ZXJ5IHBhcmFtZXRlciBzdHJpbmdcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7c3RyaW5nfVxuXHRcdFx0ICovXG5cdFx0XHRxdWVyeVN0cmluZyA9ICc/JyArICh3aW5kb3cubG9jYXRpb24uc2VhcmNoXG5cdFx0XHRcdFx0LnJlcGxhY2UoL1xcPy8sICcnKVxuXHRcdFx0XHRcdC5yZXBsYWNlKC9jSUQ9W1xcZF0rL2csICcnKVxuXHRcdFx0XHRcdC5yZXBsYWNlKC9hY3Rpb249W1xcd10rL2csICcnKVxuXHRcdFx0XHRcdC5yZXBsYWNlKC9wYWdlVG9rZW49W1xcd10rL2csICcnKVxuXHRcdFx0XHRcdC5jb25jYXQoJyYnKVxuXHRcdFx0XHRcdC5yZXBsYWNlKC8mWyZdKy9nLCAnJicpXG5cdFx0XHRcdFx0LnJlcGxhY2UoL14mL2csICcnKSk7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBQcmVwYXJlcyBidXR0b25zIGZvciB0aGUgbW9kYWxcblx0XHQgKiBAcGFyYW0ge29iamVjdCB8IGpRdWVyeX0gJHRoYXRcblx0XHQgKiBAcmV0dXJucyB7QXJyYXl9XG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX2dldE1vZGFsQnV0dG9ucyA9IGZ1bmN0aW9uKCR0aGF0KSB7XG5cdFx0XHR2YXIgYnV0dG9ucyA9IFtdO1xuXHRcdFx0XG5cdFx0XHR2YXIgc3VibWl0QnRuLCBhYm9ydEJ0bjtcblx0XHRcdFxuXHRcdFx0c3dpdGNoIChvcHRpb25zLmFjdGlvbikge1xuXHRcdFx0XHRjYXNlICdkZWxldGUnOlxuXHRcdFx0XHRcdHN1Ym1pdEJ0biA9ICR0aGF0LmZpbmQoJ2lucHV0OmZpcnN0Jyk7XG5cdFx0XHRcdFx0YWJvcnRCdG4gPSAkdGhhdC5maW5kKCdhLmJ0bicpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCQoc3VibWl0QnRuKS5oaWRlKCk7XG5cdFx0XHRcdFx0JChhYm9ydEJ0bikuaGlkZSgpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGJ1dHRvbnMucHVzaChcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY2xvc2UnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuJyxcblx0XHRcdFx0XHRcdFx0J2NsaWNrJzogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0XHRcdFx0YWJvcnRCdG4udHJpZ2dlcignY2xpY2snKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZGVsZXRlJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHRcdFx0J2NsYXNzJzogJ2J0biBidG4tcHJpbWFyeScsXG5cdFx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRcdHZhciBvYmogPSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRwYWdlVG9rZW46ICQoJ2lucHV0W25hbWU9XCJwYWdlX3Rva2VuXCJdOmZpcnN0Jylcblx0XHRcdFx0XHRcdFx0XHRcdFx0LmF0dHIoJ3ZhbHVlJyksXG5cdFx0XHRcdFx0XHRcdFx0XHRjSUQ6IHdpbmRvdy5sb2NhdGlvbi5ocmVmLm1hdGNoKC9jSUQ9XFxkKy8pWzBdXG5cdFx0XHRcdFx0XHRcdFx0fTtcblx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHRvYmoudXJsID0gW1xuXHRcdFx0XHRcdFx0XHRcdFx0c3JjUGF0aCxcblx0XHRcdFx0XHRcdFx0XHRcdHF1ZXJ5U3RyaW5nLFxuXHRcdFx0XHRcdFx0XHRcdFx0J2FjdGlvbj1kZWxldGVjb25maXJtJyxcblx0XHRcdFx0XHRcdFx0XHRcdCcmJyArIG9iai5jSURcblx0XHRcdFx0XHRcdFx0XHRdLmpvaW4oJycpO1xuXHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdHZhciAkZm9ybSA9ICQoJzxmb3JtIG5hbWU9XCJjdXN0b21lcnNcIiBtZXRob2Q9XCJwb3N0XCIgYWN0aW9uPScgKyBvYmoudXJsICsgJz48L2Zvcm0+Jyk7XG5cdFx0XHRcdFx0XHRcdFx0JGZvcm0uYXBwZW5kKCc8aW5wdXQgdHlwZT1cImhpZGRlblwiIG5hbWU9XCJwYWdlX3Rva2VuXCIgdmFsdWU9JyArIG9iai5wYWdlVG9rZW4gKyAnPicpO1xuXHRcdFx0XHRcdFx0XHRcdCRmb3JtLmFwcGVuZCgnPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBuYW1lPVwiZGVsZXRlY29uZmlybVwiIHZhbHVlPVwiRGVsZXRlQ29uZmlybVwiPicpO1xuXHRcdFx0XHRcdFx0XHRcdCRmb3JtLmFwcGVuZFRvKCdib2R5Jyk7XG5cdFx0XHRcdFx0XHRcdFx0JGZvcm0uc3VibWl0KCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdlZGl0c3RhdHVzJzpcblx0XHRcdFx0XHRzdWJtaXRCdG4gPSAkdGhhdC5maW5kKCdpbnB1dDplcSgxKScpO1xuXHRcdFx0XHRcdGFib3J0QnRuID0gJHRoYXQuZmluZCgnYS5idG4nKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkKHN1Ym1pdEJ0bikuaGlkZSgpO1xuXHRcdFx0XHRcdCQoYWJvcnRCdG4pLmhpZGUoKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRidXR0b25zLnB1c2goXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Nsb3NlJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHRcdFx0J2NsYXNzJzogJ2J0bicsXG5cdFx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0XHRcdHdpbmRvdy5vcGVuKGFib3J0QnRuLmF0dHIoJ2hyZWYnKSwgJ19zZWxmJyk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3VwZGF0ZScsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHRcdCdjbGFzcyc6ICdidG4gYnRuLXByaW1hcnknLFxuXHRcdFx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0XHR2YXIgb2JqID0ge1xuXHRcdFx0XHRcdFx0XHRcdFx0cGFnZVRva2VuOiAkKCdpbnB1dFtuYW1lPVwicGFnZV90b2tlblwiXTpmaXJzdCcpXG5cdFx0XHRcdFx0XHRcdFx0XHRcdC5hdHRyKCd2YWx1ZScpLFxuXHRcdFx0XHRcdFx0XHRcdFx0Y0lEOiB3aW5kb3cubG9jYXRpb24uaHJlZi5tYXRjaCgvY0lEPVxcZCsvKVswXSxcblx0XHRcdFx0XHRcdFx0XHRcdHN0YXR1czogJHRoYXQuZmluZCgnc2VsZWN0JykudmFsKClcblx0XHRcdFx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdG9iai51cmwgPSBbXG5cdFx0XHRcdFx0XHRcdFx0XHRzcmNQYXRoLFxuXHRcdFx0XHRcdFx0XHRcdFx0cXVlcnlTdHJpbmcsXG5cdFx0XHRcdFx0XHRcdFx0XHQnYWN0aW9uPXN0YXR1c2NvbmZpcm0nLFxuXHRcdFx0XHRcdFx0XHRcdFx0JyYnICsgb2JqLmNJRFxuXHRcdFx0XHRcdFx0XHRcdF0uam9pbignJyk7XG5cdFx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdFx0dmFyICRmb3JtID0gJCgnPGZvcm0gbmFtZT1cImN1c3RvbWVyc1wiIG1ldGhvZD1cInBvc3RcIiBhY3Rpb249JyArIG9iai51cmwgKyAnPjwvZm9ybT4nKTtcblx0XHRcdFx0XHRcdFx0XHQkZm9ybS5hcHBlbmQoJzxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgbmFtZT1cInBhZ2VfdG9rZW5cIiB2YWx1ZT0nICsgb2JqLnBhZ2VUb2tlbiArICc+Jyk7XG5cdFx0XHRcdFx0XHRcdFx0JGZvcm0uYXBwZW5kKCc8aW5wdXQgdHlwZT1cImhpZGRlblwiIG5hbWU9XCJzdGF0dXNcIiB2YWx1ZT0nICsgb2JqLnN0YXR1cyArICc+Jyk7XG5cdFx0XHRcdFx0XHRcdFx0JGZvcm0uYXBwZW5kKCc8aW5wdXQgdHlwZT1cImhpZGRlblwiIG5hbWU9XCJzdGF0dXNjb25maXJtXCIgdmFsdWU9XCJVcGRhdGVcIj4nKTtcblx0XHRcdFx0XHRcdFx0XHQkZm9ybS5hcHBlbmRUbygnYm9keScpO1xuXHRcdFx0XHRcdFx0XHRcdCRmb3JtLnN1Ym1pdCgpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnaXBsb2cnOlxuXHRcdFx0XHRcdGJ1dHRvbnMucHVzaCh7XG5cdFx0XHRcdFx0XHQndGV4dCc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdjbG9zZScsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuJyxcblx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHQkKHRoaXMpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnbmV3X21lbW8nOlxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKHN1Ym1pdEJ0bik7XG5cdFx0XHRcdFx0YnV0dG9ucy5wdXNoKHtcblx0XHRcdFx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Nsb3NlJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHRcdCdjbGFzcyc6ICdidG4nLFxuXHRcdFx0XHRcdFx0J2NsaWNrJzogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdGJ1dHRvbnMucHVzaCh7XG5cdFx0XHRcdFx0XHQndGV4dCc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdzZW5kJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHRcdCdjbGFzcyc6ICdidG4gYnRuLXByaW1hcnknLFxuXHRcdFx0XHRcdFx0J2NsaWNrJzogZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0XHRcdFx0Ly9ldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRcdFx0XHQvL2dtX2NhbmNlbCgnZ21fc2VuZF9vcmRlci5waHAnLCAnJnR5cGU9Y2FuY2VsJywgJ0NBTkNFTCcpO1xuXHRcdFx0XHRcdFx0XHQkdGhhdC5zdWJtaXQoKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdkZWxldGVfcGVyc29uYWxfZGF0YSc6XG4gICAgICAgICAgICAgICAgXHRzdWJtaXRCdG4gPSAkdGhhdC5maW5kKCdpbnB1dDpzdWJtaXQnKS5maXJzdCgpO1xuICAgICAgICAgICAgICAgICAgICBzdWJtaXRCdG4uaGlkZSgpO1xuXG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvbnMucHVzaChcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY2xvc2UnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuJyxcblx0XHRcdFx0XHRcdFx0J2NsaWNrJzogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2RlbGV0ZScsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHRcdCdjbGFzcyc6ICdidG4gYnRuLXByaW1hcnknLFxuXHRcdFx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VibWl0QnRuLmNsaWNrKCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHQpO1xuXG4gICAgICAgICAgICAgICAgXHRicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdleHBvcnRfcGVyc29uYWxfZGF0YSc6XG4gICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ0biA9ICR0aGF0LmZpbmQoJ2lucHV0OnN1Ym1pdCcpLmZpcnN0KCk7XG4gICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ0bi5oaWRlKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgYnV0dG9ucy5wdXNoKFxuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Nsb3NlJywgJ2J1dHRvbnMnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnY2xhc3MnOiAnYnRuJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnY2xpY2snOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAndGV4dCc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fRVhQT1JUJywgJ2FkbWluX2J1dHRvbnMnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnY2xhc3MnOiAnYnRuIGJ0bi1wcmltYXJ5JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnY2xpY2snOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXJsID0gYCR7JHRoYXQuYXR0cignYWN0aW9uJyl9JiR7JHRoYXQuc2VyaWFsaXplKCl9YDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93Lm9wZW4odXJsLCAnX2JsYW5rJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRyZXR1cm4gYnV0dG9ucztcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENyZWF0ZXMgZGlhbG9nIGZvciBzaW5nbGUgcmVtb3ZhbFxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9vcGVuRGVsZXRlRGlhbG9nID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQkdGhpcy5kaWFsb2coe1xuXHRcdFx0XHQndGl0bGUnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9JTkZPX0hFQURJTkdfREVMRVRFX0NVU1RPTUVSJywgJ2FkbWluX2N1c3RvbWVycycpLFxuXHRcdFx0XHQnbW9kYWwnOiB0cnVlLFxuXHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0J2J1dHRvbnMnOiBfZ2V0TW9kYWxCdXR0b25zKCR0aGlzKSxcblx0XHRcdFx0J3dpZHRoJzogNDIwLFxuXHRcdFx0XHQnY2xvc2VPbkVzY2FwZSc6IGZhbHNlLFxuXHRcdFx0XHQnb3Blbic6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQoJy51aS1kaWFsb2ctdGl0bGViYXItY2xvc2UnKS5oaWRlKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlcyBkaWFsb2cgZm9yIHNpbmdsZSBzdGF0dXMgY2hhbmdlXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHR2YXIgX29wZW5FZGl0U3RhdHVzRGlhbG9nID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQkdGhpcy5kaWFsb2coe1xuXHRcdFx0XHQndGl0bGUnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnVEVYVF9JTkZPX0hFQURJTkdfU1RBVFVTX0NVU1RPTUVSJywgJ2FkbWluX2N1c3RvbWVycycpLFxuXHRcdFx0XHQnbW9kYWwnOiB0cnVlLFxuXHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0J2J1dHRvbnMnOiBfZ2V0TW9kYWxCdXR0b25zKCR0aGlzKSxcblx0XHRcdFx0J3dpZHRoJzogNDIwLFxuXHRcdFx0XHQnY2xvc2VPbkVzY2FwZSc6IGZhbHNlLFxuXHRcdFx0XHQnb3Blbic6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdC8vIE1ha2UgU29tZSBGaXhlc1xuXHRcdFx0XHRcdCQoJy51aS1kaWFsb2ctdGl0bGViYXItY2xvc2UnKS5oaWRlKCk7XG5cdFx0XHRcdFx0JCh0aGlzKVxuXHRcdFx0XHRcdFx0LmZpbmQoJ3NlbGVjdFtuYW1lPVwic3RhdHVzXCJdJylcblx0XHRcdFx0XHRcdC5jc3Moe1xuXHRcdFx0XHRcdFx0XHR3aWR0aDogJzEwMCUnLFxuXHRcdFx0XHRcdFx0XHRoZWlnaHQ6ICczNXB4Jyxcblx0XHRcdFx0XHRcdFx0Zm9udFNpemU6ICcxMnB4J1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlcyBkaWFsb2cgZm9yIHNpbmdsZSBJUCBsb2dcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfb3BlbklwTG9nRGlhbG9nID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQkdGhpcyA9ICQoJzxkaXY+PC9kaXY+Jyk7XG5cdFx0XHRcblx0XHRcdCQoJ1tkYXRhLWlwbG9nXScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCR0aGlzLmFwcGVuZCh0aGlzKTtcblx0XHRcdFx0JHRoaXMuYXBwZW5kKCc8YnI+PGJyPicpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmFwcGVuZFRvKCdib2R5Jyk7XG5cdFx0XHQkdGhpcy5kaWFsb2coe1xuXHRcdFx0XHQndGl0bGUnOiAnSVAtTG9nJyxcblx0XHRcdFx0J21vZGFsJzogdHJ1ZSxcblx0XHRcdFx0J2RpYWxvZ0NsYXNzJzogJ2d4LWNvbnRhaW5lcicsXG5cdFx0XHRcdCdidXR0b25zJzogX2dldE1vZGFsQnV0dG9ucygkdGhpcyksXG5cdFx0XHRcdCd3aWR0aCc6IDQyMCxcblx0XHRcdFx0J2Nsb3NlT25Fc2NhcGUnOiBmYWxzZVxuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX29wZW5OZXdNZW1vRGlhbG9nID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciAkZm9ybSA9ICQoJyNjdXN0b21lcl9tZW1vX2Zvcm0nKTtcblx0XHRcdFxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFxuXHRcdFx0JGZvcm0uZGlhbG9nKHtcblx0XHRcdFx0J3RpdGxlJzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ1RFWFRfTkVXX01FTU8nLCAnYWRtaW5fY3VzdG9tZXJzJyksXG5cdFx0XHRcdCdtb2RhbCc6IHRydWUsXG5cdFx0XHRcdCdkaWFsb2dDbGFzcyc6ICdneC1jb250YWluZXInLFxuXHRcdFx0XHQnYnV0dG9ucyc6IF9nZXRNb2RhbEJ1dHRvbnMoJGZvcm0pLFxuXHRcdFx0XHQnd2lkdGgnOiA1ODBcblx0XHRcdH0pO1xuXHRcdH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENyZWF0ZXMgZGlhbG9nIGZvciB0aGUgcGVyc29uYWwgZGF0YSBkZWxldGlvblxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgdmFyIF9vcGVuRGVsZXRlUGVyc29uYWxEYXRhRGlhbG9nID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkdGhpcy5kaWFsb2coe1xuICAgICAgICAgICAgICAgICd0aXRsZSc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdkZWxldGVfcGVyc29uYWxfZGF0YScsICdhZG1pbl9jdXN0b21lcnMnKSxcbiAgICAgICAgICAgICAgICAnbW9kYWwnOiB0cnVlLFxuICAgICAgICAgICAgICAgICdkaWFsb2dDbGFzcyc6ICdneC1jb250YWluZXInLFxuICAgICAgICAgICAgICAgICdidXR0b25zJzogX2dldE1vZGFsQnV0dG9ucygkdGhpcyksXG4gICAgICAgICAgICAgICAgJ3dpZHRoJzogNDIwLFxuICAgICAgICAgICAgICAgICdjbG9zZU9uRXNjYXBlJzogZmFsc2UsXG4gICAgICAgICAgICAgICAgJ29wZW4nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgnLnVpLWRpYWxvZy10aXRsZWJhci1jbG9zZScpLmhpZGUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ3JlYXRlcyBkaWFsb2cgZm9yIHRoZSBwZXJzb25hbCBkYXRhIGV4cG9ydFxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgdmFyIF9vcGVuRXhwb3J0UGVyc29uYWxEYXRhRGlhbG9nID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkdGhpcy5kaWFsb2coe1xuICAgICAgICAgICAgICAgICd0aXRsZSc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdleHBvcnRfcGVyc29uYWxfZGF0YScsICdhZG1pbl9jdXN0b21lcnMnKSxcbiAgICAgICAgICAgICAgICAnbW9kYWwnOiB0cnVlLFxuICAgICAgICAgICAgICAgICdkaWFsb2dDbGFzcyc6ICdneC1jb250YWluZXInLFxuICAgICAgICAgICAgICAgICdidXR0b25zJzogX2dldE1vZGFsQnV0dG9ucygkdGhpcyksXG4gICAgICAgICAgICAgICAgJ3dpZHRoJzogNDIwLFxuICAgICAgICAgICAgICAgICdjbG9zZU9uRXNjYXBlJzogZmFsc2UsXG4gICAgICAgICAgICAgICAgJ29wZW4nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgnLnVpLWRpYWxvZy10aXRsZWJhci1jbG9zZScpLmhpZGUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRcblx0XHRcdHN3aXRjaCAob3B0aW9ucy5hY3Rpb24pIHtcblx0XHRcdFx0Y2FzZSAnZGVsZXRlJzpcblx0XHRcdFx0XHRfb3BlbkRlbGV0ZURpYWxvZygpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdlZGl0c3RhdHVzJzpcblx0XHRcdFx0XHRfb3BlbkVkaXRTdGF0dXNEaWFsb2coKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnaXBsb2cnOlxuXHRcdFx0XHRcdF9vcGVuSXBMb2dEaWFsb2coKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnbmV3X21lbW8nOlxuXHRcdFx0XHRcdCR0aGlzLm9uKCdjbGljaycsIF9vcGVuTmV3TWVtb0RpYWxvZyk7XG5cdFx0XHRcdFx0YnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnZGVsZXRlX3BlcnNvbmFsX2RhdGEnOlxuICAgICAgICAgICAgICAgICAgICBfb3BlbkRlbGV0ZVBlcnNvbmFsRGF0YURpYWxvZygpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdleHBvcnRfcGVyc29uYWxfZGF0YSc6XG4gICAgICAgICAgICAgICAgICAgIF9vcGVuRXhwb3J0UGVyc29uYWxEYXRhRGlhbG9nKCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
