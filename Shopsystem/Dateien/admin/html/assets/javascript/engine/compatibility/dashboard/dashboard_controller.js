'use strict';

/* --------------------------------------------------------------
 dashboard_controller.js 2018-01-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Dashboard Controller
 *
 * This controller will handle dashboard stats page (compatibility).
 *
 * @module Compatibility/dashboard_controller
 */
gx.compatibility.module('dashboard_controller', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', jse.source + '/vendor/DateJS/date.min.js', 'datatable', 'user_configuration_service', 'xhr'],

/**  @lends module:Compatibility/dashboard_controller */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Last Orders Table Selector
  *
  * @var {object}
  */
	$lastOrdersTable = $this.find('.latest-orders-table'),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		'collapsed': false
	},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * UserConfiguration Service Alias
  */
	userConfigurationService = jse.libs.user_configuration_service,


	/**
  * Statistics Element Selectors
  */
	$dropdown = $('.js-interval-dropdown'),
	    $container = $dropdown.parents('.toolbar:first'),
	    $statisticChartTab = $('.statistic-chart'),
	    $statisticGrid = $('#statistic-grid'),
	    $statisticChart = $statisticChartTab.find('#dashboard-chart'),
	    $itemDropdown = $('.statistic-chart-dropdown'),
	    $tabDropdown = $('.statistic-tab-dropdown'),
	    $tabs = $('.ui-tabs'),


	/**
  * Module
  *
  * @type {object}
  */
	module = {},
	    orderStatusData;

	/**
  * Get badge class (gx-admin.css) for graphical representation of the order status.
  *
  * @param {object} rowData Contains all the row data.
  *
  * @return {string} Returns the correct badge class for the order (e.g. "badge-success", "badge-danger" ...)
  */
	var _getBadgeClass = function _getBadgeClass(rowData) {
		switch (rowData.orders_status) {
			case '1':
				return 'badge badge-warning';
			case '2':
				return 'badge badge-primary';
			case '3':
			case '7':
			case '149':
			case '161':
			case '163':
				return 'badge badge-success';
			case '0':
			case '6':
			case '99':
			case '162':
			case '171':
				return 'badge badge-danger';
			default:
				return '';
		}
	};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * On row click event go to the order details page.
  *
  * @param {object} event
  */
	var _onRowClick = function _onRowClick(event) {
		$(this).parent('tr').find('td:eq(0) a').get(0).click(); // click first cell link
	};

	/**
  * Initializes statistic-related stuff (specially interval dropdown actions)
  */
	var _initStatistics = function _initStatistics() {

		// Configuration parameters
		var configParams = {
			userId: $dropdown.data('userId'),
			configurationKey: 'statisticsInterval'
		};

		// Function to execute after getting configuration value from server.
		var prepare = function prepare(value) {
			// Select right value
			$dropdown.find('option[value="' + value + '"]').prop('selected', true);

			// Show dropdown again
			$container.animate({
				opacity: 1
			}, 'slow');

			// Performs action on changing value in this dropdown
			// Update values in statistic box widgets and
			// area chart widget. Additionally the value will be saved.
			$dropdown.on('change', function (event) {
				var setConfigurationValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

				// Get selected value
				var interval = $(this).find('option:selected').val();

				// Update statistic boxes
				$statisticGrid.trigger('get:data', interval);

				// Update chart (if visible)
				if ($statisticChart.is(':visible')) {
					$statisticChart.trigger('get:data', interval);
				}

				// Save config
				if (setConfigurationValue !== false) {
					userConfigurationService.set({
						data: $.extend(configParams, {
							configurationValue: interval
						})
					});
				}
			});

			// Trigger change to refresh data on statistics.
			$(document).on('JSENGINE_INIT_FINISHED', function () {
				$dropdown.trigger('change', [false]);
			});
		};

		// Hide element (to fade it in later after performing server request)
		$container.animate({
			'opacity': 0.1
		}, 'slow');

		// Get configuration from the server.
		var value = options.statisticsInterval || 'one_week'; // Default Value
		prepare(value);
	};

	/**
  * Initialize the statistics tab.
  */
	var _initStatisticChart = function _initStatisticChart() {
		// Configuration parameters
		var configParams = {
			userId: $dropdown.data('userId'),
			configurationKey: 'statisticsChartItem'
		};

		// Function to execute after getting configuration value from server
		function prepare(item) {
			var setConfigurationValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

			// Select right value
			$itemDropdown.find('option[value="' + item + '"]').prop('selected', true);

			// Get interval value from dropdown
			var interval = $dropdown.find('option:selected').val();

			// Show dropdown again
			$itemDropdown.animate({
				'opacity': 1
			}, 'slow');

			// Update chart 
			$statisticChart.trigger('get:data', interval, item);

			// Save config
			if (setConfigurationValue) {
				userConfigurationService.set({
					data: $.extend(configParams, {
						configurationValue: item
					})
				});
			}
		}

		/**
   * Get Configuration Value from Server
   */
		function getConfigurationValue() {
			var interval = setInterval(function () {
				if ($statisticChart.is(':visible')) {
					var value = options.statisticsChartItem || 'sales'; // Default value
					prepare(value, false);
					clearInterval(interval);
				}
			}, 100);

			// Perform action on changing item value in dropdown
			$itemDropdown.off().on('change', function () {
				var item = $(this).find('option:selected').val();
				prepare(item);
			});
		}

		// Perform actions on opening tab.
		$('a[href="#chart"]').off().on('click', getConfigurationValue);
	};

	var _initTabSelector = function _initTabSelector() {
		var configParams = {
			userId: $dropdown.data('userId'),
			configurationKey: 'statisticsTab'
		};

		$tabDropdown.on('change', function (event) {
			var setConfigurationValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

			var value = $(this).find('option:selected').val();
			$tabs.trigger('show:tab', value);

			if (setConfigurationValue !== false) {
				userConfigurationService.set({
					data: $.extend(configParams, {
						configurationValue: value
					})
				});
			}
		});

		function prepare(value) {
			$tabDropdown.find('option[value="' + value + '"]').prop('selected', true).trigger('change', [false]);
		}

		var value = options.statisticsTab !== '' ? options.statisticsTab : 1; // Default Value
		prepare(value);
	};

	var _initDashboardToggler = function _initDashboardToggler() {
		var $toggler = $('<i class="fa fa-angle-double-up"></i>');

		if (options.collapsed) {
			$toggler = $('<i class="fa fa-angle-double-down"></i>');
			$('.dashboard-chart').hide();
		}

		$this.find('.dashboard-toggler').append($toggler);
		$toggler.on('click', _toggleDashboard);
	};

	var _toggleDashboard = function _toggleDashboard(event, $toggler) {
		var configParams = {
			userId: $dropdown.data('userId'),
			configurationKey: 'dashboard_chart_collapse',
			configurationValue: !options.collapsed
		};

		options.collapsed = !options.collapsed;
		userConfigurationService.set({
			data: configParams
		});

		if (options.collapsed) {
			$('.dashboard-chart').slideUp();
		} else {
			$('.statistic-tab-dropdown').trigger('change');
			$('.dashboard-chart').slideDown();
		}

		$this.find('.dashboard-toggler i').toggleClass('fa-angle-double-down');
		$this.find('.dashboard-toggler i').toggleClass('fa-angle-double-up');
	};

	var _createLatestOrdersTable = function _createLatestOrdersTable() {
		// Get latest orders and create a new DataTable instance.
		jse.libs.datatable.create($lastOrdersTable, {
			processing: true,
			dom: 't',
			ordering: false,
			ajax: jse.core.config.get('appUrl') + '/admin/admin.php?do=Dashboard/GetLatestOrders',
			language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
			order: [[3, 'desc']],
			columns: [
			// Order ID
			{
				data: 'orders_id',
				className: 'text-right',
				render: function render(data, type, row, meta) {
					return '<a href="orders.php?page=1&oID=' + data + '&action=edit">' + data + '</a>';
				}
			},
			// Customer's name
			{
				data: 'customers_name'
			},
			// Order total in text format
			{
				data: 'text',
				className: 'text-right'
			}, {
				data: 'date_purchased',
				render: function render(data, type, row, meta) {
					var dt = Date.parse(data); // using datejs
					return dt.toString('dd.MM.yyyy HH:mm');
				}
			},
			// Payment method
			{
				data: 'payment_method'
			},
			// Order Status name
			{
				data: 'orders_status_name',
				render: function render(data, type, row, meta) {
					var className = _getBadgeClass(row);
					var i;
					var bgColor;
					var color;

					if (undefined === orderStatusData) {
						return '<span class="badge ' + className + '">' + data + '</span>';
					}

					for (i = 0; i < orderStatusData.length; i++) {
						if (orderStatusData[i].id === Number(row.orders_status)) {
							bgColor = orderStatusData[i].backgroundColor;
							color = orderStatusData[i].color;
							break;
						}
					}

					// console.log(orderStatusData);

					return '<span class="badge" style="background-color: #' + bgColor + '; background-image: none; color: #' + color + '">' + data + '</span>';
				}
			}]
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Initialize the dashboard statistics.
		_initStatistics();
		_initStatisticChart();
		_initTabSelector();
		_initDashboardToggler();

		// fetch order status data before creating latest orders table
		if (options.orders_allowed) {
			jse.libs.xhr.get({
				url: './admin.php?do=OrderStatusAjax'
			}).done(function (r) {
				orderStatusData = r;
				_createLatestOrdersTable();
			}).fail(function (r) {
				return _createLatestOrdersTable();
			});
		}

		$lastOrdersTable.on('init.dt', function () {
			// Bind row click event only if there are rows in the table.
			if ($lastOrdersTable.DataTable().data().length > 0) {
				$lastOrdersTable.on('click', 'tbody tr td', _onRowClick);
			}

			// Show the cursor as a pointer for each row.
			$this.find('tr:not(":eq(0)")').addClass('cursor-pointer');
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhc2hib2FyZC9kYXNoYm9hcmRfY29udHJvbGxlci5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJGxhc3RPcmRlcnNUYWJsZSIsImZpbmQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJ1c2VyQ29uZmlndXJhdGlvblNlcnZpY2UiLCJsaWJzIiwidXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UiLCIkZHJvcGRvd24iLCIkY29udGFpbmVyIiwicGFyZW50cyIsIiRzdGF0aXN0aWNDaGFydFRhYiIsIiRzdGF0aXN0aWNHcmlkIiwiJHN0YXRpc3RpY0NoYXJ0IiwiJGl0ZW1Ecm9wZG93biIsIiR0YWJEcm9wZG93biIsIiR0YWJzIiwib3JkZXJTdGF0dXNEYXRhIiwiX2dldEJhZGdlQ2xhc3MiLCJyb3dEYXRhIiwib3JkZXJzX3N0YXR1cyIsIl9vblJvd0NsaWNrIiwiZXZlbnQiLCJwYXJlbnQiLCJnZXQiLCJjbGljayIsIl9pbml0U3RhdGlzdGljcyIsImNvbmZpZ1BhcmFtcyIsInVzZXJJZCIsImNvbmZpZ3VyYXRpb25LZXkiLCJwcmVwYXJlIiwidmFsdWUiLCJwcm9wIiwiYW5pbWF0ZSIsIm9wYWNpdHkiLCJvbiIsInNldENvbmZpZ3VyYXRpb25WYWx1ZSIsImludGVydmFsIiwidmFsIiwidHJpZ2dlciIsImlzIiwic2V0IiwiY29uZmlndXJhdGlvblZhbHVlIiwiZG9jdW1lbnQiLCJzdGF0aXN0aWNzSW50ZXJ2YWwiLCJfaW5pdFN0YXRpc3RpY0NoYXJ0IiwiaXRlbSIsImdldENvbmZpZ3VyYXRpb25WYWx1ZSIsInNldEludGVydmFsIiwic3RhdGlzdGljc0NoYXJ0SXRlbSIsImNsZWFySW50ZXJ2YWwiLCJvZmYiLCJfaW5pdFRhYlNlbGVjdG9yIiwic3RhdGlzdGljc1RhYiIsIl9pbml0RGFzaGJvYXJkVG9nZ2xlciIsIiR0b2dnbGVyIiwiY29sbGFwc2VkIiwiaGlkZSIsImFwcGVuZCIsIl90b2dnbGVEYXNoYm9hcmQiLCJzbGlkZVVwIiwic2xpZGVEb3duIiwidG9nZ2xlQ2xhc3MiLCJfY3JlYXRlTGF0ZXN0T3JkZXJzVGFibGUiLCJkYXRhdGFibGUiLCJjcmVhdGUiLCJwcm9jZXNzaW5nIiwiZG9tIiwib3JkZXJpbmciLCJhamF4IiwiY29yZSIsImNvbmZpZyIsImxhbmd1YWdlIiwiZ2V0VHJhbnNsYXRpb25zIiwib3JkZXIiLCJjb2x1bW5zIiwiY2xhc3NOYW1lIiwicmVuZGVyIiwidHlwZSIsInJvdyIsIm1ldGEiLCJkdCIsIkRhdGUiLCJwYXJzZSIsInRvU3RyaW5nIiwiaSIsImJnQ29sb3IiLCJjb2xvciIsInVuZGVmaW5lZCIsImxlbmd0aCIsImlkIiwiTnVtYmVyIiwiYmFja2dyb3VuZENvbG9yIiwiaW5pdCIsImRvbmUiLCJvcmRlcnNfYWxsb3dlZCIsInhociIsInVybCIsInIiLCJmYWlsIiwiRGF0YVRhYmxlIiwiYWRkQ2xhc3MiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7OztBQU9BQSxHQUFHQyxhQUFILENBQWlCQyxNQUFqQixDQUNDLHNCQURELEVBR0MsQ0FDSUMsSUFBSUMsTUFEUixtREFFSUQsSUFBSUMsTUFGUixrREFHSUQsSUFBSUMsTUFIUixpQ0FJQyxXQUpELEVBS0MsNEJBTEQsRUFNQyxLQU5ELENBSEQ7O0FBWUM7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLG9CQUFtQkYsTUFBTUcsSUFBTixDQUFXLHNCQUFYLENBYnBCOzs7QUFlQzs7Ozs7QUFLQUMsWUFBVztBQUNWLGVBQWE7QUFESCxFQXBCWjs7O0FBd0JDOzs7OztBQUtBQyxXQUFVSixFQUFFSyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCTCxJQUE3QixDQTdCWDs7O0FBK0JDOzs7QUFHQVEsNEJBQTJCVixJQUFJVyxJQUFKLENBQVNDLDBCQWxDckM7OztBQW9DQzs7O0FBR0FDLGFBQVlULEVBQUUsdUJBQUYsQ0F2Q2I7QUFBQSxLQXdDQ1UsYUFBYUQsVUFBVUUsT0FBVixDQUFrQixnQkFBbEIsQ0F4Q2Q7QUFBQSxLQXlDQ0MscUJBQXFCWixFQUFFLGtCQUFGLENBekN0QjtBQUFBLEtBMENDYSxpQkFBaUJiLEVBQUUsaUJBQUYsQ0ExQ2xCO0FBQUEsS0EyQ0NjLGtCQUFrQkYsbUJBQW1CVixJQUFuQixDQUF3QixrQkFBeEIsQ0EzQ25CO0FBQUEsS0E0Q0NhLGdCQUFnQmYsRUFBRSwyQkFBRixDQTVDakI7QUFBQSxLQTZDQ2dCLGVBQWVoQixFQUFFLHlCQUFGLENBN0NoQjtBQUFBLEtBOENDaUIsUUFBUWpCLEVBQUUsVUFBRixDQTlDVDs7O0FBZ0RDOzs7OztBQUtBTCxVQUFTLEVBckRWO0FBQUEsS0F1REN1QixlQXZERDs7QUF5REE7Ozs7Ozs7QUFPQSxLQUFJQyxpQkFBaUIsU0FBakJBLGNBQWlCLENBQVNDLE9BQVQsRUFBa0I7QUFDdEMsVUFBUUEsUUFBUUMsYUFBaEI7QUFDQyxRQUFLLEdBQUw7QUFDQyxXQUFPLHFCQUFQO0FBQ0QsUUFBSyxHQUFMO0FBQ0MsV0FBTyxxQkFBUDtBQUNELFFBQUssR0FBTDtBQUNBLFFBQUssR0FBTDtBQUNBLFFBQUssS0FBTDtBQUNBLFFBQUssS0FBTDtBQUNBLFFBQUssS0FBTDtBQUNDLFdBQU8scUJBQVA7QUFDRCxRQUFLLEdBQUw7QUFDQSxRQUFLLEdBQUw7QUFDQSxRQUFLLElBQUw7QUFDQSxRQUFLLEtBQUw7QUFDQSxRQUFLLEtBQUw7QUFDQyxXQUFPLG9CQUFQO0FBQ0Q7QUFDQyxXQUFPLEVBQVA7QUFsQkY7QUFvQkEsRUFyQkQ7O0FBdUJBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxLQUFJQyxjQUFjLFNBQWRBLFdBQWMsQ0FBU0MsS0FBVCxFQUFnQjtBQUNqQ3ZCLElBQUUsSUFBRixFQUFRd0IsTUFBUixDQUFlLElBQWYsRUFBcUJ0QixJQUFyQixDQUEwQixZQUExQixFQUF3Q3VCLEdBQXhDLENBQTRDLENBQTVDLEVBQStDQyxLQUEvQyxHQURpQyxDQUN1QjtBQUN4RCxFQUZEOztBQUlBOzs7QUFHQSxLQUFJQyxrQkFBa0IsU0FBbEJBLGVBQWtCLEdBQVc7O0FBRWhDO0FBQ0EsTUFBSUMsZUFBZTtBQUNsQkMsV0FBUXBCLFVBQVVYLElBQVYsQ0FBZSxRQUFmLENBRFU7QUFFbEJnQyxxQkFBa0I7QUFGQSxHQUFuQjs7QUFLQTtBQUNBLE1BQUlDLFVBQVUsU0FBVkEsT0FBVSxDQUFTQyxLQUFULEVBQWdCO0FBQzdCO0FBQ0F2QixhQUNFUCxJQURGLENBQ08sbUJBQW1COEIsS0FBbkIsR0FBMkIsSUFEbEMsRUFFRUMsSUFGRixDQUVPLFVBRlAsRUFFbUIsSUFGbkI7O0FBSUE7QUFDQXZCLGNBQVd3QixPQUFYLENBQW1CO0FBQ2xCQyxhQUFTO0FBRFMsSUFBbkIsRUFFRyxNQUZIOztBQUlBO0FBQ0E7QUFDQTtBQUNBMUIsYUFBVTJCLEVBQVYsQ0FBYSxRQUFiLEVBQXVCLFVBQVNiLEtBQVQsRUFBOEM7QUFBQSxRQUE5QmMscUJBQThCLHVFQUFOLElBQU07O0FBQ3BFO0FBQ0EsUUFBSUMsV0FBV3RDLEVBQUUsSUFBRixFQUFRRSxJQUFSLENBQWEsaUJBQWIsRUFBZ0NxQyxHQUFoQyxFQUFmOztBQUVBO0FBQ0ExQixtQkFBZTJCLE9BQWYsQ0FBdUIsVUFBdkIsRUFBbUNGLFFBQW5DOztBQUVBO0FBQ0EsUUFBSXhCLGdCQUFnQjJCLEVBQWhCLENBQW1CLFVBQW5CLENBQUosRUFBb0M7QUFDbkMzQixxQkFBZ0IwQixPQUFoQixDQUF3QixVQUF4QixFQUFvQ0YsUUFBcEM7QUFDQTs7QUFFRDtBQUNBLFFBQUlELDBCQUEwQixLQUE5QixFQUFxQztBQUNwQy9CLDhCQUF5Qm9DLEdBQXpCLENBQTZCO0FBQzVCNUMsWUFBTUUsRUFBRUssTUFBRixDQUFTdUIsWUFBVCxFQUF1QjtBQUM1QmUsMkJBQW9CTDtBQURRLE9BQXZCO0FBRHNCLE1BQTdCO0FBS0E7QUFDRCxJQXBCRDs7QUFzQkE7QUFDQXRDLEtBQUU0QyxRQUFGLEVBQVlSLEVBQVosQ0FBZSx3QkFBZixFQUF5QyxZQUFXO0FBQ25EM0IsY0FBVStCLE9BQVYsQ0FBa0IsUUFBbEIsRUFBNEIsQ0FBQyxLQUFELENBQTVCO0FBQ0EsSUFGRDtBQUdBLEdBeENEOztBQTBDQTtBQUNBOUIsYUFBV3dCLE9BQVgsQ0FBbUI7QUFDbEIsY0FBVztBQURPLEdBQW5CLEVBRUcsTUFGSDs7QUFJQTtBQUNBLE1BQUlGLFFBQVE1QixRQUFReUMsa0JBQVIsSUFBOEIsVUFBMUMsQ0F6RGdDLENBeURzQjtBQUN0RGQsVUFBUUMsS0FBUjtBQUNBLEVBM0REOztBQTZEQTs7O0FBR0EsS0FBSWMsc0JBQXNCLFNBQXRCQSxtQkFBc0IsR0FBVztBQUNwQztBQUNBLE1BQUlsQixlQUFlO0FBQ2xCQyxXQUFRcEIsVUFBVVgsSUFBVixDQUFlLFFBQWYsQ0FEVTtBQUVsQmdDLHFCQUFrQjtBQUZBLEdBQW5COztBQUtBO0FBQ0EsV0FBU0MsT0FBVCxDQUFpQmdCLElBQWpCLEVBQXFEO0FBQUEsT0FBOUJWLHFCQUE4Qix1RUFBTixJQUFNOztBQUNwRDtBQUNBdEIsaUJBQ0ViLElBREYsQ0FDTyxtQkFBbUI2QyxJQUFuQixHQUEwQixJQURqQyxFQUVFZCxJQUZGLENBRU8sVUFGUCxFQUVtQixJQUZuQjs7QUFJQTtBQUNBLE9BQUlLLFdBQVc3QixVQUFVUCxJQUFWLENBQWUsaUJBQWYsRUFBa0NxQyxHQUFsQyxFQUFmOztBQUVBO0FBQ0F4QixpQkFBY21CLE9BQWQsQ0FBc0I7QUFDckIsZUFBVztBQURVLElBQXRCLEVBRUcsTUFGSDs7QUFJQTtBQUNBcEIsbUJBQWdCMEIsT0FBaEIsQ0FBd0IsVUFBeEIsRUFBb0NGLFFBQXBDLEVBQThDUyxJQUE5Qzs7QUFFQTtBQUNBLE9BQUlWLHFCQUFKLEVBQTJCO0FBQzFCL0IsNkJBQXlCb0MsR0FBekIsQ0FBNkI7QUFDNUI1QyxXQUFNRSxFQUFFSyxNQUFGLENBQVN1QixZQUFULEVBQXVCO0FBQzVCZSwwQkFBb0JJO0FBRFEsTUFBdkI7QUFEc0IsS0FBN0I7QUFLQTtBQUNEOztBQUVEOzs7QUFHQSxXQUFTQyxxQkFBVCxHQUFpQztBQUNoQyxPQUFJVixXQUFXVyxZQUFZLFlBQVc7QUFDckMsUUFBSW5DLGdCQUFnQjJCLEVBQWhCLENBQW1CLFVBQW5CLENBQUosRUFBb0M7QUFDbkMsU0FBSVQsUUFBUTVCLFFBQVE4QyxtQkFBUixJQUErQixPQUEzQyxDQURtQyxDQUNpQjtBQUNwRG5CLGFBQVFDLEtBQVIsRUFBZSxLQUFmO0FBQ0FtQixtQkFBY2IsUUFBZDtBQUNBO0FBQ0QsSUFOYyxFQU1aLEdBTlksQ0FBZjs7QUFRQTtBQUNBdkIsaUJBQ0VxQyxHQURGLEdBRUVoQixFQUZGLENBRUssUUFGTCxFQUVlLFlBQVc7QUFDeEIsUUFBSVcsT0FBTy9DLEVBQUUsSUFBRixFQUFRRSxJQUFSLENBQWEsaUJBQWIsRUFBZ0NxQyxHQUFoQyxFQUFYO0FBQ0FSLFlBQVFnQixJQUFSO0FBQ0EsSUFMRjtBQU1BOztBQUVEO0FBQ0EvQyxJQUFFLGtCQUFGLEVBQ0VvRCxHQURGLEdBRUVoQixFQUZGLENBRUssT0FGTCxFQUVjWSxxQkFGZDtBQUlBLEVBN0REOztBQStEQSxLQUFJSyxtQkFBbUIsU0FBbkJBLGdCQUFtQixHQUFXO0FBQ2pDLE1BQUl6QixlQUFlO0FBQ2xCQyxXQUFRcEIsVUFBVVgsSUFBVixDQUFlLFFBQWYsQ0FEVTtBQUVsQmdDLHFCQUFrQjtBQUZBLEdBQW5COztBQUtBZCxlQUFhb0IsRUFBYixDQUFnQixRQUFoQixFQUEwQixVQUFTYixLQUFULEVBQThDO0FBQUEsT0FBOUJjLHFCQUE4Qix1RUFBTixJQUFNOztBQUN2RSxPQUFJTCxRQUFRaEMsRUFBRSxJQUFGLEVBQVFFLElBQVIsQ0FBYSxpQkFBYixFQUFnQ3FDLEdBQWhDLEVBQVo7QUFDQXRCLFNBQU11QixPQUFOLENBQWMsVUFBZCxFQUEwQlIsS0FBMUI7O0FBRUEsT0FBSUssMEJBQTBCLEtBQTlCLEVBQXFDO0FBQ3BDL0IsNkJBQXlCb0MsR0FBekIsQ0FBNkI7QUFDNUI1QyxXQUFNRSxFQUFFSyxNQUFGLENBQVN1QixZQUFULEVBQXVCO0FBQzVCZSwwQkFBb0JYO0FBRFEsTUFBdkI7QUFEc0IsS0FBN0I7QUFLQTtBQUNELEdBWEQ7O0FBYUEsV0FBU0QsT0FBVCxDQUFpQkMsS0FBakIsRUFBd0I7QUFDdkJoQixnQkFDRWQsSUFERixDQUNPLG1CQUFtQjhCLEtBQW5CLEdBQTJCLElBRGxDLEVBRUVDLElBRkYsQ0FFTyxVQUZQLEVBRW1CLElBRm5CLEVBR0VPLE9BSEYsQ0FHVSxRQUhWLEVBR29CLENBQUMsS0FBRCxDQUhwQjtBQUlBOztBQUVELE1BQUlSLFFBQVE1QixRQUFRa0QsYUFBUixLQUEwQixFQUExQixHQUErQmxELFFBQVFrRCxhQUF2QyxHQUF1RCxDQUFuRSxDQTFCaUMsQ0EwQnFDO0FBQ3RFdkIsVUFBUUMsS0FBUjtBQUNBLEVBNUJEOztBQThCQSxLQUFJdUIsd0JBQXdCLFNBQXhCQSxxQkFBd0IsR0FBVztBQUN0QyxNQUFJQyxXQUFXeEQsRUFBRSx1Q0FBRixDQUFmOztBQUVBLE1BQUlJLFFBQVFxRCxTQUFaLEVBQXVCO0FBQ3RCRCxjQUFXeEQsRUFBRSx5Q0FBRixDQUFYO0FBQ0FBLEtBQUUsa0JBQUYsRUFBc0IwRCxJQUF0QjtBQUNBOztBQUVEM0QsUUFBTUcsSUFBTixDQUFXLG9CQUFYLEVBQWlDeUQsTUFBakMsQ0FBd0NILFFBQXhDO0FBQ0FBLFdBQVNwQixFQUFULENBQVksT0FBWixFQUFxQndCLGdCQUFyQjtBQUNBLEVBVkQ7O0FBWUEsS0FBSUEsbUJBQW1CLFNBQW5CQSxnQkFBbUIsQ0FBU3JDLEtBQVQsRUFBZ0JpQyxRQUFoQixFQUEwQjtBQUNoRCxNQUFJNUIsZUFBZTtBQUNsQkMsV0FBUXBCLFVBQVVYLElBQVYsQ0FBZSxRQUFmLENBRFU7QUFFbEJnQyxxQkFBa0IsMEJBRkE7QUFHbEJhLHVCQUFvQixDQUFDdkMsUUFBUXFEO0FBSFgsR0FBbkI7O0FBTUFyRCxVQUFRcUQsU0FBUixHQUFvQixDQUFDckQsUUFBUXFELFNBQTdCO0FBQ0FuRCwyQkFBeUJvQyxHQUF6QixDQUE2QjtBQUM1QjVDLFNBQU04QjtBQURzQixHQUE3Qjs7QUFJQSxNQUFJeEIsUUFBUXFELFNBQVosRUFBdUI7QUFDdEJ6RCxLQUFFLGtCQUFGLEVBQXNCNkQsT0FBdEI7QUFDQSxHQUZELE1BRU87QUFDTjdELEtBQUUseUJBQUYsRUFBNkJ3QyxPQUE3QixDQUFxQyxRQUFyQztBQUNBeEMsS0FBRSxrQkFBRixFQUFzQjhELFNBQXRCO0FBQ0E7O0FBRUQvRCxRQUFNRyxJQUFOLENBQVcsc0JBQVgsRUFBbUM2RCxXQUFuQyxDQUErQyxzQkFBL0M7QUFDQWhFLFFBQU1HLElBQU4sQ0FBVyxzQkFBWCxFQUFtQzZELFdBQW5DLENBQStDLG9CQUEvQztBQUNBLEVBckJEOztBQXVCQSxLQUFJQywyQkFBMkIsU0FBM0JBLHdCQUEyQixHQUFXO0FBQ3pDO0FBQ0FwRSxNQUFJVyxJQUFKLENBQVMwRCxTQUFULENBQW1CQyxNQUFuQixDQUEwQmpFLGdCQUExQixFQUE0QztBQUMzQ2tFLGVBQVksSUFEK0I7QUFFM0NDLFFBQUssR0FGc0M7QUFHM0NDLGFBQVUsS0FIaUM7QUFJM0NDLFNBQU0xRSxJQUFJMkUsSUFBSixDQUFTQyxNQUFULENBQWdCL0MsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsK0NBSks7QUFLM0NnRCxhQUFVN0UsSUFBSVcsSUFBSixDQUFTMEQsU0FBVCxDQUFtQlMsZUFBbkIsQ0FBbUM5RSxJQUFJMkUsSUFBSixDQUFTQyxNQUFULENBQWdCL0MsR0FBaEIsQ0FBb0IsY0FBcEIsQ0FBbkMsQ0FMaUM7QUFNM0NrRCxVQUFPLENBQUMsQ0FBQyxDQUFELEVBQUksTUFBSixDQUFELENBTm9DO0FBTzNDQyxZQUFTO0FBQ1I7QUFDQTtBQUNDOUUsVUFBTSxXQURQO0FBRUMrRSxlQUFXLFlBRlo7QUFHQ0MsWUFBUSxnQkFBU2hGLElBQVQsRUFBZWlGLElBQWYsRUFBcUJDLEdBQXJCLEVBQTBCQyxJQUExQixFQUFnQztBQUN2QyxZQUFPLG9DQUFvQ25GLElBQXBDLEdBQTJDLGdCQUEzQyxHQUE4REEsSUFBOUQsR0FBcUUsTUFBNUU7QUFDQTtBQUxGLElBRlE7QUFTUjtBQUNBO0FBQ0NBLFVBQU07QUFEUCxJQVZRO0FBYVI7QUFDQTtBQUNDQSxVQUFNLE1BRFA7QUFFQytFLGVBQVc7QUFGWixJQWRRLEVBa0JSO0FBQ0MvRSxVQUFNLGdCQURQO0FBRUNnRixZQUFRLGdCQUFTaEYsSUFBVCxFQUFlaUYsSUFBZixFQUFxQkMsR0FBckIsRUFBMEJDLElBQTFCLEVBQWdDO0FBQ3ZDLFNBQUlDLEtBQUtDLEtBQUtDLEtBQUwsQ0FBV3RGLElBQVgsQ0FBVCxDQUR1QyxDQUNaO0FBQzNCLFlBQU9vRixHQUFHRyxRQUFILENBQVksa0JBQVosQ0FBUDtBQUNBO0FBTEYsSUFsQlE7QUF5QlI7QUFDQTtBQUNDdkYsVUFBTTtBQURQLElBMUJRO0FBNkJSO0FBQ0E7QUFDQ0EsVUFBTSxvQkFEUDtBQUVDZ0YsWUFBUSxnQkFBU2hGLElBQVQsRUFBZWlGLElBQWYsRUFBcUJDLEdBQXJCLEVBQTBCQyxJQUExQixFQUFnQztBQUN2QyxTQUFJSixZQUFZMUQsZUFBZTZELEdBQWYsQ0FBaEI7QUFDQSxTQUFJTSxDQUFKO0FBQ0EsU0FBSUMsT0FBSjtBQUNBLFNBQUlDLEtBQUo7O0FBRUEsU0FBSUMsY0FBY3ZFLGVBQWxCLEVBQW1DO0FBQ2xDLGFBQU8sd0JBQXdCMkQsU0FBeEIsR0FBb0MsSUFBcEMsR0FBMkMvRSxJQUEzQyxHQUFrRCxTQUF6RDtBQUNBOztBQUVELFVBQUt3RixJQUFJLENBQVQsRUFBWUEsSUFBSXBFLGdCQUFnQndFLE1BQWhDLEVBQXdDSixHQUF4QyxFQUE2QztBQUM1QyxVQUFJcEUsZ0JBQWdCb0UsQ0FBaEIsRUFBbUJLLEVBQW5CLEtBQTBCQyxPQUFPWixJQUFJM0QsYUFBWCxDQUE5QixFQUF5RDtBQUN4RGtFLGlCQUFVckUsZ0JBQWdCb0UsQ0FBaEIsRUFBbUJPLGVBQTdCO0FBQ0FMLGVBQVF0RSxnQkFBZ0JvRSxDQUFoQixFQUFtQkUsS0FBM0I7QUFDQTtBQUNBO0FBQ0Q7O0FBRUQ7O0FBRUEsWUFBTyxtREFBbURELE9BQW5ELEdBQ0osb0NBREksR0FDbUNDLEtBRG5DLEdBQzJDLElBRDNDLEdBQ2tEMUYsSUFEbEQsR0FDeUQsU0FEaEU7QUFFQTtBQXhCRixJQTlCUTtBQVBrQyxHQUE1QztBQWlFQSxFQW5FRDs7QUFxRUE7QUFDQTtBQUNBOztBQUVBSCxRQUFPbUcsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjtBQUNBcEU7QUFDQW1CO0FBQ0FPO0FBQ0FFOztBQUVBO0FBQ0EsTUFBR25ELFFBQVE0RixjQUFYLEVBQTJCO0FBQzFCcEcsT0FBSVcsSUFBSixDQUFTMEYsR0FBVCxDQUFheEUsR0FBYixDQUFpQjtBQUNoQnlFLFNBQUs7QUFEVyxJQUFqQixFQUVHSCxJQUZILENBRVEsYUFBSztBQUNaN0Usc0JBQWtCaUYsQ0FBbEI7QUFDQW5DO0FBQ0EsSUFMRCxFQUtHb0MsSUFMSCxDQUtRO0FBQUEsV0FBS3BDLDBCQUFMO0FBQUEsSUFMUjtBQU1BOztBQUVEL0QsbUJBQWlCbUMsRUFBakIsQ0FBb0IsU0FBcEIsRUFBK0IsWUFBVztBQUN6QztBQUNBLE9BQUluQyxpQkFBaUJvRyxTQUFqQixHQUE2QnZHLElBQTdCLEdBQW9DNEYsTUFBcEMsR0FBNkMsQ0FBakQsRUFBb0Q7QUFDbkR6RixxQkFBaUJtQyxFQUFqQixDQUFvQixPQUFwQixFQUE2QixhQUE3QixFQUE0Q2QsV0FBNUM7QUFDQTs7QUFFRDtBQUNBdkIsU0FBTUcsSUFBTixDQUFXLGtCQUFYLEVBQStCb0csUUFBL0IsQ0FBd0MsZ0JBQXhDO0FBQ0EsR0FSRDs7QUFVQVA7QUFDQSxFQTVCRDs7QUE4QkEsUUFBT3BHLE1BQVA7QUFDQSxDQXJhRiIsImZpbGUiOiJkYXNoYm9hcmQvZGFzaGJvYXJkX2NvbnRyb2xsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGRhc2hib2FyZF9jb250cm9sbGVyLmpzIDIwMTgtMDEtMjRcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIERhc2hib2FyZCBDb250cm9sbGVyXG4gKlxuICogVGhpcyBjb250cm9sbGVyIHdpbGwgaGFuZGxlIGRhc2hib2FyZCBzdGF0cyBwYWdlIChjb21wYXRpYmlsaXR5KS5cbiAqXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvZGFzaGJvYXJkX2NvbnRyb2xsZXJcbiAqL1xuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdkYXNoYm9hcmRfY29udHJvbGxlcicsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmpzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvRGF0ZUpTL2RhdGUubWluLmpzYCxcblx0XHQnZGF0YXRhYmxlJyxcblx0XHQndXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UnLFxuXHRcdCd4aHInXG5cdF0sXG5cdFxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9kYXNoYm9hcmRfY29udHJvbGxlciAqL1xuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBMYXN0IE9yZGVycyBUYWJsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JGxhc3RPcmRlcnNUYWJsZSA9ICR0aGlzLmZpbmQoJy5sYXRlc3Qtb3JkZXJzLXRhYmxlJyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdCdjb2xsYXBzZWQnOiBmYWxzZVxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBVc2VyQ29uZmlndXJhdGlvbiBTZXJ2aWNlIEFsaWFzXG5cdFx0XHQgKi9cblx0XHRcdHVzZXJDb25maWd1cmF0aW9uU2VydmljZSA9IGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFN0YXRpc3RpY3MgRWxlbWVudCBTZWxlY3RvcnNcblx0XHRcdCAqL1xuXHRcdFx0JGRyb3Bkb3duID0gJCgnLmpzLWludGVydmFsLWRyb3Bkb3duJyksXG5cdFx0XHQkY29udGFpbmVyID0gJGRyb3Bkb3duLnBhcmVudHMoJy50b29sYmFyOmZpcnN0JyksXG5cdFx0XHQkc3RhdGlzdGljQ2hhcnRUYWIgPSAkKCcuc3RhdGlzdGljLWNoYXJ0JyksXG5cdFx0XHQkc3RhdGlzdGljR3JpZCA9ICQoJyNzdGF0aXN0aWMtZ3JpZCcpLFxuXHRcdFx0JHN0YXRpc3RpY0NoYXJ0ID0gJHN0YXRpc3RpY0NoYXJ0VGFiLmZpbmQoJyNkYXNoYm9hcmQtY2hhcnQnKSxcblx0XHRcdCRpdGVtRHJvcGRvd24gPSAkKCcuc3RhdGlzdGljLWNoYXJ0LWRyb3Bkb3duJyksXG5cdFx0XHQkdGFiRHJvcGRvd24gPSAkKCcuc3RhdGlzdGljLXRhYi1kcm9wZG93bicpLFxuXHRcdFx0JHRhYnMgPSAkKCcudWktdGFicycpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZVxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9LFxuXHRcdFx0XG5cdFx0XHRvcmRlclN0YXR1c0RhdGE7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2V0IGJhZGdlIGNsYXNzIChneC1hZG1pbi5jc3MpIGZvciBncmFwaGljYWwgcmVwcmVzZW50YXRpb24gb2YgdGhlIG9yZGVyIHN0YXR1cy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSByb3dEYXRhIENvbnRhaW5zIGFsbCB0aGUgcm93IGRhdGEuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IFJldHVybnMgdGhlIGNvcnJlY3QgYmFkZ2UgY2xhc3MgZm9yIHRoZSBvcmRlciAoZS5nLiBcImJhZGdlLXN1Y2Nlc3NcIiwgXCJiYWRnZS1kYW5nZXJcIiAuLi4pXG5cdFx0ICovXG5cdFx0dmFyIF9nZXRCYWRnZUNsYXNzID0gZnVuY3Rpb24ocm93RGF0YSkge1xuXHRcdFx0c3dpdGNoIChyb3dEYXRhLm9yZGVyc19zdGF0dXMpIHtcblx0XHRcdFx0Y2FzZSAnMSc6XG5cdFx0XHRcdFx0cmV0dXJuICdiYWRnZSBiYWRnZS13YXJuaW5nJztcblx0XHRcdFx0Y2FzZSAnMic6XG5cdFx0XHRcdFx0cmV0dXJuICdiYWRnZSBiYWRnZS1wcmltYXJ5Jztcblx0XHRcdFx0Y2FzZSAnMyc6XG5cdFx0XHRcdGNhc2UgJzcnOlxuXHRcdFx0XHRjYXNlICcxNDknOlxuXHRcdFx0XHRjYXNlICcxNjEnOlxuXHRcdFx0XHRjYXNlICcxNjMnOlxuXHRcdFx0XHRcdHJldHVybiAnYmFkZ2UgYmFkZ2Utc3VjY2Vzcyc7XG5cdFx0XHRcdGNhc2UgJzAnOlxuXHRcdFx0XHRjYXNlICc2Jzpcblx0XHRcdFx0Y2FzZSAnOTknOlxuXHRcdFx0XHRjYXNlICcxNjInOlxuXHRcdFx0XHRjYXNlICcxNzEnOlxuXHRcdFx0XHRcdHJldHVybiAnYmFkZ2UgYmFkZ2UtZGFuZ2VyJztcblx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRyZXR1cm4gJyc7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9uIHJvdyBjbGljayBldmVudCBnbyB0byB0aGUgb3JkZXIgZGV0YWlscyBwYWdlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50XG5cdFx0ICovXG5cdFx0dmFyIF9vblJvd0NsaWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdCQodGhpcykucGFyZW50KCd0cicpLmZpbmQoJ3RkOmVxKDApIGEnKS5nZXQoMCkuY2xpY2soKTsgLy8gY2xpY2sgZmlyc3QgY2VsbCBsaW5rXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplcyBzdGF0aXN0aWMtcmVsYXRlZCBzdHVmZiAoc3BlY2lhbGx5IGludGVydmFsIGRyb3Bkb3duIGFjdGlvbnMpXG5cdFx0ICovXG5cdFx0dmFyIF9pbml0U3RhdGlzdGljcyA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0XG5cdFx0XHQvLyBDb25maWd1cmF0aW9uIHBhcmFtZXRlcnNcblx0XHRcdHZhciBjb25maWdQYXJhbXMgPSB7XG5cdFx0XHRcdHVzZXJJZDogJGRyb3Bkb3duLmRhdGEoJ3VzZXJJZCcpLFxuXHRcdFx0XHRjb25maWd1cmF0aW9uS2V5OiAnc3RhdGlzdGljc0ludGVydmFsJ1xuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0Ly8gRnVuY3Rpb24gdG8gZXhlY3V0ZSBhZnRlciBnZXR0aW5nIGNvbmZpZ3VyYXRpb24gdmFsdWUgZnJvbSBzZXJ2ZXIuXG5cdFx0XHR2YXIgcHJlcGFyZSA9IGZ1bmN0aW9uKHZhbHVlKSB7XG5cdFx0XHRcdC8vIFNlbGVjdCByaWdodCB2YWx1ZVxuXHRcdFx0XHQkZHJvcGRvd25cblx0XHRcdFx0XHQuZmluZCgnb3B0aW9uW3ZhbHVlPVwiJyArIHZhbHVlICsgJ1wiXScpXG5cdFx0XHRcdFx0LnByb3AoJ3NlbGVjdGVkJywgdHJ1ZSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTaG93IGRyb3Bkb3duIGFnYWluXG5cdFx0XHRcdCRjb250YWluZXIuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0b3BhY2l0eTogMVxuXHRcdFx0XHR9LCAnc2xvdycpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUGVyZm9ybXMgYWN0aW9uIG9uIGNoYW5naW5nIHZhbHVlIGluIHRoaXMgZHJvcGRvd25cblx0XHRcdFx0Ly8gVXBkYXRlIHZhbHVlcyBpbiBzdGF0aXN0aWMgYm94IHdpZGdldHMgYW5kXG5cdFx0XHRcdC8vIGFyZWEgY2hhcnQgd2lkZ2V0LiBBZGRpdGlvbmFsbHkgdGhlIHZhbHVlIHdpbGwgYmUgc2F2ZWQuXG5cdFx0XHRcdCRkcm9wZG93bi5vbignY2hhbmdlJywgZnVuY3Rpb24oZXZlbnQsIHNldENvbmZpZ3VyYXRpb25WYWx1ZSA9IHRydWUpIHtcblx0XHRcdFx0XHQvLyBHZXQgc2VsZWN0ZWQgdmFsdWVcblx0XHRcdFx0XHR2YXIgaW50ZXJ2YWwgPSAkKHRoaXMpLmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpLnZhbCgpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFVwZGF0ZSBzdGF0aXN0aWMgYm94ZXNcblx0XHRcdFx0XHQkc3RhdGlzdGljR3JpZC50cmlnZ2VyKCdnZXQ6ZGF0YScsIGludGVydmFsKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBVcGRhdGUgY2hhcnQgKGlmIHZpc2libGUpXG5cdFx0XHRcdFx0aWYgKCRzdGF0aXN0aWNDaGFydC5pcygnOnZpc2libGUnKSkge1xuXHRcdFx0XHRcdFx0JHN0YXRpc3RpY0NoYXJ0LnRyaWdnZXIoJ2dldDpkYXRhJywgaW50ZXJ2YWwpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBTYXZlIGNvbmZpZ1xuXHRcdFx0XHRcdGlmIChzZXRDb25maWd1cmF0aW9uVmFsdWUgIT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0XHR1c2VyQ29uZmlndXJhdGlvblNlcnZpY2Uuc2V0KHtcblx0XHRcdFx0XHRcdFx0ZGF0YTogJC5leHRlbmQoY29uZmlnUGFyYW1zLCB7XG5cdFx0XHRcdFx0XHRcdFx0Y29uZmlndXJhdGlvblZhbHVlOiBpbnRlcnZhbFxuXHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFRyaWdnZXIgY2hhbmdlIHRvIHJlZnJlc2ggZGF0YSBvbiBzdGF0aXN0aWNzLlxuXHRcdFx0XHQkKGRvY3VtZW50KS5vbignSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCRkcm9wZG93bi50cmlnZ2VyKCdjaGFuZ2UnLCBbZmFsc2VdKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBIaWRlIGVsZW1lbnQgKHRvIGZhZGUgaXQgaW4gbGF0ZXIgYWZ0ZXIgcGVyZm9ybWluZyBzZXJ2ZXIgcmVxdWVzdClcblx0XHRcdCRjb250YWluZXIuYW5pbWF0ZSh7XG5cdFx0XHRcdCdvcGFjaXR5JzogMC4xXG5cdFx0XHR9LCAnc2xvdycpO1xuXHRcdFx0XG5cdFx0XHQvLyBHZXQgY29uZmlndXJhdGlvbiBmcm9tIHRoZSBzZXJ2ZXIuXG5cdFx0XHR2YXIgdmFsdWUgPSBvcHRpb25zLnN0YXRpc3RpY3NJbnRlcnZhbCB8fCAnb25lX3dlZWsnOyAvLyBEZWZhdWx0IFZhbHVlXG5cdFx0XHRwcmVwYXJlKHZhbHVlKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgdGhlIHN0YXRpc3RpY3MgdGFiLlxuXHRcdCAqL1xuXHRcdHZhciBfaW5pdFN0YXRpc3RpY0NoYXJ0ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQvLyBDb25maWd1cmF0aW9uIHBhcmFtZXRlcnNcblx0XHRcdHZhciBjb25maWdQYXJhbXMgPSB7XG5cdFx0XHRcdHVzZXJJZDogJGRyb3Bkb3duLmRhdGEoJ3VzZXJJZCcpLFxuXHRcdFx0XHRjb25maWd1cmF0aW9uS2V5OiAnc3RhdGlzdGljc0NoYXJ0SXRlbSdcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdC8vIEZ1bmN0aW9uIHRvIGV4ZWN1dGUgYWZ0ZXIgZ2V0dGluZyBjb25maWd1cmF0aW9uIHZhbHVlIGZyb20gc2VydmVyXG5cdFx0XHRmdW5jdGlvbiBwcmVwYXJlKGl0ZW0sIHNldENvbmZpZ3VyYXRpb25WYWx1ZSA9IHRydWUpIHtcblx0XHRcdFx0Ly8gU2VsZWN0IHJpZ2h0IHZhbHVlXG5cdFx0XHRcdCRpdGVtRHJvcGRvd25cblx0XHRcdFx0XHQuZmluZCgnb3B0aW9uW3ZhbHVlPVwiJyArIGl0ZW0gKyAnXCJdJylcblx0XHRcdFx0XHQucHJvcCgnc2VsZWN0ZWQnLCB0cnVlKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEdldCBpbnRlcnZhbCB2YWx1ZSBmcm9tIGRyb3Bkb3duXG5cdFx0XHRcdHZhciBpbnRlcnZhbCA9ICRkcm9wZG93bi5maW5kKCdvcHRpb246c2VsZWN0ZWQnKS52YWwoKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFNob3cgZHJvcGRvd24gYWdhaW5cblx0XHRcdFx0JGl0ZW1Ecm9wZG93bi5hbmltYXRlKHtcblx0XHRcdFx0XHQnb3BhY2l0eSc6IDFcblx0XHRcdFx0fSwgJ3Nsb3cnKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFVwZGF0ZSBjaGFydCBcblx0XHRcdFx0JHN0YXRpc3RpY0NoYXJ0LnRyaWdnZXIoJ2dldDpkYXRhJywgaW50ZXJ2YWwsIGl0ZW0pO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gU2F2ZSBjb25maWdcblx0XHRcdFx0aWYgKHNldENvbmZpZ3VyYXRpb25WYWx1ZSkge1xuXHRcdFx0XHRcdHVzZXJDb25maWd1cmF0aW9uU2VydmljZS5zZXQoe1xuXHRcdFx0XHRcdFx0ZGF0YTogJC5leHRlbmQoY29uZmlnUGFyYW1zLCB7XG5cdFx0XHRcdFx0XHRcdGNvbmZpZ3VyYXRpb25WYWx1ZTogaXRlbVxuXHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEdldCBDb25maWd1cmF0aW9uIFZhbHVlIGZyb20gU2VydmVyXG5cdFx0XHQgKi9cblx0XHRcdGZ1bmN0aW9uIGdldENvbmZpZ3VyYXRpb25WYWx1ZSgpIHtcblx0XHRcdFx0dmFyIGludGVydmFsID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0aWYgKCRzdGF0aXN0aWNDaGFydC5pcygnOnZpc2libGUnKSkge1xuXHRcdFx0XHRcdFx0dmFyIHZhbHVlID0gb3B0aW9ucy5zdGF0aXN0aWNzQ2hhcnRJdGVtIHx8ICdzYWxlcyc7IC8vIERlZmF1bHQgdmFsdWVcblx0XHRcdFx0XHRcdHByZXBhcmUodmFsdWUsIGZhbHNlKTtcblx0XHRcdFx0XHRcdGNsZWFySW50ZXJ2YWwoaW50ZXJ2YWwpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSwgMTAwKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIFBlcmZvcm0gYWN0aW9uIG9uIGNoYW5naW5nIGl0ZW0gdmFsdWUgaW4gZHJvcGRvd25cblx0XHRcdFx0JGl0ZW1Ecm9wZG93blxuXHRcdFx0XHRcdC5vZmYoKVxuXHRcdFx0XHRcdC5vbignY2hhbmdlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHR2YXIgaXRlbSA9ICQodGhpcykuZmluZCgnb3B0aW9uOnNlbGVjdGVkJykudmFsKCk7XG5cdFx0XHRcdFx0XHRwcmVwYXJlKGl0ZW0pO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBQZXJmb3JtIGFjdGlvbnMgb24gb3BlbmluZyB0YWIuXG5cdFx0XHQkKCdhW2hyZWY9XCIjY2hhcnRcIl0nKVxuXHRcdFx0XHQub2ZmKClcblx0XHRcdFx0Lm9uKCdjbGljaycsIGdldENvbmZpZ3VyYXRpb25WYWx1ZSk7XG5cdFx0XHRcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfaW5pdFRhYlNlbGVjdG9yID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgY29uZmlnUGFyYW1zID0ge1xuXHRcdFx0XHR1c2VySWQ6ICRkcm9wZG93bi5kYXRhKCd1c2VySWQnKSxcblx0XHRcdFx0Y29uZmlndXJhdGlvbktleTogJ3N0YXRpc3RpY3NUYWInXG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQkdGFiRHJvcGRvd24ub24oJ2NoYW5nZScsIGZ1bmN0aW9uKGV2ZW50LCBzZXRDb25maWd1cmF0aW9uVmFsdWUgPSB0cnVlKSB7XG5cdFx0XHRcdHZhciB2YWx1ZSA9ICQodGhpcykuZmluZCgnb3B0aW9uOnNlbGVjdGVkJykudmFsKCk7XG5cdFx0XHRcdCR0YWJzLnRyaWdnZXIoJ3Nob3c6dGFiJywgdmFsdWUpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKHNldENvbmZpZ3VyYXRpb25WYWx1ZSAhPT0gZmFsc2UpIHtcblx0XHRcdFx0XHR1c2VyQ29uZmlndXJhdGlvblNlcnZpY2Uuc2V0KHtcblx0XHRcdFx0XHRcdGRhdGE6ICQuZXh0ZW5kKGNvbmZpZ1BhcmFtcywge1xuXHRcdFx0XHRcdFx0XHRjb25maWd1cmF0aW9uVmFsdWU6IHZhbHVlXG5cdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0ZnVuY3Rpb24gcHJlcGFyZSh2YWx1ZSkge1xuXHRcdFx0XHQkdGFiRHJvcGRvd25cblx0XHRcdFx0XHQuZmluZCgnb3B0aW9uW3ZhbHVlPVwiJyArIHZhbHVlICsgJ1wiXScpXG5cdFx0XHRcdFx0LnByb3AoJ3NlbGVjdGVkJywgdHJ1ZSlcblx0XHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJywgW2ZhbHNlXSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdHZhciB2YWx1ZSA9IG9wdGlvbnMuc3RhdGlzdGljc1RhYiAhPT0gJycgPyBvcHRpb25zLnN0YXRpc3RpY3NUYWIgOiAxOyAvLyBEZWZhdWx0IFZhbHVlXG5cdFx0XHRwcmVwYXJlKHZhbHVlKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfaW5pdERhc2hib2FyZFRvZ2dsZXIgPSBmdW5jdGlvbigpIHtcblx0XHRcdHZhciAkdG9nZ2xlciA9ICQoJzxpIGNsYXNzPVwiZmEgZmEtYW5nbGUtZG91YmxlLXVwXCI+PC9pPicpO1xuXHRcdFx0XG5cdFx0XHRpZiAob3B0aW9ucy5jb2xsYXBzZWQpIHtcblx0XHRcdFx0JHRvZ2dsZXIgPSAkKCc8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLWRvdWJsZS1kb3duXCI+PC9pPicpO1xuXHRcdFx0XHQkKCcuZGFzaGJvYXJkLWNoYXJ0JykuaGlkZSgpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkdGhpcy5maW5kKCcuZGFzaGJvYXJkLXRvZ2dsZXInKS5hcHBlbmQoJHRvZ2dsZXIpO1xuXHRcdFx0JHRvZ2dsZXIub24oJ2NsaWNrJywgX3RvZ2dsZURhc2hib2FyZCk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX3RvZ2dsZURhc2hib2FyZCA9IGZ1bmN0aW9uKGV2ZW50LCAkdG9nZ2xlcikge1xuXHRcdFx0dmFyIGNvbmZpZ1BhcmFtcyA9IHtcblx0XHRcdFx0dXNlcklkOiAkZHJvcGRvd24uZGF0YSgndXNlcklkJyksXG5cdFx0XHRcdGNvbmZpZ3VyYXRpb25LZXk6ICdkYXNoYm9hcmRfY2hhcnRfY29sbGFwc2UnLFxuXHRcdFx0XHRjb25maWd1cmF0aW9uVmFsdWU6ICFvcHRpb25zLmNvbGxhcHNlZFxuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0b3B0aW9ucy5jb2xsYXBzZWQgPSAhb3B0aW9ucy5jb2xsYXBzZWQ7XG5cdFx0XHR1c2VyQ29uZmlndXJhdGlvblNlcnZpY2Uuc2V0KHtcblx0XHRcdFx0ZGF0YTogY29uZmlnUGFyYW1zXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0aWYgKG9wdGlvbnMuY29sbGFwc2VkKSB7XG5cdFx0XHRcdCQoJy5kYXNoYm9hcmQtY2hhcnQnKS5zbGlkZVVwKCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQkKCcuc3RhdGlzdGljLXRhYi1kcm9wZG93bicpLnRyaWdnZXIoJ2NoYW5nZScpO1xuXHRcdFx0XHQkKCcuZGFzaGJvYXJkLWNoYXJ0Jykuc2xpZGVEb3duKCk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQoJy5kYXNoYm9hcmQtdG9nZ2xlciBpJykudG9nZ2xlQ2xhc3MoJ2ZhLWFuZ2xlLWRvdWJsZS1kb3duJyk7XG5cdFx0XHQkdGhpcy5maW5kKCcuZGFzaGJvYXJkLXRvZ2dsZXIgaScpLnRvZ2dsZUNsYXNzKCdmYS1hbmdsZS1kb3VibGUtdXAnKTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfY3JlYXRlTGF0ZXN0T3JkZXJzVGFibGUgPSBmdW5jdGlvbigpIHtcblx0XHRcdC8vIEdldCBsYXRlc3Qgb3JkZXJzIGFuZCBjcmVhdGUgYSBuZXcgRGF0YVRhYmxlIGluc3RhbmNlLlxuXHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmNyZWF0ZSgkbGFzdE9yZGVyc1RhYmxlLCB7XG5cdFx0XHRcdHByb2Nlc3Npbmc6IHRydWUsXG5cdFx0XHRcdGRvbTogJ3QnLFxuXHRcdFx0XHRvcmRlcmluZzogZmFsc2UsXG5cdFx0XHRcdGFqYXg6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89RGFzaGJvYXJkL0dldExhdGVzdE9yZGVycycsXG5cdFx0XHRcdGxhbmd1YWdlOiBqc2UubGlicy5kYXRhdGFibGUuZ2V0VHJhbnNsYXRpb25zKGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpKSxcblx0XHRcdFx0b3JkZXI6IFtbMywgJ2Rlc2MnXV0sXG5cdFx0XHRcdGNvbHVtbnM6IFtcblx0XHRcdFx0XHQvLyBPcmRlciBJRFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGRhdGE6ICdvcmRlcnNfaWQnLFxuXHRcdFx0XHRcdFx0Y2xhc3NOYW1lOiAndGV4dC1yaWdodCcsXG5cdFx0XHRcdFx0XHRyZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdywgbWV0YSkge1xuXHRcdFx0XHRcdFx0XHRyZXR1cm4gJzxhIGhyZWY9XCJvcmRlcnMucGhwP3BhZ2U9MSZvSUQ9JyArIGRhdGEgKyAnJmFjdGlvbj1lZGl0XCI+JyArIGRhdGEgKyAnPC9hPic7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHQvLyBDdXN0b21lcidzIG5hbWVcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiAnY3VzdG9tZXJzX25hbWUnXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHQvLyBPcmRlciB0b3RhbCBpbiB0ZXh0IGZvcm1hdFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGRhdGE6ICd0ZXh0Jyxcblx0XHRcdFx0XHRcdGNsYXNzTmFtZTogJ3RleHQtcmlnaHQnXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiAnZGF0ZV9wdXJjaGFzZWQnLFxuXHRcdFx0XHRcdFx0cmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHRcdFx0dmFyIGR0ID0gRGF0ZS5wYXJzZShkYXRhKTsgLy8gdXNpbmcgZGF0ZWpzXG5cdFx0XHRcdFx0XHRcdHJldHVybiBkdC50b1N0cmluZygnZGQuTU0ueXl5eSBISDptbScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0Ly8gUGF5bWVudCBtZXRob2Rcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhOiAncGF5bWVudF9tZXRob2QnXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHQvLyBPcmRlciBTdGF0dXMgbmFtZVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGRhdGE6ICdvcmRlcnNfc3RhdHVzX25hbWUnLFxuXHRcdFx0XHRcdFx0cmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHRcdFx0dmFyIGNsYXNzTmFtZSA9IF9nZXRCYWRnZUNsYXNzKHJvdyk7XG5cdFx0XHRcdFx0XHRcdHZhciBpO1xuXHRcdFx0XHRcdFx0XHR2YXIgYmdDb2xvcjtcblx0XHRcdFx0XHRcdFx0dmFyIGNvbG9yO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0aWYgKHVuZGVmaW5lZCA9PT0gb3JkZXJTdGF0dXNEYXRhKSB7XG5cdFx0XHRcdFx0XHRcdFx0cmV0dXJuICc8c3BhbiBjbGFzcz1cImJhZGdlICcgKyBjbGFzc05hbWUgKyAnXCI+JyArIGRhdGEgKyAnPC9zcGFuPic7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdGZvciAoaSA9IDA7IGkgPCBvcmRlclN0YXR1c0RhdGEubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0XHRcdFx0XHRpZiAob3JkZXJTdGF0dXNEYXRhW2ldLmlkID09PSBOdW1iZXIocm93Lm9yZGVyc19zdGF0dXMpKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRiZ0NvbG9yID0gb3JkZXJTdGF0dXNEYXRhW2ldLmJhY2tncm91bmRDb2xvcjtcblx0XHRcdFx0XHRcdFx0XHRcdGNvbG9yID0gb3JkZXJTdGF0dXNEYXRhW2ldLmNvbG9yO1xuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHQvLyBjb25zb2xlLmxvZyhvcmRlclN0YXR1c0RhdGEpO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0cmV0dXJuICc8c3BhbiBjbGFzcz1cImJhZGdlXCIgc3R5bGU9XCJiYWNrZ3JvdW5kLWNvbG9yOiAjJyArIGJnQ29sb3Jcblx0XHRcdFx0XHRcdFx0XHQrICc7IGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7IGNvbG9yOiAjJyArIGNvbG9yICsgJ1wiPicgKyBkYXRhICsgJzwvc3Bhbj4nO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBJbml0aWFsaXplIHRoZSBkYXNoYm9hcmQgc3RhdGlzdGljcy5cblx0XHRcdF9pbml0U3RhdGlzdGljcygpO1xuXHRcdFx0X2luaXRTdGF0aXN0aWNDaGFydCgpO1xuXHRcdFx0X2luaXRUYWJTZWxlY3RvcigpO1xuXHRcdFx0X2luaXREYXNoYm9hcmRUb2dnbGVyKCk7XG5cdFx0XHRcblx0XHRcdC8vIGZldGNoIG9yZGVyIHN0YXR1cyBkYXRhIGJlZm9yZSBjcmVhdGluZyBsYXRlc3Qgb3JkZXJzIHRhYmxlXG5cdFx0XHRpZihvcHRpb25zLm9yZGVyc19hbGxvd2VkKSB7XG5cdFx0XHRcdGpzZS5saWJzLnhoci5nZXQoe1xuXHRcdFx0XHRcdHVybDogJy4vYWRtaW4ucGhwP2RvPU9yZGVyU3RhdHVzQWpheCdcblx0XHRcdFx0fSkuZG9uZShyID0+IHtcblx0XHRcdFx0XHRvcmRlclN0YXR1c0RhdGEgPSByO1xuXHRcdFx0XHRcdF9jcmVhdGVMYXRlc3RPcmRlcnNUYWJsZSgpO1xuXHRcdFx0XHR9KS5mYWlsKHIgPT4gX2NyZWF0ZUxhdGVzdE9yZGVyc1RhYmxlKCkpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkbGFzdE9yZGVyc1RhYmxlLm9uKCdpbml0LmR0JywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdC8vIEJpbmQgcm93IGNsaWNrIGV2ZW50IG9ubHkgaWYgdGhlcmUgYXJlIHJvd3MgaW4gdGhlIHRhYmxlLlxuXHRcdFx0XHRpZiAoJGxhc3RPcmRlcnNUYWJsZS5EYXRhVGFibGUoKS5kYXRhKCkubGVuZ3RoID4gMCkge1xuXHRcdFx0XHRcdCRsYXN0T3JkZXJzVGFibGUub24oJ2NsaWNrJywgJ3Rib2R5IHRyIHRkJywgX29uUm93Q2xpY2spO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBTaG93IHRoZSBjdXJzb3IgYXMgYSBwb2ludGVyIGZvciBlYWNoIHJvdy5cblx0XHRcdFx0JHRoaXMuZmluZCgndHI6bm90KFwiOmVxKDApXCIpJykuYWRkQ2xhc3MoJ2N1cnNvci1wb2ludGVyJyk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
