'use strict';

/* --------------------------------------------------------------
 edit_product_controller.js 2015-09-01 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Edit product controller
 *
 * This controller contains the dynamic form changes of the new_product page.
 *
 * @module Compatibility/edit_product_controller
 */
gx.compatibility.module('edit_product_controller', [jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.min.js'],

/**  @lends module:Compatibility/edit_product_controller */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$('.delete_personal_offer').on('click', function () {
			var t_quantity = $(this).closest('.old_personal_offer').find('input[name^="products_quantity_staffel_"]').val();
			var t_group_id = '' + $(this).closest('.personal_offers').prop('id').replace('scale_price_', '');

			$(this).closest('.personal_offers').find('.added_personal_offers').append('<input type="hidden" name="delete_products_quantity_staffel_' + t_group_id + '[]" value="' + t_quantity + '" />');
			$(this).closest('.old_personal_offer').remove();

			return false;
		});

		$('.add_personal_offer').on('click', function () {
			$(this).closest('.personal_offers').find('.added_personal_offers').append($(this).closest('.personal_offers').find('.new_personal_offer').html());
			$(this).closest('.personal_offers').find('.added_personal_offers input[name^="products_quantity_staffel_"]:last').val('');
			$(this).closest('.personal_offers').find('.added_personal_offers input[name^="products_price_staffel_"]:last').val('0');

			return false;
		});

		$('input[name=products_model]').bind('change', function () {
			if ($(this).val().match(/GIFT_/g)) {
				$('select[name=products_tax_class_id]').val(0);
				$('select[name=products_tax_class_id]').attr('disabled', 'disabled');
				$('select[name=products_tax_class_id]').parent().append('<span style="display: inline-block; margin: 0 0 0 20px; color: red;">' + '<?php echo TEXT_NO_TAX_RATE_BY_GIFT; ?></span>');
			} else if ($('select[name=products_tax_class_id]').attr('disabled')) {
				$('select[name=products_tax_class_id]').removeAttr('disabled');
				$('select[name=products_tax_class_id]').parent().find('span').remove();
			}
		});

		$('.category-details').sortable({
			// axis: 'y', 
			items: '> .tab-section',
			containment: 'parent'
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2R1Y3RzL2VkaXRfcHJvZHVjdF9jb250cm9sbGVyLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJpbml0IiwiZG9uZSIsIm9uIiwidF9xdWFudGl0eSIsImNsb3Nlc3QiLCJmaW5kIiwidmFsIiwidF9ncm91cF9pZCIsInByb3AiLCJyZXBsYWNlIiwiYXBwZW5kIiwicmVtb3ZlIiwiaHRtbCIsImJpbmQiLCJtYXRjaCIsImF0dHIiLCJwYXJlbnQiLCJyZW1vdmVBdHRyIiwic29ydGFibGUiLCJpdGVtcyIsImNvbnRhaW5tZW50Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyx5QkFERCxFQUdDLENBQ0NDLElBQUlDLE1BQUosR0FBYSxxQ0FEZCxFQUVDRCxJQUFJQyxNQUFKLEdBQWEsb0NBRmQsQ0FIRDs7QUFRQzs7QUFFQSxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsWUFBVyxFQWJaOzs7QUFlQzs7Ozs7QUFLQUMsV0FBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FwQlg7OztBQXNCQzs7Ozs7QUFLQUgsVUFBUyxFQTNCVjs7QUE2QkE7QUFDQTtBQUNBOztBQUVBQSxRQUFPUyxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCTCxJQUFFLHdCQUFGLEVBQTRCTSxFQUE1QixDQUErQixPQUEvQixFQUF3QyxZQUFXO0FBQ2xELE9BQUlDLGFBQWFQLEVBQUUsSUFBRixFQUFRUSxPQUFSLENBQWdCLHFCQUFoQixFQUF1Q0MsSUFBdkMsQ0FDaEIsMkNBRGdCLEVBQzZCQyxHQUQ3QixFQUFqQjtBQUVBLE9BQUlDLGFBQWEsS0FBS1gsRUFBRSxJQUFGLEVBQVFRLE9BQVIsQ0FBZ0Isa0JBQWhCLEVBQW9DSSxJQUFwQyxDQUF5QyxJQUF6QyxFQUErQ0MsT0FBL0MsQ0FBdUQsY0FBdkQsRUFDcEIsRUFEb0IsQ0FBdEI7O0FBR0FiLEtBQUUsSUFBRixFQUFRUSxPQUFSLENBQWdCLGtCQUFoQixFQUFvQ0MsSUFBcEMsQ0FBeUMsd0JBQXpDLEVBQW1FSyxNQUFuRSxDQUNDLGlFQUFpRUgsVUFBakUsR0FDQSxhQURBLEdBQ2dCSixVQURoQixHQUVBLE1BSEQ7QUFJQVAsS0FBRSxJQUFGLEVBQVFRLE9BQVIsQ0FBZ0IscUJBQWhCLEVBQXVDTyxNQUF2Qzs7QUFFQSxVQUFPLEtBQVA7QUFDQSxHQWJEOztBQWVBZixJQUFFLHFCQUFGLEVBQXlCTSxFQUF6QixDQUE0QixPQUE1QixFQUFxQyxZQUFXO0FBQy9DTixLQUFFLElBQUYsRUFBUVEsT0FBUixDQUFnQixrQkFBaEIsRUFBb0NDLElBQXBDLENBQXlDLHdCQUF6QyxFQUFtRUssTUFBbkUsQ0FBMEVkLEVBQUUsSUFBRixFQUFRUSxPQUFSLENBQ3pFLGtCQUR5RSxFQUNyREMsSUFEcUQsQ0FFekUscUJBRnlFLEVBRWxETyxJQUZrRCxFQUExRTtBQUdBaEIsS0FBRSxJQUFGLEVBQVFRLE9BQVIsQ0FBZ0Isa0JBQWhCLEVBQW9DQyxJQUFwQyxDQUNDLHVFQURELEVBQzBFQyxHQUQxRSxDQUM4RSxFQUQ5RTtBQUVBVixLQUFFLElBQUYsRUFBUVEsT0FBUixDQUFnQixrQkFBaEIsRUFBb0NDLElBQXBDLENBQ0Msb0VBREQsRUFDdUVDLEdBRHZFLENBRUMsR0FGRDs7QUFJQSxVQUFPLEtBQVA7QUFDQSxHQVhEOztBQWFBVixJQUFFLDRCQUFGLEVBQWdDaUIsSUFBaEMsQ0FBcUMsUUFBckMsRUFBK0MsWUFBVztBQUN6RCxPQUFJakIsRUFBRSxJQUFGLEVBQVFVLEdBQVIsR0FBY1EsS0FBZCxDQUFvQixRQUFwQixDQUFKLEVBQW1DO0FBQ2xDbEIsTUFBRSxvQ0FBRixFQUF3Q1UsR0FBeEMsQ0FBNEMsQ0FBNUM7QUFDQVYsTUFBRSxvQ0FBRixFQUF3Q21CLElBQXhDLENBQTZDLFVBQTdDLEVBQXlELFVBQXpEO0FBQ0FuQixNQUFFLG9DQUFGLEVBQXdDb0IsTUFBeEMsR0FBaUROLE1BQWpELENBQ0MsMEVBQ0EsZ0RBRkQ7QUFJQSxJQVBELE1BT08sSUFBSWQsRUFBRSxvQ0FBRixFQUF3Q21CLElBQXhDLENBQTZDLFVBQTdDLENBQUosRUFBOEQ7QUFDcEVuQixNQUFFLG9DQUFGLEVBQXdDcUIsVUFBeEMsQ0FBbUQsVUFBbkQ7QUFDQXJCLE1BQUUsb0NBQUYsRUFBd0NvQixNQUF4QyxHQUFpRFgsSUFBakQsQ0FBc0QsTUFBdEQsRUFBOERNLE1BQTlEO0FBQ0E7QUFDRCxHQVpEOztBQWNBZixJQUFFLG1CQUFGLEVBQXVCc0IsUUFBdkIsQ0FBZ0M7QUFDL0I7QUFDQUMsVUFBTyxnQkFGd0I7QUFHL0JDLGdCQUFhO0FBSGtCLEdBQWhDOztBQU1BbkI7QUFDQSxFQWxERDs7QUFvREEsUUFBT1YsTUFBUDtBQUNBLENBeEdGIiwiZmlsZSI6InByb2R1Y3RzL2VkaXRfcHJvZHVjdF9jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBlZGl0X3Byb2R1Y3RfY29udHJvbGxlci5qcyAyMDE1LTA5LTAxIGdtXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNSBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBFZGl0IHByb2R1Y3QgY29udHJvbGxlclxuICpcbiAqIFRoaXMgY29udHJvbGxlciBjb250YWlucyB0aGUgZHluYW1pYyBmb3JtIGNoYW5nZXMgb2YgdGhlIG5ld19wcm9kdWN0IHBhZ2UuXG4gKlxuICogQG1vZHVsZSBDb21wYXRpYmlsaXR5L2VkaXRfcHJvZHVjdF9jb250cm9sbGVyXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnZWRpdF9wcm9kdWN0X2NvbnRyb2xsZXInLFxuXHRcblx0W1xuXHRcdGpzZS5zb3VyY2UgKyAnL3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5jc3MnLCBcblx0XHRqc2Uuc291cmNlICsgJy92ZW5kb3IvanF1ZXJ5LXVpL2pxdWVyeS11aS5taW4uanMnXG5cdF0sXG5cdFxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9lZGl0X3Byb2R1Y3RfY29udHJvbGxlciAqL1xuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JCgnLmRlbGV0ZV9wZXJzb25hbF9vZmZlcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgdF9xdWFudGl0eSA9ICQodGhpcykuY2xvc2VzdCgnLm9sZF9wZXJzb25hbF9vZmZlcicpLmZpbmQoXG5cdFx0XHRcdFx0J2lucHV0W25hbWVePVwicHJvZHVjdHNfcXVhbnRpdHlfc3RhZmZlbF9cIl0nKS52YWwoKTtcblx0XHRcdFx0dmFyIHRfZ3JvdXBfaWQgPSAnJyArICQodGhpcykuY2xvc2VzdCgnLnBlcnNvbmFsX29mZmVycycpLnByb3AoJ2lkJykucmVwbGFjZSgnc2NhbGVfcHJpY2VfJyxcblx0XHRcdFx0XHRcdCcnKTtcblx0XHRcdFx0XG5cdFx0XHRcdCQodGhpcykuY2xvc2VzdCgnLnBlcnNvbmFsX29mZmVycycpLmZpbmQoJy5hZGRlZF9wZXJzb25hbF9vZmZlcnMnKS5hcHBlbmQoXG5cdFx0XHRcdFx0JzxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgbmFtZT1cImRlbGV0ZV9wcm9kdWN0c19xdWFudGl0eV9zdGFmZmVsXycgKyB0X2dyb3VwX2lkICtcblx0XHRcdFx0XHQnW11cIiB2YWx1ZT1cIicgKyB0X3F1YW50aXR5ICtcblx0XHRcdFx0XHQnXCIgLz4nKTtcblx0XHRcdFx0JCh0aGlzKS5jbG9zZXN0KCcub2xkX3BlcnNvbmFsX29mZmVyJykucmVtb3ZlKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JCgnLmFkZF9wZXJzb25hbF9vZmZlcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkKHRoaXMpLmNsb3Nlc3QoJy5wZXJzb25hbF9vZmZlcnMnKS5maW5kKCcuYWRkZWRfcGVyc29uYWxfb2ZmZXJzJykuYXBwZW5kKCQodGhpcykuY2xvc2VzdChcblx0XHRcdFx0XHQnLnBlcnNvbmFsX29mZmVycycpLmZpbmQoXG5cdFx0XHRcdFx0Jy5uZXdfcGVyc29uYWxfb2ZmZXInKS5odG1sKCkpO1xuXHRcdFx0XHQkKHRoaXMpLmNsb3Nlc3QoJy5wZXJzb25hbF9vZmZlcnMnKS5maW5kKFxuXHRcdFx0XHRcdCcuYWRkZWRfcGVyc29uYWxfb2ZmZXJzIGlucHV0W25hbWVePVwicHJvZHVjdHNfcXVhbnRpdHlfc3RhZmZlbF9cIl06bGFzdCcpLnZhbCgnJyk7XG5cdFx0XHRcdCQodGhpcykuY2xvc2VzdCgnLnBlcnNvbmFsX29mZmVycycpLmZpbmQoXG5cdFx0XHRcdFx0Jy5hZGRlZF9wZXJzb25hbF9vZmZlcnMgaW5wdXRbbmFtZV49XCJwcm9kdWN0c19wcmljZV9zdGFmZmVsX1wiXTpsYXN0JykudmFsKFxuXHRcdFx0XHRcdCcwJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JCgnaW5wdXRbbmFtZT1wcm9kdWN0c19tb2RlbF0nKS5iaW5kKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKCQodGhpcykudmFsKCkubWF0Y2goL0dJRlRfL2cpKSB7XG5cdFx0XHRcdFx0JCgnc2VsZWN0W25hbWU9cHJvZHVjdHNfdGF4X2NsYXNzX2lkXScpLnZhbCgwKTtcblx0XHRcdFx0XHQkKCdzZWxlY3RbbmFtZT1wcm9kdWN0c190YXhfY2xhc3NfaWRdJykuYXR0cignZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcblx0XHRcdFx0XHQkKCdzZWxlY3RbbmFtZT1wcm9kdWN0c190YXhfY2xhc3NfaWRdJykucGFyZW50KCkuYXBwZW5kKFxuXHRcdFx0XHRcdFx0JzxzcGFuIHN0eWxlPVwiZGlzcGxheTogaW5saW5lLWJsb2NrOyBtYXJnaW46IDAgMCAwIDIwcHg7IGNvbG9yOiByZWQ7XCI+JyArXG5cdFx0XHRcdFx0XHQnPD9waHAgZWNobyBURVhUX05PX1RBWF9SQVRFX0JZX0dJRlQ7ID8+PC9zcGFuPidcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHR9IGVsc2UgaWYgKCQoJ3NlbGVjdFtuYW1lPXByb2R1Y3RzX3RheF9jbGFzc19pZF0nKS5hdHRyKCdkaXNhYmxlZCcpKSB7XG5cdFx0XHRcdFx0JCgnc2VsZWN0W25hbWU9cHJvZHVjdHNfdGF4X2NsYXNzX2lkXScpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG5cdFx0XHRcdFx0JCgnc2VsZWN0W25hbWU9cHJvZHVjdHNfdGF4X2NsYXNzX2lkXScpLnBhcmVudCgpLmZpbmQoJ3NwYW4nKS5yZW1vdmUoKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCQoJy5jYXRlZ29yeS1kZXRhaWxzJykuc29ydGFibGUoe1xuXHRcdFx0XHQvLyBheGlzOiAneScsIFxuXHRcdFx0XHRpdGVtczogJz4gLnRhYi1zZWN0aW9uJyxcblx0XHRcdFx0Y29udGFpbm1lbnQ6ICdwYXJlbnQnXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
