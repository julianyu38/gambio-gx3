'use strict';

/* --------------------------------------------------------------
 security_page.js 2018-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Security Page Controller
 *
 * Changing behavior in the security page.
 * Add readonly-attribute to input elements if captcha_type-dropdown value 'standard' is selected
 *
 * @module Compatibility/security_page
 */
gx.compatibility.module('security_page', [],

/**  @lends module:Compatibility/security_page */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	var _disableInputs = function _disableInputs() {
		console.log('change');
		var selectors = ['#GM_RECAPTCHA_PUBLIC_KEY', '#GM_RECAPTCHA_PRIVATE_KEY'];

		var read_only = true;
		var captchaType = $('#captcha_type').val();
		if (captchaType === 'recaptcha' || captchaType === 'recaptcha_v2') {
			read_only = false;
		}

		$.each(selectors, function () {
			$(this).attr('readonly', read_only);
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		_disableInputs();
		$this.on('change', _disableInputs);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlY3VyaXR5L3NlY3VyaXR5X3BhZ2UuanMiXSwibmFtZXMiOlsiZ3giLCJjb21wYXRpYmlsaXR5IiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9kaXNhYmxlSW5wdXRzIiwiY29uc29sZSIsImxvZyIsInNlbGVjdG9ycyIsInJlYWRfb25seSIsImNhcHRjaGFUeXBlIiwidmFsIiwiZWFjaCIsImF0dHIiLCJpbml0IiwiZG9uZSIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7O0FBUUFBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCLENBQ0MsZUFERCxFQUdDLEVBSEQ7O0FBS0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFlBQVcsRUFiWjs7O0FBZUM7Ozs7O0FBS0FDLFdBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBcEJYOzs7QUFzQkM7Ozs7O0FBS0FELFVBQVMsRUEzQlY7O0FBNkJBO0FBQ0E7QUFDQTs7QUFFQSxLQUFJTyxpQkFBaUIsU0FBakJBLGNBQWlCLEdBQVc7QUFDL0JDLFVBQVFDLEdBQVIsQ0FBWSxRQUFaO0FBQ0EsTUFBSUMsWUFBWSxDQUNmLDBCQURlLEVBRWYsMkJBRmUsQ0FBaEI7O0FBS0EsTUFBSUMsWUFBWSxJQUFoQjtBQUNBLE1BQUlDLGNBQWNULEVBQUUsZUFBRixFQUFtQlUsR0FBbkIsRUFBbEI7QUFDQSxNQUFJRCxnQkFBZ0IsV0FBaEIsSUFBK0JBLGdCQUFnQixjQUFuRCxFQUFtRTtBQUNsRUQsZUFBWSxLQUFaO0FBQ0E7O0FBRURSLElBQUVXLElBQUYsQ0FBT0osU0FBUCxFQUFrQixZQUFXO0FBQzVCUCxLQUFFLElBQUYsRUFBUVksSUFBUixDQUFhLFVBQWIsRUFBeUJKLFNBQXpCO0FBQ0EsR0FGRDtBQUdBLEVBaEJEOztBQWtCQTtBQUNBO0FBQ0E7O0FBRUFYLFFBQU9nQixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCVjtBQUNBTCxRQUFNZ0IsRUFBTixDQUFTLFFBQVQsRUFBbUJYLGNBQW5CO0FBQ0FVO0FBQ0EsRUFKRDs7QUFNQSxRQUFPakIsTUFBUDtBQUNBLENBN0VGIiwiZmlsZSI6InNlY3VyaXR5L3NlY3VyaXR5X3BhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNlY3VyaXR5X3BhZ2UuanMgMjAxOC0wMi0yM1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTggR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgU2VjdXJpdHkgUGFnZSBDb250cm9sbGVyXG4gKlxuICogQ2hhbmdpbmcgYmVoYXZpb3IgaW4gdGhlIHNlY3VyaXR5IHBhZ2UuXG4gKiBBZGQgcmVhZG9ubHktYXR0cmlidXRlIHRvIGlucHV0IGVsZW1lbnRzIGlmIGNhcHRjaGFfdHlwZS1kcm9wZG93biB2YWx1ZSAnc3RhbmRhcmQnIGlzIHNlbGVjdGVkXG4gKlxuICogQG1vZHVsZSBDb21wYXRpYmlsaXR5L3NlY3VyaXR5X3BhZ2VcbiAqL1xuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdzZWN1cml0eV9wYWdlJyxcblx0XG5cdFtdLFxuXHRcblx0LyoqICBAbGVuZHMgbW9kdWxlOkNvbXBhdGliaWxpdHkvc2VjdXJpdHlfcGFnZSAqL1xuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhciBfZGlzYWJsZUlucHV0cyA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0Y29uc29sZS5sb2coJ2NoYW5nZScpO1xuXHRcdFx0dmFyIHNlbGVjdG9ycyA9IFtcblx0XHRcdFx0JyNHTV9SRUNBUFRDSEFfUFVCTElDX0tFWScsXG5cdFx0XHRcdCcjR01fUkVDQVBUQ0hBX1BSSVZBVEVfS0VZJ1xuXHRcdFx0XTtcblx0XHRcdFxuXHRcdFx0dmFyIHJlYWRfb25seSA9IHRydWU7XG5cdFx0XHR2YXIgY2FwdGNoYVR5cGUgPSAkKCcjY2FwdGNoYV90eXBlJykudmFsKCk7XG5cdFx0XHRpZiAoY2FwdGNoYVR5cGUgPT09ICdyZWNhcHRjaGEnIHx8IGNhcHRjaGFUeXBlID09PSAncmVjYXB0Y2hhX3YyJykge1xuXHRcdFx0XHRyZWFkX29ubHkgPSBmYWxzZTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JC5lYWNoKHNlbGVjdG9ycywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCQodGhpcykuYXR0cigncmVhZG9ubHknLCByZWFkX29ubHkpO1xuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0X2Rpc2FibGVJbnB1dHMoKTtcblx0XHRcdCR0aGlzLm9uKCdjaGFuZ2UnLCBfZGlzYWJsZUlucHV0cyk7XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
