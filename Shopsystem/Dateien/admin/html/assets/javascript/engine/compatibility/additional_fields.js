'use strict';

/* --------------------------------------------------------------
 additional_fields.js 2015-09-30 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Additional Fields
 *
 * This module will handle the additional fields actions on the product page.
 *
 * @module Compatibility/additional_fields
 */
gx.compatibility.module('additional_fields', [],

/**  @lends module:Compatibility/additional_fields */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Count var for adding new fields
  *
  * @type {int}
  */
	newFieldFormCount = 1,


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	var _add = function _add(event) {

		event.preventDefault();

		$this.find('.additional_fields').append($this.find('.new_additional_fields').html().replace(/%/g, newFieldFormCount));

		$this.find('.additional_fields input').prop('disabled', false);
		$this.find('.additional_fields textarea').prop('disabled', false);

		$this.find('.additional_fields .delete_additional_field:last').on('click', _delete);

		newFieldFormCount++;
		$(this).blur();

		return false;
	};

	var _delete = function _delete() {
		var id = $(this).data('additional_field_id'),
		    $message = $('<div class="add-padding-10"><p>' + jse.core.lang.translate('additional_fields_delete_confirmation', 'new_product') + '</p></div>'),
		    $addtionalField = $(this).parents('tbody:first');

		$message.dialog({
			'title': '',
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': [{
				'text': jse.core.lang.translate('close', 'buttons'),
				'class': 'btn',
				'click': function click() {
					$(this).dialog('close');
				}
			}, {
				'text': jse.core.lang.translate('delete', 'buttons'),
				'class': 'btn btn-primary',
				'click': function click() {
					if (id) {
						$this.append('<input type="hidden" ' + 'name="additional_field_delete_array[]" value="' + id + '" />');
					}

					$addtionalField.remove();
					$(this).dialog('close');
				}
			}],
			'width': 420
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {

		$this.find('.add_additional_field').on('click', _add);

		$this.find('.delete_additional_field').each(function () {
			$(this).on('click', _delete);
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZGl0aW9uYWxfZmllbGRzLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJuZXdGaWVsZEZvcm1Db3VudCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9hZGQiLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiZmluZCIsImFwcGVuZCIsImh0bWwiLCJyZXBsYWNlIiwicHJvcCIsIm9uIiwiX2RlbGV0ZSIsImJsdXIiLCJpZCIsIiRtZXNzYWdlIiwianNlIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCIkYWRkdGlvbmFsRmllbGQiLCJwYXJlbnRzIiwiZGlhbG9nIiwicmVtb3ZlIiwiaW5pdCIsImRvbmUiLCJlYWNoIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyxtQkFERCxFQUdDLEVBSEQ7O0FBS0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLHFCQUFvQixDQWJyQjs7O0FBZUM7Ozs7O0FBS0FDLFlBQVcsRUFwQlo7OztBQXNCQzs7Ozs7QUFLQUMsV0FBVUgsRUFBRUksTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkosSUFBN0IsQ0EzQlg7OztBQTZCQzs7Ozs7QUFLQUQsVUFBUyxFQWxDVjs7QUFvQ0E7QUFDQTtBQUNBOztBQUVBLEtBQUlRLE9BQU8sU0FBUEEsSUFBTyxDQUFTQyxLQUFULEVBQWdCOztBQUUxQkEsUUFBTUMsY0FBTjs7QUFFQVIsUUFBTVMsSUFBTixDQUFXLG9CQUFYLEVBQWlDQyxNQUFqQyxDQUF3Q1YsTUFBTVMsSUFBTixDQUFXLHdCQUFYLEVBQXFDRSxJQUFyQyxHQUN0Q0MsT0FEc0MsQ0FDOUIsSUFEOEIsRUFDeEJWLGlCQUR3QixDQUF4Qzs7QUFHQUYsUUFBTVMsSUFBTixDQUFXLDBCQUFYLEVBQXVDSSxJQUF2QyxDQUE0QyxVQUE1QyxFQUF3RCxLQUF4RDtBQUNBYixRQUFNUyxJQUFOLENBQVcsNkJBQVgsRUFBMENJLElBQTFDLENBQStDLFVBQS9DLEVBQTJELEtBQTNEOztBQUVBYixRQUFNUyxJQUFOLENBQVcsa0RBQVgsRUFBK0RLLEVBQS9ELENBQWtFLE9BQWxFLEVBQTJFQyxPQUEzRTs7QUFFQWI7QUFDQUQsSUFBRSxJQUFGLEVBQVFlLElBQVI7O0FBRUEsU0FBTyxLQUFQO0FBQ0EsRUFoQkQ7O0FBa0JBLEtBQUlELFVBQVUsU0FBVkEsT0FBVSxHQUFXO0FBQ3hCLE1BQUlFLEtBQUtoQixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLHFCQUFiLENBQVQ7QUFBQSxNQUNDbUIsV0FBV2pCLEVBQUUsb0NBQW9Da0IsSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FDL0MsdUNBRCtDLEVBRS9DLGFBRitDLENBQXBDLEdBRU0sWUFGUixDQURaO0FBQUEsTUFJQ0Msa0JBQWtCdEIsRUFBRSxJQUFGLEVBQVF1QixPQUFSLENBQWdCLGFBQWhCLENBSm5COztBQU1BTixXQUFTTyxNQUFULENBQWdCO0FBQ2YsWUFBUyxFQURNO0FBRWYsWUFBUyxJQUZNO0FBR2Ysa0JBQWUsY0FIQTtBQUlmLGNBQVcsQ0FDVjtBQUNDLFlBQVFOLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFNBQWpDLENBRFQ7QUFFQyxhQUFTLEtBRlY7QUFHQyxhQUFTLGlCQUFXO0FBQ25CckIsT0FBRSxJQUFGLEVBQVF3QixNQUFSLENBQWUsT0FBZjtBQUNBO0FBTEYsSUFEVSxFQVFWO0FBQ0MsWUFBUU4sSUFBSUMsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsUUFBeEIsRUFBa0MsU0FBbEMsQ0FEVDtBQUVDLGFBQVMsaUJBRlY7QUFHQyxhQUFTLGlCQUFXO0FBQ25CLFNBQUlMLEVBQUosRUFBUTtBQUNQakIsWUFBTVUsTUFBTixDQUFhLDBCQUNWLGdEQURVLEdBQ3lDTyxFQUR6QyxHQUM4QyxNQUQzRDtBQUVBOztBQUVETSxxQkFBZ0JHLE1BQWhCO0FBQ0F6QixPQUFFLElBQUYsRUFBUXdCLE1BQVIsQ0FBZSxPQUFmO0FBQ0E7QUFYRixJQVJVLENBSkk7QUEwQmYsWUFBUztBQTFCTSxHQUFoQjtBQTRCQSxFQW5DRDs7QUFxQ0E7QUFDQTtBQUNBOztBQUVBM0IsUUFBTzZCLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7O0FBRTVCNUIsUUFBTVMsSUFBTixDQUFXLHVCQUFYLEVBQW9DSyxFQUFwQyxDQUF1QyxPQUF2QyxFQUFnRFIsSUFBaEQ7O0FBRUFOLFFBQU1TLElBQU4sQ0FBVywwQkFBWCxFQUF1Q29CLElBQXZDLENBQTRDLFlBQVc7QUFDdEQ1QixLQUFFLElBQUYsRUFBUWEsRUFBUixDQUFXLE9BQVgsRUFBb0JDLE9BQXBCO0FBQ0EsR0FGRDs7QUFJQWE7QUFDQSxFQVREOztBQVdBLFFBQU85QixNQUFQO0FBQ0EsQ0E5SEYiLCJmaWxlIjoiYWRkaXRpb25hbF9maWVsZHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGFkZGl0aW9uYWxfZmllbGRzLmpzIDIwMTUtMDktMzAgZ21cbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE1IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqICMjIEFkZGl0aW9uYWwgRmllbGRzXG4gKlxuICogVGhpcyBtb2R1bGUgd2lsbCBoYW5kbGUgdGhlIGFkZGl0aW9uYWwgZmllbGRzIGFjdGlvbnMgb24gdGhlIHByb2R1Y3QgcGFnZS5cbiAqXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvYWRkaXRpb25hbF9maWVsZHNcbiAqL1xuZ3guY29tcGF0aWJpbGl0eS5tb2R1bGUoXG5cdCdhZGRpdGlvbmFsX2ZpZWxkcycsXG5cdFxuXHRbXSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb21wYXRpYmlsaXR5L2FkZGl0aW9uYWxfZmllbGRzICovXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFUyBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIENvdW50IHZhciBmb3IgYWRkaW5nIG5ldyBmaWVsZHNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7aW50fVxuXHRcdFx0ICovXG5cdFx0XHRuZXdGaWVsZEZvcm1Db3VudCA9IDEsXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXIgX2FkZCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQoJy5hZGRpdGlvbmFsX2ZpZWxkcycpLmFwcGVuZCgkdGhpcy5maW5kKCcubmV3X2FkZGl0aW9uYWxfZmllbGRzJykuaHRtbCgpXG5cdFx0XHRcdC5yZXBsYWNlKC8lL2csIG5ld0ZpZWxkRm9ybUNvdW50KSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQoJy5hZGRpdGlvbmFsX2ZpZWxkcyBpbnB1dCcpLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuXHRcdFx0JHRoaXMuZmluZCgnLmFkZGl0aW9uYWxfZmllbGRzIHRleHRhcmVhJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQoJy5hZGRpdGlvbmFsX2ZpZWxkcyAuZGVsZXRlX2FkZGl0aW9uYWxfZmllbGQ6bGFzdCcpLm9uKCdjbGljaycsIF9kZWxldGUpO1xuXHRcdFx0XG5cdFx0XHRuZXdGaWVsZEZvcm1Db3VudCsrO1xuXHRcdFx0JCh0aGlzKS5ibHVyKCk7XG5cdFx0XHRcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfZGVsZXRlID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgaWQgPSAkKHRoaXMpLmRhdGEoJ2FkZGl0aW9uYWxfZmllbGRfaWQnKSxcblx0XHRcdFx0JG1lc3NhZ2UgPSAkKCc8ZGl2IGNsYXNzPVwiYWRkLXBhZGRpbmctMTBcIj48cD4nICsganNlLmNvcmUubGFuZy50cmFuc2xhdGUoXG5cdFx0XHRcdFx0XHQnYWRkaXRpb25hbF9maWVsZHNfZGVsZXRlX2NvbmZpcm1hdGlvbicsXG5cdFx0XHRcdFx0XHQnbmV3X3Byb2R1Y3QnKSArICc8L3A+PC9kaXY+JyksXG5cdFx0XHRcdCRhZGR0aW9uYWxGaWVsZCA9ICQodGhpcykucGFyZW50cygndGJvZHk6Zmlyc3QnKTtcblx0XHRcdFxuXHRcdFx0JG1lc3NhZ2UuZGlhbG9nKHtcblx0XHRcdFx0J3RpdGxlJzogJycsXG5cdFx0XHRcdCdtb2RhbCc6IHRydWUsXG5cdFx0XHRcdCdkaWFsb2dDbGFzcyc6ICdneC1jb250YWluZXInLFxuXHRcdFx0XHQnYnV0dG9ucyc6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHQndGV4dCc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdjbG9zZScsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuJyxcblx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHQkKHRoaXMpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdCd0ZXh0JzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2RlbGV0ZScsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuIGJ0bi1wcmltYXJ5Jyxcblx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRpZiAoaWQpIHtcblx0XHRcdFx0XHRcdFx0XHQkdGhpcy5hcHBlbmQoJzxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgJ1xuXHRcdFx0XHRcdFx0XHRcdFx0KyAnbmFtZT1cImFkZGl0aW9uYWxfZmllbGRfZGVsZXRlX2FycmF5W11cIiB2YWx1ZT1cIicgKyBpZCArICdcIiAvPicpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHQkYWRkdGlvbmFsRmllbGQucmVtb3ZlKCk7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuZGlhbG9nKCdjbG9zZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XSxcblx0XHRcdFx0J3dpZHRoJzogNDIwXG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQoJy5hZGRfYWRkaXRpb25hbF9maWVsZCcpLm9uKCdjbGljaycsIF9hZGQpO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5maW5kKCcuZGVsZXRlX2FkZGl0aW9uYWxfZmllbGQnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkKHRoaXMpLm9uKCdjbGljaycsIF9kZWxldGUpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
