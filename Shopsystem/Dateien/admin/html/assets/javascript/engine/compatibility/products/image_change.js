'use strict';

/* --------------------------------------------------------------
 image_change.js 2018-04-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Products Image Change Module
 *
 * This module is responsible for effects on image changes.
 *
 * @module Compatibility/image_change
 */
gx.compatibility.module(
// Module name
'image_change',

// Module dependencies
['image_resizer', 'xhr', 'modal', jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.js'],

/** @lends module:Compatibility/image_change */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	// Shortcut to module element.

	var $this = $(this);

	// Shortcut to image resizer libary.
	var resize = jse.libs.image_resizer.resize;

	// AJAX request library
	var xhr = jse.libs.xhr;

	// Modal library
	var modal = jse.libs.modal;

	// Elements selector object.
	var selectors = {
		input: 'input:file',
		form: 'form',
		previewImage: '[data-image]',
		filenameLabel: '[data-filename-label]',
		filenameInput: '[data-filename-input]',
		showImage: '[data-show-image]',
		fileInputName: '[data-file-input-name]',
		originalImageName: '[data-original-image]'
	};

	// Module object.
	var module = {};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Handles file changes in input field.
  * @param  {jQuery.Event} event Event fired
  */
	var _onNewImage = function _onNewImage(event) {
		// Preview image.
		var $previewImage = $this.find(selectors.previewImage);

		// File put.
		var file = event.target.files[0];
		var fileName = file.name;

		if (/\.(jpg|jpeg|png|gif|bmp)$/i.test(fileName) === false) {
			modal.message({
				title: jse.core.lang.translate('GM_UPLOAD_IMAGE_MODAL_ERROR', 'gm_product_images'),
				content: jse.core.lang.translate('GM_UPLOAD_IMAGE_MODAL_INVALID_FILE_FORMAT', 'gm_product_images')
			});

			return;
		}

		// Replace some specialchars. Still allowed are these chars: . - _
		fileName = fileName.replace(/[\s!$§%#^&*()+|~=`´{}\[\]:";\'<>?,\\\/]+/g, '-');

		// Make sure that the filename is unique.
		var length = $('input[name="image_file[' + fileName + ']"]').length,
		    counter = 1;
		while (length !== 0) {
			var newFileName = fileName.replace(/(\.)/, String(counter) + '.');

			length = $('input[name="image_file[' + newFileName + ']"]').length;

			if (length === 0) {
				fileName = newFileName;
			}

			counter++;
		}

		xhr.get({ url: 'admin.php?do=MaxFileSize' }).done(function (result) {
			var maxFileSizeAllowed = result.maxFileSize;
			var actualFileSize = file.size / Math.pow(1024, 2);

			if (actualFileSize > maxFileSizeAllowed) {
				var message = jse.core.lang.translate('TXT_FILE_TOO_LARGE', 'categories');
				alert(message + maxFileSizeAllowed + ' MB');
				return;
			}
			// Create a FileReader to read the input file.
			var Reader = new FileReader();

			// As soon as the image file has been loaded,
			// the loaded image file will be put into the
			// preview image tag and finally resized and displayed.
			Reader.onload = function (event) {
				// Put loaded image file into preview image tag and resize it.
				$previewImage.attr('src', event.target.result);
				resize($previewImage);
			};

			// Load image and trigger the FileReaders' `onload` event.
			Reader.readAsDataURL(file);

			// Change text in file name label and input field.
			$this.find(selectors.filenameLabel).text(fileName);
			$this.find(selectors.filenameInput).val(fileName);
			$this.find(selectors.showImage).val(fileName);
			if (!$this.find(selectors.originalImageName).val()) {
				$this.find(selectors.originalImageName).val(fileName);
				$this.find(selectors.showImage).val(fileName);
			}
			_updateFileInputName();
		});
	};

	var _updateFileInputName = function _updateFileInputName(event) {
		$this.find(selectors.fileInputName).attr('name', 'image_file[' + $this.find(selectors.filenameInput).val() + ']');
	};

	/**
  * Handles manual filename changes in input field.
  * @param  {jQuery.Event} event Event fired
  */
	var _changeDataShowImage = function _changeDataShowImage(event) {
		// Replace some specialchars. Still allowed are these chars: . - _
		var fileName = $this.find(selectors.filenameInput).val().replace(/[\s!$§%#^&*()+|~=`´{}\[\]:";\'<>?,\\\/]+/g, '-');

		$this.find(selectors.showImage).val(fileName);
	};

	module.init = function (done) {
		// Handle file change.
		$this.find(selectors.input).on('change', _onNewImage);

		// Update name attribute of the file input
		$this.find(selectors.filenameInput).on('change', _updateFileInputName);

		// Update filename of the image show value
		$this.find(selectors.filenameInput).on('input', _changeDataShowImage);

		// Register as finished
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2R1Y3RzL2ltYWdlX2NoYW5nZS5qcyJdLCJuYW1lcyI6WyJneCIsImNvbXBhdGliaWxpdHkiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwicmVzaXplIiwibGlicyIsImltYWdlX3Jlc2l6ZXIiLCJ4aHIiLCJtb2RhbCIsInNlbGVjdG9ycyIsImlucHV0IiwiZm9ybSIsInByZXZpZXdJbWFnZSIsImZpbGVuYW1lTGFiZWwiLCJmaWxlbmFtZUlucHV0Iiwic2hvd0ltYWdlIiwiZmlsZUlucHV0TmFtZSIsIm9yaWdpbmFsSW1hZ2VOYW1lIiwiX29uTmV3SW1hZ2UiLCJldmVudCIsIiRwcmV2aWV3SW1hZ2UiLCJmaW5kIiwiZmlsZSIsInRhcmdldCIsImZpbGVzIiwiZmlsZU5hbWUiLCJuYW1lIiwidGVzdCIsIm1lc3NhZ2UiLCJ0aXRsZSIsImNvcmUiLCJsYW5nIiwidHJhbnNsYXRlIiwiY29udGVudCIsInJlcGxhY2UiLCJsZW5ndGgiLCJjb3VudGVyIiwibmV3RmlsZU5hbWUiLCJTdHJpbmciLCJnZXQiLCJ1cmwiLCJkb25lIiwicmVzdWx0IiwibWF4RmlsZVNpemVBbGxvd2VkIiwibWF4RmlsZVNpemUiLCJhY3R1YWxGaWxlU2l6ZSIsInNpemUiLCJNYXRoIiwicG93IiwiYWxlcnQiLCJSZWFkZXIiLCJGaWxlUmVhZGVyIiwib25sb2FkIiwiYXR0ciIsInJlYWRBc0RhdGFVUkwiLCJ0ZXh0IiwidmFsIiwiX3VwZGF0ZUZpbGVJbnB1dE5hbWUiLCJfY2hhbmdlRGF0YVNob3dJbWFnZSIsImluaXQiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCO0FBQ0M7QUFDQSxjQUZEOztBQUlDO0FBQ0EsQ0FDQyxlQURELEVBQ2tCLEtBRGxCLEVBQ3lCLE9BRHpCLEVBRUlDLElBQUlDLE1BRlIsMENBR0lELElBQUlDLE1BSFIsb0NBTEQ7O0FBV0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFDQSxLQUFJQyxRQUFRQyxFQUFFLElBQUYsQ0FBWjs7QUFFQTtBQUNBLEtBQUlDLFNBQVNMLElBQUlNLElBQUosQ0FBU0MsYUFBVCxDQUF1QkYsTUFBcEM7O0FBRUE7QUFDQSxLQUFJRyxNQUFNUixJQUFJTSxJQUFKLENBQVNFLEdBQW5COztBQUVBO0FBQ0EsS0FBSUMsUUFBUVQsSUFBSU0sSUFBSixDQUFTRyxLQUFyQjs7QUFFQTtBQUNBLEtBQUlDLFlBQVk7QUFDZkMsU0FBTyxZQURRO0FBRWZDLFFBQU0sTUFGUztBQUdmQyxnQkFBYyxjQUhDO0FBSWZDLGlCQUFlLHVCQUpBO0FBS2ZDLGlCQUFlLHVCQUxBO0FBTWZDLGFBQVcsbUJBTkk7QUFPZkMsaUJBQWUsd0JBUEE7QUFRZkMscUJBQW1CO0FBUkosRUFBaEI7O0FBV0E7QUFDQSxLQUFJbkIsU0FBUyxFQUFiOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7OztBQUlBLEtBQUlvQixjQUFjLFNBQWRBLFdBQWMsQ0FBU0MsS0FBVCxFQUFnQjtBQUNqQztBQUNBLE1BQUlDLGdCQUFnQmxCLE1BQU1tQixJQUFOLENBQVdaLFVBQVVHLFlBQXJCLENBQXBCOztBQUVBO0FBQ0EsTUFBSVUsT0FBT0gsTUFBTUksTUFBTixDQUFhQyxLQUFiLENBQW1CLENBQW5CLENBQVg7QUFDQSxNQUFJQyxXQUFXSCxLQUFLSSxJQUFwQjs7QUFFQSxNQUFLLDRCQUFELENBQStCQyxJQUEvQixDQUFvQ0YsUUFBcEMsTUFBa0QsS0FBdEQsRUFBNkQ7QUFDNURqQixTQUFNb0IsT0FBTixDQUFjO0FBQ2JDLFdBQU85QixJQUFJK0IsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsNkJBQXhCLEVBQXVELG1CQUF2RCxDQURNO0FBRWJDLGFBQVNsQyxJQUFJK0IsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsMkNBQXhCLEVBQXFFLG1CQUFyRTtBQUZJLElBQWQ7O0FBS0E7QUFDQTs7QUFFRDtBQUNBUCxhQUFXQSxTQUFTUyxPQUFULENBQWlCLDJDQUFqQixFQUE4RCxHQUE5RCxDQUFYOztBQUVBO0FBQ0EsTUFBSUMsU0FBU2hDLEVBQUUsNEJBQTRCc0IsUUFBNUIsR0FBdUMsS0FBekMsRUFBZ0RVLE1BQTdEO0FBQUEsTUFDQ0MsVUFBVSxDQURYO0FBRUEsU0FBT0QsV0FBVyxDQUFsQixFQUFxQjtBQUNwQixPQUFJRSxjQUFjWixTQUFTUyxPQUFULENBQWlCLE1BQWpCLEVBQXlCSSxPQUFPRixPQUFQLElBQWtCLEdBQTNDLENBQWxCOztBQUVBRCxZQUFTaEMsRUFBRSw0QkFBNEJrQyxXQUE1QixHQUEwQyxLQUE1QyxFQUFtREYsTUFBNUQ7O0FBRUEsT0FBSUEsV0FBVyxDQUFmLEVBQWtCO0FBQ2pCVixlQUFXWSxXQUFYO0FBQ0E7O0FBRUREO0FBQ0E7O0FBRUQ3QixNQUFJZ0MsR0FBSixDQUFRLEVBQUNDLEtBQUssMEJBQU4sRUFBUixFQUNFQyxJQURGLENBQ08sVUFBU0MsTUFBVCxFQUFpQjtBQUN0QixPQUFJQyxxQkFBcUJELE9BQU9FLFdBQWhDO0FBQ0EsT0FBSUMsaUJBQWlCdkIsS0FBS3dCLElBQUwsR0FBWUMsS0FBS0MsR0FBTCxDQUFTLElBQVQsRUFBZSxDQUFmLENBQWpDOztBQUVBLE9BQUlILGlCQUFpQkYsa0JBQXJCLEVBQXlDO0FBQ3hDLFFBQUlmLFVBQVU3QixJQUFJK0IsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isb0JBQXhCLEVBQThDLFlBQTlDLENBQWQ7QUFDQWlCLFVBQU1yQixVQUFVZSxrQkFBVixHQUErQixLQUFyQztBQUNBO0FBQ0E7QUFDRDtBQUNBLE9BQUlPLFNBQVMsSUFBSUMsVUFBSixFQUFiOztBQUVBO0FBQ0E7QUFDQTtBQUNBRCxVQUFPRSxNQUFQLEdBQWdCLFVBQVNqQyxLQUFULEVBQWdCO0FBQy9CO0FBQ0FDLGtCQUFjaUMsSUFBZCxDQUFtQixLQUFuQixFQUEwQmxDLE1BQU1JLE1BQU4sQ0FBYW1CLE1BQXZDO0FBQ0F0QyxXQUFPZ0IsYUFBUDtBQUNBLElBSkQ7O0FBTUE7QUFDQThCLFVBQU9JLGFBQVAsQ0FBcUJoQyxJQUFyQjs7QUFFQTtBQUNBcEIsU0FBTW1CLElBQU4sQ0FBV1osVUFBVUksYUFBckIsRUFBb0MwQyxJQUFwQyxDQUF5QzlCLFFBQXpDO0FBQ0F2QixTQUFNbUIsSUFBTixDQUFXWixVQUFVSyxhQUFyQixFQUFvQzBDLEdBQXBDLENBQXdDL0IsUUFBeEM7QUFDQXZCLFNBQU1tQixJQUFOLENBQVdaLFVBQVVNLFNBQXJCLEVBQWdDeUMsR0FBaEMsQ0FBb0MvQixRQUFwQztBQUNBLE9BQUksQ0FBQ3ZCLE1BQU1tQixJQUFOLENBQVdaLFVBQVVRLGlCQUFyQixFQUF3Q3VDLEdBQXhDLEVBQUwsRUFBb0Q7QUFDbkR0RCxVQUFNbUIsSUFBTixDQUFXWixVQUFVUSxpQkFBckIsRUFBd0N1QyxHQUF4QyxDQUE0Qy9CLFFBQTVDO0FBQ0F2QixVQUFNbUIsSUFBTixDQUFXWixVQUFVTSxTQUFyQixFQUFnQ3lDLEdBQWhDLENBQW9DL0IsUUFBcEM7QUFDQTtBQUNEZ0M7QUFDQSxHQWxDRjtBQW1DQSxFQXRFRDs7QUF3RUEsS0FBSUEsdUJBQXVCLFNBQXZCQSxvQkFBdUIsQ0FBU3RDLEtBQVQsRUFBZ0I7QUFDMUNqQixRQUFNbUIsSUFBTixDQUFXWixVQUFVTyxhQUFyQixFQUNFcUMsSUFERixDQUNPLE1BRFAsRUFDZSxnQkFBZ0JuRCxNQUFNbUIsSUFBTixDQUFXWixVQUFVSyxhQUFyQixFQUFvQzBDLEdBQXBDLEVBQWhCLEdBQTRELEdBRDNFO0FBRUEsRUFIRDs7QUFLQTs7OztBQUlBLEtBQUlFLHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQVN2QyxLQUFULEVBQWdCO0FBQzFDO0FBQ0EsTUFBSU0sV0FBV3ZCLE1BQU1tQixJQUFOLENBQVdaLFVBQVVLLGFBQXJCLEVBQ2IwQyxHQURhLEdBRWJ0QixPQUZhLENBRUwsMkNBRkssRUFFd0MsR0FGeEMsQ0FBZjs7QUFJQWhDLFFBQU1tQixJQUFOLENBQVdaLFVBQVVNLFNBQXJCLEVBQWdDeUMsR0FBaEMsQ0FBb0MvQixRQUFwQztBQUNBLEVBUEQ7O0FBU0EzQixRQUFPNkQsSUFBUCxHQUFjLFVBQVNsQixJQUFULEVBQWU7QUFDNUI7QUFDQXZDLFFBQ0VtQixJQURGLENBQ09aLFVBQVVDLEtBRGpCLEVBRUVrRCxFQUZGLENBRUssUUFGTCxFQUVlMUMsV0FGZjs7QUFJQTtBQUNBaEIsUUFDRW1CLElBREYsQ0FDT1osVUFBVUssYUFEakIsRUFFRThDLEVBRkYsQ0FFSyxRQUZMLEVBRWVILG9CQUZmOztBQUlBO0FBQ0F2RCxRQUNFbUIsSUFERixDQUNPWixVQUFVSyxhQURqQixFQUVFOEMsRUFGRixDQUVLLE9BRkwsRUFFY0Ysb0JBRmQ7O0FBSUE7QUFDQWpCO0FBQ0EsRUFsQkQ7O0FBb0JBLFFBQU8zQyxNQUFQO0FBQ0EsQ0F2S0YiLCJmaWxlIjoicHJvZHVjdHMvaW1hZ2VfY2hhbmdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBpbWFnZV9jaGFuZ2UuanMgMjAxOC0wNC0wOVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTggR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgUHJvZHVjdHMgSW1hZ2UgQ2hhbmdlIE1vZHVsZVxuICpcbiAqIFRoaXMgbW9kdWxlIGlzIHJlc3BvbnNpYmxlIGZvciBlZmZlY3RzIG9uIGltYWdlIGNoYW5nZXMuXG4gKlxuICogQG1vZHVsZSBDb21wYXRpYmlsaXR5L2ltYWdlX2NoYW5nZVxuICovXG5neC5jb21wYXRpYmlsaXR5Lm1vZHVsZShcblx0Ly8gTW9kdWxlIG5hbWVcblx0J2ltYWdlX2NoYW5nZScsXG5cdFxuXHQvLyBNb2R1bGUgZGVwZW5kZW5jaWVzXG5cdFtcblx0XHQnaW1hZ2VfcmVzaXplcicsICd4aHInLCAnbW9kYWwnLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5jc3NgLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLmpzYFxuXHRdLFxuXHRcblx0LyoqIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9pbWFnZV9jaGFuZ2UgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvLyBTaG9ydGN1dCB0byBtb2R1bGUgZWxlbWVudC5cblx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8vIFNob3J0Y3V0IHRvIGltYWdlIHJlc2l6ZXIgbGliYXJ5LlxuXHRcdHZhciByZXNpemUgPSBqc2UubGlicy5pbWFnZV9yZXNpemVyLnJlc2l6ZTtcblx0XHRcblx0XHQvLyBBSkFYIHJlcXVlc3QgbGlicmFyeVxuXHRcdHZhciB4aHIgPSBqc2UubGlicy54aHI7XG5cdFx0XG5cdFx0Ly8gTW9kYWwgbGlicmFyeVxuXHRcdHZhciBtb2RhbCA9IGpzZS5saWJzLm1vZGFsO1xuXHRcdFxuXHRcdC8vIEVsZW1lbnRzIHNlbGVjdG9yIG9iamVjdC5cblx0XHR2YXIgc2VsZWN0b3JzID0ge1xuXHRcdFx0aW5wdXQ6ICdpbnB1dDpmaWxlJyxcblx0XHRcdGZvcm06ICdmb3JtJyxcblx0XHRcdHByZXZpZXdJbWFnZTogJ1tkYXRhLWltYWdlXScsXG5cdFx0XHRmaWxlbmFtZUxhYmVsOiAnW2RhdGEtZmlsZW5hbWUtbGFiZWxdJyxcblx0XHRcdGZpbGVuYW1lSW5wdXQ6ICdbZGF0YS1maWxlbmFtZS1pbnB1dF0nLFxuXHRcdFx0c2hvd0ltYWdlOiAnW2RhdGEtc2hvdy1pbWFnZV0nLFxuXHRcdFx0ZmlsZUlucHV0TmFtZTogJ1tkYXRhLWZpbGUtaW5wdXQtbmFtZV0nLFxuXHRcdFx0b3JpZ2luYWxJbWFnZU5hbWU6ICdbZGF0YS1vcmlnaW5hbC1pbWFnZV0nXG5cdFx0fTtcblx0XHRcblx0XHQvLyBNb2R1bGUgb2JqZWN0LlxuXHRcdHZhciBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBQUklWQVRFIE1FVEhPRFNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIGZpbGUgY2hhbmdlcyBpbiBpbnB1dCBmaWVsZC5cblx0XHQgKiBAcGFyYW0gIHtqUXVlcnkuRXZlbnR9IGV2ZW50IEV2ZW50IGZpcmVkXG5cdFx0ICovXG5cdFx0dmFyIF9vbk5ld0ltYWdlID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdC8vIFByZXZpZXcgaW1hZ2UuXG5cdFx0XHR2YXIgJHByZXZpZXdJbWFnZSA9ICR0aGlzLmZpbmQoc2VsZWN0b3JzLnByZXZpZXdJbWFnZSk7XG5cdFx0XHRcblx0XHRcdC8vIEZpbGUgcHV0LlxuXHRcdFx0dmFyIGZpbGUgPSBldmVudC50YXJnZXQuZmlsZXNbMF07XG5cdFx0XHR2YXIgZmlsZU5hbWUgPSBmaWxlLm5hbWU7XG5cdFx0XHRcblx0XHRcdGlmICgoL1xcLihqcGd8anBlZ3xwbmd8Z2lmfGJtcCkkL2kpLnRlc3QoZmlsZU5hbWUpID09PSBmYWxzZSkge1xuXHRcdFx0XHRtb2RhbC5tZXNzYWdlKHtcblx0XHRcdFx0XHR0aXRsZToganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0dNX1VQTE9BRF9JTUFHRV9NT0RBTF9FUlJPUicsICdnbV9wcm9kdWN0X2ltYWdlcycpLFxuXHRcdFx0XHRcdGNvbnRlbnQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdHTV9VUExPQURfSU1BR0VfTU9EQUxfSU5WQUxJRF9GSUxFX0ZPUk1BVCcsICdnbV9wcm9kdWN0X2ltYWdlcycpXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBSZXBsYWNlIHNvbWUgc3BlY2lhbGNoYXJzLiBTdGlsbCBhbGxvd2VkIGFyZSB0aGVzZSBjaGFyczogLiAtIF9cblx0XHRcdGZpbGVOYW1lID0gZmlsZU5hbWUucmVwbGFjZSgvW1xccyEkwqclI14mKigpK3x+PWDCtHt9XFxbXFxdOlwiO1xcJzw+PyxcXFxcXFwvXSsvZywgJy0nKTtcblx0XHRcdFxuXHRcdFx0Ly8gTWFrZSBzdXJlIHRoYXQgdGhlIGZpbGVuYW1lIGlzIHVuaXF1ZS5cblx0XHRcdHZhciBsZW5ndGggPSAkKCdpbnB1dFtuYW1lPVwiaW1hZ2VfZmlsZVsnICsgZmlsZU5hbWUgKyAnXVwiXScpLmxlbmd0aCxcblx0XHRcdFx0Y291bnRlciA9IDE7XG5cdFx0XHR3aGlsZSAobGVuZ3RoICE9PSAwKSB7XG5cdFx0XHRcdHZhciBuZXdGaWxlTmFtZSA9IGZpbGVOYW1lLnJlcGxhY2UoLyhcXC4pLywgU3RyaW5nKGNvdW50ZXIpICsgJy4nKTtcblx0XHRcdFx0XG5cdFx0XHRcdGxlbmd0aCA9ICQoJ2lucHV0W25hbWU9XCJpbWFnZV9maWxlWycgKyBuZXdGaWxlTmFtZSArICddXCJdJykubGVuZ3RoO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKGxlbmd0aCA9PT0gMCkge1xuXHRcdFx0XHRcdGZpbGVOYW1lID0gbmV3RmlsZU5hbWU7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGNvdW50ZXIrKztcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0eGhyLmdldCh7dXJsOiAnYWRtaW4ucGhwP2RvPU1heEZpbGVTaXplJ30pXG5cdFx0XHRcdC5kb25lKGZ1bmN0aW9uKHJlc3VsdCkge1xuXHRcdFx0XHRcdHZhciBtYXhGaWxlU2l6ZUFsbG93ZWQgPSByZXN1bHQubWF4RmlsZVNpemU7XG5cdFx0XHRcdFx0dmFyIGFjdHVhbEZpbGVTaXplID0gZmlsZS5zaXplIC8gTWF0aC5wb3coMTAyNCwgMik7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKGFjdHVhbEZpbGVTaXplID4gbWF4RmlsZVNpemVBbGxvd2VkKSB7XG5cdFx0XHRcdFx0XHR2YXIgbWVzc2FnZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUWFRfRklMRV9UT09fTEFSR0UnLCAnY2F0ZWdvcmllcycpO1xuXHRcdFx0XHRcdFx0YWxlcnQobWVzc2FnZSArIG1heEZpbGVTaXplQWxsb3dlZCArICcgTUInKTtcblx0XHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0Ly8gQ3JlYXRlIGEgRmlsZVJlYWRlciB0byByZWFkIHRoZSBpbnB1dCBmaWxlLlxuXHRcdFx0XHRcdHZhciBSZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIEFzIHNvb24gYXMgdGhlIGltYWdlIGZpbGUgaGFzIGJlZW4gbG9hZGVkLFxuXHRcdFx0XHRcdC8vIHRoZSBsb2FkZWQgaW1hZ2UgZmlsZSB3aWxsIGJlIHB1dCBpbnRvIHRoZVxuXHRcdFx0XHRcdC8vIHByZXZpZXcgaW1hZ2UgdGFnIGFuZCBmaW5hbGx5IHJlc2l6ZWQgYW5kIGRpc3BsYXllZC5cblx0XHRcdFx0XHRSZWFkZXIub25sb2FkID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0XHRcdC8vIFB1dCBsb2FkZWQgaW1hZ2UgZmlsZSBpbnRvIHByZXZpZXcgaW1hZ2UgdGFnIGFuZCByZXNpemUgaXQuXG5cdFx0XHRcdFx0XHQkcHJldmlld0ltYWdlLmF0dHIoJ3NyYycsIGV2ZW50LnRhcmdldC5yZXN1bHQpO1xuXHRcdFx0XHRcdFx0cmVzaXplKCRwcmV2aWV3SW1hZ2UpO1xuXHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gTG9hZCBpbWFnZSBhbmQgdHJpZ2dlciB0aGUgRmlsZVJlYWRlcnMnIGBvbmxvYWRgIGV2ZW50LlxuXHRcdFx0XHRcdFJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIENoYW5nZSB0ZXh0IGluIGZpbGUgbmFtZSBsYWJlbCBhbmQgaW5wdXQgZmllbGQuXG5cdFx0XHRcdFx0JHRoaXMuZmluZChzZWxlY3RvcnMuZmlsZW5hbWVMYWJlbCkudGV4dChmaWxlTmFtZSk7XG5cdFx0XHRcdFx0JHRoaXMuZmluZChzZWxlY3RvcnMuZmlsZW5hbWVJbnB1dCkudmFsKGZpbGVOYW1lKTtcblx0XHRcdFx0XHQkdGhpcy5maW5kKHNlbGVjdG9ycy5zaG93SW1hZ2UpLnZhbChmaWxlTmFtZSk7XG5cdFx0XHRcdFx0aWYgKCEkdGhpcy5maW5kKHNlbGVjdG9ycy5vcmlnaW5hbEltYWdlTmFtZSkudmFsKCkpIHtcblx0XHRcdFx0XHRcdCR0aGlzLmZpbmQoc2VsZWN0b3JzLm9yaWdpbmFsSW1hZ2VOYW1lKS52YWwoZmlsZU5hbWUpO1xuXHRcdFx0XHRcdFx0JHRoaXMuZmluZChzZWxlY3RvcnMuc2hvd0ltYWdlKS52YWwoZmlsZU5hbWUpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRfdXBkYXRlRmlsZUlucHV0TmFtZSgpO1xuXHRcdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdHZhciBfdXBkYXRlRmlsZUlucHV0TmFtZSA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHQkdGhpcy5maW5kKHNlbGVjdG9ycy5maWxlSW5wdXROYW1lKVxuXHRcdFx0XHQuYXR0cignbmFtZScsICdpbWFnZV9maWxlWycgKyAkdGhpcy5maW5kKHNlbGVjdG9ycy5maWxlbmFtZUlucHV0KS52YWwoKSArICddJyk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIG1hbnVhbCBmaWxlbmFtZSBjaGFuZ2VzIGluIGlucHV0IGZpZWxkLlxuXHRcdCAqIEBwYXJhbSAge2pRdWVyeS5FdmVudH0gZXZlbnQgRXZlbnQgZmlyZWRcblx0XHQgKi9cblx0XHR2YXIgX2NoYW5nZURhdGFTaG93SW1hZ2UgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0Ly8gUmVwbGFjZSBzb21lIHNwZWNpYWxjaGFycy4gU3RpbGwgYWxsb3dlZCBhcmUgdGhlc2UgY2hhcnM6IC4gLSBfXG5cdFx0XHR2YXIgZmlsZU5hbWUgPSAkdGhpcy5maW5kKHNlbGVjdG9ycy5maWxlbmFtZUlucHV0KVxuXHRcdFx0XHQudmFsKClcblx0XHRcdFx0LnJlcGxhY2UoL1tcXHMhJMKnJSNeJiooKSt8fj1gwrR7fVxcW1xcXTpcIjtcXCc8Pj8sXFxcXFxcL10rL2csICctJyk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQoc2VsZWN0b3JzLnNob3dJbWFnZSkudmFsKGZpbGVOYW1lKTtcblx0XHR9O1xuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0Ly8gSGFuZGxlIGZpbGUgY2hhbmdlLlxuXHRcdFx0JHRoaXNcblx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLmlucHV0KVxuXHRcdFx0XHQub24oJ2NoYW5nZScsIF9vbk5ld0ltYWdlKTtcblx0XHRcdFxuXHRcdFx0Ly8gVXBkYXRlIG5hbWUgYXR0cmlidXRlIG9mIHRoZSBmaWxlIGlucHV0XG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQuZmluZChzZWxlY3RvcnMuZmlsZW5hbWVJbnB1dClcblx0XHRcdFx0Lm9uKCdjaGFuZ2UnLCBfdXBkYXRlRmlsZUlucHV0TmFtZSk7XG5cdFx0XHRcblx0XHRcdC8vIFVwZGF0ZSBmaWxlbmFtZSBvZiB0aGUgaW1hZ2Ugc2hvdyB2YWx1ZVxuXHRcdFx0JHRoaXNcblx0XHRcdFx0LmZpbmQoc2VsZWN0b3JzLmZpbGVuYW1lSW5wdXQpXG5cdFx0XHRcdC5vbignaW5wdXQnLCBfY2hhbmdlRGF0YVNob3dJbWFnZSk7XG5cdFx0XHRcblx0XHRcdC8vIFJlZ2lzdGVyIGFzIGZpbmlzaGVkXG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
