'use strict';

/* --------------------------------------------------------------
 order_customizer.js 2017-05-24 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Orders Modal Layer Module
 *
 * This module will open a modal layer for order actions like deleting or changing the oder status.
 *
 * @module Compatibility/order_customizer
 */
gx.compatibility.module('order_customizer', [],

/**  @lends module:Compatibility/order_customizer */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ------------------------------------------------------------------------

	var _openCustomizerSet = function _openCustomizerSet(event) {

		var $customizer = $(options.selector);

		$('select[id^="element_"]').each(function (j) {
			var attr_id = $(this).attr('id');
			var attr_class = $(this).attr('class');
			var attr_name = $(this).attr('name');
			var attr_style = $(this).attr('style');
			var attr_value = $(this).children('option').val();

			$(this).replaceWith('<input type="text" name="' + attr_name + '" id="' + attr_id + '" class="' + attr_class + '" style="' + attr_style + '" value="' + attr_value + '">');
		});

		event.preventDefault();
		event.stopPropagation();
		$customizer.dialog({
			'title': jse.core.lang.translate('HEADING_GX_CUSTOMIZER', 'orders'),
			'modal': true,
			'dialogClass': 'gx-container',
			'buttons': [{
				'text': jse.core.lang.translate('save', 'buttons'),
				'class': 'btn',
				'click': function click() {
					var content_div = $customizer.children('[id^="gm_gprint_content"]');
					var _error = false;
					content_div.children('[id^="surface_"]').each(function (i) {
						var surface_id = $(this).attr('id');
						var inputs = {};
						var container_div = $(this).children('[id^="element_container_"]');

						container_div.children('input[id^="element_"]').each(function (j) {
							var input_id = $(this).attr('id');
							var input = {};
							input.id = input_id.substring(input_id.lastIndexOf('_') + 1);
							input.value = $(this).val();
							inputs[input_id] = input;
						});
						container_div.children('textarea[id^="element_"]').each(function (j) {
							var input_id = $(this).attr('id');
							var input = {};
							input.id = input_id.substring(input_id.lastIndexOf('_') + 1);
							input.value = $(this).val();
							inputs[input_id] = input;
						});

						if (Object.keys(inputs).length > 0) {
							$.ajax({
								type: 'POST',
								url: 'request_port.php?module=GPrintOrder&action=save_surfaces_group_inputs',
								dataType: 'json',
								context: this,
								async: false,
								data: {
									"inputs": inputs,
									"surface_id": surface_id.substring(surface_id.lastIndexOf('_') + 1)
								},
								success: function success() {
									console.log('gut');
								},
								error: function error() {
									console.log('error');
									_error = true;
								}
							});
						}
					});

					if (_error) {
						alert(jse.core.lang.translate('GM_GPRINT_SAVE_FAILED', 'admin_gm_gprint'));
					} else {
						alert(jse.core.lang.translate('GM_GPRINT_SAVE_SUCCESSFULL', 'admin_gm_gprint'));
					}
				}
			}, {
				'text': jse.core.lang.translate('close', 'buttons'),
				'class': 'btn',
				'click': function click() {
					$(this).dialog('close');
				}
			}],
			'width': 420,
			open: function open() {
				this.style.overflow = '';
			}
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('click', _openCustomizerSet);
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vcmRlcl9jdXN0b21pemVyLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfb3BlbkN1c3RvbWl6ZXJTZXQiLCJldmVudCIsIiRjdXN0b21pemVyIiwic2VsZWN0b3IiLCJlYWNoIiwiaiIsImF0dHJfaWQiLCJhdHRyIiwiYXR0cl9jbGFzcyIsImF0dHJfbmFtZSIsImF0dHJfc3R5bGUiLCJhdHRyX3ZhbHVlIiwiY2hpbGRyZW4iLCJ2YWwiLCJyZXBsYWNlV2l0aCIsInByZXZlbnREZWZhdWx0Iiwic3RvcFByb3BhZ2F0aW9uIiwiZGlhbG9nIiwianNlIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJjbGljayIsImNvbnRlbnRfZGl2IiwiZXJyb3IiLCJpIiwic3VyZmFjZV9pZCIsImlucHV0cyIsImNvbnRhaW5lcl9kaXYiLCJpbnB1dF9pZCIsImlucHV0IiwiaWQiLCJzdWJzdHJpbmciLCJsYXN0SW5kZXhPZiIsInZhbHVlIiwiT2JqZWN0Iiwia2V5cyIsImxlbmd0aCIsImFqYXgiLCJ0eXBlIiwidXJsIiwiZGF0YVR5cGUiLCJjb250ZXh0IiwiYXN5bmMiLCJzdWNjZXNzIiwiY29uc29sZSIsImxvZyIsImFsZXJ0Iiwib3BlbiIsInN0eWxlIiwib3ZlcmZsb3ciLCJpbml0IiwiZG9uZSIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQUEsR0FBR0MsYUFBSCxDQUFpQkMsTUFBakIsQ0FDQyxrQkFERCxFQUdDLEVBSEQ7O0FBS0M7O0FBRUEsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFlBQVcsRUFiWjs7O0FBZUM7Ozs7O0FBS0FDLFdBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBcEJYOzs7QUFzQkM7Ozs7O0FBS0FELFVBQVMsRUEzQlY7O0FBNkJBO0FBQ0E7QUFDQTs7QUFFQSxLQUFJTyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFTQyxLQUFULEVBQWdCOztBQUV4QyxNQUFJQyxjQUFjTixFQUFFRSxRQUFRSyxRQUFWLENBQWxCOztBQUVBUCxJQUFFLHdCQUFGLEVBQTRCUSxJQUE1QixDQUFpQyxVQUFTQyxDQUFULEVBQVk7QUFDNUMsT0FBSUMsVUFBVVYsRUFBRSxJQUFGLEVBQVFXLElBQVIsQ0FBYSxJQUFiLENBQWQ7QUFDQSxPQUFJQyxhQUFhWixFQUFFLElBQUYsRUFBUVcsSUFBUixDQUFhLE9BQWIsQ0FBakI7QUFDQSxPQUFJRSxZQUFZYixFQUFFLElBQUYsRUFBUVcsSUFBUixDQUFhLE1BQWIsQ0FBaEI7QUFDQSxPQUFJRyxhQUFhZCxFQUFFLElBQUYsRUFBUVcsSUFBUixDQUFhLE9BQWIsQ0FBakI7QUFDQSxPQUFJSSxhQUFhZixFQUFFLElBQUYsRUFBUWdCLFFBQVIsQ0FBaUIsUUFBakIsRUFBMkJDLEdBQTNCLEVBQWpCOztBQUVBakIsS0FBRSxJQUFGLEVBQVFrQixXQUFSLENBQW9CLDhCQUE4QkwsU0FBOUIsR0FBMEMsUUFBMUMsR0FBcURILE9BQXJELEdBQ25CLFdBRG1CLEdBQ0xFLFVBREssR0FDUSxXQURSLEdBQ3NCRSxVQUR0QixHQUNtQyxXQURuQyxHQUNpREMsVUFEakQsR0FDOEQsSUFEbEY7QUFFQSxHQVREOztBQVdBVixRQUFNYyxjQUFOO0FBQ0FkLFFBQU1lLGVBQU47QUFDQWQsY0FBWWUsTUFBWixDQUFtQjtBQUNsQixZQUFTQyxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix1QkFBeEIsRUFBaUQsUUFBakQsQ0FEUztBQUVsQixZQUFTLElBRlM7QUFHbEIsa0JBQWUsY0FIRztBQUlsQixjQUFXLENBQ1Y7QUFDQyxZQUFRSCxJQUFJQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixNQUF4QixFQUFnQyxTQUFoQyxDQURUO0FBRUMsYUFBUyxLQUZWO0FBR0MsYUFBUyxTQUFTQyxLQUFULEdBQWlCO0FBQ3pCLFNBQUlDLGNBQWNyQixZQUFZVSxRQUFaLENBQXFCLDJCQUFyQixDQUFsQjtBQUNBLFNBQUlZLFNBQVEsS0FBWjtBQUNBRCxpQkFBWVgsUUFBWixDQUFxQixrQkFBckIsRUFBeUNSLElBQXpDLENBQThDLFVBQVNxQixDQUFULEVBQVk7QUFDekQsVUFBSUMsYUFBYTlCLEVBQUUsSUFBRixFQUFRVyxJQUFSLENBQWEsSUFBYixDQUFqQjtBQUNBLFVBQUlvQixTQUFTLEVBQWI7QUFDQSxVQUFJQyxnQkFBZ0JoQyxFQUFFLElBQUYsRUFBUWdCLFFBQVIsQ0FBaUIsNEJBQWpCLENBQXBCOztBQUVBZ0Isb0JBQWNoQixRQUFkLENBQXVCLHVCQUF2QixFQUFnRFIsSUFBaEQsQ0FBcUQsVUFBU0MsQ0FBVCxFQUFZO0FBQ2hFLFdBQUl3QixXQUFXakMsRUFBRSxJQUFGLEVBQVFXLElBQVIsQ0FBYSxJQUFiLENBQWY7QUFDQSxXQUFJdUIsUUFBUSxFQUFaO0FBQ0FBLGFBQU1DLEVBQU4sR0FBV0YsU0FBU0csU0FBVCxDQUFtQkgsU0FBU0ksV0FBVCxDQUFxQixHQUFyQixJQUE0QixDQUEvQyxDQUFYO0FBQ0FILGFBQU1JLEtBQU4sR0FBY3RDLEVBQUUsSUFBRixFQUFRaUIsR0FBUixFQUFkO0FBQ0FjLGNBQU9FLFFBQVAsSUFBbUJDLEtBQW5CO0FBQ0EsT0FORDtBQU9BRixvQkFBY2hCLFFBQWQsQ0FBdUIsMEJBQXZCLEVBQW1EUixJQUFuRCxDQUF3RCxVQUFTQyxDQUFULEVBQVk7QUFDbkUsV0FBSXdCLFdBQVdqQyxFQUFFLElBQUYsRUFBUVcsSUFBUixDQUFhLElBQWIsQ0FBZjtBQUNBLFdBQUl1QixRQUFRLEVBQVo7QUFDQUEsYUFBTUMsRUFBTixHQUFXRixTQUFTRyxTQUFULENBQW1CSCxTQUFTSSxXQUFULENBQXFCLEdBQXJCLElBQTRCLENBQS9DLENBQVg7QUFDQUgsYUFBTUksS0FBTixHQUFjdEMsRUFBRSxJQUFGLEVBQVFpQixHQUFSLEVBQWQ7QUFDQWMsY0FBT0UsUUFBUCxJQUFtQkMsS0FBbkI7QUFDQSxPQU5EOztBQVFBLFVBQUlLLE9BQU9DLElBQVAsQ0FBWVQsTUFBWixFQUFvQlUsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDbkN6QyxTQUFFMEMsSUFBRixDQUFPO0FBQ05DLGNBQU0sTUFEQTtBQUVOQyxhQUFLLHVFQUZDO0FBR05DLGtCQUFVLE1BSEo7QUFJTkMsaUJBQVMsSUFKSDtBQUtOQyxlQUFPLEtBTEQ7QUFNTmpELGNBQU07QUFDTCxtQkFBVWlDLE1BREw7QUFFTCx1QkFBY0QsV0FBV00sU0FBWCxDQUFxQk4sV0FBV08sV0FBWCxDQUF1QixHQUF2QixJQUE4QixDQUFuRDtBQUZULFNBTkE7QUFVTlcsaUJBQVMsbUJBQVc7QUFDbkJDLGlCQUFRQyxHQUFSLENBQVksS0FBWjtBQUNBLFNBWks7QUFhTnRCLGVBQU8saUJBQVc7QUFDakJxQixpQkFBUUMsR0FBUixDQUFZLE9BQVo7QUFDQXRCLGtCQUFRLElBQVI7QUFDQTtBQWhCSyxRQUFQO0FBa0JBO0FBQ0QsTUF4Q0Q7O0FBMENBLFNBQUlBLE1BQUosRUFBVztBQUNWdUIsWUFBTTdCLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHVCQUF4QixFQUFpRCxpQkFBakQsQ0FBTjtBQUNBLE1BRkQsTUFHSztBQUNKMEIsWUFBTTdCLElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLDRCQUF4QixFQUFzRCxpQkFBdEQsQ0FBTjtBQUNBO0FBQ0Q7QUF0REYsSUFEVSxFQXdEUDtBQUNGLFlBQVFILElBQUlDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFNBQWpDLENBRE47QUFFRixhQUFTLEtBRlA7QUFHRixhQUFTLGlCQUFXO0FBQ25CekIsT0FBRSxJQUFGLEVBQVFxQixNQUFSLENBQWUsT0FBZjtBQUNBO0FBTEMsSUF4RE8sQ0FKTztBQW9FbEIsWUFBUyxHQXBFUztBQXFFbEIrQixPQXJFa0Isa0JBcUVYO0FBQ1MsU0FBS0MsS0FBTCxDQUFXQyxRQUFYLEdBQXNCLEVBQXRCO0FBQ2Y7QUF2RWlCLEdBQW5CO0FBeUVBLEVBMUZEOztBQTRGQTtBQUNBO0FBQ0E7O0FBRUF6RCxRQUFPMEQsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QnpELFFBQU0wRCxFQUFOLENBQVMsT0FBVCxFQUFrQnJELGtCQUFsQjtBQUNBb0Q7QUFDQSxFQUhEOztBQUtBLFFBQU8zRCxNQUFQO0FBQ0EsQ0F0SkYiLCJmaWxlIjoib3JkZXJzL29yZGVyX2N1c3RvbWl6ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIG9yZGVyX2N1c3RvbWl6ZXIuanMgMjAxNy0wNS0yNCBnbVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgT3JkZXJzIE1vZGFsIExheWVyIE1vZHVsZVxuICpcbiAqIFRoaXMgbW9kdWxlIHdpbGwgb3BlbiBhIG1vZGFsIGxheWVyIGZvciBvcmRlciBhY3Rpb25zIGxpa2UgZGVsZXRpbmcgb3IgY2hhbmdpbmcgdGhlIG9kZXIgc3RhdHVzLlxuICpcbiAqIEBtb2R1bGUgQ29tcGF0aWJpbGl0eS9vcmRlcl9jdXN0b21pemVyXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnb3JkZXJfY3VzdG9taXplcicsXG5cdFxuXHRbXSxcblx0XG5cdC8qKiAgQGxlbmRzIG1vZHVsZTpDb21wYXRpYmlsaXR5L29yZGVyX2N1c3RvbWl6ZXIgKi9cblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHZhciB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gUFJJVkFURSBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXIgX29wZW5DdXN0b21pemVyU2V0ID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFxuXHRcdFx0dmFyICRjdXN0b21pemVyID0gJChvcHRpb25zLnNlbGVjdG9yKTtcblx0XHRcdFxuXHRcdFx0JCgnc2VsZWN0W2lkXj1cImVsZW1lbnRfXCJdJykuZWFjaChmdW5jdGlvbihqKSB7XG5cdFx0XHRcdHZhciBhdHRyX2lkID0gJCh0aGlzKS5hdHRyKCdpZCcpO1xuXHRcdFx0XHR2YXIgYXR0cl9jbGFzcyA9ICQodGhpcykuYXR0cignY2xhc3MnKTtcblx0XHRcdFx0dmFyIGF0dHJfbmFtZSA9ICQodGhpcykuYXR0cignbmFtZScpO1xuXHRcdFx0XHR2YXIgYXR0cl9zdHlsZSA9ICQodGhpcykuYXR0cignc3R5bGUnKTtcblx0XHRcdFx0dmFyIGF0dHJfdmFsdWUgPSAkKHRoaXMpLmNoaWxkcmVuKCdvcHRpb24nKS52YWwoKTtcblx0XHRcdFx0XG5cdFx0XHRcdCQodGhpcykucmVwbGFjZVdpdGgoJzxpbnB1dCB0eXBlPVwidGV4dFwiIG5hbWU9XCInICsgYXR0cl9uYW1lICsgJ1wiIGlkPVwiJyArIGF0dHJfaWQgK1xuXHRcdFx0XHRcdCdcIiBjbGFzcz1cIicgKyBhdHRyX2NsYXNzICsgJ1wiIHN0eWxlPVwiJyArIGF0dHJfc3R5bGUgKyAnXCIgdmFsdWU9XCInICsgYXR0cl92YWx1ZSArICdcIj4nKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHQkY3VzdG9taXplci5kaWFsb2coe1xuXHRcdFx0XHQndGl0bGUnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnSEVBRElOR19HWF9DVVNUT01JWkVSJywgJ29yZGVycycpLFxuXHRcdFx0XHQnbW9kYWwnOiB0cnVlLFxuXHRcdFx0XHQnZGlhbG9nQ2xhc3MnOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0J2J1dHRvbnMnOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc2F2ZScsICdidXR0b25zJyksXG5cdFx0XHRcdFx0XHQnY2xhc3MnOiAnYnRuJyxcblx0XHRcdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uIGNsaWNrKCkge1xuXHRcdFx0XHRcdFx0XHR2YXIgY29udGVudF9kaXYgPSAkY3VzdG9taXplci5jaGlsZHJlbignW2lkXj1cImdtX2dwcmludF9jb250ZW50XCJdJyk7XG5cdFx0XHRcdFx0XHRcdHZhciBlcnJvciA9IGZhbHNlO1xuXHRcdFx0XHRcdFx0XHRjb250ZW50X2Rpdi5jaGlsZHJlbignW2lkXj1cInN1cmZhY2VfXCJdJykuZWFjaChmdW5jdGlvbihpKSB7XG5cdFx0XHRcdFx0XHRcdFx0dmFyIHN1cmZhY2VfaWQgPSAkKHRoaXMpLmF0dHIoJ2lkJyk7XG5cdFx0XHRcdFx0XHRcdFx0dmFyIGlucHV0cyA9IHt9O1xuXHRcdFx0XHRcdFx0XHRcdHZhciBjb250YWluZXJfZGl2ID0gJCh0aGlzKS5jaGlsZHJlbignW2lkXj1cImVsZW1lbnRfY29udGFpbmVyX1wiXScpO1xuXHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdGNvbnRhaW5lcl9kaXYuY2hpbGRyZW4oJ2lucHV0W2lkXj1cImVsZW1lbnRfXCJdJykuZWFjaChmdW5jdGlvbihqKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHR2YXIgaW5wdXRfaWQgPSAkKHRoaXMpLmF0dHIoJ2lkJyk7XG5cdFx0XHRcdFx0XHRcdFx0XHR2YXIgaW5wdXQgPSB7fTtcblx0XHRcdFx0XHRcdFx0XHRcdGlucHV0LmlkID0gaW5wdXRfaWQuc3Vic3RyaW5nKGlucHV0X2lkLmxhc3RJbmRleE9mKCdfJykgKyAxKTtcblx0XHRcdFx0XHRcdFx0XHRcdGlucHV0LnZhbHVlID0gJCh0aGlzKS52YWwoKTtcblx0XHRcdFx0XHRcdFx0XHRcdGlucHV0c1tpbnB1dF9pZF0gPSBpbnB1dDtcblx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHRjb250YWluZXJfZGl2LmNoaWxkcmVuKCd0ZXh0YXJlYVtpZF49XCJlbGVtZW50X1wiXScpLmVhY2goZnVuY3Rpb24oaikge1xuXHRcdFx0XHRcdFx0XHRcdFx0dmFyIGlucHV0X2lkID0gJCh0aGlzKS5hdHRyKCdpZCcpO1xuXHRcdFx0XHRcdFx0XHRcdFx0dmFyIGlucHV0ID0ge307XG5cdFx0XHRcdFx0XHRcdFx0XHRpbnB1dC5pZCA9IGlucHV0X2lkLnN1YnN0cmluZyhpbnB1dF9pZC5sYXN0SW5kZXhPZignXycpICsgMSk7XG5cdFx0XHRcdFx0XHRcdFx0XHRpbnB1dC52YWx1ZSA9ICQodGhpcykudmFsKCk7XG5cdFx0XHRcdFx0XHRcdFx0XHRpbnB1dHNbaW5wdXRfaWRdID0gaW5wdXQ7XG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdFx0aWYgKE9iamVjdC5rZXlzKGlucHV0cykubGVuZ3RoID4gMCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0JC5hamF4KHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0dHlwZTogJ1BPU1QnLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHR1cmw6ICdyZXF1ZXN0X3BvcnQucGhwP21vZHVsZT1HUHJpbnRPcmRlciZhY3Rpb249c2F2ZV9zdXJmYWNlc19ncm91cF9pbnB1dHMnLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRjb250ZXh0OiB0aGlzLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRhc3luYzogZmFsc2UsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImlucHV0c1wiOiBpbnB1dHMsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJzdXJmYWNlX2lkXCI6IHN1cmZhY2VfaWQuc3Vic3RyaW5nKHN1cmZhY2VfaWQubGFzdEluZGV4T2YoJ18nKSArIDEpLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdWNjZXNzOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZygnZ3V0Jyk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGVycm9yOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZygnZXJyb3InKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRlcnJvciA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRpZiAoZXJyb3IpIHtcblx0XHRcdFx0XHRcdFx0XHRhbGVydChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnR01fR1BSSU5UX1NBVkVfRkFJTEVEJywgJ2FkbWluX2dtX2dwcmludCcpKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRcdFx0XHRhbGVydChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnR01fR1BSSU5UX1NBVkVfU1VDQ0VTU0ZVTEwnLCAnYWRtaW5fZ21fZ3ByaW50JykpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSwge1xuXHRcdFx0XHRcdFx0J3RleHQnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY2xvc2UnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0J2NsYXNzJzogJ2J0bicsXG5cdFx0XHRcdFx0XHQnY2xpY2snOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdLFxuXHRcdFx0XHQnd2lkdGgnOiA0MjAsXG5cdFx0XHRcdG9wZW4oKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3R5bGUub3ZlcmZsb3cgPSAnJztcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JHRoaXMub24oJ2NsaWNrJywgX29wZW5DdXN0b21pemVyU2V0KTtcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
