'use strict';

/* --------------------------------------------------------------
 order_details_controller.js 2018-05-02
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Order Details Controller
 *
 * This controller will handle the compatibility order details wrapper.
 *
 * @module Compatibility/order_details_controller
 */
gx.compatibility.module('order_details_controller', ['xhr', 'loading_spinner', 'modal', gx.source + '/libs/action_mapper', gx.source + '/libs/button_dropdown'],

/**  @lends module:Compatibility/order_details_controller */

function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {},


	/**
  * Final Options
  *
  * @var {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Create Invoice Click Event
  *
  * This callback will make sure that the invoice is created and then make a page reload.
  *
  * @param {jQuery.Event} event
  */
	var _onCreateInvoiceClick = function _onCreateInvoiceClick(event) {
		event.preventDefault();

		var $pageToken = $('input[name="page_token"]');

		var $modal = $('#orders_create_invoice_modal');

		var $frameContent = $this.find('.frame-wrapper.invoices').find('.frame-content');

		var $loadingSpinner = jse.libs.loading_spinner.show($frameContent);

		var requestOptions = {
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=OrdersModalsAjax/GetInvoiceCount',
			data: {
				orderId: options.order_id,
				pageToken: $pageToken.length ? $pageToken.val() : ''
			}
		};

		var createInvoiceUrl = event.target.getAttribute('href') + '&ajax=1';

		function createInvoice() {
			var downloadPdfWindow = window.open('about:blank');

			function onRequestSuccess(response) {
				var queryString = $.param({
					module: 'OrderAdmin',
					action: 'showPdf',
					type: 'invoice',
					invoice_id: response.invoiceId
				});

				downloadPdfWindow.location = jse.core.config.get('appUrl') + '/admin/request_port.php?' + queryString;

				location.reload();
			}

			function onRequestFailure() {
				downloadPdfWindow.close();
			}

			jse.libs.xhr.get({ url: createInvoiceUrl }).done(onRequestSuccess).fail(onRequestFailure);
		}

		function onRequestSuccess(response) {
			function onAbort() {
				$(this).dialog('close');
			}

			jse.libs.loading_spinner.hide($loadingSpinner);

			if (!response.count) {
				createInvoice();
				return;
			}

			$modal.dialog({
				title: jse.core.lang.translate('TITLE_CREATE_INVOICE', 'orders'),
				modal: true,
				width: 420,
				dialogClass: 'gx-container',
				buttons: [{
					text: jse.core.lang.translate('yes', 'buttons'),
					class: 'btn',
					click: createInvoice
				}, {
					text: jse.core.lang.translate('no', 'buttons'),
					class: 'btn',
					click: onAbort
				}]
			});
		}

		function onRequestFailure() {
			jse.libs.loading_spinner.hide($loadingSpinner);
			createInvoice();
		}

		jse.libs.xhr.get(requestOptions).done(onRequestSuccess).fail(onRequestFailure);
	};

	/**
  * Create Packing Slip Click Event
  *
  * This callback will make sure that the packing slip is created and then make a page reload.
  *
  * @param {jQuery.Event} event
  */
	var _onCreatePackingSlipClick = function _onCreatePackingSlipClick(event) {
		event.preventDefault();

		var downloadPdfWindow = window.open('about:blank');
		var url = event.target.getAttribute('href') + '&ajax=1';

		$.getJSON(url).done(function (response) {
			downloadPdfWindow.location = jse.core.config.get('appUrl') + '/admin/request_port.php?' + $.param({
				module: 'OrderAdmin',
				action: 'showPdf',
				type: 'packingslip',
				file: response.filename + '__' + response.filenameSuffix
			});
			location.reload();
		}).fail(function (jqXHR, textStatus, errorThrown) {
			downloadPdfWindow.close();
		});
	};

	/**
  * Open order status modal on click on elements with class add-order-status
  *
  * @param {jQuery.Event} event
  */
	var _onAddOrderStatusClick = function _onAddOrderStatusClick(event) {
		event.preventDefault();
		$('.update-order-status').trigger('click');
	};

	var _moveSaveActionsToBottom = function _moveSaveActionsToBottom() {
		var $mainActions = $('div.text-right:nth-child(2)');
		var $bottomBar = $('.footer-info');
		var $newContainer = $('<div class="pull-right info"></div>');

		$mainActions.appendTo($newContainer);
		$newContainer.appendTo($bottomBar);
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$('.message_stack_container').addClass('breakpoint-large');

		_moveSaveActionsToBottom();

		$this.on('click', '.add-order-status', _onAddOrderStatusClick).on('click', '.create-invoice', _onCreateInvoiceClick).on('click', '.create-packing-slip', _onCreatePackingSlipClick);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vcmRlcl9kZXRhaWxzX2NvbnRyb2xsZXIuanMiXSwibmFtZXMiOlsiZ3giLCJjb21wYXRpYmlsaXR5IiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9vbkNyZWF0ZUludm9pY2VDbGljayIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCIkcGFnZVRva2VuIiwiJG1vZGFsIiwiJGZyYW1lQ29udGVudCIsImZpbmQiLCIkbG9hZGluZ1NwaW5uZXIiLCJqc2UiLCJsaWJzIiwibG9hZGluZ19zcGlubmVyIiwic2hvdyIsInJlcXVlc3RPcHRpb25zIiwidXJsIiwiY29yZSIsImNvbmZpZyIsImdldCIsIm9yZGVySWQiLCJvcmRlcl9pZCIsInBhZ2VUb2tlbiIsImxlbmd0aCIsInZhbCIsImNyZWF0ZUludm9pY2VVcmwiLCJ0YXJnZXQiLCJnZXRBdHRyaWJ1dGUiLCJjcmVhdGVJbnZvaWNlIiwiZG93bmxvYWRQZGZXaW5kb3ciLCJ3aW5kb3ciLCJvcGVuIiwib25SZXF1ZXN0U3VjY2VzcyIsInJlc3BvbnNlIiwicXVlcnlTdHJpbmciLCJwYXJhbSIsImFjdGlvbiIsInR5cGUiLCJpbnZvaWNlX2lkIiwiaW52b2ljZUlkIiwibG9jYXRpb24iLCJyZWxvYWQiLCJvblJlcXVlc3RGYWlsdXJlIiwiY2xvc2UiLCJ4aHIiLCJkb25lIiwiZmFpbCIsIm9uQWJvcnQiLCJkaWFsb2ciLCJoaWRlIiwiY291bnQiLCJ0aXRsZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJtb2RhbCIsIndpZHRoIiwiZGlhbG9nQ2xhc3MiLCJidXR0b25zIiwidGV4dCIsImNsYXNzIiwiY2xpY2siLCJfb25DcmVhdGVQYWNraW5nU2xpcENsaWNrIiwiZ2V0SlNPTiIsImZpbGUiLCJmaWxlbmFtZSIsImZpbGVuYW1lU3VmZml4IiwianFYSFIiLCJ0ZXh0U3RhdHVzIiwiZXJyb3JUaHJvd24iLCJfb25BZGRPcmRlclN0YXR1c0NsaWNrIiwidHJpZ2dlciIsIl9tb3ZlU2F2ZUFjdGlvbnNUb0JvdHRvbSIsIiRtYWluQWN0aW9ucyIsIiRib3R0b21CYXIiLCIkbmV3Q29udGFpbmVyIiwiYXBwZW5kVG8iLCJpbml0IiwiYWRkQ2xhc3MiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0FBLEdBQUdDLGFBQUgsQ0FBaUJDLE1BQWpCLENBQ0MsMEJBREQsRUFHQyxDQUNDLEtBREQsRUFFQyxpQkFGRCxFQUdDLE9BSEQsRUFJQ0YsR0FBR0csTUFBSCxHQUFZLHFCQUpiLEVBS0NILEdBQUdHLE1BQUgsR0FBWSx1QkFMYixDQUhEOztBQVdDOztBQUVBLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXLEVBYlo7OztBQWVDOzs7OztBQUtBQyxXQUFVRixFQUFFRyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCSCxJQUE3QixDQXBCWDs7O0FBc0JDOzs7OztBQUtBRixVQUFTLEVBM0JWOztBQTZCQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUFPQSxLQUFJUSx3QkFBd0IsU0FBeEJBLHFCQUF3QixDQUFTQyxLQUFULEVBQWdCO0FBQzNDQSxRQUFNQyxjQUFOOztBQUVBLE1BQUlDLGFBQWFQLEVBQUUsMEJBQUYsQ0FBakI7O0FBRUEsTUFBSVEsU0FBU1IsRUFBRSw4QkFBRixDQUFiOztBQUVBLE1BQUlTLGdCQUFnQlYsTUFDbEJXLElBRGtCLENBQ2IseUJBRGEsRUFFbEJBLElBRmtCLENBRWIsZ0JBRmEsQ0FBcEI7O0FBSUEsTUFBSUMsa0JBQWtCQyxJQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLElBQXpCLENBQThCTixhQUE5QixDQUF0Qjs7QUFFQSxNQUFJTyxpQkFBaUI7QUFDcEJDLFFBQUtMLElBQUlNLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0Msc0RBRGpCO0FBRXBCdEIsU0FBTTtBQUNMdUIsYUFBU25CLFFBQVFvQixRQURaO0FBRUxDLGVBQVloQixXQUFXaUIsTUFBWixHQUFzQmpCLFdBQVdrQixHQUFYLEVBQXRCLEdBQXlDO0FBRi9DO0FBRmMsR0FBckI7O0FBUUEsTUFBSUMsbUJBQW1CckIsTUFBTXNCLE1BQU4sQ0FBYUMsWUFBYixDQUEwQixNQUExQixJQUFvQyxTQUEzRDs7QUFFQSxXQUFTQyxhQUFULEdBQXlCO0FBQ3hCLE9BQUlDLG9CQUFvQkMsT0FBT0MsSUFBUCxDQUFZLGFBQVosQ0FBeEI7O0FBRUEsWUFBU0MsZ0JBQVQsQ0FBMEJDLFFBQTFCLEVBQW9DO0FBQ25DLFFBQUlDLGNBQWNuQyxFQUFFb0MsS0FBRixDQUFRO0FBQ1B4QyxhQUFRLFlBREQ7QUFFUHlDLGFBQVEsU0FGRDtBQUdQQyxXQUFNLFNBSEM7QUFJUEMsaUJBQVlMLFNBQVNNO0FBSmQsS0FBUixDQUFsQjs7QUFPQVYsc0JBQWtCVyxRQUFsQixHQUE2QjdCLElBQUlNLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsMEJBQWhDLEdBQTZEZSxXQUExRjs7QUFFQU0sYUFBU0MsTUFBVDtBQUNBOztBQUVELFlBQVNDLGdCQUFULEdBQTRCO0FBQzNCYixzQkFBa0JjLEtBQWxCO0FBQ0E7O0FBRURoQyxPQUFJQyxJQUFKLENBQVNnQyxHQUFULENBQWF6QixHQUFiLENBQWlCLEVBQUVILEtBQUtTLGdCQUFQLEVBQWpCLEVBQ0VvQixJQURGLENBQ09iLGdCQURQLEVBRUVjLElBRkYsQ0FFT0osZ0JBRlA7QUFHQTs7QUFFRCxXQUFTVixnQkFBVCxDQUEwQkMsUUFBMUIsRUFBb0M7QUFDbkMsWUFBU2MsT0FBVCxHQUFtQjtBQUNsQmhELE1BQUUsSUFBRixFQUFRaUQsTUFBUixDQUFlLE9BQWY7QUFDQTs7QUFFRHJDLE9BQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5Qm9DLElBQXpCLENBQThCdkMsZUFBOUI7O0FBRUEsT0FBSSxDQUFDdUIsU0FBU2lCLEtBQWQsRUFBcUI7QUFDcEJ0QjtBQUNBO0FBQ0E7O0FBRURyQixVQUFPeUMsTUFBUCxDQUFjO0FBQ2JHLFdBQU94QyxJQUFJTSxJQUFKLENBQVNtQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isc0JBQXhCLEVBQWdELFFBQWhELENBRE07QUFFYkMsV0FBTyxJQUZNO0FBR2JDLFdBQU8sR0FITTtBQUliQyxpQkFBYSxjQUpBO0FBS2JDLGFBQVMsQ0FDUjtBQUNDQyxXQUFNL0MsSUFBSU0sSUFBSixDQUFTbUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLEtBQXhCLEVBQStCLFNBQS9CLENBRFA7QUFFQ00sWUFBTyxLQUZSO0FBR0NDLFlBQU9oQztBQUhSLEtBRFEsRUFNUjtBQUNDOEIsV0FBTS9DLElBQUlNLElBQUosQ0FBU21DLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixJQUF4QixFQUE4QixTQUE5QixDQURQO0FBRUNNLFlBQU8sS0FGUjtBQUdDQyxZQUFPYjtBQUhSLEtBTlE7QUFMSSxJQUFkO0FBa0JBOztBQUVELFdBQVNMLGdCQUFULEdBQTRCO0FBQzNCL0IsT0FBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCb0MsSUFBekIsQ0FBOEJ2QyxlQUE5QjtBQUNBa0I7QUFDQTs7QUFFRGpCLE1BQUlDLElBQUosQ0FBU2dDLEdBQVQsQ0FBYXpCLEdBQWIsQ0FBaUJKLGNBQWpCLEVBQ0U4QixJQURGLENBQ09iLGdCQURQLEVBRUVjLElBRkYsQ0FFT0osZ0JBRlA7QUFHQSxFQXhGRDs7QUEwRkE7Ozs7Ozs7QUFPQSxLQUFJbUIsNEJBQTRCLFNBQTVCQSx5QkFBNEIsQ0FBU3pELEtBQVQsRUFBZ0I7QUFDL0NBLFFBQU1DLGNBQU47O0FBRUEsTUFBSXdCLG9CQUFvQkMsT0FBT0MsSUFBUCxDQUFZLGFBQVosQ0FBeEI7QUFDQSxNQUFJZixNQUFNWixNQUFNc0IsTUFBTixDQUFhQyxZQUFiLENBQTBCLE1BQTFCLElBQW9DLFNBQTlDOztBQUVBNUIsSUFBRStELE9BQUYsQ0FBVTlDLEdBQVYsRUFDRTZCLElBREYsQ0FDTyxVQUFTWixRQUFULEVBQW1CO0FBQ3hCSixxQkFBa0JXLFFBQWxCLEdBQTZCN0IsSUFBSU0sSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQywwQkFBaEMsR0FBNkRwQixFQUFFb0MsS0FBRixDQUFRO0FBQ2hHeEMsWUFBUSxZQUR3RjtBQUVoR3lDLFlBQVEsU0FGd0Y7QUFHaEdDLFVBQU0sYUFIMEY7QUFJaEcwQixVQUFNOUIsU0FBUytCLFFBQVQsR0FBb0IsSUFBcEIsR0FBMkIvQixTQUFTZ0M7QUFKc0QsSUFBUixDQUExRjtBQU1BekIsWUFBU0MsTUFBVDtBQUNBLEdBVEYsRUFVRUssSUFWRixDQVVPLFVBQVNvQixLQUFULEVBQWdCQyxVQUFoQixFQUE0QkMsV0FBNUIsRUFBeUM7QUFDOUN2QyxxQkFBa0JjLEtBQWxCO0FBQ0EsR0FaRjtBQWFBLEVBbkJEOztBQXFCQTs7Ozs7QUFLQSxLQUFJMEIseUJBQXlCLFNBQXpCQSxzQkFBeUIsQ0FBU2pFLEtBQVQsRUFBZ0I7QUFDNUNBLFFBQU1DLGNBQU47QUFDQU4sSUFBRSxzQkFBRixFQUEwQnVFLE9BQTFCLENBQWtDLE9BQWxDO0FBQ0EsRUFIRDs7QUFLQSxLQUFNQywyQkFBMkIsU0FBM0JBLHdCQUEyQixHQUFNO0FBQ3RDLE1BQU1DLGVBQWV6RSxFQUFFLDZCQUFGLENBQXJCO0FBQ0EsTUFBTTBFLGFBQWExRSxFQUFFLGNBQUYsQ0FBbkI7QUFDQSxNQUFNMkUsZ0JBQWdCM0UsRUFBRSxxQ0FBRixDQUF0Qjs7QUFFQXlFLGVBQWFHLFFBQWIsQ0FBc0JELGFBQXRCO0FBQ0FBLGdCQUFjQyxRQUFkLENBQXVCRixVQUF2QjtBQUNBLEVBUEQ7O0FBU0E7QUFDQTtBQUNBOztBQUVBOUUsUUFBT2lGLElBQVAsR0FBYyxVQUFTL0IsSUFBVCxFQUFlO0FBQzVCOUMsSUFBRSwwQkFBRixFQUE4QjhFLFFBQTlCLENBQXVDLGtCQUF2Qzs7QUFFQU47O0FBRUF6RSxRQUNFZ0YsRUFERixDQUNLLE9BREwsRUFDYyxtQkFEZCxFQUNtQ1Qsc0JBRG5DLEVBRUVTLEVBRkYsQ0FFSyxPQUZMLEVBRWMsaUJBRmQsRUFFaUMzRSxxQkFGakMsRUFHRTJFLEVBSEYsQ0FHSyxPQUhMLEVBR2Msc0JBSGQsRUFHc0NqQix5QkFIdEM7O0FBS0FoQjtBQUNBLEVBWEQ7O0FBYUEsUUFBT2xELE1BQVA7QUFDQSxDQXhORiIsImZpbGUiOiJvcmRlcnMvb3JkZXJfZGV0YWlsc19jb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBvcmRlcl9kZXRhaWxzX2NvbnRyb2xsZXIuanMgMjAxOC0wNS0wMlxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTggR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgT3JkZXIgRGV0YWlscyBDb250cm9sbGVyXG4gKlxuICogVGhpcyBjb250cm9sbGVyIHdpbGwgaGFuZGxlIHRoZSBjb21wYXRpYmlsaXR5IG9yZGVyIGRldGFpbHMgd3JhcHBlci5cbiAqXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvb3JkZXJfZGV0YWlsc19jb250cm9sbGVyXG4gKi9cbmd4LmNvbXBhdGliaWxpdHkubW9kdWxlKFxuXHQnb3JkZXJfZGV0YWlsc19jb250cm9sbGVyJyxcblx0XG5cdFtcblx0XHQneGhyJyxcblx0XHQnbG9hZGluZ19zcGlubmVyJyxcblx0XHQnbW9kYWwnLFxuXHRcdGd4LnNvdXJjZSArICcvbGlicy9hY3Rpb25fbWFwcGVyJyxcblx0XHRneC5zb3VyY2UgKyAnL2xpYnMvYnV0dG9uX2Ryb3Bkb3duJ1xuXHRdLFxuXHRcblx0LyoqICBAbGVuZHMgbW9kdWxlOkNvbXBhdGliaWxpdHkvb3JkZXJfZGV0YWlsc19jb250cm9sbGVyICovXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFUyBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bGV0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0ge30sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB2YXIge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENyZWF0ZSBJbnZvaWNlIENsaWNrIEV2ZW50XG5cdFx0ICpcblx0XHQgKiBUaGlzIGNhbGxiYWNrIHdpbGwgbWFrZSBzdXJlIHRoYXQgdGhlIGludm9pY2UgaXMgY3JlYXRlZCBhbmQgdGhlbiBtYWtlIGEgcGFnZSByZWxvYWQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKi9cblx0XHR2YXIgX29uQ3JlYXRlSW52b2ljZUNsaWNrID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdHZhciAkcGFnZVRva2VuID0gJCgnaW5wdXRbbmFtZT1cInBhZ2VfdG9rZW5cIl0nKTtcblx0XHRcdFxuXHRcdFx0dmFyICRtb2RhbCA9ICQoJyNvcmRlcnNfY3JlYXRlX2ludm9pY2VfbW9kYWwnKTtcblx0XHRcdFxuXHRcdFx0dmFyICRmcmFtZUNvbnRlbnQgPSAkdGhpc1xuXHRcdFx0XHQuZmluZCgnLmZyYW1lLXdyYXBwZXIuaW52b2ljZXMnKVxuXHRcdFx0XHQuZmluZCgnLmZyYW1lLWNvbnRlbnQnKTtcblx0XHRcdFxuXHRcdFx0dmFyICRsb2FkaW5nU3Bpbm5lciA9IGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5zaG93KCRmcmFtZUNvbnRlbnQpO1xuXHRcdFx0XG5cdFx0XHR2YXIgcmVxdWVzdE9wdGlvbnMgPSB7XG5cdFx0XHRcdHVybDoganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1PcmRlcnNNb2RhbHNBamF4L0dldEludm9pY2VDb3VudCcsXG5cdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHRvcmRlcklkOiBvcHRpb25zLm9yZGVyX2lkLFxuXHRcdFx0XHRcdHBhZ2VUb2tlbjogKCRwYWdlVG9rZW4ubGVuZ3RoKSA/ICRwYWdlVG9rZW4udmFsKCkgOiAnJ1xuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHR2YXIgY3JlYXRlSW52b2ljZVVybCA9IGV2ZW50LnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2hyZWYnKSArICcmYWpheD0xJztcblx0XHRcdFxuXHRcdFx0ZnVuY3Rpb24gY3JlYXRlSW52b2ljZSgpIHtcblx0XHRcdFx0dmFyIGRvd25sb2FkUGRmV2luZG93ID0gd2luZG93Lm9wZW4oJ2Fib3V0OmJsYW5rJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRmdW5jdGlvbiBvblJlcXVlc3RTdWNjZXNzKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0dmFyIHF1ZXJ5U3RyaW5nID0gJC5wYXJhbSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBtb2R1bGU6ICdPcmRlckFkbWluJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ3Nob3dQZGYnLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2ludm9pY2UnLFxuICAgICAgICAgICAgICAgICAgICAgICAgaW52b2ljZV9pZDogcmVzcG9uc2UuaW52b2ljZUlkXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGRvd25sb2FkUGRmV2luZG93LmxvY2F0aW9uID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL3JlcXVlc3RfcG9ydC5waHA/JyArIHF1ZXJ5U3RyaW5nO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGxvY2F0aW9uLnJlbG9hZCgpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRmdW5jdGlvbiBvblJlcXVlc3RGYWlsdXJlKCkge1xuXHRcdFx0XHRcdGRvd25sb2FkUGRmV2luZG93LmNsb3NlKCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGpzZS5saWJzLnhoci5nZXQoeyB1cmw6IGNyZWF0ZUludm9pY2VVcmwgfSlcblx0XHRcdFx0XHQuZG9uZShvblJlcXVlc3RTdWNjZXNzKVxuXHRcdFx0XHRcdC5mYWlsKG9uUmVxdWVzdEZhaWx1cmUpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRmdW5jdGlvbiBvblJlcXVlc3RTdWNjZXNzKHJlc3BvbnNlKSB7XG5cdFx0XHRcdGZ1bmN0aW9uIG9uQWJvcnQoKSB7XG5cdFx0XHRcdFx0JCh0aGlzKS5kaWFsb2coJ2Nsb3NlJyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5oaWRlKCRsb2FkaW5nU3Bpbm5lcik7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoIXJlc3BvbnNlLmNvdW50KSB7XG5cdFx0XHRcdFx0Y3JlYXRlSW52b2ljZSgpO1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0JG1vZGFsLmRpYWxvZyh7XG5cdFx0XHRcdFx0dGl0bGU6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdUSVRMRV9DUkVBVEVfSU5WT0lDRScsICdvcmRlcnMnKSxcblx0XHRcdFx0XHRtb2RhbDogdHJ1ZSxcblx0XHRcdFx0XHR3aWR0aDogNDIwLFxuXHRcdFx0XHRcdGRpYWxvZ0NsYXNzOiAnZ3gtY29udGFpbmVyJyxcblx0XHRcdFx0XHRidXR0b25zOiBbXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdHRleHQ6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCd5ZXMnLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0XHRjbGFzczogJ2J0bicsXG5cdFx0XHRcdFx0XHRcdGNsaWNrOiBjcmVhdGVJbnZvaWNlXG5cdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHR0ZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnbm8nLCAnYnV0dG9ucycpLFxuXHRcdFx0XHRcdFx0XHRjbGFzczogJ2J0bicsXG5cdFx0XHRcdFx0XHRcdGNsaWNrOiBvbkFib3J0XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XVxuICAgICAgICAgICAgICAgIH0pO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRmdW5jdGlvbiBvblJlcXVlc3RGYWlsdXJlKCkge1xuXHRcdFx0XHRqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuaGlkZSgkbG9hZGluZ1NwaW5uZXIpO1xuXHRcdFx0XHRjcmVhdGVJbnZvaWNlKCk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGpzZS5saWJzLnhoci5nZXQocmVxdWVzdE9wdGlvbnMpXG5cdFx0XHRcdC5kb25lKG9uUmVxdWVzdFN1Y2Nlc3MpXG5cdFx0XHRcdC5mYWlsKG9uUmVxdWVzdEZhaWx1cmUpO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3JlYXRlIFBhY2tpbmcgU2xpcCBDbGljayBFdmVudFxuXHRcdCAqXG5cdFx0ICogVGhpcyBjYWxsYmFjayB3aWxsIG1ha2Ugc3VyZSB0aGF0IHRoZSBwYWNraW5nIHNsaXAgaXMgY3JlYXRlZCBhbmQgdGhlbiBtYWtlIGEgcGFnZSByZWxvYWQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge2pRdWVyeS5FdmVudH0gZXZlbnRcblx0XHQgKi9cblx0XHR2YXIgX29uQ3JlYXRlUGFja2luZ1NsaXBDbGljayA9IGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHR2YXIgZG93bmxvYWRQZGZXaW5kb3cgPSB3aW5kb3cub3BlbignYWJvdXQ6YmxhbmsnKTtcblx0XHRcdHZhciB1cmwgPSBldmVudC50YXJnZXQuZ2V0QXR0cmlidXRlKCdocmVmJykgKyAnJmFqYXg9MSc7XG5cdFx0XHRcblx0XHRcdCQuZ2V0SlNPTih1cmwpXG5cdFx0XHRcdC5kb25lKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0ZG93bmxvYWRQZGZXaW5kb3cubG9jYXRpb24gPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vcmVxdWVzdF9wb3J0LnBocD8nICsgJC5wYXJhbSh7XG5cdFx0XHRcdFx0XHRcdG1vZHVsZTogJ09yZGVyQWRtaW4nLFxuXHRcdFx0XHRcdFx0XHRhY3Rpb246ICdzaG93UGRmJyxcblx0XHRcdFx0XHRcdFx0dHlwZTogJ3BhY2tpbmdzbGlwJyxcblx0XHRcdFx0XHRcdFx0ZmlsZTogcmVzcG9uc2UuZmlsZW5hbWUgKyAnX18nICsgcmVzcG9uc2UuZmlsZW5hbWVTdWZmaXhcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdGxvY2F0aW9uLnJlbG9hZCgpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuZmFpbChmdW5jdGlvbihqcVhIUiwgdGV4dFN0YXR1cywgZXJyb3JUaHJvd24pIHtcblx0XHRcdFx0XHRkb3dubG9hZFBkZldpbmRvdy5jbG9zZSgpO1xuXHRcdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9wZW4gb3JkZXIgc3RhdHVzIG1vZGFsIG9uIGNsaWNrIG9uIGVsZW1lbnRzIHdpdGggY2xhc3MgYWRkLW9yZGVyLXN0YXR1c1xuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtqUXVlcnkuRXZlbnR9IGV2ZW50XG5cdFx0ICovXG5cdFx0dmFyIF9vbkFkZE9yZGVyU3RhdHVzQ2xpY2sgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdCQoJy51cGRhdGUtb3JkZXItc3RhdHVzJykudHJpZ2dlcignY2xpY2snKTtcblx0XHR9O1xuXHRcdFxuXHRcdGNvbnN0IF9tb3ZlU2F2ZUFjdGlvbnNUb0JvdHRvbSA9ICgpID0+IHtcblx0XHRcdGNvbnN0ICRtYWluQWN0aW9ucyA9ICQoJ2Rpdi50ZXh0LXJpZ2h0Om50aC1jaGlsZCgyKScpO1xuXHRcdFx0Y29uc3QgJGJvdHRvbUJhciA9ICQoJy5mb290ZXItaW5mbycpO1xuXHRcdFx0Y29uc3QgJG5ld0NvbnRhaW5lciA9ICQoJzxkaXYgY2xhc3M9XCJwdWxsLXJpZ2h0IGluZm9cIj48L2Rpdj4nKTtcblx0XHRcdFxuXHRcdFx0JG1haW5BY3Rpb25zLmFwcGVuZFRvKCRuZXdDb250YWluZXIpO1xuXHRcdFx0JG5ld0NvbnRhaW5lci5hcHBlbmRUbygkYm90dG9tQmFyKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkKCcubWVzc2FnZV9zdGFja19jb250YWluZXInKS5hZGRDbGFzcygnYnJlYWtwb2ludC1sYXJnZScpO1xuXHRcdFx0XG5cdFx0XHRfbW92ZVNhdmVBY3Rpb25zVG9Cb3R0b20oKTtcblx0XHRcdFxuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdjbGljaycsICcuYWRkLW9yZGVyLXN0YXR1cycsIF9vbkFkZE9yZGVyU3RhdHVzQ2xpY2spXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmNyZWF0ZS1pbnZvaWNlJywgX29uQ3JlYXRlSW52b2ljZUNsaWNrKVxuXHRcdFx0XHQub24oJ2NsaWNrJywgJy5jcmVhdGUtcGFja2luZy1zbGlwJywgX29uQ3JlYXRlUGFja2luZ1NsaXBDbGljayk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
