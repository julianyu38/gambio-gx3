'use strict';

/* --------------------------------------------------------------
	orders_shipcloud.js 2016-03-01
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2015 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * ## Orders Shipcloud Module
 *
 * This module implements the user interface for creating shipping labels via Shipcloud.io
 *
 * @module Compatibility/orders_shipcloud
 */
gx.compatibility.module('orders_shipcloud', [gx.source + '/libs/action_mapper', gx.source + '/libs/button_dropdown', 'loading_spinner'],

/**  @lends module:Compatibility/orders_shipcloud */
function (data) {

	'use strict';

	var
	/**
  * Module Selector
  *
  * @var {object}
  */
	$this = $(this),


	/**
  * The mapper library
  *
  * @var {object}
  */
	mapper = jse.libs.action_mapper,


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	var _singleFormInit = function _singleFormInit() {
		gx.widgets.init($('#shipcloud_modal'));
		$('#sc_modal_content').removeClass('sc_loading');
		if ($('#sc_single_container').data('is_configured') === 1) {
			$('#sc_show_labels').show();
		} else {
			$('#sc_show_labels').hide();
		}
		$('#sc_single_form').on('submit', function (e) {
			e.preventDefault();
		});
		$('#sc_single_form input.create_label').on('click', _singleFormSubmitHandler);
		$('#sc_single_form select[name="carrier"]').on('change', function (e) {
			$('#sc_single_form input[type="text"]').trigger('change');
			$('#sc_single_form .carrier-specific').not('.carrier-' + $(this).val()).hide('fast');
			$('#sc_single_form .carrier-' + $(this).val()).not(':visible').show('fast');
		});
		$('#sc_single_form .price_value').on('change', function () {
			$('#sc_single_form div.sc_quote').html('');
		});
		$('#sc_package_template').on('change', _templateSelectionHandler);
		$('#sc_single_form input.template_value').on('change', function () {
			$('#sc_package_template').val('-1');
		});
		$('#sc_get_quote').button('disable');
		$('#sc_single_form input[name="quote_carriers[]"]').on('change', function () {
			if ($('#sc_single_form input[name="quote_carriers[]"]:checked').length > 0) {
				$('#sc_get_quote').button('enable');
			} else {
				$('#sc_get_quote').button('disable');
			}
		});
		$('#sc_single_form input[name="quote_carriers[]"]:first').trigger('change');
	};

	var _templateSelectionHandler = function _templateSelectionHandler(e) {
		var $form, $template;
		$form = $(this).closest('form');
		$template = $('option:selected', $(this));
		if ($template.val() !== '-1') {
			$('input[name="package[weight]"]', $form).val($template.data('weight'));
			$('input[name="package[height]"]', $form).val($template.data('height'));
			$('input[name="package[width]"]', $form).val($template.data('width'));
			$('input[name="package[length]"]', $form).val($template.data('length'));
		}
	};

	var _openSingleFormModal = function _openSingleFormModal(event) {
		var orderId = $(event.target).parents('tr').data('row-id') || $('body').find('#gm_order_id').val();
		$('#sc_modal_content').empty().addClass('sc_loading');
		var button_create_label = jse.core.lang.translate('create_label', 'shipcloud'),
		    shipcloud_modal_buttons = [];

		shipcloud_modal_buttons.push({
			'text': jse.core.lang.translate('close', 'buttons'),
			'class': 'btn',
			'click': function click() {
				$(this).dialog('close');
				$('#sc_get_quote').show();
			}
		});
		shipcloud_modal_buttons.push({
			'text': jse.core.lang.translate('show_existing_labels', 'shipcloud'),
			'class': 'btn',
			'click': _showLabelsHandler,
			'id': 'sc_show_labels'
		});
		shipcloud_modal_buttons.push({
			'text': jse.core.lang.translate('get_quotes', 'shipcloud'),
			'class': 'btn btn-primary',
			'click': _singleFormGetQuoteHandler,
			'id': 'sc_get_quote'
		});

		$('#shipcloud_modal').dialog({
			autoOpen: false,
			modal: true,
			'title': jse.core.lang.translate('create_label', 'shipcloud'),
			'dialogClass': 'gx-container',
			buttons: shipcloud_modal_buttons,
			width: 1000,
			position: { my: 'center top', at: 'center bottom', of: '.main-top-header' }
		});
		$('#shipcloud_modal').dialog('open');
		$('#sc_modal_content').load('admin.php?do=Shipcloud/CreateLabelForm&orders_id=' + orderId, _singleFormInit);
	};

	var _addShipcloudDropdownEntry = function _addShipcloudDropdownEntry() {
		$('.gx-orders-table tr').not('.dataTableHeadingRow').each(function () {
			jse.libs.button_dropdown.mapAction($(this), 'admin_menu_entry', 'shipcloud', _openSingleFormModal);
		});
		jse.libs.button_dropdown.mapAction($('.bottom-save-bar'), 'admin_menu_entry', 'shipcloud', _openSingleFormModal);
	};

	var _labellistPickupCheckboxHandler = function _labellistPickupCheckboxHandler() {
		$('#sc-labellist-dropdown button, div.pickup_time input').prop('disabled', $('input.pickup_checkbox:checked').length === 0);
	};

	var _loadLabelList = function _loadLabelList(orders_id) {
		$('#sc_modal_content').load('admin.php?do=Shipcloud/LoadLabelList&orders_id=' + orders_id, function () {
			gx.widgets.init($('#sc_modal_content'));
			$('#shipcloud_modal').dialog({
				'title': jse.core.lang.translate('labellist_for', 'shipcloud') + ' ' + orders_id
			});
			$('#sc_modal_content').removeClass('sc_loading');

			$('form#sc_pickup').on('submit', function (e) {
				e.preventDefault();
			});
			jse.libs.button_dropdown.mapAction($('#sc-labellist-dropdown'), 'download_labels', 'shipcloud', _packedDownloadHandler);
			jse.libs.button_dropdown.mapAction($('#sc-labellist-dropdown'), 'order_pickups', 'shipcloud', _pickupSubmitHandler);
			$('input.pickup_checkbox').on('click', _labellistPickupCheckboxHandler);
			setTimeout(_labellistPickupCheckboxHandler, 200);
			$('input.pickup_checkbox_all').on('click', function () {
				if ($(this).prop('checked') === true) {
					$('input.pickup_checkbox').prop('checked', true);
					$('input.pickup_checkbox').parent().addClass('checked');
				} else {
					$('input.pickup_checkbox').prop('checked', false);
					$('input.pickup_checkbox').parent().removeClass('checked');
				}
				_labellistPickupCheckboxHandler();
			});
			$('a.sc-del-label').on('click', function (e) {
				e.preventDefault();
				var shipment_id = $(this).data('shipment-id'),
				    $buttonPlace = $(this).closest('span.sc-del-label'),
				    $row = $(this).closest('tr');
				$.ajax({
					type: 'POST',
					url: jse.core.config.get('appUrl') + '/admin/admin.php?do=Shipcloud/DeleteShipment',
					data: { shipment_id: shipment_id },
					dataType: 'json'
				}).done(function (data) {
					if (data.result === 'ERROR') {
						$buttonPlace.html(data.error_message);
						$buttonPlace.addClass('badge').addClass('badge-danger');
					} else {
						$buttonPlace.html(jse.core.lang.translate('shipment_deleted', 'shipcloud'));
						$('a, input, td.checkbox > *', $row).remove();
						$row.addClass('deleted-shipment');
					}
				}).fail(function (data) {
					$buttonPlace.html(jse.core.lang.translate('submit_error', 'shipcloud'));
				});
			});
		});
	};

	var _packedDownloadHandler = function _packedDownloadHandler(e) {
		e.preventDefault();
		var urls = [],
		    request = {};
		$('input.pickup_checkbox:checked').each(function () {
			var href = $('a.label-link', $(this).closest('tr')).attr('href');
			urls.push(href);
		});
		if (urls) {
			$('#download_result').show();
			$('#download_result').html(jse.core.lang.translate('loading', 'shipcloud'));
			request.urls = urls;
			request.page_token = $('#sc_modal_content input[name="page_token"]').val();

			$.ajax({
				type: 'POST',
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=PackedDownload/DownloadByJson',
				data: JSON.stringify(request),
				dataType: 'json'
			}).done(function (data) {
				var downloadlink = jse.core.config.get('appUrl') + '/admin/admin.php?do=PackedDownload/DownloadPackage&key=' + data.downloadKey;
				if (data.result === 'OK') {
					$('#download_result').html('<iframe class="download_iframe" src="' + downloadlink + '"></iframe>');
				}
				if (data.result === 'ERROR') {
					$('#download_result').html(data.error_message);
				}
			}).fail(function (data) {
				$('#download_result').html(jse.core.lang.translate('submit_error', 'shipcloud'));
			});
		}
		return true;
	};

	var _pickupSubmitHandler = function _pickupSubmitHandler(e) {
		e.preventDefault();
		if ($('input.pickup_checkbox:checked').length > 0) {
			var formdata = $('form#sc_pickup').serialize();
			$('#pickup_result').html(jse.core.lang.translate('sending_pickup_request', 'shipcloud'));
			$('#pickup_result').show();
			$.ajax({
				type: 'POST',
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=Shipcloud/PickupShipments',
				data: formdata,
				dataType: 'json'
			}).done(function (data) {
				var result_message = '';
				data.result_messages.forEach(function (message) {
					result_message = result_message + message + '<br>';
				});
				$('#pickup_result').html(result_message);
			}).fail(function (data) {
				alert(jse.core.lang.translate('submit_error', 'shipcloud'));
			});
		}
		return true;
	};

	var _loadUnconfiguredNote = function _loadUnconfiguredNote() {
		$('#sc_modal_content').load('admin.php?do=Shipcloud/UnconfiguredNote');
	};

	var _showLabelsHandler = function _showLabelsHandler(e) {
		var orders_id = $('#sc_single_form input[name="orders_id"]').val();
		$('#sc_modal_content').empty().addClass('sc_loading');
		_loadLabelList(orders_id);
		$('#sc_show_labels').hide();
		$('#sc_get_quote').hide();
		return false;
	};

	var _singleFormGetQuoteHandler = function _singleFormGetQuoteHandler() {
		var $form = $('#sc_single_form'),
		    quote = '';

		$('#sc_single_form .sc_quote').html('');
		$('#sc_single_form .sc_quote').attr('title', '');

		$('input[name="quote_carriers[]"]:checked').each(function () {
			var carrier = $(this).val(),
			    $create_label = $('input.create_label', $(this).closest('tr'));
			$('input[name="carrier"]', $form).val(carrier);
			$('#sc_quote_' + carrier).html(jse.core.lang.translate('loading', 'shipcloud'));
			$.ajax({
				type: 'POST',
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=Shipcloud/GetShipmentQuote',
				data: $form.serialize(),
				dataType: 'json'
			}).done(function (data) {
				if (data.result === 'OK') {
					quote = data.shipment_quote;
					$('#sc_quote_' + carrier).html(quote);
				} else if (data.result === 'ERROR') {
					$('#sc_quote_' + carrier).html(jse.core.lang.translate('not_possible', 'shipcloud'));
					$('#sc_quote_' + carrier).attr('title', data.error_message);
				} else if (data.result === 'UNCONFIGURED') {
					_loadUnconfiguredNote();
				}
			}).fail(function (data) {
				quote = jse.core.lang.translate('get_quote_error', 'shipcloud');
				$('#sc_quote_' + carrier).html(quote);
			});
		});

		$('input[name="carrier"]', $form).val('');
	};

	var _singleFormSubmitHandler = function _singleFormSubmitHandler(e) {
		var carrier, formdata;
		$('#sc_show_labels').hide();
		$('#sc_get_quote').hide();
		carrier = $(this).attr('name');
		$('input[name="carrier"]').val(carrier);
		formdata = $('#sc_single_form').serialize();
		$('#sc_modal_content').empty().addClass('sc_loading');
		// alert('data: '+formdata);
		$.ajax({
			type: 'POST',
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=Shipcloud/CreateLabelFormSubmit',
			data: formdata,
			dataType: 'json'
		}).done(function (data) {
			$('#sc_modal_content').removeClass('sc_loading');
			if (data.result === 'UNCONFIGURED') {
				_loadUnconfiguredNote();
			} else if (data.result === 'OK') {
				_loadLabelList(data.orders_id);
			} else {
				if (data.error_message) {
					$('#sc_modal_content').html('<div class="sc_error">' + data.error_message + '</div>');
				}
			}
		}).fail(function (data) {
			alert(jse.core.lang.translate('submit_error', 'shipcloud'));
		});
		return false;
	};

	var _multiDropdownHandler = function _multiDropdownHandler(e) {
		var selected_orders = [],
		    orders_param = '';
		$('input[name="gm_multi_status[]"]:checked').each(function () {
			selected_orders.push($(this).val());
		});
		$('#sc_modal_content').empty().addClass('sc_loading');
		var shipcloud_modal_buttons = [];
		shipcloud_modal_buttons.push({
			'text': jse.core.lang.translate('close', 'buttons'),
			'class': 'btn',
			'click': function click() {
				$(this).dialog('close');
				$('#sc_get_quote').show();
			}
		});
		shipcloud_modal_buttons.push({
			'text': jse.core.lang.translate('get_quotes', 'shipcloud'),
			'class': 'btn btn-primary',
			'click': _multiFormGetQuoteHandler,
			'id': 'sc_get_quote'
		});

		$('#shipcloud_modal').dialog({
			autoOpen: false,
			modal: true,
			'title': jse.core.lang.translate('create_labels', 'shipcloud'),
			'dialogClass': 'gx-container',
			buttons: shipcloud_modal_buttons,
			width: 1000,
			position: { my: 'center top', at: 'center bottom', of: '.main-top-header' }
		});

		$('#shipcloud_modal').dialog('open');
		selected_orders.forEach(function (item) {
			orders_param += 'orders[]=' + item + '&';
		});
		$('#sc_modal_content').load('admin.php?do=Shipcloud/CreateMultiLabelForm&' + orders_param, _multiFormInit);
	};

	var _multiFormInit = function _multiFormInit() {
		$('#shipcloud_modal').dialog({
			'title': jse.core.lang.translate('create_labels', 'shipcloud')
		});
		$('#sc_modal_content').removeClass('sc_loading');
		$('#sc_multi_form').on('submit', function (e) {
			e.preventDefault();return false;
		});
		$('#sc_create_label').hide();
		$('#sc_show_labels').hide();
		$('#sc_modal_content input, #sc_modal_content select').on('change', function () {
			$('.sc_multi_quote').hide();
		});
		$('#sc_package_template').on('change', _templateSelectionHandler);
		$('input.create_label').on('click', _multiFormSubmitHandler);
	};

	var _multiFormSubmitHandler = function _multiFormSubmitHandler(event) {
		var formdata, carrier;
		carrier = $(this).attr('name');
		$('#sc_multi_form input[name="carrier"]').val(carrier);
		formdata = $('#sc_multi_form').serialize();
		$('#sc_modal_content').empty().addClass('sc_loading');
		$.ajax({
			type: 'POST',
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=Shipcloud/CreateMultiLabelFormSubmit',
			data: formdata,
			dataType: 'json'
		}).done(function (data) {
			$('#sc_modal_content').removeClass('sc_loading');
			if (data.result === 'UNCONFIGURED') {
				_loadUnconfiguredNote();
			} else if (data.result === 'OK') {
				_loadMultiLabelList(data.orders_ids, data.shipments);
			} else {
				if (data.error_message) {
					$('#sc_modal_content').html('<div class="sc_error">' + data.error_message + '</div>');
				}
			}
		}).fail(function (data) {
			alert(jse.core.lang.translate('submit_error', 'shipcloud'));
		});
		return false;
	};

	var _loadMultiLabelList = function _loadMultiLabelList(orders_ids, shipments) {
		var multiLabelListParams = { 'orders_ids': orders_ids, 'shipments': shipments };

		$('#sc_modal_content').load(jse.core.config.get('appUrl') + '/admin/admin.php?do=Shipcloud/LoadMultiLabelList', { "json": JSON.stringify(multiLabelListParams) }, function () {
			gx.widgets.init($('#shipcloud_modal'));
			$('#shipcloud_modal').dialog({
				'title': jse.core.lang.translate('labellist', 'shipcloud')
			});
			$('#sc_modal_content').removeClass('sc_loading');
			$('#sc_get_quote').hide();

			$('form#sc_pickup').on('submit', function (e) {
				e.preventDefault();
			});
			jse.libs.button_dropdown.mapAction($('#sc-labellist-dropdown'), 'download_labels', 'shipcloud', _packedDownloadHandler);
			jse.libs.button_dropdown.mapAction($('#sc-labellist-dropdown'), 'order_pickups', 'shipcloud', _pickupSubmitHandler);
			$('input.pickup_checkbox').on('click', _labellistPickupCheckboxHandler);
			setTimeout(_labellistPickupCheckboxHandler, 200);
			$('input.pickup_checkbox_all').on('click', function () {
				if ($(this).prop('checked') === true) {
					$('input.pickup_checkbox').prop('checked', true);
					$('input.pickup_checkbox').parent().addClass('checked');
				} else {
					$('input.pickup_checkbox').prop('checked', false);
					$('input.pickup_checkbox').parent().removeClass('checked');
				}
				_labellistPickupCheckboxHandler();
			});
		});
	};

	var _multiPickupSubmitHandler = _pickupSubmitHandler;

	var _multiFormGetQuoteHandler = function _multiFormGetQuoteHandler() {
		var formdata;
		$('div.sc_quote').html('');
		formdata = $('#sc_multi_form').serialize();
		$.ajax({
			type: 'POST',
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=Shipcloud/GetMultiShipmentQuote',
			data: formdata,
			dataType: 'json'
		}).done(function (data) {
			if (data.result === 'OK') {
				for (var squote in data.shipment_quotes) {
					$('#sc_multi_quote_' + data.shipment_quotes[squote].orders_id).html(data.shipment_quotes[squote].shipment_quote);
				}
				$('div.sc_multi_quote').show('fast');

				for (var carrier in data.carriers_total) {
					$('#sc_quote_' + carrier).html(data.carriers_total[carrier]);
				}
			}
		}).fail(function (data) {
			alert(jse.core.lang.translate('submit_error', 'shipcloud'));
		});
		return false;
	};

	module.init = function (done) {
		$('body').prepend($('<div id="shipcloud_modal" title="' + jse.core.lang.translate('create_label_window_title', 'shipcloud') + '" style="display: none;"><div id="sc_modal_content"></div></div>'));

		var interval_counter = 10,
		    interval = setInterval(function () {
			if ($('.js-button-dropdown').length) {
				clearInterval(interval);
				_addShipcloudDropdownEntry();
			}
			if (interval_counter-- === 0) {
				clearInterval(interval);
			}
		}, 400);

		jse.libs.button_dropdown.mapAction($('#orders-table-dropdown'), 'create_labels', 'shipcloud', _multiDropdownHandler);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVycy9vcmRlcnNfc2hpcGNsb3VkLmpzIl0sIm5hbWVzIjpbImd4IiwiY29tcGF0aWJpbGl0eSIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJtYXBwZXIiLCJqc2UiLCJsaWJzIiwiYWN0aW9uX21hcHBlciIsIl9zaW5nbGVGb3JtSW5pdCIsIndpZGdldHMiLCJpbml0IiwicmVtb3ZlQ2xhc3MiLCJzaG93IiwiaGlkZSIsIm9uIiwiZSIsInByZXZlbnREZWZhdWx0IiwiX3NpbmdsZUZvcm1TdWJtaXRIYW5kbGVyIiwidHJpZ2dlciIsIm5vdCIsInZhbCIsImh0bWwiLCJfdGVtcGxhdGVTZWxlY3Rpb25IYW5kbGVyIiwiYnV0dG9uIiwibGVuZ3RoIiwiJGZvcm0iLCIkdGVtcGxhdGUiLCJjbG9zZXN0IiwiX29wZW5TaW5nbGVGb3JtTW9kYWwiLCJldmVudCIsIm9yZGVySWQiLCJ0YXJnZXQiLCJwYXJlbnRzIiwiZmluZCIsImVtcHR5IiwiYWRkQ2xhc3MiLCJidXR0b25fY3JlYXRlX2xhYmVsIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJzaGlwY2xvdWRfbW9kYWxfYnV0dG9ucyIsInB1c2giLCJkaWFsb2ciLCJfc2hvd0xhYmVsc0hhbmRsZXIiLCJfc2luZ2xlRm9ybUdldFF1b3RlSGFuZGxlciIsImF1dG9PcGVuIiwibW9kYWwiLCJidXR0b25zIiwid2lkdGgiLCJwb3NpdGlvbiIsIm15IiwiYXQiLCJvZiIsImxvYWQiLCJfYWRkU2hpcGNsb3VkRHJvcGRvd25FbnRyeSIsImVhY2giLCJidXR0b25fZHJvcGRvd24iLCJtYXBBY3Rpb24iLCJfbGFiZWxsaXN0UGlja3VwQ2hlY2tib3hIYW5kbGVyIiwicHJvcCIsIl9sb2FkTGFiZWxMaXN0Iiwib3JkZXJzX2lkIiwiX3BhY2tlZERvd25sb2FkSGFuZGxlciIsIl9waWNrdXBTdWJtaXRIYW5kbGVyIiwic2V0VGltZW91dCIsInBhcmVudCIsInNoaXBtZW50X2lkIiwiJGJ1dHRvblBsYWNlIiwiJHJvdyIsImFqYXgiLCJ0eXBlIiwidXJsIiwiY29uZmlnIiwiZ2V0IiwiZGF0YVR5cGUiLCJkb25lIiwicmVzdWx0IiwiZXJyb3JfbWVzc2FnZSIsInJlbW92ZSIsImZhaWwiLCJ1cmxzIiwicmVxdWVzdCIsImhyZWYiLCJhdHRyIiwicGFnZV90b2tlbiIsIkpTT04iLCJzdHJpbmdpZnkiLCJkb3dubG9hZGxpbmsiLCJkb3dubG9hZEtleSIsImZvcm1kYXRhIiwic2VyaWFsaXplIiwicmVzdWx0X21lc3NhZ2UiLCJyZXN1bHRfbWVzc2FnZXMiLCJmb3JFYWNoIiwibWVzc2FnZSIsImFsZXJ0IiwiX2xvYWRVbmNvbmZpZ3VyZWROb3RlIiwicXVvdGUiLCJjYXJyaWVyIiwiJGNyZWF0ZV9sYWJlbCIsInNoaXBtZW50X3F1b3RlIiwiX211bHRpRHJvcGRvd25IYW5kbGVyIiwic2VsZWN0ZWRfb3JkZXJzIiwib3JkZXJzX3BhcmFtIiwiX211bHRpRm9ybUdldFF1b3RlSGFuZGxlciIsIml0ZW0iLCJfbXVsdGlGb3JtSW5pdCIsIl9tdWx0aUZvcm1TdWJtaXRIYW5kbGVyIiwiX2xvYWRNdWx0aUxhYmVsTGlzdCIsIm9yZGVyc19pZHMiLCJzaGlwbWVudHMiLCJtdWx0aUxhYmVsTGlzdFBhcmFtcyIsIl9tdWx0aVBpY2t1cFN1Ym1pdEhhbmRsZXIiLCJzcXVvdGUiLCJzaGlwbWVudF9xdW90ZXMiLCJjYXJyaWVyc190b3RhbCIsInByZXBlbmQiLCJpbnRlcnZhbF9jb3VudGVyIiwiaW50ZXJ2YWwiLCJzZXRJbnRlcnZhbCIsImNsZWFySW50ZXJ2YWwiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7OztBQU9BQSxHQUFHQyxhQUFILENBQWlCQyxNQUFqQixDQUNDLGtCQURELEVBR0MsQ0FDQ0YsR0FBR0csTUFBSCxHQUFZLHFCQURiLEVBRUNILEdBQUdHLE1BQUgsR0FBWSx1QkFGYixFQUdDLGlCQUhELENBSEQ7O0FBU0M7QUFDQSxVQUFVQyxJQUFWLEVBQWdCOztBQUVmOztBQUVBO0FBQ0E7Ozs7O0FBS0NDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsVUFBU0MsSUFBSUMsSUFBSixDQUFTQyxhQWJuQjs7O0FBZUM7Ozs7O0FBS0FSLFVBQVMsRUFwQlY7O0FBc0JBLEtBQUlTLGtCQUFrQixTQUFsQkEsZUFBa0IsR0FBVztBQUNoQ1gsS0FBR1ksT0FBSCxDQUFXQyxJQUFYLENBQWdCUCxFQUFFLGtCQUFGLENBQWhCO0FBQ0FBLElBQUUsbUJBQUYsRUFBdUJRLFdBQXZCLENBQW1DLFlBQW5DO0FBQ0EsTUFBSVIsRUFBRSxzQkFBRixFQUEwQkYsSUFBMUIsQ0FBK0IsZUFBL0IsTUFBb0QsQ0FBeEQsRUFDQTtBQUNDRSxLQUFFLGlCQUFGLEVBQXFCUyxJQUFyQjtBQUNBLEdBSEQsTUFLQTtBQUNDVCxLQUFFLGlCQUFGLEVBQXFCVSxJQUFyQjtBQUNBO0FBQ0RWLElBQUUsaUJBQUYsRUFBcUJXLEVBQXJCLENBQXdCLFFBQXhCLEVBQWtDLFVBQVNDLENBQVQsRUFBWTtBQUFFQSxLQUFFQyxjQUFGO0FBQXFCLEdBQXJFO0FBQ0FiLElBQUUsb0NBQUYsRUFBd0NXLEVBQXhDLENBQTJDLE9BQTNDLEVBQW9ERyx3QkFBcEQ7QUFDQWQsSUFBRSx3Q0FBRixFQUE0Q1csRUFBNUMsQ0FBK0MsUUFBL0MsRUFBeUQsVUFBU0MsQ0FBVCxFQUFZO0FBQ3BFWixLQUFFLG9DQUFGLEVBQXdDZSxPQUF4QyxDQUFnRCxRQUFoRDtBQUNBZixLQUFFLG1DQUFGLEVBQXVDZ0IsR0FBdkMsQ0FBMkMsY0FBWWhCLEVBQUUsSUFBRixFQUFRaUIsR0FBUixFQUF2RCxFQUFzRVAsSUFBdEUsQ0FBMkUsTUFBM0U7QUFDQVYsS0FBRSw4QkFBNEJBLEVBQUUsSUFBRixFQUFRaUIsR0FBUixFQUE5QixFQUE2Q0QsR0FBN0MsQ0FBaUQsVUFBakQsRUFBNkRQLElBQTdELENBQWtFLE1BQWxFO0FBQ0EsR0FKRDtBQUtBVCxJQUFFLDhCQUFGLEVBQWtDVyxFQUFsQyxDQUFxQyxRQUFyQyxFQUErQyxZQUFXO0FBQ3pEWCxLQUFFLDhCQUFGLEVBQWtDa0IsSUFBbEMsQ0FBdUMsRUFBdkM7QUFDQSxHQUZEO0FBR0FsQixJQUFFLHNCQUFGLEVBQTBCVyxFQUExQixDQUE2QixRQUE3QixFQUF1Q1EseUJBQXZDO0FBQ0FuQixJQUFFLHNDQUFGLEVBQTBDVyxFQUExQyxDQUE2QyxRQUE3QyxFQUF1RCxZQUFXO0FBQUVYLEtBQUUsc0JBQUYsRUFBMEJpQixHQUExQixDQUE4QixJQUE5QjtBQUFzQyxHQUExRztBQUNBakIsSUFBRSxlQUFGLEVBQW1Cb0IsTUFBbkIsQ0FBMEIsU0FBMUI7QUFDQXBCLElBQUUsZ0RBQUYsRUFBb0RXLEVBQXBELENBQXVELFFBQXZELEVBQWlFLFlBQVc7QUFDM0UsT0FBSVgsRUFBRSx3REFBRixFQUE0RHFCLE1BQTVELEdBQXFFLENBQXpFLEVBQ0E7QUFDQ3JCLE1BQUUsZUFBRixFQUFtQm9CLE1BQW5CLENBQTBCLFFBQTFCO0FBQ0EsSUFIRCxNQUtBO0FBQ0NwQixNQUFFLGVBQUYsRUFBbUJvQixNQUFuQixDQUEwQixTQUExQjtBQUNBO0FBQ0QsR0FURDtBQVVBcEIsSUFBRSxzREFBRixFQUEwRGUsT0FBMUQsQ0FBa0UsUUFBbEU7QUFDQSxFQW5DRDs7QUFxQ0EsS0FBSUksNEJBQTRCLFNBQTVCQSx5QkFBNEIsQ0FBU1AsQ0FBVCxFQUFZO0FBQzNDLE1BQUlVLEtBQUosRUFBV0MsU0FBWDtBQUNBRCxVQUFZdEIsRUFBRSxJQUFGLEVBQVF3QixPQUFSLENBQWdCLE1BQWhCLENBQVo7QUFDQUQsY0FBWXZCLEVBQUUsaUJBQUYsRUFBcUJBLEVBQUUsSUFBRixDQUFyQixDQUFaO0FBQ0EsTUFBSXVCLFVBQVVOLEdBQVYsT0FBb0IsSUFBeEIsRUFDQTtBQUNDakIsS0FBRSwrQkFBRixFQUFtQ3NCLEtBQW5DLEVBQTBDTCxHQUExQyxDQUE4Q00sVUFBVXpCLElBQVYsQ0FBZSxRQUFmLENBQTlDO0FBQ0FFLEtBQUUsK0JBQUYsRUFBbUNzQixLQUFuQyxFQUEwQ0wsR0FBMUMsQ0FBOENNLFVBQVV6QixJQUFWLENBQWUsUUFBZixDQUE5QztBQUNBRSxLQUFFLDhCQUFGLEVBQW1Dc0IsS0FBbkMsRUFBMENMLEdBQTFDLENBQThDTSxVQUFVekIsSUFBVixDQUFlLE9BQWYsQ0FBOUM7QUFDQUUsS0FBRSwrQkFBRixFQUFtQ3NCLEtBQW5DLEVBQTBDTCxHQUExQyxDQUE4Q00sVUFBVXpCLElBQVYsQ0FBZSxRQUFmLENBQTlDO0FBQ0E7QUFDRCxFQVhEOztBQWFBLEtBQUkyQix1QkFBdUIsU0FBdkJBLG9CQUF1QixDQUFTQyxLQUFULEVBQzNCO0FBQ0MsTUFBSUMsVUFBVTNCLEVBQUUwQixNQUFNRSxNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixJQUF4QixFQUE4Qi9CLElBQTlCLENBQW1DLFFBQW5DLEtBQWdERSxFQUFFLE1BQUYsRUFBVThCLElBQVYsQ0FBZSxjQUFmLEVBQStCYixHQUEvQixFQUE5RDtBQUNBakIsSUFBRSxtQkFBRixFQUF1QitCLEtBQXZCLEdBQStCQyxRQUEvQixDQUF3QyxZQUF4QztBQUNBLE1BQUlDLHNCQUFzQi9CLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxXQUF4QyxDQUExQjtBQUFBLE1BQ0NDLDBCQUEwQixFQUQzQjs7QUFHQUEsMEJBQXdCQyxJQUF4QixDQUE2QjtBQUM1QixXQUFTcEMsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFNBQWpDLENBRG1CO0FBRTFCLFlBQVMsS0FGaUI7QUFHMUIsWUFBUyxpQkFBWTtBQUNwQnBDLE1BQUUsSUFBRixFQUFRdUMsTUFBUixDQUFlLE9BQWY7QUFDQXZDLE1BQUUsZUFBRixFQUFtQlMsSUFBbkI7QUFDQTtBQU55QixHQUE3QjtBQVFBNEIsMEJBQXdCQyxJQUF4QixDQUE2QjtBQUM1QixXQUFTcEMsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHNCQUF4QixFQUFnRCxXQUFoRCxDQURtQjtBQUU1QixZQUFTLEtBRm1CO0FBRzVCLFlBQVNJLGtCQUhtQjtBQUk1QixTQUFTO0FBSm1CLEdBQTdCO0FBTUFILDBCQUF3QkMsSUFBeEIsQ0FBNkI7QUFDNUIsV0FBU3BDLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixZQUF4QixFQUFzQyxXQUF0QyxDQURtQjtBQUU1QixZQUFTLGlCQUZtQjtBQUc1QixZQUFTSywwQkFIbUI7QUFJNUIsU0FBUztBQUptQixHQUE3Qjs7QUFPQXpDLElBQUUsa0JBQUYsRUFBc0J1QyxNQUF0QixDQUE2QjtBQUM1QkcsYUFBZSxLQURhO0FBRTVCQyxVQUFlLElBRmE7QUFHNUIsWUFBZXpDLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxXQUF4QyxDQUhhO0FBSTVCLGtCQUFlLGNBSmE7QUFLNUJRLFlBQWVQLHVCQUxhO0FBTTVCUSxVQUFlLElBTmE7QUFPNUJDLGFBQWUsRUFBRUMsSUFBSSxZQUFOLEVBQW9CQyxJQUFJLGVBQXhCLEVBQXlDQyxJQUFJLGtCQUE3QztBQVBhLEdBQTdCO0FBU0FqRCxJQUFFLGtCQUFGLEVBQXNCdUMsTUFBdEIsQ0FBNkIsTUFBN0I7QUFDQXZDLElBQUUsbUJBQUYsRUFBdUJrRCxJQUF2QixDQUE0QixzREFBc0R2QixPQUFsRixFQUNDdEIsZUFERDtBQUVBLEVBeENEOztBQTBDQSxLQUFJOEMsNkJBQTZCLFNBQTdCQSwwQkFBNkIsR0FBWTtBQUM1Q25ELElBQUUscUJBQUYsRUFBeUJnQixHQUF6QixDQUE2QixzQkFBN0IsRUFBcURvQyxJQUFyRCxDQUEwRCxZQUFZO0FBQ3JFbEQsT0FBSUMsSUFBSixDQUFTa0QsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUN0RCxFQUFFLElBQUYsQ0FBbkMsRUFBNEMsa0JBQTVDLEVBQWdFLFdBQWhFLEVBQTZFeUIsb0JBQTdFO0FBQ0EsR0FGRDtBQUdBdkIsTUFBSUMsSUFBSixDQUFTa0QsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUN0RCxFQUFFLGtCQUFGLENBQW5DLEVBQTBELGtCQUExRCxFQUE4RSxXQUE5RSxFQUEyRnlCLG9CQUEzRjtBQUNBLEVBTEQ7O0FBT0EsS0FBSThCLGtDQUFrQyxTQUFsQ0EsK0JBQWtDLEdBQVc7QUFDaER2RCxJQUFFLHNEQUFGLEVBQ0V3RCxJQURGLENBQ08sVUFEUCxFQUNtQnhELEVBQUUsK0JBQUYsRUFBbUNxQixNQUFuQyxLQUE4QyxDQURqRTtBQUVBLEVBSEQ7O0FBS0EsS0FBSW9DLGlCQUFpQixTQUFqQkEsY0FBaUIsQ0FBVUMsU0FBVixFQUNyQjtBQUNDMUQsSUFBRSxtQkFBRixFQUF1QmtELElBQXZCLENBQTRCLG9EQUFvRFEsU0FBaEYsRUFDQyxZQUFZO0FBQ1hoRSxNQUFHWSxPQUFILENBQVdDLElBQVgsQ0FBZ0JQLEVBQUUsbUJBQUYsQ0FBaEI7QUFDQUEsS0FBRSxrQkFBRixFQUFzQnVDLE1BQXRCLENBQTZCO0FBQzVCLGFBQVNyQyxJQUFJZ0MsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsZUFBeEIsRUFBeUMsV0FBekMsSUFBd0QsR0FBeEQsR0FBOERzQjtBQUQzQyxJQUE3QjtBQUdBMUQsS0FBRSxtQkFBRixFQUF1QlEsV0FBdkIsQ0FBbUMsWUFBbkM7O0FBRUFSLEtBQUUsZ0JBQUYsRUFBb0JXLEVBQXBCLENBQXVCLFFBQXZCLEVBQWlDLFVBQVNDLENBQVQsRUFBWTtBQUFFQSxNQUFFQyxjQUFGO0FBQXFCLElBQXBFO0FBQ0FYLE9BQUlDLElBQUosQ0FBU2tELGVBQVQsQ0FBeUJDLFNBQXpCLENBQ0N0RCxFQUFFLHdCQUFGLENBREQsRUFFQyxpQkFGRCxFQUdDLFdBSEQsRUFJQzJELHNCQUpEO0FBTUF6RCxPQUFJQyxJQUFKLENBQVNrRCxlQUFULENBQXlCQyxTQUF6QixDQUNDdEQsRUFBRSx3QkFBRixDQURELEVBRUMsZUFGRCxFQUdDLFdBSEQsRUFJQzRELG9CQUpEO0FBTUE1RCxLQUFFLHVCQUFGLEVBQTJCVyxFQUEzQixDQUE4QixPQUE5QixFQUF1QzRDLCtCQUF2QztBQUNBTSxjQUFXTiwrQkFBWCxFQUE0QyxHQUE1QztBQUNBdkQsS0FBRSwyQkFBRixFQUErQlcsRUFBL0IsQ0FBa0MsT0FBbEMsRUFBMkMsWUFDM0M7QUFDQyxRQUFJWCxFQUFFLElBQUYsRUFBUXdELElBQVIsQ0FBYSxTQUFiLE1BQTRCLElBQWhDLEVBQ0E7QUFDQ3hELE9BQUUsdUJBQUYsRUFBMkJ3RCxJQUEzQixDQUFnQyxTQUFoQyxFQUEyQyxJQUEzQztBQUNBeEQsT0FBRSx1QkFBRixFQUEyQjhELE1BQTNCLEdBQW9DOUIsUUFBcEMsQ0FBNkMsU0FBN0M7QUFDQSxLQUpELE1BTUE7QUFDQ2hDLE9BQUUsdUJBQUYsRUFBMkJ3RCxJQUEzQixDQUFnQyxTQUFoQyxFQUEyQyxLQUEzQztBQUNBeEQsT0FBRSx1QkFBRixFQUEyQjhELE1BQTNCLEdBQW9DdEQsV0FBcEMsQ0FBZ0QsU0FBaEQ7QUFDQTtBQUNEK0M7QUFDQSxJQWJEO0FBY0F2RCxLQUFFLGdCQUFGLEVBQW9CVyxFQUFwQixDQUF1QixPQUF2QixFQUFnQyxVQUFTQyxDQUFULEVBQVk7QUFDM0NBLE1BQUVDLGNBQUY7QUFDQSxRQUFJa0QsY0FBZS9ELEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsYUFBYixDQUFuQjtBQUFBLFFBQ0lrRSxlQUFlaEUsRUFBRSxJQUFGLEVBQVF3QixPQUFSLENBQWdCLG1CQUFoQixDQURuQjtBQUFBLFFBRUl5QyxPQUFlakUsRUFBRSxJQUFGLEVBQVF3QixPQUFSLENBQWdCLElBQWhCLENBRm5CO0FBR0F4QixNQUFFa0UsSUFBRixDQUFPO0FBQ05DLFdBQVUsTUFESjtBQUVOQyxVQUFXbEUsSUFBSWdDLElBQUosQ0FBU21DLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLDhDQUZyQztBQUdOeEUsV0FBVSxFQUFFaUUsYUFBYUEsV0FBZixFQUhKO0FBSU5RLGVBQVU7QUFKSixLQUFQLEVBTUNDLElBTkQsQ0FNTSxVQUFTMUUsSUFBVCxFQUFlO0FBQ3BCLFNBQUdBLEtBQUsyRSxNQUFMLEtBQWdCLE9BQW5CLEVBQ0E7QUFDQ1QsbUJBQWE5QyxJQUFiLENBQWtCcEIsS0FBSzRFLGFBQXZCO0FBQ0FWLG1CQUFhaEMsUUFBYixDQUFzQixPQUF0QixFQUErQkEsUUFBL0IsQ0FBd0MsY0FBeEM7QUFDQSxNQUpELE1BTUE7QUFDQ2dDLG1CQUFhOUMsSUFBYixDQUFrQmhCLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQkFBeEIsRUFBNEMsV0FBNUMsQ0FBbEI7QUFDQXBDLFFBQUUsMkJBQUYsRUFBK0JpRSxJQUEvQixFQUFxQ1UsTUFBckM7QUFDQVYsV0FBS2pDLFFBQUwsQ0FBYyxrQkFBZDtBQUNBO0FBQ0QsS0FsQkQsRUFtQkM0QyxJQW5CRCxDQW1CTSxVQUFTOUUsSUFBVCxFQUFlO0FBQ3BCa0Usa0JBQWE5QyxJQUFiLENBQWtCaEIsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGNBQXhCLEVBQXdDLFdBQXhDLENBQWxCO0FBQ0EsS0FyQkQ7QUFzQkEsSUEzQkQ7QUE0QkEsR0FqRUY7QUFrRUEsRUFwRUQ7O0FBc0VBLEtBQUl1Qix5QkFBeUIsU0FBekJBLHNCQUF5QixDQUFTL0MsQ0FBVCxFQUM3QjtBQUNDQSxJQUFFQyxjQUFGO0FBQ0EsTUFBSWdFLE9BQU8sRUFBWDtBQUFBLE1BQWVDLFVBQVUsRUFBekI7QUFDQTlFLElBQUUsK0JBQUYsRUFBbUNvRCxJQUFuQyxDQUF3QyxZQUFXO0FBQ2xELE9BQUkyQixPQUFPL0UsRUFBRSxjQUFGLEVBQWtCQSxFQUFFLElBQUYsRUFBUXdCLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbEIsRUFBeUN3RCxJQUF6QyxDQUE4QyxNQUE5QyxDQUFYO0FBQ0FILFFBQUt2QyxJQUFMLENBQVV5QyxJQUFWO0FBQ0EsR0FIRDtBQUlBLE1BQUlGLElBQUosRUFDQTtBQUNDN0UsS0FBRSxrQkFBRixFQUFzQlMsSUFBdEI7QUFDQVQsS0FBRSxrQkFBRixFQUFzQmtCLElBQXRCLENBQTJCaEIsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFNBQXhCLEVBQW1DLFdBQW5DLENBQTNCO0FBQ0EwQyxXQUFRRCxJQUFSLEdBQXFCQSxJQUFyQjtBQUNBQyxXQUFRRyxVQUFSLEdBQXFCakYsRUFBRSw0Q0FBRixFQUFnRGlCLEdBQWhELEVBQXJCOztBQUVBakIsS0FBRWtFLElBQUYsQ0FBTztBQUNOQyxVQUFVLE1BREo7QUFFTkMsU0FBV2xFLElBQUlnQyxJQUFKLENBQVNtQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxtREFGckM7QUFHTnhFLFVBQVVvRixLQUFLQyxTQUFMLENBQWVMLE9BQWYsQ0FISjtBQUlOUCxjQUFVO0FBSkosSUFBUCxFQU1DQyxJQU5ELENBTU0sVUFBUzFFLElBQVQsRUFBZTtBQUNwQixRQUFJc0YsZUFDRGxGLElBQUlnQyxJQUFKLENBQVNtQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUNBLHlEQURBLEdBRUF4RSxLQUFLdUYsV0FIUjtBQUlBLFFBQUl2RixLQUFLMkUsTUFBTCxLQUFnQixJQUFwQixFQUNBO0FBQ0N6RSxPQUFFLGtCQUFGLEVBQXNCa0IsSUFBdEIsQ0FBMkIsMENBQXdDa0UsWUFBeEMsR0FBcUQsYUFBaEY7QUFDQTtBQUNELFFBQUl0RixLQUFLMkUsTUFBTCxLQUFnQixPQUFwQixFQUNBO0FBQ0N6RSxPQUFFLGtCQUFGLEVBQXNCa0IsSUFBdEIsQ0FBMkJwQixLQUFLNEUsYUFBaEM7QUFDQTtBQUNELElBbkJELEVBb0JDRSxJQXBCRCxDQW9CTSxVQUFTOUUsSUFBVCxFQUFlO0FBQ3BCRSxNQUFFLGtCQUFGLEVBQXNCa0IsSUFBdEIsQ0FBMkJoQixJQUFJZ0MsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsY0FBeEIsRUFBd0MsV0FBeEMsQ0FBM0I7QUFDQSxJQXRCRDtBQXVCQTtBQUNELFNBQU8sSUFBUDtBQUNBLEVBeENEOztBQTBDQSxLQUFJd0IsdUJBQXVCLFNBQXZCQSxvQkFBdUIsQ0FBU2hELENBQVQsRUFBWTtBQUN0Q0EsSUFBRUMsY0FBRjtBQUNBLE1BQUliLEVBQUUsK0JBQUYsRUFBbUNxQixNQUFuQyxHQUE0QyxDQUFoRCxFQUNBO0FBQ0MsT0FBSWlFLFdBQVd0RixFQUFFLGdCQUFGLEVBQW9CdUYsU0FBcEIsRUFBZjtBQUNBdkYsS0FBRSxnQkFBRixFQUFvQmtCLElBQXBCLENBQXlCaEIsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHdCQUF4QixFQUFrRCxXQUFsRCxDQUF6QjtBQUNBcEMsS0FBRSxnQkFBRixFQUFvQlMsSUFBcEI7QUFDQVQsS0FBRWtFLElBQUYsQ0FBTztBQUNOQyxVQUFVLE1BREo7QUFFTkMsU0FBVWxFLElBQUlnQyxJQUFKLENBQVNtQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQywrQ0FGcEM7QUFHTnhFLFVBQVV3RixRQUhKO0FBSU5mLGNBQVU7QUFKSixJQUFQLEVBTUNDLElBTkQsQ0FNTSxVQUFTMUUsSUFBVCxFQUFlO0FBQ3BCLFFBQUkwRixpQkFBaUIsRUFBckI7QUFDQTFGLFNBQUsyRixlQUFMLENBQXFCQyxPQUFyQixDQUE2QixVQUFTQyxPQUFULEVBQWtCO0FBQUVILHNCQUFpQkEsaUJBQWlCRyxPQUFqQixHQUEyQixNQUE1QztBQUFxRCxLQUF0RztBQUNBM0YsTUFBRSxnQkFBRixFQUFvQmtCLElBQXBCLENBQXlCc0UsY0FBekI7QUFDQSxJQVZELEVBV0NaLElBWEQsQ0FXTSxVQUFTOUUsSUFBVCxFQUFlO0FBQ3BCOEYsVUFBTTFGLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxXQUF4QyxDQUFOO0FBQ0EsSUFiRDtBQWNBO0FBQ0QsU0FBTyxJQUFQO0FBQ0EsRUF2QkQ7O0FBeUJBLEtBQUl5RCx3QkFBd0IsU0FBeEJBLHFCQUF3QixHQUM1QjtBQUNDN0YsSUFBRSxtQkFBRixFQUF1QmtELElBQXZCLENBQTRCLHlDQUE1QjtBQUNBLEVBSEQ7O0FBS0EsS0FBSVYscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBVTVCLENBQVYsRUFBYTtBQUNyQyxNQUFJOEMsWUFBWTFELEVBQUUseUNBQUYsRUFBNkNpQixHQUE3QyxFQUFoQjtBQUNBakIsSUFBRSxtQkFBRixFQUF1QitCLEtBQXZCLEdBQStCQyxRQUEvQixDQUF3QyxZQUF4QztBQUNBeUIsaUJBQWVDLFNBQWY7QUFDQTFELElBQUUsaUJBQUYsRUFBcUJVLElBQXJCO0FBQ0FWLElBQUUsZUFBRixFQUFtQlUsSUFBbkI7QUFDQSxTQUFPLEtBQVA7QUFDQSxFQVBEOztBQVNBLEtBQUkrQiw2QkFBNkIsU0FBN0JBLDBCQUE2QixHQUNqQztBQUNDLE1BQUluQixRQUFRdEIsRUFBRSxpQkFBRixDQUFaO0FBQUEsTUFDSThGLFFBQVEsRUFEWjs7QUFHQTlGLElBQUUsMkJBQUYsRUFBK0JrQixJQUEvQixDQUFvQyxFQUFwQztBQUNBbEIsSUFBRSwyQkFBRixFQUErQmdGLElBQS9CLENBQW9DLE9BQXBDLEVBQTZDLEVBQTdDOztBQUVBaEYsSUFBRSx3Q0FBRixFQUE0Q29ELElBQTVDLENBQWlELFlBQVc7QUFDM0QsT0FBSTJDLFVBQVUvRixFQUFFLElBQUYsRUFBUWlCLEdBQVIsRUFBZDtBQUFBLE9BQ0krRSxnQkFBZ0JoRyxFQUFFLG9CQUFGLEVBQXdCQSxFQUFFLElBQUYsRUFBUXdCLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBeEIsQ0FEcEI7QUFFQXhCLEtBQUUsdUJBQUYsRUFBMkJzQixLQUEzQixFQUFrQ0wsR0FBbEMsQ0FBc0M4RSxPQUF0QztBQUNBL0YsS0FBRSxlQUFhK0YsT0FBZixFQUF3QjdFLElBQXhCLENBQTZCaEIsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFNBQXhCLEVBQW1DLFdBQW5DLENBQTdCO0FBQ0FwQyxLQUFFa0UsSUFBRixDQUFPO0FBQ05DLFVBQVUsTUFESjtBQUVOQyxTQUFVbEUsSUFBSWdDLElBQUosQ0FBU21DLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLGdEQUZwQztBQUdOeEUsVUFBVXdCLE1BQU1pRSxTQUFOLEVBSEo7QUFJTmhCLGNBQVU7QUFKSixJQUFQLEVBTUNDLElBTkQsQ0FNTSxVQUFVMUUsSUFBVixFQUFnQjtBQUNyQixRQUFJQSxLQUFLMkUsTUFBTCxLQUFnQixJQUFwQixFQUNBO0FBQ0NxQixhQUFRaEcsS0FBS21HLGNBQWI7QUFDQWpHLE9BQUUsZUFBYStGLE9BQWYsRUFBd0I3RSxJQUF4QixDQUE2QjRFLEtBQTdCO0FBQ0EsS0FKRCxNQUtLLElBQUloRyxLQUFLMkUsTUFBTCxLQUFnQixPQUFwQixFQUNMO0FBQ0N6RSxPQUFFLGVBQWErRixPQUFmLEVBQXdCN0UsSUFBeEIsQ0FBNkJoQixJQUFJZ0MsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsY0FBeEIsRUFBd0MsV0FBeEMsQ0FBN0I7QUFDQXBDLE9BQUUsZUFBYStGLE9BQWYsRUFBd0JmLElBQXhCLENBQTZCLE9BQTdCLEVBQXNDbEYsS0FBSzRFLGFBQTNDO0FBQ0EsS0FKSSxNQUtBLElBQUk1RSxLQUFLMkUsTUFBTCxLQUFnQixjQUFwQixFQUNMO0FBQ0NvQjtBQUNBO0FBQ0QsSUFyQkQsRUFzQkNqQixJQXRCRCxDQXNCTSxVQUFVOUUsSUFBVixFQUFnQjtBQUNyQmdHLFlBQVE1RixJQUFJZ0MsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsaUJBQXhCLEVBQTJDLFdBQTNDLENBQVI7QUFDQXBDLE1BQUUsZUFBYStGLE9BQWYsRUFBd0I3RSxJQUF4QixDQUE2QjRFLEtBQTdCO0FBQ0EsSUF6QkQ7QUEwQkEsR0EvQkQ7O0FBaUNBOUYsSUFBRSx1QkFBRixFQUEyQnNCLEtBQTNCLEVBQWtDTCxHQUFsQyxDQUFzQyxFQUF0QztBQUNBLEVBMUNEOztBQTRDQSxLQUFJSCwyQkFBMkIsU0FBM0JBLHdCQUEyQixDQUFVRixDQUFWLEVBQWE7QUFDM0MsTUFBSW1GLE9BQUosRUFBYVQsUUFBYjtBQUNBdEYsSUFBRSxpQkFBRixFQUFxQlUsSUFBckI7QUFDQVYsSUFBRSxlQUFGLEVBQW1CVSxJQUFuQjtBQUNBcUYsWUFBVS9GLEVBQUUsSUFBRixFQUFRZ0YsSUFBUixDQUFhLE1BQWIsQ0FBVjtBQUNBaEYsSUFBRSx1QkFBRixFQUEyQmlCLEdBQTNCLENBQStCOEUsT0FBL0I7QUFDQVQsYUFBV3RGLEVBQUUsaUJBQUYsRUFBcUJ1RixTQUFyQixFQUFYO0FBQ0F2RixJQUFFLG1CQUFGLEVBQXVCK0IsS0FBdkIsR0FBK0JDLFFBQS9CLENBQXdDLFlBQXhDO0FBQ0E7QUFDQWhDLElBQUVrRSxJQUFGLENBQU87QUFDTkMsU0FBVSxNQURKO0FBRU5DLFFBQVVsRSxJQUFJZ0MsSUFBSixDQUFTbUMsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MscURBRnBDO0FBR054RSxTQUFVd0YsUUFISjtBQUlOZixhQUFVO0FBSkosR0FBUCxFQU1DQyxJQU5ELENBTU0sVUFBVTFFLElBQVYsRUFBZ0I7QUFDckJFLEtBQUUsbUJBQUYsRUFBdUJRLFdBQXZCLENBQW1DLFlBQW5DO0FBQ0EsT0FBSVYsS0FBSzJFLE1BQUwsS0FBZ0IsY0FBcEIsRUFDQTtBQUNDb0I7QUFDQSxJQUhELE1BSUssSUFBSS9GLEtBQUsyRSxNQUFMLEtBQWdCLElBQXBCLEVBQ0w7QUFDQ2hCLG1CQUFlM0QsS0FBSzRELFNBQXBCO0FBQ0EsSUFISSxNQUtMO0FBQ0MsUUFBSTVELEtBQUs0RSxhQUFULEVBQ0E7QUFDQzFFLE9BQUUsbUJBQUYsRUFBdUJrQixJQUF2QixDQUE0QiwyQkFBeUJwQixLQUFLNEUsYUFBOUIsR0FBNEMsUUFBeEU7QUFDQTtBQUNEO0FBQ0QsR0F2QkQsRUF3QkNFLElBeEJELENBd0JNLFVBQVU5RSxJQUFWLEVBQWdCO0FBQ3JCOEYsU0FBTTFGLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxXQUF4QyxDQUFOO0FBQ0EsR0ExQkQ7QUEyQkEsU0FBTyxLQUFQO0FBQ0EsRUFyQ0Q7O0FBdUNBLEtBQUk4RCx3QkFBd0IsU0FBeEJBLHFCQUF3QixDQUFTdEYsQ0FBVCxFQUM1QjtBQUNDLE1BQUl1RixrQkFBa0IsRUFBdEI7QUFBQSxNQUEwQkMsZUFBZSxFQUF6QztBQUNBcEcsSUFBRSx5Q0FBRixFQUE2Q29ELElBQTdDLENBQWtELFlBQVc7QUFDNUQrQyxtQkFBZ0I3RCxJQUFoQixDQUFxQnRDLEVBQUUsSUFBRixFQUFRaUIsR0FBUixFQUFyQjtBQUNBLEdBRkQ7QUFHQWpCLElBQUUsbUJBQUYsRUFBdUIrQixLQUF2QixHQUErQkMsUUFBL0IsQ0FBd0MsWUFBeEM7QUFDQSxNQUFJSywwQkFBMEIsRUFBOUI7QUFDQUEsMEJBQXdCQyxJQUF4QixDQUE2QjtBQUM1QixXQUFTcEMsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFNBQWpDLENBRG1CO0FBRTVCLFlBQVMsS0FGbUI7QUFHNUIsWUFBUyxpQkFBWTtBQUNwQnBDLE1BQUUsSUFBRixFQUFRdUMsTUFBUixDQUFlLE9BQWY7QUFDQXZDLE1BQUUsZUFBRixFQUFtQlMsSUFBbkI7QUFDQTtBQU4yQixHQUE3QjtBQVFBNEIsMEJBQXdCQyxJQUF4QixDQUE2QjtBQUM1QixXQUFTcEMsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFlBQXhCLEVBQXNDLFdBQXRDLENBRG1CO0FBRTVCLFlBQVMsaUJBRm1CO0FBRzVCLFlBQVNpRSx5QkFIbUI7QUFJNUIsU0FBUztBQUptQixHQUE3Qjs7QUFPQXJHLElBQUUsa0JBQUYsRUFBc0J1QyxNQUF0QixDQUE2QjtBQUM1QkcsYUFBZSxLQURhO0FBRTVCQyxVQUFlLElBRmE7QUFHNUIsWUFBZXpDLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixlQUF4QixFQUF5QyxXQUF6QyxDQUhhO0FBSTVCLGtCQUFlLGNBSmE7QUFLNUJRLFlBQWVQLHVCQUxhO0FBTTVCUSxVQUFlLElBTmE7QUFPNUJDLGFBQWUsRUFBRUMsSUFBSSxZQUFOLEVBQW9CQyxJQUFJLGVBQXhCLEVBQXlDQyxJQUFJLGtCQUE3QztBQVBhLEdBQTdCOztBQVVBakQsSUFBRSxrQkFBRixFQUFzQnVDLE1BQXRCLENBQTZCLE1BQTdCO0FBQ0E0RCxrQkFBZ0JULE9BQWhCLENBQXdCLFVBQVNZLElBQVQsRUFBZTtBQUN0Q0YsbUJBQWdCLGNBQVlFLElBQVosR0FBaUIsR0FBakM7QUFDQSxHQUZEO0FBR0F0RyxJQUFFLG1CQUFGLEVBQXVCa0QsSUFBdkIsQ0FBNEIsaURBQStDa0QsWUFBM0UsRUFBeUZHLGNBQXpGO0FBQ0EsRUF0Q0Q7O0FBd0NBLEtBQUlBLGlCQUFpQixTQUFqQkEsY0FBaUIsR0FBVztBQUMvQnZHLElBQUUsa0JBQUYsRUFBc0J1QyxNQUF0QixDQUE2QjtBQUM1QixZQUFTckMsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGVBQXhCLEVBQXlDLFdBQXpDO0FBRG1CLEdBQTdCO0FBR0FwQyxJQUFFLG1CQUFGLEVBQXVCUSxXQUF2QixDQUFtQyxZQUFuQztBQUNBUixJQUFFLGdCQUFGLEVBQW9CVyxFQUFwQixDQUF1QixRQUF2QixFQUFpQyxVQUFTQyxDQUFULEVBQVk7QUFBRUEsS0FBRUMsY0FBRixHQUFvQixPQUFPLEtBQVA7QUFBZSxHQUFsRjtBQUNBYixJQUFFLGtCQUFGLEVBQXNCVSxJQUF0QjtBQUNBVixJQUFFLGlCQUFGLEVBQXFCVSxJQUFyQjtBQUNBVixJQUFFLG1EQUFGLEVBQXVEVyxFQUF2RCxDQUEwRCxRQUExRCxFQUFvRSxZQUFXO0FBQzlFWCxLQUFFLGlCQUFGLEVBQXFCVSxJQUFyQjtBQUNBLEdBRkQ7QUFHQVYsSUFBRSxzQkFBRixFQUEwQlcsRUFBMUIsQ0FBNkIsUUFBN0IsRUFBdUNRLHlCQUF2QztBQUNBbkIsSUFBRSxvQkFBRixFQUF3QlcsRUFBeEIsQ0FBMkIsT0FBM0IsRUFBb0M2Rix1QkFBcEM7QUFDQSxFQWJEOztBQWVBLEtBQUlBLDBCQUEwQixTQUExQkEsdUJBQTBCLENBQVM5RSxLQUFULEVBQWdCO0FBQzdDLE1BQUk0RCxRQUFKLEVBQWNTLE9BQWQ7QUFDQUEsWUFBVS9GLEVBQUUsSUFBRixFQUFRZ0YsSUFBUixDQUFhLE1BQWIsQ0FBVjtBQUNBaEYsSUFBRSxzQ0FBRixFQUEwQ2lCLEdBQTFDLENBQThDOEUsT0FBOUM7QUFDQVQsYUFBV3RGLEVBQUUsZ0JBQUYsRUFBb0J1RixTQUFwQixFQUFYO0FBQ0F2RixJQUFFLG1CQUFGLEVBQXVCK0IsS0FBdkIsR0FBK0JDLFFBQS9CLENBQXdDLFlBQXhDO0FBQ0FoQyxJQUFFa0UsSUFBRixDQUFPO0FBQ05DLFNBQVUsTUFESjtBQUVOQyxRQUFVbEUsSUFBSWdDLElBQUosQ0FBU21DLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLDBEQUZwQztBQUdOeEUsU0FBVXdGLFFBSEo7QUFJTmYsYUFBVTtBQUpKLEdBQVAsRUFNQ0MsSUFORCxDQU1NLFVBQVUxRSxJQUFWLEVBQWdCO0FBQ3JCRSxLQUFFLG1CQUFGLEVBQXVCUSxXQUF2QixDQUFtQyxZQUFuQztBQUNBLE9BQUlWLEtBQUsyRSxNQUFMLEtBQWdCLGNBQXBCLEVBQ0E7QUFDQ29CO0FBQ0EsSUFIRCxNQUlLLElBQUkvRixLQUFLMkUsTUFBTCxLQUFnQixJQUFwQixFQUNMO0FBQ0NnQyx3QkFBb0IzRyxLQUFLNEcsVUFBekIsRUFBcUM1RyxLQUFLNkcsU0FBMUM7QUFDQSxJQUhJLE1BS0w7QUFDQyxRQUFJN0csS0FBSzRFLGFBQVQsRUFDQTtBQUNDMUUsT0FBRSxtQkFBRixFQUF1QmtCLElBQXZCLENBQTRCLDJCQUF5QnBCLEtBQUs0RSxhQUE5QixHQUE0QyxRQUF4RTtBQUNBO0FBQ0Q7QUFDRCxHQXZCRCxFQXdCQ0UsSUF4QkQsQ0F3Qk0sVUFBVTlFLElBQVYsRUFBZ0I7QUFDckI4RixTQUFNMUYsSUFBSWdDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGNBQXhCLEVBQXdDLFdBQXhDLENBQU47QUFDQSxHQTFCRDtBQTJCQSxTQUFPLEtBQVA7QUFDQSxFQWxDRDs7QUFvQ0EsS0FBSXFFLHNCQUFzQixTQUF0QkEsbUJBQXNCLENBQVNDLFVBQVQsRUFBcUJDLFNBQXJCLEVBQzFCO0FBQ0MsTUFBSUMsdUJBQXVCLEVBQUUsY0FBY0YsVUFBaEIsRUFBNEIsYUFBYUMsU0FBekMsRUFBM0I7O0FBRUEzRyxJQUFFLG1CQUFGLEVBQXVCa0QsSUFBdkIsQ0FDQ2hELElBQUlnQyxJQUFKLENBQVNtQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxrREFEakMsRUFFQyxFQUFFLFFBQVFZLEtBQUtDLFNBQUwsQ0FBZXlCLG9CQUFmLENBQVYsRUFGRCxFQUdDLFlBQVk7QUFDWGxILE1BQUdZLE9BQUgsQ0FBV0MsSUFBWCxDQUFnQlAsRUFBRSxrQkFBRixDQUFoQjtBQUNBQSxLQUFFLGtCQUFGLEVBQXNCdUMsTUFBdEIsQ0FBNkI7QUFDNUIsYUFBU3JDLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixXQUF4QixFQUFxQyxXQUFyQztBQURtQixJQUE3QjtBQUdBcEMsS0FBRSxtQkFBRixFQUF1QlEsV0FBdkIsQ0FBbUMsWUFBbkM7QUFDQVIsS0FBRSxlQUFGLEVBQW1CVSxJQUFuQjs7QUFFQVYsS0FBRSxnQkFBRixFQUFvQlcsRUFBcEIsQ0FBdUIsUUFBdkIsRUFBaUMsVUFBU0MsQ0FBVCxFQUFZO0FBQUVBLE1BQUVDLGNBQUY7QUFBcUIsSUFBcEU7QUFDQVgsT0FBSUMsSUFBSixDQUFTa0QsZUFBVCxDQUF5QkMsU0FBekIsQ0FDQ3RELEVBQUUsd0JBQUYsQ0FERCxFQUVDLGlCQUZELEVBR0MsV0FIRCxFQUlDMkQsc0JBSkQ7QUFNQXpELE9BQUlDLElBQUosQ0FBU2tELGVBQVQsQ0FBeUJDLFNBQXpCLENBQ0N0RCxFQUFFLHdCQUFGLENBREQsRUFFQyxlQUZELEVBR0MsV0FIRCxFQUlDNEQsb0JBSkQ7QUFNQTVELEtBQUUsdUJBQUYsRUFBMkJXLEVBQTNCLENBQThCLE9BQTlCLEVBQXVDNEMsK0JBQXZDO0FBQ0FNLGNBQVdOLCtCQUFYLEVBQTRDLEdBQTVDO0FBQ0F2RCxLQUFFLDJCQUFGLEVBQStCVyxFQUEvQixDQUFrQyxPQUFsQyxFQUEyQyxZQUMzQztBQUNDLFFBQUlYLEVBQUUsSUFBRixFQUFRd0QsSUFBUixDQUFhLFNBQWIsTUFBNEIsSUFBaEMsRUFDQTtBQUNDeEQsT0FBRSx1QkFBRixFQUEyQndELElBQTNCLENBQWdDLFNBQWhDLEVBQTJDLElBQTNDO0FBQ0F4RCxPQUFFLHVCQUFGLEVBQTJCOEQsTUFBM0IsR0FBb0M5QixRQUFwQyxDQUE2QyxTQUE3QztBQUNBLEtBSkQsTUFNQTtBQUNDaEMsT0FBRSx1QkFBRixFQUEyQndELElBQTNCLENBQWdDLFNBQWhDLEVBQTJDLEtBQTNDO0FBQ0F4RCxPQUFFLHVCQUFGLEVBQTJCOEQsTUFBM0IsR0FBb0N0RCxXQUFwQyxDQUFnRCxTQUFoRDtBQUNBO0FBQ0QrQztBQUNBLElBYkQ7QUFjQSxHQXhDRjtBQTBDQSxFQTlDRDs7QUFnREEsS0FBSXNELDRCQUE0QmpELG9CQUFoQzs7QUFFQSxLQUFJeUMsNEJBQTRCLFNBQTVCQSx5QkFBNEIsR0FBVztBQUMxQyxNQUFJZixRQUFKO0FBQ0F0RixJQUFFLGNBQUYsRUFBa0JrQixJQUFsQixDQUF1QixFQUF2QjtBQUNBb0UsYUFBV3RGLEVBQUUsZ0JBQUYsRUFBb0J1RixTQUFwQixFQUFYO0FBQ0F2RixJQUFFa0UsSUFBRixDQUFPO0FBQ05DLFNBQVUsTUFESjtBQUVOQyxRQUFVbEUsSUFBSWdDLElBQUosQ0FBU21DLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLElBQWdDLHFEQUZwQztBQUdOeEUsU0FBVXdGLFFBSEo7QUFJTmYsYUFBVTtBQUpKLEdBQVAsRUFNQ0MsSUFORCxDQU1NLFVBQVUxRSxJQUFWLEVBQWdCO0FBQ3JCLE9BQUlBLEtBQUsyRSxNQUFMLEtBQWdCLElBQXBCLEVBQ0E7QUFDQyxTQUFJLElBQUlxQyxNQUFSLElBQWtCaEgsS0FBS2lILGVBQXZCLEVBQXdDO0FBQ3ZDL0csT0FBRSxxQkFBcUJGLEtBQUtpSCxlQUFMLENBQXFCRCxNQUFyQixFQUE2QnBELFNBQXBELEVBQ0V4QyxJQURGLENBQ09wQixLQUFLaUgsZUFBTCxDQUFxQkQsTUFBckIsRUFBNkJiLGNBRHBDO0FBRUE7QUFDRGpHLE1BQUUsb0JBQUYsRUFBd0JTLElBQXhCLENBQTZCLE1BQTdCOztBQUVBLFNBQUksSUFBSXNGLE9BQVIsSUFBbUJqRyxLQUFLa0gsY0FBeEIsRUFDQTtBQUNDaEgsT0FBRSxlQUFhK0YsT0FBZixFQUF3QjdFLElBQXhCLENBQTZCcEIsS0FBS2tILGNBQUwsQ0FBb0JqQixPQUFwQixDQUE3QjtBQUNBO0FBQ0Q7QUFDRCxHQXBCRCxFQXFCQ25CLElBckJELENBcUJNLFVBQVU5RSxJQUFWLEVBQWdCO0FBQ3JCOEYsU0FBTTFGLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixjQUF4QixFQUF3QyxXQUF4QyxDQUFOO0FBQ0EsR0F2QkQ7QUF3QkEsU0FBTyxLQUFQO0FBQ0EsRUE3QkQ7O0FBZ0NBeEMsUUFBT1csSUFBUCxHQUFjLFVBQVVpRSxJQUFWLEVBQWdCO0FBQzdCeEUsSUFBRSxNQUFGLEVBQVVpSCxPQUFWLENBQWtCakgsRUFBRSxzQ0FBc0NFLElBQUlnQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUN4RCwyQkFEd0QsRUFDM0IsV0FEMkIsQ0FBdEMsR0FFbkIsa0VBRmlCLENBQWxCOztBQUlBLE1BQUk4RSxtQkFBbUIsRUFBdkI7QUFBQSxNQUNJQyxXQUFXQyxZQUFZLFlBQVk7QUFDdEMsT0FBSXBILEVBQUUscUJBQUYsRUFBeUJxQixNQUE3QixFQUFxQztBQUNwQ2dHLGtCQUFjRixRQUFkO0FBQ0FoRTtBQUNBO0FBQ0QsT0FBSStELHVCQUF1QixDQUEzQixFQUNBO0FBQ0NHLGtCQUFjRixRQUFkO0FBQ0E7QUFDRCxHQVRjLEVBU1osR0FUWSxDQURmOztBQVlBakgsTUFBSUMsSUFBSixDQUFTa0QsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUN0RCxFQUFFLHdCQUFGLENBQW5DLEVBQWdFLGVBQWhFLEVBQWlGLFdBQWpGLEVBQThGa0cscUJBQTlGOztBQUVBMUI7QUFDQSxFQXBCRDs7QUFzQkEsUUFBTzVFLE1BQVA7QUFDQSxDQTFqQkYiLCJmaWxlIjoib3JkZXJzL29yZGVyc19zaGlwY2xvdWQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRvcmRlcnNfc2hpcGNsb3VkLmpzIDIwMTYtMDMtMDFcblx0R2FtYmlvIEdtYkhcblx0aHR0cDovL3d3dy5nYW1iaW8uZGVcblx0Q29weXJpZ2h0IChjKSAyMDE1IEdhbWJpbyBHbWJIXG5cdFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuXHRbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cblx0LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiovXG5cbi8qKlxuICogIyMgT3JkZXJzIFNoaXBjbG91ZCBNb2R1bGVcbiAqXG4gKiBUaGlzIG1vZHVsZSBpbXBsZW1lbnRzIHRoZSB1c2VyIGludGVyZmFjZSBmb3IgY3JlYXRpbmcgc2hpcHBpbmcgbGFiZWxzIHZpYSBTaGlwY2xvdWQuaW9cbiAqXG4gKiBAbW9kdWxlIENvbXBhdGliaWxpdHkvb3JkZXJzX3NoaXBjbG91ZFxuICovXG5neC5jb21wYXRpYmlsaXR5Lm1vZHVsZShcblx0J29yZGVyc19zaGlwY2xvdWQnLFxuXG5cdFtcblx0XHRneC5zb3VyY2UgKyAnL2xpYnMvYWN0aW9uX21hcHBlcicsXG5cdFx0Z3guc291cmNlICsgJy9saWJzL2J1dHRvbl9kcm9wZG93bicsXG5cdFx0J2xvYWRpbmdfc3Bpbm5lcidcblx0XSxcblxuXHQvKiogIEBsZW5kcyBtb2R1bGU6Q29tcGF0aWJpbGl0eS9vcmRlcnNfc2hpcGNsb3VkICovXG5cdGZ1bmN0aW9uIChkYXRhKSB7XG5cblx0XHQndXNlIHN0cmljdCc7XG5cblx0XHR2YXJcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHQgKlxuXHRcdCAqIEB2YXIge29iamVjdH1cblx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBUaGUgbWFwcGVyIGxpYnJhcnlcblx0XHRcdCAqXG5cdFx0XHQgKiBAdmFyIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1hcHBlciA9IGpzZS5saWJzLmFjdGlvbl9tYXBwZXIsXG5cblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9O1xuXG5cdFx0dmFyIF9zaW5nbGVGb3JtSW5pdCA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0Z3gud2lkZ2V0cy5pbml0KCQoJyNzaGlwY2xvdWRfbW9kYWwnKSk7XG5cdFx0XHQkKCcjc2NfbW9kYWxfY29udGVudCcpLnJlbW92ZUNsYXNzKCdzY19sb2FkaW5nJyk7XG5cdFx0XHRpZiAoJCgnI3NjX3NpbmdsZV9jb250YWluZXInKS5kYXRhKCdpc19jb25maWd1cmVkJykgPT09IDEpXG5cdFx0XHR7XG5cdFx0XHRcdCQoJyNzY19zaG93X2xhYmVscycpLnNob3coKTtcblx0XHRcdH1cblx0XHRcdGVsc2Vcblx0XHRcdHtcblx0XHRcdFx0JCgnI3NjX3Nob3dfbGFiZWxzJykuaGlkZSgpO1xuXHRcdFx0fVxuXHRcdFx0JCgnI3NjX3NpbmdsZV9mb3JtJykub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGUpIHsgZS5wcmV2ZW50RGVmYXVsdCgpOyB9KTtcblx0XHRcdCQoJyNzY19zaW5nbGVfZm9ybSBpbnB1dC5jcmVhdGVfbGFiZWwnKS5vbignY2xpY2snLCBfc2luZ2xlRm9ybVN1Ym1pdEhhbmRsZXIpO1xuXHRcdFx0JCgnI3NjX3NpbmdsZV9mb3JtIHNlbGVjdFtuYW1lPVwiY2FycmllclwiXScpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdCQoJyNzY19zaW5nbGVfZm9ybSBpbnB1dFt0eXBlPVwidGV4dFwiXScpLnRyaWdnZXIoJ2NoYW5nZScpO1xuXHRcdFx0XHQkKCcjc2Nfc2luZ2xlX2Zvcm0gLmNhcnJpZXItc3BlY2lmaWMnKS5ub3QoJy5jYXJyaWVyLScrJCh0aGlzKS52YWwoKSkuaGlkZSgnZmFzdCcpO1xuXHRcdFx0XHQkKCcjc2Nfc2luZ2xlX2Zvcm0gLmNhcnJpZXItJyskKHRoaXMpLnZhbCgpKS5ub3QoJzp2aXNpYmxlJykuc2hvdygnZmFzdCcpO1xuXHRcdFx0fSk7XG5cdFx0XHQkKCcjc2Nfc2luZ2xlX2Zvcm0gLnByaWNlX3ZhbHVlJykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkKCcjc2Nfc2luZ2xlX2Zvcm0gZGl2LnNjX3F1b3RlJykuaHRtbCgnJyk7XG5cdFx0XHR9KTtcblx0XHRcdCQoJyNzY19wYWNrYWdlX3RlbXBsYXRlJykub24oJ2NoYW5nZScsIF90ZW1wbGF0ZVNlbGVjdGlvbkhhbmRsZXIpO1xuXHRcdFx0JCgnI3NjX3NpbmdsZV9mb3JtIGlucHV0LnRlbXBsYXRlX3ZhbHVlJykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKCkgeyAkKCcjc2NfcGFja2FnZV90ZW1wbGF0ZScpLnZhbCgnLTEnKTsgfSk7XG5cdFx0XHQkKCcjc2NfZ2V0X3F1b3RlJykuYnV0dG9uKCdkaXNhYmxlJyk7XG5cdFx0XHQkKCcjc2Nfc2luZ2xlX2Zvcm0gaW5wdXRbbmFtZT1cInF1b3RlX2NhcnJpZXJzW11cIl0nKS5vbignY2hhbmdlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGlmICgkKCcjc2Nfc2luZ2xlX2Zvcm0gaW5wdXRbbmFtZT1cInF1b3RlX2NhcnJpZXJzW11cIl06Y2hlY2tlZCcpLmxlbmd0aCA+IDApXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQkKCcjc2NfZ2V0X3F1b3RlJykuYnV0dG9uKCdlbmFibGUnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQkKCcjc2NfZ2V0X3F1b3RlJykuYnV0dG9uKCdkaXNhYmxlJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0JCgnI3NjX3NpbmdsZV9mb3JtIGlucHV0W25hbWU9XCJxdW90ZV9jYXJyaWVyc1tdXCJdOmZpcnN0JykudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0fTtcblxuXHRcdHZhciBfdGVtcGxhdGVTZWxlY3Rpb25IYW5kbGVyID0gZnVuY3Rpb24oZSkge1xuXHRcdFx0dmFyICRmb3JtLCAkdGVtcGxhdGU7XG5cdFx0XHQkZm9ybSAgICAgPSAkKHRoaXMpLmNsb3Nlc3QoJ2Zvcm0nKTtcblx0XHRcdCR0ZW1wbGF0ZSA9ICQoJ29wdGlvbjpzZWxlY3RlZCcsICQodGhpcykpO1xuXHRcdFx0aWYgKCR0ZW1wbGF0ZS52YWwoKSAhPT0gJy0xJylcblx0XHRcdHtcblx0XHRcdFx0JCgnaW5wdXRbbmFtZT1cInBhY2thZ2Vbd2VpZ2h0XVwiXScsICRmb3JtKS52YWwoJHRlbXBsYXRlLmRhdGEoJ3dlaWdodCcpKTtcblx0XHRcdFx0JCgnaW5wdXRbbmFtZT1cInBhY2thZ2VbaGVpZ2h0XVwiXScsICRmb3JtKS52YWwoJHRlbXBsYXRlLmRhdGEoJ2hlaWdodCcpKTtcblx0XHRcdFx0JCgnaW5wdXRbbmFtZT1cInBhY2thZ2Vbd2lkdGhdXCJdJywgICRmb3JtKS52YWwoJHRlbXBsYXRlLmRhdGEoJ3dpZHRoJykpO1xuXHRcdFx0XHQkKCdpbnB1dFtuYW1lPVwicGFja2FnZVtsZW5ndGhdXCJdJywgJGZvcm0pLnZhbCgkdGVtcGxhdGUuZGF0YSgnbGVuZ3RoJykpO1xuXHRcdFx0fVxuXHRcdH07XG5cblx0XHR2YXIgX29wZW5TaW5nbGVGb3JtTW9kYWwgPSBmdW5jdGlvbihldmVudClcblx0XHR7XG5cdFx0XHR2YXIgb3JkZXJJZCA9ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCd0cicpLmRhdGEoJ3Jvdy1pZCcpIHx8ICQoJ2JvZHknKS5maW5kKCcjZ21fb3JkZXJfaWQnKS52YWwoKTtcblx0XHRcdCQoJyNzY19tb2RhbF9jb250ZW50JykuZW1wdHkoKS5hZGRDbGFzcygnc2NfbG9hZGluZycpO1xuXHRcdFx0dmFyIGJ1dHRvbl9jcmVhdGVfbGFiZWwgPSBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY3JlYXRlX2xhYmVsJywgJ3NoaXBjbG91ZCcpLFxuXHRcdFx0XHRzaGlwY2xvdWRfbW9kYWxfYnV0dG9ucyA9IFtdO1xuXG5cdFx0XHRzaGlwY2xvdWRfbW9kYWxfYnV0dG9ucy5wdXNoKHtcblx0XHRcdFx0J3RleHQnOiAganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Nsb3NlJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0XHRcdCdjbGFzcyc6ICdidG4nLFxuXHRcdFx0XHRcdFx0J2NsaWNrJzogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdFx0XHQkKHRoaXMpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHRcdFx0JCgnI3NjX2dldF9xdW90ZScpLnNob3coKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0c2hpcGNsb3VkX21vZGFsX2J1dHRvbnMucHVzaCh7XG5cdFx0XHRcdCd0ZXh0JzogIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdzaG93X2V4aXN0aW5nX2xhYmVscycsICdzaGlwY2xvdWQnKSxcblx0XHRcdFx0J2NsYXNzJzogJ2J0bicsXG5cdFx0XHRcdCdjbGljayc6IF9zaG93TGFiZWxzSGFuZGxlcixcblx0XHRcdFx0J2lkJzogICAgJ3NjX3Nob3dfbGFiZWxzJ1xuXHRcdFx0fSk7XG5cdFx0XHRzaGlwY2xvdWRfbW9kYWxfYnV0dG9ucy5wdXNoKHtcblx0XHRcdFx0J3RleHQnOiAganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2dldF9xdW90ZXMnLCAnc2hpcGNsb3VkJyksXG5cdFx0XHRcdCdjbGFzcyc6ICdidG4gYnRuLXByaW1hcnknLFxuXHRcdFx0XHQnY2xpY2snOiBfc2luZ2xlRm9ybUdldFF1b3RlSGFuZGxlcixcblx0XHRcdFx0J2lkJzogICAgJ3NjX2dldF9xdW90ZSdcblx0XHRcdH0pO1xuXG5cdFx0XHQkKCcjc2hpcGNsb3VkX21vZGFsJykuZGlhbG9nKHtcblx0XHRcdFx0YXV0b09wZW46ICAgICAgZmFsc2UsXG5cdFx0XHRcdG1vZGFsOiAgICAgICAgIHRydWUsXG5cdFx0XHRcdCd0aXRsZSc6ICAgICAgIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdjcmVhdGVfbGFiZWwnLCAnc2hpcGNsb3VkJyksXG5cdFx0XHRcdCdkaWFsb2dDbGFzcyc6ICdneC1jb250YWluZXInLFxuXHRcdFx0XHRidXR0b25zOiAgICAgICBzaGlwY2xvdWRfbW9kYWxfYnV0dG9ucyxcblx0XHRcdFx0d2lkdGg6ICAgICAgICAgMTAwMCxcblx0XHRcdFx0cG9zaXRpb246ICAgICAgeyBteTogJ2NlbnRlciB0b3AnLCBhdDogJ2NlbnRlciBib3R0b20nLCBvZjogJy5tYWluLXRvcC1oZWFkZXInIH1cblx0XHRcdH0pO1xuXHRcdFx0JCgnI3NoaXBjbG91ZF9tb2RhbCcpLmRpYWxvZygnb3BlbicpO1xuXHRcdFx0JCgnI3NjX21vZGFsX2NvbnRlbnQnKS5sb2FkKCdhZG1pbi5waHA/ZG89U2hpcGNsb3VkL0NyZWF0ZUxhYmVsRm9ybSZvcmRlcnNfaWQ9JyArIG9yZGVySWQsXG5cdFx0XHRcdF9zaW5nbGVGb3JtSW5pdCk7XG5cdFx0fTtcblxuXHRcdHZhciBfYWRkU2hpcGNsb3VkRHJvcGRvd25FbnRyeSA9IGZ1bmN0aW9uICgpIHtcblx0XHRcdCQoJy5neC1vcmRlcnMtdGFibGUgdHInKS5ub3QoJy5kYXRhVGFibGVIZWFkaW5nUm93JykuZWFjaChmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5tYXBBY3Rpb24oJCh0aGlzKSwgJ2FkbWluX21lbnVfZW50cnknLCAnc2hpcGNsb3VkJywgX29wZW5TaW5nbGVGb3JtTW9kYWwpO1xuXHRcdFx0fSk7XG5cdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24ubWFwQWN0aW9uKCQoJy5ib3R0b20tc2F2ZS1iYXInKSwgJ2FkbWluX21lbnVfZW50cnknLCAnc2hpcGNsb3VkJywgX29wZW5TaW5nbGVGb3JtTW9kYWwpO1xuXHRcdH07XG5cblx0XHR2YXIgX2xhYmVsbGlzdFBpY2t1cENoZWNrYm94SGFuZGxlciA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0JCgnI3NjLWxhYmVsbGlzdC1kcm9wZG93biBidXR0b24sIGRpdi5waWNrdXBfdGltZSBpbnB1dCcpXG5cdFx0XHRcdC5wcm9wKCdkaXNhYmxlZCcsICQoJ2lucHV0LnBpY2t1cF9jaGVja2JveDpjaGVja2VkJykubGVuZ3RoID09PSAwKTtcblx0XHR9O1xuXG5cdFx0dmFyIF9sb2FkTGFiZWxMaXN0ID0gZnVuY3Rpb24gKG9yZGVyc19pZClcblx0XHR7XG5cdFx0XHQkKCcjc2NfbW9kYWxfY29udGVudCcpLmxvYWQoJ2FkbWluLnBocD9kbz1TaGlwY2xvdWQvTG9hZExhYmVsTGlzdCZvcmRlcnNfaWQ9JyArIG9yZGVyc19pZCxcblx0XHRcdFx0ZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdGd4LndpZGdldHMuaW5pdCgkKCcjc2NfbW9kYWxfY29udGVudCcpKTtcblx0XHRcdFx0XHQkKCcjc2hpcGNsb3VkX21vZGFsJykuZGlhbG9nKHtcblx0XHRcdFx0XHRcdCd0aXRsZSc6IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdsYWJlbGxpc3RfZm9yJywgJ3NoaXBjbG91ZCcpICsgJyAnICsgb3JkZXJzX2lkXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0JCgnI3NjX21vZGFsX2NvbnRlbnQnKS5yZW1vdmVDbGFzcygnc2NfbG9hZGluZycpO1xuXG5cdFx0XHRcdFx0JCgnZm9ybSNzY19waWNrdXAnKS5vbignc3VibWl0JywgZnVuY3Rpb24oZSkgeyBlLnByZXZlbnREZWZhdWx0KCk7IH0pO1xuXHRcdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5tYXBBY3Rpb24oXG5cdFx0XHRcdFx0XHQkKCcjc2MtbGFiZWxsaXN0LWRyb3Bkb3duJyksXG5cdFx0XHRcdFx0XHQnZG93bmxvYWRfbGFiZWxzJyxcblx0XHRcdFx0XHRcdCdzaGlwY2xvdWQnLFxuXHRcdFx0XHRcdFx0X3BhY2tlZERvd25sb2FkSGFuZGxlclxuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLm1hcEFjdGlvbihcblx0XHRcdFx0XHRcdCQoJyNzYy1sYWJlbGxpc3QtZHJvcGRvd24nKSxcblx0XHRcdFx0XHRcdCdvcmRlcl9waWNrdXBzJyxcblx0XHRcdFx0XHRcdCdzaGlwY2xvdWQnLFxuXHRcdFx0XHRcdFx0X3BpY2t1cFN1Ym1pdEhhbmRsZXJcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdCQoJ2lucHV0LnBpY2t1cF9jaGVja2JveCcpLm9uKCdjbGljaycsIF9sYWJlbGxpc3RQaWNrdXBDaGVja2JveEhhbmRsZXIpO1xuXHRcdFx0XHRcdHNldFRpbWVvdXQoX2xhYmVsbGlzdFBpY2t1cENoZWNrYm94SGFuZGxlciwgMjAwKTtcblx0XHRcdFx0XHQkKCdpbnB1dC5waWNrdXBfY2hlY2tib3hfYWxsJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGlmICgkKHRoaXMpLnByb3AoJ2NoZWNrZWQnKSA9PT0gdHJ1ZSlcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0JCgnaW5wdXQucGlja3VwX2NoZWNrYm94JykucHJvcCgnY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0XHRcdFx0XHQkKCdpbnB1dC5waWNrdXBfY2hlY2tib3gnKS5wYXJlbnQoKS5hZGRDbGFzcygnY2hlY2tlZCcpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHQkKCdpbnB1dC5waWNrdXBfY2hlY2tib3gnKS5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xuXHRcdFx0XHRcdFx0XHQkKCdpbnB1dC5waWNrdXBfY2hlY2tib3gnKS5wYXJlbnQoKS5yZW1vdmVDbGFzcygnY2hlY2tlZCcpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0X2xhYmVsbGlzdFBpY2t1cENoZWNrYm94SGFuZGxlcigpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdCQoJ2Euc2MtZGVsLWxhYmVsJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRcdFx0dmFyIHNoaXBtZW50X2lkICA9ICQodGhpcykuZGF0YSgnc2hpcG1lbnQtaWQnKSxcblx0XHRcdFx0XHRcdCAgICAkYnV0dG9uUGxhY2UgPSAkKHRoaXMpLmNsb3Nlc3QoJ3NwYW4uc2MtZGVsLWxhYmVsJyksXG5cdFx0XHRcdFx0XHQgICAgJHJvdyAgICAgICAgID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpO1xuXHRcdFx0XHRcdFx0JC5hamF4KHtcblx0XHRcdFx0XHRcdFx0dHlwZTogICAgICdQT1NUJyxcblx0XHRcdFx0XHRcdFx0dXJsOiAgICAgICBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVNoaXBjbG91ZC9EZWxldGVTaGlwbWVudCcsXG5cdFx0XHRcdFx0XHRcdGRhdGE6ICAgICB7IHNoaXBtZW50X2lkOiBzaGlwbWVudF9pZCB9LFxuXHRcdFx0XHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nXG5cdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0LmRvbmUoZnVuY3Rpb24oZGF0YSkge1xuXHRcdFx0XHRcdFx0XHRpZihkYXRhLnJlc3VsdCA9PT0gJ0VSUk9SJylcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdCRidXR0b25QbGFjZS5odG1sKGRhdGEuZXJyb3JfbWVzc2FnZSk7XG5cdFx0XHRcdFx0XHRcdFx0JGJ1dHRvblBsYWNlLmFkZENsYXNzKCdiYWRnZScpLmFkZENsYXNzKCdiYWRnZS1kYW5nZXInKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0XHQkYnV0dG9uUGxhY2UuaHRtbChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc2hpcG1lbnRfZGVsZXRlZCcsICdzaGlwY2xvdWQnKSk7XG5cdFx0XHRcdFx0XHRcdFx0JCgnYSwgaW5wdXQsIHRkLmNoZWNrYm94ID4gKicsICRyb3cpLnJlbW92ZSgpO1xuXHRcdFx0XHRcdFx0XHRcdCRyb3cuYWRkQ2xhc3MoJ2RlbGV0ZWQtc2hpcG1lbnQnKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdC5mYWlsKGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdFx0XHRcdFx0JGJ1dHRvblBsYWNlLmh0bWwoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3N1Ym1pdF9lcnJvcicsICdzaGlwY2xvdWQnKSk7XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fSk7XG5cdFx0fTtcblxuXHRcdHZhciBfcGFja2VkRG93bmxvYWRIYW5kbGVyID0gZnVuY3Rpb24oZSlcblx0XHR7XG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHR2YXIgdXJscyA9IFtdLCByZXF1ZXN0ID0ge307XG5cdFx0XHQkKCdpbnB1dC5waWNrdXBfY2hlY2tib3g6Y2hlY2tlZCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHZhciBocmVmID0gJCgnYS5sYWJlbC1saW5rJywgJCh0aGlzKS5jbG9zZXN0KCd0cicpKS5hdHRyKCdocmVmJyk7XG5cdFx0XHRcdHVybHMucHVzaChocmVmKTtcblx0XHRcdH0pO1xuXHRcdFx0aWYgKHVybHMpXG5cdFx0XHR7XG5cdFx0XHRcdCQoJyNkb3dubG9hZF9yZXN1bHQnKS5zaG93KCk7XG5cdFx0XHRcdCQoJyNkb3dubG9hZF9yZXN1bHQnKS5odG1sKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdsb2FkaW5nJywgJ3NoaXBjbG91ZCcpKTtcblx0XHRcdFx0cmVxdWVzdC51cmxzICAgICAgID0gdXJscztcblx0XHRcdFx0cmVxdWVzdC5wYWdlX3Rva2VuID0gJCgnI3NjX21vZGFsX2NvbnRlbnQgaW5wdXRbbmFtZT1cInBhZ2VfdG9rZW5cIl0nKS52YWwoKTtcblxuXHRcdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHRcdHR5cGU6ICAgICAnUE9TVCcsXG5cdFx0XHRcdFx0dXJsOiAgICAgICBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVBhY2tlZERvd25sb2FkL0Rvd25sb2FkQnlKc29uJyxcblx0XHRcdFx0XHRkYXRhOiAgICAgSlNPTi5zdHJpbmdpZnkocmVxdWVzdCksXG5cdFx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJ1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuZG9uZShmdW5jdGlvbihkYXRhKSB7XG5cdFx0XHRcdFx0dmFyIGRvd25sb2FkbGluayA9XG5cdFx0XHRcdFx0XHQgIGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICtcblx0XHRcdFx0XHRcdCAgJy9hZG1pbi9hZG1pbi5waHA/ZG89UGFja2VkRG93bmxvYWQvRG93bmxvYWRQYWNrYWdlJmtleT0nICtcblx0XHRcdFx0XHRcdCAgZGF0YS5kb3dubG9hZEtleTtcblx0XHRcdFx0XHRpZiAoZGF0YS5yZXN1bHQgPT09ICdPSycpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0JCgnI2Rvd25sb2FkX3Jlc3VsdCcpLmh0bWwoJzxpZnJhbWUgY2xhc3M9XCJkb3dubG9hZF9pZnJhbWVcIiBzcmM9XCInK2Rvd25sb2FkbGluaysnXCI+PC9pZnJhbWU+Jyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGlmIChkYXRhLnJlc3VsdCA9PT0gJ0VSUk9SJylcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHQkKCcjZG93bmxvYWRfcmVzdWx0JykuaHRtbChkYXRhLmVycm9yX21lc3NhZ2UpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSlcblx0XHRcdFx0LmZhaWwoZnVuY3Rpb24oZGF0YSkge1xuXHRcdFx0XHRcdCQoJyNkb3dubG9hZF9yZXN1bHQnKS5odG1sKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdzdWJtaXRfZXJyb3InLCAnc2hpcGNsb3VkJykpO1xuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH07XG5cblx0XHR2YXIgX3BpY2t1cFN1Ym1pdEhhbmRsZXIgPSBmdW5jdGlvbihlKSB7XG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRpZiAoJCgnaW5wdXQucGlja3VwX2NoZWNrYm94OmNoZWNrZWQnKS5sZW5ndGggPiAwKVxuXHRcdFx0e1xuXHRcdFx0XHR2YXIgZm9ybWRhdGEgPSAkKCdmb3JtI3NjX3BpY2t1cCcpLnNlcmlhbGl6ZSgpO1xuXHRcdFx0XHQkKCcjcGlja3VwX3Jlc3VsdCcpLmh0bWwoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3NlbmRpbmdfcGlja3VwX3JlcXVlc3QnLCAnc2hpcGNsb3VkJykpO1xuXHRcdFx0XHQkKCcjcGlja3VwX3Jlc3VsdCcpLnNob3coKTtcblx0XHRcdFx0JC5hamF4KHtcblx0XHRcdFx0XHR0eXBlOiAgICAgJ1BPU1QnLFxuXHRcdFx0XHRcdHVybDogICAgICBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVNoaXBjbG91ZC9QaWNrdXBTaGlwbWVudHMnLFxuXHRcdFx0XHRcdGRhdGE6ICAgICBmb3JtZGF0YSxcblx0XHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nXG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5kb25lKGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdFx0XHR2YXIgcmVzdWx0X21lc3NhZ2UgPSAnJztcblx0XHRcdFx0XHRkYXRhLnJlc3VsdF9tZXNzYWdlcy5mb3JFYWNoKGZ1bmN0aW9uKG1lc3NhZ2UpIHsgcmVzdWx0X21lc3NhZ2UgPSByZXN1bHRfbWVzc2FnZSArIG1lc3NhZ2UgKyAnPGJyPic7IH0pO1xuXHRcdFx0XHRcdCQoJyNwaWNrdXBfcmVzdWx0JykuaHRtbChyZXN1bHRfbWVzc2FnZSk7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5mYWlsKGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdFx0XHRhbGVydChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc3VibWl0X2Vycm9yJywgJ3NoaXBjbG91ZCcpKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9O1xuXG5cdFx0dmFyIF9sb2FkVW5jb25maWd1cmVkTm90ZSA9IGZ1bmN0aW9uICgpXG5cdFx0e1xuXHRcdFx0JCgnI3NjX21vZGFsX2NvbnRlbnQnKS5sb2FkKCdhZG1pbi5waHA/ZG89U2hpcGNsb3VkL1VuY29uZmlndXJlZE5vdGUnKTtcblx0XHR9O1xuXG5cdFx0dmFyIF9zaG93TGFiZWxzSGFuZGxlciA9IGZ1bmN0aW9uIChlKSB7XG5cdFx0XHR2YXIgb3JkZXJzX2lkID0gJCgnI3NjX3NpbmdsZV9mb3JtIGlucHV0W25hbWU9XCJvcmRlcnNfaWRcIl0nKS52YWwoKTtcblx0XHRcdCQoJyNzY19tb2RhbF9jb250ZW50JykuZW1wdHkoKS5hZGRDbGFzcygnc2NfbG9hZGluZycpO1xuXHRcdFx0X2xvYWRMYWJlbExpc3Qob3JkZXJzX2lkKTtcblx0XHRcdCQoJyNzY19zaG93X2xhYmVscycpLmhpZGUoKTtcblx0XHRcdCQoJyNzY19nZXRfcXVvdGUnKS5oaWRlKCk7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fTtcblxuXHRcdHZhciBfc2luZ2xlRm9ybUdldFF1b3RlSGFuZGxlciA9IGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgJGZvcm0gPSAkKCcjc2Nfc2luZ2xlX2Zvcm0nKSxcblx0XHRcdCAgICBxdW90ZSA9ICcnO1xuXG5cdFx0XHQkKCcjc2Nfc2luZ2xlX2Zvcm0gLnNjX3F1b3RlJykuaHRtbCgnJyk7XG5cdFx0XHQkKCcjc2Nfc2luZ2xlX2Zvcm0gLnNjX3F1b3RlJykuYXR0cigndGl0bGUnLCAnJyk7XG5cblx0XHRcdCQoJ2lucHV0W25hbWU9XCJxdW90ZV9jYXJyaWVyc1tdXCJdOmNoZWNrZWQnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgY2FycmllciA9ICQodGhpcykudmFsKCksXG5cdFx0XHRcdCAgICAkY3JlYXRlX2xhYmVsID0gJCgnaW5wdXQuY3JlYXRlX2xhYmVsJywgJCh0aGlzKS5jbG9zZXN0KCd0cicpKTtcblx0XHRcdFx0JCgnaW5wdXRbbmFtZT1cImNhcnJpZXJcIl0nLCAkZm9ybSkudmFsKGNhcnJpZXIpO1xuXHRcdFx0XHQkKCcjc2NfcXVvdGVfJytjYXJyaWVyKS5odG1sKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdsb2FkaW5nJywgJ3NoaXBjbG91ZCcpKTtcblx0XHRcdFx0JC5hamF4KHtcblx0XHRcdFx0XHR0eXBlOiAgICAgJ1BPU1QnLFxuXHRcdFx0XHRcdHVybDogICAgICBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVNoaXBjbG91ZC9HZXRTaGlwbWVudFF1b3RlJyxcblx0XHRcdFx0XHRkYXRhOiAgICAgJGZvcm0uc2VyaWFsaXplKCksXG5cdFx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJ1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuXHRcdFx0XHRcdGlmIChkYXRhLnJlc3VsdCA9PT0gJ09LJylcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRxdW90ZSA9IGRhdGEuc2hpcG1lbnRfcXVvdGU7XG5cdFx0XHRcdFx0XHQkKCcjc2NfcXVvdGVfJytjYXJyaWVyKS5odG1sKHF1b3RlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSBpZiAoZGF0YS5yZXN1bHQgPT09ICdFUlJPUicpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0JCgnI3NjX3F1b3RlXycrY2FycmllcikuaHRtbChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnbm90X3Bvc3NpYmxlJywgJ3NoaXBjbG91ZCcpKTtcblx0XHRcdFx0XHRcdCQoJyNzY19xdW90ZV8nK2NhcnJpZXIpLmF0dHIoJ3RpdGxlJywgZGF0YS5lcnJvcl9tZXNzYWdlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSBpZiAoZGF0YS5yZXN1bHQgPT09ICdVTkNPTkZJR1VSRUQnKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdF9sb2FkVW5jb25maWd1cmVkTm90ZSgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSlcblx0XHRcdFx0LmZhaWwoZnVuY3Rpb24gKGRhdGEpIHtcblx0XHRcdFx0XHRxdW90ZSA9IGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdnZXRfcXVvdGVfZXJyb3InLCAnc2hpcGNsb3VkJyk7XG5cdFx0XHRcdFx0JCgnI3NjX3F1b3RlXycrY2FycmllcikuaHRtbChxdW90ZSk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cblx0XHRcdCQoJ2lucHV0W25hbWU9XCJjYXJyaWVyXCJdJywgJGZvcm0pLnZhbCgnJyk7XG5cdFx0fTtcblxuXHRcdHZhciBfc2luZ2xlRm9ybVN1Ym1pdEhhbmRsZXIgPSBmdW5jdGlvbiAoZSkge1xuXHRcdFx0dmFyIGNhcnJpZXIsIGZvcm1kYXRhO1xuXHRcdFx0JCgnI3NjX3Nob3dfbGFiZWxzJykuaGlkZSgpO1xuXHRcdFx0JCgnI3NjX2dldF9xdW90ZScpLmhpZGUoKTtcblx0XHRcdGNhcnJpZXIgPSAkKHRoaXMpLmF0dHIoJ25hbWUnKTtcblx0XHRcdCQoJ2lucHV0W25hbWU9XCJjYXJyaWVyXCJdJykudmFsKGNhcnJpZXIpO1xuXHRcdFx0Zm9ybWRhdGEgPSAkKCcjc2Nfc2luZ2xlX2Zvcm0nKS5zZXJpYWxpemUoKTtcblx0XHRcdCQoJyNzY19tb2RhbF9jb250ZW50JykuZW1wdHkoKS5hZGRDbGFzcygnc2NfbG9hZGluZycpO1xuXHRcdFx0Ly8gYWxlcnQoJ2RhdGE6ICcrZm9ybWRhdGEpO1xuXHRcdFx0JC5hamF4KHtcblx0XHRcdFx0dHlwZTogICAgICdQT1NUJyxcblx0XHRcdFx0dXJsOiAgICAgIGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89U2hpcGNsb3VkL0NyZWF0ZUxhYmVsRm9ybVN1Ym1pdCcsXG5cdFx0XHRcdGRhdGE6ICAgICBmb3JtZGF0YSxcblx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJ1xuXHRcdFx0fSlcblx0XHRcdC5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG5cdFx0XHRcdCQoJyNzY19tb2RhbF9jb250ZW50JykucmVtb3ZlQ2xhc3MoJ3NjX2xvYWRpbmcnKTtcblx0XHRcdFx0aWYgKGRhdGEucmVzdWx0ID09PSAnVU5DT05GSUdVUkVEJylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdF9sb2FkVW5jb25maWd1cmVkTm90ZSgpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2UgaWYgKGRhdGEucmVzdWx0ID09PSAnT0snKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0X2xvYWRMYWJlbExpc3QoZGF0YS5vcmRlcnNfaWQpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGlmIChkYXRhLmVycm9yX21lc3NhZ2UpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0JCgnI3NjX21vZGFsX2NvbnRlbnQnKS5odG1sKCc8ZGl2IGNsYXNzPVwic2NfZXJyb3JcIj4nK2RhdGEuZXJyb3JfbWVzc2FnZSsnPC9kaXY+Jyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KVxuXHRcdFx0LmZhaWwoZnVuY3Rpb24gKGRhdGEpIHtcblx0XHRcdFx0YWxlcnQoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ3N1Ym1pdF9lcnJvcicsICdzaGlwY2xvdWQnKSk7XG5cdFx0XHR9KTtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9O1xuXG5cdFx0dmFyIF9tdWx0aURyb3Bkb3duSGFuZGxlciA9IGZ1bmN0aW9uKGUpXG5cdFx0e1xuXHRcdFx0dmFyIHNlbGVjdGVkX29yZGVycyA9IFtdLCBvcmRlcnNfcGFyYW0gPSAnJztcblx0XHRcdCQoJ2lucHV0W25hbWU9XCJnbV9tdWx0aV9zdGF0dXNbXVwiXTpjaGVja2VkJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0c2VsZWN0ZWRfb3JkZXJzLnB1c2goJCh0aGlzKS52YWwoKSk7XG5cdFx0XHR9KTtcblx0XHRcdCQoJyNzY19tb2RhbF9jb250ZW50JykuZW1wdHkoKS5hZGRDbGFzcygnc2NfbG9hZGluZycpO1xuXHRcdFx0dmFyIHNoaXBjbG91ZF9tb2RhbF9idXR0b25zID0gW107XG5cdFx0XHRzaGlwY2xvdWRfbW9kYWxfYnV0dG9ucy5wdXNoKHtcblx0XHRcdFx0J3RleHQnOiAganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2Nsb3NlJywgJ2J1dHRvbnMnKSxcblx0XHRcdFx0J2NsYXNzJzogJ2J0bicsXG5cdFx0XHRcdCdjbGljayc6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHQkKHRoaXMpLmRpYWxvZygnY2xvc2UnKTtcblx0XHRcdFx0XHQkKCcjc2NfZ2V0X3F1b3RlJykuc2hvdygpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdHNoaXBjbG91ZF9tb2RhbF9idXR0b25zLnB1c2goe1xuXHRcdFx0XHQndGV4dCc6ICBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZ2V0X3F1b3RlcycsICdzaGlwY2xvdWQnKSxcblx0XHRcdFx0J2NsYXNzJzogJ2J0biBidG4tcHJpbWFyeScsXG5cdFx0XHRcdCdjbGljayc6IF9tdWx0aUZvcm1HZXRRdW90ZUhhbmRsZXIsXG5cdFx0XHRcdCdpZCc6ICAgICdzY19nZXRfcXVvdGUnXG5cdFx0XHR9KTtcblxuXHRcdFx0JCgnI3NoaXBjbG91ZF9tb2RhbCcpLmRpYWxvZyh7XG5cdFx0XHRcdGF1dG9PcGVuOiAgICAgIGZhbHNlLFxuXHRcdFx0XHRtb2RhbDogICAgICAgICB0cnVlLFxuXHRcdFx0XHQndGl0bGUnOiAgICAgICBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY3JlYXRlX2xhYmVscycsICdzaGlwY2xvdWQnKSxcblx0XHRcdFx0J2RpYWxvZ0NsYXNzJzogJ2d4LWNvbnRhaW5lcicsXG5cdFx0XHRcdGJ1dHRvbnM6ICAgICAgIHNoaXBjbG91ZF9tb2RhbF9idXR0b25zLFxuXHRcdFx0XHR3aWR0aDogICAgICAgICAxMDAwLFxuXHRcdFx0XHRwb3NpdGlvbjogICAgICB7IG15OiAnY2VudGVyIHRvcCcsIGF0OiAnY2VudGVyIGJvdHRvbScsIG9mOiAnLm1haW4tdG9wLWhlYWRlcicgfVxuXHRcdFx0fSk7XG5cblx0XHRcdCQoJyNzaGlwY2xvdWRfbW9kYWwnKS5kaWFsb2coJ29wZW4nKTtcblx0XHRcdHNlbGVjdGVkX29yZGVycy5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW0pIHtcblx0XHRcdFx0b3JkZXJzX3BhcmFtICs9ICdvcmRlcnNbXT0nK2l0ZW0rJyYnO1xuXHRcdFx0fSk7XG5cdFx0XHQkKCcjc2NfbW9kYWxfY29udGVudCcpLmxvYWQoJ2FkbWluLnBocD9kbz1TaGlwY2xvdWQvQ3JlYXRlTXVsdGlMYWJlbEZvcm0mJytvcmRlcnNfcGFyYW0sIF9tdWx0aUZvcm1Jbml0KTtcblx0XHR9O1xuXG5cdFx0dmFyIF9tdWx0aUZvcm1Jbml0ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQkKCcjc2hpcGNsb3VkX21vZGFsJykuZGlhbG9nKHtcblx0XHRcdFx0J3RpdGxlJzoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2NyZWF0ZV9sYWJlbHMnLCAnc2hpcGNsb3VkJylcblx0XHRcdH0pO1xuXHRcdFx0JCgnI3NjX21vZGFsX2NvbnRlbnQnKS5yZW1vdmVDbGFzcygnc2NfbG9hZGluZycpO1xuXHRcdFx0JCgnI3NjX211bHRpX2Zvcm0nKS5vbignc3VibWl0JywgZnVuY3Rpb24oZSkgeyBlLnByZXZlbnREZWZhdWx0KCk7IHJldHVybiBmYWxzZTsgfSk7XG5cdFx0XHQkKCcjc2NfY3JlYXRlX2xhYmVsJykuaGlkZSgpO1xuXHRcdFx0JCgnI3NjX3Nob3dfbGFiZWxzJykuaGlkZSgpO1xuXHRcdFx0JCgnI3NjX21vZGFsX2NvbnRlbnQgaW5wdXQsICNzY19tb2RhbF9jb250ZW50IHNlbGVjdCcpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0JCgnLnNjX211bHRpX3F1b3RlJykuaGlkZSgpO1xuXHRcdFx0fSk7XG5cdFx0XHQkKCcjc2NfcGFja2FnZV90ZW1wbGF0ZScpLm9uKCdjaGFuZ2UnLCBfdGVtcGxhdGVTZWxlY3Rpb25IYW5kbGVyKTtcblx0XHRcdCQoJ2lucHV0LmNyZWF0ZV9sYWJlbCcpLm9uKCdjbGljaycsIF9tdWx0aUZvcm1TdWJtaXRIYW5kbGVyKTtcblx0XHR9O1xuXG5cdFx0dmFyIF9tdWx0aUZvcm1TdWJtaXRIYW5kbGVyID0gZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdHZhciBmb3JtZGF0YSwgY2Fycmllcjtcblx0XHRcdGNhcnJpZXIgPSAkKHRoaXMpLmF0dHIoJ25hbWUnKTtcblx0XHRcdCQoJyNzY19tdWx0aV9mb3JtIGlucHV0W25hbWU9XCJjYXJyaWVyXCJdJykudmFsKGNhcnJpZXIpO1xuXHRcdFx0Zm9ybWRhdGEgPSAkKCcjc2NfbXVsdGlfZm9ybScpLnNlcmlhbGl6ZSgpO1xuXHRcdFx0JCgnI3NjX21vZGFsX2NvbnRlbnQnKS5lbXB0eSgpLmFkZENsYXNzKCdzY19sb2FkaW5nJyk7XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR0eXBlOiAgICAgJ1BPU1QnLFxuXHRcdFx0XHR1cmw6ICAgICAganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1TaGlwY2xvdWQvQ3JlYXRlTXVsdGlMYWJlbEZvcm1TdWJtaXQnLFxuXHRcdFx0XHRkYXRhOiAgICAgZm9ybWRhdGEsXG5cdFx0XHRcdGRhdGFUeXBlOiAnanNvbidcblx0XHRcdH0pXG5cdFx0XHQuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuXHRcdFx0XHQkKCcjc2NfbW9kYWxfY29udGVudCcpLnJlbW92ZUNsYXNzKCdzY19sb2FkaW5nJyk7XG5cdFx0XHRcdGlmIChkYXRhLnJlc3VsdCA9PT0gJ1VOQ09ORklHVVJFRCcpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRfbG9hZFVuY29uZmlndXJlZE5vdGUoKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIGlmIChkYXRhLnJlc3VsdCA9PT0gJ09LJylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdF9sb2FkTXVsdGlMYWJlbExpc3QoZGF0YS5vcmRlcnNfaWRzLCBkYXRhLnNoaXBtZW50cyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYgKGRhdGEuZXJyb3JfbWVzc2FnZSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHQkKCcjc2NfbW9kYWxfY29udGVudCcpLmh0bWwoJzxkaXYgY2xhc3M9XCJzY19lcnJvclwiPicrZGF0YS5lcnJvcl9tZXNzYWdlKyc8L2Rpdj4nKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0XHQuZmFpbChmdW5jdGlvbiAoZGF0YSkge1xuXHRcdFx0XHRhbGVydChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc3VibWl0X2Vycm9yJywgJ3NoaXBjbG91ZCcpKTtcblx0XHRcdH0pO1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH07XG5cblx0XHR2YXIgX2xvYWRNdWx0aUxhYmVsTGlzdCA9IGZ1bmN0aW9uKG9yZGVyc19pZHMsIHNoaXBtZW50cylcblx0XHR7XG5cdFx0XHR2YXIgbXVsdGlMYWJlbExpc3RQYXJhbXMgPSB7ICdvcmRlcnNfaWRzJzogb3JkZXJzX2lkcywgJ3NoaXBtZW50cyc6IHNoaXBtZW50cyB9O1xuXG5cdFx0XHQkKCcjc2NfbW9kYWxfY29udGVudCcpLmxvYWQoXG5cdFx0XHRcdGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpICsgJy9hZG1pbi9hZG1pbi5waHA/ZG89U2hpcGNsb3VkL0xvYWRNdWx0aUxhYmVsTGlzdCcsXG5cdFx0XHRcdHsgXCJqc29uXCI6IEpTT04uc3RyaW5naWZ5KG11bHRpTGFiZWxMaXN0UGFyYW1zKSB9LFxuXHRcdFx0XHRmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdFx0Z3gud2lkZ2V0cy5pbml0KCQoJyNzaGlwY2xvdWRfbW9kYWwnKSk7XG5cdFx0XHRcdFx0JCgnI3NoaXBjbG91ZF9tb2RhbCcpLmRpYWxvZyh7XG5cdFx0XHRcdFx0XHQndGl0bGUnOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnbGFiZWxsaXN0JywgJ3NoaXBjbG91ZCcpXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0JCgnI3NjX21vZGFsX2NvbnRlbnQnKS5yZW1vdmVDbGFzcygnc2NfbG9hZGluZycpO1xuXHRcdFx0XHRcdCQoJyNzY19nZXRfcXVvdGUnKS5oaWRlKCk7XG5cblx0XHRcdFx0XHQkKCdmb3JtI3NjX3BpY2t1cCcpLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihlKSB7IGUucHJldmVudERlZmF1bHQoKTsgfSk7XG5cdFx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLm1hcEFjdGlvbihcblx0XHRcdFx0XHRcdCQoJyNzYy1sYWJlbGxpc3QtZHJvcGRvd24nKSxcblx0XHRcdFx0XHRcdCdkb3dubG9hZF9sYWJlbHMnLFxuXHRcdFx0XHRcdFx0J3NoaXBjbG91ZCcsXG5cdFx0XHRcdFx0XHRfcGFja2VkRG93bmxvYWRIYW5kbGVyXG5cdFx0XHRcdFx0KTtcblx0XHRcdFx0XHRqc2UubGlicy5idXR0b25fZHJvcGRvd24ubWFwQWN0aW9uKFxuXHRcdFx0XHRcdFx0JCgnI3NjLWxhYmVsbGlzdC1kcm9wZG93bicpLFxuXHRcdFx0XHRcdFx0J29yZGVyX3BpY2t1cHMnLFxuXHRcdFx0XHRcdFx0J3NoaXBjbG91ZCcsXG5cdFx0XHRcdFx0XHRfcGlja3VwU3VibWl0SGFuZGxlclxuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0JCgnaW5wdXQucGlja3VwX2NoZWNrYm94Jykub24oJ2NsaWNrJywgX2xhYmVsbGlzdFBpY2t1cENoZWNrYm94SGFuZGxlcik7XG5cdFx0XHRcdFx0c2V0VGltZW91dChfbGFiZWxsaXN0UGlja3VwQ2hlY2tib3hIYW5kbGVyLCAyMDApO1xuXHRcdFx0XHRcdCQoJ2lucHV0LnBpY2t1cF9jaGVja2JveF9hbGwnKS5vbignY2xpY2snLCBmdW5jdGlvbigpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0aWYgKCQodGhpcykucHJvcCgnY2hlY2tlZCcpID09PSB0cnVlKVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHQkKCdpbnB1dC5waWNrdXBfY2hlY2tib3gnKS5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG5cdFx0XHRcdFx0XHRcdCQoJ2lucHV0LnBpY2t1cF9jaGVja2JveCcpLnBhcmVudCgpLmFkZENsYXNzKCdjaGVja2VkJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdCQoJ2lucHV0LnBpY2t1cF9jaGVja2JveCcpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHRcdFx0XHRcdCQoJ2lucHV0LnBpY2t1cF9jaGVja2JveCcpLnBhcmVudCgpLnJlbW92ZUNsYXNzKCdjaGVja2VkJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRfbGFiZWxsaXN0UGlja3VwQ2hlY2tib3hIYW5kbGVyKCk7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdCk7XG5cdFx0fTtcblxuXHRcdHZhciBfbXVsdGlQaWNrdXBTdWJtaXRIYW5kbGVyID0gX3BpY2t1cFN1Ym1pdEhhbmRsZXI7XG5cblx0XHR2YXIgX211bHRpRm9ybUdldFF1b3RlSGFuZGxlciA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIGZvcm1kYXRhO1xuXHRcdFx0JCgnZGl2LnNjX3F1b3RlJykuaHRtbCgnJyk7XG5cdFx0XHRmb3JtZGF0YSA9ICQoJyNzY19tdWx0aV9mb3JtJykuc2VyaWFsaXplKCk7XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR0eXBlOiAgICAgJ1BPU1QnLFxuXHRcdFx0XHR1cmw6ICAgICAganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1TaGlwY2xvdWQvR2V0TXVsdGlTaGlwbWVudFF1b3RlJyxcblx0XHRcdFx0ZGF0YTogICAgIGZvcm1kYXRhLFxuXHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nXG5cdFx0XHR9KVxuXHRcdFx0LmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcblx0XHRcdFx0aWYgKGRhdGEucmVzdWx0ID09PSAnT0snKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0Zm9yKHZhciBzcXVvdGUgaW4gZGF0YS5zaGlwbWVudF9xdW90ZXMpIHtcblx0XHRcdFx0XHRcdCQoJyNzY19tdWx0aV9xdW90ZV8nICsgZGF0YS5zaGlwbWVudF9xdW90ZXNbc3F1b3RlXS5vcmRlcnNfaWQpXG5cdFx0XHRcdFx0XHRcdC5odG1sKGRhdGEuc2hpcG1lbnRfcXVvdGVzW3NxdW90ZV0uc2hpcG1lbnRfcXVvdGUpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQkKCdkaXYuc2NfbXVsdGlfcXVvdGUnKS5zaG93KCdmYXN0Jyk7XG5cblx0XHRcdFx0XHRmb3IodmFyIGNhcnJpZXIgaW4gZGF0YS5jYXJyaWVyc190b3RhbClcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHQkKCcjc2NfcXVvdGVfJytjYXJyaWVyKS5odG1sKGRhdGEuY2FycmllcnNfdG90YWxbY2Fycmllcl0pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSlcblx0XHRcdC5mYWlsKGZ1bmN0aW9uIChkYXRhKSB7XG5cdFx0XHRcdGFsZXJ0KGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdzdWJtaXRfZXJyb3InLCAnc2hpcGNsb3VkJykpO1xuXHRcdFx0fSk7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fTtcblxuXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbiAoZG9uZSkge1xuXHRcdFx0JCgnYm9keScpLnByZXBlbmQoJCgnPGRpdiBpZD1cInNoaXBjbG91ZF9tb2RhbFwiIHRpdGxlPVwiJyArIGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKFxuXHRcdFx0XHRcdCdjcmVhdGVfbGFiZWxfd2luZG93X3RpdGxlJywgJ3NoaXBjbG91ZCcpICtcblx0XHRcdFx0J1wiIHN0eWxlPVwiZGlzcGxheTogbm9uZTtcIj48ZGl2IGlkPVwic2NfbW9kYWxfY29udGVudFwiPjwvZGl2PjwvZGl2PicpKTtcblxuXHRcdFx0dmFyIGludGVydmFsX2NvdW50ZXIgPSAxMCxcblx0XHRcdCAgICBpbnRlcnZhbCA9IHNldEludGVydmFsKGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0aWYgKCQoJy5qcy1idXR0b24tZHJvcGRvd24nKS5sZW5ndGgpIHtcblx0XHRcdFx0XHRjbGVhckludGVydmFsKGludGVydmFsKTtcblx0XHRcdFx0XHRfYWRkU2hpcGNsb3VkRHJvcGRvd25FbnRyeSgpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmIChpbnRlcnZhbF9jb3VudGVyLS0gPT09IDApXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRjbGVhckludGVydmFsKGludGVydmFsKTtcblx0XHRcdFx0fVxuXHRcdFx0fSwgNDAwKTtcblxuXHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLm1hcEFjdGlvbigkKCcjb3JkZXJzLXRhYmxlLWRyb3Bkb3duJyksICdjcmVhdGVfbGFiZWxzJywgJ3NoaXBjbG91ZCcsIF9tdWx0aURyb3Bkb3duSGFuZGxlcik7XG5cblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fVxuKTtcblxuIl19
