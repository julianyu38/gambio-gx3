'use strict';

/* --------------------------------------------------------------
 datatable_loading_spinner.js 2017-03-16
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Enable DataTable Loading Spinner
 *
 * The loading spinner will be visible during every DataTable AJAX request.
 * 
 * ### Options 
 * 
 * ** Z-Index Reference Selector | `data-datatable_loading_spinner-z-index-reference-selector` | String | Optional**
 * Provide a reference selector that will be used as a z-index reference. Defaults to ".table-fixed-header thead.fixed".
 *
 * @module Admin/Extensions/datatable_loading_spinner
 */
gx.extensions.module('datatable_loading_spinner', ['loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options 
  * 
  * @type {Object}
  */
	var defaults = {
		zIndexReferenceSelector: '.table-fixed-header thead.fixed'
	};

	/**
  * Final Options
  * 
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Loading Spinner Selector
  *
  * @type {jQuery}
  */
	var $spinner = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * On Pre DataTable XHR Event
  *
  * Display the loading spinner on the table.
  */
	function _onDataTablePreXhr() {
		var zIndex = parseInt($(options.zIndexReferenceSelector).css('z-index'));
		$spinner = jse.libs.loading_spinner.show($this, zIndex);
	}

	/**
  * On XHR DataTable Event
  *
  * Hide the displayed loading spinner.
  */
	function _onDataTableXhr() {
		if ($spinner) {
			jse.libs.loading_spinner.hide($spinner);
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('preXhr.dt', _onDataTablePreXhr).on('xhr.dt', _onDataTableXhr);

		$(window).on('JSENGINE_INIT_FINISHED', function () {
			_onDataTablePreXhr();

			// Hide the spinner if the table is already loaded.  
			if ($this.DataTable().ajax.json() !== undefined) {
				_onDataTableXhr();
			}
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGF0YWJsZV9sb2FkaW5nX3NwaW5uZXIuanMiXSwibmFtZXMiOlsiZ3giLCJleHRlbnNpb25zIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwiekluZGV4UmVmZXJlbmNlU2VsZWN0b3IiLCJvcHRpb25zIiwiZXh0ZW5kIiwiJHNwaW5uZXIiLCJfb25EYXRhVGFibGVQcmVYaHIiLCJ6SW5kZXgiLCJwYXJzZUludCIsImNzcyIsImpzZSIsImxpYnMiLCJsb2FkaW5nX3NwaW5uZXIiLCJzaG93IiwiX29uRGF0YVRhYmxlWGhyIiwiaGlkZSIsImluaXQiLCJkb25lIiwib24iLCJ3aW5kb3ciLCJEYXRhVGFibGUiLCJhamF4IiwianNvbiIsInVuZGVmaW5lZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7QUFZQUEsR0FBR0MsVUFBSCxDQUFjQyxNQUFkLENBQXFCLDJCQUFyQixFQUFrRCxDQUFDLGlCQUFELENBQWxELEVBQXVFLFVBQVNDLElBQVQsRUFBZTs7QUFFckY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCQywyQkFBeUI7QUFEVCxFQUFqQjs7QUFJQTs7Ozs7QUFLQSxLQUFNQyxVQUFVSCxFQUFFSSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJILFFBQW5CLEVBQTZCSCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRCxTQUFTLEVBQWY7O0FBRUE7Ozs7O0FBS0EsS0FBSVEsaUJBQUo7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNDLGtCQUFULEdBQThCO0FBQzdCLE1BQU1DLFNBQVNDLFNBQVNSLEVBQUVHLFFBQVFELHVCQUFWLEVBQW1DTyxHQUFuQyxDQUF1QyxTQUF2QyxDQUFULENBQWY7QUFDQUosYUFBV0ssSUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxJQUF6QixDQUE4QmQsS0FBOUIsRUFBcUNRLE1BQXJDLENBQVg7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTTyxlQUFULEdBQTJCO0FBQzFCLE1BQUlULFFBQUosRUFBYztBQUNiSyxPQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJHLElBQXpCLENBQThCVixRQUE5QjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBUixRQUFPbUIsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QmxCLFFBQ0VtQixFQURGLENBQ0ssV0FETCxFQUNrQlosa0JBRGxCLEVBRUVZLEVBRkYsQ0FFSyxRQUZMLEVBRWVKLGVBRmY7O0FBSUFkLElBQUVtQixNQUFGLEVBQVVELEVBQVYsQ0FBYSx3QkFBYixFQUF1QyxZQUFNO0FBQzVDWjs7QUFFQTtBQUNBLE9BQUlQLE1BQU1xQixTQUFOLEdBQWtCQyxJQUFsQixDQUF1QkMsSUFBdkIsT0FBa0NDLFNBQXRDLEVBQWlEO0FBQ2hEVDtBQUNBO0FBQ0QsR0FQRDs7QUFTQUc7QUFDQSxFQWZEOztBQWlCQSxRQUFPcEIsTUFBUDtBQUVBLENBN0ZEIiwiZmlsZSI6ImRhdGF0YWJsZV9sb2FkaW5nX3NwaW5uZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gZGF0YXRhYmxlX2xvYWRpbmdfc3Bpbm5lci5qcyAyMDE3LTAzLTE2XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqICMjIEVuYWJsZSBEYXRhVGFibGUgTG9hZGluZyBTcGlubmVyXHJcbiAqXHJcbiAqIFRoZSBsb2FkaW5nIHNwaW5uZXIgd2lsbCBiZSB2aXNpYmxlIGR1cmluZyBldmVyeSBEYXRhVGFibGUgQUpBWCByZXF1ZXN0LlxyXG4gKiBcclxuICogIyMjIE9wdGlvbnMgXHJcbiAqIFxyXG4gKiAqKiBaLUluZGV4IFJlZmVyZW5jZSBTZWxlY3RvciB8IGBkYXRhLWRhdGF0YWJsZV9sb2FkaW5nX3NwaW5uZXItei1pbmRleC1yZWZlcmVuY2Utc2VsZWN0b3JgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxyXG4gKiBQcm92aWRlIGEgcmVmZXJlbmNlIHNlbGVjdG9yIHRoYXQgd2lsbCBiZSB1c2VkIGFzIGEgei1pbmRleCByZWZlcmVuY2UuIERlZmF1bHRzIHRvIFwiLnRhYmxlLWZpeGVkLWhlYWRlciB0aGVhZC5maXhlZFwiLlxyXG4gKlxyXG4gKiBAbW9kdWxlIEFkbWluL0V4dGVuc2lvbnMvZGF0YXRhYmxlX2xvYWRpbmdfc3Bpbm5lclxyXG4gKi9cclxuZ3guZXh0ZW5zaW9ucy5tb2R1bGUoJ2RhdGF0YWJsZV9sb2FkaW5nX3NwaW5uZXInLCBbJ2xvYWRpbmdfc3Bpbm5lciddLCBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIFZBUklBQkxFU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBTZWxlY3RvclxyXG5cdCAqXHJcblx0ICogQHR5cGUge2pRdWVyeX1cclxuXHQgKi9cclxuXHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogRGVmYXVsdCBPcHRpb25zIFxyXG5cdCAqIFxyXG5cdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0ICovXHJcblx0Y29uc3QgZGVmYXVsdHMgPSB7XHJcblx0XHR6SW5kZXhSZWZlcmVuY2VTZWxlY3RvcjogJy50YWJsZS1maXhlZC1oZWFkZXIgdGhlYWQuZml4ZWQnXHRcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEZpbmFsIE9wdGlvbnNcclxuXHQgKiBcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdCAqXHJcblx0ICogQHR5cGUge09iamVjdH1cclxuXHQgKi9cclxuXHRjb25zdCBtb2R1bGUgPSB7fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBMb2FkaW5nIFNwaW5uZXIgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0bGV0ICRzcGlubmVyO1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIEZVTkNUSU9OU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE9uIFByZSBEYXRhVGFibGUgWEhSIEV2ZW50XHJcblx0ICpcclxuXHQgKiBEaXNwbGF5IHRoZSBsb2FkaW5nIHNwaW5uZXIgb24gdGhlIHRhYmxlLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vbkRhdGFUYWJsZVByZVhocigpIHtcclxuXHRcdGNvbnN0IHpJbmRleCA9IHBhcnNlSW50KCQob3B0aW9ucy56SW5kZXhSZWZlcmVuY2VTZWxlY3RvcikuY3NzKCd6LWluZGV4JykpO1xyXG5cdFx0JHNwaW5uZXIgPSBqc2UubGlicy5sb2FkaW5nX3NwaW5uZXIuc2hvdygkdGhpcywgekluZGV4KTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogT24gWEhSIERhdGFUYWJsZSBFdmVudFxyXG5cdCAqXHJcblx0ICogSGlkZSB0aGUgZGlzcGxheWVkIGxvYWRpbmcgc3Bpbm5lci5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25EYXRhVGFibGVYaHIoKSB7XHJcblx0XHRpZiAoJHNwaW5uZXIpIHtcclxuXHRcdFx0anNlLmxpYnMubG9hZGluZ19zcGlubmVyLmhpZGUoJHNwaW5uZXIpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBJTklUSUFMSVpBVElPTlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xyXG5cdFx0JHRoaXNcclxuXHRcdFx0Lm9uKCdwcmVYaHIuZHQnLCBfb25EYXRhVGFibGVQcmVYaHIpXHJcblx0XHRcdC5vbigneGhyLmR0JywgX29uRGF0YVRhYmxlWGhyKTtcclxuXHRcdFxyXG5cdFx0JCh3aW5kb3cpLm9uKCdKU0VOR0lORV9JTklUX0ZJTklTSEVEJywgKCkgPT4ge1xyXG5cdFx0XHRfb25EYXRhVGFibGVQcmVYaHIoKTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEhpZGUgdGhlIHNwaW5uZXIgaWYgdGhlIHRhYmxlIGlzIGFscmVhZHkgbG9hZGVkLiAgXHJcblx0XHRcdGlmICgkdGhpcy5EYXRhVGFibGUoKS5hamF4Lmpzb24oKSAhPT0gdW5kZWZpbmVkKSB7XHJcblx0XHRcdFx0X29uRGF0YVRhYmxlWGhyKCk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHRkb25lKCk7XHJcblx0fTtcclxuXHRcclxuXHRyZXR1cm4gbW9kdWxlO1xyXG5cdFxyXG59KTsiXX0=
