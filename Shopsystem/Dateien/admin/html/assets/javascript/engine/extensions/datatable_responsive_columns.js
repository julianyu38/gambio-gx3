'use strict';

/* --------------------------------------------------------------
 datatable_responsive_columns.js 2017-03-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Enable DataTable Responsive Columns
 *
 * This module will enable the responsive columns functionality which will resize the columns until a minimum
 * width is reach. Afterwards the columns will be hidden and the content will be displayed by through an icon
 * tooltip.
 *
 * ### Options
 *
 * **Initial Visibility Toggle Selector | `data-data_relative_columns-visibility-toggle-selector` | String | Optional**
 *
 * Provide a selector relative to each thead > tr element in order to hide the column on page load and then show it
 * again once the responsive widths have been calculated. The provided selector must point to the biggest column in
 * order to avoid broken displays till the table becomes responsive.
 *
 * @module Admin/Extensions/data_relative_columns
 */
gx.extensions.module('datatable_responsive_columns', [jse.source + '/vendor/qtip2/jquery.qtip.min.css', jse.source + '/vendor/qtip2/jquery.qtip.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		visibilityToggleSelector: '[data-column-name="actions"]'
	};

	/**
  * Final Options
  *
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * DataTable Initialization Columns
  *
  * @type {Array}
  */
	var columnDefinitions = void 0;

	/**
  * Width Factor Sum
  *
  * @type {Number}
  */
	var widthFactorSum = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Update empty table "colspan" attribute.
  *
  * This method will keep the empty table row width in sync with the table width.
  */
	function _updateEmptyTableColSpan() {
		if ($this.find('.dataTables_empty').length > 0) {
			var colspan = $this.find('thead:first tr:first .actions').index() + 1 - $this.find('thead:first tr:first th.hidden').length;
			$this.find('.dataTables_empty').attr('colspan', colspan);
		}
	}

	/**
  * Add hidden columns content icon to actions cell of a single row.
  *
  * Call this method only if you are sure there is no icon previously set (runs faster).
  *
  * @param {jQuery} $tr
  */
	function _addHiddenColumnsContentIcon($tr) {
		$tr.find('td.actions div:first').prepend('<i class="fa fa-ellipsis-h meta-icon hidden-columns-content"></i>');
	}

	/**
  * Get the cell content.
  *
  * This method will also search for child input and select elements and return the appropriate content.
  *
  * @param {jQuery} $td Table cell to be examined.
  *
  * @return {String}
  */
	function _getCellContent($td) {
		if ($td.find('select').length) {
			return $td.find('select option:selected').text();
		} else if ($td.find('input:text').length) {
			return $td.find('input:text').val();
		} else if ($td.find('input:checkbox').length) {
			return $td.find('input:checkbox').prop('checked') ? '✔' : '✘';
		} else {
			return $td.text();
		}
	}

	/**
  * Generates and sets the tooltip content for the hidden columns content.
  *
  * @param {jQuery} $tr The current row selector.
  */
	function _generateHiddenColumnsContent($tr) {
		var hiddenColumnContentHtml = '';

		$tr.find('td.hidden').each(function (index, td) {
			hiddenColumnContentHtml += $this.find('thead:first tr:first th:eq(' + $(td).index() + ')').text() + ': ' + _getCellContent($(td)) + '<br/>';
		});

		$tr.find('.hidden-columns-content').qtip({
			content: hiddenColumnContentHtml,
			style: {
				classes: 'gx-qtip info'
			},
			hide: {
				fixed: true,
				delay: 300
			}
		});
	}

	/**
  * Hide DataTable Columns
  *
  * This method is part of the responsive tables solution.
  *
  * @param {jQuery} $targetWrapper Target datatable instance wrapper div.
  * @param {jQuery} $firstHiddenColumn The first hidden column (first column with the .hidden class).
  */
	function _hideColumns($targetWrapper, $firstHiddenColumn) {
		var $lastVisibleColumn = $firstHiddenColumn.length !== 0 ? $firstHiddenColumn.prev() : $this.find('thead:first th.actions').prev();

		if ($lastVisibleColumn.hasClass('hidden') || $lastVisibleColumn.index() === 0) {
			return; // First column or already hidden, do not continue.
		}

		// Show hidden column content icon.
		if ($this.find('.hidden-columns-content').length === 0) {
			$this.find('tbody tr').each(function (index, tr) {
				_addHiddenColumnsContentIcon($(tr));
			});
		}

		// Hide the last visible column.
		$this.find('tr').each(function (index, tr) {
			$(tr).find('th:eq(' + $lastVisibleColumn.index() + '), td:eq(' + $lastVisibleColumn.index() + ')').addClass('hidden');

			// Generate the hidden columns content.
			_generateHiddenColumnsContent($(tr));
		});

		_updateEmptyTableColSpan();

		// If there are still columns which don't fit within the viewport, hide them.
		if ($targetWrapper.width() < $this.width() && $lastVisibleColumn.index() > 1) {
			_toggleColumnsVisibility();
		}
	}

	/**
  * Show DataTable Columns
  *
  * This method is part of the responsive tables solution.
  *
  * @param {jQuery} $targetWrapper Target datatable instance wrapper div.
  * @param {jQuery} $firstHiddenColumn The first hidden column (first column with the .hidden class).
  */
	function _showColumns($targetWrapper, $firstHiddenColumn) {
		if ($firstHiddenColumn.length === 0) {
			return;
		}

		var firstHiddenColumnWidth = parseInt($firstHiddenColumn.css('min-width'));
		var tableMinWidth = 0;

		// Calculate the table min width by each column min width.
		$this.find('thead:first tr:first th').each(function (index, th) {
			if (!$(th).hasClass('hidden')) {
				tableMinWidth += parseInt($(th).css('min-width'));
			}
		});

		// Show the first hidden column.
		if (tableMinWidth + firstHiddenColumnWidth <= $targetWrapper.outerWidth()) {
			$this.find('tr').each(function (index, tr) {
				$(tr).find('th:eq(' + $firstHiddenColumn.index() + '), td:eq(' + $firstHiddenColumn.index() + ')').removeClass('hidden');

				_generateHiddenColumnsContent($(tr));
			});

			_updateEmptyTableColSpan();

			// Hide hidden column content icon.
			if ($this.find('thead:first tr:first th.hidden').length === 0) {
				$this.find('.hidden-columns-content').remove();
			}

			// If there are still columns which would fit fit within the viewport, show them.
			var newTableMinWidth = tableMinWidth + firstHiddenColumnWidth + parseInt($firstHiddenColumn.next('.hidden').css('min-width'));

			if (newTableMinWidth <= $targetWrapper.outerWidth() && $firstHiddenColumn.next('.hidden').length !== 0) {
				_toggleColumnsVisibility();
			}
		}
	}

	/**
  * Toggle column visibility depending the window size.
  */
	function _toggleColumnsVisibility() {
		var $targetWrapper = $this.parent();
		var $firstHiddenColumn = $this.find('thead:first th.hidden:first');

		if ($targetWrapper.width() < $this.width()) {
			_hideColumns($targetWrapper, $firstHiddenColumn);
		} else {
			_showColumns($targetWrapper, $firstHiddenColumn);
		}
	}

	/**
  * Calculate and set the relative column widths.
  *
  * The relative width calculation works with a width-factor system where each column preserves a
  * specific amount of the table width.
  *
  * This factor is not defining a percentage, rather only a width-volume. Percentage widths will not
  * work correctly when the table has fewer columns than the original settings.
  */
	function _applyRelativeColumnWidths() {
		$this.find('thead:first tr:first th').each(function () {
			var _this = this;

			if ($(this).css('display') === 'none') {
				return true;
			}

			var currentColumnDefinition = void 0;

			columnDefinitions.forEach(function (columnDefinition) {
				if (columnDefinition.name === $(_this).data('columnName')) {
					currentColumnDefinition = columnDefinition;
				}
			});

			if (currentColumnDefinition && currentColumnDefinition.widthFactor) {
				var index = $(this).index();
				var width = Math.round(currentColumnDefinition.widthFactor / widthFactorSum * 100 * 100) / 100;
				$this.find('thead').each(function (i, thead) {
					$(thead).find('tr').each(function (i, tr) {
						$(tr).find('th').eq(index).css('width', width + '%');
					});
				});
			}
		});
	}

	/**
  * Applies the column width if the current column width is smaller.
  */
	function _applyMinimumColumnWidths() {
		$this.find('thead:first tr:first th').each(function (index) {
			var _this2 = this;

			if ($(this).css('display') === 'none') {
				return true;
			}

			var currentColumnDefinition = void 0;

			columnDefinitions.forEach(function (columnDefinition) {
				if (columnDefinition.name === $(_this2).data('columnName')) {
					currentColumnDefinition = columnDefinition;
				}
			});

			if (!currentColumnDefinition) {
				return true;
			}

			var currentWidth = $(this).outerWidth();
			var definitionMinWidth = parseInt(currentColumnDefinition.minWidth);

			if (currentWidth < definitionMinWidth) {
				// Force the correct column min-widths for all thead columns.
				$this.find('thead').each(function (i, thead) {
					$(thead).find('tr').each(function (i, tr) {
						$(tr).find('th').eq(index).outerWidth(definitionMinWidth).css('max-width', definitionMinWidth).css('min-width', definitionMinWidth);
					});
				});
			}
		});
	}

	/**
  * On DataTable Draw Event
  */
	function _onDataTableDraw() {
		// Wait until the contents of the table are rendered. DataTables will sometimes fire the draw event
		// even before the td elements are rendered in the browser.
		var interval = setInterval(function () {
			if ($this.find('tbody tr:last td.actions').length === 1) {
				_applyRelativeColumnWidths();
				_applyMinimumColumnWidths();
				_toggleColumnsVisibility();

				// Hide the tbody cells depending on whether the respective <th> element is hidden.
				$this.find('thead:first tr:first th').each(function (index, th) {
					if ($(th).hasClass('hidden')) {
						$this.find('tbody tr').each(function (i, tr) {
							$(tr).find('td:eq(' + index + ')').addClass('hidden');
						});
					}
				});

				// Add the hidden columns icon if needed.
				if ($this.find('thead th.hidden').length) {
					$this.find('tbody tr').each(function (index, tr) {
						if ($(tr).find('.hidden-columns-content').length) {
							return true;
						}
						_addHiddenColumnsContentIcon($(tr));
						_generateHiddenColumnsContent($(tr));
					});
				}

				clearInterval(interval);
			}
		}, 500);
	}

	/**
  * On Window Resize Event
  */
	function _onWindowResize() {
		$this.find('thead.fixed').outerWidth($this.outerWidth());
		_applyRelativeColumnWidths();
		_applyMinimumColumnWidths();
		_toggleColumnsVisibility();
	}

	/**
  * On DataTable Initialize Event 
  */
	function _onDataTableInit() {
		$this.find(options.visibilityToggleSelector).show();
		_updateEmptyTableColSpan();

		columnDefinitions = $this.DataTable().init().columns;
		widthFactorSum = 0;

		columnDefinitions.forEach(function (columnDefinition) {
			widthFactorSum += columnDefinition.widthFactor || 0;
		});

		$this.on('draw.dt', _onDataTableDraw);
		$(window).on('resize', _onWindowResize);

		_onWindowResize();
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.on('init.dt', _onDataTableInit);

		$(window).on('JSENGINE_INIT_FINISHED', function () {
			if ($this.DataTable().ajax.json() !== undefined) {
				_onDataTableInit();
			}
		});

		$this.find(options.visibilityToggleSelector).hide();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGF0YWJsZV9yZXNwb25zaXZlX2NvbHVtbnMuanMiXSwibmFtZXMiOlsiZ3giLCJleHRlbnNpb25zIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwidmlzaWJpbGl0eVRvZ2dsZVNlbGVjdG9yIiwib3B0aW9ucyIsImV4dGVuZCIsImNvbHVtbkRlZmluaXRpb25zIiwid2lkdGhGYWN0b3JTdW0iLCJfdXBkYXRlRW1wdHlUYWJsZUNvbFNwYW4iLCJmaW5kIiwibGVuZ3RoIiwiY29sc3BhbiIsImluZGV4IiwiYXR0ciIsIl9hZGRIaWRkZW5Db2x1bW5zQ29udGVudEljb24iLCIkdHIiLCJwcmVwZW5kIiwiX2dldENlbGxDb250ZW50IiwiJHRkIiwidGV4dCIsInZhbCIsInByb3AiLCJfZ2VuZXJhdGVIaWRkZW5Db2x1bW5zQ29udGVudCIsImhpZGRlbkNvbHVtbkNvbnRlbnRIdG1sIiwiZWFjaCIsInRkIiwicXRpcCIsImNvbnRlbnQiLCJzdHlsZSIsImNsYXNzZXMiLCJoaWRlIiwiZml4ZWQiLCJkZWxheSIsIl9oaWRlQ29sdW1ucyIsIiR0YXJnZXRXcmFwcGVyIiwiJGZpcnN0SGlkZGVuQ29sdW1uIiwiJGxhc3RWaXNpYmxlQ29sdW1uIiwicHJldiIsImhhc0NsYXNzIiwidHIiLCJhZGRDbGFzcyIsIndpZHRoIiwiX3RvZ2dsZUNvbHVtbnNWaXNpYmlsaXR5IiwiX3Nob3dDb2x1bW5zIiwiZmlyc3RIaWRkZW5Db2x1bW5XaWR0aCIsInBhcnNlSW50IiwiY3NzIiwidGFibGVNaW5XaWR0aCIsInRoIiwib3V0ZXJXaWR0aCIsInJlbW92ZUNsYXNzIiwicmVtb3ZlIiwibmV3VGFibGVNaW5XaWR0aCIsIm5leHQiLCJwYXJlbnQiLCJfYXBwbHlSZWxhdGl2ZUNvbHVtbldpZHRocyIsImN1cnJlbnRDb2x1bW5EZWZpbml0aW9uIiwiZm9yRWFjaCIsImNvbHVtbkRlZmluaXRpb24iLCJuYW1lIiwid2lkdGhGYWN0b3IiLCJNYXRoIiwicm91bmQiLCJpIiwidGhlYWQiLCJlcSIsIl9hcHBseU1pbmltdW1Db2x1bW5XaWR0aHMiLCJjdXJyZW50V2lkdGgiLCJkZWZpbml0aW9uTWluV2lkdGgiLCJtaW5XaWR0aCIsIl9vbkRhdGFUYWJsZURyYXciLCJpbnRlcnZhbCIsInNldEludGVydmFsIiwiY2xlYXJJbnRlcnZhbCIsIl9vbldpbmRvd1Jlc2l6ZSIsIl9vbkRhdGFUYWJsZUluaXQiLCJzaG93IiwiRGF0YVRhYmxlIiwiaW5pdCIsImNvbHVtbnMiLCJvbiIsIndpbmRvdyIsImRvbmUiLCJhamF4IiwianNvbiIsInVuZGVmaW5lZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQUEsR0FBR0MsVUFBSCxDQUFjQyxNQUFkLENBQ0MsOEJBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLHdDQUVJRCxJQUFJQyxNQUZSLHNDQUhELEVBUUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsV0FBVztBQUNoQkMsNEJBQTBCO0FBRFYsRUFBakI7O0FBSUE7Ozs7O0FBS0EsS0FBTUMsVUFBVUgsRUFBRUksTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CSCxRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUgsU0FBUyxFQUFmOztBQUVBOzs7OztBQUtBLEtBQUlVLDBCQUFKOztBQUVBOzs7OztBQUtBLEtBQUlDLHVCQUFKOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxVQUFTQyx3QkFBVCxHQUFvQztBQUNuQyxNQUFJUixNQUFNUyxJQUFOLENBQVcsbUJBQVgsRUFBZ0NDLE1BQWhDLEdBQXlDLENBQTdDLEVBQWdEO0FBQy9DLE9BQU1DLFVBQVdYLE1BQU1TLElBQU4sQ0FBVywrQkFBWCxFQUE0Q0csS0FBNUMsS0FBc0QsQ0FBdkQsR0FDYlosTUFBTVMsSUFBTixDQUFXLGdDQUFYLEVBQTZDQyxNQURoRDtBQUVBVixTQUFNUyxJQUFOLENBQVcsbUJBQVgsRUFBZ0NJLElBQWhDLENBQXFDLFNBQXJDLEVBQWdERixPQUFoRDtBQUNBO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTRyw0QkFBVCxDQUFzQ0MsR0FBdEMsRUFBMkM7QUFDMUNBLE1BQUlOLElBQUosQ0FBUyxzQkFBVCxFQUNFTyxPQURGO0FBRUE7O0FBRUQ7Ozs7Ozs7OztBQVNBLFVBQVNDLGVBQVQsQ0FBeUJDLEdBQXpCLEVBQThCO0FBQzdCLE1BQUlBLElBQUlULElBQUosQ0FBUyxRQUFULEVBQW1CQyxNQUF2QixFQUErQjtBQUM5QixVQUFPUSxJQUFJVCxJQUFKLENBQVMsd0JBQVQsRUFBbUNVLElBQW5DLEVBQVA7QUFDQSxHQUZELE1BRU8sSUFBSUQsSUFBSVQsSUFBSixDQUFTLFlBQVQsRUFBdUJDLE1BQTNCLEVBQW1DO0FBQ3pDLFVBQU9RLElBQUlULElBQUosQ0FBUyxZQUFULEVBQXVCVyxHQUF2QixFQUFQO0FBQ0EsR0FGTSxNQUVBLElBQUlGLElBQUlULElBQUosQ0FBUyxnQkFBVCxFQUEyQkMsTUFBL0IsRUFBdUM7QUFDN0MsVUFBT1EsSUFBSVQsSUFBSixDQUFTLGdCQUFULEVBQTJCWSxJQUEzQixDQUFnQyxTQUFoQyxJQUE2QyxHQUE3QyxHQUFtRCxHQUExRDtBQUNBLEdBRk0sTUFFQTtBQUNOLFVBQU9ILElBQUlDLElBQUosRUFBUDtBQUNBO0FBQ0Q7O0FBRUQ7Ozs7O0FBS0EsVUFBU0csNkJBQVQsQ0FBdUNQLEdBQXZDLEVBQTRDO0FBQzNDLE1BQUlRLDBCQUEwQixFQUE5Qjs7QUFFQVIsTUFBSU4sSUFBSixDQUFTLFdBQVQsRUFBc0JlLElBQXRCLENBQTJCLFVBQUNaLEtBQUQsRUFBUWEsRUFBUixFQUFlO0FBQ3pDRiw4QkFBMkJ2QixNQUFNUyxJQUFOLGlDQUF5Q1IsRUFBRXdCLEVBQUYsRUFBTWIsS0FBTixFQUF6QyxRQUEyRE8sSUFBM0QsS0FDeEIsSUFEd0IsR0FDakJGLGdCQUFnQmhCLEVBQUV3QixFQUFGLENBQWhCLENBRGlCLEdBQ1EsT0FEbkM7QUFFQSxHQUhEOztBQUtBVixNQUFJTixJQUFKLENBQVMseUJBQVQsRUFBb0NpQixJQUFwQyxDQUF5QztBQUN4Q0MsWUFBU0osdUJBRCtCO0FBRXhDSyxVQUFPO0FBQ05DLGFBQVM7QUFESCxJQUZpQztBQUt4Q0MsU0FBTTtBQUNMQyxXQUFPLElBREY7QUFFTEMsV0FBTztBQUZGO0FBTGtDLEdBQXpDO0FBVUE7O0FBRUQ7Ozs7Ozs7O0FBUUEsVUFBU0MsWUFBVCxDQUFzQkMsY0FBdEIsRUFBc0NDLGtCQUF0QyxFQUEwRDtBQUN6RCxNQUFNQyxxQkFBc0JELG1CQUFtQnpCLE1BQW5CLEtBQThCLENBQS9CLEdBQ3hCeUIsbUJBQW1CRSxJQUFuQixFQUR3QixHQUV4QnJDLE1BQU1TLElBQU4sQ0FBVyx3QkFBWCxFQUFxQzRCLElBQXJDLEVBRkg7O0FBSUEsTUFBSUQsbUJBQW1CRSxRQUFuQixDQUE0QixRQUE1QixLQUF5Q0YsbUJBQW1CeEIsS0FBbkIsT0FBK0IsQ0FBNUUsRUFBK0U7QUFDOUUsVUFEOEUsQ0FDdEU7QUFDUjs7QUFFRDtBQUNBLE1BQUlaLE1BQU1TLElBQU4sQ0FBVyx5QkFBWCxFQUFzQ0MsTUFBdEMsS0FBaUQsQ0FBckQsRUFBd0Q7QUFDdkRWLFNBQU1TLElBQU4sQ0FBVyxVQUFYLEVBQXVCZSxJQUF2QixDQUE0QixVQUFDWixLQUFELEVBQVEyQixFQUFSLEVBQWU7QUFDMUN6QixpQ0FBNkJiLEVBQUVzQyxFQUFGLENBQTdCO0FBQ0EsSUFGRDtBQUdBOztBQUVEO0FBQ0F2QyxRQUFNUyxJQUFOLENBQVcsSUFBWCxFQUFpQmUsSUFBakIsQ0FBc0IsVUFBQ1osS0FBRCxFQUFRMkIsRUFBUixFQUFlO0FBQ3BDdEMsS0FBRXNDLEVBQUYsRUFDRTlCLElBREYsWUFDZ0IyQixtQkFBbUJ4QixLQUFuQixFQURoQixpQkFDc0R3QixtQkFBbUJ4QixLQUFuQixFQUR0RCxRQUVFNEIsUUFGRixDQUVXLFFBRlg7O0FBSUE7QUFDQWxCLGlDQUE4QnJCLEVBQUVzQyxFQUFGLENBQTlCO0FBQ0EsR0FQRDs7QUFTQS9COztBQUVBO0FBQ0EsTUFBSTBCLGVBQWVPLEtBQWYsS0FBeUJ6QyxNQUFNeUMsS0FBTixFQUF6QixJQUEwQ0wsbUJBQW1CeEIsS0FBbkIsS0FBNkIsQ0FBM0UsRUFBOEU7QUFDN0U4QjtBQUNBO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsVUFBU0MsWUFBVCxDQUFzQlQsY0FBdEIsRUFBc0NDLGtCQUF0QyxFQUEwRDtBQUN6RCxNQUFJQSxtQkFBbUJ6QixNQUFuQixLQUE4QixDQUFsQyxFQUFxQztBQUNwQztBQUNBOztBQUVELE1BQU1rQyx5QkFBeUJDLFNBQVNWLG1CQUFtQlcsR0FBbkIsQ0FBdUIsV0FBdkIsQ0FBVCxDQUEvQjtBQUNBLE1BQUlDLGdCQUFnQixDQUFwQjs7QUFFQTtBQUNBL0MsUUFBTVMsSUFBTixDQUFXLHlCQUFYLEVBQXNDZSxJQUF0QyxDQUEyQyxVQUFDWixLQUFELEVBQVFvQyxFQUFSLEVBQWU7QUFDekQsT0FBSSxDQUFDL0MsRUFBRStDLEVBQUYsRUFBTVYsUUFBTixDQUFlLFFBQWYsQ0FBTCxFQUErQjtBQUM5QlMscUJBQWlCRixTQUFTNUMsRUFBRStDLEVBQUYsRUFBTUYsR0FBTixDQUFVLFdBQVYsQ0FBVCxDQUFqQjtBQUNBO0FBQ0QsR0FKRDs7QUFNQTtBQUNBLE1BQUlDLGdCQUFnQkgsc0JBQWhCLElBQTBDVixlQUFlZSxVQUFmLEVBQTlDLEVBQTJFO0FBQzFFakQsU0FBTVMsSUFBTixDQUFXLElBQVgsRUFBaUJlLElBQWpCLENBQXNCLFVBQUNaLEtBQUQsRUFBUTJCLEVBQVIsRUFBZTtBQUNwQ3RDLE1BQUVzQyxFQUFGLEVBQ0U5QixJQURGLFlBQ2dCMEIsbUJBQW1CdkIsS0FBbkIsRUFEaEIsaUJBQ3NEdUIsbUJBQW1CdkIsS0FBbkIsRUFEdEQsUUFFRXNDLFdBRkYsQ0FFYyxRQUZkOztBQUlBNUIsa0NBQThCckIsRUFBRXNDLEVBQUYsQ0FBOUI7QUFDQSxJQU5EOztBQVFBL0I7O0FBRUE7QUFDQSxPQUFJUixNQUFNUyxJQUFOLENBQVcsZ0NBQVgsRUFBNkNDLE1BQTdDLEtBQXdELENBQTVELEVBQStEO0FBQzlEVixVQUFNUyxJQUFOLENBQVcseUJBQVgsRUFBc0MwQyxNQUF0QztBQUNBOztBQUVEO0FBQ0EsT0FBTUMsbUJBQW1CTCxnQkFBZ0JILHNCQUFoQixHQUN0QkMsU0FBU1YsbUJBQW1Ca0IsSUFBbkIsQ0FBd0IsU0FBeEIsRUFBbUNQLEdBQW5DLENBQXVDLFdBQXZDLENBQVQsQ0FESDs7QUFHQSxPQUFJTSxvQkFBb0JsQixlQUFlZSxVQUFmLEVBQXBCLElBQW1EZCxtQkFBbUJrQixJQUFuQixDQUF3QixTQUF4QixFQUFtQzNDLE1BQW5DLEtBQ2xELENBREwsRUFDUTtBQUNQZ0M7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQ7OztBQUdBLFVBQVNBLHdCQUFULEdBQW9DO0FBQ25DLE1BQU1SLGlCQUFpQmxDLE1BQU1zRCxNQUFOLEVBQXZCO0FBQ0EsTUFBTW5CLHFCQUFxQm5DLE1BQU1TLElBQU4sQ0FBVyw2QkFBWCxDQUEzQjs7QUFFQSxNQUFJeUIsZUFBZU8sS0FBZixLQUF5QnpDLE1BQU15QyxLQUFOLEVBQTdCLEVBQTRDO0FBQzNDUixnQkFBYUMsY0FBYixFQUE2QkMsa0JBQTdCO0FBQ0EsR0FGRCxNQUVPO0FBQ05RLGdCQUFhVCxjQUFiLEVBQTZCQyxrQkFBN0I7QUFDQTtBQUNEOztBQUVEOzs7Ozs7Ozs7QUFTQSxVQUFTb0IsMEJBQVQsR0FBc0M7QUFDckN2RCxRQUFNUyxJQUFOLENBQVcseUJBQVgsRUFBc0NlLElBQXRDLENBQTJDLFlBQVc7QUFBQTs7QUFDckQsT0FBSXZCLEVBQUUsSUFBRixFQUFRNkMsR0FBUixDQUFZLFNBQVosTUFBMkIsTUFBL0IsRUFBdUM7QUFDdEMsV0FBTyxJQUFQO0FBQ0E7O0FBRUQsT0FBSVUsZ0NBQUo7O0FBRUFsRCxxQkFBa0JtRCxPQUFsQixDQUEwQixVQUFDQyxnQkFBRCxFQUFzQjtBQUMvQyxRQUFJQSxpQkFBaUJDLElBQWpCLEtBQTBCMUQsRUFBRSxLQUFGLEVBQVFGLElBQVIsQ0FBYSxZQUFiLENBQTlCLEVBQTBEO0FBQ3pEeUQsK0JBQTBCRSxnQkFBMUI7QUFDQTtBQUNELElBSkQ7O0FBTUEsT0FBSUYsMkJBQTJCQSx3QkFBd0JJLFdBQXZELEVBQW9FO0FBQ25FLFFBQU1oRCxRQUFRWCxFQUFFLElBQUYsRUFBUVcsS0FBUixFQUFkO0FBQ0EsUUFBTTZCLFFBQVFvQixLQUFLQyxLQUFMLENBQVdOLHdCQUF3QkksV0FBeEIsR0FBc0NyRCxjQUF0QyxHQUF1RCxHQUF2RCxHQUE2RCxHQUF4RSxJQUErRSxHQUE3RjtBQUNBUCxVQUFNUyxJQUFOLENBQVcsT0FBWCxFQUFvQmUsSUFBcEIsQ0FBeUIsVUFBQ3VDLENBQUQsRUFBSUMsS0FBSixFQUFjO0FBQ3RDL0QsT0FBRStELEtBQUYsRUFBU3ZELElBQVQsQ0FBYyxJQUFkLEVBQW9CZSxJQUFwQixDQUF5QixVQUFDdUMsQ0FBRCxFQUFJeEIsRUFBSixFQUFXO0FBQ25DdEMsUUFBRXNDLEVBQUYsRUFBTTlCLElBQU4sQ0FBVyxJQUFYLEVBQWlCd0QsRUFBakIsQ0FBb0JyRCxLQUFwQixFQUEyQmtDLEdBQTNCLENBQStCLE9BQS9CLEVBQXdDTCxRQUFRLEdBQWhEO0FBQ0EsTUFGRDtBQUdBLEtBSkQ7QUFLQTtBQUNELEdBdEJEO0FBdUJBOztBQUVEOzs7QUFHQSxVQUFTeUIseUJBQVQsR0FBcUM7QUFDcENsRSxRQUFNUyxJQUFOLENBQVcseUJBQVgsRUFBc0NlLElBQXRDLENBQTJDLFVBQVNaLEtBQVQsRUFBZ0I7QUFBQTs7QUFDMUQsT0FBSVgsRUFBRSxJQUFGLEVBQVE2QyxHQUFSLENBQVksU0FBWixNQUEyQixNQUEvQixFQUF1QztBQUN0QyxXQUFPLElBQVA7QUFDQTs7QUFFRCxPQUFJVSxnQ0FBSjs7QUFFQWxELHFCQUFrQm1ELE9BQWxCLENBQTBCLFVBQUNDLGdCQUFELEVBQXNCO0FBQy9DLFFBQUlBLGlCQUFpQkMsSUFBakIsS0FBMEIxRCxFQUFFLE1BQUYsRUFBUUYsSUFBUixDQUFhLFlBQWIsQ0FBOUIsRUFBMEQ7QUFDekR5RCwrQkFBMEJFLGdCQUExQjtBQUNBO0FBQ0QsSUFKRDs7QUFNQSxPQUFJLENBQUNGLHVCQUFMLEVBQThCO0FBQzdCLFdBQU8sSUFBUDtBQUNBOztBQUVELE9BQU1XLGVBQWVsRSxFQUFFLElBQUYsRUFBUWdELFVBQVIsRUFBckI7QUFDQSxPQUFNbUIscUJBQXFCdkIsU0FBU1csd0JBQXdCYSxRQUFqQyxDQUEzQjs7QUFFQSxPQUFJRixlQUFlQyxrQkFBbkIsRUFBdUM7QUFDdEM7QUFDQXBFLFVBQU1TLElBQU4sQ0FBVyxPQUFYLEVBQW9CZSxJQUFwQixDQUF5QixVQUFDdUMsQ0FBRCxFQUFJQyxLQUFKLEVBQWM7QUFDdEMvRCxPQUFFK0QsS0FBRixFQUFTdkQsSUFBVCxDQUFjLElBQWQsRUFBb0JlLElBQXBCLENBQXlCLFVBQUN1QyxDQUFELEVBQUl4QixFQUFKLEVBQVc7QUFDbkN0QyxRQUFFc0MsRUFBRixFQUFNOUIsSUFBTixDQUFXLElBQVgsRUFBaUJ3RCxFQUFqQixDQUFvQnJELEtBQXBCLEVBQ0VxQyxVQURGLENBQ2FtQixrQkFEYixFQUVFdEIsR0FGRixDQUVNLFdBRk4sRUFFbUJzQixrQkFGbkIsRUFHRXRCLEdBSEYsQ0FHTSxXQUhOLEVBR21Cc0Isa0JBSG5CO0FBSUEsTUFMRDtBQU1BLEtBUEQ7QUFRQTtBQUNELEdBL0JEO0FBZ0NBOztBQUVEOzs7QUFHQSxVQUFTRSxnQkFBVCxHQUE0QjtBQUMzQjtBQUNBO0FBQ0EsTUFBSUMsV0FBV0MsWUFBWSxZQUFZO0FBQ3RDLE9BQUl4RSxNQUFNUyxJQUFOLENBQVcsMEJBQVgsRUFBdUNDLE1BQXZDLEtBQWtELENBQXRELEVBQXlEO0FBQ3hENkM7QUFDQVc7QUFDQXhCOztBQUVBO0FBQ0ExQyxVQUFNUyxJQUFOLENBQVcseUJBQVgsRUFBc0NlLElBQXRDLENBQTJDLFVBQVVaLEtBQVYsRUFBaUJvQyxFQUFqQixFQUFxQjtBQUMvRCxTQUFJL0MsRUFBRStDLEVBQUYsRUFBTVYsUUFBTixDQUFlLFFBQWYsQ0FBSixFQUE4QjtBQUM3QnRDLFlBQU1TLElBQU4sQ0FBVyxVQUFYLEVBQXVCZSxJQUF2QixDQUE0QixVQUFVdUMsQ0FBVixFQUFheEIsRUFBYixFQUFpQjtBQUM1Q3RDLFNBQUVzQyxFQUFGLEVBQU05QixJQUFOLENBQVcsV0FBV0csS0FBWCxHQUFtQixHQUE5QixFQUFtQzRCLFFBQW5DLENBQTRDLFFBQTVDO0FBQ0EsT0FGRDtBQUdBO0FBQ0QsS0FORDs7QUFRQTtBQUNBLFFBQUl4QyxNQUFNUyxJQUFOLENBQVcsaUJBQVgsRUFBOEJDLE1BQWxDLEVBQTBDO0FBQ3pDVixXQUFNUyxJQUFOLENBQVcsVUFBWCxFQUF1QmUsSUFBdkIsQ0FBNEIsVUFBVVosS0FBVixFQUFpQjJCLEVBQWpCLEVBQXFCO0FBQ2hELFVBQUl0QyxFQUFFc0MsRUFBRixFQUFNOUIsSUFBTixDQUFXLHlCQUFYLEVBQXNDQyxNQUExQyxFQUFrRDtBQUNqRCxjQUFPLElBQVA7QUFDQTtBQUNESSxtQ0FBNkJiLEVBQUVzQyxFQUFGLENBQTdCO0FBQ0FqQixvQ0FBOEJyQixFQUFFc0MsRUFBRixDQUE5QjtBQUNBLE1BTkQ7QUFPQTs7QUFFRGtDLGtCQUFjRixRQUFkO0FBQ0E7QUFDRCxHQTVCYyxFQTRCWixHQTVCWSxDQUFmO0FBNkJBOztBQUVEOzs7QUFHQSxVQUFTRyxlQUFULEdBQTJCO0FBQzFCMUUsUUFBTVMsSUFBTixDQUFXLGFBQVgsRUFBMEJ3QyxVQUExQixDQUFxQ2pELE1BQU1pRCxVQUFOLEVBQXJDO0FBQ0FNO0FBQ0FXO0FBQ0F4QjtBQUNBOztBQUVEOzs7QUFHQSxVQUFTaUMsZ0JBQVQsR0FBNEI7QUFDM0IzRSxRQUFNUyxJQUFOLENBQVdMLFFBQVFELHdCQUFuQixFQUE2Q3lFLElBQTdDO0FBQ0FwRTs7QUFFQUYsc0JBQW9CTixNQUFNNkUsU0FBTixHQUFrQkMsSUFBbEIsR0FBeUJDLE9BQTdDO0FBQ0F4RSxtQkFBaUIsQ0FBakI7O0FBRUFELG9CQUFrQm1ELE9BQWxCLENBQTBCLFVBQUNDLGdCQUFELEVBQXNCO0FBQy9DbkQscUJBQWtCbUQsaUJBQWlCRSxXQUFqQixJQUFnQyxDQUFsRDtBQUNBLEdBRkQ7O0FBSUE1RCxRQUFNZ0YsRUFBTixDQUFTLFNBQVQsRUFBb0JWLGdCQUFwQjtBQUNBckUsSUFBRWdGLE1BQUYsRUFBVUQsRUFBVixDQUFhLFFBQWIsRUFBdUJOLGVBQXZCOztBQUVBQTtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTlFLFFBQU9rRixJQUFQLEdBQWMsVUFBU0ksSUFBVCxFQUFlO0FBQzVCbEYsUUFBTWdGLEVBQU4sQ0FBUyxTQUFULEVBQW9CTCxnQkFBcEI7O0FBRUExRSxJQUFFZ0YsTUFBRixFQUFVRCxFQUFWLENBQWEsd0JBQWIsRUFBdUMsWUFBTTtBQUM1QyxPQUFJaEYsTUFBTTZFLFNBQU4sR0FBa0JNLElBQWxCLENBQXVCQyxJQUF2QixPQUFrQ0MsU0FBdEMsRUFBaUQ7QUFDaERWO0FBQ0E7QUFDRCxHQUpEOztBQU1BM0UsUUFBTVMsSUFBTixDQUFXTCxRQUFRRCx3QkFBbkIsRUFBNkMyQixJQUE3Qzs7QUFFQW9EO0FBQ0EsRUFaRDs7QUFjQSxRQUFPdEYsTUFBUDtBQUVBLENBbFpGIiwiZmlsZSI6ImRhdGF0YWJsZV9yZXNwb25zaXZlX2NvbHVtbnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gZGF0YXRhYmxlX3Jlc3BvbnNpdmVfY29sdW1ucy5qcyAyMDE3LTAzLTA4XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqICMjIEVuYWJsZSBEYXRhVGFibGUgUmVzcG9uc2l2ZSBDb2x1bW5zXHJcbiAqXHJcbiAqIFRoaXMgbW9kdWxlIHdpbGwgZW5hYmxlIHRoZSByZXNwb25zaXZlIGNvbHVtbnMgZnVuY3Rpb25hbGl0eSB3aGljaCB3aWxsIHJlc2l6ZSB0aGUgY29sdW1ucyB1bnRpbCBhIG1pbmltdW1cclxuICogd2lkdGggaXMgcmVhY2guIEFmdGVyd2FyZHMgdGhlIGNvbHVtbnMgd2lsbCBiZSBoaWRkZW4gYW5kIHRoZSBjb250ZW50IHdpbGwgYmUgZGlzcGxheWVkIGJ5IHRocm91Z2ggYW4gaWNvblxyXG4gKiB0b29sdGlwLlxyXG4gKlxyXG4gKiAjIyMgT3B0aW9uc1xyXG4gKlxyXG4gKiAqKkluaXRpYWwgVmlzaWJpbGl0eSBUb2dnbGUgU2VsZWN0b3IgfCBgZGF0YS1kYXRhX3JlbGF0aXZlX2NvbHVtbnMtdmlzaWJpbGl0eS10b2dnbGUtc2VsZWN0b3JgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxyXG4gKlxyXG4gKiBQcm92aWRlIGEgc2VsZWN0b3IgcmVsYXRpdmUgdG8gZWFjaCB0aGVhZCA+IHRyIGVsZW1lbnQgaW4gb3JkZXIgdG8gaGlkZSB0aGUgY29sdW1uIG9uIHBhZ2UgbG9hZCBhbmQgdGhlbiBzaG93IGl0XHJcbiAqIGFnYWluIG9uY2UgdGhlIHJlc3BvbnNpdmUgd2lkdGhzIGhhdmUgYmVlbiBjYWxjdWxhdGVkLiBUaGUgcHJvdmlkZWQgc2VsZWN0b3IgbXVzdCBwb2ludCB0byB0aGUgYmlnZ2VzdCBjb2x1bW4gaW5cclxuICogb3JkZXIgdG8gYXZvaWQgYnJva2VuIGRpc3BsYXlzIHRpbGwgdGhlIHRhYmxlIGJlY29tZXMgcmVzcG9uc2l2ZS5cclxuICpcclxuICogQG1vZHVsZSBBZG1pbi9FeHRlbnNpb25zL2RhdGFfcmVsYXRpdmVfY29sdW1uc1xyXG4gKi9cclxuZ3guZXh0ZW5zaW9ucy5tb2R1bGUoXHJcblx0J2RhdGF0YWJsZV9yZXNwb25zaXZlX2NvbHVtbnMnLFxyXG5cdFxyXG5cdFtcclxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9xdGlwMi9qcXVlcnkucXRpcC5taW4uY3NzYCxcclxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9xdGlwMi9qcXVlcnkucXRpcC5taW4uanNgLFxyXG5cdF0sXHJcblx0XHJcblx0ZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0XHJcblx0XHQndXNlIHN0cmljdCc7XHJcblx0XHRcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0Ly8gVkFSSUFCTEVTXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge09iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7XHJcblx0XHRcdHZpc2liaWxpdHlUb2dnbGVTZWxlY3RvcjogJ1tkYXRhLWNvbHVtbi1uYW1lPVwiYWN0aW9uc1wiXSdcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRmluYWwgT3B0aW9uc1xyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIERhdGFUYWJsZSBJbml0aWFsaXphdGlvbiBDb2x1bW5zXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge0FycmF5fVxyXG5cdFx0ICovXHJcblx0XHRsZXQgY29sdW1uRGVmaW5pdGlvbnM7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogV2lkdGggRmFjdG9yIFN1bVxyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtOdW1iZXJ9XHJcblx0XHQgKi9cclxuXHRcdGxldCB3aWR0aEZhY3RvclN1bTtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBGVU5DVElPTlNcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIFVwZGF0ZSBlbXB0eSB0YWJsZSBcImNvbHNwYW5cIiBhdHRyaWJ1dGUuXHJcblx0XHQgKlxyXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCBrZWVwIHRoZSBlbXB0eSB0YWJsZSByb3cgd2lkdGggaW4gc3luYyB3aXRoIHRoZSB0YWJsZSB3aWR0aC5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX3VwZGF0ZUVtcHR5VGFibGVDb2xTcGFuKCkge1xyXG5cdFx0XHRpZiAoJHRoaXMuZmluZCgnLmRhdGFUYWJsZXNfZW1wdHknKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0Y29uc3QgY29sc3BhbiA9ICgkdGhpcy5maW5kKCd0aGVhZDpmaXJzdCB0cjpmaXJzdCAuYWN0aW9ucycpLmluZGV4KCkgKyAxKVxyXG5cdFx0XHRcdFx0LSAkdGhpcy5maW5kKCd0aGVhZDpmaXJzdCB0cjpmaXJzdCB0aC5oaWRkZW4nKS5sZW5ndGg7XHJcblx0XHRcdFx0JHRoaXMuZmluZCgnLmRhdGFUYWJsZXNfZW1wdHknKS5hdHRyKCdjb2xzcGFuJywgY29sc3Bhbik7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBBZGQgaGlkZGVuIGNvbHVtbnMgY29udGVudCBpY29uIHRvIGFjdGlvbnMgY2VsbCBvZiBhIHNpbmdsZSByb3cuXHJcblx0XHQgKlxyXG5cdFx0ICogQ2FsbCB0aGlzIG1ldGhvZCBvbmx5IGlmIHlvdSBhcmUgc3VyZSB0aGVyZSBpcyBubyBpY29uIHByZXZpb3VzbHkgc2V0IChydW5zIGZhc3RlcikuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICR0clxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfYWRkSGlkZGVuQ29sdW1uc0NvbnRlbnRJY29uKCR0cikge1xyXG5cdFx0XHQkdHIuZmluZCgndGQuYWN0aW9ucyBkaXY6Zmlyc3QnKVxyXG5cdFx0XHRcdC5wcmVwZW5kKGA8aSBjbGFzcz1cImZhIGZhLWVsbGlwc2lzLWggbWV0YS1pY29uIGhpZGRlbi1jb2x1bW5zLWNvbnRlbnRcIj48L2k+YCk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogR2V0IHRoZSBjZWxsIGNvbnRlbnQuXHJcblx0XHQgKlxyXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCBhbHNvIHNlYXJjaCBmb3IgY2hpbGQgaW5wdXQgYW5kIHNlbGVjdCBlbGVtZW50cyBhbmQgcmV0dXJuIHRoZSBhcHByb3ByaWF0ZSBjb250ZW50LlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGQgVGFibGUgY2VsbCB0byBiZSBleGFtaW5lZC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcmV0dXJuIHtTdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9nZXRDZWxsQ29udGVudCgkdGQpIHtcclxuXHRcdFx0aWYgKCR0ZC5maW5kKCdzZWxlY3QnKS5sZW5ndGgpIHtcclxuXHRcdFx0XHRyZXR1cm4gJHRkLmZpbmQoJ3NlbGVjdCBvcHRpb246c2VsZWN0ZWQnKS50ZXh0KCk7XHJcblx0XHRcdH0gZWxzZSBpZiAoJHRkLmZpbmQoJ2lucHV0OnRleHQnKS5sZW5ndGgpIHtcclxuXHRcdFx0XHRyZXR1cm4gJHRkLmZpbmQoJ2lucHV0OnRleHQnKS52YWwoKTtcclxuXHRcdFx0fSBlbHNlIGlmICgkdGQuZmluZCgnaW5wdXQ6Y2hlY2tib3gnKS5sZW5ndGgpIHtcclxuXHRcdFx0XHRyZXR1cm4gJHRkLmZpbmQoJ2lucHV0OmNoZWNrYm94JykucHJvcCgnY2hlY2tlZCcpID8gJ+KclCcgOiAn4pyYJztcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXR1cm4gJHRkLnRleHQoKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIEdlbmVyYXRlcyBhbmQgc2V0cyB0aGUgdG9vbHRpcCBjb250ZW50IGZvciB0aGUgaGlkZGVuIGNvbHVtbnMgY29udGVudC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRyIFRoZSBjdXJyZW50IHJvdyBzZWxlY3Rvci5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX2dlbmVyYXRlSGlkZGVuQ29sdW1uc0NvbnRlbnQoJHRyKSB7XHJcblx0XHRcdGxldCBoaWRkZW5Db2x1bW5Db250ZW50SHRtbCA9ICcnO1xyXG5cdFx0XHRcclxuXHRcdFx0JHRyLmZpbmQoJ3RkLmhpZGRlbicpLmVhY2goKGluZGV4LCB0ZCkgPT4ge1xyXG5cdFx0XHRcdGhpZGRlbkNvbHVtbkNvbnRlbnRIdG1sICs9ICR0aGlzLmZpbmQoYHRoZWFkOmZpcnN0IHRyOmZpcnN0IHRoOmVxKCR7JCh0ZCkuaW5kZXgoKX0pYCkudGV4dCgpXHJcblx0XHRcdFx0XHQrICc6ICcgKyBfZ2V0Q2VsbENvbnRlbnQoJCh0ZCkpICsgJzxici8+JztcclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQkdHIuZmluZCgnLmhpZGRlbi1jb2x1bW5zLWNvbnRlbnQnKS5xdGlwKHtcclxuXHRcdFx0XHRjb250ZW50OiBoaWRkZW5Db2x1bW5Db250ZW50SHRtbCxcclxuXHRcdFx0XHRzdHlsZToge1xyXG5cdFx0XHRcdFx0Y2xhc3NlczogJ2d4LXF0aXAgaW5mbydcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdGhpZGU6IHtcclxuXHRcdFx0XHRcdGZpeGVkOiB0cnVlLFxyXG5cdFx0XHRcdFx0ZGVsYXk6IDMwMFxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogSGlkZSBEYXRhVGFibGUgQ29sdW1uc1xyXG5cdFx0ICpcclxuXHRcdCAqIFRoaXMgbWV0aG9kIGlzIHBhcnQgb2YgdGhlIHJlc3BvbnNpdmUgdGFibGVzIHNvbHV0aW9uLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGFyZ2V0V3JhcHBlciBUYXJnZXQgZGF0YXRhYmxlIGluc3RhbmNlIHdyYXBwZXIgZGl2LlxyXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICRmaXJzdEhpZGRlbkNvbHVtbiBUaGUgZmlyc3QgaGlkZGVuIGNvbHVtbiAoZmlyc3QgY29sdW1uIHdpdGggdGhlIC5oaWRkZW4gY2xhc3MpLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfaGlkZUNvbHVtbnMoJHRhcmdldFdyYXBwZXIsICRmaXJzdEhpZGRlbkNvbHVtbikge1xyXG5cdFx0XHRjb25zdCAkbGFzdFZpc2libGVDb2x1bW4gPSAoJGZpcnN0SGlkZGVuQ29sdW1uLmxlbmd0aCAhPT0gMClcclxuXHRcdFx0XHQ/ICRmaXJzdEhpZGRlbkNvbHVtbi5wcmV2KClcclxuXHRcdFx0XHQ6ICR0aGlzLmZpbmQoJ3RoZWFkOmZpcnN0IHRoLmFjdGlvbnMnKS5wcmV2KCk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoJGxhc3RWaXNpYmxlQ29sdW1uLmhhc0NsYXNzKCdoaWRkZW4nKSB8fCAkbGFzdFZpc2libGVDb2x1bW4uaW5kZXgoKSA9PT0gMCkge1xyXG5cdFx0XHRcdHJldHVybjsgLy8gRmlyc3QgY29sdW1uIG9yIGFscmVhZHkgaGlkZGVuLCBkbyBub3QgY29udGludWUuXHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdC8vIFNob3cgaGlkZGVuIGNvbHVtbiBjb250ZW50IGljb24uXHJcblx0XHRcdGlmICgkdGhpcy5maW5kKCcuaGlkZGVuLWNvbHVtbnMtY29udGVudCcpLmxlbmd0aCA9PT0gMCkge1xyXG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3Rib2R5IHRyJykuZWFjaCgoaW5kZXgsIHRyKSA9PiB7XHJcblx0XHRcdFx0XHRfYWRkSGlkZGVuQ29sdW1uc0NvbnRlbnRJY29uKCQodHIpKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0Ly8gSGlkZSB0aGUgbGFzdCB2aXNpYmxlIGNvbHVtbi5cclxuXHRcdFx0JHRoaXMuZmluZCgndHInKS5lYWNoKChpbmRleCwgdHIpID0+IHtcclxuXHRcdFx0XHQkKHRyKVxyXG5cdFx0XHRcdFx0LmZpbmQoYHRoOmVxKCR7JGxhc3RWaXNpYmxlQ29sdW1uLmluZGV4KCl9KSwgdGQ6ZXEoJHskbGFzdFZpc2libGVDb2x1bW4uaW5kZXgoKX0pYClcclxuXHRcdFx0XHRcdC5hZGRDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gR2VuZXJhdGUgdGhlIGhpZGRlbiBjb2x1bW5zIGNvbnRlbnQuXHJcblx0XHRcdFx0X2dlbmVyYXRlSGlkZGVuQ29sdW1uc0NvbnRlbnQoJCh0cikpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdF91cGRhdGVFbXB0eVRhYmxlQ29sU3BhbigpO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gSWYgdGhlcmUgYXJlIHN0aWxsIGNvbHVtbnMgd2hpY2ggZG9uJ3QgZml0IHdpdGhpbiB0aGUgdmlld3BvcnQsIGhpZGUgdGhlbS5cclxuXHRcdFx0aWYgKCR0YXJnZXRXcmFwcGVyLndpZHRoKCkgPCAkdGhpcy53aWR0aCgpICYmICRsYXN0VmlzaWJsZUNvbHVtbi5pbmRleCgpID4gMSkge1xyXG5cdFx0XHRcdF90b2dnbGVDb2x1bW5zVmlzaWJpbGl0eSgpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogU2hvdyBEYXRhVGFibGUgQ29sdW1uc1xyXG5cdFx0ICpcclxuXHRcdCAqIFRoaXMgbWV0aG9kIGlzIHBhcnQgb2YgdGhlIHJlc3BvbnNpdmUgdGFibGVzIHNvbHV0aW9uLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGFyZ2V0V3JhcHBlciBUYXJnZXQgZGF0YXRhYmxlIGluc3RhbmNlIHdyYXBwZXIgZGl2LlxyXG5cdFx0ICogQHBhcmFtIHtqUXVlcnl9ICRmaXJzdEhpZGRlbkNvbHVtbiBUaGUgZmlyc3QgaGlkZGVuIGNvbHVtbiAoZmlyc3QgY29sdW1uIHdpdGggdGhlIC5oaWRkZW4gY2xhc3MpLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfc2hvd0NvbHVtbnMoJHRhcmdldFdyYXBwZXIsICRmaXJzdEhpZGRlbkNvbHVtbikge1xyXG5cdFx0XHRpZiAoJGZpcnN0SGlkZGVuQ29sdW1uLmxlbmd0aCA9PT0gMCkge1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3QgZmlyc3RIaWRkZW5Db2x1bW5XaWR0aCA9IHBhcnNlSW50KCRmaXJzdEhpZGRlbkNvbHVtbi5jc3MoJ21pbi13aWR0aCcpKTtcclxuXHRcdFx0bGV0IHRhYmxlTWluV2lkdGggPSAwO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gQ2FsY3VsYXRlIHRoZSB0YWJsZSBtaW4gd2lkdGggYnkgZWFjaCBjb2x1bW4gbWluIHdpZHRoLlxyXG5cdFx0XHQkdGhpcy5maW5kKCd0aGVhZDpmaXJzdCB0cjpmaXJzdCB0aCcpLmVhY2goKGluZGV4LCB0aCkgPT4ge1xyXG5cdFx0XHRcdGlmICghJCh0aCkuaGFzQ2xhc3MoJ2hpZGRlbicpKSB7XHJcblx0XHRcdFx0XHR0YWJsZU1pbldpZHRoICs9IHBhcnNlSW50KCQodGgpLmNzcygnbWluLXdpZHRoJykpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBTaG93IHRoZSBmaXJzdCBoaWRkZW4gY29sdW1uLlxyXG5cdFx0XHRpZiAodGFibGVNaW5XaWR0aCArIGZpcnN0SGlkZGVuQ29sdW1uV2lkdGggPD0gJHRhcmdldFdyYXBwZXIub3V0ZXJXaWR0aCgpKSB7XHJcblx0XHRcdFx0JHRoaXMuZmluZCgndHInKS5lYWNoKChpbmRleCwgdHIpID0+IHtcclxuXHRcdFx0XHRcdCQodHIpXHJcblx0XHRcdFx0XHRcdC5maW5kKGB0aDplcSgkeyRmaXJzdEhpZGRlbkNvbHVtbi5pbmRleCgpfSksIHRkOmVxKCR7JGZpcnN0SGlkZGVuQ29sdW1uLmluZGV4KCl9KWApXHJcblx0XHRcdFx0XHRcdC5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdF9nZW5lcmF0ZUhpZGRlbkNvbHVtbnNDb250ZW50KCQodHIpKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRfdXBkYXRlRW1wdHlUYWJsZUNvbFNwYW4oKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBIaWRlIGhpZGRlbiBjb2x1bW4gY29udGVudCBpY29uLlxyXG5cdFx0XHRcdGlmICgkdGhpcy5maW5kKCd0aGVhZDpmaXJzdCB0cjpmaXJzdCB0aC5oaWRkZW4nKS5sZW5ndGggPT09IDApIHtcclxuXHRcdFx0XHRcdCR0aGlzLmZpbmQoJy5oaWRkZW4tY29sdW1ucy1jb250ZW50JykucmVtb3ZlKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIElmIHRoZXJlIGFyZSBzdGlsbCBjb2x1bW5zIHdoaWNoIHdvdWxkIGZpdCBmaXQgd2l0aGluIHRoZSB2aWV3cG9ydCwgc2hvdyB0aGVtLlxyXG5cdFx0XHRcdGNvbnN0IG5ld1RhYmxlTWluV2lkdGggPSB0YWJsZU1pbldpZHRoICsgZmlyc3RIaWRkZW5Db2x1bW5XaWR0aFxyXG5cdFx0XHRcdFx0KyBwYXJzZUludCgkZmlyc3RIaWRkZW5Db2x1bW4ubmV4dCgnLmhpZGRlbicpLmNzcygnbWluLXdpZHRoJykpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChuZXdUYWJsZU1pbldpZHRoIDw9ICR0YXJnZXRXcmFwcGVyLm91dGVyV2lkdGgoKSAmJiAkZmlyc3RIaWRkZW5Db2x1bW4ubmV4dCgnLmhpZGRlbicpLmxlbmd0aFxyXG5cdFx0XHRcdFx0IT09IDApIHtcclxuXHRcdFx0XHRcdF90b2dnbGVDb2x1bW5zVmlzaWJpbGl0eSgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIFRvZ2dsZSBjb2x1bW4gdmlzaWJpbGl0eSBkZXBlbmRpbmcgdGhlIHdpbmRvdyBzaXplLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfdG9nZ2xlQ29sdW1uc1Zpc2liaWxpdHkoKSB7XHJcblx0XHRcdGNvbnN0ICR0YXJnZXRXcmFwcGVyID0gJHRoaXMucGFyZW50KCk7XHJcblx0XHRcdGNvbnN0ICRmaXJzdEhpZGRlbkNvbHVtbiA9ICR0aGlzLmZpbmQoJ3RoZWFkOmZpcnN0IHRoLmhpZGRlbjpmaXJzdCcpO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKCR0YXJnZXRXcmFwcGVyLndpZHRoKCkgPCAkdGhpcy53aWR0aCgpKSB7XHJcblx0XHRcdFx0X2hpZGVDb2x1bW5zKCR0YXJnZXRXcmFwcGVyLCAkZmlyc3RIaWRkZW5Db2x1bW4pO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdF9zaG93Q29sdW1ucygkdGFyZ2V0V3JhcHBlciwgJGZpcnN0SGlkZGVuQ29sdW1uKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIENhbGN1bGF0ZSBhbmQgc2V0IHRoZSByZWxhdGl2ZSBjb2x1bW4gd2lkdGhzLlxyXG5cdFx0ICpcclxuXHRcdCAqIFRoZSByZWxhdGl2ZSB3aWR0aCBjYWxjdWxhdGlvbiB3b3JrcyB3aXRoIGEgd2lkdGgtZmFjdG9yIHN5c3RlbSB3aGVyZSBlYWNoIGNvbHVtbiBwcmVzZXJ2ZXMgYVxyXG5cdFx0ICogc3BlY2lmaWMgYW1vdW50IG9mIHRoZSB0YWJsZSB3aWR0aC5cclxuXHRcdCAqXHJcblx0XHQgKiBUaGlzIGZhY3RvciBpcyBub3QgZGVmaW5pbmcgYSBwZXJjZW50YWdlLCByYXRoZXIgb25seSBhIHdpZHRoLXZvbHVtZS4gUGVyY2VudGFnZSB3aWR0aHMgd2lsbCBub3RcclxuXHRcdCAqIHdvcmsgY29ycmVjdGx5IHdoZW4gdGhlIHRhYmxlIGhhcyBmZXdlciBjb2x1bW5zIHRoYW4gdGhlIG9yaWdpbmFsIHNldHRpbmdzLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfYXBwbHlSZWxhdGl2ZUNvbHVtbldpZHRocygpIHtcclxuXHRcdFx0JHRoaXMuZmluZCgndGhlYWQ6Zmlyc3QgdHI6Zmlyc3QgdGgnKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGlmICgkKHRoaXMpLmNzcygnZGlzcGxheScpID09PSAnbm9uZScpIHtcclxuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRsZXQgY3VycmVudENvbHVtbkRlZmluaXRpb247XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y29sdW1uRGVmaW5pdGlvbnMuZm9yRWFjaCgoY29sdW1uRGVmaW5pdGlvbikgPT4ge1xyXG5cdFx0XHRcdFx0aWYgKGNvbHVtbkRlZmluaXRpb24ubmFtZSA9PT0gJCh0aGlzKS5kYXRhKCdjb2x1bW5OYW1lJykpIHtcclxuXHRcdFx0XHRcdFx0Y3VycmVudENvbHVtbkRlZmluaXRpb24gPSBjb2x1bW5EZWZpbml0aW9uO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChjdXJyZW50Q29sdW1uRGVmaW5pdGlvbiAmJiBjdXJyZW50Q29sdW1uRGVmaW5pdGlvbi53aWR0aEZhY3Rvcikge1xyXG5cdFx0XHRcdFx0Y29uc3QgaW5kZXggPSAkKHRoaXMpLmluZGV4KCk7XHJcblx0XHRcdFx0XHRjb25zdCB3aWR0aCA9IE1hdGgucm91bmQoY3VycmVudENvbHVtbkRlZmluaXRpb24ud2lkdGhGYWN0b3IgLyB3aWR0aEZhY3RvclN1bSAqIDEwMCAqIDEwMCkgLyAxMDA7XHJcblx0XHRcdFx0XHQkdGhpcy5maW5kKCd0aGVhZCcpLmVhY2goKGksIHRoZWFkKSA9PiB7XHJcblx0XHRcdFx0XHRcdCQodGhlYWQpLmZpbmQoJ3RyJykuZWFjaCgoaSwgdHIpID0+IHtcclxuXHRcdFx0XHRcdFx0XHQkKHRyKS5maW5kKCd0aCcpLmVxKGluZGV4KS5jc3MoJ3dpZHRoJywgd2lkdGggKyAnJScpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogQXBwbGllcyB0aGUgY29sdW1uIHdpZHRoIGlmIHRoZSBjdXJyZW50IGNvbHVtbiB3aWR0aCBpcyBzbWFsbGVyLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfYXBwbHlNaW5pbXVtQ29sdW1uV2lkdGhzKCkge1xyXG5cdFx0XHQkdGhpcy5maW5kKCd0aGVhZDpmaXJzdCB0cjpmaXJzdCB0aCcpLmVhY2goZnVuY3Rpb24oaW5kZXgpIHtcclxuXHRcdFx0XHRpZiAoJCh0aGlzKS5jc3MoJ2Rpc3BsYXknKSA9PT0gJ25vbmUnKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0bGV0IGN1cnJlbnRDb2x1bW5EZWZpbml0aW9uO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGNvbHVtbkRlZmluaXRpb25zLmZvckVhY2goKGNvbHVtbkRlZmluaXRpb24pID0+IHtcclxuXHRcdFx0XHRcdGlmIChjb2x1bW5EZWZpbml0aW9uLm5hbWUgPT09ICQodGhpcykuZGF0YSgnY29sdW1uTmFtZScpKSB7XHJcblx0XHRcdFx0XHRcdGN1cnJlbnRDb2x1bW5EZWZpbml0aW9uID0gY29sdW1uRGVmaW5pdGlvbjtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoIWN1cnJlbnRDb2x1bW5EZWZpbml0aW9uKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y29uc3QgY3VycmVudFdpZHRoID0gJCh0aGlzKS5vdXRlcldpZHRoKCk7XHJcblx0XHRcdFx0Y29uc3QgZGVmaW5pdGlvbk1pbldpZHRoID0gcGFyc2VJbnQoY3VycmVudENvbHVtbkRlZmluaXRpb24ubWluV2lkdGgpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChjdXJyZW50V2lkdGggPCBkZWZpbml0aW9uTWluV2lkdGgpIHtcclxuXHRcdFx0XHRcdC8vIEZvcmNlIHRoZSBjb3JyZWN0IGNvbHVtbiBtaW4td2lkdGhzIGZvciBhbGwgdGhlYWQgY29sdW1ucy5cclxuXHRcdFx0XHRcdCR0aGlzLmZpbmQoJ3RoZWFkJykuZWFjaCgoaSwgdGhlYWQpID0+IHtcclxuXHRcdFx0XHRcdFx0JCh0aGVhZCkuZmluZCgndHInKS5lYWNoKChpLCB0cikgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdCQodHIpLmZpbmQoJ3RoJykuZXEoaW5kZXgpXHJcblx0XHRcdFx0XHRcdFx0XHQub3V0ZXJXaWR0aChkZWZpbml0aW9uTWluV2lkdGgpXHJcblx0XHRcdFx0XHRcdFx0XHQuY3NzKCdtYXgtd2lkdGgnLCBkZWZpbml0aW9uTWluV2lkdGgpXHJcblx0XHRcdFx0XHRcdFx0XHQuY3NzKCdtaW4td2lkdGgnLCBkZWZpbml0aW9uTWluV2lkdGgpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gRGF0YVRhYmxlIERyYXcgRXZlbnRcclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uRGF0YVRhYmxlRHJhdygpIHtcclxuXHRcdFx0Ly8gV2FpdCB1bnRpbCB0aGUgY29udGVudHMgb2YgdGhlIHRhYmxlIGFyZSByZW5kZXJlZC4gRGF0YVRhYmxlcyB3aWxsIHNvbWV0aW1lcyBmaXJlIHRoZSBkcmF3IGV2ZW50XHJcblx0XHRcdC8vIGV2ZW4gYmVmb3JlIHRoZSB0ZCBlbGVtZW50cyBhcmUgcmVuZGVyZWQgaW4gdGhlIGJyb3dzZXIuXHJcblx0XHRcdHZhciBpbnRlcnZhbCA9IHNldEludGVydmFsKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRpZiAoJHRoaXMuZmluZCgndGJvZHkgdHI6bGFzdCB0ZC5hY3Rpb25zJykubGVuZ3RoID09PSAxKSB7XHJcblx0XHRcdFx0XHRfYXBwbHlSZWxhdGl2ZUNvbHVtbldpZHRocygpO1xyXG5cdFx0XHRcdFx0X2FwcGx5TWluaW11bUNvbHVtbldpZHRocygpO1xyXG5cdFx0XHRcdFx0X3RvZ2dsZUNvbHVtbnNWaXNpYmlsaXR5KCk7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdC8vIEhpZGUgdGhlIHRib2R5IGNlbGxzIGRlcGVuZGluZyBvbiB3aGV0aGVyIHRoZSByZXNwZWN0aXZlIDx0aD4gZWxlbWVudCBpcyBoaWRkZW4uXHJcblx0XHRcdFx0XHQkdGhpcy5maW5kKCd0aGVhZDpmaXJzdCB0cjpmaXJzdCB0aCcpLmVhY2goZnVuY3Rpb24gKGluZGV4LCB0aCkge1xyXG5cdFx0XHRcdFx0XHRpZiAoJCh0aCkuaGFzQ2xhc3MoJ2hpZGRlbicpKSB7XHJcblx0XHRcdFx0XHRcdFx0JHRoaXMuZmluZCgndGJvZHkgdHInKS5lYWNoKGZ1bmN0aW9uIChpLCB0cikge1xyXG5cdFx0XHRcdFx0XHRcdFx0JCh0cikuZmluZCgndGQ6ZXEoJyArIGluZGV4ICsgJyknKS5hZGRDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBBZGQgdGhlIGhpZGRlbiBjb2x1bW5zIGljb24gaWYgbmVlZGVkLlxyXG5cdFx0XHRcdFx0aWYgKCR0aGlzLmZpbmQoJ3RoZWFkIHRoLmhpZGRlbicpLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHQkdGhpcy5maW5kKCd0Ym9keSB0cicpLmVhY2goZnVuY3Rpb24gKGluZGV4LCB0cikge1xyXG5cdFx0XHRcdFx0XHRcdGlmICgkKHRyKS5maW5kKCcuaGlkZGVuLWNvbHVtbnMtY29udGVudCcpLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdF9hZGRIaWRkZW5Db2x1bW5zQ29udGVudEljb24oJCh0cikpO1xyXG5cdFx0XHRcdFx0XHRcdF9nZW5lcmF0ZUhpZGRlbkNvbHVtbnNDb250ZW50KCQodHIpKTtcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGNsZWFySW50ZXJ2YWwoaW50ZXJ2YWwpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSwgNTAwKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBXaW5kb3cgUmVzaXplIEV2ZW50XHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vbldpbmRvd1Jlc2l6ZSgpIHtcclxuXHRcdFx0JHRoaXMuZmluZCgndGhlYWQuZml4ZWQnKS5vdXRlcldpZHRoKCR0aGlzLm91dGVyV2lkdGgoKSk7XHJcblx0XHRcdF9hcHBseVJlbGF0aXZlQ29sdW1uV2lkdGhzKCk7XHJcblx0XHRcdF9hcHBseU1pbmltdW1Db2x1bW5XaWR0aHMoKTtcclxuXHRcdFx0X3RvZ2dsZUNvbHVtbnNWaXNpYmlsaXR5KCk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogT24gRGF0YVRhYmxlIEluaXRpYWxpemUgRXZlbnQgXHJcblx0XHQgKi9cclxuXHRcdGZ1bmN0aW9uIF9vbkRhdGFUYWJsZUluaXQoKSB7XHJcblx0XHRcdCR0aGlzLmZpbmQob3B0aW9ucy52aXNpYmlsaXR5VG9nZ2xlU2VsZWN0b3IpLnNob3coKTtcclxuXHRcdFx0X3VwZGF0ZUVtcHR5VGFibGVDb2xTcGFuKCk7XHJcblx0XHRcdFxyXG5cdFx0XHRjb2x1bW5EZWZpbml0aW9ucyA9ICR0aGlzLkRhdGFUYWJsZSgpLmluaXQoKS5jb2x1bW5zO1xyXG5cdFx0XHR3aWR0aEZhY3RvclN1bSA9IDA7XHJcblx0XHRcdFxyXG5cdFx0XHRjb2x1bW5EZWZpbml0aW9ucy5mb3JFYWNoKChjb2x1bW5EZWZpbml0aW9uKSA9PiB7XHJcblx0XHRcdFx0d2lkdGhGYWN0b3JTdW0gKz0gY29sdW1uRGVmaW5pdGlvbi53aWR0aEZhY3RvciB8fCAwO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdCR0aGlzLm9uKCdkcmF3LmR0JywgX29uRGF0YVRhYmxlRHJhdyk7XHJcblx0XHRcdCQod2luZG93KS5vbigncmVzaXplJywgX29uV2luZG93UmVzaXplKTtcclxuXHRcdFx0XHJcblx0XHRcdF9vbldpbmRvd1Jlc2l6ZSgpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdCR0aGlzLm9uKCdpbml0LmR0JywgX29uRGF0YVRhYmxlSW5pdCk7XHJcblx0XHRcdFxyXG5cdFx0XHQkKHdpbmRvdykub24oJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCAoKSA9PiB7XHJcblx0XHRcdFx0aWYgKCR0aGlzLkRhdGFUYWJsZSgpLmFqYXguanNvbigpICE9PSB1bmRlZmluZWQpIHtcclxuXHRcdFx0XHRcdF9vbkRhdGFUYWJsZUluaXQoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0JHRoaXMuZmluZChvcHRpb25zLnZpc2liaWxpdHlUb2dnbGVTZWxlY3RvcikuaGlkZSgpO1xyXG5cdFx0XHRcclxuXHRcdFx0ZG9uZSgpO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0cmV0dXJuIG1vZHVsZTtcclxuXHRcdFxyXG5cdH0pOyBcclxuIl19
