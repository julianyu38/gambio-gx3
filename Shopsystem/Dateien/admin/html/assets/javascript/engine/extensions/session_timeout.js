'use strict';

/* --------------------------------------------------------------
 session_timeout.js 2016-11-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2011 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.extensions.module('session_timeout', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		url: jse.core.config.get('appUrl') + '/admin/admin.php?do=SessionTimeoutAjax',
		timeout: 1000
	},


	/**
  * Final Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Is firing by <div data-gx-extension="session_timeout" hidden></div>
  *
  * @see admin/html/content/admin_menu.html
  * @private
  */
	var _initializeTimeout = function _initializeTimeout() {
		$.getJSON(options.url).done(function (response) {
			if (response.logout === true) {
				return;
			}

			options.timeout = response.data[0];
			setTimeout(_initializeTimeout, options.timeout);
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(document).on('JSENGINE_INIT_FINISHED', _initializeTimeout);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlc3Npb25fdGltZW91dC5qcyJdLCJuYW1lcyI6WyJneCIsImV4dGVuc2lvbnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJ1cmwiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwidGltZW91dCIsIm9wdGlvbnMiLCJleHRlbmQiLCJfaW5pdGlhbGl6ZVRpbWVvdXQiLCJnZXRKU09OIiwiZG9uZSIsInJlc3BvbnNlIiwibG9nb3V0Iiwic2V0VGltZW91dCIsImluaXQiLCJkb2N1bWVudCIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBV0FBLEdBQUdDLFVBQUgsQ0FBY0MsTUFBZCxDQUNDLGlCQURELEVBR0MsRUFIRCxFQUtDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXO0FBQ1ZDLE9BQUtDLElBQUlDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0Msd0NBRDNCO0FBRVZDLFdBQVM7QUFGQyxFQWJaOzs7QUFrQkM7Ozs7O0FBS0FDLFdBQVVSLEVBQUVTLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQlIsUUFBbkIsRUFBNkJILElBQTdCLENBdkJYOzs7QUF5QkM7Ozs7O0FBS0FELFVBQVMsRUE5QlY7O0FBZ0NBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUEsS0FBSWEscUJBQXFCLFNBQXJCQSxrQkFBcUIsR0FBVztBQUNuQ1YsSUFBRVcsT0FBRixDQUFVSCxRQUFRTixHQUFsQixFQUNFVSxJQURGLENBQ08sVUFBU0MsUUFBVCxFQUFtQjtBQUN4QixPQUFJQSxTQUFTQyxNQUFULEtBQW9CLElBQXhCLEVBQThCO0FBQzdCO0FBQ0E7O0FBRUROLFdBQVFELE9BQVIsR0FBa0JNLFNBQVNmLElBQVQsQ0FBYyxDQUFkLENBQWxCO0FBQ0FpQixjQUFXTCxrQkFBWCxFQUErQkYsUUFBUUQsT0FBdkM7QUFDQSxHQVJGO0FBU0EsRUFWRDs7QUFZQTtBQUNBO0FBQ0E7O0FBRUFWLFFBQU9tQixJQUFQLEdBQWMsVUFBU0osSUFBVCxFQUFlO0FBQzVCWixJQUFFaUIsUUFBRixFQUFZQyxFQUFaLENBQWUsd0JBQWYsRUFBeUNSLGtCQUF6Qzs7QUFFQUU7QUFDQSxFQUpEOztBQU1BLFFBQU9mLE1BQVA7QUFDQSxDQTlFRiIsImZpbGUiOiJzZXNzaW9uX3RpbWVvdXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNlc3Npb25fdGltZW91dC5qcyAyMDE2LTExLTEwXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxMSBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuXG5neC5leHRlbnNpb25zLm1vZHVsZShcblx0J3Nlc3Npb25fdGltZW91dCcsXG5cdFxuXHRbXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bGV0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0dXJsOiBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPVNlc3Npb25UaW1lb3V0QWpheCcsXG5cdFx0XHRcdHRpbWVvdXQ6IDEwMDAsXG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSXMgZmlyaW5nIGJ5IDxkaXYgZGF0YS1neC1leHRlbnNpb249XCJzZXNzaW9uX3RpbWVvdXRcIiBoaWRkZW4+PC9kaXY+XG5cdFx0ICpcblx0XHQgKiBAc2VlIGFkbWluL2h0bWwvY29udGVudC9hZG1pbl9tZW51Lmh0bWxcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfaW5pdGlhbGl6ZVRpbWVvdXQgPSBmdW5jdGlvbigpIHtcblx0XHRcdCQuZ2V0SlNPTihvcHRpb25zLnVybClcblx0XHRcdFx0LmRvbmUoZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRpZiAocmVzcG9uc2UubG9nb3V0ID09PSB0cnVlKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdG9wdGlvbnMudGltZW91dCA9IHJlc3BvbnNlLmRhdGFbMF07XG5cdFx0XHRcdFx0c2V0VGltZW91dChfaW5pdGlhbGl6ZVRpbWVvdXQsIG9wdGlvbnMudGltZW91dCk7XG5cdFx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCQoZG9jdW1lbnQpLm9uKCdKU0VOR0lORV9JTklUX0ZJTklTSEVEJywgX2luaXRpYWxpemVUaW1lb3V0KTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7Il19
