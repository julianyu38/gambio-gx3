'use strict';

/* --------------------------------------------------------------
 shortcuts.js 2016-09-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Shortcuts Extension
 *
 * This extensions handles the invocation of custom registered shortcut callbacks.
 * You can also register your own shortcuts by using the library function.
 * Please refer to the library file for further information.
 *
 * @module Admin/Extensions/shortcuts
 */
gx.extensions.module('shortcuts', [gx.source + '/libs/shortcuts'], function (data) {
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	// Module element.
	var $this = $(this);

	// Module default parameters.
	var defaults = {
		// Milliseconds to wait after pressing a key (for the shortcut check).
		delay: 100
	};

	// Module options.
	var options = $.extend(true, {}, defaults, data);

	// Module instance.
	var module = {};

	// List of currently pressed keys.
	var pressedKeys = [];

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Handles the key down event (when a key has been pressed down).
  *
  * @param {jQuery.Event} event Triggered event.
  * @private
  */
	function _onKeyDown(event) {
		// Register pressed keys.
		pressedKeys.push(event.which);
	}

	/**
  * Handles the key up event (when a key has been released).
  *
  * @private
  */
	function _onKeyUp() {
		// Perform the shortcut check after a certain delay.
		setTimeout(_checkShortcut, options.delay);
	}

	/**
  * Performs a shortcut check by matching the shortcuts in the register object.
  *
  * @private
  */
	function _checkShortcut() {
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = jse.libs.shortcuts.registeredShortcuts[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var shortcut = _step.value;

				if (_checkArrayEquality(shortcut.combination, pressedKeys)) {
					shortcut.callback();
					break;
				}
			}

			// Reset pressed keys array.
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		pressedKeys = [];
	}

	/**
  * Returns whether the provided arrays are equal.
  *
  * @param {Array} a Array to compare.
  * @param {Array} b Other array to compare.
  * @returns {Boolean} Are the arrays equal?
  * @private
  */
	function _checkArrayEquality(a, b) {
		if (a === b) {
			return true;
		}
		if (a == null || b === null) {
			return false;
		}
		if (a.length !== b.length) {
			return false;
		}

		for (var i = 0; i < a.length; ++i) {
			if (a[i] !== b[i]) {
				return false;
			}
		}

		return true;
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		// Bind key press event handlers.
		$this.on('keydown', _onKeyDown).on('keyup', _onKeyUp);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNob3J0Y3V0cy5qcyJdLCJuYW1lcyI6WyJneCIsImV4dGVuc2lvbnMiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJkZWxheSIsIm9wdGlvbnMiLCJleHRlbmQiLCJwcmVzc2VkS2V5cyIsIl9vbktleURvd24iLCJldmVudCIsInB1c2giLCJ3aGljaCIsIl9vbktleVVwIiwic2V0VGltZW91dCIsIl9jaGVja1Nob3J0Y3V0IiwianNlIiwibGlicyIsInNob3J0Y3V0cyIsInJlZ2lzdGVyZWRTaG9ydGN1dHMiLCJzaG9ydGN1dCIsIl9jaGVja0FycmF5RXF1YWxpdHkiLCJjb21iaW5hdGlvbiIsImNhbGxiYWNrIiwiYSIsImIiLCJsZW5ndGgiLCJpIiwiaW5pdCIsIm9uIiwiZG9uZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7QUFTQUEsR0FBR0MsVUFBSCxDQUFjQyxNQUFkLENBQ0MsV0FERCxFQUdDLENBQ0lGLEdBQUdHLE1BRFAscUJBSEQsRUFPQyxVQUFTQyxJQUFULEVBQWU7QUFDZDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTtBQUNBLEtBQU1DLFdBQVc7QUFDaEI7QUFDQUMsU0FBTztBQUZTLEVBQWpCOztBQUtBO0FBQ0EsS0FBTUMsVUFBVUgsRUFBRUksTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CSCxRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7QUFDQSxLQUFNRixTQUFTLEVBQWY7O0FBRUE7QUFDQSxLQUFJUyxjQUFjLEVBQWxCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUEsVUFBU0MsVUFBVCxDQUFvQkMsS0FBcEIsRUFBMkI7QUFDMUI7QUFDQUYsY0FBWUcsSUFBWixDQUFpQkQsTUFBTUUsS0FBdkI7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxRQUFULEdBQW9CO0FBQ25CO0FBQ0FDLGFBQVdDLGNBQVgsRUFBMkJULFFBQVFELEtBQW5DO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU1UsY0FBVCxHQUEwQjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUN6Qix3QkFBdUJDLElBQUlDLElBQUosQ0FBU0MsU0FBVCxDQUFtQkMsbUJBQTFDLDhIQUErRDtBQUFBLFFBQXBEQyxRQUFvRDs7QUFDOUQsUUFBSUMsb0JBQW9CRCxTQUFTRSxXQUE3QixFQUEwQ2QsV0FBMUMsQ0FBSixFQUE0RDtBQUMzRFksY0FBU0csUUFBVDtBQUNBO0FBQ0E7QUFDRDs7QUFFRDtBQVJ5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQVN6QmYsZ0JBQWMsRUFBZDtBQUNBOztBQUVEOzs7Ozs7OztBQVFBLFVBQVNhLG1CQUFULENBQTZCRyxDQUE3QixFQUFnQ0MsQ0FBaEMsRUFBbUM7QUFDbEMsTUFBSUQsTUFBTUMsQ0FBVixFQUFhO0FBQ1osVUFBTyxJQUFQO0FBQ0E7QUFDRCxNQUFJRCxLQUFLLElBQUwsSUFBYUMsTUFBTSxJQUF2QixFQUE2QjtBQUM1QixVQUFPLEtBQVA7QUFDQTtBQUNELE1BQUlELEVBQUVFLE1BQUYsS0FBYUQsRUFBRUMsTUFBbkIsRUFBMkI7QUFDMUIsVUFBTyxLQUFQO0FBQ0E7O0FBRUQsT0FBSyxJQUFJQyxJQUFJLENBQWIsRUFBZ0JBLElBQUlILEVBQUVFLE1BQXRCLEVBQThCLEVBQUVDLENBQWhDLEVBQW1DO0FBQ2xDLE9BQUlILEVBQUVHLENBQUYsTUFBU0YsRUFBRUUsQ0FBRixDQUFiLEVBQW1CO0FBQ2xCLFdBQU8sS0FBUDtBQUNBO0FBQ0Q7O0FBRUQsU0FBTyxJQUFQO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOzs7QUFHQTVCLFFBQU82QixJQUFQLEdBQWMsZ0JBQVE7QUFDckI7QUFDQTFCLFFBQ0UyQixFQURGLENBQ0ssU0FETCxFQUNnQnBCLFVBRGhCLEVBRUVvQixFQUZGLENBRUssT0FGTCxFQUVjaEIsUUFGZDs7QUFJQTtBQUNBaUI7QUFDQSxFQVJEOztBQVVBLFFBQU8vQixNQUFQO0FBQ0EsQ0F0SEYiLCJmaWxlIjoic2hvcnRjdXRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzaG9ydGN1dHMuanMgMjAxNi0wOS0xM1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgU2hvcnRjdXRzIEV4dGVuc2lvblxuICpcbiAqIFRoaXMgZXh0ZW5zaW9ucyBoYW5kbGVzIHRoZSBpbnZvY2F0aW9uIG9mIGN1c3RvbSByZWdpc3RlcmVkIHNob3J0Y3V0IGNhbGxiYWNrcy5cbiAqIFlvdSBjYW4gYWxzbyByZWdpc3RlciB5b3VyIG93biBzaG9ydGN1dHMgYnkgdXNpbmcgdGhlIGxpYnJhcnkgZnVuY3Rpb24uXG4gKiBQbGVhc2UgcmVmZXIgdG8gdGhlIGxpYnJhcnkgZmlsZSBmb3IgZnVydGhlciBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAbW9kdWxlIEFkbWluL0V4dGVuc2lvbnMvc2hvcnRjdXRzXG4gKi9cbmd4LmV4dGVuc2lvbnMubW9kdWxlKFxuXHQnc2hvcnRjdXRzJyxcblxuXHRbXG5cdFx0YCR7Z3guc291cmNlfS9saWJzL3Nob3J0Y3V0c2Bcblx0XSxcblxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cblx0XHQvLyBNb2R1bGUgZWxlbWVudC5cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cblx0XHQvLyBNb2R1bGUgZGVmYXVsdCBwYXJhbWV0ZXJzLlxuXHRcdGNvbnN0IGRlZmF1bHRzID0ge1xuXHRcdFx0Ly8gTWlsbGlzZWNvbmRzIHRvIHdhaXQgYWZ0ZXIgcHJlc3NpbmcgYSBrZXkgKGZvciB0aGUgc2hvcnRjdXQgY2hlY2spLlxuXHRcdFx0ZGVsYXk6IDEwMFxuXHRcdH07XG5cblx0XHQvLyBNb2R1bGUgb3B0aW9ucy5cblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcblxuXHRcdC8vIE1vZHVsZSBpbnN0YW5jZS5cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblxuXHRcdC8vIExpc3Qgb2YgY3VycmVudGx5IHByZXNzZWQga2V5cy5cblx0XHRsZXQgcHJlc3NlZEtleXMgPSBbXTtcblxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlcyB0aGUga2V5IGRvd24gZXZlbnQgKHdoZW4gYSBrZXkgaGFzIGJlZW4gcHJlc3NlZCBkb3duKS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5LkV2ZW50fSBldmVudCBUcmlnZ2VyZWQgZXZlbnQuXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25LZXlEb3duKGV2ZW50KSB7XG5cdFx0XHQvLyBSZWdpc3RlciBwcmVzc2VkIGtleXMuXG5cdFx0XHRwcmVzc2VkS2V5cy5wdXNoKGV2ZW50LndoaWNoKTtcblx0XHR9XG5cblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBrZXkgdXAgZXZlbnQgKHdoZW4gYSBrZXkgaGFzIGJlZW4gcmVsZWFzZWQpLlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25LZXlVcCgpIHtcblx0XHRcdC8vIFBlcmZvcm0gdGhlIHNob3J0Y3V0IGNoZWNrIGFmdGVyIGEgY2VydGFpbiBkZWxheS5cblx0XHRcdHNldFRpbWVvdXQoX2NoZWNrU2hvcnRjdXQsIG9wdGlvbnMuZGVsYXkpO1xuXHRcdH1cblxuXHRcdC8qKlxuXHRcdCAqIFBlcmZvcm1zIGEgc2hvcnRjdXQgY2hlY2sgYnkgbWF0Y2hpbmcgdGhlIHNob3J0Y3V0cyBpbiB0aGUgcmVnaXN0ZXIgb2JqZWN0LlxuXHRcdCAqXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfY2hlY2tTaG9ydGN1dCgpIHtcblx0XHRcdGZvciAoY29uc3Qgc2hvcnRjdXQgb2YganNlLmxpYnMuc2hvcnRjdXRzLnJlZ2lzdGVyZWRTaG9ydGN1dHMpIHtcblx0XHRcdFx0aWYgKF9jaGVja0FycmF5RXF1YWxpdHkoc2hvcnRjdXQuY29tYmluYXRpb24sIHByZXNzZWRLZXlzKSkge1xuXHRcdFx0XHRcdHNob3J0Y3V0LmNhbGxiYWNrKCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0Ly8gUmVzZXQgcHJlc3NlZCBrZXlzIGFycmF5LlxuXHRcdFx0cHJlc3NlZEtleXMgPSBbXTtcblx0XHR9XG5cblx0XHQvKipcblx0XHQgKiBSZXR1cm5zIHdoZXRoZXIgdGhlIHByb3ZpZGVkIGFycmF5cyBhcmUgZXF1YWwuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge0FycmF5fSBhIEFycmF5IHRvIGNvbXBhcmUuXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gYiBPdGhlciBhcnJheSB0byBjb21wYXJlLlxuXHRcdCAqIEByZXR1cm5zIHtCb29sZWFufSBBcmUgdGhlIGFycmF5cyBlcXVhbD9cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9jaGVja0FycmF5RXF1YWxpdHkoYSwgYikge1xuXHRcdFx0aWYgKGEgPT09IGIpIHtcblx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHR9XG5cdFx0XHRpZiAoYSA9PSBudWxsIHx8IGIgPT09IG51bGwpIHtcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0fVxuXHRcdFx0aWYgKGEubGVuZ3RoICE9PSBiLmxlbmd0aCkge1xuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHR9XG5cblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgYS5sZW5ndGg7ICsraSkge1xuXHRcdFx0XHRpZiAoYVtpXSAhPT0gYltpXSkge1xuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9XG5cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIHdpZGdldCwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdC8vIEJpbmQga2V5IHByZXNzIGV2ZW50IGhhbmRsZXJzLlxuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdrZXlkb3duJywgX29uS2V5RG93bilcblx0XHRcdFx0Lm9uKCdrZXl1cCcsIF9vbktleVVwKTtcblxuXHRcdFx0Ly8gRmluaXNoIGluaXRpYWxpemF0aW9uLlxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9XG4pO1xuIl19
