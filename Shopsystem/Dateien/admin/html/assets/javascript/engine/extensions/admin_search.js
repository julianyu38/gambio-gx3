'use strict';

/* --------------------------------------------------------------
 admin_search.js 2017-01-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Admin Search Extension
 *
 * Extension for search in orders, customers and categories in the admin panel
 *
 * @module Admin/Extension/admin_search
 * @requires jQueryUI
 * @ignore
 */
gx.extensions.module('admin_search', ['user_configuration_service', 'url_arguments', 'loading_spinner'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	// Elements.

	var $this = $(this),
	    $button = $(data.button),
	    $dropdown = $('ul.searchable:first'),
	    recentSearch = $.trim(decodeURIComponent(jse.libs.url_arguments.getUrlParameters(location.href).search || ''));

	// Current search area.
	var searchArea;

	// Text labels.
	var labels = {
		searchIn: jse.core.lang.translate('admin_search_in_label', 'admin_labels'),
		orders: jse.core.lang.translate('admin_search_orders', 'admin_labels'),
		invoices: jse.core.lang.translate('admin_search_invoices', 'admin_labels'),
		customers: jse.core.lang.translate('admin_search_customers', 'admin_labels'),
		categories: jse.core.lang.translate('admin_search_categories', 'admin_labels')
	};

	// Key code map.
	var keyMap = {
		ESC: 27,
		ARROW_UP: 38,
		ARROW_DOWN: 40,
		ENTER: 13
	};

	// Library access shortcuts.
	var userConfigurationService = jse.libs.user_configuration_service,
	    urlArguments = jse.libs.url_arguments;

	// Configuration settings for UserConfigurationService.
	var configurationContainer = {
		userId: data.customer_id,
		configurationKey: 'recent_search_area'
	};

	// Module object (JSEngine).
	var module = {};

	// ------------------------------------------------------------------------
	// METHODS
	// ------------------------------------------------------------------------

	/**
  * Refreshes the search area variable
  *
  * Shows the new search area in the button
  * @private
  */
	var _refreshSearchArea = function _refreshSearchArea() {
		// Abort if no new search area is provided
		if (!$('.search-item.active').length) {
			console.error('No active list item!');
		}

		// Assign new search area
		searchArea = $('.search-item.active').data('searchArea');
		$this.trigger('refresh:search-area');
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	var _initializeInput = function _initializeInput() {

		// Click event
		$this.on('click', function () {
			$this.trigger('refresh:search-area');
			if ($this.val() === '') {
				$this.val(recentSearch);
			}
			$dropdown.trigger('show:dropdown');
			$this.trigger('focus');
		});

		// Keyboard events
		$this.on('keyup', function (event) {
			switch (event.which) {

				// Perform search if enter key is pressed
				case keyMap.ENTER:
					$this.trigger('perform:search');
					break;

				// Close dropdown if escape key is pressed
				case keyMap.ESC:
					$dropdown.trigger('hide:dropdown');
					return;

				// Navigate up in dropdown
				case keyMap.ARROW_UP:
					$dropdown.trigger('select:item:previous');
					break;
				case keyMap.ARROW_DOWN:
					$dropdown.trigger('select:item:next');
					break;
			}
			$dropdown.trigger('refresh:search-item');
		});

		// Search events
		$this.on('perform:search', function () {
			var inputValue = encodeURIComponent($this.val()),
			    openMode = '_self',
			    url;

			switch (searchArea) {
				case 'customers':
					url = ['customers.php', '?search=', inputValue].join('');
					break;
				case 'categories':
					url = ['categories.php', '?search=', inputValue].join('');
					break;
				case 'orders':
					url = ['admin.php', '?', $.param({
						do: 'OrdersOverview',
						filter: {
							number: inputValue
						}
					})].join('');
					break;
				case 'invoices':
					url = ['admin.php', '?', $.param({
						do: 'InvoicesOverview',
						filter: {
							invoiceNumber: inputValue
						}
					})].join('');
					break;
			}

			// Display loading spinner. 
			jse.libs.loading_spinner.show($dropdown, '9999');

			userConfigurationService.set({
				data: $.extend(configurationContainer, {
					configurationValue: searchArea
				}),
				onSuccess: function onSuccess() {
					window.open(url, openMode);
				},
				onError: function onError() {
					window.open(url, openMode);
				}
			});
		});

		// Change search area event
		$this.on('refresh:search-area', function () {
			$this.prop('placeholder', labels[searchArea]);
		});

		// Remove placeholder when input is inactive
		$this.on('blur', function () {
			$this.prop('placeholder', '');
			$dropdown.trigger('hide:dropdown');
		});
	};

	var _initializeButton = function _initializeButton() {
		$button.on('click', function () {
			$this.trigger('refresh:search-area');
			$this.val(recentSearch);
			$dropdown.trigger('show:dropdown');
			$this.trigger('focus');
		});
	};

	var _initializeDropdown = function _initializeDropdown() {
		// Select item
		$dropdown.on('select:item', function () {
			$dropdown.find('li[data-search-area=' + searchArea + ']').addClass('active');
		});

		// Show event
		$dropdown.on('show:dropdown', function () {
			$dropdown.fadeIn();
			$dropdown.trigger('select:item');
			$dropdown.trigger('refresh:search-item');
		});

		// Select first item
		$dropdown.on('select:item:first', function () {
			var $activeListItem = $dropdown.find('li.search-item.active');
			var $firstListItem = $dropdown.find('li.search-item:first');
			$activeListItem.removeClass('active');
			$firstListItem.addClass('active');
			_refreshSearchArea();
			$dropdown.trigger('select:item');
		});

		$dropdown.on('select:item:last', function () {
			var $activeListItem = $dropdown.find('li.search-item.active');
			var $lastListItem = $dropdown.find('li.search-item:last');
			$activeListItem.removeClass('active');
			$lastListItem.addClass('active');
			_refreshSearchArea();
			$dropdown.trigger('select:item');
		});

		// Select previous item event
		$dropdown.on('select:item:previous', function () {
			var $activeListItem = $dropdown.find('li.search-item.active');
			var $prev = $activeListItem.prev();

			if ($prev.length) {
				$activeListItem.removeClass('active');
				$prev.addClass('active');
				_refreshSearchArea();
				$dropdown.trigger('select:item');
			} else {
				$dropdown.trigger('select:item:last');
			}
		});

		// Select previous item event
		$dropdown.on('select:item:next', function () {
			var $activeListItem = $dropdown.find('li.search-item.active');
			var $next = $activeListItem.next();

			if ($next.length) {
				$activeListItem.removeClass('active');
				$next.addClass('active');
				_refreshSearchArea();
				$dropdown.trigger('select:item');
			} else {
				$dropdown.trigger('select:item:first');
			}
		});

		// Hide event
		$dropdown.on('hide:dropdown', function () {
			$dropdown.fadeOut();
		});

		// Item click event
		$dropdown.on('click', function (event) {
			event.stopPropagation();

			$dropdown.find('li').removeClass('active');

			var $elementToActivate = $(event.target).is('span') ? $(event.target).parents('li:first') : $(event.target);

			$elementToActivate.addClass('active');

			_refreshSearchArea();
			$dropdown.trigger('hide:dropdown');
			$this.trigger('perform:search');
		});

		// Item search event
		$dropdown.on('refresh:search-item', function () {
			$('.search-item').each(function () {
				// Update search query
				$(this).find('.search-query-item').text($this.val());

				// Update search description
				var searchAreaText = [labels.searchIn, labels[$(this).data('searchArea')]].join(' ');

				$(this).find('.search-query-description').text(searchAreaText);
			});
		});
	};

	var _initializeRecentSearch = function _initializeRecentSearch() {
		$(document).on('JSENGINE_INIT_FINISHED', function () {
			if (recentSearch != '') {
				$this.prop('value', recentSearch);
				$this.focus();
				$dropdown.trigger('show:dropdown');
				$dropdown.trigger('refresh:search-item');
			}
		});
	};

	/**
  * Initialize method of the extension, called by the engine.
  */
	module.init = function (done) {
		_initializeInput();
		_initializeDropdown();
		_initializeButton();
		_initializeRecentSearch();

		searchArea = data.recentSearchArea || 'categories';
		$dropdown.trigger('select:item');

		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkbWluX3NlYXJjaC5qcyJdLCJuYW1lcyI6WyJneCIsImV4dGVuc2lvbnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJGJ1dHRvbiIsImJ1dHRvbiIsIiRkcm9wZG93biIsInJlY2VudFNlYXJjaCIsInRyaW0iLCJkZWNvZGVVUklDb21wb25lbnQiLCJqc2UiLCJsaWJzIiwidXJsX2FyZ3VtZW50cyIsImdldFVybFBhcmFtZXRlcnMiLCJsb2NhdGlvbiIsImhyZWYiLCJzZWFyY2giLCJzZWFyY2hBcmVhIiwibGFiZWxzIiwic2VhcmNoSW4iLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSIsIm9yZGVycyIsImludm9pY2VzIiwiY3VzdG9tZXJzIiwiY2F0ZWdvcmllcyIsImtleU1hcCIsIkVTQyIsIkFSUk9XX1VQIiwiQVJST1dfRE9XTiIsIkVOVEVSIiwidXNlckNvbmZpZ3VyYXRpb25TZXJ2aWNlIiwidXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UiLCJ1cmxBcmd1bWVudHMiLCJjb25maWd1cmF0aW9uQ29udGFpbmVyIiwidXNlcklkIiwiY3VzdG9tZXJfaWQiLCJjb25maWd1cmF0aW9uS2V5IiwiX3JlZnJlc2hTZWFyY2hBcmVhIiwibGVuZ3RoIiwiY29uc29sZSIsImVycm9yIiwidHJpZ2dlciIsIl9pbml0aWFsaXplSW5wdXQiLCJvbiIsInZhbCIsImV2ZW50Iiwid2hpY2giLCJpbnB1dFZhbHVlIiwiZW5jb2RlVVJJQ29tcG9uZW50Iiwib3Blbk1vZGUiLCJ1cmwiLCJqb2luIiwicGFyYW0iLCJkbyIsImZpbHRlciIsIm51bWJlciIsImludm9pY2VOdW1iZXIiLCJsb2FkaW5nX3NwaW5uZXIiLCJzaG93Iiwic2V0IiwiZXh0ZW5kIiwiY29uZmlndXJhdGlvblZhbHVlIiwib25TdWNjZXNzIiwid2luZG93Iiwib3BlbiIsIm9uRXJyb3IiLCJwcm9wIiwiX2luaXRpYWxpemVCdXR0b24iLCJfaW5pdGlhbGl6ZURyb3Bkb3duIiwiZmluZCIsImFkZENsYXNzIiwiZmFkZUluIiwiJGFjdGl2ZUxpc3RJdGVtIiwiJGZpcnN0TGlzdEl0ZW0iLCJyZW1vdmVDbGFzcyIsIiRsYXN0TGlzdEl0ZW0iLCIkcHJldiIsInByZXYiLCIkbmV4dCIsIm5leHQiLCJmYWRlT3V0Iiwic3RvcFByb3BhZ2F0aW9uIiwiJGVsZW1lbnRUb0FjdGl2YXRlIiwidGFyZ2V0IiwiaXMiLCJwYXJlbnRzIiwiZWFjaCIsInRleHQiLCJzZWFyY2hBcmVhVGV4dCIsIl9pbml0aWFsaXplUmVjZW50U2VhcmNoIiwiZG9jdW1lbnQiLCJmb2N1cyIsImluaXQiLCJkb25lIiwicmVjZW50U2VhcmNoQXJlYSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7QUFTQUEsR0FBR0MsVUFBSCxDQUFjQyxNQUFkLENBQ0MsY0FERCxFQUdDLENBQUMsNEJBQUQsRUFBK0IsZUFBL0IsRUFBZ0QsaUJBQWhELENBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUNBLEtBQUlDLFFBQVFDLEVBQUUsSUFBRixDQUFaO0FBQUEsS0FDQ0MsVUFBVUQsRUFBRUYsS0FBS0ksTUFBUCxDQURYO0FBQUEsS0FFQ0MsWUFBWUgsRUFBRSxxQkFBRixDQUZiO0FBQUEsS0FHQ0ksZUFBZUosRUFBRUssSUFBRixDQUFPQyxtQkFBbUJDLElBQUlDLElBQUosQ0FBU0MsYUFBVCxDQUF1QkMsZ0JBQXZCLENBQXdDQyxTQUFTQyxJQUFqRCxFQUF1REMsTUFBdkQsSUFBaUUsRUFBcEYsQ0FBUCxDQUhoQjs7QUFLQTtBQUNBLEtBQUlDLFVBQUo7O0FBRUE7QUFDQSxLQUFJQyxTQUFTO0FBQ1pDLFlBQVVULElBQUlVLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHVCQUF4QixFQUFpRCxjQUFqRCxDQURFO0FBRVpDLFVBQVFiLElBQUlVLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHFCQUF4QixFQUErQyxjQUEvQyxDQUZJO0FBR1pFLFlBQVVkLElBQUlVLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHVCQUF4QixFQUFpRCxjQUFqRCxDQUhFO0FBSVpHLGFBQVdmLElBQUlVLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHdCQUF4QixFQUFrRCxjQUFsRCxDQUpDO0FBS1pJLGNBQVloQixJQUFJVSxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3Qix5QkFBeEIsRUFBbUQsY0FBbkQ7QUFMQSxFQUFiOztBQVFBO0FBQ0EsS0FBSUssU0FBUztBQUNaQyxPQUFLLEVBRE87QUFFWkMsWUFBVSxFQUZFO0FBR1pDLGNBQVksRUFIQTtBQUlaQyxTQUFPO0FBSkssRUFBYjs7QUFPQTtBQUNBLEtBQUlDLDJCQUEyQnRCLElBQUlDLElBQUosQ0FBU3NCLDBCQUF4QztBQUFBLEtBQ0NDLGVBQWV4QixJQUFJQyxJQUFKLENBQVNDLGFBRHpCOztBQUdBO0FBQ0EsS0FBSXVCLHlCQUF5QjtBQUM1QkMsVUFBUW5DLEtBQUtvQyxXQURlO0FBRTVCQyxvQkFBa0I7QUFGVSxFQUE3Qjs7QUFLQTtBQUNBLEtBQUl0QyxTQUFTLEVBQWI7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFNQSxLQUFJdUMscUJBQXFCLFNBQXJCQSxrQkFBcUIsR0FBVztBQUNuQztBQUNBLE1BQUksQ0FBQ3BDLEVBQUUscUJBQUYsRUFBeUJxQyxNQUE5QixFQUFzQztBQUNyQ0MsV0FBUUMsS0FBUixDQUFjLHNCQUFkO0FBQ0E7O0FBRUQ7QUFDQXpCLGVBQWFkLEVBQUUscUJBQUYsRUFBeUJGLElBQXpCLENBQThCLFlBQTlCLENBQWI7QUFDQUMsUUFBTXlDLE9BQU4sQ0FBYyxxQkFBZDtBQUNBLEVBVEQ7O0FBV0E7QUFDQTtBQUNBOztBQUVBLEtBQUlDLG1CQUFtQixTQUFuQkEsZ0JBQW1CLEdBQVc7O0FBRWpDO0FBQ0ExQyxRQUFNMkMsRUFBTixDQUFTLE9BQVQsRUFBa0IsWUFBVztBQUM1QjNDLFNBQU15QyxPQUFOLENBQWMscUJBQWQ7QUFDQSxPQUFHekMsTUFBTTRDLEdBQU4sT0FBZ0IsRUFBbkIsRUFDQTtBQUNDNUMsVUFBTTRDLEdBQU4sQ0FBVXZDLFlBQVY7QUFDQTtBQUNERCxhQUFVcUMsT0FBVixDQUFrQixlQUFsQjtBQUNBekMsU0FBTXlDLE9BQU4sQ0FBYyxPQUFkO0FBQ0EsR0FSRDs7QUFVQTtBQUNBekMsUUFBTTJDLEVBQU4sQ0FBUyxPQUFULEVBQWtCLFVBQVNFLEtBQVQsRUFBZ0I7QUFDakMsV0FBUUEsTUFBTUMsS0FBZDs7QUFFQztBQUNBLFNBQUtyQixPQUFPSSxLQUFaO0FBQ0M3QixXQUFNeUMsT0FBTixDQUFjLGdCQUFkO0FBQ0E7O0FBRUQ7QUFDQSxTQUFLaEIsT0FBT0MsR0FBWjtBQUNDdEIsZUFBVXFDLE9BQVYsQ0FBa0IsZUFBbEI7QUFDQTs7QUFFRDtBQUNBLFNBQUtoQixPQUFPRSxRQUFaO0FBQ0N2QixlQUFVcUMsT0FBVixDQUFrQixzQkFBbEI7QUFDQTtBQUNELFNBQUtoQixPQUFPRyxVQUFaO0FBQ0N4QixlQUFVcUMsT0FBVixDQUFrQixrQkFBbEI7QUFDQTtBQWxCRjtBQW9CQXJDLGFBQVVxQyxPQUFWLENBQWtCLHFCQUFsQjtBQUNBLEdBdEJEOztBQXdCQTtBQUNBekMsUUFBTTJDLEVBQU4sQ0FBUyxnQkFBVCxFQUEyQixZQUFXO0FBQ3JDLE9BQUlJLGFBQWFDLG1CQUFtQmhELE1BQU00QyxHQUFOLEVBQW5CLENBQWpCO0FBQUEsT0FDQ0ssV0FBVyxPQURaO0FBQUEsT0FFQ0MsR0FGRDs7QUFJQSxXQUFRbkMsVUFBUjtBQUNDLFNBQUssV0FBTDtBQUNDbUMsV0FBTSxDQUNMLGVBREssRUFFTCxVQUZLLEVBR0xILFVBSEssRUFJSkksSUFKSSxDQUlDLEVBSkQsQ0FBTjtBQUtBO0FBQ0QsU0FBSyxZQUFMO0FBQ0NELFdBQU0sQ0FDTCxnQkFESyxFQUVMLFVBRkssRUFHTEgsVUFISyxFQUlKSSxJQUpJLENBSUMsRUFKRCxDQUFOO0FBS0E7QUFDRCxTQUFLLFFBQUw7QUFDQ0QsV0FBTSxDQUNMLFdBREssRUFFTCxHQUZLLEVBR0xqRCxFQUFFbUQsS0FBRixDQUFRO0FBQ1BDLFVBQUksZ0JBREc7QUFFUEMsY0FBUTtBQUNQQyxlQUFRUjtBQUREO0FBRkQsTUFBUixDQUhLLEVBU0pJLElBVEksQ0FTQyxFQVRELENBQU47QUFVQTtBQUNELFNBQUssVUFBTDtBQUNDRCxXQUFNLENBQ0wsV0FESyxFQUVMLEdBRkssRUFHTGpELEVBQUVtRCxLQUFGLENBQVE7QUFDUEMsVUFBSSxrQkFERztBQUVQQyxjQUFRO0FBQ1BFLHNCQUFlVDtBQURSO0FBRkQsTUFBUixDQUhLLEVBU0pJLElBVEksQ0FTQyxFQVRELENBQU47QUFVQTtBQXRDRjs7QUF5Q0E7QUFDQTNDLE9BQUlDLElBQUosQ0FBU2dELGVBQVQsQ0FBeUJDLElBQXpCLENBQThCdEQsU0FBOUIsRUFBeUMsTUFBekM7O0FBRUEwQiw0QkFBeUI2QixHQUF6QixDQUE2QjtBQUM1QjVELFVBQU1FLEVBQUUyRCxNQUFGLENBQVMzQixzQkFBVCxFQUFpQztBQUN0QzRCLHlCQUFvQjlDO0FBRGtCLEtBQWpDLENBRHNCO0FBSTVCK0MsZUFBVyxxQkFBVztBQUNyQkMsWUFBT0MsSUFBUCxDQUFZZCxHQUFaLEVBQWlCRCxRQUFqQjtBQUNBLEtBTjJCO0FBTzVCZ0IsYUFBUyxtQkFBVztBQUNuQkYsWUFBT0MsSUFBUCxDQUFZZCxHQUFaLEVBQWlCRCxRQUFqQjtBQUNBO0FBVDJCLElBQTdCO0FBWUEsR0E3REQ7O0FBK0RBO0FBQ0FqRCxRQUFNMkMsRUFBTixDQUFTLHFCQUFULEVBQWdDLFlBQVc7QUFDMUMzQyxTQUFNa0UsSUFBTixDQUFXLGFBQVgsRUFBMEJsRCxPQUFPRCxVQUFQLENBQTFCO0FBQ0EsR0FGRDs7QUFJQTtBQUNBZixRQUFNMkMsRUFBTixDQUFTLE1BQVQsRUFBaUIsWUFBVztBQUMzQjNDLFNBQU1rRSxJQUFOLENBQVcsYUFBWCxFQUEwQixFQUExQjtBQUNBOUQsYUFBVXFDLE9BQVYsQ0FBa0IsZUFBbEI7QUFDQSxHQUhEO0FBSUEsRUFoSEQ7O0FBa0hBLEtBQUkwQixvQkFBb0IsU0FBcEJBLGlCQUFvQixHQUFXO0FBQ2xDakUsVUFBUXlDLEVBQVIsQ0FBVyxPQUFYLEVBQW9CLFlBQVc7QUFDOUIzQyxTQUFNeUMsT0FBTixDQUFjLHFCQUFkO0FBQ0F6QyxTQUFNNEMsR0FBTixDQUFVdkMsWUFBVjtBQUNBRCxhQUFVcUMsT0FBVixDQUFrQixlQUFsQjtBQUNBekMsU0FBTXlDLE9BQU4sQ0FBYyxPQUFkO0FBQ0EsR0FMRDtBQU1BLEVBUEQ7O0FBU0EsS0FBSTJCLHNCQUFzQixTQUF0QkEsbUJBQXNCLEdBQVc7QUFDcEM7QUFDQWhFLFlBQVV1QyxFQUFWLENBQWEsYUFBYixFQUE0QixZQUFXO0FBQ3RDdkMsYUFDRWlFLElBREYsQ0FDTyx5QkFBeUJ0RCxVQUF6QixHQUFzQyxHQUQ3QyxFQUVFdUQsUUFGRixDQUVXLFFBRlg7QUFHQSxHQUpEOztBQU1BO0FBQ0FsRSxZQUFVdUMsRUFBVixDQUFhLGVBQWIsRUFBOEIsWUFBVztBQUN4Q3ZDLGFBQVVtRSxNQUFWO0FBQ0FuRSxhQUFVcUMsT0FBVixDQUFrQixhQUFsQjtBQUNBckMsYUFBVXFDLE9BQVYsQ0FBa0IscUJBQWxCO0FBRUEsR0FMRDs7QUFPQTtBQUNBckMsWUFBVXVDLEVBQVYsQ0FBYSxtQkFBYixFQUFrQyxZQUFXO0FBQzVDLE9BQUk2QixrQkFBa0JwRSxVQUFVaUUsSUFBVixDQUFlLHVCQUFmLENBQXRCO0FBQ0EsT0FBSUksaUJBQWlCckUsVUFBVWlFLElBQVYsQ0FBZSxzQkFBZixDQUFyQjtBQUNBRyxtQkFBZ0JFLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0FELGtCQUFlSCxRQUFmLENBQXdCLFFBQXhCO0FBQ0FqQztBQUNBakMsYUFBVXFDLE9BQVYsQ0FBa0IsYUFBbEI7QUFDQSxHQVBEOztBQVNBckMsWUFBVXVDLEVBQVYsQ0FBYSxrQkFBYixFQUFpQyxZQUFXO0FBQzNDLE9BQUk2QixrQkFBa0JwRSxVQUFVaUUsSUFBVixDQUFlLHVCQUFmLENBQXRCO0FBQ0EsT0FBSU0sZ0JBQWdCdkUsVUFBVWlFLElBQVYsQ0FBZSxxQkFBZixDQUFwQjtBQUNBRyxtQkFBZ0JFLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0FDLGlCQUFjTCxRQUFkLENBQXVCLFFBQXZCO0FBQ0FqQztBQUNBakMsYUFBVXFDLE9BQVYsQ0FBa0IsYUFBbEI7QUFDQSxHQVBEOztBQVNBO0FBQ0FyQyxZQUFVdUMsRUFBVixDQUFhLHNCQUFiLEVBQXFDLFlBQVc7QUFDL0MsT0FBSTZCLGtCQUFrQnBFLFVBQVVpRSxJQUFWLENBQWUsdUJBQWYsQ0FBdEI7QUFDQSxPQUFJTyxRQUFRSixnQkFBZ0JLLElBQWhCLEVBQVo7O0FBRUEsT0FBSUQsTUFBTXRDLE1BQVYsRUFBa0I7QUFDakJrQyxvQkFBZ0JFLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0FFLFVBQU1OLFFBQU4sQ0FBZSxRQUFmO0FBQ0FqQztBQUNBakMsY0FBVXFDLE9BQVYsQ0FBa0IsYUFBbEI7QUFDQSxJQUxELE1BS087QUFDTnJDLGNBQVVxQyxPQUFWLENBQWtCLGtCQUFsQjtBQUNBO0FBQ0QsR0FaRDs7QUFjQTtBQUNBckMsWUFBVXVDLEVBQVYsQ0FBYSxrQkFBYixFQUFpQyxZQUFXO0FBQzNDLE9BQUk2QixrQkFBa0JwRSxVQUFVaUUsSUFBVixDQUFlLHVCQUFmLENBQXRCO0FBQ0EsT0FBSVMsUUFBUU4sZ0JBQWdCTyxJQUFoQixFQUFaOztBQUVBLE9BQUlELE1BQU14QyxNQUFWLEVBQWtCO0FBQ2pCa0Msb0JBQWdCRSxXQUFoQixDQUE0QixRQUE1QjtBQUNBSSxVQUFNUixRQUFOLENBQWUsUUFBZjtBQUNBakM7QUFDQWpDLGNBQVVxQyxPQUFWLENBQWtCLGFBQWxCO0FBQ0EsSUFMRCxNQUtPO0FBQ05yQyxjQUFVcUMsT0FBVixDQUFrQixtQkFBbEI7QUFDQTtBQUNELEdBWkQ7O0FBY0E7QUFDQXJDLFlBQVV1QyxFQUFWLENBQWEsZUFBYixFQUE4QixZQUFXO0FBQ3hDdkMsYUFBVTRFLE9BQVY7QUFDQSxHQUZEOztBQUlBO0FBQ0E1RSxZQUFVdUMsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBU0UsS0FBVCxFQUFnQjtBQUNyQ0EsU0FBTW9DLGVBQU47O0FBRUE3RSxhQUNFaUUsSUFERixDQUNPLElBRFAsRUFFRUssV0FGRixDQUVjLFFBRmQ7O0FBSUEsT0FBSVEscUJBQXFCakYsRUFBRTRDLE1BQU1zQyxNQUFSLEVBQWdCQyxFQUFoQixDQUFtQixNQUFuQixJQUNBbkYsRUFBRTRDLE1BQU1zQyxNQUFSLEVBQWdCRSxPQUFoQixDQUF3QixVQUF4QixDQURBLEdBRUFwRixFQUFFNEMsTUFBTXNDLE1BQVIsQ0FGekI7O0FBSUFELHNCQUFtQlosUUFBbkIsQ0FBNEIsUUFBNUI7O0FBRUFqQztBQUNBakMsYUFBVXFDLE9BQVYsQ0FBa0IsZUFBbEI7QUFDQXpDLFNBQU15QyxPQUFOLENBQWMsZ0JBQWQ7QUFDQSxHQWhCRDs7QUFrQkE7QUFDQXJDLFlBQVV1QyxFQUFWLENBQWEscUJBQWIsRUFBb0MsWUFBVztBQUM5QzFDLEtBQUUsY0FBRixFQUFrQnFGLElBQWxCLENBQXVCLFlBQVc7QUFDakM7QUFDQXJGLE1BQUUsSUFBRixFQUNFb0UsSUFERixDQUNPLG9CQURQLEVBRUVrQixJQUZGLENBRU92RixNQUFNNEMsR0FBTixFQUZQOztBQUlBO0FBQ0EsUUFBSTRDLGlCQUFpQixDQUNwQnhFLE9BQU9DLFFBRGEsRUFFcEJELE9BQU9mLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsWUFBYixDQUFQLENBRm9CLEVBR25Cb0QsSUFIbUIsQ0FHZCxHQUhjLENBQXJCOztBQUtBbEQsTUFBRSxJQUFGLEVBQ0VvRSxJQURGLENBQ08sMkJBRFAsRUFFRWtCLElBRkYsQ0FFT0MsY0FGUDtBQUdBLElBZkQ7QUFnQkEsR0FqQkQ7QUFrQkEsRUE1R0Q7O0FBOEdBLEtBQUlDLDBCQUEwQixTQUExQkEsdUJBQTBCLEdBQVc7QUFDeEN4RixJQUFFeUYsUUFBRixFQUFZL0MsRUFBWixDQUFlLHdCQUFmLEVBQXlDLFlBQVc7QUFDbkQsT0FBSXRDLGdCQUFnQixFQUFwQixFQUF3QjtBQUN2QkwsVUFBTWtFLElBQU4sQ0FBVyxPQUFYLEVBQW9CN0QsWUFBcEI7QUFDQUwsVUFBTTJGLEtBQU47QUFDQXZGLGNBQVVxQyxPQUFWLENBQWtCLGVBQWxCO0FBQ0FyQyxjQUFVcUMsT0FBVixDQUFrQixxQkFBbEI7QUFDQTtBQUNELEdBUEQ7QUFRQSxFQVREOztBQVdBOzs7QUFHQTNDLFFBQU84RixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCbkQ7QUFDQTBCO0FBQ0FEO0FBQ0FzQjs7QUFFQTFFLGVBQWFoQixLQUFLK0YsZ0JBQUwsSUFBeUIsWUFBdEM7QUFDQTFGLFlBQVVxQyxPQUFWLENBQWtCLGFBQWxCOztBQUVBb0Q7QUFDQSxFQVZEOztBQVlBO0FBQ0EsUUFBTy9GLE1BQVA7QUFDQSxDQWxWRiIsImZpbGUiOiJhZG1pbl9zZWFyY2guanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGFkbWluX3NlYXJjaC5qcyAyMDE3LTAxLTEwXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBBZG1pbiBTZWFyY2ggRXh0ZW5zaW9uXG4gKlxuICogRXh0ZW5zaW9uIGZvciBzZWFyY2ggaW4gb3JkZXJzLCBjdXN0b21lcnMgYW5kIGNhdGVnb3JpZXMgaW4gdGhlIGFkbWluIHBhbmVsXG4gKlxuICogQG1vZHVsZSBBZG1pbi9FeHRlbnNpb24vYWRtaW5fc2VhcmNoXG4gKiBAcmVxdWlyZXMgalF1ZXJ5VUlcbiAqIEBpZ25vcmVcbiAqL1xuZ3guZXh0ZW5zaW9ucy5tb2R1bGUoXG5cdCdhZG1pbl9zZWFyY2gnLFxuXHRcblx0Wyd1c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZScsICd1cmxfYXJndW1lbnRzJywgJ2xvYWRpbmdfc3Bpbm5lciddLFxuXG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8vIEVsZW1lbnRzLlxuXHRcdHZhciAkdGhpcyA9ICQodGhpcyksXG5cdFx0XHQkYnV0dG9uID0gJChkYXRhLmJ1dHRvbiksXG5cdFx0XHQkZHJvcGRvd24gPSAkKCd1bC5zZWFyY2hhYmxlOmZpcnN0JyksXG5cdFx0XHRyZWNlbnRTZWFyY2ggPSAkLnRyaW0oZGVjb2RlVVJJQ29tcG9uZW50KGpzZS5saWJzLnVybF9hcmd1bWVudHMuZ2V0VXJsUGFyYW1ldGVycyhsb2NhdGlvbi5ocmVmKS5zZWFyY2ggfHwgJycpKTtcblx0XHRcblx0XHQvLyBDdXJyZW50IHNlYXJjaCBhcmVhLlxuXHRcdHZhciBzZWFyY2hBcmVhO1xuXHRcdFxuXHRcdC8vIFRleHQgbGFiZWxzLlxuXHRcdHZhciBsYWJlbHMgPSB7XG5cdFx0XHRzZWFyY2hJbjoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2FkbWluX3NlYXJjaF9pbl9sYWJlbCcsICdhZG1pbl9sYWJlbHMnKSxcblx0XHRcdG9yZGVyczoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2FkbWluX3NlYXJjaF9vcmRlcnMnLCAnYWRtaW5fbGFiZWxzJyksXG5cdFx0XHRpbnZvaWNlczoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2FkbWluX3NlYXJjaF9pbnZvaWNlcycsICdhZG1pbl9sYWJlbHMnKSxcblx0XHRcdGN1c3RvbWVyczoganNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2FkbWluX3NlYXJjaF9jdXN0b21lcnMnLCAnYWRtaW5fbGFiZWxzJyksXG5cdFx0XHRjYXRlZ29yaWVzOiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnYWRtaW5fc2VhcmNoX2NhdGVnb3JpZXMnLCAnYWRtaW5fbGFiZWxzJyksXG5cdFx0fTtcblx0XHRcblx0XHQvLyBLZXkgY29kZSBtYXAuXG5cdFx0dmFyIGtleU1hcCA9IHtcblx0XHRcdEVTQzogMjcsXG5cdFx0XHRBUlJPV19VUDogMzgsXG5cdFx0XHRBUlJPV19ET1dOOiA0MCxcblx0XHRcdEVOVEVSOiAxM1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gTGlicmFyeSBhY2Nlc3Mgc2hvcnRjdXRzLlxuXHRcdHZhciB1c2VyQ29uZmlndXJhdGlvblNlcnZpY2UgPSBqc2UubGlicy51c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSxcblx0XHRcdHVybEFyZ3VtZW50cyA9IGpzZS5saWJzLnVybF9hcmd1bWVudHM7XG5cdFx0XG5cdFx0Ly8gQ29uZmlndXJhdGlvbiBzZXR0aW5ncyBmb3IgVXNlckNvbmZpZ3VyYXRpb25TZXJ2aWNlLlxuXHRcdHZhciBjb25maWd1cmF0aW9uQ29udGFpbmVyID0ge1xuXHRcdFx0dXNlcklkOiBkYXRhLmN1c3RvbWVyX2lkLFxuXHRcdFx0Y29uZmlndXJhdGlvbktleTogJ3JlY2VudF9zZWFyY2hfYXJlYSdcblx0XHR9O1xuXHRcdFxuXHRcdC8vIE1vZHVsZSBvYmplY3QgKEpTRW5naW5lKS5cblx0XHR2YXIgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gTUVUSE9EU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlZnJlc2hlcyB0aGUgc2VhcmNoIGFyZWEgdmFyaWFibGVcblx0XHQgKlxuXHRcdCAqIFNob3dzIHRoZSBuZXcgc2VhcmNoIGFyZWEgaW4gdGhlIGJ1dHRvblxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0dmFyIF9yZWZyZXNoU2VhcmNoQXJlYSA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0Ly8gQWJvcnQgaWYgbm8gbmV3IHNlYXJjaCBhcmVhIGlzIHByb3ZpZGVkXG5cdFx0XHRpZiAoISQoJy5zZWFyY2gtaXRlbS5hY3RpdmUnKS5sZW5ndGgpIHtcblx0XHRcdFx0Y29uc29sZS5lcnJvcignTm8gYWN0aXZlIGxpc3QgaXRlbSEnKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gQXNzaWduIG5ldyBzZWFyY2ggYXJlYVxuXHRcdFx0c2VhcmNoQXJlYSA9ICQoJy5zZWFyY2gtaXRlbS5hY3RpdmUnKS5kYXRhKCdzZWFyY2hBcmVhJyk7XG5cdFx0XHQkdGhpcy50cmlnZ2VyKCdyZWZyZXNoOnNlYXJjaC1hcmVhJyk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhciBfaW5pdGlhbGl6ZUlucHV0ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcblx0XHRcdC8vIENsaWNrIGV2ZW50XG5cdFx0XHQkdGhpcy5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0JHRoaXMudHJpZ2dlcigncmVmcmVzaDpzZWFyY2gtYXJlYScpO1xuXHRcdFx0XHRpZigkdGhpcy52YWwoKSA9PT0gJycpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQkdGhpcy52YWwocmVjZW50U2VhcmNoKTtcblx0XHRcdFx0fVxuXHRcdFx0XHQkZHJvcGRvd24udHJpZ2dlcignc2hvdzpkcm9wZG93bicpO1xuXHRcdFx0XHQkdGhpcy50cmlnZ2VyKCdmb2N1cycpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIEtleWJvYXJkIGV2ZW50c1xuXHRcdFx0JHRoaXMub24oJ2tleXVwJywgZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRcdFx0c3dpdGNoIChldmVudC53aGljaCkge1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFBlcmZvcm0gc2VhcmNoIGlmIGVudGVyIGtleSBpcyBwcmVzc2VkXG5cdFx0XHRcdFx0Y2FzZSBrZXlNYXAuRU5URVI6XG5cdFx0XHRcdFx0XHQkdGhpcy50cmlnZ2VyKCdwZXJmb3JtOnNlYXJjaCcpO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gQ2xvc2UgZHJvcGRvd24gaWYgZXNjYXBlIGtleSBpcyBwcmVzc2VkXG5cdFx0XHRcdFx0Y2FzZSBrZXlNYXAuRVNDOlxuXHRcdFx0XHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ2hpZGU6ZHJvcGRvd24nKTtcblx0XHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBOYXZpZ2F0ZSB1cCBpbiBkcm9wZG93blxuXHRcdFx0XHRcdGNhc2Uga2V5TWFwLkFSUk9XX1VQOlxuXHRcdFx0XHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ3NlbGVjdDppdGVtOnByZXZpb3VzJyk7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlIGtleU1hcC5BUlJPV19ET1dOOlxuXHRcdFx0XHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ3NlbGVjdDppdGVtOm5leHQnKTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHRcdCRkcm9wZG93bi50cmlnZ2VyKCdyZWZyZXNoOnNlYXJjaC1pdGVtJyk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gU2VhcmNoIGV2ZW50c1xuXHRcdFx0JHRoaXMub24oJ3BlcmZvcm06c2VhcmNoJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHZhciBpbnB1dFZhbHVlID0gZW5jb2RlVVJJQ29tcG9uZW50KCR0aGlzLnZhbCgpKSxcblx0XHRcdFx0XHRvcGVuTW9kZSA9ICdfc2VsZicsXG5cdFx0XHRcdFx0dXJsO1xuXHRcdFx0XHRcblx0XHRcdFx0c3dpdGNoIChzZWFyY2hBcmVhKSB7XG5cdFx0XHRcdFx0Y2FzZSAnY3VzdG9tZXJzJzpcblx0XHRcdFx0XHRcdHVybCA9IFtcblx0XHRcdFx0XHRcdFx0J2N1c3RvbWVycy5waHAnLFxuXHRcdFx0XHRcdFx0XHQnP3NlYXJjaD0nLFxuXHRcdFx0XHRcdFx0XHRpbnB1dFZhbHVlXG5cdFx0XHRcdFx0XHRdLmpvaW4oJycpO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0Y2FzZSAnY2F0ZWdvcmllcyc6XG5cdFx0XHRcdFx0XHR1cmwgPSBbXG5cdFx0XHRcdFx0XHRcdCdjYXRlZ29yaWVzLnBocCcsXG5cdFx0XHRcdFx0XHRcdCc/c2VhcmNoPScsXG5cdFx0XHRcdFx0XHRcdGlucHV0VmFsdWVcblx0XHRcdFx0XHRcdF0uam9pbignJyk7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlICdvcmRlcnMnOlxuXHRcdFx0XHRcdFx0dXJsID0gW1xuXHRcdFx0XHRcdFx0XHQnYWRtaW4ucGhwJyxcblx0XHRcdFx0XHRcdFx0Jz8nLFxuXHRcdFx0XHRcdFx0XHQkLnBhcmFtKHtcblx0XHRcdFx0XHRcdFx0XHRkbzogJ09yZGVyc092ZXJ2aWV3Jyxcblx0XHRcdFx0XHRcdFx0XHRmaWx0ZXI6IHtcblx0XHRcdFx0XHRcdFx0XHRcdG51bWJlcjogaW5wdXRWYWx1ZVxuXHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdF0uam9pbignJyk7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlICdpbnZvaWNlcyc6XG5cdFx0XHRcdFx0XHR1cmwgPSBbXG5cdFx0XHRcdFx0XHRcdCdhZG1pbi5waHAnLFxuXHRcdFx0XHRcdFx0XHQnPycsXG5cdFx0XHRcdFx0XHRcdCQucGFyYW0oe1xuXHRcdFx0XHRcdFx0XHRcdGRvOiAnSW52b2ljZXNPdmVydmlldycsXG5cdFx0XHRcdFx0XHRcdFx0ZmlsdGVyOiB7XG5cdFx0XHRcdFx0XHRcdFx0XHRpbnZvaWNlTnVtYmVyOiBpbnB1dFZhbHVlXG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XS5qb2luKCcnKTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBEaXNwbGF5IGxvYWRpbmcgc3Bpbm5lci4gXG5cdFx0XHRcdGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5zaG93KCRkcm9wZG93biwgJzk5OTknKTtcblx0XHRcdFx0XG5cdFx0XHRcdHVzZXJDb25maWd1cmF0aW9uU2VydmljZS5zZXQoe1xuXHRcdFx0XHRcdGRhdGE6ICQuZXh0ZW5kKGNvbmZpZ3VyYXRpb25Db250YWluZXIsIHtcblx0XHRcdFx0XHRcdGNvbmZpZ3VyYXRpb25WYWx1ZTogc2VhcmNoQXJlYVxuXHRcdFx0XHRcdH0pLFxuXHRcdFx0XHRcdG9uU3VjY2VzczogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHR3aW5kb3cub3Blbih1cmwsIG9wZW5Nb2RlKTtcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdG9uRXJyb3I6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0d2luZG93Lm9wZW4odXJsLCBvcGVuTW9kZSk7XHRcdFxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIENoYW5nZSBzZWFyY2ggYXJlYSBldmVudFxuXHRcdFx0JHRoaXMub24oJ3JlZnJlc2g6c2VhcmNoLWFyZWEnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0JHRoaXMucHJvcCgncGxhY2Vob2xkZXInLCBsYWJlbHNbc2VhcmNoQXJlYV0pO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIFJlbW92ZSBwbGFjZWhvbGRlciB3aGVuIGlucHV0IGlzIGluYWN0aXZlXG5cdFx0XHQkdGhpcy5vbignYmx1cicsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkdGhpcy5wcm9wKCdwbGFjZWhvbGRlcicsICcnKTtcblx0XHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ2hpZGU6ZHJvcGRvd24nKTtcblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9pbml0aWFsaXplQnV0dG9uID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQkYnV0dG9uLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkdGhpcy50cmlnZ2VyKCdyZWZyZXNoOnNlYXJjaC1hcmVhJyk7XG5cdFx0XHRcdCR0aGlzLnZhbChyZWNlbnRTZWFyY2gpO1xuXHRcdFx0XHQkZHJvcGRvd24udHJpZ2dlcignc2hvdzpkcm9wZG93bicpO1xuXHRcdFx0XHQkdGhpcy50cmlnZ2VyKCdmb2N1cycpO1xuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHR2YXIgX2luaXRpYWxpemVEcm9wZG93biA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0Ly8gU2VsZWN0IGl0ZW1cblx0XHRcdCRkcm9wZG93bi5vbignc2VsZWN0Oml0ZW0nLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0JGRyb3Bkb3duXG5cdFx0XHRcdFx0LmZpbmQoJ2xpW2RhdGEtc2VhcmNoLWFyZWE9JyArIHNlYXJjaEFyZWEgKyAnXScpXG5cdFx0XHRcdFx0LmFkZENsYXNzKCdhY3RpdmUnKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBTaG93IGV2ZW50XG5cdFx0XHQkZHJvcGRvd24ub24oJ3Nob3c6ZHJvcGRvd24nLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0JGRyb3Bkb3duLmZhZGVJbigpO1xuXHRcdFx0XHQkZHJvcGRvd24udHJpZ2dlcignc2VsZWN0Oml0ZW0nKTtcblx0XHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ3JlZnJlc2g6c2VhcmNoLWl0ZW0nKTtcblx0XHRcdFx0XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gU2VsZWN0IGZpcnN0IGl0ZW1cblx0XHRcdCRkcm9wZG93bi5vbignc2VsZWN0Oml0ZW06Zmlyc3QnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyICRhY3RpdmVMaXN0SXRlbSA9ICRkcm9wZG93bi5maW5kKCdsaS5zZWFyY2gtaXRlbS5hY3RpdmUnKTtcblx0XHRcdFx0dmFyICRmaXJzdExpc3RJdGVtID0gJGRyb3Bkb3duLmZpbmQoJ2xpLnNlYXJjaC1pdGVtOmZpcnN0Jyk7XG5cdFx0XHRcdCRhY3RpdmVMaXN0SXRlbS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdCRmaXJzdExpc3RJdGVtLmFkZENsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0X3JlZnJlc2hTZWFyY2hBcmVhKCk7XG5cdFx0XHRcdCRkcm9wZG93bi50cmlnZ2VyKCdzZWxlY3Q6aXRlbScpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdCRkcm9wZG93bi5vbignc2VsZWN0Oml0ZW06bGFzdCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgJGFjdGl2ZUxpc3RJdGVtID0gJGRyb3Bkb3duLmZpbmQoJ2xpLnNlYXJjaC1pdGVtLmFjdGl2ZScpO1xuXHRcdFx0XHR2YXIgJGxhc3RMaXN0SXRlbSA9ICRkcm9wZG93bi5maW5kKCdsaS5zZWFyY2gtaXRlbTpsYXN0Jyk7XG5cdFx0XHRcdCRhY3RpdmVMaXN0SXRlbS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdCRsYXN0TGlzdEl0ZW0uYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHRfcmVmcmVzaFNlYXJjaEFyZWEoKTtcblx0XHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ3NlbGVjdDppdGVtJyk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gU2VsZWN0IHByZXZpb3VzIGl0ZW0gZXZlbnRcblx0XHRcdCRkcm9wZG93bi5vbignc2VsZWN0Oml0ZW06cHJldmlvdXMnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyICRhY3RpdmVMaXN0SXRlbSA9ICRkcm9wZG93bi5maW5kKCdsaS5zZWFyY2gtaXRlbS5hY3RpdmUnKTtcblx0XHRcdFx0dmFyICRwcmV2ID0gJGFjdGl2ZUxpc3RJdGVtLnByZXYoKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICgkcHJldi5sZW5ndGgpIHtcblx0XHRcdFx0XHQkYWN0aXZlTGlzdEl0ZW0ucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHRcdCRwcmV2LmFkZENsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0XHRfcmVmcmVzaFNlYXJjaEFyZWEoKTtcblx0XHRcdFx0XHQkZHJvcGRvd24udHJpZ2dlcignc2VsZWN0Oml0ZW0nKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHQkZHJvcGRvd24udHJpZ2dlcignc2VsZWN0Oml0ZW06bGFzdCcpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gU2VsZWN0IHByZXZpb3VzIGl0ZW0gZXZlbnRcblx0XHRcdCRkcm9wZG93bi5vbignc2VsZWN0Oml0ZW06bmV4dCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgJGFjdGl2ZUxpc3RJdGVtID0gJGRyb3Bkb3duLmZpbmQoJ2xpLnNlYXJjaC1pdGVtLmFjdGl2ZScpO1xuXHRcdFx0XHR2YXIgJG5leHQgPSAkYWN0aXZlTGlzdEl0ZW0ubmV4dCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKCRuZXh0Lmxlbmd0aCkge1xuXHRcdFx0XHRcdCRhY3RpdmVMaXN0SXRlbS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdFx0JG5leHQuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHRcdF9yZWZyZXNoU2VhcmNoQXJlYSgpO1xuXHRcdFx0XHRcdCRkcm9wZG93bi50cmlnZ2VyKCdzZWxlY3Q6aXRlbScpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdCRkcm9wZG93bi50cmlnZ2VyKCdzZWxlY3Q6aXRlbTpmaXJzdCcpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gSGlkZSBldmVudFxuXHRcdFx0JGRyb3Bkb3duLm9uKCdoaWRlOmRyb3Bkb3duJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCRkcm9wZG93bi5mYWRlT3V0KCk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gSXRlbSBjbGljayBldmVudFxuXHRcdFx0JGRyb3Bkb3duLm9uKCdjbGljaycsIGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XHRcblx0XHRcdFx0JGRyb3Bkb3duXG5cdFx0XHRcdFx0LmZpbmQoJ2xpJylcblx0XHRcdFx0XHQucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHRcblx0XHRcdFx0dmFyICRlbGVtZW50VG9BY3RpdmF0ZSA9ICQoZXZlbnQudGFyZ2V0KS5pcygnc3BhbicpID9cblx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCdsaTpmaXJzdCcpIDpcblx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICQoZXZlbnQudGFyZ2V0KTtcblx0XHRcdFx0XG5cdFx0XHRcdCRlbGVtZW50VG9BY3RpdmF0ZS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRfcmVmcmVzaFNlYXJjaEFyZWEoKTtcblx0XHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ2hpZGU6ZHJvcGRvd24nKTtcblx0XHRcdFx0JHRoaXMudHJpZ2dlcigncGVyZm9ybTpzZWFyY2gnKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBJdGVtIHNlYXJjaCBldmVudFxuXHRcdFx0JGRyb3Bkb3duLm9uKCdyZWZyZXNoOnNlYXJjaC1pdGVtJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCQoJy5zZWFyY2gtaXRlbScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0Ly8gVXBkYXRlIHNlYXJjaCBxdWVyeVxuXHRcdFx0XHRcdCQodGhpcylcblx0XHRcdFx0XHRcdC5maW5kKCcuc2VhcmNoLXF1ZXJ5LWl0ZW0nKVxuXHRcdFx0XHRcdFx0LnRleHQoJHRoaXMudmFsKCkpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFVwZGF0ZSBzZWFyY2ggZGVzY3JpcHRpb25cblx0XHRcdFx0XHR2YXIgc2VhcmNoQXJlYVRleHQgPSBbXG5cdFx0XHRcdFx0XHRsYWJlbHMuc2VhcmNoSW4sXG5cdFx0XHRcdFx0XHRsYWJlbHNbJCh0aGlzKS5kYXRhKCdzZWFyY2hBcmVhJyldXG5cdFx0XHRcdFx0XS5qb2luKCcgJyk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JCh0aGlzKVxuXHRcdFx0XHRcdFx0LmZpbmQoJy5zZWFyY2gtcXVlcnktZGVzY3JpcHRpb24nKVxuXHRcdFx0XHRcdFx0LnRleHQoc2VhcmNoQXJlYVRleHQpO1xuXHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0dmFyIF9pbml0aWFsaXplUmVjZW50U2VhcmNoID0gZnVuY3Rpb24oKSB7XG5cdFx0XHQkKGRvY3VtZW50KS5vbignSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRpZiAocmVjZW50U2VhcmNoICE9ICcnKSB7XG5cdFx0XHRcdFx0JHRoaXMucHJvcCgndmFsdWUnLCByZWNlbnRTZWFyY2gpO1xuXHRcdFx0XHRcdCR0aGlzLmZvY3VzKCk7XG5cdFx0XHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ3Nob3c6ZHJvcGRvd24nKTtcblx0XHRcdFx0XHQkZHJvcGRvd24udHJpZ2dlcigncmVmcmVzaDpzZWFyY2gtaXRlbScpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSBleHRlbnNpb24sIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0X2luaXRpYWxpemVJbnB1dCgpO1xuXHRcdFx0X2luaXRpYWxpemVEcm9wZG93bigpO1xuXHRcdFx0X2luaXRpYWxpemVCdXR0b24oKTtcblx0XHRcdF9pbml0aWFsaXplUmVjZW50U2VhcmNoKCk7XG5cblx0XHRcdHNlYXJjaEFyZWEgPSBkYXRhLnJlY2VudFNlYXJjaEFyZWEgfHwgJ2NhdGVnb3JpZXMnO1xuXHRcdFx0JGRyb3Bkb3duLnRyaWdnZXIoJ3NlbGVjdDppdGVtJyk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIFJldHVybiBkYXRhIHRvIG1vZHVsZSBlbmdpbmUuXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
