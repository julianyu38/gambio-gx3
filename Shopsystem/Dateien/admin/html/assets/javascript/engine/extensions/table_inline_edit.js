'use strict';

/* --------------------------------------------------------------
 table_inline_edit.js 2015-10-16 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Extension for making tables editable.
 *
 * @module Admin/Extensions/table_inline_edit
 * @ignore
 */
gx.extensions.module('table_inline_edit', ['form', 'xhr', 'fallback'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Extension Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Template Selector
  *
  * @type {object}
  */
	$template = null,


	/**
  * Table Body Selector
  *
  * @type {object}
  */
	$table_body = null,


	/**
  * Default Options for Extension
  *
  * @type {object}
  */
	defaults = {
		'multiEdit': false
	},


	/**
  * Final Extension Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONALITY
	// ------------------------------------------------------------------------

	/**
  * Switch State
  *
  * Function that enables / disables, depending on the mode, all input fields inside
  * the $element and shows / hides the corresponding buttons.
  *
  * @param {string} mode Set the given mode. Possible values: 'edit', 'add', 'default'
  * @param {object} $element The element jQuery selection that gets modified
  * @param {boolean} addClass If true, the state class gets added to the element
  */
	var _switchState = function _switchState(mode, $element, addClass) {

		var $targets = $element.find('input, textarea, select, button, i'),
		    $edit = $targets.filter('.editmode'),
		    $add = $targets.filter('.addmode'),
		    $default = $targets.filter('.defaultmode'),
		    $others = $targets.filter(':not(.editmode):not(.addmode):not(.defaultmode)');

		// Hide all buttons
		$edit.hide();
		$add.hide();
		$default.hide();

		// Remove alt-text if available
		$element.find('.table_inlineedit_alt').remove();

		switch (mode) {
			case 'edit':
				// Switch to edit mode
				$edit.show();
				$others.prop('disabled', false);
				break;
			case 'add':
				// Switch to add mode
				$add.show();
				$others.prop('disabled', false);
				break;
			default:
				// Switch to default-mode
				$default.show();
				$others.prop('disabled', true).each(function () {
					// Check if there is an alt text given for the input field
					var $self = $(this),
					    dataset = jse.libs.fallback._data($self, 'table_inline_edit');

					// Replace some kind of form fields with span tags
					if ($self.attr('type') && $self.attr('type').toLowerCase() === 'checkbox' && dataset.alt) {
						var values = dataset.alt.split('_'),
						    checked = $self.prop('checked');
						$self.after('<span class="table_inlineedit_alt">' + (checked ? values[0] : values[1]) + '</span>');
					} else if ($self.prop('tagName').toLowerCase() === 'select') {
						var waitUntilValues = function waitUntilValues() {
							$edit.hide();
							if (!$self.children().length) {
								setTimeout(function () {
									waitUntilValues();
								}, 200);
							} else {
								$self.children('[value="' + $self.val() + '"]').text();
								$self.after('<span class="table_inlineedit_alt">' + $self.children('[value="' + $self.val() + '"]').text() + '</span>');
								return;
							}
						};

						waitUntilValues();
					}
				});
				break;
		}

		$this.trigger('FORM_UPDATE', []);

		// Add the mode class
		if (addClass) {
			$element.removeClass('edit add default').addClass(mode);
		}
	};

	/**
  * Create New Line
  *
  * Creates a new "add"-line by cloning the footer template.
  */
	var _createNewLine = function _createNewLine() {
		var $newLine = $template.clone();

		$newLine.find('[name]').each(function () {
			var $self = $(this);

			$self.attr('name', $self.attr('name').replace('[]', '[0]'));
		});

		_switchState('add', $newLine, true);
		// Rename the temporarily widget data attributes
		jse.libs.fallback.setupWidgetAttr($newLine);
		$table_body.append($newLine);
		// Start the widgets
		gx.widgets.init($table_body.find('tr').last());
		gx.extensions.init($table_body.find('tr').last());
		gx.controllers.init($table_body.find('tr').last());
		gx.compatibility.init($table_body.find('tr').last());
		jse.widgets.init($table_body.find('tr').last());
		jse.extensions.init($table_body.find('tr').last());
	};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Handler for the "abort"-button
  *
  * @returns {boolean} If function gets called directly, the return value is the state of the abort.
  */
	var _abortHandler = function _abortHandler() {
		var $tr = $(this).closest('tr'),
		    cache = JSON.stringify($tr.data('formcache')),
		    current = JSON.stringify(jse.libs.form.getData($tr, undefined, true)),
		    deferred = $.Deferred();

		/**
   * Helper function to reset a line state
   *
   * @private
   */
		var _resetLine = function _resetLine(e) {
			if (e) {
				$('#lightbox_package_' + e.data.id + 'admin_button').off();
				$('#lightbox_package_' + e.data.id);
				$.lightbox_plugin('close', e.data.id);
			}

			if (e && e.data.reject) {
				deferred.reject();
			} else {
				// Reset the validation state
				$tr.trigger('validator.reset', []);
				// Reset the form data
				jse.libs.form.prefillForm($tr, $tr.data('formcache'), true);
				_switchState('default', $tr, true);
				deferred.resolve();
			}
		};

		// Compare the old with the new data. If changes were made, confirm the abort
		if (cache !== current) {
			var href = 'lightbox_confirm.html?section=shop_offline&amp;' + 'message=dicard_changes_hint&amp;buttons=cancel-discard',
			    linkHtml = '<a href="' + href + '"></a>',
			    lightboxLink = $(linkHtml),
			    lightboxId = lightboxLink.lightbox_plugin({
				'lightbox_width': '360px'
			});

			$('#lightbox_package_' + lightboxId).one('click', '.discard', {
				'reject': false,
				'id': lightboxId
			}, _resetLine).one('click', '.cancel', {
				'reject': true,
				'id': lightboxId
			}, _resetLine);
		} else {
			_resetLine();
		}

		return deferred.promise();
	};

	/**
  * Handler for the "edit"-button
  */
	var _editHandler = function _editHandler() {
		var $tr = $(this).closest('tr'),
		    $edited = $this.find('tr.edit'),
		    promises = [];

		if (!options.multiEdit && $edited.length) {
			// If multiEdit is disabled and other lines are in edit mode, wait for confirmation
			// of the abort event on the other lines.
			$edited.each(function () {
				promises.push(_abortHandler.call($(this).find('.row_abort').first()));
			});
		}

		$.when.apply(undefined, promises).promise().done(function () {
			// Store the current data of the line in cache
			$tr.data('formcache', jse.libs.form.getData($tr, undefined, true));
			_switchState('edit', $tr, true);
		});
	};

	/**
  * Handler for the "save"-button
  */
	var _saveHandler = function _saveHandler() {
		var $self = $(this),
		    $tr = $self.closest('tr'),
		    dataset = jse.libs.form.getData($tr, undefined, true),
		    url = $self.data().url,
		    deferred = $.Deferred();

		// Done callback on validation success
		deferred.done(function () {
			if (url) {
				// If a url is given, post the data against the server
				jse.core.debug.info('Sending data:', dataset);
				jse.libs.xhr.ajax({
					'url': url,
					'data': dataset
				});
			}

			$this.trigger('row_saved', [dataset]);
			_switchState('default', $tr, true);
		});

		// Get validation state of the line. On success goto deferred.done callback
		$tr.trigger('validator.validate', [{
			'deferred': deferred
		}]);
	};

	/**
  * Handler for the "delete"-button
  */
	var _deleteHandler = function _deleteHandler() {
		var $self = $(this),
		    $tr = $self.closest('tr'),
		    dataset = {
			id: $tr.data('id')
		},
		    url = $self.data().url,
		    html = '<a href="lightbox_confirm.html?section=shop_offline&amp;message=delete_job' + '&amp;buttons=cancel-delete"></a>',
		    lightboxLink = $(html),
		    lightboxId = lightboxLink.lightbox_plugin({
			'lightbox_width': '360px'
		});

		$('#lightbox_package_' + lightboxId).one('click', '.delete', function () {
			$.lightbox_plugin('close', lightboxId);

			if (url) {
				// If a url is given, post the data against the server
				jse.libs.xhr.ajax({
					'url': url,
					'data': dataset
				});
			}

			$this.trigger('row_deleted', [dataset]);
			$tr.remove();
		});
	};

	/**
  * Handler for the 'add'-button
  */
	var _addHandler = function _addHandler() {
		var $self = $(this),
		    url = $self.data().url,
		    $tr = $self.closest('tr'),
		    dataset = jse.libs.form.getData($tr, undefined, true),
		    deferred = $.Deferred();

		// Done callback on validation success
		deferred.done(function () {
			var _finalize = function _finalize() {
				// Switch the state of the line and
				// create a new 'add'-line
				$this.trigger('row_added', [dataset]);
				_switchState('default', $tr, true);
				_createNewLine();
			};

			if (url) {
				// If a url is given, post the data against the server
				// The respone of the server contains an id, which will be
				// injected into the field names
				jse.core.debug.info('Sending data:', dataset);
				jse.libs.xhr.ajax({
					'url': url,
					'data': dataset
				}).done(function (result) {
					var id = result.id,
					    $targets = $tr.find('input:not(:button), textarea, select');

					$targets.each(function () {
						var $elem = $(this),
						    name = $elem.attr('name').replace('[0]', '[' + id + ']');

						if ($elem.data().lightboxHref) {
							$elem.data().lightboxHref = $elem.data().lightboxHref.replace('id=0', 'id=' + id);
						}
						$elem.attr('name', name);
					});

					$tr.find('[data-lightbox-href]').each(function () {
						var newLink = $(this).attr('data-lightbox-href').replace('id=0', 'id=' + id);
						$(this).attr('data-lightbox-href', newLink).data().lightboxHref = newLink;
					});

					// Update the hidden editor identifiers with the new record ID.
					$tr.find('input:hidden').each(function (index, inputHidden) {
						var $inputHidden = $(inputHidden),
						    name = $inputHidden.attr('name');

						if (name && name.indexOf('{id}') !== -1) {
							$inputHidden.attr('name', name.replace('{id}', id));
						}
					});

					_finalize();
				});
			} else {
				_finalize();
			}
		});

		// Get validation state of the line. On success goto deferred.done callback
		$tr.trigger('validator.validate', [{
			'deferred': deferred
		}]);
	};

	/**
  * Handler to update the table state, if an widget inside the table gets initialized
  * (needed to disable the datepicker buttons).
  *
  * @param {object} e    jQuery event-object
  */
	var _initialiedHandler = function _initialiedHandler(e) {
		var inside = $this.filter($(e.target)).add($this.find($(e.target))).length;

		if (!inside) {
			var $tr = $(e.target).closest('tr'),
			    type = $tr.hasClass('edit') ? 'edit' : $tr.hasClass('add') ? 'add' : 'default';

			_switchState(type, $tr, true);
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the extension, called by the engine.
  */
	module.init = function (done) {
		$template = $this.find('tfoot > tr');
		$table_body = $this.children('tbody');

		// Add a special class to the table, to style
		// disabled input boxes
		$this.addClass('table_inlineedit');

		// Set the default state for all tr
		_switchState('default', $table_body);
		// Add the "Add"-line to the table
		_createNewLine();

		// Add event listeners for all buttons and
		// a listener for the widget initialized event
		// from widgets inside the table
		$this.on('click', '.row_edit', _editHandler).on('click', '.row_delete', _deleteHandler).on('click', '.row_save', _saveHandler).on('click', '.row_add', _addHandler).on('click', '.row_abort', _abortHandler).on('widget.initialized', _initialiedHandler);

		$('body').on('validator.validate', function (e, d) {
			if (d && d.deferred) {
				// Event listener that performs on every validate trigger that isn't handled by the validator.
				d.deferred.resolve();
			}
		});
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYmxlX2lubGluZV9lZGl0LmpzIl0sIm5hbWVzIjpbImd4IiwiZXh0ZW5zaW9ucyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkdGVtcGxhdGUiLCIkdGFibGVfYm9keSIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9zd2l0Y2hTdGF0ZSIsIm1vZGUiLCIkZWxlbWVudCIsImFkZENsYXNzIiwiJHRhcmdldHMiLCJmaW5kIiwiJGVkaXQiLCJmaWx0ZXIiLCIkYWRkIiwiJGRlZmF1bHQiLCIkb3RoZXJzIiwiaGlkZSIsInJlbW92ZSIsInNob3ciLCJwcm9wIiwiZWFjaCIsIiRzZWxmIiwiZGF0YXNldCIsImpzZSIsImxpYnMiLCJmYWxsYmFjayIsIl9kYXRhIiwiYXR0ciIsInRvTG93ZXJDYXNlIiwiYWx0IiwidmFsdWVzIiwic3BsaXQiLCJjaGVja2VkIiwiYWZ0ZXIiLCJ3YWl0VW50aWxWYWx1ZXMiLCJjaGlsZHJlbiIsImxlbmd0aCIsInNldFRpbWVvdXQiLCJ2YWwiLCJ0ZXh0IiwidHJpZ2dlciIsInJlbW92ZUNsYXNzIiwiX2NyZWF0ZU5ld0xpbmUiLCIkbmV3TGluZSIsImNsb25lIiwicmVwbGFjZSIsInNldHVwV2lkZ2V0QXR0ciIsImFwcGVuZCIsIndpZGdldHMiLCJpbml0IiwibGFzdCIsImNvbnRyb2xsZXJzIiwiY29tcGF0aWJpbGl0eSIsIl9hYm9ydEhhbmRsZXIiLCIkdHIiLCJjbG9zZXN0IiwiY2FjaGUiLCJKU09OIiwic3RyaW5naWZ5IiwiY3VycmVudCIsImZvcm0iLCJnZXREYXRhIiwidW5kZWZpbmVkIiwiZGVmZXJyZWQiLCJEZWZlcnJlZCIsIl9yZXNldExpbmUiLCJlIiwiaWQiLCJvZmYiLCJsaWdodGJveF9wbHVnaW4iLCJyZWplY3QiLCJwcmVmaWxsRm9ybSIsInJlc29sdmUiLCJocmVmIiwibGlua0h0bWwiLCJsaWdodGJveExpbmsiLCJsaWdodGJveElkIiwib25lIiwicHJvbWlzZSIsIl9lZGl0SGFuZGxlciIsIiRlZGl0ZWQiLCJwcm9taXNlcyIsIm11bHRpRWRpdCIsInB1c2giLCJjYWxsIiwiZmlyc3QiLCJ3aGVuIiwiYXBwbHkiLCJkb25lIiwiX3NhdmVIYW5kbGVyIiwidXJsIiwiY29yZSIsImRlYnVnIiwiaW5mbyIsInhociIsImFqYXgiLCJfZGVsZXRlSGFuZGxlciIsImh0bWwiLCJfYWRkSGFuZGxlciIsIl9maW5hbGl6ZSIsInJlc3VsdCIsIiRlbGVtIiwibmFtZSIsImxpZ2h0Ym94SHJlZiIsIm5ld0xpbmsiLCJpbmRleCIsImlucHV0SGlkZGVuIiwiJGlucHV0SGlkZGVuIiwiaW5kZXhPZiIsIl9pbml0aWFsaWVkSGFuZGxlciIsImluc2lkZSIsInRhcmdldCIsImFkZCIsInR5cGUiLCJoYXNDbGFzcyIsIm9uIiwiZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7QUFNQUEsR0FBR0MsVUFBSCxDQUFjQyxNQUFkLENBQ0MsbUJBREQsRUFHQyxDQUFDLE1BQUQsRUFBUyxLQUFULEVBQWdCLFVBQWhCLENBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsYUFBWSxJQWJiOzs7QUFlQzs7Ozs7QUFLQUMsZUFBYyxJQXBCZjs7O0FBc0JDOzs7OztBQUtBQyxZQUFXO0FBQ1YsZUFBYTtBQURILEVBM0JaOzs7QUErQkM7Ozs7O0FBS0FDLFdBQVVKLEVBQUVLLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJMLElBQTdCLENBcENYOzs7QUFzQ0M7Ozs7O0FBS0FELFVBQVMsRUEzQ1Y7O0FBNkNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQVVBLEtBQUlTLGVBQWUsU0FBZkEsWUFBZSxDQUFTQyxJQUFULEVBQWVDLFFBQWYsRUFBeUJDLFFBQXpCLEVBQW1DOztBQUVyRCxNQUFJQyxXQUFXRixTQUFTRyxJQUFULENBQWMsb0NBQWQsQ0FBZjtBQUFBLE1BQ0NDLFFBQVFGLFNBQVNHLE1BQVQsQ0FBZ0IsV0FBaEIsQ0FEVDtBQUFBLE1BRUNDLE9BQU9KLFNBQVNHLE1BQVQsQ0FBZ0IsVUFBaEIsQ0FGUjtBQUFBLE1BR0NFLFdBQVdMLFNBQVNHLE1BQVQsQ0FBZ0IsY0FBaEIsQ0FIWjtBQUFBLE1BSUNHLFVBQVVOLFNBQVNHLE1BQVQsQ0FBZ0IsaURBQWhCLENBSlg7O0FBTUE7QUFDQUQsUUFBTUssSUFBTjtBQUNBSCxPQUFLRyxJQUFMO0FBQ0FGLFdBQVNFLElBQVQ7O0FBRUE7QUFDQVQsV0FDRUcsSUFERixDQUNPLHVCQURQLEVBRUVPLE1BRkY7O0FBSUEsVUFBUVgsSUFBUjtBQUNDLFFBQUssTUFBTDtBQUNDO0FBQ0FLLFVBQU1PLElBQU47QUFDQUgsWUFBUUksSUFBUixDQUFhLFVBQWIsRUFBeUIsS0FBekI7QUFDQTtBQUNELFFBQUssS0FBTDtBQUNDO0FBQ0FOLFNBQUtLLElBQUw7QUFDQUgsWUFBUUksSUFBUixDQUFhLFVBQWIsRUFBeUIsS0FBekI7QUFDQTtBQUNEO0FBQ0M7QUFDQUwsYUFBU0ksSUFBVDtBQUNBSCxZQUNFSSxJQURGLENBQ08sVUFEUCxFQUNtQixJQURuQixFQUVFQyxJQUZGLENBRU8sWUFBVztBQUNoQjtBQUNBLFNBQUlDLFFBQVF0QixFQUFFLElBQUYsQ0FBWjtBQUFBLFNBQ0N1QixVQUFVQyxJQUFJQyxJQUFKLENBQVNDLFFBQVQsQ0FBa0JDLEtBQWxCLENBQXdCTCxLQUF4QixFQUErQixtQkFBL0IsQ0FEWDs7QUFHQTtBQUNBLFNBQUlBLE1BQU1NLElBQU4sQ0FBVyxNQUFYLEtBQXNCTixNQUFNTSxJQUFOLENBQVcsTUFBWCxFQUFtQkMsV0FBbkIsT0FBcUMsVUFBM0QsSUFBeUVOLFFBQVFPLEdBQXJGLEVBQTBGO0FBQ3pGLFVBQUlDLFNBQVNSLFFBQVFPLEdBQVIsQ0FBWUUsS0FBWixDQUFrQixHQUFsQixDQUFiO0FBQUEsVUFDQ0MsVUFBVVgsTUFBTUYsSUFBTixDQUFXLFNBQVgsQ0FEWDtBQUVBRSxZQUFNWSxLQUFOLENBQVkseUNBQXlDRCxVQUFVRixPQUFPLENBQVAsQ0FBVixHQUFzQkEsT0FBTyxDQUFQLENBQS9ELElBQ1gsU0FERDtBQUVBLE1BTEQsTUFLTyxJQUFJVCxNQUFNRixJQUFOLENBQVcsU0FBWCxFQUFzQlMsV0FBdEIsT0FBd0MsUUFBNUMsRUFBc0Q7QUFDNUQsVUFBSU0sa0JBQWtCLFNBQWxCQSxlQUFrQixHQUFXO0FBQ2hDdkIsYUFBTUssSUFBTjtBQUNBLFdBQUksQ0FBQ0ssTUFBTWMsUUFBTixHQUFpQkMsTUFBdEIsRUFBOEI7QUFDN0JDLG1CQUFXLFlBQVc7QUFDckJIO0FBQ0EsU0FGRCxFQUVHLEdBRkg7QUFHQSxRQUpELE1BSU87QUFDTmIsY0FBTWMsUUFBTixDQUFlLGFBQWFkLE1BQU1pQixHQUFOLEVBQWIsR0FBMkIsSUFBMUMsRUFBZ0RDLElBQWhEO0FBQ0FsQixjQUFNWSxLQUFOLENBQ0Msd0NBQ0FaLE1BQU1jLFFBQU4sQ0FBZSxhQUFhZCxNQUFNaUIsR0FBTixFQUFiLEdBQTJCLElBQTFDLEVBQWdEQyxJQUFoRCxFQURBLEdBRUEsU0FIRDtBQUtBO0FBQ0E7QUFDRCxPQWZEOztBQWlCQUw7QUFDQTtBQUNELEtBakNGO0FBa0NBO0FBaERGOztBQW1EQXBDLFFBQU0wQyxPQUFOLENBQWMsYUFBZCxFQUE2QixFQUE3Qjs7QUFFQTtBQUNBLE1BQUloQyxRQUFKLEVBQWM7QUFDYkQsWUFDRWtDLFdBREYsQ0FDYyxrQkFEZCxFQUVFakMsUUFGRixDQUVXRixJQUZYO0FBR0E7QUFDRCxFQTdFRDs7QUErRUE7Ozs7O0FBS0EsS0FBSW9DLGlCQUFpQixTQUFqQkEsY0FBaUIsR0FBVztBQUMvQixNQUFJQyxXQUFXM0MsVUFBVTRDLEtBQVYsRUFBZjs7QUFFQUQsV0FDRWpDLElBREYsQ0FDTyxRQURQLEVBRUVVLElBRkYsQ0FFTyxZQUFXO0FBQ2hCLE9BQUlDLFFBQVF0QixFQUFFLElBQUYsQ0FBWjs7QUFFQXNCLFNBQU1NLElBQU4sQ0FBVyxNQUFYLEVBQW1CTixNQUFNTSxJQUFOLENBQVcsTUFBWCxFQUFtQmtCLE9BQW5CLENBQTJCLElBQTNCLEVBQWlDLEtBQWpDLENBQW5CO0FBQ0EsR0FORjs7QUFRQXhDLGVBQWEsS0FBYixFQUFvQnNDLFFBQXBCLEVBQThCLElBQTlCO0FBQ0E7QUFDQXBCLE1BQUlDLElBQUosQ0FBU0MsUUFBVCxDQUFrQnFCLGVBQWxCLENBQWtDSCxRQUFsQztBQUNBMUMsY0FBWThDLE1BQVosQ0FBbUJKLFFBQW5CO0FBQ0E7QUFDQWpELEtBQUdzRCxPQUFILENBQVdDLElBQVgsQ0FBZ0JoRCxZQUFZUyxJQUFaLENBQWlCLElBQWpCLEVBQXVCd0MsSUFBdkIsRUFBaEI7QUFDQXhELEtBQUdDLFVBQUgsQ0FBY3NELElBQWQsQ0FBbUJoRCxZQUFZUyxJQUFaLENBQWlCLElBQWpCLEVBQXVCd0MsSUFBdkIsRUFBbkI7QUFDQXhELEtBQUd5RCxXQUFILENBQWVGLElBQWYsQ0FBb0JoRCxZQUFZUyxJQUFaLENBQWlCLElBQWpCLEVBQXVCd0MsSUFBdkIsRUFBcEI7QUFDQXhELEtBQUcwRCxhQUFILENBQWlCSCxJQUFqQixDQUFzQmhELFlBQVlTLElBQVosQ0FBaUIsSUFBakIsRUFBdUJ3QyxJQUF2QixFQUF0QjtBQUNBM0IsTUFBSXlCLE9BQUosQ0FBWUMsSUFBWixDQUFpQmhELFlBQVlTLElBQVosQ0FBaUIsSUFBakIsRUFBdUJ3QyxJQUF2QixFQUFqQjtBQUNBM0IsTUFBSTVCLFVBQUosQ0FBZXNELElBQWYsQ0FBb0JoRCxZQUFZUyxJQUFaLENBQWlCLElBQWpCLEVBQXVCd0MsSUFBdkIsRUFBcEI7QUFDQSxFQXRCRDs7QUF3QkE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLEtBQUlHLGdCQUFnQixTQUFoQkEsYUFBZ0IsR0FBVztBQUM5QixNQUFJQyxNQUFNdkQsRUFBRSxJQUFGLEVBQVF3RCxPQUFSLENBQWdCLElBQWhCLENBQVY7QUFBQSxNQUNDQyxRQUFRQyxLQUFLQyxTQUFMLENBQWVKLElBQUl6RCxJQUFKLENBQVMsV0FBVCxDQUFmLENBRFQ7QUFBQSxNQUVDOEQsVUFBVUYsS0FBS0MsU0FBTCxDQUFlbkMsSUFBSUMsSUFBSixDQUFTb0MsSUFBVCxDQUFjQyxPQUFkLENBQXNCUCxHQUF0QixFQUEyQlEsU0FBM0IsRUFBc0MsSUFBdEMsQ0FBZixDQUZYO0FBQUEsTUFHQ0MsV0FBV2hFLEVBQUVpRSxRQUFGLEVBSFo7O0FBS0E7Ozs7O0FBS0EsTUFBSUMsYUFBYSxTQUFiQSxVQUFhLENBQVNDLENBQVQsRUFBWTtBQUM1QixPQUFJQSxDQUFKLEVBQU87QUFDTm5FLE1BQUUsdUJBQXVCbUUsRUFBRXJFLElBQUYsQ0FBT3NFLEVBQTlCLEdBQW1DLGNBQXJDLEVBQXFEQyxHQUFyRDtBQUNBckUsTUFBRSx1QkFBdUJtRSxFQUFFckUsSUFBRixDQUFPc0UsRUFBaEM7QUFDQXBFLE1BQUVzRSxlQUFGLENBQWtCLE9BQWxCLEVBQTJCSCxFQUFFckUsSUFBRixDQUFPc0UsRUFBbEM7QUFDQTs7QUFFRCxPQUFJRCxLQUFLQSxFQUFFckUsSUFBRixDQUFPeUUsTUFBaEIsRUFBd0I7QUFDdkJQLGFBQVNPLE1BQVQ7QUFDQSxJQUZELE1BRU87QUFDTjtBQUNBaEIsUUFBSWQsT0FBSixDQUFZLGlCQUFaLEVBQStCLEVBQS9CO0FBQ0E7QUFDQWpCLFFBQUlDLElBQUosQ0FBU29DLElBQVQsQ0FBY1csV0FBZCxDQUEwQmpCLEdBQTFCLEVBQStCQSxJQUFJekQsSUFBSixDQUFTLFdBQVQsQ0FBL0IsRUFBc0QsSUFBdEQ7QUFDQVEsaUJBQWEsU0FBYixFQUF3QmlELEdBQXhCLEVBQTZCLElBQTdCO0FBQ0FTLGFBQVNTLE9BQVQ7QUFDQTtBQUNELEdBakJEOztBQW1CQTtBQUNBLE1BQUloQixVQUFVRyxPQUFkLEVBQXVCO0FBQ3RCLE9BQ0NjLE9BQU8sb0RBQ04sd0RBRkY7QUFBQSxPQUdDQyxXQUFXLGNBQWNELElBQWQsR0FBcUIsUUFIakM7QUFBQSxPQUlDRSxlQUFlNUUsRUFBRTJFLFFBQUYsQ0FKaEI7QUFBQSxPQUtDRSxhQUFhRCxhQUFhTixlQUFiLENBQTZCO0FBQ3pDLHNCQUFrQjtBQUR1QixJQUE3QixDQUxkOztBQVNBdEUsS0FBRSx1QkFBdUI2RSxVQUF6QixFQUNFQyxHQURGLENBQ00sT0FETixFQUNlLFVBRGYsRUFDMkI7QUFDekIsY0FBVSxLQURlO0FBRXpCLFVBQU1EO0FBRm1CLElBRDNCLEVBSUlYLFVBSkosRUFLRVksR0FMRixDQUtNLE9BTE4sRUFLZSxTQUxmLEVBSzBCO0FBQ3hCLGNBQVUsSUFEYztBQUV4QixVQUFNRDtBQUZrQixJQUwxQixFQVFJWCxVQVJKO0FBVUEsR0FwQkQsTUFvQk87QUFDTkE7QUFDQTs7QUFFRCxTQUFPRixTQUFTZSxPQUFULEVBQVA7QUFFQSxFQXpERDs7QUEyREE7OztBQUdBLEtBQUlDLGVBQWUsU0FBZkEsWUFBZSxHQUFXO0FBQzdCLE1BQUl6QixNQUFNdkQsRUFBRSxJQUFGLEVBQVF3RCxPQUFSLENBQWdCLElBQWhCLENBQVY7QUFBQSxNQUNDeUIsVUFBVWxGLE1BQU1ZLElBQU4sQ0FBVyxTQUFYLENBRFg7QUFBQSxNQUVDdUUsV0FBVyxFQUZaOztBQUlBLE1BQUksQ0FBQzlFLFFBQVErRSxTQUFULElBQXNCRixRQUFRNUMsTUFBbEMsRUFBMEM7QUFDekM7QUFDQTtBQUNBNEMsV0FDRTVELElBREYsQ0FDTyxZQUFXO0FBQ2hCNkQsYUFBU0UsSUFBVCxDQUFjOUIsY0FBYytCLElBQWQsQ0FBbUJyRixFQUFFLElBQUYsRUFBUVcsSUFBUixDQUFhLFlBQWIsRUFBMkIyRSxLQUEzQixFQUFuQixDQUFkO0FBQ0EsSUFIRjtBQUlBOztBQUVEdEYsSUFBRXVGLElBQUYsQ0FBT0MsS0FBUCxDQUFhekIsU0FBYixFQUF3Qm1CLFFBQXhCLEVBQWtDSCxPQUFsQyxHQUE0Q1UsSUFBNUMsQ0FBaUQsWUFBVztBQUMzRDtBQUNBbEMsT0FBSXpELElBQUosQ0FBUyxXQUFULEVBQXNCMEIsSUFBSUMsSUFBSixDQUFTb0MsSUFBVCxDQUFjQyxPQUFkLENBQXNCUCxHQUF0QixFQUEyQlEsU0FBM0IsRUFBc0MsSUFBdEMsQ0FBdEI7QUFDQXpELGdCQUFhLE1BQWIsRUFBcUJpRCxHQUFyQixFQUEwQixJQUExQjtBQUNBLEdBSkQ7QUFLQSxFQW5CRDs7QUFxQkE7OztBQUdBLEtBQUltQyxlQUFlLFNBQWZBLFlBQWUsR0FBVztBQUM3QixNQUFJcEUsUUFBUXRCLEVBQUUsSUFBRixDQUFaO0FBQUEsTUFDQ3VELE1BQU1qQyxNQUFNa0MsT0FBTixDQUFjLElBQWQsQ0FEUDtBQUFBLE1BRUNqQyxVQUFVQyxJQUFJQyxJQUFKLENBQVNvQyxJQUFULENBQWNDLE9BQWQsQ0FBc0JQLEdBQXRCLEVBQTJCUSxTQUEzQixFQUFzQyxJQUF0QyxDQUZYO0FBQUEsTUFHQzRCLE1BQU1yRSxNQUFNeEIsSUFBTixHQUFhNkYsR0FIcEI7QUFBQSxNQUlDM0IsV0FBV2hFLEVBQUVpRSxRQUFGLEVBSlo7O0FBTUE7QUFDQUQsV0FBU3lCLElBQVQsQ0FBYyxZQUFXO0FBQ3hCLE9BQUlFLEdBQUosRUFBUztBQUNSO0FBQ0FuRSxRQUFJb0UsSUFBSixDQUFTQyxLQUFULENBQWVDLElBQWYsQ0FBb0IsZUFBcEIsRUFBcUN2RSxPQUFyQztBQUNBQyxRQUFJQyxJQUFKLENBQVNzRSxHQUFULENBQWFDLElBQWIsQ0FBa0I7QUFDakIsWUFBT0wsR0FEVTtBQUVqQixhQUFRcEU7QUFGUyxLQUFsQjtBQUlBOztBQUVEeEIsU0FBTTBDLE9BQU4sQ0FBYyxXQUFkLEVBQTJCLENBQUNsQixPQUFELENBQTNCO0FBQ0FqQixnQkFBYSxTQUFiLEVBQXdCaUQsR0FBeEIsRUFBNkIsSUFBN0I7QUFFQSxHQWJEOztBQWVBO0FBQ0FBLE1BQUlkLE9BQUosQ0FBWSxvQkFBWixFQUFrQyxDQUNqQztBQUNDLGVBQVl1QjtBQURiLEdBRGlDLENBQWxDO0FBTUEsRUE5QkQ7O0FBZ0NBOzs7QUFHQSxLQUFJaUMsaUJBQWlCLFNBQWpCQSxjQUFpQixHQUFXO0FBQy9CLE1BQUkzRSxRQUFRdEIsRUFBRSxJQUFGLENBQVo7QUFBQSxNQUNDdUQsTUFBTWpDLE1BQU1rQyxPQUFOLENBQWMsSUFBZCxDQURQO0FBQUEsTUFFQ2pDLFVBQVU7QUFDVDZDLE9BQUliLElBQUl6RCxJQUFKLENBQVMsSUFBVDtBQURLLEdBRlg7QUFBQSxNQUtDNkYsTUFBTXJFLE1BQU14QixJQUFOLEdBQWE2RixHQUxwQjtBQUFBLE1BTUNPLE9BQU8sK0VBQ0osa0NBUEo7QUFBQSxNQVFDdEIsZUFBZTVFLEVBQUVrRyxJQUFGLENBUmhCO0FBQUEsTUFTQ3JCLGFBQWFELGFBQWFOLGVBQWIsQ0FBNkI7QUFDekMscUJBQWtCO0FBRHVCLEdBQTdCLENBVGQ7O0FBYUF0RSxJQUFFLHVCQUF1QjZFLFVBQXpCLEVBQXFDQyxHQUFyQyxDQUF5QyxPQUF6QyxFQUFrRCxTQUFsRCxFQUE2RCxZQUFXO0FBQ3ZFOUUsS0FBRXNFLGVBQUYsQ0FBa0IsT0FBbEIsRUFBMkJPLFVBQTNCOztBQUVBLE9BQUljLEdBQUosRUFBUztBQUNSO0FBQ0FuRSxRQUFJQyxJQUFKLENBQVNzRSxHQUFULENBQWFDLElBQWIsQ0FBa0I7QUFDakIsWUFBT0wsR0FEVTtBQUVqQixhQUFRcEU7QUFGUyxLQUFsQjtBQUlBOztBQUVEeEIsU0FBTTBDLE9BQU4sQ0FBYyxhQUFkLEVBQTZCLENBQUNsQixPQUFELENBQTdCO0FBQ0FnQyxPQUFJckMsTUFBSjtBQUNBLEdBYkQ7QUFjQSxFQTVCRDs7QUE4QkE7OztBQUdBLEtBQUlpRixjQUFjLFNBQWRBLFdBQWMsR0FBVztBQUM1QixNQUFJN0UsUUFBUXRCLEVBQUUsSUFBRixDQUFaO0FBQUEsTUFDQzJGLE1BQU1yRSxNQUFNeEIsSUFBTixHQUFhNkYsR0FEcEI7QUFBQSxNQUVDcEMsTUFBTWpDLE1BQU1rQyxPQUFOLENBQWMsSUFBZCxDQUZQO0FBQUEsTUFHQ2pDLFVBQVVDLElBQUlDLElBQUosQ0FBU29DLElBQVQsQ0FBY0MsT0FBZCxDQUFzQlAsR0FBdEIsRUFBMkJRLFNBQTNCLEVBQXNDLElBQXRDLENBSFg7QUFBQSxNQUlDQyxXQUFXaEUsRUFBRWlFLFFBQUYsRUFKWjs7QUFNQTtBQUNBRCxXQUFTeUIsSUFBVCxDQUFjLFlBQVc7QUFDeEIsT0FBSVcsWUFBWSxTQUFaQSxTQUFZLEdBQVc7QUFDMUI7QUFDQTtBQUNBckcsVUFBTTBDLE9BQU4sQ0FBYyxXQUFkLEVBQTJCLENBQUNsQixPQUFELENBQTNCO0FBQ0FqQixpQkFBYSxTQUFiLEVBQXdCaUQsR0FBeEIsRUFBNkIsSUFBN0I7QUFDQVo7QUFDQSxJQU5EOztBQVFBLE9BQUlnRCxHQUFKLEVBQVM7QUFDUjtBQUNBO0FBQ0E7QUFDQW5FLFFBQUlvRSxJQUFKLENBQVNDLEtBQVQsQ0FBZUMsSUFBZixDQUFvQixlQUFwQixFQUFxQ3ZFLE9BQXJDO0FBQ0FDLFFBQUlDLElBQUosQ0FBU3NFLEdBQVQsQ0FBYUMsSUFBYixDQUFrQjtBQUNqQixZQUFPTCxHQURVO0FBRWpCLGFBQVFwRTtBQUZTLEtBQWxCLEVBR0drRSxJQUhILENBR1EsVUFBU1ksTUFBVCxFQUFpQjtBQUN4QixTQUFJakMsS0FBS2lDLE9BQU9qQyxFQUFoQjtBQUFBLFNBQ0MxRCxXQUFXNkMsSUFBSTVDLElBQUosQ0FBUyxzQ0FBVCxDQURaOztBQUdBRCxjQUFTVyxJQUFULENBQWMsWUFBVztBQUN4QixVQUFJaUYsUUFBUXRHLEVBQUUsSUFBRixDQUFaO0FBQUEsVUFDQ3VHLE9BQU9ELE1BQ0wxRSxJQURLLENBQ0EsTUFEQSxFQUVMa0IsT0FGSyxDQUVHLEtBRkgsRUFFVSxNQUFNc0IsRUFBTixHQUFXLEdBRnJCLENBRFI7O0FBS0EsVUFBSWtDLE1BQU14RyxJQUFOLEdBQWEwRyxZQUFqQixFQUErQjtBQUM5QkYsYUFBTXhHLElBQU4sR0FBYTBHLFlBQWIsR0FBNEJGLE1BQU14RyxJQUFOLEdBQWEwRyxZQUFiLENBQTBCMUQsT0FBMUIsQ0FBa0MsTUFBbEMsRUFBMEMsUUFBUXNCLEVBQWxELENBQTVCO0FBQ0E7QUFDRGtDLFlBQU0xRSxJQUFOLENBQVcsTUFBWCxFQUFtQjJFLElBQW5CO0FBQ0EsTUFWRDs7QUFZQWhELFNBQUk1QyxJQUFKLENBQVMsc0JBQVQsRUFBaUNVLElBQWpDLENBQXNDLFlBQVc7QUFDaEQsVUFBSW9GLFVBQVV6RyxFQUFFLElBQUYsRUFBUTRCLElBQVIsQ0FBYSxvQkFBYixFQUFtQ2tCLE9BQW5DLENBQTJDLE1BQTNDLEVBQW1ELFFBQVFzQixFQUEzRCxDQUFkO0FBQ0FwRSxRQUFFLElBQUYsRUFDRTRCLElBREYsQ0FDTyxvQkFEUCxFQUM2QjZFLE9BRDdCLEVBRUUzRyxJQUZGLEdBRVMwRyxZQUZULEdBRXdCQyxPQUZ4QjtBQUdBLE1BTEQ7O0FBT0E7QUFDQWxELFNBQUk1QyxJQUFKLENBQVMsY0FBVCxFQUF5QlUsSUFBekIsQ0FBOEIsVUFBU3FGLEtBQVQsRUFBZ0JDLFdBQWhCLEVBQTZCO0FBQzFELFVBQUlDLGVBQWU1RyxFQUFFMkcsV0FBRixDQUFuQjtBQUFBLFVBQ0NKLE9BQU9LLGFBQWFoRixJQUFiLENBQWtCLE1BQWxCLENBRFI7O0FBR0EsVUFBSTJFLFFBQVFBLEtBQUtNLE9BQUwsQ0FBYSxNQUFiLE1BQXlCLENBQUMsQ0FBdEMsRUFBeUM7QUFDeENELG9CQUFhaEYsSUFBYixDQUFrQixNQUFsQixFQUEwQjJFLEtBQUt6RCxPQUFMLENBQWEsTUFBYixFQUFxQnNCLEVBQXJCLENBQTFCO0FBQ0E7QUFDRCxNQVBEOztBQVNBZ0M7QUFDQSxLQXJDRDtBQXNDQSxJQTNDRCxNQTJDTztBQUNOQTtBQUNBO0FBQ0QsR0F2REQ7O0FBeURBO0FBQ0E3QyxNQUFJZCxPQUFKLENBQVksb0JBQVosRUFBa0MsQ0FDakM7QUFDQyxlQUFZdUI7QUFEYixHQURpQyxDQUFsQztBQU1BLEVBeEVEOztBQTBFQTs7Ozs7O0FBTUEsS0FBSThDLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQVMzQyxDQUFULEVBQVk7QUFDcEMsTUFBSTRDLFNBQVNoSCxNQUNYYyxNQURXLENBQ0piLEVBQUVtRSxFQUFFNkMsTUFBSixDQURJLEVBRVhDLEdBRlcsQ0FFUGxILE1BQU1ZLElBQU4sQ0FBV1gsRUFBRW1FLEVBQUU2QyxNQUFKLENBQVgsQ0FGTyxFQUdYM0UsTUFIRjs7QUFLQSxNQUFJLENBQUMwRSxNQUFMLEVBQWE7QUFDWixPQUFJeEQsTUFBTXZELEVBQUVtRSxFQUFFNkMsTUFBSixFQUFZeEQsT0FBWixDQUFvQixJQUFwQixDQUFWO0FBQUEsT0FDQzBELE9BQVEzRCxJQUFJNEQsUUFBSixDQUFhLE1BQWIsQ0FBRCxHQUF5QixNQUF6QixHQUNDNUQsSUFBSTRELFFBQUosQ0FBYSxLQUFiLENBQUQsR0FBd0IsS0FBeEIsR0FDQSxTQUhSOztBQUtBN0csZ0JBQWE0RyxJQUFiLEVBQW1CM0QsR0FBbkIsRUFBd0IsSUFBeEI7QUFDQTtBQUNELEVBZEQ7O0FBZ0JBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0ExRCxRQUFPcUQsSUFBUCxHQUFjLFVBQVN1QyxJQUFULEVBQWU7QUFDNUJ4RixjQUFZRixNQUFNWSxJQUFOLENBQVcsWUFBWCxDQUFaO0FBQ0FULGdCQUFjSCxNQUFNcUMsUUFBTixDQUFlLE9BQWYsQ0FBZDs7QUFFQTtBQUNBO0FBQ0FyQyxRQUFNVSxRQUFOLENBQWUsa0JBQWY7O0FBRUE7QUFDQUgsZUFBYSxTQUFiLEVBQXdCSixXQUF4QjtBQUNBO0FBQ0F5Qzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTVDLFFBQ0VxSCxFQURGLENBQ0ssT0FETCxFQUNjLFdBRGQsRUFDMkJwQyxZQUQzQixFQUVFb0MsRUFGRixDQUVLLE9BRkwsRUFFYyxhQUZkLEVBRTZCbkIsY0FGN0IsRUFHRW1CLEVBSEYsQ0FHSyxPQUhMLEVBR2MsV0FIZCxFQUcyQjFCLFlBSDNCLEVBSUUwQixFQUpGLENBSUssT0FKTCxFQUljLFVBSmQsRUFJMEJqQixXQUoxQixFQUtFaUIsRUFMRixDQUtLLE9BTEwsRUFLYyxZQUxkLEVBSzRCOUQsYUFMNUIsRUFNRThELEVBTkYsQ0FNSyxvQkFOTCxFQU0yQk4sa0JBTjNCOztBQVFBOUcsSUFBRSxNQUFGLEVBQ0VvSCxFQURGLENBQ0ssb0JBREwsRUFDMkIsVUFBVWpELENBQVYsRUFBYWtELENBQWIsRUFBZ0I7QUFDekMsT0FBSUEsS0FBS0EsRUFBRXJELFFBQVgsRUFBcUI7QUFDcEI7QUFDQXFELE1BQUVyRCxRQUFGLENBQVdTLE9BQVg7QUFDQTtBQUNELEdBTkY7QUFPQWdCO0FBQ0EsRUFoQ0Q7O0FBa0NBO0FBQ0EsUUFBTzVGLE1BQVA7QUFDQSxDQWxlRiIsImZpbGUiOiJ0YWJsZV9pbmxpbmVfZWRpdC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gdGFibGVfaW5saW5lX2VkaXQuanMgMjAxNS0xMC0xNiBnbVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTUgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgRXh0ZW5zaW9uIGZvciBtYWtpbmcgdGFibGVzIGVkaXRhYmxlLlxuICpcbiAqIEBtb2R1bGUgQWRtaW4vRXh0ZW5zaW9ucy90YWJsZV9pbmxpbmVfZWRpdFxuICogQGlnbm9yZVxuICovXG5neC5leHRlbnNpb25zLm1vZHVsZShcblx0J3RhYmxlX2lubGluZV9lZGl0Jyxcblx0XG5cdFsnZm9ybScsICd4aHInLCAnZmFsbGJhY2snXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBFeHRlbnNpb24gUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFRlbXBsYXRlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRlbXBsYXRlID0gbnVsbCxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBUYWJsZSBCb2R5IFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRhYmxlX2JvZHkgPSBudWxsLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9ucyBmb3IgRXh0ZW5zaW9uXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdCdtdWx0aUVkaXQnOiBmYWxzZVxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBFeHRlbnNpb24gT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTkFMSVRZXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU3dpdGNoIFN0YXRlXG5cdFx0ICpcblx0XHQgKiBGdW5jdGlvbiB0aGF0IGVuYWJsZXMgLyBkaXNhYmxlcywgZGVwZW5kaW5nIG9uIHRoZSBtb2RlLCBhbGwgaW5wdXQgZmllbGRzIGluc2lkZVxuXHRcdCAqIHRoZSAkZWxlbWVudCBhbmQgc2hvd3MgLyBoaWRlcyB0aGUgY29ycmVzcG9uZGluZyBidXR0b25zLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IG1vZGUgU2V0IHRoZSBnaXZlbiBtb2RlLiBQb3NzaWJsZSB2YWx1ZXM6ICdlZGl0JywgJ2FkZCcsICdkZWZhdWx0J1xuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSAkZWxlbWVudCBUaGUgZWxlbWVudCBqUXVlcnkgc2VsZWN0aW9uIHRoYXQgZ2V0cyBtb2RpZmllZFxuXHRcdCAqIEBwYXJhbSB7Ym9vbGVhbn0gYWRkQ2xhc3MgSWYgdHJ1ZSwgdGhlIHN0YXRlIGNsYXNzIGdldHMgYWRkZWQgdG8gdGhlIGVsZW1lbnRcblx0XHQgKi9cblx0XHR2YXIgX3N3aXRjaFN0YXRlID0gZnVuY3Rpb24obW9kZSwgJGVsZW1lbnQsIGFkZENsYXNzKSB7XG5cdFx0XHRcblx0XHRcdHZhciAkdGFyZ2V0cyA9ICRlbGVtZW50LmZpbmQoJ2lucHV0LCB0ZXh0YXJlYSwgc2VsZWN0LCBidXR0b24sIGknKSxcblx0XHRcdFx0JGVkaXQgPSAkdGFyZ2V0cy5maWx0ZXIoJy5lZGl0bW9kZScpLFxuXHRcdFx0XHQkYWRkID0gJHRhcmdldHMuZmlsdGVyKCcuYWRkbW9kZScpLFxuXHRcdFx0XHQkZGVmYXVsdCA9ICR0YXJnZXRzLmZpbHRlcignLmRlZmF1bHRtb2RlJyksXG5cdFx0XHRcdCRvdGhlcnMgPSAkdGFyZ2V0cy5maWx0ZXIoJzpub3QoLmVkaXRtb2RlKTpub3QoLmFkZG1vZGUpOm5vdCguZGVmYXVsdG1vZGUpJyk7XG5cdFx0XHRcblx0XHRcdC8vIEhpZGUgYWxsIGJ1dHRvbnNcblx0XHRcdCRlZGl0LmhpZGUoKTtcblx0XHRcdCRhZGQuaGlkZSgpO1xuXHRcdFx0JGRlZmF1bHQuaGlkZSgpO1xuXHRcdFx0XG5cdFx0XHQvLyBSZW1vdmUgYWx0LXRleHQgaWYgYXZhaWxhYmxlXG5cdFx0XHQkZWxlbWVudFxuXHRcdFx0XHQuZmluZCgnLnRhYmxlX2lubGluZWVkaXRfYWx0Jylcblx0XHRcdFx0LnJlbW92ZSgpO1xuXHRcdFx0XG5cdFx0XHRzd2l0Y2ggKG1vZGUpIHtcblx0XHRcdFx0Y2FzZSAnZWRpdCc6XG5cdFx0XHRcdFx0Ly8gU3dpdGNoIHRvIGVkaXQgbW9kZVxuXHRcdFx0XHRcdCRlZGl0LnNob3coKTtcblx0XHRcdFx0XHQkb3RoZXJzLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdhZGQnOlxuXHRcdFx0XHRcdC8vIFN3aXRjaCB0byBhZGQgbW9kZVxuXHRcdFx0XHRcdCRhZGQuc2hvdygpO1xuXHRcdFx0XHRcdCRvdGhlcnMucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0Ly8gU3dpdGNoIHRvIGRlZmF1bHQtbW9kZVxuXHRcdFx0XHRcdCRkZWZhdWx0LnNob3coKTtcblx0XHRcdFx0XHQkb3RoZXJzXG5cdFx0XHRcdFx0XHQucHJvcCgnZGlzYWJsZWQnLCB0cnVlKVxuXHRcdFx0XHRcdFx0LmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdC8vIENoZWNrIGlmIHRoZXJlIGlzIGFuIGFsdCB0ZXh0IGdpdmVuIGZvciB0aGUgaW5wdXQgZmllbGRcblx0XHRcdFx0XHRcdFx0dmFyICRzZWxmID0gJCh0aGlzKSxcblx0XHRcdFx0XHRcdFx0XHRkYXRhc2V0ID0ganNlLmxpYnMuZmFsbGJhY2suX2RhdGEoJHNlbGYsICd0YWJsZV9pbmxpbmVfZWRpdCcpO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0Ly8gUmVwbGFjZSBzb21lIGtpbmQgb2YgZm9ybSBmaWVsZHMgd2l0aCBzcGFuIHRhZ3Ncblx0XHRcdFx0XHRcdFx0aWYgKCRzZWxmLmF0dHIoJ3R5cGUnKSAmJiAkc2VsZi5hdHRyKCd0eXBlJykudG9Mb3dlckNhc2UoKSA9PT0gJ2NoZWNrYm94JyAmJiBkYXRhc2V0LmFsdCkge1xuXHRcdFx0XHRcdFx0XHRcdHZhciB2YWx1ZXMgPSBkYXRhc2V0LmFsdC5zcGxpdCgnXycpLFxuXHRcdFx0XHRcdFx0XHRcdFx0Y2hlY2tlZCA9ICRzZWxmLnByb3AoJ2NoZWNrZWQnKTtcblx0XHRcdFx0XHRcdFx0XHQkc2VsZi5hZnRlcignPHNwYW4gY2xhc3M9XCJ0YWJsZV9pbmxpbmVlZGl0X2FsdFwiPicgKyAoY2hlY2tlZCA/IHZhbHVlc1swXSA6IHZhbHVlc1sxXSkgK1xuXHRcdFx0XHRcdFx0XHRcdFx0Jzwvc3Bhbj4nKTtcblx0XHRcdFx0XHRcdFx0fSBlbHNlIGlmICgkc2VsZi5wcm9wKCd0YWdOYW1lJykudG9Mb3dlckNhc2UoKSA9PT0gJ3NlbGVjdCcpIHtcblx0XHRcdFx0XHRcdFx0XHR2YXIgd2FpdFVudGlsVmFsdWVzID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHQkZWRpdC5oaWRlKCk7XG5cdFx0XHRcdFx0XHRcdFx0XHRpZiAoISRzZWxmLmNoaWxkcmVuKCkubGVuZ3RoKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0d2FpdFVudGlsVmFsdWVzKCk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIDIwMCk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQkc2VsZi5jaGlsZHJlbignW3ZhbHVlPVwiJyArICRzZWxmLnZhbCgpICsgJ1wiXScpLnRleHQoKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0JHNlbGYuYWZ0ZXIoXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0JzxzcGFuIGNsYXNzPVwidGFibGVfaW5saW5lZWRpdF9hbHRcIj4nICtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQkc2VsZi5jaGlsZHJlbignW3ZhbHVlPVwiJyArICRzZWxmLnZhbCgpICsgJ1wiXScpLnRleHQoKSArXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0Jzwvc3Bhbj4nXG5cdFx0XHRcdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdHdhaXRVbnRpbFZhbHVlcygpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JHRoaXMudHJpZ2dlcignRk9STV9VUERBVEUnLCBbXSk7XG5cdFx0XHRcblx0XHRcdC8vIEFkZCB0aGUgbW9kZSBjbGFzc1xuXHRcdFx0aWYgKGFkZENsYXNzKSB7XG5cdFx0XHRcdCRlbGVtZW50XG5cdFx0XHRcdFx0LnJlbW92ZUNsYXNzKCdlZGl0IGFkZCBkZWZhdWx0Jylcblx0XHRcdFx0XHQuYWRkQ2xhc3MobW9kZSk7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDcmVhdGUgTmV3IExpbmVcblx0XHQgKlxuXHRcdCAqIENyZWF0ZXMgYSBuZXcgXCJhZGRcIi1saW5lIGJ5IGNsb25pbmcgdGhlIGZvb3RlciB0ZW1wbGF0ZS5cblx0XHQgKi9cblx0XHR2YXIgX2NyZWF0ZU5ld0xpbmUgPSBmdW5jdGlvbigpIHtcblx0XHRcdHZhciAkbmV3TGluZSA9ICR0ZW1wbGF0ZS5jbG9uZSgpO1xuXHRcdFx0XG5cdFx0XHQkbmV3TGluZVxuXHRcdFx0XHQuZmluZCgnW25hbWVdJylcblx0XHRcdFx0LmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0dmFyICRzZWxmID0gJCh0aGlzKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkc2VsZi5hdHRyKCduYW1lJywgJHNlbGYuYXR0cignbmFtZScpLnJlcGxhY2UoJ1tdJywgJ1swXScpKTtcblx0XHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdF9zd2l0Y2hTdGF0ZSgnYWRkJywgJG5ld0xpbmUsIHRydWUpO1xuXHRcdFx0Ly8gUmVuYW1lIHRoZSB0ZW1wb3JhcmlseSB3aWRnZXQgZGF0YSBhdHRyaWJ1dGVzXG5cdFx0XHRqc2UubGlicy5mYWxsYmFjay5zZXR1cFdpZGdldEF0dHIoJG5ld0xpbmUpO1xuXHRcdFx0JHRhYmxlX2JvZHkuYXBwZW5kKCRuZXdMaW5lKTtcblx0XHRcdC8vIFN0YXJ0IHRoZSB3aWRnZXRzXG5cdFx0XHRneC53aWRnZXRzLmluaXQoJHRhYmxlX2JvZHkuZmluZCgndHInKS5sYXN0KCkpO1xuXHRcdFx0Z3guZXh0ZW5zaW9ucy5pbml0KCR0YWJsZV9ib2R5LmZpbmQoJ3RyJykubGFzdCgpKTtcblx0XHRcdGd4LmNvbnRyb2xsZXJzLmluaXQoJHRhYmxlX2JvZHkuZmluZCgndHInKS5sYXN0KCkpO1xuXHRcdFx0Z3guY29tcGF0aWJpbGl0eS5pbml0KCR0YWJsZV9ib2R5LmZpbmQoJ3RyJykubGFzdCgpKTtcblx0XHRcdGpzZS53aWRnZXRzLmluaXQoJHRhYmxlX2JvZHkuZmluZCgndHInKS5sYXN0KCkpO1xuXHRcdFx0anNlLmV4dGVuc2lvbnMuaW5pdCgkdGFibGVfYm9keS5maW5kKCd0cicpLmxhc3QoKSk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXIgZm9yIHRoZSBcImFib3J0XCItYnV0dG9uXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJucyB7Ym9vbGVhbn0gSWYgZnVuY3Rpb24gZ2V0cyBjYWxsZWQgZGlyZWN0bHksIHRoZSByZXR1cm4gdmFsdWUgaXMgdGhlIHN0YXRlIG9mIHRoZSBhYm9ydC5cblx0XHQgKi9cblx0XHR2YXIgX2Fib3J0SGFuZGxlciA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyICR0ciA9ICQodGhpcykuY2xvc2VzdCgndHInKSxcblx0XHRcdFx0Y2FjaGUgPSBKU09OLnN0cmluZ2lmeSgkdHIuZGF0YSgnZm9ybWNhY2hlJykpLFxuXHRcdFx0XHRjdXJyZW50ID0gSlNPTi5zdHJpbmdpZnkoanNlLmxpYnMuZm9ybS5nZXREYXRhKCR0ciwgdW5kZWZpbmVkLCB0cnVlKSksXG5cdFx0XHRcdGRlZmVycmVkID0gJC5EZWZlcnJlZCgpO1xuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEhlbHBlciBmdW5jdGlvbiB0byByZXNldCBhIGxpbmUgc3RhdGVcblx0XHRcdCAqXG5cdFx0XHQgKiBAcHJpdmF0ZVxuXHRcdFx0ICovXG5cdFx0XHR2YXIgX3Jlc2V0TGluZSA9IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0aWYgKGUpIHtcblx0XHRcdFx0XHQkKCcjbGlnaHRib3hfcGFja2FnZV8nICsgZS5kYXRhLmlkICsgJ2FkbWluX2J1dHRvbicpLm9mZigpO1xuXHRcdFx0XHRcdCQoJyNsaWdodGJveF9wYWNrYWdlXycgKyBlLmRhdGEuaWQpO1xuXHRcdFx0XHRcdCQubGlnaHRib3hfcGx1Z2luKCdjbG9zZScsIGUuZGF0YS5pZCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGlmIChlICYmIGUuZGF0YS5yZWplY3QpIHtcblx0XHRcdFx0XHRkZWZlcnJlZC5yZWplY3QoKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHQvLyBSZXNldCB0aGUgdmFsaWRhdGlvbiBzdGF0ZVxuXHRcdFx0XHRcdCR0ci50cmlnZ2VyKCd2YWxpZGF0b3IucmVzZXQnLCBbXSk7XG5cdFx0XHRcdFx0Ly8gUmVzZXQgdGhlIGZvcm0gZGF0YVxuXHRcdFx0XHRcdGpzZS5saWJzLmZvcm0ucHJlZmlsbEZvcm0oJHRyLCAkdHIuZGF0YSgnZm9ybWNhY2hlJyksIHRydWUpO1xuXHRcdFx0XHRcdF9zd2l0Y2hTdGF0ZSgnZGVmYXVsdCcsICR0ciwgdHJ1ZSk7XG5cdFx0XHRcdFx0ZGVmZXJyZWQucmVzb2x2ZSgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBDb21wYXJlIHRoZSBvbGQgd2l0aCB0aGUgbmV3IGRhdGEuIElmIGNoYW5nZXMgd2VyZSBtYWRlLCBjb25maXJtIHRoZSBhYm9ydFxuXHRcdFx0aWYgKGNhY2hlICE9PSBjdXJyZW50KSB7XG5cdFx0XHRcdHZhclxuXHRcdFx0XHRcdGhyZWYgPSAnbGlnaHRib3hfY29uZmlybS5odG1sP3NlY3Rpb249c2hvcF9vZmZsaW5lJmFtcDsnICtcblx0XHRcdFx0XHRcdCdtZXNzYWdlPWRpY2FyZF9jaGFuZ2VzX2hpbnQmYW1wO2J1dHRvbnM9Y2FuY2VsLWRpc2NhcmQnLFxuXHRcdFx0XHRcdGxpbmtIdG1sID0gJzxhIGhyZWY9XCInICsgaHJlZiArICdcIj48L2E+Jyxcblx0XHRcdFx0XHRsaWdodGJveExpbmsgPSAkKGxpbmtIdG1sKSxcblx0XHRcdFx0XHRsaWdodGJveElkID0gbGlnaHRib3hMaW5rLmxpZ2h0Ym94X3BsdWdpbih7XG5cdFx0XHRcdFx0XHQnbGlnaHRib3hfd2lkdGgnOiAnMzYwcHgnXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkKCcjbGlnaHRib3hfcGFja2FnZV8nICsgbGlnaHRib3hJZClcblx0XHRcdFx0XHQub25lKCdjbGljaycsICcuZGlzY2FyZCcsIHtcblx0XHRcdFx0XHRcdCdyZWplY3QnOiBmYWxzZSxcblx0XHRcdFx0XHRcdCdpZCc6IGxpZ2h0Ym94SWRcblx0XHRcdFx0XHR9LCBfcmVzZXRMaW5lKVxuXHRcdFx0XHRcdC5vbmUoJ2NsaWNrJywgJy5jYW5jZWwnLCB7XG5cdFx0XHRcdFx0XHQncmVqZWN0JzogdHJ1ZSxcblx0XHRcdFx0XHRcdCdpZCc6IGxpZ2h0Ym94SWRcblx0XHRcdFx0XHR9LCBfcmVzZXRMaW5lKTtcblx0XHRcdFx0XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRfcmVzZXRMaW5lKCk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdHJldHVybiBkZWZlcnJlZC5wcm9taXNlKCk7XG5cdFx0XHRcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXIgZm9yIHRoZSBcImVkaXRcIi1idXR0b25cblx0XHQgKi9cblx0XHR2YXIgX2VkaXRIYW5kbGVyID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJHRyID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpLFxuXHRcdFx0XHQkZWRpdGVkID0gJHRoaXMuZmluZCgndHIuZWRpdCcpLFxuXHRcdFx0XHRwcm9taXNlcyA9IFtdO1xuXHRcdFx0XG5cdFx0XHRpZiAoIW9wdGlvbnMubXVsdGlFZGl0ICYmICRlZGl0ZWQubGVuZ3RoKSB7XG5cdFx0XHRcdC8vIElmIG11bHRpRWRpdCBpcyBkaXNhYmxlZCBhbmQgb3RoZXIgbGluZXMgYXJlIGluIGVkaXQgbW9kZSwgd2FpdCBmb3IgY29uZmlybWF0aW9uXG5cdFx0XHRcdC8vIG9mIHRoZSBhYm9ydCBldmVudCBvbiB0aGUgb3RoZXIgbGluZXMuXG5cdFx0XHRcdCRlZGl0ZWRcblx0XHRcdFx0XHQuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHByb21pc2VzLnB1c2goX2Fib3J0SGFuZGxlci5jYWxsKCQodGhpcykuZmluZCgnLnJvd19hYm9ydCcpLmZpcnN0KCkpKTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0JC53aGVuLmFwcGx5KHVuZGVmaW5lZCwgcHJvbWlzZXMpLnByb21pc2UoKS5kb25lKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQvLyBTdG9yZSB0aGUgY3VycmVudCBkYXRhIG9mIHRoZSBsaW5lIGluIGNhY2hlXG5cdFx0XHRcdCR0ci5kYXRhKCdmb3JtY2FjaGUnLCBqc2UubGlicy5mb3JtLmdldERhdGEoJHRyLCB1bmRlZmluZWQsIHRydWUpKTtcblx0XHRcdFx0X3N3aXRjaFN0YXRlKCdlZGl0JywgJHRyLCB0cnVlKTtcblx0XHRcdH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlciBmb3IgdGhlIFwic2F2ZVwiLWJ1dHRvblxuXHRcdCAqL1xuXHRcdHZhciBfc2F2ZUhhbmRsZXIgPSBmdW5jdGlvbigpIHtcblx0XHRcdHZhciAkc2VsZiA9ICQodGhpcyksXG5cdFx0XHRcdCR0ciA9ICRzZWxmLmNsb3Nlc3QoJ3RyJyksXG5cdFx0XHRcdGRhdGFzZXQgPSBqc2UubGlicy5mb3JtLmdldERhdGEoJHRyLCB1bmRlZmluZWQsIHRydWUpLFxuXHRcdFx0XHR1cmwgPSAkc2VsZi5kYXRhKCkudXJsLFxuXHRcdFx0XHRkZWZlcnJlZCA9ICQuRGVmZXJyZWQoKTtcblx0XHRcdFxuXHRcdFx0Ly8gRG9uZSBjYWxsYmFjayBvbiB2YWxpZGF0aW9uIHN1Y2Nlc3Ncblx0XHRcdGRlZmVycmVkLmRvbmUoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGlmICh1cmwpIHtcblx0XHRcdFx0XHQvLyBJZiBhIHVybCBpcyBnaXZlbiwgcG9zdCB0aGUgZGF0YSBhZ2FpbnN0IHRoZSBzZXJ2ZXJcblx0XHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5pbmZvKCdTZW5kaW5nIGRhdGE6JywgZGF0YXNldCk7XG5cdFx0XHRcdFx0anNlLmxpYnMueGhyLmFqYXgoe1xuXHRcdFx0XHRcdFx0J3VybCc6IHVybCxcblx0XHRcdFx0XHRcdCdkYXRhJzogZGF0YXNldFxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQkdGhpcy50cmlnZ2VyKCdyb3dfc2F2ZWQnLCBbZGF0YXNldF0pO1xuXHRcdFx0XHRfc3dpdGNoU3RhdGUoJ2RlZmF1bHQnLCAkdHIsIHRydWUpO1xuXHRcdFx0XHRcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBHZXQgdmFsaWRhdGlvbiBzdGF0ZSBvZiB0aGUgbGluZS4gT24gc3VjY2VzcyBnb3RvIGRlZmVycmVkLmRvbmUgY2FsbGJhY2tcblx0XHRcdCR0ci50cmlnZ2VyKCd2YWxpZGF0b3IudmFsaWRhdGUnLCBbXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQnZGVmZXJyZWQnOiBkZWZlcnJlZFxuXHRcdFx0XHR9XG5cdFx0XHRdKTtcblx0XHRcdFxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlciBmb3IgdGhlIFwiZGVsZXRlXCItYnV0dG9uXG5cdFx0ICovXG5cdFx0dmFyIF9kZWxldGVIYW5kbGVyID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJHNlbGYgPSAkKHRoaXMpLFxuXHRcdFx0XHQkdHIgPSAkc2VsZi5jbG9zZXN0KCd0cicpLFxuXHRcdFx0XHRkYXRhc2V0ID0ge1xuXHRcdFx0XHRcdGlkOiAkdHIuZGF0YSgnaWQnKVxuXHRcdFx0XHR9LFxuXHRcdFx0XHR1cmwgPSAkc2VsZi5kYXRhKCkudXJsLFxuXHRcdFx0XHRodG1sID0gJzxhIGhyZWY9XCJsaWdodGJveF9jb25maXJtLmh0bWw/c2VjdGlvbj1zaG9wX29mZmxpbmUmYW1wO21lc3NhZ2U9ZGVsZXRlX2pvYidcblx0XHRcdFx0XHQrICcmYW1wO2J1dHRvbnM9Y2FuY2VsLWRlbGV0ZVwiPjwvYT4nLFxuXHRcdFx0XHRsaWdodGJveExpbmsgPSAkKGh0bWwpLFxuXHRcdFx0XHRsaWdodGJveElkID0gbGlnaHRib3hMaW5rLmxpZ2h0Ym94X3BsdWdpbih7XG5cdFx0XHRcdFx0J2xpZ2h0Ym94X3dpZHRoJzogJzM2MHB4J1xuXHRcdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JCgnI2xpZ2h0Ym94X3BhY2thZ2VfJyArIGxpZ2h0Ym94SWQpLm9uZSgnY2xpY2snLCAnLmRlbGV0ZScsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkLmxpZ2h0Ym94X3BsdWdpbignY2xvc2UnLCBsaWdodGJveElkKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICh1cmwpIHtcblx0XHRcdFx0XHQvLyBJZiBhIHVybCBpcyBnaXZlbiwgcG9zdCB0aGUgZGF0YSBhZ2FpbnN0IHRoZSBzZXJ2ZXJcblx0XHRcdFx0XHRqc2UubGlicy54aHIuYWpheCh7XG5cdFx0XHRcdFx0XHQndXJsJzogdXJsLFxuXHRcdFx0XHRcdFx0J2RhdGEnOiBkYXRhc2V0XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdCR0aGlzLnRyaWdnZXIoJ3Jvd19kZWxldGVkJywgW2RhdGFzZXRdKTtcblx0XHRcdFx0JHRyLnJlbW92ZSgpO1xuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVyIGZvciB0aGUgJ2FkZCctYnV0dG9uXG5cdFx0ICovXG5cdFx0dmFyIF9hZGRIYW5kbGVyID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgJHNlbGYgPSAkKHRoaXMpLFxuXHRcdFx0XHR1cmwgPSAkc2VsZi5kYXRhKCkudXJsLFxuXHRcdFx0XHQkdHIgPSAkc2VsZi5jbG9zZXN0KCd0cicpLFxuXHRcdFx0XHRkYXRhc2V0ID0ganNlLmxpYnMuZm9ybS5nZXREYXRhKCR0ciwgdW5kZWZpbmVkLCB0cnVlKSxcblx0XHRcdFx0ZGVmZXJyZWQgPSAkLkRlZmVycmVkKCk7XG5cdFx0XHRcblx0XHRcdC8vIERvbmUgY2FsbGJhY2sgb24gdmFsaWRhdGlvbiBzdWNjZXNzXG5cdFx0XHRkZWZlcnJlZC5kb25lKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgX2ZpbmFsaXplID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0Ly8gU3dpdGNoIHRoZSBzdGF0ZSBvZiB0aGUgbGluZSBhbmRcblx0XHRcdFx0XHQvLyBjcmVhdGUgYSBuZXcgJ2FkZCctbGluZVxuXHRcdFx0XHRcdCR0aGlzLnRyaWdnZXIoJ3Jvd19hZGRlZCcsIFtkYXRhc2V0XSk7XG5cdFx0XHRcdFx0X3N3aXRjaFN0YXRlKCdkZWZhdWx0JywgJHRyLCB0cnVlKTtcblx0XHRcdFx0XHRfY3JlYXRlTmV3TGluZSgpO1xuXHRcdFx0XHR9O1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKHVybCkge1xuXHRcdFx0XHRcdC8vIElmIGEgdXJsIGlzIGdpdmVuLCBwb3N0IHRoZSBkYXRhIGFnYWluc3QgdGhlIHNlcnZlclxuXHRcdFx0XHRcdC8vIFRoZSByZXNwb25lIG9mIHRoZSBzZXJ2ZXIgY29udGFpbnMgYW4gaWQsIHdoaWNoIHdpbGwgYmVcblx0XHRcdFx0XHQvLyBpbmplY3RlZCBpbnRvIHRoZSBmaWVsZCBuYW1lc1xuXHRcdFx0XHRcdGpzZS5jb3JlLmRlYnVnLmluZm8oJ1NlbmRpbmcgZGF0YTonLCBkYXRhc2V0KTtcblx0XHRcdFx0XHRqc2UubGlicy54aHIuYWpheCh7XG5cdFx0XHRcdFx0XHQndXJsJzogdXJsLFxuXHRcdFx0XHRcdFx0J2RhdGEnOiBkYXRhc2V0XG5cdFx0XHRcdFx0fSkuZG9uZShmdW5jdGlvbihyZXN1bHQpIHtcblx0XHRcdFx0XHRcdHZhciBpZCA9IHJlc3VsdC5pZCxcblx0XHRcdFx0XHRcdFx0JHRhcmdldHMgPSAkdHIuZmluZCgnaW5wdXQ6bm90KDpidXR0b24pLCB0ZXh0YXJlYSwgc2VsZWN0Jyk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdCR0YXJnZXRzLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdHZhciAkZWxlbSA9ICQodGhpcyksXG5cdFx0XHRcdFx0XHRcdFx0bmFtZSA9ICRlbGVtXG5cdFx0XHRcdFx0XHRcdFx0XHQuYXR0cignbmFtZScpXG5cdFx0XHRcdFx0XHRcdFx0XHQucmVwbGFjZSgnWzBdJywgJ1snICsgaWQgKyAnXScpO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0aWYgKCRlbGVtLmRhdGEoKS5saWdodGJveEhyZWYpIHtcblx0XHRcdFx0XHRcdFx0XHQkZWxlbS5kYXRhKCkubGlnaHRib3hIcmVmID0gJGVsZW0uZGF0YSgpLmxpZ2h0Ym94SHJlZi5yZXBsYWNlKCdpZD0wJywgJ2lkPScgKyBpZCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0JGVsZW0uYXR0cignbmFtZScsIG5hbWUpO1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdCR0ci5maW5kKCdbZGF0YS1saWdodGJveC1ocmVmXScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdHZhciBuZXdMaW5rID0gJCh0aGlzKS5hdHRyKCdkYXRhLWxpZ2h0Ym94LWhyZWYnKS5yZXBsYWNlKCdpZD0wJywgJ2lkPScgKyBpZCk7XG5cdFx0XHRcdFx0XHRcdCQodGhpcylcblx0XHRcdFx0XHRcdFx0XHQuYXR0cignZGF0YS1saWdodGJveC1ocmVmJywgbmV3TGluaylcblx0XHRcdFx0XHRcdFx0XHQuZGF0YSgpLmxpZ2h0Ym94SHJlZiA9IG5ld0xpbms7XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0Ly8gVXBkYXRlIHRoZSBoaWRkZW4gZWRpdG9yIGlkZW50aWZpZXJzIHdpdGggdGhlIG5ldyByZWNvcmQgSUQuXG5cdFx0XHRcdFx0XHQkdHIuZmluZCgnaW5wdXQ6aGlkZGVuJykuZWFjaChmdW5jdGlvbihpbmRleCwgaW5wdXRIaWRkZW4pIHtcblx0XHRcdFx0XHRcdFx0dmFyICRpbnB1dEhpZGRlbiA9ICQoaW5wdXRIaWRkZW4pLFxuXHRcdFx0XHRcdFx0XHRcdG5hbWUgPSAkaW5wdXRIaWRkZW4uYXR0cignbmFtZScpOyBcblx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdGlmIChuYW1lICYmIG5hbWUuaW5kZXhPZigne2lkfScpICE9PSAtMSkge1xuXHRcdFx0XHRcdFx0XHRcdCRpbnB1dEhpZGRlbi5hdHRyKCduYW1lJywgbmFtZS5yZXBsYWNlKCd7aWR9JywgaWQpKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fSk7IFxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRfZmluYWxpemUoKTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRfZmluYWxpemUoKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIEdldCB2YWxpZGF0aW9uIHN0YXRlIG9mIHRoZSBsaW5lLiBPbiBzdWNjZXNzIGdvdG8gZGVmZXJyZWQuZG9uZSBjYWxsYmFja1xuXHRcdFx0JHRyLnRyaWdnZXIoJ3ZhbGlkYXRvci52YWxpZGF0ZScsIFtcblx0XHRcdFx0e1xuXHRcdFx0XHRcdCdkZWZlcnJlZCc6IGRlZmVycmVkXG5cdFx0XHRcdH1cblx0XHRcdF0pO1xuXHRcdFx0XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVyIHRvIHVwZGF0ZSB0aGUgdGFibGUgc3RhdGUsIGlmIGFuIHdpZGdldCBpbnNpZGUgdGhlIHRhYmxlIGdldHMgaW5pdGlhbGl6ZWRcblx0XHQgKiAobmVlZGVkIHRvIGRpc2FibGUgdGhlIGRhdGVwaWNrZXIgYnV0dG9ucykuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZSAgICBqUXVlcnkgZXZlbnQtb2JqZWN0XG5cdFx0ICovXG5cdFx0dmFyIF9pbml0aWFsaWVkSGFuZGxlciA9IGZ1bmN0aW9uKGUpIHtcblx0XHRcdHZhciBpbnNpZGUgPSAkdGhpc1xuXHRcdFx0XHQuZmlsdGVyKCQoZS50YXJnZXQpKVxuXHRcdFx0XHQuYWRkKCR0aGlzLmZpbmQoJChlLnRhcmdldCkpKVxuXHRcdFx0XHQubGVuZ3RoO1xuXHRcdFx0XG5cdFx0XHRpZiAoIWluc2lkZSkge1xuXHRcdFx0XHR2YXIgJHRyID0gJChlLnRhcmdldCkuY2xvc2VzdCgndHInKSxcblx0XHRcdFx0XHR0eXBlID0gKCR0ci5oYXNDbGFzcygnZWRpdCcpKSA/ICdlZGl0JyA6XG5cdFx0XHRcdFx0ICAgICAgICgkdHIuaGFzQ2xhc3MoJ2FkZCcpKSA/ICdhZGQnIDpcblx0XHRcdFx0XHQgICAgICAgJ2RlZmF1bHQnO1xuXHRcdFx0XHRcblx0XHRcdFx0X3N3aXRjaFN0YXRlKHR5cGUsICR0ciwgdHJ1ZSk7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSBleHRlbnNpb24sIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JHRlbXBsYXRlID0gJHRoaXMuZmluZCgndGZvb3QgPiB0cicpO1xuXHRcdFx0JHRhYmxlX2JvZHkgPSAkdGhpcy5jaGlsZHJlbigndGJvZHknKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWRkIGEgc3BlY2lhbCBjbGFzcyB0byB0aGUgdGFibGUsIHRvIHN0eWxlXG5cdFx0XHQvLyBkaXNhYmxlZCBpbnB1dCBib3hlc1xuXHRcdFx0JHRoaXMuYWRkQ2xhc3MoJ3RhYmxlX2lubGluZWVkaXQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2V0IHRoZSBkZWZhdWx0IHN0YXRlIGZvciBhbGwgdHJcblx0XHRcdF9zd2l0Y2hTdGF0ZSgnZGVmYXVsdCcsICR0YWJsZV9ib2R5KTtcblx0XHRcdC8vIEFkZCB0aGUgXCJBZGRcIi1saW5lIHRvIHRoZSB0YWJsZVxuXHRcdFx0X2NyZWF0ZU5ld0xpbmUoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQWRkIGV2ZW50IGxpc3RlbmVycyBmb3IgYWxsIGJ1dHRvbnMgYW5kXG5cdFx0XHQvLyBhIGxpc3RlbmVyIGZvciB0aGUgd2lkZ2V0IGluaXRpYWxpemVkIGV2ZW50XG5cdFx0XHQvLyBmcm9tIHdpZGdldHMgaW5zaWRlIHRoZSB0YWJsZVxuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdjbGljaycsICcucm93X2VkaXQnLCBfZWRpdEhhbmRsZXIpXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLnJvd19kZWxldGUnLCBfZGVsZXRlSGFuZGxlcilcblx0XHRcdFx0Lm9uKCdjbGljaycsICcucm93X3NhdmUnLCBfc2F2ZUhhbmRsZXIpXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLnJvd19hZGQnLCBfYWRkSGFuZGxlcilcblx0XHRcdFx0Lm9uKCdjbGljaycsICcucm93X2Fib3J0JywgX2Fib3J0SGFuZGxlcilcblx0XHRcdFx0Lm9uKCd3aWRnZXQuaW5pdGlhbGl6ZWQnLCBfaW5pdGlhbGllZEhhbmRsZXIpO1xuXHRcdFx0XG5cdFx0XHQkKCdib2R5Jylcblx0XHRcdFx0Lm9uKCd2YWxpZGF0b3IudmFsaWRhdGUnLCBmdW5jdGlvbiAoZSwgZCkge1xuXHRcdFx0XHRcdGlmIChkICYmIGQuZGVmZXJyZWQpIHtcblx0XHRcdFx0XHRcdC8vIEV2ZW50IGxpc3RlbmVyIHRoYXQgcGVyZm9ybXMgb24gZXZlcnkgdmFsaWRhdGUgdHJpZ2dlciB0aGF0IGlzbid0IGhhbmRsZWQgYnkgdGhlIHZhbGlkYXRvci5cblx0XHRcdFx0XHRcdGQuZGVmZXJyZWQucmVzb2x2ZSgpOyBcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
