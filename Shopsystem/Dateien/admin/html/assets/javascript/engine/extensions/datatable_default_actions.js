'use strict';

/* --------------------------------------------------------------
 datatable_default_actions.js 2016-10-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Enable Default Dropdown Actions
 *
 * This extension will handle the "defaultRowAction" and "defaultBulkAction" data attributes of the table upon
 * initialization or user click.
 *
 * ### Options
 *
 * **Default Row Action | `data-datatable_default_actions-row` | String | Required**
 *
 * Provide the default row action. This will automatically be mapped to the defaultRowAction data value of the table.
 *
 * **Default Bulk Action | `data-datatable_default_actions-bulk` | String | Required**
 *
 * Provide the default bulk action. This will automatically be mapped to the defaultBulkAction data value of the table.
 *
 * **Bulk Action Selector | `data-datatable_default_actions-bulk-action-selector` | String | Optional**
 *
 * Provide a selector for the bulk action dropdown widget. The default value is '.bulk-action'.
 *
 * ### Methods
 *
 * **Ensure Default Task**
 *
 * This method will make sure that there is a default task selected. Call it after you setup the row or bulk dropdown
 * actions. Sometimes the user_configuration db value might contain a default value that is not present in the dropdowns
 * anymore (e.g. removed module). In order to make sure that there will always be a default value use this method after
 * creating the dropdown actions and it will use the first dropdown action as default if needed.
 *
 * ```javascript
 * // Ensure default row actions.
 * $('.table-main').datatable_default_actions('ensure', 'row');
 *
 * // Ensure default bulk actions.
 * $('.table-main').datatable_default_actions('ensure', 'bulk');
 * ```
 *
 * @module Admin/extensions/datatable_default_actions
 */
gx.extensions.module('datatable_default_actions', [gx.source + '/libs/button_dropdown'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------


	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		bulkActionSelector: '.bulk-action'
	};

	/**
  * Final Options
  *
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Ensure that there will be a default action in the row or bulk dropdowns.
  *
  * @param {String} type Can be whether 'row' or 'bulk'.
  */
	function _ensure(type) {
		var $table = $(this);

		switch (type) {
			case 'row':
				var $rowActions = $table.find('tbody .btn-group.dropdown');
				// debugger;
				$rowActions.each(function () {
					if ($(this).find('button:empty').length) {
						var $actionLink = $(this).find('ul li:first a');
						jse.libs.button_dropdown.setDefaultAction($(this), $actionLink);
					}
				});

				break;

			case 'bulk':
				var $bulkAction = $(options.bulkActionSelector);

				if ($bulkAction.find('button:first').text() === '') {
					var $actionLink = $bulkAction.find('ul li:first a');
					jse.libs.button_dropdown.setDefaultAction($bulkAction, $actionLink);
				}

				break;

			default:
				throw new Error('Invalid "ensure" type given (expected "row" or "bulk" got : "' + type + '").');
		}
	}

	/**
  * On Button Drodpown Action Click
  *
  * Update the defaultBulkAction and defaultRowAction data attributes.
  */
	function _onButtonDropdownActionClick() {
		var property = $(this).parents('.btn-group')[0] === $(options.bulkActionSelector)[0] ? 'defaultBulkAction' : 'defaultRowAction';

		$this.data(property, $(this).data('configurationValue'));
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$this.data({
			defaultRowAction: options.row,
			defaultBulkAction: options.bulk
		});

		$this.on('click', '.btn-group.dropdown a', _onButtonDropdownActionClick);
		$('body').on('click', options.bulkActionSelector, _onButtonDropdownActionClick);

		// Bind module api to jQuery object. 
		$.fn.extend({
			datatable_default_actions: function datatable_default_actions(action) {
				switch (action) {
					case 'ensure':
						for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
							args[_key - 1] = arguments[_key];
						}

						return _ensure.apply(this, args);
				}
			}
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnMuanMiXSwibmFtZXMiOlsiZ3giLCJleHRlbnNpb25zIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwiYnVsa0FjdGlvblNlbGVjdG9yIiwib3B0aW9ucyIsImV4dGVuZCIsIl9lbnN1cmUiLCJ0eXBlIiwiJHRhYmxlIiwiJHJvd0FjdGlvbnMiLCJmaW5kIiwiZWFjaCIsImxlbmd0aCIsIiRhY3Rpb25MaW5rIiwianNlIiwibGlicyIsImJ1dHRvbl9kcm9wZG93biIsInNldERlZmF1bHRBY3Rpb24iLCIkYnVsa0FjdGlvbiIsInRleHQiLCJFcnJvciIsIl9vbkJ1dHRvbkRyb3Bkb3duQWN0aW9uQ2xpY2siLCJwcm9wZXJ0eSIsInBhcmVudHMiLCJpbml0IiwiZG9uZSIsImRlZmF1bHRSb3dBY3Rpb24iLCJyb3ciLCJkZWZhdWx0QnVsa0FjdGlvbiIsImJ1bGsiLCJvbiIsImZuIiwiZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9ucyIsImFjdGlvbiIsImFyZ3MiLCJhcHBseSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1Q0FBLEdBQUdDLFVBQUgsQ0FBY0MsTUFBZCxDQUFxQiwyQkFBckIsRUFBa0QsQ0FBSUYsR0FBR0csTUFBUCwyQkFBbEQsRUFBeUYsVUFBU0MsSUFBVCxFQUFlOztBQUV2Rzs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCQyxzQkFBb0I7QUFESixFQUFqQjs7QUFJQTs7Ozs7QUFLQSxLQUFNQyxVQUFVSCxFQUFFSSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJILFFBQW5CLEVBQTZCSCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRixTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNTLE9BQVQsQ0FBaUJDLElBQWpCLEVBQXVCO0FBQ3RCLE1BQU1DLFNBQVNQLEVBQUUsSUFBRixDQUFmOztBQUVBLFVBQVFNLElBQVI7QUFDQyxRQUFLLEtBQUw7QUFDQyxRQUFNRSxjQUFjRCxPQUFPRSxJQUFQLENBQVksMkJBQVosQ0FBcEI7QUFDQTtBQUNBRCxnQkFBWUUsSUFBWixDQUFpQixZQUFZO0FBQzVCLFNBQUlWLEVBQUUsSUFBRixFQUFRUyxJQUFSLENBQWEsY0FBYixFQUE2QkUsTUFBakMsRUFBeUM7QUFDeEMsVUFBTUMsY0FBY1osRUFBRSxJQUFGLEVBQVFTLElBQVIsQ0FBYSxlQUFiLENBQXBCO0FBQ0FJLFVBQUlDLElBQUosQ0FBU0MsZUFBVCxDQUF5QkMsZ0JBQXpCLENBQTBDaEIsRUFBRSxJQUFGLENBQTFDLEVBQW1EWSxXQUFuRDtBQUNBO0FBQ0QsS0FMRDs7QUFPQTs7QUFFRCxRQUFLLE1BQUw7QUFDQyxRQUFNSyxjQUFjakIsRUFBRUcsUUFBUUQsa0JBQVYsQ0FBcEI7O0FBRUEsUUFBSWUsWUFBWVIsSUFBWixDQUFpQixjQUFqQixFQUFpQ1MsSUFBakMsT0FBNEMsRUFBaEQsRUFBb0Q7QUFDbkQsU0FBTU4sY0FBY0ssWUFBWVIsSUFBWixDQUFpQixlQUFqQixDQUFwQjtBQUNBSSxTQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJDLGdCQUF6QixDQUEwQ0MsV0FBMUMsRUFBdURMLFdBQXZEO0FBQ0E7O0FBRUQ7O0FBRUQ7QUFDQyxVQUFNLElBQUlPLEtBQUosbUVBQTBFYixJQUExRSxTQUFOO0FBeEJGO0FBMEJBOztBQUVEOzs7OztBQUtBLFVBQVNjLDRCQUFULEdBQXdDO0FBQ3ZDLE1BQU1DLFdBQVdyQixFQUFFLElBQUYsRUFBUXNCLE9BQVIsQ0FBZ0IsWUFBaEIsRUFBOEIsQ0FBOUIsTUFBcUN0QixFQUFFRyxRQUFRRCxrQkFBVixFQUE4QixDQUE5QixDQUFyQyxHQUNkLG1CQURjLEdBQ1Esa0JBRHpCOztBQUdBSCxRQUFNRCxJQUFOLENBQVd1QixRQUFYLEVBQXFCckIsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxvQkFBYixDQUFyQjtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQUYsUUFBTzJCLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJ6QixRQUFNRCxJQUFOLENBQVc7QUFDVjJCLHFCQUFrQnRCLFFBQVF1QixHQURoQjtBQUVWQyxzQkFBbUJ4QixRQUFReUI7QUFGakIsR0FBWDs7QUFLQTdCLFFBQU04QixFQUFOLENBQVMsT0FBVCxFQUFrQix1QkFBbEIsRUFBMkNULDRCQUEzQztBQUNBcEIsSUFBRSxNQUFGLEVBQVU2QixFQUFWLENBQWEsT0FBYixFQUFzQjFCLFFBQVFELGtCQUE5QixFQUFrRGtCLDRCQUFsRDs7QUFFQTtBQUNBcEIsSUFBRThCLEVBQUYsQ0FBSzFCLE1BQUwsQ0FBWTtBQUNYMkIsOEJBQTJCLG1DQUFTQyxNQUFULEVBQTBCO0FBQ3BELFlBQVFBLE1BQVI7QUFDQyxVQUFLLFFBQUw7QUFBQSx3Q0FGNkNDLElBRTdDO0FBRjZDQSxXQUU3QztBQUFBOztBQUNDLGFBQU81QixRQUFRNkIsS0FBUixDQUFjLElBQWQsRUFBb0JELElBQXBCLENBQVA7QUFGRjtBQUlBO0FBTlUsR0FBWjs7QUFTQVQ7QUFDQSxFQXBCRDs7QUFzQkEsUUFBTzVCLE1BQVA7QUFFQSxDQXZIRCIsImZpbGUiOiJkYXRhdGFibGVfZGVmYXVsdF9hY3Rpb25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGRhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnMuanMgMjAxNi0xMC0xMlxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiAjIyBFbmFibGUgRGVmYXVsdCBEcm9wZG93biBBY3Rpb25zXHJcbiAqXHJcbiAqIFRoaXMgZXh0ZW5zaW9uIHdpbGwgaGFuZGxlIHRoZSBcImRlZmF1bHRSb3dBY3Rpb25cIiBhbmQgXCJkZWZhdWx0QnVsa0FjdGlvblwiIGRhdGEgYXR0cmlidXRlcyBvZiB0aGUgdGFibGUgdXBvblxyXG4gKiBpbml0aWFsaXphdGlvbiBvciB1c2VyIGNsaWNrLlxyXG4gKlxyXG4gKiAjIyMgT3B0aW9uc1xyXG4gKlxyXG4gKiAqKkRlZmF1bHQgUm93IEFjdGlvbiB8IGBkYXRhLWRhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnMtcm93YCB8IFN0cmluZyB8IFJlcXVpcmVkKipcclxuICpcclxuICogUHJvdmlkZSB0aGUgZGVmYXVsdCByb3cgYWN0aW9uLiBUaGlzIHdpbGwgYXV0b21hdGljYWxseSBiZSBtYXBwZWQgdG8gdGhlIGRlZmF1bHRSb3dBY3Rpb24gZGF0YSB2YWx1ZSBvZiB0aGUgdGFibGUuXHJcbiAqXHJcbiAqICoqRGVmYXVsdCBCdWxrIEFjdGlvbiB8IGBkYXRhLWRhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnMtYnVsa2AgfCBTdHJpbmcgfCBSZXF1aXJlZCoqXHJcbiAqXHJcbiAqIFByb3ZpZGUgdGhlIGRlZmF1bHQgYnVsayBhY3Rpb24uIFRoaXMgd2lsbCBhdXRvbWF0aWNhbGx5IGJlIG1hcHBlZCB0byB0aGUgZGVmYXVsdEJ1bGtBY3Rpb24gZGF0YSB2YWx1ZSBvZiB0aGUgdGFibGUuXHJcbiAqXHJcbiAqICoqQnVsayBBY3Rpb24gU2VsZWN0b3IgfCBgZGF0YS1kYXRhdGFibGVfZGVmYXVsdF9hY3Rpb25zLWJ1bGstYWN0aW9uLXNlbGVjdG9yYCB8IFN0cmluZyB8IE9wdGlvbmFsKipcclxuICpcclxuICogUHJvdmlkZSBhIHNlbGVjdG9yIGZvciB0aGUgYnVsayBhY3Rpb24gZHJvcGRvd24gd2lkZ2V0LiBUaGUgZGVmYXVsdCB2YWx1ZSBpcyAnLmJ1bGstYWN0aW9uJy5cclxuICpcclxuICogIyMjIE1ldGhvZHNcclxuICpcclxuICogKipFbnN1cmUgRGVmYXVsdCBUYXNrKipcclxuICpcclxuICogVGhpcyBtZXRob2Qgd2lsbCBtYWtlIHN1cmUgdGhhdCB0aGVyZSBpcyBhIGRlZmF1bHQgdGFzayBzZWxlY3RlZC4gQ2FsbCBpdCBhZnRlciB5b3Ugc2V0dXAgdGhlIHJvdyBvciBidWxrIGRyb3Bkb3duXHJcbiAqIGFjdGlvbnMuIFNvbWV0aW1lcyB0aGUgdXNlcl9jb25maWd1cmF0aW9uIGRiIHZhbHVlIG1pZ2h0IGNvbnRhaW4gYSBkZWZhdWx0IHZhbHVlIHRoYXQgaXMgbm90IHByZXNlbnQgaW4gdGhlIGRyb3Bkb3duc1xyXG4gKiBhbnltb3JlIChlLmcuIHJlbW92ZWQgbW9kdWxlKS4gSW4gb3JkZXIgdG8gbWFrZSBzdXJlIHRoYXQgdGhlcmUgd2lsbCBhbHdheXMgYmUgYSBkZWZhdWx0IHZhbHVlIHVzZSB0aGlzIG1ldGhvZCBhZnRlclxyXG4gKiBjcmVhdGluZyB0aGUgZHJvcGRvd24gYWN0aW9ucyBhbmQgaXQgd2lsbCB1c2UgdGhlIGZpcnN0IGRyb3Bkb3duIGFjdGlvbiBhcyBkZWZhdWx0IGlmIG5lZWRlZC5cclxuICpcclxuICogYGBgamF2YXNjcmlwdFxyXG4gKiAvLyBFbnN1cmUgZGVmYXVsdCByb3cgYWN0aW9ucy5cclxuICogJCgnLnRhYmxlLW1haW4nKS5kYXRhdGFibGVfZGVmYXVsdF9hY3Rpb25zKCdlbnN1cmUnLCAncm93Jyk7XHJcbiAqXHJcbiAqIC8vIEVuc3VyZSBkZWZhdWx0IGJ1bGsgYWN0aW9ucy5cclxuICogJCgnLnRhYmxlLW1haW4nKS5kYXRhdGFibGVfZGVmYXVsdF9hY3Rpb25zKCdlbnN1cmUnLCAnYnVsaycpO1xyXG4gKiBgYGBcclxuICpcclxuICogQG1vZHVsZSBBZG1pbi9leHRlbnNpb25zL2RhdGF0YWJsZV9kZWZhdWx0X2FjdGlvbnNcclxuICovXHJcbmd4LmV4dGVuc2lvbnMubW9kdWxlKCdkYXRhdGFibGVfZGVmYXVsdF9hY3Rpb25zJywgW2Ake2d4LnNvdXJjZX0vbGlicy9idXR0b25fZHJvcGRvd25gXSwgZnVuY3Rpb24oZGF0YSkge1xyXG5cdFxyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBWQVJJQUJMRVNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIERlZmF1bHQgT3B0aW9uc1xyXG5cdCAqXHJcblx0ICogQHR5cGUge09iamVjdH1cclxuXHQgKi9cclxuXHRjb25zdCBkZWZhdWx0cyA9IHtcclxuXHRcdGJ1bGtBY3Rpb25TZWxlY3RvcjogJy5idWxrLWFjdGlvbidcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEZpbmFsIE9wdGlvbnNcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtPYmplY3R9XHJcblx0ICovXHJcblx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIEluc3RhbmNlXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIEZVTkNUSU9OU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEVuc3VyZSB0aGF0IHRoZXJlIHdpbGwgYmUgYSBkZWZhdWx0IGFjdGlvbiBpbiB0aGUgcm93IG9yIGJ1bGsgZHJvcGRvd25zLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IHR5cGUgQ2FuIGJlIHdoZXRoZXIgJ3Jvdycgb3IgJ2J1bGsnLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9lbnN1cmUodHlwZSkge1xyXG5cdFx0Y29uc3QgJHRhYmxlID0gJCh0aGlzKTtcclxuXHRcdFxyXG5cdFx0c3dpdGNoICh0eXBlKSB7XHJcblx0XHRcdGNhc2UgJ3Jvdyc6XHJcblx0XHRcdFx0Y29uc3QgJHJvd0FjdGlvbnMgPSAkdGFibGUuZmluZCgndGJvZHkgLmJ0bi1ncm91cC5kcm9wZG93bicpO1xyXG5cdFx0XHRcdC8vIGRlYnVnZ2VyO1xyXG5cdFx0XHRcdCRyb3dBY3Rpb25zLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0aWYgKCQodGhpcykuZmluZCgnYnV0dG9uOmVtcHR5JykubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRcdGNvbnN0ICRhY3Rpb25MaW5rID0gJCh0aGlzKS5maW5kKCd1bCBsaTpmaXJzdCBhJyk7XHJcblx0XHRcdFx0XHRcdGpzZS5saWJzLmJ1dHRvbl9kcm9wZG93bi5zZXREZWZhdWx0QWN0aW9uKCQodGhpcyksICRhY3Rpb25MaW5rKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHJcblx0XHRcdGNhc2UgJ2J1bGsnOlxyXG5cdFx0XHRcdGNvbnN0ICRidWxrQWN0aW9uID0gJChvcHRpb25zLmJ1bGtBY3Rpb25TZWxlY3Rvcik7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKCRidWxrQWN0aW9uLmZpbmQoJ2J1dHRvbjpmaXJzdCcpLnRleHQoKSA9PT0gJycpIHtcclxuXHRcdFx0XHRcdGNvbnN0ICRhY3Rpb25MaW5rID0gJGJ1bGtBY3Rpb24uZmluZCgndWwgbGk6Zmlyc3QgYScpO1xyXG5cdFx0XHRcdFx0anNlLmxpYnMuYnV0dG9uX2Ryb3Bkb3duLnNldERlZmF1bHRBY3Rpb24oJGJ1bGtBY3Rpb24sICRhY3Rpb25MaW5rKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFxyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdHRocm93IG5ldyBFcnJvcihgSW52YWxpZCBcImVuc3VyZVwiIHR5cGUgZ2l2ZW4gKGV4cGVjdGVkIFwicm93XCIgb3IgXCJidWxrXCIgZ290IDogXCIke3R5cGV9XCIpLmApO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBCdXR0b24gRHJvZHBvd24gQWN0aW9uIENsaWNrXHJcblx0ICpcclxuXHQgKiBVcGRhdGUgdGhlIGRlZmF1bHRCdWxrQWN0aW9uIGFuZCBkZWZhdWx0Um93QWN0aW9uIGRhdGEgYXR0cmlidXRlcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfb25CdXR0b25Ecm9wZG93bkFjdGlvbkNsaWNrKCkge1xyXG5cdFx0Y29uc3QgcHJvcGVydHkgPSAkKHRoaXMpLnBhcmVudHMoJy5idG4tZ3JvdXAnKVswXSA9PT0gJChvcHRpb25zLmJ1bGtBY3Rpb25TZWxlY3RvcilbMF1cclxuXHRcdFx0PyAnZGVmYXVsdEJ1bGtBY3Rpb24nIDogJ2RlZmF1bHRSb3dBY3Rpb24nO1xyXG5cdFx0XHJcblx0XHQkdGhpcy5kYXRhKHByb3BlcnR5LCAkKHRoaXMpLmRhdGEoJ2NvbmZpZ3VyYXRpb25WYWx1ZScpKTtcclxuXHR9XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gSU5JVElBTElaQVRJT05cclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdCR0aGlzLmRhdGEoe1xyXG5cdFx0XHRkZWZhdWx0Um93QWN0aW9uOiBvcHRpb25zLnJvdyxcclxuXHRcdFx0ZGVmYXVsdEJ1bGtBY3Rpb246IG9wdGlvbnMuYnVsa1xyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdCR0aGlzLm9uKCdjbGljaycsICcuYnRuLWdyb3VwLmRyb3Bkb3duIGEnLCBfb25CdXR0b25Ecm9wZG93bkFjdGlvbkNsaWNrKTtcclxuXHRcdCQoJ2JvZHknKS5vbignY2xpY2snLCBvcHRpb25zLmJ1bGtBY3Rpb25TZWxlY3RvciwgX29uQnV0dG9uRHJvcGRvd25BY3Rpb25DbGljayk7XHJcblx0XHRcclxuXHRcdC8vIEJpbmQgbW9kdWxlIGFwaSB0byBqUXVlcnkgb2JqZWN0LiBcclxuXHRcdCQuZm4uZXh0ZW5kKHtcclxuXHRcdFx0ZGF0YXRhYmxlX2RlZmF1bHRfYWN0aW9uczogZnVuY3Rpb24oYWN0aW9uLCAuLi5hcmdzKSB7XHJcblx0XHRcdFx0c3dpdGNoIChhY3Rpb24pIHtcclxuXHRcdFx0XHRcdGNhc2UgJ2Vuc3VyZSc6XHJcblx0XHRcdFx0XHRcdHJldHVybiBfZW5zdXJlLmFwcGx5KHRoaXMsIGFyZ3MpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGRvbmUoKTtcclxuXHR9O1xyXG5cdFxyXG5cdHJldHVybiBtb2R1bGU7XHJcblx0XHJcbn0pOyJdfQ==
