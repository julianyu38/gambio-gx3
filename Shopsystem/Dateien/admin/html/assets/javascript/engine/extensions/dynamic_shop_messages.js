'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* --------------------------------------------------------------
 dynamic_shop_messages.js 2016-05-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Dynamic Shop Messages
 *
 * This extension module is meant to be executed once in every page load. Its purpose is to display
 * custom notifications into various positions of the HTML. The notification source may vary in each
 * case but the original data should come from Gambio's Customer Portal.
 *
 * The module supports the use of a "url" option which will be used for loading the JSON data through an
 * AJAX call.
 * 
 * ### Options 
 * 
 * **Data Source URL | `data-dynamic_shop_messages-url` | String | Optional**
 * 
 * Provide the URL which will be used to fetch the dynamic shop messages. By default the DynamicShopMessages
 * controller will be used. 
 * 
 * **Response Envelope | `data-dynamic_shop_messages-response-envelope` | String | Optional**
 * 
 * Set a custom response envelop for the response object. By default "MESSAGES" will be used, because this is 
 * the envelope from the Gambio Portal response. 
 *
 * ### Example
 * 
 * ```html
 * <div data-gx-extension="dynamic_shop_messages"
 *     data-dynamic_shop_messages-url="http://custom-url.com/myscript.php"
 *     data-dynamic_shop_messages-response-envelope="MESSAGES">
 *   <-- HTML CONTENT -->
 * </div>
 * ```
 *
 * @module Admin/Extensions/dynamic_shop_messages
 * @ignore
 */
gx.extensions.module('dynamic_shop_messages', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		url: jse.core.config.get('appUrl') + '/admin/admin.php?do=DynamicShopMessages',
		lifetime: 30000, // maximum search lifetime (ms)
		responseEnvelope: 'MESSAGES'
	},


	/**
  * Final Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Checks if an HTML markup string is valid.
  *
  * {@link http://stackoverflow.com/a/14216406}
  *
  * @param {string} html The HTML markup to be validated.
  *
  * @returns {bool} Returns the validation result.
  */
	var _validateHtml = function _validateHtml(html) {
		var doc = document.createElement('div');
		doc.innerHTML = html;
		return doc.innerHTML === html;
	};

	/**
  * Check the current page matches the target_page value of the JSON data.
  *
  * @param {string|array} targetPageValue Contains a URL string or an array of URLs to be matched.
  *
  * @return {bool} Returns the validation check.
  */
	var _checkTargetPage = function _checkTargetPage(targetPageValue) {
		var result = false;

		if ((typeof targetPageValue === 'undefined' ? 'undefined' : _typeof(targetPageValue)) !== 'object') {
			targetPageValue = [targetPageValue];
		}

		$.each(targetPageValue, function () {
			var regex = new RegExp(this);

			if (window.location.href === jse.core.config.get('appUrl') + '/admin/' + this || regex.test(window.location.href)) {
				result = true;
				return false; // exit loop
			}
		});

		return result;
	};

	/**
  * Try to apply the dynamic message data into the page.
  *
  * @param {array} messages
  */
	var _apply = function _apply(messages) {
		$.each(messages, function (index, entry) {
			try {
				// Check if we have target information in the message entry.
				if (entry.target_page === undefined || entry.target_selector === undefined) {
					throw new TypeError('No target information provided. Skipping to the next entry...');
				}

				// Check if we are in the target page.
				if (!_checkTargetPage(entry.target_page)) {
					throw new TypeError('The entry is not targeted for the current page. Skipping to the next entry...');
				}

				// Find the target selector and append the HTML message. The module will keep on searching
				// for the target selector for as long as the "options.lifetime" value is.
				var currentTimestamp = Date.now;

				var intv = setInterval(function () {
					var $target = $this.find(entry.target_selector);

					if ($target.length > 0) {
						var htmlBackup = $target.html();
						$target.append(entry.message);

						// Check if the current HTML is valid and revert it otherwise.
						if (!_validateHtml($target.html())) {
							$target.html(htmlBackup);
							jse.core.debug.error('Dynamic message couldn\'t be applied.', entry);
						}

						clearInterval(intv); // stop searching
					}

					if (Date.now - currentTimestamp > options.lifetime) {
						clearInterval(intv);
						throw Error('Search lifetime limit exceeded, no element matched the provided selector.');
					}
				}, 300);
			} catch (e) {
				return true; // Continue loop with next message entry.
			}
		});
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(document).on('JSENGINE_INIT_FINISHED', function () {
			$.getJSON(options.url).done(function (response) {
				_apply(response[options.responseEnvelope]);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				jse.core.debug.info('Could not load the dynamic shop messages.', jqXHR, textStatus, errorThrown);
			});
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImR5bmFtaWNfc2hvcF9tZXNzYWdlcy5qcyJdLCJuYW1lcyI6WyJneCIsImV4dGVuc2lvbnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJ1cmwiLCJqc2UiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwibGlmZXRpbWUiLCJyZXNwb25zZUVudmVsb3BlIiwib3B0aW9ucyIsImV4dGVuZCIsIl92YWxpZGF0ZUh0bWwiLCJodG1sIiwiZG9jIiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50IiwiaW5uZXJIVE1MIiwiX2NoZWNrVGFyZ2V0UGFnZSIsInRhcmdldFBhZ2VWYWx1ZSIsInJlc3VsdCIsImVhY2giLCJyZWdleCIsIlJlZ0V4cCIsIndpbmRvdyIsImxvY2F0aW9uIiwiaHJlZiIsInRlc3QiLCJfYXBwbHkiLCJtZXNzYWdlcyIsImluZGV4IiwiZW50cnkiLCJ0YXJnZXRfcGFnZSIsInVuZGVmaW5lZCIsInRhcmdldF9zZWxlY3RvciIsIlR5cGVFcnJvciIsImN1cnJlbnRUaW1lc3RhbXAiLCJEYXRlIiwibm93IiwiaW50diIsInNldEludGVydmFsIiwiJHRhcmdldCIsImZpbmQiLCJsZW5ndGgiLCJodG1sQmFja3VwIiwiYXBwZW5kIiwibWVzc2FnZSIsImRlYnVnIiwiZXJyb3IiLCJjbGVhckludGVydmFsIiwiRXJyb3IiLCJlIiwiaW5pdCIsImRvbmUiLCJvbiIsImdldEpTT04iLCJyZXNwb25zZSIsImZhaWwiLCJqcVhIUiIsInRleHRTdGF0dXMiLCJlcnJvclRocm93biIsImluZm8iXSwibWFwcGluZ3MiOiI7Ozs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQUEsR0FBR0MsVUFBSCxDQUFjQyxNQUFkLENBQ0MsdUJBREQsRUFHQyxFQUhELEVBS0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFlBQVc7QUFDVkMsT0FBS0MsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyx5Q0FEM0I7QUFFVkMsWUFBVSxLQUZBLEVBRU87QUFDakJDLG9CQUFrQjtBQUhSLEVBYlo7OztBQW1CQzs7Ozs7QUFLQUMsV0FBVVQsRUFBRVUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CVCxRQUFuQixFQUE2QkgsSUFBN0IsQ0F4Qlg7OztBQTBCQzs7Ozs7QUFLQUQsVUFBUyxFQS9CVjs7QUFpQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7QUFTQSxLQUFJYyxnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVNDLElBQVQsRUFBZTtBQUNsQyxNQUFJQyxNQUFNQyxTQUFTQyxhQUFULENBQXVCLEtBQXZCLENBQVY7QUFDQUYsTUFBSUcsU0FBSixHQUFnQkosSUFBaEI7QUFDQSxTQUFRQyxJQUFJRyxTQUFKLEtBQWtCSixJQUExQjtBQUNBLEVBSkQ7O0FBTUE7Ozs7Ozs7QUFPQSxLQUFJSyxtQkFBbUIsU0FBbkJBLGdCQUFtQixDQUFTQyxlQUFULEVBQTBCO0FBQ2hELE1BQUlDLFNBQVMsS0FBYjs7QUFFQSxNQUFJLFFBQU9ELGVBQVAseUNBQU9BLGVBQVAsT0FBMkIsUUFBL0IsRUFBeUM7QUFDeENBLHFCQUFrQixDQUFDQSxlQUFELENBQWxCO0FBQ0E7O0FBRURsQixJQUFFb0IsSUFBRixDQUFPRixlQUFQLEVBQXdCLFlBQVc7QUFDbEMsT0FBSUcsUUFBUSxJQUFJQyxNQUFKLENBQVcsSUFBWCxDQUFaOztBQUVBLE9BQUlDLE9BQU9DLFFBQVAsQ0FBZ0JDLElBQWhCLEtBQXlCdEIsSUFBSUMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixRQUFwQixJQUFnQyxTQUFoQyxHQUE0QyxJQUFyRSxJQUNBZSxNQUFNSyxJQUFOLENBQVdILE9BQU9DLFFBQVAsQ0FBZ0JDLElBQTNCLENBREosRUFDc0M7QUFDckNOLGFBQVMsSUFBVDtBQUNBLFdBQU8sS0FBUCxDQUZxQyxDQUV2QjtBQUNkO0FBQ0QsR0FSRDs7QUFVQSxTQUFPQSxNQUFQO0FBQ0EsRUFsQkQ7O0FBb0JBOzs7OztBQUtBLEtBQUlRLFNBQVMsU0FBVEEsTUFBUyxDQUFTQyxRQUFULEVBQW1CO0FBQy9CNUIsSUFBRW9CLElBQUYsQ0FBT1EsUUFBUCxFQUFpQixVQUFTQyxLQUFULEVBQWdCQyxLQUFoQixFQUF1QjtBQUN2QyxPQUFJO0FBQ0g7QUFDQSxRQUFJQSxNQUFNQyxXQUFOLEtBQXNCQyxTQUF0QixJQUFtQ0YsTUFBTUcsZUFBTixLQUEwQkQsU0FBakUsRUFBNEU7QUFDM0UsV0FBTSxJQUFJRSxTQUFKLENBQWMsK0RBQWQsQ0FBTjtBQUNBOztBQUVEO0FBQ0EsUUFBSSxDQUFDakIsaUJBQWlCYSxNQUFNQyxXQUF2QixDQUFMLEVBQTBDO0FBQ3pDLFdBQU0sSUFBSUcsU0FBSixDQUNMLCtFQURLLENBQU47QUFFQTs7QUFFRDtBQUNBO0FBQ0EsUUFBSUMsbUJBQW1CQyxLQUFLQyxHQUE1Qjs7QUFFQSxRQUFJQyxPQUFPQyxZQUFZLFlBQVc7QUFDakMsU0FBSUMsVUFBVXpDLE1BQU0wQyxJQUFOLENBQVdYLE1BQU1HLGVBQWpCLENBQWQ7O0FBRUEsU0FBSU8sUUFBUUUsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUN2QixVQUFJQyxhQUFhSCxRQUFRNUIsSUFBUixFQUFqQjtBQUNBNEIsY0FBUUksTUFBUixDQUFlZCxNQUFNZSxPQUFyQjs7QUFFQTtBQUNBLFVBQUksQ0FBQ2xDLGNBQWM2QixRQUFRNUIsSUFBUixFQUFkLENBQUwsRUFBb0M7QUFDbkM0QixlQUFRNUIsSUFBUixDQUFhK0IsVUFBYjtBQUNBeEMsV0FBSUMsSUFBSixDQUFTMEMsS0FBVCxDQUFlQyxLQUFmLENBQXFCLHVDQUFyQixFQUE4RGpCLEtBQTlEO0FBQ0E7O0FBRURrQixvQkFBY1YsSUFBZCxFQVZ1QixDQVVGO0FBQ3JCOztBQUVELFNBQUlGLEtBQUtDLEdBQUwsR0FBV0YsZ0JBQVgsR0FBOEIxQixRQUFRRixRQUExQyxFQUFvRDtBQUNuRHlDLG9CQUFjVixJQUFkO0FBQ0EsWUFBTVcsTUFDTCwyRUFESyxDQUFOO0FBRUE7QUFDRCxLQXJCVSxFQXFCUixHQXJCUSxDQUFYO0FBdUJBLElBdkNELENBdUNFLE9BQU9DLENBQVAsRUFBVTtBQUNYLFdBQU8sSUFBUCxDQURXLENBQ0U7QUFDYjtBQUNELEdBM0NEO0FBNENBLEVBN0NEOztBQStDQTtBQUNBO0FBQ0E7O0FBRUFyRCxRQUFPc0QsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QnBELElBQUVjLFFBQUYsRUFBWXVDLEVBQVosQ0FBZSx3QkFBZixFQUF5QyxZQUFXO0FBQ25EckQsS0FBRXNELE9BQUYsQ0FBVTdDLFFBQVFQLEdBQWxCLEVBQ0VrRCxJQURGLENBQ08sVUFBU0csUUFBVCxFQUFtQjtBQUN4QjVCLFdBQU80QixTQUFTOUMsUUFBUUQsZ0JBQWpCLENBQVA7QUFDQSxJQUhGLEVBSUVnRCxJQUpGLENBSU8sVUFBU0MsS0FBVCxFQUFnQkMsVUFBaEIsRUFBNEJDLFdBQTVCLEVBQXlDO0FBQzlDeEQsUUFBSUMsSUFBSixDQUFTMEMsS0FBVCxDQUFlYyxJQUFmLENBQW9CLDJDQUFwQixFQUFpRUgsS0FBakUsRUFBd0VDLFVBQXhFLEVBQ0NDLFdBREQ7QUFFQSxJQVBGO0FBUUEsR0FURDs7QUFXQVA7QUFDQSxFQWJEOztBQWVBLFFBQU92RCxNQUFQO0FBQ0EsQ0FwS0YiLCJmaWxlIjoiZHluYW1pY19zaG9wX21lc3NhZ2VzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBkeW5hbWljX3Nob3BfbWVzc2FnZXMuanMgMjAxNi0wNS0xMVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgRHluYW1pYyBTaG9wIE1lc3NhZ2VzXG4gKlxuICogVGhpcyBleHRlbnNpb24gbW9kdWxlIGlzIG1lYW50IHRvIGJlIGV4ZWN1dGVkIG9uY2UgaW4gZXZlcnkgcGFnZSBsb2FkLiBJdHMgcHVycG9zZSBpcyB0byBkaXNwbGF5XG4gKiBjdXN0b20gbm90aWZpY2F0aW9ucyBpbnRvIHZhcmlvdXMgcG9zaXRpb25zIG9mIHRoZSBIVE1MLiBUaGUgbm90aWZpY2F0aW9uIHNvdXJjZSBtYXkgdmFyeSBpbiBlYWNoXG4gKiBjYXNlIGJ1dCB0aGUgb3JpZ2luYWwgZGF0YSBzaG91bGQgY29tZSBmcm9tIEdhbWJpbydzIEN1c3RvbWVyIFBvcnRhbC5cbiAqXG4gKiBUaGUgbW9kdWxlIHN1cHBvcnRzIHRoZSB1c2Ugb2YgYSBcInVybFwiIG9wdGlvbiB3aGljaCB3aWxsIGJlIHVzZWQgZm9yIGxvYWRpbmcgdGhlIEpTT04gZGF0YSB0aHJvdWdoIGFuXG4gKiBBSkFYIGNhbGwuXG4gKiBcbiAqICMjIyBPcHRpb25zIFxuICogXG4gKiAqKkRhdGEgU291cmNlIFVSTCB8IGBkYXRhLWR5bmFtaWNfc2hvcF9tZXNzYWdlcy11cmxgIHwgU3RyaW5nIHwgT3B0aW9uYWwqKlxuICogXG4gKiBQcm92aWRlIHRoZSBVUkwgd2hpY2ggd2lsbCBiZSB1c2VkIHRvIGZldGNoIHRoZSBkeW5hbWljIHNob3AgbWVzc2FnZXMuIEJ5IGRlZmF1bHQgdGhlIER5bmFtaWNTaG9wTWVzc2FnZXNcbiAqIGNvbnRyb2xsZXIgd2lsbCBiZSB1c2VkLiBcbiAqIFxuICogKipSZXNwb25zZSBFbnZlbG9wZSB8IGBkYXRhLWR5bmFtaWNfc2hvcF9tZXNzYWdlcy1yZXNwb25zZS1lbnZlbG9wZWAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKiBcbiAqIFNldCBhIGN1c3RvbSByZXNwb25zZSBlbnZlbG9wIGZvciB0aGUgcmVzcG9uc2Ugb2JqZWN0LiBCeSBkZWZhdWx0IFwiTUVTU0FHRVNcIiB3aWxsIGJlIHVzZWQsIGJlY2F1c2UgdGhpcyBpcyBcbiAqIHRoZSBlbnZlbG9wZSBmcm9tIHRoZSBHYW1iaW8gUG9ydGFsIHJlc3BvbnNlLiBcbiAqXG4gKiAjIyMgRXhhbXBsZVxuICogXG4gKiBgYGBodG1sXG4gKiA8ZGl2IGRhdGEtZ3gtZXh0ZW5zaW9uPVwiZHluYW1pY19zaG9wX21lc3NhZ2VzXCJcbiAqICAgICBkYXRhLWR5bmFtaWNfc2hvcF9tZXNzYWdlcy11cmw9XCJodHRwOi8vY3VzdG9tLXVybC5jb20vbXlzY3JpcHQucGhwXCJcbiAqICAgICBkYXRhLWR5bmFtaWNfc2hvcF9tZXNzYWdlcy1yZXNwb25zZS1lbnZlbG9wZT1cIk1FU1NBR0VTXCI+XG4gKiAgIDwtLSBIVE1MIENPTlRFTlQgLS0+XG4gKiA8L2Rpdj5cbiAqIGBgYFxuICpcbiAqIEBtb2R1bGUgQWRtaW4vRXh0ZW5zaW9ucy9keW5hbWljX3Nob3BfbWVzc2FnZXNcbiAqIEBpZ25vcmVcbiAqL1xuZ3guZXh0ZW5zaW9ucy5tb2R1bGUoXG5cdCdkeW5hbWljX3Nob3BfbWVzc2FnZXMnLFxuXHRcblx0W10sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdGxldFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdHVybDoganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluL2FkbWluLnBocD9kbz1EeW5hbWljU2hvcE1lc3NhZ2VzJyxcblx0XHRcdFx0bGlmZXRpbWU6IDMwMDAwLCAvLyBtYXhpbXVtIHNlYXJjaCBsaWZldGltZSAobXMpXG5cdFx0XHRcdHJlc3BvbnNlRW52ZWxvcGU6ICdNRVNTQUdFUydcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDaGVja3MgaWYgYW4gSFRNTCBtYXJrdXAgc3RyaW5nIGlzIHZhbGlkLlxuXHRcdCAqXG5cdFx0ICoge0BsaW5rIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzE0MjE2NDA2fVxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGh0bWwgVGhlIEhUTUwgbWFya3VwIHRvIGJlIHZhbGlkYXRlZC5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm5zIHtib29sfSBSZXR1cm5zIHRoZSB2YWxpZGF0aW9uIHJlc3VsdC5cblx0XHQgKi9cblx0XHR2YXIgX3ZhbGlkYXRlSHRtbCA9IGZ1bmN0aW9uKGh0bWwpIHtcblx0XHRcdHZhciBkb2MgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcblx0XHRcdGRvYy5pbm5lckhUTUwgPSBodG1sO1xuXHRcdFx0cmV0dXJuIChkb2MuaW5uZXJIVE1MID09PSBodG1sKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENoZWNrIHRoZSBjdXJyZW50IHBhZ2UgbWF0Y2hlcyB0aGUgdGFyZ2V0X3BhZ2UgdmFsdWUgb2YgdGhlIEpTT04gZGF0YS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfGFycmF5fSB0YXJnZXRQYWdlVmFsdWUgQ29udGFpbnMgYSBVUkwgc3RyaW5nIG9yIGFuIGFycmF5IG9mIFVSTHMgdG8gYmUgbWF0Y2hlZC5cblx0XHQgKlxuXHRcdCAqIEByZXR1cm4ge2Jvb2x9IFJldHVybnMgdGhlIHZhbGlkYXRpb24gY2hlY2suXG5cdFx0ICovXG5cdFx0dmFyIF9jaGVja1RhcmdldFBhZ2UgPSBmdW5jdGlvbih0YXJnZXRQYWdlVmFsdWUpIHtcblx0XHRcdHZhciByZXN1bHQgPSBmYWxzZTtcblx0XHRcdFxuXHRcdFx0aWYgKHR5cGVvZiB0YXJnZXRQYWdlVmFsdWUgIT09ICdvYmplY3QnKSB7XG5cdFx0XHRcdHRhcmdldFBhZ2VWYWx1ZSA9IFt0YXJnZXRQYWdlVmFsdWVdO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkLmVhY2godGFyZ2V0UGFnZVZhbHVlLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyIHJlZ2V4ID0gbmV3IFJlZ0V4cCh0aGlzKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICh3aW5kb3cubG9jYXRpb24uaHJlZiA9PT0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJykgKyAnL2FkbWluLycgKyB0aGlzXG5cdFx0XHRcdFx0fHwgcmVnZXgudGVzdCh3aW5kb3cubG9jYXRpb24uaHJlZikpIHtcblx0XHRcdFx0XHRyZXN1bHQgPSB0cnVlO1xuXHRcdFx0XHRcdHJldHVybiBmYWxzZTsgLy8gZXhpdCBsb29wXG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gcmVzdWx0O1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVHJ5IHRvIGFwcGx5IHRoZSBkeW5hbWljIG1lc3NhZ2UgZGF0YSBpbnRvIHRoZSBwYWdlLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHthcnJheX0gbWVzc2FnZXNcblx0XHQgKi9cblx0XHR2YXIgX2FwcGx5ID0gZnVuY3Rpb24obWVzc2FnZXMpIHtcblx0XHRcdCQuZWFjaChtZXNzYWdlcywgZnVuY3Rpb24oaW5kZXgsIGVudHJ5KSB7XG5cdFx0XHRcdHRyeSB7XG5cdFx0XHRcdFx0Ly8gQ2hlY2sgaWYgd2UgaGF2ZSB0YXJnZXQgaW5mb3JtYXRpb24gaW4gdGhlIG1lc3NhZ2UgZW50cnkuXG5cdFx0XHRcdFx0aWYgKGVudHJ5LnRhcmdldF9wYWdlID09PSB1bmRlZmluZWQgfHwgZW50cnkudGFyZ2V0X3NlbGVjdG9yID09PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ05vIHRhcmdldCBpbmZvcm1hdGlvbiBwcm92aWRlZC4gU2tpcHBpbmcgdG8gdGhlIG5leHQgZW50cnkuLi4nKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gQ2hlY2sgaWYgd2UgYXJlIGluIHRoZSB0YXJnZXQgcGFnZS5cblx0XHRcdFx0XHRpZiAoIV9jaGVja1RhcmdldFBhZ2UoZW50cnkudGFyZ2V0X3BhZ2UpKSB7XG5cdFx0XHRcdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKFxuXHRcdFx0XHRcdFx0XHQnVGhlIGVudHJ5IGlzIG5vdCB0YXJnZXRlZCBmb3IgdGhlIGN1cnJlbnQgcGFnZS4gU2tpcHBpbmcgdG8gdGhlIG5leHQgZW50cnkuLi4nKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gRmluZCB0aGUgdGFyZ2V0IHNlbGVjdG9yIGFuZCBhcHBlbmQgdGhlIEhUTUwgbWVzc2FnZS4gVGhlIG1vZHVsZSB3aWxsIGtlZXAgb24gc2VhcmNoaW5nXG5cdFx0XHRcdFx0Ly8gZm9yIHRoZSB0YXJnZXQgc2VsZWN0b3IgZm9yIGFzIGxvbmcgYXMgdGhlIFwib3B0aW9ucy5saWZldGltZVwiIHZhbHVlIGlzLlxuXHRcdFx0XHRcdHZhciBjdXJyZW50VGltZXN0YW1wID0gRGF0ZS5ub3c7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0dmFyIGludHYgPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHZhciAkdGFyZ2V0ID0gJHRoaXMuZmluZChlbnRyeS50YXJnZXRfc2VsZWN0b3IpO1xuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRpZiAoJHRhcmdldC5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdFx0XHRcdHZhciBodG1sQmFja3VwID0gJHRhcmdldC5odG1sKCk7XG5cdFx0XHRcdFx0XHRcdCR0YXJnZXQuYXBwZW5kKGVudHJ5Lm1lc3NhZ2UpO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0Ly8gQ2hlY2sgaWYgdGhlIGN1cnJlbnQgSFRNTCBpcyB2YWxpZCBhbmQgcmV2ZXJ0IGl0IG90aGVyd2lzZS5cblx0XHRcdFx0XHRcdFx0aWYgKCFfdmFsaWRhdGVIdG1sKCR0YXJnZXQuaHRtbCgpKSkge1xuXHRcdFx0XHRcdFx0XHRcdCR0YXJnZXQuaHRtbChodG1sQmFja3VwKTtcblx0XHRcdFx0XHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5lcnJvcignRHluYW1pYyBtZXNzYWdlIGNvdWxkblxcJ3QgYmUgYXBwbGllZC4nLCBlbnRyeSk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdGNsZWFySW50ZXJ2YWwoaW50dik7IC8vIHN0b3Agc2VhcmNoaW5nXG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGlmIChEYXRlLm5vdyAtIGN1cnJlbnRUaW1lc3RhbXAgPiBvcHRpb25zLmxpZmV0aW1lKSB7XG5cdFx0XHRcdFx0XHRcdGNsZWFySW50ZXJ2YWwoaW50dik7XG5cdFx0XHRcdFx0XHRcdHRocm93IEVycm9yKFxuXHRcdFx0XHRcdFx0XHRcdCdTZWFyY2ggbGlmZXRpbWUgbGltaXQgZXhjZWVkZWQsIG5vIGVsZW1lbnQgbWF0Y2hlZCB0aGUgcHJvdmlkZWQgc2VsZWN0b3IuJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSwgMzAwKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0fSBjYXRjaCAoZSkge1xuXHRcdFx0XHRcdHJldHVybiB0cnVlOyAvLyBDb250aW51ZSBsb29wIHdpdGggbmV4dCBtZXNzYWdlIGVudHJ5LlxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkKGRvY3VtZW50KS5vbignSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkLmdldEpTT04ob3B0aW9ucy51cmwpXG5cdFx0XHRcdFx0LmRvbmUoZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRcdF9hcHBseShyZXNwb25zZVtvcHRpb25zLnJlc3BvbnNlRW52ZWxvcGVdKTtcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5mYWlsKGZ1bmN0aW9uKGpxWEhSLCB0ZXh0U3RhdHVzLCBlcnJvclRocm93bikge1xuXHRcdFx0XHRcdFx0anNlLmNvcmUuZGVidWcuaW5mbygnQ291bGQgbm90IGxvYWQgdGhlIGR5bmFtaWMgc2hvcCBtZXNzYWdlcy4nLCBqcVhIUiwgdGV4dFN0YXR1cyxcblx0XHRcdFx0XHRcdFx0ZXJyb3JUaHJvd24pO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
