'use strict';

/* --------------------------------------------------------------
 disable_ckedit.js 2016-09-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Disable CKEdit
 *
 * Extension to enable or disable (readonly) CKEditors corresponding to a checkbox value.
 * 
 * **Important: This widget is not used anymore and will be removed in the future.** 
 *
 * @deprecated Since v1.5 will be removed in v1.7
 * 
 * @module Admin/Extensions/disable_ckedit
 * @ignore
 */
gx.extensions.module('disable_ckedit', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE INITIALIZATION
	// ------------------------------------------------------------------------

	var
	/**
  * Extension Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Options for Extension
  *
  * @type {object}
  */
	defaults = {
		'invert': false // if true, the checkbox has to be deselected to enable the ckeditor
	},


	/**
  * Final Extension Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {},


	/**
  * Interval
  *
  * @type {number}
  */
	interval = null;

	// ------------------------------------------------------------------------
	// EVENT HANDLER
	// ------------------------------------------------------------------------

	/**
  * Switch CKEdit
  *
  * Function to detect if a CKEdit is bound to the target text field. If so,
  * set the readonly state of the box corresponding to the checkbox value.
  */
	var _switchCkEdit = function _switchCkEdit() {
		if (window.CKEDITOR && CKEDITOR.instances && CKEDITOR.instances[options.target]) {

			if (interval) {
				clearInterval(interval);
			}

			var checked = $this.prop('checked');
			checked = options.invert ? !checked : checked;
			try {
				CKEDITOR.instances[options.target].setReadOnly(!checked);
			} catch (err) {
				interval = setInterval(function () {
					CKEDITOR.instances[options.target].setReadOnly(!checked);
					clearInterval(interval);
				}, 100);
			}
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize function of the extension, called by the engine.
  */
	module.init = function (done) {
		$this.on('change', _switchCkEdit);
		_switchCkEdit();
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpc2FibGVfY2tlZGl0LmpzIl0sIm5hbWVzIjpbImd4IiwiZXh0ZW5zaW9ucyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJpbnRlcnZhbCIsIl9zd2l0Y2hDa0VkaXQiLCJ3aW5kb3ciLCJDS0VESVRPUiIsImluc3RhbmNlcyIsInRhcmdldCIsImNsZWFySW50ZXJ2YWwiLCJjaGVja2VkIiwicHJvcCIsImludmVydCIsInNldFJlYWRPbmx5IiwiZXJyIiwic2V0SW50ZXJ2YWwiLCJpbml0IiwiZG9uZSIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7OztBQVlBQSxHQUFHQyxVQUFILENBQWNDLE1BQWQsQ0FDQyxnQkFERCxFQUdDLEVBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsWUFBVztBQUNWLFlBQVUsS0FEQSxDQUNNO0FBRE4sRUFiWjs7O0FBaUJDOzs7OztBQUtBQyxXQUFVRixFQUFFRyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCSCxJQUE3QixDQXRCWDs7O0FBd0JDOzs7OztBQUtBRCxVQUFTLEVBN0JWOzs7QUErQkM7Ozs7O0FBS0FPLFlBQVcsSUFwQ1o7O0FBc0NBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUEsS0FBSUMsZ0JBQWdCLFNBQWhCQSxhQUFnQixHQUFXO0FBQzlCLE1BQUlDLE9BQU9DLFFBQVAsSUFBbUJBLFNBQVNDLFNBQTVCLElBQXlDRCxTQUFTQyxTQUFULENBQW1CTixRQUFRTyxNQUEzQixDQUE3QyxFQUFpRjs7QUFFaEYsT0FBSUwsUUFBSixFQUFjO0FBQ2JNLGtCQUFjTixRQUFkO0FBQ0E7O0FBRUQsT0FBSU8sVUFBVVosTUFBTWEsSUFBTixDQUFXLFNBQVgsQ0FBZDtBQUNBRCxhQUFXVCxRQUFRVyxNQUFULEdBQW1CLENBQUNGLE9BQXBCLEdBQThCQSxPQUF4QztBQUNBLE9BQUk7QUFDSEosYUFBU0MsU0FBVCxDQUFtQk4sUUFBUU8sTUFBM0IsRUFBbUNLLFdBQW5DLENBQStDLENBQUNILE9BQWhEO0FBQ0EsSUFGRCxDQUVFLE9BQU9JLEdBQVAsRUFBWTtBQUNiWCxlQUFXWSxZQUFZLFlBQVc7QUFDakNULGNBQVNDLFNBQVQsQ0FBbUJOLFFBQVFPLE1BQTNCLEVBQW1DSyxXQUFuQyxDQUErQyxDQUFDSCxPQUFoRDtBQUNBRCxtQkFBY04sUUFBZDtBQUNBLEtBSFUsRUFHUixHQUhRLENBQVg7QUFJQTtBQUVEO0FBQ0QsRUFuQkQ7O0FBcUJBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0FQLFFBQU9vQixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCbkIsUUFBTW9CLEVBQU4sQ0FBUyxRQUFULEVBQW1CZCxhQUFuQjtBQUNBQTtBQUNBYTtBQUNBLEVBSkQ7O0FBTUE7QUFDQSxRQUFPckIsTUFBUDtBQUVBLENBbEdGIiwiZmlsZSI6ImRpc2FibGVfY2tlZGl0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBkaXNhYmxlX2NrZWRpdC5qcyAyMDE2LTA5LTA5XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBEaXNhYmxlIENLRWRpdFxuICpcbiAqIEV4dGVuc2lvbiB0byBlbmFibGUgb3IgZGlzYWJsZSAocmVhZG9ubHkpIENLRWRpdG9ycyBjb3JyZXNwb25kaW5nIHRvIGEgY2hlY2tib3ggdmFsdWUuXG4gKiBcbiAqICoqSW1wb3J0YW50OiBUaGlzIHdpZGdldCBpcyBub3QgdXNlZCBhbnltb3JlIGFuZCB3aWxsIGJlIHJlbW92ZWQgaW4gdGhlIGZ1dHVyZS4qKiBcbiAqXG4gKiBAZGVwcmVjYXRlZCBTaW5jZSB2MS41IHdpbGwgYmUgcmVtb3ZlZCBpbiB2MS43XG4gKiBcbiAqIEBtb2R1bGUgQWRtaW4vRXh0ZW5zaW9ucy9kaXNhYmxlX2NrZWRpdFxuICogQGlnbm9yZVxuICovXG5neC5leHRlbnNpb25zLm1vZHVsZShcblx0J2Rpc2FibGVfY2tlZGl0Jyxcblx0XG5cdFtdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBFeHRlbnNpb24gUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9ucyBmb3IgRXh0ZW5zaW9uXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdCdpbnZlcnQnOiBmYWxzZSAvLyBpZiB0cnVlLCB0aGUgY2hlY2tib3ggaGFzIHRvIGJlIGRlc2VsZWN0ZWQgdG8gZW5hYmxlIHRoZSBja2VkaXRvclxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBFeHRlbnNpb24gT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBJbnRlcnZhbFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtudW1iZXJ9XG5cdFx0XHQgKi9cblx0XHRcdGludGVydmFsID0gbnVsbDtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBFVkVOVCBIQU5ETEVSXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU3dpdGNoIENLRWRpdFxuXHRcdCAqXG5cdFx0ICogRnVuY3Rpb24gdG8gZGV0ZWN0IGlmIGEgQ0tFZGl0IGlzIGJvdW5kIHRvIHRoZSB0YXJnZXQgdGV4dCBmaWVsZC4gSWYgc28sXG5cdFx0ICogc2V0IHRoZSByZWFkb25seSBzdGF0ZSBvZiB0aGUgYm94IGNvcnJlc3BvbmRpbmcgdG8gdGhlIGNoZWNrYm94IHZhbHVlLlxuXHRcdCAqL1xuXHRcdHZhciBfc3dpdGNoQ2tFZGl0ID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRpZiAod2luZG93LkNLRURJVE9SICYmIENLRURJVE9SLmluc3RhbmNlcyAmJiBDS0VESVRPUi5pbnN0YW5jZXNbb3B0aW9ucy50YXJnZXRdKSB7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoaW50ZXJ2YWwpIHtcblx0XHRcdFx0XHRjbGVhckludGVydmFsKGludGVydmFsKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0dmFyIGNoZWNrZWQgPSAkdGhpcy5wcm9wKCdjaGVja2VkJyk7XG5cdFx0XHRcdGNoZWNrZWQgPSAob3B0aW9ucy5pbnZlcnQpID8gIWNoZWNrZWQgOiBjaGVja2VkO1xuXHRcdFx0XHR0cnkge1xuXHRcdFx0XHRcdENLRURJVE9SLmluc3RhbmNlc1tvcHRpb25zLnRhcmdldF0uc2V0UmVhZE9ubHkoIWNoZWNrZWQpO1xuXHRcdFx0XHR9IGNhdGNoIChlcnIpIHtcblx0XHRcdFx0XHRpbnRlcnZhbCA9IHNldEludGVydmFsKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0Q0tFRElUT1IuaW5zdGFuY2VzW29wdGlvbnMudGFyZ2V0XS5zZXRSZWFkT25seSghY2hlY2tlZCk7XG5cdFx0XHRcdFx0XHRjbGVhckludGVydmFsKGludGVydmFsKTtcblx0XHRcdFx0XHR9LCAxMDApO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIGZ1bmN0aW9uIG9mIHRoZSBleHRlbnNpb24sIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JHRoaXMub24oJ2NoYW5nZScsIF9zd2l0Y2hDa0VkaXQpO1xuXHRcdFx0X3N3aXRjaENrRWRpdCgpO1xuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHRcdFxuXHR9KTtcbiJdfQ==
