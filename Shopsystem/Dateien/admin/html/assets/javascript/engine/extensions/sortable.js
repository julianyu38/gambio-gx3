'use strict';

/* --------------------------------------------------------------
 sortable.js 2016-09-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Sortable Extension
 *
 * Uses jQuery UI's sortable plugin to realize sortable functionality. You can pass sortable options via
 * data attributes.
 *
 * ### Options
 *
 * **URL | data-sortable-handle | String | Optional**
 *
 * The destination URL to be used after the user clicks on the element.
 *
 * ### Example
 *
 * ```html
 * <div data-gx-extension="sortable" data-sortable-handle=".drag-handle">
 *   <img src="handle.jpg" class="drag-handle" />
 * </div>
 * ```
 *
 * This extension is based in the jQuery UI sortable interaction utility:
 * {@link http://api.jqueryui.com/sortable}
 *
 * @module Admin/Extensions/sortable
 * @requires jQueryUI
 */
gx.extensions.module('sortable', [jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	// Widget element.

	var $this = $(this);

	// Default widget options (Sortable options).
	var defaults = {
		revert: true,
		containment: 'parent'
	};

	// Widget options.
	var options = $.extend(true, {}, defaults, data);

	// Module instance.
	var module = {};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		// Enable sortable.
		$this.sortable(options);

		// Finish initialization.
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNvcnRhYmxlLmpzIl0sIm5hbWVzIjpbImd4IiwiZXh0ZW5zaW9ucyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsInJldmVydCIsImNvbnRhaW5tZW50Iiwib3B0aW9ucyIsImV4dGVuZCIsImluaXQiLCJzb3J0YWJsZSIsImRvbmUiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEwQkFBLEdBQUdDLFVBQUgsQ0FBY0MsTUFBZCxDQUNDLFVBREQsRUFHQyxDQUNJQyxJQUFJQyxNQURSLDBDQUVJRCxJQUFJQyxNQUZSLHdDQUhELEVBUUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFDQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTtBQUNBLEtBQU1DLFdBQVc7QUFDaEJDLFVBQVEsSUFEUTtBQUVoQkMsZUFBYTtBQUZHLEVBQWpCOztBQUtBO0FBQ0EsS0FBTUMsVUFBVUosRUFBRUssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CSixRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7QUFDQSxLQUFNSCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQUEsUUFBT1csSUFBUCxHQUFjLGdCQUFRO0FBQ3JCO0FBQ0FQLFFBQU1RLFFBQU4sQ0FBZUgsT0FBZjs7QUFFQTtBQUNBSTtBQUNBLEVBTkQ7O0FBUUEsUUFBT2IsTUFBUDtBQUNBLENBL0NGIiwiZmlsZSI6InNvcnRhYmxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBzb3J0YWJsZS5qcyAyMDE2LTA5LTEzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBTb3J0YWJsZSBFeHRlbnNpb25cbiAqXG4gKiBVc2VzIGpRdWVyeSBVSSdzIHNvcnRhYmxlIHBsdWdpbiB0byByZWFsaXplIHNvcnRhYmxlIGZ1bmN0aW9uYWxpdHkuIFlvdSBjYW4gcGFzcyBzb3J0YWJsZSBvcHRpb25zIHZpYVxuICogZGF0YSBhdHRyaWJ1dGVzLlxuICpcbiAqICMjIyBPcHRpb25zXG4gKlxuICogKipVUkwgfCBkYXRhLXNvcnRhYmxlLWhhbmRsZSB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqXG4gKiBUaGUgZGVzdGluYXRpb24gVVJMIHRvIGJlIHVzZWQgYWZ0ZXIgdGhlIHVzZXIgY2xpY2tzIG9uIHRoZSBlbGVtZW50LlxuICpcbiAqICMjIyBFeGFtcGxlXG4gKlxuICogYGBgaHRtbFxuICogPGRpdiBkYXRhLWd4LWV4dGVuc2lvbj1cInNvcnRhYmxlXCIgZGF0YS1zb3J0YWJsZS1oYW5kbGU9XCIuZHJhZy1oYW5kbGVcIj5cbiAqICAgPGltZyBzcmM9XCJoYW5kbGUuanBnXCIgY2xhc3M9XCJkcmFnLWhhbmRsZVwiIC8+XG4gKiA8L2Rpdj5cbiAqIGBgYFxuICpcbiAqIFRoaXMgZXh0ZW5zaW9uIGlzIGJhc2VkIGluIHRoZSBqUXVlcnkgVUkgc29ydGFibGUgaW50ZXJhY3Rpb24gdXRpbGl0eTpcbiAqIHtAbGluayBodHRwOi8vYXBpLmpxdWVyeXVpLmNvbS9zb3J0YWJsZX1cbiAqXG4gKiBAbW9kdWxlIEFkbWluL0V4dGVuc2lvbnMvc29ydGFibGVcbiAqIEByZXF1aXJlcyBqUXVlcnlVSVxuICovXG5neC5leHRlbnNpb25zLm1vZHVsZShcblx0J3NvcnRhYmxlJyxcblxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS11aS9qcXVlcnktdWkubWluLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS11aS9qcXVlcnktdWkubWluLmpzYCxcblx0XSxcblxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdFx0Ly8gV2lkZ2V0IGVsZW1lbnQuXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXG5cdFx0Ly8gRGVmYXVsdCB3aWRnZXQgb3B0aW9ucyAoU29ydGFibGUgb3B0aW9ucykuXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7XG5cdFx0XHRyZXZlcnQ6IHRydWUsXG5cdFx0XHRjb250YWlubWVudDogJ3BhcmVudCdcblx0XHR9O1xuXG5cdFx0Ly8gV2lkZ2V0IG9wdGlvbnMuXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cblx0XHQvLyBNb2R1bGUgaW5zdGFuY2UuXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIHdpZGdldCwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBkb25lID0+IHtcblx0XHRcdC8vIEVuYWJsZSBzb3J0YWJsZS5cblx0XHRcdCR0aGlzLnNvcnRhYmxlKG9wdGlvbnMpO1xuXG5cdFx0XHQvLyBGaW5pc2ggaW5pdGlhbGl6YXRpb24uXG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
