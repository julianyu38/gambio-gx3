'use strict';

/* --------------------------------------------------------------
 ajax_search.js 2015-10-15 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## AJAX Search Extension
 *
 * Enables the AJAX search and display for an element. This extension is used along with text_edit.js and 
 * ajax_search.js in the Gambio Admin "Text Edit | Texte Anpassen" page.
 *
 * @module Admin/Extensions/ajax_search
 * @ignore
 */
gx.extensions.module('ajax_search', ['form', jse.source + '/vendor/mustache.js/mustache.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Extension Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Options for Extension.
  *
  * @type {object}
  */
	defaults = {},


	/**
  * AJAX URL
  *
  * @type {string}
  */
	url = $this.attr('action'),


	/**
  * Final Extension Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// META INITIALIZE
	// ------------------------------------------------------------------------

	/**
  * Initialize function of the extension, called by the engine.
  */
	module.init = function (done) {

		var template = $(options.template).html(),
		    $target = $(options.target);

		$this.on('submit', function (event) {
			event.preventDefault();

			var data = jse.libs.form.getData($this);

			// Check for required fields.
			var abort = false;
			$this.find('[required]').each(function () {
				if ($(this).val() === '') {
					abort = true;
					return false; // exit $.each loop
				}
			});
			if (abort) {
				return; // abort because there is a missing field
			}

			$.ajax({
				'url': url,
				'method': 'post',
				'dataType': 'json',
				'data': data,
				'page_token': jse.core.config.get('pageToken')
			}).done(function (result) {
				var markup = Mustache.render(template, result.payload);
				$target.empty().append(markup).parent().show();
			});
		});

		done();
	};

	// Return data to module engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFqYXhfc2VhcmNoLmpzIl0sIm5hbWVzIjpbImd4IiwiZXh0ZW5zaW9ucyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsInVybCIsImF0dHIiLCJvcHRpb25zIiwiZXh0ZW5kIiwiaW5pdCIsImRvbmUiLCJ0ZW1wbGF0ZSIsImh0bWwiLCIkdGFyZ2V0IiwidGFyZ2V0Iiwib24iLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwibGlicyIsImZvcm0iLCJnZXREYXRhIiwiYWJvcnQiLCJmaW5kIiwiZWFjaCIsInZhbCIsImFqYXgiLCJjb3JlIiwiY29uZmlnIiwiZ2V0IiwicmVzdWx0IiwibWFya3VwIiwiTXVzdGFjaGUiLCJyZW5kZXIiLCJwYXlsb2FkIiwiZW1wdHkiLCJhcHBlbmQiLCJwYXJlbnQiLCJzaG93Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7OztBQVNBQSxHQUFHQyxVQUFILENBQWNDLE1BQWQsQ0FDQyxhQURELEVBR0MsQ0FDQyxNQURELEVBRUlDLElBQUlDLE1BRlIseUNBSEQsRUFRQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsWUFBVyxFQWJaOzs7QUFlQzs7Ozs7QUFLQUMsT0FBTUgsTUFBTUksSUFBTixDQUFXLFFBQVgsQ0FwQlA7OztBQXNCQzs7Ozs7QUFLQUMsV0FBVUosRUFBRUssTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CSixRQUFuQixFQUE2QkgsSUFBN0IsQ0EzQlg7OztBQTZCQzs7Ozs7QUFLQUgsVUFBUyxFQWxDVjs7QUFvQ0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQUEsUUFBT1csSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTs7QUFFNUIsTUFBSUMsV0FBV1IsRUFBRUksUUFBUUksUUFBVixFQUFvQkMsSUFBcEIsRUFBZjtBQUFBLE1BQ0NDLFVBQVVWLEVBQUVJLFFBQVFPLE1BQVYsQ0FEWDs7QUFHQVosUUFBTWEsRUFBTixDQUFTLFFBQVQsRUFBbUIsVUFBU0MsS0FBVCxFQUFnQjtBQUNsQ0EsU0FBTUMsY0FBTjs7QUFFQSxPQUFJaEIsT0FBT0YsSUFBSW1CLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxPQUFkLENBQXNCbEIsS0FBdEIsQ0FBWDs7QUFFQTtBQUNBLE9BQUltQixRQUFRLEtBQVo7QUFDQW5CLFNBQU1vQixJQUFOLENBQVcsWUFBWCxFQUF5QkMsSUFBekIsQ0FBOEIsWUFBVztBQUN4QyxRQUFJcEIsRUFBRSxJQUFGLEVBQVFxQixHQUFSLE9BQWtCLEVBQXRCLEVBQTBCO0FBQ3pCSCxhQUFRLElBQVI7QUFDQSxZQUFPLEtBQVAsQ0FGeUIsQ0FFWDtBQUNkO0FBQ0QsSUFMRDtBQU1BLE9BQUlBLEtBQUosRUFBVztBQUNWLFdBRFUsQ0FDRjtBQUNSOztBQUVEbEIsS0FBRXNCLElBQUYsQ0FBTztBQUNOLFdBQU9wQixHQUREO0FBRU4sY0FBVSxNQUZKO0FBR04sZ0JBQVksTUFITjtBQUlOLFlBQVFKLElBSkY7QUFLTixrQkFBY0YsSUFBSTJCLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsV0FBcEI7QUFMUixJQUFQLEVBTUdsQixJQU5ILENBTVEsVUFBU21CLE1BQVQsRUFBaUI7QUFDeEIsUUFBSUMsU0FBU0MsU0FBU0MsTUFBVCxDQUFnQnJCLFFBQWhCLEVBQTBCa0IsT0FBT0ksT0FBakMsQ0FBYjtBQUNBcEIsWUFDRXFCLEtBREYsR0FFRUMsTUFGRixDQUVTTCxNQUZULEVBR0VNLE1BSEYsR0FJRUMsSUFKRjtBQUtBLElBYkQ7QUFlQSxHQWhDRDs7QUFrQ0EzQjtBQUNBLEVBeENEOztBQTBDQTtBQUNBLFFBQU9aLE1BQVA7QUFDQSxDQXZHRiIsImZpbGUiOiJhamF4X3NlYXJjaC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gYWpheF9zZWFyY2guanMgMjAxNS0xMC0xNSBnbVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTUgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgQUpBWCBTZWFyY2ggRXh0ZW5zaW9uXG4gKlxuICogRW5hYmxlcyB0aGUgQUpBWCBzZWFyY2ggYW5kIGRpc3BsYXkgZm9yIGFuIGVsZW1lbnQuIFRoaXMgZXh0ZW5zaW9uIGlzIHVzZWQgYWxvbmcgd2l0aCB0ZXh0X2VkaXQuanMgYW5kIFxuICogYWpheF9zZWFyY2guanMgaW4gdGhlIEdhbWJpbyBBZG1pbiBcIlRleHQgRWRpdCB8IFRleHRlIEFucGFzc2VuXCIgcGFnZS5cbiAqXG4gKiBAbW9kdWxlIEFkbWluL0V4dGVuc2lvbnMvYWpheF9zZWFyY2hcbiAqIEBpZ25vcmVcbiAqL1xuZ3guZXh0ZW5zaW9ucy5tb2R1bGUoXG5cdCdhamF4X3NlYXJjaCcsXG5cdFxuXHRbXG5cdFx0J2Zvcm0nLFxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9tdXN0YWNoZS5qcy9tdXN0YWNoZS5taW4uanNgXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogRXh0ZW5zaW9uIFJlZmVyZW5jZVxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnMgZm9yIEV4dGVuc2lvbi5cblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEFKQVggVVJMXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge3N0cmluZ31cblx0XHRcdCAqL1xuXHRcdFx0dXJsID0gJHRoaXMuYXR0cignYWN0aW9uJyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgRXh0ZW5zaW9uIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gTUVUQSBJTklUSUFMSVpFXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBmdW5jdGlvbiBvZiB0aGUgZXh0ZW5zaW9uLCBjYWxsZWQgYnkgdGhlIGVuZ2luZS5cblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdFxuXHRcdFx0dmFyIHRlbXBsYXRlID0gJChvcHRpb25zLnRlbXBsYXRlKS5odG1sKCksXG5cdFx0XHRcdCR0YXJnZXQgPSAkKG9wdGlvbnMudGFyZ2V0KTtcblx0XHRcdFxuXHRcdFx0JHRoaXMub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGV2ZW50KSB7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFxuXHRcdFx0XHR2YXIgZGF0YSA9IGpzZS5saWJzLmZvcm0uZ2V0RGF0YSgkdGhpcyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBDaGVjayBmb3IgcmVxdWlyZWQgZmllbGRzLlxuXHRcdFx0XHR2YXIgYWJvcnQgPSBmYWxzZTtcblx0XHRcdFx0JHRoaXMuZmluZCgnW3JlcXVpcmVkXScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0aWYgKCQodGhpcykudmFsKCkgPT09ICcnKSB7XG5cdFx0XHRcdFx0XHRhYm9ydCA9IHRydWU7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7IC8vIGV4aXQgJC5lYWNoIGxvb3Bcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRpZiAoYWJvcnQpIHtcblx0XHRcdFx0XHRyZXR1cm47IC8vIGFib3J0IGJlY2F1c2UgdGhlcmUgaXMgYSBtaXNzaW5nIGZpZWxkXG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdFx0J3VybCc6IHVybCxcblx0XHRcdFx0XHQnbWV0aG9kJzogJ3Bvc3QnLFxuXHRcdFx0XHRcdCdkYXRhVHlwZSc6ICdqc29uJyxcblx0XHRcdFx0XHQnZGF0YSc6IGRhdGEsXG5cdFx0XHRcdFx0J3BhZ2VfdG9rZW4nOiBqc2UuY29yZS5jb25maWcuZ2V0KCdwYWdlVG9rZW4nKVxuXHRcdFx0XHR9KS5kb25lKGZ1bmN0aW9uKHJlc3VsdCkge1xuXHRcdFx0XHRcdHZhciBtYXJrdXAgPSBNdXN0YWNoZS5yZW5kZXIodGVtcGxhdGUsIHJlc3VsdC5wYXlsb2FkKTtcblx0XHRcdFx0XHQkdGFyZ2V0XG5cdFx0XHRcdFx0XHQuZW1wdHkoKVxuXHRcdFx0XHRcdFx0LmFwcGVuZChtYXJrdXApXG5cdFx0XHRcdFx0XHQucGFyZW50KClcblx0XHRcdFx0XHRcdC5zaG93KCk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byBtb2R1bGUgZW5naW5lXG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7XG4iXX0=
