'use strict';

/* --------------------------------------------------------------
 datatable_fixed_header.js 2016-07-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Enable Fixed DataTable Header
 *
 * The table header will remain in the viewport as the user scrolls down the page. The style change of this
 * module is a bit tricky because we need to remove the thead from the normal flow, something that breaks the
 * display of the table. Therefore a helper clone of the thead is used to maintain the table formatting.
 *
 * **Notice #1**: The .table-fixed-header class is styled by the _tables.scss and is part of this solution.
 *
 * **Notice #2**: This method will take into concern the .content-header element which shouldn't overlap the
 * table header.
 *
 * @module Admin/Extensions/datatable_fixed_header
 */
gx.extensions.module('datatable_fixed_header', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Table Header Selector
  *
  * @type {jQuery}
  */
	var $thead = $this.children('thead');

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	/**
  * Marks the end of the table.
  *
  * This value is used to stop the fixed header when the user reaches the end of the table.
  *
  * @type {Number}
  */
	var tableOffsetBottom = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * On DataTable Draw Event
  *
  * Re-calculate the table bottom offset value.
  */
	function _onDataTableDraw() {
		tableOffsetBottom = $this.offset().top + $this.height() - $thead.height();
	}

	/**
  * On DataTable Initialization
  *
  * Modify the table HTML and set the required event handling for the fixed header functionality.
  */
	function _onDataTableInit() {
		var $mainHeader = $('#main-header');
		var $contentHeader = $('.content-header');
		var $clone = $thead.clone();
		var originalTop = $thead.offset().top;
		var isFixed = false;
		var rollingAnimationInterval = null;

		$clone.hide().addClass('table-fixed-header-helper').prependTo($this);

		$(window).on('scroll', function () {
			var scrollTop = $(window).scrollTop();

			if (!isFixed && scrollTop + $mainHeader.outerHeight() > originalTop) {
				$this.addClass('table-fixed-header');
				$thead.outerWidth($this.outerWidth()).addClass('fixed');
				$clone.show();
				isFixed = true;
			} else if (isFixed && scrollTop + $mainHeader.outerHeight() < originalTop) {
				$this.removeClass('table-fixed-header');
				$thead.outerWidth('').removeClass('fixed');
				$clone.hide();
				isFixed = false;
			}

			if (scrollTop >= tableOffsetBottom) {
				$thead.removeClass('fixed');
			} else if ($(window).scrollTop() < tableOffsetBottom && !$thead.hasClass('fixed')) {
				$thead.addClass('fixed');
			}
		}).on('content_header:roll_in', function () {
			rollingAnimationInterval = setInterval(function () {
				$thead.css('top', $contentHeader.position().top + $contentHeader.outerHeight());
				if ($contentHeader.hasClass('fixed')) {
					clearInterval(rollingAnimationInterval);
				}
			}, 1);
		}).on('content_header:roll_out', function () {
			clearInterval(rollingAnimationInterval);
			$thead.css('top', $mainHeader.outerHeight());
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$(window).on('JSENGINE_INIT_FINISHED', function () {
			$this.on('draw.dt', _onDataTableDraw).on('init.dt', _onDataTableInit);

			// Setup fixed header functionality if the table is already initialized.
			if ($this.DataTable().ajax.json() !== undefined) {
				_onDataTableInit();
			}
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGF0YWJsZV9maXhlZF9oZWFkZXIuanMiXSwibmFtZXMiOlsiZ3giLCJleHRlbnNpb25zIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIiR0aGVhZCIsImNoaWxkcmVuIiwidGFibGVPZmZzZXRCb3R0b20iLCJfb25EYXRhVGFibGVEcmF3Iiwib2Zmc2V0IiwidG9wIiwiaGVpZ2h0IiwiX29uRGF0YVRhYmxlSW5pdCIsIiRtYWluSGVhZGVyIiwiJGNvbnRlbnRIZWFkZXIiLCIkY2xvbmUiLCJjbG9uZSIsIm9yaWdpbmFsVG9wIiwiaXNGaXhlZCIsInJvbGxpbmdBbmltYXRpb25JbnRlcnZhbCIsImhpZGUiLCJhZGRDbGFzcyIsInByZXBlbmRUbyIsIndpbmRvdyIsIm9uIiwic2Nyb2xsVG9wIiwib3V0ZXJIZWlnaHQiLCJvdXRlcldpZHRoIiwic2hvdyIsInJlbW92ZUNsYXNzIiwiaGFzQ2xhc3MiLCJzZXRJbnRlcnZhbCIsImNzcyIsInBvc2l0aW9uIiwiY2xlYXJJbnRlcnZhbCIsImluaXQiLCJkb25lIiwiRGF0YVRhYmxlIiwiYWpheCIsImpzb24iLCJ1bmRlZmluZWQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7QUFjQUEsR0FBR0MsVUFBSCxDQUFjQyxNQUFkLENBQXFCLHdCQUFyQixFQUErQyxFQUEvQyxFQUFtRCxVQUFTQyxJQUFULEVBQWU7O0FBRWpFOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsU0FBU0YsTUFBTUcsUUFBTixDQUFlLE9BQWYsQ0FBZjs7QUFFQTs7Ozs7QUFLQSxLQUFNTCxTQUFTLEVBQWY7O0FBRUE7Ozs7Ozs7QUFPQSxLQUFJTSwwQkFBSjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0MsZ0JBQVQsR0FBNEI7QUFDM0JELHNCQUFvQkosTUFBTU0sTUFBTixHQUFlQyxHQUFmLEdBQXFCUCxNQUFNUSxNQUFOLEVBQXJCLEdBQXNDTixPQUFPTSxNQUFQLEVBQTFEO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU0MsZ0JBQVQsR0FBNEI7QUFDM0IsTUFBTUMsY0FBY1QsRUFBRSxjQUFGLENBQXBCO0FBQ0EsTUFBTVUsaUJBQWlCVixFQUFFLGlCQUFGLENBQXZCO0FBQ0EsTUFBTVcsU0FBU1YsT0FBT1csS0FBUCxFQUFmO0FBQ0EsTUFBTUMsY0FBY1osT0FBT0ksTUFBUCxHQUFnQkMsR0FBcEM7QUFDQSxNQUFJUSxVQUFVLEtBQWQ7QUFDQSxNQUFJQywyQkFBMkIsSUFBL0I7O0FBRUFKLFNBQ0VLLElBREYsR0FFRUMsUUFGRixDQUVXLDJCQUZYLEVBR0VDLFNBSEYsQ0FHWW5CLEtBSFo7O0FBS0FDLElBQUVtQixNQUFGLEVBQ0VDLEVBREYsQ0FDSyxRQURMLEVBQ2UsWUFBVztBQUN4QixPQUFNQyxZQUFZckIsRUFBRW1CLE1BQUYsRUFBVUUsU0FBVixFQUFsQjs7QUFFQSxPQUFJLENBQUNQLE9BQUQsSUFBWU8sWUFBWVosWUFBWWEsV0FBWixFQUFaLEdBQXdDVCxXQUF4RCxFQUFxRTtBQUNwRWQsVUFBTWtCLFFBQU4sQ0FBZSxvQkFBZjtBQUNBaEIsV0FDRXNCLFVBREYsQ0FDYXhCLE1BQU13QixVQUFOLEVBRGIsRUFFRU4sUUFGRixDQUVXLE9BRlg7QUFHQU4sV0FBT2EsSUFBUDtBQUNBVixjQUFVLElBQVY7QUFDQSxJQVBELE1BT08sSUFBSUEsV0FBV08sWUFBWVosWUFBWWEsV0FBWixFQUFaLEdBQXdDVCxXQUF2RCxFQUFvRTtBQUMxRWQsVUFBTTBCLFdBQU4sQ0FBa0Isb0JBQWxCO0FBQ0F4QixXQUNFc0IsVUFERixDQUNhLEVBRGIsRUFFRUUsV0FGRixDQUVjLE9BRmQ7QUFHQWQsV0FBT0ssSUFBUDtBQUNBRixjQUFVLEtBQVY7QUFDQTs7QUFFRCxPQUFJTyxhQUFhbEIsaUJBQWpCLEVBQW9DO0FBQ25DRixXQUFPd0IsV0FBUCxDQUFtQixPQUFuQjtBQUNBLElBRkQsTUFFTyxJQUFJekIsRUFBRW1CLE1BQUYsRUFBVUUsU0FBVixLQUF3QmxCLGlCQUF4QixJQUE2QyxDQUFDRixPQUFPeUIsUUFBUCxDQUFnQixPQUFoQixDQUFsRCxFQUE0RTtBQUNsRnpCLFdBQU9nQixRQUFQLENBQWdCLE9BQWhCO0FBQ0E7QUFDRCxHQXpCRixFQTBCRUcsRUExQkYsQ0EwQkssd0JBMUJMLEVBMEIrQixZQUFXO0FBQ3hDTCw4QkFBMkJZLFlBQVksWUFBTTtBQUM1QzFCLFdBQU8yQixHQUFQLENBQVcsS0FBWCxFQUFrQmxCLGVBQWVtQixRQUFmLEdBQTBCdkIsR0FBMUIsR0FBZ0NJLGVBQWVZLFdBQWYsRUFBbEQ7QUFDQSxRQUFJWixlQUFlZ0IsUUFBZixDQUF3QixPQUF4QixDQUFKLEVBQXNDO0FBQ3JDSSxtQkFBY2Ysd0JBQWQ7QUFDQTtBQUNELElBTDBCLEVBS3hCLENBTHdCLENBQTNCO0FBTUEsR0FqQ0YsRUFrQ0VLLEVBbENGLENBa0NLLHlCQWxDTCxFQWtDZ0MsWUFBVztBQUN6Q1UsaUJBQWNmLHdCQUFkO0FBQ0FkLFVBQU8yQixHQUFQLENBQVcsS0FBWCxFQUFrQm5CLFlBQVlhLFdBQVosRUFBbEI7QUFDQSxHQXJDRjtBQXNDQTs7QUFHRDtBQUNBO0FBQ0E7O0FBRUF6QixRQUFPa0MsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QmhDLElBQUVtQixNQUFGLEVBQVVDLEVBQVYsQ0FBYSx3QkFBYixFQUF1QyxZQUFNO0FBQzVDckIsU0FDRXFCLEVBREYsQ0FDSyxTQURMLEVBQ2dCaEIsZ0JBRGhCLEVBRUVnQixFQUZGLENBRUssU0FGTCxFQUVnQlosZ0JBRmhCOztBQUlBO0FBQ0EsT0FBSVQsTUFBTWtDLFNBQU4sR0FBa0JDLElBQWxCLENBQXVCQyxJQUF2QixPQUFrQ0MsU0FBdEMsRUFBaUQ7QUFDaEQ1QjtBQUNBO0FBQ0QsR0FURDs7QUFXQXdCO0FBQ0EsRUFiRDs7QUFlQSxRQUFPbkMsTUFBUDtBQUVBLENBbklEIiwiZmlsZSI6ImRhdGF0YWJsZV9maXhlZF9oZWFkZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gZGF0YXRhYmxlX2ZpeGVkX2hlYWRlci5qcyAyMDE2LTA3LTEzXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqICMjIEVuYWJsZSBGaXhlZCBEYXRhVGFibGUgSGVhZGVyXHJcbiAqXHJcbiAqIFRoZSB0YWJsZSBoZWFkZXIgd2lsbCByZW1haW4gaW4gdGhlIHZpZXdwb3J0IGFzIHRoZSB1c2VyIHNjcm9sbHMgZG93biB0aGUgcGFnZS4gVGhlIHN0eWxlIGNoYW5nZSBvZiB0aGlzXHJcbiAqIG1vZHVsZSBpcyBhIGJpdCB0cmlja3kgYmVjYXVzZSB3ZSBuZWVkIHRvIHJlbW92ZSB0aGUgdGhlYWQgZnJvbSB0aGUgbm9ybWFsIGZsb3csIHNvbWV0aGluZyB0aGF0IGJyZWFrcyB0aGVcclxuICogZGlzcGxheSBvZiB0aGUgdGFibGUuIFRoZXJlZm9yZSBhIGhlbHBlciBjbG9uZSBvZiB0aGUgdGhlYWQgaXMgdXNlZCB0byBtYWludGFpbiB0aGUgdGFibGUgZm9ybWF0dGluZy5cclxuICpcclxuICogKipOb3RpY2UgIzEqKjogVGhlIC50YWJsZS1maXhlZC1oZWFkZXIgY2xhc3MgaXMgc3R5bGVkIGJ5IHRoZSBfdGFibGVzLnNjc3MgYW5kIGlzIHBhcnQgb2YgdGhpcyBzb2x1dGlvbi5cclxuICpcclxuICogKipOb3RpY2UgIzIqKjogVGhpcyBtZXRob2Qgd2lsbCB0YWtlIGludG8gY29uY2VybiB0aGUgLmNvbnRlbnQtaGVhZGVyIGVsZW1lbnQgd2hpY2ggc2hvdWxkbid0IG92ZXJsYXAgdGhlXHJcbiAqIHRhYmxlIGhlYWRlci5cclxuICpcclxuICogQG1vZHVsZSBBZG1pbi9FeHRlbnNpb25zL2RhdGF0YWJsZV9maXhlZF9oZWFkZXJcclxuICovXHJcbmd4LmV4dGVuc2lvbnMubW9kdWxlKCdkYXRhdGFibGVfZml4ZWRfaGVhZGVyJywgW10sIGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gVkFSSUFCTEVTXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7alF1ZXJ5fVxyXG5cdCAqL1xyXG5cdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBUYWJsZSBIZWFkZXIgU2VsZWN0b3JcclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtqUXVlcnl9XHJcblx0ICovXHJcblx0Y29uc3QgJHRoZWFkID0gJHRoaXMuY2hpbGRyZW4oJ3RoZWFkJyk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIEluc3RhbmNlXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IG1vZHVsZSA9IHt9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1hcmtzIHRoZSBlbmQgb2YgdGhlIHRhYmxlLlxyXG5cdCAqXHJcblx0ICogVGhpcyB2YWx1ZSBpcyB1c2VkIHRvIHN0b3AgdGhlIGZpeGVkIGhlYWRlciB3aGVuIHRoZSB1c2VyIHJlYWNoZXMgdGhlIGVuZCBvZiB0aGUgdGFibGUuXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7TnVtYmVyfVxyXG5cdCAqL1xyXG5cdGxldCB0YWJsZU9mZnNldEJvdHRvbTtcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBGVU5DVElPTlNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBEYXRhVGFibGUgRHJhdyBFdmVudFxyXG5cdCAqXHJcblx0ICogUmUtY2FsY3VsYXRlIHRoZSB0YWJsZSBib3R0b20gb2Zmc2V0IHZhbHVlLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vbkRhdGFUYWJsZURyYXcoKSB7XHJcblx0XHR0YWJsZU9mZnNldEJvdHRvbSA9ICR0aGlzLm9mZnNldCgpLnRvcCArICR0aGlzLmhlaWdodCgpIC0gJHRoZWFkLmhlaWdodCgpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBPbiBEYXRhVGFibGUgSW5pdGlhbGl6YXRpb25cclxuXHQgKlxyXG5cdCAqIE1vZGlmeSB0aGUgdGFibGUgSFRNTCBhbmQgc2V0IHRoZSByZXF1aXJlZCBldmVudCBoYW5kbGluZyBmb3IgdGhlIGZpeGVkIGhlYWRlciBmdW5jdGlvbmFsaXR5LlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIF9vbkRhdGFUYWJsZUluaXQoKSB7XHJcblx0XHRjb25zdCAkbWFpbkhlYWRlciA9ICQoJyNtYWluLWhlYWRlcicpO1xyXG5cdFx0Y29uc3QgJGNvbnRlbnRIZWFkZXIgPSAkKCcuY29udGVudC1oZWFkZXInKTtcclxuXHRcdGNvbnN0ICRjbG9uZSA9ICR0aGVhZC5jbG9uZSgpO1xyXG5cdFx0Y29uc3Qgb3JpZ2luYWxUb3AgPSAkdGhlYWQub2Zmc2V0KCkudG9wO1xyXG5cdFx0bGV0IGlzRml4ZWQgPSBmYWxzZTtcclxuXHRcdGxldCByb2xsaW5nQW5pbWF0aW9uSW50ZXJ2YWwgPSBudWxsO1xyXG5cdFx0XHJcblx0XHQkY2xvbmVcclxuXHRcdFx0LmhpZGUoKVxyXG5cdFx0XHQuYWRkQ2xhc3MoJ3RhYmxlLWZpeGVkLWhlYWRlci1oZWxwZXInKVxyXG5cdFx0XHQucHJlcGVuZFRvKCR0aGlzKTtcclxuXHRcdFxyXG5cdFx0JCh3aW5kb3cpXHJcblx0XHRcdC5vbignc2Nyb2xsJywgZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0Y29uc3Qgc2Nyb2xsVG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmICghaXNGaXhlZCAmJiBzY3JvbGxUb3AgKyAkbWFpbkhlYWRlci5vdXRlckhlaWdodCgpID4gb3JpZ2luYWxUb3ApIHtcclxuXHRcdFx0XHRcdCR0aGlzLmFkZENsYXNzKCd0YWJsZS1maXhlZC1oZWFkZXInKTtcclxuXHRcdFx0XHRcdCR0aGVhZFxyXG5cdFx0XHRcdFx0XHQub3V0ZXJXaWR0aCgkdGhpcy5vdXRlcldpZHRoKCkpXHJcblx0XHRcdFx0XHRcdC5hZGRDbGFzcygnZml4ZWQnKTtcclxuXHRcdFx0XHRcdCRjbG9uZS5zaG93KCk7XHJcblx0XHRcdFx0XHRpc0ZpeGVkID0gdHJ1ZTtcclxuXHRcdFx0XHR9IGVsc2UgaWYgKGlzRml4ZWQgJiYgc2Nyb2xsVG9wICsgJG1haW5IZWFkZXIub3V0ZXJIZWlnaHQoKSA8IG9yaWdpbmFsVG9wKSB7XHJcblx0XHRcdFx0XHQkdGhpcy5yZW1vdmVDbGFzcygndGFibGUtZml4ZWQtaGVhZGVyJyk7XHJcblx0XHRcdFx0XHQkdGhlYWRcclxuXHRcdFx0XHRcdFx0Lm91dGVyV2lkdGgoJycpXHJcblx0XHRcdFx0XHRcdC5yZW1vdmVDbGFzcygnZml4ZWQnKTtcclxuXHRcdFx0XHRcdCRjbG9uZS5oaWRlKCk7XHJcblx0XHRcdFx0XHRpc0ZpeGVkID0gZmFsc2U7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChzY3JvbGxUb3AgPj0gdGFibGVPZmZzZXRCb3R0b20pIHtcclxuXHRcdFx0XHRcdCR0aGVhZC5yZW1vdmVDbGFzcygnZml4ZWQnKTtcclxuXHRcdFx0XHR9IGVsc2UgaWYgKCQod2luZG93KS5zY3JvbGxUb3AoKSA8IHRhYmxlT2Zmc2V0Qm90dG9tICYmICEkdGhlYWQuaGFzQ2xhc3MoJ2ZpeGVkJykpIHtcclxuXHRcdFx0XHRcdCR0aGVhZC5hZGRDbGFzcygnZml4ZWQnKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHRcdC5vbignY29udGVudF9oZWFkZXI6cm9sbF9pbicsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdHJvbGxpbmdBbmltYXRpb25JbnRlcnZhbCA9IHNldEludGVydmFsKCgpID0+IHtcclxuXHRcdFx0XHRcdCR0aGVhZC5jc3MoJ3RvcCcsICRjb250ZW50SGVhZGVyLnBvc2l0aW9uKCkudG9wICsgJGNvbnRlbnRIZWFkZXIub3V0ZXJIZWlnaHQoKSk7XHJcblx0XHRcdFx0XHRpZiAoJGNvbnRlbnRIZWFkZXIuaGFzQ2xhc3MoJ2ZpeGVkJykpIHtcclxuXHRcdFx0XHRcdFx0Y2xlYXJJbnRlcnZhbChyb2xsaW5nQW5pbWF0aW9uSW50ZXJ2YWwpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sIDEpO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQub24oJ2NvbnRlbnRfaGVhZGVyOnJvbGxfb3V0JywgZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0Y2xlYXJJbnRlcnZhbChyb2xsaW5nQW5pbWF0aW9uSW50ZXJ2YWwpO1xyXG5cdFx0XHRcdCR0aGVhZC5jc3MoJ3RvcCcsICRtYWluSGVhZGVyLm91dGVySGVpZ2h0KCkpO1xyXG5cdFx0XHR9KTtcclxuXHR9XHJcblx0XHJcblx0XHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gSU5JVElBTElaQVRJT05cclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcclxuXHRcdCQod2luZG93KS5vbignSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsICgpID0+IHtcclxuXHRcdFx0JHRoaXNcclxuXHRcdFx0XHQub24oJ2RyYXcuZHQnLCBfb25EYXRhVGFibGVEcmF3KVxyXG5cdFx0XHRcdC5vbignaW5pdC5kdCcsIF9vbkRhdGFUYWJsZUluaXQpO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gU2V0dXAgZml4ZWQgaGVhZGVyIGZ1bmN0aW9uYWxpdHkgaWYgdGhlIHRhYmxlIGlzIGFscmVhZHkgaW5pdGlhbGl6ZWQuXHJcblx0XHRcdGlmICgkdGhpcy5EYXRhVGFibGUoKS5hamF4Lmpzb24oKSAhPT0gdW5kZWZpbmVkKSB7XHJcblx0XHRcdFx0X29uRGF0YVRhYmxlSW5pdCgpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0ZG9uZSgpO1xyXG5cdH07XHJcblx0XHJcblx0cmV0dXJuIG1vZHVsZTtcclxuXHRcclxufSk7Il19
