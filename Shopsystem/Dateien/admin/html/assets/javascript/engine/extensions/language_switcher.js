'use strict';

/* --------------------------------------------------------------
 language_switcher.js 2016-09-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Language Switcher Extension
 *
 * @module Admin/Extensions/language_switcher
 * @ignore
 */
gx.extensions.module('language_switcher', ['form', 'fallback', gx.source + '/libs/editor_values'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Extension Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Options for Extension
  *
  * @type {object}
  */
	defaults = {
		position: 1, // Position of the language id in the field name (zero indexed)
		initLang: jse.core.registry.get('languageId') // Current language on init
	},


	/**
  * Final Extension Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {},


	/**
  * Language Names
  *
  * @type {Array}
  */
	names = [],


	/**
  * Buttons Selector
  *
  * @type {object}
  */
	$languageLinks = null;

	// ------------------------------------------------------------------------
	// MAIN FUNCTIONALITY
	// ------------------------------------------------------------------------

	/**
  * Generate Transfer Object
  *
  * Generates a JSON transfer object to get data from fields named <X> to be stored in
  * fields with name <Y>. Therefore the names getting transformed the right way to be
  * able to use "jse.libs.form.prefillForm"
  *
  * @param {string} langActive String with the current lang id.
  * @param {boolean} toHidden If true, the destination are the hidden fields (else the input fields).
  */
	var _generateTransferObject = function _generateTransferObject(langActive, toHidden) {

		var currentData = {},
		    fullData = jse.libs.fallback.getData($this);

		$.each(names, function (i, v) {

			var keySplit = v.match(/\[([^\]]+)\]/gi),
			    baseKey = v.split('[')[0],
			    srcKey = baseKey,
			    destKey = baseKey,
			    valid = false;

			// Only execute if name schema matches
			if (keySplit) {
				// Generate key names
				$.each(keySplit, function (i, v) {
					if (options.position !== i) {
						destKey += v;
						srcKey += v;
					} else {
						if (toHidden) {
							destKey += '[' + langActive + ']';
						} else {
							srcKey += '[' + langActive + ']';
						}
						valid = true;
					}
				});

				// Push data to the result object
				if (valid && fullData[srcKey] !== undefined) {
					currentData[destKey] = fullData[srcKey];
				}
			}
		});

		return currentData;
	};

	/**
  * Store Data To Hidden
  *
  * Function to store input field data to hidden fields.
  *
  * @param {object} $activeButton jQuery selector object with the active language id.
  */
	var _storeDataToHidden = function _storeDataToHidden($activeButton) {
		var langActive = $activeButton.attr('href').slice(1);

		// Update textarea fields with data from CKEditor.
		$this.find('textarea').each(function (index, textarea) {
			var $textarea = $(textarea);

			if ($textarea.parent('.editor-wrapper').length) {
				var value = jse.libs.editor_values.getValue($textarea);
				$textarea.val(value);
			}
		});

		// Store data to hidden fields.
		jse.libs.form.prefillForm($this, _generateTransferObject(langActive, true), false);
	};

	/**
  * Get From Hidden
  *
  * Function to restore input field data from hidden fields
  *
  * @param {object} $activeButton jQuery selector object with the active language id.
  */
	var _getDataFromHidden = function _getDataFromHidden($activeButton) {
		var langActive = $activeButton.attr('href').slice(1);

		// Restore data to input fields
		jse.libs.form.prefillForm($this, _generateTransferObject(langActive, false), false);

		// Update the editors with the new data from textareas.
		$this.find('textarea').not('[data-language_switcher-ignore]').each(function (index, textarea) {
			var $textarea = $(textarea),
			    value = $textarea.val();

			if ($textarea.parent('.editor-wrapper').length) {
				jse.libs.editor_values.setValue($textarea, value);
			}
		});
	};

	/**
  * Update Editors
  *
  * Helper function to add a blur event on every editor that is loaded inside
  * of $this. To prevent multiple blur events on one editor, all names of the
  * tags that already got an blur event are saved.
  */
	var _updateEditors = function _updateEditors() {
		if (window.CKEDITOR) {
			$this.find('textarea.wysiwyg').each(function (index, textarea) {
				var $textarea = $(textarea),
				    name = $textarea.attr('name'),
				    editorType = $textarea.data('editorType') || 'ckeditor';

				switch (editorType) {
					case 'ckeditor':
						if (CKEDITOR.instances[name]) {
							CKEDITOR.instances[name].on('blur', function () {
								_storeDataToHidden($languageLinks.filter('.active'));
							});
						}

						break;

					case 'codemirror':
						var $codeMirror = $textarea.siblings('.CodeMirror'),
						    instance = $codeMirror.length ? $codeMirror[0].CodeMirror : null;

						if (instance) {
							instance.on('blur', function () {
								_storeDataToHidden($languageLinks.filter('.active'));
							});
						}

						break;
				}

				jse.libs.editor_values.setValue($textarea, $textarea.val());
			});
		}
	};

	// ------------------------------------------------------------------------
	// EVENT HANDLER
	// ------------------------------------------------------------------------

	/**
  * On Click Event Handler
  *
  * Event listener to store current data to hidden fields and restore hidden
  * data to text fields if a flag button gets clicked
  *
  * @param {object} event Contains information about the event.
  */
	var _clickHandler = function _clickHandler(event) {
		event.preventDefault();

		var $self = $(this);

		if (!$self.hasClass('active')) {

			var $activeButton = $languageLinks.filter('.active');

			$languageLinks.removeClass('active');
			$self.addClass('active');

			if ($activeButton.length) {
				_storeDataToHidden($activeButton);
			}

			_getDataFromHidden($self);
		}
	};

	/**
  * Update Field Event Handler
  *
  * @param {object} event Contains information about the event.
  */
	var _updateField = function _updateField(event) {
		event.preventDefault();
		var $activeButton = $languageLinks.filter('.active');
		_getDataFromHidden($activeButton);
	};

	/**
  * Get Language
  *
  * Function to return the current language id via an deferred object.
  *
  * @param {object} event jQuery event object.
  * @param {object} deferred Data object that contains the deferred object.
  */
	var _getLanguage = function _getLanguage(event, deferred) {
		if (deferred && deferred.deferred) {
			var lang = $languageLinks.filter('.active').first().attr('href').slice(1);

			deferred.deferred.resolve(lang);
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Init function of the extension, called by the engine.
  */
	module.init = function (done) {
		$languageLinks = $this.find('.buttonbar a'); // @todo Make the selector dynamic through an option.

		/**
   * Bind event listener to the form fields, and store the names of the field in cache. To prevent 
   * empty editors (because of already loaded editors on init of this script update them with the 
   * correct value.
   * 
   * @todo Move method outside the initialize method (avoid function nesting without specific reason). 
   */
		var _addEventHandler = function _addEventHandler() {
			names = [];

			// Get all needed selectors.
			var $formFields = $this.find('input:not(:button):not(:submit), select, textarea').not('[data-language_switcher-ignore]');

			$formFields.each(function () {

				var $self = $(this),
				    type = jse.libs.form.getFieldType($self),
				    event = $.inArray(type, ['text', 'textarea']) > -1 ? 'blur' : 'change',
				    name = $self.attr('name');

				names.push(name);

				$self.on(event, function () {
					_storeDataToHidden($languageLinks.filter('.active'));
				});
			});
		};

		_addEventHandler();

		// Bind event handler to the flags buttons.
		$languageLinks.on('click', _clickHandler).filter('[href="#' + options.initLang + '"]').trigger('click');

		// Bind the editor related events whenever the editor widget is ready.
		$(window).on('editor:ready', _updateEditors);

		$('form').on('submit', function () {
			_storeDataToHidden($languageLinks.filter('.active'));
		});

		$this.on('layerClose', function () {
			// Workaround to update the hidden fields on layer close.
			_storeDataToHidden($languageLinks.filter('.active'));
		}).on('language_switcher.update', _addEventHandler).on('language_switcher.updateField', _updateField).on('language_switcher.getLang', _getLanguage).on('click', '.editor-switch', _updateEditors);

		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxhbmd1YWdlX3N3aXRjaGVyLmpzIl0sIm5hbWVzIjpbImd4IiwiZXh0ZW5zaW9ucyIsIm1vZHVsZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsInBvc2l0aW9uIiwiaW5pdExhbmciLCJqc2UiLCJjb3JlIiwicmVnaXN0cnkiLCJnZXQiLCJvcHRpb25zIiwiZXh0ZW5kIiwibmFtZXMiLCIkbGFuZ3VhZ2VMaW5rcyIsIl9nZW5lcmF0ZVRyYW5zZmVyT2JqZWN0IiwibGFuZ0FjdGl2ZSIsInRvSGlkZGVuIiwiY3VycmVudERhdGEiLCJmdWxsRGF0YSIsImxpYnMiLCJmYWxsYmFjayIsImdldERhdGEiLCJlYWNoIiwiaSIsInYiLCJrZXlTcGxpdCIsIm1hdGNoIiwiYmFzZUtleSIsInNwbGl0Iiwic3JjS2V5IiwiZGVzdEtleSIsInZhbGlkIiwidW5kZWZpbmVkIiwiX3N0b3JlRGF0YVRvSGlkZGVuIiwiJGFjdGl2ZUJ1dHRvbiIsImF0dHIiLCJzbGljZSIsImZpbmQiLCJpbmRleCIsInRleHRhcmVhIiwiJHRleHRhcmVhIiwicGFyZW50IiwibGVuZ3RoIiwidmFsdWUiLCJlZGl0b3JfdmFsdWVzIiwiZ2V0VmFsdWUiLCJ2YWwiLCJmb3JtIiwicHJlZmlsbEZvcm0iLCJfZ2V0RGF0YUZyb21IaWRkZW4iLCJub3QiLCJzZXRWYWx1ZSIsIl91cGRhdGVFZGl0b3JzIiwid2luZG93IiwiQ0tFRElUT1IiLCJuYW1lIiwiZWRpdG9yVHlwZSIsImluc3RhbmNlcyIsIm9uIiwiZmlsdGVyIiwiJGNvZGVNaXJyb3IiLCJzaWJsaW5ncyIsImluc3RhbmNlIiwiQ29kZU1pcnJvciIsIl9jbGlja0hhbmRsZXIiLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiJHNlbGYiLCJoYXNDbGFzcyIsInJlbW92ZUNsYXNzIiwiYWRkQ2xhc3MiLCJfdXBkYXRlRmllbGQiLCJfZ2V0TGFuZ3VhZ2UiLCJkZWZlcnJlZCIsImxhbmciLCJmaXJzdCIsInJlc29sdmUiLCJpbml0IiwiZG9uZSIsIl9hZGRFdmVudEhhbmRsZXIiLCIkZm9ybUZpZWxkcyIsInR5cGUiLCJnZXRGaWVsZFR5cGUiLCJpbkFycmF5IiwicHVzaCIsInRyaWdnZXIiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7O0FBTUFBLEdBQUdDLFVBQUgsQ0FBY0MsTUFBZCxDQUNDLG1CQURELEVBR0MsQ0FBQyxNQUFELEVBQVMsVUFBVCxFQUF3QkYsR0FBR0csTUFBM0IseUJBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsWUFBVztBQUNWQyxZQUFVLENBREEsRUFDRztBQUNiQyxZQUFVQyxJQUFJQyxJQUFKLENBQVNDLFFBQVQsQ0FBa0JDLEdBQWxCLENBQXNCLFlBQXRCLENBRkEsQ0FFb0M7QUFGcEMsRUFiWjs7O0FBa0JDOzs7OztBQUtBQyxXQUFVUixFQUFFUyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJSLFFBQW5CLEVBQTZCSCxJQUE3QixDQXZCWDs7O0FBeUJDOzs7OztBQUtBRixVQUFTLEVBOUJWOzs7QUFnQ0M7Ozs7O0FBS0FjLFNBQVEsRUFyQ1Q7OztBQXVDQzs7Ozs7QUFLQUMsa0JBQWlCLElBNUNsQjs7QUE4Q0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBVUEsS0FBSUMsMEJBQTBCLFNBQTFCQSx1QkFBMEIsQ0FBU0MsVUFBVCxFQUFxQkMsUUFBckIsRUFBK0I7O0FBRTVELE1BQUlDLGNBQWMsRUFBbEI7QUFBQSxNQUNDQyxXQUFXWixJQUFJYSxJQUFKLENBQVNDLFFBQVQsQ0FBa0JDLE9BQWxCLENBQTBCcEIsS0FBMUIsQ0FEWjs7QUFHQUMsSUFBRW9CLElBQUYsQ0FBT1YsS0FBUCxFQUFjLFVBQVNXLENBQVQsRUFBWUMsQ0FBWixFQUFlOztBQUU1QixPQUFJQyxXQUFXRCxFQUFFRSxLQUFGLENBQVEsZ0JBQVIsQ0FBZjtBQUFBLE9BQ0NDLFVBQVVILEVBQUVJLEtBQUYsQ0FBUSxHQUFSLEVBQWEsQ0FBYixDQURYO0FBQUEsT0FFQ0MsU0FBU0YsT0FGVjtBQUFBLE9BR0NHLFVBQVVILE9BSFg7QUFBQSxPQUlDSSxRQUFRLEtBSlQ7O0FBTUE7QUFDQSxPQUFJTixRQUFKLEVBQWM7QUFDYjtBQUNBdkIsTUFBRW9CLElBQUYsQ0FBT0csUUFBUCxFQUFpQixVQUFTRixDQUFULEVBQVlDLENBQVosRUFBZTtBQUMvQixTQUFJZCxRQUFRTixRQUFSLEtBQXFCbUIsQ0FBekIsRUFBNEI7QUFDM0JPLGlCQUFXTixDQUFYO0FBQ0FLLGdCQUFVTCxDQUFWO0FBQ0EsTUFIRCxNQUdPO0FBQ04sVUFBSVIsUUFBSixFQUFjO0FBQ2JjLGtCQUFXLE1BQU1mLFVBQU4sR0FBbUIsR0FBOUI7QUFDQSxPQUZELE1BRU87QUFDTmMsaUJBQVUsTUFBTWQsVUFBTixHQUFtQixHQUE3QjtBQUNBO0FBQ0RnQixjQUFRLElBQVI7QUFDQTtBQUNELEtBWkQ7O0FBY0E7QUFDQSxRQUFJQSxTQUFTYixTQUFTVyxNQUFULE1BQXFCRyxTQUFsQyxFQUE2QztBQUM1Q2YsaUJBQVlhLE9BQVosSUFBdUJaLFNBQVNXLE1BQVQsQ0FBdkI7QUFDQTtBQUNEO0FBQ0QsR0E5QkQ7O0FBZ0NBLFNBQU9aLFdBQVA7QUFDQSxFQXRDRDs7QUF3Q0E7Ozs7Ozs7QUFPQSxLQUFJZ0IscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBU0MsYUFBVCxFQUF3QjtBQUNoRCxNQUFJbkIsYUFBYW1CLGNBQWNDLElBQWQsQ0FBbUIsTUFBbkIsRUFBMkJDLEtBQTNCLENBQWlDLENBQWpDLENBQWpCOztBQUVBO0FBQ0FuQyxRQUNFb0MsSUFERixDQUNPLFVBRFAsRUFFRWYsSUFGRixDQUVPLFVBQVNnQixLQUFULEVBQWdCQyxRQUFoQixFQUEwQjtBQUMvQixPQUFJQyxZQUFZdEMsRUFBRXFDLFFBQUYsQ0FBaEI7O0FBRUEsT0FBSUMsVUFBVUMsTUFBVixDQUFpQixpQkFBakIsRUFBb0NDLE1BQXhDLEVBQWdEO0FBQy9DLFFBQUlDLFFBQVFyQyxJQUFJYSxJQUFKLENBQVN5QixhQUFULENBQXVCQyxRQUF2QixDQUFnQ0wsU0FBaEMsQ0FBWjtBQUNBQSxjQUFVTSxHQUFWLENBQWNILEtBQWQ7QUFDQTtBQUNELEdBVEY7O0FBV0E7QUFDQXJDLE1BQUlhLElBQUosQ0FBUzRCLElBQVQsQ0FBY0MsV0FBZCxDQUEwQi9DLEtBQTFCLEVBQWlDYSx3QkFBd0JDLFVBQXhCLEVBQW9DLElBQXBDLENBQWpDLEVBQTRFLEtBQTVFO0FBQ0EsRUFqQkQ7O0FBbUJBOzs7Ozs7O0FBT0EsS0FBSWtDLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQVNmLGFBQVQsRUFBd0I7QUFDaEQsTUFBSW5CLGFBQWFtQixjQUFjQyxJQUFkLENBQW1CLE1BQW5CLEVBQTJCQyxLQUEzQixDQUFpQyxDQUFqQyxDQUFqQjs7QUFFQTtBQUNBOUIsTUFBSWEsSUFBSixDQUFTNEIsSUFBVCxDQUFjQyxXQUFkLENBQTBCL0MsS0FBMUIsRUFBaUNhLHdCQUF3QkMsVUFBeEIsRUFBb0MsS0FBcEMsQ0FBakMsRUFBNkUsS0FBN0U7O0FBRUE7QUFDQWQsUUFDRW9DLElBREYsQ0FDTyxVQURQLEVBRUVhLEdBRkYsQ0FFTSxpQ0FGTixFQUdFNUIsSUFIRixDQUdPLFVBQVNnQixLQUFULEVBQWdCQyxRQUFoQixFQUEwQjtBQUMvQixPQUFJQyxZQUFZdEMsRUFBRXFDLFFBQUYsQ0FBaEI7QUFBQSxPQUNDSSxRQUFRSCxVQUFVTSxHQUFWLEVBRFQ7O0FBR0EsT0FBSU4sVUFBVUMsTUFBVixDQUFpQixpQkFBakIsRUFBb0NDLE1BQXhDLEVBQWdEO0FBQy9DcEMsUUFBSWEsSUFBSixDQUFTeUIsYUFBVCxDQUF1Qk8sUUFBdkIsQ0FBZ0NYLFNBQWhDLEVBQTJDRyxLQUEzQztBQUNBO0FBQ0QsR0FWRjtBQVdBLEVBbEJEOztBQW9CQTs7Ozs7OztBQU9BLEtBQUlTLGlCQUFpQixTQUFqQkEsY0FBaUIsR0FBVztBQUMvQixNQUFJQyxPQUFPQyxRQUFYLEVBQXFCO0FBQ3BCckQsU0FDRW9DLElBREYsQ0FDTyxrQkFEUCxFQUVFZixJQUZGLENBRU8sVUFBU2dCLEtBQVQsRUFBZ0JDLFFBQWhCLEVBQTBCO0FBQy9CLFFBQUlDLFlBQVl0QyxFQUFFcUMsUUFBRixDQUFoQjtBQUFBLFFBQ0NnQixPQUFPZixVQUFVTCxJQUFWLENBQWUsTUFBZixDQURSO0FBQUEsUUFFQ3FCLGFBQWFoQixVQUFVeEMsSUFBVixDQUFlLFlBQWYsS0FBZ0MsVUFGOUM7O0FBSUEsWUFBUXdELFVBQVI7QUFDQyxVQUFLLFVBQUw7QUFDQyxVQUFJRixTQUFTRyxTQUFULENBQW1CRixJQUFuQixDQUFKLEVBQThCO0FBQzdCRCxnQkFBU0csU0FBVCxDQUFtQkYsSUFBbkIsRUFBeUJHLEVBQXpCLENBQTRCLE1BQTVCLEVBQW9DLFlBQVc7QUFDOUN6QiwyQkFBbUJwQixlQUFlOEMsTUFBZixDQUFzQixTQUF0QixDQUFuQjtBQUNBLFFBRkQ7QUFHQTs7QUFFRDs7QUFFRCxVQUFLLFlBQUw7QUFDQyxVQUFJQyxjQUFjcEIsVUFBVXFCLFFBQVYsQ0FBbUIsYUFBbkIsQ0FBbEI7QUFBQSxVQUNDQyxXQUFXRixZQUFZbEIsTUFBWixHQUFxQmtCLFlBQVksQ0FBWixFQUFlRyxVQUFwQyxHQUFpRCxJQUQ3RDs7QUFHQSxVQUFJRCxRQUFKLEVBQWM7QUFDYkEsZ0JBQVNKLEVBQVQsQ0FBWSxNQUFaLEVBQW9CLFlBQVc7QUFDOUJ6QiwyQkFBbUJwQixlQUFlOEMsTUFBZixDQUFzQixTQUF0QixDQUFuQjtBQUNBLFFBRkQ7QUFHQTs7QUFFRDtBQXBCRjs7QUF1QkFyRCxRQUFJYSxJQUFKLENBQVN5QixhQUFULENBQXVCTyxRQUF2QixDQUFnQ1gsU0FBaEMsRUFBMkNBLFVBQVVNLEdBQVYsRUFBM0M7QUFDQSxJQS9CRjtBQWdDQTtBQUNELEVBbkNEOztBQXFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FBUUEsS0FBSWtCLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBU0MsS0FBVCxFQUFnQjtBQUNuQ0EsUUFBTUMsY0FBTjs7QUFFQSxNQUFJQyxRQUFRakUsRUFBRSxJQUFGLENBQVo7O0FBRUEsTUFBSSxDQUFDaUUsTUFBTUMsUUFBTixDQUFlLFFBQWYsQ0FBTCxFQUErQjs7QUFFOUIsT0FBSWxDLGdCQUFnQnJCLGVBQWU4QyxNQUFmLENBQXNCLFNBQXRCLENBQXBCOztBQUVBOUMsa0JBQWV3RCxXQUFmLENBQTJCLFFBQTNCO0FBQ0FGLFNBQU1HLFFBQU4sQ0FBZSxRQUFmOztBQUVBLE9BQUlwQyxjQUFjUSxNQUFsQixFQUEwQjtBQUN6QlQsdUJBQW1CQyxhQUFuQjtBQUNBOztBQUVEZSxzQkFBbUJrQixLQUFuQjtBQUNBO0FBQ0QsRUFsQkQ7O0FBb0JBOzs7OztBQUtBLEtBQUlJLGVBQWUsU0FBZkEsWUFBZSxDQUFTTixLQUFULEVBQWdCO0FBQ2xDQSxRQUFNQyxjQUFOO0FBQ0EsTUFBSWhDLGdCQUFnQnJCLGVBQWU4QyxNQUFmLENBQXNCLFNBQXRCLENBQXBCO0FBQ0FWLHFCQUFtQmYsYUFBbkI7QUFDQSxFQUpEOztBQU1BOzs7Ozs7OztBQVFBLEtBQUlzQyxlQUFlLFNBQWZBLFlBQWUsQ0FBU1AsS0FBVCxFQUFnQlEsUUFBaEIsRUFBMEI7QUFDNUMsTUFBSUEsWUFBWUEsU0FBU0EsUUFBekIsRUFBbUM7QUFDbEMsT0FBSUMsT0FBTzdELGVBQ1Q4QyxNQURTLENBQ0YsU0FERSxFQUVUZ0IsS0FGUyxHQUdUeEMsSUFIUyxDQUdKLE1BSEksRUFJVEMsS0FKUyxDQUlILENBSkcsQ0FBWDs7QUFNQXFDLFlBQVNBLFFBQVQsQ0FBa0JHLE9BQWxCLENBQTBCRixJQUExQjtBQUNBO0FBQ0QsRUFWRDs7QUFZQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBNUUsUUFBTytFLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJqRSxtQkFBaUJaLE1BQU1vQyxJQUFOLENBQVcsY0FBWCxDQUFqQixDQUQ0QixDQUNpQjs7QUFFN0M7Ozs7Ozs7QUFPQSxNQUFJMEMsbUJBQW1CLFNBQW5CQSxnQkFBbUIsR0FBVztBQUNqQ25FLFdBQVEsRUFBUjs7QUFFQTtBQUNBLE9BQUlvRSxjQUFjL0UsTUFBTW9DLElBQU4sQ0FBVyxtREFBWCxFQUNoQmEsR0FEZ0IsQ0FDWixpQ0FEWSxDQUFsQjs7QUFHQThCLGVBQVkxRCxJQUFaLENBQWlCLFlBQVc7O0FBRTNCLFFBQUk2QyxRQUFRakUsRUFBRSxJQUFGLENBQVo7QUFBQSxRQUNDK0UsT0FBTzNFLElBQUlhLElBQUosQ0FBUzRCLElBQVQsQ0FBY21DLFlBQWQsQ0FBMkJmLEtBQTNCLENBRFI7QUFBQSxRQUVDRixRQUFTL0QsRUFBRWlGLE9BQUYsQ0FBVUYsSUFBVixFQUFnQixDQUFDLE1BQUQsRUFBUyxVQUFULENBQWhCLElBQXdDLENBQUMsQ0FBMUMsR0FBK0MsTUFBL0MsR0FBd0QsUUFGakU7QUFBQSxRQUdDMUIsT0FBT1ksTUFBTWhDLElBQU4sQ0FBVyxNQUFYLENBSFI7O0FBS0F2QixVQUFNd0UsSUFBTixDQUFXN0IsSUFBWDs7QUFFQVksVUFDRVQsRUFERixDQUNLTyxLQURMLEVBQ1ksWUFBVztBQUNyQmhDLHdCQUFtQnBCLGVBQWU4QyxNQUFmLENBQXNCLFNBQXRCLENBQW5CO0FBQ0EsS0FIRjtBQUlBLElBYkQ7QUFjQSxHQXJCRDs7QUF1QkFvQjs7QUFFQTtBQUNBbEUsaUJBQ0U2QyxFQURGLENBQ0ssT0FETCxFQUNjTSxhQURkLEVBRUVMLE1BRkYsQ0FFUyxhQUFhakQsUUFBUUwsUUFBckIsR0FBZ0MsSUFGekMsRUFHRWdGLE9BSEYsQ0FHVSxPQUhWOztBQUtBO0FBQ0FuRixJQUFFbUQsTUFBRixFQUFVSyxFQUFWLENBQWEsY0FBYixFQUE2Qk4sY0FBN0I7O0FBRUFsRCxJQUFFLE1BQUYsRUFBVXdELEVBQVYsQ0FBYSxRQUFiLEVBQXVCLFlBQVk7QUFDbEN6QixzQkFBbUJwQixlQUFlOEMsTUFBZixDQUFzQixTQUF0QixDQUFuQjtBQUNBLEdBRkQ7O0FBSUExRCxRQUNFeUQsRUFERixDQUNLLFlBREwsRUFDbUIsWUFBVztBQUM1QjtBQUNBekIsc0JBQW1CcEIsZUFBZThDLE1BQWYsQ0FBc0IsU0FBdEIsQ0FBbkI7QUFDQSxHQUpGLEVBS0VELEVBTEYsQ0FLSywwQkFMTCxFQUtpQ3FCLGdCQUxqQyxFQU1FckIsRUFORixDQU1LLCtCQU5MLEVBTXNDYSxZQU50QyxFQU9FYixFQVBGLENBT0ssMkJBUEwsRUFPa0NjLFlBUGxDLEVBUUVkLEVBUkYsQ0FRSyxPQVJMLEVBUWMsZ0JBUmQsRUFRZ0NOLGNBUmhDOztBQVVBMEI7QUFDQSxFQTNERDs7QUE2REE7QUFDQSxRQUFPaEYsTUFBUDtBQUNBLENBdlZGIiwiZmlsZSI6Imxhbmd1YWdlX3N3aXRjaGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBsYW5ndWFnZV9zd2l0Y2hlci5qcyAyMDE2LTA5LTA2XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiAjIyBMYW5ndWFnZSBTd2l0Y2hlciBFeHRlbnNpb25cbiAqXG4gKiBAbW9kdWxlIEFkbWluL0V4dGVuc2lvbnMvbGFuZ3VhZ2Vfc3dpdGNoZXJcbiAqIEBpZ25vcmVcbiAqL1xuZ3guZXh0ZW5zaW9ucy5tb2R1bGUoXG5cdCdsYW5ndWFnZV9zd2l0Y2hlcicsXG5cdFxuXHRbJ2Zvcm0nLCAnZmFsbGJhY2snLCBgJHtneC5zb3VyY2V9L2xpYnMvZWRpdG9yX3ZhbHVlc2BdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIEV4dGVuc2lvbiBSZWZlcmVuY2Vcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGhpcyA9ICQodGhpcyksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRGVmYXVsdCBPcHRpb25zIGZvciBFeHRlbnNpb25cblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0cG9zaXRpb246IDEsIC8vIFBvc2l0aW9uIG9mIHRoZSBsYW5ndWFnZSBpZCBpbiB0aGUgZmllbGQgbmFtZSAoemVybyBpbmRleGVkKVxuXHRcdFx0XHRpbml0TGFuZzoganNlLmNvcmUucmVnaXN0cnkuZ2V0KCdsYW5ndWFnZUlkJykgLy8gQ3VycmVudCBsYW5ndWFnZSBvbiBpbml0XG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEZpbmFsIEV4dGVuc2lvbiBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG1vZHVsZSA9IHt9LFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIExhbmd1YWdlIE5hbWVzXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge0FycmF5fVxuXHRcdFx0ICovXG5cdFx0XHRuYW1lcyA9IFtdLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEJ1dHRvbnMgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkbGFuZ3VhZ2VMaW5rcyA9IG51bGw7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gTUFJTiBGVU5DVElPTkFMSVRZXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogR2VuZXJhdGUgVHJhbnNmZXIgT2JqZWN0XG5cdFx0ICpcblx0XHQgKiBHZW5lcmF0ZXMgYSBKU09OIHRyYW5zZmVyIG9iamVjdCB0byBnZXQgZGF0YSBmcm9tIGZpZWxkcyBuYW1lZCA8WD4gdG8gYmUgc3RvcmVkIGluXG5cdFx0ICogZmllbGRzIHdpdGggbmFtZSA8WT4uIFRoZXJlZm9yZSB0aGUgbmFtZXMgZ2V0dGluZyB0cmFuc2Zvcm1lZCB0aGUgcmlnaHQgd2F5IHRvIGJlXG5cdFx0ICogYWJsZSB0byB1c2UgXCJqc2UubGlicy5mb3JtLnByZWZpbGxGb3JtXCJcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBsYW5nQWN0aXZlIFN0cmluZyB3aXRoIHRoZSBjdXJyZW50IGxhbmcgaWQuXG5cdFx0ICogQHBhcmFtIHtib29sZWFufSB0b0hpZGRlbiBJZiB0cnVlLCB0aGUgZGVzdGluYXRpb24gYXJlIHRoZSBoaWRkZW4gZmllbGRzIChlbHNlIHRoZSBpbnB1dCBmaWVsZHMpLlxuXHRcdCAqL1xuXHRcdHZhciBfZ2VuZXJhdGVUcmFuc2Zlck9iamVjdCA9IGZ1bmN0aW9uKGxhbmdBY3RpdmUsIHRvSGlkZGVuKSB7XG5cdFx0XHRcblx0XHRcdHZhciBjdXJyZW50RGF0YSA9IHt9LFxuXHRcdFx0XHRmdWxsRGF0YSA9IGpzZS5saWJzLmZhbGxiYWNrLmdldERhdGEoJHRoaXMpO1xuXHRcdFx0XG5cdFx0XHQkLmVhY2gobmFtZXMsIGZ1bmN0aW9uKGksIHYpIHtcblx0XHRcdFx0XG5cdFx0XHRcdHZhciBrZXlTcGxpdCA9IHYubWF0Y2goL1xcWyhbXlxcXV0rKVxcXS9naSksXG5cdFx0XHRcdFx0YmFzZUtleSA9IHYuc3BsaXQoJ1snKVswXSxcblx0XHRcdFx0XHRzcmNLZXkgPSBiYXNlS2V5LFxuXHRcdFx0XHRcdGRlc3RLZXkgPSBiYXNlS2V5LFxuXHRcdFx0XHRcdHZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBPbmx5IGV4ZWN1dGUgaWYgbmFtZSBzY2hlbWEgbWF0Y2hlc1xuXHRcdFx0XHRpZiAoa2V5U3BsaXQpIHtcblx0XHRcdFx0XHQvLyBHZW5lcmF0ZSBrZXkgbmFtZXNcblx0XHRcdFx0XHQkLmVhY2goa2V5U3BsaXQsIGZ1bmN0aW9uKGksIHYpIHtcblx0XHRcdFx0XHRcdGlmIChvcHRpb25zLnBvc2l0aW9uICE9PSBpKSB7XG5cdFx0XHRcdFx0XHRcdGRlc3RLZXkgKz0gdjtcblx0XHRcdFx0XHRcdFx0c3JjS2V5ICs9IHY7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRpZiAodG9IaWRkZW4pIHtcblx0XHRcdFx0XHRcdFx0XHRkZXN0S2V5ICs9ICdbJyArIGxhbmdBY3RpdmUgKyAnXSc7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0c3JjS2V5ICs9ICdbJyArIGxhbmdBY3RpdmUgKyAnXSc7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0dmFsaWQgPSB0cnVlO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFB1c2ggZGF0YSB0byB0aGUgcmVzdWx0IG9iamVjdFxuXHRcdFx0XHRcdGlmICh2YWxpZCAmJiBmdWxsRGF0YVtzcmNLZXldICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0XHRcdGN1cnJlbnREYXRhW2Rlc3RLZXldID0gZnVsbERhdGFbc3JjS2V5XTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gY3VycmVudERhdGE7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBTdG9yZSBEYXRhIFRvIEhpZGRlblxuXHRcdCAqXG5cdFx0ICogRnVuY3Rpb24gdG8gc3RvcmUgaW5wdXQgZmllbGQgZGF0YSB0byBoaWRkZW4gZmllbGRzLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9ICRhY3RpdmVCdXR0b24galF1ZXJ5IHNlbGVjdG9yIG9iamVjdCB3aXRoIHRoZSBhY3RpdmUgbGFuZ3VhZ2UgaWQuXG5cdFx0ICovXG5cdFx0dmFyIF9zdG9yZURhdGFUb0hpZGRlbiA9IGZ1bmN0aW9uKCRhY3RpdmVCdXR0b24pIHtcblx0XHRcdHZhciBsYW5nQWN0aXZlID0gJGFjdGl2ZUJ1dHRvbi5hdHRyKCdocmVmJykuc2xpY2UoMSk7XG5cdFx0XHRcblx0XHRcdC8vIFVwZGF0ZSB0ZXh0YXJlYSBmaWVsZHMgd2l0aCBkYXRhIGZyb20gQ0tFZGl0b3IuXG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQuZmluZCgndGV4dGFyZWEnKVxuXHRcdFx0XHQuZWFjaChmdW5jdGlvbihpbmRleCwgdGV4dGFyZWEpIHtcblx0XHRcdFx0XHR2YXIgJHRleHRhcmVhID0gJCh0ZXh0YXJlYSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKCR0ZXh0YXJlYS5wYXJlbnQoJy5lZGl0b3Itd3JhcHBlcicpLmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0dmFyIHZhbHVlID0ganNlLmxpYnMuZWRpdG9yX3ZhbHVlcy5nZXRWYWx1ZSgkdGV4dGFyZWEpO1xuXHRcdFx0XHRcdFx0JHRleHRhcmVhLnZhbCh2YWx1ZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gU3RvcmUgZGF0YSB0byBoaWRkZW4gZmllbGRzLlxuXHRcdFx0anNlLmxpYnMuZm9ybS5wcmVmaWxsRm9ybSgkdGhpcywgX2dlbmVyYXRlVHJhbnNmZXJPYmplY3QobGFuZ0FjdGl2ZSwgdHJ1ZSksIGZhbHNlKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEdldCBGcm9tIEhpZGRlblxuXHRcdCAqXG5cdFx0ICogRnVuY3Rpb24gdG8gcmVzdG9yZSBpbnB1dCBmaWVsZCBkYXRhIGZyb20gaGlkZGVuIGZpZWxkc1xuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9ICRhY3RpdmVCdXR0b24galF1ZXJ5IHNlbGVjdG9yIG9iamVjdCB3aXRoIHRoZSBhY3RpdmUgbGFuZ3VhZ2UgaWQuXG5cdFx0ICovXG5cdFx0dmFyIF9nZXREYXRhRnJvbUhpZGRlbiA9IGZ1bmN0aW9uKCRhY3RpdmVCdXR0b24pIHtcblx0XHRcdHZhciBsYW5nQWN0aXZlID0gJGFjdGl2ZUJ1dHRvbi5hdHRyKCdocmVmJykuc2xpY2UoMSk7XG5cdFx0XHRcblx0XHRcdC8vIFJlc3RvcmUgZGF0YSB0byBpbnB1dCBmaWVsZHNcblx0XHRcdGpzZS5saWJzLmZvcm0ucHJlZmlsbEZvcm0oJHRoaXMsIF9nZW5lcmF0ZVRyYW5zZmVyT2JqZWN0KGxhbmdBY3RpdmUsIGZhbHNlKSwgZmFsc2UpO1xuXHRcdFx0XG5cdFx0XHQvLyBVcGRhdGUgdGhlIGVkaXRvcnMgd2l0aCB0aGUgbmV3IGRhdGEgZnJvbSB0ZXh0YXJlYXMuXG5cdFx0XHQkdGhpc1xuXHRcdFx0XHQuZmluZCgndGV4dGFyZWEnKVxuXHRcdFx0XHQubm90KCdbZGF0YS1sYW5ndWFnZV9zd2l0Y2hlci1pZ25vcmVdJylcblx0XHRcdFx0LmVhY2goZnVuY3Rpb24oaW5kZXgsIHRleHRhcmVhKSB7XG5cdFx0XHRcdFx0dmFyICR0ZXh0YXJlYSA9ICQodGV4dGFyZWEpLFxuXHRcdFx0XHRcdFx0dmFsdWUgPSAkdGV4dGFyZWEudmFsKCk7IFxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmICgkdGV4dGFyZWEucGFyZW50KCcuZWRpdG9yLXdyYXBwZXInKS5sZW5ndGgpIHtcblx0XHRcdFx0XHRcdGpzZS5saWJzLmVkaXRvcl92YWx1ZXMuc2V0VmFsdWUoJHRleHRhcmVhLCB2YWx1ZSk7IFxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBVcGRhdGUgRWRpdG9yc1xuXHRcdCAqXG5cdFx0ICogSGVscGVyIGZ1bmN0aW9uIHRvIGFkZCBhIGJsdXIgZXZlbnQgb24gZXZlcnkgZWRpdG9yIHRoYXQgaXMgbG9hZGVkIGluc2lkZVxuXHRcdCAqIG9mICR0aGlzLiBUbyBwcmV2ZW50IG11bHRpcGxlIGJsdXIgZXZlbnRzIG9uIG9uZSBlZGl0b3IsIGFsbCBuYW1lcyBvZiB0aGVcblx0XHQgKiB0YWdzIHRoYXQgYWxyZWFkeSBnb3QgYW4gYmx1ciBldmVudCBhcmUgc2F2ZWQuXG5cdFx0ICovXG5cdFx0dmFyIF91cGRhdGVFZGl0b3JzID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRpZiAod2luZG93LkNLRURJVE9SKSB7XG5cdFx0XHRcdCR0aGlzXG5cdFx0XHRcdFx0LmZpbmQoJ3RleHRhcmVhLnd5c2l3eWcnKVxuXHRcdFx0XHRcdC5lYWNoKGZ1bmN0aW9uKGluZGV4LCB0ZXh0YXJlYSkge1xuXHRcdFx0XHRcdFx0dmFyICR0ZXh0YXJlYSA9ICQodGV4dGFyZWEpLFxuXHRcdFx0XHRcdFx0XHRuYW1lID0gJHRleHRhcmVhLmF0dHIoJ25hbWUnKSxcblx0XHRcdFx0XHRcdFx0ZWRpdG9yVHlwZSA9ICR0ZXh0YXJlYS5kYXRhKCdlZGl0b3JUeXBlJykgfHwgJ2NrZWRpdG9yJzsgXG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdHN3aXRjaCAoZWRpdG9yVHlwZSkge1xuXHRcdFx0XHRcdFx0XHRjYXNlICdja2VkaXRvcic6XG5cdFx0XHRcdFx0XHRcdFx0aWYgKENLRURJVE9SLmluc3RhbmNlc1tuYW1lXSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0Q0tFRElUT1IuaW5zdGFuY2VzW25hbWVdLm9uKCdibHVyJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdF9zdG9yZURhdGFUb0hpZGRlbigkbGFuZ3VhZ2VMaW5rcy5maWx0ZXIoJy5hY3RpdmUnKSk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7IFxuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0Y2FzZSAnY29kZW1pcnJvcic6IFxuXHRcdFx0XHRcdFx0XHRcdHZhciAkY29kZU1pcnJvciA9ICR0ZXh0YXJlYS5zaWJsaW5ncygnLkNvZGVNaXJyb3InKSxcblx0XHRcdFx0XHRcdFx0XHRcdGluc3RhbmNlID0gJGNvZGVNaXJyb3IubGVuZ3RoID8gJGNvZGVNaXJyb3JbMF0uQ29kZU1pcnJvciA6IG51bGw7IFxuXHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdGlmIChpbnN0YW5jZSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0aW5zdGFuY2Uub24oJ2JsdXInLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0X3N0b3JlRGF0YVRvSGlkZGVuKCRsYW5ndWFnZUxpbmtzLmZpbHRlcignLmFjdGl2ZScpKTtcblx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHRicmVhazsgXG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGpzZS5saWJzLmVkaXRvcl92YWx1ZXMuc2V0VmFsdWUoJHRleHRhcmVhLCAkdGV4dGFyZWEudmFsKCkpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE9uIENsaWNrIEV2ZW50IEhhbmRsZXJcblx0XHQgKlxuXHRcdCAqIEV2ZW50IGxpc3RlbmVyIHRvIHN0b3JlIGN1cnJlbnQgZGF0YSB0byBoaWRkZW4gZmllbGRzIGFuZCByZXN0b3JlIGhpZGRlblxuXHRcdCAqIGRhdGEgdG8gdGV4dCBmaWVsZHMgaWYgYSBmbGFnIGJ1dHRvbiBnZXRzIGNsaWNrZWRcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBDb250YWlucyBpbmZvcm1hdGlvbiBhYm91dCB0aGUgZXZlbnQuXG5cdFx0ICovXG5cdFx0dmFyIF9jbGlja0hhbmRsZXIgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFxuXHRcdFx0dmFyICRzZWxmID0gJCh0aGlzKTtcblx0XHRcdFxuXHRcdFx0aWYgKCEkc2VsZi5oYXNDbGFzcygnYWN0aXZlJykpIHtcblx0XHRcdFx0XG5cdFx0XHRcdHZhciAkYWN0aXZlQnV0dG9uID0gJGxhbmd1YWdlTGlua3MuZmlsdGVyKCcuYWN0aXZlJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQkbGFuZ3VhZ2VMaW5rcy5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdCRzZWxmLmFkZENsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICgkYWN0aXZlQnV0dG9uLmxlbmd0aCkge1xuXHRcdFx0XHRcdF9zdG9yZURhdGFUb0hpZGRlbigkYWN0aXZlQnV0dG9uKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0X2dldERhdGFGcm9tSGlkZGVuKCRzZWxmKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZSBGaWVsZCBFdmVudCBIYW5kbGVyXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgQ29udGFpbnMgaW5mb3JtYXRpb24gYWJvdXQgdGhlIGV2ZW50LlxuXHRcdCAqL1xuXHRcdHZhciBfdXBkYXRlRmllbGQgPSBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdHZhciAkYWN0aXZlQnV0dG9uID0gJGxhbmd1YWdlTGlua3MuZmlsdGVyKCcuYWN0aXZlJyk7XG5cdFx0XHRfZ2V0RGF0YUZyb21IaWRkZW4oJGFjdGl2ZUJ1dHRvbik7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgTGFuZ3VhZ2Vcblx0XHQgKlxuXHRcdCAqIEZ1bmN0aW9uIHRvIHJldHVybiB0aGUgY3VycmVudCBsYW5ndWFnZSBpZCB2aWEgYW4gZGVmZXJyZWQgb2JqZWN0LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QuXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGRlZmVycmVkIERhdGEgb2JqZWN0IHRoYXQgY29udGFpbnMgdGhlIGRlZmVycmVkIG9iamVjdC5cblx0XHQgKi9cblx0XHR2YXIgX2dldExhbmd1YWdlID0gZnVuY3Rpb24oZXZlbnQsIGRlZmVycmVkKSB7XG5cdFx0XHRpZiAoZGVmZXJyZWQgJiYgZGVmZXJyZWQuZGVmZXJyZWQpIHtcblx0XHRcdFx0dmFyIGxhbmcgPSAkbGFuZ3VhZ2VMaW5rc1xuXHRcdFx0XHRcdC5maWx0ZXIoJy5hY3RpdmUnKVxuXHRcdFx0XHRcdC5maXJzdCgpXG5cdFx0XHRcdFx0LmF0dHIoJ2hyZWYnKVxuXHRcdFx0XHRcdC5zbGljZSgxKTtcblx0XHRcdFx0XG5cdFx0XHRcdGRlZmVycmVkLmRlZmVycmVkLnJlc29sdmUobGFuZyk7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXQgZnVuY3Rpb24gb2YgdGhlIGV4dGVuc2lvbiwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQkbGFuZ3VhZ2VMaW5rcyA9ICR0aGlzLmZpbmQoJy5idXR0b25iYXIgYScpOyAvLyBAdG9kbyBNYWtlIHRoZSBzZWxlY3RvciBkeW5hbWljIHRocm91Z2ggYW4gb3B0aW9uLlxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIEJpbmQgZXZlbnQgbGlzdGVuZXIgdG8gdGhlIGZvcm0gZmllbGRzLCBhbmQgc3RvcmUgdGhlIG5hbWVzIG9mIHRoZSBmaWVsZCBpbiBjYWNoZS4gVG8gcHJldmVudCBcblx0XHRcdCAqIGVtcHR5IGVkaXRvcnMgKGJlY2F1c2Ugb2YgYWxyZWFkeSBsb2FkZWQgZWRpdG9ycyBvbiBpbml0IG9mIHRoaXMgc2NyaXB0IHVwZGF0ZSB0aGVtIHdpdGggdGhlIFxuXHRcdFx0ICogY29ycmVjdCB2YWx1ZS5cblx0XHRcdCAqIFxuXHRcdFx0ICogQHRvZG8gTW92ZSBtZXRob2Qgb3V0c2lkZSB0aGUgaW5pdGlhbGl6ZSBtZXRob2QgKGF2b2lkIGZ1bmN0aW9uIG5lc3Rpbmcgd2l0aG91dCBzcGVjaWZpYyByZWFzb24pLiBcblx0XHRcdCAqL1xuXHRcdFx0dmFyIF9hZGRFdmVudEhhbmRsZXIgPSBmdW5jdGlvbigpIHtcblx0XHRcdFx0bmFtZXMgPSBbXTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIEdldCBhbGwgbmVlZGVkIHNlbGVjdG9ycy5cblx0XHRcdFx0dmFyICRmb3JtRmllbGRzID0gJHRoaXMuZmluZCgnaW5wdXQ6bm90KDpidXR0b24pOm5vdCg6c3VibWl0KSwgc2VsZWN0LCB0ZXh0YXJlYScpXG5cdFx0XHRcdFx0Lm5vdCgnW2RhdGEtbGFuZ3VhZ2Vfc3dpdGNoZXItaWdub3JlXScpO1xuXHRcdFx0XHRcblx0XHRcdFx0JGZvcm1GaWVsZHMuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHR2YXIgJHNlbGYgPSAkKHRoaXMpLFxuXHRcdFx0XHRcdFx0dHlwZSA9IGpzZS5saWJzLmZvcm0uZ2V0RmllbGRUeXBlKCRzZWxmKSxcblx0XHRcdFx0XHRcdGV2ZW50ID0gKCQuaW5BcnJheSh0eXBlLCBbJ3RleHQnLCAndGV4dGFyZWEnXSkgPiAtMSkgPyAnYmx1cicgOiAnY2hhbmdlJyxcblx0XHRcdFx0XHRcdG5hbWUgPSAkc2VsZi5hdHRyKCduYW1lJyk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0bmFtZXMucHVzaChuYW1lKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkc2VsZlxuXHRcdFx0XHRcdFx0Lm9uKGV2ZW50LCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0X3N0b3JlRGF0YVRvSGlkZGVuKCRsYW5ndWFnZUxpbmtzLmZpbHRlcignLmFjdGl2ZScpKTtcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9KTtcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdF9hZGRFdmVudEhhbmRsZXIoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQmluZCBldmVudCBoYW5kbGVyIHRvIHRoZSBmbGFncyBidXR0b25zLlxuXHRcdFx0JGxhbmd1YWdlTGlua3Ncblx0XHRcdFx0Lm9uKCdjbGljaycsIF9jbGlja0hhbmRsZXIpXG5cdFx0XHRcdC5maWx0ZXIoJ1tocmVmPVwiIycgKyBvcHRpb25zLmluaXRMYW5nICsgJ1wiXScpXG5cdFx0XHRcdC50cmlnZ2VyKCdjbGljaycpO1xuXHRcdFx0XG5cdFx0XHQvLyBCaW5kIHRoZSBlZGl0b3IgcmVsYXRlZCBldmVudHMgd2hlbmV2ZXIgdGhlIGVkaXRvciB3aWRnZXQgaXMgcmVhZHkuXG5cdFx0XHQkKHdpbmRvdykub24oJ2VkaXRvcjpyZWFkeScsIF91cGRhdGVFZGl0b3JzKTtcblx0XHRcdFxuXHRcdFx0JCgnZm9ybScpLm9uKCdzdWJtaXQnLCBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdF9zdG9yZURhdGFUb0hpZGRlbigkbGFuZ3VhZ2VMaW5rcy5maWx0ZXIoJy5hY3RpdmUnKSk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JHRoaXNcblx0XHRcdFx0Lm9uKCdsYXllckNsb3NlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0Ly8gV29ya2Fyb3VuZCB0byB1cGRhdGUgdGhlIGhpZGRlbiBmaWVsZHMgb24gbGF5ZXIgY2xvc2UuXG5cdFx0XHRcdFx0X3N0b3JlRGF0YVRvSGlkZGVuKCRsYW5ndWFnZUxpbmtzLmZpbHRlcignLmFjdGl2ZScpKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0Lm9uKCdsYW5ndWFnZV9zd2l0Y2hlci51cGRhdGUnLCBfYWRkRXZlbnRIYW5kbGVyKVxuXHRcdFx0XHQub24oJ2xhbmd1YWdlX3N3aXRjaGVyLnVwZGF0ZUZpZWxkJywgX3VwZGF0ZUZpZWxkKVxuXHRcdFx0XHQub24oJ2xhbmd1YWdlX3N3aXRjaGVyLmdldExhbmcnLCBfZ2V0TGFuZ3VhZ2UpXG5cdFx0XHRcdC5vbignY2xpY2snLCAnLmVkaXRvci1zd2l0Y2gnLCBfdXBkYXRlRWRpdG9ycyk7IFxuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byBtb2R1bGUgZW5naW5lLlxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
