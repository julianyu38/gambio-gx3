'use strict';

/* --------------------------------------------------------------
 ajax_file_upload.js 2016-02-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## AJAX File Upload Extension
 *
 * This extension will enable an existing **input[type=file]** element to upload files through AJAX.
 * The upload method can be invoked either manually by calling the "upload" function or automatically
 * once the file is selected. A "validate" event is triggered before upload starts so that you can
 * validate the selected file before it is uploaded and stop the procedure if needed.
 *
 * Currently the module supports the basic upload functionality but you can add extra logic on your own
 * by following code examples in the official page of the plugin.
 * 
 * The "auto" option is enabled by default which means that the extension will automatically trigger 
 * the upload operation when the user selects a file.
 *
 * {@link https://github.com/blueimp/jQuery-File-Upload/wiki/Basic-plugin}
 *
 * **Important**: If you need to support older versions of Internet Explorer you should use the automatic upload 
 * mode because the manual mode uses the JavaScript File API and this is supported from IE 10+.
 *
 * ### Options
 * 
 * **URL | `data-ajax_upload_file-url` | String | Required**
 * 
 * Define the upload URL that will handle the file upload.
 * 
 * **Auto | `data-ajax_upload_file-auto` | Boolean | Optional**
 * 
 * Define whether the upload process will be started automatically after the user selects a file.
 *
 * ### Events
 * ```javascript
 * // Add your validation rules, triggered before upload (Manual Mode - Requires JS file API support).
 * $('#upload-file').on('validate', function(event, file) {});
 * 
 * // Triggered when server responds to upload request (Manual + Auto Mode).
 * $('#upload-file').on('upload', function(event, response) {});
 * ```
 * 
 * ### Methods
 * ```javascript
 * // Trigger the selected file validation, returns a bool value.
 * $('#upload-file').validate(); 
 * 
 * // Trigger the file upload, callback argument is optional.
 * $('#upload-file').upload(callback); 
 * ```
 *
 * ### Example
 * 
 * **Automatic Upload**
 * 
 * The upload process will be triggered automatically after the user selects a file.
 * 
 * ```html
 * <!-- HTML -->
 * <input id="upload-file" type="file" data-gx-extension="ajax_file_upload"
 *             data-ajax_file_upload-url="http://url/to/upload-script.php" />
 *
 * <!-- JavaScript -->
 * <script>
 *     $('#upload-file').on('validate', function(event, file) {
 *          // Validation Checks (Only IE 10+) ...
 *          return true; // Return true for success or false for failure - will stop the upload.
 *     });
 *
 *     $('#upload-file').on('upload', function(event, response) {
 *          // The "response" parameter contains the server's response information.
 *     });
 * </script>
 * ```
 * 
 * **Manual Upload**
 * 
 * The upload process needs to be triggered through JavaScript as shown in the following example.
 * 
 * 
 * ```html
 * <!-- HTML -->
 * <input id="upload-file" type="file" data-gx-extension="ajax_file_upload"
 *         data-ajax_file_upload-url="http://url/to/upload-script.php" 
 *         data-ajax_file_upload-auto="false" />
 * <button id="upload-file-button">Trigger Upload</button>
 *
 * <!-- JavaScript -->
 * <script>
 *     $('#upload-file-button').on('click', function() {
 *          $('#upload-file').upload(function(response) {
 *              // Callback Function (Optional)
 *          });
 *     });
 * </script>
 * ```
 *
 * @module Admin/Extensions/ajax_file_upload
 * @requires jQuery-File-Upload
 */
gx.extensions.module('ajax_file_upload', [jse.source + '/vendor/blueimp-file-upload/jquery.fileupload.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Extension Reference Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Options for Extension.
  *
  * @type {object}
  */
	defaults = {
		auto: true
	},


	/**
  * Final Extension Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONALITY
	// ------------------------------------------------------------------------

	/**
  * Check method element type.
  *
  * The element that uses the extended jquery methods must be an input[type=file].
  * Otherwise an exception is thrown.
  *
  * @param {object} $element jQuery selector for the element to be checked.
  *
  * @throws Exception when the element called is not a valid input[type=file].
  *
  * @private
  */
	var _checkElementType = function _checkElementType($element) {
		if (!$element.is('input[type=file]')) {
			throw '$.upload() method is supported only in input[type=file] elements.';
		}
	};

	/**
  * Uploads selected file to server.
  *
  * This method uses the JavaScript File API that is supported from IE10+. If
  * you need to support older browser just enable the auto-upload option and do
  * not use this method.
  *
  * @param callback
  */
	var _upload = function _upload(callback) {
		// Trigger "validate" event for file upload element.
		var file = $this.get(0).files[0];
		if (!_validate(file) || !$this.trigger('validate', [file])) {
			return; // Do not continue as validation checks failed.
		}

		// Create a new instance of the plugin and upload the selected file.
		$this.fileupload({
			url: options.url,
			dataType: 'json'
		});

		$this.fileupload('send', {
			files: [file]
		}).success(function (result, textStatus, jqXHR, file) {
			jse.core.debug.info('AJAX File Upload Success Response:', result, textStatus);
			if (typeof callback === 'function') {
				callback(result);
			}
		}).error(function (jqXHR, textStatus, errorThrown) {
			jse.core.debug.error('AJAX File Upload Failure Response:', jqXHR, textStatus, errorThrown);
		}).complete(function (result, textStatus, jqXHR) {
			$this.fileupload('destroy'); // Not necessary anymore.
		});
	};

	/**
  * Default Validation Rules
  *
  * This method will check for invalid filenames or exceeded file size (if necessary).
  *
  * @param {object} file Contains the information of the file to be uploaded.
  */
	var _validate = function _validate(file) {
		// @todo Implement default file validation.
		try {
			// Check if a file was selected.
			if (file === undefined) {
				throw 'No file was selected for upload.';
			}
			return true;
		} catch (exception) {
			jse.core.debug.error(exception);
			return false;
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize function of the extension, called by the engine.
  */
	module.init = function (done) {
		// Check if upload script URL was provided (required value).
		if (options.url === undefined || options.url === '') {
			jse.core.debug.error('Upload URL was not provided for "ajax_file_upload" extension.');
			return;
		}

		if (options.auto === true) {
			$this.fileupload({
				'dataType': 'json',
				'url': options.url,
				done: function done(event, data) {
					$(this).trigger('upload', [data.result]);
				}
			});
		} else {
			// Extend jQuery object with upload method for element.
			$.fn.extend({
				upload: function upload(callback) {
					_checkElementType($(this));
					_upload(callback); // Trigger upload handler
				},
				validate: function validate() {
					_checkElementType($(this));
					return _validate(this.files[0]);
				}
			});
		}

		// Notify engine that the extension initialization is complete.
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFqYXhfZmlsZV91cGxvYWQuanMiXSwibmFtZXMiOlsiZ3giLCJleHRlbnNpb25zIiwibW9kdWxlIiwianNlIiwic291cmNlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwiYXV0byIsIm9wdGlvbnMiLCJleHRlbmQiLCJfY2hlY2tFbGVtZW50VHlwZSIsIiRlbGVtZW50IiwiaXMiLCJfdXBsb2FkIiwiY2FsbGJhY2siLCJmaWxlIiwiZ2V0IiwiZmlsZXMiLCJfdmFsaWRhdGUiLCJ0cmlnZ2VyIiwiZmlsZXVwbG9hZCIsInVybCIsImRhdGFUeXBlIiwic3VjY2VzcyIsInJlc3VsdCIsInRleHRTdGF0dXMiLCJqcVhIUiIsImNvcmUiLCJkZWJ1ZyIsImluZm8iLCJlcnJvciIsImVycm9yVGhyb3duIiwiY29tcGxldGUiLCJ1bmRlZmluZWQiLCJleGNlcHRpb24iLCJpbml0IiwiZG9uZSIsImV2ZW50IiwiZm4iLCJ1cGxvYWQiLCJ2YWxpZGF0ZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnR0FBLEdBQUdDLFVBQUgsQ0FBY0MsTUFBZCxDQUNDLGtCQURELEVBR0MsQ0FDQ0MsSUFBSUMsTUFBSixHQUFhLHNEQURkLENBSEQsRUFPQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsWUFBVztBQUNWQyxRQUFNO0FBREksRUFiWjs7O0FBaUJDOzs7OztBQUtBQyxXQUFVSCxFQUFFSSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJILFFBQW5CLEVBQTZCSCxJQUE3QixDQXRCWDs7O0FBd0JDOzs7OztBQUtBSCxVQUFTLEVBN0JWOztBQStCQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQVlBLEtBQUlVLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQVNDLFFBQVQsRUFBbUI7QUFDMUMsTUFBSSxDQUFDQSxTQUFTQyxFQUFULENBQVksa0JBQVosQ0FBTCxFQUFzQztBQUNyQyxTQUFNLG1FQUFOO0FBQ0E7QUFDRCxFQUpEOztBQU1BOzs7Ozs7Ozs7QUFTQSxLQUFJQyxVQUFVLFNBQVZBLE9BQVUsQ0FBU0MsUUFBVCxFQUFtQjtBQUNoQztBQUNBLE1BQUlDLE9BQU9YLE1BQU1ZLEdBQU4sQ0FBVSxDQUFWLEVBQWFDLEtBQWIsQ0FBbUIsQ0FBbkIsQ0FBWDtBQUNBLE1BQUksQ0FBQ0MsVUFBVUgsSUFBVixDQUFELElBQW9CLENBQUNYLE1BQU1lLE9BQU4sQ0FBYyxVQUFkLEVBQTBCLENBQUNKLElBQUQsQ0FBMUIsQ0FBekIsRUFBNEQ7QUFDM0QsVUFEMkQsQ0FDbkQ7QUFDUjs7QUFFRDtBQUNBWCxRQUFNZ0IsVUFBTixDQUFpQjtBQUNoQkMsUUFBS2IsUUFBUWEsR0FERztBQUVoQkMsYUFBVTtBQUZNLEdBQWpCOztBQUtBbEIsUUFBTWdCLFVBQU4sQ0FBaUIsTUFBakIsRUFBeUI7QUFDdkJILFVBQU8sQ0FBQ0YsSUFBRDtBQURnQixHQUF6QixFQUdFUSxPQUhGLENBR1UsVUFBU0MsTUFBVCxFQUFpQkMsVUFBakIsRUFBNkJDLEtBQTdCLEVBQW9DWCxJQUFwQyxFQUEwQztBQUNsRGQsT0FBSTBCLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxJQUFmLENBQW9CLG9DQUFwQixFQUEwREwsTUFBMUQsRUFBa0VDLFVBQWxFO0FBQ0EsT0FBSSxPQUFPWCxRQUFQLEtBQW9CLFVBQXhCLEVBQW9DO0FBQ25DQSxhQUFTVSxNQUFUO0FBQ0E7QUFDRCxHQVJGLEVBU0VNLEtBVEYsQ0FTUSxVQUFTSixLQUFULEVBQWdCRCxVQUFoQixFQUE0Qk0sV0FBNUIsRUFBeUM7QUFDL0M5QixPQUFJMEIsSUFBSixDQUFTQyxLQUFULENBQWVFLEtBQWYsQ0FBcUIsb0NBQXJCLEVBQTJESixLQUEzRCxFQUFrRUQsVUFBbEUsRUFBOEVNLFdBQTlFO0FBQ0EsR0FYRixFQVlFQyxRQVpGLENBWVcsVUFBU1IsTUFBVCxFQUFpQkMsVUFBakIsRUFBNkJDLEtBQTdCLEVBQW9DO0FBQzdDdEIsU0FBTWdCLFVBQU4sQ0FBaUIsU0FBakIsRUFENkMsQ0FDaEI7QUFDN0IsR0FkRjtBQWVBLEVBNUJEOztBQThCQTs7Ozs7OztBQU9BLEtBQUlGLFlBQVksU0FBWkEsU0FBWSxDQUFTSCxJQUFULEVBQWU7QUFDOUI7QUFDQSxNQUFJO0FBQ0g7QUFDQSxPQUFJQSxTQUFTa0IsU0FBYixFQUF3QjtBQUN2QixVQUFNLGtDQUFOO0FBQ0E7QUFDRCxVQUFPLElBQVA7QUFDQSxHQU5ELENBTUUsT0FBT0MsU0FBUCxFQUFrQjtBQUNuQmpDLE9BQUkwQixJQUFKLENBQVNDLEtBQVQsQ0FBZUUsS0FBZixDQUFxQkksU0FBckI7QUFDQSxVQUFPLEtBQVA7QUFDQTtBQUNELEVBWkQ7O0FBY0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQWxDLFFBQU9tQyxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCO0FBQ0EsTUFBSTVCLFFBQVFhLEdBQVIsS0FBZ0JZLFNBQWhCLElBQTZCekIsUUFBUWEsR0FBUixLQUFnQixFQUFqRCxFQUFxRDtBQUNwRHBCLE9BQUkwQixJQUFKLENBQVNDLEtBQVQsQ0FBZUUsS0FBZixDQUFxQiwrREFBckI7QUFDQTtBQUNBOztBQUVELE1BQUl0QixRQUFRRCxJQUFSLEtBQWlCLElBQXJCLEVBQTJCO0FBQzFCSCxTQUFNZ0IsVUFBTixDQUFpQjtBQUNoQixnQkFBWSxNQURJO0FBRWhCLFdBQU9aLFFBQVFhLEdBRkM7QUFHaEJlLFVBQU0sY0FBU0MsS0FBVCxFQUFnQmxDLElBQWhCLEVBQXNCO0FBQzNCRSxPQUFFLElBQUYsRUFBUWMsT0FBUixDQUFnQixRQUFoQixFQUEwQixDQUFDaEIsS0FBS3FCLE1BQU4sQ0FBMUI7QUFDQTtBQUxlLElBQWpCO0FBT0EsR0FSRCxNQVFPO0FBQ047QUFDQW5CLEtBQUVpQyxFQUFGLENBQUs3QixNQUFMLENBQVk7QUFDWDhCLFlBQVEsZ0JBQVN6QixRQUFULEVBQW1CO0FBQzFCSix1QkFBa0JMLEVBQUUsSUFBRixDQUFsQjtBQUNBUSxhQUFRQyxRQUFSLEVBRjBCLENBRVA7QUFDbkIsS0FKVTtBQUtYMEIsY0FBVSxvQkFBVztBQUNwQjlCLHVCQUFrQkwsRUFBRSxJQUFGLENBQWxCO0FBQ0EsWUFBT2EsVUFBVSxLQUFLRCxLQUFMLENBQVcsQ0FBWCxDQUFWLENBQVA7QUFDQTtBQVJVLElBQVo7QUFVQTs7QUFFRDtBQUNBbUI7QUFDQSxFQS9CRDs7QUFpQ0E7QUFDQSxRQUFPcEMsTUFBUDtBQUNBLENBMUtGIiwiZmlsZSI6ImFqYXhfZmlsZV91cGxvYWQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGFqYXhfZmlsZV91cGxvYWQuanMgMjAxNi0wMi0xMVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgQUpBWCBGaWxlIFVwbG9hZCBFeHRlbnNpb25cbiAqXG4gKiBUaGlzIGV4dGVuc2lvbiB3aWxsIGVuYWJsZSBhbiBleGlzdGluZyAqKmlucHV0W3R5cGU9ZmlsZV0qKiBlbGVtZW50IHRvIHVwbG9hZCBmaWxlcyB0aHJvdWdoIEFKQVguXG4gKiBUaGUgdXBsb2FkIG1ldGhvZCBjYW4gYmUgaW52b2tlZCBlaXRoZXIgbWFudWFsbHkgYnkgY2FsbGluZyB0aGUgXCJ1cGxvYWRcIiBmdW5jdGlvbiBvciBhdXRvbWF0aWNhbGx5XG4gKiBvbmNlIHRoZSBmaWxlIGlzIHNlbGVjdGVkLiBBIFwidmFsaWRhdGVcIiBldmVudCBpcyB0cmlnZ2VyZWQgYmVmb3JlIHVwbG9hZCBzdGFydHMgc28gdGhhdCB5b3UgY2FuXG4gKiB2YWxpZGF0ZSB0aGUgc2VsZWN0ZWQgZmlsZSBiZWZvcmUgaXQgaXMgdXBsb2FkZWQgYW5kIHN0b3AgdGhlIHByb2NlZHVyZSBpZiBuZWVkZWQuXG4gKlxuICogQ3VycmVudGx5IHRoZSBtb2R1bGUgc3VwcG9ydHMgdGhlIGJhc2ljIHVwbG9hZCBmdW5jdGlvbmFsaXR5IGJ1dCB5b3UgY2FuIGFkZCBleHRyYSBsb2dpYyBvbiB5b3VyIG93blxuICogYnkgZm9sbG93aW5nIGNvZGUgZXhhbXBsZXMgaW4gdGhlIG9mZmljaWFsIHBhZ2Ugb2YgdGhlIHBsdWdpbi5cbiAqIFxuICogVGhlIFwiYXV0b1wiIG9wdGlvbiBpcyBlbmFibGVkIGJ5IGRlZmF1bHQgd2hpY2ggbWVhbnMgdGhhdCB0aGUgZXh0ZW5zaW9uIHdpbGwgYXV0b21hdGljYWxseSB0cmlnZ2VyIFxuICogdGhlIHVwbG9hZCBvcGVyYXRpb24gd2hlbiB0aGUgdXNlciBzZWxlY3RzIGEgZmlsZS5cbiAqXG4gKiB7QGxpbmsgaHR0cHM6Ly9naXRodWIuY29tL2JsdWVpbXAvalF1ZXJ5LUZpbGUtVXBsb2FkL3dpa2kvQmFzaWMtcGx1Z2lufVxuICpcbiAqICoqSW1wb3J0YW50Kio6IElmIHlvdSBuZWVkIHRvIHN1cHBvcnQgb2xkZXIgdmVyc2lvbnMgb2YgSW50ZXJuZXQgRXhwbG9yZXIgeW91IHNob3VsZCB1c2UgdGhlIGF1dG9tYXRpYyB1cGxvYWQgXG4gKiBtb2RlIGJlY2F1c2UgdGhlIG1hbnVhbCBtb2RlIHVzZXMgdGhlIEphdmFTY3JpcHQgRmlsZSBBUEkgYW5kIHRoaXMgaXMgc3VwcG9ydGVkIGZyb20gSUUgMTArLlxuICpcbiAqICMjIyBPcHRpb25zXG4gKiBcbiAqICoqVVJMIHwgYGRhdGEtYWpheF91cGxvYWRfZmlsZS11cmxgIHwgU3RyaW5nIHwgUmVxdWlyZWQqKlxuICogXG4gKiBEZWZpbmUgdGhlIHVwbG9hZCBVUkwgdGhhdCB3aWxsIGhhbmRsZSB0aGUgZmlsZSB1cGxvYWQuXG4gKiBcbiAqICoqQXV0byB8IGBkYXRhLWFqYXhfdXBsb2FkX2ZpbGUtYXV0b2AgfCBCb29sZWFuIHwgT3B0aW9uYWwqKlxuICogXG4gKiBEZWZpbmUgd2hldGhlciB0aGUgdXBsb2FkIHByb2Nlc3Mgd2lsbCBiZSBzdGFydGVkIGF1dG9tYXRpY2FsbHkgYWZ0ZXIgdGhlIHVzZXIgc2VsZWN0cyBhIGZpbGUuXG4gKlxuICogIyMjIEV2ZW50c1xuICogYGBgamF2YXNjcmlwdFxuICogLy8gQWRkIHlvdXIgdmFsaWRhdGlvbiBydWxlcywgdHJpZ2dlcmVkIGJlZm9yZSB1cGxvYWQgKE1hbnVhbCBNb2RlIC0gUmVxdWlyZXMgSlMgZmlsZSBBUEkgc3VwcG9ydCkuXG4gKiAkKCcjdXBsb2FkLWZpbGUnKS5vbigndmFsaWRhdGUnLCBmdW5jdGlvbihldmVudCwgZmlsZSkge30pO1xuICogXG4gKiAvLyBUcmlnZ2VyZWQgd2hlbiBzZXJ2ZXIgcmVzcG9uZHMgdG8gdXBsb2FkIHJlcXVlc3QgKE1hbnVhbCArIEF1dG8gTW9kZSkuXG4gKiAkKCcjdXBsb2FkLWZpbGUnKS5vbigndXBsb2FkJywgZnVuY3Rpb24oZXZlbnQsIHJlc3BvbnNlKSB7fSk7XG4gKiBgYGBcbiAqIFxuICogIyMjIE1ldGhvZHNcbiAqIGBgYGphdmFzY3JpcHRcbiAqIC8vIFRyaWdnZXIgdGhlIHNlbGVjdGVkIGZpbGUgdmFsaWRhdGlvbiwgcmV0dXJucyBhIGJvb2wgdmFsdWUuXG4gKiAkKCcjdXBsb2FkLWZpbGUnKS52YWxpZGF0ZSgpOyBcbiAqIFxuICogLy8gVHJpZ2dlciB0aGUgZmlsZSB1cGxvYWQsIGNhbGxiYWNrIGFyZ3VtZW50IGlzIG9wdGlvbmFsLlxuICogJCgnI3VwbG9hZC1maWxlJykudXBsb2FkKGNhbGxiYWNrKTsgXG4gKiBgYGBcbiAqXG4gKiAjIyMgRXhhbXBsZVxuICogXG4gKiAqKkF1dG9tYXRpYyBVcGxvYWQqKlxuICogXG4gKiBUaGUgdXBsb2FkIHByb2Nlc3Mgd2lsbCBiZSB0cmlnZ2VyZWQgYXV0b21hdGljYWxseSBhZnRlciB0aGUgdXNlciBzZWxlY3RzIGEgZmlsZS5cbiAqIFxuICogYGBgaHRtbFxuICogPCEtLSBIVE1MIC0tPlxuICogPGlucHV0IGlkPVwidXBsb2FkLWZpbGVcIiB0eXBlPVwiZmlsZVwiIGRhdGEtZ3gtZXh0ZW5zaW9uPVwiYWpheF9maWxlX3VwbG9hZFwiXG4gKiAgICAgICAgICAgICBkYXRhLWFqYXhfZmlsZV91cGxvYWQtdXJsPVwiaHR0cDovL3VybC90by91cGxvYWQtc2NyaXB0LnBocFwiIC8+XG4gKlxuICogPCEtLSBKYXZhU2NyaXB0IC0tPlxuICogPHNjcmlwdD5cbiAqICAgICAkKCcjdXBsb2FkLWZpbGUnKS5vbigndmFsaWRhdGUnLCBmdW5jdGlvbihldmVudCwgZmlsZSkge1xuICogICAgICAgICAgLy8gVmFsaWRhdGlvbiBDaGVja3MgKE9ubHkgSUUgMTArKSAuLi5cbiAqICAgICAgICAgIHJldHVybiB0cnVlOyAvLyBSZXR1cm4gdHJ1ZSBmb3Igc3VjY2VzcyBvciBmYWxzZSBmb3IgZmFpbHVyZSAtIHdpbGwgc3RvcCB0aGUgdXBsb2FkLlxuICogICAgIH0pO1xuICpcbiAqICAgICAkKCcjdXBsb2FkLWZpbGUnKS5vbigndXBsb2FkJywgZnVuY3Rpb24oZXZlbnQsIHJlc3BvbnNlKSB7XG4gKiAgICAgICAgICAvLyBUaGUgXCJyZXNwb25zZVwiIHBhcmFtZXRlciBjb250YWlucyB0aGUgc2VydmVyJ3MgcmVzcG9uc2UgaW5mb3JtYXRpb24uXG4gKiAgICAgfSk7XG4gKiA8L3NjcmlwdD5cbiAqIGBgYFxuICogXG4gKiAqKk1hbnVhbCBVcGxvYWQqKlxuICogXG4gKiBUaGUgdXBsb2FkIHByb2Nlc3MgbmVlZHMgdG8gYmUgdHJpZ2dlcmVkIHRocm91Z2ggSmF2YVNjcmlwdCBhcyBzaG93biBpbiB0aGUgZm9sbG93aW5nIGV4YW1wbGUuXG4gKiBcbiAqIFxuICogYGBgaHRtbFxuICogPCEtLSBIVE1MIC0tPlxuICogPGlucHV0IGlkPVwidXBsb2FkLWZpbGVcIiB0eXBlPVwiZmlsZVwiIGRhdGEtZ3gtZXh0ZW5zaW9uPVwiYWpheF9maWxlX3VwbG9hZFwiXG4gKiAgICAgICAgIGRhdGEtYWpheF9maWxlX3VwbG9hZC11cmw9XCJodHRwOi8vdXJsL3RvL3VwbG9hZC1zY3JpcHQucGhwXCIgXG4gKiAgICAgICAgIGRhdGEtYWpheF9maWxlX3VwbG9hZC1hdXRvPVwiZmFsc2VcIiAvPlxuICogPGJ1dHRvbiBpZD1cInVwbG9hZC1maWxlLWJ1dHRvblwiPlRyaWdnZXIgVXBsb2FkPC9idXR0b24+XG4gKlxuICogPCEtLSBKYXZhU2NyaXB0IC0tPlxuICogPHNjcmlwdD5cbiAqICAgICAkKCcjdXBsb2FkLWZpbGUtYnV0dG9uJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gKiAgICAgICAgICAkKCcjdXBsb2FkLWZpbGUnKS51cGxvYWQoZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAqICAgICAgICAgICAgICAvLyBDYWxsYmFjayBGdW5jdGlvbiAoT3B0aW9uYWwpXG4gKiAgICAgICAgICB9KTtcbiAqICAgICB9KTtcbiAqIDwvc2NyaXB0PlxuICogYGBgXG4gKlxuICogQG1vZHVsZSBBZG1pbi9FeHRlbnNpb25zL2FqYXhfZmlsZV91cGxvYWRcbiAqIEByZXF1aXJlcyBqUXVlcnktRmlsZS1VcGxvYWRcbiAqL1xuZ3guZXh0ZW5zaW9ucy5tb2R1bGUoXG5cdCdhamF4X2ZpbGVfdXBsb2FkJyxcblx0XG5cdFtcblx0XHRqc2Uuc291cmNlICsgJy92ZW5kb3IvYmx1ZWltcC1maWxlLXVwbG9hZC9qcXVlcnkuZmlsZXVwbG9hZC5taW4uanMnXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogRXh0ZW5zaW9uIFJlZmVyZW5jZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE9wdGlvbnMgZm9yIEV4dGVuc2lvbi5cblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRkZWZhdWx0cyA9IHtcblx0XHRcdFx0YXV0bzogdHJ1ZVxuXHRcdFx0fSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBGaW5hbCBFeHRlbnNpb24gT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTkFMSVRZXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2hlY2sgbWV0aG9kIGVsZW1lbnQgdHlwZS5cblx0XHQgKlxuXHRcdCAqIFRoZSBlbGVtZW50IHRoYXQgdXNlcyB0aGUgZXh0ZW5kZWQganF1ZXJ5IG1ldGhvZHMgbXVzdCBiZSBhbiBpbnB1dFt0eXBlPWZpbGVdLlxuXHRcdCAqIE90aGVyd2lzZSBhbiBleGNlcHRpb24gaXMgdGhyb3duLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9ICRlbGVtZW50IGpRdWVyeSBzZWxlY3RvciBmb3IgdGhlIGVsZW1lbnQgdG8gYmUgY2hlY2tlZC5cblx0XHQgKlxuXHRcdCAqIEB0aHJvd3MgRXhjZXB0aW9uIHdoZW4gdGhlIGVsZW1lbnQgY2FsbGVkIGlzIG5vdCBhIHZhbGlkIGlucHV0W3R5cGU9ZmlsZV0uXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdHZhciBfY2hlY2tFbGVtZW50VHlwZSA9IGZ1bmN0aW9uKCRlbGVtZW50KSB7XG5cdFx0XHRpZiAoISRlbGVtZW50LmlzKCdpbnB1dFt0eXBlPWZpbGVdJykpIHtcblx0XHRcdFx0dGhyb3cgJyQudXBsb2FkKCkgbWV0aG9kIGlzIHN1cHBvcnRlZCBvbmx5IGluIGlucHV0W3R5cGU9ZmlsZV0gZWxlbWVudHMuJztcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwbG9hZHMgc2VsZWN0ZWQgZmlsZSB0byBzZXJ2ZXIuXG5cdFx0ICpcblx0XHQgKiBUaGlzIG1ldGhvZCB1c2VzIHRoZSBKYXZhU2NyaXB0IEZpbGUgQVBJIHRoYXQgaXMgc3VwcG9ydGVkIGZyb20gSUUxMCsuIElmXG5cdFx0ICogeW91IG5lZWQgdG8gc3VwcG9ydCBvbGRlciBicm93c2VyIGp1c3QgZW5hYmxlIHRoZSBhdXRvLXVwbG9hZCBvcHRpb24gYW5kIGRvXG5cdFx0ICogbm90IHVzZSB0aGlzIG1ldGhvZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSBjYWxsYmFja1xuXHRcdCAqL1xuXHRcdHZhciBfdXBsb2FkID0gZnVuY3Rpb24oY2FsbGJhY2spIHtcblx0XHRcdC8vIFRyaWdnZXIgXCJ2YWxpZGF0ZVwiIGV2ZW50IGZvciBmaWxlIHVwbG9hZCBlbGVtZW50LlxuXHRcdFx0dmFyIGZpbGUgPSAkdGhpcy5nZXQoMCkuZmlsZXNbMF07XG5cdFx0XHRpZiAoIV92YWxpZGF0ZShmaWxlKSB8fCAhJHRoaXMudHJpZ2dlcigndmFsaWRhdGUnLCBbZmlsZV0pKSB7XG5cdFx0XHRcdHJldHVybjsgLy8gRG8gbm90IGNvbnRpbnVlIGFzIHZhbGlkYXRpb24gY2hlY2tzIGZhaWxlZC5cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gQ3JlYXRlIGEgbmV3IGluc3RhbmNlIG9mIHRoZSBwbHVnaW4gYW5kIHVwbG9hZCB0aGUgc2VsZWN0ZWQgZmlsZS5cblx0XHRcdCR0aGlzLmZpbGV1cGxvYWQoe1xuXHRcdFx0XHR1cmw6IG9wdGlvbnMudXJsLFxuXHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JHRoaXMuZmlsZXVwbG9hZCgnc2VuZCcsIHtcblx0XHRcdFx0XHRmaWxlczogW2ZpbGVdXG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5zdWNjZXNzKGZ1bmN0aW9uKHJlc3VsdCwgdGV4dFN0YXR1cywganFYSFIsIGZpbGUpIHtcblx0XHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5pbmZvKCdBSkFYIEZpbGUgVXBsb2FkIFN1Y2Nlc3MgUmVzcG9uc2U6JywgcmVzdWx0LCB0ZXh0U3RhdHVzKTtcblx0XHRcdFx0XHRpZiAodHlwZW9mIGNhbGxiYWNrID09PSAnZnVuY3Rpb24nKSB7XG5cdFx0XHRcdFx0XHRjYWxsYmFjayhyZXN1bHQpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSlcblx0XHRcdFx0LmVycm9yKGZ1bmN0aW9uKGpxWEhSLCB0ZXh0U3RhdHVzLCBlcnJvclRocm93bikge1xuXHRcdFx0XHRcdGpzZS5jb3JlLmRlYnVnLmVycm9yKCdBSkFYIEZpbGUgVXBsb2FkIEZhaWx1cmUgUmVzcG9uc2U6JywganFYSFIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LmNvbXBsZXRlKGZ1bmN0aW9uKHJlc3VsdCwgdGV4dFN0YXR1cywganFYSFIpIHtcblx0XHRcdFx0XHQkdGhpcy5maWxldXBsb2FkKCdkZXN0cm95Jyk7IC8vIE5vdCBuZWNlc3NhcnkgYW55bW9yZS5cblx0XHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWZhdWx0IFZhbGlkYXRpb24gUnVsZXNcblx0XHQgKlxuXHRcdCAqIFRoaXMgbWV0aG9kIHdpbGwgY2hlY2sgZm9yIGludmFsaWQgZmlsZW5hbWVzIG9yIGV4Y2VlZGVkIGZpbGUgc2l6ZSAoaWYgbmVjZXNzYXJ5KS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBmaWxlIENvbnRhaW5zIHRoZSBpbmZvcm1hdGlvbiBvZiB0aGUgZmlsZSB0byBiZSB1cGxvYWRlZC5cblx0XHQgKi9cblx0XHR2YXIgX3ZhbGlkYXRlID0gZnVuY3Rpb24oZmlsZSkge1xuXHRcdFx0Ly8gQHRvZG8gSW1wbGVtZW50IGRlZmF1bHQgZmlsZSB2YWxpZGF0aW9uLlxuXHRcdFx0dHJ5IHtcblx0XHRcdFx0Ly8gQ2hlY2sgaWYgYSBmaWxlIHdhcyBzZWxlY3RlZC5cblx0XHRcdFx0aWYgKGZpbGUgPT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRcdHRocm93ICdObyBmaWxlIHdhcyBzZWxlY3RlZCBmb3IgdXBsb2FkLic7XG5cdFx0XHRcdH1cblx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHR9IGNhdGNoIChleGNlcHRpb24pIHtcblx0XHRcdFx0anNlLmNvcmUuZGVidWcuZXJyb3IoZXhjZXB0aW9uKTtcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIGZ1bmN0aW9uIG9mIHRoZSBleHRlbnNpb24sIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0Ly8gQ2hlY2sgaWYgdXBsb2FkIHNjcmlwdCBVUkwgd2FzIHByb3ZpZGVkIChyZXF1aXJlZCB2YWx1ZSkuXG5cdFx0XHRpZiAob3B0aW9ucy51cmwgPT09IHVuZGVmaW5lZCB8fCBvcHRpb25zLnVybCA9PT0gJycpIHtcblx0XHRcdFx0anNlLmNvcmUuZGVidWcuZXJyb3IoJ1VwbG9hZCBVUkwgd2FzIG5vdCBwcm92aWRlZCBmb3IgXCJhamF4X2ZpbGVfdXBsb2FkXCIgZXh0ZW5zaW9uLicpO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGlmIChvcHRpb25zLmF1dG8gPT09IHRydWUpIHtcblx0XHRcdFx0JHRoaXMuZmlsZXVwbG9hZCh7XG5cdFx0XHRcdFx0J2RhdGFUeXBlJzogJ2pzb24nLFxuXHRcdFx0XHRcdCd1cmwnOiBvcHRpb25zLnVybCxcblx0XHRcdFx0XHRkb25lOiBmdW5jdGlvbihldmVudCwgZGF0YSkge1xuXHRcdFx0XHRcdFx0JCh0aGlzKS50cmlnZ2VyKCd1cGxvYWQnLCBbZGF0YS5yZXN1bHRdKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Ly8gRXh0ZW5kIGpRdWVyeSBvYmplY3Qgd2l0aCB1cGxvYWQgbWV0aG9kIGZvciBlbGVtZW50LlxuXHRcdFx0XHQkLmZuLmV4dGVuZCh7XG5cdFx0XHRcdFx0dXBsb2FkOiBmdW5jdGlvbihjYWxsYmFjaykge1xuXHRcdFx0XHRcdFx0X2NoZWNrRWxlbWVudFR5cGUoJCh0aGlzKSk7XG5cdFx0XHRcdFx0XHRfdXBsb2FkKGNhbGxiYWNrKTsgLy8gVHJpZ2dlciB1cGxvYWQgaGFuZGxlclxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0dmFsaWRhdGU6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0X2NoZWNrRWxlbWVudFR5cGUoJCh0aGlzKSk7XG5cdFx0XHRcdFx0XHRyZXR1cm4gX3ZhbGlkYXRlKHRoaXMuZmlsZXNbMF0pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIE5vdGlmeSBlbmdpbmUgdGhhdCB0aGUgZXh0ZW5zaW9uIGluaXRpYWxpemF0aW9uIGlzIGNvbXBsZXRlLlxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
