"use strict";

/* --------------------------------------------------------------
 canvas_area_draw.js 2016-10-27
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Canvas image map area draw plugin extension.
 *
 * Extension to initialize the jQuery plugin 'jquery-canvas-area-draw'.
 *
 * Options:
 *      - image-url {String} URL to image.
 *      - coordinates {String} Comma-separated coordinate values (optional).
 *
 * Events:
 *      - 'reset' to delete shape.
 *
 * @module Admin/Extensions/canvas_area_draw
 */
gx.extensions.module('canvas_area_draw', [], function (data) {
	"use strict";

	// Module element, which represents a form.

	var $this = $(this);

	// Module options.
	var options = $.extend(true, {}, data);

	// Module object.
	var module = {};

	// Module initialize function.
	module.init = function (done) {
		$this.canvasAreaDraw(options);
		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhbnZhc19hcmVhX2RyYXcuanMiXSwibmFtZXMiOlsiZ3giLCJleHRlbnNpb25zIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsIm9wdGlvbnMiLCJleHRlbmQiLCJpbml0IiwiY2FudmFzQXJlYURyYXciLCJkb25lIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7O0FBY0FBLEdBQUdDLFVBQUgsQ0FBY0MsTUFBZCxDQUNDLGtCQURELEVBR0MsRUFIRCxFQUtDLFVBQVNDLElBQVQsRUFBZTtBQUNkOztBQUVBOztBQUNBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBO0FBQ0EsS0FBTUMsVUFBVUQsRUFBRUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CSixJQUFuQixDQUFoQjs7QUFFQTtBQUNBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTtBQUNBQSxRQUFPTSxJQUFQLEdBQWMsZ0JBQVE7QUFDckJKLFFBQU1LLGNBQU4sQ0FBcUJILE9BQXJCO0FBQ0FJO0FBQ0EsRUFIRDs7QUFLQTtBQUNBLFFBQU9SLE1BQVA7QUFDQSxDQXpCRiIsImZpbGUiOiJjYW52YXNfYXJlYV9kcmF3LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBjYW52YXNfYXJlYV9kcmF3LmpzIDIwMTYtMTAtMjdcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4vKipcbiAqIENhbnZhcyBpbWFnZSBtYXAgYXJlYSBkcmF3IHBsdWdpbiBleHRlbnNpb24uXG4gKlxuICogRXh0ZW5zaW9uIHRvIGluaXRpYWxpemUgdGhlIGpRdWVyeSBwbHVnaW4gJ2pxdWVyeS1jYW52YXMtYXJlYS1kcmF3Jy5cbiAqXG4gKiBPcHRpb25zOlxuICogICAgICAtIGltYWdlLXVybCB7U3RyaW5nfSBVUkwgdG8gaW1hZ2UuXG4gKiAgICAgIC0gY29vcmRpbmF0ZXMge1N0cmluZ30gQ29tbWEtc2VwYXJhdGVkIGNvb3JkaW5hdGUgdmFsdWVzIChvcHRpb25hbCkuXG4gKlxuICogRXZlbnRzOlxuICogICAgICAtICdyZXNldCcgdG8gZGVsZXRlIHNoYXBlLlxuICpcbiAqIEBtb2R1bGUgQWRtaW4vRXh0ZW5zaW9ucy9jYW52YXNfYXJlYV9kcmF3XG4gKi9cbmd4LmV4dGVuc2lvbnMubW9kdWxlKFxuXHQnY2FudmFzX2FyZWFfZHJhdycsXG5cdFxuXHRbXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcInVzZSBzdHJpY3RcIjtcblx0XHRcblx0XHQvLyBNb2R1bGUgZWxlbWVudCwgd2hpY2ggcmVwcmVzZW50cyBhIGZvcm0uXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8vIE1vZHVsZSBvcHRpb25zLlxuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGF0YSk7XG5cdFx0XG5cdFx0Ly8gTW9kdWxlIG9iamVjdC5cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyBNb2R1bGUgaW5pdGlhbGl6ZSBmdW5jdGlvbi5cblx0XHRtb2R1bGUuaW5pdCA9IGRvbmUgPT4ge1xuXHRcdFx0JHRoaXMuY2FudmFzQXJlYURyYXcob3B0aW9ucyk7XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyBSZXR1cm4gZGF0YSB0byBtb2R1bGUgZW5naW5lLlxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7Il19
