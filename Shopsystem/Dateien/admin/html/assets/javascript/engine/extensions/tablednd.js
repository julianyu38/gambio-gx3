'use strict';

/* --------------------------------------------------------------
 tablednd.js 2015-09-17 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Table Dnd Extension
 *
 * Sorts lines in connected tables.
 *
 * @module Admin/Extensions/tablednd
 * @ignore
 */
gx.extensions.module('tablednd', [jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.min.js'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Extension Reference
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Table Body Selector
  *
  * @type {object}
  */
	$tbody = null,


	/**
  * Default Options for Extension
  *
  * @type {object}
  */
	defaults = {
		'addclass': 'clsDnd', // classname added to body
		'disabledclass': 'sort-disabled', // classname added to body
		'handle': false // handler which enables the sortable
	},


	/**
  * Final Extension Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Setup Dummies
  */
	var setupDummies = function setupDummies() {
		// On drag stop, update dummy line visibility
		$tbody.each(function () {
			var $self = $(this),
			    $sortDisabled = $self.find('.' + options.disabledclass);

			if ($self.children().length > 1) {
				$sortDisabled.hide();
			} else {
				$sortDisabled.show();
			}

			var rowHidden = $sortDisabled.clone();
			$sortDisabled.remove();
			$self.prepend(rowHidden);
		});
	};

	/**
  * Initialize method of the extension, called by the engine.
  */
	module.init = function (done) {
		$tbody = $this.find('tbody');
		var strTimestamp = parseInt(new Date().getTime() * Math.random(), 10),
		    strClsDnd = options.addclass + '_' + strTimestamp,
		    config = {
			'handle': options.handle,
			'connectWith': '.' + strClsDnd,
			'containment': $this,
			'sort': function sort(event, ui) {
				$(event.target).each(function () {
					var $self = $(this),
					    $sortDisabled = $self.find('.' + options.disabledclass);

					if ($self.children().length > 2) {
						$sortDisabled.hide();
					} else {
						$sortDisabled.show();
						var rowHidden = $sortDisabled.clone();
						$sortDisabled.remove();
						$self.append(rowHidden);
					}
				});
			},
			'stop': function stop(event, ui) {
				setupDummies();
				// Trigger an update event on table
				$this.trigger('tablednd.update', []);
			}
		};

		// Add a special class and start the sortable plugin.
		$tbody.addClass(strClsDnd).sortable(config);

		setupDummies();

		done();
	};

	// Return data to module engine.
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYmxlZG5kLmpzIl0sIm5hbWVzIjpbImd4IiwiZXh0ZW5zaW9ucyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkdGJvZHkiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJzZXR1cER1bW1pZXMiLCJlYWNoIiwiJHNlbGYiLCIkc29ydERpc2FibGVkIiwiZmluZCIsImRpc2FibGVkY2xhc3MiLCJjaGlsZHJlbiIsImxlbmd0aCIsImhpZGUiLCJzaG93Iiwicm93SGlkZGVuIiwiY2xvbmUiLCJyZW1vdmUiLCJwcmVwZW5kIiwiaW5pdCIsImRvbmUiLCJzdHJUaW1lc3RhbXAiLCJwYXJzZUludCIsIkRhdGUiLCJnZXRUaW1lIiwiTWF0aCIsInJhbmRvbSIsInN0ckNsc0RuZCIsImFkZGNsYXNzIiwiY29uZmlnIiwiaGFuZGxlIiwiZXZlbnQiLCJ1aSIsInRhcmdldCIsImFwcGVuZCIsInRyaWdnZXIiLCJhZGRDbGFzcyIsInNvcnRhYmxlIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7O0FBUUFBLEdBQUdDLFVBQUgsQ0FBY0MsTUFBZCxDQUNDLFVBREQsRUFHQyxDQUNDQyxJQUFJQyxNQUFKLEdBQWEscUNBRGQsRUFFQ0QsSUFBSUMsTUFBSixHQUFhLG9DQUZkLENBSEQsRUFRQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0M7Ozs7O0FBS0FDLFNBQVFDLEVBQUUsSUFBRixDQU5UOzs7QUFRQzs7Ozs7QUFLQUMsVUFBUyxJQWJWOzs7QUFlQzs7Ozs7QUFLQUMsWUFBVztBQUNWLGNBQVksUUFERixFQUNZO0FBQ3RCLG1CQUFpQixlQUZQLEVBRXdCO0FBQ2xDLFlBQVUsS0FIQSxDQUdNO0FBSE4sRUFwQlo7OztBQTBCQzs7Ozs7QUFLQUMsV0FBVUgsRUFBRUksTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkosSUFBN0IsQ0EvQlg7OztBQWlDQzs7Ozs7QUFLQUgsVUFBUyxFQXRDVjs7QUF3Q0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxLQUFJVSxlQUFlLFNBQWZBLFlBQWUsR0FBVztBQUM3QjtBQUNBSixTQUFPSyxJQUFQLENBQVksWUFBVztBQUN0QixPQUFJQyxRQUFRUCxFQUFFLElBQUYsQ0FBWjtBQUFBLE9BQ0NRLGdCQUFnQkQsTUFBTUUsSUFBTixDQUFXLE1BQU1OLFFBQVFPLGFBQXpCLENBRGpCOztBQUdBLE9BQUlILE1BQU1JLFFBQU4sR0FBaUJDLE1BQWpCLEdBQTBCLENBQTlCLEVBQWlDO0FBQ2hDSixrQkFBY0ssSUFBZDtBQUNBLElBRkQsTUFFTztBQUNOTCxrQkFBY00sSUFBZDtBQUNBOztBQUVELE9BQUlDLFlBQVlQLGNBQWNRLEtBQWQsRUFBaEI7QUFDQVIsaUJBQWNTLE1BQWQ7QUFDQVYsU0FBTVcsT0FBTixDQUFjSCxTQUFkO0FBRUEsR0FkRDtBQWVBLEVBakJEOztBQW1CQTs7O0FBR0FwQixRQUFPd0IsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1Qm5CLFdBQVNGLE1BQU1VLElBQU4sQ0FBVyxPQUFYLENBQVQ7QUFDQSxNQUFJWSxlQUFlQyxTQUFTLElBQUlDLElBQUosR0FBV0MsT0FBWCxLQUF1QkMsS0FBS0MsTUFBTCxFQUFoQyxFQUErQyxFQUEvQyxDQUFuQjtBQUFBLE1BQ0NDLFlBQVl4QixRQUFReUIsUUFBUixHQUFtQixHQUFuQixHQUF5QlAsWUFEdEM7QUFBQSxNQUVDUSxTQUFTO0FBQ1IsYUFBVTFCLFFBQVEyQixNQURWO0FBRVIsa0JBQWUsTUFBTUgsU0FGYjtBQUdSLGtCQUFlNUIsS0FIUDtBQUlSLFdBQVEsY0FBU2dDLEtBQVQsRUFBZ0JDLEVBQWhCLEVBQW9CO0FBQzNCaEMsTUFBRStCLE1BQU1FLE1BQVIsRUFBZ0IzQixJQUFoQixDQUFxQixZQUFXO0FBQy9CLFNBQUlDLFFBQVFQLEVBQUUsSUFBRixDQUFaO0FBQUEsU0FDQ1EsZ0JBQWdCRCxNQUFNRSxJQUFOLENBQVcsTUFBTU4sUUFBUU8sYUFBekIsQ0FEakI7O0FBR0EsU0FBSUgsTUFBTUksUUFBTixHQUFpQkMsTUFBakIsR0FBMEIsQ0FBOUIsRUFBaUM7QUFDaENKLG9CQUFjSyxJQUFkO0FBQ0EsTUFGRCxNQUVPO0FBQ05MLG9CQUFjTSxJQUFkO0FBQ0EsVUFBSUMsWUFBWVAsY0FBY1EsS0FBZCxFQUFoQjtBQUNBUixvQkFBY1MsTUFBZDtBQUNBVixZQUFNMkIsTUFBTixDQUFhbkIsU0FBYjtBQUNBO0FBQ0QsS0FaRDtBQWNBLElBbkJPO0FBb0JSLFdBQVEsY0FBU2dCLEtBQVQsRUFBZ0JDLEVBQWhCLEVBQW9CO0FBQzNCM0I7QUFDQTtBQUNBTixVQUFNb0MsT0FBTixDQUFjLGlCQUFkLEVBQWlDLEVBQWpDO0FBQ0E7QUF4Qk8sR0FGVjs7QUE2QkE7QUFDQWxDLFNBQ0VtQyxRQURGLENBQ1dULFNBRFgsRUFFRVUsUUFGRixDQUVXUixNQUZYOztBQUlBeEI7O0FBRUFlO0FBQ0EsRUF2Q0Q7O0FBeUNBO0FBQ0EsUUFBT3pCLE1BQVA7QUFDQSxDQWhJRiIsImZpbGUiOiJ0YWJsZWRuZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gdGFibGVkbmQuanMgMjAxNS0wOS0xNyBnbVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTUgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgVGFibGUgRG5kIEV4dGVuc2lvblxuICpcbiAqIFNvcnRzIGxpbmVzIGluIGNvbm5lY3RlZCB0YWJsZXMuXG4gKlxuICogQG1vZHVsZSBBZG1pbi9FeHRlbnNpb25zL3RhYmxlZG5kXG4gKiBAaWdub3JlXG4gKi9cbmd4LmV4dGVuc2lvbnMubW9kdWxlKFxuXHQndGFibGVkbmQnLFxuXHRcblx0W1xuXHRcdGpzZS5zb3VyY2UgKyAnL3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5jc3MnLFxuXHRcdGpzZS5zb3VyY2UgKyAnL3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5qcydcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdHZhclxuXHRcdFx0LyoqXG5cdFx0XHQgKiBFeHRlbnNpb24gUmVmZXJlbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIFRhYmxlIEJvZHkgU2VsZWN0b3Jcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHQkdGJvZHkgPSBudWxsLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9ucyBmb3IgRXh0ZW5zaW9uXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdCdhZGRjbGFzcyc6ICdjbHNEbmQnLCAvLyBjbGFzc25hbWUgYWRkZWQgdG8gYm9keVxuXHRcdFx0XHQnZGlzYWJsZWRjbGFzcyc6ICdzb3J0LWRpc2FibGVkJywgLy8gY2xhc3NuYW1lIGFkZGVkIHRvIGJvZHlcblx0XHRcdFx0J2hhbmRsZSc6IGZhbHNlIC8vIGhhbmRsZXIgd2hpY2ggZW5hYmxlcyB0aGUgc29ydGFibGVcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgRXh0ZW5zaW9uIE9wdGlvbnNcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSU5JVElBTElaQVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBTZXR1cCBEdW1taWVzXG5cdFx0ICovXG5cdFx0dmFyIHNldHVwRHVtbWllcyA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0Ly8gT24gZHJhZyBzdG9wLCB1cGRhdGUgZHVtbXkgbGluZSB2aXNpYmlsaXR5XG5cdFx0XHQkdGJvZHkuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyICRzZWxmID0gJCh0aGlzKSxcblx0XHRcdFx0XHQkc29ydERpc2FibGVkID0gJHNlbGYuZmluZCgnLicgKyBvcHRpb25zLmRpc2FibGVkY2xhc3MpO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKCRzZWxmLmNoaWxkcmVuKCkubGVuZ3RoID4gMSkge1xuXHRcdFx0XHRcdCRzb3J0RGlzYWJsZWQuaGlkZSgpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdCRzb3J0RGlzYWJsZWQuc2hvdygpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHR2YXIgcm93SGlkZGVuID0gJHNvcnREaXNhYmxlZC5jbG9uZSgpO1xuXHRcdFx0XHQkc29ydERpc2FibGVkLnJlbW92ZSgpO1xuXHRcdFx0XHQkc2VsZi5wcmVwZW5kKHJvd0hpZGRlbik7XG5cdFx0XHRcdFxuXHRcdFx0fSk7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBJbml0aWFsaXplIG1ldGhvZCBvZiB0aGUgZXh0ZW5zaW9uLCBjYWxsZWQgYnkgdGhlIGVuZ2luZS5cblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdCR0Ym9keSA9ICR0aGlzLmZpbmQoJ3Rib2R5Jyk7XG5cdFx0XHR2YXIgc3RyVGltZXN0YW1wID0gcGFyc2VJbnQobmV3IERhdGUoKS5nZXRUaW1lKCkgKiBNYXRoLnJhbmRvbSgpLCAxMCksXG5cdFx0XHRcdHN0ckNsc0RuZCA9IG9wdGlvbnMuYWRkY2xhc3MgKyAnXycgKyBzdHJUaW1lc3RhbXAsXG5cdFx0XHRcdGNvbmZpZyA9IHtcblx0XHRcdFx0XHQnaGFuZGxlJzogb3B0aW9ucy5oYW5kbGUsXG5cdFx0XHRcdFx0J2Nvbm5lY3RXaXRoJzogJy4nICsgc3RyQ2xzRG5kLFxuXHRcdFx0XHRcdCdjb250YWlubWVudCc6ICR0aGlzLFxuXHRcdFx0XHRcdCdzb3J0JzogZnVuY3Rpb24oZXZlbnQsIHVpKSB7XG5cdFx0XHRcdFx0XHQkKGV2ZW50LnRhcmdldCkuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0dmFyICRzZWxmID0gJCh0aGlzKSxcblx0XHRcdFx0XHRcdFx0XHQkc29ydERpc2FibGVkID0gJHNlbGYuZmluZCgnLicgKyBvcHRpb25zLmRpc2FibGVkY2xhc3MpO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0aWYgKCRzZWxmLmNoaWxkcmVuKCkubGVuZ3RoID4gMikge1xuXHRcdFx0XHRcdFx0XHRcdCRzb3J0RGlzYWJsZWQuaGlkZSgpO1xuXHRcdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRcdCRzb3J0RGlzYWJsZWQuc2hvdygpO1xuXHRcdFx0XHRcdFx0XHRcdHZhciByb3dIaWRkZW4gPSAkc29ydERpc2FibGVkLmNsb25lKCk7XG5cdFx0XHRcdFx0XHRcdFx0JHNvcnREaXNhYmxlZC5yZW1vdmUoKTtcblx0XHRcdFx0XHRcdFx0XHQkc2VsZi5hcHBlbmQocm93SGlkZGVuKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdCdzdG9wJzogZnVuY3Rpb24oZXZlbnQsIHVpKSB7XG5cdFx0XHRcdFx0XHRzZXR1cER1bW1pZXMoKTtcblx0XHRcdFx0XHRcdC8vIFRyaWdnZXIgYW4gdXBkYXRlIGV2ZW50IG9uIHRhYmxlXG5cdFx0XHRcdFx0XHQkdGhpcy50cmlnZ2VyKCd0YWJsZWRuZC51cGRhdGUnLCBbXSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9O1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgYSBzcGVjaWFsIGNsYXNzIGFuZCBzdGFydCB0aGUgc29ydGFibGUgcGx1Z2luLlxuXHRcdFx0JHRib2R5XG5cdFx0XHRcdC5hZGRDbGFzcyhzdHJDbHNEbmQpXG5cdFx0XHRcdC5zb3J0YWJsZShjb25maWcpO1xuXHRcdFx0XG5cdFx0XHRzZXR1cER1bW1pZXMoKTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gbW9kdWxlIGVuZ2luZS5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
