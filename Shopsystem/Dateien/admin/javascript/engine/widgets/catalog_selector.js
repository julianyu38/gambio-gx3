/* --------------------------------------------------------------
 catalog_selector.js 2017-11-30
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## CatalogSelector Widget
 *
 * This Widgets generates a selection for products (categories at not implemented yet, but possibil).
 * The given HTML code will be cloned to and modified to create this selection.
 *
 *
 * ### Parent Container Options
 *
 * **Headline text | `data-catalog_selector-headline_text` | String | Required**
 *
 * Textphrase for the collapse headline.
 *
 * **Add button text | `data-catalog_selector-add_button_text` | String | Required**
 *
 * Textphrase for the add new dropdown button.
 *
 * **Form group selector | `data-catalog_selector-group_selector` | String | Optional**
 *
 * This selector is required to select the form group that must be contained in the given HTML.
 *
 * **Label selector | `data-catalog_selector-label_selector` | String | Optional**
 *
 * This selector is required to select the label of the form group.
 *
 * **Dropdown selector | `data-catalog_selector-dropdown_selector` | String | Optional**
 *
 * This selector is required to select dropdown of the form group.
 *
 * **Remove icon column selector | `data-catalog_selector-remove_selector` | String | Optional**
 *
 * This selector is required to select the column for the remove icon of the form group.
 *
 * **Selected data | `data-catalog_selector-selected_data` | String | Optional**
 *
 * Already selected data for the selection.
 *
 *
 * ### Example
 * ```html
 * <div data-gx-widget="catalog_selector"
 *     data-catalog_selector-group_selector=".form-group"
 *     data-catalog_selector-label_selector="label"
 *     data-catalog_selector-dropdown_selector=".catalog-selector-dropdown"
 *     data-catalog_selector-remove_selector=".catalog-selector-remove"
 *     data-catalog_selector-headline_text="HEADLINE"
 *     data-catalog_selector-add_button_text="Add dropdown"
 *     data-catalog_selector-selected_data="1,2,3,4"
 * >
 *     <div class="form-group">
 *         <label class="col-md-4">{$txt.LABEL_SHOW_FOR_PRODUCT}:</label>
 *         <div class="col-md-3">
 *             <select class="form-control catalog-selector-dropdown"
 *             name="content_manager[infopage][sitemap_changefreq][{$languageCode}]"></select>
 *         </div>
 *         <div class="col-md-1 catalog-selector-remove"></div>
 *     </div>
 * </div>
 * ```
 *
 *
 * @module Admin/Widgets/catalog_selector
 */
gx.widgets.module(
	'catalog_selector',
	
	[
		`${gx.source}/widgets/collapser`
	],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLE DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Widget Reference
			 *
			 * @type {object}
			 */
			$this = $(this),
			
			/**
			 * Copy of the given form group
			 *
			 * @type {object}
			 */
			$groupObject = {},
			
			/**
			 * Data that's used to fill the dropdown.
			 * Will be loaded by ajax from the widget ajax controller
			 *
			 * @type string
			 */
			dataTreeHtml = {},
			
			/**
			 * Default Options for Widget
			 *
			 * @type {object}
			 */
			defaults = {
				'group_selector': '.form-group',
				'label_selector': 'label',
				'dropdown_selector': '.catalog-selector-dropdown',
				'remove_selector': '.catalog-selector-remove',
				'selected_data': ''
			},
			
			/**
			 * Final Widget Options
			 *
			 * @type {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
				
		// ------------------------------------------------------------------------
		// INITIALIZE
		// ------------------------------------------------------------------------
		
		/**
		 * Initialize method of the widget, called by the engine.
		 */
		module.init = function(done) {
			// Perform ajax request to collect products and categories
			_performRequest('getProductsTreeAsOptgroups')
				.done(function(response) {
					dataTreeHtml = JSON.parse(response)['html'];
				});
			
			// Initialize widget html
			$this
				.append(`
					<fieldset>
	                    <div class="frame-wrapper default">
							<div class="frame-head"
								data-gx-widget="collapser"
								data-collapser-target_selector=".frame-content">
								` + options.headline_text + `
							</div>
							<div class="frame-content">
								<div class="catalog-selection-data"></div>
							</div>
						</div>
					</fieldset>
			`);
			$this
				.find('.catalog-selection-data')
				.parent()
				.append(`
					<button type="button" class="btn catalog-selection-new-dropdown">
						`+options.add_button_text+`
					</button>
				`)
				.find('.catalog-selection-new-dropdown')
				.on('click', _onNewDropdown);
			gx.widgets.init($this.parent());
			
			// Backup form group
			$groupObject = $this.find(options.group_selector);
			$this.find(options.group_selector).remove();
			
			// Fill dropdown menu of backuped form group with options
			$groupObject
				.find(options.dropdown_selector)
				.append(dataTreeHtml);
			
			// Delete given html (It is will not be used anymore)
			$groupObject.clone().appendTo($this.find('.catalog-selection-data'));
			
			// Add pre-selected data
			_addSelectedData();
			
			done();
		};
		
		
		// ------------------------------------------------------------------------
		// HELPERS
		// ------------------------------------------------------------------------
		
		/**
		 * Performs the ajax request to collec tthe products and categories
		 *
		 * @param {String} action
		 * @returns {String} JSON
		 */
		function _performRequest(action) {
			const URL_BASE = 'admin.php?do=CatalogSelectWidgetAjax/';
			
			// AJAX request options.
			const ajaxOptions = {
				url: URL_BASE + action,
				async: false,
			};
			
			// Returns deferred object.
			return $.ajax(ajaxOptions);
		}
		
		/**
		 * Create a new form group with another dropdown
		 */
		function _onNewDropdown() {
			$groupObject
				.clone()
				.appendTo($this.find('.catalog-selection-data'))
				.find(options.label_selector)
				.empty()
				.parent()
				.find(options.remove_selector)
				.append('<i class="fa fa-trash-o" aria-hidden="true"></i>')
				.find('i')
				.on('click', _removeDropdown);
		}
		
		/**
		 * Removes a form group. Will be initialize by a click event on the remove icon.
		 */
		function _removeDropdown() {
			$(this).closest(options.group_selector).remove();
		}
		
		/**
		 * Adds the pre selected data.
		 */
		function _addSelectedData() {
			if(options.selected_data == ''){
				return;
			}
			
			if(String(options.selected_data).indexOf(",") == -1){
				$this.find(options.dropdown_selector+':last').val(options.selected_data);
				_onNewDropdown();
			}else{
				var selectedData = options.selected_data.split(',');
				console.log(selectedData);
				for(var i in selectedData)
				{
					$this.find(options.dropdown_selector+':last').val(selectedData[i]);
					_onNewDropdown();
				}
			}
		}
		
		// Return data to module engine
		return module;
	});
