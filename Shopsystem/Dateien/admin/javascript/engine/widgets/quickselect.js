/* --------------------------------------------------------------
 quickselect.js 2017-09-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## QuickSelect Widget
 *
 * This widget is a custom implementation of tabs functionality.
 *
 * The actual `<div>` which contains the tabs, has to have a CSS-Class named **quickselect-headline-wrapper**.
 * The tabs will be identified by this CSS-Class. The content of the tabs has to be in a `<div>` which has to have
 * a CSS-Class called **quickselect-content-wrapper**. The elements inside, have to be in the same order as the tabs.
 *
 * ### Widget Instance Options
 *
 * **Tab alignment | `data-quickselect-align` | string | Optional**
 *
 * This option can be used to align the tabs (not the content). In some cases it is necessary to align th tabs itself
 * to righ right.
 *
 * **Start tab | `data-quickselect-start` | integer | Optional**
 *
 * This option can be used to choose a starting tab. Mind that 0 is associated with the first tab content.
 *
 * ### Example
 *
 * ```html
 * <div data-gx-widget="quickselect" data-quickselect-align="left" data-quickselect-start="1">
 *     <!-- Tabs -->
 *     <div class="quickselect-headline-wrapper">
 *         <a href="#tab1">Tab #1</a>
 *         <a href="#tab2">Tab #2</a>
 *         <a href="#tab3">Tab #3</a>
 *     </div>
 *
 *     <!-- Content -->
 *     <div class="quickselect-content-wrapper">
 *         <div>Content of tab #1.</div>
 *         <div>Content of tab #2.</div>
 *         <div>Content of tab #3.</div>
 *     </div>
 * </div>
 * ```
 *
 * @module Admin/Widgets/quickselect
 */
gx.widgets.module(
	'quickselect',
	
	[],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLE DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Widget Reference
			 *
			 * @type {object}
			 */
			$this = $(this),
			
			/**
			 * Headline Tags Selector
			 *
			 * @type {object}
			 */
			$headlineTags = null,
			
			/**
			 * Content Tags Selector
			 *
			 * @type {object}
			 */
			$contentTags = null,
			
			/**
			 * Default Options for Widget
			 *
			 * @type {object}
			 */
			defaults = {},
			
			/**
			 * Final Widget Options
			 *
			 * @type {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// EVENT HANDLERS
		// ------------------------------------------------------------------------
		
		/**
		 * Click handler for the tabs onClick the content gets switched.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		var _clickHandler = function(event) {
			event.preventDefault();
			event.stopPropagation();
			
			$headlineTags.removeClass('active');
			
			var index = $(this)
				.addClass('active')
				.index();
			
			$contentTags
				.hide()
				.eq(index)
				.show();
			
			$this.trigger('shown:tab', {index});
		};
		
		/**
		 * Handles external "show" event
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 * @param {number} tab index to show
		 */
		var _showHandler = function(event, index) {
			event.preventDefault();
			event.stopPropagation();
			$headlineTags.eq(index).trigger('click');
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZE
		// ------------------------------------------------------------------------
		
		/**
		 * Initialize method of the widget, called by the engine.
		 */
		module.init = function(done) {
			$headlineTags = $this
				.children('.quickselect-headline-wrapper')
				.children('a');
			
			$contentTags = $this
				.children('.quickselect-content-wrapper')
				.children('div');
			
			$this.addClass('ui-tabs');
			$this.on('click', '.quickselect-headline-wrapper > a', _clickHandler);
			$this.on('show:tab', _showHandler);
			
			if (options.align == 'right') {
				$this.children('.quickselect-headline-wrapper').css("float", "right");
			}
			
			if (Number.isInteger(options.start)) {
				$headlineTags
					.eq(options.start)
					.trigger('click');
			}
			else {
				$headlineTags
					.eq(0)
					.trigger('click');
			}
			
			done();
		};
		
		// Return data to module engine
		return module;
	});
