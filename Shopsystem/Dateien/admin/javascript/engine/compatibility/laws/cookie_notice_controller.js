/* --------------------------------------------------------------
 cookies_notice_controller.js 2016-09-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Cookie Notice Controller
 *
 * Compatibility module that handles the "Cookie Notice" page under the "Rights" menu of "Shop Settings" section.
 * The data of the form are updated upon change and this module will just post them to LawsController. Check out
 * the fields that are language dependent, they will be changed when the user selects a language from the language
 * switcher component.
 *
 * @module Compatibility/cookie_notice_controller
 */
gx.compatibility.module(
	'cookie_notice_controller',
	
	['loading_spinner', `${gx.source}/libs/editor_values`, `${gx.source}/libs/info_messages`],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		var
			/**
			 * Module Selector
			 *
			 * @type {object}
			 */
			$this = $(this),
			
			/**
			 * Module Instance
			 *
			 * @type {object}
			 */
			module = {
				model: {
					formData: jse.core.config.get('appUrl') +
					'/admin/admin.php?do=Laws/GetCookiePreferences&pageToken=' + jse.core.config.get('pageToken')
				}
			};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * On Form Submit Event
		 *
		 * @param {object} event jQuery Event Object
		 */
		var _onFormSubmit = function(event) {
			event.preventDefault();
			
			// Prepare form data and send them to the LawsController class. 
			var postUrl = jse.core.config.get('appUrl') + '/admin/admin.php?do=Laws/SaveCookiePreferences',
				postData = $.extend({pageToken: jse.core.config.get('pageToken')}, module.model.formData),
				$spinner;
			
			$.ajax({
				url: postUrl,
				type: 'POST',
				data: postData,
				dataType: 'json',
				beforeSend: function() {
					$spinner = jse.libs.loading_spinner.show($this, '4');
				}
			})
				.done(function() { // Display success message.
					jse.libs.info_messages.addSuccess(jse.core.lang.translate('TXT_SAVE_SUCCESS', 'admin_general'));
				})
				.fail(function(jqxhr, textStatus, errorThrown) { // Display failure message.
					jse.libs.info_messages.addError(jse.core.lang.translate('TXT_SAVE_ERROR', 'admin_general'));
					jse.core.debug.error('Could not save Cookie Notice preferences.', jqxhr, textStatus, errorThrown);
				})
				.always(function() {
					jse.libs.loading_spinner.hide($spinner);
					
					// Scroll to the top, so that the user sees the appropriate message.
					$('html, body').animate({scrollTop: 0});
				});
		};
		
		/**
		 * On Language Flag Click Event
		 *
		 * @param {object} event jQuery event object.
		 */
		var _onLanguageClick = function(event) {
			event.preventDefault();
			
			$(this).siblings().removeClass('active');
			$(this).addClass('active');
			
			// Load the language specific fields.
			$.each(module.model.formData, function(name, value) {
				var $element = $this.find('[name="' + name + '"]');
				
				if ($element.data('multilingual') !== undefined) {
					var selectedLanguageCode = $('.languages a.active').data('code');
					$element.val(value[selectedLanguageCode]);
					if ($element.is('textarea') && $element.parents('.editor-wrapper').length) {
						jse.libs.editor_values.setValue($element, value[selectedLanguageCode]);
					}
				} else {
					$element.val(value);
					
					if ($element.is(':checkbox') && value === 'true') {
						$element.parent().addClass('checked');
						$element.prop('checked', true);
					}
					
					if (name === 'position' && !value) {
						$element.find('option[value="top"]').prop('selected', true).trigger('change');
					}
				}
			});
		};
		
		/**
		 * On Input Element Change Event
		 */
		var _onInputChange = function() {
			var $element = $(this);
			
			if ($element.data('multilingual') !== undefined) {
				var selectedLanguageCode = $('.languages a.active').data('code');
				module.model.formData[$element.attr('name')][selectedLanguageCode] = $element.val();
			} else {
				module.model.formData[$element.attr('name')] = $element.val();
			}
		};
		
		/**
		 * On Switcher Widget Click Event
		 */
		var _onSwitcherClick = function() {
			module.model.formData[$(this).find('input:checkbox').attr('name')] = $(this).hasClass('checked');
		};
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = function(done) {
			// Bind form event handlers. 
			$this
				.on('submit', _onFormSubmit)
				.on('click', '.languages a', _onLanguageClick)
				.on('click', '.switcher', _onSwitcherClick);
			
			$this
				.find('input:hidden, input:text, select, textarea')
				.on('change', _onInputChange);
				
			// Select active language.
			$('.languages').find('.active').trigger('click');
			
			// Set the color-preview colors.
			$this.find('.color-preview').each(function() {
				$(this).css('background-color', $(this).siblings('input:hidden').val());
			});
			
			done();
		};
		
		return module;
	});