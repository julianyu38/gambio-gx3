/* --------------------------------------------------------------
 order_details_controller.js 2018-05-02
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Order Details Controller
 *
 * This controller will handle the compatibility order details wrapper.
 *
 * @module Compatibility/order_details_controller
 */
gx.compatibility.module(
	'order_details_controller',
	
	[
		'xhr',
		'loading_spinner',
		'modal',
		gx.source + '/libs/action_mapper',
		gx.source + '/libs/button_dropdown'
	],
	
	/**  @lends module:Compatibility/order_details_controller */
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		let
			/**
			 * Module Selector
			 *
			 * @var {object}
			 */
			$this = $(this),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {},
			
			/**
			 * Final Options
			 *
			 * @var {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Create Invoice Click Event
		 *
		 * This callback will make sure that the invoice is created and then make a page reload.
		 *
		 * @param {jQuery.Event} event
		 */
		var _onCreateInvoiceClick = function(event) {
			event.preventDefault();
			
			var $pageToken = $('input[name="page_token"]');
			
			var $modal = $('#orders_create_invoice_modal');
			
			var $frameContent = $this
				.find('.frame-wrapper.invoices')
				.find('.frame-content');
			
			var $loadingSpinner = jse.libs.loading_spinner.show($frameContent);
			
			var requestOptions = {
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=OrdersModalsAjax/GetInvoiceCount',
				data: {
					orderId: options.order_id,
					pageToken: ($pageToken.length) ? $pageToken.val() : ''
				}
			};
			
			var createInvoiceUrl = event.target.getAttribute('href') + '&ajax=1';
			
			function createInvoice() {
				var downloadPdfWindow = window.open('about:blank');
				
				function onRequestSuccess(response) {
					var queryString = $.param({
                        module: 'OrderAdmin',
                        action: 'showPdf',
                        type: 'invoice',
                        invoice_id: response.invoiceId
                    });
					
					downloadPdfWindow.location = jse.core.config.get('appUrl') + '/admin/request_port.php?' + queryString;
					
					location.reload();
				}
				
				function onRequestFailure() {
					downloadPdfWindow.close();
				}
				
				jse.libs.xhr.get({ url: createInvoiceUrl })
					.done(onRequestSuccess)
					.fail(onRequestFailure);
			}
			
			function onRequestSuccess(response) {
				function onAbort() {
					$(this).dialog('close');
				}
				
				jse.libs.loading_spinner.hide($loadingSpinner);
				
				if (!response.count) {
					createInvoice();
					return;
				}
				
				$modal.dialog({
					title: jse.core.lang.translate('TITLE_CREATE_INVOICE', 'orders'),
					modal: true,
					width: 420,
					dialogClass: 'gx-container',
					buttons: [
						{
							text: jse.core.lang.translate('yes', 'buttons'),
							class: 'btn',
							click: createInvoice
						},
						{
							text: jse.core.lang.translate('no', 'buttons'),
							class: 'btn',
							click: onAbort
						}
					]
                });
			}
			
			function onRequestFailure() {
				jse.libs.loading_spinner.hide($loadingSpinner);
				createInvoice();
			}
			
			jse.libs.xhr.get(requestOptions)
				.done(onRequestSuccess)
				.fail(onRequestFailure);
		};
		
		/**
		 * Create Packing Slip Click Event
		 *
		 * This callback will make sure that the packing slip is created and then make a page reload.
		 *
		 * @param {jQuery.Event} event
		 */
		var _onCreatePackingSlipClick = function(event) {
			event.preventDefault();
			
			var downloadPdfWindow = window.open('about:blank');
			var url = event.target.getAttribute('href') + '&ajax=1';
			
			$.getJSON(url)
				.done(function(response) {
					downloadPdfWindow.location = jse.core.config.get('appUrl') + '/admin/request_port.php?' + $.param({
							module: 'OrderAdmin',
							action: 'showPdf',
							type: 'packingslip',
							file: response.filename + '__' + response.filenameSuffix
						});
					location.reload();
				})
				.fail(function(jqXHR, textStatus, errorThrown) {
					downloadPdfWindow.close();
				});
		};
		
		/**
		 * Open order status modal on click on elements with class add-order-status
		 *
		 * @param {jQuery.Event} event
		 */
		var _onAddOrderStatusClick = function(event) {
			event.preventDefault();
			$('.update-order-status').trigger('click');
		};
		
		const _moveSaveActionsToBottom = () => {
			const $mainActions = $('div.text-right:nth-child(2)');
			const $bottomBar = $('.footer-info');
			const $newContainer = $('<div class="pull-right info"></div>');
			
			$mainActions.appendTo($newContainer);
			$newContainer.appendTo($bottomBar);
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$('.message_stack_container').addClass('breakpoint-large');
			
			_moveSaveActionsToBottom();
			
			$this
				.on('click', '.add-order-status', _onAddOrderStatusClick)
				.on('click', '.create-invoice', _onCreateInvoiceClick)
				.on('click', '.create-packing-slip', _onCreatePackingSlipClick);
			
			done();
		};
		
		return module;
	});
