/* --------------------------------------------------------------
 order_email_invoice.js 2016-12-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * This module handels the click event of the invoice email button
 *
 * @module Compatibility/order_email_invoice
 */
gx.compatibility.module(
	'order_email_invoice',
	
	['modal'],
	
	/**  @lends module:Compatibility/order_email_invoice */
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @var {object}
		 */
		const $this = $(this);
		
		/**
		 * Default Options
		 *
		 * @type {object}
		 */
		const defaults = {};
		
		/**
		 * Final Options
		 *
		 * @var {object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module Object
		 *
		 * @type {object}
		 */
		const module = {};
		
		/**
		 * On Email Invoice Click
		 *
		 * Display the email-invoice modal.
		 */
		function _onEmailInvoiceClick() {
			const $modal = $('.email-invoice.modal');
			const url = jse.core.config.get('appUrl') + '/admin/admin.php';
			const data = {
				id: options.id,
				do: 'OrdersModalsAjax/GetEmailInvoiceSubject',
				pageToken: jse.core.config.get('pageToken')
			};
			let invoiceNumbersHtml = '';
			
			$modal.find('.customer-info').text(`"${options.name}"`);
			$modal.find('.email-address').val(options.email);
			
			$modal
				.data('orderId', options.id)
				.modal('show');
			
			$.ajax({url, data, dataType: 'json'}).done((response) => {
				$modal.attr('data-gx-widget', 'single_checkbox');
				
				$modal.find('.subject').val(response.subject);
				if (response.invoiceIdExists) {
					$modal.find('.invoice-num-info').addClass('hidden');
					$modal.find('.no-invoice').removeClass('hidden');
				} else {
					$modal.find('.invoice-num-info').removeClass('hidden');
					$modal.find('.no-invoice').addClass('hidden');
				}
				
				if (Object.keys(response.invoiceNumbers).length <= 1) {
					$modal.find('.invoice-numbers').addClass('hidden');
				} else {
					$modal.find('.invoice-numbers').removeClass('hidden');
				}
				
				for (let invoiceId in response.invoiceNumbers) {
					invoiceNumbersHtml +=
						'<p><input type="checkbox" name="invoice_ids[]" value="' + invoiceId
						+ '" checked="checked" class="invoice-numbers-checkbox" /> '
						+ response.invoiceNumbers[invoiceId] + '</p>';
				}
				
				$modal.find('.invoice-numbers-checkboxes').html(invoiceNumbersHtml);
				
				$modal.find('.invoice-numbers-checkbox').on('change', _onChangeEmailInvoiceCheckbox);
				
				gx.widgets.init($modal);
			});
		}
		
		/**
		 * On Email Invoice Checkbox Change
		 *
		 * Disable send button if all invoice number checkboxes are unchecked. Otherwise enable the send button again.
		 */
		function _onChangeEmailInvoiceCheckbox() {
			const $modal = $('.email-invoice.modal');
			
			if ($modal.find('.invoice-numbers-checkbox').length > 0) {
				if ($modal.find('.invoice-numbers-checkbox:checked').length > 0) {
					$modal.find('.send').prop('disabled', false);
				} else {
					$modal.find('.send').prop('disabled', true);
				}
			} else {
				$modal.find('.send').prop('disabled', false);
			}
		}
		
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$this.on('click', _onEmailInvoiceClick);
			
			done();
		};
		
		return module;
	});
