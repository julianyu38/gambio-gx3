/* --------------------------------------------------------------
 order_customizer.js 2017-05-24 gm
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Orders Modal Layer Module
 *
 * This module will open a modal layer for order actions like deleting or changing the oder status.
 *
 * @module Compatibility/order_customizer
 */
gx.compatibility.module(
	'order_customizer',
	
	[],
	
	/**  @lends module:Compatibility/order_customizer */
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Module Selector
			 *
			 * @var {object}
			 */
			$this = $(this),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {},
			
			/**
			 * Final Options
			 *
			 * @var {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------
		
		var _openCustomizerSet = function(event) {
			
			var $customizer = $(options.selector);
			
			$('select[id^="element_"]').each(function(j) {
				var attr_id = $(this).attr('id');
				var attr_class = $(this).attr('class');
				var attr_name = $(this).attr('name');
				var attr_style = $(this).attr('style');
				var attr_value = $(this).children('option').val();
				
				$(this).replaceWith('<input type="text" name="' + attr_name + '" id="' + attr_id +
					'" class="' + attr_class + '" style="' + attr_style + '" value="' + attr_value + '">');
			});
			
			event.preventDefault();
			event.stopPropagation();
			$customizer.dialog({
				'title': jse.core.lang.translate('HEADING_GX_CUSTOMIZER', 'orders'),
				'modal': true,
				'dialogClass': 'gx-container',
				'buttons': [
					{
						'text': jse.core.lang.translate('save', 'buttons'),
						'class': 'btn',
						'click': function click() {
							var content_div = $customizer.children('[id^="gm_gprint_content"]');
							var error = false;
							content_div.children('[id^="surface_"]').each(function(i) {
								var surface_id = $(this).attr('id');
								var inputs = {};
								var container_div = $(this).children('[id^="element_container_"]');
								
								container_div.children('input[id^="element_"]').each(function(j) {
									var input_id = $(this).attr('id');
									var input = {};
									input.id = input_id.substring(input_id.lastIndexOf('_') + 1);
									input.value = $(this).val();
									inputs[input_id] = input;
								});
								container_div.children('textarea[id^="element_"]').each(function(j) {
									var input_id = $(this).attr('id');
									var input = {};
									input.id = input_id.substring(input_id.lastIndexOf('_') + 1);
									input.value = $(this).val();
									inputs[input_id] = input;
								});
								
								if (Object.keys(inputs).length > 0) {
									$.ajax({
										type: 'POST',
										url: 'request_port.php?module=GPrintOrder&action=save_surfaces_group_inputs',
										dataType: 'json',
										context: this,
										async: false,
										data: {
											"inputs": inputs,
											"surface_id": surface_id.substring(surface_id.lastIndexOf('_') + 1),
										},
										success: function() {
											console.log('gut');
										},
										error: function() {
											console.log('error');
											error = true;
										}
									});
								}
							});
							
							if (error) {
								alert(jse.core.lang.translate('GM_GPRINT_SAVE_FAILED', 'admin_gm_gprint'));
							}
							else {
								alert(jse.core.lang.translate('GM_GPRINT_SAVE_SUCCESSFULL', 'admin_gm_gprint'));
							}
						}
					}, {
						'text': jse.core.lang.translate('close', 'buttons'),
						'class': 'btn',
						'click': function() {
							$(this).dialog('close');
						}
					}
				],
				'width': 420,
				open() {
                    this.style.overflow = '';
				}
			});
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$this.on('click', _openCustomizerSet);
			done();
		};
		
		return module;
	});
