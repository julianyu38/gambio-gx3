/* --------------------------------------------------------------
 order_invoice.js 2015-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2015 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## PayPal3 Payment Details on Order Page
 *
 * This module add the paypal3 payment informationen to the order details page.
 *
 * @module Compatibility/order_invoice
 */
gx.compatibility.module(
	'order_invoice',
	
	['modal'],
	
	/**  @lends module:Compatibility/order_invoice */
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Module Selector
			 *
			 * @var {object}
			 */
			$this = $(this),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {},
			
			/**
			 * Final Options
			 *
			 * @var {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		var _cancelInvoiceClickHandler = function() {
			var confirm = false;
			var $link = $(this);
			
			jse.libs.modal.message({
				minWidth: 400,
				title: jse.core.lang.translate('HEADING_MODAL_CANCELLATION_INVOICE', 'orders'),
				content: jse.core.lang.translate('TEXT_MODAL_CANCELLATION_INVOICE', 'orders').replace('%s', $link.data('invoice-number')),
				buttons: [
					{
						text: jse.core.lang.translate('no', 'lightbox_buttons'),
						click: function() {
							$(this).dialog('close');
						}
					},
					{
						text: jse.core.lang.translate('yes', 'lightbox_buttons'),
						click: function() {
							$(this).dialog('close');
							window.open($link.attr('href'), '_blank');
						}
					}
				]
			});
			
			return false;
		};
			
			
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$this.append($('table.invoice'));
			$this.find('.cancel-invoice').on('click', _cancelInvoiceClickHandler);
			
			done();
		};
		
		return module;
	});
