/* --------------------------------------------------------------
 bottom_save_bar.js 2017-09-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Bottom Save Bar Module
 *
 * This module will move all content, that is contained in any element that has the class "bottom-save-bar-content" to
 * the Bottom Save Bar.
 *
 * @module Compatibility/bottom_save_bar
 */
gx.compatibility.module(
	// Module name
	'bottom_save_bar',
	
	// Module dependencies
	[],
	
	/**  @lends module:Compatibility/bottom_save_bar */
	
	function() {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		const module = {},
			$target = $('.bottom-save-bar');
		
		// ------------------------------------------------------------------------
		// EVENT HANDLER
		// ------------------------------------------------------------------------
		
		const _initialize = function() {
			$('.bottom-save-bar-content > *:not([type="hidden"]):not(.btn-primary)').each(function(index, element) {
				const $element = $(element).css('float', 'none');
				_handleContentType($element);
			});
			
			$('.bottom-save-bar-content > *.btn-primary:not([type="hidden"])').each(function(index, element) {
				const $element = $(element).css('float', 'none');
				_handleContentType($element);
			});
		};
		
		const _handleContentType = function($element) {
			if ($element.is('input:not([type="button"])') || $element.is('button[type="submit"]')) {
				_handleFormElement($element);
			}
			else {
				_handleOtherContent($element);
			}
		};
		
		const _handleFormElement = function($element) {
			let $clone = $element.clone();
			$element.hide();
			
			if ($element.is('[type="submit"]')) {
				$clone.attr('type', 'button');
			}
			
			$clone.on('click', function() {
				$element.trigger('click');
			});
			
			$target.append($clone);
		};
		
		const _handleOtherContent = function($element) {
			$target.append($element);
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			_initialize();
			done();
		};
		
		return module;
	});
