/* --------------------------------------------------------------
 switch_datepicker_field.js 2016-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.compatibility.module(
	'switch_datepicker_field',
	
	[],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Module Selector
			 *
			 * @var {object}
			 */
			$this = $(this),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {
				current_select: '#GM_PDF_INVOICE_USE_CURRENT_DATE',
				notice: '.manual-invoice-date-notice'
			},
			
			/**
			 * Final Options
			 *
			 * @var {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------
		
		var _checkVisibility = function() {
			if ($(options.current_select).val() == 1) {
				$this.attr('disabled', 'disabled');
				$(options.notice).hide();
			} else {
				$this.removeAttr('disabled');
				$(options.notice).show();
			}
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			
			_checkVisibility();
			
			$(options.current_select).on('change', function() {
				_checkVisibility();
			});
			
			done();
		};
		
		return module;
	});