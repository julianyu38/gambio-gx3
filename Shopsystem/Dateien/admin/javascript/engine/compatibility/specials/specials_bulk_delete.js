/* --------------------------------------------------------------
 specials_bulk_delete.js 2018-04-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Specials Bulk Delete Module
 *
 * This module handels the bulk deletion for the specials.
 *
 * @module Compatibility/specials_bulk_delete
 */
gx.compatibility.module(
	'specials_bulk_delete',
	
	[],
	
	/**  @lends module:Compatibility/specials_bulk_delete */
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		/**
		 * Module Reference
		 *
		 * @var {object}
		 */
		const $this = $(this);
		
		/**
		 * Bulk delete modal object
		 *
		 * @type {object}
		 */
		const $modal = $('#modal_layer_container');
		
		/**
		 * Bulk delete form object
		 *
		 * @type {object}
		 */
		const $form = $('#bulk_delete_confirm_form');
		
		/**
		 * Default Options
		 *
		 * @type {object}
		 */
		const defaults = {};
		
		/**
		 * Final Options
		 *
		 * @var {object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module Object
		 *
		 * @type {object}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------
		
		
		/**
		 * Handler for the click event, that's triggered by the delete button.
		 */
		const _handleDeleteButton = (event) => {
			event.preventDefault();
			
			$form.find('.products-to-delete').html('');
			$.each($this.find('.delete-special:checked'), _addSpecialsToModal);
			
			$form.dialog({
				'title': jse.core.lang.translate('TEXT_INFO_HEADING_BULK_DELETE_SPECIALS', 'admin_specials'),
				'modal': true,
				'dialogClass': 'gx-container',
				'buttons': _getModalButtons($form),
				'width': 420
			});
		};
		
		/**
		 * Returns the modal buttons, that are used in the delete confirmation modal.
		 *
		 * @return {object} Buttons fpr the delete confirmation modal
		 */
		const _getModalButtons = ($form) => {
			return [
				{
					'text': jse.core.lang.translate('close', 'buttons'),
					'class': 'btn',
					'click': function() {
						$(this).dialog('close');
					}
				},
				{
					'text': jse.core.lang.translate('delete', 'buttons'),
					'class': 'btn btn-primary',
					'click': function() {
						$form.submit();
					}
				}
			];
		};
		
		/**
		 * Adds the informations of the selected specials to the delete confirmation modal.
		 */
		const _addSpecialsToModal = (key, checkbox) => {
			$form.find('.products-to-delete').append($('<li/>', {
				'text': $(checkbox).data('product-name')
			}));
			
			$form.find('.products-to-delete').append($('<input/>', {
				'name': 'deleteSpecial[]',
				'value': $(checkbox).data('special-id')
			}).hide());
		};
		
		/**
		 * Handler for the click event, that's triggered by a normal checkbox in the specials overview table.
		 */
		const _handleNormalCheckbox = () => {
			const numChecked = $this.find('.delete-special:checked').length;
			const numNotChecked = $this.find('.delete-special').length;
			
			if (numChecked === numNotChecked && numChecked > 0) {
				$this.find('.select-all-checkbox').prop('checked', true);
				$this.find('.select-all-checkbox').closest('.single-checkbox').addClass('checked');
			} else {
				$this.find('.select-all-checkbox').prop('checked', false);
				$this.find('.select-all-checkbox').closest('.single-checkbox').removeClass('checked');
			}
			
			if (numChecked === 0) {
				$this.find('.bulk-delete').prop('disabled', true);
			} else {
				$this.find('.bulk-delete').prop('disabled', false);
			}
		};
		
		/**
		 * Handler for the click event, that's triggered by the checkbox in the head of the specials overview table.
		 */
		const _handleSelectAllCheckbox = () => {
			const allChecked = $this.find('.select-all-checkbox').is(":checked");
			
			if (allChecked) {
				$this.find('.delete-special').prop('checked', true);
				$this.find('.delete-special').parent('.single-checkbox').addClass('checked');
			} else {
				$this.find('.delete-special').prop('checked', false);
				$this.find('.delete-special').parent('.single-checkbox').removeClass('checked');
			}
			
			if ($this.find('.select-all-checkbox').is(":checked") && $this.find('.delete-special:checked').length > 0) {
				$this.find('.bulk-delete').prop('disabled', false);
			} else {
				$this.find('.bulk-delete').prop('disabled', true);
			}
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = (done) => {
			$this.find('.select-all-checkbox').on('click', _handleSelectAllCheckbox);
			$this.find('.delete-special').on('click', _handleNormalCheckbox);
			$this.find('.bulk-delete').on('click', _handleDeleteButton);
			
			done();
		};
		
		return module;
	}
);