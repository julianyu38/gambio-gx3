/* --------------------------------------------------------------
 image_change.js 2018-04-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Products Image Change Module
 *
 * This module is responsible for effects on image changes.
 *
 * @module Compatibility/image_change
 */
gx.compatibility.module(
	// Module name
	'image_change',
	
	// Module dependencies
	[
		'image_resizer', 'xhr', 'modal',
		`${jse.source}/vendor/jquery-ui/jquery-ui.min.css`,
		`${jse.source}/vendor/jquery-ui/jquery-ui.js`
	],
	
	/** @lends module:Compatibility/image_change */
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		// Shortcut to module element.
		var $this = $(this);
		
		// Shortcut to image resizer libary.
		var resize = jse.libs.image_resizer.resize;
		
		// AJAX request library
		var xhr = jse.libs.xhr;
		
		// Modal library
		var modal = jse.libs.modal;
		
		// Elements selector object.
		var selectors = {
			input: 'input:file',
			form: 'form',
			previewImage: '[data-image]',
			filenameLabel: '[data-filename-label]',
			filenameInput: '[data-filename-input]',
			showImage: '[data-show-image]',
			fileInputName: '[data-file-input-name]',
			originalImageName: '[data-original-image]'
		};
		
		// Module object.
		var module = {};
		
		// ------------------------------------------------------------------------
		// PRIVATE METHODS
		// ------------------------------------------------------------------------
		
		/**
		 * Handles file changes in input field.
		 * @param  {jQuery.Event} event Event fired
		 */
		var _onNewImage = function(event) {
			// Preview image.
			var $previewImage = $this.find(selectors.previewImage);
			
			// File put.
			var file = event.target.files[0];
			var fileName = file.name;
			
			if ((/\.(jpg|jpeg|png|gif|bmp)$/i).test(fileName) === false) {
				modal.message({
					title: jse.core.lang.translate('GM_UPLOAD_IMAGE_MODAL_ERROR', 'gm_product_images'),
					content: jse.core.lang.translate('GM_UPLOAD_IMAGE_MODAL_INVALID_FILE_FORMAT', 'gm_product_images')
				});
				
				return;
			}
			
			// Replace some specialchars. Still allowed are these chars: . - _
			fileName = fileName.replace(/[\s!$§%#^&*()+|~=`´{}\[\]:";\'<>?,\\\/]+/g, '-');
			
			// Make sure that the filename is unique.
			var length = $('input[name="image_file[' + fileName + ']"]').length,
				counter = 1;
			while (length !== 0) {
				var newFileName = fileName.replace(/(\.)/, String(counter) + '.');
				
				length = $('input[name="image_file[' + newFileName + ']"]').length;
				
				if (length === 0) {
					fileName = newFileName;
				}
				
				counter++;
			}
			
			xhr.get({url: 'admin.php?do=MaxFileSize'})
				.done(function(result) {
					var maxFileSizeAllowed = result.maxFileSize;
					var actualFileSize = file.size / Math.pow(1024, 2);
					
					if (actualFileSize > maxFileSizeAllowed) {
						var message = jse.core.lang.translate('TXT_FILE_TOO_LARGE', 'categories');
						alert(message + maxFileSizeAllowed + ' MB');
						return;
					}
					// Create a FileReader to read the input file.
					var Reader = new FileReader();
					
					// As soon as the image file has been loaded,
					// the loaded image file will be put into the
					// preview image tag and finally resized and displayed.
					Reader.onload = function(event) {
						// Put loaded image file into preview image tag and resize it.
						$previewImage.attr('src', event.target.result);
						resize($previewImage);
					};
					
					// Load image and trigger the FileReaders' `onload` event.
					Reader.readAsDataURL(file);
					
					// Change text in file name label and input field.
					$this.find(selectors.filenameLabel).text(fileName);
					$this.find(selectors.filenameInput).val(fileName);
					$this.find(selectors.showImage).val(fileName);
					if (!$this.find(selectors.originalImageName).val()) {
						$this.find(selectors.originalImageName).val(fileName);
						$this.find(selectors.showImage).val(fileName);
					}
					_updateFileInputName();
				});
		};
		
		var _updateFileInputName = function(event) {
			$this.find(selectors.fileInputName)
				.attr('name', 'image_file[' + $this.find(selectors.filenameInput).val() + ']');
		};
		
		/**
		 * Handles manual filename changes in input field.
		 * @param  {jQuery.Event} event Event fired
		 */
		var _changeDataShowImage = function(event) {
			// Replace some specialchars. Still allowed are these chars: . - _
			var fileName = $this.find(selectors.filenameInput)
				.val()
				.replace(/[\s!$§%#^&*()+|~=`´{}\[\]:";\'<>?,\\\/]+/g, '-');
			
			$this.find(selectors.showImage).val(fileName);
		};
		
		module.init = function(done) {
			// Handle file change.
			$this
				.find(selectors.input)
				.on('change', _onNewImage);
			
			// Update name attribute of the file input
			$this
				.find(selectors.filenameInput)
				.on('change', _updateFileInputName);
			
			// Update filename of the image show value
			$this
				.find(selectors.filenameInput)
				.on('input', _changeDataShowImage);
			
			// Register as finished
			done();
		};
		
		return module;
	});
