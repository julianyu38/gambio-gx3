/* --------------------------------------------------------------
 category_menu 2017-05-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.compatibility.module('category_menu', [], function(data) {
	
	'use strict';
	
	var $this = $(this),
		
		/**
		 * Module Object
		 *
		 * @type {object}
		 */
		module = {};
	
	const $catMenuTopSwitcher = $('input:checkbox[name="CAT_MENU_TOP"]');
	const $catMenuLeftSwitcher = $('input:checkbox[name="CAT_MENU_LEFT"]');
	const $showSubcategoriesSwitcher = $('input:checkbox[name="SHOW_SUBCATEGORIES"]');
	const $useAccordionEffectSwitcher = $('input:checkbox[name="CATEGORY_ACCORDION_EFFECT"]');
	
	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------
	
	function _onCatMenuTopSwitcherChange() {
		if ($catMenuTopSwitcher.prop('checked') === false) {
			$catMenuLeftSwitcher.parent().addClass('checked disabled');
			$showSubcategoriesSwitcher.parent().addClass('disabled').removeClass('checked');
			$useAccordionEffectSwitcher.parent().removeClass('disabled');
			
			$catMenuLeftSwitcher.prop('checked', true);
			$showSubcategoriesSwitcher.prop('checked', false);
		} else {
			$catMenuLeftSwitcher.parent().removeClass('disabled');
			$showSubcategoriesSwitcher.parent().removeClass('disabled');
			$useAccordionEffectSwitcher.parent().removeClass('disabled');
		}
	}
	
	function _onSubcategoriesSwitcherChange() {
		if ($showSubcategoriesSwitcher.prop('checked') === true) {
			$useAccordionEffectSwitcher.parent().addClass('disabled').removeClass('checked');
			
			$useAccordionEffectSwitcher.prop('checked', false);
		} else {
			$useAccordionEffectSwitcher.parent().removeClass('disabled');
		}
	}
	
	function _onAccordionSwitcherChange() {
		if ($useAccordionEffectSwitcher.prop('checked') === true || $catMenuTopSwitcher.prop('checked') === false) {
			$showSubcategoriesSwitcher.parent().addClass('disabled').removeClass('checked');
			
			$showSubcategoriesSwitcher.prop('checked', false);
		} else {
			$showSubcategoriesSwitcher.parent().removeClass('disabled');
		}
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	
	/**
	 * Initialize method of the widget, called by the engine.
	 */
	module.init = function(done) {
		$this.on('checkbox:change', $catMenuTopSwitcher, _onCatMenuTopSwitcherChange);
		$this.on('checkbox:change', $showSubcategoriesSwitcher, _onSubcategoriesSwitcherChange);
		$this.on('checkbox:change', $useAccordionEffectSwitcher, _onAccordionSwitcherChange);
		
		$(document).on('JSENGINE_INIT_FINISHED', function() {
			_onCatMenuTopSwitcherChange();
			_onSubcategoriesSwitcherChange();
			_onAccordionSwitcherChange();
		});
		
		
		done();
	};
	
	// Return data to module engine.
	return module;
});