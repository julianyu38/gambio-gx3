/* --------------------------------------------------------------
 new_customer_zones_controller.js 2017-03-27
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * The Component for handling the federal state dropdown depending on the country.
 * The field will be blacked out if there are no federal states for the selected
 * country.
 */
gx.compatibility.module(
	'new_customer_zones_controller',
	
	[
		'form',
		'xhr'
	],
	
	function(data) {
		
		'use strict';
		
		// ########## VARIABLE INITIALIZATION ##########
		
		var $this            = $(this),
			$customerStates  = $('select[name=customers_state]'),
			$deliveryStates  = $('select[name=delivery_state]'),
			$billingStates   = $('select[name=billing_state]'),
			
			$customerFormGroup = $('select[name=customers_state]').closest('div.grid'),
			$deliveryFormGroup = $('select[name=delivery_state]').closest('div.grid'),
			$billingFormGroup  = $('select[name=billing_state]').closest('div.grid'),
			
			defaults = {
				loadStates: 'admin.php?do=Zones/OrderAddressEdit',
				customersCountry:  'select[name=customers_country]',
				deliveryCountry:   'select[name=delivery_country_iso_code_2]',
				billingCountry:    'select[name=billing_country_iso_code_2]',
				
			},
			
			options = $.extend(true, {}, defaults, data),
			module = {};
		
		
		var _changeHandler = function(e) {
			var dataset      = jse.libs.form.getData($this);
			dataset.selectors = e.data.selectors;

			jse.libs.xhr.ajax({url: options.loadStates, data: dataset}, true).done(function(result) {
				
				var $selector;
				var $selectorFormGroup;

				switch (result.selector) {
					case 'customers_country':
						$selector = $customerStates;
						$selectorFormGroup = $customerFormGroup;
						break;
					case 'delivery_country':
						$selector = $deliveryStates;
						$selectorFormGroup = $deliveryFormGroup;
						break;
					case 'billing_country':
						$selector = $billingStates;
						$selectorFormGroup = $billingFormGroup;
						break;
				}

				if (result.success) {
					
					$selector.children('option').remove();
					$selector.prop("disabled", false);

					$.each(result.data, function(key, value) {
						if(value.selected)
						{
							$selector.append($("<option selected/>").val(value.name).text(value.name));
						}
						else
						{
							$selector.append($("<option />").val(value.name).text(value.name));
						}
					});
					
					$selectorFormGroup.show();

				}
				else {
					$selectorFormGroup.hide();
					$selector.prop("disabled", true);
				}

			});
			
		};
		
		// ########## INITIALIZATION ##########
		
		/**
		 * Init function of the widget
		 * @constructor
		 */
		module.init = function(done) {
			
			$this.on('change', options.customersCountry, {'selectors': {'country': 'customers_country', 'state': 'customers_state', 'selected': 'select_customers_state'}}, _changeHandler)
				.on('change', options.deliveryCountry, {'selectors': {'country': 'delivery_country', 'state': 'delivery_state', 'selected': 'select_delivery_state'}}, _changeHandler)
				.on('change', options.billingCountry, {'selectors': {'country': 'billing_country', 'state': 'billing_state', 'selected': 'select_billing_state'}}, _changeHandler);
			
			$this.find(options.customersCountry).trigger('change', {'selectors': {'country': 'customers_country', 'state': 'customers_state', 'selected': 'select_customers_state'}});
			$this.find(options.deliveryCountry).trigger('change', {'selectors': {'country': 'delivery_country', 'state': 'delivery_state', 'selected': 'select_delivery_state'}});
			$this.find(options.billingCountry).trigger('change', {'selectors': {'country': 'billing_country', 'state': 'billing_state', 'selected': 'select_billing_state'}});
			
			done();
		};
		
		// Return data to widget engine
		return module;
	});
