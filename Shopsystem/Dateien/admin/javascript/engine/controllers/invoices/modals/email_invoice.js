/* --------------------------------------------------------------
 email_invoice.js 2016-05-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Email Invoice Modal Controller
 *
 * Handles the functionality of the Email Invoice modal.
 */
gx.controllers.module('email_invoice', ['modal'], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {
		bindings: {
			subject: $this.find('.subject'),
			emailAddress: $this.find('.email-address')
		}
	};
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Send the modal data to the form through an AJAX call.
	 *
	 * @param {jQuery.Event} event
	 */
	function _onSendClick(event) {
		$this.find('.has-error').removeClass('has-error');
		
		const {orderId, invoiceId} = $this.data();
		
		const parameters = {
			oID: orderId,
			iID: invoiceId,
			type: 'invoice',
			mail: '1',
			gm_quick_mail: '1',
			preview: '1'
		};
		const url = jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php?' + $.param(parameters);
		const data = {
			gm_mail: module.bindings.emailAddress.get(),
			gm_subject: module.bindings.subject.get()
		};
		
		if (data.gm_subject === '') {
			$this.find('.subject').parents('.form-group').addClass('has-error');
		}
		
		if (data.gm_mail === '') {
			$this.find('.email-address').parents('.form-group').addClass('has-error');
		}
		
		if ($this.find('.has-error').length) {
			return;
		}
		
		const $sendButton = $(event.target);
		
		$sendButton.addClass('disabled').prop('disabled', true);
		
		$.ajax({
			url,
			data,
			method: 'POST'
		})
			.done(response => {
				const message = jse.core.lang.translate('MAIL_SUCCESS', 'gm_send_order');
				
				$('.invoices .table-main').DataTable().ajax.reload(null, false);
				$('.invoices .table-main').invoices_overview_filter('reload');
				
				// Show success message in the admin info box.
				jse.libs.info_box.addSuccessMessage(message);
				$this.modal('hide');
			})
			.fail((jqxhr, textStatus, errorThrown) => {
				const title = jse.core.lang.translate('error', 'messages');
				const message = jse.core.lang.translate('MAIL_UNSUCCESS', 'gm_send_order');
				
				// Show error message in a modal.
				jse.libs.modal.showMessage(title, message);
			})
			.always(() => {
				$sendButton.removeClass('disabled').prop('disabled', false);
			});
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};
	
	return module;
});