/* --------------------------------------------------------------
 bulk_email_invoice.js 2016-10-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Bulk Email Invoice Modal Controller
 */
gx.controllers.module('bulk_email_invoice', ['modal', 'loading_spinner'], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {
		bindings: {subject: $this.find('.subject')}
	};
	
	/**
	 * Selector for the email list item.
	 *
	 * @type {String}
	 */
	const emailListItemSelector = '.email-list-item';
	
	/**
	 * Selector for the email list item ID.
	 *
	 * @type {String}
	 */
	const emailListItemEmailSelector = '.email-input';
	
	/**
	 * Selector for the modal content body layer.
	 *
	 * @type {String}
	 */
	const modalContentSelector = '.modal-content';
		
	/**
	 * Request URL
	 *
	 * @type {String}
	 */
	const requestUrl = jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php';
	
	/**
	 * Loading spinner instance.
	 *
	 * @type {jQuery|null}
	 */
	let $spinner = null;
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Show/hide loading spinner.
	 *
	 * @param {Boolean} doShow Show the loading spinner?
	 */
	function _toggleSpinner(doShow) {
		if (doShow) {
			$spinner = jse.libs.loading_spinner.show($this.find(modalContentSelector), $this.css('z-index'));
		} else {
			jse.libs.loading_spinner.hide($spinner);
		}
	}
	
	/**
	 * Parse subject and replace the placeholders with the variables.
	 *
	 * @param {Object} invoiceData Invoice data
	 *
	 * @return {String}
	 */
	function _getParsedSubject(invoiceData) {
		const dateFormat = jse.core.config.get('languageCode') === 'de' ? 'DD.MM.YY' : 'MM.DD.YY'; 
		
		return module.bindings.subject.get()
			.replace('{INVOICE_NUM}', invoiceData.invoiceNumber)
			.replace('{INVOICE_DATE}', moment(invoiceData).format(dateFormat))
			.replace('{ORDER_ID}', invoiceData.orderId);
	}
	
	/**
	 * Handles the successful delivery of all messages.
	 */
	function _handleDeliverySuccess() {
		const message = jse.core.lang.translate('BULK_MAIL_SUCCESS', 'gm_send_order');
		
		// Show success message in the admin info box.
		jse.libs.info_box.addSuccessMessage(message);
		
		$('.invoices .table-main').DataTable().ajax.reload(null, false);
		$('.invoices .table-main').invoices_overview_filter('reload');
		
		// Hide modal and loading spinner.
		_toggleSpinner(false);
		$this.modal('hide');
	}
	
	/**
	 * Handles the failure of the message delivery.
	 */
	function _handleDeliveryFail() {
		const title = jse.core.lang.translate('error', 'messages');
		const message = jse.core.lang.translate('BULK_MAIL_UNSUCCESS', 'gm_send_order');
		
		// Show error message in a modal.
		jse.libs.modal.showMessage(title, message);
		
		// Hide modal and the loading spinner and re-enable the send button.
		_toggleSpinner(false);
		$this.modal('hide');
	}
	
	/**
	 * Validate the form for empty fields. 
	 * 
	 * @return {Boolean}
	 */
	function _validateForm() {
		$this.find('.has-error').removeClass('has-error'); 
		
		$this.find('input:text').each((index, input) => {
			const $input = $(input); 
			
			if ($input.val() === '') {
				$input.parents('.form-group').addClass('has-error'); 
			}
		}); 
			
		return !$this.find('.has-error').length; 
	}
	
	/**
	 * Send the modal data to the form through an AJAX call.
	 */
	function _onSendClick() {
		if (!_validateForm()) {
			return;
		}		
		
		// Collection of requests in promise format.
		const promises = [];
		
		// Email list item elements.
		const $emailListItems = $this.find(emailListItemSelector);
		
		// Abort and hide modal on empty email list entries.
		if (!$emailListItems.length) {
			$this.modal('hide');
			return;
		}
		
		// Show loading spinner.
		_toggleSpinner(true);
		
		// Fill orders array with data.
		$emailListItems.each((index, element) => {
			// Order data.
			const invoiceData = $(element).data('invoice');
			
			// Email address entered in input field.
			const enteredEmail = $(element).find(emailListItemEmailSelector).val();
			
			// Promise wrapper for AJAX requests.
			const promise = new Promise((resolve, reject) => {
				// Request GET parameters to send.
				const getParameters = {
					oID: invoiceData.orderId,
					iID: invoiceData.invoiceId,
					type: 'invoice',
					mail: '1',
					gm_quick_mail: '1',
					preview: '1'
				};
				
				// Composed request URL.
				const url = requestUrl + '?' + $.param(getParameters);
				
				// Data to send.
				const data = {
					gm_mail: enteredEmail,
					gm_subject: _getParsedSubject(invoiceData)
				};
				
				$.ajax({method: 'POST', url, data})
					.done(resolve)
					.fail(reject);
			});
			
			// Add promise to array.
			promises.push(promise);
		});
		
		// Wait for all promise to respond and handle success/error.
		Promise.all(promises)
			.then(_handleDeliverySuccess)
			.catch(_handleDeliveryFail);
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};
	
	return module;
});