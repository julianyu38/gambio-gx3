/* --------------------------------------------------------------
 delete.js 2016-09-30
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Delete Invoice Modal Controller
 */
gx.controllers.module('delete', ['modal'], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {
		bindings: {
			selectedInvoices: $this.find('.selected-invoice-ids')
		}
	};
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Send the modal data to the form through an AJAX call.
	 */
	function _onDeleteClick() {
		const url = jse.core.config.get('appUrl') + '/admin/admin.php?do=InvoicesModalsAjax/DeleteInvoice';
		const data = {
			selectedInvoices: module.bindings.selectedInvoices.get().split(','),
			pageToken: jse.core.config.get('pageToken')
		};
		const $deleteButton = $(this);
		
		$deleteButton.addClass('disabled').prop('disabled', true);
		
		$.ajax({
			url,
			data,
			method: 'POST',
			dataType: 'json'
		})
			.done(function(response) {
				jse.libs.info_box.addSuccessMessage(
					jse.core.lang.translate('DELETE_INVOICES_SUCCESS', 'admin_invoices'));
				$('.invoices .table-main').DataTable().ajax.reload(null, false);
				$('.invoices .table-main').invoices_overview_filter('reload');
			})
			.fail(function(jqxhr, textStatus, errorThrown) {
				jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'),
					jse.core.lang.translate('DELETE_INVOICES_ERROR', 'admin_invoices'));
			})
			.always(function() {
				$this.modal('hide');
				$deleteButton.removeClass('disabled').prop('disabled', false);
			});
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this.on('click', '.btn.delete', _onDeleteClick);
		done();
	};
	
	return module;
});