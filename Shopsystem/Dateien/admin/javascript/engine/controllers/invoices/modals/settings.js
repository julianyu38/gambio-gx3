/* --------------------------------------------------------------
 settings.js 2016-10-04
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Invoices Overview Settings 
 *
 * It retrieves the settings data via the user configuration service and sets the values.
 * You are able to change the column sort order and the visibility of each column. Additionally
 * you can change the height of the table rows.
 */
gx.controllers.module(
	'settings',
	
	[
		'user_configuration_service',
		'loading_spinner',
		`${gx.source}/libs/overview_settings_modal_controller`
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = function(done) {
			const SettingsModalController = jse.libs.overview_settings_modal_controller.class;
			
			const dependencies = {
				element: $this,
				userCfgService: jse.libs.user_configuration_service,
				loadingSpinner: jse.libs.loading_spinner,
				userId: data.userId,
				defaultColumnSettings: data.defaultColumnSettings,
				translator: jse.core.lang,
				page: 'invoices'
			};
			
			const settingsModal = new SettingsModalController(
				dependencies.element,
				dependencies.userCfgService,
				dependencies.loadingSpinner,
				dependencies.userId,
				dependencies.defaultColumnSettings,
				dependencies.translator,
				dependencies.page
			);
			
			settingsModal.initialize();
			
			done();
		};
		
		return module;
	});
