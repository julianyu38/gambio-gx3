/* --------------------------------------------------------------
 actions.js 2016-10-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Actions
 *
 * This module creates the bulk and row actions for the table.
 */
gx.controllers.module(
	'actions',
	
	['user_configuration_service', `${gx.source}/libs/button_dropdown`],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Create Bulk Actions
		 *
		 * This callback can be called once during the initialization of this module.
		 */
		function _createBulkActions() {
			// Add actions to the bulk-action dropdown.
			const $bulkActions = $('.bulk-action');
			const defaultBulkAction = $this.data('defaultBulkAction') || 'bulk-email-invoice';
			
			jse.libs.button_dropdown.bindDefaultAction($bulkActions, jse.core.registry.get('userId'),
				'invoicesOverviewBulkAction', jse.libs.user_configuration_service);
			
			// Email
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_EMAIL', 'admin_buttons'),
				class: 'bulk-email-invoice',
				data: {configurationValue: 'bulk-email-invoice'},
				isDefault: defaultBulkAction === 'bulk-email-invoice',
				callback: e => e.preventDefault()
			});
			
			// Change status
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_MULTI_CHANGE_ORDER_STATUS', 'orders'),
				class: 'change-status',
				data: {configurationValue: 'change-status'},
				isDefault: defaultBulkAction === 'change-status',
				callback: e => e.preventDefault()
			});
			
			// Download invoices
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BULK_DOWNLOAD_INVOICES', 'admin_invoices'),
				class: 'bulk-download-invoice',
				data: {configurationValue: 'bulk-download-invoice'},
				isDefault: defaultBulkAction === 'bulk-download-invoice',
				callback: e => e.preventDefault()
			});
			
			// Delete
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_MULTI_DELETE', 'orders'),
				class: 'delete',
				data: {configurationValue: 'delete'},
				isDefault: defaultBulkAction === 'delete',
				callback: e => e.preventDefault()
			});
			
			// Cancellation Invoice
			if (!!data.isPdfCreatorInstalled) {
				jse.libs.button_dropdown.addAction($bulkActions, {
					text: jse.core.lang.translate('CANCELLATION_INVOICE', 'admin_invoices'),
					class: 'bulk-cancellation-invoice',
					data: {configurationValue: 'bulk-cancellation-invoice'},
					isDefault: defaultBulkAction === 'bulk-cancellation-invoice',
					callback: e => e.preventDefault()
				});	
			}
			
			$this.datatable_default_actions('ensure', 'bulk');
		}
		
		/**
		 * Create Table Row Actions
		 *
		 * This function must be call with every table draw.dt event.
		 */
		function _createRowActions() {
			// Re-create the checkbox widgets and the row actions. 
			const defaultRowAction = $this.data('defaultRowAction') || 'view';
			
			jse.libs.button_dropdown.bindDefaultAction($this.find('.btn-group.dropdown'),
				jse.core.registry.get('userId'), 'invoicesOverviewRowAction', jse.libs.user_configuration_service);
			
			$this.find('.btn-group.dropdown').each(function() {
				const {invoiceNumber, orderId, isCancellationInvoice, country} = $(this).parents('tr').data();
				
				// View
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('TEXT_SHOW', 'orders'),
					href: `request_port.php?module=OrderAdmin&action=showPdf&type=invoice`
					+ `&invoice_number=${invoiceNumber}&order_id=${orderId}`,
					target: '_blank',
					class: 'view',
					data: {configurationValue: 'view'},
					isDefault: defaultRowAction === 'view'
				});
				
				// Download
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_DOWNLOAD', 'admin_buttons'),
					href: `request_port.php?module=OrderAdmin&action=downloadPdf&type=invoice`
					+ `&invoice_number=${invoiceNumber}&order_id=${orderId}`,
					target: '_blank',
					class: 'download',
					data: {configurationValue: 'download'},
					isDefault: defaultRowAction === 'download'
				});
				
				// Email
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_EMAIL', 'admin_buttons'),
					class: 'email-invoice',
					data: {configurationValue: 'email-invoice'},
					isDefault: defaultRowAction === 'email-invoice',
					callback: e => e.preventDefault()
				});
				
				// Change Status
				if (orderId > 0) {
					jse.libs.button_dropdown.addAction($(this), {
						text: jse.core.lang.translate('TEXT_GM_STATUS', 'orders'),
						class: 'change-status',
						data: {configurationValue: 'change-status'},
						isDefault: defaultRowAction === 'change-status',
						callback: e => e.preventDefault()
					});
				}
				
				// Delete
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_MULTI_DELETE', 'orders'),
					class: 'delete',
					data: {configurationValue: 'delete'},
					isDefault: defaultRowAction === 'delete',
					callback: e => e.preventDefault()
				});
				
				// Cancellation invoice
				if (isCancellationInvoice === false && orderId > 0 && country !== '' && !!data.isPdfCreatorInstalled) {
					jse.libs.button_dropdown.addAction($(this), {
						text: jse.core.lang.translate('CANCELLATION_INVOICE', 'admin_invoices'),
						class: 'cancellation-invoice',
						data: {configurationValue: 'cancellation-invoice'},
						isDefault: defaultRowAction === 'cancellation-invoice',
						callback: e => e.preventDefault()
					});
				}
				
				$this.datatable_default_actions('ensure', 'row');
			});
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$(window).on('JSENGINE_INIT_FINISHED', () => {
				$this.on('draw.dt', _createRowActions);
				_createRowActions();
				_createBulkActions();
			});
			
			done();
		};
		
		return module;
		
	}); 