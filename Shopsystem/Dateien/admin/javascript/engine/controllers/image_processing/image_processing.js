/* --------------------------------------------------------------
 image_processing.js 2017-03-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Image Processing
 *
 * This module will execute the image processing by sending POST-Requests to the
 * ImageProcessingController interface
 *
 * @module Controllers/image_processing
 */
gx.controllers.module(
	'image_processing',
	
	[
		gx.source + '/libs/info_messages'
	],
	
	/**  @lends module:Controllers/image_processing */
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Module Selector
			 *
			 * @var {object}
			 */
			$this = $(this),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {},
			
			/**
			 * Flag if an error occurred during the image processing
			 *
			 * @type {boolean}
			 */
			error = false,
			
			/**
			 * Final Options
			 *
			 * @var {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Reference to the info messages library
			 * 
			 * @type {object}
			 */
			messages = jse.libs.info_messages,
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// EVENT HANDLERS
		// ------------------------------------------------------------------------
		
		var _onClick = function() {
			var title = jse.core.lang.translate('image_processing_title', 'image_processing');
			var startimageNr = parseInt($('#image-processing-startimage-nr').val());
			var startimageFile = $('#image-processing-startimage-file').val();
			
			$('.process-modal').dialog({
				'title': title,
				'modal': true,
				'dialogClass': 'gx-container',
				'buttons': [
					{
						'text': jse.core.lang.translate('close', 'buttons'),
						'class': 'btn',
						'click': function() {
							$(this).dialog('close');
						}
					}
				],
				'width': 580
			});
			
			$('#image-processing-file').html(' ');
			
			if (startimageNr <= 0 || isNaN(startimageNr)) {
				startimageNr = 1;
			}
			
			if (startimageFile != '') {
				_processImage(0, startimageFile);
			} else {
				_processImage(startimageNr, '');
			}
		};
		
		// ------------------------------------------------------------------------
		// AJAX
		// ------------------------------------------------------------------------
		
		var _processImage = function(imageNumber, imageFile) {
			
			$.ajax({
				'type': 'POST',
				'url': 'admin.php?do=ImageProcessing/Process',
				'timeout': 30000,
				'dataType': 'json',
				'context': this,
				'data': {
					'image_number': imageNumber,
					'image_file': imageFile
				},
				success: function(response) {
					if (response.payload.nextImageNr != 0) {
						imageNumber = response.payload.nextImageNr - 1;
					}
					
					var progress = (100 / response.payload.imagesCount) * imageNumber;
					progress = Math.round(progress);
					
					$('.process-modal .progress-bar').attr('aria-valuenow', progress);
					$('.process-modal .progress-bar').css('min-width', '70px');
					$('.process-modal .progress-bar').css('width', progress + '%');
                    $('.process-modal .progress-bar').html(imageNumber + ' / ' + response.payload.imagesCount);
                    $('#image-processing-file').html(response.payload.imageName);
					
					if (!response.success) {
						error = true;
					}
					
					if (!response.payload.finished) {
						// check is there is nextImageNr not 0 (only if filename was given)
						if (response.payload.nextImageNr != 0) {
							_processImage(response.payload.nextImageNr, '');
						} else {
							imageNumber += 1;
							_processImage(imageNumber, '');
						}
					} else {
						$('.process-modal').dialog('close');
						$('.process-modal .progress-bar').attr('aria-valuenow', 0);
						$('.process-modal .progress-bar').css('width', '0%');
						$('.process-modal .progress-bar').html('');
						
						if (error) {
							if (response.payload.fileNotFound) {
								messages.addError(jse.core.lang.translate('image_processing_file_not_found',
									'image_processing'));
							} else {
								messages.addError(jse.core.lang.translate('image_processing_error',
									'image_processing'));
							}
						} else {
							messages.addSuccess(jse.core.lang.translate('image_processing_success',
								'image_processing'));
						}
						
						error = false;
					}
				}
			});
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$this.on('click', '.js-process', _onClick);
			done();
		};
		
		return module;
	});
