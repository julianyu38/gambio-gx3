/* --------------------------------------------------------------
 admin_access_admin_edit.js 2018-04-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
	// ------------------------------------------------------------------------
	// CONTROLLER NAME
	// ------------------------------------------------------------------------
	'admin_access_roles',
	
	// ------------------------------------------------------------------------
	// CONTROLLER LIBRARIES
	// ------------------------------------------------------------------------
	[
		'modal',
		`${jse.source}/vendor/jquery-ui/jquery-ui.min.css`,
		`${jse.source}/vendor/jquery-ui/jquery-ui.js`
	],
	
	// ------------------------------------------------------------------------
	// CONTROLLER BUSINESS LOGIC
	// ------------------------------------------------------------------------
	function(data) {
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Controller reference.
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Default options for controller,
		 *
		 * @type {object}
		 */
		const defaults = {};
		
		/**
		 * Final controller options.
		 *
		 * @type {object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module object.
		 *
		 * @type {{}}
		 */
		const module = {};
		
		/**
		 * Modal objects.
		 *
		 * @type {object}
		 */
		const $modal = {
			'create_edit': $('.create-edit-role-modal'),
			'delete': $('.delete-role-modal')
		};
		
		/**
		 * Sortable list.
		 *
		 * @type {jQuery}
		 */
		const $sortableList = $('ul.overview-list');
		
		/**
		 * URLs object. Contains all URLs that are necessary for this controller.
		 *
		 * @type {object}
		 */
		const urls = {
			'getRoleData': 'admin.php?do=AdminAccessAjax/getRoleData',
			'deleteRole': 'admin.php?do=AdminAccessAjax/deleteRole',
			'saveRoleData': 'admin.php?do=AdminAccessAjax/saveRoleData',
			'saveSorting': 'admin.php?do=AdminAccessAjax/saveRoleSorting'
		};
		
		/**
		 * Object for the selected role row.
		 *
		 * @type {object}
		 */
		let $selectedRoleRow = {};
		
		// ------------------------------------------------------------------------
		// PRIVATE METHODS
		// ------------------------------------------------------------------------
		
		/**
		 * Opens a modal with an error message for an unexpected error.
		 */
		function _unexpectedError() {
			jse.libs.modal.showMessage(
				jse.core.lang.translate('error_modal_title', 'admin_access'),
				jse.core.lang.translate('error_ajax_request_failed', 'admin_access')
			);
		}
		
		/**
		 * Clears the form inputs of the role modal.
		 */
		function _clearModalForm() {
			$modal['create_edit']
				.find('input[name=roleId]')
				.val('');
			$modal['create_edit']
				.find('input[name=roleName]')
				.val('')
				.removeClass('error');
			$modal['create_edit']
				.find('input[name=roleDescription]')
				.val('');
			$modal['create_edit']
				.find('.nav-tabs li:first a')
				.click();
		}
		
		/**
		 * Updates the form inputs of the role modal.
		 */
		function _updateModalForm(formdata) {
			_clearModalForm();
			
			$modal['create_edit']
				.find('input[name=roleId]')
				.val(formdata['id']);
			$modal['create_edit']
				.find('input[name=roleSortOrder]')
				.val(formdata['sortOrder']);
			for (let lang in formdata['names']) {
				$modal['create_edit']
					.find('input[name=roleName][data-lang=' + lang + ']')
					.val(formdata['names'][lang]);
			}
			for (let lang in formdata['descriptions']) {
				$modal['create_edit']
					.find('input[name=roleDescription][data-lang=' + lang + ']')
					.val(formdata['descriptions'][lang]);
			}
		}
		
		/**
		 * Return the form data of the role modal.
		 */
		function _getModalFormData() {
			const roleId = parseInt($modal['create_edit'].find('input[name=roleId]').val());
			let names = {};
			let descriptions = {};
			const sortOrder = $('.overview-list li').length;
			
			$modal['create_edit']
				.find('input[name=roleName]')
				.each((index, element) => {
					names[$(element).data('lang')] = $(element).val();
				});
			$modal['create_edit']
				.find('input[name=roleDescription]')
				.each((index, element) => {
					descriptions[$(element).data('lang')] = $(element).val();
				});
			
			return {
				'id': roleId === NaN ? '' : roleId,
				'sortOrder': sortOrder,
				'names': names,
				'descriptions': descriptions
			};
		}
		
		/**
		 * Refreshes the role overview
		 */
		function _addRowToOverview(data) {
			if ($selectedRoleRow !== undefined) {
				$selectedRoleRow
					.find('.list-element-title')
					.text(data['names'][jse.core.config.get('languageCode').toUpperCase()]);
			} else {
				$('.overview-list').append(`
					<li class="col-md-12 list-element" data-list-element-id="` + data['id'] + `">
						<span class="list-element-text list-element-title col-md-8"
						      title="` + data['descriptions'][jse.core.config.get('languageCode').toUpperCase()] + `"
						>
							` + data['names'][jse.core.config.get('languageCode').toUpperCase()].replace(/</g, '&lt;')
					+ `
						</span>
						<span class="col-md-4 list-element-actions">
							<a href="#" class="edit-role" data-id="` + data['id'] + `"><i class="fa fa-pencil"></i></a>
							<a href="admin.php?do=AdminAccess/managePermissions&id=` + data['id'] + `"><i class="fa fa-cog"></i></a>
							<a href="#" class="delete-role" data-id="` + data['id'] + `"><i class="fa fa-trash-o"></i></a>
							<a href="#" class="sort-handle ui-sortable-handle"><i class="fa fa-sort"></i></a>
						</span>
					</li>
				`);
				
				$('.overview-list .list-element:last a.create-role').on('click', _openCreateRoleModal);
				$('.overview-list .list-element:last a.edit-role').on('click', _openEditRoleModal);
				$('.overview-list .list-element:last a.delete-role').on('click', _openDeleteRoleModal);
				$('.overview-list .list-element:last a.sort-handle').on('click', (event) => {
					event.preventDefault();
				});
			}
		}
		
		// ------------------------------------------------------------------------
		// EVENT HANDLER
		// ------------------------------------------------------------------------
		
		/**
		 * Click handler for the create role button.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _openCreateRoleModal(event) {
			// Prevent default action.
			event.preventDefault();
			
			$selectedRoleRow = undefined;
			
			// Reset modal data and open modal
			_clearModalForm();
			$modal['create_edit']
				.find('.modal-title')
				.text(jse.core.lang.translate('role_modal_create_title', 'admin_access'));
			$modal['create_edit']
				.find('button.confirm')
				.text(jse.core.lang.translate('BUTTON_CREATE', 'admin_buttons'));
			$modal['create_edit']
				.modal('show');
		}
		
		/**
		 * Click handler for the edit role button.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _openEditRoleModal(event) {
			// Prevent default action.
			event.preventDefault();
			
			const roleId = $(this).data('id');
			$selectedRoleRow = $(this).closest('.list-element');
			
			// Load role data and open modal on success
			$.ajax({
				type: "GET",
				url: urls.getRoleData + '&roleId=' + roleId,
				success: (response) => {
					response = JSON.parse(response);
					if (response['success'] === true) {
						_updateModalForm(response['data']);
						$modal['create_edit']
							.find('.modal-title')
							.text(jse.core.lang.translate('role_modal_edit_title', 'admin_access'));
						$modal['create_edit']
							.find('button.confirm')
							.text(jse.core.lang.translate('BUTTON_SAVE', 'admin_buttons'));
						$modal['create_edit']
							.modal('show');
						
						return;
					}
					_unexpectedError();
				},
				error: () => {
					_unexpectedError();
				},
			});
		}
		
		/**
		 * Click handler for the delete role button.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _openDeleteRoleModal(event) {
			// Prevent default action.
			event.preventDefault();
			
			const roleId = $(this).data('id');
			$selectedRoleRow = $(this).closest('.list-element');
			
			// Load role data and open modal on success
			$.ajax({
				type: "GET",
				url: urls.getRoleData + '&roleId=' + roleId,
				success: (response) => {
					response = JSON.parse(response);
					if (response['success'] === true) {
						$modal['delete']
							.find('input[name=roleId]')
							.val(response['data']['id']);
						$modal['delete']
							.find('fieldset .role-name')
							.text(response['data']['names'][jse.core.config.get('languageCode').toUpperCase()]);
						$modal['delete']
							.find('fieldset .role-description')
							.text(response['data']['descriptions'][jse.core.config.get('languageCode').toUpperCase()]);
						$modal['delete']
							.modal('show');
						
						return;
					}
					_unexpectedError();
				},
				error: () => {
					_unexpectedError();
				},
			});
		}
		
		/**
		 * Click handler for the delete modal submit button.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _submitCreateEditModalForm(event) {
			// Prevent default action.
			event.preventDefault();
			
			const data = _getModalFormData();
			const $emptyNameInputs = $modal['create_edit'].find('input[name=roleName]').filter(function() {
				return $(this).val() === '';
			});
			const $nonEmptyNameInputs = $modal['create_edit'].find('input[name=roleName]').filter(function() {
				return $(this).val() !== '';
			});
			
			if ($emptyNameInputs.size() > 0) {
				if ($nonEmptyNameInputs.size() > 0) {
					for (let lang in data.names) {
						if (data.names[lang] === '') {
							data.names[lang] = $nonEmptyNameInputs.first().val();
						}
					}
				} else {
					$emptyNameInputs.addClass('error');
					return;
				}
			}
			
			// Update role data
			{
				$.ajax({
					type: "POST",
					url: urls.saveRoleData,
					data: data,
					success: (response) => {
						response = JSON.parse(response);
						if (response['success'] === true) {
							$modal['create_edit']
								.modal('hide');
							data['id'] = response['roleId'];
							_addRowToOverview(data);
							
							if ($('.overview-list-container').hasClass('empty')) {
								$('.overview-list li:not(".list-headline"):first').remove();
								$('.overview-list-container').removeClass('empty');
							}
							
							return;
						}
						_unexpectedError();
					},
					error: () => {
						_unexpectedError();
					},
				});
			}
		}
		
		/**
		 * Click handler for the create edit modal submit button.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _submitDeleteModalForm(event) {
			// Prevent default action.
			event.preventDefault();
			
			const roleId = $modal['delete'].find('input[name=roleId]').val();
			
			// Update role data
			$.ajax({
				type: "POST",
				url: urls.deleteRole,
				data: {
					'roleId': roleId
				},
				success: (response) => {
					response = JSON.parse(response);
					if (response['success'] === true) {
						$modal['delete']
							.modal('hide');
						$selectedRoleRow.remove();
						
						if ($('.overview-list li:not(".list-headline")').length === 0) {
							$('.overview-list').append(`
								<li class="col-md-12 list-element">
									<span class="title">`
								+ jse.core.lang.translate('text_empty_roles_overview_list', 'admin_access') + `</span>
								</li>
							`);
							$('.overview-list-container').addClass('empty');
						}
						
						return;
					}
					_unexpectedError();
				},
				error: () => {
					_unexpectedError();
				},
			});
		}
		
		/**
		 * Sorting event handler for sortable plugin
		 *
		 * Makes a call to the ajax controller after a sorting event
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 * @param {object} ui    Sortable list (ul) object with new sort order
		 */
		function _saveSorting(event, ui) {
			if (!ui.item.parent().is('ul')) {
				$sortableList.sortable('cancel');
			}
			
			$.ajax({
				url: urls.saveSorting,
				method: 'POST',
				data: {
					'sorting': $sortableList.sortable('toArray', {attribute: 'data-list-element-id'})
				},
				success: function(response) {
					response = JSON.parse(response);
					if (response.success === false) {
						_unexpectedError()
					} else {
						jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('text_saved_sorting', 'admin_access'));
					}
				},
				error: function() {
					_unexpectedError()
				}
			});
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		module.init = done => {
			// initialization logic
			$('a.create-role').on('click', _openCreateRoleModal);
			$('a.edit-role').on('click', _openEditRoleModal);
			$('a.delete-role').on('click', _openDeleteRoleModal);
			$('a.sort-handle').on('click', (event) => {
				event.preventDefault();
			});
			
			$modal['create_edit']
				.find('button.confirm')
				.on('click', _submitCreateEditModalForm);
			$modal['delete']
				.find('button.confirm')
				.on('click', _submitDeleteModalForm);
			
			$sortableList
				.sortable({
					items: 'li.list-element',
					axis: 'y',
					cursor: 'move',
					handle: '.sort-handle',
					containment: 'document',
					opacity: 0.75,
					placeholder: 'col-md-12 list-element sort-placeholder'
				})
				.on('sortupdate', _saveSorting)
				.disableSelection();
			
			done();
		}
		
		return module;
	}
);