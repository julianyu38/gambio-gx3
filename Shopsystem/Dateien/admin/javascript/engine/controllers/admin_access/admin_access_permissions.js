/* --------------------------------------------------------------
 admin_access_permissions.js 2018-02-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
	// ------------------------------------------------------------------------
	// CONTROLLER NAME
	// ------------------------------------------------------------------------
	'admin_access_permissions',
	
	// ------------------------------------------------------------------------
	// CONTROLLER LIBRARIES
	// ------------------------------------------------------------------------
	[
		'modal',
	],
	
	// ------------------------------------------------------------------------
	// CONTROLLER BUSINESS LOGIC
	// ------------------------------------------------------------------------
	function(data) {
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Controller reference.
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Default options for controller,
		 *
		 * @type {object}
		 */
		const defaults = {};
		
		/**
		 * Final controller options.
		 *
		 * @type {object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module object.
		 *
		 * @type {{}}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// PRIVATE METHODS
		// ------------------------------------------------------------------------
		
		/**
		 * Opens a modal with an error message for an unexpected error.
		 */
		function _setAllPermissionGrantingSwitcherValues() {
			const $readingAll = $('input.all-permission-checkbox[data-permission-type=reading]');
			const $writingAll = $('input.all-permission-checkbox[data-permission-type=writing]');
			const $deletingAll = $('input.all-permission-checkbox[data-permission-type=deleting]');
			if ($readingAll !== undefined) {
				$readingAll
					.switcher('checked', $('input.permission-checkbox[data-permission-type=reading]').length
						=== $('input.permission-checkbox[data-permission-type=reading]:checked').length);
			}
			if ($writingAll !== undefined) {
				$writingAll
					.switcher('checked', $('input.permission-checkbox[data-permission-type=writing]').length
						=== $('input.permission-checkbox[data-permission-type=writing]:checked').length);
			}
			if ($deletingAll !== undefined) {
				$deletingAll
					.switcher('checked', $('input.permission-checkbox[data-permission-type=deleting]').length
						=== $('input.permission-checkbox[data-permission-type=deleting]:checked').length);
			}
		}
		
		// ------------------------------------------------------------------------
		// EVENT HANDLER
		// ------------------------------------------------------------------------
		
		/**
		 * Click handler for the permission granting switchers.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _updatePermissionGranting(event) {
			// Set checkbox for parent group or child groups for special conditions
			if ($(this).data('parent-id') > 0 && $(this).is(':checked')) {
				$('input.permission-checkbox[data-group-id=' + $(this).data('parent-id') + '][data-permission-type='
					+ $(this).data('permission-type') + ']').switcher('checked', true);
			}
			if ($(this).data('group-id') > 0 && $(this).is(':checked') === false) {
				$('input.permission-checkbox[data-parent-id=' + $(this).data('group-id') + '][data-permission-type='
					+ $(this).data('permission-type') + ']').switcher('checked', false);
			}
			
			_setAllPermissionGrantingSwitcherValues();
		}
		
		/**
		 * Click handler for the all permission granting switchers.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _updateAllPermissionGranting(event) {
			if ($(this).is(':checked')) {
				$('input.permission-checkbox:not(:checked)')
					.switcher('checked', true);
			} else {
				$('input.permission-checkbox[data-permission-type=' + $(this).data('permission-type') + ']:checked')
					.switcher('checked', false);
			}
		}
		
		/**
		 * Click handler for the group collapse handler.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _collapsePermissionSubGroups(event) {
			const groupId = $(this).data('groupId');
			const $icon = $(this).find('span');
			
			if ($('li[data-parent-group-id=' + groupId + ']').length) {
				if ($icon.hasClass('fa-minus-square-o')) {
					$('li[data-parent-group-id=' + groupId + ']').hide();
					$icon.removeClass('fa-minus-square-o');
					$icon.addClass('fa-plus-square-o');
				} else {
					$('li[data-parent-group-id=' + groupId + ']').show();
					$icon.addClass('fa-minus-square-o');
					$icon.removeClass('fa-plus-square-o');
				}
				$('li.list-element').css('background-color', '#FFFFFF');
				$('li.list-element:visible:even').css('background-color', '#F9F9F9');
			}
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		module.init = done => {
			// initialization logic
			$('input.permission-checkbox').on('change', _updatePermissionGranting);
			$('input.all-permission-checkbox').on('change', _updateAllPermissionGranting);
			$('span.list-element-collapse-handler').on('click', _collapsePermissionSubGroups);
			_setAllPermissionGrantingSwitcherValues();
			
			done();
		}
		
		return module;
	}
);