gx.controllers.module(
    'overview',

    // controller libraries
	['xhr', gx.source + '/libs/info_messages', 'modal', `${gx.source}/libs/info_box`],

    // controller business logic
    function (data) {
        'use strict';

        /**
         * Module Selector.
         *
         * @type {jQuery}
         */
        const $this = $(this);

        /**
         * Manufacturer creation modal.
         *
         * @type {*}
         */
        const $creationModal = $this.find('.creation-modal');

        /**
         * Manufacturer remove confirmation modal.
         *
         * @type {*}
         */
        const $removeConfirmationModal = $this.find('.remove-confirmation-modal');

        /**
         * Manufacturer edit modal.
         *
         * @type {*}
         */
        const $editModal = $this.find('.edit-modal');

        /**
         * Ajax object.
         *
         * @type {object}
         */
        const ajax = jse.libs.xhr;

        /**
         * Info box Object.
         *
         * @type {object}
         */
        const infoBox = jse.libs.info_box;

        /**
         * Language object.
         *
         * @type {object}
         */
        const lang = jse.core.lang;

        /**
         * Default options for controller,
         *
         * @type {object}
         */
        const defaults = {};

        /**
         * Final controller options.
         *
         * @type {object}
         */
        const options = $.extend(true, {}, defaults, data);

        /**
         * Module object.
         *
         * @type {{}}
         */
        const module = {};

        /**
         * Initializes the remove confirmation modal.
         *
         * @param eventObject Event object to fetch targets id.
         */
        const _initRemoveConfirmationModal = eventObject => {
            ajax.get({
                url: './admin.php?do=ManufacturerAjax/getById&id=' + eventObject.target.dataset.id
            }).done(response => {
                $removeConfirmationModal.modal('show');
                _renderRemoveConfirmationModal(response);
            });
        };

        /**
         * Initializes the edit modal.
         *
         * @param eventObject Event object to fetch targets id.
         */
        const _initEditModal = eventObject => {
            ajax.get({
                url: './admin.php?do=ManufacturerAjax/getById&id=' + eventObject.target.dataset.id
            }).done(response => {
                $editModal.modal('show');
                _renderEditModal(response);
                if (response.image !== '') {
                    _setImagePreview(response);
                }
            });
        };

        /**
         * Initializes the editing of an Manufacturer.
         *
         * @private
         */
        const _editManufacturer = () => {
            if (_validateNameInput($editModal)) {
                _updateData();
            }
        };

        /**
         * Initializes the creation of an Manufacturer.
         *
         * @private
         */
        const _createManufacturer = () => {
            if (_validateNameInput($creationModal)) {
                _storeData();
            }
        };

        /**
         * Initializes the cleaning process from create Modal.
         *
         * @private
         */
        const _cleanupCreationModal = () => {
            _truncateModalFields($creationModal);
            _truncateModalFieldClasses($creationModal);
            _resetModalMessages($creationModal);
        };

        /**
         * Initializes the cleaning process from edit Modal.
         *
         * @private
         */
        const _cleanupEditModal = () => {
            _removeWarning();
            _truncateModalFields($editModal);
            _truncateModalFieldClasses($editModal);
            _resetModalMessages($editModal);
            _removeImagePreview();
            _removeImageCheckboxChecked();
            _removeImageCheckboxText();
            _removeImageCheckbox();
        };

        /**
         * Sends an ajax request to store a new manufacturer entity.
         *
         * @private
         */
        const _storeData = () => {
            ajax.post({
                url: './admin.php?do=ManufacturerAjax/save',
                data: _createInputData('create'),
                processData: false,
                contentType: false
            }).then(response => {
                if (response.success) {
                    _renderTable();
                    $creationModal.modal('hide');

                    if (response.renamed) {
                        _createImageRenameInfo();
                    } else {
                        infoBox.addSuccessMessage(lang.translate('TEXT_SAVE_SUCCESS', 'manufacturers'));
                    }
                }
            });
        };

        /**
         * Sends an ajax request to edit an manufacturer entity.
         *
         * @private
         */
        const _updateData = () => {
            ajax.post({
                url: './admin.php?do=ManufacturerAjax/update',
                data: _createInputData('edit'),
                processData: false,
                contentType: false
            }).then(response => {
                if (response.success) {
                    _renderTable();
                    $editModal.modal('hide');

                    $creationModal.modal('hide');
                    if (response.renamed) {
                        _createImageRenameInfo();
                    } else {
                        infoBox.addSuccessMessage(lang.translate('TEXT_EDIT_SUCCESS', 'manufacturers'));
                    }
                }
            })
        };

        /**
         * Creates an info box with renamed image info.
         *
         * @private
         */
        const _createImageRenameInfo = () => {
            infoBox.addMessage({
                source: 'ajax',
                status: 'new',
                type: 'info',
                customerId: 1,
                visibility: 'removable',
                headline: lang.translate('TEXT_MANUFACTURERS_IMAGE', 'manufacturers'),
                message: lang.translate('TEXT_RENAMED_IMAGE', 'manufacturers'),
            }).done(() => $('.info-box').trigger('refresh:messages'));
        };

        /**
         * Sends an ajax request to remove an manufacturer entity.
         *
         * @private
         */
        const _removeManufacturer = () => {
            ajax.post({
                url: './admin.php?do=ManufacturerAjax/remove',
                data: {
                    id: $('.manufacturer-remove-id').val()
                }
            }).then(response => {
                if (response.success) {
                    _renderTable();
                    $removeConfirmationModal.modal('hide');
                    infoBox.addSuccessMessage(lang.translate('TEXT_INFO_DELETE_SUCCESS', 'manufacturers'));
                }
            });
        };

        /**
         * Returns true if the name input not empty and false if empty.
         *
         * @param modal
         * @returns {boolean}
         * @private
         */
        const _validateNameInput = modal => {
            const $nameInput = modal.find('input[name="manufacturer_name"]');

            if ($nameInput.val() === '') {
                $nameInput.parent().addClass('has-error');
                modal.find('p.manufacturer-modal-info')
                    .first()
                    .text(lang.translate('ERROR_MISSING_NAME', 'manufacturers'))
                    .addClass('text-danger');
                $('#input-manufacturers-name').focus();
                return false;
            }
            return true;
        };

        /**
         * Removes the has-error class from parent name input in modal.
         *
         * @param modal
         * @private
         */
        const _truncateModalFieldClasses = (modal) => {
            const $nameInput = modal.find('input[name="manufacturer_name"]');
            $nameInput.parent().removeClass('has-error');
        };

        /**
         * Removes the image delete checkbox text if one exists.
         *
         * @private
         */
        const _removeImageCheckboxText = () => {
            if ($editModal.find('.checkbox-info-text')) {
                $editModal.find('.checkbox-info-text').remove('.checkbox-info-text');
            }
        };

        /**
         * Removes the image delete checkbox text if one exists.
         *
         * @private
         */
        const _removeImageCheckboxChecked = () => {
            if ($editModal.find('.delete-image-checkbox').is(':checked')) {
                $editModal.find('.delete-image-checkbox').removeAttr('checked');
            }
        };
        /**
         * Removes the image delete checkbox text if one exists.
         *
         * @private
         */
        const _removeImagePreview = () => {
            if ($editModal.find('.manufacturer-image')) {
                $editModal.find('.manufacturer-image').remove();
            }
        };

        /**
         * Set the Checkbox type to hidden.
         *
         * @private
         */
        const _removeImageCheckbox = () => {
            if ($editModal.find('.single-checkbox')) {
                $editModal.find('.single-checkbox').remove();
            }
        };

        /**
         * Resets the info message from modal.
         *
         * @param modal
         * @private
         */
        const _resetModalMessages = (modal) => {
            modal.find('p.manufacturer-modal-info')
                .first()
                .text(lang.translate('TEXT_NEW_INTRO', 'manufacturers')).removeClass('text-danger');
        };

        /**
         * Truncate all input values from modal.
         *
         * @param modal
         * @private
         */
        const _truncateModalFields = (modal) => {
            modal.find('input').each((key, value) => {
                value.value = '';
            });
        };

        /**
         * Renders overview table , to see changes immediately.
         *
         * @private
         */
        const _renderTable = () => {
            ajax.get({
                url: './admin.php?do=ManufacturerAjax/getData'
            }).done(response => {
                const $body = $('.manufacturer-table tbody');
                $body.empty();

                for (let i = 0; i < response.length; i++) {
                    const $row = $('<tr/>');
                    const $nameColumn = $('<td/>', {
                        'text': response[i].name
                    });
                    const $actionsContainer = $('<div/>', {
                        'class': 'pull-right action-list visible-on-hover'
                    });
                    const $edit = $('<i/>', {
                        'data-id': response[i].id,
                        'data-toggle': 'modal',
                        'data-target': '.edit-modal',
                        'class': 'fa fa-pencil edit',
                    });
                    const $delete = $('<i/>', {
                        'data-id': response[i].id,
                        'data-toggle': 'modal',
                        'data-target': '.remove-confirmation-modal',
                        'class': 'fa fa-trash-o delete'
                    });
                    const $actionsCol = $('<td/>', {
                        'class': 'actions'
                    });

                    $actionsContainer.append($edit).append($delete).appendTo($actionsCol);
                    $row.append($nameColumn).append($actionsCol).appendTo($body);
                }
                $this.find('.delete').on('click', _initRemoveConfirmationModal);
                $this.find('.edit').on('click', _initEditModal);
            });
        };

        /**
         * Renders remove confirmation modal with given data.
         *
         * @param response
         * @private
         */
        const _renderRemoveConfirmationModal = (response) => {
            const $info = $('.manufacturer-modal-remove-info');
            const $name = lang.translate('TEXT_MANUFACTURERS', 'manufacturers') + response.name
                + '<br><br>';

            $info.empty();
            $('.manufacturer-remove-id').val(response.id);
            $info.append($name);
        };

        /**
         * Renders edit modal with given data.
         *
         * @param response
         * @private
         */
        const _renderEditModal = response => {
            $editModal.find('.manufacturer-id').val(response.id.toString());
            $editModal.find('#input-manufacturers-name').val(response.name);

            for (let languageCode in response.urls) {
                $editModal.find('.manufacturer-' + languageCode).val(response.urls[languageCode]);
            }

            if ($editModal.find('input[name="manufacturer_logo"]').attr('type') === 'text') {
                $editModal.find('input[name="manufacturer_logo"]').val(response.image);
            }

            if (response.image !== '') {
                _setDeleteImageCheckbox()
            }

            $editModal.find('.manufacturers-img-path').text(response.image);
            $editModal.find('.manufacturer-date-added')
                .text('')
                .append(response.dateAdded);

            $editModal.find('.manufacturer-last-modified')
                .text('')
                .append(response.lastModified);
        };

        /**
         * Sets an image preview in an manufacturer-image-container.
         *
         * @param response {{
         *                   imagePath : string
         *                   name      : string
         *                   image     : string
         *                 }}
         * @private
         */
        const _setImagePreview = (response) => {
            const $manufacturerImage = $('<img/>', {
                'class': 'manufacturer-image ',
                'src': response.imagePath,
                'alt': response.name,
                'title': response.image
            });
            $editModal.find('.manufacturer-image-container').append($manufacturerImage);
        };

        /**
         * Sets the image delete Checkbox input.
         *
         * @private
         */
        const _setDeleteImageCheckbox = () => {
            const $checkboxText = $('<span/>', {
                'class': 'checkbox-info-text',
                'text': ' ' + lang.translate('TEXT_DELETE_IMAGE', 'manufacturers')
            });
            const $checkboxInput = $('<input/>', {
                'class': 'delete-image-checkbox',
                'type': 'checkbox',
                'title': lang.translate('TEXT_DELETE_IMAGE', 'manufacturers')
            });

            if ($editModal.find('.single-checkbox')) {
                $editModal.find('.single-checkbox').remove();

            }
            $editModal.find('.manufacturer-image-data')
                .append($checkboxInput)
                .append($checkboxText)
                .attr('data-gx-widget', 'single_checkbox');

            gx.widgets.init($editModal);
        };

        /**
         * Removes the warning text.
         *
         * @private
         */
        const _removeWarning = () => {
            $editModal.find('p.manufacturer-modal-info')
                .text('')
                .removeClass('text-danger');
        };


        /**
         * Creates and return data object from formula information's.
         *
         * @param modalType
         * @returns {{}}
         * @private
         */
        const _createInputData = modalType => {
            const $modal = modalType === 'edit' ? $editModal : $creationModal;
            const $inputs = $modal.find('input[type="text"]');
            const $id = $modal.find('input[name="id"]').val();
            const $img = $modal.find('input[name="manufacturer_logo"]');
            const $imgPath = $img.val();
            const isFileManagerInput = $modal.find('.responsive-file-manager').length !== 0;
            const data = new FormData(document.querySelector('.manufacturer-form'));

            data.append('manufacturer_file', !isFileManagerInput);

            for (let i = 0; i < $inputs.length; i++) {
                data.append($inputs[i].getAttribute('name'), $inputs[i].value);
            }

            if (undefined !== $id) {
                data.append('id', $id);
            }

            if (!isFileManagerInput && undefined !== $img) {
                const file = $img[0].files[0] === null ? $imgPath : $img[0].files[0];
                if (file) {
                    data.append('manufacturer_logo', file);
                }
            }

            if ($modal === $editModal) {
                data.append('manufacturer_checkbox', $editModal.find('.delete-image-checkbox').is(':checked'));

                if (data['manufacturer_logo'] === '' && $imgPath !== '') {
                    data.append('manufacturer_logo', $imgPath);
                }
            }

            return data;
        };

        // event handler

        // initialization
        module.init = done => {
            // initialization logic

            $this.find('.delete').on('click', _initRemoveConfirmationModal);
            $creationModal.find('.btn-primary').on('click', _createManufacturer);
            $removeConfirmationModal.find('.btn-danger').on('click', _removeManufacturer);
            $editModal.find('.btn-primary').on('click', _editManufacturer);
            $this.find('.edit').on('click', _initEditModal);

            //actions
            $creationModal.on('hide.bs.modal', _cleanupCreationModal);
            $editModal.on('hide.bs.modal', _cleanupEditModal);

            done();
        };
        return module;
    });
