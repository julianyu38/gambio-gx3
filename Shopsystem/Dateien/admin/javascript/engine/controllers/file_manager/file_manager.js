/* --------------------------------------------------------------
 file_manager.js 2017-03-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 
 based on:
 (c) 2013 John Campbell (jcampbell1) - Simple PHP File Manager
 
 Released under the MIT License (MIT)
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 --------------------------------------------------------------
 */

gx.controllers.module(
	'file_manager',
	
	[
		`${jse.source}/vendor/qtip2/jquery.qtip.css`,
		`${jse.source}/vendor/qtip2/jquery.qtip.js`,
		'modal',
	],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Default Options
		 *
		 * @type {Object}
		 */
		const defaults = {};
		
		/**
		 * Final Options
		 *
		 * @type {Object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module Object
		 *
		 * @type {Object}
		 */
		const module = {};
		
		/**
		 * @type int
		 */
		const XSRF = (document.cookie.match('(^|; )_sfm_xsrf=([^;]*)') || 0)[2];
		
		/**
		 * @type {string}
		 */
		const url = jse.core.config.get('appUrl') + '/admin/admin.php';
		
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		function _tableSorter() {
			const $table = this;
			this.find('th:not(.no_sort)').click(function() {
				const idx = $(this).index();
				const direction = $(this).hasClass('sort_asc');
				$table.tablesortby(idx, direction);
			});
			return this;
		}
		
		function _tableSortBy(idx, direction) {
			const $rows = this.find('tbody tr');
			
			function elementToVal(a) {
				const $a_elem = $(a).find('td:nth-child(' + (idx + 1) + ')');
				const a_val = $a_elem.attr('data-sort') || $a_elem.text();
				return (a_val == parseInt(a_val) ? parseInt(a_val) : a_val);
			}
			
			$rows.sort(function(a, b) {
				const a_val = elementToVal(a), b_val = elementToVal(b);
				return (a_val > b_val ? 1 : (a_val == b_val ? 0 : -1)) * (direction ? 1 : -1);
			});
			this.find('th').removeClass('sort_asc sort_desc');
			$(this).find('thead th:nth-child(' + (idx + 1) + ')').addClass(direction ? 'sort_desc' : 'sort_asc');
			for (let i = 0; i < $rows.length; i++) {
				this.append($rows[i]);
			}
			this.settablesortmarkers();
			return this;
		}
		
		function _tableReSort() {
			const $e = this.find('thead th.sort_asc, thead th.sort_desc');
			if ($e.length) {
				this.tablesortby($e.index(), $e.hasClass('sort_desc'));
			}
			
			return this;
		}
		
		function _setTableSortMarkers() {
			this.find('thead th span.indicator').remove();
			this.find('thead th.sort_asc').append('<span class="indicator"><i class="fa fa-caret-up"></i><span>');
			this.find('thead th.sort_desc').append('<span class="indicator"><i class="fa fa-caret-down"></i><span>');
			return this;
		}
		
		function _delete(data) {
			const modalTitle = jse.core.lang.translate('CONFIRM_DELETE_TITLE', 'file_manager');
			const modalMessage = jse.core.lang.translate('CONFIRM_DELETE_TEXT', 'file_manager');
			const modalButtons = [
				{
					title: jse.core.lang.translate('yes', 'buttons'),
					callback: event => {
						closeModal(event);
						$.post(url + '?do=FileManager/Delete&content=' + $this.data('content'), {
							file: $(this).attr('data-file'),
							xsrf: XSRF
						}, function(response) {
							_list();
						}, 'json');
						return false;
					}
				},
				{
					title: jse.core.lang.translate('no', 'buttons'),
					callback: closeModal
				}
			];
			
			function closeModal(event) {
				$(event.target).parents('.modal').modal('hide');
			}
			
			jse.libs.modal.showMessage(modalTitle, modalMessage, modalButtons)
		}
		
		function _mkdir(e) {
			let hashval = decodeURIComponent(window.location.hash.substr(1)),
				$dir = $('#dirname');
			e.preventDefault();
			$dir.val().length && $.post(url + '?do=FileManager/Mkdir&content=' + $this.data('content') + '&file='
				+ hashval, {
				name: $dir.val(),
				xsrf: XSRF
			}, function(data) {
				_list();
			}, 'json');
			$dir.val('');
			return false;
		}
		
		function _uploadFile(file) {
			const folder = decodeURIComponent(window.location.hash.substr(1)),
				upload_progress = $('#upload_progress');
			
			if (file.size > $this.data('maxUploadSize')) {
				const $error_row = _renderFileSizeErrorRow(file, folder);
				upload_progress.append($error_row);
				window.setTimeout(function() {
					$error_row.fadeOut();
				}, 5000);
				return false;
			}
			
			let $row = _renderFileUploadRow(file, folder);
			upload_progress.append($row);
			let fd = new FormData();
			fd.append('file_data', file);
			fd.append('file', folder);
			fd.append('xsrf', XSRF);
			let xhr = new XMLHttpRequest();
			xhr.open('POST', url + '?do=FileManager/Upload&content=' + $this.data('content') + '&directory=' + folder);
			xhr.onload = function() {
				$row.remove();
				_list();
			};
			xhr.upload.onprogress = function(e) {
				if (e.lengthComputable) {
					$row.find('.progress').css('width', (e.loaded / e.total * 100 | 0) + '%');
				}
			};
			xhr.send(fd);
		}
		
		function _renderFileUploadRow(file, folder) {
			return $('<div/>')
				.append($('<span class="fileuploadname" />').text((folder ? folder + '/' : '') + file.name))
				.append($('<div class="progress_track"><div class="progress"></div></div>'))
				.append($('<span class="size" />').text(_formatFileSize(file.size)))
		}
		
		function _renderFileSizeErrorRow(file, folder) {
			return $('<div class="error" />')
				.append($('<span class="fileuploadname" />')
					.text('Error: ' + (folder ? folder + '/' : '') + file.name))
				.append($('<span/>').html(' file size - <b>' + _formatFileSize(file.size) + '</b>'
					+ ' exceeds max upload size of <b>' + _formatFileSize($this.data('maxUploadSize')) + '</b>'));
		}
		
		function _fileUploadStuff() {
			$('#file_drop_target').bind('dragover', function() {
				$(this).addClass('drag_over');
				return false;
			}).bind('dragend', function() {
				$(this).removeClass('drag_over');
				return false;
			}).bind('drop', function(e) {
				e.preventDefault();
				const files = e.originalEvent.dataTransfer.files;
				$.each(files, function(k, file) {
					_uploadFile(file);
				});
				$(this).removeClass('drag_over');
			});
			$('input[type=file]').change(function(e) {
				e.preventDefault();
				$.each(this.files, function(k, file) {
					_uploadFile(file);
				});
			});
		}
		
		function _list() {
			const hashval = decodeURIComponent(window.location.hash.substr(1)),
				$list = $('#list'),
				$body = $('body');
			$.get(url, {
				'do': 'FileManager/List',
				'content': $this.data('content'),
				'file': hashval
			}, function(data) {
				$list.empty();
				$('#breadcrumb').empty().html(_renderBreadcrumbs(hashval));
				if (data.success) {
					if (hashval !== '') {
						$list.append(_renderFileRow({
							mtime: '',
							name: '..',
							path: hashval.substring(0, hashval.lastIndexOf('/')),
							is_dir: true,
							is_deleteable: false,
							is_readable: true,
							is_writable: true,
							is_executable: true,
							info_message: ''
						}));
					}
					
					$.each(data.results, function(k, v) {
						$list.append(_renderFileRow(v));
					});
					!data.results.length
					&& $list.append('<tr><td class="empty" colspan="5">'
						+ jse.core.lang.translate('EMPTY_DIRECTORY', 'file_manager') + '</td></tr>')
					data.is_writable ? $body.removeClass('no_write') : $body.addClass('no_write');
				} else {
					console.warn(data.error.msg);
				}
				$('.delete').on('click', _delete);
				$('#table').retablesort();
			}, 'json');
		}
		
		function _renderFileRow(data) {
			const data_path = data.path.indexOf('/') === 0 ? data.path.substring(1) : data.path,
				data_type = data_path.substring(data_path.lastIndexOf('.') + 1),
				image_file_extensions = ['jpg', 'jpeg', 'gif', 'png', 'bmp'],
				is_image = image_file_extensions.indexOf(data_type) > -1,
				thumb = is_image ? url + '?do=FileManager/Thumb&file=' + encodeURIComponent(data_path) + '&content='
				+ $this.data('content') : '',
				icon_src = is_image ? thumb : 'data:image/png;base64,'
				+ (data.is_dir ? 'iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAI0SURBVFiF7Vctb1RRED1nZu5977VQVBEQBKZ1GCDBEwy+ISgCBsMPwOH4CUXgsKQOAxq5CaKChEBqShNK222327f79n0MgpRQ2qC2twKOGjE352TO3Jl76e44S8iZsgOww+Dhi/V3nePOsQRFv679/qsnV96ehgAeWvBged3vXi+OJewMW/Q+T8YCLr18fPnNqQq4fS0/MWlQdviwVqNpp9Mvs7l8Wn50aRH4zQIAqOruxANZAG4thKmQA8D7j5OFw/iIgLXvo6mR/B36K+LNp71vVd1cTMR8BFmwTesc88/uLQ5FKO4+k4aarbuPnq98mbdo2q70hmU0VREkEeCOtqrbMprmFqM1psoYAsg0U9EBtB0YozUWzWpVZQgBxMm3YPoCiLpxRrPaYrBKRSUL5qn2AgFU0koMVlkMOo6G2SIymQCAGE/AGHRsWbCRKc8VmaBN4wBIwkZkFmxkWZDSFCwyommZSABgCmZBSsuiHahA8kA2iZYzSapAsmgHlgfdVyGLTFg3iZqQhAqZB923GGUgQhYRVElmAUXIGGVgedQ9AJJnAkqyClCEkkfdM1Pt13VHdxDpnof0jgxB+mYqO5PaCSDRIAbgDgdpKjtmwm13irsnq4ATdKeYcNvUZAt0dg5NVwEQFKrJlpn45lwh/LpbWdela4K5QsXEN61tytWr81l5YSY/n4wdQH84qjd2J6vEz+W0BOAGgLlE/AMAPQCv6e4gmWYC/QF3d/7zf8P/An4AWL/T1+B2nyIAAAAASUVORK5CYII=' : 'iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABAklEQVRIie2UMW6DMBSG/4cYkJClIhauwMgx8CnSC9EjJKcwd2HGYmAwEoMREtClEJxYakmcoWq/yX623veebZmWZcFKWZbXyTHeOeeXfWDN69/uzPP8x1mVUmiaBlLKsxACAC6cc2OPd7zYK1EUYRgGZFkG3/fPAE5fIjcCAJimCXEcGxKnAiICERkSIcQmeVoQhiHatoWUEkopJEkCAB/r+t0lHyVN023c9z201qiq6s2ZYA9jDIwx1HW9xZ4+Ihta69cK9vwLvsX6ivYf4FGIyJj/rg5uqwccd2Ar7OUdOL/kPyKY5/mhZJ53/2asgiAIHhLYMARd16EoCozj6EzwCYrrX5dC9FQIAAAAAElFTkSuQmCC'),
				link_href = data.is_dir ? _rebuildUrl() + '#'
				+ encodeURIComponent(data_path) : jse.core.config.get('appUrl') + '/' + $this.data('subDirectory')
				            + data_path;
			const $link = $('<a class="name"/>')
					.attr('href', link_href)
					.attr('target', (!data.is_dir && !allow_direct_link)? '_blank' : '')
					.text(data.name),
				allow_direct_link = $this.data('allowDirectLink'),
				$dl_icon = $('<i/>').addClass('fa fa-download'),
				$dl_link = $('<a/>')
					.attr('href', url + '?do=FileManager/Download&content=' + $this.data('content') + '&file='
						+ encodeURIComponent(data_path))
					.attr('target', (!data.is_dir && !allow_direct_link)? '_blank' : '')
					.addClass('download')
					.append($dl_icon),
				$delete_icon = $('<i/>').addClass('fa fa-trash-o'),
				$delete_link = $('<a/>')
					.attr('data-file', data_path)
					.addClass('delete')
					.append($delete_icon),
				tooltip_options = {
					style: {
						classes: 'gx-container gx-qtip info large'
					},
					position: {
						my: 'right bottom',
						at: 'left top'
					}
				},
				$info_icon = $('<i/>').addClass('fa fa-info-circle'),
				$info_text = $('<span/>').attr('title', (data.info_message !== '' ? jse.core.lang.translate(data.info_message, 'file_manager') : ''))
					.append($info_icon).qtip(tooltip_options),
				
				$thumb = $('<img style="vertical-align:middle"/>')
					.attr('src', icon_src),
				
				$actions = $('<div/>')
					.addClass('pull-right' + data.info_message ? '' : ' visible-on-hover')
					.append($dl_link)
					.append(data.is_deleteable ? $delete_link : '')
					.append(data.info_message ? $info_text : '');
			
			
			
			if (!data.is_dir && !allow_direct_link) {
				$link.css('pointer-events', 'none');
			}
			
			return $('<tr />')
				.addClass(data.is_dir ? 'is_dir' : '')
				.append($('<td class="first" />').append($('<a />').attr('href', link_href).append($thumb)))
				.append($('<td class="name" />').append($link))
				.append($('<td/>').attr('data-sort', data.is_dir ? -1 : data.size)
					.html($('<span class="size" />').text(_formatFileSize(data.size))))
				.append($('<td/>').attr('data-sort', data.mtime).text(data.mtime !== '' ? _formatTimestamp(data.mtime) : ''))
				.append($('<td/>').addClass('actions').addClass('action-list').append($actions));
		}
		
		function _renderBreadcrumbs(path) {
			let base = '';
			const data_path = $this.data('subDirectory').substring(0, $this.data('subDirectory').length - 1);
			const $html = $('<div/>').append($('<a href="' + _rebuildUrl() + '#">' + data_path + '</a></div>'));
			$.each(path.split('/'), function(k, v) {
				if (v) {
					$html.append($('<span/>').text(' ▸ '))
						.append($('<a/>').attr('href', _rebuildUrl() + '#' + encodeURIComponent(base + v)).text(v));
					base += v + '/';
				}
			});
			return $html;
		}
		
		function _rebuildUrl() {
			return url + '?do=FileManager&content=' + $this.data('content');
		}
		
		function _formatTimestamp(unix_timestamp) {
			const m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
				d = new Date(unix_timestamp * 1000);
			return [
				m[d.getMonth()], ' ', d.getDate(), ', ', d.getFullYear(), " ",
				(d.getHours() % 12 || 12), ":", (d.getMinutes() < 10 ? '0' : '') + d.getMinutes(),
				" ", d.getHours() >= 12 ? 'PM' : 'AM'
			].join('');
		}
		
		function _formatFileSize(bytes) {
			const s = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'];
			let pos = 0;
			for (; bytes >= 1000; pos++, bytes /= 1024) {
				;
			}
			const d = Math.round(bytes * 10);
			return pos ? [parseInt(d / 10), ".", d % 10, " ", s[pos]].join('') : bytes + ' bytes';
		}
		
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$.data($this, 'content', ($this.data('content')||'images'));
			$.fn.tablesorter = _tableSorter;
			$.fn.tablesortby = _tableSortBy;
			$.fn.retablesort = _tableReSort;
			$.fn.settablesortmarkers = _setTableSortMarkers;
			
			$(window).on('hashchange', _list).trigger('hashchange');
			$('#table').tablesorter();
			$('#do_mkdir').on('click', _mkdir);
			$('#mkdir').on('submit', _mkdir);
			
			// file upload stuff
			if ($this.data('allowUpload')) {
				_fileUploadStuff();
			}
			
			done();
		};
		
		return module;
	}
);