/* --------------------------------------------------------------
 init.js 2017-03-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * QuickEdit Table Controller
 *
 * This controller initializes the main QuickEdit table with a new jQuery DataTables instance.
 */
gx.controllers.module(
	'init',
	
	[
		`${jse.source}/vendor/datatables/jquery.dataTables.min.css`,
		`${jse.source}/vendor/datatables/jquery.dataTables.min.js`,
		'datatable',
		`${jse.source}/vendor/jquery-deparam/jquery-deparam.min.js`,
		`${gx.source}/libs/quick_edit_overview_columns`
	],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		/**
		 * Special Price Modal
		 *
		 * @type {jQuery}
		 */
		const $specialPriceModal = $this.parents('.quick-edit.overview').find('.special-prices.modal');
		
		/**
		 * Search Parameters
		 *
		 * @type {Object}
		 */
		const parameters = $.deparam(window.location.search.slice(1));
		
		/**
		 * DataTable Columns
		 *
		 * @type {Object[]}
		 */
		const columns = jse.libs.datatable.prepareColumns($this, jse.libs.quick_edit_overview_columns, data.activeColumns);
		
		/**
		 * DataTable Options
		 *
		 * @type {Object}
		 */
		const options = {
			autoWidth: false,
			dom: 't',
			pageLength: parseInt(parameters.length || data.pageLength),
			displayStart: parseInt(parameters.page) ? (parseInt(parameters.page) - 1) * 25 : 0,
			serverSide: true,
			language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
			ajax: {
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/DataTable',
				type: 'POST',
				data: {
					pageToken: jse.core.config.get('pageToken')
				}
			},
			orderCellsTop: true,
			order: _getOrder(parameters, columns),
			searchCols: _getSearchCols(parameters, columns),
			columns: columns,
		};
		
		let dataTable; 
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Get Initial Table Order
		 *
		 * @param {Object} parameters Contains the URL parameters.
		 * @param {Object} columns Contains the column definitions.
		 *
		 * @return {Object[]} Returns the ordered column definitions.
		 */
		function _getOrder(parameters, columns) {
			let index = 1; // Order by first column by default.
			let direction = 'asc'; // Order ASC by default.
			let columnName = 'name'; // Order by products name by default.
			
			$this.on('click', 'th', function() {
				columnName = $(this).data('column-name');
				index = dataTable.column(this).index();
			});
			
			// Apply initial table sort.
			if (parameters.sort) {
				direction = parameters.sort.charAt(0) === '-' ? 'desc' : 'asc';
				const columnName = parameters.sort.slice(1);
				
				for (let column of columns) {
					if (column.name === columnName) {
						index = columns.indexOf(column);
						break;
					}
				}
			} else if (data.activeColumns.indexOf('name') > -1) { // Order by name if possible.
				index = data.activeColumns.indexOf('name');
			}
			
			return [[index, direction]];
		}
		
		/**
		 * Get Initial Search Cols
		 *
		 * @param {Object} parameters Contains the URL parameters.
		 * @param {Object} columns Contains the column definitions.
		 *
		 * @returns {Object[]} Returns the initial filtering values.
		 */
		function _getSearchCols(parameters, columns) {
			if (!parameters.filter) {
				return [];
			}
			
			const searchCols = [];
			
			for (let column of columns) {
				let entry = null;
				let value = parameters.filter[column.name];
				
				if (value) {
					entry = {search: value};
				}
				
				searchCols.push(entry);
			}
			
			return searchCols;
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$specialPriceModal
				.on('hide.bs.modal', function() {
					$this.DataTable().ajax.reload();
				});
			
			$this.on('draw.dt', () => {
				$this.find('thead input:checkbox')
					.prop('checked', false)
					.trigger('change', [false]);
				$this.find('tbody').attr('data-gx-widget', 'single_checkbox switcher');
				$this.find('tbody').attr('data-single_checkbox-selector', '.overview-row-selection');
				$this.find('tbody').attr('data-switcher-selector', '.convert-to-switcher');
				
				gx.widgets.init($this);
			});
			
			dataTable = jse.libs.datatable.create($this, options);
			
			done();
		};
		
		return module;
	});