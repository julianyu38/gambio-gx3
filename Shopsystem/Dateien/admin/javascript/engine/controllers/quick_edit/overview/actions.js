/* --------------------------------------------------------------
 actions.js 2017-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Actions
 *
 * This module creates the bulk and row actions for the table.
 */
gx.controllers.module(
	'actions',
	
	['user_configuration_service', `${gx.source}/libs/button_dropdown`],
	
	function() {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Create Bulk Actions
		 *
		 * This callback can be called once during the initialization of this module.
		 */
		function _createBulkActions() {
			// Add actions to the bulk-action dropdown.
			const $bulkActions = $('.overview-bulk-action');
			const defaultBulkAction = $this.data('defaultBulkAction') || 'bulk-row-edit';
			
			// Edit
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
				class: 'bulk-row-edit',
				data: {configurationValue: 'bulk-row-edit'},
				isDefault: defaultBulkAction === 'bulk-row-edit' || defaultBulkAction === 'save-bulk-row-edits',
				callback: e => e.preventDefault()
			});
			
			// Save
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
				class: 'save-bulk-row-edits hidden',
				data: {configurationValue: 'save-bulk-row-edits'},
				isDefault: false,  // "Save" must not be shown as a default value. 
				callback: e => e.preventDefault()
			});
			
			// Properties
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_PROPERTIES', 'admin_quick_edit'),
				class: 'bulk-properties-edit',
				data: {configurationValue: 'bulk-properties-edit'},
				isDefault: defaultBulkAction === 'bulk-properties-edit',
				callback: e => e.preventDefault()
			});
			
			// Special Price
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_SPECIAL_PRICE', 'admin_quick_edit'),
				class: 'bulk-special-price-edit',
				data: {configurationValue: 'bulk-special-price-edit'},
				isDefault: defaultBulkAction === 'bulk-special-price-edit',
				callback: e => e.preventDefault()
			});
			
			$this.parents('.quick-edit.overview')
				.find('a.bulk-properties-edit')
				.attr('data-toggle', 'modal')
				.attr('data-target', '.properties.modal');
			
			$this.parents('.quick-edit.overview')
				.find('a.bulk-special-price-edit')
				.attr('data-toggle', 'modal')
				.attr('data-target', '.special-prices.modal');
			
			$this.datatable_default_actions('ensure', 'bulk');
		}
		
		/**
		 * Create Table Row Actions
		 *
		 * This function must be call with every table draw.dt event.
		 */
		function _createRowActions() {
			// Re-create the checkbox widgets and the row actions. 
			const defaultRowAction = $this.data('defaultRowAction') || 'show-product';
			
			$this.find('.btn-group.dropdown').each(function() {
				const productId = $(this).parents('tr').data('id');
				
				// Show Product
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_SHOW', 'admin_quick_edit'),
					href: `${jse.core.config.get('appUrl')}/admin/categories.php?pID=${productId}&action=new_product`,
					class: 'show-product',
					data: {configurationValue: 'show-product'},
					isDefault: defaultRowAction === 'show-product',
					callback: e => e.preventDefault()
				});
				
				// Edit
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
					class: 'row-edit',
					data: {configurationValue: 'row-edit'},
					isDefault: defaultRowAction === 'row-edit' || defaultRowAction === 'save-row-edits',
					callback: e => e.preventDefault()
				});
				
				// Save
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
					class: 'save-row-edits hidden',
					data: {configurationValue: 'save-row-edits'},
					isDefault: false, // "Save" must not be saved as a pre-selected value.
					callback: e => e.preventDefault()
				});
				
				// Properties
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_PROPERTIES', 'admin_quick_edit'),
					class: 'products-properties',
					data: {configurationValue: 'properties'},
					isDefault: defaultRowAction === 'properties',
					callback: e => e.preventDefault()
				});
				
				// Special Price
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_SPECIAL_PRICE', 'admin_quick_edit'),
					class: 'special-price',
					data: {configurationValue: 'special-price'},
					isDefault: defaultRowAction === 'special-price',
					callback: e => e.preventDefault()
				});
				
				// Graduated Prices
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_GRADUATED_PRICES', 'admin_quick_edit'),
					class: 'graduated-prices',
					data: {configurationValue: 'graduated-prices'},
					isDefault: defaultRowAction === 'graduated-prices',
					callback: e => e.preventDefault()
				});
				
				$this.parents('.quick-edit.overview')
					.find('a.products-properties')
					.attr('data-toggle', 'modal')
					.attr('data-target', '.properties.modal');
				
				$this.parents('.quick-edit.overview')
					.find('a.special-price')
					.attr('data-toggle', 'modal')
					.attr('data-target', '.special-prices.modal');
				
				$this.datatable_default_actions('ensure', 'row');
			});
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$(window).on('JSENGINE_INIT_FINISHED', () => {
				$this.on('draw.dt', _createRowActions);
				_createRowActions();
				_createBulkActions();
			});
			
			done();
		};
		
		return module;
		
	}); 