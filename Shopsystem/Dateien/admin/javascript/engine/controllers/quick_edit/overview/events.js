/* --------------------------------------------------------------
 events.js 2018-04-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Main Table Events
 *
 * Handles the events of the main QuickEdit table.
 */
gx.controllers.module('events',
	[
		'loading_spinner', 'modal', `${gx.source}/libs/button_dropdown`, `${gx.source}/libs/info_box`
	],
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Bulk selection change event handler.
		 *
		 * @param {jQuery.Event} event Contains event information.
		 * @param {Boolean} [propagate = true] Whether to propagate the event or not.
		 */
		function _onBulkSelectionChange(event, propagate = true) {
			if (propagate === false) {
				return;
			}
			
			$this.find('tbody input:checkbox.overview-row-selection')
				.single_checkbox('checked', $(this).prop('checked'))
				.trigger('change');
		}
		
		/**
		 * Table row click event handler.
		 *
		 * @param {jQuery.Event} event Contains event information.
		 */
		function _onTableRowClick(event) {
			if (!$(event.target).is('td')) {
				return;
			}
			
			const $singleCheckbox = $(this).find('input:checkbox.overview-row-selection');
			
			$singleCheckbox.prop('checked', !$(this)
				.find('input:checkbox.overview-row-selection')
				.prop('checked'))
				.trigger('change');
		}
		
		/**
		 * Enables row editing mode.
		 */
		function _onTableRowEditClick() {
			const $tableRow = $(this).parents('tr');
			const $singleCheckbox = $(this).parents('tr').find('input:checkbox.overview-row-selection');
			
			const $dropdown = $tableRow.find('.btn-group.dropdown');
			const $saveAction = $dropdown.find('.save-row-edits');
			const $editAction = $tableRow.find('.row-edit');
			$editAction.addClass('hidden');
			$saveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($dropdown, $saveAction);
			
			$tableRow.find('td.editable').each(function() {
				if ($(this).find('input:text').length > 0) {
					return;
				}
				
				if ($(this).hasClass('manufacturer')) {
					const rowIndex = $(this).parents('tr');
					const rowData = $this.DataTable().row(rowIndex).data();
					const options = rowData.DT_RowData.option.manufacturer;
					
					let html = '';
					
					options.forEach(option => {
						html += `<option value="${option.id}" ${rowData.manufacturer == option.value ? 'selected' : ''}>
							${option.value}
						</option>`;
					});
					
					this.innerHTML = `<select class="form-control">${html}</select>`;
					
				} else {
					this.innerHTML =
						`<input type="text" class="form-control" value="` + this.innerText.replace(/"/g, '&quot;')
						+ `" />`;
				}
				
			});
			
			$singleCheckbox.prop('checked', !$(this)
				.find('input:checkbox.overview-row-selection')
				.prop('checked'))
				.trigger('change');
		}
		
		/**
		 * Show product icon event handler.
		 *
		 * Navigates the browser to the product details page.
		 */
		function _onTableRowShowClick() {
			const productId = $(this).parents('tr').data('id');
			
			const parameters = {
				pID: productId,
				action: 'new_product',
			};
			
			window.open(`${jse.core.config.get('appUrl')}/admin/categories.php?` + $.param(parameters), '_blank');
		}
		
		/**
		 * Bulk row edit event handler.
		 *
		 * Enables the edit mode for the selected rows.
		 */
		function _onTableBulkRowEditClick() {
			const $bulkEditAction = $(this);
			const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.overview-row-selection');
			
			if ($checkedSingleCheckboxes.length) {
				const $bulkActionsDropdown = $('.overview-bulk-action');
				const $bulkSaveAction = $bulkActionsDropdown.find('.save-bulk-row-edits');
				$bulkEditAction.addClass('hidden');
				$bulkSaveAction.removeClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkSaveAction);
			}
			
			$checkedSingleCheckboxes.each(function() {
				const $tableRow = $(this).parents('tr');
				const $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
				const $saveAction = $rowActionsDropdown.find('.save-row-edits');
				const $editAction = $tableRow.find('.row-edit');
				
				$editAction.addClass('hidden');
				$saveAction.removeClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $saveAction);
				
				$tableRow.find('td.editable').each(function() {
					
					if ($(this).find('input:text').length > 0) {
						return;
					}
					
					if ($(this).hasClass('manufacturer')) {
						const rowIndex = $(this).parents('tr');
						const rowData = $this.DataTable().row(rowIndex).data();
						const options = rowData.DT_RowData.option.manufacturer;
						
						let html = '';
						
						options.forEach(option => {
							html +=
								`<option value="${option.id}" ${rowData.manufacturer == option.value ? 'selected' : ''}>
							${option.value}
						</option>`;
						});
						
						this.innerHTML = `<select class="form-control">${html}</select>`;
						
					} else {
						this.innerHTML =
							`<input type="text" class="form-control" value="` + this.innerText.replace(/"/g, '&quot;')
							+ `" />`;
					}
				});
			});
		}
		
		/**
		 * Table row checkbox change event handler.
		 */
		function _onTableRowCheckboxChange() {
			const $bulkActionDropdownButtons = $this.parents('.quick-edit.overview')
				.find('.overview-bulk-action > button');
			const $tableRow = $(this).parents('tr');
			
			if ($this.find('input:checkbox:checked.overview-row-selection').length > 0) {
				$bulkActionDropdownButtons.removeClass('disabled');
			} else {
				$bulkActionDropdownButtons.addClass('disabled');
				
				const $bulkActionsDropdown = $('.overview-bulk-action');
				const $bulkSaveAction = $bulkActionsDropdown.find('.save-bulk-row-edits');
				
				if (!$bulkSaveAction.hasClass('hidden')) {
					const $bulkEditAction = $bulkActionsDropdown.find('.bulk-row-edit');
					$bulkEditAction.removeClass('hidden');
					$bulkSaveAction.addClass('hidden');
					jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkEditAction);
				}
			}
			
			if (!$(this).prop('checked')) {
				_resolveRowChanges($(this).parents('tr'));
				
				const $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
				const $saveAction = $rowActionsDropdown.find('.save-row-edits');
				
				if (!$saveAction.hasClass('hidden')) {
					const $editAction = $tableRow.find('.row-edit');
					$editAction.removeClass('hidden');
					$saveAction.addClass('hidden');
					jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $editAction.last());
				}
			}
		}
		
		/**
		 * Page mode change between "edit" and "filtering".
		 */
		function _onPageModeChange() {
			if ($(this).val() == 'edit-mode') {
				$this.find('tr.filter').attr('hidden', true);
				$this.find('tr.edit').attr('hidden', false);
				$this.find('thead tr:first-child th').addClass('edit-mode');
			} else {
				$this.find('tr.filter').attr('hidden', false);
				$this.find('tr.edit').attr('hidden', true);
				$this.find('thead tr:first-child th').removeClass('edit-mode');
			}
		}
		
		/**
		 * Cancel data modifications event handler.
		 */
		function _onCancelClick() {
			const $pageMode = $(this).closest('thead').find('.select-page-mode');
			const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.overview-row-selection');
			
			if ($pageMode.val() == 'edit-mode') {
				$pageMode.val('filter-mode')
			} else {
				$pageMode.val('edit-mode')
			}
			
			$checkedSingleCheckboxes.each(function() {
				$(this).prop('checked', !$(this)
					.prop('checked'))
					.trigger('change');
				
				_resolveRowChanges($(this).parents('tr'));
			});
			
			_onPageModeChange();
		}
		
		/**
		 * Restores all the row data changes back to their original state.
		 *
		 * @param {jQuery.Event} $row Current row selector.
		 */
		function _resolveRowChanges($row) {
			const rowIndex = $this.DataTable().row($row).index();
			
			$row.find('input:text, select').not('.select-tax, .select-shipping-time').each(function() {
				const $cell = $(this).closest('td');
				const columnIndex = $this.DataTable().column($cell).index();
				this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
			});
		}
		
		/**
		 * Table row data change event handler.
		 *
		 * It's being triggered every time a row input/select field is changed.
		 */
		function _onTableRowDataChange() {
			const $row = $(this).closest('tr');
			const rowIndex = $this.DataTable().row($row).index();
			
			$row.find('input:text, select').not('.select-tax, .select-shipping-time').each(function() {
				const $cell = $(this).closest('td');
				const columnIndex = $this.DataTable().column($cell).index();
				
				if ($.trim($(this).val()) != $this.DataTable().cell(rowIndex, columnIndex).data()) {
					$(this).addClass('modified');
					return;
				}
				
				$(this).removeClass('modified');
			});
		}
		
		/**
		 * Table row switcher change event handler.
		 *
		 * @param {HTMLElement} switcher Current switcher element.
		 */
		function _onTableRowSwitcherChange() {
			const url = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/Update';
			const productId = $(this).parents('tr').data('id');
			const $cell = $(this).closest('td');
			const columnIndex = $this.DataTable().column($cell).index();
			const columnName = $this.find('tr.filter th').eq(columnIndex).data('columnName');
			const data = {};
			const value = {};
			
			value[columnName] = $(this).prop('checked') ? 1 : 0;
			data[productId] = value;
			
			$.post(url, {
				data,
				pageToken: jse.core.config.get('pageToken'),
			}).done(response => {
				response = $.parseJSON(response);
				
				if (response.success) {
					const content = jse.core.lang.translate('SUCCESS_PRODUCT_UPDATED', 'admin_quick_edit');
					
					// Show success message in the admin info box.
					jse.libs.info_box.addSuccessMessage(content);
					
					return;
				}
				
				const title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
				const message = jse.core.lang.translate('EDIT_ERROR', 'admin_quick_edit');
				const buttons = [
					{
						title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
						callback: event => $(event.currentTarget).parents('.modal').modal('hide')
					}
				];
				
				jse.libs.modal.showMessage(title, message, buttons);
			});
		}
		
		/**
		 * Create inventory list click event handler.
		 *
		 * Generates an inventory list PDF from the selected rows or from all the records if no row is selected.
		 */
		function _onCreateInventoryListClick() {
			const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.overview-row-selection');
			const $modal = $this.parents('.quick-edit.overview').find('div.downloads');
			const $download = $modal.find('button.download-button');
			const generateUrl = jse.core.config.get('appUrl')
				+ '/admin/admin.php?do=QuickEditOverviewAjax/CreateInventoryFile';
			const downloadUrl = jse.core.config.get('appUrl')
				+ '/admin/admin.php?do=QuickEditOverviewAjax/DownloadInventoryFile&pageToken='
				+ jse.core.config.get('pageToken');
			const productsId = [];
			
			$checkedSingleCheckboxes.each(function() {
				let id = $(this).parents('tr').data('id');
				productsId.push(id);
			});
			
			const data = {
				data: 'inventoryList',
				products: productsId,
				pageToken: jse.core.config.get('pageToken')
			};
			
			$modal.find('.modal-body')
				.empty()
				.append(`<p><i class="fa fa-clock-o"></i> ${jse.core.lang.translate('TEXT_PLEASE_WAIT', 'admin_quick_edit')}...</p>`);
			
			$.post(generateUrl, data).done(response => {
				response = $.parseJSON(response);
				
				if (response.success) {
					const $documentTarget = `<a href="${downloadUrl}" class="btn btn-primary download-button" target="_blank">${jse.core.lang.translate('BUTTON_DOWNLOAD', 'admin_quick_edit')}</a>`;
					
					$modal.find('.modal-body')
						.html(`<p><i class="fa fa-check"></i> ${jse.core.lang.translate('TEXT_READY_TO_DOWNLOAD_DOCUMENT', 'admin_quick_edit')}...</p>`);
					
					$download.replaceWith($documentTarget);
					$modal.find('.download-button').on('click', function() {
						$modal.modal('hide');
					})
				}
				
			});
			
			$modal.on('hidden.bs.modal', function() {
				const $download = $modal.find('a.download-button');
				const $downloadButton = `<button type="button" class="btn btn-primary download-button" disabled>${jse.core.lang.translate('BUTTON_DOWNLOAD', 'admin_quick_edit')}</button>`;
				
				$download.replaceWith($downloadButton);
			})
		}
		
		/**
		 * Display graduated prices management modal.
		 *
		 * Note: Current product ID must be set as a data "productId" value in the modal container element.
		 */
		function _onTableRowGraduatedPricesClick() {
			$('.graduated-prices.modal')
				.data('productId', $(this).parents('tr').data('id'))
				.modal('show');
		}
		
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$this
				.on('click', 'tbody tr', _onTableRowClick)
				.on('click', '.row-edit', _onTableRowEditClick)
				.on('click', '.show-product', _onTableRowShowClick)
				.on('click', '.graduated-prices', _onTableRowGraduatedPricesClick)
				.on('change', '.overview-bulk-selection', _onBulkSelectionChange)
				.on('keyup', 'tbody tr input:text', _onTableRowDataChange)
				.on('change', 'tbody tr select', _onTableRowDataChange);
			
			$this.parents('.quick-edit.overview')
				.on('change', 'input:checkbox.overview-row-selection', _onTableRowCheckboxChange)
				.on('change', '.select-page-mode', _onPageModeChange)
				.on('click', '.cancel-edits', _onCancelClick)
				.on('click', '.btn-group .bulk-row-edit', _onTableBulkRowEditClick);
			
			$('body')
				.on('click', '.create-inventory-list', _onCreateInventoryListClick);
			
			$this.on('draw.dt', () => {
				$this.find('.convert-to-switcher').on('change', _onTableRowSwitcherChange);
				_onTableRowCheckboxChange();
			});
			
			done();
		};
		
		return module;
	});

