/* --------------------------------------------------------------
 actions.js 2017-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Special Prices Table Actions Controller
 *
 * This module creates the bulk and row actions for the table.
 */
gx.controllers.module(
	'special_prices_actions',
	
	[`${gx.source}/libs/button_dropdown`, 'user_configuration_service'],
	
	function() {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Create Bulk Actions
		 *
		 * This callback can be called once during the initialization of this module.
		 */
		function _createBulkActions() {
			// Add actions to the bulk-action dropdown.
			const $bulkActions = $('.special-price-bulk-action');
			const defaultBulkAction = 'special-price-bulk-row-edit';
			
			// Edit
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
				class: 'special-price-bulk-row-edit',
				data: {configurationValue: 'special-price-bulk-row-edit'},
				isDefault: defaultBulkAction === 'special-price-bulk-row-edit'
				|| defaultBulkAction === 'save-special-price-bulk-row-edits',
				callback: e => e.preventDefault()
			});
			
			// Save
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
				class: 'save-special-price-bulk-row-edits hidden',
				data: {configurationValue: 'save-special-price-bulk-row-edits'},
				isDefault: false,  // "Save" must not be shown as a default value. 
				callback: e => e.preventDefault()
			});
			
			// Delete
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_DELETE', 'admin_quick_edit'),
				class: 'bulk-delete-special-price',
				data: {configurationValue: 'delete-special-price'},
				isDefault: defaultBulkAction === 'delete-special-price',
				callback: e => e.preventDefault()
			});
			
			$this.datatable_default_actions('ensure', 'bulk');
		}
		
		function _createRowActions() {
			const defaultRowAction = $this.data('defaultRowAction') || 'row-special-price-edit';
			
			$this.find('.btn-group.dropdown').each(function() {
				// Edit
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
					class: 'row-special-price-edit',
					data: {configurationValue: 'row-special-price-edit'},
					isDefault: defaultRowAction === 'row-special-price-edit'
					|| defaultRowAction === 'save-special-price-edits',
					callback: e => e.preventDefault()
				});
				
				// Save
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
					class: 'save-special-price-edits hidden',
					data: {configurationValue: 'save-special-price-edits'},
					isDefault: false,  // "Save" must not be shown as a default value. 
					callback: e => e.preventDefault()
				});
				
				// Delete
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_DELETE', 'admin_quick_edit'),
					class: 'row-delete-special-price',
					data: {configurationValue: 'row-delete-special-price'},
					isDefault: defaultRowAction === 'row-delete-special-price',
					callback: e => e.preventDefault()
				});
				
				$this.datatable_default_actions('ensure', 'row');
			});
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$(window).on('JSENGINE_INIT_FINISHED', () => {
				$this.on('draw.dt', _createRowActions);
				_createRowActions();
				_createBulkActions();
			});
			
			done();
		};
		
		return module;
		
	}); 