/* --------------------------------------------------------------
 events.js 2017-10-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Special Prices Table Events Controller
 *
 * Handles the events of the main QuickEdit table.
 */
gx.controllers.module('special_prices_events', ['modal', 'loading_spinner'], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {};
	
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Bulk selection change event handler.
	 *
	 * @param {jQuery.Event} event Contains event information.
	 * @param {Boolean} [propagate = true] Whether to propagate the event or not.
	 */
	function _onBulkSelectionChange(event, propagate = true) {
		if (propagate === false) {
			return;
		}
		
		$this.find('tbody input:checkbox.special-price-row-selection')
			.single_checkbox('checked', $(this).prop('checked'))
			.trigger('change');
	}
	
	/**
	 * Table row click event handler.
	 *
	 * @param {jQuery.Event} event Contains event information.
	 */
	function _onTableRowClick(event) {
		if (!$(event.target).is('td')) {
			return;
		}
		
		const $singleCheckbox = $(this).find('input:checkbox.special-price-row-selection');
		
		$singleCheckbox.prop('checked', !$(this)
			.find('input:checkbox.special-price-row-selection')
			.prop('checked'))
			.trigger('change');
	}
	
	/**
	 * Enables row editing mode.
	 */
	function _onTableRowEditClick() {
		const $tableRow = $(this).parents('tr');
		const $singleCheckbox = $(this).parents('tr').find('input:checkbox.special-price-row-selection');
		
		const $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
		const $saveAction = $rowActionsDropdown.find('.save-special-price-edits');
		const $editAction = $tableRow.find('.row-special-price-edit');
		$editAction.addClass('hidden');
		$saveAction.removeClass('hidden');
		jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $saveAction);
		
		$tableRow.find('td.editable').each(function() {
			if ($(this).find('input:text').length > 0) {
				return;
			}
			
			if ($(this).hasClass('date')) {
				this.innerHTML = `<input type="text" class="form-control datepicker" value="${this.innerText}" />`;
			} else {
				this.innerHTML = `<input type="text" class="form-control" value="${this.innerText}" />`;
			}
		});
		
		$singleCheckbox.prop('checked', !$(this)
			.find('input:checkbox.special-price-row-selection')
			.prop('checked'))
			.trigger('change');
		
		$tableRow.find('.datepicker').datepicker({
			onSelect: _onTableRowDataChange,
			dateFormat: jse.core.config.get('languageCode') === 'de' ? 'dd.mm.yy' : 'mm.dd.yy'
		});
	}
	
	/**
	 * Bulk row edit event handler.
	 *
	 * Enables the edit mode for the selected rows.
	 */
	function _onTableBulkRowEditClick() {
		const $bulkEditAction = $(this);
		const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.special-price-row-selection');
		
		if ($checkedSingleCheckboxes.length) {
			const $bulkActionsDropdown = $('.special-price-bulk-action');
			const $bulkSaveAction = $bulkActionsDropdown.find('.save-special-price-bulk-row-edits');
			$bulkEditAction.addClass('hidden');
			$bulkSaveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkSaveAction);
		}
		
		$checkedSingleCheckboxes.each(function() {
			const $tableRow = $(this).parents('tr');
			const $rowActionDropdown = $tableRow.find('.btn-group.dropdown');
			const $saveAction = $rowActionDropdown.find('.save-special-price-edits');
			const $editAction = $rowActionDropdown.find('.row-special-price-edit');
			
			$editAction.addClass('hidden');
			$saveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($rowActionDropdown, $saveAction);
			
			$tableRow.find('td.editable').each(function() {
				if ($(this).find('input:text').length > 0) {
					return;
				}
				
				if ($(this).hasClass('date')) {
					this.innerHTML = `<input type="text" class="form-control datepicker" value="${this.innerText}" />`;
				} else {
					this.innerHTML = `<input type="text" class="form-control" value="${this.innerText}" />`;
				}
			});
			
			$tableRow.find('.datepicker').datepicker({
				onSelect: _onTableRowDataChange
			});
		});
	}
	
	/**
	 * Table row checkbox change event handler.
	 */
	function _onTableRowCheckboxChange() {
		const $bulkActionDropdownButtons = $this.parents('.special-prices.modal')
			.find('.special-price-bulk-action > button');
		const $tableRow = $(this).parents('tr');
		
		if ($this.find('input:checkbox:checked.special-price-row-selection').length > 0) {
			$bulkActionDropdownButtons.removeClass('disabled');
		} else {
			$bulkActionDropdownButtons.addClass('disabled');
			
			const $bulkActionsDropdown = $('.special-price-bulk-action');
			const $bulkSaveAction = $bulkActionsDropdown.find('.save-special-price-bulk-row-edits');
			
			if (!$bulkSaveAction.hasClass('hidden')) {
				const $bulkEditAction = $bulkActionsDropdown.find('.special-price-bulk-row-edit');
				$bulkEditAction.removeClass('hidden');
				$bulkSaveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkEditAction);
			}
		}
		
		$tableRow.find('p.values_price').each(function() {
			if ($(this).data('special-price-price-type') == 'fix') {
				$(this).parents('td').addClass('editable');
			}
		});
		
		if (!$(this).prop('checked')) {
			_resolveRowChanges($(this).parents('tr'));
			
			const $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
			const $saveAction = $rowActionsDropdown.find('.save-special-price-edits');
			
			if (!$saveAction.hasClass('hidden')) {
				const $editAction = $rowActionsDropdown.find('.row-special-price-edit');
				$editAction.removeClass('hidden');
				$saveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $editAction.last());
			}
		}
	}
	
	/**
	 * Page mode change between "edit" and "filtering".
	 */
	function _onPageModeChange() {
		if ($(this).val() == 'edit-mode') {
			$this.find('tr.special-price-filter').attr('hidden', true);
			$this.find('tr.special-price-edit').attr('hidden', false);
			$this.find('thead tr:first-child th').addClass('edit-mode');
		} else {
			$this.find('tr.special-price-filter').attr('hidden', false);
			$this.find('tr.special-price-edit').attr('hidden', true);
			$this.find('thead tr:first-child th').removeClass('edit-mode');
		}
	}
	
	/**
	 * Restores all the row data changes back to their original state.
	 *
	 * @param {jQuery.Event} $row Current row selector.
	 */
	function _resolveRowChanges($row) {
		const rowIndex = $this.DataTable().row($row).index();
		
		$row.find('input:text').each(function() {
			const $cell = $(this).closest('td');
			const columnIndex = $this.DataTable().column($cell).index();
			
			this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
		});
	}
	
	/**
	 * Table row data change event handler.
	 *
	 * It's being triggered every time a row input/select field is changed.
	 */
	function _onTableRowDataChange() {
		const $row = $(this).parents('tr');
		const rowIndex = $this.DataTable().row($row).index();
		
		$row.find('input:text').each(function() {
			const $cell = $(this).closest('td');
			const columnIndex = $this.DataTable().column($cell).index();
			
			if ($.trim($(this).val()) != $this.DataTable().cell(rowIndex, columnIndex).data()) {
				$(this).addClass('modified');
				
				return;
			}
			
			$(this).removeClass('modified');
		});
	}
	
	/**
	 * Table row switcher change event handler.
	 *
	 * @param {HTMLElement} switcher Current switcher element.
	 */
	function _onTableRowSwitcherChange(switcher) {
		const url = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditSpecialPricesAjax/Update';
		const specialPrice = $(switcher).parents('tr').attr('id');
		const $cell = $(switcher).closest('td');
		const columnIndex = $this.DataTable().column($cell).index();
		const columnName = $this.find('tr.special-price-filter th').eq(columnIndex).data('columnName');
		const data = {};
		const value = {};
		
		value[columnName] = $(switcher).prop('checked') ? 1 : 0;
		data[specialPrice] = value;
		
		$.post(url, {
			data,
			pageToken: jse.core.config.get('pageToken'),
		}).done(response => {
			response = $.parseJSON(response);
			
			if (response.success) {
				return;
			}
			
			const title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
			const message = jse.core.lang.translate('EDIT_ERROR', 'admin_quick_edit');
			const buttons = [
				{
					title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
					callback: event => $(event.currentTarget).parents('.modal').modal('hide')
				}
			];
			
			jse.libs.modal.showMessage(title, message, buttons);
		});
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this
			.on('click', 'tbody tr', _onTableRowClick)
			.on('click', '.row-special-price-edit', _onTableRowEditClick)
			.on('change', '.special-price-bulk-selection', _onBulkSelectionChange)
			.on('keyup', 'tbody tr input:text', _onTableRowDataChange);
		
		$this.parents('.special-prices.modal')
			.on('change', 'input:checkbox.special-price-row-selection', _onTableRowCheckboxChange)
			.on('change', '.select-special-price-page-mode', _onPageModeChange)
			.on('click', '.btn-group .special-price-bulk-row-edit', _onTableBulkRowEditClick);
		
		$this.on('draw.dt', () => {
			$this.find('.convert-to-switcher').each((index, switcher) => {
				let skipInitialEvent = true;
				$(switcher).on('change', () => {
					if (skipInitialEvent) {
						skipInitialEvent = false;
						return;
					}
					
					_onTableRowSwitcherChange(switcher);
				});
			});
			
			_onTableRowCheckboxChange();
		});
		
		done();
	};
	
	return module;
});

