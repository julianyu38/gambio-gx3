/* --------------------------------------------------------------
 events.js 2016-10-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Table Events Controller
 *
 * Handles the events of the properties table.
 */
gx.controllers.module('properties_events', ['loading_spinner', `${gx.source}/libs/button_dropdown`], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {};
	
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Bulk selection change event handler.
	 *
	 * @param {jQuery.Event} event Contains event information.
	 * @param {Boolean} [propagate = true] Whether to propagate the event or not.
	 */
	function _onBulkSelectionChange(event, propagate = true) {
		if (propagate === false) {
			return;
		}
		
		$this.find('tbody input:checkbox.properties-row-selection')
			.single_checkbox('checked', $(this).prop('checked'))
			.trigger('change');
	}
	
	/**
	 * Table row click event handler.
	 *
	 * @param {jQuery.Event} event Contains event information.
	 */
	function _onTableRowClick(event) {
		if (!$(event.target).is('td')) {
			return;
		}
		
		const $singleCheckbox = $(this).find('input:checkbox.properties-row-selection');
		
		$singleCheckbox.prop('checked', !$(this)
			.find('input:checkbox.properties-row-selection')
			.prop('checked'))
			.trigger('change');
	}
	
	/**
	 * Enables row editing mode.
	 */
	function _onTableRowEditClick() {
		const $tableRow = $(this).parents('tr');
		const $singleCheckbox = $(this).parents('tr').find('input:checkbox.properties-row-selection');
		
		$tableRow.find('td.editable').each(function() {
			if ($(this).find('input:text').length > 0) {
				return;
			}
			
			if ($(this).find('p.values_price').length > 0) {
				$(this).find('p.values_price').each(function() {
					let html = `<input data-properties-price-type="${$(this).data('properties-price-type')}"
									   type="text" 
									   class="form-control values-price" 
									   value="${$(this).context.innerText}" />`;
					
					$(this).replaceWith(html);
				});
			} else {
				this.innerHTML = `<input type="text" class="form-control" value="${this.innerText}" />`;
			}
		});
		
		$singleCheckbox.prop('checked', !$(this)
			.find('input:checkbox.properties-row-selection')
			.prop('checked'))
			.trigger('change');
		
		const $rowActionsDropdown = $tableRow.find('.dropdown');
		const $editAction = $(this);
		const $saveAction = $rowActionsDropdown.find('.save-properties-edits');
		$editAction.addClass('hidden');
		$saveAction.removeClass('hidden');
		jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $saveAction);
	}
	
	/**
	 * Bulk row edit event handler.
	 *
	 * Enables the edit mode for the selected rows.
	 */
	function _onTableBulkRowEditClick() {
		const $bulkEditAction = $(this);
		const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.properties-row-selection');
		
		if ($checkedSingleCheckboxes.length) {
			const $bulkActionsDropdown = $('.properties-bulk-action');
			const $bulkSaveAction = $bulkActionsDropdown.find('.save-properties-bulk-row-edits');
			$bulkEditAction.addClass('hidden');
			$bulkSaveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkSaveAction);
		}
		
		$checkedSingleCheckboxes.each(function() {
			const $tableRow = $(this).parents('tr');
			const $rowActionDropdown = $tableRow.find('.btn-group.dropdown');
			const $saveAction = $rowActionDropdown.find('.save-properties-edits');
			const $editAction = $rowActionDropdown.find('.row-properties-edit');
			
			$editAction.addClass('hidden');
			$saveAction.removeClass('hidden');
			jse.libs.button_dropdown.setDefaultAction($rowActionDropdown, $saveAction);
			
			$tableRow.find('td.editable').each(function() {
				if ($(this).find('input:text').length > 0) {
					return;
				}
				
				if ($(this).find('p.values_price').length > 0) {
					$(this).find('p.values_price').each(function() {
						let html = `<input data-properties-price-type="${$(this).data('properties-price-type')}"
										   type="text" 
										   class="form-control values-price" 
										   value="${$(this).context.innerText}" />`;
						
						$(this).replaceWith(html);
					});
				} else {
					this.innerHTML = `<input type="text" class="form-control" value="${this.innerText}" />`;
				}
			});
			
			
		});
	}
	
	/**
	 * Table row checkbox change event handler.
	 */
	function _onTableRowCheckboxChange() {
		const $bulkActionDropdownButtons = $this.parents('.properties.modal').find('.properties-bulk-action > button');
		const $tableRow = $(this).parents('tr');
		
		if ($this.find('input:checkbox:checked.properties-row-selection').length > 0) {
			$bulkActionDropdownButtons.removeClass('disabled');
		} else {
			$bulkActionDropdownButtons.addClass('disabled');
			
			const $bulkActionsDropdown = $('.properties-bulk-action');
			const $bulkSaveAction = $bulkActionsDropdown.find('.save-properties-bulk-row-edits');
			
			if (!$bulkSaveAction.hasClass('hidden')) {
				const $bulkEditAction = $bulkActionsDropdown.find('.properties-bulk-row-edit');
				$bulkEditAction.removeClass('hidden');
				$bulkSaveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($bulkActionsDropdown, $bulkEditAction);
			}
		}
		
		$(this).parents('tr').find('p.values_price').each(function() {
			if ($(this).data('properties-price-type') == 'fix') {
				$(this).parents('td').addClass('editable');
			}
		});
		
		if (!$(this).prop('checked')) {
			_resolveRowChanges($(this).parents('tr'));
			
			const $rowActionsDropdown = $tableRow.find('.btn-group.dropdown');
			const $saveAction = $rowActionsDropdown.find('.save-properties-edits');
			
			if (!$saveAction.hasClass('hidden')) {
				const $editAction = $tableRow.find('.row-properties-edit');
				$editAction.removeClass('hidden');
				$saveAction.addClass('hidden');
				jse.libs.button_dropdown.setDefaultAction($rowActionsDropdown, $editAction.last());
			}
		}
	}
	
	/**
	 * Cancel data modifications event handler.
	 */
	function _onCancelClick() {
		const $pageMode = $(this).closest('thead').find('.select-properties-page-mode');
		const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.properties-row-selection');
		
		if ($pageMode.val() == 'edit-mode') {
			$pageMode.val('filter-mode')
		} else {
			$pageMode.val('edit-mode')
		}
		
		$checkedSingleCheckboxes.each(function() {
			$(this).prop('checked', !$(this)
				.prop('checked'))
				.trigger('change');
			
			_resolveRowChanges($(this).parents('tr'));
		});
		
		_onPageModeChange();
	}
	
	/**
	 * Page mode change between "edit" and "filtering".
	 */
	function _onPageModeChange() {
		if ($(this).val() == 'edit-mode') {
			$this.find('tr.properties-filter').attr('hidden', true);
			$this.find('tr.properties-edit').attr('hidden', false);
			$this.find('thead tr:first-child th').addClass('edit-mode');
		} else {
			$this.find('tr.properties-filter').attr('hidden', false);
			$this.find('tr.properties-edit').attr('hidden', true);
			$this.find('thead tr:first-child th').removeClass('edit-mode');
		}
	}
	
	/**
	 * Restores all the row data changes back to their original state.
	 *
	 * @param {jQuery.Event} $row Current row selector.
	 */
	function _resolveRowChanges($row) {
		const rowIndex = $this.DataTable().row($row).index();
		
		$row.find('input:text:not(.values-price)').each(function() {
			const $cell = $(this).closest('td');
			const columnIndex = $this.DataTable().column($cell).index();
			
			this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
		});
		
		$row.find('input:text.values-price').each(function() {
			const $cell = $(this).closest('td');
			const columnIndex = $this.DataTable().column($cell).index();
			const cellData = $this.DataTable().cell(rowIndex, columnIndex).data();
			
			let html = `<div class="col-lg-12">
							<label class="control-label">${cellData.values_name}</label>
							<p data-properties-price-type="${$(this).data('properties-price-type')}"
							   class="form-control-static values_price">${cellData.values_price}</p>
						</div>`;
			
			
			$(this).closest('div.form-group').replaceWith(`<div class="form-group">${html}</div>`);
		});
	}
	
	/**
	 * Table row data change event handler.
	 *
	 * It's being triggered every time a row input/select field is changed.
	 */
	function _onTableRowDataChange() {
		const $row = $(this).parents('tr');
		const rowIndex = $this.DataTable().row($row).index();
		
		$row.find('input:text').each(function() {
			const $cell = $(this).closest('td');
			const columnIndex = $this.DataTable().column($cell).index();
			
			if ($.trim($(this).val()) != $this.DataTable().cell(rowIndex, columnIndex).data()) {
				$(this).addClass('modified');
				
				return;
			}
			
			$(this).removeClass('modified');
		});
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this
			.on('click', 'tbody tr', _onTableRowClick)
			.on('click', '.row-properties-edit', _onTableRowEditClick)
			.on('change', '.properties-bulk-selection', _onBulkSelectionChange)
			.on('keyup', 'tbody tr input:text', _onTableRowDataChange);
		
		$this.parents('.properties.modal')
			.on('change', 'input:checkbox.properties-row-selection', _onTableRowCheckboxChange)
			.on('change', '.select-properties-page-mode', _onPageModeChange)
			.on('click', '.cancel-properties-edits', _onCancelClick)
			.on('click', '.btn-group .properties-bulk-row-edit', _onTableBulkRowEditClick);
		
		done();
	};
	
	return module;
});

