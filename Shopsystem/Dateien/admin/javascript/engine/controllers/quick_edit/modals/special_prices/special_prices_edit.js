/* --------------------------------------------------------------
 special_price_edit.js 2016-10-25
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Special Price Edit Controller
 *
 * This controller handles the special price editing functionality.
 */
gx.controllers.module(
	'special_prices_edit',
	
	[
		'modal',
		`${jse.source}/vendor/sumoselect/jquery.sumoselect.min.js`,
		`${jse.source}/vendor/sumoselect/sumoselect.min.css`
	],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Enter Key Code
		 *
		 * @type {Number}
		 */
		const ENTER_KEY_CODE = 13;
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Edit Row Selector
		 *
		 * @type {jQuery}
		 */
		const $edit = $this.find('tr.special-price-edit');
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {bindings: {}};
		
		$edit.find('th').each(function() {
			const columnName = $(this).data('columnName');
			
			if (columnName === 'checkbox' || columnName === 'actions') {
				return true;
			}
			
			module.bindings[columnName] = $(this).find('input, select').first();
		});
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Save modified data with an AJAX request.
		 *
		 * @param {Object} data Contains the updated data.
		 */
		function _save(data) {
			const url = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditSpecialPricesAjax/Update';
			const edit = {
				data,
				pageToken: jse.core.config.get('pageToken')
			};
			
			$.post(url, edit)
				.done(response => {
					$this.DataTable().draw();
					
					response = $.parseJSON(response);
					
					if (response.success) {
						$edit.find('input, select').not('.length, .select-page-mode').val('');
						$edit.find('select').not('.length, .select-page-mode').multi_select('refresh');
						
						return;
					}
					
					const title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
					const message = jse.core.lang.translate('EDIT_ERROR', 'admin_quick_edit');
					const buttons = [
						{
							title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
							callback: event => $(event.currentTarget).parents('.modal').modal('hide')
						}
					];
					
					jse.libs.modal.showMessage(title, message, buttons);
				});
		}
		
		/**
		 * Deletes special price entries for provided products.
		 *
		 * @param {Number[]} products Contains the product IDs.
		 */
		function _delete(products) {
			const url = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditSpecialPricesAjax/Delete';
			const edit = {
				products,
				pageToken: jse.core.config.get('pageToken')
			};
			
			$.post(url, edit)
				.done(response => {
					$this.DataTable().draw();
					
					response = $.parseJSON(response);
					
					if (response.success) {
						$edit.find('input, select').not('.length, .select-page-mode').val('');
						$edit.find('select').not('.length, .select-page-mode').multi_select('refresh');
						
						return;
					}
					
					const title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
					const message = jse.core.lang.translate('EDIT_ERROR', 'admin_quick_edit');
					const buttons = [
						{
							title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
							callback: event => $(event.currentTarget).parents('.modal').modal('hide')
						}
					];
					
					jse.libs.modal.showMessage(title, message, buttons);
				});
		}
		
		/**
		 * Save modifications event handler.
		 *
		 * This method will look for modified values and send them to back-end with an AJAX request.
		 */
		function _onSaveClick() {
			const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.special-price-row-selection');
			const edit = {};
			const data = {};
			
			if ($checkedSingleCheckboxes.length === 0) {
				const title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
				const message = jse.core.lang.translate('NO_PRODUCT_SELECTED', 'admin_quick_edit');
				const buttons = [
					{
						title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
						callback: event => $(event.currentTarget).parents('.modal').modal('hide')
					}
				];
				
				jse.libs.modal.showMessage(title, message, buttons);
				
				return;
			}
			
			$edit.find('th').each(function() {
				const columnName = $(this).data('columnName');
				
				if (columnName === 'checkbox' || columnName === 'actions') {
					return true;
				}
				
				let value = module.bindings[columnName].get();
				
				if (value) {
					edit[columnName] = value;
				}
			});
			
			$checkedSingleCheckboxes.each(function() {
				const $tableRow = $(this).parents('tr');
				
				if ($tableRow.find('input:text.modified').length > 0) {
					let inlineEdit = {};
					
					$tableRow.find('input:text.modified').each(function() {
						const $cell = $(this).closest('td');
						const columnIndex = $this.DataTable().column($cell).index();
						const columnName = $edit.find('th').eq(columnIndex).data('columnName');
						
						
						inlineEdit[columnName] = $(this).val();
					});
					
					data[$tableRow.attr('id')] = inlineEdit;
				} else {
					data[$tableRow.attr('id')] = edit;
				}
				
				$(this).prop('checked', !$(this)
					.prop('checked'))
					.trigger('change');
			});
			
			if (_checkForModifications(data) === false) {
				$checkedSingleCheckboxes.each(function() {
					_resolveRowChanges($(this).parents('tr'));
				});
				
				return;
			}
			
			_save(data);
		}
		
		/**
		 * Delete special prices event handler.
		 *
		 * @param {jQuery.Event} event Contains event information.
		 */
		function _onDeleteClick(event) {
			const products = [];
			
			// Perform delete row action. 
			if ($(event.target).is('.row-delete-special-price')) {
				products.push($(this).parents('tr').attr('id'));
				$(this).prop('checked', !$(this).prop('checked')).trigger('change');
				_delete(products);
				return;
			}
			
			// Perform delete bulk selection action.
			const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.special-price-row-selection');
			
			if ($checkedSingleCheckboxes.length === 0) {
				const title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
				const message = jse.core.lang.translate('NO_PRODUCT_SELECTED', 'admin_quick_edit');
				const buttons = [
					{
						title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
						callback: event => $(event.currentTarget).parents('.modal').modal('hide')
					}
				];
				
				jse.libs.modal.showMessage(title, message, buttons);
				
				return;
			}
			
			$checkedSingleCheckboxes.each(function() {
				products.push($(this).parents('tr').attr('id'));
				$(this).prop('checked', !$(this).prop('checked')).trigger('change');
			});
			
			_delete(products);
		}
		
		/**
		 * Checks for value modifications.
		 *
		 * @param {Object} data Contains current row data.
		 *
		 * @return {boolean} Returns whether modifications were made or not.
		 */
		function _checkForModifications(data) {
			let modificationExists = false;
			
			for (let property in data) {
				if (data.hasOwnProperty(property)) {
					if (!$.isEmptyObject(data[property])) {
						modificationExists = true;
					}
				}
			}
			
			return modificationExists;
		}
		
		/**
		 * Resolves row changes.
		 *
		 * @param {jQuery} $row Selector of the row to be resolved.
		 */
		function _resolveRowChanges($row) {
			const rowIndex = $this.DataTable().row($row).index();
			
			$row.find('input:text').each(function() {
				const $cell = $(this).closest('td');
				const columnIndex = $this.DataTable().column($cell).index();
				this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
			});
		}
		
		/**
		 * Cancel modifications event handler.
		 */
		function _onCancelClick() {
			const $pageMode = $(this).closest('thead').find('.select-special-price-page-mode');
			const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.special-price-row-selection');
			
			if ($pageMode.val() == 'edit-mode') {
				$pageMode.val('filter-mode')
			} else {
				$pageMode.val('edit-mode')
			}
			
			$checkedSingleCheckboxes.each(function() {
				$(this).prop('checked', !$(this)
					.prop('checked'))
					.trigger('change');
				
				_resolveRowChanges($(this).parents('tr'));
			});
			
			_pageModeChange();
		}
		
		/**
		 * Page Mode Change Callback
		 */
		function _pageModeChange() {
			if ($(this).val() == 'edit-mode') {
				$this.find('tr.special-price-filter').attr('hidden', true);
				$this.find('tr.special-price-edit').attr('hidden', false);
				$this.find('thead tr:first-child th').addClass('edit-mode');
			} else {
				$this.find('tr.special-price-filter').attr('hidden', false);
				$this.find('tr.special-price-edit').attr('hidden', true);
				$this.find('thead tr:first-child th').removeClass('edit-mode');
			}
		}
		
		/**
		 * Trigger filtering once the user presses the enter key inside a filter input.
		 *
		 * @param {jQuery.Event} event Contains event information.
		 */
		function _onInputTextFilterKeyUp(event) {
			if (event.which === ENTER_KEY_CODE) {
				$edit.find('.apply-special-price-edits').trigger('click');
			}
		}
		
		/**
		 * Trigger modifications submit once the user presses the enter key inside a edit input.
		 *
		 * @param {jQuery.Event} event Contains event information.
		 */
		function _onInputTextRowKeyUp(event) {
			if (event.which === ENTER_KEY_CODE) {
				$edit.parents('.special-prices.modal').find('.save-special-price-bulk-row-edits').trigger('click');
			}
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$edit
				.on('keyup', 'input:text', _onInputTextFilterKeyUp)
				.on('click', '.apply-special-price-edits', _onSaveClick)
				.on('click', '.cancel-special-price-edits', _onCancelClick);
			
			$edit.parents('.quick-edit.special-price')
				.on('keyup', 'td.editable', _onInputTextRowKeyUp)
				.on('click', '.row-delete-special-price', _onDeleteClick);
			
			$edit.parents('.special-prices.modal')
				.on('click', '.save-special-price-edits', _onSaveClick)
				.on('click', '.save-special-price-bulk-row-edits', _onSaveClick)
				.on('click', '.bulk-delete-special-price', _onDeleteClick);
			
			$this.find('[data-single_select-instance]').each(function() {
				const $select = $(this);
				
				$select.removeAttr('data-single_select-instance');
				
				$select.SumoSelect();
				$select[0].sumo.add('', `${jse.core.lang.translate('TOOLTIP_NO_CHANGES', 'admin_quick_edit')}`, 0);
				$select[0].sumo.unSelectAll();
			});
			
			done();
		};
		
		return module;
	});