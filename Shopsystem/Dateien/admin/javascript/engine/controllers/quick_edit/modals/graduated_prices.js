/* --------------------------------------------------------------
 graduated_prices.js 2017-03-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Graduate Prices Modal Controller
 *
 * Handles the graduate prices modal functionality.
 */
gx.controllers.module('graduated_prices', ['modal', `${gx.source}/libs/info_box`], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {};
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Creates a graduated prices form row.
	 *
	 * @param {Object} graduatedPrice Contains the graduated price information.
	 *
	 * @return {jQuery} Returns the created row selector.
	 */
	function _createRow(graduatedPrice) {
		const $row = $('<div />', {
			'class': 'row',
			'data-graduated-price': graduatedPrice
		});
		
		const $quantityFormGroup = $('<div />', {
			'class': 'form-group col-xs-4'
		});
		
		const $quantityLabel = $('<label />', {
			'class': 'control-label',
			'text': jse.core.lang.translate('NUMBER_OF_PIECES', 'admin_quick_edit')
		});
		
		const $quantityInput = $('<input />', {
			'type': 'text',
			'class': 'form-control quantity',
			'value': graduatedPrice.quantity
		});
		
		$quantityFormGroup.append($quantityLabel, $quantityInput);
		
		const $priceFormGroup = $('<div />', {
			'class': 'form-group col-xs-4'
		});
		
		const $priceLabel = $('<label />', {
			'class': 'control-label',
			'text': jse.core.lang.translate('PRICE', 'admin_quick_edit')
		});
		
		const $priceInputGroup = $('<div />', {
			'class': 'input-group'
		});
		
		const $priceInput = $('<input />', {
			'type': 'text',
			'class': 'form-control price',
			'value': graduatedPrice.personal_offer
		});
		
		const $priceInputGroupButton = $('<span />', {
			'class': 'input-group-btn'
		});
		
		const $deleteButton = $('<button />', {
			'class': 'btn delete',
			'html': '<i class="fa fa-trash"></i>'
		});
		
		if (graduatedPrice.price_id === '') {
			$deleteButton.prop('disabled', true);
		}
		
		$priceInputGroupButton.append($deleteButton);
		
		$priceInputGroup.append($priceInput, $priceInputGroupButton);
		
		$priceFormGroup.append($priceLabel, $priceInputGroup);
		
		$row.append($quantityFormGroup, $priceFormGroup);
		
		return $row;
	}
	
	/**
	 * Handles AJAX request errors.
	 *
	 * @param {jQuery.jqXHR} jqXHR jQuery request object.
	 * @param {String} textStatus Request status string.
	 * @param {Error} errorThrown Thrown error object.
	 */
	function _handleRequestError(jqXHR, textStatus, errorThrown) {
		jse.libs.modal.message({
			title: jse.core.lang.translate('error', 'messages'),
			content: jse.core.lang.translate('UNEXPECTED_REQUEST_ERROR', 'admin_quick_edit')
		});
	}
	
	/**
	 * Get graduated prices for selected product.
	 *
	 * @param {Number} productId Selected product ID.
	 *
	 * @return {jQuery.jqXHR} Returns request's deferred object.
	 */
	function _getGraduatedPrices(productId) {
		return $.ajax({
			method: 'GET',
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/ProductGraduatedPrices',
			data: {
				productId,
				pageToken: jse.core.config.get('pageToken')
			},
			dataType: 'json'
		});
	}
	
	/**
	 * Renders graduated prices content.
	 *
	 * @param {Object} graduatedPrices Contains graduated prices info for selected product.
	 */
	function _displayGraduatedPrices(graduatedPrices) {
		const $modalBody = $this.find('.modal-body');
		
		$modalBody.empty();
		
		const $tabList = $('<ul />', {
			'class': 'nav nav-tabs',
			'role': 'tablist'
		});
		
		const $tabContent = $('<div />', {
			'class': 'tab-content'
		});
		
		for (let customerStatusId in graduatedPrices.data[0].customers) {
			const customerStatus = graduatedPrices.data[0].customers[customerStatusId];
			
			const $tab = $('<li />');
			
			const graduatedPricesCount = customerStatus.graduations.length
				? ` (${customerStatus.graduations.length})` : '';
			
			const $link = $('<a />', {
				'href': `#customer-status-${customerStatusId}`,
				'role': 'tab',
				'data-toggle': 'tab',
				'html': customerStatus.status_name + graduatedPricesCount
			});
			
			$tab.append($link);
			
			$tabList.append($tab);
			
			// Add new tab container in tab content. 
			const $tabPane = $('<div />', {
				'role': 'tabpanel',
				'class': 'tab-pane fade',
				'id': `customer-status-${customerStatusId}`,
				'data-customer-status-id': customerStatusId
			});
			
			for (let graduation of customerStatus.graduations) {
				graduation.customer_status_id = customerStatusId;
				$tabPane.append(_createRow(graduation));
			}
			
			$tabPane.append(_createRow({
				price_id: '',
				quantity: '',
				personal_offer: '',
				customer_status_id: customerStatusId
			}));
			
			$tabContent.append($tabPane);
		}
		
		$modalBody.append($tabList, $tabContent);
		
		// Show the first tab contents. 
		$tabList.find('a:first').tab('show');
	}
	
	/**
	 * Updates row count in tab link.
	 */
	function _updateTabCounters() {
		$this.find('.tab-pane').each(function() {
			const $tabPane = $(this);
			const graduatedPricesCount = $tabPane.find('.row').length - 1;
			const $tabLink = $this.find(`[href="#${$tabPane.attr('id')}"]`);
			const countText = graduatedPricesCount > 0 ? `(${graduatedPricesCount})` : '';
			
			if ($tabLink.text().search(/\(.*\)/) !== -1) {
				$tabLink.text($tabLink.text().replace(/\(.*\)/, countText));	
			} else {
				$tabLink.text($tabLink.text() + countText);
			}
		});
	}
	
	/**
	 * Row input key up event handler.
	 */
	function _onRowInputKeyUp() {
		const $row = $(this).parents('.row:first');
		const $lastRow = $row.parents('.tab-pane').find('.row:last');
		
		if ($lastRow[0] === $row[0] && $row.find('input.quantity').val() !== '') {
			const $tabPane = $row.parents('.tab-pane:first');
			
			$row.find('.btn.delete').prop('disabled', false);
			
			$tabPane.append(_createRow({
				price_id: '',
				quantity: '',
				personal_offer: '',
				customer_status_id: $tabPane.data('customerStatusId')
			}));
			
			_updateTabCounters();
		}
	}
	
	/**
	 * Row delete button click event handler.
	 */
	function _onRowDeleteClick() {
		$(this).parents('.row:first').remove();
		_updateTabCounters();
	}
	
	/**
	 * Graduated prices modal show event handler.
	 *
	 * Loads and displays graduated price info for the selected product.
	 */
	function _onModalShow() {
		_getGraduatedPrices($this.data('productId'))
			.done(_displayGraduatedPrices)
			.fail(_handleRequestError);
	}
	
	/**
	 * Saves graduated prices and closes the modal.
	 */
	function _onSaveClick() {
		const customerStatuses = {};
		
		$this.find('.tab-pane').each((index, tabPane) => {
			const $tabPane = $(tabPane);
			const customerStatusId = $tabPane.data('customerStatusId');
			
			customerStatuses[customerStatusId] = [];
			
			$tabPane.find('.row').each((index, row) => {
				const $row = $(row);
				
				if ($row.is(':last-child')) {
					return false;
				}
				
				customerStatuses[customerStatusId].push({
					price_id: $row.data('price_id'),
					quantity: $row.find('input.quantity').val(),
					personal_offer: $row.find('input.price').val()
				});
			});
			
			// Add value for empty groups.
			if (!customerStatuses[customerStatusId].length) {
				customerStatuses[customerStatusId].push('empty');
			}
			
		});
		
		$.ajax({
			method: 'POST',
			url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditOverviewAjax/SaveGraduatedPrices',
			data: {
				pageToken: jse.core.config.get('pageToken'),
				productId: $this.data('productId'),
				customerStatuses
			},
			dataType: 'json'
		})
			.done(response => {
				$this.modal('hide');
				jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('SUCCESS_PRODUCT_UPDATED', 'admin_quick_edit'));
			})
			.fail(_handleRequestError)
	}
	
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this
			.on('click', '.btn.delete', _onRowDeleteClick)
			.on('click', '.btn.save', _onSaveClick)
			.on('keyup', 'input.form-control', _onRowInputKeyUp)
			.on('show.bs.modal', _onModalShow);
		
		done();
	};
	
	return module;
	
}); 