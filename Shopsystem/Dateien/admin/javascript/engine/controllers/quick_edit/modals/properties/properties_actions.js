/* --------------------------------------------------------------
 actions.js 2017-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Table Actions Controller
 *
 * This module creates the bulk and row actions for the properties table.
 */
gx.controllers.module(
	'properties_actions',
	
	[`${gx.source}/libs/button_dropdown`, 'user_configuration_service'],
	
	function() {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Create Bulk Actions
		 *
		 * This callback can be called once during the initialization of this module.
		 */
		function _createBulkActions() {
			// Add actions to the bulk-action dropdown.
			const $bulkActions = $('.properties-bulk-action');
			const defaultBulkAction = 'properties-bulk-row-edit';
			
			// Edit
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
				class: 'properties-bulk-row-edit',
				data: {configurationValue: 'properties-bulk-row-edit'},
				isDefault: defaultBulkAction === 'properties-bulk-row-edit'
				|| defaultBulkAction === 'save-properties-bulk-row-edits',
				callback: e => e.preventDefault()
			});
			
			// Save
			jse.libs.button_dropdown.addAction($bulkActions, {
				text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
				class: 'save-properties-bulk-row-edits hidden',
				data: {configurationValue: 'save-properties-bulk-row-edits'},
				isDefault: false, // "Save" must not be shown as a default value. 
				callback: e => e.preventDefault()
			});
			
			$this.datatable_default_actions('ensure', 'bulk');
		}
		
		function _createRowActions() {
			const defaultRowAction = $this.data('defaultRowAction') || 'row-properties-edit';
			
			$this.find('.btn-group.dropdown').each(function() {
				// Edit
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_EDIT', 'admin_quick_edit'),
					class: 'row-properties-edit',
					data: {configurationValue: 'row-properties-edit'},
					isDefault: defaultRowAction === 'row-properties-edit'
					|| defaultRowAction === 'save-properties-edits',
					callback: e => e.preventDefault()
				});
				
				// Save
				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('BUTTON_SAVE', 'admin_quick_edit'),
					class: 'save-properties-edits hidden',
					data: {configurationValue: 'save-properties-edits'},
					isDefault: false, // "Save" must not be shown as a default value. 
					callback: e => e.preventDefault()
				});
				
				$this.datatable_default_actions('ensure', 'row');
			});
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$(window).on('JSENGINE_INIT_FINISHED', () => {
				$this.on('draw.dt', _createRowActions);
				_createRowActions();
				_createBulkActions();
			});
			
			done();
		};
		
		return module;
		
	}); 