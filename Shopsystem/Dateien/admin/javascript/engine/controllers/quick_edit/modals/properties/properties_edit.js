/* --------------------------------------------------------------
 edit.js 2016-10-25
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Properties Table Editing Controller
 *
 * Handles the editing functionality of the properties modal table.
 */
gx.controllers.module(
	'properties_edit', 
	
	[
		'modal',
		`${jse.source}/vendor/sumoselect/jquery.sumoselect.min.js`,
		`${jse.source}/vendor/sumoselect/sumoselect.min.css`
	], 
	
	function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Enter Key Code
	 *
	 * @type {Number}
	 */
	const ENTER_KEY_CODE = 13;
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Edit Row Selector
	 *
	 * @type {jQuery}
	 */
	const $edit = $this.find('tr.properties-edit');
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {bindings: {}};
	
	// Dynamically define the edit row data-bindings.
	$edit.find('th').each(function() {
		const columnName = $(this).data('columnName');
		
		if (columnName === 'checkbox' || columnName === 'actions') {
			return true;
		}
		
		module.bindings[columnName] = $(this).find('input, select').first();
	});
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Save modifications event handler.
	 *
	 * This method will look for modified values and send them to back-end with an AJAX request.
	 */
	function _onSaveClick() {
		const $checkedSingleCheckboxes = $this.find('input:checkbox:checked.properties-row-selection');
		const edit = {};
		const data = {};
		
		if ($checkedSingleCheckboxes.length === 0) {
			const title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
			const message = jse.core.lang.translate('NO_PRODUCT_SELECTED', 'admin_quick_edit');
			const buttons = [
				{
					title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
					callback: event => $(event.currentTarget).parents('.modal').modal('hide')
				}
			];
			
			jse.libs.modal.showMessage(title, message, buttons);
			
			return;
		}
		
		$edit.find('th').each(function() {
			const columnName = $(this).data('columnName');
			
			if (columnName === 'checkbox' || columnName === 'actions') {
				return true;
			}
			
			let value = module.bindings[columnName].get();
			
			if (value) {
				edit[columnName] = value;
			}
		});
		
		$checkedSingleCheckboxes.each(function() {
			const $tableRow = $(this).parents('tr');
			
			if ($tableRow.find('input:text.modified').length > 0) {
				let inlineEdit = {};
				
				$tableRow.find('input:text.modified').each(function() {
					const $cell = $(this).closest('td');
					const columnIndex = $this.DataTable().column($cell).index();
					const columnName = $edit.find('th').eq(columnIndex).data('columnName');
					
					inlineEdit[columnName] = $(this).val();
				});
				
				data[$tableRow.attr('id')] = inlineEdit;
			} else {
				data[$tableRow.attr('id')] = edit;
			}
			
			$(this).prop('checked', !$(this)
				.prop('checked'))
				.trigger('change');
		});
		
		if (_checkForModifications(data) === false) {
			$checkedSingleCheckboxes.each(function() {
				_resolveRowChanges($(this).parents('tr'));
			});
			
			return;
		}
		
		_save(data);
	}
	
	/**
	 * Checks for value modifications.
	 *
	 * @param {Object} data Contains current row data.
	 *
	 * @return {boolean} Returns whether modifications were made or not.
	 */
	function _checkForModifications(data) {
		let modificationExists = false;
		
		for (let property in data) {
			if (data.hasOwnProperty(property)) {
				if (!$.isEmptyObject(data[property])) {
					modificationExists = true;
				}
			}
		}
		
		return modificationExists;
	}
	
	/**
	 * Resolves row changes.
	 *
	 * @param {jQuery} $row Selector of the row to be resolved.
	 */
	function _resolveRowChanges($row) {
		const rowIndex = $this.DataTable().row($row).index();
		
		$row.find('input:text').each(function() {
			const $cell = $(this).closest('td');
			const columnIndex = $this.DataTable().column($cell).index();
			
			this.parentElement.innerHTML = $this.DataTable().cell(rowIndex, columnIndex).data();
		});
	}
	
	/**
	 * Trigger filtering once the user presses the enter key inside a filter input.
	 *
	 * @param {jQuery.Event} event Contains event information.
	 */
	function _onInputTextFilterKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$edit.find('.apply-properties-edits').trigger('click');
		}
	}
	
	/**
	 * Trigger modifications submit once the user presses the enter key inside a edit input.
	 *
	 * @param {jQuery.Event} event Contains event information.
	 */
	function _onInputTextRowKeyUp(event) {
		if (event.which === ENTER_KEY_CODE) {
			$edit.parents('.properties.modal').find('.save-properties-bulk-row-edits').trigger('click');
		}
	}
	
	/**
	 * Stores the change of a shipping time value.
	 */
	function _onTableRowShippingTimeChange() {
		const propertiesId = $(this).parents('tr').attr('id');
		const shippingTimeId = $(this).val();
		const edit = {};
		const data = {};
		
		edit['combiShippingStatusName'] = shippingTimeId;
		data[propertiesId] = edit;
		
		_save(data, false);
	}
	
	/**
	 * Stores the change of a price type value.
	 */
	function _onTableRowPriceTypeChange() {
		const propertiesId = $(this).parents('tr').attr('id');
		const priceType = $(this).val();
		const edit = {};
		const data = {};
		
		edit['combiPriceType'] = priceType;
		data[propertiesId] = edit;
		
		_save(data, false);
	}
	
	/**
	 * Save modified data with an AJAX request.
	 *
	 * @param {Object} data Contains the updated data.
	 * @param {Boolean} [reload=true] Reload the page after the request is done.
	 */
	function _save(data, reload = true) {
		const url = jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditProductPropertiesAjax/Update';
		const edit = {
			data,
			pageToken: jse.core.config.get('pageToken')
		};
		
		$.post(url, edit)
			.done(response => {
				response = $.parseJSON(response);
				
				if (response.success) {
					$edit.find('input, select').not('.length, .select-page-mode').val('');
					$edit.find('select').not('.length, .select-page-mode').multi_select('refresh');
					
					if (reload) {
						$this.DataTable().ajax.reload();
					}
					
					return;
				}
				
				const title = jse.core.lang.translate('MODAL_TITLE_NODE', 'admin_quick_edit');
				const message = jse.core.lang.translate('EDIT_ERROR', 'admin_quick_edit');
				const buttons = [
					{
						title: jse.core.lang.translate('BUTTON_CLOSE', 'admin_quick_edit'),
						callback: event => $(event.currentTarget).parents('.modal').modal('hide')
					}
				];
				
				jse.libs.modal.showMessage(title, message, buttons);
			});
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this.on('change', '.select-properties-shipping-time', _onTableRowShippingTimeChange)
			.on('change', '.select-properties-price-type', _onTableRowPriceTypeChange);
		
		$edit
			.on('keyup', 'input:text', _onInputTextFilterKeyUp)
			.on('click', '.apply-properties-edits', _onSaveClick);
		
		$edit.parents('.quick-edit.properties')
			.on('keyup', 'td.editable', _onInputTextRowKeyUp);
		
		$edit.parents('.properties.modal')
			.on('click', '.save-properties-edits', _onSaveClick)
			.on('click', '.save-properties-bulk-row-edits', _onSaveClick);
		
		// Initialize the elements.
		$this.find('[data-single_select-instance]').each(function() {
			const $select = $(this);
			
			$select.removeAttr('data-single_select-instance');
			
			// Instantiate the widget without an AJAX request.
			$select.SumoSelect();
			$select[0].sumo.add('', `${jse.core.lang.translate('TOOLTIP_NO_CHANGES', 'admin_quick_edit')}`, 0);
			$select[0].sumo.unSelectAll();
		});
		
		done();
	};
	
	return module;
});