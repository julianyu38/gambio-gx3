/* --------------------------------------------------------------
 special_price_init 2016-12-14
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Special Prices Table Initializations Controller
 *
 * This controller initializes the main QuickEdit special prices table with a new jQuery DataTables instance.
 */
gx.controllers.module(
	'special_prices_init',
	
	[
		`${jse.source}/vendor/datatables/jquery.dataTables.min.css`,
		`${jse.source}/vendor/datatables/jquery.dataTables.min.js`,
		'datatable',
		`${gx.source}/libs/quick_edit_special_prices_overview_columns`
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		/**
		 * Selected Product IDs
		 *
		 * @type {Number[]}
		 */
		let productIds = [0];
		
		/**
		 * Properties Modal Selector
		 *
		 * @type {jQuery}
		 */
		const $modal = $this.parents('.modal');
		
		/**
		 * DataTable Columns
		 *
		 * @type {Object[]}
		 */
		const columns = jse.libs.datatable.prepareColumns($this, jse.libs.quick_edit_special_prices_overview_columns, data.specialPriceActiveColumns);
		
		/**
		 * DataTable Options
		 *
		 * @type {Object}
		 */
		const options = {
			autoWidth: false,
			dom: 't',
			pageLength: data.pageLength,
			displayStart: 0,
			serverSide: true,
			language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
			ajax: {
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=QuickEditSpecialPricesAjax/DataTable',
				type: 'POST',
				data: (data) => {
					data.productId = _getProductIds();
					data.pageToken = jse.core.config.get('pageToken');
					return data;
				}
			},
			orderCellsTop: true,
			order: _getOrder(),
			columns: columns,
			createdRow(row, data, dataIndex) {
				if (data.specialPriceIsNewEntry === true) {
					$(row).addClass('special-new-entry');
				}
			}
		};
		
		/**
		 * Loading Spinner 
		 * 
		 * @type {jQuery}
		 */
		let $spinner = null;
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Get Initial Table Order
		 *
		 * @return {Object[]} Returns the ordered column definitions.
		 */
		function _getOrder() {
			let index = 1; // Order by first column by default.
			let direction = 'asc'; // Order ASC by default.
			let columnName = 'productsName'; // Order by products name by default.
			
			$this.on('click', 'th', function() {
				columnName = $(this).data('column-name');
				index = $this.DataTable().column(this).index();
			});
			
			if (data.specialPriceActiveColumns.indexOf('productsName') > -1) { // Order by name if possible.
				index = data.specialPriceActiveColumns.indexOf('productsName');
			}
			
			return [[index, direction]];
		}
		
		/**
		 * Set current product IDs.
		 *
		 * @param {jQuery.Event} event Contains event information.
		 */
		function _setProductIds(event) {
			productIds = [];
			
			if ($(event.target).is('a.special-price')) {
				productIds.push($(this).parents('tr').data('id'));
				
				return;
			}
			
			const $singleCheckboxes = $this.parents('.quick-edit.overview')
				.find('input:checkbox:checked.overview-row-selection');
			
			$singleCheckboxes.each(function() {
				productIds.push($(this).parents('tr').data('id'));
			});
		}
		
		/**
		 * Get Product IDs.
		 *
		 * @return {Number[]} Returns the product IDs.
		 */
		function _getProductIds() {
			return productIds;
		}
		
		/**
		 * Special prices modal shown event handler.
		 */
		function _onModalShown() {
			$this.parents('body').find('.comiseo-daterangepicker').css('z-index', 1070);
			$(window).trigger('resize');
			
			if (!$.fn.DataTable.isDataTable($this)) {
				jse.libs.datatable.create($this, options);
				
				return;
			}
			
			$this.DataTable().ajax.reload();
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = function(done) {
			$modal
				.on('show.bs.modal', function() {
					$(this).find('.modal-dialog').css('z-index', 1060)
				})
				.on('shown.bs.modal', _onModalShown)
				.on('hide.bs.modal', function() {
					$(this).find('.modal-dialog').css('z-index', 0);
				});
			
			$this.parents('.quick-edit.overview')
				.on('click', 'a.special-price', _setProductIds)
				.on('click', 'a.bulk-special-price-edit', _setProductIds);
			
			$this.on('draw.dt', () => {
				$this.find('thead input:checkbox')
					.prop('checked', false)
					.trigger('change', [false]);
				$this.find('tbody').attr('data-gx-widget', 'single_checkbox switcher');
				$this.find('tbody').attr('data-single_checkbox-selector', '.special-price-row-selection');
				$this.find('tbody').attr('data-switcher-selector', '.convert-to-switcher');
				
				gx.widgets.init($this);
			});
			
			$this.on('preXhr.dt', () => $spinner =
				jse.libs.loading_spinner.show($modal.find('.modal-content'), $modal.css('z-index')));
			$this.on('xhr.dt', () => jse.libs.loading_spinner.hide($spinner));
			
			jse.libs.datatable.create($this, options);
			
			done();
		};
		
		return module;
	});