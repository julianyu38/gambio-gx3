/* --------------------------------------------------------------
 gxmodule_configuration.js 2018-01-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('gxmodule_configuration', ['modal'],

	function (data) {
	
		'use strict';
	
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Standard button
		 *
		 * @type {jQuery}
		 */
		const $button = $('.GXModuleConfigButton');
		
		/**
		 * Button in modal dialog
		 *
		 * @type {jQuery}
		 */
		const $modalButton = $('.GXModuleModalButton');
		
		/**
		 * Modal dialog
		 *
		 * @type {jQuery}
		 */
		const $modal = $('.modal');
		
		const $loadingSpinnder = $('.loading-spinner');
	                      
		/**
		 * Default Options
		 *
		 * @type {object}
		 */
		const defaults = {};
	
		/**
		 * Final Options
		 *
		 * @type {object}
		 */
		const options = $.extend(true, {}, defaults, data);
	
		/**
		 * Module Object
		 *
		 * @type {object}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// EVENT HANDLERS
		// ------------------------------------------------------------------------
		
		/**
		 * Event handling of buttons
		 */
		$button.on('click', function (event) {
			event.preventDefault();
			$loadingSpinnder.fadeIn();
			var action = $(this).data('action');
			var controller = $(this).data('controller');
			var message = $(this).data('message');
			var formData = $(this).closest('form').serialize();
			var request = $.ajax('admin.php?do=GXModuleCenterModuleButtonActionsAjax',{method: 'post',data: {action,controller,formData},dataType: "json"});
			
			request.done(function(response){
				$loadingSpinnder.fadeOut();
				if(response.success) {
					
					if(!message){
						message = response.data;
					}
					
					jse.libs.info_box.addSuccessMessage(
						message);
				}
				else
				{
					jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'),response.data);
				}
			})
				.fail(function(jqxhr, textStatus, errorThrown) {
					$('.loading-spinner').fadeOut();
					jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'),
					                           errorThrown);
				});
		});
		
		/**
		 * Event handling of buttons in modal dialogs
		 */
		$modalButton.on('click', function (event) {
			event.preventDefault();
			var modal = $(this).data('target');
			$(modal).modal('show');
		});
		
		/**
		 * Load HTML Code for modal dialog
		 * on error show modal with error message
		 */
		$modal.each(function () {
			var modalContent = $(this).data('content');
			var modalBody = $(this).find('.modal-body');
			if (modalContent) {
				$(modalBody).load('admin.php?do=GXModuleCenterModuleButtonActionsAjax/Modal', {content: modalContent}, function (response, status, xhr) {
					
					var IS_JSON = true;
					try {
						var json = $.parseJSON(response);
					}
					catch (err) {
						IS_JSON = false;
					}
					
					if (IS_JSON) {
						jse.libs.modal.showMessage(jse.core.lang.translate('error', 'messages'),
						                           json.data);
					}
				});
			}
		});
	
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
	
		module.init = function (done) {
			done();
		};
	
		return module;
	});