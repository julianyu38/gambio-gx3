/* --------------------------------------------------------------
 module_center.js 2017-09-27
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Module Center
 *
 * This module will handle the click events on the module center page
 *
 * @module Controllers/module_center
 */
gx.controllers.module(
	'module_center',
	
	[
		`${jse.source}/vendor/datatables/jquery.dataTables.min.css`,
		`${jse.source}/vendor/datatables/jquery.dataTables.min.js`,
		'datatable'
	],
	
	/**  @lends module:Controllers/module_center */
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Module Selector
			 *
			 * @var {object}
			 */
			$this = $(this),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {},
			
			/**
			 * Elements
			 * 
			 * @type {object}
			 */
			elements = {
				
				form: $('.bottom-save-bar form'),
				
				/**
				 * Install Button
				 * 
				 * @type {jQuery}
				 */
				installButton: $('.bottom-save-bar form button[name="install"]'),
				
				/**
				 * Uninstall Button
				 *
				 * @type {jQuery}
				 */
				uninstallButton: $('.bottom-save-bar form button[name="uninstall"]'),
				
				/**
				 * Edit Button
				 *
				 * @type {jQuery}
				 */
				editButton: $('.bottom-save-bar form a.btn'),
				
				/**
				 * Module Hidden Field
				 * 
				 * @type {jQuery}
				 */
				hiddenModule: $('.bottom-save-bar form input[name="module"]')
			},
			
			/**
			 * Final Options
			 *
			 * @var {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// PRIVATE METHODS
		// ------------------------------------------------------------------------
		
		var _loadModuleData = function(module) {
			
			$.ajax({
					url: 'admin.php?do=ModuleCenter/GetData&module=' + module,
					type: 'GET',
					dataType: 'json'
				})
				.success(function(data) {
					if (data.success) {
						$('.configuration-box-header h2').html(data.payload.title);
						$('.configuration-box-description p').html(data.payload.description);
						
						elements.editButton.attr('href', 'admin.php?do=' + data.payload.name +
							'ModuleCenterModule');
						
						if (data.payload.isInstalled) {
							//$('.gx-configuration-box form').attr('action', 'admin.php?do=ModuleCenter/Destroy');
							elements.uninstallButton.show();
							elements.editButton.show();
							elements.installButton.hide();
						} else {
							elements.form.attr('action', 'admin.php?do=ModuleCenter/Store');
							elements.uninstallButton.hide();
							elements.editButton.hide();
							elements.installButton.show();
						}
						
						elements.hiddenModule.val(data.payload.name);
						$('.gx-configuration-box').css('visibility', 'visible');
					}
				});
		};
		
		var _openUninstallDialog = function(event) {
			event.preventDefault();
			var $dialog = $('#module-center-confirmation-dialog'),
				module = elements.hiddenModule.val();
			
			$dialog.find('.modal-info-text').html(jse.core.lang.translate('text_uninstall_confirmation',
				'module_center'));
			$dialog.find('input[name="module"]').val(module);
			
			$dialog.dialog({
				'title': jse.core.lang.translate('uninstall_confirmation_title', 'module_center')
					.replace('%s',
						module),
				'modal': true,
				'dialogClass': 'gx-container',
				'buttons': [
					{
						'text': jse.core.lang.translate('close', 'buttons'),
						'class': 'btn',
						'click': function() {
							$(this).dialog('close');
						}
					},
					{
						'text': jse.core.lang.translate('uninstall', 'buttons'),
						'class': 'btn btn-primary',
						'click': function() {
							$dialog.find('form').submit();
						}
					}
				]
			});
		};
		
		var queryString = (function(a) {
			if (a === '') {
				return {};
			}
			var b = {};
			for (var i = 0; i < a.length; ++i) {
				var p = a[i].split('=', 2);
				if (p.length === 1) {
					b[p[0]] = '';
				} else {
					b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, ' '));
				}
			}
			return b;
		})(window.location.search.substr(1).split('&'));
		
		// ------------------------------------------------------------------------
		// EVENT HANDLERS
		// ------------------------------------------------------------------------
		
		var _hashChange = function() {
			var module = window.location.hash.substring(1);
			
			if (module !== '') {
				_loadModuleData(module);
			}
		};
		
		var _setRowActive = function() {
			$('.gx-modules-table .dataTableRow.active').removeClass('active');
			
			$(this).addClass('active');
			
			$('html, body').animate({
				scrollTop: 0
			});
		};
		
		var _removeParam = function(key, sourceURL) {
		    var URL = sourceURL.split("?")[0];
		    var params = [];
		    var queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
		    
		    if (queryString !== "") {
		        params = queryString.split("&");
		        for (var i = params.length - 1; i >= 0; i -= 1) {
		            var param = params[i].split("=")[0];
		            if (param === key) {
		                params.splice(i, 1);
		            }
		        }
		        URL = URL + "?" + params.join("&");
		    }
		    window.history.pushState({}, document.title, URL);
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			jse.libs.datatable.create($this.find('.gx-modules-table'), {
				'dom': 't',
				'autoWidth': false,
				'pageLength': 1000,
				'columnDefs': [
					{
						'targets': [1, 2],
						'orderable': false
					}
				],
				'order': []
			});
			
			if (typeof queryString.module !== 'undefined') {
				_loadModuleData(queryString.module);
			} else {
				$('.gx-configuration-box').css('visibility', 'hidden');
				$('.bottom-save-bar .btn-edit').hide();
			}
			
			_hashChange();
			
			$(window).on('hashchange', _hashChange);
			$('.gx-modules-table .dataTableRow').on('click', _setRowActive);
			elements.uninstallButton.on('click', _openUninstallDialog);
			
			_removeParam('module', window.location.href)
			
			done();
		};
		
		return module;
	});
