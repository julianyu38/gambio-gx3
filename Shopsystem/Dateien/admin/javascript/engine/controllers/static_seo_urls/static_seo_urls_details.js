/* --------------------------------------------------------------
 static_seo_urls_details.js 2017-05-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Static Seo Url Edit Form
 *
 * Handles the static seo url details form operations.
 */
gx.controllers.module(
	'static_seo_urls_details',
	[
		'modal',
		'loading_spinner'
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Master data panel selector.
		 *
		 * @type {jQuery}
		 */
		const $masterDataPanel = $this.find('.panel.master-data');
		
		/**
		 * Static seo url panel containers (each language holds a container, that contains the static seo url panels).
		 *
		 * @type {jQuery}
		 */
		const $tabContents = $this.find('.tab-pane');
		
		/**
		 * Spinner Selector
		 *
		 * @type {jQuery}
		 */
		let $spinner = null;
		
		/**
		 * Do a refresh instead of redirecting?
		 *
		 * @type {Boolean}
		 */
		let doRefresh = false;
		
		/**
		 * Default Options
		 *
		 * @type {Object}
		 */
		const defaults = {};
		
		/**
		 * Final Options
		 *
		 * @type {Object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Handles the form submit event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 * @param {Object} data Event data.
		 */
		function _onFormSubmit(event, data) {
			// Prevent the submit of the form.
			event.preventDefault();
			
			// Check refresh parameter.
			if (data && data.refresh) {
				doRefresh = true;
			}
			
			// Show loading spinner.
			$spinner = jse.libs.loading_spinner.show($this);
			
			// Upload files.
			_performSubmit()
				.then(() => jse.libs.loading_spinner.hide($spinner));
		}
		
		/**
		 * Shows the submit error message modal.
		 */
		function _showFailMessage() {
			// Message texts.
			const errorTitle = jse.core.lang.translate('FORM_SUBMIT_ERROR_MODAL_TITLE', 'static_seo_urls');
			const errorMessage = jse.core.lang.translate('FORM_SUBMIT_ERROR_MODAL_TEXT', 'static_seo_urls');
			
			// Show modal.
			jse.libs.modal.showMessage(errorTitle, errorMessage);
			
			// Hide loading spinner.
			if ($spinner) {
				jse.libs.loading_spinner.hide($spinner)
			}
		}
		
		/**
		 * Performs the form submit AJAX request.
		 *
		 * @return {jQuery.jqXHR} jQuery XHR object.
		 */
		function _performSubmit() {
			return $.ajax({
				method: 'POST',
				url: $this.attr('action'),
				data: _getFormData(),
				dataType: 'json'
			})
				.done(response => {
					// URL to redirect to.
					const url = doRefresh ? `admin.php?do=StaticSeoUrl/details&id=${response.id}` : options.redirectUrl;
					
					// Prevent that the page unload prompt is displayed on save action.
					window.onbeforeunload = null;
					
					// Open overview page.
					window.open(url, '_self');
				}, 'json')
				.fail(_showFailMessage);
		}
		
		
		/**
		 * Returns the gathered form data.
		 *
		 * @return {Object} Form data.
		 */
		function _getFormData() {
			// Form data object.
			const data = {
				id: $this.data('id')
			};
			
			// Extend form data object with all necessary properties.
			return $.extend(true, data, _getMasterData(), _getStaticSeoUrlContentsData());
		}
		
		/**
		 * Returns the static_seo_url's master data.
		 *
		 * @return {Object} Static seo urls master data.
		 */
		function _getMasterData() {
			const name = $masterDataPanel
				.find('input[name="name"]')
				.val();
			
			const sitemapEntry = $masterDataPanel
				.find('input[name="sitemap_entry"]')
				.is(":checked");
			
			const changefreq = $masterDataPanel
				.find('select[name="changefreq"]')
				.val();
			
			const priority = $masterDataPanel
				.find('select[name="priority"]')
				.val();
			
			const robotsEntry = $masterDataPanel
				.find('input[name="robots_disallow_entry"]')
				.is(":checked");
			
			return {name, sitemapEntry, changefreq, priority, robotsEntry};
		}
		
		/**
		 * Returns the static seo url contents data by iterating over the tab content elements
		 * which represent a container for each language.
		 *
		 * The returned object contains a property for each language.
		 * The key is the language code and the value is an array containing
		 * all contents information as collection.
		 *
		 * Example output:
		 * {
		 *   de: [{
		 *     id: 1,
		 *     title: 'Welcome...',
		 *     ...
		 *   }]
		 * }
		 *
		 * @return {Object} Static seo url contents data.
		 */
		function _getStaticSeoUrlContentsData() {
			// Static seo url data object.
			const staticSeoUrlContents = {};
			
			// Iterate over each content panel container.
			$tabContents.each((index, element) => {
				// Static seo url panel container element.
				const $staticSeoUrlContentPanelContainer = $(element);
				
				// Static seo url panel elements.
				const $staticSeoUrlContentPanels = $staticSeoUrlContentPanelContainer.find('.panel');
				
				// Get static seo url panel container language code.
				const languageCode = $staticSeoUrlContentPanelContainer.data('language');
				
				// Add property to static seo url contents data object,
				// which contains the language code as key and the static seo url contents data as value.
				staticSeoUrlContents[languageCode] =
					$.map($staticSeoUrlContentPanels, element => _getStaticSeoUrlContentData(element));
			});
			
			return {staticSeoUrlContents};
		}
		
		/**
		 * Returns the data for a static seo url.
		 *
		 * @param {HTMLElement} staticSeoUrlContentPanel Static seo url content panel element.
		 *
		 * @return {Object} Static seo url data.
		 */
		function _getStaticSeoUrlContentData(staticSeoUrlContentPanel) {
			const $element = $(staticSeoUrlContentPanel);
			return {
				id: $element.data('id'),
				title: $element.find('input[name="title"]').val(),
				description: $element.find('textarea[name="description"]').val(),
				keywords: $element.find('textarea[name="keywords"]').val()
			};
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Listen to form submit event.
			$this.on('submit', _onFormSubmit);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
)
;