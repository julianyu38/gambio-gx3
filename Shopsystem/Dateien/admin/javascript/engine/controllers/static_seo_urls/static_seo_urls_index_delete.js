/* --------------------------------------------------------------
 static_seo_urls_index_delete.js 2017-05-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Static Seo Urls Overview Delete
 *
 * Handles the delete operation of the static seo urls overview page.
 */
gx.controllers.module(
	'static_seo_urls_index_delete',
	
	[
		'modal',
		`${gx.source}/libs/info_box`
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Static seo url delete confirmation modal.
		 *
		 * @type {jQuery}
		 */
		const $modal = $('.delete-static-seo-url.modal');
		
		/**
		 * Delete button selector string.
		 *
		 * @type {String}
		 */
		const deleteButtonSelector = '.btn-delete';
		
		/**
		 * Delete static seo url action URL.
		 *
		 * @type {String}
		 */
		const url = `${jse.core.config.get('appUrl')}/admin/admin.php?do=StaticSeoUrlAjax/DeleteStaticSeoUrl`;
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Shows the submit error message modal.
		 */
		function _showFailMessage() {
			// Message texts.
			const errorTitle = jse.core.lang.translate('DELETE_STATIC_SEO_URL_ERROR_TITLE', 'static_seo_urls');
			const errorMessage = jse.core.lang.translate('DELETE_STATIC_SEO_URL_ERROR_TEXT', 'static_seo_urls');
			
			// Show modal.
			jse.libs.modal.showMessage(errorTitle, errorMessage);
		}
		
		/**
		 * Handles the delete click event by opening the delete confirmation modal.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onDeleteClick(event) {
			// Prevent default action.
			event.preventDefault();
			
			// Table row.
			const $parentTableRow = $(event.target).parents('tr');
			
			// Delete confirmation modal button.
			const $confirmButton = $modal.find('button.confirm');
			
			// Empty modal body.
			$modal.find('.modal-body .form-group').remove();
			
			// Static seo url data.
			const staticSeoUrlData = {
				id: $parentTableRow.data('staticSeoUrlId'),
				name: $parentTableRow.data('staticSeoUrlName')
			};
			
			// Put new static seo url information into modal body.
			$modal
				.find('.modal-body fieldset')
				.html(_generateStaticSeoUrlInfoMarkup(staticSeoUrlData));
			
			// Show modal.
			$modal.modal('show');
			
			// Handle delete confirmation modal button click event.
			$confirmButton
				.off('click')
				.on('click', () => _onConfirmButtonClick(staticSeoUrlData.id));
		}
		
		/**
		 * Handles the delete confirmation button click event by removing the static seo url through an AJAX request.
		 *
		 * @param {Number} staticSeoUrlId Static Seo Url ID.
		 */
		function _onConfirmButtonClick(staticSeoUrlId) {
			// AJAX request options.
			const requestOptions = {
				type: 'POST',
				data: {staticSeoUrlId},
				url
			};
			
			// Perform request.
			$.ajax(requestOptions)
				.done(response => _handleDeleteRequestResponse(response, staticSeoUrlId))
				.fail(_showFailMessage)
				.always(() => $modal.modal('hide'));
		}
		
		/**
		 * Handles static seo url deletion AJAX action server response.
		 *
		 * @param {Object} response Server response.
		 * @param {Number} staticSeoUrlId ID of deleted static seo url.
		 */
		function _handleDeleteRequestResponse(response, staticSeoUrlId) {
			// Error message phrases.
			const errorTitle = jse.core.lang.translate('DELETE_STATIC_SEO_URL_ERROR_TITLE', 'static_seo_urls');
			const errorMessage = jse.core.lang.translate('DELETE_STATIC_SEO_URL_ERROR_TEXT', 'static_seo_urls');
			
			// Table body.
			const $tableBody = $this.find('tbody');
			
			// Table rows.
			const $rows = $tableBody.find(`[data-static-seo-url-id]`);
			
			// Table rows that will be deleted.
			const $rowToDelete = $rows.filter(`[data-static-seo-url-id="${staticSeoUrlId}"]`);
			
			// 'No results' message table row template.
			const $emptyRowTemplate = $('#template-table-row-empty');
			
			// Check for action success.
			if (response.includes('success')) {
				// Delete respective table rows.
				$rowToDelete.remove();
				
				// Add success message to admin info box.
				jse.libs.info_box.addSuccessMessage();
				
				// If there are no rows, show 'No results' message row.
				if (($rows.length - 1) < 1) {
					$tableBody
						.empty()
						.append($emptyRowTemplate.clone().html());
				}
			} else {
				// Show error message modal.
				jse.libs.modal.showMessage(errorTitle, errorMessage);
			}
		}
		
		/**
		 * Generates HTML containing the static seo url information for the delete confirmation modal.
		 *
		 * @param {Object} data Static seo url data.
		 *
		 * @return {String} Created HTML string.
		 */
		function _generateStaticSeoUrlInfoMarkup(data) {
			// Label phrases.
			const staticSeoUrlNameLabel = jse.core.lang.translate('NAME', 'static_seo_urls');
			
			// Return markup.
			return `
					<div class="form-group">
						<label class="col-md-5">${staticSeoUrlNameLabel}</label>
						<div class="col-md-7">${data.name}</div>
					</div>
			`;
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Listen to form submit event.
			$this.on('click', deleteButtonSelector, _onDeleteClick);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
);
