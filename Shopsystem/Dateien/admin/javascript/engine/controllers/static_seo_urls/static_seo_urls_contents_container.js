/* --------------------------------------------------------------
 static_seo_urls_contents_container.js 2017-05-24
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Static Seo Url Content Container (Tabs)
 *
 * Handles the static seo url content container functionality in the static seo url details page.
 */
gx.controllers.module(
	'static_seo_urls_contents_container',
	
	[
		`${jse.source}/vendor/jquery-ui/jquery-ui.min.css`,
		`${jse.source}/vendor/jquery-ui/jquery-ui.min.js`,
		'modal'
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Save Bar
		 * @type {jQuery}
		 */
		const saveBar = $('.bottom-save-bar');
		
		/**
		 * Elements
		 *
		 * @type {Object}
		 */
		const elements = {
			// Buttons.
			buttons: {
				
				// Submit button group.
				submit: saveBar.find('.submit-button-group'),
				
				// Submit button for save static seo url
				submitSave: saveBar.find('.save'),
				
				// Submit button for save and refresh static seo url
				submitRefresh: saveBar.find('.refresh'),
			},
			
			// Template.
			templates: {},
			
			// Modals.
			modals: {},
			
			// Tabs.
			tabHeader: $this.find('.nav-tabs'),
			
			// Form.
			form: $('.static-seo-urls-details > form')
		};
		
		/**
		 * Selector Strings
		 *
		 * @type {Object}
		 */
		const selectors = {
			// Icon selector strings.
			icons: {
				// Delete button on the panel header.
				delete: '.icon.delete',
				
				// Drag button on the panel header.
				drag: '.drag-handle',
				
				// Collapser button on the panel header.
				collapser: '.collapser',
				
				// Image delete button.
				imageDelete: '.action-icon.delete',
				
				// Image map edit button.
				imageMap: '.action-icon.image-map',
				
				// Upload image button.
				upload: '.action-icon.upload'
			},
			
			// Inputs selector strings.
			inputs: {
				// General image select dropdowns.
				dropdown: '.dropdown-input',
				
				// Thumbnail dropdown.
				thumbnailImageDropdown: '[name="thumbnail"]',
				
				// Title.
				title: 'input[name="title"]',
				
				// File.
				file: '.file-input'
			},
			
			// Static seo url content panel.
			staticSeoUrlContentPanel: '.panel',
			
			// Tab body.
			tabBody: '.tab-pane',
			
			// Static seo url content panel title.
			staticSeoUrlContentPanelTitle: '.staticSeoUrlContent-title',
			
			// Setting row (form group).
			configurationRow: '.row.form-group',
			
			// Data list container for image map.
			imageMapDataList: '.image-map-data-list',
		};
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Handles the key-up event on the staticSeoUrlContent title input field.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onTitleKeyup(event) {
			// Title input field.
			const $input = $(event.target);
			
			// Static seo url content panel title element.
			const $title = $input
				.parents(selectors.staticSeoUrlContentPanel)
				.find(selectors.staticSeoUrlContentPanelTitle);
			
			// Transfer input value to staticSeoUrlContent panel title.
			$title.text($input.val());
		}
		
		/**
		 * Handles the mouse-enter event on a configuration row.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onConfigRowMouseEnter(event) {
			// Configuration row element.
			const $row = $(event.target);
			
			// Image map edit icon.
			const $imageMapIcon = $row.find(selectors.icons.imageMap);
			
			// Image map data container list element.
			const $list = $row.find(selectors.imageMapDataList);
			
			// Return immediately, if the image map edit icon does not exist.
			if (!$imageMapIcon.length || !$list.length) {
				return;
			}
			
			if ($list.children().length) {
				$imageMapIcon.removeClass('fa-external-link').addClass('fa-external-link-square');
			} else {
				$imageMapIcon.removeClass('fa-external-link-square').addClass('fa-external-link');
			}
		}
		
		/**
		 * Handles the click event on the save button.
		 */
		function _onSubmitSave() {
			elements
				.form
				.trigger('submit');
		}
		
		/**
		 * Handles the click event on the refresh list item in the submit button group.
		 */
		function _onSubmitRefresh() {
			elements
				.form
				.trigger('submit', {refresh: true});
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			
			// Attach event handlers to sort actions, static seo url content panel delete button and inputs fields.
			$this
				.on('keyup', selectors.inputs.title, _onTitleKeyup)
				.on('mouseenter', selectors.configurationRow, _onConfigRowMouseEnter);
			
			// Attach event listeners to submit buttons.
			elements.buttons.submitSave.on('click', _onSubmitSave);
			elements.buttons.submitRefresh.on('click', _onSubmitRefresh);
			
			// Activate first tab.
			elements.tabHeader
				.children()
				.first()
				.addClass('active');
			
			// Activate first tab content.
			$this
				.find(selectors.tabBody)
				.first()
				.addClass('active in');
			
			// Trigger dropdown change event to hide the remove icon, if 'do not use' is selected.
			$this
				.find(selectors.inputs.dropdown)
				.trigger('change', [false]);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
);
