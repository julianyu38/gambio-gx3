/* --------------------------------------------------------------
 static_seo_urls_index_checkboxes.js 2017-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Static Seo Urls Overview Start Page Option
 *
 * Handles the switchers toggling.
 */
gx.controllers.module(
	'static_seo_urls_index_checkboxes',
	
	[
		'modal',
		`${gx.source}/libs/info_box`
	],
	
	function() {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		
		/**
		 * CSS class names.
		 *
		 * @type {Object}
		 */
		const classes = {
			switcher: 'switcher',
		};
		
		/**
		 * Selector Strings
		 *
		 * @type {Object}
		 */
		const selectors = {
			switcher: `.${classes.switcher}`,
			switcherCheckbox: `.${classes.switcher} :checkbox`,
		};
		
		
		/**
		 * URI map.
		 *
		 * @type {Object}
		 */
		const uris = {
			activate: `${jse.core.config.get('appUrl')}/admin/admin.php?do=StaticSeoUrlAjax/Activate`,
			deactivate: `${jse.core.config.get('appUrl')}/admin/admin.php?do=StaticSeoUrlAjax/Deactivate`,
		};
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Shows the submit error message modal.
		 */
		function _showFailMessage() {
			// Message texts.
			const errorTitle = jse.core.lang.translate('STATIC_SEO_URL_CHECKBOX_ERROR_TITLE', 'static_seo_urls');
			const errorMessage = jse.core.lang.translate('STATIC_SEO_URL_CHECKBOX_ERROR_TEXT', 'static_seo_urls');
			
			// Show modal.
			jse.libs.modal.showMessage(errorTitle, errorMessage);
		}
		
		/**
		 * Handles the switchers change events.
		 *
		 * @param {jQuery.Event} event Trigger event.
		 */
		function _onSwitcherChange(event) {
			// Clicked element.
			const $target = $(event.target);
			
			// Clicked switcher element.
			const $clickedSwitcher = $target.hasClass(classes.switcher) ? $target : $target.parents(selectors.switcher);
			
			// Clicked static seo url ID.
			const staticSeoUrlId = $clickedSwitcher.parents('tr').data('static-seo-url-id');
			
			// Is staticPage set as start page static page?
			const isActive = !$clickedSwitcher.hasClass('checked');
			
			// Which field should be updated
			const fieldName = $target.attr('name');
			
			// Disable all switchers.
			_toggleSwitchers(false);
			
			// Activate or deactivate static seo url depending on the state.
			isActive ? _deactivate(staticSeoUrlId, $clickedSwitcher, fieldName) : _activate(staticSeoUrlId, $clickedSwitcher, fieldName);
		}
		
		/**
		 * Deactivates the static seo url.
		 *
		 * @param {Number} staticSeoUrlId Static seo url ID.
		 * @param {jQuery} $clickedSwitcher Clicked static seo url element.
		 * @param {String} fieldName Field name to be deactivated.
		 */
		function _deactivate(staticSeoUrlId, $clickedSwitcher, fieldName) {
			// Request options.
			const requestOptions = {
				type: 'POST',
				data: {staticSeoUrlId, fieldName},
				url: uris.deactivate,
			};
			
			// Handles the 'always' case by enabling the clicked static seo url.
			const handleAlways = () => $clickedSwitcher.removeClass('disabled');
			
			// Handles the 'done' case with the server response.
			const handleDone = response => {
				// Enable all switchers.
				_toggleSwitchers(true);
				
				// Notify user.
				_notify(response.includes('success'));
			};
			
			// Perform request.
			$.ajax(requestOptions)
				.done(handleDone)
				.fail(_showFailMessage)
				.always(handleAlways);
		}
		
		/**
		 * Activates the static seo url.
		 *
		 * @param {Number} staticSeoUrlId Static seo url ID.
		 * @param {jQuery} $clickedSwitcher Clicked static seo url element.
		 * @param {String} fieldName Field name to be activated.
		 */
		function _activate(staticSeoUrlId, $clickedSwitcher, fieldName) {
			// Request options.
			const requestOptions = {
				type: 'POST',
				data: {staticSeoUrlId, fieldName},
				url: uris.activate,
			};
			
			// Handles the 'always' case by enabling the clicked static seo url.
			const handleAlways = () => _toggleSwitchers(true);
			
			// Handles the 'done' case with the server response.
			const handleDone = response => {
				// Clicked switcher's checkbox.
				const $checkbox = $clickedSwitcher.find(':checkbox');
				
				// Enable all switchers.
				_toggleSwitchers(true);
				
				// Check switcher.
				$checkbox.switcher('checked', true);
				
				// Notify user.
				_notify(response.includes('success'));
			};
			
			// Perform request.
			$.ajax(requestOptions)
				.done(handleDone)
				.fail(_showFailMessage)
				.always(handleAlways);
		}
		
		/**
		 * If the server response is successful, it removes any previous messages and
		 * adds new success message to admin info box.
		 *
		 * Otherwise its shows an error message modal.
		 *
		 * @param {Boolean} isSuccessful Is the server response successful?
		 */
		function _notify(isSuccessful) {
			if (isSuccessful) {
				jse.libs.info_box.deleteBySource('adminAction').then(() => jse.libs.info_box.addSuccessMessage())
			} else {
				jse.libs.modal.showMessage(
					jse.core.lang.translate('STATIC_SEO_URL_CHECKBOX_ERROR_TITLE', 'static_seo_urls'),
					jse.core.lang.translate('STATIC_SEO_URL_CHECKBOX_ERROR_TEXT', 'static_seo_urls')
				);
			}
		}
		
		/**
		 * Enables or disables the switchers.
		 *
		 * @param {Boolean} doEnable Enable the switchers?
		 */
		function _toggleSwitchers(doEnable) {
			$this.find(selectors.switcher)[`${doEnable ? 'remove' : 'add'}Class`]('disabled');
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Listen to set start page event.
			$this.on('change', selectors.switcher, _onSwitcherChange);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
);
