/* --------------------------------------------------------------
 sortable.js 2018-01-05
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Content manager controller to implement sortable functionality for pages.
 */
gx.controllers.module(
    'sortable',

    [
        `${jse.source}/vendor/jquery-ui/jquery-ui.min.css`,
        `${jse.source}/vendor/jquery-ui/jquery-ui.js`
    ],

    function () {
        'use strict';

        const SORTING_ID_ATTRIBUTE = 'data-sorting-id';

        const SAVE_SORTING_REQUEST_URL = 'admin.php?do=ContentManagerPagesAjax/SavePagesSorting';
        const SAVE_SORTING_REQUEST_METHOD = 'POST';

        const $errorModal = $('.sorting-failed.modal');
        const $emptyListTemplate = $('#empty-list');
        const $lists = $('#pages_main, #pages_secondary, #pages_info, #pages_info_box');

        const sortableOptions = {
            items: 'li.content-manager-element',
            axis: 'y',
            cursor: 'move',
            handle: '.sort-handle',
            containment: '#main-content',
            connectWith: '.content-manager-container',
            placeholder: 'col-md-12 content-manager-element sort-placeholder'
        };

        function init(done) {
            $lists
                .sortable(sortableOptions)
                .on('sortupdate', saveSorting)
                .disableSelection();

            done();
        }

        function handleError() {
            $errorModal.modal('show');
        }

        function handleResponses(...responses) {
            for (const response of responses) {
                const parsed = JSON.parse(response[0]);

                if (parsed[0] !== 'success') {
                    handleError();
                }
            }
        }

        function saveSorting(event, ui) {
            if (!ui.item.parent().is('ul')) {
                $lists.sortable('cancel');
            }

            const requests = [];

            function performRequest(index, element) {
                const $list = $(element);

                const ajaxOptions = {
                    url: SAVE_SORTING_REQUEST_URL,
                    method: SAVE_SORTING_REQUEST_METHOD,
                    data: {
                        position: $list.prop('id'),
                        pages: $list.sortable('toArray', {attribute: SORTING_ID_ATTRIBUTE})
                    }
                };

                const request = $.ajax(ajaxOptions);

                requests.push(request);

                updateEntryCount($list);
            }

            if (!ui.item.parents('.ui-sortable').is(this)) {
                return;
            }

            $lists.each(performRequest);

            $.when(...requests)
                .then(handleResponses)
                .fail(handleError);
        }

        function updateEntryCount($list) {
            const $container = $list.find('.content-manager-elements-list');
            const $entries = $container.find('.content-manager-element');
            const $emptyListElement = $entries.not(`[${SORTING_ID_ATTRIBUTE}]`);

            if ($entries.length - $emptyListElement.length === 0) {
                $container
                    .empty()
                    .append($emptyListTemplate.clone().html());
            } else {
                $emptyListElement.remove();
            }
        }

        return {init};
    });
