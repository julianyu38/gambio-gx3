/* --------------------------------------------------------------
 pages_form.js 2017-09-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
	'type_selection',
	
	[
	],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Module Selector
			 *
			 * @type {jQuery}
			 */
			$this = $(this),
			
			/**
			 * Dropdown for the selection
			 *
			 * @type {jQuery}
			 */
			$selection =  $(this).find('.content-manager-type-selection'),
			
			$contentContainer = $(this).find('.content-manager-type-selection-content'),
			
			/**
			 * Corresponding contents for this selection
			 *
			 * @type {jQuery}
			 */
			$contents = $contentContainer.children('div'),
			
			/**
			 * Save buttons
			 * 
			 * @type {jQuery}
			 */
			$saveButtons = $('#main-footer .bottom-save-bar').find('button[type="submit"]'),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {
			},
			
			/**
			 * Final Options
			 *
			 * @type {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			
			/**
			 * URLs for deleting different types of content
			 * 
			 * @type {{page: string, element: string, file: string, link: string}}
			 */
			urls = {
			},
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// EVENT HANDLERS
		// ------------------------------------------------------------------------
		
		/**
		 * Click handler for the tabs onClick the content gets switched.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		const _selectionChanged = function() {
			$contents.hide();
			$contentContainer
				.find('.' + $selection.val())
				.show();
			$saveButtons.attr('form', $selection.val());
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			_selectionChanged();
			
			$selection
				.on('change', _selectionChanged);
			
			done();
		};
		
		return module;
	});