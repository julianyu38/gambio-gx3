/* --------------------------------------------------------------
 save_bar.js 2017-11-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module( 'save_bar', [],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		 /**
		  * Module Selector
		  *
		  * @type {jQuery}
		  */
		 const $this = $(this);
		
		 /**
		  * Bottom save bar
		  *
		  * @type {jQuery}
		  */
		 const $bottomSaveBar = $('.bottom-save-bar');
		
		 /**
		  * Save button
		  *
		  * @type {jQuery}
		  */
		 const $saveButton = $bottomSaveBar.find('button[name="save"]');
		
		/**
		 * Update button
		 *
		 * @type {jQuery}
		 */
			const $updateButton = $bottomSaveBar.find('button[name="update"]');
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			const defaults = {};
			
			/**
			 * Final Options
			 *
			 * @type {object}
			 */
			const options = $.extend(true, {}, defaults, data);
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			const module = {};
		
		// ------------------------------------------------------------------------
		// EVENT HANDLERS
		// ------------------------------------------------------------------------
		
		// Handle save button click
		$saveButton.on('click', (event) => {
			event.preventDefault();
			const formName = $(this).attr('form');
			const $form = $('form#' + formName);
			
			$form.submit();
		});
		
		// Handle update button click
		$updateButton.on('click', (event) => {
			event.preventDefault();
			const formName = $(this).attr('form');
			const $form = $('form#' + formName);
			const formURL = $form.attr('action');
			
			// Add another parameter, so that we stay on the same page after
			// the form was submitted
			$form.attr('action', formURL + '&update=1');
			
			$form.submit();
		});
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			done();
		};
		
		return module;
	});
