/* --------------------------------------------------------------
 content_manager_delete.js 2017-09-07
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Content Manager Overview Delete
 *
 * Controller Module to delete Content Manger Entries and product contents.
 *
 * Handles the delete operation of the Content Manager overview pages.
 */
gx.controllers.module(
	'content_manager_delete',
	
	[
		'modal',
		`${gx.source}/libs/info_box`
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Content Manager delete confirmation modal.
		 *
		 * @type {jQuery}
		 */
		const $modal = $('.delete-content.modal');
		
		/**
		 * Content Manager elements container
		 * 
		 * @type {jQuery}
		 */
		const $elementContainer = $('.element-container');
		
		/**
		 * Selectors for deleting different types of contents
		 * 
		 * @type {{basic: string, page: string, element: string}}
		 */
		const deleteSelectors = {
			basic: '.delete-content',
			page: '.delete-content.page',
			element: '.delete-content.element',
		};
		
		/**
		 * URLs for deleting different types of content
		 * 
		 * @type {{page: string, element: string}}
		 */
		const urls = {
			page: `${jse.core.config.get('appUrl')}/admin/admin.php?do=ContentManagerPagesAjax/delete`,
			element: `${jse.core.config.get('appUrl')}/admin/admin.php?do=ContentManagerElementsAjax/delete`,
		}
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Handles the delete click event by opening the delete confirmation modal.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 * @param {String} url URL for AjaxRequest.
		 */
		function _onDeleteClick(event, url) {
			// Prevent default action.
			event.preventDefault();
			
			// Clicked content entry
			const $content_entry = $(event.target).closest('.content-action-icons').siblings('.content-name');
			
			// Id of the content that should be deleted
			const content_id = $(event.target).parents(deleteSelectors.basic).data('contentId');
			
			// Group Id of the content that should be deleted
			const group_id = $(event.target).parents(deleteSelectors.basic).data('groupId');
			
			// Delete confirmation modal button.
			const $confirmButton = $modal.find('button.confirm');
			
			// Empty modal body.
			$modal.find('.modal-body .form-group').remove();
			
			// Slider data.
			const contentData = {
				id: content_id,
				groupId: group_id,
				name: $content_entry.text()
			};
			
			// Put new slider information into modal body.
			$modal
				.find('.modal-body fieldset')
				.html(_generateContentInfoMarkup(contentData));
			
			// Show modal.
			$modal.modal('show');
			
			// Handle delete confirmation modal button click event.
			$confirmButton
				.off('click')
				.on('click', () => _onConfirmButtonClick(contentData.id, contentData.groupId, url));
		}
		
		/**
		 * Handles the delete confirmation button click event by removing the content through an AJAX request.
		 *
		 * @param {Number} contentId Content ID.
		 * @param {Number} groupId Group ID.
		 * @param {String} url URL for AjaxRequest.
		 */
		function _onConfirmButtonClick(contentId, groupId, url) {
			// AJAX request options.
			const requestOptions = {
				type: 'POST',
				data: {id: groupId},
				url
			};
			
			// Perform request.
			$.ajax(requestOptions)
				.done(response => _handleDeleteRequestResponse(response, contentId))
				.always(() => $modal.modal('hide'));
		}
		
		/**
		 * Handles content deletion AJAX action server response.
		 *
		 * @param {Object} response Server response.
		 * @param {Number} contentId ID of deleted content.
		 */
		function _handleDeleteRequestResponse(response, contentId) {
			// Error message phrases.
			const errorTitle = jse.core.lang.translate('DELETE_CONTENT_ERROR_TITLE', 'content_manager');
			const errorMessage = jse.core.lang.translate('DELETE_CONTENT_ERROR_TEXT', 'content_manager');
			
			// Element that triggers the delete action
			const $delete_trigger = $elementContainer.find('.delete-content'+`[data-content-id="${contentId}"]`);
			
			// List element that should be removed
			const $element_to_delete = $delete_trigger.closest('li.content-manager-element');
			
			// List of all siblings of the removed element
			const $list_elements = $delete_trigger.closest('li.content-manager-element').siblings();
			
			// Element that contains all content list items
			const $list = $element_to_delete.closest('ul.content-manager-elements-list')
			
			// 'No results' message list element template.
			const $emptyListTemplate = $('#empty-list');
			
			// Check for action success.
			if (response.includes('success')) {
				// Delete respective table rows.
				$element_to_delete.remove();
				
				// Add success message to admin info box.
				jse.libs.info_box.addSuccessMessage();
				
				// If there are no rows, show 'No results' message row.
				if (($list_elements.length) < 1) {
					$list
						.empty()
						.append($emptyListTemplate.clone().html());
				}
			} else {
				// Show error message modal.
				jse.libs.modal.showMessage(errorTitle, errorMessage);
			}
		}
		
		/**
		 * Generates HTML containing the content entry information for the delete confirmation modal.
		 *
		 * @param {Object} data Content Manager data.
		 * @param {String} data.name Name of the Content Manager entry.
		 * @param {Number} data.groupId Id of the Content Manager entry.
		 *
		 * @return {String} Created HTML string.
		 */
		function _generateContentInfoMarkup(data) {
			// Label phrases.
			const contentNameLabel = jse.core.lang.translate('TEXT_TITLE', 'content_manager');
			const groupIdLabel = jse.core.lang.translate('TEXT_GROUP', 'content_manager');
			
			// Return markup.
			return `
					<div class="form-group">
						<label class="col-md-5">${contentNameLabel}</label>
						<div class="col-md-7">${data.name}</div>
					</div>

					<div class="form-group">
						<label class="col-md-5">${groupIdLabel}</label>
						<div class="col-md-7">${data.groupId}</div>
					</div>
			`;
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Listen to click event on delete icon
			$this.on('click', deleteSelectors.page, function(event) {
				_onDeleteClick(event, urls.page);
			});
			
			$this.on('click', deleteSelectors.element, function(event) {
				_onDeleteClick(event, urls.element);
			});
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
);