/* --------------------------------------------------------------
 content_manager_preview.js 2017-09-07
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Content Manager Preview
 *
 * Controller Module to open preview modal
 *
 * Opens a modal with the preview of the selected Content Manager entry.
 */
gx.controllers.module(
	'content_manager_preview',
	
	[
		'modal'
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Preview modal.
		 *
		 * @type {jQuery}
		 */
		const $modal = $('.preview.modal');
		
		/**
		 * Open preview icon selector string.
		 *
		 * @type {String}
		 */
		const openPreviewSelector = '.open-preview';
		
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		function _onPreviewClick(event) {
			event.preventDefault();
			
			// Iframe that should show the preview
			const $iframe = $modal.find('.modal-body iframe');
			
			// Id of the content that should be shown in preview modal
			const content_id = $(event.target).parents(openPreviewSelector).data('contentId');
			
			// Url of link element that should be shown in preview modal
			const link_url = $(event.target).parents(openPreviewSelector).attr('href');
			
			// Check if a link URL is set. If not use the content id to display a preview
			if (link_url !== '#' && link_url.indexOf('http') >= 0) {
				$iframe.css('display', 'none');
				$modal.find('.modal-body').append(_generateContentInfoMarkup(link_url));
			} else if (link_url !== '#' && link_url.indexOf('http') < 0) {
				$iframe.css('display', 'block');
				$iframe.attr('src', `${jse.core.config.get('appUrl')}/` + link_url);
			} else {
				$iframe.css('display', 'block');
				$iframe.attr('src', `${jse.core.config.get('appUrl')}/admin/content_preview.php?coID=` + content_id);
			}
			
			// Display the preview modal
			$modal.modal('show');
		}
		
		
		/**
		 * Generates HTML containing the external link information.
		 *
		 * @param {String} data Link.
		 *
		 * @return {String} Created HTML string.
		 */
		function _generateContentInfoMarkup(data) {
			// Label phrases.
			const externalLinkInfo = jse.core.lang.translate('TEXT_EXTERNAL_LINK_INFO', 'content_manager');
			
			// Return markup.
			return `
					<div class="col-md-12">
						${data} ${externalLinkInfo}
					</div>
			`;
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Listen to click event on preview icon
			$this.on('click', openPreviewSelector, _onPreviewClick);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
);
