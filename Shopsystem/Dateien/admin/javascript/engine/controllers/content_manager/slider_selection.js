/* --------------------------------------------------------------
 slider_selection.js 2017-11-27
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
	'slider_selection', [],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
			/**
			 * Module Selector
			 *
			 * @type {jQuery}
			 */
			const $this = $(this);
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			const defaults = {
				contentSliderSelector: '.content-slider-selection',
				formSelector: 'form.content-manager-form',
				hiddenFieldSelector: 'input.content-manager-slider-id',
			};
			
			/**
			 * Final Options
			 *
			 * @type {object}
			 */
			const options = $.extend(true, {}, defaults, data);
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			const module = {};
		
		// ------------------------------------------------------------------------
		// EVENT HANDLERS
		// ------------------------------------------------------------------------
		
		/**
		 * Change handler for content slider selection.
		 */
		const _selectionChanged = () => {
			$(options.formSelector + ' ' + options.hiddenFieldSelector).val($this.find(options.contentSliderSelector).val());
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			_selectionChanged();
			
			$this.on('change', options.contentSliderSelector, _selectionChanged);
			
			done();
		};
		
		return module;
	});