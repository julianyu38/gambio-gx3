/* --------------------------------------------------------------
 product_content.js 2017-09-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
	'overview',
	
	[
		`${jse.source}/vendor/datatables/jquery.dataTables.min.css`,
		`${jse.source}/vendor/datatables/jquery.dataTables.min.js`,
		'datatable'
	],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES DEFINITION
		// ------------------------------------------------------------------------
		
		var
			/**
			 * Module Selector
			 *
			 * @type {object}
			 */
			$this = $(this),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {
				filterColumnIndex: 0,
				filterRegexPrefix: '^',
                expert: ''
			},
			
			/**
			 * Final Options
			 *
			 * @type {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			
			/**
			 * URLs for deleting different types of content
			 * 
			 * @type {{page: string, element: string, file: string, link: string}}
			 */
			urls = {
				page: `${jse.core.config.get('appUrl')}/admin/admin.php?do=ContentManagerAjax/Delete`,
				element: `${jse.core.config.get('appUrl')}/admin/admin.php?do=ContentManagerAjax/Delete`,
				file: `${jse.core.config.get('appUrl')}/admin/admin.php?do=ContentManagerProductContentsAjax/deleteFile`,
				link: `${jse.core.config.get('appUrl')}/admin/admin.php?do=ContentManagerProductContentsAjax/deleteLink`
			},
			
			/**
			 * Content Manager delete confirmation modal.
			 *
			 * @type {jQuery}
			 */
			$modal = $('.delete-content.modal'),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		function _createTopBar($table) {
			
			
			$table.parent().prepend(`
				<div class="row headline">
					<div class="col-md-6 quick-search">
						<form class="form-inline form-group remove-padding">
							<label for="search-keyword">` + jse.core.lang.translate('search', 'admin_labels') + `</label>
							<input type="text" class="search-keyword" />
						</form>
					</div>
					<div class="col-md-6">

						<div data-gx-widget="button_dropdown"
						     data-button_dropdown-user_id="{$smarty.session.customer_id}"
						>
							<div data-use-button_dropdown="true"
							     data-custom_caret_btn_class="btn-success"
							     class="pull-right"
							>
								<button data-gx-extension="link" data-link-url="admin.php?do=ContentManagerProductContents/new&type=file` + options.expert + `" class="btn btn-success">
									<i class="fa fa-plus btn-icon"></i>
									` + jse.core.lang.translate('BUTTON_NEW_PREFIX', 'admin_buttons') + `
									` + jse.core.lang.translate('LABEL_PRODUCT_CONTENT_FILE', 'content_manager') + `
								</button>
								<ul>
									<li><span data-gx-extension="link" data-link-url="admin.php?do=ContentManagerProductContents/new&type=link` + options.expert + `">
									` + jse.core.lang.translate('BUTTON_NEW_PREFIX_2', 'admin_buttons') + `
									` + jse.core.lang.translate('LABEL_PRODUCT_CONTENT_LINK', 'content_manager') + `</span></li>
									<li><span data-gx-extension="link" data-link-url="admin.php?do=ContentManagerProductContents/new&type=text` + options.expert + `">
									` + jse.core.lang.translate('BUTTON_NEW_PREFIX_2', 'admin_buttons') + `
									` + jse.core.lang.translate('LABEL_PRODUCT_CONTENT_TEXT', 'content_manager') + `</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			`);
			
			gx.widgets.init($table.parent());
			gx.extensions.init($table.parent());
		}
		
		function _createBottomBar($table) {
			const $paginator = $('<div class="paginator" />');
			const $datatableComponents = $('<div class="row datatable-components" />');
			
			const $pageLength = $('<select class="page-length" />');
			$pageLength
				.append(new Option('20 ' + jse.core.lang.translate('PER_PAGE', 'admin_general'), 20, true, true))
				.append(new Option('30 ' + jse.core.lang.translate('PER_PAGE', 'admin_general')), 30)
				.append(new Option('50 ' + jse.core.lang.translate('PER_PAGE', 'admin_general')), 50)
				.append(new Option('100 ' + jse.core.lang.translate('PER_PAGE', 'admin_general')), 100)
				.css('float', 'left')
				.appendTo($datatableComponents);
			
			$table.siblings('.dataTables_info')
				.appendTo($datatableComponents)
				.css('clear', 'none');
			
			$table.siblings('.dataTables_paginate')
				.appendTo($datatableComponents)
				.css('clear', 'none');
			
			$paginator
				.append($datatableComponents);
			
			$table.parent().append($paginator);
		}
		
		function _onQuickSearchSubmit(event) {
			event.preventDefault();
			
			const $table = $(this).parent().siblings('.data-table');
			const keyword = $(this).find('.search-keyword').val();
			
			$table.DataTable().search(keyword, true, false).draw();
		}
		
		function _onPageLengthChange() {
			const $table = $(this).parents('.paginator').siblings('.data-table');
			$table.DataTable().page.len($(this).val()).draw();
		}
		
		/**
		 * Handles the delete click event by opening the delete confirmation modal.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 * @param {String} url URL for AjaxRequest.
		 * @param {Object} $table DataTable object
		 */
		function _onDeleteClicked(event, url, $table) {
			// Prevent default action.
			event.preventDefault();
			
			// Clicked content entry
			const $content_entry = $(event.target).closest('.content-action-icons').siblings('.content-name');
			
			// Id of the content that should be deleted
			const content_id = $(event.target).parents('.delete-content').data('contentId');
			
			// Group Id of the content that should be deleted
			const group_id = $(event.target).parents('.delete-content').data('groupId');
			
			// Delete confirmation modal button.
			const $confirmButton = $modal.find('button.confirm');
			
			// Empty modal body.
			$modal.find('.modal-body .form-group').remove();
			
			// Slider data.
			const contentData = {
				id: content_id,
				groupId: group_id,
				name: $content_entry.text()
			};
			
			// Put new slider information into modal body.
			$modal
				.find('.modal-body fieldset')
				.html(_generateContentInfoMarkup(contentData));
			
			// Show modal.
			$modal.modal('show');
			
			// Handle delete confirmation modal button click event.
			$confirmButton
				.off('click')
				.on('click', () => _onConfirmButtonClick(contentData.groupId, url, $table));
		}
		
		/**
		 * Handles the delete confirmation button click event by removing the content through an AJAX request.
		 *
		 * @param {Number} groupId Group ID.
		 * @param {String} url URL for AjaxRequest.
		 * @param {Object} $table DataTable object
		 */
		function _onConfirmButtonClick(groupId, url, $table) {
			// AJAX request options.
			const requestOptions = {
				type: 'POST',
				data: {id: groupId},
				url
			};
			
			// Perform request.
			$.ajax(requestOptions)
				.done(response => _handleDeleteRequestResponse(response, $table, groupId))
				.always(() => $modal.modal('hide'));
		}
		
		/**
		 * Handles content deletion AJAX action server response.
		 * 
		 * @param {Object} response Server response.
		 * @param {Object} $table DataTable object
		 * @param {Number} groupId ID of deleted content.
		 */
		function _handleDeleteRequestResponse(response, $table, groupId) {
			// Error message phrases.
			const errorTitle = jse.core.lang.translate('DELETE_CONTENT_ERROR_TITLE', 'content_manager');
			const errorMessage = jse.core.lang.translate('DELETE_CONTENT_ERROR_TEXT', 'content_manager');
			
			// Element that triggers the delete action
			const $delete_trigger = $('.delete-content'+`[data-group-id="${groupId}"]`);
			
			// List element that should be removed
			const $element_to_delete = $delete_trigger.closest('.content-manager-element');
			
			// Check for action success.
			if (response.includes('success')) {
				// Delete respective table rows.
				
				$table.row($element_to_delete).remove().draw();
				
				// Add success message to admin info box.
				jse.libs.info_box.addSuccessMessage();
			} else {
				// Show error message modal.
				jse.libs.modal.showMessage(errorTitle, errorMessage);
			}
		}
		
		/**
		 * Generates HTML containing the content entry information for the delete confirmation modal.
		 *
		 * @param {Object} data Content Manager data.
		 * @param {String} data.name Name of the Content Manager entry.
		 * @param {Number} data.groupId Id of the Content Manager entry.
		 *
		 * @return {String} Created HTML string.
		 */
		function _generateContentInfoMarkup(data) {
			// Label phrases.
			const contentNameLabel = jse.core.lang.translate('TEXT_TITLE', 'content_manager');
			const groupIdLabel = jse.core.lang.translate('TEXT_GROUP', 'content_manager');
			
			// Return markup.
			return `
					<div class="form-group">
						<label class="col-md-5">${contentNameLabel}</label>
						<div class="col-md-7">${data.name}</div>
					</div>

					<div class="form-group">
						<label class="col-md-5">${groupIdLabel}</label>
						<div class="col-md-7">${data.groupId}</div>
					</div>
			`;
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			const $tables = $this.find('.data-table');
			
			$tables.on('init.dt', function(event, settings, json) {
				const $table = $(this);
				_createTopBar($table);
				_createBottomBar($table);
			});
			
			const $table =jse.libs.datatable.create($tables, {
				autoWidth: false,
				dom: 'rtip',
				pageLength: 20,
				language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
				createdRow: function(row, data, dataIndex) {
					$(row).find('td').each(function() {
						$(this).html($(this).html().trim().replace(/(\r\n|\n|\r)/gm, ''));
					});
				}
			});
			
			$this
				.on('submit', '.quick-search', _onQuickSearchSubmit)
				.on('change', '.page-length', _onPageLengthChange)
				.on('click', '.delete-content.file', function(event) {
					_onDeleteClicked(event, urls.file, $table);
				})
				.on('click', '.delete-content.link', function(event) {
					_onDeleteClicked(event, urls.link, $table);
				});
			
			done();
		};
		
		return module;
	});