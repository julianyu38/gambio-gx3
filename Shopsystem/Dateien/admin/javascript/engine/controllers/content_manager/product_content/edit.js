gx.controllers.module(
	// controller name
	'edit',
	
	// controller libraries
	[],
	
	// controller business logic
	function(data) {
		'use strict';
		
		// variables
		/**
		 * Controller reference.
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Default options for controller,
		 *
		 * @type {object}
		 */
		const defaults = {};
		
		/**
		 * Final controller options.
		 *
		 * @type {object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module object.
		 *
		 * @type {{}}
		 */
		const module = {};
		
		// private methods
		
		/**
		 * Sets all event handler for the product content forms.
		 *
		 * @private
		 */
		const _setEventHandler = () => {
			$this.find('form').on('submit', _formValidation);
		};
		
		// event handler
		
		/**
		 * Validates the product contents creation and edit form.
		 * Ensures that the internal_name value is set.
		 *
		 * @param {jQuery.Event} e Event object.
		 * @private
		 */
		const _formValidation = e => {
			const $form = $(e.currentTarget);
			const $internalName = $form.find('input[name="content_manager[' + $form.attr('id')
				+ '][internal_name]"]');
			
			if ($internalName.val() === '') {
				e.stopPropagation();
				e.preventDefault();
				
				$internalName.parents('.form-group').addClass('has-error');
				return;
			}
			$internalName.parents('.form-group').removeClass('has-error');
		};
		
		// initialization
		module.init = done => {
			_setEventHandler();
			done();
		};
		
		return module;
	}
);