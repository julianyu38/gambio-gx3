/* --------------------------------------------------------------
 payment_shipping_matrix.js 2018-05-28
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
	// controller name
	'payment_shipping_matrix',
	
	// controller libraries
	['xhr'],
	
	// controller business logic
	function(data) {
		'use strict';
		
		// variables
		/**
		 * Controller reference.
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Default options for controller,
		 *
		 * @type {object}
		 */
		const defaults = {};
		
		/**
		 * Final controller options.
		 *
		 * @type {object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module object.
		 *
		 * @type {{}}
		 */
		const module = {};
		
		const $modalBody = $this.find('.modal-body');
		
		const $modalFooter = $this.find('.modal-footer');
		
		let $modalInfoRow;
		
		let $countrySwitcher;
		
		let $countrySelectionTable;
		
		let languages;
		
		let oldData;
		
		// private methods
		const _initCountrySelection = () => {
			$modalBody.empty().append(_renderModalInfo());
			_renderCountryTable(true)
		};
		
		const _renderModalInfo = () => {
			$modalInfoRow = $('<div/>', {'class': 'row', 'data-gx-widget': 'switcher'});
			const $descriptionCol = $('<div/>', {'class': 'col-md-6'}).appendTo($modalInfoRow);
			const $selectionCol = $('<div/>', {'class': 'col-md-6'}).appendTo($modalInfoRow);
			const $formGroup = $('<div/>', {'class': 'form-group'}).appendTo($selectionCol);
			
			$('<span/>', {
				'class': 'modal-info',
				'text': jse.core.lang.translate('select_countries', 'shipping_and_payment_matrix')
			}).appendTo($descriptionCol);
			
			$('<label/>', {
				'class': 'col-md-10 text-right',
				'text': jse.core.lang.translate('show_only_enabled_countries', 'shipping_and_payment_matrix')
			}).appendTo($formGroup);
			$countrySwitcher =
				$('<input/>', {
					'type': 'checkbox',
					'checked': true,
					'class': 'col-md-2 show-all-countries'
				}).appendTo($formGroup);
			
			return $modalInfoRow;
		};
		
		const _renderCountryTable = activeOnly => {
			jse.libs.xhr.get({
				'url': './admin.php?do=ShippingPaymentMatrixAjax/getCountryList'
			}).then(response => {
				let i = 0;
				languages = response.languages;
				oldData = [];
				
				if ($modalBody.find('table').length > 0) {
					$modalBody.find('table').remove();
				}
				
				$countrySelectionTable =
					$('<table/>', {'class': 'table', 'data-gx-widget': 'switcher'}).appendTo($modalBody);
				const $tHead = $('<thead/>').appendTo($countrySelectionTable);
				const $tBody = $('<tbody/>').appendTo($countrySelectionTable);
				const $headRow = $('<tr/>').appendTo($tHead);
				$('<th/>', {'text': jse.core.lang.translate('country', 'shipping_and_payment_matrix')})
					.appendTo($headRow);
				$('<th/>', {'text': jse.core.lang.translate('status', 'shipping_and_payment_matrix')})
					.appendTo($headRow);
				
				for (; i < response.countries.length; i++) {
					if (response.countries[i].data) {
						oldData.push(response.countries[i].data);
					}
					
					if (undefined !== activeOnly && activeOnly) {
						if (response.countries[i].active) {
							let $row = $('<tr/>').appendTo($tBody);
							$('<td/>', {'text': response.countries[i].name}).appendTo($row);
							let $statusCol = $('<td/>').appendTo($row);
							$('<input/>', {
								'type': 'checkbox',
								'checked': true,
								'value': response.countries[i].id,
								'data-code': response.countries[i].code,
								'data-lang-id': response.countries[i].languageId || ''
							}).appendTo($statusCol);
						}
					} else {
						let $row = $('<tr/>').appendTo($tBody);
						$('<td/>', {'text': response.countries[i].name}).appendTo($row);
						let $statusCol = $('<td/>').appendTo($row);
						$('<input/>', {
							'type': 'checkbox',
							'checked': response.countries[i].active === true,
							'value': response.countries[i].id,
							'data-code': response.countries[i].code,
							'data-lang-id': response.countries[i].languageId || ''
						}).appendTo($statusCol);
					}
				}
				
				$modalFooter.empty()
					.append($('<button/>', {
						'class': 'btn btn-default further',
						'type': 'button',
						'text': jse.core.lang.translate('button_forward', 'shipping_and_payment_matrix')
					}));
				
				gx.widgets.init($modalBody);
				_setEventListener();
			});
		};
		
		const _getSelectedCountries = () => {
			const countries = [];
			$countrySelectionTable.find('input[type="checkbox"]:checked').each((index, input) => {
				countries.push({
					'id': $(input).val(),
					'name': $(input).parents('tr').first().find('td').first().text(),
					'code': $(input).data('code'),
					'languageId': $(input).data('lang-id')
				});
			});
			
			return countries;
		};
		
		const _renderFormTable = countries => {
			const $nav = $('<ul/>', {
				'class': 'nav nav-tabs',
				'role': 'tablist'
			}).appendTo($modalBody);
			const $tabContent = $('<div>', {'class': 'tab-content'}).appendTo($modalBody);
			
			for (let i = 0; i < languages.length; i++) {
				let $navIcon = $('<img/>', {'src': '../lang/' + languages[i].directory.toLowerCase() + '/icon.gif'})
				let $navLink = $('<a/>', {
					'href': '#payment-shipping-matrix-form-' + languages[i].code,
					'data-language': languages[i].code,
					'aria-controls': 'home',
					'role': 'tab',
					'data-toggle': 'tab'
				}).append($navIcon);
				let $navItem = $('<li/>', {'role': 'presentation'}).append($navLink);
				$nav.append($navItem);
				
				let $tabPanel = $('<div/>', {
					'role': 'tabpanel',
					'class': 'tab-pane fade',
					'id': 'payment-shipping-matrix-form-' + languages[i].code,
					'data-language': languages[i].code
				}).appendTo($tabContent);
				
				let $matrixFormTable = $('<table/>', {'class': 'table'}).appendTo($tabPanel);
				
				if (i === 0) {
					$navItem.addClass('active');
					$tabPanel.addClass('active in');
				}
				
				const $tHead = $('<thead/>').appendTo($matrixFormTable);
				const $tBody = $('<tbody/>').appendTo($matrixFormTable);
				
				const $headRow = $('<tr/>').appendTo($tHead);
				$('<th/>', {'text': jse.core.lang.translate('country', 'shipping_and_payment_matrix')})
					.appendTo($headRow);
				$('<th/>', {'text': jse.core.lang.translate('shipping_costs', 'shipping_and_payment_matrix')})
					.appendTo($headRow);
				$('<th/>', {'text': jse.core.lang.translate('shipping_time', 'shipping_and_payment_matrix')})
					.appendTo($headRow);
				$('<th/>', {'text': jse.core.lang.translate('payment_methods', 'shipping_and_payment_matrix')})
					.appendTo($headRow);
				
				let j = 0;
				for (; j < countries.length; j++) {
					let $row = $('<tr/>').appendTo($tBody);
					let langCode = countries[j].code;
					
					$('<td/>', {'text': countries[j].name}).appendTo($row);
					
					let $shippingInfoTextArea = $('<textarea/>', {
						'name': 'shipping_info[' + languages[i].id + '][' + langCode + ']'
					});
					$('<td/>')
						.append($shippingInfoTextArea)
						.appendTo($row);
					
					let $shippingTimeTextArea = $('<textarea/>', {
						'name': 'shipping_time[' + languages[i].id + '][' + langCode + ']'
					});
					$('<td/>')
						.append($shippingTimeTextArea)
						.appendTo($row);
					
					let $paymentInfoTextArea = $('<textarea/>', {
						'name': 'payment_info[' + languages[i].id + '][' + langCode + ']'
					});
					$('<td/>')
						.append($paymentInfoTextArea)
						.appendTo($row);
					
					for (let k = 0; k < oldData.length; k++) {
						
						if (Number(languages[i].id) in oldData[k]) {
							if (countries[j].code.toUpperCase() in oldData[k][languages[i].id]) {
								$shippingInfoTextArea.val(oldData[k][languages[i].id][countries[j].code.toUpperCase()].shippingInfo);
								$shippingTimeTextArea.val(oldData[k][languages[i].id][countries[j].code.toUpperCase()].shippingTime);
								$paymentInfoTextArea.val(oldData[k][languages[i].id][countries[j].code.toUpperCase()].paymentInfo);
							}
						}
					}
				}
			}
			
			$countrySelectionTable.remove();
			_renderFormModalActions();
		};
		
		const _renderFormModalActions = () => {
			$modalFooter.empty();
			const $backBtn = $('<button/>', {
				'class': 'btn btn-default',
				'text': jse.core.lang.translate('button_back', 'shipping_and_payment_matrix')
			}).appendTo($modalFooter);
			const $storeBtn = $('<button/>', {
				'class': 'btn btn-primary',
				'text': jse.core.lang.translate('button_save_and_close', 'shipping_and_payment_matrix')
			}).appendTo($modalFooter);
			
			$backBtn.on('click', () => {
				$modalBody.empty().append(_renderModalInfo());
				_renderCountryTable(true);
			});
			$storeBtn.on('click', () => {
				jse.libs.xhr.post({
					'url': './admin.php?do=ShippingPaymentMatrixAjax/save',
					'data': $modalBody.find('textarea').serializeArray()
				}).then(r => {
					$this.modal('hide');
					jse.libs.info_box.addSuccessMessage(jse.core.lang.translate('save_success', 'shipping_and_payment_matrix'));
				});
			});
		};
		
		const _setEventListener = () => {
			$countrySwitcher.off().on('change', e => {
				_renderCountryTable($(e.currentTarget).prop('checked'))
			});
			$this.find('.further').off().on('click', e => {
				$modalInfoRow.remove();
				_renderFormTable(_getSelectedCountries());
			});
		};
		
		// event handler
		
		// initialization
		module.init = done => {
			// initialization logic
			
			$this.on('show.bs.modal', _initCountrySelection);
			
			done();
		}
		
		return module;
	}
); // way to easy