/* --------------------------------------------------------------
 create_missing_documents.js 2016-10-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('overview', ['modal', 'xhr', gx.source + '/libs/info_messages'], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Default Module Options
	 *
	 * @type {object}
	 */
	const defaults = {};
	
	/**
	 * Final Module Options
	 *
	 * @type {object}
	 */
	const options = $.extend(true, {}, defaults, data);
	
	/**
	 * Module Object
	 *
	 * @type {object}
	 */
	const module = {};
	
	/**
	 * Quantity Unit Creation modal.
	 *
	 * @type {*}
	 */
	const $creationModal = $('.create-modal');
	
	/**
	 * Quantity QuantityUnitQuantityUnitQuantityUnitQuantityUnitQuantityUnitQuantityUnitUnit edit modal.
	 *
	 * @type {*}
	 */
	const $editModal = $('.edit-modal');
	
	/**
	 * Quantity Unit remove confirmation modal.
	 *
	 * @type {*}
	 */
	const $removeConfirmationModal = $('.remove-confirmation-modal');
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Initializes the creation modal.
	 *
	 * @private
	 */
	const _initCreationModal = () => {
		const $inputs = $creationModal.find('input[type="text"]');
		let i = 0;
		
		for (; i < $inputs.length; i++) {
			$inputs[i].value = '';
			$($inputs[i]).parent().removeClass('has-error');
		}
		$creationModal.find('p.quantity-units-modal-info')
			.first()
			.text(jse.core.lang.translate('TEXT_INFO_INSERT_INTRO', 'quantity_units'))
			.removeClass('text-danger');
		$creationModal.modal('show');
	};
	
	/**
	 * Initializes the edit modal.
	 *
	 * @param e Event object to fetch targets id.
	 * @private
	 */
	const _initEditModal = e => {
		jse.libs.xhr.get({
			url: './admin.php?do=QuantityUnitAjax/getById&id=' + e.target.dataset.id
		}).done(r => {
			const $editModalForm = $editModal.find('form');
			const $idInput = $('<input/>').attr('type', 'hidden').attr('name', 'id');
			$editModalForm.empty();
			
			$idInput.val(r.id.toString());
			$editModalForm.append($idInput);
			$editModal.find('p.quantity-units-modal-info')
				.first()
				.text(jse.core.lang.translate('TEXT_INFO_EDIT_INTRO', 'quantity_units'))
				.removeClass('text-danger');
			
			for (let languageCode in r.names) {
				let $formGroup = $('<div/>').addClass('form-group');
				let $inputContainer = $('<div/>').addClass('col-sm-12');
				let $input = $('<input/>')
					.attr('type', 'text')
					.attr('data-gx-widget', 'icon_input')
					.attr('data-lang-id', r.languageIds[languageCode])
					.attr('name', 'name[' + r.languageIds[languageCode] + ']')
					.addClass('form-control')
					.val(r.names[languageCode]);
				
				$inputContainer.append($input);
				$formGroup.append($inputContainer);
				$editModalForm.append($formGroup);
			}
			
			gx.widgets.init($editModalForm);
			$editModal.modal('show');
		});
	}
	
	/**
	 * Initializes the remove confirmation modal.
	 *
	 * @param e Event object to fetch targets id.
	 * @private
	 */
	const _initRemoveConfirmationModal = e => {
		$removeConfirmationModal.modal('show');
		jse.libs.xhr.get({
			url: './admin.php?do=QuantityUnitAjax/getById&id=' + e.target.dataset.id
		}).done(r => {
			const $info = $('.quantity-units-modal-remove-info');
			$info.empty();
			$('.quantity-units-remove-id').val(r.id);
			
			for (let languageCode in r.names) {
				let newContent = languageCode + ': ' + r.names[languageCode] + '<br/>';
				let oldContent = $info.html();
				$info.html(oldContent + newContent);
			}
		});
	}
	
	/**
	 * Sends an ajax request to store a new quantity units entity.
	 *
	 * @private
	 */
	const _storeQuantityUnit = () => {
		const $button = $('#create-quantity-units')
		$button.prop('disabled', true);
		
		if (_validateInputFields('create')) {
			jse.libs.xhr.post({
				url: './admin.php?do=QuantityUnitAjax/store',
				data: _inputData('create')
			})
				.done(() => _renderTable(jse.core.lang.translate('TEXT_INFO_ADD_SUCCESS', 'quantity_units')))
				.always(() => $button.prop('disabled', false));
		}
	}
	
	/**
	 * Sends an ajax request to update a new quantity unit entity.
	 *
	 * @private
	 */
	const _editQuantityUnit = () => {
		if (_validateInputFields('edit')) {
			jse.libs.xhr.post({
				url: './admin.php?do=QuantityUnitAjax/edit',
				data: _inputData('edit')
			}).done(() => _renderTable(jse.core.lang.translate('TEXT_INFO_EDIT_SUCCESS', 'quantity_units')));
		}
	};
	
	/**
	 * Sends an ajax request to remove a new quantity unit entity.
	 *
	 * @private
	 */
	const _removeQuantityUnit = () => {
		jse.libs.xhr.post({
			url: './admin.php?do=QuantityUnitAjax/remove',
			data: {
				id: $('.quantity-units-remove-id').val()
			}
		}).done(() => _renderTable(jse.core.lang.translate('TEXT_INFO_DELETE_SUCCESS', 'quantity_units')));
	};
	
	/**
	 * Renders the quantity unit table again.
	 *
	 * @private
	 */
	const _renderTable = (msg) => {
		$creationModal.modal('hide');
		$editModal.modal('hide');
		$removeConfirmationModal.modal('hide');
		
		if (undefined !== msg) {
			jse.libs.info_box.addSuccessMessage(msg);
		}
		
		jse.libs.xhr.get({
			url: './admin.php?do=QuantityUnitAjax/getData'
		}).done(r => {
			let i = 0;
			const $body = $('.quantity-units-table tbody');
			$body.empty();
			
			for (; i < r.data.length; i++) {
				let $row = $('<tr/>');
				let $dataCol = $('<td/>');
				let $actionsCol = $('<td/>').addClass('actions');
				let $actionsContainer = $('<div/>').addClass('pull-right action-list visible-on-hover');
				let $edit = $('<i/>').addClass('fa fa-pencil edit').attr('data-id', r.data[i].id);
				let $delete = $('<i/>').addClass('fa fa-trash-o delete').attr('data-id', r.data[i].id);
				
				$actionsContainer.append($edit).append($delete);
				$actionsCol.append($actionsContainer);
				$dataCol.text(r.data[i].names[r.languageCode]);
				
				$row.append($dataCol).append($actionsCol);
				$body.append($row);
			}
			$this.find('.edit').on('click', _initEditModal);
			$this.find('.delete').on('click', _initRemoveConfirmationModal);
		});
	};
	
	/**
	 * Validates the input fields, returns true if they are valid and false otherwise.
	 *
	 * @param {string} modal Modal instance, whether 'create' or 'edit'.
	 * @returns {boolean}
	 * @private
	 */
	const _validateInputFields = (modal) => {
		const $modal = modal === 'edit' ? $editModal : $creationModal;
		const $inputs = $modal.find('input[type="text"]');
		let valid = false;
		
		
		for (let i = 0; i < $inputs.length; i++) {
			if ($inputs[i].value !== '') {
				valid = true;
			}
		}
		
		if (!valid) {
			for (let i = 0; i < $inputs.length; i++) {
				$($inputs[i]).parent().addClass('has-error');
			}
			$modal.find('p.quantity-units-modal-info')
				.first()
				.text(jse.core.lang.translate('ERROR_INVALID_INPUT_FIELDS', 'quantity_units'))
				.removeClass('text-info')
				.addClass('text-danger');
			
			return false;
		} else {
			for (let i = 0; i < $inputs.length; i++) {
				$($inputs[i]).parent().removeClass('has-error');
			}
			let textConstant = modal === 'edit' ? 'TEXT_QUANTITYUNIT' : 'TEXT_NEW_QUANTITYUNIT';
			$modal.find('p.quantity-units-modal-info')
				.first()
				.text(jse.core.lang.translate(textConstant, 'quantity_units'))
				.removeClass('text-danger');
			
			return true;
		}
	};
	
	/**
	 * Fetches the data from the input fields.
	 *
	 * @param {string} modal Modal instance, whether 'create' or 'edit'.
	 * @returns {{id: int, name: object}}
	 * @private
	 */
	const _inputData = (modal) => {
		const data = {};
		const $modal = modal === 'edit' ? $editModal : $creationModal;
		const $inputs = $modal.find('input[type="text"]');
		const $id = $modal.find('input[name="id"]').val();
		
		for (let i = 0; i < $inputs.length; i++) {
			data[$inputs[i].getAttribute('name')] = $inputs[i].value;
		}
		
		if (undefined !== $id) {
			data['id'] = $id;
		}
		
		return data;
	}
	
	
	// ------------------------------------------------------------------------
	// INITIALIZE
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$('#add-quantity-units').on('click', _initCreationModal);
		$this.find('.edit').on('click', _initEditModal);
		$this.find('.delete').on('click', _initRemoveConfirmationModal);
		
		// actions
		$('#create-quantity-units').on('click', _storeQuantityUnit);
		$('#edit-quantity-units').on('click', _editQuantityUnit);
		$('#remove-quantity-units').on('click', _removeQuantityUnit);
		
		done();
	};
	
	return module;
});