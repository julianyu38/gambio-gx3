/* --------------------------------------------------------------
 offline_layer_editor.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Site Online/Offline Layer Editor Controller
 *
 * @module Controllers/offline_layer_editor
 */
gx.controllers.module(
	'offline_layer_editor',
	
	['form', 'fallback', gx.source + '/libs/editor_values', gx.source + '/libs/editor_instances'],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		var $this = $(this),
			defaults = {},
			options = $.extend(true, {}, defaults, data),
			lightboxParameters = $this.data('lightboxParams'),
			module = {},
			$fields = null,
			temporaryNamePostfix = '',
			$layer = $('#lightbox_package_' + lightboxParameters.identifier), 
			$form = $this.find('.lightbox_content_container form'),
			$parentForm = $(lightboxParameters.element).closest('tr');
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		var _modifyFormInputNames = function(revertOriginalNames) {
			$fields.each(function() {
				var $field = $(this),
					name = $field.attr('name');
				name = revertOriginalNames ? name.replace(temporaryNamePostfix, '') : (name + temporaryNamePostfix);
				$field.attr('name', name);
			});
		};
		
		var _onOkButtonClick = function() {
			$form
				.find('.wysiwyg')
				.each(function(index, textarea) {
					var $textarea = $(textarea),
						value = jse.libs.editor_values.getValue($textarea);
					
					$textarea.val(value);
				});
			
			$layer
				.find('form')
				.trigger('layerClose');
			
			_modifyFormInputNames(true);
			jse.libs.form.prefillForm($parentForm, jse.libs.fallback.getData($form), false);
			$.lightbox_plugin('close', lightboxParameters.identifier);
			_destroyEditorInstance();
			
			// Set editor-type hidden fields in the timer table (they'll be saved on save/insert from PHP).
			var $textarea = $this.find('.wysiwyg'),
				editorIdentifier = $textarea.data('editorIdentifier'),
				editorType = $textarea.data('editorType'),
				$inputHidden = $form.find('[name="editor_identifiers[' + editorIdentifier + ']"]'),
				$editIcon = $('.timer-table [name="editor_identifiers[' + editorIdentifier + ']"]:first').parent().find('i.open_lightbox'); 
			
			$inputHidden.val(editorType);
			
			if ($editIcon.length) {
				// $editIcon.attr('data-lightbox-editor-type', editorType); 
				$editIcon.data('lightboxEditorType', editorType); 
			}
		};
		
		var _onCloseButtonClick = function() {
			_modifyFormInputNames(true);	
			_destroyEditorInstance(); 
		};
		
		var _destroyEditorInstance = function() {
			var $textarea = $this.find('.wysiwyg');
			
			if ($textarea.length) {
				jse.libs.editor_instances.destroy($textarea);	
			}
			
			$(window).off('editor:initialize editor:ready');
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// -----------------------------------------------------------------------
		
		module.init = function(done) {
			var dataset = jse.libs.fallback.getData($parentForm);
			$fields = $parentForm.find('[name]');
			temporaryNamePostfix = '_tmp_' + parseInt(Math.random() * new Date().getTime());
			
			_modifyFormInputNames();
			jse.libs.form.prefillForm($form, dataset, false);
			jse.libs.fallback.setupWidgetAttr($this);
			
			$.when(
				gx.extensions.init($this), 
				gx.controllers.init($this),  
				gx.widgets.init($this),  
				gx.compatibility.init($this)
			).then(function() {
				// Delay the editor initialization until the lightbox is ready.
				$(window).trigger('editor:initialize');
			});
			
			$layer
				.on('click', '.ok', _onOkButtonClick)
				.on('click', '.close', _onCloseButtonClick);
				
			$this.find('form').trigger('language_switcher.updateField');
			
			done();
		};
		
		return module;
	});
