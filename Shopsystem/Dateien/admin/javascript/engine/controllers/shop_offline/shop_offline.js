/* --------------------------------------------------------------
 shop_offline.js 2016-09-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * General Controller of Shop Online/Offline
 */
gx.controllers.module('shop_offline', [], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 * 
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance 
	 * 
	 * @type {Object}
	 */
	const module = {}; 
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	function _toggleLanguageSelection() {
		const $languagesButtonBar = $('.languages.buttonbar'); 
		
		if ($(this).attr('href') === '#status') {
			$languagesButtonBar.css('visibility', 'hidden'); 
		} else {
			$languagesButtonBar.css('visibility', 'visible'); 
		}
	}
	
	function _correctCodeMirrorDisplay() {
		setTimeout(() => {
			$('.CodeMirror:visible').each((index, codeMirror) => {
				codeMirror.CodeMirror.refresh();
			})
		}, 100); 
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this.find('.tab-headline-wrapper > a')
			.on('click', _toggleLanguageSelection) 
			.on('click', _correctCodeMirrorDisplay); 
		
		_toggleLanguageSelection.call($('.tab-headline-wrapper a')[0]);
		
		done(); 
	}; 
	
	return module;
	
}); 