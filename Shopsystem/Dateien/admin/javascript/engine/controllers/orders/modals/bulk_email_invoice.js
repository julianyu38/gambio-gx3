/* --------------------------------------------------------------
 bulk_email_invoice.js 2017-10-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Bulk Email Invoice Modal Controller
 *
 * This controller handles the orders bulk invoice emails modal where the user can send emails for the
 * selected orders. The controller is able to create missing invoices if needed.
 */
gx.controllers.module('bulk_email_invoice', ['modal', 'loading_spinner'], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {
		bindings: {
			subject: $this.find('.subject')
		}
	};
	
	/**
	 * Selector for the email list item.
	 *
	 * @type {String}
	 */
	const emailListItemSelector = '.email-list-item';
	
	/**
	 * Selector for the email list item ID.
	 *
	 * @type {String}
	 */
	const emailListItemEmailSelector = '.email-input';
	
	/**
	 * Selector for the latest invoice number of the list item
	 *
	 * @type {string}
	 */
	const latestInvoiceIdSelector = '.latest-invoice-id';
	
	/**
	 * Selector for the option that indicates if missing invoices should be created
	 *
	 * @type {String}
	 */
	const createMissingInvoicesSelector = '.create-missing-invoices';
	
	/**
	 * Selector for the modal content body layer.
	 *
	 * @type {String}
	 */
	const modalContentSelector = '.modal-content';
	
	/**
	 * GM PDF Order URL
	 *
	 * @type {String}
	 */
	const gmPdfOrderUrl = jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php';
	
	/**
	 * Admin Request URL
	 *
	 * @type {String}
	 */
	const adminRequestUrl = jse.core.config.get('appUrl') + '/admin/admin.php';
	
	/**
	 * Loading Spinner Selector
	 *
	 * @type {jQuery|null}
	 */
	let $spinner = null;
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Show/hide loading spinner.
	 *
	 * @param {Boolean} spinnerVisibility Whether to show or hide the spinner.
	 */
	function _toggleSpinner(spinnerVisibility) {
		if (spinnerVisibility) {
			$spinner = jse.libs.loading_spinner.show($this.find(modalContentSelector), $this.css('z-index'));
		} else {
			jse.libs.loading_spinner.hide($spinner);
		}
	}
	
	/**
	 * Parse subject and replace the placeholders with the variables.
	 *
	 * @param {Object} orderData Contains the order's data.
	 * @param {Object} subjectData Contains the required subject's data.
	 *
	 * @return {String} Returns the final email subject string.
	 */
	function _getParsedSubject(orderData, subjectData) {
		return module.bindings.subject.get()
			.replace('{INVOICE_NUM}', subjectData.invoiceNumber)
			.replace('{INVOICE_ID}', subjectData.invoiceNumber)
			.replace('{INVOICE_DATE}', subjectData.invoiceDate)
			.replace('{DATE}', subjectData.invoiceDate)
			.replace('{ORDER_ID}', orderData.id);
	}
	
	/**
	 * Handles the successful delivery of all messages.
	 */
	function _handleDeliverySuccess() {
		const message = jse.core.lang.translate('BULK_MAIL_SUCCESS', 'gm_send_order');
		
		// Show success message in the admin info box.
		jse.libs.info_box.addSuccessMessage(message);
		
		$('.orders .table-main').DataTable().ajax.reload(null, false);
		$('.orders .table-main').orders_overview_filter('reload');
		
		// Hide modal and loading spinner.
		_toggleSpinner(false);
		$this.modal('hide');
	}
	
	/**
	 * Handles the failure of the message delivery.
	 */
	function _handleDeliveryFailure() {
		const title = jse.core.lang.translate('error', 'messages');
		const content = jse.core.lang.translate('BULK_MAIL_UNSUCCESS', 'gm_send_order');
		
		// Show error message in a modal.
		jse.libs.modal.message({title, content});
		
		// Hide modal and the loading spinner and re-enable the send button.
		_toggleSpinner(false);
		$this.modal('hide');
	}
	
	/**
	 * Get the IDs of the orders that do not have an invoice.
	 *
	 * @param {Number[]} orderIds The orders to be validated.
	 *
	 * @return {Promise}
	 */
	function _getOrdersWithoutDocuments(orderIds) {
		return new Promise((resolve, reject) => {
			const data = {
				do: 'OrdersOverviewAjax/GetOrdersWithoutDocuments',
				pageToken: jse.core.config.get('pageToken'),
				type: 'invoice',
				orderIds
			};
			
			$.getJSON(adminRequestUrl, data)
				.done(orderIdsWithoutDocument => resolve({orderIds, orderIdsWithoutDocument}))
				.fail(reject);
		});
	}
	
	/**
	 * Validate selected orders and generate/remove the orders without documents.
	 *
	 * @param {Object} selection Contains the "orderIds" and "orderIdsWithoutDocument" properties.
	 *
	 * @return {Promise} Returns the promises of the delegated methods.
	 */
	function _handleMissingDocuments(selection) {
		// Indicates if missing invoices should be created
		const createMissingInvoices = $this.find(createMissingInvoicesSelector).prop('checked');
		
		if (createMissingInvoices) {
			return _createMissingDocuments(selection.orderIds, selection.orderIdsWithoutDocument)
		} else {
			return _removeOrderIdsWithoutDocument(selection.orderIds, selection.orderIdsWithoutDocument)
		}
	}
	
	/**
	 * Create Missing Order Documents and set the new latest invoice id selector for which no invoice has yet existed.
	 *
	 * @param {Number[]} orderIds Selected order IDs.
	 * @param {Number[]} orderIdsWithoutDocument Order IDs that do not have a document.
	 *
	 * @return {Promise} Returns a promise that will be resolved with all the order IDs.
	 */
	function _createMissingDocuments(orderIds, orderIdsWithoutDocument) {
		return new Promise(resolve => {
			const requests = [];
			
			orderIdsWithoutDocument.forEach(id => {
				const url = gmPdfOrderUrl + `?oID=${id}&type=invoice&ajax=1`;
				const request = $.getJSON(url);
				
				request.done(response => {
					$(emailListItemSelector).each((index, emailListItem) => {
						const $emailListItem = $(emailListItem);
						
						if ($emailListItem.data('order').id === parseInt(id)) {
							$emailListItem.find(latestInvoiceIdSelector).val(response.invoiceId);
							return false;
						}
					});
				});
				
				requests.push(request);
			});
			
			return $.when(...requests).done(() => resolve(orderIds));
		});
	}
	
	/**
	 * Remove order IDs that do not have a document.
	 *
	 * @param {Number[]} orderIds Selected order IDs.
	 * @param {Number[]} orderIdsWithoutDocument Order IDs that do not have a document.
	 *
	 * @return {Promise} Returns a promise that will be resolved with the orders that do have a document.
	 */
	function _removeOrderIdsWithoutDocument(orderIds, orderIdsWithoutDocument) {
		return new Promise(resolve => {
			const orderIdsWithDocument = orderIds.filter(orderId => !orderIdsWithoutDocument.includes(String(orderId)));
			resolve(orderIdsWithDocument);
		});
	}
	
	/**
	 * Get the required data for the email subject of the provided invoice.
	 *
	 * @param {Number} invoiceId Get the subject information for the selected invoice.
	 *
	 * @return {jQuery.jqXHR} The request will be resolved with the subject data.
	 */
	function _getSubjectData(invoiceId) {
		return $.ajax({
			url: `${adminRequestUrl}?do=OrdersModalsAjax/GetEmailInvoiceSubjectData`,
			method: 'POST',
			dataType: 'json',
			data: {
				invoiceId,
				pageToken: jse.core.config.get('pageToken')
			}
		});
	}
	
	/**
	 * Send Invoice Emails
	 *
	 * @param {Number[]} orderIds Contains the IDs of the orders to be finally sent with an email.
	 */
	function _sendInvoiceEmails(orderIds) {
		return new Promise((resolve, reject) => {
			const createMissingInvoices = $this.find(createMissingInvoicesSelector).prop('checked');
			const $emailListItems = $this.find(emailListItemSelector);
			
			// Abort and hide modal on empty email list entries.
			if (!$emailListItems.length || !orderIds.length) {
				const title = jse.core.lang.translate('TITLE_INVOICE', 'gm_order_menu');
				const message = jse.core.lang.translate('NO_RECORDS_ERROR', 'orders');
				jse.libs.modal.showMessage(title, message);
				$this.modal('hide');
				return;
			}
			
			// Show loading spinner.
			_toggleSpinner(true);
			
			// Collection of requests in promise format.
			const requests = [];
			
			// Fill orders array with data.
			$emailListItems.each((index, emailListItem) => {
				const orderData = $(emailListItem).data('order');
				
				if (!orderIds.includes(orderData.id)) {
					return true; // Current order does not have an invoice document.
				}
				
				// Email address entered in input field.
				const email = $(emailListItem).find(emailListItemEmailSelector).val();
				
				// The latest invoice id of the order
				const invoiceId = $(emailListItem).find(latestInvoiceIdSelector).val();
				
				_getSubjectData(invoiceId)
					.then(subjectData => {
						// Request GET parameters to send.
						const parameters = {
							oID: orderData.id,
							type: 'invoice',
							mail: '1',
							gm_quick_mail: '1'
						};
						
						const url = gmPdfOrderUrl + '?' + $.param(parameters);
						const data = {
							gm_mail: email,
							gm_subject: _getParsedSubject(orderData, subjectData),
							create_missing_invoices: Number(createMissingInvoices) // 1 or 0
						};
						
						if (invoiceId !== '0') {
							data.invoice_ids = [invoiceId];
						}
						
						// Create AJAX request.
						requests.push($.ajax({method: 'POST', url, data}));
					});
			});
			
			$.when(...requests)
				.done(resolve)
				.fail(reject);
		});
	}
	
	/**
	 * Send the invoice emails.
	 *
	 * This method will only send emails for the orders that do have an invoice document. If the
	 * "create-missing-invoices" checkbox is active, new invoices will be generated for the orders that
	 * are do not have one.
	 */
	function _onSendClick() {
		const orderIds = [];
		
		$this.find(emailListItemSelector).each((index, emailListItem) => {
			orderIds.push($(emailListItem).data('order').id);
		});
		
		_getOrdersWithoutDocuments(orderIds)
			.then(_handleMissingDocuments)
			.then(_sendInvoiceEmails)
			.then(_handleDeliverySuccess)
			.catch(_handleDeliveryFailure);
	}
	
	
	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this.on('click', '.btn.send', _onSendClick);
		done();
	};
	
	return module;
});