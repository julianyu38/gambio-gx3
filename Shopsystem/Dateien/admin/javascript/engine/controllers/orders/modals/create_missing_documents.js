/* --------------------------------------------------------------
 create_missing_documents.js 2016-10-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Create Missing Documents Modal
 *
 * This modal prompts the user for the creation of missing order documents (invoices or packing slips). Make sure
 * that you set the "orderIds", "orderIdsWithoutDocument", "type" data values to the modal before showing it.
 *
 * Example:
 *
 * ```
 * $modal
 *   .data({
 *     orderIds: [...], 
 *     orderIdsWithoutDocument: [...], 
 *     type: 'invoice'
 *   })
 *   .modal('show');
 * ```
 */
gx.controllers.module('create_missing_documents', ['modal'], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Module Instance
	 *
	 * @type {Object}
	 */
	const module = {
		bindings: {
			createMissingDocumentsCheckbox: $this.find('.create-missing-documents-checkbox')
		}
	};
	
	/**
	 * Proceed Event Constant
	 *
	 * @type {String}
	 */
	const PROCEED_EVENT = 'create_missing_documents:proceed';
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Create missing documents.
	 *
	 * @param {Number[]} orderIds
	 * @param {Number[]} orderIdsWithoutDocument
	 * @param {String} type
	 */
	function _createMissingDocuments(orderIds, orderIdsWithoutDocument, type) {
		const requests = [];
		const downloadPdfWindow = window.open('about:blank');
		
		orderIdsWithoutDocument.forEach(oID => {
			const data = {
				type,
				oID,
				ajax: '1'
			};
			
			requests.push($.getJSON(jse.core.config.get('appUrl') + '/admin/gm_pdf_order.php', data));
		});
		
		$.when(...requests).done(() => $this.trigger(PROCEED_EVENT, [orderIds, type, downloadPdfWindow]));
	}
	
	/**
	 * Remove order IDs that do not have a document.
	 *
	 * @param {Number[]} orderIds
	 * @param {Number[]} orderIdsWithoutDocument
	 * @param {String} type
	 */
	function _removeOrderIdsWithoutDocument(orderIds, orderIdsWithoutDocument, type) {
		orderIds.forEach((id, index) => {
			if (orderIdsWithoutDocument.includes(String(id))) {
				orderIds.splice(index);
			}
		});
		
		if (!orderIds.length) {
			const title = jse.core.lang.translate('DOWNLOAD_DOCUMENTS', 'admin_orders');
			const message = jse.core.lang.translate('NO_DOCUMENTS_ERROR', 'orders');
			jse.libs.modal.showMessage(title, message);
		} else {
			$this.trigger(PROCEED_EVENT, [orderIds, type]);
		}
	}
	
	/**
	 * On Bulk PDF Modal Confirmation Click
	 *
	 * Proceed with the generation of the concatenated PDF. This handler will trigger the "create_missing_document"
	 */
	function _onOkButtonClick() {
		const createMissingDocuments = module.bindings.createMissingDocumentsCheckbox.get();
		const {orderIds, orderIdsWithoutDocument, type} = $this.data();
		
		if (createMissingDocuments) {
			_createMissingDocuments(orderIds, orderIdsWithoutDocument, type);
		} else {
			_removeOrderIdsWithoutDocument(orderIds, orderIdsWithoutDocument, type);
		}
		
		$this.modal('hide');
	}
	
	// ------------------------------------------------------------------------
	// INITIALIZE
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$this.on('click', '.btn.ok', _onOkButtonClick);
		done();
	};
	
	return module;
}); 