/* --------------------------------------------------------------
 sliders_delete.js 2016-09-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Sliders Overview Delete
 * 
 * Controller Module To Delete Sliders.
 *
 * Handles the delete operation of the sliders overview page.
 */
gx.controllers.module(
	'sliders_delete',
	
	[
		'modal',
		`${gx.source}/libs/info_box`
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector 
		 * 
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Slider delete confirmation modal.
		 * 
		 * @type {jQuery}
		 */
		const $modal = $('.delete-slider.modal');
		
		/**
		 * Delete button selector string.
		 * 
		 * @type {String}
		 */
		const deleteButtonSelector = '.btn-delete';
		
		/**
		 * Delete slider action URL.
		 * 
		 * @type {String}
		 */
		const url = `${jse.core.config.get('appUrl')}/admin/admin.php?do=SlidersOverviewAjax/DeleteSlider`;
		
		/**
		 * Module Instance 
		 * 
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Handles the delete click event by opening the delete confirmation modal.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onDeleteClick(event) {
			// Prevent default action.
			event.preventDefault();
			
			// Table row.
			const $parentTableRow = $(event.target).parents('tr');
			
			// Delete confirmation modal button.
			const $confirmButton = $modal.find('button.confirm');
			
			// Empty modal body.
			$modal.find('.modal-body .form-group').remove();
			
			// Slider data.
			const sliderData = {
				id: $parentTableRow.data('sliderId'),
				name: $parentTableRow.data('sliderName'),
				slidesQuantity: $parentTableRow.data('slidesQuantity')
			};
			
			// Put new slider information into modal body.
			$modal
				.find('.modal-body fieldset')
				.html(_generateSliderInfoMarkup(sliderData));
			
			// Show modal.
			$modal.modal('show');
			
			// Handle delete confirmation modal button click event.
			$confirmButton
				.off('click')
				.on('click', () => _onConfirmButtonClick(sliderData.id));
		}
		
		/**
		 * Handles the delete confirmation button click event by removing the slider through an AJAX request.
		 *
		 * @param {Number} sliderId Slider ID.
		 */
		function _onConfirmButtonClick(sliderId) {
			// AJAX request options.
			const requestOptions = {
				type: 'POST',
				data: {sliderId},
				url
			};
			
			// Perform request.
			$.ajax(requestOptions)
				.done(response => _handleDeleteRequestResponse(response, sliderId))
				.always(() => $modal.modal('hide'));
		}
		
		/**
		 * Handles slider deletion AJAX action server response.
		 *
		 * @param {Object} response Server response.
		 * @param {Number} sliderId ID of deleted slider.
		 */
		function _handleDeleteRequestResponse(response, sliderId) {
			// Error message phrases.
			const errorTitle = jse.core.lang.translate('DELETE_SLIDER_ERROR_TITLE', 'sliders');
			const errorMessage = jse.core.lang.translate('DELETE_SLIDER_ERROR_TEXT', 'sliders');
			
			// Table body.
			const $tableBody = $this.find('tbody');
			
			// Table rows.
			const $rows = $tableBody.find(`[data-slider-id]`);
			
			// Table rows that will be deleted.
			const $rowToDelete = $rows.filter(`[data-slider-id="${sliderId}"]`);
			
			// 'No results' message table row template.
			const $emptyRowTemplate = $('#template-table-row-empty');
			
			// Check for action success.
			if (response.includes('success')) {
				// Delete respective table rows.
				$rowToDelete.remove();
				
				// Add success message to admin info box.
				jse.libs.info_box.addSuccessMessage();
				
				// If there are no rows, show 'No results' message row.
				if (($rows.length - 1) < 1) {
					$tableBody
						.empty()
						.append($emptyRowTemplate.clone().html());
				}
			} else {
				// Show error message modal.
				jse.libs.modal.showMessage(errorTitle, errorMessage);
			}
		}
		
		/**
		 * Generates HTML containing the slider information for the delete confirmation modal.
		 *
		 * @param {Object} data Slider data.
		 * @param {String} data.name Name of the slider.
		 * @param {String} data.slidesQuantity Quantity of slides from slider.
		 * 
		 * @return {String} Created HTML string.
		 */
		function _generateSliderInfoMarkup(data) {
			// Label phrases.
			const sliderNameLabel = jse.core.lang.translate('NAME', 'sliders');
			const slidesQuantityLabel = jse.core.lang.translate('AMOUNT_OF_SLIDES', 'sliders');
			
			// Return markup.
			return `
					<div class="form-group">
						<label class="col-md-5">${sliderNameLabel}</label>
						<div class="col-md-7">${data.name}</div>
					</div>

					<div class="form-group">
						<label class="col-md-5">${slidesQuantityLabel}</label>
						<div class="col-md-7">${data.slidesQuantity}</div>
					</div>
			`;
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Listen to form submit event.
			$this.on('click', deleteButtonSelector, _onDeleteClick);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
);
