/* --------------------------------------------------------------
 sliders_start_page.js 2016-12-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Sliders Overview Start Page Option
 *
 * Handles the start page switcher toggling.
 */
gx.controllers.module(
	'sliders_start_page',
	
	[
		'modal',
		`${gx.source}/libs/info_box`
	],
	
	function () {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		
		/**
		 * CSS class names.
		 *
		 * @type {Object}
		 */
		const classes = {
			switcher: 'switcher',
		};
		
		/**
		 * Selector Strings
		 *
		 * @type {Object}
		 */
		const selectors = {
			switcher: `.${classes.switcher}`,
			switcherCheckbox: `.${classes.switcher} :checkbox`,
		};
		
		
		/**
		 * URI map.
		 *
		 * @type {Object}
		 */
		const uris = {
			activate: `${jse.core.config.get('appUrl')}/admin/admin.php?do=SlidersOverviewAjax/SetStartPageSlider`,
			deactivate: `${jse.core.config.get('appUrl')}/admin/admin.php?do=SlidersOverviewAjax/DeactivateStartPageSlider`,
		};
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Handles the slider start page switcher change event.
		 *
		 * @param {jQuery.Event} event Trigger event.
		 */
		function _onSwitcherChange(event) {
			// Clicked element.
			const $target = $(event.target);
			
			// Clicked switcher element.
			const $clickedSwitcher = $target.hasClass(classes.switcher) ? $target : $target.parents(selectors.switcher);
			
			// Clicked slider ID.
			const sliderId = $clickedSwitcher.parents('tr').data('sliderId');
			
			// Is slider set as start page slider?
			const isActive = !$clickedSwitcher.hasClass('checked');
			
			// Disable all switchers.
			_toggleSwitchers(false);
			
			// Activate or deactivate slider depending on the state.
			isActive ? _deactivate(sliderId, $clickedSwitcher) : _activate(sliderId, $clickedSwitcher);
		}
		
		/**
		 * Deactivates the slider.
		 *
		 * @param {Number} sliderId Slider ID.
		 * @param {jQuery} $clickedSwitcher Clicked slider element.
		 */
		function _deactivate(sliderId, $clickedSwitcher) {
			// Request options.
			const requestOptions = {
				type: 'POST',
				data: {sliderId},
				url: uris.deactivate,
			};
			
			// Handles the 'always' case by enabling the clicked slider.
			const handleAlways = () => $clickedSwitcher.removeClass('disabled');
			
			// Handles the 'done' case with the server response.
			const handleDone = response => {
				// Enable all switchers.
				_toggleSwitchers(true);
				
				// Deactivate each slider.
				$this
					.find(selectors.switcherCheckbox)
					.each((index, checkbox) => $(checkbox).switcher('checked', false));
				
				// Notify user.
				_notify(response.includes('success'));
			};
			
			// Perform request.
			$.ajax(requestOptions)
				.done(handleDone)
				.always(handleAlways);
		}
		
		/**
		 * Activates the slider.
		 *
		 * @param {Number} sliderId Slider ID.
		 * @param {jQuery} $clickedSwitcher Clicked slider element.
		 */
		function _activate(sliderId, $clickedSwitcher) {
			// Request options.
			const requestOptions = {
				type: 'POST',
				data: {sliderId},
				url: uris.activate,
			};
			
			// Handles the 'always' case by enabling the clicked slider.
			const handleAlways = () => _toggleSwitchers(true);
			
			// Handles the 'done' case with the server response.
			const handleDone = response => {
				// Clicked switcher's checkbox.
				const $checkbox = $clickedSwitcher.find(':checkbox');
				
				// Enable all switchers.
				_toggleSwitchers(true);
				
				// Check switcher.
				$checkbox.switcher('checked', true);
				
				// Deactivate each slider, except the clicked one.
				$this
					.find(selectors.switcherCheckbox)
					.not($checkbox)
					.each((index, checkbox) => $(checkbox).switcher('checked', false));
				
				// Notify user.
				_notify(response.includes('success'));
			};
			
			// Perform request.
			$.ajax(requestOptions)
				.done(handleDone)
				.always(handleAlways);
		}
		
		/**
		 * If the server response is successful, it removes any previous messages and
		 * adds new success message to admin info box.
		 *
		 * Otherwise its shows an error message modal.
		 *
		 * @param {Boolean} isSuccessful Is the server response successful?
		 */
		function _notify(isSuccessful) {
			if (isSuccessful) {
				jse.libs.info_box.deleteBySource('adminAction').then(() => jse.libs.info_box.addSuccessMessage())
			} else {
				jse.libs.modal.showMessage(
					jse.core.lang.translate('SLIDER_START_PAGE_ERROR_TITLE', 'sliders'),
					jse.core.lang.translate('SLIDER_START_PAGE_ERROR_TEXT', 'sliders')
				);
			}
		}
		
		/**
		 * Enables or disables the switchers.
		 *
		 * @param {Boolean} doEnable Enable the switchers?
		 */
		function _toggleSwitchers(doEnable) {
			$this.find(selectors.switcher)[`${doEnable ? 'remove' : 'add'}Class`]('disabled');
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Listen to set start page event.
			$this.on('change', selectors.switcher, _onSwitcherChange);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
);
