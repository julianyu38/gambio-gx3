/* --------------------------------------------------------------
 slides_container.js 2016-12-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Slides Container (Tabs)
 *
 * Handles the sliders container functionality in the sliders details page.
 */
gx.controllers.module(
	'slides_container',
	
	[
		`${jse.source}/vendor/jquery-deparam/jquery-deparam.min.js`,
		`${jse.source}/vendor/jquery-ui/jquery-ui.min.css`,
		`${jse.source}/vendor/jquery-ui/jquery-ui.min.js`,
		'xhr',
		'modal'
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		const $footer = $('#main-footer');
		
		/**
		 * Elements
		 *
		 * @type {Object}
		 */
		const elements = {
			// Buttons.
			buttons: {
				// Sort mode button.
				sort: $this.find('.sort-button'),
				
				// Create button.
				create: $this.find('.btn-create'),
				
				// Submit button group.
				submit: $footer.find('.submit-button-group'),
				
				// Submit button for save slider
				submitSave: $footer.find('.save'),
				
				// Submit button for save and refresh slider
				submitRefresh: $footer.find('.refresh'),
			},
			
			// Template.
			templates: {
				// Slide panel set template.
				slidePanel: $this.find('#slide-panel-template')
			},
			
			// Modals.
			modals: {
				// Delete image modal.
				deleteImage: $('.delete-image.modal'),
				
				// Delete slide modal.
				deleteSlide: $('.delete-slide.modal'),
				
				// Edit image map modal.
				imageMap: $('.image-map.modal'),
			},
			
			// Tabs.
			tabHeader: $this.find('.nav-tabs'),
			
			// Select box which holds all images that will be deleted.
			deleteImageSelect: $('#delete_images')
		};
		
		/**
		 * CSS class names.
		 *
		 * @type {Object}
		 */
		const classes = {
			// New image.
			newImage: 'new'
		};
		
		/**
		 * Selector Strings
		 *
		 * @type {Object}
		 */
		const selectors = {
			// Icon selector strings.
			icons: {
				// Delete button on the panel header.
				delete: '.icon.delete',
				
				// Drag button on the panel header.
				drag: '.drag-handle',
				
				// Collapser button on the panel header.
				collapser: '.collapser',
				
				// Image delete button.
				imageDelete: '.action-icon.delete',
				
				// Image map edit button.
				imageMap: '.action-icon.image-map',
				
				// Upload image button.
				upload: '.action-icon.upload'
			},
			
			// Inputs selector strings.
			inputs: {
				// General image select dropdowns.
				dropdown: '.dropdown-input',
				
				// Thumbnail dropdown.
				thumbnailImageDropdown: '[name="thumbnail"]',
				
				// Title.
				title: 'input[name="title"]',
				
				// File.
				file: '.file-input'
			},
			
			// Slide panel.
			slidePanel: '.panel',
			
			// Tab body.
			tabBody: '.tab-pane',
			
			// Slide panel title.
			slidePanelTitle: '.slide-title',
			
			// Setting row (form group).
			configurationRow: '.row.form-group',
			
			// Data list container for image map.
			imageMapDataList: '.image-map-data-list',
		};
		
		/**
		 * Cache list of open slide panels.
		 *
		 * @type {jQuery[]}
		 */
		let openSlidePanels = [];
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Registers a change, so that the user gets a confirmation dialog while leaving the page.
		 */
		function _registerChange() {
			// Object of GET parameters.
			const getParameters = $.deparam(window.location.search.slice(1));
			
			// Only register in slider edit mode.
			if ('id' in getParameters) {
				window.onbeforeunload = () => jse.core.lang.translate('EXIT_CONFIRMATION_TEXT', 'sliders');
			}
		}
		
		/**
		 * Handles the image dropdown change event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 * @param {Boolean} [removeAllDataListItems = false] Remove all data list container list items?
		 */
		function _onImageChange(event, removeAllDataListItems = true) {
			// Image dropdown element.
			const $dropdown = $(event.target);
			
			// Remove icon element.
			const $removeIcon = $dropdown
				.parents(selectors.configurationRow)
				.find(selectors.icons.imageDelete);
			
			// Image map icon element.
			const $imageMapIcon = $dropdown
				.parents(selectors.configurationRow)
				.find(selectors.icons.imageMap);
			
			// Image map data container list element.
			const $list = $dropdown
				.parents(selectors.configurationRow)
				.find(selectors.imageMapDataList);
			
			// Remove the remove icon if 'do not use' is selected.
			$removeIcon[$dropdown.val() ? 'show' : 'hide']();
			
			// Remove the image map icon if 'do not use' is selected.
			$imageMapIcon[$dropdown.val() ? 'show' : 'hide']();
			
			// Empty image map data container list.
			$list
				.children()
				.filter(removeAllDataListItems ? '*' : '.new')
				.remove();
		}
		
		/**
		 * Triggers the file select (click) event of the invisible file input field.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onUploadIconClick(event) {
			$(event.target)
				.parents(selectors.configurationRow)
				.find(selectors.inputs.file)
				.trigger('click');
		}
		
		/**
		 * Handles the file select (change) event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onImageAdd(event) {
			// Exit method, if selection has been aborted.
			if (!event.target.files.length) {
				return;
			}
			
			// File input element.
			const $fileInput = $(event.target);
			
			// Image dropdown.
			const $dropdown = $fileInput
				.parents(selectors.configurationRow)
				.find(selectors.inputs.dropdown);
			
			// Regular expression to validate the file name.
			const regex = /(.)(jpg|jpeg|png|gif|bmp)$/i;
			
			// File name.
			const fileName = event.target.files[0].name;
			
			// Is the dropdown for thumbnail images?
			const isThumbnailImage = !!$fileInput
				.parents(selectors.configurationRow)
				.find(selectors.inputs.thumbnailImageDropdown)
				.length;
			
			// Exit method and show modal, if file type does not match.
			if (!regex.test(fileName)) {
				// Show modal.
				jse.libs.modal.showMessage(
					jse.core.lang.translate('INVALID_FILE_MODAL_TITLE', 'sliders'),
					jse.core.lang.translate('INVALID_FILE_MODAL_TEXT', 'sliders')
				);
				
				// Reset value.
				$fileInput.val('');
				
				return;
			}
			
			// Exit method and show modal, if filename is already present in dropdown.
			for (const $option of $dropdown[0].children) {
				// Check if option's text content matches with the name of the selected file.
				if ($option.textContent === fileName) {
					// Show modal.
					jse.libs.modal.showMessage(
						jse.core.lang.translate('FILENAME_ALREADY_USED_MODAL_TITLE', 'sliders'),
						jse.core.lang.translate('FILENAME_ALREADY_USED_MODAL_TEXT', 'sliders')
					);
					
					// Reset value.
					$fileInput.val('');
					
					return;
				}
			}
			
			// Add files to dropdowns.
			_addImageToDropdowns(fileName, isThumbnailImage);
			
			// Select value.
			$dropdown
				.val(fileName)
				.trigger('change');
		}
		
		/**
		 * Handles the image delete button click event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onImageDelete(event) {
			// Loading CSS class name.
			const loadingClass = 'loading';
			
			// Image dropdown container.
			const $configurationRow = $(event.target).parents(selectors.configurationRow);
			
			// Image dropdown.
			const $dropdown = $configurationRow.find(selectors.inputs.dropdown);
			
			// Slide ID.
			const slideId = $configurationRow.parents(selectors.slidePanel).data('id');
			
			// Slide image ID.
			const slideImageId = $configurationRow.data('id');
			
			// Is the dropdown for thumbnail images?
			const isThumbnailImage = !!$dropdown.is(selectors.inputs.thumbnailImageDropdown);
			
			// Selected file name.
			const fileName = $dropdown.val();
			
			// Add loading state.
			$dropdown.addClass(loadingClass);
			
			// Image usage check request options.
			const requestOptions = {
				url: 'admin.php?do=SlidersDetailsAjax/CheckImageUsage',
				data: {
					filename: fileName,
					is_thumbnail: isThumbnailImage,
					slide_id: slideId,
					slide_image_id: slideImageId
				}
			};
			
			// Perform deletion.
			const performDeletion = () => {
				// Put image name into deleter select box.
				elements.deleteImageSelect.append($('<option>', {
					val: fileName,
					class: isThumbnailImage ? 'thumbnail' : ''
				}));
				
				// Delete image from dropdowns.
				_deleteImageFromDropdowns(fileName, isThumbnailImage);
			};
			
			// Check image usage.
			jse.libs.xhr.get(requestOptions).then(response => {
				// Remove loading state.
				$dropdown.removeClass(loadingClass);
				
				if (response.isUsed) {
					// Modal confirmation button element.
					const $confirmButton = elements.modals.deleteImage.find('button.confirm');
					
					// Show modal.
					elements.modals.deleteImage.modal('show');
					
					// Listen to confirmation button click event.
					$confirmButton
						.off('click')
						.on('click', performDeletion);
				} else {
					performDeletion();
				}
			});
		}
		
		/**
		 * Handles the image map edit button click event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onImageMap(event) {
			// Slide image ID.
			const slideImageId = $(event.target)
				.parents(selectors.configurationRow)
				.data('id');
			
			// List element which acts like a data container.
			const $list = $(event.target)
				.parents(selectors.configurationRow)
				.find(selectors.imageMapDataList);
			
			// Image dropdown.
			const $dropdown = $(event.target)
				.parents(selectors.configurationRow)
				.find(selectors.inputs.dropdown);
			
			// Slide image file name.
			const imageFilename = $dropdown.val();
			
			// Is a new image selected?
			const isNewImageSelected = $dropdown.find('option:selected').hasClass(classes.newImage);
			
			// Path to image URL.
			const imageUrl = jse.core.config.get('appUrl') + '/images/slider_images/' + imageFilename;
			
			// Show save first notice modal and return immediately, if the slide image has no ID.
			if (!slideImageId || isNewImageSelected) {
				jse.libs.modal.showMessage(
					jse.core.lang.translate('IMAGE_MAP_MODAL_TITLE', 'sliders'),
					jse.core.lang.translate('SAVE_SLIDER_FIRST_NOTICE_TEXT', 'sliders')
				);
				
				return;
			}
			
			// Show image map modal.
			elements.modals.imageMap.trigger('show', [$list, imageUrl]);
		}
		
		/**
		 * Handles the sort button click event.
		 */
		function _onSortButtonClick() {
			// Indicator CSS classes.
			const indicatorClass = 'mode-on btn-primary';
			
			// Selector string for the slide panel body.
			const slidePanelBodySelector = '.panel-body';
			
			// Slides container tabs, except the active one.
			const $otherTabs = elements.tabHeader
				.children()
				.not('.active');
			
			// Is the sort mode on?
			const isModeOn = elements.buttons.sort.hasClass(indicatorClass);
			
			// Language-specific button texts.
			const enterText = elements.buttons.sort.data('textEnter');
			const exitText = elements.buttons.sort.data('textExit');
			
			// All slide panels.
			const $slides = $this.find(selectors.slidePanel);
			
			// Apply fade effect onto slide panels.
			$slides
				.hide()
				.fadeIn();
			
			// Switch text and toggle indicator class.
			elements.buttons.sort[isModeOn ? 'removeClass' : 'addClass'](indicatorClass)
				.text(isModeOn ? enterText : exitText);
			
			// Toggle create button.
			elements.buttons.create.prop('disabled', !isModeOn);
			
			// Toggle drag handle buttons.
			$slides.find(selectors.icons.drag)[isModeOn ? 'hide' : 'show']();
			
			// Toggle other tabs.
			$otherTabs[isModeOn ? 'fadeIn' : 'fadeOut']();
			
			// Toggle collapser and hide buttons.
			$slides
				.find(selectors.icons.collapser)
				.add(selectors.icons.delete)[isModeOn ? 'show' : 'hide']();
			
			// Save open slide panels.
			if (!isModeOn) {
				openSlidePanels = $slides.filter((index, element) => $(element)
					.find(slidePanelBodySelector)
					.is(':visible'));
			}
			
			// Toggle saved open slide panels.
			openSlidePanels.each((index, element) => $(element)
				.find(selectors.icons.collapser)
				.trigger('click'));
		}
		
		/**
		 * Handles the sort start event.
		 */
		function _onSortStart() {
			// Tab content element for selected language. 
			const $tabBody = $this
				.find(selectors.tabBody)
				.filter(':visible');
			
			// Refresh tab sizes and positions.
			$tabBody.sortable('refreshPositions');
		}
		
		/**
		 * Handles the sort stop event.
		 */
		function _onSortStop() {
			// Register change, to make prompt on page unload.
			_registerChange();
		}
		
		/**
		 * Handles the delete icon click event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onDeleteIconClick(event) {
			// Slide panel element.
			const $slidePanel = $(event.target).parents(selectors.slidePanel);
			
			// Modal confirmation button element.
			const $confirmButton = elements.modals.deleteSlide.find('button.confirm');
			
			// Show modal.
			elements.modals.deleteSlide.modal('show');
			
			// Listen to confirmation button click event.
			$confirmButton
				.off('click')
				.on('click', () => _onDeleteConfirmationButtonClick(elements.modals.deleteSlide, $slidePanel));
		}
		
		/**
		 * Handles the create button click event.
		 */
		function _onCreateButtonClick() {
			// Make a clone of the slide panel template and create a new element.
			const $slidePanel = $(elements.templates.slidePanel.clone().html());
			
			// Tab content element for selected language. 
			const $tabBody = $this
				.find(selectors.tabBody)
				.filter(':visible');
			
			// Slide panels.
			const $slidePanels = $tabBody.find(selectors.slidePanel);
			
			// Next panel index.
			const panelIndex = $slidePanels.length + 1;
			
			// Title for new slide panel.
			const newTitle = `${jse.core.lang.translate('NEW_SLIDE', 'sliders')} ${panelIndex}`;
			
			// Add title to slide panel header.
			$slidePanel
				.find(selectors.slidePanelTitle)
				.text(newTitle);
			
			// Add title to input field.
			$slidePanel
				.find(selectors.inputs.title)
				.val(newTitle);
			
			// Add values to dropdowns.
			if ($slidePanels.length) {
				// Get all image dropdowns of the first panel.
				const $dropdowns = $slidePanels
					.first()
					.find(selectors.inputs.dropdown);
				
				// Get the thumbnail dropdown options.
				const $thumbnailOptions = $dropdowns
					.filter(selectors.inputs.thumbnailImageDropdown)
					.children()
					.clone();
				
				// Get the image dropdown options.
				const $imageOptions = $dropdowns
					.not(selectors.inputs.thumbnailImageDropdown)
					.first()
					.children()
					.clone();
				
				// Replace thumbnail options in new slide panel.
				$slidePanel
					.find(selectors.inputs.thumbnailImageDropdown)
					.empty()
					.append($thumbnailOptions)
					.val('');
				
				// Replace image options in new slide panel.
				$slidePanel
					.find(selectors.inputs.dropdown)
					.not(selectors.inputs.thumbnailImageDropdown)
					.empty()
					.append($imageOptions)
					.val('');
			}
			
			// Add new slide panel element to tab body with fade effect.
			$slidePanel
				.hide()
				.prependTo($tabBody)
				.fadeIn();
			
			// Initialize widgets and extensions on the new slide panel element.
			gx.widgets.init($slidePanel);
			gx.extensions.init($slidePanel);
			
			// Trigger change to show the right action icons.
			$slidePanel
				.find('select')
				.trigger('change');
			
			// Register change, to make prompt on page unload.
			_registerChange();
			
			// Toggle submit buttons.
			toggleSubmitButtons();
		}
		
		/**
		 * Handles the confirmation button click event in the delete confirmation modal.
		 *
		 * @param {jQuery} $modal Delete confirmation modal element.
		 * @param {jQuery} $slidePanel Slide panel element.
		 */
		function _onDeleteConfirmationButtonClick($modal, $slidePanel) {
			// Hide modal.
			$modal.modal('hide');
			
			// Fade out slide panel element and then remove it.
			$slidePanel.fadeOut(400, () => {
				// Remove slide panel.
				$slidePanel.remove()
				
				// Toggle submit buttons.
				toggleSubmitButtons();
			});
			
			// Register change, to make prompt on page unload.
			_registerChange();
		}
		
		/**
		 * Handles the key-up event on the slide title input field.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onTitleKeyup(event) {
			// Title input field.
			const $input = $(event.target);
			
			// Slide panel title element.
			const $title = $input
				.parents(selectors.slidePanel)
				.find(selectors.slidePanelTitle);
			
			// Transfer input value to slide panel title.
			$title.text($input.val());
		}
		
		/**
		 * Handles the mouse-enter event on a configuration row.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onConfigRowMouseEnter(event) {
			// Configuration row element.
			const $row = $(event.target);
			
			// Image map edit icon.
			const $imageMapIcon = $row.find(selectors.icons.imageMap);
			
			// Image map data container list element.
			const $list = $row.find(selectors.imageMapDataList);
			
			// Return immediately, if the image map edit icon does not exist.
			if (!$imageMapIcon.length || !$list.length) {
				return;
			}
			
			if ($list.children().length) {
				$imageMapIcon.removeClass('fa-external-link').addClass('fa-external-link-square');
			} else {
				$imageMapIcon.removeClass('fa-external-link-square').addClass('fa-external-link');
			}
		}
		
		/**
		 * Handles the click event on the save button.
		 */
		function _onSubmitSave() {
			$this
				.parents('form')
				.trigger('submit');
		}
		
		/**
		 * Handles the click event on the refresh list item in the submit button group.
		 */
		function _onSubmitRefresh() {
			$this
				.parents('form')
				.trigger('submit', {refresh: true});
		}
		
		/**
		 * Adds an image to the image dropdowns.
		 *
		 * @param {String} fileName Name of the selected file.
		 * @param {Boolean} [thumbnailImagesOnly = false] Apply on thumbnail image dropdowns only?
		 */
		function _addImageToDropdowns(fileName, thumbnailImagesOnly = false) {
			// Select specific dropdowns.
			const $dropdowns = $this
				.find(selectors.inputs.dropdown)
				[thumbnailImagesOnly ? 'filter' : 'not'](selectors.inputs.thumbnailImageDropdown);
			
			// Create new image option element.
			const $option = $('<option>', {value: fileName, text: fileName, class: classes.newImage});
			
			// Append new options to dropdowns.
			$dropdowns.append($option);
		}
		
		/**
		 * Deletes an image from the image dropdowns.
		 *
		 * @param {String} fileName Name of the selected file.
		 * @param {Boolean} [thumbnailImagesOnly = false] Apply on thumbnail image dropdowns only?
		 */
		function _deleteImageFromDropdowns(fileName, thumbnailImagesOnly = false) {
			// Select all dropdowns.
			const $dropdowns = $this
				.find(selectors.inputs.dropdown)
				[thumbnailImagesOnly ? 'filter' : 'not'](selectors.inputs.thumbnailImageDropdown);
			
			// Remove image option from each dropdown.
			$dropdowns.each((index, element) => {
				// Dropdown element.
				const $dropdown = $(element);
				
				// Remove option.
				$dropdown.find(`[value="${fileName}"]`).remove();
				
				// Set to default value if there are no image file options.
				if ($dropdown.children().length <= 1) {
					$dropdown.val('');
				}
				
				// Trigger change.
				$dropdown.trigger('change');
			});
		}
		
		/**
		 * Disables or enables the submit buttons.
		 */
		function toggleSubmitButtons() {
			// Enable the submit buttons?
			let doEnableSubmitButtons = true;
			
			// Slides.
			const $slides = $this.find(selectors.slidePanel);
			
			// Disable submit buttons, if there are no slides.
			if (!$slides.length) {
				doEnableSubmitButtons = false;
			}
			
			// Disable/Enable submit buttons.
			elements.buttons.submit
				.children()
				.not('ul')
				.prop('disabled', !doEnableSubmitButtons);
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Attach click event handler to sort button.
			elements.buttons.sort.on('click', _onSortButtonClick);
			
			// Attach event handlers to sort actions, slide panel delete button and inputs fields.
			$this
				.on('sortstart', _onSortStart)
				.on('sortstop', _onSortStop)
				.on('click', selectors.icons.delete, _onDeleteIconClick)
				.on('keyup', selectors.inputs.title, _onTitleKeyup)
				.on('change', selectors.inputs.file, _onImageAdd)
				.on('click', selectors.icons.upload, _onUploadIconClick)
				.on('click', selectors.icons.imageDelete, _onImageDelete)
				.on('click', selectors.icons.imageMap, _onImageMap)
				.on('change', selectors.inputs.dropdown, _onImageChange)
				.on('mouseenter', selectors.configurationRow, _onConfigRowMouseEnter);
			
			// Attach event listeners to submit buttons.
			elements.buttons.submitSave.on('click', _onSubmitSave);
			elements.buttons.submitRefresh.on('click', _onSubmitRefresh);
			
			// Attach click event handler to create button.
			elements.buttons.create.on('click', _onCreateButtonClick);
			
			// Activate first tab.
			elements.tabHeader
				.children()
				.first()
				.addClass('active');
			
			// Activate first tab content.
			$this
				.find(selectors.tabBody)
				.first()
				.addClass('active in');
			
			// Trigger dropdown change event to hide the remove icon, if 'do not use' is selected.
			$this
				.find(selectors.inputs.dropdown)
				.trigger('change', [false]);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
);
