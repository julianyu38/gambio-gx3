/* --------------------------------------------------------------
 slider_form.js 2016-12-12
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Slider Edit Form
 *
 * Handles the sliders details form operations.
 */
gx.controllers.module(
	'slider_form',
	
	[
		'xhr',
		'modal',
		'loading_spinner'
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module Selector
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Master data panel selector.
		 *
		 * @type {jQuery}
		 */
		const $masterDataPanel = $this.find('.panel.master-data');
		
		/**
		 * Slide panel containers (each language holds a container, that contains the slide panels).
		 *
		 * @type {jQuery}
		 */
		const $tabContents = $this.find('.tab-pane');
		
		/**
		 * Deleter select box.
		 *
		 * @type {jQuery}
		 */
		const $imageDeleteSelect = $this.find('#delete_images');
		
		/**
		 * Spinner Selector
		 *
		 * @type {jQuery}
		 */
		let $spinner = null;
		
		/**
		 * Do a refresh instead of redirecting?
		 *
		 * @type {Boolean}
		 */
		let doRefresh = false;
		
		/**
		 * Default Options
		 *
		 * @type {Object}
		 */
		const defaults = {};
		
		/**
		 * Final Options
		 *
		 * @type {Object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Handles the form submit event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 * @param {Object} data Event data.
		 */
		function _onFormSubmit(event, data) {
			// Prevent the submit of the form.
			event.preventDefault();
			
			// Check refresh parameter.
			if (data && data.refresh) {
				doRefresh = true;
			}
			
			// Show loading spinner.
			$spinner = jse.libs.loading_spinner.show($this);
			
			// Upload files.
			_uploadFiles()
				.then(_deleteFiles, _showFailMessage)
				.then(_performSubmit, _showFailMessage)
				.then(() => jse.libs.loading_spinner.hide($spinner));
		}
		
		/**
		 * Shows the submit error message modal.
		 */
		function _showFailMessage() {
			// Message texts.
			const errorTitle = jse.core.lang.translate('FORM_SUBMIT_ERROR_MODAL_TITLE', 'sliders');
			const errorMessage = jse.core.lang.translate('FORM_SUBMIT_ERROR_MODAL_TEXT', 'sliders');
			
			// Show modal.
			jse.libs.modal.showMessage(errorTitle, errorMessage);
			
			// Hide loading spinner.
			if ($spinner) {
				jse.libs.loading_spinner.hide($spinner)
			}
		}
		
		/**
		 * Performs the form submit AJAX request.
		 */
		function _performSubmit() {
			// AJAX request options.
			const requestOptions = {
				url: $this.attr('action'),
				data: _getFormData()
			};
			
			jse.libs.xhr.post(requestOptions)
				.done(response => {
					// URL to redirect to.
					const url = doRefresh ? `admin.php?do=SlidersDetails&id=${response.id}` : options.redirectUrl;
					
					// Prevent that the page unload prompt is displayed on save action.
					window.onbeforeunload = null;
					
					// Open overview page.
					window.open(url, '_self');
				})
				.fail(_showFailMessage);
		}
		
		
		/**
		 * Returns the gathered form data.
		 *
		 * @return {Object} Form data.
		 */
		function _getFormData() {
			// Form data object.
			const data = {
				id: $this.data('id')
			};
			
			// Extend form data object with all necessary properties.
			return $.extend(true, data, _getMasterData(), _getSlidesData());
		}
		
		/**
		 * Returns the slider's master data.
		 *
		 * @return {Object} Slider master data.
		 */
		function _getMasterData() {
			const name = $masterDataPanel
				.find('input[name="name"]')
				.val();
			
			const speed = $masterDataPanel
				.find('input[name="speed"]')
				.val();
			
			return {name, speed};
		}
		
		/**
		 * Returns the slides data by iterating over the tab content elements
		 * which represent a container for each language.
		 *
		 * The returned object contains a property for each language.
		 * The key is the language code and the value is an array containing
		 * all slides information as collection.
		 *
		 * Example output:
		 * {
		 *   de: [{
		 *     id: 1,
		 *     thumbnail: 'My picture',
		 *     ...
		 *   }]
		 * }
		 *
		 * @return {Object} Slides data.
		 */
		function _getSlidesData() {
			// Slides data object.
			const slides = {};
			
			// Iterate over each slider panel container.
			$tabContents.each((index, element) => {
				// Slide panel container element.
				const $slidePanelContainer = $(element);
				
				// Slide panel elements.
				const $slidePanels = $slidePanelContainer.find('.panel');
				
				// Get slide panel container language code.
				const languageCode = $slidePanelContainer.data('language');
				
				// Add property to slides data object,
				// which contains the language code as key and the slides data as value.
				slides[languageCode] = $.map($slidePanels, element => _getSlideData(element));
			});
			
			return {slides};
		}
		
		/**
		 * Returns the data for a slide.
		 *
		 * @param {HTMLElement} slidePanel Slide panel element.
		 *
		 * @return {Object} Slide data.
		 */
		function _getSlideData(slidePanel) {
			const $element = $(slidePanel);
			
			return {
				id: $element.data('id'),
				title: $element.find('input[name="title"]').val(),
				alt_text: $element.find('input[name="alt_text"]').val(),
				thumbnail: $element.find('select[name="thumbnail"]').val(),
				link: $element.find('input[name="link"]').val(),
				link_target: $element.find('select[name="link_target"]').val(),
				images: _getSlideImagesData(slidePanel)
			};
		}
		
		/**
		 * Returns the slide images data.
		 *
		 * @param {HTMLElement} slidePanel Slide panel element.
		 *
		 * @return {Object} Slide images data.
		 */
		function _getSlideImagesData(slidePanel) {
			// Image dropdown container elements (without thumbnail).
			const $imageDropdownContainers = $(slidePanel)
				.find('.row.form-group')
				.filter((index, element) => {
					const $dropdown = $(element).find('.dropdown-input');
					
					return $dropdown.length && !$dropdown.is('[name="thumbnail"]');
				});
			
			// Iterate over each dropdown element and retrieve the data.
			return $.map($imageDropdownContainers, element => {
				// Dropdown container element.
				const $element = $(element);
				
				// Dropdown element.
				const $dropdown = $element.find('.dropdown-input');
				
				// Image data object.
				return {
					id: $element.data('id'),
					breakpoint: $dropdown.attr('name').replace('breakpoint_', ''),
					image: $dropdown.val(),
					areas: _getSlideImageAreaData(element)
				};
			});
		}
		
		/**
		 * Returns the slide image area data.
		 *
		 * @param {HTMLElement} slideImageContainer Slide image configuration row element.
		 *
		 * @return {Object} Slide image area data.
		 */
		function _getSlideImageAreaData(slideImageContainer) {
			// Slide image area data container list items.
			const $listItems = $(slideImageContainer)
				.find('.image-map-data-list')
				.children();
			
			// Iterate over each dropdown list item element and retrieve the data.
			return $.map($listItems, element => {
				// List item element.
				const $element = $(element);
				
				return {
					id: $element.data('id'),
					linkTitle: $element.data('linkTitle'),
					linkUrl: $element.data('linkUrl'),
					linkTarget: $element.data('linkTarget'),
					coordinates: $element.data('coordinates')
				}
			});
		}
		
		/**
		 * Performs the images upload AJAX request.
		 *
		 * @return {jQuery.jqXHR} jQuery deferred object.
		 */
		function _uploadFiles() {
			// Form data object.
			const formData = new FormData();
			
			// File inputs.
			const $fileInputs = $this.find(':file');
			
			// Append files to form data object.
			$fileInputs.each((index, element) => {
				// File.
				const file = element.files[0];
				
				// Data entry key.
				const key = element.id + '[]';
				
				// Append file to form data object.
				if (file) {
					formData.append(key, file);
				}
			});
			
			// AJAX request options.
			const requestOptions = {
				url: options.imagesUploadUrl,
				data: formData,
				processData: false,
				contentType: false
			};
			
			// Perform AJAX request.
			return jse.libs.xhr.post(requestOptions);
		}
		
		/**
		 * Performs the image deletion AJAX request.
		 */
		function _deleteFiles() {
			// List of image file names.
			let fileNames = [];
			
			// List of thumbnail images.
			let thumbnailNames = [];
			
			// Fill the file names list.
			$imageDeleteSelect
				.children()
				.each((index, element) => {
					element.className === 'thumbnail' ?
					thumbnailNames.push(element.value) :
					fileNames.push(element.value);
				});
			
			// AJAX request options.
			const requestOptions = {
				url: options.imagesDeleteUrl,
				data: {file_names: fileNames, thumbnail_names: thumbnailNames}
			};
			
			return jse.libs.xhr.post(requestOptions);
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Listen to form submit event.
			$this.on('submit', _onFormSubmit);
			
			// Finish initialization.
			done();
		};
		
		return module;
	}
)
;