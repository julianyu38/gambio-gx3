/* --------------------------------------------------------------
 image_map.js 2016-11-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Controller Module For Slide Image Map Modal.
 *
 * Public methods:
 *  - 'show' shows the dropdown.
 */
gx.controllers.module(
	'image_map',
	
	[`${jse.source}/vendor/jquery-canvas-area-draw/jquery.canvasAreaDraw.min.js`],
	
	function() {
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		/**
		 * Module element (modal element).
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * CSS Class Map
		 *
		 * @type {Object}
		 */
		const classes = {
			// Hidden class.
			hidden: 'hidden',
			
			// New inserted option.
			newOption: 'new'
		};
		
		/**
		 * CSS ID Map
		 *
		 * @type {Object}
		 */
		const ids = {
			canvas: 'canvas'
		};
		
		/**
		 * Element Map
		 *
		 * @type {Object}
		 */
		const elements = {
			// Canvas extension element.
			extension: $this.find(`#${ids.canvas}`),
			
			// Container elements.
			containers: {
				// Image container.
				image: $this.find('.row.image'),
				
				// Canvas container.
				canvas: $this.find('.row.canvas'),
				
				// Action buttons container.
				actionButtons: $this.find('.row.actions')
			},
			
			// Form inputs.
			inputs: {
				// Link area dropdown.
				area: $this.find('#image-map-area'),
				
				// Link title.
				linkTitle: $this.find('#image-map-link-title'),
				
				// Link URL.
				linkUrl: $this.find('#image-map-link-url'),
				
				// Link target.
				linkTarget: $this.find('#image-map-link-target')
			},
			
			// Buttons.
			buttons: {
				// Close modal.
				close: $this.find('.btn.action-close'),
				
				// Create image area.
				create: $this.find('.btn.action-create'),
				
				// Abort edit.
				abort: $this.find('.btn.action-abort'),
				
				// Delete image area.
				delete: $this.find('.btn.action-delete'),
				
				// Apply image area changes.
				apply: $this.find('.btn.action-apply'),
				
				// Reset path.
				reset: $this.find('.btn-default.action-reset')
			},
			
			// Alerts 
			alerts: {
				info: $this.find('.alert')
			}
		};
		
		/**
		 * Value Map
		 *
		 * @type {Object}
		 */
		const values = {
			// Empty string.
			empty: '',
			
			// 'Please select' value.
			nil: '',
			
			// Open in same window.
			sameWindowTarget: '_self'
		};
		
		/**
		 * Module Instance
		 *
		 * @type {Object}
		 */
		const module = {};
		
		/**
		 * Data container list.
		 *
		 * @type {jQuery|null}
		 */
		let $list = null;
		
		/**
		 * Slide image URL.
		 *
		 * @type {String|null}
		 */
		let imageUrl = null;
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Fills the area dropdown with the data container list items.
		 */
		function _fillAreaDropdown() {
			/**
			 * Transfers the data from the data container list item to the area dropdown.
			 *
			 * @param {Number} index Iteration index.
			 * @param {Element} element Iteration element.
			 */
			const _transferToAreaDropdown = (index, element) => {
				// Create new option element.
				const $option = $('<option>');
				
				// Set text content and value and append option to area dropdown.
				$option
					.text(element.dataset.linkTitle)
					.val(index)
					.appendTo(elements.inputs.area);
			};
			
			// Delete all children, except the first one.
			elements.inputs.area
				.children()
				.not(':first')
				.remove();
			
			// Transfer data container list items to area dropdown.
			$list.children()
				.each(_transferToAreaDropdown);
		}
		
		/**
		 * Switches to the default view.
		 */
		function _switchToDefaultView() {
			// Image tag.
			const $image = $(`<img src="${imageUrl}">`);
			
			// Enable area dropdown.
			elements.inputs.area
				.prop('disabled', false);
			
			// Disable and empty the link title input.
			elements.inputs.linkTitle
				.prop('disabled', true)
				.val(values.empty);
			
			// Disable and empty the link URL title input.
			elements.inputs.linkUrl
				.prop('disabled', true)
				.val(values.empty);
			
			// Disable and empty the link target title input.
			elements.inputs.linkTarget
				.prop('disabled', true)
				.val(values.sameWindowTarget);
			
			// Hide button container.
			elements.containers.actionButtons
				.addClass(classes.hidden);
			
			// Show and empty image container and append image.
			elements.containers.image
				.empty()
				.removeClass(classes.hidden)
				.append($image);
			
			// Hide canvas container.
			elements.containers.canvas
				.addClass(classes.hidden);
			
			// Empty extension element.
			elements.extension.empty();
			
			// Show create button.
			elements.buttons.create
				.removeClass(classes.hidden);
			
			// Hide reset button.
			elements.buttons.reset
				.addClass(classes.hidden);
			
			// Hide information alert-box. 
			elements.alerts.info.addClass('hidden');
		}
		
		/**
		 * Switches to the new image area view.
		 */
		function _switchToNewView() {
			// Create new draw extension element.
			const $extension = $(`<div
				id="${ids.canvas}"
				data-gx-extension="canvas_area_draw"
				data-canvas_area_draw-image-url="${imageUrl}">`);
			
			// Enable link title input.
			elements.inputs.linkTitle
				.prop('disabled', false);
			
			// Enable link URL input.
			elements.inputs.linkUrl
				.prop('disabled', false);
			
			// Enable the link target input and set the value to 'self'.
			elements.inputs.linkTarget
				.prop('disabled', false)
				.val(values.sameWindowTarget);
			
			// Disable the area dropdown.
			elements.inputs.area
				.prop('disabled', true);
			
			// Hide create button.
			elements.buttons.create
				.addClass(classes.hidden);
			
			// Show apply button.
			elements.buttons.apply
				.removeClass(classes.hidden);
			
			// Show abort button.
			elements.buttons.abort
				.removeClass(classes.hidden);
			
			// Hide delete button.
			elements.buttons.delete
				.addClass(classes.hidden);
			
			// Show action button container.
			elements.containers.actionButtons
				.removeClass(classes.hidden);
			
			// Hide image container.
			elements.containers.image
				.addClass(classes.hidden);
			
			// Show canvas container.
			elements.containers.canvas
				.removeClass(classes.hidden);
			
			// Remove existing canvas element.
			elements.extension
				.remove();
			
			// Add newly created canvas extension element.
			elements.containers.canvas
				.append($extension);
			
			// Assign new element reference.
			elements.extension = $extension;
			
			// Initialize extension.
			gx.extensions.init($extension);
			
			// Insert text into link title input and focus to that element.
			elements.inputs.linkTitle
				.val(jse.core.lang.translate('NEW_LINKED_AREA', 'sliders'))
				.trigger('focus')
				.trigger('select');
			
			// Show reset button.
			elements.buttons.reset
				.removeClass(classes.hidden);
			
			// Display information alert-box. 
			elements.alerts.info.removeClass('hidden');
		}
		
		/**
		 * Switches to the image area edit view.
		 */
		function _switchToEditView() {
			// Index of the selected option (subtracted 1 to be compatible with data container list element).
			const optionIndex = elements.inputs.area.find('option:selected').index() - 1;
			
			// Corresponding list item in the data container list element.
			const $listItem = $list
				.children()
				.eq(optionIndex);
			
			// Create new draw extension element.
			const $extension = $(`<div
				id="${ids.canvas}"
				data-gx-extension="canvas_area_draw"
				data-canvas_area_draw-image-url="${imageUrl}"
				data-canvas_area_draw-coordinates="${$listItem.data('coordinates')}"
			>`);
			
			// Enable the link title input element and assign value.
			elements.inputs.linkTitle
				.prop('disabled', false)
				.val($listItem.data('linkTitle'));
			
			// Enable the link URL input element and assign value.
			elements.inputs.linkUrl
				.prop('disabled', false)
				.val($listItem.data('linkUrl'));
			
			// Enable the link target input element and assign value.
			elements.inputs.linkTarget
				.prop('disabled', false)
				.val($listItem.data('linkTarget'));
			
			// Disable area dropdown.
			elements.inputs.area
				.prop('disabled', true);
			
			// Show apply button.
			elements.buttons.apply
				.removeClass(classes.hidden);
			
			// Show abort button.
			elements.buttons.abort
				.removeClass(classes.hidden);
			
			// Show delete button.
			elements.buttons.delete
				.removeClass(classes.hidden);
			
			// Hide create button.
			elements.buttons.create
				.addClass(classes.hidden);
			
			// Show action button container.
			elements.containers.actionButtons
				.removeClass(classes.hidden);
			
			// Hide image container.
			elements.containers.image
				.addClass(classes.hidden);
			
			// Show canvas container.
			elements.containers.canvas
				.removeClass(classes.hidden);
			
			// Remove existing canvas element.
			elements.extension
				.remove();
			
			// Add newly created canvas extension element.
			elements.containers.canvas
				.append($extension);
			
			// Assign new element reference.
			elements.extension = $extension;
			
			// Initialize extension.
			gx.extensions.init($extension);
			
			// Show reset button.
			elements.buttons.reset
				.removeClass(classes.hidden);
			
			// Display information alert-box. 
			elements.alerts.info.removeClass('hidden');
		}
		
		/**
		 * Handles the 'input' event on the link title input field.
		 */
		function _onLinkTitleInput() {
			// Link title input value.
			const linkTitle = elements.inputs.linkTitle.val();
			
			// Transfer link title value to option text.
			elements.inputs.area.find('option:selected').text(linkTitle);
		}
		
		/**
		 * Switches to a specific image map view, depending on the area dropdown selection.
		 */
		function _onSwitchArea() {
			// Selected option element.
			const $selectedOption = elements.inputs.area.find('option:selected');
			
			// Is the 'please select' selected?
			const isDefaultValueSelected = !$selectedOption.index();
			
			// Is a newly added option selected?
			const isNewOptionSelected = $selectedOption.hasClass(classes.newOption);
			
			// If option is selected, then switch to default view.
			// Or if the a new option is selected, switch to new area view.
			// Otherwise switch to edit view.
			if (isDefaultValueSelected) {
				_switchToDefaultView();
			} else if (isNewOptionSelected) {
				_switchToNewView();
			} else {
				_switchToEditView();
			}
		}
		
		/**
		 * Creates a new image area.
		 */
		function _onCreate() {
			// Create new option with random value.
			const $option = $('<option>', {
				class: classes.newOption,
				value: Math.random() * Math.pow(10, 5) ^ 1,
				text: jse.core.lang.translate('NEW_LINKED_AREA', 'sliders')
			});
			
			// Add new option to input.
			elements.inputs.area.append($option);
			
			// Select new option in dropdown.
			elements.inputs.area.val($option.val());
			
			// Trigger change event in area dropdown to switch to image area.
			elements.inputs.area.trigger('change');
		}
		
		/**
		 * Handles the 'click' event on the apply button.
		 */
		function _onApply() {
			// Selected option.
			const $selected = elements.inputs.area.find('option:selected');
			
			// Index of the selected option (subtracted 1 to be compatible with data container list element).
			const optionIndex = $selected.index() - 1;
			
			// Is the image area new?
			const isNewImageArea = $selected.hasClass(classes.newOption);
			
			// Image map coordinates.
			const coordinates = elements.extension.find(':hidden').val();
			
			// Create new list item element.
			const $listItem = $(`<li
				data-id="0"
				data-link-title="${elements.inputs.linkTitle.val()}"
				data-link-url="${elements.inputs.linkUrl.val()}"
				data-link-target="${elements.inputs.linkTarget.val()}"
				data-coordinates="${coordinates}"
			>`);
			
			// Trimmed link title value.
			const linkTitle = elements.inputs.linkTitle.val().trim();
			
			// Abort and show modal, if link title or coordinates are empty.
			if (!coordinates || !linkTitle) {
				jse.libs.modal.showMessage(
					jse.core.lang.translate('MISSING_PATH_OR_LINK_TITLE_MODAL_TITLE', 'sliders'),
					jse.core.lang.translate('MISSING_PATH_OR_LINK_TITLE_MODAL_TEXT', 'sliders')
				);
				
				return;
			}
			
			// Add list item, if the selected image area is new.
			// Otherwise replace the already listed item.
			if (isNewImageArea) {
				// Remove new option class.
				$selected.removeClass(classes.newOption);
				
				// Add list item to data container list element.
				$list.append($listItem);
			} else {
				// Replace data container list item with created one.
				$list.children().eq(optionIndex).replaceWith($listItem);
			}
			
			// Select 'please select' dropdown item.
			elements.inputs.area.val(values.nil);
			
			// Trigger 'change' event to get to the default view.
			elements.inputs.area.trigger('change');
		}
		
		/**
		 * Handles the 'click' event on the abort button.
		 */
		function _onAbort() {
			// Selected option.
			const $selected = elements.inputs.area.find('option:selected');
			
			// Is the image area new?
			const isNewImageArea = $selected.hasClass(classes.newOption);
			
			// Remove option from area dropdown, if the selected image area is new.
			// Otherwise the area dropdown will be refilled.
			if (isNewImageArea) {
				$selected.remove();
			} else {
				_fillAreaDropdown();
			}
			
			// Select 'please select' dropdown item.
			elements.inputs.area.val(values.nil);
			
			// Trigger 'change' event to get to the default view.
			elements.inputs.area.trigger('change');
		}
		
		/**
		 * Handles the 'click' event on the delete button.
		 */
		function _onDelete() {
			// Selected option.
			const $selected = elements.inputs.area.find('option:selected');
			
			// Index of the selected option (subtracted 1 to be compatible with data container list element).
			const optionIndex = $selected.index() - 1;
			
			// Delete data container list item.
			$list.children().eq(optionIndex).remove();
			
			// Syncronize area dropdown.
			_fillAreaDropdown();
			
			// Select 'please select' dropdown item.
			elements.inputs.area.val(values.nil);
			
			// Trigger 'change' event to get to the default view.
			elements.inputs.area.trigger('change');
		}
		
		/**
		 * Handles the 'click' event on the reset button.
		 */
		function _onReset() {
			// Trigger the 'reset' event to clear the path.
			elements.extension.trigger('reset');
		}
		
		/**
		 * Handles the 'show' event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 * @param {jQuery} $listParameter Data container list element.
		 * @param {String} imageUrlPath URL to slide image.
		 */
		function _onShow(event, $listParameter, imageUrlPath) {
			// Show modal.
			$this.modal('show');
			
			// Assign data container list element value.
			$list = $listParameter;
			
			// Assign image URL value.
			imageUrl = imageUrlPath;
		}
		
		/**
		 * Handles the 'click' event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onClick(event) {
			// Check, whether the create button is clicked.
			if (elements.buttons.create.is(event.target)) {
				_onCreate();
				return;
			}
			
			// Check, whether the apply button is clicked.
			if (elements.buttons.apply.is(event.target)) {
				_onApply();
				return;
			}
			
			// Check, whether the abort button is clicked.
			if (elements.buttons.abort.is(event.target)) {
				_onAbort();
				return;
			}
			
			// Check, whether the delete button is clicked.
			if (elements.buttons.delete.is(event.target)) {
				_onDelete();
				return;
			}
			
			// Check, whether the reset button is clicked.
			if (elements.buttons.reset.is(event.target)) {
				_onReset();
				return;
			}
		}
		
		/**
		 * Handles the 'change' event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onChange(event) {
			// Check, whether the area dropdown is changed.
			if (elements.inputs.area.is(event.target)) {
				_onSwitchArea();
			}
		}
		
		/**
		 * Handles the modal shown event, which is triggered by the bootstrap modal plugin.
		 */
		function _onModalShown() {
			// Fill the area dropdown with the values from the data container list.
			_fillAreaDropdown();
			
			// Select 'please select' dropdown item and trigger 'change' event to get to the default view.
			elements.inputs.area
				.val(values.nil)
				.trigger('change');
		}
		
		/**
		 * Handles the modal hidden event, which is triggered by the bootstrap modal plugin.
		 */
		function _onModalHidden() {
			// Select 'please select' dropdown item and trigger 'change' event to get to the default view.
			elements.inputs.area
				.val(values.nil)
				.trigger('change');
		}
		
		/**
		 * Handles the 'input' event.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 */
		function _onInput(event) {
			// Check, whether the link title is the changed element.
			if (elements.inputs.linkTitle.is(event.target)) {
				_onLinkTitleInput();
			}
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Bind event handlers.
			$this
				.on('click', _onClick)
				.on('change', _onChange)
				.on('shown.bs.modal', _onModalShown)
				.on('hidden.bs.modal', _onModalHidden)
				.on('show', _onShow)
				.on('input', _onInput);
			
			// Finish initialization.
			done();
		};
		
		return module;
	});