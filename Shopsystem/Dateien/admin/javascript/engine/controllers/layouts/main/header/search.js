/* --------------------------------------------------------------
 search.js 2016-10-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Global Search Controller Module
 */
gx.controllers.module(
	'search',
	
	[
		'user_configuration_service',
		'loading_spinner',
		`${gx.source}/libs/search`,
		`${gx.source}/libs/shortcuts`
	],
	
	function(data) {
		
		'use strict';
		
		// --------------------------------------------------------------------
		// VARIABLES
		// --------------------------------------------------------------------
		
		// Module element, which represents the info box.
		const $this = $(this);
		
		// Module default parameters.
		const defaults = {
			// Recent search area.
			recentSearchArea: 'orders',
			
			// Hyperlink open mode.
			openMode: '_self'
		};
		
		// Module options.
		const options = $.extend(true, {}, defaults, data);
		
		// Elements.
		const elements = {
			// Search term input.
			input: $this.find('.input'),
			
			// Search area list.
			list: $this.find('.list'),
			
			// Search area list item.
			listItems: $this.find('.list-item'),
			
			// Search input trigger button.
			button: $('.actions .search')
		};
		
		// Element selector strings.
		const selectors = {
			// List item search term placeholder selector string.
			placeholder: '.placeholder'
		};
		
		// Attributes.
		const attributes = {
			// Data attributes.
			data: {searchArea: 'searchArea'}
		};
		
		// Event key codes.
		const keyCodes = {
			esc: 27,
			arrowUp: 38,
			arrowDown: 40,
			enter: 13
		};
		
		// Module object.
		const module = {
			bindings: {input: elements.input}
		};
		
		// Current selected search area.
		let searchArea = null;
		
		// Do focus on search input?
		let doInputFocus = false;
		
		// --------------------------------------------------------------------
		// FUNCTIONS
		// --------------------------------------------------------------------
		
		/**
		 * Sets the search area and activates the respective list item element.
		 *
		 * @param {String} area Search area name.
		 */
		function _setSearchArea(area) {
			// Active list item CSS class.
			const activeClass = 'active';
			
			// Set internal area value.
			searchArea = area;
			
			// Set class.
			elements.listItems
				.removeClass(activeClass)
				.filter(`li[data-search-area="${area}"]`)
				.addClass(activeClass);
		}
		
		/**
		 * Handles event for the click action on the input field.
		 */
		function _onInputClick() {
			_togglePlaceholder(true);
			_fillTermInListItems();
			_toggleDropdown(true);
		}
		
		/**
		 * Handles event for the key up press action within the input field.
		 *
		 * @param {jQuery.Event} event Event fired.
		 */
		function _onInputKeyUp(event) {
			switch (event.which) {
				// Hide search bar on escape key.
				case keyCodes.esc:
					_toggleDropdown(false);
					_togglePlaceholder(false);
					elements.input.trigger('blur');
					break;
				
				// Start the search on return key.
				case keyCodes.enter:
					_performSearch();
					break;
				
				// Cycle selection through search entity list items on vertical arrow keys.
				case keyCodes.arrowUp:
				case keyCodes.arrowDown:
					// Direction.
					const isUp = event.which === keyCodes.arrowUp;
					
					// Current search area list item
					const $currentItem = elements.listItems.filter('.active');
					
					// First search area list item.
					const $firstItem = elements.listItems.first();
					
					// Last search area list item.
					const $lastItem = elements.listItems.last();
					
					// Determine the next selected element.
					let $followingItem = isUp ? $currentItem.prev() : $currentItem.next();
					
					// If there is no next element, then the first/last element is selected.
					if (!$followingItem.length) {
						$followingItem = isUp ? $lastItem : $firstItem;
					}
					
					// Fetch search area from next list item.
					const area = $followingItem.data(attributes.data.searchArea);
					
					// Set entity value and select entity on the list item and set placeholder.
					_setSearchArea(area);
					_togglePlaceholder(true, area);
					
					break;
				
				// Fill search term into dropdown list items on letter keypress.
				default:
					_toggleDropdown(true);
					_fillTermInListItems();
			}
		}
		
		/**
		 * Handles event for the click action outside of the controller area.
		 *
		 * @param {jQuery.Event} event Event fired.
		 */
		function _onOutsideClick(event) {
			// Clicked target element.
			const $target = event.target;
			
			// Target element verifiers.
			const isNotTargetSearchArea = !$this.has($target).length;
			const isNotTargetSearchButton = !elements.button.has($target).length;
			
			// Clear the placeholder and hide dropdown,
			// if clicked target is not within search area.
			if (isNotTargetSearchArea && isNotTargetSearchButton) {
				_toggleDropdown(false);
				_togglePlaceholder(false);
				elements.input.trigger('blur');
			}
		}
		
		/**
		 * Handles event for the click action on a dropdown list item.
		 *
		 * @param {jQuery.Event} event Event fired.
		 */
		function _onListClick(event) {
			// Get entity from list item.
			const area = $(event.currentTarget).data(attributes.data.searchArea);
			
			_togglePlaceholder(true, area);
			_setSearchArea(area);
			_performSearch();
		}
		
		/**
		 * Handles event for the button click action.
		 *
		 * @private
		 */
		function _onButtonClick() {
			// Proxy click and focus to the search input field.
			elements.input
				.trigger('click')
				.trigger('focus');
		}
		
		/**
		 * Handles event for window inactivation.
		 */
		function _onWindowBlur() {
			_toggleDropdown(false);
			_togglePlaceholder(false);
			elements.input.trigger('blur');
		}
		
		/**
		 * Handles the set input value custom event method.
		 *
		 * @param {jQuery.Event} event Triggered event.
		 * @param {String} value Desired input value.
		 * @param {Boolean} doFocus Do focus on the input field?
		 */
		function _onSetValue(event, value, doFocus) {
			// Set admin search input value.
			elements.input
				.val(value)
				.trigger('keyup');
			
			// Register admin search input focus trigger.
			doInputFocus = !!doFocus;
		}
		
		/**
		 * Handles JSEngine finish event.
		 */
		function _onEngineFinished() {
			// Trigger focus on admin search input field, if registered.
			if (doInputFocus) {
				elements.input.trigger('focus');
			}
			
			// Set search entity.
			_setSearchArea(options.recentSearchArea);
		}
		
		/**
		 * Fetches the translation for the search area.
		 *
		 * @param {String} area Search area.
		 *
		 * @return {String} Translated area label text.
		 */
		function _getTranslationForArea(area) {
			// Language section.
			const section = 'admin_labels';
			
			// Translation text key.
			const key = `admin_search_${area}`;
			
			// Return fetched translation text.
			return jse.core.lang.translate(key, section);
		}
		
		/**
		 * Prepends the current search term into the dropdown list items.
		 */
		function _fillTermInListItems() {
			elements.list
				.find(selectors.placeholder)
				.each((index, element) => $(element).text(module.bindings.input.get()));
		}
		
		/**
		 * Shows and hides the dropdown.
		 *
		 * @param {Boolean} doShow Show the dropdown?
		 */
		function _toggleDropdown(doShow) {
			// Class for visible dropdown.
			const ACTIVE_CLASS = 'active';
			
			// Toggle dropdown dependent on the provided boolean value.
			elements.list[doShow ? 'addClass' : 'removeClass'](ACTIVE_CLASS);
		}
		
		/**
		 * Shows and hides the input placeholder.
		 *
		 * @param {Boolean} doShow Show the placeholder?
		 * @param {String} area Search area to show as placeholder (optional).
		 */
		function _togglePlaceholder(doShow, area) {
			elements.input.prop('placeholder', doShow ? _getTranslationForArea(area || searchArea
				|| defaults.recentSearchArea) : '');
		}
		
		/**
		 * Saves the search entity to the user configuration and performs the search.
		 */
		function _performSearch() {
			// Set default search area if non provided. 
			searchArea = searchArea || defaults.recentSearchArea;
			
			// Check search area URL availability.
			if (!Object.keys(jse.libs.search.urls).includes(searchArea)) {
				throw new Error(`No URL for search area "${searchArea}" found.`);
			}
			
			// Display loading spinner. 
			jse.libs.loading_spinner.show(elements.list, '9999');
			
			// Compose search URL with search term.
			const url = jse.libs.search.urls[searchArea] + encodeURIComponent(module.bindings.input.get());
			
			// Save selected entity to server via user configuration service.
			jse.libs.user_configuration_service.set({
				data: {
					userId: jse.core.registry.get('userId'),
					configurationKey: jse.libs.search.configurationKey,
					configurationValue: searchArea	
				},
				onSuccess() {
					// Open composed URL.
					window.open(url, options.openMode);
				},
				onError() {
					// Open composed URL.
					window.open(url, options.openMode);
				}
			});
		}
		
		/**
		 * Registers the shortcut for the search.
		 */
		function _registerShortcut() {
			// Shortcut library abbreviation.
			const lib = jse.libs.shortcuts;
			
			// Combination name.
			const name = 'openSearch';
			
			// Key combination (CTRL + SHIFT + F).
			const combination = [lib.KEY_CODES.CTRL_L, lib.KEY_CODES.SHIFT_L, 70];
			
			// Callback function.
			const callback = () => elements.input.trigger('click').trigger('focus');
			
			// Register shortcut.
			lib.registerShortcut(name, combination, callback);
		}
		
		// --------------------------------------------------------------------
		// INITIALIZATION
		// --------------------------------------------------------------------
		
		module.init = done => {
			// Bind list item event handler.
			elements.listItems.on('click', _onListClick);
			
			// Bind button event handler.
			elements.button.on('click', _onButtonClick);
			
			// Bind input event handlers.
			elements.input
				.on('click', _onInputClick)
				.on('keyup', _onInputKeyUp);
			
			// Bind window event handlers.
			$(window)
				.on('click', _onOutsideClick)
				.on('blur', _onWindowBlur);
			
			// Bind set input value event handler.
			$this.on('set:value', _onSetValue);
			
			// Bind JSEngine ready state event handler.
			$(document).on('JSENGINE_INIT_FINISHED', _onEngineFinished);
			
			// Register shortcut.
			_registerShortcut();
			
			// Finish initialization.
			done();
		};
		
		return module;
	});
