/* --------------------------------------------------------------
 images_input.js 2017-09-21
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Product Images Input Fields Controller
 *
 * This controller adds indices to the images input
 *
 * @module Controllers/images_input
 */
gx.controllers.module(
    'images_input',

    [],

    function () {
        const elements = {
            module: $(this)
        };

        const selectors = {
            imageWrapper: '.product-image-wrapper',
            inputs: {
                showImageCheckbox: '[data-show-image]'
            }
        };

        const module = {};

        function onFormSubmit() {
            const imageWrappers = elements.module.find(selectors.imageWrapper);

            function modifyShowImageCheckbox(index, element) {
                const inputElement = $(element).find(selectors.inputs.showImageCheckbox);
                const isChecked = inputElement.parent().hasClass('checked');

                inputElement
                    .attr('name', `image_show[${index}]`)
                    .val(isChecked ? 'on' : 'off');
            }

            imageWrappers.each(modifyShowImageCheckbox);
        }

        module.init = done => {
            elements.module.on('submit', onFormSubmit);

            done();
        };

        return module;
    });
