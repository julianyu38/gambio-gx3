/* --------------------------------------------------------------
 update_stock.js 2018-05-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module(
	// ------------------------------------------------------------------------
	// CONTROLLER NAME
	// ------------------------------------------------------------------------
	'update_stock',
	
	// ------------------------------------------------------------------------
	// CONTROLLER LIBRARIES
	// ------------------------------------------------------------------------
	[],
	
	// ------------------------------------------------------------------------
	// CONTROLLER BUSINESS LOGIC
	// ------------------------------------------------------------------------
	function(data) {
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Controller reference.
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Default options for controller,
		 *
		 * @type {object}
		 */
		const defaults = {};
		
		/**
		 * Final controller options.
		 *
		 * @type {object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module object.
		 *
		 * @type {{}}
		 */
		const module = {};
		
		// ------------------------------------------------------------------------
		// PRIVATE METHODS
		// ------------------------------------------------------------------------
		
		// ------------------------------------------------------------------------
		// EVENT HANDLER
		// ------------------------------------------------------------------------
		
		/**
		 * Click handler for the create role button.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function _updateStockValue() {
			$('input[name="update_stock"]').not($this).val($this.is(':checked')? '1' : '0');
		}
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		module.init = done => {
			$this.on('change', _updateStockValue);
			
			done();
		}
		
		return module;
	}
);