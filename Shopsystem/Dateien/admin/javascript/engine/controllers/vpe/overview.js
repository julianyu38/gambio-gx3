/* --------------------------------------------------------------
 create_missing_documents.js 2016-10-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx.controllers.module('overview', ['modal', 'xhr', gx.source + '/libs/info_messages'], function(data) {
	
	'use strict';
	
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
	
	/**
	 * Module Selector
	 *
	 * @type {jQuery}
	 */
	const $this = $(this);
	
	/**
	 * Default Module Options
	 *
	 * @type {object}
	 */
	const defaults = {};
	
	/**
	 * Final Module Options
	 *
	 * @type {object}
	 */
	const options = $.extend(true, {}, defaults, data);
	
	/**
	 * Module Object
	 *
	 * @type {object}
	 */
	const module = {};
	
	/**
	 * VPE Creation modal.
	 *
	 * @type {*}
	 */
	const $creationModal = $('.create-modal');
	
	/**
	 * VPE edit modal.
	 *
	 * @type {*}
	 */
	const $editModal = $('.edit-modal');
	
	/**
	 * VPE remove confirmation modal.
	 *
	 * @type {*}
	 */
	const $removeConfirmationModal = $('.remove-confirmation-modal');
	
	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------
	
	/**
	 * Initializes the creation modal.
	 *
	 * @private
	 */
	const _initCreationModal = () => {
		const $inputs = $creationModal.find('input[type="text"]');
		let i = 0;
		
		for (; i < $inputs.length; i++) {
			$inputs[i].value = '';
			$($inputs[i]).parent().removeClass('has-error');
		}
		$creationModal.find('p.vpe-modal-info')
			.first()
			.text(jse.core.lang.translate('TEXT_INFO_INSERT_INTRO', 'products_vpe'))
			.removeClass('text-danger');
		$creationModal.modal('show');
	};
	
	/**
	 * Initializes the edit modal.
	 *
	 * @param e Event object to fetch targets id.
	 * @private
	 */
	const _initEditModal = e => {
		jse.libs.xhr.get({
			url: './admin.php?do=VPEAjax/getById&id=' + e.target.dataset.id
		}).done(r => {
			const $editModalForm = $editModal.find('form');
			const $idInput = $('<input/>').attr('type', 'hidden').attr('name', 'id');
			let $hiddenDefaultInput;
			$editModalForm.empty();
			
			$idInput.val(r.id.toString());
			$editModalForm.append($idInput);
			$editModal.find('p.vpe-modal-info')
				.first()
				.text(jse.core.lang.translate('TEXT_INFO_EDIT_INTRO', 'products_vpe'))
				.removeClass('text-danger');
			
			for (let languageCode in r.names) {
				let $formGroup = $('<div/>').addClass('form-group');
				let $inputContainer = $('<div/>').addClass('col-sm-12');
				let $input = $('<input/>')
					.attr('type', 'text')
					.attr('data-gx-widget', 'icon_input')
					.attr('data-lang-id', r.languageIds[languageCode])
					.attr('name', 'name[' + r.languageIds[languageCode] + ']')
					.addClass('form-control')
					.val(r.names[languageCode]);
				
				$inputContainer.append($input);
				$formGroup.append($inputContainer);
				$editModalForm.append($formGroup);
			}
			
			if (r.default) {
				$hiddenDefaultInput = $('<input/>').attr('type', 'hidden').attr('name', 'is-default').val('1');
				$editModalForm.append($hiddenDefaultInput);
			}
			
			$editModalForm.append(_createDefaultSwitcher(r.default));
			gx.widgets.init($editModalForm);
			$editModal.modal('show');
		});
	}
	
	/**
	 * Creates the switcher to set the vpe default.
	 *
	 * @param {bool} isDefault If true, the switcher is checked.
	 * @private
	 */
	const _createDefaultSwitcher = (isDefault) => {
		const $formGroup = $('<div/>').addClass('form-group');
		const $label = $('<label/>')
			.addClass('col-sm-2 control-label')
			.text(jse.core.lang.translate('TEXT_DEFAULT', 'admin_general'));
		const $checkboxContainer = $('<div/>').addClass('col-sm-10').attr('data-gx-widget', 'checkbox');
		const $checkbox = $('<input/>')
			.attr('type', 'checkbox')
			.attr('name', 'default')
			.attr('title', jse.core.lang.translate('TEXT_DEFAULT', 'admin_general'));
		if (isDefault) {
			$checkbox.prop('checked', true);
		}
		$checkboxContainer.append($checkbox);
		
		return $formGroup.append($label).append($checkboxContainer);
	}
	
	/**
	 * Initializes the remove confirmation modal.
	 *
	 * @param e Event object to fetch targets id.
	 * @private
	 */
	const _initRemoveConfirmationModal = e => {
		$removeConfirmationModal.modal('show');
		jse.libs.xhr.get({
			url: './admin.php?do=VPEAjax/getById&id=' + e.target.dataset.id
		}).done(r => {
			const $info = $('.vpe-modal-remove-info');
			$info.empty();
			$('.vpe-remove-id').val(r.id);
			
			for (let languageCode in r.names) {
				let newContent = languageCode + ': ' + r.names[languageCode] + '<br/>';
				let oldContent = $info.html();
				$info.html(oldContent + newContent);
			}
		});
	}
	
	/**
	 * Sends an ajax request to store a new vpe entity.
	 *
	 * @private
	 */
	const _storeVpe = () => {
		if (_validateInputFields('create')) {
			jse.libs.xhr.post({
				url: './admin.php?do=VPEAjax/store',
				data: _inputData('create')
			})
				.done(() => _renderTable(jse.core.lang.translate('TEXT_INFO_ADD_SUCCESS', 'products_vpe')));
		}
	}
	
	/**
	 * Sends an ajax request to update a new vpe entity.
	 *
	 * @private
	 */
	const _editVpe = () => {
		if (_validateInputFields('edit')) {
			jse.libs.xhr.post({
				url: './admin.php?do=VPEAjax/edit',
				data: _inputData('edit')
			}).done(() => _renderTable(jse.core.lang.translate('TEXT_INFO_EDIT_SUCCESS', 'products_vpe')));
		}
	};
	
	/**
	 * Sends an ajax request to remove a new vpe entity.
	 *
	 * @private
	 */
	const _removeVpe = () => {
		jse.libs.xhr.post({
			url: './admin.php?do=VPEAjax/remove',
			data: {
				id: $('.vpe-remove-id').val()
			}
		}).done(() => _renderTable(jse.core.lang.translate('TEXT_INFO_DELETE_SUCCESS', 'products_vpe')));
	};
	
	/**
	 * Renders the vpe table again.
	 *
	 * @private
	 */
	const _renderTable = (msg) => {
		$creationModal.modal('hide');
		$editModal.modal('hide');
		$removeConfirmationModal.modal('hide');
		
		if (undefined !== msg) {
			jse.libs.info_box.addSuccessMessage(msg);
		}
		
		jse.libs.xhr.get({
			url: './admin.php?do=VPEAjax/getData'
		}).done(r => {
			let i = 0;
			const $body = $('.vpe-table tbody');
			$body.empty();
			
			for (; i < r.data.length; i++) {
				let $row = $('<tr/>');
				let $dataCol = $('<td/>');
				let $actionsCol = $('<td/>').addClass('actions');
				let $actionsContainer = $('<div/>').addClass('pull-right action-list visible-on-hover');
				let $edit = $('<i/>').addClass('fa fa-pencil edit').attr('data-id', r.data[i].id);
				let isDefault = r.data[i].default ? ' (' + jse.core.lang.translate('TEXT_DEFAULT', 'admin_general')
				                                  + ')' : '';
				let $delete;
				
				$actionsContainer.append($edit);
				
				if (!r.data[i].default) {
					$delete = $('<i/>').addClass('fa fa-trash-o delete').attr('data-id', r.data[i].id);
					$actionsContainer.append($delete);
				}
				
				$actionsCol.append($actionsContainer);
				$dataCol.text(r.data[i].names[r.languageCode] + isDefault);
				
				$row.append($dataCol).append($actionsCol);
				$body.append($row);
			}
			$this.find('.edit').on('click', _initEditModal);
			$this.find('.delete').on('click', _initRemoveConfirmationModal);
		});
	};
	
	/**
	 * Validates the input fields, returns true if they are valid and false otherwise.
	 *
	 * @param {string} modal Modal instance, whether 'create' or 'edit'.
	 * @returns {boolean}
	 * @private
	 */
	const _validateInputFields = (modal) => {
		const $modal = modal === 'edit' ? $editModal : $creationModal;
		const $inputs = $modal.find('input[type="text"]');
		let valid = false;
		
		
		for (let i = 0; i < $inputs.length; i++) {
			if ($inputs[i].value !== '') {
				valid = true;
			}
		}
		
		if (!valid) {
			for (let i = 0; i < $inputs.length; i++) {
				$($inputs[i]).parent().addClass('has-error');
			}
			$modal.find('p.vpe-modal-info')
				.first()
				.text(jse.core.lang.translate('ERROR_INVALID_INPUT_FIELDS', 'products_vpe'))
				.removeClass('text-info')
				.addClass('text-danger');
			
			return false;
		} else {
			for (let i = 0; i < $inputs.length; i++) {
				$($inputs[i]).parent().removeClass('has-error');
			}
			let textConstant = modal === 'edit' ? 'TEXT_INFO_EDIT_INTRO' : 'TEXT_INFO_INSERT_INTRO';
			$modal.find('p.vpe-modal-info')
				.first()
				.text(jse.core.lang.translate(textConstant, 'products_vpe'))
				.removeClass('text-danger');
			
			return true;
		}
	};
	
	/**
	 * Fetches the data from the input fields.
	 *
	 * @param {string} modal Modal instance, whether 'create' or 'edit'.
	 * @returns {{id: int, name: object}}
	 * @private
	 */
	const _inputData = (modal) => {
		const data = {};
		const $modal = modal === 'edit' ? $editModal : $creationModal;
		const $inputs = $modal.find('input[type="text"]');
		const $default = $modal.find('input[name="default"]');
		const $id = $modal.find('input[name="id"]').val();
		const $isDefault = $modal.find('input[name="is-default"]').val();
		
		for (let i = 0; i < $inputs.length; i++) {
			data[$inputs[i].getAttribute('name')] = $inputs[i].value;
		}
		
		if ($default.parent().hasClass('checked')) {
			data['default'] = true;
		}
		
		if (undefined !== $id) {
			data['id'] = $id;
		}
		
		if (undefined !== $isDefault) {
			data['isDefault'] = $isDefault;
		}
		
		return data;
	}
	
	
	// ------------------------------------------------------------------------
	// INITIALIZE
	// ------------------------------------------------------------------------
	
	module.init = function(done) {
		$('#add-vpe').on('click', _initCreationModal);
		$this.find('.edit').on('click', _initEditModal);
		$this.find('.delete').on('click', _initRemoveConfirmationModal);
		
		// actions
		$('#create-vpe').on('click', _storeVpe);
		$('#edit-vpe').on('click', _editVpe);
		$('#remove-vpe').on('click', _removeVpe);
		
		done();
	};
	
	return module;
}); 