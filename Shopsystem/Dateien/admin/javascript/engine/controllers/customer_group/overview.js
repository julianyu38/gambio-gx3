gx.controllers.module(
    'overview',

    // controller libraries
    [
        'xhr', `${gx.source}/libs/info_messages`,
        'modal', `${gx.source}/libs/info_box`,
    ],

    // controller business logic
    function (data) {
        'use strict';

        /**
         * Module Selector.
         *
         * @type {jQuery}
         */
        const $this = $(this);

        /**
         * Default options for controller,
         *
         * @type {object}
         */
        const defaults = {};

        /**
         * Final controller options.
         *
         * @type {object}
         */
        const options = $.extend(true, {}, defaults, data);

        /**
         * Module object.
         *
         * @type {{}}
         */
        const module = {};

        /**
         * Ajax object.
         *
         * @type {object}
         */
        const ajax = jse.libs.xhr;

        /**
         * Info box Object.
         *
         * @type {object} /admin/info_box.js
         */
        const infoBox = jse.libs.info_box;

        /**
         * Language Code.
         *
         * @type {string}
         */
        const langCode = jse.core.config.get('languageCode').toUpperCase();

        /**
         * Language object.
         *
         * @type {object}
         */
        const lang = jse.core.lang;

        /**
         * Customer group creation modal.
         *
         * @type {*}
         */
        const $creationModal = $this.find('.creation-modal');

        /**
         * Customer Group remove confirmation modal.
         *
         * @type {*}
         */
        const $deleteModal = $this.find('.remove-confirmation-modal');

        /**
         * Manufacturer edit modal.
         *
         * @type {*}
         */
        const $editModal = $this.find('.edit-modal');

        // Initializations

        /**
         * Init the customer group create process.
         *
         * @private
         */
        const _initCreateCustomerGroup = () => {
            if (_initValidation($creationModal)) {
                _storeData();
            }
        };

        /**
         * Init the customer group edit process.
         *
         * @private
         */
        const _initEditCustomerGroup = () => {
            if (_initValidation($editModal)) {
                _updateData();
            }
        };

        /**
         * Init the customer group delete process.
         *
         * @private
         */
        const _intiDeleteCustomerGroup = () => {
            _deleteData();
        };

        /**
         * Init the validation process.
         *
         * @param modal
         * @returns {boolean}
         * @private
         */
        const _initValidation = (modal) => {
            return _validateNameInput(modal) && _validateMinMaxPrices(modal) && _validateFormInputs(modal);
        };

        /**
         * Init the cleanup for the creation modal.
         *
         * @private
         */
        const _initCleanupCreationModal = () => {
            _resetForm($creationModal);
            _resetCheckboxes($creationModal);
        };

        /**
         * Init the cleanup for the edit modal.
         *
         * @private
         */
        const _initCleanupEditModal = () => {
            _resetForm($editModal);
            _resetCheckboxes($editModal);
            _resetHiddenFields();
        };

        /**
         * Init the button display handling.
         *
         * @param defaultSetting
         * @param id
         * @private
         */
        const _initDisplaying = (defaultSetting, id) => {
            const $defaultSetting = $editModal.find('.default-button');
            const $defaultButton = $editModal.find('.default-input');
            const $graduatedSetting = $editModal.find('.graduated-prices-button');
            const $waringGuestDefaultText = $editModal.find('.warning-guest-default');
            const defaultCase = _hideElementByValue($defaultSetting, defaultSetting);
            const adminCase = _hideElementByValue($defaultSetting, id, 0);
            const guestCase = _hideElementByValue($defaultSetting, id, 1);
            const guestDefaultCase = guestCase ? _showElementByValue($defaultSetting, defaultCase, guestCase) : false;

            $waringGuestDefaultText.hide();
            _hideElementByValue($graduatedSetting, adminCase);
            _hideElementByValue($defaultButton, guestDefaultCase);
            _showElementByValue($waringGuestDefaultText, guestDefaultCase);
        };

        //Validations

        /**
         * Returns true if the name input not empty and false if empty.
         *
         * @param modal
         * @returns {boolean}
         * @private
         */
        const _validateNameInput = (modal) => {
            _resetNameInputErrorMessage(modal);
            return _setNameInputErrorMessage(modal);
        };

        const _validateMinMaxPrices = (modal) => {
            _resetMinMaxErrorMessage(modal);
            return _setMinMaxErrorMessage(modal);
        };

        /**
         * Checks validity of html5 input tags and shows native html5 error messages if not valid.
         *
         * @param modal
         * @returns {boolean}
         * @private
         */
        const _validateFormInputs = (modal) => {
            const $form = modal.find('.customer-group-form');

            if (!$form[0].checkValidity()) {
                $form.find(':submit').click();
                return false;
            }

            return true;
        };

        //Setter

        /**
         * Sets an Error message to customer-group-modal-info for name input.
         *
         * @param modal
         * @returns {boolean}
         * @private
         */
        const _setNameInputErrorMessage = (modal) => {
            const $nameInput = modal.find('input:first-of-type#customer-group-name');

            if ($nameInput.val() === '') {
                $nameInput.parent().addClass('has-error');
                modal.find('p.customer-group-modal-info')
                    .first()
                    .text(lang.translate('ERROR_MISSING_NAME', 'customers_status'))
                    .addClass('text-danger');
                $('#customer-group-name').focus();
                return false;
            }

            return true
        };

        /**
         * Sets an Error message to customer-group-modal-order-values-info for min max order values.
         *
         * @param modal
         * @returns {boolean}
         * @private
         */
        const _setMinMaxErrorMessage = (modal) => {
            const $minInput = modal.find('input#customer-group-min-order');
            const $maxInput = modal.find('input#customer-group-max-order');

            if ($minInput.val() === '' || $maxInput.val() === '') {
                return true;
            }

            if (Number($minInput.val()) > Number($maxInput.val())) {
                $maxInput.parent().addClass('has-error');
                $minInput.parent().addClass('has-error');
                modal.find('p.customer-group-modal-order-values-info')
                    .first()
                    .text(lang.translate('ERROR_MIN_VALUE_GRATER_THAN_MAX_VALUE', 'customers_status'))
                    .addClass('text-danger');
                $('#customer-group-min-order').focus();
                return false;
            }

            return true
        };

        //Reseter

        /**
         * Resets hidden field sets from edit modal.
         *
         * @private
         */
        const _resetHiddenFields = () => {
            $editModal.find('.default-button').show();
            $editModal.find('.default-input').show();
            $editModal.find('.graduated-prices-button').show();
        };

        /**
         * Resets the form to the default values.
         *
         * @param modal
         * @private
         */
        const _resetForm = (modal) => {
            modal.find('form.customer-group-form')[0].reset();
            modal.find('input[name="show_add_tax"]').val("false");
            modal.find('input[name="base"]').val("0");
        };

        /**
         * resets all checkbox inputs.
         *
         * @param modal
         * @private
         */
        const _resetCheckboxes = (modal) => {
            modal.find('.switcher.checked')
                .each((index, switcher) => $(switcher)
                    .removeClass('checked')
                    .find(':checkbox')
                    .prop('checked', false)
                    .trigger('change'));
        };


        /**
         * Replaces the name required error message with the info message.
         *
         * @param modal
         * @private
         */
        const _resetNameInputErrorMessage = (modal) => {
            if (modal.find('p.customer-group-modal-info').hasClass('text-danger')) {
                modal.find('p.customer-group-modal-info')
                    .first()
                    .text(lang.translate('TEXT_INFO_INSERT_INTRO', 'customers_status'))
                    .removeClass('text-danger');

                modal.find('input:first-of-type#customer-group-name').parent().removeClass('has-error');
            }
        };

        /**
         * Replace the min max error message whit an empty string.
         *
         * @param modal
         * @private
         */
        const _resetMinMaxErrorMessage = (modal) => {
            if (modal.find('p.customer-group-modal-order-values-info').hasClass('text-danger')) {
                modal.find('p.customer-group-modal-order-values-info')
                    .first()
                    .text('')
                    .removeClass('text-danger');

                modal.find('input#customer-group-min-order').parent().removeClass('has-error');
                modal.find('input#customer-group-max-order').parent().removeClass('has-error');
            }
        };

        //Rendering

        /**
         * Renders the delete modal.
         *
         * @param response ajax response to render the right data.
         * @private
         */
        const _renderDeleteModal = (response) => {
            const $info = $deleteModal.find('.remove-info');
            const $name = `${lang.translate('TEXT_INFO_CUSTOMERS_STATUS_NAME', 'customers_status')} ${response.name[langCode]}`;

            $info.empty();
            $deleteModal.find('.customer-group-remove-id').val(response.id);
            $info.append($name);
        };

        /**
         *
         * @param response {{
		 *                   name                   : {
		 *                                             LanguageCode : String
		 *                                            }
		 *                   id                     : number,
		 *                   members                : number,
		 *                   min_order              : number,
		 *                   max_order              : number,
		 *                   discount_price         : number,
		 *                   order_discount_price   : number,
		 *                   payment_unallowed      : string,
		 *                   shipping_unallowed     : string,
		 *                   public                 : bool,
		 *                   order_discount         : bool,
		 *                   graduated_prices       : bool,
		 *                   customer_show_         : bool,
		 *                   show_add_tax           : bool,
		 *                   add_tax                : bool,
		 *                   discount_attributes    : bool,
		 *                   fsk18                  : bool,
		 *                   fsk18_display          : bool,
		 *                   write_reviews          : bool,
		 *                   read_reviews           : bool,
		 *                   default                : bool,
		 *                   }}
         * @private
         */
        const _renderEditModal = (response) => {
            _setNameInputs(response.name);
            delete response.name;

            _initDisplaying(response.default, response.id);
            _setInputData(response);

            $editModal.find('select[name="show_add_tax"]').val(response.show_add_tax.toString());
        };

        /**
         * Renders the overview table with given response data.
         *
         * @param response {{}}  {0{name,id,min_order,...}
		 *                       {1{name,id,min_order,...}
		 *                       {..{       ...         }}
         * @private
         */
        const _renderOverviewTable = (response) => {
            const $body = $('.customer-group-table tbody');
            $body.empty();

            for (let i = 0; i < response.length; i++) {
                const $row = $('<tr/>');
                const $customerNumber = _createTableColumn(response[i].members);
                const $mwstColumn = _createTableColumn(lang.translate(response[i].show_add_tax ? 'TAX_YES' : 'TAX_NO', 'customers_status'));
                const $mwstOrderColumn = _getIconColumn(response[i].add_tax);
                const $discountColumn = _createTableColumn(`${response[i].discount_price}%`);
                const $orderDiscountColumn = _createTableColumn(`${response[i].order_discount_price}%`);
                const $fsk18Column = _getIconColumn(response[i].fsk18_display, response[i].fsk18);
                const $customerReviewsColumn = _getIconColumn(response[i].read_reviews, response[i].write_reviews);
                const $actionColumn = _getActionColumn(response[i].id, response[i].default);
                const $graduatedPricesColumn = response[i].id > 0 ?
                    _getIconColumn(response[i].graduated_prices) :
                    _getIconColumn(false);
                const $nameColumn = _createTableColumn()
                    .attr('title', 'ID : ' + response[i].id)
                    .append(response[i].default ?
                        '<b>' + response[i].name[langCode] + '</b>' :
                        response[i].name[langCode]);

                $row.append($customerNumber)
                    .append($nameColumn)
                    .append($mwstColumn)
                    .append($mwstOrderColumn)
                    .append($discountColumn)
                    .append($orderDiscountColumn)
                    .append($graduatedPricesColumn)
                    .append($fsk18Column)
                    .append($customerReviewsColumn)
                    .append($actionColumn)
                    .appendTo($body);
            }
            $this.find('.delete').on('click', _initDeleteModal);
            $this.find('.edit').on('click', _initEditModal);
        };

        //Ajax handling

        /**
         * Init the render of the edit modal.
         *
         * @param eventObject
         * @private
         */
        const _initEditModal = (eventObject) => {
            ajax.get({
                url: `./admin.php?do=CustomerGroupAjax/getById&id=${eventObject.target.dataset.id}`
            }).done(response => {
                _renderEditModal(response);
                $editModal.modal('show');
            });
        };

        /**
         * Init the render of the overview table.
         *
         * @private
         */
        const _initRenderOverviewTable = () => {
            ajax.get({
                url: './admin.php?do=CustomerGroupAjax/getData'
            }).done(response => {
                _renderOverviewTable(response);
            });
        };

        /**
         * Init the render of the delete modal.
         *
         * @param eventObject {{}}
         * @private
         */
        const _initDeleteModal = (eventObject) => {
            ajax.get({
                url: `./admin.php?do=CustomerGroupAjax/getNameById&id=${eventObject.target.dataset.id}`
            }).done(response => {
                _renderDeleteModal(response);
                $deleteModal.modal('show');
            });
        };

        /**
         * Sends an ajax request to store a new customer group entity in database.
         *
         * @private
         */
        const _storeData = () => {
            ajax.post({
                url: './admin.php?do=CustomerGroupAjax/store',
                data: _createInputData($creationModal),
            }).then(response => {
                if (response.success) {
                    $creationModal.modal('hide');
                    _initRenderOverviewTable();
                    _initCleanupCreationModal();

                    infoBox.addSuccessMessage(lang.translate('TEXT_SAVE_SUCCESS', 'customers_status'));
                }
            });
        };

        const _updateData = () => {
            ajax.post({
                url: './admin.php?do=CustomerGroupAjax/update',
                data: _createInputData($editModal)
            }).then(response => {
                if (response.success) {
                    $editModal.modal('hide');
                    _initRenderOverviewTable();
                    _initCleanupEditModal();
                    infoBox.addSuccessMessage(lang.translate('TEXT_EDIT_SUCCESS', 'customers_status'));
                }
            })
        };

        /**
         * Sends an ajax request to delete an customer group entity from database;
         *
         * @private
         */
        const _deleteData = () => {
            ajax.post({
                url: './admin.php?do=CustomerGroupAjax/delete',
                data: {
                    id: $deleteModal.find('.customer-group-remove-id').val()
                }
            }).then(response => {
                if (response.success) {
                    _initRenderOverviewTable();
                    $deleteModal.modal('hide');
                    infoBox.addSuccessMessage(lang.translate('TEXT_INFO_DELETE_SUCCESS', 'customers_status'));
                }
            });
        };

        //Create functions

        /**
         * Creates an data object from inputs.
         *
         * @param modal the given modal where inputs shod get from.
         * @returns {{}} an data object with input data.
         * @private
         */
        const _createInputData = (modal) => {
            let data = {};
            const $inputs = modal.find('input[type="text"], input[type="number"], input[type="checkbox"]');
            const $id = modal.find('input[name="id"]').val();

            data['show_add_tax'] = modal.find('select[name="show_add_tax"]').val();
            data['base'] = modal.find('select[name="base"]').val();

            data = _getDataFromInputs($inputs, data);

            if (undefined !== $id) {
                data['id'] = $id;
            }

            return data;
        };

        /**
         * Creates an table column with given text value as text attribute.
         *
         * @param textValue
         * @returns {*|jQuery|HTMLElement}
         * @private
         */
        const _createTableColumn = (textValue = '') => {
            return $('<td/>', {
                'text': textValue
            });
        };

        /**
         * Creates an i tag with given class value.
         *
         * @param classValue
         * @returns {*|jQuery|HTMLElement}
         * @private
         */
        const _createITag = (classValue) => {
            return $('<i/>', {
                'class': 'fa ' + classValue,
                'aria-hidden': true
            });
        };

        //Getter

        /**
         * Gets all data from given inputs and saves them into the given data object.
         *
         * @param inputs collection to get the value and states.
         * @param data object to extend the input data in it.
         * @returns {*} data object from input fields.
         * @private
         */
        const _getDataFromInputs = (inputs, data) => {
            for (let i = 0; i < inputs.length; i++) {
                data[inputs[i].getAttribute('name')] =
                    inputs[i].value === ('on' || 'off') ? inputs[i].checked : inputs[i].value;
            }

            return data;
        };

        /**
         * Gets an check or cross icon , determine by boolValue.
         *
         * @param boolValue
         * @private
         */
        const _getIcon = (boolValue) => {
            const check = 'fa-check';
            const cross = 'fa-times';
            
            return boolValue ? _createITag(check) : _createITag(cross);
        };

        /**
         * Returns an table column with an icon in it.
         *
         * @param boolValue
         * @param optionalBool
         * @private
         */
        const _getIconColumn = (boolValue, optionalBool = null) => {
	        if (optionalBool === null) {
		        return _createTableColumn().append(_getIcon(boolValue));
	        }
	
	        return _createTableColumn().append(_getIcon(boolValue).append(' | ').append(_getIcon(optionalBool)));
        };

        /**
         * Returns an action column.
         *
         * @param id
         * @param defaultSetting
         * @private
         */
        const _getActionColumn = (id, defaultSetting) => {
            const $actionsContainer = $('<div/>', {
                'class': 'pull-right action-list visible-on-hover'
            });
            const $actionsColumn = $('<td/>', {
                'class': 'actions'
            });
            const $edit = $('<i/>', {
                'data-id': id.toString(),
                'data-toggle': 'modal',
                'class': 'fa fa-pencil edit',
            });
            const $delete = $('<i/>', {
                'data-id': id.toString(),
                'data-toggle': 'modal',
                'class': 'fa fa-trash-o delete'
            });

            if (id > 1 && !defaultSetting) {
                $actionsContainer.append($edit).append($delete).appendTo($actionsColumn);
            }
            else {
                $actionsContainer.append($edit).appendTo($actionsColumn);
            }

            return $actionsColumn;
        };

        //Setter

        /**
         * Sets checkboxes in from to true, if response data is true.
         *
         * @param response {{}}
         * @param data
         * @returns {boolean}
         * @private
         */
        const _setCheckBoxes = (response, data) => {
            if (response[data] === true) {
                $editModal.find(`input[name="${data}"]`)
                    .prop('checked', response[data])
                    .val('on')
                    .parent('.switcher')
                    .addClass('checked')
                    .trigger('change');

            }
        };

        /**
         * Sets the name inputs by language code.
         *
         * @param name {{
		 *               LanguageCode : String
		 *             }}
         * @private
         */
        const _setNameInputs = (name) => {
            for (let languageCode in name) {
                $editModal.find(`.customer-group-name-${languageCode}`).val(name[languageCode]);
            }
        };

        /**
         * Sets the input data from response object to the input fields.
         *
         * @param response {{}}
         * @private
         */
        const _setInputData = (response) => {
            for (let data  in response) {
                if (typeof(response[data]) === 'boolean') {
                    _setCheckBoxes(response, data);
                }
                else {
                    $editModal.find(`input[name="${data}"]`).val(response[data]);
                }
            }
        };

        // Helper functions

        /**
         * Hides element if firstValue true or the same as the second Value.
         * Returns true if hidden and false if not.
         *
         * @param $element
         * @param firstValue
         * @param secondValue
         * @returns {boolean}
         * @private
         */
        const _hideElementByValue = ($element, firstValue, secondValue = true) => {
            if (firstValue === secondValue) {
                $element.hide();
                return true
            }
            return false;
        };

        /**
         * Shows element if firstValue true or the same as the second Value.
         * Returns true if Shown and false if not.
         *
         * @param $element
         * @param firstValue
         * @param secondValue
         * @returns {boolean}
         * @private
         */
        const _showElementByValue = ($element, firstValue, secondValue = true) => {
            if (firstValue === secondValue) {
                $element.show();
                return true
            }
            return false;
        };

        // event handler

        // initialization
        module.init = done => {


            // initialization logic
            $creationModal.find('.btn-primary').on('click', _initCreateCustomerGroup);
            $deleteModal.find('.btn-danger').on('click', _intiDeleteCustomerGroup);
            $editModal.find('.btn-primary').on('click', _initEditCustomerGroup);
            $deleteModal.find('.btn btn-default').on('click', $deleteModal.find('.remove-info').empty());

            //actions
            $this.find('.delete').on('click', _initDeleteModal);
            $this.find('.edit').on('click', _initEditModal);

            $creationModal.on('hide.bs.modal', _initCleanupCreationModal);
            $editModal.on('hide.bs.modal', _initCleanupEditModal);
            done();
        };
        return module;

    })
;
