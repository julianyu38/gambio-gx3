/* --------------------------------------------------------------
 quick_edit_special_prices_overview_columns.js 2016-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.quick_edit_special_prices_overview_columns = jse.libs.quick_edit_special_prices_overview_columns || {};

(function(exports) {
	
	'use strict';
	
	exports.checkbox = exports.checkbox || {
			data: null,
			minWidth: '50px',
			widthFactor: 0.01,
			orderable: false,
			searchable: false,
			render() {
				return `<input type="checkbox" class="special-price-row-selection" />`
			}
		};
	
	exports.productsName = exports.productsName || {
			data: 'productsName',
			minWidth: '160px',
			widthFactor: 1.6
		};
	
	exports.productsModel = exports.productsModel || {
			data: 'productsModel',
			minWidth: '140px',
			widthFactor: 1.3,
		};
	
	exports.productsPrice = exports.productsPrice || {
			data: 'productsPrice',
			minWidth: '100px',
			widthFactor: 1,
			className: 'numeric'
		};
	
	exports.specialPrice = exports.specialPrice || {
			data: 'specialPrice',
			minWidth: '110px',
			widthFactor: 1,
			className: 'numeric editable'
		};
	
	exports.specialPriceQuantity = exports.specialPriceQuantity || {
			data: 'specialPriceQuantity',
			minWidth: '90px',
			widthFactor: 1,
			className: 'numeric editable',
		};
	
	exports.specialPriceExpiresDate = exports.specialPriceExpiresDate || {
			data: 'specialPriceExpiresDate',
			minWidth: '110px',
			widthFactor: 1.1,
			className: 'numeric editable date'
		};
	
	exports.specialPriceStatus = exports.specialPriceStatus || {
			data: 'specialPriceStatus',
			minWidth: '90px',
			widthFactor: 1.2,
			className: 'status',
			searchable: false,
			render(data) {
				return `<input type="checkbox" ${data ? 'checked' : ''} class="convert-to-switcher" />`
			}
		};
	
	exports.actions = exports.actions || {
			data: null,
			minWidth: '400px',
			widthFactor: 3.2,
			className: 'actions',
			orderable: false,
			searchable: false,
			render(data, type, full, meta) {
				return `					
					<div class="pull-left"></div>
					<div class="pull-right action-list visible-on-hover">
						<div class="btn-group dropdown">
							<button type="button"
									class="btn btn-default"></button>
							<button type="button"
									class="btn btn-default dropdown-toggle"
									data-toggle="dropdown"
									aria-haspopup="true"
									aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right"></ul>
						</div>
					</div>
				`;
			}
		};
	
})(jse.libs.quick_edit_special_prices_overview_columns);