/* --------------------------------------------------------------
 invocies_overview_columns.js 2016-10-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.invoices_overview_columns = jse.libs.invoices_overview_columns || {};

/**
 * ## Invoices Table Column Definitions
 *
 * This module defines the column definition of the invoices overview table. They can be overridden by other
 * scripts by modifying the array with new columns, or by replacing the property values of the contained
 * fields.
 *
 * @module Admin/Libs/invoices_overview_columns
 * @exports jse.libs.invoices_overview_columns
 */
(function(exports) {
	
	'use strict';
	
	exports.checkbox = exports.checkbox || {
			data: null,
			minWidth: '50px',
			widthFactor: 0.01,
			orderable: false,
			searchable: false,
			defaultContent: '<input type="checkbox" />'
		};
	
	exports.invoiceNumber = exports.invoiceNumber || {
			data: 'invoiceNumber',
			minWidth: '130px',
			widthFactor: 1.6,
			className: 'numeric',
			render(data, type, full, meta) {
				// Create a link element.
				const link = document.createElement('a');
				
				// Invoice link parameters.
				const parameters = {
					module: 'OrderAdmin',
					action: 'showPdf',
					type: 'invoice',
					invoice_id: full.DT_RowData.invoiceId
				};
				
				// Compound invoice link.
				const url = `${jse.core.config.get('appUrl')}/admin/request_port.php?${$.param(parameters)}`;
				
				// Set element's link attribute.
				link.setAttribute('href', url);
				link.setAttribute('target', '_blank');
				link.setAttribute('title', data);
				
				// Set element's text content.
				link.textContent = data;
				
				// Return element's entire HTML.
				return link.outerHTML;
			}
		};
	
	exports.invoiceDate = exports.invoiceDate || {
			data: 'invoiceDate',
			minWidth: '100px',
			widthFactor: 1.6,
			render(data, type, full, meta) {
				return moment(data).format('DD.MM.YY - HH:mm')
			}
		};
	
	exports.sum = exports.sum || {
			data: 'sum',
			minWidth: '90px',
			widthFactor: 1,
			className: 'numeric',
			render(data, type, full, meta) {
				return `<span class="tooltip-invoice-sum-block">${data}</span>`;
			}
		};
	
	exports.customer = exports.customer || {
			data: 'customer',
			minWidth: '190px',
			widthFactor: 1.5,
			render(data, type, full, meta) {
				let linkElement = full.DT_RowData.customerId
					? `<a class="tooltip-customer-addresses" 
							href="customers.php?cID=${full.DT_RowData.customerId}&action=edit">${data}</a>`
					: `<span class="tooltip-customer-addresses">${data}</span>`;
				
				if (full.DT_RowData.customerMemos.length > 0) {
					linkElement +=
						` <i class="fa fa-sticky-note-o tooltip-customer-memos tooltip-trigger"
                                aria-hidden="true"></i>`;
				}
				
				return linkElement;
			}
		};
	
	exports.group = exports.group || {
			data: 'group',
			minWidth: '85px',
			widthFactor: 1.2
		};
	
	exports.countryIsoCode = exports.countryIsoCode || {
			data: 'countryIsoCode',
			minWidth: '75px',
			widthFactor: 1.4,
			render(data, type, full, meta) {
				let html = '';
				
				if (data) {
					html =
						`<img src="${jse.core.config.get('appUrl')}/images/icons/flags/${data.toLowerCase()}.png" />&nbsp;`;
				}
				
				const title = jse.core.lang.translate('SHIPPING_ORIGIN_COUNTRY_TITLE', 'configuration')
					+ ': ' + full.DT_RowData.country;
				
				html += `<span title="${title}">${data}</span>`;
				
				return html;
			}
		};
	
	exports.orderId = exports.orderId || {
			data: 'orderId',
			minWidth: '75px',
			widthFactor: 1,
			className: 'numeric',
			render(data, type, full, meta) {
				if (data === 0) {
					return '';
				}
				
				// Create a link element.
				const $link = document.createElement('a');
				
				// URL parameters.
				const parameters = {
					oID: data,
					action: 'edit'
				};
				
				// Compound order link.
				const url = `${jse.core.config.get('appUrl')}/admin/orders.php?${$.param(parameters)}`;
				
				// Set element's link attribute.
				$link.setAttribute('href', url);
				
				// Set element's text content.
				$link.textContent = data;
				
				// Return element's entire HTML.
				return $link.outerHTML;
			}
		};
	
	exports.orderDate = exports.orderDate || {
			data: 'orderDate',
			minWidth: '100px',
			widthFactor: 1.6,
			render(data, type, full, meta) {
				return moment(data).format('DD.MM.YY - HH:mm')
			}
		};
	
	exports.paymentMethod = exports.paymentMethod || {
			data: 'paymentMethod',
			minWidth: '110px',
			widthFactor: 2,
			render(data, type, full, meta) {
				return `<span title="${full.DT_RowData.paymentMethod}">${data}</span>`
			}
		};
	
	exports.status = exports.status || {
			data: 'status',
			minWidth: '120px',
			widthFactor: 2,
			render(data, type, full, meta) {
				return !data ? '' : `
					<span class="order-status tooltip-invoice-status-history label label-${full.DT_RowData.statusId}">
						${data}
					</span>
				`;
			}
		};
	
	exports.actions = exports.actions || {
			data: null,
			minWidth: '400px',
			widthFactor: 4.6,
			className: 'actions',
			orderable: false,
			searchable: false,
			render(data, type, full, meta) {
				return `					
					<div class="pull-left"></div>
					<div class="pull-right action-list visible-on-hover">
						<a href="request_port.php?module=OrderAdmin&action=showPdf&type=invoice`
					+ `&invoice_number=${full.DT_RowData.invoiceNumber}&order_id=${full.DT_RowData.orderId}" 
							target="_blank">
							<i class="fa fa-eye view"></i>
						</a>
						<a href="request_port.php?module=OrderAdmin&action=downloadPdf&type=invoice`
					+ `&invoice_number=${full.DT_RowData.invoiceNumber}&order_id=${full.DT_RowData.orderId}" 
						target="_blank">
							<i class="fa fa-download download"></i>
						</a>
						<i class="fa fa-envelope-o email-invoice"></i>
						<i class="fa fa-trash-o delete"></i>
					
						<div class="btn-group dropdown">
							<button type="button"
									class="btn btn-default"></button>
							<button type="button"
									class="btn btn-default dropdown-toggle"
									data-toggle="dropdown"
									aria-haspopup="true"
									aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right"></ul>
						</div>
					</div>
				`;
			}
		};
})(jse.libs.invoices_overview_columns); 