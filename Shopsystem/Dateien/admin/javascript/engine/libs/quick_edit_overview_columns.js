/* --------------------------------------------------------------
 quick_edit_overview_columns.js 2016-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.quick_edit_overview_columns = jse.libs.quick_edit_overview_columns || {};

(function(exports) {
	
	'use strict';
	
	exports.checkbox = exports.checkbox || {
			data: null,
			minWidth: '50px',
			widthFactor: 0.01,
			orderable: false,
			searchable: false,
			render() {
				return `<input type="checkbox" class="overview-row-selection" />`
			}
		};
	
	exports.id = exports.id || {
			data: 'id',
			minWidth: '65px',
			widthFactor: 0.6,
		};
	
	exports.category = exports.category || {
			data: 'category',
			minWidth: '150px',
			widthFactor: 1.5,
		};
		
	exports.name = exports.name || {
			data: 'name',
			minWidth: '190px',
			widthFactor: 1.5,
			className: 'editable'
		};
	
	exports.model = exports.model || {
			data: 'model',
			minWidth: '90px',
			widthFactor: 1.2,
			className: 'editable'
		};
	
	exports.manufacturer = exports.manufacturer || {
			data: 'manufacturer',
			minWidth: '150px',
			widthFactor: 1.5,
			className: 'editable manufacturer'
		};
	
	exports.quantity = exports.quantity || {
			data: 'quantity',
			minWidth: '75px',
			widthFactor: 1,
			className: 'numeric editable'
		};
	
	exports.price = exports.price || {
			data: 'price',
			minWidth: '85px',
			widthFactor: 1.2,
			className: 'numeric editable product-price'
		};
	
	exports.discount = exports.discount || {
			data: 'discount',
			minWidth: '75px',
			widthFactor: 1,
			className: 'numeric editable'
		};
	
	exports.specialPrice = exports.specialPrice || {
			data: 'specialPrice',
			minWidth: '85px',
			widthFactor: 1.2,
			className: 'numeric editable tooltip-products-special-price'
		};
	
	exports.tax = exports.tax || {
			data: 'tax',
			minWidth: '85px',
			widthFactor: 1.2,
			render(data, type, full, meta){
				let html = '';
				const options = full.DT_RowData.option.tax;
				
				options.forEach(option => {
					html += `<option value="${option.id}" ${full.taxClassId == option.id ? 'selected' : ''}>
							${option.value}
						</option>`;
				});
				
				return `<select class="form-control select-tax">${html}</select>`;
			}
		};
	
	exports.shippingStatusName = exports.shippingStatusName || {
			data: 'shippingStatusName',
			minWidth: '100px',
			widthFactor: 1.2,
			render(data, type, full, meta){
				let html = '';
				const options = full.DT_RowData.option.shipment;
				
				options.forEach(option => {
					html += `<option value="${option.id}" ${full.shippingTimeId == option.id ? 'selected' : ''}>
							${option.value}
						</option>`;
				});
				
				return `<select class="form-control select-shipping-time">${html}</select>`;
			}
		};
	
	exports.weight = exports.weight || {
			data: 'weight',
			minWidth: '75px',
			widthFactor: 1.2,
			className: 'numeric editable'
		};
	
	exports.shippingCosts = exports.shippingCosts || {
			data: 'shippingCosts',
			minWidth: '85px',
			widthFactor: 1.2,
			className: 'numeric editable'
		};
	
	exports.status = exports.status || {
			data: 'status',
			minWidth: '85px',
			widthFactor: 1.2,
			className: 'status',
			searchable: false,
			render(data) {
				return `<input type="checkbox" ${(data ? "checked" : "")} class="convert-to-switcher" />`
			}
		};
	
	exports.actions = exports.actions || {
			data: null,
			minWidth: '450px',
			widthFactor: 5.2,
			className: 'actions',
			orderable: false,
			searchable: false,
			render(data, type, full, meta) {
				return `					
					<div class="pull-left"></div>
					<div class="pull-right action-list visible-on-hover">
						<i class="fa fa-pencil row-edit"></i>
						<i class="fa fa-eye show-product"></i>
					
						<div class="btn-group dropdown">
							<button type="button"
									class="btn btn-default"></button>
							<button type="button"
									class="btn btn-default dropdown-toggle"
									data-toggle="dropdown"
									aria-haspopup="true"
									aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right"></ul>
						</div>
					</div>
				`;
			}
		};
})(jse.libs.quick_edit_overview_columns); 