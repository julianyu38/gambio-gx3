/* --------------------------------------------------------------
 orders_overview_columns.js 2018-01-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.orders_overview_columns = jse.libs.orders_overview_columns || {};

/**
 * ## Orders Table Column Definitions
 *
 * This module defines the column definition of the order overview table. They can be overridden by other
 * scripts by modifying the array with new columns, or by replacing the property values of the contained
 * fields.
 *
 * @module Admin/Libs/orders_overview_columns
 * @exports jse.libs.orders_overview_columns
 * @requires momentjs
 */
(function(exports) {
	
	'use strict';
	
	exports.checkbox = exports.checkbox || {
			data: null,
			minWidth: '50px',
			widthFactor: 0.01,
			orderable: false,
			searchable: false,
			defaultContent: '<input type="checkbox" />'
		};
	
	exports.number = exports.number || {
			data: 'number',
			minWidth: '75px',
			widthFactor: 1,
			className: 'numeric',
			render(data, type, full, meta) {
				let linkElement = '';
				
				if (full.DT_RowData.comment !== '') {
					// Remove double quotes to avoid broken tooltips
					let escapedComment = full.DT_RowData.comment.replace(/"/,'');
					
					linkElement += `
									<i class="fa fa-comment-o tooltip-order-comment tooltip-trigger"
										aria-hidden="true" title="${escapedComment}"></i>&nbsp;
								`;
				}
				
				const editUrl = 'orders.php?' + $.param({
					oID: full.DT_RowData.id,
					action: 'edit',
					overview: $.deparam(window.location.search.slice(1))
				});
				
				linkElement += `
								<a class="tooltip-order-items" href="${editUrl}">
									${full.DT_RowData.id}
								</a>
							`;
				
				return linkElement;
			}
		};
	
	exports.customer = exports.customer || {
			data: 'customer',
			minWidth: '190px',
			widthFactor: 1.5,
			render(data, type, full, meta) {
				let linkElement = full.DT_RowData.customerId
					? `<a class="tooltip-customer-addresses" 
							href="customers.php?cID=${full.DT_RowData.customerId}&action=edit">${data}</a>`
					: `<span class="tooltip-customer-addresses">${data}</span>`;
				
				if (full.DT_RowData.customerMemos.length > 0) {
					linkElement +=
						` <i class="fa fa-sticky-note-o tooltip-customer-memos tooltip-trigger" 
                                aria-hidden="true"></i>`;
				}
				
				return linkElement;
			}
		};
	
	exports.group = exports.group || {
			data: 'group',
			minWidth: '85px',
			widthFactor: 1.2
		};
	
	exports.sum = exports.sum || {
			data: 'sum',
			minWidth: '90px',
			widthFactor: 1,
			className: 'numeric',
			render(data, type, full, meta) {
				return `<span class="tooltip-order-sum-block">${data}</span>`;
			}
		};
	
	exports.paymentMethod = exports.paymentMethod || {
			data: 'paymentMethod',
			minWidth: '110px',
			widthFactor: 2,
			render(data, type, full, meta) {
				return `<span title="${full.DT_RowData.paymentMethod}">${data}</span>`
			}
		};
	
	exports.shippingMethod = exports.shippingMethod || {
			data: 'shippingMethod',
			minWidth: '110px',
			widthFactor: 2,
			className: 'shipping-method',
			render(data, type, full, meta) {
				return `<span title="${full.DT_RowData.shippingMethod}">${data}</span>`
					+ (full.DT_RowData.trackingLinks.length
						? ' <i class="fa fa-truck fa-lg tooltip-tracking-links tooltip-trigger"></i>' : '');
			},
			createdCell(td, cellData, rowData, row, col) {
				(rowData.DT_RowData.trackingLinks.length) ?
				$(td).children(':first')
					.data('orderId', rowData.DT_RowData.id)
					.attr('data-toggle', 'modal')
					.attr('data-target', '.add-tracking-number.modal') :
				$(td)
					.data('orderId', rowData.DT_RowData.id)
					.attr('data-toggle', 'modal')
					.attr('data-target', '.add-tracking-number.modal')
			}
		};
	
	exports.countryIsoCode = exports.countryIsoCode || {
			data: 'countryIsoCode',
			minWidth: '75px',
			widthFactor: 1.4,
			render(data, type, full, meta) {
				let html = '';
				
				if (data) {
					html =
						`<img src="${jse.core.config.get('appUrl')}/images/icons/flags/${data.toLowerCase()}.png" />&nbsp;`;
				}
				
				const title = jse.core.lang.translate('SHIPPING_ORIGIN_COUNTRY_TITLE', 'configuration')
					+ ': ' + full.DT_RowData.country;
				
				html += `<span title="${title}">${data}</span>`;
				
				return html;
			}
		};
	
	exports.date = exports.date || {
			data: 'date',
			minWidth: '100px',
			widthFactor: 1.6,
			render(data, type, full, meta) {
				return moment(data).format('DD.MM.YY - HH:mm');
			}
		};
	
	exports.status = exports.status || {
			data: 'status',
			minWidth: '120px',
			widthFactor: 2,
			render(data, type, full, meta) {
				return `
					<span data-toggle="modal" data-target=".status.modal"
							class="order-status tooltip-order-status-history label label-${full.DT_RowData.statusId}">
						${data}
					</span>
				`;
			}
		};
	
	exports.totalWeight = exports.totalWeight || {
			data: 'totalWeight',
			minWidth: '50px',
			widthFactor: 0.6,
			className: 'numeric'
		};
	
	exports.invoiceNumber = exports.invoiceNumber || {
			data: 'invoiceNumber',
			minWidth: '75px',
			widthFactor: 1,
			render(data, type, full, meta) {
				// Create a 'span' container element.
				const $html = document.createElement('span');
				
				// Iterator function to add a link element into container.
				const _addLinkElement = (invoiceNumber, index, array) => {
					// Is the current iteration the last one?
					const isLastIteration = (index === (array.length - 1));
					
					// Invoice link parameters.
					const parameters = {
						module: 'OrderAdmin',
						action: 'showPdf',
						type: 'invoice',
						order_id: full.DT_RowData.id,
						invoice_number: invoiceNumber
					};
					
					// Compound invoice link.
					const url = `${jse.core.config.get('appUrl')}/admin/request_port.php?${$.param(parameters)}`;
					
					// Create link element.
					const $link = document.createElement('a');
					
					// Set link on element.
					$link.setAttribute('href', url);
					$link.setAttribute('target', '_blank');
					
					// Set invoice number as text on element.
					$link.textContent = invoiceNumber + (isLastIteration ? '' : ', ');
					
					// Append element to container.
					$html.appendChild($link);
				};
				
				// Add tooltip classes to element.
				$html.classList.add('tooltip-invoices', 'tooltip-trigger');
				
				// Iterate over each invoice number and create link.
				full.DT_RowData.invoiceNumbers.forEach(_addLinkElement);
				
				return $html.outerHTML;
			}
		};
	
	exports.actions = exports.actions || {
			data: null,
			minWidth: '350px',
			widthFactor: 4.6,
			className: 'actions',
			orderable: false,
			searchable: false,
			render(data, type, full, meta) {
				let withdrawalIdsHtml = '';
				
				full.DT_RowData.withdrawalIds.forEach((withdrawalId) => {
					withdrawalIdsHtml += `
						<a href="withdrawals.php?id=${withdrawalId}&action=edit" 
								title="${jse.core.lang.translate('TABLE_HEADING_WITHDRAWAL_ID', 'orders')} ${withdrawalId}">
							<img src="html/assets/images/legacy/icons/withdrawal-on.png" 
								class="tooltip-withdrawal tooltip-trigger meta-icon" 
								data-withdrawal-id="${withdrawalId}" />
						</a>
					`;
				});
				
				const editUrl = 'orders.php?' + $.param({
					oID: full.DT_RowData.id,
					action: 'edit',
					overview: $.deparam(window.location.search.slice(1))
				});
				
				let mailStatusHtml = !full.DT_RowData.mailStatus
					? `<i class="fa fa-envelope-o meta-icon tooltip-confirmation-not-sent email-order tooltip-trigger"
						title="${jse.core.lang.translate('TEXT_CONFIRMATION_NOT_SENT', 'orders')}"></i>` : '';
				
				return `
					<div class="pull-left">
						${withdrawalIdsHtml}
						${mailStatusHtml}
					</div>
					
					<div class="pull-right action-list visible-on-hover">
						<a href="${editUrl}">
							<i class="fa fa-eye edit"></i>
						</a>
						<i class="fa fa-trash-o delete"></i>
						
						<div class="btn-group dropdown">
							<button type="button"
									class="btn btn-default"></button>
							<button type="button"
									class="btn btn-default dropdown-toggle"
									data-toggle="dropdown"
									aria-haspopup="true"
									aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right"></ul>
						</div>
					</div>
				`;
			}
		};
})(jse.libs.orders_overview_columns); 