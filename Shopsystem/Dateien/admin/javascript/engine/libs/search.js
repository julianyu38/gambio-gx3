/* --------------------------------------------------------------
 search.js 2016-10-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.search = jse.libs.search || {};

/**
 * ## Admin search library.
 *
 * This module provides the search URLs and the configuration key for the admin search controller.
 * Additionally you can set a pre-defined search value by overriding the default search value.
 *
 * @module Admin/Libs/search
 * @exports jse.libs.search
 */
(function(exports) {
	// User configuration key.
	exports.configurationKey = 'recent_search_area';

	// Search areas URLs.
	exports.urls = {
		// Customers
		customers: 'customers.php?' + $.param({search: ''}),

		// Categories and products
		categories: 'categories.php?' + $.param({search: ''}),

		// Orders
		orders: 'admin.php?' + $.param({
			do: 'OrdersOverview',
			filter: {
				number: ''
			}
		}),
		
		// Invoices
		invoices: 'admin.php?' + $.param({
			do: 'InvoicesOverview',
			filter: {
				invoiceNumber: ''
			}
		})
	};

	/**
	 * Replaces the admin search input value with the given one.
	 *
	 * @param {String} term Search term.
	 * @param {Boolean} doFocus Do focus on the input field?
	 */
	exports.setValue = (term, doFocus) => $('#search-controller').trigger('set:value', [term, doFocus]);
}(jse.libs.search));
