/* --------------------------------------------------------------
 shortcuts.js 2016-09-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.shortcuts = jse.libs.shortcuts || {};

/**
 * ## Shortcuts library.
 *
 * This module holds the registered shortcuts and provides predefined constants for event key codes.
 * Additionally, you are allowed to register a custom shortcut by using the `registerShortcut()` function.
 *
 * Example on how to register a custom shortcut:
 *  jse.libs.shortcuts.registerShortcut('myShortcut', [17, 16, 70], _doThis);
 *
 * @module Admin/Libs/shortcuts
 * @exports jse.libs.shortcuts
 */
(function(exports) {
	// Event key codes map.
	exports.KEY_CODES = {
		// Left control key.
		CTRL_L: 17,

		// Left shift key.
		SHIFT_L: 16,

		// Above number key 1.
		NUM_1: 49,
		NUM_2: 50,
		NUM_3: 51,
		NUM_4: 52,
		NUM_5: 53,
		NUM_6: 54,
		NUM_7: 55,
		NUM_8: 56,
		NUM_9: 57
	};

	// List of registered shortcuts.
	// Structure:
	// [
	//      {
	//          name: 'openSearch',
	//          combination: [17, 16, 70],
	//          callback: () => 'Hello world!'
	//      }
	// ]
	exports.registeredShortcuts = [];

	/**
	 * Registers a shortcut by adding the key combination and the respective callback function to the register object.
	 *
	 * @param {String} name Name of the key combination.
	 * @param {Number[]} combination Array of event key numbers that represent a key combination.
	 * @param {Function} callback Function to be invoked on key combination match.
	 */
	exports.registerShortcut = (name, combination, callback) => {
		// Check combination name.
		if (!name || typeof name !== 'string') {
			throw new Error('Key combination name parameter is missing or invalid');
		}

		// Check combination.
		if (!combination || !Array.isArray(combination)) {
			throw new Error('Key combination parameter is missing or invalid');
		}

		// Check callback function.
		if (!callback || typeof callback !== 'function') {
			throw new Error('Callback function parameter is missing or invalid');
		}

		// Register shortcut.
		exports.registeredShortcuts.push({name, combination, callback});
	}
}(jse.libs.shortcuts));
