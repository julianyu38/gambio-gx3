/* --------------------------------------------------------------
 quick_edit_properties_overview_columns.js 2016-09-29
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.quick_edit_properties_overview_columns = jse.libs.quick_edit_properties_overview_columns || {};

(function(exports) {
	
	'use strict';
	
	exports.checkbox = exports.checkbox || {
			data: null,
			minWidth: '50px',
			widthFactor: 0.01,
			orderable: false,
			searchable: false,
			render() {
				return `<input type="checkbox" class="properties-row-selection" />`
			}
		};
	
	exports.productsName = exports.productsName || {
			data: 'productsName',
			minWidth: '150px',
			widthFactor: 1.6
		};
	
	exports.combiName = exports.combiName || {
			data: 'combiName',
			minWidth: '150px',
			widthFactor: 1.6,
		};
	
	exports.combiModel = exports.combiModel || {
			data: 'combiModel',
			minWidth: '120px',
			widthFactor: 1,
			className: 'editable'
		};
	
	exports.combiQuantity = exports.combiQuantity || {
			data: 'combiQuantity',
			minWidth: '90px',
			widthFactor: 1,
			className: 'numeric editable'
		};
	
	exports.combiPrice = exports.combiPrice || {
			data: 'combiPrice',
			minWidth: '90px',
			widthFactor: 1,
			className: 'numeric combi-price',
			render(data, type, full, meta){
				let html = `<div class="col-lg-12">
								<label class="control-label">${full.combiName}</label>
								<p data-properties-price-type="${full.combiPriceType}" 
								   class="form-control-static values_price">
										${full.combiPrice.values_price}
								</p>
							 </div>`;
				
				
				return `<div class="container">
							<div class="row">
								<div class="form-horizontal">
									<div class="form-group">${html}</div>
								</div>
							</div>
						</div>`;
			}
		};
	
	exports.combiPriceType = exports.combiPriceType || {
			data: 'combiPriceType',
			minWidth: '120px',
			widthFactor: 1.1,
			render(data, type, full, meta){
				let html = '';
				const options = full.option.priceType;
				
				options.forEach(option => {
					html += `<option value="${option.id}" ${full.combiPriceType == option.id ? 'selected' : ''}>
							${option.value}
						</option>`;
				});
				
				return `<select class="form-control select-properties-price-type">${html}</select>`;
			}
		};
	
	exports.combiEan = exports.combiEan || {
			data: 'combiEan',
			minWidth: '90px',
			widthFactor: 1,
			className: 'editable'
		};
	
	exports.combiWeight = exports.combiWeight || {
			data: 'combiWeight',
			minWidth: '90px',
			widthFactor: 1,
			className: 'numeric editable'
		};
	
	exports.combiShippingStatusName = exports.combiShippingStatusName || {
			data: 'combiShippingTimeId',
			minWidth: '120px',
			widthFactor: 1.1,
			render(data, type, full, meta){
				let html = '';
				const options = full.option.shipment;
				
				options.forEach(option => {
					html += `<option value="${option.id}" ${full.combiShippingTimeId == option.id ? 'selected' : ''}>
							${option.value}
						</option>`;
				});
				
				return `<select class="form-control select-properties-shipping-time">${html}</select>`;
			}
		};
	
	
	exports.actions = exports.actions || {
			data: null,
			minWidth: '400px',
			widthFactor: 3.2,
			className: 'actions',
			orderable: false,
			searchable: false,
			render(data, type, full, meta) {
				return `					
					<div class="pull-left"></div>
					<div class="pull-right action-list visible-on-hover">
						
						<div class="btn-group dropdown">
							<button type="button"
									class="btn btn-default"></button>
							<button type="button"
									class="btn btn-default dropdown-toggle"
									data-toggle="dropdown"
									aria-haspopup="true"
									aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right"></ul>
						</div>
					</div>
				`;
			}
		};
})(jse.libs.quick_edit_properties_overview_columns);