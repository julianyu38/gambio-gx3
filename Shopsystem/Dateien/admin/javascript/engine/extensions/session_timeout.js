/* --------------------------------------------------------------
 session_timeout.js 2016-11-10
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2011 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */


gx.extensions.module(
	'session_timeout',
	
	[],
	
	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		let
			/**
			 * Module Selector
			 *
			 * @type {object}
			 */
			$this = $(this),
			
			/**
			 * Default Options
			 *
			 * @type {object}
			 */
			defaults = {
				url: jse.core.config.get('appUrl') + '/admin/admin.php?do=SessionTimeoutAjax',
				timeout: 1000,
			},
			
			/**
			 * Final Options
			 *
			 * @type {object}
			 */
			options = $.extend(true, {}, defaults, data),
			
			/**
			 * Module Object
			 *
			 * @type {object}
			 */
			module = {};
		
		// ------------------------------------------------------------------------
		// FUNCTIONS
		// ------------------------------------------------------------------------
		
		/**
		 * Is firing by <div data-gx-extension="session_timeout" hidden></div>
		 *
		 * @see admin/html/content/admin_menu.html
		 * @private
		 */
		var _initializeTimeout = function() {
			$.getJSON(options.url)
				.done(function(response) {
					if (response.logout === true) {
						return;
					}
					
					options.timeout = response.data[0];
					setTimeout(_initializeTimeout, options.timeout);
				});
		};
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		
		module.init = function(done) {
			$(document).on('JSENGINE_INIT_FINISHED', _initializeTimeout);
			
			done();
		};
		
		return module;
	});