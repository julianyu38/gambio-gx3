/* --------------------------------------------------------------
 sortable.js 2016-09-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Sortable Extension
 *
 * Uses jQuery UI's sortable plugin to realize sortable functionality. You can pass sortable options via
 * data attributes.
 *
 * ### Options
 *
 * **URL | data-sortable-handle | String | Optional**
 *
 * The destination URL to be used after the user clicks on the element.
 *
 * ### Example
 *
 * ```html
 * <div data-gx-extension="sortable" data-sortable-handle=".drag-handle">
 *   <img src="handle.jpg" class="drag-handle" />
 * </div>
 * ```
 *
 * This extension is based in the jQuery UI sortable interaction utility:
 * {@link http://api.jqueryui.com/sortable}
 *
 * @module Admin/Extensions/sortable
 * @requires jQueryUI
 */
gx.extensions.module(
	'sortable',

	[
		`${jse.source}/vendor/jquery-ui/jquery-ui.min.css`,
		`${jse.source}/vendor/jquery-ui/jquery-ui.min.js`,
	],

	function(data) {
		
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------

		// Widget element.
		const $this = $(this);

		// Default widget options (Sortable options).
		const defaults = {
			revert: true,
			containment: 'parent'
		};

		// Widget options.
		const options = $.extend(true, {}, defaults, data);

		// Module instance.
		const module = {};

		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------

		/**
		 * Initialize method of the widget, called by the engine.
		 */
		module.init = done => {
			// Enable sortable.
			$this.sortable(options);

			// Finish initialization.
			done();
		};

		return module;
	});
