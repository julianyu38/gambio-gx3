/* --------------------------------------------------------------
 canvas_area_draw.js 2016-10-27
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Canvas image map area draw plugin extension.
 *
 * Extension to initialize the jQuery plugin 'jquery-canvas-area-draw'.
 *
 * Options:
 *      - image-url {String} URL to image.
 *      - coordinates {String} Comma-separated coordinate values (optional).
 *
 * Events:
 *      - 'reset' to delete shape.
 *
 * @module Admin/Extensions/canvas_area_draw
 */
gx.extensions.module(
	'canvas_area_draw',
	
	[],
	
	function(data) {
		"use strict";
		
		// Module element, which represents a form.
		const $this = $(this);
		
		// Module options.
		const options = $.extend(true, {}, data);
		
		// Module object.
		const module = {};
		
		// Module initialize function.
		module.init = done => {
			$this.canvasAreaDraw(options);
			done();
		};
		
		// Return data to module engine.
		return module;
	}
);