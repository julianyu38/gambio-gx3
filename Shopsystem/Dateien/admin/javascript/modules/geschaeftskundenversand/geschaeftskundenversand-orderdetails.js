/* --------------------------------------------------------------
	geschaeftskundenversand-orderdetails.js 2016-08-31
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$(function() {
	$('div.gkv_orderdetails').closest('div.frame-wrapper').hide();

	var gkvButtonDropdownHandler = function(event)
	{
		var orderId = $(event.target).parents('tr').data('row-id') || $('body').find('#gm_order_id').val();
		window.location = jse.core.config.get('appUrl') + '/admin/admin.php?do=Geschaeftskundenversand/PrepareLabel&oID=' + orderId;
	}

	var interval_counter = 10,
	    interval = setInterval(function () {
		if (jse.libs.button_dropdown && $('.js-button-dropdown').length) {
			clearInterval(interval);
			jse.libs.button_dropdown.mapAction($('.bottom-save-bar'), 'gkv_label_get', 'admin_labels', gkvButtonDropdownHandler);
		}
		if (interval_counter-- === 0) {
			clearInterval(interval);
		}
	}, 400);
});
