/* --------------------------------------------------------------
	geschaeftskundenversand.js 2016-07-06
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/


$(function() {
	'use strict';

	const $table = $('.orders .table-main');

	$table.on('init.dt', function() {

		const _initSingleAction = function($theTable) {
			$theTable.find('.btn-group.dropdown').each(function() {
				const orderId = $(this).parents('tr').data('id'),
					defaultRowAction = $theTable.data('init-default-row-action') || 'edit';

				jse.libs.button_dropdown.addAction($(this), {
					text: jse.core.lang.translate('gkv_label_get', 'module_center_module'),
					href: jse.core.config.get('appUrl') + '/admin/admin.php?do=Geschaeftskundenversand/PrepareLabel&oID=' + orderId,
					class: 'gkv-single',
					data: {configurationValue: 'gkv-single'},
					isDefault: defaultRowAction === 'gkv-single',
				});
			});
		}

		/*
		const _initBulkAction = function() {
			var isDefault = $table.data('defaultBulkAction') === 'gkv-bulk';
			jse.libs.button_dropdown.addAction($('.bulk-action'), {
				text: jse.core.lang.translate('gkv_label_get', 'module_center_module'),
				href: jse.core.config.get('appUrl') + '/admin/admin.php?do=Geschaeftskundenversand/PrepareLabelBulk',
				data: {configurationValue: 'gkv-bulk'},
				isDefault: isDefault,
			});
		}
		*/

		$table.on('draw.dt', () => _initSingleAction($table));

		_initSingleAction($table);
		// _initBulkAction();
	});
});
