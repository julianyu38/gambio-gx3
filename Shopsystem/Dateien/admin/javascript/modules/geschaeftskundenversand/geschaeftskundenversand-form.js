/* --------------------------------------------------------------
	geschaeftskundenversand-form.js 2016-08-22
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$(function() {
	$('#toggle-extd-view').on('click', function(e) {
		e.preventDefault();
		$('.extd-view').toggle();
	});
	$('.extd-view').hide();

	$('select#gkv_product_index').on('change', function(e) {
		const $option = $('option:selected', this),
		      product = $option.data('dhl_product'),
		      isExport = $option.data('dhl_targetarea') !== 'domestic';

		$('input, select', 'div.dhl_service').attr('disabled', 'disabled');
		$('input[type="checkbox"]', 'div.dhl_service').closest('span.single-checkbox').addClass('disabled');
		$('input, select', 'div.dhl_service.' + product).removeAttr('disabled');
		$('input[type="checkbox"]', 'div.dhl_service.' + product).closest('span.single-checkbox').removeClass('disabled');
		$('div.exportdoc').toggle(isExport);
	});
	$('select#gkv_product_index').change();

	$('select.country').on('change', function(e) {
		const shipper_country  = $('select.shipper_country').val(),
		      receiver_country = $('select.receiver_country:visible').val(),
		      isExport         = shipper_country !== receiver_country;
		if(shipper_country !== undefined && receiver_country !== undefined)
		{
			if(isExport === true)
			{
				// alert('to export ' + shipper_country + ' ' + receiver_country);
				$('select#gkv_product_index').val($('select#gkv_product_index option').not('option[data-dhl_targetarea="domestic"]').first().val());
				$('select#gkv_product_index').trigger('change');
				$('div.exportdoc').show();
			}
			else
			{
				// alert('to domestic ' + shipper_country + ' ' + receiver_country);
				$('select#gkv_product_index').val($('select#gkv_product_index option[data-dhl_targetarea="domestic"]').first().val());
				$('select#gkv_product_index').trigger('change');
				$('div.exportdoc').hide();
			}
		}
	});

	$('select#receiver_type').on('change', function(e) {
		const $option = $('option:selected', this),
		      receiver_type = $option.attr('value');
		$('div.receiver_data').each(function() {
			$(this).toggle($(this).hasClass('receiver_' + receiver_type));
		});
		$('select.country').first().trigger('change');
	});
	$('select#receiver_type').trigger('change');

	$('button#customize_shipper').on('click', function(e) {
		e.preventDefault();
		$('div.customize-shipper-button').remove();
		$('div.customize-shipper').css('display', 'inline-block');
	});

	$('button#customize_returnreceiver').on('click', function(e) {
		e.preventDefault();
		$('div.customize-returnreceiver-button').remove();
		$('div.customize-returnreceiver').css('display', 'inline-block');
	});

	$('input[name="identcheck"]').on('change', function(e) {
		$('.dhl_service_identcheck').toggle($(this).get(0).checked);
	});
	$('input[name="identcheck"]').trigger('change');

	/*
	$('select[name="exportdoc/exporttype"]').on('change', function(e) {
		const exportActivated = $(this).val() !== 'NONE';
		$('div.exportdoc').not($(this).closest('div.exportdoc')).toggle(exportActivated);
	});
	$('select[name="exportdoc/exporttype"]').trigger('change');
	*/

	$('button#add_export_position').on('click', function(e) {
		e.preventDefault();
		let $newrow = $('table.exportpositions tr.template_row').clone(true);
		$newrow.removeClass('template_row');
		$('table.exportpositions tbody').append($newrow);
	});

	$('button.remove_row').on('click', function(e) {
		e.preventDefault();
		$(this).closest('tr').remove();
	});

	$('button#cod_helper').on('click', function(e) {
		e.preventDefault();
		let cod_amount = $(this).data('cod_amount');
		$('input[name="cashondelivery"]').val(cod_amount);
	})
	
});
