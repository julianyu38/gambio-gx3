<?php
/* --------------------------------------------------------------
   * $Id: CYYatPref.php,v 1.4 2008/02/17 10:40:47 tobias Exp $
   * Konfiguration des Exports
   --------------------------------------------------------------*/

class CYYatPref {
    var $currency; // Währung, die exportiert wird
    var $customer_status; // Kundengruppe zur Preisberechnung
    var $language; // Sprache der Beschreibungen
    var $username; // Benutzername bei Yatego
    var $password; // Passwort bei Yatego
    var $quantities; // Export der Lagerbestände
    var $exportall; // Export aller Artikel
    var $h2longdesc; // Artikelname in <h2> vor Langbeschreibung
    var $genshortdesc; // Erstellen der Kurzbeschreibung
    var $genpackagesize; // Erstellen der Grundpreis
    var $properties; // Artikeleigenschaften oder Artikelattribute
    var $exportean; // Export EAN

    function CYYatPref() {
        // Initialisierung mit den Werten aus der Datenbank
        $this->currency = YATEGO_CURRENCY;
        $this->customer_status = YATEGO_CUSTOMER_STATUS;
        $this->language = YATEGO_LANGUAGE;
        $this->username = YATEGO_USERNAME;
        $this->password = YATEGO_PASSWORD;
        $this->quantities = YATEGO_QUANTITIES;
        $this->exportall = YATEGO_EXPORTALL;
        $this->h2longdesc = YATEGO_H2LONGDESC;
        $this->genshortdesc = YATEGO_GENSHORTDESC;
        $this->genpackagesize = YATEGO_GENPACKAGESIZE;
        $this->properties = YATEGO_PROPERTIES;
        $this->exportean = YATEGO_EXPORTEAN;
    }

    /* --------------------------------------------------------------
       * Ändern der Währung
       --------------------------------------------------------------*/
    function setCurrency($curr) {
        if($this->currency != $curr) {
            $this->currency = $curr;
            xtc_db_query("UPDATE configuration SET configuration_value='" . $this->currency . "' WHERE configuration_key='YATEGO_CURRENCY'");
            return true;
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern der Kundengruppe
       --------------------------------------------------------------*/
    function setCustomerStatus($cust) {
        if($this->customer_status != $cust) {
            $this->customer_status = $cust;
            xtc_db_query("UPDATE configuration SET configuration_value='" . $this->customer_status . "' WHERE configuration_key='YATEGO_CUSTOMER_STATUS'");
            return true;
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern der Sprache
       --------------------------------------------------------------*/
    function setLanguage($lang) {
        if($this->language != $lang) {
            $this->language = $lang;
            xtc_db_query("UPDATE configuration SET configuration_value='" . $this->language . "' WHERE configuration_key='YATEGO_LANGUAGE'");
            return true;
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern des Yatego Benutzernamens
       --------------------------------------------------------------*/
    function setUsername($user) {
        if($this->username != $user) {
            $this->username = $user;
            xtc_db_query("UPDATE configuration SET configuration_value='" . xtc_db_input($this->username) . "' WHERE configuration_key='YATEGO_USERNAME'");
            return true;
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern des Yatego Passworts
       --------------------------------------------------------------*/
    function setPassword($pass) {
        if($this->password != $pass) {
            $this->password = $pass;
            xtc_db_query("UPDATE configuration SET configuration_value='" . $this->password . "' WHERE configuration_key='YATEGO_PASSWORD'");
            return true;
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern der Lagerbestände
       --------------------------------------------------------------*/
    function setQuantities($quan) {
        if($this->quantities != $quan) {
            $this->quantities = $quan;
            if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->quantities . "' WHERE configuration_key='YATEGO_QUANTITIES'")) {
                return true;
            }
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern der Option zum Exportieren aller Artikel
       --------------------------------------------------------------*/
    function setExportAll($exp) {
        if($this->exportall != $exp) {
            $this->exportall = $exp;
            if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->exportall . "' WHERE configuration_key='YATEGO_EXPORTALL'")) {
                return true;
            }
            xtc_db_query("TRUNCATE TABLE yatego_articles");
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern des Artikelnamens vor Langbeschreibung
       --------------------------------------------------------------*/
    function setH2longdesc($h2) {
        if($this->h2longdesc != $h2) {
            $this->h2longdesc = $h2;
            if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->h2longdesc . "' WHERE configuration_key='YATEGO_H2LONGDESC'")) {
                return true;
            }
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern der Erstellung der Kurzbeschreibung
       --------------------------------------------------------------*/
    function setGenshortdesc($gen) {
        if($this->genshortdesc != $gen) {
            $this->genshortdesc = $gen;
            if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->genshortdesc . "' WHERE configuration_key='YATEGO_GENSHORTDESC'")) {
                return true;
            }
        }
        return false;
    }


    /* --------------------------------------------------------------
       * Ändern der Erstellung der Grundpreis
       --------------------------------------------------------------*/
    function setGenpackagesize($gen) {
        if($this->genpackagesize != $gen) {
            $this->genpackagesize = $gen;
            if(xtc_db_query("UPDATE configuration SET configuration_value='".$this->genpackagesize."' WHERE configuration_key='YATEGO_GENPACKAGESIZE'")) {
                return true;
            }
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Ändern der Variantenoption
       --------------------------------------------------------------*/
        function setProperties($gen) {
            if($this->properties != $gen) {
                $this->properties = $gen;
                if(xtc_db_query("UPDATE configuration SET configuration_value='".$this->properties."' WHERE configuration_key='YATEGO_PROPERTIES'")) {
                    return true;
                }
            }
            return false;
        }


    /* --------------------------------------------------------------
       * Ändern der Option zum Exportieren der EAN
       --------------------------------------------------------------*/
    function setExportEAN($exp) {
        if($this->exportean != $exp) {
            $this->exportean = $exp;
            if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->exportean . "' WHERE configuration_key='YATEGO_EXPORTEAN'")) {
                return true;
            }
        }
        return false;
    }

    /* --------------------------------------------------------------
       * Anzeige der Einstellungen
       * Auswahl wird per POST an yatego.php geschickt
       --------------------------------------------------------------*/
    function display() {

        $link_yatego = xtc_href_link('yatego.php');
        if (strpos($link_yatego, '?') !== false)
        {
            $link_yatego .= '&';
        }
        else
        {
            $link_yatego .= '?';
        }

        echo '<div class="gx-container"><form class="grid" action="' . $link_yatego . 'module=yatego&amp;section=preferences" method="post" accept-charset="' . $_SESSION['language_charset'] . '">';
        ?>
        <fieldset class="span6">
        <h3>Export Einstellungen</h3>
        <div class="control-group">
            <label for="yatego_language">Sprache</label>
            <select id="yatego_language" name="yatego_language" class="input-default">
            <?php
            // Es werden nur die Sprachen angezeigt, die auch installiert sind.
            // Hierfür wird die Klasse des XT:Commerce verwendet
            if (!isset($lng) && !is_object($lng)) {
                include(DIR_WS_CLASSES . 'language.php');
                $lng = new language;
            }
            reset($lng->catalog_languages);
            foreach($lng->catalog_languages as $key => $value) {
                echo '<option '.($value['id']==$this->language?'selected':'').' value="'.$value['id'].'">'.$value['name'].'</option>';
            }
            ?>
            </select>
        </div>
        <div class="control-group">
            <label for="yatego_currency">W&auml;hrung</label>
            <select id="yatego_currency" name="yatego_currency" class="input-default">
                <?php
                $currencies=xtc_db_query("SELECT title, code FROM ".TABLE_CURRENCIES);
                while($currencies_data=xtc_db_fetch_array($currencies)) {
                    echo '<option '.($currencies_data['code']==$this->currency?'selected':'').' value="'.$currencies_data['code'].'">'.$currencies_data['title'].'</option>';
                }
                ?>
            </select>
        </div>
        <div class="control-group">
            <label for="yatego_customer_status">Kundengruppe</label>
            <select id="yatego_customer_status" name="yatego_customer_status" class="input-default">
                <?php
                $customers_statuses_array = xtc_get_customers_statuses();
                foreach($customers_statuses_array as $value) {
                    echo '<option '.($value['id']==$this->customer_status?'selected':'').' value="'.$value['id'].'">'.$value['text'].'</option>';
                }
                ?>
            </select>
        </div>
        <div class="control-group">
            <label for="yatego_properties">Varianten</label>
            <select id="yatego_properties" name="yatego_properties" class="input-default">
                <?php
                $properties_array[] = array(
                    'id' => 'properties',
                    'text' => 'Artikeleigenschaften'
                );
                $properties_array[] = array(
                    'id' => 'attributes',
                    'text' => 'Artikelattribute'
                );
                foreach($properties_array as $value) {
                    echo '<option '.($value['id']==$this->properties?'selected':'').' value="'.$value['id'].'">'.$value['text'].'</option>';
                }
                ?>
            </select>
        </div>
        <?php
		echo '<div class="control-group">
				<label for="yatego_username">Yatego Benutzername</label>
				<input type="text" name="yatego_username" id="yatego_username" value="' . htmlspecialchars($this->username) . '" class="input-default" />
			</div>
			<div class="control-group">
				<label for="yatego_password">Yatego Passwort</label>
				<input type="password" name="yatego_password" id="yatego_password" value="' . htmlspecialchars($this->password) . '" class="input-default" />
			</div>
			<div class="control-group">
				<label for="yatego_quantities">Lagerbest&auml;nde exportieren</label>
				<div class="gx-container" data-gx-widget="checkbox">
				    <input class="pull-left" type="checkbox" name="yatego_quantities" id="yatego_quantities" value="1" '. ($this->quantities == 'false' ? '' : 'checked="checked"') .' />
				</div>
			</div>
			<div class="control-group">
				<label for="yatego_exportall">Alle Artikel exportieren</label>
				<div class="gx-container" data-gx-widget="checkbox">
				    <input class="pull-left" type="checkbox" name="yatego_exportall" id="yatego_exportall" value="1" '. ($this->exportall == 'false' ? '' : 'checked="checked"') .' />
				</div>
			</div>
			<div class="control-group">
				<label for="yatego_genshortdesc">Keywords exportieren</label>
				<div class="gx-container" data-gx-widget="checkbox">
				    <input class="pull-left" type="checkbox" name="yatego_genshortdesc" id="yatego_genshortdesc" value="1" '. ($this->genshortdesc == 'false' ? '' : 'checked="checked"') .' />
				</div>
			</div>
			<div class="control-group">
				<label for="yatego_genpackagesize">Grundpreis generieren</label>
				<div class="gx-container" data-gx-widget="checkbox">
				    <input class="pull-left" type="checkbox" name="yatego_genpackagesize" id="yatego_genpackagesize" value="1" '. ($this->genpackagesize == 'false' ? '' : 'checked="checked"') .' />
				</div>
			</div>
			<div class="control-group">
				<label for="yatego_genpackagesize">EAN exportieren</label>
				<div class="gx-container" data-gx-widget="checkbox">
				    <input class="pull-left" type="checkbox" name="yatego_exportean" id="yatego_exportean" value="1" '. ($this->exportean == 'false' ? '' : 'checked="checked"') .' />
				</div>
			</div>
			<br>
			<input type="submit" class="button" style="width:auto" value="Speichern" />
			</fieldset>
			</form>
			</div>';
	}
}