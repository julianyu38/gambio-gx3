(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* --------------------------------------------------------------
 collection.js 2016-06-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

(function () {

	'use strict';

	/**
  * Class Collection
  * 
  * This class is used to handle multiple modules of the same type (controllers, extensions ...).
  *
  * @class JSE/Constructors/Collection
  */

	var Collection = function () {
		/**
   * Class Constructor 
   * 
   * @param {String} name The collection name - must be unique.
   * @param {String} attribute The attribute that will trigger collection's modules.
   * @param {Object} namespace Optional, the namespace instance where the collection belongs.
   */
		function Collection(name, attribute, namespace) {
			_classCallCheck(this, Collection);

			this.name = name;
			this.attribute = attribute;
			this.namespace = namespace;
			this.cache = {
				modules: {},
				data: {}
			};
		}

		/**
   * Define a new engine module.
   *
   * This function will define a new module into the engine. Each module will be stored in the
   * collection's cache to prevent unnecessary file transfers. The same happens with the default
   * configuration that append to the module definition.
   *
   * @param {String} name Name of the module (same as the filename).
   * @param {Array} dependencies Array of libraries that this module depends on (will be loaded asynchronously).
   * Apply only filenames without extension e.g. ["emails"].
   * @param {Object} code Contains the module code (function).
   */


		_createClass(Collection, [{
			key: 'module',
			value: function module(name, dependencies, code) {
				// Check if required values are available and of correct type.
				if (!name || typeof name !== 'string' || typeof code !== 'function') {
					jse.core.debug.warn('Registration of the module failed, due to bad function call', arguments);
					return false;
				}

				// Check if the module is already defined.
				if (this.cache.modules[name]) {
					jse.core.debug.warn('Registration of module "' + name + '" skipped, because it already exists.');
					return false;
				}

				// Store the module to cache so that it can be used later.
				this.cache.modules[name] = {
					code: code,
					dependencies: dependencies
				};
			}

			/**
    * Initialize Module Collection
    *
    * This method will trigger the page modules initialization. It will search all
    * the DOM for the "data-gx-extension", "data-gx-controller" or
    * "data-gx-widget" attributes and load the relevant scripts through RequireJS.
    *
    * @param {jQuery} $parent Optional (null), parent element will be used to search for the required modules.
    * 
    * @return {jQuery.Deferred} namespaceDeferred Deferred object that gets processed after the
    * module initialization is finished.
    */

		}, {
			key: 'init',
			value: function init() {
				var _this = this;

				var $parent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

				// Store the namespaces reference of the collection.
				if (!this.namespace) {
					throw new Error('Collection cannot be initialized without its parent namespace instance.');
				}

				// Set the default parent-object if none was given.
				if ($parent === undefined || $parent === null) {
					$parent = $('html');
				}

				var attribute = 'data-' + this.namespace.name + '-' + this.attribute;
				var namespaceDeferred = $.Deferred();
				var deferredCollection = [];

				$parent.filter('[' + attribute + ']').add($parent.find('[' + attribute + ']')).each(function (index, element) {
					var $element = $(element);
					var modules = $element.attr(attribute);

					$element.removeAttr(attribute);

					$.each(modules.replace(/(\r\n|\n|\r|\s\s+)/gm, ' ').trim().split(' '), function (index, name) {
						if (name === '') {
							return true;
						}

						var deferred = $.Deferred();
						deferredCollection.push(deferred);

						jse.core.module_loader.load($element, name, _this).done(function (module) {
							return module.init(deferred);
						}).fail(function (error) {
							deferred.reject();
							// Log the error in the console but do not stop the engine execution.
							jse.core.debug.error('Could not load module: ' + name, error);
						});
					});
				});

				// Always resolve the namespace, even if there are module errors.
				$.when.apply(undefined, deferredCollection).always(function () {
					return namespaceDeferred.resolve();
				});

				return deferredCollection.length ? namespaceDeferred.promise() : namespaceDeferred.resolve();
			}
		}]);

		return Collection;
	}();

	jse.constructors.Collection = Collection;
})();

},{}],2:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* --------------------------------------------------------------
 data_binding.js 2016-05-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

(function () {

	'use strict';

	/**
  * Data Binding Class 
  * 
  * Handles two-way data binding with UI elements. 
  * 
  * @class JSE/Constructors/DataBinding
  */

	var DataBinding = function () {
		/**
   * Class Constructor 
   * 
   * @param {String} name The name of the binding. 
   * @param {Object} $element Target element to be bond. 
   */
		function DataBinding(name, $element) {
			_classCallCheck(this, DataBinding);

			this.name = name;
			this.$element = $element;
			this.value = null;
			this.isMutable = $element.is('input, textarea, select');
			this.init();
		}

		/**
   * Initialize the binding.
   */


		_createClass(DataBinding, [{
			key: 'init',
			value: function init() {
				var _this = this;

				this.$element.on('change', function () {
					_this.get();
				});
			}

			/**
    * Get binding value. 
    * 
    * @returns {*}
    */

		}, {
			key: 'get',
			value: function get() {
				this.value = this.isMutable ? this.$element.val() : this.$element.html();

				if (this.$element.is(':checkbox') || this.$element.is(':radio')) {
					this.value = this.$element.prop('checked');
				}

				return this.value;
			}

			/**
    * Set binding value. 
    * 
    * @param {String} value
    */

		}, {
			key: 'set',
			value: function set(value) {
				this.value = value;

				if (this.isMutable) {
					this.$element.val(value);
				} else {
					this.$element.html(value);
				}
			}
		}]);

		return DataBinding;
	}();

	jse.constructors.DataBinding = DataBinding;
})();

},{}],3:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* --------------------------------------------------------------
 module.js 2016-05-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 ----------------------------------------------------------------
 */

(function () {

	'use strict';

	/**
  * Class Module
  *
  * This class is used for representing a module instance within the JSE ecosystem.
  *
  * @class JSE/Constructors/Module
  */

	var Module = function () {
		/**
   * Class Constructor
   *
   * @param {Object} $element Module element selector object.
   * @param {String} name The module name (might contain the path)
   * @param {Object} collection The collection instance of the module.
   */
		function Module($element, name, collection) {
			_classCallCheck(this, Module);

			this.$element = $element;
			this.name = name;
			this.collection = collection;
		}

		/**
   * Initialize the module execution.
   *
   * This function will execute the "init" method of each module.
   *
   * @param {Object} collectionDeferred Deferred object that gets processed after the module
   * initialization is finished.
   */


		_createClass(Module, [{
			key: 'init',
			value: function init(collectionDeferred) {
				var _this = this;

				// Store module instance alias.
				var cached = this.collection.cache.modules[this.name];
				var timeout = null;

				try {
					if (!cached) {
						throw new Error('Module "' + this.name + '" could not be found in the collection cache.');
					}

					var data = this._getModuleData();
					var instance = cached.code.call(this.$element, data);

					// Provide a done function that needs to be called from the module, in order to inform 
					// that the module "init" function was completed successfully.
					var done = function done() {
						_this.$element.trigger('jse:module:initialized', [{ module: _this.name }]);
						jse.core.debug.info('Module "' + _this.name + '" initialized successfully.');
						collectionDeferred.resolve();
						clearTimeout(timeout);
					};

					// Load the module data before the module is loaded.
					this._loadModuleData(instance).done(function () {
						// Reject the collectionDeferred if the module isn't initialized after 10 seconds.
						timeout = setTimeout(function () {
							jse.core.debug.warn('Module was not initialized after 10 seconds! -- ' + _this.name);
							collectionDeferred.reject();
						}, 10000);

						instance.init(done);
					}).fail(function (error) {
						collectionDeferred.reject();
						jse.core.debug.error('Could not load module\'s meta data.', error);
					});
				} catch (exception) {
					collectionDeferred.reject();
					jse.core.debug.error('Cannot initialize module "' + this.name + '".', exception);
					$(window).trigger('error', [exception]); // Inform the engine about the exception.
				}

				return collectionDeferred.promise();
			}

			/**
    * Parse the module data attributes.
    *
    * @return {Object} Returns an object that contains the data of the module.
    *
    * @private
    */

		}, {
			key: '_getModuleData',
			value: function _getModuleData() {
				var _this2 = this;

				var data = {};

				$.each(this.$element.data(), function (name, value) {
					if (name.indexOf(_this2.name) === 0 || name.indexOf(_this2.name.toLowerCase()) === 0) {
						var key = name.substr(_this2.name.length);
						key = key.substr(0, 1).toLowerCase() + key.substr(1);
						data[key] = value;
						// Remove data attribute from element (sanitise camel case first).
						var sanitisedKey = key.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
						_this2.$element.removeAttr('data-' + _this2.name + '-' + sanitisedKey);
					}
				});

				return data;
			}

			/**
    * Modules return objects which might contain requirements.
    *
    * @param {Object} instance Module instance object.
    *
    * @return {Object} Returns a promise object that will be resolved when the data are fetched.
    *
    * @private
    */

		}, {
			key: '_loadModuleData',
			value: function _loadModuleData(instance) {
				var deferred = $.Deferred();
				var deferredCollection = [];

				try {
					if (instance.model) {
						$.each(instance.model, function (index, url) {
							var modelDeferred = $.Deferred();
							deferredCollection.push(modelDeferred);
							$.getJSON(url).done(function (response) {
								instance.model[index] = response;
								modelDeferred.resolve(response);
							}).fail(function (error) {
								modelDeferred.reject(error);
							});
						});
					}

					if (instance.view) {
						$.each(instance.view, function (index, url) {
							var viewDeferred = $.Deferred();
							deferredCollection.push(viewDeferred);
							$.get(url).done(function (response) {
								instance.view[index] = response;
								viewDeferred.resolve(response);
							}).fail(function (error) {
								viewDeferred.reject(error);
							});
						});
					}

					if (instance.bindings) {
						for (var name in instance.bindings) {
							var $element = instance.bindings[name];
							instance.bindings[name] = new jse.constructors.DataBinding(name, $element);
						}
					}

					$.when.apply(undefined, deferredCollection).done(deferred.resolve).fail(function (error) {
						deferred.reject(new Error('Cannot load data for module "' + instance.name + '".', error));
					});
				} catch (exception) {
					deferred.reject(exception);
					jse.core.debug.error('Cannot preload module data for "' + this.name + '".', exception);
					$(window).trigger('error', [exception]); // Inform the engine about the exception.
				}

				return deferred.promise();
			}
		}]);

		return Module;
	}();

	jse.constructors.Module = Module;
})();

},{}],4:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* --------------------------------------------------------------
 namespace.js 2016-05-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

(function () {

	'use strict';

	/**
  * Class Namespace
  *
  * This class is used to handle multiple collections of modules. Every namespace has its own source URL 
  * for loading the data. That means that JSE can load modules from multiple places at the same time. 
  *
  * @class JSE/Constructors/Namespace
  */

	var Namespace = function () {
		/**
   * Class Constructor
   *
   * @param {String} name The namespace name must be unique within the app.
   * @param {String} source Complete URL to the namespace modules directory (without trailing slash).
   * @param {Array} collections Contains collection instances to be included in the namespace.
   */
		function Namespace(name, source, collections) {
			_classCallCheck(this, Namespace);

			this.name = name;
			this.source = source;
			this.collections = collections; // contains the default instances   		
		}

		/**
   * Initialize the namespace collections.
   *
   * This method will create new collection instances based in the original ones.
   *
   * @return {jQuery.Promise} Returns a promise that will be resolved once every namespace collection
   * is resolved.
   */


		_createClass(Namespace, [{
			key: 'init',
			value: function init() {
				var deferredCollection = [];

				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = this.collections[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var collection = _step.value;

						this[collection.name] = new jse.constructors.Collection(collection.name, collection.attribute, this);
						var deferred = this[collection.name].init();
						deferredCollection.push(deferred);
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				return deferredCollection.length ? $.when.apply(undefined, deferredCollection).promise() : $.Deferred().resolve();
			}
		}]);

		return Namespace;
	}();

	jse.constructors.Namespace = Namespace;
})();

},{}],5:[function(require,module,exports){
'use strict';

/* --------------------------------------------------------------
 about.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * JSE Information Module
 * 
 * Execute the `jse.about()` command and you will get a new log entry in the
 * console with info about the engine. The "about" method is only available in
 * the "development" environment of the engine.
 *
 * @module JSE/Core/about
 */
document.addEventListener('DOMContentLoaded', function () {

	'use strict';

	if (jse.core.config.get('environment') === 'production') {
		return;
	}

	jse.about = function () {
		var info = '\n\t\t\tJS ENGINE v' + jse.core.config.get('version') + ' \xA9 GAMBIO GMBH\n\t\t\t----------------------------------------------------------------\n\t\t\tThe JS Engine enables developers to load automatically small pieces of javascript code by\n\t\t\tplacing specific data attributes to the HTML markup of a page. It was built with modularity\n\t\t\tin mind so that modules can be reused into multiple places without extra effort. The engine\n\t\t\tcontains namespaces which contain collections of modules, each one of whom serve a different\n\t\t\tgeneric purpose.\n\t\t\tVisit http://developers.gambio.de for complete reference of the JS Engine.\n\t\t\t\n\t\t\tFALLBACK INFORMATION\n\t\t\t----------------------------------------------------------------\n\t\t\tSince the engine code becomes bigger there are sections that need to be refactored in order\n\t\t\tto become more flexible. In most cases a warning log will be displayed at the browser\'s console\n\t\t\twhenever there is a use of a deprecated function. Below there is a quick list of fallback support\n\t\t\tthat will be removed in the future versions of the engine.\n\t\t\t\n\t\t\t1. The main engine object was renamed from "gx" to "jse" which stands for the JavaScript Engine.\n\t\t\t2. The "gx.lib" object is removed after a long deprecation period. You should update the modules \n\t\t\t   that contained calls to the functions of this object.\n\t\t\t3. The gx.<collection-name>.register function is deprecated by v1.2, use the \n\t\t\t   <namespace>.<collection>.module() instead.\n\t\t';

		jse.core.debug.info(info);
	};
});

},{}],6:[function(require,module,exports){
'use strict';

/* --------------------------------------------------------------
 config.js 2017-03-26
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.core.config = jse.core.config || {};

/**
 * JSE Configuration Module
 *
 * Once the config object is initialized you cannot change its values. This is done in order to
 * prevent unpleasant situations where one code section changes a core config setting that affects
 * another code section in a way that is hard to discover.
 *
 * ```javascript
 * const appUrl = jse.core.config.get('appUrl');
 * ```
 *
 * @module JSE/Core/config
 */
(function (exports) {

	'use strict';

	// ------------------------------------------------------------------------
	// CONFIGURATION VALUES
	// ------------------------------------------------------------------------

	var config = {
		/**
   * Engine Version
   *
   * @type {String}
   */
		version: '1.5',

		/**
   * App URL
   *
   * e.g. 'http://app.com'
   *
   * @type {String}
   */
		appUrl: null,

		/**
   * Shop URL
   *
   * e.g. 'http://shop.de'
   *
   * @deprecated Since v1.4, use appUrl instead.
   *
   * @type {String}
   */
		shopUrl: null,

		/**
   * App Version
   *
   * e.g. '2.7.3.0'
   *
   * @type {String}
   */
		appVersion: null,

		/**
   * Shop Version
   *
   * e.g. '2.7.3.0'
   *
   * @deprecated Since 1.4, use appVersion instead.
   *
   * @type {String}
   */
		shopVersion: null,

		/**
   * URL to JSEngine Directory.
   *
   * e.g. 'http://app.com/JSEngine
   *
   * @type {String}
   */
		engineUrl: null,

		/**
   * Engine Environment
   *
   * Defines the functionality of the engine in many sections.
   *
   * Values: 'development', 'production'
   *
   * @type {String}
   */
		environment: 'production',

		/**
   * Translations Object
   *
   * Contains the loaded translations to be used within JSEngine.
   *
   * @see jse.core.lang object
   *
   * @type {Object}
   */
		translations: {},

		/**
   * Module Collections
   *
   * Provide array with { name: '', attribute: ''} objects that define the collections to be used within
   * the application.
   *
   * @type {Array}
   */
		collections: [],

		/**
   * Current Language Code
   *
   * @type {String}
   */
		languageCode: 'de',

		/**
   * Set the debug level to one of the following: 'DEBUG', 'INFO', 'LOG', 'WARN', 'ERROR',
   * 'ALERT', 'SILENT'.
   *
   * @type {String}
   */
		debug: 'SILENT',

		/**
   * Use cache busting technique when loading modules.
   *
   * @deprecated Since v1.4
   * 
   * @see jse.core.module_loader object
   *
   * @type {Boolean}
   */
		cacheBust: true,

		/**
   * Whether the client has a mobile interface.
   *
   * @type {Boolean}
   */
		mobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),

		/**
   * Whether the client supports touch events.
   *
   * @type {Boolean}
   */
		touch: 'ontouchstart' in window || window.ontouchstart || window.onmsgesturechange ? true : false,

		/**
   * Specify the path for the file manager.
   *
   * @deprecated Since v1.4
   * 
   * @type {String}
   */
		filemanager: 'includes/ckeditor/filemanager/index.html',

		/**
   * Page token to include in every AJAX request.
   *
   * The page token is used to avoid CSRF attacks. It must be provided by the backend and it will
   * be validated there.
   *
   * @type {String}
   */
		pageToken: '',

		/**
   * Cache Token String 
   * 
   * This configuration value will be used in production environment for cache busting. It must 
   * be provided with the window.JSEngineConfiguration object.
   * 
   * @type {String}
   */
		cacheToken: '',

		/**
   * Defines whether the history object is available.
   *
   * @type {Boolean}
   */
		history: history && history.replaceState && history.pushState
	};

	/**
  * Blacklist config values in production environment.
  * 
  * @type {String[]}
  */
	var blacklist = ['version', 'appVersion', 'shopVersion'];

	// ------------------------------------------------------------------------
	// PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
  * Get a configuration value.
  *
  * @param {String} name The configuration value name to be retrieved.
  *
  * @return {*} Returns the config value.
  */
	exports.get = function (name) {
		if (config.environment === 'production' && blacklist.includes(name)) {
			return null;
		}

		return config[name];
	};

	/**
  * Initialize the JS Engine config object.
  *
  * This method will parse the global "JSEngineConfiguration" object and then remove
  * it from the global scope so that it becomes the only config source for javascript.
  *
  * Notice: The only required JSEngineConfiguration values are the "environment" and the "appUrl".
  *
  * @param {Object} jsEngineConfiguration Must contain information that define core operations
  * of the engine. Check the "libs/initialize" entry of the engine documentation.
  */
	exports.init = function (jsEngineConfiguration) {
		config.environment = jsEngineConfiguration.environment;
		config.appUrl = jsEngineConfiguration.appUrl.replace(/\/+$/, ''); // Remove trailing slash from appUrl.

		if (config.environment === 'development') {
			config.cacheBust = false;
			config.minified = false;
			config.debug = 'DEBUG';
		}

		if (jsEngineConfiguration.engineUrl !== undefined) {
			config.engineUrl = jsEngineConfiguration.engineUrl.replace(/\/+$/, '');
		} else {
			config.engineUrl = config.appUrl + '/JSEngine/build';
		}

		if (jsEngineConfiguration.translations !== undefined) {
			config.translations = jsEngineConfiguration.translations;

			for (var sectionName in config.translations) {
				jse.core.lang.addSection(sectionName, config.translations[sectionName]);
			}
		}

		if (jsEngineConfiguration.collections !== undefined) {
			config.collections = jsEngineConfiguration.collections;
		} else {
			config.collections = [{ name: 'controllers', attribute: 'controller' }, { name: 'extensions', attribute: 'extension' }, { name: 'widgets', attribute: 'widget' }];
		}

		if (jsEngineConfiguration.appVersion !== undefined) {
			config.appVersion = jsEngineConfiguration.appVersion;
		}

		if (jsEngineConfiguration.shopUrl !== undefined) {
			jse.core.debug.warn('JS Engine: "shopUrl" is deprecated and will be removed in JS Engine v1.5, please ' + 'use the "appUrl" instead.');
			config.shopUrl = jsEngineConfiguration.shopUrl.replace(/\/+$/, '');
			config.appUrl = config.appUrl || config.shopUrl; // Make sure the "appUrl" value is not empty.
		}

		if (jsEngineConfiguration.shopVersion !== undefined) {
			jse.core.debug.warn('JS Engine: "shopVersion" is deprecated and will be removed in JS Engine v1.5, please ' + 'use the "appVersion" instead.');
			config.shopVersion = jsEngineConfiguration.shopVersion;
		}

		if (jsEngineConfiguration.prefix !== undefined) {
			config.prefix = jsEngineConfiguration.prefix;
		}

		if (jsEngineConfiguration.languageCode !== undefined) {
			config.languageCode = jsEngineConfiguration.languageCode;
		}

		if (document.getElementById('init-js') !== null && document.getElementById('init-js').hasAttribute('data-page-token')) {
			jsEngineConfiguration.pageToken = document.getElementById('init-js').getAttribute('data-page-token');
		}

		if (jsEngineConfiguration.pageToken !== undefined) {
			config.pageToken = jsEngineConfiguration.pageToken;
		}

		if (jsEngineConfiguration.cacheToken !== undefined) {
			config.cacheToken = jsEngineConfiguration.cacheToken;
		}

		// Add the "touchEvents" entry so that modules can bind various touch events depending the browser.
		var generalTouchEvents = {
			start: 'touchstart',
			end: 'trouchend',
			move: 'touchmove'
		};

		var microsoftTouchEvents = {
			start: 'pointerdown',
			end: 'pointerup',
			move: 'pointermove'
		};

		config.touchEvents = window.onmsgesturechange ? microsoftTouchEvents : generalTouchEvents;

		// Set initial registry values. 
		for (var entry in jsEngineConfiguration.registry) {
			jse.core.registry.set(entry, jsEngineConfiguration.registry[entry]);
		}

		// Initialize the module loader object.
		jse.core.module_loader.init();

		// Destroy global EngineConfiguration object.
		delete window.JSEngineConfiguration;
	};
})(jse.core.config);

},{}],7:[function(require,module,exports){
'use strict';

/* --------------------------------------------------------------
 debug.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.core.debug = jse.core.debug || {};

/**
 * JSE Debug Module
 *
 * This object provides an wrapper to the console.log function and enables easy use
 * of the different log types like "info", "warning", "error" etc.
 *
 * @module JSE/Core/debug
 */
(function (exports) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * @type {String}
  */

	var TYPE_DEBUG = 'DEBUG';

	/**
  * @type {String}
  */
	var TYPE_INFO = 'INFO';

	/**
  * @type {String}
  */
	var TYPE_LOG = 'LOG';

	/**
  * @type {String}
  */
	var TYPE_WARN = 'WARN';

	/**
  * @type {String}
  */
	var TYPE_ERROR = 'ERROR';

	/**
  * @type {String}
  */
	var TYPE_ALERT = 'ALERT';

	/**
  * @type {String}
  */
	var TYPE_MOBILE = 'MOBILE';

	/**
  * @type {String}
  */
	var TYPE_SILENT = 'SILENT';

	/**
  * All possible debug levels in the order of importance.
  *
  * @type {String[]}
  */
	var levels = [TYPE_DEBUG, TYPE_INFO, TYPE_LOG, TYPE_WARN, TYPE_ERROR, TYPE_ALERT, TYPE_MOBILE, TYPE_SILENT];

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Set Favicon to Error State.
  *
  * This method will only work if <canvas> is supported from the browser.
  *
  * @private
  */
	function _setFaviconToErrorState() {
		var canvas = document.createElement('canvas');
		var favicon = document.querySelector('[rel="shortcut icon"]');

		if (canvas.getContext && !favicon.className.includes('error-state')) {
			var img = document.createElement('img');
			canvas.height = canvas.width = 16;
			var ctx = canvas.getContext('2d');
			img.onload = function () {
				// Continue once the image has been loaded. 
				ctx.drawImage(this, 0, 0);
				ctx.globalAlpha = 0.65;
				ctx.fillStyle = '#FF0000';
				ctx.rect(0, 0, 16, 16);
				ctx.fill();
				favicon.href = canvas.toDataURL('image/png');
				favicon.className += 'error-state';
			};
			img.src = favicon.href;
		}
	}

	/**
  * Error handler that fetches all exceptions thrown by the javascript.
  *
  * @private
  */
	function _globalErrorHandler() {
		if (jse.core.config.get('environment') !== 'production') {
			// Log the error in the browser's console. 
			if (jse.core.debug !== undefined) {
				jse.core.debug.error('JS Engine Error Handler', arguments);
			} else {
				console.log('JS Engine Error Handler', arguments);
			}

			// Update the page title with an error count.
			var regex = /.\ \[(.+)\]\ /;
			var title = window.document.title;
			var errorCount = 1;

			// Gets the current error count and recreates the default title of the page.
			if (title.match(regex) !== null) {
				errorCount = parseInt(title.match(/\d+/)[0], 10) + 1;
				title = title.replace(regex, '');
			}

			// Re-creates the error flag at the title with the new error count.
			title = '✖ [' + errorCount + '] ' + title;
			window.document.title = title;

			// Set Favicon to Error State.
			_setFaviconToErrorState();
		}

		return true;
	}

	/**
  * Executes the correct console/alert statement.
  *
  * @param {Object} caller (optional) Contains the caller information to be displayed.
  * @param {Object} data (optional) Contains any additional data to be included in the debug output.
  *
  * @private
  */
	function _execute(caller, data) {
		var currentLogIndex = levels.indexOf(caller);
		var allowedLogIndex = levels.indexOf(jse.core.config.get('debug'));
		var consoleMethod = null;

		if (currentLogIndex >= allowedLogIndex) {
			consoleMethod = caller.toLowerCase();

			switch (consoleMethod) {
				case 'alert':
					alert(JSON.stringify(data));
					break;

				case 'mobile':
					var $mobileDebugModal = $('.mobile-debug-modal');

					if (!$mobileDebugModal.length) {
						$('<div />').addClass('mobile-debug-modal').css({
							position: 'fixed',
							top: 0,
							left: 0,
							maxHeight: '50%',
							minWidth: '200px',
							maxWidth: '300px',
							backgroundColor: 'crimson',
							zIndex: 100000,
							overflow: 'scroll'
						}).appendTo($('body'));
					}

					$mobileDebugModal.append('<p>' + JSON.stringify(data) + '</p>');
					break;

				default:
					if (console === undefined) {
						return; // There is no console support so do not proceed.
					}

					if (typeof console[consoleMethod].apply === 'function' || typeof console.log.apply === 'function') {
						if (console[consoleMethod] !== undefined) {
							console[consoleMethod].apply(console, data);
						} else {
							console.log.apply(console, data);
						}
					} else {
						console.log(data);
					}
			}
		}
	}

	/**
  * Bind Global Error Handler
  */
	exports.bindGlobalErrorHandler = function () {
		window.onerror = _globalErrorHandler;
	};

	/**
  * Replaces console.debug
  *
  * @params {*} arguments Any data that should be shown in the console statement.
  */
	exports.debug = function () {
		_execute(TYPE_DEBUG, arguments);
	};

	/**
  * Replaces console.info
  *
  * @params {*} arguments Any data that should be shown in the console statement.
  */
	exports.info = function () {
		_execute(TYPE_INFO, arguments);
	};

	/**
  * Replaces console.log
  *
  * @params {*} arguments Any data that should be shown in the console statement.
  */
	exports.log = function () {
		_execute(TYPE_LOG, arguments);
	};

	/**
  * Replaces console.warn
  *
  * @params {*} arguments Any data that should be shown in the console statement.
  */
	exports.warn = function () {
		_execute(TYPE_WARN, arguments);
	};

	/**
  * Replaces console.error
  *
  * @param {*} arguments Any data that should be shown in the console statement.
  */
	exports.error = function () {
		_execute(TYPE_ERROR, arguments);
	};

	/**
  * Replaces alert
  *
  * @param {*} arguments Any data that should be shown in the console statement.
  */
	exports.alert = function () {
		_execute(TYPE_ALERT, arguments);
	};

	/**
  * Debug info for mobile devices.
  *
  * @param {*} arguments Any data that should be shown in the console statement.
  */
	exports.mobile = function () {
		_execute(TYPE_MOBILE, arguments);
	};
})(jse.core.debug);

},{}],8:[function(require,module,exports){
'use strict';

/* --------------------------------------------------------------
 engine.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.core.engine = jse.core.engine || {};

/**
 * JSE Core Module
 *
 * This object will initialize the page namespaces and collections.
 *
 * @module JSE/Core/engine
 */
(function (exports) {

	'use strict';

	// ------------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Initialize the page namespaces.
  *
  * This method will search the page HTML for available namespaces.
  *
  * @param {Array} collections Contains the module collection instances to be included in the namespaces.
  *
  * @return {Array} Returns an array with the page namespace names.
  *
  * @private
  */

	function _initNamespaces(collections) {
		var pageNamespaceNames = [];

		// Use the custom pseudo selector defined at extend.js in order to fetch the available namespaces.
		var nodes = Array.from(document.getElementsByTagName('*')),
		    regex = /data-(.*)-namespace/;

		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = nodes[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var node = _step.value;
				var _iteratorNormalCompletion3 = true;
				var _didIteratorError3 = false;
				var _iteratorError3 = undefined;

				try {
					for (var _iterator3 = Array.from(node.attributes)[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var attribute = _step3.value;

						if (attribute.name.search(regex) !== -1) {
							// Parse the namespace name and source URL.
							var name = attribute.name.replace(regex, '$1'),
							    source = attribute.value;

							// Check if the namespace is already defined.
							if (pageNamespaceNames.indexOf(name) > -1) {
								if (window[name].source !== source) {
									jse.core.debug.error('Element with the duplicate namespace name: ' + node);
									throw new Error('The namespace "' + name + '" is already defined. Please select another ' + 'name for your namespace.');
								}
								continue; // The namespace is already defined, continue loop.
							}

							if (source === '') {
								throw new SyntaxError('Namespace source is empty: ' + name);
							}

							// Create a new namespaces instance in the global scope (the global scope is used for 
							// fallback support of old module definitions).
							if (name === 'jse') {
								// Modify the engine object with Namespace attributes.
								_convertEngineToNamespace(source, collections);
							} else {
								window[name] = new jse.constructors.Namespace(name, source, collections);
							}

							pageNamespaceNames.push(name);
							node.removeAttribute(attribute.name);
						}
					}
				} catch (err) {
					_didIteratorError3 = true;
					_iteratorError3 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError3) {
							throw _iteratorError3;
						}
					}
				}
			}

			// Throw an error if no namespaces were found.
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		if (pageNamespaceNames.length === 0) {
			throw new Error('No module namespaces were found, without namespaces it is not possible to ' + 'load any modules.');
		}

		// Initialize the namespace instances.
		var deferredCollection = [];

		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			var _loop = function _loop() {
				var name = _step2.value;

				var deferred = $.Deferred();

				deferredCollection.push(deferred);

				window[name].init().done(deferred.resolve).fail(deferred.reject).always(function () {
					return jse.core.debug.info('Namespace promises were resolved: ', name);
				});
			};

			for (var _iterator2 = pageNamespaceNames[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				_loop();
			}

			// Trigger an event after the engine has initialized all new modules.
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		$.when.apply(undefined, deferredCollection).always(function () {
			var event = document.createEvent('Event');
			event.initEvent('JSENGINE_INIT_FINISHED', true, true);
			document.querySelector('body').dispatchEvent(event);
			jse.core.registry.set('jseEndTime', new Date().getTime());
			jse.core.debug.info('JS Engine Loading Time: ', jse.core.registry.get('jseEndTime') - jse.core.registry.get('jseStartTime'), 'ms');
			if (window.Cypress) {
				window.jseReady = true;
			}
		});

		return pageNamespaceNames;
	}

	/**
  * Convert the "jse" object to a Namespace compatible object.
  *
  * In order to support the "jse" namespace name for the core modules placed in the "JSEngine"
  * directory, we will need to modify the already existing "jse" object so that it can operate
  * as a namespace without losing its initial attributes.
  *
  * @param {String} source Namespace source path for the module files.
  * @param {Array} collections Contain instances to the prototype collection instances.
  *
  * @private
  */
	function _convertEngineToNamespace(source, collections) {
		var tmpNamespace = new jse.constructors.Namespace('jse', source, collections);
		jse.name = tmpNamespace.name;
		jse.source = tmpNamespace.source;
		jse.collections = tmpNamespace.collections;
		jse.init = jse.constructors.Namespace.prototype.init;
	}

	// ------------------------------------------------------------------------
	// PUBLIC FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Initialize the engine.
  *
  * @param {Array} collections Contains the supported module collection data.
  */
	exports.init = function (collections) {
		// Global error handler that executes if an uncaught JS error occurs on page.
		jse.core.debug.bindGlobalErrorHandler();

		// Initialize the page namespaces.
		var pageNamespaceNames = _initNamespaces(collections);

		// Log the page namespaces (for debugging only).
		jse.core.debug.info('Page Namespaces: ' + pageNamespaceNames.join());

		// Update the engine registry.
		jse.core.registry.set('namespaces', pageNamespaceNames);
	};
})(jse.core.engine);

},{}],9:[function(require,module,exports){
'use strict';

/* --------------------------------------------------------------
 extensions.js 2017-03-03
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * JSE Extensions
 *
 * Extend the default behaviour of engine components or external plugins before they are loaded.
 *
 * @module JSE/Core/extend
 */
(function () {

	'use strict';

	// ------------------------------------------------------------------------
	// PARSE MODULE DATA JQUERY EXTENSION
	// ------------------------------------------------------------------------

	$.fn.extend({
		parseModuleData: function parseModuleData(moduleName) {
			if (!moduleName || moduleName === '') {
				throw new Error('Module name was not provided as an argument.');
			}

			var initialData = $(this).data();
			var filteredData = {};

			// Searches for module relevant data inside the main-data-object. Data for other widgets will not get 
			// passed to this widget.
			$.each(initialData, function (key, value) {
				if (key.indexOf(moduleName) === 0 || key.indexOf(moduleName.toLowerCase()) === 0) {
					var newKey = key.substr(moduleName.length);
					newKey = newKey.substr(0, 1).toLowerCase() + newKey.substr(1);
					filteredData[newKey] = value;
				}
			});

			return filteredData;
		}
	});

	// ------------------------------------------------------------------------
	// DATEPICKER REGIONAL INFO
	// ------------------------------------------------------------------------

	if ($.datepicker !== undefined) {
		$.datepicker.regional.de = {
			dateFormat: 'dd.mm.yy',
			firstDay: 1,
			isRTL: false
		};
		$.datepicker.setDefaults($.datepicker.regional.de);
	}
})();

},{}],10:[function(require,module,exports){
/* --------------------------------------------------------------
 initialize.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

'use strict';

/**
 * JSE Initialization Module
 *
 * The document-ready event of the page will trigger the JavaScript Engine initialization. The
 * engine requires a global configuration object "window.JSEngineConfiguration" to be pre-defined
 * in order to retrieve the basic configuration info. After a successful initialization this object
 * is removed from the window object.
 *
 * ### Configuration Sample
 *
 * ```js
 * window.JSEngineConfiguration = {
 *   environment: 'production',
 *   appUrl: 'http://app.com',
 *   collections: [
 *     {name: 'controllers', attribute: 'controller'}
 *   ],  
 *   translations: {
 *     'sectionName': { 'translationKey': 'translationValue' },
 *     'anotherSection': { ... }
 *   },
 *   languageCode: 'en',
 *   pageToken: '9asd7f9879sd8f79s98s7d98f'
 * };
 * ```
 *
 * @module JSE/Core/initialize
 */

// Initialize base engine object. Every other part of the engine will refer to this
// central object for the core operations.

window.jse = {
  core: {},
  libs: {},
  constructors: {}
};

// Initialize the engine on window load. 
document.addEventListener('DOMContentLoaded', function () {
  try {
    // Check if global JSEngineConfiguration object is defined.
    if (window.JSEngineConfiguration === undefined) {
      throw new Error('The "window.JSEngineConfiguration" object is not defined in the global scope. ' + 'This object is required by the engine upon its initialization.');
    }

    // Parse JSEngineConfiguration object.
    jse.core.config.init(window.JSEngineConfiguration);

    // Store the JSE start time in registry (profiling). 
    jse.core.registry.set('jseStartTime', Date.now());

    // Initialize the module collections.
    jse.core.engine.init(jse.core.config.get('collections'));
  } catch (exception) {
    jse.core.debug.error('Unexpected error during JS Engine initialization!', exception);
    // Inform the engine about the exception.
    var event = document.createEvent('CustomEvent');
    event.initCustomEvent('error', true, true, exception);
    window.dispatchEvent(event);
  }
});

},{}],11:[function(require,module,exports){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* --------------------------------------------------------------
 lang.js 2016-08-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.core.lang = jse.core.lang || {};

/**
 * JSE Localization Library
 *
 * The global Lang object contains language information that can be easily used in your
 * JavaScript code. The object contains constance translations and dynamic sections that
 * can be loaded and used in different page.
 *
 * #### Important
 * The engine will automatically load translation sections that are present in the
 * `window.JSEngineConfiguration.translations` property upon initialization. For more
 * information look at the "core/initialize" page of documentation reference.
 *
 * ```javascript
 * jse.core.lang.addSection('sectionName', { translationKey: 'translationValue' }); // Add translation section.
 * jse.core.translate('translationKey', 'sectionName'); // Get the translated string.
 * jse.core.getSections(); // returns array with sections e.g. ['admin_buttons', 'general']
 * ```
 *
 * @module JSE/Core/lang
 */
(function (exports) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Contains various translation sections.
  *
  * @type {Object}
  */

	var sections = {};

	// ------------------------------------------------------------------------
	// PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
  * Add a translation section.
  *
  * @param {String} name Name of the section, used later for accessing translation strings.
  * @param {Object} translations Key - value object containing the translations.
  *
  * @throws {Error} If "name" or "translations" arguments are invalid.
  */
	exports.addSection = function (name, translations) {
		if (typeof name !== 'string' || (typeof translations === 'undefined' ? 'undefined' : _typeof(translations)) !== 'object' || translations === null) {
			throw new Error('window.gx.core.lang.addSection: Invalid arguments provided (name: ' + (typeof name === 'undefined' ? 'undefined' : _typeof(name)) + ', ' + ('translations: ' + (typeof translations === 'undefined' ? 'undefined' : _typeof(translations)) + ').'));
		}
		sections[name] = translations;
	};

	/**
  * Get loaded translation sections.
  *
  * Useful for asserting present translation sections.
  *
  * @return {Array} Returns array with the existing sections.
  */
	exports.getSections = function () {
		var result = [];

		for (var section in sections) {
			result.push(section);
		}

		return result;
	};

	/**
  * Translate string in Javascript code.
  *
  * @param {String} phrase Name of the phrase containing the translation.
  * @param {String} section Section name containing the translation string.
  *
  * @return {String} Returns the translated string.
  *
  * @throws {Error} If provided arguments are invalid.
  * @throws {Error} If required section does not exist or translation could not be found.
  */
	exports.translate = function (phrase, section) {
		// Validate provided arguments.
		if (typeof phrase !== 'string' || typeof section !== 'string') {
			throw new Error('Invalid arguments provided in translate method (phrase: ' + (typeof phrase === 'undefined' ? 'undefined' : _typeof(phrase)) + ', ' + ('section: ' + (typeof section === 'undefined' ? 'undefined' : _typeof(section)) + ').'));
		}

		// Check if translation exists.
		if (sections[section] === undefined || sections[section][phrase] === undefined) {
			jse.core.debug.warn('Could not found requested translation (phrase: ' + phrase + ', section: ' + section + ').');
			return '{' + section + '.' + phrase + '}';
		}

		return sections[section][phrase];
	};
})(jse.core.lang);

},{}],12:[function(require,module,exports){
'use strict';

require('./initialize');

require('../constructors/collection');

require('../constructors/data_binding');

require('../constructors/module');

require('../constructors/namespace');

require('./about');

require('./config');

require('./debug');

require('./engine');

require('./extend');

require('./lang');

require('./require');

require('./module_loader');

require('./polyfills');

require('./registry');

},{"../constructors/collection":1,"../constructors/data_binding":2,"../constructors/module":3,"../constructors/namespace":4,"./about":5,"./config":6,"./debug":7,"./engine":8,"./extend":9,"./initialize":10,"./lang":11,"./module_loader":13,"./polyfills":14,"./registry":15,"./require":16}],13:[function(require,module,exports){
'use strict';

/* --------------------------------------------------------------
 module_loader.js 2016-06-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.core.module_loader = jse.core.module_loader || {};

/**
 * JSE Module Loader
 *
 * This object is an adapter between the engine and RequireJS which is used to load the required files 
 * into the client.
 * 
 * @todo Remove require.js dependency and load the module/lib files manually.
 *
 * @module JSE/Core/module_loader
 */
(function (exports) {

	'use strict';

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Load CSS file.
  * 
  * @param {String} url Absolute URL of the CSS file to be loaded. 
  * 
  * @private
  */

	function _loadCss(url) {
		var link = document.createElement('link');
		link.type = 'text/css';
		link.rel = 'stylesheet';
		link.href = url;
		document.getElementsByTagName('head')[0].appendChild(link);
	}

	// ------------------------------------------------------------------------
	// PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
  * Initialize the module loader.
  *
  * Execute this method after the engine config is initialized. It will configure require.js
  * so that it will be able to find the project files.
  * 
  * The cache busting method will try to create a number based on the current shop version.
  */
	exports.init = function () {
		var cacheBust = '';

		if (jse.core.config.get('environment') === 'production' && jse.core.config.get('cacheToken')) {
			cacheBust = 'bust=' + jse.core.config.get('cacheToken');
		}

		var config = {
			baseUrl: jse.core.config.get('appUrl'),
			urlArgs: cacheBust,
			onError: function onError(error) {
				jse.core.debug.error('RequireJS Error:', error);
			}
		};

		window.require.config(config);
	};

	/**
  * Require JS and CSS files .
  * 
  * Notice: There's no concrete way to determine when CSS dependencies are loaded.
  * 
  * @param {String[]} dependencies Dependency URLs.
  * @param {Function} callback Callback method to be called once the dependencies are loaded. 
  */
	exports.require = function (dependencies, callback) {
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = dependencies[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var dependency = _step.value;

				if (dependency.includes('.css')) {
					_loadCss(dependency);
					var index = dependencies.indexOf(dependency);
					dependencies.splice(index, 1);
				}
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		if (dependencies.length === 0) {
			callback();
		} else {
			window.require(dependencies, callback);
		}
	};

	/**
  * Load a module file with the use of requirejs.
  *
  * @param {Object} $element Selector of the element which has the module definition.
  * @param {String} name Module name to be loaded. Modules have the same names as their files.
  * @param {Object} collection Current collection instance.
  *
  * @return {Object} Returns a promise object to be resolved with the module instance as a parameter.
  */
	exports.load = function ($element, name, collection) {
		var deferred = $.Deferred();

		try {
			if (name === '') {
				deferred.reject(new Error('Module name cannot be empty.'));
			}

			var baseModuleName = name.replace(/.*\/(.*)$/, '$1'); // Name without the parent directories.

			// Try to load the cached instance of the module.
			var cached = collection.cache.modules[baseModuleName];
			if (cached && cached.code === 'function') {
				deferred.resolve(new jse.constructors.Module($element, baseModuleName, collection));
				return true; // continue loop
			}

			// Try to load the module file from the server.
			var fileExtension = jse.core.config.get('debug') !== 'DEBUG' ? '.min.js' : '.js';
			var url = collection.namespace.source + '/' + collection.name + '/' + name + fileExtension;

			window.require([url], function () {
				if (collection.cache.modules[baseModuleName] === undefined) {
					throw new Error('Module "' + name + '" wasn\'t defined correctly. Check the module code for ' + 'further troubleshooting.');
				}

				// Use the slice method for copying the array. 
				var dependencies = collection.cache.modules[baseModuleName].dependencies.slice();

				if (dependencies.length === 0) {
					// no dependencies
					deferred.resolve(new jse.constructors.Module($element, baseModuleName, collection));
					return true; // continue loop
				}

				// Load the dependencies first.
				for (var index in dependencies) {
					var dependency = dependencies[index];

					if (dependency.indexOf('.css') !== -1) {
						_loadCss(dependency);
						dependencies.splice(index, 1);
						continue;
					}

					// Then convert the relative path to JSEngine/libs directory.
					if (dependency.indexOf('http') === -1) {
						dependencies[index] = jse.core.config.get('engineUrl') + '/libs/' + dependency + fileExtension;
					} else if (dependency.substr(-3) !== '.js') {
						// Then add the dynamic file extension to the URL.
						dependencies[index] += fileExtension;
					}
				}

				window.require(dependencies, function () {
					deferred.resolve(new jse.constructors.Module($element, baseModuleName, collection));
				});
			});
		} catch (exception) {
			deferred.reject(exception);
		}

		return deferred.promise();
	};
})(jse.core.module_loader);

},{}],14:[function(require,module,exports){
'use strict';

/* --------------------------------------------------------------
 polyfills.js 2016-05-17
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * JSE Polyfills 
 * 
 * Required polyfills for compatibility among old browsers.
 *
 * @module JSE/Core/polyfills
 */
(function () {

	'use strict';

	// Internet Explorer does not support the origin property of the window.location object.
	// {@link http://tosbourn.com/a-fix-for-window-location-origin-in-internet-explorer}

	if (!window.location.origin) {
		window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
	}

	// Date.now method polyfill
	// {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/now}
	if (!Date.now) {
		Date.now = function now() {
			return new Date().getTime();
		};
	}
})();

},{}],15:[function(require,module,exports){
'use strict';

/* --------------------------------------------------------------
 registry.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.core.registry = jse.core.registry || {};

/**
 * JS Engine Registry
 *
 * This object contains string data that other sections of the engine need in order to operate correctly.
 *
 * @module JSE/Core/registry
 */
(function (exports) {

	'use strict';

	/**
  * Contains the registry values.
  * 
  * @type {Object[]}
  */

	var registry = [];

	/**
  * Set a value in the registry.
  *
  * @param {String} name Contains the name of the entry to be added.
  * @param {*} value The value to be written in the registry.
  */
	exports.set = function (name, value) {
		// If a registry entry with the same name exists already the following console warning will
		// inform developers that they are overwriting an existing value, something useful when debugging.
		if (registry[name] !== undefined) {
			jse.core.debug.warn('The registry value with the name "' + name + '" will be overwritten.');
		}

		registry[name] = value;
	};

	/**
  * Get a value from the registry.
  *
  * @param {String} name The name of the entry value to be returned.
  *
  * @returns {*} Returns the value that matches the name.
  */
	exports.get = function (name) {
		return registry[name];
	};

	/**
  * Check the current content of the registry object.
  *
  * This method is only available when the engine environment is turned into development.
  */
	exports.debug = function () {
		if (jse.core.config.get('environment') === 'development') {
			jse.core.debug.log('Registry Object:', registry);
		} else {
			throw new Error('This function is not allowed in a production environment.');
		}
	};
})(jse.core.registry);

},{}],16:[function(require,module,exports){
(function (global){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* --------------------------------------------------------------
 require.js 2017-03-28
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Asynchronous Module Loading
 *
 * This module is a fork of RequireJS without the AMD functionality. The global "define" method is removed as
 * it's not necessary by JS Engine. 
 * 
 * {@link https://github.com/requirejs/requirejs}
 *
 * Not using strict: uneven strict support in browsers, #392, and causes problems with requirejs.exec()/transpiler
 * plugins that may not be strict.
 */
(function () {

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------ 

	window.requirejs = undefined;
	window.require = undefined;

	var req = void 0;
	var s = void 0;
	var head = void 0;
	var baseElement = void 0;
	var dataMain = void 0;
	var src = void 0;
	var interactiveScript = void 0;
	var mainScript = void 0;
	var subPath = void 0;
	var version = '2.1.22';
	var jsSuffixRegExp = /\.js$/;
	var currDirRegExp = /^\.\//;
	var op = Object.prototype;
	var ostring = op.toString;
	var hasOwn = op.hasOwnProperty;
	var isBrowser = !!(typeof window !== 'undefined' && typeof navigator !== 'undefined' && window.document);
	var isWebWorker = !isBrowser && typeof importScripts !== 'undefined';
	// PS3 indicates loaded and complete, but need to wait for complet specifically. Sequence is 'loading', 'loaded', 
	// execution then 'complete'. The UA check is unfortunate, but not sure how to feature test w/o causing perf issues.
	var readyRegExp = isBrowser && navigator.platform === 'PLAYSTATION 3' ? /^complete$/ : /^(complete|loaded)$/;
	var defContextName = '_';
	// Oh the tragedy, detecting opera. See the usage of isOpera for reason.
	var isOpera = typeof opera !== 'undefined' && opera.toString() === '[object Opera]';
	var contexts = {};
	var cfg = {};
	var globalDefQueue = [];
	var useInteractive = false;

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------ 

	/**
  * Check whether value is a function. 
  * 
  * @param {*} it Value to be checked.
  * 
  * @return {boolean} Returns the validation result.
  */
	function isFunction(it) {
		return ostring.call(it) === '[object Function]';
	}

	/**
  * Check whether value is an array.
  *
  * @param {*} it Value to be checked.
  *
  * @return {boolean} Returns the validation result.
  */
	function isArray(it) {
		return ostring.call(it) === '[object Array]';
	}

	/**
  * Helper function for iterating over an array. 
  * 
  * If the func returns a true value, it will break out of the loop.
  */
	function each(ary, func) {
		if (ary) {
			for (var i = 0; i < ary.length; i += 1) {
				if (ary[i] && func(ary[i], i, ary)) {
					break;
				}
			}
		}
	}

	/**
  * Helper function for iterating over an array backwards. 
  * 
  * If the func returns a true value, it will break out of the loop.
  */
	function eachReverse(ary, func) {
		if (ary) {
			var i = void 0;
			for (i = ary.length - 1; i > -1; i -= 1) {
				if (ary[i] && func(ary[i], i, ary)) {
					break;
				}
			}
		}
	}

	/**
  * Check whether an object has a specific property. 
  * 
  * @param {Object} obj Object to be checked.
  * @param {String} prop Property name to be checked. 
  * 
  * @return {Boolean} Returns the validation result.
  */
	function hasProp(obj, prop) {
		return hasOwn.call(obj, prop);
	}

	/**
  * Check if an object has a property and if that property contains a truthy value. 
  * 
  * @param {Object} obj Object to be checked.
  * @param {String} prop Property name to be checked.
  *
  * @return {Boolean} Returns the validation result.
  */
	function getOwn(obj, prop) {
		return hasProp(obj, prop) && obj[prop];
	}

	/**
  * Cycles over properties in an object and calls a function for each property value. 
  * 
  * If the function returns a truthy value, then the iteration is stopped.
  */
	function eachProp(obj, func) {
		var prop = void 0;
		for (prop in obj) {
			if (hasProp(obj, prop)) {
				if (func(obj[prop], prop)) {
					break;
				}
			}
		}
	}

	/**
  * Simple function to mix in properties from source into target, but only if target does not already have a 
  * property of the same name.
  */
	function mixin(target, source, force, deepStringMixin) {
		if (source) {
			eachProp(source, function (value, prop) {
				if (force || !hasProp(target, prop)) {
					if (deepStringMixin && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value && !isArray(value) && !isFunction(value) && !(value instanceof RegExp)) {

						if (!target[prop]) {
							target[prop] = {};
						}
						mixin(target[prop], value, force, deepStringMixin);
					} else {
						target[prop] = value;
					}
				}
			});
		}
		return target;
	}

	// Similar to Function.prototype.bind, but the 'this' object is specified first, since it is easier to read/figure 
	// out what 'this' will be.
	function bind(obj, fn) {
		return function () {
			return fn.apply(obj, arguments);
		};
	}

	function scripts() {
		return document.getElementsByTagName('script');
	}

	function defaultOnError(err) {
		throw err;
	}

	// Allow getting a global that is expressed in dot notation, like 'a.b.c'.
	function getGlobal(value) {
		if (!value) {
			return value;
		}
		var g = global;
		each(value.split('.'), function (part) {
			g = g[part];
		});
		return g;
	}

	/**
  * Constructs an error with a pointer to an URL with more information.
  * 
  * @param {String} id The error ID that maps to an ID on a web page.
  * @param {String} msg Human readable error.
  * @param {Error} [err] The original error, if there is one.
  *
  * @return {Error}
  */
	function makeError(id, msg, err, requireModules) {
		var error = new Error(msg + '\nhttp://requirejs.org/docs/errors.html#' + id);

		error.requireType = id;
		error.requireModules = requireModules;

		if (err) {
			error.originalError = err;
		}

		return error;
	}

	if (typeof window.requirejs !== 'undefined') {
		if (isFunction(window.requirejs)) {
			// Do not overwrite an existing requirejs instance.
			return;
		}
		cfg = window.requirejs;
		window.requirejs = undefined;
	}

	// Allow for a require config object
	if (typeof window.require !== 'undefined' && !isFunction(window.require)) {
		// assume it is a config object.
		cfg = window.require;
		window.require = undefined;
	}

	function newContext(contextName) {
		var inCheckLoaded = void 0,
		    Module = void 0,
		    context = void 0,
		    handlers = void 0,
		    checkLoadedTimeoutId = void 0,
		    _config = {
			// Defaults. Do not set a default for map
			// config to speed up normalize(), which
			// will run faster if there is no default.
			waitSeconds: 7,
			baseUrl: './',
			paths: {},
			bundles: {},
			pkgs: {},
			shim: {},
			config: {}
		},
		    registry = {},

		// registry of just enabled modules, to speed
		// cycle breaking code when lots of modules
		// are registered, but not activated.
		enabledRegistry = {},
		    undefEvents = {},
		    defQueue = [],
		    _defined = {},
		    urlFetched = {},
		    bundlesMap = {},
		    requireCounter = 1,
		    unnormalizedCounter = 1;

		/**
   * Trims the . and .. from an array of path segments.
   * 
   * It will keep a leading path segment if a .. will become the first path segment, to help with module name 
   * lookups, which act like paths, but can be remapped. But the end result, all paths that use this function 
   * should look normalized.
   * 
   * NOTE: this method MODIFIES the input array.
   * 
   * @param {Array} ary the array of path segments.
   */
		function trimDots(ary) {
			var i = void 0,
			    part = void 0;
			for (i = 0; i < ary.length; i++) {
				part = ary[i];
				if (part === '.') {
					ary.splice(i, 1);
					i -= 1;
				} else if (part === '..') {
					// If at the start, or previous value is still ..,  keep them so that when converted to a path it 
					// may still work when converted to a path, even though as an ID it is less than ideal. In larger 
					// point releases, may be better to just kick out an error.
					if (i === 0 || i === 1 && ary[2] === '..' || ary[i - 1] === '..') {
						continue;
					} else if (i > 0) {
						ary.splice(i - 1, 2);
						i -= 2;
					}
				}
			}
		}

		/**
   * Given a relative module name, like ./something, normalize it to a real name that can be mapped to a path.
   * 
   * @param {String} name the relative name
   * @param {String} baseName a real name that the name arg is relative to.
   * @param {Boolean} applyMap apply the map config to the value. Should only be done if this normalization is 
   * for a dependency ID.
   * 
   * @return {String} normalized name
   */
		function normalize(name, baseName, applyMap) {
			var pkgMain = void 0,
			    mapValue = void 0,
			    nameParts = void 0,
			    i = void 0,
			    j = void 0,
			    nameSegment = void 0,
			    lastIndex = void 0,
			    foundMap = void 0,
			    foundI = void 0,
			    foundStarMap = void 0,
			    starI = void 0,
			    normalizedBaseParts = void 0,
			    baseParts = baseName && baseName.split('/'),
			    map = _config.map,
			    starMap = map && map['*'];

			// Adjust any relative paths.
			if (name) {
				name = name.split('/');
				lastIndex = name.length - 1;

				// If wanting node ID compatibility, strip .js from end of IDs. Have to do this here, and not in 
				// nameToUrl because node allows either .js or non .js to map to same file.
				if (_config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
					name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
				}

				// Starts with a '.' so need the baseName
				if (name[0].charAt(0) === '.' && baseParts) {
					// Convert baseName to array, and lop off the last part, so that . matches that 'directory' and not 
					// name of the baseName's module. For instance, baseName of 'one/two/three', maps to 
					// 'one/two/three.js', but we want the directory, 'one/two' for this normalization.
					normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
					name = normalizedBaseParts.concat(name);
				}

				trimDots(name);
				name = name.join('/');
			}

			// Apply map config if available.
			if (applyMap && map && (baseParts || starMap)) {
				nameParts = name.split('/');

				outerLoop: for (i = nameParts.length; i > 0; i -= 1) {
					nameSegment = nameParts.slice(0, i).join('/');

					if (baseParts) {
						// Find the longest baseName segment match in the config. So, do joins on the biggest to 
						// smallest lengths of baseParts.
						for (j = baseParts.length; j > 0; j -= 1) {
							mapValue = getOwn(map, baseParts.slice(0, j).join('/'));

							// baseName segment has config, find if it has one for this name.
							if (mapValue) {
								mapValue = getOwn(mapValue, nameSegment);
								if (mapValue) {
									// Match, update name to the new value.
									foundMap = mapValue;
									foundI = i;
									break outerLoop;
								}
							}
						}
					}

					// Check for a star map match, but just hold on to it, if there is a shorter segment match later in 
					// a matching config, then favor over this star map.
					if (!foundStarMap && starMap && getOwn(starMap, nameSegment)) {
						foundStarMap = getOwn(starMap, nameSegment);
						starI = i;
					}
				}

				if (!foundMap && foundStarMap) {
					foundMap = foundStarMap;
					foundI = starI;
				}

				if (foundMap) {
					nameParts.splice(0, foundI, foundMap);
					name = nameParts.join('/');
				}
			}

			// If the name points to a package's name, use the package main instead.
			pkgMain = getOwn(_config.pkgs, name);

			return pkgMain ? pkgMain : name;
		}

		function removeScript(name) {
			if (isBrowser) {
				each(scripts(), function (scriptNode) {
					if (scriptNode.getAttribute('data-requiremodule') === name && scriptNode.getAttribute('data-requirecontext') === context.contextName) {
						scriptNode.parentNode.removeChild(scriptNode);
						return true;
					}
				});
			}
		}

		function hasPathFallback(id) {
			var pathConfig = getOwn(_config.paths, id);
			if (pathConfig && isArray(pathConfig) && pathConfig.length > 1) {
				// Pop off the first array value, since it failed, and retry.
				pathConfig.shift();
				context.require.undef(id);

				// Custom require that does not do map translation, since ID is "absolute", already mapped/resolved.
				context.makeRequire(null, {
					skipMap: true
				})([id]);

				return true;
			}
		}

		// Turns a plugin!resource to [plugin, resource] with the plugin being undefined if the name did not have a 
		// plugin prefix.
		function splitPrefix(name) {
			var prefix = void 0,
			    index = name ? name.indexOf('!') : -1;
			if (index > -1) {
				prefix = name.substring(0, index);
				name = name.substring(index + 1, name.length);
			}
			return [prefix, name];
		}

		/**
   * Creates a module mapping that includes plugin prefix, module name, and path. If parentModuleMap is provided 
   * it will also normalize the name via require.normalize()
   *
   * @param {String} name The module name.
   * @param {String} [parentModuleMap] Parent module map for the module name, used to resolve relative names.
   * @param {Boolean} isNormalized Is the ID already normalized? This is true if this call is done for a define() 
   * module ID.
   * @param {Boolean} applyMap: apply the map config to the ID. Should only be true if this map is for a dependency.
   *
   * @return {Object}
   */
		function makeModuleMap(name, parentModuleMap, isNormalized, applyMap) {
			var url = void 0,
			    pluginModule = void 0,
			    suffix = void 0,
			    nameParts = void 0,
			    prefix = null,
			    parentName = parentModuleMap ? parentModuleMap.name : null,
			    originalName = name,
			    isDefine = true,
			    normalizedName = '';

			// If no name, then it means it is a require call, generate an
			// internal name.
			if (!name) {
				isDefine = false;
				name = '_@r' + (requireCounter += 1);
			}

			nameParts = splitPrefix(name);
			prefix = nameParts[0];
			name = nameParts[1];

			if (prefix) {
				prefix = normalize(prefix, parentName, applyMap);
				pluginModule = getOwn(_defined, prefix);
			}

			// Account for relative paths if there is a base name.
			if (name) {
				if (prefix) {
					if (pluginModule && pluginModule.normalize) {
						// Plugin is loaded, use its normalize method.
						normalizedName = pluginModule.normalize(name, function (name) {
							return normalize(name, parentName, applyMap);
						});
					} else {
						// If nested plugin references, then do not try to
						// normalize, as it will not normalize correctly. This
						// places a restriction on resourceIds, and the longer
						// term solution is not to normalize until plugins are
						// loaded and all normalizations to allow for async
						// loading of a loader plugin. But for now, fixes the
						// common uses. Details in #1131
						normalizedName = name.indexOf('!') === -1 ? normalize(name, parentName, applyMap) : name;
					}
				} else {
					// A regular module.
					normalizedName = normalize(name, parentName, applyMap);

					// Normalized name may be a plugin ID due to map config
					// application in normalize. The map config values must
					// already be normalized, so do not need to redo that part.
					nameParts = splitPrefix(normalizedName);
					prefix = nameParts[0];
					normalizedName = nameParts[1];
					isNormalized = true;

					url = context.nameToUrl(normalizedName);
				}
			}

			// If the id is a plugin id that cannot be determined if it needs
			// normalization, stamp it with a unique ID so two matching relative
			// ids that may conflict can be separate.
			suffix = prefix && !pluginModule && !isNormalized ? '_unnormalized' + (unnormalizedCounter += 1) : '';

			return {
				prefix: prefix,
				name: normalizedName,
				parentMap: parentModuleMap,
				unnormalized: !!suffix,
				url: url,
				originalName: originalName,
				isDefine: isDefine,
				id: (prefix ? prefix + '!' + normalizedName : normalizedName) + suffix
			};
		}

		function getModule(depMap) {
			var id = depMap.id,
			    mod = getOwn(registry, id);

			if (!mod) {
				mod = registry[id] = new context.Module(depMap);
			}

			return mod;
		}

		function on(depMap, name, fn) {
			var id = depMap.id,
			    mod = getOwn(registry, id);

			if (hasProp(_defined, id) && (!mod || mod.defineEmitComplete)) {
				if (name === 'defined') {
					fn(_defined[id]);
				}
			} else {
				mod = getModule(depMap);
				if (mod.error && name === 'error') {
					fn(mod.error);
				} else {
					mod.on(name, fn);
				}
			}
		}

		function onError(err, errback) {
			var ids = err.requireModules,
			    notified = false;

			if (errback) {
				errback(err);
			} else {
				each(ids, function (id) {
					var mod = getOwn(registry, id);
					if (mod) {
						// Set error on module, so it skips timeout checks.
						mod.error = err;
						if (mod.events.error) {
							notified = true;
							mod.emit('error', err);
						}
					}
				});

				if (!notified) {
					req.onError(err);
				}
			}
		}

		/**
   * Internal method to transfer globalQueue items to this context's
   * defQueue.
   */
		function takeGlobalQueue() {
			// Push all the globalDefQueue items into the context's defQueue
			if (globalDefQueue.length) {
				each(globalDefQueue, function (queueItem) {
					var id = queueItem[0];
					if (typeof id === 'string') {
						context.defQueueMap[id] = true;
					}
					defQueue.push(queueItem);
				});
				globalDefQueue = [];
			}
		}

		handlers = {
			'require': function require(mod) {
				if (mod.require) {
					return mod.require;
				} else {
					return mod.require = context.makeRequire(mod.map);
				}
			},
			'exports': function exports(mod) {
				mod.usingExports = true;
				if (mod.map.isDefine) {
					if (mod.exports) {
						return _defined[mod.map.id] = mod.exports;
					} else {
						return mod.exports = _defined[mod.map.id] = {};
					}
				}
			},
			'module': function module(mod) {
				if (mod.module) {
					return mod.module;
				} else {
					return mod.module = {
						id: mod.map.id,
						uri: mod.map.url,
						config: function config() {
							return getOwn(_config.config, mod.map.id) || {};
						},
						exports: mod.exports || (mod.exports = {})
					};
				}
			}
		};

		function cleanRegistry(id) {
			// Clean up machinery used for waiting modules.
			delete registry[id];
			delete enabledRegistry[id];
		}

		function breakCycle(mod, traced, processed) {
			var id = mod.map.id;

			if (mod.error) {
				mod.emit('error', mod.error);
			} else {
				traced[id] = true;
				each(mod.depMaps, function (depMap, i) {
					var depId = depMap.id,
					    dep = getOwn(registry, depId);

					// Only force things that have not completed
					// being defined, so still in the registry,
					// and only if it has not been matched up
					// in the module already.
					if (dep && !mod.depMatched[i] && !processed[depId]) {
						if (getOwn(traced, depId)) {
							mod.defineDep(i, _defined[depId]);
							mod.check(); // pass false?
						} else {
							breakCycle(dep, traced, processed);
						}
					}
				});
				processed[id] = true;
			}
		}

		function checkLoaded() {
			var err = void 0,
			    usingPathFallback = void 0,
			    waitInterval = _config.waitSeconds * 1000,

			// It is possible to disable the wait interval by using waitSeconds of 0.
			expired = waitInterval && context.startTime + waitInterval < new Date().getTime(),
			    noLoads = [],
			    reqCalls = [],
			    stillLoading = false,
			    needCycleCheck = true;

			// Do not bother if this call was a result of a cycle break.
			if (inCheckLoaded) {
				return;
			}

			inCheckLoaded = true;

			// Figure out the state of all the modules.
			eachProp(enabledRegistry, function (mod) {
				var map = mod.map,
				    modId = map.id;

				// Skip things that are not enabled or in error state.
				if (!mod.enabled) {
					return;
				}

				if (!map.isDefine) {
					reqCalls.push(mod);
				}

				if (!mod.error) {
					// If the module should be executed, and it has not
					// been inited and time is up, remember it.
					if (!mod.inited && expired) {
						if (hasPathFallback(modId)) {
							usingPathFallback = true;
							stillLoading = true;
						} else {
							noLoads.push(modId);
							removeScript(modId);
						}
					} else if (!mod.inited && mod.fetched && map.isDefine) {
						stillLoading = true;
						if (!map.prefix) {
							// No reason to keep looking for unfinished
							// loading. If the only stillLoading is a
							// plugin resource though, keep going,
							// because it may be that a plugin resource
							// is waiting on a non-plugin cycle.
							return needCycleCheck = false;
						}
					}
				}
			});

			if (expired && noLoads.length) {
				// If wait time expired, throw error of unloaded modules.
				err = makeError('timeout', 'Load timeout for modules: ' + noLoads, null, noLoads);
				err.contextName = context.contextName;
				return onError(err);
			}

			// Not expired, check for a cycle.
			if (needCycleCheck) {
				each(reqCalls, function (mod) {
					breakCycle(mod, {}, {});
				});
			}

			// If still waiting on loads, and the waiting load is something
			// other than a plugin resource, or there are still outstanding
			// scripts, then just try back later.
			if ((!expired || usingPathFallback) && stillLoading) {
				// Something is still waiting to load. Wait for it, but only
				// if a timeout is not already in effect.
				if ((isBrowser || isWebWorker) && !checkLoadedTimeoutId) {
					checkLoadedTimeoutId = setTimeout(function () {
						checkLoadedTimeoutId = 0;
						checkLoaded();
					}, 50);
				}
			}

			inCheckLoaded = false;
		}

		Module = function Module(map) {
			this.events = getOwn(undefEvents, map.id) || {};
			this.map = map;
			this.shim = getOwn(_config.shim, map.id);
			this.depExports = [];
			this.depMaps = [];
			this.depMatched = [];
			this.pluginMaps = {};
			this.depCount = 0;

			/* this.exports this.factory
    this.depMaps = [],
    this.enabled, this.fetched
    */
		};

		Module.prototype = {
			init: function init(depMaps, factory, errback, options) {
				options = options || {};

				// Do not do more inits if already done. Can happen if there
				// are multiple define calls for the same module. That is not
				// a normal, common case, but it is also not unexpected.
				if (this.inited) {
					return;
				}

				this.factory = factory;

				if (errback) {
					// Register for errors on this module.
					this.on('error', errback);
				} else if (this.events.error) {
					// If no errback already, but there are error listeners
					// on this module, set up an errback to pass to the deps.
					errback = bind(this, function (err) {
						this.emit('error', err);
					});
				}

				// Do a copy of the dependency array, so that
				// source inputs are not modified. For example
				// "shim" deps are passed in here directly, and
				// doing a direct modification of the depMaps array
				// would affect that config.
				this.depMaps = depMaps && depMaps.slice(0);

				this.errback = errback;

				// Indicate this module has be initialized
				this.inited = true;

				this.ignore = options.ignore;

				// Could have option to init this module in enabled mode,
				// or could have been previously marked as enabled. However,
				// the dependencies are not known until init is called. So
				// if enabled previously, now trigger dependencies as enabled.
				if (options.enabled || this.enabled) {
					// Enable this module and dependencies.
					// Will call this.check()
					this.enable();
				} else {
					this.check();
				}
			},

			defineDep: function defineDep(i, depExports) {
				// Because of cycles, defined callback for a given
				// export can be called more than once.
				if (!this.depMatched[i]) {
					this.depMatched[i] = true;
					this.depCount -= 1;
					this.depExports[i] = depExports;
				}
			},

			fetch: function fetch() {
				if (this.fetched) {
					return;
				}
				this.fetched = true;

				context.startTime = new Date().getTime();

				var map = this.map;

				// If the manager is for a plugin managed resource,
				// ask the plugin to load it now.
				if (this.shim) {
					context.makeRequire(this.map, {
						enableBuildCallback: true
					})(this.shim.deps || [], bind(this, function () {
						return map.prefix ? this.callPlugin() : this.load();
					}));
				} else {
					// Regular dependency.
					return map.prefix ? this.callPlugin() : this.load();
				}
			},

			load: function load() {
				var url = this.map.url;

				// Regular dependency.
				if (!urlFetched[url]) {
					urlFetched[url] = true;
					context.load(this.map.id, url);
				}
			},

			/**
    * Checks if the module is ready to define itself, and if so,
    * define it.
    */
			check: function check() {
				if (!this.enabled || this.enabling) {
					return;
				}

				var err = void 0,
				    cjsModule = void 0,
				    id = this.map.id,
				    depExports = this.depExports,
				    exports = this.exports,
				    factory = this.factory;

				if (!this.inited) {
					// Only fetch if not already in the defQueue.
					if (!hasProp(context.defQueueMap, id)) {
						this.fetch();
					}
				} else if (this.error) {
					this.emit('error', this.error);
				} else if (!this.defining) {
					// The factory could trigger another require call
					// that would result in checking this module to
					// define itself again. If already in the process
					// of doing that, skip this work.
					this.defining = true;

					if (this.depCount < 1 && !this.defined) {
						if (isFunction(factory)) {
							try {
								exports = context.execCb(id, factory, depExports, exports);
							} catch (e) {
								err = e;
							}

							// Favor return value over exports. If node/cjs in play,
							// then will not have a return value anyway. Favor
							// module.exports assignment over exports object.
							if (this.map.isDefine && exports === undefined) {
								cjsModule = this.module;
								if (cjsModule) {
									exports = cjsModule.exports;
								} else if (this.usingExports) {
									// exports already set the defined value.
									exports = this.exports;
								}
							}

							if (err) {
								// If there is an error listener, favor passing
								// to that instead of throwing an error. However,
								// only do it for define()'d  modules. require
								// errbacks should not be called for failures in
								// their callbacks (#699). However if a global
								// onError is set, use that.
								if (this.events.error && this.map.isDefine || req.onError !== defaultOnError) {
									err.requireMap = this.map;
									err.requireModules = this.map.isDefine ? [this.map.id] : null;
									err.requireType = this.map.isDefine ? 'define' : 'require';
									return onError(this.error = err);
								} else if (typeof console !== 'undefined' && console.error) {
									// Log the error for debugging. If promises could be
									// used, this would be different, but making do.
									console.error(err);
								} else {
									// Do not want to completely lose the error. While this
									// will mess up processing and lead to similar results
									// as bug 1440, it at least surfaces the error.
									req.onError(err);
								}
							}
						} else {
							// Just a literal value
							exports = factory;
						}

						this.exports = exports;

						if (this.map.isDefine && !this.ignore) {
							_defined[id] = exports;

							if (req.onResourceLoad) {
								var resLoadMaps = [];
								each(this.depMaps, function (depMap) {
									resLoadMaps.push(depMap.normalizedMap || depMap);
								});
								req.onResourceLoad(context, this.map, resLoadMaps);
							}
						}

						// Clean up
						cleanRegistry(id);

						this.defined = true;
					}

					// Finished the define stage. Allow calling check again
					// to allow define notifications below in the case of a
					// cycle.
					this.defining = false;

					if (this.defined && !this.defineEmitted) {
						this.defineEmitted = true;
						this.emit('defined', this.exports);
						this.defineEmitComplete = true;
					}
				}
			},

			callPlugin: function callPlugin() {
				var map = this.map;
				var id = map.id;
				// Map already normalized the prefix.
				var pluginMap = makeModuleMap(map.prefix);

				// Mark this as a dependency for this plugin, so it
				// can be traced for cycles.
				this.depMaps.push(pluginMap);

				on(pluginMap, 'defined', bind(this, function (plugin) {
					var load = void 0,
					    normalizedMap = void 0,
					    normalizedMod = void 0,
					    bundleId = getOwn(bundlesMap, this.map.id),
					    name = this.map.name,
					    parentName = this.map.parentMap ? this.map.parentMap.name : null,
					    localRequire = context.makeRequire(map.parentMap, {
						enableBuildCallback: true
					});

					// If current map is not normalized, wait for that
					// normalized name to load instead of continuing.
					if (this.map.unnormalized) {
						// Normalize the ID if the plugin allows it.
						if (plugin.normalize) {
							name = plugin.normalize(name, function (name) {
								return normalize(name, parentName, true);
							}) || '';
						}

						// prefix and name should already be normalized, no need
						// for applying map config again either.
						normalizedMap = makeModuleMap(map.prefix + '!' + name, this.map.parentMap);
						on(normalizedMap, 'defined', bind(this, function (value) {
							this.map.normalizedMap = normalizedMap;
							this.init([], function () {
								return value;
							}, null, {
								enabled: true,
								ignore: true
							});
						}));

						normalizedMod = getOwn(registry, normalizedMap.id);
						if (normalizedMod) {
							// Mark this as a dependency for this plugin, so it
							// can be traced for cycles.
							this.depMaps.push(normalizedMap);

							if (this.events.error) {
								normalizedMod.on('error', bind(this, function (err) {
									this.emit('error', err);
								}));
							}
							normalizedMod.enable();
						}

						return;
					}

					// If a paths config, then just load that file instead to
					// resolve the plugin, as it is built into that paths layer.
					if (bundleId) {
						this.map.url = context.nameToUrl(bundleId);
						this.load();
						return;
					}

					load = bind(this, function (value) {
						this.init([], function () {
							return value;
						}, null, {
							enabled: true
						});
					});

					load.error = bind(this, function (err) {
						this.inited = true;
						this.error = err;
						err.requireModules = [id];

						// Remove temp unnormalized modules for this module, since they will never be resolved otherwise 
						// now.
						eachProp(registry, function (mod) {
							if (mod.map.id.indexOf(id + '_unnormalized') === 0) {
								cleanRegistry(mod.map.id);
							}
						});

						onError(err);
					});

					// Allow plugins to load other code without having to know the context or how to 'complete' the 
					// load.
					load.fromText = bind(this, function (text, textAlt) {
						/*jslint evil: true */
						var moduleName = map.name,
						    moduleMap = makeModuleMap(moduleName),
						    hasInteractive = useInteractive;

						// As of 2.1.0, support just passing the text, to reinforce fromText only being called once per 
						// resource. Still support old style of passing moduleName but discard that moduleName in favor 
						// of the internal ref.
						if (textAlt) {
							text = textAlt;
						}

						// Turn off interactive script matching for IE for any define calls in the text, then turn it 
						// back on at the end.
						if (hasInteractive) {
							useInteractive = false;
						}

						// Prime the system by creating a module instance for it.
						getModule(moduleMap);

						// Transfer any config to this other module.
						if (hasProp(_config.config, id)) {
							_config.config[moduleName] = _config.config[id];
						}

						try {
							req.exec(text);
						} catch (e) {
							return onError(makeError('fromtexteval', 'fromText eval for ' + id + ' failed: ' + e, e, [id]));
						}

						if (hasInteractive) {
							useInteractive = true;
						}

						// Mark this as a dependency for the plugin resource.
						this.depMaps.push(moduleMap);

						// Support anonymous modules.
						context.completeLoad(moduleName);

						// Bind the value of that module to the value for this resource ID.
						localRequire([moduleName], load);
					});

					// Use parentName here since the plugin's name is not reliable, could be some weird string with no 
					// path that actually wants to reference the parentName's path.
					plugin.load(map.name, localRequire, load, _config);
				}));

				context.enable(pluginMap, this);
				this.pluginMaps[pluginMap.id] = pluginMap;
			},

			enable: function enable() {
				enabledRegistry[this.map.id] = this;
				this.enabled = true;

				// Set flag mentioning that the module is enabling, so that immediate calls to the defined callbacks for 
				// dependencies do not trigger inadvertent load with the depCount still being zero.
				this.enabling = true;

				// Enable each dependency.
				each(this.depMaps, bind(this, function (depMap, i) {
					var id = void 0;
					var mod = void 0;
					var handler = void 0;

					if (typeof depMap === 'string') {
						// Dependency needs to be converted to a depMap and wired up to this module.
						depMap = makeModuleMap(depMap, this.map.isDefine ? this.map : this.map.parentMap, false, !this.skipMap);
						this.depMaps[i] = depMap;

						handler = getOwn(handlers, depMap.id);

						if (handler) {
							this.depExports[i] = handler(this);
							return;
						}

						this.depCount += 1;

						on(depMap, 'defined', bind(this, function (depExports) {
							if (this.undefed) {
								return;
							}
							this.defineDep(i, depExports);
							this.check();
						}));

						if (this.errback) {
							on(depMap, 'error', bind(this, this.errback));
						} else if (this.events.error) {
							// No direct errback on this module, but something else is listening for errors, so be sure 
							// to propagate the error correctly.
							on(depMap, 'error', bind(this, function (err) {
								this.emit('error', err);
							}));
						}
					}

					id = depMap.id;
					mod = registry[id];

					// Skip special modules like 'require', 'exports', 'module'. Also, don't call enable if it is 
					// already enabled, important in circular dependency cases.
					if (!hasProp(handlers, id) && mod && !mod.enabled) {
						context.enable(depMap, this);
					}
				}));

				// Enable each plugin that is used in  a dependency.
				eachProp(this.pluginMaps, bind(this, function (pluginMap) {
					var mod = getOwn(registry, pluginMap.id);
					if (mod && !mod.enabled) {
						context.enable(pluginMap, this);
					}
				}));

				this.enabling = false;

				this.check();
			},

			on: function on(name, cb) {
				var cbs = this.events[name];
				if (!cbs) {
					cbs = this.events[name] = [];
				}
				cbs.push(cb);
			},

			emit: function emit(name, evt) {
				each(this.events[name], function (cb) {
					cb(evt);
				});
				if (name === 'error') {
					// Now that the error handler was triggered, remove the listeners, since this broken Module instance
					// can stay around for a while in the registry.
					delete this.events[name];
				}
			}
		};

		function callGetModule(args) {
			// Skip modules already defined.
			if (!hasProp(_defined, args[0])) {
				getModule(makeModuleMap(args[0], null, true)).init(args[1], args[2]);
			}
		}

		function removeListener(node, func, name, ieName) {
			// Favor detachEvent because of IE9 issue, see attachEvent/addEventListener comment elsewhere in this file.
			if (node.detachEvent && !isOpera) {
				// Probably IE. If not it will throw an error, which will be useful to know.
				if (ieName) {
					node.detachEvent(ieName, func);
				}
			} else {
				node.removeEventListener(name, func, false);
			}
		}

		/**
   * Given an event from a script node, get the requirejs info from it, and then removes the event listeners on 
   * the node.
   * 
   * @param {Event} evt
   * 
   * @return {Object}
   */
		function getScriptData(evt) {
			// Using currentTarget instead of target for Firefox 2.0's sake. Not all old browsers will be supported, but 
			// this one was easy enough to support and still makes sense.
			var node = evt.currentTarget || evt.srcElement;

			// Remove the listeners once here.
			removeListener(node, context.onScriptLoad, 'load', 'onreadystatechange');
			removeListener(node, context.onScriptError, 'error');

			return {
				node: node,
				id: node && node.getAttribute('data-requiremodule')
			};
		}

		function intakeDefines() {
			var args = void 0;

			// Any defined modules in the global queue, intake them now.
			takeGlobalQueue();

			// Make sure any remaining defQueue items get properly processed.
			while (defQueue.length) {
				args = defQueue.shift();
				if (args[0] === null) {
					return onError(makeError('mismatch', 'Mismatched anonymous define() module: ' + args[args.length - 1]));
				} else {
					// args are id, deps, factory. Should be normalized by the
					// define() function.
					callGetModule(args);
				}
			}
			context.defQueueMap = {};
		}

		context = {
			config: _config,
			contextName: contextName,
			registry: registry,
			defined: _defined,
			urlFetched: urlFetched,
			defQueue: defQueue,
			defQueueMap: {},
			Module: Module,
			makeModuleMap: makeModuleMap,
			nextTick: req.nextTick,
			onError: onError,

			/**
    * Set a configuration for the context.
    * 
    * @param {Object} cfg config object to integrate.
    */
			configure: function configure(cfg) {
				// Make sure the baseUrl ends in a slash.
				if (cfg.baseUrl) {
					if (cfg.baseUrl.charAt(cfg.baseUrl.length - 1) !== '/') {
						cfg.baseUrl += '/';
					}
				}

				// Save off the paths since they require special processing, they are additive.
				var shim = _config.shim,
				    objs = {
					paths: true,
					bundles: true,
					config: true,
					map: true
				};

				eachProp(cfg, function (value, prop) {
					if (objs[prop]) {
						if (!_config[prop]) {
							_config[prop] = {};
						}
						mixin(_config[prop], value, true, true);
					} else {
						_config[prop] = value;
					}
				});

				// Reverse map the bundles
				if (cfg.bundles) {
					eachProp(cfg.bundles, function (value, prop) {
						each(value, function (v) {
							if (v !== prop) {
								bundlesMap[v] = prop;
							}
						});
					});
				}

				// Merge shim
				if (cfg.shim) {
					eachProp(cfg.shim, function (value, id) {
						// Normalize the structure
						if (isArray(value)) {
							value = {
								deps: value
							};
						}
						if ((value.exports || value.init) && !value.exportsFn) {
							value.exportsFn = context.makeShimExports(value);
						}
						shim[id] = value;
					});
					_config.shim = shim;
				}

				// Adjust packages if necessary.
				if (cfg.packages) {
					each(cfg.packages, function (pkgObj) {
						var location = void 0,
						    name = void 0;

						pkgObj = typeof pkgObj === 'string' ? { name: pkgObj } : pkgObj;

						name = pkgObj.name;
						location = pkgObj.location;
						if (location) {
							_config.paths[name] = pkgObj.location;
						}

						// Save pointer to main module ID for pkg name. Remove leading dot in main, so main paths are 
						// normalized, and remove any trailing .js, since different package envs have different 
						// conventions: some use a module name, some use a file name.
						_config.pkgs[name] = pkgObj.name + '/' + (pkgObj.main || 'main').replace(currDirRegExp, '').replace(jsSuffixRegExp, '');
					});
				}

				// If there are any "waiting to execute" modules in the registry, update the maps for them, since their 
				// info, like URLs to load, may have changed.
				eachProp(registry, function (mod, id) {
					// If module already has init called, since it is too late to modify them, and ignore unnormalized 
					// ones since they are transient.
					if (!mod.inited && !mod.map.unnormalized) {
						mod.map = makeModuleMap(id, null, true);
					}
				});

				// If a deps array or a config callback is specified, then call require with those args. This is useful 
				// when require is defined as a config object before require.js is loaded.
				if (cfg.deps || cfg.callback) {
					context.require(cfg.deps || [], cfg.callback);
				}
			},

			makeShimExports: function makeShimExports(value) {
				function fn() {
					var ret = void 0;
					if (value.init) {
						ret = value.init.apply(global, arguments);
					}
					return ret || value.exports && getGlobal(value.exports);
				}

				return fn;
			},

			makeRequire: function makeRequire(relMap, options) {
				options = options || {};

				function localRequire(deps, callback, errback) {
					var id = void 0,
					    map = void 0,
					    requireMod = void 0;

					if (options.enableBuildCallback && callback && isFunction(callback)) {
						callback.__requireJsBuild = true;
					}

					if (typeof deps === 'string') {
						if (isFunction(callback)) {
							// Invalid call
							return onError(makeError('requireargs', 'Invalid require call'), errback);
						}

						// If require|exports|module are requested, get the value for them from the special handlers. 
						// Caveat: this only works while module is being defined.
						if (relMap && hasProp(handlers, deps)) {
							return handlers[deps](registry[relMap.id]);
						}

						// Synchronous access to one module. If require.get is available (as in the Node adapter), 
						// prefer that.
						if (req.get) {
							return req.get(context, deps, relMap, localRequire);
						}

						// Normalize module name, if it contains . or ..
						map = makeModuleMap(deps, relMap, false, true);
						id = map.id;

						if (!hasProp(_defined, id)) {
							return onError(makeError('notloaded', 'Module name "' + id + '" has not been loaded yet for context: ' + contextName + (relMap ? '' : '. Use require([])')));
						}
						return _defined[id];
					}

					// Grab defines waiting in the global queue.
					intakeDefines();

					// Mark all the dependencies as needing to be loaded.
					context.nextTick(function () {
						// Some defines could have been added since the
						// require call, collect them.
						intakeDefines();

						requireMod = getModule(makeModuleMap(null, relMap));

						// Store if map config should be applied to this require
						// call for dependencies.
						requireMod.skipMap = options.skipMap;

						requireMod.init(deps, callback, errback, {
							enabled: true
						});

						checkLoaded();
					});

					return localRequire;
				}

				mixin(localRequire, {
					isBrowser: isBrowser,

					/**
      * Converts a module name + .extension into an URL path.
      * 
      * *Requires* the use of a module name. It does not support using plain URLs like nameToUrl.
      */
					toUrl: function toUrl(moduleNamePlusExt) {
						var ext = void 0,
						    index = moduleNamePlusExt.lastIndexOf('.'),
						    segment = moduleNamePlusExt.split('/')[0],
						    isRelative = segment === '.' || segment === '..';

						// Have a file extension alias, and it is not the
						// dots from a relative path.
						if (index !== -1 && (!isRelative || index > 1)) {
							ext = moduleNamePlusExt.substring(index, moduleNamePlusExt.length);
							moduleNamePlusExt = moduleNamePlusExt.substring(0, index);
						}

						return context.nameToUrl(normalize(moduleNamePlusExt, relMap && relMap.id, true), ext, true);
					},

					defined: function defined(id) {
						return hasProp(_defined, makeModuleMap(id, relMap, false, true).id);
					},

					specified: function specified(id) {
						id = makeModuleMap(id, relMap, false, true).id;
						return hasProp(_defined, id) || hasProp(registry, id);
					}
				});

				// Only allow undef on top level require calls.
				if (!relMap) {
					localRequire.undef = function (id) {
						// Bind any waiting define() calls to this context, fix for #408.
						takeGlobalQueue();

						var map = makeModuleMap(id, relMap, true);
						var mod = getOwn(registry, id);

						mod.undefed = true;
						removeScript(id);

						delete _defined[id];
						delete urlFetched[map.url];
						delete undefEvents[id];

						// Clean queued defines too. Go backwards in array so that the splices do not mess up the 
						// iteration.
						eachReverse(defQueue, function (args, i) {
							if (args[0] === id) {
								defQueue.splice(i, 1);
							}
						});
						delete context.defQueueMap[id];

						if (mod) {
							// Hold on to listeners in case the module will be attempted to be reloaded using a 
							// different config.
							if (mod.events.defined) {
								undefEvents[id] = mod.events;
							}

							cleanRegistry(id);
						}
					};
				}

				return localRequire;
			},

			/**
    * Called to enable a module if it is still in the registry awaiting enablement. A second arg, parent, the 
    * parent module, is passed in for context, when this method is overridden by the optimizer. Not shown here 
    * to keep code compact.
    */
			enable: function enable(depMap) {
				var mod = getOwn(registry, depMap.id);
				if (mod) {
					getModule(depMap).enable();
				}
			},

			/**
    * Internal method used by environment adapters to complete a load event.
    * 
    * A load event could be a script load or just a load pass from a synchronous load call. 
    * 
    * @param {String} moduleName The name of the module to potentially complete.
    */
			completeLoad: function completeLoad(moduleName) {
				var found = void 0;
				var args = void 0;
				var mod = void 0;
				var shim = getOwn(_config.shim, moduleName) || {};
				var shExports = shim.exports;

				takeGlobalQueue();

				while (defQueue.length) {
					args = defQueue.shift();
					if (args[0] === null) {
						args[0] = moduleName;
						// If already found an anonymous module and bound it to this name, then this is some other anon 
						// module waiting for its completeLoad to fire.
						if (found) {
							break;
						}
						found = true;
					} else if (args[0] === moduleName) {
						// Found matching define call for this script!
						found = true;
					}

					callGetModule(args);
				}
				context.defQueueMap = {};

				// Do this after the cycle of callGetModule in case the result
				// of those calls/init calls changes the registry.
				mod = getOwn(registry, moduleName);

				if (!found && !hasProp(_defined, moduleName) && mod && !mod.inited) {
					if (_config.enforceDefine && (!shExports || !getGlobal(shExports))) {
						if (hasPathFallback(moduleName)) {
							return;
						} else {
							return onError(makeError('nodefine', 'No define call for ' + moduleName, null, [moduleName]));
						}
					} else {
						// A script that does not call define(), so just simulate the call for it.
						callGetModule([moduleName, shim.deps || [], shim.exportsFn]);
					}
				}

				checkLoaded();
			},

			/**
    * Converts a module name to a file path. 
    * 
    * Supports cases where moduleName may actually be just an URL. 
    * 
    * Note that it **does not** call normalize on the moduleName, it is assumed to have already been 
    * normalized. This is an internal API, not a public one. Use toUrl for the public API.
    */
			nameToUrl: function nameToUrl(moduleName, ext, skipExt) {
				var paths = void 0;
				var syms = void 0;
				var i = void 0;
				var parentModule = void 0;
				var url = void 0;
				var parentPath = void 0,
				    bundleId = void 0;
				var pkgMain = getOwn(_config.pkgs, moduleName);

				if (pkgMain) {
					moduleName = pkgMain;
				}

				bundleId = getOwn(bundlesMap, moduleName);

				if (bundleId) {
					return context.nameToUrl(bundleId, ext, skipExt);
				}

				// If a colon is in the URL, it indicates a protocol is used and it is just an URL to a file, or if it 
				// starts with a slash, contains a query arg (i.e. ?) or ends with .js, then assume the user meant to 
				// use an url and not a module id. The slash is important for protocol-less URLs as well as full paths.
				if (req.jsExtRegExp.test(moduleName)) {
					// Just a plain path, not module name lookup, so just return it. Add extension if it is included. 
					// This is a bit wonky, only non-.js things pass an extension, this method probably needs to be 
					// reworked.
					url = moduleName + (ext || '');
				} else {
					// A module that needs to be converted to a path.
					paths = _config.paths;

					syms = moduleName.split('/');
					// For each module name segment, see if there is a path registered for it. Start with most specific 
					// name and work up from it.
					for (i = syms.length; i > 0; i -= 1) {
						parentModule = syms.slice(0, i).join('/');

						parentPath = getOwn(paths, parentModule);
						if (parentPath) {
							// If an array, it means there are a few choices, Choose the one that is desired.
							if (isArray(parentPath)) {
								parentPath = parentPath[0];
							}
							syms.splice(0, i, parentPath);
							break;
						}
					}

					// Join the path parts together, then figure out if baseUrl is needed.
					url = syms.join('/');
					url += ext || (/^data\:|\?/.test(url) || skipExt ? '' : '.js');
					url = (url.charAt(0) === '/' || url.match(/^[\w\+\.\-]+:/) ? '' : _config.baseUrl) + url;
				}

				return _config.urlArgs ? url + ((url.indexOf('?') === -1 ? '?' : '&') + _config.urlArgs) : url;
			},

			// Delegates to req.load. Broken out as a separate function to allow overriding in the optimizer.
			load: function load(id, url) {
				req.load(context, id, url);
			},

			/**
    * Executes a module callback function. 
    * 
    * Broken out as a separate function solely to allow the build system to sequence the files in the built
    * layer in the right sequence.
    *
    * @private
    */
			execCb: function execCb(name, callback, args, exports) {
				return callback.apply(exports, args);
			},

			/**
    * Callback for script loads, used to check status of loading.
    *
    * @param {Event} evt the event from the browser for the script that was loaded.
    */
			onScriptLoad: function onScriptLoad(evt) {
				// Using currentTarget instead of target for Firefox 2.0's sake. Not
				// all old browsers will be supported, but this one was easy enough
				// to support and still makes sense.
				if (evt.type === 'load' || readyRegExp.test((evt.currentTarget || evt.srcElement).readyState)) {
					// Reset interactive script so a script node is not held onto for
					// to long.
					interactiveScript = null;

					// Pull out the name of the module and the context.
					var data = getScriptData(evt);
					context.completeLoad(data.id);
				}
			},

			/**
    * Callback for script errors.
    */
			onScriptError: function onScriptError(evt) {
				var data = getScriptData(evt);
				if (!hasPathFallback(data.id)) {
					var parents = [];
					eachProp(registry, function (value, key) {
						if (key.indexOf('_@r') !== 0) {
							each(value.depMaps, function (depMap) {
								if (depMap.id === data.id) {
									parents.push(key);
								}
								return true;
							});
						}
					});
					return onError(makeError('scripterror', 'Script error for "' + data.id + (parents.length ? '", needed by: ' + parents.join(', ') : '"'), evt, [data.id]));
				}
			}
		};

		context.require = context.makeRequire();
		return context;
	}

	/**
  * Main entry point.
  *
  * If the only argument to require is a string, then the module that is represented by that string is fetched for 
  * the appropriate context.
  *
  * If the first argument is an array, then it will be treated as an array of dependency string names to fetch. An 
  * optional function callback can be specified to execute when all of those dependencies are available.
  *
  * Make a local req variable to help Caja compliance (it assumes things on a require that are not standardized), 
  * and to give a short name for minification/local scope use.
  */
	req = window.requirejs = function (deps, callback, errback, optional) {
		// Find the right context, use default
		var context = void 0;
		var config = void 0;
		var contextName = defContextName;

		// Determine if have config object in the call.
		if (!isArray(deps) && typeof deps !== 'string') {
			// deps is a config object
			config = deps;
			if (isArray(callback)) {
				// Adjust args if there are dependencies
				deps = callback;
				callback = errback;
				errback = optional;
			} else {
				deps = [];
			}
		}

		if (config && config.context) {
			contextName = config.context;
		}

		context = getOwn(contexts, contextName);
		if (!context) {
			context = contexts[contextName] = req.s.newContext(contextName);
		}

		if (config) {
			context.configure(config);
		}

		return context.require(deps, callback, errback);
	};

	/**
  * Support require.config() to make it easier to cooperate with other AMD loaders on globally agreed names.
  */
	req.config = function (config) {
		return req(config);
	};

	/**
  * Execute something after the current tick of the event loop. 
  * 
  * Override for other envs that have a better solution than setTimeout.
  * 
  * @param  {Function} fn function to execute later.
  */
	req.nextTick = typeof setTimeout !== 'undefined' ? function (fn) {
		setTimeout(fn, 4);
	} : function (fn) {
		fn();
	};

	/**
  * Export require as a global, but only if it does not already exist.
  */
	if (!window.require) {
		window.require = req;
	}

	req.version = version;

	// Used to filter out dependencies that are already paths.
	req.jsExtRegExp = /^\/|:|\?|\.js$/;
	req.isBrowser = isBrowser;
	s = req.s = {
		contexts: contexts,
		newContext: newContext
	};

	// Create default context.
	req({});

	// Exports some context-sensitive methods on global require.
	each(['toUrl', 'undef', 'defined', 'specified'], function (prop) {
		//  Reference from contexts instead of early binding to default context,
		// so that during builds, the latest instance of the default context
		// with its config gets used.
		req[prop] = function () {
			var ctx = contexts[defContextName];
			return ctx.require[prop].apply(ctx, arguments);
		};
	});

	if (isBrowser) {
		head = s.head = document.getElementsByTagName('head')[0];
		// If BASE tag is in play, using appendChild is a problem for IE6.
		// When that browser dies, this can be removed. Details in this jQuery bug:
		// http://dev.jquery.com/ticket/2709
		baseElement = document.getElementsByTagName('base')[0];
		if (baseElement) {
			head = s.head = baseElement.parentNode;
		}
	}

	/**
  * Any errors that require explicitly generates will be passed to this function. 
  * 
  * Intercept/override it if you want custom error handling.
  * 
  * @param {Error} err the error object.
  */
	req.onError = defaultOnError;

	/**
  * Creates the node for the load command. Only used in browser envs.
  */
	req.createNode = function (config, moduleName, url) {
		var node = config.xhtml ? document.createElementNS('http://www.w3.org/1999/xhtml', 'html:script') : document.createElement('script');
		node.type = config.scriptType || 'text/javascript';
		node.charset = 'utf-8';
		node.async = true;
		return node;
	};

	/**
  * Does the request to load a module for the browser case.
  * 
  * Make this a separate function to allow other environments to override it.
  *
  * @param {Object} context the require context to find state.
  * @param {String} moduleName the name of the module.
  * @param {Object} url the URL to the module.
  */
	req.load = function (context, moduleName, url) {
		var config = context && context.config || {};
		var node = void 0;

		if (isBrowser) {
			// In the browser so use a script tag
			node = req.createNode(config, moduleName, url);
			if (config.onNodeCreated) {
				config.onNodeCreated(node, config, moduleName, url);
			}

			node.setAttribute('data-requirecontext', context.contextName);
			node.setAttribute('data-requiremodule', moduleName);

			// Set up load listener. Test attachEvent first because IE9 has a subtle issue in its addEventListener and 
			// script onload firings that do not match the behavior of all other browsers with addEventListener support, 
			// which fire the onload event for a script right after the script execution. See:
			// https://connect.microsoft.com/IE/feedback/details/648057/script-onload-event-is-not-fired-immediately-after-script-execution
			// UNFORTUNATELY Opera implements attachEvent but does not follow the script script execution mode.
			if (node.attachEvent &&
			// Check if node.attachEvent is artificially added by custom script or natively supported by browser
			// read https://github.com/jrburke/requirejs/issues/187
			// if we can NOT find [native code] then it must NOT natively supported. in IE8, node.attachEvent does 
			// not have toString(). Note the test for "[native code" with no closing brace, see:
			// https://github.com/jrburke/requirejs/issues/273
			!(node.attachEvent.toString && node.attachEvent.toString().indexOf('[native code') < 0) && !isOpera) {
				// Probably IE. IE (at least 6-8) do not fire script onload right after executing the script, so
				// we cannot tie the anonymous define call to a name. However, IE reports the script as being in 
				// 'interactive' readyState at the time of the define call.
				useInteractive = true;

				node.attachEvent('onreadystatechange', context.onScriptLoad);
				// It would be great to add an error handler here to catch 404s in IE9+. However, onreadystatechange 
				// will fire before the error handler, so that does not help. If addEventListener is used, then IE will 
				// fire error before load, but we cannot use that pathway given the connect.microsoft.com issue 
				// mentioned above about not doing the 'script execute, then fire the script load event listener before 
				// execute next script' that other browsers do. Best hope: IE10 fixes the issues, and then destroys all 
				// installs of IE 6-9.
				// node.attachEvent('onerror', context.onScriptError);
			} else {
				node.addEventListener('load', context.onScriptLoad, false);
				node.addEventListener('error', context.onScriptError, false);
			}
			node.src = url;

			// For some cache cases in IE 6-8, the script executes before the end of the appendChild execution, so to 
			// tie an anonymous define call to the module name (which is stored on the node), hold on to a reference to 
			// this node, but clear after the DOM insertion.
			if (baseElement) {
				head.insertBefore(node, baseElement);
			} else {
				head.appendChild(node);
			}

			return node;
		} else if (isWebWorker) {
			try {
				// In a web worker, use importScripts. This is not a very
				// efficient use of importScripts, importScripts will block until
				// its script is downloaded and evaluated. However, if web workers
				// are in play, the expectation is that a build has been done so
				// that only one script needs to be loaded anyway. This may need
				// to be reevaluated if other use cases become common.
				importScripts(url);

				// Account for anonymous modules
				context.completeLoad(moduleName);
			} catch (e) {
				context.onError(makeError('importscripts', 'importScripts failed for ' + moduleName + ' at ' + url, e, [moduleName]));
			}
		}
	};

	// Look for a data-main script attribute, which could also adjust the baseUrl.
	if (isBrowser && !cfg.skipDataMain) {
		// Figure out baseUrl. Get it from the script tag with require.js in it.
		eachReverse(scripts(), function (script) {
			// Set the 'head' where we can append children by
			// using the script's parent.
			if (!head) {
				head = script.parentNode;
			}

			// Look for a data-main attribute to set main script for the page
			// to load. If it is there, the path to data main becomes the
			// baseUrl, if it is not already set.
			dataMain = script.getAttribute('data-main');
			if (dataMain) {
				// Preserve dataMain in case it is a path (i.e. contains '?')
				mainScript = dataMain;

				// Set final baseUrl if there is not already an explicit one.
				if (!cfg.baseUrl) {
					// Pull off the directory of data-main for use as the
					// baseUrl.
					src = mainScript.split('/');
					mainScript = src.pop();
					subPath = src.length ? src.join('/') + '/' : './';

					cfg.baseUrl = subPath;
				}

				// Strip off any trailing .js since mainScript is now
				// like a module name.
				mainScript = mainScript.replace(jsSuffixRegExp, '');

				// If mainScript is still a path, fall back to dataMain
				if (req.jsExtRegExp.test(mainScript)) {
					mainScript = dataMain;
				}

				// Put the data-main script in the files to load.
				cfg.deps = cfg.deps ? cfg.deps.concat(mainScript) : [mainScript];

				return true;
			}
		});
	}

	/**
  * Executes the text. Normally just uses eval, but can be modified to use a better, environment-specific call. 
  * Only used for transpiling loader plugins, not for plain JS modules.
  * 
  * @param {String} text The text to execute/evaluate.
  */
	req.exec = function (text) {
		/*jslint evil: true */
		return eval(text);
	};

	// Set up with config info.
	req(cfg);
})();

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}]},{},[12])

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvSlNFbmdpbmUvY29uc3RydWN0b3JzL2NvbGxlY3Rpb24uanMiLCJzcmMvSlNFbmdpbmUvY29uc3RydWN0b3JzL2RhdGFfYmluZGluZy5qcyIsInNyYy9KU0VuZ2luZS9jb25zdHJ1Y3RvcnMvbW9kdWxlLmpzIiwic3JjL0pTRW5naW5lL2NvbnN0cnVjdG9ycy9uYW1lc3BhY2UuanMiLCJzcmMvSlNFbmdpbmUvY29yZS9hYm91dC5qcyIsInNyYy9KU0VuZ2luZS9jb3JlL2NvbmZpZy5qcyIsInNyYy9KU0VuZ2luZS9jb3JlL2RlYnVnLmpzIiwic3JjL0pTRW5naW5lL2NvcmUvZW5naW5lLmpzIiwic3JjL0pTRW5naW5lL2NvcmUvZXh0ZW5kLmpzIiwic3JjL0pTRW5naW5lL2NvcmUvaW5pdGlhbGl6ZS5qcyIsInNyYy9KU0VuZ2luZS9jb3JlL2xhbmcuanMiLCJzcmMvSlNFbmdpbmUvY29yZS9tYWluLmpzIiwic3JjL0pTRW5naW5lL2NvcmUvbW9kdWxlX2xvYWRlci5qcyIsInNyYy9KU0VuZ2luZS9jb3JlL3BvbHlmaWxscy5qcyIsInNyYy9KU0VuZ2luZS9jb3JlL3JlZ2lzdHJ5LmpzIiwic3JjL0pTRW5naW5lL2NvcmUvcmVxdWlyZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7OztBQ0FBOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxZQUFXOztBQUVYOztBQUVBOzs7Ozs7OztBQUpXLEtBV0wsVUFYSztBQVlWOzs7Ozs7O0FBT0Esc0JBQVksSUFBWixFQUFrQixTQUFsQixFQUE2QixTQUE3QixFQUF3QztBQUFBOztBQUN2QyxRQUFLLElBQUwsR0FBWSxJQUFaO0FBQ0EsUUFBSyxTQUFMLEdBQWlCLFNBQWpCO0FBQ0EsUUFBSyxTQUFMLEdBQWlCLFNBQWpCO0FBQ0EsUUFBSyxLQUFMLEdBQWE7QUFDWixhQUFTLEVBREc7QUFFWixVQUFNO0FBRk0sSUFBYjtBQUlBOztBQUVEOzs7Ozs7Ozs7Ozs7OztBQTdCVTtBQUFBO0FBQUEsMEJBeUNILElBekNHLEVBeUNHLFlBekNILEVBeUNpQixJQXpDakIsRUF5Q3VCO0FBQ2hDO0FBQ0EsUUFBSSxDQUFDLElBQUQsSUFBUyxPQUFPLElBQVAsS0FBZ0IsUUFBekIsSUFBcUMsT0FBTyxJQUFQLEtBQWdCLFVBQXpELEVBQXFFO0FBQ3BFLFNBQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxJQUFmLENBQW9CLDZEQUFwQixFQUFtRixTQUFuRjtBQUNBLFlBQU8sS0FBUDtBQUNBOztBQUVEO0FBQ0EsUUFBSSxLQUFLLEtBQUwsQ0FBVyxPQUFYLENBQW1CLElBQW5CLENBQUosRUFBOEI7QUFDN0IsU0FBSSxJQUFKLENBQVMsS0FBVCxDQUFlLElBQWYsQ0FBb0IsNkJBQTZCLElBQTdCLEdBQW9DLHVDQUF4RDtBQUNBLFlBQU8sS0FBUDtBQUNBOztBQUVEO0FBQ0EsU0FBSyxLQUFMLENBQVcsT0FBWCxDQUFtQixJQUFuQixJQUEyQjtBQUMxQixXQUFNLElBRG9CO0FBRTFCLG1CQUFjO0FBRlksS0FBM0I7QUFJQTs7QUFFRDs7Ozs7Ozs7Ozs7OztBQTdEVTtBQUFBO0FBQUEsMEJBeUVXO0FBQUE7O0FBQUEsUUFBaEIsT0FBZ0IsdUVBQU4sSUFBTTs7QUFDcEI7QUFDQSxRQUFJLENBQUMsS0FBSyxTQUFWLEVBQXFCO0FBQ3BCLFdBQU0sSUFBSSxLQUFKLENBQVUseUVBQVYsQ0FBTjtBQUNBOztBQUVEO0FBQ0EsUUFBSSxZQUFZLFNBQVosSUFBeUIsWUFBWSxJQUF6QyxFQUErQztBQUM5QyxlQUFVLEVBQUUsTUFBRixDQUFWO0FBQ0E7O0FBRUQsUUFBTSxZQUFZLFVBQVUsS0FBSyxTQUFMLENBQWUsSUFBekIsR0FBZ0MsR0FBaEMsR0FBc0MsS0FBSyxTQUE3RDtBQUNBLFFBQU0sb0JBQW9CLEVBQUUsUUFBRixFQUExQjtBQUNBLFFBQU0scUJBQXFCLEVBQTNCOztBQUVBLFlBQ0UsTUFERixDQUNTLE1BQU0sU0FBTixHQUFrQixHQUQzQixFQUVFLEdBRkYsQ0FFTSxRQUFRLElBQVIsQ0FBYSxNQUFNLFNBQU4sR0FBa0IsR0FBL0IsQ0FGTixFQUdFLElBSEYsQ0FHTyxVQUFDLEtBQUQsRUFBUSxPQUFSLEVBQW9CO0FBQ3pCLFNBQU0sV0FBVyxFQUFFLE9BQUYsQ0FBakI7QUFDQSxTQUFNLFVBQVUsU0FBUyxJQUFULENBQWMsU0FBZCxDQUFoQjs7QUFFQSxjQUFTLFVBQVQsQ0FBb0IsU0FBcEI7O0FBRUEsT0FBRSxJQUFGLENBQU8sUUFBUSxPQUFSLENBQWdCLHNCQUFoQixFQUF3QyxHQUF4QyxFQUE2QyxJQUE3QyxHQUFvRCxLQUFwRCxDQUEwRCxHQUExRCxDQUFQLEVBQXVFLFVBQUMsS0FBRCxFQUFRLElBQVIsRUFBaUI7QUFDdkYsVUFBSSxTQUFTLEVBQWIsRUFBaUI7QUFDaEIsY0FBTyxJQUFQO0FBQ0E7O0FBRUQsVUFBTSxXQUFXLEVBQUUsUUFBRixFQUFqQjtBQUNBLHlCQUFtQixJQUFuQixDQUF3QixRQUF4Qjs7QUFFQSxVQUFJLElBQUosQ0FBUyxhQUFULENBQ0UsSUFERixDQUNPLFFBRFAsRUFDaUIsSUFEakIsRUFDdUIsS0FEdkIsRUFFRSxJQUZGLENBRU8sVUFBQyxNQUFEO0FBQUEsY0FBWSxPQUFPLElBQVAsQ0FBWSxRQUFaLENBQVo7QUFBQSxPQUZQLEVBR0UsSUFIRixDQUdPLFVBQUMsS0FBRCxFQUFXO0FBQ2hCLGdCQUFTLE1BQVQ7QUFDQTtBQUNBLFdBQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxLQUFmLENBQXFCLDRCQUE0QixJQUFqRCxFQUF1RCxLQUF2RDtBQUNBLE9BUEY7QUFRQSxNQWhCRDtBQWlCQSxLQTFCRjs7QUE0QkE7QUFDQSxNQUFFLElBQUYsQ0FBTyxLQUFQLENBQWEsU0FBYixFQUF3QixrQkFBeEIsRUFBNEMsTUFBNUMsQ0FBbUQ7QUFBQSxZQUFNLGtCQUFrQixPQUFsQixFQUFOO0FBQUEsS0FBbkQ7O0FBRUEsV0FBTyxtQkFBbUIsTUFBbkIsR0FBNEIsa0JBQWtCLE9BQWxCLEVBQTVCLEdBQTBELGtCQUFrQixPQUFsQixFQUFqRTtBQUNBO0FBeEhTOztBQUFBO0FBQUE7O0FBMkhYLEtBQUksWUFBSixDQUFpQixVQUFqQixHQUE4QixVQUE5QjtBQUNBLENBNUhEOzs7Ozs7Ozs7QUNWQTs7Ozs7Ozs7OztBQVVBLENBQUMsWUFBVzs7QUFFWDs7QUFFQTs7Ozs7Ozs7QUFKVyxLQVdMLFdBWEs7QUFZVjs7Ozs7O0FBTUEsdUJBQVksSUFBWixFQUFrQixRQUFsQixFQUE0QjtBQUFBOztBQUMzQixRQUFLLElBQUwsR0FBWSxJQUFaO0FBQ0EsUUFBSyxRQUFMLEdBQWdCLFFBQWhCO0FBQ0EsUUFBSyxLQUFMLEdBQWEsSUFBYjtBQUNBLFFBQUssU0FBTCxHQUFpQixTQUFTLEVBQVQsQ0FBWSx5QkFBWixDQUFqQjtBQUNBLFFBQUssSUFBTDtBQUNBOztBQUVEOzs7OztBQTFCVTtBQUFBO0FBQUEsMEJBNkJIO0FBQUE7O0FBQ04sU0FBSyxRQUFMLENBQWMsRUFBZCxDQUFpQixRQUFqQixFQUEyQixZQUFNO0FBQ2hDLFdBQUssR0FBTDtBQUNBLEtBRkQ7QUFHQTs7QUFFRDs7Ozs7O0FBbkNVO0FBQUE7QUFBQSx5QkF3Q0o7QUFDTCxTQUFLLEtBQUwsR0FBYSxLQUFLLFNBQUwsR0FBaUIsS0FBSyxRQUFMLENBQWMsR0FBZCxFQUFqQixHQUF1QyxLQUFLLFFBQUwsQ0FBYyxJQUFkLEVBQXBEOztBQUVBLFFBQUksS0FBSyxRQUFMLENBQWMsRUFBZCxDQUFpQixXQUFqQixLQUFrQyxLQUFLLFFBQUwsQ0FBYyxFQUFkLENBQWlCLFFBQWpCLENBQXRDLEVBQWtFO0FBQ2pFLFVBQUssS0FBTCxHQUFhLEtBQUssUUFBTCxDQUFjLElBQWQsQ0FBbUIsU0FBbkIsQ0FBYjtBQUNBOztBQUVELFdBQU8sS0FBSyxLQUFaO0FBQ0E7O0FBRUQ7Ozs7OztBQWxEVTtBQUFBO0FBQUEsdUJBdUROLEtBdkRNLEVBdURDO0FBQ1YsU0FBSyxLQUFMLEdBQWEsS0FBYjs7QUFFQSxRQUFJLEtBQUssU0FBVCxFQUFvQjtBQUNuQixVQUFLLFFBQUwsQ0FBYyxHQUFkLENBQWtCLEtBQWxCO0FBQ0EsS0FGRCxNQUVPO0FBQ04sVUFBSyxRQUFMLENBQWMsSUFBZCxDQUFtQixLQUFuQjtBQUNBO0FBQ0Q7QUEvRFM7O0FBQUE7QUFBQTs7QUFrRVgsS0FBSSxZQUFKLENBQWlCLFdBQWpCLEdBQStCLFdBQS9CO0FBQ0EsQ0FuRUQ7Ozs7Ozs7OztBQ1ZBOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxZQUFXOztBQUVYOztBQUVBOzs7Ozs7OztBQUpXLEtBV0wsTUFYSztBQVlWOzs7Ozs7O0FBT0Esa0JBQVksUUFBWixFQUFzQixJQUF0QixFQUE0QixVQUE1QixFQUF3QztBQUFBOztBQUN2QyxRQUFLLFFBQUwsR0FBZ0IsUUFBaEI7QUFDQSxRQUFLLElBQUwsR0FBWSxJQUFaO0FBQ0EsUUFBSyxVQUFMLEdBQWtCLFVBQWxCO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7QUF6QlU7QUFBQTtBQUFBLHdCQWlDTCxrQkFqQ0ssRUFpQ2U7QUFBQTs7QUFDeEI7QUFDQSxRQUFNLFNBQVMsS0FBSyxVQUFMLENBQWdCLEtBQWhCLENBQXNCLE9BQXRCLENBQThCLEtBQUssSUFBbkMsQ0FBZjtBQUNBLFFBQUksVUFBVSxJQUFkOztBQUVBLFFBQUk7QUFDSCxTQUFJLENBQUMsTUFBTCxFQUFhO0FBQ1osWUFBTSxJQUFJLEtBQUosY0FBcUIsS0FBSyxJQUExQixtREFBTjtBQUNBOztBQUVELFNBQU0sT0FBTyxLQUFLLGNBQUwsRUFBYjtBQUNBLFNBQU0sV0FBVyxPQUFPLElBQVAsQ0FBWSxJQUFaLENBQWlCLEtBQUssUUFBdEIsRUFBZ0MsSUFBaEMsQ0FBakI7O0FBRUE7QUFDQTtBQUNBLFNBQU0sT0FBTyxTQUFQLElBQU8sR0FBTTtBQUNsQixZQUFLLFFBQUwsQ0FBYyxPQUFkLENBQXNCLHdCQUF0QixFQUFnRCxDQUFDLEVBQUMsUUFBUSxNQUFLLElBQWQsRUFBRCxDQUFoRDtBQUNBLFVBQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxJQUFmLGNBQStCLE1BQUssSUFBcEM7QUFDQSx5QkFBbUIsT0FBbkI7QUFDQSxtQkFBYSxPQUFiO0FBQ0EsTUFMRDs7QUFPQTtBQUNBLFVBQUssZUFBTCxDQUFxQixRQUFyQixFQUNFLElBREYsQ0FDTyxZQUFNO0FBQ1g7QUFDQSxnQkFBVSxXQUFXLFlBQU07QUFDMUIsV0FBSSxJQUFKLENBQVMsS0FBVCxDQUFlLElBQWYsQ0FBb0IscURBQXFELE1BQUssSUFBOUU7QUFDQSwwQkFBbUIsTUFBbkI7QUFDQSxPQUhTLEVBR1AsS0FITyxDQUFWOztBQUtBLGVBQVMsSUFBVCxDQUFjLElBQWQ7QUFDQSxNQVRGLEVBVUUsSUFWRixDQVVPLFVBQUMsS0FBRCxFQUFXO0FBQ2hCLHlCQUFtQixNQUFuQjtBQUNBLFVBQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxLQUFmLENBQXFCLHFDQUFyQixFQUE0RCxLQUE1RDtBQUNBLE1BYkY7QUFjQSxLQWhDRCxDQWdDRSxPQUFPLFNBQVAsRUFBa0I7QUFDbkIsd0JBQW1CLE1BQW5CO0FBQ0EsU0FBSSxJQUFKLENBQVMsS0FBVCxDQUFlLEtBQWYsZ0NBQWtELEtBQUssSUFBdkQsU0FBaUUsU0FBakU7QUFDQSxPQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLE9BQWxCLEVBQTJCLENBQUMsU0FBRCxDQUEzQixFQUhtQixDQUdzQjtBQUN6Qzs7QUFFRCxXQUFPLG1CQUFtQixPQUFuQixFQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBL0VVO0FBQUE7QUFBQSxvQ0FzRk87QUFBQTs7QUFDaEIsUUFBTSxPQUFPLEVBQWI7O0FBRUEsTUFBRSxJQUFGLENBQU8sS0FBSyxRQUFMLENBQWMsSUFBZCxFQUFQLEVBQTZCLFVBQUMsSUFBRCxFQUFPLEtBQVAsRUFBaUI7QUFDN0MsU0FBSSxLQUFLLE9BQUwsQ0FBYSxPQUFLLElBQWxCLE1BQTRCLENBQTVCLElBQWlDLEtBQUssT0FBTCxDQUFhLE9BQUssSUFBTCxDQUFVLFdBQVYsRUFBYixNQUEwQyxDQUEvRSxFQUFrRjtBQUNqRixVQUFJLE1BQU0sS0FBSyxNQUFMLENBQVksT0FBSyxJQUFMLENBQVUsTUFBdEIsQ0FBVjtBQUNBLFlBQU0sSUFBSSxNQUFKLENBQVcsQ0FBWCxFQUFjLENBQWQsRUFBaUIsV0FBakIsS0FBaUMsSUFBSSxNQUFKLENBQVcsQ0FBWCxDQUF2QztBQUNBLFdBQUssR0FBTCxJQUFZLEtBQVo7QUFDQTtBQUNBLFVBQU0sZUFBZSxJQUFJLE9BQUosQ0FBWSxpQkFBWixFQUErQixPQUEvQixFQUF3QyxXQUF4QyxFQUFyQjtBQUNBLGFBQUssUUFBTCxDQUFjLFVBQWQsV0FBaUMsT0FBSyxJQUF0QyxTQUE4QyxZQUE5QztBQUNBO0FBQ0QsS0FURDs7QUFXQSxXQUFPLElBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7OztBQXZHVTtBQUFBO0FBQUEsbUNBZ0hNLFFBaEhOLEVBZ0hnQjtBQUN6QixRQUFNLFdBQVcsRUFBRSxRQUFGLEVBQWpCO0FBQ0EsUUFBTSxxQkFBcUIsRUFBM0I7O0FBRUEsUUFBSTtBQUNILFNBQUksU0FBUyxLQUFiLEVBQW9CO0FBQ25CLFFBQUUsSUFBRixDQUFPLFNBQVMsS0FBaEIsRUFBdUIsVUFBUyxLQUFULEVBQWdCLEdBQWhCLEVBQXFCO0FBQzNDLFdBQU0sZ0JBQWdCLEVBQUUsUUFBRixFQUF0QjtBQUNBLDBCQUFtQixJQUFuQixDQUF3QixhQUF4QjtBQUNBLFNBQUUsT0FBRixDQUFVLEdBQVYsRUFDRSxJQURGLENBQ08sVUFBQyxRQUFELEVBQWM7QUFDbkIsaUJBQVMsS0FBVCxDQUFlLEtBQWYsSUFBd0IsUUFBeEI7QUFDQSxzQkFBYyxPQUFkLENBQXNCLFFBQXRCO0FBQ0EsUUFKRixFQUtFLElBTEYsQ0FLTyxVQUFDLEtBQUQsRUFBVztBQUNoQixzQkFBYyxNQUFkLENBQXFCLEtBQXJCO0FBQ0EsUUFQRjtBQVFBLE9BWEQ7QUFZQTs7QUFFRCxTQUFJLFNBQVMsSUFBYixFQUFtQjtBQUNsQixRQUFFLElBQUYsQ0FBTyxTQUFTLElBQWhCLEVBQXNCLFVBQVMsS0FBVCxFQUFnQixHQUFoQixFQUFxQjtBQUMxQyxXQUFNLGVBQWUsRUFBRSxRQUFGLEVBQXJCO0FBQ0EsMEJBQW1CLElBQW5CLENBQXdCLFlBQXhCO0FBQ0EsU0FBRSxHQUFGLENBQU0sR0FBTixFQUNFLElBREYsQ0FDTyxVQUFDLFFBQUQsRUFBYztBQUNuQixpQkFBUyxJQUFULENBQWMsS0FBZCxJQUF1QixRQUF2QjtBQUNBLHFCQUFhLE9BQWIsQ0FBcUIsUUFBckI7QUFDQSxRQUpGLEVBS0UsSUFMRixDQUtPLFVBQUMsS0FBRCxFQUFXO0FBQ2hCLHFCQUFhLE1BQWIsQ0FBb0IsS0FBcEI7QUFDQSxRQVBGO0FBUUEsT0FYRDtBQVlBOztBQUVELFNBQUksU0FBUyxRQUFiLEVBQXVCO0FBQ3RCLFdBQUssSUFBSSxJQUFULElBQWlCLFNBQVMsUUFBMUIsRUFBb0M7QUFDbkMsV0FBTSxXQUFXLFNBQVMsUUFBVCxDQUFrQixJQUFsQixDQUFqQjtBQUNBLGdCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsSUFBMEIsSUFBSSxJQUFJLFlBQUosQ0FBaUIsV0FBckIsQ0FBaUMsSUFBakMsRUFBdUMsUUFBdkMsQ0FBMUI7QUFDQTtBQUNEOztBQUVELE9BQUUsSUFBRixDQUFPLEtBQVAsQ0FBYSxTQUFiLEVBQXdCLGtCQUF4QixFQUNFLElBREYsQ0FDTyxTQUFTLE9BRGhCLEVBRUUsSUFGRixDQUVPLFVBQUMsS0FBRCxFQUFXO0FBQ2hCLGVBQVMsTUFBVCxDQUFnQixJQUFJLEtBQUosbUNBQTBDLFNBQVMsSUFBbkQsU0FBNkQsS0FBN0QsQ0FBaEI7QUFDQSxNQUpGO0FBS0EsS0EzQ0QsQ0EyQ0UsT0FBTyxTQUFQLEVBQWtCO0FBQ25CLGNBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNBLFNBQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxLQUFmLHNDQUF3RCxLQUFLLElBQTdELFNBQXVFLFNBQXZFO0FBQ0EsT0FBRSxNQUFGLEVBQVUsT0FBVixDQUFrQixPQUFsQixFQUEyQixDQUFDLFNBQUQsQ0FBM0IsRUFIbUIsQ0FHc0I7QUFDekM7O0FBRUQsV0FBTyxTQUFTLE9BQVQsRUFBUDtBQUNBO0FBdEtTOztBQUFBO0FBQUE7O0FBeUtYLEtBQUksWUFBSixDQUFpQixNQUFqQixHQUEwQixNQUExQjtBQUNBLENBMUtEOzs7Ozs7Ozs7QUNWQTs7Ozs7Ozs7OztBQVVBLENBQUMsWUFBVzs7QUFFWDs7QUFFQTs7Ozs7Ozs7O0FBSlcsS0FZTCxTQVpLO0FBYVY7Ozs7Ozs7QUFPQSxxQkFBWSxJQUFaLEVBQWtCLE1BQWxCLEVBQTBCLFdBQTFCLEVBQXVDO0FBQUE7O0FBQ3RDLFFBQUssSUFBTCxHQUFZLElBQVo7QUFDQSxRQUFLLE1BQUwsR0FBYyxNQUFkO0FBQ0EsUUFBSyxXQUFMLEdBQW1CLFdBQW5CLENBSHNDLENBR047QUFDaEM7O0FBRUQ7Ozs7Ozs7Ozs7QUExQlU7QUFBQTtBQUFBLDBCQWtDSDtBQUNOLFFBQU0scUJBQXFCLEVBQTNCOztBQURNO0FBQUE7QUFBQTs7QUFBQTtBQUdOLDBCQUF1QixLQUFLLFdBQTVCLDhIQUF5QztBQUFBLFVBQWhDLFVBQWdDOztBQUN4QyxXQUFLLFdBQVcsSUFBaEIsSUFBd0IsSUFBSSxJQUFJLFlBQUosQ0FBaUIsVUFBckIsQ0FBZ0MsV0FBVyxJQUEzQyxFQUFpRCxXQUFXLFNBQTVELEVBQXVFLElBQXZFLENBQXhCO0FBQ0EsVUFBTSxXQUFXLEtBQUssV0FBVyxJQUFoQixFQUFzQixJQUF0QixFQUFqQjtBQUNBLHlCQUFtQixJQUFuQixDQUF3QixRQUF4QjtBQUNBO0FBUEs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFTTixXQUFPLG1CQUFtQixNQUFuQixHQUE0QixFQUFFLElBQUYsQ0FBTyxLQUFQLENBQWEsU0FBYixFQUF3QixrQkFBeEIsRUFBNEMsT0FBNUMsRUFBNUIsR0FBb0YsRUFBRSxRQUFGLEdBQWEsT0FBYixFQUEzRjtBQUNBO0FBNUNTOztBQUFBO0FBQUE7O0FBK0NYLEtBQUksWUFBSixDQUFpQixTQUFqQixHQUE2QixTQUE3QjtBQUNBLENBaEREOzs7OztBQ1ZBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7OztBQVNBLFNBQVMsZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQThDLFlBQVc7O0FBRXhEOztBQUVBLEtBQUksSUFBSSxJQUFKLENBQVMsTUFBVCxDQUFnQixHQUFoQixDQUFvQixhQUFwQixNQUF1QyxZQUEzQyxFQUF5RDtBQUN4RDtBQUNBOztBQUVELEtBQUksS0FBSixHQUFZLFlBQVk7QUFDdkIsTUFBTSwrQkFDUSxJQUFJLElBQUosQ0FBUyxNQUFULENBQWdCLEdBQWhCLENBQW9CLFNBQXBCLENBRFIsODlDQUFOOztBQXdCQSxNQUFJLElBQUosQ0FBUyxLQUFULENBQWUsSUFBZixDQUFvQixJQUFwQjtBQUNBLEVBMUJEO0FBMkJBLENBbkNEOzs7OztBQ25CQTs7Ozs7Ozs7OztBQVVBLElBQUksSUFBSixDQUFTLE1BQVQsR0FBa0IsSUFBSSxJQUFKLENBQVMsTUFBVCxJQUFtQixFQUFyQzs7QUFFQTs7Ozs7Ozs7Ozs7OztBQWFDLFdBQVMsT0FBVCxFQUFrQjs7QUFFbEI7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEtBQU0sU0FBUztBQUNkOzs7OztBQUtBLFdBQVMsS0FOSzs7QUFRZDs7Ozs7OztBQU9BLFVBQVEsSUFmTTs7QUFpQmQ7Ozs7Ozs7OztBQVNBLFdBQVMsSUExQks7O0FBNEJkOzs7Ozs7O0FBT0EsY0FBWSxJQW5DRTs7QUFxQ2Q7Ozs7Ozs7OztBQVNBLGVBQWEsSUE5Q0M7O0FBZ0RkOzs7Ozs7O0FBT0EsYUFBVyxJQXZERzs7QUF5RGQ7Ozs7Ozs7OztBQVNBLGVBQWEsWUFsRUM7O0FBb0VkOzs7Ozs7Ozs7QUFTQSxnQkFBYyxFQTdFQTs7QUErRWQ7Ozs7Ozs7O0FBUUEsZUFBYSxFQXZGQzs7QUF5RmQ7Ozs7O0FBS0EsZ0JBQWMsSUE5RkE7O0FBZ0dkOzs7Ozs7QUFNQSxTQUFPLFFBdEdPOztBQXdHZDs7Ozs7Ozs7O0FBU0EsYUFBVyxJQWpIRzs7QUFtSGQ7Ozs7O0FBS0EsVUFBUyxpRUFBaUUsSUFBakUsQ0FBc0UsVUFBVSxTQUFoRixDQXhISzs7QUEwSGQ7Ozs7O0FBS0EsU0FBUyxrQkFBa0IsTUFBbkIsSUFBOEIsT0FBTyxZQUFyQyxJQUFxRCxPQUFPLGlCQUE3RCxHQUFrRixJQUFsRixHQUF5RixLQS9IbEY7O0FBaUlkOzs7Ozs7O0FBT0EsZUFBYSwwQ0F4SUM7O0FBMElkOzs7Ozs7OztBQVFBLGFBQVcsRUFsSkc7O0FBb0pkOzs7Ozs7OztBQVFBLGNBQVksRUE1SkU7O0FBOEpkOzs7OztBQUtBLFdBQVMsV0FBVyxRQUFRLFlBQW5CLElBQW1DLFFBQVE7QUFuS3RDLEVBQWY7O0FBc0tBOzs7OztBQUtBLEtBQU0sWUFBWSxDQUNqQixTQURpQixFQUVqQixZQUZpQixFQUdqQixhQUhpQixDQUFsQjs7QUFNQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUFPQSxTQUFRLEdBQVIsR0FBYyxVQUFTLElBQVQsRUFBZTtBQUM1QixNQUFJLE9BQU8sV0FBUCxLQUF1QixZQUF2QixJQUF1QyxVQUFVLFFBQVYsQ0FBbUIsSUFBbkIsQ0FBM0MsRUFBcUU7QUFDcEUsVUFBTyxJQUFQO0FBQ0E7O0FBRUQsU0FBTyxPQUFPLElBQVAsQ0FBUDtBQUNBLEVBTkQ7O0FBUUE7Ozs7Ozs7Ozs7O0FBV0EsU0FBUSxJQUFSLEdBQWUsVUFBUyxxQkFBVCxFQUFnQztBQUM5QyxTQUFPLFdBQVAsR0FBcUIsc0JBQXNCLFdBQTNDO0FBQ0EsU0FBTyxNQUFQLEdBQWdCLHNCQUFzQixNQUF0QixDQUE2QixPQUE3QixDQUFxQyxNQUFyQyxFQUE2QyxFQUE3QyxDQUFoQixDQUY4QyxDQUVvQjs7QUFFbEUsTUFBSSxPQUFPLFdBQVAsS0FBdUIsYUFBM0IsRUFBMEM7QUFDekMsVUFBTyxTQUFQLEdBQW1CLEtBQW5CO0FBQ0EsVUFBTyxRQUFQLEdBQWtCLEtBQWxCO0FBQ0EsVUFBTyxLQUFQLEdBQWUsT0FBZjtBQUNBOztBQUVELE1BQUksc0JBQXNCLFNBQXRCLEtBQW9DLFNBQXhDLEVBQW1EO0FBQ2xELFVBQU8sU0FBUCxHQUFtQixzQkFBc0IsU0FBdEIsQ0FBZ0MsT0FBaEMsQ0FBd0MsTUFBeEMsRUFBZ0QsRUFBaEQsQ0FBbkI7QUFDQSxHQUZELE1BRU87QUFDTixVQUFPLFNBQVAsR0FBbUIsT0FBTyxNQUFQLEdBQWdCLGlCQUFuQztBQUNBOztBQUVELE1BQUksc0JBQXNCLFlBQXRCLEtBQXVDLFNBQTNDLEVBQXNEO0FBQ3JELFVBQU8sWUFBUCxHQUFzQixzQkFBc0IsWUFBNUM7O0FBRUEsUUFBSyxJQUFJLFdBQVQsSUFBd0IsT0FBTyxZQUEvQixFQUE2QztBQUM1QyxRQUFJLElBQUosQ0FBUyxJQUFULENBQWMsVUFBZCxDQUF5QixXQUF6QixFQUFzQyxPQUFPLFlBQVAsQ0FBb0IsV0FBcEIsQ0FBdEM7QUFDQTtBQUNEOztBQUVELE1BQUksc0JBQXNCLFdBQXRCLEtBQXNDLFNBQTFDLEVBQXFEO0FBQ3BELFVBQU8sV0FBUCxHQUFxQixzQkFBc0IsV0FBM0M7QUFDQSxHQUZELE1BRU87QUFDTixVQUFPLFdBQVAsR0FBcUIsQ0FDcEIsRUFBQyxNQUFNLGFBQVAsRUFBc0IsV0FBVyxZQUFqQyxFQURvQixFQUVwQixFQUFDLE1BQU0sWUFBUCxFQUFxQixXQUFXLFdBQWhDLEVBRm9CLEVBR3BCLEVBQUMsTUFBTSxTQUFQLEVBQWtCLFdBQVcsUUFBN0IsRUFIb0IsQ0FBckI7QUFLQTs7QUFFRCxNQUFJLHNCQUFzQixVQUF0QixLQUFxQyxTQUF6QyxFQUFvRDtBQUNuRCxVQUFPLFVBQVAsR0FBb0Isc0JBQXNCLFVBQTFDO0FBQ0E7O0FBRUQsTUFBSSxzQkFBc0IsT0FBdEIsS0FBa0MsU0FBdEMsRUFBaUQ7QUFDaEQsT0FBSSxJQUFKLENBQVMsS0FBVCxDQUFlLElBQWYsQ0FBb0Isc0ZBQ2pCLDJCQURIO0FBRUEsVUFBTyxPQUFQLEdBQWlCLHNCQUFzQixPQUF0QixDQUE4QixPQUE5QixDQUFzQyxNQUF0QyxFQUE4QyxFQUE5QyxDQUFqQjtBQUNBLFVBQU8sTUFBUCxHQUFnQixPQUFPLE1BQVAsSUFBaUIsT0FBTyxPQUF4QyxDQUpnRCxDQUlDO0FBQ2pEOztBQUVELE1BQUksc0JBQXNCLFdBQXRCLEtBQXNDLFNBQTFDLEVBQXFEO0FBQ3BELE9BQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxJQUFmLENBQW9CLDBGQUNqQiwrQkFESDtBQUVBLFVBQU8sV0FBUCxHQUFxQixzQkFBc0IsV0FBM0M7QUFDQTs7QUFFRCxNQUFJLHNCQUFzQixNQUF0QixLQUFpQyxTQUFyQyxFQUFnRDtBQUMvQyxVQUFPLE1BQVAsR0FBZ0Isc0JBQXNCLE1BQXRDO0FBQ0E7O0FBRUQsTUFBSSxzQkFBc0IsWUFBdEIsS0FBdUMsU0FBM0MsRUFBc0Q7QUFDckQsVUFBTyxZQUFQLEdBQXNCLHNCQUFzQixZQUE1QztBQUNBOztBQUVELE1BQUksU0FBUyxjQUFULENBQXdCLFNBQXhCLE1BQXVDLElBQXZDLElBQ0EsU0FBUyxjQUFULENBQXdCLFNBQXhCLEVBQW1DLFlBQW5DLENBQWdELGlCQUFoRCxDQURKLEVBQ3dFO0FBQ3ZFLHlCQUFzQixTQUF0QixHQUFrQyxTQUFTLGNBQVQsQ0FBd0IsU0FBeEIsRUFBbUMsWUFBbkMsQ0FBZ0QsaUJBQWhELENBQWxDO0FBQ0E7O0FBRUQsTUFBSSxzQkFBc0IsU0FBdEIsS0FBb0MsU0FBeEMsRUFBbUQ7QUFDbEQsVUFBTyxTQUFQLEdBQW1CLHNCQUFzQixTQUF6QztBQUNBOztBQUVELE1BQUksc0JBQXNCLFVBQXRCLEtBQXFDLFNBQXpDLEVBQW9EO0FBQ25ELFVBQU8sVUFBUCxHQUFvQixzQkFBc0IsVUFBMUM7QUFDQTs7QUFFRDtBQUNBLE1BQU0scUJBQXFCO0FBQzFCLFVBQU8sWUFEbUI7QUFFMUIsUUFBSyxXQUZxQjtBQUcxQixTQUFNO0FBSG9CLEdBQTNCOztBQU1BLE1BQU0sdUJBQXVCO0FBQzVCLFVBQU8sYUFEcUI7QUFFNUIsUUFBSyxXQUZ1QjtBQUc1QixTQUFNO0FBSHNCLEdBQTdCOztBQU1BLFNBQU8sV0FBUCxHQUFzQixPQUFPLGlCQUFSLEdBQTZCLG9CQUE3QixHQUFvRCxrQkFBekU7O0FBRUE7QUFDQSxPQUFLLElBQUksS0FBVCxJQUFrQixzQkFBc0IsUUFBeEMsRUFBa0Q7QUFDakQsT0FBSSxJQUFKLENBQVMsUUFBVCxDQUFrQixHQUFsQixDQUFzQixLQUF0QixFQUE2QixzQkFBc0IsUUFBdEIsQ0FBK0IsS0FBL0IsQ0FBN0I7QUFDQTs7QUFFRDtBQUNBLE1BQUksSUFBSixDQUFTLGFBQVQsQ0FBdUIsSUFBdkI7O0FBRUE7QUFDQSxTQUFPLE9BQU8scUJBQWQ7QUFDQSxFQWpHRDtBQW1HQSxDQTFUQSxFQTBUQyxJQUFJLElBQUosQ0FBUyxNQTFUVixDQUFEOzs7OztBQ3pCQTs7Ozs7Ozs7OztBQVVBLElBQUksSUFBSixDQUFTLEtBQVQsR0FBaUIsSUFBSSxJQUFKLENBQVMsS0FBVCxJQUFrQixFQUFuQzs7QUFFQTs7Ozs7Ozs7QUFRQyxXQUFTLE9BQVQsRUFBa0I7QUFDbEI7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7O0FBR0EsS0FBTSxhQUFhLE9BQW5COztBQUVBOzs7QUFHQSxLQUFNLFlBQVksTUFBbEI7O0FBRUE7OztBQUdBLEtBQU0sV0FBVyxLQUFqQjs7QUFFQTs7O0FBR0EsS0FBTSxZQUFZLE1BQWxCOztBQUVBOzs7QUFHQSxLQUFNLGFBQWEsT0FBbkI7O0FBRUE7OztBQUdBLEtBQU0sYUFBYSxPQUFuQjs7QUFFQTs7O0FBR0EsS0FBTSxjQUFjLFFBQXBCOztBQUVBOzs7QUFHQSxLQUFNLGNBQWMsUUFBcEI7O0FBRUE7Ozs7O0FBS0EsS0FBTSxTQUFTLENBQ2QsVUFEYyxFQUVkLFNBRmMsRUFHZCxRQUhjLEVBSWQsU0FKYyxFQUtkLFVBTGMsRUFNZCxVQU5jLEVBT2QsV0FQYyxFQVFkLFdBUmMsQ0FBZjs7QUFXQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUFPQSxVQUFTLHVCQUFULEdBQW1DO0FBQ2xDLE1BQU0sU0FBUyxTQUFTLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBZjtBQUNBLE1BQU0sVUFBVSxTQUFTLGFBQVQsQ0FBdUIsdUJBQXZCLENBQWhCOztBQUVBLE1BQUksT0FBTyxVQUFQLElBQXFCLENBQUMsUUFBUSxTQUFSLENBQWtCLFFBQWxCLENBQTJCLGFBQTNCLENBQTFCLEVBQXFFO0FBQ3BFLE9BQU0sTUFBTSxTQUFTLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBWjtBQUNBLFVBQU8sTUFBUCxHQUFnQixPQUFPLEtBQVAsR0FBZSxFQUEvQjtBQUNBLE9BQU0sTUFBTSxPQUFPLFVBQVAsQ0FBa0IsSUFBbEIsQ0FBWjtBQUNBLE9BQUksTUFBSixHQUFhLFlBQVc7QUFBRTtBQUN6QixRQUFJLFNBQUosQ0FBYyxJQUFkLEVBQW9CLENBQXBCLEVBQXVCLENBQXZCO0FBQ0EsUUFBSSxXQUFKLEdBQWtCLElBQWxCO0FBQ0EsUUFBSSxTQUFKLEdBQWdCLFNBQWhCO0FBQ0EsUUFBSSxJQUFKLENBQVMsQ0FBVCxFQUFZLENBQVosRUFBZSxFQUFmLEVBQW1CLEVBQW5CO0FBQ0EsUUFBSSxJQUFKO0FBQ0EsWUFBUSxJQUFSLEdBQWUsT0FBTyxTQUFQLENBQWlCLFdBQWpCLENBQWY7QUFDQSxZQUFRLFNBQVIsSUFBcUIsYUFBckI7QUFDQSxJQVJEO0FBU0EsT0FBSSxHQUFKLEdBQVUsUUFBUSxJQUFsQjtBQUNBO0FBQ0Q7O0FBRUQ7Ozs7O0FBS0EsVUFBUyxtQkFBVCxHQUErQjtBQUM5QixNQUFJLElBQUksSUFBSixDQUFTLE1BQVQsQ0FBZ0IsR0FBaEIsQ0FBb0IsYUFBcEIsTUFBdUMsWUFBM0MsRUFBeUQ7QUFDeEQ7QUFDQSxPQUFJLElBQUksSUFBSixDQUFTLEtBQVQsS0FBbUIsU0FBdkIsRUFBa0M7QUFDakMsUUFBSSxJQUFKLENBQVMsS0FBVCxDQUFlLEtBQWYsQ0FBcUIseUJBQXJCLEVBQWdELFNBQWhEO0FBQ0EsSUFGRCxNQUVPO0FBQ04sWUFBUSxHQUFSLENBQVkseUJBQVosRUFBdUMsU0FBdkM7QUFDQTs7QUFFRDtBQUNBLE9BQU0sUUFBUSxlQUFkO0FBQ0EsT0FBSSxRQUFRLE9BQU8sUUFBUCxDQUFnQixLQUE1QjtBQUNBLE9BQUksYUFBYSxDQUFqQjs7QUFFQTtBQUNBLE9BQUksTUFBTSxLQUFOLENBQVksS0FBWixNQUF1QixJQUEzQixFQUFpQztBQUNoQyxpQkFBYSxTQUFTLE1BQU0sS0FBTixDQUFZLEtBQVosRUFBbUIsQ0FBbkIsQ0FBVCxFQUFnQyxFQUFoQyxJQUFzQyxDQUFuRDtBQUNBLFlBQVEsTUFBTSxPQUFOLENBQWMsS0FBZCxFQUFxQixFQUFyQixDQUFSO0FBQ0E7O0FBRUQ7QUFDQSxXQUFRLFFBQVEsVUFBUixHQUFxQixJQUFyQixHQUE0QixLQUFwQztBQUNBLFVBQU8sUUFBUCxDQUFnQixLQUFoQixHQUF3QixLQUF4Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUQsU0FBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBUUEsVUFBUyxRQUFULENBQWtCLE1BQWxCLEVBQTBCLElBQTFCLEVBQWdDO0FBQy9CLE1BQU0sa0JBQWtCLE9BQU8sT0FBUCxDQUFlLE1BQWYsQ0FBeEI7QUFDQSxNQUFNLGtCQUFrQixPQUFPLE9BQVAsQ0FBZSxJQUFJLElBQUosQ0FBUyxNQUFULENBQWdCLEdBQWhCLENBQW9CLE9BQXBCLENBQWYsQ0FBeEI7QUFDQSxNQUFJLGdCQUFnQixJQUFwQjs7QUFFQSxNQUFJLG1CQUFtQixlQUF2QixFQUF3QztBQUN2QyxtQkFBZ0IsT0FBTyxXQUFQLEVBQWhCOztBQUVBLFdBQVEsYUFBUjtBQUNDLFNBQUssT0FBTDtBQUNDLFdBQU0sS0FBSyxTQUFMLENBQWUsSUFBZixDQUFOO0FBQ0E7O0FBRUQsU0FBSyxRQUFMO0FBQ0MsU0FBTSxvQkFBb0IsRUFBRSxxQkFBRixDQUExQjs7QUFFQSxTQUFJLENBQUMsa0JBQWtCLE1BQXZCLEVBQStCO0FBQzlCLFFBQUUsU0FBRixFQUNFLFFBREYsQ0FDVyxvQkFEWCxFQUVFLEdBRkYsQ0FFTTtBQUNKLGlCQUFVLE9BRE47QUFFSixZQUFLLENBRkQ7QUFHSixhQUFNLENBSEY7QUFJSixrQkFBVyxLQUpQO0FBS0osaUJBQVUsT0FMTjtBQU1KLGlCQUFVLE9BTk47QUFPSix3QkFBaUIsU0FQYjtBQVFKLGVBQVEsTUFSSjtBQVNKLGlCQUFVO0FBVE4sT0FGTixFQWFFLFFBYkYsQ0FhVyxFQUFFLE1BQUYsQ0FiWDtBQWNBOztBQUVELHVCQUFrQixNQUFsQixDQUF5QixRQUFRLEtBQUssU0FBTCxDQUFlLElBQWYsQ0FBUixHQUErQixNQUF4RDtBQUNBOztBQUVEO0FBQ0MsU0FBSSxZQUFZLFNBQWhCLEVBQTJCO0FBQzFCLGFBRDBCLENBQ2xCO0FBQ1I7O0FBRUQsU0FBSSxPQUFPLFFBQVEsYUFBUixFQUF1QixLQUE5QixLQUF3QyxVQUF4QyxJQUFzRCxPQUFPLFFBQVEsR0FBUixDQUFZLEtBQW5CLEtBQTZCLFVBQXZGLEVBQW1HO0FBQ2xHLFVBQUksUUFBUSxhQUFSLE1BQTJCLFNBQS9CLEVBQTBDO0FBQ3pDLGVBQVEsYUFBUixFQUF1QixLQUF2QixDQUE2QixPQUE3QixFQUFzQyxJQUF0QztBQUNBLE9BRkQsTUFFTztBQUNOLGVBQVEsR0FBUixDQUFZLEtBQVosQ0FBa0IsT0FBbEIsRUFBMkIsSUFBM0I7QUFDQTtBQUNELE1BTkQsTUFNTztBQUNOLGNBQVEsR0FBUixDQUFZLElBQVo7QUFDQTtBQXpDSDtBQTJDQTtBQUNEOztBQUVEOzs7QUFHQSxTQUFRLHNCQUFSLEdBQWlDLFlBQVc7QUFDM0MsU0FBTyxPQUFQLEdBQWlCLG1CQUFqQjtBQUNBLEVBRkQ7O0FBSUE7Ozs7O0FBS0EsU0FBUSxLQUFSLEdBQWdCLFlBQVc7QUFDMUIsV0FBUyxVQUFULEVBQXFCLFNBQXJCO0FBQ0EsRUFGRDs7QUFJQTs7Ozs7QUFLQSxTQUFRLElBQVIsR0FBZSxZQUFXO0FBQ3pCLFdBQVMsU0FBVCxFQUFvQixTQUFwQjtBQUNBLEVBRkQ7O0FBSUE7Ozs7O0FBS0EsU0FBUSxHQUFSLEdBQWMsWUFBVztBQUN4QixXQUFTLFFBQVQsRUFBbUIsU0FBbkI7QUFDQSxFQUZEOztBQUlBOzs7OztBQUtBLFNBQVEsSUFBUixHQUFlLFlBQVc7QUFDekIsV0FBUyxTQUFULEVBQW9CLFNBQXBCO0FBQ0EsRUFGRDs7QUFJQTs7Ozs7QUFLQSxTQUFRLEtBQVIsR0FBZ0IsWUFBVztBQUMxQixXQUFTLFVBQVQsRUFBcUIsU0FBckI7QUFDQSxFQUZEOztBQUlBOzs7OztBQUtBLFNBQVEsS0FBUixHQUFnQixZQUFXO0FBQzFCLFdBQVMsVUFBVCxFQUFxQixTQUFyQjtBQUNBLEVBRkQ7O0FBSUE7Ozs7O0FBS0EsU0FBUSxNQUFSLEdBQWlCLFlBQVc7QUFDM0IsV0FBUyxXQUFULEVBQXNCLFNBQXRCO0FBQ0EsRUFGRDtBQUlBLENBdlFBLEVBdVFDLElBQUksSUFBSixDQUFTLEtBdlFWLENBQUQ7Ozs7O0FDcEJBOzs7Ozs7Ozs7O0FBVUEsSUFBSSxJQUFKLENBQVMsTUFBVCxHQUFrQixJQUFJLElBQUosQ0FBUyxNQUFULElBQW1CLEVBQXJDOztBQUVBOzs7Ozs7O0FBT0EsQ0FBQyxVQUFTLE9BQVQsRUFBa0I7O0FBRWxCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FBV0EsVUFBUyxlQUFULENBQXlCLFdBQXpCLEVBQXNDO0FBQ3JDLE1BQUkscUJBQXFCLEVBQXpCOztBQUVBO0FBQ0EsTUFBSSxRQUFRLE1BQU0sSUFBTixDQUFXLFNBQVMsb0JBQVQsQ0FBOEIsR0FBOUIsQ0FBWCxDQUFaO0FBQUEsTUFDQyxRQUFRLHFCQURUOztBQUpxQztBQUFBO0FBQUE7O0FBQUE7QUFPckMsd0JBQWlCLEtBQWpCLDhIQUF3QjtBQUFBLFFBQWYsSUFBZTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUN2QiwyQkFBc0IsTUFBTSxJQUFOLENBQVcsS0FBSyxVQUFoQixDQUF0QixtSUFBbUQ7QUFBQSxVQUExQyxTQUEwQzs7QUFDbEQsVUFBSSxVQUFVLElBQVYsQ0FBZSxNQUFmLENBQXNCLEtBQXRCLE1BQWlDLENBQUMsQ0FBdEMsRUFBeUM7QUFDeEM7QUFDQSxXQUFJLE9BQU8sVUFBVSxJQUFWLENBQWUsT0FBZixDQUF1QixLQUF2QixFQUE4QixJQUE5QixDQUFYO0FBQUEsV0FDQyxTQUFTLFVBQVUsS0FEcEI7O0FBR0E7QUFDQSxXQUFJLG1CQUFtQixPQUFuQixDQUEyQixJQUEzQixJQUFtQyxDQUFDLENBQXhDLEVBQTJDO0FBQzFDLFlBQUksT0FBTyxJQUFQLEVBQWEsTUFBYixLQUF3QixNQUE1QixFQUFvQztBQUNuQyxhQUFJLElBQUosQ0FBUyxLQUFULENBQWUsS0FBZixpREFBbUUsSUFBbkU7QUFDQSxlQUFNLElBQUksS0FBSixDQUFVLG9CQUFrQixJQUFsQiw4RUFBVixDQUFOO0FBRUE7QUFDRCxpQkFOMEMsQ0FNaEM7QUFDVjs7QUFFRCxXQUFJLFdBQVcsRUFBZixFQUFtQjtBQUNsQixjQUFNLElBQUksV0FBSixpQ0FBOEMsSUFBOUMsQ0FBTjtBQUNBOztBQUVEO0FBQ0E7QUFDQSxXQUFJLFNBQVMsS0FBYixFQUFvQjtBQUFFO0FBQ3JCLGtDQUEwQixNQUExQixFQUFrQyxXQUFsQztBQUNBLFFBRkQsTUFFTztBQUNOLGVBQU8sSUFBUCxJQUFlLElBQUksSUFBSSxZQUFKLENBQWlCLFNBQXJCLENBQStCLElBQS9CLEVBQXFDLE1BQXJDLEVBQTZDLFdBQTdDLENBQWY7QUFDQTs7QUFFRCwwQkFBbUIsSUFBbkIsQ0FBd0IsSUFBeEI7QUFDQSxZQUFLLGVBQUwsQ0FBcUIsVUFBVSxJQUEvQjtBQUNBO0FBQ0Q7QUFoQ3NCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFpQ3ZCOztBQUVEO0FBMUNxQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQTJDckMsTUFBSSxtQkFBbUIsTUFBbkIsS0FBOEIsQ0FBbEMsRUFBcUM7QUFDcEMsU0FBTSxJQUFJLEtBQUosQ0FBVSwrRUFDZixtQkFESyxDQUFOO0FBRUE7O0FBRUQ7QUFDQSxNQUFJLHFCQUFxQixFQUF6Qjs7QUFqRHFDO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsUUFtRDVCLElBbkQ0Qjs7QUFvRHBDLFFBQUksV0FBVyxFQUFFLFFBQUYsRUFBZjs7QUFFQSx1QkFBbUIsSUFBbkIsQ0FBd0IsUUFBeEI7O0FBRUEsV0FBTyxJQUFQLEVBQ0UsSUFERixHQUVFLElBRkYsQ0FFTyxTQUFTLE9BRmhCLEVBR0UsSUFIRixDQUdPLFNBQVMsTUFIaEIsRUFJRSxNQUpGLENBSVM7QUFBQSxZQUFPLElBQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxJQUFmLENBQW9CLG9DQUFwQixFQUEwRCxJQUExRCxDQUFQO0FBQUEsS0FKVDtBQXhEb0M7O0FBbURyQyx5QkFBaUIsa0JBQWpCLG1JQUFxQztBQUFBO0FBVXBDOztBQUVEO0FBL0RxQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWdFckMsSUFBRSxJQUFGLENBQU8sS0FBUCxDQUFhLFNBQWIsRUFBd0Isa0JBQXhCLEVBQTRDLE1BQTVDLENBQW1ELFlBQVc7QUFDN0QsT0FBSSxRQUFRLFNBQVMsV0FBVCxDQUFxQixPQUFyQixDQUFaO0FBQ0EsU0FBTSxTQUFOLENBQWdCLHdCQUFoQixFQUEwQyxJQUExQyxFQUFnRCxJQUFoRDtBQUNBLFlBQVMsYUFBVCxDQUF1QixNQUF2QixFQUErQixhQUEvQixDQUE2QyxLQUE3QztBQUNBLE9BQUksSUFBSixDQUFTLFFBQVQsQ0FBa0IsR0FBbEIsQ0FBc0IsWUFBdEIsRUFBb0MsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFwQztBQUNBLE9BQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxJQUFmLENBQW9CLDBCQUFwQixFQUFnRCxJQUFJLElBQUosQ0FBUyxRQUFULENBQWtCLEdBQWxCLENBQXNCLFlBQXRCLElBQzdDLElBQUksSUFBSixDQUFTLFFBQVQsQ0FBa0IsR0FBbEIsQ0FBc0IsY0FBdEIsQ0FESCxFQUMwQyxJQUQxQztBQUVBLE9BQUksT0FBTyxPQUFYLEVBQW9CO0FBQ25CLFdBQU8sUUFBUCxHQUFrQixJQUFsQjtBQUNBO0FBQ0QsR0FWRDs7QUFZQSxTQUFPLGtCQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7OztBQVlBLFVBQVMseUJBQVQsQ0FBbUMsTUFBbkMsRUFBMkMsV0FBM0MsRUFBd0Q7QUFDdkQsTUFBSSxlQUFlLElBQUksSUFBSSxZQUFKLENBQWlCLFNBQXJCLENBQStCLEtBQS9CLEVBQXNDLE1BQXRDLEVBQThDLFdBQTlDLENBQW5CO0FBQ0EsTUFBSSxJQUFKLEdBQVcsYUFBYSxJQUF4QjtBQUNBLE1BQUksTUFBSixHQUFhLGFBQWEsTUFBMUI7QUFDQSxNQUFJLFdBQUosR0FBa0IsYUFBYSxXQUEvQjtBQUNBLE1BQUksSUFBSixHQUFXLElBQUksWUFBSixDQUFpQixTQUFqQixDQUEyQixTQUEzQixDQUFxQyxJQUFoRDtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxTQUFRLElBQVIsR0FBZSxVQUFVLFdBQVYsRUFBdUI7QUFDckM7QUFDQSxNQUFJLElBQUosQ0FBUyxLQUFULENBQWUsc0JBQWY7O0FBRUE7QUFDQSxNQUFJLHFCQUFxQixnQkFBZ0IsV0FBaEIsQ0FBekI7O0FBRUE7QUFDQSxNQUFJLElBQUosQ0FBUyxLQUFULENBQWUsSUFBZixDQUFvQixzQkFBc0IsbUJBQW1CLElBQW5CLEVBQTFDOztBQUVBO0FBQ0EsTUFBSSxJQUFKLENBQVMsUUFBVCxDQUFrQixHQUFsQixDQUFzQixZQUF0QixFQUFvQyxrQkFBcEM7QUFDQSxFQVpEO0FBY0EsQ0E3SUQsRUE2SUcsSUFBSSxJQUFKLENBQVMsTUE3SVo7Ozs7O0FDbkJBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7QUFPQyxhQUFZOztBQUVaOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxHQUFFLEVBQUYsQ0FBSyxNQUFMLENBQVk7QUFDWCxtQkFBaUIseUJBQVMsVUFBVCxFQUFxQjtBQUNyQyxPQUFJLENBQUMsVUFBRCxJQUFlLGVBQWUsRUFBbEMsRUFBc0M7QUFDckMsVUFBTSxJQUFJLEtBQUosQ0FBVSw4Q0FBVixDQUFOO0FBQ0E7O0FBRUQsT0FBTSxjQUFjLEVBQUUsSUFBRixFQUFRLElBQVIsRUFBcEI7QUFDQSxPQUFNLGVBQWUsRUFBckI7O0FBRUE7QUFDQTtBQUNBLEtBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBVSxHQUFWLEVBQWUsS0FBZixFQUFzQjtBQUN6QyxRQUFJLElBQUksT0FBSixDQUFZLFVBQVosTUFBNEIsQ0FBNUIsSUFBaUMsSUFBSSxPQUFKLENBQVksV0FBVyxXQUFYLEVBQVosTUFBMEMsQ0FBL0UsRUFBa0Y7QUFDakYsU0FBSSxTQUFTLElBQUksTUFBSixDQUFXLFdBQVcsTUFBdEIsQ0FBYjtBQUNBLGNBQVMsT0FBTyxNQUFQLENBQWMsQ0FBZCxFQUFpQixDQUFqQixFQUFvQixXQUFwQixLQUFvQyxPQUFPLE1BQVAsQ0FBYyxDQUFkLENBQTdDO0FBQ0Esa0JBQWEsTUFBYixJQUF1QixLQUF2QjtBQUNBO0FBQ0QsSUFORDs7QUFRQSxVQUFPLFlBQVA7QUFDQTtBQXBCVSxFQUFaOztBQXVCQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSSxFQUFFLFVBQUYsS0FBaUIsU0FBckIsRUFBZ0M7QUFDL0IsSUFBRSxVQUFGLENBQWEsUUFBYixDQUFzQixFQUF0QixHQUEyQjtBQUMxQixlQUFZLFVBRGM7QUFFMUIsYUFBVSxDQUZnQjtBQUcxQixVQUFPO0FBSG1CLEdBQTNCO0FBS0EsSUFBRSxVQUFGLENBQWEsV0FBYixDQUF5QixFQUFFLFVBQUYsQ0FBYSxRQUFiLENBQXNCLEVBQS9DO0FBQ0E7QUFDRCxDQTNDQSxHQUFEOzs7QUNqQkE7Ozs7Ozs7Ozs7QUFVQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE2QkE7QUFDQTs7QUFDQSxPQUFPLEdBQVAsR0FBYTtBQUNaLFFBQU0sRUFETTtBQUVaLFFBQU0sRUFGTTtBQUdaLGdCQUFjO0FBSEYsQ0FBYjs7QUFNQTtBQUNBLFNBQVMsZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQThDLFlBQVc7QUFDeEQsTUFBSTtBQUNIO0FBQ0EsUUFBSSxPQUFPLHFCQUFQLEtBQWlDLFNBQXJDLEVBQWdEO0FBQy9DLFlBQU0sSUFBSSxLQUFKLENBQVUsbUZBQ2YsZ0VBREssQ0FBTjtBQUVBOztBQUVEO0FBQ0EsUUFBSSxJQUFKLENBQVMsTUFBVCxDQUFnQixJQUFoQixDQUFxQixPQUFPLHFCQUE1Qjs7QUFFQTtBQUNBLFFBQUksSUFBSixDQUFTLFFBQVQsQ0FBa0IsR0FBbEIsQ0FBc0IsY0FBdEIsRUFBc0MsS0FBSyxHQUFMLEVBQXRDOztBQUVBO0FBQ0EsUUFBSSxJQUFKLENBQVMsTUFBVCxDQUFnQixJQUFoQixDQUFxQixJQUFJLElBQUosQ0FBUyxNQUFULENBQWdCLEdBQWhCLENBQW9CLGFBQXBCLENBQXJCO0FBQ0EsR0FmRCxDQWVFLE9BQU8sU0FBUCxFQUFrQjtBQUNuQixRQUFJLElBQUosQ0FBUyxLQUFULENBQWUsS0FBZixDQUFxQixtREFBckIsRUFBMEUsU0FBMUU7QUFDQTtBQUNBLFFBQU0sUUFBUSxTQUFTLFdBQVQsQ0FBcUIsYUFBckIsQ0FBZDtBQUNBLFVBQU0sZUFBTixDQUFzQixPQUF0QixFQUErQixJQUEvQixFQUFxQyxJQUFyQyxFQUEyQyxTQUEzQztBQUNBLFdBQU8sYUFBUCxDQUFxQixLQUFyQjtBQUNBO0FBQ0QsQ0F2QkQ7Ozs7Ozs7QUNsREE7Ozs7Ozs7Ozs7QUFVQSxJQUFJLElBQUosQ0FBUyxJQUFULEdBQWdCLElBQUksSUFBSixDQUFTLElBQVQsSUFBaUIsRUFBakM7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JDLFdBQVUsT0FBVixFQUFtQjs7QUFFbkI7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNLFdBQVcsRUFBakI7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQVFBLFNBQVEsVUFBUixHQUFxQixVQUFVLElBQVYsRUFBZ0IsWUFBaEIsRUFBOEI7QUFDbEQsTUFBSSxPQUFPLElBQVAsS0FBZ0IsUUFBaEIsSUFBNEIsUUFBTyxZQUFQLHlDQUFPLFlBQVAsT0FBd0IsUUFBcEQsSUFBZ0UsaUJBQWlCLElBQXJGLEVBQTJGO0FBQzFGLFNBQU0sSUFBSSxLQUFKLENBQVUsK0VBQTRFLElBQTVFLHlDQUE0RSxJQUE1RSx3Q0FDMEIsWUFEMUIseUNBQzBCLFlBRDFCLFVBQVYsQ0FBTjtBQUVBO0FBQ0QsV0FBUyxJQUFULElBQWlCLFlBQWpCO0FBQ0EsRUFORDs7QUFRQTs7Ozs7OztBQU9BLFNBQVEsV0FBUixHQUFzQixZQUFZO0FBQ2pDLE1BQU0sU0FBUyxFQUFmOztBQUVBLE9BQUssSUFBSSxPQUFULElBQW9CLFFBQXBCLEVBQThCO0FBQzdCLFVBQU8sSUFBUCxDQUFZLE9BQVo7QUFDQTs7QUFFRCxTQUFPLE1BQVA7QUFDQSxFQVJEOztBQVVBOzs7Ozs7Ozs7OztBQVdBLFNBQVEsU0FBUixHQUFvQixVQUFVLE1BQVYsRUFBa0IsT0FBbEIsRUFBMkI7QUFDOUM7QUFDQSxNQUFJLE9BQU8sTUFBUCxLQUFrQixRQUFsQixJQUE4QixPQUFPLE9BQVAsS0FBbUIsUUFBckQsRUFBK0Q7QUFDOUQsU0FBTSxJQUFJLEtBQUosQ0FBVSxxRUFBa0UsTUFBbEUseUNBQWtFLE1BQWxFLG1DQUNxQixPQURyQix5Q0FDcUIsT0FEckIsVUFBVixDQUFOO0FBRUE7O0FBRUQ7QUFDQSxNQUFJLFNBQVMsT0FBVCxNQUFzQixTQUF0QixJQUFtQyxTQUFTLE9BQVQsRUFBa0IsTUFBbEIsTUFBOEIsU0FBckUsRUFBZ0Y7QUFDL0UsT0FBSSxJQUFKLENBQVMsS0FBVCxDQUFlLElBQWYscURBQXNFLE1BQXRFLG1CQUEwRixPQUExRjtBQUNBLFVBQU8sTUFBTSxPQUFOLEdBQWdCLEdBQWhCLEdBQXNCLE1BQXRCLEdBQStCLEdBQXRDO0FBQ0E7O0FBRUQsU0FBTyxTQUFTLE9BQVQsRUFBa0IsTUFBbEIsQ0FBUDtBQUNBLEVBZEQ7QUFnQkEsQ0EvRUEsRUErRUMsSUFBSSxJQUFKLENBQVMsSUEvRVYsQ0FBRDs7Ozs7QUNyQkE7O0FBR0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBR0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7O0FDN0JBOzs7Ozs7Ozs7O0FBVUEsSUFBSSxJQUFKLENBQVMsYUFBVCxHQUF5QixJQUFJLElBQUosQ0FBUyxhQUFULElBQTBCLEVBQW5EOztBQUVBOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxVQUFVLE9BQVYsRUFBbUI7O0FBRW5COztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7QUFPQSxVQUFTLFFBQVQsQ0FBa0IsR0FBbEIsRUFBdUI7QUFDdEIsTUFBTSxPQUFPLFNBQVMsYUFBVCxDQUF1QixNQUF2QixDQUFiO0FBQ0EsT0FBSyxJQUFMLEdBQVksVUFBWjtBQUNBLE9BQUssR0FBTCxHQUFXLFlBQVg7QUFDQSxPQUFLLElBQUwsR0FBWSxHQUFaO0FBQ0EsV0FBUyxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxFQUF5QyxXQUF6QyxDQUFxRCxJQUFyRDtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7QUFRQSxTQUFRLElBQVIsR0FBZSxZQUFZO0FBQzFCLE1BQUksWUFBWSxFQUFoQjs7QUFFQSxNQUFJLElBQUksSUFBSixDQUFTLE1BQVQsQ0FBZ0IsR0FBaEIsQ0FBb0IsYUFBcEIsTUFBdUMsWUFBdkMsSUFBdUQsSUFBSSxJQUFKLENBQVMsTUFBVCxDQUFnQixHQUFoQixDQUFvQixZQUFwQixDQUEzRCxFQUE4RjtBQUM3Rix5QkFBb0IsSUFBSSxJQUFKLENBQVMsTUFBVCxDQUFnQixHQUFoQixDQUFvQixZQUFwQixDQUFwQjtBQUNBOztBQUVELE1BQU0sU0FBUztBQUNkLFlBQVMsSUFBSSxJQUFKLENBQVMsTUFBVCxDQUFnQixHQUFoQixDQUFvQixRQUFwQixDQURLO0FBRWQsWUFBUyxTQUZLO0FBR2QsWUFBUyxpQkFBVSxLQUFWLEVBQWlCO0FBQ3pCLFFBQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxLQUFmLENBQXFCLGtCQUFyQixFQUF5QyxLQUF6QztBQUNBO0FBTGEsR0FBZjs7QUFRQSxTQUFPLE9BQVAsQ0FBZSxNQUFmLENBQXNCLE1BQXRCO0FBQ0EsRUFoQkQ7O0FBa0JBOzs7Ozs7OztBQVFBLFNBQVEsT0FBUixHQUFrQixVQUFTLFlBQVQsRUFBdUIsUUFBdkIsRUFBaUM7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFDbEQsd0JBQXVCLFlBQXZCLDhIQUFxQztBQUFBLFFBQTVCLFVBQTRCOztBQUNwQyxRQUFJLFdBQVcsUUFBWCxDQUFvQixNQUFwQixDQUFKLEVBQWlDO0FBQ2hDLGNBQVMsVUFBVDtBQUNBLFNBQU0sUUFBUSxhQUFhLE9BQWIsQ0FBcUIsVUFBckIsQ0FBZDtBQUNBLGtCQUFhLE1BQWIsQ0FBb0IsS0FBcEIsRUFBMkIsQ0FBM0I7QUFDQTtBQUNEO0FBUGlEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBU2xELE1BQUksYUFBYSxNQUFiLEtBQXdCLENBQTVCLEVBQStCO0FBQzlCO0FBQ0EsR0FGRCxNQUVPO0FBQ04sVUFBTyxPQUFQLENBQWUsWUFBZixFQUE2QixRQUE3QjtBQUNBO0FBQ0QsRUFkRDs7QUFnQkE7Ozs7Ozs7OztBQVNBLFNBQVEsSUFBUixHQUFlLFVBQVUsUUFBVixFQUFvQixJQUFwQixFQUEwQixVQUExQixFQUFzQztBQUNwRCxNQUFNLFdBQVcsRUFBRSxRQUFGLEVBQWpCOztBQUVBLE1BQUk7QUFDSCxPQUFJLFNBQVMsRUFBYixFQUFpQjtBQUNoQixhQUFTLE1BQVQsQ0FBZ0IsSUFBSSxLQUFKLENBQVUsOEJBQVYsQ0FBaEI7QUFDQTs7QUFFRCxPQUFNLGlCQUFpQixLQUFLLE9BQUwsQ0FBYSxXQUFiLEVBQTBCLElBQTFCLENBQXZCLENBTEcsQ0FLcUQ7O0FBRXhEO0FBQ0EsT0FBTSxTQUFTLFdBQVcsS0FBWCxDQUFpQixPQUFqQixDQUF5QixjQUF6QixDQUFmO0FBQ0EsT0FBSSxVQUFVLE9BQU8sSUFBUCxLQUFnQixVQUE5QixFQUEwQztBQUN6QyxhQUFTLE9BQVQsQ0FBaUIsSUFBSSxJQUFJLFlBQUosQ0FBaUIsTUFBckIsQ0FBNEIsUUFBNUIsRUFBc0MsY0FBdEMsRUFBc0QsVUFBdEQsQ0FBakI7QUFDQSxXQUFPLElBQVAsQ0FGeUMsQ0FFNUI7QUFDYjs7QUFFRDtBQUNBLE9BQU0sZ0JBQWdCLElBQUksSUFBSixDQUFTLE1BQVQsQ0FBZ0IsR0FBaEIsQ0FBb0IsT0FBcEIsTUFBaUMsT0FBakMsR0FBMkMsU0FBM0MsR0FBdUQsS0FBN0U7QUFDQSxPQUFNLE1BQU0sV0FBVyxTQUFYLENBQXFCLE1BQXJCLEdBQThCLEdBQTlCLEdBQW9DLFdBQVcsSUFBL0MsR0FBc0QsR0FBdEQsR0FBNEQsSUFBNUQsR0FBbUUsYUFBL0U7O0FBRUEsVUFBTyxPQUFQLENBQWUsQ0FBQyxHQUFELENBQWYsRUFBc0IsWUFBTTtBQUMzQixRQUFJLFdBQVcsS0FBWCxDQUFpQixPQUFqQixDQUF5QixjQUF6QixNQUE2QyxTQUFqRCxFQUE0RDtBQUMzRCxXQUFNLElBQUksS0FBSixDQUFVLGFBQWEsSUFBYixHQUFvQix5REFBcEIsR0FDRSwwQkFEWixDQUFOO0FBRUE7O0FBRUQ7QUFDQSxRQUFNLGVBQWUsV0FBVyxLQUFYLENBQWlCLE9BQWpCLENBQXlCLGNBQXpCLEVBQXlDLFlBQXpDLENBQXNELEtBQXRELEVBQXJCOztBQUVBLFFBQUksYUFBYSxNQUFiLEtBQXdCLENBQTVCLEVBQStCO0FBQUU7QUFDaEMsY0FBUyxPQUFULENBQWlCLElBQUksSUFBSSxZQUFKLENBQWlCLE1BQXJCLENBQTRCLFFBQTVCLEVBQXNDLGNBQXRDLEVBQXNELFVBQXRELENBQWpCO0FBQ0EsWUFBTyxJQUFQLENBRjhCLENBRWpCO0FBQ2I7O0FBRUQ7QUFDQSxTQUFLLElBQUksS0FBVCxJQUFrQixZQUFsQixFQUFnQztBQUMvQixTQUFNLGFBQWEsYUFBYSxLQUFiLENBQW5COztBQUVBLFNBQUksV0FBVyxPQUFYLENBQW1CLE1BQW5CLE1BQStCLENBQUMsQ0FBcEMsRUFBdUM7QUFDdEMsZUFBUyxVQUFUO0FBQ0EsbUJBQWEsTUFBYixDQUFvQixLQUFwQixFQUE0QixDQUE1QjtBQUNBO0FBQ0E7O0FBRUQ7QUFDQSxTQUFJLFdBQVcsT0FBWCxDQUFtQixNQUFuQixNQUErQixDQUFDLENBQXBDLEVBQXVDO0FBQ3RDLG1CQUFhLEtBQWIsSUFBc0IsSUFBSSxJQUFKLENBQVMsTUFBVCxDQUFnQixHQUFoQixDQUFvQixXQUFwQixJQUFtQyxRQUFuQyxHQUE4QyxVQUE5QyxHQUEyRCxhQUFqRjtBQUNBLE1BRkQsTUFFTyxJQUFJLFdBQVcsTUFBWCxDQUFrQixDQUFDLENBQW5CLE1BQTBCLEtBQTlCLEVBQXFDO0FBQUU7QUFDN0MsbUJBQWEsS0FBYixLQUF1QixhQUF2QjtBQUNBO0FBQ0Q7O0FBRUQsV0FBTyxPQUFQLENBQWUsWUFBZixFQUE2QixZQUFNO0FBQ2xDLGNBQVMsT0FBVCxDQUFpQixJQUFJLElBQUksWUFBSixDQUFpQixNQUFyQixDQUE0QixRQUE1QixFQUFzQyxjQUF0QyxFQUFzRCxVQUF0RCxDQUFqQjtBQUNBLEtBRkQ7QUFHQSxJQW5DRDtBQW9DQSxHQXRERCxDQXNERSxPQUFPLFNBQVAsRUFBa0I7QUFDbkIsWUFBUyxNQUFULENBQWdCLFNBQWhCO0FBQ0E7O0FBRUQsU0FBTyxTQUFTLE9BQVQsRUFBUDtBQUNBLEVBOUREO0FBZ0VBLENBdEpELEVBc0pHLElBQUksSUFBSixDQUFTLGFBdEpaOzs7OztBQ3RCQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7O0FBT0EsQ0FBQyxZQUFZOztBQUVaOztBQUVBO0FBQ0E7O0FBQ0EsS0FBSSxDQUFDLE9BQU8sUUFBUCxDQUFnQixNQUFyQixFQUE2QjtBQUM1QixTQUFPLFFBQVAsQ0FBZ0IsTUFBaEIsR0FBeUIsT0FBTyxRQUFQLENBQWdCLFFBQWhCLEdBQTJCLElBQTNCLEdBQ0EsT0FBTyxRQUFQLENBQWdCLFFBRGhCLElBQzRCLE9BQU8sUUFBUCxDQUFnQixJQUFoQixHQUF1QixNQUFNLE9BQU8sUUFBUCxDQUFnQixJQUE3QyxHQUFvRCxFQURoRixDQUF6QjtBQUVBOztBQUVEO0FBQ0E7QUFDQSxLQUFJLENBQUMsS0FBSyxHQUFWLEVBQWU7QUFDZCxPQUFLLEdBQUwsR0FBVyxTQUFTLEdBQVQsR0FBZTtBQUN6QixVQUFPLElBQUksSUFBSixHQUFXLE9BQVgsRUFBUDtBQUNBLEdBRkQ7QUFHQTtBQUVELENBbkJEOzs7OztBQ2pCQTs7Ozs7Ozs7OztBQVVBLElBQUksSUFBSixDQUFTLFFBQVQsR0FBb0IsSUFBSSxJQUFKLENBQVMsUUFBVCxJQUFxQixFQUF6Qzs7QUFFQTs7Ozs7OztBQU9BLENBQUMsVUFBVSxPQUFWLEVBQW1COztBQUVuQjs7QUFFQTs7Ozs7O0FBS0EsS0FBTSxXQUFXLEVBQWpCOztBQUVBOzs7Ozs7QUFNQSxTQUFRLEdBQVIsR0FBYyxVQUFVLElBQVYsRUFBZ0IsS0FBaEIsRUFBdUI7QUFDcEM7QUFDQTtBQUNBLE1BQUksU0FBUyxJQUFULE1BQW1CLFNBQXZCLEVBQWtDO0FBQ2pDLE9BQUksSUFBSixDQUFTLEtBQVQsQ0FBZSxJQUFmLENBQW9CLHVDQUF1QyxJQUF2QyxHQUE4Qyx3QkFBbEU7QUFDQTs7QUFFRCxXQUFTLElBQVQsSUFBaUIsS0FBakI7QUFDQSxFQVJEOztBQVVBOzs7Ozs7O0FBT0EsU0FBUSxHQUFSLEdBQWMsVUFBVSxJQUFWLEVBQWdCO0FBQzdCLFNBQU8sU0FBUyxJQUFULENBQVA7QUFDQSxFQUZEOztBQUlBOzs7OztBQUtBLFNBQVEsS0FBUixHQUFnQixZQUFZO0FBQzNCLE1BQUksSUFBSSxJQUFKLENBQVMsTUFBVCxDQUFnQixHQUFoQixDQUFvQixhQUFwQixNQUF1QyxhQUEzQyxFQUEwRDtBQUN6RCxPQUFJLElBQUosQ0FBUyxLQUFULENBQWUsR0FBZixDQUFtQixrQkFBbkIsRUFBdUMsUUFBdkM7QUFDQSxHQUZELE1BRU87QUFDTixTQUFNLElBQUksS0FBSixDQUFVLDJEQUFWLENBQU47QUFDQTtBQUNELEVBTkQ7QUFRQSxDQW5ERCxFQW1ERyxJQUFJLElBQUosQ0FBUyxRQW5EWjs7Ozs7Ozs7QUNuQkE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7QUFXQSxDQUFDLFlBQVc7O0FBRVg7QUFDQTtBQUNBOztBQUVBLFFBQU8sU0FBUCxHQUFtQixTQUFuQjtBQUNBLFFBQU8sT0FBUCxHQUFpQixTQUFqQjs7QUFFQSxLQUFJLFlBQUo7QUFDQSxLQUFJLFVBQUo7QUFDQSxLQUFJLGFBQUo7QUFDQSxLQUFJLG9CQUFKO0FBQ0EsS0FBSSxpQkFBSjtBQUNBLEtBQUksWUFBSjtBQUNBLEtBQUksMEJBQUo7QUFDQSxLQUFJLG1CQUFKO0FBQ0EsS0FBSSxnQkFBSjtBQUNBLEtBQUksVUFBVSxRQUFkO0FBQ0EsS0FBSSxpQkFBaUIsT0FBckI7QUFDQSxLQUFJLGdCQUFnQixPQUFwQjtBQUNBLEtBQUksS0FBSyxPQUFPLFNBQWhCO0FBQ0EsS0FBSSxVQUFVLEdBQUcsUUFBakI7QUFDQSxLQUFJLFNBQVMsR0FBRyxjQUFoQjtBQUNBLEtBQUksWUFBWSxDQUFDLEVBQUUsT0FBTyxNQUFQLEtBQWtCLFdBQWxCLElBQWlDLE9BQU8sU0FBUCxLQUFxQixXQUF0RCxJQUFxRSxPQUFPLFFBQTlFLENBQWpCO0FBQ0EsS0FBSSxjQUFjLENBQUMsU0FBRCxJQUFjLE9BQU8sYUFBUCxLQUF5QixXQUF6RDtBQUNBO0FBQ0E7QUFDQSxLQUFJLGNBQWMsYUFBYSxVQUFVLFFBQVYsS0FBdUIsZUFBcEMsR0FBc0QsWUFBdEQsR0FBcUUscUJBQXZGO0FBQ0EsS0FBSSxpQkFBaUIsR0FBckI7QUFDQTtBQUNBLEtBQUksVUFBVSxPQUFPLEtBQVAsS0FBaUIsV0FBakIsSUFBZ0MsTUFBTSxRQUFOLE9BQXFCLGdCQUFuRTtBQUNBLEtBQUksV0FBVyxFQUFmO0FBQ0EsS0FBSSxNQUFNLEVBQVY7QUFDQSxLQUFJLGlCQUFpQixFQUFyQjtBQUNBLEtBQUksaUJBQWlCLEtBQXJCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVMsVUFBVCxDQUFvQixFQUFwQixFQUF3QjtBQUN2QixTQUFPLFFBQVEsSUFBUixDQUFhLEVBQWIsTUFBcUIsbUJBQTVCO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTLE9BQVQsQ0FBaUIsRUFBakIsRUFBcUI7QUFDcEIsU0FBTyxRQUFRLElBQVIsQ0FBYSxFQUFiLE1BQXFCLGdCQUE1QjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVMsSUFBVCxDQUFjLEdBQWQsRUFBbUIsSUFBbkIsRUFBeUI7QUFDeEIsTUFBSSxHQUFKLEVBQVM7QUFDUixRQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksSUFBSSxNQUF4QixFQUFnQyxLQUFLLENBQXJDLEVBQXdDO0FBQ3ZDLFFBQUksSUFBSSxDQUFKLEtBQVUsS0FBSyxJQUFJLENBQUosQ0FBTCxFQUFhLENBQWIsRUFBZ0IsR0FBaEIsQ0FBZCxFQUFvQztBQUNuQztBQUNBO0FBQ0Q7QUFDRDtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVMsV0FBVCxDQUFxQixHQUFyQixFQUEwQixJQUExQixFQUFnQztBQUMvQixNQUFJLEdBQUosRUFBUztBQUNSLE9BQUksVUFBSjtBQUNBLFFBQUssSUFBSSxJQUFJLE1BQUosR0FBYSxDQUF0QixFQUF5QixJQUFJLENBQUMsQ0FBOUIsRUFBaUMsS0FBSyxDQUF0QyxFQUF5QztBQUN4QyxRQUFJLElBQUksQ0FBSixLQUFVLEtBQUssSUFBSSxDQUFKLENBQUwsRUFBYSxDQUFiLEVBQWdCLEdBQWhCLENBQWQsRUFBb0M7QUFDbkM7QUFDQTtBQUNEO0FBQ0Q7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxVQUFTLE9BQVQsQ0FBaUIsR0FBakIsRUFBc0IsSUFBdEIsRUFBNEI7QUFDM0IsU0FBTyxPQUFPLElBQVAsQ0FBWSxHQUFaLEVBQWlCLElBQWpCLENBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7QUFRQSxVQUFTLE1BQVQsQ0FBZ0IsR0FBaEIsRUFBcUIsSUFBckIsRUFBMkI7QUFDMUIsU0FBTyxRQUFRLEdBQVIsRUFBYSxJQUFiLEtBQXNCLElBQUksSUFBSixDQUE3QjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVMsUUFBVCxDQUFrQixHQUFsQixFQUF1QixJQUF2QixFQUE2QjtBQUM1QixNQUFJLGFBQUo7QUFDQSxPQUFLLElBQUwsSUFBYSxHQUFiLEVBQWtCO0FBQ2pCLE9BQUksUUFBUSxHQUFSLEVBQWEsSUFBYixDQUFKLEVBQXdCO0FBQ3ZCLFFBQUksS0FBSyxJQUFJLElBQUosQ0FBTCxFQUFnQixJQUFoQixDQUFKLEVBQTJCO0FBQzFCO0FBQ0E7QUFDRDtBQUNEO0FBQ0Q7O0FBRUQ7Ozs7QUFJQSxVQUFTLEtBQVQsQ0FBZSxNQUFmLEVBQXVCLE1BQXZCLEVBQStCLEtBQS9CLEVBQXNDLGVBQXRDLEVBQXVEO0FBQ3RELE1BQUksTUFBSixFQUFZO0FBQ1gsWUFBUyxNQUFULEVBQWlCLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN0QyxRQUFJLFNBQVMsQ0FBQyxRQUFRLE1BQVIsRUFBZ0IsSUFBaEIsQ0FBZCxFQUFxQztBQUNwQyxTQUFJLG1CQUFtQixRQUFPLEtBQVAseUNBQU8sS0FBUCxPQUFpQixRQUFwQyxJQUFnRCxLQUFoRCxJQUNILENBQUMsUUFBUSxLQUFSLENBREUsSUFDZ0IsQ0FBQyxXQUFXLEtBQVgsQ0FEakIsSUFFSCxFQUFFLGlCQUFpQixNQUFuQixDQUZELEVBRTZCOztBQUU1QixVQUFJLENBQUMsT0FBTyxJQUFQLENBQUwsRUFBbUI7QUFDbEIsY0FBTyxJQUFQLElBQWUsRUFBZjtBQUNBO0FBQ0QsWUFBTSxPQUFPLElBQVAsQ0FBTixFQUFvQixLQUFwQixFQUEyQixLQUEzQixFQUFrQyxlQUFsQztBQUNBLE1BUkQsTUFRTztBQUNOLGFBQU8sSUFBUCxJQUFlLEtBQWY7QUFDQTtBQUNEO0FBQ0QsSUFkRDtBQWVBO0FBQ0QsU0FBTyxNQUFQO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBLFVBQVMsSUFBVCxDQUFjLEdBQWQsRUFBbUIsRUFBbkIsRUFBdUI7QUFDdEIsU0FBTyxZQUFXO0FBQ2pCLFVBQU8sR0FBRyxLQUFILENBQVMsR0FBVCxFQUFjLFNBQWQsQ0FBUDtBQUNBLEdBRkQ7QUFHQTs7QUFFRCxVQUFTLE9BQVQsR0FBbUI7QUFDbEIsU0FBTyxTQUFTLG9CQUFULENBQThCLFFBQTlCLENBQVA7QUFDQTs7QUFFRCxVQUFTLGNBQVQsQ0FBd0IsR0FBeEIsRUFBNkI7QUFDNUIsUUFBTSxHQUFOO0FBQ0E7O0FBRUQ7QUFDQSxVQUFTLFNBQVQsQ0FBbUIsS0FBbkIsRUFBMEI7QUFDekIsTUFBSSxDQUFDLEtBQUwsRUFBWTtBQUNYLFVBQU8sS0FBUDtBQUNBO0FBQ0QsTUFBSSxJQUFJLE1BQVI7QUFDQSxPQUFLLE1BQU0sS0FBTixDQUFZLEdBQVosQ0FBTCxFQUF1QixVQUFTLElBQVQsRUFBZTtBQUNyQyxPQUFJLEVBQUUsSUFBRixDQUFKO0FBQ0EsR0FGRDtBQUdBLFNBQU8sQ0FBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7QUFTQSxVQUFTLFNBQVQsQ0FBbUIsRUFBbkIsRUFBdUIsR0FBdkIsRUFBNEIsR0FBNUIsRUFBaUMsY0FBakMsRUFBaUQ7QUFDaEQsTUFBTSxRQUFRLElBQUksS0FBSixDQUFVLE1BQU0sMENBQU4sR0FBbUQsRUFBN0QsQ0FBZDs7QUFFQSxRQUFNLFdBQU4sR0FBb0IsRUFBcEI7QUFDQSxRQUFNLGNBQU4sR0FBdUIsY0FBdkI7O0FBRUEsTUFBSSxHQUFKLEVBQVM7QUFDUixTQUFNLGFBQU4sR0FBc0IsR0FBdEI7QUFDQTs7QUFFRCxTQUFPLEtBQVA7QUFDQTs7QUFFRCxLQUFJLE9BQU8sT0FBTyxTQUFkLEtBQTRCLFdBQWhDLEVBQTZDO0FBQzVDLE1BQUksV0FBVyxPQUFPLFNBQWxCLENBQUosRUFBa0M7QUFDakM7QUFDQTtBQUNBO0FBQ0QsUUFBTSxPQUFPLFNBQWI7QUFDQSxTQUFPLFNBQVAsR0FBbUIsU0FBbkI7QUFDQTs7QUFFRDtBQUNBLEtBQUksT0FBTyxPQUFPLE9BQWQsS0FBMEIsV0FBMUIsSUFBeUMsQ0FBQyxXQUFXLE9BQU8sT0FBbEIsQ0FBOUMsRUFBMEU7QUFDekU7QUFDQSxRQUFNLE9BQU8sT0FBYjtBQUNBLFNBQU8sT0FBUCxHQUFpQixTQUFqQjtBQUNBOztBQUVELFVBQVMsVUFBVCxDQUFvQixXQUFwQixFQUFpQztBQUNoQyxNQUFJLHNCQUFKO0FBQUEsTUFBbUIsZUFBbkI7QUFBQSxNQUEyQixnQkFBM0I7QUFBQSxNQUFvQyxpQkFBcEM7QUFBQSxNQUNDLDZCQUREO0FBQUEsTUFFQyxVQUFTO0FBQ1I7QUFDQTtBQUNBO0FBQ0EsZ0JBQWEsQ0FKTDtBQUtSLFlBQVMsSUFMRDtBQU1SLFVBQU8sRUFOQztBQU9SLFlBQVMsRUFQRDtBQVFSLFNBQU0sRUFSRTtBQVNSLFNBQU0sRUFURTtBQVVSLFdBQVE7QUFWQSxHQUZWO0FBQUEsTUFjQyxXQUFXLEVBZFo7O0FBZUM7QUFDQTtBQUNBO0FBQ0Esb0JBQWtCLEVBbEJuQjtBQUFBLE1BbUJDLGNBQWMsRUFuQmY7QUFBQSxNQW9CQyxXQUFXLEVBcEJaO0FBQUEsTUFxQkMsV0FBVSxFQXJCWDtBQUFBLE1Bc0JDLGFBQWEsRUF0QmQ7QUFBQSxNQXVCQyxhQUFhLEVBdkJkO0FBQUEsTUF3QkMsaUJBQWlCLENBeEJsQjtBQUFBLE1BeUJDLHNCQUFzQixDQXpCdkI7O0FBMkJBOzs7Ozs7Ozs7OztBQVdBLFdBQVMsUUFBVCxDQUFrQixHQUFsQixFQUF1QjtBQUN0QixPQUFJLFVBQUo7QUFBQSxPQUFPLGFBQVA7QUFDQSxRQUFLLElBQUksQ0FBVCxFQUFZLElBQUksSUFBSSxNQUFwQixFQUE0QixHQUE1QixFQUFpQztBQUNoQyxXQUFPLElBQUksQ0FBSixDQUFQO0FBQ0EsUUFBSSxTQUFTLEdBQWIsRUFBa0I7QUFDakIsU0FBSSxNQUFKLENBQVcsQ0FBWCxFQUFjLENBQWQ7QUFDQSxVQUFLLENBQUw7QUFDQSxLQUhELE1BR08sSUFBSSxTQUFTLElBQWIsRUFBbUI7QUFDekI7QUFDQTtBQUNBO0FBQ0EsU0FBSSxNQUFNLENBQU4sSUFBWSxNQUFNLENBQU4sSUFBVyxJQUFJLENBQUosTUFBVyxJQUFsQyxJQUEyQyxJQUFJLElBQUksQ0FBUixNQUFlLElBQTlELEVBQW9FO0FBQ25FO0FBQ0EsTUFGRCxNQUVPLElBQUksSUFBSSxDQUFSLEVBQVc7QUFDakIsVUFBSSxNQUFKLENBQVcsSUFBSSxDQUFmLEVBQWtCLENBQWxCO0FBQ0EsV0FBSyxDQUFMO0FBQ0E7QUFDRDtBQUNEO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7QUFVQSxXQUFTLFNBQVQsQ0FBbUIsSUFBbkIsRUFBeUIsUUFBekIsRUFBbUMsUUFBbkMsRUFBNkM7QUFDNUMsT0FBSSxnQkFBSjtBQUFBLE9BQWEsaUJBQWI7QUFBQSxPQUF1QixrQkFBdkI7QUFBQSxPQUFrQyxVQUFsQztBQUFBLE9BQXFDLFVBQXJDO0FBQUEsT0FBd0Msb0JBQXhDO0FBQUEsT0FBcUQsa0JBQXJEO0FBQUEsT0FDQyxpQkFERDtBQUFBLE9BQ1csZUFEWDtBQUFBLE9BQ21CLHFCQURuQjtBQUFBLE9BQ2lDLGNBRGpDO0FBQUEsT0FDd0MsNEJBRHhDO0FBQUEsT0FFQyxZQUFhLFlBQVksU0FBUyxLQUFULENBQWUsR0FBZixDQUYxQjtBQUFBLE9BR0MsTUFBTSxRQUFPLEdBSGQ7QUFBQSxPQUlDLFVBQVUsT0FBTyxJQUFJLEdBQUosQ0FKbEI7O0FBTUE7QUFDQSxPQUFJLElBQUosRUFBVTtBQUNULFdBQU8sS0FBSyxLQUFMLENBQVcsR0FBWCxDQUFQO0FBQ0EsZ0JBQVksS0FBSyxNQUFMLEdBQWMsQ0FBMUI7O0FBRUE7QUFDQTtBQUNBLFFBQUksUUFBTyxZQUFQLElBQXVCLGVBQWUsSUFBZixDQUFvQixLQUFLLFNBQUwsQ0FBcEIsQ0FBM0IsRUFBaUU7QUFDaEUsVUFBSyxTQUFMLElBQWtCLEtBQUssU0FBTCxFQUFnQixPQUFoQixDQUF3QixjQUF4QixFQUF3QyxFQUF4QyxDQUFsQjtBQUNBOztBQUVEO0FBQ0EsUUFBSSxLQUFLLENBQUwsRUFBUSxNQUFSLENBQWUsQ0FBZixNQUFzQixHQUF0QixJQUE2QixTQUFqQyxFQUE0QztBQUMzQztBQUNBO0FBQ0E7QUFDQSwyQkFBc0IsVUFBVSxLQUFWLENBQWdCLENBQWhCLEVBQW1CLFVBQVUsTUFBVixHQUFtQixDQUF0QyxDQUF0QjtBQUNBLFlBQU8sb0JBQW9CLE1BQXBCLENBQTJCLElBQTNCLENBQVA7QUFDQTs7QUFFRCxhQUFTLElBQVQ7QUFDQSxXQUFPLEtBQUssSUFBTCxDQUFVLEdBQVYsQ0FBUDtBQUNBOztBQUVEO0FBQ0EsT0FBSSxZQUFZLEdBQVosS0FBb0IsYUFBYSxPQUFqQyxDQUFKLEVBQStDO0FBQzlDLGdCQUFZLEtBQUssS0FBTCxDQUFXLEdBQVgsQ0FBWjs7QUFFQSxlQUFXLEtBQUssSUFBSSxVQUFVLE1BQW5CLEVBQTJCLElBQUksQ0FBL0IsRUFBa0MsS0FBSyxDQUF2QyxFQUEwQztBQUNwRCxtQkFBYyxVQUFVLEtBQVYsQ0FBZ0IsQ0FBaEIsRUFBbUIsQ0FBbkIsRUFBc0IsSUFBdEIsQ0FBMkIsR0FBM0IsQ0FBZDs7QUFFQSxTQUFJLFNBQUosRUFBZTtBQUNkO0FBQ0E7QUFDQSxXQUFLLElBQUksVUFBVSxNQUFuQixFQUEyQixJQUFJLENBQS9CLEVBQWtDLEtBQUssQ0FBdkMsRUFBMEM7QUFDekMsa0JBQVcsT0FBTyxHQUFQLEVBQVksVUFBVSxLQUFWLENBQWdCLENBQWhCLEVBQW1CLENBQW5CLEVBQXNCLElBQXRCLENBQTJCLEdBQTNCLENBQVosQ0FBWDs7QUFFQTtBQUNBLFdBQUksUUFBSixFQUFjO0FBQ2IsbUJBQVcsT0FBTyxRQUFQLEVBQWlCLFdBQWpCLENBQVg7QUFDQSxZQUFJLFFBQUosRUFBYztBQUNiO0FBQ0Esb0JBQVcsUUFBWDtBQUNBLGtCQUFTLENBQVQ7QUFDQSxlQUFNLFNBQU47QUFDQTtBQUNEO0FBQ0Q7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsU0FBSSxDQUFDLFlBQUQsSUFBaUIsT0FBakIsSUFBNEIsT0FBTyxPQUFQLEVBQWdCLFdBQWhCLENBQWhDLEVBQThEO0FBQzdELHFCQUFlLE9BQU8sT0FBUCxFQUFnQixXQUFoQixDQUFmO0FBQ0EsY0FBUSxDQUFSO0FBQ0E7QUFDRDs7QUFFRCxRQUFJLENBQUMsUUFBRCxJQUFhLFlBQWpCLEVBQStCO0FBQzlCLGdCQUFXLFlBQVg7QUFDQSxjQUFTLEtBQVQ7QUFDQTs7QUFFRCxRQUFJLFFBQUosRUFBYztBQUNiLGVBQVUsTUFBVixDQUFpQixDQUFqQixFQUFvQixNQUFwQixFQUE0QixRQUE1QjtBQUNBLFlBQU8sVUFBVSxJQUFWLENBQWUsR0FBZixDQUFQO0FBQ0E7QUFDRDs7QUFFRDtBQUNBLGFBQVUsT0FBTyxRQUFPLElBQWQsRUFBb0IsSUFBcEIsQ0FBVjs7QUFFQSxVQUFPLFVBQVUsT0FBVixHQUFvQixJQUEzQjtBQUNBOztBQUVELFdBQVMsWUFBVCxDQUFzQixJQUF0QixFQUE0QjtBQUMzQixPQUFJLFNBQUosRUFBZTtBQUNkLFNBQUssU0FBTCxFQUFnQixVQUFTLFVBQVQsRUFBcUI7QUFDcEMsU0FBSSxXQUFXLFlBQVgsQ0FBd0Isb0JBQXhCLE1BQWtELElBQWxELElBQ0gsV0FBVyxZQUFYLENBQXdCLHFCQUF4QixNQUFtRCxRQUFRLFdBRDVELEVBQ3lFO0FBQ3hFLGlCQUFXLFVBQVgsQ0FBc0IsV0FBdEIsQ0FBa0MsVUFBbEM7QUFDQSxhQUFPLElBQVA7QUFDQTtBQUNELEtBTkQ7QUFPQTtBQUNEOztBQUVELFdBQVMsZUFBVCxDQUF5QixFQUF6QixFQUE2QjtBQUM1QixPQUFJLGFBQWEsT0FBTyxRQUFPLEtBQWQsRUFBcUIsRUFBckIsQ0FBakI7QUFDQSxPQUFJLGNBQWMsUUFBUSxVQUFSLENBQWQsSUFBcUMsV0FBVyxNQUFYLEdBQW9CLENBQTdELEVBQWdFO0FBQy9EO0FBQ0EsZUFBVyxLQUFYO0FBQ0EsWUFBUSxPQUFSLENBQWdCLEtBQWhCLENBQXNCLEVBQXRCOztBQUVBO0FBQ0EsWUFBUSxXQUFSLENBQW9CLElBQXBCLEVBQTBCO0FBQ3pCLGNBQVM7QUFEZ0IsS0FBMUIsRUFFRyxDQUFDLEVBQUQsQ0FGSDs7QUFJQSxXQUFPLElBQVA7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQSxXQUFTLFdBQVQsQ0FBcUIsSUFBckIsRUFBMkI7QUFDMUIsT0FBSSxlQUFKO0FBQUEsT0FDQyxRQUFRLE9BQU8sS0FBSyxPQUFMLENBQWEsR0FBYixDQUFQLEdBQTJCLENBQUMsQ0FEckM7QUFFQSxPQUFJLFFBQVEsQ0FBQyxDQUFiLEVBQWdCO0FBQ2YsYUFBUyxLQUFLLFNBQUwsQ0FBZSxDQUFmLEVBQWtCLEtBQWxCLENBQVQ7QUFDQSxXQUFPLEtBQUssU0FBTCxDQUFlLFFBQVEsQ0FBdkIsRUFBMEIsS0FBSyxNQUEvQixDQUFQO0FBQ0E7QUFDRCxVQUFPLENBQUMsTUFBRCxFQUFTLElBQVQsQ0FBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7Ozs7QUFZQSxXQUFTLGFBQVQsQ0FBdUIsSUFBdkIsRUFBNkIsZUFBN0IsRUFBOEMsWUFBOUMsRUFBNEQsUUFBNUQsRUFBc0U7QUFDckUsT0FBSSxZQUFKO0FBQUEsT0FBUyxxQkFBVDtBQUFBLE9BQXVCLGVBQXZCO0FBQUEsT0FBK0Isa0JBQS9CO0FBQUEsT0FDQyxTQUFTLElBRFY7QUFBQSxPQUVDLGFBQWEsa0JBQWtCLGdCQUFnQixJQUFsQyxHQUF5QyxJQUZ2RDtBQUFBLE9BR0MsZUFBZSxJQUhoQjtBQUFBLE9BSUMsV0FBVyxJQUpaO0FBQUEsT0FLQyxpQkFBaUIsRUFMbEI7O0FBT0E7QUFDQTtBQUNBLE9BQUksQ0FBQyxJQUFMLEVBQVc7QUFDVixlQUFXLEtBQVg7QUFDQSxXQUFPLFNBQVMsa0JBQWtCLENBQTNCLENBQVA7QUFDQTs7QUFFRCxlQUFZLFlBQVksSUFBWixDQUFaO0FBQ0EsWUFBUyxVQUFVLENBQVYsQ0FBVDtBQUNBLFVBQU8sVUFBVSxDQUFWLENBQVA7O0FBRUEsT0FBSSxNQUFKLEVBQVk7QUFDWCxhQUFTLFVBQVUsTUFBVixFQUFrQixVQUFsQixFQUE4QixRQUE5QixDQUFUO0FBQ0EsbUJBQWUsT0FBTyxRQUFQLEVBQWdCLE1BQWhCLENBQWY7QUFDQTs7QUFFRDtBQUNBLE9BQUksSUFBSixFQUFVO0FBQ1QsUUFBSSxNQUFKLEVBQVk7QUFDWCxTQUFJLGdCQUFnQixhQUFhLFNBQWpDLEVBQTRDO0FBQzNDO0FBQ0EsdUJBQWlCLGFBQWEsU0FBYixDQUF1QixJQUF2QixFQUE2QixVQUFTLElBQVQsRUFBZTtBQUM1RCxjQUFPLFVBQVUsSUFBVixFQUFnQixVQUFoQixFQUE0QixRQUE1QixDQUFQO0FBQ0EsT0FGZ0IsQ0FBakI7QUFHQSxNQUxELE1BS087QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUFpQixLQUFLLE9BQUwsQ0FBYSxHQUFiLE1BQXNCLENBQUMsQ0FBdkIsR0FDQSxVQUFVLElBQVYsRUFBZ0IsVUFBaEIsRUFBNEIsUUFBNUIsQ0FEQSxHQUVBLElBRmpCO0FBR0E7QUFDRCxLQWxCRCxNQWtCTztBQUNOO0FBQ0Esc0JBQWlCLFVBQVUsSUFBVixFQUFnQixVQUFoQixFQUE0QixRQUE1QixDQUFqQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBWSxZQUFZLGNBQVosQ0FBWjtBQUNBLGNBQVMsVUFBVSxDQUFWLENBQVQ7QUFDQSxzQkFBaUIsVUFBVSxDQUFWLENBQWpCO0FBQ0Esb0JBQWUsSUFBZjs7QUFFQSxXQUFNLFFBQVEsU0FBUixDQUFrQixjQUFsQixDQUFOO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxZQUFTLFVBQVUsQ0FBQyxZQUFYLElBQTJCLENBQUMsWUFBNUIsR0FDQSxtQkFBbUIsdUJBQXVCLENBQTFDLENBREEsR0FFQSxFQUZUOztBQUlBLFVBQU87QUFDTixZQUFRLE1BREY7QUFFTixVQUFNLGNBRkE7QUFHTixlQUFXLGVBSEw7QUFJTixrQkFBYyxDQUFDLENBQUMsTUFKVjtBQUtOLFNBQUssR0FMQztBQU1OLGtCQUFjLFlBTlI7QUFPTixjQUFVLFFBUEo7QUFRTixRQUFJLENBQUMsU0FDQSxTQUFTLEdBQVQsR0FBZSxjQURmLEdBRUEsY0FGRCxJQUVtQjtBQVZqQixJQUFQO0FBWUE7O0FBRUQsV0FBUyxTQUFULENBQW1CLE1BQW5CLEVBQTJCO0FBQzFCLE9BQUksS0FBSyxPQUFPLEVBQWhCO0FBQUEsT0FDQyxNQUFNLE9BQU8sUUFBUCxFQUFpQixFQUFqQixDQURQOztBQUdBLE9BQUksQ0FBQyxHQUFMLEVBQVU7QUFDVCxVQUFNLFNBQVMsRUFBVCxJQUFlLElBQUksUUFBUSxNQUFaLENBQW1CLE1BQW5CLENBQXJCO0FBQ0E7O0FBRUQsVUFBTyxHQUFQO0FBQ0E7O0FBRUQsV0FBUyxFQUFULENBQVksTUFBWixFQUFvQixJQUFwQixFQUEwQixFQUExQixFQUE4QjtBQUM3QixPQUFJLEtBQUssT0FBTyxFQUFoQjtBQUFBLE9BQ0MsTUFBTSxPQUFPLFFBQVAsRUFBaUIsRUFBakIsQ0FEUDs7QUFHQSxPQUFJLFFBQVEsUUFBUixFQUFpQixFQUFqQixNQUNGLENBQUMsR0FBRCxJQUFRLElBQUksa0JBRFYsQ0FBSixFQUNtQztBQUNsQyxRQUFJLFNBQVMsU0FBYixFQUF3QjtBQUN2QixRQUFHLFNBQVEsRUFBUixDQUFIO0FBQ0E7QUFDRCxJQUxELE1BS087QUFDTixVQUFNLFVBQVUsTUFBVixDQUFOO0FBQ0EsUUFBSSxJQUFJLEtBQUosSUFBYSxTQUFTLE9BQTFCLEVBQW1DO0FBQ2xDLFFBQUcsSUFBSSxLQUFQO0FBQ0EsS0FGRCxNQUVPO0FBQ04sU0FBSSxFQUFKLENBQU8sSUFBUCxFQUFhLEVBQWI7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQsV0FBUyxPQUFULENBQWlCLEdBQWpCLEVBQXNCLE9BQXRCLEVBQStCO0FBQzlCLE9BQUksTUFBTSxJQUFJLGNBQWQ7QUFBQSxPQUNDLFdBQVcsS0FEWjs7QUFHQSxPQUFJLE9BQUosRUFBYTtBQUNaLFlBQVEsR0FBUjtBQUNBLElBRkQsTUFFTztBQUNOLFNBQUssR0FBTCxFQUFVLFVBQVMsRUFBVCxFQUFhO0FBQ3RCLFNBQUksTUFBTSxPQUFPLFFBQVAsRUFBaUIsRUFBakIsQ0FBVjtBQUNBLFNBQUksR0FBSixFQUFTO0FBQ1I7QUFDQSxVQUFJLEtBQUosR0FBWSxHQUFaO0FBQ0EsVUFBSSxJQUFJLE1BQUosQ0FBVyxLQUFmLEVBQXNCO0FBQ3JCLGtCQUFXLElBQVg7QUFDQSxXQUFJLElBQUosQ0FBUyxPQUFULEVBQWtCLEdBQWxCO0FBQ0E7QUFDRDtBQUNELEtBVkQ7O0FBWUEsUUFBSSxDQUFDLFFBQUwsRUFBZTtBQUNkLFNBQUksT0FBSixDQUFZLEdBQVo7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQ7Ozs7QUFJQSxXQUFTLGVBQVQsR0FBMkI7QUFDMUI7QUFDQSxPQUFJLGVBQWUsTUFBbkIsRUFBMkI7QUFDMUIsU0FBSyxjQUFMLEVBQXFCLFVBQVMsU0FBVCxFQUFvQjtBQUN4QyxTQUFJLEtBQUssVUFBVSxDQUFWLENBQVQ7QUFDQSxTQUFJLE9BQU8sRUFBUCxLQUFjLFFBQWxCLEVBQTRCO0FBQzNCLGNBQVEsV0FBUixDQUFvQixFQUFwQixJQUEwQixJQUExQjtBQUNBO0FBQ0QsY0FBUyxJQUFULENBQWMsU0FBZDtBQUNBLEtBTkQ7QUFPQSxxQkFBaUIsRUFBakI7QUFDQTtBQUNEOztBQUVELGFBQVc7QUFDVixjQUFXLGlCQUFTLEdBQVQsRUFBYztBQUN4QixRQUFJLElBQUksT0FBUixFQUFpQjtBQUNoQixZQUFPLElBQUksT0FBWDtBQUNBLEtBRkQsTUFFTztBQUNOLFlBQVEsSUFBSSxPQUFKLEdBQWMsUUFBUSxXQUFSLENBQW9CLElBQUksR0FBeEIsQ0FBdEI7QUFDQTtBQUNELElBUFM7QUFRVixjQUFXLGlCQUFTLEdBQVQsRUFBYztBQUN4QixRQUFJLFlBQUosR0FBbUIsSUFBbkI7QUFDQSxRQUFJLElBQUksR0FBSixDQUFRLFFBQVosRUFBc0I7QUFDckIsU0FBSSxJQUFJLE9BQVIsRUFBaUI7QUFDaEIsYUFBUSxTQUFRLElBQUksR0FBSixDQUFRLEVBQWhCLElBQXNCLElBQUksT0FBbEM7QUFDQSxNQUZELE1BRU87QUFDTixhQUFRLElBQUksT0FBSixHQUFjLFNBQVEsSUFBSSxHQUFKLENBQVEsRUFBaEIsSUFBc0IsRUFBNUM7QUFDQTtBQUNEO0FBQ0QsSUFqQlM7QUFrQlYsYUFBVSxnQkFBUyxHQUFULEVBQWM7QUFDdkIsUUFBSSxJQUFJLE1BQVIsRUFBZ0I7QUFDZixZQUFPLElBQUksTUFBWDtBQUNBLEtBRkQsTUFFTztBQUNOLFlBQVEsSUFBSSxNQUFKLEdBQWE7QUFDcEIsVUFBSSxJQUFJLEdBQUosQ0FBUSxFQURRO0FBRXBCLFdBQUssSUFBSSxHQUFKLENBQVEsR0FGTztBQUdwQixjQUFRLGtCQUFXO0FBQ2xCLGNBQU8sT0FBTyxRQUFPLE1BQWQsRUFBc0IsSUFBSSxHQUFKLENBQVEsRUFBOUIsS0FBcUMsRUFBNUM7QUFDQSxPQUxtQjtBQU1wQixlQUFTLElBQUksT0FBSixLQUFnQixJQUFJLE9BQUosR0FBYyxFQUE5QjtBQU5XLE1BQXJCO0FBUUE7QUFDRDtBQS9CUyxHQUFYOztBQWtDQSxXQUFTLGFBQVQsQ0FBdUIsRUFBdkIsRUFBMkI7QUFDMUI7QUFDQSxVQUFPLFNBQVMsRUFBVCxDQUFQO0FBQ0EsVUFBTyxnQkFBZ0IsRUFBaEIsQ0FBUDtBQUNBOztBQUVELFdBQVMsVUFBVCxDQUFvQixHQUFwQixFQUF5QixNQUF6QixFQUFpQyxTQUFqQyxFQUE0QztBQUMzQyxPQUFJLEtBQUssSUFBSSxHQUFKLENBQVEsRUFBakI7O0FBRUEsT0FBSSxJQUFJLEtBQVIsRUFBZTtBQUNkLFFBQUksSUFBSixDQUFTLE9BQVQsRUFBa0IsSUFBSSxLQUF0QjtBQUNBLElBRkQsTUFFTztBQUNOLFdBQU8sRUFBUCxJQUFhLElBQWI7QUFDQSxTQUFLLElBQUksT0FBVCxFQUFrQixVQUFTLE1BQVQsRUFBaUIsQ0FBakIsRUFBb0I7QUFDckMsU0FBSSxRQUFRLE9BQU8sRUFBbkI7QUFBQSxTQUNDLE1BQU0sT0FBTyxRQUFQLEVBQWlCLEtBQWpCLENBRFA7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFJLE9BQU8sQ0FBQyxJQUFJLFVBQUosQ0FBZSxDQUFmLENBQVIsSUFBNkIsQ0FBQyxVQUFVLEtBQVYsQ0FBbEMsRUFBb0Q7QUFDbkQsVUFBSSxPQUFPLE1BQVAsRUFBZSxLQUFmLENBQUosRUFBMkI7QUFDMUIsV0FBSSxTQUFKLENBQWMsQ0FBZCxFQUFpQixTQUFRLEtBQVIsQ0FBakI7QUFDQSxXQUFJLEtBQUosR0FGMEIsQ0FFYjtBQUNiLE9BSEQsTUFHTztBQUNOLGtCQUFXLEdBQVgsRUFBZ0IsTUFBaEIsRUFBd0IsU0FBeEI7QUFDQTtBQUNEO0FBQ0QsS0FoQkQ7QUFpQkEsY0FBVSxFQUFWLElBQWdCLElBQWhCO0FBQ0E7QUFDRDs7QUFFRCxXQUFTLFdBQVQsR0FBdUI7QUFDdEIsT0FBSSxZQUFKO0FBQUEsT0FBUywwQkFBVDtBQUFBLE9BQ0MsZUFBZSxRQUFPLFdBQVAsR0FBcUIsSUFEckM7O0FBRUM7QUFDQSxhQUFVLGdCQUFpQixRQUFRLFNBQVIsR0FBb0IsWUFBckIsR0FBcUMsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUhoRTtBQUFBLE9BSUMsVUFBVSxFQUpYO0FBQUEsT0FLQyxXQUFXLEVBTFo7QUFBQSxPQU1DLGVBQWUsS0FOaEI7QUFBQSxPQU9DLGlCQUFpQixJQVBsQjs7QUFTQTtBQUNBLE9BQUksYUFBSixFQUFtQjtBQUNsQjtBQUNBOztBQUVELG1CQUFnQixJQUFoQjs7QUFFQTtBQUNBLFlBQVMsZUFBVCxFQUEwQixVQUFTLEdBQVQsRUFBYztBQUN2QyxRQUFJLE1BQU0sSUFBSSxHQUFkO0FBQUEsUUFDQyxRQUFRLElBQUksRUFEYjs7QUFHQTtBQUNBLFFBQUksQ0FBQyxJQUFJLE9BQVQsRUFBa0I7QUFDakI7QUFDQTs7QUFFRCxRQUFJLENBQUMsSUFBSSxRQUFULEVBQW1CO0FBQ2xCLGNBQVMsSUFBVCxDQUFjLEdBQWQ7QUFDQTs7QUFFRCxRQUFJLENBQUMsSUFBSSxLQUFULEVBQWdCO0FBQ2Y7QUFDQTtBQUNBLFNBQUksQ0FBQyxJQUFJLE1BQUwsSUFBZSxPQUFuQixFQUE0QjtBQUMzQixVQUFJLGdCQUFnQixLQUFoQixDQUFKLEVBQTRCO0FBQzNCLDJCQUFvQixJQUFwQjtBQUNBLHNCQUFlLElBQWY7QUFDQSxPQUhELE1BR087QUFDTixlQUFRLElBQVIsQ0FBYSxLQUFiO0FBQ0Esb0JBQWEsS0FBYjtBQUNBO0FBQ0QsTUFSRCxNQVFPLElBQUksQ0FBQyxJQUFJLE1BQUwsSUFBZSxJQUFJLE9BQW5CLElBQThCLElBQUksUUFBdEMsRUFBZ0Q7QUFDdEQscUJBQWUsSUFBZjtBQUNBLFVBQUksQ0FBQyxJQUFJLE1BQVQsRUFBaUI7QUFDaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQVEsaUJBQWlCLEtBQXpCO0FBQ0E7QUFDRDtBQUNEO0FBQ0QsSUFwQ0Q7O0FBc0NBLE9BQUksV0FBVyxRQUFRLE1BQXZCLEVBQStCO0FBQzlCO0FBQ0EsVUFBTSxVQUFVLFNBQVYsRUFBcUIsK0JBQStCLE9BQXBELEVBQTZELElBQTdELEVBQW1FLE9BQW5FLENBQU47QUFDQSxRQUFJLFdBQUosR0FBa0IsUUFBUSxXQUExQjtBQUNBLFdBQU8sUUFBUSxHQUFSLENBQVA7QUFDQTs7QUFFRDtBQUNBLE9BQUksY0FBSixFQUFvQjtBQUNuQixTQUFLLFFBQUwsRUFBZSxVQUFTLEdBQVQsRUFBYztBQUM1QixnQkFBVyxHQUFYLEVBQWdCLEVBQWhCLEVBQW9CLEVBQXBCO0FBQ0EsS0FGRDtBQUdBOztBQUVEO0FBQ0E7QUFDQTtBQUNBLE9BQUksQ0FBQyxDQUFDLE9BQUQsSUFBWSxpQkFBYixLQUFtQyxZQUF2QyxFQUFxRDtBQUNwRDtBQUNBO0FBQ0EsUUFBSSxDQUFDLGFBQWEsV0FBZCxLQUE4QixDQUFDLG9CQUFuQyxFQUF5RDtBQUN4RCw0QkFBdUIsV0FBVyxZQUFXO0FBQzVDLDZCQUF1QixDQUF2QjtBQUNBO0FBQ0EsTUFIc0IsRUFHcEIsRUFIb0IsQ0FBdkI7QUFJQTtBQUNEOztBQUVELG1CQUFnQixLQUFoQjtBQUNBOztBQUVELFdBQVMsZ0JBQVMsR0FBVCxFQUFjO0FBQ3RCLFFBQUssTUFBTCxHQUFjLE9BQU8sV0FBUCxFQUFvQixJQUFJLEVBQXhCLEtBQStCLEVBQTdDO0FBQ0EsUUFBSyxHQUFMLEdBQVcsR0FBWDtBQUNBLFFBQUssSUFBTCxHQUFZLE9BQU8sUUFBTyxJQUFkLEVBQW9CLElBQUksRUFBeEIsQ0FBWjtBQUNBLFFBQUssVUFBTCxHQUFrQixFQUFsQjtBQUNBLFFBQUssT0FBTCxHQUFlLEVBQWY7QUFDQSxRQUFLLFVBQUwsR0FBa0IsRUFBbEI7QUFDQSxRQUFLLFVBQUwsR0FBa0IsRUFBbEI7QUFDQSxRQUFLLFFBQUwsR0FBZ0IsQ0FBaEI7O0FBRUE7Ozs7QUFJQSxHQWREOztBQWdCQSxTQUFPLFNBQVAsR0FBbUI7QUFDbEIsU0FBTSxjQUFTLE9BQVQsRUFBa0IsT0FBbEIsRUFBMkIsT0FBM0IsRUFBb0MsT0FBcEMsRUFBNkM7QUFDbEQsY0FBVSxXQUFXLEVBQXJCOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFFBQUksS0FBSyxNQUFULEVBQWlCO0FBQ2hCO0FBQ0E7O0FBRUQsU0FBSyxPQUFMLEdBQWUsT0FBZjs7QUFFQSxRQUFJLE9BQUosRUFBYTtBQUNaO0FBQ0EsVUFBSyxFQUFMLENBQVEsT0FBUixFQUFpQixPQUFqQjtBQUNBLEtBSEQsTUFHTyxJQUFJLEtBQUssTUFBTCxDQUFZLEtBQWhCLEVBQXVCO0FBQzdCO0FBQ0E7QUFDQSxlQUFVLEtBQUssSUFBTCxFQUFXLFVBQVMsR0FBVCxFQUFjO0FBQ2xDLFdBQUssSUFBTCxDQUFVLE9BQVYsRUFBbUIsR0FBbkI7QUFDQSxNQUZTLENBQVY7QUFHQTs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBSyxPQUFMLEdBQWUsV0FBVyxRQUFRLEtBQVIsQ0FBYyxDQUFkLENBQTFCOztBQUVBLFNBQUssT0FBTCxHQUFlLE9BQWY7O0FBRUE7QUFDQSxTQUFLLE1BQUwsR0FBYyxJQUFkOztBQUVBLFNBQUssTUFBTCxHQUFjLFFBQVEsTUFBdEI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJLFFBQVEsT0FBUixJQUFtQixLQUFLLE9BQTVCLEVBQXFDO0FBQ3BDO0FBQ0E7QUFDQSxVQUFLLE1BQUw7QUFDQSxLQUpELE1BSU87QUFDTixVQUFLLEtBQUw7QUFDQTtBQUNELElBakRpQjs7QUFtRGxCLGNBQVcsbUJBQVMsQ0FBVCxFQUFZLFVBQVosRUFBd0I7QUFDbEM7QUFDQTtBQUNBLFFBQUksQ0FBQyxLQUFLLFVBQUwsQ0FBZ0IsQ0FBaEIsQ0FBTCxFQUF5QjtBQUN4QixVQUFLLFVBQUwsQ0FBZ0IsQ0FBaEIsSUFBcUIsSUFBckI7QUFDQSxVQUFLLFFBQUwsSUFBaUIsQ0FBakI7QUFDQSxVQUFLLFVBQUwsQ0FBZ0IsQ0FBaEIsSUFBcUIsVUFBckI7QUFDQTtBQUNELElBM0RpQjs7QUE2RGxCLFVBQU8saUJBQVc7QUFDakIsUUFBSSxLQUFLLE9BQVQsRUFBa0I7QUFDakI7QUFDQTtBQUNELFNBQUssT0FBTCxHQUFlLElBQWY7O0FBRUEsWUFBUSxTQUFSLEdBQXFCLElBQUksSUFBSixFQUFELENBQWEsT0FBYixFQUFwQjs7QUFFQSxRQUFJLE1BQU0sS0FBSyxHQUFmOztBQUVBO0FBQ0E7QUFDQSxRQUFJLEtBQUssSUFBVCxFQUFlO0FBQ2QsYUFBUSxXQUFSLENBQW9CLEtBQUssR0FBekIsRUFBOEI7QUFDN0IsMkJBQXFCO0FBRFEsTUFBOUIsRUFFRyxLQUFLLElBQUwsQ0FBVSxJQUFWLElBQWtCLEVBRnJCLEVBRXlCLEtBQUssSUFBTCxFQUFXLFlBQVc7QUFDOUMsYUFBTyxJQUFJLE1BQUosR0FBYSxLQUFLLFVBQUwsRUFBYixHQUFpQyxLQUFLLElBQUwsRUFBeEM7QUFDQSxNQUZ3QixDQUZ6QjtBQUtBLEtBTkQsTUFNTztBQUNOO0FBQ0EsWUFBTyxJQUFJLE1BQUosR0FBYSxLQUFLLFVBQUwsRUFBYixHQUFpQyxLQUFLLElBQUwsRUFBeEM7QUFDQTtBQUNELElBbkZpQjs7QUFxRmxCLFNBQU0sZ0JBQVc7QUFDaEIsUUFBSSxNQUFNLEtBQUssR0FBTCxDQUFTLEdBQW5COztBQUVBO0FBQ0EsUUFBSSxDQUFDLFdBQVcsR0FBWCxDQUFMLEVBQXNCO0FBQ3JCLGdCQUFXLEdBQVgsSUFBa0IsSUFBbEI7QUFDQSxhQUFRLElBQVIsQ0FBYSxLQUFLLEdBQUwsQ0FBUyxFQUF0QixFQUEwQixHQUExQjtBQUNBO0FBQ0QsSUE3RmlCOztBQStGbEI7Ozs7QUFJQSxVQUFPLGlCQUFXO0FBQ2pCLFFBQUksQ0FBQyxLQUFLLE9BQU4sSUFBaUIsS0FBSyxRQUExQixFQUFvQztBQUNuQztBQUNBOztBQUVELFFBQUksWUFBSjtBQUFBLFFBQVMsa0JBQVQ7QUFBQSxRQUNDLEtBQUssS0FBSyxHQUFMLENBQVMsRUFEZjtBQUFBLFFBRUMsYUFBYSxLQUFLLFVBRm5CO0FBQUEsUUFHQyxVQUFVLEtBQUssT0FIaEI7QUFBQSxRQUlDLFVBQVUsS0FBSyxPQUpoQjs7QUFNQSxRQUFJLENBQUMsS0FBSyxNQUFWLEVBQWtCO0FBQ2pCO0FBQ0EsU0FBSSxDQUFDLFFBQVEsUUFBUSxXQUFoQixFQUE2QixFQUE3QixDQUFMLEVBQXVDO0FBQ3RDLFdBQUssS0FBTDtBQUNBO0FBQ0QsS0FMRCxNQUtPLElBQUksS0FBSyxLQUFULEVBQWdCO0FBQ3RCLFVBQUssSUFBTCxDQUFVLE9BQVYsRUFBbUIsS0FBSyxLQUF4QjtBQUNBLEtBRk0sTUFFQSxJQUFJLENBQUMsS0FBSyxRQUFWLEVBQW9CO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBSyxRQUFMLEdBQWdCLElBQWhCOztBQUVBLFNBQUksS0FBSyxRQUFMLEdBQWdCLENBQWhCLElBQXFCLENBQUMsS0FBSyxPQUEvQixFQUF3QztBQUN2QyxVQUFJLFdBQVcsT0FBWCxDQUFKLEVBQXlCO0FBQ3hCLFdBQUk7QUFDSCxrQkFBVSxRQUFRLE1BQVIsQ0FBZSxFQUFmLEVBQW1CLE9BQW5CLEVBQTRCLFVBQTVCLEVBQXdDLE9BQXhDLENBQVY7QUFDQSxRQUZELENBRUUsT0FBTyxDQUFQLEVBQVU7QUFDWCxjQUFNLENBQU47QUFDQTs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxXQUFJLEtBQUssR0FBTCxDQUFTLFFBQVQsSUFBcUIsWUFBWSxTQUFyQyxFQUFnRDtBQUMvQyxvQkFBWSxLQUFLLE1BQWpCO0FBQ0EsWUFBSSxTQUFKLEVBQWU7QUFDZCxtQkFBVSxVQUFVLE9BQXBCO0FBQ0EsU0FGRCxNQUVPLElBQUksS0FBSyxZQUFULEVBQXVCO0FBQzdCO0FBQ0EsbUJBQVUsS0FBSyxPQUFmO0FBQ0E7QUFDRDs7QUFFRCxXQUFJLEdBQUosRUFBUztBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQUssS0FBSyxNQUFMLENBQVksS0FBWixJQUFxQixLQUFLLEdBQUwsQ0FBUyxRQUEvQixJQUNILElBQUksT0FBSixLQUFnQixjQURqQixFQUNpQztBQUNoQyxhQUFJLFVBQUosR0FBaUIsS0FBSyxHQUF0QjtBQUNBLGFBQUksY0FBSixHQUFxQixLQUFLLEdBQUwsQ0FBUyxRQUFULEdBQW9CLENBQUMsS0FBSyxHQUFMLENBQVMsRUFBVixDQUFwQixHQUFvQyxJQUF6RDtBQUNBLGFBQUksV0FBSixHQUFrQixLQUFLLEdBQUwsQ0FBUyxRQUFULEdBQW9CLFFBQXBCLEdBQStCLFNBQWpEO0FBQ0EsZ0JBQU8sUUFBUyxLQUFLLEtBQUwsR0FBYSxHQUF0QixDQUFQO0FBQ0EsU0FORCxNQU1PLElBQUksT0FBTyxPQUFQLEtBQW1CLFdBQW5CLElBQ1YsUUFBUSxLQURGLEVBQ1M7QUFDZjtBQUNBO0FBQ0EsaUJBQVEsS0FBUixDQUFjLEdBQWQ7QUFDQSxTQUxNLE1BS0E7QUFDTjtBQUNBO0FBQ0E7QUFDQSxhQUFJLE9BQUosQ0FBWSxHQUFaO0FBQ0E7QUFDRDtBQUNELE9BN0NELE1BNkNPO0FBQ047QUFDQSxpQkFBVSxPQUFWO0FBQ0E7O0FBRUQsV0FBSyxPQUFMLEdBQWUsT0FBZjs7QUFFQSxVQUFJLEtBQUssR0FBTCxDQUFTLFFBQVQsSUFBcUIsQ0FBQyxLQUFLLE1BQS9CLEVBQXVDO0FBQ3RDLGdCQUFRLEVBQVIsSUFBYyxPQUFkOztBQUVBLFdBQUksSUFBSSxjQUFSLEVBQXdCO0FBQ3ZCLFlBQUksY0FBYyxFQUFsQjtBQUNBLGFBQUssS0FBSyxPQUFWLEVBQW1CLFVBQVMsTUFBVCxFQUFpQjtBQUNuQyxxQkFBWSxJQUFaLENBQWlCLE9BQU8sYUFBUCxJQUF3QixNQUF6QztBQUNBLFNBRkQ7QUFHQSxZQUFJLGNBQUosQ0FBbUIsT0FBbkIsRUFBNEIsS0FBSyxHQUFqQyxFQUFzQyxXQUF0QztBQUNBO0FBQ0Q7O0FBRUQ7QUFDQSxvQkFBYyxFQUFkOztBQUVBLFdBQUssT0FBTCxHQUFlLElBQWY7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxVQUFLLFFBQUwsR0FBZ0IsS0FBaEI7O0FBRUEsU0FBSSxLQUFLLE9BQUwsSUFBZ0IsQ0FBQyxLQUFLLGFBQTFCLEVBQXlDO0FBQ3hDLFdBQUssYUFBTCxHQUFxQixJQUFyQjtBQUNBLFdBQUssSUFBTCxDQUFVLFNBQVYsRUFBcUIsS0FBSyxPQUExQjtBQUNBLFdBQUssa0JBQUwsR0FBMEIsSUFBMUI7QUFDQTtBQUVEO0FBQ0QsSUEvTWlCOztBQWlObEIsZUFBWSxzQkFBVztBQUN0QixRQUFJLE1BQU0sS0FBSyxHQUFmO0FBQ0EsUUFBSSxLQUFLLElBQUksRUFBYjtBQUNBO0FBQ0EsUUFBSSxZQUFZLGNBQWMsSUFBSSxNQUFsQixDQUFoQjs7QUFFQTtBQUNBO0FBQ0EsU0FBSyxPQUFMLENBQWEsSUFBYixDQUFrQixTQUFsQjs7QUFFQSxPQUFHLFNBQUgsRUFBYyxTQUFkLEVBQXlCLEtBQUssSUFBTCxFQUFXLFVBQVMsTUFBVCxFQUFpQjtBQUNwRCxTQUFJLGFBQUo7QUFBQSxTQUFVLHNCQUFWO0FBQUEsU0FBeUIsc0JBQXpCO0FBQUEsU0FDQyxXQUFXLE9BQU8sVUFBUCxFQUFtQixLQUFLLEdBQUwsQ0FBUyxFQUE1QixDQURaO0FBQUEsU0FFQyxPQUFPLEtBQUssR0FBTCxDQUFTLElBRmpCO0FBQUEsU0FHQyxhQUFhLEtBQUssR0FBTCxDQUFTLFNBQVQsR0FBcUIsS0FBSyxHQUFMLENBQVMsU0FBVCxDQUFtQixJQUF4QyxHQUErQyxJQUg3RDtBQUFBLFNBSUMsZUFBZSxRQUFRLFdBQVIsQ0FBb0IsSUFBSSxTQUF4QixFQUFtQztBQUNqRCwyQkFBcUI7QUFENEIsTUFBbkMsQ0FKaEI7O0FBUUE7QUFDQTtBQUNBLFNBQUksS0FBSyxHQUFMLENBQVMsWUFBYixFQUEyQjtBQUMxQjtBQUNBLFVBQUksT0FBTyxTQUFYLEVBQXNCO0FBQ3JCLGNBQU8sT0FBTyxTQUFQLENBQWlCLElBQWpCLEVBQXVCLFVBQVMsSUFBVCxFQUFlO0FBQzNDLGVBQU8sVUFBVSxJQUFWLEVBQWdCLFVBQWhCLEVBQTRCLElBQTVCLENBQVA7QUFDQSxRQUZLLEtBRUEsRUFGUDtBQUdBOztBQUVEO0FBQ0E7QUFDQSxzQkFBZ0IsY0FBYyxJQUFJLE1BQUosR0FBYSxHQUFiLEdBQW1CLElBQWpDLEVBQ2YsS0FBSyxHQUFMLENBQVMsU0FETSxDQUFoQjtBQUVBLFNBQUcsYUFBSCxFQUNDLFNBREQsRUFDWSxLQUFLLElBQUwsRUFBVyxVQUFTLEtBQVQsRUFBZ0I7QUFDckMsWUFBSyxHQUFMLENBQVMsYUFBVCxHQUF5QixhQUF6QjtBQUNBLFlBQUssSUFBTCxDQUFVLEVBQVYsRUFBYyxZQUFXO0FBQ3hCLGVBQU8sS0FBUDtBQUNBLFFBRkQsRUFFRyxJQUZILEVBRVM7QUFDUixpQkFBUyxJQUREO0FBRVIsZ0JBQVE7QUFGQSxRQUZUO0FBTUEsT0FSVSxDQURaOztBQVdBLHNCQUFnQixPQUFPLFFBQVAsRUFBaUIsY0FBYyxFQUEvQixDQUFoQjtBQUNBLFVBQUksYUFBSixFQUFtQjtBQUNsQjtBQUNBO0FBQ0EsWUFBSyxPQUFMLENBQWEsSUFBYixDQUFrQixhQUFsQjs7QUFFQSxXQUFJLEtBQUssTUFBTCxDQUFZLEtBQWhCLEVBQXVCO0FBQ3RCLHNCQUFjLEVBQWQsQ0FBaUIsT0FBakIsRUFBMEIsS0FBSyxJQUFMLEVBQVcsVUFBUyxHQUFULEVBQWM7QUFDbEQsY0FBSyxJQUFMLENBQVUsT0FBVixFQUFtQixHQUFuQjtBQUNBLFNBRnlCLENBQTFCO0FBR0E7QUFDRCxxQkFBYyxNQUFkO0FBQ0E7O0FBRUQ7QUFDQTs7QUFFRDtBQUNBO0FBQ0EsU0FBSSxRQUFKLEVBQWM7QUFDYixXQUFLLEdBQUwsQ0FBUyxHQUFULEdBQWUsUUFBUSxTQUFSLENBQWtCLFFBQWxCLENBQWY7QUFDQSxXQUFLLElBQUw7QUFDQTtBQUNBOztBQUVELFlBQU8sS0FBSyxJQUFMLEVBQVcsVUFBUyxLQUFULEVBQWdCO0FBQ2pDLFdBQUssSUFBTCxDQUFVLEVBQVYsRUFBYyxZQUFXO0FBQ3hCLGNBQU8sS0FBUDtBQUNBLE9BRkQsRUFFRyxJQUZILEVBRVM7QUFDUixnQkFBUztBQURELE9BRlQ7QUFLQSxNQU5NLENBQVA7O0FBUUEsVUFBSyxLQUFMLEdBQWEsS0FBSyxJQUFMLEVBQVcsVUFBUyxHQUFULEVBQWM7QUFDckMsV0FBSyxNQUFMLEdBQWMsSUFBZDtBQUNBLFdBQUssS0FBTCxHQUFhLEdBQWI7QUFDQSxVQUFJLGNBQUosR0FBcUIsQ0FBQyxFQUFELENBQXJCOztBQUVBO0FBQ0E7QUFDQSxlQUFTLFFBQVQsRUFBbUIsVUFBUyxHQUFULEVBQWM7QUFDaEMsV0FBSSxJQUFJLEdBQUosQ0FBUSxFQUFSLENBQVcsT0FBWCxDQUFtQixLQUFLLGVBQXhCLE1BQTZDLENBQWpELEVBQW9EO0FBQ25ELHNCQUFjLElBQUksR0FBSixDQUFRLEVBQXRCO0FBQ0E7QUFDRCxPQUpEOztBQU1BLGNBQVEsR0FBUjtBQUNBLE1BZFksQ0FBYjs7QUFnQkE7QUFDQTtBQUNBLFVBQUssUUFBTCxHQUFnQixLQUFLLElBQUwsRUFBVyxVQUFTLElBQVQsRUFBZSxPQUFmLEVBQXdCO0FBQ2xEO0FBQ0EsVUFBSSxhQUFhLElBQUksSUFBckI7QUFBQSxVQUNDLFlBQVksY0FBYyxVQUFkLENBRGI7QUFBQSxVQUVDLGlCQUFpQixjQUZsQjs7QUFJQTtBQUNBO0FBQ0E7QUFDQSxVQUFJLE9BQUosRUFBYTtBQUNaLGNBQU8sT0FBUDtBQUNBOztBQUVEO0FBQ0E7QUFDQSxVQUFJLGNBQUosRUFBb0I7QUFDbkIsd0JBQWlCLEtBQWpCO0FBQ0E7O0FBRUQ7QUFDQSxnQkFBVSxTQUFWOztBQUVBO0FBQ0EsVUFBSSxRQUFRLFFBQU8sTUFBZixFQUF1QixFQUF2QixDQUFKLEVBQWdDO0FBQy9CLGVBQU8sTUFBUCxDQUFjLFVBQWQsSUFBNEIsUUFBTyxNQUFQLENBQWMsRUFBZCxDQUE1QjtBQUNBOztBQUVELFVBQUk7QUFDSCxXQUFJLElBQUosQ0FBUyxJQUFUO0FBQ0EsT0FGRCxDQUVFLE9BQU8sQ0FBUCxFQUFVO0FBQ1gsY0FBTyxRQUFRLFVBQVUsY0FBVixFQUNkLHVCQUF1QixFQUF2QixHQUNBLFdBREEsR0FDYyxDQUZBLEVBR2QsQ0FIYyxFQUlkLENBQUMsRUFBRCxDQUpjLENBQVIsQ0FBUDtBQUtBOztBQUVELFVBQUksY0FBSixFQUFvQjtBQUNuQix3QkFBaUIsSUFBakI7QUFDQTs7QUFFRDtBQUNBLFdBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsU0FBbEI7O0FBRUE7QUFDQSxjQUFRLFlBQVIsQ0FBcUIsVUFBckI7O0FBRUE7QUFDQSxtQkFBYSxDQUFDLFVBQUQsQ0FBYixFQUEyQixJQUEzQjtBQUNBLE1BakRlLENBQWhCOztBQW1EQTtBQUNBO0FBQ0EsWUFBTyxJQUFQLENBQVksSUFBSSxJQUFoQixFQUFzQixZQUF0QixFQUFvQyxJQUFwQyxFQUEwQyxPQUExQztBQUNBLEtBM0l3QixDQUF6Qjs7QUE2SUEsWUFBUSxNQUFSLENBQWUsU0FBZixFQUEwQixJQUExQjtBQUNBLFNBQUssVUFBTCxDQUFnQixVQUFVLEVBQTFCLElBQWdDLFNBQWhDO0FBQ0EsSUExV2lCOztBQTRXbEIsV0FBUSxrQkFBVztBQUNsQixvQkFBZ0IsS0FBSyxHQUFMLENBQVMsRUFBekIsSUFBK0IsSUFBL0I7QUFDQSxTQUFLLE9BQUwsR0FBZSxJQUFmOztBQUVBO0FBQ0E7QUFDQSxTQUFLLFFBQUwsR0FBZ0IsSUFBaEI7O0FBRUE7QUFDQSxTQUFLLEtBQUssT0FBVixFQUFtQixLQUFLLElBQUwsRUFBVyxVQUFTLE1BQVQsRUFBaUIsQ0FBakIsRUFBb0I7QUFDakQsU0FBSSxXQUFKO0FBQ0EsU0FBSSxZQUFKO0FBQ0EsU0FBSSxnQkFBSjs7QUFFQSxTQUFJLE9BQU8sTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUMvQjtBQUNBLGVBQVMsY0FBYyxNQUFkLEVBQ1AsS0FBSyxHQUFMLENBQVMsUUFBVCxHQUFvQixLQUFLLEdBQXpCLEdBQStCLEtBQUssR0FBTCxDQUFTLFNBRGpDLEVBRVIsS0FGUSxFQUdSLENBQUMsS0FBSyxPQUhFLENBQVQ7QUFJQSxXQUFLLE9BQUwsQ0FBYSxDQUFiLElBQWtCLE1BQWxCOztBQUVBLGdCQUFVLE9BQU8sUUFBUCxFQUFpQixPQUFPLEVBQXhCLENBQVY7O0FBRUEsVUFBSSxPQUFKLEVBQWE7QUFDWixZQUFLLFVBQUwsQ0FBZ0IsQ0FBaEIsSUFBcUIsUUFBUSxJQUFSLENBQXJCO0FBQ0E7QUFDQTs7QUFFRCxXQUFLLFFBQUwsSUFBaUIsQ0FBakI7O0FBRUEsU0FBRyxNQUFILEVBQVcsU0FBWCxFQUFzQixLQUFLLElBQUwsRUFBVyxVQUFTLFVBQVQsRUFBcUI7QUFDckQsV0FBSSxLQUFLLE9BQVQsRUFBa0I7QUFDakI7QUFDQTtBQUNELFlBQUssU0FBTCxDQUFlLENBQWYsRUFBa0IsVUFBbEI7QUFDQSxZQUFLLEtBQUw7QUFDQSxPQU5xQixDQUF0Qjs7QUFRQSxVQUFJLEtBQUssT0FBVCxFQUFrQjtBQUNqQixVQUFHLE1BQUgsRUFBVyxPQUFYLEVBQW9CLEtBQUssSUFBTCxFQUFXLEtBQUssT0FBaEIsQ0FBcEI7QUFDQSxPQUZELE1BRU8sSUFBSSxLQUFLLE1BQUwsQ0FBWSxLQUFoQixFQUF1QjtBQUM3QjtBQUNBO0FBQ0EsVUFBRyxNQUFILEVBQVcsT0FBWCxFQUFvQixLQUFLLElBQUwsRUFBVyxVQUFTLEdBQVQsRUFBYztBQUM1QyxhQUFLLElBQUwsQ0FBVSxPQUFWLEVBQW1CLEdBQW5CO0FBQ0EsUUFGbUIsQ0FBcEI7QUFHQTtBQUNEOztBQUVELFVBQUssT0FBTyxFQUFaO0FBQ0EsV0FBTSxTQUFTLEVBQVQsQ0FBTjs7QUFFQTtBQUNBO0FBQ0EsU0FBSSxDQUFDLFFBQVEsUUFBUixFQUFrQixFQUFsQixDQUFELElBQTBCLEdBQTFCLElBQWlDLENBQUMsSUFBSSxPQUExQyxFQUFtRDtBQUNsRCxjQUFRLE1BQVIsQ0FBZSxNQUFmLEVBQXVCLElBQXZCO0FBQ0E7QUFDRCxLQWpEa0IsQ0FBbkI7O0FBbURBO0FBQ0EsYUFBUyxLQUFLLFVBQWQsRUFBMEIsS0FBSyxJQUFMLEVBQVcsVUFBUyxTQUFULEVBQW9CO0FBQ3hELFNBQUksTUFBTSxPQUFPLFFBQVAsRUFBaUIsVUFBVSxFQUEzQixDQUFWO0FBQ0EsU0FBSSxPQUFPLENBQUMsSUFBSSxPQUFoQixFQUF5QjtBQUN4QixjQUFRLE1BQVIsQ0FBZSxTQUFmLEVBQTBCLElBQTFCO0FBQ0E7QUFDRCxLQUx5QixDQUExQjs7QUFPQSxTQUFLLFFBQUwsR0FBZ0IsS0FBaEI7O0FBRUEsU0FBSyxLQUFMO0FBQ0EsSUFuYmlCOztBQXFibEIsT0FBSSxZQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CO0FBQ3RCLFFBQUksTUFBTSxLQUFLLE1BQUwsQ0FBWSxJQUFaLENBQVY7QUFDQSxRQUFJLENBQUMsR0FBTCxFQUFVO0FBQ1QsV0FBTSxLQUFLLE1BQUwsQ0FBWSxJQUFaLElBQW9CLEVBQTFCO0FBQ0E7QUFDRCxRQUFJLElBQUosQ0FBUyxFQUFUO0FBQ0EsSUEzYmlCOztBQTZibEIsU0FBTSxjQUFTLElBQVQsRUFBZSxHQUFmLEVBQW9CO0FBQ3pCLFNBQUssS0FBSyxNQUFMLENBQVksSUFBWixDQUFMLEVBQXdCLFVBQVMsRUFBVCxFQUFhO0FBQ3BDLFFBQUcsR0FBSDtBQUNBLEtBRkQ7QUFHQSxRQUFJLFNBQVMsT0FBYixFQUFzQjtBQUNyQjtBQUNBO0FBQ0EsWUFBTyxLQUFLLE1BQUwsQ0FBWSxJQUFaLENBQVA7QUFDQTtBQUNEO0FBdGNpQixHQUFuQjs7QUF5Y0EsV0FBUyxhQUFULENBQXVCLElBQXZCLEVBQTZCO0FBQzVCO0FBQ0EsT0FBSSxDQUFDLFFBQVEsUUFBUixFQUFpQixLQUFLLENBQUwsQ0FBakIsQ0FBTCxFQUFnQztBQUMvQixjQUFVLGNBQWMsS0FBSyxDQUFMLENBQWQsRUFBdUIsSUFBdkIsRUFBNkIsSUFBN0IsQ0FBVixFQUE4QyxJQUE5QyxDQUFtRCxLQUFLLENBQUwsQ0FBbkQsRUFBNEQsS0FBSyxDQUFMLENBQTVEO0FBQ0E7QUFDRDs7QUFFRCxXQUFTLGNBQVQsQ0FBd0IsSUFBeEIsRUFBOEIsSUFBOUIsRUFBb0MsSUFBcEMsRUFBMEMsTUFBMUMsRUFBa0Q7QUFDakQ7QUFDQSxPQUFJLEtBQUssV0FBTCxJQUFvQixDQUFDLE9BQXpCLEVBQWtDO0FBQ2pDO0FBQ0EsUUFBSSxNQUFKLEVBQVk7QUFDWCxVQUFLLFdBQUwsQ0FBaUIsTUFBakIsRUFBeUIsSUFBekI7QUFDQTtBQUNELElBTEQsTUFLTztBQUNOLFNBQUssbUJBQUwsQ0FBeUIsSUFBekIsRUFBK0IsSUFBL0IsRUFBcUMsS0FBckM7QUFDQTtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFdBQVMsYUFBVCxDQUF1QixHQUF2QixFQUE0QjtBQUMzQjtBQUNBO0FBQ0EsT0FBSSxPQUFPLElBQUksYUFBSixJQUFxQixJQUFJLFVBQXBDOztBQUVBO0FBQ0Esa0JBQWUsSUFBZixFQUFxQixRQUFRLFlBQTdCLEVBQTJDLE1BQTNDLEVBQW1ELG9CQUFuRDtBQUNBLGtCQUFlLElBQWYsRUFBcUIsUUFBUSxhQUE3QixFQUE0QyxPQUE1Qzs7QUFFQSxVQUFPO0FBQ04sVUFBTSxJQURBO0FBRU4sUUFBSSxRQUFRLEtBQUssWUFBTCxDQUFrQixvQkFBbEI7QUFGTixJQUFQO0FBSUE7O0FBRUQsV0FBUyxhQUFULEdBQXlCO0FBQ3hCLE9BQUksYUFBSjs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsVUFBTyxTQUFTLE1BQWhCLEVBQXdCO0FBQ3ZCLFdBQU8sU0FBUyxLQUFULEVBQVA7QUFDQSxRQUFJLEtBQUssQ0FBTCxNQUFZLElBQWhCLEVBQXNCO0FBQ3JCLFlBQU8sUUFBUSxVQUFVLFVBQVYsRUFBc0IsMkNBQ3BDLEtBQUssS0FBSyxNQUFMLEdBQWMsQ0FBbkIsQ0FEYyxDQUFSLENBQVA7QUFFQSxLQUhELE1BR087QUFDTjtBQUNBO0FBQ0EsbUJBQWMsSUFBZDtBQUNBO0FBQ0Q7QUFDRCxXQUFRLFdBQVIsR0FBc0IsRUFBdEI7QUFDQTs7QUFFRCxZQUFVO0FBQ1QsV0FBUSxPQURDO0FBRVQsZ0JBQWEsV0FGSjtBQUdULGFBQVUsUUFIRDtBQUlULFlBQVMsUUFKQTtBQUtULGVBQVksVUFMSDtBQU1ULGFBQVUsUUFORDtBQU9ULGdCQUFhLEVBUEo7QUFRVCxXQUFRLE1BUkM7QUFTVCxrQkFBZSxhQVROO0FBVVQsYUFBVSxJQUFJLFFBVkw7QUFXVCxZQUFTLE9BWEE7O0FBYVQ7Ozs7O0FBS0EsY0FBVyxtQkFBUyxHQUFULEVBQWM7QUFDeEI7QUFDQSxRQUFJLElBQUksT0FBUixFQUFpQjtBQUNoQixTQUFJLElBQUksT0FBSixDQUFZLE1BQVosQ0FBbUIsSUFBSSxPQUFKLENBQVksTUFBWixHQUFxQixDQUF4QyxNQUErQyxHQUFuRCxFQUF3RDtBQUN2RCxVQUFJLE9BQUosSUFBZSxHQUFmO0FBQ0E7QUFDRDs7QUFFRDtBQUNBLFFBQUksT0FBTyxRQUFPLElBQWxCO0FBQUEsUUFDQyxPQUFPO0FBQ04sWUFBTyxJQUREO0FBRU4sY0FBUyxJQUZIO0FBR04sYUFBUSxJQUhGO0FBSU4sVUFBSztBQUpDLEtBRFI7O0FBUUEsYUFBUyxHQUFULEVBQWMsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ25DLFNBQUksS0FBSyxJQUFMLENBQUosRUFBZ0I7QUFDZixVQUFJLENBQUMsUUFBTyxJQUFQLENBQUwsRUFBbUI7QUFDbEIsZUFBTyxJQUFQLElBQWUsRUFBZjtBQUNBO0FBQ0QsWUFBTSxRQUFPLElBQVAsQ0FBTixFQUFvQixLQUFwQixFQUEyQixJQUEzQixFQUFpQyxJQUFqQztBQUNBLE1BTEQsTUFLTztBQUNOLGNBQU8sSUFBUCxJQUFlLEtBQWY7QUFDQTtBQUNELEtBVEQ7O0FBV0E7QUFDQSxRQUFJLElBQUksT0FBUixFQUFpQjtBQUNoQixjQUFTLElBQUksT0FBYixFQUFzQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDM0MsV0FBSyxLQUFMLEVBQVksVUFBUyxDQUFULEVBQVk7QUFDdkIsV0FBSSxNQUFNLElBQVYsRUFBZ0I7QUFDZixtQkFBVyxDQUFYLElBQWdCLElBQWhCO0FBQ0E7QUFDRCxPQUpEO0FBS0EsTUFORDtBQU9BOztBQUVEO0FBQ0EsUUFBSSxJQUFJLElBQVIsRUFBYztBQUNiLGNBQVMsSUFBSSxJQUFiLEVBQW1CLFVBQVMsS0FBVCxFQUFnQixFQUFoQixFQUFvQjtBQUN0QztBQUNBLFVBQUksUUFBUSxLQUFSLENBQUosRUFBb0I7QUFDbkIsZUFBUTtBQUNQLGNBQU07QUFEQyxRQUFSO0FBR0E7QUFDRCxVQUFJLENBQUMsTUFBTSxPQUFOLElBQWlCLE1BQU0sSUFBeEIsS0FBaUMsQ0FBQyxNQUFNLFNBQTVDLEVBQXVEO0FBQ3RELGFBQU0sU0FBTixHQUFrQixRQUFRLGVBQVIsQ0FBd0IsS0FBeEIsQ0FBbEI7QUFDQTtBQUNELFdBQUssRUFBTCxJQUFXLEtBQVg7QUFDQSxNQVhEO0FBWUEsYUFBTyxJQUFQLEdBQWMsSUFBZDtBQUNBOztBQUVEO0FBQ0EsUUFBSSxJQUFJLFFBQVIsRUFBa0I7QUFDakIsVUFBSyxJQUFJLFFBQVQsRUFBbUIsVUFBUyxNQUFULEVBQWlCO0FBQ25DLFVBQUksaUJBQUo7QUFBQSxVQUFjLGFBQWQ7O0FBRUEsZUFBUyxPQUFPLE1BQVAsS0FBa0IsUUFBbEIsR0FBNkIsRUFBQyxNQUFNLE1BQVAsRUFBN0IsR0FBOEMsTUFBdkQ7O0FBRUEsYUFBTyxPQUFPLElBQWQ7QUFDQSxpQkFBVyxPQUFPLFFBQWxCO0FBQ0EsVUFBSSxRQUFKLEVBQWM7QUFDYixlQUFPLEtBQVAsQ0FBYSxJQUFiLElBQXFCLE9BQU8sUUFBNUI7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxjQUFPLElBQVAsQ0FBWSxJQUFaLElBQW9CLE9BQU8sSUFBUCxHQUFjLEdBQWQsR0FBb0IsQ0FBQyxPQUFPLElBQVAsSUFBZSxNQUFoQixFQUNyQyxPQURxQyxDQUM3QixhQUQ2QixFQUNkLEVBRGMsRUFFckMsT0FGcUMsQ0FFN0IsY0FGNkIsRUFFYixFQUZhLENBQXhDO0FBR0EsTUFqQkQ7QUFrQkE7O0FBRUQ7QUFDQTtBQUNBLGFBQVMsUUFBVCxFQUFtQixVQUFTLEdBQVQsRUFBYyxFQUFkLEVBQWtCO0FBQ3BDO0FBQ0E7QUFDQSxTQUFJLENBQUMsSUFBSSxNQUFMLElBQWUsQ0FBQyxJQUFJLEdBQUosQ0FBUSxZQUE1QixFQUEwQztBQUN6QyxVQUFJLEdBQUosR0FBVSxjQUFjLEVBQWQsRUFBa0IsSUFBbEIsRUFBd0IsSUFBeEIsQ0FBVjtBQUNBO0FBQ0QsS0FORDs7QUFRQTtBQUNBO0FBQ0EsUUFBSSxJQUFJLElBQUosSUFBWSxJQUFJLFFBQXBCLEVBQThCO0FBQzdCLGFBQVEsT0FBUixDQUFnQixJQUFJLElBQUosSUFBWSxFQUE1QixFQUFnQyxJQUFJLFFBQXBDO0FBQ0E7QUFDRCxJQS9HUTs7QUFpSFQsb0JBQWlCLHlCQUFTLEtBQVQsRUFBZ0I7QUFDaEMsYUFBUyxFQUFULEdBQWM7QUFDYixTQUFJLFlBQUo7QUFDQSxTQUFJLE1BQU0sSUFBVixFQUFnQjtBQUNmLFlBQU0sTUFBTSxJQUFOLENBQVcsS0FBWCxDQUFpQixNQUFqQixFQUF5QixTQUF6QixDQUFOO0FBQ0E7QUFDRCxZQUFPLE9BQVEsTUFBTSxPQUFOLElBQWlCLFVBQVUsTUFBTSxPQUFoQixDQUFoQztBQUNBOztBQUVELFdBQU8sRUFBUDtBQUNBLElBM0hROztBQTZIVCxnQkFBYSxxQkFBUyxNQUFULEVBQWlCLE9BQWpCLEVBQTBCO0FBQ3RDLGNBQVUsV0FBVyxFQUFyQjs7QUFFQSxhQUFTLFlBQVQsQ0FBc0IsSUFBdEIsRUFBNEIsUUFBNUIsRUFBc0MsT0FBdEMsRUFBK0M7QUFDOUMsU0FBSSxXQUFKO0FBQUEsU0FBUSxZQUFSO0FBQUEsU0FBYSxtQkFBYjs7QUFFQSxTQUFJLFFBQVEsbUJBQVIsSUFBK0IsUUFBL0IsSUFBMkMsV0FBVyxRQUFYLENBQS9DLEVBQXFFO0FBQ3BFLGVBQVMsZ0JBQVQsR0FBNEIsSUFBNUI7QUFDQTs7QUFFRCxTQUFJLE9BQU8sSUFBUCxLQUFnQixRQUFwQixFQUE4QjtBQUM3QixVQUFJLFdBQVcsUUFBWCxDQUFKLEVBQTBCO0FBQ3pCO0FBQ0EsY0FBTyxRQUFRLFVBQVUsYUFBVixFQUF5QixzQkFBekIsQ0FBUixFQUEwRCxPQUExRCxDQUFQO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBLFVBQUksVUFBVSxRQUFRLFFBQVIsRUFBa0IsSUFBbEIsQ0FBZCxFQUF1QztBQUN0QyxjQUFPLFNBQVMsSUFBVCxFQUFlLFNBQVMsT0FBTyxFQUFoQixDQUFmLENBQVA7QUFDQTs7QUFFRDtBQUNBO0FBQ0EsVUFBSSxJQUFJLEdBQVIsRUFBYTtBQUNaLGNBQU8sSUFBSSxHQUFKLENBQVEsT0FBUixFQUFpQixJQUFqQixFQUF1QixNQUF2QixFQUErQixZQUEvQixDQUFQO0FBQ0E7O0FBRUQ7QUFDQSxZQUFNLGNBQWMsSUFBZCxFQUFvQixNQUFwQixFQUE0QixLQUE1QixFQUFtQyxJQUFuQyxDQUFOO0FBQ0EsV0FBSyxJQUFJLEVBQVQ7O0FBRUEsVUFBSSxDQUFDLFFBQVEsUUFBUixFQUFpQixFQUFqQixDQUFMLEVBQTJCO0FBQzFCLGNBQU8sUUFBUSxVQUFVLFdBQVYsRUFBdUIsa0JBQ3JDLEVBRHFDLEdBRXJDLHlDQUZxQyxHQUdyQyxXQUhxQyxJQUlwQyxTQUFTLEVBQVQsR0FBYyxtQkFKc0IsQ0FBdkIsQ0FBUixDQUFQO0FBS0E7QUFDRCxhQUFPLFNBQVEsRUFBUixDQUFQO0FBQ0E7O0FBRUQ7QUFDQTs7QUFFQTtBQUNBLGFBQVEsUUFBUixDQUFpQixZQUFXO0FBQzNCO0FBQ0E7QUFDQTs7QUFFQSxtQkFBYSxVQUFVLGNBQWMsSUFBZCxFQUFvQixNQUFwQixDQUFWLENBQWI7O0FBRUE7QUFDQTtBQUNBLGlCQUFXLE9BQVgsR0FBcUIsUUFBUSxPQUE3Qjs7QUFFQSxpQkFBVyxJQUFYLENBQWdCLElBQWhCLEVBQXNCLFFBQXRCLEVBQWdDLE9BQWhDLEVBQXlDO0FBQ3hDLGdCQUFTO0FBRCtCLE9BQXpDOztBQUlBO0FBQ0EsTUFoQkQ7O0FBa0JBLFlBQU8sWUFBUDtBQUNBOztBQUVELFVBQU0sWUFBTixFQUFvQjtBQUNuQixnQkFBVyxTQURROztBQUduQjs7Ozs7QUFLQSxZQUFPLGVBQVMsaUJBQVQsRUFBNEI7QUFDbEMsVUFBSSxZQUFKO0FBQUEsVUFDQyxRQUFRLGtCQUFrQixXQUFsQixDQUE4QixHQUE5QixDQURUO0FBQUEsVUFFQyxVQUFVLGtCQUFrQixLQUFsQixDQUF3QixHQUF4QixFQUE2QixDQUE3QixDQUZYO0FBQUEsVUFHQyxhQUFhLFlBQVksR0FBWixJQUFtQixZQUFZLElBSDdDOztBQUtBO0FBQ0E7QUFDQSxVQUFJLFVBQVUsQ0FBQyxDQUFYLEtBQWlCLENBQUMsVUFBRCxJQUFlLFFBQVEsQ0FBeEMsQ0FBSixFQUFnRDtBQUMvQyxhQUFNLGtCQUFrQixTQUFsQixDQUE0QixLQUE1QixFQUFtQyxrQkFBa0IsTUFBckQsQ0FBTjtBQUNBLDJCQUFvQixrQkFBa0IsU0FBbEIsQ0FBNEIsQ0FBNUIsRUFBK0IsS0FBL0IsQ0FBcEI7QUFDQTs7QUFFRCxhQUFPLFFBQVEsU0FBUixDQUFrQixVQUFVLGlCQUFWLEVBQ3hCLFVBQVUsT0FBTyxFQURPLEVBQ0gsSUFERyxDQUFsQixFQUNzQixHQUR0QixFQUMyQixJQUQzQixDQUFQO0FBRUEsTUF2QmtCOztBQXlCbkIsY0FBUyxpQkFBUyxFQUFULEVBQWE7QUFDckIsYUFBTyxRQUFRLFFBQVIsRUFBaUIsY0FBYyxFQUFkLEVBQWtCLE1BQWxCLEVBQTBCLEtBQTFCLEVBQWlDLElBQWpDLEVBQXVDLEVBQXhELENBQVA7QUFDQSxNQTNCa0I7O0FBNkJuQixnQkFBVyxtQkFBUyxFQUFULEVBQWE7QUFDdkIsV0FBSyxjQUFjLEVBQWQsRUFBa0IsTUFBbEIsRUFBMEIsS0FBMUIsRUFBaUMsSUFBakMsRUFBdUMsRUFBNUM7QUFDQSxhQUFPLFFBQVEsUUFBUixFQUFpQixFQUFqQixLQUF3QixRQUFRLFFBQVIsRUFBa0IsRUFBbEIsQ0FBL0I7QUFDQTtBQWhDa0IsS0FBcEI7O0FBbUNBO0FBQ0EsUUFBSSxDQUFDLE1BQUwsRUFBYTtBQUNaLGtCQUFhLEtBQWIsR0FBcUIsVUFBUyxFQUFULEVBQWE7QUFDakM7QUFDQTs7QUFFQSxVQUFJLE1BQU0sY0FBYyxFQUFkLEVBQWtCLE1BQWxCLEVBQTBCLElBQTFCLENBQVY7QUFDQSxVQUFJLE1BQU0sT0FBTyxRQUFQLEVBQWlCLEVBQWpCLENBQVY7O0FBRUEsVUFBSSxPQUFKLEdBQWMsSUFBZDtBQUNBLG1CQUFhLEVBQWI7O0FBRUEsYUFBTyxTQUFRLEVBQVIsQ0FBUDtBQUNBLGFBQU8sV0FBVyxJQUFJLEdBQWYsQ0FBUDtBQUNBLGFBQU8sWUFBWSxFQUFaLENBQVA7O0FBRUE7QUFDQTtBQUNBLGtCQUFZLFFBQVosRUFBc0IsVUFBUyxJQUFULEVBQWUsQ0FBZixFQUFrQjtBQUN2QyxXQUFJLEtBQUssQ0FBTCxNQUFZLEVBQWhCLEVBQW9CO0FBQ25CLGlCQUFTLE1BQVQsQ0FBZ0IsQ0FBaEIsRUFBbUIsQ0FBbkI7QUFDQTtBQUNELE9BSkQ7QUFLQSxhQUFPLFFBQVEsV0FBUixDQUFvQixFQUFwQixDQUFQOztBQUVBLFVBQUksR0FBSixFQUFTO0FBQ1I7QUFDQTtBQUNBLFdBQUksSUFBSSxNQUFKLENBQVcsT0FBZixFQUF3QjtBQUN2QixvQkFBWSxFQUFaLElBQWtCLElBQUksTUFBdEI7QUFDQTs7QUFFRCxxQkFBYyxFQUFkO0FBQ0E7QUFDRCxNQWhDRDtBQWlDQTs7QUFFRCxXQUFPLFlBQVA7QUFDQSxJQXpRUTs7QUEyUVQ7Ozs7O0FBS0EsV0FBUSxnQkFBUyxNQUFULEVBQWlCO0FBQ3hCLFFBQUksTUFBTSxPQUFPLFFBQVAsRUFBaUIsT0FBTyxFQUF4QixDQUFWO0FBQ0EsUUFBSSxHQUFKLEVBQVM7QUFDUixlQUFVLE1BQVYsRUFBa0IsTUFBbEI7QUFDQTtBQUNELElBclJROztBQXVSVDs7Ozs7OztBQU9BLGlCQUFjLHNCQUFTLFVBQVQsRUFBcUI7QUFDbEMsUUFBSSxjQUFKO0FBQ0EsUUFBSSxhQUFKO0FBQ0EsUUFBSSxZQUFKO0FBQ0EsUUFBSSxPQUFPLE9BQU8sUUFBTyxJQUFkLEVBQW9CLFVBQXBCLEtBQW1DLEVBQTlDO0FBQ0EsUUFBSSxZQUFZLEtBQUssT0FBckI7O0FBRUE7O0FBRUEsV0FBTyxTQUFTLE1BQWhCLEVBQXdCO0FBQ3ZCLFlBQU8sU0FBUyxLQUFULEVBQVA7QUFDQSxTQUFJLEtBQUssQ0FBTCxNQUFZLElBQWhCLEVBQXNCO0FBQ3JCLFdBQUssQ0FBTCxJQUFVLFVBQVY7QUFDQTtBQUNBO0FBQ0EsVUFBSSxLQUFKLEVBQVc7QUFDVjtBQUNBO0FBQ0QsY0FBUSxJQUFSO0FBQ0EsTUFSRCxNQVFPLElBQUksS0FBSyxDQUFMLE1BQVksVUFBaEIsRUFBNEI7QUFDbEM7QUFDQSxjQUFRLElBQVI7QUFDQTs7QUFFRCxtQkFBYyxJQUFkO0FBQ0E7QUFDRCxZQUFRLFdBQVIsR0FBc0IsRUFBdEI7O0FBRUE7QUFDQTtBQUNBLFVBQU0sT0FBTyxRQUFQLEVBQWlCLFVBQWpCLENBQU47O0FBRUEsUUFBSSxDQUFDLEtBQUQsSUFBVSxDQUFDLFFBQVEsUUFBUixFQUFpQixVQUFqQixDQUFYLElBQTJDLEdBQTNDLElBQWtELENBQUMsSUFBSSxNQUEzRCxFQUFtRTtBQUNsRSxTQUFJLFFBQU8sYUFBUCxLQUF5QixDQUFDLFNBQUQsSUFBYyxDQUFDLFVBQVUsU0FBVixDQUF4QyxDQUFKLEVBQW1FO0FBQ2xFLFVBQUksZ0JBQWdCLFVBQWhCLENBQUosRUFBaUM7QUFDaEM7QUFDQSxPQUZELE1BRU87QUFDTixjQUFPLFFBQVEsVUFBVSxVQUFWLEVBQ2Qsd0JBQXdCLFVBRFYsRUFFZCxJQUZjLEVBR2QsQ0FBQyxVQUFELENBSGMsQ0FBUixDQUFQO0FBSUE7QUFDRCxNQVRELE1BU087QUFDTjtBQUNBLG9CQUFjLENBQUMsVUFBRCxFQUFjLEtBQUssSUFBTCxJQUFhLEVBQTNCLEVBQWdDLEtBQUssU0FBckMsQ0FBZDtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQSxJQS9VUTs7QUFpVlQ7Ozs7Ozs7O0FBUUEsY0FBVyxtQkFBUyxVQUFULEVBQXFCLEdBQXJCLEVBQTBCLE9BQTFCLEVBQW1DO0FBQzdDLFFBQUksY0FBSjtBQUNBLFFBQUksYUFBSjtBQUNBLFFBQUksVUFBSjtBQUNBLFFBQUkscUJBQUo7QUFDQSxRQUFJLFlBQUo7QUFDQSxRQUFJLG1CQUFKO0FBQUEsUUFBZ0IsaUJBQWhCO0FBQ0EsUUFBSSxVQUFVLE9BQU8sUUFBTyxJQUFkLEVBQW9CLFVBQXBCLENBQWQ7O0FBRUEsUUFBSSxPQUFKLEVBQWE7QUFDWixrQkFBYSxPQUFiO0FBQ0E7O0FBRUQsZUFBVyxPQUFPLFVBQVAsRUFBbUIsVUFBbkIsQ0FBWDs7QUFFQSxRQUFJLFFBQUosRUFBYztBQUNiLFlBQU8sUUFBUSxTQUFSLENBQWtCLFFBQWxCLEVBQTRCLEdBQTVCLEVBQWlDLE9BQWpDLENBQVA7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxRQUFJLElBQUksV0FBSixDQUFnQixJQUFoQixDQUFxQixVQUFyQixDQUFKLEVBQXNDO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBLFdBQU0sY0FBYyxPQUFPLEVBQXJCLENBQU47QUFDQSxLQUxELE1BS087QUFDTjtBQUNBLGFBQVEsUUFBTyxLQUFmOztBQUVBLFlBQU8sV0FBVyxLQUFYLENBQWlCLEdBQWpCLENBQVA7QUFDQTtBQUNBO0FBQ0EsVUFBSyxJQUFJLEtBQUssTUFBZCxFQUFzQixJQUFJLENBQTFCLEVBQTZCLEtBQUssQ0FBbEMsRUFBcUM7QUFDcEMscUJBQWUsS0FBSyxLQUFMLENBQVcsQ0FBWCxFQUFjLENBQWQsRUFBaUIsSUFBakIsQ0FBc0IsR0FBdEIsQ0FBZjs7QUFFQSxtQkFBYSxPQUFPLEtBQVAsRUFBYyxZQUFkLENBQWI7QUFDQSxVQUFJLFVBQUosRUFBZ0I7QUFDZjtBQUNBLFdBQUksUUFBUSxVQUFSLENBQUosRUFBeUI7QUFDeEIscUJBQWEsV0FBVyxDQUFYLENBQWI7QUFDQTtBQUNELFlBQUssTUFBTCxDQUFZLENBQVosRUFBZSxDQUFmLEVBQWtCLFVBQWxCO0FBQ0E7QUFDQTtBQUNEOztBQUVEO0FBQ0EsV0FBTSxLQUFLLElBQUwsQ0FBVSxHQUFWLENBQU47QUFDQSxZQUFRLFFBQVEsYUFBYSxJQUFiLENBQWtCLEdBQWxCLEtBQTBCLE9BQTFCLEdBQW9DLEVBQXBDLEdBQXlDLEtBQWpELENBQVI7QUFDQSxXQUFNLENBQUMsSUFBSSxNQUFKLENBQVcsQ0FBWCxNQUFrQixHQUFsQixJQUF5QixJQUFJLEtBQUosQ0FBVSxlQUFWLENBQXpCLEdBQXNELEVBQXRELEdBQTJELFFBQU8sT0FBbkUsSUFBOEUsR0FBcEY7QUFDQTs7QUFFRCxXQUFPLFFBQU8sT0FBUCxHQUFpQixPQUNELENBQUMsSUFBSSxPQUFKLENBQVksR0FBWixNQUFxQixDQUFDLENBQXRCLEdBQTBCLEdBQTFCLEdBQWdDLEdBQWpDLElBQ0QsUUFBTyxPQUZMLENBQWpCLEdBRWlDLEdBRnhDO0FBR0EsSUFsWlE7O0FBb1pUO0FBQ0EsU0FBTSxjQUFTLEVBQVQsRUFBYSxHQUFiLEVBQWtCO0FBQ3ZCLFFBQUksSUFBSixDQUFTLE9BQVQsRUFBa0IsRUFBbEIsRUFBc0IsR0FBdEI7QUFDQSxJQXZaUTs7QUF5WlQ7Ozs7Ozs7O0FBUUEsV0FBUSxnQkFBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QixFQUErQixPQUEvQixFQUF3QztBQUMvQyxXQUFPLFNBQVMsS0FBVCxDQUFlLE9BQWYsRUFBd0IsSUFBeEIsQ0FBUDtBQUNBLElBbmFROztBQXFhVDs7Ozs7QUFLQSxpQkFBYyxzQkFBUyxHQUFULEVBQWM7QUFDM0I7QUFDQTtBQUNBO0FBQ0EsUUFBSSxJQUFJLElBQUosS0FBYSxNQUFiLElBQ0YsWUFBWSxJQUFaLENBQWlCLENBQUMsSUFBSSxhQUFKLElBQXFCLElBQUksVUFBMUIsRUFBc0MsVUFBdkQsQ0FERixFQUN1RTtBQUN0RTtBQUNBO0FBQ0EseUJBQW9CLElBQXBCOztBQUVBO0FBQ0EsU0FBSSxPQUFPLGNBQWMsR0FBZCxDQUFYO0FBQ0EsYUFBUSxZQUFSLENBQXFCLEtBQUssRUFBMUI7QUFDQTtBQUNELElBeGJROztBQTBiVDs7O0FBR0Esa0JBQWUsdUJBQVMsR0FBVCxFQUFjO0FBQzVCLFFBQUksT0FBTyxjQUFjLEdBQWQsQ0FBWDtBQUNBLFFBQUksQ0FBQyxnQkFBZ0IsS0FBSyxFQUFyQixDQUFMLEVBQStCO0FBQzlCLFNBQUksVUFBVSxFQUFkO0FBQ0EsY0FBUyxRQUFULEVBQW1CLFVBQVMsS0FBVCxFQUFnQixHQUFoQixFQUFxQjtBQUN2QyxVQUFJLElBQUksT0FBSixDQUFZLEtBQVosTUFBdUIsQ0FBM0IsRUFBOEI7QUFDN0IsWUFBSyxNQUFNLE9BQVgsRUFBb0IsVUFBUyxNQUFULEVBQWlCO0FBQ3BDLFlBQUksT0FBTyxFQUFQLEtBQWMsS0FBSyxFQUF2QixFQUEyQjtBQUMxQixpQkFBUSxJQUFSLENBQWEsR0FBYjtBQUNBO0FBQ0QsZUFBTyxJQUFQO0FBQ0EsUUFMRDtBQU1BO0FBQ0QsTUFURDtBQVVBLFlBQU8sUUFBUSxVQUFVLGFBQVYsRUFBeUIsdUJBQXVCLEtBQUssRUFBNUIsSUFDdEMsUUFBUSxNQUFSLEdBQ0EsbUJBQW1CLFFBQVEsSUFBUixDQUFhLElBQWIsQ0FEbkIsR0FFQSxHQUhzQyxDQUF6QixFQUdQLEdBSE8sRUFHRixDQUFDLEtBQUssRUFBTixDQUhFLENBQVIsQ0FBUDtBQUlBO0FBQ0Q7QUFoZFEsR0FBVjs7QUFtZEEsVUFBUSxPQUFSLEdBQWtCLFFBQVEsV0FBUixFQUFsQjtBQUNBLFNBQU8sT0FBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7Ozs7QUFZQSxPQUFNLE9BQU8sU0FBUCxHQUFtQixVQUFTLElBQVQsRUFBZSxRQUFmLEVBQXlCLE9BQXpCLEVBQWtDLFFBQWxDLEVBQTRDO0FBQ3BFO0FBQ0EsTUFBSSxnQkFBSjtBQUNBLE1BQUksZUFBSjtBQUNBLE1BQUksY0FBYyxjQUFsQjs7QUFFQTtBQUNBLE1BQUksQ0FBQyxRQUFRLElBQVIsQ0FBRCxJQUFrQixPQUFPLElBQVAsS0FBZ0IsUUFBdEMsRUFBZ0Q7QUFDL0M7QUFDQSxZQUFTLElBQVQ7QUFDQSxPQUFJLFFBQVEsUUFBUixDQUFKLEVBQXVCO0FBQ3RCO0FBQ0EsV0FBTyxRQUFQO0FBQ0EsZUFBVyxPQUFYO0FBQ0EsY0FBVSxRQUFWO0FBQ0EsSUFMRCxNQUtPO0FBQ04sV0FBTyxFQUFQO0FBQ0E7QUFDRDs7QUFFRCxNQUFJLFVBQVUsT0FBTyxPQUFyQixFQUE4QjtBQUM3QixpQkFBYyxPQUFPLE9BQXJCO0FBQ0E7O0FBRUQsWUFBVSxPQUFPLFFBQVAsRUFBaUIsV0FBakIsQ0FBVjtBQUNBLE1BQUksQ0FBQyxPQUFMLEVBQWM7QUFDYixhQUFVLFNBQVMsV0FBVCxJQUF3QixJQUFJLENBQUosQ0FBTSxVQUFOLENBQWlCLFdBQWpCLENBQWxDO0FBQ0E7O0FBRUQsTUFBSSxNQUFKLEVBQVk7QUFDWCxXQUFRLFNBQVIsQ0FBa0IsTUFBbEI7QUFDQTs7QUFFRCxTQUFPLFFBQVEsT0FBUixDQUFnQixJQUFoQixFQUFzQixRQUF0QixFQUFnQyxPQUFoQyxDQUFQO0FBQ0EsRUFsQ0Q7O0FBb0NBOzs7QUFHQSxLQUFJLE1BQUosR0FBYSxVQUFTLE1BQVQsRUFBaUI7QUFDN0IsU0FBTyxJQUFJLE1BQUosQ0FBUDtBQUNBLEVBRkQ7O0FBSUE7Ozs7Ozs7QUFPQSxLQUFJLFFBQUosR0FBZSxPQUFPLFVBQVAsS0FBc0IsV0FBdEIsR0FBb0MsVUFBUyxFQUFULEVBQWE7QUFDL0QsYUFBVyxFQUFYLEVBQWUsQ0FBZjtBQUNBLEVBRmMsR0FFWCxVQUFTLEVBQVQsRUFBYTtBQUNoQjtBQUNBLEVBSkQ7O0FBTUE7OztBQUdBLEtBQUksQ0FBQyxPQUFPLE9BQVosRUFBcUI7QUFDcEIsU0FBTyxPQUFQLEdBQWlCLEdBQWpCO0FBQ0E7O0FBRUQsS0FBSSxPQUFKLEdBQWMsT0FBZDs7QUFFQTtBQUNBLEtBQUksV0FBSixHQUFrQixnQkFBbEI7QUFDQSxLQUFJLFNBQUosR0FBZ0IsU0FBaEI7QUFDQSxLQUFJLElBQUksQ0FBSixHQUFRO0FBQ1gsWUFBVSxRQURDO0FBRVgsY0FBWTtBQUZELEVBQVo7O0FBS0E7QUFDQSxLQUFJLEVBQUo7O0FBRUE7QUFDQSxNQUFLLENBQ0osT0FESSxFQUVKLE9BRkksRUFHSixTQUhJLEVBSUosV0FKSSxDQUFMLEVBS0csVUFBUyxJQUFULEVBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0EsTUFBSSxJQUFKLElBQVksWUFBVztBQUN0QixPQUFJLE1BQU0sU0FBUyxjQUFULENBQVY7QUFDQSxVQUFPLElBQUksT0FBSixDQUFZLElBQVosRUFBa0IsS0FBbEIsQ0FBd0IsR0FBeEIsRUFBNkIsU0FBN0IsQ0FBUDtBQUNBLEdBSEQ7QUFJQSxFQWJEOztBQWVBLEtBQUksU0FBSixFQUFlO0FBQ2QsU0FBTyxFQUFFLElBQUYsR0FBUyxTQUFTLG9CQUFULENBQThCLE1BQTlCLEVBQXNDLENBQXRDLENBQWhCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWMsU0FBUyxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxDQUFkO0FBQ0EsTUFBSSxXQUFKLEVBQWlCO0FBQ2hCLFVBQU8sRUFBRSxJQUFGLEdBQVMsWUFBWSxVQUE1QjtBQUNBO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxLQUFJLE9BQUosR0FBYyxjQUFkOztBQUVBOzs7QUFHQSxLQUFJLFVBQUosR0FBaUIsVUFBUyxNQUFULEVBQWlCLFVBQWpCLEVBQTZCLEdBQTdCLEVBQWtDO0FBQ2xELE1BQU0sT0FBTyxPQUFPLEtBQVAsR0FDRixTQUFTLGVBQVQsQ0FBeUIsOEJBQXpCLEVBQXlELGFBQXpELENBREUsR0FFRixTQUFTLGFBQVQsQ0FBdUIsUUFBdkIsQ0FGWDtBQUdBLE9BQUssSUFBTCxHQUFZLE9BQU8sVUFBUCxJQUFxQixpQkFBakM7QUFDQSxPQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0EsT0FBSyxLQUFMLEdBQWEsSUFBYjtBQUNBLFNBQU8sSUFBUDtBQUNBLEVBUkQ7O0FBVUE7Ozs7Ozs7OztBQVNBLEtBQUksSUFBSixHQUFXLFVBQVMsT0FBVCxFQUFrQixVQUFsQixFQUE4QixHQUE5QixFQUFtQztBQUM3QyxNQUFJLFNBQVUsV0FBVyxRQUFRLE1BQXBCLElBQStCLEVBQTVDO0FBQ0EsTUFBSSxhQUFKOztBQUVBLE1BQUksU0FBSixFQUFlO0FBQ2Q7QUFDQSxVQUFPLElBQUksVUFBSixDQUFlLE1BQWYsRUFBdUIsVUFBdkIsRUFBbUMsR0FBbkMsQ0FBUDtBQUNBLE9BQUksT0FBTyxhQUFYLEVBQTBCO0FBQ3pCLFdBQU8sYUFBUCxDQUFxQixJQUFyQixFQUEyQixNQUEzQixFQUFtQyxVQUFuQyxFQUErQyxHQUEvQztBQUNBOztBQUVELFFBQUssWUFBTCxDQUFrQixxQkFBbEIsRUFBeUMsUUFBUSxXQUFqRDtBQUNBLFFBQUssWUFBTCxDQUFrQixvQkFBbEIsRUFBd0MsVUFBeEM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQUksS0FBSyxXQUFMO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUUsS0FBSyxXQUFMLENBQWlCLFFBQWpCLElBQTZCLEtBQUssV0FBTCxDQUFpQixRQUFqQixHQUE0QixPQUE1QixDQUFvQyxjQUFwQyxJQUFzRCxDQUFyRixDQU5HLElBT0gsQ0FBQyxPQVBGLEVBT1c7QUFDVjtBQUNBO0FBQ0E7QUFDQSxxQkFBaUIsSUFBakI7O0FBRUEsU0FBSyxXQUFMLENBQWlCLG9CQUFqQixFQUF1QyxRQUFRLFlBQS9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQXJCRCxNQXFCTztBQUNOLFNBQUssZ0JBQUwsQ0FBc0IsTUFBdEIsRUFBOEIsUUFBUSxZQUF0QyxFQUFvRCxLQUFwRDtBQUNBLFNBQUssZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsUUFBUSxhQUF2QyxFQUFzRCxLQUF0RDtBQUNBO0FBQ0QsUUFBSyxHQUFMLEdBQVcsR0FBWDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFJLFdBQUosRUFBaUI7QUFDaEIsU0FBSyxZQUFMLENBQWtCLElBQWxCLEVBQXdCLFdBQXhCO0FBQ0EsSUFGRCxNQUVPO0FBQ04sU0FBSyxXQUFMLENBQWlCLElBQWpCO0FBQ0E7O0FBRUQsVUFBTyxJQUFQO0FBQ0EsR0FwREQsTUFvRE8sSUFBSSxXQUFKLEVBQWlCO0FBQ3ZCLE9BQUk7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBYyxHQUFkOztBQUVBO0FBQ0EsWUFBUSxZQUFSLENBQXFCLFVBQXJCO0FBQ0EsSUFYRCxDQVdFLE9BQU8sQ0FBUCxFQUFVO0FBQ1gsWUFBUSxPQUFSLENBQWdCLFVBQVUsZUFBVixFQUNmLDhCQUNBLFVBREEsR0FDYSxNQURiLEdBQ3NCLEdBRlAsRUFHZixDQUhlLEVBSWYsQ0FBQyxVQUFELENBSmUsQ0FBaEI7QUFLQTtBQUNEO0FBQ0QsRUE1RUQ7O0FBOEVBO0FBQ0EsS0FBSSxhQUFhLENBQUMsSUFBSSxZQUF0QixFQUFvQztBQUNuQztBQUNBLGNBQVksU0FBWixFQUF1QixVQUFTLE1BQVQsRUFBaUI7QUFDdkM7QUFDQTtBQUNBLE9BQUksQ0FBQyxJQUFMLEVBQVc7QUFDVixXQUFPLE9BQU8sVUFBZDtBQUNBOztBQUVEO0FBQ0E7QUFDQTtBQUNBLGNBQVcsT0FBTyxZQUFQLENBQW9CLFdBQXBCLENBQVg7QUFDQSxPQUFJLFFBQUosRUFBYztBQUNiO0FBQ0EsaUJBQWEsUUFBYjs7QUFFQTtBQUNBLFFBQUksQ0FBQyxJQUFJLE9BQVQsRUFBa0I7QUFDakI7QUFDQTtBQUNBLFdBQU0sV0FBVyxLQUFYLENBQWlCLEdBQWpCLENBQU47QUFDQSxrQkFBYSxJQUFJLEdBQUosRUFBYjtBQUNBLGVBQVUsSUFBSSxNQUFKLEdBQWEsSUFBSSxJQUFKLENBQVMsR0FBVCxJQUFnQixHQUE3QixHQUFtQyxJQUE3Qzs7QUFFQSxTQUFJLE9BQUosR0FBYyxPQUFkO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBLGlCQUFhLFdBQVcsT0FBWCxDQUFtQixjQUFuQixFQUFtQyxFQUFuQyxDQUFiOztBQUVBO0FBQ0EsUUFBSSxJQUFJLFdBQUosQ0FBZ0IsSUFBaEIsQ0FBcUIsVUFBckIsQ0FBSixFQUFzQztBQUNyQyxrQkFBYSxRQUFiO0FBQ0E7O0FBRUQ7QUFDQSxRQUFJLElBQUosR0FBVyxJQUFJLElBQUosR0FBVyxJQUFJLElBQUosQ0FBUyxNQUFULENBQWdCLFVBQWhCLENBQVgsR0FBeUMsQ0FBQyxVQUFELENBQXBEOztBQUVBLFdBQU8sSUFBUDtBQUNBO0FBQ0QsR0F4Q0Q7QUF5Q0E7O0FBRUQ7Ozs7OztBQU1BLEtBQUksSUFBSixHQUFXLFVBQVMsSUFBVCxFQUFlO0FBQ3pCO0FBQ0EsU0FBTyxLQUFLLElBQUwsQ0FBUDtBQUNBLEVBSEQ7O0FBS0E7QUFDQSxLQUFJLEdBQUo7QUFDQSxDQTUrREQiLCJmaWxlIjoianNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gY29sbGVjdGlvbi5qcyAyMDE2LTA2LTIyXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuKGZ1bmN0aW9uKCkge1xuXHRcblx0J3VzZSBzdHJpY3QnO1xuXHRcblx0LyoqXG5cdCAqIENsYXNzIENvbGxlY3Rpb25cblx0ICogXG5cdCAqIFRoaXMgY2xhc3MgaXMgdXNlZCB0byBoYW5kbGUgbXVsdGlwbGUgbW9kdWxlcyBvZiB0aGUgc2FtZSB0eXBlIChjb250cm9sbGVycywgZXh0ZW5zaW9ucyAuLi4pLlxuXHQgKlxuXHQgKiBAY2xhc3MgSlNFL0NvbnN0cnVjdG9ycy9Db2xsZWN0aW9uXG5cdCAqL1xuXHRjbGFzcyBDb2xsZWN0aW9uIHtcblx0XHQvKipcblx0XHQgKiBDbGFzcyBDb25zdHJ1Y3RvciBcblx0XHQgKiBcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBUaGUgY29sbGVjdGlvbiBuYW1lIC0gbXVzdCBiZSB1bmlxdWUuXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IGF0dHJpYnV0ZSBUaGUgYXR0cmlidXRlIHRoYXQgd2lsbCB0cmlnZ2VyIGNvbGxlY3Rpb24ncyBtb2R1bGVzLlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBuYW1lc3BhY2UgT3B0aW9uYWwsIHRoZSBuYW1lc3BhY2UgaW5zdGFuY2Ugd2hlcmUgdGhlIGNvbGxlY3Rpb24gYmVsb25ncy5cblx0XHQgKi9cblx0XHRjb25zdHJ1Y3RvcihuYW1lLCBhdHRyaWJ1dGUsIG5hbWVzcGFjZSkge1xuXHRcdFx0dGhpcy5uYW1lID0gbmFtZTtcblx0XHRcdHRoaXMuYXR0cmlidXRlID0gYXR0cmlidXRlO1xuXHRcdFx0dGhpcy5uYW1lc3BhY2UgPSBuYW1lc3BhY2U7XG5cdFx0XHR0aGlzLmNhY2hlID0ge1xuXHRcdFx0XHRtb2R1bGVzOiB7fSxcblx0XHRcdFx0ZGF0YToge31cblx0XHRcdH07XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmluZSBhIG5ldyBlbmdpbmUgbW9kdWxlLlxuXHRcdCAqXG5cdFx0ICogVGhpcyBmdW5jdGlvbiB3aWxsIGRlZmluZSBhIG5ldyBtb2R1bGUgaW50byB0aGUgZW5naW5lLiBFYWNoIG1vZHVsZSB3aWxsIGJlIHN0b3JlZCBpbiB0aGVcblx0XHQgKiBjb2xsZWN0aW9uJ3MgY2FjaGUgdG8gcHJldmVudCB1bm5lY2Vzc2FyeSBmaWxlIHRyYW5zZmVycy4gVGhlIHNhbWUgaGFwcGVucyB3aXRoIHRoZSBkZWZhdWx0XG5cdFx0ICogY29uZmlndXJhdGlvbiB0aGF0IGFwcGVuZCB0byB0aGUgbW9kdWxlIGRlZmluaXRpb24uXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBOYW1lIG9mIHRoZSBtb2R1bGUgKHNhbWUgYXMgdGhlIGZpbGVuYW1lKS5cblx0XHQgKiBAcGFyYW0ge0FycmF5fSBkZXBlbmRlbmNpZXMgQXJyYXkgb2YgbGlicmFyaWVzIHRoYXQgdGhpcyBtb2R1bGUgZGVwZW5kcyBvbiAod2lsbCBiZSBsb2FkZWQgYXN5bmNocm9ub3VzbHkpLlxuXHRcdCAqIEFwcGx5IG9ubHkgZmlsZW5hbWVzIHdpdGhvdXQgZXh0ZW5zaW9uIGUuZy4gW1wiZW1haWxzXCJdLlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBjb2RlIENvbnRhaW5zIHRoZSBtb2R1bGUgY29kZSAoZnVuY3Rpb24pLlxuXHRcdCAqL1xuXHRcdG1vZHVsZShuYW1lLCBkZXBlbmRlbmNpZXMsIGNvZGUpIHtcblx0XHRcdC8vIENoZWNrIGlmIHJlcXVpcmVkIHZhbHVlcyBhcmUgYXZhaWxhYmxlIGFuZCBvZiBjb3JyZWN0IHR5cGUuXG5cdFx0XHRpZiAoIW5hbWUgfHwgdHlwZW9mIG5hbWUgIT09ICdzdHJpbmcnIHx8IHR5cGVvZiBjb2RlICE9PSAnZnVuY3Rpb24nKSB7XG5cdFx0XHRcdGpzZS5jb3JlLmRlYnVnLndhcm4oJ1JlZ2lzdHJhdGlvbiBvZiB0aGUgbW9kdWxlIGZhaWxlZCwgZHVlIHRvIGJhZCBmdW5jdGlvbiBjYWxsJywgYXJndW1lbnRzKTtcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBDaGVjayBpZiB0aGUgbW9kdWxlIGlzIGFscmVhZHkgZGVmaW5lZC5cblx0XHRcdGlmICh0aGlzLmNhY2hlLm1vZHVsZXNbbmFtZV0pIHtcblx0XHRcdFx0anNlLmNvcmUuZGVidWcud2FybignUmVnaXN0cmF0aW9uIG9mIG1vZHVsZSBcIicgKyBuYW1lICsgJ1wiIHNraXBwZWQsIGJlY2F1c2UgaXQgYWxyZWFkeSBleGlzdHMuJyk7XG5cdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gU3RvcmUgdGhlIG1vZHVsZSB0byBjYWNoZSBzbyB0aGF0IGl0IGNhbiBiZSB1c2VkIGxhdGVyLlxuXHRcdFx0dGhpcy5jYWNoZS5tb2R1bGVzW25hbWVdID0ge1xuXHRcdFx0XHRjb2RlOiBjb2RlLFxuXHRcdFx0XHRkZXBlbmRlbmNpZXM6IGRlcGVuZGVuY2llc1xuXHRcdFx0fTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBNb2R1bGUgQ29sbGVjdGlvblxuXHRcdCAqXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCB0cmlnZ2VyIHRoZSBwYWdlIG1vZHVsZXMgaW5pdGlhbGl6YXRpb24uIEl0IHdpbGwgc2VhcmNoIGFsbFxuXHRcdCAqIHRoZSBET00gZm9yIHRoZSBcImRhdGEtZ3gtZXh0ZW5zaW9uXCIsIFwiZGF0YS1neC1jb250cm9sbGVyXCIgb3Jcblx0XHQgKiBcImRhdGEtZ3gtd2lkZ2V0XCIgYXR0cmlidXRlcyBhbmQgbG9hZCB0aGUgcmVsZXZhbnQgc2NyaXB0cyB0aHJvdWdoIFJlcXVpcmVKUy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7alF1ZXJ5fSAkcGFyZW50IE9wdGlvbmFsIChudWxsKSwgcGFyZW50IGVsZW1lbnQgd2lsbCBiZSB1c2VkIHRvIHNlYXJjaCBmb3IgdGhlIHJlcXVpcmVkIG1vZHVsZXMuXG5cdFx0ICogXG5cdFx0ICogQHJldHVybiB7alF1ZXJ5LkRlZmVycmVkfSBuYW1lc3BhY2VEZWZlcnJlZCBEZWZlcnJlZCBvYmplY3QgdGhhdCBnZXRzIHByb2Nlc3NlZCBhZnRlciB0aGVcblx0XHQgKiBtb2R1bGUgaW5pdGlhbGl6YXRpb24gaXMgZmluaXNoZWQuXG5cdFx0ICovXG5cdFx0aW5pdCgkcGFyZW50ID0gbnVsbCkge1xuXHRcdFx0Ly8gU3RvcmUgdGhlIG5hbWVzcGFjZXMgcmVmZXJlbmNlIG9mIHRoZSBjb2xsZWN0aW9uLlxuXHRcdFx0aWYgKCF0aGlzLm5hbWVzcGFjZSkge1xuXHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0NvbGxlY3Rpb24gY2Fubm90IGJlIGluaXRpYWxpemVkIHdpdGhvdXQgaXRzIHBhcmVudCBuYW1lc3BhY2UgaW5zdGFuY2UuJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIFNldCB0aGUgZGVmYXVsdCBwYXJlbnQtb2JqZWN0IGlmIG5vbmUgd2FzIGdpdmVuLlxuXHRcdFx0aWYgKCRwYXJlbnQgPT09IHVuZGVmaW5lZCB8fCAkcGFyZW50ID09PSBudWxsKSB7XG5cdFx0XHRcdCRwYXJlbnQgPSAkKCdodG1sJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGNvbnN0IGF0dHJpYnV0ZSA9ICdkYXRhLScgKyB0aGlzLm5hbWVzcGFjZS5uYW1lICsgJy0nICsgdGhpcy5hdHRyaWJ1dGU7XG5cdFx0XHRjb25zdCBuYW1lc3BhY2VEZWZlcnJlZCA9ICQuRGVmZXJyZWQoKTtcblx0XHRcdGNvbnN0IGRlZmVycmVkQ29sbGVjdGlvbiA9IFtdO1xuXHRcdFx0XG5cdFx0XHQkcGFyZW50XG5cdFx0XHRcdC5maWx0ZXIoJ1snICsgYXR0cmlidXRlICsgJ10nKVxuXHRcdFx0XHQuYWRkKCRwYXJlbnQuZmluZCgnWycgKyBhdHRyaWJ1dGUgKyAnXScpKVxuXHRcdFx0XHQuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcblx0XHRcdFx0XHRjb25zdCAkZWxlbWVudCA9ICQoZWxlbWVudCk7XG5cdFx0XHRcdFx0Y29uc3QgbW9kdWxlcyA9ICRlbGVtZW50LmF0dHIoYXR0cmlidXRlKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkZWxlbWVudC5yZW1vdmVBdHRyKGF0dHJpYnV0ZSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JC5lYWNoKG1vZHVsZXMucmVwbGFjZSgvKFxcclxcbnxcXG58XFxyfFxcc1xccyspL2dtLCAnICcpLnRyaW0oKS5zcGxpdCgnICcpLCAoaW5kZXgsIG5hbWUpID0+IHtcblx0XHRcdFx0XHRcdGlmIChuYW1lID09PSAnJykge1xuXHRcdFx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0Y29uc3QgZGVmZXJyZWQgPSAkLkRlZmVycmVkKCk7XG5cdFx0XHRcdFx0XHRkZWZlcnJlZENvbGxlY3Rpb24ucHVzaChkZWZlcnJlZCk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGpzZS5jb3JlLm1vZHVsZV9sb2FkZXJcblx0XHRcdFx0XHRcdFx0LmxvYWQoJGVsZW1lbnQsIG5hbWUsIHRoaXMpXG5cdFx0XHRcdFx0XHRcdC5kb25lKChtb2R1bGUpID0+IG1vZHVsZS5pbml0KGRlZmVycmVkKSlcblx0XHRcdFx0XHRcdFx0LmZhaWwoKGVycm9yKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0ZGVmZXJyZWQucmVqZWN0KCk7XG5cdFx0XHRcdFx0XHRcdFx0Ly8gTG9nIHRoZSBlcnJvciBpbiB0aGUgY29uc29sZSBidXQgZG8gbm90IHN0b3AgdGhlIGVuZ2luZSBleGVjdXRpb24uXG5cdFx0XHRcdFx0XHRcdFx0anNlLmNvcmUuZGVidWcuZXJyb3IoJ0NvdWxkIG5vdCBsb2FkIG1vZHVsZTogJyArIG5hbWUsIGVycm9yKTtcblx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBbHdheXMgcmVzb2x2ZSB0aGUgbmFtZXNwYWNlLCBldmVuIGlmIHRoZXJlIGFyZSBtb2R1bGUgZXJyb3JzLlxuXHRcdFx0JC53aGVuLmFwcGx5KHVuZGVmaW5lZCwgZGVmZXJyZWRDb2xsZWN0aW9uKS5hbHdheXMoKCkgPT4gbmFtZXNwYWNlRGVmZXJyZWQucmVzb2x2ZSgpKTtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIGRlZmVycmVkQ29sbGVjdGlvbi5sZW5ndGggPyBuYW1lc3BhY2VEZWZlcnJlZC5wcm9taXNlKCkgOiBuYW1lc3BhY2VEZWZlcnJlZC5yZXNvbHZlKCk7IFxuXHRcdH1cblx0fVxuXHRcblx0anNlLmNvbnN0cnVjdG9ycy5Db2xsZWN0aW9uID0gQ29sbGVjdGlvbjtcbn0pKCk7XG4iLCIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gZGF0YV9iaW5kaW5nLmpzIDIwMTYtMDUtMTdcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4oZnVuY3Rpb24oKSB7XHJcblx0XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIERhdGEgQmluZGluZyBDbGFzcyBcclxuXHQgKiBcclxuXHQgKiBIYW5kbGVzIHR3by13YXkgZGF0YSBiaW5kaW5nIHdpdGggVUkgZWxlbWVudHMuIFxyXG5cdCAqIFxyXG5cdCAqIEBjbGFzcyBKU0UvQ29uc3RydWN0b3JzL0RhdGFCaW5kaW5nXHJcblx0ICovXHJcblx0Y2xhc3MgRGF0YUJpbmRpbmcge1xyXG5cdFx0LyoqXHJcblx0XHQgKiBDbGFzcyBDb25zdHJ1Y3RvciBcclxuXHRcdCAqIFxyXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgVGhlIG5hbWUgb2YgdGhlIGJpbmRpbmcuIFxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9ICRlbGVtZW50IFRhcmdldCBlbGVtZW50IHRvIGJlIGJvbmQuIFxyXG5cdFx0ICovXHJcblx0XHRjb25zdHJ1Y3RvcihuYW1lLCAkZWxlbWVudCkge1xyXG5cdFx0XHR0aGlzLm5hbWUgPSBuYW1lO1xyXG5cdFx0XHR0aGlzLiRlbGVtZW50ID0gJGVsZW1lbnQ7XHJcblx0XHRcdHRoaXMudmFsdWUgPSBudWxsO1xyXG5cdFx0XHR0aGlzLmlzTXV0YWJsZSA9ICRlbGVtZW50LmlzKCdpbnB1dCwgdGV4dGFyZWEsIHNlbGVjdCcpO1xyXG5cdFx0XHR0aGlzLmluaXQoKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBJbml0aWFsaXplIHRoZSBiaW5kaW5nLlxyXG5cdFx0ICovXHJcblx0XHRpbml0KCkge1xyXG5cdFx0XHR0aGlzLiRlbGVtZW50Lm9uKCdjaGFuZ2UnLCAoKSA9PiB7XHJcblx0XHRcdFx0dGhpcy5nZXQoKTtcclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogR2V0IGJpbmRpbmcgdmFsdWUuIFxyXG5cdFx0ICogXHJcblx0XHQgKiBAcmV0dXJucyB7Kn1cclxuXHRcdCAqL1xyXG5cdFx0Z2V0KCkge1xyXG5cdFx0XHR0aGlzLnZhbHVlID0gdGhpcy5pc011dGFibGUgPyB0aGlzLiRlbGVtZW50LnZhbCgpIDogdGhpcy4kZWxlbWVudC5odG1sKCk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAodGhpcy4kZWxlbWVudC5pcygnOmNoZWNrYm94JykgfHwgIHRoaXMuJGVsZW1lbnQuaXMoJzpyYWRpbycpKSB7XHJcblx0XHRcdFx0dGhpcy52YWx1ZSA9IHRoaXMuJGVsZW1lbnQucHJvcCgnY2hlY2tlZCcpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRyZXR1cm4gdGhpcy52YWx1ZTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBTZXQgYmluZGluZyB2YWx1ZS4gXHJcblx0XHQgKiBcclxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSB2YWx1ZVxyXG5cdFx0ICovXHJcblx0XHRzZXQodmFsdWUpIHtcclxuXHRcdFx0dGhpcy52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKHRoaXMuaXNNdXRhYmxlKSB7XHJcblx0XHRcdFx0dGhpcy4kZWxlbWVudC52YWwodmFsdWUpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHRoaXMuJGVsZW1lbnQuaHRtbCh2YWx1ZSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0anNlLmNvbnN0cnVjdG9ycy5EYXRhQmluZGluZyA9IERhdGFCaW5kaW5nO1xyXG59KSgpO1xyXG4iLCIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIG1vZHVsZS5qcyAyMDE2LTA1LTE3XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4oZnVuY3Rpb24oKSB7XG5cdFxuXHQndXNlIHN0cmljdCc7XG5cdFxuXHQvKipcblx0ICogQ2xhc3MgTW9kdWxlXG5cdCAqXG5cdCAqIFRoaXMgY2xhc3MgaXMgdXNlZCBmb3IgcmVwcmVzZW50aW5nIGEgbW9kdWxlIGluc3RhbmNlIHdpdGhpbiB0aGUgSlNFIGVjb3N5c3RlbS5cblx0ICpcblx0ICogQGNsYXNzIEpTRS9Db25zdHJ1Y3RvcnMvTW9kdWxlXG5cdCAqL1xuXHRjbGFzcyBNb2R1bGUge1xuXHRcdC8qKlxuXHRcdCAqIENsYXNzIENvbnN0cnVjdG9yXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gJGVsZW1lbnQgTW9kdWxlIGVsZW1lbnQgc2VsZWN0b3Igb2JqZWN0LlxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIFRoZSBtb2R1bGUgbmFtZSAobWlnaHQgY29udGFpbiB0aGUgcGF0aClcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gY29sbGVjdGlvbiBUaGUgY29sbGVjdGlvbiBpbnN0YW5jZSBvZiB0aGUgbW9kdWxlLlxuXHRcdCAqL1xuXHRcdGNvbnN0cnVjdG9yKCRlbGVtZW50LCBuYW1lLCBjb2xsZWN0aW9uKSB7XG5cdFx0XHR0aGlzLiRlbGVtZW50ID0gJGVsZW1lbnQ7XG5cdFx0XHR0aGlzLm5hbWUgPSBuYW1lO1xuXHRcdFx0dGhpcy5jb2xsZWN0aW9uID0gY29sbGVjdGlvbjtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSB0aGUgbW9kdWxlIGV4ZWN1dGlvbi5cblx0XHQgKlxuXHRcdCAqIFRoaXMgZnVuY3Rpb24gd2lsbCBleGVjdXRlIHRoZSBcImluaXRcIiBtZXRob2Qgb2YgZWFjaCBtb2R1bGUuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gY29sbGVjdGlvbkRlZmVycmVkIERlZmVycmVkIG9iamVjdCB0aGF0IGdldHMgcHJvY2Vzc2VkIGFmdGVyIHRoZSBtb2R1bGVcblx0XHQgKiBpbml0aWFsaXphdGlvbiBpcyBmaW5pc2hlZC5cblx0XHQgKi9cblx0XHRpbml0KGNvbGxlY3Rpb25EZWZlcnJlZCkge1xuXHRcdFx0Ly8gU3RvcmUgbW9kdWxlIGluc3RhbmNlIGFsaWFzLlxuXHRcdFx0Y29uc3QgY2FjaGVkID0gdGhpcy5jb2xsZWN0aW9uLmNhY2hlLm1vZHVsZXNbdGhpcy5uYW1lXTtcblx0XHRcdGxldCB0aW1lb3V0ID0gbnVsbDtcblx0XHRcdFxuXHRcdFx0dHJ5IHtcblx0XHRcdFx0aWYgKCFjYWNoZWQpIHtcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoYE1vZHVsZSBcIiR7dGhpcy5uYW1lfVwiIGNvdWxkIG5vdCBiZSBmb3VuZCBpbiB0aGUgY29sbGVjdGlvbiBjYWNoZS5gKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0Y29uc3QgZGF0YSA9IHRoaXMuX2dldE1vZHVsZURhdGEoKTtcblx0XHRcdFx0Y29uc3QgaW5zdGFuY2UgPSBjYWNoZWQuY29kZS5jYWxsKHRoaXMuJGVsZW1lbnQsIGRhdGEpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly8gUHJvdmlkZSBhIGRvbmUgZnVuY3Rpb24gdGhhdCBuZWVkcyB0byBiZSBjYWxsZWQgZnJvbSB0aGUgbW9kdWxlLCBpbiBvcmRlciB0byBpbmZvcm0gXG5cdFx0XHRcdC8vIHRoYXQgdGhlIG1vZHVsZSBcImluaXRcIiBmdW5jdGlvbiB3YXMgY29tcGxldGVkIHN1Y2Nlc3NmdWxseS5cblx0XHRcdFx0Y29uc3QgZG9uZSA9ICgpID0+IHtcblx0XHRcdFx0XHR0aGlzLiRlbGVtZW50LnRyaWdnZXIoJ2pzZTptb2R1bGU6aW5pdGlhbGl6ZWQnLCBbe21vZHVsZTogdGhpcy5uYW1lfV0pO1xuXHRcdFx0XHRcdGpzZS5jb3JlLmRlYnVnLmluZm8oYE1vZHVsZSBcIiR7dGhpcy5uYW1lfVwiIGluaXRpYWxpemVkIHN1Y2Nlc3NmdWxseS5gKTtcblx0XHRcdFx0XHRjb2xsZWN0aW9uRGVmZXJyZWQucmVzb2x2ZSgpO1xuXHRcdFx0XHRcdGNsZWFyVGltZW91dCh0aW1lb3V0KTtcblx0XHRcdFx0fTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vIExvYWQgdGhlIG1vZHVsZSBkYXRhIGJlZm9yZSB0aGUgbW9kdWxlIGlzIGxvYWRlZC5cblx0XHRcdFx0dGhpcy5fbG9hZE1vZHVsZURhdGEoaW5zdGFuY2UpXG5cdFx0XHRcdFx0LmRvbmUoKCkgPT4ge1xuXHRcdFx0XHRcdFx0Ly8gUmVqZWN0IHRoZSBjb2xsZWN0aW9uRGVmZXJyZWQgaWYgdGhlIG1vZHVsZSBpc24ndCBpbml0aWFsaXplZCBhZnRlciAxMCBzZWNvbmRzLlxuXHRcdFx0XHRcdFx0dGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy53YXJuKCdNb2R1bGUgd2FzIG5vdCBpbml0aWFsaXplZCBhZnRlciAxMCBzZWNvbmRzISAtLSAnICsgdGhpcy5uYW1lKTtcblx0XHRcdFx0XHRcdFx0Y29sbGVjdGlvbkRlZmVycmVkLnJlamVjdCgpO1xuXHRcdFx0XHRcdFx0fSwgMTAwMDApO1xuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRpbnN0YW5jZS5pbml0KGRvbmUpO1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmZhaWwoKGVycm9yKSA9PiB7XG5cdFx0XHRcdFx0XHRjb2xsZWN0aW9uRGVmZXJyZWQucmVqZWN0KCk7XG5cdFx0XHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5lcnJvcignQ291bGQgbm90IGxvYWQgbW9kdWxlXFwncyBtZXRhIGRhdGEuJywgZXJyb3IpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0fSBjYXRjaCAoZXhjZXB0aW9uKSB7XG5cdFx0XHRcdGNvbGxlY3Rpb25EZWZlcnJlZC5yZWplY3QoKTtcblx0XHRcdFx0anNlLmNvcmUuZGVidWcuZXJyb3IoYENhbm5vdCBpbml0aWFsaXplIG1vZHVsZSBcIiR7dGhpcy5uYW1lfVwiLmAsIGV4Y2VwdGlvbik7XG5cdFx0XHRcdCQod2luZG93KS50cmlnZ2VyKCdlcnJvcicsIFtleGNlcHRpb25dKTsgLy8gSW5mb3JtIHRoZSBlbmdpbmUgYWJvdXQgdGhlIGV4Y2VwdGlvbi5cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0cmV0dXJuIGNvbGxlY3Rpb25EZWZlcnJlZC5wcm9taXNlKCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFBhcnNlIHRoZSBtb2R1bGUgZGF0YSBhdHRyaWJ1dGVzLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T2JqZWN0fSBSZXR1cm5zIGFuIG9iamVjdCB0aGF0IGNvbnRhaW5zIHRoZSBkYXRhIG9mIHRoZSBtb2R1bGUuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdF9nZXRNb2R1bGVEYXRhKCkge1xuXHRcdFx0Y29uc3QgZGF0YSA9IHt9O1xuXHRcdFx0XG5cdFx0XHQkLmVhY2godGhpcy4kZWxlbWVudC5kYXRhKCksIChuYW1lLCB2YWx1ZSkgPT4ge1xuXHRcdFx0XHRpZiAobmFtZS5pbmRleE9mKHRoaXMubmFtZSkgPT09IDAgfHwgbmFtZS5pbmRleE9mKHRoaXMubmFtZS50b0xvd2VyQ2FzZSgpKSA9PT0gMCkge1xuXHRcdFx0XHRcdGxldCBrZXkgPSBuYW1lLnN1YnN0cih0aGlzLm5hbWUubGVuZ3RoKTtcblx0XHRcdFx0XHRrZXkgPSBrZXkuc3Vic3RyKDAsIDEpLnRvTG93ZXJDYXNlKCkgKyBrZXkuc3Vic3RyKDEpO1xuXHRcdFx0XHRcdGRhdGFba2V5XSA9IHZhbHVlO1xuXHRcdFx0XHRcdC8vIFJlbW92ZSBkYXRhIGF0dHJpYnV0ZSBmcm9tIGVsZW1lbnQgKHNhbml0aXNlIGNhbWVsIGNhc2UgZmlyc3QpLlxuXHRcdFx0XHRcdGNvbnN0IHNhbml0aXNlZEtleSA9IGtleS5yZXBsYWNlKC8oW2Etel0pKFtBLVpdKS9nLCAnJDEtJDInKS50b0xvd2VyQ2FzZSgpO1xuXHRcdFx0XHRcdHRoaXMuJGVsZW1lbnQucmVtb3ZlQXR0cihgZGF0YS0ke3RoaXMubmFtZX0tJHtzYW5pdGlzZWRLZXl9YCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gZGF0YTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlcyByZXR1cm4gb2JqZWN0cyB3aGljaCBtaWdodCBjb250YWluIHJlcXVpcmVtZW50cy5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBpbnN0YW5jZSBNb2R1bGUgaW5zdGFuY2Ugb2JqZWN0LlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7T2JqZWN0fSBSZXR1cm5zIGEgcHJvbWlzZSBvYmplY3QgdGhhdCB3aWxsIGJlIHJlc29sdmVkIHdoZW4gdGhlIGRhdGEgYXJlIGZldGNoZWQuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdF9sb2FkTW9kdWxlRGF0YShpbnN0YW5jZSkge1xuXHRcdFx0Y29uc3QgZGVmZXJyZWQgPSAkLkRlZmVycmVkKCk7XG5cdFx0XHRjb25zdCBkZWZlcnJlZENvbGxlY3Rpb24gPSBbXTtcblx0XHRcdFxuXHRcdFx0dHJ5IHtcblx0XHRcdFx0aWYgKGluc3RhbmNlLm1vZGVsKSB7XG5cdFx0XHRcdFx0JC5lYWNoKGluc3RhbmNlLm1vZGVsLCBmdW5jdGlvbihpbmRleCwgdXJsKSB7XG5cdFx0XHRcdFx0XHRjb25zdCBtb2RlbERlZmVycmVkID0gJC5EZWZlcnJlZCgpO1xuXHRcdFx0XHRcdFx0ZGVmZXJyZWRDb2xsZWN0aW9uLnB1c2gobW9kZWxEZWZlcnJlZCk7XG5cdFx0XHRcdFx0XHQkLmdldEpTT04odXJsKVxuXHRcdFx0XHRcdFx0XHQuZG9uZSgocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0XHRcdFx0XHRpbnN0YW5jZS5tb2RlbFtpbmRleF0gPSByZXNwb25zZTtcblx0XHRcdFx0XHRcdFx0XHRtb2RlbERlZmVycmVkLnJlc29sdmUocmVzcG9uc2UpO1xuXHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHQuZmFpbCgoZXJyb3IpID0+IHtcblx0XHRcdFx0XHRcdFx0XHRtb2RlbERlZmVycmVkLnJlamVjdChlcnJvcik7XG5cdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoaW5zdGFuY2Uudmlldykge1xuXHRcdFx0XHRcdCQuZWFjaChpbnN0YW5jZS52aWV3LCBmdW5jdGlvbihpbmRleCwgdXJsKSB7XG5cdFx0XHRcdFx0XHRjb25zdCB2aWV3RGVmZXJyZWQgPSAkLkRlZmVycmVkKCk7XG5cdFx0XHRcdFx0XHRkZWZlcnJlZENvbGxlY3Rpb24ucHVzaCh2aWV3RGVmZXJyZWQpO1xuXHRcdFx0XHRcdFx0JC5nZXQodXJsKVxuXHRcdFx0XHRcdFx0XHQuZG9uZSgocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0XHRcdFx0XHRpbnN0YW5jZS52aWV3W2luZGV4XSA9IHJlc3BvbnNlO1xuXHRcdFx0XHRcdFx0XHRcdHZpZXdEZWZlcnJlZC5yZXNvbHZlKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdFx0LmZhaWwoKGVycm9yKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0dmlld0RlZmVycmVkLnJlamVjdChlcnJvcik7XG5cdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAoaW5zdGFuY2UuYmluZGluZ3MpIHtcblx0XHRcdFx0XHRmb3IgKGxldCBuYW1lIGluIGluc3RhbmNlLmJpbmRpbmdzKSB7XG5cdFx0XHRcdFx0XHRjb25zdCAkZWxlbWVudCA9IGluc3RhbmNlLmJpbmRpbmdzW25hbWVdO1xuXHRcdFx0XHRcdFx0aW5zdGFuY2UuYmluZGluZ3NbbmFtZV0gPSBuZXcganNlLmNvbnN0cnVjdG9ycy5EYXRhQmluZGluZyhuYW1lLCAkZWxlbWVudCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQkLndoZW4uYXBwbHkodW5kZWZpbmVkLCBkZWZlcnJlZENvbGxlY3Rpb24pXG5cdFx0XHRcdFx0LmRvbmUoZGVmZXJyZWQucmVzb2x2ZSlcblx0XHRcdFx0XHQuZmFpbCgoZXJyb3IpID0+IHtcblx0XHRcdFx0XHRcdGRlZmVycmVkLnJlamVjdChuZXcgRXJyb3IoYENhbm5vdCBsb2FkIGRhdGEgZm9yIG1vZHVsZSBcIiR7aW5zdGFuY2UubmFtZX1cIi5gLCBlcnJvcikpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0fSBjYXRjaCAoZXhjZXB0aW9uKSB7XG5cdFx0XHRcdGRlZmVycmVkLnJlamVjdChleGNlcHRpb24pO1xuXHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5lcnJvcihgQ2Fubm90IHByZWxvYWQgbW9kdWxlIGRhdGEgZm9yIFwiJHt0aGlzLm5hbWV9XCIuYCwgZXhjZXB0aW9uKTtcblx0XHRcdFx0JCh3aW5kb3cpLnRyaWdnZXIoJ2Vycm9yJywgW2V4Y2VwdGlvbl0pOyAvLyBJbmZvcm0gdGhlIGVuZ2luZSBhYm91dCB0aGUgZXhjZXB0aW9uLlxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRyZXR1cm4gZGVmZXJyZWQucHJvbWlzZSgpO1xuXHRcdH1cblx0fVxuXHRcblx0anNlLmNvbnN0cnVjdG9ycy5Nb2R1bGUgPSBNb2R1bGU7XG59KSgpO1xuIiwiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBuYW1lc3BhY2UuanMgMjAxNi0wNS0xN1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbihmdW5jdGlvbigpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8qKlxuXHQgKiBDbGFzcyBOYW1lc3BhY2Vcblx0ICpcblx0ICogVGhpcyBjbGFzcyBpcyB1c2VkIHRvIGhhbmRsZSBtdWx0aXBsZSBjb2xsZWN0aW9ucyBvZiBtb2R1bGVzLiBFdmVyeSBuYW1lc3BhY2UgaGFzIGl0cyBvd24gc291cmNlIFVSTCBcblx0ICogZm9yIGxvYWRpbmcgdGhlIGRhdGEuIFRoYXQgbWVhbnMgdGhhdCBKU0UgY2FuIGxvYWQgbW9kdWxlcyBmcm9tIG11bHRpcGxlIHBsYWNlcyBhdCB0aGUgc2FtZSB0aW1lLiBcblx0ICpcblx0ICogQGNsYXNzIEpTRS9Db25zdHJ1Y3RvcnMvTmFtZXNwYWNlXG5cdCAqL1xuXHRjbGFzcyBOYW1lc3BhY2Uge1xuXHRcdC8qKlxuXHRcdCAqIENsYXNzIENvbnN0cnVjdG9yXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBUaGUgbmFtZXNwYWNlIG5hbWUgbXVzdCBiZSB1bmlxdWUgd2l0aGluIHRoZSBhcHAuXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IHNvdXJjZSBDb21wbGV0ZSBVUkwgdG8gdGhlIG5hbWVzcGFjZSBtb2R1bGVzIGRpcmVjdG9yeSAod2l0aG91dCB0cmFpbGluZyBzbGFzaCkuXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gY29sbGVjdGlvbnMgQ29udGFpbnMgY29sbGVjdGlvbiBpbnN0YW5jZXMgdG8gYmUgaW5jbHVkZWQgaW4gdGhlIG5hbWVzcGFjZS5cblx0XHQgKi9cblx0XHRjb25zdHJ1Y3RvcihuYW1lLCBzb3VyY2UsIGNvbGxlY3Rpb25zKSB7XG5cdFx0XHR0aGlzLm5hbWUgPSBuYW1lO1xuXHRcdFx0dGhpcy5zb3VyY2UgPSBzb3VyY2U7XG5cdFx0XHR0aGlzLmNvbGxlY3Rpb25zID0gY29sbGVjdGlvbnM7IC8vIGNvbnRhaW5zIHRoZSBkZWZhdWx0IGluc3RhbmNlcyAgIFx0XHRcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSB0aGUgbmFtZXNwYWNlIGNvbGxlY3Rpb25zLlxuXHRcdCAqXG5cdFx0ICogVGhpcyBtZXRob2Qgd2lsbCBjcmVhdGUgbmV3IGNvbGxlY3Rpb24gaW5zdGFuY2VzIGJhc2VkIGluIHRoZSBvcmlnaW5hbCBvbmVzLlxuXHRcdCAqXG5cdFx0ICogQHJldHVybiB7alF1ZXJ5LlByb21pc2V9IFJldHVybnMgYSBwcm9taXNlIHRoYXQgd2lsbCBiZSByZXNvbHZlZCBvbmNlIGV2ZXJ5IG5hbWVzcGFjZSBjb2xsZWN0aW9uXG5cdFx0ICogaXMgcmVzb2x2ZWQuXG5cdFx0ICovXG5cdFx0aW5pdCgpIHtcblx0XHRcdGNvbnN0IGRlZmVycmVkQ29sbGVjdGlvbiA9IFtdO1xuXHRcdFx0XG5cdFx0XHRmb3IgKGxldCBjb2xsZWN0aW9uIG9mIHRoaXMuY29sbGVjdGlvbnMpIHtcblx0XHRcdFx0dGhpc1tjb2xsZWN0aW9uLm5hbWVdID0gbmV3IGpzZS5jb25zdHJ1Y3RvcnMuQ29sbGVjdGlvbihjb2xsZWN0aW9uLm5hbWUsIGNvbGxlY3Rpb24uYXR0cmlidXRlLCB0aGlzKTtcblx0XHRcdFx0Y29uc3QgZGVmZXJyZWQgPSB0aGlzW2NvbGxlY3Rpb24ubmFtZV0uaW5pdCgpO1xuXHRcdFx0XHRkZWZlcnJlZENvbGxlY3Rpb24ucHVzaChkZWZlcnJlZCk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdHJldHVybiBkZWZlcnJlZENvbGxlY3Rpb24ubGVuZ3RoID8gJC53aGVuLmFwcGx5KHVuZGVmaW5lZCwgZGVmZXJyZWRDb2xsZWN0aW9uKS5wcm9taXNlKCkgOiAkLkRlZmVycmVkKCkucmVzb2x2ZSgpO1xuXHRcdH1cblx0fVxuXHRcblx0anNlLmNvbnN0cnVjdG9ycy5OYW1lc3BhY2UgPSBOYW1lc3BhY2U7XG59KSgpO1xuIiwiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBhYm91dC5qcyAyMDE2LTA5LTA4XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBKU0UgSW5mb3JtYXRpb24gTW9kdWxlXG4gKiBcbiAqIEV4ZWN1dGUgdGhlIGBqc2UuYWJvdXQoKWAgY29tbWFuZCBhbmQgeW91IHdpbGwgZ2V0IGEgbmV3IGxvZyBlbnRyeSBpbiB0aGVcbiAqIGNvbnNvbGUgd2l0aCBpbmZvIGFib3V0IHRoZSBlbmdpbmUuIFRoZSBcImFib3V0XCIgbWV0aG9kIGlzIG9ubHkgYXZhaWxhYmxlIGluXG4gKiB0aGUgXCJkZXZlbG9wbWVudFwiIGVudmlyb25tZW50IG9mIHRoZSBlbmdpbmUuXG4gKlxuICogQG1vZHVsZSBKU0UvQ29yZS9hYm91dFxuICovXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZnVuY3Rpb24oKSB7XG5cblx0J3VzZSBzdHJpY3QnO1xuXG5cdGlmIChqc2UuY29yZS5jb25maWcuZ2V0KCdlbnZpcm9ubWVudCcpID09PSAncHJvZHVjdGlvbicpIHtcblx0XHRyZXR1cm47XG5cdH1cblxuXHRqc2UuYWJvdXQgPSBmdW5jdGlvbiAoKSB7XG5cdFx0Y29uc3QgaW5mbyA9IGBcblx0XHRcdEpTIEVOR0lORSB2JHtqc2UuY29yZS5jb25maWcuZ2V0KCd2ZXJzaW9uJyl9IMKpIEdBTUJJTyBHTUJIXG5cdFx0XHQtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XHRUaGUgSlMgRW5naW5lIGVuYWJsZXMgZGV2ZWxvcGVycyB0byBsb2FkIGF1dG9tYXRpY2FsbHkgc21hbGwgcGllY2VzIG9mIGphdmFzY3JpcHQgY29kZSBieVxuXHRcdFx0cGxhY2luZyBzcGVjaWZpYyBkYXRhIGF0dHJpYnV0ZXMgdG8gdGhlIEhUTUwgbWFya3VwIG9mIGEgcGFnZS4gSXQgd2FzIGJ1aWx0IHdpdGggbW9kdWxhcml0eVxuXHRcdFx0aW4gbWluZCBzbyB0aGF0IG1vZHVsZXMgY2FuIGJlIHJldXNlZCBpbnRvIG11bHRpcGxlIHBsYWNlcyB3aXRob3V0IGV4dHJhIGVmZm9ydC4gVGhlIGVuZ2luZVxuXHRcdFx0Y29udGFpbnMgbmFtZXNwYWNlcyB3aGljaCBjb250YWluIGNvbGxlY3Rpb25zIG9mIG1vZHVsZXMsIGVhY2ggb25lIG9mIHdob20gc2VydmUgYSBkaWZmZXJlbnRcblx0XHRcdGdlbmVyaWMgcHVycG9zZS5cblx0XHRcdFZpc2l0IGh0dHA6Ly9kZXZlbG9wZXJzLmdhbWJpby5kZSBmb3IgY29tcGxldGUgcmVmZXJlbmNlIG9mIHRoZSBKUyBFbmdpbmUuXG5cdFx0XHRcblx0XHRcdEZBTExCQUNLIElORk9STUFUSU9OXG5cdFx0XHQtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XHRTaW5jZSB0aGUgZW5naW5lIGNvZGUgYmVjb21lcyBiaWdnZXIgdGhlcmUgYXJlIHNlY3Rpb25zIHRoYXQgbmVlZCB0byBiZSByZWZhY3RvcmVkIGluIG9yZGVyXG5cdFx0XHR0byBiZWNvbWUgbW9yZSBmbGV4aWJsZS4gSW4gbW9zdCBjYXNlcyBhIHdhcm5pbmcgbG9nIHdpbGwgYmUgZGlzcGxheWVkIGF0IHRoZSBicm93c2VyXFwncyBjb25zb2xlXG5cdFx0XHR3aGVuZXZlciB0aGVyZSBpcyBhIHVzZSBvZiBhIGRlcHJlY2F0ZWQgZnVuY3Rpb24uIEJlbG93IHRoZXJlIGlzIGEgcXVpY2sgbGlzdCBvZiBmYWxsYmFjayBzdXBwb3J0XG5cdFx0XHR0aGF0IHdpbGwgYmUgcmVtb3ZlZCBpbiB0aGUgZnV0dXJlIHZlcnNpb25zIG9mIHRoZSBlbmdpbmUuXG5cdFx0XHRcblx0XHRcdDEuIFRoZSBtYWluIGVuZ2luZSBvYmplY3Qgd2FzIHJlbmFtZWQgZnJvbSBcImd4XCIgdG8gXCJqc2VcIiB3aGljaCBzdGFuZHMgZm9yIHRoZSBKYXZhU2NyaXB0IEVuZ2luZS5cblx0XHRcdDIuIFRoZSBcImd4LmxpYlwiIG9iamVjdCBpcyByZW1vdmVkIGFmdGVyIGEgbG9uZyBkZXByZWNhdGlvbiBwZXJpb2QuIFlvdSBzaG91bGQgdXBkYXRlIHRoZSBtb2R1bGVzIFxuXHRcdFx0ICAgdGhhdCBjb250YWluZWQgY2FsbHMgdG8gdGhlIGZ1bmN0aW9ucyBvZiB0aGlzIG9iamVjdC5cblx0XHRcdDMuIFRoZSBneC48Y29sbGVjdGlvbi1uYW1lPi5yZWdpc3RlciBmdW5jdGlvbiBpcyBkZXByZWNhdGVkIGJ5IHYxLjIsIHVzZSB0aGUgXG5cdFx0XHQgICA8bmFtZXNwYWNlPi48Y29sbGVjdGlvbj4ubW9kdWxlKCkgaW5zdGVhZC5cblx0XHRgO1xuXHRcdFxuXHRcdGpzZS5jb3JlLmRlYnVnLmluZm8oaW5mbyk7XG5cdH07XG59KTtcbiIsIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gY29uZmlnLmpzIDIwMTctMDMtMjZcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UuY29yZS5jb25maWcgPSBqc2UuY29yZS5jb25maWcgfHwge307XG5cbi8qKlxuICogSlNFIENvbmZpZ3VyYXRpb24gTW9kdWxlXG4gKlxuICogT25jZSB0aGUgY29uZmlnIG9iamVjdCBpcyBpbml0aWFsaXplZCB5b3UgY2Fubm90IGNoYW5nZSBpdHMgdmFsdWVzLiBUaGlzIGlzIGRvbmUgaW4gb3JkZXIgdG9cbiAqIHByZXZlbnQgdW5wbGVhc2FudCBzaXR1YXRpb25zIHdoZXJlIG9uZSBjb2RlIHNlY3Rpb24gY2hhbmdlcyBhIGNvcmUgY29uZmlnIHNldHRpbmcgdGhhdCBhZmZlY3RzXG4gKiBhbm90aGVyIGNvZGUgc2VjdGlvbiBpbiBhIHdheSB0aGF0IGlzIGhhcmQgdG8gZGlzY292ZXIuXG4gKlxuICogYGBgamF2YXNjcmlwdFxuICogY29uc3QgYXBwVXJsID0ganNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyk7XG4gKiBgYGBcbiAqXG4gKiBAbW9kdWxlIEpTRS9Db3JlL2NvbmZpZ1xuICovXG4oZnVuY3Rpb24oZXhwb3J0cykge1xuXHRcblx0J3VzZSBzdHJpY3QnO1xuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIENPTkZJR1VSQVRJT04gVkFMVUVTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0Y29uc3QgY29uZmlnID0ge1xuXHRcdC8qKlxuXHRcdCAqIEVuZ2luZSBWZXJzaW9uXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdHZlcnNpb246ICcxLjUnLFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFwcCBVUkxcblx0XHQgKlxuXHRcdCAqIGUuZy4gJ2h0dHA6Ly9hcHAuY29tJ1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge1N0cmluZ31cblx0XHQgKi9cblx0XHRhcHBVcmw6IG51bGwsXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2hvcCBVUkxcblx0XHQgKlxuXHRcdCAqIGUuZy4gJ2h0dHA6Ly9zaG9wLmRlJ1xuXHRcdCAqXG5cdFx0ICogQGRlcHJlY2F0ZWQgU2luY2UgdjEuNCwgdXNlIGFwcFVybCBpbnN0ZWFkLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge1N0cmluZ31cblx0XHQgKi9cblx0XHRzaG9wVXJsOiBudWxsLFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEFwcCBWZXJzaW9uXG5cdFx0ICpcblx0XHQgKiBlLmcuICcyLjcuMy4wJ1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge1N0cmluZ31cblx0XHQgKi9cblx0XHRhcHBWZXJzaW9uOiBudWxsLFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFNob3AgVmVyc2lvblxuXHRcdCAqXG5cdFx0ICogZS5nLiAnMi43LjMuMCdcblx0XHQgKlxuXHRcdCAqIEBkZXByZWNhdGVkIFNpbmNlIDEuNCwgdXNlIGFwcFZlcnNpb24gaW5zdGVhZC5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtTdHJpbmd9XG5cdFx0ICovXG5cdFx0c2hvcFZlcnNpb246IG51bGwsXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVVJMIHRvIEpTRW5naW5lIERpcmVjdG9yeS5cblx0XHQgKlxuXHRcdCAqIGUuZy4gJ2h0dHA6Ly9hcHAuY29tL0pTRW5naW5lXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGVuZ2luZVVybDogbnVsbCxcblx0XHRcblx0XHQvKipcblx0XHQgKiBFbmdpbmUgRW52aXJvbm1lbnRcblx0XHQgKlxuXHRcdCAqIERlZmluZXMgdGhlIGZ1bmN0aW9uYWxpdHkgb2YgdGhlIGVuZ2luZSBpbiBtYW55IHNlY3Rpb25zLlxuXHRcdCAqXG5cdFx0ICogVmFsdWVzOiAnZGV2ZWxvcG1lbnQnLCAncHJvZHVjdGlvbidcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtTdHJpbmd9XG5cdFx0ICovXG5cdFx0ZW52aXJvbm1lbnQ6ICdwcm9kdWN0aW9uJyxcblx0XHRcblx0XHQvKipcblx0XHQgKiBUcmFuc2xhdGlvbnMgT2JqZWN0XG5cdFx0ICpcblx0XHQgKiBDb250YWlucyB0aGUgbG9hZGVkIHRyYW5zbGF0aW9ucyB0byBiZSB1c2VkIHdpdGhpbiBKU0VuZ2luZS5cblx0XHQgKlxuXHRcdCAqIEBzZWUganNlLmNvcmUubGFuZyBvYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtPYmplY3R9XG5cdFx0ICovXG5cdFx0dHJhbnNsYXRpb25zOiB7fSxcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgQ29sbGVjdGlvbnNcblx0XHQgKlxuXHRcdCAqIFByb3ZpZGUgYXJyYXkgd2l0aCB7IG5hbWU6ICcnLCBhdHRyaWJ1dGU6ICcnfSBvYmplY3RzIHRoYXQgZGVmaW5lIHRoZSBjb2xsZWN0aW9ucyB0byBiZSB1c2VkIHdpdGhpblxuXHRcdCAqIHRoZSBhcHBsaWNhdGlvbi5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtBcnJheX1cblx0XHQgKi9cblx0XHRjb2xsZWN0aW9uczogW10sXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ3VycmVudCBMYW5ndWFnZSBDb2RlXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGxhbmd1YWdlQ29kZTogJ2RlJyxcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZXQgdGhlIGRlYnVnIGxldmVsIHRvIG9uZSBvZiB0aGUgZm9sbG93aW5nOiAnREVCVUcnLCAnSU5GTycsICdMT0cnLCAnV0FSTicsICdFUlJPUicsXG5cdFx0ICogJ0FMRVJUJywgJ1NJTEVOVCcuXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGRlYnVnOiAnU0lMRU5UJyxcblx0XHRcblx0XHQvKipcblx0XHQgKiBVc2UgY2FjaGUgYnVzdGluZyB0ZWNobmlxdWUgd2hlbiBsb2FkaW5nIG1vZHVsZXMuXG5cdFx0ICpcblx0XHQgKiBAZGVwcmVjYXRlZCBTaW5jZSB2MS40XG5cdFx0ICogXG5cdFx0ICogQHNlZSBqc2UuY29yZS5tb2R1bGVfbG9hZGVyIG9iamVjdFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge0Jvb2xlYW59XG5cdFx0ICovXG5cdFx0Y2FjaGVCdXN0OiB0cnVlLFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFdoZXRoZXIgdGhlIGNsaWVudCBoYXMgYSBtb2JpbGUgaW50ZXJmYWNlLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge0Jvb2xlYW59XG5cdFx0ICovXG5cdFx0bW9iaWxlOiAoL0FuZHJvaWR8d2ViT1N8aVBob25lfGlQYWR8aVBvZHxCbGFja0JlcnJ5fElFTW9iaWxlfE9wZXJhIE1pbmkvaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpKSxcblx0XHRcblx0XHQvKipcblx0XHQgKiBXaGV0aGVyIHRoZSBjbGllbnQgc3VwcG9ydHMgdG91Y2ggZXZlbnRzLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge0Jvb2xlYW59XG5cdFx0ICovXG5cdFx0dG91Y2g6ICgoJ29udG91Y2hzdGFydCcgaW4gd2luZG93KSB8fCB3aW5kb3cub250b3VjaHN0YXJ0IHx8IHdpbmRvdy5vbm1zZ2VzdHVyZWNoYW5nZSkgPyB0cnVlIDogZmFsc2UsXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU3BlY2lmeSB0aGUgcGF0aCBmb3IgdGhlIGZpbGUgbWFuYWdlci5cblx0XHQgKlxuXHRcdCAqIEBkZXByZWNhdGVkIFNpbmNlIHYxLjRcblx0XHQgKiBcblx0XHQgKiBAdHlwZSB7U3RyaW5nfVxuXHRcdCAqL1xuXHRcdGZpbGVtYW5hZ2VyOiAnaW5jbHVkZXMvY2tlZGl0b3IvZmlsZW1hbmFnZXIvaW5kZXguaHRtbCcsXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUGFnZSB0b2tlbiB0byBpbmNsdWRlIGluIGV2ZXJ5IEFKQVggcmVxdWVzdC5cblx0XHQgKlxuXHRcdCAqIFRoZSBwYWdlIHRva2VuIGlzIHVzZWQgdG8gYXZvaWQgQ1NSRiBhdHRhY2tzLiBJdCBtdXN0IGJlIHByb3ZpZGVkIGJ5IHRoZSBiYWNrZW5kIGFuZCBpdCB3aWxsXG5cdFx0ICogYmUgdmFsaWRhdGVkIHRoZXJlLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge1N0cmluZ31cblx0XHQgKi9cblx0XHRwYWdlVG9rZW46ICcnLFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENhY2hlIFRva2VuIFN0cmluZyBcblx0XHQgKiBcblx0XHQgKiBUaGlzIGNvbmZpZ3VyYXRpb24gdmFsdWUgd2lsbCBiZSB1c2VkIGluIHByb2R1Y3Rpb24gZW52aXJvbm1lbnQgZm9yIGNhY2hlIGJ1c3RpbmcuIEl0IG11c3QgXG5cdFx0ICogYmUgcHJvdmlkZWQgd2l0aCB0aGUgd2luZG93LkpTRW5naW5lQ29uZmlndXJhdGlvbiBvYmplY3QuXG5cdFx0ICogXG5cdFx0ICogQHR5cGUge1N0cmluZ31cblx0XHQgKi9cblx0XHRjYWNoZVRva2VuOiAnJyxcblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWZpbmVzIHdoZXRoZXIgdGhlIGhpc3Rvcnkgb2JqZWN0IGlzIGF2YWlsYWJsZS5cblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtCb29sZWFufVxuXHRcdCAqL1xuXHRcdGhpc3Rvcnk6IGhpc3RvcnkgJiYgaGlzdG9yeS5yZXBsYWNlU3RhdGUgJiYgaGlzdG9yeS5wdXNoU3RhdGVcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBCbGFja2xpc3QgY29uZmlnIHZhbHVlcyBpbiBwcm9kdWN0aW9uIGVudmlyb25tZW50LlxuXHQgKiBcblx0ICogQHR5cGUge1N0cmluZ1tdfVxuXHQgKi9cblx0Y29uc3QgYmxhY2tsaXN0ID0gW1xuXHRcdCd2ZXJzaW9uJyxcblx0XHQnYXBwVmVyc2lvbicsXG5cdFx0J3Nob3BWZXJzaW9uJ1xuXHRdO1xuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIFBVQkxJQyBNRVRIT0RTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIEdldCBhIGNvbmZpZ3VyYXRpb24gdmFsdWUuXG5cdCAqXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIFRoZSBjb25maWd1cmF0aW9uIHZhbHVlIG5hbWUgdG8gYmUgcmV0cmlldmVkLlxuXHQgKlxuXHQgKiBAcmV0dXJuIHsqfSBSZXR1cm5zIHRoZSBjb25maWcgdmFsdWUuXG5cdCAqL1xuXHRleHBvcnRzLmdldCA9IGZ1bmN0aW9uKG5hbWUpIHtcblx0XHRpZiAoY29uZmlnLmVudmlyb25tZW50ID09PSAncHJvZHVjdGlvbicgJiYgYmxhY2tsaXN0LmluY2x1ZGVzKG5hbWUpKSB7XG5cdFx0XHRyZXR1cm4gbnVsbDsgXG5cdFx0fVxuXHRcdFxuXHRcdHJldHVybiBjb25maWdbbmFtZV07XG5cdH07XG5cdFxuXHQvKipcblx0ICogSW5pdGlhbGl6ZSB0aGUgSlMgRW5naW5lIGNvbmZpZyBvYmplY3QuXG5cdCAqXG5cdCAqIFRoaXMgbWV0aG9kIHdpbGwgcGFyc2UgdGhlIGdsb2JhbCBcIkpTRW5naW5lQ29uZmlndXJhdGlvblwiIG9iamVjdCBhbmQgdGhlbiByZW1vdmVcblx0ICogaXQgZnJvbSB0aGUgZ2xvYmFsIHNjb3BlIHNvIHRoYXQgaXQgYmVjb21lcyB0aGUgb25seSBjb25maWcgc291cmNlIGZvciBqYXZhc2NyaXB0LlxuXHQgKlxuXHQgKiBOb3RpY2U6IFRoZSBvbmx5IHJlcXVpcmVkIEpTRW5naW5lQ29uZmlndXJhdGlvbiB2YWx1ZXMgYXJlIHRoZSBcImVudmlyb25tZW50XCIgYW5kIHRoZSBcImFwcFVybFwiLlxuXHQgKlxuXHQgKiBAcGFyYW0ge09iamVjdH0ganNFbmdpbmVDb25maWd1cmF0aW9uIE11c3QgY29udGFpbiBpbmZvcm1hdGlvbiB0aGF0IGRlZmluZSBjb3JlIG9wZXJhdGlvbnNcblx0ICogb2YgdGhlIGVuZ2luZS4gQ2hlY2sgdGhlIFwibGlicy9pbml0aWFsaXplXCIgZW50cnkgb2YgdGhlIGVuZ2luZSBkb2N1bWVudGF0aW9uLlxuXHQgKi9cblx0ZXhwb3J0cy5pbml0ID0gZnVuY3Rpb24oanNFbmdpbmVDb25maWd1cmF0aW9uKSB7XG5cdFx0Y29uZmlnLmVudmlyb25tZW50ID0ganNFbmdpbmVDb25maWd1cmF0aW9uLmVudmlyb25tZW50O1xuXHRcdGNvbmZpZy5hcHBVcmwgPSBqc0VuZ2luZUNvbmZpZ3VyYXRpb24uYXBwVXJsLnJlcGxhY2UoL1xcLyskLywgJycpOyAvLyBSZW1vdmUgdHJhaWxpbmcgc2xhc2ggZnJvbSBhcHBVcmwuXG5cdFx0XG5cdFx0aWYgKGNvbmZpZy5lbnZpcm9ubWVudCA9PT0gJ2RldmVsb3BtZW50Jykge1xuXHRcdFx0Y29uZmlnLmNhY2hlQnVzdCA9IGZhbHNlO1xuXHRcdFx0Y29uZmlnLm1pbmlmaWVkID0gZmFsc2U7XG5cdFx0XHRjb25maWcuZGVidWcgPSAnREVCVUcnO1xuXHRcdH1cblx0XHRcblx0XHRpZiAoanNFbmdpbmVDb25maWd1cmF0aW9uLmVuZ2luZVVybCAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRjb25maWcuZW5naW5lVXJsID0ganNFbmdpbmVDb25maWd1cmF0aW9uLmVuZ2luZVVybC5yZXBsYWNlKC9cXC8rJC8sICcnKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0Y29uZmlnLmVuZ2luZVVybCA9IGNvbmZpZy5hcHBVcmwgKyAnL0pTRW5naW5lL2J1aWxkJztcblx0XHR9XG5cdFx0XG5cdFx0aWYgKGpzRW5naW5lQ29uZmlndXJhdGlvbi50cmFuc2xhdGlvbnMgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0Y29uZmlnLnRyYW5zbGF0aW9ucyA9IGpzRW5naW5lQ29uZmlndXJhdGlvbi50cmFuc2xhdGlvbnM7XG5cdFx0XHRcblx0XHRcdGZvciAobGV0IHNlY3Rpb25OYW1lIGluIGNvbmZpZy50cmFuc2xhdGlvbnMpIHtcblx0XHRcdFx0anNlLmNvcmUubGFuZy5hZGRTZWN0aW9uKHNlY3Rpb25OYW1lLCBjb25maWcudHJhbnNsYXRpb25zW3NlY3Rpb25OYW1lXSk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdGlmIChqc0VuZ2luZUNvbmZpZ3VyYXRpb24uY29sbGVjdGlvbnMgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0Y29uZmlnLmNvbGxlY3Rpb25zID0ganNFbmdpbmVDb25maWd1cmF0aW9uLmNvbGxlY3Rpb25zO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRjb25maWcuY29sbGVjdGlvbnMgPSBbXG5cdFx0XHRcdHtuYW1lOiAnY29udHJvbGxlcnMnLCBhdHRyaWJ1dGU6ICdjb250cm9sbGVyJ30sXG5cdFx0XHRcdHtuYW1lOiAnZXh0ZW5zaW9ucycsIGF0dHJpYnV0ZTogJ2V4dGVuc2lvbid9LFxuXHRcdFx0XHR7bmFtZTogJ3dpZGdldHMnLCBhdHRyaWJ1dGU6ICd3aWRnZXQnfVxuXHRcdFx0XVxuXHRcdH1cblx0XHRcblx0XHRpZiAoanNFbmdpbmVDb25maWd1cmF0aW9uLmFwcFZlcnNpb24gIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0Y29uZmlnLmFwcFZlcnNpb24gPSBqc0VuZ2luZUNvbmZpZ3VyYXRpb24uYXBwVmVyc2lvbjtcblx0XHR9XG5cdFx0XG5cdFx0aWYgKGpzRW5naW5lQ29uZmlndXJhdGlvbi5zaG9wVXJsICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdGpzZS5jb3JlLmRlYnVnLndhcm4oJ0pTIEVuZ2luZTogXCJzaG9wVXJsXCIgaXMgZGVwcmVjYXRlZCBhbmQgd2lsbCBiZSByZW1vdmVkIGluIEpTIEVuZ2luZSB2MS41LCBwbGVhc2UgJ1xuXHRcdFx0XHQrICd1c2UgdGhlIFwiYXBwVXJsXCIgaW5zdGVhZC4nKTtcblx0XHRcdGNvbmZpZy5zaG9wVXJsID0ganNFbmdpbmVDb25maWd1cmF0aW9uLnNob3BVcmwucmVwbGFjZSgvXFwvKyQvLCAnJyk7XG5cdFx0XHRjb25maWcuYXBwVXJsID0gY29uZmlnLmFwcFVybCB8fCBjb25maWcuc2hvcFVybDsgLy8gTWFrZSBzdXJlIHRoZSBcImFwcFVybFwiIHZhbHVlIGlzIG5vdCBlbXB0eS5cblx0XHR9XG5cdFx0XG5cdFx0aWYgKGpzRW5naW5lQ29uZmlndXJhdGlvbi5zaG9wVmVyc2lvbiAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRqc2UuY29yZS5kZWJ1Zy53YXJuKCdKUyBFbmdpbmU6IFwic2hvcFZlcnNpb25cIiBpcyBkZXByZWNhdGVkIGFuZCB3aWxsIGJlIHJlbW92ZWQgaW4gSlMgRW5naW5lIHYxLjUsIHBsZWFzZSAnXG5cdFx0XHRcdCsgJ3VzZSB0aGUgXCJhcHBWZXJzaW9uXCIgaW5zdGVhZC4nKTtcblx0XHRcdGNvbmZpZy5zaG9wVmVyc2lvbiA9IGpzRW5naW5lQ29uZmlndXJhdGlvbi5zaG9wVmVyc2lvbjtcblx0XHR9XG5cdFx0XG5cdFx0aWYgKGpzRW5naW5lQ29uZmlndXJhdGlvbi5wcmVmaXggIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0Y29uZmlnLnByZWZpeCA9IGpzRW5naW5lQ29uZmlndXJhdGlvbi5wcmVmaXg7XG5cdFx0fVxuXHRcdFxuXHRcdGlmIChqc0VuZ2luZUNvbmZpZ3VyYXRpb24ubGFuZ3VhZ2VDb2RlICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdGNvbmZpZy5sYW5ndWFnZUNvZGUgPSBqc0VuZ2luZUNvbmZpZ3VyYXRpb24ubGFuZ3VhZ2VDb2RlO1xuXHRcdH1cblx0XHRcblx0XHRpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2luaXQtanMnKSAhPT0gbnVsbFxuXHRcdFx0JiYgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2luaXQtanMnKS5oYXNBdHRyaWJ1dGUoJ2RhdGEtcGFnZS10b2tlbicpKSB7XG5cdFx0XHRqc0VuZ2luZUNvbmZpZ3VyYXRpb24ucGFnZVRva2VuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2luaXQtanMnKS5nZXRBdHRyaWJ1dGUoJ2RhdGEtcGFnZS10b2tlbicpO1xuXHRcdH1cblx0XHRcblx0XHRpZiAoanNFbmdpbmVDb25maWd1cmF0aW9uLnBhZ2VUb2tlbiAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRjb25maWcucGFnZVRva2VuID0ganNFbmdpbmVDb25maWd1cmF0aW9uLnBhZ2VUb2tlbjtcblx0XHR9XG5cdFx0XG5cdFx0aWYgKGpzRW5naW5lQ29uZmlndXJhdGlvbi5jYWNoZVRva2VuICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdGNvbmZpZy5jYWNoZVRva2VuID0ganNFbmdpbmVDb25maWd1cmF0aW9uLmNhY2hlVG9rZW47XG5cdFx0fVxuXHRcdFxuXHRcdC8vIEFkZCB0aGUgXCJ0b3VjaEV2ZW50c1wiIGVudHJ5IHNvIHRoYXQgbW9kdWxlcyBjYW4gYmluZCB2YXJpb3VzIHRvdWNoIGV2ZW50cyBkZXBlbmRpbmcgdGhlIGJyb3dzZXIuXG5cdFx0Y29uc3QgZ2VuZXJhbFRvdWNoRXZlbnRzID0ge1xuXHRcdFx0c3RhcnQ6ICd0b3VjaHN0YXJ0Jyxcblx0XHRcdGVuZDogJ3Ryb3VjaGVuZCcsXG5cdFx0XHRtb3ZlOiAndG91Y2htb3ZlJ1xuXHRcdH07XG5cdFx0XG5cdFx0Y29uc3QgbWljcm9zb2Z0VG91Y2hFdmVudHMgPSB7XG5cdFx0XHRzdGFydDogJ3BvaW50ZXJkb3duJyxcblx0XHRcdGVuZDogJ3BvaW50ZXJ1cCcsXG5cdFx0XHRtb3ZlOiAncG9pbnRlcm1vdmUnXG5cdFx0fTtcblx0XHRcblx0XHRjb25maWcudG91Y2hFdmVudHMgPSAod2luZG93Lm9ubXNnZXN0dXJlY2hhbmdlKSA/IG1pY3Jvc29mdFRvdWNoRXZlbnRzIDogZ2VuZXJhbFRvdWNoRXZlbnRzO1xuXHRcdFxuXHRcdC8vIFNldCBpbml0aWFsIHJlZ2lzdHJ5IHZhbHVlcy4gXG5cdFx0Zm9yIChsZXQgZW50cnkgaW4ganNFbmdpbmVDb25maWd1cmF0aW9uLnJlZ2lzdHJ5KSB7XG5cdFx0XHRqc2UuY29yZS5yZWdpc3RyeS5zZXQoZW50cnksIGpzRW5naW5lQ29uZmlndXJhdGlvbi5yZWdpc3RyeVtlbnRyeV0pOyBcblx0XHR9XG5cdFx0XG5cdFx0Ly8gSW5pdGlhbGl6ZSB0aGUgbW9kdWxlIGxvYWRlciBvYmplY3QuXG5cdFx0anNlLmNvcmUubW9kdWxlX2xvYWRlci5pbml0KCk7XG5cdFx0XG5cdFx0Ly8gRGVzdHJveSBnbG9iYWwgRW5naW5lQ29uZmlndXJhdGlvbiBvYmplY3QuXG5cdFx0ZGVsZXRlIHdpbmRvdy5KU0VuZ2luZUNvbmZpZ3VyYXRpb247XG5cdH07XG5cdFxufShqc2UuY29yZS5jb25maWcpKTtcbiIsIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZGVidWcuanMgMjAxNi0wOS0wOFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmpzZS5jb3JlLmRlYnVnID0ganNlLmNvcmUuZGVidWcgfHwge307XG5cbi8qKlxuICogSlNFIERlYnVnIE1vZHVsZVxuICpcbiAqIFRoaXMgb2JqZWN0IHByb3ZpZGVzIGFuIHdyYXBwZXIgdG8gdGhlIGNvbnNvbGUubG9nIGZ1bmN0aW9uIGFuZCBlbmFibGVzIGVhc3kgdXNlXG4gKiBvZiB0aGUgZGlmZmVyZW50IGxvZyB0eXBlcyBsaWtlIFwiaW5mb1wiLCBcIndhcm5pbmdcIiwgXCJlcnJvclwiIGV0Yy5cbiAqXG4gKiBAbW9kdWxlIEpTRS9Db3JlL2RlYnVnXG4gKi9cbihmdW5jdGlvbihleHBvcnRzKSB7XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBWQVJJQUJMRVNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQvKipcblx0ICogQHR5cGUge1N0cmluZ31cblx0ICovXG5cdGNvbnN0IFRZUEVfREVCVUcgPSAnREVCVUcnO1xuXHRcblx0LyoqXG5cdCAqIEB0eXBlIHtTdHJpbmd9XG5cdCAqL1xuXHRjb25zdCBUWVBFX0lORk8gPSAnSU5GTyc7XG5cdFxuXHQvKipcblx0ICogQHR5cGUge1N0cmluZ31cblx0ICovXG5cdGNvbnN0IFRZUEVfTE9HID0gJ0xPRyc7XG5cdFxuXHQvKipcblx0ICogQHR5cGUge1N0cmluZ31cblx0ICovXG5cdGNvbnN0IFRZUEVfV0FSTiA9ICdXQVJOJztcblx0XG5cdC8qKlxuXHQgKiBAdHlwZSB7U3RyaW5nfVxuXHQgKi9cblx0Y29uc3QgVFlQRV9FUlJPUiA9ICdFUlJPUic7XG5cdFxuXHQvKipcblx0ICogQHR5cGUge1N0cmluZ31cblx0ICovXG5cdGNvbnN0IFRZUEVfQUxFUlQgPSAnQUxFUlQnO1xuXHRcblx0LyoqXG5cdCAqIEB0eXBlIHtTdHJpbmd9XG5cdCAqL1xuXHRjb25zdCBUWVBFX01PQklMRSA9ICdNT0JJTEUnO1xuXHRcblx0LyoqXG5cdCAqIEB0eXBlIHtTdHJpbmd9XG5cdCAqL1xuXHRjb25zdCBUWVBFX1NJTEVOVCA9ICdTSUxFTlQnO1xuXHRcblx0LyoqXG5cdCAqIEFsbCBwb3NzaWJsZSBkZWJ1ZyBsZXZlbHMgaW4gdGhlIG9yZGVyIG9mIGltcG9ydGFuY2UuXG5cdCAqXG5cdCAqIEB0eXBlIHtTdHJpbmdbXX1cblx0ICovXG5cdGNvbnN0IGxldmVscyA9IFtcblx0XHRUWVBFX0RFQlVHLFxuXHRcdFRZUEVfSU5GTyxcblx0XHRUWVBFX0xPRyxcblx0XHRUWVBFX1dBUk4sXG5cdFx0VFlQRV9FUlJPUixcblx0XHRUWVBFX0FMRVJULFxuXHRcdFRZUEVfTU9CSUxFLFxuXHRcdFRZUEVfU0lMRU5UXG5cdF07XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gRlVOQ1RJT05TXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIFNldCBGYXZpY29uIHRvIEVycm9yIFN0YXRlLlxuXHQgKlxuXHQgKiBUaGlzIG1ldGhvZCB3aWxsIG9ubHkgd29yayBpZiA8Y2FudmFzPiBpcyBzdXBwb3J0ZWQgZnJvbSB0aGUgYnJvd3Nlci5cblx0ICpcblx0ICogQHByaXZhdGVcblx0ICovXG5cdGZ1bmN0aW9uIF9zZXRGYXZpY29uVG9FcnJvclN0YXRlKCkge1xuXHRcdGNvbnN0IGNhbnZhcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2NhbnZhcycpO1xuXHRcdGNvbnN0IGZhdmljb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdbcmVsPVwic2hvcnRjdXQgaWNvblwiXScpO1xuXHRcdFxuXHRcdGlmIChjYW52YXMuZ2V0Q29udGV4dCAmJiAhZmF2aWNvbi5jbGFzc05hbWUuaW5jbHVkZXMoJ2Vycm9yLXN0YXRlJykpIHtcblx0XHRcdGNvbnN0IGltZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2ltZycpO1xuXHRcdFx0Y2FudmFzLmhlaWdodCA9IGNhbnZhcy53aWR0aCA9IDE2O1xuXHRcdFx0Y29uc3QgY3R4ID0gY2FudmFzLmdldENvbnRleHQoJzJkJyk7XG5cdFx0XHRpbWcub25sb2FkID0gZnVuY3Rpb24oKSB7IC8vIENvbnRpbnVlIG9uY2UgdGhlIGltYWdlIGhhcyBiZWVuIGxvYWRlZC4gXG5cdFx0XHRcdGN0eC5kcmF3SW1hZ2UodGhpcywgMCwgMCk7XG5cdFx0XHRcdGN0eC5nbG9iYWxBbHBoYSA9IDAuNjU7XG5cdFx0XHRcdGN0eC5maWxsU3R5bGUgPSAnI0ZGMDAwMCc7XG5cdFx0XHRcdGN0eC5yZWN0KDAsIDAsIDE2LCAxNik7XG5cdFx0XHRcdGN0eC5maWxsKCk7XG5cdFx0XHRcdGZhdmljb24uaHJlZiA9IGNhbnZhcy50b0RhdGFVUkwoJ2ltYWdlL3BuZycpO1xuXHRcdFx0XHRmYXZpY29uLmNsYXNzTmFtZSArPSAnZXJyb3Itc3RhdGUnO1xuXHRcdFx0fTtcblx0XHRcdGltZy5zcmMgPSBmYXZpY29uLmhyZWY7XG5cdFx0fVxuXHR9XG5cdFxuXHQvKipcblx0ICogRXJyb3IgaGFuZGxlciB0aGF0IGZldGNoZXMgYWxsIGV4Y2VwdGlvbnMgdGhyb3duIGJ5IHRoZSBqYXZhc2NyaXB0LlxuXHQgKlxuXHQgKiBAcHJpdmF0ZVxuXHQgKi9cblx0ZnVuY3Rpb24gX2dsb2JhbEVycm9ySGFuZGxlcigpIHtcblx0XHRpZiAoanNlLmNvcmUuY29uZmlnLmdldCgnZW52aXJvbm1lbnQnKSAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG5cdFx0XHQvLyBMb2cgdGhlIGVycm9yIGluIHRoZSBicm93c2VyJ3MgY29uc29sZS4gXG5cdFx0XHRpZiAoanNlLmNvcmUuZGVidWcgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRqc2UuY29yZS5kZWJ1Zy5lcnJvcignSlMgRW5naW5lIEVycm9yIEhhbmRsZXInLCBhcmd1bWVudHMpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Y29uc29sZS5sb2coJ0pTIEVuZ2luZSBFcnJvciBIYW5kbGVyJywgYXJndW1lbnRzKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0Ly8gVXBkYXRlIHRoZSBwYWdlIHRpdGxlIHdpdGggYW4gZXJyb3IgY291bnQuXG5cdFx0XHRjb25zdCByZWdleCA9IC8uXFwgXFxbKC4rKVxcXVxcIC87XG5cdFx0XHRsZXQgdGl0bGUgPSB3aW5kb3cuZG9jdW1lbnQudGl0bGU7XG5cdFx0XHRsZXQgZXJyb3JDb3VudCA9IDE7XG5cdFx0XHRcblx0XHRcdC8vIEdldHMgdGhlIGN1cnJlbnQgZXJyb3IgY291bnQgYW5kIHJlY3JlYXRlcyB0aGUgZGVmYXVsdCB0aXRsZSBvZiB0aGUgcGFnZS5cblx0XHRcdGlmICh0aXRsZS5tYXRjaChyZWdleCkgIT09IG51bGwpIHtcblx0XHRcdFx0ZXJyb3JDb3VudCA9IHBhcnNlSW50KHRpdGxlLm1hdGNoKC9cXGQrLylbMF0sIDEwKSArIDE7XG5cdFx0XHRcdHRpdGxlID0gdGl0bGUucmVwbGFjZShyZWdleCwgJycpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBSZS1jcmVhdGVzIHRoZSBlcnJvciBmbGFnIGF0IHRoZSB0aXRsZSB3aXRoIHRoZSBuZXcgZXJyb3IgY291bnQuXG5cdFx0XHR0aXRsZSA9ICfinJYgWycgKyBlcnJvckNvdW50ICsgJ10gJyArIHRpdGxlO1xuXHRcdFx0d2luZG93LmRvY3VtZW50LnRpdGxlID0gdGl0bGU7XG5cdFx0XHRcblx0XHRcdC8vIFNldCBGYXZpY29uIHRvIEVycm9yIFN0YXRlLlxuXHRcdFx0X3NldEZhdmljb25Ub0Vycm9yU3RhdGUoKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIHRydWU7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBFeGVjdXRlcyB0aGUgY29ycmVjdCBjb25zb2xlL2FsZXJ0IHN0YXRlbWVudC5cblx0ICpcblx0ICogQHBhcmFtIHtPYmplY3R9IGNhbGxlciAob3B0aW9uYWwpIENvbnRhaW5zIHRoZSBjYWxsZXIgaW5mb3JtYXRpb24gdG8gYmUgZGlzcGxheWVkLlxuXHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YSAob3B0aW9uYWwpIENvbnRhaW5zIGFueSBhZGRpdGlvbmFsIGRhdGEgdG8gYmUgaW5jbHVkZWQgaW4gdGhlIGRlYnVnIG91dHB1dC5cblx0ICpcblx0ICogQHByaXZhdGVcblx0ICovXG5cdGZ1bmN0aW9uIF9leGVjdXRlKGNhbGxlciwgZGF0YSkge1xuXHRcdGNvbnN0IGN1cnJlbnRMb2dJbmRleCA9IGxldmVscy5pbmRleE9mKGNhbGxlcik7XG5cdFx0Y29uc3QgYWxsb3dlZExvZ0luZGV4ID0gbGV2ZWxzLmluZGV4T2YoanNlLmNvcmUuY29uZmlnLmdldCgnZGVidWcnKSk7XG5cdFx0bGV0IGNvbnNvbGVNZXRob2QgPSBudWxsO1xuXHRcdFxuXHRcdGlmIChjdXJyZW50TG9nSW5kZXggPj0gYWxsb3dlZExvZ0luZGV4KSB7XG5cdFx0XHRjb25zb2xlTWV0aG9kID0gY2FsbGVyLnRvTG93ZXJDYXNlKCk7XG5cdFx0XHRcblx0XHRcdHN3aXRjaCAoY29uc29sZU1ldGhvZCkge1xuXHRcdFx0XHRjYXNlICdhbGVydCc6XG5cdFx0XHRcdFx0YWxlcnQoSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcblx0XHRcdFx0Y2FzZSAnbW9iaWxlJzpcblx0XHRcdFx0XHRjb25zdCAkbW9iaWxlRGVidWdNb2RhbCA9ICQoJy5tb2JpbGUtZGVidWctbW9kYWwnKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAoISRtb2JpbGVEZWJ1Z01vZGFsLmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0JCgnPGRpdiAvPicpXG5cdFx0XHRcdFx0XHRcdC5hZGRDbGFzcygnbW9iaWxlLWRlYnVnLW1vZGFsJylcblx0XHRcdFx0XHRcdFx0LmNzcyh7XG5cdFx0XHRcdFx0XHRcdFx0cG9zaXRpb246ICdmaXhlZCcsXG5cdFx0XHRcdFx0XHRcdFx0dG9wOiAwLFxuXHRcdFx0XHRcdFx0XHRcdGxlZnQ6IDAsXG5cdFx0XHRcdFx0XHRcdFx0bWF4SGVpZ2h0OiAnNTAlJyxcblx0XHRcdFx0XHRcdFx0XHRtaW5XaWR0aDogJzIwMHB4Jyxcblx0XHRcdFx0XHRcdFx0XHRtYXhXaWR0aDogJzMwMHB4Jyxcblx0XHRcdFx0XHRcdFx0XHRiYWNrZ3JvdW5kQ29sb3I6ICdjcmltc29uJyxcblx0XHRcdFx0XHRcdFx0XHR6SW5kZXg6IDEwMDAwMCxcblx0XHRcdFx0XHRcdFx0XHRvdmVyZmxvdzogJ3Njcm9sbCdcblx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdFx0LmFwcGVuZFRvKCQoJ2JvZHknKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdCRtb2JpbGVEZWJ1Z01vZGFsLmFwcGVuZCgnPHA+JyArIEpTT04uc3RyaW5naWZ5KGRhdGEpICsgJzwvcD4nKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XG5cdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0aWYgKGNvbnNvbGUgPT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRcdFx0cmV0dXJuOyAvLyBUaGVyZSBpcyBubyBjb25zb2xlIHN1cHBvcnQgc28gZG8gbm90IHByb2NlZWQuXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmICh0eXBlb2YgY29uc29sZVtjb25zb2xlTWV0aG9kXS5hcHBseSA9PT0gJ2Z1bmN0aW9uJyB8fCB0eXBlb2YgY29uc29sZS5sb2cuYXBwbHkgPT09ICdmdW5jdGlvbicpIHtcblx0XHRcdFx0XHRcdGlmIChjb25zb2xlW2NvbnNvbGVNZXRob2RdICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0XHRcdFx0Y29uc29sZVtjb25zb2xlTWV0aG9kXS5hcHBseShjb25zb2xlLCBkYXRhKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nLmFwcGx5KGNvbnNvbGUsIGRhdGEpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhkYXRhKTtcblx0XHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cdFxuXHQvKipcblx0ICogQmluZCBHbG9iYWwgRXJyb3IgSGFuZGxlclxuXHQgKi9cblx0ZXhwb3J0cy5iaW5kR2xvYmFsRXJyb3JIYW5kbGVyID0gZnVuY3Rpb24oKSB7XG5cdFx0d2luZG93Lm9uZXJyb3IgPSBfZ2xvYmFsRXJyb3JIYW5kbGVyO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIFJlcGxhY2VzIGNvbnNvbGUuZGVidWdcblx0ICpcblx0ICogQHBhcmFtcyB7Kn0gYXJndW1lbnRzIEFueSBkYXRhIHRoYXQgc2hvdWxkIGJlIHNob3duIGluIHRoZSBjb25zb2xlIHN0YXRlbWVudC5cblx0ICovXG5cdGV4cG9ydHMuZGVidWcgPSBmdW5jdGlvbigpIHtcblx0XHRfZXhlY3V0ZShUWVBFX0RFQlVHLCBhcmd1bWVudHMpO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIFJlcGxhY2VzIGNvbnNvbGUuaW5mb1xuXHQgKlxuXHQgKiBAcGFyYW1zIHsqfSBhcmd1bWVudHMgQW55IGRhdGEgdGhhdCBzaG91bGQgYmUgc2hvd24gaW4gdGhlIGNvbnNvbGUgc3RhdGVtZW50LlxuXHQgKi9cblx0ZXhwb3J0cy5pbmZvID0gZnVuY3Rpb24oKSB7XG5cdFx0X2V4ZWN1dGUoVFlQRV9JTkZPLCBhcmd1bWVudHMpO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIFJlcGxhY2VzIGNvbnNvbGUubG9nXG5cdCAqXG5cdCAqIEBwYXJhbXMgeyp9IGFyZ3VtZW50cyBBbnkgZGF0YSB0aGF0IHNob3VsZCBiZSBzaG93biBpbiB0aGUgY29uc29sZSBzdGF0ZW1lbnQuXG5cdCAqL1xuXHRleHBvcnRzLmxvZyA9IGZ1bmN0aW9uKCkge1xuXHRcdF9leGVjdXRlKFRZUEVfTE9HLCBhcmd1bWVudHMpO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIFJlcGxhY2VzIGNvbnNvbGUud2FyblxuXHQgKlxuXHQgKiBAcGFyYW1zIHsqfSBhcmd1bWVudHMgQW55IGRhdGEgdGhhdCBzaG91bGQgYmUgc2hvd24gaW4gdGhlIGNvbnNvbGUgc3RhdGVtZW50LlxuXHQgKi9cblx0ZXhwb3J0cy53YXJuID0gZnVuY3Rpb24oKSB7XG5cdFx0X2V4ZWN1dGUoVFlQRV9XQVJOLCBhcmd1bWVudHMpO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIFJlcGxhY2VzIGNvbnNvbGUuZXJyb3Jcblx0ICpcblx0ICogQHBhcmFtIHsqfSBhcmd1bWVudHMgQW55IGRhdGEgdGhhdCBzaG91bGQgYmUgc2hvd24gaW4gdGhlIGNvbnNvbGUgc3RhdGVtZW50LlxuXHQgKi9cblx0ZXhwb3J0cy5lcnJvciA9IGZ1bmN0aW9uKCkge1xuXHRcdF9leGVjdXRlKFRZUEVfRVJST1IsIGFyZ3VtZW50cyk7XG5cdH07XG5cdFxuXHQvKipcblx0ICogUmVwbGFjZXMgYWxlcnRcblx0ICpcblx0ICogQHBhcmFtIHsqfSBhcmd1bWVudHMgQW55IGRhdGEgdGhhdCBzaG91bGQgYmUgc2hvd24gaW4gdGhlIGNvbnNvbGUgc3RhdGVtZW50LlxuXHQgKi9cblx0ZXhwb3J0cy5hbGVydCA9IGZ1bmN0aW9uKCkge1xuXHRcdF9leGVjdXRlKFRZUEVfQUxFUlQsIGFyZ3VtZW50cyk7XG5cdH07XG5cdFxuXHQvKipcblx0ICogRGVidWcgaW5mbyBmb3IgbW9iaWxlIGRldmljZXMuXG5cdCAqXG5cdCAqIEBwYXJhbSB7Kn0gYXJndW1lbnRzIEFueSBkYXRhIHRoYXQgc2hvdWxkIGJlIHNob3duIGluIHRoZSBjb25zb2xlIHN0YXRlbWVudC5cblx0ICovXG5cdGV4cG9ydHMubW9iaWxlID0gZnVuY3Rpb24oKSB7XG5cdFx0X2V4ZWN1dGUoVFlQRV9NT0JJTEUsIGFyZ3VtZW50cyk7XG5cdH07XG5cdFxufShqc2UuY29yZS5kZWJ1ZykpO1xuIiwiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBlbmdpbmUuanMgMjAxNi0wOS0wOFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmpzZS5jb3JlLmVuZ2luZSA9IGpzZS5jb3JlLmVuZ2luZSB8fCB7fTtcblxuLyoqXG4gKiBKU0UgQ29yZSBNb2R1bGVcbiAqXG4gKiBUaGlzIG9iamVjdCB3aWxsIGluaXRpYWxpemUgdGhlIHBhZ2UgbmFtZXNwYWNlcyBhbmQgY29sbGVjdGlvbnMuXG4gKlxuICogQG1vZHVsZSBKU0UvQ29yZS9lbmdpbmVcbiAqL1xuKGZ1bmN0aW9uKGV4cG9ydHMpIHtcblxuXHQndXNlIHN0cmljdCc7XG5cblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIFBSSVZBVEUgRlVOQ1RJT05TXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdC8qKlxuXHQgKiBJbml0aWFsaXplIHRoZSBwYWdlIG5hbWVzcGFjZXMuXG5cdCAqXG5cdCAqIFRoaXMgbWV0aG9kIHdpbGwgc2VhcmNoIHRoZSBwYWdlIEhUTUwgZm9yIGF2YWlsYWJsZSBuYW1lc3BhY2VzLlxuXHQgKlxuXHQgKiBAcGFyYW0ge0FycmF5fSBjb2xsZWN0aW9ucyBDb250YWlucyB0aGUgbW9kdWxlIGNvbGxlY3Rpb24gaW5zdGFuY2VzIHRvIGJlIGluY2x1ZGVkIGluIHRoZSBuYW1lc3BhY2VzLlxuXHQgKlxuXHQgKiBAcmV0dXJuIHtBcnJheX0gUmV0dXJucyBhbiBhcnJheSB3aXRoIHRoZSBwYWdlIG5hbWVzcGFjZSBuYW1lcy5cblx0ICpcblx0ICogQHByaXZhdGVcblx0ICovXG5cdGZ1bmN0aW9uIF9pbml0TmFtZXNwYWNlcyhjb2xsZWN0aW9ucykge1xuXHRcdGxldCBwYWdlTmFtZXNwYWNlTmFtZXMgPSBbXTtcblxuXHRcdC8vIFVzZSB0aGUgY3VzdG9tIHBzZXVkbyBzZWxlY3RvciBkZWZpbmVkIGF0IGV4dGVuZC5qcyBpbiBvcmRlciB0byBmZXRjaCB0aGUgYXZhaWxhYmxlIG5hbWVzcGFjZXMuXG5cdFx0bGV0IG5vZGVzID0gQXJyYXkuZnJvbShkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnKicpKSxcblx0XHRcdHJlZ2V4ID0gL2RhdGEtKC4qKS1uYW1lc3BhY2UvO1xuXHRcdFxuXHRcdGZvciAobGV0IG5vZGUgb2Ygbm9kZXMpIHtcblx0XHRcdGZvciAobGV0IGF0dHJpYnV0ZSBvZiBBcnJheS5mcm9tKG5vZGUuYXR0cmlidXRlcykpIHtcblx0XHRcdFx0aWYgKGF0dHJpYnV0ZS5uYW1lLnNlYXJjaChyZWdleCkgIT09IC0xKSB7XG5cdFx0XHRcdFx0Ly8gUGFyc2UgdGhlIG5hbWVzcGFjZSBuYW1lIGFuZCBzb3VyY2UgVVJMLlxuXHRcdFx0XHRcdGxldCBuYW1lID0gYXR0cmlidXRlLm5hbWUucmVwbGFjZShyZWdleCwgJyQxJyksXG5cdFx0XHRcdFx0XHRzb3VyY2UgPSBhdHRyaWJ1dGUudmFsdWU7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gQ2hlY2sgaWYgdGhlIG5hbWVzcGFjZSBpcyBhbHJlYWR5IGRlZmluZWQuXG5cdFx0XHRcdFx0aWYgKHBhZ2VOYW1lc3BhY2VOYW1lcy5pbmRleE9mKG5hbWUpID4gLTEpIHtcblx0XHRcdFx0XHRcdGlmICh3aW5kb3dbbmFtZV0uc291cmNlICE9PSBzb3VyY2UpIHtcblx0XHRcdFx0XHRcdFx0anNlLmNvcmUuZGVidWcuZXJyb3IoYEVsZW1lbnQgd2l0aCB0aGUgZHVwbGljYXRlIG5hbWVzcGFjZSBuYW1lOiAke25vZGV9YCk7XG5cdFx0XHRcdFx0XHRcdHRocm93IG5ldyBFcnJvcihgVGhlIG5hbWVzcGFjZSBcIiR7bmFtZX1cIiBpcyBhbHJlYWR5IGRlZmluZWQuIFBsZWFzZSBzZWxlY3QgYW5vdGhlciBgICtcblx0XHRcdFx0XHRcdFx0XHRgbmFtZSBmb3IgeW91ciBuYW1lc3BhY2UuYCk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRjb250aW51ZTsgLy8gVGhlIG5hbWVzcGFjZSBpcyBhbHJlYWR5IGRlZmluZWQsIGNvbnRpbnVlIGxvb3AuXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmIChzb3VyY2UgPT09ICcnKSB7XG5cdFx0XHRcdFx0XHR0aHJvdyBuZXcgU3ludGF4RXJyb3IoYE5hbWVzcGFjZSBzb3VyY2UgaXMgZW1wdHk6ICR7bmFtZX1gKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gQ3JlYXRlIGEgbmV3IG5hbWVzcGFjZXMgaW5zdGFuY2UgaW4gdGhlIGdsb2JhbCBzY29wZSAodGhlIGdsb2JhbCBzY29wZSBpcyB1c2VkIGZvciBcblx0XHRcdFx0XHQvLyBmYWxsYmFjayBzdXBwb3J0IG9mIG9sZCBtb2R1bGUgZGVmaW5pdGlvbnMpLlxuXHRcdFx0XHRcdGlmIChuYW1lID09PSAnanNlJykgeyAvLyBNb2RpZnkgdGhlIGVuZ2luZSBvYmplY3Qgd2l0aCBOYW1lc3BhY2UgYXR0cmlidXRlcy5cblx0XHRcdFx0XHRcdF9jb252ZXJ0RW5naW5lVG9OYW1lc3BhY2Uoc291cmNlLCBjb2xsZWN0aW9ucyk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHdpbmRvd1tuYW1lXSA9IG5ldyBqc2UuY29uc3RydWN0b3JzLk5hbWVzcGFjZShuYW1lLCBzb3VyY2UsIGNvbGxlY3Rpb25zKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0cGFnZU5hbWVzcGFjZU5hbWVzLnB1c2gobmFtZSk7XG5cdFx0XHRcdFx0bm9kZS5yZW1vdmVBdHRyaWJ1dGUoYXR0cmlidXRlLm5hbWUpOyBcblx0XHRcdFx0fVx0XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0Ly8gVGhyb3cgYW4gZXJyb3IgaWYgbm8gbmFtZXNwYWNlcyB3ZXJlIGZvdW5kLlxuXHRcdGlmIChwYWdlTmFtZXNwYWNlTmFtZXMubGVuZ3RoID09PSAwKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ05vIG1vZHVsZSBuYW1lc3BhY2VzIHdlcmUgZm91bmQsIHdpdGhvdXQgbmFtZXNwYWNlcyBpdCBpcyBub3QgcG9zc2libGUgdG8gJyArXG5cdFx0XHRcdCdsb2FkIGFueSBtb2R1bGVzLicpO1xuXHRcdH1cblxuXHRcdC8vIEluaXRpYWxpemUgdGhlIG5hbWVzcGFjZSBpbnN0YW5jZXMuXG5cdFx0bGV0IGRlZmVycmVkQ29sbGVjdGlvbiA9IFtdO1xuXHRcdFxuXHRcdGZvciAobGV0IG5hbWUgb2YgcGFnZU5hbWVzcGFjZU5hbWVzKSB7XG5cdFx0XHRsZXQgZGVmZXJyZWQgPSAkLkRlZmVycmVkKCk7XG5cdFx0XHRcblx0XHRcdGRlZmVycmVkQ29sbGVjdGlvbi5wdXNoKGRlZmVycmVkKTtcblx0XHRcdFxuXHRcdFx0d2luZG93W25hbWVdXG5cdFx0XHRcdC5pbml0KClcblx0XHRcdFx0LmRvbmUoZGVmZXJyZWQucmVzb2x2ZSlcblx0XHRcdFx0LmZhaWwoZGVmZXJyZWQucmVqZWN0KVxuXHRcdFx0XHQuYWx3YXlzKCgpID0+ICBqc2UuY29yZS5kZWJ1Zy5pbmZvKCdOYW1lc3BhY2UgcHJvbWlzZXMgd2VyZSByZXNvbHZlZDogJywgbmFtZSkpO1xuXHRcdH1cblxuXHRcdC8vIFRyaWdnZXIgYW4gZXZlbnQgYWZ0ZXIgdGhlIGVuZ2luZSBoYXMgaW5pdGlhbGl6ZWQgYWxsIG5ldyBtb2R1bGVzLlxuXHRcdCQud2hlbi5hcHBseSh1bmRlZmluZWQsIGRlZmVycmVkQ29sbGVjdGlvbikuYWx3YXlzKGZ1bmN0aW9uKCkge1xuXHRcdFx0bGV0IGV2ZW50ID0gZG9jdW1lbnQuY3JlYXRlRXZlbnQoJ0V2ZW50Jyk7XG5cdFx0XHRldmVudC5pbml0RXZlbnQoJ0pTRU5HSU5FX0lOSVRfRklOSVNIRUQnLCB0cnVlLCB0cnVlKTtcblx0XHRcdGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2JvZHknKS5kaXNwYXRjaEV2ZW50KGV2ZW50KTtcblx0XHRcdGpzZS5jb3JlLnJlZ2lzdHJ5LnNldCgnanNlRW5kVGltZScsIG5ldyBEYXRlKCkuZ2V0VGltZSgpKTtcblx0XHRcdGpzZS5jb3JlLmRlYnVnLmluZm8oJ0pTIEVuZ2luZSBMb2FkaW5nIFRpbWU6ICcsIGpzZS5jb3JlLnJlZ2lzdHJ5LmdldCgnanNlRW5kVGltZScpIFxuXHRcdFx0XHQtIGpzZS5jb3JlLnJlZ2lzdHJ5LmdldCgnanNlU3RhcnRUaW1lJyksICdtcycpO1xuXHRcdFx0aWYgKHdpbmRvdy5DeXByZXNzKSB7XG5cdFx0XHRcdHdpbmRvdy5qc2VSZWFkeSA9IHRydWU7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gcGFnZU5hbWVzcGFjZU5hbWVzO1xuXHR9XG5cblx0LyoqXG5cdCAqIENvbnZlcnQgdGhlIFwianNlXCIgb2JqZWN0IHRvIGEgTmFtZXNwYWNlIGNvbXBhdGlibGUgb2JqZWN0LlxuXHQgKlxuXHQgKiBJbiBvcmRlciB0byBzdXBwb3J0IHRoZSBcImpzZVwiIG5hbWVzcGFjZSBuYW1lIGZvciB0aGUgY29yZSBtb2R1bGVzIHBsYWNlZCBpbiB0aGUgXCJKU0VuZ2luZVwiXG5cdCAqIGRpcmVjdG9yeSwgd2Ugd2lsbCBuZWVkIHRvIG1vZGlmeSB0aGUgYWxyZWFkeSBleGlzdGluZyBcImpzZVwiIG9iamVjdCBzbyB0aGF0IGl0IGNhbiBvcGVyYXRlXG5cdCAqIGFzIGEgbmFtZXNwYWNlIHdpdGhvdXQgbG9zaW5nIGl0cyBpbml0aWFsIGF0dHJpYnV0ZXMuXG5cdCAqXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBzb3VyY2UgTmFtZXNwYWNlIHNvdXJjZSBwYXRoIGZvciB0aGUgbW9kdWxlIGZpbGVzLlxuXHQgKiBAcGFyYW0ge0FycmF5fSBjb2xsZWN0aW9ucyBDb250YWluIGluc3RhbmNlcyB0byB0aGUgcHJvdG90eXBlIGNvbGxlY3Rpb24gaW5zdGFuY2VzLlxuXHQgKlxuXHQgKiBAcHJpdmF0ZVxuXHQgKi9cblx0ZnVuY3Rpb24gX2NvbnZlcnRFbmdpbmVUb05hbWVzcGFjZShzb3VyY2UsIGNvbGxlY3Rpb25zKSB7XG5cdFx0bGV0IHRtcE5hbWVzcGFjZSA9IG5ldyBqc2UuY29uc3RydWN0b3JzLk5hbWVzcGFjZSgnanNlJywgc291cmNlLCBjb2xsZWN0aW9ucyk7XG5cdFx0anNlLm5hbWUgPSB0bXBOYW1lc3BhY2UubmFtZTtcblx0XHRqc2Uuc291cmNlID0gdG1wTmFtZXNwYWNlLnNvdXJjZTtcblx0XHRqc2UuY29sbGVjdGlvbnMgPSB0bXBOYW1lc3BhY2UuY29sbGVjdGlvbnM7XG5cdFx0anNlLmluaXQgPSBqc2UuY29uc3RydWN0b3JzLk5hbWVzcGFjZS5wcm90b3R5cGUuaW5pdDtcblx0fVxuXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHQvLyBQVUJMSUMgRlVOQ1RJT05TXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdC8qKlxuXHQgKiBJbml0aWFsaXplIHRoZSBlbmdpbmUuXG5cdCAqXG5cdCAqIEBwYXJhbSB7QXJyYXl9IGNvbGxlY3Rpb25zIENvbnRhaW5zIHRoZSBzdXBwb3J0ZWQgbW9kdWxlIGNvbGxlY3Rpb24gZGF0YS5cblx0ICovXG5cdGV4cG9ydHMuaW5pdCA9IGZ1bmN0aW9uIChjb2xsZWN0aW9ucykge1xuXHRcdC8vIEdsb2JhbCBlcnJvciBoYW5kbGVyIHRoYXQgZXhlY3V0ZXMgaWYgYW4gdW5jYXVnaHQgSlMgZXJyb3Igb2NjdXJzIG9uIHBhZ2UuXG5cdFx0anNlLmNvcmUuZGVidWcuYmluZEdsb2JhbEVycm9ySGFuZGxlcigpOyBcblxuXHRcdC8vIEluaXRpYWxpemUgdGhlIHBhZ2UgbmFtZXNwYWNlcy5cblx0XHRsZXQgcGFnZU5hbWVzcGFjZU5hbWVzID0gX2luaXROYW1lc3BhY2VzKGNvbGxlY3Rpb25zKTtcblxuXHRcdC8vIExvZyB0aGUgcGFnZSBuYW1lc3BhY2VzIChmb3IgZGVidWdnaW5nIG9ubHkpLlxuXHRcdGpzZS5jb3JlLmRlYnVnLmluZm8oJ1BhZ2UgTmFtZXNwYWNlczogJyArIHBhZ2VOYW1lc3BhY2VOYW1lcy5qb2luKCkpO1xuXG5cdFx0Ly8gVXBkYXRlIHRoZSBlbmdpbmUgcmVnaXN0cnkuXG5cdFx0anNlLmNvcmUucmVnaXN0cnkuc2V0KCduYW1lc3BhY2VzJywgcGFnZU5hbWVzcGFjZU5hbWVzKTtcblx0fTtcblxufSkoanNlLmNvcmUuZW5naW5lKTtcbiIsIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZXh0ZW5zaW9ucy5qcyAyMDE3LTAzLTAzXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBKU0UgRXh0ZW5zaW9uc1xuICpcbiAqIEV4dGVuZCB0aGUgZGVmYXVsdCBiZWhhdmlvdXIgb2YgZW5naW5lIGNvbXBvbmVudHMgb3IgZXh0ZXJuYWwgcGx1Z2lucyBiZWZvcmUgdGhleSBhcmUgbG9hZGVkLlxuICpcbiAqIEBtb2R1bGUgSlNFL0NvcmUvZXh0ZW5kXG4gKi9cbihmdW5jdGlvbiAoKSB7XG5cblx0J3VzZSBzdHJpY3QnO1xuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIFBBUlNFIE1PRFVMRSBEQVRBIEpRVUVSWSBFWFRFTlNJT05cblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFxuXHQkLmZuLmV4dGVuZCh7XG5cdFx0cGFyc2VNb2R1bGVEYXRhOiBmdW5jdGlvbihtb2R1bGVOYW1lKSB7XG5cdFx0XHRpZiAoIW1vZHVsZU5hbWUgfHwgbW9kdWxlTmFtZSA9PT0gJycpIHtcblx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdNb2R1bGUgbmFtZSB3YXMgbm90IHByb3ZpZGVkIGFzIGFuIGFyZ3VtZW50LicpXG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGNvbnN0IGluaXRpYWxEYXRhID0gJCh0aGlzKS5kYXRhKCk7XG5cdFx0XHRjb25zdCBmaWx0ZXJlZERhdGEgPSB7fTtcblx0XHRcdFxuXHRcdFx0Ly8gU2VhcmNoZXMgZm9yIG1vZHVsZSByZWxldmFudCBkYXRhIGluc2lkZSB0aGUgbWFpbi1kYXRhLW9iamVjdC4gRGF0YSBmb3Igb3RoZXIgd2lkZ2V0cyB3aWxsIG5vdCBnZXQgXG5cdFx0XHQvLyBwYXNzZWQgdG8gdGhpcyB3aWRnZXQuXG5cdFx0XHQkLmVhY2goaW5pdGlhbERhdGEsIGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG5cdFx0XHRcdGlmIChrZXkuaW5kZXhPZihtb2R1bGVOYW1lKSA9PT0gMCB8fCBrZXkuaW5kZXhPZihtb2R1bGVOYW1lLnRvTG93ZXJDYXNlKCkpID09PSAwKSB7XG5cdFx0XHRcdFx0bGV0IG5ld0tleSA9IGtleS5zdWJzdHIobW9kdWxlTmFtZS5sZW5ndGgpO1xuXHRcdFx0XHRcdG5ld0tleSA9IG5ld0tleS5zdWJzdHIoMCwgMSkudG9Mb3dlckNhc2UoKSArIG5ld0tleS5zdWJzdHIoMSk7XG5cdFx0XHRcdFx0ZmlsdGVyZWREYXRhW25ld0tleV0gPSB2YWx1ZTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdHJldHVybiBmaWx0ZXJlZERhdGE7XG5cdFx0fVxuXHR9KTtcblxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gREFURVBJQ0tFUiBSRUdJT05BTCBJTkZPXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdGlmICgkLmRhdGVwaWNrZXIgIT09IHVuZGVmaW5lZCnCoHtcblx0XHQkLmRhdGVwaWNrZXIucmVnaW9uYWwuZGUgPSB7XG5cdFx0XHRkYXRlRm9ybWF0OiAnZGQubW0ueXknLFxuXHRcdFx0Zmlyc3REYXk6IDEsXG5cdFx0XHRpc1JUTDogZmFsc2Vcblx0XHR9O1xuXHRcdCQuZGF0ZXBpY2tlci5zZXREZWZhdWx0cygkLmRhdGVwaWNrZXIucmVnaW9uYWwuZGUpO1xuXHR9XG59KCkpO1xuIiwiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBpbml0aWFsaXplLmpzIDIwMTYtMDktMDhcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG4ndXNlIHN0cmljdCc7IFxuXG4vKipcbiAqIEpTRSBJbml0aWFsaXphdGlvbiBNb2R1bGVcbiAqXG4gKiBUaGUgZG9jdW1lbnQtcmVhZHkgZXZlbnQgb2YgdGhlIHBhZ2Ugd2lsbCB0cmlnZ2VyIHRoZSBKYXZhU2NyaXB0IEVuZ2luZSBpbml0aWFsaXphdGlvbi4gVGhlXG4gKiBlbmdpbmUgcmVxdWlyZXMgYSBnbG9iYWwgY29uZmlndXJhdGlvbiBvYmplY3QgXCJ3aW5kb3cuSlNFbmdpbmVDb25maWd1cmF0aW9uXCIgdG8gYmUgcHJlLWRlZmluZWRcbiAqIGluIG9yZGVyIHRvIHJldHJpZXZlIHRoZSBiYXNpYyBjb25maWd1cmF0aW9uIGluZm8uIEFmdGVyIGEgc3VjY2Vzc2Z1bCBpbml0aWFsaXphdGlvbiB0aGlzIG9iamVjdFxuICogaXMgcmVtb3ZlZCBmcm9tIHRoZSB3aW5kb3cgb2JqZWN0LlxuICpcbiAqICMjIyBDb25maWd1cmF0aW9uIFNhbXBsZVxuICpcbiAqIGBgYGpzXG4gKiB3aW5kb3cuSlNFbmdpbmVDb25maWd1cmF0aW9uID0ge1xuICogICBlbnZpcm9ubWVudDogJ3Byb2R1Y3Rpb24nLFxuICogICBhcHBVcmw6ICdodHRwOi8vYXBwLmNvbScsXG4gKiAgIGNvbGxlY3Rpb25zOiBbXG4gKiAgICAge25hbWU6ICdjb250cm9sbGVycycsIGF0dHJpYnV0ZTogJ2NvbnRyb2xsZXInfVxuICogICBdLCAgXG4gKiAgIHRyYW5zbGF0aW9uczoge1xuICogICAgICdzZWN0aW9uTmFtZSc6IHsgJ3RyYW5zbGF0aW9uS2V5JzogJ3RyYW5zbGF0aW9uVmFsdWUnIH0sXG4gKiAgICAgJ2Fub3RoZXJTZWN0aW9uJzogeyAuLi4gfVxuICogICB9LFxuICogICBsYW5ndWFnZUNvZGU6ICdlbicsXG4gKiAgIHBhZ2VUb2tlbjogJzlhc2Q3Zjk4NzlzZDhmNzlzOThzN2Q5OGYnXG4gKiB9O1xuICogYGBgXG4gKlxuICogQG1vZHVsZSBKU0UvQ29yZS9pbml0aWFsaXplXG4gKi9cblxuLy8gSW5pdGlhbGl6ZSBiYXNlIGVuZ2luZSBvYmplY3QuIEV2ZXJ5IG90aGVyIHBhcnQgb2YgdGhlIGVuZ2luZSB3aWxsIHJlZmVyIHRvIHRoaXNcbi8vIGNlbnRyYWwgb2JqZWN0IGZvciB0aGUgY29yZSBvcGVyYXRpb25zLlxud2luZG93LmpzZSA9IHtcblx0Y29yZToge30sXG5cdGxpYnM6IHt9LFxuXHRjb25zdHJ1Y3RvcnM6IHt9XG59O1xuXG4vLyBJbml0aWFsaXplIHRoZSBlbmdpbmUgb24gd2luZG93IGxvYWQuIFxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZ1bmN0aW9uKCkge1xuXHR0cnkge1xuXHRcdC8vIENoZWNrIGlmIGdsb2JhbCBKU0VuZ2luZUNvbmZpZ3VyYXRpb24gb2JqZWN0IGlzIGRlZmluZWQuXG5cdFx0aWYgKHdpbmRvdy5KU0VuZ2luZUNvbmZpZ3VyYXRpb24gPT09IHVuZGVmaW5lZCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdUaGUgXCJ3aW5kb3cuSlNFbmdpbmVDb25maWd1cmF0aW9uXCIgb2JqZWN0IGlzIG5vdCBkZWZpbmVkIGluIHRoZSBnbG9iYWwgc2NvcGUuICcgK1xuXHRcdFx0XHQnVGhpcyBvYmplY3QgaXMgcmVxdWlyZWQgYnkgdGhlIGVuZ2luZSB1cG9uIGl0cyBpbml0aWFsaXphdGlvbi4nKTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gUGFyc2UgSlNFbmdpbmVDb25maWd1cmF0aW9uIG9iamVjdC5cblx0XHRqc2UuY29yZS5jb25maWcuaW5pdCh3aW5kb3cuSlNFbmdpbmVDb25maWd1cmF0aW9uKTtcblx0XHRcblx0XHQvLyBTdG9yZSB0aGUgSlNFIHN0YXJ0IHRpbWUgaW4gcmVnaXN0cnkgKHByb2ZpbGluZykuIFxuXHRcdGpzZS5jb3JlLnJlZ2lzdHJ5LnNldCgnanNlU3RhcnRUaW1lJywgRGF0ZS5ub3coKSk7XG5cdFx0XG5cdFx0Ly8gSW5pdGlhbGl6ZSB0aGUgbW9kdWxlIGNvbGxlY3Rpb25zLlxuXHRcdGpzZS5jb3JlLmVuZ2luZS5pbml0KGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2NvbGxlY3Rpb25zJykpO1xuXHR9IGNhdGNoIChleGNlcHRpb24pIHtcblx0XHRqc2UuY29yZS5kZWJ1Zy5lcnJvcignVW5leHBlY3RlZCBlcnJvciBkdXJpbmcgSlMgRW5naW5lIGluaXRpYWxpemF0aW9uIScsIGV4Y2VwdGlvbik7XG5cdFx0Ly8gSW5mb3JtIHRoZSBlbmdpbmUgYWJvdXQgdGhlIGV4Y2VwdGlvbi5cblx0XHRjb25zdCBldmVudCA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KCdDdXN0b21FdmVudCcpOyBcblx0XHRldmVudC5pbml0Q3VzdG9tRXZlbnQoJ2Vycm9yJywgdHJ1ZSwgdHJ1ZSwgZXhjZXB0aW9uKTtcblx0XHR3aW5kb3cuZGlzcGF0Y2hFdmVudChldmVudCk7IFxuXHR9XG59KTsgXG4iLCIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGxhbmcuanMgMjAxNi0wOC0yM1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmpzZS5jb3JlLmxhbmcgPSBqc2UuY29yZS5sYW5nIHx8IHt9O1xuXG4vKipcbiAqIEpTRSBMb2NhbGl6YXRpb24gTGlicmFyeVxuICpcbiAqIFRoZSBnbG9iYWwgTGFuZyBvYmplY3QgY29udGFpbnMgbGFuZ3VhZ2UgaW5mb3JtYXRpb24gdGhhdCBjYW4gYmUgZWFzaWx5IHVzZWQgaW4geW91clxuICogSmF2YVNjcmlwdCBjb2RlLiBUaGUgb2JqZWN0IGNvbnRhaW5zIGNvbnN0YW5jZSB0cmFuc2xhdGlvbnMgYW5kIGR5bmFtaWMgc2VjdGlvbnMgdGhhdFxuICogY2FuIGJlIGxvYWRlZCBhbmQgdXNlZCBpbiBkaWZmZXJlbnQgcGFnZS5cbiAqXG4gKiAjIyMjIEltcG9ydGFudFxuICogVGhlIGVuZ2luZSB3aWxsIGF1dG9tYXRpY2FsbHkgbG9hZCB0cmFuc2xhdGlvbiBzZWN0aW9ucyB0aGF0IGFyZSBwcmVzZW50IGluIHRoZVxuICogYHdpbmRvdy5KU0VuZ2luZUNvbmZpZ3VyYXRpb24udHJhbnNsYXRpb25zYCBwcm9wZXJ0eSB1cG9uIGluaXRpYWxpemF0aW9uLiBGb3IgbW9yZVxuICogaW5mb3JtYXRpb24gbG9vayBhdCB0aGUgXCJjb3JlL2luaXRpYWxpemVcIiBwYWdlIG9mIGRvY3VtZW50YXRpb24gcmVmZXJlbmNlLlxuICpcbiAqIGBgYGphdmFzY3JpcHRcbiAqIGpzZS5jb3JlLmxhbmcuYWRkU2VjdGlvbignc2VjdGlvbk5hbWUnLCB7IHRyYW5zbGF0aW9uS2V5OiAndHJhbnNsYXRpb25WYWx1ZScgfSk7IC8vIEFkZCB0cmFuc2xhdGlvbiBzZWN0aW9uLlxuICoganNlLmNvcmUudHJhbnNsYXRlKCd0cmFuc2xhdGlvbktleScsICdzZWN0aW9uTmFtZScpOyAvLyBHZXQgdGhlIHRyYW5zbGF0ZWQgc3RyaW5nLlxuICoganNlLmNvcmUuZ2V0U2VjdGlvbnMoKTsgLy8gcmV0dXJucyBhcnJheSB3aXRoIHNlY3Rpb25zIGUuZy4gWydhZG1pbl9idXR0b25zJywgJ2dlbmVyYWwnXVxuICogYGBgXG4gKlxuICogQG1vZHVsZSBKU0UvQ29yZS9sYW5nXG4gKi9cbihmdW5jdGlvbiAoZXhwb3J0cykge1xuXG5cdCd1c2Ugc3RyaWN0JztcblxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gVkFSSUFCTEVTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdC8qKlxuXHQgKiBDb250YWlucyB2YXJpb3VzIHRyYW5zbGF0aW9uIHNlY3Rpb25zLlxuXHQgKlxuXHQgKiBAdHlwZSB7T2JqZWN0fVxuXHQgKi9cblx0Y29uc3Qgc2VjdGlvbnMgPSB7fTtcblxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gUFVCTElDIE1FVEhPRFNcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cblx0LyoqXG5cdCAqIEFkZCBhIHRyYW5zbGF0aW9uIHNlY3Rpb24uXG5cdCAqXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIE5hbWUgb2YgdGhlIHNlY3Rpb24sIHVzZWQgbGF0ZXIgZm9yIGFjY2Vzc2luZyB0cmFuc2xhdGlvbiBzdHJpbmdzLlxuXHQgKiBAcGFyYW0ge09iamVjdH0gdHJhbnNsYXRpb25zIEtleSAtIHZhbHVlIG9iamVjdCBjb250YWluaW5nIHRoZSB0cmFuc2xhdGlvbnMuXG5cdCAqXG5cdCAqIEB0aHJvd3Mge0Vycm9yfSBJZiBcIm5hbWVcIiBvciBcInRyYW5zbGF0aW9uc1wiIGFyZ3VtZW50cyBhcmUgaW52YWxpZC5cblx0ICovXG5cdGV4cG9ydHMuYWRkU2VjdGlvbiA9IGZ1bmN0aW9uIChuYW1lLCB0cmFuc2xhdGlvbnMpIHtcblx0XHRpZiAodHlwZW9mIG5hbWUgIT09ICdzdHJpbmcnIHx8IHR5cGVvZiB0cmFuc2xhdGlvbnMgIT09ICdvYmplY3QnIHx8IHRyYW5zbGF0aW9ucyA9PT0gbnVsbCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKGB3aW5kb3cuZ3guY29yZS5sYW5nLmFkZFNlY3Rpb246IEludmFsaWQgYXJndW1lbnRzIHByb3ZpZGVkIChuYW1lOiAke3R5cGVvZiBuYW1lfSwgYCBcblx0XHRcdCAgICAgICAgICAgICAgICArIGB0cmFuc2xhdGlvbnM6ICR7dHlwZW9mIHRyYW5zbGF0aW9uc30pLmApO1xuXHRcdH1cblx0XHRzZWN0aW9uc1tuYW1lXSA9IHRyYW5zbGF0aW9ucztcblx0fTtcblxuXHQvKipcblx0ICogR2V0IGxvYWRlZCB0cmFuc2xhdGlvbiBzZWN0aW9ucy5cblx0ICpcblx0ICogVXNlZnVsIGZvciBhc3NlcnRpbmcgcHJlc2VudCB0cmFuc2xhdGlvbiBzZWN0aW9ucy5cblx0ICpcblx0ICogQHJldHVybiB7QXJyYXl9IFJldHVybnMgYXJyYXkgd2l0aCB0aGUgZXhpc3Rpbmcgc2VjdGlvbnMuXG5cdCAqL1xuXHRleHBvcnRzLmdldFNlY3Rpb25zID0gZnVuY3Rpb24gKCkge1xuXHRcdGNvbnN0IHJlc3VsdCA9IFtdO1xuXHRcdFxuXHRcdGZvciAobGV0IHNlY3Rpb24gaW4gc2VjdGlvbnMpIHtcblx0XHRcdHJlc3VsdC5wdXNoKHNlY3Rpb24pO1xuXHRcdH1cblx0XHRcblx0XHRyZXR1cm4gcmVzdWx0O1xuXHR9O1xuXG5cdC8qKlxuXHQgKiBUcmFuc2xhdGUgc3RyaW5nIGluIEphdmFzY3JpcHQgY29kZS5cblx0ICpcblx0ICogQHBhcmFtIHtTdHJpbmd9IHBocmFzZSBOYW1lIG9mIHRoZSBwaHJhc2UgY29udGFpbmluZyB0aGUgdHJhbnNsYXRpb24uXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBzZWN0aW9uIFNlY3Rpb24gbmFtZSBjb250YWluaW5nIHRoZSB0cmFuc2xhdGlvbiBzdHJpbmcuXG5cdCAqXG5cdCAqIEByZXR1cm4ge1N0cmluZ30gUmV0dXJucyB0aGUgdHJhbnNsYXRlZCBzdHJpbmcuXG5cdCAqXG5cdCAqIEB0aHJvd3Mge0Vycm9yfSBJZiBwcm92aWRlZCBhcmd1bWVudHMgYXJlIGludmFsaWQuXG5cdCAqIEB0aHJvd3Mge0Vycm9yfSBJZiByZXF1aXJlZCBzZWN0aW9uIGRvZXMgbm90IGV4aXN0IG9yIHRyYW5zbGF0aW9uIGNvdWxkIG5vdCBiZSBmb3VuZC5cblx0ICovXG5cdGV4cG9ydHMudHJhbnNsYXRlID0gZnVuY3Rpb24gKHBocmFzZSwgc2VjdGlvbikge1xuXHRcdC8vIFZhbGlkYXRlIHByb3ZpZGVkIGFyZ3VtZW50cy5cblx0XHRpZiAodHlwZW9mIHBocmFzZSAhPT0gJ3N0cmluZycgfHwgdHlwZW9mIHNlY3Rpb24gIT09ICdzdHJpbmcnKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoYEludmFsaWQgYXJndW1lbnRzIHByb3ZpZGVkIGluIHRyYW5zbGF0ZSBtZXRob2QgKHBocmFzZTogJHt0eXBlb2YgcGhyYXNlfSwgYFxuXHRcdFx0ICAgICAgICAgICAgICAgICsgYHNlY3Rpb246ICR7dHlwZW9mIHNlY3Rpb259KS5gKTtcblx0XHR9XG5cblx0XHQvLyBDaGVjayBpZiB0cmFuc2xhdGlvbiBleGlzdHMuXG5cdFx0aWYgKHNlY3Rpb25zW3NlY3Rpb25dID09PSB1bmRlZmluZWQgfHwgc2VjdGlvbnNbc2VjdGlvbl1bcGhyYXNlXSA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRqc2UuY29yZS5kZWJ1Zy53YXJuKGBDb3VsZCBub3QgZm91bmQgcmVxdWVzdGVkIHRyYW5zbGF0aW9uIChwaHJhc2U6ICR7cGhyYXNlfSwgc2VjdGlvbjogJHtzZWN0aW9ufSkuYCk7XG5cdFx0XHRyZXR1cm4gJ3snICsgc2VjdGlvbiArICcuJyArIHBocmFzZSArICd9Jztcblx0XHR9XG5cblx0XHRyZXR1cm4gc2VjdGlvbnNbc2VjdGlvbl1bcGhyYXNlXTtcblx0fTtcblxufShqc2UuY29yZS5sYW5nKSk7XG4iLCIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gbWFpbi5qcyAyMDE2LTA1LTE3XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLy8gSW1wb3J0IGluaXRpYWxpemF0aW9uIHNjcmlwdC4gXHJcbmltcG9ydCAnLi9pbml0aWFsaXplJzsgIFxyXG5cclxuLy8gSW1wb3J0IHRoZSBjb25zdHJ1Y3RvciBmaWxlcy4gXHJcbmltcG9ydCAnLi4vY29uc3RydWN0b3JzL2NvbGxlY3Rpb24nO1xyXG5pbXBvcnQgJy4uL2NvbnN0cnVjdG9ycy9kYXRhX2JpbmRpbmcnO1xyXG5pbXBvcnQgJy4uL2NvbnN0cnVjdG9ycy9tb2R1bGUnO1xyXG5pbXBvcnQgJy4uL2NvbnN0cnVjdG9ycy9uYW1lc3BhY2UnO1xyXG5cclxuLy8gSW1wb3J0IHRoZSBjb3JlIGZpbGVzLiBcclxuaW1wb3J0ICcuL2Fib3V0JztcclxuaW1wb3J0ICcuL2NvbmZpZyc7XHJcbmltcG9ydCAnLi9kZWJ1Zyc7XHJcbmltcG9ydCAnLi9lbmdpbmUnO1xyXG5pbXBvcnQgJy4vZXh0ZW5kJztcclxuaW1wb3J0ICcuL2xhbmcnO1xyXG5pbXBvcnQgJy4vcmVxdWlyZSc7XHJcbmltcG9ydCAnLi9tb2R1bGVfbG9hZGVyJztcclxuaW1wb3J0ICcuL3BvbHlmaWxscyc7XHJcbmltcG9ydCAnLi9yZWdpc3RyeSc7IiwiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBtb2R1bGVfbG9hZGVyLmpzIDIwMTYtMDYtMjNcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UuY29yZS5tb2R1bGVfbG9hZGVyID0ganNlLmNvcmUubW9kdWxlX2xvYWRlciB8fCB7fTtcblxuLyoqXG4gKiBKU0UgTW9kdWxlIExvYWRlclxuICpcbiAqIFRoaXMgb2JqZWN0IGlzIGFuIGFkYXB0ZXIgYmV0d2VlbiB0aGUgZW5naW5lIGFuZCBSZXF1aXJlSlMgd2hpY2ggaXMgdXNlZCB0byBsb2FkIHRoZSByZXF1aXJlZCBmaWxlcyBcbiAqIGludG8gdGhlIGNsaWVudC5cbiAqIFxuICogQHRvZG8gUmVtb3ZlIHJlcXVpcmUuanMgZGVwZW5kZW5jeSBhbmQgbG9hZCB0aGUgbW9kdWxlL2xpYiBmaWxlcyBtYW51YWxseS5cbiAqXG4gKiBAbW9kdWxlIEpTRS9Db3JlL21vZHVsZV9sb2FkZXJcbiAqL1xuKGZ1bmN0aW9uIChleHBvcnRzKSB7XG5cblx0J3VzZSBzdHJpY3QnO1xuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIFBSSVZBVEUgTUVUSE9EU1xuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XG5cdC8qKlxuXHQgKiBMb2FkIENTUyBmaWxlLlxuXHQgKiBcblx0ICogQHBhcmFtIHtTdHJpbmd9IHVybCBBYnNvbHV0ZSBVUkwgb2YgdGhlIENTUyBmaWxlIHRvIGJlIGxvYWRlZC4gXG5cdCAqIFxuXHQgKiBAcHJpdmF0ZVxuXHQgKi9cblx0ZnVuY3Rpb24gX2xvYWRDc3ModXJsKSB7XG5cdFx0Y29uc3QgbGluayA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2xpbmsnKTtcblx0XHRsaW5rLnR5cGUgPSAndGV4dC9jc3MnO1xuXHRcdGxpbmsucmVsID0gJ3N0eWxlc2hlZXQnO1xuXHRcdGxpbmsuaHJlZiA9IHVybDtcblx0XHRkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnaGVhZCcpWzBdLmFwcGVuZENoaWxkKGxpbmspO1xuXHR9XG5cblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIFBVQkxJQyBNRVRIT0RTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cdC8qKlxuXHQgKiBJbml0aWFsaXplIHRoZSBtb2R1bGUgbG9hZGVyLlxuXHQgKlxuXHQgKiBFeGVjdXRlIHRoaXMgbWV0aG9kIGFmdGVyIHRoZSBlbmdpbmUgY29uZmlnIGlzIGluaXRpYWxpemVkLiBJdCB3aWxsIGNvbmZpZ3VyZSByZXF1aXJlLmpzXG5cdCAqIHNvIHRoYXQgaXQgd2lsbCBiZSBhYmxlIHRvIGZpbmQgdGhlIHByb2plY3QgZmlsZXMuXG5cdCAqIFxuXHQgKiBUaGUgY2FjaGUgYnVzdGluZyBtZXRob2Qgd2lsbCB0cnkgdG8gY3JlYXRlIGEgbnVtYmVyIGJhc2VkIG9uIHRoZSBjdXJyZW50IHNob3AgdmVyc2lvbi5cblx0ICovXG5cdGV4cG9ydHMuaW5pdCA9IGZ1bmN0aW9uICgpIHtcblx0XHRsZXQgY2FjaGVCdXN0ID0gJyc7XG5cdFx0XG5cdFx0aWYgKGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2Vudmlyb25tZW50JykgPT09ICdwcm9kdWN0aW9uJyAmJiBqc2UuY29yZS5jb25maWcuZ2V0KCdjYWNoZVRva2VuJykpIHtcblx0XHRcdGNhY2hlQnVzdCA9IGBidXN0PSR7anNlLmNvcmUuY29uZmlnLmdldCgnY2FjaGVUb2tlbicpfWA7XG5cdFx0fVxuXHRcdFxuXHRcdGNvbnN0IGNvbmZpZyA9IHtcblx0XHRcdGJhc2VVcmw6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2FwcFVybCcpLFxuXHRcdFx0dXJsQXJnczogY2FjaGVCdXN0LFxuXHRcdFx0b25FcnJvcjogZnVuY3Rpb24gKGVycm9yKSB7XG5cdFx0XHRcdGpzZS5jb3JlLmRlYnVnLmVycm9yKCdSZXF1aXJlSlMgRXJyb3I6JywgZXJyb3IpO1xuXHRcdFx0fVxuXHRcdH07XG5cblx0XHR3aW5kb3cucmVxdWlyZS5jb25maWcoY29uZmlnKTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBSZXF1aXJlIEpTIGFuZCBDU1MgZmlsZXMgLlxuXHQgKiBcblx0ICogTm90aWNlOiBUaGVyZSdzIG5vIGNvbmNyZXRlIHdheSB0byBkZXRlcm1pbmUgd2hlbiBDU1MgZGVwZW5kZW5jaWVzIGFyZSBsb2FkZWQuXG5cdCAqIFxuXHQgKiBAcGFyYW0ge1N0cmluZ1tdfSBkZXBlbmRlbmNpZXMgRGVwZW5kZW5jeSBVUkxzLlxuXHQgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFjayBDYWxsYmFjayBtZXRob2QgdG8gYmUgY2FsbGVkIG9uY2UgdGhlIGRlcGVuZGVuY2llcyBhcmUgbG9hZGVkLiBcblx0ICovXG5cdGV4cG9ydHMucmVxdWlyZSA9IGZ1bmN0aW9uKGRlcGVuZGVuY2llcywgY2FsbGJhY2spIHtcblx0XHRmb3IgKGxldCBkZXBlbmRlbmN5IG9mIGRlcGVuZGVuY2llcykge1xuXHRcdFx0aWYgKGRlcGVuZGVuY3kuaW5jbHVkZXMoJy5jc3MnKSkge1xuXHRcdFx0XHRfbG9hZENzcyhkZXBlbmRlbmN5KTsgXG5cdFx0XHRcdGNvbnN0IGluZGV4ID0gZGVwZW5kZW5jaWVzLmluZGV4T2YoZGVwZW5kZW5jeSk7IFxuXHRcdFx0XHRkZXBlbmRlbmNpZXMuc3BsaWNlKGluZGV4LCAxKTsgXG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdGlmIChkZXBlbmRlbmNpZXMubGVuZ3RoID09PSAwKSB7XG5cdFx0XHRjYWxsYmFjaygpOyBcblx0XHR9IGVsc2Uge1xuXHRcdFx0d2luZG93LnJlcXVpcmUoZGVwZW5kZW5jaWVzLCBjYWxsYmFjayk7XHRcblx0XHR9IFxuXHR9OyBcblxuXHQvKipcblx0ICogTG9hZCBhIG1vZHVsZSBmaWxlIHdpdGggdGhlIHVzZSBvZiByZXF1aXJlanMuXG5cdCAqXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSAkZWxlbWVudCBTZWxlY3RvciBvZiB0aGUgZWxlbWVudCB3aGljaCBoYXMgdGhlIG1vZHVsZSBkZWZpbml0aW9uLlxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBNb2R1bGUgbmFtZSB0byBiZSBsb2FkZWQuIE1vZHVsZXMgaGF2ZSB0aGUgc2FtZSBuYW1lcyBhcyB0aGVpciBmaWxlcy5cblx0ICogQHBhcmFtIHtPYmplY3R9IGNvbGxlY3Rpb24gQ3VycmVudCBjb2xsZWN0aW9uIGluc3RhbmNlLlxuXHQgKlxuXHQgKiBAcmV0dXJuIHtPYmplY3R9IFJldHVybnMgYSBwcm9taXNlIG9iamVjdCB0byBiZSByZXNvbHZlZCB3aXRoIHRoZSBtb2R1bGUgaW5zdGFuY2UgYXMgYSBwYXJhbWV0ZXIuXG5cdCAqL1xuXHRleHBvcnRzLmxvYWQgPSBmdW5jdGlvbiAoJGVsZW1lbnQsIG5hbWUsIGNvbGxlY3Rpb24pIHtcblx0XHRjb25zdCBkZWZlcnJlZCA9ICQuRGVmZXJyZWQoKTtcblxuXHRcdHRyeSB7XG5cdFx0XHRpZiAobmFtZSA9PT0gJycpIHtcblx0XHRcdFx0ZGVmZXJyZWQucmVqZWN0KG5ldyBFcnJvcignTW9kdWxlIG5hbWUgY2Fubm90IGJlIGVtcHR5LicpKTtcblx0XHRcdH1cblxuXHRcdFx0Y29uc3QgYmFzZU1vZHVsZU5hbWUgPSBuYW1lLnJlcGxhY2UoLy4qXFwvKC4qKSQvLCAnJDEnKTsgLy8gTmFtZSB3aXRob3V0IHRoZSBwYXJlbnQgZGlyZWN0b3JpZXMuXG5cblx0XHRcdC8vIFRyeSB0byBsb2FkIHRoZSBjYWNoZWQgaW5zdGFuY2Ugb2YgdGhlIG1vZHVsZS5cblx0XHRcdGNvbnN0IGNhY2hlZCA9IGNvbGxlY3Rpb24uY2FjaGUubW9kdWxlc1tiYXNlTW9kdWxlTmFtZV07XG5cdFx0XHRpZiAoY2FjaGVkICYmIGNhY2hlZC5jb2RlID09PSAnZnVuY3Rpb24nKSB7XG5cdFx0XHRcdGRlZmVycmVkLnJlc29sdmUobmV3IGpzZS5jb25zdHJ1Y3RvcnMuTW9kdWxlKCRlbGVtZW50LCBiYXNlTW9kdWxlTmFtZSwgY29sbGVjdGlvbikpO1xuXHRcdFx0XHRyZXR1cm4gdHJ1ZTsgLy8gY29udGludWUgbG9vcFxuXHRcdFx0fVxuXG5cdFx0XHQvLyBUcnkgdG8gbG9hZCB0aGUgbW9kdWxlIGZpbGUgZnJvbSB0aGUgc2VydmVyLlxuXHRcdFx0Y29uc3QgZmlsZUV4dGVuc2lvbiA9IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2RlYnVnJykgIT09ICdERUJVRycgPyAnLm1pbi5qcycgOiAnLmpzJztcblx0XHRcdGNvbnN0IHVybCA9IGNvbGxlY3Rpb24ubmFtZXNwYWNlLnNvdXJjZSArICcvJyArIGNvbGxlY3Rpb24ubmFtZSArICcvJyArIG5hbWUgKyBmaWxlRXh0ZW5zaW9uO1xuXG5cdFx0XHR3aW5kb3cucmVxdWlyZShbdXJsXSwgKCkgPT4ge1xuXHRcdFx0XHRpZiAoY29sbGVjdGlvbi5jYWNoZS5tb2R1bGVzW2Jhc2VNb2R1bGVOYW1lXSA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdNb2R1bGUgXCInICsgbmFtZSArICdcIiB3YXNuXFwndCBkZWZpbmVkIGNvcnJlY3RseS4gQ2hlY2sgdGhlIG1vZHVsZSBjb2RlIGZvciAnXG5cdFx0XHRcdFx0ICAgICAgICAgICAgICAgICsgJ2Z1cnRoZXIgdHJvdWJsZXNob290aW5nLicpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly8gVXNlIHRoZSBzbGljZSBtZXRob2QgZm9yIGNvcHlpbmcgdGhlIGFycmF5LiBcblx0XHRcdFx0Y29uc3QgZGVwZW5kZW5jaWVzID0gY29sbGVjdGlvbi5jYWNoZS5tb2R1bGVzW2Jhc2VNb2R1bGVOYW1lXS5kZXBlbmRlbmNpZXMuc2xpY2UoKTsgXG5cblx0XHRcdFx0aWYgKGRlcGVuZGVuY2llcy5sZW5ndGggPT09IDApIHsgLy8gbm8gZGVwZW5kZW5jaWVzXG5cdFx0XHRcdFx0ZGVmZXJyZWQucmVzb2x2ZShuZXcganNlLmNvbnN0cnVjdG9ycy5Nb2R1bGUoJGVsZW1lbnQsIGJhc2VNb2R1bGVOYW1lLCBjb2xsZWN0aW9uKSk7XG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7IC8vIGNvbnRpbnVlIGxvb3Bcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vIExvYWQgdGhlIGRlcGVuZGVuY2llcyBmaXJzdC5cblx0XHRcdFx0Zm9yIChsZXQgaW5kZXggaW4gZGVwZW5kZW5jaWVzKSB7XG5cdFx0XHRcdFx0Y29uc3QgZGVwZW5kZW5jeSA9IGRlcGVuZGVuY2llc1tpbmRleF07IFxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmIChkZXBlbmRlbmN5LmluZGV4T2YoJy5jc3MnKSAhPT0gLTEpIHtcblx0XHRcdFx0XHRcdF9sb2FkQ3NzKGRlcGVuZGVuY3kpOyBcblx0XHRcdFx0XHRcdGRlcGVuZGVuY2llcy5zcGxpY2UoaW5kZXgsICAxKTsgXG5cdFx0XHRcdFx0XHRjb250aW51ZTsgXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIFRoZW4gY29udmVydCB0aGUgcmVsYXRpdmUgcGF0aCB0byBKU0VuZ2luZS9saWJzIGRpcmVjdG9yeS5cblx0XHRcdFx0XHRpZiAoZGVwZW5kZW5jeS5pbmRleE9mKCdodHRwJykgPT09IC0xKSB7XG5cdFx0XHRcdFx0XHRkZXBlbmRlbmNpZXNbaW5kZXhdID0ganNlLmNvcmUuY29uZmlnLmdldCgnZW5naW5lVXJsJykgKyAnL2xpYnMvJyArIGRlcGVuZGVuY3kgKyBmaWxlRXh0ZW5zaW9uO1xuXHRcdFx0XHRcdH0gZWxzZSBpZiAoZGVwZW5kZW5jeS5zdWJzdHIoLTMpICE9PSAnLmpzJykgeyAvLyBUaGVuIGFkZCB0aGUgZHluYW1pYyBmaWxlIGV4dGVuc2lvbiB0byB0aGUgVVJMLlxuXHRcdFx0XHRcdFx0ZGVwZW5kZW5jaWVzW2luZGV4XSArPSBmaWxlRXh0ZW5zaW9uO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cdFx0XHRcdHdpbmRvdy5yZXF1aXJlKGRlcGVuZGVuY2llcywgKCkgPT4ge1xuXHRcdFx0XHRcdGRlZmVycmVkLnJlc29sdmUobmV3IGpzZS5jb25zdHJ1Y3RvcnMuTW9kdWxlKCRlbGVtZW50LCBiYXNlTW9kdWxlTmFtZSwgY29sbGVjdGlvbikpO1xuXHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdH0gY2F0Y2ggKGV4Y2VwdGlvbikge1xuXHRcdFx0ZGVmZXJyZWQucmVqZWN0KGV4Y2VwdGlvbik7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGRlZmVycmVkLnByb21pc2UoKTtcblx0fTtcblxufSkoanNlLmNvcmUubW9kdWxlX2xvYWRlcik7XG4iLCIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHBvbHlmaWxscy5qcyAyMDE2LTA1LTE3XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyoqXG4gKiBKU0UgUG9seWZpbGxzIFxuICogXG4gKiBSZXF1aXJlZCBwb2x5ZmlsbHMgZm9yIGNvbXBhdGliaWxpdHkgYW1vbmcgb2xkIGJyb3dzZXJzLlxuICpcbiAqIEBtb2R1bGUgSlNFL0NvcmUvcG9seWZpbGxzXG4gKi9cbihmdW5jdGlvbiAoKSB7XG5cblx0J3VzZSBzdHJpY3QnO1xuXG5cdC8vIEludGVybmV0IEV4cGxvcmVyIGRvZXMgbm90IHN1cHBvcnQgdGhlIG9yaWdpbiBwcm9wZXJ0eSBvZiB0aGUgd2luZG93LmxvY2F0aW9uIG9iamVjdC5cblx0Ly8ge0BsaW5rIGh0dHA6Ly90b3Nib3Vybi5jb20vYS1maXgtZm9yLXdpbmRvdy1sb2NhdGlvbi1vcmlnaW4taW4taW50ZXJuZXQtZXhwbG9yZXJ9XG5cdGlmICghd2luZG93LmxvY2F0aW9uLm9yaWdpbikge1xuXHRcdHdpbmRvdy5sb2NhdGlvbi5vcmlnaW4gPSB3aW5kb3cubG9jYXRpb24ucHJvdG9jb2wgKyAnLy8nICtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhvc3RuYW1lICsgKHdpbmRvdy5sb2NhdGlvbi5wb3J0ID8gJzonICsgd2luZG93LmxvY2F0aW9uLnBvcnQgOiAnJyk7XG5cdH1cblxuXHQvLyBEYXRlLm5vdyBtZXRob2QgcG9seWZpbGxcblx0Ly8ge0BsaW5rIGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0phdmFTY3JpcHQvUmVmZXJlbmNlL0dsb2JhbF9PYmplY3RzL0RhdGUvbm93fVxuXHRpZiAoIURhdGUubm93KSB7XG5cdFx0RGF0ZS5ub3cgPSBmdW5jdGlvbiBub3coKSB7XG5cdFx0XHRyZXR1cm4gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG5cdFx0fTtcblx0fVxuXHRcbn0pKCk7XG5cblxuIiwiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiByZWdpc3RyeS5qcyAyMDE2LTA5LTA4XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuanNlLmNvcmUucmVnaXN0cnkgPSBqc2UuY29yZS5yZWdpc3RyeSB8fCB7fTtcblxuLyoqXG4gKiBKUyBFbmdpbmUgUmVnaXN0cnlcbiAqXG4gKiBUaGlzIG9iamVjdCBjb250YWlucyBzdHJpbmcgZGF0YSB0aGF0IG90aGVyIHNlY3Rpb25zIG9mIHRoZSBlbmdpbmUgbmVlZCBpbiBvcmRlciB0byBvcGVyYXRlIGNvcnJlY3RseS5cbiAqXG4gKiBAbW9kdWxlIEpTRS9Db3JlL3JlZ2lzdHJ5XG4gKi9cbihmdW5jdGlvbiAoZXhwb3J0cykge1xuXG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8qKlxuXHQgKiBDb250YWlucyB0aGUgcmVnaXN0cnkgdmFsdWVzLlxuXHQgKiBcblx0ICogQHR5cGUge09iamVjdFtdfVxuXHQgKi9cblx0Y29uc3QgcmVnaXN0cnkgPSBbXTtcblxuXHQvKipcblx0ICogU2V0IGEgdmFsdWUgaW4gdGhlIHJlZ2lzdHJ5LlxuXHQgKlxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBDb250YWlucyB0aGUgbmFtZSBvZiB0aGUgZW50cnkgdG8gYmUgYWRkZWQuXG5cdCAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGJlIHdyaXR0ZW4gaW4gdGhlIHJlZ2lzdHJ5LlxuXHQgKi9cblx0ZXhwb3J0cy5zZXQgPSBmdW5jdGlvbiAobmFtZSwgdmFsdWUpIHtcblx0XHQvLyBJZiBhIHJlZ2lzdHJ5IGVudHJ5IHdpdGggdGhlIHNhbWUgbmFtZSBleGlzdHMgYWxyZWFkeSB0aGUgZm9sbG93aW5nIGNvbnNvbGUgd2FybmluZyB3aWxsXG5cdFx0Ly8gaW5mb3JtIGRldmVsb3BlcnMgdGhhdCB0aGV5IGFyZSBvdmVyd3JpdGluZyBhbiBleGlzdGluZyB2YWx1ZSwgc29tZXRoaW5nIHVzZWZ1bCB3aGVuIGRlYnVnZ2luZy5cblx0XHRpZiAocmVnaXN0cnlbbmFtZV0gIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0anNlLmNvcmUuZGVidWcud2FybignVGhlIHJlZ2lzdHJ5IHZhbHVlIHdpdGggdGhlIG5hbWUgXCInICsgbmFtZSArICdcIiB3aWxsIGJlIG92ZXJ3cml0dGVuLicpO1xuXHRcdH1cblxuXHRcdHJlZ2lzdHJ5W25hbWVdID0gdmFsdWU7XG5cdH07XG5cblx0LyoqXG5cdCAqIEdldCBhIHZhbHVlIGZyb20gdGhlIHJlZ2lzdHJ5LlxuXHQgKlxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBUaGUgbmFtZSBvZiB0aGUgZW50cnkgdmFsdWUgdG8gYmUgcmV0dXJuZWQuXG5cdCAqXG5cdCAqIEByZXR1cm5zIHsqfSBSZXR1cm5zIHRoZSB2YWx1ZSB0aGF0IG1hdGNoZXMgdGhlIG5hbWUuXG5cdCAqL1xuXHRleHBvcnRzLmdldCA9IGZ1bmN0aW9uIChuYW1lKSB7XG5cdFx0cmV0dXJuIHJlZ2lzdHJ5W25hbWVdO1xuXHR9O1xuXG5cdC8qKlxuXHQgKiBDaGVjayB0aGUgY3VycmVudCBjb250ZW50IG9mIHRoZSByZWdpc3RyeSBvYmplY3QuXG5cdCAqXG5cdCAqIFRoaXMgbWV0aG9kIGlzIG9ubHkgYXZhaWxhYmxlIHdoZW4gdGhlIGVuZ2luZSBlbnZpcm9ubWVudCBpcyB0dXJuZWQgaW50byBkZXZlbG9wbWVudC5cblx0ICovXG5cdGV4cG9ydHMuZGVidWcgPSBmdW5jdGlvbiAoKSB7XG5cdFx0aWYgKGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2Vudmlyb25tZW50JykgPT09ICdkZXZlbG9wbWVudCcpIHtcblx0XHRcdGpzZS5jb3JlLmRlYnVnLmxvZygnUmVnaXN0cnkgT2JqZWN0OicsIHJlZ2lzdHJ5KTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdUaGlzIGZ1bmN0aW9uIGlzIG5vdCBhbGxvd2VkIGluIGEgcHJvZHVjdGlvbiBlbnZpcm9ubWVudC4nKTtcblx0XHR9XG5cdH07XG5cbn0pKGpzZS5jb3JlLnJlZ2lzdHJ5KTtcbiIsIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiByZXF1aXJlLmpzIDIwMTctMDMtMjhcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogQXN5bmNocm9ub3VzIE1vZHVsZSBMb2FkaW5nXHJcbiAqXHJcbiAqIFRoaXMgbW9kdWxlIGlzIGEgZm9yayBvZiBSZXF1aXJlSlMgd2l0aG91dCB0aGUgQU1EIGZ1bmN0aW9uYWxpdHkuIFRoZSBnbG9iYWwgXCJkZWZpbmVcIiBtZXRob2QgaXMgcmVtb3ZlZCBhc1xyXG4gKiBpdCdzIG5vdCBuZWNlc3NhcnkgYnkgSlMgRW5naW5lLiBcclxuICogXHJcbiAqIHtAbGluayBodHRwczovL2dpdGh1Yi5jb20vcmVxdWlyZWpzL3JlcXVpcmVqc31cclxuICpcclxuICogTm90IHVzaW5nIHN0cmljdDogdW5ldmVuIHN0cmljdCBzdXBwb3J0IGluIGJyb3dzZXJzLCAjMzkyLCBhbmQgY2F1c2VzIHByb2JsZW1zIHdpdGggcmVxdWlyZWpzLmV4ZWMoKS90cmFuc3BpbGVyXHJcbiAqIHBsdWdpbnMgdGhhdCBtYXkgbm90IGJlIHN0cmljdC5cclxuICovXHJcbihmdW5jdGlvbigpIHtcclxuXHRcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyBWQVJJQUJMRVNcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gXHJcblx0XHJcblx0d2luZG93LnJlcXVpcmVqcyA9IHVuZGVmaW5lZDtcclxuXHR3aW5kb3cucmVxdWlyZSA9IHVuZGVmaW5lZDtcclxuXHRcclxuXHRsZXQgcmVxOyBcclxuXHRsZXQgczsgXHJcblx0bGV0IGhlYWQ7IFxyXG5cdGxldCBiYXNlRWxlbWVudDsgXHJcblx0bGV0IGRhdGFNYWluOyBcclxuXHRsZXQgc3JjO1xyXG5cdGxldCBpbnRlcmFjdGl2ZVNjcmlwdDsgXHJcblx0bGV0IG1haW5TY3JpcHQ7IFxyXG5cdGxldCBzdWJQYXRoO1xyXG5cdGxldCB2ZXJzaW9uID0gJzIuMS4yMic7XHJcblx0bGV0IGpzU3VmZml4UmVnRXhwID0gL1xcLmpzJC87XHJcblx0bGV0IGN1cnJEaXJSZWdFeHAgPSAvXlxcLlxcLy87XHJcblx0bGV0IG9wID0gT2JqZWN0LnByb3RvdHlwZTtcclxuXHRsZXQgb3N0cmluZyA9IG9wLnRvU3RyaW5nO1xyXG5cdGxldCBoYXNPd24gPSBvcC5oYXNPd25Qcm9wZXJ0eTtcclxuXHRsZXQgaXNCcm93c2VyID0gISEodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50KTtcclxuXHRsZXQgaXNXZWJXb3JrZXIgPSAhaXNCcm93c2VyICYmIHR5cGVvZiBpbXBvcnRTY3JpcHRzICE9PSAndW5kZWZpbmVkJztcclxuXHQvLyBQUzMgaW5kaWNhdGVzIGxvYWRlZCBhbmQgY29tcGxldGUsIGJ1dCBuZWVkIHRvIHdhaXQgZm9yIGNvbXBsZXQgc3BlY2lmaWNhbGx5LiBTZXF1ZW5jZSBpcyAnbG9hZGluZycsICdsb2FkZWQnLCBcclxuXHQvLyBleGVjdXRpb24gdGhlbiAnY29tcGxldGUnLiBUaGUgVUEgY2hlY2sgaXMgdW5mb3J0dW5hdGUsIGJ1dCBub3Qgc3VyZSBob3cgdG8gZmVhdHVyZSB0ZXN0IHcvbyBjYXVzaW5nIHBlcmYgaXNzdWVzLlxyXG5cdGxldCByZWFkeVJlZ0V4cCA9IGlzQnJvd3NlciAmJiBuYXZpZ2F0b3IucGxhdGZvcm0gPT09ICdQTEFZU1RBVElPTiAzJyA/IC9eY29tcGxldGUkLyA6IC9eKGNvbXBsZXRlfGxvYWRlZCkkLztcclxuXHRsZXQgZGVmQ29udGV4dE5hbWUgPSAnXyc7XHJcblx0Ly8gT2ggdGhlIHRyYWdlZHksIGRldGVjdGluZyBvcGVyYS4gU2VlIHRoZSB1c2FnZSBvZiBpc09wZXJhIGZvciByZWFzb24uXHJcblx0bGV0IGlzT3BlcmEgPSB0eXBlb2Ygb3BlcmEgIT09ICd1bmRlZmluZWQnICYmIG9wZXJhLnRvU3RyaW5nKCkgPT09ICdbb2JqZWN0IE9wZXJhXSc7XHJcblx0bGV0IGNvbnRleHRzID0ge307XHJcblx0bGV0IGNmZyA9IHt9O1xyXG5cdGxldCBnbG9iYWxEZWZRdWV1ZSA9IFtdO1xyXG5cdGxldCB1c2VJbnRlcmFjdGl2ZSA9IGZhbHNlO1xyXG5cdFxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vIEZVTkNUSU9OU1xyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSBcclxuXHRcclxuXHQvKipcclxuXHQgKiBDaGVjayB3aGV0aGVyIHZhbHVlIGlzIGEgZnVuY3Rpb24uIFxyXG5cdCAqIFxyXG5cdCAqIEBwYXJhbSB7Kn0gaXQgVmFsdWUgdG8gYmUgY2hlY2tlZC5cclxuXHQgKiBcclxuXHQgKiBAcmV0dXJuIHtib29sZWFufSBSZXR1cm5zIHRoZSB2YWxpZGF0aW9uIHJlc3VsdC5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBpc0Z1bmN0aW9uKGl0KSB7XHJcblx0XHRyZXR1cm4gb3N0cmluZy5jYWxsKGl0KSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJztcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2hlY2sgd2hldGhlciB2YWx1ZSBpcyBhbiBhcnJheS5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7Kn0gaXQgVmFsdWUgdG8gYmUgY2hlY2tlZC5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm4ge2Jvb2xlYW59IFJldHVybnMgdGhlIHZhbGlkYXRpb24gcmVzdWx0LlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIGlzQXJyYXkoaXQpIHtcclxuXHRcdHJldHVybiBvc3RyaW5nLmNhbGwoaXQpID09PSAnW29iamVjdCBBcnJheV0nO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBIZWxwZXIgZnVuY3Rpb24gZm9yIGl0ZXJhdGluZyBvdmVyIGFuIGFycmF5LiBcclxuXHQgKiBcclxuXHQgKiBJZiB0aGUgZnVuYyByZXR1cm5zIGEgdHJ1ZSB2YWx1ZSwgaXQgd2lsbCBicmVhayBvdXQgb2YgdGhlIGxvb3AuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gZWFjaChhcnksIGZ1bmMpIHtcclxuXHRcdGlmIChhcnkpIHtcclxuXHRcdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBhcnkubGVuZ3RoOyBpICs9IDEpIHtcclxuXHRcdFx0XHRpZiAoYXJ5W2ldICYmIGZ1bmMoYXJ5W2ldLCBpLCBhcnkpKSB7XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogSGVscGVyIGZ1bmN0aW9uIGZvciBpdGVyYXRpbmcgb3ZlciBhbiBhcnJheSBiYWNrd2FyZHMuIFxyXG5cdCAqIFxyXG5cdCAqIElmIHRoZSBmdW5jIHJldHVybnMgYSB0cnVlIHZhbHVlLCBpdCB3aWxsIGJyZWFrIG91dCBvZiB0aGUgbG9vcC5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBlYWNoUmV2ZXJzZShhcnksIGZ1bmMpIHtcclxuXHRcdGlmIChhcnkpIHtcclxuXHRcdFx0bGV0IGk7XHJcblx0XHRcdGZvciAoaSA9IGFyeS5sZW5ndGggLSAxOyBpID4gLTE7IGkgLT0gMSkge1xyXG5cdFx0XHRcdGlmIChhcnlbaV0gJiYgZnVuYyhhcnlbaV0sIGksIGFyeSkpIHtcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBDaGVjayB3aGV0aGVyIGFuIG9iamVjdCBoYXMgYSBzcGVjaWZpYyBwcm9wZXJ0eS4gXHJcblx0ICogXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IG9iaiBPYmplY3QgdG8gYmUgY2hlY2tlZC5cclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gcHJvcCBQcm9wZXJ0eSBuYW1lIHRvIGJlIGNoZWNrZWQuIFxyXG5cdCAqIFxyXG5cdCAqIEByZXR1cm4ge0Jvb2xlYW59IFJldHVybnMgdGhlIHZhbGlkYXRpb24gcmVzdWx0LlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIGhhc1Byb3Aob2JqLCBwcm9wKSB7XHJcblx0XHRyZXR1cm4gaGFzT3duLmNhbGwob2JqLCBwcm9wKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2hlY2sgaWYgYW4gb2JqZWN0IGhhcyBhIHByb3BlcnR5IGFuZCBpZiB0aGF0IHByb3BlcnR5IGNvbnRhaW5zIGEgdHJ1dGh5IHZhbHVlLiBcclxuXHQgKiBcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gb2JqIE9iamVjdCB0byBiZSBjaGVja2VkLlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wIFByb3BlcnR5IG5hbWUgdG8gYmUgY2hlY2tlZC5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm4ge0Jvb2xlYW59IFJldHVybnMgdGhlIHZhbGlkYXRpb24gcmVzdWx0LlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIGdldE93bihvYmosIHByb3ApIHtcclxuXHRcdHJldHVybiBoYXNQcm9wKG9iaiwgcHJvcCkgJiYgb2JqW3Byb3BdO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBDeWNsZXMgb3ZlciBwcm9wZXJ0aWVzIGluIGFuIG9iamVjdCBhbmQgY2FsbHMgYSBmdW5jdGlvbiBmb3IgZWFjaCBwcm9wZXJ0eSB2YWx1ZS4gXHJcblx0ICogXHJcblx0ICogSWYgdGhlIGZ1bmN0aW9uIHJldHVybnMgYSB0cnV0aHkgdmFsdWUsIHRoZW4gdGhlIGl0ZXJhdGlvbiBpcyBzdG9wcGVkLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIGVhY2hQcm9wKG9iaiwgZnVuYykge1xyXG5cdFx0bGV0IHByb3A7XHJcblx0XHRmb3IgKHByb3AgaW4gb2JqKSB7XHJcblx0XHRcdGlmIChoYXNQcm9wKG9iaiwgcHJvcCkpIHtcclxuXHRcdFx0XHRpZiAoZnVuYyhvYmpbcHJvcF0sIHByb3ApKSB7XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2ltcGxlIGZ1bmN0aW9uIHRvIG1peCBpbiBwcm9wZXJ0aWVzIGZyb20gc291cmNlIGludG8gdGFyZ2V0LCBidXQgb25seSBpZiB0YXJnZXQgZG9lcyBub3QgYWxyZWFkeSBoYXZlIGEgXHJcblx0ICogcHJvcGVydHkgb2YgdGhlIHNhbWUgbmFtZS5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBtaXhpbih0YXJnZXQsIHNvdXJjZSwgZm9yY2UsIGRlZXBTdHJpbmdNaXhpbikge1xyXG5cdFx0aWYgKHNvdXJjZSkge1xyXG5cdFx0XHRlYWNoUHJvcChzb3VyY2UsIGZ1bmN0aW9uKHZhbHVlLCBwcm9wKSB7XHJcblx0XHRcdFx0aWYgKGZvcmNlIHx8ICFoYXNQcm9wKHRhcmdldCwgcHJvcCkpIHtcclxuXHRcdFx0XHRcdGlmIChkZWVwU3RyaW5nTWl4aW4gJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJlxyXG5cdFx0XHRcdFx0XHQhaXNBcnJheSh2YWx1ZSkgJiYgIWlzRnVuY3Rpb24odmFsdWUpICYmXHJcblx0XHRcdFx0XHRcdCEodmFsdWUgaW5zdGFuY2VvZiBSZWdFeHApKSB7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRpZiAoIXRhcmdldFtwcm9wXSkge1xyXG5cdFx0XHRcdFx0XHRcdHRhcmdldFtwcm9wXSA9IHt9O1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdG1peGluKHRhcmdldFtwcm9wXSwgdmFsdWUsIGZvcmNlLCBkZWVwU3RyaW5nTWl4aW4pO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0dGFyZ2V0W3Byb3BdID0gdmFsdWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHRcdHJldHVybiB0YXJnZXQ7XHJcblx0fVxyXG5cdFxyXG5cdC8vIFNpbWlsYXIgdG8gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQsIGJ1dCB0aGUgJ3RoaXMnIG9iamVjdCBpcyBzcGVjaWZpZWQgZmlyc3QsIHNpbmNlIGl0IGlzIGVhc2llciB0byByZWFkL2ZpZ3VyZSBcclxuXHQvLyBvdXQgd2hhdCAndGhpcycgd2lsbCBiZS5cclxuXHRmdW5jdGlvbiBiaW5kKG9iaiwgZm4pIHtcclxuXHRcdHJldHVybiBmdW5jdGlvbigpIHtcclxuXHRcdFx0cmV0dXJuIGZuLmFwcGx5KG9iaiwgYXJndW1lbnRzKTtcclxuXHRcdH07XHJcblx0fVxyXG5cdFxyXG5cdGZ1bmN0aW9uIHNjcmlwdHMoKSB7XHJcblx0XHRyZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ3NjcmlwdCcpO1xyXG5cdH1cclxuXHRcclxuXHRmdW5jdGlvbiBkZWZhdWx0T25FcnJvcihlcnIpIHtcclxuXHRcdHRocm93IGVycjtcclxuXHR9XHJcblx0XHJcblx0Ly8gQWxsb3cgZ2V0dGluZyBhIGdsb2JhbCB0aGF0IGlzIGV4cHJlc3NlZCBpbiBkb3Qgbm90YXRpb24sIGxpa2UgJ2EuYi5jJy5cclxuXHRmdW5jdGlvbiBnZXRHbG9iYWwodmFsdWUpIHtcclxuXHRcdGlmICghdmFsdWUpIHtcclxuXHRcdFx0cmV0dXJuIHZhbHVlO1xyXG5cdFx0fVxyXG5cdFx0bGV0IGcgPSBnbG9iYWw7XHJcblx0XHRlYWNoKHZhbHVlLnNwbGl0KCcuJyksIGZ1bmN0aW9uKHBhcnQpIHtcclxuXHRcdFx0ZyA9IGdbcGFydF07XHJcblx0XHR9KTtcclxuXHRcdHJldHVybiBnO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBDb25zdHJ1Y3RzIGFuIGVycm9yIHdpdGggYSBwb2ludGVyIHRvIGFuIFVSTCB3aXRoIG1vcmUgaW5mb3JtYXRpb24uXHJcblx0ICogXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IGlkIFRoZSBlcnJvciBJRCB0aGF0IG1hcHMgdG8gYW4gSUQgb24gYSB3ZWIgcGFnZS5cclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbXNnIEh1bWFuIHJlYWRhYmxlIGVycm9yLlxyXG5cdCAqIEBwYXJhbSB7RXJyb3J9IFtlcnJdIFRoZSBvcmlnaW5hbCBlcnJvciwgaWYgdGhlcmUgaXMgb25lLlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7RXJyb3J9XHJcblx0ICovXHJcblx0ZnVuY3Rpb24gbWFrZUVycm9yKGlkLCBtc2csIGVyciwgcmVxdWlyZU1vZHVsZXMpIHtcclxuXHRcdGNvbnN0IGVycm9yID0gbmV3IEVycm9yKG1zZyArICdcXG5odHRwOi8vcmVxdWlyZWpzLm9yZy9kb2NzL2Vycm9ycy5odG1sIycgKyBpZCk7XHJcblx0XHRcclxuXHRcdGVycm9yLnJlcXVpcmVUeXBlID0gaWQ7XHJcblx0XHRlcnJvci5yZXF1aXJlTW9kdWxlcyA9IHJlcXVpcmVNb2R1bGVzO1xyXG5cdFx0XHJcblx0XHRpZiAoZXJyKSB7XHJcblx0XHRcdGVycm9yLm9yaWdpbmFsRXJyb3IgPSBlcnI7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdHJldHVybiBlcnJvcjtcclxuXHR9XHJcblx0XHJcblx0aWYgKHR5cGVvZiB3aW5kb3cucmVxdWlyZWpzICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0aWYgKGlzRnVuY3Rpb24od2luZG93LnJlcXVpcmVqcykpIHtcclxuXHRcdFx0Ly8gRG8gbm90IG92ZXJ3cml0ZSBhbiBleGlzdGluZyByZXF1aXJlanMgaW5zdGFuY2UuXHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdGNmZyA9IHdpbmRvdy5yZXF1aXJlanM7XHJcblx0XHR3aW5kb3cucmVxdWlyZWpzID0gdW5kZWZpbmVkO1xyXG5cdH1cclxuXHRcclxuXHQvLyBBbGxvdyBmb3IgYSByZXF1aXJlIGNvbmZpZyBvYmplY3RcclxuXHRpZiAodHlwZW9mIHdpbmRvdy5yZXF1aXJlICE9PSAndW5kZWZpbmVkJyAmJiAhaXNGdW5jdGlvbih3aW5kb3cucmVxdWlyZSkpIHtcclxuXHRcdC8vIGFzc3VtZSBpdCBpcyBhIGNvbmZpZyBvYmplY3QuXHJcblx0XHRjZmcgPSB3aW5kb3cucmVxdWlyZTtcclxuXHRcdHdpbmRvdy5yZXF1aXJlID0gdW5kZWZpbmVkO1xyXG5cdH1cclxuXHRcclxuXHRmdW5jdGlvbiBuZXdDb250ZXh0KGNvbnRleHROYW1lKSB7XHJcblx0XHRsZXQgaW5DaGVja0xvYWRlZCwgTW9kdWxlLCBjb250ZXh0LCBoYW5kbGVycyxcclxuXHRcdFx0Y2hlY2tMb2FkZWRUaW1lb3V0SWQsXHJcblx0XHRcdGNvbmZpZyA9IHtcclxuXHRcdFx0XHQvLyBEZWZhdWx0cy4gRG8gbm90IHNldCBhIGRlZmF1bHQgZm9yIG1hcFxyXG5cdFx0XHRcdC8vIGNvbmZpZyB0byBzcGVlZCB1cCBub3JtYWxpemUoKSwgd2hpY2hcclxuXHRcdFx0XHQvLyB3aWxsIHJ1biBmYXN0ZXIgaWYgdGhlcmUgaXMgbm8gZGVmYXVsdC5cclxuXHRcdFx0XHR3YWl0U2Vjb25kczogNyxcclxuXHRcdFx0XHRiYXNlVXJsOiAnLi8nLFxyXG5cdFx0XHRcdHBhdGhzOiB7fSxcclxuXHRcdFx0XHRidW5kbGVzOiB7fSxcclxuXHRcdFx0XHRwa2dzOiB7fSxcclxuXHRcdFx0XHRzaGltOiB7fSxcclxuXHRcdFx0XHRjb25maWc6IHt9XHJcblx0XHRcdH0sXHJcblx0XHRcdHJlZ2lzdHJ5ID0ge30sXHJcblx0XHRcdC8vIHJlZ2lzdHJ5IG9mIGp1c3QgZW5hYmxlZCBtb2R1bGVzLCB0byBzcGVlZFxyXG5cdFx0XHQvLyBjeWNsZSBicmVha2luZyBjb2RlIHdoZW4gbG90cyBvZiBtb2R1bGVzXHJcblx0XHRcdC8vIGFyZSByZWdpc3RlcmVkLCBidXQgbm90IGFjdGl2YXRlZC5cclxuXHRcdFx0ZW5hYmxlZFJlZ2lzdHJ5ID0ge30sXHJcblx0XHRcdHVuZGVmRXZlbnRzID0ge30sXHJcblx0XHRcdGRlZlF1ZXVlID0gW10sXHJcblx0XHRcdGRlZmluZWQgPSB7fSxcclxuXHRcdFx0dXJsRmV0Y2hlZCA9IHt9LFxyXG5cdFx0XHRidW5kbGVzTWFwID0ge30sXHJcblx0XHRcdHJlcXVpcmVDb3VudGVyID0gMSxcclxuXHRcdFx0dW5ub3JtYWxpemVkQ291bnRlciA9IDE7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogVHJpbXMgdGhlIC4gYW5kIC4uIGZyb20gYW4gYXJyYXkgb2YgcGF0aCBzZWdtZW50cy5cclxuXHRcdCAqIFxyXG5cdFx0ICogSXQgd2lsbCBrZWVwIGEgbGVhZGluZyBwYXRoIHNlZ21lbnQgaWYgYSAuLiB3aWxsIGJlY29tZSB0aGUgZmlyc3QgcGF0aCBzZWdtZW50LCB0byBoZWxwIHdpdGggbW9kdWxlIG5hbWUgXHJcblx0XHQgKiBsb29rdXBzLCB3aGljaCBhY3QgbGlrZSBwYXRocywgYnV0IGNhbiBiZSByZW1hcHBlZC4gQnV0IHRoZSBlbmQgcmVzdWx0LCBhbGwgcGF0aHMgdGhhdCB1c2UgdGhpcyBmdW5jdGlvbiBcclxuXHRcdCAqIHNob3VsZCBsb29rIG5vcm1hbGl6ZWQuXHJcblx0XHQgKiBcclxuXHRcdCAqIE5PVEU6IHRoaXMgbWV0aG9kIE1PRElGSUVTIHRoZSBpbnB1dCBhcnJheS5cclxuXHRcdCAqIFxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gYXJ5IHRoZSBhcnJheSBvZiBwYXRoIHNlZ21lbnRzLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiB0cmltRG90cyhhcnkpIHtcclxuXHRcdFx0bGV0IGksIHBhcnQ7XHJcblx0XHRcdGZvciAoaSA9IDA7IGkgPCBhcnkubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRwYXJ0ID0gYXJ5W2ldO1xyXG5cdFx0XHRcdGlmIChwYXJ0ID09PSAnLicpIHtcclxuXHRcdFx0XHRcdGFyeS5zcGxpY2UoaSwgMSk7XHJcblx0XHRcdFx0XHRpIC09IDE7XHJcblx0XHRcdFx0fSBlbHNlIGlmIChwYXJ0ID09PSAnLi4nKSB7XHJcblx0XHRcdFx0XHQvLyBJZiBhdCB0aGUgc3RhcnQsIG9yIHByZXZpb3VzIHZhbHVlIGlzIHN0aWxsIC4uLCAga2VlcCB0aGVtIHNvIHRoYXQgd2hlbiBjb252ZXJ0ZWQgdG8gYSBwYXRoIGl0IFxyXG5cdFx0XHRcdFx0Ly8gbWF5IHN0aWxsIHdvcmsgd2hlbiBjb252ZXJ0ZWQgdG8gYSBwYXRoLCBldmVuIHRob3VnaCBhcyBhbiBJRCBpdCBpcyBsZXNzIHRoYW4gaWRlYWwuIEluIGxhcmdlciBcclxuXHRcdFx0XHRcdC8vIHBvaW50IHJlbGVhc2VzLCBtYXkgYmUgYmV0dGVyIHRvIGp1c3Qga2ljayBvdXQgYW4gZXJyb3IuXHJcblx0XHRcdFx0XHRpZiAoaSA9PT0gMCB8fCAoaSA9PT0gMSAmJiBhcnlbMl0gPT09ICcuLicpIHx8IGFyeVtpIC0gMV0gPT09ICcuLicpIHtcclxuXHRcdFx0XHRcdFx0Y29udGludWU7XHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKGkgPiAwKSB7XHJcblx0XHRcdFx0XHRcdGFyeS5zcGxpY2UoaSAtIDEsIDIpO1xyXG5cdFx0XHRcdFx0XHRpIC09IDI7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogR2l2ZW4gYSByZWxhdGl2ZSBtb2R1bGUgbmFtZSwgbGlrZSAuL3NvbWV0aGluZywgbm9ybWFsaXplIGl0IHRvIGEgcmVhbCBuYW1lIHRoYXQgY2FuIGJlIG1hcHBlZCB0byBhIHBhdGguXHJcblx0XHQgKiBcclxuXHRcdCAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIHRoZSByZWxhdGl2ZSBuYW1lXHJcblx0XHQgKiBAcGFyYW0ge1N0cmluZ30gYmFzZU5hbWUgYSByZWFsIG5hbWUgdGhhdCB0aGUgbmFtZSBhcmcgaXMgcmVsYXRpdmUgdG8uXHJcblx0XHQgKiBAcGFyYW0ge0Jvb2xlYW59IGFwcGx5TWFwIGFwcGx5IHRoZSBtYXAgY29uZmlnIHRvIHRoZSB2YWx1ZS4gU2hvdWxkIG9ubHkgYmUgZG9uZSBpZiB0aGlzIG5vcm1hbGl6YXRpb24gaXMgXHJcblx0XHQgKiBmb3IgYSBkZXBlbmRlbmN5IElELlxyXG5cdFx0ICogXHJcblx0XHQgKiBAcmV0dXJuIHtTdHJpbmd9IG5vcm1hbGl6ZWQgbmFtZVxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBub3JtYWxpemUobmFtZSwgYmFzZU5hbWUsIGFwcGx5TWFwKSB7XHJcblx0XHRcdGxldCBwa2dNYWluLCBtYXBWYWx1ZSwgbmFtZVBhcnRzLCBpLCBqLCBuYW1lU2VnbWVudCwgbGFzdEluZGV4LFxyXG5cdFx0XHRcdGZvdW5kTWFwLCBmb3VuZEksIGZvdW5kU3Rhck1hcCwgc3RhckksIG5vcm1hbGl6ZWRCYXNlUGFydHMsXHJcblx0XHRcdFx0YmFzZVBhcnRzID0gKGJhc2VOYW1lICYmIGJhc2VOYW1lLnNwbGl0KCcvJykpLFxyXG5cdFx0XHRcdG1hcCA9IGNvbmZpZy5tYXAsXHJcblx0XHRcdFx0c3Rhck1hcCA9IG1hcCAmJiBtYXBbJyonXTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEFkanVzdCBhbnkgcmVsYXRpdmUgcGF0aHMuXHJcblx0XHRcdGlmIChuYW1lKSB7XHJcblx0XHRcdFx0bmFtZSA9IG5hbWUuc3BsaXQoJy8nKTtcclxuXHRcdFx0XHRsYXN0SW5kZXggPSBuYW1lLmxlbmd0aCAtIDE7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gSWYgd2FudGluZyBub2RlIElEIGNvbXBhdGliaWxpdHksIHN0cmlwIC5qcyBmcm9tIGVuZCBvZiBJRHMuIEhhdmUgdG8gZG8gdGhpcyBoZXJlLCBhbmQgbm90IGluIFxyXG5cdFx0XHRcdC8vIG5hbWVUb1VybCBiZWNhdXNlIG5vZGUgYWxsb3dzIGVpdGhlciAuanMgb3Igbm9uIC5qcyB0byBtYXAgdG8gc2FtZSBmaWxlLlxyXG5cdFx0XHRcdGlmIChjb25maWcubm9kZUlkQ29tcGF0ICYmIGpzU3VmZml4UmVnRXhwLnRlc3QobmFtZVtsYXN0SW5kZXhdKSkge1xyXG5cdFx0XHRcdFx0bmFtZVtsYXN0SW5kZXhdID0gbmFtZVtsYXN0SW5kZXhdLnJlcGxhY2UoanNTdWZmaXhSZWdFeHAsICcnKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gU3RhcnRzIHdpdGggYSAnLicgc28gbmVlZCB0aGUgYmFzZU5hbWVcclxuXHRcdFx0XHRpZiAobmFtZVswXS5jaGFyQXQoMCkgPT09ICcuJyAmJiBiYXNlUGFydHMpIHtcclxuXHRcdFx0XHRcdC8vIENvbnZlcnQgYmFzZU5hbWUgdG8gYXJyYXksIGFuZCBsb3Agb2ZmIHRoZSBsYXN0IHBhcnQsIHNvIHRoYXQgLiBtYXRjaGVzIHRoYXQgJ2RpcmVjdG9yeScgYW5kIG5vdCBcclxuXHRcdFx0XHRcdC8vIG5hbWUgb2YgdGhlIGJhc2VOYW1lJ3MgbW9kdWxlLiBGb3IgaW5zdGFuY2UsIGJhc2VOYW1lIG9mICdvbmUvdHdvL3RocmVlJywgbWFwcyB0byBcclxuXHRcdFx0XHRcdC8vICdvbmUvdHdvL3RocmVlLmpzJywgYnV0IHdlIHdhbnQgdGhlIGRpcmVjdG9yeSwgJ29uZS90d28nIGZvciB0aGlzIG5vcm1hbGl6YXRpb24uXHJcblx0XHRcdFx0XHRub3JtYWxpemVkQmFzZVBhcnRzID0gYmFzZVBhcnRzLnNsaWNlKDAsIGJhc2VQYXJ0cy5sZW5ndGggLSAxKTtcclxuXHRcdFx0XHRcdG5hbWUgPSBub3JtYWxpemVkQmFzZVBhcnRzLmNvbmNhdChuYW1lKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0dHJpbURvdHMobmFtZSk7XHJcblx0XHRcdFx0bmFtZSA9IG5hbWUuam9pbignLycpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBBcHBseSBtYXAgY29uZmlnIGlmIGF2YWlsYWJsZS5cclxuXHRcdFx0aWYgKGFwcGx5TWFwICYmIG1hcCAmJiAoYmFzZVBhcnRzIHx8IHN0YXJNYXApKSB7XHJcblx0XHRcdFx0bmFtZVBhcnRzID0gbmFtZS5zcGxpdCgnLycpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdG91dGVyTG9vcDogZm9yIChpID0gbmFtZVBhcnRzLmxlbmd0aDsgaSA+IDA7IGkgLT0gMSkge1xyXG5cdFx0XHRcdFx0bmFtZVNlZ21lbnQgPSBuYW1lUGFydHMuc2xpY2UoMCwgaSkuam9pbignLycpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRpZiAoYmFzZVBhcnRzKSB7XHJcblx0XHRcdFx0XHRcdC8vIEZpbmQgdGhlIGxvbmdlc3QgYmFzZU5hbWUgc2VnbWVudCBtYXRjaCBpbiB0aGUgY29uZmlnLiBTbywgZG8gam9pbnMgb24gdGhlIGJpZ2dlc3QgdG8gXHJcblx0XHRcdFx0XHRcdC8vIHNtYWxsZXN0IGxlbmd0aHMgb2YgYmFzZVBhcnRzLlxyXG5cdFx0XHRcdFx0XHRmb3IgKGogPSBiYXNlUGFydHMubGVuZ3RoOyBqID4gMDsgaiAtPSAxKSB7XHJcblx0XHRcdFx0XHRcdFx0bWFwVmFsdWUgPSBnZXRPd24obWFwLCBiYXNlUGFydHMuc2xpY2UoMCwgaikuam9pbignLycpKTtcclxuXHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHQvLyBiYXNlTmFtZSBzZWdtZW50IGhhcyBjb25maWcsIGZpbmQgaWYgaXQgaGFzIG9uZSBmb3IgdGhpcyBuYW1lLlxyXG5cdFx0XHRcdFx0XHRcdGlmIChtYXBWYWx1ZSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0bWFwVmFsdWUgPSBnZXRPd24obWFwVmFsdWUsIG5hbWVTZWdtZW50KTtcclxuXHRcdFx0XHRcdFx0XHRcdGlmIChtYXBWYWx1ZSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQvLyBNYXRjaCwgdXBkYXRlIG5hbWUgdG8gdGhlIG5ldyB2YWx1ZS5cclxuXHRcdFx0XHRcdFx0XHRcdFx0Zm91bmRNYXAgPSBtYXBWYWx1ZTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0Zm91bmRJID0gaTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWsgb3V0ZXJMb29wO1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBDaGVjayBmb3IgYSBzdGFyIG1hcCBtYXRjaCwgYnV0IGp1c3QgaG9sZCBvbiB0byBpdCwgaWYgdGhlcmUgaXMgYSBzaG9ydGVyIHNlZ21lbnQgbWF0Y2ggbGF0ZXIgaW4gXHJcblx0XHRcdFx0XHQvLyBhIG1hdGNoaW5nIGNvbmZpZywgdGhlbiBmYXZvciBvdmVyIHRoaXMgc3RhciBtYXAuXHJcblx0XHRcdFx0XHRpZiAoIWZvdW5kU3Rhck1hcCAmJiBzdGFyTWFwICYmIGdldE93bihzdGFyTWFwLCBuYW1lU2VnbWVudCkpIHtcclxuXHRcdFx0XHRcdFx0Zm91bmRTdGFyTWFwID0gZ2V0T3duKHN0YXJNYXAsIG5hbWVTZWdtZW50KTtcclxuXHRcdFx0XHRcdFx0c3RhckkgPSBpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoIWZvdW5kTWFwICYmIGZvdW5kU3Rhck1hcCkge1xyXG5cdFx0XHRcdFx0Zm91bmRNYXAgPSBmb3VuZFN0YXJNYXA7XHJcblx0XHRcdFx0XHRmb3VuZEkgPSBzdGFySTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKGZvdW5kTWFwKSB7XHJcblx0XHRcdFx0XHRuYW1lUGFydHMuc3BsaWNlKDAsIGZvdW5kSSwgZm91bmRNYXApO1xyXG5cdFx0XHRcdFx0bmFtZSA9IG5hbWVQYXJ0cy5qb2luKCcvJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBJZiB0aGUgbmFtZSBwb2ludHMgdG8gYSBwYWNrYWdlJ3MgbmFtZSwgdXNlIHRoZSBwYWNrYWdlIG1haW4gaW5zdGVhZC5cclxuXHRcdFx0cGtnTWFpbiA9IGdldE93bihjb25maWcucGtncywgbmFtZSk7XHJcblx0XHRcdFxyXG5cdFx0XHRyZXR1cm4gcGtnTWFpbiA/IHBrZ01haW4gOiBuYW1lO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRmdW5jdGlvbiByZW1vdmVTY3JpcHQobmFtZSkge1xyXG5cdFx0XHRpZiAoaXNCcm93c2VyKSB7XHJcblx0XHRcdFx0ZWFjaChzY3JpcHRzKCksIGZ1bmN0aW9uKHNjcmlwdE5vZGUpIHtcclxuXHRcdFx0XHRcdGlmIChzY3JpcHROb2RlLmdldEF0dHJpYnV0ZSgnZGF0YS1yZXF1aXJlbW9kdWxlJykgPT09IG5hbWUgJiZcclxuXHRcdFx0XHRcdFx0c2NyaXB0Tm9kZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtcmVxdWlyZWNvbnRleHQnKSA9PT0gY29udGV4dC5jb250ZXh0TmFtZSkge1xyXG5cdFx0XHRcdFx0XHRzY3JpcHROb2RlLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc2NyaXB0Tm9kZSk7XHJcblx0XHRcdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdGZ1bmN0aW9uIGhhc1BhdGhGYWxsYmFjayhpZCkge1xyXG5cdFx0XHRsZXQgcGF0aENvbmZpZyA9IGdldE93bihjb25maWcucGF0aHMsIGlkKTtcclxuXHRcdFx0aWYgKHBhdGhDb25maWcgJiYgaXNBcnJheShwYXRoQ29uZmlnKSAmJiBwYXRoQ29uZmlnLmxlbmd0aCA+IDEpIHtcclxuXHRcdFx0XHQvLyBQb3Agb2ZmIHRoZSBmaXJzdCBhcnJheSB2YWx1ZSwgc2luY2UgaXQgZmFpbGVkLCBhbmQgcmV0cnkuXHJcblx0XHRcdFx0cGF0aENvbmZpZy5zaGlmdCgpO1xyXG5cdFx0XHRcdGNvbnRleHQucmVxdWlyZS51bmRlZihpZCk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gQ3VzdG9tIHJlcXVpcmUgdGhhdCBkb2VzIG5vdCBkbyBtYXAgdHJhbnNsYXRpb24sIHNpbmNlIElEIGlzIFwiYWJzb2x1dGVcIiwgYWxyZWFkeSBtYXBwZWQvcmVzb2x2ZWQuXHJcblx0XHRcdFx0Y29udGV4dC5tYWtlUmVxdWlyZShudWxsLCB7XHJcblx0XHRcdFx0XHRza2lwTWFwOiB0cnVlXHJcblx0XHRcdFx0fSkoW2lkXSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Ly8gVHVybnMgYSBwbHVnaW4hcmVzb3VyY2UgdG8gW3BsdWdpbiwgcmVzb3VyY2VdIHdpdGggdGhlIHBsdWdpbiBiZWluZyB1bmRlZmluZWQgaWYgdGhlIG5hbWUgZGlkIG5vdCBoYXZlIGEgXHJcblx0XHQvLyBwbHVnaW4gcHJlZml4LlxyXG5cdFx0ZnVuY3Rpb24gc3BsaXRQcmVmaXgobmFtZSkge1xyXG5cdFx0XHRsZXQgcHJlZml4LFxyXG5cdFx0XHRcdGluZGV4ID0gbmFtZSA/IG5hbWUuaW5kZXhPZignIScpIDogLTE7XHJcblx0XHRcdGlmIChpbmRleCA+IC0xKSB7XHJcblx0XHRcdFx0cHJlZml4ID0gbmFtZS5zdWJzdHJpbmcoMCwgaW5kZXgpO1xyXG5cdFx0XHRcdG5hbWUgPSBuYW1lLnN1YnN0cmluZyhpbmRleCArIDEsIG5hbWUubGVuZ3RoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gW3ByZWZpeCwgbmFtZV07XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogQ3JlYXRlcyBhIG1vZHVsZSBtYXBwaW5nIHRoYXQgaW5jbHVkZXMgcGx1Z2luIHByZWZpeCwgbW9kdWxlIG5hbWUsIGFuZCBwYXRoLiBJZiBwYXJlbnRNb2R1bGVNYXAgaXMgcHJvdmlkZWQgXHJcblx0XHQgKiBpdCB3aWxsIGFsc28gbm9ybWFsaXplIHRoZSBuYW1lIHZpYSByZXF1aXJlLm5vcm1hbGl6ZSgpXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgVGhlIG1vZHVsZSBuYW1lLlxyXG5cdFx0ICogQHBhcmFtIHtTdHJpbmd9IFtwYXJlbnRNb2R1bGVNYXBdIFBhcmVudCBtb2R1bGUgbWFwIGZvciB0aGUgbW9kdWxlIG5hbWUsIHVzZWQgdG8gcmVzb2x2ZSByZWxhdGl2ZSBuYW1lcy5cclxuXHRcdCAqIEBwYXJhbSB7Qm9vbGVhbn0gaXNOb3JtYWxpemVkIElzIHRoZSBJRCBhbHJlYWR5IG5vcm1hbGl6ZWQ/IFRoaXMgaXMgdHJ1ZSBpZiB0aGlzIGNhbGwgaXMgZG9uZSBmb3IgYSBkZWZpbmUoKSBcclxuXHRcdCAqIG1vZHVsZSBJRC5cclxuXHRcdCAqIEBwYXJhbSB7Qm9vbGVhbn0gYXBwbHlNYXA6IGFwcGx5IHRoZSBtYXAgY29uZmlnIHRvIHRoZSBJRC4gU2hvdWxkIG9ubHkgYmUgdHJ1ZSBpZiB0aGlzIG1hcCBpcyBmb3IgYSBkZXBlbmRlbmN5LlxyXG5cdFx0ICpcclxuXHRcdCAqIEByZXR1cm4ge09iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gbWFrZU1vZHVsZU1hcChuYW1lLCBwYXJlbnRNb2R1bGVNYXAsIGlzTm9ybWFsaXplZCwgYXBwbHlNYXApIHtcclxuXHRcdFx0bGV0IHVybCwgcGx1Z2luTW9kdWxlLCBzdWZmaXgsIG5hbWVQYXJ0cyxcclxuXHRcdFx0XHRwcmVmaXggPSBudWxsLFxyXG5cdFx0XHRcdHBhcmVudE5hbWUgPSBwYXJlbnRNb2R1bGVNYXAgPyBwYXJlbnRNb2R1bGVNYXAubmFtZSA6IG51bGwsXHJcblx0XHRcdFx0b3JpZ2luYWxOYW1lID0gbmFtZSxcclxuXHRcdFx0XHRpc0RlZmluZSA9IHRydWUsXHJcblx0XHRcdFx0bm9ybWFsaXplZE5hbWUgPSAnJztcclxuXHRcdFx0XHJcblx0XHRcdC8vIElmIG5vIG5hbWUsIHRoZW4gaXQgbWVhbnMgaXQgaXMgYSByZXF1aXJlIGNhbGwsIGdlbmVyYXRlIGFuXHJcblx0XHRcdC8vIGludGVybmFsIG5hbWUuXHJcblx0XHRcdGlmICghbmFtZSkge1xyXG5cdFx0XHRcdGlzRGVmaW5lID0gZmFsc2U7XHJcblx0XHRcdFx0bmFtZSA9ICdfQHInICsgKHJlcXVpcmVDb3VudGVyICs9IDEpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRuYW1lUGFydHMgPSBzcGxpdFByZWZpeChuYW1lKTtcclxuXHRcdFx0cHJlZml4ID0gbmFtZVBhcnRzWzBdO1xyXG5cdFx0XHRuYW1lID0gbmFtZVBhcnRzWzFdO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKHByZWZpeCkge1xyXG5cdFx0XHRcdHByZWZpeCA9IG5vcm1hbGl6ZShwcmVmaXgsIHBhcmVudE5hbWUsIGFwcGx5TWFwKTtcclxuXHRcdFx0XHRwbHVnaW5Nb2R1bGUgPSBnZXRPd24oZGVmaW5lZCwgcHJlZml4KTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0Ly8gQWNjb3VudCBmb3IgcmVsYXRpdmUgcGF0aHMgaWYgdGhlcmUgaXMgYSBiYXNlIG5hbWUuXHJcblx0XHRcdGlmIChuYW1lKSB7XHJcblx0XHRcdFx0aWYgKHByZWZpeCkge1xyXG5cdFx0XHRcdFx0aWYgKHBsdWdpbk1vZHVsZSAmJiBwbHVnaW5Nb2R1bGUubm9ybWFsaXplKSB7XHJcblx0XHRcdFx0XHRcdC8vIFBsdWdpbiBpcyBsb2FkZWQsIHVzZSBpdHMgbm9ybWFsaXplIG1ldGhvZC5cclxuXHRcdFx0XHRcdFx0bm9ybWFsaXplZE5hbWUgPSBwbHVnaW5Nb2R1bGUubm9ybWFsaXplKG5hbWUsIGZ1bmN0aW9uKG5hbWUpIHtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gbm9ybWFsaXplKG5hbWUsIHBhcmVudE5hbWUsIGFwcGx5TWFwKTtcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHQvLyBJZiBuZXN0ZWQgcGx1Z2luIHJlZmVyZW5jZXMsIHRoZW4gZG8gbm90IHRyeSB0b1xyXG5cdFx0XHRcdFx0XHQvLyBub3JtYWxpemUsIGFzIGl0IHdpbGwgbm90IG5vcm1hbGl6ZSBjb3JyZWN0bHkuIFRoaXNcclxuXHRcdFx0XHRcdFx0Ly8gcGxhY2VzIGEgcmVzdHJpY3Rpb24gb24gcmVzb3VyY2VJZHMsIGFuZCB0aGUgbG9uZ2VyXHJcblx0XHRcdFx0XHRcdC8vIHRlcm0gc29sdXRpb24gaXMgbm90IHRvIG5vcm1hbGl6ZSB1bnRpbCBwbHVnaW5zIGFyZVxyXG5cdFx0XHRcdFx0XHQvLyBsb2FkZWQgYW5kIGFsbCBub3JtYWxpemF0aW9ucyB0byBhbGxvdyBmb3IgYXN5bmNcclxuXHRcdFx0XHRcdFx0Ly8gbG9hZGluZyBvZiBhIGxvYWRlciBwbHVnaW4uIEJ1dCBmb3Igbm93LCBmaXhlcyB0aGVcclxuXHRcdFx0XHRcdFx0Ly8gY29tbW9uIHVzZXMuIERldGFpbHMgaW4gIzExMzFcclxuXHRcdFx0XHRcdFx0bm9ybWFsaXplZE5hbWUgPSBuYW1lLmluZGV4T2YoJyEnKSA9PT0gLTEgP1xyXG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgIG5vcm1hbGl6ZShuYW1lLCBwYXJlbnROYW1lLCBhcHBseU1hcCkgOlxyXG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgIG5hbWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdC8vIEEgcmVndWxhciBtb2R1bGUuXHJcblx0XHRcdFx0XHRub3JtYWxpemVkTmFtZSA9IG5vcm1hbGl6ZShuYW1lLCBwYXJlbnROYW1lLCBhcHBseU1hcCk7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdC8vIE5vcm1hbGl6ZWQgbmFtZSBtYXkgYmUgYSBwbHVnaW4gSUQgZHVlIHRvIG1hcCBjb25maWdcclxuXHRcdFx0XHRcdC8vIGFwcGxpY2F0aW9uIGluIG5vcm1hbGl6ZS4gVGhlIG1hcCBjb25maWcgdmFsdWVzIG11c3RcclxuXHRcdFx0XHRcdC8vIGFscmVhZHkgYmUgbm9ybWFsaXplZCwgc28gZG8gbm90IG5lZWQgdG8gcmVkbyB0aGF0IHBhcnQuXHJcblx0XHRcdFx0XHRuYW1lUGFydHMgPSBzcGxpdFByZWZpeChub3JtYWxpemVkTmFtZSk7XHJcblx0XHRcdFx0XHRwcmVmaXggPSBuYW1lUGFydHNbMF07XHJcblx0XHRcdFx0XHRub3JtYWxpemVkTmFtZSA9IG5hbWVQYXJ0c1sxXTtcclxuXHRcdFx0XHRcdGlzTm9ybWFsaXplZCA9IHRydWU7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdHVybCA9IGNvbnRleHQubmFtZVRvVXJsKG5vcm1hbGl6ZWROYW1lKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdC8vIElmIHRoZSBpZCBpcyBhIHBsdWdpbiBpZCB0aGF0IGNhbm5vdCBiZSBkZXRlcm1pbmVkIGlmIGl0IG5lZWRzXHJcblx0XHRcdC8vIG5vcm1hbGl6YXRpb24sIHN0YW1wIGl0IHdpdGggYSB1bmlxdWUgSUQgc28gdHdvIG1hdGNoaW5nIHJlbGF0aXZlXHJcblx0XHRcdC8vIGlkcyB0aGF0IG1heSBjb25mbGljdCBjYW4gYmUgc2VwYXJhdGUuXHJcblx0XHRcdHN1ZmZpeCA9IHByZWZpeCAmJiAhcGx1Z2luTW9kdWxlICYmICFpc05vcm1hbGl6ZWQgP1xyXG5cdFx0XHQgICAgICAgICAnX3Vubm9ybWFsaXplZCcgKyAodW5ub3JtYWxpemVkQ291bnRlciArPSAxKSA6XHJcblx0XHRcdCAgICAgICAgICcnO1xyXG5cdFx0XHRcclxuXHRcdFx0cmV0dXJuIHtcclxuXHRcdFx0XHRwcmVmaXg6IHByZWZpeCxcclxuXHRcdFx0XHRuYW1lOiBub3JtYWxpemVkTmFtZSxcclxuXHRcdFx0XHRwYXJlbnRNYXA6IHBhcmVudE1vZHVsZU1hcCxcclxuXHRcdFx0XHR1bm5vcm1hbGl6ZWQ6ICEhc3VmZml4LFxyXG5cdFx0XHRcdHVybDogdXJsLFxyXG5cdFx0XHRcdG9yaWdpbmFsTmFtZTogb3JpZ2luYWxOYW1lLFxyXG5cdFx0XHRcdGlzRGVmaW5lOiBpc0RlZmluZSxcclxuXHRcdFx0XHRpZDogKHByZWZpeCA/XHJcblx0XHRcdFx0ICAgICBwcmVmaXggKyAnIScgKyBub3JtYWxpemVkTmFtZSA6XHJcblx0XHRcdFx0ICAgICBub3JtYWxpemVkTmFtZSkgKyBzdWZmaXhcclxuXHRcdFx0fTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlKGRlcE1hcCkge1xyXG5cdFx0XHRsZXQgaWQgPSBkZXBNYXAuaWQsXHJcblx0XHRcdFx0bW9kID0gZ2V0T3duKHJlZ2lzdHJ5LCBpZCk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoIW1vZCkge1xyXG5cdFx0XHRcdG1vZCA9IHJlZ2lzdHJ5W2lkXSA9IG5ldyBjb250ZXh0Lk1vZHVsZShkZXBNYXApO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRyZXR1cm4gbW9kO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRmdW5jdGlvbiBvbihkZXBNYXAsIG5hbWUsIGZuKSB7XHJcblx0XHRcdGxldCBpZCA9IGRlcE1hcC5pZCxcclxuXHRcdFx0XHRtb2QgPSBnZXRPd24ocmVnaXN0cnksIGlkKTtcclxuXHRcdFx0XHJcblx0XHRcdGlmIChoYXNQcm9wKGRlZmluZWQsIGlkKSAmJlxyXG5cdFx0XHRcdCghbW9kIHx8IG1vZC5kZWZpbmVFbWl0Q29tcGxldGUpKSB7XHJcblx0XHRcdFx0aWYgKG5hbWUgPT09ICdkZWZpbmVkJykge1xyXG5cdFx0XHRcdFx0Zm4oZGVmaW5lZFtpZF0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRtb2QgPSBnZXRNb2R1bGUoZGVwTWFwKTtcclxuXHRcdFx0XHRpZiAobW9kLmVycm9yICYmIG5hbWUgPT09ICdlcnJvcicpIHtcclxuXHRcdFx0XHRcdGZuKG1vZC5lcnJvcik7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdG1vZC5vbihuYW1lLCBmbik7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdGZ1bmN0aW9uIG9uRXJyb3IoZXJyLCBlcnJiYWNrKSB7XHJcblx0XHRcdGxldCBpZHMgPSBlcnIucmVxdWlyZU1vZHVsZXMsXHJcblx0XHRcdFx0bm90aWZpZWQgPSBmYWxzZTtcclxuXHRcdFx0XHJcblx0XHRcdGlmIChlcnJiYWNrKSB7XHJcblx0XHRcdFx0ZXJyYmFjayhlcnIpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGVhY2goaWRzLCBmdW5jdGlvbihpZCkge1xyXG5cdFx0XHRcdFx0bGV0IG1vZCA9IGdldE93bihyZWdpc3RyeSwgaWQpO1xyXG5cdFx0XHRcdFx0aWYgKG1vZCkge1xyXG5cdFx0XHRcdFx0XHQvLyBTZXQgZXJyb3Igb24gbW9kdWxlLCBzbyBpdCBza2lwcyB0aW1lb3V0IGNoZWNrcy5cclxuXHRcdFx0XHRcdFx0bW9kLmVycm9yID0gZXJyO1xyXG5cdFx0XHRcdFx0XHRpZiAobW9kLmV2ZW50cy5lcnJvcikge1xyXG5cdFx0XHRcdFx0XHRcdG5vdGlmaWVkID0gdHJ1ZTtcclxuXHRcdFx0XHRcdFx0XHRtb2QuZW1pdCgnZXJyb3InLCBlcnIpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKCFub3RpZmllZCkge1xyXG5cdFx0XHRcdFx0cmVxLm9uRXJyb3IoZXJyKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBJbnRlcm5hbCBtZXRob2QgdG8gdHJhbnNmZXIgZ2xvYmFsUXVldWUgaXRlbXMgdG8gdGhpcyBjb250ZXh0J3NcclxuXHRcdCAqIGRlZlF1ZXVlLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiB0YWtlR2xvYmFsUXVldWUoKSB7XHJcblx0XHRcdC8vIFB1c2ggYWxsIHRoZSBnbG9iYWxEZWZRdWV1ZSBpdGVtcyBpbnRvIHRoZSBjb250ZXh0J3MgZGVmUXVldWVcclxuXHRcdFx0aWYgKGdsb2JhbERlZlF1ZXVlLmxlbmd0aCkge1xyXG5cdFx0XHRcdGVhY2goZ2xvYmFsRGVmUXVldWUsIGZ1bmN0aW9uKHF1ZXVlSXRlbSkge1xyXG5cdFx0XHRcdFx0bGV0IGlkID0gcXVldWVJdGVtWzBdO1xyXG5cdFx0XHRcdFx0aWYgKHR5cGVvZiBpZCA9PT0gJ3N0cmluZycpIHtcclxuXHRcdFx0XHRcdFx0Y29udGV4dC5kZWZRdWV1ZU1hcFtpZF0gPSB0cnVlO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0ZGVmUXVldWUucHVzaChxdWV1ZUl0ZW0pO1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdGdsb2JhbERlZlF1ZXVlID0gW107XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0aGFuZGxlcnMgPSB7XHJcblx0XHRcdCdyZXF1aXJlJzogZnVuY3Rpb24obW9kKSB7XHJcblx0XHRcdFx0aWYgKG1vZC5yZXF1aXJlKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gbW9kLnJlcXVpcmU7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHJldHVybiAobW9kLnJlcXVpcmUgPSBjb250ZXh0Lm1ha2VSZXF1aXJlKG1vZC5tYXApKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sXHJcblx0XHRcdCdleHBvcnRzJzogZnVuY3Rpb24obW9kKSB7XHJcblx0XHRcdFx0bW9kLnVzaW5nRXhwb3J0cyA9IHRydWU7XHJcblx0XHRcdFx0aWYgKG1vZC5tYXAuaXNEZWZpbmUpIHtcclxuXHRcdFx0XHRcdGlmIChtb2QuZXhwb3J0cykge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gKGRlZmluZWRbbW9kLm1hcC5pZF0gPSBtb2QuZXhwb3J0cyk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gKG1vZC5leHBvcnRzID0gZGVmaW5lZFttb2QubWFwLmlkXSA9IHt9KTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sXHJcblx0XHRcdCdtb2R1bGUnOiBmdW5jdGlvbihtb2QpIHtcclxuXHRcdFx0XHRpZiAobW9kLm1vZHVsZSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIG1vZC5tb2R1bGU7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHJldHVybiAobW9kLm1vZHVsZSA9IHtcclxuXHRcdFx0XHRcdFx0aWQ6IG1vZC5tYXAuaWQsXHJcblx0XHRcdFx0XHRcdHVyaTogbW9kLm1hcC51cmwsXHJcblx0XHRcdFx0XHRcdGNvbmZpZzogZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRcdFx0cmV0dXJuIGdldE93bihjb25maWcuY29uZmlnLCBtb2QubWFwLmlkKSB8fCB7fTtcclxuXHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0ZXhwb3J0czogbW9kLmV4cG9ydHMgfHwgKG1vZC5leHBvcnRzID0ge30pXHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblx0XHRcclxuXHRcdGZ1bmN0aW9uIGNsZWFuUmVnaXN0cnkoaWQpIHtcclxuXHRcdFx0Ly8gQ2xlYW4gdXAgbWFjaGluZXJ5IHVzZWQgZm9yIHdhaXRpbmcgbW9kdWxlcy5cclxuXHRcdFx0ZGVsZXRlIHJlZ2lzdHJ5W2lkXTtcclxuXHRcdFx0ZGVsZXRlIGVuYWJsZWRSZWdpc3RyeVtpZF07XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdGZ1bmN0aW9uIGJyZWFrQ3ljbGUobW9kLCB0cmFjZWQsIHByb2Nlc3NlZCkge1xyXG5cdFx0XHRsZXQgaWQgPSBtb2QubWFwLmlkO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKG1vZC5lcnJvcikge1xyXG5cdFx0XHRcdG1vZC5lbWl0KCdlcnJvcicsIG1vZC5lcnJvcik7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dHJhY2VkW2lkXSA9IHRydWU7XHJcblx0XHRcdFx0ZWFjaChtb2QuZGVwTWFwcywgZnVuY3Rpb24oZGVwTWFwLCBpKSB7XHJcblx0XHRcdFx0XHRsZXQgZGVwSWQgPSBkZXBNYXAuaWQsXHJcblx0XHRcdFx0XHRcdGRlcCA9IGdldE93bihyZWdpc3RyeSwgZGVwSWQpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBPbmx5IGZvcmNlIHRoaW5ncyB0aGF0IGhhdmUgbm90IGNvbXBsZXRlZFxyXG5cdFx0XHRcdFx0Ly8gYmVpbmcgZGVmaW5lZCwgc28gc3RpbGwgaW4gdGhlIHJlZ2lzdHJ5LFxyXG5cdFx0XHRcdFx0Ly8gYW5kIG9ubHkgaWYgaXQgaGFzIG5vdCBiZWVuIG1hdGNoZWQgdXBcclxuXHRcdFx0XHRcdC8vIGluIHRoZSBtb2R1bGUgYWxyZWFkeS5cclxuXHRcdFx0XHRcdGlmIChkZXAgJiYgIW1vZC5kZXBNYXRjaGVkW2ldICYmICFwcm9jZXNzZWRbZGVwSWRdKSB7XHJcblx0XHRcdFx0XHRcdGlmIChnZXRPd24odHJhY2VkLCBkZXBJZCkpIHtcclxuXHRcdFx0XHRcdFx0XHRtb2QuZGVmaW5lRGVwKGksIGRlZmluZWRbZGVwSWRdKTtcclxuXHRcdFx0XHRcdFx0XHRtb2QuY2hlY2soKTsgLy8gcGFzcyBmYWxzZT9cclxuXHRcdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0XHRicmVha0N5Y2xlKGRlcCwgdHJhY2VkLCBwcm9jZXNzZWQpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0cHJvY2Vzc2VkW2lkXSA9IHRydWU7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0ZnVuY3Rpb24gY2hlY2tMb2FkZWQoKSB7XHJcblx0XHRcdGxldCBlcnIsIHVzaW5nUGF0aEZhbGxiYWNrLFxyXG5cdFx0XHRcdHdhaXRJbnRlcnZhbCA9IGNvbmZpZy53YWl0U2Vjb25kcyAqIDEwMDAsXHJcblx0XHRcdFx0Ly8gSXQgaXMgcG9zc2libGUgdG8gZGlzYWJsZSB0aGUgd2FpdCBpbnRlcnZhbCBieSB1c2luZyB3YWl0U2Vjb25kcyBvZiAwLlxyXG5cdFx0XHRcdGV4cGlyZWQgPSB3YWl0SW50ZXJ2YWwgJiYgKGNvbnRleHQuc3RhcnRUaW1lICsgd2FpdEludGVydmFsKSA8IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxyXG5cdFx0XHRcdG5vTG9hZHMgPSBbXSxcclxuXHRcdFx0XHRyZXFDYWxscyA9IFtdLFxyXG5cdFx0XHRcdHN0aWxsTG9hZGluZyA9IGZhbHNlLFxyXG5cdFx0XHRcdG5lZWRDeWNsZUNoZWNrID0gdHJ1ZTtcclxuXHRcdFx0XHJcblx0XHRcdC8vIERvIG5vdCBib3RoZXIgaWYgdGhpcyBjYWxsIHdhcyBhIHJlc3VsdCBvZiBhIGN5Y2xlIGJyZWFrLlxyXG5cdFx0XHRpZiAoaW5DaGVja0xvYWRlZCkge1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0aW5DaGVja0xvYWRlZCA9IHRydWU7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBGaWd1cmUgb3V0IHRoZSBzdGF0ZSBvZiBhbGwgdGhlIG1vZHVsZXMuXHJcblx0XHRcdGVhY2hQcm9wKGVuYWJsZWRSZWdpc3RyeSwgZnVuY3Rpb24obW9kKSB7XHJcblx0XHRcdFx0bGV0IG1hcCA9IG1vZC5tYXAsXHJcblx0XHRcdFx0XHRtb2RJZCA9IG1hcC5pZDtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBTa2lwIHRoaW5ncyB0aGF0IGFyZSBub3QgZW5hYmxlZCBvciBpbiBlcnJvciBzdGF0ZS5cclxuXHRcdFx0XHRpZiAoIW1vZC5lbmFibGVkKSB7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmICghbWFwLmlzRGVmaW5lKSB7XHJcblx0XHRcdFx0XHRyZXFDYWxscy5wdXNoKG1vZCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmICghbW9kLmVycm9yKSB7XHJcblx0XHRcdFx0XHQvLyBJZiB0aGUgbW9kdWxlIHNob3VsZCBiZSBleGVjdXRlZCwgYW5kIGl0IGhhcyBub3RcclxuXHRcdFx0XHRcdC8vIGJlZW4gaW5pdGVkIGFuZCB0aW1lIGlzIHVwLCByZW1lbWJlciBpdC5cclxuXHRcdFx0XHRcdGlmICghbW9kLmluaXRlZCAmJiBleHBpcmVkKSB7XHJcblx0XHRcdFx0XHRcdGlmIChoYXNQYXRoRmFsbGJhY2sobW9kSWQpKSB7XHJcblx0XHRcdFx0XHRcdFx0dXNpbmdQYXRoRmFsbGJhY2sgPSB0cnVlO1xyXG5cdFx0XHRcdFx0XHRcdHN0aWxsTG9hZGluZyA9IHRydWU7XHJcblx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0bm9Mb2Fkcy5wdXNoKG1vZElkKTtcclxuXHRcdFx0XHRcdFx0XHRyZW1vdmVTY3JpcHQobW9kSWQpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKCFtb2QuaW5pdGVkICYmIG1vZC5mZXRjaGVkICYmIG1hcC5pc0RlZmluZSkge1xyXG5cdFx0XHRcdFx0XHRzdGlsbExvYWRpbmcgPSB0cnVlO1xyXG5cdFx0XHRcdFx0XHRpZiAoIW1hcC5wcmVmaXgpIHtcclxuXHRcdFx0XHRcdFx0XHQvLyBObyByZWFzb24gdG8ga2VlcCBsb29raW5nIGZvciB1bmZpbmlzaGVkXHJcblx0XHRcdFx0XHRcdFx0Ly8gbG9hZGluZy4gSWYgdGhlIG9ubHkgc3RpbGxMb2FkaW5nIGlzIGFcclxuXHRcdFx0XHRcdFx0XHQvLyBwbHVnaW4gcmVzb3VyY2UgdGhvdWdoLCBrZWVwIGdvaW5nLFxyXG5cdFx0XHRcdFx0XHRcdC8vIGJlY2F1c2UgaXQgbWF5IGJlIHRoYXQgYSBwbHVnaW4gcmVzb3VyY2VcclxuXHRcdFx0XHRcdFx0XHQvLyBpcyB3YWl0aW5nIG9uIGEgbm9uLXBsdWdpbiBjeWNsZS5cclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gKG5lZWRDeWNsZUNoZWNrID0gZmFsc2UpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0XHJcblx0XHRcdGlmIChleHBpcmVkICYmIG5vTG9hZHMubGVuZ3RoKSB7XHJcblx0XHRcdFx0Ly8gSWYgd2FpdCB0aW1lIGV4cGlyZWQsIHRocm93IGVycm9yIG9mIHVubG9hZGVkIG1vZHVsZXMuXHJcblx0XHRcdFx0ZXJyID0gbWFrZUVycm9yKCd0aW1lb3V0JywgJ0xvYWQgdGltZW91dCBmb3IgbW9kdWxlczogJyArIG5vTG9hZHMsIG51bGwsIG5vTG9hZHMpO1xyXG5cdFx0XHRcdGVyci5jb250ZXh0TmFtZSA9IGNvbnRleHQuY29udGV4dE5hbWU7XHJcblx0XHRcdFx0cmV0dXJuIG9uRXJyb3IoZXJyKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0Ly8gTm90IGV4cGlyZWQsIGNoZWNrIGZvciBhIGN5Y2xlLlxyXG5cdFx0XHRpZiAobmVlZEN5Y2xlQ2hlY2spIHtcclxuXHRcdFx0XHRlYWNoKHJlcUNhbGxzLCBmdW5jdGlvbihtb2QpIHtcclxuXHRcdFx0XHRcdGJyZWFrQ3ljbGUobW9kLCB7fSwge30pO1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBJZiBzdGlsbCB3YWl0aW5nIG9uIGxvYWRzLCBhbmQgdGhlIHdhaXRpbmcgbG9hZCBpcyBzb21ldGhpbmdcclxuXHRcdFx0Ly8gb3RoZXIgdGhhbiBhIHBsdWdpbiByZXNvdXJjZSwgb3IgdGhlcmUgYXJlIHN0aWxsIG91dHN0YW5kaW5nXHJcblx0XHRcdC8vIHNjcmlwdHMsIHRoZW4ganVzdCB0cnkgYmFjayBsYXRlci5cclxuXHRcdFx0aWYgKCghZXhwaXJlZCB8fCB1c2luZ1BhdGhGYWxsYmFjaykgJiYgc3RpbGxMb2FkaW5nKSB7XHJcblx0XHRcdFx0Ly8gU29tZXRoaW5nIGlzIHN0aWxsIHdhaXRpbmcgdG8gbG9hZC4gV2FpdCBmb3IgaXQsIGJ1dCBvbmx5XHJcblx0XHRcdFx0Ly8gaWYgYSB0aW1lb3V0IGlzIG5vdCBhbHJlYWR5IGluIGVmZmVjdC5cclxuXHRcdFx0XHRpZiAoKGlzQnJvd3NlciB8fCBpc1dlYldvcmtlcikgJiYgIWNoZWNrTG9hZGVkVGltZW91dElkKSB7XHJcblx0XHRcdFx0XHRjaGVja0xvYWRlZFRpbWVvdXRJZCA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRcdGNoZWNrTG9hZGVkVGltZW91dElkID0gMDtcclxuXHRcdFx0XHRcdFx0Y2hlY2tMb2FkZWQoKTtcclxuXHRcdFx0XHRcdH0sIDUwKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdGluQ2hlY2tMb2FkZWQgPSBmYWxzZTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0TW9kdWxlID0gZnVuY3Rpb24obWFwKSB7XHJcblx0XHRcdHRoaXMuZXZlbnRzID0gZ2V0T3duKHVuZGVmRXZlbnRzLCBtYXAuaWQpIHx8IHt9O1xyXG5cdFx0XHR0aGlzLm1hcCA9IG1hcDtcclxuXHRcdFx0dGhpcy5zaGltID0gZ2V0T3duKGNvbmZpZy5zaGltLCBtYXAuaWQpO1xyXG5cdFx0XHR0aGlzLmRlcEV4cG9ydHMgPSBbXTtcclxuXHRcdFx0dGhpcy5kZXBNYXBzID0gW107XHJcblx0XHRcdHRoaXMuZGVwTWF0Y2hlZCA9IFtdO1xyXG5cdFx0XHR0aGlzLnBsdWdpbk1hcHMgPSB7fTtcclxuXHRcdFx0dGhpcy5kZXBDb3VudCA9IDA7XHJcblx0XHRcdFxyXG5cdFx0XHQvKiB0aGlzLmV4cG9ydHMgdGhpcy5mYWN0b3J5XHJcblx0XHRcdCB0aGlzLmRlcE1hcHMgPSBbXSxcclxuXHRcdFx0IHRoaXMuZW5hYmxlZCwgdGhpcy5mZXRjaGVkXHJcblx0XHRcdCAqL1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0TW9kdWxlLnByb3RvdHlwZSA9IHtcclxuXHRcdFx0aW5pdDogZnVuY3Rpb24oZGVwTWFwcywgZmFjdG9yeSwgZXJyYmFjaywgb3B0aW9ucykge1xyXG5cdFx0XHRcdG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIERvIG5vdCBkbyBtb3JlIGluaXRzIGlmIGFscmVhZHkgZG9uZS4gQ2FuIGhhcHBlbiBpZiB0aGVyZVxyXG5cdFx0XHRcdC8vIGFyZSBtdWx0aXBsZSBkZWZpbmUgY2FsbHMgZm9yIHRoZSBzYW1lIG1vZHVsZS4gVGhhdCBpcyBub3RcclxuXHRcdFx0XHQvLyBhIG5vcm1hbCwgY29tbW9uIGNhc2UsIGJ1dCBpdCBpcyBhbHNvIG5vdCB1bmV4cGVjdGVkLlxyXG5cdFx0XHRcdGlmICh0aGlzLmluaXRlZCkge1xyXG5cdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHR0aGlzLmZhY3RvcnkgPSBmYWN0b3J5O1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChlcnJiYWNrKSB7XHJcblx0XHRcdFx0XHQvLyBSZWdpc3RlciBmb3IgZXJyb3JzIG9uIHRoaXMgbW9kdWxlLlxyXG5cdFx0XHRcdFx0dGhpcy5vbignZXJyb3InLCBlcnJiYWNrKTtcclxuXHRcdFx0XHR9IGVsc2UgaWYgKHRoaXMuZXZlbnRzLmVycm9yKSB7XHJcblx0XHRcdFx0XHQvLyBJZiBubyBlcnJiYWNrIGFscmVhZHksIGJ1dCB0aGVyZSBhcmUgZXJyb3IgbGlzdGVuZXJzXHJcblx0XHRcdFx0XHQvLyBvbiB0aGlzIG1vZHVsZSwgc2V0IHVwIGFuIGVycmJhY2sgdG8gcGFzcyB0byB0aGUgZGVwcy5cclxuXHRcdFx0XHRcdGVycmJhY2sgPSBiaW5kKHRoaXMsIGZ1bmN0aW9uKGVycikge1xyXG5cdFx0XHRcdFx0XHR0aGlzLmVtaXQoJ2Vycm9yJywgZXJyKTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBEbyBhIGNvcHkgb2YgdGhlIGRlcGVuZGVuY3kgYXJyYXksIHNvIHRoYXRcclxuXHRcdFx0XHQvLyBzb3VyY2UgaW5wdXRzIGFyZSBub3QgbW9kaWZpZWQuIEZvciBleGFtcGxlXHJcblx0XHRcdFx0Ly8gXCJzaGltXCIgZGVwcyBhcmUgcGFzc2VkIGluIGhlcmUgZGlyZWN0bHksIGFuZFxyXG5cdFx0XHRcdC8vIGRvaW5nIGEgZGlyZWN0IG1vZGlmaWNhdGlvbiBvZiB0aGUgZGVwTWFwcyBhcnJheVxyXG5cdFx0XHRcdC8vIHdvdWxkIGFmZmVjdCB0aGF0IGNvbmZpZy5cclxuXHRcdFx0XHR0aGlzLmRlcE1hcHMgPSBkZXBNYXBzICYmIGRlcE1hcHMuc2xpY2UoMCk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0dGhpcy5lcnJiYWNrID0gZXJyYmFjaztcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBJbmRpY2F0ZSB0aGlzIG1vZHVsZSBoYXMgYmUgaW5pdGlhbGl6ZWRcclxuXHRcdFx0XHR0aGlzLmluaXRlZCA9IHRydWU7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0dGhpcy5pZ25vcmUgPSBvcHRpb25zLmlnbm9yZTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBDb3VsZCBoYXZlIG9wdGlvbiB0byBpbml0IHRoaXMgbW9kdWxlIGluIGVuYWJsZWQgbW9kZSxcclxuXHRcdFx0XHQvLyBvciBjb3VsZCBoYXZlIGJlZW4gcHJldmlvdXNseSBtYXJrZWQgYXMgZW5hYmxlZC4gSG93ZXZlcixcclxuXHRcdFx0XHQvLyB0aGUgZGVwZW5kZW5jaWVzIGFyZSBub3Qga25vd24gdW50aWwgaW5pdCBpcyBjYWxsZWQuIFNvXHJcblx0XHRcdFx0Ly8gaWYgZW5hYmxlZCBwcmV2aW91c2x5LCBub3cgdHJpZ2dlciBkZXBlbmRlbmNpZXMgYXMgZW5hYmxlZC5cclxuXHRcdFx0XHRpZiAob3B0aW9ucy5lbmFibGVkIHx8IHRoaXMuZW5hYmxlZCkge1xyXG5cdFx0XHRcdFx0Ly8gRW5hYmxlIHRoaXMgbW9kdWxlIGFuZCBkZXBlbmRlbmNpZXMuXHJcblx0XHRcdFx0XHQvLyBXaWxsIGNhbGwgdGhpcy5jaGVjaygpXHJcblx0XHRcdFx0XHR0aGlzLmVuYWJsZSgpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLmNoZWNrKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LFxyXG5cdFx0XHRcclxuXHRcdFx0ZGVmaW5lRGVwOiBmdW5jdGlvbihpLCBkZXBFeHBvcnRzKSB7XHJcblx0XHRcdFx0Ly8gQmVjYXVzZSBvZiBjeWNsZXMsIGRlZmluZWQgY2FsbGJhY2sgZm9yIGEgZ2l2ZW5cclxuXHRcdFx0XHQvLyBleHBvcnQgY2FuIGJlIGNhbGxlZCBtb3JlIHRoYW4gb25jZS5cclxuXHRcdFx0XHRpZiAoIXRoaXMuZGVwTWF0Y2hlZFtpXSkge1xyXG5cdFx0XHRcdFx0dGhpcy5kZXBNYXRjaGVkW2ldID0gdHJ1ZTtcclxuXHRcdFx0XHRcdHRoaXMuZGVwQ291bnQgLT0gMTtcclxuXHRcdFx0XHRcdHRoaXMuZGVwRXhwb3J0c1tpXSA9IGRlcEV4cG9ydHM7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LFxyXG5cdFx0XHRcclxuXHRcdFx0ZmV0Y2g6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGlmICh0aGlzLmZldGNoZWQpIHtcclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0dGhpcy5mZXRjaGVkID0gdHJ1ZTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRjb250ZXh0LnN0YXJ0VGltZSA9IChuZXcgRGF0ZSgpKS5nZXRUaW1lKCk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0bGV0IG1hcCA9IHRoaXMubWFwO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIElmIHRoZSBtYW5hZ2VyIGlzIGZvciBhIHBsdWdpbiBtYW5hZ2VkIHJlc291cmNlLFxyXG5cdFx0XHRcdC8vIGFzayB0aGUgcGx1Z2luIHRvIGxvYWQgaXQgbm93LlxyXG5cdFx0XHRcdGlmICh0aGlzLnNoaW0pIHtcclxuXHRcdFx0XHRcdGNvbnRleHQubWFrZVJlcXVpcmUodGhpcy5tYXAsIHtcclxuXHRcdFx0XHRcdFx0ZW5hYmxlQnVpbGRDYWxsYmFjazogdHJ1ZVxyXG5cdFx0XHRcdFx0fSkodGhpcy5zaGltLmRlcHMgfHwgW10sIGJpbmQodGhpcywgZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiBtYXAucHJlZml4ID8gdGhpcy5jYWxsUGx1Z2luKCkgOiB0aGlzLmxvYWQoKTtcclxuXHRcdFx0XHRcdH0pKTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0Ly8gUmVndWxhciBkZXBlbmRlbmN5LlxyXG5cdFx0XHRcdFx0cmV0dXJuIG1hcC5wcmVmaXggPyB0aGlzLmNhbGxQbHVnaW4oKSA6IHRoaXMubG9hZCgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdGxvYWQ6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGxldCB1cmwgPSB0aGlzLm1hcC51cmw7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gUmVndWxhciBkZXBlbmRlbmN5LlxyXG5cdFx0XHRcdGlmICghdXJsRmV0Y2hlZFt1cmxdKSB7XHJcblx0XHRcdFx0XHR1cmxGZXRjaGVkW3VybF0gPSB0cnVlO1xyXG5cdFx0XHRcdFx0Y29udGV4dC5sb2FkKHRoaXMubWFwLmlkLCB1cmwpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdC8qKlxyXG5cdFx0XHQgKiBDaGVja3MgaWYgdGhlIG1vZHVsZSBpcyByZWFkeSB0byBkZWZpbmUgaXRzZWxmLCBhbmQgaWYgc28sXHJcblx0XHRcdCAqIGRlZmluZSBpdC5cclxuXHRcdFx0ICovXHJcblx0XHRcdGNoZWNrOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRpZiAoIXRoaXMuZW5hYmxlZCB8fCB0aGlzLmVuYWJsaW5nKSB7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGxldCBlcnIsIGNqc01vZHVsZSxcclxuXHRcdFx0XHRcdGlkID0gdGhpcy5tYXAuaWQsXHJcblx0XHRcdFx0XHRkZXBFeHBvcnRzID0gdGhpcy5kZXBFeHBvcnRzLFxyXG5cdFx0XHRcdFx0ZXhwb3J0cyA9IHRoaXMuZXhwb3J0cyxcclxuXHRcdFx0XHRcdGZhY3RvcnkgPSB0aGlzLmZhY3Rvcnk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKCF0aGlzLmluaXRlZCkge1xyXG5cdFx0XHRcdFx0Ly8gT25seSBmZXRjaCBpZiBub3QgYWxyZWFkeSBpbiB0aGUgZGVmUXVldWUuXHJcblx0XHRcdFx0XHRpZiAoIWhhc1Byb3AoY29udGV4dC5kZWZRdWV1ZU1hcCwgaWQpKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuZmV0Y2goKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9IGVsc2UgaWYgKHRoaXMuZXJyb3IpIHtcclxuXHRcdFx0XHRcdHRoaXMuZW1pdCgnZXJyb3InLCB0aGlzLmVycm9yKTtcclxuXHRcdFx0XHR9IGVsc2UgaWYgKCF0aGlzLmRlZmluaW5nKSB7XHJcblx0XHRcdFx0XHQvLyBUaGUgZmFjdG9yeSBjb3VsZCB0cmlnZ2VyIGFub3RoZXIgcmVxdWlyZSBjYWxsXHJcblx0XHRcdFx0XHQvLyB0aGF0IHdvdWxkIHJlc3VsdCBpbiBjaGVja2luZyB0aGlzIG1vZHVsZSB0b1xyXG5cdFx0XHRcdFx0Ly8gZGVmaW5lIGl0c2VsZiBhZ2Fpbi4gSWYgYWxyZWFkeSBpbiB0aGUgcHJvY2Vzc1xyXG5cdFx0XHRcdFx0Ly8gb2YgZG9pbmcgdGhhdCwgc2tpcCB0aGlzIHdvcmsuXHJcblx0XHRcdFx0XHR0aGlzLmRlZmluaW5nID0gdHJ1ZTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0aWYgKHRoaXMuZGVwQ291bnQgPCAxICYmICF0aGlzLmRlZmluZWQpIHtcclxuXHRcdFx0XHRcdFx0aWYgKGlzRnVuY3Rpb24oZmFjdG9yeSkpIHtcclxuXHRcdFx0XHRcdFx0XHR0cnkge1xyXG5cdFx0XHRcdFx0XHRcdFx0ZXhwb3J0cyA9IGNvbnRleHQuZXhlY0NiKGlkLCBmYWN0b3J5LCBkZXBFeHBvcnRzLCBleHBvcnRzKTtcclxuXHRcdFx0XHRcdFx0XHR9IGNhdGNoIChlKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRlcnIgPSBlO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHQvLyBGYXZvciByZXR1cm4gdmFsdWUgb3ZlciBleHBvcnRzLiBJZiBub2RlL2NqcyBpbiBwbGF5LFxyXG5cdFx0XHRcdFx0XHRcdC8vIHRoZW4gd2lsbCBub3QgaGF2ZSBhIHJldHVybiB2YWx1ZSBhbnl3YXkuIEZhdm9yXHJcblx0XHRcdFx0XHRcdFx0Ly8gbW9kdWxlLmV4cG9ydHMgYXNzaWdubWVudCBvdmVyIGV4cG9ydHMgb2JqZWN0LlxyXG5cdFx0XHRcdFx0XHRcdGlmICh0aGlzLm1hcC5pc0RlZmluZSAmJiBleHBvcnRzID09PSB1bmRlZmluZWQpIHtcclxuXHRcdFx0XHRcdFx0XHRcdGNqc01vZHVsZSA9IHRoaXMubW9kdWxlO1xyXG5cdFx0XHRcdFx0XHRcdFx0aWYgKGNqc01vZHVsZSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRleHBvcnRzID0gY2pzTW9kdWxlLmV4cG9ydHM7XHJcblx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHRoaXMudXNpbmdFeHBvcnRzKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdC8vIGV4cG9ydHMgYWxyZWFkeSBzZXQgdGhlIGRlZmluZWQgdmFsdWUuXHJcblx0XHRcdFx0XHRcdFx0XHRcdGV4cG9ydHMgPSB0aGlzLmV4cG9ydHM7XHJcblx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRcdGlmIChlcnIpIHtcclxuXHRcdFx0XHRcdFx0XHRcdC8vIElmIHRoZXJlIGlzIGFuIGVycm9yIGxpc3RlbmVyLCBmYXZvciBwYXNzaW5nXHJcblx0XHRcdFx0XHRcdFx0XHQvLyB0byB0aGF0IGluc3RlYWQgb2YgdGhyb3dpbmcgYW4gZXJyb3IuIEhvd2V2ZXIsXHJcblx0XHRcdFx0XHRcdFx0XHQvLyBvbmx5IGRvIGl0IGZvciBkZWZpbmUoKSdkICBtb2R1bGVzLiByZXF1aXJlXHJcblx0XHRcdFx0XHRcdFx0XHQvLyBlcnJiYWNrcyBzaG91bGQgbm90IGJlIGNhbGxlZCBmb3IgZmFpbHVyZXMgaW5cclxuXHRcdFx0XHRcdFx0XHRcdC8vIHRoZWlyIGNhbGxiYWNrcyAoIzY5OSkuIEhvd2V2ZXIgaWYgYSBnbG9iYWxcclxuXHRcdFx0XHRcdFx0XHRcdC8vIG9uRXJyb3IgaXMgc2V0LCB1c2UgdGhhdC5cclxuXHRcdFx0XHRcdFx0XHRcdGlmICgodGhpcy5ldmVudHMuZXJyb3IgJiYgdGhpcy5tYXAuaXNEZWZpbmUpIHx8XHJcblx0XHRcdFx0XHRcdFx0XHRcdHJlcS5vbkVycm9yICE9PSBkZWZhdWx0T25FcnJvcikge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRlcnIucmVxdWlyZU1hcCA9IHRoaXMubWFwO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRlcnIucmVxdWlyZU1vZHVsZXMgPSB0aGlzLm1hcC5pc0RlZmluZSA/IFt0aGlzLm1hcC5pZF0gOiBudWxsO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRlcnIucmVxdWlyZVR5cGUgPSB0aGlzLm1hcC5pc0RlZmluZSA/ICdkZWZpbmUnIDogJ3JlcXVpcmUnO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRyZXR1cm4gb25FcnJvcigodGhpcy5lcnJvciA9IGVycikpO1xyXG5cdFx0XHRcdFx0XHRcdFx0fSBlbHNlIGlmICh0eXBlb2YgY29uc29sZSAhPT0gJ3VuZGVmaW5lZCcgJiZcclxuXHRcdFx0XHRcdFx0XHRcdFx0Y29uc29sZS5lcnJvcikge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQvLyBMb2cgdGhlIGVycm9yIGZvciBkZWJ1Z2dpbmcuIElmIHByb21pc2VzIGNvdWxkIGJlXHJcblx0XHRcdFx0XHRcdFx0XHRcdC8vIHVzZWQsIHRoaXMgd291bGQgYmUgZGlmZmVyZW50LCBidXQgbWFraW5nIGRvLlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRjb25zb2xlLmVycm9yKGVycik7XHJcblx0XHRcdFx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQvLyBEbyBub3Qgd2FudCB0byBjb21wbGV0ZWx5IGxvc2UgdGhlIGVycm9yLiBXaGlsZSB0aGlzXHJcblx0XHRcdFx0XHRcdFx0XHRcdC8vIHdpbGwgbWVzcyB1cCBwcm9jZXNzaW5nIGFuZCBsZWFkIHRvIHNpbWlsYXIgcmVzdWx0c1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQvLyBhcyBidWcgMTQ0MCwgaXQgYXQgbGVhc3Qgc3VyZmFjZXMgdGhlIGVycm9yLlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRyZXEub25FcnJvcihlcnIpO1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0XHQvLyBKdXN0IGEgbGl0ZXJhbCB2YWx1ZVxyXG5cdFx0XHRcdFx0XHRcdGV4cG9ydHMgPSBmYWN0b3J5O1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHR0aGlzLmV4cG9ydHMgPSBleHBvcnRzO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0aWYgKHRoaXMubWFwLmlzRGVmaW5lICYmICF0aGlzLmlnbm9yZSkge1xyXG5cdFx0XHRcdFx0XHRcdGRlZmluZWRbaWRdID0gZXhwb3J0cztcclxuXHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHRpZiAocmVxLm9uUmVzb3VyY2VMb2FkKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRsZXQgcmVzTG9hZE1hcHMgPSBbXTtcclxuXHRcdFx0XHRcdFx0XHRcdGVhY2godGhpcy5kZXBNYXBzLCBmdW5jdGlvbihkZXBNYXApIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0cmVzTG9hZE1hcHMucHVzaChkZXBNYXAubm9ybWFsaXplZE1hcCB8fCBkZXBNYXApO1xyXG5cdFx0XHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRcdFx0XHRyZXEub25SZXNvdXJjZUxvYWQoY29udGV4dCwgdGhpcy5tYXAsIHJlc0xvYWRNYXBzKTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdC8vIENsZWFuIHVwXHJcblx0XHRcdFx0XHRcdGNsZWFuUmVnaXN0cnkoaWQpO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVkID0gdHJ1ZTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0Ly8gRmluaXNoZWQgdGhlIGRlZmluZSBzdGFnZS4gQWxsb3cgY2FsbGluZyBjaGVjayBhZ2FpblxyXG5cdFx0XHRcdFx0Ly8gdG8gYWxsb3cgZGVmaW5lIG5vdGlmaWNhdGlvbnMgYmVsb3cgaW4gdGhlIGNhc2Ugb2YgYVxyXG5cdFx0XHRcdFx0Ly8gY3ljbGUuXHJcblx0XHRcdFx0XHR0aGlzLmRlZmluaW5nID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmICh0aGlzLmRlZmluZWQgJiYgIXRoaXMuZGVmaW5lRW1pdHRlZCkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZUVtaXR0ZWQgPSB0cnVlO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmVtaXQoJ2RlZmluZWQnLCB0aGlzLmV4cG9ydHMpO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlZmluZUVtaXRDb21wbGV0ZSA9IHRydWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sXHJcblx0XHRcdFxyXG5cdFx0XHRjYWxsUGx1Z2luOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRsZXQgbWFwID0gdGhpcy5tYXA7XHJcblx0XHRcdFx0bGV0IGlkID0gbWFwLmlkO1xyXG5cdFx0XHRcdC8vIE1hcCBhbHJlYWR5IG5vcm1hbGl6ZWQgdGhlIHByZWZpeC5cclxuXHRcdFx0XHRsZXQgcGx1Z2luTWFwID0gbWFrZU1vZHVsZU1hcChtYXAucHJlZml4KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBNYXJrIHRoaXMgYXMgYSBkZXBlbmRlbmN5IGZvciB0aGlzIHBsdWdpbiwgc28gaXRcclxuXHRcdFx0XHQvLyBjYW4gYmUgdHJhY2VkIGZvciBjeWNsZXMuXHJcblx0XHRcdFx0dGhpcy5kZXBNYXBzLnB1c2gocGx1Z2luTWFwKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRvbihwbHVnaW5NYXAsICdkZWZpbmVkJywgYmluZCh0aGlzLCBmdW5jdGlvbihwbHVnaW4pIHtcclxuXHRcdFx0XHRcdGxldCBsb2FkLCBub3JtYWxpemVkTWFwLCBub3JtYWxpemVkTW9kLFxyXG5cdFx0XHRcdFx0XHRidW5kbGVJZCA9IGdldE93bihidW5kbGVzTWFwLCB0aGlzLm1hcC5pZCksXHJcblx0XHRcdFx0XHRcdG5hbWUgPSB0aGlzLm1hcC5uYW1lLFxyXG5cdFx0XHRcdFx0XHRwYXJlbnROYW1lID0gdGhpcy5tYXAucGFyZW50TWFwID8gdGhpcy5tYXAucGFyZW50TWFwLm5hbWUgOiBudWxsLFxyXG5cdFx0XHRcdFx0XHRsb2NhbFJlcXVpcmUgPSBjb250ZXh0Lm1ha2VSZXF1aXJlKG1hcC5wYXJlbnRNYXAsIHtcclxuXHRcdFx0XHRcdFx0XHRlbmFibGVCdWlsZENhbGxiYWNrOiB0cnVlXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBJZiBjdXJyZW50IG1hcCBpcyBub3Qgbm9ybWFsaXplZCwgd2FpdCBmb3IgdGhhdFxyXG5cdFx0XHRcdFx0Ly8gbm9ybWFsaXplZCBuYW1lIHRvIGxvYWQgaW5zdGVhZCBvZiBjb250aW51aW5nLlxyXG5cdFx0XHRcdFx0aWYgKHRoaXMubWFwLnVubm9ybWFsaXplZCkge1xyXG5cdFx0XHRcdFx0XHQvLyBOb3JtYWxpemUgdGhlIElEIGlmIHRoZSBwbHVnaW4gYWxsb3dzIGl0LlxyXG5cdFx0XHRcdFx0XHRpZiAocGx1Z2luLm5vcm1hbGl6ZSkge1xyXG5cdFx0XHRcdFx0XHRcdG5hbWUgPSBwbHVnaW4ubm9ybWFsaXplKG5hbWUsIGZ1bmN0aW9uKG5hbWUpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0cmV0dXJuIG5vcm1hbGl6ZShuYW1lLCBwYXJlbnROYW1lLCB0cnVlKTtcclxuXHRcdFx0XHRcdFx0XHRcdH0pIHx8ICcnO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHQvLyBwcmVmaXggYW5kIG5hbWUgc2hvdWxkIGFscmVhZHkgYmUgbm9ybWFsaXplZCwgbm8gbmVlZFxyXG5cdFx0XHRcdFx0XHQvLyBmb3IgYXBwbHlpbmcgbWFwIGNvbmZpZyBhZ2FpbiBlaXRoZXIuXHJcblx0XHRcdFx0XHRcdG5vcm1hbGl6ZWRNYXAgPSBtYWtlTW9kdWxlTWFwKG1hcC5wcmVmaXggKyAnIScgKyBuYW1lLFxyXG5cdFx0XHRcdFx0XHRcdHRoaXMubWFwLnBhcmVudE1hcCk7XHJcblx0XHRcdFx0XHRcdG9uKG5vcm1hbGl6ZWRNYXAsXHJcblx0XHRcdFx0XHRcdFx0J2RlZmluZWQnLCBiaW5kKHRoaXMsIGZ1bmN0aW9uKHZhbHVlKSB7XHJcblx0XHRcdFx0XHRcdFx0XHR0aGlzLm1hcC5ub3JtYWxpemVkTWFwID0gbm9ybWFsaXplZE1hcDtcclxuXHRcdFx0XHRcdFx0XHRcdHRoaXMuaW5pdChbXSwgZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHJldHVybiB2YWx1ZTtcclxuXHRcdFx0XHRcdFx0XHRcdH0sIG51bGwsIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0ZW5hYmxlZDogdHJ1ZSxcclxuXHRcdFx0XHRcdFx0XHRcdFx0aWdub3JlOiB0cnVlXHJcblx0XHRcdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0XHR9KSk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRub3JtYWxpemVkTW9kID0gZ2V0T3duKHJlZ2lzdHJ5LCBub3JtYWxpemVkTWFwLmlkKTtcclxuXHRcdFx0XHRcdFx0aWYgKG5vcm1hbGl6ZWRNb2QpIHtcclxuXHRcdFx0XHRcdFx0XHQvLyBNYXJrIHRoaXMgYXMgYSBkZXBlbmRlbmN5IGZvciB0aGlzIHBsdWdpbiwgc28gaXRcclxuXHRcdFx0XHRcdFx0XHQvLyBjYW4gYmUgdHJhY2VkIGZvciBjeWNsZXMuXHJcblx0XHRcdFx0XHRcdFx0dGhpcy5kZXBNYXBzLnB1c2gobm9ybWFsaXplZE1hcCk7XHJcblx0XHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0aWYgKHRoaXMuZXZlbnRzLmVycm9yKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRub3JtYWxpemVkTW9kLm9uKCdlcnJvcicsIGJpbmQodGhpcywgZnVuY3Rpb24oZXJyKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuZW1pdCgnZXJyb3InLCBlcnIpO1xyXG5cdFx0XHRcdFx0XHRcdFx0fSkpO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRub3JtYWxpemVkTW9kLmVuYWJsZSgpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdC8vIElmIGEgcGF0aHMgY29uZmlnLCB0aGVuIGp1c3QgbG9hZCB0aGF0IGZpbGUgaW5zdGVhZCB0b1xyXG5cdFx0XHRcdFx0Ly8gcmVzb2x2ZSB0aGUgcGx1Z2luLCBhcyBpdCBpcyBidWlsdCBpbnRvIHRoYXQgcGF0aHMgbGF5ZXIuXHJcblx0XHRcdFx0XHRpZiAoYnVuZGxlSWQpIHtcclxuXHRcdFx0XHRcdFx0dGhpcy5tYXAudXJsID0gY29udGV4dC5uYW1lVG9VcmwoYnVuZGxlSWQpO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmxvYWQoKTtcclxuXHRcdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRsb2FkID0gYmluZCh0aGlzLCBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLmluaXQoW10sIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRcdHJldHVybiB2YWx1ZTtcclxuXHRcdFx0XHRcdFx0fSwgbnVsbCwge1xyXG5cdFx0XHRcdFx0XHRcdGVuYWJsZWQ6IHRydWVcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0bG9hZC5lcnJvciA9IGJpbmQodGhpcywgZnVuY3Rpb24oZXJyKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuaW5pdGVkID0gdHJ1ZTtcclxuXHRcdFx0XHRcdFx0dGhpcy5lcnJvciA9IGVycjtcclxuXHRcdFx0XHRcdFx0ZXJyLnJlcXVpcmVNb2R1bGVzID0gW2lkXTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdC8vIFJlbW92ZSB0ZW1wIHVubm9ybWFsaXplZCBtb2R1bGVzIGZvciB0aGlzIG1vZHVsZSwgc2luY2UgdGhleSB3aWxsIG5ldmVyIGJlIHJlc29sdmVkIG90aGVyd2lzZSBcclxuXHRcdFx0XHRcdFx0Ly8gbm93LlxyXG5cdFx0XHRcdFx0XHRlYWNoUHJvcChyZWdpc3RyeSwgZnVuY3Rpb24obW9kKSB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKG1vZC5tYXAuaWQuaW5kZXhPZihpZCArICdfdW5ub3JtYWxpemVkJykgPT09IDApIHtcclxuXHRcdFx0XHRcdFx0XHRcdGNsZWFuUmVnaXN0cnkobW9kLm1hcC5pZCk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdG9uRXJyb3IoZXJyKTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBBbGxvdyBwbHVnaW5zIHRvIGxvYWQgb3RoZXIgY29kZSB3aXRob3V0IGhhdmluZyB0byBrbm93IHRoZSBjb250ZXh0IG9yIGhvdyB0byAnY29tcGxldGUnIHRoZSBcclxuXHRcdFx0XHRcdC8vIGxvYWQuXHJcblx0XHRcdFx0XHRsb2FkLmZyb21UZXh0ID0gYmluZCh0aGlzLCBmdW5jdGlvbih0ZXh0LCB0ZXh0QWx0KSB7XHJcblx0XHRcdFx0XHRcdC8qanNsaW50IGV2aWw6IHRydWUgKi9cclxuXHRcdFx0XHRcdFx0bGV0IG1vZHVsZU5hbWUgPSBtYXAubmFtZSxcclxuXHRcdFx0XHRcdFx0XHRtb2R1bGVNYXAgPSBtYWtlTW9kdWxlTWFwKG1vZHVsZU5hbWUpLFxyXG5cdFx0XHRcdFx0XHRcdGhhc0ludGVyYWN0aXZlID0gdXNlSW50ZXJhY3RpdmU7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHQvLyBBcyBvZiAyLjEuMCwgc3VwcG9ydCBqdXN0IHBhc3NpbmcgdGhlIHRleHQsIHRvIHJlaW5mb3JjZSBmcm9tVGV4dCBvbmx5IGJlaW5nIGNhbGxlZCBvbmNlIHBlciBcclxuXHRcdFx0XHRcdFx0Ly8gcmVzb3VyY2UuIFN0aWxsIHN1cHBvcnQgb2xkIHN0eWxlIG9mIHBhc3NpbmcgbW9kdWxlTmFtZSBidXQgZGlzY2FyZCB0aGF0IG1vZHVsZU5hbWUgaW4gZmF2b3IgXHJcblx0XHRcdFx0XHRcdC8vIG9mIHRoZSBpbnRlcm5hbCByZWYuXHJcblx0XHRcdFx0XHRcdGlmICh0ZXh0QWx0KSB7XHJcblx0XHRcdFx0XHRcdFx0dGV4dCA9IHRleHRBbHQ7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdC8vIFR1cm4gb2ZmIGludGVyYWN0aXZlIHNjcmlwdCBtYXRjaGluZyBmb3IgSUUgZm9yIGFueSBkZWZpbmUgY2FsbHMgaW4gdGhlIHRleHQsIHRoZW4gdHVybiBpdCBcclxuXHRcdFx0XHRcdFx0Ly8gYmFjayBvbiBhdCB0aGUgZW5kLlxyXG5cdFx0XHRcdFx0XHRpZiAoaGFzSW50ZXJhY3RpdmUpIHtcclxuXHRcdFx0XHRcdFx0XHR1c2VJbnRlcmFjdGl2ZSA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHQvLyBQcmltZSB0aGUgc3lzdGVtIGJ5IGNyZWF0aW5nIGEgbW9kdWxlIGluc3RhbmNlIGZvciBpdC5cclxuXHRcdFx0XHRcdFx0Z2V0TW9kdWxlKG1vZHVsZU1hcCk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHQvLyBUcmFuc2ZlciBhbnkgY29uZmlnIHRvIHRoaXMgb3RoZXIgbW9kdWxlLlxyXG5cdFx0XHRcdFx0XHRpZiAoaGFzUHJvcChjb25maWcuY29uZmlnLCBpZCkpIHtcclxuXHRcdFx0XHRcdFx0XHRjb25maWcuY29uZmlnW21vZHVsZU5hbWVdID0gY29uZmlnLmNvbmZpZ1tpZF07XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdHRyeSB7XHJcblx0XHRcdFx0XHRcdFx0cmVxLmV4ZWModGV4dCk7XHJcblx0XHRcdFx0XHRcdH0gY2F0Y2ggKGUpIHtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gb25FcnJvcihtYWtlRXJyb3IoJ2Zyb210ZXh0ZXZhbCcsXHJcblx0XHRcdFx0XHRcdFx0XHQnZnJvbVRleHQgZXZhbCBmb3IgJyArIGlkICtcclxuXHRcdFx0XHRcdFx0XHRcdCcgZmFpbGVkOiAnICsgZSxcclxuXHRcdFx0XHRcdFx0XHRcdGUsXHJcblx0XHRcdFx0XHRcdFx0XHRbaWRdKSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdGlmIChoYXNJbnRlcmFjdGl2ZSkge1xyXG5cdFx0XHRcdFx0XHRcdHVzZUludGVyYWN0aXZlID0gdHJ1ZTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gTWFyayB0aGlzIGFzIGEgZGVwZW5kZW5jeSBmb3IgdGhlIHBsdWdpbiByZXNvdXJjZS5cclxuXHRcdFx0XHRcdFx0dGhpcy5kZXBNYXBzLnB1c2gobW9kdWxlTWFwKTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdC8vIFN1cHBvcnQgYW5vbnltb3VzIG1vZHVsZXMuXHJcblx0XHRcdFx0XHRcdGNvbnRleHQuY29tcGxldGVMb2FkKG1vZHVsZU5hbWUpO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gQmluZCB0aGUgdmFsdWUgb2YgdGhhdCBtb2R1bGUgdG8gdGhlIHZhbHVlIGZvciB0aGlzIHJlc291cmNlIElELlxyXG5cdFx0XHRcdFx0XHRsb2NhbFJlcXVpcmUoW21vZHVsZU5hbWVdLCBsb2FkKTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBVc2UgcGFyZW50TmFtZSBoZXJlIHNpbmNlIHRoZSBwbHVnaW4ncyBuYW1lIGlzIG5vdCByZWxpYWJsZSwgY291bGQgYmUgc29tZSB3ZWlyZCBzdHJpbmcgd2l0aCBubyBcclxuXHRcdFx0XHRcdC8vIHBhdGggdGhhdCBhY3R1YWxseSB3YW50cyB0byByZWZlcmVuY2UgdGhlIHBhcmVudE5hbWUncyBwYXRoLlxyXG5cdFx0XHRcdFx0cGx1Z2luLmxvYWQobWFwLm5hbWUsIGxvY2FsUmVxdWlyZSwgbG9hZCwgY29uZmlnKTtcclxuXHRcdFx0XHR9KSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y29udGV4dC5lbmFibGUocGx1Z2luTWFwLCB0aGlzKTtcclxuXHRcdFx0XHR0aGlzLnBsdWdpbk1hcHNbcGx1Z2luTWFwLmlkXSA9IHBsdWdpbk1hcDtcclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdGVuYWJsZTogZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0ZW5hYmxlZFJlZ2lzdHJ5W3RoaXMubWFwLmlkXSA9IHRoaXM7XHJcblx0XHRcdFx0dGhpcy5lbmFibGVkID0gdHJ1ZTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBTZXQgZmxhZyBtZW50aW9uaW5nIHRoYXQgdGhlIG1vZHVsZSBpcyBlbmFibGluZywgc28gdGhhdCBpbW1lZGlhdGUgY2FsbHMgdG8gdGhlIGRlZmluZWQgY2FsbGJhY2tzIGZvciBcclxuXHRcdFx0XHQvLyBkZXBlbmRlbmNpZXMgZG8gbm90IHRyaWdnZXIgaW5hZHZlcnRlbnQgbG9hZCB3aXRoIHRoZSBkZXBDb3VudCBzdGlsbCBiZWluZyB6ZXJvLlxyXG5cdFx0XHRcdHRoaXMuZW5hYmxpbmcgPSB0cnVlO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIEVuYWJsZSBlYWNoIGRlcGVuZGVuY3kuXHJcblx0XHRcdFx0ZWFjaCh0aGlzLmRlcE1hcHMsIGJpbmQodGhpcywgZnVuY3Rpb24oZGVwTWFwLCBpKSB7XHJcblx0XHRcdFx0XHRsZXQgaWQ7IFxyXG5cdFx0XHRcdFx0bGV0IG1vZDsgXHJcblx0XHRcdFx0XHRsZXQgaGFuZGxlcjtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0aWYgKHR5cGVvZiBkZXBNYXAgPT09ICdzdHJpbmcnKSB7XHJcblx0XHRcdFx0XHRcdC8vIERlcGVuZGVuY3kgbmVlZHMgdG8gYmUgY29udmVydGVkIHRvIGEgZGVwTWFwIGFuZCB3aXJlZCB1cCB0byB0aGlzIG1vZHVsZS5cclxuXHRcdFx0XHRcdFx0ZGVwTWFwID0gbWFrZU1vZHVsZU1hcChkZXBNYXAsXHJcblx0XHRcdFx0XHRcdFx0KHRoaXMubWFwLmlzRGVmaW5lID8gdGhpcy5tYXAgOiB0aGlzLm1hcC5wYXJlbnRNYXApLFxyXG5cdFx0XHRcdFx0XHRcdGZhbHNlLFxyXG5cdFx0XHRcdFx0XHRcdCF0aGlzLnNraXBNYXApO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmRlcE1hcHNbaV0gPSBkZXBNYXA7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRoYW5kbGVyID0gZ2V0T3duKGhhbmRsZXJzLCBkZXBNYXAuaWQpO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0aWYgKGhhbmRsZXIpIHtcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmRlcEV4cG9ydHNbaV0gPSBoYW5kbGVyKHRoaXMpO1xyXG5cdFx0XHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0dGhpcy5kZXBDb3VudCArPSAxO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0b24oZGVwTWFwLCAnZGVmaW5lZCcsIGJpbmQodGhpcywgZnVuY3Rpb24oZGVwRXhwb3J0cykge1xyXG5cdFx0XHRcdFx0XHRcdGlmICh0aGlzLnVuZGVmZWQpIHtcclxuXHRcdFx0XHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5kZWZpbmVEZXAoaSwgZGVwRXhwb3J0cyk7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5jaGVjaygpO1xyXG5cdFx0XHRcdFx0XHR9KSk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRpZiAodGhpcy5lcnJiYWNrKSB7XHJcblx0XHRcdFx0XHRcdFx0b24oZGVwTWFwLCAnZXJyb3InLCBiaW5kKHRoaXMsIHRoaXMuZXJyYmFjaykpO1xyXG5cdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHRoaXMuZXZlbnRzLmVycm9yKSB7XHJcblx0XHRcdFx0XHRcdFx0Ly8gTm8gZGlyZWN0IGVycmJhY2sgb24gdGhpcyBtb2R1bGUsIGJ1dCBzb21ldGhpbmcgZWxzZSBpcyBsaXN0ZW5pbmcgZm9yIGVycm9ycywgc28gYmUgc3VyZSBcclxuXHRcdFx0XHRcdFx0XHQvLyB0byBwcm9wYWdhdGUgdGhlIGVycm9yIGNvcnJlY3RseS5cclxuXHRcdFx0XHRcdFx0XHRvbihkZXBNYXAsICdlcnJvcicsIGJpbmQodGhpcywgZnVuY3Rpb24oZXJyKSB7XHJcblx0XHRcdFx0XHRcdFx0XHR0aGlzLmVtaXQoJ2Vycm9yJywgZXJyKTtcclxuXHRcdFx0XHRcdFx0XHR9KSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0aWQgPSBkZXBNYXAuaWQ7XHJcblx0XHRcdFx0XHRtb2QgPSByZWdpc3RyeVtpZF07XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdC8vIFNraXAgc3BlY2lhbCBtb2R1bGVzIGxpa2UgJ3JlcXVpcmUnLCAnZXhwb3J0cycsICdtb2R1bGUnLiBBbHNvLCBkb24ndCBjYWxsIGVuYWJsZSBpZiBpdCBpcyBcclxuXHRcdFx0XHRcdC8vIGFscmVhZHkgZW5hYmxlZCwgaW1wb3J0YW50IGluIGNpcmN1bGFyIGRlcGVuZGVuY3kgY2FzZXMuXHJcblx0XHRcdFx0XHRpZiAoIWhhc1Byb3AoaGFuZGxlcnMsIGlkKSAmJiBtb2QgJiYgIW1vZC5lbmFibGVkKSB7XHJcblx0XHRcdFx0XHRcdGNvbnRleHQuZW5hYmxlKGRlcE1hcCwgdGhpcyk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSkpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIEVuYWJsZSBlYWNoIHBsdWdpbiB0aGF0IGlzIHVzZWQgaW4gIGEgZGVwZW5kZW5jeS5cclxuXHRcdFx0XHRlYWNoUHJvcCh0aGlzLnBsdWdpbk1hcHMsIGJpbmQodGhpcywgZnVuY3Rpb24ocGx1Z2luTWFwKSB7XHJcblx0XHRcdFx0XHRsZXQgbW9kID0gZ2V0T3duKHJlZ2lzdHJ5LCBwbHVnaW5NYXAuaWQpO1xyXG5cdFx0XHRcdFx0aWYgKG1vZCAmJiAhbW9kLmVuYWJsZWQpIHtcclxuXHRcdFx0XHRcdFx0Y29udGV4dC5lbmFibGUocGx1Z2luTWFwLCB0aGlzKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0dGhpcy5lbmFibGluZyA9IGZhbHNlO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdHRoaXMuY2hlY2soKTtcclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdG9uOiBmdW5jdGlvbihuYW1lLCBjYikge1xyXG5cdFx0XHRcdGxldCBjYnMgPSB0aGlzLmV2ZW50c1tuYW1lXTtcclxuXHRcdFx0XHRpZiAoIWNicykge1xyXG5cdFx0XHRcdFx0Y2JzID0gdGhpcy5ldmVudHNbbmFtZV0gPSBbXTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0Y2JzLnB1c2goY2IpO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRcclxuXHRcdFx0ZW1pdDogZnVuY3Rpb24obmFtZSwgZXZ0KSB7XHJcblx0XHRcdFx0ZWFjaCh0aGlzLmV2ZW50c1tuYW1lXSwgZnVuY3Rpb24oY2IpIHtcclxuXHRcdFx0XHRcdGNiKGV2dCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0aWYgKG5hbWUgPT09ICdlcnJvcicpIHtcclxuXHRcdFx0XHRcdC8vIE5vdyB0aGF0IHRoZSBlcnJvciBoYW5kbGVyIHdhcyB0cmlnZ2VyZWQsIHJlbW92ZSB0aGUgbGlzdGVuZXJzLCBzaW5jZSB0aGlzIGJyb2tlbiBNb2R1bGUgaW5zdGFuY2VcclxuXHRcdFx0XHRcdC8vIGNhbiBzdGF5IGFyb3VuZCBmb3IgYSB3aGlsZSBpbiB0aGUgcmVnaXN0cnkuXHJcblx0XHRcdFx0XHRkZWxldGUgdGhpcy5ldmVudHNbbmFtZV07XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHRmdW5jdGlvbiBjYWxsR2V0TW9kdWxlKGFyZ3MpIHtcclxuXHRcdFx0Ly8gU2tpcCBtb2R1bGVzIGFscmVhZHkgZGVmaW5lZC5cclxuXHRcdFx0aWYgKCFoYXNQcm9wKGRlZmluZWQsIGFyZ3NbMF0pKSB7XHJcblx0XHRcdFx0Z2V0TW9kdWxlKG1ha2VNb2R1bGVNYXAoYXJnc1swXSwgbnVsbCwgdHJ1ZSkpLmluaXQoYXJnc1sxXSwgYXJnc1syXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0ZnVuY3Rpb24gcmVtb3ZlTGlzdGVuZXIobm9kZSwgZnVuYywgbmFtZSwgaWVOYW1lKSB7XHJcblx0XHRcdC8vIEZhdm9yIGRldGFjaEV2ZW50IGJlY2F1c2Ugb2YgSUU5IGlzc3VlLCBzZWUgYXR0YWNoRXZlbnQvYWRkRXZlbnRMaXN0ZW5lciBjb21tZW50IGVsc2V3aGVyZSBpbiB0aGlzIGZpbGUuXHJcblx0XHRcdGlmIChub2RlLmRldGFjaEV2ZW50ICYmICFpc09wZXJhKSB7XHJcblx0XHRcdFx0Ly8gUHJvYmFibHkgSUUuIElmIG5vdCBpdCB3aWxsIHRocm93IGFuIGVycm9yLCB3aGljaCB3aWxsIGJlIHVzZWZ1bCB0byBrbm93LlxyXG5cdFx0XHRcdGlmIChpZU5hbWUpIHtcclxuXHRcdFx0XHRcdG5vZGUuZGV0YWNoRXZlbnQoaWVOYW1lLCBmdW5jKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0bm9kZS5yZW1vdmVFdmVudExpc3RlbmVyKG5hbWUsIGZ1bmMsIGZhbHNlKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIEdpdmVuIGFuIGV2ZW50IGZyb20gYSBzY3JpcHQgbm9kZSwgZ2V0IHRoZSByZXF1aXJlanMgaW5mbyBmcm9tIGl0LCBhbmQgdGhlbiByZW1vdmVzIHRoZSBldmVudCBsaXN0ZW5lcnMgb24gXHJcblx0XHQgKiB0aGUgbm9kZS5cclxuXHRcdCAqIFxyXG5cdFx0ICogQHBhcmFtIHtFdmVudH0gZXZ0XHJcblx0XHQgKiBcclxuXHRcdCAqIEByZXR1cm4ge09iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gZ2V0U2NyaXB0RGF0YShldnQpIHtcclxuXHRcdFx0Ly8gVXNpbmcgY3VycmVudFRhcmdldCBpbnN0ZWFkIG9mIHRhcmdldCBmb3IgRmlyZWZveCAyLjAncyBzYWtlLiBOb3QgYWxsIG9sZCBicm93c2VycyB3aWxsIGJlIHN1cHBvcnRlZCwgYnV0IFxyXG5cdFx0XHQvLyB0aGlzIG9uZSB3YXMgZWFzeSBlbm91Z2ggdG8gc3VwcG9ydCBhbmQgc3RpbGwgbWFrZXMgc2Vuc2UuXHJcblx0XHRcdGxldCBub2RlID0gZXZ0LmN1cnJlbnRUYXJnZXQgfHwgZXZ0LnNyY0VsZW1lbnQ7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBSZW1vdmUgdGhlIGxpc3RlbmVycyBvbmNlIGhlcmUuXHJcblx0XHRcdHJlbW92ZUxpc3RlbmVyKG5vZGUsIGNvbnRleHQub25TY3JpcHRMb2FkLCAnbG9hZCcsICdvbnJlYWR5c3RhdGVjaGFuZ2UnKTtcclxuXHRcdFx0cmVtb3ZlTGlzdGVuZXIobm9kZSwgY29udGV4dC5vblNjcmlwdEVycm9yLCAnZXJyb3InKTtcclxuXHRcdFx0XHJcblx0XHRcdHJldHVybiB7XHJcblx0XHRcdFx0bm9kZTogbm9kZSxcclxuXHRcdFx0XHRpZDogbm9kZSAmJiBub2RlLmdldEF0dHJpYnV0ZSgnZGF0YS1yZXF1aXJlbW9kdWxlJylcclxuXHRcdFx0fTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0ZnVuY3Rpb24gaW50YWtlRGVmaW5lcygpIHtcclxuXHRcdFx0bGV0IGFyZ3M7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBBbnkgZGVmaW5lZCBtb2R1bGVzIGluIHRoZSBnbG9iYWwgcXVldWUsIGludGFrZSB0aGVtIG5vdy5cclxuXHRcdFx0dGFrZUdsb2JhbFF1ZXVlKCk7XHJcblx0XHRcdFxyXG5cdFx0XHQvLyBNYWtlIHN1cmUgYW55IHJlbWFpbmluZyBkZWZRdWV1ZSBpdGVtcyBnZXQgcHJvcGVybHkgcHJvY2Vzc2VkLlxyXG5cdFx0XHR3aGlsZSAoZGVmUXVldWUubGVuZ3RoKSB7XHJcblx0XHRcdFx0YXJncyA9IGRlZlF1ZXVlLnNoaWZ0KCk7XHJcblx0XHRcdFx0aWYgKGFyZ3NbMF0gPT09IG51bGwpIHtcclxuXHRcdFx0XHRcdHJldHVybiBvbkVycm9yKG1ha2VFcnJvcignbWlzbWF0Y2gnLCAnTWlzbWF0Y2hlZCBhbm9ueW1vdXMgZGVmaW5lKCkgbW9kdWxlOiAnICtcclxuXHRcdFx0XHRcdFx0YXJnc1thcmdzLmxlbmd0aCAtIDFdKSk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdC8vIGFyZ3MgYXJlIGlkLCBkZXBzLCBmYWN0b3J5LiBTaG91bGQgYmUgbm9ybWFsaXplZCBieSB0aGVcclxuXHRcdFx0XHRcdC8vIGRlZmluZSgpIGZ1bmN0aW9uLlxyXG5cdFx0XHRcdFx0Y2FsbEdldE1vZHVsZShhcmdzKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0Y29udGV4dC5kZWZRdWV1ZU1hcCA9IHt9O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRjb250ZXh0ID0ge1xyXG5cdFx0XHRjb25maWc6IGNvbmZpZyxcclxuXHRcdFx0Y29udGV4dE5hbWU6IGNvbnRleHROYW1lLFxyXG5cdFx0XHRyZWdpc3RyeTogcmVnaXN0cnksXHJcblx0XHRcdGRlZmluZWQ6IGRlZmluZWQsXHJcblx0XHRcdHVybEZldGNoZWQ6IHVybEZldGNoZWQsXHJcblx0XHRcdGRlZlF1ZXVlOiBkZWZRdWV1ZSxcclxuXHRcdFx0ZGVmUXVldWVNYXA6IHt9LFxyXG5cdFx0XHRNb2R1bGU6IE1vZHVsZSxcclxuXHRcdFx0bWFrZU1vZHVsZU1hcDogbWFrZU1vZHVsZU1hcCxcclxuXHRcdFx0bmV4dFRpY2s6IHJlcS5uZXh0VGljayxcclxuXHRcdFx0b25FcnJvcjogb25FcnJvcixcclxuXHRcdFx0XHJcblx0XHRcdC8qKlxyXG5cdFx0XHQgKiBTZXQgYSBjb25maWd1cmF0aW9uIGZvciB0aGUgY29udGV4dC5cclxuXHRcdFx0ICogXHJcblx0XHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBjZmcgY29uZmlnIG9iamVjdCB0byBpbnRlZ3JhdGUuXHJcblx0XHRcdCAqL1xyXG5cdFx0XHRjb25maWd1cmU6IGZ1bmN0aW9uKGNmZykge1xyXG5cdFx0XHRcdC8vIE1ha2Ugc3VyZSB0aGUgYmFzZVVybCBlbmRzIGluIGEgc2xhc2guXHJcblx0XHRcdFx0aWYgKGNmZy5iYXNlVXJsKSB7XHJcblx0XHRcdFx0XHRpZiAoY2ZnLmJhc2VVcmwuY2hhckF0KGNmZy5iYXNlVXJsLmxlbmd0aCAtIDEpICE9PSAnLycpIHtcclxuXHRcdFx0XHRcdFx0Y2ZnLmJhc2VVcmwgKz0gJy8nO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBTYXZlIG9mZiB0aGUgcGF0aHMgc2luY2UgdGhleSByZXF1aXJlIHNwZWNpYWwgcHJvY2Vzc2luZywgdGhleSBhcmUgYWRkaXRpdmUuXHJcblx0XHRcdFx0bGV0IHNoaW0gPSBjb25maWcuc2hpbSxcclxuXHRcdFx0XHRcdG9ianMgPSB7XHJcblx0XHRcdFx0XHRcdHBhdGhzOiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRidW5kbGVzOiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRjb25maWc6IHRydWUsXHJcblx0XHRcdFx0XHRcdG1hcDogdHJ1ZVxyXG5cdFx0XHRcdFx0fTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRlYWNoUHJvcChjZmcsIGZ1bmN0aW9uKHZhbHVlLCBwcm9wKSB7XHJcblx0XHRcdFx0XHRpZiAob2Jqc1twcm9wXSkge1xyXG5cdFx0XHRcdFx0XHRpZiAoIWNvbmZpZ1twcm9wXSkge1xyXG5cdFx0XHRcdFx0XHRcdGNvbmZpZ1twcm9wXSA9IHt9O1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdG1peGluKGNvbmZpZ1twcm9wXSwgdmFsdWUsIHRydWUsIHRydWUpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0Y29uZmlnW3Byb3BdID0gdmFsdWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gUmV2ZXJzZSBtYXAgdGhlIGJ1bmRsZXNcclxuXHRcdFx0XHRpZiAoY2ZnLmJ1bmRsZXMpIHtcclxuXHRcdFx0XHRcdGVhY2hQcm9wKGNmZy5idW5kbGVzLCBmdW5jdGlvbih2YWx1ZSwgcHJvcCkge1xyXG5cdFx0XHRcdFx0XHRlYWNoKHZhbHVlLCBmdW5jdGlvbih2KSB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKHYgIT09IHByb3ApIHtcclxuXHRcdFx0XHRcdFx0XHRcdGJ1bmRsZXNNYXBbdl0gPSBwcm9wO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gTWVyZ2Ugc2hpbVxyXG5cdFx0XHRcdGlmIChjZmcuc2hpbSkge1xyXG5cdFx0XHRcdFx0ZWFjaFByb3AoY2ZnLnNoaW0sIGZ1bmN0aW9uKHZhbHVlLCBpZCkge1xyXG5cdFx0XHRcdFx0XHQvLyBOb3JtYWxpemUgdGhlIHN0cnVjdHVyZVxyXG5cdFx0XHRcdFx0XHRpZiAoaXNBcnJheSh2YWx1ZSkpIHtcclxuXHRcdFx0XHRcdFx0XHR2YWx1ZSA9IHtcclxuXHRcdFx0XHRcdFx0XHRcdGRlcHM6IHZhbHVlXHJcblx0XHRcdFx0XHRcdFx0fTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRpZiAoKHZhbHVlLmV4cG9ydHMgfHwgdmFsdWUuaW5pdCkgJiYgIXZhbHVlLmV4cG9ydHNGbikge1xyXG5cdFx0XHRcdFx0XHRcdHZhbHVlLmV4cG9ydHNGbiA9IGNvbnRleHQubWFrZVNoaW1FeHBvcnRzKHZhbHVlKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRzaGltW2lkXSA9IHZhbHVlO1xyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRjb25maWcuc2hpbSA9IHNoaW07XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIEFkanVzdCBwYWNrYWdlcyBpZiBuZWNlc3NhcnkuXHJcblx0XHRcdFx0aWYgKGNmZy5wYWNrYWdlcykge1xyXG5cdFx0XHRcdFx0ZWFjaChjZmcucGFja2FnZXMsIGZ1bmN0aW9uKHBrZ09iaikge1xyXG5cdFx0XHRcdFx0XHRsZXQgbG9jYXRpb24sIG5hbWU7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRwa2dPYmogPSB0eXBlb2YgcGtnT2JqID09PSAnc3RyaW5nJyA/IHtuYW1lOiBwa2dPYmp9IDogcGtnT2JqO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0bmFtZSA9IHBrZ09iai5uYW1lO1xyXG5cdFx0XHRcdFx0XHRsb2NhdGlvbiA9IHBrZ09iai5sb2NhdGlvbjtcclxuXHRcdFx0XHRcdFx0aWYgKGxvY2F0aW9uKSB7XHJcblx0XHRcdFx0XHRcdFx0Y29uZmlnLnBhdGhzW25hbWVdID0gcGtnT2JqLmxvY2F0aW9uO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHQvLyBTYXZlIHBvaW50ZXIgdG8gbWFpbiBtb2R1bGUgSUQgZm9yIHBrZyBuYW1lLiBSZW1vdmUgbGVhZGluZyBkb3QgaW4gbWFpbiwgc28gbWFpbiBwYXRocyBhcmUgXHJcblx0XHRcdFx0XHRcdC8vIG5vcm1hbGl6ZWQsIGFuZCByZW1vdmUgYW55IHRyYWlsaW5nIC5qcywgc2luY2UgZGlmZmVyZW50IHBhY2thZ2UgZW52cyBoYXZlIGRpZmZlcmVudCBcclxuXHRcdFx0XHRcdFx0Ly8gY29udmVudGlvbnM6IHNvbWUgdXNlIGEgbW9kdWxlIG5hbWUsIHNvbWUgdXNlIGEgZmlsZSBuYW1lLlxyXG5cdFx0XHRcdFx0XHRjb25maWcucGtnc1tuYW1lXSA9IHBrZ09iai5uYW1lICsgJy8nICsgKHBrZ09iai5tYWluIHx8ICdtYWluJylcclxuXHRcdFx0XHRcdFx0XHRcdC5yZXBsYWNlKGN1cnJEaXJSZWdFeHAsICcnKVxyXG5cdFx0XHRcdFx0XHRcdFx0LnJlcGxhY2UoanNTdWZmaXhSZWdFeHAsICcnKTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBJZiB0aGVyZSBhcmUgYW55IFwid2FpdGluZyB0byBleGVjdXRlXCIgbW9kdWxlcyBpbiB0aGUgcmVnaXN0cnksIHVwZGF0ZSB0aGUgbWFwcyBmb3IgdGhlbSwgc2luY2UgdGhlaXIgXHJcblx0XHRcdFx0Ly8gaW5mbywgbGlrZSBVUkxzIHRvIGxvYWQsIG1heSBoYXZlIGNoYW5nZWQuXHJcblx0XHRcdFx0ZWFjaFByb3AocmVnaXN0cnksIGZ1bmN0aW9uKG1vZCwgaWQpIHtcclxuXHRcdFx0XHRcdC8vIElmIG1vZHVsZSBhbHJlYWR5IGhhcyBpbml0IGNhbGxlZCwgc2luY2UgaXQgaXMgdG9vIGxhdGUgdG8gbW9kaWZ5IHRoZW0sIGFuZCBpZ25vcmUgdW5ub3JtYWxpemVkIFxyXG5cdFx0XHRcdFx0Ly8gb25lcyBzaW5jZSB0aGV5IGFyZSB0cmFuc2llbnQuXHJcblx0XHRcdFx0XHRpZiAoIW1vZC5pbml0ZWQgJiYgIW1vZC5tYXAudW5ub3JtYWxpemVkKSB7XHJcblx0XHRcdFx0XHRcdG1vZC5tYXAgPSBtYWtlTW9kdWxlTWFwKGlkLCBudWxsLCB0cnVlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBJZiBhIGRlcHMgYXJyYXkgb3IgYSBjb25maWcgY2FsbGJhY2sgaXMgc3BlY2lmaWVkLCB0aGVuIGNhbGwgcmVxdWlyZSB3aXRoIHRob3NlIGFyZ3MuIFRoaXMgaXMgdXNlZnVsIFxyXG5cdFx0XHRcdC8vIHdoZW4gcmVxdWlyZSBpcyBkZWZpbmVkIGFzIGEgY29uZmlnIG9iamVjdCBiZWZvcmUgcmVxdWlyZS5qcyBpcyBsb2FkZWQuXHJcblx0XHRcdFx0aWYgKGNmZy5kZXBzIHx8IGNmZy5jYWxsYmFjaykge1xyXG5cdFx0XHRcdFx0Y29udGV4dC5yZXF1aXJlKGNmZy5kZXBzIHx8IFtdLCBjZmcuY2FsbGJhY2spO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdG1ha2VTaGltRXhwb3J0czogZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0XHRmdW5jdGlvbiBmbigpIHtcclxuXHRcdFx0XHRcdGxldCByZXQ7XHJcblx0XHRcdFx0XHRpZiAodmFsdWUuaW5pdCkge1xyXG5cdFx0XHRcdFx0XHRyZXQgPSB2YWx1ZS5pbml0LmFwcGx5KGdsb2JhbCwgYXJndW1lbnRzKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJldHVybiByZXQgfHwgKHZhbHVlLmV4cG9ydHMgJiYgZ2V0R2xvYmFsKHZhbHVlLmV4cG9ydHMpKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0cmV0dXJuIGZuO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRcclxuXHRcdFx0bWFrZVJlcXVpcmU6IGZ1bmN0aW9uKHJlbE1hcCwgb3B0aW9ucykge1xyXG5cdFx0XHRcdG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGZ1bmN0aW9uIGxvY2FsUmVxdWlyZShkZXBzLCBjYWxsYmFjaywgZXJyYmFjaykge1xyXG5cdFx0XHRcdFx0bGV0IGlkLCBtYXAsIHJlcXVpcmVNb2Q7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmIChvcHRpb25zLmVuYWJsZUJ1aWxkQ2FsbGJhY2sgJiYgY2FsbGJhY2sgJiYgaXNGdW5jdGlvbihjYWxsYmFjaykpIHtcclxuXHRcdFx0XHRcdFx0Y2FsbGJhY2suX19yZXF1aXJlSnNCdWlsZCA9IHRydWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmICh0eXBlb2YgZGVwcyA9PT0gJ3N0cmluZycpIHtcclxuXHRcdFx0XHRcdFx0aWYgKGlzRnVuY3Rpb24oY2FsbGJhY2spKSB7XHJcblx0XHRcdFx0XHRcdFx0Ly8gSW52YWxpZCBjYWxsXHJcblx0XHRcdFx0XHRcdFx0cmV0dXJuIG9uRXJyb3IobWFrZUVycm9yKCdyZXF1aXJlYXJncycsICdJbnZhbGlkIHJlcXVpcmUgY2FsbCcpLCBlcnJiYWNrKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gSWYgcmVxdWlyZXxleHBvcnRzfG1vZHVsZSBhcmUgcmVxdWVzdGVkLCBnZXQgdGhlIHZhbHVlIGZvciB0aGVtIGZyb20gdGhlIHNwZWNpYWwgaGFuZGxlcnMuIFxyXG5cdFx0XHRcdFx0XHQvLyBDYXZlYXQ6IHRoaXMgb25seSB3b3JrcyB3aGlsZSBtb2R1bGUgaXMgYmVpbmcgZGVmaW5lZC5cclxuXHRcdFx0XHRcdFx0aWYgKHJlbE1hcCAmJiBoYXNQcm9wKGhhbmRsZXJzLCBkZXBzKSkge1xyXG5cdFx0XHRcdFx0XHRcdHJldHVybiBoYW5kbGVyc1tkZXBzXShyZWdpc3RyeVtyZWxNYXAuaWRdKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gU3luY2hyb25vdXMgYWNjZXNzIHRvIG9uZSBtb2R1bGUuIElmIHJlcXVpcmUuZ2V0IGlzIGF2YWlsYWJsZSAoYXMgaW4gdGhlIE5vZGUgYWRhcHRlciksIFxyXG5cdFx0XHRcdFx0XHQvLyBwcmVmZXIgdGhhdC5cclxuXHRcdFx0XHRcdFx0aWYgKHJlcS5nZXQpIHtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gcmVxLmdldChjb250ZXh0LCBkZXBzLCByZWxNYXAsIGxvY2FsUmVxdWlyZSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdC8vIE5vcm1hbGl6ZSBtb2R1bGUgbmFtZSwgaWYgaXQgY29udGFpbnMgLiBvciAuLlxyXG5cdFx0XHRcdFx0XHRtYXAgPSBtYWtlTW9kdWxlTWFwKGRlcHMsIHJlbE1hcCwgZmFsc2UsIHRydWUpO1xyXG5cdFx0XHRcdFx0XHRpZCA9IG1hcC5pZDtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdGlmICghaGFzUHJvcChkZWZpbmVkLCBpZCkpIHtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gb25FcnJvcihtYWtlRXJyb3IoJ25vdGxvYWRlZCcsICdNb2R1bGUgbmFtZSBcIicgK1xyXG5cdFx0XHRcdFx0XHRcdFx0aWQgK1xyXG5cdFx0XHRcdFx0XHRcdFx0J1wiIGhhcyBub3QgYmVlbiBsb2FkZWQgeWV0IGZvciBjb250ZXh0OiAnICtcclxuXHRcdFx0XHRcdFx0XHRcdGNvbnRleHROYW1lICtcclxuXHRcdFx0XHRcdFx0XHRcdChyZWxNYXAgPyAnJyA6ICcuIFVzZSByZXF1aXJlKFtdKScpKSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0cmV0dXJuIGRlZmluZWRbaWRdO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBHcmFiIGRlZmluZXMgd2FpdGluZyBpbiB0aGUgZ2xvYmFsIHF1ZXVlLlxyXG5cdFx0XHRcdFx0aW50YWtlRGVmaW5lcygpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBNYXJrIGFsbCB0aGUgZGVwZW5kZW5jaWVzIGFzIG5lZWRpbmcgdG8gYmUgbG9hZGVkLlxyXG5cdFx0XHRcdFx0Y29udGV4dC5uZXh0VGljayhmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdFx0Ly8gU29tZSBkZWZpbmVzIGNvdWxkIGhhdmUgYmVlbiBhZGRlZCBzaW5jZSB0aGVcclxuXHRcdFx0XHRcdFx0Ly8gcmVxdWlyZSBjYWxsLCBjb2xsZWN0IHRoZW0uXHJcblx0XHRcdFx0XHRcdGludGFrZURlZmluZXMoKTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdHJlcXVpcmVNb2QgPSBnZXRNb2R1bGUobWFrZU1vZHVsZU1hcChudWxsLCByZWxNYXApKTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdC8vIFN0b3JlIGlmIG1hcCBjb25maWcgc2hvdWxkIGJlIGFwcGxpZWQgdG8gdGhpcyByZXF1aXJlXHJcblx0XHRcdFx0XHRcdC8vIGNhbGwgZm9yIGRlcGVuZGVuY2llcy5cclxuXHRcdFx0XHRcdFx0cmVxdWlyZU1vZC5za2lwTWFwID0gb3B0aW9ucy5za2lwTWFwO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0cmVxdWlyZU1vZC5pbml0KGRlcHMsIGNhbGxiYWNrLCBlcnJiYWNrLCB7XHJcblx0XHRcdFx0XHRcdFx0ZW5hYmxlZDogdHJ1ZVxyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdGNoZWNrTG9hZGVkKCk7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0cmV0dXJuIGxvY2FsUmVxdWlyZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0bWl4aW4obG9jYWxSZXF1aXJlLCB7XHJcblx0XHRcdFx0XHRpc0Jyb3dzZXI6IGlzQnJvd3NlcixcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0LyoqXHJcblx0XHRcdFx0XHQgKiBDb252ZXJ0cyBhIG1vZHVsZSBuYW1lICsgLmV4dGVuc2lvbiBpbnRvIGFuIFVSTCBwYXRoLlxyXG5cdFx0XHRcdFx0ICogXHJcblx0XHRcdFx0XHQgKiAqUmVxdWlyZXMqIHRoZSB1c2Ugb2YgYSBtb2R1bGUgbmFtZS4gSXQgZG9lcyBub3Qgc3VwcG9ydCB1c2luZyBwbGFpbiBVUkxzIGxpa2UgbmFtZVRvVXJsLlxyXG5cdFx0XHRcdFx0ICovXHJcblx0XHRcdFx0XHR0b1VybDogZnVuY3Rpb24obW9kdWxlTmFtZVBsdXNFeHQpIHtcclxuXHRcdFx0XHRcdFx0bGV0IGV4dCxcclxuXHRcdFx0XHRcdFx0XHRpbmRleCA9IG1vZHVsZU5hbWVQbHVzRXh0Lmxhc3RJbmRleE9mKCcuJyksXHJcblx0XHRcdFx0XHRcdFx0c2VnbWVudCA9IG1vZHVsZU5hbWVQbHVzRXh0LnNwbGl0KCcvJylbMF0sXHJcblx0XHRcdFx0XHRcdFx0aXNSZWxhdGl2ZSA9IHNlZ21lbnQgPT09ICcuJyB8fCBzZWdtZW50ID09PSAnLi4nO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gSGF2ZSBhIGZpbGUgZXh0ZW5zaW9uIGFsaWFzLCBhbmQgaXQgaXMgbm90IHRoZVxyXG5cdFx0XHRcdFx0XHQvLyBkb3RzIGZyb20gYSByZWxhdGl2ZSBwYXRoLlxyXG5cdFx0XHRcdFx0XHRpZiAoaW5kZXggIT09IC0xICYmICghaXNSZWxhdGl2ZSB8fCBpbmRleCA+IDEpKSB7XHJcblx0XHRcdFx0XHRcdFx0ZXh0ID0gbW9kdWxlTmFtZVBsdXNFeHQuc3Vic3RyaW5nKGluZGV4LCBtb2R1bGVOYW1lUGx1c0V4dC5sZW5ndGgpO1xyXG5cdFx0XHRcdFx0XHRcdG1vZHVsZU5hbWVQbHVzRXh0ID0gbW9kdWxlTmFtZVBsdXNFeHQuc3Vic3RyaW5nKDAsIGluZGV4KTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0cmV0dXJuIGNvbnRleHQubmFtZVRvVXJsKG5vcm1hbGl6ZShtb2R1bGVOYW1lUGx1c0V4dCxcclxuXHRcdFx0XHRcdFx0XHRyZWxNYXAgJiYgcmVsTWFwLmlkLCB0cnVlKSwgZXh0LCB0cnVlKTtcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGRlZmluZWQ6IGZ1bmN0aW9uKGlkKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiBoYXNQcm9wKGRlZmluZWQsIG1ha2VNb2R1bGVNYXAoaWQsIHJlbE1hcCwgZmFsc2UsIHRydWUpLmlkKTtcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdHNwZWNpZmllZDogZnVuY3Rpb24oaWQpIHtcclxuXHRcdFx0XHRcdFx0aWQgPSBtYWtlTW9kdWxlTWFwKGlkLCByZWxNYXAsIGZhbHNlLCB0cnVlKS5pZDtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIGhhc1Byb3AoZGVmaW5lZCwgaWQpIHx8IGhhc1Byb3AocmVnaXN0cnksIGlkKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBPbmx5IGFsbG93IHVuZGVmIG9uIHRvcCBsZXZlbCByZXF1aXJlIGNhbGxzLlxyXG5cdFx0XHRcdGlmICghcmVsTWFwKSB7XHJcblx0XHRcdFx0XHRsb2NhbFJlcXVpcmUudW5kZWYgPSBmdW5jdGlvbihpZCkge1xyXG5cdFx0XHRcdFx0XHQvLyBCaW5kIGFueSB3YWl0aW5nIGRlZmluZSgpIGNhbGxzIHRvIHRoaXMgY29udGV4dCwgZml4IGZvciAjNDA4LlxyXG5cdFx0XHRcdFx0XHR0YWtlR2xvYmFsUXVldWUoKTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdGxldCBtYXAgPSBtYWtlTW9kdWxlTWFwKGlkLCByZWxNYXAsIHRydWUpO1xyXG5cdFx0XHRcdFx0XHRsZXQgbW9kID0gZ2V0T3duKHJlZ2lzdHJ5LCBpZCk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRtb2QudW5kZWZlZCA9IHRydWU7XHJcblx0XHRcdFx0XHRcdHJlbW92ZVNjcmlwdChpZCk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRkZWxldGUgZGVmaW5lZFtpZF07XHJcblx0XHRcdFx0XHRcdGRlbGV0ZSB1cmxGZXRjaGVkW21hcC51cmxdO1xyXG5cdFx0XHRcdFx0XHRkZWxldGUgdW5kZWZFdmVudHNbaWRdO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Ly8gQ2xlYW4gcXVldWVkIGRlZmluZXMgdG9vLiBHbyBiYWNrd2FyZHMgaW4gYXJyYXkgc28gdGhhdCB0aGUgc3BsaWNlcyBkbyBub3QgbWVzcyB1cCB0aGUgXHJcblx0XHRcdFx0XHRcdC8vIGl0ZXJhdGlvbi5cclxuXHRcdFx0XHRcdFx0ZWFjaFJldmVyc2UoZGVmUXVldWUsIGZ1bmN0aW9uKGFyZ3MsIGkpIHtcclxuXHRcdFx0XHRcdFx0XHRpZiAoYXJnc1swXSA9PT0gaWQpIHtcclxuXHRcdFx0XHRcdFx0XHRcdGRlZlF1ZXVlLnNwbGljZShpLCAxKTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHRkZWxldGUgY29udGV4dC5kZWZRdWV1ZU1hcFtpZF07XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRpZiAobW9kKSB7XHJcblx0XHRcdFx0XHRcdFx0Ly8gSG9sZCBvbiB0byBsaXN0ZW5lcnMgaW4gY2FzZSB0aGUgbW9kdWxlIHdpbGwgYmUgYXR0ZW1wdGVkIHRvIGJlIHJlbG9hZGVkIHVzaW5nIGEgXHJcblx0XHRcdFx0XHRcdFx0Ly8gZGlmZmVyZW50IGNvbmZpZy5cclxuXHRcdFx0XHRcdFx0XHRpZiAobW9kLmV2ZW50cy5kZWZpbmVkKSB7XHJcblx0XHRcdFx0XHRcdFx0XHR1bmRlZkV2ZW50c1tpZF0gPSBtb2QuZXZlbnRzO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHRjbGVhblJlZ2lzdHJ5KGlkKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0cmV0dXJuIGxvY2FsUmVxdWlyZTtcclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdC8qKlxyXG5cdFx0XHQgKiBDYWxsZWQgdG8gZW5hYmxlIGEgbW9kdWxlIGlmIGl0IGlzIHN0aWxsIGluIHRoZSByZWdpc3RyeSBhd2FpdGluZyBlbmFibGVtZW50LiBBIHNlY29uZCBhcmcsIHBhcmVudCwgdGhlIFxyXG5cdFx0XHQgKiBwYXJlbnQgbW9kdWxlLCBpcyBwYXNzZWQgaW4gZm9yIGNvbnRleHQsIHdoZW4gdGhpcyBtZXRob2QgaXMgb3ZlcnJpZGRlbiBieSB0aGUgb3B0aW1pemVyLiBOb3Qgc2hvd24gaGVyZSBcclxuXHRcdFx0ICogdG8ga2VlcCBjb2RlIGNvbXBhY3QuXHJcblx0XHRcdCAqL1xyXG5cdFx0XHRlbmFibGU6IGZ1bmN0aW9uKGRlcE1hcCkge1xyXG5cdFx0XHRcdGxldCBtb2QgPSBnZXRPd24ocmVnaXN0cnksIGRlcE1hcC5pZCk7XHJcblx0XHRcdFx0aWYgKG1vZCkge1xyXG5cdFx0XHRcdFx0Z2V0TW9kdWxlKGRlcE1hcCkuZW5hYmxlKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LFxyXG5cdFx0XHRcclxuXHRcdFx0LyoqXHJcblx0XHRcdCAqIEludGVybmFsIG1ldGhvZCB1c2VkIGJ5IGVudmlyb25tZW50IGFkYXB0ZXJzIHRvIGNvbXBsZXRlIGEgbG9hZCBldmVudC5cclxuXHRcdFx0ICogXHJcblx0XHRcdCAqIEEgbG9hZCBldmVudCBjb3VsZCBiZSBhIHNjcmlwdCBsb2FkIG9yIGp1c3QgYSBsb2FkIHBhc3MgZnJvbSBhIHN5bmNocm9ub3VzIGxvYWQgY2FsbC4gXHJcblx0XHRcdCAqIFxyXG5cdFx0XHQgKiBAcGFyYW0ge1N0cmluZ30gbW9kdWxlTmFtZSBUaGUgbmFtZSBvZiB0aGUgbW9kdWxlIHRvIHBvdGVudGlhbGx5IGNvbXBsZXRlLlxyXG5cdFx0XHQgKi9cclxuXHRcdFx0Y29tcGxldGVMb2FkOiBmdW5jdGlvbihtb2R1bGVOYW1lKSB7XHJcblx0XHRcdFx0bGV0IGZvdW5kOyBcclxuXHRcdFx0XHRsZXQgYXJnczsgXHJcblx0XHRcdFx0bGV0IG1vZDtcclxuXHRcdFx0XHRsZXQgc2hpbSA9IGdldE93bihjb25maWcuc2hpbSwgbW9kdWxlTmFtZSkgfHwge307XHJcblx0XHRcdFx0bGV0IHNoRXhwb3J0cyA9IHNoaW0uZXhwb3J0cztcclxuXHRcdFx0XHRcclxuXHRcdFx0XHR0YWtlR2xvYmFsUXVldWUoKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHR3aGlsZSAoZGVmUXVldWUubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRhcmdzID0gZGVmUXVldWUuc2hpZnQoKTtcclxuXHRcdFx0XHRcdGlmIChhcmdzWzBdID09PSBudWxsKSB7XHJcblx0XHRcdFx0XHRcdGFyZ3NbMF0gPSBtb2R1bGVOYW1lO1xyXG5cdFx0XHRcdFx0XHQvLyBJZiBhbHJlYWR5IGZvdW5kIGFuIGFub255bW91cyBtb2R1bGUgYW5kIGJvdW5kIGl0IHRvIHRoaXMgbmFtZSwgdGhlbiB0aGlzIGlzIHNvbWUgb3RoZXIgYW5vbiBcclxuXHRcdFx0XHRcdFx0Ly8gbW9kdWxlIHdhaXRpbmcgZm9yIGl0cyBjb21wbGV0ZUxvYWQgdG8gZmlyZS5cclxuXHRcdFx0XHRcdFx0aWYgKGZvdW5kKSB7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0Zm91bmQgPSB0cnVlO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmIChhcmdzWzBdID09PSBtb2R1bGVOYW1lKSB7XHJcblx0XHRcdFx0XHRcdC8vIEZvdW5kIG1hdGNoaW5nIGRlZmluZSBjYWxsIGZvciB0aGlzIHNjcmlwdCFcclxuXHRcdFx0XHRcdFx0Zm91bmQgPSB0cnVlO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRjYWxsR2V0TW9kdWxlKGFyZ3MpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRjb250ZXh0LmRlZlF1ZXVlTWFwID0ge307XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gRG8gdGhpcyBhZnRlciB0aGUgY3ljbGUgb2YgY2FsbEdldE1vZHVsZSBpbiBjYXNlIHRoZSByZXN1bHRcclxuXHRcdFx0XHQvLyBvZiB0aG9zZSBjYWxscy9pbml0IGNhbGxzIGNoYW5nZXMgdGhlIHJlZ2lzdHJ5LlxyXG5cdFx0XHRcdG1vZCA9IGdldE93bihyZWdpc3RyeSwgbW9kdWxlTmFtZSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKCFmb3VuZCAmJiAhaGFzUHJvcChkZWZpbmVkLCBtb2R1bGVOYW1lKSAmJiBtb2QgJiYgIW1vZC5pbml0ZWQpIHtcclxuXHRcdFx0XHRcdGlmIChjb25maWcuZW5mb3JjZURlZmluZSAmJiAoIXNoRXhwb3J0cyB8fCAhZ2V0R2xvYmFsKHNoRXhwb3J0cykpKSB7XHJcblx0XHRcdFx0XHRcdGlmIChoYXNQYXRoRmFsbGJhY2sobW9kdWxlTmFtZSkpIHtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0cmV0dXJuIG9uRXJyb3IobWFrZUVycm9yKCdub2RlZmluZScsXHJcblx0XHRcdFx0XHRcdFx0XHQnTm8gZGVmaW5lIGNhbGwgZm9yICcgKyBtb2R1bGVOYW1lLFxyXG5cdFx0XHRcdFx0XHRcdFx0bnVsbCxcclxuXHRcdFx0XHRcdFx0XHRcdFttb2R1bGVOYW1lXSkpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHQvLyBBIHNjcmlwdCB0aGF0IGRvZXMgbm90IGNhbGwgZGVmaW5lKCksIHNvIGp1c3Qgc2ltdWxhdGUgdGhlIGNhbGwgZm9yIGl0LlxyXG5cdFx0XHRcdFx0XHRjYWxsR2V0TW9kdWxlKFttb2R1bGVOYW1lLCAoc2hpbS5kZXBzIHx8IFtdKSwgc2hpbS5leHBvcnRzRm5dKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Y2hlY2tMb2FkZWQoKTtcclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdC8qKlxyXG5cdFx0XHQgKiBDb252ZXJ0cyBhIG1vZHVsZSBuYW1lIHRvIGEgZmlsZSBwYXRoLiBcclxuXHRcdFx0ICogXHJcblx0XHRcdCAqIFN1cHBvcnRzIGNhc2VzIHdoZXJlIG1vZHVsZU5hbWUgbWF5IGFjdHVhbGx5IGJlIGp1c3QgYW4gVVJMLiBcclxuXHRcdFx0ICogXHJcblx0XHRcdCAqIE5vdGUgdGhhdCBpdCAqKmRvZXMgbm90KiogY2FsbCBub3JtYWxpemUgb24gdGhlIG1vZHVsZU5hbWUsIGl0IGlzIGFzc3VtZWQgdG8gaGF2ZSBhbHJlYWR5IGJlZW4gXHJcblx0XHRcdCAqIG5vcm1hbGl6ZWQuIFRoaXMgaXMgYW4gaW50ZXJuYWwgQVBJLCBub3QgYSBwdWJsaWMgb25lLiBVc2UgdG9VcmwgZm9yIHRoZSBwdWJsaWMgQVBJLlxyXG5cdFx0XHQgKi9cclxuXHRcdFx0bmFtZVRvVXJsOiBmdW5jdGlvbihtb2R1bGVOYW1lLCBleHQsIHNraXBFeHQpIHtcclxuXHRcdFx0XHRsZXQgcGF0aHM7IFxyXG5cdFx0XHRcdGxldCBzeW1zOyBcclxuXHRcdFx0XHRsZXQgaTsgXHJcblx0XHRcdFx0bGV0IHBhcmVudE1vZHVsZTsgXHJcblx0XHRcdFx0bGV0IHVybDtcclxuXHRcdFx0XHRsZXQgcGFyZW50UGF0aCwgYnVuZGxlSWQ7XHJcblx0XHRcdFx0bGV0IHBrZ01haW4gPSBnZXRPd24oY29uZmlnLnBrZ3MsIG1vZHVsZU5hbWUpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChwa2dNYWluKSB7XHJcblx0XHRcdFx0XHRtb2R1bGVOYW1lID0gcGtnTWFpbjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0YnVuZGxlSWQgPSBnZXRPd24oYnVuZGxlc01hcCwgbW9kdWxlTmFtZSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKGJ1bmRsZUlkKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gY29udGV4dC5uYW1lVG9VcmwoYnVuZGxlSWQsIGV4dCwgc2tpcEV4dCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIElmIGEgY29sb24gaXMgaW4gdGhlIFVSTCwgaXQgaW5kaWNhdGVzIGEgcHJvdG9jb2wgaXMgdXNlZCBhbmQgaXQgaXMganVzdCBhbiBVUkwgdG8gYSBmaWxlLCBvciBpZiBpdCBcclxuXHRcdFx0XHQvLyBzdGFydHMgd2l0aCBhIHNsYXNoLCBjb250YWlucyBhIHF1ZXJ5IGFyZyAoaS5lLiA/KSBvciBlbmRzIHdpdGggLmpzLCB0aGVuIGFzc3VtZSB0aGUgdXNlciBtZWFudCB0byBcclxuXHRcdFx0XHQvLyB1c2UgYW4gdXJsIGFuZCBub3QgYSBtb2R1bGUgaWQuIFRoZSBzbGFzaCBpcyBpbXBvcnRhbnQgZm9yIHByb3RvY29sLWxlc3MgVVJMcyBhcyB3ZWxsIGFzIGZ1bGwgcGF0aHMuXHJcblx0XHRcdFx0aWYgKHJlcS5qc0V4dFJlZ0V4cC50ZXN0KG1vZHVsZU5hbWUpKSB7XHJcblx0XHRcdFx0XHQvLyBKdXN0IGEgcGxhaW4gcGF0aCwgbm90IG1vZHVsZSBuYW1lIGxvb2t1cCwgc28ganVzdCByZXR1cm4gaXQuIEFkZCBleHRlbnNpb24gaWYgaXQgaXMgaW5jbHVkZWQuIFxyXG5cdFx0XHRcdFx0Ly8gVGhpcyBpcyBhIGJpdCB3b25reSwgb25seSBub24tLmpzIHRoaW5ncyBwYXNzIGFuIGV4dGVuc2lvbiwgdGhpcyBtZXRob2QgcHJvYmFibHkgbmVlZHMgdG8gYmUgXHJcblx0XHRcdFx0XHQvLyByZXdvcmtlZC5cclxuXHRcdFx0XHRcdHVybCA9IG1vZHVsZU5hbWUgKyAoZXh0IHx8ICcnKTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0Ly8gQSBtb2R1bGUgdGhhdCBuZWVkcyB0byBiZSBjb252ZXJ0ZWQgdG8gYSBwYXRoLlxyXG5cdFx0XHRcdFx0cGF0aHMgPSBjb25maWcucGF0aHM7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdHN5bXMgPSBtb2R1bGVOYW1lLnNwbGl0KCcvJyk7XHJcblx0XHRcdFx0XHQvLyBGb3IgZWFjaCBtb2R1bGUgbmFtZSBzZWdtZW50LCBzZWUgaWYgdGhlcmUgaXMgYSBwYXRoIHJlZ2lzdGVyZWQgZm9yIGl0LiBTdGFydCB3aXRoIG1vc3Qgc3BlY2lmaWMgXHJcblx0XHRcdFx0XHQvLyBuYW1lIGFuZCB3b3JrIHVwIGZyb20gaXQuXHJcblx0XHRcdFx0XHRmb3IgKGkgPSBzeW1zLmxlbmd0aDsgaSA+IDA7IGkgLT0gMSkge1xyXG5cdFx0XHRcdFx0XHRwYXJlbnRNb2R1bGUgPSBzeW1zLnNsaWNlKDAsIGkpLmpvaW4oJy8nKTtcclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdHBhcmVudFBhdGggPSBnZXRPd24ocGF0aHMsIHBhcmVudE1vZHVsZSk7XHJcblx0XHRcdFx0XHRcdGlmIChwYXJlbnRQYXRoKSB7XHJcblx0XHRcdFx0XHRcdFx0Ly8gSWYgYW4gYXJyYXksIGl0IG1lYW5zIHRoZXJlIGFyZSBhIGZldyBjaG9pY2VzLCBDaG9vc2UgdGhlIG9uZSB0aGF0IGlzIGRlc2lyZWQuXHJcblx0XHRcdFx0XHRcdFx0aWYgKGlzQXJyYXkocGFyZW50UGF0aCkpIHtcclxuXHRcdFx0XHRcdFx0XHRcdHBhcmVudFBhdGggPSBwYXJlbnRQYXRoWzBdO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRzeW1zLnNwbGljZSgwLCBpLCBwYXJlbnRQYXRoKTtcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBKb2luIHRoZSBwYXRoIHBhcnRzIHRvZ2V0aGVyLCB0aGVuIGZpZ3VyZSBvdXQgaWYgYmFzZVVybCBpcyBuZWVkZWQuXHJcblx0XHRcdFx0XHR1cmwgPSBzeW1zLmpvaW4oJy8nKTtcclxuXHRcdFx0XHRcdHVybCArPSAoZXh0IHx8ICgvXmRhdGFcXDp8XFw/Ly50ZXN0KHVybCkgfHwgc2tpcEV4dCA/ICcnIDogJy5qcycpKTtcclxuXHRcdFx0XHRcdHVybCA9ICh1cmwuY2hhckF0KDApID09PSAnLycgfHwgdXJsLm1hdGNoKC9eW1xcd1xcK1xcLlxcLV0rOi8pID8gJycgOiBjb25maWcuYmFzZVVybCkgKyB1cmw7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdHJldHVybiBjb25maWcudXJsQXJncyA/IHVybCArXHJcblx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICgodXJsLmluZGV4T2YoJz8nKSA9PT0gLTEgPyAnPycgOiAnJicpICtcclxuXHRcdFx0XHQgICAgICAgICAgICAgICAgICAgICAgY29uZmlnLnVybEFyZ3MpIDogdXJsO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRcclxuXHRcdFx0Ly8gRGVsZWdhdGVzIHRvIHJlcS5sb2FkLiBCcm9rZW4gb3V0IGFzIGEgc2VwYXJhdGUgZnVuY3Rpb24gdG8gYWxsb3cgb3ZlcnJpZGluZyBpbiB0aGUgb3B0aW1pemVyLlxyXG5cdFx0XHRsb2FkOiBmdW5jdGlvbihpZCwgdXJsKSB7XHJcblx0XHRcdFx0cmVxLmxvYWQoY29udGV4dCwgaWQsIHVybCk7XHJcblx0XHRcdH0sXHJcblx0XHRcdFxyXG5cdFx0XHQvKipcclxuXHRcdFx0ICogRXhlY3V0ZXMgYSBtb2R1bGUgY2FsbGJhY2sgZnVuY3Rpb24uIFxyXG5cdFx0XHQgKiBcclxuXHRcdFx0ICogQnJva2VuIG91dCBhcyBhIHNlcGFyYXRlIGZ1bmN0aW9uIHNvbGVseSB0byBhbGxvdyB0aGUgYnVpbGQgc3lzdGVtIHRvIHNlcXVlbmNlIHRoZSBmaWxlcyBpbiB0aGUgYnVpbHRcclxuXHRcdFx0ICogbGF5ZXIgaW4gdGhlIHJpZ2h0IHNlcXVlbmNlLlxyXG5cdFx0XHQgKlxyXG5cdFx0XHQgKiBAcHJpdmF0ZVxyXG5cdFx0XHQgKi9cclxuXHRcdFx0ZXhlY0NiOiBmdW5jdGlvbihuYW1lLCBjYWxsYmFjaywgYXJncywgZXhwb3J0cykge1xyXG5cdFx0XHRcdHJldHVybiBjYWxsYmFjay5hcHBseShleHBvcnRzLCBhcmdzKTtcclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdC8qKlxyXG5cdFx0XHQgKiBDYWxsYmFjayBmb3Igc2NyaXB0IGxvYWRzLCB1c2VkIHRvIGNoZWNrIHN0YXR1cyBvZiBsb2FkaW5nLlxyXG5cdFx0XHQgKlxyXG5cdFx0XHQgKiBAcGFyYW0ge0V2ZW50fSBldnQgdGhlIGV2ZW50IGZyb20gdGhlIGJyb3dzZXIgZm9yIHRoZSBzY3JpcHQgdGhhdCB3YXMgbG9hZGVkLlxyXG5cdFx0XHQgKi9cclxuXHRcdFx0b25TY3JpcHRMb2FkOiBmdW5jdGlvbihldnQpIHtcclxuXHRcdFx0XHQvLyBVc2luZyBjdXJyZW50VGFyZ2V0IGluc3RlYWQgb2YgdGFyZ2V0IGZvciBGaXJlZm94IDIuMCdzIHNha2UuIE5vdFxyXG5cdFx0XHRcdC8vIGFsbCBvbGQgYnJvd3NlcnMgd2lsbCBiZSBzdXBwb3J0ZWQsIGJ1dCB0aGlzIG9uZSB3YXMgZWFzeSBlbm91Z2hcclxuXHRcdFx0XHQvLyB0byBzdXBwb3J0IGFuZCBzdGlsbCBtYWtlcyBzZW5zZS5cclxuXHRcdFx0XHRpZiAoZXZ0LnR5cGUgPT09ICdsb2FkJyB8fFxyXG5cdFx0XHRcdFx0KHJlYWR5UmVnRXhwLnRlc3QoKGV2dC5jdXJyZW50VGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50KS5yZWFkeVN0YXRlKSkpIHtcclxuXHRcdFx0XHRcdC8vIFJlc2V0IGludGVyYWN0aXZlIHNjcmlwdCBzbyBhIHNjcmlwdCBub2RlIGlzIG5vdCBoZWxkIG9udG8gZm9yXHJcblx0XHRcdFx0XHQvLyB0byBsb25nLlxyXG5cdFx0XHRcdFx0aW50ZXJhY3RpdmVTY3JpcHQgPSBudWxsO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQvLyBQdWxsIG91dCB0aGUgbmFtZSBvZiB0aGUgbW9kdWxlIGFuZCB0aGUgY29udGV4dC5cclxuXHRcdFx0XHRcdGxldCBkYXRhID0gZ2V0U2NyaXB0RGF0YShldnQpO1xyXG5cdFx0XHRcdFx0Y29udGV4dC5jb21wbGV0ZUxvYWQoZGF0YS5pZCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LFxyXG5cdFx0XHRcclxuXHRcdFx0LyoqXHJcblx0XHRcdCAqIENhbGxiYWNrIGZvciBzY3JpcHQgZXJyb3JzLlxyXG5cdFx0XHQgKi9cclxuXHRcdFx0b25TY3JpcHRFcnJvcjogZnVuY3Rpb24oZXZ0KSB7XHJcblx0XHRcdFx0bGV0IGRhdGEgPSBnZXRTY3JpcHREYXRhKGV2dCk7XHJcblx0XHRcdFx0aWYgKCFoYXNQYXRoRmFsbGJhY2soZGF0YS5pZCkpIHtcclxuXHRcdFx0XHRcdGxldCBwYXJlbnRzID0gW107XHJcblx0XHRcdFx0XHRlYWNoUHJvcChyZWdpc3RyeSwgZnVuY3Rpb24odmFsdWUsIGtleSkge1xyXG5cdFx0XHRcdFx0XHRpZiAoa2V5LmluZGV4T2YoJ19AcicpICE9PSAwKSB7XHJcblx0XHRcdFx0XHRcdFx0ZWFjaCh2YWx1ZS5kZXBNYXBzLCBmdW5jdGlvbihkZXBNYXApIHtcclxuXHRcdFx0XHRcdFx0XHRcdGlmIChkZXBNYXAuaWQgPT09IGRhdGEuaWQpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0cGFyZW50cy5wdXNoKGtleSk7XHJcblx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRyZXR1cm4gb25FcnJvcihtYWtlRXJyb3IoJ3NjcmlwdGVycm9yJywgJ1NjcmlwdCBlcnJvciBmb3IgXCInICsgZGF0YS5pZCArXHJcblx0XHRcdFx0XHRcdChwYXJlbnRzLmxlbmd0aCA/XHJcblx0XHRcdFx0XHRcdCAnXCIsIG5lZWRlZCBieTogJyArIHBhcmVudHMuam9pbignLCAnKSA6XHJcblx0XHRcdFx0XHRcdCAnXCInKSwgZXZ0LCBbZGF0YS5pZF0pKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblx0XHRcclxuXHRcdGNvbnRleHQucmVxdWlyZSA9IGNvbnRleHQubWFrZVJlcXVpcmUoKTtcclxuXHRcdHJldHVybiBjb250ZXh0O1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBNYWluIGVudHJ5IHBvaW50LlxyXG5cdCAqXHJcblx0ICogSWYgdGhlIG9ubHkgYXJndW1lbnQgdG8gcmVxdWlyZSBpcyBhIHN0cmluZywgdGhlbiB0aGUgbW9kdWxlIHRoYXQgaXMgcmVwcmVzZW50ZWQgYnkgdGhhdCBzdHJpbmcgaXMgZmV0Y2hlZCBmb3IgXHJcblx0ICogdGhlIGFwcHJvcHJpYXRlIGNvbnRleHQuXHJcblx0ICpcclxuXHQgKiBJZiB0aGUgZmlyc3QgYXJndW1lbnQgaXMgYW4gYXJyYXksIHRoZW4gaXQgd2lsbCBiZSB0cmVhdGVkIGFzIGFuIGFycmF5IG9mIGRlcGVuZGVuY3kgc3RyaW5nIG5hbWVzIHRvIGZldGNoLiBBbiBcclxuXHQgKiBvcHRpb25hbCBmdW5jdGlvbiBjYWxsYmFjayBjYW4gYmUgc3BlY2lmaWVkIHRvIGV4ZWN1dGUgd2hlbiBhbGwgb2YgdGhvc2UgZGVwZW5kZW5jaWVzIGFyZSBhdmFpbGFibGUuXHJcblx0ICpcclxuXHQgKiBNYWtlIGEgbG9jYWwgcmVxIHZhcmlhYmxlIHRvIGhlbHAgQ2FqYSBjb21wbGlhbmNlIChpdCBhc3N1bWVzIHRoaW5ncyBvbiBhIHJlcXVpcmUgdGhhdCBhcmUgbm90IHN0YW5kYXJkaXplZCksIFxyXG5cdCAqIGFuZCB0byBnaXZlIGEgc2hvcnQgbmFtZSBmb3IgbWluaWZpY2F0aW9uL2xvY2FsIHNjb3BlIHVzZS5cclxuXHQgKi9cclxuXHRyZXEgPSB3aW5kb3cucmVxdWlyZWpzID0gZnVuY3Rpb24oZGVwcywgY2FsbGJhY2ssIGVycmJhY2ssIG9wdGlvbmFsKSB7XHJcblx0XHQvLyBGaW5kIHRoZSByaWdodCBjb250ZXh0LCB1c2UgZGVmYXVsdFxyXG5cdFx0bGV0IGNvbnRleHQ7IFxyXG5cdFx0bGV0IGNvbmZpZztcclxuXHRcdGxldCBjb250ZXh0TmFtZSA9IGRlZkNvbnRleHROYW1lO1xyXG5cdFx0XHJcblx0XHQvLyBEZXRlcm1pbmUgaWYgaGF2ZSBjb25maWcgb2JqZWN0IGluIHRoZSBjYWxsLlxyXG5cdFx0aWYgKCFpc0FycmF5KGRlcHMpICYmIHR5cGVvZiBkZXBzICE9PSAnc3RyaW5nJykge1xyXG5cdFx0XHQvLyBkZXBzIGlzIGEgY29uZmlnIG9iamVjdFxyXG5cdFx0XHRjb25maWcgPSBkZXBzO1xyXG5cdFx0XHRpZiAoaXNBcnJheShjYWxsYmFjaykpIHtcclxuXHRcdFx0XHQvLyBBZGp1c3QgYXJncyBpZiB0aGVyZSBhcmUgZGVwZW5kZW5jaWVzXHJcblx0XHRcdFx0ZGVwcyA9IGNhbGxiYWNrO1xyXG5cdFx0XHRcdGNhbGxiYWNrID0gZXJyYmFjaztcclxuXHRcdFx0XHRlcnJiYWNrID0gb3B0aW9uYWw7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0ZGVwcyA9IFtdO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdGlmIChjb25maWcgJiYgY29uZmlnLmNvbnRleHQpIHtcclxuXHRcdFx0Y29udGV4dE5hbWUgPSBjb25maWcuY29udGV4dDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Y29udGV4dCA9IGdldE93bihjb250ZXh0cywgY29udGV4dE5hbWUpO1xyXG5cdFx0aWYgKCFjb250ZXh0KSB7XHJcblx0XHRcdGNvbnRleHQgPSBjb250ZXh0c1tjb250ZXh0TmFtZV0gPSByZXEucy5uZXdDb250ZXh0KGNvbnRleHROYW1lKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0aWYgKGNvbmZpZykge1xyXG5cdFx0XHRjb250ZXh0LmNvbmZpZ3VyZShjb25maWcpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRyZXR1cm4gY29udGV4dC5yZXF1aXJlKGRlcHMsIGNhbGxiYWNrLCBlcnJiYWNrKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFN1cHBvcnQgcmVxdWlyZS5jb25maWcoKSB0byBtYWtlIGl0IGVhc2llciB0byBjb29wZXJhdGUgd2l0aCBvdGhlciBBTUQgbG9hZGVycyBvbiBnbG9iYWxseSBhZ3JlZWQgbmFtZXMuXHJcblx0ICovXHJcblx0cmVxLmNvbmZpZyA9IGZ1bmN0aW9uKGNvbmZpZykge1xyXG5cdFx0cmV0dXJuIHJlcShjb25maWcpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogRXhlY3V0ZSBzb21ldGhpbmcgYWZ0ZXIgdGhlIGN1cnJlbnQgdGljayBvZiB0aGUgZXZlbnQgbG9vcC4gXHJcblx0ICogXHJcblx0ICogT3ZlcnJpZGUgZm9yIG90aGVyIGVudnMgdGhhdCBoYXZlIGEgYmV0dGVyIHNvbHV0aW9uIHRoYW4gc2V0VGltZW91dC5cclxuXHQgKiBcclxuXHQgKiBAcGFyYW0gIHtGdW5jdGlvbn0gZm4gZnVuY3Rpb24gdG8gZXhlY3V0ZSBsYXRlci5cclxuXHQgKi9cclxuXHRyZXEubmV4dFRpY2sgPSB0eXBlb2Ygc2V0VGltZW91dCAhPT0gJ3VuZGVmaW5lZCcgPyBmdW5jdGlvbihmbikge1xyXG5cdFx0c2V0VGltZW91dChmbiwgNCk7XHJcblx0fSA6IGZ1bmN0aW9uKGZuKSB7XHJcblx0XHRmbigpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogRXhwb3J0IHJlcXVpcmUgYXMgYSBnbG9iYWwsIGJ1dCBvbmx5IGlmIGl0IGRvZXMgbm90IGFscmVhZHkgZXhpc3QuXHJcblx0ICovXHJcblx0aWYgKCF3aW5kb3cucmVxdWlyZSkge1xyXG5cdFx0d2luZG93LnJlcXVpcmUgPSByZXE7XHJcblx0fVxyXG5cdFxyXG5cdHJlcS52ZXJzaW9uID0gdmVyc2lvbjtcclxuXHRcclxuXHQvLyBVc2VkIHRvIGZpbHRlciBvdXQgZGVwZW5kZW5jaWVzIHRoYXQgYXJlIGFscmVhZHkgcGF0aHMuXHJcblx0cmVxLmpzRXh0UmVnRXhwID0gL15cXC98OnxcXD98XFwuanMkLztcclxuXHRyZXEuaXNCcm93c2VyID0gaXNCcm93c2VyO1xyXG5cdHMgPSByZXEucyA9IHtcclxuXHRcdGNvbnRleHRzOiBjb250ZXh0cyxcclxuXHRcdG5ld0NvbnRleHQ6IG5ld0NvbnRleHRcclxuXHR9O1xyXG5cdFxyXG5cdC8vIENyZWF0ZSBkZWZhdWx0IGNvbnRleHQuXHJcblx0cmVxKHt9KTtcclxuXHRcclxuXHQvLyBFeHBvcnRzIHNvbWUgY29udGV4dC1zZW5zaXRpdmUgbWV0aG9kcyBvbiBnbG9iYWwgcmVxdWlyZS5cclxuXHRlYWNoKFtcclxuXHRcdCd0b1VybCcsXHJcblx0XHQndW5kZWYnLFxyXG5cdFx0J2RlZmluZWQnLFxyXG5cdFx0J3NwZWNpZmllZCdcclxuXHRdLCBmdW5jdGlvbihwcm9wKSB7XHJcblx0XHQvLyAgUmVmZXJlbmNlIGZyb20gY29udGV4dHMgaW5zdGVhZCBvZiBlYXJseSBiaW5kaW5nIHRvIGRlZmF1bHQgY29udGV4dCxcclxuXHRcdC8vIHNvIHRoYXQgZHVyaW5nIGJ1aWxkcywgdGhlIGxhdGVzdCBpbnN0YW5jZSBvZiB0aGUgZGVmYXVsdCBjb250ZXh0XHJcblx0XHQvLyB3aXRoIGl0cyBjb25maWcgZ2V0cyB1c2VkLlxyXG5cdFx0cmVxW3Byb3BdID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdGxldCBjdHggPSBjb250ZXh0c1tkZWZDb250ZXh0TmFtZV07XHJcblx0XHRcdHJldHVybiBjdHgucmVxdWlyZVtwcm9wXS5hcHBseShjdHgsIGFyZ3VtZW50cyk7XHJcblx0XHR9O1xyXG5cdH0pO1xyXG5cdFxyXG5cdGlmIChpc0Jyb3dzZXIpIHtcclxuXHRcdGhlYWQgPSBzLmhlYWQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnaGVhZCcpWzBdO1xyXG5cdFx0Ly8gSWYgQkFTRSB0YWcgaXMgaW4gcGxheSwgdXNpbmcgYXBwZW5kQ2hpbGQgaXMgYSBwcm9ibGVtIGZvciBJRTYuXHJcblx0XHQvLyBXaGVuIHRoYXQgYnJvd3NlciBkaWVzLCB0aGlzIGNhbiBiZSByZW1vdmVkLiBEZXRhaWxzIGluIHRoaXMgalF1ZXJ5IGJ1ZzpcclxuXHRcdC8vIGh0dHA6Ly9kZXYuanF1ZXJ5LmNvbS90aWNrZXQvMjcwOVxyXG5cdFx0YmFzZUVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnYmFzZScpWzBdO1xyXG5cdFx0aWYgKGJhc2VFbGVtZW50KSB7XHJcblx0XHRcdGhlYWQgPSBzLmhlYWQgPSBiYXNlRWxlbWVudC5wYXJlbnROb2RlO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBBbnkgZXJyb3JzIHRoYXQgcmVxdWlyZSBleHBsaWNpdGx5IGdlbmVyYXRlcyB3aWxsIGJlIHBhc3NlZCB0byB0aGlzIGZ1bmN0aW9uLiBcclxuXHQgKiBcclxuXHQgKiBJbnRlcmNlcHQvb3ZlcnJpZGUgaXQgaWYgeW91IHdhbnQgY3VzdG9tIGVycm9yIGhhbmRsaW5nLlxyXG5cdCAqIFxyXG5cdCAqIEBwYXJhbSB7RXJyb3J9IGVyciB0aGUgZXJyb3Igb2JqZWN0LlxyXG5cdCAqL1xyXG5cdHJlcS5vbkVycm9yID0gZGVmYXVsdE9uRXJyb3I7XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ3JlYXRlcyB0aGUgbm9kZSBmb3IgdGhlIGxvYWQgY29tbWFuZC4gT25seSB1c2VkIGluIGJyb3dzZXIgZW52cy5cclxuXHQgKi9cclxuXHRyZXEuY3JlYXRlTm9kZSA9IGZ1bmN0aW9uKGNvbmZpZywgbW9kdWxlTmFtZSwgdXJsKSB7XHJcblx0XHRjb25zdCBub2RlID0gY29uZmlnLnhodG1sID9cclxuXHRcdCAgICAgICAgICAgZG9jdW1lbnQuY3JlYXRlRWxlbWVudE5TKCdodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sJywgJ2h0bWw6c2NyaXB0JykgOlxyXG5cdFx0ICAgICAgICAgICBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuXHRcdG5vZGUudHlwZSA9IGNvbmZpZy5zY3JpcHRUeXBlIHx8ICd0ZXh0L2phdmFzY3JpcHQnO1xyXG5cdFx0bm9kZS5jaGFyc2V0ID0gJ3V0Zi04JztcclxuXHRcdG5vZGUuYXN5bmMgPSB0cnVlO1xyXG5cdFx0cmV0dXJuIG5vZGU7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBEb2VzIHRoZSByZXF1ZXN0IHRvIGxvYWQgYSBtb2R1bGUgZm9yIHRoZSBicm93c2VyIGNhc2UuXHJcblx0ICogXHJcblx0ICogTWFrZSB0aGlzIGEgc2VwYXJhdGUgZnVuY3Rpb24gdG8gYWxsb3cgb3RoZXIgZW52aXJvbm1lbnRzIHRvIG92ZXJyaWRlIGl0LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IGNvbnRleHQgdGhlIHJlcXVpcmUgY29udGV4dCB0byBmaW5kIHN0YXRlLlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBtb2R1bGVOYW1lIHRoZSBuYW1lIG9mIHRoZSBtb2R1bGUuXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHVybCB0aGUgVVJMIHRvIHRoZSBtb2R1bGUuXHJcblx0ICovXHJcblx0cmVxLmxvYWQgPSBmdW5jdGlvbihjb250ZXh0LCBtb2R1bGVOYW1lLCB1cmwpIHtcclxuXHRcdGxldCBjb25maWcgPSAoY29udGV4dCAmJiBjb250ZXh0LmNvbmZpZykgfHwge307XHJcblx0XHRsZXQgbm9kZTtcclxuXHRcdFxyXG5cdFx0aWYgKGlzQnJvd3Nlcikge1xyXG5cdFx0XHQvLyBJbiB0aGUgYnJvd3NlciBzbyB1c2UgYSBzY3JpcHQgdGFnXHJcblx0XHRcdG5vZGUgPSByZXEuY3JlYXRlTm9kZShjb25maWcsIG1vZHVsZU5hbWUsIHVybCk7XHJcblx0XHRcdGlmIChjb25maWcub25Ob2RlQ3JlYXRlZCkge1xyXG5cdFx0XHRcdGNvbmZpZy5vbk5vZGVDcmVhdGVkKG5vZGUsIGNvbmZpZywgbW9kdWxlTmFtZSwgdXJsKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0bm9kZS5zZXRBdHRyaWJ1dGUoJ2RhdGEtcmVxdWlyZWNvbnRleHQnLCBjb250ZXh0LmNvbnRleHROYW1lKTtcclxuXHRcdFx0bm9kZS5zZXRBdHRyaWJ1dGUoJ2RhdGEtcmVxdWlyZW1vZHVsZScsIG1vZHVsZU5hbWUpO1xyXG5cdFx0XHRcclxuXHRcdFx0Ly8gU2V0IHVwIGxvYWQgbGlzdGVuZXIuIFRlc3QgYXR0YWNoRXZlbnQgZmlyc3QgYmVjYXVzZSBJRTkgaGFzIGEgc3VidGxlIGlzc3VlIGluIGl0cyBhZGRFdmVudExpc3RlbmVyIGFuZCBcclxuXHRcdFx0Ly8gc2NyaXB0IG9ubG9hZCBmaXJpbmdzIHRoYXQgZG8gbm90IG1hdGNoIHRoZSBiZWhhdmlvciBvZiBhbGwgb3RoZXIgYnJvd3NlcnMgd2l0aCBhZGRFdmVudExpc3RlbmVyIHN1cHBvcnQsIFxyXG5cdFx0XHQvLyB3aGljaCBmaXJlIHRoZSBvbmxvYWQgZXZlbnQgZm9yIGEgc2NyaXB0IHJpZ2h0IGFmdGVyIHRoZSBzY3JpcHQgZXhlY3V0aW9uLiBTZWU6XHJcblx0XHRcdC8vIGh0dHBzOi8vY29ubmVjdC5taWNyb3NvZnQuY29tL0lFL2ZlZWRiYWNrL2RldGFpbHMvNjQ4MDU3L3NjcmlwdC1vbmxvYWQtZXZlbnQtaXMtbm90LWZpcmVkLWltbWVkaWF0ZWx5LWFmdGVyLXNjcmlwdC1leGVjdXRpb25cclxuXHRcdFx0Ly8gVU5GT1JUVU5BVEVMWSBPcGVyYSBpbXBsZW1lbnRzIGF0dGFjaEV2ZW50IGJ1dCBkb2VzIG5vdCBmb2xsb3cgdGhlIHNjcmlwdCBzY3JpcHQgZXhlY3V0aW9uIG1vZGUuXHJcblx0XHRcdGlmIChub2RlLmF0dGFjaEV2ZW50ICYmXHJcblx0XHRcdFx0Ly8gQ2hlY2sgaWYgbm9kZS5hdHRhY2hFdmVudCBpcyBhcnRpZmljaWFsbHkgYWRkZWQgYnkgY3VzdG9tIHNjcmlwdCBvciBuYXRpdmVseSBzdXBwb3J0ZWQgYnkgYnJvd3NlclxyXG5cdFx0XHRcdC8vIHJlYWQgaHR0cHM6Ly9naXRodWIuY29tL2pyYnVya2UvcmVxdWlyZWpzL2lzc3Vlcy8xODdcclxuXHRcdFx0XHQvLyBpZiB3ZSBjYW4gTk9UIGZpbmQgW25hdGl2ZSBjb2RlXSB0aGVuIGl0IG11c3QgTk9UIG5hdGl2ZWx5IHN1cHBvcnRlZC4gaW4gSUU4LCBub2RlLmF0dGFjaEV2ZW50IGRvZXMgXHJcblx0XHRcdFx0Ly8gbm90IGhhdmUgdG9TdHJpbmcoKS4gTm90ZSB0aGUgdGVzdCBmb3IgXCJbbmF0aXZlIGNvZGVcIiB3aXRoIG5vIGNsb3NpbmcgYnJhY2UsIHNlZTpcclxuXHRcdFx0XHQvLyBodHRwczovL2dpdGh1Yi5jb20vanJidXJrZS9yZXF1aXJlanMvaXNzdWVzLzI3M1xyXG5cdFx0XHRcdCEobm9kZS5hdHRhY2hFdmVudC50b1N0cmluZyAmJiBub2RlLmF0dGFjaEV2ZW50LnRvU3RyaW5nKCkuaW5kZXhPZignW25hdGl2ZSBjb2RlJykgPCAwKSAmJlxyXG5cdFx0XHRcdCFpc09wZXJhKSB7XHJcblx0XHRcdFx0Ly8gUHJvYmFibHkgSUUuIElFIChhdCBsZWFzdCA2LTgpIGRvIG5vdCBmaXJlIHNjcmlwdCBvbmxvYWQgcmlnaHQgYWZ0ZXIgZXhlY3V0aW5nIHRoZSBzY3JpcHQsIHNvXHJcblx0XHRcdFx0Ly8gd2UgY2Fubm90IHRpZSB0aGUgYW5vbnltb3VzIGRlZmluZSBjYWxsIHRvIGEgbmFtZS4gSG93ZXZlciwgSUUgcmVwb3J0cyB0aGUgc2NyaXB0IGFzIGJlaW5nIGluIFxyXG5cdFx0XHRcdC8vICdpbnRlcmFjdGl2ZScgcmVhZHlTdGF0ZSBhdCB0aGUgdGltZSBvZiB0aGUgZGVmaW5lIGNhbGwuXHJcblx0XHRcdFx0dXNlSW50ZXJhY3RpdmUgPSB0cnVlO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdG5vZGUuYXR0YWNoRXZlbnQoJ29ucmVhZHlzdGF0ZWNoYW5nZScsIGNvbnRleHQub25TY3JpcHRMb2FkKTtcclxuXHRcdFx0XHQvLyBJdCB3b3VsZCBiZSBncmVhdCB0byBhZGQgYW4gZXJyb3IgaGFuZGxlciBoZXJlIHRvIGNhdGNoIDQwNHMgaW4gSUU5Ky4gSG93ZXZlciwgb25yZWFkeXN0YXRlY2hhbmdlIFxyXG5cdFx0XHRcdC8vIHdpbGwgZmlyZSBiZWZvcmUgdGhlIGVycm9yIGhhbmRsZXIsIHNvIHRoYXQgZG9lcyBub3QgaGVscC4gSWYgYWRkRXZlbnRMaXN0ZW5lciBpcyB1c2VkLCB0aGVuIElFIHdpbGwgXHJcblx0XHRcdFx0Ly8gZmlyZSBlcnJvciBiZWZvcmUgbG9hZCwgYnV0IHdlIGNhbm5vdCB1c2UgdGhhdCBwYXRod2F5IGdpdmVuIHRoZSBjb25uZWN0Lm1pY3Jvc29mdC5jb20gaXNzdWUgXHJcblx0XHRcdFx0Ly8gbWVudGlvbmVkIGFib3ZlIGFib3V0IG5vdCBkb2luZyB0aGUgJ3NjcmlwdCBleGVjdXRlLCB0aGVuIGZpcmUgdGhlIHNjcmlwdCBsb2FkIGV2ZW50IGxpc3RlbmVyIGJlZm9yZSBcclxuXHRcdFx0XHQvLyBleGVjdXRlIG5leHQgc2NyaXB0JyB0aGF0IG90aGVyIGJyb3dzZXJzIGRvLiBCZXN0IGhvcGU6IElFMTAgZml4ZXMgdGhlIGlzc3VlcywgYW5kIHRoZW4gZGVzdHJveXMgYWxsIFxyXG5cdFx0XHRcdC8vIGluc3RhbGxzIG9mIElFIDYtOS5cclxuXHRcdFx0XHQvLyBub2RlLmF0dGFjaEV2ZW50KCdvbmVycm9yJywgY29udGV4dC5vblNjcmlwdEVycm9yKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRub2RlLmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBjb250ZXh0Lm9uU2NyaXB0TG9hZCwgZmFsc2UpO1xyXG5cdFx0XHRcdG5vZGUuYWRkRXZlbnRMaXN0ZW5lcignZXJyb3InLCBjb250ZXh0Lm9uU2NyaXB0RXJyb3IsIGZhbHNlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRub2RlLnNyYyA9IHVybDtcclxuXHRcdFx0XHJcblx0XHRcdC8vIEZvciBzb21lIGNhY2hlIGNhc2VzIGluIElFIDYtOCwgdGhlIHNjcmlwdCBleGVjdXRlcyBiZWZvcmUgdGhlIGVuZCBvZiB0aGUgYXBwZW5kQ2hpbGQgZXhlY3V0aW9uLCBzbyB0byBcclxuXHRcdFx0Ly8gdGllIGFuIGFub255bW91cyBkZWZpbmUgY2FsbCB0byB0aGUgbW9kdWxlIG5hbWUgKHdoaWNoIGlzIHN0b3JlZCBvbiB0aGUgbm9kZSksIGhvbGQgb24gdG8gYSByZWZlcmVuY2UgdG8gXHJcblx0XHRcdC8vIHRoaXMgbm9kZSwgYnV0IGNsZWFyIGFmdGVyIHRoZSBET00gaW5zZXJ0aW9uLlxyXG5cdFx0XHRpZiAoYmFzZUVsZW1lbnQpIHtcclxuXHRcdFx0XHRoZWFkLmluc2VydEJlZm9yZShub2RlLCBiYXNlRWxlbWVudCk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aGVhZC5hcHBlbmRDaGlsZChub2RlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0cmV0dXJuIG5vZGU7XHJcblx0XHR9IGVsc2UgaWYgKGlzV2ViV29ya2VyKSB7XHJcblx0XHRcdHRyeSB7XHJcblx0XHRcdFx0Ly8gSW4gYSB3ZWIgd29ya2VyLCB1c2UgaW1wb3J0U2NyaXB0cy4gVGhpcyBpcyBub3QgYSB2ZXJ5XHJcblx0XHRcdFx0Ly8gZWZmaWNpZW50IHVzZSBvZiBpbXBvcnRTY3JpcHRzLCBpbXBvcnRTY3JpcHRzIHdpbGwgYmxvY2sgdW50aWxcclxuXHRcdFx0XHQvLyBpdHMgc2NyaXB0IGlzIGRvd25sb2FkZWQgYW5kIGV2YWx1YXRlZC4gSG93ZXZlciwgaWYgd2ViIHdvcmtlcnNcclxuXHRcdFx0XHQvLyBhcmUgaW4gcGxheSwgdGhlIGV4cGVjdGF0aW9uIGlzIHRoYXQgYSBidWlsZCBoYXMgYmVlbiBkb25lIHNvXHJcblx0XHRcdFx0Ly8gdGhhdCBvbmx5IG9uZSBzY3JpcHQgbmVlZHMgdG8gYmUgbG9hZGVkIGFueXdheS4gVGhpcyBtYXkgbmVlZFxyXG5cdFx0XHRcdC8vIHRvIGJlIHJlZXZhbHVhdGVkIGlmIG90aGVyIHVzZSBjYXNlcyBiZWNvbWUgY29tbW9uLlxyXG5cdFx0XHRcdGltcG9ydFNjcmlwdHModXJsKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBBY2NvdW50IGZvciBhbm9ueW1vdXMgbW9kdWxlc1xyXG5cdFx0XHRcdGNvbnRleHQuY29tcGxldGVMb2FkKG1vZHVsZU5hbWUpO1xyXG5cdFx0XHR9IGNhdGNoIChlKSB7XHJcblx0XHRcdFx0Y29udGV4dC5vbkVycm9yKG1ha2VFcnJvcignaW1wb3J0c2NyaXB0cycsXHJcblx0XHRcdFx0XHQnaW1wb3J0U2NyaXB0cyBmYWlsZWQgZm9yICcgK1xyXG5cdFx0XHRcdFx0bW9kdWxlTmFtZSArICcgYXQgJyArIHVybCxcclxuXHRcdFx0XHRcdGUsXHJcblx0XHRcdFx0XHRbbW9kdWxlTmFtZV0pKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH07XHJcblx0XHJcblx0Ly8gTG9vayBmb3IgYSBkYXRhLW1haW4gc2NyaXB0IGF0dHJpYnV0ZSwgd2hpY2ggY291bGQgYWxzbyBhZGp1c3QgdGhlIGJhc2VVcmwuXHJcblx0aWYgKGlzQnJvd3NlciAmJiAhY2ZnLnNraXBEYXRhTWFpbikge1xyXG5cdFx0Ly8gRmlndXJlIG91dCBiYXNlVXJsLiBHZXQgaXQgZnJvbSB0aGUgc2NyaXB0IHRhZyB3aXRoIHJlcXVpcmUuanMgaW4gaXQuXHJcblx0XHRlYWNoUmV2ZXJzZShzY3JpcHRzKCksIGZ1bmN0aW9uKHNjcmlwdCkge1xyXG5cdFx0XHQvLyBTZXQgdGhlICdoZWFkJyB3aGVyZSB3ZSBjYW4gYXBwZW5kIGNoaWxkcmVuIGJ5XHJcblx0XHRcdC8vIHVzaW5nIHRoZSBzY3JpcHQncyBwYXJlbnQuXHJcblx0XHRcdGlmICghaGVhZCkge1xyXG5cdFx0XHRcdGhlYWQgPSBzY3JpcHQucGFyZW50Tm9kZTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0Ly8gTG9vayBmb3IgYSBkYXRhLW1haW4gYXR0cmlidXRlIHRvIHNldCBtYWluIHNjcmlwdCBmb3IgdGhlIHBhZ2VcclxuXHRcdFx0Ly8gdG8gbG9hZC4gSWYgaXQgaXMgdGhlcmUsIHRoZSBwYXRoIHRvIGRhdGEgbWFpbiBiZWNvbWVzIHRoZVxyXG5cdFx0XHQvLyBiYXNlVXJsLCBpZiBpdCBpcyBub3QgYWxyZWFkeSBzZXQuXHJcblx0XHRcdGRhdGFNYWluID0gc2NyaXB0LmdldEF0dHJpYnV0ZSgnZGF0YS1tYWluJyk7XHJcblx0XHRcdGlmIChkYXRhTWFpbikge1xyXG5cdFx0XHRcdC8vIFByZXNlcnZlIGRhdGFNYWluIGluIGNhc2UgaXQgaXMgYSBwYXRoIChpLmUuIGNvbnRhaW5zICc/JylcclxuXHRcdFx0XHRtYWluU2NyaXB0ID0gZGF0YU1haW47XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gU2V0IGZpbmFsIGJhc2VVcmwgaWYgdGhlcmUgaXMgbm90IGFscmVhZHkgYW4gZXhwbGljaXQgb25lLlxyXG5cdFx0XHRcdGlmICghY2ZnLmJhc2VVcmwpIHtcclxuXHRcdFx0XHRcdC8vIFB1bGwgb2ZmIHRoZSBkaXJlY3Rvcnkgb2YgZGF0YS1tYWluIGZvciB1c2UgYXMgdGhlXHJcblx0XHRcdFx0XHQvLyBiYXNlVXJsLlxyXG5cdFx0XHRcdFx0c3JjID0gbWFpblNjcmlwdC5zcGxpdCgnLycpO1xyXG5cdFx0XHRcdFx0bWFpblNjcmlwdCA9IHNyYy5wb3AoKTtcclxuXHRcdFx0XHRcdHN1YlBhdGggPSBzcmMubGVuZ3RoID8gc3JjLmpvaW4oJy8nKSArICcvJyA6ICcuLyc7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGNmZy5iYXNlVXJsID0gc3ViUGF0aDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gU3RyaXAgb2ZmIGFueSB0cmFpbGluZyAuanMgc2luY2UgbWFpblNjcmlwdCBpcyBub3dcclxuXHRcdFx0XHQvLyBsaWtlIGEgbW9kdWxlIG5hbWUuXHJcblx0XHRcdFx0bWFpblNjcmlwdCA9IG1haW5TY3JpcHQucmVwbGFjZShqc1N1ZmZpeFJlZ0V4cCwgJycpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vIElmIG1haW5TY3JpcHQgaXMgc3RpbGwgYSBwYXRoLCBmYWxsIGJhY2sgdG8gZGF0YU1haW5cclxuXHRcdFx0XHRpZiAocmVxLmpzRXh0UmVnRXhwLnRlc3QobWFpblNjcmlwdCkpIHtcclxuXHRcdFx0XHRcdG1haW5TY3JpcHQgPSBkYXRhTWFpbjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Ly8gUHV0IHRoZSBkYXRhLW1haW4gc2NyaXB0IGluIHRoZSBmaWxlcyB0byBsb2FkLlxyXG5cdFx0XHRcdGNmZy5kZXBzID0gY2ZnLmRlcHMgPyBjZmcuZGVwcy5jb25jYXQobWFpblNjcmlwdCkgOiBbbWFpblNjcmlwdF07XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBFeGVjdXRlcyB0aGUgdGV4dC4gTm9ybWFsbHkganVzdCB1c2VzIGV2YWwsIGJ1dCBjYW4gYmUgbW9kaWZpZWQgdG8gdXNlIGEgYmV0dGVyLCBlbnZpcm9ubWVudC1zcGVjaWZpYyBjYWxsLiBcclxuXHQgKiBPbmx5IHVzZWQgZm9yIHRyYW5zcGlsaW5nIGxvYWRlciBwbHVnaW5zLCBub3QgZm9yIHBsYWluIEpTIG1vZHVsZXMuXHJcblx0ICogXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IHRleHQgVGhlIHRleHQgdG8gZXhlY3V0ZS9ldmFsdWF0ZS5cclxuXHQgKi9cclxuXHRyZXEuZXhlYyA9IGZ1bmN0aW9uKHRleHQpIHtcclxuXHRcdC8qanNsaW50IGV2aWw6IHRydWUgKi9cclxuXHRcdHJldHVybiBldmFsKHRleHQpO1xyXG5cdH07XHJcblx0XHJcblx0Ly8gU2V0IHVwIHdpdGggY29uZmlnIGluZm8uXHJcblx0cmVxKGNmZyk7XHJcbn0pKCk7XHJcbiJdLCJwcmVFeGlzdGluZ0NvbW1lbnQiOiIvLyMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtODtiYXNlNjQsZXlKMlpYSnphVzl1SWpvekxDSnpiM1Z5WTJWeklqcGJJbTV2WkdWZmJXOWtkV3hsY3k5aWNtOTNjMlZ5TFhCaFkyc3ZYM0J5Wld4MVpHVXVhbk1pTENKemNtTXZTbE5GYm1kcGJtVXZZMjl1YzNSeWRXTjBiM0p6TDJOdmJHeGxZM1JwYjI0dWFuTWlMQ0p6Y21NdlNsTkZibWRwYm1VdlkyOXVjM1J5ZFdOMGIzSnpMMlJoZEdGZlltbHVaR2x1Wnk1cWN5SXNJbk55WXk5S1UwVnVaMmx1WlM5amIyNXpkSEoxWTNSdmNuTXZiVzlrZFd4bExtcHpJaXdpYzNKakwwcFRSVzVuYVc1bEwyTnZibk4wY25WamRHOXljeTl1WVcxbGMzQmhZMlV1YW5NaUxDSnpjbU12U2xORmJtZHBibVV2WTI5eVpTOWhZbTkxZEM1cWN5SXNJbk55WXk5S1UwVnVaMmx1WlM5amIzSmxMMk52Ym1acFp5NXFjeUlzSW5OeVl5OUtVMFZ1WjJsdVpTOWpiM0psTDJSbFluVm5MbXB6SWl3aWMzSmpMMHBUUlc1bmFXNWxMMk52Y21VdlpXNW5hVzVsTG1weklpd2ljM0pqTDBwVFJXNW5hVzVsTDJOdmNtVXZaWGgwWlc1a0xtcHpJaXdpYzNKakwwcFRSVzVuYVc1bEwyTnZjbVV2YVc1cGRHbGhiR2w2WlM1cWN5SXNJbk55WXk5S1UwVnVaMmx1WlM5amIzSmxMMnhoYm1jdWFuTWlMQ0p6Y21NdlNsTkZibWRwYm1VdlkyOXlaUzl0WVdsdUxtcHpJaXdpYzNKakwwcFRSVzVuYVc1bEwyTnZjbVV2Ylc5a2RXeGxYMnh2WVdSbGNpNXFjeUlzSW5OeVl5OUtVMFZ1WjJsdVpTOWpiM0psTDNCdmJIbG1hV3hzY3k1cWN5SXNJbk55WXk5S1UwVnVaMmx1WlM5amIzSmxMM0psWjJsemRISjVMbXB6SWl3aWMzSmpMMHBUUlc1bmFXNWxMMk52Y21VdmNtVnhkV2x5WlM1cWN5SmRMQ0p1WVcxbGN5STZXMTBzSW0xaGNIQnBibWR6SWpvaVFVRkJRVHM3T3pzN096dEJRMEZCT3pzN096czdPenM3TzBGQlZVRXNRMEZCUXl4WlFVRlhPenRCUVVWWU96dEJRVVZCT3pzN096czdPenRCUVVwWExFdEJWMHdzVlVGWVN6dEJRVmxXT3pzN096czdPMEZCVDBFc2MwSkJRVmtzU1VGQldpeEZRVUZyUWl4VFFVRnNRaXhGUVVFMlFpeFRRVUUzUWl4RlFVRjNRenRCUVVGQk96dEJRVU4yUXl4UlFVRkxMRWxCUVV3c1IwRkJXU3hKUVVGYU8wRkJRMEVzVVVGQlN5eFRRVUZNTEVkQlFXbENMRk5CUVdwQ08wRkJRMEVzVVVGQlN5eFRRVUZNTEVkQlFXbENMRk5CUVdwQ08wRkJRMEVzVVVGQlN5eExRVUZNTEVkQlFXRTdRVUZEV2l4aFFVRlRMRVZCUkVjN1FVRkZXaXhWUVVGTk8wRkJSazBzU1VGQllqdEJRVWxCT3p0QlFVVkVPenM3T3pzN096czdPenM3T3p0QlFUZENWVHRCUVVGQk8wRkJRVUVzTUVKQmVVTklMRWxCZWtOSExFVkJlVU5ITEZsQmVrTklMRVZCZVVOcFFpeEpRWHBEYWtJc1JVRjVRM1ZDTzBGQlEyaERPMEZCUTBFc1VVRkJTU3hEUVVGRExFbEJRVVFzU1VGQlV5eFBRVUZQTEVsQlFWQXNTMEZCWjBJc1VVRkJla0lzU1VGQmNVTXNUMEZCVHl4SlFVRlFMRXRCUVdkQ0xGVkJRWHBFTEVWQlFYRkZPMEZCUTNCRkxGTkJRVWtzU1VGQlNpeERRVUZUTEV0QlFWUXNRMEZCWlN4SlFVRm1MRU5CUVc5Q0xEWkVRVUZ3UWl4RlFVRnRSaXhUUVVGdVJqdEJRVU5CTEZsQlFVOHNTMEZCVUR0QlFVTkJPenRCUVVWRU8wRkJRMEVzVVVGQlNTeExRVUZMTEV0QlFVd3NRMEZCVnl4UFFVRllMRU5CUVcxQ0xFbEJRVzVDTEVOQlFVb3NSVUZCT0VJN1FVRkROMElzVTBGQlNTeEpRVUZLTEVOQlFWTXNTMEZCVkN4RFFVRmxMRWxCUVdZc1EwRkJiMElzTmtKQlFUWkNMRWxCUVRkQ0xFZEJRVzlETEhWRFFVRjRSRHRCUVVOQkxGbEJRVThzUzBGQlVEdEJRVU5CT3p0QlFVVkVPMEZCUTBFc1UwRkJTeXhMUVVGTUxFTkJRVmNzVDBGQldDeERRVUZ0UWl4SlFVRnVRaXhKUVVFeVFqdEJRVU14UWl4WFFVRk5MRWxCUkc5Q08wRkJSVEZDTEcxQ1FVRmpPMEZCUmxrc1MwRkJNMEk3UVVGSlFUczdRVUZGUkRzN096czdPenM3T3pzN096dEJRVGRFVlR0QlFVRkJPMEZCUVVFc01FSkJlVVZYTzBGQlFVRTdPMEZCUVVFc1VVRkJhRUlzVDBGQlowSXNkVVZCUVU0c1NVRkJUVHM3UVVGRGNFSTdRVUZEUVN4UlFVRkpMRU5CUVVNc1MwRkJTeXhUUVVGV0xFVkJRWEZDTzBGQlEzQkNMRmRCUVUwc1NVRkJTU3hMUVVGS0xFTkJRVlVzZVVWQlFWWXNRMEZCVGp0QlFVTkJPenRCUVVWRU8wRkJRMEVzVVVGQlNTeFpRVUZaTEZOQlFWb3NTVUZCZVVJc1dVRkJXU3hKUVVGNlF5eEZRVUVyUXp0QlFVTTVReXhsUVVGVkxFVkJRVVVzVFVGQlJpeERRVUZXTzBGQlEwRTdPMEZCUlVRc1VVRkJUU3haUVVGWkxGVkJRVlVzUzBGQlN5eFRRVUZNTEVOQlFXVXNTVUZCZWtJc1IwRkJaME1zUjBGQmFFTXNSMEZCYzBNc1MwRkJTeXhUUVVFM1JEdEJRVU5CTEZGQlFVMHNiMEpCUVc5Q0xFVkJRVVVzVVVGQlJpeEZRVUV4UWp0QlFVTkJMRkZCUVUwc2NVSkJRWEZDTEVWQlFUTkNPenRCUVVWQkxGbEJRMFVzVFVGRVJpeERRVU5UTEUxQlFVMHNVMEZCVGl4SFFVRnJRaXhIUVVRelFpeEZRVVZGTEVkQlJrWXNRMEZGVFN4UlFVRlJMRWxCUVZJc1EwRkJZU3hOUVVGTkxGTkJRVTRzUjBGQmEwSXNSMEZCTDBJc1EwRkdUaXhGUVVkRkxFbEJTRVlzUTBGSFR5eFZRVUZETEV0QlFVUXNSVUZCVVN4UFFVRlNMRVZCUVc5Q08wRkJRM3BDTEZOQlFVMHNWMEZCVnl4RlFVRkZMRTlCUVVZc1EwRkJha0k3UVVGRFFTeFRRVUZOTEZWQlFWVXNVMEZCVXl4SlFVRlVMRU5CUVdNc1UwRkJaQ3hEUVVGb1FqczdRVUZGUVN4alFVRlRMRlZCUVZRc1EwRkJiMElzVTBGQmNFSTdPMEZCUlVFc1QwRkJSU3hKUVVGR0xFTkJRVThzVVVGQlVTeFBRVUZTTEVOQlFXZENMSE5DUVVGb1FpeEZRVUYzUXl4SFFVRjRReXhGUVVFMlF5eEpRVUUzUXl4SFFVRnZSQ3hMUVVGd1JDeERRVUV3UkN4SFFVRXhSQ3hEUVVGUUxFVkJRWFZGTEZWQlFVTXNTMEZCUkN4RlFVRlJMRWxCUVZJc1JVRkJhVUk3UVVGRGRrWXNWVUZCU1N4VFFVRlRMRVZCUVdJc1JVRkJhVUk3UVVGRGFFSXNZMEZCVHl4SlFVRlFPMEZCUTBFN08wRkJSVVFzVlVGQlRTeFhRVUZYTEVWQlFVVXNVVUZCUml4RlFVRnFRanRCUVVOQkxIbENRVUZ0UWl4SlFVRnVRaXhEUVVGM1FpeFJRVUY0UWpzN1FVRkZRU3hWUVVGSkxFbEJRVW9zUTBGQlV5eGhRVUZVTEVOQlEwVXNTVUZFUml4RFFVTlBMRkZCUkZBc1JVRkRhVUlzU1VGRWFrSXNSVUZEZFVJc1MwRkVka0lzUlVGRlJTeEpRVVpHTEVOQlJVOHNWVUZCUXl4TlFVRkVPMEZCUVVFc1kwRkJXU3hQUVVGUExFbEJRVkFzUTBGQldTeFJRVUZhTEVOQlFWbzdRVUZCUVN4UFFVWlFMRVZCUjBVc1NVRklSaXhEUVVkUExGVkJRVU1zUzBGQlJDeEZRVUZYTzBGQlEyaENMR2RDUVVGVExFMUJRVlE3UVVGRFFUdEJRVU5CTEZkQlFVa3NTVUZCU2l4RFFVRlRMRXRCUVZRc1EwRkJaU3hMUVVGbUxFTkJRWEZDTERSQ1FVRTBRaXhKUVVGcVJDeEZRVUYxUkN4TFFVRjJSRHRCUVVOQkxFOUJVRVk3UVVGUlFTeE5RV2hDUkR0QlFXbENRU3hMUVRGQ1JqczdRVUUwUWtFN1FVRkRRU3hOUVVGRkxFbEJRVVlzUTBGQlR5eExRVUZRTEVOQlFXRXNVMEZCWWl4RlFVRjNRaXhyUWtGQmVFSXNSVUZCTkVNc1RVRkJOVU1zUTBGQmJVUTdRVUZCUVN4WlFVRk5MR3RDUVVGclFpeFBRVUZzUWl4RlFVRk9PMEZCUVVFc1MwRkJia1E3TzBGQlJVRXNWMEZCVHl4dFFrRkJiVUlzVFVGQmJrSXNSMEZCTkVJc2EwSkJRV3RDTEU5QlFXeENMRVZCUVRWQ0xFZEJRVEJFTEd0Q1FVRnJRaXhQUVVGc1FpeEZRVUZxUlR0QlFVTkJPMEZCZUVoVE96dEJRVUZCTzBGQlFVRTdPMEZCTWtoWUxFdEJRVWtzV1VGQlNpeERRVUZwUWl4VlFVRnFRaXhIUVVFNFFpeFZRVUU1UWp0QlFVTkJMRU5CTlVoRU96czdPenM3T3pzN1FVTldRVHM3T3pzN096czdPenRCUVZWQkxFTkJRVU1zV1VGQlZ6czdRVUZGV0RzN1FVRkZRVHM3T3pzN096czdRVUZLVnl4TFFWZE1MRmRCV0VzN1FVRlpWanM3T3pzN08wRkJUVUVzZFVKQlFWa3NTVUZCV2l4RlFVRnJRaXhSUVVGc1FpeEZRVUUwUWp0QlFVRkJPenRCUVVNelFpeFJRVUZMTEVsQlFVd3NSMEZCV1N4SlFVRmFPMEZCUTBFc1VVRkJTeXhSUVVGTUxFZEJRV2RDTEZGQlFXaENPMEZCUTBFc1VVRkJTeXhMUVVGTUxFZEJRV0VzU1VGQllqdEJRVU5CTEZGQlFVc3NVMEZCVEN4SFFVRnBRaXhUUVVGVExFVkJRVlFzUTBGQldTeDVRa0ZCV2l4RFFVRnFRanRCUVVOQkxGRkJRVXNzU1VGQlREdEJRVU5CT3p0QlFVVkVPenM3T3p0QlFURkNWVHRCUVVGQk8wRkJRVUVzTUVKQk5rSklPMEZCUVVFN08wRkJRMDRzVTBGQlN5eFJRVUZNTEVOQlFXTXNSVUZCWkN4RFFVRnBRaXhSUVVGcVFpeEZRVUV5UWl4WlFVRk5PMEZCUTJoRExGZEJRVXNzUjBGQlREdEJRVU5CTEV0QlJrUTdRVUZIUVRzN1FVRkZSRHM3T3pzN08wRkJia05WTzBGQlFVRTdRVUZCUVN4NVFrRjNRMG83UVVGRFRDeFRRVUZMTEV0QlFVd3NSMEZCWVN4TFFVRkxMRk5CUVV3c1IwRkJhVUlzUzBGQlN5eFJRVUZNTEVOQlFXTXNSMEZCWkN4RlFVRnFRaXhIUVVGMVF5eExRVUZMTEZGQlFVd3NRMEZCWXl4SlFVRmtMRVZCUVhCRU96dEJRVVZCTEZGQlFVa3NTMEZCU3l4UlFVRk1MRU5CUVdNc1JVRkJaQ3hEUVVGcFFpeFhRVUZxUWl4TFFVRnJReXhMUVVGTExGRkJRVXdzUTBGQll5eEZRVUZrTEVOQlFXbENMRkZCUVdwQ0xFTkJRWFJETEVWQlFXdEZPMEZCUTJwRkxGVkJRVXNzUzBGQlRDeEhRVUZoTEV0QlFVc3NVVUZCVEN4RFFVRmpMRWxCUVdRc1EwRkJiVUlzVTBGQmJrSXNRMEZCWWp0QlFVTkJPenRCUVVWRUxGZEJRVThzUzBGQlN5eExRVUZhTzBGQlEwRTdPMEZCUlVRN096czdPenRCUVd4RVZUdEJRVUZCTzBGQlFVRXNkVUpCZFVST0xFdEJka1JOTEVWQmRVUkRPMEZCUTFZc1UwRkJTeXhMUVVGTUxFZEJRV0VzUzBGQllqczdRVUZGUVN4UlFVRkpMRXRCUVVzc1UwRkJWQ3hGUVVGdlFqdEJRVU51UWl4VlFVRkxMRkZCUVV3c1EwRkJZeXhIUVVGa0xFTkJRV3RDTEV0QlFXeENPMEZCUTBFc1MwRkdSQ3hOUVVWUE8wRkJRMDRzVlVGQlN5eFJRVUZNTEVOQlFXTXNTVUZCWkN4RFFVRnRRaXhMUVVGdVFqdEJRVU5CTzBGQlEwUTdRVUV2UkZNN08wRkJRVUU3UVVGQlFUczdRVUZyUlZnc1MwRkJTU3haUVVGS0xFTkJRV2xDTEZkQlFXcENMRWRCUVN0Q0xGZEJRUzlDTzBGQlEwRXNRMEZ1UlVRN096czdPenM3T3p0QlExWkJPenM3T3pzN096czdPMEZCVlVFc1EwRkJReXhaUVVGWE96dEJRVVZZT3p0QlFVVkJPenM3T3pzN096dEJRVXBYTEV0QlYwd3NUVUZZU3p0QlFWbFdPenM3T3pzN08wRkJUMEVzYTBKQlFWa3NVVUZCV2l4RlFVRnpRaXhKUVVGMFFpeEZRVUUwUWl4VlFVRTFRaXhGUVVGM1F6dEJRVUZCT3p0QlFVTjJReXhSUVVGTExGRkJRVXdzUjBGQlowSXNVVUZCYUVJN1FVRkRRU3hSUVVGTExFbEJRVXdzUjBGQldTeEpRVUZhTzBGQlEwRXNVVUZCU3l4VlFVRk1MRWRCUVd0Q0xGVkJRV3hDTzBGQlEwRTdPMEZCUlVRN096czdPenM3T3pzN1FVRjZRbFU3UVVGQlFUdEJRVUZCTEhkQ1FXbERUQ3hyUWtGcVEwc3NSVUZwUTJVN1FVRkJRVHM3UVVGRGVFSTdRVUZEUVN4UlFVRk5MRk5CUVZNc1MwRkJTeXhWUVVGTUxFTkJRV2RDTEV0QlFXaENMRU5CUVhOQ0xFOUJRWFJDTEVOQlFUaENMRXRCUVVzc1NVRkJia01zUTBGQlpqdEJRVU5CTEZGQlFVa3NWVUZCVlN4SlFVRmtPenRCUVVWQkxGRkJRVWs3UVVGRFNDeFRRVUZKTEVOQlFVTXNUVUZCVEN4RlFVRmhPMEZCUTFvc1dVRkJUU3hKUVVGSkxFdEJRVW9zWTBGQmNVSXNTMEZCU3l4SlFVRXhRaXh0UkVGQlRqdEJRVU5CT3p0QlFVVkVMRk5CUVUwc1QwRkJUeXhMUVVGTExHTkJRVXdzUlVGQllqdEJRVU5CTEZOQlFVMHNWMEZCVnl4UFFVRlBMRWxCUVZBc1EwRkJXU3hKUVVGYUxFTkJRV2xDTEV0QlFVc3NVVUZCZEVJc1JVRkJaME1zU1VGQmFFTXNRMEZCYWtJN08wRkJSVUU3UVVGRFFUdEJRVU5CTEZOQlFVMHNUMEZCVHl4VFFVRlFMRWxCUVU4c1IwRkJUVHRCUVVOc1FpeFpRVUZMTEZGQlFVd3NRMEZCWXl4UFFVRmtMRU5CUVhOQ0xIZENRVUYwUWl4RlFVRm5SQ3hEUVVGRExFVkJRVU1zVVVGQlVTeE5RVUZMTEVsQlFXUXNSVUZCUkN4RFFVRm9SRHRCUVVOQkxGVkJRVWtzU1VGQlNpeERRVUZUTEV0QlFWUXNRMEZCWlN4SlFVRm1MR05CUVN0Q0xFMUJRVXNzU1VGQmNFTTdRVUZEUVN4NVFrRkJiVUlzVDBGQmJrSTdRVUZEUVN4dFFrRkJZU3hQUVVGaU8wRkJRMEVzVFVGTVJEczdRVUZQUVR0QlFVTkJMRlZCUVVzc1pVRkJUQ3hEUVVGeFFpeFJRVUZ5UWl4RlFVTkZMRWxCUkVZc1EwRkRUeXhaUVVGTk8wRkJRMWc3UVVGRFFTeG5Ra0ZCVlN4WFFVRlhMRmxCUVUwN1FVRkRNVUlzVjBGQlNTeEpRVUZLTEVOQlFWTXNTMEZCVkN4RFFVRmxMRWxCUVdZc1EwRkJiMElzY1VSQlFYRkVMRTFCUVVzc1NVRkJPVVU3UVVGRFFTd3dRa0ZCYlVJc1RVRkJia0k3UVVGRFFTeFBRVWhUTEVWQlIxQXNTMEZJVHl4RFFVRldPenRCUVV0QkxHVkJRVk1zU1VGQlZDeERRVUZqTEVsQlFXUTdRVUZEUVN4TlFWUkdMRVZCVlVVc1NVRldSaXhEUVZWUExGVkJRVU1zUzBGQlJDeEZRVUZYTzBGQlEyaENMSGxDUVVGdFFpeE5RVUZ1UWp0QlFVTkJMRlZCUVVrc1NVRkJTaXhEUVVGVExFdEJRVlFzUTBGQlpTeExRVUZtTEVOQlFYRkNMSEZEUVVGeVFpeEZRVUUwUkN4TFFVRTFSRHRCUVVOQkxFMUJZa1k3UVVGalFTeExRV2hEUkN4RFFXZERSU3hQUVVGUExGTkJRVkFzUlVGQmEwSTdRVUZEYmtJc2QwSkJRVzFDTEUxQlFXNUNPMEZCUTBFc1UwRkJTU3hKUVVGS0xFTkJRVk1zUzBGQlZDeERRVUZsTEV0QlFXWXNaME5CUVd0RUxFdEJRVXNzU1VGQmRrUXNVMEZCYVVVc1UwRkJha1U3UVVGRFFTeFBRVUZGTEUxQlFVWXNSVUZCVlN4UFFVRldMRU5CUVd0Q0xFOUJRV3hDTEVWQlFUSkNMRU5CUVVNc1UwRkJSQ3hEUVVFelFpeEZRVWh0UWl4RFFVZHpRanRCUVVONlF6czdRVUZGUkN4WFFVRlBMRzFDUVVGdFFpeFBRVUZ1UWl4RlFVRlFPMEZCUTBFN08wRkJSVVE3T3pzN096czdPMEZCTDBWVk8wRkJRVUU3UVVGQlFTeHZRMEZ6Ums4N1FVRkJRVHM3UVVGRGFFSXNVVUZCVFN4UFFVRlBMRVZCUVdJN08wRkJSVUVzVFVGQlJTeEpRVUZHTEVOQlFVOHNTMEZCU3l4UlFVRk1MRU5CUVdNc1NVRkJaQ3hGUVVGUUxFVkJRVFpDTEZWQlFVTXNTVUZCUkN4RlFVRlBMRXRCUVZBc1JVRkJhVUk3UVVGRE4wTXNVMEZCU1N4TFFVRkxMRTlCUVV3c1EwRkJZU3hQUVVGTExFbEJRV3hDTEUxQlFUUkNMRU5CUVRWQ0xFbEJRV2xETEV0QlFVc3NUMEZCVEN4RFFVRmhMRTlCUVVzc1NVRkJUQ3hEUVVGVkxGZEJRVllzUlVGQllpeE5RVUV3UXl4RFFVRXZSU3hGUVVGclJqdEJRVU5xUml4VlFVRkpMRTFCUVUwc1MwRkJTeXhOUVVGTUxFTkJRVmtzVDBGQlN5eEpRVUZNTEVOQlFWVXNUVUZCZEVJc1EwRkJWanRCUVVOQkxGbEJRVTBzU1VGQlNTeE5RVUZLTEVOQlFWY3NRMEZCV0N4RlFVRmpMRU5CUVdRc1JVRkJhVUlzVjBGQmFrSXNTMEZCYVVNc1NVRkJTU3hOUVVGS0xFTkJRVmNzUTBGQldDeERRVUYyUXp0QlFVTkJMRmRCUVVzc1IwRkJUQ3hKUVVGWkxFdEJRVm83UVVGRFFUdEJRVU5CTEZWQlFVMHNaVUZCWlN4SlFVRkpMRTlCUVVvc1EwRkJXU3hwUWtGQldpeEZRVUVyUWl4UFFVRXZRaXhGUVVGM1F5eFhRVUY0UXl4RlFVRnlRanRCUVVOQkxHRkJRVXNzVVVGQlRDeERRVUZqTEZWQlFXUXNWMEZCYVVNc1QwRkJTeXhKUVVGMFF5eFRRVUU0UXl4WlFVRTVRenRCUVVOQk8wRkJRMFFzUzBGVVJEczdRVUZYUVN4WFFVRlBMRWxCUVZBN1FVRkRRVHM3UVVGRlJEczdPenM3T3pzN096dEJRWFpIVlR0QlFVRkJPMEZCUVVFc2JVTkJaMGhOTEZGQmFFaE9MRVZCWjBoblFqdEJRVU42UWl4UlFVRk5MRmRCUVZjc1JVRkJSU3hSUVVGR0xFVkJRV3BDTzBGQlEwRXNVVUZCVFN4eFFrRkJjVUlzUlVGQk0wSTdPMEZCUlVFc1VVRkJTVHRCUVVOSUxGTkJRVWtzVTBGQlV5eExRVUZpTEVWQlFXOUNPMEZCUTI1Q0xGRkJRVVVzU1VGQlJpeERRVUZQTEZOQlFWTXNTMEZCYUVJc1JVRkJkVUlzVlVGQlV5eExRVUZVTEVWQlFXZENMRWRCUVdoQ0xFVkJRWEZDTzBGQlF6TkRMRmRCUVUwc1owSkJRV2RDTEVWQlFVVXNVVUZCUml4RlFVRjBRanRCUVVOQkxEQkNRVUZ0UWl4SlFVRnVRaXhEUVVGM1FpeGhRVUY0UWp0QlFVTkJMRk5CUVVVc1QwRkJSaXhEUVVGVkxFZEJRVllzUlVGRFJTeEpRVVJHTEVOQlEwOHNWVUZCUXl4UlFVRkVMRVZCUVdNN1FVRkRia0lzYVVKQlFWTXNTMEZCVkN4RFFVRmxMRXRCUVdZc1NVRkJkMElzVVVGQmVFSTdRVUZEUVN4elFrRkJZeXhQUVVGa0xFTkJRWE5DTEZGQlFYUkNPMEZCUTBFc1VVRktSaXhGUVV0RkxFbEJURVlzUTBGTFR5eFZRVUZETEV0QlFVUXNSVUZCVnp0QlFVTm9RaXh6UWtGQll5eE5RVUZrTEVOQlFYRkNMRXRCUVhKQ08wRkJRMEVzVVVGUVJqdEJRVkZCTEU5QldFUTdRVUZaUVRzN1FVRkZSQ3hUUVVGSkxGTkJRVk1zU1VGQllpeEZRVUZ0UWp0QlFVTnNRaXhSUVVGRkxFbEJRVVlzUTBGQlR5eFRRVUZUTEVsQlFXaENMRVZCUVhOQ0xGVkJRVk1zUzBGQlZDeEZRVUZuUWl4SFFVRm9RaXhGUVVGeFFqdEJRVU14UXl4WFFVRk5MR1ZCUVdVc1JVRkJSU3hSUVVGR0xFVkJRWEpDTzBGQlEwRXNNRUpCUVcxQ0xFbEJRVzVDTEVOQlFYZENMRmxCUVhoQ08wRkJRMEVzVTBGQlJTeEhRVUZHTEVOQlFVMHNSMEZCVGl4RlFVTkZMRWxCUkVZc1EwRkRUeXhWUVVGRExGRkJRVVFzUlVGQll6dEJRVU51UWl4cFFrRkJVeXhKUVVGVUxFTkJRV01zUzBGQlpDeEpRVUYxUWl4UlFVRjJRanRCUVVOQkxIRkNRVUZoTEU5QlFXSXNRMEZCY1VJc1VVRkJja0k3UVVGRFFTeFJRVXBHTEVWQlMwVXNTVUZNUml4RFFVdFBMRlZCUVVNc1MwRkJSQ3hGUVVGWE8wRkJRMmhDTEhGQ1FVRmhMRTFCUVdJc1EwRkJiMElzUzBGQmNFSTdRVUZEUVN4UlFWQkdPMEZCVVVFc1QwRllSRHRCUVZsQk96dEJRVVZFTEZOQlFVa3NVMEZCVXl4UlFVRmlMRVZCUVhWQ08wRkJRM1JDTEZkQlFVc3NTVUZCU1N4SlFVRlVMRWxCUVdsQ0xGTkJRVk1zVVVGQk1VSXNSVUZCYjBNN1FVRkRia01zVjBGQlRTeFhRVUZYTEZOQlFWTXNVVUZCVkN4RFFVRnJRaXhKUVVGc1FpeERRVUZxUWp0QlFVTkJMR2RDUVVGVExGRkJRVlFzUTBGQmEwSXNTVUZCYkVJc1NVRkJNRUlzU1VGQlNTeEpRVUZKTEZsQlFVb3NRMEZCYVVJc1YwRkJja0lzUTBGQmFVTXNTVUZCYWtNc1JVRkJkVU1zVVVGQmRrTXNRMEZCTVVJN1FVRkRRVHRCUVVORU96dEJRVVZFTEU5QlFVVXNTVUZCUml4RFFVRlBMRXRCUVZBc1EwRkJZU3hUUVVGaUxFVkJRWGRDTEd0Q1FVRjRRaXhGUVVORkxFbEJSRVlzUTBGRFR5eFRRVUZUTEU5QlJHaENMRVZCUlVVc1NVRkdSaXhEUVVWUExGVkJRVU1zUzBGQlJDeEZRVUZYTzBGQlEyaENMR1ZCUVZNc1RVRkJWQ3hEUVVGblFpeEpRVUZKTEV0QlFVb3NiVU5CUVRCRExGTkJRVk1zU1VGQmJrUXNVMEZCTmtRc1MwRkJOMFFzUTBGQmFFSTdRVUZEUVN4TlFVcEdPMEZCUzBFc1MwRXpRMFFzUTBFeVEwVXNUMEZCVHl4VFFVRlFMRVZCUVd0Q08wRkJRMjVDTEdOQlFWTXNUVUZCVkN4RFFVRm5RaXhUUVVGb1FqdEJRVU5CTEZOQlFVa3NTVUZCU2l4RFFVRlRMRXRCUVZRc1EwRkJaU3hMUVVGbUxITkRRVUYzUkN4TFFVRkxMRWxCUVRkRUxGTkJRWFZGTEZOQlFYWkZPMEZCUTBFc1QwRkJSU3hOUVVGR0xFVkJRVlVzVDBGQlZpeERRVUZyUWl4UFFVRnNRaXhGUVVFeVFpeERRVUZETEZOQlFVUXNRMEZCTTBJc1JVRkliVUlzUTBGSGMwSTdRVUZEZWtNN08wRkJSVVFzVjBGQlR5eFRRVUZUTEU5QlFWUXNSVUZCVUR0QlFVTkJPMEZCZEV0VE96dEJRVUZCTzBGQlFVRTdPMEZCZVV0WUxFdEJRVWtzV1VGQlNpeERRVUZwUWl4TlFVRnFRaXhIUVVFd1FpeE5RVUV4UWp0QlFVTkJMRU5CTVV0RU96czdPenM3T3pzN1FVTldRVHM3T3pzN096czdPenRCUVZWQkxFTkJRVU1zV1VGQlZ6czdRVUZGV0RzN1FVRkZRVHM3T3pzN096czdPMEZCU2xjc1MwRlpUQ3hUUVZwTE8wRkJZVlk3T3pzN096czdRVUZQUVN4eFFrRkJXU3hKUVVGYUxFVkJRV3RDTEUxQlFXeENMRVZCUVRCQ0xGZEJRVEZDTEVWQlFYVkRPMEZCUVVFN08wRkJRM1JETEZGQlFVc3NTVUZCVEN4SFFVRlpMRWxCUVZvN1FVRkRRU3hSUVVGTExFMUJRVXdzUjBGQll5eE5RVUZrTzBGQlEwRXNVVUZCU3l4WFFVRk1MRWRCUVcxQ0xGZEJRVzVDTEVOQlNITkRMRU5CUjA0N1FVRkRhRU03TzBGQlJVUTdPenM3T3pzN096czdRVUV4UWxVN1FVRkJRVHRCUVVGQkxEQkNRV3REU0R0QlFVTk9MRkZCUVUwc2NVSkJRWEZDTEVWQlFUTkNPenRCUVVSTk8wRkJRVUU3UVVGQlFUczdRVUZCUVR0QlFVZE9MREJDUVVGMVFpeExRVUZMTEZkQlFUVkNMRGhJUVVGNVF6dEJRVUZCTEZWQlFXaERMRlZCUVdkRE96dEJRVU40UXl4WFFVRkxMRmRCUVZjc1NVRkJhRUlzU1VGQmQwSXNTVUZCU1N4SlFVRkpMRmxCUVVvc1EwRkJhVUlzVlVGQmNrSXNRMEZCWjBNc1YwRkJWeXhKUVVFelF5eEZRVUZwUkN4WFFVRlhMRk5CUVRWRUxFVkJRWFZGTEVsQlFYWkZMRU5CUVhoQ08wRkJRMEVzVlVGQlRTeFhRVUZYTEV0QlFVc3NWMEZCVnl4SlFVRm9RaXhGUVVGelFpeEpRVUYwUWl4RlFVRnFRanRCUVVOQkxIbENRVUZ0UWl4SlFVRnVRaXhEUVVGM1FpeFJRVUY0UWp0QlFVTkJPMEZCVUVzN1FVRkJRVHRCUVVGQk8wRkJRVUU3UVVGQlFUdEJRVUZCTzBGQlFVRTdRVUZCUVR0QlFVRkJPMEZCUVVFN1FVRkJRVHRCUVVGQk8wRkJRVUU3UVVGQlFUczdRVUZUVGl4WFFVRlBMRzFDUVVGdFFpeE5RVUZ1UWl4SFFVRTBRaXhGUVVGRkxFbEJRVVlzUTBGQlR5eExRVUZRTEVOQlFXRXNVMEZCWWl4RlFVRjNRaXhyUWtGQmVFSXNSVUZCTkVNc1QwRkJOVU1zUlVGQk5VSXNSMEZCYjBZc1JVRkJSU3hSUVVGR0xFZEJRV0VzVDBGQllpeEZRVUV6Ump0QlFVTkJPMEZCTlVOVE96dEJRVUZCTzBGQlFVRTdPMEZCSzBOWUxFdEJRVWtzV1VGQlNpeERRVUZwUWl4VFFVRnFRaXhIUVVFMlFpeFRRVUUzUWp0QlFVTkJMRU5CYUVSRU96czdPenRCUTFaQk96czdPenM3T3pzN08wRkJWVUU3T3pzN096czdPenRCUVZOQkxGTkJRVk1zWjBKQlFWUXNRMEZCTUVJc2EwSkJRVEZDTEVWQlFUaERMRmxCUVZjN08wRkJSWGhFT3p0QlFVVkJMRXRCUVVrc1NVRkJTU3hKUVVGS0xFTkJRVk1zVFVGQlZDeERRVUZuUWl4SFFVRm9RaXhEUVVGdlFpeGhRVUZ3UWl4TlFVRjFReXhaUVVFelF5eEZRVUY1UkR0QlFVTjRSRHRCUVVOQk96dEJRVVZFTEV0QlFVa3NTMEZCU2l4SFFVRlpMRmxCUVZrN1FVRkRka0lzVFVGQlRTd3JRa0ZEVVN4SlFVRkpMRWxCUVVvc1EwRkJVeXhOUVVGVUxFTkJRV2RDTEVkQlFXaENMRU5CUVc5Q0xGTkJRWEJDTEVOQlJGSXNPRGxEUVVGT096dEJRWGRDUVN4TlFVRkpMRWxCUVVvc1EwRkJVeXhMUVVGVUxFTkJRV1VzU1VGQlppeERRVUZ2UWl4SlFVRndRanRCUVVOQkxFVkJNVUpFTzBGQk1rSkJMRU5CYmtORU96czdPenRCUTI1Q1FUczdPenM3T3pzN096dEJRVlZCTEVsQlFVa3NTVUZCU2l4RFFVRlRMRTFCUVZRc1IwRkJhMElzU1VGQlNTeEpRVUZLTEVOQlFWTXNUVUZCVkN4SlFVRnRRaXhGUVVGeVF6czdRVUZGUVRzN096czdPenM3T3pzN096dEJRV0ZETEZkQlFWTXNUMEZCVkN4RlFVRnJRanM3UVVGRmJFSTdPMEZCUlVFN1FVRkRRVHRCUVVOQk96dEJRVVZCTEV0QlFVMHNVMEZCVXp0QlFVTmtPenM3T3p0QlFVdEJMRmRCUVZNc1MwRk9TenM3UVVGUlpEczdPenM3T3p0QlFVOUJMRlZCUVZFc1NVRm1UVHM3UVVGcFFtUTdPenM3T3pzN096dEJRVk5CTEZkQlFWTXNTVUV4UWtzN08wRkJORUprT3pzN096czdPMEZCVDBFc1kwRkJXU3hKUVc1RFJUczdRVUZ4UTJRN096czdPenM3T3p0QlFWTkJMR1ZCUVdFc1NVRTVRME03TzBGQlowUmtPenM3T3pzN08wRkJUMEVzWVVGQlZ5eEpRWFpFUnpzN1FVRjVSR1E3T3pzN096czdPenRCUVZOQkxHVkJRV0VzV1VGc1JVTTdPMEZCYjBWa096czdPenM3T3pzN1FVRlRRU3huUWtGQll5eEZRVGRGUVRzN1FVRXJSV1E3T3pzN096czdPMEZCVVVFc1pVRkJZU3hGUVhaR1F6czdRVUY1Um1RN096czdPMEZCUzBFc1owSkJRV01zU1VFNVJrRTdPMEZCWjBka096czdPenM3UVVGTlFTeFRRVUZQTEZGQmRFZFBPenRCUVhkSFpEczdPenM3T3pzN08wRkJVMEVzWVVGQlZ5eEpRV3BJUnpzN1FVRnRTR1E3T3pzN08wRkJTMEVzVlVGQlV5eHBSVUZCYVVVc1NVRkJha1VzUTBGQmMwVXNWVUZCVlN4VFFVRm9SaXhEUVhoSVN6czdRVUV3U0dRN096czdPMEZCUzBFc1UwRkJVeXhyUWtGQmEwSXNUVUZCYmtJc1NVRkJPRUlzVDBGQlR5eFpRVUZ5UXl4SlFVRnhSQ3hQUVVGUExHbENRVUUzUkN4SFFVRnJSaXhKUVVGc1JpeEhRVUY1Uml4TFFTOUliRVk3TzBGQmFVbGtPenM3T3pzN08wRkJUMEVzWlVGQllTd3dRMEY0U1VNN08wRkJNRWxrT3pzN096czdPenRCUVZGQkxHRkJRVmNzUlVGc1NrYzdPMEZCYjBwa096czdPenM3T3p0QlFWRkJMR05CUVZrc1JVRTFTa1U3TzBGQk9FcGtPenM3T3p0QlFVdEJMRmRCUVZNc1YwRkJWeXhSUVVGUkxGbEJRVzVDTEVsQlFXMURMRkZCUVZFN1FVRnVTM1JETEVWQlFXWTdPMEZCYzB0Qk96czdPenRCUVV0QkxFdEJRVTBzV1VGQldTeERRVU5xUWl4VFFVUnBRaXhGUVVWcVFpeFpRVVpwUWl4RlFVZHFRaXhoUVVocFFpeERRVUZzUWpzN1FVRk5RVHRCUVVOQk8wRkJRMEU3TzBGQlJVRTdPenM3T3pzN1FVRlBRU3hUUVVGUkxFZEJRVklzUjBGQll5eFZRVUZUTEVsQlFWUXNSVUZCWlR0QlFVTTFRaXhOUVVGSkxFOUJRVThzVjBGQlVDeExRVUYxUWl4WlFVRjJRaXhKUVVGMVF5eFZRVUZWTEZGQlFWWXNRMEZCYlVJc1NVRkJia0lzUTBGQk0wTXNSVUZCY1VVN1FVRkRjRVVzVlVGQlR5eEpRVUZRTzBGQlEwRTdPMEZCUlVRc1UwRkJUeXhQUVVGUExFbEJRVkFzUTBGQlVEdEJRVU5CTEVWQlRrUTdPMEZCVVVFN096czdPenM3T3pzN08wRkJWMEVzVTBGQlVTeEpRVUZTTEVkQlFXVXNWVUZCVXl4eFFrRkJWQ3hGUVVGblF6dEJRVU01UXl4VFFVRlBMRmRCUVZBc1IwRkJjVUlzYzBKQlFYTkNMRmRCUVRORE8wRkJRMEVzVTBGQlR5eE5RVUZRTEVkQlFXZENMSE5DUVVGelFpeE5RVUYwUWl4RFFVRTJRaXhQUVVFM1FpeERRVUZ4UXl4TlFVRnlReXhGUVVFMlF5eEZRVUUzUXl4RFFVRm9RaXhEUVVZNFF5eERRVVZ2UWpzN1FVRkZiRVVzVFVGQlNTeFBRVUZQTEZkQlFWQXNTMEZCZFVJc1lVRkJNMElzUlVGQk1FTTdRVUZEZWtNc1ZVRkJUeXhUUVVGUUxFZEJRVzFDTEV0QlFXNUNPMEZCUTBFc1ZVRkJUeXhSUVVGUUxFZEJRV3RDTEV0QlFXeENPMEZCUTBFc1ZVRkJUeXhMUVVGUUxFZEJRV1VzVDBGQlpqdEJRVU5CT3p0QlFVVkVMRTFCUVVrc2MwSkJRWE5DTEZOQlFYUkNMRXRCUVc5RExGTkJRWGhETEVWQlFXMUVPMEZCUTJ4RUxGVkJRVThzVTBGQlVDeEhRVUZ0UWl4elFrRkJjMElzVTBGQmRFSXNRMEZCWjBNc1QwRkJhRU1zUTBGQmQwTXNUVUZCZUVNc1JVRkJaMFFzUlVGQmFFUXNRMEZCYmtJN1FVRkRRU3hIUVVaRUxFMUJSVTg3UVVGRFRpeFZRVUZQTEZOQlFWQXNSMEZCYlVJc1QwRkJUeXhOUVVGUUxFZEJRV2RDTEdsQ1FVRnVRenRCUVVOQk96dEJRVVZFTEUxQlFVa3NjMEpCUVhOQ0xGbEJRWFJDTEV0QlFYVkRMRk5CUVRORExFVkJRWE5FTzBGQlEzSkVMRlZCUVU4c1dVRkJVQ3hIUVVGelFpeHpRa0ZCYzBJc1dVRkJOVU03TzBGQlJVRXNVVUZCU3l4SlFVRkpMRmRCUVZRc1NVRkJkMElzVDBGQlR5eFpRVUV2UWl4RlFVRTJRenRCUVVNMVF5eFJRVUZKTEVsQlFVb3NRMEZCVXl4SlFVRlVMRU5CUVdNc1ZVRkJaQ3hEUVVGNVFpeFhRVUY2UWl4RlFVRnpReXhQUVVGUExGbEJRVkFzUTBGQmIwSXNWMEZCY0VJc1EwRkJkRU03UVVGRFFUdEJRVU5FT3p0QlFVVkVMRTFCUVVrc2MwSkJRWE5DTEZkQlFYUkNMRXRCUVhORExGTkJRVEZETEVWQlFYRkVPMEZCUTNCRUxGVkJRVThzVjBGQlVDeEhRVUZ4UWl4elFrRkJjMElzVjBGQk0wTTdRVUZEUVN4SFFVWkVMRTFCUlU4N1FVRkRUaXhWUVVGUExGZEJRVkFzUjBGQmNVSXNRMEZEY0VJc1JVRkJReXhOUVVGTkxHRkJRVkFzUlVGQmMwSXNWMEZCVnl4WlFVRnFReXhGUVVSdlFpeEZRVVZ3UWl4RlFVRkRMRTFCUVUwc1dVRkJVQ3hGUVVGeFFpeFhRVUZYTEZkQlFXaERMRVZCUm05Q0xFVkJSM0JDTEVWQlFVTXNUVUZCVFN4VFFVRlFMRVZCUVd0Q0xGZEJRVmNzVVVGQk4wSXNSVUZJYjBJc1EwRkJja0k3UVVGTFFUczdRVUZGUkN4TlFVRkpMSE5DUVVGelFpeFZRVUYwUWl4TFFVRnhReXhUUVVGNlF5eEZRVUZ2UkR0QlFVTnVSQ3hWUVVGUExGVkJRVkFzUjBGQmIwSXNjMEpCUVhOQ0xGVkJRVEZETzBGQlEwRTdPMEZCUlVRc1RVRkJTU3h6UWtGQmMwSXNUMEZCZEVJc1MwRkJhME1zVTBGQmRFTXNSVUZCYVVRN1FVRkRhRVFzVDBGQlNTeEpRVUZLTEVOQlFWTXNTMEZCVkN4RFFVRmxMRWxCUVdZc1EwRkJiMElzYzBaQlEycENMREpDUVVSSU8wRkJSVUVzVlVGQlR5eFBRVUZRTEVkQlFXbENMSE5DUVVGelFpeFBRVUYwUWl4RFFVRTRRaXhQUVVFNVFpeERRVUZ6UXl4TlFVRjBReXhGUVVFNFF5eEZRVUU1UXl4RFFVRnFRanRCUVVOQkxGVkJRVThzVFVGQlVDeEhRVUZuUWl4UFFVRlBMRTFCUVZBc1NVRkJhVUlzVDBGQlR5eFBRVUY0UXl4RFFVcG5SQ3hEUVVsRE8wRkJRMnBFT3p0QlFVVkVMRTFCUVVrc2MwSkJRWE5DTEZkQlFYUkNMRXRCUVhORExGTkJRVEZETEVWQlFYRkVPMEZCUTNCRUxFOUJRVWtzU1VGQlNpeERRVUZUTEV0QlFWUXNRMEZCWlN4SlFVRm1MRU5CUVc5Q0xEQkdRVU5xUWl3clFrRkVTRHRCUVVWQkxGVkJRVThzVjBGQlVDeEhRVUZ4UWl4elFrRkJjMElzVjBGQk0wTTdRVUZEUVRzN1FVRkZSQ3hOUVVGSkxITkNRVUZ6UWl4TlFVRjBRaXhMUVVGcFF5eFRRVUZ5UXl4RlFVRm5SRHRCUVVNdlF5eFZRVUZQTEUxQlFWQXNSMEZCWjBJc2MwSkJRWE5DTEUxQlFYUkRPMEZCUTBFN08wRkJSVVFzVFVGQlNTeHpRa0ZCYzBJc1dVRkJkRUlzUzBGQmRVTXNVMEZCTTBNc1JVRkJjMFE3UVVGRGNrUXNWVUZCVHl4WlFVRlFMRWRCUVhOQ0xITkNRVUZ6UWl4WlFVRTFRenRCUVVOQk96dEJRVVZFTEUxQlFVa3NVMEZCVXl4alFVRlVMRU5CUVhkQ0xGTkJRWGhDTEUxQlFYVkRMRWxCUVhaRExFbEJRMEVzVTBGQlV5eGpRVUZVTEVOQlFYZENMRk5CUVhoQ0xFVkJRVzFETEZsQlFXNURMRU5CUVdkRUxHbENRVUZvUkN4RFFVUktMRVZCUTNkRk8wRkJRM1pGTEhsQ1FVRnpRaXhUUVVGMFFpeEhRVUZyUXl4VFFVRlRMR05CUVZRc1EwRkJkMElzVTBGQmVFSXNSVUZCYlVNc1dVRkJia01zUTBGQlowUXNhVUpCUVdoRUxFTkJRV3hETzBGQlEwRTdPMEZCUlVRc1RVRkJTU3h6UWtGQmMwSXNVMEZCZEVJc1MwRkJiME1zVTBGQmVFTXNSVUZCYlVRN1FVRkRiRVFzVlVGQlR5eFRRVUZRTEVkQlFXMUNMSE5DUVVGelFpeFRRVUY2UXp0QlFVTkJPenRCUVVWRUxFMUJRVWtzYzBKQlFYTkNMRlZCUVhSQ0xFdEJRWEZETEZOQlFYcERMRVZCUVc5RU8wRkJRMjVFTEZWQlFVOHNWVUZCVUN4SFFVRnZRaXh6UWtGQmMwSXNWVUZCTVVNN1FVRkRRVHM3UVVGRlJEdEJRVU5CTEUxQlFVMHNjVUpCUVhGQ08wRkJRekZDTEZWQlFVOHNXVUZFYlVJN1FVRkZNVUlzVVVGQlN5eFhRVVp4UWp0QlFVY3hRaXhUUVVGTk8wRkJTRzlDTEVkQlFUTkNPenRCUVUxQkxFMUJRVTBzZFVKQlFYVkNPMEZCUXpWQ0xGVkJRVThzWVVGRWNVSTdRVUZGTlVJc1VVRkJTeXhYUVVaMVFqdEJRVWMxUWl4VFFVRk5PMEZCU0hOQ0xFZEJRVGRDT3p0QlFVMUJMRk5CUVU4c1YwRkJVQ3hIUVVGelFpeFBRVUZQTEdsQ1FVRlNMRWRCUVRaQ0xHOUNRVUUzUWl4SFFVRnZSQ3hyUWtGQmVrVTdPMEZCUlVFN1FVRkRRU3hQUVVGTExFbEJRVWtzUzBGQlZDeEpRVUZyUWl4elFrRkJjMElzVVVGQmVFTXNSVUZCYTBRN1FVRkRha1FzVDBGQlNTeEpRVUZLTEVOQlFWTXNVVUZCVkN4RFFVRnJRaXhIUVVGc1FpeERRVUZ6UWl4TFFVRjBRaXhGUVVFMlFpeHpRa0ZCYzBJc1VVRkJkRUlzUTBGQkswSXNTMEZCTDBJc1EwRkJOMEk3UVVGRFFUczdRVUZGUkR0QlFVTkJMRTFCUVVrc1NVRkJTaXhEUVVGVExHRkJRVlFzUTBGQmRVSXNTVUZCZGtJN08wRkJSVUU3UVVGRFFTeFRRVUZQTEU5QlFVOHNjVUpCUVdRN1FVRkRRU3hGUVdwSFJEdEJRVzFIUVN4RFFURlVRU3hGUVRCVVF5eEpRVUZKTEVsQlFVb3NRMEZCVXl4TlFURlVWaXhEUVVGRU96czdPenRCUTNwQ1FUczdPenM3T3pzN096dEJRVlZCTEVsQlFVa3NTVUZCU2l4RFFVRlRMRXRCUVZRc1IwRkJhVUlzU1VGQlNTeEpRVUZLTEVOQlFWTXNTMEZCVkN4SlFVRnJRaXhGUVVGdVF6czdRVUZGUVRzN096czdPenM3UVVGUlF5eFhRVUZUTEU5QlFWUXNSVUZCYTBJN1FVRkRiRUk3TzBGQlJVRTdRVUZEUVR0QlFVTkJPenRCUVVWQk96czdPMEZCUjBFc1MwRkJUU3hoUVVGaExFOUJRVzVDT3p0QlFVVkJPenM3UVVGSFFTeExRVUZOTEZsQlFWa3NUVUZCYkVJN08wRkJSVUU3T3p0QlFVZEJMRXRCUVUwc1YwRkJWeXhMUVVGcVFqczdRVUZGUVRzN08wRkJSMEVzUzBGQlRTeFpRVUZaTEUxQlFXeENPenRCUVVWQk96czdRVUZIUVN4TFFVRk5MR0ZCUVdFc1QwRkJia0k3TzBGQlJVRTdPenRCUVVkQkxFdEJRVTBzWVVGQllTeFBRVUZ1UWpzN1FVRkZRVHM3TzBGQlIwRXNTMEZCVFN4alFVRmpMRkZCUVhCQ096dEJRVVZCT3pzN1FVRkhRU3hMUVVGTkxHTkJRV01zVVVGQmNFSTdPMEZCUlVFN096czdPMEZCUzBFc1MwRkJUU3hUUVVGVExFTkJRMlFzVlVGRVl5eEZRVVZrTEZOQlJtTXNSVUZIWkN4UlFVaGpMRVZCU1dRc1UwRktZeXhGUVV0a0xGVkJUR01zUlVGTlpDeFZRVTVqTEVWQlQyUXNWMEZRWXl4RlFWRmtMRmRCVW1Nc1EwRkJaanM3UVVGWFFUdEJRVU5CTzBGQlEwRTdPMEZCUlVFN096czdPenM3UVVGUFFTeFZRVUZUTEhWQ1FVRlVMRWRCUVcxRE8wRkJRMnhETEUxQlFVMHNVMEZCVXl4VFFVRlRMR0ZCUVZRc1EwRkJkVUlzVVVGQmRrSXNRMEZCWmp0QlFVTkJMRTFCUVUwc1ZVRkJWU3hUUVVGVExHRkJRVlFzUTBGQmRVSXNkVUpCUVhaQ0xFTkJRV2hDT3p0QlFVVkJMRTFCUVVrc1QwRkJUeXhWUVVGUUxFbEJRWEZDTEVOQlFVTXNVVUZCVVN4VFFVRlNMRU5CUVd0Q0xGRkJRV3hDTEVOQlFUSkNMR0ZCUVROQ0xFTkJRVEZDTEVWQlFYRkZPMEZCUTNCRkxFOUJRVTBzVFVGQlRTeFRRVUZUTEdGQlFWUXNRMEZCZFVJc1MwRkJka0lzUTBGQldqdEJRVU5CTEZWQlFVOHNUVUZCVUN4SFFVRm5RaXhQUVVGUExFdEJRVkFzUjBGQlpTeEZRVUV2UWp0QlFVTkJMRTlCUVUwc1RVRkJUU3hQUVVGUExGVkJRVkFzUTBGQmEwSXNTVUZCYkVJc1EwRkJXanRCUVVOQkxFOUJRVWtzVFVGQlNpeEhRVUZoTEZsQlFWYzdRVUZCUlR0QlFVTjZRaXhSUVVGSkxGTkJRVW9zUTBGQll5eEpRVUZrTEVWQlFXOUNMRU5CUVhCQ0xFVkJRWFZDTEVOQlFYWkNPMEZCUTBFc1VVRkJTU3hYUVVGS0xFZEJRV3RDTEVsQlFXeENPMEZCUTBFc1VVRkJTU3hUUVVGS0xFZEJRV2RDTEZOQlFXaENPMEZCUTBFc1VVRkJTU3hKUVVGS0xFTkJRVk1zUTBGQlZDeEZRVUZaTEVOQlFWb3NSVUZCWlN4RlFVRm1MRVZCUVcxQ0xFVkJRVzVDTzBGQlEwRXNVVUZCU1N4SlFVRktPMEZCUTBFc1dVRkJVU3hKUVVGU0xFZEJRV1VzVDBGQlR5eFRRVUZRTEVOQlFXbENMRmRCUVdwQ0xFTkJRV1k3UVVGRFFTeFpRVUZSTEZOQlFWSXNTVUZCY1VJc1lVRkJja0k3UVVGRFFTeEpRVkpFTzBGQlUwRXNUMEZCU1N4SFFVRktMRWRCUVZVc1VVRkJVU3hKUVVGc1FqdEJRVU5CTzBGQlEwUTdPMEZCUlVRN096czdPMEZCUzBFc1ZVRkJVeXh0UWtGQlZDeEhRVUVyUWp0QlFVTTVRaXhOUVVGSkxFbEJRVWtzU1VGQlNpeERRVUZUTEUxQlFWUXNRMEZCWjBJc1IwRkJhRUlzUTBGQmIwSXNZVUZCY0VJc1RVRkJkVU1zV1VGQk0wTXNSVUZCZVVRN1FVRkRlRVE3UVVGRFFTeFBRVUZKTEVsQlFVa3NTVUZCU2l4RFFVRlRMRXRCUVZRc1MwRkJiVUlzVTBGQmRrSXNSVUZCYTBNN1FVRkRha01zVVVGQlNTeEpRVUZLTEVOQlFWTXNTMEZCVkN4RFFVRmxMRXRCUVdZc1EwRkJjVUlzZVVKQlFYSkNMRVZCUVdkRUxGTkJRV2hFTzBGQlEwRXNTVUZHUkN4TlFVVlBPMEZCUTA0c1dVRkJVU3hIUVVGU0xFTkJRVmtzZVVKQlFWb3NSVUZCZFVNc1UwRkJka003UVVGRFFUczdRVUZGUkR0QlFVTkJMRTlCUVUwc1VVRkJVU3hsUVVGa08wRkJRMEVzVDBGQlNTeFJRVUZSTEU5QlFVOHNVVUZCVUN4RFFVRm5RaXhMUVVFMVFqdEJRVU5CTEU5QlFVa3NZVUZCWVN4RFFVRnFRanM3UVVGRlFUdEJRVU5CTEU5QlFVa3NUVUZCVFN4TFFVRk9MRU5CUVZrc1MwRkJXaXhOUVVGMVFpeEpRVUV6UWl4RlFVRnBRenRCUVVOb1F5eHBRa0ZCWVN4VFFVRlRMRTFCUVUwc1MwRkJUaXhEUVVGWkxFdEJRVm9zUlVGQmJVSXNRMEZCYmtJc1EwRkJWQ3hGUVVGblF5eEZRVUZvUXl4SlFVRnpReXhEUVVGdVJEdEJRVU5CTEZsQlFWRXNUVUZCVFN4UFFVRk9MRU5CUVdNc1MwRkJaQ3hGUVVGeFFpeEZRVUZ5UWl4RFFVRlNPMEZCUTBFN08wRkJSVVE3UVVGRFFTeFhRVUZSTEZGQlFWRXNWVUZCVWl4SFFVRnhRaXhKUVVGeVFpeEhRVUUwUWl4TFFVRndRenRCUVVOQkxGVkJRVThzVVVGQlVDeERRVUZuUWl4TFFVRm9RaXhIUVVGM1FpeExRVUY0UWpzN1FVRkZRVHRCUVVOQk8wRkJRMEU3TzBGQlJVUXNVMEZCVHl4SlFVRlFPMEZCUTBFN08wRkJSVVE3T3pzN096czdPMEZCVVVFc1ZVRkJVeXhSUVVGVUxFTkJRV3RDTEUxQlFXeENMRVZCUVRCQ0xFbEJRVEZDTEVWQlFXZERPMEZCUXk5Q0xFMUJRVTBzYTBKQlFXdENMRTlCUVU4c1QwRkJVQ3hEUVVGbExFMUJRV1lzUTBGQmVFSTdRVUZEUVN4TlFVRk5MR3RDUVVGclFpeFBRVUZQTEU5QlFWQXNRMEZCWlN4SlFVRkpMRWxCUVVvc1EwRkJVeXhOUVVGVUxFTkJRV2RDTEVkQlFXaENMRU5CUVc5Q0xFOUJRWEJDTEVOQlFXWXNRMEZCZUVJN1FVRkRRU3hOUVVGSkxHZENRVUZuUWl4SlFVRndRanM3UVVGRlFTeE5RVUZKTEcxQ1FVRnRRaXhsUVVGMlFpeEZRVUYzUXp0QlFVTjJReXh0UWtGQlowSXNUMEZCVHl4WFFVRlFMRVZCUVdoQ096dEJRVVZCTEZkQlFWRXNZVUZCVWp0QlFVTkRMRk5CUVVzc1QwRkJURHRCUVVORExGZEJRVTBzUzBGQlN5eFRRVUZNTEVOQlFXVXNTVUZCWml4RFFVRk9PMEZCUTBFN08wRkJSVVFzVTBGQlN5eFJRVUZNTzBGQlEwTXNVMEZCVFN4dlFrRkJiMElzUlVGQlJTeHhRa0ZCUml4RFFVRXhRanM3UVVGRlFTeFRRVUZKTEVOQlFVTXNhMEpCUVd0Q0xFMUJRWFpDTEVWQlFTdENPMEZCUXpsQ0xGRkJRVVVzVTBGQlJpeEZRVU5GTEZGQlJFWXNRMEZEVnl4dlFrRkVXQ3hGUVVWRkxFZEJSa1lzUTBGRlRUdEJRVU5LTEdsQ1FVRlZMRTlCUkU0N1FVRkZTaXhaUVVGTExFTkJSa1E3UVVGSFNpeGhRVUZOTEVOQlNFWTdRVUZKU2l4clFrRkJWeXhMUVVwUU8wRkJTMG9zYVVKQlFWVXNUMEZNVGp0QlFVMUtMR2xDUVVGVkxFOUJUazQ3UVVGUFNpeDNRa0ZCYVVJc1UwRlFZanRCUVZGS0xHVkJRVkVzVFVGU1NqdEJRVk5LTEdsQ1FVRlZPMEZCVkU0c1QwRkdUaXhGUVdGRkxGRkJZa1lzUTBGaFZ5eEZRVUZGTEUxQlFVWXNRMEZpV0R0QlFXTkJPenRCUVVWRUxIVkNRVUZyUWl4TlFVRnNRaXhEUVVGNVFpeFJRVUZSTEV0QlFVc3NVMEZCVEN4RFFVRmxMRWxCUVdZc1EwRkJVaXhIUVVFclFpeE5RVUY0UkR0QlFVTkJPenRCUVVWRU8wRkJRME1zVTBGQlNTeFpRVUZaTEZOQlFXaENMRVZCUVRKQ08wRkJRekZDTEdGQlJEQkNMRU5CUTJ4Q08wRkJRMUk3TzBGQlJVUXNVMEZCU1N4UFFVRlBMRkZCUVZFc1lVRkJVaXhGUVVGMVFpeExRVUU1UWl4TFFVRjNReXhWUVVGNFF5eEpRVUZ6UkN4UFFVRlBMRkZCUVZFc1IwRkJVaXhEUVVGWkxFdEJRVzVDTEV0QlFUWkNMRlZCUVhaR0xFVkJRVzFITzBGQlEyeEhMRlZCUVVrc1VVRkJVU3hoUVVGU0xFMUJRVEpDTEZOQlFTOUNMRVZCUVRCRE8wRkJRM3BETEdWQlFWRXNZVUZCVWl4RlFVRjFRaXhMUVVGMlFpeERRVUUyUWl4UFFVRTNRaXhGUVVGelF5eEpRVUYwUXp0QlFVTkJMRTlCUmtRc1RVRkZUenRCUVVOT0xHVkJRVkVzUjBGQlVpeERRVUZaTEV0QlFWb3NRMEZCYTBJc1QwRkJiRUlzUlVGQk1rSXNTVUZCTTBJN1FVRkRRVHRCUVVORUxFMUJUa1FzVFVGTlR6dEJRVU5PTEdOQlFWRXNSMEZCVWl4RFFVRlpMRWxCUVZvN1FVRkRRVHRCUVhwRFNEdEJRVEpEUVR0QlFVTkVPenRCUVVWRU96czdRVUZIUVN4VFFVRlJMSE5DUVVGU0xFZEJRV2xETEZsQlFWYzdRVUZETTBNc1UwRkJUeXhQUVVGUUxFZEJRV2xDTEcxQ1FVRnFRanRCUVVOQkxFVkJSa1E3TzBGQlNVRTdPenM3TzBGQlMwRXNVMEZCVVN4TFFVRlNMRWRCUVdkQ0xGbEJRVmM3UVVGRE1VSXNWMEZCVXl4VlFVRlVMRVZCUVhGQ0xGTkJRWEpDTzBGQlEwRXNSVUZHUkRzN1FVRkpRVHM3T3pzN1FVRkxRU3hUUVVGUkxFbEJRVklzUjBGQlpTeFpRVUZYTzBGQlEzcENMRmRCUVZNc1UwRkJWQ3hGUVVGdlFpeFRRVUZ3UWp0QlFVTkJMRVZCUmtRN08wRkJTVUU3T3pzN08wRkJTMEVzVTBGQlVTeEhRVUZTTEVkQlFXTXNXVUZCVnp0QlFVTjRRaXhYUVVGVExGRkJRVlFzUlVGQmJVSXNVMEZCYmtJN1FVRkRRU3hGUVVaRU96dEJRVWxCT3pzN096dEJRVXRCTEZOQlFWRXNTVUZCVWl4SFFVRmxMRmxCUVZjN1FVRkRla0lzVjBGQlV5eFRRVUZVTEVWQlFXOUNMRk5CUVhCQ08wRkJRMEVzUlVGR1JEczdRVUZKUVRzN096czdRVUZMUVN4VFFVRlJMRXRCUVZJc1IwRkJaMElzV1VGQlZ6dEJRVU14UWl4WFFVRlRMRlZCUVZRc1JVRkJjVUlzVTBGQmNrSTdRVUZEUVN4RlFVWkVPenRCUVVsQk96czdPenRCUVV0QkxGTkJRVkVzUzBGQlVpeEhRVUZuUWl4WlFVRlhPMEZCUXpGQ0xGZEJRVk1zVlVGQlZDeEZRVUZ4UWl4VFFVRnlRanRCUVVOQkxFVkJSa1E3TzBGQlNVRTdPenM3TzBGQlMwRXNVMEZCVVN4TlFVRlNMRWRCUVdsQ0xGbEJRVmM3UVVGRE0wSXNWMEZCVXl4WFFVRlVMRVZCUVhOQ0xGTkJRWFJDTzBGQlEwRXNSVUZHUkR0QlFVbEJMRU5CZGxGQkxFVkJkVkZETEVsQlFVa3NTVUZCU2l4RFFVRlRMRXRCZGxGV0xFTkJRVVE3T3pzN08wRkRjRUpCT3pzN096czdPenM3TzBGQlZVRXNTVUZCU1N4SlFVRktMRU5CUVZNc1RVRkJWQ3hIUVVGclFpeEpRVUZKTEVsQlFVb3NRMEZCVXl4TlFVRlVMRWxCUVcxQ0xFVkJRWEpET3p0QlFVVkJPenM3T3pzN08wRkJUMEVzUTBGQlF5eFZRVUZUTEU5QlFWUXNSVUZCYTBJN08wRkJSV3hDT3p0QlFVVkJPMEZCUTBFN1FVRkRRVHM3UVVGRlFUczdPenM3T3pzN096czdPMEZCVjBFc1ZVRkJVeXhsUVVGVUxFTkJRWGxDTEZkQlFYcENMRVZCUVhORE8wRkJRM0pETEUxQlFVa3NjVUpCUVhGQ0xFVkJRWHBDT3p0QlFVVkJPMEZCUTBFc1RVRkJTU3hSUVVGUkxFMUJRVTBzU1VGQlRpeERRVUZYTEZOQlFWTXNiMEpCUVZRc1EwRkJPRUlzUjBGQk9VSXNRMEZCV0N4RFFVRmFPMEZCUVVFc1RVRkRReXhSUVVGUkxIRkNRVVJVT3p0QlFVcHhRenRCUVVGQk8wRkJRVUU3TzBGQlFVRTdRVUZQY2tNc2QwSkJRV2xDTEV0QlFXcENMRGhJUVVGM1FqdEJRVUZCTEZGQlFXWXNTVUZCWlR0QlFVRkJPMEZCUVVFN1FVRkJRVHM3UVVGQlFUdEJRVU4yUWl3eVFrRkJjMElzVFVGQlRTeEpRVUZPTEVOQlFWY3NTMEZCU3l4VlFVRm9RaXhEUVVGMFFpeHRTVUZCYlVRN1FVRkJRU3hWUVVFeFF5eFRRVUV3UXpzN1FVRkRiRVFzVlVGQlNTeFZRVUZWTEVsQlFWWXNRMEZCWlN4TlFVRm1MRU5CUVhOQ0xFdEJRWFJDTEUxQlFXbERMRU5CUVVNc1EwRkJkRU1zUlVGQmVVTTdRVUZEZUVNN1FVRkRRU3hYUVVGSkxFOUJRVThzVlVGQlZTeEpRVUZXTEVOQlFXVXNUMEZCWml4RFFVRjFRaXhMUVVGMlFpeEZRVUU0UWl4SlFVRTVRaXhEUVVGWU8wRkJRVUVzVjBGRFF5eFRRVUZUTEZWQlFWVXNTMEZFY0VJN08wRkJSMEU3UVVGRFFTeFhRVUZKTEcxQ1FVRnRRaXhQUVVGdVFpeERRVUV5UWl4SlFVRXpRaXhKUVVGdFF5eERRVUZETEVOQlFYaERMRVZCUVRKRE8wRkJRekZETEZsQlFVa3NUMEZCVHl4SlFVRlFMRVZCUVdFc1RVRkJZaXhMUVVGM1FpeE5RVUUxUWl4RlFVRnZRenRCUVVOdVF5eGhRVUZKTEVsQlFVb3NRMEZCVXl4TFFVRlVMRU5CUVdVc1MwRkJaaXhwUkVGQmJVVXNTVUZCYmtVN1FVRkRRU3hsUVVGTkxFbEJRVWtzUzBGQlNpeERRVUZWTEc5Q1FVRnJRaXhKUVVGc1FpdzRSVUZCVml4RFFVRk9PMEZCUlVFN1FVRkRSQ3hwUWtGT01FTXNRMEZOYUVNN1FVRkRWanM3UVVGRlJDeFhRVUZKTEZkQlFWY3NSVUZCWml4RlFVRnRRanRCUVVOc1FpeGpRVUZOTEVsQlFVa3NWMEZCU2l4cFEwRkJPRU1zU1VGQk9VTXNRMEZCVGp0QlFVTkJPenRCUVVWRU8wRkJRMEU3UVVGRFFTeFhRVUZKTEZOQlFWTXNTMEZCWWl4RlFVRnZRanRCUVVGRk8wRkJRM0pDTEd0RFFVRXdRaXhOUVVFeFFpeEZRVUZyUXl4WFFVRnNRenRCUVVOQkxGRkJSa1FzVFVGRlR6dEJRVU5PTEdWQlFVOHNTVUZCVUN4SlFVRmxMRWxCUVVrc1NVRkJTU3haUVVGS0xFTkJRV2xDTEZOQlFYSkNMRU5CUVN0Q0xFbEJRUzlDTEVWQlFYRkRMRTFCUVhKRExFVkJRVFpETEZkQlFUZERMRU5CUVdZN1FVRkRRVHM3UVVGRlJDd3dRa0ZCYlVJc1NVRkJia0lzUTBGQmQwSXNTVUZCZUVJN1FVRkRRU3haUVVGTExHVkJRVXdzUTBGQmNVSXNWVUZCVlN4SlFVRXZRanRCUVVOQk8wRkJRMFE3UVVGb1EzTkNPMEZCUVVFN1FVRkJRVHRCUVVGQk8wRkJRVUU3UVVGQlFUdEJRVUZCTzBGQlFVRTdRVUZCUVR0QlFVRkJPMEZCUVVFN1FVRkJRVHRCUVVGQk8wRkJRVUU3UVVGcFEzWkNPenRCUVVWRU8wRkJNVU54UXp0QlFVRkJPMEZCUVVFN1FVRkJRVHRCUVVGQk8wRkJRVUU3UVVGQlFUdEJRVUZCTzBGQlFVRTdRVUZCUVR0QlFVRkJPMEZCUVVFN1FVRkJRVHRCUVVGQk96dEJRVEpEY2tNc1RVRkJTU3h0UWtGQmJVSXNUVUZCYmtJc1MwRkJPRUlzUTBGQmJFTXNSVUZCY1VNN1FVRkRjRU1zVTBGQlRTeEpRVUZKTEV0QlFVb3NRMEZCVlN3clJVRkRaaXh0UWtGRVN5eERRVUZPTzBGQlJVRTdPMEZCUlVRN1FVRkRRU3hOUVVGSkxIRkNRVUZ4UWl4RlFVRjZRanM3UVVGcVJIRkRPMEZCUVVFN1FVRkJRVHM3UVVGQlFUdEJRVUZCTzBGQlFVRXNVVUZ0UkRWQ0xFbEJia1EwUWpzN1FVRnZSSEJETEZGQlFVa3NWMEZCVnl4RlFVRkZMRkZCUVVZc1JVRkJaanM3UVVGRlFTeDFRa0ZCYlVJc1NVRkJia0lzUTBGQmQwSXNVVUZCZUVJN08wRkJSVUVzVjBGQlR5eEpRVUZRTEVWQlEwVXNTVUZFUml4SFFVVkZMRWxCUmtZc1EwRkZUeXhUUVVGVExFOUJSbWhDTEVWQlIwVXNTVUZJUml4RFFVZFBMRk5CUVZNc1RVRklhRUlzUlVGSlJTeE5RVXBHTEVOQlNWTTdRVUZCUVN4WlFVRlBMRWxCUVVrc1NVRkJTaXhEUVVGVExFdEJRVlFzUTBGQlpTeEpRVUZtTEVOQlFXOUNMRzlEUVVGd1FpeEZRVUV3UkN4SlFVRXhSQ3hEUVVGUU8wRkJRVUVzUzBGS1ZEdEJRWGhFYjBNN08wRkJiVVJ5UXl4NVFrRkJhVUlzYTBKQlFXcENMRzFKUVVGeFF6dEJRVUZCTzBGQlZYQkRPenRCUVVWRU8wRkJMMFJ4UXp0QlFVRkJPMEZCUVVFN1FVRkJRVHRCUVVGQk8wRkJRVUU3UVVGQlFUdEJRVUZCTzBGQlFVRTdRVUZCUVR0QlFVRkJPMEZCUVVFN1FVRkJRVHRCUVVGQk96dEJRV2RGY2tNc1NVRkJSU3hKUVVGR0xFTkJRVThzUzBGQlVDeERRVUZoTEZOQlFXSXNSVUZCZDBJc2EwSkJRWGhDTEVWQlFUUkRMRTFCUVRWRExFTkJRVzFFTEZsQlFWYzdRVUZETjBRc1QwRkJTU3hSUVVGUkxGTkJRVk1zVjBGQlZDeERRVUZ4UWl4UFFVRnlRaXhEUVVGYU8wRkJRMEVzVTBGQlRTeFRRVUZPTEVOQlFXZENMSGRDUVVGb1FpeEZRVUV3UXl4SlFVRXhReXhGUVVGblJDeEpRVUZvUkR0QlFVTkJMRmxCUVZNc1lVRkJWQ3hEUVVGMVFpeE5RVUYyUWl4RlFVRXJRaXhoUVVFdlFpeERRVUUyUXl4TFFVRTNRenRCUVVOQkxFOUJRVWtzU1VGQlNpeERRVUZUTEZGQlFWUXNRMEZCYTBJc1IwRkJiRUlzUTBGQmMwSXNXVUZCZEVJc1JVRkJiME1zU1VGQlNTeEpRVUZLTEVkQlFWY3NUMEZCV0N4RlFVRndRenRCUVVOQkxFOUJRVWtzU1VGQlNpeERRVUZUTEV0QlFWUXNRMEZCWlN4SlFVRm1MRU5CUVc5Q0xEQkNRVUZ3UWl4RlFVRm5SQ3hKUVVGSkxFbEJRVW9zUTBGQlV5eFJRVUZVTEVOQlFXdENMRWRCUVd4Q0xFTkJRWE5DTEZsQlFYUkNMRWxCUXpkRExFbEJRVWtzU1VGQlNpeERRVUZUTEZGQlFWUXNRMEZCYTBJc1IwRkJiRUlzUTBGQmMwSXNZMEZCZEVJc1EwRkVTQ3hGUVVNd1F5eEpRVVF4UXp0QlFVVkJMRTlCUVVrc1QwRkJUeXhQUVVGWUxFVkJRVzlDTzBGQlEyNUNMRmRCUVU4c1VVRkJVQ3hIUVVGclFpeEpRVUZzUWp0QlFVTkJPMEZCUTBRc1IwRldSRHM3UVVGWlFTeFRRVUZQTEd0Q1FVRlFPMEZCUTBFN08wRkJSVVE3T3pzN096czdPenM3T3p0QlFWbEJMRlZCUVZNc2VVSkJRVlFzUTBGQmJVTXNUVUZCYmtNc1JVRkJNa01zVjBGQk0wTXNSVUZCZDBRN1FVRkRka1FzVFVGQlNTeGxRVUZsTEVsQlFVa3NTVUZCU1N4WlFVRktMRU5CUVdsQ0xGTkJRWEpDTEVOQlFTdENMRXRCUVM5Q0xFVkJRWE5ETEUxQlFYUkRMRVZCUVRoRExGZEJRVGxETEVOQlFXNUNPMEZCUTBFc1RVRkJTU3hKUVVGS0xFZEJRVmNzWVVGQllTeEpRVUY0UWp0QlFVTkJMRTFCUVVrc1RVRkJTaXhIUVVGaExHRkJRV0VzVFVGQk1VSTdRVUZEUVN4TlFVRkpMRmRCUVVvc1IwRkJhMElzWVVGQllTeFhRVUV2UWp0QlFVTkJMRTFCUVVrc1NVRkJTaXhIUVVGWExFbEJRVWtzV1VGQlNpeERRVUZwUWl4VFFVRnFRaXhEUVVFeVFpeFRRVUV6UWl4RFFVRnhReXhKUVVGb1JEdEJRVU5CT3p0QlFVVkVPMEZCUTBFN1FVRkRRVHM3UVVGRlFUczdPenM3UVVGTFFTeFRRVUZSTEVsQlFWSXNSMEZCWlN4VlFVRlZMRmRCUVZZc1JVRkJkVUk3UVVGRGNrTTdRVUZEUVN4TlFVRkpMRWxCUVVvc1EwRkJVeXhMUVVGVUxFTkJRV1VzYzBKQlFXWTdPMEZCUlVFN1FVRkRRU3hOUVVGSkxIRkNRVUZ4UWl4blFrRkJaMElzVjBGQmFFSXNRMEZCZWtJN08wRkJSVUU3UVVGRFFTeE5RVUZKTEVsQlFVb3NRMEZCVXl4TFFVRlVMRU5CUVdVc1NVRkJaaXhEUVVGdlFpeHpRa0ZCYzBJc2JVSkJRVzFDTEVsQlFXNUNMRVZCUVRGRE96dEJRVVZCTzBGQlEwRXNUVUZCU1N4SlFVRktMRU5CUVZNc1VVRkJWQ3hEUVVGclFpeEhRVUZzUWl4RFFVRnpRaXhaUVVGMFFpeEZRVUZ2UXl4clFrRkJjRU03UVVGRFFTeEZRVnBFTzBGQlkwRXNRMEUzU1VRc1JVRTJTVWNzU1VGQlNTeEpRVUZLTEVOQlFWTXNUVUUzU1ZvN096czdPMEZEYmtKQk96czdPenM3T3pzN08wRkJWVUU3T3pzN096czdRVUZQUXl4aFFVRlpPenRCUVVWYU96dEJRVVZCTzBGQlEwRTdRVUZEUVRzN1FVRkZRU3hIUVVGRkxFVkJRVVlzUTBGQlN5eE5RVUZNTEVOQlFWazdRVUZEV0N4dFFrRkJhVUlzZVVKQlFWTXNWVUZCVkN4RlFVRnhRanRCUVVOeVF5eFBRVUZKTEVOQlFVTXNWVUZCUkN4SlFVRmxMR1ZCUVdVc1JVRkJiRU1zUlVGQmMwTTdRVUZEY2tNc1ZVRkJUU3hKUVVGSkxFdEJRVW9zUTBGQlZTdzRRMEZCVml4RFFVRk9PMEZCUTBFN08wRkJSVVFzVDBGQlRTeGpRVUZqTEVWQlFVVXNTVUZCUml4RlFVRlJMRWxCUVZJc1JVRkJjRUk3UVVGRFFTeFBRVUZOTEdWQlFXVXNSVUZCY2tJN08wRkJSVUU3UVVGRFFUdEJRVU5CTEV0QlFVVXNTVUZCUml4RFFVRlBMRmRCUVZBc1JVRkJiMElzVlVGQlZTeEhRVUZXTEVWQlFXVXNTMEZCWml4RlFVRnpRanRCUVVONlF5eFJRVUZKTEVsQlFVa3NUMEZCU2l4RFFVRlpMRlZCUVZvc1RVRkJORUlzUTBGQk5VSXNTVUZCYVVNc1NVRkJTU3hQUVVGS0xFTkJRVmtzVjBGQlZ5eFhRVUZZTEVWQlFWb3NUVUZCTUVNc1EwRkJMMFVzUlVGQmEwWTdRVUZEYWtZc1UwRkJTU3hUUVVGVExFbEJRVWtzVFVGQlNpeERRVUZYTEZkQlFWY3NUVUZCZEVJc1EwRkJZanRCUVVOQkxHTkJRVk1zVDBGQlR5eE5RVUZRTEVOQlFXTXNRMEZCWkN4RlFVRnBRaXhEUVVGcVFpeEZRVUZ2UWl4WFFVRndRaXhMUVVGdlF5eFBRVUZQTEUxQlFWQXNRMEZCWXl4RFFVRmtMRU5CUVRkRE8wRkJRMEVzYTBKQlFXRXNUVUZCWWl4SlFVRjFRaXhMUVVGMlFqdEJRVU5CTzBGQlEwUXNTVUZPUkRzN1FVRlJRU3hWUVVGUExGbEJRVkE3UVVGRFFUdEJRWEJDVlN4RlFVRmFPenRCUVhWQ1FUdEJRVU5CTzBGQlEwRTdPMEZCUlVFc1MwRkJTU3hGUVVGRkxGVkJRVVlzUzBGQmFVSXNVMEZCY2tJc1JVRkJaME03UVVGREwwSXNTVUZCUlN4VlFVRkdMRU5CUVdFc1VVRkJZaXhEUVVGelFpeEZRVUYwUWl4SFFVRXlRanRCUVVNeFFpeGxRVUZaTEZWQlJHTTdRVUZGTVVJc1lVRkJWU3hEUVVablFqdEJRVWN4UWl4VlFVRlBPMEZCU0cxQ0xFZEJRVE5DTzBGQlMwRXNTVUZCUlN4VlFVRkdMRU5CUVdFc1YwRkJZaXhEUVVGNVFpeEZRVUZGTEZWQlFVWXNRMEZCWVN4UlFVRmlMRU5CUVhOQ0xFVkJRUzlETzBGQlEwRTdRVUZEUkN4RFFUTkRRU3hIUVVGRU96czdRVU5xUWtFN096czdPenM3T3pzN1FVRlZRVHM3UVVGRlFUczdPenM3T3pzN096czdPenM3T3pzN096czdPenM3T3pzN096czdRVUUyUWtFN1FVRkRRVHM3UVVGRFFTeFBRVUZQTEVkQlFWQXNSMEZCWVR0QlFVTmFMRkZCUVUwc1JVRkVUVHRCUVVWYUxGRkJRVTBzUlVGR1RUdEJRVWRhTEdkQ1FVRmpPMEZCU0VZc1EwRkJZanM3UVVGTlFUdEJRVU5CTEZOQlFWTXNaMEpCUVZRc1EwRkJNRUlzYTBKQlFURkNMRVZCUVRoRExGbEJRVmM3UVVGRGVFUXNUVUZCU1R0QlFVTklPMEZCUTBFc1VVRkJTU3hQUVVGUExIRkNRVUZRTEV0QlFXbERMRk5CUVhKRExFVkJRV2RFTzBGQlF5OURMRmxCUVUwc1NVRkJTU3hMUVVGS0xFTkJRVlVzYlVaQlEyWXNaMFZCUkVzc1EwRkJUanRCUVVWQk96dEJRVVZFTzBGQlEwRXNVVUZCU1N4SlFVRktMRU5CUVZNc1RVRkJWQ3hEUVVGblFpeEpRVUZvUWl4RFFVRnhRaXhQUVVGUExIRkNRVUUxUWpzN1FVRkZRVHRCUVVOQkxGRkJRVWtzU1VGQlNpeERRVUZUTEZGQlFWUXNRMEZCYTBJc1IwRkJiRUlzUTBGQmMwSXNZMEZCZEVJc1JVRkJjME1zUzBGQlN5eEhRVUZNTEVWQlFYUkRPenRCUVVWQk8wRkJRMEVzVVVGQlNTeEpRVUZLTEVOQlFWTXNUVUZCVkN4RFFVRm5RaXhKUVVGb1FpeERRVUZ4UWl4SlFVRkpMRWxCUVVvc1EwRkJVeXhOUVVGVUxFTkJRV2RDTEVkQlFXaENMRU5CUVc5Q0xHRkJRWEJDTEVOQlFYSkNPMEZCUTBFc1IwRm1SQ3hEUVdWRkxFOUJRVThzVTBGQlVDeEZRVUZyUWp0QlFVTnVRaXhSUVVGSkxFbEJRVW9zUTBGQlV5eExRVUZVTEVOQlFXVXNTMEZCWml4RFFVRnhRaXh0UkVGQmNrSXNSVUZCTUVVc1UwRkJNVVU3UVVGRFFUdEJRVU5CTEZGQlFVMHNVVUZCVVN4VFFVRlRMRmRCUVZRc1EwRkJjVUlzWVVGQmNrSXNRMEZCWkR0QlFVTkJMRlZCUVUwc1pVRkJUaXhEUVVGelFpeFBRVUYwUWl4RlFVRXJRaXhKUVVFdlFpeEZRVUZ4UXl4SlFVRnlReXhGUVVFeVF5eFRRVUV6UXp0QlFVTkJMRmRCUVU4c1lVRkJVQ3hEUVVGeFFpeExRVUZ5UWp0QlFVTkJPMEZCUTBRc1EwRjJRa1E3T3pzN096czdRVU5zUkVFN096czdPenM3T3pzN1FVRlZRU3hKUVVGSkxFbEJRVW9zUTBGQlV5eEpRVUZVTEVkQlFXZENMRWxCUVVrc1NVRkJTaXhEUVVGVExFbEJRVlFzU1VGQmFVSXNSVUZCYWtNN08wRkJSVUU3T3pzN096czdPenM3T3pzN096czdPenM3TzBGQmIwSkRMRmRCUVZVc1QwRkJWaXhGUVVGdFFqczdRVUZGYmtJN08wRkJSVUU3UVVGRFFUdEJRVU5CT3p0QlFVVkJPenM3T3pzN1FVRkxRU3hMUVVGTkxGZEJRVmNzUlVGQmFrSTdPMEZCUlVFN1FVRkRRVHRCUVVOQk96dEJRVVZCT3pzN096czdPenRCUVZGQkxGTkJRVkVzVlVGQlVpeEhRVUZ4UWl4VlFVRlZMRWxCUVZZc1JVRkJaMElzV1VGQmFFSXNSVUZCT0VJN1FVRkRiRVFzVFVGQlNTeFBRVUZQTEVsQlFWQXNTMEZCWjBJc1VVRkJhRUlzU1VGQk5FSXNVVUZCVHl4WlFVRlFMSGxEUVVGUExGbEJRVkFzVDBGQmQwSXNVVUZCY0VRc1NVRkJaMFVzYVVKQlFXbENMRWxCUVhKR0xFVkJRVEpHTzBGQlF6RkdMRk5CUVUwc1NVRkJTU3hMUVVGS0xFTkJRVlVzSzBWQlFUUkZMRWxCUVRWRkxIbERRVUUwUlN4SlFVRTFSU3gzUTBGRE1FSXNXVUZFTVVJc2VVTkJRekJDTEZsQlJERkNMRlZCUVZZc1EwRkJUanRCUVVWQk8wRkJRMFFzVjBGQlV5eEpRVUZVTEVsQlFXbENMRmxCUVdwQ08wRkJRMEVzUlVGT1JEczdRVUZSUVRzN096czdPenRCUVU5QkxGTkJRVkVzVjBGQlVpeEhRVUZ6UWl4WlFVRlpPMEZCUTJwRExFMUJRVTBzVTBGQlV5eEZRVUZtT3p0QlFVVkJMRTlCUVVzc1NVRkJTU3hQUVVGVUxFbEJRVzlDTEZGQlFYQkNMRVZCUVRoQ08wRkJRemRDTEZWQlFVOHNTVUZCVUN4RFFVRlpMRTlCUVZvN1FVRkRRVHM3UVVGRlJDeFRRVUZQTEUxQlFWQTdRVUZEUVN4RlFWSkVPenRCUVZWQk96czdPenM3T3pzN096dEJRVmRCTEZOQlFWRXNVMEZCVWl4SFFVRnZRaXhWUVVGVkxFMUJRVllzUlVGQmEwSXNUMEZCYkVJc1JVRkJNa0k3UVVGRE9VTTdRVUZEUVN4TlFVRkpMRTlCUVU4c1RVRkJVQ3hMUVVGclFpeFJRVUZzUWl4SlFVRTRRaXhQUVVGUExFOUJRVkFzUzBGQmJVSXNVVUZCY2tRc1JVRkJLMFE3UVVGRE9VUXNVMEZCVFN4SlFVRkpMRXRCUVVvc1EwRkJWU3h4UlVGQmEwVXNUVUZCYkVVc2VVTkJRV3RGTEUxQlFXeEZMRzFEUVVOeFFpeFBRVVJ5UWl4NVEwRkRjVUlzVDBGRWNrSXNWVUZCVml4RFFVRk9PMEZCUlVFN08wRkJSVVE3UVVGRFFTeE5RVUZKTEZOQlFWTXNUMEZCVkN4TlFVRnpRaXhUUVVGMFFpeEpRVUZ0UXl4VFFVRlRMRTlCUVZRc1JVRkJhMElzVFVGQmJFSXNUVUZCT0VJc1UwRkJja1VzUlVGQlowWTdRVUZETDBVc1QwRkJTU3hKUVVGS0xFTkJRVk1zUzBGQlZDeERRVUZsTEVsQlFXWXNjVVJCUVhORkxFMUJRWFJGTEcxQ1FVRXdSaXhQUVVFeFJqdEJRVU5CTEZWQlFVOHNUVUZCVFN4UFFVRk9MRWRCUVdkQ0xFZEJRV2hDTEVkQlFYTkNMRTFCUVhSQ0xFZEJRU3RDTEVkQlFYUkRPMEZCUTBFN08wRkJSVVFzVTBGQlR5eFRRVUZUTEU5QlFWUXNSVUZCYTBJc1RVRkJiRUlzUTBGQlVEdEJRVU5CTEVWQlpFUTdRVUZuUWtFc1EwRXZSVUVzUlVFclJVTXNTVUZCU1N4SlFVRktMRU5CUVZNc1NVRXZSVllzUTBGQlJEczdPenM3UVVOeVFrRTdPMEZCUjBFN08wRkJRMEU3TzBGQlEwRTdPMEZCUTBFN08wRkJSMEU3TzBGQlEwRTdPMEZCUTBFN08wRkJRMEU3TzBGQlEwRTdPMEZCUTBFN08wRkJRMEU3TzBGQlEwRTdPMEZCUTBFN08wRkJRMEU3T3pzN08wRkROMEpCT3pzN096czdPenM3TzBGQlZVRXNTVUZCU1N4SlFVRktMRU5CUVZNc1lVRkJWQ3hIUVVGNVFpeEpRVUZKTEVsQlFVb3NRMEZCVXl4aFFVRlVMRWxCUVRCQ0xFVkJRVzVFT3p0QlFVVkJPenM3T3pzN096czdPMEZCVlVFc1EwRkJReXhWUVVGVkxFOUJRVllzUlVGQmJVSTdPMEZCUlc1Q096dEJRVVZCTzBGQlEwRTdRVUZEUVRzN1FVRkZRVHM3T3pzN096czdRVUZQUVN4VlFVRlRMRkZCUVZRc1EwRkJhMElzUjBGQmJFSXNSVUZCZFVJN1FVRkRkRUlzVFVGQlRTeFBRVUZQTEZOQlFWTXNZVUZCVkN4RFFVRjFRaXhOUVVGMlFpeERRVUZpTzBGQlEwRXNUMEZCU3l4SlFVRk1MRWRCUVZrc1ZVRkJXanRCUVVOQkxFOUJRVXNzUjBGQlRDeEhRVUZYTEZsQlFWZzdRVUZEUVN4UFFVRkxMRWxCUVV3c1IwRkJXU3hIUVVGYU8wRkJRMEVzVjBGQlV5eHZRa0ZCVkN4RFFVRTRRaXhOUVVFNVFpeEZRVUZ6UXl4RFFVRjBReXhGUVVGNVF5eFhRVUY2UXl4RFFVRnhSQ3hKUVVGeVJEdEJRVU5CT3p0QlFVVkVPMEZCUTBFN1FVRkRRVHM3UVVGRlFUczdPenM3T3pzN1FVRlJRU3hUUVVGUkxFbEJRVklzUjBGQlpTeFpRVUZaTzBGQlF6RkNMRTFCUVVrc1dVRkJXU3hGUVVGb1FqczdRVUZGUVN4TlFVRkpMRWxCUVVrc1NVRkJTaXhEUVVGVExFMUJRVlFzUTBGQlowSXNSMEZCYUVJc1EwRkJiMElzWVVGQmNFSXNUVUZCZFVNc1dVRkJka01zU1VGQmRVUXNTVUZCU1N4SlFVRktMRU5CUVZNc1RVRkJWQ3hEUVVGblFpeEhRVUZvUWl4RFFVRnZRaXhaUVVGd1FpeERRVUV6UkN4RlFVRTRSanRCUVVNM1JpeDVRa0ZCYjBJc1NVRkJTU3hKUVVGS0xFTkJRVk1zVFVGQlZDeERRVUZuUWl4SFFVRm9RaXhEUVVGdlFpeFpRVUZ3UWl4RFFVRndRanRCUVVOQk96dEJRVVZFTEUxQlFVMHNVMEZCVXp0QlFVTmtMRmxCUVZNc1NVRkJTU3hKUVVGS0xFTkJRVk1zVFVGQlZDeERRVUZuUWl4SFFVRm9RaXhEUVVGdlFpeFJRVUZ3UWl4RFFVUkxPMEZCUldRc1dVRkJVeXhUUVVaTE8wRkJSMlFzV1VGQlV5eHBRa0ZCVlN4TFFVRldMRVZCUVdsQ08wRkJRM3BDTEZGQlFVa3NTVUZCU2l4RFFVRlRMRXRCUVZRc1EwRkJaU3hMUVVGbUxFTkJRWEZDTEd0Q1FVRnlRaXhGUVVGNVF5eExRVUY2UXp0QlFVTkJPMEZCVEdFc1IwRkJaanM3UVVGUlFTeFRRVUZQTEU5QlFWQXNRMEZCWlN4TlFVRm1MRU5CUVhOQ0xFMUJRWFJDTzBGQlEwRXNSVUZvUWtRN08wRkJhMEpCT3pzN096czdPenRCUVZGQkxGTkJRVkVzVDBGQlVpeEhRVUZyUWl4VlFVRlRMRmxCUVZRc1JVRkJkVUlzVVVGQmRrSXNSVUZCYVVNN1FVRkJRVHRCUVVGQk8wRkJRVUU3TzBGQlFVRTdRVUZEYkVRc2QwSkJRWFZDTEZsQlFYWkNMRGhJUVVGeFF6dEJRVUZCTEZGQlFUVkNMRlZCUVRSQ096dEJRVU53UXl4UlFVRkpMRmRCUVZjc1VVRkJXQ3hEUVVGdlFpeE5RVUZ3UWl4RFFVRktMRVZCUVdsRE8wRkJRMmhETEdOQlFWTXNWVUZCVkR0QlFVTkJMRk5CUVUwc1VVRkJVU3hoUVVGaExFOUJRV0lzUTBGQmNVSXNWVUZCY2tJc1EwRkJaRHRCUVVOQkxHdENRVUZoTEUxQlFXSXNRMEZCYjBJc1MwRkJjRUlzUlVGQk1rSXNRMEZCTTBJN1FVRkRRVHRCUVVORU8wRkJVR2xFTzBGQlFVRTdRVUZCUVR0QlFVRkJPMEZCUVVFN1FVRkJRVHRCUVVGQk8wRkJRVUU3UVVGQlFUdEJRVUZCTzBGQlFVRTdRVUZCUVR0QlFVRkJPMEZCUVVFN08wRkJVMnhFTEUxQlFVa3NZVUZCWVN4TlFVRmlMRXRCUVhkQ0xFTkJRVFZDTEVWQlFTdENPMEZCUXpsQ08wRkJRMEVzUjBGR1JDeE5RVVZQTzBGQlEwNHNWVUZCVHl4UFFVRlFMRU5CUVdVc1dVRkJaaXhGUVVFMlFpeFJRVUUzUWp0QlFVTkJPMEZCUTBRc1JVRmtSRHM3UVVGblFrRTdPenM3T3pzN096dEJRVk5CTEZOQlFWRXNTVUZCVWl4SFFVRmxMRlZCUVZVc1VVRkJWaXhGUVVGdlFpeEpRVUZ3UWl4RlFVRXdRaXhWUVVFeFFpeEZRVUZ6UXp0QlFVTndSQ3hOUVVGTkxGZEJRVmNzUlVGQlJTeFJRVUZHTEVWQlFXcENPenRCUVVWQkxFMUJRVWs3UVVGRFNDeFBRVUZKTEZOQlFWTXNSVUZCWWl4RlFVRnBRanRCUVVOb1FpeGhRVUZUTEUxQlFWUXNRMEZCWjBJc1NVRkJTU3hMUVVGS0xFTkJRVlVzT0VKQlFWWXNRMEZCYUVJN1FVRkRRVHM3UVVGRlJDeFBRVUZOTEdsQ1FVRnBRaXhMUVVGTExFOUJRVXdzUTBGQllTeFhRVUZpTEVWQlFUQkNMRWxCUVRGQ0xFTkJRWFpDTEVOQlRFY3NRMEZMY1VRN08wRkJSWGhFTzBGQlEwRXNUMEZCVFN4VFFVRlRMRmRCUVZjc1MwRkJXQ3hEUVVGcFFpeFBRVUZxUWl4RFFVRjVRaXhqUVVGNlFpeERRVUZtTzBGQlEwRXNUMEZCU1N4VlFVRlZMRTlCUVU4c1NVRkJVQ3hMUVVGblFpeFZRVUU1UWl4RlFVRXdRenRCUVVONlF5eGhRVUZUTEU5QlFWUXNRMEZCYVVJc1NVRkJTU3hKUVVGSkxGbEJRVW9zUTBGQmFVSXNUVUZCY2tJc1EwRkJORUlzVVVGQk5VSXNSVUZCYzBNc1kwRkJkRU1zUlVGQmMwUXNWVUZCZEVRc1EwRkJha0k3UVVGRFFTeFhRVUZQTEVsQlFWQXNRMEZHZVVNc1EwRkZOVUk3UVVGRFlqczdRVUZGUkR0QlFVTkJMRTlCUVUwc1owSkJRV2RDTEVsQlFVa3NTVUZCU2l4RFFVRlRMRTFCUVZRc1EwRkJaMElzUjBGQmFFSXNRMEZCYjBJc1QwRkJjRUlzVFVGQmFVTXNUMEZCYWtNc1IwRkJNa01zVTBGQk0wTXNSMEZCZFVRc1MwRkJOMFU3UVVGRFFTeFBRVUZOTEUxQlFVMHNWMEZCVnl4VFFVRllMRU5CUVhGQ0xFMUJRWEpDTEVkQlFUaENMRWRCUVRsQ0xFZEJRVzlETEZkQlFWY3NTVUZCTDBNc1IwRkJjMFFzUjBGQmRFUXNSMEZCTkVRc1NVRkJOVVFzUjBGQmJVVXNZVUZCTDBVN08wRkJSVUVzVlVGQlR5eFBRVUZRTEVOQlFXVXNRMEZCUXl4SFFVRkVMRU5CUVdZc1JVRkJjMElzV1VGQlRUdEJRVU16UWl4UlFVRkpMRmRCUVZjc1MwRkJXQ3hEUVVGcFFpeFBRVUZxUWl4RFFVRjVRaXhqUVVGNlFpeE5RVUUyUXl4VFFVRnFSQ3hGUVVFMFJEdEJRVU16UkN4WFFVRk5MRWxCUVVrc1MwRkJTaXhEUVVGVkxHRkJRV0VzU1VGQllpeEhRVUZ2UWl4NVJFRkJjRUlzUjBGRFJTd3dRa0ZFV2l4RFFVRk9PMEZCUlVFN08wRkJSVVE3UVVGRFFTeFJRVUZOTEdWQlFXVXNWMEZCVnl4TFFVRllMRU5CUVdsQ0xFOUJRV3BDTEVOQlFYbENMR05CUVhwQ0xFVkJRWGxETEZsQlFYcERMRU5CUVhORUxFdEJRWFJFTEVWQlFYSkNPenRCUVVWQkxGRkJRVWtzWVVGQllTeE5RVUZpTEV0QlFYZENMRU5CUVRWQ0xFVkJRU3RDTzBGQlFVVTdRVUZEYUVNc1kwRkJVeXhQUVVGVUxFTkJRV2xDTEVsQlFVa3NTVUZCU1N4WlFVRktMRU5CUVdsQ0xFMUJRWEpDTEVOQlFUUkNMRkZCUVRWQ0xFVkJRWE5ETEdOQlFYUkRMRVZCUVhORUxGVkJRWFJFTEVOQlFXcENPMEZCUTBFc1dVRkJUeXhKUVVGUUxFTkJSamhDTEVOQlJXcENPMEZCUTJJN08wRkJSVVE3UVVGRFFTeFRRVUZMTEVsQlFVa3NTMEZCVkN4SlFVRnJRaXhaUVVGc1FpeEZRVUZuUXp0QlFVTXZRaXhUUVVGTkxHRkJRV0VzWVVGQllTeExRVUZpTEVOQlFXNUNPenRCUVVWQkxGTkJRVWtzVjBGQlZ5eFBRVUZZTEVOQlFXMUNMRTFCUVc1Q0xFMUJRU3RDTEVOQlFVTXNRMEZCY0VNc1JVRkJkVU03UVVGRGRFTXNaVUZCVXl4VlFVRlVPMEZCUTBFc2JVSkJRV0VzVFVGQllpeERRVUZ2UWl4TFFVRndRaXhGUVVFMFFpeERRVUUxUWp0QlFVTkJPMEZCUTBFN08wRkJSVVE3UVVGRFFTeFRRVUZKTEZkQlFWY3NUMEZCV0N4RFFVRnRRaXhOUVVGdVFpeE5RVUVyUWl4RFFVRkRMRU5CUVhCRExFVkJRWFZETzBGQlEzUkRMRzFDUVVGaExFdEJRV0lzU1VGQmMwSXNTVUZCU1N4SlFVRktMRU5CUVZNc1RVRkJWQ3hEUVVGblFpeEhRVUZvUWl4RFFVRnZRaXhYUVVGd1FpeEpRVUZ0UXl4UlFVRnVReXhIUVVFNFF5eFZRVUU1UXl4SFFVRXlSQ3hoUVVGcVJqdEJRVU5CTEUxQlJrUXNUVUZGVHl4SlFVRkpMRmRCUVZjc1RVRkJXQ3hEUVVGclFpeERRVUZETEVOQlFXNUNMRTFCUVRCQ0xFdEJRVGxDTEVWQlFYRkRPMEZCUVVVN1FVRkROME1zYlVKQlFXRXNTMEZCWWl4TFFVRjFRaXhoUVVGMlFqdEJRVU5CTzBGQlEwUTdPMEZCUlVRc1YwRkJUeXhQUVVGUUxFTkJRV1VzV1VGQlppeEZRVUUyUWl4WlFVRk5PMEZCUTJ4RExHTkJRVk1zVDBGQlZDeERRVUZwUWl4SlFVRkpMRWxCUVVrc1dVRkJTaXhEUVVGcFFpeE5RVUZ5UWl4RFFVRTBRaXhSUVVFMVFpeEZRVUZ6UXl4alFVRjBReXhGUVVGelJDeFZRVUYwUkN4RFFVRnFRanRCUVVOQkxFdEJSa1E3UVVGSFFTeEpRVzVEUkR0QlFXOURRU3hIUVhSRVJDeERRWE5FUlN4UFFVRlBMRk5CUVZBc1JVRkJhMEk3UVVGRGJrSXNXVUZCVXl4TlFVRlVMRU5CUVdkQ0xGTkJRV2hDTzBGQlEwRTdPMEZCUlVRc1UwRkJUeXhUUVVGVExFOUJRVlFzUlVGQlVEdEJRVU5CTEVWQk9VUkVPMEZCWjBWQkxFTkJkRXBFTEVWQmMwcEhMRWxCUVVrc1NVRkJTaXhEUVVGVExHRkJkRXBhT3pzN096dEJRM1JDUVRzN096czdPenM3T3p0QlFWVkJPenM3T3pzN08wRkJUMEVzUTBGQlF5eFpRVUZaT3p0QlFVVmFPenRCUVVWQk8wRkJRMEU3TzBGQlEwRXNTMEZCU1N4RFFVRkRMRTlCUVU4c1VVRkJVQ3hEUVVGblFpeE5RVUZ5UWl4RlFVRTJRanRCUVVNMVFpeFRRVUZQTEZGQlFWQXNRMEZCWjBJc1RVRkJhRUlzUjBGQmVVSXNUMEZCVHl4UlFVRlFMRU5CUVdkQ0xGRkJRV2hDTEVkQlFUSkNMRWxCUVROQ0xFZEJRMEVzVDBGQlR5eFJRVUZRTEVOQlFXZENMRkZCUkdoQ0xFbEJRelJDTEU5QlFVOHNVVUZCVUN4RFFVRm5RaXhKUVVGb1FpeEhRVUYxUWl4TlFVRk5MRTlCUVU4c1VVRkJVQ3hEUVVGblFpeEpRVUUzUXl4SFFVRnZSQ3hGUVVSb1JpeERRVUY2UWp0QlFVVkJPenRCUVVWRU8wRkJRMEU3UVVGRFFTeExRVUZKTEVOQlFVTXNTMEZCU3l4SFFVRldMRVZCUVdVN1FVRkRaQ3hQUVVGTExFZEJRVXdzUjBGQlZ5eFRRVUZUTEVkQlFWUXNSMEZCWlR0QlFVTjZRaXhWUVVGUExFbEJRVWtzU1VGQlNpeEhRVUZYTEU5QlFWZ3NSVUZCVUR0QlFVTkJMRWRCUmtRN1FVRkhRVHRCUVVWRUxFTkJia0pFT3pzN096dEJRMnBDUVRzN096czdPenM3T3p0QlFWVkJMRWxCUVVrc1NVRkJTaXhEUVVGVExGRkJRVlFzUjBGQmIwSXNTVUZCU1N4SlFVRktMRU5CUVZNc1VVRkJWQ3hKUVVGeFFpeEZRVUY2UXpzN1FVRkZRVHM3T3pzN096dEJRVTlCTEVOQlFVTXNWVUZCVlN4UFFVRldMRVZCUVcxQ096dEJRVVZ1UWpzN1FVRkZRVHM3T3pzN08wRkJTMEVzUzBGQlRTeFhRVUZYTEVWQlFXcENPenRCUVVWQk96czdPenM3UVVGTlFTeFRRVUZSTEVkQlFWSXNSMEZCWXl4VlFVRlZMRWxCUVZZc1JVRkJaMElzUzBGQmFFSXNSVUZCZFVJN1FVRkRjRU03UVVGRFFUdEJRVU5CTEUxQlFVa3NVMEZCVXl4SlFVRlVMRTFCUVcxQ0xGTkJRWFpDTEVWQlFXdERPMEZCUTJwRExFOUJRVWtzU1VGQlNpeERRVUZUTEV0QlFWUXNRMEZCWlN4SlFVRm1MRU5CUVc5Q0xIVkRRVUYxUXl4SlFVRjJReXhIUVVFNFF5eDNRa0ZCYkVVN1FVRkRRVHM3UVVGRlJDeFhRVUZUTEVsQlFWUXNTVUZCYVVJc1MwRkJha0k3UVVGRFFTeEZRVkpFT3p0QlFWVkJPenM3T3pzN08wRkJUMEVzVTBGQlVTeEhRVUZTTEVkQlFXTXNWVUZCVlN4SlFVRldMRVZCUVdkQ08wRkJRemRDTEZOQlFVOHNVMEZCVXl4SlFVRlVMRU5CUVZBN1FVRkRRU3hGUVVaRU96dEJRVWxCT3pzN096dEJRVXRCTEZOQlFWRXNTMEZCVWl4SFFVRm5RaXhaUVVGWk8wRkJRek5DTEUxQlFVa3NTVUZCU1N4SlFVRktMRU5CUVZNc1RVRkJWQ3hEUVVGblFpeEhRVUZvUWl4RFFVRnZRaXhoUVVGd1FpeE5RVUYxUXl4aFFVRXpReXhGUVVFd1JEdEJRVU42UkN4UFFVRkpMRWxCUVVvc1EwRkJVeXhMUVVGVUxFTkJRV1VzUjBGQlppeERRVUZ0UWl4clFrRkJia0lzUlVGQmRVTXNVVUZCZGtNN1FVRkRRU3hIUVVaRUxFMUJSVTg3UVVGRFRpeFRRVUZOTEVsQlFVa3NTMEZCU2l4RFFVRlZMREpFUVVGV0xFTkJRVTQ3UVVGRFFUdEJRVU5FTEVWQlRrUTdRVUZSUVN4RFFXNUVSQ3hGUVcxRVJ5eEpRVUZKTEVsQlFVb3NRMEZCVXl4UlFXNUVXanM3T3pzN096czdRVU51UWtFN096czdPenM3T3pzN1FVRlZRVHM3T3pzN096czdPenM3UVVGWFFTeERRVUZETEZsQlFWYzdPMEZCUlZnN1FVRkRRVHRCUVVOQk96dEJRVVZCTEZGQlFVOHNVMEZCVUN4SFFVRnRRaXhUUVVGdVFqdEJRVU5CTEZGQlFVOHNUMEZCVUN4SFFVRnBRaXhUUVVGcVFqczdRVUZGUVN4TFFVRkpMRmxCUVVvN1FVRkRRU3hMUVVGSkxGVkJRVW83UVVGRFFTeExRVUZKTEdGQlFVbzdRVUZEUVN4TFFVRkpMRzlDUVVGS08wRkJRMEVzUzBGQlNTeHBRa0ZCU2p0QlFVTkJMRXRCUVVrc1dVRkJTanRCUVVOQkxFdEJRVWtzTUVKQlFVbzdRVUZEUVN4TFFVRkpMRzFDUVVGS08wRkJRMEVzUzBGQlNTeG5Ra0ZCU2p0QlFVTkJMRXRCUVVrc1ZVRkJWU3hSUVVGa08wRkJRMEVzUzBGQlNTeHBRa0ZCYVVJc1QwRkJja0k3UVVGRFFTeExRVUZKTEdkQ1FVRm5RaXhQUVVGd1FqdEJRVU5CTEV0QlFVa3NTMEZCU3l4UFFVRlBMRk5CUVdoQ08wRkJRMEVzUzBGQlNTeFZRVUZWTEVkQlFVY3NVVUZCYWtJN1FVRkRRU3hMUVVGSkxGTkJRVk1zUjBGQlJ5eGpRVUZvUWp0QlFVTkJMRXRCUVVrc1dVRkJXU3hEUVVGRExFVkJRVVVzVDBGQlR5eE5RVUZRTEV0QlFXdENMRmRCUVd4Q0xFbEJRV2xETEU5QlFVOHNVMEZCVUN4TFFVRnhRaXhYUVVGMFJDeEpRVUZ4UlN4UFFVRlBMRkZCUVRsRkxFTkJRV3BDTzBGQlEwRXNTMEZCU1N4alFVRmpMRU5CUVVNc1UwRkJSQ3hKUVVGakxFOUJRVThzWVVGQlVDeExRVUY1UWl4WFFVRjZSRHRCUVVOQk8wRkJRMEU3UVVGRFFTeExRVUZKTEdOQlFXTXNZVUZCWVN4VlFVRlZMRkZCUVZZc1MwRkJkVUlzWlVGQmNFTXNSMEZCYzBRc1dVRkJkRVFzUjBGQmNVVXNjVUpCUVhaR08wRkJRMEVzUzBGQlNTeHBRa0ZCYVVJc1IwRkJja0k3UVVGRFFUdEJRVU5CTEV0QlFVa3NWVUZCVlN4UFFVRlBMRXRCUVZBc1MwRkJhVUlzVjBGQmFrSXNTVUZCWjBNc1RVRkJUU3hSUVVGT0xFOUJRWEZDTEdkQ1FVRnVSVHRCUVVOQkxFdEJRVWtzVjBGQlZ5eEZRVUZtTzBGQlEwRXNTMEZCU1N4TlFVRk5MRVZCUVZZN1FVRkRRU3hMUVVGSkxHbENRVUZwUWl4RlFVRnlRanRCUVVOQkxFdEJRVWtzYVVKQlFXbENMRXRCUVhKQ096dEJRVVZCTzBGQlEwRTdRVUZEUVRzN1FVRkZRVHM3T3pzN096dEJRVTlCTEZWQlFWTXNWVUZCVkN4RFFVRnZRaXhGUVVGd1FpeEZRVUYzUWp0QlFVTjJRaXhUUVVGUExGRkJRVkVzU1VGQlVpeERRVUZoTEVWQlFXSXNUVUZCY1VJc2JVSkJRVFZDTzBGQlEwRTdPMEZCUlVRN096czdPenM3UVVGUFFTeFZRVUZUTEU5QlFWUXNRMEZCYVVJc1JVRkJha0lzUlVGQmNVSTdRVUZEY0VJc1UwRkJUeXhSUVVGUkxFbEJRVklzUTBGQllTeEZRVUZpTEUxQlFYRkNMR2RDUVVFMVFqdEJRVU5CT3p0QlFVVkVPenM3T3p0QlFVdEJMRlZCUVZNc1NVRkJWQ3hEUVVGakxFZEJRV1FzUlVGQmJVSXNTVUZCYmtJc1JVRkJlVUk3UVVGRGVFSXNUVUZCU1N4SFFVRktMRVZCUVZNN1FVRkRVaXhSUVVGTExFbEJRVWtzU1VGQlNTeERRVUZpTEVWQlFXZENMRWxCUVVrc1NVRkJTU3hOUVVGNFFpeEZRVUZuUXl4TFFVRkxMRU5CUVhKRExFVkJRWGRETzBGQlEzWkRMRkZCUVVrc1NVRkJTU3hEUVVGS0xFdEJRVlVzUzBGQlN5eEpRVUZKTEVOQlFVb3NRMEZCVEN4RlFVRmhMRU5CUVdJc1JVRkJaMElzUjBGQmFFSXNRMEZCWkN4RlFVRnZRenRCUVVOdVF6dEJRVU5CTzBGQlEwUTdRVUZEUkR0QlFVTkVPenRCUVVWRU96czdPenRCUVV0QkxGVkJRVk1zVjBGQlZDeERRVUZ4UWl4SFFVRnlRaXhGUVVFd1FpeEpRVUV4UWl4RlFVRm5RenRCUVVNdlFpeE5RVUZKTEVkQlFVb3NSVUZCVXp0QlFVTlNMRTlCUVVrc1ZVRkJTanRCUVVOQkxGRkJRVXNzU1VGQlNTeEpRVUZKTEUxQlFVb3NSMEZCWVN4RFFVRjBRaXhGUVVGNVFpeEpRVUZKTEVOQlFVTXNRMEZCT1VJc1JVRkJhVU1zUzBGQlN5eERRVUYwUXl4RlFVRjVRenRCUVVONFF5eFJRVUZKTEVsQlFVa3NRMEZCU2l4TFFVRlZMRXRCUVVzc1NVRkJTU3hEUVVGS0xFTkJRVXdzUlVGQllTeERRVUZpTEVWQlFXZENMRWRCUVdoQ0xFTkJRV1FzUlVGQmIwTTdRVUZEYmtNN1FVRkRRVHRCUVVORU8wRkJRMFE3UVVGRFJEczdRVUZGUkRzN096czdPenM3UVVGUlFTeFZRVUZUTEU5QlFWUXNRMEZCYVVJc1IwRkJha0lzUlVGQmMwSXNTVUZCZEVJc1JVRkJORUk3UVVGRE0wSXNVMEZCVHl4UFFVRlBMRWxCUVZBc1EwRkJXU3hIUVVGYUxFVkJRV2xDTEVsQlFXcENMRU5CUVZBN1FVRkRRVHM3UVVGRlJEczdPenM3T3pzN1FVRlJRU3hWUVVGVExFMUJRVlFzUTBGQlowSXNSMEZCYUVJc1JVRkJjVUlzU1VGQmNrSXNSVUZCTWtJN1FVRkRNVUlzVTBGQlR5eFJRVUZSTEVkQlFWSXNSVUZCWVN4SlFVRmlMRXRCUVhOQ0xFbEJRVWtzU1VGQlNpeERRVUUzUWp0QlFVTkJPenRCUVVWRU96czdPenRCUVV0QkxGVkJRVk1zVVVGQlZDeERRVUZyUWl4SFFVRnNRaXhGUVVGMVFpeEpRVUYyUWl4RlFVRTJRanRCUVVNMVFpeE5RVUZKTEdGQlFVbzdRVUZEUVN4UFFVRkxMRWxCUVV3c1NVRkJZU3hIUVVGaUxFVkJRV3RDTzBGQlEycENMRTlCUVVrc1VVRkJVU3hIUVVGU0xFVkJRV0VzU1VGQllpeERRVUZLTEVWQlFYZENPMEZCUTNaQ0xGRkJRVWtzUzBGQlN5eEpRVUZKTEVsQlFVb3NRMEZCVEN4RlFVRm5RaXhKUVVGb1FpeERRVUZLTEVWQlFUSkNPMEZCUXpGQ08wRkJRMEU3UVVGRFJEdEJRVU5FTzBGQlEwUTdPMEZCUlVRN096czdRVUZKUVN4VlFVRlRMRXRCUVZRc1EwRkJaU3hOUVVGbUxFVkJRWFZDTEUxQlFYWkNMRVZCUVN0Q0xFdEJRUzlDTEVWQlFYTkRMR1ZCUVhSRExFVkJRWFZFTzBGQlEzUkVMRTFCUVVrc1RVRkJTaXhGUVVGWk8wRkJRMWdzV1VGQlV5eE5RVUZVTEVWQlFXbENMRlZCUVZNc1MwRkJWQ3hGUVVGblFpeEpRVUZvUWl4RlFVRnpRanRCUVVOMFF5eFJRVUZKTEZOQlFWTXNRMEZCUXl4UlFVRlJMRTFCUVZJc1JVRkJaMElzU1VGQmFFSXNRMEZCWkN4RlFVRnhRenRCUVVOd1F5eFRRVUZKTEcxQ1FVRnRRaXhSUVVGUExFdEJRVkFzZVVOQlFVOHNTMEZCVUN4UFFVRnBRaXhSUVVGd1F5eEpRVUZuUkN4TFFVRm9SQ3hKUVVOSUxFTkJRVU1zVVVGQlVTeExRVUZTTEVOQlJFVXNTVUZEWjBJc1EwRkJReXhYUVVGWExFdEJRVmdzUTBGRWFrSXNTVUZGU0N4RlFVRkZMR2xDUVVGcFFpeE5RVUZ1UWl4RFFVWkVMRVZCUlRaQ096dEJRVVUxUWl4VlFVRkpMRU5CUVVNc1QwRkJUeXhKUVVGUUxFTkJRVXdzUlVGQmJVSTdRVUZEYkVJc1kwRkJUeXhKUVVGUUxFbEJRV1VzUlVGQlpqdEJRVU5CTzBGQlEwUXNXVUZCVFN4UFFVRlBMRWxCUVZBc1EwRkJUaXhGUVVGdlFpeExRVUZ3UWl4RlFVRXlRaXhMUVVFelFpeEZRVUZyUXl4bFFVRnNRenRCUVVOQkxFMUJVa1FzVFVGUlR6dEJRVU5PTEdGQlFVOHNTVUZCVUN4SlFVRmxMRXRCUVdZN1FVRkRRVHRCUVVORU8wRkJRMFFzU1VGa1JEdEJRV1ZCTzBGQlEwUXNVMEZCVHl4TlFVRlFPMEZCUTBFN08wRkJSVVE3UVVGRFFUdEJRVU5CTEZWQlFWTXNTVUZCVkN4RFFVRmpMRWRCUVdRc1JVRkJiVUlzUlVGQmJrSXNSVUZCZFVJN1FVRkRkRUlzVTBGQlR5eFpRVUZYTzBGQlEycENMRlZCUVU4c1IwRkJSeXhMUVVGSUxFTkJRVk1zUjBGQlZDeEZRVUZqTEZOQlFXUXNRMEZCVUR0QlFVTkJMRWRCUmtRN1FVRkhRVHM3UVVGRlJDeFZRVUZUTEU5QlFWUXNSMEZCYlVJN1FVRkRiRUlzVTBGQlR5eFRRVUZUTEc5Q1FVRlVMRU5CUVRoQ0xGRkJRVGxDTEVOQlFWQTdRVUZEUVRzN1FVRkZSQ3hWUVVGVExHTkJRVlFzUTBGQmQwSXNSMEZCZUVJc1JVRkJOa0k3UVVGRE5VSXNVVUZCVFN4SFFVRk9PMEZCUTBFN08wRkJSVVE3UVVGRFFTeFZRVUZUTEZOQlFWUXNRMEZCYlVJc1MwRkJia0lzUlVGQk1FSTdRVUZEZWtJc1RVRkJTU3hEUVVGRExFdEJRVXdzUlVGQldUdEJRVU5ZTEZWQlFVOHNTMEZCVUR0QlFVTkJPMEZCUTBRc1RVRkJTU3hKUVVGSkxFMUJRVkk3UVVGRFFTeFBRVUZMTEUxQlFVMHNTMEZCVGl4RFFVRlpMRWRCUVZvc1EwRkJUQ3hGUVVGMVFpeFZRVUZUTEVsQlFWUXNSVUZCWlR0QlFVTnlReXhQUVVGSkxFVkJRVVVzU1VGQlJpeERRVUZLTzBGQlEwRXNSMEZHUkR0QlFVZEJMRk5CUVU4c1EwRkJVRHRCUVVOQk96dEJRVVZFT3pzN096czdPenM3UVVGVFFTeFZRVUZUTEZOQlFWUXNRMEZCYlVJc1JVRkJia0lzUlVGQmRVSXNSMEZCZGtJc1JVRkJORUlzUjBGQk5VSXNSVUZCYVVNc1kwRkJha01zUlVGQmFVUTdRVUZEYUVRc1RVRkJUU3hSUVVGUkxFbEJRVWtzUzBGQlNpeERRVUZWTEUxQlFVMHNNRU5CUVU0c1IwRkJiVVFzUlVGQk4wUXNRMEZCWkRzN1FVRkZRU3hSUVVGTkxGZEJRVTRzUjBGQmIwSXNSVUZCY0VJN1FVRkRRU3hSUVVGTkxHTkJRVTRzUjBGQmRVSXNZMEZCZGtJN08wRkJSVUVzVFVGQlNTeEhRVUZLTEVWQlFWTTdRVUZEVWl4VFFVRk5MR0ZCUVU0c1IwRkJjMElzUjBGQmRFSTdRVUZEUVRzN1FVRkZSQ3hUUVVGUExFdEJRVkE3UVVGRFFUczdRVUZGUkN4TFFVRkpMRTlCUVU4c1QwRkJUeXhUUVVGa0xFdEJRVFJDTEZkQlFXaERMRVZCUVRaRE8wRkJRelZETEUxQlFVa3NWMEZCVnl4UFFVRlBMRk5CUVd4Q0xFTkJRVW9zUlVGQmEwTTdRVUZEYWtNN1FVRkRRVHRCUVVOQk8wRkJRMFFzVVVGQlRTeFBRVUZQTEZOQlFXSTdRVUZEUVN4VFFVRlBMRk5CUVZBc1IwRkJiVUlzVTBGQmJrSTdRVUZEUVRzN1FVRkZSRHRCUVVOQkxFdEJRVWtzVDBGQlR5eFBRVUZQTEU5QlFXUXNTMEZCTUVJc1YwRkJNVUlzU1VGQmVVTXNRMEZCUXl4WFFVRlhMRTlCUVU4c1QwRkJiRUlzUTBGQk9VTXNSVUZCTUVVN1FVRkRla1U3UVVGRFFTeFJRVUZOTEU5QlFVOHNUMEZCWWp0QlFVTkJMRk5CUVU4c1QwRkJVQ3hIUVVGcFFpeFRRVUZxUWp0QlFVTkJPenRCUVVWRUxGVkJRVk1zVlVGQlZDeERRVUZ2UWl4WFFVRndRaXhGUVVGcFF6dEJRVU5vUXl4TlFVRkpMSE5DUVVGS08wRkJRVUVzVFVGQmJVSXNaVUZCYmtJN1FVRkJRU3hOUVVFeVFpeG5Ra0ZCTTBJN1FVRkJRU3hOUVVGdlF5eHBRa0ZCY0VNN1FVRkJRU3hOUVVORExEWkNRVVJFTzBGQlFVRXNUVUZGUXl4VlFVRlRPMEZCUTFJN1FVRkRRVHRCUVVOQk8wRkJRMEVzWjBKQlFXRXNRMEZLVER0QlFVdFNMRmxCUVZNc1NVRk1SRHRCUVUxU0xGVkJRVThzUlVGT1F6dEJRVTlTTEZsQlFWTXNSVUZRUkR0QlFWRlNMRk5CUVUwc1JVRlNSVHRCUVZOU0xGTkJRVTBzUlVGVVJUdEJRVlZTTEZkQlFWRTdRVUZXUVN4SFFVWldPMEZCUVVFc1RVRmpReXhYUVVGWExFVkJaRm83TzBGQlpVTTdRVUZEUVR0QlFVTkJPMEZCUTBFc2IwSkJRV3RDTEVWQmJFSnVRanRCUVVGQkxFMUJiVUpETEdOQlFXTXNSVUZ1UW1ZN1FVRkJRU3hOUVc5Q1F5eFhRVUZYTEVWQmNFSmFPMEZCUVVFc1RVRnhRa01zVjBGQlZTeEZRWEpDV0R0QlFVRkJMRTFCYzBKRExHRkJRV0VzUlVGMFFtUTdRVUZCUVN4TlFYVkNReXhoUVVGaExFVkJka0prTzBGQlFVRXNUVUYzUWtNc2FVSkJRV2xDTEVOQmVFSnNRanRCUVVGQkxFMUJlVUpETEhOQ1FVRnpRaXhEUVhwQ2RrSTdPMEZCTWtKQk96czdPenM3T3pzN096dEJRVmRCTEZkQlFWTXNVVUZCVkN4RFFVRnJRaXhIUVVGc1FpeEZRVUYxUWp0QlFVTjBRaXhQUVVGSkxGVkJRVW83UVVGQlFTeFBRVUZQTEdGQlFWQTdRVUZEUVN4UlFVRkxMRWxCUVVrc1EwRkJWQ3hGUVVGWkxFbEJRVWtzU1VGQlNTeE5RVUZ3UWl4RlFVRTBRaXhIUVVFMVFpeEZRVUZwUXp0QlFVTm9ReXhYUVVGUExFbEJRVWtzUTBGQlNpeERRVUZRTzBGQlEwRXNVVUZCU1N4VFFVRlRMRWRCUVdJc1JVRkJhMEk3UVVGRGFrSXNVMEZCU1N4TlFVRktMRU5CUVZjc1EwRkJXQ3hGUVVGakxFTkJRV1E3UVVGRFFTeFZRVUZMTEVOQlFVdzdRVUZEUVN4TFFVaEVMRTFCUjA4c1NVRkJTU3hUUVVGVExFbEJRV0lzUlVGQmJVSTdRVUZEZWtJN1FVRkRRVHRCUVVOQk8wRkJRMEVzVTBGQlNTeE5RVUZOTEVOQlFVNHNTVUZCV1N4TlFVRk5MRU5CUVU0c1NVRkJWeXhKUVVGSkxFTkJRVW9zVFVGQlZ5eEpRVUZzUXl4SlFVRXlReXhKUVVGSkxFbEJRVWtzUTBGQlVpeE5RVUZsTEVsQlFUbEVMRVZCUVc5Rk8wRkJRMjVGTzBGQlEwRXNUVUZHUkN4TlFVVlBMRWxCUVVrc1NVRkJTU3hEUVVGU0xFVkJRVmM3UVVGRGFrSXNWVUZCU1N4TlFVRktMRU5CUVZjc1NVRkJTU3hEUVVGbUxFVkJRV3RDTEVOQlFXeENPMEZCUTBFc1YwRkJTeXhEUVVGTU8wRkJRMEU3UVVGRFJEdEJRVU5FTzBGQlEwUTdPMEZCUlVRN096czdPenM3T3pzN1FVRlZRU3hYUVVGVExGTkJRVlFzUTBGQmJVSXNTVUZCYmtJc1JVRkJlVUlzVVVGQmVrSXNSVUZCYlVNc1VVRkJia01zUlVGQk5rTTdRVUZETlVNc1QwRkJTU3huUWtGQlNqdEJRVUZCTEU5QlFXRXNhVUpCUVdJN1FVRkJRU3hQUVVGMVFpeHJRa0ZCZGtJN1FVRkJRU3hQUVVGclF5eFZRVUZzUXp0QlFVRkJMRTlCUVhGRExGVkJRWEpETzBGQlFVRXNUMEZCZDBNc2IwSkJRWGhETzBGQlFVRXNUMEZCY1VRc2EwSkJRWEpFTzBGQlFVRXNUMEZEUXl4cFFrRkVSRHRCUVVGQkxFOUJRMWNzWlVGRVdEdEJRVUZCTEU5QlEyMUNMSEZDUVVSdVFqdEJRVUZCTEU5QlEybERMR05CUkdwRE8wRkJRVUVzVDBGRGQwTXNORUpCUkhoRE8wRkJRVUVzVDBGRlF5eFpRVUZoTEZsQlFWa3NVMEZCVXl4TFFVRlVMRU5CUVdVc1IwRkJaaXhEUVVZeFFqdEJRVUZCTEU5QlIwTXNUVUZCVFN4UlFVRlBMRWRCU0dRN1FVRkJRU3hQUVVsRExGVkJRVlVzVDBGQlR5eEpRVUZKTEVkQlFVb3NRMEZLYkVJN08wRkJUVUU3UVVGRFFTeFBRVUZKTEVsQlFVb3NSVUZCVlR0QlFVTlVMRmRCUVU4c1MwRkJTeXhMUVVGTUxFTkJRVmNzUjBGQldDeERRVUZRTzBGQlEwRXNaMEpCUVZrc1MwRkJTeXhOUVVGTUxFZEJRV01zUTBGQk1VSTdPMEZCUlVFN1FVRkRRVHRCUVVOQkxGRkJRVWtzVVVGQlR5eFpRVUZRTEVsQlFYVkNMR1ZCUVdVc1NVRkJaaXhEUVVGdlFpeExRVUZMTEZOQlFVd3NRMEZCY0VJc1EwRkJNMElzUlVGQmFVVTdRVUZEYUVVc1ZVRkJTeXhUUVVGTUxFbEJRV3RDTEV0QlFVc3NVMEZCVEN4RlFVRm5RaXhQUVVGb1FpeERRVUYzUWl4alFVRjRRaXhGUVVGM1F5eEZRVUY0UXl4RFFVRnNRanRCUVVOQk96dEJRVVZFTzBGQlEwRXNVVUZCU1N4TFFVRkxMRU5CUVV3c1JVRkJVU3hOUVVGU0xFTkJRV1VzUTBGQlppeE5RVUZ6UWl4SFFVRjBRaXhKUVVFMlFpeFRRVUZxUXl4RlFVRTBRenRCUVVNelF6dEJRVU5CTzBGQlEwRTdRVUZEUVN3eVFrRkJjMElzVlVGQlZTeExRVUZXTEVOQlFXZENMRU5CUVdoQ0xFVkJRVzFDTEZWQlFWVXNUVUZCVml4SFFVRnRRaXhEUVVGMFF5eERRVUYwUWp0QlFVTkJMRmxCUVU4c2IwSkJRVzlDTEUxQlFYQkNMRU5CUVRKQ0xFbEJRVE5DTEVOQlFWQTdRVUZEUVRzN1FVRkZSQ3hoUVVGVExFbEJRVlE3UVVGRFFTeFhRVUZQTEV0QlFVc3NTVUZCVEN4RFFVRlZMRWRCUVZZc1EwRkJVRHRCUVVOQk96dEJRVVZFTzBGQlEwRXNUMEZCU1N4WlFVRlpMRWRCUVZvc1MwRkJiMElzWVVGQllTeFBRVUZxUXl4RFFVRktMRVZCUVN0RE8wRkJRemxETEdkQ1FVRlpMRXRCUVVzc1MwRkJUQ3hEUVVGWExFZEJRVmdzUTBGQldqczdRVUZGUVN4bFFVRlhMRXRCUVVzc1NVRkJTU3hWUVVGVkxFMUJRVzVDTEVWQlFUSkNMRWxCUVVrc1EwRkJMMElzUlVGQmEwTXNTMEZCU3l4RFFVRjJReXhGUVVFd1F6dEJRVU53UkN4dFFrRkJZeXhWUVVGVkxFdEJRVllzUTBGQlowSXNRMEZCYUVJc1JVRkJiVUlzUTBGQmJrSXNSVUZCYzBJc1NVRkJkRUlzUTBGQk1rSXNSMEZCTTBJc1EwRkJaRHM3UVVGRlFTeFRRVUZKTEZOQlFVb3NSVUZCWlR0QlFVTmtPMEZCUTBFN1FVRkRRU3hYUVVGTExFbEJRVWtzVlVGQlZTeE5RVUZ1UWl4RlFVRXlRaXhKUVVGSkxFTkJRUzlDTEVWQlFXdERMRXRCUVVzc1EwRkJka01zUlVGQk1FTTdRVUZEZWtNc2EwSkJRVmNzVDBGQlR5eEhRVUZRTEVWQlFWa3NWVUZCVlN4TFFVRldMRU5CUVdkQ0xFTkJRV2hDTEVWQlFXMUNMRU5CUVc1Q0xFVkJRWE5DTEVsQlFYUkNMRU5CUVRKQ0xFZEJRVE5DTEVOQlFWb3NRMEZCV0RzN1FVRkZRVHRCUVVOQkxGZEJRVWtzVVVGQlNpeEZRVUZqTzBGQlEySXNiVUpCUVZjc1QwRkJUeXhSUVVGUUxFVkJRV2xDTEZkQlFXcENMRU5CUVZnN1FVRkRRU3haUVVGSkxGRkJRVW9zUlVGQll6dEJRVU5pTzBGQlEwRXNiMEpCUVZjc1VVRkJXRHRCUVVOQkxHdENRVUZUTEVOQlFWUTdRVUZEUVN4bFFVRk5MRk5CUVU0N1FVRkRRVHRCUVVORU8wRkJRMFE3UVVGRFJEczdRVUZGUkR0QlFVTkJPMEZCUTBFc1UwRkJTU3hEUVVGRExGbEJRVVFzU1VGQmFVSXNUMEZCYWtJc1NVRkJORUlzVDBGQlR5eFBRVUZRTEVWQlFXZENMRmRCUVdoQ0xFTkJRV2hETEVWQlFUaEVPMEZCUXpkRUxIRkNRVUZsTEU5QlFVOHNUMEZCVUN4RlFVRm5RaXhYUVVGb1FpeERRVUZtTzBGQlEwRXNZMEZCVVN4RFFVRlNPMEZCUTBFN1FVRkRSRHM3UVVGRlJDeFJRVUZKTEVOQlFVTXNVVUZCUkN4SlFVRmhMRmxCUVdwQ0xFVkJRU3RDTzBGQlF6bENMR2RDUVVGWExGbEJRVmc3UVVGRFFTeGpRVUZUTEV0QlFWUTdRVUZEUVRzN1FVRkZSQ3hSUVVGSkxGRkJRVW9zUlVGQll6dEJRVU5pTEdWQlFWVXNUVUZCVml4RFFVRnBRaXhEUVVGcVFpeEZRVUZ2UWl4TlFVRndRaXhGUVVFMFFpeFJRVUUxUWp0QlFVTkJMRmxCUVU4c1ZVRkJWU3hKUVVGV0xFTkJRV1VzUjBGQlppeERRVUZRTzBGQlEwRTdRVUZEUkRzN1FVRkZSRHRCUVVOQkxHRkJRVlVzVDBGQlR5eFJRVUZQTEVsQlFXUXNSVUZCYjBJc1NVRkJjRUlzUTBGQlZqczdRVUZGUVN4VlFVRlBMRlZCUVZVc1QwRkJWaXhIUVVGdlFpeEpRVUV6UWp0QlFVTkJPenRCUVVWRUxGZEJRVk1zV1VGQlZDeERRVUZ6UWl4SlFVRjBRaXhGUVVFMFFqdEJRVU16UWl4UFFVRkpMRk5CUVVvc1JVRkJaVHRCUVVOa0xGTkJRVXNzVTBGQlRDeEZRVUZuUWl4VlFVRlRMRlZCUVZRc1JVRkJjVUk3UVVGRGNFTXNVMEZCU1N4WFFVRlhMRmxCUVZnc1EwRkJkMElzYjBKQlFYaENMRTFCUVd0RUxFbEJRV3hFTEVsQlEwZ3NWMEZCVnl4WlFVRllMRU5CUVhkQ0xIRkNRVUY0UWl4TlFVRnRSQ3hSUVVGUkxGZEJSRFZFTEVWQlEzbEZPMEZCUTNoRkxHbENRVUZYTEZWQlFWZ3NRMEZCYzBJc1YwRkJkRUlzUTBGQmEwTXNWVUZCYkVNN1FVRkRRU3hoUVVGUExFbEJRVkE3UVVGRFFUdEJRVU5FTEV0QlRrUTdRVUZQUVR0QlFVTkVPenRCUVVWRUxGZEJRVk1zWlVGQlZDeERRVUY1UWl4RlFVRjZRaXhGUVVFMlFqdEJRVU0xUWl4UFFVRkpMR0ZCUVdFc1QwRkJUeXhSUVVGUExFdEJRV1FzUlVGQmNVSXNSVUZCY2tJc1EwRkJha0k3UVVGRFFTeFBRVUZKTEdOQlFXTXNVVUZCVVN4VlFVRlNMRU5CUVdRc1NVRkJjVU1zVjBGQlZ5eE5RVUZZTEVkQlFXOUNMRU5CUVRkRUxFVkJRV2RGTzBGQlF5OUVPMEZCUTBFc1pVRkJWeXhMUVVGWU8wRkJRMEVzV1VGQlVTeFBRVUZTTEVOQlFXZENMRXRCUVdoQ0xFTkJRWE5DTEVWQlFYUkNPenRCUVVWQk8wRkJRMEVzV1VGQlVTeFhRVUZTTEVOQlFXOUNMRWxCUVhCQ0xFVkJRVEJDTzBGQlEzcENMR05CUVZNN1FVRkVaMElzUzBGQk1VSXNSVUZGUnl4RFFVRkRMRVZCUVVRc1EwRkdTRHM3UVVGSlFTeFhRVUZQTEVsQlFWQTdRVUZEUVR0QlFVTkVPenRCUVVWRU8wRkJRMEU3UVVGRFFTeFhRVUZUTEZkQlFWUXNRMEZCY1VJc1NVRkJja0lzUlVGQk1rSTdRVUZETVVJc1QwRkJTU3hsUVVGS08wRkJRVUVzVDBGRFF5eFJRVUZSTEU5QlFVOHNTMEZCU3l4UFFVRk1MRU5CUVdFc1IwRkJZaXhEUVVGUUxFZEJRVEpDTEVOQlFVTXNRMEZFY2tNN1FVRkZRU3hQUVVGSkxGRkJRVkVzUTBGQlF5eERRVUZpTEVWQlFXZENPMEZCUTJZc1lVRkJVeXhMUVVGTExGTkJRVXdzUTBGQlpTeERRVUZtTEVWQlFXdENMRXRCUVd4Q0xFTkJRVlE3UVVGRFFTeFhRVUZQTEV0QlFVc3NVMEZCVEN4RFFVRmxMRkZCUVZFc1EwRkJka0lzUlVGQk1FSXNTMEZCU3l4TlFVRXZRaXhEUVVGUU8wRkJRMEU3UVVGRFJDeFZRVUZQTEVOQlFVTXNUVUZCUkN4RlFVRlRMRWxCUVZRc1EwRkJVRHRCUVVOQk96dEJRVVZFT3pzN096czdPenM3T3pzN1FVRlpRU3hYUVVGVExHRkJRVlFzUTBGQmRVSXNTVUZCZGtJc1JVRkJOa0lzWlVGQk4wSXNSVUZCT0VNc1dVRkJPVU1zUlVGQk5FUXNVVUZCTlVRc1JVRkJjMFU3UVVGRGNrVXNUMEZCU1N4WlFVRktPMEZCUVVFc1QwRkJVeXh4UWtGQlZEdEJRVUZCTEU5QlFYVkNMR1ZCUVhaQ08wRkJRVUVzVDBGQkswSXNhMEpCUVM5Q08wRkJRVUVzVDBGRFF5eFRRVUZUTEVsQlJGWTdRVUZCUVN4UFFVVkRMR0ZCUVdFc2EwSkJRV3RDTEdkQ1FVRm5RaXhKUVVGc1F5eEhRVUY1UXl4SlFVWjJSRHRCUVVGQkxFOUJSME1zWlVGQlpTeEpRVWhvUWp0QlFVRkJMRTlCU1VNc1YwRkJWeXhKUVVwYU8wRkJRVUVzVDBGTFF5eHBRa0ZCYVVJc1JVRk1iRUk3TzBGQlQwRTdRVUZEUVR0QlFVTkJMRTlCUVVrc1EwRkJReXhKUVVGTUxFVkJRVmM3UVVGRFZpeGxRVUZYTEV0QlFWZzdRVUZEUVN4WFFVRlBMRk5CUVZNc2EwSkJRV3RDTEVOQlFUTkNMRU5CUVZBN1FVRkRRVHM3UVVGRlJDeGxRVUZaTEZsQlFWa3NTVUZCV2l4RFFVRmFPMEZCUTBFc1dVRkJVeXhWUVVGVkxFTkJRVllzUTBGQlZEdEJRVU5CTEZWQlFVOHNWVUZCVlN4RFFVRldMRU5CUVZBN08wRkJSVUVzVDBGQlNTeE5RVUZLTEVWQlFWazdRVUZEV0N4aFFVRlRMRlZCUVZVc1RVRkJWaXhGUVVGclFpeFZRVUZzUWl4RlFVRTRRaXhSUVVFNVFpeERRVUZVTzBGQlEwRXNiVUpCUVdVc1QwRkJUeXhSUVVGUUxFVkJRV2RDTEUxQlFXaENMRU5CUVdZN1FVRkRRVHM3UVVGRlJEdEJRVU5CTEU5QlFVa3NTVUZCU2l4RlFVRlZPMEZCUTFRc1VVRkJTU3hOUVVGS0xFVkJRVms3UVVGRFdDeFRRVUZKTEdkQ1FVRm5RaXhoUVVGaExGTkJRV3BETEVWQlFUUkRPMEZCUXpORE8wRkJRMEVzZFVKQlFXbENMR0ZCUVdFc1UwRkJZaXhEUVVGMVFpeEpRVUYyUWl4RlFVRTJRaXhWUVVGVExFbEJRVlFzUlVGQlpUdEJRVU0xUkN4alFVRlBMRlZCUVZVc1NVRkJWaXhGUVVGblFpeFZRVUZvUWl4RlFVRTBRaXhSUVVFMVFpeERRVUZRTzBGQlEwRXNUMEZHWjBJc1EwRkJha0k3UVVGSFFTeE5RVXhFTEUxQlMwODdRVUZEVGp0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTEhWQ1FVRnBRaXhMUVVGTExFOUJRVXdzUTBGQllTeEhRVUZpTEUxQlFYTkNMRU5CUVVNc1EwRkJka0lzUjBGRFFTeFZRVUZWTEVsQlFWWXNSVUZCWjBJc1ZVRkJhRUlzUlVGQk5FSXNVVUZCTlVJc1EwRkVRU3hIUVVWQkxFbEJSbXBDTzBGQlIwRTdRVUZEUkN4TFFXeENSQ3hOUVd0Q1R6dEJRVU5PTzBGQlEwRXNjMEpCUVdsQ0xGVkJRVlVzU1VGQlZpeEZRVUZuUWl4VlFVRm9RaXhGUVVFMFFpeFJRVUUxUWl4RFFVRnFRanM3UVVGRlFUdEJRVU5CTzBGQlEwRTdRVUZEUVN4cFFrRkJXU3haUVVGWkxHTkJRVm9zUTBGQldqdEJRVU5CTEdOQlFWTXNWVUZCVlN4RFFVRldMRU5CUVZRN1FVRkRRU3h6UWtGQmFVSXNWVUZCVlN4RFFVRldMRU5CUVdwQ08wRkJRMEVzYjBKQlFXVXNTVUZCWmpzN1FVRkZRU3hYUVVGTkxGRkJRVkVzVTBGQlVpeERRVUZyUWl4alFVRnNRaXhEUVVGT08wRkJRMEU3UVVGRFJEczdRVUZGUkR0QlFVTkJPMEZCUTBFN1FVRkRRU3haUVVGVExGVkJRVlVzUTBGQlF5eFpRVUZZTEVsQlFUSkNMRU5CUVVNc1dVRkJOVUlzUjBGRFFTeHRRa0ZCYlVJc2RVSkJRWFZDTEVOQlFURkRMRU5CUkVFc1IwRkZRU3hGUVVaVU96dEJRVWxCTEZWQlFVODdRVUZEVGl4WlFVRlJMRTFCUkVZN1FVRkZUaXhWUVVGTkxHTkJSa0U3UVVGSFRpeGxRVUZYTEdWQlNFdzdRVUZKVGl4clFrRkJZeXhEUVVGRExFTkJRVU1zVFVGS1ZqdEJRVXRPTEZOQlFVc3NSMEZNUXp0QlFVMU9MR3RDUVVGakxGbEJUbEk3UVVGUFRpeGpRVUZWTEZGQlVFbzdRVUZSVGl4UlFVRkpMRU5CUVVNc1UwRkRRU3hUUVVGVExFZEJRVlFzUjBGQlpTeGpRVVJtTEVkQlJVRXNZMEZHUkN4SlFVVnRRanRCUVZacVFpeEpRVUZRTzBGQldVRTdPMEZCUlVRc1YwRkJVeXhUUVVGVUxFTkJRVzFDTEUxQlFXNUNMRVZCUVRKQ08wRkJRekZDTEU5QlFVa3NTMEZCU3l4UFFVRlBMRVZCUVdoQ08wRkJRVUVzVDBGRFF5eE5RVUZOTEU5QlFVOHNVVUZCVUN4RlFVRnBRaXhGUVVGcVFpeERRVVJRT3p0QlFVZEJMRTlCUVVrc1EwRkJReXhIUVVGTUxFVkJRVlU3UVVGRFZDeFZRVUZOTEZOQlFWTXNSVUZCVkN4SlFVRmxMRWxCUVVrc1VVRkJVU3hOUVVGYUxFTkJRVzFDTEUxQlFXNUNMRU5CUVhKQ08wRkJRMEU3TzBGQlJVUXNWVUZCVHl4SFFVRlFPMEZCUTBFN08wRkJSVVFzVjBGQlV5eEZRVUZVTEVOQlFWa3NUVUZCV2l4RlFVRnZRaXhKUVVGd1FpeEZRVUV3UWl4RlFVRXhRaXhGUVVFNFFqdEJRVU0zUWl4UFFVRkpMRXRCUVVzc1QwRkJUeXhGUVVGb1FqdEJRVUZCTEU5QlEwTXNUVUZCVFN4UFFVRlBMRkZCUVZBc1JVRkJhVUlzUlVGQmFrSXNRMEZFVURzN1FVRkhRU3hQUVVGSkxGRkJRVkVzVVVGQlVpeEZRVUZwUWl4RlFVRnFRaXhOUVVOR0xFTkJRVU1zUjBGQlJDeEpRVUZSTEVsQlFVa3NhMEpCUkZZc1EwRkJTaXhGUVVOdFF6dEJRVU5zUXl4UlFVRkpMRk5CUVZNc1UwRkJZaXhGUVVGM1FqdEJRVU4yUWl4UlFVRkhMRk5CUVZFc1JVRkJVaXhEUVVGSU8wRkJRMEU3UVVGRFJDeEpRVXhFTEUxQlMwODdRVUZEVGl4VlFVRk5MRlZCUVZVc1RVRkJWaXhEUVVGT08wRkJRMEVzVVVGQlNTeEpRVUZKTEV0QlFVb3NTVUZCWVN4VFFVRlRMRTlCUVRGQ0xFVkJRVzFETzBGQlEyeERMRkZCUVVjc1NVRkJTU3hMUVVGUU8wRkJRMEVzUzBGR1JDeE5RVVZQTzBGQlEwNHNVMEZCU1N4RlFVRktMRU5CUVU4c1NVRkJVQ3hGUVVGaExFVkJRV0k3UVVGRFFUdEJRVU5FTzBGQlEwUTdPMEZCUlVRc1YwRkJVeXhQUVVGVUxFTkJRV2xDTEVkQlFXcENMRVZCUVhOQ0xFOUJRWFJDTEVWQlFTdENPMEZCUXpsQ0xFOUJRVWtzVFVGQlRTeEpRVUZKTEdOQlFXUTdRVUZCUVN4UFFVTkRMRmRCUVZjc1MwRkVXanM3UVVGSFFTeFBRVUZKTEU5QlFVb3NSVUZCWVR0QlFVTmFMRmxCUVZFc1IwRkJVanRCUVVOQkxFbEJSa1FzVFVGRlR6dEJRVU5PTEZOQlFVc3NSMEZCVEN4RlFVRlZMRlZCUVZNc1JVRkJWQ3hGUVVGaE8wRkJRM1JDTEZOQlFVa3NUVUZCVFN4UFFVRlBMRkZCUVZBc1JVRkJhVUlzUlVGQmFrSXNRMEZCVmp0QlFVTkJMRk5CUVVrc1IwRkJTaXhGUVVGVE8wRkJRMUk3UVVGRFFTeFZRVUZKTEV0QlFVb3NSMEZCV1N4SFFVRmFPMEZCUTBFc1ZVRkJTU3hKUVVGSkxFMUJRVW9zUTBGQlZ5eExRVUZtTEVWQlFYTkNPMEZCUTNKQ0xHdENRVUZYTEVsQlFWZzdRVUZEUVN4WFFVRkpMRWxCUVVvc1EwRkJVeXhQUVVGVUxFVkJRV3RDTEVkQlFXeENPMEZCUTBFN1FVRkRSRHRCUVVORUxFdEJWa1E3TzBGQldVRXNVVUZCU1N4RFFVRkRMRkZCUVV3c1JVRkJaVHRCUVVOa0xGTkJRVWtzVDBGQlNpeERRVUZaTEVkQlFWbzdRVUZEUVR0QlFVTkVPMEZCUTBRN08wRkJSVVE3T3pzN1FVRkpRU3hYUVVGVExHVkJRVlFzUjBGQk1rSTdRVUZETVVJN1FVRkRRU3hQUVVGSkxHVkJRV1VzVFVGQmJrSXNSVUZCTWtJN1FVRkRNVUlzVTBGQlN5eGpRVUZNTEVWQlFYRkNMRlZCUVZNc1UwRkJWQ3hGUVVGdlFqdEJRVU40UXl4VFFVRkpMRXRCUVVzc1ZVRkJWU3hEUVVGV0xFTkJRVlE3UVVGRFFTeFRRVUZKTEU5QlFVOHNSVUZCVUN4TFFVRmpMRkZCUVd4Q0xFVkJRVFJDTzBGQlF6TkNMR05CUVZFc1YwRkJVaXhEUVVGdlFpeEZRVUZ3UWl4SlFVRXdRaXhKUVVFeFFqdEJRVU5CTzBGQlEwUXNZMEZCVXl4SlFVRlVMRU5CUVdNc1UwRkJaRHRCUVVOQkxFdEJUa1E3UVVGUFFTeHhRa0ZCYVVJc1JVRkJha0k3UVVGRFFUdEJRVU5FT3p0QlFVVkVMR0ZCUVZjN1FVRkRWaXhqUVVGWExHbENRVUZUTEVkQlFWUXNSVUZCWXp0QlFVTjRRaXhSUVVGSkxFbEJRVWtzVDBGQlVpeEZRVUZwUWp0QlFVTm9RaXhaUVVGUExFbEJRVWtzVDBGQldEdEJRVU5CTEV0QlJrUXNUVUZGVHp0QlFVTk9MRmxCUVZFc1NVRkJTU3hQUVVGS0xFZEJRV01zVVVGQlVTeFhRVUZTTEVOQlFXOUNMRWxCUVVrc1IwRkJlRUlzUTBGQmRFSTdRVUZEUVR0QlFVTkVMRWxCVUZNN1FVRlJWaXhqUVVGWExHbENRVUZUTEVkQlFWUXNSVUZCWXp0QlFVTjRRaXhSUVVGSkxGbEJRVW9zUjBGQmJVSXNTVUZCYmtJN1FVRkRRU3hSUVVGSkxFbEJRVWtzUjBGQlNpeERRVUZSTEZGQlFWb3NSVUZCYzBJN1FVRkRja0lzVTBGQlNTeEpRVUZKTEU5QlFWSXNSVUZCYVVJN1FVRkRhRUlzWVVGQlVTeFRRVUZSTEVsQlFVa3NSMEZCU2l4RFFVRlJMRVZCUVdoQ0xFbEJRWE5DTEVsQlFVa3NUMEZCYkVNN1FVRkRRU3hOUVVaRUxFMUJSVTg3UVVGRFRpeGhRVUZSTEVsQlFVa3NUMEZCU2l4SFFVRmpMRk5CUVZFc1NVRkJTU3hIUVVGS0xFTkJRVkVzUlVGQmFFSXNTVUZCYzBJc1JVRkJOVU03UVVGRFFUdEJRVU5FTzBGQlEwUXNTVUZxUWxNN1FVRnJRbFlzWVVGQlZTeG5Ra0ZCVXl4SFFVRlVMRVZCUVdNN1FVRkRka0lzVVVGQlNTeEpRVUZKTEUxQlFWSXNSVUZCWjBJN1FVRkRaaXhaUVVGUExFbEJRVWtzVFVGQldEdEJRVU5CTEV0QlJrUXNUVUZGVHp0QlFVTk9MRmxCUVZFc1NVRkJTU3hOUVVGS0xFZEJRV0U3UVVGRGNFSXNWVUZCU1N4SlFVRkpMRWRCUVVvc1EwRkJVU3hGUVVSUk8wRkJSWEJDTEZkQlFVc3NTVUZCU1N4SFFVRktMRU5CUVZFc1IwRkdUenRCUVVkd1FpeGpRVUZSTEd0Q1FVRlhPMEZCUTJ4Q0xHTkJRVThzVDBGQlR5eFJRVUZQTEUxQlFXUXNSVUZCYzBJc1NVRkJTU3hIUVVGS0xFTkJRVkVzUlVGQk9VSXNTMEZCY1VNc1JVRkJOVU03UVVGRFFTeFBRVXh0UWp0QlFVMXdRaXhsUVVGVExFbEJRVWtzVDBGQlNpeExRVUZuUWl4SlFVRkpMRTlCUVVvc1IwRkJZeXhGUVVFNVFqdEJRVTVYTEUxQlFYSkNPMEZCVVVFN1FVRkRSRHRCUVM5Q1V5eEhRVUZZT3p0QlFXdERRU3hYUVVGVExHRkJRVlFzUTBGQmRVSXNSVUZCZGtJc1JVRkJNa0k3UVVGRE1VSTdRVUZEUVN4VlFVRlBMRk5CUVZNc1JVRkJWQ3hEUVVGUU8wRkJRMEVzVlVGQlR5eG5Ra0ZCWjBJc1JVRkJhRUlzUTBGQlVEdEJRVU5CT3p0QlFVVkVMRmRCUVZNc1ZVRkJWQ3hEUVVGdlFpeEhRVUZ3UWl4RlFVRjVRaXhOUVVGNlFpeEZRVUZwUXl4VFFVRnFReXhGUVVFMFF6dEJRVU16UXl4UFFVRkpMRXRCUVVzc1NVRkJTU3hIUVVGS0xFTkJRVkVzUlVGQmFrSTdPMEZCUlVFc1QwRkJTU3hKUVVGSkxFdEJRVklzUlVGQlpUdEJRVU5rTEZGQlFVa3NTVUZCU2l4RFFVRlRMRTlCUVZRc1JVRkJhMElzU1VGQlNTeExRVUYwUWp0QlFVTkJMRWxCUmtRc1RVRkZUenRCUVVOT0xGZEJRVThzUlVGQlVDeEpRVUZoTEVsQlFXSTdRVUZEUVN4VFFVRkxMRWxCUVVrc1QwRkJWQ3hGUVVGclFpeFZRVUZUTEUxQlFWUXNSVUZCYVVJc1EwRkJha0lzUlVGQmIwSTdRVUZEY2tNc1UwRkJTU3hSUVVGUkxFOUJRVThzUlVGQmJrSTdRVUZCUVN4VFFVTkRMRTFCUVUwc1QwRkJUeXhSUVVGUUxFVkJRV2xDTEV0QlFXcENMRU5CUkZBN08wRkJSMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVN4VFFVRkpMRTlCUVU4c1EwRkJReXhKUVVGSkxGVkJRVW9zUTBGQlpTeERRVUZtTEVOQlFWSXNTVUZCTmtJc1EwRkJReXhWUVVGVkxFdEJRVllzUTBGQmJFTXNSVUZCYjBRN1FVRkRia1FzVlVGQlNTeFBRVUZQTEUxQlFWQXNSVUZCWlN4TFFVRm1MRU5CUVVvc1JVRkJNa0k3UVVGRE1VSXNWMEZCU1N4VFFVRktMRU5CUVdNc1EwRkJaQ3hGUVVGcFFpeFRRVUZSTEV0QlFWSXNRMEZCYWtJN1FVRkRRU3hYUVVGSkxFdEJRVW9zUjBGR01FSXNRMEZGWWp0QlFVTmlMRTlCU0VRc1RVRkhUenRCUVVOT0xHdENRVUZYTEVkQlFWZ3NSVUZCWjBJc1RVRkJhRUlzUlVGQmQwSXNVMEZCZUVJN1FVRkRRVHRCUVVORU8wRkJRMFFzUzBGb1FrUTdRVUZwUWtFc1kwRkJWU3hGUVVGV0xFbEJRV2RDTEVsQlFXaENPMEZCUTBFN1FVRkRSRHM3UVVGRlJDeFhRVUZUTEZkQlFWUXNSMEZCZFVJN1FVRkRkRUlzVDBGQlNTeFpRVUZLTzBGQlFVRXNUMEZCVXl3d1FrRkJWRHRCUVVGQkxFOUJRME1zWlVGQlpTeFJRVUZQTEZkQlFWQXNSMEZCY1VJc1NVRkVja003TzBGQlJVTTdRVUZEUVN4aFFVRlZMR2RDUVVGcFFpeFJRVUZSTEZOQlFWSXNSMEZCYjBJc1dVRkJja0lzUjBGQmNVTXNTVUZCU1N4SlFVRktMRWRCUVZjc1QwRkJXQ3hGUVVob1JUdEJRVUZCTEU5QlNVTXNWVUZCVlN4RlFVcFlPMEZCUVVFc1QwRkxReXhYUVVGWExFVkJURm83UVVGQlFTeFBRVTFETEdWQlFXVXNTMEZPYUVJN1FVRkJRU3hQUVU5RExHbENRVUZwUWl4SlFWQnNRanM3UVVGVFFUdEJRVU5CTEU5QlFVa3NZVUZCU2l4RlFVRnRRanRCUVVOc1FqdEJRVU5CT3p0QlFVVkVMRzFDUVVGblFpeEpRVUZvUWpzN1FVRkZRVHRCUVVOQkxGbEJRVk1zWlVGQlZDeEZRVUV3UWl4VlFVRlRMRWRCUVZRc1JVRkJZenRCUVVOMlF5eFJRVUZKTEUxQlFVMHNTVUZCU1N4SFFVRmtPMEZCUVVFc1VVRkRReXhSUVVGUkxFbEJRVWtzUlVGRVlqczdRVUZIUVR0QlFVTkJMRkZCUVVrc1EwRkJReXhKUVVGSkxFOUJRVlFzUlVGQmEwSTdRVUZEYWtJN1FVRkRRVHM3UVVGRlJDeFJRVUZKTEVOQlFVTXNTVUZCU1N4UlFVRlVMRVZCUVcxQ08wRkJRMnhDTEdOQlFWTXNTVUZCVkN4RFFVRmpMRWRCUVdRN1FVRkRRVHM3UVVGRlJDeFJRVUZKTEVOQlFVTXNTVUZCU1N4TFFVRlVMRVZCUVdkQ08wRkJRMlk3UVVGRFFUdEJRVU5CTEZOQlFVa3NRMEZCUXl4SlFVRkpMRTFCUVV3c1NVRkJaU3hQUVVGdVFpeEZRVUUwUWp0QlFVTXpRaXhWUVVGSkxHZENRVUZuUWl4TFFVRm9RaXhEUVVGS0xFVkJRVFJDTzBGQlF6TkNMREpDUVVGdlFpeEpRVUZ3UWp0QlFVTkJMSE5DUVVGbExFbEJRV1k3UVVGRFFTeFBRVWhFTEUxQlIwODdRVUZEVGl4bFFVRlJMRWxCUVZJc1EwRkJZU3hMUVVGaU8wRkJRMEVzYjBKQlFXRXNTMEZCWWp0QlFVTkJPMEZCUTBRc1RVRlNSQ3hOUVZGUExFbEJRVWtzUTBGQlF5eEpRVUZKTEUxQlFVd3NTVUZCWlN4SlFVRkpMRTlCUVc1Q0xFbEJRVGhDTEVsQlFVa3NVVUZCZEVNc1JVRkJaMFE3UVVGRGRFUXNjVUpCUVdVc1NVRkJaanRCUVVOQkxGVkJRVWtzUTBGQlF5eEpRVUZKTEUxQlFWUXNSVUZCYVVJN1FVRkRhRUk3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJMR05CUVZFc2FVSkJRV2xDTEV0QlFYcENPMEZCUTBFN1FVRkRSRHRCUVVORU8wRkJRMFFzU1VGd1EwUTdPMEZCYzBOQkxFOUJRVWtzVjBGQlZ5eFJRVUZSTEUxQlFYWkNMRVZCUVN0Q08wRkJRemxDTzBGQlEwRXNWVUZCVFN4VlFVRlZMRk5CUVZZc1JVRkJjVUlzSzBKQlFTdENMRTlCUVhCRUxFVkJRVFpFTEVsQlFUZEVMRVZCUVcxRkxFOUJRVzVGTEVOQlFVNDdRVUZEUVN4UlFVRkpMRmRCUVVvc1IwRkJhMElzVVVGQlVTeFhRVUV4UWp0QlFVTkJMRmRCUVU4c1VVRkJVU3hIUVVGU0xFTkJRVkE3UVVGRFFUczdRVUZGUkR0QlFVTkJMRTlCUVVrc1kwRkJTaXhGUVVGdlFqdEJRVU51UWl4VFFVRkxMRkZCUVV3c1JVRkJaU3hWUVVGVExFZEJRVlFzUlVGQll6dEJRVU0xUWl4blFrRkJWeXhIUVVGWUxFVkJRV2RDTEVWQlFXaENMRVZCUVc5Q0xFVkJRWEJDTzBGQlEwRXNTMEZHUkR0QlFVZEJPenRCUVVWRU8wRkJRMEU3UVVGRFFUdEJRVU5CTEU5QlFVa3NRMEZCUXl4RFFVRkRMRTlCUVVRc1NVRkJXU3hwUWtGQllpeExRVUZ0UXl4WlFVRjJReXhGUVVGeFJEdEJRVU53UkR0QlFVTkJPMEZCUTBFc1VVRkJTU3hEUVVGRExHRkJRV0VzVjBGQlpDeExRVUU0UWl4RFFVRkRMRzlDUVVGdVF5eEZRVUY1UkR0QlFVTjRSQ3cwUWtGQmRVSXNWMEZCVnl4WlFVRlhPMEZCUXpWRExEWkNRVUYxUWl4RFFVRjJRanRCUVVOQk8wRkJRMEVzVFVGSWMwSXNSVUZIY0VJc1JVRkliMElzUTBGQmRrSTdRVUZKUVR0QlFVTkVPenRCUVVWRUxHMUNRVUZuUWl4TFFVRm9RanRCUVVOQk96dEJRVVZFTEZkQlFWTXNaMEpCUVZNc1IwRkJWQ3hGUVVGak8wRkJRM1JDTEZGQlFVc3NUVUZCVEN4SFFVRmpMRTlCUVU4c1YwRkJVQ3hGUVVGdlFpeEpRVUZKTEVWQlFYaENMRXRCUVN0Q0xFVkJRVGRETzBGQlEwRXNVVUZCU3l4SFFVRk1MRWRCUVZjc1IwRkJXRHRCUVVOQkxGRkJRVXNzU1VGQlRDeEhRVUZaTEU5QlFVOHNVVUZCVHl4SlFVRmtMRVZCUVc5Q0xFbEJRVWtzUlVGQmVFSXNRMEZCV2p0QlFVTkJMRkZCUVVzc1ZVRkJUQ3hIUVVGclFpeEZRVUZzUWp0QlFVTkJMRkZCUVVzc1QwRkJUQ3hIUVVGbExFVkJRV1k3UVVGRFFTeFJRVUZMTEZWQlFVd3NSMEZCYTBJc1JVRkJiRUk3UVVGRFFTeFJRVUZMTEZWQlFVd3NSMEZCYTBJc1JVRkJiRUk3UVVGRFFTeFJRVUZMTEZGQlFVd3NSMEZCWjBJc1EwRkJhRUk3TzBGQlJVRTdPenM3UVVGSlFTeEhRV1JFT3p0QlFXZENRU3hUUVVGUExGTkJRVkFzUjBGQmJVSTdRVUZEYkVJc1UwRkJUU3hqUVVGVExFOUJRVlFzUlVGQmEwSXNUMEZCYkVJc1JVRkJNa0lzVDBGQk0wSXNSVUZCYjBNc1QwRkJjRU1zUlVGQk5rTTdRVUZEYkVRc1kwRkJWU3hYUVVGWExFVkJRWEpDT3p0QlFVVkJPMEZCUTBFN1FVRkRRVHRCUVVOQkxGRkJRVWtzUzBGQlN5eE5RVUZVTEVWQlFXbENPMEZCUTJoQ08wRkJRMEU3TzBGQlJVUXNVMEZCU3l4UFFVRk1MRWRCUVdVc1QwRkJaanM3UVVGRlFTeFJRVUZKTEU5QlFVb3NSVUZCWVR0QlFVTmFPMEZCUTBFc1ZVRkJTeXhGUVVGTUxFTkJRVkVzVDBGQlVpeEZRVUZwUWl4UFFVRnFRanRCUVVOQkxFdEJTRVFzVFVGSFR5eEpRVUZKTEV0QlFVc3NUVUZCVEN4RFFVRlpMRXRCUVdoQ0xFVkJRWFZDTzBGQlF6ZENPMEZCUTBFN1FVRkRRU3hsUVVGVkxFdEJRVXNzU1VGQlRDeEZRVUZYTEZWQlFWTXNSMEZCVkN4RlFVRmpPMEZCUTJ4RExGZEJRVXNzU1VGQlRDeERRVUZWTEU5QlFWWXNSVUZCYlVJc1IwRkJia0k3UVVGRFFTeE5RVVpUTEVOQlFWWTdRVUZIUVRzN1FVRkZSRHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRXNVMEZCU3l4UFFVRk1MRWRCUVdVc1YwRkJWeXhSUVVGUkxFdEJRVklzUTBGQll5eERRVUZrTEVOQlFURkNPenRCUVVWQkxGTkJRVXNzVDBGQlRDeEhRVUZsTEU5QlFXWTdPMEZCUlVFN1FVRkRRU3hUUVVGTExFMUJRVXdzUjBGQll5eEpRVUZrT3p0QlFVVkJMRk5CUVVzc1RVRkJUQ3hIUVVGakxGRkJRVkVzVFVGQmRFSTdPMEZCUlVFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFTeFJRVUZKTEZGQlFWRXNUMEZCVWl4SlFVRnRRaXhMUVVGTExFOUJRVFZDTEVWQlFYRkRPMEZCUTNCRE8wRkJRMEU3UVVGRFFTeFZRVUZMTEUxQlFVdzdRVUZEUVN4TFFVcEVMRTFCU1U4N1FVRkRUaXhWUVVGTExFdEJRVXc3UVVGRFFUdEJRVU5FTEVsQmFrUnBRanM3UVVGdFJHeENMR05CUVZjc2JVSkJRVk1zUTBGQlZDeEZRVUZaTEZWQlFWb3NSVUZCZDBJN1FVRkRiRU03UVVGRFFUdEJRVU5CTEZGQlFVa3NRMEZCUXl4TFFVRkxMRlZCUVV3c1EwRkJaMElzUTBGQmFFSXNRMEZCVEN4RlFVRjVRanRCUVVONFFpeFZRVUZMTEZWQlFVd3NRMEZCWjBJc1EwRkJhRUlzU1VGQmNVSXNTVUZCY2tJN1FVRkRRU3hWUVVGTExGRkJRVXdzU1VGQmFVSXNRMEZCYWtJN1FVRkRRU3hWUVVGTExGVkJRVXdzUTBGQlowSXNRMEZCYUVJc1NVRkJjVUlzVlVGQmNrSTdRVUZEUVR0QlFVTkVMRWxCTTBScFFqczdRVUUyUkd4Q0xGVkJRVThzYVVKQlFWYzdRVUZEYWtJc1VVRkJTU3hMUVVGTExFOUJRVlFzUlVGQmEwSTdRVUZEYWtJN1FVRkRRVHRCUVVORUxGTkJRVXNzVDBGQlRDeEhRVUZsTEVsQlFXWTdPMEZCUlVFc1dVRkJVU3hUUVVGU0xFZEJRWEZDTEVsQlFVa3NTVUZCU2l4RlFVRkVMRU5CUVdFc1QwRkJZaXhGUVVGd1FqczdRVUZGUVN4UlFVRkpMRTFCUVUwc1MwRkJTeXhIUVVGbU96dEJRVVZCTzBGQlEwRTdRVUZEUVN4UlFVRkpMRXRCUVVzc1NVRkJWQ3hGUVVGbE8wRkJRMlFzWVVGQlVTeFhRVUZTTEVOQlFXOUNMRXRCUVVzc1IwRkJla0lzUlVGQk9FSTdRVUZETjBJc01rSkJRWEZDTzBGQlJGRXNUVUZCT1VJc1JVRkZSeXhMUVVGTExFbEJRVXdzUTBGQlZTeEpRVUZXTEVsQlFXdENMRVZCUm5KQ0xFVkJSWGxDTEV0QlFVc3NTVUZCVEN4RlFVRlhMRmxCUVZjN1FVRkRPVU1zWVVGQlR5eEpRVUZKTEUxQlFVb3NSMEZCWVN4TFFVRkxMRlZCUVV3c1JVRkJZaXhIUVVGcFF5eExRVUZMTEVsQlFVd3NSVUZCZUVNN1FVRkRRU3hOUVVaM1FpeERRVVo2UWp0QlFVdEJMRXRCVGtRc1RVRk5UenRCUVVOT08wRkJRMEVzV1VGQlR5eEpRVUZKTEUxQlFVb3NSMEZCWVN4TFFVRkxMRlZCUVV3c1JVRkJZaXhIUVVGcFF5eExRVUZMTEVsQlFVd3NSVUZCZUVNN1FVRkRRVHRCUVVORUxFbEJia1pwUWpzN1FVRnhSbXhDTEZOQlFVMHNaMEpCUVZjN1FVRkRhRUlzVVVGQlNTeE5RVUZOTEV0QlFVc3NSMEZCVEN4RFFVRlRMRWRCUVc1Q096dEJRVVZCTzBGQlEwRXNVVUZCU1N4RFFVRkRMRmRCUVZjc1IwRkJXQ3hEUVVGTUxFVkJRWE5DTzBGQlEzSkNMR2RDUVVGWExFZEJRVmdzU1VGQmEwSXNTVUZCYkVJN1FVRkRRU3hoUVVGUkxFbEJRVklzUTBGQllTeExRVUZMTEVkQlFVd3NRMEZCVXl4RlFVRjBRaXhGUVVFd1FpeEhRVUV4UWp0QlFVTkJPMEZCUTBRc1NVRTNSbWxDT3p0QlFTdEdiRUk3T3pzN1FVRkpRU3hWUVVGUExHbENRVUZYTzBGQlEycENMRkZCUVVrc1EwRkJReXhMUVVGTExFOUJRVTRzU1VGQmFVSXNTMEZCU3l4UlFVRXhRaXhGUVVGdlF6dEJRVU51UXp0QlFVTkJPenRCUVVWRUxGRkJRVWtzV1VGQlNqdEJRVUZCTEZGQlFWTXNhMEpCUVZRN1FVRkJRU3hSUVVORExFdEJRVXNzUzBGQlN5eEhRVUZNTEVOQlFWTXNSVUZFWmp0QlFVRkJMRkZCUlVNc1lVRkJZU3hMUVVGTExGVkJSbTVDTzBGQlFVRXNVVUZIUXl4VlFVRlZMRXRCUVVzc1QwRklhRUk3UVVGQlFTeFJRVWxETEZWQlFWVXNTMEZCU3l4UFFVcG9RanM3UVVGTlFTeFJRVUZKTEVOQlFVTXNTMEZCU3l4TlFVRldMRVZCUVd0Q08wRkJRMnBDTzBGQlEwRXNVMEZCU1N4RFFVRkRMRkZCUVZFc1VVRkJVU3hYUVVGb1FpeEZRVUUyUWl4RlFVRTNRaXhEUVVGTUxFVkJRWFZETzBGQlEzUkRMRmRCUVVzc1MwRkJURHRCUVVOQk8wRkJRMFFzUzBGTVJDeE5RVXRQTEVsQlFVa3NTMEZCU3l4TFFVRlVMRVZCUVdkQ08wRkJRM1JDTEZWQlFVc3NTVUZCVEN4RFFVRlZMRTlCUVZZc1JVRkJiVUlzUzBGQlN5eExRVUY0UWp0QlFVTkJMRXRCUmswc1RVRkZRU3hKUVVGSkxFTkJRVU1zUzBGQlN5eFJRVUZXTEVWQlFXOUNPMEZCUXpGQ08wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRXNWVUZCU3l4UlFVRk1MRWRCUVdkQ0xFbEJRV2hDT3p0QlFVVkJMRk5CUVVrc1MwRkJTeXhSUVVGTUxFZEJRV2RDTEVOQlFXaENMRWxCUVhGQ0xFTkJRVU1zUzBGQlN5eFBRVUV2UWl4RlFVRjNRenRCUVVOMlF5eFZRVUZKTEZkQlFWY3NUMEZCV0N4RFFVRktMRVZCUVhsQ08wRkJRM2hDTEZkQlFVazdRVUZEU0N4clFrRkJWU3hSUVVGUkxFMUJRVklzUTBGQlpTeEZRVUZtTEVWQlFXMUNMRTlCUVc1Q0xFVkJRVFJDTEZWQlFUVkNMRVZCUVhkRExFOUJRWGhETEVOQlFWWTdRVUZEUVN4UlFVWkVMRU5CUlVVc1QwRkJUeXhEUVVGUUxFVkJRVlU3UVVGRFdDeGpRVUZOTEVOQlFVNDdRVUZEUVRzN1FVRkZSRHRCUVVOQk8wRkJRMEU3UVVGRFFTeFhRVUZKTEV0QlFVc3NSMEZCVEN4RFFVRlRMRkZCUVZRc1NVRkJjVUlzV1VGQldTeFRRVUZ5UXl4RlFVRm5SRHRCUVVNdlF5eHZRa0ZCV1N4TFFVRkxMRTFCUVdwQ08wRkJRMEVzV1VGQlNTeFRRVUZLTEVWQlFXVTdRVUZEWkN4dFFrRkJWU3hWUVVGVkxFOUJRWEJDTzBGQlEwRXNVMEZHUkN4TlFVVlBMRWxCUVVrc1MwRkJTeXhaUVVGVUxFVkJRWFZDTzBGQlF6ZENPMEZCUTBFc2JVSkJRVlVzUzBGQlN5eFBRVUZtTzBGQlEwRTdRVUZEUkRzN1FVRkZSQ3hYUVVGSkxFZEJRVW9zUlVGQlV6dEJRVU5TTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQkxGbEJRVXNzUzBGQlN5eE5RVUZNTEVOQlFWa3NTMEZCV2l4SlFVRnhRaXhMUVVGTExFZEJRVXdzUTBGQlV5eFJRVUV2UWl4SlFVTklMRWxCUVVrc1QwRkJTaXhMUVVGblFpeGpRVVJxUWl4RlFVTnBRenRCUVVOb1F5eGhRVUZKTEZWQlFVb3NSMEZCYVVJc1MwRkJTeXhIUVVGMFFqdEJRVU5CTEdGQlFVa3NZMEZCU2l4SFFVRnhRaXhMUVVGTExFZEJRVXdzUTBGQlV5eFJRVUZVTEVkQlFXOUNMRU5CUVVNc1MwRkJTeXhIUVVGTUxFTkJRVk1zUlVGQlZpeERRVUZ3UWl4SFFVRnZReXhKUVVGNlJEdEJRVU5CTEdGQlFVa3NWMEZCU2l4SFFVRnJRaXhMUVVGTExFZEJRVXdzUTBGQlV5eFJRVUZVTEVkQlFXOUNMRkZCUVhCQ0xFZEJRU3RDTEZOQlFXcEVPMEZCUTBFc1owSkJRVThzVVVGQlV5eExRVUZMTEV0QlFVd3NSMEZCWVN4SFFVRjBRaXhEUVVGUU8wRkJRMEVzVTBGT1JDeE5RVTFQTEVsQlFVa3NUMEZCVHl4UFFVRlFMRXRCUVcxQ0xGZEJRVzVDTEVsQlExWXNVVUZCVVN4TFFVUkdMRVZCUTFNN1FVRkRaanRCUVVOQk8wRkJRMEVzYVVKQlFWRXNTMEZCVWl4RFFVRmpMRWRCUVdRN1FVRkRRU3hUUVV4TkxFMUJTMEU3UVVGRFRqdEJRVU5CTzBGQlEwRTdRVUZEUVN4aFFVRkpMRTlCUVVvc1EwRkJXU3hIUVVGYU8wRkJRMEU3UVVGRFJEdEJRVU5FTEU5Qk4wTkVMRTFCTmtOUE8wRkJRMDQ3UVVGRFFTeHBRa0ZCVlN4UFFVRldPMEZCUTBFN08wRkJSVVFzVjBGQlN5eFBRVUZNTEVkQlFXVXNUMEZCWmpzN1FVRkZRU3hWUVVGSkxFdEJRVXNzUjBGQlRDeERRVUZUTEZGQlFWUXNTVUZCY1VJc1EwRkJReXhMUVVGTExFMUJRUzlDTEVWQlFYVkRPMEZCUTNSRExHZENRVUZSTEVWQlFWSXNTVUZCWXl4UFFVRmtPenRCUVVWQkxGZEJRVWtzU1VGQlNTeGpRVUZTTEVWQlFYZENPMEZCUTNaQ0xGbEJRVWtzWTBGQll5eEZRVUZzUWp0QlFVTkJMR0ZCUVVzc1MwRkJTeXhQUVVGV0xFVkJRVzFDTEZWQlFWTXNUVUZCVkN4RlFVRnBRanRCUVVOdVF5eHhRa0ZCV1N4SlFVRmFMRU5CUVdsQ0xFOUJRVThzWVVGQlVDeEpRVUYzUWl4TlFVRjZRenRCUVVOQkxGTkJSa1E3UVVGSFFTeFpRVUZKTEdOQlFVb3NRMEZCYlVJc1QwRkJia0lzUlVGQk5FSXNTMEZCU3l4SFFVRnFReXhGUVVGelF5eFhRVUYwUXp0QlFVTkJPMEZCUTBRN08wRkJSVVE3UVVGRFFTeHZRa0ZCWXl4RlFVRmtPenRCUVVWQkxGZEJRVXNzVDBGQlRDeEhRVUZsTEVsQlFXWTdRVUZEUVRzN1FVRkZSRHRCUVVOQk8wRkJRMEU3UVVGRFFTeFZRVUZMTEZGQlFVd3NSMEZCWjBJc1MwRkJhRUk3TzBGQlJVRXNVMEZCU1N4TFFVRkxMRTlCUVV3c1NVRkJaMElzUTBGQlF5eExRVUZMTEdGQlFURkNMRVZCUVhsRE8wRkJRM2hETEZkQlFVc3NZVUZCVEN4SFFVRnhRaXhKUVVGeVFqdEJRVU5CTEZkQlFVc3NTVUZCVEN4RFFVRlZMRk5CUVZZc1JVRkJjVUlzUzBGQlN5eFBRVUV4UWp0QlFVTkJMRmRCUVVzc2EwSkJRVXdzUjBGQk1FSXNTVUZCTVVJN1FVRkRRVHRCUVVWRU8wRkJRMFFzU1VFdlRXbENPenRCUVdsT2JFSXNaVUZCV1N4elFrRkJWenRCUVVOMFFpeFJRVUZKTEUxQlFVMHNTMEZCU3l4SFFVRm1PMEZCUTBFc1VVRkJTU3hMUVVGTExFbEJRVWtzUlVGQllqdEJRVU5CTzBGQlEwRXNVVUZCU1N4WlFVRlpMR05CUVdNc1NVRkJTU3hOUVVGc1FpeERRVUZvUWpzN1FVRkZRVHRCUVVOQk8wRkJRMEVzVTBGQlN5eFBRVUZNTEVOQlFXRXNTVUZCWWl4RFFVRnJRaXhUUVVGc1FqczdRVUZGUVN4UFFVRkhMRk5CUVVnc1JVRkJZeXhUUVVGa0xFVkJRWGxDTEV0QlFVc3NTVUZCVEN4RlFVRlhMRlZCUVZNc1RVRkJWQ3hGUVVGcFFqdEJRVU53UkN4VFFVRkpMR0ZCUVVvN1FVRkJRU3hUUVVGVkxITkNRVUZXTzBGQlFVRXNVMEZCZVVJc2MwSkJRWHBDTzBGQlFVRXNVMEZEUXl4WFFVRlhMRTlCUVU4c1ZVRkJVQ3hGUVVGdFFpeExRVUZMTEVkQlFVd3NRMEZCVXl4RlFVRTFRaXhEUVVSYU8wRkJRVUVzVTBGRlF5eFBRVUZQTEV0QlFVc3NSMEZCVEN4RFFVRlRMRWxCUm1wQ08wRkJRVUVzVTBGSFF5eGhRVUZoTEV0QlFVc3NSMEZCVEN4RFFVRlRMRk5CUVZRc1IwRkJjVUlzUzBGQlN5eEhRVUZNTEVOQlFWTXNVMEZCVkN4RFFVRnRRaXhKUVVGNFF5eEhRVUVyUXl4SlFVZzNSRHRCUVVGQkxGTkJTVU1zWlVGQlpTeFJRVUZSTEZkQlFWSXNRMEZCYjBJc1NVRkJTU3hUUVVGNFFpeEZRVUZ0UXp0QlFVTnFSQ3d5UWtGQmNVSTdRVUZFTkVJc1RVRkJia01zUTBGS2FFSTdPMEZCVVVFN1FVRkRRVHRCUVVOQkxGTkJRVWtzUzBGQlN5eEhRVUZNTEVOQlFWTXNXVUZCWWl4RlFVRXlRanRCUVVNeFFqdEJRVU5CTEZWQlFVa3NUMEZCVHl4VFFVRllMRVZCUVhOQ08wRkJRM0pDTEdOQlFVOHNUMEZCVHl4VFFVRlFMRU5CUVdsQ0xFbEJRV3BDTEVWQlFYVkNMRlZCUVZNc1NVRkJWQ3hGUVVGbE8wRkJRek5ETEdWQlFVOHNWVUZCVlN4SlFVRldMRVZCUVdkQ0xGVkJRV2hDTEVWQlFUUkNMRWxCUVRWQ0xFTkJRVkE3UVVGRFFTeFJRVVpMTEV0QlJVRXNSVUZHVUR0QlFVZEJPenRCUVVWRU8wRkJRMEU3UVVGRFFTeHpRa0ZCWjBJc1kwRkJZeXhKUVVGSkxFMUJRVW9zUjBGQllTeEhRVUZpTEVkQlFXMUNMRWxCUVdwRExFVkJRMllzUzBGQlN5eEhRVUZNTEVOQlFWTXNVMEZFVFN4RFFVRm9RanRCUVVWQkxGTkJRVWNzWVVGQlNDeEZRVU5ETEZOQlJFUXNSVUZEV1N4TFFVRkxMRWxCUVV3c1JVRkJWeXhWUVVGVExFdEJRVlFzUlVGQlowSTdRVUZEY2tNc1dVRkJTeXhIUVVGTUxFTkJRVk1zWVVGQlZDeEhRVUY1UWl4aFFVRjZRanRCUVVOQkxGbEJRVXNzU1VGQlRDeERRVUZWTEVWQlFWWXNSVUZCWXl4WlFVRlhPMEZCUTNoQ0xHVkJRVThzUzBGQlVEdEJRVU5CTEZGQlJrUXNSVUZGUnl4SlFVWklMRVZCUlZNN1FVRkRVaXhwUWtGQlV5eEpRVVJFTzBGQlJWSXNaMEpCUVZFN1FVRkdRU3hSUVVaVU8wRkJUVUVzVDBGU1ZTeERRVVJhT3p0QlFWZEJMSE5DUVVGblFpeFBRVUZQTEZGQlFWQXNSVUZCYVVJc1kwRkJZeXhGUVVFdlFpeERRVUZvUWp0QlFVTkJMRlZCUVVrc1lVRkJTaXhGUVVGdFFqdEJRVU5zUWp0QlFVTkJPMEZCUTBFc1dVRkJTeXhQUVVGTUxFTkJRV0VzU1VGQllpeERRVUZyUWl4aFFVRnNRanM3UVVGRlFTeFhRVUZKTEV0QlFVc3NUVUZCVEN4RFFVRlpMRXRCUVdoQ0xFVkJRWFZDTzBGQlEzUkNMSE5DUVVGakxFVkJRV1FzUTBGQmFVSXNUMEZCYWtJc1JVRkJNRUlzUzBGQlN5eEpRVUZNTEVWQlFWY3NWVUZCVXl4SFFVRlVMRVZCUVdNN1FVRkRiRVFzWTBGQlN5eEpRVUZNTEVOQlFWVXNUMEZCVml4RlFVRnRRaXhIUVVGdVFqdEJRVU5CTEZOQlJubENMRU5CUVRGQ08wRkJSMEU3UVVGRFJDeHhRa0ZCWXl4TlFVRmtPMEZCUTBFN08wRkJSVVE3UVVGRFFUczdRVUZGUkR0QlFVTkJPMEZCUTBFc1UwRkJTU3hSUVVGS0xFVkJRV003UVVGRFlpeFhRVUZMTEVkQlFVd3NRMEZCVXl4SFFVRlVMRWRCUVdVc1VVRkJVU3hUUVVGU0xFTkJRV3RDTEZGQlFXeENMRU5CUVdZN1FVRkRRU3hYUVVGTExFbEJRVXc3UVVGRFFUdEJRVU5CT3p0QlFVVkVMRmxCUVU4c1MwRkJTeXhKUVVGTUxFVkJRVmNzVlVGQlV5eExRVUZVTEVWQlFXZENPMEZCUTJwRExGZEJRVXNzU1VGQlRDeERRVUZWTEVWQlFWWXNSVUZCWXl4WlFVRlhPMEZCUTNoQ0xHTkJRVThzUzBGQlVEdEJRVU5CTEU5QlJrUXNSVUZGUnl4SlFVWklMRVZCUlZNN1FVRkRVaXhuUWtGQlV6dEJRVVJFTEU5QlJsUTdRVUZMUVN4TlFVNU5MRU5CUVZBN08wRkJVVUVzVlVGQlN5eExRVUZNTEVkQlFXRXNTMEZCU3l4SlFVRk1MRVZCUVZjc1ZVRkJVeXhIUVVGVUxFVkJRV003UVVGRGNrTXNWMEZCU3l4TlFVRk1MRWRCUVdNc1NVRkJaRHRCUVVOQkxGZEJRVXNzUzBGQlRDeEhRVUZoTEVkQlFXSTdRVUZEUVN4VlFVRkpMR05CUVVvc1IwRkJjVUlzUTBGQlF5eEZRVUZFTEVOQlFYSkNPenRCUVVWQk8wRkJRMEU3UVVGRFFTeGxRVUZUTEZGQlFWUXNSVUZCYlVJc1ZVRkJVeXhIUVVGVUxFVkJRV003UVVGRGFFTXNWMEZCU1N4SlFVRkpMRWRCUVVvc1EwRkJVU3hGUVVGU0xFTkJRVmNzVDBGQldDeERRVUZ0UWl4TFFVRkxMR1ZCUVhoQ0xFMUJRVFpETEVOQlFXcEVMRVZCUVc5RU8wRkJRMjVFTEhOQ1FVRmpMRWxCUVVrc1IwRkJTaXhEUVVGUkxFVkJRWFJDTzBGQlEwRTdRVUZEUkN4UFFVcEVPenRCUVUxQkxHTkJRVkVzUjBGQlVqdEJRVU5CTEUxQlpGa3NRMEZCWWpzN1FVRm5Ra0U3UVVGRFFUdEJRVU5CTEZWQlFVc3NVVUZCVEN4SFFVRm5RaXhMUVVGTExFbEJRVXdzUlVGQlZ5eFZRVUZUTEVsQlFWUXNSVUZCWlN4UFFVRm1MRVZCUVhkQ08wRkJRMnhFTzBGQlEwRXNWVUZCU1N4aFFVRmhMRWxCUVVrc1NVRkJja0k3UVVGQlFTeFZRVU5ETEZsQlFWa3NZMEZCWXl4VlFVRmtMRU5CUkdJN1FVRkJRU3hWUVVWRExHbENRVUZwUWl4alFVWnNRanM3UVVGSlFUdEJRVU5CTzBGQlEwRTdRVUZEUVN4VlFVRkpMRTlCUVVvc1JVRkJZVHRCUVVOYUxHTkJRVThzVDBGQlVEdEJRVU5CT3p0QlFVVkVPMEZCUTBFN1FVRkRRU3hWUVVGSkxHTkJRVW9zUlVGQmIwSTdRVUZEYmtJc2QwSkJRV2xDTEV0QlFXcENPMEZCUTBFN08wRkJSVVE3UVVGRFFTeG5Ra0ZCVlN4VFFVRldPenRCUVVWQk8wRkJRMEVzVlVGQlNTeFJRVUZSTEZGQlFVOHNUVUZCWml4RlFVRjFRaXhGUVVGMlFpeERRVUZLTEVWQlFXZERPMEZCUXk5Q0xHVkJRVThzVFVGQlVDeERRVUZqTEZWQlFXUXNTVUZCTkVJc1VVRkJUeXhOUVVGUUxFTkJRV01zUlVGQlpDeERRVUUxUWp0QlFVTkJPenRCUVVWRUxGVkJRVWs3UVVGRFNDeFhRVUZKTEVsQlFVb3NRMEZCVXl4SlFVRlVPMEZCUTBFc1QwRkdSQ3hEUVVWRkxFOUJRVThzUTBGQlVDeEZRVUZWTzBGQlExZ3NZMEZCVHl4UlFVRlJMRlZCUVZVc1kwRkJWaXhGUVVOa0xIVkNRVUYxUWl4RlFVRjJRaXhIUVVOQkxGZEJSRUVzUjBGRFl5eERRVVpCTEVWQlIyUXNRMEZJWXl4RlFVbGtMRU5CUVVNc1JVRkJSQ3hEUVVwakxFTkJRVklzUTBGQlVEdEJRVXRCT3p0QlFVVkVMRlZCUVVrc1kwRkJTaXhGUVVGdlFqdEJRVU51UWl4M1FrRkJhVUlzU1VGQmFrSTdRVUZEUVRzN1FVRkZSRHRCUVVOQkxGZEJRVXNzVDBGQlRDeERRVUZoTEVsQlFXSXNRMEZCYTBJc1UwRkJiRUk3TzBGQlJVRTdRVUZEUVN4alFVRlJMRmxCUVZJc1EwRkJjVUlzVlVGQmNrSTdPMEZCUlVFN1FVRkRRU3h0UWtGQllTeERRVUZETEZWQlFVUXNRMEZCWWl4RlFVRXlRaXhKUVVFelFqdEJRVU5CTEUxQmFrUmxMRU5CUVdoQ096dEJRVzFFUVR0QlFVTkJPMEZCUTBFc1dVRkJUeXhKUVVGUUxFTkJRVmtzU1VGQlNTeEpRVUZvUWl4RlFVRnpRaXhaUVVGMFFpeEZRVUZ2UXl4SlFVRndReXhGUVVFd1F5eFBRVUV4UXp0QlFVTkJMRXRCTTBsM1FpeERRVUY2UWpzN1FVRTJTVUVzV1VGQlVTeE5RVUZTTEVOQlFXVXNVMEZCWml4RlFVRXdRaXhKUVVFeFFqdEJRVU5CTEZOQlFVc3NWVUZCVEN4RFFVRm5RaXhWUVVGVkxFVkJRVEZDTEVsQlFXZERMRk5CUVdoRE8wRkJRMEVzU1VFeFYybENPenRCUVRSWGJFSXNWMEZCVVN4clFrRkJWenRCUVVOc1FpeHZRa0ZCWjBJc1MwRkJTeXhIUVVGTUxFTkJRVk1zUlVGQmVrSXNTVUZCSzBJc1NVRkJMMEk3UVVGRFFTeFRRVUZMTEU5QlFVd3NSMEZCWlN4SlFVRm1PenRCUVVWQk8wRkJRMEU3UVVGRFFTeFRRVUZMTEZGQlFVd3NSMEZCWjBJc1NVRkJhRUk3TzBGQlJVRTdRVUZEUVN4VFFVRkxMRXRCUVVzc1QwRkJWaXhGUVVGdFFpeExRVUZMTEVsQlFVd3NSVUZCVnl4VlFVRlRMRTFCUVZRc1JVRkJhVUlzUTBGQmFrSXNSVUZCYjBJN1FVRkRha1FzVTBGQlNTeFhRVUZLTzBGQlEwRXNVMEZCU1N4WlFVRktPMEZCUTBFc1UwRkJTU3huUWtGQlNqczdRVUZGUVN4VFFVRkpMRTlCUVU4c1RVRkJVQ3hMUVVGclFpeFJRVUYwUWl4RlFVRm5RenRCUVVNdlFqdEJRVU5CTEdWQlFWTXNZMEZCWXl4TlFVRmtMRVZCUTFBc1MwRkJTeXhIUVVGTUxFTkJRVk1zVVVGQlZDeEhRVUZ2UWl4TFFVRkxMRWRCUVhwQ0xFZEJRU3RDTEV0QlFVc3NSMEZCVEN4RFFVRlRMRk5CUkdwRExFVkJSVklzUzBGR1VTeEZRVWRTTEVOQlFVTXNTMEZCU3l4UFFVaEZMRU5CUVZRN1FVRkpRU3hYUVVGTExFOUJRVXdzUTBGQllTeERRVUZpTEVsQlFXdENMRTFCUVd4Q096dEJRVVZCTEdkQ1FVRlZMRTlCUVU4c1VVRkJVQ3hGUVVGcFFpeFBRVUZQTEVWQlFYaENMRU5CUVZZN08wRkJSVUVzVlVGQlNTeFBRVUZLTEVWQlFXRTdRVUZEV2l4WlFVRkxMRlZCUVV3c1EwRkJaMElzUTBGQmFFSXNTVUZCY1VJc1VVRkJVU3hKUVVGU0xFTkJRWEpDTzBGQlEwRTdRVUZEUVRzN1FVRkZSQ3hYUVVGTExGRkJRVXdzU1VGQmFVSXNRMEZCYWtJN08wRkJSVUVzVTBGQlJ5eE5RVUZJTEVWQlFWY3NVMEZCV0N4RlFVRnpRaXhMUVVGTExFbEJRVXdzUlVGQlZ5eFZRVUZUTEZWQlFWUXNSVUZCY1VJN1FVRkRja1FzVjBGQlNTeExRVUZMTEU5QlFWUXNSVUZCYTBJN1FVRkRha0k3UVVGRFFUdEJRVU5FTEZsQlFVc3NVMEZCVEN4RFFVRmxMRU5CUVdZc1JVRkJhMElzVlVGQmJFSTdRVUZEUVN4WlFVRkxMRXRCUVV3N1FVRkRRU3hQUVU1eFFpeERRVUYwUWpzN1FVRlJRU3hWUVVGSkxFdEJRVXNzVDBGQlZDeEZRVUZyUWp0QlFVTnFRaXhWUVVGSExFMUJRVWdzUlVGQlZ5eFBRVUZZTEVWQlFXOUNMRXRCUVVzc1NVRkJUQ3hGUVVGWExFdEJRVXNzVDBGQmFFSXNRMEZCY0VJN1FVRkRRU3hQUVVaRUxFMUJSVThzU1VGQlNTeExRVUZMTEUxQlFVd3NRMEZCV1N4TFFVRm9RaXhGUVVGMVFqdEJRVU0zUWp0QlFVTkJPMEZCUTBFc1ZVRkJSeXhOUVVGSUxFVkJRVmNzVDBGQldDeEZRVUZ2UWl4TFFVRkxMRWxCUVV3c1JVRkJWeXhWUVVGVExFZEJRVlFzUlVGQll6dEJRVU0xUXl4aFFVRkxMRWxCUVV3c1EwRkJWU3hQUVVGV0xFVkJRVzFDTEVkQlFXNUNPMEZCUTBFc1VVRkdiVUlzUTBGQmNFSTdRVUZIUVR0QlFVTkVPenRCUVVWRUxGVkJRVXNzVDBGQlR5eEZRVUZhTzBGQlEwRXNWMEZCVFN4VFFVRlRMRVZCUVZRc1EwRkJUanM3UVVGRlFUdEJRVU5CTzBGQlEwRXNVMEZCU1N4RFFVRkRMRkZCUVZFc1VVRkJVaXhGUVVGclFpeEZRVUZzUWl4RFFVRkVMRWxCUVRCQ0xFZEJRVEZDTEVsQlFXbERMRU5CUVVNc1NVRkJTU3hQUVVFeFF5eEZRVUZ0UkR0QlFVTnNSQ3hqUVVGUkxFMUJRVklzUTBGQlpTeE5RVUZtTEVWQlFYVkNMRWxCUVhaQ08wRkJRMEU3UVVGRFJDeExRV3BFYTBJc1EwRkJia0k3TzBGQmJVUkJPMEZCUTBFc1lVRkJVeXhMUVVGTExGVkJRV1FzUlVGQk1FSXNTMEZCU3l4SlFVRk1MRVZCUVZjc1ZVRkJVeXhUUVVGVUxFVkJRVzlDTzBGQlEzaEVMRk5CUVVrc1RVRkJUU3hQUVVGUExGRkJRVkFzUlVGQmFVSXNWVUZCVlN4RlFVRXpRaXhEUVVGV08wRkJRMEVzVTBGQlNTeFBRVUZQTEVOQlFVTXNTVUZCU1N4UFFVRm9RaXhGUVVGNVFqdEJRVU40UWl4alFVRlJMRTFCUVZJc1EwRkJaU3hUUVVGbUxFVkJRVEJDTEVsQlFURkNPMEZCUTBFN1FVRkRSQ3hMUVV4NVFpeERRVUV4UWpzN1FVRlBRU3hUUVVGTExGRkJRVXdzUjBGQlowSXNTMEZCYUVJN08wRkJSVUVzVTBGQlN5eExRVUZNTzBGQlEwRXNTVUZ1WW1sQ096dEJRWEZpYkVJc1QwRkJTU3haUVVGVExFbEJRVlFzUlVGQlpTeEZRVUZtTEVWQlFXMUNPMEZCUTNSQ0xGRkJRVWtzVFVGQlRTeExRVUZMTEUxQlFVd3NRMEZCV1N4SlFVRmFMRU5CUVZZN1FVRkRRU3hSUVVGSkxFTkJRVU1zUjBGQlRDeEZRVUZWTzBGQlExUXNWMEZCVFN4TFFVRkxMRTFCUVV3c1EwRkJXU3hKUVVGYUxFbEJRVzlDTEVWQlFURkNPMEZCUTBFN1FVRkRSQ3hSUVVGSkxFbEJRVW9zUTBGQlV5eEZRVUZVTzBGQlEwRXNTVUV6WW1sQ096dEJRVFppYkVJc1UwRkJUU3hqUVVGVExFbEJRVlFzUlVGQlpTeEhRVUZtTEVWQlFXOUNPMEZCUTNwQ0xGTkJRVXNzUzBGQlN5eE5RVUZNTEVOQlFWa3NTVUZCV2l4RFFVRk1MRVZCUVhkQ0xGVkJRVk1zUlVGQlZDeEZRVUZoTzBGQlEzQkRMRkZCUVVjc1IwRkJTRHRCUVVOQkxFdEJSa1E3UVVGSFFTeFJRVUZKTEZOQlFWTXNUMEZCWWl4RlFVRnpRanRCUVVOeVFqdEJRVU5CTzBGQlEwRXNXVUZCVHl4TFFVRkxMRTFCUVV3c1EwRkJXU3hKUVVGYUxFTkJRVkE3UVVGRFFUdEJRVU5FTzBGQmRHTnBRaXhIUVVGdVFqczdRVUY1WTBFc1YwRkJVeXhoUVVGVUxFTkJRWFZDTEVsQlFYWkNMRVZCUVRaQ08wRkJRelZDTzBGQlEwRXNUMEZCU1N4RFFVRkRMRkZCUVZFc1VVRkJVaXhGUVVGcFFpeExRVUZMTEVOQlFVd3NRMEZCYWtJc1EwRkJUQ3hGUVVGblF6dEJRVU12UWl4alFVRlZMR05CUVdNc1MwRkJTeXhEUVVGTUxFTkJRV1FzUlVGQmRVSXNTVUZCZGtJc1JVRkJOa0lzU1VGQk4wSXNRMEZCVml4RlFVRTRReXhKUVVFNVF5eERRVUZ0UkN4TFFVRkxMRU5CUVV3c1EwRkJia1FzUlVGQk5FUXNTMEZCU3l4RFFVRk1MRU5CUVRWRU8wRkJRMEU3UVVGRFJEczdRVUZGUkN4WFFVRlRMR05CUVZRc1EwRkJkMElzU1VGQmVFSXNSVUZCT0VJc1NVRkJPVUlzUlVGQmIwTXNTVUZCY0VNc1JVRkJNRU1zVFVGQk1VTXNSVUZCYTBRN1FVRkRha1E3UVVGRFFTeFBRVUZKTEV0QlFVc3NWMEZCVEN4SlFVRnZRaXhEUVVGRExFOUJRWHBDTEVWQlFXdERPMEZCUTJwRE8wRkJRMEVzVVVGQlNTeE5RVUZLTEVWQlFWazdRVUZEV0N4VlFVRkxMRmRCUVV3c1EwRkJhVUlzVFVGQmFrSXNSVUZCZVVJc1NVRkJla0k3UVVGRFFUdEJRVU5FTEVsQlRFUXNUVUZMVHp0QlFVTk9MRk5CUVVzc2JVSkJRVXdzUTBGQmVVSXNTVUZCZWtJc1JVRkJLMElzU1VGQkwwSXNSVUZCY1VNc1MwRkJja003UVVGRFFUdEJRVU5FT3p0QlFVVkVPenM3T3pzN096dEJRVkZCTEZkQlFWTXNZVUZCVkN4RFFVRjFRaXhIUVVGMlFpeEZRVUUwUWp0QlFVTXpRanRCUVVOQk8wRkJRMEVzVDBGQlNTeFBRVUZQTEVsQlFVa3NZVUZCU2l4SlFVRnhRaXhKUVVGSkxGVkJRWEJET3p0QlFVVkJPMEZCUTBFc2EwSkJRV1VzU1VGQlppeEZRVUZ4UWl4UlFVRlJMRmxCUVRkQ0xFVkJRVEpETEUxQlFUTkRMRVZCUVcxRUxHOUNRVUZ1UkR0QlFVTkJMR3RDUVVGbExFbEJRV1lzUlVGQmNVSXNVVUZCVVN4aFFVRTNRaXhGUVVFMFF5eFBRVUUxUXpzN1FVRkZRU3hWUVVGUE8wRkJRMDRzVlVGQlRTeEpRVVJCTzBGQlJVNHNVVUZCU1N4UlFVRlJMRXRCUVVzc1dVRkJUQ3hEUVVGclFpeHZRa0ZCYkVJN1FVRkdUaXhKUVVGUU8wRkJTVUU3TzBGQlJVUXNWMEZCVXl4aFFVRlVMRWRCUVhsQ08wRkJRM2hDTEU5QlFVa3NZVUZCU2pzN1FVRkZRVHRCUVVOQk96dEJRVVZCTzBGQlEwRXNWVUZCVHl4VFFVRlRMRTFCUVdoQ0xFVkJRWGRDTzBGQlEzWkNMRmRCUVU4c1UwRkJVeXhMUVVGVUxFVkJRVkE3UVVGRFFTeFJRVUZKTEV0QlFVc3NRMEZCVEN4TlFVRlpMRWxCUVdoQ0xFVkJRWE5DTzBGQlEzSkNMRmxCUVU4c1VVRkJVU3hWUVVGVkxGVkJRVllzUlVGQmMwSXNNa05CUTNCRExFdEJRVXNzUzBGQlN5eE5RVUZNTEVkQlFXTXNRMEZCYmtJc1EwRkVZeXhEUVVGU0xFTkJRVkE3UVVGRlFTeExRVWhFTEUxQlIwODdRVUZEVGp0QlFVTkJPMEZCUTBFc2JVSkJRV01zU1VGQlpEdEJRVU5CTzBGQlEwUTdRVUZEUkN4WFFVRlJMRmRCUVZJc1IwRkJjMElzUlVGQmRFSTdRVUZEUVRzN1FVRkZSQ3haUVVGVk8wRkJRMVFzVjBGQlVTeFBRVVJETzBGQlJWUXNaMEpCUVdFc1YwRkdTanRCUVVkVUxHRkJRVlVzVVVGSVJEdEJRVWxVTEZsQlFWTXNVVUZLUVR0QlFVdFVMR1ZCUVZrc1ZVRk1TRHRCUVUxVUxHRkJRVlVzVVVGT1JEdEJRVTlVTEdkQ1FVRmhMRVZCVUVvN1FVRlJWQ3hYUVVGUkxFMUJVa003UVVGVFZDeHJRa0ZCWlN4aFFWUk9PMEZCVlZRc1lVRkJWU3hKUVVGSkxGRkJWa3c3UVVGWFZDeFpRVUZUTEU5QldFRTdPMEZCWVZRN096czdPMEZCUzBFc1kwRkJWeXh0UWtGQlV5eEhRVUZVTEVWQlFXTTdRVUZEZUVJN1FVRkRRU3hSUVVGSkxFbEJRVWtzVDBGQlVpeEZRVUZwUWp0QlFVTm9RaXhUUVVGSkxFbEJRVWtzVDBGQlNpeERRVUZaTEUxQlFWb3NRMEZCYlVJc1NVRkJTU3hQUVVGS0xFTkJRVmtzVFVGQldpeEhRVUZ4UWl4RFFVRjRReXhOUVVFclF5eEhRVUZ1UkN4RlFVRjNSRHRCUVVOMlJDeFZRVUZKTEU5QlFVb3NTVUZCWlN4SFFVRm1PMEZCUTBFN1FVRkRSRHM3UVVGRlJEdEJRVU5CTEZGQlFVa3NUMEZCVHl4UlFVRlBMRWxCUVd4Q08wRkJRVUVzVVVGRFF5eFBRVUZQTzBGQlEwNHNXVUZCVHl4SlFVUkVPMEZCUlU0c1kwRkJVeXhKUVVaSU8wRkJSMDRzWVVGQlVTeEpRVWhHTzBGQlNVNHNWVUZCU3p0QlFVcERMRXRCUkZJN08wRkJVVUVzWVVGQlV5eEhRVUZVTEVWQlFXTXNWVUZCVXl4TFFVRlVMRVZCUVdkQ0xFbEJRV2hDTEVWQlFYTkNPMEZCUTI1RExGTkJRVWtzUzBGQlN5eEpRVUZNTEVOQlFVb3NSVUZCWjBJN1FVRkRaaXhWUVVGSkxFTkJRVU1zVVVGQlR5eEpRVUZRTEVOQlFVd3NSVUZCYlVJN1FVRkRiRUlzWlVGQlR5eEpRVUZRTEVsQlFXVXNSVUZCWmp0QlFVTkJPMEZCUTBRc1dVRkJUU3hSUVVGUExFbEJRVkFzUTBGQlRpeEZRVUZ2UWl4TFFVRndRaXhGUVVFeVFpeEpRVUV6UWl4RlFVRnBReXhKUVVGcVF6dEJRVU5CTEUxQlRFUXNUVUZMVHp0QlFVTk9MR05CUVU4c1NVRkJVQ3hKUVVGbExFdEJRV1k3UVVGRFFUdEJRVU5FTEV0QlZFUTdPMEZCVjBFN1FVRkRRU3hSUVVGSkxFbEJRVWtzVDBGQlVpeEZRVUZwUWp0QlFVTm9RaXhqUVVGVExFbEJRVWtzVDBGQllpeEZRVUZ6UWl4VlFVRlRMRXRCUVZRc1JVRkJaMElzU1VGQmFFSXNSVUZCYzBJN1FVRkRNME1zVjBGQlN5eExRVUZNTEVWQlFWa3NWVUZCVXl4RFFVRlVMRVZCUVZrN1FVRkRka0lzVjBGQlNTeE5RVUZOTEVsQlFWWXNSVUZCWjBJN1FVRkRaaXh0UWtGQlZ5eERRVUZZTEVsQlFXZENMRWxCUVdoQ08wRkJRMEU3UVVGRFJDeFBRVXBFTzBGQlMwRXNUVUZPUkR0QlFVOUJPenRCUVVWRU8wRkJRMEVzVVVGQlNTeEpRVUZKTEVsQlFWSXNSVUZCWXp0QlFVTmlMR05CUVZNc1NVRkJTU3hKUVVGaUxFVkJRVzFDTEZWQlFWTXNTMEZCVkN4RlFVRm5RaXhGUVVGb1FpeEZRVUZ2UWp0QlFVTjBRenRCUVVOQkxGVkJRVWtzVVVGQlVTeExRVUZTTEVOQlFVb3NSVUZCYjBJN1FVRkRia0lzWlVGQlVUdEJRVU5RTEdOQlFVMDdRVUZFUXl4UlFVRlNPMEZCUjBFN1FVRkRSQ3hWUVVGSkxFTkJRVU1zVFVGQlRTeFBRVUZPTEVsQlFXbENMRTFCUVUwc1NVRkJlRUlzUzBGQmFVTXNRMEZCUXl4TlFVRk5MRk5CUVRWRExFVkJRWFZFTzBGQlEzUkVMR0ZCUVUwc1UwRkJUaXhIUVVGclFpeFJRVUZSTEdWQlFWSXNRMEZCZDBJc1MwRkJlRUlzUTBGQmJFSTdRVUZEUVR0QlFVTkVMRmRCUVVzc1JVRkJUQ3hKUVVGWExFdEJRVmc3UVVGRFFTeE5RVmhFTzBGQldVRXNZVUZCVHl4SlFVRlFMRWRCUVdNc1NVRkJaRHRCUVVOQk96dEJRVVZFTzBGQlEwRXNVVUZCU1N4SlFVRkpMRkZCUVZJc1JVRkJhMEk3UVVGRGFrSXNWVUZCU3l4SlFVRkpMRkZCUVZRc1JVRkJiVUlzVlVGQlV5eE5RVUZVTEVWQlFXbENPMEZCUTI1RExGVkJRVWtzYVVKQlFVbzdRVUZCUVN4VlFVRmpMR0ZCUVdRN08wRkJSVUVzWlVGQlV5eFBRVUZQTEUxQlFWQXNTMEZCYTBJc1VVRkJiRUlzUjBGQk5rSXNSVUZCUXl4TlFVRk5MRTFCUVZBc1JVRkJOMElzUjBGQk9FTXNUVUZCZGtRN08wRkJSVUVzWVVGQlR5eFBRVUZQTEVsQlFXUTdRVUZEUVN4cFFrRkJWeXhQUVVGUExGRkJRV3hDTzBGQlEwRXNWVUZCU1N4UlFVRktMRVZCUVdNN1FVRkRZaXhsUVVGUExFdEJRVkFzUTBGQllTeEpRVUZpTEVsQlFYRkNMRTlCUVU4c1VVRkJOVUk3UVVGRFFUczdRVUZGUkR0QlFVTkJPMEZCUTBFN1FVRkRRU3hqUVVGUExFbEJRVkFzUTBGQldTeEpRVUZhTEVsQlFXOUNMRTlCUVU4c1NVRkJVQ3hIUVVGakxFZEJRV1FzUjBGQmIwSXNRMEZCUXl4UFFVRlBMRWxCUVZBc1NVRkJaU3hOUVVGb1FpeEZRVU55UXl4UFFVUnhReXhEUVVNM1FpeGhRVVEyUWl4RlFVTmtMRVZCUkdNc1JVRkZja01zVDBGR2NVTXNRMEZGTjBJc1kwRkdOa0lzUlVGRllpeEZRVVpoTEVOQlFYaERPMEZCUjBFc1RVRnFRa1E3UVVGclFrRTdPMEZCUlVRN1FVRkRRVHRCUVVOQkxHRkJRVk1zVVVGQlZDeEZRVUZ0UWl4VlFVRlRMRWRCUVZRc1JVRkJZeXhGUVVGa0xFVkJRV3RDTzBGQlEzQkRPMEZCUTBFN1FVRkRRU3hUUVVGSkxFTkJRVU1zU1VGQlNTeE5RVUZNTEVsQlFXVXNRMEZCUXl4SlFVRkpMRWRCUVVvc1EwRkJVU3haUVVFMVFpeEZRVUV3UXp0QlFVTjZReXhWUVVGSkxFZEJRVW9zUjBGQlZTeGpRVUZqTEVWQlFXUXNSVUZCYTBJc1NVRkJiRUlzUlVGQmQwSXNTVUZCZUVJc1EwRkJWanRCUVVOQk8wRkJRMFFzUzBGT1JEczdRVUZSUVR0QlFVTkJPMEZCUTBFc1VVRkJTU3hKUVVGSkxFbEJRVW9zU1VGQldTeEpRVUZKTEZGQlFYQkNMRVZCUVRoQ08wRkJRemRDTEdGQlFWRXNUMEZCVWl4RFFVRm5RaXhKUVVGSkxFbEJRVW9zU1VGQldTeEZRVUUxUWl4RlFVRm5ReXhKUVVGSkxGRkJRWEJETzBGQlEwRTdRVUZEUkN4SlFTOUhVVHM3UVVGcFNGUXNiMEpCUVdsQ0xIbENRVUZUTEV0QlFWUXNSVUZCWjBJN1FVRkRhRU1zWVVGQlV5eEZRVUZVTEVkQlFXTTdRVUZEWWl4VFFVRkpMRmxCUVVvN1FVRkRRU3hUUVVGSkxFMUJRVTBzU1VGQlZpeEZRVUZuUWp0QlFVTm1MRmxCUVUwc1RVRkJUU3hKUVVGT0xFTkJRVmNzUzBGQldDeERRVUZwUWl4TlFVRnFRaXhGUVVGNVFpeFRRVUY2UWl4RFFVRk9PMEZCUTBFN1FVRkRSQ3haUVVGUExFOUJRVkVzVFVGQlRTeFBRVUZPTEVsQlFXbENMRlZCUVZVc1RVRkJUU3hQUVVGb1FpeERRVUZvUXp0QlFVTkJPenRCUVVWRUxGZEJRVThzUlVGQlVEdEJRVU5CTEVsQk0waFJPenRCUVRaSVZDeG5Ra0ZCWVN4eFFrRkJVeXhOUVVGVUxFVkJRV2xDTEU5QlFXcENMRVZCUVRCQ08wRkJRM1JETEdOQlFWVXNWMEZCVnl4RlFVRnlRanM3UVVGRlFTeGhRVUZUTEZsQlFWUXNRMEZCYzBJc1NVRkJkRUlzUlVGQk5FSXNVVUZCTlVJc1JVRkJjME1zVDBGQmRFTXNSVUZCSzBNN1FVRkRPVU1zVTBGQlNTeFhRVUZLTzBGQlFVRXNVMEZCVVN4WlFVRlNPMEZCUVVFc1UwRkJZU3h0UWtGQllqczdRVUZGUVN4VFFVRkpMRkZCUVZFc2JVSkJRVklzU1VGQkswSXNVVUZCTDBJc1NVRkJNa01zVjBGQlZ5eFJRVUZZTEVOQlFTOURMRVZCUVhGRk8wRkJRM0JGTEdWQlFWTXNaMEpCUVZRc1IwRkJORUlzU1VGQk5VSTdRVUZEUVRzN1FVRkZSQ3hUUVVGSkxFOUJRVThzU1VGQlVDeExRVUZuUWl4UlFVRndRaXhGUVVFNFFqdEJRVU0zUWl4VlFVRkpMRmRCUVZjc1VVRkJXQ3hEUVVGS0xFVkJRVEJDTzBGQlEzcENPMEZCUTBFc1kwRkJUeXhSUVVGUkxGVkJRVlVzWVVGQlZpeEZRVUY1UWl4elFrRkJla0lzUTBGQlVpeEZRVUV3UkN4UFFVRXhSQ3hEUVVGUU8wRkJRMEU3TzBGQlJVUTdRVUZEUVR0QlFVTkJMRlZCUVVrc1ZVRkJWU3hSUVVGUkxGRkJRVklzUlVGQmEwSXNTVUZCYkVJc1EwRkJaQ3hGUVVGMVF6dEJRVU4wUXl4alFVRlBMRk5CUVZNc1NVRkJWQ3hGUVVGbExGTkJRVk1zVDBGQlR5eEZRVUZvUWl4RFFVRm1MRU5CUVZBN1FVRkRRVHM3UVVGRlJEdEJRVU5CTzBGQlEwRXNWVUZCU1N4SlFVRkpMRWRCUVZJc1JVRkJZVHRCUVVOYUxHTkJRVThzU1VGQlNTeEhRVUZLTEVOQlFWRXNUMEZCVWl4RlFVRnBRaXhKUVVGcVFpeEZRVUYxUWl4TlFVRjJRaXhGUVVFclFpeFpRVUV2UWl4RFFVRlFPMEZCUTBFN08wRkJSVVE3UVVGRFFTeFpRVUZOTEdOQlFXTXNTVUZCWkN4RlFVRnZRaXhOUVVGd1FpeEZRVUUwUWl4TFFVRTFRaXhGUVVGdFF5eEpRVUZ1UXl4RFFVRk9PMEZCUTBFc1YwRkJTeXhKUVVGSkxFVkJRVlE3TzBGQlJVRXNWVUZCU1N4RFFVRkRMRkZCUVZFc1VVRkJVaXhGUVVGcFFpeEZRVUZxUWl4RFFVRk1MRVZCUVRKQ08wRkJRekZDTEdOQlFVOHNVVUZCVVN4VlFVRlZMRmRCUVZZc1JVRkJkVUlzYTBKQlEzSkRMRVZCUkhGRExFZEJSWEpETEhsRFFVWnhReXhIUVVkeVF5eFhRVWh4UXl4SlFVbHdReXhUUVVGVExFVkJRVlFzUjBGQll5eHRRa0ZLYzBJc1EwRkJka0lzUTBGQlVpeERRVUZRTzBGQlMwRTdRVUZEUkN4aFFVRlBMRk5CUVZFc1JVRkJVaXhEUVVGUU8wRkJRMEU3TzBGQlJVUTdRVUZEUVRzN1FVRkZRVHRCUVVOQkxHRkJRVkVzVVVGQlVpeERRVUZwUWl4WlFVRlhPMEZCUXpOQ08wRkJRMEU3UVVGRFFUczdRVUZGUVN4dFFrRkJZU3hWUVVGVkxHTkJRV01zU1VGQlpDeEZRVUZ2UWl4TlFVRndRaXhEUVVGV0xFTkJRV0k3TzBGQlJVRTdRVUZEUVR0QlFVTkJMR2xDUVVGWExFOUJRVmdzUjBGQmNVSXNVVUZCVVN4UFFVRTNRanM3UVVGRlFTeHBRa0ZCVnl4SlFVRllMRU5CUVdkQ0xFbEJRV2hDTEVWQlFYTkNMRkZCUVhSQ0xFVkJRV2RETEU5QlFXaERMRVZCUVhsRE8wRkJRM2hETEdkQ1FVRlRPMEZCUkN0Q0xFOUJRWHBET3p0QlFVbEJPMEZCUTBFc1RVRm9Ra1E3TzBGQmEwSkJMRmxCUVU4c1dVRkJVRHRCUVVOQk96dEJRVVZFTEZWQlFVMHNXVUZCVGl4RlFVRnZRanRCUVVOdVFpeG5Ra0ZCVnl4VFFVUlJPenRCUVVkdVFqczdPenM3UVVGTFFTeFpRVUZQTEdWQlFWTXNhVUpCUVZRc1JVRkJORUk3UVVGRGJFTXNWVUZCU1N4WlFVRktPMEZCUVVFc1ZVRkRReXhSUVVGUkxHdENRVUZyUWl4WFFVRnNRaXhEUVVFNFFpeEhRVUU1UWl4RFFVUlVPMEZCUVVFc1ZVRkZReXhWUVVGVkxHdENRVUZyUWl4TFFVRnNRaXhEUVVGM1FpeEhRVUY0UWl4RlFVRTJRaXhEUVVFM1FpeERRVVpZTzBGQlFVRXNWVUZIUXl4aFFVRmhMRmxCUVZrc1IwRkJXaXhKUVVGdFFpeFpRVUZaTEVsQlNEZERPenRCUVV0Qk8wRkJRMEU3UVVGRFFTeFZRVUZKTEZWQlFWVXNRMEZCUXl4RFFVRllMRXRCUVdsQ0xFTkJRVU1zVlVGQlJDeEpRVUZsTEZGQlFWRXNRMEZCZUVNc1EwRkJTaXhGUVVGblJEdEJRVU12UXl4aFFVRk5MR3RDUVVGclFpeFRRVUZzUWl4RFFVRTBRaXhMUVVFMVFpeEZRVUZ0UXl4clFrRkJhMElzVFVGQmNrUXNRMEZCVGp0QlFVTkJMREpDUVVGdlFpeHJRa0ZCYTBJc1UwRkJiRUlzUTBGQk5FSXNRMEZCTlVJc1JVRkJLMElzUzBGQkwwSXNRMEZCY0VJN1FVRkRRVHM3UVVGRlJDeGhRVUZQTEZGQlFWRXNVMEZCVWl4RFFVRnJRaXhWUVVGVkxHbENRVUZXTEVWQlEzaENMRlZCUVZVc1QwRkJUeXhGUVVSUExFVkJRMGdzU1VGRVJ5eERRVUZzUWl4RlFVTnpRaXhIUVVSMFFpeEZRVU15UWl4SlFVUXpRaXhEUVVGUU8wRkJSVUVzVFVGMlFtdENPenRCUVhsQ2JrSXNZMEZCVXl4cFFrRkJVeXhGUVVGVUxFVkJRV0U3UVVGRGNrSXNZVUZCVHl4UlFVRlJMRkZCUVZJc1JVRkJhVUlzWTBGQll5eEZRVUZrTEVWQlFXdENMRTFCUVd4Q0xFVkJRVEJDTEV0QlFURkNMRVZCUVdsRExFbEJRV3BETEVWQlFYVkRMRVZCUVhoRUxFTkJRVkE3UVVGRFFTeE5RVE5DYTBJN08wRkJOa0p1UWl4blFrRkJWeXh0UWtGQlV5eEZRVUZVTEVWQlFXRTdRVUZEZGtJc1YwRkJTeXhqUVVGakxFVkJRV1FzUlVGQmEwSXNUVUZCYkVJc1JVRkJNRUlzUzBGQk1VSXNSVUZCYVVNc1NVRkJha01zUlVGQmRVTXNSVUZCTlVNN1FVRkRRU3hoUVVGUExGRkJRVkVzVVVGQlVpeEZRVUZwUWl4RlFVRnFRaXhMUVVGM1FpeFJRVUZSTEZGQlFWSXNSVUZCYTBJc1JVRkJiRUlzUTBGQkwwSTdRVUZEUVR0QlFXaERhMElzUzBGQmNFSTdPMEZCYlVOQk8wRkJRMEVzVVVGQlNTeERRVUZETEUxQlFVd3NSVUZCWVR0QlFVTmFMR3RDUVVGaExFdEJRV0lzUjBGQmNVSXNWVUZCVXl4RlFVRlVMRVZCUVdFN1FVRkRha003UVVGRFFUczdRVUZGUVN4VlFVRkpMRTFCUVUwc1kwRkJZeXhGUVVGa0xFVkJRV3RDTEUxQlFXeENMRVZCUVRCQ0xFbEJRVEZDTEVOQlFWWTdRVUZEUVN4VlFVRkpMRTFCUVUwc1QwRkJUeXhSUVVGUUxFVkJRV2xDTEVWQlFXcENMRU5CUVZZN08wRkJSVUVzVlVGQlNTeFBRVUZLTEVkQlFXTXNTVUZCWkR0QlFVTkJMRzFDUVVGaExFVkJRV0k3TzBGQlJVRXNZVUZCVHl4VFFVRlJMRVZCUVZJc1EwRkJVRHRCUVVOQkxHRkJRVThzVjBGQlZ5eEpRVUZKTEVkQlFXWXNRMEZCVUR0QlFVTkJMR0ZCUVU4c1dVRkJXU3hGUVVGYUxFTkJRVkE3TzBGQlJVRTdRVUZEUVR0QlFVTkJMR3RDUVVGWkxGRkJRVm9zUlVGQmMwSXNWVUZCVXl4SlFVRlVMRVZCUVdVc1EwRkJaaXhGUVVGclFqdEJRVU4yUXl4WFFVRkpMRXRCUVVzc1EwRkJUQ3hOUVVGWkxFVkJRV2hDTEVWQlFXOUNPMEZCUTI1Q0xHbENRVUZUTEUxQlFWUXNRMEZCWjBJc1EwRkJhRUlzUlVGQmJVSXNRMEZCYmtJN1FVRkRRVHRCUVVORUxFOUJTa1E3UVVGTFFTeGhRVUZQTEZGQlFWRXNWMEZCVWl4RFFVRnZRaXhGUVVGd1FpeERRVUZRT3p0QlFVVkJMRlZCUVVrc1IwRkJTaXhGUVVGVE8wRkJRMUk3UVVGRFFUdEJRVU5CTEZkQlFVa3NTVUZCU1N4TlFVRktMRU5CUVZjc1QwRkJaaXhGUVVGM1FqdEJRVU4yUWl4dlFrRkJXU3hGUVVGYUxFbEJRV3RDTEVsQlFVa3NUVUZCZEVJN1FVRkRRVHM3UVVGRlJDeHhRa0ZCWXl4RlFVRmtPMEZCUTBFN1FVRkRSQ3hOUVdoRFJEdEJRV2xEUVRzN1FVRkZSQ3hYUVVGUExGbEJRVkE3UVVGRFFTeEpRWHBSVVRzN1FVRXlVVlE3T3pzN08wRkJTMEVzVjBGQlVTeG5Ra0ZCVXl4TlFVRlVMRVZCUVdsQ08wRkJRM2hDTEZGQlFVa3NUVUZCVFN4UFFVRlBMRkZCUVZBc1JVRkJhVUlzVDBGQlR5eEZRVUY0UWl4RFFVRldPMEZCUTBFc1VVRkJTU3hIUVVGS0xFVkJRVk03UVVGRFVpeGxRVUZWTEUxQlFWWXNSVUZCYTBJc1RVRkJiRUk3UVVGRFFUdEJRVU5FTEVsQmNsSlJPenRCUVhWU1ZEczdPenM3T3p0QlFVOUJMR2xDUVVGakxITkNRVUZUTEZWQlFWUXNSVUZCY1VJN1FVRkRiRU1zVVVGQlNTeGpRVUZLTzBGQlEwRXNVVUZCU1N4aFFVRktPMEZCUTBFc1VVRkJTU3haUVVGS08wRkJRMEVzVVVGQlNTeFBRVUZQTEU5QlFVOHNVVUZCVHl4SlFVRmtMRVZCUVc5Q0xGVkJRWEJDTEV0QlFXMURMRVZCUVRsRE8wRkJRMEVzVVVGQlNTeFpRVUZaTEV0QlFVc3NUMEZCY2tJN08wRkJSVUU3TzBGQlJVRXNWMEZCVHl4VFFVRlRMRTFCUVdoQ0xFVkJRWGRDTzBGQlEzWkNMRmxCUVU4c1UwRkJVeXhMUVVGVUxFVkJRVkE3UVVGRFFTeFRRVUZKTEV0QlFVc3NRMEZCVEN4TlFVRlpMRWxCUVdoQ0xFVkJRWE5DTzBGQlEzSkNMRmRCUVVzc1EwRkJUQ3hKUVVGVkxGVkJRVlk3UVVGRFFUdEJRVU5CTzBGQlEwRXNWVUZCU1N4TFFVRktMRVZCUVZjN1FVRkRWanRCUVVOQk8wRkJRMFFzWTBGQlVTeEpRVUZTTzBGQlEwRXNUVUZTUkN4TlFWRlBMRWxCUVVrc1MwRkJTeXhEUVVGTUxFMUJRVmtzVlVGQmFFSXNSVUZCTkVJN1FVRkRiRU03UVVGRFFTeGpRVUZSTEVsQlFWSTdRVUZEUVRzN1FVRkZSQ3h0UWtGQll5eEpRVUZrTzBGQlEwRTdRVUZEUkN4WlFVRlJMRmRCUVZJc1IwRkJjMElzUlVGQmRFSTdPMEZCUlVFN1FVRkRRVHRCUVVOQkxGVkJRVTBzVDBGQlR5eFJRVUZRTEVWQlFXbENMRlZCUVdwQ0xFTkJRVTQ3TzBGQlJVRXNVVUZCU1N4RFFVRkRMRXRCUVVRc1NVRkJWU3hEUVVGRExGRkJRVkVzVVVGQlVpeEZRVUZwUWl4VlFVRnFRaXhEUVVGWUxFbEJRVEpETEVkQlFUTkRMRWxCUVd0RUxFTkJRVU1zU1VGQlNTeE5RVUV6UkN4RlFVRnRSVHRCUVVOc1JTeFRRVUZKTEZGQlFVOHNZVUZCVUN4TFFVRjVRaXhEUVVGRExGTkJRVVFzU1VGQll5eERRVUZETEZWQlFWVXNVMEZCVml4RFFVRjRReXhEUVVGS0xFVkJRVzFGTzBGQlEyeEZMRlZCUVVrc1owSkJRV2RDTEZWQlFXaENMRU5CUVVvc1JVRkJhVU03UVVGRGFFTTdRVUZEUVN4UFFVWkVMRTFCUlU4N1FVRkRUaXhqUVVGUExGRkJRVkVzVlVGQlZTeFZRVUZXTEVWQlEyUXNkMEpCUVhkQ0xGVkJSRllzUlVGRlpDeEpRVVpqTEVWQlIyUXNRMEZCUXl4VlFVRkVMRU5CU0dNc1EwRkJVaXhEUVVGUU8wRkJTVUU3UVVGRFJDeE5RVlJFTEUxQlUwODdRVUZEVGp0QlFVTkJMRzlDUVVGakxFTkJRVU1zVlVGQlJDeEZRVUZqTEV0QlFVc3NTVUZCVEN4SlFVRmhMRVZCUVROQ0xFVkJRV2RETEV0QlFVc3NVMEZCY2tNc1EwRkJaRHRCUVVOQk8wRkJRMFE3TzBGQlJVUTdRVUZEUVN4SlFTOVZVVHM3UVVGcFZsUTdPenM3T3pzN08wRkJVVUVzWTBGQlZ5eHRRa0ZCVXl4VlFVRlVMRVZCUVhGQ0xFZEJRWEpDTEVWQlFUQkNMRTlCUVRGQ0xFVkJRVzFETzBGQlF6ZERMRkZCUVVrc1kwRkJTanRCUVVOQkxGRkJRVWtzWVVGQlNqdEJRVU5CTEZGQlFVa3NWVUZCU2p0QlFVTkJMRkZCUVVrc2NVSkJRVW83UVVGRFFTeFJRVUZKTEZsQlFVbzdRVUZEUVN4UlFVRkpMRzFDUVVGS08wRkJRVUVzVVVGQlowSXNhVUpCUVdoQ08wRkJRMEVzVVVGQlNTeFZRVUZWTEU5QlFVOHNVVUZCVHl4SlFVRmtMRVZCUVc5Q0xGVkJRWEJDTEVOQlFXUTdPMEZCUlVFc1VVRkJTU3hQUVVGS0xFVkJRV0U3UVVGRFdpeHJRa0ZCWVN4UFFVRmlPMEZCUTBFN08wRkJSVVFzWlVGQlZ5eFBRVUZQTEZWQlFWQXNSVUZCYlVJc1ZVRkJia0lzUTBGQldEczdRVUZGUVN4UlFVRkpMRkZCUVVvc1JVRkJZenRCUVVOaUxGbEJRVThzVVVGQlVTeFRRVUZTTEVOQlFXdENMRkZCUVd4Q0xFVkJRVFJDTEVkQlFUVkNMRVZCUVdsRExFOUJRV3BETEVOQlFWQTdRVUZEUVRzN1FVRkZSRHRCUVVOQk8wRkJRMEU3UVVGRFFTeFJRVUZKTEVsQlFVa3NWMEZCU2l4RFFVRm5RaXhKUVVGb1FpeERRVUZ4UWl4VlFVRnlRaXhEUVVGS0xFVkJRWE5ETzBGQlEzSkRPMEZCUTBFN1FVRkRRVHRCUVVOQkxGZEJRVTBzWTBGQll5eFBRVUZQTEVWQlFYSkNMRU5CUVU0N1FVRkRRU3hMUVV4RUxFMUJTMDg3UVVGRFRqdEJRVU5CTEdGQlFWRXNVVUZCVHl4TFFVRm1PenRCUVVWQkxGbEJRVThzVjBGQlZ5eExRVUZZTEVOQlFXbENMRWRCUVdwQ0xFTkJRVkE3UVVGRFFUdEJRVU5CTzBGQlEwRXNWVUZCU3l4SlFVRkpMRXRCUVVzc1RVRkJaQ3hGUVVGelFpeEpRVUZKTEVOQlFURkNMRVZCUVRaQ0xFdEJRVXNzUTBGQmJFTXNSVUZCY1VNN1FVRkRjRU1zY1VKQlFXVXNTMEZCU3l4TFFVRk1MRU5CUVZjc1EwRkJXQ3hGUVVGakxFTkJRV1FzUlVGQmFVSXNTVUZCYWtJc1EwRkJjMElzUjBGQmRFSXNRMEZCWmpzN1FVRkZRU3h0UWtGQllTeFBRVUZQTEV0QlFWQXNSVUZCWXl4WlFVRmtMRU5CUVdJN1FVRkRRU3hWUVVGSkxGVkJRVW9zUlVGQlowSTdRVUZEWmp0QlFVTkJMRmRCUVVrc1VVRkJVU3hWUVVGU0xFTkJRVW9zUlVGQmVVSTdRVUZEZUVJc2NVSkJRV0VzVjBGQlZ5eERRVUZZTEVOQlFXSTdRVUZEUVR0QlFVTkVMRmxCUVVzc1RVRkJUQ3hEUVVGWkxFTkJRVm9zUlVGQlpTeERRVUZtTEVWQlFXdENMRlZCUVd4Q08wRkJRMEU3UVVGRFFUdEJRVU5FT3p0QlFVVkVPMEZCUTBFc1YwRkJUU3hMUVVGTExFbEJRVXdzUTBGQlZTeEhRVUZXTEVOQlFVNDdRVUZEUVN4WlFVRlJMRkZCUVZFc1lVRkJZU3hKUVVGaUxFTkJRV3RDTEVkQlFXeENMRXRCUVRCQ0xFOUJRVEZDTEVkQlFXOURMRVZCUVhCRExFZEJRWGxETEV0QlFXcEVMRU5CUVZJN1FVRkRRU3hYUVVGTkxFTkJRVU1zU1VGQlNTeE5RVUZLTEVOQlFWY3NRMEZCV0N4TlFVRnJRaXhIUVVGc1FpeEpRVUY1UWl4SlFVRkpMRXRCUVVvc1EwRkJWU3hsUVVGV0xFTkJRWHBDTEVkQlFYTkVMRVZCUVhSRUxFZEJRVEpFTEZGQlFVOHNUMEZCYmtVc1NVRkJPRVVzUjBGQmNFWTdRVUZEUVRzN1FVRkZSQ3hYUVVGUExGRkJRVThzVDBGQlVDeEhRVUZwUWl4UFFVTkVMRU5CUVVNc1NVRkJTU3hQUVVGS0xFTkJRVmtzUjBGQldpeE5RVUZ4UWl4RFFVRkRMRU5CUVhSQ0xFZEJRVEJDTEVkQlFURkNMRWRCUVdkRExFZEJRV3BETEVsQlEwUXNVVUZCVHl4UFFVWk1MRU5CUVdwQ0xFZEJSV2xETEVkQlJuaERPMEZCUjBFc1NVRnNXbEU3TzBGQmIxcFVPMEZCUTBFc1UwRkJUU3hqUVVGVExFVkJRVlFzUlVGQllTeEhRVUZpTEVWQlFXdENPMEZCUTNaQ0xGRkJRVWtzU1VGQlNpeERRVUZUTEU5QlFWUXNSVUZCYTBJc1JVRkJiRUlzUlVGQmMwSXNSMEZCZEVJN1FVRkRRU3hKUVhaYVVUczdRVUY1V2xRN096czdPenM3TzBGQlVVRXNWMEZCVVN4blFrRkJVeXhKUVVGVUxFVkJRV1VzVVVGQlppeEZRVUY1UWl4SlFVRjZRaXhGUVVFclFpeFBRVUV2UWl4RlFVRjNRenRCUVVNdlF5eFhRVUZQTEZOQlFWTXNTMEZCVkN4RFFVRmxMRTlCUVdZc1JVRkJkMElzU1VGQmVFSXNRMEZCVUR0QlFVTkJMRWxCYm1GUk96dEJRWEZoVkRzN096czdRVUZMUVN4cFFrRkJZeXh6UWtGQlV5eEhRVUZVTEVWQlFXTTdRVUZETTBJN1FVRkRRVHRCUVVOQk8wRkJRMEVzVVVGQlNTeEpRVUZKTEVsQlFVb3NTMEZCWVN4TlFVRmlMRWxCUTBZc1dVRkJXU3hKUVVGYUxFTkJRV2xDTEVOQlFVTXNTVUZCU1N4aFFVRktMRWxCUVhGQ0xFbEJRVWtzVlVGQk1VSXNSVUZCYzBNc1ZVRkJka1FzUTBGRVJpeEZRVU4xUlR0QlFVTjBSVHRCUVVOQk8wRkJRMEVzZVVKQlFXOUNMRWxCUVhCQ096dEJRVVZCTzBGQlEwRXNVMEZCU1N4UFFVRlBMR05CUVdNc1IwRkJaQ3hEUVVGWU8wRkJRMEVzWVVGQlVTeFpRVUZTTEVOQlFYRkNMRXRCUVVzc1JVRkJNVUk3UVVGRFFUdEJRVU5FTEVsQmVHSlJPenRCUVRCaVZEczdPMEZCUjBFc2EwSkJRV1VzZFVKQlFWTXNSMEZCVkN4RlFVRmpPMEZCUXpWQ0xGRkJRVWtzVDBGQlR5eGpRVUZqTEVkQlFXUXNRMEZCV0R0QlFVTkJMRkZCUVVrc1EwRkJReXhuUWtGQlowSXNTMEZCU3l4RlFVRnlRaXhEUVVGTUxFVkJRU3RDTzBGQlF6bENMRk5CUVVrc1ZVRkJWU3hGUVVGa08wRkJRMEVzWTBGQlV5eFJRVUZVTEVWQlFXMUNMRlZCUVZNc1MwRkJWQ3hGUVVGblFpeEhRVUZvUWl4RlFVRnhRanRCUVVOMlF5eFZRVUZKTEVsQlFVa3NUMEZCU2l4RFFVRlpMRXRCUVZvc1RVRkJkVUlzUTBGQk0wSXNSVUZCT0VJN1FVRkROMElzV1VGQlN5eE5RVUZOTEU5QlFWZ3NSVUZCYjBJc1ZVRkJVeXhOUVVGVUxFVkJRV2xDTzBGQlEzQkRMRmxCUVVrc1QwRkJUeXhGUVVGUUxFdEJRV01zUzBGQlN5eEZRVUYyUWl4RlFVRXlRanRCUVVNeFFpeHBRa0ZCVVN4SlFVRlNMRU5CUVdFc1IwRkJZanRCUVVOQk8wRkJRMFFzWlVGQlR5eEpRVUZRTzBGQlEwRXNVVUZNUkR0QlFVMUJPMEZCUTBRc1RVRlVSRHRCUVZWQkxGbEJRVThzVVVGQlVTeFZRVUZWTEdGQlFWWXNSVUZCZVVJc2RVSkJRWFZDTEV0QlFVc3NSVUZCTlVJc1NVRkRkRU1zVVVGQlVTeE5RVUZTTEVkQlEwRXNiVUpCUVcxQ0xGRkJRVkVzU1VGQlVpeERRVUZoTEVsQlFXSXNRMEZFYmtJc1IwRkZRU3hIUVVoelF5eERRVUY2UWl4RlFVZFFMRWRCU0U4c1JVRkhSaXhEUVVGRExFdEJRVXNzUlVGQlRpeERRVWhGTEVOQlFWSXNRMEZCVUR0QlFVbEJPMEZCUTBRN1FVRm9aRkVzUjBGQlZqczdRVUZ0WkVFc1ZVRkJVU3hQUVVGU0xFZEJRV3RDTEZGQlFWRXNWMEZCVWl4RlFVRnNRanRCUVVOQkxGTkJRVThzVDBGQlVEdEJRVU5CT3p0QlFVVkVPenM3T3pzN096czdPenM3UVVGWlFTeFBRVUZOTEU5QlFVOHNVMEZCVUN4SFFVRnRRaXhWUVVGVExFbEJRVlFzUlVGQlpTeFJRVUZtTEVWQlFYbENMRTlCUVhwQ0xFVkJRV3RETEZGQlFXeERMRVZCUVRSRE8wRkJRM0JGTzBGQlEwRXNUVUZCU1N4blFrRkJTanRCUVVOQkxFMUJRVWtzWlVGQlNqdEJRVU5CTEUxQlFVa3NZMEZCWXl4alFVRnNRanM3UVVGRlFUdEJRVU5CTEUxQlFVa3NRMEZCUXl4UlFVRlJMRWxCUVZJc1EwRkJSQ3hKUVVGclFpeFBRVUZQTEVsQlFWQXNTMEZCWjBJc1VVRkJkRU1zUlVGQlowUTdRVUZETDBNN1FVRkRRU3haUVVGVExFbEJRVlE3UVVGRFFTeFBRVUZKTEZGQlFWRXNVVUZCVWl4RFFVRktMRVZCUVhWQ08wRkJRM1JDTzBGQlEwRXNWMEZCVHl4UlFVRlFPMEZCUTBFc1pVRkJWeXhQUVVGWU8wRkJRMEVzWTBGQlZTeFJRVUZXTzBGQlEwRXNTVUZNUkN4TlFVdFBPMEZCUTA0c1YwRkJUeXhGUVVGUU8wRkJRMEU3UVVGRFJEczdRVUZGUkN4TlFVRkpMRlZCUVZVc1QwRkJUeXhQUVVGeVFpeEZRVUU0UWp0QlFVTTNRaXhwUWtGQll5eFBRVUZQTEU5QlFYSkNPMEZCUTBFN08wRkJSVVFzV1VGQlZTeFBRVUZQTEZGQlFWQXNSVUZCYVVJc1YwRkJha0lzUTBGQlZqdEJRVU5CTEUxQlFVa3NRMEZCUXl4UFFVRk1MRVZCUVdNN1FVRkRZaXhoUVVGVkxGTkJRVk1zVjBGQlZDeEpRVUYzUWl4SlFVRkpMRU5CUVVvc1EwRkJUU3hWUVVGT0xFTkJRV2xDTEZkQlFXcENMRU5CUVd4RE8wRkJRMEU3TzBGQlJVUXNUVUZCU1N4TlFVRktMRVZCUVZrN1FVRkRXQ3hYUVVGUkxGTkJRVklzUTBGQmEwSXNUVUZCYkVJN1FVRkRRVHM3UVVGRlJDeFRRVUZQTEZGQlFWRXNUMEZCVWl4RFFVRm5RaXhKUVVGb1FpeEZRVUZ6UWl4UlFVRjBRaXhGUVVGblF5eFBRVUZvUXl4RFFVRlFPMEZCUTBFc1JVRnNRMFE3TzBGQmIwTkJPenM3UVVGSFFTeExRVUZKTEUxQlFVb3NSMEZCWVN4VlFVRlRMRTFCUVZRc1JVRkJhVUk3UVVGRE4wSXNVMEZCVHl4SlFVRkpMRTFCUVVvc1EwRkJVRHRCUVVOQkxFVkJSa1E3TzBGQlNVRTdPenM3T3pzN1FVRlBRU3hMUVVGSkxGRkJRVW9zUjBGQlpTeFBRVUZQTEZWQlFWQXNTMEZCYzBJc1YwRkJkRUlzUjBGQmIwTXNWVUZCVXl4RlFVRlVMRVZCUVdFN1FVRkRMMFFzWVVGQlZ5eEZRVUZZTEVWQlFXVXNRMEZCWmp0QlFVTkJMRVZCUm1Nc1IwRkZXQ3hWUVVGVExFVkJRVlFzUlVGQllUdEJRVU5vUWp0QlFVTkJMRVZCU2tRN08wRkJUVUU3T3p0QlFVZEJMRXRCUVVrc1EwRkJReXhQUVVGUExFOUJRVm9zUlVGQmNVSTdRVUZEY0VJc1UwRkJUeXhQUVVGUUxFZEJRV2xDTEVkQlFXcENPMEZCUTBFN08wRkJSVVFzUzBGQlNTeFBRVUZLTEVkQlFXTXNUMEZCWkRzN1FVRkZRVHRCUVVOQkxFdEJRVWtzVjBGQlNpeEhRVUZyUWl4blFrRkJiRUk3UVVGRFFTeExRVUZKTEZOQlFVb3NSMEZCWjBJc1UwRkJhRUk3UVVGRFFTeExRVUZKTEVsQlFVa3NRMEZCU2l4SFFVRlJPMEZCUTFnc1dVRkJWU3hSUVVSRE8wRkJSVmdzWTBGQldUdEJRVVpFTEVWQlFWbzdPMEZCUzBFN1FVRkRRU3hMUVVGSkxFVkJRVW83TzBGQlJVRTdRVUZEUVN4TlFVRkxMRU5CUTBvc1QwRkVTU3hGUVVWS0xFOUJSa2tzUlVGSFNpeFRRVWhKTEVWQlNVb3NWMEZLU1N4RFFVRk1MRVZCUzBjc1ZVRkJVeXhKUVVGVUxFVkJRV1U3UVVGRGFrSTdRVUZEUVR0QlFVTkJPMEZCUTBFc1RVRkJTU3hKUVVGS0xFbEJRVmtzV1VGQlZ6dEJRVU4wUWl4UFFVRkpMRTFCUVUwc1UwRkJVeXhqUVVGVUxFTkJRVlk3UVVGRFFTeFZRVUZQTEVsQlFVa3NUMEZCU2l4RFFVRlpMRWxCUVZvc1JVRkJhMElzUzBGQmJFSXNRMEZCZDBJc1IwRkJlRUlzUlVGQk5rSXNVMEZCTjBJc1EwRkJVRHRCUVVOQkxFZEJTRVE3UVVGSlFTeEZRV0pFT3p0QlFXVkJMRXRCUVVrc1UwRkJTaXhGUVVGbE8wRkJRMlFzVTBGQlR5eEZRVUZGTEVsQlFVWXNSMEZCVXl4VFFVRlRMRzlDUVVGVUxFTkJRVGhDTEUxQlFUbENMRVZCUVhORExFTkJRWFJETEVOQlFXaENPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEVzWjBKQlFXTXNVMEZCVXl4dlFrRkJWQ3hEUVVFNFFpeE5RVUU1UWl4RlFVRnpReXhEUVVGMFF5eERRVUZrTzBGQlEwRXNUVUZCU1N4WFFVRktMRVZCUVdsQ08wRkJRMmhDTEZWQlFVOHNSVUZCUlN4SlFVRkdMRWRCUVZNc1dVRkJXU3hWUVVFMVFqdEJRVU5CTzBGQlEwUTdPMEZCUlVRN096czdPenM3UVVGUFFTeExRVUZKTEU5QlFVb3NSMEZCWXl4alFVRmtPenRCUVVWQk96czdRVUZIUVN4TFFVRkpMRlZCUVVvc1IwRkJhVUlzVlVGQlV5eE5RVUZVTEVWQlFXbENMRlZCUVdwQ0xFVkJRVFpDTEVkQlFUZENMRVZCUVd0RE8wRkJRMnhFTEUxQlFVMHNUMEZCVHl4UFFVRlBMRXRCUVZBc1IwRkRSaXhUUVVGVExHVkJRVlFzUTBGQmVVSXNPRUpCUVhwQ0xFVkJRWGxFTEdGQlFYcEVMRU5CUkVVc1IwRkZSaXhUUVVGVExHRkJRVlFzUTBGQmRVSXNVVUZCZGtJc1EwRkdXRHRCUVVkQkxFOUJRVXNzU1VGQlRDeEhRVUZaTEU5QlFVOHNWVUZCVUN4SlFVRnhRaXhwUWtGQmFrTTdRVUZEUVN4UFFVRkxMRTlCUVV3c1IwRkJaU3hQUVVGbU8wRkJRMEVzVDBGQlN5eExRVUZNTEVkQlFXRXNTVUZCWWp0QlFVTkJMRk5CUVU4c1NVRkJVRHRCUVVOQkxFVkJVa1E3TzBGQlZVRTdPenM3T3pzN096dEJRVk5CTEV0QlFVa3NTVUZCU2l4SFFVRlhMRlZCUVZNc1QwRkJWQ3hGUVVGclFpeFZRVUZzUWl4RlFVRTRRaXhIUVVFNVFpeEZRVUZ0UXp0QlFVTTNReXhOUVVGSkxGTkJRVlVzVjBGQlZ5eFJRVUZSTEUxQlFYQkNMRWxCUVN0Q0xFVkJRVFZETzBGQlEwRXNUVUZCU1N4aFFVRktPenRCUVVWQkxFMUJRVWtzVTBGQlNpeEZRVUZsTzBGQlEyUTdRVUZEUVN4VlFVRlBMRWxCUVVrc1ZVRkJTaXhEUVVGbExFMUJRV1lzUlVGQmRVSXNWVUZCZGtJc1JVRkJiVU1zUjBGQmJrTXNRMEZCVUR0QlFVTkJMRTlCUVVrc1QwRkJUeXhoUVVGWUxFVkJRVEJDTzBGQlEzcENMRmRCUVU4c1lVRkJVQ3hEUVVGeFFpeEpRVUZ5UWl4RlFVRXlRaXhOUVVFelFpeEZRVUZ0UXl4VlFVRnVReXhGUVVFclF5eEhRVUV2UXp0QlFVTkJPenRCUVVWRUxGRkJRVXNzV1VGQlRDeERRVUZyUWl4eFFrRkJiRUlzUlVGQmVVTXNVVUZCVVN4WFFVRnFSRHRCUVVOQkxGRkJRVXNzV1VGQlRDeERRVUZyUWl4dlFrRkJiRUlzUlVGQmQwTXNWVUZCZUVNN08wRkJSVUU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJMRTlCUVVrc1MwRkJTeXhYUVVGTU8wRkJRMGc3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJMRXRCUVVVc1MwRkJTeXhYUVVGTUxFTkJRV2xDTEZGQlFXcENMRWxCUVRaQ0xFdEJRVXNzVjBGQlRDeERRVUZwUWl4UlFVRnFRaXhIUVVFMFFpeFBRVUUxUWl4RFFVRnZReXhqUVVGd1F5eEpRVUZ6UkN4RFFVRnlSaXhEUVU1SExFbEJUMGdzUTBGQlF5eFBRVkJHTEVWQlQxYzdRVUZEVmp0QlFVTkJPMEZCUTBFN1FVRkRRU3h4UWtGQmFVSXNTVUZCYWtJN08wRkJSVUVzVTBGQlN5eFhRVUZNTEVOQlFXbENMRzlDUVVGcVFpeEZRVUYxUXl4UlFVRlJMRmxCUVM5RE8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRU3hKUVhKQ1JDeE5RWEZDVHp0QlFVTk9MRk5CUVVzc1owSkJRVXdzUTBGQmMwSXNUVUZCZEVJc1JVRkJPRUlzVVVGQlVTeFpRVUYwUXl4RlFVRnZSQ3hMUVVGd1JEdEJRVU5CTEZOQlFVc3NaMEpCUVV3c1EwRkJjMElzVDBGQmRFSXNSVUZCSzBJc1VVRkJVU3hoUVVGMlF5eEZRVUZ6UkN4TFFVRjBSRHRCUVVOQk8wRkJRMFFzVVVGQlN5eEhRVUZNTEVkQlFWY3NSMEZCV0RzN1FVRkZRVHRCUVVOQk8wRkJRMEU3UVVGRFFTeFBRVUZKTEZkQlFVb3NSVUZCYVVJN1FVRkRhRUlzVTBGQlN5eFpRVUZNTEVOQlFXdENMRWxCUVd4Q0xFVkJRWGRDTEZkQlFYaENPMEZCUTBFc1NVRkdSQ3hOUVVWUE8wRkJRMDRzVTBGQlN5eFhRVUZNTEVOQlFXbENMRWxCUVdwQ08wRkJRMEU3TzBGQlJVUXNWVUZCVHl4SlFVRlFPMEZCUTBFc1IwRndSRVFzVFVGdlJFOHNTVUZCU1N4WFFVRktMRVZCUVdsQ08wRkJRM1pDTEU5QlFVazdRVUZEU0R0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFTeHJRa0ZCWXl4SFFVRmtPenRCUVVWQk8wRkJRMEVzV1VGQlVTeFpRVUZTTEVOQlFYRkNMRlZCUVhKQ08wRkJRMEVzU1VGWVJDeERRVmRGTEU5QlFVOHNRMEZCVUN4RlFVRlZPMEZCUTFnc1dVRkJVU3hQUVVGU0xFTkJRV2RDTEZWQlFWVXNaVUZCVml4RlFVTm1MRGhDUVVOQkxGVkJSRUVzUjBGRFlTeE5RVVJpTEVkQlEzTkNMRWRCUmxBc1JVRkhaaXhEUVVobExFVkJTV1lzUTBGQlF5eFZRVUZFTEVOQlNtVXNRMEZCYUVJN1FVRkxRVHRCUVVORU8wRkJRMFFzUlVFMVJVUTdPMEZCT0VWQk8wRkJRMEVzUzBGQlNTeGhRVUZoTEVOQlFVTXNTVUZCU1N4WlFVRjBRaXhGUVVGdlF6dEJRVU51UXp0QlFVTkJMR05CUVZrc1UwRkJXaXhGUVVGMVFpeFZRVUZUTEUxQlFWUXNSVUZCYVVJN1FVRkRka003UVVGRFFUdEJRVU5CTEU5QlFVa3NRMEZCUXl4SlFVRk1MRVZCUVZjN1FVRkRWaXhYUVVGUExFOUJRVThzVlVGQlpEdEJRVU5CT3p0QlFVVkVPMEZCUTBFN1FVRkRRVHRCUVVOQkxHTkJRVmNzVDBGQlR5eFpRVUZRTEVOQlFXOUNMRmRCUVhCQ0xFTkJRVmc3UVVGRFFTeFBRVUZKTEZGQlFVb3NSVUZCWXp0QlFVTmlPMEZCUTBFc2FVSkJRV0VzVVVGQllqczdRVUZGUVR0QlFVTkJMRkZCUVVrc1EwRkJReXhKUVVGSkxFOUJRVlFzUlVGQmEwSTdRVUZEYWtJN1FVRkRRVHRCUVVOQkxGZEJRVTBzVjBGQlZ5eExRVUZZTEVOQlFXbENMRWRCUVdwQ0xFTkJRVTQ3UVVGRFFTeHJRa0ZCWVN4SlFVRkpMRWRCUVVvc1JVRkJZanRCUVVOQkxHVkJRVlVzU1VGQlNTeE5RVUZLTEVkQlFXRXNTVUZCU1N4SlFVRktMRU5CUVZNc1IwRkJWQ3hKUVVGblFpeEhRVUUzUWl4SFFVRnRReXhKUVVFM1F6czdRVUZGUVN4VFFVRkpMRTlCUVVvc1IwRkJZeXhQUVVGa08wRkJRMEU3TzBGQlJVUTdRVUZEUVR0QlFVTkJMR2xDUVVGaExGZEJRVmNzVDBGQldDeERRVUZ0UWl4alFVRnVRaXhGUVVGdFF5eEZRVUZ1UXl4RFFVRmlPenRCUVVWQk8wRkJRMEVzVVVGQlNTeEpRVUZKTEZkQlFVb3NRMEZCWjBJc1NVRkJhRUlzUTBGQmNVSXNWVUZCY2tJc1EwRkJTaXhGUVVGelF6dEJRVU55UXl4clFrRkJZU3hSUVVGaU8wRkJRMEU3TzBGQlJVUTdRVUZEUVN4UlFVRkpMRWxCUVVvc1IwRkJWeXhKUVVGSkxFbEJRVW9zUjBGQlZ5eEpRVUZKTEVsQlFVb3NRMEZCVXl4TlFVRlVMRU5CUVdkQ0xGVkJRV2hDTEVOQlFWZ3NSMEZCZVVNc1EwRkJReXhWUVVGRUxFTkJRWEJFT3p0QlFVVkJMRmRCUVU4c1NVRkJVRHRCUVVOQk8wRkJRMFFzUjBGNFEwUTdRVUY1UTBFN08wRkJSVVE3T3pzN096dEJRVTFCTEV0QlFVa3NTVUZCU2l4SFFVRlhMRlZCUVZNc1NVRkJWQ3hGUVVGbE8wRkJRM3BDTzBGQlEwRXNVMEZCVHl4TFFVRkxMRWxCUVV3c1EwRkJVRHRCUVVOQkxFVkJTRVE3TzBGQlMwRTdRVUZEUVN4TFFVRkpMRWRCUVVvN1FVRkRRU3hEUVRVclJFUWlMQ0ptYVd4bElqb2laMlZ1WlhKaGRHVmtMbXB6SWl3aWMyOTFjbU5sVW05dmRDSTZJaUlzSW5OdmRYSmpaWE5EYjI1MFpXNTBJanBiSWlobWRXNWpkR2x2YmlncGUyWjFibU4wYVc5dUlISW9aU3h1TEhRcGUyWjFibU4wYVc5dUlHOG9hU3htS1h0cFppZ2hibHRwWFNsN2FXWW9JV1ZiYVYwcGUzWmhjaUJqUFZ3aVpuVnVZM1JwYjI1Y0lqMDlkSGx3Wlc5bUlISmxjWFZwY21VbUpuSmxjWFZwY21VN2FXWW9JV1ltSm1NcGNtVjBkWEp1SUdNb2FTd2hNQ2s3YVdZb2RTbHlaWFIxY200Z2RTaHBMQ0V3S1R0MllYSWdZVDF1WlhjZ1JYSnliM0lvWENKRFlXNXViM1FnWm1sdVpDQnRiMlIxYkdVZ0oxd2lLMmtyWENJblhDSXBPM1JvY205M0lHRXVZMjlrWlQxY0lrMVBSRlZNUlY5T1QxUmZSazlWVGtSY0lpeGhmWFpoY2lCd1BXNWJhVjA5ZTJWNGNHOXlkSE02ZTMxOU8yVmJhVjFiTUYwdVkyRnNiQ2h3TG1WNGNHOXlkSE1zWm5WdVkzUnBiMjRvY2lsN2RtRnlJRzQ5WlZ0cFhWc3hYVnR5WFR0eVpYUjFjbTRnYnlodWZIeHlLWDBzY0N4d0xtVjRjRzl5ZEhNc2NpeGxMRzRzZENsOWNtVjBkWEp1SUc1YmFWMHVaWGh3YjNKMGMzMW1iM0lvZG1GeUlIVTlYQ0ptZFc1amRHbHZibHdpUFQxMGVYQmxiMllnY21WeGRXbHlaU1ltY21WeGRXbHlaU3hwUFRBN2FUeDBMbXhsYm1kMGFEdHBLeXNwYnloMFcybGRLVHR5WlhSMWNtNGdiMzF5WlhSMWNtNGdjbjBwS0NraUxDSXZLaUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1SUdOdmJHeGxZM1JwYjI0dWFuTWdNakF4Tmkwd05pMHlNbHh1SUVkaGJXSnBieUJIYldKSVhHNGdhSFIwY0RvdkwzZDNkeTVuWVcxaWFXOHVaR1ZjYmlCRGIzQjVjbWxuYUhRZ0tHTXBJREl3TVRZZ1IyRnRZbWx2SUVkdFlraGNiaUJTWld4bFlYTmxaQ0IxYm1SbGNpQjBhR1VnUjA1VklFZGxibVZ5WVd3Z1VIVmliR2xqSUV4cFkyVnVjMlVnS0ZabGNuTnBiMjRnTWlsY2JpQmJhSFIwY0RvdkwzZDNkeTVuYm5VdWIzSm5MMnhwWTJWdWMyVnpMMmR3YkMweUxqQXVhSFJ0YkYxY2JpQXRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExWeHVJQ292WEc1Y2JpaG1kVzVqZEdsdmJpZ3BJSHRjYmx4MFhHNWNkQ2QxYzJVZ2MzUnlhV04wSnp0Y2JseDBYRzVjZEM4cUtseHVYSFFnS2lCRGJHRnpjeUJEYjJ4c1pXTjBhVzl1WEc1Y2RDQXFJRnh1WEhRZ0tpQlVhR2x6SUdOc1lYTnpJR2x6SUhWelpXUWdkRzhnYUdGdVpHeGxJRzExYkhScGNHeGxJRzF2WkhWc1pYTWdiMllnZEdobElITmhiV1VnZEhsd1pTQW9ZMjl1ZEhKdmJHeGxjbk1zSUdWNGRHVnVjMmx2Ym5NZ0xpNHVLUzVjYmx4MElDcGNibHgwSUNvZ1FHTnNZWE56SUVwVFJTOURiMjV6ZEhKMVkzUnZjbk12UTI5c2JHVmpkR2x2Ymx4dVhIUWdLaTljYmx4MFkyeGhjM01nUTI5c2JHVmpkR2x2YmlCN1hHNWNkRngwTHlvcVhHNWNkRngwSUNvZ1EyeGhjM01nUTI5dWMzUnlkV04wYjNJZ1hHNWNkRngwSUNvZ1hHNWNkRngwSUNvZ1FIQmhjbUZ0SUh0VGRISnBibWQ5SUc1aGJXVWdWR2hsSUdOdmJHeGxZM1JwYjI0Z2JtRnRaU0F0SUcxMWMzUWdZbVVnZFc1cGNYVmxMbHh1WEhSY2RDQXFJRUJ3WVhKaGJTQjdVM1J5YVc1bmZTQmhkSFJ5YVdKMWRHVWdWR2hsSUdGMGRISnBZblYwWlNCMGFHRjBJSGRwYkd3Z2RISnBaMmRsY2lCamIyeHNaV04wYVc5dUozTWdiVzlrZFd4bGN5NWNibHgwWEhRZ0tpQkFjR0Z5WVcwZ2UwOWlhbVZqZEgwZ2JtRnRaWE53WVdObElFOXdkR2x2Ym1Gc0xDQjBhR1VnYm1GdFpYTndZV05sSUdsdWMzUmhibU5sSUhkb1pYSmxJSFJvWlNCamIyeHNaV04wYVc5dUlHSmxiRzl1WjNNdVhHNWNkRngwSUNvdlhHNWNkRngwWTI5dWMzUnlkV04wYjNJb2JtRnRaU3dnWVhSMGNtbGlkWFJsTENCdVlXMWxjM0JoWTJVcElIdGNibHgwWEhSY2RIUm9hWE11Ym1GdFpTQTlJRzVoYldVN1hHNWNkRngwWEhSMGFHbHpMbUYwZEhKcFluVjBaU0E5SUdGMGRISnBZblYwWlR0Y2JseDBYSFJjZEhSb2FYTXVibUZ0WlhOd1lXTmxJRDBnYm1GdFpYTndZV05sTzF4dVhIUmNkRngwZEdocGN5NWpZV05vWlNBOUlIdGNibHgwWEhSY2RGeDBiVzlrZFd4bGN6b2dlMzBzWEc1Y2RGeDBYSFJjZEdSaGRHRTZJSHQ5WEc1Y2RGeDBYSFI5TzF4dVhIUmNkSDFjYmx4MFhIUmNibHgwWEhRdktpcGNibHgwWEhRZ0tpQkVaV1pwYm1VZ1lTQnVaWGNnWlc1bmFXNWxJRzF2WkhWc1pTNWNibHgwWEhRZ0tseHVYSFJjZENBcUlGUm9hWE1nWm5WdVkzUnBiMjRnZDJsc2JDQmtaV1pwYm1VZ1lTQnVaWGNnYlc5a2RXeGxJR2x1ZEc4Z2RHaGxJR1Z1WjJsdVpTNGdSV0ZqYUNCdGIyUjFiR1VnZDJsc2JDQmlaU0J6ZEc5eVpXUWdhVzRnZEdobFhHNWNkRngwSUNvZ1kyOXNiR1ZqZEdsdmJpZHpJR05oWTJobElIUnZJSEJ5WlhabGJuUWdkVzV1WldObGMzTmhjbmtnWm1sc1pTQjBjbUZ1YzJabGNuTXVJRlJvWlNCellXMWxJR2hoY0hCbGJuTWdkMmwwYUNCMGFHVWdaR1ZtWVhWc2RGeHVYSFJjZENBcUlHTnZibVpwWjNWeVlYUnBiMjRnZEdoaGRDQmhjSEJsYm1RZ2RHOGdkR2hsSUcxdlpIVnNaU0JrWldacGJtbDBhVzl1TGx4dVhIUmNkQ0FxWEc1Y2RGeDBJQ29nUUhCaGNtRnRJSHRUZEhKcGJtZDlJRzVoYldVZ1RtRnRaU0J2WmlCMGFHVWdiVzlrZFd4bElDaHpZVzFsSUdGeklIUm9aU0JtYVd4bGJtRnRaU2t1WEc1Y2RGeDBJQ29nUUhCaGNtRnRJSHRCY25KaGVYMGdaR1Z3Wlc1a1pXNWphV1Z6SUVGeWNtRjVJRzltSUd4cFluSmhjbWxsY3lCMGFHRjBJSFJvYVhNZ2JXOWtkV3hsSUdSbGNHVnVaSE1nYjI0Z0tIZHBiR3dnWW1VZ2JHOWhaR1ZrSUdGemVXNWphSEp2Ym05MWMyeDVLUzVjYmx4MFhIUWdLaUJCY0hCc2VTQnZibXg1SUdacGJHVnVZVzFsY3lCM2FYUm9iM1YwSUdWNGRHVnVjMmx2YmlCbExtY3VJRnRjSW1WdFlXbHNjMXdpWFM1Y2JseDBYSFFnS2lCQWNHRnlZVzBnZTA5aWFtVmpkSDBnWTI5a1pTQkRiMjUwWVdsdWN5QjBhR1VnYlc5a2RXeGxJR052WkdVZ0tHWjFibU4wYVc5dUtTNWNibHgwWEhRZ0tpOWNibHgwWEhSdGIyUjFiR1VvYm1GdFpTd2daR1Z3Wlc1a1pXNWphV1Z6TENCamIyUmxLU0I3WEc1Y2RGeDBYSFF2THlCRGFHVmpheUJwWmlCeVpYRjFhWEpsWkNCMllXeDFaWE1nWVhKbElHRjJZV2xzWVdKc1pTQmhibVFnYjJZZ1kyOXljbVZqZENCMGVYQmxMbHh1WEhSY2RGeDBhV1lnS0NGdVlXMWxJSHg4SUhSNWNHVnZaaUJ1WVcxbElDRTlQU0FuYzNSeWFXNW5KeUI4ZkNCMGVYQmxiMllnWTI5a1pTQWhQVDBnSjJaMWJtTjBhVzl1SnlrZ2UxeHVYSFJjZEZ4MFhIUnFjMlV1WTI5eVpTNWtaV0oxWnk1M1lYSnVLQ2RTWldkcGMzUnlZWFJwYjI0Z2IyWWdkR2hsSUcxdlpIVnNaU0JtWVdsc1pXUXNJR1IxWlNCMGJ5QmlZV1FnWm5WdVkzUnBiMjRnWTJGc2JDY3NJR0Z5WjNWdFpXNTBjeWs3WEc1Y2RGeDBYSFJjZEhKbGRIVnliaUJtWVd4elpUdGNibHgwWEhSY2RIMWNibHgwWEhSY2RGeHVYSFJjZEZ4MEx5OGdRMmhsWTJzZ2FXWWdkR2hsSUcxdlpIVnNaU0JwY3lCaGJISmxZV1I1SUdSbFptbHVaV1F1WEc1Y2RGeDBYSFJwWmlBb2RHaHBjeTVqWVdOb1pTNXRiMlIxYkdWelcyNWhiV1ZkS1NCN1hHNWNkRngwWEhSY2RHcHpaUzVqYjNKbExtUmxZblZuTG5kaGNtNG9KMUpsWjJsemRISmhkR2x2YmlCdlppQnRiMlIxYkdVZ1hDSW5JQ3NnYm1GdFpTQXJJQ2RjSWlCemEybHdjR1ZrTENCaVpXTmhkWE5sSUdsMElHRnNjbVZoWkhrZ1pYaHBjM1J6TGljcE8xeHVYSFJjZEZ4MFhIUnlaWFIxY200Z1ptRnNjMlU3WEc1Y2RGeDBYSFI5WEc1Y2RGeDBYSFJjYmx4MFhIUmNkQzh2SUZOMGIzSmxJSFJvWlNCdGIyUjFiR1VnZEc4Z1kyRmphR1VnYzI4Z2RHaGhkQ0JwZENCallXNGdZbVVnZFhObFpDQnNZWFJsY2k1Y2JseDBYSFJjZEhSb2FYTXVZMkZqYUdVdWJXOWtkV3hsYzF0dVlXMWxYU0E5SUh0Y2JseDBYSFJjZEZ4MFkyOWtaVG9nWTI5a1pTeGNibHgwWEhSY2RGeDBaR1Z3Wlc1a1pXNWphV1Z6T2lCa1pYQmxibVJsYm1OcFpYTmNibHgwWEhSY2RIMDdYRzVjZEZ4MGZWeHVYSFJjZEZ4dVhIUmNkQzhxS2x4dVhIUmNkQ0FxSUVsdWFYUnBZV3hwZW1VZ1RXOWtkV3hsSUVOdmJHeGxZM1JwYjI1Y2JseDBYSFFnS2x4dVhIUmNkQ0FxSUZSb2FYTWdiV1YwYUc5a0lIZHBiR3dnZEhKcFoyZGxjaUIwYUdVZ2NHRm5aU0J0YjJSMWJHVnpJR2x1YVhScFlXeHBlbUYwYVc5dUxpQkpkQ0IzYVd4c0lITmxZWEpqYUNCaGJHeGNibHgwWEhRZ0tpQjBhR1VnUkU5TklHWnZjaUIwYUdVZ1hDSmtZWFJoTFdkNExXVjRkR1Z1YzJsdmJsd2lMQ0JjSW1SaGRHRXRaM2d0WTI5dWRISnZiR3hsY2x3aUlHOXlYRzVjZEZ4MElDb2dYQ0prWVhSaExXZDRMWGRwWkdkbGRGd2lJR0YwZEhKcFluVjBaWE1nWVc1a0lHeHZZV1FnZEdobElISmxiR1YyWVc1MElITmpjbWx3ZEhNZ2RHaHliM1ZuYUNCU1pYRjFhWEpsU2xNdVhHNWNkRngwSUNwY2JseDBYSFFnS2lCQWNHRnlZVzBnZTJwUmRXVnllWDBnSkhCaGNtVnVkQ0JQY0hScGIyNWhiQ0FvYm5Wc2JDa3NJSEJoY21WdWRDQmxiR1Z0Wlc1MElIZHBiR3dnWW1VZ2RYTmxaQ0IwYnlCelpXRnlZMmdnWm05eUlIUm9aU0J5WlhGMWFYSmxaQ0J0YjJSMWJHVnpMbHh1WEhSY2RDQXFJRnh1WEhSY2RDQXFJRUJ5WlhSMWNtNGdlMnBSZFdWeWVTNUVaV1psY25KbFpIMGdibUZ0WlhOd1lXTmxSR1ZtWlhKeVpXUWdSR1ZtWlhKeVpXUWdiMkpxWldOMElIUm9ZWFFnWjJWMGN5QndjbTlqWlhOelpXUWdZV1owWlhJZ2RHaGxYRzVjZEZ4MElDb2diVzlrZFd4bElHbHVhWFJwWVd4cGVtRjBhVzl1SUdseklHWnBibWx6YUdWa0xseHVYSFJjZENBcUwxeHVYSFJjZEdsdWFYUW9KSEJoY21WdWRDQTlJRzUxYkd3cElIdGNibHgwWEhSY2RDOHZJRk4wYjNKbElIUm9aU0J1WVcxbGMzQmhZMlZ6SUhKbFptVnlaVzVqWlNCdlppQjBhR1VnWTI5c2JHVmpkR2x2Ymk1Y2JseDBYSFJjZEdsbUlDZ2hkR2hwY3k1dVlXMWxjM0JoWTJVcElIdGNibHgwWEhSY2RGeDBkR2h5YjNjZ2JtVjNJRVZ5Y205eUtDZERiMnhzWldOMGFXOXVJR05oYm01dmRDQmlaU0JwYm1sMGFXRnNhWHBsWkNCM2FYUm9iM1YwSUdsMGN5QndZWEpsYm5RZ2JtRnRaWE53WVdObElHbHVjM1JoYm1ObExpY3BPMXh1WEhSY2RGeDBmVnh1WEhSY2RGeDBYRzVjZEZ4MFhIUXZMeUJUWlhRZ2RHaGxJR1JsWm1GMWJIUWdjR0Z5Wlc1MExXOWlhbVZqZENCcFppQnViMjVsSUhkaGN5Qm5hWFpsYmk1Y2JseDBYSFJjZEdsbUlDZ2tjR0Z5Wlc1MElEMDlQU0IxYm1SbFptbHVaV1FnZkh3Z0pIQmhjbVZ1ZENBOVBUMGdiblZzYkNrZ2UxeHVYSFJjZEZ4MFhIUWtjR0Z5Wlc1MElEMGdKQ2duYUhSdGJDY3BPMXh1WEhSY2RGeDBmVnh1WEhSY2RGeDBYRzVjZEZ4MFhIUmpiMjV6ZENCaGRIUnlhV0oxZEdVZ1BTQW5aR0YwWVMwbklDc2dkR2hwY3k1dVlXMWxjM0JoWTJVdWJtRnRaU0FySUNjdEp5QXJJSFJvYVhNdVlYUjBjbWxpZFhSbE8xeHVYSFJjZEZ4MFkyOXVjM1FnYm1GdFpYTndZV05sUkdWbVpYSnlaV1FnUFNBa0xrUmxabVZ5Y21Wa0tDazdYRzVjZEZ4MFhIUmpiMjV6ZENCa1pXWmxjbkpsWkVOdmJHeGxZM1JwYjI0Z1BTQmJYVHRjYmx4MFhIUmNkRnh1WEhSY2RGeDBKSEJoY21WdWRGeHVYSFJjZEZ4MFhIUXVabWxzZEdWeUtDZGJKeUFySUdGMGRISnBZblYwWlNBcklDZGRKeWxjYmx4MFhIUmNkRngwTG1Ga1pDZ2tjR0Z5Wlc1MExtWnBibVFvSjFzbklDc2dZWFIwY21saWRYUmxJQ3NnSjEwbktTbGNibHgwWEhSY2RGeDBMbVZoWTJnb0tHbHVaR1Y0TENCbGJHVnRaVzUwS1NBOVBpQjdYRzVjZEZ4MFhIUmNkRngwWTI5dWMzUWdKR1ZzWlcxbGJuUWdQU0FrS0dWc1pXMWxiblFwTzF4dVhIUmNkRngwWEhSY2RHTnZibk4wSUcxdlpIVnNaWE1nUFNBa1pXeGxiV1Z1ZEM1aGRIUnlLR0YwZEhKcFluVjBaU2s3WEc1Y2RGeDBYSFJjZEZ4MFhHNWNkRngwWEhSY2RGeDBKR1ZzWlcxbGJuUXVjbVZ0YjNabFFYUjBjaWhoZEhSeWFXSjFkR1VwTzF4dVhIUmNkRngwWEhSY2RGeHVYSFJjZEZ4MFhIUmNkQ1F1WldGamFDaHRiMlIxYkdWekxuSmxjR3hoWTJVb0x5aGNYSEpjWEc1OFhGeHVmRnhjY254Y1hITmNYSE1yS1M5bmJTd2dKeUFuS1M1MGNtbHRLQ2t1YzNCc2FYUW9KeUFuS1N3Z0tHbHVaR1Y0TENCdVlXMWxLU0E5UGlCN1hHNWNkRngwWEhSY2RGeDBYSFJwWmlBb2JtRnRaU0E5UFQwZ0p5Y3BJSHRjYmx4MFhIUmNkRngwWEhSY2RGeDBjbVYwZFhKdUlIUnlkV1U3WEc1Y2RGeDBYSFJjZEZ4MFhIUjlYRzVjZEZ4MFhIUmNkRngwWEhSY2JseDBYSFJjZEZ4MFhIUmNkR052Ym5OMElHUmxabVZ5Y21Wa0lEMGdKQzVFWldabGNuSmxaQ2dwTzF4dVhIUmNkRngwWEhSY2RGeDBaR1ZtWlhKeVpXUkRiMnhzWldOMGFXOXVMbkIxYzJnb1pHVm1aWEp5WldRcE8xeHVYSFJjZEZ4MFhIUmNkRngwWEc1Y2RGeDBYSFJjZEZ4MFhIUnFjMlV1WTI5eVpTNXRiMlIxYkdWZmJHOWhaR1Z5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkQzVzYjJGa0tDUmxiR1Z0Wlc1MExDQnVZVzFsTENCMGFHbHpLVnh1WEhSY2RGeDBYSFJjZEZ4MFhIUXVaRzl1WlNnb2JXOWtkV3hsS1NBOVBpQnRiMlIxYkdVdWFXNXBkQ2hrWldabGNuSmxaQ2twWEc1Y2RGeDBYSFJjZEZ4MFhIUmNkQzVtWVdsc0tDaGxjbkp2Y2lrZ1BUNGdlMXh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkR1JsWm1WeWNtVmtMbkpsYW1WamRDZ3BPMXh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkQzh2SUV4dlp5QjBhR1VnWlhKeWIzSWdhVzRnZEdobElHTnZibk52YkdVZ1luVjBJR1J2SUc1dmRDQnpkRzl3SUhSb1pTQmxibWRwYm1VZ1pYaGxZM1YwYVc5dUxseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RHcHpaUzVqYjNKbExtUmxZblZuTG1WeWNtOXlLQ2REYjNWc1pDQnViM1FnYkc5aFpDQnRiMlIxYkdVNklDY2dLeUJ1WVcxbExDQmxjbkp2Y2lrN1hHNWNkRngwWEhSY2RGeDBYSFJjZEgwcE8xeHVYSFJjZEZ4MFhIUmNkSDBwTzF4dVhIUmNkRngwWEhSOUtUdGNibHgwWEhSY2RGeHVYSFJjZEZ4MEx5OGdRV3gzWVhseklISmxjMjlzZG1VZ2RHaGxJRzVoYldWemNHRmpaU3dnWlhabGJpQnBaaUIwYUdWeVpTQmhjbVVnYlc5a2RXeGxJR1Z5Y205eWN5NWNibHgwWEhSY2RDUXVkMmhsYmk1aGNIQnNlU2gxYm1SbFptbHVaV1FzSUdSbFptVnljbVZrUTI5c2JHVmpkR2x2YmlrdVlXeDNZWGx6S0NncElEMCtJRzVoYldWemNHRmpaVVJsWm1WeWNtVmtMbkpsYzI5c2RtVW9LU2s3WEc1Y2RGeDBYSFJjYmx4MFhIUmNkSEpsZEhWeWJpQmtaV1psY25KbFpFTnZiR3hsWTNScGIyNHViR1Z1WjNSb0lEOGdibUZ0WlhOd1lXTmxSR1ZtWlhKeVpXUXVjSEp2YldselpTZ3BJRG9nYm1GdFpYTndZV05sUkdWbVpYSnlaV1F1Y21WemIyeDJaU2dwT3lCY2JseDBYSFI5WEc1Y2RIMWNibHgwWEc1Y2RHcHpaUzVqYjI1emRISjFZM1J2Y25NdVEyOXNiR1ZqZEdsdmJpQTlJRU52Ykd4bFkzUnBiMjQ3WEc1OUtTZ3BPMXh1SWl3aUx5b2dMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMWNjbHh1SUdSaGRHRmZZbWx1WkdsdVp5NXFjeUF5TURFMkxUQTFMVEUzWEhKY2JpQkhZVzFpYVc4Z1IyMWlTRnh5WEc0Z2FIUjBjRG92TDNkM2R5NW5ZVzFpYVc4dVpHVmNjbHh1SUVOdmNIbHlhV2RvZENBb1l5a2dNakF4TmlCSFlXMWlhVzhnUjIxaVNGeHlYRzRnVW1Wc1pXRnpaV1FnZFc1a1pYSWdkR2hsSUVkT1ZTQkhaVzVsY21Gc0lGQjFZbXhwWXlCTWFXTmxibk5sSUNoV1pYSnphVzl1SURJcFhISmNiaUJiYUhSMGNEb3ZMM2QzZHk1bmJuVXViM0puTDJ4cFkyVnVjMlZ6TDJkd2JDMHlMakF1YUhSdGJGMWNjbHh1SUMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0WEhKY2JpQXFMMXh5WEc1Y2NseHVLR1oxYm1OMGFXOXVLQ2tnZTF4eVhHNWNkRnh5WEc1Y2RDZDFjMlVnYzNSeWFXTjBKenRjY2x4dVhIUmNjbHh1WEhRdktpcGNjbHh1WEhRZ0tpQkVZWFJoSUVKcGJtUnBibWNnUTJ4aGMzTWdYSEpjYmx4MElDb2dYSEpjYmx4MElDb2dTR0Z1Wkd4bGN5QjBkMjh0ZDJGNUlHUmhkR0VnWW1sdVpHbHVaeUIzYVhSb0lGVkpJR1ZzWlcxbGJuUnpMaUJjY2x4dVhIUWdLaUJjY2x4dVhIUWdLaUJBWTJ4aGMzTWdTbE5GTDBOdmJuTjBjblZqZEc5eWN5OUVZWFJoUW1sdVpHbHVaMXh5WEc1Y2RDQXFMMXh5WEc1Y2RHTnNZWE56SUVSaGRHRkNhVzVrYVc1bklIdGNjbHh1WEhSY2RDOHFLbHh5WEc1Y2RGeDBJQ29nUTJ4aGMzTWdRMjl1YzNSeWRXTjBiM0lnWEhKY2JseDBYSFFnS2lCY2NseHVYSFJjZENBcUlFQndZWEpoYlNCN1UzUnlhVzVuZlNCdVlXMWxJRlJvWlNCdVlXMWxJRzltSUhSb1pTQmlhVzVrYVc1bkxpQmNjbHh1WEhSY2RDQXFJRUJ3WVhKaGJTQjdUMkpxWldOMGZTQWtaV3hsYldWdWRDQlVZWEpuWlhRZ1pXeGxiV1Z1ZENCMGJ5QmlaU0JpYjI1a0xpQmNjbHh1WEhSY2RDQXFMMXh5WEc1Y2RGeDBZMjl1YzNSeWRXTjBiM0lvYm1GdFpTd2dKR1ZzWlcxbGJuUXBJSHRjY2x4dVhIUmNkRngwZEdocGN5NXVZVzFsSUQwZ2JtRnRaVHRjY2x4dVhIUmNkRngwZEdocGN5NGtaV3hsYldWdWRDQTlJQ1JsYkdWdFpXNTBPMXh5WEc1Y2RGeDBYSFIwYUdsekxuWmhiSFZsSUQwZ2JuVnNiRHRjY2x4dVhIUmNkRngwZEdocGN5NXBjMDExZEdGaWJHVWdQU0FrWld4bGJXVnVkQzVwY3lnbmFXNXdkWFFzSUhSbGVIUmhjbVZoTENCelpXeGxZM1FuS1R0Y2NseHVYSFJjZEZ4MGRHaHBjeTVwYm1sMEtDazdYSEpjYmx4MFhIUjlYSEpjYmx4MFhIUmNjbHh1WEhSY2RDOHFLbHh5WEc1Y2RGeDBJQ29nU1c1cGRHbGhiR2w2WlNCMGFHVWdZbWx1WkdsdVp5NWNjbHh1WEhSY2RDQXFMMXh5WEc1Y2RGeDBhVzVwZENncElIdGNjbHh1WEhSY2RGeDBkR2hwY3k0a1pXeGxiV1Z1ZEM1dmJpZ25ZMmhoYm1kbEp5d2dLQ2tnUFQ0Z2UxeHlYRzVjZEZ4MFhIUmNkSFJvYVhNdVoyVjBLQ2s3WEhKY2JseDBYSFJjZEgwcE8xeHlYRzVjZEZ4MGZWeHlYRzVjZEZ4MFhISmNibHgwWEhRdktpcGNjbHh1WEhSY2RDQXFJRWRsZENCaWFXNWthVzVuSUhaaGJIVmxMaUJjY2x4dVhIUmNkQ0FxSUZ4eVhHNWNkRngwSUNvZ1FISmxkSFZ5Ym5NZ2V5cDlYSEpjYmx4MFhIUWdLaTljY2x4dVhIUmNkR2RsZENncElIdGNjbHh1WEhSY2RGeDBkR2hwY3k1MllXeDFaU0E5SUhSb2FYTXVhWE5OZFhSaFlteGxJRDhnZEdocGN5NGtaV3hsYldWdWRDNTJZV3dvS1NBNklIUm9hWE11SkdWc1pXMWxiblF1YUhSdGJDZ3BPMXh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwYVdZZ0tIUm9hWE11SkdWc1pXMWxiblF1YVhNb0p6cGphR1ZqYTJKdmVDY3BJSHg4SUNCMGFHbHpMaVJsYkdWdFpXNTBMbWx6S0NjNmNtRmthVzhuS1NrZ2UxeHlYRzVjZEZ4MFhIUmNkSFJvYVhNdWRtRnNkV1VnUFNCMGFHbHpMaVJsYkdWdFpXNTBMbkJ5YjNBb0oyTm9aV05yWldRbktUdGNjbHh1WEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwY21WMGRYSnVJSFJvYVhNdWRtRnNkV1U3WEhKY2JseDBYSFI5WEhKY2JseDBYSFJjY2x4dVhIUmNkQzhxS2x4eVhHNWNkRngwSUNvZ1UyVjBJR0pwYm1ScGJtY2dkbUZzZFdVdUlGeHlYRzVjZEZ4MElDb2dYSEpjYmx4MFhIUWdLaUJBY0dGeVlXMGdlMU4wY21sdVozMGdkbUZzZFdWY2NseHVYSFJjZENBcUwxeHlYRzVjZEZ4MGMyVjBLSFpoYkhWbEtTQjdYSEpjYmx4MFhIUmNkSFJvYVhNdWRtRnNkV1VnUFNCMllXeDFaVHRjY2x4dVhIUmNkRngwWEhKY2JseDBYSFJjZEdsbUlDaDBhR2x6TG1selRYVjBZV0pzWlNrZ2UxeHlYRzVjZEZ4MFhIUmNkSFJvYVhNdUpHVnNaVzFsYm5RdWRtRnNLSFpoYkhWbEtUdGNjbHh1WEhSY2RGeDBmU0JsYkhObElIdGNjbHh1WEhSY2RGeDBYSFIwYUdsekxpUmxiR1Z0Wlc1MExtaDBiV3dvZG1Gc2RXVXBPMXh5WEc1Y2RGeDBYSFI5WEhKY2JseDBYSFI5WEhKY2JseDBmVnh5WEc1Y2RGeHlYRzVjZEdwelpTNWpiMjV6ZEhKMVkzUnZjbk11UkdGMFlVSnBibVJwYm1jZ1BTQkVZWFJoUW1sdVpHbHVaenRjY2x4dWZTa29LVHRjY2x4dUlpd2lMeW9nTFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzFjYmlCdGIyUjFiR1V1YW5NZ01qQXhOaTB3TlMweE4xeHVJRWRoYldKcGJ5QkhiV0pJWEc0Z2FIUjBjRG92TDNkM2R5NW5ZVzFpYVc4dVpHVmNiaUJEYjNCNWNtbG5hSFFnS0dNcElESXdNVFlnUjJGdFltbHZJRWR0WWtoY2JpQlNaV3hsWVhObFpDQjFibVJsY2lCMGFHVWdSMDVWSUVkbGJtVnlZV3dnVUhWaWJHbGpJRXhwWTJWdWMyVWdLRlpsY25OcGIyNGdNaWxjYmlCYmFIUjBjRG92TDNkM2R5NW5iblV1YjNKbkwyeHBZMlZ1YzJWekwyZHdiQzB5TGpBdWFIUnRiRjFjYmlBdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdFhHNGdLaTljYmx4dUtHWjFibU4wYVc5dUtDa2dlMXh1WEhSY2JseDBKM1Z6WlNCemRISnBZM1FuTzF4dVhIUmNibHgwTHlvcVhHNWNkQ0FxSUVOc1lYTnpJRTF2WkhWc1pWeHVYSFFnS2x4dVhIUWdLaUJVYUdseklHTnNZWE56SUdseklIVnpaV1FnWm05eUlISmxjSEpsYzJWdWRHbHVaeUJoSUcxdlpIVnNaU0JwYm5OMFlXNWpaU0IzYVhSb2FXNGdkR2hsSUVwVFJTQmxZMjl6ZVhOMFpXMHVYRzVjZENBcVhHNWNkQ0FxSUVCamJHRnpjeUJLVTBVdlEyOXVjM1J5ZFdOMGIzSnpMMDF2WkhWc1pWeHVYSFFnS2k5Y2JseDBZMnhoYzNNZ1RXOWtkV3hsSUh0Y2JseDBYSFF2S2lwY2JseDBYSFFnS2lCRGJHRnpjeUJEYjI1emRISjFZM1J2Y2x4dVhIUmNkQ0FxWEc1Y2RGeDBJQ29nUUhCaGNtRnRJSHRQWW1wbFkzUjlJQ1JsYkdWdFpXNTBJRTF2WkhWc1pTQmxiR1Z0Wlc1MElITmxiR1ZqZEc5eUlHOWlhbVZqZEM1Y2JseDBYSFFnS2lCQWNHRnlZVzBnZTFOMGNtbHVaMzBnYm1GdFpTQlVhR1VnYlc5a2RXeGxJRzVoYldVZ0tHMXBaMmgwSUdOdmJuUmhhVzRnZEdobElIQmhkR2dwWEc1Y2RGeDBJQ29nUUhCaGNtRnRJSHRQWW1wbFkzUjlJR052Ykd4bFkzUnBiMjRnVkdobElHTnZiR3hsWTNScGIyNGdhVzV6ZEdGdVkyVWdiMllnZEdobElHMXZaSFZzWlM1Y2JseDBYSFFnS2k5Y2JseDBYSFJqYjI1emRISjFZM1J2Y2lna1pXeGxiV1Z1ZEN3Z2JtRnRaU3dnWTI5c2JHVmpkR2x2YmlrZ2UxeHVYSFJjZEZ4MGRHaHBjeTRrWld4bGJXVnVkQ0E5SUNSbGJHVnRaVzUwTzF4dVhIUmNkRngwZEdocGN5NXVZVzFsSUQwZ2JtRnRaVHRjYmx4MFhIUmNkSFJvYVhNdVkyOXNiR1ZqZEdsdmJpQTlJR052Ykd4bFkzUnBiMjQ3WEc1Y2RGeDBmVnh1WEhSY2RGeHVYSFJjZEM4cUtseHVYSFJjZENBcUlFbHVhWFJwWVd4cGVtVWdkR2hsSUcxdlpIVnNaU0JsZUdWamRYUnBiMjR1WEc1Y2RGeDBJQ3BjYmx4MFhIUWdLaUJVYUdseklHWjFibU4wYVc5dUlIZHBiR3dnWlhobFkzVjBaU0IwYUdVZ1hDSnBibWwwWENJZ2JXVjBhRzlrSUc5bUlHVmhZMmdnYlc5a2RXeGxMbHh1WEhSY2RDQXFYRzVjZEZ4MElDb2dRSEJoY21GdElIdFBZbXBsWTNSOUlHTnZiR3hsWTNScGIyNUVaV1psY25KbFpDQkVaV1psY25KbFpDQnZZbXBsWTNRZ2RHaGhkQ0JuWlhSeklIQnliMk5sYzNObFpDQmhablJsY2lCMGFHVWdiVzlrZFd4bFhHNWNkRngwSUNvZ2FXNXBkR2xoYkdsNllYUnBiMjRnYVhNZ1ptbHVhWE5vWldRdVhHNWNkRngwSUNvdlhHNWNkRngwYVc1cGRDaGpiMnhzWldOMGFXOXVSR1ZtWlhKeVpXUXBJSHRjYmx4MFhIUmNkQzh2SUZOMGIzSmxJRzF2WkhWc1pTQnBibk4wWVc1alpTQmhiR2xoY3k1Y2JseDBYSFJjZEdOdmJuTjBJR05oWTJobFpDQTlJSFJvYVhNdVkyOXNiR1ZqZEdsdmJpNWpZV05vWlM1dGIyUjFiR1Z6VzNSb2FYTXVibUZ0WlYwN1hHNWNkRngwWEhSc1pYUWdkR2x0Wlc5MWRDQTlJRzUxYkd3N1hHNWNkRngwWEhSY2JseDBYSFJjZEhSeWVTQjdYRzVjZEZ4MFhIUmNkR2xtSUNnaFkyRmphR1ZrS1NCN1hHNWNkRngwWEhSY2RGeDBkR2h5YjNjZ2JtVjNJRVZ5Y205eUtHQk5iMlIxYkdVZ1hDSWtlM1JvYVhNdWJtRnRaWDFjSWlCamIzVnNaQ0J1YjNRZ1ltVWdabTkxYm1RZ2FXNGdkR2hsSUdOdmJHeGxZM1JwYjI0Z1kyRmphR1V1WUNrN1hHNWNkRngwWEhSY2RIMWNibHgwWEhSY2RGeDBYRzVjZEZ4MFhIUmNkR052Ym5OMElHUmhkR0VnUFNCMGFHbHpMbDluWlhSTmIyUjFiR1ZFWVhSaEtDazdYRzVjZEZ4MFhIUmNkR052Ym5OMElHbHVjM1JoYm1ObElEMGdZMkZqYUdWa0xtTnZaR1V1WTJGc2JDaDBhR2x6TGlSbGJHVnRaVzUwTENCa1lYUmhLVHRjYmx4MFhIUmNkRngwWEc1Y2RGeDBYSFJjZEM4dklGQnliM1pwWkdVZ1lTQmtiMjVsSUdaMWJtTjBhVzl1SUhSb1lYUWdibVZsWkhNZ2RHOGdZbVVnWTJGc2JHVmtJR1p5YjIwZ2RHaGxJRzF2WkhWc1pTd2dhVzRnYjNKa1pYSWdkRzhnYVc1bWIzSnRJRnh1WEhSY2RGeDBYSFF2THlCMGFHRjBJSFJvWlNCdGIyUjFiR1VnWENKcGJtbDBYQ0lnWm5WdVkzUnBiMjRnZDJGeklHTnZiWEJzWlhSbFpDQnpkV05qWlhOelpuVnNiSGt1WEc1Y2RGeDBYSFJjZEdOdmJuTjBJR1J2Ym1VZ1BTQW9LU0E5UGlCN1hHNWNkRngwWEhSY2RGeDBkR2hwY3k0a1pXeGxiV1Z1ZEM1MGNtbG5aMlZ5S0NkcWMyVTZiVzlrZFd4bE9tbHVhWFJwWVd4cGVtVmtKeXdnVzN0dGIyUjFiR1U2SUhSb2FYTXVibUZ0WlgxZEtUdGNibHgwWEhSY2RGeDBYSFJxYzJVdVkyOXlaUzVrWldKMVp5NXBibVp2S0dCTmIyUjFiR1VnWENJa2UzUm9hWE11Ym1GdFpYMWNJaUJwYm1sMGFXRnNhWHBsWkNCemRXTmpaWE56Wm5Wc2JIa3VZQ2s3WEc1Y2RGeDBYSFJjZEZ4MFkyOXNiR1ZqZEdsdmJrUmxabVZ5Y21Wa0xuSmxjMjlzZG1Vb0tUdGNibHgwWEhSY2RGeDBYSFJqYkdWaGNsUnBiV1Z2ZFhRb2RHbHRaVzkxZENrN1hHNWNkRngwWEhSY2RIMDdYRzVjZEZ4MFhIUmNkRnh1WEhSY2RGeDBYSFF2THlCTWIyRmtJSFJvWlNCdGIyUjFiR1VnWkdGMFlTQmlaV1p2Y21VZ2RHaGxJRzF2WkhWc1pTQnBjeUJzYjJGa1pXUXVYRzVjZEZ4MFhIUmNkSFJvYVhNdVgyeHZZV1JOYjJSMWJHVkVZWFJoS0dsdWMzUmhibU5sS1Z4dVhIUmNkRngwWEhSY2RDNWtiMjVsS0NncElEMCtJSHRjYmx4MFhIUmNkRngwWEhSY2RDOHZJRkpsYW1WamRDQjBhR1VnWTI5c2JHVmpkR2x2YmtSbFptVnljbVZrSUdsbUlIUm9aU0J0YjJSMWJHVWdhWE51SjNRZ2FXNXBkR2xoYkdsNlpXUWdZV1owWlhJZ01UQWdjMlZqYjI1a2N5NWNibHgwWEhSY2RGeDBYSFJjZEhScGJXVnZkWFFnUFNCelpYUlVhVzFsYjNWMEtDZ3BJRDArSUh0Y2JseDBYSFJjZEZ4MFhIUmNkRngwYW5ObExtTnZjbVV1WkdWaWRXY3VkMkZ5YmlnblRXOWtkV3hsSUhkaGN5QnViM1FnYVc1cGRHbGhiR2w2WldRZ1lXWjBaWElnTVRBZ2MyVmpiMjVrY3lFZ0xTMGdKeUFySUhSb2FYTXVibUZ0WlNrN1hHNWNkRngwWEhSY2RGeDBYSFJjZEdOdmJHeGxZM1JwYjI1RVpXWmxjbkpsWkM1eVpXcGxZM1FvS1R0Y2JseDBYSFJjZEZ4MFhIUmNkSDBzSURFd01EQXdLVHRjYmx4MFhIUmNkRngwWEhSY2RGeHVYSFJjZEZ4MFhIUmNkRngwYVc1emRHRnVZMlV1YVc1cGRDaGtiMjVsS1R0Y2JseDBYSFJjZEZ4MFhIUjlLVnh1WEhSY2RGeDBYSFJjZEM1bVlXbHNLQ2hsY25KdmNpa2dQVDRnZTF4dVhIUmNkRngwWEhSY2RGeDBZMjlzYkdWamRHbHZia1JsWm1WeWNtVmtMbkpsYW1WamRDZ3BPMXh1WEhSY2RGeDBYSFJjZEZ4MGFuTmxMbU52Y21VdVpHVmlkV2N1WlhKeWIzSW9KME52ZFd4a0lHNXZkQ0JzYjJGa0lHMXZaSFZzWlZ4Y0ozTWdiV1YwWVNCa1lYUmhMaWNzSUdWeWNtOXlLVHRjYmx4MFhIUmNkRngwWEhSOUtUdGNibHgwWEhSY2RIMGdZMkYwWTJnZ0tHVjRZMlZ3ZEdsdmJpa2dlMXh1WEhSY2RGeDBYSFJqYjJ4c1pXTjBhVzl1UkdWbVpYSnlaV1F1Y21WcVpXTjBLQ2s3WEc1Y2RGeDBYSFJjZEdwelpTNWpiM0psTG1SbFluVm5MbVZ5Y205eUtHQkRZVzV1YjNRZ2FXNXBkR2xoYkdsNlpTQnRiMlIxYkdVZ1hDSWtlM1JvYVhNdWJtRnRaWDFjSWk1Z0xDQmxlR05sY0hScGIyNHBPMXh1WEhSY2RGeDBYSFFrS0hkcGJtUnZkeWt1ZEhKcFoyZGxjaWduWlhKeWIzSW5MQ0JiWlhoalpYQjBhVzl1WFNrN0lDOHZJRWx1Wm05eWJTQjBhR1VnWlc1bmFXNWxJR0ZpYjNWMElIUm9aU0JsZUdObGNIUnBiMjR1WEc1Y2RGeDBYSFI5WEc1Y2RGeDBYSFJjYmx4MFhIUmNkSEpsZEhWeWJpQmpiMnhzWldOMGFXOXVSR1ZtWlhKeVpXUXVjSEp2YldselpTZ3BPMXh1WEhSY2RIMWNibHgwWEhSY2JseDBYSFF2S2lwY2JseDBYSFFnS2lCUVlYSnpaU0IwYUdVZ2JXOWtkV3hsSUdSaGRHRWdZWFIwY21saWRYUmxjeTVjYmx4MFhIUWdLbHh1WEhSY2RDQXFJRUJ5WlhSMWNtNGdlMDlpYW1WamRIMGdVbVYwZFhKdWN5QmhiaUJ2WW1wbFkzUWdkR2hoZENCamIyNTBZV2x1Y3lCMGFHVWdaR0YwWVNCdlppQjBhR1VnYlc5a2RXeGxMbHh1WEhSY2RDQXFYRzVjZEZ4MElDb2dRSEJ5YVhaaGRHVmNibHgwWEhRZ0tpOWNibHgwWEhSZloyVjBUVzlrZFd4bFJHRjBZU2dwSUh0Y2JseDBYSFJjZEdOdmJuTjBJR1JoZEdFZ1BTQjdmVHRjYmx4MFhIUmNkRnh1WEhSY2RGeDBKQzVsWVdOb0tIUm9hWE11SkdWc1pXMWxiblF1WkdGMFlTZ3BMQ0FvYm1GdFpTd2dkbUZzZFdVcElEMCtJSHRjYmx4MFhIUmNkRngwYVdZZ0tHNWhiV1V1YVc1a1pYaFBaaWgwYUdsekxtNWhiV1VwSUQwOVBTQXdJSHg4SUc1aGJXVXVhVzVrWlhoUFppaDBhR2x6TG01aGJXVXVkRzlNYjNkbGNrTmhjMlVvS1NrZ1BUMDlJREFwSUh0Y2JseDBYSFJjZEZ4MFhIUnNaWFFnYTJWNUlEMGdibUZ0WlM1emRXSnpkSElvZEdocGN5NXVZVzFsTG14bGJtZDBhQ2s3WEc1Y2RGeDBYSFJjZEZ4MGEyVjVJRDBnYTJWNUxuTjFZbk4wY2lnd0xDQXhLUzUwYjB4dmQyVnlRMkZ6WlNncElDc2dhMlY1TG5OMVluTjBjaWd4S1R0Y2JseDBYSFJjZEZ4MFhIUmtZWFJoVzJ0bGVWMGdQU0IyWVd4MVpUdGNibHgwWEhSY2RGeDBYSFF2THlCU1pXMXZkbVVnWkdGMFlTQmhkSFJ5YVdKMWRHVWdabkp2YlNCbGJHVnRaVzUwSUNoellXNXBkR2x6WlNCallXMWxiQ0JqWVhObElHWnBjbk4wS1M1Y2JseDBYSFJjZEZ4MFhIUmpiMjV6ZENCellXNXBkR2x6WldSTFpYa2dQU0JyWlhrdWNtVndiR0ZqWlNndktGdGhMWHBkS1NoYlFTMWFYU2t2Wnl3Z0p5UXhMU1F5SnlrdWRHOU1iM2RsY2tOaGMyVW9LVHRjYmx4MFhIUmNkRngwWEhSMGFHbHpMaVJsYkdWdFpXNTBMbkpsYlc5MlpVRjBkSElvWUdSaGRHRXRKSHQwYUdsekxtNWhiV1Y5TFNSN2MyRnVhWFJwYzJWa1MyVjVmV0FwTzF4dVhIUmNkRngwWEhSOVhHNWNkRngwWEhSOUtUdGNibHgwWEhSY2RGeHVYSFJjZEZ4MGNtVjBkWEp1SUdSaGRHRTdYRzVjZEZ4MGZWeHVYSFJjZEZ4dVhIUmNkQzhxS2x4dVhIUmNkQ0FxSUUxdlpIVnNaWE1nY21WMGRYSnVJRzlpYW1WamRITWdkMmhwWTJnZ2JXbG5hSFFnWTI5dWRHRnBiaUJ5WlhGMWFYSmxiV1Z1ZEhNdVhHNWNkRngwSUNwY2JseDBYSFFnS2lCQWNHRnlZVzBnZTA5aWFtVmpkSDBnYVc1emRHRnVZMlVnVFc5a2RXeGxJR2x1YzNSaGJtTmxJRzlpYW1WamRDNWNibHgwWEhRZ0tseHVYSFJjZENBcUlFQnlaWFIxY200Z2UwOWlhbVZqZEgwZ1VtVjBkWEp1Y3lCaElIQnliMjFwYzJVZ2IySnFaV04wSUhSb1lYUWdkMmxzYkNCaVpTQnlaWE52YkhabFpDQjNhR1Z1SUhSb1pTQmtZWFJoSUdGeVpTQm1aWFJqYUdWa0xseHVYSFJjZENBcVhHNWNkRngwSUNvZ1FIQnlhWFpoZEdWY2JseDBYSFFnS2k5Y2JseDBYSFJmYkc5aFpFMXZaSFZzWlVSaGRHRW9hVzV6ZEdGdVkyVXBJSHRjYmx4MFhIUmNkR052Ym5OMElHUmxabVZ5Y21Wa0lEMGdKQzVFWldabGNuSmxaQ2dwTzF4dVhIUmNkRngwWTI5dWMzUWdaR1ZtWlhKeVpXUkRiMnhzWldOMGFXOXVJRDBnVzEwN1hHNWNkRngwWEhSY2JseDBYSFJjZEhSeWVTQjdYRzVjZEZ4MFhIUmNkR2xtSUNocGJuTjBZVzVqWlM1dGIyUmxiQ2tnZTF4dVhIUmNkRngwWEhSY2RDUXVaV0ZqYUNocGJuTjBZVzVqWlM1dGIyUmxiQ3dnWm5WdVkzUnBiMjRvYVc1a1pYZ3NJSFZ5YkNrZ2UxeHVYSFJjZEZ4MFhIUmNkRngwWTI5dWMzUWdiVzlrWld4RVpXWmxjbkpsWkNBOUlDUXVSR1ZtWlhKeVpXUW9LVHRjYmx4MFhIUmNkRngwWEhSY2RHUmxabVZ5Y21Wa1EyOXNiR1ZqZEdsdmJpNXdkWE5vS0cxdlpHVnNSR1ZtWlhKeVpXUXBPMXh1WEhSY2RGeDBYSFJjZEZ4MEpDNW5aWFJLVTA5T0tIVnliQ2xjYmx4MFhIUmNkRngwWEhSY2RGeDBMbVJ2Ym1Vb0tISmxjM0J2Ym5ObEtTQTlQaUI3WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwYVc1emRHRnVZMlV1Ylc5a1pXeGJhVzVrWlhoZElEMGdjbVZ6Y0c5dWMyVTdYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBiVzlrWld4RVpXWmxjbkpsWkM1eVpYTnZiSFpsS0hKbGMzQnZibk5sS1R0Y2JseDBYSFJjZEZ4MFhIUmNkRngwZlNsY2JseDBYSFJjZEZ4MFhIUmNkRngwTG1aaGFXd29LR1Z5Y205eUtTQTlQaUI3WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwYlc5a1pXeEVaV1psY25KbFpDNXlaV3BsWTNRb1pYSnliM0lwTzF4dVhIUmNkRngwWEhSY2RGeDBYSFI5S1R0Y2JseDBYSFJjZEZ4MFhIUjlLVHRjYmx4MFhIUmNkRngwZlZ4dVhIUmNkRngwWEhSY2JseDBYSFJjZEZ4MGFXWWdLR2x1YzNSaGJtTmxMblpwWlhjcElIdGNibHgwWEhSY2RGeDBYSFFrTG1WaFkyZ29hVzV6ZEdGdVkyVXVkbWxsZHl3Z1puVnVZM1JwYjI0b2FXNWtaWGdzSUhWeWJDa2dlMXh1WEhSY2RGeDBYSFJjZEZ4MFkyOXVjM1FnZG1sbGQwUmxabVZ5Y21Wa0lEMGdKQzVFWldabGNuSmxaQ2dwTzF4dVhIUmNkRngwWEhSY2RGeDBaR1ZtWlhKeVpXUkRiMnhzWldOMGFXOXVMbkIxYzJnb2RtbGxkMFJsWm1WeWNtVmtLVHRjYmx4MFhIUmNkRngwWEhSY2RDUXVaMlYwS0hWeWJDbGNibHgwWEhSY2RGeDBYSFJjZEZ4MExtUnZibVVvS0hKbGMzQnZibk5sS1NBOVBpQjdYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBhVzV6ZEdGdVkyVXVkbWxsZDF0cGJtUmxlRjBnUFNCeVpYTndiMjV6WlR0Y2JseDBYSFJjZEZ4MFhIUmNkRngwWEhSMmFXVjNSR1ZtWlhKeVpXUXVjbVZ6YjJ4MlpTaHlaWE53YjI1elpTazdYRzVjZEZ4MFhIUmNkRngwWEhSY2RIMHBYRzVjZEZ4MFhIUmNkRngwWEhSY2RDNW1ZV2xzS0NobGNuSnZjaWtnUFQ0Z2UxeHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RIWnBaWGRFWldabGNuSmxaQzV5WldwbFkzUW9aWEp5YjNJcE8xeHVYSFJjZEZ4MFhIUmNkRngwWEhSOUtUdGNibHgwWEhSY2RGeDBYSFI5S1R0Y2JseDBYSFJjZEZ4MGZWeHVYSFJjZEZ4MFhIUmNibHgwWEhSY2RGeDBhV1lnS0dsdWMzUmhibU5sTG1KcGJtUnBibWR6S1NCN1hHNWNkRngwWEhSY2RGeDBabTl5SUNoc1pYUWdibUZ0WlNCcGJpQnBibk4wWVc1alpTNWlhVzVrYVc1bmN5a2dlMXh1WEhSY2RGeDBYSFJjZEZ4MFkyOXVjM1FnSkdWc1pXMWxiblFnUFNCcGJuTjBZVzVqWlM1aWFXNWthVzVuYzF0dVlXMWxYVHRjYmx4MFhIUmNkRngwWEhSY2RHbHVjM1JoYm1ObExtSnBibVJwYm1kelcyNWhiV1ZkSUQwZ2JtVjNJR3B6WlM1amIyNXpkSEoxWTNSdmNuTXVSR0YwWVVKcGJtUnBibWNvYm1GdFpTd2dKR1ZzWlcxbGJuUXBPMXh1WEhSY2RGeDBYSFJjZEgxY2JseDBYSFJjZEZ4MGZWeHVYSFJjZEZ4MFhIUmNibHgwWEhSY2RGeDBKQzUzYUdWdUxtRndjR3g1S0hWdVpHVm1hVzVsWkN3Z1pHVm1aWEp5WldSRGIyeHNaV04wYVc5dUtWeHVYSFJjZEZ4MFhIUmNkQzVrYjI1bEtHUmxabVZ5Y21Wa0xuSmxjMjlzZG1VcFhHNWNkRngwWEhSY2RGeDBMbVpoYVd3b0tHVnljbTl5S1NBOVBpQjdYRzVjZEZ4MFhIUmNkRngwWEhSa1pXWmxjbkpsWkM1eVpXcGxZM1FvYm1WM0lFVnljbTl5S0dCRFlXNXViM1FnYkc5aFpDQmtZWFJoSUdadmNpQnRiMlIxYkdVZ1hDSWtlMmx1YzNSaGJtTmxMbTVoYldWOVhDSXVZQ3dnWlhKeWIzSXBLVHRjYmx4MFhIUmNkRngwWEhSOUtUdGNibHgwWEhSY2RIMGdZMkYwWTJnZ0tHVjRZMlZ3ZEdsdmJpa2dlMXh1WEhSY2RGeDBYSFJrWldabGNuSmxaQzV5WldwbFkzUW9aWGhqWlhCMGFXOXVLVHRjYmx4MFhIUmNkRngwYW5ObExtTnZjbVV1WkdWaWRXY3VaWEp5YjNJb1lFTmhibTV2ZENCd2NtVnNiMkZrSUcxdlpIVnNaU0JrWVhSaElHWnZjaUJjSWlSN2RHaHBjeTV1WVcxbGZWd2lMbUFzSUdWNFkyVndkR2x2YmlrN1hHNWNkRngwWEhSY2RDUW9kMmx1Wkc5M0tTNTBjbWxuWjJWeUtDZGxjbkp2Y2ljc0lGdGxlR05sY0hScGIyNWRLVHNnTHk4Z1NXNW1iM0p0SUhSb1pTQmxibWRwYm1VZ1lXSnZkWFFnZEdobElHVjRZMlZ3ZEdsdmJpNWNibHgwWEhSY2RIMWNibHgwWEhSY2RGeHVYSFJjZEZ4MGNtVjBkWEp1SUdSbFptVnljbVZrTG5CeWIyMXBjMlVvS1R0Y2JseDBYSFI5WEc1Y2RIMWNibHgwWEc1Y2RHcHpaUzVqYjI1emRISjFZM1J2Y25NdVRXOWtkV3hsSUQwZ1RXOWtkV3hsTzF4dWZTa29LVHRjYmlJc0lpOHFJQzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRYRzRnYm1GdFpYTndZV05sTG1weklESXdNVFl0TURVdE1UZGNiaUJIWVcxaWFXOGdSMjFpU0Z4dUlHaDBkSEE2THk5M2QzY3VaMkZ0WW1sdkxtUmxYRzRnUTI5d2VYSnBaMmgwSUNoaktTQXlNREUySUVkaGJXSnBieUJIYldKSVhHNGdVbVZzWldGelpXUWdkVzVrWlhJZ2RHaGxJRWRPVlNCSFpXNWxjbUZzSUZCMVlteHBZeUJNYVdObGJuTmxJQ2hXWlhKemFXOXVJRElwWEc0Z1cyaDBkSEE2THk5M2QzY3VaMjUxTG05eVp5OXNhV05sYm5ObGN5OW5jR3d0TWk0d0xtaDBiV3hkWEc0Z0xTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMxY2JpQXFMMXh1WEc0b1puVnVZM1JwYjI0b0tTQjdYRzVjZEZ4dVhIUW5kWE5sSUhOMGNtbGpkQ2M3WEc1Y2RGeHVYSFF2S2lwY2JseDBJQ29nUTJ4aGMzTWdUbUZ0WlhOd1lXTmxYRzVjZENBcVhHNWNkQ0FxSUZSb2FYTWdZMnhoYzNNZ2FYTWdkWE5sWkNCMGJ5Qm9ZVzVrYkdVZ2JYVnNkR2x3YkdVZ1kyOXNiR1ZqZEdsdmJuTWdiMllnYlc5a2RXeGxjeTRnUlhabGNua2dibUZ0WlhOd1lXTmxJR2hoY3lCcGRITWdiM2R1SUhOdmRYSmpaU0JWVWt3Z1hHNWNkQ0FxSUdadmNpQnNiMkZrYVc1bklIUm9aU0JrWVhSaExpQlVhR0YwSUcxbFlXNXpJSFJvWVhRZ1NsTkZJR05oYmlCc2IyRmtJRzF2WkhWc1pYTWdabkp2YlNCdGRXeDBhWEJzWlNCd2JHRmpaWE1nWVhRZ2RHaGxJSE5oYldVZ2RHbHRaUzRnWEc1Y2RDQXFYRzVjZENBcUlFQmpiR0Z6Y3lCS1UwVXZRMjl1YzNSeWRXTjBiM0p6TDA1aGJXVnpjR0ZqWlZ4dVhIUWdLaTljYmx4MFkyeGhjM01nVG1GdFpYTndZV05sSUh0Y2JseDBYSFF2S2lwY2JseDBYSFFnS2lCRGJHRnpjeUJEYjI1emRISjFZM1J2Y2x4dVhIUmNkQ0FxWEc1Y2RGeDBJQ29nUUhCaGNtRnRJSHRUZEhKcGJtZDlJRzVoYldVZ1ZHaGxJRzVoYldWemNHRmpaU0J1WVcxbElHMTFjM1FnWW1VZ2RXNXBjWFZsSUhkcGRHaHBiaUIwYUdVZ1lYQndMbHh1WEhSY2RDQXFJRUJ3WVhKaGJTQjdVM1J5YVc1bmZTQnpiM1Z5WTJVZ1EyOXRjR3hsZEdVZ1ZWSk1JSFJ2SUhSb1pTQnVZVzFsYzNCaFkyVWdiVzlrZFd4bGN5QmthWEpsWTNSdmNua2dLSGRwZEdodmRYUWdkSEpoYVd4cGJtY2djMnhoYzJncExseHVYSFJjZENBcUlFQndZWEpoYlNCN1FYSnlZWGw5SUdOdmJHeGxZM1JwYjI1eklFTnZiblJoYVc1eklHTnZiR3hsWTNScGIyNGdhVzV6ZEdGdVkyVnpJSFJ2SUdKbElHbHVZMngxWkdWa0lHbHVJSFJvWlNCdVlXMWxjM0JoWTJVdVhHNWNkRngwSUNvdlhHNWNkRngwWTI5dWMzUnlkV04wYjNJb2JtRnRaU3dnYzI5MWNtTmxMQ0JqYjJ4c1pXTjBhVzl1Y3lrZ2UxeHVYSFJjZEZ4MGRHaHBjeTV1WVcxbElEMGdibUZ0WlR0Y2JseDBYSFJjZEhSb2FYTXVjMjkxY21ObElEMGdjMjkxY21ObE8xeHVYSFJjZEZ4MGRHaHBjeTVqYjJ4c1pXTjBhVzl1Y3lBOUlHTnZiR3hsWTNScGIyNXpPeUF2THlCamIyNTBZV2x1Y3lCMGFHVWdaR1ZtWVhWc2RDQnBibk4wWVc1alpYTWdJQ0JjZEZ4MFhHNWNkRngwZlZ4dVhIUmNkRnh1WEhSY2RDOHFLbHh1WEhSY2RDQXFJRWx1YVhScFlXeHBlbVVnZEdobElHNWhiV1Z6Y0dGalpTQmpiMnhzWldOMGFXOXVjeTVjYmx4MFhIUWdLbHh1WEhSY2RDQXFJRlJvYVhNZ2JXVjBhRzlrSUhkcGJHd2dZM0psWVhSbElHNWxkeUJqYjJ4c1pXTjBhVzl1SUdsdWMzUmhibU5sY3lCaVlYTmxaQ0JwYmlCMGFHVWdiM0pwWjJsdVlXd2diMjVsY3k1Y2JseDBYSFFnS2x4dVhIUmNkQ0FxSUVCeVpYUjFjbTRnZTJwUmRXVnllUzVRY205dGFYTmxmU0JTWlhSMWNtNXpJR0VnY0hKdmJXbHpaU0IwYUdGMElIZHBiR3dnWW1VZ2NtVnpiMngyWldRZ2IyNWpaU0JsZG1WeWVTQnVZVzFsYzNCaFkyVWdZMjlzYkdWamRHbHZibHh1WEhSY2RDQXFJR2x6SUhKbGMyOXNkbVZrTGx4dVhIUmNkQ0FxTDF4dVhIUmNkR2x1YVhRb0tTQjdYRzVjZEZ4MFhIUmpiMjV6ZENCa1pXWmxjbkpsWkVOdmJHeGxZM1JwYjI0Z1BTQmJYVHRjYmx4MFhIUmNkRnh1WEhSY2RGeDBabTl5SUNoc1pYUWdZMjlzYkdWamRHbHZiaUJ2WmlCMGFHbHpMbU52Ykd4bFkzUnBiMjV6S1NCN1hHNWNkRngwWEhSY2RIUm9hWE5iWTI5c2JHVmpkR2x2Ymk1dVlXMWxYU0E5SUc1bGR5QnFjMlV1WTI5dWMzUnlkV04wYjNKekxrTnZiR3hsWTNScGIyNG9ZMjlzYkdWamRHbHZiaTV1WVcxbExDQmpiMnhzWldOMGFXOXVMbUYwZEhKcFluVjBaU3dnZEdocGN5azdYRzVjZEZ4MFhIUmNkR052Ym5OMElHUmxabVZ5Y21Wa0lEMGdkR2hwYzF0amIyeHNaV04wYVc5dUxtNWhiV1ZkTG1sdWFYUW9LVHRjYmx4MFhIUmNkRngwWkdWbVpYSnlaV1JEYjJ4c1pXTjBhVzl1TG5CMWMyZ29aR1ZtWlhKeVpXUXBPMXh1WEhSY2RGeDBmVnh1WEhSY2RGeDBYRzVjZEZ4MFhIUnlaWFIxY200Z1pHVm1aWEp5WldSRGIyeHNaV04wYVc5dUxteGxibWQwYUNBL0lDUXVkMmhsYmk1aGNIQnNlU2gxYm1SbFptbHVaV1FzSUdSbFptVnljbVZrUTI5c2JHVmpkR2x2YmlrdWNISnZiV2x6WlNncElEb2dKQzVFWldabGNuSmxaQ2dwTG5KbGMyOXNkbVVvS1R0Y2JseDBYSFI5WEc1Y2RIMWNibHgwWEc1Y2RHcHpaUzVqYjI1emRISjFZM1J2Y25NdVRtRnRaWE53WVdObElEMGdUbUZ0WlhOd1lXTmxPMXh1ZlNrb0tUdGNiaUlzSWk4cUlDMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdFhHNGdZV0p2ZFhRdWFuTWdNakF4Tmkwd09TMHdPRnh1SUVkaGJXSnBieUJIYldKSVhHNGdhSFIwY0RvdkwzZDNkeTVuWVcxaWFXOHVaR1ZjYmlCRGIzQjVjbWxuYUhRZ0tHTXBJREl3TVRZZ1IyRnRZbWx2SUVkdFlraGNiaUJTWld4bFlYTmxaQ0IxYm1SbGNpQjBhR1VnUjA1VklFZGxibVZ5WVd3Z1VIVmliR2xqSUV4cFkyVnVjMlVnS0ZabGNuTnBiMjRnTWlsY2JpQmJhSFIwY0RvdkwzZDNkeTVuYm5VdWIzSm5MMnhwWTJWdWMyVnpMMmR3YkMweUxqQXVhSFJ0YkYxY2JpQXRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExWeHVJQ292WEc1Y2JpOHFLbHh1SUNvZ1NsTkZJRWx1Wm05eWJXRjBhVzl1SUUxdlpIVnNaVnh1SUNvZ1hHNGdLaUJGZUdWamRYUmxJSFJvWlNCZ2FuTmxMbUZpYjNWMEtDbGdJR052YlcxaGJtUWdZVzVrSUhsdmRTQjNhV3hzSUdkbGRDQmhJRzVsZHlCc2IyY2daVzUwY25rZ2FXNGdkR2hsWEc0Z0tpQmpiMjV6YjJ4bElIZHBkR2dnYVc1bWJ5QmhZbTkxZENCMGFHVWdaVzVuYVc1bExpQlVhR1VnWENKaFltOTFkRndpSUcxbGRHaHZaQ0JwY3lCdmJteDVJR0YyWVdsc1lXSnNaU0JwYmx4dUlDb2dkR2hsSUZ3aVpHVjJaV3h2Y0cxbGJuUmNJaUJsYm5acGNtOXViV1Z1ZENCdlppQjBhR1VnWlc1bmFXNWxMbHh1SUNwY2JpQXFJRUJ0YjJSMWJHVWdTbE5GTDBOdmNtVXZZV0p2ZFhSY2JpQXFMMXh1Wkc5amRXMWxiblF1WVdSa1JYWmxiblJNYVhOMFpXNWxjaWduUkU5TlEyOXVkR1Z1ZEV4dllXUmxaQ2NzSUdaMWJtTjBhVzl1S0NrZ2UxeHVYRzVjZENkMWMyVWdjM1J5YVdOMEp6dGNibHh1WEhScFppQW9hbk5sTG1OdmNtVXVZMjl1Wm1sbkxtZGxkQ2duWlc1MmFYSnZibTFsYm5RbktTQTlQVDBnSjNCeWIyUjFZM1JwYjI0bktTQjdYRzVjZEZ4MGNtVjBkWEp1TzF4dVhIUjlYRzVjYmx4MGFuTmxMbUZpYjNWMElEMGdablZ1WTNScGIyNGdLQ2tnZTF4dVhIUmNkR052Ym5OMElHbHVabThnUFNCZ1hHNWNkRngwWEhSS1V5QkZUa2RKVGtVZ2RpUjdhbk5sTG1OdmNtVXVZMjl1Wm1sbkxtZGxkQ2duZG1WeWMybHZiaWNwZlNEQ3FTQkhRVTFDU1U4Z1IwMUNTRnh1WEhSY2RGeDBMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1WEhSY2RGeDBWR2hsSUVwVElFVnVaMmx1WlNCbGJtRmliR1Z6SUdSbGRtVnNiM0JsY25NZ2RHOGdiRzloWkNCaGRYUnZiV0YwYVdOaGJHeDVJSE50WVd4c0lIQnBaV05sY3lCdlppQnFZWFpoYzJOeWFYQjBJR052WkdVZ1lubGNibHgwWEhSY2RIQnNZV05wYm1jZ2MzQmxZMmxtYVdNZ1pHRjBZU0JoZEhSeWFXSjFkR1Z6SUhSdklIUm9aU0JJVkUxTUlHMWhjbXQxY0NCdlppQmhJSEJoWjJVdUlFbDBJSGRoY3lCaWRXbHNkQ0IzYVhSb0lHMXZaSFZzWVhKcGRIbGNibHgwWEhSY2RHbHVJRzFwYm1RZ2MyOGdkR2hoZENCdGIyUjFiR1Z6SUdOaGJpQmlaU0J5WlhWelpXUWdhVzUwYnlCdGRXeDBhWEJzWlNCd2JHRmpaWE1nZDJsMGFHOTFkQ0JsZUhSeVlTQmxabVp2Y25RdUlGUm9aU0JsYm1kcGJtVmNibHgwWEhSY2RHTnZiblJoYVc1eklHNWhiV1Z6Y0dGalpYTWdkMmhwWTJnZ1kyOXVkR0ZwYmlCamIyeHNaV04wYVc5dWN5QnZaaUJ0YjJSMWJHVnpMQ0JsWVdOb0lHOXVaU0J2WmlCM2FHOXRJSE5sY25abElHRWdaR2xtWm1WeVpXNTBYRzVjZEZ4MFhIUm5aVzVsY21saklIQjFjbkJ2YzJVdVhHNWNkRngwWEhSV2FYTnBkQ0JvZEhSd09pOHZaR1YyWld4dmNHVnljeTVuWVcxaWFXOHVaR1VnWm05eUlHTnZiWEJzWlhSbElISmxabVZ5Wlc1alpTQnZaaUIwYUdVZ1NsTWdSVzVuYVc1bExseHVYSFJjZEZ4MFhHNWNkRngwWEhSR1FVeE1Ra0ZEU3lCSlRrWlBVazFCVkVsUFRseHVYSFJjZEZ4MExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExWeHVYSFJjZEZ4MFUybHVZMlVnZEdobElHVnVaMmx1WlNCamIyUmxJR0psWTI5dFpYTWdZbWxuWjJWeUlIUm9aWEpsSUdGeVpTQnpaV04wYVc5dWN5QjBhR0YwSUc1bFpXUWdkRzhnWW1VZ2NtVm1ZV04wYjNKbFpDQnBiaUJ2Y21SbGNseHVYSFJjZEZ4MGRHOGdZbVZqYjIxbElHMXZjbVVnWm14bGVHbGliR1V1SUVsdUlHMXZjM1FnWTJGelpYTWdZU0IzWVhKdWFXNW5JR3h2WnlCM2FXeHNJR0psSUdScGMzQnNZWGxsWkNCaGRDQjBhR1VnWW5KdmQzTmxjbHhjSjNNZ1kyOXVjMjlzWlZ4dVhIUmNkRngwZDJobGJtVjJaWElnZEdobGNtVWdhWE1nWVNCMWMyVWdiMllnWVNCa1pYQnlaV05oZEdWa0lHWjFibU4wYVc5dUxpQkNaV3h2ZHlCMGFHVnlaU0JwY3lCaElIRjFhV05ySUd4cGMzUWdiMllnWm1Gc2JHSmhZMnNnYzNWd2NHOXlkRnh1WEhSY2RGeDBkR2hoZENCM2FXeHNJR0psSUhKbGJXOTJaV1FnYVc0Z2RHaGxJR1oxZEhWeVpTQjJaWEp6YVc5dWN5QnZaaUIwYUdVZ1pXNW5hVzVsTGx4dVhIUmNkRngwWEc1Y2RGeDBYSFF4TGlCVWFHVWdiV0ZwYmlCbGJtZHBibVVnYjJKcVpXTjBJSGRoY3lCeVpXNWhiV1ZrSUdaeWIyMGdYQ0puZUZ3aUlIUnZJRndpYW5ObFhDSWdkMmhwWTJnZ2MzUmhibVJ6SUdadmNpQjBhR1VnU21GMllWTmpjbWx3ZENCRmJtZHBibVV1WEc1Y2RGeDBYSFF5TGlCVWFHVWdYQ0puZUM1c2FXSmNJaUJ2WW1wbFkzUWdhWE1nY21WdGIzWmxaQ0JoWm5SbGNpQmhJR3h2Ym1jZ1pHVndjbVZqWVhScGIyNGdjR1Z5YVc5a0xpQlpiM1VnYzJodmRXeGtJSFZ3WkdGMFpTQjBhR1VnYlc5a2RXeGxjeUJjYmx4MFhIUmNkQ0FnSUhSb1lYUWdZMjl1ZEdGcGJtVmtJR05oYkd4eklIUnZJSFJvWlNCbWRXNWpkR2x2Ym5NZ2IyWWdkR2hwY3lCdlltcGxZM1F1WEc1Y2RGeDBYSFF6TGlCVWFHVWdaM2d1UEdOdmJHeGxZM1JwYjI0dGJtRnRaVDR1Y21WbmFYTjBaWElnWm5WdVkzUnBiMjRnYVhNZ1pHVndjbVZqWVhSbFpDQmllU0IyTVM0eUxDQjFjMlVnZEdobElGeHVYSFJjZEZ4MElDQWdQRzVoYldWemNHRmpaVDR1UEdOdmJHeGxZM1JwYjI0K0xtMXZaSFZzWlNncElHbHVjM1JsWVdRdVhHNWNkRngwWUR0Y2JseDBYSFJjYmx4MFhIUnFjMlV1WTI5eVpTNWtaV0oxWnk1cGJtWnZLR2x1Wm04cE8xeHVYSFI5TzF4dWZTazdYRzRpTENJdktpQXRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExWeHVJR052Ym1acFp5NXFjeUF5TURFM0xUQXpMVEkyWEc0Z1IyRnRZbWx2SUVkdFlraGNiaUJvZEhSd09pOHZkM2QzTG1kaGJXSnBieTVrWlZ4dUlFTnZjSGx5YVdkb2RDQW9ZeWtnTWpBeE55QkhZVzFpYVc4Z1IyMWlTRnh1SUZKbGJHVmhjMlZrSUhWdVpHVnlJSFJvWlNCSFRsVWdSMlZ1WlhKaGJDQlFkV0pzYVdNZ1RHbGpaVzV6WlNBb1ZtVnljMmx2YmlBeUtWeHVJRnRvZEhSd09pOHZkM2QzTG1kdWRTNXZjbWN2YkdsalpXNXpaWE12WjNCc0xUSXVNQzVvZEcxc1hWeHVJQzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRYRzRnS2k5Y2JseHVhbk5sTG1OdmNtVXVZMjl1Wm1sbklEMGdhbk5sTG1OdmNtVXVZMjl1Wm1sbklIeDhJSHQ5TzF4dVhHNHZLaXBjYmlBcUlFcFRSU0JEYjI1bWFXZDFjbUYwYVc5dUlFMXZaSFZzWlZ4dUlDcGNiaUFxSUU5dVkyVWdkR2hsSUdOdmJtWnBaeUJ2WW1wbFkzUWdhWE1nYVc1cGRHbGhiR2w2WldRZ2VXOTFJR05oYm01dmRDQmphR0Z1WjJVZ2FYUnpJSFpoYkhWbGN5NGdWR2hwY3lCcGN5QmtiMjVsSUdsdUlHOXlaR1Z5SUhSdlhHNGdLaUJ3Y21WMlpXNTBJSFZ1Y0d4bFlYTmhiblFnYzJsMGRXRjBhVzl1Y3lCM2FHVnlaU0J2Ym1VZ1kyOWtaU0J6WldOMGFXOXVJR05vWVc1blpYTWdZU0JqYjNKbElHTnZibVpwWnlCelpYUjBhVzVuSUhSb1lYUWdZV1ptWldOMGMxeHVJQ29nWVc1dmRHaGxjaUJqYjJSbElITmxZM1JwYjI0Z2FXNGdZU0IzWVhrZ2RHaGhkQ0JwY3lCb1lYSmtJSFJ2SUdScGMyTnZkbVZ5TGx4dUlDcGNiaUFxSUdCZ1lHcGhkbUZ6WTNKcGNIUmNiaUFxSUdOdmJuTjBJR0Z3Y0ZWeWJDQTlJR3B6WlM1amIzSmxMbU52Ym1acFp5NW5aWFFvSjJGd2NGVnliQ2NwTzF4dUlDb2dZR0JnWEc0Z0tseHVJQ29nUUcxdlpIVnNaU0JLVTBVdlEyOXlaUzlqYjI1bWFXZGNiaUFxTDF4dUtHWjFibU4wYVc5dUtHVjRjRzl5ZEhNcElIdGNibHgwWEc1Y2RDZDFjMlVnYzNSeWFXTjBKenRjYmx4MFhHNWNkQzh2SUMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFZ4dVhIUXZMeUJEVDA1R1NVZFZVa0ZVU1U5T0lGWkJURlZGVTF4dVhIUXZMeUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzFjYmx4MFhHNWNkR052Ym5OMElHTnZibVpwWnlBOUlIdGNibHgwWEhRdktpcGNibHgwWEhRZ0tpQkZibWRwYm1VZ1ZtVnljMmx2Ymx4dVhIUmNkQ0FxWEc1Y2RGeDBJQ29nUUhSNWNHVWdlMU4wY21sdVozMWNibHgwWEhRZ0tpOWNibHgwWEhSMlpYSnphVzl1T2lBbk1TNDFKeXhjYmx4MFhIUmNibHgwWEhRdktpcGNibHgwWEhRZ0tpQkJjSEFnVlZKTVhHNWNkRngwSUNwY2JseDBYSFFnS2lCbExtY3VJQ2RvZEhSd09pOHZZWEJ3TG1OdmJTZGNibHgwWEhRZ0tseHVYSFJjZENBcUlFQjBlWEJsSUh0VGRISnBibWQ5WEc1Y2RGeDBJQ292WEc1Y2RGeDBZWEJ3VlhKc09pQnVkV3hzTEZ4dVhIUmNkRnh1WEhSY2RDOHFLbHh1WEhSY2RDQXFJRk5vYjNBZ1ZWSk1YRzVjZEZ4MElDcGNibHgwWEhRZ0tpQmxMbWN1SUNkb2RIUndPaTh2YzJodmNDNWtaU2RjYmx4MFhIUWdLbHh1WEhSY2RDQXFJRUJrWlhCeVpXTmhkR1ZrSUZOcGJtTmxJSFl4TGpRc0lIVnpaU0JoY0hCVmNtd2dhVzV6ZEdWaFpDNWNibHgwWEhRZ0tseHVYSFJjZENBcUlFQjBlWEJsSUh0VGRISnBibWQ5WEc1Y2RGeDBJQ292WEc1Y2RGeDBjMmh2Y0ZWeWJEb2diblZzYkN4Y2JseDBYSFJjYmx4MFhIUXZLaXBjYmx4MFhIUWdLaUJCY0hBZ1ZtVnljMmx2Ymx4dVhIUmNkQ0FxWEc1Y2RGeDBJQ29nWlM1bkxpQW5NaTQzTGpNdU1DZGNibHgwWEhRZ0tseHVYSFJjZENBcUlFQjBlWEJsSUh0VGRISnBibWQ5WEc1Y2RGeDBJQ292WEc1Y2RGeDBZWEJ3Vm1WeWMybHZiam9nYm5Wc2JDeGNibHgwWEhSY2JseDBYSFF2S2lwY2JseDBYSFFnS2lCVGFHOXdJRlpsY25OcGIyNWNibHgwWEhRZ0tseHVYSFJjZENBcUlHVXVaeTRnSnpJdU55NHpMakFuWEc1Y2RGeDBJQ3BjYmx4MFhIUWdLaUJBWkdWd2NtVmpZWFJsWkNCVGFXNWpaU0F4TGpRc0lIVnpaU0JoY0hCV1pYSnphVzl1SUdsdWMzUmxZV1F1WEc1Y2RGeDBJQ3BjYmx4MFhIUWdLaUJBZEhsd1pTQjdVM1J5YVc1bmZWeHVYSFJjZENBcUwxeHVYSFJjZEhOb2IzQldaWEp6YVc5dU9pQnVkV3hzTEZ4dVhIUmNkRnh1WEhSY2RDOHFLbHh1WEhSY2RDQXFJRlZTVENCMGJ5QktVMFZ1WjJsdVpTQkVhWEpsWTNSdmNua3VYRzVjZEZ4MElDcGNibHgwWEhRZ0tpQmxMbWN1SUNkb2RIUndPaTh2WVhCd0xtTnZiUzlLVTBWdVoybHVaVnh1WEhSY2RDQXFYRzVjZEZ4MElDb2dRSFI1Y0dVZ2UxTjBjbWx1WjMxY2JseDBYSFFnS2k5Y2JseDBYSFJsYm1kcGJtVlZjbXc2SUc1MWJHd3NYRzVjZEZ4MFhHNWNkRngwTHlvcVhHNWNkRngwSUNvZ1JXNW5hVzVsSUVWdWRtbHliMjV0Wlc1MFhHNWNkRngwSUNwY2JseDBYSFFnS2lCRVpXWnBibVZ6SUhSb1pTQm1kVzVqZEdsdmJtRnNhWFI1SUc5bUlIUm9aU0JsYm1kcGJtVWdhVzRnYldGdWVTQnpaV04wYVc5dWN5NWNibHgwWEhRZ0tseHVYSFJjZENBcUlGWmhiSFZsY3pvZ0oyUmxkbVZzYjNCdFpXNTBKeXdnSjNCeWIyUjFZM1JwYjI0blhHNWNkRngwSUNwY2JseDBYSFFnS2lCQWRIbHdaU0I3VTNSeWFXNW5mVnh1WEhSY2RDQXFMMXh1WEhSY2RHVnVkbWx5YjI1dFpXNTBPaUFuY0hKdlpIVmpkR2x2Ymljc1hHNWNkRngwWEc1Y2RGeDBMeW9xWEc1Y2RGeDBJQ29nVkhKaGJuTnNZWFJwYjI1eklFOWlhbVZqZEZ4dVhIUmNkQ0FxWEc1Y2RGeDBJQ29nUTI5dWRHRnBibk1nZEdobElHeHZZV1JsWkNCMGNtRnVjMnhoZEdsdmJuTWdkRzhnWW1VZ2RYTmxaQ0IzYVhSb2FXNGdTbE5GYm1kcGJtVXVYRzVjZEZ4MElDcGNibHgwWEhRZ0tpQkFjMlZsSUdwelpTNWpiM0psTG14aGJtY2diMkpxWldOMFhHNWNkRngwSUNwY2JseDBYSFFnS2lCQWRIbHdaU0I3VDJKcVpXTjBmVnh1WEhSY2RDQXFMMXh1WEhSY2RIUnlZVzV6YkdGMGFXOXVjem9nZTMwc1hHNWNkRngwWEc1Y2RGeDBMeW9xWEc1Y2RGeDBJQ29nVFc5a2RXeGxJRU52Ykd4bFkzUnBiMjV6WEc1Y2RGeDBJQ3BjYmx4MFhIUWdLaUJRY205MmFXUmxJR0Z5Y21GNUlIZHBkR2dnZXlCdVlXMWxPaUFuSnl3Z1lYUjBjbWxpZFhSbE9pQW5KMzBnYjJKcVpXTjBjeUIwYUdGMElHUmxabWx1WlNCMGFHVWdZMjlzYkdWamRHbHZibk1nZEc4Z1ltVWdkWE5sWkNCM2FYUm9hVzVjYmx4MFhIUWdLaUIwYUdVZ1lYQndiR2xqWVhScGIyNHVYRzVjZEZ4MElDcGNibHgwWEhRZ0tpQkFkSGx3WlNCN1FYSnlZWGw5WEc1Y2RGeDBJQ292WEc1Y2RGeDBZMjlzYkdWamRHbHZibk02SUZ0ZExGeHVYSFJjZEZ4dVhIUmNkQzhxS2x4dVhIUmNkQ0FxSUVOMWNuSmxiblFnVEdGdVozVmhaMlVnUTI5a1pWeHVYSFJjZENBcVhHNWNkRngwSUNvZ1FIUjVjR1VnZTFOMGNtbHVaMzFjYmx4MFhIUWdLaTljYmx4MFhIUnNZVzVuZFdGblpVTnZaR1U2SUNka1pTY3NYRzVjZEZ4MFhHNWNkRngwTHlvcVhHNWNkRngwSUNvZ1UyVjBJSFJvWlNCa1pXSjFaeUJzWlhabGJDQjBieUJ2Ym1VZ2IyWWdkR2hsSUdadmJHeHZkMmx1WnpvZ0owUkZRbFZISnl3Z0owbE9SazhuTENBblRFOUhKeXdnSjFkQlVrNG5MQ0FuUlZKU1QxSW5MRnh1WEhSY2RDQXFJQ2RCVEVWU1ZDY3NJQ2RUU1V4RlRsUW5MbHh1WEhSY2RDQXFYRzVjZEZ4MElDb2dRSFI1Y0dVZ2UxTjBjbWx1WjMxY2JseDBYSFFnS2k5Y2JseDBYSFJrWldKMVp6b2dKMU5KVEVWT1ZDY3NYRzVjZEZ4MFhHNWNkRngwTHlvcVhHNWNkRngwSUNvZ1ZYTmxJR05oWTJobElHSjFjM1JwYm1jZ2RHVmphRzVwY1hWbElIZG9aVzRnYkc5aFpHbHVaeUJ0YjJSMWJHVnpMbHh1WEhSY2RDQXFYRzVjZEZ4MElDb2dRR1JsY0hKbFkyRjBaV1FnVTJsdVkyVWdkakV1TkZ4dVhIUmNkQ0FxSUZ4dVhIUmNkQ0FxSUVCelpXVWdhbk5sTG1OdmNtVXViVzlrZFd4bFgyeHZZV1JsY2lCdlltcGxZM1JjYmx4MFhIUWdLbHh1WEhSY2RDQXFJRUIwZVhCbElIdENiMjlzWldGdWZWeHVYSFJjZENBcUwxeHVYSFJjZEdOaFkyaGxRblZ6ZERvZ2RISjFaU3hjYmx4MFhIUmNibHgwWEhRdktpcGNibHgwWEhRZ0tpQlhhR1YwYUdWeUlIUm9aU0JqYkdsbGJuUWdhR0Z6SUdFZ2JXOWlhV3hsSUdsdWRHVnlabUZqWlM1Y2JseDBYSFFnS2x4dVhIUmNkQ0FxSUVCMGVYQmxJSHRDYjI5c1pXRnVmVnh1WEhSY2RDQXFMMXh1WEhSY2RHMXZZbWxzWlRvZ0tDOUJibVJ5YjJsa2ZIZGxZazlUZkdsUWFHOXVaWHhwVUdGa2ZHbFFiMlI4UW14aFkydENaWEp5ZVh4SlJVMXZZbWxzWlh4UGNHVnlZU0JOYVc1cEwya3VkR1Z6ZENodVlYWnBaMkYwYjNJdWRYTmxja0ZuWlc1MEtTa3NYRzVjZEZ4MFhHNWNkRngwTHlvcVhHNWNkRngwSUNvZ1YyaGxkR2hsY2lCMGFHVWdZMnhwWlc1MElITjFjSEJ2Y25SeklIUnZkV05vSUdWMlpXNTBjeTVjYmx4MFhIUWdLbHh1WEhSY2RDQXFJRUIwZVhCbElIdENiMjlzWldGdWZWeHVYSFJjZENBcUwxeHVYSFJjZEhSdmRXTm9PaUFvS0NkdmJuUnZkV05vYzNSaGNuUW5JR2x1SUhkcGJtUnZkeWtnZkh3Z2QybHVaRzkzTG05dWRHOTFZMmh6ZEdGeWRDQjhmQ0IzYVc1a2IzY3ViMjV0YzJkbGMzUjFjbVZqYUdGdVoyVXBJRDhnZEhKMVpTQTZJR1poYkhObExGeHVYSFJjZEZ4dVhIUmNkQzhxS2x4dVhIUmNkQ0FxSUZOd1pXTnBabmtnZEdobElIQmhkR2dnWm05eUlIUm9aU0JtYVd4bElHMWhibUZuWlhJdVhHNWNkRngwSUNwY2JseDBYSFFnS2lCQVpHVndjbVZqWVhSbFpDQlRhVzVqWlNCMk1TNDBYRzVjZEZ4MElDb2dYRzVjZEZ4MElDb2dRSFI1Y0dVZ2UxTjBjbWx1WjMxY2JseDBYSFFnS2k5Y2JseDBYSFJtYVd4bGJXRnVZV2RsY2pvZ0oybHVZMngxWkdWekwyTnJaV1JwZEc5eUwyWnBiR1Z0WVc1aFoyVnlMMmx1WkdWNExtaDBiV3duTEZ4dVhIUmNkRnh1WEhSY2RDOHFLbHh1WEhSY2RDQXFJRkJoWjJVZ2RHOXJaVzRnZEc4Z2FXNWpiSFZrWlNCcGJpQmxkbVZ5ZVNCQlNrRllJSEpsY1hWbGMzUXVYRzVjZEZ4MElDcGNibHgwWEhRZ0tpQlVhR1VnY0dGblpTQjBiMnRsYmlCcGN5QjFjMlZrSUhSdklHRjJiMmxrSUVOVFVrWWdZWFIwWVdOcmN5NGdTWFFnYlhWemRDQmlaU0J3Y205MmFXUmxaQ0JpZVNCMGFHVWdZbUZqYTJWdVpDQmhibVFnYVhRZ2QybHNiRnh1WEhSY2RDQXFJR0psSUhaaGJHbGtZWFJsWkNCMGFHVnlaUzVjYmx4MFhIUWdLbHh1WEhSY2RDQXFJRUIwZVhCbElIdFRkSEpwYm1kOVhHNWNkRngwSUNvdlhHNWNkRngwY0dGblpWUnZhMlZ1T2lBbkp5eGNibHgwWEhSY2JseDBYSFF2S2lwY2JseDBYSFFnS2lCRFlXTm9aU0JVYjJ0bGJpQlRkSEpwYm1jZ1hHNWNkRngwSUNvZ1hHNWNkRngwSUNvZ1ZHaHBjeUJqYjI1bWFXZDFjbUYwYVc5dUlIWmhiSFZsSUhkcGJHd2dZbVVnZFhObFpDQnBiaUJ3Y205a2RXTjBhVzl1SUdWdWRtbHliMjV0Wlc1MElHWnZjaUJqWVdOb1pTQmlkWE4wYVc1bkxpQkpkQ0J0ZFhOMElGeHVYSFJjZENBcUlHSmxJSEJ5YjNacFpHVmtJSGRwZEdnZ2RHaGxJSGRwYm1SdmR5NUtVMFZ1WjJsdVpVTnZibVpwWjNWeVlYUnBiMjRnYjJKcVpXTjBMbHh1WEhSY2RDQXFJRnh1WEhSY2RDQXFJRUIwZVhCbElIdFRkSEpwYm1kOVhHNWNkRngwSUNvdlhHNWNkRngwWTJGamFHVlViMnRsYmpvZ0p5Y3NYRzVjZEZ4MFhHNWNkRngwTHlvcVhHNWNkRngwSUNvZ1JHVm1hVzVsY3lCM2FHVjBhR1Z5SUhSb1pTQm9hWE4wYjNKNUlHOWlhbVZqZENCcGN5QmhkbUZwYkdGaWJHVXVYRzVjZEZ4MElDcGNibHgwWEhRZ0tpQkFkSGx3WlNCN1FtOXZiR1ZoYm4xY2JseDBYSFFnS2k5Y2JseDBYSFJvYVhOMGIzSjVPaUJvYVhOMGIzSjVJQ1ltSUdocGMzUnZjbmt1Y21Wd2JHRmpaVk4wWVhSbElDWW1JR2hwYzNSdmNua3VjSFZ6YUZOMFlYUmxYRzVjZEgwN1hHNWNkRnh1WEhRdktpcGNibHgwSUNvZ1FteGhZMnRzYVhOMElHTnZibVpwWnlCMllXeDFaWE1nYVc0Z2NISnZaSFZqZEdsdmJpQmxiblpwY205dWJXVnVkQzVjYmx4MElDb2dYRzVjZENBcUlFQjBlWEJsSUh0VGRISnBibWRiWFgxY2JseDBJQ292WEc1Y2RHTnZibk4wSUdKc1lXTnJiR2x6ZENBOUlGdGNibHgwWEhRbmRtVnljMmx2Ymljc1hHNWNkRngwSjJGd2NGWmxjbk5wYjI0bkxGeHVYSFJjZENkemFHOXdWbVZ5YzJsdmJpZGNibHgwWFR0Y2JseDBYRzVjZEM4dklDMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExWeHVYSFF2THlCUVZVSk1TVU1nVFVWVVNFOUVVMXh1WEhRdkx5QXRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMWNibHgwWEc1Y2RDOHFLbHh1WEhRZ0tpQkhaWFFnWVNCamIyNW1hV2QxY21GMGFXOXVJSFpoYkhWbExseHVYSFFnS2x4dVhIUWdLaUJBY0dGeVlXMGdlMU4wY21sdVozMGdibUZ0WlNCVWFHVWdZMjl1Wm1sbmRYSmhkR2x2YmlCMllXeDFaU0J1WVcxbElIUnZJR0psSUhKbGRISnBaWFpsWkM1Y2JseDBJQ3BjYmx4MElDb2dRSEpsZEhWeWJpQjdLbjBnVW1WMGRYSnVjeUIwYUdVZ1kyOXVabWxuSUhaaGJIVmxMbHh1WEhRZ0tpOWNibHgwWlhod2IzSjBjeTVuWlhRZ1BTQm1kVzVqZEdsdmJpaHVZVzFsS1NCN1hHNWNkRngwYVdZZ0tHTnZibVpwWnk1bGJuWnBjbTl1YldWdWRDQTlQVDBnSjNCeWIyUjFZM1JwYjI0bklDWW1JR0pzWVdOcmJHbHpkQzVwYm1Oc2RXUmxjeWh1WVcxbEtTa2dlMXh1WEhSY2RGeDBjbVYwZFhKdUlHNTFiR3c3SUZ4dVhIUmNkSDFjYmx4MFhIUmNibHgwWEhSeVpYUjFjbTRnWTI5dVptbG5XMjVoYldWZE8xeHVYSFI5TzF4dVhIUmNibHgwTHlvcVhHNWNkQ0FxSUVsdWFYUnBZV3hwZW1VZ2RHaGxJRXBUSUVWdVoybHVaU0JqYjI1bWFXY2diMkpxWldOMExseHVYSFFnS2x4dVhIUWdLaUJVYUdseklHMWxkR2h2WkNCM2FXeHNJSEJoY25ObElIUm9aU0JuYkc5aVlXd2dYQ0pLVTBWdVoybHVaVU52Ym1acFozVnlZWFJwYjI1Y0lpQnZZbXBsWTNRZ1lXNWtJSFJvWlc0Z2NtVnRiM1psWEc1Y2RDQXFJR2wwSUdaeWIyMGdkR2hsSUdkc2IySmhiQ0J6WTI5d1pTQnpieUIwYUdGMElHbDBJR0psWTI5dFpYTWdkR2hsSUc5dWJIa2dZMjl1Wm1sbklITnZkWEpqWlNCbWIzSWdhbUYyWVhOamNtbHdkQzVjYmx4MElDcGNibHgwSUNvZ1RtOTBhV05sT2lCVWFHVWdiMjVzZVNCeVpYRjFhWEpsWkNCS1UwVnVaMmx1WlVOdmJtWnBaM1Z5WVhScGIyNGdkbUZzZFdWeklHRnlaU0IwYUdVZ1hDSmxiblpwY205dWJXVnVkRndpSUdGdVpDQjBhR1VnWENKaGNIQlZjbXhjSWk1Y2JseDBJQ3BjYmx4MElDb2dRSEJoY21GdElIdFBZbXBsWTNSOUlHcHpSVzVuYVc1bFEyOXVabWxuZFhKaGRHbHZiaUJOZFhOMElHTnZiblJoYVc0Z2FXNW1iM0p0WVhScGIyNGdkR2hoZENCa1pXWnBibVVnWTI5eVpTQnZjR1Z5WVhScGIyNXpYRzVjZENBcUlHOW1JSFJvWlNCbGJtZHBibVV1SUVOb1pXTnJJSFJvWlNCY0lteHBZbk12YVc1cGRHbGhiR2w2WlZ3aUlHVnVkSEo1SUc5bUlIUm9aU0JsYm1kcGJtVWdaRzlqZFcxbGJuUmhkR2x2Ymk1Y2JseDBJQ292WEc1Y2RHVjRjRzl5ZEhNdWFXNXBkQ0E5SUdaMWJtTjBhVzl1S0dwelJXNW5hVzVsUTI5dVptbG5kWEpoZEdsdmJpa2dlMXh1WEhSY2RHTnZibVpwWnk1bGJuWnBjbTl1YldWdWRDQTlJR3B6Ulc1bmFXNWxRMjl1Wm1sbmRYSmhkR2x2Ymk1bGJuWnBjbTl1YldWdWREdGNibHgwWEhSamIyNW1hV2N1WVhCd1ZYSnNJRDBnYW5ORmJtZHBibVZEYjI1bWFXZDFjbUYwYVc5dUxtRndjRlZ5YkM1eVpYQnNZV05sS0M5Y1hDOHJKQzhzSUNjbktUc2dMeThnVW1WdGIzWmxJSFJ5WVdsc2FXNW5JSE5zWVhOb0lHWnliMjBnWVhCd1ZYSnNMbHh1WEhSY2RGeHVYSFJjZEdsbUlDaGpiMjVtYVdjdVpXNTJhWEp2Ym0xbGJuUWdQVDA5SUNka1pYWmxiRzl3YldWdWRDY3BJSHRjYmx4MFhIUmNkR052Ym1acFp5NWpZV05vWlVKMWMzUWdQU0JtWVd4elpUdGNibHgwWEhSY2RHTnZibVpwWnk1dGFXNXBabWxsWkNBOUlHWmhiSE5sTzF4dVhIUmNkRngwWTI5dVptbG5MbVJsWW5WbklEMGdKMFJGUWxWSEp6dGNibHgwWEhSOVhHNWNkRngwWEc1Y2RGeDBhV1lnS0dwelJXNW5hVzVsUTI5dVptbG5kWEpoZEdsdmJpNWxibWRwYm1WVmNtd2dJVDA5SUhWdVpHVm1hVzVsWkNrZ2UxeHVYSFJjZEZ4MFkyOXVabWxuTG1WdVoybHVaVlZ5YkNBOUlHcHpSVzVuYVc1bFEyOXVabWxuZFhKaGRHbHZiaTVsYm1kcGJtVlZjbXd1Y21Wd2JHRmpaU2d2WEZ3dkt5UXZMQ0FuSnlrN1hHNWNkRngwZlNCbGJITmxJSHRjYmx4MFhIUmNkR052Ym1acFp5NWxibWRwYm1WVmNtd2dQU0JqYjI1bWFXY3VZWEJ3VlhKc0lDc2dKeTlLVTBWdVoybHVaUzlpZFdsc1pDYzdYRzVjZEZ4MGZWeHVYSFJjZEZ4dVhIUmNkR2xtSUNocWMwVnVaMmx1WlVOdmJtWnBaM1Z5WVhScGIyNHVkSEpoYm5Oc1lYUnBiMjV6SUNFOVBTQjFibVJsWm1sdVpXUXBJSHRjYmx4MFhIUmNkR052Ym1acFp5NTBjbUZ1YzJ4aGRHbHZibk1nUFNCcWMwVnVaMmx1WlVOdmJtWnBaM1Z5WVhScGIyNHVkSEpoYm5Oc1lYUnBiMjV6TzF4dVhIUmNkRngwWEc1Y2RGeDBYSFJtYjNJZ0tHeGxkQ0J6WldOMGFXOXVUbUZ0WlNCcGJpQmpiMjVtYVdjdWRISmhibk5zWVhScGIyNXpLU0I3WEc1Y2RGeDBYSFJjZEdwelpTNWpiM0psTG14aGJtY3VZV1JrVTJWamRHbHZiaWh6WldOMGFXOXVUbUZ0WlN3Z1kyOXVabWxuTG5SeVlXNXpiR0YwYVc5dWMxdHpaV04wYVc5dVRtRnRaVjBwTzF4dVhIUmNkRngwZlZ4dVhIUmNkSDFjYmx4MFhIUmNibHgwWEhScFppQW9hbk5GYm1kcGJtVkRiMjVtYVdkMWNtRjBhVzl1TG1OdmJHeGxZM1JwYjI1eklDRTlQU0IxYm1SbFptbHVaV1FwSUh0Y2JseDBYSFJjZEdOdmJtWnBaeTVqYjJ4c1pXTjBhVzl1Y3lBOUlHcHpSVzVuYVc1bFEyOXVabWxuZFhKaGRHbHZiaTVqYjJ4c1pXTjBhVzl1Y3p0Y2JseDBYSFI5SUdWc2MyVWdlMXh1WEhSY2RGeDBZMjl1Wm1sbkxtTnZiR3hsWTNScGIyNXpJRDBnVzF4dVhIUmNkRngwWEhSN2JtRnRaVG9nSjJOdmJuUnliMnhzWlhKekp5d2dZWFIwY21saWRYUmxPaUFuWTI5dWRISnZiR3hsY2lkOUxGeHVYSFJjZEZ4MFhIUjdibUZ0WlRvZ0oyVjRkR1Z1YzJsdmJuTW5MQ0JoZEhSeWFXSjFkR1U2SUNkbGVIUmxibk5wYjI0bmZTeGNibHgwWEhSY2RGeDBlMjVoYldVNklDZDNhV1JuWlhSekp5d2dZWFIwY21saWRYUmxPaUFuZDJsa1oyVjBKMzFjYmx4MFhIUmNkRjFjYmx4MFhIUjlYRzVjZEZ4MFhHNWNkRngwYVdZZ0tHcHpSVzVuYVc1bFEyOXVabWxuZFhKaGRHbHZiaTVoY0hCV1pYSnphVzl1SUNFOVBTQjFibVJsWm1sdVpXUXBJSHRjYmx4MFhIUmNkR052Ym1acFp5NWhjSEJXWlhKemFXOXVJRDBnYW5ORmJtZHBibVZEYjI1bWFXZDFjbUYwYVc5dUxtRndjRlpsY25OcGIyNDdYRzVjZEZ4MGZWeHVYSFJjZEZ4dVhIUmNkR2xtSUNocWMwVnVaMmx1WlVOdmJtWnBaM1Z5WVhScGIyNHVjMmh2Y0ZWeWJDQWhQVDBnZFc1a1pXWnBibVZrS1NCN1hHNWNkRngwWEhScWMyVXVZMjl5WlM1a1pXSjFaeTUzWVhKdUtDZEtVeUJGYm1kcGJtVTZJRndpYzJodmNGVnliRndpSUdseklHUmxjSEpsWTJGMFpXUWdZVzVrSUhkcGJHd2dZbVVnY21WdGIzWmxaQ0JwYmlCS1V5QkZibWRwYm1VZ2RqRXVOU3dnY0d4bFlYTmxJQ2RjYmx4MFhIUmNkRngwS3lBbmRYTmxJSFJvWlNCY0ltRndjRlZ5YkZ3aUlHbHVjM1JsWVdRdUp5azdYRzVjZEZ4MFhIUmpiMjVtYVdjdWMyaHZjRlZ5YkNBOUlHcHpSVzVuYVc1bFEyOXVabWxuZFhKaGRHbHZiaTV6YUc5d1ZYSnNMbkpsY0d4aFkyVW9MMXhjTHlza0x5d2dKeWNwTzF4dVhIUmNkRngwWTI5dVptbG5MbUZ3Y0ZWeWJDQTlJR052Ym1acFp5NWhjSEJWY213Z2ZId2dZMjl1Wm1sbkxuTm9iM0JWY213N0lDOHZJRTFoYTJVZ2MzVnlaU0IwYUdVZ1hDSmhjSEJWY214Y0lpQjJZV3gxWlNCcGN5QnViM1FnWlcxd2RIa3VYRzVjZEZ4MGZWeHVYSFJjZEZ4dVhIUmNkR2xtSUNocWMwVnVaMmx1WlVOdmJtWnBaM1Z5WVhScGIyNHVjMmh2Y0ZabGNuTnBiMjRnSVQwOUlIVnVaR1ZtYVc1bFpDa2dlMXh1WEhSY2RGeDBhbk5sTG1OdmNtVXVaR1ZpZFdjdWQyRnliaWduU2xNZ1JXNW5hVzVsT2lCY0luTm9iM0JXWlhKemFXOXVYQ0lnYVhNZ1pHVndjbVZqWVhSbFpDQmhibVFnZDJsc2JDQmlaU0J5WlcxdmRtVmtJR2x1SUVwVElFVnVaMmx1WlNCMk1TNDFMQ0J3YkdWaGMyVWdKMXh1WEhSY2RGeDBYSFFySUNkMWMyVWdkR2hsSUZ3aVlYQndWbVZ5YzJsdmJsd2lJR2x1YzNSbFlXUXVKeWs3WEc1Y2RGeDBYSFJqYjI1bWFXY3VjMmh2Y0ZabGNuTnBiMjRnUFNCcWMwVnVaMmx1WlVOdmJtWnBaM1Z5WVhScGIyNHVjMmh2Y0ZabGNuTnBiMjQ3WEc1Y2RGeDBmVnh1WEhSY2RGeHVYSFJjZEdsbUlDaHFjMFZ1WjJsdVpVTnZibVpwWjNWeVlYUnBiMjR1Y0hKbFptbDRJQ0U5UFNCMWJtUmxabWx1WldRcElIdGNibHgwWEhSY2RHTnZibVpwWnk1d2NtVm1hWGdnUFNCcWMwVnVaMmx1WlVOdmJtWnBaM1Z5WVhScGIyNHVjSEpsWm1sNE8xeHVYSFJjZEgxY2JseDBYSFJjYmx4MFhIUnBaaUFvYW5ORmJtZHBibVZEYjI1bWFXZDFjbUYwYVc5dUxteGhibWQxWVdkbFEyOWtaU0FoUFQwZ2RXNWtaV1pwYm1Wa0tTQjdYRzVjZEZ4MFhIUmpiMjVtYVdjdWJHRnVaM1ZoWjJWRGIyUmxJRDBnYW5ORmJtZHBibVZEYjI1bWFXZDFjbUYwYVc5dUxteGhibWQxWVdkbFEyOWtaVHRjYmx4MFhIUjlYRzVjZEZ4MFhHNWNkRngwYVdZZ0tHUnZZM1Z0Wlc1MExtZGxkRVZzWlcxbGJuUkNlVWxrS0NkcGJtbDBMV3B6SnlrZ0lUMDlJRzUxYkd4Y2JseDBYSFJjZENZbUlHUnZZM1Z0Wlc1MExtZGxkRVZzWlcxbGJuUkNlVWxrS0NkcGJtbDBMV3B6SnlrdWFHRnpRWFIwY21saWRYUmxLQ2RrWVhSaExYQmhaMlV0ZEc5clpXNG5LU2tnZTF4dVhIUmNkRngwYW5ORmJtZHBibVZEYjI1bWFXZDFjbUYwYVc5dUxuQmhaMlZVYjJ0bGJpQTlJR1J2WTNWdFpXNTBMbWRsZEVWc1pXMWxiblJDZVVsa0tDZHBibWwwTFdwekp5a3VaMlYwUVhSMGNtbGlkWFJsS0Nka1lYUmhMWEJoWjJVdGRHOXJaVzRuS1R0Y2JseDBYSFI5WEc1Y2RGeDBYRzVjZEZ4MGFXWWdLR3B6Ulc1bmFXNWxRMjl1Wm1sbmRYSmhkR2x2Ymk1d1lXZGxWRzlyWlc0Z0lUMDlJSFZ1WkdWbWFXNWxaQ2tnZTF4dVhIUmNkRngwWTI5dVptbG5MbkJoWjJWVWIydGxiaUE5SUdwelJXNW5hVzVsUTI5dVptbG5kWEpoZEdsdmJpNXdZV2RsVkc5clpXNDdYRzVjZEZ4MGZWeHVYSFJjZEZ4dVhIUmNkR2xtSUNocWMwVnVaMmx1WlVOdmJtWnBaM1Z5WVhScGIyNHVZMkZqYUdWVWIydGxiaUFoUFQwZ2RXNWtaV1pwYm1Wa0tTQjdYRzVjZEZ4MFhIUmpiMjVtYVdjdVkyRmphR1ZVYjJ0bGJpQTlJR3B6Ulc1bmFXNWxRMjl1Wm1sbmRYSmhkR2x2Ymk1allXTm9aVlJ2YTJWdU8xeHVYSFJjZEgxY2JseDBYSFJjYmx4MFhIUXZMeUJCWkdRZ2RHaGxJRndpZEc5MVkyaEZkbVZ1ZEhOY0lpQmxiblJ5ZVNCemJ5QjBhR0YwSUcxdlpIVnNaWE1nWTJGdUlHSnBibVFnZG1GeWFXOTFjeUIwYjNWamFDQmxkbVZ1ZEhNZ1pHVndaVzVrYVc1bklIUm9aU0JpY205M2MyVnlMbHh1WEhSY2RHTnZibk4wSUdkbGJtVnlZV3hVYjNWamFFVjJaVzUwY3lBOUlIdGNibHgwWEhSY2RITjBZWEowT2lBbmRHOTFZMmh6ZEdGeWRDY3NYRzVjZEZ4MFhIUmxibVE2SUNkMGNtOTFZMmhsYm1RbkxGeHVYSFJjZEZ4MGJXOTJaVG9nSjNSdmRXTm9iVzkyWlNkY2JseDBYSFI5TzF4dVhIUmNkRnh1WEhSY2RHTnZibk4wSUcxcFkzSnZjMjltZEZSdmRXTm9SWFpsYm5SeklEMGdlMXh1WEhSY2RGeDBjM1JoY25RNklDZHdiMmx1ZEdWeVpHOTNiaWNzWEc1Y2RGeDBYSFJsYm1RNklDZHdiMmx1ZEdWeWRYQW5MRnh1WEhSY2RGeDBiVzkyWlRvZ0ozQnZhVzUwWlhKdGIzWmxKMXh1WEhSY2RIMDdYRzVjZEZ4MFhHNWNkRngwWTI5dVptbG5MblJ2ZFdOb1JYWmxiblJ6SUQwZ0tIZHBibVJ2ZHk1dmJtMXpaMlZ6ZEhWeVpXTm9ZVzVuWlNrZ1B5QnRhV055YjNOdlpuUlViM1ZqYUVWMlpXNTBjeUE2SUdkbGJtVnlZV3hVYjNWamFFVjJaVzUwY3p0Y2JseDBYSFJjYmx4MFhIUXZMeUJUWlhRZ2FXNXBkR2xoYkNCeVpXZHBjM1J5ZVNCMllXeDFaWE11SUZ4dVhIUmNkR1p2Y2lBb2JHVjBJR1Z1ZEhKNUlHbHVJR3B6Ulc1bmFXNWxRMjl1Wm1sbmRYSmhkR2x2Ymk1eVpXZHBjM1J5ZVNrZ2UxeHVYSFJjZEZ4MGFuTmxMbU52Y21VdWNtVm5hWE4wY25rdWMyVjBLR1Z1ZEhKNUxDQnFjMFZ1WjJsdVpVTnZibVpwWjNWeVlYUnBiMjR1Y21WbmFYTjBjbmxiWlc1MGNubGRLVHNnWEc1Y2RGeDBmVnh1WEhSY2RGeHVYSFJjZEM4dklFbHVhWFJwWVd4cGVtVWdkR2hsSUcxdlpIVnNaU0JzYjJGa1pYSWdiMkpxWldOMExseHVYSFJjZEdwelpTNWpiM0psTG0xdlpIVnNaVjlzYjJGa1pYSXVhVzVwZENncE8xeHVYSFJjZEZ4dVhIUmNkQzh2SUVSbGMzUnliM2tnWjJ4dlltRnNJRVZ1WjJsdVpVTnZibVpwWjNWeVlYUnBiMjRnYjJKcVpXTjBMbHh1WEhSY2RHUmxiR1YwWlNCM2FXNWtiM2N1U2xORmJtZHBibVZEYjI1bWFXZDFjbUYwYVc5dU8xeHVYSFI5TzF4dVhIUmNibjBvYW5ObExtTnZjbVV1WTI5dVptbG5LU2s3WEc0aUxDSXZLaUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1SUdSbFluVm5MbXB6SURJd01UWXRNRGt0TURoY2JpQkhZVzFpYVc4Z1IyMWlTRnh1SUdoMGRIQTZMeTkzZDNjdVoyRnRZbWx2TG1SbFhHNGdRMjl3ZVhKcFoyaDBJQ2hqS1NBeU1ERTJJRWRoYldKcGJ5QkhiV0pJWEc0Z1VtVnNaV0Z6WldRZ2RXNWtaWElnZEdobElFZE9WU0JIWlc1bGNtRnNJRkIxWW14cFl5Qk1hV05sYm5ObElDaFdaWEp6YVc5dUlESXBYRzRnVzJoMGRIQTZMeTkzZDNjdVoyNTFMbTl5Wnk5c2FXTmxibk5sY3k5bmNHd3RNaTR3TG1oMGJXeGRYRzRnTFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzFjYmlBcUwxeHVYRzVxYzJVdVkyOXlaUzVrWldKMVp5QTlJR3B6WlM1amIzSmxMbVJsWW5WbklIeDhJSHQ5TzF4dVhHNHZLaXBjYmlBcUlFcFRSU0JFWldKMVp5Qk5iMlIxYkdWY2JpQXFYRzRnS2lCVWFHbHpJRzlpYW1WamRDQndjbTkyYVdSbGN5QmhiaUIzY21Gd2NHVnlJSFJ2SUhSb1pTQmpiMjV6YjJ4bExteHZaeUJtZFc1amRHbHZiaUJoYm1RZ1pXNWhZbXhsY3lCbFlYTjVJSFZ6WlZ4dUlDb2diMllnZEdobElHUnBabVpsY21WdWRDQnNiMmNnZEhsd1pYTWdiR2xyWlNCY0ltbHVabTljSWl3Z1hDSjNZWEp1YVc1blhDSXNJRndpWlhKeWIzSmNJaUJsZEdNdVhHNGdLbHh1SUNvZ1FHMXZaSFZzWlNCS1UwVXZRMjl5WlM5a1pXSjFaMXh1SUNvdlhHNG9ablZ1WTNScGIyNG9aWGh3YjNKMGN5a2dlMXh1WEhRbmRYTmxJSE4wY21samRDYzdYRzVjZEZ4dVhIUXZMeUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzFjYmx4MEx5OGdWa0ZTU1VGQ1RFVlRYRzVjZEM4dklDMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExWeHVYSFJjYmx4MEx5b3FYRzVjZENBcUlFQjBlWEJsSUh0VGRISnBibWQ5WEc1Y2RDQXFMMXh1WEhSamIyNXpkQ0JVV1ZCRlgwUkZRbFZISUQwZ0owUkZRbFZISnp0Y2JseDBYRzVjZEM4cUtseHVYSFFnS2lCQWRIbHdaU0I3VTNSeWFXNW5mVnh1WEhRZ0tpOWNibHgwWTI5dWMzUWdWRmxRUlY5SlRrWlBJRDBnSjBsT1JrOG5PMXh1WEhSY2JseDBMeW9xWEc1Y2RDQXFJRUIwZVhCbElIdFRkSEpwYm1kOVhHNWNkQ0FxTDF4dVhIUmpiMjV6ZENCVVdWQkZYMHhQUnlBOUlDZE1UMGNuTzF4dVhIUmNibHgwTHlvcVhHNWNkQ0FxSUVCMGVYQmxJSHRUZEhKcGJtZDlYRzVjZENBcUwxeHVYSFJqYjI1emRDQlVXVkJGWDFkQlVrNGdQU0FuVjBGU1RpYzdYRzVjZEZ4dVhIUXZLaXBjYmx4MElDb2dRSFI1Y0dVZ2UxTjBjbWx1WjMxY2JseDBJQ292WEc1Y2RHTnZibk4wSUZSWlVFVmZSVkpTVDFJZ1BTQW5SVkpTVDFJbk8xeHVYSFJjYmx4MEx5b3FYRzVjZENBcUlFQjBlWEJsSUh0VGRISnBibWQ5WEc1Y2RDQXFMMXh1WEhSamIyNXpkQ0JVV1ZCRlgwRk1SVkpVSUQwZ0owRk1SVkpVSnp0Y2JseDBYRzVjZEM4cUtseHVYSFFnS2lCQWRIbHdaU0I3VTNSeWFXNW5mVnh1WEhRZ0tpOWNibHgwWTI5dWMzUWdWRmxRUlY5TlQwSkpURVVnUFNBblRVOUNTVXhGSnp0Y2JseDBYRzVjZEM4cUtseHVYSFFnS2lCQWRIbHdaU0I3VTNSeWFXNW5mVnh1WEhRZ0tpOWNibHgwWTI5dWMzUWdWRmxRUlY5VFNVeEZUbFFnUFNBblUwbE1SVTVVSnp0Y2JseDBYRzVjZEM4cUtseHVYSFFnS2lCQmJHd2djRzl6YzJsaWJHVWdaR1ZpZFdjZ2JHVjJaV3h6SUdsdUlIUm9aU0J2Y21SbGNpQnZaaUJwYlhCdmNuUmhibU5sTGx4dVhIUWdLbHh1WEhRZ0tpQkFkSGx3WlNCN1UzUnlhVzVuVzExOVhHNWNkQ0FxTDF4dVhIUmpiMjV6ZENCc1pYWmxiSE1nUFNCYlhHNWNkRngwVkZsUVJWOUVSVUpWUnl4Y2JseDBYSFJVV1ZCRlgwbE9SazhzWEc1Y2RGeDBWRmxRUlY5TVQwY3NYRzVjZEZ4MFZGbFFSVjlYUVZKT0xGeHVYSFJjZEZSWlVFVmZSVkpTVDFJc1hHNWNkRngwVkZsUVJWOUJURVZTVkN4Y2JseDBYSFJVV1ZCRlgwMVBRa2xNUlN4Y2JseDBYSFJVV1ZCRlgxTkpURVZPVkZ4dVhIUmRPMXh1WEhSY2JseDBMeThnTFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0WEc1Y2RDOHZJRVpWVGtOVVNVOU9VMXh1WEhRdkx5QXRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMWNibHgwWEc1Y2RDOHFLbHh1WEhRZ0tpQlRaWFFnUm1GMmFXTnZiaUIwYnlCRmNuSnZjaUJUZEdGMFpTNWNibHgwSUNwY2JseDBJQ29nVkdocGN5QnRaWFJvYjJRZ2QybHNiQ0J2Ym14NUlIZHZjbXNnYVdZZ1BHTmhiblpoY3o0Z2FYTWdjM1Z3Y0c5eWRHVmtJR1p5YjIwZ2RHaGxJR0p5YjNkelpYSXVYRzVjZENBcVhHNWNkQ0FxSUVCd2NtbDJZWFJsWEc1Y2RDQXFMMXh1WEhSbWRXNWpkR2x2YmlCZmMyVjBSbUYyYVdOdmJsUnZSWEp5YjNKVGRHRjBaU2dwSUh0Y2JseDBYSFJqYjI1emRDQmpZVzUyWVhNZ1BTQmtiMk4xYldWdWRDNWpjbVZoZEdWRmJHVnRaVzUwS0NkallXNTJZWE1uS1R0Y2JseDBYSFJqYjI1emRDQm1ZWFpwWTI5dUlEMGdaRzlqZFcxbGJuUXVjWFZsY25sVFpXeGxZM1J2Y2lnblczSmxiRDFjSW5Ob2IzSjBZM1YwSUdsamIyNWNJbDBuS1R0Y2JseDBYSFJjYmx4MFhIUnBaaUFvWTJGdWRtRnpMbWRsZEVOdmJuUmxlSFFnSmlZZ0lXWmhkbWxqYjI0dVkyeGhjM05PWVcxbExtbHVZMngxWkdWektDZGxjbkp2Y2kxemRHRjBaU2NwS1NCN1hHNWNkRngwWEhSamIyNXpkQ0JwYldjZ1BTQmtiMk4xYldWdWRDNWpjbVZoZEdWRmJHVnRaVzUwS0NkcGJXY25LVHRjYmx4MFhIUmNkR05oYm5aaGN5NW9aV2xuYUhRZ1BTQmpZVzUyWVhNdWQybGtkR2dnUFNBeE5qdGNibHgwWEhSY2RHTnZibk4wSUdOMGVDQTlJR05oYm5aaGN5NW5aWFJEYjI1MFpYaDBLQ2N5WkNjcE8xeHVYSFJjZEZ4MGFXMW5MbTl1Ykc5aFpDQTlJR1oxYm1OMGFXOXVLQ2tnZXlBdkx5QkRiMjUwYVc1MVpTQnZibU5sSUhSb1pTQnBiV0ZuWlNCb1lYTWdZbVZsYmlCc2IyRmtaV1F1SUZ4dVhIUmNkRngwWEhSamRIZ3VaSEpoZDBsdFlXZGxLSFJvYVhNc0lEQXNJREFwTzF4dVhIUmNkRngwWEhSamRIZ3VaMnh2WW1Gc1FXeHdhR0VnUFNBd0xqWTFPMXh1WEhSY2RGeDBYSFJqZEhndVptbHNiRk4wZVd4bElEMGdKeU5HUmpBd01EQW5PMXh1WEhSY2RGeDBYSFJqZEhndWNtVmpkQ2d3TENBd0xDQXhOaXdnTVRZcE8xeHVYSFJjZEZ4MFhIUmpkSGd1Wm1sc2JDZ3BPMXh1WEhSY2RGeDBYSFJtWVhacFkyOXVMbWh5WldZZ1BTQmpZVzUyWVhNdWRHOUVZWFJoVlZKTUtDZHBiV0ZuWlM5d2JtY25LVHRjYmx4MFhIUmNkRngwWm1GMmFXTnZiaTVqYkdGemMwNWhiV1VnS3owZ0oyVnljbTl5TFhOMFlYUmxKenRjYmx4MFhIUmNkSDA3WEc1Y2RGeDBYSFJwYldjdWMzSmpJRDBnWm1GMmFXTnZiaTVvY21WbU8xeHVYSFJjZEgxY2JseDBmVnh1WEhSY2JseDBMeW9xWEc1Y2RDQXFJRVZ5Y205eUlHaGhibVJzWlhJZ2RHaGhkQ0JtWlhSamFHVnpJR0ZzYkNCbGVHTmxjSFJwYjI1eklIUm9jbTkzYmlCaWVTQjBhR1VnYW1GMllYTmpjbWx3ZEM1Y2JseDBJQ3BjYmx4MElDb2dRSEJ5YVhaaGRHVmNibHgwSUNvdlhHNWNkR1oxYm1OMGFXOXVJRjluYkc5aVlXeEZjbkp2Y2toaGJtUnNaWElvS1NCN1hHNWNkRngwYVdZZ0tHcHpaUzVqYjNKbExtTnZibVpwWnk1blpYUW9KMlZ1ZG1seWIyNXRaVzUwSnlrZ0lUMDlJQ2R3Y205a2RXTjBhVzl1SnlrZ2UxeHVYSFJjZEZ4MEx5OGdURzluSUhSb1pTQmxjbkp2Y2lCcGJpQjBhR1VnWW5KdmQzTmxjaWR6SUdOdmJuTnZiR1V1SUZ4dVhIUmNkRngwYVdZZ0tHcHpaUzVqYjNKbExtUmxZblZuSUNFOVBTQjFibVJsWm1sdVpXUXBJSHRjYmx4MFhIUmNkRngwYW5ObExtTnZjbVV1WkdWaWRXY3VaWEp5YjNJb0owcFRJRVZ1WjJsdVpTQkZjbkp2Y2lCSVlXNWtiR1Z5Snl3Z1lYSm5kVzFsYm5SektUdGNibHgwWEhSY2RIMGdaV3h6WlNCN1hHNWNkRngwWEhSY2RHTnZibk52YkdVdWJHOW5LQ2RLVXlCRmJtZHBibVVnUlhKeWIzSWdTR0Z1Wkd4bGNpY3NJR0Z5WjNWdFpXNTBjeWs3WEc1Y2RGeDBYSFI5WEc1Y2RGeDBYSFJjYmx4MFhIUmNkQzh2SUZWd1pHRjBaU0IwYUdVZ2NHRm5aU0IwYVhSc1pTQjNhWFJvSUdGdUlHVnljbTl5SUdOdmRXNTBMbHh1WEhSY2RGeDBZMjl1YzNRZ2NtVm5aWGdnUFNBdkxseGNJRnhjV3lndUt5bGNYRjFjWENBdk8xeHVYSFJjZEZ4MGJHVjBJSFJwZEd4bElEMGdkMmx1Wkc5M0xtUnZZM1Z0Wlc1MExuUnBkR3hsTzF4dVhIUmNkRngwYkdWMElHVnljbTl5UTI5MWJuUWdQU0F4TzF4dVhIUmNkRngwWEc1Y2RGeDBYSFF2THlCSFpYUnpJSFJvWlNCamRYSnlaVzUwSUdWeWNtOXlJR052ZFc1MElHRnVaQ0J5WldOeVpXRjBaWE1nZEdobElHUmxabUYxYkhRZ2RHbDBiR1VnYjJZZ2RHaGxJSEJoWjJVdVhHNWNkRngwWEhScFppQW9kR2wwYkdVdWJXRjBZMmdvY21WblpYZ3BJQ0U5UFNCdWRXeHNLU0I3WEc1Y2RGeDBYSFJjZEdWeWNtOXlRMjkxYm5RZ1BTQndZWEp6WlVsdWRDaDBhWFJzWlM1dFlYUmphQ2d2WEZ4a0t5OHBXekJkTENBeE1Da2dLeUF4TzF4dVhIUmNkRngwWEhSMGFYUnNaU0E5SUhScGRHeGxMbkpsY0d4aFkyVW9jbVZuWlhnc0lDY25LVHRjYmx4MFhIUmNkSDFjYmx4MFhIUmNkRnh1WEhSY2RGeDBMeThnVW1VdFkzSmxZWFJsY3lCMGFHVWdaWEp5YjNJZ1pteGhaeUJoZENCMGFHVWdkR2wwYkdVZ2QybDBhQ0IwYUdVZ2JtVjNJR1Z5Y205eUlHTnZkVzUwTGx4dVhIUmNkRngwZEdsMGJHVWdQU0FuNHB5V0lGc25JQ3NnWlhKeWIzSkRiM1Z1ZENBcklDZGRJQ2NnS3lCMGFYUnNaVHRjYmx4MFhIUmNkSGRwYm1SdmR5NWtiMk4xYldWdWRDNTBhWFJzWlNBOUlIUnBkR3hsTzF4dVhIUmNkRngwWEc1Y2RGeDBYSFF2THlCVFpYUWdSbUYyYVdOdmJpQjBieUJGY25KdmNpQlRkR0YwWlM1Y2JseDBYSFJjZEY5elpYUkdZWFpwWTI5dVZHOUZjbkp2Y2xOMFlYUmxLQ2s3WEc1Y2RGeDBmVnh1WEhSY2RGeHVYSFJjZEhKbGRIVnliaUIwY25WbE8xeHVYSFI5WEc1Y2RGeHVYSFF2S2lwY2JseDBJQ29nUlhobFkzVjBaWE1nZEdobElHTnZjbkpsWTNRZ1kyOXVjMjlzWlM5aGJHVnlkQ0J6ZEdGMFpXMWxiblF1WEc1Y2RDQXFYRzVjZENBcUlFQndZWEpoYlNCN1QySnFaV04wZlNCallXeHNaWElnS0c5d2RHbHZibUZzS1NCRGIyNTBZV2x1Y3lCMGFHVWdZMkZzYkdWeUlHbHVabTl5YldGMGFXOXVJSFJ2SUdKbElHUnBjM0JzWVhsbFpDNWNibHgwSUNvZ1FIQmhjbUZ0SUh0UFltcGxZM1I5SUdSaGRHRWdLRzl3ZEdsdmJtRnNLU0JEYjI1MFlXbHVjeUJoYm5rZ1lXUmthWFJwYjI1aGJDQmtZWFJoSUhSdklHSmxJR2x1WTJ4MVpHVmtJR2x1SUhSb1pTQmtaV0oxWnlCdmRYUndkWFF1WEc1Y2RDQXFYRzVjZENBcUlFQndjbWwyWVhSbFhHNWNkQ0FxTDF4dVhIUm1kVzVqZEdsdmJpQmZaWGhsWTNWMFpTaGpZV3hzWlhJc0lHUmhkR0VwSUh0Y2JseDBYSFJqYjI1emRDQmpkWEp5Wlc1MFRHOW5TVzVrWlhnZ1BTQnNaWFpsYkhNdWFXNWtaWGhQWmloallXeHNaWElwTzF4dVhIUmNkR052Ym5OMElHRnNiRzkzWldSTWIyZEpibVJsZUNBOUlHeGxkbVZzY3k1cGJtUmxlRTltS0dwelpTNWpiM0psTG1OdmJtWnBaeTVuWlhRb0oyUmxZblZuSnlrcE8xeHVYSFJjZEd4bGRDQmpiMjV6YjJ4bFRXVjBhRzlrSUQwZ2JuVnNiRHRjYmx4MFhIUmNibHgwWEhScFppQW9ZM1Z5Y21WdWRFeHZaMGx1WkdWNElENDlJR0ZzYkc5M1pXUk1iMmRKYm1SbGVDa2dlMXh1WEhSY2RGeDBZMjl1YzI5c1pVMWxkR2h2WkNBOUlHTmhiR3hsY2k1MGIweHZkMlZ5UTJGelpTZ3BPMXh1WEhSY2RGeDBYRzVjZEZ4MFhIUnpkMmwwWTJnZ0tHTnZibk52YkdWTlpYUm9iMlFwSUh0Y2JseDBYSFJjZEZ4MFkyRnpaU0FuWVd4bGNuUW5PbHh1WEhSY2RGeDBYSFJjZEdGc1pYSjBLRXBUVDA0dWMzUnlhVzVuYVdaNUtHUmhkR0VwS1R0Y2JseDBYSFJjZEZ4MFhIUmljbVZoYXp0Y2JseDBYSFJjZEZ4MFhHNWNkRngwWEhSY2RHTmhjMlVnSjIxdlltbHNaU2M2WEc1Y2RGeDBYSFJjZEZ4MFkyOXVjM1FnSkcxdlltbHNaVVJsWW5WblRXOWtZV3dnUFNBa0tDY3ViVzlpYVd4bExXUmxZblZuTFcxdlpHRnNKeWs3WEc1Y2RGeDBYSFJjZEZ4MFhHNWNkRngwWEhSY2RGeDBhV1lnS0NFa2JXOWlhV3hsUkdWaWRXZE5iMlJoYkM1c1pXNW5kR2dwSUh0Y2JseDBYSFJjZEZ4MFhIUmNkQ1FvSnp4a2FYWWdMejRuS1Z4dVhIUmNkRngwWEhSY2RGeDBYSFF1WVdSa1EyeGhjM01vSjIxdlltbHNaUzFrWldKMVp5MXRiMlJoYkNjcFhHNWNkRngwWEhSY2RGeDBYSFJjZEM1amMzTW9lMXh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkSEJ2YzJsMGFXOXVPaUFuWm1sNFpXUW5MRnh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkSFJ2Y0RvZ01DeGNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUnNaV1owT2lBd0xGeHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RHMWhlRWhsYVdkb2REb2dKelV3SlNjc1hHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MGJXbHVWMmxrZEdnNklDY3lNREJ3ZUNjc1hHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MGJXRjRWMmxrZEdnNklDY3pNREJ3ZUNjc1hHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MFltRmphMmR5YjNWdVpFTnZiRzl5T2lBblkzSnBiWE52Ymljc1hHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MGVrbHVaR1Y0T2lBeE1EQXdNREFzWEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwYjNabGNtWnNiM2M2SUNkelkzSnZiR3duWEc1Y2RGeDBYSFJjZEZ4MFhIUmNkSDBwWEc1Y2RGeDBYSFJjZEZ4MFhIUmNkQzVoY0hCbGJtUlVieWdrS0NkaWIyUjVKeWtwTzF4dVhIUmNkRngwWEhSY2RIMWNibHgwWEhSY2RGeDBYSFJjYmx4MFhIUmNkRngwWEhRa2JXOWlhV3hsUkdWaWRXZE5iMlJoYkM1aGNIQmxibVFvSnp4d1BpY2dLeUJLVTA5T0xuTjBjbWx1WjJsbWVTaGtZWFJoS1NBcklDYzhMM0ErSnlrN1hHNWNkRngwWEhSY2RGeDBZbkpsWVdzN1hHNWNkRngwWEhSY2RGeHVYSFJjZEZ4MFhIUmtaV1poZFd4ME9seHVYSFJjZEZ4MFhIUmNkR2xtSUNoamIyNXpiMnhsSUQwOVBTQjFibVJsWm1sdVpXUXBJSHRjYmx4MFhIUmNkRngwWEhSY2RISmxkSFZ5YmpzZ0x5OGdWR2hsY21VZ2FYTWdibThnWTI5dWMyOXNaU0J6ZFhCd2IzSjBJSE52SUdSdklHNXZkQ0J3Y205alpXVmtMbHh1WEhSY2RGeDBYSFJjZEgxY2JseDBYSFJjZEZ4MFhIUmNibHgwWEhSY2RGeDBYSFJwWmlBb2RIbHdaVzltSUdOdmJuTnZiR1ZiWTI5dWMyOXNaVTFsZEdodlpGMHVZWEJ3YkhrZ1BUMDlJQ2RtZFc1amRHbHZiaWNnZkh3Z2RIbHdaVzltSUdOdmJuTnZiR1V1Ykc5bkxtRndjR3g1SUQwOVBTQW5ablZ1WTNScGIyNG5LU0I3WEc1Y2RGeDBYSFJjZEZ4MFhIUnBaaUFvWTI5dWMyOXNaVnRqYjI1emIyeGxUV1YwYUc5a1hTQWhQVDBnZFc1a1pXWnBibVZrS1NCN1hHNWNkRngwWEhSY2RGeDBYSFJjZEdOdmJuTnZiR1ZiWTI5dWMyOXNaVTFsZEdodlpGMHVZWEJ3Ykhrb1kyOXVjMjlzWlN3Z1pHRjBZU2s3WEc1Y2RGeDBYSFJjZEZ4MFhIUjlJR1ZzYzJVZ2UxeHVYSFJjZEZ4MFhIUmNkRngwWEhSamIyNXpiMnhsTG14dlp5NWhjSEJzZVNoamIyNXpiMnhsTENCa1lYUmhLVHRjYmx4MFhIUmNkRngwWEhSY2RIMWNibHgwWEhSY2RGeDBYSFI5SUdWc2MyVWdlMXh1WEhSY2RGeDBYSFJjZEZ4MFkyOXVjMjlzWlM1c2IyY29aR0YwWVNrN1hHNWNkRngwWEhSY2RGeDBmVnh1WEhSY2RGeDBmVnh1WEhSY2RIMWNibHgwZlZ4dVhIUmNibHgwTHlvcVhHNWNkQ0FxSUVKcGJtUWdSMnh2WW1Gc0lFVnljbTl5SUVoaGJtUnNaWEpjYmx4MElDb3ZYRzVjZEdWNGNHOXlkSE11WW1sdVpFZHNiMkpoYkVWeWNtOXlTR0Z1Wkd4bGNpQTlJR1oxYm1OMGFXOXVLQ2tnZTF4dVhIUmNkSGRwYm1SdmR5NXZibVZ5Y205eUlEMGdYMmRzYjJKaGJFVnljbTl5U0dGdVpHeGxjanRjYmx4MGZUdGNibHgwWEc1Y2RDOHFLbHh1WEhRZ0tpQlNaWEJzWVdObGN5QmpiMjV6YjJ4bExtUmxZblZuWEc1Y2RDQXFYRzVjZENBcUlFQndZWEpoYlhNZ2V5cDlJR0Z5WjNWdFpXNTBjeUJCYm5rZ1pHRjBZU0IwYUdGMElITm9iM1ZzWkNCaVpTQnphRzkzYmlCcGJpQjBhR1VnWTI5dWMyOXNaU0J6ZEdGMFpXMWxiblF1WEc1Y2RDQXFMMXh1WEhSbGVIQnZjblJ6TG1SbFluVm5JRDBnWm5WdVkzUnBiMjRvS1NCN1hHNWNkRngwWDJWNFpXTjFkR1VvVkZsUVJWOUVSVUpWUnl3Z1lYSm5kVzFsYm5SektUdGNibHgwZlR0Y2JseDBYRzVjZEM4cUtseHVYSFFnS2lCU1pYQnNZV05sY3lCamIyNXpiMnhsTG1sdVptOWNibHgwSUNwY2JseDBJQ29nUUhCaGNtRnRjeUI3S24wZ1lYSm5kVzFsYm5SeklFRnVlU0JrWVhSaElIUm9ZWFFnYzJodmRXeGtJR0psSUhOb2IzZHVJR2x1SUhSb1pTQmpiMjV6YjJ4bElITjBZWFJsYldWdWRDNWNibHgwSUNvdlhHNWNkR1Y0Y0c5eWRITXVhVzVtYnlBOUlHWjFibU4wYVc5dUtDa2dlMXh1WEhSY2RGOWxlR1ZqZFhSbEtGUlpVRVZmU1U1R1R5d2dZWEpuZFcxbGJuUnpLVHRjYmx4MGZUdGNibHgwWEc1Y2RDOHFLbHh1WEhRZ0tpQlNaWEJzWVdObGN5QmpiMjV6YjJ4bExteHZaMXh1WEhRZ0tseHVYSFFnS2lCQWNHRnlZVzF6SUhzcWZTQmhjbWQxYldWdWRITWdRVzU1SUdSaGRHRWdkR2hoZENCemFHOTFiR1FnWW1VZ2MyaHZkMjRnYVc0Z2RHaGxJR052Ym5OdmJHVWdjM1JoZEdWdFpXNTBMbHh1WEhRZ0tpOWNibHgwWlhod2IzSjBjeTVzYjJjZ1BTQm1kVzVqZEdsdmJpZ3BJSHRjYmx4MFhIUmZaWGhsWTNWMFpTaFVXVkJGWDB4UFJ5d2dZWEpuZFcxbGJuUnpLVHRjYmx4MGZUdGNibHgwWEc1Y2RDOHFLbHh1WEhRZ0tpQlNaWEJzWVdObGN5QmpiMjV6YjJ4bExuZGhjbTVjYmx4MElDcGNibHgwSUNvZ1FIQmhjbUZ0Y3lCN0tuMGdZWEpuZFcxbGJuUnpJRUZ1ZVNCa1lYUmhJSFJvWVhRZ2MyaHZkV3hrSUdKbElITm9iM2R1SUdsdUlIUm9aU0JqYjI1emIyeGxJSE4wWVhSbGJXVnVkQzVjYmx4MElDb3ZYRzVjZEdWNGNHOXlkSE11ZDJGeWJpQTlJR1oxYm1OMGFXOXVLQ2tnZTF4dVhIUmNkRjlsZUdWamRYUmxLRlJaVUVWZlYwRlNUaXdnWVhKbmRXMWxiblJ6S1R0Y2JseDBmVHRjYmx4MFhHNWNkQzhxS2x4dVhIUWdLaUJTWlhCc1lXTmxjeUJqYjI1emIyeGxMbVZ5Y205eVhHNWNkQ0FxWEc1Y2RDQXFJRUJ3WVhKaGJTQjdLbjBnWVhKbmRXMWxiblJ6SUVGdWVTQmtZWFJoSUhSb1lYUWdjMmh2ZFd4a0lHSmxJSE5vYjNkdUlHbHVJSFJvWlNCamIyNXpiMnhsSUhOMFlYUmxiV1Z1ZEM1Y2JseDBJQ292WEc1Y2RHVjRjRzl5ZEhNdVpYSnliM0lnUFNCbWRXNWpkR2x2YmlncElIdGNibHgwWEhSZlpYaGxZM1YwWlNoVVdWQkZYMFZTVWs5U0xDQmhjbWQxYldWdWRITXBPMXh1WEhSOU8xeHVYSFJjYmx4MEx5b3FYRzVjZENBcUlGSmxjR3hoWTJWeklHRnNaWEowWEc1Y2RDQXFYRzVjZENBcUlFQndZWEpoYlNCN0tuMGdZWEpuZFcxbGJuUnpJRUZ1ZVNCa1lYUmhJSFJvWVhRZ2MyaHZkV3hrSUdKbElITm9iM2R1SUdsdUlIUm9aU0JqYjI1emIyeGxJSE4wWVhSbGJXVnVkQzVjYmx4MElDb3ZYRzVjZEdWNGNHOXlkSE11WVd4bGNuUWdQU0JtZFc1amRHbHZiaWdwSUh0Y2JseDBYSFJmWlhobFkzVjBaU2hVV1ZCRlgwRk1SVkpVTENCaGNtZDFiV1Z1ZEhNcE8xeHVYSFI5TzF4dVhIUmNibHgwTHlvcVhHNWNkQ0FxSUVSbFluVm5JR2x1Wm04Z1ptOXlJRzF2WW1sc1pTQmtaWFpwWTJWekxseHVYSFFnS2x4dVhIUWdLaUJBY0dGeVlXMGdleXA5SUdGeVozVnRaVzUwY3lCQmJua2daR0YwWVNCMGFHRjBJSE5vYjNWc1pDQmlaU0J6YUc5M2JpQnBiaUIwYUdVZ1kyOXVjMjlzWlNCemRHRjBaVzFsYm5RdVhHNWNkQ0FxTDF4dVhIUmxlSEJ2Y25SekxtMXZZbWxzWlNBOUlHWjFibU4wYVc5dUtDa2dlMXh1WEhSY2RGOWxlR1ZqZFhSbEtGUlpVRVZmVFU5Q1NVeEZMQ0JoY21kMWJXVnVkSE1wTzF4dVhIUjlPMXh1WEhSY2JuMG9hbk5sTG1OdmNtVXVaR1ZpZFdjcEtUdGNiaUlzSWk4cUlDMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdFhHNGdaVzVuYVc1bExtcHpJREl3TVRZdE1Ea3RNRGhjYmlCSFlXMWlhVzhnUjIxaVNGeHVJR2gwZEhBNkx5OTNkM2N1WjJGdFltbHZMbVJsWEc0Z1EyOXdlWEpwWjJoMElDaGpLU0F5TURFMklFZGhiV0pwYnlCSGJXSklYRzRnVW1Wc1pXRnpaV1FnZFc1a1pYSWdkR2hsSUVkT1ZTQkhaVzVsY21Gc0lGQjFZbXhwWXlCTWFXTmxibk5sSUNoV1pYSnphVzl1SURJcFhHNGdXMmgwZEhBNkx5OTNkM2N1WjI1MUxtOXlaeTlzYVdObGJuTmxjeTluY0d3dE1pNHdMbWgwYld4ZFhHNGdMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMWNiaUFxTDF4dVhHNXFjMlV1WTI5eVpTNWxibWRwYm1VZ1BTQnFjMlV1WTI5eVpTNWxibWRwYm1VZ2ZId2dlMzA3WEc1Y2JpOHFLbHh1SUNvZ1NsTkZJRU52Y21VZ1RXOWtkV3hsWEc0Z0tseHVJQ29nVkdocGN5QnZZbXBsWTNRZ2QybHNiQ0JwYm1sMGFXRnNhWHBsSUhSb1pTQndZV2RsSUc1aGJXVnpjR0ZqWlhNZ1lXNWtJR052Ykd4bFkzUnBiMjV6TGx4dUlDcGNiaUFxSUVCdGIyUjFiR1VnU2xORkwwTnZjbVV2Wlc1bmFXNWxYRzRnS2k5Y2JpaG1kVzVqZEdsdmJpaGxlSEJ2Y25SektTQjdYRzVjYmx4MEozVnpaU0J6ZEhKcFkzUW5PMXh1WEc1Y2RDOHZJQzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1WEhRdkx5QlFVa2xXUVZSRklFWlZUa05VU1U5T1UxeHVYSFF2THlBdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMxY2JseHVYSFF2S2lwY2JseDBJQ29nU1c1cGRHbGhiR2w2WlNCMGFHVWdjR0ZuWlNCdVlXMWxjM0JoWTJWekxseHVYSFFnS2x4dVhIUWdLaUJVYUdseklHMWxkR2h2WkNCM2FXeHNJSE5sWVhKamFDQjBhR1VnY0dGblpTQklWRTFNSUdadmNpQmhkbUZwYkdGaWJHVWdibUZ0WlhOd1lXTmxjeTVjYmx4MElDcGNibHgwSUNvZ1FIQmhjbUZ0SUh0QmNuSmhlWDBnWTI5c2JHVmpkR2x2Ym5NZ1EyOXVkR0ZwYm5NZ2RHaGxJRzF2WkhWc1pTQmpiMnhzWldOMGFXOXVJR2x1YzNSaGJtTmxjeUIwYnlCaVpTQnBibU5zZFdSbFpDQnBiaUIwYUdVZ2JtRnRaWE53WVdObGN5NWNibHgwSUNwY2JseDBJQ29nUUhKbGRIVnliaUI3UVhKeVlYbDlJRkpsZEhWeWJuTWdZVzRnWVhKeVlYa2dkMmwwYUNCMGFHVWdjR0ZuWlNCdVlXMWxjM0JoWTJVZ2JtRnRaWE11WEc1Y2RDQXFYRzVjZENBcUlFQndjbWwyWVhSbFhHNWNkQ0FxTDF4dVhIUm1kVzVqZEdsdmJpQmZhVzVwZEU1aGJXVnpjR0ZqWlhNb1kyOXNiR1ZqZEdsdmJuTXBJSHRjYmx4MFhIUnNaWFFnY0dGblpVNWhiV1Z6Y0dGalpVNWhiV1Z6SUQwZ1cxMDdYRzVjYmx4MFhIUXZMeUJWYzJVZ2RHaGxJR04xYzNSdmJTQndjMlYxWkc4Z2MyVnNaV04wYjNJZ1pHVm1hVzVsWkNCaGRDQmxlSFJsYm1RdWFuTWdhVzRnYjNKa1pYSWdkRzhnWm1WMFkyZ2dkR2hsSUdGMllXbHNZV0pzWlNCdVlXMWxjM0JoWTJWekxseHVYSFJjZEd4bGRDQnViMlJsY3lBOUlFRnljbUY1TG1aeWIyMG9aRzlqZFcxbGJuUXVaMlYwUld4bGJXVnVkSE5DZVZSaFowNWhiV1VvSnlvbktTa3NYRzVjZEZ4MFhIUnlaV2RsZUNBOUlDOWtZWFJoTFNndUtpa3RibUZ0WlhOd1lXTmxMenRjYmx4MFhIUmNibHgwWEhSbWIzSWdLR3hsZENCdWIyUmxJRzltSUc1dlpHVnpLU0I3WEc1Y2RGeDBYSFJtYjNJZ0tHeGxkQ0JoZEhSeWFXSjFkR1VnYjJZZ1FYSnlZWGt1Wm5KdmJTaHViMlJsTG1GMGRISnBZblYwWlhNcEtTQjdYRzVjZEZ4MFhIUmNkR2xtSUNoaGRIUnlhV0oxZEdVdWJtRnRaUzV6WldGeVkyZ29jbVZuWlhncElDRTlQU0F0TVNrZ2UxeHVYSFJjZEZ4MFhIUmNkQzh2SUZCaGNuTmxJSFJvWlNCdVlXMWxjM0JoWTJVZ2JtRnRaU0JoYm1RZ2MyOTFjbU5sSUZWU1RDNWNibHgwWEhSY2RGeDBYSFJzWlhRZ2JtRnRaU0E5SUdGMGRISnBZblYwWlM1dVlXMWxMbkpsY0d4aFkyVW9jbVZuWlhnc0lDY2tNU2NwTEZ4dVhIUmNkRngwWEhSY2RGeDBjMjkxY21ObElEMGdZWFIwY21saWRYUmxMblpoYkhWbE8xeHVYSFJjZEZ4MFhIUmNkRnh1WEhSY2RGeDBYSFJjZEM4dklFTm9aV05ySUdsbUlIUm9aU0J1WVcxbGMzQmhZMlVnYVhNZ1lXeHlaV0ZrZVNCa1pXWnBibVZrTGx4dVhIUmNkRngwWEhSY2RHbG1JQ2h3WVdkbFRtRnRaWE53WVdObFRtRnRaWE11YVc1a1pYaFBaaWh1WVcxbEtTQStJQzB4S1NCN1hHNWNkRngwWEhSY2RGeDBYSFJwWmlBb2QybHVaRzkzVzI1aGJXVmRMbk52ZFhKalpTQWhQVDBnYzI5MWNtTmxLU0I3WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkR3B6WlM1amIzSmxMbVJsWW5WbkxtVnljbTl5S0dCRmJHVnRaVzUwSUhkcGRHZ2dkR2hsSUdSMWNHeHBZMkYwWlNCdVlXMWxjM0JoWTJVZ2JtRnRaVG9nSkh0dWIyUmxmV0FwTzF4dVhIUmNkRngwWEhSY2RGeDBYSFIwYUhKdmR5QnVaWGNnUlhKeWIzSW9ZRlJvWlNCdVlXMWxjM0JoWTJVZ1hDSWtlMjVoYldWOVhDSWdhWE1nWVd4eVpXRmtlU0JrWldacGJtVmtMaUJRYkdWaGMyVWdjMlZzWldOMElHRnViM1JvWlhJZ1lDQXJYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBZRzVoYldVZ1ptOXlJSGx2ZFhJZ2JtRnRaWE53WVdObExtQXBPMXh1WEhSY2RGeDBYSFJjZEZ4MGZWeHVYSFJjZEZ4MFhIUmNkRngwWTI5dWRHbHVkV1U3SUM4dklGUm9aU0J1WVcxbGMzQmhZMlVnYVhNZ1lXeHlaV0ZrZVNCa1pXWnBibVZrTENCamIyNTBhVzUxWlNCc2IyOXdMbHh1WEhSY2RGeDBYSFJjZEgxY2JseDBYSFJjZEZ4MFhIUmNibHgwWEhSY2RGeDBYSFJwWmlBb2MyOTFjbU5sSUQwOVBTQW5KeWtnZTF4dVhIUmNkRngwWEhSY2RGeDBkR2h5YjNjZ2JtVjNJRk41Ym5SaGVFVnljbTl5S0dCT1lXMWxjM0JoWTJVZ2MyOTFjbU5sSUdseklHVnRjSFI1T2lBa2UyNWhiV1Y5WUNrN1hHNWNkRngwWEhSY2RGeDBmVnh1WEhSY2RGeDBYSFJjZEZ4dVhIUmNkRngwWEhSY2RDOHZJRU55WldGMFpTQmhJRzVsZHlCdVlXMWxjM0JoWTJWeklHbHVjM1JoYm1ObElHbHVJSFJvWlNCbmJHOWlZV3dnYzJOdmNHVWdLSFJvWlNCbmJHOWlZV3dnYzJOdmNHVWdhWE1nZFhObFpDQm1iM0lnWEc1Y2RGeDBYSFJjZEZ4MEx5OGdabUZzYkdKaFkyc2djM1Z3Y0c5eWRDQnZaaUJ2YkdRZ2JXOWtkV3hsSUdSbFptbHVhWFJwYjI1ektTNWNibHgwWEhSY2RGeDBYSFJwWmlBb2JtRnRaU0E5UFQwZ0oycHpaU2NwSUhzZ0x5OGdUVzlrYVdaNUlIUm9aU0JsYm1kcGJtVWdiMkpxWldOMElIZHBkR2dnVG1GdFpYTndZV05sSUdGMGRISnBZblYwWlhNdVhHNWNkRngwWEhSY2RGeDBYSFJmWTI5dWRtVnlkRVZ1WjJsdVpWUnZUbUZ0WlhOd1lXTmxLSE52ZFhKalpTd2dZMjlzYkdWamRHbHZibk1wTzF4dVhIUmNkRngwWEhSY2RIMGdaV3h6WlNCN1hHNWNkRngwWEhSY2RGeDBYSFIzYVc1a2IzZGJibUZ0WlYwZ1BTQnVaWGNnYW5ObExtTnZibk4wY25WamRHOXljeTVPWVcxbGMzQmhZMlVvYm1GdFpTd2djMjkxY21ObExDQmpiMnhzWldOMGFXOXVjeWs3WEc1Y2RGeDBYSFJjZEZ4MGZWeHVYSFJjZEZ4MFhIUmNkRnh1WEhSY2RGeDBYSFJjZEhCaFoyVk9ZVzFsYzNCaFkyVk9ZVzFsY3k1d2RYTm9LRzVoYldVcE8xeHVYSFJjZEZ4MFhIUmNkRzV2WkdVdWNtVnRiM1psUVhSMGNtbGlkWFJsS0dGMGRISnBZblYwWlM1dVlXMWxLVHNnWEc1Y2RGeDBYSFJjZEgxY2RGeHVYSFJjZEZ4MGZWeHVYSFJjZEgxY2JseHVYSFJjZEM4dklGUm9jbTkzSUdGdUlHVnljbTl5SUdsbUlHNXZJRzVoYldWemNHRmpaWE1nZDJWeVpTQm1iM1Z1WkM1Y2JseDBYSFJwWmlBb2NHRm5aVTVoYldWemNHRmpaVTVoYldWekxteGxibWQwYUNBOVBUMGdNQ2tnZTF4dVhIUmNkRngwZEdoeWIzY2dibVYzSUVWeWNtOXlLQ2RPYnlCdGIyUjFiR1VnYm1GdFpYTndZV05sY3lCM1pYSmxJR1p2ZFc1a0xDQjNhWFJvYjNWMElHNWhiV1Z6Y0dGalpYTWdhWFFnYVhNZ2JtOTBJSEJ2YzNOcFlteGxJSFJ2SUNjZ0sxeHVYSFJjZEZ4MFhIUW5iRzloWkNCaGJua2diVzlrZFd4bGN5NG5LVHRjYmx4MFhIUjlYRzVjYmx4MFhIUXZMeUJKYm1sMGFXRnNhWHBsSUhSb1pTQnVZVzFsYzNCaFkyVWdhVzV6ZEdGdVkyVnpMbHh1WEhSY2RHeGxkQ0JrWldabGNuSmxaRU52Ykd4bFkzUnBiMjRnUFNCYlhUdGNibHgwWEhSY2JseDBYSFJtYjNJZ0tHeGxkQ0J1WVcxbElHOW1JSEJoWjJWT1lXMWxjM0JoWTJWT1lXMWxjeWtnZTF4dVhIUmNkRngwYkdWMElHUmxabVZ5Y21Wa0lEMGdKQzVFWldabGNuSmxaQ2dwTzF4dVhIUmNkRngwWEc1Y2RGeDBYSFJrWldabGNuSmxaRU52Ykd4bFkzUnBiMjR1Y0hWemFDaGtaV1psY25KbFpDazdYRzVjZEZ4MFhIUmNibHgwWEhSY2RIZHBibVJ2ZDF0dVlXMWxYVnh1WEhSY2RGeDBYSFF1YVc1cGRDZ3BYRzVjZEZ4MFhIUmNkQzVrYjI1bEtHUmxabVZ5Y21Wa0xuSmxjMjlzZG1VcFhHNWNkRngwWEhSY2RDNW1ZV2xzS0dSbFptVnljbVZrTG5KbGFtVmpkQ2xjYmx4MFhIUmNkRngwTG1Gc2QyRjVjeWdvS1NBOVBpQWdhbk5sTG1OdmNtVXVaR1ZpZFdjdWFXNW1ieWduVG1GdFpYTndZV05sSUhCeWIyMXBjMlZ6SUhkbGNtVWdjbVZ6YjJ4MlpXUTZJQ2NzSUc1aGJXVXBLVHRjYmx4MFhIUjlYRzVjYmx4MFhIUXZMeUJVY21sbloyVnlJR0Z1SUdWMlpXNTBJR0ZtZEdWeUlIUm9aU0JsYm1kcGJtVWdhR0Z6SUdsdWFYUnBZV3hwZW1Wa0lHRnNiQ0J1WlhjZ2JXOWtkV3hsY3k1Y2JseDBYSFFrTG5kb1pXNHVZWEJ3Ykhrb2RXNWtaV1pwYm1Wa0xDQmtaV1psY25KbFpFTnZiR3hsWTNScGIyNHBMbUZzZDJGNWN5aG1kVzVqZEdsdmJpZ3BJSHRjYmx4MFhIUmNkR3hsZENCbGRtVnVkQ0E5SUdSdlkzVnRaVzUwTG1OeVpXRjBaVVYyWlc1MEtDZEZkbVZ1ZENjcE8xeHVYSFJjZEZ4MFpYWmxiblF1YVc1cGRFVjJaVzUwS0NkS1UwVk9SMGxPUlY5SlRrbFVYMFpKVGtsVFNFVkVKeXdnZEhKMVpTd2dkSEoxWlNrN1hHNWNkRngwWEhSa2IyTjFiV1Z1ZEM1eGRXVnllVk5sYkdWamRHOXlLQ2RpYjJSNUp5a3VaR2x6Y0dGMFkyaEZkbVZ1ZENobGRtVnVkQ2s3WEc1Y2RGeDBYSFJxYzJVdVkyOXlaUzV5WldkcGMzUnllUzV6WlhRb0oycHpaVVZ1WkZScGJXVW5MQ0J1WlhjZ1JHRjBaU2dwTG1kbGRGUnBiV1VvS1NrN1hHNWNkRngwWEhScWMyVXVZMjl5WlM1a1pXSjFaeTVwYm1adktDZEtVeUJGYm1kcGJtVWdURzloWkdsdVp5QlVhVzFsT2lBbkxDQnFjMlV1WTI5eVpTNXlaV2RwYzNSeWVTNW5aWFFvSjJwelpVVnVaRlJwYldVbktTQmNibHgwWEhSY2RGeDBMU0JxYzJVdVkyOXlaUzV5WldkcGMzUnllUzVuWlhRb0oycHpaVk4wWVhKMFZHbHRaU2NwTENBbmJYTW5LVHRjYmx4MFhIUmNkR2xtSUNoM2FXNWtiM2N1UTNsd2NtVnpjeWtnZTF4dVhIUmNkRngwWEhSM2FXNWtiM2N1YW5ObFVtVmhaSGtnUFNCMGNuVmxPMXh1WEhSY2RGeDBmVnh1WEhSY2RIMHBPMXh1WEc1Y2RGeDBjbVYwZFhKdUlIQmhaMlZPWVcxbGMzQmhZMlZPWVcxbGN6dGNibHgwZlZ4dVhHNWNkQzhxS2x4dVhIUWdLaUJEYjI1MlpYSjBJSFJvWlNCY0ltcHpaVndpSUc5aWFtVmpkQ0IwYnlCaElFNWhiV1Z6Y0dGalpTQmpiMjF3WVhScFlteGxJRzlpYW1WamRDNWNibHgwSUNwY2JseDBJQ29nU1c0Z2IzSmtaWElnZEc4Z2MzVndjRzl5ZENCMGFHVWdYQ0pxYzJWY0lpQnVZVzFsYzNCaFkyVWdibUZ0WlNCbWIzSWdkR2hsSUdOdmNtVWdiVzlrZFd4bGN5QndiR0ZqWldRZ2FXNGdkR2hsSUZ3aVNsTkZibWRwYm1WY0lseHVYSFFnS2lCa2FYSmxZM1J2Y25rc0lIZGxJSGRwYkd3Z2JtVmxaQ0IwYnlCdGIyUnBabmtnZEdobElHRnNjbVZoWkhrZ1pYaHBjM1JwYm1jZ1hDSnFjMlZjSWlCdlltcGxZM1FnYzI4Z2RHaGhkQ0JwZENCallXNGdiM0JsY21GMFpWeHVYSFFnS2lCaGN5QmhJRzVoYldWemNHRmpaU0IzYVhSb2IzVjBJR3h2YzJsdVp5QnBkSE1nYVc1cGRHbGhiQ0JoZEhSeWFXSjFkR1Z6TGx4dVhIUWdLbHh1WEhRZ0tpQkFjR0Z5WVcwZ2UxTjBjbWx1WjMwZ2MyOTFjbU5sSUU1aGJXVnpjR0ZqWlNCemIzVnlZMlVnY0dGMGFDQm1iM0lnZEdobElHMXZaSFZzWlNCbWFXeGxjeTVjYmx4MElDb2dRSEJoY21GdElIdEJjbkpoZVgwZ1kyOXNiR1ZqZEdsdmJuTWdRMjl1ZEdGcGJpQnBibk4wWVc1alpYTWdkRzhnZEdobElIQnliM1J2ZEhsd1pTQmpiMnhzWldOMGFXOXVJR2x1YzNSaGJtTmxjeTVjYmx4MElDcGNibHgwSUNvZ1FIQnlhWFpoZEdWY2JseDBJQ292WEc1Y2RHWjFibU4wYVc5dUlGOWpiMjUyWlhKMFJXNW5hVzVsVkc5T1lXMWxjM0JoWTJVb2MyOTFjbU5sTENCamIyeHNaV04wYVc5dWN5a2dlMXh1WEhSY2RHeGxkQ0IwYlhCT1lXMWxjM0JoWTJVZ1BTQnVaWGNnYW5ObExtTnZibk4wY25WamRHOXljeTVPWVcxbGMzQmhZMlVvSjJwelpTY3NJSE52ZFhKalpTd2dZMjlzYkdWamRHbHZibk1wTzF4dVhIUmNkR3B6WlM1dVlXMWxJRDBnZEcxd1RtRnRaWE53WVdObExtNWhiV1U3WEc1Y2RGeDBhbk5sTG5OdmRYSmpaU0E5SUhSdGNFNWhiV1Z6Y0dGalpTNXpiM1Z5WTJVN1hHNWNkRngwYW5ObExtTnZiR3hsWTNScGIyNXpJRDBnZEcxd1RtRnRaWE53WVdObExtTnZiR3hsWTNScGIyNXpPMXh1WEhSY2RHcHpaUzVwYm1sMElEMGdhbk5sTG1OdmJuTjBjblZqZEc5eWN5NU9ZVzFsYzNCaFkyVXVjSEp2ZEc5MGVYQmxMbWx1YVhRN1hHNWNkSDFjYmx4dVhIUXZMeUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzFjYmx4MEx5OGdVRlZDVEVsRElFWlZUa05VU1U5T1UxeHVYSFF2THlBdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMxY2JseHVYSFF2S2lwY2JseDBJQ29nU1c1cGRHbGhiR2w2WlNCMGFHVWdaVzVuYVc1bExseHVYSFFnS2x4dVhIUWdLaUJBY0dGeVlXMGdlMEZ5Y21GNWZTQmpiMnhzWldOMGFXOXVjeUJEYjI1MFlXbHVjeUIwYUdVZ2MzVndjRzl5ZEdWa0lHMXZaSFZzWlNCamIyeHNaV04wYVc5dUlHUmhkR0V1WEc1Y2RDQXFMMXh1WEhSbGVIQnZjblJ6TG1sdWFYUWdQU0JtZFc1amRHbHZiaUFvWTI5c2JHVmpkR2x2Ym5NcElIdGNibHgwWEhRdkx5QkhiRzlpWVd3Z1pYSnliM0lnYUdGdVpHeGxjaUIwYUdGMElHVjRaV04xZEdWeklHbG1JR0Z1SUhWdVkyRjFaMmgwSUVwVElHVnljbTl5SUc5alkzVnljeUJ2YmlCd1lXZGxMbHh1WEhSY2RHcHpaUzVqYjNKbExtUmxZblZuTG1KcGJtUkhiRzlpWVd4RmNuSnZja2hoYm1Sc1pYSW9LVHNnWEc1Y2JseDBYSFF2THlCSmJtbDBhV0ZzYVhwbElIUm9aU0J3WVdkbElHNWhiV1Z6Y0dGalpYTXVYRzVjZEZ4MGJHVjBJSEJoWjJWT1lXMWxjM0JoWTJWT1lXMWxjeUE5SUY5cGJtbDBUbUZ0WlhOd1lXTmxjeWhqYjJ4c1pXTjBhVzl1Y3lrN1hHNWNibHgwWEhRdkx5Qk1iMmNnZEdobElIQmhaMlVnYm1GdFpYTndZV05sY3lBb1ptOXlJR1JsWW5WbloybHVaeUJ2Ym14NUtTNWNibHgwWEhScWMyVXVZMjl5WlM1a1pXSjFaeTVwYm1adktDZFFZV2RsSUU1aGJXVnpjR0ZqWlhNNklDY2dLeUJ3WVdkbFRtRnRaWE53WVdObFRtRnRaWE11YW05cGJpZ3BLVHRjYmx4dVhIUmNkQzh2SUZWd1pHRjBaU0IwYUdVZ1pXNW5hVzVsSUhKbFoybHpkSEo1TGx4dVhIUmNkR3B6WlM1amIzSmxMbkpsWjJsemRISjVMbk5sZENnbmJtRnRaWE53WVdObGN5Y3NJSEJoWjJWT1lXMWxjM0JoWTJWT1lXMWxjeWs3WEc1Y2RIMDdYRzVjYm4wcEtHcHpaUzVqYjNKbExtVnVaMmx1WlNrN1hHNGlMQ0l2S2lBdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFZ4dUlHVjRkR1Z1YzJsdmJuTXVhbk1nTWpBeE55MHdNeTB3TTF4dUlFZGhiV0pwYnlCSGJXSklYRzRnYUhSMGNEb3ZMM2QzZHk1bllXMWlhVzh1WkdWY2JpQkRiM0I1Y21sbmFIUWdLR01wSURJd01UY2dSMkZ0WW1sdklFZHRZa2hjYmlCU1pXeGxZWE5sWkNCMWJtUmxjaUIwYUdVZ1IwNVZJRWRsYm1WeVlXd2dVSFZpYkdsaklFeHBZMlZ1YzJVZ0tGWmxjbk5wYjI0Z01pbGNiaUJiYUhSMGNEb3ZMM2QzZHk1bmJuVXViM0puTDJ4cFkyVnVjMlZ6TDJkd2JDMHlMakF1YUhSdGJGMWNiaUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1SUNvdlhHNWNiaThxS2x4dUlDb2dTbE5GSUVWNGRHVnVjMmx2Ym5OY2JpQXFYRzRnS2lCRmVIUmxibVFnZEdobElHUmxabUYxYkhRZ1ltVm9ZWFpwYjNWeUlHOW1JR1Z1WjJsdVpTQmpiMjF3YjI1bGJuUnpJRzl5SUdWNGRHVnlibUZzSUhCc2RXZHBibk1nWW1WbWIzSmxJSFJvWlhrZ1lYSmxJR3h2WVdSbFpDNWNiaUFxWEc0Z0tpQkFiVzlrZFd4bElFcFRSUzlEYjNKbEwyVjRkR1Z1WkZ4dUlDb3ZYRzRvWm5WdVkzUnBiMjRnS0NrZ2UxeHVYRzVjZENkMWMyVWdjM1J5YVdOMEp6dGNibHgwWEc1Y2RDOHZJQzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1WEhRdkx5QlFRVkpUUlNCTlQwUlZURVVnUkVGVVFTQktVVlZGVWxrZ1JWaFVSVTVUU1U5T1hHNWNkQzh2SUMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFZ4dVhIUmNibHgwSkM1bWJpNWxlSFJsYm1Rb2UxeHVYSFJjZEhCaGNuTmxUVzlrZFd4bFJHRjBZVG9nWm5WdVkzUnBiMjRvYlc5a2RXeGxUbUZ0WlNrZ2UxeHVYSFJjZEZ4MGFXWWdLQ0Z0YjJSMWJHVk9ZVzFsSUh4OElHMXZaSFZzWlU1aGJXVWdQVDA5SUNjbktTQjdYRzVjZEZ4MFhIUmNkSFJvY205M0lHNWxkeUJGY25KdmNpZ25UVzlrZFd4bElHNWhiV1VnZDJGeklHNXZkQ0J3Y205MmFXUmxaQ0JoY3lCaGJpQmhjbWQxYldWdWRDNG5LVnh1WEhSY2RGeDBmVnh1WEhSY2RGeDBYRzVjZEZ4MFhIUmpiMjV6ZENCcGJtbDBhV0ZzUkdGMFlTQTlJQ1FvZEdocGN5a3VaR0YwWVNncE8xeHVYSFJjZEZ4MFkyOXVjM1FnWm1sc2RHVnlaV1JFWVhSaElEMGdlMzA3WEc1Y2RGeDBYSFJjYmx4MFhIUmNkQzh2SUZObFlYSmphR1Z6SUdadmNpQnRiMlIxYkdVZ2NtVnNaWFpoYm5RZ1pHRjBZU0JwYm5OcFpHVWdkR2hsSUcxaGFXNHRaR0YwWVMxdlltcGxZM1F1SUVSaGRHRWdabTl5SUc5MGFHVnlJSGRwWkdkbGRITWdkMmxzYkNCdWIzUWdaMlYwSUZ4dVhIUmNkRngwTHk4Z2NHRnpjMlZrSUhSdklIUm9hWE1nZDJsa1oyVjBMbHh1WEhSY2RGeDBKQzVsWVdOb0tHbHVhWFJwWVd4RVlYUmhMQ0JtZFc1amRHbHZiaUFvYTJWNUxDQjJZV3gxWlNrZ2UxeHVYSFJjZEZ4MFhIUnBaaUFvYTJWNUxtbHVaR1Y0VDJZb2JXOWtkV3hsVG1GdFpTa2dQVDA5SURBZ2ZId2dhMlY1TG1sdVpHVjRUMllvYlc5a2RXeGxUbUZ0WlM1MGIweHZkMlZ5UTJGelpTZ3BLU0E5UFQwZ01Da2dlMXh1WEhSY2RGeDBYSFJjZEd4bGRDQnVaWGRMWlhrZ1BTQnJaWGt1YzNWaWMzUnlLRzF2WkhWc1pVNWhiV1V1YkdWdVozUm9LVHRjYmx4MFhIUmNkRngwWEhSdVpYZExaWGtnUFNCdVpYZExaWGt1YzNWaWMzUnlLREFzSURFcExuUnZURzkzWlhKRFlYTmxLQ2tnS3lCdVpYZExaWGt1YzNWaWMzUnlLREVwTzF4dVhIUmNkRngwWEhSY2RHWnBiSFJsY21Wa1JHRjBZVnR1WlhkTFpYbGRJRDBnZG1Gc2RXVTdYRzVjZEZ4MFhIUmNkSDFjYmx4MFhIUmNkSDBwTzF4dVhIUmNkRngwWEc1Y2RGeDBYSFJ5WlhSMWNtNGdabWxzZEdWeVpXUkVZWFJoTzF4dVhIUmNkSDFjYmx4MGZTazdYRzVjYmx4MEx5OGdMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRYRzVjZEM4dklFUkJWRVZRU1VOTFJWSWdVa1ZIU1U5T1FVd2dTVTVHVDF4dVhIUXZMeUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzFjYmx4dVhIUnBaaUFvSkM1a1lYUmxjR2xqYTJWeUlDRTlQU0IxYm1SbFptbHVaV1Fwd3FCN1hHNWNkRngwSkM1a1lYUmxjR2xqYTJWeUxuSmxaMmx2Ym1Gc0xtUmxJRDBnZTF4dVhIUmNkRngwWkdGMFpVWnZjbTFoZERvZ0oyUmtMbTF0TG5sNUp5eGNibHgwWEhSY2RHWnBjbk4wUkdGNU9pQXhMRnh1WEhSY2RGeDBhWE5TVkV3NklHWmhiSE5sWEc1Y2RGeDBmVHRjYmx4MFhIUWtMbVJoZEdWd2FXTnJaWEl1YzJWMFJHVm1ZWFZzZEhNb0pDNWtZWFJsY0dsamEyVnlMbkpsWjJsdmJtRnNMbVJsS1R0Y2JseDBmVnh1ZlNncEtUdGNiaUlzSWk4cUlDMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdFhHNGdhVzVwZEdsaGJHbDZaUzVxY3lBeU1ERTJMVEE1TFRBNFhHNGdSMkZ0WW1sdklFZHRZa2hjYmlCb2RIUndPaTh2ZDNkM0xtZGhiV0pwYnk1a1pWeHVJRU52Y0hseWFXZG9kQ0FvWXlrZ01qQXhOaUJIWVcxaWFXOGdSMjFpU0Z4dUlGSmxiR1ZoYzJWa0lIVnVaR1Z5SUhSb1pTQkhUbFVnUjJWdVpYSmhiQ0JRZFdKc2FXTWdUR2xqWlc1elpTQW9WbVZ5YzJsdmJpQXlLVnh1SUZ0b2RIUndPaTh2ZDNkM0xtZHVkUzV2Y21jdmJHbGpaVzV6WlhNdlozQnNMVEl1TUM1b2RHMXNYVnh1SUMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0WEc0Z0tpOWNibHh1SjNWelpTQnpkSEpwWTNRbk95QmNibHh1THlvcVhHNGdLaUJLVTBVZ1NXNXBkR2xoYkdsNllYUnBiMjRnVFc5a2RXeGxYRzRnS2x4dUlDb2dWR2hsSUdSdlkzVnRaVzUwTFhKbFlXUjVJR1YyWlc1MElHOW1JSFJvWlNCd1lXZGxJSGRwYkd3Z2RISnBaMmRsY2lCMGFHVWdTbUYyWVZOamNtbHdkQ0JGYm1kcGJtVWdhVzVwZEdsaGJHbDZZWFJwYjI0dUlGUm9aVnh1SUNvZ1pXNW5hVzVsSUhKbGNYVnBjbVZ6SUdFZ1oyeHZZbUZzSUdOdmJtWnBaM1Z5WVhScGIyNGdiMkpxWldOMElGd2lkMmx1Wkc5M0xrcFRSVzVuYVc1bFEyOXVabWxuZFhKaGRHbHZibHdpSUhSdklHSmxJSEJ5WlMxa1pXWnBibVZrWEc0Z0tpQnBiaUJ2Y21SbGNpQjBieUJ5WlhSeWFXVjJaU0IwYUdVZ1ltRnphV01nWTI5dVptbG5kWEpoZEdsdmJpQnBibVp2TGlCQlpuUmxjaUJoSUhOMVkyTmxjM05tZFd3Z2FXNXBkR2xoYkdsNllYUnBiMjRnZEdocGN5QnZZbXBsWTNSY2JpQXFJR2x6SUhKbGJXOTJaV1FnWm5KdmJTQjBhR1VnZDJsdVpHOTNJRzlpYW1WamRDNWNiaUFxWEc0Z0tpQWpJeU1nUTI5dVptbG5kWEpoZEdsdmJpQlRZVzF3YkdWY2JpQXFYRzRnS2lCZ1lHQnFjMXh1SUNvZ2QybHVaRzkzTGtwVFJXNW5hVzVsUTI5dVptbG5kWEpoZEdsdmJpQTlJSHRjYmlBcUlDQWdaVzUyYVhKdmJtMWxiblE2SUNkd2NtOWtkV04wYVc5dUp5eGNiaUFxSUNBZ1lYQndWWEpzT2lBbmFIUjBjRG92TDJGd2NDNWpiMjBuTEZ4dUlDb2dJQ0JqYjJ4c1pXTjBhVzl1Y3pvZ1cxeHVJQ29nSUNBZ0lIdHVZVzFsT2lBblkyOXVkSEp2Ykd4bGNuTW5MQ0JoZEhSeWFXSjFkR1U2SUNkamIyNTBjbTlzYkdWeUozMWNiaUFxSUNBZ1hTd2dJRnh1SUNvZ0lDQjBjbUZ1YzJ4aGRHbHZibk02SUh0Y2JpQXFJQ0FnSUNBbmMyVmpkR2x2Yms1aGJXVW5PaUI3SUNkMGNtRnVjMnhoZEdsdmJrdGxlU2M2SUNkMGNtRnVjMnhoZEdsdmJsWmhiSFZsSnlCOUxGeHVJQ29nSUNBZ0lDZGhibTkwYUdWeVUyVmpkR2x2YmljNklIc2dMaTR1SUgxY2JpQXFJQ0FnZlN4Y2JpQXFJQ0FnYkdGdVozVmhaMlZEYjJSbE9pQW5aVzRuTEZ4dUlDb2dJQ0J3WVdkbFZHOXJaVzQ2SUNjNVlYTmtOMlk1T0RjNWMyUTRaamM1Y3prNGN6ZGtPVGhtSjF4dUlDb2dmVHRjYmlBcUlHQmdZRnh1SUNwY2JpQXFJRUJ0YjJSMWJHVWdTbE5GTDBOdmNtVXZhVzVwZEdsaGJHbDZaVnh1SUNvdlhHNWNiaTh2SUVsdWFYUnBZV3hwZW1VZ1ltRnpaU0JsYm1kcGJtVWdiMkpxWldOMExpQkZkbVZ5ZVNCdmRHaGxjaUJ3WVhKMElHOW1JSFJvWlNCbGJtZHBibVVnZDJsc2JDQnlaV1psY2lCMGJ5QjBhR2x6WEc0dkx5QmpaVzUwY21Gc0lHOWlhbVZqZENCbWIzSWdkR2hsSUdOdmNtVWdiM0JsY21GMGFXOXVjeTVjYm5kcGJtUnZkeTVxYzJVZ1BTQjdYRzVjZEdOdmNtVTZJSHQ5TEZ4dVhIUnNhV0p6T2lCN2ZTeGNibHgwWTI5dWMzUnlkV04wYjNKek9pQjdmVnh1ZlR0Y2JseHVMeThnU1c1cGRHbGhiR2w2WlNCMGFHVWdaVzVuYVc1bElHOXVJSGRwYm1SdmR5QnNiMkZrTGlCY2JtUnZZM1Z0Wlc1MExtRmtaRVYyWlc1MFRHbHpkR1Z1WlhJb0owUlBUVU52Ym5SbGJuUk1iMkZrWldRbkxDQm1kVzVqZEdsdmJpZ3BJSHRjYmx4MGRISjVJSHRjYmx4MFhIUXZMeUJEYUdWamF5QnBaaUJuYkc5aVlXd2dTbE5GYm1kcGJtVkRiMjVtYVdkMWNtRjBhVzl1SUc5aWFtVmpkQ0JwY3lCa1pXWnBibVZrTGx4dVhIUmNkR2xtSUNoM2FXNWtiM2N1U2xORmJtZHBibVZEYjI1bWFXZDFjbUYwYVc5dUlEMDlQU0IxYm1SbFptbHVaV1FwSUh0Y2JseDBYSFJjZEhSb2NtOTNJRzVsZHlCRmNuSnZjaWduVkdobElGd2lkMmx1Wkc5M0xrcFRSVzVuYVc1bFEyOXVabWxuZFhKaGRHbHZibHdpSUc5aWFtVmpkQ0JwY3lCdWIzUWdaR1ZtYVc1bFpDQnBiaUIwYUdVZ1oyeHZZbUZzSUhOamIzQmxMaUFuSUN0Y2JseDBYSFJjZEZ4MEoxUm9hWE1nYjJKcVpXTjBJR2x6SUhKbGNYVnBjbVZrSUdKNUlIUm9aU0JsYm1kcGJtVWdkWEJ2YmlCcGRITWdhVzVwZEdsaGJHbDZZWFJwYjI0dUp5azdYRzVjZEZ4MGZWeHVYSFJjZEZ4dVhIUmNkQzh2SUZCaGNuTmxJRXBUUlc1bmFXNWxRMjl1Wm1sbmRYSmhkR2x2YmlCdlltcGxZM1F1WEc1Y2RGeDBhbk5sTG1OdmNtVXVZMjl1Wm1sbkxtbHVhWFFvZDJsdVpHOTNMa3BUUlc1bmFXNWxRMjl1Wm1sbmRYSmhkR2x2YmlrN1hHNWNkRngwWEc1Y2RGeDBMeThnVTNSdmNtVWdkR2hsSUVwVFJTQnpkR0Z5ZENCMGFXMWxJR2x1SUhKbFoybHpkSEo1SUNod2NtOW1hV3hwYm1jcExpQmNibHgwWEhScWMyVXVZMjl5WlM1eVpXZHBjM1J5ZVM1elpYUW9KMnB6WlZOMFlYSjBWR2x0WlNjc0lFUmhkR1V1Ym05M0tDa3BPMXh1WEhSY2RGeHVYSFJjZEM4dklFbHVhWFJwWVd4cGVtVWdkR2hsSUcxdlpIVnNaU0JqYjJ4c1pXTjBhVzl1Y3k1Y2JseDBYSFJxYzJVdVkyOXlaUzVsYm1kcGJtVXVhVzVwZENocWMyVXVZMjl5WlM1amIyNW1hV2N1WjJWMEtDZGpiMnhzWldOMGFXOXVjeWNwS1R0Y2JseDBmU0JqWVhSamFDQW9aWGhqWlhCMGFXOXVLU0I3WEc1Y2RGeDBhbk5sTG1OdmNtVXVaR1ZpZFdjdVpYSnliM0lvSjFWdVpYaHdaV04wWldRZ1pYSnliM0lnWkhWeWFXNW5JRXBUSUVWdVoybHVaU0JwYm1sMGFXRnNhWHBoZEdsdmJpRW5MQ0JsZUdObGNIUnBiMjRwTzF4dVhIUmNkQzh2SUVsdVptOXliU0IwYUdVZ1pXNW5hVzVsSUdGaWIzVjBJSFJvWlNCbGVHTmxjSFJwYjI0dVhHNWNkRngwWTI5dWMzUWdaWFpsYm5RZ1BTQmtiMk4xYldWdWRDNWpjbVZoZEdWRmRtVnVkQ2duUTNWemRHOXRSWFpsYm5RbktUc2dYRzVjZEZ4MFpYWmxiblF1YVc1cGRFTjFjM1J2YlVWMlpXNTBLQ2RsY25KdmNpY3NJSFJ5ZFdVc0lIUnlkV1VzSUdWNFkyVndkR2x2YmlrN1hHNWNkRngwZDJsdVpHOTNMbVJwYzNCaGRHTm9SWFpsYm5Rb1pYWmxiblFwT3lCY2JseDBmVnh1ZlNrN0lGeHVJaXdpTHlvZ0xTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMxY2JpQnNZVzVuTG1weklESXdNVFl0TURndE1qTmNiaUJIWVcxaWFXOGdSMjFpU0Z4dUlHaDBkSEE2THk5M2QzY3VaMkZ0WW1sdkxtUmxYRzRnUTI5d2VYSnBaMmgwSUNoaktTQXlNREUySUVkaGJXSnBieUJIYldKSVhHNGdVbVZzWldGelpXUWdkVzVrWlhJZ2RHaGxJRWRPVlNCSFpXNWxjbUZzSUZCMVlteHBZeUJNYVdObGJuTmxJQ2hXWlhKemFXOXVJRElwWEc0Z1cyaDBkSEE2THk5M2QzY3VaMjUxTG05eVp5OXNhV05sYm5ObGN5OW5jR3d0TWk0d0xtaDBiV3hkWEc0Z0xTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMxY2JpQXFMMXh1WEc1cWMyVXVZMjl5WlM1c1lXNW5JRDBnYW5ObExtTnZjbVV1YkdGdVp5QjhmQ0I3ZlR0Y2JseHVMeW9xWEc0Z0tpQktVMFVnVEc5allXeHBlbUYwYVc5dUlFeHBZbkpoY25sY2JpQXFYRzRnS2lCVWFHVWdaMnh2WW1Gc0lFeGhibWNnYjJKcVpXTjBJR052Ym5SaGFXNXpJR3hoYm1kMVlXZGxJR2x1Wm05eWJXRjBhVzl1SUhSb1lYUWdZMkZ1SUdKbElHVmhjMmxzZVNCMWMyVmtJR2x1SUhsdmRYSmNiaUFxSUVwaGRtRlRZM0pwY0hRZ1kyOWtaUzRnVkdobElHOWlhbVZqZENCamIyNTBZV2x1Y3lCamIyNXpkR0Z1WTJVZ2RISmhibk5zWVhScGIyNXpJR0Z1WkNCa2VXNWhiV2xqSUhObFkzUnBiMjV6SUhSb1lYUmNiaUFxSUdOaGJpQmlaU0JzYjJGa1pXUWdZVzVrSUhWelpXUWdhVzRnWkdsbVptVnlaVzUwSUhCaFoyVXVYRzRnS2x4dUlDb2dJeU1qSXlCSmJYQnZjblJoYm5SY2JpQXFJRlJvWlNCbGJtZHBibVVnZDJsc2JDQmhkWFJ2YldGMGFXTmhiR3g1SUd4dllXUWdkSEpoYm5Oc1lYUnBiMjRnYzJWamRHbHZibk1nZEdoaGRDQmhjbVVnY0hKbGMyVnVkQ0JwYmlCMGFHVmNiaUFxSUdCM2FXNWtiM2N1U2xORmJtZHBibVZEYjI1bWFXZDFjbUYwYVc5dUxuUnlZVzV6YkdGMGFXOXVjMkFnY0hKdmNHVnlkSGtnZFhCdmJpQnBibWwwYVdGc2FYcGhkR2x2Ymk0Z1JtOXlJRzF2Y21WY2JpQXFJR2x1Wm05eWJXRjBhVzl1SUd4dmIyc2dZWFFnZEdobElGd2lZMjl5WlM5cGJtbDBhV0ZzYVhwbFhDSWdjR0ZuWlNCdlppQmtiMk4xYldWdWRHRjBhVzl1SUhKbFptVnlaVzVqWlM1Y2JpQXFYRzRnS2lCZ1lHQnFZWFpoYzJOeWFYQjBYRzRnS2lCcWMyVXVZMjl5WlM1c1lXNW5MbUZrWkZObFkzUnBiMjRvSjNObFkzUnBiMjVPWVcxbEp5d2dleUIwY21GdWMyeGhkR2x2Ymt0bGVUb2dKM1J5WVc1emJHRjBhVzl1Vm1Gc2RXVW5JSDBwT3lBdkx5QkJaR1FnZEhKaGJuTnNZWFJwYjI0Z2MyVmpkR2x2Ymk1Y2JpQXFJR3B6WlM1amIzSmxMblJ5WVc1emJHRjBaU2duZEhKaGJuTnNZWFJwYjI1TFpYa25MQ0FuYzJWamRHbHZiazVoYldVbktUc2dMeThnUjJWMElIUm9aU0IwY21GdWMyeGhkR1ZrSUhOMGNtbHVaeTVjYmlBcUlHcHpaUzVqYjNKbExtZGxkRk5sWTNScGIyNXpLQ2s3SUM4dklISmxkSFZ5Ym5NZ1lYSnlZWGtnZDJsMGFDQnpaV04wYVc5dWN5QmxMbWN1SUZzbllXUnRhVzVmWW5WMGRHOXVjeWNzSUNkblpXNWxjbUZzSjExY2JpQXFJR0JnWUZ4dUlDcGNiaUFxSUVCdGIyUjFiR1VnU2xORkwwTnZjbVV2YkdGdVoxeHVJQ292WEc0b1puVnVZM1JwYjI0Z0tHVjRjRzl5ZEhNcElIdGNibHh1WEhRbmRYTmxJSE4wY21samRDYzdYRzVjYmx4MEx5OGdMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRYRzVjZEM4dklGWkJVa2xCUWt4RlUxeHVYSFF2THlBdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMxY2JseHVYSFF2S2lwY2JseDBJQ29nUTI5dWRHRnBibk1nZG1GeWFXOTFjeUIwY21GdWMyeGhkR2x2YmlCelpXTjBhVzl1Y3k1Y2JseDBJQ3BjYmx4MElDb2dRSFI1Y0dVZ2UwOWlhbVZqZEgxY2JseDBJQ292WEc1Y2RHTnZibk4wSUhObFkzUnBiMjV6SUQwZ2UzMDdYRzVjYmx4MEx5OGdMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRYRzVjZEM4dklGQlZRa3hKUXlCTlJWUklUMFJUWEc1Y2RDOHZJQzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1WEc1Y2RDOHFLbHh1WEhRZ0tpQkJaR1FnWVNCMGNtRnVjMnhoZEdsdmJpQnpaV04wYVc5dUxseHVYSFFnS2x4dVhIUWdLaUJBY0dGeVlXMGdlMU4wY21sdVozMGdibUZ0WlNCT1lXMWxJRzltSUhSb1pTQnpaV04wYVc5dUxDQjFjMlZrSUd4aGRHVnlJR1p2Y2lCaFkyTmxjM05wYm1jZ2RISmhibk5zWVhScGIyNGdjM1J5YVc1bmN5NWNibHgwSUNvZ1FIQmhjbUZ0SUh0UFltcGxZM1I5SUhSeVlXNXpiR0YwYVc5dWN5QkxaWGtnTFNCMllXeDFaU0J2WW1wbFkzUWdZMjl1ZEdGcGJtbHVaeUIwYUdVZ2RISmhibk5zWVhScGIyNXpMbHh1WEhRZ0tseHVYSFFnS2lCQWRHaHliM2R6SUh0RmNuSnZjbjBnU1dZZ1hDSnVZVzFsWENJZ2IzSWdYQ0owY21GdWMyeGhkR2x2Ym5OY0lpQmhjbWQxYldWdWRITWdZWEpsSUdsdWRtRnNhV1F1WEc1Y2RDQXFMMXh1WEhSbGVIQnZjblJ6TG1Ga1pGTmxZM1JwYjI0Z1BTQm1kVzVqZEdsdmJpQW9ibUZ0WlN3Z2RISmhibk5zWVhScGIyNXpLU0I3WEc1Y2RGeDBhV1lnS0hSNWNHVnZaaUJ1WVcxbElDRTlQU0FuYzNSeWFXNW5KeUI4ZkNCMGVYQmxiMllnZEhKaGJuTnNZWFJwYjI1eklDRTlQU0FuYjJKcVpXTjBKeUI4ZkNCMGNtRnVjMnhoZEdsdmJuTWdQVDA5SUc1MWJHd3BJSHRjYmx4MFhIUmNkSFJvY205M0lHNWxkeUJGY25KdmNpaGdkMmx1Wkc5M0xtZDRMbU52Y21VdWJHRnVaeTVoWkdSVFpXTjBhVzl1T2lCSmJuWmhiR2xrSUdGeVozVnRaVzUwY3lCd2NtOTJhV1JsWkNBb2JtRnRaVG9nSkh0MGVYQmxiMllnYm1GdFpYMHNJR0FnWEc1Y2RGeDBYSFFnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdLeUJnZEhKaGJuTnNZWFJwYjI1ek9pQWtlM1I1Y0dWdlppQjBjbUZ1YzJ4aGRHbHZibk45S1M1Z0tUdGNibHgwWEhSOVhHNWNkRngwYzJWamRHbHZibk5iYm1GdFpWMGdQU0IwY21GdWMyeGhkR2x2Ym5NN1hHNWNkSDA3WEc1Y2JseDBMeW9xWEc1Y2RDQXFJRWRsZENCc2IyRmtaV1FnZEhKaGJuTnNZWFJwYjI0Z2MyVmpkR2x2Ym5NdVhHNWNkQ0FxWEc1Y2RDQXFJRlZ6WldaMWJDQm1iM0lnWVhOelpYSjBhVzVuSUhCeVpYTmxiblFnZEhKaGJuTnNZWFJwYjI0Z2MyVmpkR2x2Ym5NdVhHNWNkQ0FxWEc1Y2RDQXFJRUJ5WlhSMWNtNGdlMEZ5Y21GNWZTQlNaWFIxY201eklHRnljbUY1SUhkcGRHZ2dkR2hsSUdWNGFYTjBhVzVuSUhObFkzUnBiMjV6TGx4dVhIUWdLaTljYmx4MFpYaHdiM0owY3k1blpYUlRaV04wYVc5dWN5QTlJR1oxYm1OMGFXOXVJQ2dwSUh0Y2JseDBYSFJqYjI1emRDQnlaWE4xYkhRZ1BTQmJYVHRjYmx4MFhIUmNibHgwWEhSbWIzSWdLR3hsZENCelpXTjBhVzl1SUdsdUlITmxZM1JwYjI1ektTQjdYRzVjZEZ4MFhIUnlaWE4xYkhRdWNIVnphQ2h6WldOMGFXOXVLVHRjYmx4MFhIUjlYRzVjZEZ4MFhHNWNkRngwY21WMGRYSnVJSEpsYzNWc2REdGNibHgwZlR0Y2JseHVYSFF2S2lwY2JseDBJQ29nVkhKaGJuTnNZWFJsSUhOMGNtbHVaeUJwYmlCS1lYWmhjMk55YVhCMElHTnZaR1V1WEc1Y2RDQXFYRzVjZENBcUlFQndZWEpoYlNCN1UzUnlhVzVuZlNCd2FISmhjMlVnVG1GdFpTQnZaaUIwYUdVZ2NHaHlZWE5sSUdOdmJuUmhhVzVwYm1jZ2RHaGxJSFJ5WVc1emJHRjBhVzl1TGx4dVhIUWdLaUJBY0dGeVlXMGdlMU4wY21sdVozMGdjMlZqZEdsdmJpQlRaV04wYVc5dUlHNWhiV1VnWTI5dWRHRnBibWx1WnlCMGFHVWdkSEpoYm5Oc1lYUnBiMjRnYzNSeWFXNW5MbHh1WEhRZ0tseHVYSFFnS2lCQWNtVjBkWEp1SUh0VGRISnBibWQ5SUZKbGRIVnlibk1nZEdobElIUnlZVzV6YkdGMFpXUWdjM1J5YVc1bkxseHVYSFFnS2x4dVhIUWdLaUJBZEdoeWIzZHpJSHRGY25KdmNuMGdTV1lnY0hKdmRtbGtaV1FnWVhKbmRXMWxiblJ6SUdGeVpTQnBiblpoYkdsa0xseHVYSFFnS2lCQWRHaHliM2R6SUh0RmNuSnZjbjBnU1dZZ2NtVnhkV2x5WldRZ2MyVmpkR2x2YmlCa2IyVnpJRzV2ZENCbGVHbHpkQ0J2Y2lCMGNtRnVjMnhoZEdsdmJpQmpiM1ZzWkNCdWIzUWdZbVVnWm05MWJtUXVYRzVjZENBcUwxeHVYSFJsZUhCdmNuUnpMblJ5WVc1emJHRjBaU0E5SUdaMWJtTjBhVzl1SUNod2FISmhjMlVzSUhObFkzUnBiMjRwSUh0Y2JseDBYSFF2THlCV1lXeHBaR0YwWlNCd2NtOTJhV1JsWkNCaGNtZDFiV1Z1ZEhNdVhHNWNkRngwYVdZZ0tIUjVjR1Z2WmlCd2FISmhjMlVnSVQwOUlDZHpkSEpwYm1jbklIeDhJSFI1Y0dWdlppQnpaV04wYVc5dUlDRTlQU0FuYzNSeWFXNW5KeWtnZTF4dVhIUmNkRngwZEdoeWIzY2dibVYzSUVWeWNtOXlLR0JKYm5aaGJHbGtJR0Z5WjNWdFpXNTBjeUJ3Y205MmFXUmxaQ0JwYmlCMGNtRnVjMnhoZEdVZ2JXVjBhRzlrSUNod2FISmhjMlU2SUNSN2RIbHdaVzltSUhCb2NtRnpaWDBzSUdCY2JseDBYSFJjZENBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FySUdCelpXTjBhVzl1T2lBa2UzUjVjR1Z2WmlCelpXTjBhVzl1ZlNrdVlDazdYRzVjZEZ4MGZWeHVYRzVjZEZ4MEx5OGdRMmhsWTJzZ2FXWWdkSEpoYm5Oc1lYUnBiMjRnWlhocGMzUnpMbHh1WEhSY2RHbG1JQ2h6WldOMGFXOXVjMXR6WldOMGFXOXVYU0E5UFQwZ2RXNWtaV1pwYm1Wa0lIeDhJSE5sWTNScGIyNXpXM05sWTNScGIyNWRXM0JvY21GelpWMGdQVDA5SUhWdVpHVm1hVzVsWkNrZ2UxeHVYSFJjZEZ4MGFuTmxMbU52Y21VdVpHVmlkV2N1ZDJGeWJpaGdRMjkxYkdRZ2JtOTBJR1p2ZFc1a0lISmxjWFZsYzNSbFpDQjBjbUZ1YzJ4aGRHbHZiaUFvY0doeVlYTmxPaUFrZTNCb2NtRnpaWDBzSUhObFkzUnBiMjQ2SUNSN2MyVmpkR2x2Ym4wcExtQXBPMXh1WEhSY2RGeDBjbVYwZFhKdUlDZDdKeUFySUhObFkzUnBiMjRnS3lBbkxpY2dLeUJ3YUhKaGMyVWdLeUFuZlNjN1hHNWNkRngwZlZ4dVhHNWNkRngwY21WMGRYSnVJSE5sWTNScGIyNXpXM05sWTNScGIyNWRXM0JvY21GelpWMDdYRzVjZEgwN1hHNWNibjBvYW5ObExtTnZjbVV1YkdGdVp5a3BPMXh1SWl3aUx5b2dMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMWNjbHh1SUcxaGFXNHVhbk1nTWpBeE5pMHdOUzB4TjF4eVhHNGdSMkZ0WW1sdklFZHRZa2hjY2x4dUlHaDBkSEE2THk5M2QzY3VaMkZ0WW1sdkxtUmxYSEpjYmlCRGIzQjVjbWxuYUhRZ0tHTXBJREl3TVRZZ1IyRnRZbWx2SUVkdFlraGNjbHh1SUZKbGJHVmhjMlZrSUhWdVpHVnlJSFJvWlNCSFRsVWdSMlZ1WlhKaGJDQlFkV0pzYVdNZ1RHbGpaVzV6WlNBb1ZtVnljMmx2YmlBeUtWeHlYRzRnVzJoMGRIQTZMeTkzZDNjdVoyNTFMbTl5Wnk5c2FXTmxibk5sY3k5bmNHd3RNaTR3TG1oMGJXeGRYSEpjYmlBdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFZ4eVhHNGdLaTljY2x4dVhISmNiaTh2SUVsdGNHOXlkQ0JwYm1sMGFXRnNhWHBoZEdsdmJpQnpZM0pwY0hRdUlGeHlYRzVwYlhCdmNuUWdKeTR2YVc1cGRHbGhiR2w2WlNjN0lDQmNjbHh1WEhKY2JpOHZJRWx0Y0c5eWRDQjBhR1VnWTI5dWMzUnlkV04wYjNJZ1ptbHNaWE11SUZ4eVhHNXBiWEJ2Y25RZ0p5NHVMMk52Ym5OMGNuVmpkRzl5Y3k5amIyeHNaV04wYVc5dUp6dGNjbHh1YVcxd2IzSjBJQ2N1TGk5amIyNXpkSEoxWTNSdmNuTXZaR0YwWVY5aWFXNWthVzVuSnp0Y2NseHVhVzF3YjNKMElDY3VMaTlqYjI1emRISjFZM1J2Y25NdmJXOWtkV3hsSnp0Y2NseHVhVzF3YjNKMElDY3VMaTlqYjI1emRISjFZM1J2Y25NdmJtRnRaWE53WVdObEp6dGNjbHh1WEhKY2JpOHZJRWx0Y0c5eWRDQjBhR1VnWTI5eVpTQm1hV3hsY3k0Z1hISmNibWx0Y0c5eWRDQW5MaTloWW05MWRDYzdYSEpjYm1sdGNHOXlkQ0FuTGk5amIyNW1hV2NuTzF4eVhHNXBiWEJ2Y25RZ0p5NHZaR1ZpZFdjbk8xeHlYRzVwYlhCdmNuUWdKeTR2Wlc1bmFXNWxKenRjY2x4dWFXMXdiM0owSUNjdUwyVjRkR1Z1WkNjN1hISmNibWx0Y0c5eWRDQW5MaTlzWVc1bkp6dGNjbHh1YVcxd2IzSjBJQ2N1TDNKbGNYVnBjbVVuTzF4eVhHNXBiWEJ2Y25RZ0p5NHZiVzlrZFd4bFgyeHZZV1JsY2ljN1hISmNibWx0Y0c5eWRDQW5MaTl3YjJ4NVptbHNiSE1uTzF4eVhHNXBiWEJ2Y25RZ0p5NHZjbVZuYVhOMGNua25PeUlzSWk4cUlDMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdFhHNGdiVzlrZFd4bFgyeHZZV1JsY2k1cWN5QXlNREUyTFRBMkxUSXpYRzRnUjJGdFltbHZJRWR0WWtoY2JpQm9kSFJ3T2k4dmQzZDNMbWRoYldKcGJ5NWtaVnh1SUVOdmNIbHlhV2RvZENBb1l5a2dNakF4TmlCSFlXMWlhVzhnUjIxaVNGeHVJRkpsYkdWaGMyVmtJSFZ1WkdWeUlIUm9aU0JIVGxVZ1IyVnVaWEpoYkNCUWRXSnNhV01nVEdsalpXNXpaU0FvVm1WeWMybHZiaUF5S1Z4dUlGdG9kSFJ3T2k4dmQzZDNMbWR1ZFM1dmNtY3ZiR2xqWlc1elpYTXZaM0JzTFRJdU1DNW9kRzFzWFZ4dUlDMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdFhHNGdLaTljYmx4dWFuTmxMbU52Y21VdWJXOWtkV3hsWDJ4dllXUmxjaUE5SUdwelpTNWpiM0psTG0xdlpIVnNaVjlzYjJGa1pYSWdmSHdnZTMwN1hHNWNiaThxS2x4dUlDb2dTbE5GSUUxdlpIVnNaU0JNYjJGa1pYSmNiaUFxWEc0Z0tpQlVhR2x6SUc5aWFtVmpkQ0JwY3lCaGJpQmhaR0Z3ZEdWeUlHSmxkSGRsWlc0Z2RHaGxJR1Z1WjJsdVpTQmhibVFnVW1WeGRXbHlaVXBUSUhkb2FXTm9JR2x6SUhWelpXUWdkRzhnYkc5aFpDQjBhR1VnY21WeGRXbHlaV1FnWm1sc1pYTWdYRzRnS2lCcGJuUnZJSFJvWlNCamJHbGxiblF1WEc0Z0tpQmNiaUFxSUVCMGIyUnZJRkpsYlc5MlpTQnlaWEYxYVhKbExtcHpJR1JsY0dWdVpHVnVZM2tnWVc1a0lHeHZZV1FnZEdobElHMXZaSFZzWlM5c2FXSWdabWxzWlhNZ2JXRnVkV0ZzYkhrdVhHNGdLbHh1SUNvZ1FHMXZaSFZzWlNCS1UwVXZRMjl5WlM5dGIyUjFiR1ZmYkc5aFpHVnlYRzRnS2k5Y2JpaG1kVzVqZEdsdmJpQW9aWGh3YjNKMGN5a2dlMXh1WEc1Y2RDZDFjMlVnYzNSeWFXTjBKenRjYmx4MFhHNWNkQzh2SUMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFZ4dVhIUXZMeUJRVWtsV1FWUkZJRTFGVkVoUFJGTmNibHgwTHk4Z0xTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdFhHNWNkRnh1WEhRdktpcGNibHgwSUNvZ1RHOWhaQ0JEVTFNZ1ptbHNaUzVjYmx4MElDb2dYRzVjZENBcUlFQndZWEpoYlNCN1UzUnlhVzVuZlNCMWNtd2dRV0p6YjJ4MWRHVWdWVkpNSUc5bUlIUm9aU0JEVTFNZ1ptbHNaU0IwYnlCaVpTQnNiMkZrWldRdUlGeHVYSFFnS2lCY2JseDBJQ29nUUhCeWFYWmhkR1ZjYmx4MElDb3ZYRzVjZEdaMWJtTjBhVzl1SUY5c2IyRmtRM056S0hWeWJDa2dlMXh1WEhSY2RHTnZibk4wSUd4cGJtc2dQU0JrYjJOMWJXVnVkQzVqY21WaGRHVkZiR1Z0Wlc1MEtDZHNhVzVySnlrN1hHNWNkRngwYkdsdWF5NTBlWEJsSUQwZ0ozUmxlSFF2WTNOekp6dGNibHgwWEhSc2FXNXJMbkpsYkNBOUlDZHpkSGxzWlhOb1pXVjBKenRjYmx4MFhIUnNhVzVyTG1oeVpXWWdQU0IxY213N1hHNWNkRngwWkc5amRXMWxiblF1WjJWMFJXeGxiV1Z1ZEhOQ2VWUmhaMDVoYldVb0oyaGxZV1FuS1Zzd1hTNWhjSEJsYm1SRGFHbHNaQ2hzYVc1cktUdGNibHgwZlZ4dVhHNWNkQzh2SUMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFZ4dVhIUXZMeUJRVlVKTVNVTWdUVVZVU0U5RVUxeHVYSFF2THlBdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMxY2JseHVYSFF2S2lwY2JseDBJQ29nU1c1cGRHbGhiR2w2WlNCMGFHVWdiVzlrZFd4bElHeHZZV1JsY2k1Y2JseDBJQ3BjYmx4MElDb2dSWGhsWTNWMFpTQjBhR2x6SUcxbGRHaHZaQ0JoWm5SbGNpQjBhR1VnWlc1bmFXNWxJR052Ym1acFp5QnBjeUJwYm1sMGFXRnNhWHBsWkM0Z1NYUWdkMmxzYkNCamIyNW1hV2QxY21VZ2NtVnhkV2x5WlM1cWMxeHVYSFFnS2lCemJ5QjBhR0YwSUdsMElIZHBiR3dnWW1VZ1lXSnNaU0IwYnlCbWFXNWtJSFJvWlNCd2NtOXFaV04wSUdacGJHVnpMbHh1WEhRZ0tpQmNibHgwSUNvZ1ZHaGxJR05oWTJobElHSjFjM1JwYm1jZ2JXVjBhRzlrSUhkcGJHd2dkSEo1SUhSdklHTnlaV0YwWlNCaElHNTFiV0psY2lCaVlYTmxaQ0J2YmlCMGFHVWdZM1Z5Y21WdWRDQnphRzl3SUhabGNuTnBiMjR1WEc1Y2RDQXFMMXh1WEhSbGVIQnZjblJ6TG1sdWFYUWdQU0JtZFc1amRHbHZiaUFvS1NCN1hHNWNkRngwYkdWMElHTmhZMmhsUW5WemRDQTlJQ2NuTzF4dVhIUmNkRnh1WEhSY2RHbG1JQ2hxYzJVdVkyOXlaUzVqYjI1bWFXY3VaMlYwS0NkbGJuWnBjbTl1YldWdWRDY3BJRDA5UFNBbmNISnZaSFZqZEdsdmJpY2dKaVlnYW5ObExtTnZjbVV1WTI5dVptbG5MbWRsZENnblkyRmphR1ZVYjJ0bGJpY3BLU0I3WEc1Y2RGeDBYSFJqWVdOb1pVSjFjM1FnUFNCZ1luVnpkRDBrZTJwelpTNWpiM0psTG1OdmJtWnBaeTVuWlhRb0oyTmhZMmhsVkc5clpXNG5LWDFnTzF4dVhIUmNkSDFjYmx4MFhIUmNibHgwWEhSamIyNXpkQ0JqYjI1bWFXY2dQU0I3WEc1Y2RGeDBYSFJpWVhObFZYSnNPaUJxYzJVdVkyOXlaUzVqYjI1bWFXY3VaMlYwS0NkaGNIQlZjbXduS1N4Y2JseDBYSFJjZEhWeWJFRnlaM002SUdOaFkyaGxRblZ6ZEN4Y2JseDBYSFJjZEc5dVJYSnliM0k2SUdaMWJtTjBhVzl1SUNobGNuSnZjaWtnZTF4dVhIUmNkRngwWEhScWMyVXVZMjl5WlM1a1pXSjFaeTVsY25KdmNpZ25VbVZ4ZFdseVpVcFRJRVZ5Y205eU9pY3NJR1Z5Y205eUtUdGNibHgwWEhSY2RIMWNibHgwWEhSOU8xeHVYRzVjZEZ4MGQybHVaRzkzTG5KbGNYVnBjbVV1WTI5dVptbG5LR052Ym1acFp5azdYRzVjZEgwN1hHNWNkRnh1WEhRdktpcGNibHgwSUNvZ1VtVnhkV2x5WlNCS1V5QmhibVFnUTFOVElHWnBiR1Z6SUM1Y2JseDBJQ29nWEc1Y2RDQXFJRTV2ZEdsalpUb2dWR2hsY21VbmN5QnVieUJqYjI1amNtVjBaU0IzWVhrZ2RHOGdaR1YwWlhKdGFXNWxJSGRvWlc0Z1ExTlRJR1JsY0dWdVpHVnVZMmxsY3lCaGNtVWdiRzloWkdWa0xseHVYSFFnS2lCY2JseDBJQ29nUUhCaGNtRnRJSHRUZEhKcGJtZGJYWDBnWkdWd1pXNWtaVzVqYVdWeklFUmxjR1Z1WkdWdVkza2dWVkpNY3k1Y2JseDBJQ29nUUhCaGNtRnRJSHRHZFc1amRHbHZibjBnWTJGc2JHSmhZMnNnUTJGc2JHSmhZMnNnYldWMGFHOWtJSFJ2SUdKbElHTmhiR3hsWkNCdmJtTmxJSFJvWlNCa1pYQmxibVJsYm1OcFpYTWdZWEpsSUd4dllXUmxaQzRnWEc1Y2RDQXFMMXh1WEhSbGVIQnZjblJ6TG5KbGNYVnBjbVVnUFNCbWRXNWpkR2x2Ymloa1pYQmxibVJsYm1OcFpYTXNJR05oYkd4aVlXTnJLU0I3WEc1Y2RGeDBabTl5SUNoc1pYUWdaR1Z3Wlc1a1pXNWplU0J2WmlCa1pYQmxibVJsYm1OcFpYTXBJSHRjYmx4MFhIUmNkR2xtSUNoa1pYQmxibVJsYm1ONUxtbHVZMngxWkdWektDY3VZM056SnlrcElIdGNibHgwWEhSY2RGeDBYMnh2WVdSRGMzTW9aR1Z3Wlc1a1pXNWplU2s3SUZ4dVhIUmNkRngwWEhSamIyNXpkQ0JwYm1SbGVDQTlJR1JsY0dWdVpHVnVZMmxsY3k1cGJtUmxlRTltS0dSbGNHVnVaR1Z1WTNrcE95QmNibHgwWEhSY2RGeDBaR1Z3Wlc1a1pXNWphV1Z6TG5Od2JHbGpaU2hwYm1SbGVDd2dNU2s3SUZ4dVhIUmNkRngwZlZ4dVhIUmNkSDFjYmx4MFhIUmNibHgwWEhScFppQW9aR1Z3Wlc1a1pXNWphV1Z6TG14bGJtZDBhQ0E5UFQwZ01Da2dlMXh1WEhSY2RGeDBZMkZzYkdKaFkyc29LVHNnWEc1Y2RGeDBmU0JsYkhObElIdGNibHgwWEhSY2RIZHBibVJ2ZHk1eVpYRjFhWEpsS0dSbGNHVnVaR1Z1WTJsbGN5d2dZMkZzYkdKaFkyc3BPMXgwWEc1Y2RGeDBmU0JjYmx4MGZUc2dYRzVjYmx4MEx5b3FYRzVjZENBcUlFeHZZV1FnWVNCdGIyUjFiR1VnWm1sc1pTQjNhWFJvSUhSb1pTQjFjMlVnYjJZZ2NtVnhkV2x5WldwekxseHVYSFFnS2x4dVhIUWdLaUJBY0dGeVlXMGdlMDlpYW1WamRIMGdKR1ZzWlcxbGJuUWdVMlZzWldOMGIzSWdiMllnZEdobElHVnNaVzFsYm5RZ2QyaHBZMmdnYUdGeklIUm9aU0J0YjJSMWJHVWdaR1ZtYVc1cGRHbHZiaTVjYmx4MElDb2dRSEJoY21GdElIdFRkSEpwYm1kOUlHNWhiV1VnVFc5a2RXeGxJRzVoYldVZ2RHOGdZbVVnYkc5aFpHVmtMaUJOYjJSMWJHVnpJR2hoZG1VZ2RHaGxJSE5oYldVZ2JtRnRaWE1nWVhNZ2RHaGxhWElnWm1sc1pYTXVYRzVjZENBcUlFQndZWEpoYlNCN1QySnFaV04wZlNCamIyeHNaV04wYVc5dUlFTjFjbkpsYm5RZ1kyOXNiR1ZqZEdsdmJpQnBibk4wWVc1alpTNWNibHgwSUNwY2JseDBJQ29nUUhKbGRIVnliaUI3VDJKcVpXTjBmU0JTWlhSMWNtNXpJR0VnY0hKdmJXbHpaU0J2WW1wbFkzUWdkRzhnWW1VZ2NtVnpiMngyWldRZ2QybDBhQ0IwYUdVZ2JXOWtkV3hsSUdsdWMzUmhibU5sSUdGeklHRWdjR0Z5WVcxbGRHVnlMbHh1WEhRZ0tpOWNibHgwWlhod2IzSjBjeTVzYjJGa0lEMGdablZ1WTNScGIyNGdLQ1JsYkdWdFpXNTBMQ0J1WVcxbExDQmpiMnhzWldOMGFXOXVLU0I3WEc1Y2RGeDBZMjl1YzNRZ1pHVm1aWEp5WldRZ1BTQWtMa1JsWm1WeWNtVmtLQ2s3WEc1Y2JseDBYSFIwY25rZ2UxeHVYSFJjZEZ4MGFXWWdLRzVoYldVZ1BUMDlJQ2NuS1NCN1hHNWNkRngwWEhSY2RHUmxabVZ5Y21Wa0xuSmxhbVZqZENodVpYY2dSWEp5YjNJb0owMXZaSFZzWlNCdVlXMWxJR05oYm01dmRDQmlaU0JsYlhCMGVTNG5LU2s3WEc1Y2RGeDBYSFI5WEc1Y2JseDBYSFJjZEdOdmJuTjBJR0poYzJWTmIyUjFiR1ZPWVcxbElEMGdibUZ0WlM1eVpYQnNZV05sS0M4dUtseGNMeWd1S2lra0x5d2dKeVF4SnlrN0lDOHZJRTVoYldVZ2QybDBhRzkxZENCMGFHVWdjR0Z5Wlc1MElHUnBjbVZqZEc5eWFXVnpMbHh1WEc1Y2RGeDBYSFF2THlCVWNua2dkRzhnYkc5aFpDQjBhR1VnWTJGamFHVmtJR2x1YzNSaGJtTmxJRzltSUhSb1pTQnRiMlIxYkdVdVhHNWNkRngwWEhSamIyNXpkQ0JqWVdOb1pXUWdQU0JqYjJ4c1pXTjBhVzl1TG1OaFkyaGxMbTF2WkhWc1pYTmJZbUZ6WlUxdlpIVnNaVTVoYldWZE8xeHVYSFJjZEZ4MGFXWWdLR05oWTJobFpDQW1KaUJqWVdOb1pXUXVZMjlrWlNBOVBUMGdKMloxYm1OMGFXOXVKeWtnZTF4dVhIUmNkRngwWEhSa1pXWmxjbkpsWkM1eVpYTnZiSFpsS0c1bGR5QnFjMlV1WTI5dWMzUnlkV04wYjNKekxrMXZaSFZzWlNna1pXeGxiV1Z1ZEN3Z1ltRnpaVTF2WkhWc1pVNWhiV1VzSUdOdmJHeGxZM1JwYjI0cEtUdGNibHgwWEhSY2RGeDBjbVYwZFhKdUlIUnlkV1U3SUM4dklHTnZiblJwYm5WbElHeHZiM0JjYmx4MFhIUmNkSDFjYmx4dVhIUmNkRngwTHk4Z1ZISjVJSFJ2SUd4dllXUWdkR2hsSUcxdlpIVnNaU0JtYVd4bElHWnliMjBnZEdobElITmxjblpsY2k1Y2JseDBYSFJjZEdOdmJuTjBJR1pwYkdWRmVIUmxibk5wYjI0Z1BTQnFjMlV1WTI5eVpTNWpiMjVtYVdjdVoyVjBLQ2RrWldKMVp5Y3BJQ0U5UFNBblJFVkNWVWNuSUQ4Z0p5NXRhVzR1YW5NbklEb2dKeTVxY3ljN1hHNWNkRngwWEhSamIyNXpkQ0IxY213Z1BTQmpiMnhzWldOMGFXOXVMbTVoYldWemNHRmpaUzV6YjNWeVkyVWdLeUFuTHljZ0t5QmpiMnhzWldOMGFXOXVMbTVoYldVZ0t5QW5MeWNnS3lCdVlXMWxJQ3NnWm1sc1pVVjRkR1Z1YzJsdmJqdGNibHh1WEhSY2RGeDBkMmx1Wkc5M0xuSmxjWFZwY21Vb1czVnliRjBzSUNncElEMCtJSHRjYmx4MFhIUmNkRngwYVdZZ0tHTnZiR3hsWTNScGIyNHVZMkZqYUdVdWJXOWtkV3hsYzF0aVlYTmxUVzlrZFd4bFRtRnRaVjBnUFQwOUlIVnVaR1ZtYVc1bFpDa2dlMXh1WEhSY2RGeDBYSFJjZEhSb2NtOTNJRzVsZHlCRmNuSnZjaWduVFc5a2RXeGxJRndpSnlBcklHNWhiV1VnS3lBblhDSWdkMkZ6Ymx4Y0ozUWdaR1ZtYVc1bFpDQmpiM0p5WldOMGJIa3VJRU5vWldOcklIUm9aU0J0YjJSMWJHVWdZMjlrWlNCbWIzSWdKMXh1WEhSY2RGeDBYSFJjZENBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FySUNkbWRYSjBhR1Z5SUhSeWIzVmliR1Z6YUc5dmRHbHVaeTRuS1R0Y2JseDBYSFJjZEZ4MGZWeHVYRzVjZEZ4MFhIUmNkQzh2SUZWelpTQjBhR1VnYzJ4cFkyVWdiV1YwYUc5a0lHWnZjaUJqYjNCNWFXNW5JSFJvWlNCaGNuSmhlUzRnWEc1Y2RGeDBYSFJjZEdOdmJuTjBJR1JsY0dWdVpHVnVZMmxsY3lBOUlHTnZiR3hsWTNScGIyNHVZMkZqYUdVdWJXOWtkV3hsYzF0aVlYTmxUVzlrZFd4bFRtRnRaVjB1WkdWd1pXNWtaVzVqYVdWekxuTnNhV05sS0NrN0lGeHVYRzVjZEZ4MFhIUmNkR2xtSUNoa1pYQmxibVJsYm1OcFpYTXViR1Z1WjNSb0lEMDlQU0F3S1NCN0lDOHZJRzV2SUdSbGNHVnVaR1Z1WTJsbGMxeHVYSFJjZEZ4MFhIUmNkR1JsWm1WeWNtVmtMbkpsYzI5c2RtVW9ibVYzSUdwelpTNWpiMjV6ZEhKMVkzUnZjbk11VFc5a2RXeGxLQ1JsYkdWdFpXNTBMQ0JpWVhObFRXOWtkV3hsVG1GdFpTd2dZMjlzYkdWamRHbHZiaWtwTzF4dVhIUmNkRngwWEhSY2RISmxkSFZ5YmlCMGNuVmxPeUF2THlCamIyNTBhVzUxWlNCc2IyOXdYRzVjZEZ4MFhIUmNkSDFjYmx4dVhIUmNkRngwWEhRdkx5Qk1iMkZrSUhSb1pTQmtaWEJsYm1SbGJtTnBaWE1nWm1seWMzUXVYRzVjZEZ4MFhIUmNkR1p2Y2lBb2JHVjBJR2x1WkdWNElHbHVJR1JsY0dWdVpHVnVZMmxsY3lrZ2UxeHVYSFJjZEZ4MFhIUmNkR052Ym5OMElHUmxjR1Z1WkdWdVkza2dQU0JrWlhCbGJtUmxibU5wWlhOYmFXNWtaWGhkT3lCY2JseDBYSFJjZEZ4MFhIUmNibHgwWEhSY2RGeDBYSFJwWmlBb1pHVndaVzVrWlc1amVTNXBibVJsZUU5bUtDY3VZM056SnlrZ0lUMDlJQzB4S1NCN1hHNWNkRngwWEhSY2RGeDBYSFJmYkc5aFpFTnpjeWhrWlhCbGJtUmxibU41S1RzZ1hHNWNkRngwWEhSY2RGeDBYSFJrWlhCbGJtUmxibU5wWlhNdWMzQnNhV05sS0dsdVpHVjRMQ0FnTVNrN0lGeHVYSFJjZEZ4MFhIUmNkRngwWTI5dWRHbHVkV1U3SUZ4dVhIUmNkRngwWEhSY2RIMWNibHgwWEhSY2RGeDBYSFJjYmx4MFhIUmNkRngwWEhRdkx5QlVhR1Z1SUdOdmJuWmxjblFnZEdobElISmxiR0YwYVhabElIQmhkR2dnZEc4Z1NsTkZibWRwYm1VdmJHbGljeUJrYVhKbFkzUnZjbmt1WEc1Y2RGeDBYSFJjZEZ4MGFXWWdLR1JsY0dWdVpHVnVZM2t1YVc1a1pYaFBaaWduYUhSMGNDY3BJRDA5UFNBdE1Ta2dlMXh1WEhSY2RGeDBYSFJjZEZ4MFpHVndaVzVrWlc1amFXVnpXMmx1WkdWNFhTQTlJR3B6WlM1amIzSmxMbU52Ym1acFp5NW5aWFFvSjJWdVoybHVaVlZ5YkNjcElDc2dKeTlzYVdKekx5Y2dLeUJrWlhCbGJtUmxibU41SUNzZ1ptbHNaVVY0ZEdWdWMybHZianRjYmx4MFhIUmNkRngwWEhSOUlHVnNjMlVnYVdZZ0tHUmxjR1Z1WkdWdVkza3VjM1ZpYzNSeUtDMHpLU0FoUFQwZ0p5NXFjeWNwSUhzZ0x5OGdWR2hsYmlCaFpHUWdkR2hsSUdSNWJtRnRhV01nWm1sc1pTQmxlSFJsYm5OcGIyNGdkRzhnZEdobElGVlNUQzVjYmx4MFhIUmNkRngwWEhSY2RHUmxjR1Z1WkdWdVkybGxjMXRwYm1SbGVGMGdLejBnWm1sc1pVVjRkR1Z1YzJsdmJqdGNibHgwWEhSY2RGeDBYSFI5WEc1Y2RGeDBYSFJjZEgxY2JseHVYSFJjZEZ4MFhIUjNhVzVrYjNjdWNtVnhkV2x5WlNoa1pYQmxibVJsYm1OcFpYTXNJQ2dwSUQwK0lIdGNibHgwWEhSY2RGeDBYSFJrWldabGNuSmxaQzV5WlhOdmJIWmxLRzVsZHlCcWMyVXVZMjl1YzNSeWRXTjBiM0p6TGsxdlpIVnNaU2drWld4bGJXVnVkQ3dnWW1GelpVMXZaSFZzWlU1aGJXVXNJR052Ykd4bFkzUnBiMjRwS1R0Y2JseDBYSFJjZEZ4MGZTazdYRzVjZEZ4MFhIUjlLVHRjYmx4MFhIUjlJR05oZEdOb0lDaGxlR05sY0hScGIyNHBJSHRjYmx4MFhIUmNkR1JsWm1WeWNtVmtMbkpsYW1WamRDaGxlR05sY0hScGIyNHBPMXh1WEhSY2RIMWNibHh1WEhSY2RISmxkSFZ5YmlCa1pXWmxjbkpsWkM1d2NtOXRhWE5sS0NrN1hHNWNkSDA3WEc1Y2JuMHBLR3B6WlM1amIzSmxMbTF2WkhWc1pWOXNiMkZrWlhJcE8xeHVJaXdpTHlvZ0xTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMxY2JpQndiMng1Wm1sc2JITXVhbk1nTWpBeE5pMHdOUzB4TjF4dUlFZGhiV0pwYnlCSGJXSklYRzRnYUhSMGNEb3ZMM2QzZHk1bllXMWlhVzh1WkdWY2JpQkRiM0I1Y21sbmFIUWdLR01wSURJd01UWWdSMkZ0WW1sdklFZHRZa2hjYmlCU1pXeGxZWE5sWkNCMWJtUmxjaUIwYUdVZ1IwNVZJRWRsYm1WeVlXd2dVSFZpYkdsaklFeHBZMlZ1YzJVZ0tGWmxjbk5wYjI0Z01pbGNiaUJiYUhSMGNEb3ZMM2QzZHk1bmJuVXViM0puTDJ4cFkyVnVjMlZ6TDJkd2JDMHlMakF1YUhSdGJGMWNiaUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1SUNvdlhHNWNiaThxS2x4dUlDb2dTbE5GSUZCdmJIbG1hV3hzY3lCY2JpQXFJRnh1SUNvZ1VtVnhkV2x5WldRZ2NHOXNlV1pwYkd4eklHWnZjaUJqYjIxd1lYUnBZbWxzYVhSNUlHRnRiMjVuSUc5c1pDQmljbTkzYzJWeWN5NWNiaUFxWEc0Z0tpQkFiVzlrZFd4bElFcFRSUzlEYjNKbEwzQnZiSGxtYVd4c2MxeHVJQ292WEc0b1puVnVZM1JwYjI0Z0tDa2dlMXh1WEc1Y2RDZDFjMlVnYzNSeWFXTjBKenRjYmx4dVhIUXZMeUJKYm5SbGNtNWxkQ0JGZUhCc2IzSmxjaUJrYjJWeklHNXZkQ0J6ZFhCd2IzSjBJSFJvWlNCdmNtbG5hVzRnY0hKdmNHVnlkSGtnYjJZZ2RHaGxJSGRwYm1SdmR5NXNiMk5oZEdsdmJpQnZZbXBsWTNRdVhHNWNkQzh2SUh0QWJHbHVheUJvZEhSd09pOHZkRzl6WW05MWNtNHVZMjl0TDJFdFptbDRMV1p2Y2kxM2FXNWtiM2N0Ykc5allYUnBiMjR0YjNKcFoybHVMV2x1TFdsdWRHVnlibVYwTFdWNGNHeHZjbVZ5ZlZ4dVhIUnBaaUFvSVhkcGJtUnZkeTVzYjJOaGRHbHZiaTV2Y21sbmFXNHBJSHRjYmx4MFhIUjNhVzVrYjNjdWJHOWpZWFJwYjI0dWIzSnBaMmx1SUQwZ2QybHVaRzkzTG14dlkyRjBhVzl1TG5CeWIzUnZZMjlzSUNzZ0p5OHZKeUFyWEc1Y2RGeDBJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lIZHBibVJ2ZHk1c2IyTmhkR2x2Ymk1b2IzTjBibUZ0WlNBcklDaDNhVzVrYjNjdWJHOWpZWFJwYjI0dWNHOXlkQ0EvSUNjNkp5QXJJSGRwYm1SdmR5NXNiMk5oZEdsdmJpNXdiM0owSURvZ0p5Y3BPMXh1WEhSOVhHNWNibHgwTHk4Z1JHRjBaUzV1YjNjZ2JXVjBhRzlrSUhCdmJIbG1hV3hzWEc1Y2RDOHZJSHRBYkdsdWF5Qm9kSFJ3Y3pvdkwyUmxkbVZzYjNCbGNpNXRiM3BwYkd4aExtOXlaeTlsYmkxVlV5OWtiMk56TDFkbFlpOUtZWFpoVTJOeWFYQjBMMUpsWm1WeVpXNWpaUzlIYkc5aVlXeGZUMkpxWldOMGN5OUVZWFJsTDI1dmQzMWNibHgwYVdZZ0tDRkVZWFJsTG01dmR5a2dlMXh1WEhSY2RFUmhkR1V1Ym05M0lEMGdablZ1WTNScGIyNGdibTkzS0NrZ2UxeHVYSFJjZEZ4MGNtVjBkWEp1SUc1bGR5QkVZWFJsS0NrdVoyVjBWR2x0WlNncE8xeHVYSFJjZEgwN1hHNWNkSDFjYmx4MFhHNTlLU2dwTzF4dVhHNWNiaUlzSWk4cUlDMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdFhHNGdjbVZuYVhOMGNua3Vhbk1nTWpBeE5pMHdPUzB3T0Z4dUlFZGhiV0pwYnlCSGJXSklYRzRnYUhSMGNEb3ZMM2QzZHk1bllXMWlhVzh1WkdWY2JpQkRiM0I1Y21sbmFIUWdLR01wSURJd01UWWdSMkZ0WW1sdklFZHRZa2hjYmlCU1pXeGxZWE5sWkNCMWJtUmxjaUIwYUdVZ1IwNVZJRWRsYm1WeVlXd2dVSFZpYkdsaklFeHBZMlZ1YzJVZ0tGWmxjbk5wYjI0Z01pbGNiaUJiYUhSMGNEb3ZMM2QzZHk1bmJuVXViM0puTDJ4cFkyVnVjMlZ6TDJkd2JDMHlMakF1YUhSdGJGMWNiaUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMVnh1SUNvdlhHNWNibXB6WlM1amIzSmxMbkpsWjJsemRISjVJRDBnYW5ObExtTnZjbVV1Y21WbmFYTjBjbmtnZkh3Z2UzMDdYRzVjYmk4cUtseHVJQ29nU2xNZ1JXNW5hVzVsSUZKbFoybHpkSEo1WEc0Z0tseHVJQ29nVkdocGN5QnZZbXBsWTNRZ1kyOXVkR0ZwYm5NZ2MzUnlhVzVuSUdSaGRHRWdkR2hoZENCdmRHaGxjaUJ6WldOMGFXOXVjeUJ2WmlCMGFHVWdaVzVuYVc1bElHNWxaV1FnYVc0Z2IzSmtaWElnZEc4Z2IzQmxjbUYwWlNCamIzSnlaV04wYkhrdVhHNGdLbHh1SUNvZ1FHMXZaSFZzWlNCS1UwVXZRMjl5WlM5eVpXZHBjM1J5ZVZ4dUlDb3ZYRzRvWm5WdVkzUnBiMjRnS0dWNGNHOXlkSE1wSUh0Y2JseHVYSFFuZFhObElITjBjbWxqZENjN1hHNWNkRnh1WEhRdktpcGNibHgwSUNvZ1EyOXVkR0ZwYm5NZ2RHaGxJSEpsWjJsemRISjVJSFpoYkhWbGN5NWNibHgwSUNvZ1hHNWNkQ0FxSUVCMGVYQmxJSHRQWW1wbFkzUmJYWDFjYmx4MElDb3ZYRzVjZEdOdmJuTjBJSEpsWjJsemRISjVJRDBnVzEwN1hHNWNibHgwTHlvcVhHNWNkQ0FxSUZObGRDQmhJSFpoYkhWbElHbHVJSFJvWlNCeVpXZHBjM1J5ZVM1Y2JseDBJQ3BjYmx4MElDb2dRSEJoY21GdElIdFRkSEpwYm1kOUlHNWhiV1VnUTI5dWRHRnBibk1nZEdobElHNWhiV1VnYjJZZ2RHaGxJR1Z1ZEhKNUlIUnZJR0psSUdGa1pHVmtMbHh1WEhRZ0tpQkFjR0Z5WVcwZ2V5cDlJSFpoYkhWbElGUm9aU0IyWVd4MVpTQjBieUJpWlNCM2NtbDBkR1Z1SUdsdUlIUm9aU0J5WldkcGMzUnllUzVjYmx4MElDb3ZYRzVjZEdWNGNHOXlkSE11YzJWMElEMGdablZ1WTNScGIyNGdLRzVoYldVc0lIWmhiSFZsS1NCN1hHNWNkRngwTHk4Z1NXWWdZU0J5WldkcGMzUnllU0JsYm5SeWVTQjNhWFJvSUhSb1pTQnpZVzFsSUc1aGJXVWdaWGhwYzNSeklHRnNjbVZoWkhrZ2RHaGxJR1p2Ykd4dmQybHVaeUJqYjI1emIyeGxJSGRoY201cGJtY2dkMmxzYkZ4dVhIUmNkQzh2SUdsdVptOXliU0JrWlhabGJHOXdaWEp6SUhSb1lYUWdkR2hsZVNCaGNtVWdiM1psY25keWFYUnBibWNnWVc0Z1pYaHBjM1JwYm1jZ2RtRnNkV1VzSUhOdmJXVjBhR2x1WnlCMWMyVm1kV3dnZDJobGJpQmtaV0oxWjJkcGJtY3VYRzVjZEZ4MGFXWWdLSEpsWjJsemRISjVXMjVoYldWZElDRTlQU0IxYm1SbFptbHVaV1FwSUh0Y2JseDBYSFJjZEdwelpTNWpiM0psTG1SbFluVm5MbmRoY200b0oxUm9aU0J5WldkcGMzUnllU0IyWVd4MVpTQjNhWFJvSUhSb1pTQnVZVzFsSUZ3aUp5QXJJRzVoYldVZ0t5QW5YQ0lnZDJsc2JDQmlaU0J2ZG1WeWQzSnBkSFJsYmk0bktUdGNibHgwWEhSOVhHNWNibHgwWEhSeVpXZHBjM1J5ZVZ0dVlXMWxYU0E5SUhaaGJIVmxPMXh1WEhSOU8xeHVYRzVjZEM4cUtseHVYSFFnS2lCSFpYUWdZU0IyWVd4MVpTQm1jbTl0SUhSb1pTQnlaV2RwYzNSeWVTNWNibHgwSUNwY2JseDBJQ29nUUhCaGNtRnRJSHRUZEhKcGJtZDlJRzVoYldVZ1ZHaGxJRzVoYldVZ2IyWWdkR2hsSUdWdWRISjVJSFpoYkhWbElIUnZJR0psSUhKbGRIVnlibVZrTGx4dVhIUWdLbHh1WEhRZ0tpQkFjbVYwZFhKdWN5QjdLbjBnVW1WMGRYSnVjeUIwYUdVZ2RtRnNkV1VnZEdoaGRDQnRZWFJqYUdWeklIUm9aU0J1WVcxbExseHVYSFFnS2k5Y2JseDBaWGh3YjNKMGN5NW5aWFFnUFNCbWRXNWpkR2x2YmlBb2JtRnRaU2tnZTF4dVhIUmNkSEpsZEhWeWJpQnlaV2RwYzNSeWVWdHVZVzFsWFR0Y2JseDBmVHRjYmx4dVhIUXZLaXBjYmx4MElDb2dRMmhsWTJzZ2RHaGxJR04xY25KbGJuUWdZMjl1ZEdWdWRDQnZaaUIwYUdVZ2NtVm5hWE4wY25rZ2IySnFaV04wTGx4dVhIUWdLbHh1WEhRZ0tpQlVhR2x6SUcxbGRHaHZaQ0JwY3lCdmJteDVJR0YyWVdsc1lXSnNaU0IzYUdWdUlIUm9aU0JsYm1kcGJtVWdaVzUyYVhKdmJtMWxiblFnYVhNZ2RIVnlibVZrSUdsdWRHOGdaR1YyWld4dmNHMWxiblF1WEc1Y2RDQXFMMXh1WEhSbGVIQnZjblJ6TG1SbFluVm5JRDBnWm5WdVkzUnBiMjRnS0NrZ2UxeHVYSFJjZEdsbUlDaHFjMlV1WTI5eVpTNWpiMjVtYVdjdVoyVjBLQ2RsYm5acGNtOXViV1Z1ZENjcElEMDlQU0FuWkdWMlpXeHZjRzFsYm5RbktTQjdYRzVjZEZ4MFhIUnFjMlV1WTI5eVpTNWtaV0oxWnk1c2IyY29KMUpsWjJsemRISjVJRTlpYW1WamREb25MQ0J5WldkcGMzUnllU2s3WEc1Y2RGeDBmU0JsYkhObElIdGNibHgwWEhSY2RIUm9jbTkzSUc1bGR5QkZjbkp2Y2lnblZHaHBjeUJtZFc1amRHbHZiaUJwY3lCdWIzUWdZV3hzYjNkbFpDQnBiaUJoSUhCeWIyUjFZM1JwYjI0Z1pXNTJhWEp2Ym0xbGJuUXVKeWs3WEc1Y2RGeDBmVnh1WEhSOU8xeHVYRzU5S1NocWMyVXVZMjl5WlM1eVpXZHBjM1J5ZVNrN1hHNGlMQ0l2S2lBdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFZ4eVhHNGdjbVZ4ZFdseVpTNXFjeUF5TURFM0xUQXpMVEk0WEhKY2JpQkhZVzFpYVc4Z1IyMWlTRnh5WEc0Z2FIUjBjRG92TDNkM2R5NW5ZVzFpYVc4dVpHVmNjbHh1SUVOdmNIbHlhV2RvZENBb1l5a2dNakF4TnlCSFlXMWlhVzhnUjIxaVNGeHlYRzRnVW1Wc1pXRnpaV1FnZFc1a1pYSWdkR2hsSUVkT1ZTQkhaVzVsY21Gc0lGQjFZbXhwWXlCTWFXTmxibk5sSUNoV1pYSnphVzl1SURJcFhISmNiaUJiYUhSMGNEb3ZMM2QzZHk1bmJuVXViM0puTDJ4cFkyVnVjMlZ6TDJkd2JDMHlMakF1YUhSdGJGMWNjbHh1SUMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0WEhKY2JpQXFMMXh5WEc1Y2NseHVMeW9xWEhKY2JpQXFJRUZ6ZVc1amFISnZibTkxY3lCTmIyUjFiR1VnVEc5aFpHbHVaMXh5WEc0Z0tseHlYRzRnS2lCVWFHbHpJRzF2WkhWc1pTQnBjeUJoSUdadmNtc2diMllnVW1WeGRXbHlaVXBUSUhkcGRHaHZkWFFnZEdobElFRk5SQ0JtZFc1amRHbHZibUZzYVhSNUxpQlVhR1VnWjJ4dlltRnNJRndpWkdWbWFXNWxYQ0lnYldWMGFHOWtJR2x6SUhKbGJXOTJaV1FnWVhOY2NseHVJQ29nYVhRbmN5QnViM1FnYm1WalpYTnpZWEo1SUdKNUlFcFRJRVZ1WjJsdVpTNGdYSEpjYmlBcUlGeHlYRzRnS2lCN1FHeHBibXNnYUhSMGNITTZMeTluYVhSb2RXSXVZMjl0TDNKbGNYVnBjbVZxY3k5eVpYRjFhWEpsYW5OOVhISmNiaUFxWEhKY2JpQXFJRTV2ZENCMWMybHVaeUJ6ZEhKcFkzUTZJSFZ1WlhabGJpQnpkSEpwWTNRZ2MzVndjRzl5ZENCcGJpQmljbTkzYzJWeWN5d2dJek01TWl3Z1lXNWtJR05oZFhObGN5QndjbTlpYkdWdGN5QjNhWFJvSUhKbGNYVnBjbVZxY3k1bGVHVmpLQ2t2ZEhKaGJuTndhV3hsY2x4eVhHNGdLaUJ3YkhWbmFXNXpJSFJvWVhRZ2JXRjVJRzV2ZENCaVpTQnpkSEpwWTNRdVhISmNiaUFxTDF4eVhHNG9ablZ1WTNScGIyNG9LU0I3WEhKY2JseDBYSEpjYmx4MEx5OGdMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRYSEpjYmx4MEx5OGdWa0ZTU1VGQ1RFVlRYSEpjYmx4MEx5OGdMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRJRnh5WEc1Y2RGeHlYRzVjZEhkcGJtUnZkeTV5WlhGMWFYSmxhbk1nUFNCMWJtUmxabWx1WldRN1hISmNibHgwZDJsdVpHOTNMbkpsY1hWcGNtVWdQU0IxYm1SbFptbHVaV1E3WEhKY2JseDBYSEpjYmx4MGJHVjBJSEpsY1RzZ1hISmNibHgwYkdWMElITTdJRnh5WEc1Y2RHeGxkQ0JvWldGa095QmNjbHh1WEhSc1pYUWdZbUZ6WlVWc1pXMWxiblE3SUZ4eVhHNWNkR3hsZENCa1lYUmhUV0ZwYmpzZ1hISmNibHgwYkdWMElITnlZenRjY2x4dVhIUnNaWFFnYVc1MFpYSmhZM1JwZG1WVFkzSnBjSFE3SUZ4eVhHNWNkR3hsZENCdFlXbHVVMk55YVhCME95QmNjbHh1WEhSc1pYUWdjM1ZpVUdGMGFEdGNjbHh1WEhSc1pYUWdkbVZ5YzJsdmJpQTlJQ2N5TGpFdU1qSW5PMXh5WEc1Y2RHeGxkQ0JxYzFOMVptWnBlRkpsWjBWNGNDQTlJQzljWEM1cWN5UXZPMXh5WEc1Y2RHeGxkQ0JqZFhKeVJHbHlVbVZuUlhod0lEMGdMMTVjWEM1Y1hDOHZPMXh5WEc1Y2RHeGxkQ0J2Y0NBOUlFOWlhbVZqZEM1d2NtOTBiM1I1Y0dVN1hISmNibHgwYkdWMElHOXpkSEpwYm1jZ1BTQnZjQzUwYjFOMGNtbHVaenRjY2x4dVhIUnNaWFFnYUdGelQzZHVJRDBnYjNBdWFHRnpUM2R1VUhKdmNHVnlkSGs3WEhKY2JseDBiR1YwSUdselFuSnZkM05sY2lBOUlDRWhLSFI1Y0dWdlppQjNhVzVrYjNjZ0lUMDlJQ2QxYm1SbFptbHVaV1FuSUNZbUlIUjVjR1Z2WmlCdVlYWnBaMkYwYjNJZ0lUMDlJQ2QxYm1SbFptbHVaV1FuSUNZbUlIZHBibVJ2ZHk1a2IyTjFiV1Z1ZENrN1hISmNibHgwYkdWMElHbHpWMlZpVjI5eWEyVnlJRDBnSVdselFuSnZkM05sY2lBbUppQjBlWEJsYjJZZ2FXMXdiM0owVTJOeWFYQjBjeUFoUFQwZ0ozVnVaR1ZtYVc1bFpDYzdYSEpjYmx4MEx5OGdVRk16SUdsdVpHbGpZWFJsY3lCc2IyRmtaV1FnWVc1a0lHTnZiWEJzWlhSbExDQmlkWFFnYm1WbFpDQjBieUIzWVdsMElHWnZjaUJqYjIxd2JHVjBJSE53WldOcFptbGpZV3hzZVM0Z1UyVnhkV1Z1WTJVZ2FYTWdKMnh2WVdScGJtY25MQ0FuYkc5aFpHVmtKeXdnWEhKY2JseDBMeThnWlhobFkzVjBhVzl1SUhSb1pXNGdKMk52YlhCc1pYUmxKeTRnVkdobElGVkJJR05vWldOcklHbHpJSFZ1Wm05eWRIVnVZWFJsTENCaWRYUWdibTkwSUhOMWNtVWdhRzkzSUhSdklHWmxZWFIxY21VZ2RHVnpkQ0IzTDI4Z1kyRjFjMmx1WnlCd1pYSm1JR2x6YzNWbGN5NWNjbHh1WEhSc1pYUWdjbVZoWkhsU1pXZEZlSEFnUFNCcGMwSnliM2R6WlhJZ0ppWWdibUYyYVdkaGRHOXlMbkJzWVhSbWIzSnRJRDA5UFNBblVFeEJXVk5VUVZSSlQwNGdNeWNnUHlBdlhtTnZiWEJzWlhSbEpDOGdPaUF2WGloamIyMXdiR1YwWlh4c2IyRmtaV1FwSkM4N1hISmNibHgwYkdWMElHUmxaa052Ym5SbGVIUk9ZVzFsSUQwZ0oxOG5PMXh5WEc1Y2RDOHZJRTlvSUhSb1pTQjBjbUZuWldSNUxDQmtaWFJsWTNScGJtY2diM0JsY21FdUlGTmxaU0IwYUdVZ2RYTmhaMlVnYjJZZ2FYTlBjR1Z5WVNCbWIzSWdjbVZoYzI5dUxseHlYRzVjZEd4bGRDQnBjMDl3WlhKaElEMGdkSGx3Wlc5bUlHOXdaWEpoSUNFOVBTQW5kVzVrWldacGJtVmtKeUFtSmlCdmNHVnlZUzUwYjFOMGNtbHVaeWdwSUQwOVBTQW5XMjlpYW1WamRDQlBjR1Z5WVYwbk8xeHlYRzVjZEd4bGRDQmpiMjUwWlhoMGN5QTlJSHQ5TzF4eVhHNWNkR3hsZENCalptY2dQU0I3ZlR0Y2NseHVYSFJzWlhRZ1oyeHZZbUZzUkdWbVVYVmxkV1VnUFNCYlhUdGNjbHh1WEhSc1pYUWdkWE5sU1c1MFpYSmhZM1JwZG1VZ1BTQm1ZV3h6WlR0Y2NseHVYSFJjY2x4dVhIUXZMeUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzFjY2x4dVhIUXZMeUJHVlU1RFZFbFBUbE5jY2x4dVhIUXZMeUF0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzB0TFMwdExTMHRMUzBnWEhKY2JseDBYSEpjYmx4MEx5b3FYSEpjYmx4MElDb2dRMmhsWTJzZ2QyaGxkR2hsY2lCMllXeDFaU0JwY3lCaElHWjFibU4wYVc5dUxpQmNjbHh1WEhRZ0tpQmNjbHh1WEhRZ0tpQkFjR0Z5WVcwZ2V5cDlJR2wwSUZaaGJIVmxJSFJ2SUdKbElHTm9aV05yWldRdVhISmNibHgwSUNvZ1hISmNibHgwSUNvZ1FISmxkSFZ5YmlCN1ltOXZiR1ZoYm4wZ1VtVjBkWEp1Y3lCMGFHVWdkbUZzYVdSaGRHbHZiaUJ5WlhOMWJIUXVYSEpjYmx4MElDb3ZYSEpjYmx4MFpuVnVZM1JwYjI0Z2FYTkdkVzVqZEdsdmJpaHBkQ2tnZTF4eVhHNWNkRngwY21WMGRYSnVJRzl6ZEhKcGJtY3VZMkZzYkNocGRDa2dQVDA5SUNkYmIySnFaV04wSUVaMWJtTjBhVzl1WFNjN1hISmNibHgwZlZ4eVhHNWNkRnh5WEc1Y2RDOHFLbHh5WEc1Y2RDQXFJRU5vWldOcklIZG9aWFJvWlhJZ2RtRnNkV1VnYVhNZ1lXNGdZWEp5WVhrdVhISmNibHgwSUNwY2NseHVYSFFnS2lCQWNHRnlZVzBnZXlwOUlHbDBJRlpoYkhWbElIUnZJR0psSUdOb1pXTnJaV1F1WEhKY2JseDBJQ3BjY2x4dVhIUWdLaUJBY21WMGRYSnVJSHRpYjI5c1pXRnVmU0JTWlhSMWNtNXpJSFJvWlNCMllXeHBaR0YwYVc5dUlISmxjM1ZzZEM1Y2NseHVYSFFnS2k5Y2NseHVYSFJtZFc1amRHbHZiaUJwYzBGeWNtRjVLR2wwS1NCN1hISmNibHgwWEhSeVpYUjFjbTRnYjNOMGNtbHVaeTVqWVd4c0tHbDBLU0E5UFQwZ0oxdHZZbXBsWTNRZ1FYSnlZWGxkSnp0Y2NseHVYSFI5WEhKY2JseDBYSEpjYmx4MEx5b3FYSEpjYmx4MElDb2dTR1ZzY0dWeUlHWjFibU4wYVc5dUlHWnZjaUJwZEdWeVlYUnBibWNnYjNabGNpQmhiaUJoY25KaGVTNGdYSEpjYmx4MElDb2dYSEpjYmx4MElDb2dTV1lnZEdobElHWjFibU1nY21WMGRYSnVjeUJoSUhSeWRXVWdkbUZzZFdVc0lHbDBJSGRwYkd3Z1luSmxZV3NnYjNWMElHOW1JSFJvWlNCc2IyOXdMbHh5WEc1Y2RDQXFMMXh5WEc1Y2RHWjFibU4wYVc5dUlHVmhZMmdvWVhKNUxDQm1kVzVqS1NCN1hISmNibHgwWEhScFppQW9ZWEo1S1NCN1hISmNibHgwWEhSY2RHWnZjaUFvYkdWMElHa2dQU0F3T3lCcElEd2dZWEo1TG14bGJtZDBhRHNnYVNBclBTQXhLU0I3WEhKY2JseDBYSFJjZEZ4MGFXWWdLR0Z5ZVZ0cFhTQW1KaUJtZFc1aktHRnllVnRwWFN3Z2FTd2dZWEo1S1NrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWW5KbFlXczdYSEpjYmx4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSOVhISmNibHgwWEhSOVhISmNibHgwZlZ4eVhHNWNkRnh5WEc1Y2RDOHFLbHh5WEc1Y2RDQXFJRWhsYkhCbGNpQm1kVzVqZEdsdmJpQm1iM0lnYVhSbGNtRjBhVzVuSUc5MlpYSWdZVzRnWVhKeVlYa2dZbUZqYTNkaGNtUnpMaUJjY2x4dVhIUWdLaUJjY2x4dVhIUWdLaUJKWmlCMGFHVWdablZ1WXlCeVpYUjFjbTV6SUdFZ2RISjFaU0IyWVd4MVpTd2dhWFFnZDJsc2JDQmljbVZoYXlCdmRYUWdiMllnZEdobElHeHZiM0F1WEhKY2JseDBJQ292WEhKY2JseDBablZ1WTNScGIyNGdaV0ZqYUZKbGRtVnljMlVvWVhKNUxDQm1kVzVqS1NCN1hISmNibHgwWEhScFppQW9ZWEo1S1NCN1hISmNibHgwWEhSY2RHeGxkQ0JwTzF4eVhHNWNkRngwWEhSbWIzSWdLR2tnUFNCaGNua3ViR1Z1WjNSb0lDMGdNVHNnYVNBK0lDMHhPeUJwSUMwOUlERXBJSHRjY2x4dVhIUmNkRngwWEhScFppQW9ZWEo1VzJsZElDWW1JR1oxYm1Nb1lYSjVXMmxkTENCcExDQmhjbmtwS1NCN1hISmNibHgwWEhSY2RGeDBYSFJpY21WaGF6dGNjbHh1WEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEgxY2NseHVYSFJjZEgxY2NseHVYSFI5WEhKY2JseDBYSEpjYmx4MEx5b3FYSEpjYmx4MElDb2dRMmhsWTJzZ2QyaGxkR2hsY2lCaGJpQnZZbXBsWTNRZ2FHRnpJR0VnYzNCbFkybG1hV01nY0hKdmNHVnlkSGt1SUZ4eVhHNWNkQ0FxSUZ4eVhHNWNkQ0FxSUVCd1lYSmhiU0I3VDJKcVpXTjBmU0J2WW1vZ1QySnFaV04wSUhSdklHSmxJR05vWldOclpXUXVYSEpjYmx4MElDb2dRSEJoY21GdElIdFRkSEpwYm1kOUlIQnliM0FnVUhKdmNHVnlkSGtnYm1GdFpTQjBieUJpWlNCamFHVmphMlZrTGlCY2NseHVYSFFnS2lCY2NseHVYSFFnS2lCQWNtVjBkWEp1SUh0Q2IyOXNaV0Z1ZlNCU1pYUjFjbTV6SUhSb1pTQjJZV3hwWkdGMGFXOXVJSEpsYzNWc2RDNWNjbHh1WEhRZ0tpOWNjbHh1WEhSbWRXNWpkR2x2YmlCb1lYTlFjbTl3S0c5aWFpd2djSEp2Y0NrZ2UxeHlYRzVjZEZ4MGNtVjBkWEp1SUdoaGMwOTNiaTVqWVd4c0tHOWlhaXdnY0hKdmNDazdYSEpjYmx4MGZWeHlYRzVjZEZ4eVhHNWNkQzhxS2x4eVhHNWNkQ0FxSUVOb1pXTnJJR2xtSUdGdUlHOWlhbVZqZENCb1lYTWdZU0J3Y205d1pYSjBlU0JoYm1RZ2FXWWdkR2hoZENCd2NtOXdaWEowZVNCamIyNTBZV2x1Y3lCaElIUnlkWFJvZVNCMllXeDFaUzRnWEhKY2JseDBJQ29nWEhKY2JseDBJQ29nUUhCaGNtRnRJSHRQWW1wbFkzUjlJRzlpYWlCUFltcGxZM1FnZEc4Z1ltVWdZMmhsWTJ0bFpDNWNjbHh1WEhRZ0tpQkFjR0Z5WVcwZ2UxTjBjbWx1WjMwZ2NISnZjQ0JRY205d1pYSjBlU0J1WVcxbElIUnZJR0psSUdOb1pXTnJaV1F1WEhKY2JseDBJQ3BjY2x4dVhIUWdLaUJBY21WMGRYSnVJSHRDYjI5c1pXRnVmU0JTWlhSMWNtNXpJSFJvWlNCMllXeHBaR0YwYVc5dUlISmxjM1ZzZEM1Y2NseHVYSFFnS2k5Y2NseHVYSFJtZFc1amRHbHZiaUJuWlhSUGQyNG9iMkpxTENCd2NtOXdLU0I3WEhKY2JseDBYSFJ5WlhSMWNtNGdhR0Z6VUhKdmNDaHZZbW9zSUhCeWIzQXBJQ1ltSUc5aWFsdHdjbTl3WFR0Y2NseHVYSFI5WEhKY2JseDBYSEpjYmx4MEx5b3FYSEpjYmx4MElDb2dRM2xqYkdWeklHOTJaWElnY0hKdmNHVnlkR2xsY3lCcGJpQmhiaUJ2WW1wbFkzUWdZVzVrSUdOaGJHeHpJR0VnWm5WdVkzUnBiMjRnWm05eUlHVmhZMmdnY0hKdmNHVnlkSGtnZG1Gc2RXVXVJRnh5WEc1Y2RDQXFJRnh5WEc1Y2RDQXFJRWxtSUhSb1pTQm1kVzVqZEdsdmJpQnlaWFIxY201eklHRWdkSEoxZEdoNUlIWmhiSFZsTENCMGFHVnVJSFJvWlNCcGRHVnlZWFJwYjI0Z2FYTWdjM1J2Y0hCbFpDNWNjbHh1WEhRZ0tpOWNjbHh1WEhSbWRXNWpkR2x2YmlCbFlXTm9VSEp2Y0Nodlltb3NJR1oxYm1NcElIdGNjbHh1WEhSY2RHeGxkQ0J3Y205d08xeHlYRzVjZEZ4MFptOXlJQ2h3Y205d0lHbHVJRzlpYWlrZ2UxeHlYRzVjZEZ4MFhIUnBaaUFvYUdGelVISnZjQ2h2WW1vc0lIQnliM0FwS1NCN1hISmNibHgwWEhSY2RGeDBhV1lnS0daMWJtTW9iMkpxVzNCeWIzQmRMQ0J3Y205d0tTa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFluSmxZV3M3WEhKY2JseDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUjlYSEpjYmx4MFhIUjlYSEpjYmx4MGZWeHlYRzVjZEZ4eVhHNWNkQzhxS2x4eVhHNWNkQ0FxSUZOcGJYQnNaU0JtZFc1amRHbHZiaUIwYnlCdGFYZ2dhVzRnY0hKdmNHVnlkR2xsY3lCbWNtOXRJSE52ZFhKalpTQnBiblJ2SUhSaGNtZGxkQ3dnWW5WMElHOXViSGtnYVdZZ2RHRnlaMlYwSUdSdlpYTWdibTkwSUdGc2NtVmhaSGtnYUdGMlpTQmhJRnh5WEc1Y2RDQXFJSEJ5YjNCbGNuUjVJRzltSUhSb1pTQnpZVzFsSUc1aGJXVXVYSEpjYmx4MElDb3ZYSEpjYmx4MFpuVnVZM1JwYjI0Z2JXbDRhVzRvZEdGeVoyVjBMQ0J6YjNWeVkyVXNJR1p2Y21ObExDQmtaV1Z3VTNSeWFXNW5UV2w0YVc0cElIdGNjbHh1WEhSY2RHbG1JQ2h6YjNWeVkyVXBJSHRjY2x4dVhIUmNkRngwWldGamFGQnliM0FvYzI5MWNtTmxMQ0JtZFc1amRHbHZiaWgyWVd4MVpTd2djSEp2Y0NrZ2UxeHlYRzVjZEZ4MFhIUmNkR2xtSUNobWIzSmpaU0I4ZkNBaGFHRnpVSEp2Y0NoMFlYSm5aWFFzSUhCeWIzQXBLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUnBaaUFvWkdWbGNGTjBjbWx1WjAxcGVHbHVJQ1ltSUhSNWNHVnZaaUIyWVd4MVpTQTlQVDBnSjI5aWFtVmpkQ2NnSmlZZ2RtRnNkV1VnSmlaY2NseHVYSFJjZEZ4MFhIUmNkRngwSVdselFYSnlZWGtvZG1Gc2RXVXBJQ1ltSUNGcGMwWjFibU4wYVc5dUtIWmhiSFZsS1NBbUpseHlYRzVjZEZ4MFhIUmNkRngwWEhRaEtIWmhiSFZsSUdsdWMzUmhibU5sYjJZZ1VtVm5SWGh3S1NrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkRngwYVdZZ0tDRjBZWEpuWlhSYmNISnZjRjBwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSMFlYSm5aWFJiY0hKdmNGMGdQU0I3ZlR0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RGeDBYSFJ0YVhocGJpaDBZWEpuWlhSYmNISnZjRjBzSUhaaGJIVmxMQ0JtYjNKalpTd2daR1ZsY0ZOMGNtbHVaMDFwZUdsdUtUdGNjbHh1WEhSY2RGeDBYSFJjZEgwZ1pXeHpaU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSFJoY21kbGRGdHdjbTl3WFNBOUlIWmhiSFZsTzF4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MGZTazdYSEpjYmx4MFhIUjlYSEpjYmx4MFhIUnlaWFIxY200Z2RHRnlaMlYwTzF4eVhHNWNkSDFjY2x4dVhIUmNjbHh1WEhRdkx5QlRhVzFwYkdGeUlIUnZJRVoxYm1OMGFXOXVMbkJ5YjNSdmRIbHdaUzVpYVc1a0xDQmlkWFFnZEdobElDZDBhR2x6SnlCdlltcGxZM1FnYVhNZ2MzQmxZMmxtYVdWa0lHWnBjbk4wTENCemFXNWpaU0JwZENCcGN5QmxZWE5wWlhJZ2RHOGdjbVZoWkM5bWFXZDFjbVVnWEhKY2JseDBMeThnYjNWMElIZG9ZWFFnSjNSb2FYTW5JSGRwYkd3Z1ltVXVYSEpjYmx4MFpuVnVZM1JwYjI0Z1ltbHVaQ2h2WW1vc0lHWnVLU0I3WEhKY2JseDBYSFJ5WlhSMWNtNGdablZ1WTNScGIyNG9LU0I3WEhKY2JseDBYSFJjZEhKbGRIVnliaUJtYmk1aGNIQnNlU2h2WW1vc0lHRnlaM1Z0Wlc1MGN5azdYSEpjYmx4MFhIUjlPMXh5WEc1Y2RIMWNjbHh1WEhSY2NseHVYSFJtZFc1amRHbHZiaUJ6WTNKcGNIUnpLQ2tnZTF4eVhHNWNkRngwY21WMGRYSnVJR1J2WTNWdFpXNTBMbWRsZEVWc1pXMWxiblJ6UW5sVVlXZE9ZVzFsS0NkelkzSnBjSFFuS1R0Y2NseHVYSFI5WEhKY2JseDBYSEpjYmx4MFpuVnVZM1JwYjI0Z1pHVm1ZWFZzZEU5dVJYSnliM0lvWlhKeUtTQjdYSEpjYmx4MFhIUjBhSEp2ZHlCbGNuSTdYSEpjYmx4MGZWeHlYRzVjZEZ4eVhHNWNkQzh2SUVGc2JHOTNJR2RsZEhScGJtY2dZU0JuYkc5aVlXd2dkR2hoZENCcGN5QmxlSEJ5WlhOelpXUWdhVzRnWkc5MElHNXZkR0YwYVc5dUxDQnNhV3RsSUNkaExtSXVZeWN1WEhKY2JseDBablZ1WTNScGIyNGdaMlYwUjJ4dlltRnNLSFpoYkhWbEtTQjdYSEpjYmx4MFhIUnBaaUFvSVhaaGJIVmxLU0I3WEhKY2JseDBYSFJjZEhKbGRIVnliaUIyWVd4MVpUdGNjbHh1WEhSY2RIMWNjbHh1WEhSY2RHeGxkQ0JuSUQwZ1oyeHZZbUZzTzF4eVhHNWNkRngwWldGamFDaDJZV3gxWlM1emNHeHBkQ2duTGljcExDQm1kVzVqZEdsdmJpaHdZWEowS1NCN1hISmNibHgwWEhSY2RHY2dQU0JuVzNCaGNuUmRPMXh5WEc1Y2RGeDBmU2s3WEhKY2JseDBYSFJ5WlhSMWNtNGdaenRjY2x4dVhIUjlYSEpjYmx4MFhISmNibHgwTHlvcVhISmNibHgwSUNvZ1EyOXVjM1J5ZFdOMGN5QmhiaUJsY25KdmNpQjNhWFJvSUdFZ2NHOXBiblJsY2lCMGJ5QmhiaUJWVWt3Z2QybDBhQ0J0YjNKbElHbHVabTl5YldGMGFXOXVMbHh5WEc1Y2RDQXFJRnh5WEc1Y2RDQXFJRUJ3WVhKaGJTQjdVM1J5YVc1bmZTQnBaQ0JVYUdVZ1pYSnliM0lnU1VRZ2RHaGhkQ0J0WVhCeklIUnZJR0Z1SUVsRUlHOXVJR0VnZDJWaUlIQmhaMlV1WEhKY2JseDBJQ29nUUhCaGNtRnRJSHRUZEhKcGJtZDlJRzF6WnlCSWRXMWhiaUJ5WldGa1lXSnNaU0JsY25KdmNpNWNjbHh1WEhRZ0tpQkFjR0Z5WVcwZ2UwVnljbTl5ZlNCYlpYSnlYU0JVYUdVZ2IzSnBaMmx1WVd3Z1pYSnliM0lzSUdsbUlIUm9aWEpsSUdseklHOXVaUzVjY2x4dVhIUWdLbHh5WEc1Y2RDQXFJRUJ5WlhSMWNtNGdlMFZ5Y205eWZWeHlYRzVjZENBcUwxeHlYRzVjZEdaMWJtTjBhVzl1SUcxaGEyVkZjbkp2Y2locFpDd2diWE5uTENCbGNuSXNJSEpsY1hWcGNtVk5iMlIxYkdWektTQjdYSEpjYmx4MFhIUmpiMjV6ZENCbGNuSnZjaUE5SUc1bGR5QkZjbkp2Y2lodGMyY2dLeUFuWEZ4dWFIUjBjRG92TDNKbGNYVnBjbVZxY3k1dmNtY3ZaRzlqY3k5bGNuSnZjbk11YUhSdGJDTW5JQ3NnYVdRcE8xeHlYRzVjZEZ4MFhISmNibHgwWEhSbGNuSnZjaTV5WlhGMWFYSmxWSGx3WlNBOUlHbGtPMXh5WEc1Y2RGeDBaWEp5YjNJdWNtVnhkV2x5WlUxdlpIVnNaWE1nUFNCeVpYRjFhWEpsVFc5a2RXeGxjenRjY2x4dVhIUmNkRnh5WEc1Y2RGeDBhV1lnS0dWeWNpa2dlMXh5WEc1Y2RGeDBYSFJsY25KdmNpNXZjbWxuYVc1aGJFVnljbTl5SUQwZ1pYSnlPMXh5WEc1Y2RGeDBmVnh5WEc1Y2RGeDBYSEpjYmx4MFhIUnlaWFIxY200Z1pYSnliM0k3WEhKY2JseDBmVnh5WEc1Y2RGeHlYRzVjZEdsbUlDaDBlWEJsYjJZZ2QybHVaRzkzTG5KbGNYVnBjbVZxY3lBaFBUMGdKM1Z1WkdWbWFXNWxaQ2NwSUh0Y2NseHVYSFJjZEdsbUlDaHBjMFoxYm1OMGFXOXVLSGRwYm1SdmR5NXlaWEYxYVhKbGFuTXBLU0I3WEhKY2JseDBYSFJjZEM4dklFUnZJRzV2ZENCdmRtVnlkM0pwZEdVZ1lXNGdaWGhwYzNScGJtY2djbVZ4ZFdseVpXcHpJR2x1YzNSaGJtTmxMbHh5WEc1Y2RGeDBYSFJ5WlhSMWNtNDdYSEpjYmx4MFhIUjlYSEpjYmx4MFhIUmpabWNnUFNCM2FXNWtiM2N1Y21WeGRXbHlaV3B6TzF4eVhHNWNkRngwZDJsdVpHOTNMbkpsY1hWcGNtVnFjeUE5SUhWdVpHVm1hVzVsWkR0Y2NseHVYSFI5WEhKY2JseDBYSEpjYmx4MEx5OGdRV3hzYjNjZ1ptOXlJR0VnY21WeGRXbHlaU0JqYjI1bWFXY2diMkpxWldOMFhISmNibHgwYVdZZ0tIUjVjR1Z2WmlCM2FXNWtiM2N1Y21WeGRXbHlaU0FoUFQwZ0ozVnVaR1ZtYVc1bFpDY2dKaVlnSVdselJuVnVZM1JwYjI0b2QybHVaRzkzTG5KbGNYVnBjbVVwS1NCN1hISmNibHgwWEhRdkx5QmhjM04xYldVZ2FYUWdhWE1nWVNCamIyNW1hV2NnYjJKcVpXTjBMbHh5WEc1Y2RGeDBZMlpuSUQwZ2QybHVaRzkzTG5KbGNYVnBjbVU3WEhKY2JseDBYSFIzYVc1a2IzY3VjbVZ4ZFdseVpTQTlJSFZ1WkdWbWFXNWxaRHRjY2x4dVhIUjlYSEpjYmx4MFhISmNibHgwWm5WdVkzUnBiMjRnYm1WM1EyOXVkR1Y0ZENoamIyNTBaWGgwVG1GdFpTa2dlMXh5WEc1Y2RGeDBiR1YwSUdsdVEyaGxZMnRNYjJGa1pXUXNJRTF2WkhWc1pTd2dZMjl1ZEdWNGRDd2dhR0Z1Wkd4bGNuTXNYSEpjYmx4MFhIUmNkR05vWldOclRHOWhaR1ZrVkdsdFpXOTFkRWxrTEZ4eVhHNWNkRngwWEhSamIyNW1hV2NnUFNCN1hISmNibHgwWEhSY2RGeDBMeThnUkdWbVlYVnNkSE11SUVSdklHNXZkQ0J6WlhRZ1lTQmtaV1poZFd4MElHWnZjaUJ0WVhCY2NseHVYSFJjZEZ4MFhIUXZMeUJqYjI1bWFXY2dkRzhnYzNCbFpXUWdkWEFnYm05eWJXRnNhWHBsS0Nrc0lIZG9hV05vWEhKY2JseDBYSFJjZEZ4MEx5OGdkMmxzYkNCeWRXNGdabUZ6ZEdWeUlHbG1JSFJvWlhKbElHbHpJRzV2SUdSbFptRjFiSFF1WEhKY2JseDBYSFJjZEZ4MGQyRnBkRk5sWTI5dVpITTZJRGNzWEhKY2JseDBYSFJjZEZ4MFltRnpaVlZ5YkRvZ0p5NHZKeXhjY2x4dVhIUmNkRngwWEhSd1lYUm9jem9nZTMwc1hISmNibHgwWEhSY2RGeDBZblZ1Wkd4bGN6b2dlMzBzWEhKY2JseDBYSFJjZEZ4MGNHdG5jem9nZTMwc1hISmNibHgwWEhSY2RGeDBjMmhwYlRvZ2UzMHNYSEpjYmx4MFhIUmNkRngwWTI5dVptbG5PaUI3ZlZ4eVhHNWNkRngwWEhSOUxGeHlYRzVjZEZ4MFhIUnlaV2RwYzNSeWVTQTlJSHQ5TEZ4eVhHNWNkRngwWEhRdkx5QnlaV2RwYzNSeWVTQnZaaUJxZFhOMElHVnVZV0pzWldRZ2JXOWtkV3hsY3l3Z2RHOGdjM0JsWldSY2NseHVYSFJjZEZ4MEx5OGdZM2xqYkdVZ1luSmxZV3RwYm1jZ1kyOWtaU0IzYUdWdUlHeHZkSE1nYjJZZ2JXOWtkV3hsYzF4eVhHNWNkRngwWEhRdkx5QmhjbVVnY21WbmFYTjBaWEpsWkN3Z1luVjBJRzV2ZENCaFkzUnBkbUYwWldRdVhISmNibHgwWEhSY2RHVnVZV0pzWldSU1pXZHBjM1J5ZVNBOUlIdDlMRnh5WEc1Y2RGeDBYSFIxYm1SbFprVjJaVzUwY3lBOUlIdDlMRnh5WEc1Y2RGeDBYSFJrWldaUmRXVjFaU0E5SUZ0ZExGeHlYRzVjZEZ4MFhIUmtaV1pwYm1Wa0lEMGdlMzBzWEhKY2JseDBYSFJjZEhWeWJFWmxkR05vWldRZ1BTQjdmU3hjY2x4dVhIUmNkRngwWW5WdVpHeGxjMDFoY0NBOUlIdDlMRnh5WEc1Y2RGeDBYSFJ5WlhGMWFYSmxRMjkxYm5SbGNpQTlJREVzWEhKY2JseDBYSFJjZEhWdWJtOXliV0ZzYVhwbFpFTnZkVzUwWlhJZ1BTQXhPMXh5WEc1Y2RGeDBYSEpjYmx4MFhIUXZLaXBjY2x4dVhIUmNkQ0FxSUZSeWFXMXpJSFJvWlNBdUlHRnVaQ0F1TGlCbWNtOXRJR0Z1SUdGeWNtRjVJRzltSUhCaGRHZ2djMlZuYldWdWRITXVYSEpjYmx4MFhIUWdLaUJjY2x4dVhIUmNkQ0FxSUVsMElIZHBiR3dnYTJWbGNDQmhJR3hsWVdScGJtY2djR0YwYUNCelpXZHRaVzUwSUdsbUlHRWdMaTRnZDJsc2JDQmlaV052YldVZ2RHaGxJR1pwY25OMElIQmhkR2dnYzJWbmJXVnVkQ3dnZEc4Z2FHVnNjQ0IzYVhSb0lHMXZaSFZzWlNCdVlXMWxJRnh5WEc1Y2RGeDBJQ29nYkc5dmEzVndjeXdnZDJocFkyZ2dZV04wSUd4cGEyVWdjR0YwYUhNc0lHSjFkQ0JqWVc0Z1ltVWdjbVZ0WVhCd1pXUXVJRUoxZENCMGFHVWdaVzVrSUhKbGMzVnNkQ3dnWVd4c0lIQmhkR2h6SUhSb1lYUWdkWE5sSUhSb2FYTWdablZ1WTNScGIyNGdYSEpjYmx4MFhIUWdLaUJ6YUc5MWJHUWdiRzl2YXlCdWIzSnRZV3hwZW1Wa0xseHlYRzVjZEZ4MElDb2dYSEpjYmx4MFhIUWdLaUJPVDFSRk9pQjBhR2x6SUcxbGRHaHZaQ0JOVDBSSlJrbEZVeUIwYUdVZ2FXNXdkWFFnWVhKeVlYa3VYSEpjYmx4MFhIUWdLaUJjY2x4dVhIUmNkQ0FxSUVCd1lYSmhiU0I3UVhKeVlYbDlJR0Z5ZVNCMGFHVWdZWEp5WVhrZ2IyWWdjR0YwYUNCelpXZHRaVzUwY3k1Y2NseHVYSFJjZENBcUwxeHlYRzVjZEZ4MFpuVnVZM1JwYjI0Z2RISnBiVVJ2ZEhNb1lYSjVLU0I3WEhKY2JseDBYSFJjZEd4bGRDQnBMQ0J3WVhKME8xeHlYRzVjZEZ4MFhIUm1iM0lnS0drZ1BTQXdPeUJwSUR3Z1lYSjVMbXhsYm1kMGFEc2dhU3NyS1NCN1hISmNibHgwWEhSY2RGeDBjR0Z5ZENBOUlHRnllVnRwWFR0Y2NseHVYSFJjZEZ4MFhIUnBaaUFvY0dGeWRDQTlQVDBnSnk0bktTQjdYSEpjYmx4MFhIUmNkRngwWEhSaGNua3VjM0JzYVdObEtHa3NJREVwTzF4eVhHNWNkRngwWEhSY2RGeDBhU0F0UFNBeE8xeHlYRzVjZEZ4MFhIUmNkSDBnWld4elpTQnBaaUFvY0dGeWRDQTlQVDBnSnk0dUp5a2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MEx5OGdTV1lnWVhRZ2RHaGxJSE4wWVhKMExDQnZjaUJ3Y21WMmFXOTFjeUIyWVd4MVpTQnBjeUJ6ZEdsc2JDQXVMaXdnSUd0bFpYQWdkR2hsYlNCemJ5QjBhR0YwSUhkb1pXNGdZMjl1ZG1WeWRHVmtJSFJ2SUdFZ2NHRjBhQ0JwZENCY2NseHVYSFJjZEZ4MFhIUmNkQzh2SUcxaGVTQnpkR2xzYkNCM2IzSnJJSGRvWlc0Z1kyOXVkbVZ5ZEdWa0lIUnZJR0VnY0dGMGFDd2daWFpsYmlCMGFHOTFaMmdnWVhNZ1lXNGdTVVFnYVhRZ2FYTWdiR1Z6Y3lCMGFHRnVJR2xrWldGc0xpQkpiaUJzWVhKblpYSWdYSEpjYmx4MFhIUmNkRngwWEhRdkx5QndiMmx1ZENCeVpXeGxZWE5sY3l3Z2JXRjVJR0psSUdKbGRIUmxjaUIwYnlCcWRYTjBJR3RwWTJzZ2IzVjBJR0Z1SUdWeWNtOXlMbHh5WEc1Y2RGeDBYSFJjZEZ4MGFXWWdLR2tnUFQwOUlEQWdmSHdnS0drZ1BUMDlJREVnSmlZZ1lYSjVXekpkSUQwOVBTQW5MaTRuS1NCOGZDQmhjbmxiYVNBdElERmRJRDA5UFNBbkxpNG5LU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR052Ym5ScGJuVmxPMXh5WEc1Y2RGeDBYSFJjZEZ4MGZTQmxiSE5sSUdsbUlDaHBJRDRnTUNrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSaGNua3VjM0JzYVdObEtHa2dMU0F4TENBeUtUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGFTQXRQU0F5TzF4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MGZWeHlYRzVjZEZ4MGZWeHlYRzVjZEZ4MFhISmNibHgwWEhRdktpcGNjbHh1WEhSY2RDQXFJRWRwZG1WdUlHRWdjbVZzWVhScGRtVWdiVzlrZFd4bElHNWhiV1VzSUd4cGEyVWdMaTl6YjIxbGRHaHBibWNzSUc1dmNtMWhiR2w2WlNCcGRDQjBieUJoSUhKbFlXd2dibUZ0WlNCMGFHRjBJR05oYmlCaVpTQnRZWEJ3WldRZ2RHOGdZU0J3WVhSb0xseHlYRzVjZEZ4MElDb2dYSEpjYmx4MFhIUWdLaUJBY0dGeVlXMGdlMU4wY21sdVozMGdibUZ0WlNCMGFHVWdjbVZzWVhScGRtVWdibUZ0WlZ4eVhHNWNkRngwSUNvZ1FIQmhjbUZ0SUh0VGRISnBibWQ5SUdKaGMyVk9ZVzFsSUdFZ2NtVmhiQ0J1WVcxbElIUm9ZWFFnZEdobElHNWhiV1VnWVhKbklHbHpJSEpsYkdGMGFYWmxJSFJ2TGx4eVhHNWNkRngwSUNvZ1FIQmhjbUZ0SUh0Q2IyOXNaV0Z1ZlNCaGNIQnNlVTFoY0NCaGNIQnNlU0IwYUdVZ2JXRndJR052Ym1acFp5QjBieUIwYUdVZ2RtRnNkV1V1SUZOb2IzVnNaQ0J2Ym14NUlHSmxJR1J2Ym1VZ2FXWWdkR2hwY3lCdWIzSnRZV3hwZW1GMGFXOXVJR2x6SUZ4eVhHNWNkRngwSUNvZ1ptOXlJR0VnWkdWd1pXNWtaVzVqZVNCSlJDNWNjbHh1WEhSY2RDQXFJRnh5WEc1Y2RGeDBJQ29nUUhKbGRIVnliaUI3VTNSeWFXNW5mU0J1YjNKdFlXeHBlbVZrSUc1aGJXVmNjbHh1WEhSY2RDQXFMMXh5WEc1Y2RGeDBablZ1WTNScGIyNGdibTl5YldGc2FYcGxLRzVoYldVc0lHSmhjMlZPWVcxbExDQmhjSEJzZVUxaGNDa2dlMXh5WEc1Y2RGeDBYSFJzWlhRZ2NHdG5UV0ZwYml3Z2JXRndWbUZzZFdVc0lHNWhiV1ZRWVhKMGN5d2dhU3dnYWl3Z2JtRnRaVk5sWjIxbGJuUXNJR3hoYzNSSmJtUmxlQ3hjY2x4dVhIUmNkRngwWEhSbWIzVnVaRTFoY0N3Z1ptOTFibVJKTENCbWIzVnVaRk4wWVhKTllYQXNJSE4wWVhKSkxDQnViM0p0WVd4cGVtVmtRbUZ6WlZCaGNuUnpMRnh5WEc1Y2RGeDBYSFJjZEdKaGMyVlFZWEowY3lBOUlDaGlZWE5sVG1GdFpTQW1KaUJpWVhObFRtRnRaUzV6Y0d4cGRDZ25MeWNwS1N4Y2NseHVYSFJjZEZ4MFhIUnRZWEFnUFNCamIyNW1hV2N1YldGd0xGeHlYRzVjZEZ4MFhIUmNkSE4wWVhKTllYQWdQU0J0WVhBZ0ppWWdiV0Z3V3ljcUoxMDdYSEpjYmx4MFhIUmNkRnh5WEc1Y2RGeDBYSFF2THlCQlpHcDFjM1FnWVc1NUlISmxiR0YwYVhabElIQmhkR2h6TGx4eVhHNWNkRngwWEhScFppQW9ibUZ0WlNrZ2UxeHlYRzVjZEZ4MFhIUmNkRzVoYldVZ1BTQnVZVzFsTG5Od2JHbDBLQ2N2SnlrN1hISmNibHgwWEhSY2RGeDBiR0Z6ZEVsdVpHVjRJRDBnYm1GdFpTNXNaVzVuZEdnZ0xTQXhPMXh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RDOHZJRWxtSUhkaGJuUnBibWNnYm05a1pTQkpSQ0JqYjIxd1lYUnBZbWxzYVhSNUxDQnpkSEpwY0NBdWFuTWdabkp2YlNCbGJtUWdiMllnU1VSekxpQklZWFpsSUhSdklHUnZJSFJvYVhNZ2FHVnlaU3dnWVc1a0lHNXZkQ0JwYmlCY2NseHVYSFJjZEZ4MFhIUXZMeUJ1WVcxbFZHOVZjbXdnWW1WallYVnpaU0J1YjJSbElHRnNiRzkzY3lCbGFYUm9aWElnTG1weklHOXlJRzV2YmlBdWFuTWdkRzhnYldGd0lIUnZJSE5oYldVZ1ptbHNaUzVjY2x4dVhIUmNkRngwWEhScFppQW9ZMjl1Wm1sbkxtNXZaR1ZKWkVOdmJYQmhkQ0FtSmlCcWMxTjFabVpwZUZKbFowVjRjQzUwWlhOMEtHNWhiV1ZiYkdGemRFbHVaR1Y0WFNrcElIdGNjbHh1WEhSY2RGeDBYSFJjZEc1aGJXVmJiR0Z6ZEVsdVpHVjRYU0E5SUc1aGJXVmJiR0Z6ZEVsdVpHVjRYUzV5WlhCc1lXTmxLR3B6VTNWbVptbDRVbVZuUlhod0xDQW5KeWs3WEhKY2JseDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEM4dklGTjBZWEowY3lCM2FYUm9JR0VnSnk0bklITnZJRzVsWldRZ2RHaGxJR0poYzJWT1lXMWxYSEpjYmx4MFhIUmNkRngwYVdZZ0tHNWhiV1ZiTUYwdVkyaGhja0YwS0RBcElEMDlQU0FuTGljZ0ppWWdZbUZ6WlZCaGNuUnpLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUXZMeUJEYjI1MlpYSjBJR0poYzJWT1lXMWxJSFJ2SUdGeWNtRjVMQ0JoYm1RZ2JHOXdJRzltWmlCMGFHVWdiR0Z6ZENCd1lYSjBMQ0J6YnlCMGFHRjBJQzRnYldGMFkyaGxjeUIwYUdGMElDZGthWEpsWTNSdmNua25JR0Z1WkNCdWIzUWdYSEpjYmx4MFhIUmNkRngwWEhRdkx5QnVZVzFsSUc5bUlIUm9aU0JpWVhObFRtRnRaU2R6SUcxdlpIVnNaUzRnUm05eUlHbHVjM1JoYm1ObExDQmlZWE5sVG1GdFpTQnZaaUFuYjI1bEwzUjNieTkwYUhKbFpTY3NJRzFoY0hNZ2RHOGdYSEpjYmx4MFhIUmNkRngwWEhRdkx5QW5iMjVsTDNSM2J5OTBhSEpsWlM1cWN5Y3NJR0oxZENCM1pTQjNZVzUwSUhSb1pTQmthWEpsWTNSdmNua3NJQ2R2Ym1VdmRIZHZKeUJtYjNJZ2RHaHBjeUJ1YjNKdFlXeHBlbUYwYVc5dUxseHlYRzVjZEZ4MFhIUmNkRngwYm05eWJXRnNhWHBsWkVKaGMyVlFZWEowY3lBOUlHSmhjMlZRWVhKMGN5NXpiR2xqWlNnd0xDQmlZWE5sVUdGeWRITXViR1Z1WjNSb0lDMGdNU2s3WEhKY2JseDBYSFJjZEZ4MFhIUnVZVzFsSUQwZ2JtOXliV0ZzYVhwbFpFSmhjMlZRWVhKMGN5NWpiMjVqWVhRb2JtRnRaU2s3WEhKY2JseDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEhSeWFXMUViM1J6S0c1aGJXVXBPMXh5WEc1Y2RGeDBYSFJjZEc1aGJXVWdQU0J1WVcxbExtcHZhVzRvSnk4bktUdGNjbHh1WEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwTHk4Z1FYQndiSGtnYldGd0lHTnZibVpwWnlCcFppQmhkbUZwYkdGaWJHVXVYSEpjYmx4MFhIUmNkR2xtSUNoaGNIQnNlVTFoY0NBbUppQnRZWEFnSmlZZ0tHSmhjMlZRWVhKMGN5QjhmQ0J6ZEdGeVRXRndLU2tnZTF4eVhHNWNkRngwWEhSY2RHNWhiV1ZRWVhKMGN5QTlJRzVoYldVdWMzQnNhWFFvSnk4bktUdGNjbHh1WEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSdmRYUmxja3h2YjNBNklHWnZjaUFvYVNBOUlHNWhiV1ZRWVhKMGN5NXNaVzVuZEdnN0lHa2dQaUF3T3lCcElDMDlJREVwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRzVoYldWVFpXZHRaVzUwSUQwZ2JtRnRaVkJoY25SekxuTnNhV05sS0RBc0lHa3BMbXB2YVc0b0p5OG5LVHRjY2x4dVhIUmNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkRngwYVdZZ0tHSmhjMlZRWVhKMGN5a2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUXZMeUJHYVc1a0lIUm9aU0JzYjI1blpYTjBJR0poYzJWT1lXMWxJSE5sWjIxbGJuUWdiV0YwWTJnZ2FXNGdkR2hsSUdOdmJtWnBaeTRnVTI4c0lHUnZJR3B2YVc1eklHOXVJSFJvWlNCaWFXZG5aWE4wSUhSdklGeHlYRzVjZEZ4MFhIUmNkRngwWEhRdkx5QnpiV0ZzYkdWemRDQnNaVzVuZEdoeklHOW1JR0poYzJWUVlYSjBjeTVjY2x4dVhIUmNkRngwWEhSY2RGeDBabTl5SUNocUlEMGdZbUZ6WlZCaGNuUnpMbXhsYm1kMGFEc2dhaUErSURBN0lHb2dMVDBnTVNrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RHMWhjRlpoYkhWbElEMGdaMlYwVDNkdUtHMWhjQ3dnWW1GelpWQmhjblJ6TG5Oc2FXTmxLREFzSUdvcExtcHZhVzRvSnk4bktTazdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBMeThnWW1GelpVNWhiV1VnYzJWbmJXVnVkQ0JvWVhNZ1kyOXVabWxuTENCbWFXNWtJR2xtSUdsMElHaGhjeUJ2Ym1VZ1ptOXlJSFJvYVhNZ2JtRnRaUzVjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJwWmlBb2JXRndWbUZzZFdVcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkRzFoY0ZaaGJIVmxJRDBnWjJWMFQzZHVLRzFoY0ZaaGJIVmxMQ0J1WVcxbFUyVm5iV1Z1ZENrN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUnBaaUFvYldGd1ZtRnNkV1VwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RGeDBMeThnVFdGMFkyZ3NJSFZ3WkdGMFpTQnVZVzFsSUhSdklIUm9aU0J1WlhjZ2RtRnNkV1V1WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhSY2RHWnZkVzVrVFdGd0lEMGdiV0Z3Vm1Gc2RXVTdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSFJjZEdadmRXNWtTU0E5SUdrN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmNkR0p5WldGcklHOTFkR1Z5VEc5dmNEdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBMeThnUTJobFkyc2dabTl5SUdFZ2MzUmhjaUJ0WVhBZ2JXRjBZMmdzSUdKMWRDQnFkWE4wSUdodmJHUWdiMjRnZEc4Z2FYUXNJR2xtSUhSb1pYSmxJR2x6SUdFZ2MyaHZjblJsY2lCelpXZHRaVzUwSUcxaGRHTm9JR3hoZEdWeUlHbHVJRnh5WEc1Y2RGeDBYSFJjZEZ4MEx5OGdZU0J0WVhSamFHbHVaeUJqYjI1bWFXY3NJSFJvWlc0Z1ptRjJiM0lnYjNabGNpQjBhR2x6SUhOMFlYSWdiV0Z3TGx4eVhHNWNkRngwWEhSY2RGeDBhV1lnS0NGbWIzVnVaRk4wWVhKTllYQWdKaVlnYzNSaGNrMWhjQ0FtSmlCblpYUlBkMjRvYzNSaGNrMWhjQ3dnYm1GdFpWTmxaMjFsYm5RcEtTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RHWnZkVzVrVTNSaGNrMWhjQ0E5SUdkbGRFOTNiaWh6ZEdGeVRXRndMQ0J1WVcxbFUyVm5iV1Z1ZENrN1hISmNibHgwWEhSY2RGeDBYSFJjZEhOMFlYSkpJRDBnYVR0Y2NseHVYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwYVdZZ0tDRm1iM1Z1WkUxaGNDQW1KaUJtYjNWdVpGTjBZWEpOWVhBcElIdGNjbHh1WEhSY2RGeDBYSFJjZEdadmRXNWtUV0Z3SUQwZ1ptOTFibVJUZEdGeVRXRndPMXh5WEc1Y2RGeDBYSFJjZEZ4MFptOTFibVJKSUQwZ2MzUmhja2s3WEhKY2JseDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEdsbUlDaG1iM1Z1WkUxaGNDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGJtRnRaVkJoY25SekxuTndiR2xqWlNnd0xDQm1iM1Z1WkVrc0lHWnZkVzVrVFdGd0tUdGNjbHh1WEhSY2RGeDBYSFJjZEc1aGJXVWdQU0J1WVcxbFVHRnlkSE11YW05cGJpZ25MeWNwTzF4eVhHNWNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwTHk4Z1NXWWdkR2hsSUc1aGJXVWdjRzlwYm5SeklIUnZJR0VnY0dGamEyRm5aU2R6SUc1aGJXVXNJSFZ6WlNCMGFHVWdjR0ZqYTJGblpTQnRZV2x1SUdsdWMzUmxZV1F1WEhKY2JseDBYSFJjZEhCclowMWhhVzRnUFNCblpYUlBkMjRvWTI5dVptbG5MbkJyWjNNc0lHNWhiV1VwTzF4eVhHNWNkRngwWEhSY2NseHVYSFJjZEZ4MGNtVjBkWEp1SUhCclowMWhhVzRnUHlCd2EyZE5ZV2x1SURvZ2JtRnRaVHRjY2x4dVhIUmNkSDFjY2x4dVhIUmNkRnh5WEc1Y2RGeDBablZ1WTNScGIyNGdjbVZ0YjNabFUyTnlhWEIwS0c1aGJXVXBJSHRjY2x4dVhIUmNkRngwYVdZZ0tHbHpRbkp2ZDNObGNpa2dlMXh5WEc1Y2RGeDBYSFJjZEdWaFkyZ29jMk55YVhCMGN5Z3BMQ0JtZFc1amRHbHZiaWh6WTNKcGNIUk9iMlJsS1NCN1hISmNibHgwWEhSY2RGeDBYSFJwWmlBb2MyTnlhWEIwVG05a1pTNW5aWFJCZEhSeWFXSjFkR1VvSjJSaGRHRXRjbVZ4ZFdseVpXMXZaSFZzWlNjcElEMDlQU0J1WVcxbElDWW1YSEpjYmx4MFhIUmNkRngwWEhSY2RITmpjbWx3ZEU1dlpHVXVaMlYwUVhSMGNtbGlkWFJsS0Nka1lYUmhMWEpsY1hWcGNtVmpiMjUwWlhoMEp5a2dQVDA5SUdOdmJuUmxlSFF1WTI5dWRHVjRkRTVoYldVcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGMyTnlhWEIwVG05a1pTNXdZWEpsYm5ST2IyUmxMbkpsYlc5MlpVTm9hV3hrS0hOamNtbHdkRTV2WkdVcE8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSeVpYUjFjbTRnZEhKMVpUdGNjbHh1WEhSY2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhIUjlLVHRjY2x4dVhIUmNkRngwZlZ4eVhHNWNkRngwZlZ4eVhHNWNkRngwWEhKY2JseDBYSFJtZFc1amRHbHZiaUJvWVhOUVlYUm9SbUZzYkdKaFkyc29hV1FwSUh0Y2NseHVYSFJjZEZ4MGJHVjBJSEJoZEdoRGIyNW1hV2NnUFNCblpYUlBkMjRvWTI5dVptbG5MbkJoZEdoekxDQnBaQ2s3WEhKY2JseDBYSFJjZEdsbUlDaHdZWFJvUTI5dVptbG5JQ1ltSUdselFYSnlZWGtvY0dGMGFFTnZibVpwWnlrZ0ppWWdjR0YwYUVOdmJtWnBaeTVzWlc1bmRHZ2dQaUF4S1NCN1hISmNibHgwWEhSY2RGeDBMeThnVUc5d0lHOW1aaUIwYUdVZ1ptbHljM1FnWVhKeVlYa2dkbUZzZFdVc0lITnBibU5sSUdsMElHWmhhV3hsWkN3Z1lXNWtJSEpsZEhKNUxseHlYRzVjZEZ4MFhIUmNkSEJoZEdoRGIyNW1hV2N1YzJocFpuUW9LVHRjY2x4dVhIUmNkRngwWEhSamIyNTBaWGgwTG5KbGNYVnBjbVV1ZFc1a1pXWW9hV1FwTzF4eVhHNWNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkQzh2SUVOMWMzUnZiU0J5WlhGMWFYSmxJSFJvWVhRZ1pHOWxjeUJ1YjNRZ1pHOGdiV0Z3SUhSeVlXNXpiR0YwYVc5dUxDQnphVzVqWlNCSlJDQnBjeUJjSW1GaWMyOXNkWFJsWENJc0lHRnNjbVZoWkhrZ2JXRndjR1ZrTDNKbGMyOXNkbVZrTGx4eVhHNWNkRngwWEhSY2RHTnZiblJsZUhRdWJXRnJaVkpsY1hWcGNtVW9iblZzYkN3Z2UxeHlYRzVjZEZ4MFhIUmNkRngwYzJ0cGNFMWhjRG9nZEhKMVpWeHlYRzVjZEZ4MFhIUmNkSDBwS0Z0cFpGMHBPMXh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RISmxkSFZ5YmlCMGNuVmxPMXh5WEc1Y2RGeDBYSFI5WEhKY2JseDBYSFI5WEhKY2JseDBYSFJjY2x4dVhIUmNkQzh2SUZSMWNtNXpJR0VnY0d4MVoybHVJWEpsYzI5MWNtTmxJSFJ2SUZ0d2JIVm5hVzRzSUhKbGMyOTFjbU5sWFNCM2FYUm9JSFJvWlNCd2JIVm5hVzRnWW1WcGJtY2dkVzVrWldacGJtVmtJR2xtSUhSb1pTQnVZVzFsSUdScFpDQnViM1FnYUdGMlpTQmhJRnh5WEc1Y2RGeDBMeThnY0d4MVoybHVJSEJ5WldacGVDNWNjbHh1WEhSY2RHWjFibU4wYVc5dUlITndiR2wwVUhKbFptbDRLRzVoYldVcElIdGNjbHh1WEhSY2RGeDBiR1YwSUhCeVpXWnBlQ3hjY2x4dVhIUmNkRngwWEhScGJtUmxlQ0E5SUc1aGJXVWdQeUJ1WVcxbExtbHVaR1Y0VDJZb0p5RW5LU0E2SUMweE8xeHlYRzVjZEZ4MFhIUnBaaUFvYVc1a1pYZ2dQaUF0TVNrZ2UxeHlYRzVjZEZ4MFhIUmNkSEJ5WldacGVDQTlJRzVoYldVdWMzVmljM1J5YVc1bktEQXNJR2x1WkdWNEtUdGNjbHh1WEhSY2RGeDBYSFJ1WVcxbElEMGdibUZ0WlM1emRXSnpkSEpwYm1jb2FXNWtaWGdnS3lBeExDQnVZVzFsTG14bGJtZDBhQ2s3WEhKY2JseDBYSFJjZEgxY2NseHVYSFJjZEZ4MGNtVjBkWEp1SUZ0d2NtVm1hWGdzSUc1aGJXVmRPMXh5WEc1Y2RGeDBmVnh5WEc1Y2RGeDBYSEpjYmx4MFhIUXZLaXBjY2x4dVhIUmNkQ0FxSUVOeVpXRjBaWE1nWVNCdGIyUjFiR1VnYldGd2NHbHVaeUIwYUdGMElHbHVZMngxWkdWeklIQnNkV2RwYmlCd2NtVm1hWGdzSUcxdlpIVnNaU0J1WVcxbExDQmhibVFnY0dGMGFDNGdTV1lnY0dGeVpXNTBUVzlrZFd4bFRXRndJR2x6SUhCeWIzWnBaR1ZrSUZ4eVhHNWNkRngwSUNvZ2FYUWdkMmxzYkNCaGJITnZJRzV2Y20xaGJHbDZaU0IwYUdVZ2JtRnRaU0IyYVdFZ2NtVnhkV2x5WlM1dWIzSnRZV3hwZW1Vb0tWeHlYRzVjZEZ4MElDcGNjbHh1WEhSY2RDQXFJRUJ3WVhKaGJTQjdVM1J5YVc1bmZTQnVZVzFsSUZSb1pTQnRiMlIxYkdVZ2JtRnRaUzVjY2x4dVhIUmNkQ0FxSUVCd1lYSmhiU0I3VTNSeWFXNW5mU0JiY0dGeVpXNTBUVzlrZFd4bFRXRndYU0JRWVhKbGJuUWdiVzlrZFd4bElHMWhjQ0JtYjNJZ2RHaGxJRzF2WkhWc1pTQnVZVzFsTENCMWMyVmtJSFJ2SUhKbGMyOXNkbVVnY21Wc1lYUnBkbVVnYm1GdFpYTXVYSEpjYmx4MFhIUWdLaUJBY0dGeVlXMGdlMEp2YjJ4bFlXNTlJR2x6VG05eWJXRnNhWHBsWkNCSmN5QjBhR1VnU1VRZ1lXeHlaV0ZrZVNCdWIzSnRZV3hwZW1Wa1B5QlVhR2x6SUdseklIUnlkV1VnYVdZZ2RHaHBjeUJqWVd4c0lHbHpJR1J2Ym1VZ1ptOXlJR0VnWkdWbWFXNWxLQ2tnWEhKY2JseDBYSFFnS2lCdGIyUjFiR1VnU1VRdVhISmNibHgwWEhRZ0tpQkFjR0Z5WVcwZ2UwSnZiMnhsWVc1OUlHRndjR3g1VFdGd09pQmhjSEJzZVNCMGFHVWdiV0Z3SUdOdmJtWnBaeUIwYnlCMGFHVWdTVVF1SUZOb2IzVnNaQ0J2Ym14NUlHSmxJSFJ5ZFdVZ2FXWWdkR2hwY3lCdFlYQWdhWE1nWm05eUlHRWdaR1Z3Wlc1a1pXNWplUzVjY2x4dVhIUmNkQ0FxWEhKY2JseDBYSFFnS2lCQWNtVjBkWEp1SUh0UFltcGxZM1I5WEhKY2JseDBYSFFnS2k5Y2NseHVYSFJjZEdaMWJtTjBhVzl1SUcxaGEyVk5iMlIxYkdWTllYQW9ibUZ0WlN3Z2NHRnlaVzUwVFc5a2RXeGxUV0Z3TENCcGMwNXZjbTFoYkdsNlpXUXNJR0Z3Y0d4NVRXRndLU0I3WEhKY2JseDBYSFJjZEd4bGRDQjFjbXdzSUhCc2RXZHBiazF2WkhWc1pTd2djM1ZtWm1sNExDQnVZVzFsVUdGeWRITXNYSEpjYmx4MFhIUmNkRngwY0hKbFptbDRJRDBnYm5Wc2JDeGNjbHh1WEhSY2RGeDBYSFJ3WVhKbGJuUk9ZVzFsSUQwZ2NHRnlaVzUwVFc5a2RXeGxUV0Z3SUQ4Z2NHRnlaVzUwVFc5a2RXeGxUV0Z3TG01aGJXVWdPaUJ1ZFd4c0xGeHlYRzVjZEZ4MFhIUmNkRzl5YVdkcGJtRnNUbUZ0WlNBOUlHNWhiV1VzWEhKY2JseDBYSFJjZEZ4MGFYTkVaV1pwYm1VZ1BTQjBjblZsTEZ4eVhHNWNkRngwWEhSY2RHNXZjbTFoYkdsNlpXUk9ZVzFsSUQwZ0p5YzdYSEpjYmx4MFhIUmNkRnh5WEc1Y2RGeDBYSFF2THlCSlppQnVieUJ1WVcxbExDQjBhR1Z1SUdsMElHMWxZVzV6SUdsMElHbHpJR0VnY21WeGRXbHlaU0JqWVd4c0xDQm5aVzVsY21GMFpTQmhibHh5WEc1Y2RGeDBYSFF2THlCcGJuUmxjbTVoYkNCdVlXMWxMbHh5WEc1Y2RGeDBYSFJwWmlBb0lXNWhiV1VwSUh0Y2NseHVYSFJjZEZ4MFhIUnBjMFJsWm1sdVpTQTlJR1poYkhObE8xeHlYRzVjZEZ4MFhIUmNkRzVoYldVZ1BTQW5YMEJ5SnlBcklDaHlaWEYxYVhKbFEyOTFiblJsY2lBclBTQXhLVHRjY2x4dVhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2NseHVYSFJjZEZ4MGJtRnRaVkJoY25SeklEMGdjM0JzYVhSUWNtVm1hWGdvYm1GdFpTazdYSEpjYmx4MFhIUmNkSEJ5WldacGVDQTlJRzVoYldWUVlYSjBjMXN3WFR0Y2NseHVYSFJjZEZ4MGJtRnRaU0E5SUc1aGJXVlFZWEowYzFzeFhUdGNjbHh1WEhSY2RGeDBYSEpjYmx4MFhIUmNkR2xtSUNod2NtVm1hWGdwSUh0Y2NseHVYSFJjZEZ4MFhIUndjbVZtYVhnZ1BTQnViM0p0WVd4cGVtVW9jSEpsWm1sNExDQndZWEpsYm5ST1lXMWxMQ0JoY0hCc2VVMWhjQ2s3WEhKY2JseDBYSFJjZEZ4MGNHeDFaMmx1VFc5a2RXeGxJRDBnWjJWMFQzZHVLR1JsWm1sdVpXUXNJSEJ5WldacGVDazdYSEpjYmx4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhKY2JseDBYSFJjZEM4dklFRmpZMjkxYm5RZ1ptOXlJSEpsYkdGMGFYWmxJSEJoZEdoeklHbG1JSFJvWlhKbElHbHpJR0VnWW1GelpTQnVZVzFsTGx4eVhHNWNkRngwWEhScFppQW9ibUZ0WlNrZ2UxeHlYRzVjZEZ4MFhIUmNkR2xtSUNod2NtVm1hWGdwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkR2xtSUNod2JIVm5hVzVOYjJSMWJHVWdKaVlnY0d4MVoybHVUVzlrZFd4bExtNXZjbTFoYkdsNlpTa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUXZMeUJRYkhWbmFXNGdhWE1nYkc5aFpHVmtMQ0IxYzJVZ2FYUnpJRzV2Y20xaGJHbDZaU0J0WlhSb2IyUXVYSEpjYmx4MFhIUmNkRngwWEhSY2RHNXZjbTFoYkdsNlpXUk9ZVzFsSUQwZ2NHeDFaMmx1VFc5a2RXeGxMbTV2Y20xaGJHbDZaU2h1WVcxbExDQm1kVzVqZEdsdmJpaHVZVzFsS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGNtVjBkWEp1SUc1dmNtMWhiR2w2WlNodVlXMWxMQ0J3WVhKbGJuUk9ZVzFsTENCaGNIQnNlVTFoY0NrN1hISmNibHgwWEhSY2RGeDBYSFJjZEgwcE8xeHlYRzVjZEZ4MFhIUmNkRngwZlNCbGJITmxJSHRjY2x4dVhIUmNkRngwWEhSY2RGeDBMeThnU1dZZ2JtVnpkR1ZrSUhCc2RXZHBiaUJ5WldabGNtVnVZMlZ6TENCMGFHVnVJR1J2SUc1dmRDQjBjbmtnZEc5Y2NseHVYSFJjZEZ4MFhIUmNkRngwTHk4Z2JtOXliV0ZzYVhwbExDQmhjeUJwZENCM2FXeHNJRzV2ZENCdWIzSnRZV3hwZW1VZ1kyOXljbVZqZEd4NUxpQlVhR2x6WEhKY2JseDBYSFJjZEZ4MFhIUmNkQzh2SUhCc1lXTmxjeUJoSUhKbGMzUnlhV04wYVc5dUlHOXVJSEpsYzI5MWNtTmxTV1J6TENCaGJtUWdkR2hsSUd4dmJtZGxjbHh5WEc1Y2RGeDBYSFJjZEZ4MFhIUXZMeUIwWlhKdElITnZiSFYwYVc5dUlHbHpJRzV2ZENCMGJ5QnViM0p0WVd4cGVtVWdkVzUwYVd3Z2NHeDFaMmx1Y3lCaGNtVmNjbHh1WEhSY2RGeDBYSFJjZEZ4MEx5OGdiRzloWkdWa0lHRnVaQ0JoYkd3Z2JtOXliV0ZzYVhwaGRHbHZibk1nZEc4Z1lXeHNiM2NnWm05eUlHRnplVzVqWEhKY2JseDBYSFJjZEZ4MFhIUmNkQzh2SUd4dllXUnBibWNnYjJZZ1lTQnNiMkZrWlhJZ2NHeDFaMmx1TGlCQ2RYUWdabTl5SUc1dmR5d2dabWw0WlhNZ2RHaGxYSEpjYmx4MFhIUmNkRngwWEhSY2RDOHZJR052YlcxdmJpQjFjMlZ6TGlCRVpYUmhhV3h6SUdsdUlDTXhNVE14WEhKY2JseDBYSFJjZEZ4MFhIUmNkRzV2Y20xaGJHbDZaV1JPWVcxbElEMGdibUZ0WlM1cGJtUmxlRTltS0NjaEp5a2dQVDA5SUMweElEOWNjbHh1WEhSY2RGeDBYSFJjZEZ4MElDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNCdWIzSnRZV3hwZW1Vb2JtRnRaU3dnY0dGeVpXNTBUbUZ0WlN3Z1lYQndiSGxOWVhBcElEcGNjbHh1WEhSY2RGeDBYSFJjZEZ4MElDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNCdVlXMWxPMXh5WEc1Y2RGeDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkSDBnWld4elpTQjdYSEpjYmx4MFhIUmNkRngwWEhRdkx5QkJJSEpsWjNWc1lYSWdiVzlrZFd4bExseHlYRzVjZEZ4MFhIUmNkRngwYm05eWJXRnNhWHBsWkU1aGJXVWdQU0J1YjNKdFlXeHBlbVVvYm1GdFpTd2djR0Z5Wlc1MFRtRnRaU3dnWVhCd2JIbE5ZWEFwTzF4eVhHNWNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhRdkx5Qk9iM0p0WVd4cGVtVmtJRzVoYldVZ2JXRjVJR0psSUdFZ2NHeDFaMmx1SUVsRUlHUjFaU0IwYnlCdFlYQWdZMjl1Wm1sblhISmNibHgwWEhSY2RGeDBYSFF2THlCaGNIQnNhV05oZEdsdmJpQnBiaUJ1YjNKdFlXeHBlbVV1SUZSb1pTQnRZWEFnWTI5dVptbG5JSFpoYkhWbGN5QnRkWE4wWEhKY2JseDBYSFJjZEZ4MFhIUXZMeUJoYkhKbFlXUjVJR0psSUc1dmNtMWhiR2w2WldRc0lITnZJR1J2SUc1dmRDQnVaV1ZrSUhSdklISmxaRzhnZEdoaGRDQndZWEowTGx4eVhHNWNkRngwWEhSY2RGeDBibUZ0WlZCaGNuUnpJRDBnYzNCc2FYUlFjbVZtYVhnb2JtOXliV0ZzYVhwbFpFNWhiV1VwTzF4eVhHNWNkRngwWEhSY2RGeDBjSEpsWm1sNElEMGdibUZ0WlZCaGNuUnpXekJkTzF4eVhHNWNkRngwWEhSY2RGeDBibTl5YldGc2FYcGxaRTVoYldVZ1BTQnVZVzFsVUdGeWRITmJNVjA3WEhKY2JseDBYSFJjZEZ4MFhIUnBjMDV2Y20xaGJHbDZaV1FnUFNCMGNuVmxPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFIxY213Z1BTQmpiMjUwWlhoMExtNWhiV1ZVYjFWeWJDaHViM0p0WVd4cGVtVmtUbUZ0WlNrN1hISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4eVhHNWNkRngwWEhRdkx5QkpaaUIwYUdVZ2FXUWdhWE1nWVNCd2JIVm5hVzRnYVdRZ2RHaGhkQ0JqWVc1dWIzUWdZbVVnWkdWMFpYSnRhVzVsWkNCcFppQnBkQ0J1WldWa2MxeHlYRzVjZEZ4MFhIUXZMeUJ1YjNKdFlXeHBlbUYwYVc5dUxDQnpkR0Z0Y0NCcGRDQjNhWFJvSUdFZ2RXNXBjWFZsSUVsRUlITnZJSFIzYnlCdFlYUmphR2x1WnlCeVpXeGhkR2wyWlZ4eVhHNWNkRngwWEhRdkx5QnBaSE1nZEdoaGRDQnRZWGtnWTI5dVpteHBZM1FnWTJGdUlHSmxJSE5sY0dGeVlYUmxMbHh5WEc1Y2RGeDBYSFJ6ZFdabWFYZ2dQU0J3Y21WbWFYZ2dKaVlnSVhCc2RXZHBiazF2WkhWc1pTQW1KaUFoYVhOT2IzSnRZV3hwZW1Wa0lEOWNjbHh1WEhSY2RGeDBJQ0FnSUNBZ0lDQWdKMTkxYm01dmNtMWhiR2w2WldRbklDc2dLSFZ1Ym05eWJXRnNhWHBsWkVOdmRXNTBaWElnS3owZ01Ta2dPbHh5WEc1Y2RGeDBYSFFnSUNBZ0lDQWdJQ0FuSnp0Y2NseHVYSFJjZEZ4MFhISmNibHgwWEhSY2RISmxkSFZ5YmlCN1hISmNibHgwWEhSY2RGeDBjSEpsWm1sNE9pQndjbVZtYVhnc1hISmNibHgwWEhSY2RGeDBibUZ0WlRvZ2JtOXliV0ZzYVhwbFpFNWhiV1VzWEhKY2JseDBYSFJjZEZ4MGNHRnlaVzUwVFdGd09pQndZWEpsYm5STmIyUjFiR1ZOWVhBc1hISmNibHgwWEhSY2RGeDBkVzV1YjNKdFlXeHBlbVZrT2lBaElYTjFabVpwZUN4Y2NseHVYSFJjZEZ4MFhIUjFjbXc2SUhWeWJDeGNjbHh1WEhSY2RGeDBYSFJ2Y21sbmFXNWhiRTVoYldVNklHOXlhV2RwYm1Gc1RtRnRaU3hjY2x4dVhIUmNkRngwWEhScGMwUmxabWx1WlRvZ2FYTkVaV1pwYm1Vc1hISmNibHgwWEhSY2RGeDBhV1E2SUNod2NtVm1hWGdnUDF4eVhHNWNkRngwWEhSY2RDQWdJQ0FnY0hKbFptbDRJQ3NnSnlFbklDc2dibTl5YldGc2FYcGxaRTVoYldVZ09seHlYRzVjZEZ4MFhIUmNkQ0FnSUNBZ2JtOXliV0ZzYVhwbFpFNWhiV1VwSUNzZ2MzVm1abWw0WEhKY2JseDBYSFJjZEgwN1hISmNibHgwWEhSOVhISmNibHgwWEhSY2NseHVYSFJjZEdaMWJtTjBhVzl1SUdkbGRFMXZaSFZzWlNoa1pYQk5ZWEFwSUh0Y2NseHVYSFJjZEZ4MGJHVjBJR2xrSUQwZ1pHVndUV0Z3TG1sa0xGeHlYRzVjZEZ4MFhIUmNkRzF2WkNBOUlHZGxkRTkzYmloeVpXZHBjM1J5ZVN3Z2FXUXBPMXh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwYVdZZ0tDRnRiMlFwSUh0Y2NseHVYSFJjZEZ4MFhIUnRiMlFnUFNCeVpXZHBjM1J5ZVZ0cFpGMGdQU0J1WlhjZ1kyOXVkR1Y0ZEM1TmIyUjFiR1VvWkdWd1RXRndLVHRjY2x4dVhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2NseHVYSFJjZEZ4MGNtVjBkWEp1SUcxdlpEdGNjbHh1WEhSY2RIMWNjbHh1WEhSY2RGeHlYRzVjZEZ4MFpuVnVZM1JwYjI0Z2IyNG9aR1Z3VFdGd0xDQnVZVzFsTENCbWJpa2dlMXh5WEc1Y2RGeDBYSFJzWlhRZ2FXUWdQU0JrWlhCTllYQXVhV1FzWEhKY2JseDBYSFJjZEZ4MGJXOWtJRDBnWjJWMFQzZHVLSEpsWjJsemRISjVMQ0JwWkNrN1hISmNibHgwWEhSY2RGeHlYRzVjZEZ4MFhIUnBaaUFvYUdGelVISnZjQ2hrWldacGJtVmtMQ0JwWkNrZ0ppWmNjbHh1WEhSY2RGeDBYSFFvSVcxdlpDQjhmQ0J0YjJRdVpHVm1hVzVsUlcxcGRFTnZiWEJzWlhSbEtTa2dlMXh5WEc1Y2RGeDBYSFJjZEdsbUlDaHVZVzFsSUQwOVBTQW5aR1ZtYVc1bFpDY3BJSHRjY2x4dVhIUmNkRngwWEhSY2RHWnVLR1JsWm1sdVpXUmJhV1JkS1R0Y2NseHVYSFJjZEZ4MFhIUjlYSEpjYmx4MFhIUmNkSDBnWld4elpTQjdYSEpjYmx4MFhIUmNkRngwYlc5a0lEMGdaMlYwVFc5a2RXeGxLR1JsY0UxaGNDazdYSEpjYmx4MFhIUmNkRngwYVdZZ0tHMXZaQzVsY25KdmNpQW1KaUJ1WVcxbElEMDlQU0FuWlhKeWIzSW5LU0I3WEhKY2JseDBYSFJjZEZ4MFhIUm1iaWh0YjJRdVpYSnliM0lwTzF4eVhHNWNkRngwWEhSY2RIMGdaV3h6WlNCN1hISmNibHgwWEhSY2RGeDBYSFJ0YjJRdWIyNG9ibUZ0WlN3Z1ptNHBPMXh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MGZWeHlYRzVjZEZ4MGZWeHlYRzVjZEZ4MFhISmNibHgwWEhSbWRXNWpkR2x2YmlCdmJrVnljbTl5S0dWeWNpd2daWEp5WW1GamF5a2dlMXh5WEc1Y2RGeDBYSFJzWlhRZ2FXUnpJRDBnWlhKeUxuSmxjWFZwY21WTmIyUjFiR1Z6TEZ4eVhHNWNkRngwWEhSY2RHNXZkR2xtYVdWa0lEMGdabUZzYzJVN1hISmNibHgwWEhSY2RGeHlYRzVjZEZ4MFhIUnBaaUFvWlhKeVltRmpheWtnZTF4eVhHNWNkRngwWEhSY2RHVnljbUpoWTJzb1pYSnlLVHRjY2x4dVhIUmNkRngwZlNCbGJITmxJSHRjY2x4dVhIUmNkRngwWEhSbFlXTm9LR2xrY3l3Z1puVnVZM1JwYjI0b2FXUXBJSHRjY2x4dVhIUmNkRngwWEhSY2RHeGxkQ0J0YjJRZ1BTQm5aWFJQZDI0b2NtVm5hWE4wY25rc0lHbGtLVHRjY2x4dVhIUmNkRngwWEhSY2RHbG1JQ2h0YjJRcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MEx5OGdVMlYwSUdWeWNtOXlJRzl1SUcxdlpIVnNaU3dnYzI4Z2FYUWdjMnRwY0hNZ2RHbHRaVzkxZENCamFHVmphM011WEhKY2JseDBYSFJjZEZ4MFhIUmNkRzF2WkM1bGNuSnZjaUE5SUdWeWNqdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGFXWWdLRzF2WkM1bGRtVnVkSE11WlhKeWIzSXBJSHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJ1YjNScFptbGxaQ0E5SUhSeWRXVTdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBiVzlrTG1WdGFYUW9KMlZ5Y205eUp5d2daWEp5S1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEgwcE8xeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEdsbUlDZ2hibTkwYVdacFpXUXBJSHRjY2x4dVhIUmNkRngwWEhSY2RISmxjUzV2YmtWeWNtOXlLR1Z5Y2lrN1hISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFI5WEhKY2JseDBYSFI5WEhKY2JseDBYSFJjY2x4dVhIUmNkQzhxS2x4eVhHNWNkRngwSUNvZ1NXNTBaWEp1WVd3Z2JXVjBhRzlrSUhSdklIUnlZVzV6Wm1WeUlHZHNiMkpoYkZGMVpYVmxJR2wwWlcxeklIUnZJSFJvYVhNZ1kyOXVkR1Y0ZENkelhISmNibHgwWEhRZ0tpQmtaV1pSZFdWMVpTNWNjbHh1WEhSY2RDQXFMMXh5WEc1Y2RGeDBablZ1WTNScGIyNGdkR0ZyWlVkc2IySmhiRkYxWlhWbEtDa2dlMXh5WEc1Y2RGeDBYSFF2THlCUWRYTm9JR0ZzYkNCMGFHVWdaMnh2WW1Gc1JHVm1VWFZsZFdVZ2FYUmxiWE1nYVc1MGJ5QjBhR1VnWTI5dWRHVjRkQ2R6SUdSbFpsRjFaWFZsWEhKY2JseDBYSFJjZEdsbUlDaG5iRzlpWVd4RVpXWlJkV1YxWlM1c1pXNW5kR2dwSUh0Y2NseHVYSFJjZEZ4MFhIUmxZV05vS0dkc2IySmhiRVJsWmxGMVpYVmxMQ0JtZFc1amRHbHZiaWh4ZFdWMVpVbDBaVzBwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkR3hsZENCcFpDQTlJSEYxWlhWbFNYUmxiVnN3WFR0Y2NseHVYSFJjZEZ4MFhIUmNkR2xtSUNoMGVYQmxiMllnYVdRZ1BUMDlJQ2R6ZEhKcGJtY25LU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR052Ym5SbGVIUXVaR1ZtVVhWbGRXVk5ZWEJiYVdSZElEMGdkSEoxWlR0Y2NseHVYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RHUmxabEYxWlhWbExuQjFjMmdvY1hWbGRXVkpkR1Z0S1R0Y2NseHVYSFJjZEZ4MFhIUjlLVHRjY2x4dVhIUmNkRngwWEhSbmJHOWlZV3hFWldaUmRXVjFaU0E5SUZ0ZE8xeHlYRzVjZEZ4MFhIUjlYSEpjYmx4MFhIUjlYSEpjYmx4MFhIUmNjbHh1WEhSY2RHaGhibVJzWlhKeklEMGdlMXh5WEc1Y2RGeDBYSFFuY21WeGRXbHlaU2M2SUdaMWJtTjBhVzl1S0cxdlpDa2dlMXh5WEc1Y2RGeDBYSFJjZEdsbUlDaHRiMlF1Y21WeGRXbHlaU2tnZTF4eVhHNWNkRngwWEhSY2RGeDBjbVYwZFhKdUlHMXZaQzV5WlhGMWFYSmxPMXh5WEc1Y2RGeDBYSFJjZEgwZ1pXeHpaU0I3WEhKY2JseDBYSFJjZEZ4MFhIUnlaWFIxY200Z0tHMXZaQzV5WlhGMWFYSmxJRDBnWTI5dWRHVjRkQzV0WVd0bFVtVnhkV2x5WlNodGIyUXViV0Z3S1NrN1hISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFI5TEZ4eVhHNWNkRngwWEhRblpYaHdiM0owY3ljNklHWjFibU4wYVc5dUtHMXZaQ2tnZTF4eVhHNWNkRngwWEhSY2RHMXZaQzUxYzJsdVowVjRjRzl5ZEhNZ1BTQjBjblZsTzF4eVhHNWNkRngwWEhSY2RHbG1JQ2h0YjJRdWJXRndMbWx6UkdWbWFXNWxLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUnBaaUFvYlc5a0xtVjRjRzl5ZEhNcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGNtVjBkWEp1SUNoa1pXWnBibVZrVzIxdlpDNXRZWEF1YVdSZElEMGdiVzlrTG1WNGNHOXlkSE1wTzF4eVhHNWNkRngwWEhSY2RGeDBmU0JsYkhObElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGNtVjBkWEp1SUNodGIyUXVaWGh3YjNKMGN5QTlJR1JsWm1sdVpXUmJiVzlrTG0xaGNDNXBaRjBnUFNCN2ZTazdYSEpjYmx4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFI5TEZ4eVhHNWNkRngwWEhRbmJXOWtkV3hsSnpvZ1puVnVZM1JwYjI0b2JXOWtLU0I3WEhKY2JseDBYSFJjZEZ4MGFXWWdLRzF2WkM1dGIyUjFiR1VwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkSEpsZEhWeWJpQnRiMlF1Ylc5a2RXeGxPMXh5WEc1Y2RGeDBYSFJjZEgwZ1pXeHpaU0I3WEhKY2JseDBYSFJjZEZ4MFhIUnlaWFIxY200Z0tHMXZaQzV0YjJSMWJHVWdQU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR2xrT2lCdGIyUXViV0Z3TG1sa0xGeHlYRzVjZEZ4MFhIUmNkRngwWEhSMWNtazZJRzF2WkM1dFlYQXVkWEpzTEZ4eVhHNWNkRngwWEhSY2RGeDBYSFJqYjI1bWFXYzZJR1oxYm1OMGFXOXVLQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEhKbGRIVnliaUJuWlhSUGQyNG9ZMjl1Wm1sbkxtTnZibVpwWnl3Z2JXOWtMbTFoY0M1cFpDa2dmSHdnZTMwN1hISmNibHgwWEhSY2RGeDBYSFJjZEgwc1hISmNibHgwWEhSY2RGeDBYSFJjZEdWNGNHOXlkSE02SUcxdlpDNWxlSEJ2Y25SeklIeDhJQ2h0YjJRdVpYaHdiM0owY3lBOUlIdDlLVnh5WEc1Y2RGeDBYSFJjZEZ4MGZTazdYSEpjYmx4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSOVhISmNibHgwWEhSOU8xeHlYRzVjZEZ4MFhISmNibHgwWEhSbWRXNWpkR2x2YmlCamJHVmhibEpsWjJsemRISjVLR2xrS1NCN1hISmNibHgwWEhSY2RDOHZJRU5zWldGdUlIVndJRzFoWTJocGJtVnllU0IxYzJWa0lHWnZjaUIzWVdsMGFXNW5JRzF2WkhWc1pYTXVYSEpjYmx4MFhIUmNkR1JsYkdWMFpTQnlaV2RwYzNSeWVWdHBaRjA3WEhKY2JseDBYSFJjZEdSbGJHVjBaU0JsYm1GaWJHVmtVbVZuYVhOMGNubGJhV1JkTzF4eVhHNWNkRngwZlZ4eVhHNWNkRngwWEhKY2JseDBYSFJtZFc1amRHbHZiaUJpY21WaGEwTjVZMnhsS0cxdlpDd2dkSEpoWTJWa0xDQndjbTlqWlhOelpXUXBJSHRjY2x4dVhIUmNkRngwYkdWMElHbGtJRDBnYlc5a0xtMWhjQzVwWkR0Y2NseHVYSFJjZEZ4MFhISmNibHgwWEhSY2RHbG1JQ2h0YjJRdVpYSnliM0lwSUh0Y2NseHVYSFJjZEZ4MFhIUnRiMlF1WlcxcGRDZ25aWEp5YjNJbkxDQnRiMlF1WlhKeWIzSXBPMXh5WEc1Y2RGeDBYSFI5SUdWc2MyVWdlMXh5WEc1Y2RGeDBYSFJjZEhSeVlXTmxaRnRwWkYwZ1BTQjBjblZsTzF4eVhHNWNkRngwWEhSY2RHVmhZMmdvYlc5a0xtUmxjRTFoY0hNc0lHWjFibU4wYVc5dUtHUmxjRTFoY0N3Z2FTa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGJHVjBJR1JsY0Vsa0lEMGdaR1Z3VFdGd0xtbGtMRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmtaWEFnUFNCblpYUlBkMjRvY21WbmFYTjBjbmtzSUdSbGNFbGtLVHRjY2x4dVhIUmNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkRngwTHk4Z1QyNXNlU0JtYjNKalpTQjBhR2x1WjNNZ2RHaGhkQ0JvWVhabElHNXZkQ0JqYjIxd2JHVjBaV1JjY2x4dVhIUmNkRngwWEhSY2RDOHZJR0psYVc1bklHUmxabWx1WldRc0lITnZJSE4wYVd4c0lHbHVJSFJvWlNCeVpXZHBjM1J5ZVN4Y2NseHVYSFJjZEZ4MFhIUmNkQzh2SUdGdVpDQnZibXg1SUdsbUlHbDBJR2hoY3lCdWIzUWdZbVZsYmlCdFlYUmphR1ZrSUhWd1hISmNibHgwWEhSY2RGeDBYSFF2THlCcGJpQjBhR1VnYlc5a2RXeGxJR0ZzY21WaFpIa3VYSEpjYmx4MFhIUmNkRngwWEhScFppQW9aR1Z3SUNZbUlDRnRiMlF1WkdWd1RXRjBZMmhsWkZ0cFhTQW1KaUFoY0hKdlkyVnpjMlZrVzJSbGNFbGtYU2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJwWmlBb1oyVjBUM2R1S0hSeVlXTmxaQ3dnWkdWd1NXUXBLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwYlc5a0xtUmxabWx1WlVSbGNDaHBMQ0JrWldacGJtVmtXMlJsY0Vsa1hTazdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBiVzlrTG1Ob1pXTnJLQ2s3SUM4dklIQmhjM01nWm1Gc2MyVS9YSEpjYmx4MFhIUmNkRngwWEhSY2RIMGdaV3h6WlNCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFluSmxZV3REZVdOc1pTaGtaWEFzSUhSeVlXTmxaQ3dnY0hKdlkyVnpjMlZrS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEgwcE8xeHlYRzVjZEZ4MFhIUmNkSEJ5YjJObGMzTmxaRnRwWkYwZ1BTQjBjblZsTzF4eVhHNWNkRngwWEhSOVhISmNibHgwWEhSOVhISmNibHgwWEhSY2NseHVYSFJjZEdaMWJtTjBhVzl1SUdOb1pXTnJURzloWkdWa0tDa2dlMXh5WEc1Y2RGeDBYSFJzWlhRZ1pYSnlMQ0IxYzJsdVoxQmhkR2hHWVd4c1ltRmpheXhjY2x4dVhIUmNkRngwWEhSM1lXbDBTVzUwWlhKMllXd2dQU0JqYjI1bWFXY3VkMkZwZEZObFkyOXVaSE1nS2lBeE1EQXdMRnh5WEc1Y2RGeDBYSFJjZEM4dklFbDBJR2x6SUhCdmMzTnBZbXhsSUhSdklHUnBjMkZpYkdVZ2RHaGxJSGRoYVhRZ2FXNTBaWEoyWVd3Z1lua2dkWE5wYm1jZ2QyRnBkRk5sWTI5dVpITWdiMllnTUM1Y2NseHVYSFJjZEZ4MFhIUmxlSEJwY21Wa0lEMGdkMkZwZEVsdWRHVnlkbUZzSUNZbUlDaGpiMjUwWlhoMExuTjBZWEowVkdsdFpTQXJJSGRoYVhSSmJuUmxjblpoYkNrZ1BDQnVaWGNnUkdGMFpTZ3BMbWRsZEZScGJXVW9LU3hjY2x4dVhIUmNkRngwWEhSdWIweHZZV1J6SUQwZ1cxMHNYSEpjYmx4MFhIUmNkRngwY21WeFEyRnNiSE1nUFNCYlhTeGNjbHh1WEhSY2RGeDBYSFJ6ZEdsc2JFeHZZV1JwYm1jZ1BTQm1ZV3h6WlN4Y2NseHVYSFJjZEZ4MFhIUnVaV1ZrUTNsamJHVkRhR1ZqYXlBOUlIUnlkV1U3WEhKY2JseDBYSFJjZEZ4eVhHNWNkRngwWEhRdkx5QkVieUJ1YjNRZ1ltOTBhR1Z5SUdsbUlIUm9hWE1nWTJGc2JDQjNZWE1nWVNCeVpYTjFiSFFnYjJZZ1lTQmplV05zWlNCaWNtVmhheTVjY2x4dVhIUmNkRngwYVdZZ0tHbHVRMmhsWTJ0TWIyRmtaV1FwSUh0Y2NseHVYSFJjZEZ4MFhIUnlaWFIxY200N1hISmNibHgwWEhSY2RIMWNjbHh1WEhSY2RGeDBYSEpjYmx4MFhIUmNkR2x1UTJobFkydE1iMkZrWldRZ1BTQjBjblZsTzF4eVhHNWNkRngwWEhSY2NseHVYSFJjZEZ4MEx5OGdSbWxuZFhKbElHOTFkQ0IwYUdVZ2MzUmhkR1VnYjJZZ1lXeHNJSFJvWlNCdGIyUjFiR1Z6TGx4eVhHNWNkRngwWEhSbFlXTm9VSEp2Y0NobGJtRmliR1ZrVW1WbmFYTjBjbmtzSUdaMWJtTjBhVzl1S0cxdlpDa2dlMXh5WEc1Y2RGeDBYSFJjZEd4bGRDQnRZWEFnUFNCdGIyUXViV0Z3TEZ4eVhHNWNkRngwWEhSY2RGeDBiVzlrU1dRZ1BTQnRZWEF1YVdRN1hISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwTHk4Z1UydHBjQ0IwYUdsdVozTWdkR2hoZENCaGNtVWdibTkwSUdWdVlXSnNaV1FnYjNJZ2FXNGdaWEp5YjNJZ2MzUmhkR1V1WEhKY2JseDBYSFJjZEZ4MGFXWWdLQ0Z0YjJRdVpXNWhZbXhsWkNrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwY21WMGRYSnVPMXh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFJwWmlBb0lXMWhjQzVwYzBSbFptbHVaU2tnZTF4eVhHNWNkRngwWEhSY2RGeDBjbVZ4UTJGc2JITXVjSFZ6YUNodGIyUXBPMXh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFJwWmlBb0lXMXZaQzVsY25KdmNpa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MEx5OGdTV1lnZEdobElHMXZaSFZzWlNCemFHOTFiR1FnWW1VZ1pYaGxZM1YwWldRc0lHRnVaQ0JwZENCb1lYTWdibTkwWEhKY2JseDBYSFJjZEZ4MFhIUXZMeUJpWldWdUlHbHVhWFJsWkNCaGJtUWdkR2x0WlNCcGN5QjFjQ3dnY21WdFpXMWlaWElnYVhRdVhISmNibHgwWEhSY2RGeDBYSFJwWmlBb0lXMXZaQzVwYm1sMFpXUWdKaVlnWlhod2FYSmxaQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJwWmlBb2FHRnpVR0YwYUVaaGJHeGlZV05yS0cxdlpFbGtLU2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEhWemFXNW5VR0YwYUVaaGJHeGlZV05ySUQwZ2RISjFaVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJ6ZEdsc2JFeHZZV1JwYm1jZ1BTQjBjblZsTzF4eVhHNWNkRngwWEhSY2RGeDBYSFI5SUdWc2MyVWdlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRzV2VEc5aFpITXVjSFZ6YUNodGIyUkpaQ2s3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwY21WdGIzWmxVMk55YVhCMEtHMXZaRWxrS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RGeDBmU0JsYkhObElHbG1JQ2doYlc5a0xtbHVhWFJsWkNBbUppQnRiMlF1Wm1WMFkyaGxaQ0FtSmlCdFlYQXVhWE5FWldacGJtVXBJSHRjY2x4dVhIUmNkRngwWEhSY2RGeDBjM1JwYkd4TWIyRmthVzVuSUQwZ2RISjFaVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBhV1lnS0NGdFlYQXVjSEpsWm1sNEtTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBMeThnVG04Z2NtVmhjMjl1SUhSdklHdGxaWEFnYkc5dmEybHVaeUJtYjNJZ2RXNW1hVzVwYzJobFpGeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RDOHZJR3h2WVdScGJtY3VJRWxtSUhSb1pTQnZibXg1SUhOMGFXeHNURzloWkdsdVp5QnBjeUJoWEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwTHk4Z2NHeDFaMmx1SUhKbGMyOTFjbU5sSUhSb2IzVm5hQ3dnYTJWbGNDQm5iMmx1Wnl4Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhRdkx5QmlaV05oZFhObElHbDBJRzFoZVNCaVpTQjBhR0YwSUdFZ2NHeDFaMmx1SUhKbGMyOTFjbU5sWEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwTHk4Z2FYTWdkMkZwZEdsdVp5QnZiaUJoSUc1dmJpMXdiSFZuYVc0Z1kzbGpiR1V1WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwY21WMGRYSnVJQ2h1WldWa1EzbGpiR1ZEYUdWamF5QTlJR1poYkhObEtUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBmU2s3WEhKY2JseDBYSFJjZEZ4eVhHNWNkRngwWEhScFppQW9aWGh3YVhKbFpDQW1KaUJ1YjB4dllXUnpMbXhsYm1kMGFDa2dlMXh5WEc1Y2RGeDBYSFJjZEM4dklFbG1JSGRoYVhRZ2RHbHRaU0JsZUhCcGNtVmtMQ0IwYUhKdmR5Qmxjbkp2Y2lCdlppQjFibXh2WVdSbFpDQnRiMlIxYkdWekxseHlYRzVjZEZ4MFhIUmNkR1Z5Y2lBOUlHMWhhMlZGY25KdmNpZ25kR2x0Wlc5MWRDY3NJQ2RNYjJGa0lIUnBiV1Z2ZFhRZ1ptOXlJRzF2WkhWc1pYTTZJQ2NnS3lCdWIweHZZV1J6TENCdWRXeHNMQ0J1YjB4dllXUnpLVHRjY2x4dVhIUmNkRngwWEhSbGNuSXVZMjl1ZEdWNGRFNWhiV1VnUFNCamIyNTBaWGgwTG1OdmJuUmxlSFJPWVcxbE8xeHlYRzVjZEZ4MFhIUmNkSEpsZEhWeWJpQnZia1Z5Y205eUtHVnljaWs3WEhKY2JseDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhISmNibHgwWEhSY2RDOHZJRTV2ZENCbGVIQnBjbVZrTENCamFHVmpheUJtYjNJZ1lTQmplV05zWlM1Y2NseHVYSFJjZEZ4MGFXWWdLRzVsWldSRGVXTnNaVU5vWldOcktTQjdYSEpjYmx4MFhIUmNkRngwWldGamFDaHlaWEZEWVd4c2N5d2dablZ1WTNScGIyNG9iVzlrS1NCN1hISmNibHgwWEhSY2RGeDBYSFJpY21WaGEwTjVZMnhsS0cxdlpDd2dlMzBzSUh0OUtUdGNjbHh1WEhSY2RGeDBYSFI5S1R0Y2NseHVYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNjbHh1WEhSY2RGeDBMeThnU1dZZ2MzUnBiR3dnZDJGcGRHbHVaeUJ2YmlCc2IyRmtjeXdnWVc1a0lIUm9aU0IzWVdsMGFXNW5JR3h2WVdRZ2FYTWdjMjl0WlhSb2FXNW5YSEpjYmx4MFhIUmNkQzh2SUc5MGFHVnlJSFJvWVc0Z1lTQndiSFZuYVc0Z2NtVnpiM1Z5WTJVc0lHOXlJSFJvWlhKbElHRnlaU0J6ZEdsc2JDQnZkWFJ6ZEdGdVpHbHVaMXh5WEc1Y2RGeDBYSFF2THlCelkzSnBjSFJ6TENCMGFHVnVJR3AxYzNRZ2RISjVJR0poWTJzZ2JHRjBaWEl1WEhKY2JseDBYSFJjZEdsbUlDZ29JV1Y0Y0dseVpXUWdmSHdnZFhOcGJtZFFZWFJvUm1Gc2JHSmhZMnNwSUNZbUlITjBhV3hzVEc5aFpHbHVaeWtnZTF4eVhHNWNkRngwWEhSY2RDOHZJRk52YldWMGFHbHVaeUJwY3lCemRHbHNiQ0IzWVdsMGFXNW5JSFJ2SUd4dllXUXVJRmRoYVhRZ1ptOXlJR2wwTENCaWRYUWdiMjVzZVZ4eVhHNWNkRngwWEhSY2RDOHZJR2xtSUdFZ2RHbHRaVzkxZENCcGN5QnViM1FnWVd4eVpXRmtlU0JwYmlCbFptWmxZM1F1WEhKY2JseDBYSFJjZEZ4MGFXWWdLQ2hwYzBKeWIzZHpaWElnZkh3Z2FYTlhaV0pYYjNKclpYSXBJQ1ltSUNGamFHVmphMHh2WVdSbFpGUnBiV1Z2ZFhSSlpDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFkyaGxZMnRNYjJGa1pXUlVhVzFsYjNWMFNXUWdQU0J6WlhSVWFXMWxiM1YwS0daMWJtTjBhVzl1S0NrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSamFHVmphMHh2WVdSbFpGUnBiV1Z2ZFhSSlpDQTlJREE3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR05vWldOclRHOWhaR1ZrS0NrN1hISmNibHgwWEhSY2RGeDBYSFI5TENBMU1DazdYSEpjYmx4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSOVhISmNibHgwWEhSY2RGeHlYRzVjZEZ4MFhIUnBia05vWldOclRHOWhaR1ZrSUQwZ1ptRnNjMlU3WEhKY2JseDBYSFI5WEhKY2JseDBYSFJjY2x4dVhIUmNkRTF2WkhWc1pTQTlJR1oxYm1OMGFXOXVLRzFoY0NrZ2UxeHlYRzVjZEZ4MFhIUjBhR2x6TG1WMlpXNTBjeUE5SUdkbGRFOTNiaWgxYm1SbFprVjJaVzUwY3l3Z2JXRndMbWxrS1NCOGZDQjdmVHRjY2x4dVhIUmNkRngwZEdocGN5NXRZWEFnUFNCdFlYQTdYSEpjYmx4MFhIUmNkSFJvYVhNdWMyaHBiU0E5SUdkbGRFOTNiaWhqYjI1bWFXY3VjMmhwYlN3Z2JXRndMbWxrS1R0Y2NseHVYSFJjZEZ4MGRHaHBjeTVrWlhCRmVIQnZjblJ6SUQwZ1cxMDdYSEpjYmx4MFhIUmNkSFJvYVhNdVpHVndUV0Z3Y3lBOUlGdGRPMXh5WEc1Y2RGeDBYSFIwYUdsekxtUmxjRTFoZEdOb1pXUWdQU0JiWFR0Y2NseHVYSFJjZEZ4MGRHaHBjeTV3YkhWbmFXNU5ZWEJ6SUQwZ2UzMDdYSEpjYmx4MFhIUmNkSFJvYVhNdVpHVndRMjkxYm5RZ1BTQXdPMXh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwTHlvZ2RHaHBjeTVsZUhCdmNuUnpJSFJvYVhNdVptRmpkRzl5ZVZ4eVhHNWNkRngwWEhRZ2RHaHBjeTVrWlhCTllYQnpJRDBnVzEwc1hISmNibHgwWEhSY2RDQjBhR2x6TG1WdVlXSnNaV1FzSUhSb2FYTXVabVYwWTJobFpGeHlYRzVjZEZ4MFhIUWdLaTljY2x4dVhIUmNkSDA3WEhKY2JseDBYSFJjY2x4dVhIUmNkRTF2WkhWc1pTNXdjbTkwYjNSNWNHVWdQU0I3WEhKY2JseDBYSFJjZEdsdWFYUTZJR1oxYm1OMGFXOXVLR1JsY0UxaGNITXNJR1poWTNSdmNua3NJR1Z5Y21KaFkyc3NJRzl3ZEdsdmJuTXBJSHRjY2x4dVhIUmNkRngwWEhSdmNIUnBiMjV6SUQwZ2IzQjBhVzl1Y3lCOGZDQjdmVHRjY2x4dVhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUXZMeUJFYnlCdWIzUWdaRzhnYlc5eVpTQnBibWwwY3lCcFppQmhiSEpsWVdSNUlHUnZibVV1SUVOaGJpQm9ZWEJ3Wlc0Z2FXWWdkR2hsY21WY2NseHVYSFJjZEZ4MFhIUXZMeUJoY21VZ2JYVnNkR2x3YkdVZ1pHVm1hVzVsSUdOaGJHeHpJR1p2Y2lCMGFHVWdjMkZ0WlNCdGIyUjFiR1V1SUZSb1lYUWdhWE1nYm05MFhISmNibHgwWEhSY2RGeDBMeThnWVNCdWIzSnRZV3dzSUdOdmJXMXZiaUJqWVhObExDQmlkWFFnYVhRZ2FYTWdZV3h6YnlCdWIzUWdkVzVsZUhCbFkzUmxaQzVjY2x4dVhIUmNkRngwWEhScFppQW9kR2hwY3k1cGJtbDBaV1FwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkSEpsZEhWeWJqdGNjbHh1WEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBkR2hwY3k1bVlXTjBiM0o1SUQwZ1ptRmpkRzl5ZVR0Y2NseHVYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFJwWmlBb1pYSnlZbUZqYXlrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwTHk4Z1VtVm5hWE4wWlhJZ1ptOXlJR1Z5Y205eWN5QnZiaUIwYUdseklHMXZaSFZzWlM1Y2NseHVYSFJjZEZ4MFhIUmNkSFJvYVhNdWIyNG9KMlZ5Y205eUp5d2daWEp5WW1GamF5azdYSEpjYmx4MFhIUmNkRngwZlNCbGJITmxJR2xtSUNoMGFHbHpMbVYyWlc1MGN5NWxjbkp2Y2lrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwTHk4Z1NXWWdibThnWlhKeVltRmpheUJoYkhKbFlXUjVMQ0JpZFhRZ2RHaGxjbVVnWVhKbElHVnljbTl5SUd4cGMzUmxibVZ5YzF4eVhHNWNkRngwWEhSY2RGeDBMeThnYjI0Z2RHaHBjeUJ0YjJSMWJHVXNJSE5sZENCMWNDQmhiaUJsY25KaVlXTnJJSFJ2SUhCaGMzTWdkRzhnZEdobElHUmxjSE11WEhKY2JseDBYSFJjZEZ4MFhIUmxjbkppWVdOcklEMGdZbWx1WkNoMGFHbHpMQ0JtZFc1amRHbHZiaWhsY25JcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGRHaHBjeTVsYldsMEtDZGxjbkp2Y2ljc0lHVnljaWs3WEhKY2JseDBYSFJjZEZ4MFhIUjlLVHRjY2x4dVhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwTHk4Z1JHOGdZU0JqYjNCNUlHOW1JSFJvWlNCa1pYQmxibVJsYm1ONUlHRnljbUY1TENCemJ5QjBhR0YwWEhKY2JseDBYSFJjZEZ4MEx5OGdjMjkxY21ObElHbHVjSFYwY3lCaGNtVWdibTkwSUcxdlpHbG1hV1ZrTGlCR2IzSWdaWGhoYlhCc1pWeHlYRzVjZEZ4MFhIUmNkQzh2SUZ3aWMyaHBiVndpSUdSbGNITWdZWEpsSUhCaGMzTmxaQ0JwYmlCb1pYSmxJR1JwY21WamRHeDVMQ0JoYm1SY2NseHVYSFJjZEZ4MFhIUXZMeUJrYjJsdVp5QmhJR1JwY21WamRDQnRiMlJwWm1sallYUnBiMjRnYjJZZ2RHaGxJR1JsY0UxaGNITWdZWEp5WVhsY2NseHVYSFJjZEZ4MFhIUXZMeUIzYjNWc1pDQmhabVpsWTNRZ2RHaGhkQ0JqYjI1bWFXY3VYSEpjYmx4MFhIUmNkRngwZEdocGN5NWtaWEJOWVhCeklEMGdaR1Z3VFdGd2N5QW1KaUJrWlhCTllYQnpMbk5zYVdObEtEQXBPMXh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RIUm9hWE11WlhKeVltRmpheUE5SUdWeWNtSmhZMnM3WEhKY2JseDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBMeThnU1c1a2FXTmhkR1VnZEdocGN5QnRiMlIxYkdVZ2FHRnpJR0psSUdsdWFYUnBZV3hwZW1Wa1hISmNibHgwWEhSY2RGeDBkR2hwY3k1cGJtbDBaV1FnUFNCMGNuVmxPMXh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RIUm9hWE11YVdkdWIzSmxJRDBnYjNCMGFXOXVjeTVwWjI1dmNtVTdYSEpjYmx4MFhIUmNkRngwWEhKY2JseDBYSFJjZEZ4MEx5OGdRMjkxYkdRZ2FHRjJaU0J2Y0hScGIyNGdkRzhnYVc1cGRDQjBhR2x6SUcxdlpIVnNaU0JwYmlCbGJtRmliR1ZrSUcxdlpHVXNYSEpjYmx4MFhIUmNkRngwTHk4Z2IzSWdZMjkxYkdRZ2FHRjJaU0JpWldWdUlIQnlaWFpwYjNWemJIa2diV0Z5YTJWa0lHRnpJR1Z1WVdKc1pXUXVJRWh2ZDJWMlpYSXNYSEpjYmx4MFhIUmNkRngwTHk4Z2RHaGxJR1JsY0dWdVpHVnVZMmxsY3lCaGNtVWdibTkwSUd0dWIzZHVJSFZ1ZEdsc0lHbHVhWFFnYVhNZ1kyRnNiR1ZrTGlCVGIxeHlYRzVjZEZ4MFhIUmNkQzh2SUdsbUlHVnVZV0pzWldRZ2NISmxkbWx2ZFhOc2VTd2dibTkzSUhSeWFXZG5aWElnWkdWd1pXNWtaVzVqYVdWeklHRnpJR1Z1WVdKc1pXUXVYSEpjYmx4MFhIUmNkRngwYVdZZ0tHOXdkR2x2Ym5NdVpXNWhZbXhsWkNCOGZDQjBhR2x6TG1WdVlXSnNaV1FwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkQzh2SUVWdVlXSnNaU0IwYUdseklHMXZaSFZzWlNCaGJtUWdaR1Z3Wlc1a1pXNWphV1Z6TGx4eVhHNWNkRngwWEhSY2RGeDBMeThnVjJsc2JDQmpZV3hzSUhSb2FYTXVZMmhsWTJzb0tWeHlYRzVjZEZ4MFhIUmNkRngwZEdocGN5NWxibUZpYkdVb0tUdGNjbHh1WEhSY2RGeDBYSFI5SUdWc2MyVWdlMXh5WEc1Y2RGeDBYSFJjZEZ4MGRHaHBjeTVqYUdWamF5Z3BPMXh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MGZTeGNjbHh1WEhSY2RGeDBYSEpjYmx4MFhIUmNkR1JsWm1sdVpVUmxjRG9nWm5WdVkzUnBiMjRvYVN3Z1pHVndSWGh3YjNKMGN5a2dlMXh5WEc1Y2RGeDBYSFJjZEM4dklFSmxZMkYxYzJVZ2IyWWdZM2xqYkdWekxDQmtaV1pwYm1Wa0lHTmhiR3hpWVdOcklHWnZjaUJoSUdkcGRtVnVYSEpjYmx4MFhIUmNkRngwTHk4Z1pYaHdiM0owSUdOaGJpQmlaU0JqWVd4c1pXUWdiVzl5WlNCMGFHRnVJRzl1WTJVdVhISmNibHgwWEhSY2RGeDBhV1lnS0NGMGFHbHpMbVJsY0UxaGRHTm9aV1JiYVYwcElIdGNjbHh1WEhSY2RGeDBYSFJjZEhSb2FYTXVaR1Z3VFdGMFkyaGxaRnRwWFNBOUlIUnlkV1U3WEhKY2JseDBYSFJjZEZ4MFhIUjBhR2x6TG1SbGNFTnZkVzUwSUMwOUlERTdYSEpjYmx4MFhIUmNkRngwWEhSMGFHbHpMbVJsY0VWNGNHOXlkSE5iYVYwZ1BTQmtaWEJGZUhCdmNuUnpPMXh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MGZTeGNjbHh1WEhSY2RGeDBYSEpjYmx4MFhIUmNkR1psZEdOb09pQm1kVzVqZEdsdmJpZ3BJSHRjY2x4dVhIUmNkRngwWEhScFppQW9kR2hwY3k1bVpYUmphR1ZrS1NCN1hISmNibHgwWEhSY2RGeDBYSFJ5WlhSMWNtNDdYSEpjYmx4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RIUm9hWE11Wm1WMFkyaGxaQ0E5SUhSeWRXVTdYSEpjYmx4MFhIUmNkRngwWEhKY2JseDBYSFJjZEZ4MFkyOXVkR1Y0ZEM1emRHRnlkRlJwYldVZ1BTQW9ibVYzSUVSaGRHVW9LU2t1WjJWMFZHbHRaU2dwTzF4eVhHNWNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkR3hsZENCdFlYQWdQU0IwYUdsekxtMWhjRHRjY2x4dVhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUXZMeUJKWmlCMGFHVWdiV0Z1WVdkbGNpQnBjeUJtYjNJZ1lTQndiSFZuYVc0Z2JXRnVZV2RsWkNCeVpYTnZkWEpqWlN4Y2NseHVYSFJjZEZ4MFhIUXZMeUJoYzJzZ2RHaGxJSEJzZFdkcGJpQjBieUJzYjJGa0lHbDBJRzV2ZHk1Y2NseHVYSFJjZEZ4MFhIUnBaaUFvZEdocGN5NXphR2x0S1NCN1hISmNibHgwWEhSY2RGeDBYSFJqYjI1MFpYaDBMbTFoYTJWU1pYRjFhWEpsS0hSb2FYTXViV0Z3TENCN1hISmNibHgwWEhSY2RGeDBYSFJjZEdWdVlXSnNaVUoxYVd4a1EyRnNiR0poWTJzNklIUnlkV1ZjY2x4dVhIUmNkRngwWEhSY2RIMHBLSFJvYVhNdWMyaHBiUzVrWlhCeklIeDhJRnRkTENCaWFXNWtLSFJvYVhNc0lHWjFibU4wYVc5dUtDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUnlaWFIxY200Z2JXRndMbkJ5WldacGVDQS9JSFJvYVhNdVkyRnNiRkJzZFdkcGJpZ3BJRG9nZEdocGN5NXNiMkZrS0NrN1hISmNibHgwWEhSY2RGeDBYSFI5S1NrN1hISmNibHgwWEhSY2RGeDBmU0JsYkhObElIdGNjbHh1WEhSY2RGeDBYSFJjZEM4dklGSmxaM1ZzWVhJZ1pHVndaVzVrWlc1amVTNWNjbHh1WEhSY2RGeDBYSFJjZEhKbGRIVnliaUJ0WVhBdWNISmxabWw0SUQ4Z2RHaHBjeTVqWVd4c1VHeDFaMmx1S0NrZ09pQjBhR2x6TG14dllXUW9LVHRjY2x4dVhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RIMHNYSEpjYmx4MFhIUmNkRnh5WEc1Y2RGeDBYSFJzYjJGa09pQm1kVzVqZEdsdmJpZ3BJSHRjY2x4dVhIUmNkRngwWEhSc1pYUWdkWEpzSUQwZ2RHaHBjeTV0WVhBdWRYSnNPMXh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RDOHZJRkpsWjNWc1lYSWdaR1Z3Wlc1a1pXNWplUzVjY2x4dVhIUmNkRngwWEhScFppQW9JWFZ5YkVabGRHTm9aV1JiZFhKc1hTa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGRYSnNSbVYwWTJobFpGdDFjbXhkSUQwZ2RISjFaVHRjY2x4dVhIUmNkRngwWEhSY2RHTnZiblJsZUhRdWJHOWhaQ2gwYUdsekxtMWhjQzVwWkN3Z2RYSnNLVHRjY2x4dVhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RIMHNYSEpjYmx4MFhIUmNkRnh5WEc1Y2RGeDBYSFF2S2lwY2NseHVYSFJjZEZ4MElDb2dRMmhsWTJ0eklHbG1JSFJvWlNCdGIyUjFiR1VnYVhNZ2NtVmhaSGtnZEc4Z1pHVm1hVzVsSUdsMGMyVnNaaXdnWVc1a0lHbG1JSE52TEZ4eVhHNWNkRngwWEhRZ0tpQmtaV1pwYm1VZ2FYUXVYSEpjYmx4MFhIUmNkQ0FxTDF4eVhHNWNkRngwWEhSamFHVmphem9nWm5WdVkzUnBiMjRvS1NCN1hISmNibHgwWEhSY2RGeDBhV1lnS0NGMGFHbHpMbVZ1WVdKc1pXUWdmSHdnZEdocGN5NWxibUZpYkdsdVp5a2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGNtVjBkWEp1TzF4eVhHNWNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSc1pYUWdaWEp5TENCamFuTk5iMlIxYkdVc1hISmNibHgwWEhSY2RGeDBYSFJwWkNBOUlIUm9hWE11YldGd0xtbGtMRnh5WEc1Y2RGeDBYSFJjZEZ4MFpHVndSWGh3YjNKMGN5QTlJSFJvYVhNdVpHVndSWGh3YjNKMGN5eGNjbHh1WEhSY2RGeDBYSFJjZEdWNGNHOXlkSE1nUFNCMGFHbHpMbVY0Y0c5eWRITXNYSEpjYmx4MFhIUmNkRngwWEhSbVlXTjBiM0o1SUQwZ2RHaHBjeTVtWVdOMGIzSjVPMXh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RHbG1JQ2doZEdocGN5NXBibWwwWldRcElIdGNjbHh1WEhSY2RGeDBYSFJjZEM4dklFOXViSGtnWm1WMFkyZ2dhV1lnYm05MElHRnNjbVZoWkhrZ2FXNGdkR2hsSUdSbFpsRjFaWFZsTGx4eVhHNWNkRngwWEhSY2RGeDBhV1lnS0NGb1lYTlFjbTl3S0dOdmJuUmxlSFF1WkdWbVVYVmxkV1ZOWVhBc0lHbGtLU2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFIwYUdsekxtWmxkR05vS0NrN1hISmNibHgwWEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MGZTQmxiSE5sSUdsbUlDaDBhR2x6TG1WeWNtOXlLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUjBhR2x6TG1WdGFYUW9KMlZ5Y205eUp5d2dkR2hwY3k1bGNuSnZjaWs3WEhKY2JseDBYSFJjZEZ4MGZTQmxiSE5sSUdsbUlDZ2hkR2hwY3k1a1pXWnBibWx1WnlrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwTHk4Z1ZHaGxJR1poWTNSdmNua2dZMjkxYkdRZ2RISnBaMmRsY2lCaGJtOTBhR1Z5SUhKbGNYVnBjbVVnWTJGc2JGeHlYRzVjZEZ4MFhIUmNkRngwTHk4Z2RHaGhkQ0IzYjNWc1pDQnlaWE4xYkhRZ2FXNGdZMmhsWTJ0cGJtY2dkR2hwY3lCdGIyUjFiR1VnZEc5Y2NseHVYSFJjZEZ4MFhIUmNkQzh2SUdSbFptbHVaU0JwZEhObGJHWWdZV2RoYVc0dUlFbG1JR0ZzY21WaFpIa2dhVzRnZEdobElIQnliMk5sYzNOY2NseHVYSFJjZEZ4MFhIUmNkQzh2SUc5bUlHUnZhVzVuSUhSb1lYUXNJSE5yYVhBZ2RHaHBjeUIzYjNKckxseHlYRzVjZEZ4MFhIUmNkRngwZEdocGN5NWtaV1pwYm1sdVp5QTlJSFJ5ZFdVN1hISmNibHgwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RHbG1JQ2gwYUdsekxtUmxjRU52ZFc1MElEd2dNU0FtSmlBaGRHaHBjeTVrWldacGJtVmtLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR2xtSUNocGMwWjFibU4wYVc5dUtHWmhZM1J2Y25rcEtTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBkSEo1SUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RHVjRjRzl5ZEhNZ1BTQmpiMjUwWlhoMExtVjRaV05EWWlocFpDd2dabUZqZEc5eWVTd2daR1Z3Ulhod2IzSjBjeXdnWlhod2IzSjBjeWs3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwZlNCallYUmphQ0FvWlNrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBaWEp5SUQwZ1pUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUjlYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBMeThnUm1GMmIzSWdjbVYwZFhKdUlIWmhiSFZsSUc5MlpYSWdaWGh3YjNKMGN5NGdTV1lnYm05a1pTOWphbk1nYVc0Z2NHeGhlU3hjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFF2THlCMGFHVnVJSGRwYkd3Z2JtOTBJR2hoZG1VZ1lTQnlaWFIxY200Z2RtRnNkV1VnWVc1NWQyRjVMaUJHWVhadmNseHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RDOHZJRzF2WkhWc1pTNWxlSEJ2Y25SeklHRnpjMmxuYm0xbGJuUWdiM1psY2lCbGVIQnZjblJ6SUc5aWFtVmpkQzVjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJwWmlBb2RHaHBjeTV0WVhBdWFYTkVaV1pwYm1VZ0ppWWdaWGh3YjNKMGN5QTlQVDBnZFc1a1pXWnBibVZrS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmphbk5OYjJSMWJHVWdQU0IwYUdsekxtMXZaSFZzWlR0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RHbG1JQ2hqYW5OTmIyUjFiR1VwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RGeDBaWGh3YjNKMGN5QTlJR05xYzAxdlpIVnNaUzVsZUhCdmNuUnpPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwZlNCbGJITmxJR2xtSUNoMGFHbHpMblZ6YVc1blJYaHdiM0owY3lrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBYSFF2THlCbGVIQnZjblJ6SUdGc2NtVmhaSGtnYzJWMElIUm9aU0JrWldacGJtVmtJSFpoYkhWbExseHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBYSFJsZUhCdmNuUnpJRDBnZEdocGN5NWxlSEJ2Y25Sek8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJwWmlBb1pYSnlLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhRdkx5QkpaaUIwYUdWeVpTQnBjeUJoYmlCbGNuSnZjaUJzYVhOMFpXNWxjaXdnWm1GMmIzSWdjR0Z6YzJsdVoxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBMeThnZEc4Z2RHaGhkQ0JwYm5OMFpXRmtJRzltSUhSb2NtOTNhVzVuSUdGdUlHVnljbTl5TGlCSWIzZGxkbVZ5TEZ4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MEx5OGdiMjVzZVNCa2J5QnBkQ0JtYjNJZ1pHVm1hVzVsS0NrblpDQWdiVzlrZFd4bGN5NGdjbVZ4ZFdseVpWeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBMeThnWlhKeVltRmphM01nYzJodmRXeGtJRzV2ZENCaVpTQmpZV3hzWldRZ1ptOXlJR1poYVd4MWNtVnpJR2x1WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhRdkx5QjBhR1ZwY2lCallXeHNZbUZqYTNNZ0tDTTJPVGtwTGlCSWIzZGxkbVZ5SUdsbUlHRWdaMnh2WW1Gc1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUXZMeUJ2YmtWeWNtOXlJR2x6SUhObGRDd2dkWE5sSUhSb1lYUXVYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSFJwWmlBb0tIUm9hWE11WlhabGJuUnpMbVZ5Y205eUlDWW1JSFJvYVhNdWJXRndMbWx6UkdWbWFXNWxLU0I4ZkZ4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MFhIUnlaWEV1YjI1RmNuSnZjaUFoUFQwZ1pHVm1ZWFZzZEU5dVJYSnliM0lwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RGeDBaWEp5TG5KbGNYVnBjbVZOWVhBZ1BTQjBhR2x6TG0xaGNEdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkRngwWlhKeUxuSmxjWFZwY21WTmIyUjFiR1Z6SUQwZ2RHaHBjeTV0WVhBdWFYTkVaV1pwYm1VZ1B5QmJkR2hwY3k1dFlYQXVhV1JkSURvZ2JuVnNiRHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJjZEZ4MFpYSnlMbkpsY1hWcGNtVlVlWEJsSUQwZ2RHaHBjeTV0WVhBdWFYTkVaV1pwYm1VZ1B5QW5aR1ZtYVc1bEp5QTZJQ2R5WlhGMWFYSmxKenRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJjZEZ4MGNtVjBkWEp1SUc5dVJYSnliM0lvS0hSb2FYTXVaWEp5YjNJZ1BTQmxjbklwS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RIMGdaV3h6WlNCcFppQW9kSGx3Wlc5bUlHTnZibk52YkdVZ0lUMDlJQ2QxYm1SbFptbHVaV1FuSUNZbVhISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmNkR052Ym5OdmJHVXVaWEp5YjNJcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkRngwTHk4Z1RHOW5JSFJvWlNCbGNuSnZjaUJtYjNJZ1pHVmlkV2RuYVc1bkxpQkpaaUJ3Y205dGFYTmxjeUJqYjNWc1pDQmlaVnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwWEhRdkx5QjFjMlZrTENCMGFHbHpJSGR2ZFd4a0lHSmxJR1JwWm1abGNtVnVkQ3dnWW5WMElHMWhhMmx1WnlCa2J5NWNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkRngwWTI5dWMyOXNaUzVsY25KdmNpaGxjbklwTzF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MGZTQmxiSE5sSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RGeDBMeThnUkc4Z2JtOTBJSGRoYm5RZ2RHOGdZMjl0Y0d4bGRHVnNlU0JzYjNObElIUm9aU0JsY25KdmNpNGdWMmhwYkdVZ2RHaHBjMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwWEhRdkx5QjNhV3hzSUcxbGMzTWdkWEFnY0hKdlkyVnpjMmx1WnlCaGJtUWdiR1ZoWkNCMGJ5QnphVzFwYkdGeUlISmxjM1ZzZEhOY2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RGeDBMeThnWVhNZ1luVm5JREUwTkRBc0lHbDBJR0YwSUd4bFlYTjBJSE4xY21aaFkyVnpJSFJvWlNCbGNuSnZjaTVjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJjZEZ4MGNtVnhMbTl1UlhKeWIzSW9aWEp5S1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUjlYSEpjYmx4MFhIUmNkRngwWEhSY2RIMGdaV3h6WlNCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MEx5OGdTblZ6ZENCaElHeHBkR1Z5WVd3Z2RtRnNkV1ZjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJsZUhCdmNuUnpJRDBnWm1GamRHOXllVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFJjZEZ4MGRHaHBjeTVsZUhCdmNuUnpJRDBnWlhod2IzSjBjenRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RHbG1JQ2gwYUdsekxtMWhjQzVwYzBSbFptbHVaU0FtSmlBaGRHaHBjeTVwWjI1dmNtVXBJSHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJrWldacGJtVmtXMmxrWFNBOUlHVjRjRzl5ZEhNN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGFXWWdLSEpsY1M1dmJsSmxjMjkxY21ObFRHOWhaQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MGJHVjBJSEpsYzB4dllXUk5ZWEJ6SUQwZ1cxMDdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSFJsWVdOb0tIUm9hWE11WkdWd1RXRndjeXdnWm5WdVkzUnBiMjRvWkdWd1RXRndLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhSY2RISmxjMHh2WVdSTllYQnpMbkIxYzJnb1pHVndUV0Z3TG01dmNtMWhiR2w2WldSTllYQWdmSHdnWkdWd1RXRndLVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJjZEgwcE8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBjbVZ4TG05dVVtVnpiM1Z5WTJWTWIyRmtLR052Ym5SbGVIUXNJSFJvYVhNdWJXRndMQ0J5WlhOTWIyRmtUV0Z3Y3lrN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBYSFF2THlCRGJHVmhiaUIxY0Z4eVhHNWNkRngwWEhSY2RGeDBYSFJqYkdWaGJsSmxaMmx6ZEhKNUtHbGtLVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RIUm9hWE11WkdWbWFXNWxaQ0E5SUhSeWRXVTdYSEpjYmx4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RDOHZJRVpwYm1semFHVmtJSFJvWlNCa1pXWnBibVVnYzNSaFoyVXVJRUZzYkc5M0lHTmhiR3hwYm1jZ1kyaGxZMnNnWVdkaGFXNWNjbHh1WEhSY2RGeDBYSFJjZEM4dklIUnZJR0ZzYkc5M0lHUmxabWx1WlNCdWIzUnBabWxqWVhScGIyNXpJR0psYkc5M0lHbHVJSFJvWlNCallYTmxJRzltSUdGY2NseHVYSFJjZEZ4MFhIUmNkQzh2SUdONVkyeGxMbHh5WEc1Y2RGeDBYSFJjZEZ4MGRHaHBjeTVrWldacGJtbHVaeUE5SUdaaGJITmxPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFJwWmlBb2RHaHBjeTVrWldacGJtVmtJQ1ltSUNGMGFHbHpMbVJsWm1sdVpVVnRhWFIwWldRcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGRHaHBjeTVrWldacGJtVkZiV2wwZEdWa0lEMGdkSEoxWlR0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZEdocGN5NWxiV2wwS0Nka1pXWnBibVZrSnl3Z2RHaHBjeTVsZUhCdmNuUnpLVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBkR2hwY3k1a1pXWnBibVZGYldsMFEyOXRjR3hsZEdVZ1BTQjBjblZsTzF4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFI5TEZ4eVhHNWNkRngwWEhSY2NseHVYSFJjZEZ4MFkyRnNiRkJzZFdkcGJqb2dablZ1WTNScGIyNG9LU0I3WEhKY2JseDBYSFJjZEZ4MGJHVjBJRzFoY0NBOUlIUm9hWE11YldGd08xeHlYRzVjZEZ4MFhIUmNkR3hsZENCcFpDQTlJRzFoY0M1cFpEdGNjbHh1WEhSY2RGeDBYSFF2THlCTllYQWdZV3h5WldGa2VTQnViM0p0WVd4cGVtVmtJSFJvWlNCd2NtVm1hWGd1WEhKY2JseDBYSFJjZEZ4MGJHVjBJSEJzZFdkcGJrMWhjQ0E5SUcxaGEyVk5iMlIxYkdWTllYQW9iV0Z3TG5CeVpXWnBlQ2s3WEhKY2JseDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBMeThnVFdGeWF5QjBhR2x6SUdGeklHRWdaR1Z3Wlc1a1pXNWplU0JtYjNJZ2RHaHBjeUJ3YkhWbmFXNHNJSE52SUdsMFhISmNibHgwWEhSY2RGeDBMeThnWTJGdUlHSmxJSFJ5WVdObFpDQm1iM0lnWTNsamJHVnpMbHh5WEc1Y2RGeDBYSFJjZEhSb2FYTXVaR1Z3VFdGd2N5NXdkWE5vS0hCc2RXZHBiazFoY0NrN1hISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwYjI0b2NHeDFaMmx1VFdGd0xDQW5aR1ZtYVc1bFpDY3NJR0pwYm1Rb2RHaHBjeXdnWm5WdVkzUnBiMjRvY0d4MVoybHVLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUnNaWFFnYkc5aFpDd2dibTl5YldGc2FYcGxaRTFoY0N3Z2JtOXliV0ZzYVhwbFpFMXZaQ3hjY2x4dVhIUmNkRngwWEhSY2RGeDBZblZ1Wkd4bFNXUWdQU0JuWlhSUGQyNG9ZblZ1Wkd4bGMwMWhjQ3dnZEdocGN5NXRZWEF1YVdRcExGeHlYRzVjZEZ4MFhIUmNkRngwWEhSdVlXMWxJRDBnZEdocGN5NXRZWEF1Ym1GdFpTeGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGNHRnlaVzUwVG1GdFpTQTlJSFJvYVhNdWJXRndMbkJoY21WdWRFMWhjQ0EvSUhSb2FYTXViV0Z3TG5CaGNtVnVkRTFoY0M1dVlXMWxJRG9nYm5Wc2JDeGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGJHOWpZV3hTWlhGMWFYSmxJRDBnWTI5dWRHVjRkQzV0WVd0bFVtVnhkV2x5WlNodFlYQXVjR0Z5Wlc1MFRXRndMQ0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWlc1aFlteGxRblZwYkdSRFlXeHNZbUZqYXpvZ2RISjFaVnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUjlLVHRjY2x4dVhIUmNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkRngwTHk4Z1NXWWdZM1Z5Y21WdWRDQnRZWEFnYVhNZ2JtOTBJRzV2Y20xaGJHbDZaV1FzSUhkaGFYUWdabTl5SUhSb1lYUmNjbHh1WEhSY2RGeDBYSFJjZEM4dklHNXZjbTFoYkdsNlpXUWdibUZ0WlNCMGJ5QnNiMkZrSUdsdWMzUmxZV1FnYjJZZ1kyOXVkR2x1ZFdsdVp5NWNjbHh1WEhSY2RGeDBYSFJjZEdsbUlDaDBhR2x6TG0xaGNDNTFibTV2Y20xaGJHbDZaV1FwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwTHk4Z1RtOXliV0ZzYVhwbElIUm9aU0JKUkNCcFppQjBhR1VnY0d4MVoybHVJR0ZzYkc5M2N5QnBkQzVjY2x4dVhIUmNkRngwWEhSY2RGeDBhV1lnS0hCc2RXZHBiaTV1YjNKdFlXeHBlbVVwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSdVlXMWxJRDBnY0d4MVoybHVMbTV2Y20xaGJHbDZaU2h1WVcxbExDQm1kVzVqZEdsdmJpaHVZVzFsS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmNkSEpsZEhWeWJpQnViM0p0WVd4cGVtVW9ibUZ0WlN3Z2NHRnlaVzUwVG1GdFpTd2dkSEoxWlNrN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUjlLU0I4ZkNBbkp6dGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkRngwTHk4Z2NISmxabWw0SUdGdVpDQnVZVzFsSUhOb2IzVnNaQ0JoYkhKbFlXUjVJR0psSUc1dmNtMWhiR2w2WldRc0lHNXZJRzVsWldSY2NseHVYSFJjZEZ4MFhIUmNkRngwTHk4Z1ptOXlJR0Z3Y0d4NWFXNW5JRzFoY0NCamIyNW1hV2NnWVdkaGFXNGdaV2wwYUdWeUxseHlYRzVjZEZ4MFhIUmNkRngwWEhSdWIzSnRZV3hwZW1Wa1RXRndJRDBnYldGclpVMXZaSFZzWlUxaGNDaHRZWEF1Y0hKbFptbDRJQ3NnSnlFbklDc2dibUZ0WlN4Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSMGFHbHpMbTFoY0M1d1lYSmxiblJOWVhBcE8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSdmJpaHViM0p0WVd4cGVtVmtUV0Z3TEZ4eVhHNWNkRngwWEhSY2RGeDBYSFJjZENka1pXWnBibVZrSnl3Z1ltbHVaQ2gwYUdsekxDQm1kVzVqZEdsdmJpaDJZV3gxWlNrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBkR2hwY3k1dFlYQXVibTl5YldGc2FYcGxaRTFoY0NBOUlHNXZjbTFoYkdsNlpXUk5ZWEE3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhSMGFHbHpMbWx1YVhRb1cxMHNJR1oxYm1OMGFXOXVLQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MFhIUnlaWFIxY200Z2RtRnNkV1U3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhSOUxDQnVkV3hzTENCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmNkR1Z1WVdKc1pXUTZJSFJ5ZFdVc1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmNkR2xuYm05eVpUb2dkSEoxWlZ4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MGZTazdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBmU2twTzF4eVhHNWNkRngwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RGeDBibTl5YldGc2FYcGxaRTF2WkNBOUlHZGxkRTkzYmloeVpXZHBjM1J5ZVN3Z2JtOXliV0ZzYVhwbFpFMWhjQzVwWkNrN1hISmNibHgwWEhSY2RGeDBYSFJjZEdsbUlDaHViM0p0WVd4cGVtVmtUVzlrS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MEx5OGdUV0Z5YXlCMGFHbHpJR0Z6SUdFZ1pHVndaVzVrWlc1amVTQm1iM0lnZEdocGN5QndiSFZuYVc0c0lITnZJR2wwWEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwTHk4Z1kyRnVJR0psSUhSeVlXTmxaQ0JtYjNJZ1kzbGpiR1Z6TGx4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEhSb2FYTXVaR1Z3VFdGd2N5NXdkWE5vS0c1dmNtMWhiR2w2WldSTllYQXBPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkR2xtSUNoMGFHbHpMbVYyWlc1MGN5NWxjbkp2Y2lrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBibTl5YldGc2FYcGxaRTF2WkM1dmJpZ25aWEp5YjNJbkxDQmlhVzVrS0hSb2FYTXNJR1oxYm1OMGFXOXVLR1Z5Y2lrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBYSFIwYUdsekxtVnRhWFFvSjJWeWNtOXlKeXdnWlhKeUtUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkSDBwS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGJtOXliV0ZzYVhwbFpFMXZaQzVsYm1GaWJHVW9LVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFJjZEZ4MGNtVjBkWEp1TzF4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFF2THlCSlppQmhJSEJoZEdoeklHTnZibVpwWnl3Z2RHaGxiaUJxZFhOMElHeHZZV1FnZEdoaGRDQm1hV3hsSUdsdWMzUmxZV1FnZEc5Y2NseHVYSFJjZEZ4MFhIUmNkQzh2SUhKbGMyOXNkbVVnZEdobElIQnNkV2RwYml3Z1lYTWdhWFFnYVhNZ1luVnBiSFFnYVc1MGJ5QjBhR0YwSUhCaGRHaHpJR3hoZVdWeUxseHlYRzVjZEZ4MFhIUmNkRngwYVdZZ0tHSjFibVJzWlVsa0tTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RIUm9hWE11YldGd0xuVnliQ0E5SUdOdmJuUmxlSFF1Ym1GdFpWUnZWWEpzS0dKMWJtUnNaVWxrS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZEdocGN5NXNiMkZrS0NrN1hISmNibHgwWEhSY2RGeDBYSFJjZEhKbGRIVnlianRjY2x4dVhIUmNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBiRzloWkNBOUlHSnBibVFvZEdocGN5d2dablZ1WTNScGIyNG9kbUZzZFdVcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGRHaHBjeTVwYm1sMEtGdGRMQ0JtZFc1amRHbHZiaWdwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSeVpYUjFjbTRnZG1Gc2RXVTdYSEpjYmx4MFhIUmNkRngwWEhSY2RIMHNJRzUxYkd3c0lIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmxibUZpYkdWa09pQjBjblZsWEhKY2JseDBYSFJjZEZ4MFhIUmNkSDBwTzF4eVhHNWNkRngwWEhSY2RGeDBmU2s3WEhKY2JseDBYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFJjZEd4dllXUXVaWEp5YjNJZ1BTQmlhVzVrS0hSb2FYTXNJR1oxYm1OMGFXOXVLR1Z5Y2lrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSMGFHbHpMbWx1YVhSbFpDQTlJSFJ5ZFdVN1hISmNibHgwWEhSY2RGeDBYSFJjZEhSb2FYTXVaWEp5YjNJZ1BTQmxjbkk3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR1Z5Y2k1eVpYRjFhWEpsVFc5a2RXeGxjeUE5SUZ0cFpGMDdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkRngwWEhRdkx5QlNaVzF2ZG1VZ2RHVnRjQ0IxYm01dmNtMWhiR2w2WldRZ2JXOWtkV3hsY3lCbWIzSWdkR2hwY3lCdGIyUjFiR1VzSUhOcGJtTmxJSFJvWlhrZ2QybHNiQ0J1WlhabGNpQmlaU0J5WlhOdmJIWmxaQ0J2ZEdobGNuZHBjMlVnWEhKY2JseDBYSFJjZEZ4MFhIUmNkQzh2SUc1dmR5NWNjbHh1WEhSY2RGeDBYSFJjZEZ4MFpXRmphRkJ5YjNBb2NtVm5hWE4wY25rc0lHWjFibU4wYVc5dUtHMXZaQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEdsbUlDaHRiMlF1YldGd0xtbGtMbWx1WkdWNFQyWW9hV1FnS3lBblgzVnVibTl5YldGc2FYcGxaQ2NwSUQwOVBTQXdLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhSamJHVmhibEpsWjJsemRISjVLRzF2WkM1dFlYQXVhV1FwTzF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhIUmNkRngwZlNrN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBYSFJ2YmtWeWNtOXlLR1Z5Y2lrN1hISmNibHgwWEhSY2RGeDBYSFI5S1R0Y2NseHVYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MEx5OGdRV3hzYjNjZ2NHeDFaMmx1Y3lCMGJ5QnNiMkZrSUc5MGFHVnlJR052WkdVZ2QybDBhRzkxZENCb1lYWnBibWNnZEc4Z2EyNXZkeUIwYUdVZ1kyOXVkR1Y0ZENCdmNpQm9iM2NnZEc4Z0oyTnZiWEJzWlhSbEp5QjBhR1VnWEhKY2JseDBYSFJjZEZ4MFhIUXZMeUJzYjJGa0xseHlYRzVjZEZ4MFhIUmNkRngwYkc5aFpDNW1jbTl0VkdWNGRDQTlJR0pwYm1Rb2RHaHBjeXdnWm5WdVkzUnBiMjRvZEdWNGRDd2dkR1Y0ZEVGc2RDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUXZLbXB6YkdsdWRDQmxkbWxzT2lCMGNuVmxJQ292WEhKY2JseDBYSFJjZEZ4MFhIUmNkR3hsZENCdGIyUjFiR1ZPWVcxbElEMGdiV0Z3TG01aGJXVXNYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBiVzlrZFd4bFRXRndJRDBnYldGclpVMXZaSFZzWlUxaGNDaHRiMlIxYkdWT1lXMWxLU3hjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJvWVhOSmJuUmxjbUZqZEdsMlpTQTlJSFZ6WlVsdWRHVnlZV04wYVhabE8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkRngwTHk4Z1FYTWdiMllnTWk0eExqQXNJSE4xY0hCdmNuUWdhblZ6ZENCd1lYTnphVzVuSUhSb1pTQjBaWGgwTENCMGJ5QnlaV2x1Wm05eVkyVWdabkp2YlZSbGVIUWdiMjVzZVNCaVpXbHVaeUJqWVd4c1pXUWdiMjVqWlNCd1pYSWdYSEpjYmx4MFhIUmNkRngwWEhSY2RDOHZJSEpsYzI5MWNtTmxMaUJUZEdsc2JDQnpkWEJ3YjNKMElHOXNaQ0J6ZEhsc1pTQnZaaUJ3WVhOemFXNW5JRzF2WkhWc1pVNWhiV1VnWW5WMElHUnBjMk5oY21RZ2RHaGhkQ0J0YjJSMWJHVk9ZVzFsSUdsdUlHWmhkbTl5SUZ4eVhHNWNkRngwWEhSY2RGeDBYSFF2THlCdlppQjBhR1VnYVc1MFpYSnVZV3dnY21WbUxseHlYRzVjZEZ4MFhIUmNkRngwWEhScFppQW9kR1Y0ZEVGc2RDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkSFJsZUhRZ1BTQjBaWGgwUVd4ME8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBYSFF2THlCVWRYSnVJRzltWmlCcGJuUmxjbUZqZEdsMlpTQnpZM0pwY0hRZ2JXRjBZMmhwYm1jZ1ptOXlJRWxGSUdadmNpQmhibmtnWkdWbWFXNWxJR05oYkd4eklHbHVJSFJvWlNCMFpYaDBMQ0IwYUdWdUlIUjFjbTRnYVhRZ1hISmNibHgwWEhSY2RGeDBYSFJjZEM4dklHSmhZMnNnYjI0Z1lYUWdkR2hsSUdWdVpDNWNjbHh1WEhSY2RGeDBYSFJjZEZ4MGFXWWdLR2hoYzBsdWRHVnlZV04wYVhabEtTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBkWE5sU1c1MFpYSmhZM1JwZG1VZ1BTQm1ZV3h6WlR0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RGeDBMeThnVUhKcGJXVWdkR2hsSUhONWMzUmxiU0JpZVNCamNtVmhkR2x1WnlCaElHMXZaSFZzWlNCcGJuTjBZVzVqWlNCbWIzSWdhWFF1WEhKY2JseDBYSFJjZEZ4MFhIUmNkR2RsZEUxdlpIVnNaU2h0YjJSMWJHVk5ZWEFwTzF4eVhHNWNkRngwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RGeDBMeThnVkhKaGJuTm1aWElnWVc1NUlHTnZibVpwWnlCMGJ5QjBhR2x6SUc5MGFHVnlJRzF2WkhWc1pTNWNjbHh1WEhSY2RGeDBYSFJjZEZ4MGFXWWdLR2hoYzFCeWIzQW9ZMjl1Wm1sbkxtTnZibVpwWnl3Z2FXUXBLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWTI5dVptbG5MbU52Ym1acFoxdHRiMlIxYkdWT1lXMWxYU0E5SUdOdmJtWnBaeTVqYjI1bWFXZGJhV1JkTzF4eVhHNWNkRngwWEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUjBjbmtnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEhKbGNTNWxlR1ZqS0hSbGVIUXBPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUjlJR05oZEdOb0lDaGxLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwY21WMGRYSnVJRzl1UlhKeWIzSW9iV0ZyWlVWeWNtOXlLQ2RtY205dGRHVjRkR1YyWVd3bkxGeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RGeDBKMlp5YjIxVVpYaDBJR1YyWVd3Z1ptOXlJQ2NnS3lCcFpDQXJYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSFFuSUdaaGFXeGxaRG9nSnlBcklHVXNYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSFJsTEZ4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MFcybGtYU2twTzF4eVhHNWNkRngwWEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUnBaaUFvYUdGelNXNTBaWEpoWTNScGRtVXBJSHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFIxYzJWSmJuUmxjbUZqZEdsMlpTQTlJSFJ5ZFdVN1hISmNibHgwWEhSY2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhIUmNkRngwWEhKY2JseDBYSFJjZEZ4MFhIUmNkQzh2SUUxaGNtc2dkR2hwY3lCaGN5QmhJR1JsY0dWdVpHVnVZM2tnWm05eUlIUm9aU0J3YkhWbmFXNGdjbVZ6YjNWeVkyVXVYSEpjYmx4MFhIUmNkRngwWEhSY2RIUm9hWE11WkdWd1RXRndjeTV3ZFhOb0tHMXZaSFZzWlUxaGNDazdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkRngwWEhRdkx5QlRkWEJ3YjNKMElHRnViMjU1Ylc5MWN5QnRiMlIxYkdWekxseHlYRzVjZEZ4MFhIUmNkRngwWEhSamIyNTBaWGgwTG1OdmJYQnNaWFJsVEc5aFpDaHRiMlIxYkdWT1lXMWxLVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RDOHZJRUpwYm1RZ2RHaGxJSFpoYkhWbElHOW1JSFJvWVhRZ2JXOWtkV3hsSUhSdklIUm9aU0IyWVd4MVpTQm1iM0lnZEdocGN5QnlaWE52ZFhKalpTQkpSQzVjY2x4dVhIUmNkRngwWEhSY2RGeDBiRzlqWVd4U1pYRjFhWEpsS0Z0dGIyUjFiR1ZPWVcxbFhTd2diRzloWkNrN1hISmNibHgwWEhSY2RGeDBYSFI5S1R0Y2NseHVYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MEx5OGdWWE5sSUhCaGNtVnVkRTVoYldVZ2FHVnlaU0J6YVc1alpTQjBhR1VnY0d4MVoybHVKM01nYm1GdFpTQnBjeUJ1YjNRZ2NtVnNhV0ZpYkdVc0lHTnZkV3hrSUdKbElITnZiV1VnZDJWcGNtUWdjM1J5YVc1bklIZHBkR2dnYm04Z1hISmNibHgwWEhSY2RGeDBYSFF2THlCd1lYUm9JSFJvWVhRZ1lXTjBkV0ZzYkhrZ2QyRnVkSE1nZEc4Z2NtVm1aWEpsYm1ObElIUm9aU0J3WVhKbGJuUk9ZVzFsSjNNZ2NHRjBhQzVjY2x4dVhIUmNkRngwWEhSY2RIQnNkV2RwYmk1c2IyRmtLRzFoY0M1dVlXMWxMQ0JzYjJOaGJGSmxjWFZwY21Vc0lHeHZZV1FzSUdOdmJtWnBaeWs3WEhKY2JseDBYSFJjZEZ4MGZTa3BPMXh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RHTnZiblJsZUhRdVpXNWhZbXhsS0hCc2RXZHBiazFoY0N3Z2RHaHBjeWs3WEhKY2JseDBYSFJjZEZ4MGRHaHBjeTV3YkhWbmFXNU5ZWEJ6VzNCc2RXZHBiazFoY0M1cFpGMGdQU0J3YkhWbmFXNU5ZWEE3WEhKY2JseDBYSFJjZEgwc1hISmNibHgwWEhSY2RGeHlYRzVjZEZ4MFhIUmxibUZpYkdVNklHWjFibU4wYVc5dUtDa2dlMXh5WEc1Y2RGeDBYSFJjZEdWdVlXSnNaV1JTWldkcGMzUnllVnQwYUdsekxtMWhjQzVwWkYwZ1BTQjBhR2x6TzF4eVhHNWNkRngwWEhSY2RIUm9hWE11Wlc1aFlteGxaQ0E5SUhSeWRXVTdYSEpjYmx4MFhIUmNkRngwWEhKY2JseDBYSFJjZEZ4MEx5OGdVMlYwSUdac1lXY2diV1Z1ZEdsdmJtbHVaeUIwYUdGMElIUm9aU0J0YjJSMWJHVWdhWE1nWlc1aFlteHBibWNzSUhOdklIUm9ZWFFnYVcxdFpXUnBZWFJsSUdOaGJHeHpJSFJ2SUhSb1pTQmtaV1pwYm1Wa0lHTmhiR3hpWVdOcmN5Qm1iM0lnWEhKY2JseDBYSFJjZEZ4MEx5OGdaR1Z3Wlc1a1pXNWphV1Z6SUdSdklHNXZkQ0IwY21sbloyVnlJR2x1WVdSMlpYSjBaVzUwSUd4dllXUWdkMmwwYUNCMGFHVWdaR1Z3UTI5MWJuUWdjM1JwYkd3Z1ltVnBibWNnZW1WeWJ5NWNjbHh1WEhSY2RGeDBYSFIwYUdsekxtVnVZV0pzYVc1bklEMGdkSEoxWlR0Y2NseHVYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFF2THlCRmJtRmliR1VnWldGamFDQmtaWEJsYm1SbGJtTjVMbHh5WEc1Y2RGeDBYSFJjZEdWaFkyZ29kR2hwY3k1a1pYQk5ZWEJ6TENCaWFXNWtLSFJvYVhNc0lHWjFibU4wYVc5dUtHUmxjRTFoY0N3Z2FTa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGJHVjBJR2xrT3lCY2NseHVYSFJjZEZ4MFhIUmNkR3hsZENCdGIyUTdJRnh5WEc1Y2RGeDBYSFJjZEZ4MGJHVjBJR2hoYm1Sc1pYSTdYSEpjYmx4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkR2xtSUNoMGVYQmxiMllnWkdWd1RXRndJRDA5UFNBbmMzUnlhVzVuSnlrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhRdkx5QkVaWEJsYm1SbGJtTjVJRzVsWldSeklIUnZJR0psSUdOdmJuWmxjblJsWkNCMGJ5QmhJR1JsY0UxaGNDQmhibVFnZDJseVpXUWdkWEFnZEc4Z2RHaHBjeUJ0YjJSMWJHVXVYSEpjYmx4MFhIUmNkRngwWEhSY2RHUmxjRTFoY0NBOUlHMWhhMlZOYjJSMWJHVk5ZWEFvWkdWd1RXRndMRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkQ2gwYUdsekxtMWhjQzVwYzBSbFptbHVaU0EvSUhSb2FYTXViV0Z3SURvZ2RHaHBjeTV0WVhBdWNHRnlaVzUwVFdGd0tTeGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUm1ZV3h6WlN4Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhRaGRHaHBjeTV6YTJsd1RXRndLVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBkR2hwY3k1a1pYQk5ZWEJ6VzJsZElEMGdaR1Z3VFdGd08xeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkRngwYUdGdVpHeGxjaUE5SUdkbGRFOTNiaWhvWVc1a2JHVnljeXdnWkdWd1RXRndMbWxrS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhKY2JseDBYSFJjZEZ4MFhIUmNkR2xtSUNob1lXNWtiR1Z5S1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGRHaHBjeTVrWlhCRmVIQnZjblJ6VzJsZElEMGdhR0Z1Wkd4bGNpaDBhR2x6S1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSeVpYUjFjbTQ3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RIUm9hWE11WkdWd1EyOTFiblFnS3owZ01UdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFJjZEc5dUtHUmxjRTFoY0N3Z0oyUmxabWx1WldRbkxDQmlhVzVrS0hSb2FYTXNJR1oxYm1OMGFXOXVLR1JsY0VWNGNHOXlkSE1wSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhScFppQW9kR2hwY3k1MWJtUmxabVZrS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUnlaWFIxY200N1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RIUm9hWE11WkdWbWFXNWxSR1Z3S0drc0lHUmxjRVY0Y0c5eWRITXBPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkSFJvYVhNdVkyaGxZMnNvS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZlNrcE8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkRngwYVdZZ0tIUm9hWE11WlhKeVltRmpheWtnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEc5dUtHUmxjRTFoY0N3Z0oyVnljbTl5Snl3Z1ltbHVaQ2gwYUdsekxDQjBhR2x6TG1WeWNtSmhZMnNwS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwZlNCbGJITmxJR2xtSUNoMGFHbHpMbVYyWlc1MGN5NWxjbkp2Y2lrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RDOHZJRTV2SUdScGNtVmpkQ0JsY25KaVlXTnJJRzl1SUhSb2FYTWdiVzlrZFd4bExDQmlkWFFnYzI5dFpYUm9hVzVuSUdWc2MyVWdhWE1nYkdsemRHVnVhVzVuSUdadmNpQmxjbkp2Y25Nc0lITnZJR0psSUhOMWNtVWdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBMeThnZEc4Z2NISnZjR0ZuWVhSbElIUm9aU0JsY25KdmNpQmpiM0p5WldOMGJIa3VYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBiMjRvWkdWd1RXRndMQ0FuWlhKeWIzSW5MQ0JpYVc1a0tIUm9hWE1zSUdaMWJtTjBhVzl1S0dWeWNpa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwZEdocGN5NWxiV2wwS0NkbGNuSnZjaWNzSUdWeWNpazdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBmU2twTzF4eVhHNWNkRngwWEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MFhIUjlYSEpjYmx4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkR2xrSUQwZ1pHVndUV0Z3TG1sa08xeHlYRzVjZEZ4MFhIUmNkRngwYlc5a0lEMGdjbVZuYVhOMGNubGJhV1JkTzF4eVhHNWNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhRdkx5QlRhMmx3SUhOd1pXTnBZV3dnYlc5a2RXeGxjeUJzYVd0bElDZHlaWEYxYVhKbEp5d2dKMlY0Y0c5eWRITW5MQ0FuYlc5a2RXeGxKeTRnUVd4emJ5d2daRzl1SjNRZ1kyRnNiQ0JsYm1GaWJHVWdhV1lnYVhRZ2FYTWdYSEpjYmx4MFhIUmNkRngwWEhRdkx5QmhiSEpsWVdSNUlHVnVZV0pzWldRc0lHbHRjRzl5ZEdGdWRDQnBiaUJqYVhKamRXeGhjaUJrWlhCbGJtUmxibU41SUdOaGMyVnpMbHh5WEc1Y2RGeDBYSFJjZEZ4MGFXWWdLQ0ZvWVhOUWNtOXdLR2hoYm1Sc1pYSnpMQ0JwWkNrZ0ppWWdiVzlrSUNZbUlDRnRiMlF1Wlc1aFlteGxaQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJqYjI1MFpYaDBMbVZ1WVdKc1pTaGtaWEJOWVhBc0lIUm9hWE1wTzF4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEgwcEtUdGNjbHh1WEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhRdkx5QkZibUZpYkdVZ1pXRmphQ0J3YkhWbmFXNGdkR2hoZENCcGN5QjFjMlZrSUdsdUlDQmhJR1JsY0dWdVpHVnVZM2t1WEhKY2JseDBYSFJjZEZ4MFpXRmphRkJ5YjNBb2RHaHBjeTV3YkhWbmFXNU5ZWEJ6TENCaWFXNWtLSFJvYVhNc0lHWjFibU4wYVc5dUtIQnNkV2RwYmsxaGNDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGJHVjBJRzF2WkNBOUlHZGxkRTkzYmloeVpXZHBjM1J5ZVN3Z2NHeDFaMmx1VFdGd0xtbGtLVHRjY2x4dVhIUmNkRngwWEhSY2RHbG1JQ2h0YjJRZ0ppWWdJVzF2WkM1bGJtRmliR1ZrS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEdOdmJuUmxlSFF1Wlc1aFlteGxLSEJzZFdkcGJrMWhjQ3dnZEdocGN5azdYSEpjYmx4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBmU2twTzF4eVhHNWNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkSFJvYVhNdVpXNWhZbXhwYm1jZ1BTQm1ZV3h6WlR0Y2NseHVYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFIwYUdsekxtTm9aV05yS0NrN1hISmNibHgwWEhSY2RIMHNYSEpjYmx4MFhIUmNkRnh5WEc1Y2RGeDBYSFJ2YmpvZ1puVnVZM1JwYjI0b2JtRnRaU3dnWTJJcElIdGNjbHh1WEhSY2RGeDBYSFJzWlhRZ1kySnpJRDBnZEdocGN5NWxkbVZ1ZEhOYmJtRnRaVjA3WEhKY2JseDBYSFJjZEZ4MGFXWWdLQ0ZqWW5NcElIdGNjbHh1WEhSY2RGeDBYSFJjZEdOaWN5QTlJSFJvYVhNdVpYWmxiblJ6VzI1aGJXVmRJRDBnVzEwN1hISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEdOaWN5NXdkWE5vS0dOaUtUdGNjbHh1WEhSY2RGeDBmU3hjY2x4dVhIUmNkRngwWEhKY2JseDBYSFJjZEdWdGFYUTZJR1oxYm1OMGFXOXVLRzVoYldVc0lHVjJkQ2tnZTF4eVhHNWNkRngwWEhSY2RHVmhZMmdvZEdocGN5NWxkbVZ1ZEhOYmJtRnRaVjBzSUdaMWJtTjBhVzl1S0dOaUtTQjdYSEpjYmx4MFhIUmNkRngwWEhSallpaGxkblFwTzF4eVhHNWNkRngwWEhSY2RIMHBPMXh5WEc1Y2RGeDBYSFJjZEdsbUlDaHVZVzFsSUQwOVBTQW5aWEp5YjNJbktTQjdYSEpjYmx4MFhIUmNkRngwWEhRdkx5Qk9iM2NnZEdoaGRDQjBhR1VnWlhKeWIzSWdhR0Z1Wkd4bGNpQjNZWE1nZEhKcFoyZGxjbVZrTENCeVpXMXZkbVVnZEdobElHeHBjM1JsYm1WeWN5d2djMmx1WTJVZ2RHaHBjeUJpY205clpXNGdUVzlrZFd4bElHbHVjM1JoYm1ObFhISmNibHgwWEhSY2RGeDBYSFF2THlCallXNGdjM1JoZVNCaGNtOTFibVFnWm05eUlHRWdkMmhwYkdVZ2FXNGdkR2hsSUhKbFoybHpkSEo1TGx4eVhHNWNkRngwWEhSY2RGeDBaR1ZzWlhSbElIUm9hWE11WlhabGJuUnpXMjVoYldWZE8xeHlYRzVjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwZlZ4eVhHNWNkRngwZlR0Y2NseHVYSFJjZEZ4eVhHNWNkRngwWm5WdVkzUnBiMjRnWTJGc2JFZGxkRTF2WkhWc1pTaGhjbWR6S1NCN1hISmNibHgwWEhSY2RDOHZJRk5yYVhBZ2JXOWtkV3hsY3lCaGJISmxZV1I1SUdSbFptbHVaV1F1WEhKY2JseDBYSFJjZEdsbUlDZ2hhR0Z6VUhKdmNDaGtaV1pwYm1Wa0xDQmhjbWR6V3pCZEtTa2dlMXh5WEc1Y2RGeDBYSFJjZEdkbGRFMXZaSFZzWlNodFlXdGxUVzlrZFd4bFRXRndLR0Z5WjNOYk1GMHNJRzUxYkd3c0lIUnlkV1VwS1M1cGJtbDBLR0Z5WjNOYk1WMHNJR0Z5WjNOYk1sMHBPMXh5WEc1Y2RGeDBYSFI5WEhKY2JseDBYSFI5WEhKY2JseDBYSFJjY2x4dVhIUmNkR1oxYm1OMGFXOXVJSEpsYlc5MlpVeHBjM1JsYm1WeUtHNXZaR1VzSUdaMWJtTXNJRzVoYldVc0lHbGxUbUZ0WlNrZ2UxeHlYRzVjZEZ4MFhIUXZMeUJHWVhadmNpQmtaWFJoWTJoRmRtVnVkQ0JpWldOaGRYTmxJRzltSUVsRk9TQnBjM04xWlN3Z2MyVmxJR0YwZEdGamFFVjJaVzUwTDJGa1pFVjJaVzUwVEdsemRHVnVaWElnWTI5dGJXVnVkQ0JsYkhObGQyaGxjbVVnYVc0Z2RHaHBjeUJtYVd4bExseHlYRzVjZEZ4MFhIUnBaaUFvYm05a1pTNWtaWFJoWTJoRmRtVnVkQ0FtSmlBaGFYTlBjR1Z5WVNrZ2UxeHlYRzVjZEZ4MFhIUmNkQzh2SUZCeWIySmhZbXg1SUVsRkxpQkpaaUJ1YjNRZ2FYUWdkMmxzYkNCMGFISnZkeUJoYmlCbGNuSnZjaXdnZDJocFkyZ2dkMmxzYkNCaVpTQjFjMlZtZFd3Z2RHOGdhMjV2ZHk1Y2NseHVYSFJjZEZ4MFhIUnBaaUFvYVdWT1lXMWxLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUnViMlJsTG1SbGRHRmphRVYyWlc1MEtHbGxUbUZ0WlN3Z1puVnVZeWs3WEhKY2JseDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUjlJR1ZzYzJVZ2UxeHlYRzVjZEZ4MFhIUmNkRzV2WkdVdWNtVnRiM1psUlhabGJuUk1hWE4wWlc1bGNpaHVZVzFsTENCbWRXNWpMQ0JtWVd4elpTazdYSEpjYmx4MFhIUmNkSDFjY2x4dVhIUmNkSDFjY2x4dVhIUmNkRnh5WEc1Y2RGeDBMeW9xWEhKY2JseDBYSFFnS2lCSGFYWmxiaUJoYmlCbGRtVnVkQ0JtY205dElHRWdjMk55YVhCMElHNXZaR1VzSUdkbGRDQjBhR1VnY21WeGRXbHlaV3B6SUdsdVptOGdabkp2YlNCcGRDd2dZVzVrSUhSb1pXNGdjbVZ0YjNabGN5QjBhR1VnWlhabGJuUWdiR2x6ZEdWdVpYSnpJRzl1SUZ4eVhHNWNkRngwSUNvZ2RHaGxJRzV2WkdVdVhISmNibHgwWEhRZ0tpQmNjbHh1WEhSY2RDQXFJRUJ3WVhKaGJTQjdSWFpsYm5SOUlHVjJkRnh5WEc1Y2RGeDBJQ29nWEhKY2JseDBYSFFnS2lCQWNtVjBkWEp1SUh0UFltcGxZM1I5WEhKY2JseDBYSFFnS2k5Y2NseHVYSFJjZEdaMWJtTjBhVzl1SUdkbGRGTmpjbWx3ZEVSaGRHRW9aWFowS1NCN1hISmNibHgwWEhSY2RDOHZJRlZ6YVc1bklHTjFjbkpsYm5SVVlYSm5aWFFnYVc1emRHVmhaQ0J2WmlCMFlYSm5aWFFnWm05eUlFWnBjbVZtYjNnZ01pNHdKM01nYzJGclpTNGdUbTkwSUdGc2JDQnZiR1FnWW5KdmQzTmxjbk1nZDJsc2JDQmlaU0J6ZFhCd2IzSjBaV1FzSUdKMWRDQmNjbHh1WEhSY2RGeDBMeThnZEdocGN5QnZibVVnZDJGeklHVmhjM2tnWlc1dmRXZG9JSFJ2SUhOMWNIQnZjblFnWVc1a0lITjBhV3hzSUcxaGEyVnpJSE5sYm5ObExseHlYRzVjZEZ4MFhIUnNaWFFnYm05a1pTQTlJR1YyZEM1amRYSnlaVzUwVkdGeVoyVjBJSHg4SUdWMmRDNXpjbU5GYkdWdFpXNTBPMXh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwTHk4Z1VtVnRiM1psSUhSb1pTQnNhWE4wWlc1bGNuTWdiMjVqWlNCb1pYSmxMbHh5WEc1Y2RGeDBYSFJ5WlcxdmRtVk1hWE4wWlc1bGNpaHViMlJsTENCamIyNTBaWGgwTG05dVUyTnlhWEIwVEc5aFpDd2dKMnh2WVdRbkxDQW5iMjV5WldGa2VYTjBZWFJsWTJoaGJtZGxKeWs3WEhKY2JseDBYSFJjZEhKbGJXOTJaVXhwYzNSbGJtVnlLRzV2WkdVc0lHTnZiblJsZUhRdWIyNVRZM0pwY0hSRmNuSnZjaXdnSjJWeWNtOXlKeWs3WEhKY2JseDBYSFJjZEZ4eVhHNWNkRngwWEhSeVpYUjFjbTRnZTF4eVhHNWNkRngwWEhSY2RHNXZaR1U2SUc1dlpHVXNYSEpjYmx4MFhIUmNkRngwYVdRNklHNXZaR1VnSmlZZ2JtOWtaUzVuWlhSQmRIUnlhV0oxZEdVb0oyUmhkR0V0Y21WeGRXbHlaVzF2WkhWc1pTY3BYSEpjYmx4MFhIUmNkSDA3WEhKY2JseDBYSFI5WEhKY2JseDBYSFJjY2x4dVhIUmNkR1oxYm1OMGFXOXVJR2x1ZEdGclpVUmxabWx1WlhNb0tTQjdYSEpjYmx4MFhIUmNkR3hsZENCaGNtZHpPMXh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwTHk4Z1FXNTVJR1JsWm1sdVpXUWdiVzlrZFd4bGN5QnBiaUIwYUdVZ1oyeHZZbUZzSUhGMVpYVmxMQ0JwYm5SaGEyVWdkR2hsYlNCdWIzY3VYSEpjYmx4MFhIUmNkSFJoYTJWSGJHOWlZV3hSZFdWMVpTZ3BPMXh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwTHk4Z1RXRnJaU0J6ZFhKbElHRnVlU0J5WlcxaGFXNXBibWNnWkdWbVVYVmxkV1VnYVhSbGJYTWdaMlYwSUhCeWIzQmxjbXg1SUhCeWIyTmxjM05sWkM1Y2NseHVYSFJjZEZ4MGQyaHBiR1VnS0dSbFpsRjFaWFZsTG14bGJtZDBhQ2tnZTF4eVhHNWNkRngwWEhSY2RHRnlaM01nUFNCa1pXWlJkV1YxWlM1emFHbG1kQ2dwTzF4eVhHNWNkRngwWEhSY2RHbG1JQ2hoY21keld6QmRJRDA5UFNCdWRXeHNLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUnlaWFIxY200Z2IyNUZjbkp2Y2lodFlXdGxSWEp5YjNJb0oyMXBjMjFoZEdOb0p5d2dKMDFwYzIxaGRHTm9aV1FnWVc1dmJubHRiM1Z6SUdSbFptbHVaU2dwSUcxdlpIVnNaVG9nSnlBclhISmNibHgwWEhSY2RGeDBYSFJjZEdGeVozTmJZWEpuY3k1c1pXNW5kR2dnTFNBeFhTa3BPMXh5WEc1Y2RGeDBYSFJjZEgwZ1pXeHpaU0I3WEhKY2JseDBYSFJjZEZ4MFhIUXZMeUJoY21keklHRnlaU0JwWkN3Z1pHVndjeXdnWm1GamRHOXllUzRnVTJodmRXeGtJR0psSUc1dmNtMWhiR2w2WldRZ1lua2dkR2hsWEhKY2JseDBYSFJjZEZ4MFhIUXZMeUJrWldacGJtVW9LU0JtZFc1amRHbHZiaTVjY2x4dVhIUmNkRngwWEhSY2RHTmhiR3hIWlhSTmIyUjFiR1VvWVhKbmN5azdYSEpjYmx4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSOVhISmNibHgwWEhSY2RHTnZiblJsZUhRdVpHVm1VWFZsZFdWTllYQWdQU0I3ZlR0Y2NseHVYSFJjZEgxY2NseHVYSFJjZEZ4eVhHNWNkRngwWTI5dWRHVjRkQ0E5SUh0Y2NseHVYSFJjZEZ4MFkyOXVabWxuT2lCamIyNW1hV2NzWEhKY2JseDBYSFJjZEdOdmJuUmxlSFJPWVcxbE9pQmpiMjUwWlhoMFRtRnRaU3hjY2x4dVhIUmNkRngwY21WbmFYTjBjbms2SUhKbFoybHpkSEo1TEZ4eVhHNWNkRngwWEhSa1pXWnBibVZrT2lCa1pXWnBibVZrTEZ4eVhHNWNkRngwWEhSMWNteEdaWFJqYUdWa09pQjFjbXhHWlhSamFHVmtMRnh5WEc1Y2RGeDBYSFJrWldaUmRXVjFaVG9nWkdWbVVYVmxkV1VzWEhKY2JseDBYSFJjZEdSbFpsRjFaWFZsVFdGd09pQjdmU3hjY2x4dVhIUmNkRngwVFc5a2RXeGxPaUJOYjJSMWJHVXNYSEpjYmx4MFhIUmNkRzFoYTJWTmIyUjFiR1ZOWVhBNklHMWhhMlZOYjJSMWJHVk5ZWEFzWEhKY2JseDBYSFJjZEc1bGVIUlVhV05yT2lCeVpYRXVibVY0ZEZScFkyc3NYSEpjYmx4MFhIUmNkRzl1UlhKeWIzSTZJRzl1UlhKeWIzSXNYSEpjYmx4MFhIUmNkRnh5WEc1Y2RGeDBYSFF2S2lwY2NseHVYSFJjZEZ4MElDb2dVMlYwSUdFZ1kyOXVabWxuZFhKaGRHbHZiaUJtYjNJZ2RHaGxJR052Ym5SbGVIUXVYSEpjYmx4MFhIUmNkQ0FxSUZ4eVhHNWNkRngwWEhRZ0tpQkFjR0Z5WVcwZ2UwOWlhbVZqZEgwZ1kyWm5JR052Ym1acFp5QnZZbXBsWTNRZ2RHOGdhVzUwWldkeVlYUmxMbHh5WEc1Y2RGeDBYSFFnS2k5Y2NseHVYSFJjZEZ4MFkyOXVabWxuZFhKbE9pQm1kVzVqZEdsdmJpaGpabWNwSUh0Y2NseHVYSFJjZEZ4MFhIUXZMeUJOWVd0bElITjFjbVVnZEdobElHSmhjMlZWY213Z1pXNWtjeUJwYmlCaElITnNZWE5vTGx4eVhHNWNkRngwWEhSY2RHbG1JQ2hqWm1jdVltRnpaVlZ5YkNrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwYVdZZ0tHTm1aeTVpWVhObFZYSnNMbU5vWVhKQmRDaGpabWN1WW1GelpWVnliQzVzWlc1bmRHZ2dMU0F4S1NBaFBUMGdKeThuS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEdObVp5NWlZWE5sVlhKc0lDczlJQ2N2Snp0Y2NseHVYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwTHk4Z1UyRjJaU0J2Wm1ZZ2RHaGxJSEJoZEdoeklITnBibU5sSUhSb1pYa2djbVZ4ZFdseVpTQnpjR1ZqYVdGc0lIQnliMk5sYzNOcGJtY3NJSFJvWlhrZ1lYSmxJR0ZrWkdsMGFYWmxMbHh5WEc1Y2RGeDBYSFJjZEd4bGRDQnphR2x0SUQwZ1kyOXVabWxuTG5Ob2FXMHNYSEpjYmx4MFhIUmNkRngwWEhSdlltcHpJRDBnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJ3WVhSb2N6b2dkSEoxWlN4Y2NseHVYSFJjZEZ4MFhIUmNkRngwWW5WdVpHeGxjem9nZEhKMVpTeGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFkyOXVabWxuT2lCMGNuVmxMRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUnRZWEE2SUhSeWRXVmNjbHh1WEhSY2RGeDBYSFJjZEgwN1hISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWldGamFGQnliM0FvWTJabkxDQm1kVzVqZEdsdmJpaDJZV3gxWlN3Z2NISnZjQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBhV1lnS0c5aWFuTmJjSEp2Y0YwcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGFXWWdLQ0ZqYjI1bWFXZGJjSEp2Y0YwcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmpiMjVtYVdkYmNISnZjRjBnUFNCN2ZUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRngwWEhSdGFYaHBiaWhqYjI1bWFXZGJjSEp2Y0Ywc0lIWmhiSFZsTENCMGNuVmxMQ0IwY25WbEtUdGNjbHh1WEhSY2RGeDBYSFJjZEgwZ1pXeHpaU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR052Ym1acFoxdHdjbTl3WFNBOUlIWmhiSFZsTzF4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEgwcE8xeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEM4dklGSmxkbVZ5YzJVZ2JXRndJSFJvWlNCaWRXNWtiR1Z6WEhKY2JseDBYSFJjZEZ4MGFXWWdLR05tWnk1aWRXNWtiR1Z6S1NCN1hISmNibHgwWEhSY2RGeDBYSFJsWVdOb1VISnZjQ2hqWm1jdVluVnVaR3hsY3l3Z1puVnVZM1JwYjI0b2RtRnNkV1VzSUhCeWIzQXBJSHRjY2x4dVhIUmNkRngwWEhSY2RGeDBaV0ZqYUNoMllXeDFaU3dnWm5WdVkzUnBiMjRvZGlrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RHbG1JQ2gySUNFOVBTQndjbTl3S1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmlkVzVrYkdWelRXRndXM1pkSUQwZ2NISnZjRHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDBwTzF4eVhHNWNkRngwWEhSY2RGeDBmU2s3WEhKY2JseDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEM4dklFMWxjbWRsSUhOb2FXMWNjbHh1WEhSY2RGeDBYSFJwWmlBb1kyWm5Mbk5vYVcwcElIdGNjbHh1WEhSY2RGeDBYSFJjZEdWaFkyaFFjbTl3S0dObVp5NXphR2x0TENCbWRXNWpkR2x2YmloMllXeDFaU3dnYVdRcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MEx5OGdUbTl5YldGc2FYcGxJSFJvWlNCemRISjFZM1IxY21WY2NseHVYSFJjZEZ4MFhIUmNkRngwYVdZZ0tHbHpRWEp5WVhrb2RtRnNkV1VwS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGRtRnNkV1VnUFNCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmtaWEJ6T2lCMllXeDFaVnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkSDA3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RGeDBhV1lnS0NoMllXeDFaUzVsZUhCdmNuUnpJSHg4SUhaaGJIVmxMbWx1YVhRcElDWW1JQ0YyWVd4MVpTNWxlSEJ2Y25SelJtNHBJSHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFIyWVd4MVpTNWxlSEJ2Y25SelJtNGdQU0JqYjI1MFpYaDBMbTFoYTJWVGFHbHRSWGh3YjNKMGN5aDJZV3gxWlNrN1hISmNibHgwWEhSY2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhIUmNkRngwYzJocGJWdHBaRjBnUFNCMllXeDFaVHRjY2x4dVhIUmNkRngwWEhSY2RIMHBPMXh5WEc1Y2RGeDBYSFJjZEZ4MFkyOXVabWxuTG5Ob2FXMGdQU0J6YUdsdE8xeHlYRzVjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUXZMeUJCWkdwMWMzUWdjR0ZqYTJGblpYTWdhV1lnYm1WalpYTnpZWEo1TGx4eVhHNWNkRngwWEhSY2RHbG1JQ2hqWm1jdWNHRmphMkZuWlhNcElIdGNjbHh1WEhSY2RGeDBYSFJjZEdWaFkyZ29ZMlpuTG5CaFkydGhaMlZ6TENCbWRXNWpkR2x2Ymlod2EyZFBZbW9wSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwYkdWMElHeHZZMkYwYVc5dUxDQnVZVzFsTzF4eVhHNWNkRngwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RGeDBjR3RuVDJKcUlEMGdkSGx3Wlc5bUlIQnJaMDlpYWlBOVBUMGdKM04wY21sdVp5Y2dQeUI3Ym1GdFpUb2djR3RuVDJKcWZTQTZJSEJyWjA5aWFqdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFJjZEc1aGJXVWdQU0J3YTJkUFltb3VibUZ0WlR0Y2NseHVYSFJjZEZ4MFhIUmNkRngwYkc5allYUnBiMjRnUFNCd2EyZFBZbW91Ykc5allYUnBiMjQ3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR2xtSUNoc2IyTmhkR2x2YmlrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RHTnZibVpwWnk1d1lYUm9jMXR1WVcxbFhTQTlJSEJyWjA5aWFpNXNiMk5oZEdsdmJqdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkRngwTHk4Z1UyRjJaU0J3YjJsdWRHVnlJSFJ2SUcxaGFXNGdiVzlrZFd4bElFbEVJR1p2Y2lCd2EyY2dibUZ0WlM0Z1VtVnRiM1psSUd4bFlXUnBibWNnWkc5MElHbHVJRzFoYVc0c0lITnZJRzFoYVc0Z2NHRjBhSE1nWVhKbElGeHlYRzVjZEZ4MFhIUmNkRngwWEhRdkx5QnViM0p0WVd4cGVtVmtMQ0JoYm1RZ2NtVnRiM1psSUdGdWVTQjBjbUZwYkdsdVp5QXVhbk1zSUhOcGJtTmxJR1JwWm1abGNtVnVkQ0J3WVdOcllXZGxJR1Z1ZG5NZ2FHRjJaU0JrYVdabVpYSmxiblFnWEhKY2JseDBYSFJjZEZ4MFhIUmNkQzh2SUdOdmJuWmxiblJwYjI1ek9pQnpiMjFsSUhWelpTQmhJRzF2WkhWc1pTQnVZVzFsTENCemIyMWxJSFZ6WlNCaElHWnBiR1VnYm1GdFpTNWNjbHh1WEhSY2RGeDBYSFJjZEZ4MFkyOXVabWxuTG5CclozTmJibUZ0WlYwZ1BTQndhMmRQWW1vdWJtRnRaU0FySUNjdkp5QXJJQ2h3YTJkUFltb3ViV0ZwYmlCOGZDQW5iV0ZwYmljcFhISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUXVjbVZ3YkdGalpTaGpkWEp5UkdseVVtVm5SWGh3TENBbkp5bGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkQzV5WlhCc1lXTmxLR3B6VTNWbVptbDRVbVZuUlhod0xDQW5KeWs3WEhKY2JseDBYSFJjZEZ4MFhIUjlLVHRjY2x4dVhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwTHk4Z1NXWWdkR2hsY21VZ1lYSmxJR0Z1ZVNCY0luZGhhWFJwYm1jZ2RHOGdaWGhsWTNWMFpWd2lJRzF2WkhWc1pYTWdhVzRnZEdobElISmxaMmx6ZEhKNUxDQjFjR1JoZEdVZ2RHaGxJRzFoY0hNZ1ptOXlJSFJvWlcwc0lITnBibU5sSUhSb1pXbHlJRnh5WEc1Y2RGeDBYSFJjZEM4dklHbHVabThzSUd4cGEyVWdWVkpNY3lCMGJ5QnNiMkZrTENCdFlYa2dhR0YyWlNCamFHRnVaMlZrTGx4eVhHNWNkRngwWEhSY2RHVmhZMmhRY205d0tISmxaMmx6ZEhKNUxDQm1kVzVqZEdsdmJpaHRiMlFzSUdsa0tTQjdYSEpjYmx4MFhIUmNkRngwWEhRdkx5QkpaaUJ0YjJSMWJHVWdZV3h5WldGa2VTQm9ZWE1nYVc1cGRDQmpZV3hzWldRc0lITnBibU5sSUdsMElHbHpJSFJ2YnlCc1lYUmxJSFJ2SUcxdlpHbG1lU0IwYUdWdExDQmhibVFnYVdkdWIzSmxJSFZ1Ym05eWJXRnNhWHBsWkNCY2NseHVYSFJjZEZ4MFhIUmNkQzh2SUc5dVpYTWdjMmx1WTJVZ2RHaGxlU0JoY21VZ2RISmhibk5wWlc1MExseHlYRzVjZEZ4MFhIUmNkRngwYVdZZ0tDRnRiMlF1YVc1cGRHVmtJQ1ltSUNGdGIyUXViV0Z3TG5WdWJtOXliV0ZzYVhwbFpDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUnRiMlF1YldGd0lEMGdiV0ZyWlUxdlpIVnNaVTFoY0NocFpDd2diblZzYkN3Z2RISjFaU2s3WEhKY2JseDBYSFJjZEZ4MFhIUjlYSEpjYmx4MFhIUmNkRngwZlNrN1hISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwTHk4Z1NXWWdZU0JrWlhCeklHRnljbUY1SUc5eUlHRWdZMjl1Wm1sbklHTmhiR3hpWVdOcklHbHpJSE53WldOcFptbGxaQ3dnZEdobGJpQmpZV3hzSUhKbGNYVnBjbVVnZDJsMGFDQjBhRzl6WlNCaGNtZHpMaUJVYUdseklHbHpJSFZ6WldaMWJDQmNjbHh1WEhSY2RGeDBYSFF2THlCM2FHVnVJSEpsY1hWcGNtVWdhWE1nWkdWbWFXNWxaQ0JoY3lCaElHTnZibVpwWnlCdlltcGxZM1FnWW1WbWIzSmxJSEpsY1hWcGNtVXVhbk1nYVhNZ2JHOWhaR1ZrTGx4eVhHNWNkRngwWEhSY2RHbG1JQ2hqWm1jdVpHVndjeUI4ZkNCalptY3VZMkZzYkdKaFkyc3BJSHRjY2x4dVhIUmNkRngwWEhSY2RHTnZiblJsZUhRdWNtVnhkV2x5WlNoalptY3VaR1Z3Y3lCOGZDQmJYU3dnWTJabkxtTmhiR3hpWVdOcktUdGNjbHh1WEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEgwc1hISmNibHgwWEhSY2RGeHlYRzVjZEZ4MFhIUnRZV3RsVTJocGJVVjRjRzl5ZEhNNklHWjFibU4wYVc5dUtIWmhiSFZsS1NCN1hISmNibHgwWEhSY2RGeDBablZ1WTNScGIyNGdabTRvS1NCN1hISmNibHgwWEhSY2RGeDBYSFJzWlhRZ2NtVjBPMXh5WEc1Y2RGeDBYSFJjZEZ4MGFXWWdLSFpoYkhWbExtbHVhWFFwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwY21WMElEMGdkbUZzZFdVdWFXNXBkQzVoY0hCc2VTaG5iRzlpWVd3c0lHRnlaM1Z0Wlc1MGN5azdYSEpjYmx4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSFJ5WlhSMWNtNGdjbVYwSUh4OElDaDJZV3gxWlM1bGVIQnZjblJ6SUNZbUlHZGxkRWRzYjJKaGJDaDJZV3gxWlM1bGVIQnZjblJ6S1NrN1hISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RISmxkSFZ5YmlCbWJqdGNjbHh1WEhSY2RGeDBmU3hjY2x4dVhIUmNkRngwWEhKY2JseDBYSFJjZEcxaGEyVlNaWEYxYVhKbE9pQm1kVzVqZEdsdmJpaHlaV3hOWVhBc0lHOXdkR2x2Ym5NcElIdGNjbHh1WEhSY2RGeDBYSFJ2Y0hScGIyNXpJRDBnYjNCMGFXOXVjeUI4ZkNCN2ZUdGNjbHh1WEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSbWRXNWpkR2x2YmlCc2IyTmhiRkpsY1hWcGNtVW9aR1Z3Y3l3Z1kyRnNiR0poWTJzc0lHVnljbUpoWTJzcElIdGNjbHh1WEhSY2RGeDBYSFJjZEd4bGRDQnBaQ3dnYldGd0xDQnlaWEYxYVhKbFRXOWtPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFJwWmlBb2IzQjBhVzl1Y3k1bGJtRmliR1ZDZFdsc1pFTmhiR3hpWVdOcklDWW1JR05oYkd4aVlXTnJJQ1ltSUdselJuVnVZM1JwYjI0b1kyRnNiR0poWTJzcEtTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RHTmhiR3hpWVdOckxsOWZjbVZ4ZFdseVpVcHpRblZwYkdRZ1BTQjBjblZsTzF4eVhHNWNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFJwWmlBb2RIbHdaVzltSUdSbGNITWdQVDA5SUNkemRISnBibWNuS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEdsbUlDaHBjMFoxYm1OMGFXOXVLR05oYkd4aVlXTnJLU2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEM4dklFbHVkbUZzYVdRZ1kyRnNiRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkSEpsZEhWeWJpQnZia1Z5Y205eUtHMWhhMlZGY25KdmNpZ25jbVZ4ZFdseVpXRnlaM01uTENBblNXNTJZV3hwWkNCeVpYRjFhWEpsSUdOaGJHd25LU3dnWlhKeVltRmpheWs3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RDOHZJRWxtSUhKbGNYVnBjbVY4Wlhod2IzSjBjM3h0YjJSMWJHVWdZWEpsSUhKbGNYVmxjM1JsWkN3Z1oyVjBJSFJvWlNCMllXeDFaU0JtYjNJZ2RHaGxiU0JtY205dElIUm9aU0J6Y0dWamFXRnNJR2hoYm1Sc1pYSnpMaUJjY2x4dVhIUmNkRngwWEhSY2RGeDBMeThnUTJGMlpXRjBPaUIwYUdseklHOXViSGtnZDI5eWEzTWdkMmhwYkdVZ2JXOWtkV3hsSUdseklHSmxhVzVuSUdSbFptbHVaV1F1WEhKY2JseDBYSFJjZEZ4MFhIUmNkR2xtSUNoeVpXeE5ZWEFnSmlZZ2FHRnpVSEp2Y0Nob1lXNWtiR1Z5Y3l3Z1pHVndjeWtwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWEhSeVpYUjFjbTRnYUdGdVpHeGxjbk5iWkdWd2MxMG9jbVZuYVhOMGNubGJjbVZzVFdGd0xtbGtYU2s3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RDOHZJRk41Ym1Ob2NtOXViM1Z6SUdGalkyVnpjeUIwYnlCdmJtVWdiVzlrZFd4bExpQkpaaUJ5WlhGMWFYSmxMbWRsZENCcGN5QmhkbUZwYkdGaWJHVWdLR0Z6SUdsdUlIUm9aU0JPYjJSbElHRmtZWEIwWlhJcExDQmNjbHh1WEhSY2RGeDBYSFJjZEZ4MEx5OGdjSEpsWm1WeUlIUm9ZWFF1WEhKY2JseDBYSFJjZEZ4MFhIUmNkR2xtSUNoeVpYRXVaMlYwS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGNtVjBkWEp1SUhKbGNTNW5aWFFvWTI5dWRHVjRkQ3dnWkdWd2N5d2djbVZzVFdGd0xDQnNiMk5oYkZKbGNYVnBjbVVwTzF4eVhHNWNkRngwWEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUXZMeUJPYjNKdFlXeHBlbVVnYlc5a2RXeGxJRzVoYldVc0lHbG1JR2wwSUdOdmJuUmhhVzV6SUM0Z2IzSWdMaTVjY2x4dVhIUmNkRngwWEhSY2RGeDBiV0Z3SUQwZ2JXRnJaVTF2WkhWc1pVMWhjQ2hrWlhCekxDQnlaV3hOWVhBc0lHWmhiSE5sTENCMGNuVmxLVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBhV1FnUFNCdFlYQXVhV1E3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUnBaaUFvSVdoaGMxQnliM0FvWkdWbWFXNWxaQ3dnYVdRcEtTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBjbVYwZFhKdUlHOXVSWEp5YjNJb2JXRnJaVVZ5Y205eUtDZHViM1JzYjJGa1pXUW5MQ0FuVFc5a2RXeGxJRzVoYldVZ1hDSW5JQ3RjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJjZEdsa0lDdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUmNkQ2RjSWlCb1lYTWdibTkwSUdKbFpXNGdiRzloWkdWa0lIbGxkQ0JtYjNJZ1kyOXVkR1Y0ZERvZ0p5QXJYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSFJqYjI1MFpYaDBUbUZ0WlNBclhISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUW9jbVZzVFdGd0lEOGdKeWNnT2lBbkxpQlZjMlVnY21WeGRXbHlaU2hiWFNrbktTa3BPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUjlYSEpjYmx4MFhIUmNkRngwWEhSY2RISmxkSFZ5YmlCa1pXWnBibVZrVzJsa1hUdGNjbHh1WEhSY2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MEx5OGdSM0poWWlCa1pXWnBibVZ6SUhkaGFYUnBibWNnYVc0Z2RHaGxJR2RzYjJKaGJDQnhkV1YxWlM1Y2NseHVYSFJjZEZ4MFhIUmNkR2x1ZEdGclpVUmxabWx1WlhNb0tUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBMeThnVFdGeWF5QmhiR3dnZEdobElHUmxjR1Z1WkdWdVkybGxjeUJoY3lCdVpXVmthVzVuSUhSdklHSmxJR3h2WVdSbFpDNWNjbHh1WEhSY2RGeDBYSFJjZEdOdmJuUmxlSFF1Ym1WNGRGUnBZMnNvWm5WdVkzUnBiMjRvS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEM4dklGTnZiV1VnWkdWbWFXNWxjeUJqYjNWc1pDQm9ZWFpsSUdKbFpXNGdZV1JrWldRZ2MybHVZMlVnZEdobFhISmNibHgwWEhSY2RGeDBYSFJjZEM4dklISmxjWFZwY21VZ1kyRnNiQ3dnWTI5c2JHVmpkQ0IwYUdWdExseHlYRzVjZEZ4MFhIUmNkRngwWEhScGJuUmhhMlZFWldacGJtVnpLQ2s3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUnlaWEYxYVhKbFRXOWtJRDBnWjJWMFRXOWtkV3hsS0cxaGEyVk5iMlIxYkdWTllYQW9iblZzYkN3Z2NtVnNUV0Z3S1NrN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBYSFF2THlCVGRHOXlaU0JwWmlCdFlYQWdZMjl1Wm1sbklITm9iM1ZzWkNCaVpTQmhjSEJzYVdWa0lIUnZJSFJvYVhNZ2NtVnhkV2x5WlZ4eVhHNWNkRngwWEhSY2RGeDBYSFF2THlCallXeHNJR1p2Y2lCa1pYQmxibVJsYm1OcFpYTXVYSEpjYmx4MFhIUmNkRngwWEhSY2RISmxjWFZwY21WTmIyUXVjMnRwY0UxaGNDQTlJRzl3ZEdsdmJuTXVjMnRwY0UxaGNEdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFJjZEhKbGNYVnBjbVZOYjJRdWFXNXBkQ2hrWlhCekxDQmpZV3hzWW1GamF5d2daWEp5WW1GamF5d2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkR1Z1WVdKc1pXUTZJSFJ5ZFdWY2NseHVYSFJjZEZ4MFhIUmNkRngwZlNrN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBYSFJqYUdWamEweHZZV1JsWkNncE8xeHlYRzVjZEZ4MFhIUmNkRngwZlNrN1hISmNibHgwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RISmxkSFZ5YmlCc2IyTmhiRkpsY1hWcGNtVTdYSEpjYmx4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkRzFwZUdsdUtHeHZZMkZzVW1WeGRXbHlaU3dnZTF4eVhHNWNkRngwWEhSY2RGeDBhWE5DY205M2MyVnlPaUJwYzBKeWIzZHpaWElzWEhKY2JseDBYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFJjZEM4cUtseHlYRzVjZEZ4MFhIUmNkRngwSUNvZ1EyOXVkbVZ5ZEhNZ1lTQnRiMlIxYkdVZ2JtRnRaU0FySUM1bGVIUmxibk5wYjI0Z2FXNTBieUJoYmlCVlVrd2djR0YwYUM1Y2NseHVYSFJjZEZ4MFhIUmNkQ0FxSUZ4eVhHNWNkRngwWEhSY2RGeDBJQ29nS2xKbGNYVnBjbVZ6S2lCMGFHVWdkWE5sSUc5bUlHRWdiVzlrZFd4bElHNWhiV1V1SUVsMElHUnZaWE1nYm05MElITjFjSEJ2Y25RZ2RYTnBibWNnY0d4aGFXNGdWVkpNY3lCc2FXdGxJRzVoYldWVWIxVnliQzVjY2x4dVhIUmNkRngwWEhSY2RDQXFMMXh5WEc1Y2RGeDBYSFJjZEZ4MGRHOVZjbXc2SUdaMWJtTjBhVzl1S0cxdlpIVnNaVTVoYldWUWJIVnpSWGgwS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEd4bGRDQmxlSFFzWEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwYVc1a1pYZ2dQU0J0YjJSMWJHVk9ZVzFsVUd4MWMwVjRkQzVzWVhOMFNXNWtaWGhQWmlnbkxpY3BMRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkSE5sWjIxbGJuUWdQU0J0YjJSMWJHVk9ZVzFsVUd4MWMwVjRkQzV6Y0d4cGRDZ25MeWNwV3pCZExGeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2RHbHpVbVZzWVhScGRtVWdQU0J6WldkdFpXNTBJRDA5UFNBbkxpY2dmSHdnYzJWbmJXVnVkQ0E5UFQwZ0p5NHVKenRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RDOHZJRWhoZG1VZ1lTQm1hV3hsSUdWNGRHVnVjMmx2YmlCaGJHbGhjeXdnWVc1a0lHbDBJR2x6SUc1dmRDQjBhR1ZjY2x4dVhIUmNkRngwWEhSY2RGeDBMeThnWkc5MGN5Qm1jbTl0SUdFZ2NtVnNZWFJwZG1VZ2NHRjBhQzVjY2x4dVhIUmNkRngwWEhSY2RGeDBhV1lnS0dsdVpHVjRJQ0U5UFNBdE1TQW1KaUFvSVdselVtVnNZWFJwZG1VZ2ZId2dhVzVrWlhnZ1BpQXhLU2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEdWNGRDQTlJRzF2WkhWc1pVNWhiV1ZRYkhWelJYaDBMbk4xWW5OMGNtbHVaeWhwYm1SbGVDd2diVzlrZFd4bFRtRnRaVkJzZFhORmVIUXViR1Z1WjNSb0tUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUnRiMlIxYkdWT1lXMWxVR3gxYzBWNGRDQTlJRzF2WkhWc1pVNWhiV1ZRYkhWelJYaDBMbk4xWW5OMGNtbHVaeWd3TENCcGJtUmxlQ2s3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RISmxkSFZ5YmlCamIyNTBaWGgwTG01aGJXVlViMVZ5YkNodWIzSnRZV3hwZW1Vb2JXOWtkV3hsVG1GdFpWQnNkWE5GZUhRc1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGNtVnNUV0Z3SUNZbUlISmxiRTFoY0M1cFpDd2dkSEoxWlNrc0lHVjRkQ3dnZEhKMVpTazdYSEpjYmx4MFhIUmNkRngwWEhSOUxGeHlYRzVjZEZ4MFhIUmNkRngwWEhKY2JseDBYSFJjZEZ4MFhIUmtaV1pwYm1Wa09pQm1kVzVqZEdsdmJpaHBaQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJ5WlhSMWNtNGdhR0Z6VUhKdmNDaGtaV1pwYm1Wa0xDQnRZV3RsVFc5a2RXeGxUV0Z3S0dsa0xDQnlaV3hOWVhBc0lHWmhiSE5sTENCMGNuVmxLUzVwWkNrN1hISmNibHgwWEhSY2RGeDBYSFI5TEZ4eVhHNWNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSemNHVmphV1pwWldRNklHWjFibU4wYVc5dUtHbGtLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkR2xrSUQwZ2JXRnJaVTF2WkhWc1pVMWhjQ2hwWkN3Z2NtVnNUV0Z3TENCbVlXeHpaU3dnZEhKMVpTa3VhV1E3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSEpsZEhWeWJpQm9ZWE5RY205d0tHUmxabWx1WldRc0lHbGtLU0I4ZkNCb1lYTlFjbTl3S0hKbFoybHpkSEo1TENCcFpDazdYSEpjYmx4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBmU2s3WEhKY2JseDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBMeThnVDI1c2VTQmhiR3h2ZHlCMWJtUmxaaUJ2YmlCMGIzQWdiR1YyWld3Z2NtVnhkV2x5WlNCallXeHNjeTVjY2x4dVhIUmNkRngwWEhScFppQW9JWEpsYkUxaGNDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGJHOWpZV3hTWlhGMWFYSmxMblZ1WkdWbUlEMGdablZ1WTNScGIyNG9hV1FwSUh0Y2NseHVYSFJjZEZ4MFhIUmNkRngwTHk4Z1FtbHVaQ0JoYm5rZ2QyRnBkR2x1WnlCa1pXWnBibVVvS1NCallXeHNjeUIwYnlCMGFHbHpJR052Ym5SbGVIUXNJR1pwZUNCbWIzSWdJelF3T0M1Y2NseHVYSFJjZEZ4MFhIUmNkRngwZEdGclpVZHNiMkpoYkZGMVpYVmxLQ2s3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUnNaWFFnYldGd0lEMGdiV0ZyWlUxdlpIVnNaVTFoY0NocFpDd2djbVZzVFdGd0xDQjBjblZsS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRngwYkdWMElHMXZaQ0E5SUdkbGRFOTNiaWh5WldkcGMzUnllU3dnYVdRcE8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUmNkRngwYlc5a0xuVnVaR1ZtWldRZ1BTQjBjblZsTzF4eVhHNWNkRngwWEhSY2RGeDBYSFJ5WlcxdmRtVlRZM0pwY0hRb2FXUXBPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFJjZEZ4MFpHVnNaWFJsSUdSbFptbHVaV1JiYVdSZE8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSa1pXeGxkR1VnZFhKc1JtVjBZMmhsWkZ0dFlYQXVkWEpzWFR0Y2NseHVYSFJjZEZ4MFhIUmNkRngwWkdWc1pYUmxJSFZ1WkdWbVJYWmxiblJ6VzJsa1hUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhISmNibHgwWEhSY2RGeDBYSFJjZEM4dklFTnNaV0Z1SUhGMVpYVmxaQ0JrWldacGJtVnpJSFJ2Ynk0Z1IyOGdZbUZqYTNkaGNtUnpJR2x1SUdGeWNtRjVJSE52SUhSb1lYUWdkR2hsSUhOd2JHbGpaWE1nWkc4Z2JtOTBJRzFsYzNNZ2RYQWdkR2hsSUZ4eVhHNWNkRngwWEhSY2RGeDBYSFF2THlCcGRHVnlZWFJwYjI0dVhISmNibHgwWEhSY2RGeDBYSFJjZEdWaFkyaFNaWFpsY25ObEtHUmxabEYxWlhWbExDQm1kVzVqZEdsdmJpaGhjbWR6TENCcEtTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBhV1lnS0dGeVozTmJNRjBnUFQwOUlHbGtLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhSa1pXWlJkV1YxWlM1emNHeHBZMlVvYVN3Z01TazdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUjlLVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBaR1ZzWlhSbElHTnZiblJsZUhRdVpHVm1VWFZsZFdWTllYQmJhV1JkTzF4eVhHNWNkRngwWEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSY2RGeDBhV1lnS0cxdlpDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkQzh2SUVodmJHUWdiMjRnZEc4Z2JHbHpkR1Z1WlhKeklHbHVJR05oYzJVZ2RHaGxJRzF2WkhWc1pTQjNhV3hzSUdKbElHRjBkR1Z0Y0hSbFpDQjBieUJpWlNCeVpXeHZZV1JsWkNCMWMybHVaeUJoSUZ4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEM4dklHUnBabVpsY21WdWRDQmpiMjVtYVdjdVhISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGFXWWdLRzF2WkM1bGRtVnVkSE11WkdWbWFXNWxaQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MGRXNWtaV1pGZG1WdWRITmJhV1JkSUQwZ2JXOWtMbVYyWlc1MGN6dGNjbHh1WEhSY2RGeDBYSFJjZEZ4MFhIUjlYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBZMnhsWVc1U1pXZHBjM1J5ZVNocFpDazdYSEpjYmx4MFhIUmNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBYSFJjZEgwN1hISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RISmxkSFZ5YmlCc2IyTmhiRkpsY1hWcGNtVTdYSEpjYmx4MFhIUmNkSDBzWEhKY2JseDBYSFJjZEZ4eVhHNWNkRngwWEhRdktpcGNjbHh1WEhSY2RGeDBJQ29nUTJGc2JHVmtJSFJ2SUdWdVlXSnNaU0JoSUcxdlpIVnNaU0JwWmlCcGRDQnBjeUJ6ZEdsc2JDQnBiaUIwYUdVZ2NtVm5hWE4wY25rZ1lYZGhhWFJwYm1jZ1pXNWhZbXhsYldWdWRDNGdRU0J6WldOdmJtUWdZWEpuTENCd1lYSmxiblFzSUhSb1pTQmNjbHh1WEhSY2RGeDBJQ29nY0dGeVpXNTBJRzF2WkhWc1pTd2dhWE1nY0dGemMyVmtJR2x1SUdadmNpQmpiMjUwWlhoMExDQjNhR1Z1SUhSb2FYTWdiV1YwYUc5a0lHbHpJRzkyWlhKeWFXUmtaVzRnWW5rZ2RHaGxJRzl3ZEdsdGFYcGxjaTRnVG05MElITm9iM2R1SUdobGNtVWdYSEpjYmx4MFhIUmNkQ0FxSUhSdklHdGxaWEFnWTI5a1pTQmpiMjF3WVdOMExseHlYRzVjZEZ4MFhIUWdLaTljY2x4dVhIUmNkRngwWlc1aFlteGxPaUJtZFc1amRHbHZiaWhrWlhCTllYQXBJSHRjY2x4dVhIUmNkRngwWEhSc1pYUWdiVzlrSUQwZ1oyVjBUM2R1S0hKbFoybHpkSEo1TENCa1pYQk5ZWEF1YVdRcE8xeHlYRzVjZEZ4MFhIUmNkR2xtSUNodGIyUXBJSHRjY2x4dVhIUmNkRngwWEhSY2RHZGxkRTF2WkhWc1pTaGtaWEJOWVhBcExtVnVZV0pzWlNncE8xeHlYRzVjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwZlN4Y2NseHVYSFJjZEZ4MFhISmNibHgwWEhSY2RDOHFLbHh5WEc1Y2RGeDBYSFFnS2lCSmJuUmxjbTVoYkNCdFpYUm9iMlFnZFhObFpDQmllU0JsYm5acGNtOXViV1Z1ZENCaFpHRndkR1Z5Y3lCMGJ5QmpiMjF3YkdWMFpTQmhJR3h2WVdRZ1pYWmxiblF1WEhKY2JseDBYSFJjZENBcUlGeHlYRzVjZEZ4MFhIUWdLaUJCSUd4dllXUWdaWFpsYm5RZ1kyOTFiR1FnWW1VZ1lTQnpZM0pwY0hRZ2JHOWhaQ0J2Y2lCcWRYTjBJR0VnYkc5aFpDQndZWE56SUdaeWIyMGdZU0J6ZVc1amFISnZibTkxY3lCc2IyRmtJR05oYkd3dUlGeHlYRzVjZEZ4MFhIUWdLaUJjY2x4dVhIUmNkRngwSUNvZ1FIQmhjbUZ0SUh0VGRISnBibWQ5SUcxdlpIVnNaVTVoYldVZ1ZHaGxJRzVoYldVZ2IyWWdkR2hsSUcxdlpIVnNaU0IwYnlCd2IzUmxiblJwWVd4c2VTQmpiMjF3YkdWMFpTNWNjbHh1WEhSY2RGeDBJQ292WEhKY2JseDBYSFJjZEdOdmJYQnNaWFJsVEc5aFpEb2dablZ1WTNScGIyNG9iVzlrZFd4bFRtRnRaU2tnZTF4eVhHNWNkRngwWEhSY2RHeGxkQ0JtYjNWdVpEc2dYSEpjYmx4MFhIUmNkRngwYkdWMElHRnlaM003SUZ4eVhHNWNkRngwWEhSY2RHeGxkQ0J0YjJRN1hISmNibHgwWEhSY2RGeDBiR1YwSUhOb2FXMGdQU0JuWlhSUGQyNG9ZMjl1Wm1sbkxuTm9hVzBzSUcxdlpIVnNaVTVoYldVcElIeDhJSHQ5TzF4eVhHNWNkRngwWEhSY2RHeGxkQ0J6YUVWNGNHOXlkSE1nUFNCemFHbHRMbVY0Y0c5eWRITTdYSEpjYmx4MFhIUmNkRngwWEhKY2JseDBYSFJjZEZ4MGRHRnJaVWRzYjJKaGJGRjFaWFZsS0NrN1hISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwZDJocGJHVWdLR1JsWmxGMVpYVmxMbXhsYm1kMGFDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFlYSm5jeUE5SUdSbFpsRjFaWFZsTG5Ob2FXWjBLQ2s3WEhKY2JseDBYSFJjZEZ4MFhIUnBaaUFvWVhKbmMxc3dYU0E5UFQwZ2JuVnNiQ2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFJoY21keld6QmRJRDBnYlc5a2RXeGxUbUZ0WlR0Y2NseHVYSFJjZEZ4MFhIUmNkRngwTHk4Z1NXWWdZV3h5WldGa2VTQm1iM1Z1WkNCaGJpQmhibTl1ZVcxdmRYTWdiVzlrZFd4bElHRnVaQ0JpYjNWdVpDQnBkQ0IwYnlCMGFHbHpJRzVoYldVc0lIUm9aVzRnZEdocGN5QnBjeUJ6YjIxbElHOTBhR1Z5SUdGdWIyNGdYSEpjYmx4MFhIUmNkRngwWEhSY2RDOHZJRzF2WkhWc1pTQjNZV2wwYVc1bklHWnZjaUJwZEhNZ1kyOXRjR3hsZEdWTWIyRmtJSFJ2SUdacGNtVXVYSEpjYmx4MFhIUmNkRngwWEhSY2RHbG1JQ2htYjNWdVpDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkR0p5WldGck8xeHlYRzVjZEZ4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBYSFJjZEdadmRXNWtJRDBnZEhKMVpUdGNjbHh1WEhSY2RGeDBYSFJjZEgwZ1pXeHpaU0JwWmlBb1lYSm5jMXN3WFNBOVBUMGdiVzlrZFd4bFRtRnRaU2tnZTF4eVhHNWNkRngwWEhSY2RGeDBYSFF2THlCR2IzVnVaQ0J0WVhSamFHbHVaeUJrWldacGJtVWdZMkZzYkNCbWIzSWdkR2hwY3lCelkzSnBjSFFoWEhKY2JseDBYSFJjZEZ4MFhIUmNkR1p2ZFc1a0lEMGdkSEoxWlR0Y2NseHVYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkRngwWTJGc2JFZGxkRTF2WkhWc1pTaGhjbWR6S1R0Y2NseHVYSFJjZEZ4MFhIUjlYSEpjYmx4MFhIUmNkRngwWTI5dWRHVjRkQzVrWldaUmRXVjFaVTFoY0NBOUlIdDlPMXh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RDOHZJRVJ2SUhSb2FYTWdZV1owWlhJZ2RHaGxJR041WTJ4bElHOW1JR05oYkd4SFpYUk5iMlIxYkdVZ2FXNGdZMkZ6WlNCMGFHVWdjbVZ6ZFd4MFhISmNibHgwWEhSY2RGeDBMeThnYjJZZ2RHaHZjMlVnWTJGc2JITXZhVzVwZENCallXeHNjeUJqYUdGdVoyVnpJSFJvWlNCeVpXZHBjM1J5ZVM1Y2NseHVYSFJjZEZ4MFhIUnRiMlFnUFNCblpYUlBkMjRvY21WbmFYTjBjbmtzSUcxdlpIVnNaVTVoYldVcE8xeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEdsbUlDZ2habTkxYm1RZ0ppWWdJV2hoYzFCeWIzQW9aR1ZtYVc1bFpDd2diVzlrZFd4bFRtRnRaU2tnSmlZZ2JXOWtJQ1ltSUNGdGIyUXVhVzVwZEdWa0tTQjdYSEpjYmx4MFhIUmNkRngwWEhScFppQW9ZMjl1Wm1sbkxtVnVabTl5WTJWRVpXWnBibVVnSmlZZ0tDRnphRVY0Y0c5eWRITWdmSHdnSVdkbGRFZHNiMkpoYkNoemFFVjRjRzl5ZEhNcEtTa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUnBaaUFvYUdGelVHRjBhRVpoYkd4aVlXTnJLRzF2WkhWc1pVNWhiV1VwS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MGNtVjBkWEp1TzF4eVhHNWNkRngwWEhSY2RGeDBYSFI5SUdWc2MyVWdlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkSEpsZEhWeWJpQnZia1Z5Y205eUtHMWhhMlZGY25KdmNpZ25ibTlrWldacGJtVW5MRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwSjA1dklHUmxabWx1WlNCallXeHNJR1p2Y2lBbklDc2diVzlrZFd4bFRtRnRaU3hjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFJjZEc1MWJHd3NYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSFJiYlc5a2RXeGxUbUZ0WlYwcEtUdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRngwZlNCbGJITmxJSHRjY2x4dVhIUmNkRngwWEhSY2RGeDBMeThnUVNCelkzSnBjSFFnZEdoaGRDQmtiMlZ6SUc1dmRDQmpZV3hzSUdSbFptbHVaU2dwTENCemJ5QnFkWE4wSUhOcGJYVnNZWFJsSUhSb1pTQmpZV3hzSUdadmNpQnBkQzVjY2x4dVhIUmNkRngwWEhSY2RGeDBZMkZzYkVkbGRFMXZaSFZzWlNoYmJXOWtkV3hsVG1GdFpTd2dLSE5vYVcwdVpHVndjeUI4ZkNCYlhTa3NJSE5vYVcwdVpYaHdiM0owYzBadVhTazdYSEpjYmx4MFhIUmNkRngwWEhSOVhISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RHTm9aV05yVEc5aFpHVmtLQ2s3WEhKY2JseDBYSFJjZEgwc1hISmNibHgwWEhSY2RGeHlYRzVjZEZ4MFhIUXZLaXBjY2x4dVhIUmNkRngwSUNvZ1EyOXVkbVZ5ZEhNZ1lTQnRiMlIxYkdVZ2JtRnRaU0IwYnlCaElHWnBiR1VnY0dGMGFDNGdYSEpjYmx4MFhIUmNkQ0FxSUZ4eVhHNWNkRngwWEhRZ0tpQlRkWEJ3YjNKMGN5QmpZWE5sY3lCM2FHVnlaU0J0YjJSMWJHVk9ZVzFsSUcxaGVTQmhZM1IxWVd4c2VTQmlaU0JxZFhOMElHRnVJRlZTVEM0Z1hISmNibHgwWEhSY2RDQXFJRnh5WEc1Y2RGeDBYSFFnS2lCT2IzUmxJSFJvWVhRZ2FYUWdLaXBrYjJWeklHNXZkQ29xSUdOaGJHd2dibTl5YldGc2FYcGxJRzl1SUhSb1pTQnRiMlIxYkdWT1lXMWxMQ0JwZENCcGN5QmhjM04xYldWa0lIUnZJR2hoZG1VZ1lXeHlaV0ZrZVNCaVpXVnVJRnh5WEc1Y2RGeDBYSFFnS2lCdWIzSnRZV3hwZW1Wa0xpQlVhR2x6SUdseklHRnVJR2x1ZEdWeWJtRnNJRUZRU1N3Z2JtOTBJR0VnY0hWaWJHbGpJRzl1WlM0Z1ZYTmxJSFJ2VlhKc0lHWnZjaUIwYUdVZ2NIVmliR2xqSUVGUVNTNWNjbHh1WEhSY2RGeDBJQ292WEhKY2JseDBYSFJjZEc1aGJXVlViMVZ5YkRvZ1puVnVZM1JwYjI0b2JXOWtkV3hsVG1GdFpTd2daWGgwTENCemEybHdSWGgwS1NCN1hISmNibHgwWEhSY2RGeDBiR1YwSUhCaGRHaHpPeUJjY2x4dVhIUmNkRngwWEhSc1pYUWdjM2x0Y3pzZ1hISmNibHgwWEhSY2RGeDBiR1YwSUdrN0lGeHlYRzVjZEZ4MFhIUmNkR3hsZENCd1lYSmxiblJOYjJSMWJHVTdJRnh5WEc1Y2RGeDBYSFJjZEd4bGRDQjFjbXc3WEhKY2JseDBYSFJjZEZ4MGJHVjBJSEJoY21WdWRGQmhkR2dzSUdKMWJtUnNaVWxrTzF4eVhHNWNkRngwWEhSY2RHeGxkQ0J3YTJkTllXbHVJRDBnWjJWMFQzZHVLR052Ym1acFp5NXdhMmR6TENCdGIyUjFiR1ZPWVcxbEtUdGNjbHh1WEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhScFppQW9jR3RuVFdGcGJpa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGJXOWtkV3hsVG1GdFpTQTlJSEJyWjAxaGFXNDdYSEpjYmx4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RGeHlYRzVjZEZ4MFhIUmNkR0oxYm1Sc1pVbGtJRDBnWjJWMFQzZHVLR0oxYm1Sc1pYTk5ZWEFzSUcxdlpIVnNaVTVoYldVcE8xeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEdsbUlDaGlkVzVrYkdWSlpDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MGNtVjBkWEp1SUdOdmJuUmxlSFF1Ym1GdFpWUnZWWEpzS0dKMWJtUnNaVWxrTENCbGVIUXNJSE5yYVhCRmVIUXBPMXh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhIUmNjbHh1WEhSY2RGeDBYSFF2THlCSlppQmhJR052Ykc5dUlHbHpJR2x1SUhSb1pTQlZVa3dzSUdsMElHbHVaR2xqWVhSbGN5QmhJSEJ5YjNSdlkyOXNJR2x6SUhWelpXUWdZVzVrSUdsMElHbHpJR3AxYzNRZ1lXNGdWVkpNSUhSdklHRWdabWxzWlN3Z2IzSWdhV1lnYVhRZ1hISmNibHgwWEhSY2RGeDBMeThnYzNSaGNuUnpJSGRwZEdnZ1lTQnpiR0Z6YUN3Z1kyOXVkR0ZwYm5NZ1lTQnhkV1Z5ZVNCaGNtY2dLR2t1WlM0Z1B5a2diM0lnWlc1a2N5QjNhWFJvSUM1cWN5d2dkR2hsYmlCaGMzTjFiV1VnZEdobElIVnpaWElnYldWaGJuUWdkRzhnWEhKY2JseDBYSFJjZEZ4MEx5OGdkWE5sSUdGdUlIVnliQ0JoYm1RZ2JtOTBJR0VnYlc5a2RXeGxJR2xrTGlCVWFHVWdjMnhoYzJnZ2FYTWdhVzF3YjNKMFlXNTBJR1p2Y2lCd2NtOTBiMk52YkMxc1pYTnpJRlZTVEhNZ1lYTWdkMlZzYkNCaGN5Qm1kV3hzSUhCaGRHaHpMbHh5WEc1Y2RGeDBYSFJjZEdsbUlDaHlaWEV1YW5ORmVIUlNaV2RGZUhBdWRHVnpkQ2h0YjJSMWJHVk9ZVzFsS1NrZ2UxeHlYRzVjZEZ4MFhIUmNkRngwTHk4Z1NuVnpkQ0JoSUhCc1lXbHVJSEJoZEdnc0lHNXZkQ0J0YjJSMWJHVWdibUZ0WlNCc2IyOXJkWEFzSUhOdklHcDFjM1FnY21WMGRYSnVJR2wwTGlCQlpHUWdaWGgwWlc1emFXOXVJR2xtSUdsMElHbHpJR2x1WTJ4MVpHVmtMaUJjY2x4dVhIUmNkRngwWEhSY2RDOHZJRlJvYVhNZ2FYTWdZU0JpYVhRZ2QyOXVhM2tzSUc5dWJIa2dibTl1TFM1cWN5QjBhR2x1WjNNZ2NHRnpjeUJoYmlCbGVIUmxibk5wYjI0c0lIUm9hWE1nYldWMGFHOWtJSEJ5YjJKaFlteDVJRzVsWldSeklIUnZJR0psSUZ4eVhHNWNkRngwWEhSY2RGeDBMeThnY21WM2IzSnJaV1F1WEhKY2JseDBYSFJjZEZ4MFhIUjFjbXdnUFNCdGIyUjFiR1ZPWVcxbElDc2dLR1Y0ZENCOGZDQW5KeWs3WEhKY2JseDBYSFJjZEZ4MGZTQmxiSE5sSUh0Y2NseHVYSFJjZEZ4MFhIUmNkQzh2SUVFZ2JXOWtkV3hsSUhSb1lYUWdibVZsWkhNZ2RHOGdZbVVnWTI5dWRtVnlkR1ZrSUhSdklHRWdjR0YwYUM1Y2NseHVYSFJjZEZ4MFhIUmNkSEJoZEdoeklEMGdZMjl1Wm1sbkxuQmhkR2h6TzF4eVhHNWNkRngwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwWEhSemVXMXpJRDBnYlc5a2RXeGxUbUZ0WlM1emNHeHBkQ2duTHljcE8xeHlYRzVjZEZ4MFhIUmNkRngwTHk4Z1JtOXlJR1ZoWTJnZ2JXOWtkV3hsSUc1aGJXVWdjMlZuYldWdWRDd2djMlZsSUdsbUlIUm9aWEpsSUdseklHRWdjR0YwYUNCeVpXZHBjM1JsY21Wa0lHWnZjaUJwZEM0Z1UzUmhjblFnZDJsMGFDQnRiM04wSUhOd1pXTnBabWxqSUZ4eVhHNWNkRngwWEhSY2RGeDBMeThnYm1GdFpTQmhibVFnZDI5eWF5QjFjQ0JtY205dElHbDBMbHh5WEc1Y2RGeDBYSFJjZEZ4MFptOXlJQ2hwSUQwZ2MzbHRjeTVzWlc1bmRHZzdJR2tnUGlBd095QnBJQzA5SURFcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGNHRnlaVzUwVFc5a2RXeGxJRDBnYzNsdGN5NXpiR2xqWlNnd0xDQnBLUzVxYjJsdUtDY3ZKeWs3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MFhIUndZWEpsYm5SUVlYUm9JRDBnWjJWMFQzZHVLSEJoZEdoekxDQndZWEpsYm5STmIyUjFiR1VwTzF4eVhHNWNkRngwWEhSY2RGeDBYSFJwWmlBb2NHRnlaVzUwVUdGMGFDa2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkQzh2SUVsbUlHRnVJR0Z5Y21GNUxDQnBkQ0J0WldGdWN5QjBhR1Z5WlNCaGNtVWdZU0JtWlhjZ1kyaHZhV05sY3l3Z1EyaHZiM05sSUhSb1pTQnZibVVnZEdoaGRDQnBjeUJrWlhOcGNtVmtMbHh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkR2xtSUNocGMwRnljbUY1S0hCaGNtVnVkRkJoZEdncEtTQjdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBYSFJ3WVhKbGJuUlFZWFJvSUQwZ2NHRnlaVzUwVUdGMGFGc3dYVHRjY2x4dVhIUmNkRngwWEhSY2RGeDBYSFI5WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwYzNsdGN5NXpjR3hwWTJVb01Dd2dhU3dnY0dGeVpXNTBVR0YwYUNrN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFluSmxZV3M3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RGeDBMeThnU205cGJpQjBhR1VnY0dGMGFDQndZWEowY3lCMGIyZGxkR2hsY2l3Z2RHaGxiaUJtYVdkMWNtVWdiM1YwSUdsbUlHSmhjMlZWY213Z2FYTWdibVZsWkdWa0xseHlYRzVjZEZ4MFhIUmNkRngwZFhKc0lEMGdjM2x0Y3k1cWIybHVLQ2N2SnlrN1hISmNibHgwWEhSY2RGeDBYSFIxY213Z0t6MGdLR1Y0ZENCOGZDQW9MMTVrWVhSaFhGdzZmRnhjUHk4dWRHVnpkQ2gxY213cElIeDhJSE5yYVhCRmVIUWdQeUFuSnlBNklDY3Vhbk1uS1NrN1hISmNibHgwWEhSY2RGeDBYSFIxY213Z1BTQW9kWEpzTG1Ob1lYSkJkQ2d3S1NBOVBUMGdKeThuSUh4OElIVnliQzV0WVhSamFDZ3ZYbHRjWEhkY1hDdGNYQzVjWEMxZEt6b3ZLU0EvSUNjbklEb2dZMjl1Wm1sbkxtSmhjMlZWY213cElDc2dkWEpzTzF4eVhHNWNkRngwWEhSY2RIMWNjbHh1WEhSY2RGeDBYSFJjY2x4dVhIUmNkRngwWEhSeVpYUjFjbTRnWTI5dVptbG5MblZ5YkVGeVozTWdQeUIxY213Z0sxeHlYRzVjZEZ4MFhIUmNkQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBb0tIVnliQzVwYm1SbGVFOW1LQ2MvSnlrZ1BUMDlJQzB4SUQ4Z0p6OG5JRG9nSnlZbktTQXJYSEpjYmx4MFhIUmNkRngwSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lHTnZibVpwWnk1MWNteEJjbWR6S1NBNklIVnliRHRjY2x4dVhIUmNkRngwZlN4Y2NseHVYSFJjZEZ4MFhISmNibHgwWEhSY2RDOHZJRVJsYkdWbllYUmxjeUIwYnlCeVpYRXViRzloWkM0Z1FuSnZhMlZ1SUc5MWRDQmhjeUJoSUhObGNHRnlZWFJsSUdaMWJtTjBhVzl1SUhSdklHRnNiRzkzSUc5MlpYSnlhV1JwYm1jZ2FXNGdkR2hsSUc5d2RHbHRhWHBsY2k1Y2NseHVYSFJjZEZ4MGJHOWhaRG9nWm5WdVkzUnBiMjRvYVdRc0lIVnliQ2tnZTF4eVhHNWNkRngwWEhSY2RISmxjUzVzYjJGa0tHTnZiblJsZUhRc0lHbGtMQ0IxY213cE8xeHlYRzVjZEZ4MFhIUjlMRnh5WEc1Y2RGeDBYSFJjY2x4dVhIUmNkRngwTHlvcVhISmNibHgwWEhSY2RDQXFJRVY0WldOMWRHVnpJR0VnYlc5a2RXeGxJR05oYkd4aVlXTnJJR1oxYm1OMGFXOXVMaUJjY2x4dVhIUmNkRngwSUNvZ1hISmNibHgwWEhSY2RDQXFJRUp5YjJ0bGJpQnZkWFFnWVhNZ1lTQnpaWEJoY21GMFpTQm1kVzVqZEdsdmJpQnpiMnhsYkhrZ2RHOGdZV3hzYjNjZ2RHaGxJR0oxYVd4a0lITjVjM1JsYlNCMGJ5QnpaWEYxWlc1alpTQjBhR1VnWm1sc1pYTWdhVzRnZEdobElHSjFhV3gwWEhKY2JseDBYSFJjZENBcUlHeGhlV1Z5SUdsdUlIUm9aU0J5YVdkb2RDQnpaWEYxWlc1alpTNWNjbHh1WEhSY2RGeDBJQ3BjY2x4dVhIUmNkRngwSUNvZ1FIQnlhWFpoZEdWY2NseHVYSFJjZEZ4MElDb3ZYSEpjYmx4MFhIUmNkR1Y0WldORFlqb2dablZ1WTNScGIyNG9ibUZ0WlN3Z1kyRnNiR0poWTJzc0lHRnlaM01zSUdWNGNHOXlkSE1wSUh0Y2NseHVYSFJjZEZ4MFhIUnlaWFIxY200Z1kyRnNiR0poWTJzdVlYQndiSGtvWlhod2IzSjBjeXdnWVhKbmN5azdYSEpjYmx4MFhIUmNkSDBzWEhKY2JseDBYSFJjZEZ4eVhHNWNkRngwWEhRdktpcGNjbHh1WEhSY2RGeDBJQ29nUTJGc2JHSmhZMnNnWm05eUlITmpjbWx3ZENCc2IyRmtjeXdnZFhObFpDQjBieUJqYUdWamF5QnpkR0YwZFhNZ2IyWWdiRzloWkdsdVp5NWNjbHh1WEhSY2RGeDBJQ3BjY2x4dVhIUmNkRngwSUNvZ1FIQmhjbUZ0SUh0RmRtVnVkSDBnWlhaMElIUm9aU0JsZG1WdWRDQm1jbTl0SUhSb1pTQmljbTkzYzJWeUlHWnZjaUIwYUdVZ2MyTnlhWEIwSUhSb1lYUWdkMkZ6SUd4dllXUmxaQzVjY2x4dVhIUmNkRngwSUNvdlhISmNibHgwWEhSY2RHOXVVMk55YVhCMFRHOWhaRG9nWm5WdVkzUnBiMjRvWlhaMEtTQjdYSEpjYmx4MFhIUmNkRngwTHk4Z1ZYTnBibWNnWTNWeWNtVnVkRlJoY21kbGRDQnBibk4wWldGa0lHOW1JSFJoY21kbGRDQm1iM0lnUm1seVpXWnZlQ0F5TGpBbmN5QnpZV3RsTGlCT2IzUmNjbHh1WEhSY2RGeDBYSFF2THlCaGJHd2diMnhrSUdKeWIzZHpaWEp6SUhkcGJHd2dZbVVnYzNWd2NHOXlkR1ZrTENCaWRYUWdkR2hwY3lCdmJtVWdkMkZ6SUdWaGMza2daVzV2ZFdkb1hISmNibHgwWEhSY2RGeDBMeThnZEc4Z2MzVndjRzl5ZENCaGJtUWdjM1JwYkd3Z2JXRnJaWE1nYzJWdWMyVXVYSEpjYmx4MFhIUmNkRngwYVdZZ0tHVjJkQzUwZVhCbElEMDlQU0FuYkc5aFpDY2dmSHhjY2x4dVhIUmNkRngwWEhSY2RDaHlaV0ZrZVZKbFowVjRjQzUwWlhOMEtDaGxkblF1WTNWeWNtVnVkRlJoY21kbGRDQjhmQ0JsZG5RdWMzSmpSV3hsYldWdWRDa3VjbVZoWkhsVGRHRjBaU2twS1NCN1hISmNibHgwWEhSY2RGeDBYSFF2THlCU1pYTmxkQ0JwYm5SbGNtRmpkR2wyWlNCelkzSnBjSFFnYzI4Z1lTQnpZM0pwY0hRZ2JtOWtaU0JwY3lCdWIzUWdhR1ZzWkNCdmJuUnZJR1p2Y2x4eVhHNWNkRngwWEhSY2RGeDBMeThnZEc4Z2JHOXVaeTVjY2x4dVhIUmNkRngwWEhSY2RHbHVkR1Z5WVdOMGFYWmxVMk55YVhCMElEMGdiblZzYkR0Y2NseHVYSFJjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEZ4MEx5OGdVSFZzYkNCdmRYUWdkR2hsSUc1aGJXVWdiMllnZEdobElHMXZaSFZzWlNCaGJtUWdkR2hsSUdOdmJuUmxlSFF1WEhKY2JseDBYSFJjZEZ4MFhIUnNaWFFnWkdGMFlTQTlJR2RsZEZOamNtbHdkRVJoZEdFb1pYWjBLVHRjY2x4dVhIUmNkRngwWEhSY2RHTnZiblJsZUhRdVkyOXRjR3hsZEdWTWIyRmtLR1JoZEdFdWFXUXBPMXh5WEc1Y2RGeDBYSFJjZEgxY2NseHVYSFJjZEZ4MGZTeGNjbHh1WEhSY2RGeDBYSEpjYmx4MFhIUmNkQzhxS2x4eVhHNWNkRngwWEhRZ0tpQkRZV3hzWW1GamF5Qm1iM0lnYzJOeWFYQjBJR1Z5Y205eWN5NWNjbHh1WEhSY2RGeDBJQ292WEhKY2JseDBYSFJjZEc5dVUyTnlhWEIwUlhKeWIzSTZJR1oxYm1OMGFXOXVLR1YyZENrZ2UxeHlYRzVjZEZ4MFhIUmNkR3hsZENCa1lYUmhJRDBnWjJWMFUyTnlhWEIwUkdGMFlTaGxkblFwTzF4eVhHNWNkRngwWEhSY2RHbG1JQ2doYUdGelVHRjBhRVpoYkd4aVlXTnJLR1JoZEdFdWFXUXBLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUnNaWFFnY0dGeVpXNTBjeUE5SUZ0ZE8xeHlYRzVjZEZ4MFhIUmNkRngwWldGamFGQnliM0FvY21WbmFYTjBjbmtzSUdaMWJtTjBhVzl1S0haaGJIVmxMQ0JyWlhrcElIdGNjbHh1WEhSY2RGeDBYSFJjZEZ4MGFXWWdLR3RsZVM1cGJtUmxlRTltS0NkZlFISW5LU0FoUFQwZ01Da2dlMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkR1ZoWTJnb2RtRnNkV1V1WkdWd1RXRndjeXdnWm5WdVkzUnBiMjRvWkdWd1RXRndLU0I3WEhKY2JseDBYSFJjZEZ4MFhIUmNkRngwWEhScFppQW9aR1Z3VFdGd0xtbGtJRDA5UFNCa1lYUmhMbWxrS1NCN1hISmNibHgwWEhSY2RGeDBYSFJjZEZ4MFhIUmNkSEJoY21WdWRITXVjSFZ6YUNoclpYa3BPMXh5WEc1Y2RGeDBYSFJjZEZ4MFhIUmNkRngwZlZ4eVhHNWNkRngwWEhSY2RGeDBYSFJjZEZ4MGNtVjBkWEp1SUhSeWRXVTdYSEpjYmx4MFhIUmNkRngwWEhSY2RGeDBmU2s3WEhKY2JseDBYSFJjZEZ4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhSY2RIMHBPMXh5WEc1Y2RGeDBYSFJjZEZ4MGNtVjBkWEp1SUc5dVJYSnliM0lvYldGclpVVnljbTl5S0NkelkzSnBjSFJsY25KdmNpY3NJQ2RUWTNKcGNIUWdaWEp5YjNJZ1ptOXlJRndpSnlBcklHUmhkR0V1YVdRZ0sxeHlYRzVjZEZ4MFhIUmNkRngwWEhRb2NHRnlaVzUwY3k1c1pXNW5kR2dnUDF4eVhHNWNkRngwWEhSY2RGeDBYSFFnSjF3aUxDQnVaV1ZrWldRZ1luazZJQ2NnS3lCd1lYSmxiblJ6TG1wdmFXNG9KeXdnSnlrZ09seHlYRzVjZEZ4MFhIUmNkRngwWEhRZ0oxd2lKeWtzSUdWMmRDd2dXMlJoZEdFdWFXUmRLU2s3WEhKY2JseDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUjlYSEpjYmx4MFhIUjlPMXh5WEc1Y2RGeDBYSEpjYmx4MFhIUmpiMjUwWlhoMExuSmxjWFZwY21VZ1BTQmpiMjUwWlhoMExtMWhhMlZTWlhGMWFYSmxLQ2s3WEhKY2JseDBYSFJ5WlhSMWNtNGdZMjl1ZEdWNGREdGNjbHh1WEhSOVhISmNibHgwWEhKY2JseDBMeW9xWEhKY2JseDBJQ29nVFdGcGJpQmxiblJ5ZVNCd2IybHVkQzVjY2x4dVhIUWdLbHh5WEc1Y2RDQXFJRWxtSUhSb1pTQnZibXg1SUdGeVozVnRaVzUwSUhSdklISmxjWFZwY21VZ2FYTWdZU0J6ZEhKcGJtY3NJSFJvWlc0Z2RHaGxJRzF2WkhWc1pTQjBhR0YwSUdseklISmxjSEpsYzJWdWRHVmtJR0o1SUhSb1lYUWdjM1J5YVc1bklHbHpJR1psZEdOb1pXUWdabTl5SUZ4eVhHNWNkQ0FxSUhSb1pTQmhjSEJ5YjNCeWFXRjBaU0JqYjI1MFpYaDBMbHh5WEc1Y2RDQXFYSEpjYmx4MElDb2dTV1lnZEdobElHWnBjbk4wSUdGeVozVnRaVzUwSUdseklHRnVJR0Z5Y21GNUxDQjBhR1Z1SUdsMElIZHBiR3dnWW1VZ2RISmxZWFJsWkNCaGN5QmhiaUJoY25KaGVTQnZaaUJrWlhCbGJtUmxibU41SUhOMGNtbHVaeUJ1WVcxbGN5QjBieUJtWlhSamFDNGdRVzRnWEhKY2JseDBJQ29nYjNCMGFXOXVZV3dnWm5WdVkzUnBiMjRnWTJGc2JHSmhZMnNnWTJGdUlHSmxJSE53WldOcFptbGxaQ0IwYnlCbGVHVmpkWFJsSUhkb1pXNGdZV3hzSUc5bUlIUm9iM05sSUdSbGNHVnVaR1Z1WTJsbGN5QmhjbVVnWVhaaGFXeGhZbXhsTGx4eVhHNWNkQ0FxWEhKY2JseDBJQ29nVFdGclpTQmhJR3h2WTJGc0lISmxjU0IyWVhKcFlXSnNaU0IwYnlCb1pXeHdJRU5oYW1FZ1kyOXRjR3hwWVc1alpTQW9hWFFnWVhOemRXMWxjeUIwYUdsdVozTWdiMjRnWVNCeVpYRjFhWEpsSUhSb1lYUWdZWEpsSUc1dmRDQnpkR0Z1WkdGeVpHbDZaV1FwTENCY2NseHVYSFFnS2lCaGJtUWdkRzhnWjJsMlpTQmhJSE5vYjNKMElHNWhiV1VnWm05eUlHMXBibWxtYVdOaGRHbHZiaTlzYjJOaGJDQnpZMjl3WlNCMWMyVXVYSEpjYmx4MElDb3ZYSEpjYmx4MGNtVnhJRDBnZDJsdVpHOTNMbkpsY1hWcGNtVnFjeUE5SUdaMWJtTjBhVzl1S0dSbGNITXNJR05oYkd4aVlXTnJMQ0JsY25KaVlXTnJMQ0J2Y0hScGIyNWhiQ2tnZTF4eVhHNWNkRngwTHk4Z1JtbHVaQ0IwYUdVZ2NtbG5hSFFnWTI5dWRHVjRkQ3dnZFhObElHUmxabUYxYkhSY2NseHVYSFJjZEd4bGRDQmpiMjUwWlhoME95QmNjbHh1WEhSY2RHeGxkQ0JqYjI1bWFXYzdYSEpjYmx4MFhIUnNaWFFnWTI5dWRHVjRkRTVoYldVZ1BTQmtaV1pEYjI1MFpYaDBUbUZ0WlR0Y2NseHVYSFJjZEZ4eVhHNWNkRngwTHk4Z1JHVjBaWEp0YVc1bElHbG1JR2hoZG1VZ1kyOXVabWxuSUc5aWFtVmpkQ0JwYmlCMGFHVWdZMkZzYkM1Y2NseHVYSFJjZEdsbUlDZ2hhWE5CY25KaGVTaGtaWEJ6S1NBbUppQjBlWEJsYjJZZ1pHVndjeUFoUFQwZ0ozTjBjbWx1WnljcElIdGNjbHh1WEhSY2RGeDBMeThnWkdWd2N5QnBjeUJoSUdOdmJtWnBaeUJ2WW1wbFkzUmNjbHh1WEhSY2RGeDBZMjl1Wm1sbklEMGdaR1Z3Y3p0Y2NseHVYSFJjZEZ4MGFXWWdLR2x6UVhKeVlYa29ZMkZzYkdKaFkyc3BLU0I3WEhKY2JseDBYSFJjZEZ4MEx5OGdRV1JxZFhOMElHRnlaM01nYVdZZ2RHaGxjbVVnWVhKbElHUmxjR1Z1WkdWdVkybGxjMXh5WEc1Y2RGeDBYSFJjZEdSbGNITWdQU0JqWVd4c1ltRmphenRjY2x4dVhIUmNkRngwWEhSallXeHNZbUZqYXlBOUlHVnljbUpoWTJzN1hISmNibHgwWEhSY2RGeDBaWEp5WW1GamF5QTlJRzl3ZEdsdmJtRnNPMXh5WEc1Y2RGeDBYSFI5SUdWc2MyVWdlMXh5WEc1Y2RGeDBYSFJjZEdSbGNITWdQU0JiWFR0Y2NseHVYSFJjZEZ4MGZWeHlYRzVjZEZ4MGZWeHlYRzVjZEZ4MFhISmNibHgwWEhScFppQW9ZMjl1Wm1sbklDWW1JR052Ym1acFp5NWpiMjUwWlhoMEtTQjdYSEpjYmx4MFhIUmNkR052Ym5SbGVIUk9ZVzFsSUQwZ1kyOXVabWxuTG1OdmJuUmxlSFE3WEhKY2JseDBYSFI5WEhKY2JseDBYSFJjY2x4dVhIUmNkR052Ym5SbGVIUWdQU0JuWlhSUGQyNG9ZMjl1ZEdWNGRITXNJR052Ym5SbGVIUk9ZVzFsS1R0Y2NseHVYSFJjZEdsbUlDZ2hZMjl1ZEdWNGRDa2dlMXh5WEc1Y2RGeDBYSFJqYjI1MFpYaDBJRDBnWTI5dWRHVjRkSE5iWTI5dWRHVjRkRTVoYldWZElEMGdjbVZ4TG5NdWJtVjNRMjl1ZEdWNGRDaGpiMjUwWlhoMFRtRnRaU2s3WEhKY2JseDBYSFI5WEhKY2JseDBYSFJjY2x4dVhIUmNkR2xtSUNoamIyNW1hV2NwSUh0Y2NseHVYSFJjZEZ4MFkyOXVkR1Y0ZEM1amIyNW1hV2QxY21Vb1kyOXVabWxuS1R0Y2NseHVYSFJjZEgxY2NseHVYSFJjZEZ4eVhHNWNkRngwY21WMGRYSnVJR052Ym5SbGVIUXVjbVZ4ZFdseVpTaGtaWEJ6TENCallXeHNZbUZqYXl3Z1pYSnlZbUZqYXlrN1hISmNibHgwZlR0Y2NseHVYSFJjY2x4dVhIUXZLaXBjY2x4dVhIUWdLaUJUZFhCd2IzSjBJSEpsY1hWcGNtVXVZMjl1Wm1sbktDa2dkRzhnYldGclpTQnBkQ0JsWVhOcFpYSWdkRzhnWTI5dmNHVnlZWFJsSUhkcGRHZ2diM1JvWlhJZ1FVMUVJR3h2WVdSbGNuTWdiMjRnWjJ4dlltRnNiSGtnWVdkeVpXVmtJRzVoYldWekxseHlYRzVjZENBcUwxeHlYRzVjZEhKbGNTNWpiMjVtYVdjZ1BTQm1kVzVqZEdsdmJpaGpiMjVtYVdjcElIdGNjbHh1WEhSY2RISmxkSFZ5YmlCeVpYRW9ZMjl1Wm1sbktUdGNjbHh1WEhSOU8xeHlYRzVjZEZ4eVhHNWNkQzhxS2x4eVhHNWNkQ0FxSUVWNFpXTjFkR1VnYzI5dFpYUm9hVzVuSUdGbWRHVnlJSFJvWlNCamRYSnlaVzUwSUhScFkyc2diMllnZEdobElHVjJaVzUwSUd4dmIzQXVJRnh5WEc1Y2RDQXFJRnh5WEc1Y2RDQXFJRTkyWlhKeWFXUmxJR1p2Y2lCdmRHaGxjaUJsYm5aeklIUm9ZWFFnYUdGMlpTQmhJR0psZEhSbGNpQnpiMngxZEdsdmJpQjBhR0Z1SUhObGRGUnBiV1Z2ZFhRdVhISmNibHgwSUNvZ1hISmNibHgwSUNvZ1FIQmhjbUZ0SUNCN1JuVnVZM1JwYjI1OUlHWnVJR1oxYm1OMGFXOXVJSFJ2SUdWNFpXTjFkR1VnYkdGMFpYSXVYSEpjYmx4MElDb3ZYSEpjYmx4MGNtVnhMbTVsZUhSVWFXTnJJRDBnZEhsd1pXOW1JSE5sZEZScGJXVnZkWFFnSVQwOUlDZDFibVJsWm1sdVpXUW5JRDhnWm5WdVkzUnBiMjRvWm00cElIdGNjbHh1WEhSY2RITmxkRlJwYldWdmRYUW9abTRzSURRcE8xeHlYRzVjZEgwZ09pQm1kVzVqZEdsdmJpaG1iaWtnZTF4eVhHNWNkRngwWm00b0tUdGNjbHh1WEhSOU8xeHlYRzVjZEZ4eVhHNWNkQzhxS2x4eVhHNWNkQ0FxSUVWNGNHOXlkQ0J5WlhGMWFYSmxJR0Z6SUdFZ1oyeHZZbUZzTENCaWRYUWdiMjVzZVNCcFppQnBkQ0JrYjJWeklHNXZkQ0JoYkhKbFlXUjVJR1Y0YVhOMExseHlYRzVjZENBcUwxeHlYRzVjZEdsbUlDZ2hkMmx1Wkc5M0xuSmxjWFZwY21VcElIdGNjbHh1WEhSY2RIZHBibVJ2ZHk1eVpYRjFhWEpsSUQwZ2NtVnhPMXh5WEc1Y2RIMWNjbHh1WEhSY2NseHVYSFJ5WlhFdWRtVnljMmx2YmlBOUlIWmxjbk5wYjI0N1hISmNibHgwWEhKY2JseDBMeThnVlhObFpDQjBieUJtYVd4MFpYSWdiM1YwSUdSbGNHVnVaR1Z1WTJsbGN5QjBhR0YwSUdGeVpTQmhiSEpsWVdSNUlIQmhkR2h6TGx4eVhHNWNkSEpsY1M1cWMwVjRkRkpsWjBWNGNDQTlJQzllWEZ3dmZEcDhYRncvZkZ4Y0xtcHpKQzg3WEhKY2JseDBjbVZ4TG1selFuSnZkM05sY2lBOUlHbHpRbkp2ZDNObGNqdGNjbHh1WEhSeklEMGdjbVZ4TG5NZ1BTQjdYSEpjYmx4MFhIUmpiMjUwWlhoMGN6b2dZMjl1ZEdWNGRITXNYSEpjYmx4MFhIUnVaWGREYjI1MFpYaDBPaUJ1WlhkRGIyNTBaWGgwWEhKY2JseDBmVHRjY2x4dVhIUmNjbHh1WEhRdkx5QkRjbVZoZEdVZ1pHVm1ZWFZzZENCamIyNTBaWGgwTGx4eVhHNWNkSEpsY1NoN2ZTazdYSEpjYmx4MFhISmNibHgwTHk4Z1JYaHdiM0owY3lCemIyMWxJR052Ym5SbGVIUXRjMlZ1YzJsMGFYWmxJRzFsZEdodlpITWdiMjRnWjJ4dlltRnNJSEpsY1hWcGNtVXVYSEpjYmx4MFpXRmphQ2hiWEhKY2JseDBYSFFuZEc5VmNtd25MRnh5WEc1Y2RGeDBKM1Z1WkdWbUp5eGNjbHh1WEhSY2RDZGtaV1pwYm1Wa0p5eGNjbHh1WEhSY2RDZHpjR1ZqYVdacFpXUW5YSEpjYmx4MFhTd2dablZ1WTNScGIyNG9jSEp2Y0NrZ2UxeHlYRzVjZEZ4MEx5OGdJRkpsWm1WeVpXNWpaU0JtY205dElHTnZiblJsZUhSeklHbHVjM1JsWVdRZ2IyWWdaV0Z5YkhrZ1ltbHVaR2x1WnlCMGJ5QmtaV1poZFd4MElHTnZiblJsZUhRc1hISmNibHgwWEhRdkx5QnpieUIwYUdGMElHUjFjbWx1WnlCaWRXbHNaSE1zSUhSb1pTQnNZWFJsYzNRZ2FXNXpkR0Z1WTJVZ2IyWWdkR2hsSUdSbFptRjFiSFFnWTI5dWRHVjRkRnh5WEc1Y2RGeDBMeThnZDJsMGFDQnBkSE1nWTI5dVptbG5JR2RsZEhNZ2RYTmxaQzVjY2x4dVhIUmNkSEpsY1Z0d2NtOXdYU0E5SUdaMWJtTjBhVzl1S0NrZ2UxeHlYRzVjZEZ4MFhIUnNaWFFnWTNSNElEMGdZMjl1ZEdWNGRITmJaR1ZtUTI5dWRHVjRkRTVoYldWZE8xeHlYRzVjZEZ4MFhIUnlaWFIxY200Z1kzUjRMbkpsY1hWcGNtVmJjSEp2Y0YwdVlYQndiSGtvWTNSNExDQmhjbWQxYldWdWRITXBPMXh5WEc1Y2RGeDBmVHRjY2x4dVhIUjlLVHRjY2x4dVhIUmNjbHh1WEhScFppQW9hWE5DY205M2MyVnlLU0I3WEhKY2JseDBYSFJvWldGa0lEMGdjeTVvWldGa0lEMGdaRzlqZFcxbGJuUXVaMlYwUld4bGJXVnVkSE5DZVZSaFowNWhiV1VvSjJobFlXUW5LVnN3WFR0Y2NseHVYSFJjZEM4dklFbG1JRUpCVTBVZ2RHRm5JR2x6SUdsdUlIQnNZWGtzSUhWemFXNW5JR0Z3Y0dWdVpFTm9hV3hrSUdseklHRWdjSEp2WW14bGJTQm1iM0lnU1VVMkxseHlYRzVjZEZ4MEx5OGdWMmhsYmlCMGFHRjBJR0p5YjNkelpYSWdaR2xsY3l3Z2RHaHBjeUJqWVc0Z1ltVWdjbVZ0YjNabFpDNGdSR1YwWVdsc2N5QnBiaUIwYUdseklHcFJkV1Z5ZVNCaWRXYzZYSEpjYmx4MFhIUXZMeUJvZEhSd09pOHZaR1YyTG1weGRXVnllUzVqYjIwdmRHbGphMlYwTHpJM01EbGNjbHh1WEhSY2RHSmhjMlZGYkdWdFpXNTBJRDBnWkc5amRXMWxiblF1WjJWMFJXeGxiV1Z1ZEhOQ2VWUmhaMDVoYldVb0oySmhjMlVuS1Zzd1hUdGNjbHh1WEhSY2RHbG1JQ2hpWVhObFJXeGxiV1Z1ZENrZ2UxeHlYRzVjZEZ4MFhIUm9aV0ZrSUQwZ2N5NW9aV0ZrSUQwZ1ltRnpaVVZzWlcxbGJuUXVjR0Z5Wlc1MFRtOWtaVHRjY2x4dVhIUmNkSDFjY2x4dVhIUjlYSEpjYmx4MFhISmNibHgwTHlvcVhISmNibHgwSUNvZ1FXNTVJR1Z5Y205eWN5QjBhR0YwSUhKbGNYVnBjbVVnWlhod2JHbGphWFJzZVNCblpXNWxjbUYwWlhNZ2QybHNiQ0JpWlNCd1lYTnpaV1FnZEc4Z2RHaHBjeUJtZFc1amRHbHZiaTRnWEhKY2JseDBJQ29nWEhKY2JseDBJQ29nU1c1MFpYSmpaWEIwTDI5MlpYSnlhV1JsSUdsMElHbG1JSGx2ZFNCM1lXNTBJR04xYzNSdmJTQmxjbkp2Y2lCb1lXNWtiR2x1Wnk1Y2NseHVYSFFnS2lCY2NseHVYSFFnS2lCQWNHRnlZVzBnZTBWeWNtOXlmU0JsY25JZ2RHaGxJR1Z5Y205eUlHOWlhbVZqZEM1Y2NseHVYSFFnS2k5Y2NseHVYSFJ5WlhFdWIyNUZjbkp2Y2lBOUlHUmxabUYxYkhSUGJrVnljbTl5TzF4eVhHNWNkRnh5WEc1Y2RDOHFLbHh5WEc1Y2RDQXFJRU55WldGMFpYTWdkR2hsSUc1dlpHVWdabTl5SUhSb1pTQnNiMkZrSUdOdmJXMWhibVF1SUU5dWJIa2dkWE5sWkNCcGJpQmljbTkzYzJWeUlHVnVkbk11WEhKY2JseDBJQ292WEhKY2JseDBjbVZ4TG1OeVpXRjBaVTV2WkdVZ1BTQm1kVzVqZEdsdmJpaGpiMjVtYVdjc0lHMXZaSFZzWlU1aGJXVXNJSFZ5YkNrZ2UxeHlYRzVjZEZ4MFkyOXVjM1FnYm05a1pTQTlJR052Ym1acFp5NTRhSFJ0YkNBL1hISmNibHgwWEhRZ0lDQWdJQ0FnSUNBZ0lHUnZZM1Z0Wlc1MExtTnlaV0YwWlVWc1pXMWxiblJPVXlnbmFIUjBjRG92TDNkM2R5NTNNeTV2Y21jdk1UazVPUzk0YUhSdGJDY3NJQ2RvZEcxc09uTmpjbWx3ZENjcElEcGNjbHh1WEhSY2RDQWdJQ0FnSUNBZ0lDQWdaRzlqZFcxbGJuUXVZM0psWVhSbFJXeGxiV1Z1ZENnbmMyTnlhWEIwSnlrN1hISmNibHgwWEhSdWIyUmxMblI1Y0dVZ1BTQmpiMjVtYVdjdWMyTnlhWEIwVkhsd1pTQjhmQ0FuZEdWNGRDOXFZWFpoYzJOeWFYQjBKenRjY2x4dVhIUmNkRzV2WkdVdVkyaGhjbk5sZENBOUlDZDFkR1l0T0NjN1hISmNibHgwWEhSdWIyUmxMbUZ6ZVc1aklEMGdkSEoxWlR0Y2NseHVYSFJjZEhKbGRIVnliaUJ1YjJSbE8xeHlYRzVjZEgwN1hISmNibHgwWEhKY2JseDBMeW9xWEhKY2JseDBJQ29nUkc5bGN5QjBhR1VnY21WeGRXVnpkQ0IwYnlCc2IyRmtJR0VnYlc5a2RXeGxJR1p2Y2lCMGFHVWdZbkp2ZDNObGNpQmpZWE5sTGx4eVhHNWNkQ0FxSUZ4eVhHNWNkQ0FxSUUxaGEyVWdkR2hwY3lCaElITmxjR0Z5WVhSbElHWjFibU4wYVc5dUlIUnZJR0ZzYkc5M0lHOTBhR1Z5SUdWdWRtbHliMjV0Wlc1MGN5QjBieUJ2ZG1WeWNtbGtaU0JwZEM1Y2NseHVYSFFnS2x4eVhHNWNkQ0FxSUVCd1lYSmhiU0I3VDJKcVpXTjBmU0JqYjI1MFpYaDBJSFJvWlNCeVpYRjFhWEpsSUdOdmJuUmxlSFFnZEc4Z1ptbHVaQ0J6ZEdGMFpTNWNjbHh1WEhRZ0tpQkFjR0Z5WVcwZ2UxTjBjbWx1WjMwZ2JXOWtkV3hsVG1GdFpTQjBhR1VnYm1GdFpTQnZaaUIwYUdVZ2JXOWtkV3hsTGx4eVhHNWNkQ0FxSUVCd1lYSmhiU0I3VDJKcVpXTjBmU0IxY213Z2RHaGxJRlZTVENCMGJ5QjBhR1VnYlc5a2RXeGxMbHh5WEc1Y2RDQXFMMXh5WEc1Y2RISmxjUzVzYjJGa0lEMGdablZ1WTNScGIyNG9ZMjl1ZEdWNGRDd2diVzlrZFd4bFRtRnRaU3dnZFhKc0tTQjdYSEpjYmx4MFhIUnNaWFFnWTI5dVptbG5JRDBnS0dOdmJuUmxlSFFnSmlZZ1kyOXVkR1Y0ZEM1amIyNW1hV2NwSUh4OElIdDlPMXh5WEc1Y2RGeDBiR1YwSUc1dlpHVTdYSEpjYmx4MFhIUmNjbHh1WEhSY2RHbG1JQ2hwYzBKeWIzZHpaWElwSUh0Y2NseHVYSFJjZEZ4MEx5OGdTVzRnZEdobElHSnliM2R6WlhJZ2MyOGdkWE5sSUdFZ2MyTnlhWEIwSUhSaFoxeHlYRzVjZEZ4MFhIUnViMlJsSUQwZ2NtVnhMbU55WldGMFpVNXZaR1VvWTI5dVptbG5MQ0J0YjJSMWJHVk9ZVzFsTENCMWNtd3BPMXh5WEc1Y2RGeDBYSFJwWmlBb1kyOXVabWxuTG05dVRtOWtaVU55WldGMFpXUXBJSHRjY2x4dVhIUmNkRngwWEhSamIyNW1hV2N1YjI1T2IyUmxRM0psWVhSbFpDaHViMlJsTENCamIyNW1hV2NzSUcxdlpIVnNaVTVoYldVc0lIVnliQ2s3WEhKY2JseDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhISmNibHgwWEhSY2RHNXZaR1V1YzJWMFFYUjBjbWxpZFhSbEtDZGtZWFJoTFhKbGNYVnBjbVZqYjI1MFpYaDBKeXdnWTI5dWRHVjRkQzVqYjI1MFpYaDBUbUZ0WlNrN1hISmNibHgwWEhSY2RHNXZaR1V1YzJWMFFYUjBjbWxpZFhSbEtDZGtZWFJoTFhKbGNYVnBjbVZ0YjJSMWJHVW5MQ0J0YjJSMWJHVk9ZVzFsS1R0Y2NseHVYSFJjZEZ4MFhISmNibHgwWEhSY2RDOHZJRk5sZENCMWNDQnNiMkZrSUd4cGMzUmxibVZ5TGlCVVpYTjBJR0YwZEdGamFFVjJaVzUwSUdacGNuTjBJR0psWTJGMWMyVWdTVVU1SUdoaGN5QmhJSE4xWW5Sc1pTQnBjM04xWlNCcGJpQnBkSE1nWVdSa1JYWmxiblJNYVhOMFpXNWxjaUJoYm1RZ1hISmNibHgwWEhSY2RDOHZJSE5qY21sd2RDQnZibXh2WVdRZ1ptbHlhVzVuY3lCMGFHRjBJR1J2SUc1dmRDQnRZWFJqYUNCMGFHVWdZbVZvWVhacGIzSWdiMllnWVd4c0lHOTBhR1Z5SUdKeWIzZHpaWEp6SUhkcGRHZ2dZV1JrUlhabGJuUk1hWE4wWlc1bGNpQnpkWEJ3YjNKMExDQmNjbHh1WEhSY2RGeDBMeThnZDJocFkyZ2dabWx5WlNCMGFHVWdiMjVzYjJGa0lHVjJaVzUwSUdadmNpQmhJSE5qY21sd2RDQnlhV2RvZENCaFpuUmxjaUIwYUdVZ2MyTnlhWEIwSUdWNFpXTjFkR2x2Ymk0Z1UyVmxPbHh5WEc1Y2RGeDBYSFF2THlCb2RIUndjem92TDJOdmJtNWxZM1F1YldsamNtOXpiMlowTG1OdmJTOUpSUzltWldWa1ltRmpheTlrWlhSaGFXeHpMelkwT0RBMU55OXpZM0pwY0hRdGIyNXNiMkZrTFdWMlpXNTBMV2x6TFc1dmRDMW1hWEpsWkMxcGJXMWxaR2xoZEdWc2VTMWhablJsY2kxelkzSnBjSFF0WlhobFkzVjBhVzl1WEhKY2JseDBYSFJjZEM4dklGVk9SazlTVkZWT1FWUkZURmtnVDNCbGNtRWdhVzF3YkdWdFpXNTBjeUJoZEhSaFkyaEZkbVZ1ZENCaWRYUWdaRzlsY3lCdWIzUWdabTlzYkc5M0lIUm9aU0J6WTNKcGNIUWdjMk55YVhCMElHVjRaV04xZEdsdmJpQnRiMlJsTGx4eVhHNWNkRngwWEhScFppQW9ibTlrWlM1aGRIUmhZMmhGZG1WdWRDQW1KbHh5WEc1Y2RGeDBYSFJjZEM4dklFTm9aV05ySUdsbUlHNXZaR1V1WVhSMFlXTm9SWFpsYm5RZ2FYTWdZWEowYVdacFkybGhiR3g1SUdGa1pHVmtJR0o1SUdOMWMzUnZiU0J6WTNKcGNIUWdiM0lnYm1GMGFYWmxiSGtnYzNWd2NHOXlkR1ZrSUdKNUlHSnliM2R6WlhKY2NseHVYSFJjZEZ4MFhIUXZMeUJ5WldGa0lHaDBkSEJ6T2k4dloybDBhSFZpTG1OdmJTOXFjbUoxY210bEwzSmxjWFZwY21WcWN5OXBjM04xWlhNdk1UZzNYSEpjYmx4MFhIUmNkRngwTHk4Z2FXWWdkMlVnWTJGdUlFNVBWQ0JtYVc1a0lGdHVZWFJwZG1VZ1kyOWtaVjBnZEdobGJpQnBkQ0J0ZFhOMElFNVBWQ0J1WVhScGRtVnNlU0J6ZFhCd2IzSjBaV1F1SUdsdUlFbEZPQ3dnYm05a1pTNWhkSFJoWTJoRmRtVnVkQ0JrYjJWeklGeHlYRzVjZEZ4MFhIUmNkQzh2SUc1dmRDQm9ZWFpsSUhSdlUzUnlhVzVuS0NrdUlFNXZkR1VnZEdobElIUmxjM1FnWm05eUlGd2lXMjVoZEdsMlpTQmpiMlJsWENJZ2QybDBhQ0J1YnlCamJHOXphVzVuSUdKeVlXTmxMQ0J6WldVNlhISmNibHgwWEhSY2RGeDBMeThnYUhSMGNITTZMeTluYVhSb2RXSXVZMjl0TDJweVluVnlhMlV2Y21WeGRXbHlaV3B6TDJsemMzVmxjeTh5TnpOY2NseHVYSFJjZEZ4MFhIUWhLRzV2WkdVdVlYUjBZV05vUlhabGJuUXVkRzlUZEhKcGJtY2dKaVlnYm05a1pTNWhkSFJoWTJoRmRtVnVkQzUwYjFOMGNtbHVaeWdwTG1sdVpHVjRUMllvSjF0dVlYUnBkbVVnWTI5a1pTY3BJRHdnTUNrZ0ppWmNjbHh1WEhSY2RGeDBYSFFoYVhOUGNHVnlZU2tnZTF4eVhHNWNkRngwWEhSY2RDOHZJRkJ5YjJKaFlteDVJRWxGTGlCSlJTQW9ZWFFnYkdWaGMzUWdOaTA0S1NCa2J5QnViM1FnWm1seVpTQnpZM0pwY0hRZ2IyNXNiMkZrSUhKcFoyaDBJR0ZtZEdWeUlHVjRaV04xZEdsdVp5QjBhR1VnYzJOeWFYQjBMQ0J6YjF4eVhHNWNkRngwWEhSY2RDOHZJSGRsSUdOaGJtNXZkQ0IwYVdVZ2RHaGxJR0Z1YjI1NWJXOTFjeUJrWldacGJtVWdZMkZzYkNCMGJ5QmhJRzVoYldVdUlFaHZkMlYyWlhJc0lFbEZJSEpsY0c5eWRITWdkR2hsSUhOamNtbHdkQ0JoY3lCaVpXbHVaeUJwYmlCY2NseHVYSFJjZEZ4MFhIUXZMeUFuYVc1MFpYSmhZM1JwZG1VbklISmxZV1I1VTNSaGRHVWdZWFFnZEdobElIUnBiV1VnYjJZZ2RHaGxJR1JsWm1sdVpTQmpZV3hzTGx4eVhHNWNkRngwWEhSY2RIVnpaVWx1ZEdWeVlXTjBhWFpsSUQwZ2RISjFaVHRjY2x4dVhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUnViMlJsTG1GMGRHRmphRVYyWlc1MEtDZHZibkpsWVdSNWMzUmhkR1ZqYUdGdVoyVW5MQ0JqYjI1MFpYaDBMbTl1VTJOeWFYQjBURzloWkNrN1hISmNibHgwWEhSY2RGeDBMeThnU1hRZ2QyOTFiR1FnWW1VZ1ozSmxZWFFnZEc4Z1lXUmtJR0Z1SUdWeWNtOXlJR2hoYm1Sc1pYSWdhR1Z5WlNCMGJ5QmpZWFJqYUNBME1EUnpJR2x1SUVsRk9Tc3VJRWh2ZDJWMlpYSXNJRzl1Y21WaFpIbHpkR0YwWldOb1lXNW5aU0JjY2x4dVhIUmNkRngwWEhRdkx5QjNhV3hzSUdacGNtVWdZbVZtYjNKbElIUm9aU0JsY25KdmNpQm9ZVzVrYkdWeUxDQnpieUIwYUdGMElHUnZaWE1nYm05MElHaGxiSEF1SUVsbUlHRmtaRVYyWlc1MFRHbHpkR1Z1WlhJZ2FYTWdkWE5sWkN3Z2RHaGxiaUJKUlNCM2FXeHNJRnh5WEc1Y2RGeDBYSFJjZEM4dklHWnBjbVVnWlhKeWIzSWdZbVZtYjNKbElHeHZZV1FzSUdKMWRDQjNaU0JqWVc1dWIzUWdkWE5sSUhSb1lYUWdjR0YwYUhkaGVTQm5hWFpsYmlCMGFHVWdZMjl1Ym1WamRDNXRhV055YjNOdlpuUXVZMjl0SUdsemMzVmxJRnh5WEc1Y2RGeDBYSFJjZEM4dklHMWxiblJwYjI1bFpDQmhZbTkyWlNCaFltOTFkQ0J1YjNRZ1pHOXBibWNnZEdobElDZHpZM0pwY0hRZ1pYaGxZM1YwWlN3Z2RHaGxiaUJtYVhKbElIUm9aU0J6WTNKcGNIUWdiRzloWkNCbGRtVnVkQ0JzYVhOMFpXNWxjaUJpWldadmNtVWdYSEpjYmx4MFhIUmNkRngwTHk4Z1pYaGxZM1YwWlNCdVpYaDBJSE5qY21sd2RDY2dkR2hoZENCdmRHaGxjaUJpY205M2MyVnljeUJrYnk0Z1FtVnpkQ0JvYjNCbE9pQkpSVEV3SUdacGVHVnpJSFJvWlNCcGMzTjFaWE1zSUdGdVpDQjBhR1Z1SUdSbGMzUnliM2x6SUdGc2JDQmNjbHh1WEhSY2RGeDBYSFF2THlCcGJuTjBZV3hzY3lCdlppQkpSU0EyTFRrdVhISmNibHgwWEhSY2RGeDBMeThnYm05a1pTNWhkSFJoWTJoRmRtVnVkQ2duYjI1bGNuSnZjaWNzSUdOdmJuUmxlSFF1YjI1VFkzSnBjSFJGY25KdmNpazdYSEpjYmx4MFhIUmNkSDBnWld4elpTQjdYSEpjYmx4MFhIUmNkRngwYm05a1pTNWhaR1JGZG1WdWRFeHBjM1JsYm1WeUtDZHNiMkZrSnl3Z1kyOXVkR1Y0ZEM1dmJsTmpjbWx3ZEV4dllXUXNJR1poYkhObEtUdGNjbHh1WEhSY2RGeDBYSFJ1YjJSbExtRmtaRVYyWlc1MFRHbHpkR1Z1WlhJb0oyVnljbTl5Snl3Z1kyOXVkR1Y0ZEM1dmJsTmpjbWx3ZEVWeWNtOXlMQ0JtWVd4elpTazdYSEpjYmx4MFhIUmNkSDFjY2x4dVhIUmNkRngwYm05a1pTNXpjbU1nUFNCMWNtdzdYSEpjYmx4MFhIUmNkRnh5WEc1Y2RGeDBYSFF2THlCR2IzSWdjMjl0WlNCallXTm9aU0JqWVhObGN5QnBiaUJKUlNBMkxUZ3NJSFJvWlNCelkzSnBjSFFnWlhobFkzVjBaWE1nWW1WbWIzSmxJSFJvWlNCbGJtUWdiMllnZEdobElHRndjR1Z1WkVOb2FXeGtJR1Y0WldOMWRHbHZiaXdnYzI4Z2RHOGdYSEpjYmx4MFhIUmNkQzh2SUhScFpTQmhiaUJoYm05dWVXMXZkWE1nWkdWbWFXNWxJR05oYkd3Z2RHOGdkR2hsSUcxdlpIVnNaU0J1WVcxbElDaDNhR2xqYUNCcGN5QnpkRzl5WldRZ2IyNGdkR2hsSUc1dlpHVXBMQ0JvYjJ4a0lHOXVJSFJ2SUdFZ2NtVm1aWEpsYm1ObElIUnZJRnh5WEc1Y2RGeDBYSFF2THlCMGFHbHpJRzV2WkdVc0lHSjFkQ0JqYkdWaGNpQmhablJsY2lCMGFHVWdSRTlOSUdsdWMyVnlkR2x2Ymk1Y2NseHVYSFJjZEZ4MGFXWWdLR0poYzJWRmJHVnRaVzUwS1NCN1hISmNibHgwWEhSY2RGeDBhR1ZoWkM1cGJuTmxjblJDWldadmNtVW9ibTlrWlN3Z1ltRnpaVVZzWlcxbGJuUXBPMXh5WEc1Y2RGeDBYSFI5SUdWc2MyVWdlMXh5WEc1Y2RGeDBYSFJjZEdobFlXUXVZWEJ3Wlc1a1EyaHBiR1FvYm05a1pTazdYSEpjYmx4MFhIUmNkSDFjY2x4dVhIUmNkRngwWEhKY2JseDBYSFJjZEhKbGRIVnliaUJ1YjJSbE8xeHlYRzVjZEZ4MGZTQmxiSE5sSUdsbUlDaHBjMWRsWWxkdmNtdGxjaWtnZTF4eVhHNWNkRngwWEhSMGNua2dlMXh5WEc1Y2RGeDBYSFJjZEM4dklFbHVJR0VnZDJWaUlIZHZjbXRsY2l3Z2RYTmxJR2x0Y0c5eWRGTmpjbWx3ZEhNdUlGUm9hWE1nYVhNZ2JtOTBJR0VnZG1WeWVWeHlYRzVjZEZ4MFhIUmNkQzh2SUdWbVptbGphV1Z1ZENCMWMyVWdiMllnYVcxd2IzSjBVMk55YVhCMGN5d2dhVzF3YjNKMFUyTnlhWEIwY3lCM2FXeHNJR0pzYjJOcklIVnVkR2xzWEhKY2JseDBYSFJjZEZ4MEx5OGdhWFJ6SUhOamNtbHdkQ0JwY3lCa2IzZHViRzloWkdWa0lHRnVaQ0JsZG1Gc2RXRjBaV1F1SUVodmQyVjJaWElzSUdsbUlIZGxZaUIzYjNKclpYSnpYSEpjYmx4MFhIUmNkRngwTHk4Z1lYSmxJR2x1SUhCc1lYa3NJSFJvWlNCbGVIQmxZM1JoZEdsdmJpQnBjeUIwYUdGMElHRWdZblZwYkdRZ2FHRnpJR0psWlc0Z1pHOXVaU0J6YjF4eVhHNWNkRngwWEhSY2RDOHZJSFJvWVhRZ2IyNXNlU0J2Ym1VZ2MyTnlhWEIwSUc1bFpXUnpJSFJ2SUdKbElHeHZZV1JsWkNCaGJubDNZWGt1SUZSb2FYTWdiV0Y1SUc1bFpXUmNjbHh1WEhSY2RGeDBYSFF2THlCMGJ5QmlaU0J5WldWMllXeDFZWFJsWkNCcFppQnZkR2hsY2lCMWMyVWdZMkZ6WlhNZ1ltVmpiMjFsSUdOdmJXMXZiaTVjY2x4dVhIUmNkRngwWEhScGJYQnZjblJUWTNKcGNIUnpLSFZ5YkNrN1hISmNibHgwWEhSY2RGeDBYSEpjYmx4MFhIUmNkRngwTHk4Z1FXTmpiM1Z1ZENCbWIzSWdZVzV2Ym5sdGIzVnpJRzF2WkhWc1pYTmNjbHh1WEhSY2RGeDBYSFJqYjI1MFpYaDBMbU52YlhCc1pYUmxURzloWkNodGIyUjFiR1ZPWVcxbEtUdGNjbHh1WEhSY2RGeDBmU0JqWVhSamFDQW9aU2tnZTF4eVhHNWNkRngwWEhSY2RHTnZiblJsZUhRdWIyNUZjbkp2Y2lodFlXdGxSWEp5YjNJb0oybHRjRzl5ZEhOamNtbHdkSE1uTEZ4eVhHNWNkRngwWEhSY2RGeDBKMmx0Y0c5eWRGTmpjbWx3ZEhNZ1ptRnBiR1ZrSUdadmNpQW5JQ3RjY2x4dVhIUmNkRngwWEhSY2RHMXZaSFZzWlU1aGJXVWdLeUFuSUdGMElDY2dLeUIxY213c1hISmNibHgwWEhSY2RGeDBYSFJsTEZ4eVhHNWNkRngwWEhSY2RGeDBXMjF2WkhWc1pVNWhiV1ZkS1NrN1hISmNibHgwWEhSY2RIMWNjbHh1WEhSY2RIMWNjbHh1WEhSOU8xeHlYRzVjZEZ4eVhHNWNkQzh2SUV4dmIyc2dabTl5SUdFZ1pHRjBZUzF0WVdsdUlITmpjbWx3ZENCaGRIUnlhV0oxZEdVc0lIZG9hV05vSUdOdmRXeGtJR0ZzYzI4Z1lXUnFkWE4wSUhSb1pTQmlZWE5sVlhKc0xseHlYRzVjZEdsbUlDaHBjMEp5YjNkelpYSWdKaVlnSVdObVp5NXphMmx3UkdGMFlVMWhhVzRwSUh0Y2NseHVYSFJjZEM4dklFWnBaM1Z5WlNCdmRYUWdZbUZ6WlZWeWJDNGdSMlYwSUdsMElHWnliMjBnZEdobElITmpjbWx3ZENCMFlXY2dkMmwwYUNCeVpYRjFhWEpsTG1weklHbHVJR2wwTGx4eVhHNWNkRngwWldGamFGSmxkbVZ5YzJVb2MyTnlhWEIwY3lncExDQm1kVzVqZEdsdmJpaHpZM0pwY0hRcElIdGNjbHh1WEhSY2RGeDBMeThnVTJWMElIUm9aU0FuYUdWaFpDY2dkMmhsY21VZ2QyVWdZMkZ1SUdGd2NHVnVaQ0JqYUdsc1pISmxiaUJpZVZ4eVhHNWNkRngwWEhRdkx5QjFjMmx1WnlCMGFHVWdjMk55YVhCMEozTWdjR0Z5Wlc1MExseHlYRzVjZEZ4MFhIUnBaaUFvSVdobFlXUXBJSHRjY2x4dVhIUmNkRngwWEhSb1pXRmtJRDBnYzJOeWFYQjBMbkJoY21WdWRFNXZaR1U3WEhKY2JseDBYSFJjZEgxY2NseHVYSFJjZEZ4MFhISmNibHgwWEhSY2RDOHZJRXh2YjJzZ1ptOXlJR0VnWkdGMFlTMXRZV2x1SUdGMGRISnBZblYwWlNCMGJ5QnpaWFFnYldGcGJpQnpZM0pwY0hRZ1ptOXlJSFJvWlNCd1lXZGxYSEpjYmx4MFhIUmNkQzh2SUhSdklHeHZZV1F1SUVsbUlHbDBJR2x6SUhSb1pYSmxMQ0IwYUdVZ2NHRjBhQ0IwYnlCa1lYUmhJRzFoYVc0Z1ltVmpiMjFsY3lCMGFHVmNjbHh1WEhSY2RGeDBMeThnWW1GelpWVnliQ3dnYVdZZ2FYUWdhWE1nYm05MElHRnNjbVZoWkhrZ2MyVjBMbHh5WEc1Y2RGeDBYSFJrWVhSaFRXRnBiaUE5SUhOamNtbHdkQzVuWlhSQmRIUnlhV0oxZEdVb0oyUmhkR0V0YldGcGJpY3BPMXh5WEc1Y2RGeDBYSFJwWmlBb1pHRjBZVTFoYVc0cElIdGNjbHh1WEhSY2RGeDBYSFF2THlCUWNtVnpaWEoyWlNCa1lYUmhUV0ZwYmlCcGJpQmpZWE5sSUdsMElHbHpJR0VnY0dGMGFDQW9hUzVsTGlCamIyNTBZV2x1Y3lBblB5Y3BYSEpjYmx4MFhIUmNkRngwYldGcGJsTmpjbWx3ZENBOUlHUmhkR0ZOWVdsdU8xeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEM4dklGTmxkQ0JtYVc1aGJDQmlZWE5sVlhKc0lHbG1JSFJvWlhKbElHbHpJRzV2ZENCaGJISmxZV1I1SUdGdUlHVjRjR3hwWTJsMElHOXVaUzVjY2x4dVhIUmNkRngwWEhScFppQW9JV05tWnk1aVlYTmxWWEpzS1NCN1hISmNibHgwWEhSY2RGeDBYSFF2THlCUWRXeHNJRzltWmlCMGFHVWdaR2x5WldOMGIzSjVJRzltSUdSaGRHRXRiV0ZwYmlCbWIzSWdkWE5sSUdGeklIUm9aVnh5WEc1Y2RGeDBYSFJjZEZ4MEx5OGdZbUZ6WlZWeWJDNWNjbHh1WEhSY2RGeDBYSFJjZEhOeVl5QTlJRzFoYVc1VFkzSnBjSFF1YzNCc2FYUW9KeThuS1R0Y2NseHVYSFJjZEZ4MFhIUmNkRzFoYVc1VFkzSnBjSFFnUFNCemNtTXVjRzl3S0NrN1hISmNibHgwWEhSY2RGeDBYSFJ6ZFdKUVlYUm9JRDBnYzNKakxteGxibWQwYUNBL0lITnlZeTVxYjJsdUtDY3ZKeWtnS3lBbkx5Y2dPaUFuTGk4bk8xeHlYRzVjZEZ4MFhIUmNkRngwWEhKY2JseDBYSFJjZEZ4MFhIUmpabWN1WW1GelpWVnliQ0E5SUhOMVlsQmhkR2c3WEhKY2JseDBYSFJjZEZ4MGZWeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEM4dklGTjBjbWx3SUc5bVppQmhibmtnZEhKaGFXeHBibWNnTG1weklITnBibU5sSUcxaGFXNVRZM0pwY0hRZ2FYTWdibTkzWEhKY2JseDBYSFJjZEZ4MEx5OGdiR2xyWlNCaElHMXZaSFZzWlNCdVlXMWxMbHh5WEc1Y2RGeDBYSFJjZEcxaGFXNVRZM0pwY0hRZ1BTQnRZV2x1VTJOeWFYQjBMbkpsY0d4aFkyVW9hbk5UZFdabWFYaFNaV2RGZUhBc0lDY25LVHRjY2x4dVhIUmNkRngwWEhSY2NseHVYSFJjZEZ4MFhIUXZMeUJKWmlCdFlXbHVVMk55YVhCMElHbHpJSE4wYVd4c0lHRWdjR0YwYUN3Z1ptRnNiQ0JpWVdOcklIUnZJR1JoZEdGTllXbHVYSEpjYmx4MFhIUmNkRngwYVdZZ0tISmxjUzVxYzBWNGRGSmxaMFY0Y0M1MFpYTjBLRzFoYVc1VFkzSnBjSFFwS1NCN1hISmNibHgwWEhSY2RGeDBYSFJ0WVdsdVUyTnlhWEIwSUQwZ1pHRjBZVTFoYVc0N1hISmNibHgwWEhSY2RGeDBmVnh5WEc1Y2RGeDBYSFJjZEZ4eVhHNWNkRngwWEhSY2RDOHZJRkIxZENCMGFHVWdaR0YwWVMxdFlXbHVJSE5qY21sd2RDQnBiaUIwYUdVZ1ptbHNaWE1nZEc4Z2JHOWhaQzVjY2x4dVhIUmNkRngwWEhSalptY3VaR1Z3Y3lBOUlHTm1aeTVrWlhCeklEOGdZMlpuTG1SbGNITXVZMjl1WTJGMEtHMWhhVzVUWTNKcGNIUXBJRG9nVzIxaGFXNVRZM0pwY0hSZE8xeHlYRzVjZEZ4MFhIUmNkRnh5WEc1Y2RGeDBYSFJjZEhKbGRIVnliaUIwY25WbE8xeHlYRzVjZEZ4MFhIUjlYSEpjYmx4MFhIUjlLVHRjY2x4dVhIUjlYSEpjYmx4MFhISmNibHgwTHlvcVhISmNibHgwSUNvZ1JYaGxZM1YwWlhNZ2RHaGxJSFJsZUhRdUlFNXZjbTFoYkd4NUlHcDFjM1FnZFhObGN5QmxkbUZzTENCaWRYUWdZMkZ1SUdKbElHMXZaR2xtYVdWa0lIUnZJSFZ6WlNCaElHSmxkSFJsY2l3Z1pXNTJhWEp2Ym0xbGJuUXRjM0JsWTJsbWFXTWdZMkZzYkM0Z1hISmNibHgwSUNvZ1QyNXNlU0IxYzJWa0lHWnZjaUIwY21GdWMzQnBiR2x1WnlCc2IyRmtaWElnY0d4MVoybHVjeXdnYm05MElHWnZjaUJ3YkdGcGJpQktVeUJ0YjJSMWJHVnpMbHh5WEc1Y2RDQXFJRnh5WEc1Y2RDQXFJRUJ3WVhKaGJTQjdVM1J5YVc1bmZTQjBaWGgwSUZSb1pTQjBaWGgwSUhSdklHVjRaV04xZEdVdlpYWmhiSFZoZEdVdVhISmNibHgwSUNvdlhISmNibHgwY21WeExtVjRaV01nUFNCbWRXNWpkR2x2YmloMFpYaDBLU0I3WEhKY2JseDBYSFF2S21wemJHbHVkQ0JsZG1sc09pQjBjblZsSUNvdlhISmNibHgwWEhSeVpYUjFjbTRnWlhaaGJDaDBaWGgwS1R0Y2NseHVYSFI5TzF4eVhHNWNkRnh5WEc1Y2RDOHZJRk5sZENCMWNDQjNhWFJvSUdOdmJtWnBaeUJwYm1adkxseHlYRzVjZEhKbGNTaGpabWNwTzF4eVhHNTlLU2dwTzF4eVhHNGlYWDA9In0=
