'use strict';

/* --------------------------------------------------------------
 daterangepicker.js 2016-04-28
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Date Range Picker
 *
 * Creates an instance of the jQuery UI Daterangepicker widget which enables the user to select
 * a custom date range in the same datepicker, something that is not supported by jQuery UI.
 *
 * This widget requires the "general" translation section in order to translate the day
 * and month names.
 *
 * ### Options
 *
 * You can provide all the options of the following site as data attributes:
 *
 * {@link http://tamble.github.io/jquery-ui-daterangepicker/#options}
 *
 * ### Example
 *
 * ```html
 * <input type="text" data-jse-widget="daterangepicker" data-daterangepicker-date-format="dd.mm.yy" />
 * ```
 *
 * {@link https://github.com/tamble/jquery-ui-daterangepicker}
 *
 * @module JSE/Widgets/datarangepicker
 * @requires jQueryUI-Daterangepicker
 */
jse.widgets.module('daterangepicker', [jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.min.js', jse.source + '/vendor/momentjs/moment.min.js', jse.source + '/vendor/jquery-ui-daterangepicker/jquery.comiseo.daterangepicker.min.css'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Escape Key Code
  *
  * @type {Number}
  */

	var ESC_KEY_CODE = 27;

	/**
  * Tab Key Code
  *
  * @type {Number}
  */
	var TAB_KEY_CODE = 9;

	/**
  * Module Selector
  *
  * @type {Object}
  */
	var $this = $(this);

	/**
  * Default Options
  *
  * @type {Object}
  */
	var defaults = {
		presetRanges: [],
		dateFormat: jse.core.config.get('languageCode') === 'de' ? 'dd.mm.yy' : 'mm.dd.yy',
		momentFormat: jse.core.config.get('languageCode') === 'de' ? 'DD.MM.YY' : 'MM.DD.YY',
		applyButtonText: jse.core.lang.translate('apply', 'buttons'),
		cancelButtonText: jse.core.lang.translate('close', 'buttons'),
		datepickerOptions: {
			numberOfMonths: 2,
			changeMonth: true,
			changeYear: true,
			maxDate: null,
			minDate: new Date(1970, 1, 1),
			dayNamesMin: [jse.core.lang.translate('_SUNDAY_SHORT', 'general'), jse.core.lang.translate('_MONDAY_SHORT', 'general'), jse.core.lang.translate('_TUESDAY_SHORT', 'general'), jse.core.lang.translate('_WEDNESDAY_SHORT', 'general'), jse.core.lang.translate('_THURSDAY_SHORT', 'general'), jse.core.lang.translate('_FRIDAY_SHORT', 'general'), jse.core.lang.translate('_SATURDAY_SHORT', 'general')],
			monthNamesShort: [jse.core.lang.translate('_JANUARY_SHORT', 'general'), jse.core.lang.translate('_FEBRUARY_SHORT', 'general'), jse.core.lang.translate('_MARCH_SHORT', 'general'), jse.core.lang.translate('_APRIL_SHORT', 'general'), jse.core.lang.translate('_MAY_SHORT', 'general'), jse.core.lang.translate('_JUNE_SHORT', 'general'), jse.core.lang.translate('_JULY_SHORT', 'general'), jse.core.lang.translate('_AUGUST_SHORT', 'general'), jse.core.lang.translate('_SEPTEMBER_SHORT', 'general'), jse.core.lang.translate('_OCTOBER_SHORT', 'general'), jse.core.lang.translate('_NOVEMBER_SHORT', 'general'), jse.core.lang.translate('_DECEMBER_SHORT', 'general')],
			monthNames: [jse.core.lang.translate('_JANUARY', 'general'), jse.core.lang.translate('_FEBRUARY', 'general'), jse.core.lang.translate('_MARCH', 'general'), jse.core.lang.translate('_APRIL', 'general'), jse.core.lang.translate('_MAY', 'general'), jse.core.lang.translate('_JUNE', 'general'), jse.core.lang.translate('_JULY', 'general'), jse.core.lang.translate('_AUGUST', 'general'), jse.core.lang.translate('_SEPTEMBER', 'general'), jse.core.lang.translate('_OCTOBER', 'general'), jse.core.lang.translate('_NOVEMBER', 'general'), jse.core.lang.translate('_DECEMBER', 'general')]
		},
		onChange: function onChange() {
			var range = $this.siblings('.daterangepicker-helper').daterangepicker('getRange'),
			    start = moment(range.start).format(defaults.momentFormat),
			    end = moment(range.end).format(defaults.momentFormat),
			    value = start !== end ? start + ' - ' + end : '' + start;
			$this.val(value);
		},
		onClose: function onClose() {
			if ($this.val() === '') {
				$this.siblings('i').fadeIn();
			}
		}
	};

	/**
  * Final Options
  *
  * @type {Object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Update the range of the daterangepicker instance.
  *
  * Moment JS will try to parse the date string and will provide a value even if user's value is not
  * a complete date.
  */
	function _updateDaterangepicker() {
		try {
			if ($this.val() === '') {
				return;
			}

			var val = $this.val().split('-');
			var range = {};

			if (val.length === 1) {
				// Single date was selected. 
				range.start = range.end = moment(val[0], options.momentFormat).toDate();
			} else {
				// Date range was selected.
				range.start = moment(val[0], options.momentFormat).toDate();
				range.end = moment(val[1], options.momentFormat).toDate();
			}

			$this.siblings('.daterangepicker-helper').daterangepicker('setRange', range);
		} catch (error) {
			// Could not parse the date, do not update the input value.
			jse.core.debug.error('Daterangepicker Update Error:', error);
		}
	}

	/**
  * On Input Click/Focus Event
  *
  * Display the daterangepicker modal.
  */
	function _onInputClick() {
		if (!$('.comiseo-daterangepicker').is(':visible')) {
			$this.siblings('.daterangepicker-helper').daterangepicker('open');
			$this.siblings('i').fadeOut();
			$(document).trigger('click.sumo'); // Sumo Select compatibility for table-filter rows. 
		}
	}

	/**
  * On Input Key Down
  *
  * If the use presses the escape or tab key, close the daterangepicker modal. Otherwise if the user
  * presses the enter then the current value needs to be applied to daterangepicker.
  *
  * @param {Object} event
  */
	function _onInputKeyDown(event) {
		if (event.which === ESC_KEY_CODE || event.which === TAB_KEY_CODE) {
			// Close the daterangepicker modal. 
			$this.siblings('.daterangepicker-helper').daterangepicker('close');
			$this.blur();
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		// Daterangepicker needs to be loaded after jquery UI.
		var dependencies = [jse.source + '/vendor/jquery-ui-daterangepicker/jquery.comiseo.daterangepicker.min.js'];

		window.require(dependencies, function () {
			$this.wrap('<div class="daterangepicker-wrapper"></div>').parent().append('<i class="fa fa-calendar"></i>').append('<input type="text" class="daterangepicker-helper hidden" />').find('.daterangepicker-helper').daterangepicker(options);

			$this.siblings('button').css({
				visibility: 'hidden', // Hide the auto-generated button. 
				position: 'absolute' // Remove it from the normal flow.
			});

			$this.on('click, focus', _onInputClick).on('keydown', _onInputKeyDown).on('change', _updateDaterangepicker);

			done();
		});
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGVyYW5nZXBpY2tlci5qcyJdLCJuYW1lcyI6WyJqc2UiLCJ3aWRnZXRzIiwibW9kdWxlIiwic291cmNlIiwiZGF0YSIsIkVTQ19LRVlfQ09ERSIsIlRBQl9LRVlfQ09ERSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwicHJlc2V0UmFuZ2VzIiwiZGF0ZUZvcm1hdCIsImNvcmUiLCJjb25maWciLCJnZXQiLCJtb21lbnRGb3JtYXQiLCJhcHBseUJ1dHRvblRleHQiLCJsYW5nIiwidHJhbnNsYXRlIiwiY2FuY2VsQnV0dG9uVGV4dCIsImRhdGVwaWNrZXJPcHRpb25zIiwibnVtYmVyT2ZNb250aHMiLCJjaGFuZ2VNb250aCIsImNoYW5nZVllYXIiLCJtYXhEYXRlIiwibWluRGF0ZSIsIkRhdGUiLCJkYXlOYW1lc01pbiIsIm1vbnRoTmFtZXNTaG9ydCIsIm1vbnRoTmFtZXMiLCJvbkNoYW5nZSIsInJhbmdlIiwic2libGluZ3MiLCJkYXRlcmFuZ2VwaWNrZXIiLCJzdGFydCIsIm1vbWVudCIsImZvcm1hdCIsImVuZCIsInZhbHVlIiwidmFsIiwib25DbG9zZSIsImZhZGVJbiIsIm9wdGlvbnMiLCJleHRlbmQiLCJfdXBkYXRlRGF0ZXJhbmdlcGlja2VyIiwic3BsaXQiLCJsZW5ndGgiLCJ0b0RhdGUiLCJlcnJvciIsImRlYnVnIiwiX29uSW5wdXRDbGljayIsImlzIiwiZmFkZU91dCIsImRvY3VtZW50IiwidHJpZ2dlciIsIl9vbklucHV0S2V5RG93biIsImV2ZW50Iiwid2hpY2giLCJibHVyIiwiaW5pdCIsImRvbmUiLCJkZXBlbmRlbmNpZXMiLCJ3aW5kb3ciLCJyZXF1aXJlIiwid3JhcCIsInBhcmVudCIsImFwcGVuZCIsImZpbmQiLCJjc3MiLCJ2aXNpYmlsaXR5IiwicG9zaXRpb24iLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTBCQUEsSUFBSUMsT0FBSixDQUFZQyxNQUFaLENBQ0MsaUJBREQsRUFHQyxDQUNJRixJQUFJRyxNQURSLDBDQUVJSCxJQUFJRyxNQUZSLHlDQUdJSCxJQUFJRyxNQUhSLHFDQUlJSCxJQUFJRyxNQUpSLDhFQUhELEVBVUMsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsZUFBZSxFQUFyQjs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxlQUFlLENBQXJCOztBQUVBOzs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVc7QUFDaEJDLGdCQUFjLEVBREU7QUFFaEJDLGNBQVlYLElBQUlZLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsY0FBcEIsTUFBd0MsSUFBeEMsR0FBK0MsVUFBL0MsR0FBNEQsVUFGeEQ7QUFHaEJDLGdCQUFjZixJQUFJWSxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLE1BQXdDLElBQXhDLEdBQStDLFVBQS9DLEdBQTRELFVBSDFEO0FBSWhCRSxtQkFBaUJoQixJQUFJWSxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxTQUFqQyxDQUpEO0FBS2hCQyxvQkFBa0JuQixJQUFJWSxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixPQUF4QixFQUFpQyxTQUFqQyxDQUxGO0FBTWhCRSxxQkFBbUI7QUFDbEJDLG1CQUFnQixDQURFO0FBRWxCQyxnQkFBYSxJQUZLO0FBR2xCQyxlQUFZLElBSE07QUFJbEJDLFlBQVMsSUFKUztBQUtsQkMsWUFBUyxJQUFJQyxJQUFKLENBQVMsSUFBVCxFQUFlLENBQWYsRUFBa0IsQ0FBbEIsQ0FMUztBQU1sQkMsZ0JBQWEsQ0FDWjNCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGVBQXhCLEVBQXlDLFNBQXpDLENBRFksRUFFWmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGVBQXhCLEVBQXlDLFNBQXpDLENBRlksRUFHWmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGdCQUF4QixFQUEwQyxTQUExQyxDQUhZLEVBSVpsQixJQUFJWSxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixrQkFBeEIsRUFBNEMsU0FBNUMsQ0FKWSxFQUtabEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsaUJBQXhCLEVBQTJDLFNBQTNDLENBTFksRUFNWmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGVBQXhCLEVBQXlDLFNBQXpDLENBTlksRUFPWmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGlCQUF4QixFQUEyQyxTQUEzQyxDQVBZLENBTks7QUFlbEJVLG9CQUFpQixDQUNoQjVCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGdCQUF4QixFQUEwQyxTQUExQyxDQURnQixFQUVoQmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGlCQUF4QixFQUEyQyxTQUEzQyxDQUZnQixFQUdoQmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGNBQXhCLEVBQXdDLFNBQXhDLENBSGdCLEVBSWhCbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsY0FBeEIsRUFBd0MsU0FBeEMsQ0FKZ0IsRUFLaEJsQixJQUFJWSxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixZQUF4QixFQUFzQyxTQUF0QyxDQUxnQixFQU1oQmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGFBQXhCLEVBQXVDLFNBQXZDLENBTmdCLEVBT2hCbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsYUFBeEIsRUFBdUMsU0FBdkMsQ0FQZ0IsRUFRaEJsQixJQUFJWSxJQUFKLENBQVNLLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixlQUF4QixFQUF5QyxTQUF6QyxDQVJnQixFQVNoQmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGtCQUF4QixFQUE0QyxTQUE1QyxDQVRnQixFQVVoQmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGdCQUF4QixFQUEwQyxTQUExQyxDQVZnQixFQVdoQmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGlCQUF4QixFQUEyQyxTQUEzQyxDQVhnQixFQVloQmxCLElBQUlZLElBQUosQ0FBU0ssSUFBVCxDQUFjQyxTQUFkLENBQXdCLGlCQUF4QixFQUEyQyxTQUEzQyxDQVpnQixDQWZDO0FBNkJsQlcsZUFBWSxDQUNYN0IsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsVUFBeEIsRUFBb0MsU0FBcEMsQ0FEVyxFQUVYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsV0FBeEIsRUFBcUMsU0FBckMsQ0FGVyxFQUdYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsUUFBeEIsRUFBa0MsU0FBbEMsQ0FIVyxFQUlYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsUUFBeEIsRUFBa0MsU0FBbEMsQ0FKVyxFQUtYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsTUFBeEIsRUFBZ0MsU0FBaEMsQ0FMVyxFQU1YbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsU0FBakMsQ0FOVyxFQU9YbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsT0FBeEIsRUFBaUMsU0FBakMsQ0FQVyxFQVFYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsU0FBeEIsRUFBbUMsU0FBbkMsQ0FSVyxFQVNYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsWUFBeEIsRUFBc0MsU0FBdEMsQ0FUVyxFQVVYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsVUFBeEIsRUFBb0MsU0FBcEMsQ0FWVyxFQVdYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsV0FBeEIsRUFBcUMsU0FBckMsQ0FYVyxFQVlYbEIsSUFBSVksSUFBSixDQUFTSyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsV0FBeEIsRUFBcUMsU0FBckMsQ0FaVztBQTdCTSxHQU5IO0FBa0RoQlksWUFBVSxvQkFBVztBQUNwQixPQUFJQyxRQUFReEIsTUFBTXlCLFFBQU4sQ0FBZSx5QkFBZixFQUEwQ0MsZUFBMUMsQ0FBMEQsVUFBMUQsQ0FBWjtBQUFBLE9BQ0NDLFFBQVFDLE9BQU9KLE1BQU1HLEtBQWIsRUFBb0JFLE1BQXBCLENBQTJCM0IsU0FBU00sWUFBcEMsQ0FEVDtBQUFBLE9BRUNzQixNQUFNRixPQUFPSixNQUFNTSxHQUFiLEVBQWtCRCxNQUFsQixDQUF5QjNCLFNBQVNNLFlBQWxDLENBRlA7QUFBQSxPQUdDdUIsUUFBU0osVUFBVUcsR0FBWCxHQUFxQkgsS0FBckIsV0FBZ0NHLEdBQWhDLFFBQTJDSCxLQUhwRDtBQUlBM0IsU0FBTWdDLEdBQU4sQ0FBVUQsS0FBVjtBQUNBLEdBeERlO0FBeURoQkUsV0FBUyxtQkFBVztBQUNuQixPQUFJakMsTUFBTWdDLEdBQU4sT0FBZ0IsRUFBcEIsRUFBd0I7QUFDdkJoQyxVQUFNeUIsUUFBTixDQUFlLEdBQWYsRUFBb0JTLE1BQXBCO0FBQ0E7QUFDRDtBQTdEZSxFQUFqQjs7QUFnRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVWxDLEVBQUVtQyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJsQyxRQUFuQixFQUE2QkwsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUYsU0FBUyxFQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUEsVUFBUzBDLHNCQUFULEdBQWtDO0FBQ2pDLE1BQUk7QUFDSCxPQUFJckMsTUFBTWdDLEdBQU4sT0FBZ0IsRUFBcEIsRUFBd0I7QUFDdkI7QUFDQTs7QUFFRCxPQUFNQSxNQUFNaEMsTUFBTWdDLEdBQU4sR0FBWU0sS0FBWixDQUFrQixHQUFsQixDQUFaO0FBQ0EsT0FBTWQsUUFBUSxFQUFkOztBQUVBLE9BQUlRLElBQUlPLE1BQUosS0FBZSxDQUFuQixFQUFzQjtBQUFFO0FBQ3ZCZixVQUFNRyxLQUFOLEdBQWNILE1BQU1NLEdBQU4sR0FBWUYsT0FBT0ksSUFBSSxDQUFKLENBQVAsRUFBZUcsUUFBUTNCLFlBQXZCLEVBQXFDZ0MsTUFBckMsRUFBMUI7QUFDQSxJQUZELE1BRU87QUFBRTtBQUNSaEIsVUFBTUcsS0FBTixHQUFjQyxPQUFPSSxJQUFJLENBQUosQ0FBUCxFQUFlRyxRQUFRM0IsWUFBdkIsRUFBcUNnQyxNQUFyQyxFQUFkO0FBQ0FoQixVQUFNTSxHQUFOLEdBQVlGLE9BQU9JLElBQUksQ0FBSixDQUFQLEVBQWVHLFFBQVEzQixZQUF2QixFQUFxQ2dDLE1BQXJDLEVBQVo7QUFDQTs7QUFFRHhDLFNBQU15QixRQUFOLENBQWUseUJBQWYsRUFBMENDLGVBQTFDLENBQTBELFVBQTFELEVBQXNFRixLQUF0RTtBQUNBLEdBaEJELENBZ0JFLE9BQU9pQixLQUFQLEVBQWM7QUFDZjtBQUNBaEQsT0FBSVksSUFBSixDQUFTcUMsS0FBVCxDQUFlRCxLQUFmLENBQXFCLCtCQUFyQixFQUFzREEsS0FBdEQ7QUFDQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVNFLGFBQVQsR0FBeUI7QUFDeEIsTUFBSSxDQUFDMUMsRUFBRSwwQkFBRixFQUE4QjJDLEVBQTlCLENBQWlDLFVBQWpDLENBQUwsRUFBbUQ7QUFDbEQ1QyxTQUFNeUIsUUFBTixDQUFlLHlCQUFmLEVBQTBDQyxlQUExQyxDQUEwRCxNQUExRDtBQUNBMUIsU0FBTXlCLFFBQU4sQ0FBZSxHQUFmLEVBQW9Cb0IsT0FBcEI7QUFDQTVDLEtBQUU2QyxRQUFGLEVBQVlDLE9BQVosQ0FBb0IsWUFBcEIsRUFIa0QsQ0FHZjtBQUNuQztBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFVBQVNDLGVBQVQsQ0FBeUJDLEtBQXpCLEVBQWdDO0FBQy9CLE1BQUlBLE1BQU1DLEtBQU4sS0FBZ0JwRCxZQUFoQixJQUFnQ21ELE1BQU1DLEtBQU4sS0FBZ0JuRCxZQUFwRCxFQUFrRTtBQUFFO0FBQ25FQyxTQUFNeUIsUUFBTixDQUFlLHlCQUFmLEVBQTBDQyxlQUExQyxDQUEwRCxPQUExRDtBQUNBMUIsU0FBTW1ELElBQU47QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7QUFFQXhELFFBQU95RCxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCO0FBQ0EsTUFBTUMsZUFBZSxDQUNqQjdELElBQUlHLE1BRGEsNkVBQXJCOztBQUlBMkQsU0FBT0MsT0FBUCxDQUFlRixZQUFmLEVBQTZCLFlBQU07QUFDbEN0RCxTQUNFeUQsSUFERixDQUNPLDZDQURQLEVBRUVDLE1BRkYsR0FHRUMsTUFIRixDQUdTLGdDQUhULEVBSUVBLE1BSkYsQ0FJUyw2REFKVCxFQUtFQyxJQUxGLENBS08seUJBTFAsRUFNRWxDLGVBTkYsQ0FNa0JTLE9BTmxCOztBQVFBbkMsU0FBTXlCLFFBQU4sQ0FBZSxRQUFmLEVBQXlCb0MsR0FBekIsQ0FBNkI7QUFDNUJDLGdCQUFZLFFBRGdCLEVBQ047QUFDdEJDLGNBQVUsVUFGa0IsQ0FFUDtBQUZPLElBQTdCOztBQUtBL0QsU0FDRWdFLEVBREYsQ0FDSyxjQURMLEVBQ3FCckIsYUFEckIsRUFFRXFCLEVBRkYsQ0FFSyxTQUZMLEVBRWdCaEIsZUFGaEIsRUFHRWdCLEVBSEYsQ0FHSyxRQUhMLEVBR2UzQixzQkFIZjs7QUFLQWdCO0FBQ0EsR0FwQkQ7QUFxQkEsRUEzQkQ7O0FBNkJBLFFBQU8xRCxNQUFQO0FBRUEsQ0ExTkYiLCJmaWxlIjoiZGF0ZXJhbmdlcGlja2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGRhdGVyYW5nZXBpY2tlci5qcyAyMDE2LTA0LTI4XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIERhdGUgUmFuZ2UgUGlja2VyXHJcbiAqXHJcbiAqIENyZWF0ZXMgYW4gaW5zdGFuY2Ugb2YgdGhlIGpRdWVyeSBVSSBEYXRlcmFuZ2VwaWNrZXIgd2lkZ2V0IHdoaWNoIGVuYWJsZXMgdGhlIHVzZXIgdG8gc2VsZWN0XHJcbiAqIGEgY3VzdG9tIGRhdGUgcmFuZ2UgaW4gdGhlIHNhbWUgZGF0ZXBpY2tlciwgc29tZXRoaW5nIHRoYXQgaXMgbm90IHN1cHBvcnRlZCBieSBqUXVlcnkgVUkuXHJcbiAqXHJcbiAqIFRoaXMgd2lkZ2V0IHJlcXVpcmVzIHRoZSBcImdlbmVyYWxcIiB0cmFuc2xhdGlvbiBzZWN0aW9uIGluIG9yZGVyIHRvIHRyYW5zbGF0ZSB0aGUgZGF5XHJcbiAqIGFuZCBtb250aCBuYW1lcy5cclxuICpcclxuICogIyMjIE9wdGlvbnNcclxuICpcclxuICogWW91IGNhbiBwcm92aWRlIGFsbCB0aGUgb3B0aW9ucyBvZiB0aGUgZm9sbG93aW5nIHNpdGUgYXMgZGF0YSBhdHRyaWJ1dGVzOlxyXG4gKlxyXG4gKiB7QGxpbmsgaHR0cDovL3RhbWJsZS5naXRodWIuaW8vanF1ZXJ5LXVpLWRhdGVyYW5nZXBpY2tlci8jb3B0aW9uc31cclxuICpcclxuICogIyMjIEV4YW1wbGVcclxuICpcclxuICogYGBgaHRtbFxyXG4gKiA8aW5wdXQgdHlwZT1cInRleHRcIiBkYXRhLWpzZS13aWRnZXQ9XCJkYXRlcmFuZ2VwaWNrZXJcIiBkYXRhLWRhdGVyYW5nZXBpY2tlci1kYXRlLWZvcm1hdD1cImRkLm1tLnl5XCIgLz5cclxuICogYGBgXHJcbiAqXHJcbiAqIHtAbGluayBodHRwczovL2dpdGh1Yi5jb20vdGFtYmxlL2pxdWVyeS11aS1kYXRlcmFuZ2VwaWNrZXJ9XHJcbiAqXHJcbiAqIEBtb2R1bGUgSlNFL1dpZGdldHMvZGF0YXJhbmdlcGlja2VyXHJcbiAqIEByZXF1aXJlcyBqUXVlcnlVSS1EYXRlcmFuZ2VwaWNrZXJcclxuICovXHJcbmpzZS53aWRnZXRzLm1vZHVsZShcclxuXHQnZGF0ZXJhbmdlcGlja2VyJyxcclxuXHRcclxuXHRbXHJcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LXVpL2pxdWVyeS11aS5taW4uY3NzYCxcclxuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9qcXVlcnktdWkvanF1ZXJ5LXVpLm1pbi5qc2AsXHJcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvbW9tZW50anMvbW9tZW50Lm1pbi5qc2AsXHJcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvanF1ZXJ5LXVpLWRhdGVyYW5nZXBpY2tlci9qcXVlcnkuY29taXNlby5kYXRlcmFuZ2VwaWNrZXIubWluLmNzc2BcclxuXHRdLFxyXG5cdFxyXG5cdGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFxyXG5cdFx0J3VzZSBzdHJpY3QnO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIFZBUklBQkxFU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRXNjYXBlIEtleSBDb2RlXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge051bWJlcn1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgRVNDX0tFWV9DT0RFID0gMjc7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogVGFiIEtleSBDb2RlXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge051bWJlcn1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgVEFCX0tFWV9DT0RFID0gOTtcclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNb2R1bGUgU2VsZWN0b3JcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge09iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7XHJcblx0XHRcdHByZXNldFJhbmdlczogW10sXHJcblx0XHRcdGRhdGVGb3JtYXQ6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpID09PSAnZGUnID8gJ2RkLm1tLnl5JyA6ICdtbS5kZC55eScsXHJcblx0XHRcdG1vbWVudEZvcm1hdDoganNlLmNvcmUuY29uZmlnLmdldCgnbGFuZ3VhZ2VDb2RlJykgPT09ICdkZScgPyAnREQuTU0uWVknIDogJ01NLkRELllZJyxcclxuXHRcdFx0YXBwbHlCdXR0b25UZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnYXBwbHknLCAnYnV0dG9ucycpLFxyXG5cdFx0XHRjYW5jZWxCdXR0b25UZXh0OiBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnY2xvc2UnLCAnYnV0dG9ucycpLFxyXG5cdFx0XHRkYXRlcGlja2VyT3B0aW9uczoge1xyXG5cdFx0XHRcdG51bWJlck9mTW9udGhzOiAyLFxyXG5cdFx0XHRcdGNoYW5nZU1vbnRoOiB0cnVlLFxyXG5cdFx0XHRcdGNoYW5nZVllYXI6IHRydWUsXHJcblx0XHRcdFx0bWF4RGF0ZTogbnVsbCxcclxuXHRcdFx0XHRtaW5EYXRlOiBuZXcgRGF0ZSgxOTcwLCAxLCAxKSxcclxuXHRcdFx0XHRkYXlOYW1lc01pbjogW1xyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19TVU5EQVlfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19NT05EQVlfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19UVUVTREFZX1NIT1JUJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfV0VETkVTREFZX1NIT1JUJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfVEhVUlNEQVlfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19GUklEQVlfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19TQVRVUkRBWV9TSE9SVCcsICdnZW5lcmFsJylcclxuXHRcdFx0XHRdLFxyXG5cdFx0XHRcdG1vbnRoTmFtZXNTaG9ydDogW1xyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19KQU5VQVJZX1NIT1JUJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfRkVCUlVBUllfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19NQVJDSF9TSE9SVCcsICdnZW5lcmFsJyksXHJcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnX0FQUklMX1NIT1JUJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfTUFZX1NIT1JUJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfSlVORV9TSE9SVCcsICdnZW5lcmFsJyksXHJcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnX0pVTFlfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19BVUdVU1RfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19TRVBURU1CRVJfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19PQ1RPQkVSX1NIT1JUJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfTk9WRU1CRVJfU0hPUlQnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19ERUNFTUJFUl9TSE9SVCcsICdnZW5lcmFsJylcclxuXHRcdFx0XHRdLFxyXG5cdFx0XHRcdG1vbnRoTmFtZXM6IFtcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfSkFOVUFSWScsICdnZW5lcmFsJyksXHJcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnX0ZFQlJVQVJZJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfTUFSQ0gnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19BUFJJTCcsICdnZW5lcmFsJyksXHJcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnX01BWScsICdnZW5lcmFsJyksXHJcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnX0pVTkUnLCAnZ2VuZXJhbCcpLFxyXG5cdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ19KVUxZJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfQVVHVVNUJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfU0VQVEVNQkVSJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfT0NUT0JFUicsICdnZW5lcmFsJyksXHJcblx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnX05PVkVNQkVSJywgJ2dlbmVyYWwnKSxcclxuXHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdfREVDRU1CRVInLCAnZ2VuZXJhbCcpXHJcblx0XHRcdFx0XVxyXG5cdFx0XHR9LFxyXG5cdFx0XHRvbkNoYW5nZTogZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0bGV0IHJhbmdlID0gJHRoaXMuc2libGluZ3MoJy5kYXRlcmFuZ2VwaWNrZXItaGVscGVyJykuZGF0ZXJhbmdlcGlja2VyKCdnZXRSYW5nZScpLFxyXG5cdFx0XHRcdFx0c3RhcnQgPSBtb21lbnQocmFuZ2Uuc3RhcnQpLmZvcm1hdChkZWZhdWx0cy5tb21lbnRGb3JtYXQpLFxyXG5cdFx0XHRcdFx0ZW5kID0gbW9tZW50KHJhbmdlLmVuZCkuZm9ybWF0KGRlZmF1bHRzLm1vbWVudEZvcm1hdCksXHJcblx0XHRcdFx0XHR2YWx1ZSA9IChzdGFydCAhPT0gZW5kKSA/IGAke3N0YXJ0fSAtICR7ZW5kfWAgOiBgJHtzdGFydH1gO1xyXG5cdFx0XHRcdCR0aGlzLnZhbCh2YWx1ZSk7XHJcblx0XHRcdH0sXHJcblx0XHRcdG9uQ2xvc2U6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGlmICgkdGhpcy52YWwoKSA9PT0gJycpIHtcclxuXHRcdFx0XHRcdCR0aGlzLnNpYmxpbmdzKCdpJykuZmFkZUluKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIEZpbmFsIE9wdGlvbnNcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNb2R1bGUgSW5zdGFuY2VcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBGVU5DVElPTlNcclxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIFVwZGF0ZSB0aGUgcmFuZ2Ugb2YgdGhlIGRhdGVyYW5nZXBpY2tlciBpbnN0YW5jZS5cclxuXHRcdCAqXHJcblx0XHQgKiBNb21lbnQgSlMgd2lsbCB0cnkgdG8gcGFyc2UgdGhlIGRhdGUgc3RyaW5nIGFuZCB3aWxsIHByb3ZpZGUgYSB2YWx1ZSBldmVuIGlmIHVzZXIncyB2YWx1ZSBpcyBub3RcclxuXHRcdCAqIGEgY29tcGxldGUgZGF0ZS5cclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX3VwZGF0ZURhdGVyYW5nZXBpY2tlcigpIHtcclxuXHRcdFx0dHJ5IHtcclxuXHRcdFx0XHRpZiAoJHRoaXMudmFsKCkgPT09ICcnKSB7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGNvbnN0IHZhbCA9ICR0aGlzLnZhbCgpLnNwbGl0KCctJyk7XHJcblx0XHRcdFx0Y29uc3QgcmFuZ2UgPSB7fTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAodmFsLmxlbmd0aCA9PT0gMSkgeyAvLyBTaW5nbGUgZGF0ZSB3YXMgc2VsZWN0ZWQuIFxyXG5cdFx0XHRcdFx0cmFuZ2Uuc3RhcnQgPSByYW5nZS5lbmQgPSBtb21lbnQodmFsWzBdLCBvcHRpb25zLm1vbWVudEZvcm1hdCkudG9EYXRlKCk7XHJcblx0XHRcdFx0fSBlbHNlIHsgLy8gRGF0ZSByYW5nZSB3YXMgc2VsZWN0ZWQuXHJcblx0XHRcdFx0XHRyYW5nZS5zdGFydCA9IG1vbWVudCh2YWxbMF0sIG9wdGlvbnMubW9tZW50Rm9ybWF0KS50b0RhdGUoKTtcclxuXHRcdFx0XHRcdHJhbmdlLmVuZCA9IG1vbWVudCh2YWxbMV0sIG9wdGlvbnMubW9tZW50Rm9ybWF0KS50b0RhdGUoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JHRoaXMuc2libGluZ3MoJy5kYXRlcmFuZ2VwaWNrZXItaGVscGVyJykuZGF0ZXJhbmdlcGlja2VyKCdzZXRSYW5nZScsIHJhbmdlKTtcclxuXHRcdFx0fSBjYXRjaCAoZXJyb3IpIHtcclxuXHRcdFx0XHQvLyBDb3VsZCBub3QgcGFyc2UgdGhlIGRhdGUsIGRvIG5vdCB1cGRhdGUgdGhlIGlucHV0IHZhbHVlLlxyXG5cdFx0XHRcdGpzZS5jb3JlLmRlYnVnLmVycm9yKCdEYXRlcmFuZ2VwaWNrZXIgVXBkYXRlIEVycm9yOicsIGVycm9yKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE9uIElucHV0IENsaWNrL0ZvY3VzIEV2ZW50XHJcblx0XHQgKlxyXG5cdFx0ICogRGlzcGxheSB0aGUgZGF0ZXJhbmdlcGlja2VyIG1vZGFsLlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfb25JbnB1dENsaWNrKCkge1xyXG5cdFx0XHRpZiAoISQoJy5jb21pc2VvLWRhdGVyYW5nZXBpY2tlcicpLmlzKCc6dmlzaWJsZScpKSB7XHJcblx0XHRcdFx0JHRoaXMuc2libGluZ3MoJy5kYXRlcmFuZ2VwaWNrZXItaGVscGVyJykuZGF0ZXJhbmdlcGlja2VyKCdvcGVuJyk7XHJcblx0XHRcdFx0JHRoaXMuc2libGluZ3MoJ2knKS5mYWRlT3V0KCk7XHJcblx0XHRcdFx0JChkb2N1bWVudCkudHJpZ2dlcignY2xpY2suc3VtbycpOyAvLyBTdW1vIFNlbGVjdCBjb21wYXRpYmlsaXR5IGZvciB0YWJsZS1maWx0ZXIgcm93cy4gXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBPbiBJbnB1dCBLZXkgRG93blxyXG5cdFx0ICpcclxuXHRcdCAqIElmIHRoZSB1c2UgcHJlc3NlcyB0aGUgZXNjYXBlIG9yIHRhYiBrZXksIGNsb3NlIHRoZSBkYXRlcmFuZ2VwaWNrZXIgbW9kYWwuIE90aGVyd2lzZSBpZiB0aGUgdXNlclxyXG5cdFx0ICogcHJlc3NlcyB0aGUgZW50ZXIgdGhlbiB0aGUgY3VycmVudCB2YWx1ZSBuZWVkcyB0byBiZSBhcHBsaWVkIHRvIGRhdGVyYW5nZXBpY2tlci5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gZXZlbnRcclxuXHRcdCAqL1xyXG5cdFx0ZnVuY3Rpb24gX29uSW5wdXRLZXlEb3duKGV2ZW50KSB7XHJcblx0XHRcdGlmIChldmVudC53aGljaCA9PT0gRVNDX0tFWV9DT0RFIHx8IGV2ZW50LndoaWNoID09PSBUQUJfS0VZX0NPREUpIHsgLy8gQ2xvc2UgdGhlIGRhdGVyYW5nZXBpY2tlciBtb2RhbC4gXHJcblx0XHRcdFx0JHRoaXMuc2libGluZ3MoJy5kYXRlcmFuZ2VwaWNrZXItaGVscGVyJykuZGF0ZXJhbmdlcGlja2VyKCdjbG9zZScpO1xyXG5cdFx0XHRcdCR0aGlzLmJsdXIoKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdC8vIERhdGVyYW5nZXBpY2tlciBuZWVkcyB0byBiZSBsb2FkZWQgYWZ0ZXIganF1ZXJ5IFVJLlxyXG5cdFx0XHRjb25zdCBkZXBlbmRlbmNpZXMgPSBbXHJcblx0XHRcdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS11aS1kYXRlcmFuZ2VwaWNrZXIvanF1ZXJ5LmNvbWlzZW8uZGF0ZXJhbmdlcGlja2VyLm1pbi5qc2BcclxuXHRcdFx0XTtcclxuXHRcdFx0XHJcblx0XHRcdHdpbmRvdy5yZXF1aXJlKGRlcGVuZGVuY2llcywgKCkgPT4ge1xyXG5cdFx0XHRcdCR0aGlzXHJcblx0XHRcdFx0XHQud3JhcCgnPGRpdiBjbGFzcz1cImRhdGVyYW5nZXBpY2tlci13cmFwcGVyXCI+PC9kaXY+JylcclxuXHRcdFx0XHRcdC5wYXJlbnQoKVxyXG5cdFx0XHRcdFx0LmFwcGVuZCgnPGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT4nKVxyXG5cdFx0XHRcdFx0LmFwcGVuZCgnPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJkYXRlcmFuZ2VwaWNrZXItaGVscGVyIGhpZGRlblwiIC8+JylcclxuXHRcdFx0XHRcdC5maW5kKCcuZGF0ZXJhbmdlcGlja2VyLWhlbHBlcicpXHJcblx0XHRcdFx0XHQuZGF0ZXJhbmdlcGlja2VyKG9wdGlvbnMpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdCR0aGlzLnNpYmxpbmdzKCdidXR0b24nKS5jc3Moe1xyXG5cdFx0XHRcdFx0dmlzaWJpbGl0eTogJ2hpZGRlbicsIC8vIEhpZGUgdGhlIGF1dG8tZ2VuZXJhdGVkIGJ1dHRvbi4gXHJcblx0XHRcdFx0XHRwb3NpdGlvbjogJ2Fic29sdXRlJyAvLyBSZW1vdmUgaXQgZnJvbSB0aGUgbm9ybWFsIGZsb3cuXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0JHRoaXNcclxuXHRcdFx0XHRcdC5vbignY2xpY2ssIGZvY3VzJywgX29uSW5wdXRDbGljaylcclxuXHRcdFx0XHRcdC5vbigna2V5ZG93bicsIF9vbklucHV0S2V5RG93bilcclxuXHRcdFx0XHRcdC5vbignY2hhbmdlJywgX3VwZGF0ZURhdGVyYW5nZXBpY2tlcik7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0ZG9uZSgpO1xyXG5cdFx0XHR9KTtcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdHJldHVybiBtb2R1bGU7XHJcblx0XHRcclxuXHR9KTsiXX0=
