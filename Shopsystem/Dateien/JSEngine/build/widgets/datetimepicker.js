'use strict';

/* --------------------------------------------------------------
 datetimepicker.js 2016-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * ## Datetimepicker Widget
 *
 * This widget will convert itself or multiple elements into datetimepicker instances. Check the defaults object for a
 * list of available options.
 *
 * You can also set this module in a container element and provide the "data-datetimepicker-container" attribute and
 * this plugin will initialize all the child elements that have the "datetimepicker" class into datetimepicker widgets.
 *
 * jQuery Datetimepicker Website: {@link http://xdsoft.net/jqplugins/datetimepicker}
 * 
 * ### Options
 *
 * In addition to the options stated below, you could also add many more options shown in the
 * jQuery Datetimepicker documentation.
 *
 * **Format | `data-datetimepicker-format` | String | Optional**
 *
 * Provide the default date format. If no value is provided, the default format will be set
 * to `'d.m.Y H:i'`.
 *
 * **Lang | `data-datetimepicker-lang` | String | Optional**
 *
 * Provide the default language code. If the current language is set to english, the default
 * language code will be set to `'en-GB'`, else the language code will be set to `'de'`.
 *
 * ### Examples
 * 
 * ```html
 * <input type="text" placeholder="##.##.#### ##:##" data-gx-widget="datetimepicker" />
 * ```
 *
 * @module Admin/Widgets/datetimepicker
 * @requires jQuery-Datetimepicker-Plugin
 */
jse.widgets.module('datetimepicker', [jse.source + '/vendor/datetimepicker/jquery.datetimepicker.min.css', jse.source + '/vendor/datetimepicker/jquery.datetimepicker.full.min.js'], function (data) {

	'use strict';

	var
	/**
  * Module Selector
  *
  * @type {object}
  */
	$this = $(this),


	/**
  * Default Module Options
  *
  * @type {object}
  */
	defaults = {
		format: 'd.m.Y H:i',
		lang: jse.core.config.get('languageCode') === 'en' ? 'en-GB' : 'de'
	},


	/**
  * Final Module Options
  * 
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Instance
  *
  * @type {object}
  */
	module = {};

	/**
  * Initialize Module
  *
  * @param {function} done Call this method once the module is initialized.
  */
	module.init = function (done) {
		// Check if the datetimepicker plugin is already loaded. 
		if ($.fn.datetimepicker === undefined) {
			throw new Error('The $.fn.datetimepicker plugin must be loaded before the module is initialized.');
		}

		// Check if the current element is a container and thus need to initialize the children elements. 
		if (options.container !== undefined) {
			$this.find('.datetimepicker').datetimepicker(options);
		} else {
			$this.datetimepicker(options);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGV0aW1lcGlja2VyLmpzIl0sIm5hbWVzIjpbImpzZSIsIndpZGdldHMiLCJtb2R1bGUiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJmb3JtYXQiLCJsYW5nIiwiY29yZSIsImNvbmZpZyIsImdldCIsIm9wdGlvbnMiLCJleHRlbmQiLCJpbml0IiwiZG9uZSIsImZuIiwiZGF0ZXRpbWVwaWNrZXIiLCJ1bmRlZmluZWQiLCJFcnJvciIsImNvbnRhaW5lciIsImZpbmQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0FBLElBQUlDLE9BQUosQ0FBWUMsTUFBWixDQUNDLGdCQURELEVBR0MsQ0FDSUYsSUFBSUcsTUFEUiwyREFFSUgsSUFBSUcsTUFGUiw4REFIRCxFQVFDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNDOzs7OztBQUtBQyxTQUFRQyxFQUFFLElBQUYsQ0FOVDs7O0FBUUM7Ozs7O0FBS0FDLFlBQVc7QUFDVkMsVUFBUSxXQURFO0FBRVZDLFFBQU1ULElBQUlVLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsY0FBcEIsTUFBd0MsSUFBeEMsR0FBK0MsT0FBL0MsR0FBeUQ7QUFGckQsRUFiWjs7O0FBa0JDOzs7OztBQUtBQyxXQUFVUCxFQUFFUSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJQLFFBQW5CLEVBQTZCSCxJQUE3QixDQXZCWDs7O0FBeUJDOzs7OztBQUtBRixVQUFTLEVBOUJWOztBQWdDQTs7Ozs7QUFLQUEsUUFBT2EsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjtBQUNBLE1BQUlWLEVBQUVXLEVBQUYsQ0FBS0MsY0FBTCxLQUF3QkMsU0FBNUIsRUFBdUM7QUFDdEMsU0FBTSxJQUFJQyxLQUFKLENBQVUsaUZBQVYsQ0FBTjtBQUNBOztBQUVEO0FBQ0EsTUFBSVAsUUFBUVEsU0FBUixLQUFzQkYsU0FBMUIsRUFBcUM7QUFDcENkLFNBQU1pQixJQUFOLENBQVcsaUJBQVgsRUFBOEJKLGNBQTlCLENBQTZDTCxPQUE3QztBQUNBLEdBRkQsTUFFTztBQUNOUixTQUFNYSxjQUFOLENBQXFCTCxPQUFyQjtBQUNBOztBQUVERztBQUNBLEVBZEQ7O0FBZ0JBLFFBQU9kLE1BQVA7QUFDQSxDQWxFRiIsImZpbGUiOiJkYXRldGltZXBpY2tlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZGF0ZXRpbWVwaWNrZXIuanMgMjAxNi0wMi0yM1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogIyMgRGF0ZXRpbWVwaWNrZXIgV2lkZ2V0XG4gKlxuICogVGhpcyB3aWRnZXQgd2lsbCBjb252ZXJ0IGl0c2VsZiBvciBtdWx0aXBsZSBlbGVtZW50cyBpbnRvIGRhdGV0aW1lcGlja2VyIGluc3RhbmNlcy4gQ2hlY2sgdGhlIGRlZmF1bHRzIG9iamVjdCBmb3IgYVxuICogbGlzdCBvZiBhdmFpbGFibGUgb3B0aW9ucy5cbiAqXG4gKiBZb3UgY2FuIGFsc28gc2V0IHRoaXMgbW9kdWxlIGluIGEgY29udGFpbmVyIGVsZW1lbnQgYW5kIHByb3ZpZGUgdGhlIFwiZGF0YS1kYXRldGltZXBpY2tlci1jb250YWluZXJcIiBhdHRyaWJ1dGUgYW5kXG4gKiB0aGlzIHBsdWdpbiB3aWxsIGluaXRpYWxpemUgYWxsIHRoZSBjaGlsZCBlbGVtZW50cyB0aGF0IGhhdmUgdGhlIFwiZGF0ZXRpbWVwaWNrZXJcIiBjbGFzcyBpbnRvIGRhdGV0aW1lcGlja2VyIHdpZGdldHMuXG4gKlxuICogalF1ZXJ5IERhdGV0aW1lcGlja2VyIFdlYnNpdGU6IHtAbGluayBodHRwOi8veGRzb2Z0Lm5ldC9qcXBsdWdpbnMvZGF0ZXRpbWVwaWNrZXJ9XG4gKiBcbiAqICMjIyBPcHRpb25zXG4gKlxuICogSW4gYWRkaXRpb24gdG8gdGhlIG9wdGlvbnMgc3RhdGVkIGJlbG93LCB5b3UgY291bGQgYWxzbyBhZGQgbWFueSBtb3JlIG9wdGlvbnMgc2hvd24gaW4gdGhlXG4gKiBqUXVlcnkgRGF0ZXRpbWVwaWNrZXIgZG9jdW1lbnRhdGlvbi5cbiAqXG4gKiAqKkZvcm1hdCB8IGBkYXRhLWRhdGV0aW1lcGlja2VyLWZvcm1hdGAgfCBTdHJpbmcgfCBPcHRpb25hbCoqXG4gKlxuICogUHJvdmlkZSB0aGUgZGVmYXVsdCBkYXRlIGZvcm1hdC4gSWYgbm8gdmFsdWUgaXMgcHJvdmlkZWQsIHRoZSBkZWZhdWx0IGZvcm1hdCB3aWxsIGJlIHNldFxuICogdG8gYCdkLm0uWSBIOmknYC5cbiAqXG4gKiAqKkxhbmcgfCBgZGF0YS1kYXRldGltZXBpY2tlci1sYW5nYCB8IFN0cmluZyB8IE9wdGlvbmFsKipcbiAqXG4gKiBQcm92aWRlIHRoZSBkZWZhdWx0IGxhbmd1YWdlIGNvZGUuIElmIHRoZSBjdXJyZW50IGxhbmd1YWdlIGlzIHNldCB0byBlbmdsaXNoLCB0aGUgZGVmYXVsdFxuICogbGFuZ3VhZ2UgY29kZSB3aWxsIGJlIHNldCB0byBgJ2VuLUdCJ2AsIGVsc2UgdGhlIGxhbmd1YWdlIGNvZGUgd2lsbCBiZSBzZXQgdG8gYCdkZSdgLlxuICpcbiAqICMjIyBFeGFtcGxlc1xuICogXG4gKiBgYGBodG1sXG4gKiA8aW5wdXQgdHlwZT1cInRleHRcIiBwbGFjZWhvbGRlcj1cIiMjLiMjLiMjIyMgIyM6IyNcIiBkYXRhLWd4LXdpZGdldD1cImRhdGV0aW1lcGlja2VyXCIgLz5cbiAqIGBgYFxuICpcbiAqIEBtb2R1bGUgQWRtaW4vV2lkZ2V0cy9kYXRldGltZXBpY2tlclxuICogQHJlcXVpcmVzIGpRdWVyeS1EYXRldGltZXBpY2tlci1QbHVnaW5cbiAqL1xuanNlLndpZGdldHMubW9kdWxlKFxuXHQnZGF0ZXRpbWVwaWNrZXInLFxuXHRcblx0W1xuXHRcdGAke2pzZS5zb3VyY2V9L3ZlbmRvci9kYXRldGltZXBpY2tlci9qcXVlcnkuZGF0ZXRpbWVwaWNrZXIubWluLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGV0aW1lcGlja2VyL2pxdWVyeS5kYXRldGltZXBpY2tlci5mdWxsLm1pbi5qc2Bcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0dmFyXG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdCR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFxuXHRcdFx0LyoqXG5cdFx0XHQgKiBEZWZhdWx0IE1vZHVsZSBPcHRpb25zXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0ZGVmYXVsdHMgPSB7XG5cdFx0XHRcdGZvcm1hdDogJ2QubS5ZIEg6aScsXG5cdFx0XHRcdGxhbmc6IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpID09PSAnZW4nID8gJ2VuLUdCJyA6ICdkZSdcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgTW9kdWxlIE9wdGlvbnNcblx0XHRcdCAqIFxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSksXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIEluc3RhbmNlXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge29iamVjdH1cblx0XHRcdCAqL1xuXHRcdFx0bW9kdWxlID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBNb2R1bGVcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7ZnVuY3Rpb259IGRvbmUgQ2FsbCB0aGlzIG1ldGhvZCBvbmNlIHRoZSBtb2R1bGUgaXMgaW5pdGlhbGl6ZWQuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHQvLyBDaGVjayBpZiB0aGUgZGF0ZXRpbWVwaWNrZXIgcGx1Z2luIGlzIGFscmVhZHkgbG9hZGVkLiBcblx0XHRcdGlmICgkLmZuLmRhdGV0aW1lcGlja2VyID09PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdUaGUgJC5mbi5kYXRldGltZXBpY2tlciBwbHVnaW4gbXVzdCBiZSBsb2FkZWQgYmVmb3JlIHRoZSBtb2R1bGUgaXMgaW5pdGlhbGl6ZWQuJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdC8vIENoZWNrIGlmIHRoZSBjdXJyZW50IGVsZW1lbnQgaXMgYSBjb250YWluZXIgYW5kIHRodXMgbmVlZCB0byBpbml0aWFsaXplIHRoZSBjaGlsZHJlbiBlbGVtZW50cy4gXG5cdFx0XHRpZiAob3B0aW9ucy5jb250YWluZXIgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHQkdGhpcy5maW5kKCcuZGF0ZXRpbWVwaWNrZXInKS5kYXRldGltZXBpY2tlcihvcHRpb25zKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCR0aGlzLmRhdGV0aW1lcGlja2VyKG9wdGlvbnMpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTsgIl19
