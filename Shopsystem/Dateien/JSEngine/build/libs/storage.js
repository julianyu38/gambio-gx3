'use strict';

/* --------------------------------------------------------------
 storage.js 2016-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/* globals Storage */

jse.libs.storage = jse.libs.storage || {};

/**
 * ## Browser Storage API Library
 *
 * This library handles the HTML storage functionality. You can either store information in the session or the local 
 * storage of the browser.
 * 
 * @deprecated Will be removed with JS Engine v1.7.
 *
 * @module JSE/Libs/storage
 * @exports jse.libs.storage
 * @ignore
 */
(function (exports) {

	'use strict';

	/**
  * JavaScript Storage Object
  * 
  * @type {boolean}
  */

	var webStorage = Storage !== undefined ? true : false;

	/**
  * Stores a value in the browser storage.
  *
  * @param {object} store Storage handler object.
  * @param {boolean} overwrite Whether to overwrite an existing storage value.
  * @param {string} value String defining the value key name to be stored.
  * @param {object} dataset Contains the information to be stored.
  * @param {number} userId User id will be used to identify stored information of a specific user.
  *
  * @return {boolean} Returns the operation result.
  * 
  * @private
  */
	var _store = function _store(store, overwrite, value, dataset, userId) {

		var dataCache = null,
		    result = null;

		if (webStorage) {
			dataCache = store.getItem('user_' + userId);
			dataCache = dataCache || '{}';
			dataCache = $.parseJSON(dataCache);

			if (overwrite || dataCache[value] === undefined) {
				dataCache[value] = dataset;
			} else {
				dataCache[value] = $.extend({}, dataCache[value], dataset);
			}

			result = JSON.stringify(dataCache);
			store.setItem('user_' + userId, result);
			return true;
		}
		return false;
	};

	/**
  * Restores data from the browser storage.
  *
  * @param {object} store Storage handler object.
  * @param {string} value Value key name to be retrieved.
  * @param {number} userId User id that owns the value.
  *
  * @return {object} Returns the value if exists or an empty object if not.
  * 
  * @private
  */
	var _restore = function _restore(store, value, userId) {

		var dataCache = null;

		if (webStorage) {
			dataCache = store.getItem('user_' + userId);
			dataCache = dataCache || '{}';
			dataCache = $.parseJSON(dataCache);
			return dataCache[value] || {};
		}
		return {};
	};

	/**
  * Stores data in the browser storage.
  *
  * @param {array} destinations Array containing where to store the data (session, local).
  * @param {object} dataset Data to be stored.
  * @param {boolean} overwrite Whether to overwrite existing values.
  *
  * @return {object} Returns a promise object.
  */
	exports.store = function (destinations, dataset, overwrite) {

		var userID = $('body').data().userId,
		    resultObject = {},
		    promises = [];

		$.each(destinations, function (dest, value) {
			var localDeferred = $.Deferred();
			promises.push(localDeferred);

			switch (dest) {
				case 'session':
					resultObject.session = _store(sessionStorage, overwrite, value, dataset, userID);
					localDeferred.resolve(resultObject);
					break;
				case 'local':
					resultObject.local = _store(localStorage, overwrite, value, dataset, userID);
					localDeferred.resolve(resultObject);
					break;
				default:
					break;
			}
		});

		return $.when.apply(undefined, promises).promise();
	};

	/**
  * Restores data from the browser storage.
  *
  * @param {array} sources Defines the source of the data to be retrieved (session, local).
  *
  * @return {object} Returns a promise object.
  */
	exports.restore = function (sources) {
		var userID = $('body').data().userId,
		    resultObject = {},
		    promises = [];

		$.each(sources, function (src, value) {
			var localDeferred = $.Deferred();
			promises.push(localDeferred);

			switch (src) {
				case 'session':
					resultObject.session = _restore(sessionStorage, value, userID);
					localDeferred.resolve(resultObject);
					break;
				case 'local':
					resultObject.local = _restore(localStorage, value, userID);
					localDeferred.resolve(resultObject);
					break;
				default:
					break;
			}
		});

		return $.when.apply(undefined, promises).then(function (result) {
			return $.extend(true, {}, result.local || {}, result.session || {}, result.server || {});
		}).promise();
	};
})(jse.libs.storage);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0b3JhZ2UuanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsInN0b3JhZ2UiLCJleHBvcnRzIiwid2ViU3RvcmFnZSIsIlN0b3JhZ2UiLCJ1bmRlZmluZWQiLCJfc3RvcmUiLCJzdG9yZSIsIm92ZXJ3cml0ZSIsInZhbHVlIiwiZGF0YXNldCIsInVzZXJJZCIsImRhdGFDYWNoZSIsInJlc3VsdCIsImdldEl0ZW0iLCIkIiwicGFyc2VKU09OIiwiZXh0ZW5kIiwiSlNPTiIsInN0cmluZ2lmeSIsInNldEl0ZW0iLCJfcmVzdG9yZSIsImRlc3RpbmF0aW9ucyIsInVzZXJJRCIsImRhdGEiLCJyZXN1bHRPYmplY3QiLCJwcm9taXNlcyIsImVhY2giLCJkZXN0IiwibG9jYWxEZWZlcnJlZCIsIkRlZmVycmVkIiwicHVzaCIsInNlc3Npb24iLCJzZXNzaW9uU3RvcmFnZSIsInJlc29sdmUiLCJsb2NhbCIsImxvY2FsU3RvcmFnZSIsIndoZW4iLCJhcHBseSIsInByb21pc2UiLCJyZXN0b3JlIiwic291cmNlcyIsInNyYyIsInRoZW4iLCJzZXJ2ZXIiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7QUFFQUEsSUFBSUMsSUFBSixDQUFTQyxPQUFULEdBQW1CRixJQUFJQyxJQUFKLENBQVNDLE9BQVQsSUFBb0IsRUFBdkM7O0FBRUE7Ozs7Ozs7Ozs7OztBQVlDLFdBQVVDLE9BQVYsRUFBbUI7O0FBRW5COztBQUVBOzs7Ozs7QUFLQSxLQUFJQyxhQUFjQyxZQUFZQyxTQUFiLEdBQTBCLElBQTFCLEdBQWlDLEtBQWxEOztBQUVBOzs7Ozs7Ozs7Ozs7O0FBYUEsS0FBSUMsU0FBUyxTQUFUQSxNQUFTLENBQVVDLEtBQVYsRUFBaUJDLFNBQWpCLEVBQTRCQyxLQUE1QixFQUFtQ0MsT0FBbkMsRUFBNENDLE1BQTVDLEVBQW9EOztBQUVoRSxNQUFJQyxZQUFZLElBQWhCO0FBQUEsTUFDQ0MsU0FBUyxJQURWOztBQUdBLE1BQUlWLFVBQUosRUFBZ0I7QUFDZlMsZUFBWUwsTUFBTU8sT0FBTixDQUFjLFVBQVVILE1BQXhCLENBQVo7QUFDQUMsZUFBWUEsYUFBYSxJQUF6QjtBQUNBQSxlQUFZRyxFQUFFQyxTQUFGLENBQVlKLFNBQVosQ0FBWjs7QUFFQSxPQUFJSixhQUFhSSxVQUFVSCxLQUFWLE1BQXFCSixTQUF0QyxFQUFpRDtBQUNoRE8sY0FBVUgsS0FBVixJQUFtQkMsT0FBbkI7QUFDQSxJQUZELE1BRU87QUFDTkUsY0FBVUgsS0FBVixJQUFtQk0sRUFBRUUsTUFBRixDQUFTLEVBQVQsRUFBYUwsVUFBVUgsS0FBVixDQUFiLEVBQStCQyxPQUEvQixDQUFuQjtBQUNBOztBQUVERyxZQUFTSyxLQUFLQyxTQUFMLENBQWVQLFNBQWYsQ0FBVDtBQUNBTCxTQUFNYSxPQUFOLENBQWMsVUFBVVQsTUFBeEIsRUFBZ0NFLE1BQWhDO0FBQ0EsVUFBTyxJQUFQO0FBQ0E7QUFDRCxTQUFPLEtBQVA7QUFDQSxFQXJCRDs7QUF1QkE7Ozs7Ozs7Ozs7O0FBV0EsS0FBSVEsV0FBVyxTQUFYQSxRQUFXLENBQVVkLEtBQVYsRUFBaUJFLEtBQWpCLEVBQXdCRSxNQUF4QixFQUFnQzs7QUFFOUMsTUFBSUMsWUFBWSxJQUFoQjs7QUFFQSxNQUFJVCxVQUFKLEVBQWdCO0FBQ2ZTLGVBQVlMLE1BQU1PLE9BQU4sQ0FBYyxVQUFVSCxNQUF4QixDQUFaO0FBQ0FDLGVBQVlBLGFBQWEsSUFBekI7QUFDQUEsZUFBWUcsRUFBRUMsU0FBRixDQUFZSixTQUFaLENBQVo7QUFDQSxVQUFPQSxVQUFVSCxLQUFWLEtBQW9CLEVBQTNCO0FBQ0E7QUFDRCxTQUFPLEVBQVA7QUFDQSxFQVhEOztBQWFBOzs7Ozs7Ozs7QUFTQVAsU0FBUUssS0FBUixHQUFnQixVQUFVZSxZQUFWLEVBQXdCWixPQUF4QixFQUFpQ0YsU0FBakMsRUFBNEM7O0FBRTNELE1BQUllLFNBQVNSLEVBQUUsTUFBRixFQUFVUyxJQUFWLEdBQWlCYixNQUE5QjtBQUFBLE1BQ0NjLGVBQWUsRUFEaEI7QUFBQSxNQUVDQyxXQUFXLEVBRlo7O0FBSUFYLElBQUVZLElBQUYsQ0FBT0wsWUFBUCxFQUFxQixVQUFVTSxJQUFWLEVBQWdCbkIsS0FBaEIsRUFBdUI7QUFDM0MsT0FBSW9CLGdCQUFnQmQsRUFBRWUsUUFBRixFQUFwQjtBQUNBSixZQUFTSyxJQUFULENBQWNGLGFBQWQ7O0FBRUEsV0FBUUQsSUFBUjtBQUNDLFNBQUssU0FBTDtBQUNDSCxrQkFBYU8sT0FBYixHQUF1QjFCLE9BQU8yQixjQUFQLEVBQXVCekIsU0FBdkIsRUFBa0NDLEtBQWxDLEVBQXlDQyxPQUF6QyxFQUFrRGEsTUFBbEQsQ0FBdkI7QUFDQU0sbUJBQWNLLE9BQWQsQ0FBc0JULFlBQXRCO0FBQ0E7QUFDRCxTQUFLLE9BQUw7QUFDQ0Esa0JBQWFVLEtBQWIsR0FBcUI3QixPQUFPOEIsWUFBUCxFQUFxQjVCLFNBQXJCLEVBQWdDQyxLQUFoQyxFQUF1Q0MsT0FBdkMsRUFBZ0RhLE1BQWhELENBQXJCO0FBQ0FNLG1CQUFjSyxPQUFkLENBQXNCVCxZQUF0QjtBQUNBO0FBQ0Q7QUFDQztBQVZGO0FBWUEsR0FoQkQ7O0FBa0JBLFNBQU9WLEVBQUVzQixJQUFGLENBQU9DLEtBQVAsQ0FBYWpDLFNBQWIsRUFBd0JxQixRQUF4QixFQUFrQ2EsT0FBbEMsRUFBUDtBQUVBLEVBMUJEOztBQTRCQTs7Ozs7OztBQU9BckMsU0FBUXNDLE9BQVIsR0FBa0IsVUFBVUMsT0FBVixFQUFtQjtBQUNwQyxNQUFJbEIsU0FBU1IsRUFBRSxNQUFGLEVBQVVTLElBQVYsR0FBaUJiLE1BQTlCO0FBQUEsTUFDQ2MsZUFBZSxFQURoQjtBQUFBLE1BRUNDLFdBQVcsRUFGWjs7QUFJQVgsSUFBRVksSUFBRixDQUFPYyxPQUFQLEVBQWdCLFVBQVVDLEdBQVYsRUFBZWpDLEtBQWYsRUFBc0I7QUFDckMsT0FBSW9CLGdCQUFnQmQsRUFBRWUsUUFBRixFQUFwQjtBQUNBSixZQUFTSyxJQUFULENBQWNGLGFBQWQ7O0FBRUEsV0FBUWEsR0FBUjtBQUNDLFNBQUssU0FBTDtBQUNDakIsa0JBQWFPLE9BQWIsR0FBdUJYLFNBQVNZLGNBQVQsRUFBeUJ4QixLQUF6QixFQUFnQ2MsTUFBaEMsQ0FBdkI7QUFDQU0sbUJBQWNLLE9BQWQsQ0FBc0JULFlBQXRCO0FBQ0E7QUFDRCxTQUFLLE9BQUw7QUFDQ0Esa0JBQWFVLEtBQWIsR0FBcUJkLFNBQVNlLFlBQVQsRUFBdUIzQixLQUF2QixFQUE4QmMsTUFBOUIsQ0FBckI7QUFDQU0sbUJBQWNLLE9BQWQsQ0FBc0JULFlBQXRCO0FBQ0E7QUFDRDtBQUNDO0FBVkY7QUFZQSxHQWhCRDs7QUFrQkEsU0FBT1YsRUFBRXNCLElBQUYsQ0FDTEMsS0FESyxDQUNDakMsU0FERCxFQUNZcUIsUUFEWixFQUVMaUIsSUFGSyxDQUVBLFVBQVU5QixNQUFWLEVBQWtCO0FBQ2pCLFVBQU9FLEVBQUVFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkosT0FBT3NCLEtBQVAsSUFBZ0IsRUFBbkMsRUFBdUN0QixPQUFPbUIsT0FBUCxJQUFrQixFQUF6RCxFQUE2RG5CLE9BQU8rQixNQUFQLElBQWlCLEVBQTlFLENBQVA7QUFDQSxHQUpELEVBS0xMLE9BTEssRUFBUDtBQU1BLEVBN0JEO0FBK0JBLENBbEpBLEVBa0pDeEMsSUFBSUMsSUFBSixDQUFTQyxPQWxKVixDQUFEIiwiZmlsZSI6InN0b3JhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHN0b3JhZ2UuanMgMjAxNi0wMi0yM1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qIGdsb2JhbHMgU3RvcmFnZSAqL1xuXG5qc2UubGlicy5zdG9yYWdlID0ganNlLmxpYnMuc3RvcmFnZSB8fCB7fTtcblxuLyoqXG4gKiAjIyBCcm93c2VyIFN0b3JhZ2UgQVBJIExpYnJhcnlcbiAqXG4gKiBUaGlzIGxpYnJhcnkgaGFuZGxlcyB0aGUgSFRNTCBzdG9yYWdlIGZ1bmN0aW9uYWxpdHkuIFlvdSBjYW4gZWl0aGVyIHN0b3JlIGluZm9ybWF0aW9uIGluIHRoZSBzZXNzaW9uIG9yIHRoZSBsb2NhbCBcbiAqIHN0b3JhZ2Ugb2YgdGhlIGJyb3dzZXIuXG4gKiBcbiAqIEBkZXByZWNhdGVkIFdpbGwgYmUgcmVtb3ZlZCB3aXRoIEpTIEVuZ2luZSB2MS43LlxuICpcbiAqIEBtb2R1bGUgSlNFL0xpYnMvc3RvcmFnZVxuICogQGV4cG9ydHMganNlLmxpYnMuc3RvcmFnZVxuICogQGlnbm9yZVxuICovXG4oZnVuY3Rpb24gKGV4cG9ydHMpIHtcblxuXHQndXNlIHN0cmljdCc7XG5cblx0LyoqXG5cdCAqIEphdmFTY3JpcHQgU3RvcmFnZSBPYmplY3Rcblx0ICogXG5cdCAqIEB0eXBlIHtib29sZWFufVxuXHQgKi9cblx0dmFyIHdlYlN0b3JhZ2UgPSAoU3RvcmFnZSAhPT0gdW5kZWZpbmVkKSA/IHRydWUgOiBmYWxzZTtcblxuXHQvKipcblx0ICogU3RvcmVzIGEgdmFsdWUgaW4gdGhlIGJyb3dzZXIgc3RvcmFnZS5cblx0ICpcblx0ICogQHBhcmFtIHtvYmplY3R9IHN0b3JlIFN0b3JhZ2UgaGFuZGxlciBvYmplY3QuXG5cdCAqIEBwYXJhbSB7Ym9vbGVhbn0gb3ZlcndyaXRlIFdoZXRoZXIgdG8gb3ZlcndyaXRlIGFuIGV4aXN0aW5nIHN0b3JhZ2UgdmFsdWUuXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZSBTdHJpbmcgZGVmaW5pbmcgdGhlIHZhbHVlIGtleSBuYW1lIHRvIGJlIHN0b3JlZC5cblx0ICogQHBhcmFtIHtvYmplY3R9IGRhdGFzZXQgQ29udGFpbnMgdGhlIGluZm9ybWF0aW9uIHRvIGJlIHN0b3JlZC5cblx0ICogQHBhcmFtIHtudW1iZXJ9IHVzZXJJZCBVc2VyIGlkIHdpbGwgYmUgdXNlZCB0byBpZGVudGlmeSBzdG9yZWQgaW5mb3JtYXRpb24gb2YgYSBzcGVjaWZpYyB1c2VyLlxuXHQgKlxuXHQgKiBAcmV0dXJuIHtib29sZWFufSBSZXR1cm5zIHRoZSBvcGVyYXRpb24gcmVzdWx0LlxuXHQgKiBcblx0ICogQHByaXZhdGVcblx0ICovXG5cdHZhciBfc3RvcmUgPSBmdW5jdGlvbiAoc3RvcmUsIG92ZXJ3cml0ZSwgdmFsdWUsIGRhdGFzZXQsIHVzZXJJZCkge1xuXG5cdFx0dmFyIGRhdGFDYWNoZSA9IG51bGwsXG5cdFx0XHRyZXN1bHQgPSBudWxsO1xuXG5cdFx0aWYgKHdlYlN0b3JhZ2UpIHtcblx0XHRcdGRhdGFDYWNoZSA9IHN0b3JlLmdldEl0ZW0oJ3VzZXJfJyArIHVzZXJJZCk7XG5cdFx0XHRkYXRhQ2FjaGUgPSBkYXRhQ2FjaGUgfHwgJ3t9Jztcblx0XHRcdGRhdGFDYWNoZSA9ICQucGFyc2VKU09OKGRhdGFDYWNoZSk7XG5cblx0XHRcdGlmIChvdmVyd3JpdGUgfHwgZGF0YUNhY2hlW3ZhbHVlXSA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdGRhdGFDYWNoZVt2YWx1ZV0gPSBkYXRhc2V0O1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0ZGF0YUNhY2hlW3ZhbHVlXSA9ICQuZXh0ZW5kKHt9LCBkYXRhQ2FjaGVbdmFsdWVdLCBkYXRhc2V0KTtcblx0XHRcdH1cblxuXHRcdFx0cmVzdWx0ID0gSlNPTi5zdHJpbmdpZnkoZGF0YUNhY2hlKTtcblx0XHRcdHN0b3JlLnNldEl0ZW0oJ3VzZXJfJyArIHVzZXJJZCwgcmVzdWx0KTtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0XHRyZXR1cm4gZmFsc2U7XG5cdH07XG5cblx0LyoqXG5cdCAqIFJlc3RvcmVzIGRhdGEgZnJvbSB0aGUgYnJvd3NlciBzdG9yYWdlLlxuXHQgKlxuXHQgKiBAcGFyYW0ge29iamVjdH0gc3RvcmUgU3RvcmFnZSBoYW5kbGVyIG9iamVjdC5cblx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlIFZhbHVlIGtleSBuYW1lIHRvIGJlIHJldHJpZXZlZC5cblx0ICogQHBhcmFtIHtudW1iZXJ9IHVzZXJJZCBVc2VyIGlkIHRoYXQgb3ducyB0aGUgdmFsdWUuXG5cdCAqXG5cdCAqIEByZXR1cm4ge29iamVjdH0gUmV0dXJucyB0aGUgdmFsdWUgaWYgZXhpc3RzIG9yIGFuIGVtcHR5IG9iamVjdCBpZiBub3QuXG5cdCAqIFxuXHQgKiBAcHJpdmF0ZVxuXHQgKi9cblx0dmFyIF9yZXN0b3JlID0gZnVuY3Rpb24gKHN0b3JlLCB2YWx1ZSwgdXNlcklkKSB7XG5cblx0XHR2YXIgZGF0YUNhY2hlID0gbnVsbDtcblxuXHRcdGlmICh3ZWJTdG9yYWdlKSB7XG5cdFx0XHRkYXRhQ2FjaGUgPSBzdG9yZS5nZXRJdGVtKCd1c2VyXycgKyB1c2VySWQpO1xuXHRcdFx0ZGF0YUNhY2hlID0gZGF0YUNhY2hlIHx8ICd7fSc7XG5cdFx0XHRkYXRhQ2FjaGUgPSAkLnBhcnNlSlNPTihkYXRhQ2FjaGUpO1xuXHRcdFx0cmV0dXJuIGRhdGFDYWNoZVt2YWx1ZV0gfHwge307XG5cdFx0fVxuXHRcdHJldHVybiB7fTtcblx0fTtcblxuXHQvKipcblx0ICogU3RvcmVzIGRhdGEgaW4gdGhlIGJyb3dzZXIgc3RvcmFnZS5cblx0ICpcblx0ICogQHBhcmFtIHthcnJheX0gZGVzdGluYXRpb25zIEFycmF5IGNvbnRhaW5pbmcgd2hlcmUgdG8gc3RvcmUgdGhlIGRhdGEgKHNlc3Npb24sIGxvY2FsKS5cblx0ICogQHBhcmFtIHtvYmplY3R9IGRhdGFzZXQgRGF0YSB0byBiZSBzdG9yZWQuXG5cdCAqIEBwYXJhbSB7Ym9vbGVhbn0gb3ZlcndyaXRlIFdoZXRoZXIgdG8gb3ZlcndyaXRlIGV4aXN0aW5nIHZhbHVlcy5cblx0ICpcblx0ICogQHJldHVybiB7b2JqZWN0fSBSZXR1cm5zIGEgcHJvbWlzZSBvYmplY3QuXG5cdCAqL1xuXHRleHBvcnRzLnN0b3JlID0gZnVuY3Rpb24gKGRlc3RpbmF0aW9ucywgZGF0YXNldCwgb3ZlcndyaXRlKSB7XG5cblx0XHR2YXIgdXNlcklEID0gJCgnYm9keScpLmRhdGEoKS51c2VySWQsXG5cdFx0XHRyZXN1bHRPYmplY3QgPSB7fSxcblx0XHRcdHByb21pc2VzID0gW107XG5cblx0XHQkLmVhY2goZGVzdGluYXRpb25zLCBmdW5jdGlvbiAoZGVzdCwgdmFsdWUpIHtcblx0XHRcdHZhciBsb2NhbERlZmVycmVkID0gJC5EZWZlcnJlZCgpO1xuXHRcdFx0cHJvbWlzZXMucHVzaChsb2NhbERlZmVycmVkKTtcblxuXHRcdFx0c3dpdGNoIChkZXN0KSB7XG5cdFx0XHRcdGNhc2UgJ3Nlc3Npb24nOlxuXHRcdFx0XHRcdHJlc3VsdE9iamVjdC5zZXNzaW9uID0gX3N0b3JlKHNlc3Npb25TdG9yYWdlLCBvdmVyd3JpdGUsIHZhbHVlLCBkYXRhc2V0LCB1c2VySUQpO1xuXHRcdFx0XHRcdGxvY2FsRGVmZXJyZWQucmVzb2x2ZShyZXN1bHRPYmplY3QpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdsb2NhbCc6XG5cdFx0XHRcdFx0cmVzdWx0T2JqZWN0LmxvY2FsID0gX3N0b3JlKGxvY2FsU3RvcmFnZSwgb3ZlcndyaXRlLCB2YWx1ZSwgZGF0YXNldCwgdXNlcklEKTtcblx0XHRcdFx0XHRsb2NhbERlZmVycmVkLnJlc29sdmUocmVzdWx0T2JqZWN0KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdH1cblx0XHR9KTtcblxuXHRcdHJldHVybiAkLndoZW4uYXBwbHkodW5kZWZpbmVkLCBwcm9taXNlcykucHJvbWlzZSgpO1xuXG5cdH07XG5cblx0LyoqXG5cdCAqIFJlc3RvcmVzIGRhdGEgZnJvbSB0aGUgYnJvd3NlciBzdG9yYWdlLlxuXHQgKlxuXHQgKiBAcGFyYW0ge2FycmF5fSBzb3VyY2VzIERlZmluZXMgdGhlIHNvdXJjZSBvZiB0aGUgZGF0YSB0byBiZSByZXRyaWV2ZWQgKHNlc3Npb24sIGxvY2FsKS5cblx0ICpcblx0ICogQHJldHVybiB7b2JqZWN0fSBSZXR1cm5zIGEgcHJvbWlzZSBvYmplY3QuXG5cdCAqL1xuXHRleHBvcnRzLnJlc3RvcmUgPSBmdW5jdGlvbiAoc291cmNlcykge1xuXHRcdHZhciB1c2VySUQgPSAkKCdib2R5JykuZGF0YSgpLnVzZXJJZCxcblx0XHRcdHJlc3VsdE9iamVjdCA9IHt9LFxuXHRcdFx0cHJvbWlzZXMgPSBbXTtcblxuXHRcdCQuZWFjaChzb3VyY2VzLCBmdW5jdGlvbiAoc3JjLCB2YWx1ZSkge1xuXHRcdFx0dmFyIGxvY2FsRGVmZXJyZWQgPSAkLkRlZmVycmVkKCk7XG5cdFx0XHRwcm9taXNlcy5wdXNoKGxvY2FsRGVmZXJyZWQpO1xuXG5cdFx0XHRzd2l0Y2ggKHNyYykge1xuXHRcdFx0XHRjYXNlICdzZXNzaW9uJzpcblx0XHRcdFx0XHRyZXN1bHRPYmplY3Quc2Vzc2lvbiA9IF9yZXN0b3JlKHNlc3Npb25TdG9yYWdlLCB2YWx1ZSwgdXNlcklEKTtcblx0XHRcdFx0XHRsb2NhbERlZmVycmVkLnJlc29sdmUocmVzdWx0T2JqZWN0KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnbG9jYWwnOlxuXHRcdFx0XHRcdHJlc3VsdE9iamVjdC5sb2NhbCA9IF9yZXN0b3JlKGxvY2FsU3RvcmFnZSwgdmFsdWUsIHVzZXJJRCk7XG5cdFx0XHRcdFx0bG9jYWxEZWZlcnJlZC5yZXNvbHZlKHJlc3VsdE9iamVjdCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gJC53aGVuXG5cdFx0XHQuYXBwbHkodW5kZWZpbmVkLCBwcm9taXNlcylcblx0XHRcdC50aGVuKGZ1bmN0aW9uIChyZXN1bHQpIHtcblx0XHRcdFx0ICAgICAgcmV0dXJuICQuZXh0ZW5kKHRydWUsIHt9LCByZXN1bHQubG9jYWwgfHwge30sIHJlc3VsdC5zZXNzaW9uIHx8IHt9LCByZXN1bHQuc2VydmVyIHx8IHt9KTtcblx0XHRcdCAgICAgIH0pXG5cdFx0XHQucHJvbWlzZSgpO1xuXHR9O1xuXG59KGpzZS5saWJzLnN0b3JhZ2UpKTtcbiJdfQ==
