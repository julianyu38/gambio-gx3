'use strict';

/* --------------------------------------------------------------
 configuration.js 2016-12-01
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.configuration = jse.libs.configuration || {};

/**
 * ## Configurations Library
 *
 * This library makes it possible to receive shop configuration values.
 * 
 * @module JSE/Libs/datatable
 * @exports jse.libs.datatable
 */
(function (exports) {
	'use strict';

	/**
  * @type {String}
  */

	var pageToken = jse.core.config.get('pageToken');

	/**
  * @type {String}
  */
	var baseUrl = jse.core.config.get('appUrl') + '/shop.php?do=JsConfiguration';

	/**
  * Get the configuration value by the provided key.
  *
  * @param key Configuration key.
  *
  * @returns {Promise} The promise will be resolve with the configuration value.
  */
	exports.get = function (key) {
		return new Promise(function (resolve, reject) {
			var url = baseUrl + '/Get';
			$.ajax({ url: url, data: { key: key, pageToken: pageToken } }).done(function (response) {
				return resolve(response);
			}).fail(function (error) {
				return reject(error);
			});
		});
	};
})(jse.libs.configuration);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbmZpZ3VyYXRpb24uanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsImNvbmZpZ3VyYXRpb24iLCJleHBvcnRzIiwicGFnZVRva2VuIiwiY29yZSIsImNvbmZpZyIsImdldCIsImJhc2VVcmwiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsInVybCIsIiQiLCJhamF4IiwiZGF0YSIsImtleSIsImRvbmUiLCJyZXNwb25zZSIsImZhaWwiLCJlcnJvciJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxJQUFJQyxJQUFKLENBQVNDLGFBQVQsR0FBeUJGLElBQUlDLElBQUosQ0FBU0MsYUFBVCxJQUEwQixFQUFuRDs7QUFFQTs7Ozs7Ozs7QUFRQSxDQUFDLFVBQVNDLE9BQVQsRUFBa0I7QUFDbEI7O0FBRUE7Ozs7QUFHQSxLQUFNQyxZQUFZSixJQUFJSyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFdBQXBCLENBQWxCOztBQUVBOzs7QUFHQSxLQUFNQyxVQUFhUixJQUFJSyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFFBQXBCLENBQWIsaUNBQU47O0FBRUE7Ozs7Ozs7QUFPQUosU0FBUUksR0FBUixHQUFjLGVBQU87QUFDcEIsU0FBTyxJQUFJRSxPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3ZDLE9BQU1DLE1BQVNKLE9BQVQsU0FBTjtBQUNBSyxLQUFFQyxJQUFGLENBQU8sRUFBRUYsUUFBRixFQUFRRyxNQUFNLEVBQUVDLFFBQUYsRUFBT1osb0JBQVAsRUFBZCxFQUFQLEVBQ0VhLElBREYsQ0FDTztBQUFBLFdBQVlQLFFBQVFRLFFBQVIsQ0FBWjtBQUFBLElBRFAsRUFFRUMsSUFGRixDQUVPO0FBQUEsV0FBU1IsT0FBT1MsS0FBUCxDQUFUO0FBQUEsSUFGUDtBQUdBLEdBTE0sQ0FBUDtBQU1BLEVBUEQ7QUFTQSxDQTdCRCxFQTZCR3BCLElBQUlDLElBQUosQ0FBU0MsYUE3QloiLCJmaWxlIjoiY29uZmlndXJhdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBjb25maWd1cmF0aW9uLmpzIDIwMTYtMTItMDFcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG5qc2UubGlicy5jb25maWd1cmF0aW9uID0ganNlLmxpYnMuY29uZmlndXJhdGlvbiB8fCB7fTtcclxuXHJcbi8qKlxyXG4gKiAjIyBDb25maWd1cmF0aW9ucyBMaWJyYXJ5XHJcbiAqXHJcbiAqIFRoaXMgbGlicmFyeSBtYWtlcyBpdCBwb3NzaWJsZSB0byByZWNlaXZlIHNob3AgY29uZmlndXJhdGlvbiB2YWx1ZXMuXHJcbiAqIFxyXG4gKiBAbW9kdWxlIEpTRS9MaWJzL2RhdGF0YWJsZVxyXG4gKiBAZXhwb3J0cyBqc2UubGlicy5kYXRhdGFibGVcclxuICovXHJcbihmdW5jdGlvbihleHBvcnRzKSB7XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEB0eXBlIHtTdHJpbmd9XHJcblx0ICovXHJcblx0Y29uc3QgcGFnZVRva2VuID0ganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJyk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogQHR5cGUge1N0cmluZ31cclxuXHQgKi9cclxuXHRjb25zdCBiYXNlVXJsID0gYCR7anNlLmNvcmUuY29uZmlnLmdldCgnYXBwVXJsJyl9L3Nob3AucGhwP2RvPUpzQ29uZmlndXJhdGlvbmA7XHJcblx0XHJcblx0LyoqXHJcblx0ICogR2V0IHRoZSBjb25maWd1cmF0aW9uIHZhbHVlIGJ5IHRoZSBwcm92aWRlZCBrZXkuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ga2V5IENvbmZpZ3VyYXRpb24ga2V5LlxyXG5cdCAqXHJcblx0ICogQHJldHVybnMge1Byb21pc2V9IFRoZSBwcm9taXNlIHdpbGwgYmUgcmVzb2x2ZSB3aXRoIHRoZSBjb25maWd1cmF0aW9uIHZhbHVlLlxyXG5cdCAqL1xyXG5cdGV4cG9ydHMuZ2V0ID0ga2V5ID0+IHtcclxuXHRcdHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblx0XHRcdGNvbnN0IHVybCA9IGAke2Jhc2VVcmx9L0dldGA7XHJcblx0XHRcdCQuYWpheCh7IHVybCAsIGRhdGE6IHsga2V5LCBwYWdlVG9rZW4gfX0pXHJcblx0XHRcdFx0LmRvbmUocmVzcG9uc2UgPT4gcmVzb2x2ZShyZXNwb25zZSkpXHJcblx0XHRcdFx0LmZhaWwoZXJyb3IgPT4gcmVqZWN0KGVycm9yKSk7XHJcblx0XHR9KTtcclxuXHR9O1xyXG5cdFxyXG59KShqc2UubGlicy5jb25maWd1cmF0aW9uKTtcclxuIl19
