'use strict';

/* --------------------------------------------------------------
 url_arguments.js 2016-05-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.url_arguments = jse.libs.url_arguments || {};

/**
 * ## URL Arguments Library
 *
 * This library is created to help coding when values of URL are required.
 *
 * @module JSE/Libs/url_arguments
 * @exports jse.libs.url_arguments
 */
(function (exports) {

	'use strict';

	/**
  * Returns all URL parameters from the provided URL.
  *
  * @param {string} url (optional) The URL to be parsed. If not provided the current location will be used.
  *
  * @return {object} Returns an object that contains the parameters in key-value pairs.
  * 
  * @deprecated Use the $.deparam method which can better parse the GET parameters. 
  */

	exports.getUrlParameters = function (url) {
		var parameters = {},
		    search = url ? url.replace(/.*\?/, '') : location.search.substring(1),
		    result;

		if (search === null || search === '') {
			return parameters;
		}

		result = search.split('&');

		for (var i = 0; i < result.length; i++) {
			var tmp = result[i].split('=');
			parameters[tmp[0]] = tmp[1];
		}

		return parameters;
	};

	/**
  * Returns the current filename.
  *
  * @returns string Current filename.
  */
	exports.getCurrentFile = function () {
		var urlArray = window.location.pathname.split('/');
		return urlArray[urlArray.length - 1];
	};

	/**
  * Replaces a specific parameter value inside an URL.
  *
  * @param url The URL containing the parameter.
  * @param parameter The parameter name to be replaced.
  * @param value The new value of the parameter.
  *
  * @returns {string} Returns the updated URL string.
  */
	exports.replaceParameterValue = function (url, parameter, value) {
		var regex = new RegExp('(' + parameter + '=)[^\&]+');

		url = url.replace(regex, '$1' + value);

		if (url.search(parameter + '=') === -1 && value !== undefined) {
			if (url.search(/\?/) === -1) {
				url += '?' + encodeURIComponent(parameter) + '=' + encodeURIComponent(value);
			} else if (url.substr(url.length - 1, 1) === '?') {
				url += encodeURIComponent(parameter) + '=' + encodeURIComponent(value);
			} else {
				url += '&' + encodeURIComponent(parameter) + '=' + encodeURIComponent(value);
			}
		}

		return url;
	};
})(jse.libs.url_arguments);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVybF9hcmd1bWVudHMuanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsInVybF9hcmd1bWVudHMiLCJleHBvcnRzIiwiZ2V0VXJsUGFyYW1ldGVycyIsInVybCIsInBhcmFtZXRlcnMiLCJzZWFyY2giLCJyZXBsYWNlIiwibG9jYXRpb24iLCJzdWJzdHJpbmciLCJyZXN1bHQiLCJzcGxpdCIsImkiLCJsZW5ndGgiLCJ0bXAiLCJnZXRDdXJyZW50RmlsZSIsInVybEFycmF5Iiwid2luZG93IiwicGF0aG5hbWUiLCJyZXBsYWNlUGFyYW1ldGVyVmFsdWUiLCJwYXJhbWV0ZXIiLCJ2YWx1ZSIsInJlZ2V4IiwiUmVnRXhwIiwidW5kZWZpbmVkIiwiZW5jb2RlVVJJQ29tcG9uZW50Iiwic3Vic3RyIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLElBQUlDLElBQUosQ0FBU0MsYUFBVCxHQUF5QkYsSUFBSUMsSUFBSixDQUFTQyxhQUFULElBQTBCLEVBQW5EOztBQUVBOzs7Ozs7OztBQVFBLENBQUMsVUFBU0MsT0FBVCxFQUFrQjs7QUFFbEI7O0FBRUE7Ozs7Ozs7Ozs7QUFTQUEsU0FBUUMsZ0JBQVIsR0FBMkIsVUFBU0MsR0FBVCxFQUFjO0FBQ3hDLE1BQUlDLGFBQWEsRUFBakI7QUFBQSxNQUNDQyxTQUFVRixHQUFELEdBQVFBLElBQUlHLE9BQUosQ0FBWSxNQUFaLEVBQW9CLEVBQXBCLENBQVIsR0FBa0NDLFNBQVNGLE1BQVQsQ0FBZ0JHLFNBQWhCLENBQTBCLENBQTFCLENBRDVDO0FBQUEsTUFFQ0MsTUFGRDs7QUFJQSxNQUFJSixXQUFXLElBQVgsSUFBbUJBLFdBQVcsRUFBbEMsRUFBc0M7QUFDckMsVUFBT0QsVUFBUDtBQUNBOztBQUVESyxXQUFTSixPQUFPSyxLQUFQLENBQWEsR0FBYixDQUFUOztBQUVBLE9BQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJRixPQUFPRyxNQUEzQixFQUFtQ0QsR0FBbkMsRUFBd0M7QUFDdkMsT0FBSUUsTUFBTUosT0FBT0UsQ0FBUCxFQUFVRCxLQUFWLENBQWdCLEdBQWhCLENBQVY7QUFDQU4sY0FBV1MsSUFBSSxDQUFKLENBQVgsSUFBcUJBLElBQUksQ0FBSixDQUFyQjtBQUNBOztBQUVELFNBQU9ULFVBQVA7QUFDQSxFQWpCRDs7QUFtQkE7Ozs7O0FBS0FILFNBQVFhLGNBQVIsR0FBeUIsWUFBVztBQUNuQyxNQUFJQyxXQUFXQyxPQUFPVCxRQUFQLENBQWdCVSxRQUFoQixDQUF5QlAsS0FBekIsQ0FBK0IsR0FBL0IsQ0FBZjtBQUNBLFNBQU9LLFNBQVNBLFNBQVNILE1BQVQsR0FBa0IsQ0FBM0IsQ0FBUDtBQUNBLEVBSEQ7O0FBS0E7Ozs7Ozs7OztBQVNBWCxTQUFRaUIscUJBQVIsR0FBZ0MsVUFBU2YsR0FBVCxFQUFjZ0IsU0FBZCxFQUF5QkMsS0FBekIsRUFBZ0M7QUFDL0QsTUFBSUMsUUFBUSxJQUFJQyxNQUFKLENBQVcsTUFBTUgsU0FBTixHQUFrQixVQUE3QixDQUFaOztBQUVBaEIsUUFBTUEsSUFBSUcsT0FBSixDQUFZZSxLQUFaLEVBQW1CLE9BQU9ELEtBQTFCLENBQU47O0FBRUEsTUFBSWpCLElBQUlFLE1BQUosQ0FBV2MsWUFBWSxHQUF2QixNQUFnQyxDQUFDLENBQWpDLElBQXNDQyxVQUFVRyxTQUFwRCxFQUErRDtBQUM5RCxPQUFJcEIsSUFBSUUsTUFBSixDQUFXLElBQVgsTUFBcUIsQ0FBQyxDQUExQixFQUE2QjtBQUM1QkYsV0FBTyxNQUFNcUIsbUJBQW1CTCxTQUFuQixDQUFOLEdBQXNDLEdBQXRDLEdBQTRDSyxtQkFBbUJKLEtBQW5CLENBQW5EO0FBQ0EsSUFGRCxNQUVPLElBQUlqQixJQUFJc0IsTUFBSixDQUFXdEIsSUFBSVMsTUFBSixHQUFhLENBQXhCLEVBQTJCLENBQTNCLE1BQWtDLEdBQXRDLEVBQTJDO0FBQ2pEVCxXQUFPcUIsbUJBQW1CTCxTQUFuQixJQUFnQyxHQUFoQyxHQUFzQ0ssbUJBQW1CSixLQUFuQixDQUE3QztBQUNBLElBRk0sTUFFQTtBQUNOakIsV0FBTyxNQUFNcUIsbUJBQW1CTCxTQUFuQixDQUFOLEdBQXNDLEdBQXRDLEdBQTRDSyxtQkFBbUJKLEtBQW5CLENBQW5EO0FBQ0E7QUFDRDs7QUFFRCxTQUFPakIsR0FBUDtBQUNBLEVBaEJEO0FBa0JBLENBckVELEVBcUVHTCxJQUFJQyxJQUFKLENBQVNDLGFBckVaIiwiZmlsZSI6InVybF9hcmd1bWVudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHVybF9hcmd1bWVudHMuanMgMjAxNi0wNS0yM1xuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmpzZS5saWJzLnVybF9hcmd1bWVudHMgPSBqc2UubGlicy51cmxfYXJndW1lbnRzIHx8IHt9O1xuXG4vKipcbiAqICMjIFVSTCBBcmd1bWVudHMgTGlicmFyeVxuICpcbiAqIFRoaXMgbGlicmFyeSBpcyBjcmVhdGVkIHRvIGhlbHAgY29kaW5nIHdoZW4gdmFsdWVzIG9mIFVSTCBhcmUgcmVxdWlyZWQuXG4gKlxuICogQG1vZHVsZSBKU0UvTGlicy91cmxfYXJndW1lbnRzXG4gKiBAZXhwb3J0cyBqc2UubGlicy51cmxfYXJndW1lbnRzXG4gKi9cbihmdW5jdGlvbihleHBvcnRzKSB7XG5cdFxuXHQndXNlIHN0cmljdCc7XG5cdFxuXHQvKipcblx0ICogUmV0dXJucyBhbGwgVVJMIHBhcmFtZXRlcnMgZnJvbSB0aGUgcHJvdmlkZWQgVVJMLlxuXHQgKlxuXHQgKiBAcGFyYW0ge3N0cmluZ30gdXJsIChvcHRpb25hbCkgVGhlIFVSTCB0byBiZSBwYXJzZWQuIElmIG5vdCBwcm92aWRlZCB0aGUgY3VycmVudCBsb2NhdGlvbiB3aWxsIGJlIHVzZWQuXG5cdCAqXG5cdCAqIEByZXR1cm4ge29iamVjdH0gUmV0dXJucyBhbiBvYmplY3QgdGhhdCBjb250YWlucyB0aGUgcGFyYW1ldGVycyBpbiBrZXktdmFsdWUgcGFpcnMuXG5cdCAqIFxuXHQgKiBAZGVwcmVjYXRlZCBVc2UgdGhlICQuZGVwYXJhbSBtZXRob2Qgd2hpY2ggY2FuIGJldHRlciBwYXJzZSB0aGUgR0VUIHBhcmFtZXRlcnMuIFxuXHQgKi8gXG5cdGV4cG9ydHMuZ2V0VXJsUGFyYW1ldGVycyA9IGZ1bmN0aW9uKHVybCkge1xuXHRcdHZhciBwYXJhbWV0ZXJzID0ge30sXG5cdFx0XHRzZWFyY2ggPSAodXJsKSA/IHVybC5yZXBsYWNlKC8uKlxcPy8sICcnKSA6IGxvY2F0aW9uLnNlYXJjaC5zdWJzdHJpbmcoMSksXG5cdFx0XHRyZXN1bHQ7XG5cdFx0XG5cdFx0aWYgKHNlYXJjaCA9PT0gbnVsbCB8fCBzZWFyY2ggPT09ICcnKSB7XG5cdFx0XHRyZXR1cm4gcGFyYW1ldGVycztcblx0XHR9XG5cdFx0XG5cdFx0cmVzdWx0ID0gc2VhcmNoLnNwbGl0KCcmJyk7XG5cdFx0XG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCByZXN1bHQubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciB0bXAgPSByZXN1bHRbaV0uc3BsaXQoJz0nKTtcblx0XHRcdHBhcmFtZXRlcnNbdG1wWzBdXSA9IHRtcFsxXTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIHBhcmFtZXRlcnM7XG5cdH07XG5cdFxuXHQvKipcblx0ICogUmV0dXJucyB0aGUgY3VycmVudCBmaWxlbmFtZS5cblx0ICpcblx0ICogQHJldHVybnMgc3RyaW5nIEN1cnJlbnQgZmlsZW5hbWUuXG5cdCAqL1xuXHRleHBvcnRzLmdldEN1cnJlbnRGaWxlID0gZnVuY3Rpb24oKSB7XG5cdFx0dmFyIHVybEFycmF5ID0gd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lLnNwbGl0KCcvJyk7XG5cdFx0cmV0dXJuIHVybEFycmF5W3VybEFycmF5Lmxlbmd0aCAtIDFdO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIFJlcGxhY2VzIGEgc3BlY2lmaWMgcGFyYW1ldGVyIHZhbHVlIGluc2lkZSBhbiBVUkwuXG5cdCAqXG5cdCAqIEBwYXJhbSB1cmwgVGhlIFVSTCBjb250YWluaW5nIHRoZSBwYXJhbWV0ZXIuXG5cdCAqIEBwYXJhbSBwYXJhbWV0ZXIgVGhlIHBhcmFtZXRlciBuYW1lIHRvIGJlIHJlcGxhY2VkLlxuXHQgKiBAcGFyYW0gdmFsdWUgVGhlIG5ldyB2YWx1ZSBvZiB0aGUgcGFyYW1ldGVyLlxuXHQgKlxuXHQgKiBAcmV0dXJucyB7c3RyaW5nfSBSZXR1cm5zIHRoZSB1cGRhdGVkIFVSTCBzdHJpbmcuXG5cdCAqL1xuXHRleHBvcnRzLnJlcGxhY2VQYXJhbWV0ZXJWYWx1ZSA9IGZ1bmN0aW9uKHVybCwgcGFyYW1ldGVyLCB2YWx1ZSkge1xuXHRcdHZhciByZWdleCA9IG5ldyBSZWdFeHAoJygnICsgcGFyYW1ldGVyICsgJz0pW15cXCZdKycpO1xuXHRcdFxuXHRcdHVybCA9IHVybC5yZXBsYWNlKHJlZ2V4LCAnJDEnICsgdmFsdWUpO1xuXHRcdFxuXHRcdGlmICh1cmwuc2VhcmNoKHBhcmFtZXRlciArICc9JykgPT09IC0xICYmIHZhbHVlICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdGlmICh1cmwuc2VhcmNoKC9cXD8vKSA9PT0gLTEpIHtcblx0XHRcdFx0dXJsICs9ICc/JyArIGVuY29kZVVSSUNvbXBvbmVudChwYXJhbWV0ZXIpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlKTtcblx0XHRcdH0gZWxzZSBpZiAodXJsLnN1YnN0cih1cmwubGVuZ3RoIC0gMSwgMSkgPT09ICc/Jykge1xuXHRcdFx0XHR1cmwgKz0gZW5jb2RlVVJJQ29tcG9uZW50KHBhcmFtZXRlcikgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQodmFsdWUpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0dXJsICs9ICcmJyArIGVuY29kZVVSSUNvbXBvbmVudChwYXJhbWV0ZXIpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIHVybDtcblx0fTtcblx0XG59KShqc2UubGlicy51cmxfYXJndW1lbnRzKTtcbiJdfQ==
