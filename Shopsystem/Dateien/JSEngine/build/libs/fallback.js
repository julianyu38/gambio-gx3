'use strict';

/* --------------------------------------------------------------
 fallback.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.fallback = jse.libs.fallback || {};

/**
 * ## Fallback Library
 *
 * This library contains a set of deprecated functions that are still present for fallback support. Do not
 * use these methods in new modules.
 * 
 * @module JSE/Libs/fallback
 * @exports jse.libs.fallback
 */
(function (exports) {

	'use strict';

	/**
  * Add ":attr" pseudo selector.
  *
  * This pseudo selector is normally enabled by including the JSEngine "jquery_extensions" library. Honeygrid
  * through needs this pseudo selector in this library which might be loaded prior to jquery_extensions and
  * this is why we define it once again in this file.
  */

	if ($.expr.pseudos.attr === undefined) {
		$.expr.pseudos.attr = $.expr.createPseudo(function (selector) {
			var regexp = new RegExp(selector);
			return function (elem) {
				for (var i = 0; i < elem.attributes.length; i++) {
					var attr = elem.attributes[i];
					if (regexp.test(attr.name)) {
						return true;
					}
				}
				return false;
			};
		});
	}

	/**
  * Add a fallback usage warning in the console.
  *
  * As the JS engine evolves many old features will need to be changed in order to let a finer and clearer
  * API for the JS Engine core mechanisms. Use this method to create a fallback usage warning for the functions
  * placed within this library.
  *
  * @param {String} functionName The deprecated function name.
  *
  * @private
  */
	function _warn(functionName) {
		jse.core.debug.warn('jse.libs.fallback.' + functionName + ' was called! ' + 'Avoid the use of fallback methods in new modules.');
	}

	/**
  * Get the module related data of the provided element. 
  * 
  * @param {jQuery} $element
  * @param {String} moduleName
  * 
  * @return {Object}
  */
	exports._data = function ($element, moduleName) {
		_warn('_data');

		var initialData = $element.data(),
		    filteredData = {};

		// Searches for module relevant data inside the main-data-object.
		// Data for other widgets will not get passed to this widget
		$.each(initialData, function (key, value) {
			if (key.indexOf(moduleName) === 0 || key.indexOf(moduleName.toLowerCase()) === 0) {
				var newKey = key.substr(moduleName.length);
				newKey = newKey.substr(0, 1).toLowerCase() + newKey.substr(1);
				filteredData[newKey] = value;
			}
		});

		return filteredData;
	};

	/**
  * Setup Widget Attribute
  *
  * @param {Object} $element Change the widget attribute of an element.
  */
	exports.setupWidgetAttr = function ($element) {
		_warn('setupWidgetAttr');

		$element.filter(':attr(^data-gx-_), :attr(^data-gambio-_), :attr(^data-jse-_)').add($element.find(':attr(^data-gx-_), :attr(^data-gambio-_), :attr(^data-jse-_)')).each(function () {
			var $self = $(this),
			    attributes = $self[0].attributes,
			    matchedAttribute = void 0,
			    namespaceName = void 0;

			$.each(attributes, function (index, attribute) {
				if (attribute === undefined) {
					return true; // wrong attribute, continue loop
				}

				matchedAttribute = attribute.name.match(/data-(gambio|gx|jse)-_.*/g);

				if (matchedAttribute !== null && matchedAttribute.length > 0) {
					namespaceName = matchedAttribute[0].match(/(gambio|gx|jse)/g)[0];

					$self.attr(attribute.name.replace('data-' + namespaceName + '-_', 'data-' + namespaceName + '-'), attribute.value);
				}
			});
		});
	};

	/**
  * Get URL parameters.
  *
  * @param {String} url
  * @param {Boolean} deep
  *
  * @return {Object}
  */
	exports.getUrlParams = function (url, deep) {
		_warn('getUrlParams');

		url = decodeURIComponent(url || location.href);

		var splitUrl = url.split('?'),
		    splitParam = splitUrl.length > 1 ? splitUrl[1].split('&') : [],
		    regex = new RegExp(/\[(.*?)\]/g),
		    result = {};

		$.each(splitParam, function (i, v) {
			var keyValue = v.split('='),
			    regexResult = regex.exec(keyValue[0]),
			    base = null,
			    basename = keyValue[0].substring(0, keyValue[0].search('\\[')),
			    keys = [],
			    lastKey = null;

			if (!deep || regexResult === null) {
				result[keyValue[0]] = keyValue[1].split('#')[0];
			} else {

				result[basename] = result[basename] || [];
				base = result[basename];

				do {
					keys.push(regexResult[1]);
					regexResult = regex.exec(keyValue[0]);
				} while (regexResult !== null);

				$.each(keys, function (i, v) {
					var next = keys[i + 1];
					v = v || '0';

					if (typeof next === 'string') {
						base[v] = base[v] || [];
						base = base[v];
					} else {
						base[v] = base[v] || undefined;
						lastKey = v;
					}
				});

				if (lastKey !== null) {
					base[lastKey] = keyValue[1];
				} else {
					base = keyValue[1];
				}
			}
		});

		return result;
	};

	/**
  * Fallback getData method.
  *
  * This method was included in v1.0 of JS Engine and is replaced by the
  * "jse.libs.form.getData" method.
  *
  * @param {Object} $form Selector of the form to be parsed.
  * @param {String} ignore (optional) jQuery selector string of form elements to be ignored.
  *
  * @return {Object} Returns the data of the form as an object.
  */
	exports.getData = function ($form, ignore) {
		_warn('getData');

		var $elements = $form.find('input, textarea, select'),
		    result = {};

		if (ignore) {
			$elements = $elements.filter(':not(' + ignore + ')');
		}

		$elements.each(function () {
			var $self = $(this),
			    type = $self.prop('tagName').toLowerCase(),
			    name = $self.attr('name'),
			    $selected = null;

			type = type !== 'input' ? type : $self.attr('type').toLowerCase();

			switch (type) {
				case 'radio':
					$form.find('input[name="' + name + '"]:checked').val();
					break;
				case 'checkbox':
					if (name.search('\\[') !== -1) {
						if ($self.prop('checked')) {
							name = name.substring(0, name.search('\\['));
							if (result[name] === undefined) {
								result[name] = [];
							}
							result[name].push($(this).val());
						}
					} else {
						result[name] = $self.prop('checked');
					}
					break;
				case 'select':
					$selected = $self.find(':selected');
					if ($selected.length > 1) {
						result[name] = [];
						$selected.each(function () {
							result[name].push($(this).val());
						});
					} else {
						result[name] = $selected.val();
					}
					break;
				case 'button':
					break;
				default:
					if (name) {
						result[name] = $self.val();
					}
					break;
			}
		});
		return result;
	};
})(jse.libs.fallback);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZhbGxiYWNrLmpzIl0sIm5hbWVzIjpbImpzZSIsImxpYnMiLCJmYWxsYmFjayIsImV4cG9ydHMiLCIkIiwiZXhwciIsInBzZXVkb3MiLCJhdHRyIiwidW5kZWZpbmVkIiwiY3JlYXRlUHNldWRvIiwic2VsZWN0b3IiLCJyZWdleHAiLCJSZWdFeHAiLCJlbGVtIiwiaSIsImF0dHJpYnV0ZXMiLCJsZW5ndGgiLCJ0ZXN0IiwibmFtZSIsIl93YXJuIiwiZnVuY3Rpb25OYW1lIiwiY29yZSIsImRlYnVnIiwid2FybiIsIl9kYXRhIiwiJGVsZW1lbnQiLCJtb2R1bGVOYW1lIiwiaW5pdGlhbERhdGEiLCJkYXRhIiwiZmlsdGVyZWREYXRhIiwiZWFjaCIsImtleSIsInZhbHVlIiwiaW5kZXhPZiIsInRvTG93ZXJDYXNlIiwibmV3S2V5Iiwic3Vic3RyIiwic2V0dXBXaWRnZXRBdHRyIiwiZmlsdGVyIiwiYWRkIiwiZmluZCIsIiRzZWxmIiwibWF0Y2hlZEF0dHJpYnV0ZSIsIm5hbWVzcGFjZU5hbWUiLCJpbmRleCIsImF0dHJpYnV0ZSIsIm1hdGNoIiwicmVwbGFjZSIsImdldFVybFBhcmFtcyIsInVybCIsImRlZXAiLCJkZWNvZGVVUklDb21wb25lbnQiLCJsb2NhdGlvbiIsImhyZWYiLCJzcGxpdFVybCIsInNwbGl0Iiwic3BsaXRQYXJhbSIsInJlZ2V4IiwicmVzdWx0IiwidiIsImtleVZhbHVlIiwicmVnZXhSZXN1bHQiLCJleGVjIiwiYmFzZSIsImJhc2VuYW1lIiwic3Vic3RyaW5nIiwic2VhcmNoIiwia2V5cyIsImxhc3RLZXkiLCJwdXNoIiwibmV4dCIsImdldERhdGEiLCIkZm9ybSIsImlnbm9yZSIsIiRlbGVtZW50cyIsInR5cGUiLCJwcm9wIiwiJHNlbGVjdGVkIiwidmFsIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLElBQUlDLElBQUosQ0FBU0MsUUFBVCxHQUFvQkYsSUFBSUMsSUFBSixDQUFTQyxRQUFULElBQXFCLEVBQXpDOztBQUVBOzs7Ozs7Ozs7QUFTQSxDQUFDLFVBQVNDLE9BQVQsRUFBa0I7O0FBRWxCOztBQUVBOzs7Ozs7OztBQU9BLEtBQUlDLEVBQUVDLElBQUYsQ0FBT0MsT0FBUCxDQUFlQyxJQUFmLEtBQXdCQyxTQUE1QixFQUF1QztBQUN0Q0osSUFBRUMsSUFBRixDQUFPQyxPQUFQLENBQWVDLElBQWYsR0FBc0JILEVBQUVDLElBQUYsQ0FBT0ksWUFBUCxDQUFvQixVQUFTQyxRQUFULEVBQW1CO0FBQzVELE9BQUlDLFNBQVMsSUFBSUMsTUFBSixDQUFXRixRQUFYLENBQWI7QUFDQSxVQUFPLFVBQVNHLElBQVQsRUFBZTtBQUNyQixTQUFJLElBQUlDLElBQUksQ0FBWixFQUFlQSxJQUFJRCxLQUFLRSxVQUFMLENBQWdCQyxNQUFuQyxFQUEyQ0YsR0FBM0MsRUFBZ0Q7QUFDL0MsU0FBSVAsT0FBT00sS0FBS0UsVUFBTCxDQUFnQkQsQ0FBaEIsQ0FBWDtBQUNBLFNBQUdILE9BQU9NLElBQVAsQ0FBWVYsS0FBS1csSUFBakIsQ0FBSCxFQUEyQjtBQUMxQixhQUFPLElBQVA7QUFDQTtBQUNEO0FBQ0QsV0FBTyxLQUFQO0FBQ0EsSUFSRDtBQVNBLEdBWHFCLENBQXRCO0FBWUE7O0FBRUQ7Ozs7Ozs7Ozs7O0FBV0EsVUFBU0MsS0FBVCxDQUFlQyxZQUFmLEVBQTZCO0FBQzVCcEIsTUFBSXFCLElBQUosQ0FBU0MsS0FBVCxDQUFlQyxJQUFmLENBQW9CLHVCQUFxQkgsWUFBckIsd0VBQXBCO0FBRUE7O0FBRUQ7Ozs7Ozs7O0FBUUFqQixTQUFRcUIsS0FBUixHQUFnQixVQUFTQyxRQUFULEVBQW1CQyxVQUFuQixFQUErQjtBQUM5Q1AsUUFBTSxPQUFOOztBQUVBLE1BQUlRLGNBQWNGLFNBQVNHLElBQVQsRUFBbEI7QUFBQSxNQUNDQyxlQUFlLEVBRGhCOztBQUdBO0FBQ0E7QUFDQXpCLElBQUUwQixJQUFGLENBQU9ILFdBQVAsRUFBb0IsVUFBQ0ksR0FBRCxFQUFNQyxLQUFOLEVBQWdCO0FBQ25DLE9BQUlELElBQUlFLE9BQUosQ0FBWVAsVUFBWixNQUE0QixDQUE1QixJQUFpQ0ssSUFBSUUsT0FBSixDQUFZUCxXQUFXUSxXQUFYLEVBQVosTUFBMEMsQ0FBL0UsRUFBa0Y7QUFDakYsUUFBSUMsU0FBU0osSUFBSUssTUFBSixDQUFXVixXQUFXVixNQUF0QixDQUFiO0FBQ0FtQixhQUFTQSxPQUFPQyxNQUFQLENBQWMsQ0FBZCxFQUFpQixDQUFqQixFQUFvQkYsV0FBcEIsS0FBb0NDLE9BQU9DLE1BQVAsQ0FBYyxDQUFkLENBQTdDO0FBQ0FQLGlCQUFhTSxNQUFiLElBQXVCSCxLQUF2QjtBQUNBO0FBQ0QsR0FORDs7QUFRQSxTQUFPSCxZQUFQO0FBQ0EsRUFqQkQ7O0FBbUJBOzs7OztBQUtBMUIsU0FBUWtDLGVBQVIsR0FBMEIsVUFBU1osUUFBVCxFQUFtQjtBQUM1Q04sUUFBTSxpQkFBTjs7QUFFQU0sV0FDRWEsTUFERixDQUNTLDhEQURULEVBRUVDLEdBRkYsQ0FFTWQsU0FBU2UsSUFBVCxDQUFjLDhEQUFkLENBRk4sRUFHRVYsSUFIRixDQUdPLFlBQVc7QUFDaEIsT0FBSVcsUUFBUXJDLEVBQUUsSUFBRixDQUFaO0FBQUEsT0FDQ1csYUFBYTBCLE1BQU0sQ0FBTixFQUFTMUIsVUFEdkI7QUFBQSxPQUVDMkIseUJBRkQ7QUFBQSxPQUdDQyxzQkFIRDs7QUFLQXZDLEtBQUUwQixJQUFGLENBQU9mLFVBQVAsRUFBbUIsVUFBUzZCLEtBQVQsRUFBZ0JDLFNBQWhCLEVBQTJCO0FBQzdDLFFBQUlBLGNBQWNyQyxTQUFsQixFQUE2QjtBQUM1QixZQUFPLElBQVAsQ0FENEIsQ0FDZjtBQUNiOztBQUVEa0MsdUJBQW1CRyxVQUFVM0IsSUFBVixDQUFlNEIsS0FBZixDQUFxQiwyQkFBckIsQ0FBbkI7O0FBRUEsUUFBSUoscUJBQXFCLElBQXJCLElBQTZCQSxpQkFBaUIxQixNQUFqQixHQUEwQixDQUEzRCxFQUE4RDtBQUM3RDJCLHFCQUFnQkQsaUJBQWlCLENBQWpCLEVBQW9CSSxLQUFwQixDQUEwQixrQkFBMUIsRUFBOEMsQ0FBOUMsQ0FBaEI7O0FBRUFMLFdBQ0VsQyxJQURGLENBQ09zQyxVQUFVM0IsSUFBVixDQUFlNkIsT0FBZixDQUF1QixVQUFVSixhQUFWLEdBQTBCLElBQWpELEVBQ0wsVUFBVUEsYUFBVixHQUEwQixHQURyQixDQURQLEVBRWtDRSxVQUFVYixLQUY1QztBQUdBO0FBQ0QsSUFkRDtBQWVBLEdBeEJGO0FBeUJBLEVBNUJEOztBQThCQTs7Ozs7Ozs7QUFRQTdCLFNBQVE2QyxZQUFSLEdBQXVCLFVBQVNDLEdBQVQsRUFBY0MsSUFBZCxFQUFvQjtBQUMxQy9CLFFBQU0sY0FBTjs7QUFFQThCLFFBQU1FLG1CQUFtQkYsT0FBT0csU0FBU0MsSUFBbkMsQ0FBTjs7QUFFQSxNQUFJQyxXQUFXTCxJQUFJTSxLQUFKLENBQVUsR0FBVixDQUFmO0FBQUEsTUFDQ0MsYUFBY0YsU0FBU3RDLE1BQVQsR0FBa0IsQ0FBbkIsR0FBd0JzQyxTQUFTLENBQVQsRUFBWUMsS0FBWixDQUFrQixHQUFsQixDQUF4QixHQUFpRCxFQUQvRDtBQUFBLE1BRUNFLFFBQVEsSUFBSTdDLE1BQUosQ0FBVyxZQUFYLENBRlQ7QUFBQSxNQUdDOEMsU0FBUyxFQUhWOztBQUtBdEQsSUFBRTBCLElBQUYsQ0FBTzBCLFVBQVAsRUFBbUIsVUFBUzFDLENBQVQsRUFBWTZDLENBQVosRUFBZTtBQUNqQyxPQUFJQyxXQUFXRCxFQUFFSixLQUFGLENBQVEsR0FBUixDQUFmO0FBQUEsT0FDQ00sY0FBY0osTUFBTUssSUFBTixDQUFXRixTQUFTLENBQVQsQ0FBWCxDQURmO0FBQUEsT0FFQ0csT0FBTyxJQUZSO0FBQUEsT0FHQ0MsV0FBV0osU0FBUyxDQUFULEVBQVlLLFNBQVosQ0FBc0IsQ0FBdEIsRUFBeUJMLFNBQVMsQ0FBVCxFQUFZTSxNQUFaLENBQW1CLEtBQW5CLENBQXpCLENBSFo7QUFBQSxPQUlDQyxPQUFPLEVBSlI7QUFBQSxPQUtDQyxVQUFVLElBTFg7O0FBT0EsT0FBSSxDQUFDbEIsSUFBRCxJQUFTVyxnQkFBZ0IsSUFBN0IsRUFBbUM7QUFDbENILFdBQU9FLFNBQVMsQ0FBVCxDQUFQLElBQXNCQSxTQUFTLENBQVQsRUFBWUwsS0FBWixDQUFrQixHQUFsQixFQUF1QixDQUF2QixDQUF0QjtBQUNBLElBRkQsTUFFTzs7QUFFTkcsV0FBT00sUUFBUCxJQUFtQk4sT0FBT00sUUFBUCxLQUFvQixFQUF2QztBQUNBRCxXQUFPTCxPQUFPTSxRQUFQLENBQVA7O0FBRUEsT0FBRztBQUNGRyxVQUFLRSxJQUFMLENBQVVSLFlBQVksQ0FBWixDQUFWO0FBQ0FBLG1CQUFjSixNQUFNSyxJQUFOLENBQVdGLFNBQVMsQ0FBVCxDQUFYLENBQWQ7QUFDQSxLQUhELFFBR1NDLGdCQUFnQixJQUh6Qjs7QUFLQXpELE1BQUUwQixJQUFGLENBQU9xQyxJQUFQLEVBQWEsVUFBU3JELENBQVQsRUFBWTZDLENBQVosRUFBZTtBQUMzQixTQUFJVyxPQUFPSCxLQUFLckQsSUFBSSxDQUFULENBQVg7QUFDQTZDLFNBQUlBLEtBQUssR0FBVDs7QUFFQSxTQUFJLE9BQVFXLElBQVIsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDL0JQLFdBQUtKLENBQUwsSUFBVUksS0FBS0osQ0FBTCxLQUFXLEVBQXJCO0FBQ0FJLGFBQU9BLEtBQUtKLENBQUwsQ0FBUDtBQUNBLE1BSEQsTUFHTztBQUNOSSxXQUFLSixDQUFMLElBQVVJLEtBQUtKLENBQUwsS0FBV25ELFNBQXJCO0FBQ0E0RCxnQkFBVVQsQ0FBVjtBQUNBO0FBQ0QsS0FYRDs7QUFhQSxRQUFJUyxZQUFZLElBQWhCLEVBQXNCO0FBQ3JCTCxVQUFLSyxPQUFMLElBQWdCUixTQUFTLENBQVQsQ0FBaEI7QUFDQSxLQUZELE1BRU87QUFDTkcsWUFBT0gsU0FBUyxDQUFULENBQVA7QUFDQTtBQUNEO0FBRUQsR0F4Q0Q7O0FBMENBLFNBQU9GLE1BQVA7QUFDQSxFQXJERDs7QUF1REE7Ozs7Ozs7Ozs7O0FBV0F2RCxTQUFRb0UsT0FBUixHQUFrQixVQUFVQyxLQUFWLEVBQWlCQyxNQUFqQixFQUF5QjtBQUMxQ3RELFFBQU0sU0FBTjs7QUFFQSxNQUFJdUQsWUFBWUYsTUFBTWhDLElBQU4sQ0FBVyx5QkFBWCxDQUFoQjtBQUFBLE1BQ0NrQixTQUFTLEVBRFY7O0FBR0EsTUFBSWUsTUFBSixFQUFZO0FBQ1hDLGVBQVlBLFVBQVVwQyxNQUFWLENBQWlCLFVBQVVtQyxNQUFWLEdBQW1CLEdBQXBDLENBQVo7QUFDQTs7QUFFREMsWUFBVTVDLElBQVYsQ0FBZSxZQUFZO0FBQzFCLE9BQUlXLFFBQVFyQyxFQUFFLElBQUYsQ0FBWjtBQUFBLE9BQ0N1RSxPQUFPbEMsTUFBTW1DLElBQU4sQ0FBVyxTQUFYLEVBQXNCMUMsV0FBdEIsRUFEUjtBQUFBLE9BRUNoQixPQUFPdUIsTUFBTWxDLElBQU4sQ0FBVyxNQUFYLENBRlI7QUFBQSxPQUdDc0UsWUFBWSxJQUhiOztBQUtBRixVQUFRQSxTQUFTLE9BQVYsR0FBcUJBLElBQXJCLEdBQTRCbEMsTUFBTWxDLElBQU4sQ0FBVyxNQUFYLEVBQW1CMkIsV0FBbkIsRUFBbkM7O0FBRUEsV0FBUXlDLElBQVI7QUFDQyxTQUFLLE9BQUw7QUFDQ0gsV0FDRWhDLElBREYsQ0FDTyxpQkFBaUJ0QixJQUFqQixHQUF3QixZQUQvQixFQUVFNEQsR0FGRjtBQUdBO0FBQ0QsU0FBSyxVQUFMO0FBQ0MsU0FBSTVELEtBQUtnRCxNQUFMLENBQVksS0FBWixNQUF1QixDQUFDLENBQTVCLEVBQStCO0FBQzlCLFVBQUl6QixNQUFNbUMsSUFBTixDQUFXLFNBQVgsQ0FBSixFQUEyQjtBQUMxQjFELGNBQU9BLEtBQUsrQyxTQUFMLENBQWUsQ0FBZixFQUFrQi9DLEtBQUtnRCxNQUFMLENBQVksS0FBWixDQUFsQixDQUFQO0FBQ0EsV0FBSVIsT0FBT3hDLElBQVAsTUFBaUJWLFNBQXJCLEVBQWdDO0FBQy9Ca0QsZUFBT3hDLElBQVAsSUFBZSxFQUFmO0FBQ0E7QUFDRHdDLGNBQU94QyxJQUFQLEVBQWFtRCxJQUFiLENBQWtCakUsRUFBRSxJQUFGLEVBQVEwRSxHQUFSLEVBQWxCO0FBQ0E7QUFDRCxNQVJELE1BUU87QUFDTnBCLGFBQU94QyxJQUFQLElBQWV1QixNQUFNbUMsSUFBTixDQUFXLFNBQVgsQ0FBZjtBQUNBO0FBQ0Q7QUFDRCxTQUFLLFFBQUw7QUFDQ0MsaUJBQVlwQyxNQUFNRCxJQUFOLENBQVcsV0FBWCxDQUFaO0FBQ0EsU0FBSXFDLFVBQVU3RCxNQUFWLEdBQW1CLENBQXZCLEVBQTBCO0FBQ3pCMEMsYUFBT3hDLElBQVAsSUFBZSxFQUFmO0FBQ0EyRCxnQkFBVS9DLElBQVYsQ0FBZSxZQUFZO0FBQzFCNEIsY0FBT3hDLElBQVAsRUFBYW1ELElBQWIsQ0FBa0JqRSxFQUFFLElBQUYsRUFBUTBFLEdBQVIsRUFBbEI7QUFDQSxPQUZEO0FBR0EsTUFMRCxNQUtPO0FBQ05wQixhQUFPeEMsSUFBUCxJQUFlMkQsVUFBVUMsR0FBVixFQUFmO0FBQ0E7QUFDRDtBQUNELFNBQUssUUFBTDtBQUNDO0FBQ0Q7QUFDQyxTQUFJNUQsSUFBSixFQUFVO0FBQ1R3QyxhQUFPeEMsSUFBUCxJQUFldUIsTUFBTXFDLEdBQU4sRUFBZjtBQUNBO0FBQ0Q7QUFwQ0Y7QUFzQ0EsR0E5Q0Q7QUErQ0EsU0FBT3BCLE1BQVA7QUFDQSxFQTFERDtBQTREQSxDQTlPRCxFQThPRzFELElBQUlDLElBQUosQ0FBU0MsUUE5T1oiLCJmaWxlIjoiZmFsbGJhY2suanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gZmFsbGJhY2suanMgMjAxNi0wOS0wOFxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTYgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbmpzZS5saWJzLmZhbGxiYWNrID0ganNlLmxpYnMuZmFsbGJhY2sgfHwge307XHJcblxyXG4vKipcclxuICogIyMgRmFsbGJhY2sgTGlicmFyeVxyXG4gKlxyXG4gKiBUaGlzIGxpYnJhcnkgY29udGFpbnMgYSBzZXQgb2YgZGVwcmVjYXRlZCBmdW5jdGlvbnMgdGhhdCBhcmUgc3RpbGwgcHJlc2VudCBmb3IgZmFsbGJhY2sgc3VwcG9ydC4gRG8gbm90XHJcbiAqIHVzZSB0aGVzZSBtZXRob2RzIGluIG5ldyBtb2R1bGVzLlxyXG4gKiBcclxuICogQG1vZHVsZSBKU0UvTGlicy9mYWxsYmFja1xyXG4gKiBAZXhwb3J0cyBqc2UubGlicy5mYWxsYmFja1xyXG4gKi9cclxuKGZ1bmN0aW9uKGV4cG9ydHMpIHtcclxuXHRcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0LyoqXHJcblx0ICogQWRkIFwiOmF0dHJcIiBwc2V1ZG8gc2VsZWN0b3IuXHJcblx0ICpcclxuXHQgKiBUaGlzIHBzZXVkbyBzZWxlY3RvciBpcyBub3JtYWxseSBlbmFibGVkIGJ5IGluY2x1ZGluZyB0aGUgSlNFbmdpbmUgXCJqcXVlcnlfZXh0ZW5zaW9uc1wiIGxpYnJhcnkuIEhvbmV5Z3JpZFxyXG5cdCAqIHRocm91Z2ggbmVlZHMgdGhpcyBwc2V1ZG8gc2VsZWN0b3IgaW4gdGhpcyBsaWJyYXJ5IHdoaWNoIG1pZ2h0IGJlIGxvYWRlZCBwcmlvciB0byBqcXVlcnlfZXh0ZW5zaW9ucyBhbmRcclxuXHQgKiB0aGlzIGlzIHdoeSB3ZSBkZWZpbmUgaXQgb25jZSBhZ2FpbiBpbiB0aGlzIGZpbGUuXHJcblx0ICovXHJcblx0aWYgKCQuZXhwci5wc2V1ZG9zLmF0dHIgPT09IHVuZGVmaW5lZCkge1xyXG5cdFx0JC5leHByLnBzZXVkb3MuYXR0ciA9ICQuZXhwci5jcmVhdGVQc2V1ZG8oZnVuY3Rpb24oc2VsZWN0b3IpIHtcclxuXHRcdFx0bGV0IHJlZ2V4cCA9IG5ldyBSZWdFeHAoc2VsZWN0b3IpO1xyXG5cdFx0XHRyZXR1cm4gZnVuY3Rpb24oZWxlbSkge1xyXG5cdFx0XHRcdGZvcihsZXQgaSA9IDA7IGkgPCBlbGVtLmF0dHJpYnV0ZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRcdGxldCBhdHRyID0gZWxlbS5hdHRyaWJ1dGVzW2ldO1xyXG5cdFx0XHRcdFx0aWYocmVnZXhwLnRlc3QoYXR0ci5uYW1lKSkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0XHR9O1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEFkZCBhIGZhbGxiYWNrIHVzYWdlIHdhcm5pbmcgaW4gdGhlIGNvbnNvbGUuXHJcblx0ICpcclxuXHQgKiBBcyB0aGUgSlMgZW5naW5lIGV2b2x2ZXMgbWFueSBvbGQgZmVhdHVyZXMgd2lsbCBuZWVkIHRvIGJlIGNoYW5nZWQgaW4gb3JkZXIgdG8gbGV0IGEgZmluZXIgYW5kIGNsZWFyZXJcclxuXHQgKiBBUEkgZm9yIHRoZSBKUyBFbmdpbmUgY29yZSBtZWNoYW5pc21zLiBVc2UgdGhpcyBtZXRob2QgdG8gY3JlYXRlIGEgZmFsbGJhY2sgdXNhZ2Ugd2FybmluZyBmb3IgdGhlIGZ1bmN0aW9uc1xyXG5cdCAqIHBsYWNlZCB3aXRoaW4gdGhpcyBsaWJyYXJ5LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IGZ1bmN0aW9uTmFtZSBUaGUgZGVwcmVjYXRlZCBmdW5jdGlvbiBuYW1lLlxyXG5cdCAqXHJcblx0ICogQHByaXZhdGVcclxuXHQgKi9cclxuXHRmdW5jdGlvbiBfd2FybihmdW5jdGlvbk5hbWUpIHtcclxuXHRcdGpzZS5jb3JlLmRlYnVnLndhcm4oYGpzZS5saWJzLmZhbGxiYWNrLiR7ZnVuY3Rpb25OYW1lfSB3YXMgY2FsbGVkISBgXHJcblx0XHRcdCsgYEF2b2lkIHRoZSB1c2Ugb2YgZmFsbGJhY2sgbWV0aG9kcyBpbiBuZXcgbW9kdWxlcy5gKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogR2V0IHRoZSBtb2R1bGUgcmVsYXRlZCBkYXRhIG9mIHRoZSBwcm92aWRlZCBlbGVtZW50LiBcclxuXHQgKiBcclxuXHQgKiBAcGFyYW0ge2pRdWVyeX0gJGVsZW1lbnRcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbW9kdWxlTmFtZVxyXG5cdCAqIFxyXG5cdCAqIEByZXR1cm4ge09iamVjdH1cclxuXHQgKi9cclxuXHRleHBvcnRzLl9kYXRhID0gZnVuY3Rpb24oJGVsZW1lbnQsIG1vZHVsZU5hbWUpIHtcclxuXHRcdF93YXJuKCdfZGF0YScpO1xyXG5cdFx0XHJcblx0XHRsZXQgaW5pdGlhbERhdGEgPSAkZWxlbWVudC5kYXRhKCksXHJcblx0XHRcdGZpbHRlcmVkRGF0YSA9IHt9O1xyXG5cdFx0XHJcblx0XHQvLyBTZWFyY2hlcyBmb3IgbW9kdWxlIHJlbGV2YW50IGRhdGEgaW5zaWRlIHRoZSBtYWluLWRhdGEtb2JqZWN0LlxyXG5cdFx0Ly8gRGF0YSBmb3Igb3RoZXIgd2lkZ2V0cyB3aWxsIG5vdCBnZXQgcGFzc2VkIHRvIHRoaXMgd2lkZ2V0XHJcblx0XHQkLmVhY2goaW5pdGlhbERhdGEsIChrZXksIHZhbHVlKSA9PiB7XHJcblx0XHRcdGlmIChrZXkuaW5kZXhPZihtb2R1bGVOYW1lKSA9PT0gMCB8fCBrZXkuaW5kZXhPZihtb2R1bGVOYW1lLnRvTG93ZXJDYXNlKCkpID09PSAwKSB7XHJcblx0XHRcdFx0bGV0IG5ld0tleSA9IGtleS5zdWJzdHIobW9kdWxlTmFtZS5sZW5ndGgpO1xyXG5cdFx0XHRcdG5ld0tleSA9IG5ld0tleS5zdWJzdHIoMCwgMSkudG9Mb3dlckNhc2UoKSArIG5ld0tleS5zdWJzdHIoMSk7XHJcblx0XHRcdFx0ZmlsdGVyZWREYXRhW25ld0tleV0gPSB2YWx1ZTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdHJldHVybiBmaWx0ZXJlZERhdGE7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTZXR1cCBXaWRnZXQgQXR0cmlidXRlXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gJGVsZW1lbnQgQ2hhbmdlIHRoZSB3aWRnZXQgYXR0cmlidXRlIG9mIGFuIGVsZW1lbnQuXHJcblx0ICovXHJcblx0ZXhwb3J0cy5zZXR1cFdpZGdldEF0dHIgPSBmdW5jdGlvbigkZWxlbWVudCkge1xyXG5cdFx0X3dhcm4oJ3NldHVwV2lkZ2V0QXR0cicpO1xyXG5cdFx0XHJcblx0XHQkZWxlbWVudFxyXG5cdFx0XHQuZmlsdGVyKCc6YXR0ciheZGF0YS1neC1fKSwgOmF0dHIoXmRhdGEtZ2FtYmlvLV8pLCA6YXR0ciheZGF0YS1qc2UtXyknKVxyXG5cdFx0XHQuYWRkKCRlbGVtZW50LmZpbmQoJzphdHRyKF5kYXRhLWd4LV8pLCA6YXR0ciheZGF0YS1nYW1iaW8tXyksIDphdHRyKF5kYXRhLWpzZS1fKScpKVxyXG5cdFx0XHQuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRsZXQgJHNlbGYgPSAkKHRoaXMpLFxyXG5cdFx0XHRcdFx0YXR0cmlidXRlcyA9ICRzZWxmWzBdLmF0dHJpYnV0ZXMsXHJcblx0XHRcdFx0XHRtYXRjaGVkQXR0cmlidXRlLFxyXG5cdFx0XHRcdFx0bmFtZXNwYWNlTmFtZTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQkLmVhY2goYXR0cmlidXRlcywgZnVuY3Rpb24oaW5kZXgsIGF0dHJpYnV0ZSkge1xyXG5cdFx0XHRcdFx0aWYgKGF0dHJpYnV0ZSA9PT0gdW5kZWZpbmVkKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiB0cnVlOyAvLyB3cm9uZyBhdHRyaWJ1dGUsIGNvbnRpbnVlIGxvb3BcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0bWF0Y2hlZEF0dHJpYnV0ZSA9IGF0dHJpYnV0ZS5uYW1lLm1hdGNoKC9kYXRhLShnYW1iaW98Z3h8anNlKS1fLiovZyk7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmIChtYXRjaGVkQXR0cmlidXRlICE9PSBudWxsICYmIG1hdGNoZWRBdHRyaWJ1dGUubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdFx0XHRuYW1lc3BhY2VOYW1lID0gbWF0Y2hlZEF0dHJpYnV0ZVswXS5tYXRjaCgvKGdhbWJpb3xneHxqc2UpL2cpWzBdO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0JHNlbGZcclxuXHRcdFx0XHRcdFx0XHQuYXR0cihhdHRyaWJ1dGUubmFtZS5yZXBsYWNlKCdkYXRhLScgKyBuYW1lc3BhY2VOYW1lICsgJy1fJyxcclxuXHRcdFx0XHRcdFx0XHRcdCdkYXRhLScgKyBuYW1lc3BhY2VOYW1lICsgJy0nKSwgYXR0cmlidXRlLnZhbHVlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fSk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBHZXQgVVJMIHBhcmFtZXRlcnMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gdXJsXHJcblx0ICogQHBhcmFtIHtCb29sZWFufSBkZWVwXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtPYmplY3R9XHJcblx0ICovXHJcblx0ZXhwb3J0cy5nZXRVcmxQYXJhbXMgPSBmdW5jdGlvbih1cmwsIGRlZXApIHtcclxuXHRcdF93YXJuKCdnZXRVcmxQYXJhbXMnKTtcclxuXHRcdFxyXG5cdFx0dXJsID0gZGVjb2RlVVJJQ29tcG9uZW50KHVybCB8fCBsb2NhdGlvbi5ocmVmKTtcclxuXHRcdFxyXG5cdFx0bGV0IHNwbGl0VXJsID0gdXJsLnNwbGl0KCc/JyksXHJcblx0XHRcdHNwbGl0UGFyYW0gPSAoc3BsaXRVcmwubGVuZ3RoID4gMSkgPyBzcGxpdFVybFsxXS5zcGxpdCgnJicpIDogW10sXHJcblx0XHRcdHJlZ2V4ID0gbmV3IFJlZ0V4cCgvXFxbKC4qPylcXF0vZyksXHJcblx0XHRcdHJlc3VsdCA9IHt9O1xyXG5cdFx0XHJcblx0XHQkLmVhY2goc3BsaXRQYXJhbSwgZnVuY3Rpb24oaSwgdikge1xyXG5cdFx0XHRsZXQga2V5VmFsdWUgPSB2LnNwbGl0KCc9JyksXHJcblx0XHRcdFx0cmVnZXhSZXN1bHQgPSByZWdleC5leGVjKGtleVZhbHVlWzBdKSxcclxuXHRcdFx0XHRiYXNlID0gbnVsbCxcclxuXHRcdFx0XHRiYXNlbmFtZSA9IGtleVZhbHVlWzBdLnN1YnN0cmluZygwLCBrZXlWYWx1ZVswXS5zZWFyY2goJ1xcXFxbJykpLFxyXG5cdFx0XHRcdGtleXMgPSBbXSxcclxuXHRcdFx0XHRsYXN0S2V5ID0gbnVsbDtcclxuXHRcdFx0XHJcblx0XHRcdGlmICghZGVlcCB8fCByZWdleFJlc3VsdCA9PT0gbnVsbCkge1xyXG5cdFx0XHRcdHJlc3VsdFtrZXlWYWx1ZVswXV0gPSBrZXlWYWx1ZVsxXS5zcGxpdCgnIycpWzBdO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdHJlc3VsdFtiYXNlbmFtZV0gPSByZXN1bHRbYmFzZW5hbWVdIHx8IFtdO1xyXG5cdFx0XHRcdGJhc2UgPSByZXN1bHRbYmFzZW5hbWVdO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGRvIHtcclxuXHRcdFx0XHRcdGtleXMucHVzaChyZWdleFJlc3VsdFsxXSk7XHJcblx0XHRcdFx0XHRyZWdleFJlc3VsdCA9IHJlZ2V4LmV4ZWMoa2V5VmFsdWVbMF0pO1xyXG5cdFx0XHRcdH0gd2hpbGUgKHJlZ2V4UmVzdWx0ICE9PSBudWxsKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQkLmVhY2goa2V5cywgZnVuY3Rpb24oaSwgdikge1xyXG5cdFx0XHRcdFx0bGV0IG5leHQgPSBrZXlzW2kgKyAxXTtcclxuXHRcdFx0XHRcdHYgPSB2IHx8ICcwJztcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0aWYgKHR5cGVvZiAobmV4dCkgPT09ICdzdHJpbmcnKSB7XHJcblx0XHRcdFx0XHRcdGJhc2Vbdl0gPSBiYXNlW3ZdIHx8IFtdO1xyXG5cdFx0XHRcdFx0XHRiYXNlID0gYmFzZVt2XTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGJhc2Vbdl0gPSBiYXNlW3ZdIHx8IHVuZGVmaW5lZDtcclxuXHRcdFx0XHRcdFx0bGFzdEtleSA9IHY7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKGxhc3RLZXkgIT09IG51bGwpIHtcclxuXHRcdFx0XHRcdGJhc2VbbGFzdEtleV0gPSBrZXlWYWx1ZVsxXTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0YmFzZSA9IGtleVZhbHVlWzFdO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogRmFsbGJhY2sgZ2V0RGF0YSBtZXRob2QuXHJcblx0ICpcclxuXHQgKiBUaGlzIG1ldGhvZCB3YXMgaW5jbHVkZWQgaW4gdjEuMCBvZiBKUyBFbmdpbmUgYW5kIGlzIHJlcGxhY2VkIGJ5IHRoZVxyXG5cdCAqIFwianNlLmxpYnMuZm9ybS5nZXREYXRhXCIgbWV0aG9kLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R9ICRmb3JtIFNlbGVjdG9yIG9mIHRoZSBmb3JtIHRvIGJlIHBhcnNlZC5cclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gaWdub3JlIChvcHRpb25hbCkgalF1ZXJ5IHNlbGVjdG9yIHN0cmluZyBvZiBmb3JtIGVsZW1lbnRzIHRvIGJlIGlnbm9yZWQuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtPYmplY3R9IFJldHVybnMgdGhlIGRhdGEgb2YgdGhlIGZvcm0gYXMgYW4gb2JqZWN0LlxyXG5cdCAqL1xyXG5cdGV4cG9ydHMuZ2V0RGF0YSA9IGZ1bmN0aW9uICgkZm9ybSwgaWdub3JlKSB7XHJcblx0XHRfd2FybignZ2V0RGF0YScpO1xyXG5cdFx0XHJcblx0XHRsZXQgJGVsZW1lbnRzID0gJGZvcm0uZmluZCgnaW5wdXQsIHRleHRhcmVhLCBzZWxlY3QnKSxcclxuXHRcdFx0cmVzdWx0ID0ge307XHJcblx0XHRcclxuXHRcdGlmIChpZ25vcmUpIHtcclxuXHRcdFx0JGVsZW1lbnRzID0gJGVsZW1lbnRzLmZpbHRlcignOm5vdCgnICsgaWdub3JlICsgJyknKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0JGVsZW1lbnRzLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRsZXQgJHNlbGYgPSAkKHRoaXMpLFxyXG5cdFx0XHRcdHR5cGUgPSAkc2VsZi5wcm9wKCd0YWdOYW1lJykudG9Mb3dlckNhc2UoKSxcclxuXHRcdFx0XHRuYW1lID0gJHNlbGYuYXR0cignbmFtZScpLFxyXG5cdFx0XHRcdCRzZWxlY3RlZCA9IG51bGw7XHJcblx0XHRcdFxyXG5cdFx0XHR0eXBlID0gKHR5cGUgIT09ICdpbnB1dCcpID8gdHlwZSA6ICRzZWxmLmF0dHIoJ3R5cGUnKS50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0XHRcclxuXHRcdFx0c3dpdGNoICh0eXBlKSB7XHJcblx0XHRcdFx0Y2FzZSAncmFkaW8nOlxyXG5cdFx0XHRcdFx0JGZvcm1cclxuXHRcdFx0XHRcdFx0LmZpbmQoJ2lucHV0W25hbWU9XCInICsgbmFtZSArICdcIl06Y2hlY2tlZCcpXHJcblx0XHRcdFx0XHRcdC52YWwoKTtcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgJ2NoZWNrYm94JzpcclxuXHRcdFx0XHRcdGlmIChuYW1lLnNlYXJjaCgnXFxcXFsnKSAhPT0gLTEpIHtcclxuXHRcdFx0XHRcdFx0aWYgKCRzZWxmLnByb3AoJ2NoZWNrZWQnKSkge1xyXG5cdFx0XHRcdFx0XHRcdG5hbWUgPSBuYW1lLnN1YnN0cmluZygwLCBuYW1lLnNlYXJjaCgnXFxcXFsnKSk7XHJcblx0XHRcdFx0XHRcdFx0aWYgKHJlc3VsdFtuYW1lXSA9PT0gdW5kZWZpbmVkKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRyZXN1bHRbbmFtZV0gPSBbXTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0cmVzdWx0W25hbWVdLnB1c2goJCh0aGlzKS52YWwoKSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdHJlc3VsdFtuYW1lXSA9ICRzZWxmLnByb3AoJ2NoZWNrZWQnKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgJ3NlbGVjdCc6XHJcblx0XHRcdFx0XHQkc2VsZWN0ZWQgPSAkc2VsZi5maW5kKCc6c2VsZWN0ZWQnKTtcclxuXHRcdFx0XHRcdGlmICgkc2VsZWN0ZWQubGVuZ3RoID4gMSkge1xyXG5cdFx0XHRcdFx0XHRyZXN1bHRbbmFtZV0gPSBbXTtcclxuXHRcdFx0XHRcdFx0JHNlbGVjdGVkLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0XHRcdHJlc3VsdFtuYW1lXS5wdXNoKCQodGhpcykudmFsKCkpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdHJlc3VsdFtuYW1lXSA9ICRzZWxlY3RlZC52YWwoKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgJ2J1dHRvbic6XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFx0aWYgKG5hbWUpIHtcclxuXHRcdFx0XHRcdFx0cmVzdWx0W25hbWVdID0gJHNlbGYudmFsKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdH07XHJcblx0XHJcbn0pKGpzZS5saWJzLmZhbGxiYWNrKTsgIl19
