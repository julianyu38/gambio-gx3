'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* --------------------------------------------------------------
 form.js 2016-09-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.form = jse.libs.form || {};

/**
 * ## Form Utilities Library
 *
 * This library contains form helpers mostly required by old modules (JS Engine v1.0).
 *
 * @module JSE/Libs/forms
 * @exports jse.libs.forms
 */
(function (exports) {

	'use strict';

	/**
  * Get URL parameters.
  * 
  * @param {String} url
  * @param {Boolean} deep
  *
  * @return {Object}
  */

	function _getUrlParams(url, deep) {
		url = decodeURIComponent(url || location.href);

		var splitUrl = url.split('?'),
		    splitParam = splitUrl.length > 1 ? splitUrl[1].split('&') : [],
		    regex = new RegExp(/\[(.*?)\]/g),
		    result = {};

		$.each(splitParam, function (i, v) {
			var keyValue = v.split('='),
			    regexResult = regex.exec(keyValue[0]),
			    base = null,
			    basename = keyValue[0].substring(0, keyValue[0].search('\\[')),
			    keys = [],
			    lastKey = null;

			if (!deep || regexResult === null) {
				result[keyValue[0]] = keyValue[1].split('#')[0];
			} else {

				result[basename] = result[basename] || [];
				base = result[basename];

				do {
					keys.push(regexResult[1]);
					regexResult = regex.exec(keyValue[0]);
				} while (regexResult !== null);

				$.each(keys, function (i, v) {
					var next = keys[i + 1];
					v = v || '0';

					if (typeof next === 'string') {
						base[v] = base[v] || [];
						base = base[v];
					} else {
						base[v] = base[v] || undefined;
						lastKey = v;
					}
				});

				if (lastKey !== null) {
					base[lastKey] = keyValue[1];
				} else {
					base = keyValue[1];
				}
			}
		});

		return result;
	}

	/**
  * Create Options
  *
  * Function to add options to a select field. The full dataset for each option is added at the
  * option element.
  *
  * @param {object} $destination    jQuery-object of the select field.
  * @param {json} dataset Array that contains several objects with at least a "name" and a "value" field.
  * @param {bool} addEmpty If true, an empty select option will be generated (value = -1).
  * @param {bool} order Orders the dataset by name if true.
  *
  * @public
  */
	exports.createOptions = function ($destination, dataset, addEmpty, order) {
		var markup = [];

		// Helper for sorting the dataset
		var _optionsSorter = function _optionsSorter(a, b) {
			a = a.name.toLowerCase();
			b = b.name.toLowerCase();

			return a < b ? -1 : 1;
		};

		// Sort data
		dataset = order ? dataset.sort(_optionsSorter) : dataset;

		// Add an empty element if "addEmpty" is true
		if (addEmpty) {
			markup.push($('<option value="-1"> </option>'));
		}

		// Adding options to the markup
		$.each(dataset, function (index, value) {
			var $element = $('<option value="' + value.value + '">' + value.name + '</option>');
			$element.data('data', value);
			markup.push($element);
		});

		$destination.append(markup);
	};

	/**
  * Pre-fills a form by the given key value pairs in "options".
  *
  * @param {object} $form Element in which the form fields are searched.
  * @param {object} options A JSON with key-value pairs for the form fields.
  * @param {boolean} trigger A "change"-event gets triggered on the modified form field if true.
  *
  * @public
  */
	exports.prefillForm = function ($form, options, trigger) {
		$.each(options, function (index, value) {
			var $element = $form.find('[name="' + index + '"]'),
			    type = null;

			if ($element.length) {
				type = $element.prop('tagName').toLowerCase();
				type = type !== 'input' ? type : $element.attr('type').toLowerCase();

				switch (type) {
					case 'select':
						if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object') {
							// Case for multi-select
							$.each(value, function (i, value) {
								$element.find('option[value="' + value + '"]').prop('selected', true);
							});
						} else {
							// Case for single select
							$element.find('option[value="' + value + '"]').prop('selected', true);
						}
						break;
					case 'checkbox':
						$element.prop('checked', value !== 'false' ? true : false);
						break;
					case 'radio':
						$element.prop('checked', false);
						$element.each(function () {
							var $self = $(this);
							if ($self.val() === value.toString()) {
								$self.prop('checked', true);
							}
						});
						break;
					case 'textarea':
						$element.val(value);
						break;
					default:
						$element.val(value);
						break;
				}

				if (trigger) {
					$element.trigger('change', []);
				}
			}
		});
	};

	/**
  * Returns the data from the form fields in a jQuery advantageous JSON format
  *
  * @param {object} $form Target form selector object to be searched.
  * @param {string} ignoreSelector Selector string to be ignored.
  *
  * @return {object} Returns the data from the form elements.
  *
  * @public
  */
	exports.getData = function ($form, ignore, asJSON) {
		var $elements = $form.find('input, textarea, select'),
		    result = {};

		if (ignore) {
			$elements = $elements.filter(':not(' + ignore + ')');
		}

		$elements.each(function () {
			var $self = $(this),
			    type = $self.prop('tagName').toLowerCase(),
			    name = $self.attr('name'),
			    regex = new RegExp(/\[(.*?)\]/g),
			    regexResult = regex.exec(name),
			    watchdog = 5,
			    $selected = null,
			    res = null,
			    base = null,
			    lastKey = null;

			type = type !== 'input' ? type : $self.attr('type').toLowerCase();

			if (regexResult !== null) {

				var basename = name.substring(0, name.search('\\[')),
				    keys = [];

				result[basename] = result[basename] || (asJSON ? {} : []);
				base = result[basename];

				do {
					keys.push(regexResult[1]);
					regexResult = regex.exec(name);
					watchdog -= 1;
				} while (regexResult !== null || watchdog <= 0);

				$.each(keys, function (i, v) {
					var next = keys[i + 1];
					v = v || '0';

					if (typeof next === 'string') {
						base[v] = base[v] || (asJSON ? {} : []);
						base = base[v];
					} else if (type !== 'radio') {
						v = v && v !== '0' ? v : asJSON ? Object.keys(base).length : base.length;
						base[v] = base[v] || undefined;
					}

					lastKey = v;
				});
			}

			switch (type) {
				case 'radio':
					res = $elements.filter('input[name="' + $self.attr('name') + '"]:checked').val();
					break;
				case 'checkbox':
					res = $self.prop('checked') ? $self.val() : false;
					break;
				case 'select':
					$selected = $self.find(':selected');
					if ($selected.length > 1) {
						res = [];
						$selected.each(function () {
							res.push($(this).val());
						});
					} else {
						res = $selected.val();
					}
					break;
				case 'button':
					break;
				default:
					if (name) {
						res = $self.val();
					}
					break;
			}

			if (base !== null) {
				base[lastKey] = res;
			} else {
				result[name] = res;
			}
		});

		return result;
	};

	/**
  * Returns the form field type.
  *
  * @param {object} $element Element selector to be checked.
  *
  * @return {string} Returns the field type name of the element.
  *
  * @public
  */
	exports.getFieldType = function ($element) {
		var type = $element.prop('tagName').toLowerCase();
		return type !== 'input' ? type : $element.attr('type').toLowerCase();
	};

	/**
  * Adds a hidden field to the provided target.
  *
  * @param {object} $target Target element to prepend the hidden field to.
  * @param {boolean} replace Should the target element be replaced?
  */
	exports.addHiddenByUrl = function ($target, replace) {
		var urlParam = _getUrlParams(null),
		    $field = null,
		    hiddens = '',
		    update = [];

		$.each(urlParam, function (k, v) {
			if (v) {
				$field = $target.find('[name="' + k + '"]');

				if ($field.length === 0) {
					hiddens += '<input type="hidden" name="' + k + '" value="' + v + '" />';
				} else {
					update.push(k, v);
				}
			}
		});

		if (replace) {
			exports.prefillForm($target, update);
		}

		$target.prepend(hiddens);
	};

	/**
  * Resets the the provided target form.
  *
  * This method will clear all textfields. All radio buttons
  * and checkboxes will be unchecked, only the first checkbox and 
  * radio button will get checked. 
  *  
  * @param {object} $target Form to reset.
  */
	exports.reset = function ($target) {
		$target.find('select, input, textarea').each(function () {
			var $self = $(this),
			    type = exports.getFieldType($self);

			switch (type) {
				case 'radio':
					$target.find('input[name="' + $self.attr('name') + '"]:checked').prop('checked', false).first().prop('checked', true);
					break;
				case 'checkbox':
					$self.prop('checked', false);
					break;
				case 'select':
					$self.children().first().prop('selected', true);
					break;
				case 'textarea':
					$self.val('');
					break;
				case 'text':
					$self.val('');
					break;
				default:
					break;
			}
		});
	};
})(jse.libs.form);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvcm0uanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsImZvcm0iLCJleHBvcnRzIiwiX2dldFVybFBhcmFtcyIsInVybCIsImRlZXAiLCJkZWNvZGVVUklDb21wb25lbnQiLCJsb2NhdGlvbiIsImhyZWYiLCJzcGxpdFVybCIsInNwbGl0Iiwic3BsaXRQYXJhbSIsImxlbmd0aCIsInJlZ2V4IiwiUmVnRXhwIiwicmVzdWx0IiwiJCIsImVhY2giLCJpIiwidiIsImtleVZhbHVlIiwicmVnZXhSZXN1bHQiLCJleGVjIiwiYmFzZSIsImJhc2VuYW1lIiwic3Vic3RyaW5nIiwic2VhcmNoIiwia2V5cyIsImxhc3RLZXkiLCJwdXNoIiwibmV4dCIsInVuZGVmaW5lZCIsImNyZWF0ZU9wdGlvbnMiLCIkZGVzdGluYXRpb24iLCJkYXRhc2V0IiwiYWRkRW1wdHkiLCJvcmRlciIsIm1hcmt1cCIsIl9vcHRpb25zU29ydGVyIiwiYSIsImIiLCJuYW1lIiwidG9Mb3dlckNhc2UiLCJzb3J0IiwiaW5kZXgiLCJ2YWx1ZSIsIiRlbGVtZW50IiwiZGF0YSIsImFwcGVuZCIsInByZWZpbGxGb3JtIiwiJGZvcm0iLCJvcHRpb25zIiwidHJpZ2dlciIsImZpbmQiLCJ0eXBlIiwicHJvcCIsImF0dHIiLCIkc2VsZiIsInZhbCIsInRvU3RyaW5nIiwiZ2V0RGF0YSIsImlnbm9yZSIsImFzSlNPTiIsIiRlbGVtZW50cyIsImZpbHRlciIsIndhdGNoZG9nIiwiJHNlbGVjdGVkIiwicmVzIiwiT2JqZWN0IiwiZ2V0RmllbGRUeXBlIiwiYWRkSGlkZGVuQnlVcmwiLCIkdGFyZ2V0IiwicmVwbGFjZSIsInVybFBhcmFtIiwiJGZpZWxkIiwiaGlkZGVucyIsInVwZGF0ZSIsImsiLCJwcmVwZW5kIiwicmVzZXQiLCJmaXJzdCIsImNoaWxkcmVuIl0sIm1hcHBpbmdzIjoiOzs7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQyxJQUFULEdBQWdCRixJQUFJQyxJQUFKLENBQVNDLElBQVQsSUFBaUIsRUFBakM7O0FBRUE7Ozs7Ozs7O0FBUUEsQ0FBQyxVQUFTQyxPQUFULEVBQWtCOztBQUVsQjs7QUFFQTs7Ozs7Ozs7O0FBUUEsVUFBU0MsYUFBVCxDQUF1QkMsR0FBdkIsRUFBNEJDLElBQTVCLEVBQWtDO0FBQ2pDRCxRQUFNRSxtQkFBbUJGLE9BQU9HLFNBQVNDLElBQW5DLENBQU47O0FBRUEsTUFBSUMsV0FBV0wsSUFBSU0sS0FBSixDQUFVLEdBQVYsQ0FBZjtBQUFBLE1BQ0NDLGFBQWNGLFNBQVNHLE1BQVQsR0FBa0IsQ0FBbkIsR0FBd0JILFNBQVMsQ0FBVCxFQUFZQyxLQUFaLENBQWtCLEdBQWxCLENBQXhCLEdBQWlELEVBRC9EO0FBQUEsTUFFQ0csUUFBUSxJQUFJQyxNQUFKLENBQVcsWUFBWCxDQUZUO0FBQUEsTUFHQ0MsU0FBUyxFQUhWOztBQUtBQyxJQUFFQyxJQUFGLENBQU9OLFVBQVAsRUFBbUIsVUFBU08sQ0FBVCxFQUFZQyxDQUFaLEVBQWU7QUFDakMsT0FBSUMsV0FBV0QsRUFBRVQsS0FBRixDQUFRLEdBQVIsQ0FBZjtBQUFBLE9BQ0NXLGNBQWNSLE1BQU1TLElBQU4sQ0FBV0YsU0FBUyxDQUFULENBQVgsQ0FEZjtBQUFBLE9BRUNHLE9BQU8sSUFGUjtBQUFBLE9BR0NDLFdBQVdKLFNBQVMsQ0FBVCxFQUFZSyxTQUFaLENBQXNCLENBQXRCLEVBQXlCTCxTQUFTLENBQVQsRUFBWU0sTUFBWixDQUFtQixLQUFuQixDQUF6QixDQUhaO0FBQUEsT0FJQ0MsT0FBTyxFQUpSO0FBQUEsT0FLQ0MsVUFBVSxJQUxYOztBQU9BLE9BQUksQ0FBQ3ZCLElBQUQsSUFBU2dCLGdCQUFnQixJQUE3QixFQUFtQztBQUNsQ04sV0FBT0ssU0FBUyxDQUFULENBQVAsSUFBc0JBLFNBQVMsQ0FBVCxFQUFZVixLQUFaLENBQWtCLEdBQWxCLEVBQXVCLENBQXZCLENBQXRCO0FBQ0EsSUFGRCxNQUVPOztBQUVOSyxXQUFPUyxRQUFQLElBQW1CVCxPQUFPUyxRQUFQLEtBQW9CLEVBQXZDO0FBQ0FELFdBQU9SLE9BQU9TLFFBQVAsQ0FBUDs7QUFFQSxPQUFHO0FBQ0ZHLFVBQUtFLElBQUwsQ0FBVVIsWUFBWSxDQUFaLENBQVY7QUFDQUEsbUJBQWNSLE1BQU1TLElBQU4sQ0FBV0YsU0FBUyxDQUFULENBQVgsQ0FBZDtBQUNBLEtBSEQsUUFHU0MsZ0JBQWdCLElBSHpCOztBQUtBTCxNQUFFQyxJQUFGLENBQU9VLElBQVAsRUFBYSxVQUFTVCxDQUFULEVBQVlDLENBQVosRUFBZTtBQUMzQixTQUFJVyxPQUFPSCxLQUFLVCxJQUFJLENBQVQsQ0FBWDtBQUNBQyxTQUFJQSxLQUFLLEdBQVQ7O0FBRUEsU0FBSSxPQUFRVyxJQUFSLEtBQWtCLFFBQXRCLEVBQWdDO0FBQy9CUCxXQUFLSixDQUFMLElBQVVJLEtBQUtKLENBQUwsS0FBVyxFQUFyQjtBQUNBSSxhQUFPQSxLQUFLSixDQUFMLENBQVA7QUFDQSxNQUhELE1BR087QUFDTkksV0FBS0osQ0FBTCxJQUFVSSxLQUFLSixDQUFMLEtBQVdZLFNBQXJCO0FBQ0FILGdCQUFVVCxDQUFWO0FBQ0E7QUFDRCxLQVhEOztBQWFBLFFBQUlTLFlBQVksSUFBaEIsRUFBc0I7QUFDckJMLFVBQUtLLE9BQUwsSUFBZ0JSLFNBQVMsQ0FBVCxDQUFoQjtBQUNBLEtBRkQsTUFFTztBQUNORyxZQUFPSCxTQUFTLENBQVQsQ0FBUDtBQUNBO0FBQ0Q7QUFFRCxHQXhDRDs7QUEwQ0EsU0FBT0wsTUFBUDtBQUNBOztBQUVEOzs7Ozs7Ozs7Ozs7O0FBYUFiLFNBQVE4QixhQUFSLEdBQXdCLFVBQVNDLFlBQVQsRUFBdUJDLE9BQXZCLEVBQWdDQyxRQUFoQyxFQUEwQ0MsS0FBMUMsRUFBaUQ7QUFDeEUsTUFBSUMsU0FBUyxFQUFiOztBQUVBO0FBQ0EsTUFBSUMsaUJBQWlCLFNBQWpCQSxjQUFpQixDQUFTQyxDQUFULEVBQVlDLENBQVosRUFBZTtBQUNuQ0QsT0FBSUEsRUFBRUUsSUFBRixDQUFPQyxXQUFQLEVBQUo7QUFDQUYsT0FBSUEsRUFBRUMsSUFBRixDQUFPQyxXQUFQLEVBQUo7O0FBRUEsVUFBUUgsSUFBSUMsQ0FBTCxHQUFVLENBQUMsQ0FBWCxHQUFlLENBQXRCO0FBQ0EsR0FMRDs7QUFPQTtBQUNBTixZQUFVRSxRQUFRRixRQUFRUyxJQUFSLENBQWFMLGNBQWIsQ0FBUixHQUF1Q0osT0FBakQ7O0FBRUE7QUFDQSxNQUFJQyxRQUFKLEVBQWM7QUFDYkUsVUFBT1IsSUFBUCxDQUFZYixFQUFFLCtCQUFGLENBQVo7QUFDQTs7QUFFRDtBQUNBQSxJQUFFQyxJQUFGLENBQU9pQixPQUFQLEVBQWdCLFVBQVNVLEtBQVQsRUFBZ0JDLEtBQWhCLEVBQXVCO0FBQ3RDLE9BQUlDLFdBQVc5QixFQUFFLG9CQUFvQjZCLE1BQU1BLEtBQTFCLEdBQWtDLElBQWxDLEdBQXlDQSxNQUFNSixJQUEvQyxHQUFzRCxXQUF4RCxDQUFmO0FBQ0FLLFlBQVNDLElBQVQsQ0FBYyxNQUFkLEVBQXNCRixLQUF0QjtBQUNBUixVQUFPUixJQUFQLENBQVlpQixRQUFaO0FBQ0EsR0FKRDs7QUFNQWIsZUFBYWUsTUFBYixDQUFvQlgsTUFBcEI7QUFDQSxFQTNCRDs7QUE2QkE7Ozs7Ozs7OztBQVNBbkMsU0FBUStDLFdBQVIsR0FBc0IsVUFBU0MsS0FBVCxFQUFnQkMsT0FBaEIsRUFBeUJDLE9BQXpCLEVBQWtDO0FBQ3ZEcEMsSUFBRUMsSUFBRixDQUFPa0MsT0FBUCxFQUFnQixVQUFTUCxLQUFULEVBQWdCQyxLQUFoQixFQUF1QjtBQUN0QyxPQUFJQyxXQUFXSSxNQUFNRyxJQUFOLENBQVcsWUFBWVQsS0FBWixHQUFvQixJQUEvQixDQUFmO0FBQUEsT0FDQ1UsT0FBTyxJQURSOztBQUdBLE9BQUlSLFNBQVNsQyxNQUFiLEVBQXFCO0FBQ3BCMEMsV0FBT1IsU0FBU1MsSUFBVCxDQUFjLFNBQWQsRUFBeUJiLFdBQXpCLEVBQVA7QUFDQVksV0FBUUEsU0FBUyxPQUFWLEdBQXFCQSxJQUFyQixHQUE0QlIsU0FBU1UsSUFBVCxDQUFjLE1BQWQsRUFBc0JkLFdBQXRCLEVBQW5DOztBQUVBLFlBQVFZLElBQVI7QUFDQyxVQUFLLFFBQUw7QUFDQyxVQUFJLFFBQU9ULEtBQVAseUNBQU9BLEtBQVAsT0FBaUIsUUFBckIsRUFBK0I7QUFDOUI7QUFDQTdCLFNBQUVDLElBQUYsQ0FBTzRCLEtBQVAsRUFBYyxVQUFTM0IsQ0FBVCxFQUFZMkIsS0FBWixFQUFtQjtBQUNoQ0MsaUJBQ0VPLElBREYsQ0FDTyxtQkFBbUJSLEtBQW5CLEdBQTJCLElBRGxDLEVBRUVVLElBRkYsQ0FFTyxVQUZQLEVBRW1CLElBRm5CO0FBR0EsUUFKRDtBQUtBLE9BUEQsTUFPTztBQUNOO0FBQ0FULGdCQUNFTyxJQURGLENBQ08sbUJBQW1CUixLQUFuQixHQUEyQixJQURsQyxFQUVFVSxJQUZGLENBRU8sVUFGUCxFQUVtQixJQUZuQjtBQUdBO0FBQ0Q7QUFDRCxVQUFLLFVBQUw7QUFDQ1QsZUFBU1MsSUFBVCxDQUFjLFNBQWQsRUFBMEJWLFVBQVUsT0FBWCxHQUFzQixJQUF0QixHQUE2QixLQUF0RDtBQUNBO0FBQ0QsVUFBSyxPQUFMO0FBQ0NDLGVBQVNTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLEtBQXpCO0FBQ0FULGVBQVM3QixJQUFULENBQWMsWUFBVztBQUN4QixXQUFJd0MsUUFBUXpDLEVBQUUsSUFBRixDQUFaO0FBQ0EsV0FBSXlDLE1BQU1DLEdBQU4sT0FBZ0JiLE1BQU1jLFFBQU4sRUFBcEIsRUFBc0M7QUFDckNGLGNBQU1GLElBQU4sQ0FBVyxTQUFYLEVBQXNCLElBQXRCO0FBQ0E7QUFDRCxPQUxEO0FBTUE7QUFDRCxVQUFLLFVBQUw7QUFDQ1QsZUFBU1ksR0FBVCxDQUFhYixLQUFiO0FBQ0E7QUFDRDtBQUNDQyxlQUFTWSxHQUFULENBQWFiLEtBQWI7QUFDQTtBQWpDRjs7QUFvQ0EsUUFBSU8sT0FBSixFQUFhO0FBQ1pOLGNBQVNNLE9BQVQsQ0FBaUIsUUFBakIsRUFBMkIsRUFBM0I7QUFDQTtBQUNEO0FBQ0QsR0FoREQ7QUFrREEsRUFuREQ7O0FBcURBOzs7Ozs7Ozs7O0FBVUFsRCxTQUFRMEQsT0FBUixHQUFrQixVQUFTVixLQUFULEVBQWdCVyxNQUFoQixFQUF3QkMsTUFBeEIsRUFBZ0M7QUFDakQsTUFBSUMsWUFBWWIsTUFBTUcsSUFBTixDQUFXLHlCQUFYLENBQWhCO0FBQUEsTUFDQ3RDLFNBQVMsRUFEVjs7QUFHQSxNQUFJOEMsTUFBSixFQUFZO0FBQ1hFLGVBQVlBLFVBQVVDLE1BQVYsQ0FBaUIsVUFBVUgsTUFBVixHQUFtQixHQUFwQyxDQUFaO0FBQ0E7O0FBRURFLFlBQVU5QyxJQUFWLENBQWUsWUFBVztBQUN6QixPQUFJd0MsUUFBUXpDLEVBQUUsSUFBRixDQUFaO0FBQUEsT0FDQ3NDLE9BQU9HLE1BQU1GLElBQU4sQ0FBVyxTQUFYLEVBQXNCYixXQUF0QixFQURSO0FBQUEsT0FFQ0QsT0FBT2dCLE1BQU1ELElBQU4sQ0FBVyxNQUFYLENBRlI7QUFBQSxPQUdDM0MsUUFBUSxJQUFJQyxNQUFKLENBQVcsWUFBWCxDQUhUO0FBQUEsT0FJQ08sY0FBY1IsTUFBTVMsSUFBTixDQUFXbUIsSUFBWCxDQUpmO0FBQUEsT0FLQ3dCLFdBQVcsQ0FMWjtBQUFBLE9BTUNDLFlBQVksSUFOYjtBQUFBLE9BT0NDLE1BQU0sSUFQUDtBQUFBLE9BUUM1QyxPQUFPLElBUlI7QUFBQSxPQVNDSyxVQUFVLElBVFg7O0FBV0EwQixVQUFRQSxTQUFTLE9BQVYsR0FBcUJBLElBQXJCLEdBQTRCRyxNQUFNRCxJQUFOLENBQVcsTUFBWCxFQUFtQmQsV0FBbkIsRUFBbkM7O0FBRUEsT0FBSXJCLGdCQUFnQixJQUFwQixFQUEwQjs7QUFFekIsUUFBSUcsV0FBV2lCLEtBQUtoQixTQUFMLENBQWUsQ0FBZixFQUFrQmdCLEtBQUtmLE1BQUwsQ0FBWSxLQUFaLENBQWxCLENBQWY7QUFBQSxRQUNDQyxPQUFPLEVBRFI7O0FBR0FaLFdBQU9TLFFBQVAsSUFBbUJULE9BQU9TLFFBQVAsTUFBcUJzQyxTQUFTLEVBQVQsR0FBYyxFQUFuQyxDQUFuQjtBQUNBdkMsV0FBT1IsT0FBT1MsUUFBUCxDQUFQOztBQUVBLE9BQUc7QUFDRkcsVUFBS0UsSUFBTCxDQUFVUixZQUFZLENBQVosQ0FBVjtBQUNBQSxtQkFBY1IsTUFBTVMsSUFBTixDQUFXbUIsSUFBWCxDQUFkO0FBQ0F3QixpQkFBWSxDQUFaO0FBQ0EsS0FKRCxRQUlTNUMsZ0JBQWdCLElBQWhCLElBQXdCNEMsWUFBWSxDQUo3Qzs7QUFNQWpELE1BQUVDLElBQUYsQ0FBT1UsSUFBUCxFQUFhLFVBQVNULENBQVQsRUFBWUMsQ0FBWixFQUFlO0FBQzNCLFNBQUlXLE9BQU9ILEtBQUtULElBQUksQ0FBVCxDQUFYO0FBQ0FDLFNBQUlBLEtBQUssR0FBVDs7QUFFQSxTQUFJLE9BQVFXLElBQVIsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDL0JQLFdBQUtKLENBQUwsSUFBVUksS0FBS0osQ0FBTCxNQUFZMkMsU0FBUyxFQUFULEdBQWMsRUFBMUIsQ0FBVjtBQUNBdkMsYUFBT0EsS0FBS0osQ0FBTCxDQUFQO0FBQ0EsTUFIRCxNQUdPLElBQUltQyxTQUFTLE9BQWIsRUFBc0I7QUFDNUJuQyxVQUFLQSxLQUFLQSxNQUFNLEdBQVosR0FBbUJBLENBQW5CLEdBQ0MyQyxNQUFELEdBQVdNLE9BQU96QyxJQUFQLENBQVlKLElBQVosRUFBa0JYLE1BQTdCLEdBQXNDVyxLQUFLWCxNQUQvQztBQUVBVyxXQUFLSixDQUFMLElBQVVJLEtBQUtKLENBQUwsS0FBV1ksU0FBckI7QUFDQTs7QUFFREgsZUFBVVQsQ0FBVjtBQUNBLEtBZEQ7QUFnQkE7O0FBRUQsV0FBUW1DLElBQVI7QUFDQyxTQUFLLE9BQUw7QUFDQ2EsV0FBTUosVUFDSkMsTUFESSxDQUNHLGlCQUFpQlAsTUFBTUQsSUFBTixDQUFXLE1BQVgsQ0FBakIsR0FBc0MsWUFEekMsRUFFSkUsR0FGSSxFQUFOO0FBR0E7QUFDRCxTQUFLLFVBQUw7QUFDQ1MsV0FBT1YsTUFBTUYsSUFBTixDQUFXLFNBQVgsQ0FBRCxHQUEwQkUsTUFBTUMsR0FBTixFQUExQixHQUF3QyxLQUE5QztBQUNBO0FBQ0QsU0FBSyxRQUFMO0FBQ0NRLGlCQUFZVCxNQUFNSixJQUFOLENBQVcsV0FBWCxDQUFaO0FBQ0EsU0FBSWEsVUFBVXRELE1BQVYsR0FBbUIsQ0FBdkIsRUFBMEI7QUFDekJ1RCxZQUFNLEVBQU47QUFDQUQsZ0JBQVVqRCxJQUFWLENBQWUsWUFBVztBQUN6QmtELFdBQUl0QyxJQUFKLENBQVNiLEVBQUUsSUFBRixFQUFRMEMsR0FBUixFQUFUO0FBQ0EsT0FGRDtBQUdBLE1BTEQsTUFLTztBQUNOUyxZQUFNRCxVQUFVUixHQUFWLEVBQU47QUFDQTtBQUNEO0FBQ0QsU0FBSyxRQUFMO0FBQ0M7QUFDRDtBQUNDLFNBQUlqQixJQUFKLEVBQVU7QUFDVDBCLFlBQU1WLE1BQU1DLEdBQU4sRUFBTjtBQUNBO0FBQ0Q7QUExQkY7O0FBNkJBLE9BQUluQyxTQUFTLElBQWIsRUFBbUI7QUFDbEJBLFNBQUtLLE9BQUwsSUFBZ0J1QyxHQUFoQjtBQUNBLElBRkQsTUFFTztBQUNOcEQsV0FBTzBCLElBQVAsSUFBZTBCLEdBQWY7QUFDQTtBQUVELEdBakZEOztBQW1GQSxTQUFPcEQsTUFBUDtBQUNBLEVBNUZEOztBQThGQTs7Ozs7Ozs7O0FBU0FiLFNBQVFtRSxZQUFSLEdBQXVCLFVBQVN2QixRQUFULEVBQW1CO0FBQ3pDLE1BQUlRLE9BQU9SLFNBQVNTLElBQVQsQ0FBYyxTQUFkLEVBQXlCYixXQUF6QixFQUFYO0FBQ0EsU0FBUVksU0FBUyxPQUFWLEdBQXFCQSxJQUFyQixHQUE0QlIsU0FBU1UsSUFBVCxDQUFjLE1BQWQsRUFBc0JkLFdBQXRCLEVBQW5DO0FBQ0EsRUFIRDs7QUFLQTs7Ozs7O0FBTUF4QyxTQUFRb0UsY0FBUixHQUF5QixVQUFTQyxPQUFULEVBQWtCQyxPQUFsQixFQUEyQjtBQUNuRCxNQUFJQyxXQUFXdEUsY0FBYyxJQUFkLENBQWY7QUFBQSxNQUNDdUUsU0FBUyxJQURWO0FBQUEsTUFFQ0MsVUFBVSxFQUZYO0FBQUEsTUFHQ0MsU0FBUyxFQUhWOztBQUtBNUQsSUFBRUMsSUFBRixDQUFPd0QsUUFBUCxFQUFpQixVQUFTSSxDQUFULEVBQVkxRCxDQUFaLEVBQWU7QUFDL0IsT0FBSUEsQ0FBSixFQUFPO0FBQ051RCxhQUFTSCxRQUFRbEIsSUFBUixDQUFhLFlBQVl3QixDQUFaLEdBQWdCLElBQTdCLENBQVQ7O0FBRUEsUUFBSUgsT0FBTzlELE1BQVAsS0FBa0IsQ0FBdEIsRUFBeUI7QUFDeEIrRCxnQkFBVyxnQ0FBZ0NFLENBQWhDLEdBQW9DLFdBQXBDLEdBQWtEMUQsQ0FBbEQsR0FBc0QsTUFBakU7QUFDQSxLQUZELE1BRU87QUFDTnlELFlBQU8vQyxJQUFQLENBQVlnRCxDQUFaLEVBQWUxRCxDQUFmO0FBQ0E7QUFDRDtBQUNELEdBVkQ7O0FBWUEsTUFBSXFELE9BQUosRUFBYTtBQUNadEUsV0FBUStDLFdBQVIsQ0FBb0JzQixPQUFwQixFQUE2QkssTUFBN0I7QUFDQTs7QUFFREwsVUFBUU8sT0FBUixDQUFnQkgsT0FBaEI7QUFDQSxFQXZCRDs7QUF5QkE7Ozs7Ozs7OztBQVNBekUsU0FBUTZFLEtBQVIsR0FBZ0IsVUFBU1IsT0FBVCxFQUFrQjtBQUNqQ0EsVUFDRWxCLElBREYsQ0FDTyx5QkFEUCxFQUVFcEMsSUFGRixDQUVPLFlBQVc7QUFDaEIsT0FBSXdDLFFBQVF6QyxFQUFFLElBQUYsQ0FBWjtBQUFBLE9BQ0NzQyxPQUFPcEQsUUFBUW1FLFlBQVIsQ0FBcUJaLEtBQXJCLENBRFI7O0FBR0EsV0FBUUgsSUFBUjtBQUNDLFNBQUssT0FBTDtBQUNDaUIsYUFDRWxCLElBREYsQ0FDTyxpQkFBaUJJLE1BQU1ELElBQU4sQ0FBVyxNQUFYLENBQWpCLEdBQXNDLFlBRDdDLEVBRUVELElBRkYsQ0FFTyxTQUZQLEVBRWtCLEtBRmxCLEVBR0V5QixLQUhGLEdBSUV6QixJQUpGLENBSU8sU0FKUCxFQUlrQixJQUpsQjtBQUtBO0FBQ0QsU0FBSyxVQUFMO0FBQ0NFLFdBQU1GLElBQU4sQ0FBVyxTQUFYLEVBQXNCLEtBQXRCO0FBQ0E7QUFDRCxTQUFLLFFBQUw7QUFDQ0UsV0FDRXdCLFFBREYsR0FFRUQsS0FGRixHQUdFekIsSUFIRixDQUdPLFVBSFAsRUFHbUIsSUFIbkI7QUFJQTtBQUNELFNBQUssVUFBTDtBQUNDRSxXQUFNQyxHQUFOLENBQVUsRUFBVjtBQUNBO0FBQ0QsU0FBSyxNQUFMO0FBQ0NELFdBQU1DLEdBQU4sQ0FBVSxFQUFWO0FBQ0E7QUFDRDtBQUNDO0FBeEJGO0FBMEJBLEdBaENGO0FBaUNBLEVBbENEO0FBb0NBLENBM1dELEVBMldHM0QsSUFBSUMsSUFBSixDQUFTQyxJQTNXWiIsImZpbGUiOiJmb3JtLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBmb3JtLmpzIDIwMTYtMDktMDhcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UubGlicy5mb3JtID0ganNlLmxpYnMuZm9ybSB8fCB7fTtcblxuLyoqXG4gKiAjIyBGb3JtIFV0aWxpdGllcyBMaWJyYXJ5XG4gKlxuICogVGhpcyBsaWJyYXJ5IGNvbnRhaW5zIGZvcm0gaGVscGVycyBtb3N0bHkgcmVxdWlyZWQgYnkgb2xkIG1vZHVsZXMgKEpTIEVuZ2luZSB2MS4wKS5cbiAqXG4gKiBAbW9kdWxlIEpTRS9MaWJzL2Zvcm1zXG4gKiBAZXhwb3J0cyBqc2UubGlicy5mb3Jtc1xuICovXG4oZnVuY3Rpb24oZXhwb3J0cykge1xuXG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdC8qKlxuXHQgKiBHZXQgVVJMIHBhcmFtZXRlcnMuXG5cdCAqIFxuXHQgKiBAcGFyYW0ge1N0cmluZ30gdXJsXG5cdCAqIEBwYXJhbSB7Qm9vbGVhbn0gZGVlcFxuXHQgKlxuXHQgKiBAcmV0dXJuIHtPYmplY3R9XG5cdCAqL1xuXHRmdW5jdGlvbiBfZ2V0VXJsUGFyYW1zKHVybCwgZGVlcCkge1xuXHRcdHVybCA9IGRlY29kZVVSSUNvbXBvbmVudCh1cmwgfHwgbG9jYXRpb24uaHJlZik7XG5cdFx0XG5cdFx0bGV0IHNwbGl0VXJsID0gdXJsLnNwbGl0KCc/JyksXG5cdFx0XHRzcGxpdFBhcmFtID0gKHNwbGl0VXJsLmxlbmd0aCA+IDEpID8gc3BsaXRVcmxbMV0uc3BsaXQoJyYnKSA6IFtdLFxuXHRcdFx0cmVnZXggPSBuZXcgUmVnRXhwKC9cXFsoLio/KVxcXS9nKSxcblx0XHRcdHJlc3VsdCA9IHt9O1xuXHRcdFxuXHRcdCQuZWFjaChzcGxpdFBhcmFtLCBmdW5jdGlvbihpLCB2KSB7XG5cdFx0XHRsZXQga2V5VmFsdWUgPSB2LnNwbGl0KCc9JyksXG5cdFx0XHRcdHJlZ2V4UmVzdWx0ID0gcmVnZXguZXhlYyhrZXlWYWx1ZVswXSksXG5cdFx0XHRcdGJhc2UgPSBudWxsLFxuXHRcdFx0XHRiYXNlbmFtZSA9IGtleVZhbHVlWzBdLnN1YnN0cmluZygwLCBrZXlWYWx1ZVswXS5zZWFyY2goJ1xcXFxbJykpLFxuXHRcdFx0XHRrZXlzID0gW10sXG5cdFx0XHRcdGxhc3RLZXkgPSBudWxsO1xuXHRcdFx0XG5cdFx0XHRpZiAoIWRlZXAgfHwgcmVnZXhSZXN1bHQgPT09IG51bGwpIHtcblx0XHRcdFx0cmVzdWx0W2tleVZhbHVlWzBdXSA9IGtleVZhbHVlWzFdLnNwbGl0KCcjJylbMF07XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcblx0XHRcdFx0cmVzdWx0W2Jhc2VuYW1lXSA9IHJlc3VsdFtiYXNlbmFtZV0gfHwgW107XG5cdFx0XHRcdGJhc2UgPSByZXN1bHRbYmFzZW5hbWVdO1xuXHRcdFx0XHRcblx0XHRcdFx0ZG8ge1xuXHRcdFx0XHRcdGtleXMucHVzaChyZWdleFJlc3VsdFsxXSk7XG5cdFx0XHRcdFx0cmVnZXhSZXN1bHQgPSByZWdleC5leGVjKGtleVZhbHVlWzBdKTtcblx0XHRcdFx0fSB3aGlsZSAocmVnZXhSZXN1bHQgIT09IG51bGwpO1xuXHRcdFx0XHRcblx0XHRcdFx0JC5lYWNoKGtleXMsIGZ1bmN0aW9uKGksIHYpIHtcblx0XHRcdFx0XHRsZXQgbmV4dCA9IGtleXNbaSArIDFdO1xuXHRcdFx0XHRcdHYgPSB2IHx8ICcwJztcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAodHlwZW9mIChuZXh0KSA9PT0gJ3N0cmluZycpIHtcblx0XHRcdFx0XHRcdGJhc2Vbdl0gPSBiYXNlW3ZdIHx8IFtdO1xuXHRcdFx0XHRcdFx0YmFzZSA9IGJhc2Vbdl07XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdGJhc2Vbdl0gPSBiYXNlW3ZdIHx8IHVuZGVmaW5lZDtcblx0XHRcdFx0XHRcdGxhc3RLZXkgPSB2O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAobGFzdEtleSAhPT0gbnVsbCkge1xuXHRcdFx0XHRcdGJhc2VbbGFzdEtleV0gPSBrZXlWYWx1ZVsxXTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRiYXNlID0ga2V5VmFsdWVbMV07XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdFxuXHRcdH0pO1xuXHRcdFxuXHRcdHJldHVybiByZXN1bHQ7XG5cdH1cblxuXHQvKipcblx0ICogQ3JlYXRlIE9wdGlvbnNcblx0ICpcblx0ICogRnVuY3Rpb24gdG8gYWRkIG9wdGlvbnMgdG8gYSBzZWxlY3QgZmllbGQuIFRoZSBmdWxsIGRhdGFzZXQgZm9yIGVhY2ggb3B0aW9uIGlzIGFkZGVkIGF0IHRoZVxuXHQgKiBvcHRpb24gZWxlbWVudC5cblx0ICpcblx0ICogQHBhcmFtIHtvYmplY3R9ICRkZXN0aW5hdGlvbiAgICBqUXVlcnktb2JqZWN0IG9mIHRoZSBzZWxlY3QgZmllbGQuXG5cdCAqIEBwYXJhbSB7anNvbn0gZGF0YXNldCBBcnJheSB0aGF0IGNvbnRhaW5zIHNldmVyYWwgb2JqZWN0cyB3aXRoIGF0IGxlYXN0IGEgXCJuYW1lXCIgYW5kIGEgXCJ2YWx1ZVwiIGZpZWxkLlxuXHQgKiBAcGFyYW0ge2Jvb2x9IGFkZEVtcHR5IElmIHRydWUsIGFuIGVtcHR5IHNlbGVjdCBvcHRpb24gd2lsbCBiZSBnZW5lcmF0ZWQgKHZhbHVlID0gLTEpLlxuXHQgKiBAcGFyYW0ge2Jvb2x9IG9yZGVyIE9yZGVycyB0aGUgZGF0YXNldCBieSBuYW1lIGlmIHRydWUuXG5cdCAqXG5cdCAqIEBwdWJsaWNcblx0ICovXG5cdGV4cG9ydHMuY3JlYXRlT3B0aW9ucyA9IGZ1bmN0aW9uKCRkZXN0aW5hdGlvbiwgZGF0YXNldCwgYWRkRW1wdHksIG9yZGVyKSB7XG5cdFx0dmFyIG1hcmt1cCA9IFtdO1xuXG5cdFx0Ly8gSGVscGVyIGZvciBzb3J0aW5nIHRoZSBkYXRhc2V0XG5cdFx0dmFyIF9vcHRpb25zU29ydGVyID0gZnVuY3Rpb24oYSwgYikge1xuXHRcdFx0YSA9IGEubmFtZS50b0xvd2VyQ2FzZSgpO1xuXHRcdFx0YiA9IGIubmFtZS50b0xvd2VyQ2FzZSgpO1xuXG5cdFx0XHRyZXR1cm4gKGEgPCBiKSA/IC0xIDogMTtcblx0XHR9O1xuXG5cdFx0Ly8gU29ydCBkYXRhXG5cdFx0ZGF0YXNldCA9IG9yZGVyID8gZGF0YXNldC5zb3J0KF9vcHRpb25zU29ydGVyKSA6IGRhdGFzZXQ7XG5cblx0XHQvLyBBZGQgYW4gZW1wdHkgZWxlbWVudCBpZiBcImFkZEVtcHR5XCIgaXMgdHJ1ZVxuXHRcdGlmIChhZGRFbXB0eSkge1xuXHRcdFx0bWFya3VwLnB1c2goJCgnPG9wdGlvbiB2YWx1ZT1cIi0xXCI+IDwvb3B0aW9uPicpKTtcblx0XHR9XG5cblx0XHQvLyBBZGRpbmcgb3B0aW9ucyB0byB0aGUgbWFya3VwXG5cdFx0JC5lYWNoKGRhdGFzZXQsIGZ1bmN0aW9uKGluZGV4LCB2YWx1ZSkge1xuXHRcdFx0dmFyICRlbGVtZW50ID0gJCgnPG9wdGlvbiB2YWx1ZT1cIicgKyB2YWx1ZS52YWx1ZSArICdcIj4nICsgdmFsdWUubmFtZSArICc8L29wdGlvbj4nKTtcblx0XHRcdCRlbGVtZW50LmRhdGEoJ2RhdGEnLCB2YWx1ZSk7XG5cdFx0XHRtYXJrdXAucHVzaCgkZWxlbWVudCk7XG5cdFx0fSk7XG5cblx0XHQkZGVzdGluYXRpb24uYXBwZW5kKG1hcmt1cCk7XG5cdH07XG5cblx0LyoqXG5cdCAqIFByZS1maWxscyBhIGZvcm0gYnkgdGhlIGdpdmVuIGtleSB2YWx1ZSBwYWlycyBpbiBcIm9wdGlvbnNcIi5cblx0ICpcblx0ICogQHBhcmFtIHtvYmplY3R9ICRmb3JtIEVsZW1lbnQgaW4gd2hpY2ggdGhlIGZvcm0gZmllbGRzIGFyZSBzZWFyY2hlZC5cblx0ICogQHBhcmFtIHtvYmplY3R9IG9wdGlvbnMgQSBKU09OIHdpdGgga2V5LXZhbHVlIHBhaXJzIGZvciB0aGUgZm9ybSBmaWVsZHMuXG5cdCAqIEBwYXJhbSB7Ym9vbGVhbn0gdHJpZ2dlciBBIFwiY2hhbmdlXCItZXZlbnQgZ2V0cyB0cmlnZ2VyZWQgb24gdGhlIG1vZGlmaWVkIGZvcm0gZmllbGQgaWYgdHJ1ZS5cblx0ICpcblx0ICogQHB1YmxpY1xuXHQgKi9cblx0ZXhwb3J0cy5wcmVmaWxsRm9ybSA9IGZ1bmN0aW9uKCRmb3JtLCBvcHRpb25zLCB0cmlnZ2VyKSB7XG5cdFx0JC5lYWNoKG9wdGlvbnMsIGZ1bmN0aW9uKGluZGV4LCB2YWx1ZSkge1xuXHRcdFx0dmFyICRlbGVtZW50ID0gJGZvcm0uZmluZCgnW25hbWU9XCInICsgaW5kZXggKyAnXCJdJyksXG5cdFx0XHRcdHR5cGUgPSBudWxsO1xuXG5cdFx0XHRpZiAoJGVsZW1lbnQubGVuZ3RoKSB7XG5cdFx0XHRcdHR5cGUgPSAkZWxlbWVudC5wcm9wKCd0YWdOYW1lJykudG9Mb3dlckNhc2UoKTtcblx0XHRcdFx0dHlwZSA9ICh0eXBlICE9PSAnaW5wdXQnKSA/IHR5cGUgOiAkZWxlbWVudC5hdHRyKCd0eXBlJykudG9Mb3dlckNhc2UoKTtcblxuXHRcdFx0XHRzd2l0Y2ggKHR5cGUpIHtcblx0XHRcdFx0XHRjYXNlICdzZWxlY3QnOlxuXHRcdFx0XHRcdFx0aWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcpIHtcblx0XHRcdFx0XHRcdFx0Ly8gQ2FzZSBmb3IgbXVsdGktc2VsZWN0XG5cdFx0XHRcdFx0XHRcdCQuZWFjaCh2YWx1ZSwgZnVuY3Rpb24oaSwgdmFsdWUpIHtcblx0XHRcdFx0XHRcdFx0XHQkZWxlbWVudFxuXHRcdFx0XHRcdFx0XHRcdFx0LmZpbmQoJ29wdGlvblt2YWx1ZT1cIicgKyB2YWx1ZSArICdcIl0nKVxuXHRcdFx0XHRcdFx0XHRcdFx0LnByb3AoJ3NlbGVjdGVkJywgdHJ1ZSk7XG5cdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0Ly8gQ2FzZSBmb3Igc2luZ2xlIHNlbGVjdFxuXHRcdFx0XHRcdFx0XHQkZWxlbWVudFxuXHRcdFx0XHRcdFx0XHRcdC5maW5kKCdvcHRpb25bdmFsdWU9XCInICsgdmFsdWUgKyAnXCJdJylcblx0XHRcdFx0XHRcdFx0XHQucHJvcCgnc2VsZWN0ZWQnLCB0cnVlKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGNhc2UgJ2NoZWNrYm94Jzpcblx0XHRcdFx0XHRcdCRlbGVtZW50LnByb3AoJ2NoZWNrZWQnLCAodmFsdWUgIT09ICdmYWxzZScpID8gdHJ1ZSA6IGZhbHNlKTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGNhc2UgJ3JhZGlvJzpcblx0XHRcdFx0XHRcdCRlbGVtZW50LnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG5cdFx0XHRcdFx0XHQkZWxlbWVudC5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHR2YXIgJHNlbGYgPSAkKHRoaXMpO1xuXHRcdFx0XHRcdFx0XHRpZiAoJHNlbGYudmFsKCkgPT09IHZhbHVlLnRvU3RyaW5nKCkpIHtcblx0XHRcdFx0XHRcdFx0XHQkc2VsZi5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0Y2FzZSAndGV4dGFyZWEnOlxuXHRcdFx0XHRcdFx0JGVsZW1lbnQudmFsKHZhbHVlKTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0XHQkZWxlbWVudC52YWwodmFsdWUpO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZiAodHJpZ2dlcikge1xuXHRcdFx0XHRcdCRlbGVtZW50LnRyaWdnZXIoJ2NoYW5nZScsIFtdKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0pO1xuXG5cdH07XG5cblx0LyoqXG5cdCAqIFJldHVybnMgdGhlIGRhdGEgZnJvbSB0aGUgZm9ybSBmaWVsZHMgaW4gYSBqUXVlcnkgYWR2YW50YWdlb3VzIEpTT04gZm9ybWF0XG5cdCAqXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSAkZm9ybSBUYXJnZXQgZm9ybSBzZWxlY3RvciBvYmplY3QgdG8gYmUgc2VhcmNoZWQuXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBpZ25vcmVTZWxlY3RvciBTZWxlY3RvciBzdHJpbmcgdG8gYmUgaWdub3JlZC5cblx0ICpcblx0ICogQHJldHVybiB7b2JqZWN0fSBSZXR1cm5zIHRoZSBkYXRhIGZyb20gdGhlIGZvcm0gZWxlbWVudHMuXG5cdCAqXG5cdCAqIEBwdWJsaWNcblx0ICovXG5cdGV4cG9ydHMuZ2V0RGF0YSA9IGZ1bmN0aW9uKCRmb3JtLCBpZ25vcmUsIGFzSlNPTikge1xuXHRcdHZhciAkZWxlbWVudHMgPSAkZm9ybS5maW5kKCdpbnB1dCwgdGV4dGFyZWEsIHNlbGVjdCcpLFxuXHRcdFx0cmVzdWx0ID0ge307XG5cblx0XHRpZiAoaWdub3JlKSB7XG5cdFx0XHQkZWxlbWVudHMgPSAkZWxlbWVudHMuZmlsdGVyKCc6bm90KCcgKyBpZ25vcmUgKyAnKScpO1xuXHRcdH1cblxuXHRcdCRlbGVtZW50cy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyICRzZWxmID0gJCh0aGlzKSxcblx0XHRcdFx0dHlwZSA9ICRzZWxmLnByb3AoJ3RhZ05hbWUnKS50b0xvd2VyQ2FzZSgpLFxuXHRcdFx0XHRuYW1lID0gJHNlbGYuYXR0cignbmFtZScpLFxuXHRcdFx0XHRyZWdleCA9IG5ldyBSZWdFeHAoL1xcWyguKj8pXFxdL2cpLFxuXHRcdFx0XHRyZWdleFJlc3VsdCA9IHJlZ2V4LmV4ZWMobmFtZSksXG5cdFx0XHRcdHdhdGNoZG9nID0gNSxcblx0XHRcdFx0JHNlbGVjdGVkID0gbnVsbCxcblx0XHRcdFx0cmVzID0gbnVsbCxcblx0XHRcdFx0YmFzZSA9IG51bGwsXG5cdFx0XHRcdGxhc3RLZXkgPSBudWxsO1xuXG5cdFx0XHR0eXBlID0gKHR5cGUgIT09ICdpbnB1dCcpID8gdHlwZSA6ICRzZWxmLmF0dHIoJ3R5cGUnKS50b0xvd2VyQ2FzZSgpO1xuXG5cdFx0XHRpZiAocmVnZXhSZXN1bHQgIT09IG51bGwpIHtcblxuXHRcdFx0XHR2YXIgYmFzZW5hbWUgPSBuYW1lLnN1YnN0cmluZygwLCBuYW1lLnNlYXJjaCgnXFxcXFsnKSksXG5cdFx0XHRcdFx0a2V5cyA9IFtdO1xuXG5cdFx0XHRcdHJlc3VsdFtiYXNlbmFtZV0gPSByZXN1bHRbYmFzZW5hbWVdIHx8IChhc0pTT04gPyB7fSA6IFtdKTtcblx0XHRcdFx0YmFzZSA9IHJlc3VsdFtiYXNlbmFtZV07XG5cblx0XHRcdFx0ZG8ge1xuXHRcdFx0XHRcdGtleXMucHVzaChyZWdleFJlc3VsdFsxXSk7XG5cdFx0XHRcdFx0cmVnZXhSZXN1bHQgPSByZWdleC5leGVjKG5hbWUpO1xuXHRcdFx0XHRcdHdhdGNoZG9nIC09IDE7XG5cdFx0XHRcdH0gd2hpbGUgKHJlZ2V4UmVzdWx0ICE9PSBudWxsIHx8IHdhdGNoZG9nIDw9IDApO1xuXG5cdFx0XHRcdCQuZWFjaChrZXlzLCBmdW5jdGlvbihpLCB2KSB7XG5cdFx0XHRcdFx0dmFyIG5leHQgPSBrZXlzW2kgKyAxXTtcblx0XHRcdFx0XHR2ID0gdiB8fCAnMCc7XG5cblx0XHRcdFx0XHRpZiAodHlwZW9mIChuZXh0KSA9PT0gJ3N0cmluZycpIHtcblx0XHRcdFx0XHRcdGJhc2Vbdl0gPSBiYXNlW3ZdIHx8IChhc0pTT04gPyB7fSA6IFtdKTtcblx0XHRcdFx0XHRcdGJhc2UgPSBiYXNlW3ZdO1xuXHRcdFx0XHRcdH0gZWxzZSBpZiAodHlwZSAhPT0gJ3JhZGlvJykge1xuXHRcdFx0XHRcdFx0diA9ICh2ICYmIHYgIT09ICcwJykgPyB2IDpcblx0XHRcdFx0XHRcdCAgICAoYXNKU09OKSA/IE9iamVjdC5rZXlzKGJhc2UpLmxlbmd0aCA6IGJhc2UubGVuZ3RoO1xuXHRcdFx0XHRcdFx0YmFzZVt2XSA9IGJhc2Vbdl0gfHwgdW5kZWZpbmVkO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGxhc3RLZXkgPSB2O1xuXHRcdFx0XHR9KTtcblxuXHRcdFx0fVxuXG5cdFx0XHRzd2l0Y2ggKHR5cGUpIHtcblx0XHRcdFx0Y2FzZSAncmFkaW8nOlxuXHRcdFx0XHRcdHJlcyA9ICRlbGVtZW50c1xuXHRcdFx0XHRcdFx0LmZpbHRlcignaW5wdXRbbmFtZT1cIicgKyAkc2VsZi5hdHRyKCduYW1lJykgKyAnXCJdOmNoZWNrZWQnKVxuXHRcdFx0XHRcdFx0LnZhbCgpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdjaGVja2JveCc6XG5cdFx0XHRcdFx0cmVzID0gKCRzZWxmLnByb3AoJ2NoZWNrZWQnKSkgPyAkc2VsZi52YWwoKSA6IGZhbHNlO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlICdzZWxlY3QnOlxuXHRcdFx0XHRcdCRzZWxlY3RlZCA9ICRzZWxmLmZpbmQoJzpzZWxlY3RlZCcpO1xuXHRcdFx0XHRcdGlmICgkc2VsZWN0ZWQubGVuZ3RoID4gMSkge1xuXHRcdFx0XHRcdFx0cmVzID0gW107XG5cdFx0XHRcdFx0XHQkc2VsZWN0ZWQuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0cmVzLnB1c2goJCh0aGlzKS52YWwoKSk7XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0cmVzID0gJHNlbGVjdGVkLnZhbCgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAnYnV0dG9uJzpcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRpZiAobmFtZSkge1xuXHRcdFx0XHRcdFx0cmVzID0gJHNlbGYudmFsKCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0fVxuXG5cdFx0XHRpZiAoYmFzZSAhPT0gbnVsbCkge1xuXHRcdFx0XHRiYXNlW2xhc3RLZXldID0gcmVzO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmVzdWx0W25hbWVdID0gcmVzO1xuXHRcdFx0fVxuXG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gcmVzdWx0O1xuXHR9O1xuXG5cdC8qKlxuXHQgKiBSZXR1cm5zIHRoZSBmb3JtIGZpZWxkIHR5cGUuXG5cdCAqXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSAkZWxlbWVudCBFbGVtZW50IHNlbGVjdG9yIHRvIGJlIGNoZWNrZWQuXG5cdCAqXG5cdCAqIEByZXR1cm4ge3N0cmluZ30gUmV0dXJucyB0aGUgZmllbGQgdHlwZSBuYW1lIG9mIHRoZSBlbGVtZW50LlxuXHQgKlxuXHQgKiBAcHVibGljXG5cdCAqL1xuXHRleHBvcnRzLmdldEZpZWxkVHlwZSA9IGZ1bmN0aW9uKCRlbGVtZW50KSB7XG5cdFx0dmFyIHR5cGUgPSAkZWxlbWVudC5wcm9wKCd0YWdOYW1lJykudG9Mb3dlckNhc2UoKTtcblx0XHRyZXR1cm4gKHR5cGUgIT09ICdpbnB1dCcpID8gdHlwZSA6ICRlbGVtZW50LmF0dHIoJ3R5cGUnKS50b0xvd2VyQ2FzZSgpO1xuXHR9O1xuXG5cdC8qKlxuXHQgKiBBZGRzIGEgaGlkZGVuIGZpZWxkIHRvIHRoZSBwcm92aWRlZCB0YXJnZXQuXG5cdCAqXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSAkdGFyZ2V0IFRhcmdldCBlbGVtZW50IHRvIHByZXBlbmQgdGhlIGhpZGRlbiBmaWVsZCB0by5cblx0ICogQHBhcmFtIHtib29sZWFufSByZXBsYWNlIFNob3VsZCB0aGUgdGFyZ2V0IGVsZW1lbnQgYmUgcmVwbGFjZWQ/XG5cdCAqL1xuXHRleHBvcnRzLmFkZEhpZGRlbkJ5VXJsID0gZnVuY3Rpb24oJHRhcmdldCwgcmVwbGFjZSkge1xuXHRcdHZhciB1cmxQYXJhbSA9IF9nZXRVcmxQYXJhbXMobnVsbCksXG5cdFx0XHQkZmllbGQgPSBudWxsLFxuXHRcdFx0aGlkZGVucyA9ICcnLFxuXHRcdFx0dXBkYXRlID0gW107XG5cblx0XHQkLmVhY2godXJsUGFyYW0sIGZ1bmN0aW9uKGssIHYpIHtcblx0XHRcdGlmICh2KSB7XG5cdFx0XHRcdCRmaWVsZCA9ICR0YXJnZXQuZmluZCgnW25hbWU9XCInICsgayArICdcIl0nKTtcblxuXHRcdFx0XHRpZiAoJGZpZWxkLmxlbmd0aCA9PT0gMCkge1xuXHRcdFx0XHRcdGhpZGRlbnMgKz0gJzxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgbmFtZT1cIicgKyBrICsgJ1wiIHZhbHVlPVwiJyArIHYgKyAnXCIgLz4nO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHVwZGF0ZS5wdXNoKGssIHYpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRpZiAocmVwbGFjZSkge1xuXHRcdFx0ZXhwb3J0cy5wcmVmaWxsRm9ybSgkdGFyZ2V0LCB1cGRhdGUpO1xuXHRcdH1cblxuXHRcdCR0YXJnZXQucHJlcGVuZChoaWRkZW5zKTtcblx0fTtcblxuXHQvKipcblx0ICogUmVzZXRzIHRoZSB0aGUgcHJvdmlkZWQgdGFyZ2V0IGZvcm0uXG5cdCAqXG5cdCAqIFRoaXMgbWV0aG9kIHdpbGwgY2xlYXIgYWxsIHRleHRmaWVsZHMuIEFsbCByYWRpbyBidXR0b25zXG5cdCAqIGFuZCBjaGVja2JveGVzIHdpbGwgYmUgdW5jaGVja2VkLCBvbmx5IHRoZSBmaXJzdCBjaGVja2JveCBhbmQgXG5cdCAqIHJhZGlvIGJ1dHRvbiB3aWxsIGdldCBjaGVja2VkLiBcblx0ICogIFxuXHQgKiBAcGFyYW0ge29iamVjdH0gJHRhcmdldCBGb3JtIHRvIHJlc2V0LlxuXHQgKi9cblx0ZXhwb3J0cy5yZXNldCA9IGZ1bmN0aW9uKCR0YXJnZXQpIHtcblx0XHQkdGFyZ2V0XG5cdFx0XHQuZmluZCgnc2VsZWN0LCBpbnB1dCwgdGV4dGFyZWEnKVxuXHRcdFx0LmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHZhciAkc2VsZiA9ICQodGhpcyksXG5cdFx0XHRcdFx0dHlwZSA9IGV4cG9ydHMuZ2V0RmllbGRUeXBlKCRzZWxmKTtcblxuXHRcdFx0XHRzd2l0Y2ggKHR5cGUpIHtcblx0XHRcdFx0XHRjYXNlICdyYWRpbyc6XG5cdFx0XHRcdFx0XHQkdGFyZ2V0XG5cdFx0XHRcdFx0XHRcdC5maW5kKCdpbnB1dFtuYW1lPVwiJyArICRzZWxmLmF0dHIoJ25hbWUnKSArICdcIl06Y2hlY2tlZCcpXG5cdFx0XHRcdFx0XHRcdC5wcm9wKCdjaGVja2VkJywgZmFsc2UpXG5cdFx0XHRcdFx0XHRcdC5maXJzdCgpXG5cdFx0XHRcdFx0XHRcdC5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlICdjaGVja2JveCc6XG5cdFx0XHRcdFx0XHQkc2VsZi5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0Y2FzZSAnc2VsZWN0Jzpcblx0XHRcdFx0XHRcdCRzZWxmXG5cdFx0XHRcdFx0XHRcdC5jaGlsZHJlbigpXG5cdFx0XHRcdFx0XHRcdC5maXJzdCgpXG5cdFx0XHRcdFx0XHRcdC5wcm9wKCdzZWxlY3RlZCcsIHRydWUpO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0Y2FzZSAndGV4dGFyZWEnOlxuXHRcdFx0XHRcdFx0JHNlbGYudmFsKCcnKTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGNhc2UgJ3RleHQnOlxuXHRcdFx0XHRcdFx0JHNlbGYudmFsKCcnKTtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdH07XG5cbn0pKGpzZS5saWJzLmZvcm0pO1xuIl19
