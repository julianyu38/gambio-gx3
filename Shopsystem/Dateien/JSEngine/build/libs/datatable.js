'use strict';

/* --------------------------------------------------------------
 datatable.js 2016-07-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.datatable = jse.libs.datatable || {};

/**
 * ## DataTable Library
 *
 * This is a wrapper library for the manipulation of jQuery DataTables. Use the "create" method with DataTable 
 * configuration to initialize a table on your page. All you need when using this library is an empty `<table>` 
 * element. Visit the official website of DataTables to check examples and other information about the plugin.
 * 
 * {@link http://www.datatables.net Official DataTables Website}
 *
 * Notice: Make sure that you load the DataTables vendor files before using this module. 
 *
 * ### Examples
 * 
 * **Example - Create A New Instance**
 * ```javascript
 * var tableApi = jse.libs.datatable.create($('#my-table'), {
 *      ajax: 'http://shop.de/table-data.php',
 *      columns: [
 *          { title: 'Name', data: 'name' defaultContent: '...' },
 *          { title: 'Email', data: 'email' },
 *          { title: 'Actions', data: null, orderable: false, defaultContent: 'Add | Edit | Delete' },
 *      ]
 * });
 * ```
 *
 * **Example - Add Error Handler**
 * ```javascript
 * jse.libs.datatable.error($('#my-table'), function(event, settings, techNote, message) {
 *      // Log error in the JavaScript console.
 *      console.log('DataTable Error:', message);
 * });
 * ```
 *
 * @module JSE/Libs/datatable
 * @exports jse.libs.datatable
 * @requires jQuery-DataTables-Plugin
 */
(function (exports) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	var languages = {
		de: {
			'sEmptyTable': 'Keine Daten in der Tabelle vorhanden',
			'sInfo': '_START_ bis _END_ (von _TOTAL_)',
			'sInfoEmpty': '0 bis 0 von 0 Einträgen',
			'sInfoFiltered': '(gefiltert von _MAX_ Einträgen)',
			'sInfoPostFix': '',
			'sInfoThousands': '.',
			'sLengthMenu': '_MENU_ Einträge anzeigen',
			'sLoadingRecords': 'Wird geladen...',
			'sProcessing': 'Bitte warten...',
			'sSearch': 'Suchen',
			'sZeroRecords': 'Keine Einträge vorhanden.',
			'oPaginate': {
				'sFirst': 'Erste',
				'sPrevious': 'Zurück',
				'sNext': 'Nächste',
				'sLast': 'Letzte'
			},
			'oAria': {
				'sSortAscending': ': aktivieren, um Spalte aufsteigend zu sortieren',
				'sSortDescending': ': aktivieren, um Spalte absteigend zu sortieren'
			}
		},
		en: {
			'sEmptyTable': 'No data available in table',
			'sInfo': '_START_ to _END_ (of _TOTAL_)',
			'sInfoEmpty': 'Showing 0 to 0 of 0 entries',
			'sInfoFiltered': '(filtered from _MAX_ total entries)',
			'sInfoPostFix': '',
			'sInfoThousands': ',',
			'sLengthMenu': 'Show _MENU_ entries',
			'sLoadingRecords': 'Loading...',
			'sProcessing': 'Processing...',
			'sSearch': 'Search:',
			'sZeroRecords': 'No matching records found',
			'oPaginate': {
				'sFirst': 'First',
				'sLast': 'Last',
				'sNext': 'Next',
				'sPrevious': 'Previous'
			},
			'oAria': {
				'sSortAscending': ': activate to sort column ascending',
				'sSortDescending': ': activate to sort column descending'
			}
		}
	};

	// ------------------------------------------------------------------------
	// FUNCTIONALITY
	// ------------------------------------------------------------------------

	/**
  * Reorder the table columns as defined in the active columns array.
  *
  * @param {jQuery} $target Table jQuery selector object.
  * @param {Object} columnDefinitions Array containing the DataTable column definitions.
  * @param {Array} activeColumnNames Array containing the slug-names of the active columns.
  *
  * @return {Array} Returns array with the active column definitions ready to use in DataTable.columns option.
  *
  * @private
  */
	function _reorderColumns($target, columnDefinitions, activeColumnNames) {
		activeColumnNames.unshift('checkbox');
		activeColumnNames.push('actions');

		// Hide the table header cells that are not active.
		$.each(columnDefinitions, function (index, columnDefinition) {
			$target.find('thead tr').each(function () {
				var $headerCell = $(this).find('[data-column-name="' + columnDefinition.name + '"]');

				if (columnDefinition.data !== null && activeColumnNames.indexOf(columnDefinition.name) === -1) {
					$headerCell.hide();
				}
			});
		});

		// Prepare the active column definitions.
		var finalColumnDefinitions = [],
		    columnIndexes = [];

		$.each(activeColumnNames, function (index, name) {
			$.each(columnDefinitions, function (index, columnDefinition) {
				if (columnDefinition.name === name) {
					// Add the active column definition in the "finalColumnDefinitions" array.
					finalColumnDefinitions.push(columnDefinition);
					var headerCellIndex = $target.find('thead:first tr:first [data-column-name="' + columnDefinition.name + '"]').index();
					columnIndexes.push(headerCellIndex);
					return true; // continue
				}
			});
		});

		finalColumnDefinitions.sort(function (a, b) {
			var aIndex = activeColumnNames.indexOf(a.name);
			var bIndex = activeColumnNames.indexOf(b.name);

			if (aIndex < bIndex) {
				return -1;
			} else if (aIndex > bIndex) {
				return 1;
			} else {
				return 0;
			}
		});

		// Reorder the table header elements depending the activeColumnNames order.
		$target.find('thead tr').each(function () {
			var _this = this;

			var activeColumnSelections = [$(this).find('th:first')];

			// Sort the columns in the correct order.
			columnIndexes.forEach(function (index) {
				var $headerCell = $(_this).find('th').eq(index);
				activeColumnSelections.push($headerCell);
			});

			// Move the columns to their final position.
			activeColumnSelections.forEach(function ($headerCell, index) {
				if (index === 0) {
					return true;
				}

				$headerCell.insertAfter(activeColumnSelections[index - 1]);
			});
		});

		return finalColumnDefinitions;
	}

	/**
  * Creates a DataTable Instance
  *
  * This method will create a new instance of datatable into a `<table>` element. It enables
  * developers to easily pass the configuration needed for different and more special situations.
  *
  * @param {jQuery} $target jQuery object for the target table.
  * @param {Object} configuration DataTables configuration applied on the new instance.
  *
  * @return {DataTable} Returns the DataTable API instance (different from the jQuery object).
  */
	exports.create = function ($target, configuration) {
		return $target.DataTable(configuration);
	};

	/**
  * Sets the error handler for specific DataTable.
  *
  * DataTables provide a useful mechanism that enables developers to control errors during data parsing.
  * If there is an error in the AJAX response or some data are invalid in the JavaScript code you can use 
  * this method to control the behavior of the app and show or log the error messages.
  *
  * {@link http://datatables.net/reference/event/error}
  *
  * @param {jQuery} $target jQuery object for the target table.
  * @param {Object} callback Provide a callback method called with the "event", "settings", "techNote", 
  * "message" arguments (see provided link).
  */
	exports.error = function ($target, callback) {
		$.fn.dataTable.ext.errMode = 'none';
		$target.on('error.dt', callback).on('xhr.dt', function (event, settings, json, xhr) {
			if (json.exception === true) {
				callback(event, settings, null, json.message);
			}
		});
	};

	/**
  * Sets the callback method when ajax load of data is complete.
  *
  * This method is useful for checking PHP errors or modifying the data before
  * they are displayed to the server.
  *
  * {@link http://datatables.net/reference/event/xhr}
  *
  * @param {jQuery} $target jQuery object for the target table.
  * @param {Function} callback Provide a callback method called with the "event", "settings", "techNote", 
  * "message" arguments (see provided link).
  */
	exports.ajaxComplete = function ($target, callback) {
		$target.on('xhr.dt', callback);
	};

	/**
  * Sets the table column to be displayed as an index.
  *
  * This method will easily enable you to set a column as an index column, used
  * for numbering the table rows regardless of the search, sorting and row count.
  *
  * {@link http://www.datatables.net/examples/api/counter_columns.html}
  *
  * @param {jQuery} $target jQuery object for the target table.
  * @param {Number} columnIndex Zero based index of the column to be indexed.
  */
	exports.indexColumn = function ($target, columnIndex) {
		$target.on('order.dt search.dt', function () {
			$target.DataTable().column(columnIndex, {
				search: 'applied',
				order: 'applied'
			}).nodes().each(function (cell, index) {
				cell.innerHTML = index + 1;
			});
		});
	};

	/**
  * Returns the german translation of the DataTables
  *
  * This method provides a quick way to get the language JSON without having to perform
  * and AJAX request to the server. If you setup your DataTable manually you can set the
  * "language" attribute with this method.
  *
  * @deprecated Since v1.4, use the "getTranslations" method instead.
  *
  * @return {Object} Returns the german translation, must be the same as the "german.lang.json" file.
  */
	exports.getGermanTranslation = function () {
		jse.core.debug.warn('DataTables Library: the getGermanTranslation method is deprecated and will be removed ' + 'in JSE v1.5, please use the "getTranslations" method instead.');
		return languages.de;
	};

	/**
  * Get the DataTables translation depending the language code parameter.
  *
  * @param {String} languageCode Provide 'de' or 'en' (you can also use the jse.core.config.get('languageCode') to
  * get the current language code).
  *
  * @return {Object} Returns the translation strings in an object literal as described by the official DataTables
  * documentation.
  *
  * {@link https://www.datatables.net/plug-ins/i18n}
  */
	exports.getTranslations = function (languageCode) {
		if (languages[languageCode] === undefined) {
			jse.core.debug.warn('DataTables Library: The requested DataTables translation was not found:', languageCode);
			languageCode = 'en';
		}

		return languages[languageCode];
	};

	/**
  * Prepare table columns.
  *
  * This method will convert the column definitions to a DataTable compatible format and also reorder
  * the table header cells of the "thead" element.
  *
  * @param {jQuery} $target Table jQuery selector object.
  * @param {Object} columnDefinitions Array containing the DataTable column definitions.
  * @param {String[]} activeColumnNames Array containing the slug-names of the active columns.
  *
  * @return {Object[]} Returns array with the active column definitions ready to use in DataTable.columns option.
  */
	exports.prepareColumns = function ($target, columnDefinitions, activeColumnNames) {
		var convertedColumnDefinitions = [];

		for (var columnName in columnDefinitions) {
			var columnDefinition = columnDefinitions[columnName];
			columnDefinition.name = columnName;
			convertedColumnDefinitions.push(columnDefinition);
		}

		return _reorderColumns($target, convertedColumnDefinitions, activeColumnNames);
	};
})(jse.libs.datatable);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGF0YWJsZS5qcyJdLCJuYW1lcyI6WyJqc2UiLCJsaWJzIiwiZGF0YXRhYmxlIiwiZXhwb3J0cyIsImxhbmd1YWdlcyIsImRlIiwiZW4iLCJfcmVvcmRlckNvbHVtbnMiLCIkdGFyZ2V0IiwiY29sdW1uRGVmaW5pdGlvbnMiLCJhY3RpdmVDb2x1bW5OYW1lcyIsInVuc2hpZnQiLCJwdXNoIiwiJCIsImVhY2giLCJpbmRleCIsImNvbHVtbkRlZmluaXRpb24iLCJmaW5kIiwiJGhlYWRlckNlbGwiLCJuYW1lIiwiZGF0YSIsImluZGV4T2YiLCJoaWRlIiwiZmluYWxDb2x1bW5EZWZpbml0aW9ucyIsImNvbHVtbkluZGV4ZXMiLCJoZWFkZXJDZWxsSW5kZXgiLCJzb3J0IiwiYSIsImIiLCJhSW5kZXgiLCJiSW5kZXgiLCJhY3RpdmVDb2x1bW5TZWxlY3Rpb25zIiwiZm9yRWFjaCIsImVxIiwiaW5zZXJ0QWZ0ZXIiLCJjcmVhdGUiLCJjb25maWd1cmF0aW9uIiwiRGF0YVRhYmxlIiwiZXJyb3IiLCJjYWxsYmFjayIsImZuIiwiZGF0YVRhYmxlIiwiZXh0IiwiZXJyTW9kZSIsIm9uIiwiZXZlbnQiLCJzZXR0aW5ncyIsImpzb24iLCJ4aHIiLCJleGNlcHRpb24iLCJtZXNzYWdlIiwiYWpheENvbXBsZXRlIiwiaW5kZXhDb2x1bW4iLCJjb2x1bW5JbmRleCIsImNvbHVtbiIsInNlYXJjaCIsIm9yZGVyIiwibm9kZXMiLCJjZWxsIiwiaW5uZXJIVE1MIiwiZ2V0R2VybWFuVHJhbnNsYXRpb24iLCJjb3JlIiwiZGVidWciLCJ3YXJuIiwiZ2V0VHJhbnNsYXRpb25zIiwibGFuZ3VhZ2VDb2RlIiwidW5kZWZpbmVkIiwicHJlcGFyZUNvbHVtbnMiLCJjb252ZXJ0ZWRDb2x1bW5EZWZpbml0aW9ucyIsImNvbHVtbk5hbWUiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQyxTQUFULEdBQXFCRixJQUFJQyxJQUFKLENBQVNDLFNBQVQsSUFBc0IsRUFBM0M7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQ0MsV0FBU0MsT0FBVCxFQUFrQjs7QUFFbEI7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEtBQUlDLFlBQVk7QUFDZkMsTUFBSTtBQUNILGtCQUFlLHNDQURaO0FBRUgsWUFBUyxpQ0FGTjtBQUdILGlCQUFjLHlCQUhYO0FBSUgsb0JBQWlCLGlDQUpkO0FBS0gsbUJBQWdCLEVBTGI7QUFNSCxxQkFBa0IsR0FOZjtBQU9ILGtCQUFlLDBCQVBaO0FBUUgsc0JBQW1CLGlCQVJoQjtBQVNILGtCQUFlLGlCQVRaO0FBVUgsY0FBVyxRQVZSO0FBV0gsbUJBQWdCLDJCQVhiO0FBWUgsZ0JBQWE7QUFDWixjQUFVLE9BREU7QUFFWixpQkFBYSxRQUZEO0FBR1osYUFBUyxTQUhHO0FBSVosYUFBUztBQUpHLElBWlY7QUFrQkgsWUFBUztBQUNSLHNCQUFrQixrREFEVjtBQUVSLHVCQUFtQjtBQUZYO0FBbEJOLEdBRFc7QUF3QmZDLE1BQUk7QUFDSCxrQkFBZSw0QkFEWjtBQUVILFlBQVMsK0JBRk47QUFHSCxpQkFBYyw2QkFIWDtBQUlILG9CQUFpQixxQ0FKZDtBQUtILG1CQUFnQixFQUxiO0FBTUgscUJBQWtCLEdBTmY7QUFPSCxrQkFBZSxxQkFQWjtBQVFILHNCQUFtQixZQVJoQjtBQVNILGtCQUFlLGVBVFo7QUFVSCxjQUFXLFNBVlI7QUFXSCxtQkFBZ0IsMkJBWGI7QUFZSCxnQkFBYTtBQUNaLGNBQVUsT0FERTtBQUVaLGFBQVMsTUFGRztBQUdaLGFBQVMsTUFIRztBQUlaLGlCQUFhO0FBSkQsSUFaVjtBQWtCSCxZQUFTO0FBQ1Isc0JBQWtCLHFDQURWO0FBRVIsdUJBQW1CO0FBRlg7QUFsQk47QUF4QlcsRUFBaEI7O0FBaURBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7QUFXQSxVQUFTQyxlQUFULENBQXlCQyxPQUF6QixFQUFrQ0MsaUJBQWxDLEVBQXFEQyxpQkFBckQsRUFBd0U7QUFDdkVBLG9CQUFrQkMsT0FBbEIsQ0FBMEIsVUFBMUI7QUFDQUQsb0JBQWtCRSxJQUFsQixDQUF1QixTQUF2Qjs7QUFFQTtBQUNBQyxJQUFFQyxJQUFGLENBQU9MLGlCQUFQLEVBQTBCLFVBQUNNLEtBQUQsRUFBUUMsZ0JBQVIsRUFBNkI7QUFDdERSLFdBQVFTLElBQVIsQ0FBYSxVQUFiLEVBQXlCSCxJQUF6QixDQUE4QixZQUFXO0FBQ3hDLFFBQUlJLGNBQWNMLEVBQUUsSUFBRixFQUFRSSxJQUFSLHlCQUFtQ0QsaUJBQWlCRyxJQUFwRCxRQUFsQjs7QUFFQSxRQUFJSCxpQkFBaUJJLElBQWpCLEtBQTBCLElBQTFCLElBQWtDVixrQkFBa0JXLE9BQWxCLENBQTBCTCxpQkFBaUJHLElBQTNDLE1BQXFELENBQUMsQ0FBNUYsRUFBK0Y7QUFDOUZELGlCQUFZSSxJQUFaO0FBQ0E7QUFDRCxJQU5EO0FBT0EsR0FSRDs7QUFVQTtBQUNBLE1BQUlDLHlCQUF5QixFQUE3QjtBQUFBLE1BQ0NDLGdCQUFnQixFQURqQjs7QUFHQVgsSUFBRUMsSUFBRixDQUFPSixpQkFBUCxFQUEwQixVQUFDSyxLQUFELEVBQVFJLElBQVIsRUFBaUI7QUFDMUNOLEtBQUVDLElBQUYsQ0FBT0wsaUJBQVAsRUFBMEIsVUFBQ00sS0FBRCxFQUFRQyxnQkFBUixFQUE2QjtBQUN0RCxRQUFJQSxpQkFBaUJHLElBQWpCLEtBQTBCQSxJQUE5QixFQUFvQztBQUNuQztBQUNBSSw0QkFBdUJYLElBQXZCLENBQTRCSSxnQkFBNUI7QUFDQSxTQUFNUyxrQkFBa0JqQixRQUN0QlMsSUFEc0IsOENBQzBCRCxpQkFBaUJHLElBRDNDLFNBRXRCSixLQUZzQixFQUF4QjtBQUdBUyxtQkFBY1osSUFBZCxDQUFtQmEsZUFBbkI7QUFDQSxZQUFPLElBQVAsQ0FQbUMsQ0FPdEI7QUFDYjtBQUNELElBVkQ7QUFXQSxHQVpEOztBQWNBRix5QkFBdUJHLElBQXZCLENBQTRCLFVBQUNDLENBQUQsRUFBSUMsQ0FBSixFQUFVO0FBQ3JDLE9BQU1DLFNBQVNuQixrQkFBa0JXLE9BQWxCLENBQTBCTSxFQUFFUixJQUE1QixDQUFmO0FBQ0EsT0FBTVcsU0FBU3BCLGtCQUFrQlcsT0FBbEIsQ0FBMEJPLEVBQUVULElBQTVCLENBQWY7O0FBRUEsT0FBSVUsU0FBU0MsTUFBYixFQUFxQjtBQUNwQixXQUFPLENBQUMsQ0FBUjtBQUNBLElBRkQsTUFFTyxJQUFJRCxTQUFTQyxNQUFiLEVBQXFCO0FBQzNCLFdBQU8sQ0FBUDtBQUNBLElBRk0sTUFFQTtBQUNOLFdBQU8sQ0FBUDtBQUNBO0FBQ0QsR0FYRDs7QUFhQTtBQUNBdEIsVUFBUVMsSUFBUixDQUFhLFVBQWIsRUFBeUJILElBQXpCLENBQThCLFlBQVc7QUFBQTs7QUFDeEMsT0FBSWlCLHlCQUF5QixDQUFDbEIsRUFBRSxJQUFGLEVBQVFJLElBQVIsQ0FBYSxVQUFiLENBQUQsQ0FBN0I7O0FBRUE7QUFDQU8saUJBQWNRLE9BQWQsQ0FBc0IsVUFBQ2pCLEtBQUQsRUFBVztBQUNoQyxRQUFJRyxjQUFjTCxFQUFFLEtBQUYsRUFBUUksSUFBUixDQUFhLElBQWIsRUFBbUJnQixFQUFuQixDQUFzQmxCLEtBQXRCLENBQWxCO0FBQ0FnQiwyQkFBdUJuQixJQUF2QixDQUE0Qk0sV0FBNUI7QUFDQSxJQUhEOztBQUtBO0FBQ0FhLDBCQUF1QkMsT0FBdkIsQ0FBK0IsVUFBU2QsV0FBVCxFQUFzQkgsS0FBdEIsRUFBNkI7QUFDM0QsUUFBSUEsVUFBVSxDQUFkLEVBQWlCO0FBQ2hCLFlBQU8sSUFBUDtBQUNBOztBQUVERyxnQkFBWWdCLFdBQVosQ0FBd0JILHVCQUF1QmhCLFFBQVEsQ0FBL0IsQ0FBeEI7QUFDQSxJQU5EO0FBT0EsR0FqQkQ7O0FBbUJBLFNBQU9RLHNCQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs7O0FBV0FwQixTQUFRZ0MsTUFBUixHQUFpQixVQUFTM0IsT0FBVCxFQUFrQjRCLGFBQWxCLEVBQWlDO0FBQ2pELFNBQU81QixRQUFRNkIsU0FBUixDQUFrQkQsYUFBbEIsQ0FBUDtBQUNBLEVBRkQ7O0FBSUE7Ozs7Ozs7Ozs7Ozs7QUFhQWpDLFNBQVFtQyxLQUFSLEdBQWdCLFVBQVM5QixPQUFULEVBQWtCK0IsUUFBbEIsRUFBNEI7QUFDM0MxQixJQUFFMkIsRUFBRixDQUFLQyxTQUFMLENBQWVDLEdBQWYsQ0FBbUJDLE9BQW5CLEdBQTZCLE1BQTdCO0FBQ0FuQyxVQUNFb0MsRUFERixDQUNLLFVBREwsRUFDaUJMLFFBRGpCLEVBRUVLLEVBRkYsQ0FFSyxRQUZMLEVBRWUsVUFBQ0MsS0FBRCxFQUFRQyxRQUFSLEVBQWtCQyxJQUFsQixFQUF3QkMsR0FBeEIsRUFBZ0M7QUFDN0MsT0FBSUQsS0FBS0UsU0FBTCxLQUFtQixJQUF2QixFQUE2QjtBQUM1QlYsYUFBU00sS0FBVCxFQUFnQkMsUUFBaEIsRUFBMEIsSUFBMUIsRUFBZ0NDLEtBQUtHLE9BQXJDO0FBQ0E7QUFDRCxHQU5GO0FBT0EsRUFURDs7QUFXQTs7Ozs7Ozs7Ozs7O0FBWUEvQyxTQUFRZ0QsWUFBUixHQUF1QixVQUFTM0MsT0FBVCxFQUFrQitCLFFBQWxCLEVBQTRCO0FBQ2xEL0IsVUFBUW9DLEVBQVIsQ0FBVyxRQUFYLEVBQXFCTCxRQUFyQjtBQUNBLEVBRkQ7O0FBSUE7Ozs7Ozs7Ozs7O0FBV0FwQyxTQUFRaUQsV0FBUixHQUFzQixVQUFTNUMsT0FBVCxFQUFrQjZDLFdBQWxCLEVBQStCO0FBQ3BEN0MsVUFBUW9DLEVBQVIsQ0FBVyxvQkFBWCxFQUFpQyxZQUFXO0FBQzNDcEMsV0FBUTZCLFNBQVIsR0FBb0JpQixNQUFwQixDQUEyQkQsV0FBM0IsRUFBd0M7QUFDdkNFLFlBQVEsU0FEK0I7QUFFdkNDLFdBQU87QUFGZ0MsSUFBeEMsRUFHR0MsS0FISCxHQUdXM0MsSUFIWCxDQUdnQixVQUFTNEMsSUFBVCxFQUFlM0MsS0FBZixFQUFzQjtBQUNyQzJDLFNBQUtDLFNBQUwsR0FBaUI1QyxRQUFRLENBQXpCO0FBQ0EsSUFMRDtBQU1BLEdBUEQ7QUFRQSxFQVREOztBQVdBOzs7Ozs7Ozs7OztBQVdBWixTQUFReUQsb0JBQVIsR0FBK0IsWUFBVztBQUN6QzVELE1BQUk2RCxJQUFKLENBQVNDLEtBQVQsQ0FBZUMsSUFBZixDQUFvQiwyRkFDakIsK0RBREg7QUFFQSxTQUFPM0QsVUFBVUMsRUFBakI7QUFDQSxFQUpEOztBQU1BOzs7Ozs7Ozs7OztBQVdBRixTQUFRNkQsZUFBUixHQUEwQixVQUFTQyxZQUFULEVBQXVCO0FBQ2hELE1BQUk3RCxVQUFVNkQsWUFBVixNQUE0QkMsU0FBaEMsRUFBMkM7QUFDMUNsRSxPQUFJNkQsSUFBSixDQUFTQyxLQUFULENBQWVDLElBQWYsQ0FBb0IseUVBQXBCLEVBQStGRSxZQUEvRjtBQUNBQSxrQkFBZSxJQUFmO0FBQ0E7O0FBRUQsU0FBTzdELFVBQVU2RCxZQUFWLENBQVA7QUFDQSxFQVBEOztBQVNBOzs7Ozs7Ozs7Ozs7QUFZQTlELFNBQVFnRSxjQUFSLEdBQXlCLFVBQVMzRCxPQUFULEVBQWtCQyxpQkFBbEIsRUFBcUNDLGlCQUFyQyxFQUF3RDtBQUNoRixNQUFJMEQsNkJBQTZCLEVBQWpDOztBQUVBLE9BQUssSUFBSUMsVUFBVCxJQUF1QjVELGlCQUF2QixFQUEwQztBQUN6QyxPQUFJTyxtQkFBbUJQLGtCQUFrQjRELFVBQWxCLENBQXZCO0FBQ0FyRCxvQkFBaUJHLElBQWpCLEdBQXdCa0QsVUFBeEI7QUFDQUQsOEJBQTJCeEQsSUFBM0IsQ0FBZ0NJLGdCQUFoQztBQUNBOztBQUVELFNBQU9ULGdCQUFnQkMsT0FBaEIsRUFBeUI0RCwwQkFBekIsRUFBcUQxRCxpQkFBckQsQ0FBUDtBQUNBLEVBVkQ7QUFZQSxDQXZSQSxFQXVSQ1YsSUFBSUMsSUFBSixDQUFTQyxTQXZSVixDQUFEIiwiZmlsZSI6ImRhdGF0YWJsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gZGF0YXRhYmxlLmpzIDIwMTYtMDctMTFcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UubGlicy5kYXRhdGFibGUgPSBqc2UubGlicy5kYXRhdGFibGUgfHwge307XG5cbi8qKlxuICogIyMgRGF0YVRhYmxlIExpYnJhcnlcbiAqXG4gKiBUaGlzIGlzIGEgd3JhcHBlciBsaWJyYXJ5IGZvciB0aGUgbWFuaXB1bGF0aW9uIG9mIGpRdWVyeSBEYXRhVGFibGVzLiBVc2UgdGhlIFwiY3JlYXRlXCIgbWV0aG9kIHdpdGggRGF0YVRhYmxlIFxuICogY29uZmlndXJhdGlvbiB0byBpbml0aWFsaXplIGEgdGFibGUgb24geW91ciBwYWdlLiBBbGwgeW91IG5lZWQgd2hlbiB1c2luZyB0aGlzIGxpYnJhcnkgaXMgYW4gZW1wdHkgYDx0YWJsZT5gIFxuICogZWxlbWVudC4gVmlzaXQgdGhlIG9mZmljaWFsIHdlYnNpdGUgb2YgRGF0YVRhYmxlcyB0byBjaGVjayBleGFtcGxlcyBhbmQgb3RoZXIgaW5mb3JtYXRpb24gYWJvdXQgdGhlIHBsdWdpbi5cbiAqIFxuICoge0BsaW5rIGh0dHA6Ly93d3cuZGF0YXRhYmxlcy5uZXQgT2ZmaWNpYWwgRGF0YVRhYmxlcyBXZWJzaXRlfVxuICpcbiAqIE5vdGljZTogTWFrZSBzdXJlIHRoYXQgeW91IGxvYWQgdGhlIERhdGFUYWJsZXMgdmVuZG9yIGZpbGVzIGJlZm9yZSB1c2luZyB0aGlzIG1vZHVsZS4gXG4gKlxuICogIyMjIEV4YW1wbGVzXG4gKiBcbiAqICoqRXhhbXBsZSAtIENyZWF0ZSBBIE5ldyBJbnN0YW5jZSoqXG4gKiBgYGBqYXZhc2NyaXB0XG4gKiB2YXIgdGFibGVBcGkgPSBqc2UubGlicy5kYXRhdGFibGUuY3JlYXRlKCQoJyNteS10YWJsZScpLCB7XG4gKiAgICAgIGFqYXg6ICdodHRwOi8vc2hvcC5kZS90YWJsZS1kYXRhLnBocCcsXG4gKiAgICAgIGNvbHVtbnM6IFtcbiAqICAgICAgICAgIHsgdGl0bGU6ICdOYW1lJywgZGF0YTogJ25hbWUnIGRlZmF1bHRDb250ZW50OiAnLi4uJyB9LFxuICogICAgICAgICAgeyB0aXRsZTogJ0VtYWlsJywgZGF0YTogJ2VtYWlsJyB9LFxuICogICAgICAgICAgeyB0aXRsZTogJ0FjdGlvbnMnLCBkYXRhOiBudWxsLCBvcmRlcmFibGU6IGZhbHNlLCBkZWZhdWx0Q29udGVudDogJ0FkZCB8IEVkaXQgfCBEZWxldGUnIH0sXG4gKiAgICAgIF1cbiAqIH0pO1xuICogYGBgXG4gKlxuICogKipFeGFtcGxlIC0gQWRkIEVycm9yIEhhbmRsZXIqKlxuICogYGBgamF2YXNjcmlwdFxuICoganNlLmxpYnMuZGF0YXRhYmxlLmVycm9yKCQoJyNteS10YWJsZScpLCBmdW5jdGlvbihldmVudCwgc2V0dGluZ3MsIHRlY2hOb3RlLCBtZXNzYWdlKSB7XG4gKiAgICAgIC8vIExvZyBlcnJvciBpbiB0aGUgSmF2YVNjcmlwdCBjb25zb2xlLlxuICogICAgICBjb25zb2xlLmxvZygnRGF0YVRhYmxlIEVycm9yOicsIG1lc3NhZ2UpO1xuICogfSk7XG4gKiBgYGBcbiAqXG4gKiBAbW9kdWxlIEpTRS9MaWJzL2RhdGF0YWJsZVxuICogQGV4cG9ydHMganNlLmxpYnMuZGF0YXRhYmxlXG4gKiBAcmVxdWlyZXMgalF1ZXJ5LURhdGFUYWJsZXMtUGx1Z2luXG4gKi9cbihmdW5jdGlvbihleHBvcnRzKSB7XG5cdFxuXHQndXNlIHN0cmljdCc7XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gVkFSSUFCTEVTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0bGV0IGxhbmd1YWdlcyA9IHtcblx0XHRkZToge1xuXHRcdFx0J3NFbXB0eVRhYmxlJzogJ0tlaW5lIERhdGVuIGluIGRlciBUYWJlbGxlIHZvcmhhbmRlbicsXG5cdFx0XHQnc0luZm8nOiAnX1NUQVJUXyBiaXMgX0VORF8gKHZvbiBfVE9UQUxfKScsXG5cdFx0XHQnc0luZm9FbXB0eSc6ICcwIGJpcyAwIHZvbiAwIEVpbnRyw6RnZW4nLFxuXHRcdFx0J3NJbmZvRmlsdGVyZWQnOiAnKGdlZmlsdGVydCB2b24gX01BWF8gRWludHLDpGdlbiknLFxuXHRcdFx0J3NJbmZvUG9zdEZpeCc6ICcnLFxuXHRcdFx0J3NJbmZvVGhvdXNhbmRzJzogJy4nLFxuXHRcdFx0J3NMZW5ndGhNZW51JzogJ19NRU5VXyBFaW50csOkZ2UgYW56ZWlnZW4nLFxuXHRcdFx0J3NMb2FkaW5nUmVjb3Jkcyc6ICdXaXJkIGdlbGFkZW4uLi4nLFxuXHRcdFx0J3NQcm9jZXNzaW5nJzogJ0JpdHRlIHdhcnRlbi4uLicsXG5cdFx0XHQnc1NlYXJjaCc6ICdTdWNoZW4nLFxuXHRcdFx0J3NaZXJvUmVjb3Jkcyc6ICdLZWluZSBFaW50csOkZ2Ugdm9yaGFuZGVuLicsXG5cdFx0XHQnb1BhZ2luYXRlJzoge1xuXHRcdFx0XHQnc0ZpcnN0JzogJ0Vyc3RlJyxcblx0XHRcdFx0J3NQcmV2aW91cyc6ICdadXLDvGNrJyxcblx0XHRcdFx0J3NOZXh0JzogJ07DpGNoc3RlJyxcblx0XHRcdFx0J3NMYXN0JzogJ0xldHp0ZSdcblx0XHRcdH0sXG5cdFx0XHQnb0FyaWEnOiB7XG5cdFx0XHRcdCdzU29ydEFzY2VuZGluZyc6ICc6IGFrdGl2aWVyZW4sIHVtIFNwYWx0ZSBhdWZzdGVpZ2VuZCB6dSBzb3J0aWVyZW4nLFxuXHRcdFx0XHQnc1NvcnREZXNjZW5kaW5nJzogJzogYWt0aXZpZXJlbiwgdW0gU3BhbHRlIGFic3RlaWdlbmQgenUgc29ydGllcmVuJ1xuXHRcdFx0fVxuXHRcdH0sXG5cdFx0ZW46IHtcblx0XHRcdCdzRW1wdHlUYWJsZSc6ICdObyBkYXRhIGF2YWlsYWJsZSBpbiB0YWJsZScsXG5cdFx0XHQnc0luZm8nOiAnX1NUQVJUXyB0byBfRU5EXyAob2YgX1RPVEFMXyknLFxuXHRcdFx0J3NJbmZvRW1wdHknOiAnU2hvd2luZyAwIHRvIDAgb2YgMCBlbnRyaWVzJyxcblx0XHRcdCdzSW5mb0ZpbHRlcmVkJzogJyhmaWx0ZXJlZCBmcm9tIF9NQVhfIHRvdGFsIGVudHJpZXMpJyxcblx0XHRcdCdzSW5mb1Bvc3RGaXgnOiAnJyxcblx0XHRcdCdzSW5mb1Rob3VzYW5kcyc6ICcsJyxcblx0XHRcdCdzTGVuZ3RoTWVudSc6ICdTaG93IF9NRU5VXyBlbnRyaWVzJyxcblx0XHRcdCdzTG9hZGluZ1JlY29yZHMnOiAnTG9hZGluZy4uLicsXG5cdFx0XHQnc1Byb2Nlc3NpbmcnOiAnUHJvY2Vzc2luZy4uLicsXG5cdFx0XHQnc1NlYXJjaCc6ICdTZWFyY2g6Jyxcblx0XHRcdCdzWmVyb1JlY29yZHMnOiAnTm8gbWF0Y2hpbmcgcmVjb3JkcyBmb3VuZCcsXG5cdFx0XHQnb1BhZ2luYXRlJzoge1xuXHRcdFx0XHQnc0ZpcnN0JzogJ0ZpcnN0Jyxcblx0XHRcdFx0J3NMYXN0JzogJ0xhc3QnLFxuXHRcdFx0XHQnc05leHQnOiAnTmV4dCcsXG5cdFx0XHRcdCdzUHJldmlvdXMnOiAnUHJldmlvdXMnXG5cdFx0XHR9LFxuXHRcdFx0J29BcmlhJzoge1xuXHRcdFx0XHQnc1NvcnRBc2NlbmRpbmcnOiAnOiBhY3RpdmF0ZSB0byBzb3J0IGNvbHVtbiBhc2NlbmRpbmcnLFxuXHRcdFx0XHQnc1NvcnREZXNjZW5kaW5nJzogJzogYWN0aXZhdGUgdG8gc29ydCBjb2x1bW4gZGVzY2VuZGluZydcblx0XHRcdH1cblx0XHR9XG5cdH07XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gRlVOQ1RJT05BTElUWVxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XG5cdC8qKlxuXHQgKiBSZW9yZGVyIHRoZSB0YWJsZSBjb2x1bW5zIGFzIGRlZmluZWQgaW4gdGhlIGFjdGl2ZSBjb2x1bW5zIGFycmF5LlxuXHQgKlxuXHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRhcmdldCBUYWJsZSBqUXVlcnkgc2VsZWN0b3Igb2JqZWN0LlxuXHQgKiBAcGFyYW0ge09iamVjdH0gY29sdW1uRGVmaW5pdGlvbnMgQXJyYXkgY29udGFpbmluZyB0aGUgRGF0YVRhYmxlIGNvbHVtbiBkZWZpbml0aW9ucy5cblx0ICogQHBhcmFtIHtBcnJheX0gYWN0aXZlQ29sdW1uTmFtZXMgQXJyYXkgY29udGFpbmluZyB0aGUgc2x1Zy1uYW1lcyBvZiB0aGUgYWN0aXZlIGNvbHVtbnMuXG5cdCAqXG5cdCAqIEByZXR1cm4ge0FycmF5fSBSZXR1cm5zIGFycmF5IHdpdGggdGhlIGFjdGl2ZSBjb2x1bW4gZGVmaW5pdGlvbnMgcmVhZHkgdG8gdXNlIGluIERhdGFUYWJsZS5jb2x1bW5zIG9wdGlvbi5cblx0ICpcblx0ICogQHByaXZhdGVcblx0ICovXG5cdGZ1bmN0aW9uIF9yZW9yZGVyQ29sdW1ucygkdGFyZ2V0LCBjb2x1bW5EZWZpbml0aW9ucywgYWN0aXZlQ29sdW1uTmFtZXMpIHtcblx0XHRhY3RpdmVDb2x1bW5OYW1lcy51bnNoaWZ0KCdjaGVja2JveCcpO1xuXHRcdGFjdGl2ZUNvbHVtbk5hbWVzLnB1c2goJ2FjdGlvbnMnKTtcblx0XHRcblx0XHQvLyBIaWRlIHRoZSB0YWJsZSBoZWFkZXIgY2VsbHMgdGhhdCBhcmUgbm90IGFjdGl2ZS5cblx0XHQkLmVhY2goY29sdW1uRGVmaW5pdGlvbnMsIChpbmRleCwgY29sdW1uRGVmaW5pdGlvbikgPT4ge1xuXHRcdFx0JHRhcmdldC5maW5kKCd0aGVhZCB0cicpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGxldCAkaGVhZGVyQ2VsbCA9ICQodGhpcykuZmluZChgW2RhdGEtY29sdW1uLW5hbWU9XCIke2NvbHVtbkRlZmluaXRpb24ubmFtZX1cIl1gKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmIChjb2x1bW5EZWZpbml0aW9uLmRhdGEgIT09IG51bGwgJiYgYWN0aXZlQ29sdW1uTmFtZXMuaW5kZXhPZihjb2x1bW5EZWZpbml0aW9uLm5hbWUpID09PSAtMSkge1xuXHRcdFx0XHRcdCRoZWFkZXJDZWxsLmhpZGUoKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdFx0XG5cdFx0Ly8gUHJlcGFyZSB0aGUgYWN0aXZlIGNvbHVtbiBkZWZpbml0aW9ucy5cblx0XHRsZXQgZmluYWxDb2x1bW5EZWZpbml0aW9ucyA9IFtdLFxuXHRcdFx0Y29sdW1uSW5kZXhlcyA9IFtdO1xuXHRcdFxuXHRcdCQuZWFjaChhY3RpdmVDb2x1bW5OYW1lcywgKGluZGV4LCBuYW1lKSA9PiB7XG5cdFx0XHQkLmVhY2goY29sdW1uRGVmaW5pdGlvbnMsIChpbmRleCwgY29sdW1uRGVmaW5pdGlvbikgPT4ge1xuXHRcdFx0XHRpZiAoY29sdW1uRGVmaW5pdGlvbi5uYW1lID09PSBuYW1lKSB7XG5cdFx0XHRcdFx0Ly8gQWRkIHRoZSBhY3RpdmUgY29sdW1uIGRlZmluaXRpb24gaW4gdGhlIFwiZmluYWxDb2x1bW5EZWZpbml0aW9uc1wiIGFycmF5LlxuXHRcdFx0XHRcdGZpbmFsQ29sdW1uRGVmaW5pdGlvbnMucHVzaChjb2x1bW5EZWZpbml0aW9uKTtcblx0XHRcdFx0XHRjb25zdCBoZWFkZXJDZWxsSW5kZXggPSAkdGFyZ2V0XG5cdFx0XHRcdFx0XHQuZmluZChgdGhlYWQ6Zmlyc3QgdHI6Zmlyc3QgW2RhdGEtY29sdW1uLW5hbWU9XCIke2NvbHVtbkRlZmluaXRpb24ubmFtZX1cIl1gKVxuXHRcdFx0XHRcdFx0LmluZGV4KCk7XG5cdFx0XHRcdFx0Y29sdW1uSW5kZXhlcy5wdXNoKGhlYWRlckNlbGxJbmRleCk7XG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7IC8vIGNvbnRpbnVlXG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH0pO1xuXHRcdFxuXHRcdGZpbmFsQ29sdW1uRGVmaW5pdGlvbnMuc29ydCgoYSwgYikgPT4ge1xuXHRcdFx0Y29uc3QgYUluZGV4ID0gYWN0aXZlQ29sdW1uTmFtZXMuaW5kZXhPZihhLm5hbWUpO1xuXHRcdFx0Y29uc3QgYkluZGV4ID0gYWN0aXZlQ29sdW1uTmFtZXMuaW5kZXhPZihiLm5hbWUpO1xuXHRcdFx0XG5cdFx0XHRpZiAoYUluZGV4IDwgYkluZGV4KSB7XG5cdFx0XHRcdHJldHVybiAtMTtcblx0XHRcdH0gZWxzZSBpZiAoYUluZGV4ID4gYkluZGV4KSB7XG5cdFx0XHRcdHJldHVybiAxO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmV0dXJuIDA7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0XG5cdFx0Ly8gUmVvcmRlciB0aGUgdGFibGUgaGVhZGVyIGVsZW1lbnRzIGRlcGVuZGluZyB0aGUgYWN0aXZlQ29sdW1uTmFtZXMgb3JkZXIuXG5cdFx0JHRhcmdldC5maW5kKCd0aGVhZCB0cicpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRsZXQgYWN0aXZlQ29sdW1uU2VsZWN0aW9ucyA9IFskKHRoaXMpLmZpbmQoJ3RoOmZpcnN0JyldO1xuXHRcdFx0XG5cdFx0XHQvLyBTb3J0IHRoZSBjb2x1bW5zIGluIHRoZSBjb3JyZWN0IG9yZGVyLlxuXHRcdFx0Y29sdW1uSW5kZXhlcy5mb3JFYWNoKChpbmRleCkgPT4ge1xuXHRcdFx0XHRsZXQgJGhlYWRlckNlbGwgPSAkKHRoaXMpLmZpbmQoJ3RoJykuZXEoaW5kZXgpO1xuXHRcdFx0XHRhY3RpdmVDb2x1bW5TZWxlY3Rpb25zLnB1c2goJGhlYWRlckNlbGwpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIE1vdmUgdGhlIGNvbHVtbnMgdG8gdGhlaXIgZmluYWwgcG9zaXRpb24uXG5cdFx0XHRhY3RpdmVDb2x1bW5TZWxlY3Rpb25zLmZvckVhY2goZnVuY3Rpb24oJGhlYWRlckNlbGwsIGluZGV4KSB7XG5cdFx0XHRcdGlmIChpbmRleCA9PT0gMCkge1xuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQkaGVhZGVyQ2VsbC5pbnNlcnRBZnRlcihhY3RpdmVDb2x1bW5TZWxlY3Rpb25zW2luZGV4IC0gMV0pO1xuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdFx0XG5cdFx0cmV0dXJuIGZpbmFsQ29sdW1uRGVmaW5pdGlvbnM7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBDcmVhdGVzIGEgRGF0YVRhYmxlIEluc3RhbmNlXG5cdCAqXG5cdCAqIFRoaXMgbWV0aG9kIHdpbGwgY3JlYXRlIGEgbmV3IGluc3RhbmNlIG9mIGRhdGF0YWJsZSBpbnRvIGEgYDx0YWJsZT5gIGVsZW1lbnQuIEl0IGVuYWJsZXNcblx0ICogZGV2ZWxvcGVycyB0byBlYXNpbHkgcGFzcyB0aGUgY29uZmlndXJhdGlvbiBuZWVkZWQgZm9yIGRpZmZlcmVudCBhbmQgbW9yZSBzcGVjaWFsIHNpdHVhdGlvbnMuXG5cdCAqXG5cdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGFyZ2V0IGpRdWVyeSBvYmplY3QgZm9yIHRoZSB0YXJnZXQgdGFibGUuXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBjb25maWd1cmF0aW9uIERhdGFUYWJsZXMgY29uZmlndXJhdGlvbiBhcHBsaWVkIG9uIHRoZSBuZXcgaW5zdGFuY2UuXG5cdCAqXG5cdCAqIEByZXR1cm4ge0RhdGFUYWJsZX0gUmV0dXJucyB0aGUgRGF0YVRhYmxlIEFQSSBpbnN0YW5jZSAoZGlmZmVyZW50IGZyb20gdGhlIGpRdWVyeSBvYmplY3QpLlxuXHQgKi9cblx0ZXhwb3J0cy5jcmVhdGUgPSBmdW5jdGlvbigkdGFyZ2V0LCBjb25maWd1cmF0aW9uKSB7XG5cdFx0cmV0dXJuICR0YXJnZXQuRGF0YVRhYmxlKGNvbmZpZ3VyYXRpb24pO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIFNldHMgdGhlIGVycm9yIGhhbmRsZXIgZm9yIHNwZWNpZmljIERhdGFUYWJsZS5cblx0ICpcblx0ICogRGF0YVRhYmxlcyBwcm92aWRlIGEgdXNlZnVsIG1lY2hhbmlzbSB0aGF0IGVuYWJsZXMgZGV2ZWxvcGVycyB0byBjb250cm9sIGVycm9ycyBkdXJpbmcgZGF0YSBwYXJzaW5nLlxuXHQgKiBJZiB0aGVyZSBpcyBhbiBlcnJvciBpbiB0aGUgQUpBWCByZXNwb25zZSBvciBzb21lIGRhdGEgYXJlIGludmFsaWQgaW4gdGhlIEphdmFTY3JpcHQgY29kZSB5b3UgY2FuIHVzZSBcblx0ICogdGhpcyBtZXRob2QgdG8gY29udHJvbCB0aGUgYmVoYXZpb3Igb2YgdGhlIGFwcCBhbmQgc2hvdyBvciBsb2cgdGhlIGVycm9yIG1lc3NhZ2VzLlxuXHQgKlxuXHQgKiB7QGxpbmsgaHR0cDovL2RhdGF0YWJsZXMubmV0L3JlZmVyZW5jZS9ldmVudC9lcnJvcn1cblx0ICpcblx0ICogQHBhcmFtIHtqUXVlcnl9ICR0YXJnZXQgalF1ZXJ5IG9iamVjdCBmb3IgdGhlIHRhcmdldCB0YWJsZS5cblx0ICogQHBhcmFtIHtPYmplY3R9IGNhbGxiYWNrIFByb3ZpZGUgYSBjYWxsYmFjayBtZXRob2QgY2FsbGVkIHdpdGggdGhlIFwiZXZlbnRcIiwgXCJzZXR0aW5nc1wiLCBcInRlY2hOb3RlXCIsIFxuXHQgKiBcIm1lc3NhZ2VcIiBhcmd1bWVudHMgKHNlZSBwcm92aWRlZCBsaW5rKS5cblx0ICovXG5cdGV4cG9ydHMuZXJyb3IgPSBmdW5jdGlvbigkdGFyZ2V0LCBjYWxsYmFjaykge1xuXHRcdCQuZm4uZGF0YVRhYmxlLmV4dC5lcnJNb2RlID0gJ25vbmUnO1xuXHRcdCR0YXJnZXRcblx0XHRcdC5vbignZXJyb3IuZHQnLCBjYWxsYmFjaylcblx0XHRcdC5vbigneGhyLmR0JywgKGV2ZW50LCBzZXR0aW5ncywganNvbiwgeGhyKSA9PiB7XG5cdFx0XHRcdGlmIChqc29uLmV4Y2VwdGlvbiA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdGNhbGxiYWNrKGV2ZW50LCBzZXR0aW5ncywgbnVsbCwganNvbi5tZXNzYWdlKTsgXG5cdFx0XHRcdH1cblx0XHRcdH0pOyBcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBTZXRzIHRoZSBjYWxsYmFjayBtZXRob2Qgd2hlbiBhamF4IGxvYWQgb2YgZGF0YSBpcyBjb21wbGV0ZS5cblx0ICpcblx0ICogVGhpcyBtZXRob2QgaXMgdXNlZnVsIGZvciBjaGVja2luZyBQSFAgZXJyb3JzIG9yIG1vZGlmeWluZyB0aGUgZGF0YSBiZWZvcmVcblx0ICogdGhleSBhcmUgZGlzcGxheWVkIHRvIHRoZSBzZXJ2ZXIuXG5cdCAqXG5cdCAqIHtAbGluayBodHRwOi8vZGF0YXRhYmxlcy5uZXQvcmVmZXJlbmNlL2V2ZW50L3hocn1cblx0ICpcblx0ICogQHBhcmFtIHtqUXVlcnl9ICR0YXJnZXQgalF1ZXJ5IG9iamVjdCBmb3IgdGhlIHRhcmdldCB0YWJsZS5cblx0ICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2sgUHJvdmlkZSBhIGNhbGxiYWNrIG1ldGhvZCBjYWxsZWQgd2l0aCB0aGUgXCJldmVudFwiLCBcInNldHRpbmdzXCIsIFwidGVjaE5vdGVcIiwgXG5cdCAqIFwibWVzc2FnZVwiIGFyZ3VtZW50cyAoc2VlIHByb3ZpZGVkIGxpbmspLlxuXHQgKi9cblx0ZXhwb3J0cy5hamF4Q29tcGxldGUgPSBmdW5jdGlvbigkdGFyZ2V0LCBjYWxsYmFjaykge1xuXHRcdCR0YXJnZXQub24oJ3hoci5kdCcsIGNhbGxiYWNrKTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBTZXRzIHRoZSB0YWJsZSBjb2x1bW4gdG8gYmUgZGlzcGxheWVkIGFzIGFuIGluZGV4LlxuXHQgKlxuXHQgKiBUaGlzIG1ldGhvZCB3aWxsIGVhc2lseSBlbmFibGUgeW91IHRvIHNldCBhIGNvbHVtbiBhcyBhbiBpbmRleCBjb2x1bW4sIHVzZWRcblx0ICogZm9yIG51bWJlcmluZyB0aGUgdGFibGUgcm93cyByZWdhcmRsZXNzIG9mIHRoZSBzZWFyY2gsIHNvcnRpbmcgYW5kIHJvdyBjb3VudC5cblx0ICpcblx0ICoge0BsaW5rIGh0dHA6Ly93d3cuZGF0YXRhYmxlcy5uZXQvZXhhbXBsZXMvYXBpL2NvdW50ZXJfY29sdW1ucy5odG1sfVxuXHQgKlxuXHQgKiBAcGFyYW0ge2pRdWVyeX0gJHRhcmdldCBqUXVlcnkgb2JqZWN0IGZvciB0aGUgdGFyZ2V0IHRhYmxlLlxuXHQgKiBAcGFyYW0ge051bWJlcn0gY29sdW1uSW5kZXggWmVybyBiYXNlZCBpbmRleCBvZiB0aGUgY29sdW1uIHRvIGJlIGluZGV4ZWQuXG5cdCAqL1xuXHRleHBvcnRzLmluZGV4Q29sdW1uID0gZnVuY3Rpb24oJHRhcmdldCwgY29sdW1uSW5kZXgpIHtcblx0XHQkdGFyZ2V0Lm9uKCdvcmRlci5kdCBzZWFyY2guZHQnLCBmdW5jdGlvbigpIHtcblx0XHRcdCR0YXJnZXQuRGF0YVRhYmxlKCkuY29sdW1uKGNvbHVtbkluZGV4LCB7XG5cdFx0XHRcdHNlYXJjaDogJ2FwcGxpZWQnLFxuXHRcdFx0XHRvcmRlcjogJ2FwcGxpZWQnXG5cdFx0XHR9KS5ub2RlcygpLmVhY2goZnVuY3Rpb24oY2VsbCwgaW5kZXgpIHtcblx0XHRcdFx0Y2VsbC5pbm5lckhUTUwgPSBpbmRleCArIDE7XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBSZXR1cm5zIHRoZSBnZXJtYW4gdHJhbnNsYXRpb24gb2YgdGhlIERhdGFUYWJsZXNcblx0ICpcblx0ICogVGhpcyBtZXRob2QgcHJvdmlkZXMgYSBxdWljayB3YXkgdG8gZ2V0IHRoZSBsYW5ndWFnZSBKU09OIHdpdGhvdXQgaGF2aW5nIHRvIHBlcmZvcm1cblx0ICogYW5kIEFKQVggcmVxdWVzdCB0byB0aGUgc2VydmVyLiBJZiB5b3Ugc2V0dXAgeW91ciBEYXRhVGFibGUgbWFudWFsbHkgeW91IGNhbiBzZXQgdGhlXG5cdCAqIFwibGFuZ3VhZ2VcIiBhdHRyaWJ1dGUgd2l0aCB0aGlzIG1ldGhvZC5cblx0ICpcblx0ICogQGRlcHJlY2F0ZWQgU2luY2UgdjEuNCwgdXNlIHRoZSBcImdldFRyYW5zbGF0aW9uc1wiIG1ldGhvZCBpbnN0ZWFkLlxuXHQgKlxuXHQgKiBAcmV0dXJuIHtPYmplY3R9IFJldHVybnMgdGhlIGdlcm1hbiB0cmFuc2xhdGlvbiwgbXVzdCBiZSB0aGUgc2FtZSBhcyB0aGUgXCJnZXJtYW4ubGFuZy5qc29uXCIgZmlsZS5cblx0ICovXG5cdGV4cG9ydHMuZ2V0R2VybWFuVHJhbnNsYXRpb24gPSBmdW5jdGlvbigpIHtcblx0XHRqc2UuY29yZS5kZWJ1Zy53YXJuKCdEYXRhVGFibGVzIExpYnJhcnk6IHRoZSBnZXRHZXJtYW5UcmFuc2xhdGlvbiBtZXRob2QgaXMgZGVwcmVjYXRlZCBhbmQgd2lsbCBiZSByZW1vdmVkICdcblx0XHRcdCsgJ2luIEpTRSB2MS41LCBwbGVhc2UgdXNlIHRoZSBcImdldFRyYW5zbGF0aW9uc1wiIG1ldGhvZCBpbnN0ZWFkLicpO1xuXHRcdHJldHVybiBsYW5ndWFnZXMuZGU7XG5cdH07XG5cdFxuXHQvKipcblx0ICogR2V0IHRoZSBEYXRhVGFibGVzIHRyYW5zbGF0aW9uIGRlcGVuZGluZyB0aGUgbGFuZ3VhZ2UgY29kZSBwYXJhbWV0ZXIuXG5cdCAqXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBsYW5ndWFnZUNvZGUgUHJvdmlkZSAnZGUnIG9yICdlbicgKHlvdSBjYW4gYWxzbyB1c2UgdGhlIGpzZS5jb3JlLmNvbmZpZy5nZXQoJ2xhbmd1YWdlQ29kZScpIHRvXG5cdCAqIGdldCB0aGUgY3VycmVudCBsYW5ndWFnZSBjb2RlKS5cblx0ICpcblx0ICogQHJldHVybiB7T2JqZWN0fSBSZXR1cm5zIHRoZSB0cmFuc2xhdGlvbiBzdHJpbmdzIGluIGFuIG9iamVjdCBsaXRlcmFsIGFzIGRlc2NyaWJlZCBieSB0aGUgb2ZmaWNpYWwgRGF0YVRhYmxlc1xuXHQgKiBkb2N1bWVudGF0aW9uLlxuXHQgKlxuXHQgKiB7QGxpbmsgaHR0cHM6Ly93d3cuZGF0YXRhYmxlcy5uZXQvcGx1Zy1pbnMvaTE4bn1cblx0ICovXG5cdGV4cG9ydHMuZ2V0VHJhbnNsYXRpb25zID0gZnVuY3Rpb24obGFuZ3VhZ2VDb2RlKSB7XG5cdFx0aWYgKGxhbmd1YWdlc1tsYW5ndWFnZUNvZGVdID09PSB1bmRlZmluZWQpIHtcblx0XHRcdGpzZS5jb3JlLmRlYnVnLndhcm4oJ0RhdGFUYWJsZXMgTGlicmFyeTogVGhlIHJlcXVlc3RlZCBEYXRhVGFibGVzIHRyYW5zbGF0aW9uIHdhcyBub3QgZm91bmQ6JywgbGFuZ3VhZ2VDb2RlKTtcblx0XHRcdGxhbmd1YWdlQ29kZSA9ICdlbic7XG5cdFx0fVxuXHRcdFxuXHRcdHJldHVybiBsYW5ndWFnZXNbbGFuZ3VhZ2VDb2RlXTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBQcmVwYXJlIHRhYmxlIGNvbHVtbnMuXG5cdCAqXG5cdCAqIFRoaXMgbWV0aG9kIHdpbGwgY29udmVydCB0aGUgY29sdW1uIGRlZmluaXRpb25zIHRvIGEgRGF0YVRhYmxlIGNvbXBhdGlibGUgZm9ybWF0IGFuZCBhbHNvIHJlb3JkZXJcblx0ICogdGhlIHRhYmxlIGhlYWRlciBjZWxscyBvZiB0aGUgXCJ0aGVhZFwiIGVsZW1lbnQuXG5cdCAqXG5cdCAqIEBwYXJhbSB7alF1ZXJ5fSAkdGFyZ2V0IFRhYmxlIGpRdWVyeSBzZWxlY3RvciBvYmplY3QuXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBjb2x1bW5EZWZpbml0aW9ucyBBcnJheSBjb250YWluaW5nIHRoZSBEYXRhVGFibGUgY29sdW1uIGRlZmluaXRpb25zLlxuXHQgKiBAcGFyYW0ge1N0cmluZ1tdfSBhY3RpdmVDb2x1bW5OYW1lcyBBcnJheSBjb250YWluaW5nIHRoZSBzbHVnLW5hbWVzIG9mIHRoZSBhY3RpdmUgY29sdW1ucy5cblx0ICpcblx0ICogQHJldHVybiB7T2JqZWN0W119IFJldHVybnMgYXJyYXkgd2l0aCB0aGUgYWN0aXZlIGNvbHVtbiBkZWZpbml0aW9ucyByZWFkeSB0byB1c2UgaW4gRGF0YVRhYmxlLmNvbHVtbnMgb3B0aW9uLlxuXHQgKi9cblx0ZXhwb3J0cy5wcmVwYXJlQ29sdW1ucyA9IGZ1bmN0aW9uKCR0YXJnZXQsIGNvbHVtbkRlZmluaXRpb25zLCBhY3RpdmVDb2x1bW5OYW1lcykge1xuXHRcdGxldCBjb252ZXJ0ZWRDb2x1bW5EZWZpbml0aW9ucyA9IFtdO1xuXHRcdFxuXHRcdGZvciAobGV0IGNvbHVtbk5hbWUgaW4gY29sdW1uRGVmaW5pdGlvbnMpIHtcblx0XHRcdGxldCBjb2x1bW5EZWZpbml0aW9uID0gY29sdW1uRGVmaW5pdGlvbnNbY29sdW1uTmFtZV07XG5cdFx0XHRjb2x1bW5EZWZpbml0aW9uLm5hbWUgPSBjb2x1bW5OYW1lO1xuXHRcdFx0Y29udmVydGVkQ29sdW1uRGVmaW5pdGlvbnMucHVzaChjb2x1bW5EZWZpbml0aW9uKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIF9yZW9yZGVyQ29sdW1ucygkdGFyZ2V0LCBjb252ZXJ0ZWRDb2x1bW5EZWZpbml0aW9ucywgYWN0aXZlQ29sdW1uTmFtZXMpO1xuXHR9O1xuXHRcbn0oanNlLmxpYnMuZGF0YXRhYmxlKSk7XG4iXX0=
