'use strict';

/* --------------------------------------------------------------
 user_configuration_service.js 2016-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.user_configuration_service = jse.libs.user_configuration_service || {};

/**
 * ## User Configuration Library
 *
 * This library is an adapter for the UserConfigurationService of the shop's backend codebase. It will
 * perform AJAX requests for getting/setting user config data that is a robust way to store information
 * about a specific user.
 *
 *```js
 * var options= {
 *     data: {
 *         userId: 1,  // Current user ID
 *         configurationKey: 'recent_search_area', // Configuration key
 *         configurationValue: '', // Configuration value (only for posting)
 *     },
 *
 *     onSuccess: function (data) {}, // Callback function, that will be executed on successful request,
 *                                    // contains the response as argument.
 *
 *     onError: function (data) {},   // Callback function, that will be executed on failed request.
 * }
 *
 * jse.libs.user_configuration_service.set(options); // Set values
 *
 * jse.libs.user_configuration_service.get(options); // Get values
 * ```
 *
 * @module JSE/Libs/user_configuration_service
 * @exports jse.libs.user_configuration_service
 */
(function (exports) {

	'use strict';

	// ------------------------------------------------------------------------
	// DEFAULTS
	// ------------------------------------------------------------------------

	/**
  * Default Library Settings
  *
  * @type {object}
  */

	var defaults = {
		// URL
		baseUrl: 'admin.php?do=UserConfiguration',
		urlSet: '/set',
		urlGet: '/get'
	};

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
  * Performs AJAX request
  *
  * @param {object} params Contains the request parameters.
  * @param {string} params.type - type of request
  * @param {function} params.onSuccess - callback on success
  * @param {function} params.onError - callback on success
  * @param {object} params.data - request parameter
  *
  * @throws Error
  *
  * @private
  */
	var _request = function _request(params) {
		$.ajax({
			url: [defaults.baseUrl, params.type === 'set' ? defaults.urlSet : defaults.urlGet].join(''),
			dataType: 'json',
			data: params.data,
			method: params.type === 'set' ? 'post' : 'get',
			success: function success(data) {
				if (params.type === 'get') {
					// GET
					_handleSuccess(data, params);
				} else {
					// POST
					if (data.success) {
						_handleSuccess({}, params);
					} else if (typeof params.onError === 'function') {
						params.onError(data);
					}
				}
			},
			error: function error(data) {
				if (typeof params.onError === 'function') {
					params.onError(data);
				}
			}
		});
	};

	/**
  * Handles success requests.
  *
  * @param {object} data - Data returned from server
  * @param {object} params - Parameters
  *
  * @private
  */
	var _handleSuccess = function _handleSuccess(data, params) {
		var response = {};
		if (data.success && data.configurationValue) {
			response = data;
		}
		if (typeof params.onSuccess === 'function') {
			params.onSuccess(response);
		}
	};

	// ------------------------------------------------------------------------
	// PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
  * Returns the user configuration value.
  *
  * @param {object} options
  * @param {function} options.onSuccess - callback on success
  * @param {function} options.onError - callback on success
  * @param {object} options.data - request parameter
  */
	exports.get = function (options) {
		options.type = 'get';
		_request(options);
	};

	/**
  * Sets the user configuration value.
  *
  * @param {object} options
  * @param {function} options.onSuccess - callback on success
  * @param {function} options.onError - callback on success
  * @param {object} options.data - request parameter
  */
	exports.set = function (options) {
		options.type = 'set';
		_request(options);
	};
})(jse.libs.user_configuration_service);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlLmpzIl0sIm5hbWVzIjpbImpzZSIsImxpYnMiLCJ1c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSIsImV4cG9ydHMiLCJkZWZhdWx0cyIsImJhc2VVcmwiLCJ1cmxTZXQiLCJ1cmxHZXQiLCJfcmVxdWVzdCIsInBhcmFtcyIsIiQiLCJhamF4IiwidXJsIiwidHlwZSIsImpvaW4iLCJkYXRhVHlwZSIsImRhdGEiLCJtZXRob2QiLCJzdWNjZXNzIiwiX2hhbmRsZVN1Y2Nlc3MiLCJvbkVycm9yIiwiZXJyb3IiLCJyZXNwb25zZSIsImNvbmZpZ3VyYXRpb25WYWx1ZSIsIm9uU3VjY2VzcyIsImdldCIsIm9wdGlvbnMiLCJzZXQiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsSUFBSUMsSUFBSixDQUFTQywwQkFBVCxHQUFzQ0YsSUFBSUMsSUFBSixDQUFTQywwQkFBVCxJQUF1QyxFQUE3RTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE2QkEsQ0FBQyxVQUFTQyxPQUFULEVBQWtCOztBQUVsQjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQUlDLFdBQVc7QUFDZDtBQUNBQyxXQUFTLGdDQUZLO0FBR2RDLFVBQVEsTUFITTtBQUlkQyxVQUFRO0FBSk0sRUFBZjs7QUFPQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7QUFhQSxLQUFJQyxXQUFXLFNBQVhBLFFBQVcsQ0FBU0MsTUFBVCxFQUFpQjtBQUMvQkMsSUFBRUMsSUFBRixDQUFPO0FBQ05DLFFBQUssQ0FDSlIsU0FBU0MsT0FETCxFQUVISSxPQUFPSSxJQUFQLEtBQWdCLEtBQWhCLEdBQXdCVCxTQUFTRSxNQUFqQyxHQUEwQ0YsU0FBU0csTUFGaEQsRUFHSE8sSUFIRyxDQUdFLEVBSEYsQ0FEQztBQUtOQyxhQUFVLE1BTEo7QUFNTkMsU0FBTVAsT0FBT08sSUFOUDtBQU9OQyxXQUFTUixPQUFPSSxJQUFQLEtBQWdCLEtBQWhCLEdBQXdCLE1BQXhCLEdBQWlDLEtBUHBDO0FBUU5LLFlBQVMsaUJBQVNGLElBQVQsRUFBZTtBQUN2QixRQUFJUCxPQUFPSSxJQUFQLEtBQWdCLEtBQXBCLEVBQTJCO0FBQUU7QUFDNUJNLG9CQUFlSCxJQUFmLEVBQXFCUCxNQUFyQjtBQUNBLEtBRkQsTUFFTztBQUFFO0FBQ1IsU0FBSU8sS0FBS0UsT0FBVCxFQUFrQjtBQUNqQkMscUJBQWUsRUFBZixFQUFtQlYsTUFBbkI7QUFDQSxNQUZELE1BRU8sSUFBSSxPQUFPQSxPQUFPVyxPQUFkLEtBQTBCLFVBQTlCLEVBQTBDO0FBQ2hEWCxhQUFPVyxPQUFQLENBQWVKLElBQWY7QUFDQTtBQUNEO0FBQ0QsSUFsQks7QUFtQk5LLFVBQU8sZUFBU0wsSUFBVCxFQUFlO0FBQ3JCLFFBQUksT0FBT1AsT0FBT1csT0FBZCxLQUEwQixVQUE5QixFQUEwQztBQUN6Q1gsWUFBT1csT0FBUCxDQUFlSixJQUFmO0FBQ0E7QUFDRDtBQXZCSyxHQUFQO0FBeUJBLEVBMUJEOztBQTRCQTs7Ozs7Ozs7QUFRQSxLQUFJRyxpQkFBaUIsU0FBakJBLGNBQWlCLENBQVNILElBQVQsRUFBZVAsTUFBZixFQUF1QjtBQUMzQyxNQUFJYSxXQUFXLEVBQWY7QUFDQSxNQUFJTixLQUFLRSxPQUFMLElBQWdCRixLQUFLTyxrQkFBekIsRUFBNkM7QUFDNUNELGNBQVdOLElBQVg7QUFDQTtBQUNELE1BQUksT0FBT1AsT0FBT2UsU0FBZCxLQUE0QixVQUFoQyxFQUE0QztBQUMzQ2YsVUFBT2UsU0FBUCxDQUFpQkYsUUFBakI7QUFDQTtBQUNELEVBUkQ7O0FBVUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQVFBbkIsU0FBUXNCLEdBQVIsR0FBYyxVQUFTQyxPQUFULEVBQWtCO0FBQy9CQSxVQUFRYixJQUFSLEdBQWUsS0FBZjtBQUNBTCxXQUFTa0IsT0FBVDtBQUNBLEVBSEQ7O0FBS0E7Ozs7Ozs7O0FBUUF2QixTQUFRd0IsR0FBUixHQUFjLFVBQVNELE9BQVQsRUFBa0I7QUFDL0JBLFVBQVFiLElBQVIsR0FBZSxLQUFmO0FBQ0FMLFdBQVNrQixPQUFUO0FBQ0EsRUFIRDtBQUtBLENBakhELEVBaUhHMUIsSUFBSUMsSUFBSixDQUFTQywwQkFqSFoiLCJmaWxlIjoidXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlLmpzIDIwMTYtMDItMjNcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE2IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UubGlicy51c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZSA9IGpzZS5saWJzLnVzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlIHx8IHt9O1xuXG4vKipcbiAqICMjIFVzZXIgQ29uZmlndXJhdGlvbiBMaWJyYXJ5XG4gKlxuICogVGhpcyBsaWJyYXJ5IGlzIGFuIGFkYXB0ZXIgZm9yIHRoZSBVc2VyQ29uZmlndXJhdGlvblNlcnZpY2Ugb2YgdGhlIHNob3AncyBiYWNrZW5kIGNvZGViYXNlLiBJdCB3aWxsXG4gKiBwZXJmb3JtIEFKQVggcmVxdWVzdHMgZm9yIGdldHRpbmcvc2V0dGluZyB1c2VyIGNvbmZpZyBkYXRhIHRoYXQgaXMgYSByb2J1c3Qgd2F5IHRvIHN0b3JlIGluZm9ybWF0aW9uXG4gKiBhYm91dCBhIHNwZWNpZmljIHVzZXIuXG4gKlxuICpgYGBqc1xuICogdmFyIG9wdGlvbnM9IHtcbiAqICAgICBkYXRhOiB7XG4gKiAgICAgICAgIHVzZXJJZDogMSwgIC8vIEN1cnJlbnQgdXNlciBJRFxuICogICAgICAgICBjb25maWd1cmF0aW9uS2V5OiAncmVjZW50X3NlYXJjaF9hcmVhJywgLy8gQ29uZmlndXJhdGlvbiBrZXlcbiAqICAgICAgICAgY29uZmlndXJhdGlvblZhbHVlOiAnJywgLy8gQ29uZmlndXJhdGlvbiB2YWx1ZSAob25seSBmb3IgcG9zdGluZylcbiAqICAgICB9LFxuICpcbiAqICAgICBvblN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7fSwgLy8gQ2FsbGJhY2sgZnVuY3Rpb24sIHRoYXQgd2lsbCBiZSBleGVjdXRlZCBvbiBzdWNjZXNzZnVsIHJlcXVlc3QsXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnRhaW5zIHRoZSByZXNwb25zZSBhcyBhcmd1bWVudC5cbiAqXG4gKiAgICAgb25FcnJvcjogZnVuY3Rpb24gKGRhdGEpIHt9LCAgIC8vIENhbGxiYWNrIGZ1bmN0aW9uLCB0aGF0IHdpbGwgYmUgZXhlY3V0ZWQgb24gZmFpbGVkIHJlcXVlc3QuXG4gKiB9XG4gKlxuICoganNlLmxpYnMudXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2Uuc2V0KG9wdGlvbnMpOyAvLyBTZXQgdmFsdWVzXG4gKlxuICoganNlLmxpYnMudXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UuZ2V0KG9wdGlvbnMpOyAvLyBHZXQgdmFsdWVzXG4gKiBgYGBcbiAqXG4gKiBAbW9kdWxlIEpTRS9MaWJzL3VzZXJfY29uZmlndXJhdGlvbl9zZXJ2aWNlXG4gKiBAZXhwb3J0cyBqc2UubGlicy51c2VyX2NvbmZpZ3VyYXRpb25fc2VydmljZVxuICovXG4oZnVuY3Rpb24oZXhwb3J0cykge1xuXHRcblx0J3VzZSBzdHJpY3QnO1xuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIERFRkFVTFRTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIERlZmF1bHQgTGlicmFyeSBTZXR0aW5nc1xuXHQgKlxuXHQgKiBAdHlwZSB7b2JqZWN0fVxuXHQgKi9cblx0dmFyIGRlZmF1bHRzID0ge1xuXHRcdC8vIFVSTFxuXHRcdGJhc2VVcmw6ICdhZG1pbi5waHA/ZG89VXNlckNvbmZpZ3VyYXRpb24nLFxuXHRcdHVybFNldDogJy9zZXQnLFxuXHRcdHVybEdldDogJy9nZXQnXG5cdH07XG5cdFxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0Ly8gUFJJVkFURSBNRVRIT0RTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIFBlcmZvcm1zIEFKQVggcmVxdWVzdFxuXHQgKlxuXHQgKiBAcGFyYW0ge29iamVjdH0gcGFyYW1zIENvbnRhaW5zIHRoZSByZXF1ZXN0IHBhcmFtZXRlcnMuXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBwYXJhbXMudHlwZSAtIHR5cGUgb2YgcmVxdWVzdFxuXHQgKiBAcGFyYW0ge2Z1bmN0aW9ufSBwYXJhbXMub25TdWNjZXNzIC0gY2FsbGJhY2sgb24gc3VjY2Vzc1xuXHQgKiBAcGFyYW0ge2Z1bmN0aW9ufSBwYXJhbXMub25FcnJvciAtIGNhbGxiYWNrIG9uIHN1Y2Nlc3Ncblx0ICogQHBhcmFtIHtvYmplY3R9IHBhcmFtcy5kYXRhIC0gcmVxdWVzdCBwYXJhbWV0ZXJcblx0ICpcblx0ICogQHRocm93cyBFcnJvclxuXHQgKlxuXHQgKiBAcHJpdmF0ZVxuXHQgKi9cblx0dmFyIF9yZXF1ZXN0ID0gZnVuY3Rpb24ocGFyYW1zKSB7XG5cdFx0JC5hamF4KHtcblx0XHRcdHVybDogW1xuXHRcdFx0XHRkZWZhdWx0cy5iYXNlVXJsLFxuXHRcdFx0XHQocGFyYW1zLnR5cGUgPT09ICdzZXQnID8gZGVmYXVsdHMudXJsU2V0IDogZGVmYXVsdHMudXJsR2V0KVxuXHRcdFx0XS5qb2luKCcnKSxcblx0XHRcdGRhdGFUeXBlOiAnanNvbicsXG5cdFx0XHRkYXRhOiBwYXJhbXMuZGF0YSxcblx0XHRcdG1ldGhvZDogKHBhcmFtcy50eXBlID09PSAnc2V0JyA/ICdwb3N0JyA6ICdnZXQnKSxcblx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdFx0aWYgKHBhcmFtcy50eXBlID09PSAnZ2V0JykgeyAvLyBHRVRcblx0XHRcdFx0XHRfaGFuZGxlU3VjY2VzcyhkYXRhLCBwYXJhbXMpO1xuXHRcdFx0XHR9IGVsc2UgeyAvLyBQT1NUXG5cdFx0XHRcdFx0aWYgKGRhdGEuc3VjY2Vzcykge1xuXHRcdFx0XHRcdFx0X2hhbmRsZVN1Y2Nlc3Moe30sIHBhcmFtcyk7XG5cdFx0XHRcdFx0fSBlbHNlIGlmICh0eXBlb2YgcGFyYW1zLm9uRXJyb3IgPT09ICdmdW5jdGlvbicpIHtcblx0XHRcdFx0XHRcdHBhcmFtcy5vbkVycm9yKGRhdGEpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdGVycm9yOiBmdW5jdGlvbihkYXRhKSB7XG5cdFx0XHRcdGlmICh0eXBlb2YgcGFyYW1zLm9uRXJyb3IgPT09ICdmdW5jdGlvbicpIHtcblx0XHRcdFx0XHRwYXJhbXMub25FcnJvcihkYXRhKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0pO1xuXHR9O1xuXHRcblx0LyoqXG5cdCAqIEhhbmRsZXMgc3VjY2VzcyByZXF1ZXN0cy5cblx0ICpcblx0ICogQHBhcmFtIHtvYmplY3R9IGRhdGEgLSBEYXRhIHJldHVybmVkIGZyb20gc2VydmVyXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSBwYXJhbXMgLSBQYXJhbWV0ZXJzXG5cdCAqXG5cdCAqIEBwcml2YXRlXG5cdCAqL1xuXHR2YXIgX2hhbmRsZVN1Y2Nlc3MgPSBmdW5jdGlvbihkYXRhLCBwYXJhbXMpIHtcblx0XHR2YXIgcmVzcG9uc2UgPSB7fTtcblx0XHRpZiAoZGF0YS5zdWNjZXNzICYmIGRhdGEuY29uZmlndXJhdGlvblZhbHVlKSB7XG5cdFx0XHRyZXNwb25zZSA9IGRhdGE7XG5cdFx0fVxuXHRcdGlmICh0eXBlb2YgcGFyYW1zLm9uU3VjY2VzcyA9PT0gJ2Z1bmN0aW9uJykge1xuXHRcdFx0cGFyYW1zLm9uU3VjY2VzcyhyZXNwb25zZSk7XG5cdFx0fVxuXHR9O1xuXHRcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdC8vIFBVQkxJQyBNRVRIT0RTXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcblx0LyoqXG5cdCAqIFJldHVybnMgdGhlIHVzZXIgY29uZmlndXJhdGlvbiB2YWx1ZS5cblx0ICpcblx0ICogQHBhcmFtIHtvYmplY3R9IG9wdGlvbnNcblx0ICogQHBhcmFtIHtmdW5jdGlvbn0gb3B0aW9ucy5vblN1Y2Nlc3MgLSBjYWxsYmFjayBvbiBzdWNjZXNzXG5cdCAqIEBwYXJhbSB7ZnVuY3Rpb259IG9wdGlvbnMub25FcnJvciAtIGNhbGxiYWNrIG9uIHN1Y2Nlc3Ncblx0ICogQHBhcmFtIHtvYmplY3R9IG9wdGlvbnMuZGF0YSAtIHJlcXVlc3QgcGFyYW1ldGVyXG5cdCAqL1xuXHRleHBvcnRzLmdldCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcblx0XHRvcHRpb25zLnR5cGUgPSAnZ2V0Jztcblx0XHRfcmVxdWVzdChvcHRpb25zKTtcblx0fTtcblx0XG5cdC8qKlxuXHQgKiBTZXRzIHRoZSB1c2VyIGNvbmZpZ3VyYXRpb24gdmFsdWUuXG5cdCAqXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zXG5cdCAqIEBwYXJhbSB7ZnVuY3Rpb259IG9wdGlvbnMub25TdWNjZXNzIC0gY2FsbGJhY2sgb24gc3VjY2Vzc1xuXHQgKiBAcGFyYW0ge2Z1bmN0aW9ufSBvcHRpb25zLm9uRXJyb3IgLSBjYWxsYmFjayBvbiBzdWNjZXNzXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zLmRhdGEgLSByZXF1ZXN0IHBhcmFtZXRlclxuXHQgKi9cblx0ZXhwb3J0cy5zZXQgPSBmdW5jdGlvbihvcHRpb25zKSB7XG5cdFx0b3B0aW9ucy50eXBlID0gJ3NldCc7XG5cdFx0X3JlcXVlc3Qob3B0aW9ucyk7XG5cdH07XG5cdFxufSkoanNlLmxpYnMudXNlcl9jb25maWd1cmF0aW9uX3NlcnZpY2UpO1xuIl19
