<?php
/* --------------------------------------------------------------
   paypal3_installments.php 2018-08-30
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$languageTextManager = MainFactory::create_object('LanguageTextManager', array(), true);
$languageTextManager->init_from_lang_file('paypal3installments_module', $_SESSION['languages_id']);

class paypal3_installments_ORIGIN
{
	public $code, $title, $description, $enabled;
	public $tmpOrders = true;
	public $tmpStatus = 0;
	protected $text;
	protected $configStorage;
	protected $logger;
	protected $selection_logo = '';
	protected $minimumCartValue = 99;
	protected $maximumCartValue = 5000;
	protected $logoImage;
	protected $approval_url_rewriting_required = true;

	public function __construct()
	{
		$this->logger               = MainFactory::create('PayPalLogger');
		$this->text                 = MainFactory::create('PayPalText');
		$this->configStorage        = MainFactory::create('PayPalConfigurationStorage');
		$this->code                 = 'paypal3_installments';
		$this->enabled              = ((@constant('MODULE_PAYMENT_'.strtoupper($this->code).'_STATUS') == 'True') ? true : false);
		$this->title                = @constant('MODULE_PAYMENT_'.strtoupper($this->code).'_TEXT_TITLE_ADMIN');
		$config_button              = '<br><br><a href="'.xtc_href_link('admin.php', 'do=PayPalConfiguration').'" class="button" style="margin: auto; background-color: #E30000;">'.$this->text->get_text('configure').'</a><br><br>';
		$this->description          = @constant('MODULE_PAYMENT_'.strtoupper($this->code).'_TEXT_DESCRIPTION') . ($this->check() ? $config_button : '');
		$this->sort_order           = @constant('MODULE_PAYMENT_'.strtoupper($this->code).'_SORT_ORDER');
		$this->info                 = @constant('MODULE_PAYMENT_'.strtoupper($this->code).'_TEXT_INFO');
		$this->order_status         = $this->configStorage->get('orderstatus/completed');
		$this->order_status_pending = $this->configStorage->get('orderstatus/pending');
		$this->order_status_error   = $this->configStorage->get('orderstatus/error');
		$this->logoImage            = sprintf('<img src="%s" alt="%s" class="ppinstallments_selection">',
			xtc_href_link('templates/' . CURRENT_TEMPLATE . '/assets/images/pp_credit-german_v_rgb.png', '', 'NONSSL', true, true, false, true, true),
			@constant('MODULE_PAYMENT_'.strtoupper($this->code).'_TEXT_TITLE')
		);

		if(is_object($GLOBALS['order']))
		{
			$this->update_status();
		}

		// transport abandonment of withdrawal rights across POST/GET change of checkout_confirmation
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$_SESSION['paypal_abandonment_download'] = isset($_POST['abandonment_download']) ? 'true' : 'false';
			$_SESSION['paypal_abandonment_service'] = isset($_POST['abandonment_service']) ? 'true' : 'false';
		}
		else
		{
			if(isset($_SESSION['paypal_abandonment_download']))
			{
				$_SESSION['abandonment_download'] = $_SESSION['paypal_abandonment_download'];
				if($_SESSION['paypal_abandonment_download'] === 'true')
				{
					$_POST['abandonment_download'] = 'true';
				}
			}
			if(isset($_SESSION['paypal_abandonment_service']))
			{
				$_SESSION['abandonment_service'] = $_SESSION['paypal_abandonment_service'];
				if($_SESSION['paypal_abandonment_service'] === 'true')
				{
					$_POST['abandonment_service'] = 'true';
				}
			}
		}
	}

	public function update_status()
	{
		$order = $GLOBALS['order'];

		$orderTotalModuleInstalled = strpos(MODULE_ORDER_TOTAL_INSTALLED, 'ot_paypal3_instfee.php') !== false;
		$orderTotalModuleEnabled   = defined('MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_STATUS')
		                             && strtolower(MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_STATUS) === 'true';
		$total = $_SESSION['cart']->show_total();

		$this->enabled = $this->enabled && $this->_isConfigured() === true;
		$this->enabled = $this->enabled && gm_get_env_info('TEMPLATE_VERSION') == 3; // Honeygrid only!
		$this->enabled = $this->enabled && $orderTotalModuleInstalled && $orderTotalModuleEnabled;
		$this->enabled = $this->enabled && $this->minimumCartValue <= $total && $total <= $this->maximumCartValue;
		$this->enabled = $this->enabled && $_SESSION['cart']->get_content_type() === 'physical';
		$this->enabled = $this->enabled && $_SESSION['currency'] === 'EUR';
		$this->enabled = $this->enabled && (bool)$_SESSION['customer_b2b_status'] !== true;

		if($this->enabled === true)
		{
			$addressBookService = StaticGXCoreLoader::getService('AddressBook');
			$sendToAddress = $addressBookService->findAddressById(MainFactory::create('IdType', $_SESSION['sendto']));
			$billToAddress = $addressBookService->findAddressById(MainFactory::create('IdType', $_SESSION['billto']));
			$this->enabled = $this->enabled && $sendToAddress->getCountry()->getId() === $billToAddress->getCountry()->getId();
			$this->enabled = $this->enabled && $sendToAddress->getB2BStatus()->getStatus() !== true;
			$this->enabled = $this->enabled && $billToAddress->getB2BStatus()->getStatus() !== true;
		}

		if(($this->enabled == true) && ((int) @constant('MODULE_PAYMENT_'.strtoupper($this->code).'_ZONE') > 0))
		{
			$check_flag = false;
			$check_query = xtc_db_query("select zone_id from ".TABLE_ZONES_TO_GEO_ZONES." where geo_zone_id = '".@constant('MODULE_PAYMENT_'.strtoupper($this->code).'_ZONE')."' and zone_country_id = '".$order->billing['country']['id']."' order by zone_id");
			while($check = xtc_db_fetch_array($check_query))
			{
				if($check['zone_id'] < 1)
				{
					$check_flag = true;
					break;
				}
				elseif($check['zone_id'] == $order->billing['zone_id'])
				{
					$check_flag = true;
					break;
				}
			}

			if($check_flag == false)
			{
				$this->enabled = false;
			}
		}
	}

	protected function _isConfigured()
	{
		$isConfigured = false;
		$mode = $this->configStorage->get('mode');
		$client_id = $this->configStorage->get('restapi-credentials/'.$mode.'/client_id');
		$secret = $this->configStorage->get('restapi-credentials/'.$mode.'/secret');
		if(!empty($client_id) && !empty($secret))
		{
			$isConfigured = true;
		}
		return $isConfigured;
	}

	function javascript_validation()
	{
		return false;
	}

	public function selection()
	{
		if(isset($_GET['paypal']) && $_GET['paypal'] == 'cancel')
		{
			unset($_SESSION['payment']);
		}

		$specificPresentment = false;
		if($this->configStorage->get('show_installments_presentment_specific_payment') == true)
		{
			$cartTotal = $_SESSION['cart']->show_total();

			$contentView = MainFactory::create('ContentView');
			$contentView->set_content_template('module/paypalinstallmentspecificstatic.html');
			$contentView->set_flat_assigns(true);
			$contentView->set_caching_enabled(false);
			$contentView->set_content_data('amount', $cartTotal);
			$contentView->set_content_data('page_type', 'checkout_payment');
			$specificPresentment = $contentView->get_html();

			if((bool)$this->configStorage->get('show_installments_presentment_specific_computed') === true)
			{
				$paypalInstallments = MainFactory::create('PayPalInstallments');
				try
				{
					$response = $paypalInstallments->getInstallmentInfo($cartTotal, $_SESSION['currency'], 'DE');
					if(!empty($response->financing_options[0]->qualifying_financing_options))
					{
						$representativeOption = $paypalInstallments->getRepresentativeOption($response->financing_options[0]->qualifying_financing_options);
						$specificContentView = MainFactory::create('PayPalInstallmentSpecificUpstreamPresentmentContentView');
						$specificContentView->qualifyingOptions           = $response->financing_options[0]->qualifying_financing_options;
						$specificContentView->nonQualifyingOptions        = $response->financing_options[0]->non_qualifying_financing_options;
						$specificContentView->lender                      = implode(', ', [COMPANY_NAME, TRADER_STREET .' '. TRADER_STREET_NUMBER, TRADER_ZIPCODE .' '. TRADER_LOCATION]);
						$specificContentView->currency                    = $_SESSION['currency'];
						$specificContentView->cashPurchasePrice           = $cartTotal;
						$specificContentView->numberOfInstallments        = $representativeOption->credit_financing->term;
						$specificContentView->borrowingRate               = $representativeOption->credit_financing->nominal_rate;
						$specificContentView->annualPercentageRate        = $representativeOption->credit_financing->apr;
						$specificContentView->installmentAmount           = $representativeOption->monthly_payment->value;
						$specificContentView->totalAmount                 = $representativeOption->total_cost->value;
						$specificContentView->representativeFinancingCode = $representativeOption->credit_financing->financing_code;
						$specificPresentment = $specificContentView->get_html();
					}
				}
				catch(Exception $e)
				{
					$specificPresentment .= '<!-- Exception in PPI -->';
				}
			}
		}

		$moduleTitle = MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_TITLE;
		$descriptionText = $this->text->get_text('installments_description');
		
		$selection = [
			'id'          => $this->code,
			'module'      => $moduleTitle,
			'description' => $specificPresentment ?
				$descriptionText . '<br>' . $specificPresentment :
				@constant('MODULE_PAYMENT_'.strtoupper($this->code).'_TEXT_DESCRIPTION'),
			'fields'      => [],
		];

		return $selection;
	}

	function pre_confirmation_check()
	{
		$confirmation_ok = false;

		if(isset($_SESSION['paypal_payment_installments']['cartID']) &&
			$_SESSION['paypal_payment_installments']['cartID'] !== $_SESSION['cart']->cartID)
		{
			unset($_SESSION['paypal_payment_installments']);
			xtc_redirect(xtc_href_link('checkout_payment.php', 'reason=cart_changed&payment_error='.$this->code, 'SSL'));
		}

		if(!isset($_GET['paymentId']))
		{
			if(gm_get_conf('GM_CHECK_CONDITIONS') != 1)
			{
				$_SESSION['conditions'] = 'true/not_reqd';
			}
			if(gm_get_conf('GM_CHECK_WITHDRAWAL') != 1)
			{
				$_SESSION['withdrawal'] = 'true/not_reqd';
			}

			$this->logger->notice('Instantiating Payment for installments mode');
			try
			{
				$order_total                             = new order_total();
				$order_total_array                       = $order_total->process();
				$paypalState                             = isset($_SESSION['paypal_state']) ? $_SESSION['paypal_state'] : null;
				$payPalPaymentFactory                    = MainFactory::create_object('PayPalPaymentFactory');
				$payPalPayment                           = $payPalPaymentFactory->createPaymentFromOrder($GLOBALS['order'], 'installments', $paypalState);
				$approvalLinkEntry                       = $payPalPayment->getLink('approval_url');
				$approvalUrl                             = $this->getApprovalUrl($approvalLinkEntry->href);
				$_SESSION['paypal_payment_installments'] = array(
					'id'     => $payPalPayment->id,
					'type'   => 'installments',
					'state'  => 'created',
					'cartID' => $_SESSION['cart']->cartID,
				);
				$this->logger->notice('Installment payment created with id '.$payPalPayment->id.', redirecting customer to '.$approvalUrl);
				xtc_redirect($approvalUrl);
			}
			catch(Exception $e)
			{
				$errorMessage = $this->text->get_text('error_creating_installments_payment');
				$this->logger->notice($errorMessage.': '.$e->getMessage());
				$_SESSION['paypal3_installments_error'] = $this->text->get_text('paypal_unavailable');
				unset($_SESSION['payment']);
				xtc_redirect(xtc_href_link('checkout_payment.php', 'payment_error='.$this->code, 'SSL'));
			}
		}

		if(!empty($_GET['paymentId']) &&
			$_GET['paymentId'] == $_SESSION['paypal_payment_installments']['id'] &&
			(!empty($_GET['PayerID']) || !empty($_SESSION['paypal_payment_installments']['payer_id'])))
		{
			$this->logger->notice('Payment approved by customer: '.$_SESSION['paypal_payment_installments']['id'].', PayerID: '.$_GET['PayerID']);
			$confirmation_ok = true;
			$_SESSION['paypal_payment_installments']['state'] = 'approved';
			$_SESSION['paypal_payment_installments']['payer_id'] = $_GET['PayerID'];
			$approvedPayment = MainFactory::create('PayPalPayment', $_SESSION['paypal_payment_installments']['id']);
			if(isset($approvedPayment->credit_financing_offered))
			{
				$_SESSION['paypal_payment_installments']['total_cost']     = $approvedPayment->credit_financing_offered->total_cost->value;
				$_SESSION['paypal_payment_installments']['total_interest'] = $approvedPayment->credit_financing_offered->total_interest->value;
			}
		}
		else
		{
			xtc_redirect(xtc_href_link('checkout_payment.php', 'reason=not_approved&payment_error='.$this->code, 'SSL'));
		}

		return $confirmation_ok;
	}

	/** changes approval URL to checkoutnow2-style (if required) */
	protected function getApprovalUrl($hateoas_approval_url)
	{
		$realApprovalUrl = $hateoas_approval_url;
		if($this->approval_url_rewriting_required === true)
		{
			if($this->configStorage->get('mode') === 'sandbox')
			{
				$approvalUrlBase = 'https://www.sandbox.paypal.com/checkoutnow/2';
			}
			else
			{
				$approvalUrlBase = 'https://www.paypal.com/checkoutnow/2';
			}
			$approvalUrlQuery = parse_url($hateoas_approval_url, PHP_URL_QUERY);
			$approvalUrlQueryParts = [];
			parse_str($approvalUrlQuery, $approvalUrlQueryParts);
			$token = $approvalUrlQueryParts['token'];
			$realApprovalUrl = $approvalUrlBase . '?' . http_build_query(['token' => $token, 'LocaleCode' => $this->getLocale()]);
		}
		return $realApprovalUrl;
	}

	/** for PayPal Installments locale is always de_DE */
	protected function getLocale()
	{
		return 'de_DE';
	}

	protected function _findCountryByID($country_id) {
		$query = "SELECT * FROM `countries` WHERE countries_id = ':countries_id'";
		$query = strtr($query, array(':countries_id' => (int)$country_id));
		$result = xtc_db_query($query);
		$country_data = false;
		while($row = xtc_db_fetch_array($result)) {
			$country_data = $row;
		}
		return $country_data;
	}

	function confirmation()
	{
		unset($_SESSION['paypal_payment_instruction_href']);
		$payment = MainFactory::create('PayPalPayment', $_SESSION['paypal_payment_installments']['id']);
		$title = sprintf('%s<br>%s %s %s&nbsp;%s',
			$this->logoImage,
			$payment->credit_financing_offered->term,
			$this->text->get_text('monthly_payments_nominative') . ' ' . $this->text->get_text('installments_of'),
			number_format($payment->credit_financing_offered->monthly_payment->value, 2, ',', ''),
			$payment->credit_financing_offered->monthly_payment->currency
		);

		$confirmation = array(
			'title' => $title,
		);
		return $confirmation;
	}

	function refresh()
	{
	}

	function process_button()
	{
		$order = $GLOBALS['order'];
		$pb = '';
		return $pb;
	}

	function after_process()
	{
		// $GLOBALS['order'], $_SESSION['tmp_oID'], $GLOBALS['order_totals']
		if(!empty($_SESSION['paypal_final_order_status']))
		{
			// set orders_status again, in case something has changed it
			$insert_id = (int)$_SESSION['tmp_oID'];
			$this->logger->notice(sprintf('Re-setting order status for %d to %d', $insert_id, $_SESSION['paypal_final_order_status']));
			xtc_db_query("UPDATE ".TABLE_ORDERS." SET orders_status='".$_SESSION['paypal_final_order_status']."' WHERE orders_id='".$insert_id."'");
			unset($_SESSION['paypal_final_order_status']);
		}
	}

	function before_process()
	{
		return false;
	}

	function payment_action()
	{
		$insert_id = $GLOBALS['insert_id'];
		$order = new order($insert_id);
		$this->logger->notice('Executing payment '.$_SESSION['paypal_payment_installments']['id'] .' for orders_id '.$insert_id);

		try
		{
			$paypalPayment = MainFactory::create('PayPalPayment', $_SESSION['paypal_payment_installments']['id']);
		}
		catch(Exception $e)
		{
			$errorMessage = $this->text->get_text('error_retrieving_payment');
			unset($_SESSION['paypal_payment_installments']);
			xtc_db_query("UPDATE ".TABLE_ORDERS." SET orders_status='".$this->order_status_error."' WHERE orders_id='".$insert_id."'");
			$this->_addOrdersStatusHistoryEntry($insert_id, $this->order_status_error, $errorMessage."\n".$e->getMessage());
			$this->logger->notice($errorMessage.': '.$e->getMessage());
			$_SESSION['paypal3_installments_error'] = $errorMessage;
			xtc_redirect(xtc_href_link('checkout_payment.php', 'payment_error='.$this->code, 'SSL'));
		}

		try
		{
			$paymentFactory = MainFactory::create('PayPalPaymentFactory');
			$this->logger->notice(sprintf('adding invoice_number to payment %s: %s', $_SESSION['paypal_payment_installments']['id'], $order->info['orders_id']));
			$paymentFactory->addInvoiceNumber($_SESSION['paypal_payment_installments']['id'], $order);
			$state = isset($_SESSION['paypal_state']) ? $_SESSION['paypal_state'] : null;
			$paypalPayment->execute($_SESSION['paypal_payment_installments']['payer_id'], $order, 'inst', $state);
			$this->_addPaymentToOrder($insert_id, $paypalPayment->id, $this->configStorage->get('mode'));
			$this->logger->notice('Reloading payment to find payment instruction: '.$paypalPayment->id);
			$paypalPayment = MainFactory::create('PayPalPayment', $paypalPayment->id);
			$paymentInstruction = $paypalPayment->payment_instruction;
			if($paymentInstruction !== null)
			{
				$this->logger->notice(sprintf('storing payment instruction for order %d in database', $order->info['orders_id']));
				$this->_storePaymentInstruction($paymentInstruction, $order->info['orders_id']);
			}
			$resourceZero = (array)$paypalPayment->transactions[0]->related_resources[0];
			$transaction = array_pop($resourceZero);
			$this->logger->notice('Transaction state after execute: ' . (string)$transaction->state);

			if($transaction->state == 'pending' && $this->order_status_pending)
			{
				$final_order_status = $this->order_status_pending;
				$paymentInstructionLink = false;
				foreach($transaction->links as $transactionLink)
				{
					if($transactionLink->rel == 'payment_instruction_redirect')
					{
						$paymentInstructionLink = $transactionLink->href;
					}
				}
				if($paymentInstructionLink !== false)
				{
					$this->logger->notice('Bank pending flow activated, link: '.$paymentInstructionLink);
					$_SESSION['paypal_payment_instruction_href'] = $paymentInstructionLink;
				}
				else
				{
					$this->logger->notice('No bank pending flow required');
				}
			}
			elseif($this->order_status)
			{
				$final_order_status = $this->order_status;
			}

			$_SESSION['paypal_final_order_status'] = $final_order_status;
			xtc_db_query("UPDATE ".TABLE_ORDERS." SET orders_status='".$final_order_status."' WHERE orders_id='".$insert_id."'");
			$status_comments = '';
			$status_comments .= $this->text->get_text('checkout_mode').': '.strtoupper($_SESSION['paypal_payment_installments']['type']);
			$this->_addOrdersStatusHistoryEntry($insert_id, $final_order_status, $status_comments);
			$this->logger->notice('Payment '.$_SESSION['paypal_payment_installments']['id'].' executed.');
			$GLOBALS['tmp'] = false;
		}
		catch(Exception $e)
		{
			$errorMessage = $this->text->get_text('error_executing_payment');
			unset($_SESSION['paypal_payment_installments']);
			xtc_db_query("UPDATE ".TABLE_ORDERS." SET orders_status='".$this->order_status_error."' WHERE orders_id='".$insert_id."'");
			$this->_addOrdersStatusHistoryEntry($insert_id, $this->order_status_error, $errorMessage."\n".$e->getMessage());
			$this->logger->notice('ERROR executing payment '.$paypalPayment->id.' - '.$e->getMessage());

			$_SESSION['paypal3_installments_error'] = $errorMessage;
			xtc_redirect(xtc_href_link('checkout_payment.php', 'payment_error='.$this->code, 'SSL'));
		}

		unset($_SESSION['paypal_payment_installments']);
		unset($_SESSION['paypal_state']);
		unset($_SESSION['paypal_abandonment_service']);
		unset($_SESSION['paypal_abandonment_download']);
	}

	function get_error()
	{
		$error = false;
		if(isset($_SESSION['paypal3_installments_error']))
		{
			$error = array('error' => $_SESSION['paypal3_installments_error']);
			unset($_SESSION['paypal3_installments_error']);
		}
		return $error;
	}

	function check()
	{
		if(!isset($this->_check))
		{
			$check_query = xtc_db_query("select configuration_value from ".TABLE_CONFIGURATION." where configuration_key = 'MODULE_PAYMENT_".  strtoupper($this->code) ."_STATUS'");
			$this->_check = xtc_db_num_rows($check_query);
		}
		return $this->_check;
	}

	function install()
	{
		$config = $this->_configuration();
		$sort_order = 0;
		foreach($config as $key => $data) {
			$install_query = "insert into ".TABLE_CONFIGURATION." ( configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, use_function, date_added) ".
					"values ('MODULE_PAYMENT_".strtoupper($this->code)."_".$key."', '".$data['configuration_value']."', '6', '".$sort_order."', '".addslashes($data['set_function'])."', '".addslashes($data['use_function'])."', now())";
			xtc_db_query($install_query);
			$sort_order++;
		}
	}

	function remove()
	{
		xtc_db_query("delete from ".TABLE_CONFIGURATION." where configuration_key in ('".implode("', '", $this->keys())."')");
	}

	/**
	 * Determines the module's configuration keys
	 * @return array
	 */
	function keys()
	{
		$ckeys = array_keys($this->_configuration());
		$keys = array();
		foreach($ckeys as $k)
		{
			$keys[] = 'MODULE_PAYMENT_'.strtoupper($this->code).'_'.$k;
		}
		return $keys;
	}

	function isInstalled()
	{
		$isInstalled = true;
		foreach($this->keys() as $key)
		{
			if(!defined($key))
			{
				$isInstalled = false;
			}
		}
		return $isInstalled;
	}

	function _configuration()
	{
		$config = array(
			'STATUS' => array(
				'configuration_value' => 'True',
				'set_function' => 'gm_cfg_select_option(array(\'True\', \'False\'), ',
			),
			'SORT_ORDER' => array(
				'configuration_value' => '-9998',
			),
			'ALLOWED' => array(
				'configuration_value' => '',
			),
			'ZONE' => array(
				'configuration_value' => '0',
				'use_function' => 'xtc_get_zone_class_title',
				'set_function' => 'xtc_cfg_pull_down_zone_classes(',
			),
		);

		return $config;
	}

	/**
	 * stores payment instruction (for payment upon invoice)
	 */
	protected function _storePaymentInstruction(stdClass $paymentInstruction, $orders_id)
	{
		$orders_id = (int)$orders_id;
		if($orders_id <= 0)
		{
			throw new Exception('invalid value for orders_id');
		}
		if(isset($paymentInstruction->payment_due_date))
		{
			$dueDate = xtc_db_input($paymentInstruction->payment_due_date);
		}
		else
		{
			$dueDate = '1000-01-01';
		}
		$insert_query =
			"INSERT
				INTO `orders_payment_instruction`
			SET
				`orders_id` = ':orders_id',
				`reference` = ':reference',
				`bank_name` = ':bank_name',
				`account_holder` = ':account_holder',
				`iban` = ':iban',
				`bic` = ':bic',
				`value` = ':value',
				`currency` = ':currency',
				`due_date` = ':due_date'";
		$insert_query = strtr($insert_query, array(
				':orders_id' => $orders_id,
				':reference' => xtc_db_input($paymentInstruction->reference_number),
				':bank_name' => xtc_db_input($paymentInstruction->recipient_banking_instruction->bank_name),
				':account_holder' => xtc_db_input($paymentInstruction->recipient_banking_instruction->account_holder_name),
				':iban' => xtc_db_input($paymentInstruction->recipient_banking_instruction->international_bank_account_number),
				':bic' => xtc_db_input($paymentInstruction->recipient_banking_instruction->bank_identifier_code),
				':value' => sprintf('%.2f', (double)$paymentInstruction->amount->value),
				':currency' => xtc_db_input($paymentInstruction->amount->currency),
				':due_date' => $dueDate,
			));
		xtc_db_query($insert_query);
	}

	protected function _addOrdersStatusHistoryEntry($orders_id, $orders_status_id, $comments)
	{
		$insert_query =
			'INSERT INTO
				orders_status_history
			SET
				orders_id = \':orders_id\',
				orders_status_id = \':orders_status_id\',
				date_added = NOW(),
				customer_notified = 0,
				comments = \':comments\'';
		$insert_query = strtr($insert_query, array(
				':orders_id' => (int)$orders_id,
				':orders_status_id' => (int)$orders_status_id,
				':comments' => xtc_db_input($comments),
			));
		xtc_db_query($insert_query);
	}

	protected function _addPaymentToOrder($orders_id, $payment_id, $mode)
	{
		$query =
			'REPLACE
				INTO `orders_paypal_payments`
			SET
				`orders_id` = \':orders_id\',
				`payment_id` = \':payment_id\',
				`mode` = \':mode\'
			';
		$query = strtr($query, array(':orders_id' => (int)$orders_id, ':payment_id' => xtc_db_input($payment_id), ':mode' => xtc_db_input($mode)));
		xtc_db_query($query);
	}

}

MainFactory::load_origin_class('paypal3_installments');
