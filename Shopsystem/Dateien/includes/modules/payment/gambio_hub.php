<?php
/* --------------------------------------------------------------
   gambio_hub.php 2018-10-27
   Gambio GmbH
   http://www.gambio.del
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$languageTextManager = MainFactory::create_object('LanguageTextManager', [], true);
$languageTextManager->init_from_lang_file('gambio_hub', $_SESSION['languages_id']);

use \HubPublic\Exceptions\CurlRequestException;
use \HubPublic\Exceptions\HubException;
use \HubPublic\Http\CurlRequest;
use \HubPublic\Serializers\CartContentSerializer;
use \HubPublic\Serializers\ClientSessionInformationSerializer;
use \HubPublic\Serializers\CustomerInformationSerializer;
use \HubPublic\Serializers\HubClientInformationSerializer;
use \HubPublic\Serializers\OrderContentSerializer;
use \HubPublic\ValueObjects\Builder\CustomerInformation as CustomerInformationBuilder;
use \HubPublic\ValueObjects\Builder\OrderContent as OrderContentBuilder;
use \HubPublic\ValueObjects\ClientSessionInformation;
use \HubPublic\ValueObjects\HubClientInformation;
use \HubPublic\ValueObjects\HubClientKey;
use \HubPublic\ValueObjects\HubSessionKey;
use \HubPublic\ValueObjects\HubTransactionCode;

/**
 * Class gambio_hub_ORIGIN.
 *
 * This module servers the Gambio Hub payment method in the shop. It will store in the database
 * the selected payment module and the transaction code for future reference. Make sure that you set
 * the correct Gambio Hub API URL in the settings.
 */
class gambio_hub_ORIGIN
{
	/**
	 * @var string
	 */
	public $code;
	
	/**
	 * @var string
	 */
	public $title;
	
	/**
	 * @var string
	 */
	public $description;
	
	/**
	 * @var bool
	 */
	public $enabled;
	
	/**
	 * @var bool
	 */
	public $tmpOrders = true;
	
	
	/**
	 * Class Constructor
	 */
	public function __construct()
	{
		global $order;
		
		$this->code        = 'gambio_hub';
		$this->title       = defined('MODULE_PAYMENT_GAMBIO_HUB_TEXT_TITLE') ? MODULE_PAYMENT_GAMBIO_HUB_TEXT_TITLE : '';
		$this->description = defined('MODULE_PAYMENT_GAMBIO_HUB_TEXT_DESCRIPTION') ? MODULE_PAYMENT_GAMBIO_HUB_TEXT_DESCRIPTION : '';
		$this->enabled     = MODULE_PAYMENT_GAMBIO_HUB_STATUS === 'True';
		$this->info        = defined('MODULE_PAYMENT_GAMBIO_HUB_TEXT_INFO') ? MODULE_PAYMENT_GAMBIO_HUB_TEXT_INFO : '';
		
		if(defined('MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID') && (int)MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID > 0)
		{
			$this->order_status = MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID;
		}
		
		if(is_object($order))
		{
			$this->update_status();
		}
		
		// set gambio_hub_selection-SESSION-parameter in constructor, because ::pre_confirmation_check is too late for
		// ot-modules functionality
		if(isset($_POST) && array_key_exists('gambio_hub_selection', $_POST))
		{
			$_SESSION['gambio_hub_selection'] = $_POST['gambio_hub_selection'];
		}
	}
	
	
	/**
	 * This method is not used.
	 *
	 * @return bool
	 */
	public function update_status()
	{
		return false;
	}
	
	
	/**
	 * This method is not used.
	 *
	 * @return bool
	 */
	public function javascript_validation()
	{
		return false;
	}
	
	
	/**
	 * Provides module selection information.
	 *
	 * @return array
	 */
	public function selection()
	{
		return [
			'id'          => $this->code,
			'module'      => $this->title,
			'description' => $this->info,
		];
	}
	
	
	/**
	 * Sets the "gambio_hub_selection" in the PHP Session and redirects to module page via outputted html if payment
	 * module needs a extra steps before the confirmation page like PayPal.
	 *
	 * @todo implement whitelisting for GET and POST data
	 *
	 * @return bool
	 */
	public function pre_confirmation_check()
	{
		$helper = MainFactory::create('HubCheckoutHelper');
		
		// Hub transactions API client.
		$hubTransactionsApiClient = $helper->createHubTransactionsApiClient(new HubSessionKey($_SESSION['gambio_hub_session_key']));
		
		$cartContent              = $helper->getCartContent($GLOBALS['order']);
		$customerInformation      = $helper->getCustomerInformation($GLOBALS['order']);
		$hubClientInformation     = $helper->getHubClientInformation();
		$clientSessionInformation = $helper->getClientSessionInformation($GLOBALS['order']);
		$getData                  = $_GET ? : [];
		$postData                 = $_POST ? : [];
		
		try
		{
			$pageContentHtml = $hubTransactionsApiClient->getBeforeTransactionPageContent($cartContent,
			                                                                              $customerInformation,
			                                                                              $hubClientInformation,
			                                                                              $clientSessionInformation,
			                                                                              $getData, $postData,
			                                                                              $_SESSION['gambio_hub_selection']);
			
			if($pageContentHtml !== '')
			{
				echo $pageContentHtml;
				
				xtc_db_close();
				exit;
			}
		}
		catch(UnexpectedValueException $e)
		{
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
		}
		catch(CurlRequestException $e)
		{
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
		}
		
		return false;
	}
	
	
	/**
	 * Returns the module confirmation info.
	 *
	 * @return array
	 */
	public function confirmation()
	{
		$helper = MainFactory::create('HubCheckoutHelper');
		
		$hubTransactionsApiClient = $helper->createHubTransactionsApiClient(new HubSessionKey($_SESSION['gambio_hub_session_key']));
		
		$cartContent              = $helper->getCartContent($GLOBALS['order']);
		$customerInformation      = $helper->getCustomerInformation($GLOBALS['order']);
		$hubClientInformation     = $helper->getHubClientInformation();
		$clientSessionInformation = $helper->getClientSessionInformation($GLOBALS['order']);
		$getData                  = $_GET ? : [];
		$postData                 = $_POST ? : [];
		
		try
		{
			$confirmation = $hubTransactionsApiClient->getConfirmationContents($cartContent, $customerInformation,
			                                                                   $hubClientInformation,
			                                                                   $clientSessionInformation, $getData,
			                                                                   $postData,
			                                                                   $_SESSION['gambio_hub_selection']);
			
			$infoText                    = $_SESSION['gambio_hub_payments'][$_SESSION['gambio_hub_selection']]['info_text'];
			$confirmationArray           = [];
			$confirmationArray['fields'] = [];
			
			if($infoText !== '')
			{
				$confirmationArray['title'] = $infoText . '<br/><br/>';
			}
			
			$confirmationArray['title'] .= $confirmation['orderPaymentInstructions'];
			
			foreach($confirmation['additionalModuleValues'] as $key => $value)
			{
				$confirmationArray['fields'][] = ['title' => $key, 'field' => $value];
			}
			
			return $confirmationArray;
		}
		catch(UnexpectedValueException $e)
		{
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
		}
		catch(CurlRequestException $e)
		{
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
		}
	}
	
	
	/**
	 * This method is not used.
	 *
	 * @return bool
	 */
	public function process_button()
	{
		return false;
	}
	
	
	/**
	 * This method is not used.
	 *
	 * @return bool
	 */
	public function before_process()
	{
		return false;
	}
	
	
	/**
	 * Unsets hub session key after transaction completes.
	 *
	 * @return bool
	 */
	public function after_process()
	{
		if(array_key_exists('gambio_hub_session_key', $_SESSION))
		{
			unset($_SESSION['gambio_hub_session_key']);
		}
		
		return false;
	}
	
	
	/**
	 * This method is not used.
	 *
	 * @return bool
	 */
	public function get_error()
	{
		$error = false;
		if(null !== $_SESSION['gambio_hub_error'])
		{
			$error                        = [
				'error' => $_SESSION['gambio_hub_error'],
			];
			$_SESSION['gambio_hub_error'] = null;
		}
		
		return $error;
	}
	
	
	/**
	 * Returns whether the module is installed or not.
	 *
	 * @return bool|int
	 */
	public function check()
	{
		if(!isset ($this->_check))
		{
			$sql          = '
				SELECT `configuration_value`
			    FROM ' . TABLE_CONFIGURATION . '
				WHERE `configuration_key` = "MODULE_PAYMENT_GAMBIO_HUB_STATUS"
			';
			$check_query  = xtc_db_query($sql);
			$this->_check = xtc_db_num_rows($check_query);
		}
		
		return $this->_check;
	}
	
	
	/**
	 * Installation callback of the module.
	 */
	public function install()
	{
		// Add active state configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . "
		              (configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, date_added)
		              VALUES ('MODULE_PAYMENT_GAMBIO_HUB_STATUS', 'False', '6', '1', 'gm_cfg_select_option(array(\'True\', \'False\'), ', now())");
		
		// Add allowed state configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . "
		              (configuration_key, configuration_value,  configuration_group_id, sort_order, date_added)
		              VALUES ('MODULE_PAYMENT_GAMBIO_HUB_ALLOWED', '', '6', '0', now())");
		
		// Add sort order configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . "
		              (configuration_key, configuration_value,  configuration_group_id, sort_order, date_added)
		              VALUES ('MODULE_PAYMENT_GAMBIO_HUB_SORT_ORDER', '0', '6', '0', now())");
		
		// Add zone configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . "
		              (configuration_key, configuration_value,  configuration_group_id, sort_order, use_function, set_function, date_added)
		              VALUES ('MODULE_PAYMENT_GAMBIO_HUB_ZONE', '0', '6', '2', 'xtc_get_zone_class_title', 'xtc_cfg_pull_down_zone_classes(', now())");
		
		// Add order status configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . "
		              (configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, use_function, date_added)
		              VALUES ('MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID', '0', '6', '0', 'xtc_cfg_pull_down_order_statuses(', 'xtc_get_order_status_name', now())");
		
		// Add Hub URL configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " 
					  (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) 
					  VALUES ('MODULE_PAYMENT_GAMBIO_HUB_URL', 'https://core-api.gambiohub.com/a/api.php/api/v1', '6', '0', now())
					  ");
		
		// Add Hub settings app URL configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " 
					  (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) 
					  VALUES ('MODULE_PAYMENT_GAMBIO_HUB_SETTINGS_APP_URL', 'https://gui.gambiohub.com/a/settings', '6', '0', now())
					  ");
		
		// Add Hub account app URL configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " 
					  (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) 
					  VALUES ('MODULE_PAYMENT_GAMBIO_HUB_ACCOUNT_APP_URL', 'https://gui.gambiohub.com/a/account', '6', '0', now())
					  ");
		
		// Add Hub IP list URL configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " 
					  (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) 
					  VALUES ('MODULE_PAYMENT_GAMBIO_HUB_IP_LIST_URL', 'https://core-api.gambiohub.com/trust/hub_hosts.json', '6', '0', now())
					  ");
		
		// Add Hub REST actions URL configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " 
					  (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) 
					  VALUES ('MODULE_PAYMENT_GAMBIO_HUB_REST_ACTIONS_URL', 'https://core-api.gambiohub.com/api/trust/rest_actions.json', '6', '0', now())
					  ");
		
		// Add Hub REST actions URL configuration value.
		xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " 
					  (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) 
					  VALUES ('MODULE_PAYMENT_GAMBIO_HUB_IP_WHITELIST', '', '6', '0', now())
					  ");
		
		if(!$this->_columnExists('gambio_hub_module'))
		{
			xtc_db_query("ALTER TABLE `orders` ADD COLUMN `gambio_hub_module` VARCHAR(32) DEFAULT ''");
		}
		
		if(!$this->_columnExists('gambio_hub_module_title'))
		{
			xtc_db_query("ALTER TABLE `orders` ADD COLUMN `gambio_hub_module_title` VARCHAR(255) DEFAULT ''");
		}
		
		if(!$this->_columnExists('gambio_hub_transaction_code'))
		{
			xtc_db_query("ALTER TABLE `orders` ADD COLUMN `gambio_hub_transaction_code` VARCHAR(64) DEFAULT ''");
		}
	}
	
	
	/**
	 * Removal callback of the module.
	 */
	public function remove()
	{
		// Remove all module-related configuration values from configuration table.
		xtc_db_query("DELETE FROM " . TABLE_CONFIGURATION . "
					  WHERE configuration_key in ('" . implode("', '", $this->keys()) . "')");
	}
	
	
	/**
	 * Returns the module keys.
	 *
	 * @return array
	 */
	public function keys()
	{
		return [
			'MODULE_PAYMENT_GAMBIO_HUB_STATUS',
			'MODULE_PAYMENT_GAMBIO_HUB_ALLOWED',
			'MODULE_PAYMENT_GAMBIO_HUB_SORT_ORDER',
			'MODULE_PAYMENT_GAMBIO_HUB_ZONE',
			'MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID',
			'MODULE_PAYMENT_GAMBIO_HUB_URL',
			'MODULE_PAYMENT_GAMBIO_HUB_SETTINGS_APP_URL',
			'MODULE_PAYMENT_GAMBIO_HUB_ACCOUNT_APP_URL',
			'MODULE_PAYMENT_GAMBIO_HUB_IP_LIST_URL',
			'MODULE_PAYMENT_GAMBIO_HUB_IP_WHITELIST',
			'MODULE_PAYMENT_GAMBIO_HUB_REST_ACTIONS_URL',
		];
	}
	
	
	/**
	 * Payment action of the module.
	 *
	 * This method will start a new Gambio Hub transaction and store the transaction details to the database.
	 * The user will then be redirected to the payment site.
	 */
	public function payment_action()
	{
		try
		{
			// send additional parameters to hub
			if(isset($_POST[$_SESSION['gambio_hub_selection']]) && is_array($_POST[$_SESSION['gambio_hub_selection']]))
			{
				$moduleParameters     = $_POST[$_SESSION['gambio_hub_selection']];
				$hubSettings          = MainFactory::create('HubSettings', gm_get_conf('GAMBIO_HUB_CURL_TIMEOUT'));
				$hubCallbackApiClient = MainFactory::create('HubCallbackApiClient', MODULE_PAYMENT_GAMBIO_HUB_URL,
				                                            new CurlRequest(), LogControl::get_instance(),
				                                            $hubSettings);
				$hubCallbackApiClient->execute($_SESSION['gambio_hub_selection'], true, [
					'action' => 'moduleParameters',
				], [
					                               'sessionKey'       => $_SESSION['gambio_hub_session_key'],
					                               'moduleParameters' => json_encode($moduleParameters),
				                               ]);
			}
			
			// Store the Gambio Hub Module in the database.
			$this->_storeGambioHubModule($GLOBALS['insert_id'], $_SESSION['gambio_hub_selection']);
			
			// Store the Gambio Hub Module title in the database.
			$this->_storeGambioHubModuleTitle($GLOBALS['insert_id'],
			                                  $_SESSION['gambio_hub_payments'][$_SESSION['gambio_hub_selection']]['title']);
			
			// Hub transactions API client.
			$client = $this->_createHubTransactionsApiClient(new HubSessionKey($_SESSION['gambio_hub_session_key']));
			
			// Start the transaction and retrieve the transaction code.
			$transactionCode = $client->startTransaction($this->_createHubClientInformation(),
			                                             $this->_createOrderContent(),
			                                             $this->_createClientSessionInformation(new HubSessionKey($_SESSION['gambio_hub_session_key'])));
			
			// Get the transaction details using the transaction code.
			$transactionDetails = $client->getTransactionDetails(new HubTransactionCode($transactionCode));
			
			// set order status
			$comment = array_key_exists('statusComment',
			                            $transactionDetails) ? $transactionDetails['statusComment'] : '';
			$this->setOrderStatus($GLOBALS['insert_id'], $transactionDetails['status'], $comment);
			
			// Payment instructions
			if(!empty($transactionDetails['paymentInstructions']))
			{
				xtc_db_perform('orders_payment_instruction', $transactionDetails['paymentInstructions']);
			}
			
			// Store the Gambio Hub transaction code in the database.
			$this->_storeGambioHubTransactionCode($GLOBALS['insert_id'], $transactionCode);
			
			if(!empty($transactionDetails['errorMessage']))
			{
				throw new HubException($transactionDetails['errorMessage']);
			}
			
			// Transaction pay URL.
			$payUrl = $transactionDetails['transactionPayUrl'] ? : xtc_href_link(FILENAME_CHECKOUT_PROCESS, '', 'SSL');
			
			// Payment instructions
			if(!empty($transactionDetails['payment_instructions']))
			{
				xtc_db_perform('orders_payment_instruction', $transactionDetails['payment_instructions']);
			}
			
			// Add the session in the transaction URL.
			$payUrl .= (!array_key_exists('query', parse_url($payUrl)) ? '?' : '&') . 'session_key='
			           . $_SESSION['gambio_hub_session_key'];
			
			// Redirect to pay site.
			xtc_redirect($payUrl);
		}
		catch(UnexpectedValueException $e)
		{
			// Redirect to checkout payment page, if Gambio Hub is not available
			$_SESSION['gambio_hub_error'] = MODULE_PAYMENT_GAMBIO_HUB_ERROR;
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, 'payment_error=' . $this->code, 'SSL'));
		}
		catch(CurlRequestException $e)
		{
			$_SESSION['gambio_hub_error'] = MODULE_PAYMENT_GAMBIO_HUB_ERROR;
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, 'payment_error=' . $this->code, 'SSL'));
		}
		catch(HubException $e)
		{
			$_SESSION['gambio_hub_error'] = $e->getMessage();
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, 'payment_error=' . $this->code, 'SSL'));
		}
	}
	
	
	protected function setOrderStatus($orders_id, $status_id, $comment = '')
	{
		$setStatusSql = '
			UPDATE `orders` 
			SET `orders_status` = :status_id
			WHERE `orders_id` = :orders_id';
		$setStatusSql = strtr($setStatusSql, [
			':orders_id' => (int)$orders_id,
			':status_id' => (int)$status_id,
		]);
		xtc_db_query($setStatusSql);
		
		$historySql = '
			INSERT INTO `orders_status_history`
			SET 
				`orders_id` = :orders_id,
				`orders_status_id` = :status_id,
				`date_added` = \':date_added\',
				`customer_notified` = 0,
				`comments` = \':comments\'';
		$historySql = strtr($historySql, [
			':orders_id'  => (int)$orders_id,
			':status_id'  => (int)$status_id,
			':date_added' => date('Y-m-d H:i:s'),
			':comments'   => xtc_db_input($comment),
		]);
		xtc_db_query($historySql);
	}
	
	
	/**
	 * Stores the Gambio Hub module to the orders table.
	 *
	 * @param int    $orderId The current order ID.
	 * @param string $module  The hub module name.
	 */
	protected function _storeGambioHubModule($orderId, $module)
	{
		$sql = '
			UPDATE `orders` 
			SET `gambio_hub_module` = "' . xtc_db_input($module) . '" 
			WHERE `orders_id` = ' . (int)$orderId;
		xtc_db_query($sql);
	}
	
	
	/**
	 * Stores the Gambio Hub module title to the orders table.
	 *
	 * @param int    $orderId     The current order ID.
	 * @param string $moduleTitle The hub module name.
	 */
	protected function _storeGambioHubModuleTitle($orderId, $moduleTitle)
	{
		$sql = '
			UPDATE `orders` 
			SET `gambio_hub_module_title` = "' . xtc_db_input($moduleTitle) . '" 
			WHERE `orders_id` = ' . (int)$orderId;
		xtc_db_query($sql);
	}
	
	
	/**
	 * Store the Gambio Hub transaction code to the orders table.
	 *
	 * @param int $orderId The current order ID.
	 * @param     string   gambio_hub_transaction_code  The hub transaction code.
	 */
	protected function _storeGambioHubTransactionCode($orderId, $transactionCode)
	{
		$sql = '
			UPDATE orders 
			SET gambio_hub_transaction_code = "' . xtc_db_input($transactionCode) . '" 
			WHERE orders_id = ' . (int)$orderId;
		xtc_db_query($sql);
	}
	
	
	/**
	 * Check if given column exists in the orders table.
	 *
	 * @param $column
	 *
	 * @return bool
	 */
	protected function _columnExists($column)
	{
		$sql   = 'SHOW COLUMNS IN orders LIKE "' . xtc_db_input($column) . '"';
		$query = xtc_db_query($sql);
		
		return xtc_db_num_rows($query) > 0;
	}
	
	
	/**
	 * Creates a HubClientInformation instance.
	 *
	 * @return  \HubPublic\ValueObjects\HubClientInformation Created instance.
	 */
	protected function _createHubClientInformation()
	{
		/**
		 * Hub client key configuration.
		 *
		 * @var HubClientKeyConfiguration $hubClientKeyConfiguration
		 */
		$hubClientKeyConfiguration = MainFactory::create('HubClientKeyConfiguration');
		
		// Hub client key.
		$hubClientKey = new HubClientKey($hubClientKeyConfiguration->get());
		
		// Client version.
		$shopVersion = ltrim(gm_get_conf('INSTALLED_VERSION'), 'v');
		
		// Client URL.
		$shopUrl = GM_HTTP_SERVER . DIR_WS_CATALOG;
		
		return new HubClientInformation($hubClientKey, $shopVersion, $shopUrl);
	}
	
	
	/**
	 * Creates a HubTransactionsApiClient instance.
	 *
	 * @param \HubPublic\ValueObjects\HubSessionKey $sessionKey Hub session key.
	 *
	 * @return \HubTransactionsApiClient Created instance.
	 */
	protected function _createHubTransactionsApiClient($sessionKey)
	{
		// cURL request.
		$request = new CurlRequest();
		
		// Cart content serializer.
		$cartContentSerializer = new CartContentSerializer();
		
		// Customer information serializer.
		$customerInformationSerializer = new CustomerInformationSerializer();
		
		// Hub client information serializer.
		$hubClientInformationSerializer = new HubClientInformationSerializer();
		
		// Session information serializer.
		$clientSessionInformationSerializer = new ClientSessionInformationSerializer();
		
		// Order content serializer.
		$orderContentSerializer = new OrderContentSerializer($customerInformationSerializer);
		
		// Shop logger instance.
		$logControl = LogControl::get_instance();
		
		// Hub settings instance.
		$hubSettings = MainFactory::create('HubSettings', gm_get_conf('GAMBIO_HUB_CURL_TIMEOUT'));
		
		return MainFactory::create('HubTransactionsApiClient', MODULE_PAYMENT_GAMBIO_HUB_URL, $sessionKey, $request,
		                           $cartContentSerializer, $customerInformationSerializer,
		                           $hubClientInformationSerializer, $clientSessionInformationSerializer,
		                           $orderContentSerializer, $logControl, $hubSettings);
	}
	
	
	/**
	 * Creates a OrderContent instance.
	 *
	 * @return \HubPublic\ValueObjects\OrderContent Created instance.
	 */
	protected function _createOrderContent()
	{
		// Submitted order content.
		$order = $GLOBALS['order'];
		
		// Submitted order info content.
		$orderInfo = $order->info;
		
		// Submitted billing content.
		$billing = $order->billing;
		
		// Submitted customer content.
		$customer = $order->customer;
		
		// Submitted shipping content.
		$shipping = $order->delivery;
		
		// Order ID.
		$orderId = $GLOBALS['insert_id'];
		
		// Customer's ID.
		$customerId = $_SESSION['customer_id'];
		
		// Missing order content information.
		$missingInformation = $this->_getMissingOrderContentInformation($orderId, $customerId);
		
		// Formatted date time string.
		$customerDateOfBirthFormatted = new DateTime($missingInformation['customer']['customers_dob']);
		$customerDateOfBirthFormatted = $customerDateOfBirthFormatted->format('Y-m-d');
		
		// Parsed order created date time.
		$orderCreatedDateTimeParsed = new DateTime($missingInformation['order']['date_purchased']);
		
		// CustomerInformation instance builder.
		$customerInformationBuilder = new CustomerInformationBuilder();
		
		// Insert billing values to CustomerInformation instance.
		$customerInformationBuilder->setBillingAddress1("{$billing['street_address']} {$billing['house_number']}")
		                           ->setBillingAddress2((string)$billing['additional_address_info'])
		                           ->setBillingCity((string)$billing['city'])
		                           ->setBillingCompany((string)$billing['company'])
		                           ->setBillingCountry((string)$billing['country']['title'])
		                           ->setBillingCountryCode((string)$billing['country']['iso_code_2'])
		                           ->setBillingFirstName((string)$billing['firstname'])
		                           ->setBillingGender((string)$billing['gender'])
		                           ->setBillingLastName((string)$billing['lastname'])
		                           ->setBillingPostalCode((string)$billing['postcode'])
		                           ->setBillingState((string)$billing['state'])
		                           ->setBillingTitle((string)$billing['gender'] === 'm' ? MALE : FEMALE);
		
		// Insert customer values to CustomerInformation instance.
		$customerInformationBuilder->setCustomerAddress1("{$customer['street_address']} {$customer['house_number']}")
		                           ->setCustomerAddress2((string)$customer['additional_address_info'])
		                           ->setCustomerCity((string)$customer['city'])
		                           ->setCustomerCompany((string)$customer['company'])
		                           ->setCustomerCountry((string)$customer['country']['title'])
		                           ->setCustomerCountryCode((string)$customer['country']['iso_code_2'])
		                           ->setCustomerDateOfBirth((string)$customerDateOfBirthFormatted)
		                           ->setCustomerEmail((string)$customer['email_address'])
		                           ->setCustomerFax((string)$missingInformation['customer']['customers_fax'])
		                           ->setCustomerFirstName((string)$customer['firstname'])
		                           ->setCustomerGender((string)$customer['gender'])
		                           ->setCustomerLastName((string)$customer['lastname'])
		                           ->setCustomerNumber((string)$customerId)
		                           ->setCustomerPhone((string)$customer['telephone'])
		                           ->setCustomerPostalCode((string)$customer['postcode'])
		                           ->setCustomerState((string)$customer['state'])
		                           ->setCustomerTitle((string)$customer['gender'] === 'm' ? MALE : FEMALE);
		
		// Insert shipping values to CustomerInformation instance.
		$customerInformationBuilder->setShippingAddress1("{$shipping['street_address']} {$shipping['house_number']}")
		                           ->setShippingAddress2((string)$shipping['additional_address_info'])
		                           ->setShippingCity((string)$shipping['city'])
		                           ->setShippingCompany((string)$shipping['company'])
		                           ->setShippingCountry((string)$shipping['country']['title'])
		                           ->setShippingCountryCode((string)$shipping['country']['iso_code_2'])
		                           ->setShippingFirstName((string)$shipping['firstname'])
		                           ->setShippingGender((string)$shipping['gender'])
		                           ->setShippingLastName((string)$shipping['lastname'])
		                           ->setShippingPostalCode((string)$shipping['postcode'])
		                           ->setShippingState((string)$shipping['state'])
		                           ->setShippingTitle((string)$shipping['gender'] === 'm' ? MALE : FEMALE);
		
		// Build CustomerInformation instance.
		$customerInformation = $customerInformationBuilder->build();
		
		// OrderContent class builder.
		$orderContentBuilder = new OrderContentBuilder();
		
		$total = $orderInfo['total'];
		if($_SESSION['customers_status']['customers_status_show_price_tax'] === '0'
		   && $_SESSION['customers_status']['customers_status_add_tax_ot'] === '1')
		{
			$total += $orderInfo['tax'];
		}
		
		// Insert values to OrderContent class.
		$orderContentBuilder->setAmount(round($total, 2))
		                    ->setCurrencyCode($orderInfo['currency'])
		                    ->setCustomer($customerInformation)
		                    ->setCustomerNumber($customerId)
		                    ->setLanguageCode($_SESSION['language_code'])
		                    ->setOrderDateTime($orderCreatedDateTimeParsed)
		                    ->setOrderNumber($orderId)
		                    ->setPaymentMethod((string)$_SESSION['gambio_hub_selection'])
		                    ->setShippingMethod($orderInfo['shipping_class']);
		
		return $orderContentBuilder->build();
	}
	
	
	/**
	 * Returns the missing order content information.
	 *
	 * @param int $orderId    Order ID to fetch the missing information for.
	 * @param int $customerId Customer ID to fetch the missing information for.
	 *
	 * @return array
	 *
	 * @throws InvalidArgumentException If the provided arguments are not valid ID values.
	 */
	protected function _getMissingOrderContentInformation($orderId, $customerId)
	{
		if(!is_numeric($orderId) || (int)$orderId != $orderId || is_float($orderId) || $orderId < 0)
		{
			throw new InvalidArgumentException('Invalid $orderId argument value: ' . $orderId);
		}
		
		if(!is_numeric($customerId) || (int)$customerId != $customerId || is_float($customerId) || $customerId < 0)
		{
			throw new InvalidArgumentException('Invalid $customerId argument value: ' . $customerId);
		}
		
		// Data array which will be returned.
		$result = [];
		
		// Query to fetch the missing information an order.
		$orderQuery = xtc_db_query('
			SELECT `date_purchased`
			FROM `orders`
			WHERE `orders_id` = ' . (int)$orderId . '
		');
		
		// Fetch order query result.
		$orderQueryResult = xtc_db_fetch_array($orderQuery);
		
		// Add order result to data array.
		$result['order'] = $orderQueryResult;
		
		// Query to fetch the missing information a customer.
		$customerQuery = xtc_db_query('
			SELECT `customers_fax`, `customers_dob`
			FROM `customers`
			WHERE `customers_id` = ' . (int)$customerId . '
		');
		
		// Fetch customer query result.
		$customerQueryResult = xtc_db_fetch_array($customerQuery);
		
		// Add customer result to data array.
		$result['customer'] = $customerQueryResult;
		
		return $result;
	}
	
	
	/**
	 * Creates a ClientSessionInformation instance.
	 *
	 * @param \HubPublic\ValueObjects\HubSessionKey $sessionKey Hub session key.
	 *
	 * @return \HubPublic\ValueObjects\ClientSessionInformation Created instance.
	 */
	protected function _createClientSessionInformation($sessionKey)
	{
		// Language code.
		$languageCode = $_SESSION['language_code'];
		
		// Currency.
		$currency = $_SESSION['currency'];
		
		// User IP address.
		$userIp = $_SERVER['REMOTE_ADDR'];
		
		// User agent.
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
		
		return new ClientSessionInformation($sessionKey, $languageCode, $currency, $userIp, $userAgent);
	}
}

MainFactory::load_origin_class('gambio_hub');
