<?php
/* --------------------------------------------------------------
	ot_ppinstfee.php 2016-09-28
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$languageTextManager = MainFactory::create_object('LanguageTextManager', array(), true);
$languageTextManager->init_from_lang_file('ot_paypal3_instfee_module', $_SESSION['languages_id']);

class ot_paypal3_instfee_ORIGIN
{
	public $module_key;
	public $code;
	public $title;
	public $output;

	public function __construct()
	{
		$this->module_key  = 'paypal3_instfee';
		$this->code        = 'ot_' . $this->module_key;
		$this->title       = constant($this->getConfigurationKey('TITLE_ADMIN'));
		$this->description = constant($this->getConfigurationKey('DESCRIPTION'));
		$this->enabled     = strtolower((string)@constant($this->getConfigurationKey('STATUS'))) === 'true';
		$this->sort_order  = (int)@constant($this->getConfigurationKey('SORT_ORDER'));
		$this->output      = [];
	}

	public function process()
	{
		if($_SESSION['payment'] === 'paypal3_installments' && !empty($_SESSION['paypal_payment_installments']))
		{
			$order      = $GLOBALS['order'];
			$xtPrice    = $GLOBALS['xtPrice'];
			$fee        = $_SESSION['paypal_payment_installments']['total_interest'];
			$totalTotal = $_SESSION['paypal_payment_installments']['total_cost'];

			$this->output[] = [
				'title' => '<br>' . constant($this->getConfigurationKey('FEE_TITLE')) . ':',
				'text'  => '<br>' . $xtPrice->xtcFormat($fee, true),
				'value' => $xtPrice->xtcFormat($fee, false),
			];

			$this->output[] = [
				'title' => sprintf('<b>%s:</b>', constant($this->getConfigurationKey('TITLE'))),
				'text'  => sprintf('<b>%s</b>', $xtPrice->xtcFormat($totalTotal, true)),
				'value' => $xtPrice->xtcFormat($totalTotal, false),
			];
		}
	}

	public function check()
	{
		$status = @constant($this->getConfigurationKey('STATUS'));
		$check  = empty($status) === false;
		return $check;
	}

	public function keys()
	{
		$keys = array_keys($this->getConfiguration());
		$keys_full = array_map([$this, 'getConfigurationKey'], $keys);
		return $keys_full;
	}

	public function install()
	{
		$metaData = ['configuration_group_id' => 6, 'date_added' => date('Y-m-d H:i:s')];
		$db = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$sortOrder = 1;
		foreach($this->getConfiguration() as $configKey => $configEntry)
		{
			$insertData = array_merge($configEntry, $metaData, ['sort_order' => $sortOrder]);
			$insertData['configuration_key'] = $this->getConfigurationKey($configKey);
			$db->replace('configuration', $insertData);
			$sortOrder++;
		}
	}

	public function remove()
	{
		$db = StaticGXCoreLoader::getDatabaseQueryBuilder();
		foreach(array_keys($this->getConfiguration()) as $configKey)
		{
			$db->delete('configuration', ['configuration_key' => $this->getConfigurationKey($configKey)]);
		}
	}

	protected function getConfiguration()
	{
		$configuration = [
			'STATUS' => [
				'configuration_value' => 'true',
				'set_function'        => 'gm_cfg_select_option(array(\'true\', \'false\'), ',
			],

			'SORT_ORDER' => [
				'configuration_value' => '100',
			],
		];
		return $configuration;
	}

	protected function getConfigurationKey($keyName)
	{
		return 'MODULE_ORDER_TOTAL_' . strtoupper($this->module_key) . '_' . strtoupper($keyName);
	}

}

MainFactory::load_origin_class('ot_paypal3_instfee');

