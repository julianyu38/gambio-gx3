'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* --------------------------------------------------------------
	payone_checkout.js 2018-12-05
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2015 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Payone Checkout
 *
 * @module Widgets/payone_checkout
 */
gambio.widgets.module('payone_checkout', [], function (data) {

	'use strict';

	// ########## VARIABLE INITIALIZATION ##########

	var $this = $(this),
	    defaults = {},
	    options = $.extend(true, {}, defaults, data),
	    p1_debug = true,
	    module = {};

	// ########## PAYONE FUNCTIONS ##########

	var _p1_payment_submit_handler = function _p1_payment_submit_handler(e) {
		var selected_payment = document.querySelector('#checkout_payment input[name="payment"]:checked').value;
		if (selected_payment === 'payone_cc') {
			if (p1_debug) {
				console.log('payone cc check triggered');
			}
			e.preventDefault();
			p1cc_check();
		}
	};

	var _initOnlineTransfer = function _initOnlineTransfer() {
		$('select#otrans_type').on('change', function (e) {
			var selected_type = $(this).val();
			var $pd_table = $(this).closest('table.payone_otrans_data');
			var $datarows = $('tr.datarow', $pd_table);
			$datarows.hide();
			$('.for_' + selected_type).show();
			if (selected_type == 'pfefinance' || selected_type == 'pfcard') {
				$(this).closest('div.payment_item').addClass('data_valid');
				$(this).closest('div.payment_item').click();
			}
		});
		$('select#otrans_type').trigger('change');

		var otrans_input_handler = function otrans_input_handler(e) {
			var any_empty = false;
			$('.payone_otrans_data input[type="text"]:visible').each(function () {
				if ($(this).val() === '') {
					any_empty = true;
				}
			});
			if (any_empty === true) {
				$('table.payone_otrans_data').addClass('payone_data_missing');
			} else {
				$('table.payone_otrans_data').removeClass('payone_data_missing');
			}
			$(this).closest('div.payment_item').removeClass('data_valid');
		};

		$('.payone_otrans_data input[type="text"]').keyup(otrans_input_handler);
		$('.payone_otrans_data input[type="text"]').change(otrans_input_handler);
	};

	var _initELV = function _initELV() {
		$('table.payone_elv_data select[name="p1_elv_country"]').on('change', function (e) {
			var selected_iso_2 = $(this).val();
			var only_de_rows = $('tr.only_de', $(this).closest('table'));
			if (selected_iso_2 == 'DE') {
				only_de_rows.show('fast');
			} else {
				only_de_rows.hide('fast');
			}
		});
		$('table.payone_elv_data select[name="p1_elv_country"]').trigger('change');

		$('.sepadata input').on('change', function (e) {
			var sepadata = '';
			$('.sepadata input').each(function () {
				sepadata += $(this).val();
			});
			if (sepadata.length === 0) {
				$('tr.only_de input').removeAttr('disabled');
			} else {
				$('tr.only_de input').attr('disabled', 'disabled');
			}
		});

		$('.only_de input').on('change', function (e) {
			var accountdata = '';
			$('.only_de input').each(function () {
				accountdata += $(this).val();
			});
			if (accountdata.length === 0) {
				$('tr.sepadata input').removeAttr('disabled');
			} else {
				$('tr.sepadata input').attr('disabled', 'disabled');
			}
		});

		var pg_callback_elv = function pg_callback_elv(response) {
			if (p1_debug) {
				console.log(response);
			}
			var current_block = $('div.module_option_checked');
			if (!response || (typeof response === 'undefined' ? 'undefined' : _typeof(response)) != 'object' || response.status != 'VALID') {
				// error occurred
				var errormessage = p1_payment_error;
				if (typeof response.customermessage == 'string') {
					errormessage = response.customermessage;
				}
				$('p.p1_error', current_block).html(errormessage);
				$('p.p1_error', current_block).show();
				current_block.closest('div.payment_item').removeClass('data_valid');
				current_block.get(0).scrollIntoView();
			} else {
				pg_callback_elv_none();
				$('form#checkout_payment').trigger('submit');
			}
		};

		var pg_callback_elv_none = function pg_callback_elv_none() {
			var $checked_payment = $('input[name="payment"]:checked');
			$('p.p1_error', $checked_payment.closest('div.payment_item')).hide();
			$('table.payone_elv_data').hide();
			$('div.p1_finaldata_elv').show();
			$('td.final_elv_country').html($('select#p1_elv_country option').filter(':selected').html());
			$('td.final_elv_accountnumber').html($('input#p1_elv_accountnumber').val());
			$('td.final_elv_bankcode').html($('input#p1_elv_bankcode').val());
			$('td.final_elv_iban').html($('input#p1_elv_iban').val());
			$('td.final_elv_bic').html($('input#p1_elv_bic').val());
			$checked_payment.closest('div.payment_item').addClass('data_valid');
			$('table.payone_elv_data').removeClass('payone_paydata');
		};

		var payone_elv_checkdata = function payone_elv_checkdata(e) {
			var input_bankcountry = $('select[name="p1_elv_country"] option').filter(':selected').val();
			var input_accountnumber = $('input[name="p1_elv_accountnumber"]', $this).val();
			var input_bankcode = $('input[name="p1_elv_bankcode"]', $this).val();
			var input_iban = $('input[name="p1_elv_iban"]', $this).val();
			var input_bic = $('input[name="p1_elv_bic"]', $this).val();

			if (p1_elv_checkmode == 'none') {
				pg_callback_elv_none();
			} else {
				e.preventDefault(); // prevent submit
				var pg_config = p1_elv_config;
				var pg = new PAYONE.Gateway(pg_config, pg_callback_elv);
				var data = {};
				if (input_iban.length > 0) {
					data = {
						iban: input_iban,
						bic: input_bic,
						bankcountry: input_bankcountry
					};
				} else {
					data = {
						bankaccount: input_accountnumber,
						bankcode: input_bankcode,
						bankcountry: input_bankcountry
					};
				}

				if (p1_debug) {
					console.log(data);
				}
				pg.call(data);
			}
		};

		$('form#checkout_payment').on('submit', function (e) {
			var $checked_payment = $('input[name="payment"]:checked');
			if ($checked_payment.val() === 'payone_elv') {
				if ($checked_payment.closest('div.payment_item').hasClass('data_valid') === false) {
					payone_elv_checkdata(e);
				}
			}
		});
	};

	var _initSafeInv = function _initSafeInv() {
		var _safeInvDisplayAgreement = function _safeInvDisplayAgreement() {
			var safeInvType = $('#p1_safeinv_type').val();
			$('tr.p1-safeinv-agreement').not('.p1-show-for-' + safeInvType).hide();
			$('tr.p1-show-for-' + safeInvType).show();
		};
		$('select[name="safeinv_type"]').on('change', _safeInvDisplayAgreement);
		_safeInvDisplayAgreement();
	};

	// ########## INITIALIZATION ##########

	/**
  * Initialize Module
  * @constructor
  */
	module.init = function (done) {
		if (p1_debug) {
			console.log('payone_checkout module initializing, submodule ' + options.module);
		}
		if (options.module == 'cc') {
			$('form#checkout_payment').on('submit', _p1_payment_submit_handler);
		}
		if (options.module == 'otrans') {
			_initOnlineTransfer();
		}
		if (options.module == 'elv') {
			_initELV();
		}
		if (options.module == 'safeinv') {
			_initSafeInv();
		}
		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpZGdldHMvcGF5b25lX2NoZWNrb3V0LmpzIl0sIm5hbWVzIjpbImdhbWJpbyIsIndpZGdldHMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwicDFfZGVidWciLCJfcDFfcGF5bWVudF9zdWJtaXRfaGFuZGxlciIsImUiLCJzZWxlY3RlZF9wYXltZW50IiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwidmFsdWUiLCJjb25zb2xlIiwibG9nIiwicHJldmVudERlZmF1bHQiLCJwMWNjX2NoZWNrIiwiX2luaXRPbmxpbmVUcmFuc2ZlciIsIm9uIiwic2VsZWN0ZWRfdHlwZSIsInZhbCIsIiRwZF90YWJsZSIsImNsb3Nlc3QiLCIkZGF0YXJvd3MiLCJoaWRlIiwic2hvdyIsImFkZENsYXNzIiwiY2xpY2siLCJ0cmlnZ2VyIiwib3RyYW5zX2lucHV0X2hhbmRsZXIiLCJhbnlfZW1wdHkiLCJlYWNoIiwicmVtb3ZlQ2xhc3MiLCJrZXl1cCIsImNoYW5nZSIsIl9pbml0RUxWIiwic2VsZWN0ZWRfaXNvXzIiLCJvbmx5X2RlX3Jvd3MiLCJzZXBhZGF0YSIsImxlbmd0aCIsInJlbW92ZUF0dHIiLCJhdHRyIiwiYWNjb3VudGRhdGEiLCJwZ19jYWxsYmFja19lbHYiLCJyZXNwb25zZSIsImN1cnJlbnRfYmxvY2siLCJzdGF0dXMiLCJlcnJvcm1lc3NhZ2UiLCJwMV9wYXltZW50X2Vycm9yIiwiY3VzdG9tZXJtZXNzYWdlIiwiaHRtbCIsImdldCIsInNjcm9sbEludG9WaWV3IiwicGdfY2FsbGJhY2tfZWx2X25vbmUiLCIkY2hlY2tlZF9wYXltZW50IiwiZmlsdGVyIiwicGF5b25lX2Vsdl9jaGVja2RhdGEiLCJpbnB1dF9iYW5rY291bnRyeSIsImlucHV0X2FjY291bnRudW1iZXIiLCJpbnB1dF9iYW5rY29kZSIsImlucHV0X2liYW4iLCJpbnB1dF9iaWMiLCJwMV9lbHZfY2hlY2ttb2RlIiwicGdfY29uZmlnIiwicDFfZWx2X2NvbmZpZyIsInBnIiwiUEFZT05FIiwiR2F0ZXdheSIsImliYW4iLCJiaWMiLCJiYW5rY291bnRyeSIsImJhbmthY2NvdW50IiwiYmFua2NvZGUiLCJjYWxsIiwiaGFzQ2xhc3MiLCJfaW5pdFNhZmVJbnYiLCJfc2FmZUludkRpc3BsYXlBZ3JlZW1lbnQiLCJzYWZlSW52VHlwZSIsIm5vdCIsImluaXQiLCJkb25lIl0sIm1hcHBpbmdzIjoiOzs7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7QUFLQUEsT0FBT0MsT0FBUCxDQUFlQyxNQUFmLENBQ0MsaUJBREQsRUFHQyxFQUhELEVBS0MsVUFBU0MsSUFBVCxFQUFlOztBQUVkOztBQUVBOztBQUVBLEtBQUlDLFFBQVFDLEVBQUUsSUFBRixDQUFaO0FBQUEsS0FDQ0MsV0FBVyxFQURaO0FBQUEsS0FFQ0MsVUFBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FGWDtBQUFBLEtBR0NNLFdBQVcsSUFIWjtBQUFBLEtBSUNQLFNBQVMsRUFKVjs7QUFNQTs7QUFFQSxLQUFJUSw2QkFBNkIsU0FBN0JBLDBCQUE2QixDQUFVQyxDQUFWLEVBQWE7QUFDN0MsTUFBSUMsbUJBQW1CQyxTQUFTQyxhQUFULENBQXVCLGlEQUF2QixFQUEwRUMsS0FBakc7QUFDQSxNQUFHSCxxQkFBcUIsV0FBeEIsRUFDQTtBQUNDLE9BQUdILFFBQUgsRUFBYTtBQUFFTyxZQUFRQyxHQUFSLENBQVksMkJBQVo7QUFBMkM7QUFDMUROLEtBQUVPLGNBQUY7QUFDQUM7QUFDQTtBQUNELEVBUkQ7O0FBVUEsS0FBSUMsc0JBQXNCLFNBQXRCQSxtQkFBc0IsR0FDMUI7QUFDQ2YsSUFBRSxvQkFBRixFQUF3QmdCLEVBQXhCLENBQTJCLFFBQTNCLEVBQXFDLFVBQVNWLENBQVQsRUFBWTtBQUNoRCxPQUFJVyxnQkFBZ0JqQixFQUFFLElBQUYsRUFBUWtCLEdBQVIsRUFBcEI7QUFDQSxPQUFJQyxZQUFZbkIsRUFBRSxJQUFGLEVBQVFvQixPQUFSLENBQWdCLDBCQUFoQixDQUFoQjtBQUNBLE9BQUlDLFlBQVlyQixFQUFFLFlBQUYsRUFBZ0JtQixTQUFoQixDQUFoQjtBQUNBRSxhQUFVQyxJQUFWO0FBQ0F0QixLQUFFLFVBQVFpQixhQUFWLEVBQXlCTSxJQUF6QjtBQUNBLE9BQUdOLGlCQUFpQixZQUFqQixJQUFpQ0EsaUJBQWlCLFFBQXJELEVBQ0E7QUFDQ2pCLE1BQUUsSUFBRixFQUFRb0IsT0FBUixDQUFnQixrQkFBaEIsRUFBb0NJLFFBQXBDLENBQTZDLFlBQTdDO0FBQ0F4QixNQUFFLElBQUYsRUFBUW9CLE9BQVIsQ0FBZ0Isa0JBQWhCLEVBQW9DSyxLQUFwQztBQUNBO0FBQ0QsR0FYRDtBQVlBekIsSUFBRSxvQkFBRixFQUF3QjBCLE9BQXhCLENBQWdDLFFBQWhDOztBQUVBLE1BQUlDLHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQVNyQixDQUFULEVBQVk7QUFDdEMsT0FBSXNCLFlBQVksS0FBaEI7QUFDQTVCLEtBQUUsZ0RBQUYsRUFBb0Q2QixJQUFwRCxDQUF5RCxZQUFXO0FBQ25FLFFBQUc3QixFQUFFLElBQUYsRUFBUWtCLEdBQVIsT0FBa0IsRUFBckIsRUFBeUI7QUFBRVUsaUJBQVksSUFBWjtBQUFtQjtBQUM5QyxJQUZEO0FBR0EsT0FBR0EsY0FBYyxJQUFqQixFQUF1QjtBQUN0QjVCLE1BQUUsMEJBQUYsRUFBOEJ3QixRQUE5QixDQUF1QyxxQkFBdkM7QUFDQSxJQUZELE1BR0s7QUFDSnhCLE1BQUUsMEJBQUYsRUFBOEI4QixXQUE5QixDQUEwQyxxQkFBMUM7QUFDQTtBQUNEOUIsS0FBRSxJQUFGLEVBQVFvQixPQUFSLENBQWdCLGtCQUFoQixFQUFvQ1UsV0FBcEMsQ0FBZ0QsWUFBaEQ7QUFDQSxHQVpEOztBQWNBOUIsSUFBRSx3Q0FBRixFQUE0QytCLEtBQTVDLENBQWtESixvQkFBbEQ7QUFDQTNCLElBQUUsd0NBQUYsRUFBNENnQyxNQUE1QyxDQUFtREwsb0JBQW5EO0FBQ0EsRUFoQ0Q7O0FBa0NBLEtBQUlNLFdBQVcsU0FBWEEsUUFBVyxHQUNmO0FBQ0NqQyxJQUFFLHFEQUFGLEVBQXlEZ0IsRUFBekQsQ0FBNEQsUUFBNUQsRUFBc0UsVUFBU1YsQ0FBVCxFQUFZO0FBQ2pGLE9BQUk0QixpQkFBaUJsQyxFQUFFLElBQUYsRUFBUWtCLEdBQVIsRUFBckI7QUFDQSxPQUFJaUIsZUFBZW5DLEVBQUUsWUFBRixFQUFnQkEsRUFBRSxJQUFGLEVBQVFvQixPQUFSLENBQWdCLE9BQWhCLENBQWhCLENBQW5CO0FBQ0EsT0FBR2Msa0JBQWtCLElBQXJCLEVBQTJCO0FBQzFCQyxpQkFBYVosSUFBYixDQUFrQixNQUFsQjtBQUNBLElBRkQsTUFHSztBQUNKWSxpQkFBYWIsSUFBYixDQUFrQixNQUFsQjtBQUNBO0FBQ0QsR0FURDtBQVVBdEIsSUFBRSxxREFBRixFQUF5RDBCLE9BQXpELENBQWlFLFFBQWpFOztBQUVBMUIsSUFBRSxpQkFBRixFQUFxQmdCLEVBQXJCLENBQXdCLFFBQXhCLEVBQWtDLFVBQVNWLENBQVQsRUFBWTtBQUM3QyxPQUFJOEIsV0FBVyxFQUFmO0FBQ0FwQyxLQUFFLGlCQUFGLEVBQXFCNkIsSUFBckIsQ0FBMEIsWUFBVztBQUFFTyxnQkFBWXBDLEVBQUUsSUFBRixFQUFRa0IsR0FBUixFQUFaO0FBQTRCLElBQW5FO0FBQ0EsT0FBR2tCLFNBQVNDLE1BQVQsS0FBb0IsQ0FBdkIsRUFDQTtBQUNDckMsTUFBRSxrQkFBRixFQUFzQnNDLFVBQXRCLENBQWlDLFVBQWpDO0FBQ0EsSUFIRCxNQUtBO0FBQ0N0QyxNQUFFLGtCQUFGLEVBQXNCdUMsSUFBdEIsQ0FBMkIsVUFBM0IsRUFBdUMsVUFBdkM7QUFDQTtBQUNELEdBWEQ7O0FBYUF2QyxJQUFFLGdCQUFGLEVBQW9CZ0IsRUFBcEIsQ0FBdUIsUUFBdkIsRUFBaUMsVUFBU1YsQ0FBVCxFQUFZO0FBQzVDLE9BQUlrQyxjQUFjLEVBQWxCO0FBQ0F4QyxLQUFFLGdCQUFGLEVBQW9CNkIsSUFBcEIsQ0FBeUIsWUFBVztBQUFFVyxtQkFBZXhDLEVBQUUsSUFBRixFQUFRa0IsR0FBUixFQUFmO0FBQStCLElBQXJFO0FBQ0EsT0FBR3NCLFlBQVlILE1BQVosS0FBdUIsQ0FBMUIsRUFDQTtBQUNDckMsTUFBRSxtQkFBRixFQUF1QnNDLFVBQXZCLENBQWtDLFVBQWxDO0FBQ0EsSUFIRCxNQUtBO0FBQ0N0QyxNQUFFLG1CQUFGLEVBQXVCdUMsSUFBdkIsQ0FBNEIsVUFBNUIsRUFBd0MsVUFBeEM7QUFDQTtBQUNELEdBWEQ7O0FBYUEsTUFBSUUsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFTQyxRQUFULEVBQW1CO0FBQ3hDLE9BQUd0QyxRQUFILEVBQWE7QUFBRU8sWUFBUUMsR0FBUixDQUFZOEIsUUFBWjtBQUF3QjtBQUN2QyxPQUFJQyxnQkFBZ0IzQyxFQUFFLDJCQUFGLENBQXBCO0FBQ0EsT0FBRyxDQUFDMEMsUUFBRCxJQUFhLFFBQU9BLFFBQVAseUNBQU9BLFFBQVAsTUFBbUIsUUFBaEMsSUFBNENBLFNBQVNFLE1BQVQsSUFBbUIsT0FBbEUsRUFBMkU7QUFDMUU7QUFDQSxRQUFJQyxlQUFlQyxnQkFBbkI7QUFDQSxRQUFHLE9BQU9KLFNBQVNLLGVBQWhCLElBQW1DLFFBQXRDLEVBQWdEO0FBQy9DRixvQkFBZUgsU0FBU0ssZUFBeEI7QUFDQTtBQUNEL0MsTUFBRSxZQUFGLEVBQWdCMkMsYUFBaEIsRUFBK0JLLElBQS9CLENBQW9DSCxZQUFwQztBQUNBN0MsTUFBRSxZQUFGLEVBQWdCMkMsYUFBaEIsRUFBK0JwQixJQUEvQjtBQUNBb0Isa0JBQWN2QixPQUFkLENBQXNCLGtCQUF0QixFQUEwQ1UsV0FBMUMsQ0FBc0QsWUFBdEQ7QUFDQWEsa0JBQWNNLEdBQWQsQ0FBa0IsQ0FBbEIsRUFBcUJDLGNBQXJCO0FBQ0EsSUFWRCxNQVdLO0FBQ0pDO0FBQ0FuRCxNQUFFLHVCQUFGLEVBQTJCMEIsT0FBM0IsQ0FBbUMsUUFBbkM7QUFDQTtBQUNELEdBbEJEOztBQW9CQSxNQUFJeUIsdUJBQXVCLFNBQXZCQSxvQkFBdUIsR0FBVztBQUNyQyxPQUFJQyxtQkFBbUJwRCxFQUFFLCtCQUFGLENBQXZCO0FBQ0FBLEtBQUUsWUFBRixFQUFnQm9ELGlCQUFpQmhDLE9BQWpCLENBQXlCLGtCQUF6QixDQUFoQixFQUE4REUsSUFBOUQ7QUFDQXRCLEtBQUUsdUJBQUYsRUFBMkJzQixJQUEzQjtBQUNBdEIsS0FBRSxzQkFBRixFQUEwQnVCLElBQTFCO0FBQ0F2QixLQUFFLHNCQUFGLEVBQTBCZ0QsSUFBMUIsQ0FBK0JoRCxFQUFFLDhCQUFGLEVBQWtDcUQsTUFBbEMsQ0FBeUMsV0FBekMsRUFBc0RMLElBQXRELEVBQS9CO0FBQ0FoRCxLQUFFLDRCQUFGLEVBQWdDZ0QsSUFBaEMsQ0FBcUNoRCxFQUFFLDRCQUFGLEVBQWdDa0IsR0FBaEMsRUFBckM7QUFDQWxCLEtBQUUsdUJBQUYsRUFBMkJnRCxJQUEzQixDQUFnQ2hELEVBQUUsdUJBQUYsRUFBMkJrQixHQUEzQixFQUFoQztBQUNBbEIsS0FBRSxtQkFBRixFQUF1QmdELElBQXZCLENBQTRCaEQsRUFBRSxtQkFBRixFQUF1QmtCLEdBQXZCLEVBQTVCO0FBQ0FsQixLQUFFLGtCQUFGLEVBQXNCZ0QsSUFBdEIsQ0FBMkJoRCxFQUFFLGtCQUFGLEVBQXNCa0IsR0FBdEIsRUFBM0I7QUFDQWtDLG9CQUFpQmhDLE9BQWpCLENBQXlCLGtCQUF6QixFQUE2Q0ksUUFBN0MsQ0FBc0QsWUFBdEQ7QUFDQXhCLEtBQUUsdUJBQUYsRUFBMkI4QixXQUEzQixDQUF1QyxnQkFBdkM7QUFDQSxHQVpEOztBQWNBLE1BQUl3Qix1QkFBdUIsU0FBdkJBLG9CQUF1QixDQUFTaEQsQ0FBVCxFQUFZO0FBQ3RDLE9BQUlpRCxvQkFBb0J2RCxFQUFFLHNDQUFGLEVBQTBDcUQsTUFBMUMsQ0FBaUQsV0FBakQsRUFBOERuQyxHQUE5RCxFQUF4QjtBQUNBLE9BQUlzQyxzQkFBc0J4RCxFQUFFLG9DQUFGLEVBQXdDRCxLQUF4QyxFQUErQ21CLEdBQS9DLEVBQTFCO0FBQ0EsT0FBSXVDLGlCQUFpQnpELEVBQUUsK0JBQUYsRUFBbUNELEtBQW5DLEVBQTBDbUIsR0FBMUMsRUFBckI7QUFDQSxPQUFJd0MsYUFBYTFELEVBQUUsMkJBQUYsRUFBK0JELEtBQS9CLEVBQXNDbUIsR0FBdEMsRUFBakI7QUFDQSxPQUFJeUMsWUFBWTNELEVBQUUsMEJBQUYsRUFBOEJELEtBQTlCLEVBQXFDbUIsR0FBckMsRUFBaEI7O0FBR0EsT0FBRzBDLG9CQUFvQixNQUF2QixFQUNBO0FBQ0NUO0FBQ0EsSUFIRCxNQUtBO0FBQ0M3QyxNQUFFTyxjQUFGLEdBREQsQ0FDcUI7QUFDcEIsUUFBSWdELFlBQVlDLGFBQWhCO0FBQ0EsUUFBSUMsS0FBSyxJQUFJQyxPQUFPQyxPQUFYLENBQW1CSixTQUFuQixFQUE4QnBCLGVBQTlCLENBQVQ7QUFDQSxRQUFJM0MsT0FBTyxFQUFYO0FBQ0EsUUFBRzRELFdBQVdyQixNQUFYLEdBQW9CLENBQXZCLEVBQTBCO0FBQ3pCdkMsWUFBTztBQUNOb0UsWUFBTVIsVUFEQTtBQUVOUyxXQUFLUixTQUZDO0FBR05TLG1CQUFhYjtBQUhQLE1BQVA7QUFLQSxLQU5ELE1BT0s7QUFDSnpELFlBQU87QUFDTnVFLG1CQUFhYixtQkFEUDtBQUVOYyxnQkFBVWIsY0FGSjtBQUdOVyxtQkFBYWI7QUFIUCxNQUFQO0FBS0E7O0FBRUQsUUFBR25ELFFBQUgsRUFBYTtBQUFFTyxhQUFRQyxHQUFSLENBQVlkLElBQVo7QUFBb0I7QUFDbkNpRSxPQUFHUSxJQUFILENBQVF6RSxJQUFSO0FBQ0E7QUFDRCxHQXBDRDs7QUFzQ0FFLElBQUUsdUJBQUYsRUFBMkJnQixFQUEzQixDQUE4QixRQUE5QixFQUF3QyxVQUFTVixDQUFULEVBQVk7QUFDbkQsT0FBSThDLG1CQUFtQnBELEVBQUUsK0JBQUYsQ0FBdkI7QUFDQSxPQUFHb0QsaUJBQWlCbEMsR0FBakIsT0FBMkIsWUFBOUIsRUFDQTtBQUNDLFFBQUdrQyxpQkFBaUJoQyxPQUFqQixDQUF5QixrQkFBekIsRUFBNkNvRCxRQUE3QyxDQUFzRCxZQUF0RCxNQUF3RSxLQUEzRSxFQUNBO0FBQ0NsQiwwQkFBcUJoRCxDQUFyQjtBQUNBO0FBQ0Q7QUFDRCxHQVREO0FBVUEsRUExSEQ7O0FBNEhBLEtBQUltRSxlQUFlLFNBQWZBLFlBQWUsR0FDbkI7QUFDQyxNQUFJQywyQkFBMkIsU0FBM0JBLHdCQUEyQixHQUMvQjtBQUNDLE9BQUlDLGNBQWMzRSxFQUFFLGtCQUFGLEVBQXNCa0IsR0FBdEIsRUFBbEI7QUFDQWxCLEtBQUUseUJBQUYsRUFBNkI0RSxHQUE3QixDQUFpQyxrQkFBa0JELFdBQW5ELEVBQWdFckQsSUFBaEU7QUFDQXRCLEtBQUUsb0JBQW9CMkUsV0FBdEIsRUFBbUNwRCxJQUFuQztBQUNBLEdBTEQ7QUFNQXZCLElBQUUsNkJBQUYsRUFBaUNnQixFQUFqQyxDQUFvQyxRQUFwQyxFQUE4QzBELHdCQUE5QztBQUNBQTtBQUNBLEVBVkQ7O0FBWUE7O0FBRUE7Ozs7QUFJQTdFLFFBQU9nRixJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCLE1BQUcxRSxRQUFILEVBQWE7QUFBRU8sV0FBUUMsR0FBUixDQUFZLG9EQUFvRFYsUUFBUUwsTUFBeEU7QUFBa0Y7QUFDakcsTUFBR0ssUUFBUUwsTUFBUixJQUFrQixJQUFyQixFQUNBO0FBQ0NHLEtBQUUsdUJBQUYsRUFBMkJnQixFQUEzQixDQUE4QixRQUE5QixFQUF3Q1gsMEJBQXhDO0FBQ0E7QUFDRCxNQUFHSCxRQUFRTCxNQUFSLElBQWtCLFFBQXJCLEVBQ0E7QUFDQ2tCO0FBQ0E7QUFDRCxNQUFHYixRQUFRTCxNQUFSLElBQWtCLEtBQXJCLEVBQ0E7QUFDQ29DO0FBQ0E7QUFDRCxNQUFHL0IsUUFBUUwsTUFBUixJQUFrQixTQUFyQixFQUNBO0FBQ0M0RTtBQUNBO0FBQ0RLO0FBQ0EsRUFuQkQ7O0FBcUJBLFFBQU9qRixNQUFQO0FBQ0EsQ0FuT0YiLCJmaWxlIjoid2lkZ2V0cy9wYXlvbmVfY2hlY2tvdXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRwYXlvbmVfY2hlY2tvdXQuanMgMjAxOC0xMi0wNVxuXHRHYW1iaW8gR21iSFxuXHRodHRwOi8vd3d3LmdhbWJpby5kZVxuXHRDb3B5cmlnaHQgKGMpIDIwMTUgR2FtYmlvIEdtYkhcblx0UmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG5cdFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuXHQtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuKi9cblxuLyoqXG4gKiBQYXlvbmUgQ2hlY2tvdXRcbiAqXG4gKiBAbW9kdWxlIFdpZGdldHMvcGF5b25lX2NoZWNrb3V0XG4gKi9cbmdhbWJpby53aWRnZXRzLm1vZHVsZShcblx0J3BheW9uZV9jaGVja291dCcsXG5cblx0W10sXG5cblx0ZnVuY3Rpb24oZGF0YSkge1xuXG5cdFx0J3VzZSBzdHJpY3QnO1xuXG5cdFx0Ly8gIyMjIyMjIyMjIyBWQVJJQUJMRSBJTklUSUFMSVpBVElPTiAjIyMjIyMjIyMjXG5cblx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0ZGVmYXVsdHMgPSB7fSxcblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0cDFfZGVidWcgPSB0cnVlLFxuXHRcdFx0bW9kdWxlID0ge307XG5cblx0XHQvLyAjIyMjIyMjIyMjIFBBWU9ORSBGVU5DVElPTlMgIyMjIyMjIyMjI1xuXG5cdFx0dmFyIF9wMV9wYXltZW50X3N1Ym1pdF9oYW5kbGVyID0gZnVuY3Rpb24gKGUpIHtcblx0XHRcdHZhciBzZWxlY3RlZF9wYXltZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2NoZWNrb3V0X3BheW1lbnQgaW5wdXRbbmFtZT1cInBheW1lbnRcIl06Y2hlY2tlZCcpLnZhbHVlO1xuXHRcdFx0aWYoc2VsZWN0ZWRfcGF5bWVudCA9PT0gJ3BheW9uZV9jYycpXG5cdFx0XHR7XG5cdFx0XHRcdGlmKHAxX2RlYnVnKSB7IGNvbnNvbGUubG9nKCdwYXlvbmUgY2MgY2hlY2sgdHJpZ2dlcmVkJyk7IH1cblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRwMWNjX2NoZWNrKCk7XG5cdFx0XHR9XG5cdFx0fTtcblxuXHRcdHZhciBfaW5pdE9ubGluZVRyYW5zZmVyID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdCQoJ3NlbGVjdCNvdHJhbnNfdHlwZScpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdHZhciBzZWxlY3RlZF90eXBlID0gJCh0aGlzKS52YWwoKTtcblx0XHRcdFx0dmFyICRwZF90YWJsZSA9ICQodGhpcykuY2xvc2VzdCgndGFibGUucGF5b25lX290cmFuc19kYXRhJyk7XG5cdFx0XHRcdHZhciAkZGF0YXJvd3MgPSAkKCd0ci5kYXRhcm93JywgJHBkX3RhYmxlKTtcblx0XHRcdFx0JGRhdGFyb3dzLmhpZGUoKTtcblx0XHRcdFx0JCgnLmZvcl8nK3NlbGVjdGVkX3R5cGUpLnNob3coKTtcblx0XHRcdFx0aWYoc2VsZWN0ZWRfdHlwZSA9PSAncGZlZmluYW5jZScgfHwgc2VsZWN0ZWRfdHlwZSA9PSAncGZjYXJkJylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdCQodGhpcykuY2xvc2VzdCgnZGl2LnBheW1lbnRfaXRlbScpLmFkZENsYXNzKCdkYXRhX3ZhbGlkJyk7XG5cdFx0XHRcdFx0JCh0aGlzKS5jbG9zZXN0KCdkaXYucGF5bWVudF9pdGVtJykuY2xpY2soKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHQkKCdzZWxlY3Qjb3RyYW5zX3R5cGUnKS50cmlnZ2VyKCdjaGFuZ2UnKTtcblxuXHRcdFx0dmFyIG90cmFuc19pbnB1dF9oYW5kbGVyID0gZnVuY3Rpb24oZSkge1xuXHRcdFx0XHR2YXIgYW55X2VtcHR5ID0gZmFsc2U7XG5cdFx0XHRcdCQoJy5wYXlvbmVfb3RyYW5zX2RhdGEgaW5wdXRbdHlwZT1cInRleHRcIl06dmlzaWJsZScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0aWYoJCh0aGlzKS52YWwoKSA9PT0gJycpIHsgYW55X2VtcHR5ID0gdHJ1ZTtcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHRcdGlmKGFueV9lbXB0eSA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdCQoJ3RhYmxlLnBheW9uZV9vdHJhbnNfZGF0YScpLmFkZENsYXNzKCdwYXlvbmVfZGF0YV9taXNzaW5nJyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0JCgndGFibGUucGF5b25lX290cmFuc19kYXRhJykucmVtb3ZlQ2xhc3MoJ3BheW9uZV9kYXRhX21pc3NpbmcnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHQkKHRoaXMpLmNsb3Nlc3QoJ2Rpdi5wYXltZW50X2l0ZW0nKS5yZW1vdmVDbGFzcygnZGF0YV92YWxpZCcpO1xuXHRcdFx0fTtcblxuXHRcdFx0JCgnLnBheW9uZV9vdHJhbnNfZGF0YSBpbnB1dFt0eXBlPVwidGV4dFwiXScpLmtleXVwKG90cmFuc19pbnB1dF9oYW5kbGVyKTtcblx0XHRcdCQoJy5wYXlvbmVfb3RyYW5zX2RhdGEgaW5wdXRbdHlwZT1cInRleHRcIl0nKS5jaGFuZ2Uob3RyYW5zX2lucHV0X2hhbmRsZXIpO1xuXHRcdH07XG5cblx0XHR2YXIgX2luaXRFTFYgPSBmdW5jdGlvbigpXG5cdFx0e1xuXHRcdFx0JCgndGFibGUucGF5b25lX2Vsdl9kYXRhIHNlbGVjdFtuYW1lPVwicDFfZWx2X2NvdW50cnlcIl0nKS5vbignY2hhbmdlJywgZnVuY3Rpb24oZSkge1xuXHRcdFx0XHR2YXIgc2VsZWN0ZWRfaXNvXzIgPSAkKHRoaXMpLnZhbCgpO1xuXHRcdFx0XHR2YXIgb25seV9kZV9yb3dzID0gJCgndHIub25seV9kZScsICQodGhpcykuY2xvc2VzdCgndGFibGUnKSk7XG5cdFx0XHRcdGlmKHNlbGVjdGVkX2lzb18yID09ICdERScpIHtcblx0XHRcdFx0XHRvbmx5X2RlX3Jvd3Muc2hvdygnZmFzdCcpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Uge1xuXHRcdFx0XHRcdG9ubHlfZGVfcm93cy5oaWRlKCdmYXN0Jyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0JCgndGFibGUucGF5b25lX2Vsdl9kYXRhIHNlbGVjdFtuYW1lPVwicDFfZWx2X2NvdW50cnlcIl0nKS50cmlnZ2VyKCdjaGFuZ2UnKTtcblxuXHRcdFx0JCgnLnNlcGFkYXRhIGlucHV0Jykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0dmFyIHNlcGFkYXRhID0gJyc7XG5cdFx0XHRcdCQoJy5zZXBhZGF0YSBpbnB1dCcpLmVhY2goZnVuY3Rpb24oKSB7IHNlcGFkYXRhICs9ICQodGhpcykudmFsKCk7IH0pO1xuXHRcdFx0XHRpZihzZXBhZGF0YS5sZW5ndGggPT09IDApXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQkKCd0ci5vbmx5X2RlIGlucHV0JykucmVtb3ZlQXR0cignZGlzYWJsZWQnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQkKCd0ci5vbmx5X2RlIGlucHV0JykuYXR0cignZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cblx0XHRcdCQoJy5vbmx5X2RlIGlucHV0Jykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0dmFyIGFjY291bnRkYXRhID0gJyc7XG5cdFx0XHRcdCQoJy5vbmx5X2RlIGlucHV0JykuZWFjaChmdW5jdGlvbigpIHsgYWNjb3VudGRhdGEgKz0gJCh0aGlzKS52YWwoKTsgfSk7XG5cdFx0XHRcdGlmKGFjY291bnRkYXRhLmxlbmd0aCA9PT0gMClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdCQoJ3RyLnNlcGFkYXRhIGlucHV0JykucmVtb3ZlQXR0cignZGlzYWJsZWQnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQkKCd0ci5zZXBhZGF0YSBpbnB1dCcpLmF0dHIoJ2Rpc2FibGVkJywgJ2Rpc2FibGVkJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXG5cdFx0XHR2YXIgcGdfY2FsbGJhY2tfZWx2ID0gZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0aWYocDFfZGVidWcpIHsgY29uc29sZS5sb2cocmVzcG9uc2UpOyB9XG5cdFx0XHRcdHZhciBjdXJyZW50X2Jsb2NrID0gJCgnZGl2Lm1vZHVsZV9vcHRpb25fY2hlY2tlZCcpO1xuXHRcdFx0XHRpZighcmVzcG9uc2UgfHwgdHlwZW9mIHJlc3BvbnNlICE9ICdvYmplY3QnIHx8IHJlc3BvbnNlLnN0YXR1cyAhPSAnVkFMSUQnKSB7XG5cdFx0XHRcdFx0Ly8gZXJyb3Igb2NjdXJyZWRcblx0XHRcdFx0XHR2YXIgZXJyb3JtZXNzYWdlID0gcDFfcGF5bWVudF9lcnJvcjtcblx0XHRcdFx0XHRpZih0eXBlb2YgcmVzcG9uc2UuY3VzdG9tZXJtZXNzYWdlID09ICdzdHJpbmcnKSB7XG5cdFx0XHRcdFx0XHRlcnJvcm1lc3NhZ2UgPSByZXNwb25zZS5jdXN0b21lcm1lc3NhZ2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdCQoJ3AucDFfZXJyb3InLCBjdXJyZW50X2Jsb2NrKS5odG1sKGVycm9ybWVzc2FnZSk7XG5cdFx0XHRcdFx0JCgncC5wMV9lcnJvcicsIGN1cnJlbnRfYmxvY2spLnNob3coKTtcblx0XHRcdFx0XHRjdXJyZW50X2Jsb2NrLmNsb3Nlc3QoJ2Rpdi5wYXltZW50X2l0ZW0nKS5yZW1vdmVDbGFzcygnZGF0YV92YWxpZCcpO1xuXHRcdFx0XHRcdGN1cnJlbnRfYmxvY2suZ2V0KDApLnNjcm9sbEludG9WaWV3KCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0cGdfY2FsbGJhY2tfZWx2X25vbmUoKTtcblx0XHRcdFx0XHQkKCdmb3JtI2NoZWNrb3V0X3BheW1lbnQnKS50cmlnZ2VyKCdzdWJtaXQnKTtcblx0XHRcdFx0fVxuXHRcdFx0fTtcblxuXHRcdFx0dmFyIHBnX2NhbGxiYWNrX2Vsdl9ub25lID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHZhciAkY2hlY2tlZF9wYXltZW50ID0gJCgnaW5wdXRbbmFtZT1cInBheW1lbnRcIl06Y2hlY2tlZCcpO1xuXHRcdFx0XHQkKCdwLnAxX2Vycm9yJywgJGNoZWNrZWRfcGF5bWVudC5jbG9zZXN0KCdkaXYucGF5bWVudF9pdGVtJykpLmhpZGUoKTtcblx0XHRcdFx0JCgndGFibGUucGF5b25lX2Vsdl9kYXRhJykuaGlkZSgpO1xuXHRcdFx0XHQkKCdkaXYucDFfZmluYWxkYXRhX2VsdicpLnNob3coKTtcblx0XHRcdFx0JCgndGQuZmluYWxfZWx2X2NvdW50cnknKS5odG1sKCQoJ3NlbGVjdCNwMV9lbHZfY291bnRyeSBvcHRpb24nKS5maWx0ZXIoJzpzZWxlY3RlZCcpLmh0bWwoKSk7XG5cdFx0XHRcdCQoJ3RkLmZpbmFsX2Vsdl9hY2NvdW50bnVtYmVyJykuaHRtbCgkKCdpbnB1dCNwMV9lbHZfYWNjb3VudG51bWJlcicpLnZhbCgpKTtcblx0XHRcdFx0JCgndGQuZmluYWxfZWx2X2Jhbmtjb2RlJykuaHRtbCgkKCdpbnB1dCNwMV9lbHZfYmFua2NvZGUnKS52YWwoKSk7XG5cdFx0XHRcdCQoJ3RkLmZpbmFsX2Vsdl9pYmFuJykuaHRtbCgkKCdpbnB1dCNwMV9lbHZfaWJhbicpLnZhbCgpKTtcblx0XHRcdFx0JCgndGQuZmluYWxfZWx2X2JpYycpLmh0bWwoJCgnaW5wdXQjcDFfZWx2X2JpYycpLnZhbCgpKTtcblx0XHRcdFx0JGNoZWNrZWRfcGF5bWVudC5jbG9zZXN0KCdkaXYucGF5bWVudF9pdGVtJykuYWRkQ2xhc3MoJ2RhdGFfdmFsaWQnKTtcblx0XHRcdFx0JCgndGFibGUucGF5b25lX2Vsdl9kYXRhJykucmVtb3ZlQ2xhc3MoJ3BheW9uZV9wYXlkYXRhJyk7XG5cdFx0XHR9O1xuXG5cdFx0XHR2YXIgcGF5b25lX2Vsdl9jaGVja2RhdGEgPSBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdHZhciBpbnB1dF9iYW5rY291bnRyeSA9ICQoJ3NlbGVjdFtuYW1lPVwicDFfZWx2X2NvdW50cnlcIl0gb3B0aW9uJykuZmlsdGVyKCc6c2VsZWN0ZWQnKS52YWwoKTtcblx0XHRcdFx0dmFyIGlucHV0X2FjY291bnRudW1iZXIgPSAkKCdpbnB1dFtuYW1lPVwicDFfZWx2X2FjY291bnRudW1iZXJcIl0nLCAkdGhpcykudmFsKCk7XG5cdFx0XHRcdHZhciBpbnB1dF9iYW5rY29kZSA9ICQoJ2lucHV0W25hbWU9XCJwMV9lbHZfYmFua2NvZGVcIl0nLCAkdGhpcykudmFsKCk7XG5cdFx0XHRcdHZhciBpbnB1dF9pYmFuID0gJCgnaW5wdXRbbmFtZT1cInAxX2Vsdl9pYmFuXCJdJywgJHRoaXMpLnZhbCgpO1xuXHRcdFx0XHR2YXIgaW5wdXRfYmljID0gJCgnaW5wdXRbbmFtZT1cInAxX2Vsdl9iaWNcIl0nLCAkdGhpcykudmFsKCk7XG5cblxuXHRcdFx0XHRpZihwMV9lbHZfY2hlY2ttb2RlID09ICdub25lJylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHBnX2NhbGxiYWNrX2Vsdl9ub25lKCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpOyAvLyBwcmV2ZW50IHN1Ym1pdFxuXHRcdFx0XHRcdHZhciBwZ19jb25maWcgPSBwMV9lbHZfY29uZmlnO1xuXHRcdFx0XHRcdHZhciBwZyA9IG5ldyBQQVlPTkUuR2F0ZXdheShwZ19jb25maWcsIHBnX2NhbGxiYWNrX2Vsdik7XG5cdFx0XHRcdFx0dmFyIGRhdGEgPSB7fTtcblx0XHRcdFx0XHRpZihpbnB1dF9pYmFuLmxlbmd0aCA+IDApIHtcblx0XHRcdFx0XHRcdGRhdGEgPSB7XG5cdFx0XHRcdFx0XHRcdGliYW46IGlucHV0X2liYW4sXG5cdFx0XHRcdFx0XHRcdGJpYzogaW5wdXRfYmljLFxuXHRcdFx0XHRcdFx0XHRiYW5rY291bnRyeTogaW5wdXRfYmFua2NvdW50cnksXG5cdFx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRcdGRhdGEgPSB7XG5cdFx0XHRcdFx0XHRcdGJhbmthY2NvdW50OiBpbnB1dF9hY2NvdW50bnVtYmVyLFxuXHRcdFx0XHRcdFx0XHRiYW5rY29kZTogaW5wdXRfYmFua2NvZGUsXG5cdFx0XHRcdFx0XHRcdGJhbmtjb3VudHJ5OiBpbnB1dF9iYW5rY291bnRyeSxcblx0XHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0aWYocDFfZGVidWcpIHsgY29uc29sZS5sb2coZGF0YSk7IH1cblx0XHRcdFx0XHRwZy5jYWxsKGRhdGEpO1xuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXG5cdFx0XHQkKCdmb3JtI2NoZWNrb3V0X3BheW1lbnQnKS5vbignc3VibWl0JywgZnVuY3Rpb24oZSkge1xuXHRcdFx0XHR2YXIgJGNoZWNrZWRfcGF5bWVudCA9ICQoJ2lucHV0W25hbWU9XCJwYXltZW50XCJdOmNoZWNrZWQnKTtcblx0XHRcdFx0aWYoJGNoZWNrZWRfcGF5bWVudC52YWwoKSA9PT0gJ3BheW9uZV9lbHYnKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYoJGNoZWNrZWRfcGF5bWVudC5jbG9zZXN0KCdkaXYucGF5bWVudF9pdGVtJykuaGFzQ2xhc3MoJ2RhdGFfdmFsaWQnKSA9PT0gZmFsc2UpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0cGF5b25lX2Vsdl9jaGVja2RhdGEoZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9O1xuXG5cdFx0dmFyIF9pbml0U2FmZUludiA9IGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgX3NhZmVJbnZEaXNwbGF5QWdyZWVtZW50ID0gZnVuY3Rpb24oKVxuXHRcdFx0e1xuXHRcdFx0XHR2YXIgc2FmZUludlR5cGUgPSAkKCcjcDFfc2FmZWludl90eXBlJykudmFsKCk7XG5cdFx0XHRcdCQoJ3RyLnAxLXNhZmVpbnYtYWdyZWVtZW50Jykubm90KCcucDEtc2hvdy1mb3ItJyArIHNhZmVJbnZUeXBlKS5oaWRlKCk7XG5cdFx0XHRcdCQoJ3RyLnAxLXNob3ctZm9yLScgKyBzYWZlSW52VHlwZSkuc2hvdygpO1xuXHRcdFx0fVxuXHRcdFx0JCgnc2VsZWN0W25hbWU9XCJzYWZlaW52X3R5cGVcIl0nKS5vbignY2hhbmdlJywgX3NhZmVJbnZEaXNwbGF5QWdyZWVtZW50KTtcblx0XHRcdF9zYWZlSW52RGlzcGxheUFncmVlbWVudCgpO1xuXHRcdH1cblxuXHRcdC8vICMjIyMjIyMjIyMgSU5JVElBTElaQVRJT04gIyMjIyMjIyMjI1xuXG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBNb2R1bGVcblx0XHQgKiBAY29uc3RydWN0b3Jcblx0XHQgKi9cblx0XHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKGRvbmUpIHtcblx0XHRcdGlmKHAxX2RlYnVnKSB7IGNvbnNvbGUubG9nKCdwYXlvbmVfY2hlY2tvdXQgbW9kdWxlIGluaXRpYWxpemluZywgc3VibW9kdWxlICcgKyBvcHRpb25zLm1vZHVsZSk7IH1cblx0XHRcdGlmKG9wdGlvbnMubW9kdWxlID09ICdjYycpXG5cdFx0XHR7XG5cdFx0XHRcdCQoJ2Zvcm0jY2hlY2tvdXRfcGF5bWVudCcpLm9uKCdzdWJtaXQnLCBfcDFfcGF5bWVudF9zdWJtaXRfaGFuZGxlcik7XG5cdFx0XHR9XG5cdFx0XHRpZihvcHRpb25zLm1vZHVsZSA9PSAnb3RyYW5zJylcblx0XHRcdHtcblx0XHRcdFx0X2luaXRPbmxpbmVUcmFuc2ZlcigpO1xuXHRcdFx0fVxuXHRcdFx0aWYob3B0aW9ucy5tb2R1bGUgPT0gJ2VsdicpXG5cdFx0XHR7XG5cdFx0XHRcdF9pbml0RUxWKCk7XG5cdFx0XHR9XG5cdFx0XHRpZihvcHRpb25zLm1vZHVsZSA9PSAnc2FmZWludicpXG5cdFx0XHR7XG5cdFx0XHRcdF9pbml0U2FmZUludigpO1xuXHRcdFx0fVxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9XG4pO1xuIl19
