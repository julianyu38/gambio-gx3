'use strict';

/* --------------------------------------------------------------
 amazon_checkout.js 2018-11-16
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2016 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/* globals OffAmazonPayments */

/**
 * Widget that performs all actions of the amazon paymend method
 * at the checkout process
 */
gambio.widgets.module('amazon_checkout', [], function (data) {

	'use strict';

	// ########## VARIABLE INITIALIZATION ##########

	var $this = $(this),
	    $body = $('body'),
	    $countryNotAllowed = null,
	    $button = null,
	    $continue = null,
	    defaults = {
		// The amazon seller id
		sellerId: null,
		// The order reference id
		orderReference: null,
		// The size for the generated boxes
		size: { width: '600px', height: '400px' },
		// The size for the generated red onlay boxes
		sizeReadOnly: { width: '400px', height: '185px' },
		// Error message shown if the country isn't allowed
		countryTxt: '',
		// Text that is shown inside the signout button
		buttonTxt: '',
		// Selector for the continue button
		continueBtn: '.btn-continue',
		// Class set to error messages
		errorClass: 'amzadvpay_countrynotallowed',
		// ID set to the signout button
		buttonAClass: 'btn btn-default btn-block amazonadvpay_signout',
		// Class set to the signout button
		buttonClass: 'col-xs-6 col-sm-6 col-md-4 col-md-offset-1 amazonadvpay_signoutbutton',
		// Append the signout button after this selector
		buttonAppendAfter: '.btn-back',
		// URL the POST sends the data to
		requestURL: 'request_port.php?module=AmazonAdvPay',
		// URL the page gets redirected to after an error on signout
		signoutErrorUrl: 'shopping_cart.php?error=apa_signout'
	},
	    options = $.extend(true, {}, defaults, data),
	    module = {};

	// ########## HELPER FUNCTIONS ##########


	/**
  * Event handler that is performed on address selection
  * or clicking on the signout button. Both actions perform
  * almost the same steps except the dataset that is deliverd
  * to the server
  * @param {object} d Contains the jQuery event object or the order reference (depending on the emitting action).
  * @private
  */
	var _onAction = function _onAction(d) {

		var dataset = d && d.data && d.data.action ? d.data : {
			orderrefid: options.orderReference,
			action: 'addressSelect'
		};

		$.post(options.requestURL, dataset).done(function (result) {

			// Reload page
			if (result.reload === 'true') {
				window.location.reload();
			}

			// Redirect to an other page
			if (result.redirect_url && dataset.action === 'signOut') {
				window.location = result.redirect_url;
			}

			// Show / hide the "country not allowed" error message
			if (result.country_allowed === 'false') {
				$continue.hide();
				$this.after($countryNotAllowed);
			} else if (dataset.action !== 'signOut') {
				$continue.show();
				$this.next('.' + options.errorClass).remove();
			}
		}).fail(function (result) {
			// If an error occurs on signout redirect page
			if (dataset.action === 'signOut') {
				window.location = options.signoutErrorUrl;
			}
		});
	};

	// ########## INITIALIZATION ##########

	/**
  * Init function of the widget
  * @constructor
  */
	module.init = function (done) {

		if (!$body.hasClass('amazon-payment-initialized')) {
			$body.addClass('amazon-payment-initialized');

			// Generate markup and select elements
			$countryNotAllowed = $('<div class="' + options.errorClass + '">' + options.countryTxt + '</div>');
			$button = $('<div class="' + options.buttonClass + '"><a class="' + options.buttonAClass + '">' + options.buttonTxt + '</div></div>');
			$continue = $(options.continueBtn);

			// Enable signout button
			$button.on('click', { orderrefid: 'n/a', action: 'signOut' }, _onAction);
			$(options.buttonAppendAfter).after($button);

			// Start the amazon widgets
			try {

				// default configuration for all widgets
				var settings = {
					sellerId: options.sellerId,
					amazonOrderReferenceId: options.orderReference,
					design: {
						designMode: 'responsive'
					},
					onAddressSelect: _onAction
				};

				if (null !== document.querySelector('#addressBookWidgetDiv')) {
					new OffAmazonPayments.Widgets.AddressBook(settings).bind('addressBookWidgetDiv');
				}
				if (null !== document.querySelector('#walletWidgetDiv')) {
					new OffAmazonPayments.Widgets.Wallet(settings).bind('walletWidgetDiv');
					document.querySelector('#walletWidgetDiv').style.height = '25em';
				}

				var roAddressWidget = document.querySelector('#readOnlyAddressBookWidgetDiv'),
				    roWalletWidget = document.querySelector('#readOnlyWalletWidgetDiv');
				if (null !== roAddressWidget && null !== roWalletWidget) {
					roAddressWidget.style.height = '12em';
					roWalletWidget.style.height = '12em';
					roWalletWidget.style['margin-bottom'] = '3em';
					$.extend(settings, { displayMode: 'Read' });
					new OffAmazonPayments.Widgets.AddressBook(settings).bind('readOnlyAddressBookWidgetDiv');
					new OffAmazonPayments.Widgets.Wallet(settings).bind('readOnlyWalletWidgetDiv');
				}
			} catch (ignore) {}
		}

		done();
	};

	// Return data to widget engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpZGdldHMvYW1hem9uX2NoZWNrb3V0LmpzIl0sIm5hbWVzIjpbImdhbWJpbyIsIndpZGdldHMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiJGJvZHkiLCIkY291bnRyeU5vdEFsbG93ZWQiLCIkYnV0dG9uIiwiJGNvbnRpbnVlIiwiZGVmYXVsdHMiLCJzZWxsZXJJZCIsIm9yZGVyUmVmZXJlbmNlIiwic2l6ZSIsIndpZHRoIiwiaGVpZ2h0Iiwic2l6ZVJlYWRPbmx5IiwiY291bnRyeVR4dCIsImJ1dHRvblR4dCIsImNvbnRpbnVlQnRuIiwiZXJyb3JDbGFzcyIsImJ1dHRvbkFDbGFzcyIsImJ1dHRvbkNsYXNzIiwiYnV0dG9uQXBwZW5kQWZ0ZXIiLCJyZXF1ZXN0VVJMIiwic2lnbm91dEVycm9yVXJsIiwib3B0aW9ucyIsImV4dGVuZCIsIl9vbkFjdGlvbiIsImQiLCJkYXRhc2V0IiwiYWN0aW9uIiwib3JkZXJyZWZpZCIsInBvc3QiLCJkb25lIiwicmVzdWx0IiwicmVsb2FkIiwid2luZG93IiwibG9jYXRpb24iLCJyZWRpcmVjdF91cmwiLCJjb3VudHJ5X2FsbG93ZWQiLCJoaWRlIiwiYWZ0ZXIiLCJzaG93IiwibmV4dCIsInJlbW92ZSIsImZhaWwiLCJpbml0IiwiaGFzQ2xhc3MiLCJhZGRDbGFzcyIsIm9uIiwic2V0dGluZ3MiLCJhbWF6b25PcmRlclJlZmVyZW5jZUlkIiwiZGVzaWduIiwiZGVzaWduTW9kZSIsIm9uQWRkcmVzc1NlbGVjdCIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvciIsIk9mZkFtYXpvblBheW1lbnRzIiwiV2lkZ2V0cyIsIkFkZHJlc3NCb29rIiwiYmluZCIsIldhbGxldCIsInN0eWxlIiwicm9BZGRyZXNzV2lkZ2V0Iiwicm9XYWxsZXRXaWRnZXQiLCJkaXNwbGF5TW9kZSIsImlnbm9yZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOztBQUVBOzs7O0FBSUFBLE9BQU9DLE9BQVAsQ0FBZUMsTUFBZixDQUFzQixpQkFBdEIsRUFBeUMsRUFBekMsRUFBNkMsVUFBU0MsSUFBVCxFQUFlOztBQUUzRDs7QUFFRDs7QUFFQyxLQUFJQyxRQUFRQyxFQUFFLElBQUYsQ0FBWjtBQUFBLEtBQ0NDLFFBQVFELEVBQUUsTUFBRixDQURUO0FBQUEsS0FFQ0UscUJBQXFCLElBRnRCO0FBQUEsS0FHQ0MsVUFBVSxJQUhYO0FBQUEsS0FJQ0MsWUFBWSxJQUpiO0FBQUEsS0FLQ0MsV0FBVztBQUNWO0FBQ0FDLFlBQVUsSUFGQTtBQUdWO0FBQ0FDLGtCQUFnQixJQUpOO0FBS1Y7QUFDQUMsUUFBTSxFQUFDQyxPQUFPLE9BQVIsRUFBaUJDLFFBQVEsT0FBekIsRUFOSTtBQU9WO0FBQ0FDLGdCQUFjLEVBQUNGLE9BQU8sT0FBUixFQUFpQkMsUUFBUSxPQUF6QixFQVJKO0FBU1Y7QUFDQUUsY0FBWSxFQVZGO0FBV1Y7QUFDQUMsYUFBVyxFQVpEO0FBYVY7QUFDQUMsZUFBYSxlQWRIO0FBZVY7QUFDQUMsY0FBWSw2QkFoQkY7QUFpQlY7QUFDQUMsZ0JBQWMsZ0RBbEJKO0FBbUJWO0FBQ0FDLGVBQWEsdUVBcEJIO0FBcUJWO0FBQ0FDLHFCQUFtQixXQXRCVDtBQXVCVjtBQUNBQyxjQUFZLHNDQXhCRjtBQXlCVjtBQUNBQyxtQkFBaUI7QUExQlAsRUFMWjtBQUFBLEtBaUNDQyxVQUFVckIsRUFBRXNCLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQmpCLFFBQW5CLEVBQTZCUCxJQUE3QixDQWpDWDtBQUFBLEtBa0NDRCxTQUFTLEVBbENWOztBQW9DRDs7O0FBR0M7Ozs7Ozs7O0FBUUEsS0FBSTBCLFlBQVksU0FBWkEsU0FBWSxDQUFTQyxDQUFULEVBQVk7O0FBRTNCLE1BQUlDLFVBQVdELEtBQUtBLEVBQUUxQixJQUFQLElBQWUwQixFQUFFMUIsSUFBRixDQUFPNEIsTUFBdkIsR0FBaUNGLEVBQUUxQixJQUFuQyxHQUEwQztBQUN2RDZCLGVBQVlOLFFBQVFkLGNBRG1DO0FBRXZEbUIsV0FBUTtBQUYrQyxHQUF4RDs7QUFLQTFCLElBQUU0QixJQUFGLENBQU9QLFFBQVFGLFVBQWYsRUFBMkJNLE9BQTNCLEVBQW9DSSxJQUFwQyxDQUF5QyxVQUFTQyxNQUFULEVBQWlCOztBQUV6RDtBQUNBLE9BQUlBLE9BQU9DLE1BQVAsS0FBa0IsTUFBdEIsRUFBOEI7QUFDN0JDLFdBQU9DLFFBQVAsQ0FBZ0JGLE1BQWhCO0FBQ0E7O0FBRUQ7QUFDQSxPQUFJRCxPQUFPSSxZQUFQLElBQXVCVCxRQUFRQyxNQUFSLEtBQW1CLFNBQTlDLEVBQXlEO0FBQ3hETSxXQUFPQyxRQUFQLEdBQWtCSCxPQUFPSSxZQUF6QjtBQUNBOztBQUVEO0FBQ0EsT0FBSUosT0FBT0ssZUFBUCxLQUEyQixPQUEvQixFQUF3QztBQUN2Qy9CLGNBQVVnQyxJQUFWO0FBQ0FyQyxVQUFNc0MsS0FBTixDQUFZbkMsa0JBQVo7QUFDQSxJQUhELE1BR08sSUFBSXVCLFFBQVFDLE1BQVIsS0FBbUIsU0FBdkIsRUFBa0M7QUFDeEN0QixjQUFVa0MsSUFBVjtBQUNBdkMsVUFDRXdDLElBREYsQ0FDTyxNQUFNbEIsUUFBUU4sVUFEckIsRUFFRXlCLE1BRkY7QUFHQTtBQUVELEdBdkJELEVBdUJHQyxJQXZCSCxDQXVCUSxVQUFTWCxNQUFULEVBQWlCO0FBQ3hCO0FBQ0EsT0FBSUwsUUFBUUMsTUFBUixLQUFtQixTQUF2QixFQUFrQztBQUNqQ00sV0FBT0MsUUFBUCxHQUFrQlosUUFBUUQsZUFBMUI7QUFDQTtBQUNELEdBNUJEO0FBNkJBLEVBcENEOztBQXNDRDs7QUFFQzs7OztBQUlBdkIsUUFBTzZDLElBQVAsR0FBYyxVQUFTYixJQUFULEVBQWU7O0FBRTVCLE1BQUksQ0FBQzVCLE1BQU0wQyxRQUFOLENBQWUsNEJBQWYsQ0FBTCxFQUFtRDtBQUNsRDFDLFNBQU0yQyxRQUFOLENBQWUsNEJBQWY7O0FBRUE7QUFDQTFDLHdCQUFxQkYsRUFBRSxpQkFBaUJxQixRQUFRTixVQUF6QixHQUFzQyxJQUF0QyxHQUE2Q00sUUFBUVQsVUFBckQsR0FBa0UsUUFBcEUsQ0FBckI7QUFDQVQsYUFBVUgsRUFBRSxpQkFBaUJxQixRQUFRSixXQUF6QixHQUF1QyxjQUF2QyxHQUF3REksUUFBUUwsWUFBaEUsR0FBK0UsSUFBL0UsR0FDRUssUUFBUVIsU0FEVixHQUNzQixjQUR4QixDQUFWO0FBRUFULGVBQVlKLEVBQUVxQixRQUFRUCxXQUFWLENBQVo7O0FBRUE7QUFDQVgsV0FBUTBDLEVBQVIsQ0FBVyxPQUFYLEVBQW9CLEVBQUNsQixZQUFZLEtBQWIsRUFBb0JELFFBQVEsU0FBNUIsRUFBcEIsRUFBNERILFNBQTVEO0FBQ0F2QixLQUFFcUIsUUFBUUgsaUJBQVYsRUFBNkJtQixLQUE3QixDQUFtQ2xDLE9BQW5DOztBQUVBO0FBQ0EsT0FBSTs7QUFFSDtBQUNBLFFBQUkyQyxXQUFXO0FBQ2R4QyxlQUFVZSxRQUFRZixRQURKO0FBRWR5Qyw2QkFBd0IxQixRQUFRZCxjQUZsQjtBQUdkeUMsYUFBUTtBQUNQQyxrQkFBWTtBQURMLE1BSE07QUFNZEMsc0JBQWlCM0I7QUFOSCxLQUFmOztBQVNBLFFBQUcsU0FBUzRCLFNBQVNDLGFBQVQsQ0FBdUIsdUJBQXZCLENBQVosRUFBNkQ7QUFDNUQsU0FBSUMsa0JBQWtCQyxPQUFsQixDQUEwQkMsV0FBOUIsQ0FBMENULFFBQTFDLEVBQW9EVSxJQUFwRCxDQUF5RCxzQkFBekQ7QUFDQTtBQUNELFFBQUcsU0FBU0wsU0FBU0MsYUFBVCxDQUF1QixrQkFBdkIsQ0FBWixFQUF3RDtBQUN2RCxTQUFJQyxrQkFBa0JDLE9BQWxCLENBQTBCRyxNQUE5QixDQUFxQ1gsUUFBckMsRUFBK0NVLElBQS9DLENBQW9ELGlCQUFwRDtBQUNBTCxjQUFTQyxhQUFULENBQXVCLGtCQUF2QixFQUEyQ00sS0FBM0MsQ0FBaURoRCxNQUFqRCxHQUEwRCxNQUExRDtBQUNBOztBQUVELFFBQUlpRCxrQkFBa0JSLFNBQVNDLGFBQVQsQ0FBdUIsK0JBQXZCLENBQXRCO0FBQUEsUUFDQ1EsaUJBQWlCVCxTQUFTQyxhQUFULENBQXVCLDBCQUF2QixDQURsQjtBQUVBLFFBQUcsU0FBU08sZUFBVCxJQUE0QixTQUFTQyxjQUF4QyxFQUF3RDtBQUN2REQscUJBQWdCRCxLQUFoQixDQUFzQmhELE1BQXRCLEdBQStCLE1BQS9CO0FBQ0FrRCxvQkFBZUYsS0FBZixDQUFxQmhELE1BQXJCLEdBQThCLE1BQTlCO0FBQ0FrRCxvQkFBZUYsS0FBZixDQUFxQixlQUFyQixJQUF3QyxLQUF4QztBQUNBMUQsT0FBRXNCLE1BQUYsQ0FBU3dCLFFBQVQsRUFBbUIsRUFBQ2UsYUFBYSxNQUFkLEVBQW5CO0FBQ0EsU0FBSVIsa0JBQWtCQyxPQUFsQixDQUEwQkMsV0FBOUIsQ0FBMENULFFBQTFDLEVBQW9EVSxJQUFwRCxDQUF5RCw4QkFBekQ7QUFDQSxTQUFJSCxrQkFBa0JDLE9BQWxCLENBQTBCRyxNQUE5QixDQUFxQ1gsUUFBckMsRUFBK0NVLElBQS9DLENBQW9ELHlCQUFwRDtBQUNBO0FBRUQsSUEvQkQsQ0ErQkUsT0FBT00sTUFBUCxFQUFlLENBQ2hCO0FBQ0Q7O0FBRURqQztBQUNBLEVBcEREOztBQXNEQTtBQUNBLFFBQU9oQyxNQUFQO0FBQ0EsQ0F6SkQiLCJmaWxlIjoid2lkZ2V0cy9hbWF6b25fY2hlY2tvdXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGFtYXpvbl9jaGVja291dC5qcyAyMDE4LTExLTE2XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNiBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuLyogZ2xvYmFscyBPZmZBbWF6b25QYXltZW50cyAqL1xuXG4vKipcbiAqIFdpZGdldCB0aGF0IHBlcmZvcm1zIGFsbCBhY3Rpb25zIG9mIHRoZSBhbWF6b24gcGF5bWVuZCBtZXRob2RcbiAqIGF0IHRoZSBjaGVja291dCBwcm9jZXNzXG4gKi9cbmdhbWJpby53aWRnZXRzLm1vZHVsZSgnYW1hem9uX2NoZWNrb3V0JywgW10sIGZ1bmN0aW9uKGRhdGEpIHtcblxuXHQndXNlIHN0cmljdCc7XG5cbi8vICMjIyMjIyMjIyMgVkFSSUFCTEUgSU5JVElBTElaQVRJT04gIyMjIyMjIyMjI1xuXG5cdHZhciAkdGhpcyA9ICQodGhpcyksXG5cdFx0JGJvZHkgPSAkKCdib2R5JyksXG5cdFx0JGNvdW50cnlOb3RBbGxvd2VkID0gbnVsbCxcblx0XHQkYnV0dG9uID0gbnVsbCxcblx0XHQkY29udGludWUgPSBudWxsLFxuXHRcdGRlZmF1bHRzID0ge1xuXHRcdFx0Ly8gVGhlIGFtYXpvbiBzZWxsZXIgaWRcblx0XHRcdHNlbGxlcklkOiBudWxsLFxuXHRcdFx0Ly8gVGhlIG9yZGVyIHJlZmVyZW5jZSBpZFxuXHRcdFx0b3JkZXJSZWZlcmVuY2U6IG51bGwsXG5cdFx0XHQvLyBUaGUgc2l6ZSBmb3IgdGhlIGdlbmVyYXRlZCBib3hlc1xuXHRcdFx0c2l6ZToge3dpZHRoOiAnNjAwcHgnLCBoZWlnaHQ6ICc0MDBweCd9LFxuXHRcdFx0Ly8gVGhlIHNpemUgZm9yIHRoZSBnZW5lcmF0ZWQgcmVkIG9ubGF5IGJveGVzXG5cdFx0XHRzaXplUmVhZE9ubHk6IHt3aWR0aDogJzQwMHB4JywgaGVpZ2h0OiAnMTg1cHgnfSxcblx0XHRcdC8vIEVycm9yIG1lc3NhZ2Ugc2hvd24gaWYgdGhlIGNvdW50cnkgaXNuJ3QgYWxsb3dlZFxuXHRcdFx0Y291bnRyeVR4dDogJycsXG5cdFx0XHQvLyBUZXh0IHRoYXQgaXMgc2hvd24gaW5zaWRlIHRoZSBzaWdub3V0IGJ1dHRvblxuXHRcdFx0YnV0dG9uVHh0OiAnJyxcblx0XHRcdC8vIFNlbGVjdG9yIGZvciB0aGUgY29udGludWUgYnV0dG9uXG5cdFx0XHRjb250aW51ZUJ0bjogJy5idG4tY29udGludWUnLFxuXHRcdFx0Ly8gQ2xhc3Mgc2V0IHRvIGVycm9yIG1lc3NhZ2VzXG5cdFx0XHRlcnJvckNsYXNzOiAnYW16YWR2cGF5X2NvdW50cnlub3RhbGxvd2VkJyxcblx0XHRcdC8vIElEIHNldCB0byB0aGUgc2lnbm91dCBidXR0b25cblx0XHRcdGJ1dHRvbkFDbGFzczogJ2J0biBidG4tZGVmYXVsdCBidG4tYmxvY2sgYW1hem9uYWR2cGF5X3NpZ25vdXQnLFxuXHRcdFx0Ly8gQ2xhc3Mgc2V0IHRvIHRoZSBzaWdub3V0IGJ1dHRvblxuXHRcdFx0YnV0dG9uQ2xhc3M6ICdjb2wteHMtNiBjb2wtc20tNiBjb2wtbWQtNCBjb2wtbWQtb2Zmc2V0LTEgYW1hem9uYWR2cGF5X3NpZ25vdXRidXR0b24nLFxuXHRcdFx0Ly8gQXBwZW5kIHRoZSBzaWdub3V0IGJ1dHRvbiBhZnRlciB0aGlzIHNlbGVjdG9yXG5cdFx0XHRidXR0b25BcHBlbmRBZnRlcjogJy5idG4tYmFjaycsXG5cdFx0XHQvLyBVUkwgdGhlIFBPU1Qgc2VuZHMgdGhlIGRhdGEgdG9cblx0XHRcdHJlcXVlc3RVUkw6ICdyZXF1ZXN0X3BvcnQucGhwP21vZHVsZT1BbWF6b25BZHZQYXknLFxuXHRcdFx0Ly8gVVJMIHRoZSBwYWdlIGdldHMgcmVkaXJlY3RlZCB0byBhZnRlciBhbiBlcnJvciBvbiBzaWdub3V0XG5cdFx0XHRzaWdub3V0RXJyb3JVcmw6ICdzaG9wcGluZ19jYXJ0LnBocD9lcnJvcj1hcGFfc2lnbm91dCdcblx0XHR9LFxuXHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdG1vZHVsZSA9IHt9O1xuXG4vLyAjIyMjIyMjIyMjIEhFTFBFUiBGVU5DVElPTlMgIyMjIyMjIyMjI1xuXG5cblx0LyoqXG5cdCAqIEV2ZW50IGhhbmRsZXIgdGhhdCBpcyBwZXJmb3JtZWQgb24gYWRkcmVzcyBzZWxlY3Rpb25cblx0ICogb3IgY2xpY2tpbmcgb24gdGhlIHNpZ25vdXQgYnV0dG9uLiBCb3RoIGFjdGlvbnMgcGVyZm9ybVxuXHQgKiBhbG1vc3QgdGhlIHNhbWUgc3RlcHMgZXhjZXB0IHRoZSBkYXRhc2V0IHRoYXQgaXMgZGVsaXZlcmRcblx0ICogdG8gdGhlIHNlcnZlclxuXHQgKiBAcGFyYW0ge29iamVjdH0gZCBDb250YWlucyB0aGUgalF1ZXJ5IGV2ZW50IG9iamVjdCBvciB0aGUgb3JkZXIgcmVmZXJlbmNlIChkZXBlbmRpbmcgb24gdGhlIGVtaXR0aW5nIGFjdGlvbikuXG5cdCAqIEBwcml2YXRlXG5cdCAqL1xuXHR2YXIgX29uQWN0aW9uID0gZnVuY3Rpb24oZCkge1xuXG5cdFx0dmFyIGRhdGFzZXQgPSAoZCAmJiBkLmRhdGEgJiYgZC5kYXRhLmFjdGlvbikgPyBkLmRhdGEgOiB7XG5cdFx0XHRvcmRlcnJlZmlkOiBvcHRpb25zLm9yZGVyUmVmZXJlbmNlLFxuXHRcdFx0YWN0aW9uOiAnYWRkcmVzc1NlbGVjdCdcblx0XHR9O1xuXG5cdFx0JC5wb3N0KG9wdGlvbnMucmVxdWVzdFVSTCwgZGF0YXNldCkuZG9uZShmdW5jdGlvbihyZXN1bHQpIHtcblxuXHRcdFx0Ly8gUmVsb2FkIHBhZ2Vcblx0XHRcdGlmIChyZXN1bHQucmVsb2FkID09PSAndHJ1ZScpIHtcblx0XHRcdFx0d2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xuXHRcdFx0fVxuXG5cdFx0XHQvLyBSZWRpcmVjdCB0byBhbiBvdGhlciBwYWdlXG5cdFx0XHRpZiAocmVzdWx0LnJlZGlyZWN0X3VybCAmJiBkYXRhc2V0LmFjdGlvbiA9PT0gJ3NpZ25PdXQnKSB7XG5cdFx0XHRcdHdpbmRvdy5sb2NhdGlvbiA9IHJlc3VsdC5yZWRpcmVjdF91cmw7XG5cdFx0XHR9XG5cblx0XHRcdC8vIFNob3cgLyBoaWRlIHRoZSBcImNvdW50cnkgbm90IGFsbG93ZWRcIiBlcnJvciBtZXNzYWdlXG5cdFx0XHRpZiAocmVzdWx0LmNvdW50cnlfYWxsb3dlZCA9PT0gJ2ZhbHNlJykge1xuXHRcdFx0XHQkY29udGludWUuaGlkZSgpO1xuXHRcdFx0XHQkdGhpcy5hZnRlcigkY291bnRyeU5vdEFsbG93ZWQpO1xuXHRcdFx0fSBlbHNlIGlmIChkYXRhc2V0LmFjdGlvbiAhPT0gJ3NpZ25PdXQnKSB7XG5cdFx0XHRcdCRjb250aW51ZS5zaG93KCk7XG5cdFx0XHRcdCR0aGlzXG5cdFx0XHRcdFx0Lm5leHQoJy4nICsgb3B0aW9ucy5lcnJvckNsYXNzKVxuXHRcdFx0XHRcdC5yZW1vdmUoKTtcblx0XHRcdH1cblxuXHRcdH0pLmZhaWwoZnVuY3Rpb24ocmVzdWx0KSB7XG5cdFx0XHQvLyBJZiBhbiBlcnJvciBvY2N1cnMgb24gc2lnbm91dCByZWRpcmVjdCBwYWdlXG5cdFx0XHRpZiAoZGF0YXNldC5hY3Rpb24gPT09ICdzaWduT3V0Jykge1xuXHRcdFx0XHR3aW5kb3cubG9jYXRpb24gPSBvcHRpb25zLnNpZ25vdXRFcnJvclVybDtcblx0XHRcdH1cblx0XHR9KTtcblx0fTtcblxuLy8gIyMjIyMjIyMjIyBJTklUSUFMSVpBVElPTiAjIyMjIyMjIyMjXG5cblx0LyoqXG5cdCAqIEluaXQgZnVuY3Rpb24gb2YgdGhlIHdpZGdldFxuXHQgKiBAY29uc3RydWN0b3Jcblx0ICovXG5cdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXG5cdFx0aWYgKCEkYm9keS5oYXNDbGFzcygnYW1hem9uLXBheW1lbnQtaW5pdGlhbGl6ZWQnKSkge1xuXHRcdFx0JGJvZHkuYWRkQ2xhc3MoJ2FtYXpvbi1wYXltZW50LWluaXRpYWxpemVkJyk7XG5cblx0XHRcdC8vIEdlbmVyYXRlIG1hcmt1cCBhbmQgc2VsZWN0IGVsZW1lbnRzXG5cdFx0XHQkY291bnRyeU5vdEFsbG93ZWQgPSAkKCc8ZGl2IGNsYXNzPVwiJyArIG9wdGlvbnMuZXJyb3JDbGFzcyArICdcIj4nICsgb3B0aW9ucy5jb3VudHJ5VHh0ICsgJzwvZGl2PicpO1xuXHRcdFx0JGJ1dHRvbiA9ICQoJzxkaXYgY2xhc3M9XCInICsgb3B0aW9ucy5idXR0b25DbGFzcyArICdcIj48YSBjbGFzcz1cIicgKyBvcHRpb25zLmJ1dHRvbkFDbGFzcyArICdcIj4nXG5cdFx0XHQgICAgICAgICAgICArIG9wdGlvbnMuYnV0dG9uVHh0ICsgJzwvZGl2PjwvZGl2PicpO1xuXHRcdFx0JGNvbnRpbnVlID0gJChvcHRpb25zLmNvbnRpbnVlQnRuKTtcblxuXHRcdFx0Ly8gRW5hYmxlIHNpZ25vdXQgYnV0dG9uXG5cdFx0XHQkYnV0dG9uLm9uKCdjbGljaycsIHtvcmRlcnJlZmlkOiAnbi9hJywgYWN0aW9uOiAnc2lnbk91dCd9LCBfb25BY3Rpb24pO1xuXHRcdFx0JChvcHRpb25zLmJ1dHRvbkFwcGVuZEFmdGVyKS5hZnRlcigkYnV0dG9uKTtcblxuXHRcdFx0Ly8gU3RhcnQgdGhlIGFtYXpvbiB3aWRnZXRzXG5cdFx0XHR0cnkge1xuXG5cdFx0XHRcdC8vIGRlZmF1bHQgY29uZmlndXJhdGlvbiBmb3IgYWxsIHdpZGdldHNcblx0XHRcdFx0dmFyIHNldHRpbmdzID0ge1xuXHRcdFx0XHRcdHNlbGxlcklkOiBvcHRpb25zLnNlbGxlcklkLFxuXHRcdFx0XHRcdGFtYXpvbk9yZGVyUmVmZXJlbmNlSWQ6IG9wdGlvbnMub3JkZXJSZWZlcmVuY2UsXG5cdFx0XHRcdFx0ZGVzaWduOiB7XG5cdFx0XHRcdFx0XHRkZXNpZ25Nb2RlOiAncmVzcG9uc2l2ZSdcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdG9uQWRkcmVzc1NlbGVjdDogX29uQWN0aW9uXG5cdFx0XHRcdH07XG5cdFx0XHRcdFxuXHRcdFx0XHRpZihudWxsICE9PSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjYWRkcmVzc0Jvb2tXaWRnZXREaXYnKSkge1xuXHRcdFx0XHRcdG5ldyBPZmZBbWF6b25QYXltZW50cy5XaWRnZXRzLkFkZHJlc3NCb29rKHNldHRpbmdzKS5iaW5kKCdhZGRyZXNzQm9va1dpZGdldERpdicpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmKG51bGwgIT09IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyN3YWxsZXRXaWRnZXREaXYnKSkge1xuXHRcdFx0XHRcdG5ldyBPZmZBbWF6b25QYXltZW50cy5XaWRnZXRzLldhbGxldChzZXR0aW5ncykuYmluZCgnd2FsbGV0V2lkZ2V0RGl2Jyk7XG5cdFx0XHRcdFx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3dhbGxldFdpZGdldERpdicpLnN0eWxlLmhlaWdodCA9ICcyNWVtJztcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGxldCByb0FkZHJlc3NXaWRnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjcmVhZE9ubHlBZGRyZXNzQm9va1dpZGdldERpdicpLFxuXHRcdFx0XHRcdHJvV2FsbGV0V2lkZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3JlYWRPbmx5V2FsbGV0V2lkZ2V0RGl2Jyk7XG5cdFx0XHRcdGlmKG51bGwgIT09IHJvQWRkcmVzc1dpZGdldCAmJiBudWxsICE9PSByb1dhbGxldFdpZGdldCkge1xuXHRcdFx0XHRcdHJvQWRkcmVzc1dpZGdldC5zdHlsZS5oZWlnaHQgPSAnMTJlbSc7XG5cdFx0XHRcdFx0cm9XYWxsZXRXaWRnZXQuc3R5bGUuaGVpZ2h0ID0gJzEyZW0nO1xuXHRcdFx0XHRcdHJvV2FsbGV0V2lkZ2V0LnN0eWxlWydtYXJnaW4tYm90dG9tJ10gPSAnM2VtJztcblx0XHRcdFx0XHQkLmV4dGVuZChzZXR0aW5ncywge2Rpc3BsYXlNb2RlOiAnUmVhZCd9KTtcblx0XHRcdFx0XHRuZXcgT2ZmQW1hem9uUGF5bWVudHMuV2lkZ2V0cy5BZGRyZXNzQm9vayhzZXR0aW5ncykuYmluZCgncmVhZE9ubHlBZGRyZXNzQm9va1dpZGdldERpdicpO1xuXHRcdFx0XHRcdG5ldyBPZmZBbWF6b25QYXltZW50cy5XaWRnZXRzLldhbGxldChzZXR0aW5ncykuYmluZCgncmVhZE9ubHlXYWxsZXRXaWRnZXREaXYnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdH0gY2F0Y2ggKGlnbm9yZSkge1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdGRvbmUoKTtcblx0fTtcblxuXHQvLyBSZXR1cm4gZGF0YSB0byB3aWRnZXQgZW5naW5lXG5cdHJldHVybiBtb2R1bGU7XG59KTtcbiJdfQ==
