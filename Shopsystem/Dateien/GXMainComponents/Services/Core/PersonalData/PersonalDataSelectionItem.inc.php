<?php
/* --------------------------------------------------------------
   PersonalDataSelectionItem.inc.php 2018-05-14
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class representing the personal data selection item
 */
class PersonalDataSelectionItem {
    /**
     * Base data
     */
    const BASE_DATA = 'base_data';

    /**
     * Orders
     */
    const ORDERS = 'orders';

    /**
     * Withdrawals
     */
    const WITHDRAWALS = 'withdrawals';

    /**
     * Agreements
     */
    const AGREEMENTS = 'agreements';

    /**
     * Emails
     */
    const EMAILS = 'emails';

    /**
     * Shopping carts
     */
    const CARTS = 'carts';

    /**
     * Reviews
     */
    const REVIEWS = 'reviews';

    /**
     * Newsletter subscriptions
     */
    const NEWSLETTER_SUBSCRIPTIONS = 'newsletter_subscriptions';
}