<?php

/* --------------------------------------------------------------
   AuthCredentialsInterface.inc.php 2016-08-05
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface AuthCredentialsInterface
 * 
 * @category   System
 * @package    Authentication
 * @subpackage Interfaces
 */
interface AuthCredentialsInterface
{
	
}