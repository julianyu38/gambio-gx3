<?php
/* --------------------------------------------------------------
 ParcelTrackingCodeFinder.inc.php 2018-01-15
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Class ParcelTrackingCodeFinder
 */
class ParcelTrackingCodeFinder
{
	/**
	 * @var \CI_DB_query_builder
	 */
	protected $db;
	
	/**
	 * @var string
	 */
	protected $table = 'orders_parcel_tracking_codes';
	
	
	/**
	 * ParcelTrackingCodeFinder constructor.
	 *
	 * @param \CI_DB_query_builder $db Database access.
	 */
	public function __construct(CI_DB_query_builder $db)
	{
		$this->db = $db;
	}
	
	
	/**
	 * Returns parcel tracking code data by the given tracking code id.
	 *
	 * @param \ParcelTrackingCodeId $id Id of searched parcel tracking code entry.
	 *
	 * @return array
	 */
	public function find(ParcelTrackingCodeId $id)
	{
		return $this->db->where('orders_parcel_tracking_code_id', $id->is())->get($this->table)->row_array() ? : [];
	}
	
	
	/**
	 * Returns parcel tracking codes data by the given order id.
	 *
	 * @param \ParcelTrackingCodeOrderId $orderId Id of order.
	 *
	 * @return array
	 */
	public function findByOrderId(ParcelTrackingCodeOrderId $orderId)
	{
		return $this->db->where('order_id', $orderId->is())->get($this->table)->result_array() ? : [];
	}
	
	
	/**
	 * Returns all parcel tracking code data.
	 *
	 * @return array
	 */
	public function getAll()
	{
		return $this->db->get($this->table)->result_array() ? : [];
	}
}