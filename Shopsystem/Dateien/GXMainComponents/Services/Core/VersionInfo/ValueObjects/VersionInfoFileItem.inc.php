<?php

/* --------------------------------------------------------------
   VersionInfoFileItem.inc.php 2017-03-13
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class VersionInfoFileItem
 *
 * @category    System
 * @package     VersionInfo
 * @subpackage  ValueObjects
 */
class VersionInfoFileItem extends AbstractVersionInfoItem
{
	
}