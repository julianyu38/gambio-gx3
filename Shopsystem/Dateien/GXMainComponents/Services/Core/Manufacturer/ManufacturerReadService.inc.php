<?php

/* --------------------------------------------------------------
   ManufacturerReadService.inc.php 2017-11-08
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class ManufacturerReadService
 *
 * @category   System
 * @package    Manufacturer
 */
class ManufacturerReadService implements ManufacturerReadServiceInterface
{
	/**
	 * @var \ManufacturerRepositoryInterface
	 */
	protected $repository;
	
	
	/**
	 * ManufacturerReadService constructor.
	 *
	 * @param \ManufacturerRepositoryInterface $repository
	 */
	public function __construct(ManufacturerRepositoryInterface $repository)
	{
		return $this->repository = $repository;
	}
	
	
	/**
	 * Returns manufacturer entities as collection.
	 *
	 * @return \ManufacturerCollection
	 */
	public function getAll()
	{
		return $this->repository->getAll();
	}
	
	
	/**
	 * Returns manufacturer entity by given id.
	 *
	 * @param \IdType $manufacturerId
	 *
	 * @return \ManufacturerInterface
	 */
	public function getById(IdType $manufacturerId)
	{
		return $this->repository->getById($manufacturerId);
	}
}