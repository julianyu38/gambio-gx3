<?php
/* --------------------------------------------------------------
   ManufacturerReadServiceInterface.inc.php 2017-08-10
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface ManufacturerReadServiceInterface
 *
 * @category   System
 * @package    Manufacturer
 * @subpackage Interfaces
 */
interface ManufacturerReadServiceInterface
{
	/**
	 * Returns manufacturer entities as collection.
	 *
	 * @return \ManufacturerCollection
	 */
	public function getAll();
	
	
	/**
	 * Returns manufacturer entity by given id.
	 *
	 * @param \IdType $manufacturerId
	 *
	 * @return \ManufacturerInterface
	 */
	public function getById(IdType $manufacturerId);
}