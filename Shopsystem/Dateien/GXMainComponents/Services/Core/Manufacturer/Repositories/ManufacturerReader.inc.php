<?php
/* --------------------------------------------------------------
   ManufacturerReader.inc.php 2017-08-14
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class ManufacturerReader
 *
 * @category   System
 * @package    Manufacturer
 * @subpackage Repositories
 */
class ManufacturerReader implements ManufacturerReaderInterface
{
	
	/**
	 * @var \\CI_DB_query_builder
	 */
	protected $queryBuilder;
	
	
	/**
	 * ManufacturerReader constructor.
	 *
	 * @param \CI_DB_query_builder $query_builder
	 */
	public function __construct(CI_DB_query_builder $query_builder)
	{
		$this->queryBuilder = $query_builder;
	}
	
	
	/**
	 * Returns all manufacturer entities data as array.
	 *
	 * @return array
	 */
	public function getAll()
	{
		$rawData = $this->queryBuilder->select()
		                              ->from('manufacturers')
		                              ->join('manufacturers_info',
		                                     'manufacturers.manufacturers_id = manufacturers_info.manufacturers_id')
		                              ->order_by('manufacturers.manufacturers_id', 'asc')
		                              ->order_by('manufacturers_info.languages_id', 'asc')
		                              ->get()
		                              ->result_array();
		
		$formattedArray = [];
		$urls           = [];
		
		foreach($rawData as $data)
		{
			if(isset($oldId) && $oldId !== (int)$data['manufacturers_id'])
			{
				$formattedArray[] = $urls;
				$urls             = [];
			}
			$oldId = (int)$data['manufacturers_id'];
			
			$urls['id']                         = $oldId;
			$urls['name']                       = $data['manufacturers_name'];
			$urls['image']                      = $data['manufacturers_image'];
			$urls['dateAdded']                  = $data['date_added'];
			$urls['lastModified']               = $data['last_modified'];
			$urls['url'][$data['languages_id']] = $data['manufacturers_url'];
		}
		if(count($rawData) > 0)
		{
			$formattedArray[] = $urls;
		}
		
		return $formattedArray;
	}
	
	
	/**
	 * Returns manufacturer entity data by the given id.
	 *
	 * @param \IdType $manufacturerId
	 *
	 * @throws \EntityNotFoundException If no record was found with provided manufacturer entity id.
	 * @return array
	 */
	public function getById(IdType $manufacturerId)
	{
		$rawData = $this->queryBuilder->select()
		                              ->from('manufacturers')
		                              ->join('manufacturers_info',
		                                     'manufacturers.manufacturers_id = manufacturers_info.manufacturers_id')
		                              ->where('manufacturers.manufacturers_id', $manufacturerId->asInt())
		                              ->get()
		                              ->result_array() ? : [];
		if(count($rawData) === 0)
		{
			throw new EntityNotFoundException('Manufacturer entity was not found with provided id "'
			                                  . $manufacturerId->asInt() . '"');
		}
		
		$result    = ['id' => $manufacturerId->asInt()];
		$urlsArray = [];
		
		foreach($rawData as $data)
		{
			$result['name']                   = $data['manufacturers_name'];
			$result['image']                  = $data['manufacturers_image'];
			$result['dateAdded']              = $data['date_added'];
			$result['lastModified']           = $data['last_modified'];
			$urlsArray[$data['languages_id']] = $data['manufacturers_url'];
		}
		$result['url'] = $urlsArray;
		
		return $result;
	}
}