<?php
/* --------------------------------------------------------------
   ManufacturerReadInterface.inc.php 2017-08-10
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface ManufacturerReaderInterface
 *
 * @category   System
 * @package    Manufacturer
 * @subpackage Repositories
 */
interface ManufacturerReaderInterface
{
	/**
	 * Returns all manufacturer entities data as array.
	 *
	 * @return array
	 */
	public function getAll();
	
	
	/**
	 * Returns manufacturer entity data by the given id.
	 *
	 * @param \IdType $manufacturerId
	 *
	 * @return array
	 */
	public function getById(IdType $manufacturerId);
}