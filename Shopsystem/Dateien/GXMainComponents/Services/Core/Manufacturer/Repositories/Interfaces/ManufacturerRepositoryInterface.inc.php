<?php

/* --------------------------------------------------------------
   ManufacturerRepositoryInterface.inc.php 2017-08-10
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface ManufacturerRepositoryInterface
 *
 * @category   System
 * @package    Manufacturer
 * @subpackage Repositories
 */
interface ManufacturerRepositoryInterface
{
	/**
	 * Returns all manufacturer as collection.
	 *
	 * @return \ManufacturerCollection Manufacturer collection.
	 */
	public function getAll();
	
	
	/**
	 * Returns manufacturer entity by the given id.
	 *
	 * @param \IdType $manufacturerId IdType of entity to be returned.
	 *
	 * @return \ManufacturerInterface
	 */
	public function getById(IdType $manufacturerId);
	
	
	/**
	 * Saves manufacturer entity in database.
	 *
	 * @param \ManufacturerInterface $manufacturer Manufacturer entity to be saved.
	 *
	 * @return \ManufacturerRepositoryInterface Same instance for chained method calls.
	 */
	public function save(ManufacturerInterface $manufacturer);
	
	
	/**
	 * Deletes manufacturer entity from database.
	 *
	 * @param \ManufacturerInterface $manufacturer Manufacturer entity to be deleted.
	 *
	 * @return \ManufacturerRepositoryInterface Same instance for chained method calls.
	 */
	public function delete(ManufacturerInterface $manufacturer);
	
	
	/**
	 * creates manufacturer entity.
	 *
	 * @return \Manufacturer New manufacturer entity.
	 */
	public function createManufacturer();
}