<?php

/* --------------------------------------------------------------
   LegalTextType.inc.php 2018-05-17
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class LegalTextType
 *
 * This class provides constants which represent the valid legal text version types.
 */
class LegalTextType
{
	/**
	 * Privacy
	 */
	const PRIVACY = 'privacy';
	
	/**
	 * Agb
	 */
	const AGB = 'agb';
	
	/**
	 * Withdrawal
	 */
	const WITHDRAWAL = 'withdrawal';
	
	/**
	 * Confirm Log IP
	 */
	const CONFIRM_LOG_IP = 'confirm_log_ip';
	
	/**
	 * Cookie
	 */
	const COOKIE = 'cookie';
	
	/**
	 * Transport
	 */
	const TRANSPORT = 'transport';
}