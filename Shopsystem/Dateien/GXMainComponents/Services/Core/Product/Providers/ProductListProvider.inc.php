<?php

/* --------------------------------------------------------------
   ProductListProvider.inc.php 2018-02-14
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class ProductListProvider
 *
 * @category   System
 * @package    Product
 * @subpackage Providers
 */
class ProductListProvider implements ProductListProviderInterface
{
	/**
	 * Two-letter language code.
	 *
	 * @var LanguageCode
	 */
	protected $languageCode;
	
	/**
	 * Database query conditions.
	 *
	 * @var array
	 */
	protected $conditions;
	
	/**
	 * Product repository.
	 *
	 * @var ProductRepositoryInterface
	 */
	protected $productRepository;
	
	/**
	 * Database connection.
	 *
	 * @var CI_DB_query_builder
	 */
	protected $db;
	
	
	/**
	 * ProductListProvider constructor.
	 *
	 * @param LanguageCode               $languageCode Two-letter language code.
	 * @param array                      $conditions   Database query conditions.
	 * @param ProductRepositoryInterface $productRepo  Product repository.
	 * @param CI_DB_query_builder        $db           Database connection.
	 */
	public function __construct(LanguageCode $languageCode,
	                            array $conditions = array(),
	                            ProductRepositoryInterface $productRepo,
	                            CI_DB_query_builder $db)
	{
		$this->languageCode      = $languageCode;
		$this->conditions        = $conditions;
		$this->productRepository = $productRepo;
		$this->db                = $db;
	}
	
	
	/**
	 * Returns a product list item collection by the provided category ID.
	 *
	 * @param IdType $categoryId Category ID.
	 *
	 * @throws InvalidArgumentException if the provided category ID is not valid.
	 *
	 * @return ProductListItemCollection
	 */
	public function getByCategoryId(IdType $categoryId)
	{
		$this->_selectWithCategories()->_applyExtraConditions();
		
		$this->db->where('products_to_categories.categories_id', $categoryId->asInt());
		
		$result = $this->db->get()->result_array();
		
		return $this->_prepareCollection($result);
	}
	
	
	/**
	 * Get all product list items.
	 *
	 * @return ProductListItemCollection
	 */
	public function getAll()
	{
		// Build select part of query.
		$result = $this->_select()->_applyExtraConditions()->db->get()->result_array();
		
		return $this->_prepareCollection($result);
	}
	
	
	/**
	 * Returns a paged list of product items.
	 *
	 * @param \IntType|null $page  (Optional) Offset of resource elements.
	 * @param \IntType|null $limit (Optional) Maximum amount of elements per page.
	 *
	 * @return \ProductListItemCollection
	 */
	public function getAllPaged(IntType $page = null, IntType $limit = null)
	{
		$result = $this->_select()->_applyExtraConditions()->_applyLimitAndOffset($page, $limit)->db->get()
		                                                                                            ->result_array();
		
		return $this->_prepareCollection($result);
	}
	
	
	/**
	 * Applies a LIMIT,OFFSET clause to the currently building query.
	 *
	 * @param \IntType|null $page  Page to calculate the given offset.
	 * @param \IntType|null $limit Limit of result count.
	 *
	 * @return $this|ProductListProvider Same instance for chained method calls.
	 */
	protected function _applyLimitAndOffset(IntType $page = null, IntType $limit = null)
	{
		$limit  = $limit ? $limit->asInt() : 50;
		$offset = $page && $page->asInt() > 1 ? $limit * ($page->asInt() - 1) : 0;
		$this->db->limit($limit, $offset);
		
		return $this;
	}
	
	
	/**
	 * Build the select part of the query build.
	 *
	 * @return ProductListProvider Same instance for chained method calls.
	 */
	protected function _select()
	{
		// Build the database query.
		$this->db->select('products.*, products_description.*, products_quantity_unit.quantity_unit_id')
		         ->from('products, products_description')
		         ->join('products_quantity_unit', 'products_quantity_unit.products_id = products.products_id', 'left')
		         ->join('languages', 'languages.languages_id = products_description.language_id', 'inner')
		         ->where('products_description.products_id = products.products_id')
		         ->where('languages.code', $this->languageCode->asString())
		         ->order_by('products.products_id', 'asc')
		         ->order_by('products_description.language_id', 'asc');
		
		return $this;
	}
	
	
	/**
	 * Build the select part of the query build and additionally join the products_to_categories table.
	 *
	 * @return ProductListProvider Same instance for chained method calls.
	 */
	protected function _selectWithCategories()
	{
		// Build the database query.
		$this->_select()->db->join('products_to_categories',
		                           'products_to_categories.products_id = products.products_id', 'left');
		
		return $this;
	}
	
	
	/**
	 * Apply extra query conditions.
	 *
	 * @return ProductListProvider Same instance for chained method calls.
	 */
	protected function _applyExtraConditions()
	{
		// Check for additional conditions to be appended to query (the AND operator will be used).
		if(count($this->conditions) > 0)
		{
			$this->db->where($this->conditions);
		}
		
		return $this;
	}
	
	
	/**
	 * Prepares the ProductListItemCollection object.
	 *
	 * @param array $result Query result.
	 *
	 * @throws InvalidArgumentException if the provided result is not valid.
	 *
	 * @return ProductListItemCollection
	 */
	protected function _prepareCollection(array $result)
	{
		$listItems = array();
		
		// Iterate over each query result row and create a ProductListItem for each row which will be pushed
		// into $listItems array.
		foreach($result as $row)
		{
			$productId            = new IdType((int)$row['products_id']);
			$isActive             = new BoolType((bool)$row['products_status']);
			$sortOrder            = new IntType((int)$row['products_sort']);
			$addedDateTime        = new EmptyDateTime($row['products_date_added']);
			$availableDateTime    = new EmptyDateTime($row['products_date_available']);
			$lastModifiedDateTime = new EmptyDateTime($row['products_last_modified']);
			$orderedCount         = new IntType((int)$row['products_ordered']);
			$productModel         = new StringType((string)$row['products_model']);
			$ean                  = new StringType((string)$row['products_ean']);
			$price                = new DecimalType((float)$row['products_price']);
			$discountAllowed      = new DecimalType((float)$row['products_discount_allowed']);
			$taxClassId           = new IdType((int)$row['products_tax_class_id']);
			$quantity             = new DecimalType($row['products_quantity']);
			$name                 = new StringType((string)$row['products_name']);
			$image                = new StringType((string)$row['products_image']);
			$imageAltText         = new StringType((string)$row['gm_alt_text']);
			$urlKeyWords          = new StringType((string)$row['products_meta_keywords']);
			$weight               = new DecimalType((float)$row['products_weight']);
			$shippingCosts        = new DecimalType((float)$row['nc_ultra_shipping_costs']);
			$shippingTimeId       = new IdType((int)$row['products_shippingtime']);
			$productTypeId        = new IdType((int)$row['product_type']);
			$manufacturerId       = new IdType((int)$row['manufacturers_id']);
			$quantityUnitId       = new IdType((int)$row['quantity_unit_id']);
			$isFsk18              = new BoolType((bool)$row['products_fsk18']);
			$isVpeActive          = new BoolType((bool)$row['products_vpe_status']);
			$vpeId                = new IdType((int)$row['products_vpe']);
			$vpeValue             = new DecimalType((float)$row['products_vpe_value']);
			
			$productListItem = MainFactory::create('ProductListItem', $this->productRepository);
			
			$productListItem->setProductId($productId)
			                ->setActive($isActive)
			                ->setSortOrder($sortOrder)
			                ->setAddedDateTime($addedDateTime)
			                ->setAvailableDateTime($availableDateTime)
			                ->setLastModifiedDateTime($lastModifiedDateTime)
			                ->setOrderedCount($orderedCount)
			                ->setProductModel($productModel)
			                ->setEan($ean)
			                ->setPrice($price)
			                ->setDiscountAllowed($discountAllowed)
			                ->setTaxClassId($taxClassId)
			                ->setQuantity($quantity)
			                ->setName($name)
			                ->setImage($image)
			                ->setImageAltText($imageAltText)
			                ->setUrlKeywords($urlKeyWords)
			                ->setWeight($weight)
			                ->setShippingCosts($shippingCosts)
			                ->setShippingTimeId($shippingTimeId)
			                ->setProductTypeId($productTypeId)
			                ->setManufacturerId($manufacturerId)
			                ->setQuantityUnitId($quantityUnitId)
			                ->setFsk18($isFsk18)
			                ->setVpeActive($isVpeActive)
			                ->setVpeId($vpeId)
			                ->setVpeValue($vpeValue);
			
			$listItems[] = $productListItem;
		}
		
		$collection = MainFactory::create('ProductListItemCollection', $listItems);
		
		return $collection;
	}
}