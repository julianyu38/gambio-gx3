<?php
/* --------------------------------------------------------------
	ParcelShopFinder.inc.php 2017-07-14
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Class ParcelShopFinder
 *
 * @category   System
 * @package    Extensions
 * @subpackage ParcelShopFinder
 */
class ParcelShopFinder
{
	/**
	 * Service credentials
	 */
	const CIGLOGIN = 'gambio_1:7692819c896fe2ea5fbd861cead0a08c503b69e3d56b77380a42b02aa9';
	
	/**
	 * Service URL
	 */
	const BASE_URL = 'https://cig.dhl.de/services/sandbox/rest/parcelshopfinderrest/';

	/**
	 * maximum distance of entities returned (in meters)
	 */
	const MAX_DISTANCE = 15000;

	/**
	 * @var ParcelShopFinderLogger
	 */
	protected $logger;
	
	
	/**
	 * ParcelShopFinder constructor.
	 */
	public function __construct()
	{
		$this->logger = MainFactory::create('ParcelShopFinderLogger');
	}
	
	
	/**
	 * @param $address
	 *
	 * @return array
	 */
	public function getParcelLocationByAddress($address)
	{
		$this->logger->notice(sprintf('Requesting locations around address "%s"', $address));
		$list = [];
		$actionUrl = 'parcellocationByAddress/PARCEL_PICKUP/';
		$service   = MainFactory::create('RestService');
		$url       = self::BASE_URL . $actionUrl . rawurlencode($address);
		$request   = MainFactory::create('RestRequest', 'GET', $url, null, ['Expect:']);
		$request->setUserpass(self::CIGLOGIN);
		$result = $service->performRequest($request);
		$json   = json_decode($result->getResponseBody());
		if(empty($json->psfParcellocationList))
		{
			$this->logger->noticeDebug(sprintf("Empty response:\n%s", (string)$result));
		}
		else
		{
			$list = $json->psfParcellocationList;
			$list = array_filter($list, function($psfEntry) {
				return (int)$psfEntry->geoPosition->distance <= self::MAX_DISTANCE;
			});
			$this->addOutputTitle($list);
			$this->addOpeningHours($list);
		}
		return $list;
	}

	/**
	 * Modifies result list by adding headings for output
	 *
	 * @param $psflist array
	 * @return array
	 */
	protected function addOutputTitle($psflist)
	{
		foreach($psflist as $psfentry)
		{
			if($psfentry->keyWord === 'Postfiliale')
			{
				$psfentry->outputTitle = 'Filiale';
			}
			elseif ($psfentry->keyWord === 'DHL Packstation')
			{
				$psfentry->outputTitle = 'Packstation';
			}
			elseif ($psfentry->keyWord === 'DHL Paketshop')
			{
				$psfentry->outputTitle = 'Filiale';
			}
			else
			{
				$psfentry->outputTitle = $psfentry->keyWord;
			}
		}
		return $psflist;
	}
	
	/**
	 * @param $psflist
	 *
	 * @return mixed
	 */
	protected function addOpeningHours($psflist)
	{
		foreach($psflist as $psfentry)
		{
			$openingHours = [];
			if(property_exists($psfentry, 'psfOtherinfos'))
			{
				$cols = 0;
				$rows = 0;
				foreach($psfentry->psfOtherinfos as $otherInfo)
				{
					if($otherInfo->type === 'tt_openinghours_cols')
					{
						$cols = (int)$otherInfo->content;
					}
					if($otherInfo->type === 'tt_openinghours_rows')
					{
						$rows = (int)$otherInfo->content;
					}
					if(preg_match('/^tt_openinghour_(\d)(\d)$/', $otherInfo->type, $hoursMatches) === 1)
					{
						$rowNumber = (int)$hoursMatches[1];
						$colNumber = (int)$hoursMatches[2];
						if(!isset($openingHours[$rowNumber]))
						{
							$openingHours[$rowNumber] = [];
						}
						$openingHours[$rowNumber][$colNumber] = $otherInfo->content;
					}
					if($otherInfo->type === 'tt_openinghour_today')
					{
						$psfentry->openingHoursToday = $otherInfo->content;
					}
				}
			}
			$psfentry->openingHours = $openingHours;
		}
		
		return $psflist;
	}
}
