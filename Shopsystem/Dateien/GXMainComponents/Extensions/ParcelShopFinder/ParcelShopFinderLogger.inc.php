<?php

/* --------------------------------------------------------------
	ParcelShopFinderLogger.inc.php 2017-04-06
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Class ParcelShopFinderLogger
 *
 * @category   System
 * @package    Extensions
 * @subpackage ParcelShopFinder
 */
class ParcelShopFinderLogger
{
	/**
	 * LogControl group
	 */
	const LOG_GROUP = 'shipping';
	
	/**
	 * Log file name
	 */
	const LOG_FILE = 'shipping.parcelshopfinder';
	
	/**
	 * Debug log file name
	 */
	const LOG_FILE_DEBUG = 'shipping.parcelshopfinder-debug';
	
	/**
	 * @var LogControl
	 */
	protected $logControl;
	
	
	/**
	 * ParcelShopFinderLogger constructor.
	 */
	public function __construct()
	{
		$this->logControl = LogControl::get_instance();
	}
	
	
	/**
	 * @param $message
	 */
	public function notice($message)
	{
		$this->logControl->notice($message, self::LOG_GROUP, self::LOG_FILE);
	}
	
	
	/**
	 * @param $message
	 */
	public function noticeDebug($message)
	{
		$this->logControl->notice($message, self::LOG_GROUP, self::LOG_FILE_DEBUG);
	}
}
