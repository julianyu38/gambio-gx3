<?php
/* --------------------------------------------------------------
   GeschaeftskundenversandProductException.inc.php 2017-04-06
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GeschaeftskundenversandProductException
 *
 * @extends    Exception
 * @category   System
 * @package    Geschaeftskundenversand
 * @subpackage Exceptions
 */
class GeschaeftskundenversandProductException extends Exception
{
}