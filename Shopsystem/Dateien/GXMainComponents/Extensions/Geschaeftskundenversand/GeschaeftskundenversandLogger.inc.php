<?php
/* --------------------------------------------------------------
	GeschaeftskundenversandLogger.inc.php 2017-04-06
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Class GeschaeftskundenversandLogger
 *
 * @category   System
 * @package    Extensions
 * @subpackage Geschaeftskundenversand
 */
class GeschaeftskundenversandLogger
{
	/**
	 * LogControl group
	 */
	const LOG_GROUP = 'shipping';

	/**
	 * Log file name
	 */
	const LOG_FILE = 'shipping.geschaeftskundenversand';

	/**
	 * Debug log file name
	 */
	const LOG_FILE_DEBUG = 'shipping.geschaeftskundenversand-debug';
	
	/**
	 * @var LogControl
	 */
	protected $logControl;
	
	
	/**
	 * GeschaeftskundenversandLogger constructor.
	 */
	public function __construct()
	{
		$this->logControl = LogControl::get_instance();
	}

	public function notice($message)
	{
		$this->logControl->notice($message, self::LOG_GROUP, self::LOG_FILE);
	}

	public function noticeDebug($message)
	{
		$this->logControl->notice($message, self::LOG_GROUP, self::LOG_FILE_DEBUG);
	}
}