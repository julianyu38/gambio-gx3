<?php
/* --------------------------------------------------------------
	GeschaeftskundenversandConfigurationStorage.inc.php 2018-06-01
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Class GeschaeftskundenversandConfigurationStorage
 *
 * @extends    ConfigurationStorage
 * @category   System
 * @package    Extensions
 * @subpackage Geschaeftskundenversand
 */
class GeschaeftskundenversandConfigurationStorage extends ConfigurationStorage
{
	/**
	 * namespace inside the configuration storage
	 */
	const CONFIG_STORAGE_NAMESPACE = 'modules/shipping/geschaeftskundenversand';

	const MAJOR_VERSION = 2;
	const MINOR_VERSION = 2;

	/**
	 * array holding default values to be used in absence of configured values
	 */
	protected $default_configuration;
	
	
	/**
	 * GeschaeftskundenversandConfigurationStorage constructor.
	 * 
	 * Initializes default configuration.
	 */
	public function __construct()
	{
		parent::__construct(self::CONFIG_STORAGE_NAMESPACE);
		$this->setDefaultConfiguration();
	}

	/**
	 * fills $default_configuration with initial values
	 */
	protected function setDefaultConfiguration()
	{
		$version = self::MAJOR_VERSION . '.' . self::MINOR_VERSION;
		$this->default_configuration = array(
			'wsdl_url'                   => sprintf('https://cig.dhl.de/cig-wsdls/com/dpdhl/wsdl/geschaeftskundenversand-api/%s/geschaeftskundenversand-api-%s.wsdl', $version, $version),
			'endpoint/sandbox'           => 'https://cig.dhl.de/services/sandbox/soap',
			'endpoint/live'              => 'https://cig.dhl.de/services/production/soap',
			'mode'                       => 'live', // 'sandbox', 'live'
			'cig/live/user'              => 'gambio2_1',
			'cig/live/password'          => 'fPvzp2NhTwzIwJJwdfPfI6Caz4prh6',
			'cig/sandbox/user'           => '',
			'cig/sandbox/password'       => '',
			'credentials/user'           => '',
			'credentials/password'       => '',
			'ekp'                        => '00000000000000',
			'returnreceiver/name1'           => '',
			'returnreceiver/name2'           => '',
			'returnreceiver/name3'           => '',
			'returnreceiver/streetname'      => '',
			'returnreceiver/streetnumber'    => '',
			'returnreceiver/addressaddition' => '',
			'returnreceiver/zip'             => '',
			'returnreceiver/city'            => '',
			'returnreceiver/origincountry'   => '',
			'returnreceiver/email'           => '',
			'returnreceiver/phone'           => '',
			'shipper/name1'              => '',
			'shipper/name2'              => '',
			'shipper/name3'              => '',
			'shipper/streetname'         => '',
			'shipper/streetnumber'       => '',
			'shipper/addressaddition'    => '',
			'shipper/zip'                => '',
			'shipper/city'               => '',
			'shipper/origincountry'      => '',
			'shipper/email'              => '',
			'shipper/phone'              => '',
			'bankdata/accountowner'      => '',
			'bankdata/bankname'          => '',
			'bankdata/iban'              => '',
			'bankdata/note1'             => '',
			'bankdata/note2'             => '',
			'bankdata/bic'               => '',
			'bankdata/accountreference'  => '%orders_id%',
			'order_status_after_label'   => '-1',
			'notify_customer'            => '0',
			'parcel_service_id'          => '0',
			'prefill_email'              => '0',
			'prefill_phone'              => '0',
			'open_in_new_tab'            => '1',
			'cod_add_fee'                => '0',
			'create_return_label'        => '0',
			'age_check'                  => 'none',
			'intlpremium'                => 'never', // never|eu-only|always
			'add_packing_weight'         => '0',
		);
	}


	/**
	 * returns a single configuration value by its key
	 *
	 * @param string $key a configuration key (relative to the namespace prefix)
	 *
	 * @return string configuration value
	 */
	public function get($key)
	{
		$value = parent::get($key);
		if($value === false && array_key_exists($key, $this->default_configuration))
		{
			$value = $this->default_configuration[$key];
		}
		return $value;
	}

	/**
	 * Retrieves all keys/values from a given prefix namespace
	 *
	 * @param  string $p_prefix
	 * @return array
	 */
	public function get_all($p_prefix = '')
	{
		$values = [];
		foreach($this->default_configuration as $key => $default_value)
		{
			$key_prefix = substr($key, 0, strlen($p_prefix));
			if($key_prefix == $p_prefix)
			{
				$values[$key] = $default_value;
			}
		}
		$values = array_merge($values, parent::get_all($p_prefix));
		return $values;
	}

	/**
	 * stores a configuration value by name/key
	 *
	 * @param string $name  name/key of configuration entry
	 * @param string $value value to be stored
	 *
	 * @throws Exception if data validation fails
	 */
	public function set($name, $value)
	{
		$checkName = preg_replace('_^products/\d+/(type|attendance|alias)$_', 'products/#/$1', $name);

		switch($checkName)
		{
			case 'wsdl_url':
				$value = null;
				break;
			case 'mode':
				$value = in_array($value, ['live', 'sandbox']) ? $value : 'sandbox';
				break;
			case 'age_check':
				$value = in_array($value, ['visualage18', 'identcheck18', 'none'], true) ? $value : 'none';
				break;
			case 'ekp':
				$value = trim($value);
				if(preg_match('/^\d{10}$/', $value) !== 1)
				{
					throw new InvalidEkpFormatException();
				}
				break;
			case 'cig/live/user':
			case 'cig/live/password':
			case 'cig/sandbox/user':
			case 'cig/sandbox/password':
			case 'credentials/password':
			case 'returnreceiver/name1':
			case 'returnreceiver/name2':
			case 'returnreceiver/name3':
			case 'returnreceiver/streetname':
			case 'returnreceiver/streetnumber':
			case 'returnreceiver/zip':
			case 'returnreceiver/city':
			case 'returnreceiver/origincountry':
			case 'returnreceiver/email':
			case 'returnreceiver/phone':
			case 'shipper/name1':
			case 'shipper/name2':
			case 'shipper/name3':
			case 'shipper/streetname':
			case 'shipper/streetnumber':
			case 'shipper/zip':
			case 'shipper/city':
			case 'shipper/origincountry':
			case 'shipper/email':
			case 'shipper/phone':
			case 'bankdata/accountowner':
			case 'bankdata/bankname':
			case 'bankdata/note1':
			case 'bankdata/note2':
			case 'bankdata/bic':
			case 'bankdata/accountreference':
				$value = trim((string)$value);
				break;
            case 'bankdata/iban':
                $value = preg_replace('/\s/', '', (string)$value);
                break;
			case 'credentials/user':
				$value = strtolower(trim((string)$value));
				break;
			case 'products/#/type':
			case 'products/#/alias':
				$value = (string)$value;
				break;
			case 'products/#/attendance':
				$value = strtoupper(substr(trim($value), 0, 2));
				if(preg_match('/^[[:alnum:]]{2}$/', $value) !== 1)
				{
					die($value);
					$value = '00';
				}
				break;
			case 'order_status_after_label':
			case 'parcel_service_id':
				$value = (int)$value;
				break;
			case 'notify_customer':
			case 'prefill_phone':
			case 'prefill_email':
			case 'open_in_new_tab':
			case 'cod_add_fee':
			case 'create_return_label':
			case 'add_packing_weight':
				$value = (bool)$value ? '1' : '0';
				break;
			case 'intlpremium':
				$value = in_array($value, ['never', 'eu-only', 'always'], true) ? $value : 'never';
				break;
			default:
				//throw new Exception(sprintf('tried to set invalid key %s in %s', $key, __CLASS__));
				$value = null;
		}

		if($value === null)
		{
			return;
		}
		parent::set($name, $value);
	}

	public function getProducts()
	{
		$productsConfiguration = $this->get_all_tree('products');
		$products = [];
		if(!empty($productsConfiguration))
		{
			foreach($productsConfiguration['products'] as $productsIndex => $productConfig)
			{
				$products[$productsIndex] = MainFactory::create('GeschaeftskundenversandProduct',
					                                            $productConfig['type'],
					                                            $productConfig['attendance'],
					                                            $productConfig['alias']);
			}
		}
		return $products;
	}

	public function addProduct(GeschaeftskundenversandProduct $product)
	{
		$products = $this->getProducts();
		$productIndex = empty($products) ? 0 : max(array_keys($products)) + 1;
		$keyPrefix = sprintf('products/%d/', $productIndex);
		$this->set($keyPrefix . 'type',       $product->getType());
		$this->set($keyPrefix . 'attendance', $product->getAttendance());
		$this->set($keyPrefix . 'alias',      $product->getAlias());
	}

	public function deleteProduct($index)
	{
		$prefix = sprintf('products/%d', (int)$index);
		$this->delete_all($prefix);
	}

}
