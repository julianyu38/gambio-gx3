<?php
/* --------------------------------------------------------------
	GeschaeftskundenversandModuleCenterModule.inc.php 2017-06-15
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Class GeschaeftskundenversandModuleCenterModule
 *
 * @extends    AbstractModuleCenterModule
 * @category   System
 * @package    Modules
 */
class GeschaeftskundenversandModuleCenterModule extends AbstractModuleCenterModule
{
	/**
	 * Initializes Geschäftskundenversand module center module
	 *
	 * @return void
	 */
	protected function _init()
	{
		$this->title       = $this->languageTextManager->get_text('geschaeftskundenversand_title');
		$this->description = $this->languageTextManager->get_text('geschaeftskundenversand_description');
		$this->sortOrder   = 26963;
	}
	
	
	/**
	 * Installs the module
	 */
	public function install()
	{
		$query = 'CREATE TABLE IF NOT EXISTS `gkv_shipments` (
					`gkv_shipments_id` INT          NOT NULL AUTO_INCREMENT ,
					`orders_id`        INT          NOT NULL ,
					`shipmentnumber`   VARCHAR(39)  NOT NULL ,
					`labelurl`         VARCHAR(300) NOT NULL ,
					`returnlabelurl`   VARCHAR(300) NOT NULL ,
					`exportlabelurl`   VARCHAR(300) NOT NULL ,
					`codlabelurl`      VARCHAR(300) NOT NULL ,
					`last_modified`    TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
					PRIMARY KEY (`gkv_shipments_id`),
					INDEX `orders_id` (`orders_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8';
		$db    = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$db->query($query);
		
		parent::install();
	}
}
