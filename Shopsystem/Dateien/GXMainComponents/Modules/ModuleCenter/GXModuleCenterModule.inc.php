<?php
/* --------------------------------------------------------------
  GXModuleCenterModule.inc.php 2018-04-13
  Gambio GmbH
  http://www.gambio.de
  Copyright (c) 2018 Gambio GmbH
  Released under the GNU General Public License (Version 2)
  [http://www.gnu.org/licenses/gpl-2.0.html]
  --------------------------------------------------------------
*/

/**
 * Class GXModuleCenterModule
 * @extends    AbstractModuleCenterModule
 * @category   System
 * @package    Modules
 * @subpackage Controllers
 */
class GXModuleCenterModule extends AbstractModuleCenterModule
{
	protected function _init()
	{
	}
	
	
	/**
	 * Set the tile of the module to show in the oveview
	 *
	 * @param $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	
	/**
	 * Set the name of the module to use in ajax requests
	 *
	 * @param $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
	
	
	/**
	 * Set the sort order of module in overview
	 *
	 * @param $sortOrder
	 */
	public function setSortOrder($sortOrder)
	{
		$this->sortOrder = $sortOrder;
	}
	
	
	/**
	 * Set the version of the module
	 *
	 * @param $version
	 */
	public function setVersion($version)
	{
		$this->version = $version;
	}
	
	
	/**
	 * Set the description of the module to display in overview
	 *
	 * @param $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	
	/**
	 * Installs a GXModule and sets active
	 *
	 */
	public function install()
	{
		parent::install();
		$namespace            = $this->getName();
		$configurationStorage = MainFactory::create('GXModuleConfigurationStorage', $namespace);
		$configurationStorage->set('active', true);
		
		$cacheControl = MainFactory::create_object('CacheControl');
		$cacheControl->clear_data_cache();
		@unlink(DIR_FS_CATALOG . 'cache/__dynamics.css');
	}
	
	
	/**
	 * Uninstalls a GXModule an deletes everything in GXModuleConfigurationStorage
	 *
	 */
	public function uninstall()
	{
		parent::uninstall();
		$namespace            = $this->getName();
		$configurationStorage = MainFactory::create('GXModuleConfigurationStorage', $namespace);
		$configurationStorage->delete_all();
		
		$cacheControl = MainFactory::create_object('CacheControl');
		$cacheControl->clear_data_cache();
		@unlink(DIR_FS_CATALOG . 'cache/__dynamics.css');
	}
	
	
	/**
	 * Loads the json data of the GXModule
	 *
	 * @param $name
	 *
	 * @return array|bool|mixed
	 */
	protected function _getGXModule($name)
	{
		$gxModuleFiles = GXModulesCache::getFiles();
		
		foreach($gxModuleFiles as $file)
		{
			if(strpos($file, 'GXModule.json') !== false)
			{
				preg_match("/GXModules\/(.*)\/GXModule.json/", $file, $matches);
				$moduleData = json_decode(file_get_contents($file), true);
				if(str_replace('/', '', $matches[1]) === $name)
				{
					return $moduleData;
				}
			}
		}
		
		return false;
	}
}