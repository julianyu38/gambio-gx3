<?php
/* --------------------------------------------------------------
   resource.get_usermod.php 2017-11-27
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class Smarty_Resource_Get_Usermod
 *
 * Resource overload for the smarty include statements. It enables USERMOD-files for templates.
 *
 * USAGE:
 * {include file="get_usermod:PATH/TO/TPL.FILE"}
 */
class Smarty_Resource_Get_Usermod extends Smarty_Resource_Custom
{
	
	/**
	 * Fetch a template and its modification time
	 *
	 * @param string  $name   template name
	 * @param string  $source template source
	 * @param integer $mtime  template modification timestamp (epoch)
	 *
	 * @return void
	 */
	protected function fetch($name, &$source, &$mtime)
	{
		$filename = get_usermod($this->_getPath($name));
		
		if(stripos($filename, 'extends:') === 0)
		{
			$filename  = str_replace('extends:', '', $filename);
			$filenames = explode('|', $filename);
			$source    = '{extends file="' . $filenames[0] . '"}';
			$mtime     = 0;
			unset($filenames[0]);
			foreach($filenames as $filename)
			{
				$source .= file_get_contents($filename);
				if($mtime < filemtime($filename))
				{
					$mtime = filemtime($filename);
				}
			}
		}
		else
		{
			$source = file_get_contents($filename);
			$mtime  = filemtime($filename);
		}
	}
	
	
	/**
	 * Fetch a template's modification time
	 *
	 * @note implementing this method is optional. Only implement it if modification times can be accessed faster than
	 *       loading the complete template source.
	 *
	 * @param string $name template name
	 *
	 * @return integer timestamp (epoch) the template was modified
	 */
	protected function fetchTimestamp($name)
	{
		$filename = get_usermod($this->_getPath($name));
		
		if(stripos($filename, 'extends') === 0)
		{
			return null;
		}
		
		return filemtime($filename);
	}
	
	
	/**
	 * @param $name
	 *
	 * @return string
	 */
	protected function _getPath($name)
	{
		$path = DIR_FS_CATALOG . $name;
		
		if(defined('APPLICATION_RUN_MODE') && APPLICATION_RUN_MODE === 'backend')
		{
			$path = DIR_FS_ADMIN . 'html/content/' . $name;
		}
		
		if(!file_exists($path))
		{
			$gxModulesFiles = GXModulesCache::getInstalledModuleFiles();
			$filePath       = DIR_FS_CATALOG . 'GXModules/' . $name;
			
			if(in_array(strtolower($filePath), array_map('strtolower', $gxModulesFiles), true))
			{
				$path = $filePath;
			}
		}
		
		return $path;
	}
}