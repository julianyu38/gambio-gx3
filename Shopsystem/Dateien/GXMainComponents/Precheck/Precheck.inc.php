<?php
/* --------------------------------------------------------------
   Precheck.php 2016-12-02
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class Precheck
 *
 * @category System
 * @package  Precheck
 */
class Precheck
{
	/**
	 * Minimum PHP version required.
	 * @var string
	 */
	protected $minPhpVersion = '';
	
	/**
	 * Maximum PHP version required.
	 * @var string
	 */
	protected $maxPhpVersion = '';
	
	/**
	 * Minimum MySQL version required.
	 * @var string
	 */
	protected $minMysqlVersion = '';
	
	/**
	 * Minimum MySQL version required.
	 * @var string
	 */
	protected $maxMysqlVersion = '';
	
	/**
	 * Collection of globs to find dangerous tools
	 * like 'adminer' or 'dumper'.
	 * @var array
	 */
	protected $dangerousToolsGlobs;
	
	/**
	 * Database query builder.
	 * @var CI_DB_query_builder
	 */
	protected $db;

	/**
	 * Collection of variables used.
	 * @var KeyValueCollection
	 */
	protected $settings;
	
	/**
	 * Found configuration values.
	 * @var KeyValueCollection
	 */
	protected $configuration;
	
	/**
	 * Found shop version.
	 * @var string
	 */
	protected $shopVersion = '';

	/**
	 * Array of known languages (mostly the default installed ones).
	 * @var array
	 */
	protected $defaultLanguages;

	/**
	 * Glob search pattern for usermods.
	 * @var string
	 */
	protected $usermodGlobPattern;


	/**
	 * Precheck constructor.
	 *
	 * @param CI_DB_query_builder $db       Database query builder.
	 * @param KeyValueCollection  $settings Settings key value collection (see #_checkSettings).
	 */
	public function __construct(CI_DB_query_builder $db, KeyValueCollection $settings)
	{
		// Set database driver.
		$this->db = $db;

		// Check and set some configure.php values.
		$this->_checkSettings($settings);
		$this->settings = $settings;

		// Set minimum and maximum PHP versions required.
		$this->minPhpVersion = '5.4';
		$this->maxPhpVersion = '5.6.999';

		// Set minimum and maximum MySQL versions required.
		$this->minMysqlVersion = '5.0';
		$this->maxMysqlVersion = '5.9';

		// Set dangerous tools.
		$this->dangerousToolsGlobs = [
			'*MyAdmin*',
			'*NewAdmin*',
			'*phpm*',
			'*msd*',
			'*dumper*',
			'mybackup*',
			'*admin_access*',
			'*adminer*',
			'__*'
		];

		// Set known languages.
		$this->defaultLanguages = array('de', 'en');

		// Set configuration values.
		$this->configuration = $this->_getConfigurationValues();

		// Set usermod glob search pattern.
		$this->usermodGlobPattern = '*-USERMOD.*';
	}


	/**
	 * Verifies that the settings collection parameter contains all necessary entries.
	 *
	 * Needed keys are:
	 *  - shopVersion (Current shop version - coming from release info)
	 *  - DIR_FS_CATALOG (Value from configure.php)
	 *  - HTTP_SERVER (Value from configure.php)
	 *  - DIR_WS_CATALOG (Value from configure.php)
	 *
	 * @throws InvalidArgumentException On missing key.
	 *
	 * @param KeyValueCollection $settings
	 *
	 * @protected
	 */
	protected function _checkSettings(KeyValueCollection $settings)
	{
		$neededKeys = array('shopVersion', 'DIR_FS_CATALOG', 'HTTP_SERVER', 'DIR_WS_CATALOG');

		foreach($neededKeys as $key)
		{
			if(!$settings->keyExists($key))
			{
				throw new InvalidArgumentException('Missing setting ' . $key);
			}
		}
	}


	/**
	 * Loads the configuration from the database and returns it as collection.
	 * @return KeyValueCollection
	 * @protected
	 */
	protected function _getConfigurationValues()
	{
		$configurationValues = array();
		
		$dbTableName = 'configuration';
		$dbQuery     = $this->db->get($dbTableName);
		
		foreach($dbQuery->result_array() as $row)
		{
			$key   = $row['configuration_key'];
			$value = $row['configuration_value'];
			
			$configurationValues[$key] = $value;
		}
		
		$collection = new KeyValueCollection($configurationValues);
		
		return $collection;
	}


	/**
	 * Performs a recursive glob search.
	 *
	 * @param string $pattern Glob search pattern.
	 * @param int    $flags   Glob search flags.
	 *
	 * @return array
	 * @protected
	 */
	protected function _recursiveGlobSearch($pattern, $flags = 0)
	{
		$files = glob($pattern, $flags);
		
		if(is_array($files))
		{
			$dirs = glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT);
			
			if(is_array($dirs))
			{
				foreach($dirs as $dir)
				{
					$files = array_merge($files, $this->_recursiveGlobSearch($dir . '/' . basename($pattern), $flags));
				}
			}
		}
		else
		{
			$files = array();
		}
		
		return $files;
	}


	/**
	 * Determines if the foreign module Orgamax is installed.
	 * @return bool
	 * @protected
	 */
	protected function _hasForeignModuleOrgamax()
	{
		$shopRootDir = $this->settings->getValue('DIR_FS_CATALOG');
		$condition   = file_exists($shopRootDir . 'orgamax') || file_exists($shopRootDir . 'Orgamax')
		               || file_exists($shopRootDir . 'orga');

		return $condition;
	}


	/**
	 * Determines if the foreign module PDF Katalog (Estelco) is installed.
	 * @return bool
	 * @protected
	 */
	protected function _hasForeignModuleEstelcoPdfCatalog()
	{
		$shopRootDir = $this->settings->getValue('DIR_FS_CATALOG');
		$condition   = file_exists($shopRootDir . 'admin/pdf_config.php')
		               && file_exists($shopRootDir . 'admin/pdf_export.php');

		return $condition;
	}


	/**
	 * Determines if the foreign module JTL Wawi Connector is installed.
	 * @return bool
	 * @protected
	 */
	protected function _hasForeignModuleJtlWawiConnector()
	{
		$shopRootDir = $this->settings->getValue('DIR_FS_CATALOG');
		$condition   = file_exists($shopRootDir . 'admin/includes/modules/jtlwawi_connector')
		               && is_dir($shopRootDir . 'admin/includes/modules/jtlwawi_connector');

		return $condition;
	}


	/**
	 * Determines if the foreign module Cateno Shopsync is installed.
	 * @return bool
	 * @protected
	 */
	protected function _hasForeignModuleCatenoShopsync()
	{
		$shopRootDir = $this->settings->getValue('DIR_FS_CATALOG');
		$condition   = file_exists($shopRootDir . 'shopsync') && is_dir($shopRootDir . 'shopsync');

		return $condition;
	}


	/**
	 * Determines if the foreign module Open Cart (Offene Warenkörbe) is installed.
	 * @return bool
	 * @protected
	 */
	protected function _hasForeignModuleOpenCart()
	{
		$shopRootDir = $this->settings->getValue('DIR_FS_CATALOG');
		$condition   = file_exists($shopRootDir . 'GXUserComponents/conf/admin_menu/menu_wko.xml');

		return $condition;
	}


	/**
	 * Determines if the foreign module Blog (Indiv Style) is installed.
	 * @return bool
	 * @protected
	 */
	protected function _hasForeignModuleIndivStyleBlog()
	{
		$shopRootDir = $this->settings->getValue('DIR_FS_CATALOG');
		$condition   = file_exists($shopRootDir . 'GXUserComponents/conf/admin_menu/menu_blog.xml');

		return $condition;
	}


	/**
	 * Determines if the foreign module Gratisartikel (Indiv Style) - Free products is installed.
	 * @return bool
	 * @protected
	 */
	protected function _hasForeignModuleIndivStyleFreeProducts()
	{
		$shopRootDir = $this->settings->getValue('DIR_FS_CATALOG');
		$condition   = file_exists($shopRootDir . 'GXUserComponents/conf/admin_menu/menu_gratis.xml');

		return $condition;
	}


	/**
	 * Determines if the foreign module Offene Warenkörbe (Indiv Style) - Open cart is installed.
	 * @return bool
	 * @protected
	 */
	protected function _hasForeignModuleIndivStyleOpenCart()
	{
		$shopRootDir = $this->settings->getValue('DIR_FS_CATALOG');
		$condition   = file_exists($shopRootDir . 'GXUserComponents/conf/admin_menu/menu_owk.xml');

		return $condition;
	}


	/**
	 * Returns the shop's URL.
	 * @return string
	 */
	public function getShopUrl()
	{
		$shopUrl = $this->settings->getValue('HTTP_SERVER') . $this->settings->getValue('DIR_WS_CATALOG');
		
		return $shopUrl;
	}
	
	
	/**
	 * Returns the installed shop version.
	 * @return string
	 */
	public function getShopVersion()
	{
		$version = $this->settings->getValue('shopVersion');

		return $version;
	}
	
	
	/**
	 * Returns the server address.
	 * @return string
	 */
	public function getServerAddress()
	{
		$serverAddress = $_SERVER['SERVER_ADDR'];
		
		return $serverAddress;
	}
	

	/**
	 * Returns the server's operating system.
	 * @return string
	 */
	public function getServerOs()
	{
		$os = PHP_OS;

		return $os;
	}


	/**
	 * Returns the running PHP version number.
	 * @return string
	 */
	public function getPhpVersion()
	{
		$phpVersion = PHP_VERSION;

		return $phpVersion;
	}


	/**
	 * Returns the running MySQL version number.
	 * @return string
	 */
	public function getMysqlVersion()
	{
		$mysqlVersion = $this->db->version();

		return $mysqlVersion;
	}
	

	/**
	 * Determines if cUrl is available on this platform.
	 * @return bool
	 */
	public function isCurlEnabled()
	{
		$isCallableCurlInit = is_callable('curl_init');

		return $isCallableCurlInit;
	}


	/**
	 * Returns the name of the currently used template.
	 * @return string
	 */
	public function getActiveTemplate()
	{
		$configKey      = 'CURRENT_TEMPLATE';
		$activeTemplate = $this->configuration->getValue($configKey);

		return $activeTemplate;
	}


	/**
	 * Returns an array of additional installed templates (template name).
	 * @return array
	 */
	public function getAdditionalTemplates()
	{
		$additionalTemplates = array();

		$globPattern     = $this->settings->getValue('DIR_FS_CATALOG') . 'templates/*';
		$templateFolders = glob($globPattern, GLOB_ONLYDIR | GLOB_NOSORT);
		
		if(is_array($templateFolders))
		{
			foreach($templateFolders as $templateFolder)
			{
				$templateName = basename($templateFolder);
				
				if($templateName !== $this->getActiveTemplate())
				{
					$additionalTemplates[] = $templateName;
				}
			}
		}

		return $additionalTemplates;
	}


	/**
	 * Returns an array of usermods found.
	 * @return array
	 */
	public function getUsermods()
	{
		$shopRootPath  = $this->settings->getValue('DIR_FS_CATALOG');
		$foundUsermods = array();
		$searchPaths   = array(
			// Admin HTML usermods search path.
			$shopRootPath . 'admin/html/',

			// Admin JavaScript usermods search path.
			$shopRootPath . 'admin/javascript/',

			// Shop JavaScript usermods search path.
			$shopRootPath . 'gm/javascript/',

			// Template usermods search path.
			$shopRootPath . 'templates/' . $this->getActiveTemplate() . '/'
		);

		foreach($searchPaths as $path)
		{
			if(file_exists($path))
			{
				$foundFiles = $this->_recursiveGlobSearch($path . $this->usermodGlobPattern);

				if(count($foundFiles) > 0)
				{
					foreach($foundFiles as $file)
					{
						$foundUsermods[] = substr($file, strlen($shopRootPath));
					}
				}
			}
		}

		return $foundUsermods;
	}


	/**
	 * Determines if the MobileCandy template is installed.
	 * @return bool
	 */
	public function isInstalledMobileCandy()
	{
		$searchedTemplate    = 'MobileCandy';
		$currentTemplate     = $this->getActiveTemplate();
		$additionalTemplates = $this->getAdditionalTemplates();

		$isInstalled = $searchedTemplate === $currentTemplate || in_array($searchedTemplate, $additionalTemplates);

		return $isInstalled;
	}


	/**
	 * Returns an array of MobileCandy usermods found.
	 * @return array
	 */
	public function getMobileCandyUsermods()
	{
		$foundUsermods = array();
		$shopRootPath  = $this->settings->getValue('DIR_FS_CATALOG');
		$searchPath    = $shopRootPath . 'templates/MobileCandy/';

		if($this->isInstalledMobileCandy())
		{
			$foundFiles = $this->_recursiveGlobSearch($searchPath . $this->usermodGlobPattern);

			if(count($foundFiles) > 0)
			{
				foreach($foundFiles as $file)
				{
					$foundUsermods[] = substr($file, strlen($shopRootPath));
				}
			}
		}

		return $foundUsermods;
	}


	/**
	 * Determines if there is a global usermod directory.
	 * @return bool
	 */
	public function hasGlobalUsermodDir()
	{
		$directoryPath = $this->settings->getValue('DIR_FS_CATALOG') . 'USERMOD';
		$hasDir        = file_exists($directoryPath);

		return $hasDir;
	}


	/**
	 * Returns an array of custom user components.
	 * @return array
	 */
	public function getUserComponents()
	{
		$shopRootPath        = $this->settings->getValue('DIR_FS_CATALOG');
		$userComponentsPath  = $shopRootPath . 'GXUserComponents/';
		$globSearchPattern   = '*.*';
		$foundUserComponents = array();
		$excludes            = array('index.html', '_samples', '.gitignore');
		$excludeRegexString  = '/(' . implode('|', $excludes) . ')/i';

		$foundFiles = $this->_recursiveGlobSearch($userComponentsPath . $globSearchPattern);

		foreach($foundFiles as $path)
		{
			if(!preg_match($excludeRegexString, $path))
			{
				$foundUserComponents[] = substr($path, strlen($shopRootPath));
			}
		}

		return $foundUserComponents;
	}


	/**
	 * Returns an array of foreign installed payment modules.
	 *
	 * @param array $defaultModules          List of default payment modules (note, that a file path has to be
	 *                                       relative to the payment directory).
	 *
	 * @throws InvalidArgumentException On empty array.
	 * @return array
	 */
	public function getForeignPaymentModules(array $defaultModules)
	{
		$directory             = $this->settings->getValue('DIR_FS_CATALOG') . 'includes/modules/payment/';
		$globPattern           = '*.*';
		$modules               = array();
		$foundInstalledModules = $this->_recursiveGlobSearch($directory . $globPattern);

		foreach($foundInstalledModules as $file)
		{
			$modules[] = str_replace($directory, '', $file);
		}

		foreach($defaultModules as $file)
		{
			if(in_array(trim($file), $modules))
			{
				$index = array_search(trim($file), $modules);
				unset($modules[$index]);
			}
		}

		return $modules;
	}


	/**
	 * Returns an array of foreign installed shipping modules.
	 *
	 * @param array $defaultModules          List of default shipping modules (note, that a file path has to be
	 *                                       relative to the shipping directory).
	 *
	 * @throws InvalidArgumentException On empty array.
	 * @return array
	 */
	public function getForeignShippingModules(array $defaultModules)
	{
		$directory             = $this->settings->getValue('DIR_FS_CATALOG') . 'includes/modules/shipping/';
		$globPattern           = '*.*';
		$modules               = array();
		$foundInstalledModules = $this->_recursiveGlobSearch($directory . $globPattern);

		foreach($foundInstalledModules as $file)
		{
			$modules[] = str_replace($directory, '', $file);
		}

		foreach($defaultModules as $file)
		{
			if(in_array(trim($file), $modules))
			{
				$index = array_search(trim($file), $modules);
				unset($modules[$index]);
			}
		}

		return $modules;
	}


	/**
	 * Returns an array of foreign installed order total modules.
	 *
	 * @param array $defaultModules          List of default order total modules (note, that a file path has to be
	 *                                       relative to the order_total directory).
	 *
	 * @throws InvalidArgumentException On empty array.
	 * @return array
	 */
	public function getForeignOrderTotalModules(array $defaultModules)
	{
		$directory             = $this->settings->getValue('DIR_FS_CATALOG') . 'includes/modules/order_total/';
		$globPattern           = '*.*';
		$modules               = array();
		$foundInstalledModules = $this->_recursiveGlobSearch($directory . $globPattern);

		foreach($foundInstalledModules as $file)
		{
			$modules[] = str_replace($directory, '', $file);
		}

		foreach($defaultModules as $file)
		{
			if(in_array(trim($file), $modules))
			{
				$index = array_search(trim($file), $modules);
				unset($modules[$index]);
			}
		}

		return $modules;
	}


	/**
	 * Returns the installed foreign languages.
	 *
	 * @return array
	 */
	public function getForeignModules()
	{
		$installedForeignModuleNames = array();

		if($this->_hasForeignModuleOrgamax())
		{
			$installedForeignModuleNames[] = 'Orgamax';
		}

		if($this->_hasForeignModuleEstelcoPdfCatalog())
		{
			$installedForeignModuleNames[] = 'PDF Katalog (Estelco)';
		}

		if($this->_hasForeignModuleJtlWawiConnector())
		{
			$installedForeignModuleNames[] = 'JTL Wawi Connector';
		}

		if($this->_hasForeignModuleCatenoShopsync())
		{
			$installedForeignModuleNames[] = 'Cateno Shopsync';
		}

		if($this->_hasForeignModuleOpenCart())
		{
			$installedForeignModuleNames[] = 'Offene Warenkörbe';
		}

		if($this->_hasForeignModuleIndivStyleBlog())
		{
			$installedForeignModuleNames[] = 'Blog (Indiv Style)';
		}

		if($this->_hasForeignModuleIndivStyleFreeProducts())
		{
			$installedForeignModuleNames[] = 'Gratisartikel (Indiv Style)';
		}
		if($this->_hasForeignModuleIndivStyleOpenCart())
		{
			$installedForeignModuleNames[] = 'Offene Warenkörbe (Indiv Style)';
		}

		return $installedForeignModuleNames;
	}


	/**
	 * Returns the installed foreign languages.
	 *
	 * @return array
	 */
	public function getForeignLanguages()
	{
		$difference = array();
		$languages  = '"' . implode('","', $this->defaultLanguages) . '"';
		$dbQuery    = $this->db->query('SELECT code FROM languages WHERE code NOT IN (' . $languages . ')');
		
		foreach($dbQuery->result_array() as $row)
		{
			$difference[] = $row['code'];
		}

		return $difference;
	}


	/**
	 * Returns an array with dangerous tools installed.
	 * @return array
	 */
	public function getDangerousTools()
	{
		$foundTools   = array();
		$shopRootPath = $this->settings->getValue('DIR_FS_CATALOG');

		foreach($this->dangerousToolsGlobs as $toolsGlob)
		{
			$files = glob($shopRootPath . $toolsGlob);
			
			if(is_array($files) && count($files) > 0)
			{
				foreach($files as $file)
				{
					$foundTools[] = substr($file, strlen($shopRootPath));
				}
			}
		}

		return $foundTools;
	}


	/**
	 * Determines if the installed PHP version is above the minimum version required.
	 * If true, the result is positive.
	 * @return bool
	 */
	public function isPhpVersionAboveMinimum()
	{
		$isAboveMinimum = version_compare($this->getPhpVersion(), $this->minPhpVersion, '>=');

		return $isAboveMinimum;
	}


	/**
	 * Determines if the installed PHP version is below the maximum version required.
	 * If true, the result is positive.
	 * @return bool
	 */
	public function isPhpVersionBelowMaximum()
	{
		$isBelowMaximum = version_compare($this->getPhpVersion(), $this->maxPhpVersion, '<=');

		return $isBelowMaximum;
	}


	/**
	 * Determines if the server is running on windows operating system.
	 * @return bool
	 */
	public function isUsingWindowsOs()
	{
		$windowsOsIndicator  = 'WIN';
		$uppercasedOsInfo    = strtoupper($this->getServerOs());
		$cutUppercasedOsInfo = substr($uppercasedOsInfo, 0, 3);

		$isUsingWindows = $cutUppercasedOsInfo === $windowsOsIndicator;

		return $isUsingWindows;
	}


	/**
	 * Determines if the installed MySQL version is above the minimum version required.
	 * If true, the result is positive.
	 * @return boolean
	 */
	public function isMySqlVersionAboveMinimum()
	{
		$isAboveMinimum = version_compare($this->getMysqlVersion(), $this->minMysqlVersion, '>=');

		return $isAboveMinimum;
	}


	/**
	 * Determines if the installed MySQL version is below the maximum version required.
	 * If true, the result is positive.
	 * @return bool
	 */
	public function isMySqlVersionBelowMaximum()
	{
		$isBelowMaximum = version_compare($this->getMysqlVersion(), $this->maxMysqlVersion, '<=');

		return $isBelowMaximum;
	}

}