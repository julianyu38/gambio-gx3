<?php
/* --------------------------------------------------------------
   CacheApiV2Controller.inc.php 2018-04-25
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

MainFactory::load_class('HttpApiV2Controller');

/**
 * Class CategoriesApiV2Controller
 *
 * Provides a gateway to the CategoryWriteService and CategoryReadService classes, which handle the shop category
 * resources.
 *
 * @category   System
 * @package    ApiV2Controllers
 */
class CacheApiV2Controller extends HttpApiV2Controller
{
    /**
     * @api        {delete} /cache/ Delete Cached data
     * @apiVersion 2.7.0
     * @apiName    DeleteCache
     * @apiGroup   Cache
     *
     * @apiDescription
     * All cached data is removed/renewed. This method will always return success.
     *
     * @apiExample {curl} Delete all cached data
     *             curl -X DELETE --user admin@shop.de:12345 http://shop.de/api.php/v2/cache
     *
     * @apiSuccessExample {json} Success-Response
     * {
     *   "code": 200,
     *   "status": "success",
     *   "action": "delete"
     * }
     */
	public function delete()
	{
		$coo_cache_control         = MainFactory::create_object('CacheControl');
		$coo_phrase_cache_builder  = MainFactory::create_object('PhraseCacheBuilder', array());
		$mailTemplatesCacheBuilder = MainFactory::create_object('MailTemplatesCacheBuilder');
		
		$coo_cache_control->clear_cache();
		$coo_cache_control->clear_content_view_cache();
		$coo_cache_control->clear_templates_c();
		$coo_cache_control->clear_template_cache();
		$coo_cache_control->clear_css_cache();
		$coo_cache_control->clear_expired_shared_shopping_carts();
		$coo_cache_control->remove_reset_token();
		$coo_cache_control->rebuild_feature_index();
		$coo_cache_control->rebuild_categories_submenus_cache();
		$coo_cache_control->rebuild_products_categories_index();
		$coo_cache_control->rebuild_products_properties_index();
		$coo_phrase_cache_builder->build();
		$mailTemplatesCacheBuilder->build();
		$coo_cache_control->clear_data_cache();
		
		$response = [
			'code'   => 200,
			'status' => 'success',
			'action' => 'delete'
		];
		
		$this->_writeResponse($response);
	}
}