<?php
/* --------------------------------------------------------------
 TrackingCodeApiV2Controller.inc.php 2018-01-15
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

class TrackingCodeApiV2Controller extends HttpApiV2Controller
{
	/**
	 * @var \ParcelTrackingCodeFactory
	 */
	private $factory;
	
	
	/**
	 * Initializes API Controller
	 */
	protected function init()
	{
		$this->factory = new ParcelTrackingCodeFactory();
	}
	
	
	/**
	 * @api        {get} /tracking_code Get parcel tracking codes
	 * @apiVersion 2.6.0
	 * @apiName    GetParcelTrackingCodes
	 * @apiGroup   TrackingCode
	 *
	 * @apiDescription
	 * Returns a list with all parcel tracking code entities. You can fetch a single resource by providing the tracking
	 * code id in the request URI.
	 *
	 * @apiExample {curl} Get all parcel tracking codes
	 *             curl --user admin@shop.de:12345 http://shop.de/api.php/v2/tracking_code
	 *
	 * @apiExample {curl} Get parcel tracking code with ID = 2
	 *             curl --user admin@shop.de:12345 http://shop.de/api.php/v2/tracking_code/2
	 *
	 * @apiSuccess Response-Body If successful, this method will return the parcel tracking code resource in JSON
	 *             format.
	 *
	 * @apiSuccessExample {json} Response-Body
	 * [
	 *  {
	 *      "id": "1",
	 *      "orderId": "400210",
	 *      "trackingCode": "",
	 *      "parcelServiceId": "0",
	 *      "parcelServiceName": "Parcel Service",
	 *      "languageId": "0",
	 *      "url": "http://custom-url.com",
	 *      "comment": "hello world",
	 *      "creationDate": "2018-01-15 18:09:34"
	 *  },
	 *  {
	 *      "id": "2",
	 *      "orderId": "400211",
	 *      "trackingCode": "",
	 *      "parcelServiceId": "0",
	 *      "parcelServiceName": "A Parcel Service",
	 *      "languageId": "0",
	 *      "url": "http://best-url.com",
	 *      "comment": "Custom comment",
	 *      "creationDate": "2018-01-15 18:09:52"
	 *  }
	 * ]
	 *
	 * @apiSuccessExample {json} Response-Body
	 * {
	 *      "id": "1",
	 *      "orderId": "400210",
	 *      "trackingCode": "",
	 *      "parcelServiceId": "0",
	 *      "parcelServiceName": "Parcel Service",
	 *      "languageId": "0",
	 *      "url": "http://custom-url.com",
	 *      "comment": "hello world",
	 *      "creationDate": "2018-01-15 18:09:34"
	 *  }
	 *
	 * @apiError (Error 4xx) 404-NotFound If no resource could be found by the provided id.
	 */
	public function get()
	{
		$finder = $this->factory->finder();
		
		if(!array_key_exists(1, $this->uri))
		{
			return $this->_writeResponse($this->_convertTrackingCodes($finder->getAll()));
		}
		
		if($this->uri[0] === 'tracking_code')
		{
			$trackingCodeId = ParcelTrackingCodeId::create($this->uri[1]);
			
			return $this->_writeResponse($this->_convertTrackingCode($finder->find($trackingCodeId)));
		}
		
		$orderId = ParcelTrackingCodeOrderId::create($this->uri[1]);
		
		return $this->_writeResponse($this->_convertTrackingCodes($finder->findByOrderId($orderId)));
	}
	
	
	/**
	 * @api             {post} /orders/:order_id/tracking_code Add Tracking Code
	 * @apiVersion      2.6.0
	 * @apiName         AddTrackingCode
	 * @apiGroup        Orders
	 *
	 * @apiDescription
	 * Adds a new parcel tracking code to the order resource.
	 *
	 * @apiParamExample {json} Request-Body
	 * {
	 *      "parcelServiceId": 3,
	 *      "trackingCode": "some-tracking-code"
	 * }
	 *
	 * @apiParamExample {json} Request-Body
	 * {
	 *      "parcelServiceName": "My Custom Parcel Service",
	 *      "url": "http://parcel-service-tracking-url.de?code=my-code"
	 * }
	 *
	 * @apiParamExample {json} Request-Body
	 * {
	 *      "parcelServiceName": "My Custom Parcel Service",
	 *      "url": "http://parcel-service-tracking-url.de?code=my-code",
	 *      "comment": "This is a custom comment"
	 * }
	 *
	 * @apiParam {int} parcelServiceId Id of parcel service, must be an existing parcel service ID.
	 * @apiParam {String} trackingCode Parcel tracking code of order.
	 * @apiParam {String} parcelServiceName Custom name of parcel service, used for lightweight entity.
	 * @apiParam {String} url Parcel tracking url of order.
	 * @apiParam {String} comment Optional comment for orders tracking code.
	 *
	 * @apiSuccess (Success 201) Response-Body If successful, this method returns a complete TrackingCode resource in
	 *             the response body.
	 *
	 * @apiError        400-BadRequest The body of the request was empty or invalid.
	 *
	 * @apiErrorExample Error-Response
	 * HTTP/1.1 400 Bad Request
	 * {
	 *   "code": 400,
	 *   "status": "error",
	 *   "message": "Tracking code data were not provided."
	 * }
	 */
	public function post()
	{
		$trackingCode   = $this->_deserialize(json_decode($this->api->request->getBody(), true));
		$trackingCodeId = $trackingCode->save();
		
		$this->_writeResponse($this->_convertTrackingCode($this->factory->finder()->find($trackingCodeId)), 201);
	}
	
	
	/**
	 * @api             {delete} /tracking_code/:id Delete tracking code
	 * @apiVersion      2.6.0
	 * @apiName         DeleteTrackingCode
	 * @apiGroup        TrackingCode
	 *
	 * @apiDescription
	 * Removes an order parcel tracking code entry from the database.
	 *
	 * @apiExample {curl} Delete Parcel Tracking Code
	 *                    curl -X DELETE --user admin@shop.de:12345 http://shop.de/api.php/v2/tracking_code/4
	 *
	 * @apiSuccessExample {json} Success-Response
	 * {
	 *      "code": 200,
	 *      "status": "success",
	 *      "action": "delete",
	 *      "resource": "TrackingCode",
	 *      "trackingCodeId": 4
	 * }
	 *
	 * @apiError        400-BadRequest The provided tracking code id was invalid or not found.
	 *
	 * @apiErrorExample Error-Response
	 * HTTP/1.1 400 Bad Request
	 * {
	 *      "code" 400,
	 *      "status": "error",
	 *      "message": Invalid tracking code id provided
	 * }
	 *
	 */
	public function delete()
	{
		if(!array_key_exists(1, $this->uri))
		{
			throw new HttpApiV2Exception('Invalid resource provided, tracking code id is missing in URI.', 400);
		}
		
		$this->factory->deleteService()->delete(ParcelTrackingCodeId::create($this->uri[1]));
		
		$this->_writeResponse([
			                      'code'           => 200,
			                      'status'         => 'success',
			                      'action'         => 'delete',
			                      'resource'       => 'TrackingCode',
			                      'trackingCodeId' => $this->uri[1],
		                      ]);
	}
	
	
	protected function _convertTrackingCodes(array $orderParcelTrackingCodes)
	{
		$data = [];
		
		foreach($orderParcelTrackingCodes as $orderParcelTrackingCode)
		{
			$data[] = $this->_convertTrackingCode($orderParcelTrackingCode);
		}
		
		return $data;
	}
	
	
	protected function _convertTrackingCode(array $orderParcelTrackingCode)
	{
		return [
			'id'                => $orderParcelTrackingCode['orders_parcel_tracking_code_id'],
			'orderId'           => $orderParcelTrackingCode['order_id'],
			'trackingCode'      => $orderParcelTrackingCode['tracking_code'],
			'parcelServiceId'   => $orderParcelTrackingCode['parcel_service_id'],
			'parcelServiceName' => $orderParcelTrackingCode['parcel_service_name'],
			'languageId'        => $orderParcelTrackingCode['language_id'],
			'url'               => $orderParcelTrackingCode['url'],
			'comment'           => $orderParcelTrackingCode['comment'],
			'creationDate'      => $orderParcelTrackingCode['creation_date'],
		];
	}
	
	
	protected function _deserialize(array $json)
	{
		$this->ensureJsonIsValid($json);
		
		if(array_key_exists('parcelServiceId', $json))
		{
			return $this->_deserializeEntity($json);
		}
		
		return $this->_deserializeLightweightEntity($json);
	}
	
	
	protected function ensureJsonIsValid(array $json)
	{
		$error         = 'Invalid Request-Body for REST-Operation provided! ';
		$specificDummy = 'The key "%s" is set, so the other required key in the request body is "%s" and optionally "%s".';
		$generalDummy  = 'If the key "%s" exists in the request body, the other required key is "%s" and optionally "%s".';
		
		if(array_key_exists('parcelServiceId', $json))
		{
			if(!array_key_exists('trackingCode', $json))
			{
				$error .= sprintf($specificDummy, 'parcelServiceId', 'trackingCode', 'languageId');
				throw new \InvalidArgumentException($error);
			}
			
			return;
		}
		
		if(array_key_exists('parcelServiceName', $json))
		{
			if(!array_key_exists('url', $json))
			{
				$error .= sprintf($specificDummy, 'parcelServiceName', 'url', 'comment');
				throw new \InvalidArgumentException($error);
			}
			
			return;
		}
		
		$error .= sprintf($generalDummy, 'parcelServiceId', 'trackingCode', 'languageId') . ' ';
		$error .= sprintf($generalDummy, 'parcelServiceName', 'url', 'comment');
		
		throw new \Exception($error);
	}
	
	
	protected function _deserializeEntity(array $json)
	{
		$parcelServiceId = ParcelTrackingCodeServiceId::create($json['parcelServiceId']);
		$orderId         = ParcelTrackingCodeOrderId::create($this->uri[1]);
		$trackingCode    = OrderParcelTrackingCode::create($json['trackingCode']);
		$languageId      = $this->determineLanguageId($json);
		
		return $this->factory->create($parcelServiceId, $orderId, $trackingCode, $languageId);
	}
	
	
	protected function _deserializeLightweightEntity(array $json)
	{
		$orderId           = ParcelTrackingCodeOrderId::create($this->uri[1]);
		$parcelServiceName = ParcelTrackingCodeServiceName::name($json['parcelServiceName']);
		$url               = ParcelTrackingCodeUrl::create($json['url']);
		$comment           = array_key_exists('comment',
		                                      $json) ? ParcelTrackingCodeComment::write($json['comment']) : null;
		
		return $this->factory->createLightweight($orderId, $parcelServiceName, $url, $comment);
	}
	
	
	protected function determineLanguageId(array $json)
	{
		if(array_key_exists('languageId', $json))
		{
			return ParcelTrackingCodeLanguageId::create($json['languageId']);
		}
		
		$languageProvider = MainFactory::create('LanguageProvider', StaticGXCoreLoader::getDatabaseQueryBuilder());
		
		return ParcelTrackingCodeLanguageId::create($languageProvider->getDefaultLanguageId());
	}
}