<?php
/* --------------------------------------------------------------
  GXModuleCenterModuleController.inc.php 2018-04-13
  Gambio GmbH
  http://www.gambio.de
  Copyright (c) 2018 Gambio GmbH
  Released under the GNU General Public License (Version 2)
  [http://www.gnu.org/licenses/gpl-2.0.html]
  --------------------------------------------------------------
*/

/**
 * Class GXModuleCenterModuleController
 * @extends    AbstractModuleCenterModuleController
 * @category   System
 * @package    Modules
 * @subpackage Controllers
 */
class GXModuleCenterModuleController extends AbstractModuleCenterModuleController
{
	/**
	 * Holds json data for all modules
	 *
	 * @var array $modules
	 */
	protected $modules = [];
	
	/**
	 * Configuration storage.
	 *
	 * @var GXModuleConfigurationStorage
	 */
	protected $configurationStorage;
	
	/**
	 * @var object UserConfiguration $userConfigurationService
	 */
	protected $userConfigurationService;
	
	
	/**
	 * Initialize the module e.g. set title, description, sort order etc.
	 *
	 * Function will be called in the constructor
	 */
	protected function _init()
	{
		$this->userConfigurationService = StaticGXCoreLoader::getService('UserConfiguration');
	}
	
	
	/**
	 * Returns an AdminPageHttpControllerResponse with configuration options or returns a
	 * RedirectHttpControllerResponse with specified redirect url.
	 *
	 * @return HttpControllerResponse
	 */
	public function actionDefault()
	{
		$config        = [];
		$sections      = [];
		$userId        = new IdType(0);
		$module        = str_replace('ModuleCenterModule', '', $this->_getQueryParameter('do'));
		$configuration = $this->_getGXModuleJSONConfiguration($module);
		
		$this->initConfiguration($module);
		
		$template = $this->getTemplateFile('module_center/gx_modules/gx_module_configuration.html');
		$title    = isset($configuration['title']) ? $this->getText($configuration['title']) : $module;
		$title    = new NonEmptyStringType($title);
		
		if(is_array($configuration['configuration']))
		{
			foreach($configuration['configuration'] as $section => $fields)
			{
				foreach($fields as $key => $value)
				{
					if($key === 'fields')
					{
						foreach($value as $field => $fieldvalues)
						{
							if(isset($fieldvalues['action']) && isset($fieldvalues['action']['message']))
							{
								$fieldvalues['action']['message'] = $this->getText($fieldvalues['action']['message']);
							}
							
							if(isset($fieldvalues['buttons']))
							{
								foreach($fieldvalues['buttons'] as &$button)
								{
									$button['text'] = $this->getText($button['text']);
									if(isset($button['action']['message']))
									{
										$button['action']['message'] = $this->getText($button['action']['message']);
									}
								}
							}
							
							$config[$section][$field] = $fieldvalues;
							
							$config[$section][$field]['label']           = isset($fieldvalues['label']) ? $this->getText($fieldvalues['label']) : '';
							$config[$section][$field]['title']           = isset($fieldvalues['title']) ? $this->getText($fieldvalues['title']) : '';
							$config[$section][$field]['description']     = isset($fieldvalues['description']) ? $this->getText($fieldvalues['description']) : '';
							$config[$section][$field]['text']            = isset($fieldvalues['text']) ? $this->getText($fieldvalues['text']) : '';
							$config[$section][$field]['tooltip']['text'] = isset($fieldvalues['tooltip']['text']) ? $this->getText($fieldvalues['tooltip']['text']) : '';
							
							switch($fieldvalues['type'])
							{
								case 'file':
									$config[$section][$field]['default_value'] = $fieldvalues['folder'];
									break;
								case 'select':
								case 'multiselect':
									$config[$section][$field]['selected'] = [];
									if(isset($fieldvalues['selected']))
									{
										$config[$section][$field]['selected'] = array_flip($fieldvalues['selected']);
									}
									
									foreach($fieldvalues['values'] as $index => $option)
									{
										$value      = $option['value'];
										$textphrase = $option['text'];
										
										$config[$section][$field]['values'][$value] = $this->getText($textphrase);
										unset($config[$section][$field]['values'][$index]);
									}
									break;
								case 'editor':
									$config[$section][$field]['editor_type'] = $this->userConfigurationService->getUserConfiguration($userId,
									                                                                                                 'editor-'
									                                                                                                 . $module
									                                                                                                 . '-'
									                                                                                                 . $field) ? : 'ckeditor';
									break;
								case 'countries':
									$config[$section][$field]['values'] = xtc_get_countries();
									break;
								case 'customer_group':
									$config[$section][$field]['values'] = xtc_get_customers_statuses();
									break;
								case 'order_status':
									$config[$section][$field]['values'] = xtc_get_orders_status();
									break;
								case 'languages':
									$config[$section][$field]['values'] = xtc_get_languages();
									break;
							}
							
							if($this->configurationStorage->get($field) !== false)
							{
								$config[$section][$field]['default_value'] = $this->configurationStorage->get($field);
								if($fieldvalues['type'] === 'multiselect')
								{
									$config[$section][$field]['selected'] = array_flip((array)$this->configurationStorage->get($field));
								}
							}
						}
					}
					elseif($key === 'title')
					{
						$sections[$section] = $this->getText($value);
					}
				}
			}
		}
		
		$config = [
			'active'        => $this->configurationStorage->get('active'),
			'form_action'   => xtc_href_link('admin.php',
			                                 'do=' . $this->_getQueryParameter('do') . '/SaveConfiguration'),
			'USE_WYSIWYG'   => USE_WYSIWYG,
			'sections'      => $sections,
			'configuration' => $config,
			'module_name'   => $module
		];
		
		$data = MainFactory::create('KeyValueCollection', $config);
		
		if(isset($configuration['config_url']))
		{
			return MainFactory::create('RedirectHttpControllerResponse', xtc_href_link($configuration['config_url']));
		}
		
		// suppress direct output
		ob_start();
		MainFactory::create('AdminLayoutHttpControllerResponse', $title, $template, $data, $this->_getAssets());
		$html = ob_get_clean();
		
		return MainFactory::create('HttpControllerResponse', $html);
	}
	
	
	/**
	 * @return \AssetCollection|bool
	 */
	protected function _getAssets()
	{
		$assets = MainFactory::create('AssetCollection');
		$assets->add(MainFactory::create('Asset', 'admin_buttons.lang.inc.php'));
		$assets->add(MainFactory::create('Asset', 'styles/legacy/global-colorpicker.css'));
		$assets->add(MainFactory::create('Asset', 'includes/ckeditor/ckeditor.js'));
		
		return $assets;
	}
	
	
	/**
	 * Saves the configuration.
	 *
	 * @return RedirectHttpControllerResponse Default page.
	 */
	public function actionSaveConfiguration()
	{
		$data      = $this->_getPostDataCollection();
		$module    = substr_replace($this->_getQueryParameter('do'), '', strpos($this->_getQueryParameter('do'), '/'));
		$namespace = str_replace('ModuleCenterModule', '', $module);
		
		$this->initConfiguration($namespace);
		$moduleStatusChanged = $this->configurationStorage->get('active') !== (int)$data->getValue('active');
		
		foreach($data as $key => $value)
		{
			if(is_array($value) && $key !== 'editor_identifiers')
			{
				$this->configurationStorage->set($key, json_encode(xtc_db_prepare_input($value)));
			}
			elseif($key !== 'editor_identifiers')
			{
				$this->configurationStorage->set($key, xtc_db_prepare_input($value));
			}
		}
		
		$this->process_uploads($data);
		
		if($moduleStatusChanged)
		{
			$cacheControl = MainFactory::create_object('CacheControl');
			$cacheControl->reset_cache('modules');
			@unlink(DIR_FS_CATALOG . 'cache/__dynamics.css');
		}
		
		return MainFactory::create('RedirectHttpControllerResponse', xtc_href_link('admin.php', 'do=' . $module));
	}
	
	
	/**
	 * Get the json configuration from GXModule.json file
	 *
	 * @param $name
	 *
	 * @return array|bool
	 */
	protected function _getGXModuleJSONConfiguration($name)
	{
		$gxModuleFiles = GXModulesCache::getFiles();
		
		foreach($gxModuleFiles as $file)
		{
			if(strpos($file, 'GXModule.json') !== false)
			{
				preg_match("/GXModules\/(.*)\/GXModule.json/", $file, $matches);
				$moduleData = json_decode(file_get_contents($file), true);
				if(str_replace('/', '', $matches[1]) === $name)
				{
					return $moduleData;
				}
			}
		}
		
		return false;
	}
	
	
	/**
	 * Loads the configuration from db for specific module
	 *
	 * @param $module string
	 */
	protected function initConfiguration($module)
	{
		$this->configurationStorage = MainFactory::create('GXModuleConfigurationStorage', $module);
	}
	
	
	/**
	 * Processing the uploaded files
	 *
	 * @param $data KayValueCollection
	 */
	protected function process_uploads($data)
	{
		foreach($_FILES as $file => $value)
		{
			if(!empty($value['tmp_name']))
			{
				move_uploaded_file($value['tmp_name'],
				                   DIR_FS_CATALOG . $data->getValue($file . '_folder') . '/' . $value['name']);
				$this->configurationStorage->set($file, $data->getValue($file . '_folder') . '/' . $value['name']);
			}
		}
	}
	
	
	/**
	 * Returns the translated text for the given section phrase selector (i.e. "buttons.ok" results in "Ok")
	 *
	 * @param $sectionPhraseSelector
	 *
	 * @return string
	 */
	protected function getText($sectionPhraseSelector)
	{
		if(is_string($sectionPhraseSelector) && substr_count($sectionPhraseSelector, '.') === 1)
		{
			$sectionPhrase = explode('.', $sectionPhraseSelector);
			
			return $this->languageTextManager->get_text($sectionPhrase[1], $sectionPhrase[0],
			                                            $_SESSION['languages_id']);
		}
		
		return $sectionPhraseSelector;
	}
}