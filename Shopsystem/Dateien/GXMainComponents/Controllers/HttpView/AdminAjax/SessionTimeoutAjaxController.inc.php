<?php
/* --------------------------------------------------------------
   SessionTimeoutAjaxController.inc.php 2016-11-10
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

MainFactory::load_class('AdminHttpViewController');

/**
 * Class SessionTimeoutAjaxController
 *
 * @category System
 * @package  AdminHttpViewControllers
 */
class SessionTimeoutAjaxController extends AdminHttpViewController
{
	/**
	 * @return JsonHttpControllerResponse
	 */
	public function actionDefault()
	{
		$sessionMaxLifetime = @(int)ini_get('session.gc_maxlifetime');
		
		$serverSetting = $sessionMaxLifetime !== 0 ? $sessionMaxLifetime : 1440;
		$userSetting   = (int)SESSION_TIMEOUT * 60;
		
		if(array_key_exists('last_activity', $_SESSION)
		   && $_SERVER['REQUEST_TIME'] - $_SESSION['last_activity'] > $userSetting
		)
		{
			session_unset();
			session_destroy();
			
			return MainFactory::create('JsonHttpControllerResponse', ['logout' => true]);
		}
		
		if($serverSetting < $userSetting)
		{
			$x = $userSetting;
			$y = $serverSetting;
			
			while($y !== 0)
			{
				$tmp = $x % $y;
				$x   = $y;
				$y   = $tmp;
			}
			
			if($x === $serverSetting)
			{
				$timeout = ($x - .5) * 1000;
			}
			else
			{
				$timeout = ($x + .5) * 1000;
			}
		}
		else
		{
			$timeout = ($userSetting + .5) * 1000;
		}
		
		return MainFactory::create('JsonHttpControllerResponse',
		                           ['data' => [(int)$timeout, $_SESSION['last_activity']]]);
	}
}