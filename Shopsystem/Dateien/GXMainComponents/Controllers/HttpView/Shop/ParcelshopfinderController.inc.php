<?php

/* --------------------------------------------------------------
	ParcelshopfinderController.inc.php 2017-07-14
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Class ParcelshopfinderController
 *
 * @extends    HttpViewController
 * @category   System
 * @package    HttpViewControllers
 */
class ParcelshopfinderController extends HttpViewController
{
	/**
	 * @var ConfigurationStorage
	 */
	protected $configuration;
	
	/**
	 * @var LanguageTextManager
	 */
	protected $languageTextManager;
	
	
	protected function init()
	{
		$this->languageTextManager = MainFactory::create('LanguageTextManager', 'parcelshopfinder',
		                                                 $_SESSION['languages_id']);
		$this->configuration       = MainFactory::create('ConfigurationStorage', 'modules/shipping/parcelshopfinder');
		$this->contentView->set_template_dir(DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/module/');
	}
	
	
	public function actionDefault()
	{
		if(empty($_SESSION['customer_id']))
		{
			return MainFactory::create('RedirectHttpControllerResponse', xtc_href_link(FILENAME_DEFAULT, '', 'SSL'));
		}
		
		$street  = $this->_getQueryParameter('street');
		$house   = $this->_getQueryParameter('house');
		$zip     = $this->_getQueryParameter('zip');
		$city    = $this->_getQueryParameter('city');
		$country = $this->_getQueryParameter('country');
		$country = strtoupper(substr(trim($country), 0, 2));
		$filter  = $this->_getQueryParameter('filter');
		$filter  = in_array($filter, ['packstations', 'offices', 'both'], true) ? $filter : 'both';
		
		/*
		officially supported countries:
		Austria, Belgium, Czech Republic, Germany, Netherlands, Poland, Slovakia
		*/
		$db                 = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$supportedCountries = ['AT', 'BE', 'CZ', 'DE', 'NL', 'PL', 'SK'];
		$db->where(sprintf('countries_iso_code_2 IN (\'%s\')', implode('\', \'', $supportedCountries)));
		$countries = $db->get_where('countries', ['status' => '1'])->result_array();
		
		if(empty($country))
		{
			$defaultAddress = $db->get_where('address_book',
			                                 [
			                                 	'address_book_id' => $_SESSION['customer_default_address_id']
											 ])->row();
			$countryService = StaticGXCoreLoader::getService('Country');
			$addressCountry = $countryService->getCountryById(MainFactory::create('IdType',
			                                                                      (int)$defaultAddress->entry_country_id));
			if(empty($defaultAddress->entry_house_number))
			{
				$splitStreet = $this->splitStreet($defaultAddress->entry_street_address);
			}
			else
			{
				$splitStreet = [
					'street' => $defaultAddress->entry_street_address, 'house_no' => $defaultAddress->entry_house_number
				];
			}
			$street      = $splitStreet['street'];
			$house       = $splitStreet['house_no'];
			$zip         = $defaultAddress->entry_postcode;
			$city        = $defaultAddress->entry_city;
			$country     = $addressCountry->getIso2();
			$redirectUrl = xtc_href_link(
				'shop.php',
				http_build_query(
					[
						'do'               => 'Parcelshopfinder',
						'checkout_started' => ($this->_getQueryParameter('checkout_started') == 1) ? '1' : '0',
						'street'           => $street,
						'house'            => $house, 'zip' => $zip,
						'city'             => $city,
						'country'          => (string)$country,
						'filter'           => $filter,
					]
				),
				'SSL',
				true,
				true,
				false,
				false
			);
			
			return MainFactory::create('RedirectHttpControllerResponse', $redirectUrl);
		}

		$tplData = [
			'form_action'      => xtc_href_link('shop.php', '', 'SSL'),
			'do'               => 'Parcelshopfinder',
			'street'           => $street,
			'house'            => $house,
			'zip'              => $zip,
			'city'             => $city,
			'country'          => $country,
			'filter'           => $filter,
			'countries'        => $countries,
			'checkout_started' => $this->_getQueryParameter('checkout_started') == 1
				? '1'
				: '0',
			'search_result'    => $this->_getSearchResultOutput($country, $city, $zip, $house, $street, $filter),
		];
		
		$mainContent = $this->_render('parcelshopfinder.html', $tplData);
		
		$GLOBALS['breadcrumb']->add($this->languageTextManager->get_text('parcelshopfinder_breadcrumb'),
		                            xtc_href_link('shop.php', xtc_get_all_get_params(['do']) . '&do=Parcelshopfinder',
		                                          'SSL'));
		
		$layoutContentControl = MainFactory::create_object('LayoutContentControl');
		$layoutContentControl->set_data('GET', $this->_getQueryParametersCollection()->getArray());
		$layoutContentControl->set_data('POST', $this->_getPostDataCollection()->getArray());
		$layoutContentControl->set_('coo_breadcrumb', $GLOBALS['breadcrumb']);
		$layoutContentControl->set_('coo_product', $GLOBALS['product']);
		$layoutContentControl->set_('coo_xtc_price', $GLOBALS['xtPrice']);
		$layoutContentControl->set_('c_path', $GLOBALS['cPath']);
		$layoutContentControl->set_('main_content', $mainContent);
		$layoutContentControl->set_('request_type', $GLOBALS['request_type']);
		$layoutContentControl->proceed();
		
		$redirectUrl = $layoutContentControl->get_redirect_url();
		if(!empty($redirectUrl))
		{
			return MainFactory::create('RedirectHttpControllerResponse', $redirectUrl);
		}
		
		return MainFactory::create('HttpControllerResponse', $layoutContentControl->get_response());
	}
	
	
	protected function _getSearchResultOutput($country, $city, $zip, $house, $street, $filter = 'both')
	{
		$firstname  = $this->_getQueryParameter('firstname')
			? : $_SESSION['customer_first_name'];
		$lastname   = $this->_getQueryParameter('lastname')
			? : $_SESSION['customer_last_name'];
		$postnumber = $this->_getQueryParameter('postnumber')
			? : '';
		$postnumber = preg_replace('/[^\d]/', '', $postnumber);
		$error      = $this->_getQueryParameter('error');
		
		if(empty($country))
		{
			return '';
		}

		$countries = [
			'AT' => 'Austria',
			'BE' => 'Belgium',
			'CZ' => 'Czech Republic',
			'DE' => 'Germany',
			'NL' => 'Netherlands',
			'PL' => 'Poland',
			'SK' => 'Slovakia'
		];
		if(array_key_exists($country, $countries))
		{
			$country = $countries[$country];
		}

		$address          = sprintf('%s %s %s %s %s', $zip, $city, $street, $house, $country);
		$address_readable = '';
		$address_readable .= !empty($street) ? $street . ' ' . $house . ', ' : '';
		$address_readable .= $zip . ' ' . $city . ' (' . $country . ')';
		$psf              = MainFactory::create('ParcelShopFinder');
		$psfList          = $psf->getParcelLocationByAddress($address);
		$psfList          = $this->applyFilter($psfList, $filter);
		
		if(is_array($psfList))
		{
			$maximumListEntries = max(10, (int)$this->configuration->get('maximum_list_entries'));
			$psfList            = array_slice($psfList, 0, $maximumListEntries);
		}
		
		if(!empty($error))
		{
			if($error === 'invalid_postnumber')
			{
				$errorMessage = $this->languageTextManager->get_text('error_invalid_postnumber');
			}
		}

		$tplData = [
			'error_message' => $errorMessage ?: '',
			'customer' => [
				'firstname' => $firstname,
				'lastname' => $lastname,
				'postnumber' => $postnumber,
			],
			'search' => base64_encode(serialize([$street, $house, $zip, $city, $country])),
			'address' => $address_readable,
			'country' => $country,
			'filter'  => $filter,
			'psflist' => $psfList,
			'oo_translation' => [
				'mo' => $this->languageTextManager->get_text('openinghours_day_mo'),
				'tu' => $this->languageTextManager->get_text('openinghours_day_tu'),
				'we' => $this->languageTextManager->get_text('openinghours_day_we'),
				'th' => $this->languageTextManager->get_text('openinghours_day_th'),
				'fr' => $this->languageTextManager->get_text('openinghours_day_fr'),
				'sa' => $this->languageTextManager->get_text('openinghours_day_sa'),
				'su' => $this->languageTextManager->get_text('openinghours_day_su'),
				'dash' => '-',
				'|' => $this->languageTextManager->get_text('openinghours_and'),
			],
			'google_map_type' => $this->configuration->get('google_map_type'),
			'googleApiKey' => $this->configuration->get('google_api_key'),
			'form_action_new_ab_entry' => xtc_href_link('shop.php', 'do=Parcelshopfinder/AddAddressBookEntry', 'SSL'),
			'backlink' => xtc_href_link('shop.php', 'do=Parcelshopfinder', 'SSL'),
			'checkout_started' => ($this->_getQueryParameter('checkout_started') == 1) ? '1' : '0',
		];
		if($tplData['google_map_type'] === 'static')
		{
			$tplData['mapUrl'] = $this->makeMapUrl($psfList);
		}
		if($tplData['google_map_type'] === 'dynamic')
		{
			$tplData['markerData'] = $this->makeMarkersJson($psfList);
		}
		$tplData['searchAddress'] = sprintf('%s %s, %s %s, %s', $street, $house, $zip, $city, $country);
		
		$mainContent = $this->_render('parcelshopfinder_result.html', $tplData);
		
		return $mainContent;
	}
	
	protected function applyFilter(array $psfList, $filter = 'both')
	{
		$outList = [];
		if($filter === 'both')
		{
			$outList = $psfList;
		}
		else
		{
			$shopTypes = [];
			if($filter === 'offices')
			{
				$shopTypes = ['parcelShop', 'postOffice'];
			}
			elseif($filter === 'packstations')
			{
				$shopTypes = ['packStation'];
			}
			foreach($psfList as $psfEntry) {
				if(in_array($psfEntry->shopType, $shopTypes))
				{
					$outList[] = $psfEntry;
				}
			}
		}
		return $outList;
	}


	public function actionAddAddressBookEntry()
	{
		if(empty($_SESSION['customer_id']))
		{
			return MainFactory::create('RedirectHttpControllerResponse', xtc_href_link(FILENAME_DEFAULT, '', 'SSL'));
		}
		
		if($_SERVER['REQUEST_METHOD'] === 'POST')
		{
			$firstname        = $this->_getPostData('firstname');
			$lastname         = $this->_getPostData('lastname');
			$postnumber       = $this->_getPostData('postnumber');
			$street_address   = $this->_getPostData('street_address');
			$house_number     = $this->_getPostData('house_number');
			$additional_info  = $this->_getPostData('additional_info');
			$postcode         = $this->_getPostData('postcode');
			$city             = $this->_getPostData('city');
			$country_iso2     = $this->_getPostData('country');
			$country_iso2     = strtolower(substr(trim($country_iso2), 0, 2));
			$checkout_started = $this->_getPostData('checkout_started') == 1;
			
			$addressClass = 'psf/undefined';
			if($country_iso2 == 'de')
			{
				if(stripos($street_address, 'packstation') !== false)
				{
					$addressClass = 'packstation_2';
				}
				if(stripos($street_address, 'filiale') !== false)
				{
					$addressClass = 'postfiliale_2';
				}
				if(stripos($street_address, 'paketshop') !== false || stripos($street_address, 'parcel shop') !== false)
				{
					$addressClass = 'postfiliale_2';
				}
				
				$postnumber = abs(filter_var($postnumber, FILTER_SANITIZE_NUMBER_INT));
				if($postnumber == 0 || $this->isValidPostnummer($postnumber) !== true)
				{
					$search    = unserialize(base64_decode($this->_getPostData('search')));
					$psfParams = [
						'street'     => $search[0], 'house' => $search[1], 'zip' => $search[2], 'city' => $search[3],
						'country'    => $search[4], 'firstname' => $firstname, 'lastname' => $lastname,
						'postnumber' => $postnumber, 'additional_info' => $additional_info,
						'error'      => 'invalid_postnumber',
					];
					
					return MainFactory::create('RedirectHttpControllerResponse', xtc_href_link('shop.php',
					                                                                           'do=Parcelshopfinder&'
					                                                                           . http_build_query($psfParams),
					                                                                           'SSL', true, true, false,
					                                                                           false));
				}
				$additional_info = sprintf('Postnummer %s', $postnumber);
			}
			else
			{
				$postnumber   = '';
				$addressClass = 'parcelshop';
			}
			
			$country     = $this->findCountryByIso2($country_iso2);
			$countryZone = MainFactory::create('CustomerCountryZone', new IdType(0),
			                                   MainFactory::create('CustomerCountryZoneName', ''),
			                                   MainFactory::create('CustomerCountryZoneIsoCode', ''));
			
			$addressBlock = MainFactory::create('AddressBlock', MainFactory::create('CustomerGender', ''),
			                                    MainFactory::create('CustomerFirstname', $firstname),
			                                    MainFactory::create('CustomerLastname', $lastname),
			                                    MainFactory::create('CustomerCompany', ''),
			                                    MainFactory::create('CustomerB2BStatus',
			                                                        (bool)$_SESSION['customer_b2b_status']),
			                                    MainFactory::create('CustomerStreet', $street_address),
			                                    MainFactory::create('CustomerHouseNumber', $house_number),
			                                    MainFactory::create('CustomerAdditionalAddressInfo', $additional_info),
			                                    MainFactory::create('CustomerSuburb', ''),
			                                    MainFactory::create('CustomerPostcode', $postcode),
			                                    MainFactory::create('CustomerCity', $city), $country, $countryZone);
			
			$customerService    = StaticGXCoreLoader::getService('Customer');
			$customer           = $customerService->getCustomerById(MainFactory::create('IdType',
			                                                                            $_SESSION['customer_id']));
			$addressBookService = StaticGXCoreLoader::getService('AddressBook');
			$newAddress         = $addressBookService->createNewAddress($addressBlock, $customer);
			$newAddress->setAddressClass(MainFactory::create('AddressClass', $addressClass));
			$addressBookService->updateCustomerAddress($newAddress);
			if($checkout_started === true)
			{
				$_SESSION['sendto'] = $newAddress->getId();
			}
		}
		
		if($checkout_started === true)
		{
			return MainFactory::create('RedirectHttpControllerResponse',
			                           xtc_href_link('checkout_shipping.php', '', 'SSL'));
		}
		else
		{
			return MainFactory::create('RedirectHttpControllerResponse', xtc_href_link('address_book.php', '', 'SSL'));
		}
	}
	
	
	public function actionValidatePostnumber()
	{
		$postnumber = $this->_getQueryParameter('postnumber');
		$result     = [
			'postnumberIsValid' => !empty($postnumber) && is_numeric($postnumber)
			                       && $this->isValidPostnummer($postnumber),
		];
		
		return MainFactory::create('JsonHttpControllerResponse', $result);
	}
	
	
	/* ================================================================================================================================= */
	
	/**
	 * checks validity of a DHL post number
	 *
	 * @param $postnum string post number (up to 10 digits)
	 *
	 * @return bool true if $postnum represents a (syntactically) valid post number
	 */
	public function isValidPostnummer($postnum)
	{
		$postnum = sprintf('%010d', $postnum);
		$sum1    = 0;
		for($i = 8; $i >= 0; $i -= 2)
		{
			$sum1 += $postnum[$i];
		}
		$sum2 = 0;
		for($j = 7; $j >= 1; $j -= 2)
		{
			$sum2 += $postnum[$j];
		}
		$sum12    = ($sum1 * 4) + ($sum2 * 9);
		$checknum = (10 - ($sum12 % 10)) % 10;
		$is_valid = $postnum[9] == $checknum;
		
		return $is_valid;
	}
	
	
	/**
	 * finds a country by its 2-letter ISO code
	 *
	 * @todo To be refactored as soon as CountryService::findCountryByIso2() becomes available
	 *
	 * @param $iso2 string 2-letter ISO code
	 *
	 * @return CustomerCountry
	 */
	protected function findCountryByIso2($iso2)
	{
		$iso2       = strtolower(substr($iso2, 0, 2));
		$db         = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$countryRow = $db->get_where('countries', ['countries_iso_code_2' => $iso2])->row();
		if($countryRow === null)
		{
			throw new Exception('Invalid country code');
		}
		$countries_id   = $countryRow->countries_id;
		$countryService = StaticGXCoreLoader::getService('Country');
		$country        = $countryService->getCountryById(MainFactory::create('IdType', $countries_id));
		
		return $country;
	}
	
	
	protected function makeMarkersJson($psfList)
	{
		$iconBaseUrl = GM_HTTP_SERVER . DIR_WS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/assets/images/icons/';
		$iconPackstation = $iconBaseUrl . 'packstation.png';
		$iconFiliale     = $iconBaseUrl . 'postfiliale.png';
		$iconPaketshop   = $iconBaseUrl . 'paketshop.png';

		$mapLabels   = [
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
			'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'α', 'β', 'γ', 'δ', 'ε', 'φ', 'γ',
			'ψ', 'ι', 'θ', 'κ', 'λ', 'μ', 'ν', 'π', 'ϕ', 'ρ', 'σ', 'τ', 'ω', '•', '•', '•', '•', '•', '•', '•', '•',
			'•', '•', '•', '•', '•', '•', '•', '•', '•', '•', '•', '•', '•', '•', '•', '•', '•', '•',
		];
		$markersJson = [];
		$labelIndex  = -1;
		$lastLat     = 0;
		$lastLng     = 0;
		foreach($psfList as $psfIndex => $psfEntry)
		{
			$title = '';
			$icon  = '';

			if($psfEntry->keyWord === 'Postfiliale' || $psfEntry->keyWord === 'DHL Paketshop' || $psfEntry->keyWord === 'DHL ParcelShop') {
				$title .= 'Filiale' . ' ' . $psfEntry->primaryKeyZipRegion;
			} elseif($psfEntry->keyWord === 'DHL Packstation') {
				$title .= 'Packstation' . ' ' . $psfEntry->primaryKeyZipRegion;
			}

			if(!empty($psfEntry->shopName)) {
				$title .= ' (' . $psfEntry->shopName . ')';
			}

			if($psfEntry->shopType === 'parcelShop') {
				$icon = $iconPaketshop;
			} elseif ($psfEntry->shopType === 'postOffice') {
				$icon = $iconFiliale;
			} elseif ($psfEntry->shopType === 'packStation') {
				$icon = $iconPackstation;
			}

			if($psfEntry->geoPosition->latitude === $lastLat && $psfEntry->geoPosition->longitude === $lastLng)
			{
				$psfEntry->geoPosition->latitude -= 0.00015;
				$psfEntry->geoPosition->longitude += 0.0002;
			}

			$labelIndex++;

			if(!array_key_exists($labelIndex, $mapLabels))
			{
				break;
			}

			$markersJson[] = [
				'position' => [
					'lat' => $psfEntry->geoPosition->latitude,
					'lng' => $psfEntry->geoPosition->longitude
				],
				'title' => $title,
				'label' => $mapLabels[$labelIndex],
				'icon' => $icon,
			];

			$psfEntry->mapMarkerLabel = $mapLabels[$labelIndex];
			$lastLat                  = $psfEntry->geoPosition->latitude;
			$lastLng                  = $psfEntry->geoPosition->longitude;
		}
		
		return json_encode($markersJson);
	}
	
	
	protected function makeMapUrl($psfList)
	{
		$googleApiKey    = $this->configuration->get('google_api_key');
		$googleUrlSecret = $this->configuration->get('google_url_signature_secret');
		
		if(empty($googleApiKey) || empty($googleUrlSecret))
		{
			return false;
		}
		
		$googleUrlSecret = strtr($googleUrlSecret, ['-' => '+', '_' => '/']);
		$googleUrlSecret = base64_decode($googleUrlSecret);
		
		$mapLabels  = [
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
			'V', 'W', 'X', 'Y', 'Z'
		];
		$mapMarkers = [];
		
		foreach($psfList as $psfIndex => $psfEntry)
		{
			if(array_key_exists($psfIndex, $mapLabels))
			{
				$mapMarkers[] = [
					'color' => 'yellow',
					'label' => $mapLabels[$psfIndex],
					'lat'   => $psfEntry->geoPosition->latitude,
					'lon'   => $psfEntry->geoPosition->longitude,
				];
				
				$psfEntry->mapMarkerLabel = $mapLabels[$psfIndex];
			}
			else
			{
				break;
			}
		}
		
		$mapMarkersArray       = array_map(function ($markerData)
		{
			return 'markers=' . rawurlencode(sprintf('color:%s|label:%s|%s,%s', $markerData['color'],
			                                         $markerData['label'], $markerData['lat'], $markerData['lon']));
		}, $mapMarkers);
		$mapMarkersQueryString = implode('&', $mapMarkersArray);
		
		$mapParameters = [
			'zoom' => '13', 'size' => '600x600', 'maptype' => 'roadmap', 'key' => $googleApiKey,
		];
		$mapUrl        = sprintf('%s?%s&%s', '/maps/api/staticmap', http_build_query($mapParameters),
		                         $mapMarkersQueryString);
		$signature     = hash_hmac('sha1', $mapUrl, $googleUrlSecret, true);
		$signature     = base64_encode($signature);
		$signature     = strtr($signature, ['+' => '-', '/' => '_']);
		$mapUrl        = 'https://maps.googleapis.com' . $mapUrl . '&signature=' . $signature;
		
		return $mapUrl;
	}
	
	/* ================================================================================================================================= */
	
	/**
	 * Heuristically splits up a street address into its component street name and house number
	 *
	 * @param  string
	 *
	 * @return array with keys 'street' and 'house_no'
	 */
	protected function splitStreet($street_address)
	{
		$street_address = trim($street_address);
		$splitStreet    = [
			'street' => $street_address, 'house_no' => '',
		];
		$matches        = [];
		if(preg_match('_^(\d.*?)\s(.+)_', $street_address, $matches) === 1)
		{
			$splitStreet['street']   = $matches[2];
			$splitStreet['house_no'] = $matches[1];
		}
		else if(preg_match('_(.+?)\s?(\d.*)_', $street_address, $matches) === 1)
		{
			$splitStreet['street']   = $matches[1];
			$splitStreet['house_no'] = $matches[2];
		}
		
		return $splitStreet;
	}
}
