<?php
/* --------------------------------------------------------------
	GeschaeftskundenversandController.inc.php 2018-06-01
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Class GeschaeftskundenversandController
 * 
 * @extends    AdminHttpViewController
 * @category   System
 * @package    AdminHttpViewControllers
 */
class GeschaeftskundenversandController extends AdminHttpViewController
{
	/**
	 * @var LanguageTextManager
	 */
	protected $languageTextManager;
	
	/**
	 * @var GeschaeftskundenversandConfigurationStorage
	 */
	protected $configuration;
	
	/**
	 * @var GeschaeftskundenversandLogger
	 */
	protected $logger;
	
	
	/**
	 * Override "proceed" method of parent and use it for initialization.
	 *
	 * @param HttpContextInterface $httpContext
	 */
	public function proceed(HttpContextInterface $httpContext)
	{
		$this->logger = MainFactory::create('GeschaeftskundenversandLogger');
		$this->contentView->set_template_dir(DIR_FS_ADMIN . 'html/content/');
		$this->languageTextManager = MainFactory::create('LanguageTextManager', 'module_center_module',
		                                                 $_SESSION['languages_id']);
		$this->configuration       = MainFactory::create('GeschaeftskundenversandConfigurationStorage');
		
		parent::proceed($httpContext);
	}
	
	
	public function actionPrepareLabel()
	{
		require_once DIR_FS_ADMIN . 'includes/classes/order.php';
		$orders_id = (int)$this->_getQueryParameter('oID');
		$order     = new order($orders_id);
		$orderReadService = StaticGXCoreLoader::getService('OrderRead');
		$storedOrder = $orderReadService->getOrderById(new IdType($orders_id));
		$orderTotals = $storedOrder->getOrderTotals();
		$orderTotalValue = 0;
		foreach($orderTotals as $orderTotal)
		{
			if($orderTotal->getClass() === 'ot_total') {
				$orderTotalValue = $orderTotal->getValue();
			}
		}
		$isAdultContent = false;
		$orderReadService = StaticGXCoreLoader::getService('OrderRead');
		$productReadService = StaticGXCoreLoader::getService('ProductRead');
		$storedOrder = $orderReadService->getOrderById(new IdType($orders_id));
		foreach($storedOrder->getOrderItems() as $orderItem)
		{
			$productId = $orderItem->getAddonValue(new StringType('productId'));
			try {
                $product   = $productReadService->getProductById(new IdType($productId));
                if($product->isFsk18()) {
                    $isAdultContent = true;
                    break;
                }
            } catch (UnexpectedValueException $e) {
			    // probably a product imported from elsewhere; just ignore it.
            }
		}
		
		$customerReadService = StaticGXCoreLoader::getService('CustomerRead');
		$customer            = $customerReadService->findCustomerByEmail(new CustomerEmail($order->customer['email_address']));
		$customersDob        = ($customer instanceof CustomerInterface)
			? $customer->getDateOfBirth()->format('Y-m-d')
			: '1900-01-01';
		$customersDob        = $customersDob === '1000-01-01' ? '1900-01-01' : $customersDob;

		$productNames = [];
		$allTypes = array_merge(
			GeschaeftskundenversandProduct::getValidTypes(),
			GeschaeftskundenversandProduct::getDeprecatedTypes()
		);
		foreach($allTypes as $productType)
		{
			$productNames[$productType] = $this->languageTextManager->get_text('gkv_product_' . $productType);
		}
		
		if(empty($order->delivery['house_number']))
		{
			$splitStreet = $this->splitStreet($order->delivery['street_address']);
			$street      = $splitStreet['street'];
			$houseNumber = $splitStreet['house_no'];
		}
		else
		{
			$street      = $order->delivery['street_address'];
			$houseNumber = $order->delivery['house_number'];
		}
		
		$packstationNumber = stripos($street, 'packstation') !== false
			? $houseNumber
			: '';
		$postfilialNumber  = stripos($street, 'postfiliale') !== false
			? $houseNumber
			: '';
		$parcelshopNumber  = stripos($street, 'parcelshop') !== false
			? $houseNumber
			: '';
		$parcelshopNumber  = empty($parcelshopNumber) && stripos($street, 'paketshop') !== false
			? $houseNumber
			: '';
		if($order->delivery['country_iso_code_2'] === $this->configuration->get('shipper/origincountry'))
		{
			if(empty($postfilialNumber) && !empty($parcelshopNumber))
			{
				$postfilialNumber = $parcelshopNumber;
			}
			$parcelshopNumber = '';
		}
		
		$postnumberMatches = [];
		if(preg_match('/^(?P<name>.+)\s*\/\s*(?P<postnumber>\d+)\s*$/', $order->delivery['name'], $postnumberMatches)
		   === 1
		)
		{
			$name       = $postnumberMatches['name'];
			$postnumber = $postnumberMatches['postnumber'];
		}
		else
		{
			$postnumber = '';
			$name       = $order->delivery['name'];
			if(preg_match('/.*(packstation|parcelshop|dhl paketshop|postfiliale).*/i',
			              $order->delivery['street_address']) === 1
			)
			{
				if(is_numeric($order->delivery['additional_address_info']))
				{
					$postnumber = $order->delivery['additional_address_info'];
				}
				else if(preg_match('/^postnummer (\d{6,10})/i', $order->delivery['additional_address_info'],
				                   $postnumberMatches) === 1
				)
				{
					$postnumber = $postnumberMatches[1];
				}
			}
		}
		
		$isExport = $order->delivery['country_iso_code_2'] !== $this->configuration->get('shipper/origincountry');
		$products = $this->configuration->getProducts();
		if(count($products) === 0)
		{
			$configUrl = xtc_href_link('admin.php', 'do=GeschaeftskundenversandModuleCenterModule');
			return new RedirectHttpControllerResponse($configUrl);
		}
		foreach($products as $productsIdx => $product)
		{
			if($isExport && $product->getTargetArea() !== 'domestic')
			{
				$productsDefault = $productsIdx;
				break;
			}
			if(!$isExport && $product->getTargetArea() === 'domestic')
			{
				$productsDefault = $productsIdx;
				break;
			}
		}
		
		$premiumPreselect = false;
		if($isExport === true)
		{
			if($this->configuration->get('intlpremium') === 'always')
			{
				$premiumPreselect = true;
			}
			elseif($this->configuration->get('intlpremium') === 'eu-only')
			{
				$euCountries = [
					'be', 'gr', 'mt', 'sk', 'bg', 'ie', 'nl', 'si', 'dk', 'it', 'at', 'es', 'de', 'hr', 'pl', 'cz',
					'ee', 'lv', 'pt', 'hu', 'fi', 'lt', 'ro', 'gb', 'fr', 'lu', 'se', 'cy',
				];
				$deliveryCountry = strtolower($order->delivery['country_iso_code_2']);
				if(in_array($deliveryCountry, $euCountries, true)) {
					$premiumPreselect = true;
				}
			}
		}

		$transportConditions = $this->getTransportConditions($orders_id);
		$prefillAllowed = ($transportConditions === 'accepted' || $transportConditions === 'unshown');
		$today = new DateTime('now', new DateTimeZone('Europe/Berlin'));

		$shippingWeight = (float)$order->info['total_weight'];
		if((bool)$this->configuration->get('add_packing_weight') === true)
		{
			$packingWeight = max((float)@constant('SHIPPING_BOX_WEIGHT'), $shippingWeight * ((float)@constant('SHIPPING_BOX_PADDING') / 100));
			$shippingWeight += $packingWeight;
		}
		$shippingWeight = max(0.1, $shippingWeight);
		$productsWeight = [];
		$db = StaticGXCoreLoader::getDatabaseQueryBuilder();
		foreach($order->products as $productsIndex => $product)
        {
            $productRow = $db->get_where('products', ['products_id' => $product['id']])->row();
            $productsWeight[$productsIndex] = $productRow->products_weight;
            
            $propertiesRow = $db->from('orders_products_properties')
                                ->select('orders_products_properties.products_properties_combis_id, combi_weight')
                                ->join('products_properties_combis',
                                       'orders_products_properties.products_properties_combis_id = products_properties_combis.products_properties_combis_id')
                                ->where('orders_products_id', (int)$product['opid'])
                                ->group_by('orders_products_properties.products_properties_combis_id, combi_weight')
                                ->get()->row();
            $productsWeight[$productsIndex] += $propertiesRow->combi_weight;
            
            $attributesQuery = $db->from('orders_products_attributes')
                                  ->join('products_attributes',
                                         'products_attributes.products_id = ' . (int)$product['id'] . ' AND ' .
                                         'products_attributes.options_id = orders_products_attributes.options_id AND ' .
                                         'products_attributes.options_values_id = orders_products_attributes.options_values_id')
                                  ->select('options_values_weight, weight_prefix')
                                  ->where('orders_products_id', (int)$product['opid']);
            foreach($attributesQuery->get()->result() as $attributesRow)
            {
                $prefixFactor = $attributesRow->weight_prefix === '+' ? 1 : -1;
                $productsWeight[$productsIndex] += $attributesRow->options_values_weight * $prefixFactor;
            }
	
	        $productsWeight[$productsIndex] = max(0.01, $productsWeight[$productsIndex]);
            $productsWeight[$productsIndex] = number_format($productsWeight[$productsIndex], 3, '.', '');
        }
		
		$formdata = [
			'pageToken'            => $_SESSION['coo_page_token']->generate_token(),
			'order_url'            => xtc_href_link('orders.php', 'oID=' . $orders_id . '&action=edit'),
			'countries'            => $this->getCountries(),
			'shipments'            => $this->getShipments($orders_id),
			'configuration'        => $this->configuration->get_all(),
			'action_create_label'  => xtc_href_link('admin.php', 'do=Geschaeftskundenversand/CreateLabel'),
			'action_delete_label'  => xtc_href_link('admin.php', 'do=Geschaeftskundenversand/DeleteLabel'),
			'products'             => $products,
			'products_default'     => $productsDefault,
			'product_names'        => $productNames,
			'order'                => $order,
            'products_weight'      => $productsWeight,
			'orders_id'            => $orders_id,
			'delivery'             => $order->delivery,
			'delivery_name'        => $name,
			'delivery_postnumber'  => $postnumber,
			'delivery_packstation' => $packstationNumber,
			'delivery_postfiliale' => $postfilialNumber,
			'delivery_parcelshop'  => $parcelshopNumber,
			'delivery_street'      => $street,
			'delivery_house_no'    => $houseNumber,
			'shipping_weight'      => number_format($shippingWeight, 3, '.', ''),
			'customer_reference'   => $orders_id,
			'shipment_date'        => date('Y-m-d'),
			'preferredday_min'     => $this->addWorkdays($today, 2)->format('Y-m-d'),
			'preferredday_max'     => $this->addWorkdays($today, 6)->format('Y-m-d'),
			'is_export'            => $isExport,
			'is_cod'               => stripos($order->info['payment_method'], 'cod') !== false,
			'cod_amount'           => number_format($orderTotalValue, 2, '.', ''),
			'insurance_amount'     => number_format($orderTotalValue, 2, '.', ''),
			'transportConditions'  => $transportConditions,
			'isAdultContent'       => $isAdultContent,
			'identcheck'           => [
				'surname'   => $order->delivery['lastname'],
				'givenname' => $order->delivery['firstname'],
				'dob'       => $customersDob
			],
			'prefill_phone'        => (bool)$this->configuration->get('prefill_phone') === true && $prefillAllowed === true,
			'prefill_email'        => (bool)$this->configuration->get('prefill_email') === true && $prefillAllowed === true,
			'label_target'         => (int)$this->configuration->get('open_in_new_tab') === 1 ? '_blank' : '',
			'premium_preselect'    => $premiumPreselect,
		];
		
		$html   = $this->_render('geschaeftskundenversand_form_single.html', $formdata);
		$assets = MainFactory::create('AssetCollection');
		$assets->add(MainFactory::create('Asset', DIR_WS_CATALOG
		                                          . 'admin/html/assets/styles/modules/geschaeftskundenversand.min.css'));
		$assets->add(MainFactory::create('Asset', DIR_WS_CATALOG
		                                          . 'admin/html/assets/javascript/modules/geschaeftskundenversand/geschaeftskundenversand-form.min.js'));
		
		return MainFactory::create('AdminPageHttpControllerResponse',
		                           $this->languageTextManager->get_text('gkv_head_prepare_label'), $html, $assets);
	}
	
	
	protected function getTransportConditions($orderIdString)
	{
		try
		{
			$orderId           = new IdType($orderIdString);
			$orderReadService  = StaticGXCoreLoader::getService('OrderRead');
			$order             = $orderReadService->getOrderById($orderId);
			$addonValueService = StaticGXCoreLoader::getService('AddonValue');
			$addonValueService->loadAddonValues($order);
			$transportConditions = $order->getAddonValue(new StringType('transportConditions'));
		}
		catch(Exception $e)
		{
			$transportConditions = 'unshown';
		}
		
		return $transportConditions;
	}
	
	
	protected function getCountries()
	{
		$db        = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$countries = $db->get_where('countries', ['status' => '1'])->result_array();
		
		// print_r($countries); die();
		return $countries;
	}
	
	
	protected function insertBankDataPlaceholders($configuredText, $orders_id)
	{
		$replacements = ['%orders_id%' => (int)$orders_id];
		$outputText   = strtr($configuredText, $replacements);
		
		return $outputText;
	}
	
	
	public function actionCreateLabel()
	{
		$this->_validatePageToken();
		$postData = $this->_getPostDataCollection()->getArray();
		
		$product = Mainfactory::create('GeschaeftskundenversandProduct',
		                               $this->configuration->get('products/' . (int)$postData['product_index']
		                                                         . '/type'),
		                               $this->configuration->get('products/' . (int)$postData['product_index']
		                                                         . '/attendance'),
		                               $this->configuration->get('products/' . (int)$postData['product_index']
		                                                         . '/alias'));
		
		$note1            = $this->insertBankDataPlaceholders($this->configuration->get('bankdata/note1'),
		                                                      $postData['orders_id']);
		$note2            = $this->insertBankDataPlaceholders($this->configuration->get('bankdata/note2'),
		                                                      $postData['orders_id']);
		$accountreference = $this->insertBankDataPlaceholders($this->configuration->get('bankdata/accountreference'),
		                                                      $postData['orders_id']);
		
		$shipment = Mainfactory::create('GeschaeftskundenversandShipment', $this->configuration->get('ekp'));
		$shipment->setBankData($this->configuration->get('bankdata/accountowner'),
		                       $this->configuration->get('bankdata/bankname'),
		                       $this->configuration->get('bankdata/iban'), $note1, $note2,
		                       $this->configuration->get('bankdata/bic'), $accountreference);
		$shipment->setShipperName($postData['shipper/name1'], $postData['shipper/name2'], $postData['shipper/name3']);
		$shipment->setShipperAddress($postData['shipper/streetname'], $postData['shipper/streetnumber'],
		                             $postData['shipper/addressaddition'], $postData['shipper/zip'],
		                             $postData['shipper/city'], $postData['shipper/origincountry']);
		$shipment->setShipperCommunication($postData['shipper/phone'], $postData['shipper/email'],
		                                   $postData['shipper/name1']);
		$shipment->setReturnReceiverName($postData['returnreceiver/name1'], $postData['returnreceiver/name2'],
		                                 $postData['returnreceiver/name3']);
		$shipment->setReturnReceiverAddress($postData['returnreceiver/streetname'],
		                                    $postData['returnreceiver/streetnumber'],
		                                    $postData['returnreceiver/addressaddition'],
		                                    $postData['returnreceiver/zip'], $postData['returnreceiver/city'],
		                                    $postData['returnreceiver/origincountry']);
		$shipment->setReturnReceiverCommunication($postData['returnreceiver/phone'], $postData['returnreceiver/email'],
		                                          $postData['returnreceiver/name1']);
		
		$shipment->setReceiverName($postData['receiver/name1']);
		if($postData['receiver_type'] == 'address')
		{
			$shipment->setReceiverAddress($postData['receiver_address/streetname'],
			                              $postData['receiver_address/streetnumber'],
			                              $postData['receiver_address/addressaddition'],
			                              $postData['receiver_address/zip'], $postData['receiver_address/city'],
			                              $postData['receiver_address/origincountry']);
			$shipment->setReceiverAdditionalNames(
				$postData['receiver_address/name2'],
				$postData['receiver_address/name3']
			);
		}
		if($postData['receiver_type'] == 'packstation')
		{
			$shipment->setReceiverPackstation($postData['receiver_packstation/packstationnumber'],
			                                  $postData['receiver_packstation/zip'],
			                                  $postData['receiver_packstation/city'],
			                                  $postData['receiver_packstation/origincountry'],
			                                  $postData['receiver_packstation/postnumber']);
		}
		if($postData['receiver_type'] == 'postfiliale')
		{
			$shipment->setReceiverPostfiliale($postData['receiver_postfiliale/postfilialnumber'],
			                                  $postData['receiver_postfiliale/zip'],
			                                  $postData['receiver_postfiliale/city'],
			                                  $postData['receiver_postfiliale/origincountry'],
			                                  $postData['receiver_postfiliale/postnumber']);
		}
		if($postData['receiver_type'] == 'parcelshop')
		{
			$shipment->setReceiverParcelShop($postData['receiver_parcelshop/parcelshopnumber'],
			                                 $postData['receiver_parcelshop/zip'],
			                                 $postData['receiver_parcelshop/city'],
			                                 $postData['receiver_parcelshop/origincountry'],
			                                 $postData['receiver_parcelshop/streetname'],
			                                 $postData['receiver_parcelshop/streetnumber']);
		}
		$shipment->setReceiverCommunication($postData['receiver/phone'], $postData['receiver/email'],
		                                    $postData['receiver/name1']);
		$shipment->setNotification($postData['notification']);
		$setReturnProduct = (bool)$this->configuration->get('create_return_label');
		$shipment->setProduct($product, $setReturnProduct);
		$shipment->setWeight($postData['shipping_weight']);
		$shipment->setShipmentDate($postData['shipment_date']);
		$shipment->setCustomerReference($postData['customer_reference']);
		$shipment->setAllServices($postData);
		if($product->getTargetArea() !== 'domestic' && $postData['exportdoc/exporttype'] !== 'NONE')
		{
			$shipment->setExportDocument(
				$postData['exportdoc/exporttype'],
				$postData['exportdoc/placeofcommital'],
			    $postData['exportdoc/additionalfee'],
				$postData['exportdoc/invoicenumber'],
	            $postData['exportdoc/termsoftrade'],
				$postData['exportdoc/permitnumber'],
	            $postData['exportdoc/attestationnumber'],
				isset($postData['exportdoc/withelectronicexportntfctn'])
			);
			if($postData['exportdoc/exporttype'] === 'OTHER')
			{
				$shipment->setExportTypeDescription($postData['exportdoc/exporttypedescription']);
			}
			$numPositions = count($postData['exportpos']['description']);
			// N.B. $posIdx == 0 would be the (empty) template row
			for($posIdx = 1; $posIdx < $numPositions; $posIdx++)
			{
				$shipment->addExportDocPosition($postData['exportpos']['description'][$posIdx],
				                                $postData['exportpos']['countrycodeorigin'][$posIdx],
				                                $postData['exportpos']['customstariffnumber'][$posIdx],
				                                $postData['exportpos']['amount'][$posIdx],
				                                $postData['exportpos']['netweightkg'][$posIdx],
				                                $postData['exportpos']['customsvalue'][$posIdx]);
			}
		}
		$shipment->setPrintOnlyIfCodeable(!empty($postData['only_if_codeable']));
		$shipment->setLabelResponseType('URL');
		
		$shipmentParams = $shipment->toArray();
		$this->logger->noticeDebug(print_r($shipmentParams, true));
		try
		{
			$gkvSoapAdapter = Mainfactory::create('GeschaeftskundenversandSoapAdapter', $this->configuration);
			$sc             = $gkvSoapAdapter->getSoapClient();
			
			$response = $sc->createShipmentOrder($shipmentParams);
			$this->logger->noticeDebug(sprintf("CreateShipmentOrder request:\n%s\n", $sc->__getLastRequest()));
			$this->logger->noticeDebug(sprintf("CreateShipmentOrder response:\n%s\n", print_r($response, true)));
			if((int)$response->Status->statusCode === 0)
			{
				$this->storeCreateShipmentResponse($postData['orders_id'], $response);
				$GLOBALS['messageStack']->add_session(sprintf('%s (%s - %s (%s/%s))',
				                                              $this->languageTextManager->get_text('gkv_label_created'),
				                                              (string)$response->CreationState->LabelData->shipmentNumber,
                                                              (string)$response->Status->statusMessage,
				                                              (string)$response->Status->statusCode,
				                                              (string)$response->Status->statusText), 'info');
				$this->logger->notice(sprintf('Shipment %s created, label URL: %s',
				                              $response->CreationState->LabelData->shipmentNumber,
				                              $response->CreationState->LabelData->labelUrl));
				
				$parcelServiceId = $this->configuration->get('parcel_service_id');
				if($parcelServiceId > 0)
				{
					$parcelServiceReader      = MainFactory::create('ParcelServiceReader');
					$parcelTrackingCodeWriter = MainFactory::create('ParcelTrackingCodeWriter');
					$parcelTrackingCodeWriter->insertTrackingCode((int)$postData['orders_id'],
					                                              (string)$response->CreationState->LabelData->shipmentNumber,
					                                              $parcelServiceId, $parcelServiceReader);
				}
				
				$order_status_id = $this->configuration->get('order_status_after_label');
				$notifyCustomer  = $this->configuration->get('notify_customer');
				if($order_status_id >= 0)
				{
					$order_status_comment = $this->languageTextManager->get_text('gkv_order_status_comment') . ' '
					                        . (string)$response->CreationState->LabelData->shipmentNumber;
					$this->setOrderStatus($postData['orders_id'], $order_status_id, $order_status_comment,
					                      (bool)$notifyCustomer);
				}
			}
			else
			{
				$statusMessage = sprintf('%s (%s) - %s', $response->Status->statusMessage,
				                         $response->Status->statusCode, $response->Status->statusText);
                
				if(isset($response->CreationState))
				{
					foreach($response->CreationState->LabelData->Status->statusMessage as $partMessage)
					{
						$statusMessage .= !empty($statusMessage) ? '<br>' : '';
						$statusMessage .= (string)$partMessage;
					}
				}
				$GLOBALS['messageStack']->add_session(sprintf('%s - %s (%s)',
				                                              $this->languageTextManager->get_text('gkv_error_creating_label'),
				                                              $statusMessage,
				                                              (string)$response->Status->statusCode), 'error');
			}
		}
		catch(SoapFault $sf)
		{
			$GLOBALS['messageStack']->add_session(sprintf('%s - %s',
			                                              $this->languageTextManager->get_text('gkv_error_creating_label'),
			                                              $sf->getMessage()), 'error');
			$this->logger->notice(sprintf("ERROR/SF creating label:\n%s\nRequest:\n%s\nResponse:\n%s\n",
			                              print_r($sf, true), $this->prettyXML($sc->__getLastRequest()),
			                              $this->prettyXML($sc->__getLastResponse())));
		}
		catch(GeschaeftskundenversandSoapAdapterServiceUnavailableException $e)
		{
			$lastRequest  = isset($sc)
				? $this->prettyXML($sc->__getLastRequest())
				: '-- no request --';
			$lastResponse = isset($sc)
				? $this->prettyXML($sc->__getLastResponse())
				: '-- no response --';
			
			$GLOBALS['messageStack']->add_session(sprintf('%s - %s',
			                                              $this->languageTextManager->get_text('gkv_error_creating_label'),
			                                              $e->getMessage()), 'error');
			$this->logger->notice(sprintf("ERROR creating label:\n%s\nRequest:\n%s\nResponse:\n%s\n", $e->getMessage(),
			                              $lastRequest, $lastResponse));
		}
		
		// $GLOBALS['messageStack']->add_session(sprintf('<pre>%s</pre>', htmlspecialchars(print_r($postData, true))), 'info');
		$redirectUrl = xtc_href_link('admin.php',
		                             'do=Geschaeftskundenversand/PrepareLabel&oID=' . (int)$postData['orders_id']);
		
		return MainFactory::create('RedirectHttpControllerResponse', $redirectUrl);
	}
	
	
	protected function storeCreateShipmentResponse($orders_id, $response)
	{
		$db = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$db->insert('gkv_shipments', [
			'orders_id'      => $orders_id,
			'shipmentnumber' => (string)$response->CreationState->LabelData->shipmentNumber,
			'labelurl'       => (string)$response->CreationState->LabelData->labelUrl,
			'returnlabelurl' => (string)$response->CreationState->LabelData->returnLabelUrl,
			'exportlabelurl' => (string)$response->CreationState->LabelData->exportLabelUrl,
			'codlabelurl'    => (string)$response->CreationState->LabelData->codLabelUrl,
		]);
	}
	
	
	protected function getShipments($orders_id)
	{
		$db        = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$shipments = $db->order_by('last_modified', 'DESC')
		                ->get_where('gkv_shipments', ['orders_id' => (int)$orders_id])
		                ->result_array();
		
		return $shipments;
	}
	
	
	protected function deleteShipment($shipmentnumber)
	{
		$db = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$db->delete('gkv_shipments', ['shipmentnumber' => $shipmentnumber]);
		$db->delete('orders_parcel_tracking_codes', ['tracking_code' => $shipmentnumber]);
	}
	
	
	public function actionDeleteLabel()
	{
		$this->_validatePageToken();
		$postData = $this->_getPostDataCollection()->getArray();
		
		$gkvSoapAdapter = Mainfactory::create('GeschaeftskundenversandSoapAdapter', $this->configuration);
		$sc             = $gkvSoapAdapter->getSoapClient();
		try
		{
			$params = [
				'Version'        => [
					'majorRelease' => (string)GeschaeftskundenversandConfigurationStorage::MAJOR_VERSION,
					'minorRelease' => (string)GeschaeftskundenversandConfigurationStorage::MINOR_VERSION,
				],
				'shipmentNumber' => $postData['shipmentnumber'],
			];
			$this->logger->notice(sprintf('deleting shipment %s', $postData['shipmentnumber']));
			$response = $sc->deleteShipmentOrder($params);
			if($response->Status->statusCode == 0)
			{
				$GLOBALS['messageStack']->add_session($this->languageTextManager->get_text('gkv_label_deleted'),
				                                      'info');
				$this->deleteShipment($postData['shipmentnumber']);
				$this->logger->notice(sprintf("Shipment %s deleted %s/%s/%s", $postData['shipmentnumber'],
				                              $response->Status->statusCode, $response->Status->statusText,
				                              $response->Status->statusMessage));
				
				$order_status_id = $this->configuration->get('order_status_after_label');
				$notifyCustomer  = $this->configuration->get('notify_customer');
				if($order_status_id >= 0)
				{
					$order_status_comment = $this->languageTextManager->get_text('gkv_order_status_comment_cancelled')
					                        . ' ' . $postData['shipmentnumber'];
					$this->setOrderStatus($postData['orders_id'], $order_status_id, $order_status_comment,
					                      (bool)$notifyCustomer);
				}
			}
			else
			{
				$this->logger->notice("ERROR deleting shipment:\n" . print_r($response, true));
				$errorMessage = $this->languageTextManager->get_text('gkv_cannot_delete_label');
				$errorMessage .= sprintf(' (%s/%s/%s)', $response->Status->statusCode, $response->Status->statusText,
				                         $response->Status->statusMessage);
				$GLOBALS['messageStack']->add_session($errorMessage, 'error');
			}
			// $GLOBALS['messageStack']->add_session(sprintf('<pre>%s</pre>', htmlspecialchars(print_r($response, true))), 'info');
		}
		catch(Exception $sf)
		{
			$errorMessage = $this->languageTextManager->get_text('gkv_cannot_delete_label');
			$errorMessage .= '<br>' . $sf->getMessage();
			$GLOBALS['messageStack']->add_session($errorMessage, 'error');
			$this->logger->notice(sprintf("ERROR/SF deleting shipment:\n%s\nRequest:\n%s\nResponse:\n%s\n",
			                              print_r($sf, true), $this->prettyXML($sc->__getLastRequest()),
			                              $this->prettyXML($sc->__getLastResponse())));
		}
		
		// $GLOBALS['messageStack']->add_session(sprintf('<pre>%s</pre>', htmlspecialchars(print_r($postData, true))), 'info');
		$redirectUrl = xtc_href_link('admin.php',
		                             'do=Geschaeftskundenversand/PrepareLabel&oID=' . (int)$postData['orders_id']);
		
		return MainFactory::create('RedirectHttpControllerResponse', $redirectUrl);
	}
	
	/* ================================================================================================================================= */
	
	/**
	 * Heuristically splits up a street address into its component street name and house number
	 *
	 * @param  string
	 *
	 * @return array with keys 'street' and 'house_no'
	 */
	protected function splitStreet($street_address)
	{
		$street_address = trim($street_address);
		$splitStreet    = [
			'street'   => $street_address,
			'house_no' => '',
		];
		$matches        = [];
		if(preg_match('_^(\d.*?)\s(.+)_', $street_address, $matches) === 1)
		{
			$splitStreet['street']   = $matches[2];
			$splitStreet['house_no'] = $matches[1];
		}
		else if(preg_match('_(.+?)\s?(\d.*)_', $street_address, $matches) === 1)
		{
			$splitStreet['street']   = $matches[1];
			$splitStreet['house_no'] = $matches[2];
		}
		
		return $splitStreet;
	}
	
	
	protected function prettyXML($xml)
	{
		if(empty($xml))
		{
			return '-- NO CONTENT --';
		}
		$doc                     = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		$doc->formatOutput       = true;
		$doc->loadXML($xml);
		
		return $doc->saveXML();
	}
	
	
	/**
	 * set order status and (optionally) notify customer by email
	 *
	 * @param         int orders_id
	 * @param         int orders_status_id
	 * @param string  $order_status_comment
	 * @param boolean $notifyCustomer
	 */
	protected function setOrderStatus($orders_id, $order_status_id, $order_status_comment = '', $notifyCustomer = false)
	{
		$db = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$this->logger->notice(sprintf('changing orders status of order %s to %s', $orders_id, $order_status_id));
		$db->where('orders_id', $orders_id);
		$db->update('orders', ['orders_status' => $order_status_id]);
		
		$orders_status_history_entry = [
			'orders_id'         => $orders_id,
			'orders_status_id'  => $order_status_id,
			'date_added'        => date('Y-m-d H:i:s'),
			'customer_notified' => $notifyCustomer === true
				? '1'
				: '0',
			'comments'          => $order_status_comment,
		];
		$db->insert('orders_status_history', $orders_status_history_entry);
		if($notifyCustomer === true)
		{
			$this->logger->notice(sprintf('sending email notification regarding status change of order %s',
			                              $orders_id));
			$this->notifyCustomer($orders_id, $order_status_id, $order_status_comment);
		}
	}
	
	
	/**
	 * notify customer of a change in order status
	 *
	 * This is mostly copypasted from orders.php and MUST be refactored ASAP!
	 */
	protected function notifyCustomer($orders_id, $orders_status_id, $order_status_comment)
	{
		require_once DIR_FS_INC . 'xtc_php_mail.inc.php';
		require_once DIR_WS_CLASSES . 'order.php';
		$order       = new order((int)$orders_id);
		$lang_query  = sprintf('select languages_id from %s where directory = \'%s\'', TABLE_LANGUAGES,
		                       $order->info['language']);
		$lang_result = xtc_db_query($lang_query);
		while($lang_row = xtc_db_fetch_array($lang_result))
		{
			$lang = empty($lang_row['languages_id'])
				? $_SESSION['languages_id']
				: $lang_row['languages_id'];
		}
		$orders_status_array  = [];
		$orders_status_query  = sprintf('select orders_status_id, orders_status_name from %s where language_id = \'%s\'',
		                                TABLE_ORDERS_STATUS, $lang);
		$orders_status_result = xtc_db_query($orders_status_query);
		while($orders_status_row = xtc_db_fetch_array($orders_status_result))
		{
			$orders_status_array[$orders_status_row['orders_status_id']] = $orders_status_row['orders_status_name'];
		}
		
		$smarty = new Smarty;
		// assign language to template for caching
		$smarty->assign('language', $_SESSION['language']);
		$smarty->caching      = false;
		$smarty->template_dir = DIR_FS_CATALOG . 'templates';
		$smarty->compile_dir  = DIR_FS_CATALOG . 'templates_c';
		$smarty->config_dir   = DIR_FS_CATALOG . 'lang';
		$smarty->assign('tpl_path', 'templates/' . CURRENT_TEMPLATE . '/');
		$smarty->assign('logo_path', HTTP_SERVER . DIR_WS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/img/');
		$smarty->assign('NAME', $order->customer['name']);
		$smarty->assign('GENDER', $order->customer['gender']);
		$smarty->assign('ORDER_NR', $orders_id);
		$smarty->assign('ORDER_LINK',
		                xtc_catalog_href_link(FILENAME_CATALOG_ACCOUNT_HISTORY_INFO, 'order_id=' . $orders_id, 'SSL'));
		$smarty->assign('ORDER_DATE', xtc_date_long($order->info['date_purchased']));
		$smarty->assign('ORDER_STATUS', $orders_status_array[$orders_status_id]);
		if(defined('EMAIL_SIGNATURE'))
		{
			$smarty->assign('EMAIL_SIGNATURE_HTML', nl2br(EMAIL_SIGNATURE));
			$smarty->assign('EMAIL_SIGNATURE_TEXT', EMAIL_SIGNATURE);
		}
		
		// START Parcel Tracking Code
		/** @var ParcelTrackingCode $coo_parcel_tracking_code_item */
		$coo_parcel_tracking_code_item = MainFactory::create_object('ParcelTrackingCode');
		/** @var ParcelTrackingCodeReader $coo_parcel_tracking_code_reader */
		$coo_parcel_tracking_code_reader = MainFactory::create_object('ParcelTrackingCodeReader');
		$t_parcel_tracking_codes_array   = $coo_parcel_tracking_code_reader->getTackingCodeItemsByOrderId($coo_parcel_tracking_code_item,
		                                                                                                  $orders_id);
		$smarty->assign('PARCEL_TRACKING_CODES_ARRAY', $t_parcel_tracking_codes_array);
		$smarty->assign('PARCEL_TRACKING_CODES', 'true');
		// END Parcel Tracking Code
		
		$smarty->assign('NOTIFY_COMMENTS', nl2br($order_status_comment));
		$html_mail = fetch_email_template($smarty, 'change_order_mail', 'html');
		$smarty->assign('NOTIFY_COMMENTS', $order_status_comment);
		$txt_mail = fetch_email_template($smarty, 'change_order_mail', 'txt');
		
		if($_SESSION['language'] == 'german')
		{
			$subject = 'Ihre Bestellung ' . $orders_id . ', ' . xtc_date_long($order->info['date_purchased']) . ', '
			           . $order->customer['name'];
		}
		else
		{
			$subject = 'Your order ' . $orders_id . ', ' . xtc_date_long($order->info['date_purchased']) . ', '
			           . $order->customer['name'];
		}
		
		xtc_php_mail(EMAIL_BILLING_ADDRESS, EMAIL_BILLING_NAME, $order->customer['email_address'],
		             $order->customer['name'], '', EMAIL_BILLING_REPLY_ADDRESS, EMAIL_BILLING_REPLY_ADDRESS_NAME, '',
		             '', $subject, $html_mail, $txt_mail);
	}
	

	public function isWorkDay(DateTime $datetime)
	{
		$holidays = [
			'01-01', // Neujahr
			'05-01', // Tag der Arbeit
			'10-03', // Tag der dt. Einheit
			'12-25', // 1. Weihnachtstag
			'12-26', // 2. Weihnachtstag
		];
		$easterTimestamp = easter_date($datetime->format('Y'));
		$holiday = new DateTime('@' . $easterTimestamp);
		$holiday->setTimezone(new DateTimeZone('Europe/Berlin'));
		$holidays[] = $holiday->format('m-d'); // Ostersonntag
		$holidays[] = $holiday->sub(new DateInterval('P2D'))->format('m-d'); // Karfreitag
		$holidays[] = $holiday->add(new DateInterval('P3D'))->format('m-d'); // Ostermontag
		$holidays[] = $holiday->add(new DateInterval('P38D'))->format('m-d'); // Himmelfahrt
		$holidays[] = $holiday->add(new DateInterval('P11D'))->format('m-d'); // Pfingstmontag

		$isHoliday = in_array($datetime->format('m-d'), $holidays, true);
		$isSunday  = $datetime->format('N') === '7';
		$isWorkday = !$isSunday && !$isHoliday;
		return $isWorkday;
	}

	public function addWorkdays(DateTime $dateTime, $days)
	{
		$endDate = clone $dateTime;
		$days = abs((int)$days);
		do {
			$endDate->add(new DateInterval('P1D'));
			if($this->isWorkDay($endDate) === true) {
				$days--;
			}
		} while($days > 0);
		return $endDate;
	}
}

