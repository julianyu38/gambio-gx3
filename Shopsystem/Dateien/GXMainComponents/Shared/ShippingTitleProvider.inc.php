<?php

/* --------------------------------------------------------------
   ShippingTitleProvider.inc.php 2017-04-11
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class ShippingTitleProvider
 * 
 * @category System
 * @package  Shared
 */
class ShippingTitleProvider extends ModuleTitleProvider
{
	/**
	 * @var string
	 */
	protected static $type = 'shipping';
}