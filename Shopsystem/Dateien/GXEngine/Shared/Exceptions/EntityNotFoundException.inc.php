<?php
/* --------------------------------------------------------------
   EntityNotFoundException.inc.php 2017-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class FileNotFoundException
 *
 * @category   System
 * @package    Shared
 * @subpackage Exceptions
 */
class EntityNotFoundException extends \Exception
{
}