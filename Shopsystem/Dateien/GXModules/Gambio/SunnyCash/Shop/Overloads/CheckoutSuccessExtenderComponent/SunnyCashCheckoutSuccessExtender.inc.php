<?php
/* --------------------------------------------------------------
   SunnyCashCheckoutSuccessExtender.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashCheckoutSuccessExtender
 */
class SunnyCashCheckoutSuccessExtender extends SunnyCashCheckoutSuccessExtender_parent
{
    public function proceed()
    {
        parent::proceed();
    
        $contentView = MainFactory::create('ContentView');
        $contentView->set_template_dir(DIR_FS_CATALOG . 'GXModules/Gambio/SunnyCash/Shop/Templates/Honeygrid/module');
        $contentView->set_content_template('sunnycash_checkout_success.html');
        $contentView->set_flat_assigns(true);
        $contentView->set_caching_enabled(false);
        
        $configurationStorage = MainFactory::create('ConfigurationStorage', 'modules/GambioSunnyCash');
        $contentView->set_content_data('sunnycash_active', $configurationStorage->get('active'));
        
        $this->html_output_array['SunnyCash'] = $contentView->get_html();
    }
}