<?php
/* --------------------------------------------------------------
   SunnyCashLogoService.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashLogoService
 */
class SunnyCashLogoService
{
    /**
     * @var SunnyCashLogoStorageReader $logoStorageReader
     */
    protected $logoStorageReader;
    
    
    /**
     * SunnyCashLogoService constructor.
     *
     * @param SunnyCashLogoStorageReader $logoStorageReader
     */
    public function __construct(SunnyCashLogoStorageReader $logoStorageReader)
    {
        $this->logoStorageReader = $logoStorageReader;
    }
    
    
    /**
     * Gets the requested logo.
     * 
     * @param StringType $logoUrl
     *
     * @return SunnyCashLogo
     */
    public function getLogo(StringType $logoUrl)
    {
        return $this->logoStorageReader->getLogo($logoUrl);
    }
}