<?php
/* --------------------------------------------------------------
   SunnyCashCouponService.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashCouponService
 */
class SunnyCashCouponService
{
    /**
     * @var SunnyCashCouponRepositoryInterface $couponRepository
     */
    protected $couponRepository;
    
    
    /**
     * SunnyCashService constructor.
     *
     * @param SunnyCashCouponRepositoryInterface $couponRepository
     */
    public function __construct(SunnyCashCouponRepositoryInterface $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }
    
    
    /**
     * Gets a coupon by its ID.
     * 
     * @param IdType $id
     *
     * @return SunnyCashCoupon
     */
    public function getCoupon(IdType $id)
    {
        return $this->couponRepository->getCouponById($id);
    }
    
    
    /**
     * Gets all SunnyCash coupons.
     *
     * @param IntType $limit
     * @param IntType $offset
     * 
     * @return SunnyCashCouponCollection
     */
    public function getCoupons(IntType $limit, IntType $offset)
    {
        return $this->couponRepository->getAllCoupons($limit, $offset);
    }
    
    
    /**
     * Gets the total number of available SunnyCash coupons.
     * 
     * @return int
     */
    public function getCouponQuantity()
    {
        return $this->couponRepository->getCouponQuantity();
    }
}