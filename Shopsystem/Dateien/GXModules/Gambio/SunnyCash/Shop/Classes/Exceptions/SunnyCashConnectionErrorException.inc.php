<?php
/* --------------------------------------------------------------
   SunnyCashConnectionErrorException.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class representing an exception for a failed connection to the SunnyCash coupon feed 
 */
class SunnyCashConnectionErrorException extends Exception
{
}