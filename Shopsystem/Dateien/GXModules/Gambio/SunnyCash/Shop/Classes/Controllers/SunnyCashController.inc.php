<?php
/* --------------------------------------------------------------
   SunnyCashController.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/


/**
 * Class SunnyCashController
 */
class SunnyCashController extends HttpViewController
{
    /**
     * @var string $templatePath
     */
    protected $templatePath;
    
    /**
     * @var string $templateName
     */
    protected $templateName;
    
    
    /**
     * Initialize Controller
     */
    public function init()
    {
        $this->templatePath = DIR_FS_CATALOG . '/GXModules/Gambio/SunnyCash/Shop/Templates/Honeygrid/module/';
    }
    
    
    /**
     * Gets all coupons for the current page.
     * 
     * @return bool|\HttpControllerResponse
     */
    public function actionDefault()
    {
        $sunnyCashConfiguration = MainFactory::create('GXModuleConfigurationStorage', 'GambioSunnyCash');
        if(!$sunnyCashConfiguration->is('active'))
        {
            return MainFactory::create('RedirectHttpControllerResponse', DIR_WS_CATALOG);
        }
        
        $this->templateName = 'sunnycash_coupon_wall.html';
        
        $contentView = MainFactory::create('ContentView');
        $contentView->set_template_dir($this->templatePath);
        $contentView->set_content_template($this->templateName);
        $contentView->set_flat_assigns(true);
        $contentView->set_caching_enabled(false);
        
        $couponService = SunnyCashServiceFactory::createCouponService();
        $limit         = $this->getNumberOfCouponsPerPage();
        $page          = $this->_getQueryParameter('page') !== null ? (int)$this->_getQueryParameter('page') : 1;
        $offset        = max(0, ($page - 1) * $limit);
        
        $contentView->set_content_data('sunnycash_coupons',
            $couponService->getCoupons(new IntType($limit), new IntType($offset)));
        
        return MainFactory::create('HttpControllerResponse', $contentView->get_html());
    }
    
    
    /**
     * Gets the coupon code for a single coupon of which the ID is passed via POST data.
     * 
     * @return bool|\HttpControllerResponse
     */
    public function actionCode()
    {
        $this->templateName = 'sunnycash_coupon_modal.html';
        
        $couponService = SunnyCashServiceFactory::createCouponService();
        $coupon        = $couponService->getCoupon(new IdType($this->_getPostData('coupon_id')));
        $couponCode    = $coupon->getCode()->asString();
        
        $contentView = MainFactory::create('ContentView');
        $contentView->set_template_dir($this->templatePath);
        $contentView->set_content_template($this->templateName);
        $contentView->set_flat_assigns(true);
        $contentView->set_caching_enabled(false);
        $contentView->set_content_data('sunnycash_coupon', $coupon);
        $contentView->set_content_data('coupon_code', $couponCode);
        
        return MainFactory::create('HttpControllerResponse', $contentView->get_html());
    }
    
    
    /**
     * Gets the number of pages calculated with the configured coupons per page.
     * 
     * @return bool|\JsonHttpControllerResponse
     */
    public function actionGetPageCount()
    {
        $couponService  = SunnyCashServiceFactory::createCouponService();
        $couponQuantity = $couponService->getCouponQuantity();
        $limit          = $this->getNumberOfCouponsPerPage();
        $pageCount      = ceil($couponQuantity / $limit);
        
        return MainFactory::create('JsonHttpControllerResponse', ['pageCount' => (int)$pageCount]);
    }
    
    
    /**
     * Gets the configured number of coupons per page from the GXModuleConfigurationStorage.
     * 
     * @return int
     */
    protected function getNumberOfCouponsPerPage()
    {
        $configurationStorage = MainFactory::create('GXModuleConfigurationStorage', 'GambioSunnyCash');
        $limit                = $configurationStorage->get('coupons_per_page') ? $configurationStorage->get('coupons_per_page') : 18;
        
        return (int)$limit;
    }
}

