<?php
/* --------------------------------------------------------------
   SunnyCashLogoController.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/


/**
 * Class SunnyCashLogoController
 */
class SunnyCashLogoController extends HttpViewController
{
    public function actionDefault()
    {
        $logoService   = SunnyCashServiceFactory::createLogoService();
        $getParameters = $this->_getQueryParametersCollection()->getArray();
        $logo          = '';
        
        if ($getParameters['source'] !== '') {
            $logo = $logoService->getLogo(new StringType($getParameters['source']))->toImageString();
        }
        
        return MainFactory::create('HttpControllerResponse', $logo, ['Content-Type: image/png']);
    }
}

