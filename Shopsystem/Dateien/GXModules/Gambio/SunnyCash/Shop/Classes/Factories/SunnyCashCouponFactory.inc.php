<?php
/* --------------------------------------------------------------
   SunnyCashCouponFactory.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashCouponFactory
 *
 * @category   Core
 * @package    SunnyCash
 * @subpackage Factories
 */
class SunnyCashCouponFactory
{
    /**
     * @var SunnyCashLogoStorageWriterInterface $logoStorageWriter
     */
    protected $logoStorageWriter;
    
    /**
     * @var SunnyCashCouponCodeReaderInterface $couponCodeReader
     */
    protected $couponCodeReader;
    
    
    /**
     * SunnyCashCouponFactory constructor.
     *
     * @param SunnyCashLogoStorageWriterInterface $logoStorageWriter
     * @param SunnyCashCouponCodeReaderInterface  $couponCodeReader
     */
    public function __construct(
        SunnyCashLogoStorageWriterInterface $logoStorageWriter,
        SunnyCashCouponCodeReaderInterface $couponCodeReader
    ) {
        $this->logoStorageWriter = $logoStorageWriter;
        $this->couponCodeReader  = $couponCodeReader;
    }
    
    
    /**
     * Creates a coupon instance from JSON decoded coupon data.
     *
     * @param stdClass $coupon
     *
     * @return SunnyCashCoupon
     */
    public function createFromJson(stdClass $coupon)
    {
        return MainFactory::create(SunnyCashCoupon::class,
            $this->logoStorageWriter,
            $this->couponCodeReader,
            new IdType($coupon->id),
            new StringType($coupon->name),
            new StringType($coupon->description),
            new StringType($coupon->campaign_url),
            new StringType($coupon->partner_title),
            new StringType($coupon->offer_img),
            new StringType($coupon->type));
    }
    
    
    /**
     * Creates a collection of coupon instances.
     *
     * @param array $couponArray
     *
     * @return SunnyCashCouponCollection
     */
    public function createCollectionFromArray(array $couponArray)
    {
        return MainFactory::create(SunnyCashCouponCollection::class, $couponArray);
    }
}