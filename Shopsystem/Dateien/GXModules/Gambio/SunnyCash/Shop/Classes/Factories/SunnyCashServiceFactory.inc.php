<?php
/* --------------------------------------------------------------
   SunnyCashServiceFactory.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/
use HubPublic\Http\CurlRequest;

/**
 * Class SunnyCashServiceFactory
 *
 * @category   Core
 * @package    SunnyCash
 * @subpackage Factories
 */
class SunnyCashServiceFactory
{
    /**
     * @var string $dirFsCatalog
     */
    protected static $dirFsCatalog = DIR_FS_CATALOG;
    
    /**
     * @var string $rootDirectory
     */
    protected static $rootDirectory;
    
    /**
     * @var string $curl
     */
    protected static $curl;
    
    
    /**
     * @return \SunnyCashCouponService
     */
    public static function createCouponService()
    {
        return MainFactory::create(SunnyCashCouponService::class, self::createCouponRepository());
    }
    
    
    /**
     * @return \SunnyCashLogoService
     */
    public static function createLogoService()
    {
        return MainFactory::create(SunnyCashLogoService::class, self::createLogoStorageReader());
    }
    
    
    /**
     * @return \SunnyCashCouponRepository
     */
    protected static function createCouponRepository()
    {
        return MainFactory::create(SunnyCashCouponRepository::class, self::createCouponReader(),
            self::createCacheManager());
    }
    
    
    /**
     * @return \SunnyCashCouponReader
     */
    protected static function createCouponReader()
    {
        return MainFactory::create(SunnyCashCouponReader::class, self::createCouponFactory(), self::root());
    }
    
    
    /**
     * @return \SunnyCashCacheManager
     */
    protected static function createCacheManager()
    {
        return MainFactory::create(SunnyCashCacheManager::class, self::createCouponFactory(),
            self::createExpirationStrategy(), self::curl(), self::root());
    }
    
    
    /**
     * @return \SunnyCashCouponFactory
     */
    protected static function createCouponFactory()
    {
        return MainFactory::create(SunnyCashCouponFactory::class, self::createLogoStorageWriter(),
            self::createCouponCodeReader());
    }
    
    
    /**
     * @return \SunnyCashExpirationStrategy
     */
    protected static function createExpirationStrategy()
    {
        return MainFactory::create(SunnyCashExpirationStrategy::class);
    }
    
    
    /**
     * @return \SunnyCashLogoStorageReader
     */
    protected static function createLogoStorageReader()
    {
        return MainFactory::create(SunnyCashLogoStorageReader::class, self::createLogoFactory(), static::root());
    }
    
    
    /**
     * @return \SunnyCashLogoFactory
     */
    protected static function createLogoFactory()
    {
        return MainFactory::create(SunnyCashLogoFactory::class, self::createLogoStorageWriter());
    }
    
    
    /**
     * @return \SunnyCashLogoStorageWriter
     */
    protected static function createLogoStorageWriter()
    {
        return MainFactory::create(SunnyCashLogoStorageWriter::class, static::curl(), static::root());
    }
    
    
    /**
     * @return \SunnyCashCouponCodeReader
     */
    protected static function createCouponCodeReader()
    {
        return MainFactory::create(SunnyCashCouponCodeReader::class, static::curl());
    }
    
    
    /**
     * @return $curl
     */
    protected static function curl()
    {
        if (null === static::$curl) {
            static::$curl = new CurlRequest();
        }
        
        return static::$curl;
    }
    
    
    /**
     * @return $rootDirectory
     */
    protected static function root()
    {
        if (null === static::$rootDirectory) {
            static::$rootDirectory = new ExistingDirectory(static::$dirFsCatalog);
        }
        
        return static::$rootDirectory;
    }
}