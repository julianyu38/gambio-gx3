<?php
/* --------------------------------------------------------------
   SunnyCashLogoFactory.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashLogoFactory
 *
 * @category   Core
 * @package    SunnyCash
 * @subpackage Factories
 */
class SunnyCashLogoFactory
{
    /**
     * @var SunnyCashLogoStorageWriterInterface $logoStorageWriter
     */
    protected $logoStorageWriter;
    
    
    /**
     * SunnyCashLogoFactory constructor.
     *
     * @param SunnyCashLogoStorageWriterInterface $logoStorageWriter
     */
    public function __construct(SunnyCashLogoStorageWriterInterface $logoStorageWriter)
    {
        $this->logoStorageWriter = $logoStorageWriter;
    }
    
    
    /**
     * Creates an empty logo instance representing the absence of the logo in the filesystem.
     *
     * @return SunnyCashLogo
     */
    public function createEmptyLogo()
    {
        return MainFactory::create(SunnyCashLogo::class, $this->logoStorageWriter, new StringType(''), new StringType(''));
    }
    
    
    /**
     * Creates a logo instance from a given file path.
     *
     * @param StringType $logoUrl
     * @param StringType $logoPath
     *
     * @return SunnyCashLogo
     */
    public function createLogo(StringType $logoUrl, StringType $logoPath)
    {
        return MainFactory::create(SunnyCashLogo::class, $this->logoStorageWriter, $logoUrl, $logoPath);
    }
}