<?php
/* --------------------------------------------------------------
   SunnyCashCouponCode.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashCouponCode
 */
class SunnyCashCouponCode
{
    /**
     * @var string $code
     */
    protected $code;
    
    
    public function __construct($code)
    {
        if (!is_string($code)) {
            throw new InvalidArgumentException('SunnyCashCouponCode: Invalid argument value given (expected string got '
                                               . gettype($code) . '): ' . $code);
        }
        
        $this->code = $code;
    }
    
    
    public function asString()
    {
        return $this->code;
    }
}