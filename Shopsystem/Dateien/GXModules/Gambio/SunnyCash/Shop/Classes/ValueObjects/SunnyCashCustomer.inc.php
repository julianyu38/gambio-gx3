<?php
/* --------------------------------------------------------------
   SunnyCashCustomer.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashCustomer
 */
class SunnyCashCustomer
{
    /**
     * @var string $firstName
     */
    protected $firstName;
    
    /**
     * @var string $lastName
     */
    protected $lastName;
    
    /**
     * @var string $email
     */
    protected $email;
    
    
    /**
     * SunnyCashCustomer constructor.
     *
     * @param $firstName
     * @param $lastName
     * @param $email
     */
    public function __construct($firstName, $lastName, $email)
    {
        $this->validate($firstName);
        $this->validate($lastName);
        $this->validate($email);
        
        $this->firstName = $firstName;
        $this->lastName  = $lastName;
        $this->email     = $email;
    }
    
    
    /**
     * Validates strings.
     * 
     * @param $string
     */
    protected function validate($string)
    {
        if (!is_string($string)) {
            throw new InvalidArgumentException('SunnyCashCustomer: Invalid argument value given (expected string got '
                                               . gettype($string) . '): ' . $string);
        }
    }
    
    
    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    
    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }
    
    
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}