<?php
/* --------------------------------------------------------------
   SunnyCashExpirationStrategy.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashExpirationStrategy
 */
class SunnyCashExpirationStrategy implements SunnyCashExpirationStrategyInterface
{
    /**
     * @var int $interval Minutes
     */
    protected $interval = 120;
    
    /**
     * @var int $daytimeStart Hour of day
     */
    protected $daytimeStart = 8;
    
    /**
     * @var int $daytimeEnd Hour of day
     */
    protected $daytimeEnd = 19;
    
    /**
     * @var int $offsetMinutesForNewDay Offset time in minutes after midnight
     */
    protected $offsetMinutesForNewDay = 15;
    
    
    /**
     * Indicates if something is expired according to the strategy and the given date and time.
     *
     * @param IntType $timestamp The timestamp that is tested
     *
     * @return bool
     */
    public function isExpired(IntType $timestamp)
    {
        return ($this->isDaytime() && $this->isOlderThanInterval($timestamp))
               || $this->isOlderThanToday($timestamp);
    }
    
    
    /**
     * Indicates if it is daytime.
     *
     * @return bool
     */
    protected function isDaytime()
    {
        $hourOfDay = (int)date('H');
        
        return $hourOfDay >= $this->daytimeStart && $hourOfDay <= $this->daytimeEnd;
    }
    
    
    /**
     * Indicates if the file is older than the specified interval.
     * 
     * @param IntType $timestamp
     *
     * @return bool
     */
    protected function isOlderThanInterval(IntType $timestamp)
    {
        $actualDate      = date('Y-m-d');
        $actualHourOfDay = (int)date('H');
        $actualMinutes   = (int)date('i');
        $fileDate        = date('Y-m-d', $timestamp->asInt());
        $fileHourOfDay   = (int)date('H', $timestamp->asInt());
        $fileMinutes     = (int)date('i', $timestamp->asInt());
        
        return $actualDate > $fileDate
               || ($actualDate === $fileDate
                   && $this->minuteOfDay(new IntType($fileHourOfDay), new IntType($fileMinutes)) + $this->interval
                      < $this->minuteOfDay(new IntType($actualHourOfDay), new IntType($actualMinutes)));
    }
    
    
    /**
     * Indicates if the file is older than today plus a specified offset of this day.
     * 
     * @param IntType $timestamp
     *
     * @return bool
     */
    protected function isOlderThanToday(IntType $timestamp)
    {
        $actualDate      = date('Y-m-d');
        $actualHourOfDay = (int)date('H');
        $actualMinutes   = (int)date('i');
        $fileDate        = date('Y-m-d', $timestamp->asInt());
        $fileHourOfDay   = (int)date('H', $timestamp->asInt());
        $fileMinutes     = (int)date('i', $timestamp->asInt());
        
        return ($fileDate < $actualDate
                && $this->minuteOfDay(new IntType($actualHourOfDay), new IntType($actualMinutes))
                   > $this->offsetMinutesForNewDay)
               || ($fileDate === $actualDate
                   && $this->minuteOfDay(new IntType($fileHourOfDay), new IntType($fileMinutes))
                      < $this->offsetMinutesForNewDay);
    }
    
    
    /**
     * Calculates the number of minutes for a given number of hours and minutes.
     * 
     * @param IntType $hours
     * @param IntType $minutes
     *
     * @return int
     */
    protected function minuteOfDay(IntType $hours, IntType $minutes)
    {
        return $hours->asInt() * 60 + $minutes->asInt();
    }
}