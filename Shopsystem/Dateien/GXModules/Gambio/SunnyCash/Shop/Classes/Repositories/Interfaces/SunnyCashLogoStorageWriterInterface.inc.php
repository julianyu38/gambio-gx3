<?php
/* --------------------------------------------------------------
   SunnyCashLogoStorageWriterInterface.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface SunnyCashLogoStorageWriterInterface
 */
interface SunnyCashLogoStorageWriterInterface
{
    /**
     * Gets a new version of the requested logo from SunnyCash.
     *
     * @param StringType $logoUrl The requested logo
     *
     * @throws SunnyCashConnectionErrorException
     */
    public function store(StringType $logoUrl);
}