<?php
/* --------------------------------------------------------------
   SunnyCashCouponReaderInterface.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface SunnyCashCouponCodeReaderInterface
 */
interface SunnyCashCouponCodeReaderInterface
{
    /**
     * Gets a coupon code for the requested coupon.
     * 
     * @param IdType            $id
     * @param StringType        $transactionId
     * @param SunnyCashCustomer $customer
     *
     * @return SunnyCashCouponCode
     */
    public function getCode(IdType $id, StringType $transactionId, SunnyCashCustomer $customer);
}