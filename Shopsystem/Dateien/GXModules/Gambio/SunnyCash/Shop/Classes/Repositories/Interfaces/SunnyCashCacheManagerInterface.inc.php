<?php
/* --------------------------------------------------------------
   SunnyCashCacheManagerInterface.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface SunnyCashCacheManagerInterface
 */
interface SunnyCashCacheManagerInterface
{
    /**
     * Checks if the cache expired and refreshes it accordingly.
     */
    public function checkAndPerformRefresh();
}