<?php
/* --------------------------------------------------------------
   SunnyCashExpirationStrategyInterface.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface SunnyCashExpirationStrategyInterface
 */
interface SunnyCashExpirationStrategyInterface
{
    /**
     * Indicates if something is expired according to the strategy and the given date and time.
     * 
     * @param IntType $timestamp The timestamp that is tested
     *
     * @return bool
     */
    public function isExpired(IntType $timestamp);
}