<?php
/* --------------------------------------------------------------
   SunnyCashCouponReader.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashCouponReader
 */
class SunnyCashCouponReader implements SunnyCashCouponReaderInterface
{
    /**
     * @var \SunnyCashCouponFactory $couponFactory
     */
    protected $couponFactory;
    
    /**
     * @var \ExistingDirectory $rootDirectory
     */
    protected $rootDirectory;
    
    
    /**
     * SunnyCashCouponReader constructor.
     *
     * @param \SunnyCashCouponFactory $couponFactory
     * @param \ExistingDirectory      $rootDirectory
     */
    public function __construct(SunnyCashCouponFactory $couponFactory, ExistingDirectory $rootDirectory)
    {
        $this->couponFactory = $couponFactory;
        $this->rootDirectory = $rootDirectory;
    }
    
    
    /**
     * Gets a coupon by its ID.
     *
     * @param IdType $id
     *
     * @return SunnyCashCoupon
     *
     * @throws CouponNotFoundException
     * @throws InvalidJsonFormatException
     */
    public function getCouponById(IdType $id)
    {
        $rawJsonCoupons = $this->getJsonCoupons();
    
        if ($rawJsonCoupons === null) {
            throw new InvalidJsonFormatException;
        }
        
        /** @var array $rawJsonCoupons */
        foreach ($rawJsonCoupons as $rawJsonCoupon) {
            if ((int)$rawJsonCoupon->id === $id->asInt()) {
                return $this->couponFactory->createFromJson($rawJsonCoupon);
            }
        }
        
        throw new CouponNotFoundException;
    }
    
    
    /**
     * Gets all available SunnyCash coupons.
     *
     * @param IntType $limit
     * @param IntType $offset
     * 
     * @return SunnyCashCouponCollection
     *
     * @throws InvalidJsonFormatException In case the returned JSON is not in a valid format
     */
    public function getAllCoupons(IntType $limit, IntType $offset)
    {
        $couponArray    = [];
        $rawJsonCoupons = $this->getJsonCoupons();
        
        if ($rawJsonCoupons === null) {
            throw new InvalidJsonFormatException;
        }
        
        $couponQuantity = count($rawJsonCoupons);
        $offsetValue = $offset->asInt();
        $limitValue = $limit->asInt();
        
        /** @var array $rawJsonCoupons */
        for ($i = $offsetValue; $i < $offsetValue + $limitValue && $i < $couponQuantity; $i++) {
            $couponArray[] = $this->couponFactory->createFromJson($rawJsonCoupons[$i]);
        }
        
        return $this->couponFactory->createCollectionFromArray($couponArray);
    }
    
    
    /**
     * Gets the total number of coupons from the cache file
     * 
     * @return int
     * 
     * @throws \InvalidJsonFormatException
     */
    public function getCouponQuantity()
    {
        $rawJsonCoupons = $this->getJsonCoupons();
    
        if ($rawJsonCoupons === null) {
            throw new InvalidJsonFormatException;
        }
        
        return count($rawJsonCoupons);
    }
    
    
    /**
     * Fetches all coupons from a JSON cache file.
     *
     * @return array
     */
    protected function getJsonCoupons()
    {
        return json_decode(file_get_contents($this->rootDirectory->getAbsolutePath() . '/'
                                             . SunnyCashConfiguration::COUPON_CACHE_FILE));
    }
}