<?php
/* --------------------------------------------------------------
   SunnyCashCacheManager.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use HubPublic\Http\CurlRequest;

/**
 * Class SunnyCashCacheManager
 */
class SunnyCashCacheManager implements SunnyCashCacheManagerInterface
{
    /**
     * @var SunnyCashCouponFactory $couponFactory
     */
    protected $couponFactory;
    
    /**
     * @var CurlRequest $curl
     */
    protected $curl;
    
    /**
     * @var \ExistingDirectory $rootDirectory
     */
    protected $rootDirectory;
    
    /**
     * @var SunnyCashExpirationStrategyInterface $expirationStrategy
     */
    protected $expirationStrategy;
    
    
    /**
     * SunnyCashCacheManager constructor.
     *
     * @param \SunnyCashCouponFactory               $couponFactory
     * @param \SunnyCashExpirationStrategyInterface $expirationStrategy
     * @param \HubPublic\Http\CurlRequest           $curl
     * @param \ExistingDirectory                    $rootDirectory
     */
    public function __construct(
        SunnyCashCouponFactory $couponFactory,
        SunnyCashExpirationStrategyInterface $expirationStrategy,
        CurlRequest $curl,
        ExistingDirectory $rootDirectory
    ) {
        $this->couponFactory      = $couponFactory;
        $this->expirationStrategy = $expirationStrategy;
        $this->curl               = $curl;
        $this->rootDirectory      = $rootDirectory;
    }
    
    
    /**
     * Checks if the cache expired and refreshes it accordingly.
     */
    public function checkAndPerformRefresh()
    {
        if (!file_exists($this->rootDirectory->getAbsolutePath() . '/' . SunnyCashConfiguration::COUPON_CACHE_FILE)
            || $this->expirationStrategy->isExpired(new IntType($this->getModificationTime()))
        ) {
            $this->refresh();
        }
    }
    
    
    protected function getModificationTime()
    {
        return filemtime($this->rootDirectory->getAbsolutePath() . '/' . SunnyCashConfiguration::COUPON_CACHE_FILE);
    }
    
    
    protected function refresh()
    {
        $jsonCoupons      = $this->getJsonCoupons();
        $jsonCoupons      = json_encode($this->sortJsonCouponArray(json_decode($jsonCoupons)));
        $jsonCouponString = new StringType($jsonCoupons);
        $this->refreshJsonCouponCache($jsonCouponString);
        $this->refreshLogos($jsonCouponString);
    }
    
    
    protected function refreshJsonCouponCache(StringType $jsonCoupons)
    {
        file_put_contents($this->rootDirectory->getAbsolutePath() . '/' . SunnyCashConfiguration::COUPON_CACHE_FILE,
            $jsonCoupons->asString());
    }
    
    
    protected function refreshLogos(StringType $jsonCoupons)
    {
        /**
         * @var array $jsonCouponArray
         */
        $jsonCouponArray = json_decode($jsonCoupons->asString());
        
        foreach ($jsonCouponArray as $jsonCoupon) {
            $this->couponFactory->createFromJson($jsonCoupon)->storeLogo();
        }
    }
    
    
    protected function sortJsonCouponArray(array $jsonCoupons)
    {
        $prioritySort = function ($a, $b) {
            if (isset($a->position) && (!isset($b->position) || $b->position === null)) {
                return -1;
            }
            
            if ((!isset($a->position) || $a->position === null) && isset($b->position)) {
                return 1;
            }
            
            if (isset($a->position, $b->position) && $a->position !== $b->position) {
                return $a->position >= $b->position
                    ? 1
                    : -1;
            }
            
            return 0;
        };
        usort($jsonCoupons, $prioritySort);
        
        return $jsonCoupons;
    }
    
    
    /**
     * Performs a curl request to SunnyCash to retrieve all coupon data in raw JSON format.
     *
     * @return string All coupon data in raw JSON format
     *
     * @throws SunnyCashConnectionErrorException In case there is a connection error
     */
    protected function getJsonCoupons()
    {
        $response = $this->curl->setUrl(SunnyCashConfiguration::FEED_URL)->execute();
        
        if ($response->getStatusCode() !== 200) {
            throw new SunnyCashConnectionErrorException;
        }
        
        return $response->getBody();
    }
}