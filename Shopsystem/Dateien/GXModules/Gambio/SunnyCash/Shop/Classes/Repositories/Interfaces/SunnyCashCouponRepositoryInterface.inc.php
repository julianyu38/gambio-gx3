<?php
/* --------------------------------------------------------------
   SunnyCashCouponRepositoryInterface.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface SunnyCashCouponRepositoryInterface
 */
interface SunnyCashCouponRepositoryInterface
{
    /**
     * Gets a coupon by its ID.
     *
     * @param IdType $id
     *
     * @return SunnyCashCoupon
     */
    public function getCouponById(IdType $id);
    
    
    /**
     * Gets all available SunnyCash coupons.
     *
     * @param IntType $limit
     * @param IntType $offset
     *
     * @return SunnyCashCouponCollection
     */
    public function getAllCoupons(IntType $limit, IntType $offset);
    
    
    /**
     * Gets the total number of available SunnyCash coupons.
     *
     * @return int
     */
    public function getCouponQuantity();
}