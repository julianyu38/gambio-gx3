<?php
/* --------------------------------------------------------------
   SunnyCashLogoStorageWriter.inc.php 2018-01-30
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use HubPublic\Http\CurlRequest;

/**
 * Class SunnyCashLogoStorageWriter
 */
class SunnyCashLogoStorageWriter implements SunnyCashLogoStorageWriterInterface
{
    /**
     * @var \CurlRequest $curl
     */
    protected $curl;
    
    /**
     * @var \ExistingDirectory $rootDirectory
     */
    protected $rootDirectory;
    
    
    /**
     * SunnyCashLogoStorageWriter constructor.
     *
     * @param \HubPublic\Http\CurlRequest $curl
     * @param \ExistingDirectory          $rootDirectory
     */
    public function __construct(CurlRequest $curl, ExistingDirectory $rootDirectory)
    {
        $this->curl          = $curl;
        $this->rootDirectory = $rootDirectory;
    }
    
    
    /**
     * Gets a new version of the requested logo from SunnyCash.
     *
     * @param StringType $logoUrl The requested logo
     *
     * @throws SunnyCashConnectionErrorException
     */
    public function store(StringType $logoUrl)
    {
        if ($logoUrl->asString() === '') {
            return;
        }
        $imageString = $this->getLogoFromSunnyCash($logoUrl);
        $this->storeToFileSystem(new StringType($imageString), new StringType($this->generateFilename($logoUrl)));
    }
    
    
    /**
     * Gets the string representation of the requested logo.
     *
     * @param StringType $logoUrl
     *
     * @return string imageString of the logo
     *
     * @throws SunnyCashConnectionErrorException
     */
    protected function getLogoFromSunnyCash(StringType $logoUrl)
    {
        if ($logoUrl->asString() === '') {
            return '';
        }
        
        $response = $this->curl->setUrl($logoUrl->asString())->execute();
        
        if ($response->getStatusCode() !== 200) {
            throw new SunnyCashConnectionErrorException;
        }
        
        return $response->getBody();
    }
    
    
    /**
     * Stores the image file to the storage directory.
     *
     * @param StringType $image
     * @param StringType $filename
     */
    protected function storeToFileSystem(StringType $image, StringType $filename)
    {
        file_put_contents($this->rootDirectory->getAbsolutePath() . '/' . SunnyCashConfiguration::LOGO_STORAGE_DIRECTORY
                          . $filename->asString(), $image->asString());
    }
    
    
    /**
     * Replaces all characters of a string that are illegal in common filesystems with an underscore (_) and returns it.
     *
     * @param StringType $subject
     *
     * @return string
     */
    protected function generateFilename(StringType $subject)
    {
        return preg_replace('/[^a-z0-9_\.]/i', '_', $subject->asString());
    }
}