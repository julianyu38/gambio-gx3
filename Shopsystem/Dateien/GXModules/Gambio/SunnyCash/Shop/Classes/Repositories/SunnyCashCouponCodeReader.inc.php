<?php
/* --------------------------------------------------------------
   SunnyCashCouponCodeReader.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use HubPublic\Http\CurlRequest;

/**
 * Class SunnyCashCouponCodeReader
 */
class SunnyCashCouponCodeReader implements SunnyCashCouponCodeReaderInterface
{
    /**
     * @var CurlRequest $curl
     */
    protected $curl;
    
    
    /**
     * SunnyCashCouponCodeReader constructor.
     *
     * @param \HubPublic\Http\CurlRequest $curl
     */
    public function __construct(CurlRequest $curl)
    {
        $this->curl = $curl;
    }
    
    
    /**
     * Gets a coupon code for the requested coupon.
     *
     * @param IdType            $id
     * @param StringType        $transactionId
     * @param SunnyCashCustomer $customer
     *
     * @return SunnyCashCouponCode
     */
    public function getCode(IdType $id, StringType $transactionId, SunnyCashCustomer $customer)
    {
        $sessionCouponKey = 'SunnyCash_coupon_' . $id->asInt();
        
        if (!isset($_SESSION[$sessionCouponKey])) {
            $response                    = json_decode($this->requestCode($id, $transactionId, $customer));
            $code                        = $response->code;
            $_SESSION[$sessionCouponKey] = $code;
            
            return new SunnyCashCouponCode($code);
        }
        
        return MainFactory::create(SunnyCashCouponCode::class, $_SESSION[$sessionCouponKey]);
    }
    
    
    protected function requestCode(IdType $id, StringType $transactionId, SunnyCashCustomer $customer)
    {
        $postData = [
            'partner_id'     => SunnyCashConfiguration::PARTNER_ID,
            'key'            => SunnyCashConfiguration::API_KEY,
            'coupon_id'      => $id->asInt(),
            'transaction_id' => $transactionId->asString(),
            'customer'       => [
                'email'     => $customer->getEmail(),
                'firstname' => $customer->getFirstName(),
                'lastname'  => $customer->getLastName()
            ]
        ];
        
        $response = $this->curl->setOption(CURLOPT_POST, 1)
                               ->setOption(CURLOPT_POSTFIELDS, json_encode($postData))
                               ->setOption(CURLINFO_CONTENT_TYPE, 'application/json')
                               ->setUrl(SunnyCashConfiguration::COUPON_CODE_URL)
                               ->execute();
        
        if ($response->getStatusCode() !== 200) {
            throw new SunnyCashConnectionErrorException(json_encode($postData));
        }
        
        return $response->getBody();
    }
}