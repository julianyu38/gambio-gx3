<?php
/* --------------------------------------------------------------
   SunnyCashLogoStorageReader.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/


/**
 * Class SunnyCashLogoStorageReader
 */
class SunnyCashLogoStorageReader implements SunnyCashLogoStorageReaderInterface
{
    /**
     * @var SunnyCashLogoFactory $logoFactory
     */
    protected $logoFactory;
    
    /**
     * @var \ExistingDirectory $rootDirector
     */
    protected $rootDirectory;
    
    
    /**
     * SunnyCashLogoStorageReader constructor.
     *
     * @param \SunnyCashLogoFactory $logoFactory
     * @param \ExistingDirectory    $rootDirectory
     */
    public function __construct(SunnyCashLogoFactory $logoFactory, ExistingDirectory $rootDirectory)
    {
        $this->logoFactory = $logoFactory;
        $this->rootDirectory = $rootDirectory;
    }
    
    
    /**
     * Gets the requested logo from the filesystem.
     *
     * @param StringType $logoUrl The requested logo
     *
     * @return \SunnyCashLogo
     */
    public function getLogo(StringType $logoUrl)
    {
        if ($this->logoExists($logoUrl)) {
            return $this->logoFactory->createLogo($logoUrl, new StringType($this->generateFilename($logoUrl)));
        }
        
        return $this->logoFactory->createEmptyLogo();
    }
    
    
    /**
     * Checks if the requested logo is present.
     *
     * @param StringType $logoUrl The requested logo
     *
     * @return bool
     */
    protected function logoExists(StringType $logoUrl)
    {
        $filename = $this->generateFilename($logoUrl);
        
        return file_exists($this->rootDirectory->getAbsolutePath() . '/' . SunnyCashConfiguration::LOGO_STORAGE_DIRECTORY . $filename);
    }
    
    
    /**
     * Replaces all characters of a string that are illegal in common filesystems with an underscore (_) and returns it.
     * 
     * @param StringType $subject
     *
     * @return string
     */
    protected function generateFilename(StringType $subject)
    {
        return preg_replace('/[^a-z0-9_\.]/i', '_', $subject->asString());
    }
}