<?php
/* --------------------------------------------------------------
   SunnyCashCouponCollection.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashCouponCollection
 * 
 * @category   Core
 * @package    SunnyCash
 * @subpackage Collections
 */
class SunnyCashCouponCollection extends AbstractCollection
{
    /**
     * Return this collections valid type.
     *
     * @return string Valid type for group collection.
     */
    protected function _getValidType()
    {
        return SunnyCashCoupon::class;
    }
}