<?php
/* --------------------------------------------------------------
   SunnyCashLogo.inc.php 2018-01-31
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashLogo
 */
class SunnyCashLogo
{
    /**
     * @var SunnyCashLogoStorageWriterInterface $logoStorageWriter
     */
    protected $logoStorageWriter;
    
    /**
     * @var string $imageSource
     */
    protected $imageSource;
    
    /**
     * @var string $imagePath
     */
    protected $imagePath;
    
    
    /**
     * SunnyCashCoupon constructor.
     *
     * @param SunnyCashLogoStorageWriterInterface $logoStorageWriter
     * @param StringType                          $imageSource
     * @param StringType                          $imagePath
     */
    public function __construct(
        SunnyCashLogoStorageWriterInterface $logoStorageWriter,
        StringType $imageSource,
        StringType $imagePath
    ) {
        $this->logoStorageWriter = $logoStorageWriter;
        $this->imageSource       = $imageSource->asString();
        $this->imagePath         = $imagePath->asString();
    }
    
    
    public function store()
    {
        $this->logoStorageWriter->store(new StringType($this->imageSource));
    }
    
    
    public function getImagePath()
    {
        return DIR_FS_CATALOG . SunnyCashConfiguration::LOGO_STORAGE_DIRECTORY . $this->imagePath;
    }
    
    
    public function toImageString()
    {
        if ($this->imagePath === '') {
            return '';
        }
        
        return file_get_contents(DIR_FS_CATALOG . SunnyCashConfiguration::LOGO_STORAGE_DIRECTORY . $this->imagePath);
    }
}