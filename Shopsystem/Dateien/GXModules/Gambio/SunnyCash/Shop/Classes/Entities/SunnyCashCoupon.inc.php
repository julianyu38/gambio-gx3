<?php
/* --------------------------------------------------------------
   SunnyCashCoupon.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashCoupon
 */
class SunnyCashCoupon
{
    /**
     * @var SunnyCashLogoStorageWriterInterface $logoStorageWriter
     */
    protected $logoStorageWriter;
    
    /**
     * @var SunnyCashCouponCodeReaderInterface $couponCodeReader
     */
    protected $couponCodeReader;
    
    /**
     * @var int $id
     */
    protected $id;
    
    /**
     * @var string $name
     */
    protected $name;
    
    /**
     * @var string $description
     */
    protected $description;
    
    /**
     * @var string $url
     */
    protected $url;
    
    /**
     * @var string $partnerTitle
     */
    protected $partnerTitle;
    
    /**
     * @var string $shopLogo
     */
    protected $shopLogo;
    
    /**
     * @var string $type
     */
    protected $type;
    
    /**
     * @var string $transactionId
     */
    protected $transactionId;
    
    
    /**
     * SunnyCashCoupon constructor.
     *
     * @param SunnyCashLogoStorageWriterInterface $logoStorageWriter
     * @param SunnyCashCouponCodeReaderInterface  $couponCodeReader
     * @param IdType                              $id
     * @param StringType                          $name
     * @param StringType                          $description
     * @param StringType                          $url
     * @param StringType                          $partnerTitle
     * @param StringType                          $shopLogo
     * @param StringType                          $type
     */
    public function __construct(
        SunnyCashLogoStorageWriterInterface $logoStorageWriter,
        SunnyCashCouponCodeReaderInterface $couponCodeReader,
        IdType $id,
        StringType $name,
        StringType $description,
        StringType $url,
        StringType $partnerTitle,
        StringType $shopLogo,
        StringType $type
    ) {
        $this->logoStorageWriter = $logoStorageWriter;
        $this->couponCodeReader  = $couponCodeReader;
        $this->id                = $id->asInt();
        $this->name              = $name->asString();
        $this->description       = $description->asString();
        $this->url               = $url->asString();
        $this->partnerTitle      = $partnerTitle->asString();
        $this->shopLogo          = $shopLogo->asString();
        $this->type              = $type->asString();
        $this->transactionId     = $this->getRandomString();
    }
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    
    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->removePlaceholder(new StringType($this->url));
    }
    
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    
    /**
     * @return string
     */
    public function getPartnerTitle()
    {
        return $this->partnerTitle;
    }
    
    
    /**
     * @return string
     */
    public function getShopLogo()
    {
        return DIR_WS_CATALOG . SunnyCashConfiguration::LOGO_MANAGER_URL . '&source=' . $this->shopLogo;
    }
    
    
    /**
     * @return string
     */
    public function getRawShopLogo()
    {
        return $this->shopLogo;
    }
    
    
    /**
     * Stores the coupon logo to the filesystem.
     */
    public function storeLogo()
    {
        $this->logoStorageWriter->store(new StringType($this->shopLogo));
    }
    
    
    /**
     * @return SunnyCashCouponCode
     */
    public function getCode()
    {
        return $this->couponCodeReader->getCode(new IdType($this->id), new StringType($this->transactionId),
            MainFactory::create(SunnyCashCustomer::class, 'Max', 'Mustermann', 'm.mustermann@shop.de'));
    }
    
    
    /**
     * Removes the "_SOURCE_TRANSACTION_ID_" placeholder from SunnyCash URLs.
     *
     * @param StringType $url
     *
     * @return string
     */
    protected function removePlaceholder(StringType $url)
    {
        return str_replace('_SOURCE_TRANSACTION_ID_', $this->transactionId, $url->asString());
    }
    
    
    /**
     * Generates a random (most probably unique) string.
     *
     * @return string
     */
    protected function getRandomString()
    {
        return md5(mt_rand() . '-' . time());
    }
}