<?php
/* --------------------------------------------------------------
   SunnyCashConfiguration.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class SunnyCashConfiguration
 */
class SunnyCashConfiguration
{
    const FEED_URL = 'https://www.sunnycash.de/api/2.0/coupons?partner_id=hfc4b33w&key=833a0c2c9bbfa2e421bcd973b4720387';
    
    const COUPON_CODE_URL = 'https://www.sunnycash.de/api/2.0/coupons/code';
    
    const LOGO_MANAGER_URL = 'shop.php?do=SunnyCashLogo';
    
    const LOGO_STORAGE_DIRECTORY = 'images/sunnycash_logos/';
    
    const COUPON_CACHE_FILE = 'cache/sunnycash_coupon_cache.json';
    
    const PARTNER_ID = 'hfc4b33w';
    
    const API_KEY = '833a0c2c9bbfa2e421bcd973b4720387';
}