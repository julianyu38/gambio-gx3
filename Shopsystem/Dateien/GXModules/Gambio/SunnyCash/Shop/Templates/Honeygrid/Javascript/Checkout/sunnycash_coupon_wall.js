/* --------------------------------------------------------------
 sunnycash_coupon_wall.js 2018-08-28
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

if (location.href.search(/checkout_success\.php/) !== -1) {
    $.ajax({
        "url": 'shop.php?do=SunnyCash',
        "type": "GET"
    }).done(function(data) {
        const sunnycashCouponWall = $('#sunnycash-coupon-wall');
        if (sunnycashCouponWall) {
            sunnycashCouponWall.html(data);
            sunnycash.widgets.init($('body'));
        }
    });
}
