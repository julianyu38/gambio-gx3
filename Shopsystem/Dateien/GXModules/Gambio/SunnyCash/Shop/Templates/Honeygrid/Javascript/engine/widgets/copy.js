/* --------------------------------------------------------------
 copy.js 2018-02-04
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Copys the text of an HTML element defined by a given selector.
 */
sunnycash.widgets.module('copy', [], function(data) {
	
	'use strict';
	
	// ########## VARIABLE INITIALIZATION ##########
	
	let $this = $(this),
		defaults = {
			selector: 'body'
		},
		options = $.extend(true, {}, defaults, data),
		module = {};
	
	// ########## INITIALIZATION ##########
	
	let copyHandler = function() {
		const copyTarget = document.querySelector(options.selector);
		copyTarget.select();
		
		try {
			document.execCommand('copy');
		} catch (err) {
			jse.core.debug.log('Error occurred when copying!');
		}
	};
	
	
	/**
	 * Init function of the widget
	 * @constructor
	 */
	module.init = function(done) {
		$this.on('click', copyHandler);
		
		done();
	};
	
	// Return data to widget engine
	return module;
});
