'use strict';

/* --------------------------------------------------------------
 pagination.js 2018-03-04
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles behaviour of the pagination buttons and toggles their visibility accordingly.
 */
sunnycash.widgets.module('pagination', [], function(data) {
	
	'use strict';
	
	// ########## VARIABLE INITIALIZATION ##########
	
	let $this = $(this),
		defaults = {
			page: 1
		},
		options = $.extend(true, {}, defaults, data),
		module = {};
	
	// ########## FUNCTIONS ##########
	
	let backHandler = function() {
		options.page = Math.max(options.page - 1, 1);
		requestCoupons();
	};
	
	let forwardHandler = function() {
		options.page += 1;
		requestCoupons();
	};
	
	let requestCoupons = function() {
		$.ajax({
			"url": 'shop.php?do=SunnyCash&page=' + options.page,
			"type": "GET"
		}).done(function(data) {
			const sunnycashCouponWall = $('#sunnycash-coupon-wall');
			if (sunnycashCouponWall) {
				sunnycashCouponWall.html(data);
				sunnycash.widgets.init($('body'));
			}
		});
		showNavigation();
	};
	
	let showNavigation = function() {
		showForwardNavigation();
		showBackNavigation();
	};
	
	let showBackNavigation = function() {
		const back = $this.find('.sunnycash-coupon-wall-navigation.back');
		
		if (options.page > 1) {
			back.css('visibility', 'visible');
		}
		else {
			back.css('visibility', 'hidden');
		}
	};
	
	let showForwardNavigation = function() {
		const forward = $this.find('.sunnycash-coupon-wall-navigation.forward');
		
		$.ajax({
			"url": 'shop.php?do=SunnyCash/GetPageCount',
			"type": "GET"
		}).done(function(data) {
			if (options.page < JSON.parse(data).pageCount) {
				forward.css('visibility', 'visible');
			}
			else {
				forward.css('visibility', 'hidden');
			}
		});
	};
	
	// ########## INITIALIZATION ##########
	
	/**
	 * Init function of the widget
	 * @constructor
	 */
	module.init = function(done) {
		$this.find('.sunnycash-coupon-wall-navigation.back').on('click', backHandler);
		$this.find('.sunnycash-coupon-wall-navigation.forward').on('click', forwardHandler);
		showNavigation();
		
		done();
	};
	
	// Return data to widget engine
	return module;
});
