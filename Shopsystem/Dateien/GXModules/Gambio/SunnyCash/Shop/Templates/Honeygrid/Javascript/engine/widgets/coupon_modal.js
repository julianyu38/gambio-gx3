'use strict';

/* --------------------------------------------------------------
 coupon_modal.js 2018-02-02
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Opens a modal with a coupon code, in case the offer is of type coupon.
 */
sunnycash.widgets.module('coupon_modal', [jse.source + '/vendor/bootstrap-sass/bootstrap'], function (data) {

	'use strict';

	// ########## VARIABLE INITIALIZATION ##########

	let $this = $(this),
	    defaults = {},
	    options = $.extend(true, {}, defaults, data),
	    module = {};

	// ########## INITIALIZATION ##########

 /**
  * Init function of the widget
  * @constructor
  */
	module.init = function (done) {

		if ($this.hasClass('has-code')) {
			$this.on('click', function (e) {
				e.preventDefault();
				
				$.ajax({
					"url": "shop.php?do=SunnyCash/Code",
					"type": "POST",
					"data": {
						"coupon_id": $this.attr('data-coupon_id')
					}
				}).done(function(data) {
					const targetModal = $this.attr('data-modal_target');
					
					$(targetModal).html(data);
					sunnycash.widgets.init($(targetModal));
					
					$(targetModal).modal('show');
				});
			});
		}

		done();
	};

	// Return data to widget engine
	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpZGdldHMvY291cG9uX21vZGFsLmpzIl0sIm5hbWVzIjpbImdhbWJpbyIsIndpZGdldHMiLCJtb2R1bGUiLCJqc2UiLCJzb3VyY2UiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJvcHRpb25zIiwiZXh0ZW5kIiwiaW5pdCIsImRvbmUiLCJvbiIsImUiLCJwcmV2ZW50RGVmYXVsdCIsInRhcmdldE1vZGFsIiwiYXR0ciIsIm1vZGFsIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBV0FBLE9BQU9DLE9BQVAsQ0FBZUMsTUFBZixDQUNDLGNBREQsRUFHQyxDQUNDQyxJQUFJQyxNQUFKLEdBQWEsa0NBRGQsQ0FIRCxFQU9DLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTs7QUFFQSxLQUFJQyxRQUFRQyxFQUFFLElBQUYsQ0FBWjtBQUFBLEtBQ0NDLFdBQVcsRUFEWjtBQUFBLEtBRUNDLFVBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBRlg7QUFBQSxLQUdDSCxTQUFTLEVBSFY7O0FBS0E7OztBQUdBOztBQUVBOzs7O0FBSUFBLFFBQU9TLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7O0FBRTVCTCxJQUFFLHlCQUFGLEVBQTZCTSxFQUE3QixDQUFnQyxPQUFoQyxFQUF5QyxVQUFTQyxDQUFULEVBQVk7QUFDcERBLEtBQUVDLGNBQUY7O0FBRUEsT0FBSUMsY0FBY1QsRUFBRSxJQUFGLEVBQVFVLElBQVIsQ0FBYSxtQkFBYixDQUFsQjs7QUFFQVYsS0FBRVMsV0FBRixFQUFlRSxLQUFmLENBQXFCLE1BQXJCO0FBQ0EsR0FORDs7QUFRQU47QUFDQSxFQVhEOztBQWFBO0FBQ0EsUUFBT1YsTUFBUDtBQUNBLENBMUNGIiwiZmlsZSI6IndpZGdldHMvY291cG9uX21vZGFsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGNvdXBvbl9tb2RhbC5qcyAyMDE4LTAyLTA0XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuXHJcbmdhbWJpby53aWRnZXRzLm1vZHVsZShcclxuXHQnY291cG9uX21vZGFsJyxcclxuXHRcclxuXHRbXHJcblx0XHRqc2Uuc291cmNlICsgJy92ZW5kb3IvYm9vdHN0cmFwLXNhc3MvYm9vdHN0cmFwJ1xyXG5cdF0sXHJcblx0XHJcblx0ZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0XHJcblx0XHQndXNlIHN0cmljdCc7XHJcblx0XHRcclxuXHRcdC8vICMjIyMjIyMjIyMgVkFSSUFCTEUgSU5JVElBTElaQVRJT04gIyMjIyMjIyMjI1xyXG5cdFx0XHJcblx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpLFxyXG5cdFx0XHRkZWZhdWx0cyA9IHt9LFxyXG5cdFx0XHRvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKSxcclxuXHRcdFx0bW9kdWxlID0ge307XHJcblx0XHRcclxuXHRcdC8vICMjIyMjIyMjIyMgRVZFTlQgSEFORExFUiAjIyMjIyMjIyMjXHJcblx0XHRcclxuXHRcdFxyXG5cdFx0Ly8gIyMjIyMjIyMjIyBJTklUSUFMSVpBVElPTiAjIyMjIyMjIyMjXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogSW5pdCBmdW5jdGlvbiBvZiB0aGUgd2lkZ2V0XHJcblx0XHQgKiBAY29uc3RydWN0b3JcclxuXHRcdCAqL1xyXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XHJcblx0XHRcdFxyXG5cdFx0XHQkKCcuaGFzLWNvZGUgYS5jb3Vwb24tbGluaycpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcclxuXHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0dmFyIHRhcmdldE1vZGFsID0gJCh0aGlzKS5hdHRyKCdkYXRhLW1vZGFsX3RhcmdldCcpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdCQodGFyZ2V0TW9kYWwpLm1vZGFsKCdzaG93Jyk7XHJcblx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0ZG9uZSgpO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0Ly8gUmV0dXJuIGRhdGEgdG8gd2lkZ2V0IGVuZ2luZVxyXG5cdFx0cmV0dXJuIG1vZHVsZTtcclxuXHR9KTsiXX0=
