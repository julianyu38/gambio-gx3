'use strict';

/* --------------------------------------------------------------
 scroller.js 2018-05-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Handles behaviour of the pagination buttons and toggles their visibility accordingly.
 */
sunnycash.widgets.module(
	// ------------------------------------------------------------------------
	// WIDGET NAME
	// ------------------------------------------------------------------------
	'scroller',
	
	// ------------------------------------------------------------------------
	// LIBRARIES
	// ------------------------------------------------------------------------
	[gambio.source + '/libs/events'],
	
	// ------------------------------------------------------------------------
	// BUSINESS LOGIC
	// ------------------------------------------------------------------------
	function(data) {
		'use strict';
		
		// ------------------------------------------------------------------------
		// VARIABLES
		// ------------------------------------------------------------------------
		
		/**
		 * Controller reference.
		 *
		 * @type {jQuery}
		 */
		const $this = $(this);
		
		/**
		 * Default options for controller,
		 *
		 * @type {object}
		 */
		const defaults = {};
		
		/**
		 * Final controller options.
		 *
		 * @type {object}
		 */
		const options = $.extend(true, {}, defaults, data);
		
		/**
		 * Module object.
		 *
		 * @type {{}}
		 */
		const module = {};
		
		/**
		 * Window object.
		 *
		 * @type {object}
		 */
		const $window = $(window);
		
		
		// ------------------------------------------------------------------------
		// EVENT HANDLER
		// ------------------------------------------------------------------------
		
		/**
		 * Handler for window scrolling.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function scrollHandler() {
			const $firstCoupon = $('span.coupon-container').first();
			if ($firstCoupon.length === 0) {
				return;
			}
			
			const isVisible = $firstCoupon.offset().top <= $window.scrollTop() + window.innerHeight
				- $firstCoupon.height();
			
			if (isVisible) {
				$this.trigger(jse.libs.template.events.TRANSITION(), {
					open: false,
					classOpen: 'visible'
				});
			} else if (!isVisible) {
				$this.trigger(jse.libs.template.events.TRANSITION(), {
					open: true,
					classOpen: 'visible'
				});
			}
		}
		
		/**
		 * Handler for scroller click event.
		 *
		 * @param {object} event jQuery event object contains information of the event.
		 */
		function clickHandler(event) {
			event.preventDefault();
			
			const $firstCoupon = $('span.coupon-container').first();
			if ($firstCoupon.length === 0) {
				return;
			}
			
			$('html, body').animate({scrollTop: $firstCoupon.offset().top - $('header').height()}, options.duration);
		}
		
		
		// ------------------------------------------------------------------------
		// INITIALIZATION
		// ------------------------------------------------------------------------
		module.init = function(done) {
			$window.on('scroll', scrollHandler);
			$this.on('click', clickHandler);
			
			$this.trigger(jse.libs.template.events.TRANSITION(), {
				open: true,
				classOpen: 'visible'
			});
			
			done();
		};
		
		return module;
	}
);