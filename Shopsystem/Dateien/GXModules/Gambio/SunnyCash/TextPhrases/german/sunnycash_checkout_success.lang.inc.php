<?php
/* --------------------------------------------------------------
   sunnycash_checkout_success.lang.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'sunnycash_coupon_choose_now'         => 'Jetzt auswählen',
	'sunnycash_coupon_headline'           => 'Wählen Sie Ihren Dankeschön-Gutschein',
	'sunnycash_coupon_info'               => 'Als kleines Dankeschön haben Sie nun die Möglichkeit einen Gutschein auszuwählen:',
	'sunnycash_coupon_modal_code_info'    => 'Ihr Code:',
	'sunnycash_coupon_modal_copy_btn'     => 'Code in die Zwischenablage kopieren',
	'sunnycash_coupon_modal_headline'     => 'Ihr Gutschein für',
	'sunnycash_coupon_modal_offer_btn'    => 'Zum Angebot',
	'sunnycash_coupon_navigation_back'    => 'Zurück',
	'sunnycash_coupon_navigation_forward' => 'Weiter',
	'sunnycash_coupon_scroller_text'      => 'Unser Dankeschön <br /> für Sie - Hier klicken!',
);
