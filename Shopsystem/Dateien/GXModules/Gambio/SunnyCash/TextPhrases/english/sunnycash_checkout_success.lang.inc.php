<?php
/* --------------------------------------------------------------
   sunnycash_checkout_success.lang.inc.php 2018-05-23
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
    'sunnycash_coupon_choose_now'         => 'Choose now',
    'sunnycash_coupon_headline'           => 'Choose your thank-you coupon',
    'sunnycash_coupon_info'               => 'As a little thank-you you have the opportunity to choose a coupon now:',
    'sunnycash_coupon_modal_code_info'    => 'Your code:',
    'sunnycash_coupon_modal_copy_btn'     => 'Copy code to clipboard',
    'sunnycash_coupon_modal_headline'     => 'Your coupon for',
    'sunnycash_coupon_modal_offer_btn'    => 'To offer',
    'sunnycash_coupon_navigation_back'    => 'Back',
    'sunnycash_coupon_navigation_forward' => 'Forward',
    'sunnycash_coupon_scroller_text'      => 'Our thank-you <br /> for you - Click here!',
);
