<?php
/* --------------------------------------------------------------
   sunnycash.lang.inc.php 2018-09-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
    'title'            => 'SunnyCash',
    'description'      => 'Thank-you coupons can be a positive addition to a successful order and increase customer loyalty.',
    'configurations'   => 'Configurations',
    'coupons_per_page' => 'Coupons per page'
];