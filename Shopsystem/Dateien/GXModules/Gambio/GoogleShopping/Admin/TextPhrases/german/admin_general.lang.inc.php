<?php
/* --------------------------------------------------------------
	admin_general.lang.inc.php 2017-11-21
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'TEXT_GOOGLE_ADWORDS_CONNECTED'    => 'Google verbunden',
	'TEXT_GOOGLE_ADWORDS_DISCONNECTED' => 'Google nicht verbunden'
];
