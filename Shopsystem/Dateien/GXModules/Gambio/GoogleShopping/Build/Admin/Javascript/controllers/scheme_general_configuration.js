'use strict';

/* --------------------------------------------------------------
 scheme_general_configuration.js 2017-11-21
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gxmodules.controllers.module('scheme_general_configuration', [], function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {
		'exportTypeSelection': '#cronjob-hour-interval-selection',
		'hourSelection': '#cronjob-hour',
		'intervalSelection': '#cronjob-interval'
	};

	/**
  * Final Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// HELPER FUNCTIONS
	// ------------------------------------------------------------------------

	function _enableIntervalSelection() {
		$(options.hourSelection).attr('disabled', 'disabled');
		$(options.intervalSelection).removeAttr('disabled');
	}

	function _enableHourSelection() {
		$(options.intervalSelection).attr('disabled', 'disabled');
		$(options.hourSelection).removeAttr('disabled');
	}

	function _checkExportTypeSelection() {
		var selectionValue = $(options.exportTypeSelection).val();

		if (selectionValue === 'hour') {
			_enableHourSelection();
		} else {
			_enableIntervalSelection();
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {

		$(document).ready(function () {
			_checkExportTypeSelection();
		});

		$(options.exportTypeSelection).on('change', function () {
			_checkExportTypeSelection();
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvY29udHJvbGxlcnMvc2NoZW1lX2dlbmVyYWxfY29uZmlndXJhdGlvbi5qcyJdLCJuYW1lcyI6WyJneG1vZHVsZXMiLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJfZW5hYmxlSW50ZXJ2YWxTZWxlY3Rpb24iLCJob3VyU2VsZWN0aW9uIiwiYXR0ciIsImludGVydmFsU2VsZWN0aW9uIiwicmVtb3ZlQXR0ciIsIl9lbmFibGVIb3VyU2VsZWN0aW9uIiwiX2NoZWNrRXhwb3J0VHlwZVNlbGVjdGlvbiIsInNlbGVjdGlvblZhbHVlIiwiZXhwb3J0VHlwZVNlbGVjdGlvbiIsInZhbCIsImluaXQiLCJkb25lIiwiZG9jdW1lbnQiLCJyZWFkeSIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUFBLFVBQVVDLFdBQVYsQ0FBc0JDLE1BQXRCLENBQ0MsOEJBREQsRUFHQyxFQUhELEVBS0MsVUFBU0MsSUFBVCxFQUFlO0FBQ2Q7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCLHlCQUF1QixrQ0FEUDtBQUVoQixtQkFBaUIsZUFGRDtBQUdoQix1QkFBcUI7QUFITCxFQUFqQjs7QUFNQTs7Ozs7QUFLQSxLQUFNQyxVQUFVRixFQUFFRyxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJGLFFBQW5CLEVBQTZCSCxJQUE3QixDQUFoQjs7QUFFQTs7Ozs7QUFLQSxLQUFNRCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFVBQVNPLHdCQUFULEdBQW9DO0FBQ25DSixJQUFFRSxRQUFRRyxhQUFWLEVBQXlCQyxJQUF6QixDQUE4QixVQUE5QixFQUEwQyxVQUExQztBQUNBTixJQUFFRSxRQUFRSyxpQkFBVixFQUE2QkMsVUFBN0IsQ0FBd0MsVUFBeEM7QUFDQTs7QUFFRCxVQUFTQyxvQkFBVCxHQUFnQztBQUMvQlQsSUFBRUUsUUFBUUssaUJBQVYsRUFBNkJELElBQTdCLENBQWtDLFVBQWxDLEVBQThDLFVBQTlDO0FBQ0FOLElBQUVFLFFBQVFHLGFBQVYsRUFBeUJHLFVBQXpCLENBQW9DLFVBQXBDO0FBQ0E7O0FBRUQsVUFBU0UseUJBQVQsR0FBc0M7QUFDckMsTUFBTUMsaUJBQWlCWCxFQUFFRSxRQUFRVSxtQkFBVixFQUErQkMsR0FBL0IsRUFBdkI7O0FBRUEsTUFBSUYsbUJBQW1CLE1BQXZCLEVBQStCO0FBQzlCRjtBQUNBLEdBRkQsTUFFTztBQUNOTDtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBUCxRQUFPaUIsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTs7QUFFNUJmLElBQUVnQixRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVztBQUM1QlA7QUFDQSxHQUZEOztBQUlBVixJQUFFRSxRQUFRVSxtQkFBVixFQUErQk0sRUFBL0IsQ0FBa0MsUUFBbEMsRUFBNEMsWUFBVztBQUN0RFI7QUFDQSxHQUZEOztBQUlBSztBQUNBLEVBWEQ7O0FBYUEsUUFBT2xCLE1BQVA7QUFDQSxDQXRGRiIsImZpbGUiOiJBZG1pbi9KYXZhc2NyaXB0L2NvbnRyb2xsZXJzL3NjaGVtZV9nZW5lcmFsX2NvbmZpZ3VyYXRpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIHNjaGVtZV9nZW5lcmFsX2NvbmZpZ3VyYXRpb24uanMgMjAxNy0xMS0yMVxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmd4bW9kdWxlcy5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdzY2hlbWVfZ2VuZXJhbF9jb25maWd1cmF0aW9uJyxcblx0XG5cdFtdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBTZWxlY3RvclxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGRlZmF1bHRzID0ge1xuXHRcdFx0J2V4cG9ydFR5cGVTZWxlY3Rpb24nOiAnI2Nyb25qb2ItaG91ci1pbnRlcnZhbC1zZWxlY3Rpb24nLFxuXHRcdFx0J2hvdXJTZWxlY3Rpb24nOiAnI2Nyb25qb2ItaG91cicsXG5cdFx0XHQnaW50ZXJ2YWxTZWxlY3Rpb24nOiAnI2Nyb25qb2ItaW50ZXJ2YWwnXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSEVMUEVSIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdGZ1bmN0aW9uIF9lbmFibGVJbnRlcnZhbFNlbGVjdGlvbigpIHtcblx0XHRcdCQob3B0aW9ucy5ob3VyU2VsZWN0aW9uKS5hdHRyKCdkaXNhYmxlZCcsICdkaXNhYmxlZCcpO1xuXHRcdFx0JChvcHRpb25zLmludGVydmFsU2VsZWN0aW9uKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuXHRcdH1cblx0XHRcblx0XHRmdW5jdGlvbiBfZW5hYmxlSG91clNlbGVjdGlvbigpIHtcblx0XHRcdCQob3B0aW9ucy5pbnRlcnZhbFNlbGVjdGlvbikuYXR0cignZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcblx0XHRcdCQob3B0aW9ucy5ob3VyU2VsZWN0aW9uKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuXHRcdH1cblx0XHRcblx0XHRmdW5jdGlvbiBfY2hlY2tFeHBvcnRUeXBlU2VsZWN0aW9uICgpIHtcblx0XHRcdGNvbnN0IHNlbGVjdGlvblZhbHVlID0gJChvcHRpb25zLmV4cG9ydFR5cGVTZWxlY3Rpb24pLnZhbCgpO1xuXHRcdFx0XG5cdFx0XHRpZiAoc2VsZWN0aW9uVmFsdWUgPT09ICdob3VyJykge1xuXHRcdFx0XHRfZW5hYmxlSG91clNlbGVjdGlvbigpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0X2VuYWJsZUludGVydmFsU2VsZWN0aW9uKCk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRcblx0XHRcdCQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRfY2hlY2tFeHBvcnRUeXBlU2VsZWN0aW9uKCk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JChvcHRpb25zLmV4cG9ydFR5cGVTZWxlY3Rpb24pLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0X2NoZWNrRXhwb3J0VHlwZVNlbGVjdGlvbigpXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0ZG9uZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0cmV0dXJuIG1vZHVsZTtcblx0fSk7Il19
