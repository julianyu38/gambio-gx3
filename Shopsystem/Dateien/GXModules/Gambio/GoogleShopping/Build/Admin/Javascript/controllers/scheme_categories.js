'use strict';

/* --------------------------------------------------------------
 scheme_categories.js 2017-11-20
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gxmodules.controllers.module('scheme_categories', [], function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	var
	/**
  * Module Selector
  *
  * @type {jQuery}
  */
	$this = $(this),


	/**
  * Default Options
  *
  * @type {object}
  */
	defaults = {
		bequeathingState: 'input.bequeathing-state',
		categoryCheckbox: '.category-checkbox',
		categoryFolder: '.category-folder',
		categoryState: 'input.category-state',
		categorySubtree: 'ul.subtree',
		checkboxStates: ['self_all_sub_checked', 'self_some_sub_checked', 'self_no_sub_checked', 'no_self_all_sub_checked', 'no_self_some_sub_checked', 'no_self_no_sub_checked'],
		defaultCheckboxState: 'no_self_no_sub_checked',
		foldedClassName: 'folded',
		loadedClassName: 'loaded',
		schemeId: 0,
		selectAllCategories: '.select-all-categories',
		subcategoriesUrl: 'admin.php?do=GoogleShoppingAjax/getSubcategories',
		subtreeClassName: 'subtree',
		unfoldedClassName: 'unfolded'
	},


	/**
  * Final Options
  *
  * @type {object}
  */
	options = $.extend(true, {}, defaults, data),


	/**
  * Module Object
  *
  * @type {object}
  */
	module = {};

	// ------------------------------------------------------------------------
	// HELPER FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Sets the "select all categories" checkbox to checked, if all categories are selected.
  *
  * @private
  */
	function _updateSelectAllCategoriesCheckbox() {
		var allChecked = true;
		$this.find(options.categoryState).each(function () {
			allChecked &= $(this).val() === 'self_all_sub';
		});

		if (allChecked) {
			$this.find(options.selectAllCategories).prop('checked', true);
		}
	};

	/**
  * Sets the state of all children checkboxes to given checkState value.
  *
  * @param $checkbox
  * @param checkState
  * @private
  */
	function _updateStateOfChildrenCheckboxes($checkbox, checkState) {
		var $children = $checkbox.parent('li').children(options.categorySubtree).children('li').children(options.categoryCheckbox);

		if ($children.size() > 0) {
			$children.each(function () {
				_setCheckboxState($(this), checkState);
				_updateStateOfChildrenCheckboxes($(this), checkState);
			});
		}
	};

	/**
  * Updates the state of all parent checkboxes.
  *
  * @param $checkbox
  * @private
  */
	function _updateStateOfParentCheckboxes($checkbox) {
		var $subtree = $checkbox.closest(options.categorySubtree);

		if ($subtree.hasClass(options.subtreeClassName)) {
			var $parentCheckbox = $subtree.parent('li').children(options.categoryCheckbox);
			var checkState = _determineCheckboxState($parentCheckbox);
			_setCheckboxState($parentCheckbox, checkState);
			_updateStateOfParentCheckboxes($parentCheckbox);
		}
	};

	/**
  * Determines the state for the given checkbox.
  *
  * @param $checkbox
  * @returns {string}
  * @private
  */
	function _determineCheckboxState($checkbox) {
		var selfChecked = _isChecked($checkbox);
		var $childCategories = $checkbox.parent('li').children(options.categorySubtree).children('li').children(options.categoryCheckbox);
		var childCount = $childCategories.length;
		var childCheckCount = 0;
		var checkState = options.defaultCheckboxState;

		$childCategories.each(function () {
			if (!$(this).hasClass('no_self_no_sub_checked')) {
				childCheckCount++;
			}
			if (!$(this).hasClass('self_all_sub_checked')) {
				childCount++;
			}
		});

		if (selfChecked) {
			if (childCheckCount === childCount) {
				checkState = 'self_all_sub_checked';
			} else if (childCheckCount > 0) {
				checkState = 'self_some_sub_checked';
			} else if (childCheckCount === 0) {
				checkState = 'self_no_sub_checked';
			}
		} else {
			if (childCheckCount === childCount) {
				checkState = 'no_self_all_sub_checked';
			} else if (childCheckCount > 0) {
				checkState = 'no_self_some_sub_checked';
			} else if (childCheckCount === 0) {
				checkState = 'no_self_no_sub_checked';
			}
		}

		return checkState;
	};

	/**
  * Sets the state of the given checkbox.
  *
  * @param $checkbox
  * @param state
  * @private
  */
	function _setCheckboxState($checkbox, state) {
		_resetCheckbox($checkbox);
		$checkbox.addClass(state);
		$checkbox.parent('li').children(options.categoryState).val(state.substring(0, state.lastIndexOf('_')));
	};

	/**
  * Resets the state of the given checkbox.
  *
  * @param $checkbox
  * @private
  */
	function _resetCheckbox($checkbox) {
		for (var i = 0; i < options.checkboxStates.length; i++) {
			$checkbox.removeClass(options.checkboxStates[i]);
		}
	};

	/**
  * Returns the state of the given checkbox.
  *
  * @param $checkbox
  * @returns {string}
  * @private
  */
	function _getCheckboxState($checkbox) {
		for (var i = 0; i < options.checkboxStates.length; i++) {
			if ($checkbox.hasClass(options.checkboxStates[i])) {
				return options.checkboxStates[i];
			}
		}
		return options.defaultCheckboxState;
	};

	/**
  * Determines and sets new status of given checkbox.
  *
  * @param $checkbox
  * @returns {string}
  * @private
  */
	function _determineNewCheckboxState($checkbox) {
		var actualState = _getCheckboxState($checkbox);
		var newState = options.defaultCheckboxState;

		if ($checkbox.parent('li').children(options.categoryFolder).hasClass(options.foldedClassName) || $checkbox.parent('li').children(options.categoryFolder).hasClass(options.unfoldedClassName) && _getChildCount($checkbox) === 0) {
			switch (actualState) {
				case 'self_all_sub_checked':
				case 'self_some_sub_checked':
				case 'self_no_sub_checked':
					newState = 'no_self_no_sub_checked';
					break;

				case 'no_self_all_sub_checked':
				case 'no_self_some_sub_checked':
				case 'no_self_no_sub_checked':
					newState = 'self_all_sub_checked';
					break;

				default:
					break;
			}
		} else {
			switch (actualState) {
				case 'self_all_sub_checked':
					newState = 'no_self_all_sub_checked';
					break;

				case 'self_some_sub_checked':
					newState = 'no_self_some_sub_checked';
					break;

				case 'self_no_sub_checked':
					newState = 'no_self_no_sub_checked';
					break;

				case 'no_self_all_sub_checked':
					newState = 'self_all_sub_checked';
					break;

				case 'no_self_some_sub_checked':
					newState = 'self_some_sub_checked';
					break;

				case 'no_self_no_sub_checked':
					newState = 'self_no_sub_checked';
					break;

				default:
					break;
			}
		}

		return newState;
	};

	/**
  * Checks if given checkbox is checked.
  *
  * @param $checkbox
  * @returns {boolean}
  * @private
  */
	function _isChecked($checkbox) {
		return $checkbox.hasClass('self_all_sub_checked') || $checkbox.hasClass('self_some_sub_checked') || $checkbox.hasClass('self_no_sub_checked');
	};

	/**
  * Returns the count of children of the given checkbox. 
  * 
  * @param $checkbox
  * @returns {int}
  * @private
  */
	function _getChildCount($checkbox) {
		return $checkbox.parent('li').children(options.categorySubtree).children('li').size();
	};

	/**
  * Returns the inherited state of children checkboxes if exist, otherwise false.
  * 
  * @param $checkbox
  * @returns {string|boolean}
  * @private
  */
	function _getInheritedStateOfChildren($checkbox) {
		while ($checkbox.hasClass(options.subtreeClassName)) {
			if ($checkbox.parent('li').children(options.categoryCheckbox).hasClass('pass_on_no_self_no_sub_checked')) {
				return 'no_self_no_sub_checked';
			} else if ($checkbox.parent('li').children(options.categoryCheckbox).hasClass('pass_on_self_all_sub_checked')) {
				return 'self_all_sub_checked';
			}

			$checkbox = $checkbox.parent('li').closest(options.categorySubtree);
		}

		return false;
	};

	/**
  * Updates the folder icon and its class (folded / unfolded).
  * 
  * @param $folder
  * @private
  */
	function _toggleFolder($folder) {
		$folder.toggleClass(options.unfoldedClassName).toggleClass(options.foldedClassName);
		$folder.find('i.fa').toggleClass('fa-folder').toggleClass('fa-folder-open');
	}

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Handles the click event of a category.
  * 
  * Loads subcategories of clicked category.
  * 
  * @private
  */
	function _clickCategory() {
		var categoryId = $(this).data('category-id');
		var $this = $(this);

		if ($this.hasClass(options.loadedClassName) && $this.hasClass(options.foldedClassName)) {
			$this.parent('li').find(options.categorySubtree).show();
			_toggleFolder($this);
		} else if ($this.hasClass(options.loadedClassName) && $this.hasClass(options.unfoldedClassName)) {
			$this.parent('li').find(options.categorySubtree).hide();
			_toggleFolder($this);
		} else {
			$.ajax({
				type: "GET",
				url: options.subcategoriesUrl + '&schemeId=' + options.schemeId + '&categoryId=' + categoryId,
				success: function success(response) {
					response = JSON.parse(response);

					if (response['success'] === true) {
						$this.parent('li').find(options.categorySubtree + ':first').html(response.html);
						$this.parent('li').find(options.categorySubtree + ':first ' + options.categoryFolder).on('click', _clickCategory);
						$this.parent('li').find(options.categorySubtree + ':first ' + options.categoryCheckbox).on('click', _clickCheckbox);
						$this.addClass(options.loadedClassName);

						_toggleFolder($this);

						var inheritedState = _getInheritedStateOfChildren($this.parent('li').children(options.categorySubtree));
						if (inheritedState) {
							$this.parent('li').children(options.categorySubtree).children('li').children(options.categoryCheckbox).each(function () {
								_setCheckboxState($(this), inheritedState);
							});
						}
					}

					return response['success'];
				},
				error: function error() {
					return false;
				}
			});
		}
	}

	/**
  * Handles the click event of checkboxes.
  * 
  * Updates states of checkboxes.
  * 
  * @private
  */
	function _clickCheckbox() {
		var $checkbox = $(this);
		var checked = _isChecked($checkbox) ? 'no_self_no_sub_checked' : 'self_all_sub_checked';
		var loaded = $checkbox.parent('li').children(options.categoryFolder).hasClass(options.loadedClassName);
		var folded = $checkbox.parent('li').children(options.categoryFolder).hasClass(options.foldedClassName);
		var newState = _determineNewCheckboxState($checkbox, checked);

		_setCheckboxState($checkbox, newState);

		$checkbox.parent('li').children(options.categoryState).val(newState.substring(0, newState.lastIndexOf('_')));

		if (folded) {
			$checkbox.removeClass('pass_on_no_self_no_sub_checked');
			$checkbox.removeClass('pass_on_self_all_sub_checked');
			$checkbox.addClass('pass_on_' + checked);

			if ($checkbox.hasClass('pass_on_no_self_no_sub_checked')) {
				$checkbox.parent('li').find(options.bequeathingState + ':first').val('no_self_no_sub');
			} else {
				$checkbox.parent('li').find(options.bequeathingState + ':first').val('self_all_sub');
			}

			if (loaded) {
				_updateStateOfChildrenCheckboxes($checkbox, checked);
			}
		}

		_updateStateOfParentCheckboxes($checkbox);

		if (checked !== 'self_all_sub_checked' && $(options.selectAllCategories).is(':checked')) {
			$(options.selectAllCategories).prop('checked', false);
			$(options.selectAllCategories).parent('.single-checkbox').removeClass('checked');
		} else if ($(options.categoryFolder).size() === $(options.categoryCheckbox + '.self_all_sub_checked').size() && !$(options.selectAllCategories).is(':checked')) {
			$(options.selectAllCategories).prop('checked', true);
			$(options.selectAllCategories).parent('.single-checkbox').addClass('checked');
		}
	};

	/**
  * Handles the change event of the "select all categories" checkbox.
  * 
  * Updates all checkboxes in the categories tree (all checked / all unchecked)
  * 
  * @returns {boolean}
  * @private
  */
	function _changeSelectAllCategoriesCheckbox() {
		var checked = $(this).is(':checked') ? 'self_all_sub_checked' : 'no_self_no_sub_checked';
		var passOn = $(this).is(':checked') ? 'pass_on_self_all_sub_checked' : 'pass_on_no_self_no_sub_checked';

		$(options.categoryCheckbox).each(function () {
			_setCheckboxState($(this), checked);
			$(this).removeClass('pass_on_self_all_sub_checked');
			$(this).removeClass('pass_on_no_self_no_sub_checked');
			$(this).addClass(passOn);

			if ($(this).hasClass('pass_on_no_self_no_sub_checked')) {
				$(this).parent('li').find(options.bequeathingState + ':first').val('no_self_no_sub');
			} else {
				$(this).parent('li').find(options.bequeathingState + ':first').val('self_all_sub');
			}
		});

		return true;
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {

		$this.find(options.categoryFolder).on('click', _clickCategory);
		$this.find(options.categoryCheckbox).on('click', _clickCheckbox);
		$this.find(options.selectAllCategories).on('change', _changeSelectAllCategoriesCheckbox);

		_updateSelectAllCategoriesCheckbox();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvY29udHJvbGxlcnMvc2NoZW1lX2NhdGVnb3JpZXMuanMiXSwibmFtZXMiOlsiZ3htb2R1bGVzIiwiY29udHJvbGxlcnMiLCJtb2R1bGUiLCJkYXRhIiwiJHRoaXMiLCIkIiwiZGVmYXVsdHMiLCJiZXF1ZWF0aGluZ1N0YXRlIiwiY2F0ZWdvcnlDaGVja2JveCIsImNhdGVnb3J5Rm9sZGVyIiwiY2F0ZWdvcnlTdGF0ZSIsImNhdGVnb3J5U3VidHJlZSIsImNoZWNrYm94U3RhdGVzIiwiZGVmYXVsdENoZWNrYm94U3RhdGUiLCJmb2xkZWRDbGFzc05hbWUiLCJsb2FkZWRDbGFzc05hbWUiLCJzY2hlbWVJZCIsInNlbGVjdEFsbENhdGVnb3JpZXMiLCJzdWJjYXRlZ29yaWVzVXJsIiwic3VidHJlZUNsYXNzTmFtZSIsInVuZm9sZGVkQ2xhc3NOYW1lIiwib3B0aW9ucyIsImV4dGVuZCIsIl91cGRhdGVTZWxlY3RBbGxDYXRlZ29yaWVzQ2hlY2tib3giLCJhbGxDaGVja2VkIiwiZmluZCIsImVhY2giLCJ2YWwiLCJwcm9wIiwiX3VwZGF0ZVN0YXRlT2ZDaGlsZHJlbkNoZWNrYm94ZXMiLCIkY2hlY2tib3giLCJjaGVja1N0YXRlIiwiJGNoaWxkcmVuIiwicGFyZW50IiwiY2hpbGRyZW4iLCJzaXplIiwiX3NldENoZWNrYm94U3RhdGUiLCJfdXBkYXRlU3RhdGVPZlBhcmVudENoZWNrYm94ZXMiLCIkc3VidHJlZSIsImNsb3Nlc3QiLCJoYXNDbGFzcyIsIiRwYXJlbnRDaGVja2JveCIsIl9kZXRlcm1pbmVDaGVja2JveFN0YXRlIiwic2VsZkNoZWNrZWQiLCJfaXNDaGVja2VkIiwiJGNoaWxkQ2F0ZWdvcmllcyIsImNoaWxkQ291bnQiLCJsZW5ndGgiLCJjaGlsZENoZWNrQ291bnQiLCJzdGF0ZSIsIl9yZXNldENoZWNrYm94IiwiYWRkQ2xhc3MiLCJzdWJzdHJpbmciLCJsYXN0SW5kZXhPZiIsImkiLCJyZW1vdmVDbGFzcyIsIl9nZXRDaGVja2JveFN0YXRlIiwiX2RldGVybWluZU5ld0NoZWNrYm94U3RhdGUiLCJhY3R1YWxTdGF0ZSIsIm5ld1N0YXRlIiwiX2dldENoaWxkQ291bnQiLCJfZ2V0SW5oZXJpdGVkU3RhdGVPZkNoaWxkcmVuIiwiX3RvZ2dsZUZvbGRlciIsIiRmb2xkZXIiLCJ0b2dnbGVDbGFzcyIsIl9jbGlja0NhdGVnb3J5IiwiY2F0ZWdvcnlJZCIsInNob3ciLCJoaWRlIiwiYWpheCIsInR5cGUiLCJ1cmwiLCJzdWNjZXNzIiwicmVzcG9uc2UiLCJKU09OIiwicGFyc2UiLCJodG1sIiwib24iLCJfY2xpY2tDaGVja2JveCIsImluaGVyaXRlZFN0YXRlIiwiZXJyb3IiLCJjaGVja2VkIiwibG9hZGVkIiwiZm9sZGVkIiwiaXMiLCJfY2hhbmdlU2VsZWN0QWxsQ2F0ZWdvcmllc0NoZWNrYm94IiwicGFzc09uIiwiaW5pdCIsImRvbmUiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsVUFBVUMsV0FBVixDQUFzQkMsTUFBdEIsQ0FDQyxtQkFERCxFQUdDLEVBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7QUFDZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQzs7Ozs7QUFLQUMsU0FBUUMsRUFBRSxJQUFGLENBTlQ7OztBQVFDOzs7OztBQUtBQyxZQUFXO0FBQ1ZDLG9CQUFrQix5QkFEUjtBQUVWQyxvQkFBa0Isb0JBRlI7QUFHVkMsa0JBQWdCLGtCQUhOO0FBSVZDLGlCQUFlLHNCQUpMO0FBS1ZDLG1CQUFpQixZQUxQO0FBTVZDLGtCQUFnQixDQUNmLHNCQURlLEVBRWYsdUJBRmUsRUFHZixxQkFIZSxFQUlmLHlCQUplLEVBS2YsMEJBTGUsRUFNZix3QkFOZSxDQU5OO0FBY1ZDLHdCQUFzQix3QkFkWjtBQWVWQyxtQkFBaUIsUUFmUDtBQWdCVkMsbUJBQWlCLFFBaEJQO0FBaUJWQyxZQUFVLENBakJBO0FBa0JWQyx1QkFBcUIsd0JBbEJYO0FBbUJWQyxvQkFBa0Isa0RBbkJSO0FBb0JWQyxvQkFBa0IsU0FwQlI7QUFxQlZDLHFCQUFtQjtBQXJCVCxFQWJaOzs7QUFxQ0M7Ozs7O0FBS0FDLFdBQVVoQixFQUFFaUIsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CaEIsUUFBbkIsRUFBNkJILElBQTdCLENBMUNYOzs7QUE0Q0M7Ozs7O0FBS0FELFVBQVMsRUFqRFY7O0FBbURBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFLQSxVQUFTcUIsa0NBQVQsR0FBOEM7QUFDN0MsTUFBSUMsYUFBYSxJQUFqQjtBQUNBcEIsUUFBTXFCLElBQU4sQ0FBV0osUUFBUVgsYUFBbkIsRUFBa0NnQixJQUFsQyxDQUF1QyxZQUFXO0FBQ2pERixpQkFBY25CLEVBQUUsSUFBRixFQUFRc0IsR0FBUixPQUFrQixjQUFoQztBQUNBLEdBRkQ7O0FBSUEsTUFBSUgsVUFBSixFQUFnQjtBQUNmcEIsU0FBTXFCLElBQU4sQ0FBV0osUUFBUUosbUJBQW5CLEVBQXdDVyxJQUF4QyxDQUE2QyxTQUE3QyxFQUF3RCxJQUF4RDtBQUNBO0FBQ0Q7O0FBR0Q7Ozs7Ozs7QUFPQSxVQUFTQyxnQ0FBVCxDQUEwQ0MsU0FBMUMsRUFBcURDLFVBQXJELEVBQWlFO0FBQ2hFLE1BQU1DLFlBQVlGLFVBQVVHLE1BQVYsQ0FBaUIsSUFBakIsRUFDaEJDLFFBRGdCLENBQ1BiLFFBQVFWLGVBREQsRUFFaEJ1QixRQUZnQixDQUVQLElBRk8sRUFHaEJBLFFBSGdCLENBR1BiLFFBQVFiLGdCQUhELENBQWxCOztBQUtBLE1BQUl3QixVQUFVRyxJQUFWLEtBQW1CLENBQXZCLEVBQTBCO0FBQ3pCSCxhQUFVTixJQUFWLENBQWUsWUFBVztBQUN6QlUsc0JBQWtCL0IsRUFBRSxJQUFGLENBQWxCLEVBQTJCMEIsVUFBM0I7QUFDQUYscUNBQWlDeEIsRUFBRSxJQUFGLENBQWpDLEVBQTBDMEIsVUFBMUM7QUFDQSxJQUhEO0FBSUE7QUFDRDs7QUFHRDs7Ozs7O0FBTUEsVUFBU00sOEJBQVQsQ0FBd0NQLFNBQXhDLEVBQW1EO0FBQ2xELE1BQU1RLFdBQVdSLFVBQVVTLE9BQVYsQ0FBa0JsQixRQUFRVixlQUExQixDQUFqQjs7QUFFQSxNQUFJMkIsU0FBU0UsUUFBVCxDQUFrQm5CLFFBQVFGLGdCQUExQixDQUFKLEVBQWlEO0FBQ2hELE9BQU1zQixrQkFBa0JILFNBQVNMLE1BQVQsQ0FBZ0IsSUFBaEIsRUFBc0JDLFFBQXRCLENBQStCYixRQUFRYixnQkFBdkMsQ0FBeEI7QUFDQSxPQUFNdUIsYUFBYVcsd0JBQXdCRCxlQUF4QixDQUFuQjtBQUNBTCxxQkFBa0JLLGVBQWxCLEVBQW1DVixVQUFuQztBQUNBTSxrQ0FBK0JJLGVBQS9CO0FBQ0E7QUFDRDs7QUFHRDs7Ozs7OztBQU9BLFVBQVNDLHVCQUFULENBQWlDWixTQUFqQyxFQUE0QztBQUMzQyxNQUFNYSxjQUFjQyxXQUFXZCxTQUFYLENBQXBCO0FBQ0EsTUFBTWUsbUJBQW1CZixVQUFVRyxNQUFWLENBQWlCLElBQWpCLEVBQ3ZCQyxRQUR1QixDQUNkYixRQUFRVixlQURNLEVBRXZCdUIsUUFGdUIsQ0FFZCxJQUZjLEVBR3ZCQSxRQUh1QixDQUdkYixRQUFRYixnQkFITSxDQUF6QjtBQUlBLE1BQUlzQyxhQUFhRCxpQkFBaUJFLE1BQWxDO0FBQ0EsTUFBSUMsa0JBQWtCLENBQXRCO0FBQ0EsTUFBSWpCLGFBQWFWLFFBQVFSLG9CQUF6Qjs7QUFFQWdDLG1CQUFpQm5CLElBQWpCLENBQXNCLFlBQVc7QUFDaEMsT0FBSSxDQUFDckIsRUFBRSxJQUFGLEVBQVFtQyxRQUFSLENBQWlCLHdCQUFqQixDQUFMLEVBQWlEO0FBQ2hEUTtBQUNBO0FBQ0QsT0FBSSxDQUFDM0MsRUFBRSxJQUFGLEVBQVFtQyxRQUFSLENBQWlCLHNCQUFqQixDQUFMLEVBQStDO0FBQzlDTTtBQUNBO0FBQ0QsR0FQRDs7QUFTQSxNQUFJSCxXQUFKLEVBQWlCO0FBQ2hCLE9BQUlLLG9CQUFvQkYsVUFBeEIsRUFBb0M7QUFDbkNmLGlCQUFhLHNCQUFiO0FBQ0EsSUFGRCxNQUdLLElBQUlpQixrQkFBa0IsQ0FBdEIsRUFBeUI7QUFDN0JqQixpQkFBYSx1QkFBYjtBQUNBLElBRkksTUFHQSxJQUFJaUIsb0JBQW9CLENBQXhCLEVBQTJCO0FBQy9CakIsaUJBQWEscUJBQWI7QUFDQTtBQUNELEdBVkQsTUFXSztBQUNKLE9BQUlpQixvQkFBb0JGLFVBQXhCLEVBQW9DO0FBQ25DZixpQkFBYSx5QkFBYjtBQUNBLElBRkQsTUFHSyxJQUFJaUIsa0JBQWtCLENBQXRCLEVBQXlCO0FBQzdCakIsaUJBQWEsMEJBQWI7QUFDQSxJQUZJLE1BR0EsSUFBSWlCLG9CQUFvQixDQUF4QixFQUEyQjtBQUMvQmpCLGlCQUFhLHdCQUFiO0FBQ0E7QUFDRDs7QUFFRCxTQUFPQSxVQUFQO0FBQ0E7O0FBR0Q7Ozs7Ozs7QUFPQSxVQUFTSyxpQkFBVCxDQUEyQk4sU0FBM0IsRUFBc0NtQixLQUF0QyxFQUE2QztBQUM1Q0MsaUJBQWVwQixTQUFmO0FBQ0FBLFlBQVVxQixRQUFWLENBQW1CRixLQUFuQjtBQUNBbkIsWUFBVUcsTUFBVixDQUFpQixJQUFqQixFQUF1QkMsUUFBdkIsQ0FBZ0NiLFFBQVFYLGFBQXhDLEVBQXVEaUIsR0FBdkQsQ0FBMkRzQixNQUFNRyxTQUFOLENBQWdCLENBQWhCLEVBQW1CSCxNQUFNSSxXQUFOLENBQWtCLEdBQWxCLENBQW5CLENBQTNEO0FBQ0E7O0FBR0Q7Ozs7OztBQU1BLFVBQVNILGNBQVQsQ0FBd0JwQixTQUF4QixFQUFtQztBQUNsQyxPQUFLLElBQUl3QixJQUFJLENBQWIsRUFBZ0JBLElBQUlqQyxRQUFRVCxjQUFSLENBQXVCbUMsTUFBM0MsRUFBbURPLEdBQW5ELEVBQXdEO0FBQ3ZEeEIsYUFBVXlCLFdBQVYsQ0FBc0JsQyxRQUFRVCxjQUFSLENBQXVCMEMsQ0FBdkIsQ0FBdEI7QUFDQTtBQUNEOztBQUdEOzs7Ozs7O0FBT0EsVUFBU0UsaUJBQVQsQ0FBMkIxQixTQUEzQixFQUFzQztBQUNyQyxPQUFLLElBQUl3QixJQUFJLENBQWIsRUFBZ0JBLElBQUlqQyxRQUFRVCxjQUFSLENBQXVCbUMsTUFBM0MsRUFBbURPLEdBQW5ELEVBQXdEO0FBQ3ZELE9BQUl4QixVQUFVVSxRQUFWLENBQW1CbkIsUUFBUVQsY0FBUixDQUF1QjBDLENBQXZCLENBQW5CLENBQUosRUFBbUQ7QUFDbEQsV0FBT2pDLFFBQVFULGNBQVIsQ0FBdUIwQyxDQUF2QixDQUFQO0FBQ0E7QUFDRDtBQUNELFNBQU9qQyxRQUFRUixvQkFBZjtBQUNBOztBQUdEOzs7Ozs7O0FBT0EsVUFBUzRDLDBCQUFULENBQW9DM0IsU0FBcEMsRUFBK0M7QUFDOUMsTUFBTTRCLGNBQWNGLGtCQUFrQjFCLFNBQWxCLENBQXBCO0FBQ0EsTUFBSTZCLFdBQVd0QyxRQUFRUixvQkFBdkI7O0FBRUEsTUFBSWlCLFVBQVVHLE1BQVYsQ0FBaUIsSUFBakIsRUFBdUJDLFFBQXZCLENBQWdDYixRQUFRWixjQUF4QyxFQUF3RCtCLFFBQXhELENBQWlFbkIsUUFBUVAsZUFBekUsS0FBOEZnQixVQUMvRkcsTUFEK0YsQ0FDeEYsSUFEd0YsRUFFL0ZDLFFBRitGLENBRXRGYixRQUFRWixjQUY4RSxFQUcvRitCLFFBSCtGLENBR3RGbkIsUUFBUUQsaUJBSDhFLEtBR3hEd0MsZUFBZTlCLFNBQWYsTUFBOEIsQ0FIeEUsRUFHNEU7QUFDM0UsV0FBUTRCLFdBQVI7QUFDQyxTQUFLLHNCQUFMO0FBQ0EsU0FBSyx1QkFBTDtBQUNBLFNBQUsscUJBQUw7QUFDQ0MsZ0JBQVcsd0JBQVg7QUFDQTs7QUFFRCxTQUFLLHlCQUFMO0FBQ0EsU0FBSywwQkFBTDtBQUNBLFNBQUssd0JBQUw7QUFDQ0EsZ0JBQVcsc0JBQVg7QUFDQTs7QUFFRDtBQUNDO0FBZEY7QUFnQkEsR0FwQkQsTUFxQks7QUFDSixXQUFRRCxXQUFSO0FBQ0MsU0FBSyxzQkFBTDtBQUNDQyxnQkFBVyx5QkFBWDtBQUNBOztBQUVELFNBQUssdUJBQUw7QUFDQ0EsZ0JBQVcsMEJBQVg7QUFDQTs7QUFFRCxTQUFLLHFCQUFMO0FBQ0NBLGdCQUFXLHdCQUFYO0FBQ0E7O0FBRUQsU0FBSyx5QkFBTDtBQUNDQSxnQkFBVyxzQkFBWDtBQUNBOztBQUVELFNBQUssMEJBQUw7QUFDQ0EsZ0JBQVcsdUJBQVg7QUFDQTs7QUFFRCxTQUFLLHdCQUFMO0FBQ0NBLGdCQUFXLHFCQUFYO0FBQ0E7O0FBRUQ7QUFDQztBQTFCRjtBQTRCQTs7QUFFRCxTQUFPQSxRQUFQO0FBQ0E7O0FBR0Q7Ozs7Ozs7QUFPQSxVQUFTZixVQUFULENBQW9CZCxTQUFwQixFQUErQjtBQUM5QixTQUFPQSxVQUFVVSxRQUFWLENBQW1CLHNCQUFuQixLQUE4Q1YsVUFBVVUsUUFBVixDQUFtQix1QkFBbkIsQ0FBOUMsSUFDSFYsVUFBVVUsUUFBVixDQUFtQixxQkFBbkIsQ0FESjtBQUVBOztBQUdEOzs7Ozs7O0FBT0EsVUFBU29CLGNBQVQsQ0FBd0I5QixTQUF4QixFQUFtQztBQUNsQyxTQUFPQSxVQUFVRyxNQUFWLENBQWlCLElBQWpCLEVBQXVCQyxRQUF2QixDQUFnQ2IsUUFBUVYsZUFBeEMsRUFBeUR1QixRQUF6RCxDQUFrRSxJQUFsRSxFQUF3RUMsSUFBeEUsRUFBUDtBQUNBOztBQUdEOzs7Ozs7O0FBT0EsVUFBUzBCLDRCQUFULENBQXNDL0IsU0FBdEMsRUFBaUQ7QUFDaEQsU0FBT0EsVUFBVVUsUUFBVixDQUFtQm5CLFFBQVFGLGdCQUEzQixDQUFQLEVBQXFEO0FBQ3BELE9BQUlXLFVBQVVHLE1BQVYsQ0FBaUIsSUFBakIsRUFBdUJDLFFBQXZCLENBQWdDYixRQUFRYixnQkFBeEMsRUFBMERnQyxRQUExRCxDQUFtRSxnQ0FBbkUsQ0FBSixFQUEwRztBQUN6RyxXQUFPLHdCQUFQO0FBQ0EsSUFGRCxNQUdLLElBQUlWLFVBQVVHLE1BQVYsQ0FBaUIsSUFBakIsRUFDTkMsUUFETSxDQUNHYixRQUFRYixnQkFEWCxFQUVOZ0MsUUFGTSxDQUVHLDhCQUZILENBQUosRUFFd0M7QUFDNUMsV0FBTyxzQkFBUDtBQUNBOztBQUVEVixlQUFZQSxVQUFVRyxNQUFWLENBQWlCLElBQWpCLEVBQXVCTSxPQUF2QixDQUErQmxCLFFBQVFWLGVBQXZDLENBQVo7QUFDQTs7QUFFRCxTQUFPLEtBQVA7QUFDQTs7QUFHRDs7Ozs7O0FBTUEsVUFBU21ELGFBQVQsQ0FBdUJDLE9BQXZCLEVBQWdDO0FBQy9CQSxVQUFRQyxXQUFSLENBQW9CM0MsUUFBUUQsaUJBQTVCLEVBQStDNEMsV0FBL0MsQ0FBMkQzQyxRQUFRUCxlQUFuRTtBQUNBaUQsVUFBUXRDLElBQVIsQ0FBYSxNQUFiLEVBQXFCdUMsV0FBckIsQ0FBaUMsV0FBakMsRUFBOENBLFdBQTlDLENBQTBELGdCQUExRDtBQUNBOztBQUdEO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVNDLGNBQVQsR0FBMEI7QUFDekIsTUFBTUMsYUFBYTdELEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsYUFBYixDQUFuQjtBQUNBLE1BQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBLE1BQUlELE1BQU1vQyxRQUFOLENBQWVuQixRQUFRTixlQUF2QixLQUEyQ1gsTUFBTW9DLFFBQU4sQ0FBZW5CLFFBQVFQLGVBQXZCLENBQS9DLEVBQXdGO0FBQ3ZGVixTQUFNNkIsTUFBTixDQUFhLElBQWIsRUFBbUJSLElBQW5CLENBQXdCSixRQUFRVixlQUFoQyxFQUFpRHdELElBQWpEO0FBQ0FMLGlCQUFjMUQsS0FBZDtBQUNBLEdBSEQsTUFHTyxJQUFJQSxNQUFNb0MsUUFBTixDQUFlbkIsUUFBUU4sZUFBdkIsS0FBMkNYLE1BQU1vQyxRQUFOLENBQWVuQixRQUFRRCxpQkFBdkIsQ0FBL0MsRUFBMEY7QUFDaEdoQixTQUFNNkIsTUFBTixDQUFhLElBQWIsRUFBbUJSLElBQW5CLENBQXdCSixRQUFRVixlQUFoQyxFQUFpRHlELElBQWpEO0FBQ0FOLGlCQUFjMUQsS0FBZDtBQUNBLEdBSE0sTUFHQTtBQUNOQyxLQUFFZ0UsSUFBRixDQUFPO0FBQ05DLFVBQU0sS0FEQTtBQUVOQyxTQUFLbEQsUUFBUUgsZ0JBQVIsR0FBMkIsWUFBM0IsR0FBMENHLFFBQVFMLFFBQWxELEdBQTZELGNBQTdELEdBQThFa0QsVUFGN0U7QUFHTk0sYUFBUyxpQkFBU0MsUUFBVCxFQUFtQjtBQUMzQkEsZ0JBQVdDLEtBQUtDLEtBQUwsQ0FBV0YsUUFBWCxDQUFYOztBQUVBLFNBQUlBLFNBQVMsU0FBVCxNQUF3QixJQUE1QixFQUFrQztBQUNqQ3JFLFlBQU02QixNQUFOLENBQWEsSUFBYixFQUFtQlIsSUFBbkIsQ0FBd0JKLFFBQVFWLGVBQVIsR0FBMEIsUUFBbEQsRUFBNERpRSxJQUE1RCxDQUFpRUgsU0FBU0csSUFBMUU7QUFDQXhFLFlBQU02QixNQUFOLENBQWEsSUFBYixFQUNFUixJQURGLENBQ09KLFFBQVFWLGVBQVIsR0FBMEIsU0FBMUIsR0FBc0NVLFFBQVFaLGNBRHJELEVBRUVvRSxFQUZGLENBRUssT0FGTCxFQUVjWixjQUZkO0FBR0E3RCxZQUFNNkIsTUFBTixDQUFhLElBQWIsRUFDRVIsSUFERixDQUNPSixRQUFRVixlQUFSLEdBQTBCLFNBQTFCLEdBQXNDVSxRQUFRYixnQkFEckQsRUFFRXFFLEVBRkYsQ0FFSyxPQUZMLEVBRWNDLGNBRmQ7QUFHQTFFLFlBQU0rQyxRQUFOLENBQWU5QixRQUFRTixlQUF2Qjs7QUFFQStDLG9CQUFjMUQsS0FBZDs7QUFFQSxVQUFNMkUsaUJBQWlCbEIsNkJBQTZCekQsTUFBTTZCLE1BQU4sQ0FBYSxJQUFiLEVBQ2xEQyxRQURrRCxDQUN6Q2IsUUFBUVYsZUFEaUMsQ0FBN0IsQ0FBdkI7QUFFQSxVQUFJb0UsY0FBSixFQUFvQjtBQUNuQjNFLGFBQ0U2QixNQURGLENBQ1MsSUFEVCxFQUVFQyxRQUZGLENBRVdiLFFBQVFWLGVBRm5CLEVBR0V1QixRQUhGLENBR1csSUFIWCxFQUlFQSxRQUpGLENBSVdiLFFBQVFiLGdCQUpuQixFQUtFa0IsSUFMRixDQUtPLFlBQVc7QUFDaEJVLDBCQUFrQi9CLEVBQUUsSUFBRixDQUFsQixFQUEyQjBFLGNBQTNCO0FBQ0EsUUFQRjtBQVFBO0FBQ0Q7O0FBRUQsWUFBT04sU0FBUyxTQUFULENBQVA7QUFDQSxLQWpDSztBQWtDTk8sV0FBTyxpQkFBVztBQUNqQixZQUFPLEtBQVA7QUFDQTtBQXBDSyxJQUFQO0FBc0NBO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxVQUFTRixjQUFULEdBQTBCO0FBQ3pCLE1BQU1oRCxZQUFZekIsRUFBRSxJQUFGLENBQWxCO0FBQ0EsTUFBTTRFLFVBQVVyQyxXQUFXZCxTQUFYLElBQXdCLHdCQUF4QixHQUFtRCxzQkFBbkU7QUFDQSxNQUFNb0QsU0FBU3BELFVBQVVHLE1BQVYsQ0FBaUIsSUFBakIsRUFBdUJDLFFBQXZCLENBQWdDYixRQUFRWixjQUF4QyxFQUF3RCtCLFFBQXhELENBQWlFbkIsUUFBUU4sZUFBekUsQ0FBZjtBQUNBLE1BQU1vRSxTQUFTckQsVUFBVUcsTUFBVixDQUFpQixJQUFqQixFQUF1QkMsUUFBdkIsQ0FBZ0NiLFFBQVFaLGNBQXhDLEVBQXdEK0IsUUFBeEQsQ0FBaUVuQixRQUFRUCxlQUF6RSxDQUFmO0FBQ0EsTUFBTTZDLFdBQVdGLDJCQUEyQjNCLFNBQTNCLEVBQXNDbUQsT0FBdEMsQ0FBakI7O0FBRUE3QyxvQkFBa0JOLFNBQWxCLEVBQTZCNkIsUUFBN0I7O0FBRUE3QixZQUFVRyxNQUFWLENBQWlCLElBQWpCLEVBQ0VDLFFBREYsQ0FDV2IsUUFBUVgsYUFEbkIsRUFFRWlCLEdBRkYsQ0FFTWdDLFNBQVNQLFNBQVQsQ0FBbUIsQ0FBbkIsRUFBc0JPLFNBQVNOLFdBQVQsQ0FBcUIsR0FBckIsQ0FBdEIsQ0FGTjs7QUFJQSxNQUFJOEIsTUFBSixFQUFZO0FBQ1hyRCxhQUFVeUIsV0FBVixDQUFzQixnQ0FBdEI7QUFDQXpCLGFBQVV5QixXQUFWLENBQXNCLDhCQUF0QjtBQUNBekIsYUFBVXFCLFFBQVYsQ0FBbUIsYUFBYThCLE9BQWhDOztBQUVBLE9BQUluRCxVQUFVVSxRQUFWLENBQW1CLGdDQUFuQixDQUFKLEVBQTBEO0FBQ3pEVixjQUFVRyxNQUFWLENBQWlCLElBQWpCLEVBQXVCUixJQUF2QixDQUE0QkosUUFBUWQsZ0JBQVIsR0FBMkIsUUFBdkQsRUFBaUVvQixHQUFqRSxDQUFxRSxnQkFBckU7QUFDQSxJQUZELE1BRU87QUFDTkcsY0FBVUcsTUFBVixDQUFpQixJQUFqQixFQUF1QlIsSUFBdkIsQ0FBNEJKLFFBQVFkLGdCQUFSLEdBQTJCLFFBQXZELEVBQWlFb0IsR0FBakUsQ0FBcUUsY0FBckU7QUFDQTs7QUFFRCxPQUFJdUQsTUFBSixFQUFZO0FBQ1hyRCxxQ0FBaUNDLFNBQWpDLEVBQTRDbUQsT0FBNUM7QUFDQTtBQUNEOztBQUVENUMsaUNBQStCUCxTQUEvQjs7QUFFQSxNQUFJbUQsWUFBWSxzQkFBWixJQUFzQzVFLEVBQUVnQixRQUFRSixtQkFBVixFQUErQm1FLEVBQS9CLENBQWtDLFVBQWxDLENBQTFDLEVBQXlGO0FBQ3hGL0UsS0FBRWdCLFFBQVFKLG1CQUFWLEVBQStCVyxJQUEvQixDQUFvQyxTQUFwQyxFQUErQyxLQUEvQztBQUNBdkIsS0FBRWdCLFFBQVFKLG1CQUFWLEVBQStCZ0IsTUFBL0IsQ0FBc0Msa0JBQXRDLEVBQTBEc0IsV0FBMUQsQ0FBc0UsU0FBdEU7QUFDQSxHQUhELE1BSUssSUFBSWxELEVBQUVnQixRQUFRWixjQUFWLEVBQTBCMEIsSUFBMUIsT0FBcUM5QixFQUFFZ0IsUUFBUWIsZ0JBQVIsR0FBMkIsdUJBQTdCLEVBQXNEMkIsSUFBdEQsRUFBckMsSUFDTCxDQUFDOUIsRUFBRWdCLFFBQVFKLG1CQUFWLEVBQStCbUUsRUFBL0IsQ0FBa0MsVUFBbEMsQ0FEQSxFQUMrQztBQUNuRC9FLEtBQUVnQixRQUFRSixtQkFBVixFQUErQlcsSUFBL0IsQ0FBb0MsU0FBcEMsRUFBK0MsSUFBL0M7QUFDQXZCLEtBQUVnQixRQUFRSixtQkFBVixFQUErQmdCLE1BQS9CLENBQXNDLGtCQUF0QyxFQUEwRGtCLFFBQTFELENBQW1FLFNBQW5FO0FBQ0E7QUFDRDs7QUFHRDs7Ozs7Ozs7QUFRQSxVQUFTa0Msa0NBQVQsR0FBOEM7QUFDN0MsTUFBTUosVUFBVTVFLEVBQUUsSUFBRixFQUFRK0UsRUFBUixDQUFXLFVBQVgsSUFBeUIsc0JBQXpCLEdBQWtELHdCQUFsRTtBQUNBLE1BQU1FLFNBQVNqRixFQUFFLElBQUYsRUFBUStFLEVBQVIsQ0FBVyxVQUFYLElBQXlCLDhCQUF6QixHQUEwRCxnQ0FBekU7O0FBRUEvRSxJQUFFZ0IsUUFBUWIsZ0JBQVYsRUFBNEJrQixJQUE1QixDQUFpQyxZQUFXO0FBQzNDVSxxQkFBa0IvQixFQUFFLElBQUYsQ0FBbEIsRUFBMkI0RSxPQUEzQjtBQUNBNUUsS0FBRSxJQUFGLEVBQVFrRCxXQUFSLENBQW9CLDhCQUFwQjtBQUNBbEQsS0FBRSxJQUFGLEVBQVFrRCxXQUFSLENBQW9CLGdDQUFwQjtBQUNBbEQsS0FBRSxJQUFGLEVBQVE4QyxRQUFSLENBQWlCbUMsTUFBakI7O0FBRUEsT0FBSWpGLEVBQUUsSUFBRixFQUFRbUMsUUFBUixDQUFpQixnQ0FBakIsQ0FBSixFQUF3RDtBQUN2RG5DLE1BQUUsSUFBRixFQUFRNEIsTUFBUixDQUFlLElBQWYsRUFBcUJSLElBQXJCLENBQTBCSixRQUFRZCxnQkFBUixHQUEyQixRQUFyRCxFQUErRG9CLEdBQS9ELENBQW1FLGdCQUFuRTtBQUNBLElBRkQsTUFFTztBQUNOdEIsTUFBRSxJQUFGLEVBQVE0QixNQUFSLENBQWUsSUFBZixFQUFxQlIsSUFBckIsQ0FBMEJKLFFBQVFkLGdCQUFSLEdBQTJCLFFBQXJELEVBQStEb0IsR0FBL0QsQ0FBbUUsY0FBbkU7QUFDQTtBQUNELEdBWEQ7O0FBYUEsU0FBTyxJQUFQO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBekIsUUFBT3FGLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7O0FBRTVCcEYsUUFBTXFCLElBQU4sQ0FBV0osUUFBUVosY0FBbkIsRUFBbUNvRSxFQUFuQyxDQUFzQyxPQUF0QyxFQUErQ1osY0FBL0M7QUFDQTdELFFBQU1xQixJQUFOLENBQVdKLFFBQVFiLGdCQUFuQixFQUFxQ3FFLEVBQXJDLENBQXdDLE9BQXhDLEVBQWlEQyxjQUFqRDtBQUNBMUUsUUFBTXFCLElBQU4sQ0FBV0osUUFBUUosbUJBQW5CLEVBQXdDNEQsRUFBeEMsQ0FBMkMsUUFBM0MsRUFBcURRLGtDQUFyRDs7QUFFQTlEOztBQUVBaUU7QUFDQSxFQVREOztBQVdBLFFBQU90RixNQUFQO0FBQ0EsQ0EzZkYiLCJmaWxlIjoiQWRtaW4vSmF2YXNjcmlwdC9jb250cm9sbGVycy9zY2hlbWVfY2F0ZWdvcmllcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gc2NoZW1lX2NhdGVnb3JpZXMuanMgMjAxNy0xMS0yMFxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmd4bW9kdWxlcy5jb250cm9sbGVycy5tb2R1bGUoXG5cdCdzY2hlbWVfY2F0ZWdvcmllcycsXG5cdFxuXHRbXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTIERFRklOSVRJT05cblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHR2YXJcblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0XHQgKlxuXHRcdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHRcdCAqL1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdGRlZmF1bHRzID0ge1xuXHRcdFx0XHRiZXF1ZWF0aGluZ1N0YXRlOiAnaW5wdXQuYmVxdWVhdGhpbmctc3RhdGUnLFxuXHRcdFx0XHRjYXRlZ29yeUNoZWNrYm94OiAnLmNhdGVnb3J5LWNoZWNrYm94Jyxcblx0XHRcdFx0Y2F0ZWdvcnlGb2xkZXI6ICcuY2F0ZWdvcnktZm9sZGVyJyxcblx0XHRcdFx0Y2F0ZWdvcnlTdGF0ZTogJ2lucHV0LmNhdGVnb3J5LXN0YXRlJyxcblx0XHRcdFx0Y2F0ZWdvcnlTdWJ0cmVlOiAndWwuc3VidHJlZScsXG5cdFx0XHRcdGNoZWNrYm94U3RhdGVzOiBbXG5cdFx0XHRcdFx0J3NlbGZfYWxsX3N1Yl9jaGVja2VkJyxcblx0XHRcdFx0XHQnc2VsZl9zb21lX3N1Yl9jaGVja2VkJyxcblx0XHRcdFx0XHQnc2VsZl9ub19zdWJfY2hlY2tlZCcsXG5cdFx0XHRcdFx0J25vX3NlbGZfYWxsX3N1Yl9jaGVja2VkJyxcblx0XHRcdFx0XHQnbm9fc2VsZl9zb21lX3N1Yl9jaGVja2VkJyxcblx0XHRcdFx0XHQnbm9fc2VsZl9ub19zdWJfY2hlY2tlZCdcblx0XHRcdFx0XSxcblx0XHRcdFx0ZGVmYXVsdENoZWNrYm94U3RhdGU6ICdub19zZWxmX25vX3N1Yl9jaGVja2VkJyxcblx0XHRcdFx0Zm9sZGVkQ2xhc3NOYW1lOiAnZm9sZGVkJyxcblx0XHRcdFx0bG9hZGVkQ2xhc3NOYW1lOiAnbG9hZGVkJyxcblx0XHRcdFx0c2NoZW1lSWQ6IDAsXG5cdFx0XHRcdHNlbGVjdEFsbENhdGVnb3JpZXM6ICcuc2VsZWN0LWFsbC1jYXRlZ29yaWVzJyxcblx0XHRcdFx0c3ViY2F0ZWdvcmllc1VybDogJ2FkbWluLnBocD9kbz1Hb29nbGVTaG9wcGluZ0FqYXgvZ2V0U3ViY2F0ZWdvcmllcycsXG5cdFx0XHRcdHN1YnRyZWVDbGFzc05hbWU6ICdzdWJ0cmVlJyxcblx0XHRcdFx0dW5mb2xkZWRDbGFzc05hbWU6ICd1bmZvbGRlZCdcblx0XHRcdH0sXG5cdFx0XHRcblx0XHRcdC8qKlxuXHRcdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdFx0ICpcblx0XHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0XHQgKi9cblx0XHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpLFxuXHRcdFx0XG5cdFx0XHQvKipcblx0XHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHRcdCAqXG5cdFx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdFx0ICovXG5cdFx0XHRtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBIRUxQRVIgRlVOQ1RJT05TXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2V0cyB0aGUgXCJzZWxlY3QgYWxsIGNhdGVnb3JpZXNcIiBjaGVja2JveCB0byBjaGVja2VkLCBpZiBhbGwgY2F0ZWdvcmllcyBhcmUgc2VsZWN0ZWQuXG5cdFx0ICpcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF91cGRhdGVTZWxlY3RBbGxDYXRlZ29yaWVzQ2hlY2tib3goKSB7XG5cdFx0XHR2YXIgYWxsQ2hlY2tlZCA9IHRydWU7XG5cdFx0XHQkdGhpcy5maW5kKG9wdGlvbnMuY2F0ZWdvcnlTdGF0ZSkuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0YWxsQ2hlY2tlZCAmPSAkKHRoaXMpLnZhbCgpID09PSAnc2VsZl9hbGxfc3ViJztcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRpZiAoYWxsQ2hlY2tlZCkge1xuXHRcdFx0XHQkdGhpcy5maW5kKG9wdGlvbnMuc2VsZWN0QWxsQ2F0ZWdvcmllcykucHJvcCgnY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU2V0cyB0aGUgc3RhdGUgb2YgYWxsIGNoaWxkcmVuIGNoZWNrYm94ZXMgdG8gZ2l2ZW4gY2hlY2tTdGF0ZSB2YWx1ZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSAkY2hlY2tib3hcblx0XHQgKiBAcGFyYW0gY2hlY2tTdGF0ZVxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3VwZGF0ZVN0YXRlT2ZDaGlsZHJlbkNoZWNrYm94ZXMoJGNoZWNrYm94LCBjaGVja1N0YXRlKSB7XG5cdFx0XHRjb25zdCAkY2hpbGRyZW4gPSAkY2hlY2tib3gucGFyZW50KCdsaScpXG5cdFx0XHRcdC5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5U3VidHJlZSlcblx0XHRcdFx0LmNoaWxkcmVuKCdsaScpXG5cdFx0XHRcdC5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5Q2hlY2tib3gpO1xuXHRcdFx0XG5cdFx0XHRpZiAoJGNoaWxkcmVuLnNpemUoKSA+IDApIHtcblx0XHRcdFx0JGNoaWxkcmVuLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0X3NldENoZWNrYm94U3RhdGUoJCh0aGlzKSwgY2hlY2tTdGF0ZSk7XG5cdFx0XHRcdFx0X3VwZGF0ZVN0YXRlT2ZDaGlsZHJlbkNoZWNrYm94ZXMoJCh0aGlzKSwgY2hlY2tTdGF0ZSk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogVXBkYXRlcyB0aGUgc3RhdGUgb2YgYWxsIHBhcmVudCBjaGVja2JveGVzLlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtICRjaGVja2JveFxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3VwZGF0ZVN0YXRlT2ZQYXJlbnRDaGVja2JveGVzKCRjaGVja2JveCkge1xuXHRcdFx0Y29uc3QgJHN1YnRyZWUgPSAkY2hlY2tib3guY2xvc2VzdChvcHRpb25zLmNhdGVnb3J5U3VidHJlZSk7XG5cdFx0XHRcblx0XHRcdGlmICgkc3VidHJlZS5oYXNDbGFzcyhvcHRpb25zLnN1YnRyZWVDbGFzc05hbWUpKSB7XG5cdFx0XHRcdGNvbnN0ICRwYXJlbnRDaGVja2JveCA9ICRzdWJ0cmVlLnBhcmVudCgnbGknKS5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5Q2hlY2tib3gpO1xuXHRcdFx0XHRjb25zdCBjaGVja1N0YXRlID0gX2RldGVybWluZUNoZWNrYm94U3RhdGUoJHBhcmVudENoZWNrYm94KTtcblx0XHRcdFx0X3NldENoZWNrYm94U3RhdGUoJHBhcmVudENoZWNrYm94LCBjaGVja1N0YXRlKTtcblx0XHRcdFx0X3VwZGF0ZVN0YXRlT2ZQYXJlbnRDaGVja2JveGVzKCRwYXJlbnRDaGVja2JveCk7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBEZXRlcm1pbmVzIHRoZSBzdGF0ZSBmb3IgdGhlIGdpdmVuIGNoZWNrYm94LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtICRjaGVja2JveFxuXHRcdCAqIEByZXR1cm5zIHtzdHJpbmd9XG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZGV0ZXJtaW5lQ2hlY2tib3hTdGF0ZSgkY2hlY2tib3gpIHtcblx0XHRcdGNvbnN0IHNlbGZDaGVja2VkID0gX2lzQ2hlY2tlZCgkY2hlY2tib3gpO1xuXHRcdFx0Y29uc3QgJGNoaWxkQ2F0ZWdvcmllcyA9ICRjaGVja2JveC5wYXJlbnQoJ2xpJylcblx0XHRcdFx0LmNoaWxkcmVuKG9wdGlvbnMuY2F0ZWdvcnlTdWJ0cmVlKVxuXHRcdFx0XHQuY2hpbGRyZW4oJ2xpJylcblx0XHRcdFx0LmNoaWxkcmVuKG9wdGlvbnMuY2F0ZWdvcnlDaGVja2JveCk7XG5cdFx0XHRsZXQgY2hpbGRDb3VudCA9ICRjaGlsZENhdGVnb3JpZXMubGVuZ3RoO1xuXHRcdFx0bGV0IGNoaWxkQ2hlY2tDb3VudCA9IDA7XG5cdFx0XHRsZXQgY2hlY2tTdGF0ZSA9IG9wdGlvbnMuZGVmYXVsdENoZWNrYm94U3RhdGU7XG5cdFx0XHRcblx0XHRcdCRjaGlsZENhdGVnb3JpZXMuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKCEkKHRoaXMpLmhhc0NsYXNzKCdub19zZWxmX25vX3N1Yl9jaGVja2VkJykpIHtcblx0XHRcdFx0XHRjaGlsZENoZWNrQ291bnQrKztcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoISQodGhpcykuaGFzQ2xhc3MoJ3NlbGZfYWxsX3N1Yl9jaGVja2VkJykpIHtcblx0XHRcdFx0XHRjaGlsZENvdW50Kys7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRpZiAoc2VsZkNoZWNrZWQpIHtcblx0XHRcdFx0aWYgKGNoaWxkQ2hlY2tDb3VudCA9PT0gY2hpbGRDb3VudCkge1xuXHRcdFx0XHRcdGNoZWNrU3RhdGUgPSAnc2VsZl9hbGxfc3ViX2NoZWNrZWQnO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2UgaWYgKGNoaWxkQ2hlY2tDb3VudCA+IDApIHtcblx0XHRcdFx0XHRjaGVja1N0YXRlID0gJ3NlbGZfc29tZV9zdWJfY2hlY2tlZCc7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSBpZiAoY2hpbGRDaGVja0NvdW50ID09PSAwKSB7XG5cdFx0XHRcdFx0Y2hlY2tTdGF0ZSA9ICdzZWxmX25vX3N1Yl9jaGVja2VkJztcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0ZWxzZSB7XG5cdFx0XHRcdGlmIChjaGlsZENoZWNrQ291bnQgPT09IGNoaWxkQ291bnQpIHtcblx0XHRcdFx0XHRjaGVja1N0YXRlID0gJ25vX3NlbGZfYWxsX3N1Yl9jaGVja2VkJztcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIGlmIChjaGlsZENoZWNrQ291bnQgPiAwKSB7XG5cdFx0XHRcdFx0Y2hlY2tTdGF0ZSA9ICdub19zZWxmX3NvbWVfc3ViX2NoZWNrZWQnO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2UgaWYgKGNoaWxkQ2hlY2tDb3VudCA9PT0gMCkge1xuXHRcdFx0XHRcdGNoZWNrU3RhdGUgPSAnbm9fc2VsZl9ub19zdWJfY2hlY2tlZCc7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdFxuXHRcdFx0cmV0dXJuIGNoZWNrU3RhdGU7XG5cdFx0fTtcblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBTZXRzIHRoZSBzdGF0ZSBvZiB0aGUgZ2l2ZW4gY2hlY2tib3guXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0gJGNoZWNrYm94XG5cdFx0ICogQHBhcmFtIHN0YXRlXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2V0Q2hlY2tib3hTdGF0ZSgkY2hlY2tib3gsIHN0YXRlKSB7XG5cdFx0XHRfcmVzZXRDaGVja2JveCgkY2hlY2tib3gpO1xuXHRcdFx0JGNoZWNrYm94LmFkZENsYXNzKHN0YXRlKTtcblx0XHRcdCRjaGVja2JveC5wYXJlbnQoJ2xpJykuY2hpbGRyZW4ob3B0aW9ucy5jYXRlZ29yeVN0YXRlKS52YWwoc3RhdGUuc3Vic3RyaW5nKDAsIHN0YXRlLmxhc3RJbmRleE9mKCdfJykpKTtcblx0XHR9O1xuXHRcdFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFJlc2V0cyB0aGUgc3RhdGUgb2YgdGhlIGdpdmVuIGNoZWNrYm94LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtICRjaGVja2JveFxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3Jlc2V0Q2hlY2tib3goJGNoZWNrYm94KSB7XG5cdFx0XHRmb3IgKGxldCBpID0gMDsgaSA8IG9wdGlvbnMuY2hlY2tib3hTdGF0ZXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0JGNoZWNrYm94LnJlbW92ZUNsYXNzKG9wdGlvbnMuY2hlY2tib3hTdGF0ZXNbaV0pO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgc3RhdGUgb2YgdGhlIGdpdmVuIGNoZWNrYm94LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtICRjaGVja2JveFxuXHRcdCAqIEByZXR1cm5zIHtzdHJpbmd9XG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0Q2hlY2tib3hTdGF0ZSgkY2hlY2tib3gpIHtcblx0XHRcdGZvciAobGV0IGkgPSAwOyBpIDwgb3B0aW9ucy5jaGVja2JveFN0YXRlcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0XHRpZiAoJGNoZWNrYm94Lmhhc0NsYXNzKG9wdGlvbnMuY2hlY2tib3hTdGF0ZXNbaV0pKSB7XG5cdFx0XHRcdFx0cmV0dXJuIG9wdGlvbnMuY2hlY2tib3hTdGF0ZXNbaV07XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdHJldHVybiBvcHRpb25zLmRlZmF1bHRDaGVja2JveFN0YXRlO1xuXHRcdH07XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGV0ZXJtaW5lcyBhbmQgc2V0cyBuZXcgc3RhdHVzIG9mIGdpdmVuIGNoZWNrYm94LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtICRjaGVja2JveFxuXHRcdCAqIEByZXR1cm5zIHtzdHJpbmd9XG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZGV0ZXJtaW5lTmV3Q2hlY2tib3hTdGF0ZSgkY2hlY2tib3gpIHtcblx0XHRcdGNvbnN0IGFjdHVhbFN0YXRlID0gX2dldENoZWNrYm94U3RhdGUoJGNoZWNrYm94KTtcblx0XHRcdGxldCBuZXdTdGF0ZSA9IG9wdGlvbnMuZGVmYXVsdENoZWNrYm94U3RhdGU7XG5cdFx0XHRcblx0XHRcdGlmICgkY2hlY2tib3gucGFyZW50KCdsaScpLmNoaWxkcmVuKG9wdGlvbnMuY2F0ZWdvcnlGb2xkZXIpLmhhc0NsYXNzKG9wdGlvbnMuZm9sZGVkQ2xhc3NOYW1lKSB8fCAoJGNoZWNrYm94XG5cdFx0XHRcdFx0LnBhcmVudCgnbGknKVxuXHRcdFx0XHRcdC5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5Rm9sZGVyKVxuXHRcdFx0XHRcdC5oYXNDbGFzcyhvcHRpb25zLnVuZm9sZGVkQ2xhc3NOYW1lKSAmJiBfZ2V0Q2hpbGRDb3VudCgkY2hlY2tib3gpID09PSAwKSkge1xuXHRcdFx0XHRzd2l0Y2ggKGFjdHVhbFN0YXRlKSB7XG5cdFx0XHRcdFx0Y2FzZSAnc2VsZl9hbGxfc3ViX2NoZWNrZWQnOlxuXHRcdFx0XHRcdGNhc2UgJ3NlbGZfc29tZV9zdWJfY2hlY2tlZCc6XG5cdFx0XHRcdFx0Y2FzZSAnc2VsZl9ub19zdWJfY2hlY2tlZCc6XG5cdFx0XHRcdFx0XHRuZXdTdGF0ZSA9ICdub19zZWxmX25vX3N1Yl9jaGVja2VkJztcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGNhc2UgJ25vX3NlbGZfYWxsX3N1Yl9jaGVja2VkJzpcblx0XHRcdFx0XHRjYXNlICdub19zZWxmX3NvbWVfc3ViX2NoZWNrZWQnOlxuXHRcdFx0XHRcdGNhc2UgJ25vX3NlbGZfbm9fc3ViX2NoZWNrZWQnOlxuXHRcdFx0XHRcdFx0bmV3U3RhdGUgPSAnc2VsZl9hbGxfc3ViX2NoZWNrZWQnO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0c3dpdGNoIChhY3R1YWxTdGF0ZSkge1xuXHRcdFx0XHRcdGNhc2UgJ3NlbGZfYWxsX3N1Yl9jaGVja2VkJzpcblx0XHRcdFx0XHRcdG5ld1N0YXRlID0gJ25vX3NlbGZfYWxsX3N1Yl9jaGVja2VkJztcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGNhc2UgJ3NlbGZfc29tZV9zdWJfY2hlY2tlZCc6XG5cdFx0XHRcdFx0XHRuZXdTdGF0ZSA9ICdub19zZWxmX3NvbWVfc3ViX2NoZWNrZWQnO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Y2FzZSAnc2VsZl9ub19zdWJfY2hlY2tlZCc6XG5cdFx0XHRcdFx0XHRuZXdTdGF0ZSA9ICdub19zZWxmX25vX3N1Yl9jaGVja2VkJztcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGNhc2UgJ25vX3NlbGZfYWxsX3N1Yl9jaGVja2VkJzpcblx0XHRcdFx0XHRcdG5ld1N0YXRlID0gJ3NlbGZfYWxsX3N1Yl9jaGVja2VkJztcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGNhc2UgJ25vX3NlbGZfc29tZV9zdWJfY2hlY2tlZCc6XG5cdFx0XHRcdFx0XHRuZXdTdGF0ZSA9ICdzZWxmX3NvbWVfc3ViX2NoZWNrZWQnO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Y2FzZSAnbm9fc2VsZl9ub19zdWJfY2hlY2tlZCc6XG5cdFx0XHRcdFx0XHRuZXdTdGF0ZSA9ICdzZWxmX25vX3N1Yl9jaGVja2VkJztcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRyZXR1cm4gbmV3U3RhdGU7XG5cdFx0fTtcblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBDaGVja3MgaWYgZ2l2ZW4gY2hlY2tib3ggaXMgY2hlY2tlZC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSAkY2hlY2tib3hcblx0XHQgKiBAcmV0dXJucyB7Ym9vbGVhbn1cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9pc0NoZWNrZWQoJGNoZWNrYm94KSB7XG5cdFx0XHRyZXR1cm4gJGNoZWNrYm94Lmhhc0NsYXNzKCdzZWxmX2FsbF9zdWJfY2hlY2tlZCcpIHx8ICRjaGVja2JveC5oYXNDbGFzcygnc2VsZl9zb21lX3N1Yl9jaGVja2VkJylcblx0XHRcdFx0fHwgJGNoZWNrYm94Lmhhc0NsYXNzKCdzZWxmX25vX3N1Yl9jaGVja2VkJyk7XG5cdFx0fTtcblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXR1cm5zIHRoZSBjb3VudCBvZiBjaGlsZHJlbiBvZiB0aGUgZ2l2ZW4gY2hlY2tib3guIFxuXHRcdCAqIFxuXHRcdCAqIEBwYXJhbSAkY2hlY2tib3hcblx0XHQgKiBAcmV0dXJucyB7aW50fVxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldENoaWxkQ291bnQoJGNoZWNrYm94KSB7XG5cdFx0XHRyZXR1cm4gJGNoZWNrYm94LnBhcmVudCgnbGknKS5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5U3VidHJlZSkuY2hpbGRyZW4oJ2xpJykuc2l6ZSgpO1xuXHRcdH07XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmV0dXJucyB0aGUgaW5oZXJpdGVkIHN0YXRlIG9mIGNoaWxkcmVuIGNoZWNrYm94ZXMgaWYgZXhpc3QsIG90aGVyd2lzZSBmYWxzZS5cblx0XHQgKiBcblx0XHQgKiBAcGFyYW0gJGNoZWNrYm94XG5cdFx0ICogQHJldHVybnMge3N0cmluZ3xib29sZWFufVxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2dldEluaGVyaXRlZFN0YXRlT2ZDaGlsZHJlbigkY2hlY2tib3gpIHtcblx0XHRcdHdoaWxlICgkY2hlY2tib3guaGFzQ2xhc3Mob3B0aW9ucy5zdWJ0cmVlQ2xhc3NOYW1lKSkge1xuXHRcdFx0XHRpZiAoJGNoZWNrYm94LnBhcmVudCgnbGknKS5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5Q2hlY2tib3gpLmhhc0NsYXNzKCdwYXNzX29uX25vX3NlbGZfbm9fc3ViX2NoZWNrZWQnKSkge1xuXHRcdFx0XHRcdHJldHVybiAnbm9fc2VsZl9ub19zdWJfY2hlY2tlZCc7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSBpZiAoJGNoZWNrYm94LnBhcmVudCgnbGknKVxuXHRcdFx0XHRcdFx0LmNoaWxkcmVuKG9wdGlvbnMuY2F0ZWdvcnlDaGVja2JveClcblx0XHRcdFx0XHRcdC5oYXNDbGFzcygncGFzc19vbl9zZWxmX2FsbF9zdWJfY2hlY2tlZCcpKSB7XG5cdFx0XHRcdFx0cmV0dXJuICdzZWxmX2FsbF9zdWJfY2hlY2tlZCc7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdCRjaGVja2JveCA9ICRjaGVja2JveC5wYXJlbnQoJ2xpJykuY2xvc2VzdChvcHRpb25zLmNhdGVnb3J5U3VidHJlZSk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9O1xuXHRcdFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZXMgdGhlIGZvbGRlciBpY29uIGFuZCBpdHMgY2xhc3MgKGZvbGRlZCAvIHVuZm9sZGVkKS5cblx0XHQgKiBcblx0XHQgKiBAcGFyYW0gJGZvbGRlclxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3RvZ2dsZUZvbGRlcigkZm9sZGVyKSB7XG5cdFx0XHQkZm9sZGVyLnRvZ2dsZUNsYXNzKG9wdGlvbnMudW5mb2xkZWRDbGFzc05hbWUpLnRvZ2dsZUNsYXNzKG9wdGlvbnMuZm9sZGVkQ2xhc3NOYW1lKTtcblx0XHRcdCRmb2xkZXIuZmluZCgnaS5mYScpLnRvZ2dsZUNsYXNzKCdmYS1mb2xkZXInKS50b2dnbGVDbGFzcygnZmEtZm9sZGVyLW9wZW4nKTtcblx0XHR9XG5cdFx0XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBjbGljayBldmVudCBvZiBhIGNhdGVnb3J5LlxuXHRcdCAqIFxuXHRcdCAqIExvYWRzIHN1YmNhdGVnb3JpZXMgb2YgY2xpY2tlZCBjYXRlZ29yeS5cblx0XHQgKiBcblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9jbGlja0NhdGVnb3J5KCkge1xuXHRcdFx0Y29uc3QgY2F0ZWdvcnlJZCA9ICQodGhpcykuZGF0YSgnY2F0ZWdvcnktaWQnKTtcblx0XHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcdFxuXHRcdFx0aWYgKCR0aGlzLmhhc0NsYXNzKG9wdGlvbnMubG9hZGVkQ2xhc3NOYW1lKSAmJiAkdGhpcy5oYXNDbGFzcyhvcHRpb25zLmZvbGRlZENsYXNzTmFtZSkpIHtcblx0XHRcdFx0JHRoaXMucGFyZW50KCdsaScpLmZpbmQob3B0aW9ucy5jYXRlZ29yeVN1YnRyZWUpLnNob3coKTtcblx0XHRcdFx0X3RvZ2dsZUZvbGRlcigkdGhpcyk7XG5cdFx0XHR9IGVsc2UgaWYgKCR0aGlzLmhhc0NsYXNzKG9wdGlvbnMubG9hZGVkQ2xhc3NOYW1lKSAmJiAkdGhpcy5oYXNDbGFzcyhvcHRpb25zLnVuZm9sZGVkQ2xhc3NOYW1lKSkge1xuXHRcdFx0XHQkdGhpcy5wYXJlbnQoJ2xpJykuZmluZChvcHRpb25zLmNhdGVnb3J5U3VidHJlZSkuaGlkZSgpO1xuXHRcdFx0XHRfdG9nZ2xlRm9sZGVyKCR0aGlzKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdFx0dHlwZTogXCJHRVRcIixcblx0XHRcdFx0XHR1cmw6IG9wdGlvbnMuc3ViY2F0ZWdvcmllc1VybCArICcmc2NoZW1lSWQ9JyArIG9wdGlvbnMuc2NoZW1lSWQgKyAnJmNhdGVnb3J5SWQ9JyArIGNhdGVnb3J5SWQsXG5cdFx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRcdHJlc3BvbnNlID0gSlNPTi5wYXJzZShyZXNwb25zZSk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGlmIChyZXNwb25zZVsnc3VjY2VzcyddID09PSB0cnVlKSB7XG5cdFx0XHRcdFx0XHRcdCR0aGlzLnBhcmVudCgnbGknKS5maW5kKG9wdGlvbnMuY2F0ZWdvcnlTdWJ0cmVlICsgJzpmaXJzdCcpLmh0bWwocmVzcG9uc2UuaHRtbCk7XG5cdFx0XHRcdFx0XHRcdCR0aGlzLnBhcmVudCgnbGknKVxuXHRcdFx0XHRcdFx0XHRcdC5maW5kKG9wdGlvbnMuY2F0ZWdvcnlTdWJ0cmVlICsgJzpmaXJzdCAnICsgb3B0aW9ucy5jYXRlZ29yeUZvbGRlcilcblx0XHRcdFx0XHRcdFx0XHQub24oJ2NsaWNrJywgX2NsaWNrQ2F0ZWdvcnkpO1xuXHRcdFx0XHRcdFx0XHQkdGhpcy5wYXJlbnQoJ2xpJylcblx0XHRcdFx0XHRcdFx0XHQuZmluZChvcHRpb25zLmNhdGVnb3J5U3VidHJlZSArICc6Zmlyc3QgJyArIG9wdGlvbnMuY2F0ZWdvcnlDaGVja2JveClcblx0XHRcdFx0XHRcdFx0XHQub24oJ2NsaWNrJywgX2NsaWNrQ2hlY2tib3gpO1xuXHRcdFx0XHRcdFx0XHQkdGhpcy5hZGRDbGFzcyhvcHRpb25zLmxvYWRlZENsYXNzTmFtZSk7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRfdG9nZ2xlRm9sZGVyKCR0aGlzKTtcblx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdGNvbnN0IGluaGVyaXRlZFN0YXRlID0gX2dldEluaGVyaXRlZFN0YXRlT2ZDaGlsZHJlbigkdGhpcy5wYXJlbnQoJ2xpJylcblx0XHRcdFx0XHRcdFx0XHQuY2hpbGRyZW4ob3B0aW9ucy5jYXRlZ29yeVN1YnRyZWUpKTtcblx0XHRcdFx0XHRcdFx0aWYgKGluaGVyaXRlZFN0YXRlKSB7XG5cdFx0XHRcdFx0XHRcdFx0JHRoaXNcblx0XHRcdFx0XHRcdFx0XHRcdC5wYXJlbnQoJ2xpJylcblx0XHRcdFx0XHRcdFx0XHRcdC5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5U3VidHJlZSlcblx0XHRcdFx0XHRcdFx0XHRcdC5jaGlsZHJlbignbGknKVxuXHRcdFx0XHRcdFx0XHRcdFx0LmNoaWxkcmVuKG9wdGlvbnMuY2F0ZWdvcnlDaGVja2JveClcblx0XHRcdFx0XHRcdFx0XHRcdC5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRfc2V0Q2hlY2tib3hTdGF0ZSgkKHRoaXMpLCBpbmhlcml0ZWRTdGF0ZSk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRyZXR1cm4gcmVzcG9uc2VbJ3N1Y2Nlc3MnXTtcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdGVycm9yOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBjbGljayBldmVudCBvZiBjaGVja2JveGVzLlxuXHRcdCAqIFxuXHRcdCAqIFVwZGF0ZXMgc3RhdGVzIG9mIGNoZWNrYm94ZXMuXG5cdFx0ICogXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfY2xpY2tDaGVja2JveCgpIHtcblx0XHRcdGNvbnN0ICRjaGVja2JveCA9ICQodGhpcyk7XG5cdFx0XHRjb25zdCBjaGVja2VkID0gX2lzQ2hlY2tlZCgkY2hlY2tib3gpID8gJ25vX3NlbGZfbm9fc3ViX2NoZWNrZWQnIDogJ3NlbGZfYWxsX3N1Yl9jaGVja2VkJztcblx0XHRcdGNvbnN0IGxvYWRlZCA9ICRjaGVja2JveC5wYXJlbnQoJ2xpJykuY2hpbGRyZW4ob3B0aW9ucy5jYXRlZ29yeUZvbGRlcikuaGFzQ2xhc3Mob3B0aW9ucy5sb2FkZWRDbGFzc05hbWUpO1xuXHRcdFx0Y29uc3QgZm9sZGVkID0gJGNoZWNrYm94LnBhcmVudCgnbGknKS5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5Rm9sZGVyKS5oYXNDbGFzcyhvcHRpb25zLmZvbGRlZENsYXNzTmFtZSk7XG5cdFx0XHRjb25zdCBuZXdTdGF0ZSA9IF9kZXRlcm1pbmVOZXdDaGVja2JveFN0YXRlKCRjaGVja2JveCwgY2hlY2tlZCk7XG5cdFx0XHRcblx0XHRcdF9zZXRDaGVja2JveFN0YXRlKCRjaGVja2JveCwgbmV3U3RhdGUpO1xuXHRcdFx0XG5cdFx0XHQkY2hlY2tib3gucGFyZW50KCdsaScpXG5cdFx0XHRcdC5jaGlsZHJlbihvcHRpb25zLmNhdGVnb3J5U3RhdGUpXG5cdFx0XHRcdC52YWwobmV3U3RhdGUuc3Vic3RyaW5nKDAsIG5ld1N0YXRlLmxhc3RJbmRleE9mKCdfJykpKTtcblx0XHRcdFxuXHRcdFx0aWYgKGZvbGRlZCkge1xuXHRcdFx0XHQkY2hlY2tib3gucmVtb3ZlQ2xhc3MoJ3Bhc3Nfb25fbm9fc2VsZl9ub19zdWJfY2hlY2tlZCcpO1xuXHRcdFx0XHQkY2hlY2tib3gucmVtb3ZlQ2xhc3MoJ3Bhc3Nfb25fc2VsZl9hbGxfc3ViX2NoZWNrZWQnKTtcblx0XHRcdFx0JGNoZWNrYm94LmFkZENsYXNzKCdwYXNzX29uXycgKyBjaGVja2VkKTtcblx0XHRcdFx0XG5cdFx0XHRcdGlmICgkY2hlY2tib3guaGFzQ2xhc3MoJ3Bhc3Nfb25fbm9fc2VsZl9ub19zdWJfY2hlY2tlZCcpKSB7XG5cdFx0XHRcdFx0JGNoZWNrYm94LnBhcmVudCgnbGknKS5maW5kKG9wdGlvbnMuYmVxdWVhdGhpbmdTdGF0ZSArICc6Zmlyc3QnKS52YWwoJ25vX3NlbGZfbm9fc3ViJyk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JGNoZWNrYm94LnBhcmVudCgnbGknKS5maW5kKG9wdGlvbnMuYmVxdWVhdGhpbmdTdGF0ZSArICc6Zmlyc3QnKS52YWwoJ3NlbGZfYWxsX3N1YicpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAobG9hZGVkKSB7XG5cdFx0XHRcdFx0X3VwZGF0ZVN0YXRlT2ZDaGlsZHJlbkNoZWNrYm94ZXMoJGNoZWNrYm94LCBjaGVja2VkKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRfdXBkYXRlU3RhdGVPZlBhcmVudENoZWNrYm94ZXMoJGNoZWNrYm94KTtcblx0XHRcdFxuXHRcdFx0aWYgKGNoZWNrZWQgIT09ICdzZWxmX2FsbF9zdWJfY2hlY2tlZCcgJiYgJChvcHRpb25zLnNlbGVjdEFsbENhdGVnb3JpZXMpLmlzKCc6Y2hlY2tlZCcpKSB7XG5cdFx0XHRcdCQob3B0aW9ucy5zZWxlY3RBbGxDYXRlZ29yaWVzKS5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xuXHRcdFx0XHQkKG9wdGlvbnMuc2VsZWN0QWxsQ2F0ZWdvcmllcykucGFyZW50KCcuc2luZ2xlLWNoZWNrYm94JykucmVtb3ZlQ2xhc3MoJ2NoZWNrZWQnKTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYgKCQob3B0aW9ucy5jYXRlZ29yeUZvbGRlcikuc2l6ZSgpID09PSAkKG9wdGlvbnMuY2F0ZWdvcnlDaGVja2JveCArICcuc2VsZl9hbGxfc3ViX2NoZWNrZWQnKS5zaXplKClcblx0XHRcdFx0JiYgISQob3B0aW9ucy5zZWxlY3RBbGxDYXRlZ29yaWVzKS5pcygnOmNoZWNrZWQnKSkge1xuXHRcdFx0XHQkKG9wdGlvbnMuc2VsZWN0QWxsQ2F0ZWdvcmllcykucHJvcCgnY2hlY2tlZCcsIHRydWUpO1xuXHRcdFx0XHQkKG9wdGlvbnMuc2VsZWN0QWxsQ2F0ZWdvcmllcykucGFyZW50KCcuc2luZ2xlLWNoZWNrYm94JykuYWRkQ2xhc3MoJ2NoZWNrZWQnKTtcblx0XHRcdH1cblx0XHR9O1xuXHRcdFxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEhhbmRsZXMgdGhlIGNoYW5nZSBldmVudCBvZiB0aGUgXCJzZWxlY3QgYWxsIGNhdGVnb3JpZXNcIiBjaGVja2JveC5cblx0XHQgKiBcblx0XHQgKiBVcGRhdGVzIGFsbCBjaGVja2JveGVzIGluIHRoZSBjYXRlZ29yaWVzIHRyZWUgKGFsbCBjaGVja2VkIC8gYWxsIHVuY2hlY2tlZClcblx0XHQgKiBcblx0XHQgKiBAcmV0dXJucyB7Ym9vbGVhbn1cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9jaGFuZ2VTZWxlY3RBbGxDYXRlZ29yaWVzQ2hlY2tib3goKSB7XG5cdFx0XHRjb25zdCBjaGVja2VkID0gJCh0aGlzKS5pcygnOmNoZWNrZWQnKSA/ICdzZWxmX2FsbF9zdWJfY2hlY2tlZCcgOiAnbm9fc2VsZl9ub19zdWJfY2hlY2tlZCc7XG5cdFx0XHRjb25zdCBwYXNzT24gPSAkKHRoaXMpLmlzKCc6Y2hlY2tlZCcpID8gJ3Bhc3Nfb25fc2VsZl9hbGxfc3ViX2NoZWNrZWQnIDogJ3Bhc3Nfb25fbm9fc2VsZl9ub19zdWJfY2hlY2tlZCc7XG5cdFx0XHRcblx0XHRcdCQob3B0aW9ucy5jYXRlZ29yeUNoZWNrYm94KS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRfc2V0Q2hlY2tib3hTdGF0ZSgkKHRoaXMpLCBjaGVja2VkKTtcblx0XHRcdFx0JCh0aGlzKS5yZW1vdmVDbGFzcygncGFzc19vbl9zZWxmX2FsbF9zdWJfY2hlY2tlZCcpO1xuXHRcdFx0XHQkKHRoaXMpLnJlbW92ZUNsYXNzKCdwYXNzX29uX25vX3NlbGZfbm9fc3ViX2NoZWNrZWQnKTtcblx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcyhwYXNzT24pO1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgKCQodGhpcykuaGFzQ2xhc3MoJ3Bhc3Nfb25fbm9fc2VsZl9ub19zdWJfY2hlY2tlZCcpKSB7XG5cdFx0XHRcdFx0JCh0aGlzKS5wYXJlbnQoJ2xpJykuZmluZChvcHRpb25zLmJlcXVlYXRoaW5nU3RhdGUgKyAnOmZpcnN0JykudmFsKCdub19zZWxmX25vX3N1YicpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdCQodGhpcykucGFyZW50KCdsaScpLmZpbmQob3B0aW9ucy5iZXF1ZWF0aGluZ1N0YXRlICsgJzpmaXJzdCcpLnZhbCgnc2VsZl9hbGxfc3ViJyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRcblx0XHRcdCR0aGlzLmZpbmQob3B0aW9ucy5jYXRlZ29yeUZvbGRlcikub24oJ2NsaWNrJywgX2NsaWNrQ2F0ZWdvcnkpO1xuXHRcdFx0JHRoaXMuZmluZChvcHRpb25zLmNhdGVnb3J5Q2hlY2tib3gpLm9uKCdjbGljaycsIF9jbGlja0NoZWNrYm94KTtcblx0XHRcdCR0aGlzLmZpbmQob3B0aW9ucy5zZWxlY3RBbGxDYXRlZ29yaWVzKS5vbignY2hhbmdlJywgX2NoYW5nZVNlbGVjdEFsbENhdGVnb3JpZXNDaGVja2JveClcblx0XHRcdFxuXHRcdFx0X3VwZGF0ZVNlbGVjdEFsbENhdGVnb3JpZXNDaGVja2JveCgpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTsiXX0=
