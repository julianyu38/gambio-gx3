'use strict';

/* --------------------------------------------------------------
 scheme_overview.js 2017-12-15
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gxmodules.controllers.module('scheme_overview', ['modal'], function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Flag for cancelling export
  *
  * @type {object}
  */
	var cancelExport = false;

	/**
  * Modal objects
  *
  * @type {jQuery}
  */
	var $modals = {
		'delete': $('.delete-scheme.modal'),
		'export': $('.export-scheme.modal')
	};

	/**
  * Download button.
  *
  * @type {jQuery}
  */
	var $downloadButton = $('<a />', {
		class: 'download-export-button',
		html: $('<i/>', { class: 'fa fa-download' }),
		on: {
			click: function click(event) {
				return _downloadExport(event);
			}
		}
	});

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {
		'showAccountForm': true
	};

	/**
  * Final Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * URLs for deleting different types of content
  *
  * @type {{deleteScheme: string, runExport: string}}
  */
	var urls = {
		'deleteScheme': 'admin.php?do=GoogleShoppingAjax/deleteScheme',
		'runExport': 'admin.php?do=GoogleShoppingAjax/runExport',
		'clearExport': 'admin.php?do=GoogleShoppingAjax/clearExport',
		'downloadExport': 'admin.php?do=GoogleShoppingAjax/downloadSchemeExport'
	};

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// HELPER FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Updates the progress bar in the export modal
  *
  * @param {number} progress  Current progress in percent.
  * @param {boolean} canceled  True, if the export was canceled.
  */
	function _updateProgressBar(progress) {
		var canceled = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

		var $progressBar = $modals.export.find('.progress .progress-bar');

		if (canceled) {
			$progressBar.removeClass('active');
			$progressBar.addClass('progress-bar-danger');
			return;
		}

		if (!Number.isInteger(progress) || progress < 0) {
			progress = 0;
		} else if (progress > 100) {
			progress = 100;
		}

		$progressBar.removeClass('progress-bar-danger');
		$progressBar.addClass('active');
		if (progress === 100) {
			$progressBar.removeClass('active');
		}

		$progressBar.prop('aria-valuenow', progress);
		$progressBar.css('width', progress + '%');
		$progressBar.text(progress + '%');
	}

	/**
  * Returns the current time
  *
  * The time is needed for an update of the export scheme overview after an export.
  */
	function _getCurrentExportTime() {
		var data = new Date();
		var year = data.getFullYear();
		var month = data.getMonth() + 1 < 10 ? '0' + data.getMonth() + 1 : data.getMonth() + 1;
		var day = data.getDate() < 10 ? '0' + data.getDate() : data.getDate();
		var hour = data.getHours() < 10 ? '0' + data.getHours() : data.getHours();
		var minute = data.getMinutes() < 10 ? '0' + data.getMinutes() : data.getMinutes();
		var seconds = '00';

		return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + seconds;
	}

	/**
  * Run scheme export
  *
  * Runs the initial and all further post calls to the Google Shopping ajax controller to create the csv export.
  */
	function _runExport(schemeId, $schemeRow) {
		$.ajax({
			type: "POST",
			url: urls.runExport,
			data: { schemeId: schemeId },
			success: function success(response) {
				response = JSON.parse(response);

				// Hide modal and show error modal if export was not successful
				if (response['success'] === false) {
					$modals.export.modal('hide');
					jse.libs.modal.showMessage(jse.core.lang.translate('ERROR_TITLE', 'google_shopping'), response['error']);
					return;
				}

				// Cancel export if cancel button was clicked
				if (cancelExport === true) {
					// Update modal with informations about export canceled
					$modals.export.find('.modal-text').text(jse.core.lang.translate('EXPORT_SCHEME_MODAL_CANCELED', 'google_shopping'));
					$modals.export.find('button.cancel').text(jse.core.lang.translate('BUTTON_CLOSE', 'admin_buttons')).off('click').on('click', function () {
						return $modals.export.modal('hide');
					});
					$modals.export.find('button.cancel').prop('disabled', false);

					// Ajax call to clean up the export
					$.ajax({
						type: "POST",
						url: urls.clearExport,
						data: { 'schemeId': schemeId }
					});

					return;
				}

				// Update progress bar and make another ajax call if export is not completely done
				if (response['repeat'] === true) {
					_updateProgressBar(response['progress']);
					_runExport(schemeId, $schemeRow);
					return;
				}

				// Update progressbar to 100% and export modal
				_updateProgressBar(100);
				var $cancelButton = $modals.export.find('button.cancel');
				$cancelButton.text(jse.core.lang.translate('BUTTON_CLOSE', 'admin_buttons')).off('click').on('click', function () {
					return $modals.export.modal('hide');
				});

				// Update export date for this scheme in the overview
				$schemeRow.find('.last-export').html('<a class="download-export" href="#" data-scheme-id="' + schemeId + '">' + _getCurrentExportTime() + '</a>');

				$schemeRow.find('.last-export').find('.download-export').on('click', _downloadExport);

				// Add download button to the row, if none exist yet.
				if (!$schemeRow.find('.download-export-button').length) {
					$downloadButton.attr('data-scheme-id', schemeId);
					$schemeRow.find('.actions-container').prepend($downloadButton);
				}
			},
			error: function error() {
				$modals.export.modal('hide');
				jse.libs.modal.showMessage(jse.core.lang.translate('ERROR_TITLE', 'google_shopping'), jse.core.lang.translate('ERROR_EXPORT_AJAX_FAILED', 'google_shopping'));
			}
		});
	}

	/**
  * Delete scheme
  *
  * Runs the post call to the Google Shopping ajax handler to delete the given scheme
  */
	function _deleteScheme(schemeId, $schemeRow) {
		$.ajax({
			type: "POST",
			url: urls.deleteScheme,
			data: { schemeId: schemeId },
			success: function success(response) {
				response = JSON.parse(response);

				if (response['success'] === true) {
					$modals.delete.modal('hide');
					$schemeRow.remove();

					var $tableBody = $('.schemes-overview table tbody');
					var $tableRows = $tableBody.children();
					var $emptyTableTemplate = $('#empty-table');

					if ($tableRows.length < 1) {
						$tableBody.append($emptyTableTemplate.clone().html());
					}
				} else {
					$modals.delete.modal('hide');
					jse.libs.modal.showMessage(jse.core.lang.translate('ERROR_TITLE', 'google_shopping'), response['error']);
				}

				return response['success'];
			},
			error: function error() {
				return false;
			}
		});
	}

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Handler for the the click event of the cancel button in the export modal
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _cancelExport(event) {
		cancelExport = true;
		_updateProgressBar(0, true);
		$modals.export.find('.modal-text').text(jse.core.lang.translate('EXPORT_SCHEME_MODAL_WILL_BE_CANCELED', 'google_shopping'));
		$modals.export.find('button.cancel').prop('disabled', true);
	}

	/**
  * Click handler for the start export icons
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _onExportStart(event) {
		// Prevent default action.
		event.preventDefault();

		// Reset flag for export canceled
		cancelExport = false;

		// Collect the scheme id and the associated table row
		var schemeId = $(this).data('scheme-id');
		var $schemeRow = $(this).closest('tr');

		// Show export modal
		_updateProgressBar(0);
		$modals.export.find('button.cancel').text(jse.core.lang.translate('BUTTON_CANCEL', 'admin_buttons')).off('click').on('click', function (event) {
			return _cancelExport(event);
		});

		$modals.export.find('.modal-text').text(jse.core.lang.translate('EXPORT_SCHEME_MODAL_MESSAGE', 'google_shopping'));

		$modals.export.modal({
			show: true,
			backdrop: "static",
			keyboard: false
		});

		// Start export
		_runExport(schemeId, $schemeRow);
	}

	/**
  * Click handler for the delete scheme icons
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _showDeleteModal(event) {
		// Prevent default action.
		event.preventDefault();

		// Collect the scheme id and the associated table row
		var schemeId = $(this).data('scheme-id');
		var $schemeRow = $(this).closest('tr');

		// Show modal
		$modals.delete.find('fieldset.scheme-data div.scheme-name').text($schemeRow.find('.scheme-name').text());
		$modals.delete.modal('show');

		// Handle delete confirmation modal button click event
		var $confirmButton = $modals.delete.find('button.confirm');
		$confirmButton.off('click').on('click', function () {
			return _deleteScheme(schemeId, $schemeRow);
		});
	}

	/**
  * Handler for the account modal, that will be displayed if the user is not connected with Google AdWords
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _showAccountModal(event) {
		// Prevent default action.
		if (event !== undefined) {
			event.preventDefault();
		}

		// Show modal
		$('.adwords-account.modal').modal('show');
	}

	/**
  * Click handler for the download export link
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _downloadExport(event) {
		event.preventDefault();

		// Collect the scheme id and the associated table row
		var schemeId = $(this).data('scheme-id') ? $(this).data('scheme-id') : $(event.target).parent('a').data('scheme-id');

		// Open export for download
		window.open(urls.downloadExport + "&schemeId=" + schemeId, '_blank');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$('a.delete-scheme').on('click', _showDeleteModal);

		if (options.showAccountForm) {
			_showAccountModal();
			$('a.start-export').on('click', _showAccountModal);
			$('a.download-export').on('click', _showAccountModal);
			$('a.download-export-button').on('click', _showAccountModal);
		} else {
			$('a.start-export').on('click', _onExportStart);
			$('a.download-export').on('click', _downloadExport);
			$('a.download-export-button').on('click', _downloadExport);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvY29udHJvbGxlcnMvc2NoZW1lX292ZXJ2aWV3LmpzIl0sIm5hbWVzIjpbImd4bW9kdWxlcyIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImNhbmNlbEV4cG9ydCIsIiRtb2RhbHMiLCIkZG93bmxvYWRCdXR0b24iLCJjbGFzcyIsImh0bWwiLCJvbiIsImNsaWNrIiwiX2Rvd25sb2FkRXhwb3J0IiwiZXZlbnQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJ1cmxzIiwiX3VwZGF0ZVByb2dyZXNzQmFyIiwicHJvZ3Jlc3MiLCJjYW5jZWxlZCIsIiRwcm9ncmVzc0JhciIsImV4cG9ydCIsImZpbmQiLCJyZW1vdmVDbGFzcyIsImFkZENsYXNzIiwiTnVtYmVyIiwiaXNJbnRlZ2VyIiwicHJvcCIsImNzcyIsInRleHQiLCJfZ2V0Q3VycmVudEV4cG9ydFRpbWUiLCJEYXRlIiwieWVhciIsImdldEZ1bGxZZWFyIiwibW9udGgiLCJnZXRNb250aCIsImRheSIsImdldERhdGUiLCJob3VyIiwiZ2V0SG91cnMiLCJtaW51dGUiLCJnZXRNaW51dGVzIiwic2Vjb25kcyIsIl9ydW5FeHBvcnQiLCJzY2hlbWVJZCIsIiRzY2hlbWVSb3ciLCJhamF4IiwidHlwZSIsInVybCIsInJ1bkV4cG9ydCIsInN1Y2Nlc3MiLCJyZXNwb25zZSIsIkpTT04iLCJwYXJzZSIsIm1vZGFsIiwianNlIiwibGlicyIsInNob3dNZXNzYWdlIiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJvZmYiLCJjbGVhckV4cG9ydCIsIiRjYW5jZWxCdXR0b24iLCJsZW5ndGgiLCJhdHRyIiwicHJlcGVuZCIsImVycm9yIiwiX2RlbGV0ZVNjaGVtZSIsImRlbGV0ZVNjaGVtZSIsImRlbGV0ZSIsInJlbW92ZSIsIiR0YWJsZUJvZHkiLCIkdGFibGVSb3dzIiwiY2hpbGRyZW4iLCIkZW1wdHlUYWJsZVRlbXBsYXRlIiwiYXBwZW5kIiwiY2xvbmUiLCJfY2FuY2VsRXhwb3J0IiwiX29uRXhwb3J0U3RhcnQiLCJwcmV2ZW50RGVmYXVsdCIsImNsb3Nlc3QiLCJzaG93IiwiYmFja2Ryb3AiLCJrZXlib2FyZCIsIl9zaG93RGVsZXRlTW9kYWwiLCIkY29uZmlybUJ1dHRvbiIsIl9zaG93QWNjb3VudE1vZGFsIiwidW5kZWZpbmVkIiwidGFyZ2V0IiwicGFyZW50Iiwid2luZG93Iiwib3BlbiIsImRvd25sb2FkRXhwb3J0IiwiaW5pdCIsImRvbmUiLCJzaG93QWNjb3VudEZvcm0iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsVUFBVUMsV0FBVixDQUFzQkMsTUFBdEIsQ0FDQyxpQkFERCxFQUdDLENBQ0MsT0FERCxDQUhELEVBT0MsVUFBU0MsSUFBVCxFQUFlO0FBQ2Q7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFJQyxlQUFlLEtBQW5COztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVU7QUFDZixZQUFVRixFQUFFLHNCQUFGLENBREs7QUFFZixZQUFVQSxFQUFFLHNCQUFGO0FBRkssRUFBaEI7O0FBS0E7Ozs7O0FBS0EsS0FBTUcsa0JBQWtCSCxFQUFFLE9BQUYsRUFBVztBQUNsQ0ksU0FBTyx3QkFEMkI7QUFFbENDLFFBQU1MLEVBQUUsTUFBRixFQUFVLEVBQUNJLE9BQU8sZ0JBQVIsRUFBVixDQUY0QjtBQUdsQ0UsTUFBSTtBQUNIQyxVQUFPO0FBQUEsV0FBU0MsZ0JBQWdCQyxLQUFoQixDQUFUO0FBQUE7QUFESjtBQUg4QixFQUFYLENBQXhCOztBQVFBOzs7OztBQUtBLEtBQU1DLFdBQVc7QUFDaEIscUJBQW1CO0FBREgsRUFBakI7O0FBSUE7Ozs7O0FBS0EsS0FBTUMsVUFBVVgsRUFBRVksTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QlosSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTWUsT0FBTztBQUNaLGtCQUFnQiw4Q0FESjtBQUVaLGVBQWEsMkNBRkQ7QUFHWixpQkFBZSw2Q0FISDtBQUlaLG9CQUFrQjtBQUpOLEVBQWI7O0FBT0E7Ozs7O0FBS0EsS0FBTWhCLFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BLFVBQVNpQixrQkFBVCxDQUE0QkMsUUFBNUIsRUFBd0Q7QUFBQSxNQUFsQkMsUUFBa0IsdUVBQVAsS0FBTzs7QUFDdkQsTUFBTUMsZUFBZWYsUUFBUWdCLE1BQVIsQ0FBZUMsSUFBZixDQUFvQix5QkFBcEIsQ0FBckI7O0FBRUEsTUFBSUgsUUFBSixFQUFjO0FBQ2JDLGdCQUFhRyxXQUFiLENBQXlCLFFBQXpCO0FBQ0FILGdCQUFhSSxRQUFiLENBQXNCLHFCQUF0QjtBQUNBO0FBQ0E7O0FBRUQsTUFBSSxDQUFDQyxPQUFPQyxTQUFQLENBQWlCUixRQUFqQixDQUFELElBQStCQSxXQUFXLENBQTlDLEVBQWlEO0FBQ2hEQSxjQUFXLENBQVg7QUFDQSxHQUZELE1BRU8sSUFBSUEsV0FBVyxHQUFmLEVBQW9CO0FBQzFCQSxjQUFXLEdBQVg7QUFDQTs7QUFFREUsZUFBYUcsV0FBYixDQUF5QixxQkFBekI7QUFDQUgsZUFBYUksUUFBYixDQUFzQixRQUF0QjtBQUNBLE1BQUlOLGFBQWEsR0FBakIsRUFBc0I7QUFDckJFLGdCQUFhRyxXQUFiLENBQXlCLFFBQXpCO0FBQ0E7O0FBRURILGVBQWFPLElBQWIsQ0FBa0IsZUFBbEIsRUFBbUNULFFBQW5DO0FBQ0FFLGVBQWFRLEdBQWIsQ0FBaUIsT0FBakIsRUFBMEJWLFdBQVcsR0FBckM7QUFDQUUsZUFBYVMsSUFBYixDQUFrQlgsV0FBVyxHQUE3QjtBQUNBOztBQUdEOzs7OztBQUtBLFVBQVNZLHFCQUFULEdBQWlDO0FBQ2hDLE1BQU03QixPQUFVLElBQUk4QixJQUFKLEVBQWhCO0FBQ0EsTUFBTUMsT0FBVS9CLEtBQUtnQyxXQUFMLEVBQWhCO0FBQ0EsTUFBTUMsUUFBWWpDLEtBQUtrQyxRQUFMLEtBQWtCLENBQWxCLEdBQXNCLEVBQXZCLEdBQTZCLE1BQU1sQyxLQUFLa0MsUUFBTCxFQUFOLEdBQXdCLENBQXJELEdBQXlEbEMsS0FBS2tDLFFBQUwsS0FBa0IsQ0FBNUY7QUFDQSxNQUFNQyxNQUFZbkMsS0FBS29DLE9BQUwsS0FBaUIsRUFBbEIsR0FBd0IsTUFBTXBDLEtBQUtvQyxPQUFMLEVBQTlCLEdBQStDcEMsS0FBS29DLE9BQUwsRUFBaEU7QUFDQSxNQUFNQyxPQUFZckMsS0FBS3NDLFFBQUwsS0FBa0IsRUFBbkIsR0FBeUIsTUFBTXRDLEtBQUtzQyxRQUFMLEVBQS9CLEdBQWlEdEMsS0FBS3NDLFFBQUwsRUFBbEU7QUFDQSxNQUFNQyxTQUFZdkMsS0FBS3dDLFVBQUwsS0FBb0IsRUFBckIsR0FBMkIsTUFBTXhDLEtBQUt3QyxVQUFMLEVBQWpDLEdBQXFEeEMsS0FBS3dDLFVBQUwsRUFBdEU7QUFDQSxNQUFNQyxVQUFVLElBQWhCOztBQUVBLFNBQU9WLE9BQU8sR0FBUCxHQUFhRSxLQUFiLEdBQXFCLEdBQXJCLEdBQTJCRSxHQUEzQixHQUFpQyxHQUFqQyxHQUF1Q0UsSUFBdkMsR0FBOEMsR0FBOUMsR0FBb0RFLE1BQXBELEdBQTZELEdBQTdELEdBQW1FRSxPQUExRTtBQUNBOztBQUdEOzs7OztBQUtBLFVBQVNDLFVBQVQsQ0FBb0JDLFFBQXBCLEVBQThCQyxVQUE5QixFQUEwQztBQUN6QzFDLElBQUUyQyxJQUFGLENBQU87QUFDTkMsU0FBTSxNQURBO0FBRU5DLFFBQUtoQyxLQUFLaUMsU0FGSjtBQUdOaEQsU0FBTSxFQUFDMkMsa0JBQUQsRUFIQTtBQUlOTSxZQUFTLGlCQUFTQyxRQUFULEVBQW1CO0FBQzNCQSxlQUFXQyxLQUFLQyxLQUFMLENBQVdGLFFBQVgsQ0FBWDs7QUFFQTtBQUNBLFFBQUlBLFNBQVMsU0FBVCxNQUF3QixLQUE1QixFQUFtQztBQUNsQzlDLGFBQVFnQixNQUFSLENBQWVpQyxLQUFmLENBQXFCLE1BQXJCO0FBQ0FDLFNBQUlDLElBQUosQ0FBU0YsS0FBVCxDQUFlRyxXQUFmLENBQTJCRixJQUFJRyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxpQkFBdkMsQ0FBM0IsRUFBc0ZULFNBQVMsT0FBVCxDQUF0RjtBQUNBO0FBQ0E7O0FBRUQ7QUFDQSxRQUFJL0MsaUJBQWlCLElBQXJCLEVBQTJCO0FBQzFCO0FBQ0FDLGFBQVFnQixNQUFSLENBQWVDLElBQWYsQ0FBb0IsYUFBcEIsRUFDRU8sSUFERixDQUNPMEIsSUFBSUcsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsOEJBQXhCLEVBQXdELGlCQUF4RCxDQURQO0FBRUF2RCxhQUFRZ0IsTUFBUixDQUFlQyxJQUFmLENBQW9CLGVBQXBCLEVBQ0VPLElBREYsQ0FDTzBCLElBQUlHLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGNBQXhCLEVBQXdDLGVBQXhDLENBRFAsRUFFRUMsR0FGRixDQUVNLE9BRk4sRUFHRXBELEVBSEYsQ0FHSyxPQUhMLEVBR2M7QUFBQSxhQUFNSixRQUFRZ0IsTUFBUixDQUFlaUMsS0FBZixDQUFxQixNQUFyQixDQUFOO0FBQUEsTUFIZDtBQUlBakQsYUFBUWdCLE1BQVIsQ0FBZUMsSUFBZixDQUFvQixlQUFwQixFQUNFSyxJQURGLENBQ08sVUFEUCxFQUNtQixLQURuQjs7QUFHQTtBQUNBeEIsT0FBRTJDLElBQUYsQ0FBTztBQUNOQyxZQUFNLE1BREE7QUFFTkMsV0FBS2hDLEtBQUs4QyxXQUZKO0FBR043RCxZQUFNLEVBQUMsWUFBWTJDLFFBQWI7QUFIQSxNQUFQOztBQU1BO0FBQ0E7O0FBRUQ7QUFDQSxRQUFJTyxTQUFTLFFBQVQsTUFBdUIsSUFBM0IsRUFBaUM7QUFDaENsQyx3QkFBbUJrQyxTQUFTLFVBQVQsQ0FBbkI7QUFDQVIsZ0JBQVdDLFFBQVgsRUFBcUJDLFVBQXJCO0FBQ0E7QUFDQTs7QUFFRDtBQUNBNUIsdUJBQW1CLEdBQW5CO0FBQ0EsUUFBTThDLGdCQUFnQjFELFFBQVFnQixNQUFSLENBQWVDLElBQWYsQ0FBb0IsZUFBcEIsQ0FBdEI7QUFDQXlDLGtCQUNFbEMsSUFERixDQUNPMEIsSUFBSUcsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsY0FBeEIsRUFBd0MsZUFBeEMsQ0FEUCxFQUVFQyxHQUZGLENBRU0sT0FGTixFQUdFcEQsRUFIRixDQUdLLE9BSEwsRUFHYztBQUFBLFlBQU1KLFFBQVFnQixNQUFSLENBQWVpQyxLQUFmLENBQXFCLE1BQXJCLENBQU47QUFBQSxLQUhkOztBQUtBO0FBQ0FULGVBQVd2QixJQUFYLENBQWdCLGNBQWhCLEVBQWdDZCxJQUFoQyxDQUNDLHlEQUF5RG9DLFFBQXpELEdBQW9FLElBQXBFLEdBQ0VkLHVCQURGLEdBQzRCLE1BRjdCOztBQUtBZSxlQUFXdkIsSUFBWCxDQUFnQixjQUFoQixFQUFnQ0EsSUFBaEMsQ0FBcUMsa0JBQXJDLEVBQXlEYixFQUF6RCxDQUE0RCxPQUE1RCxFQUFxRUUsZUFBckU7O0FBRUE7QUFDQSxRQUFJLENBQUNrQyxXQUFXdkIsSUFBWCxDQUFnQix5QkFBaEIsRUFBMkMwQyxNQUFoRCxFQUF3RDtBQUN2RDFELHFCQUFnQjJELElBQWhCLENBQXFCLGdCQUFyQixFQUF1Q3JCLFFBQXZDO0FBQ0FDLGdCQUFXdkIsSUFBWCxDQUFnQixvQkFBaEIsRUFBc0M0QyxPQUF0QyxDQUE4QzVELGVBQTlDO0FBQ0E7QUFDRCxJQWhFSztBQWlFTjZELFVBQU8saUJBQVc7QUFDakI5RCxZQUFRZ0IsTUFBUixDQUFlaUMsS0FBZixDQUFxQixNQUFyQjtBQUNBQyxRQUFJQyxJQUFKLENBQVNGLEtBQVQsQ0FBZUcsV0FBZixDQUNDRixJQUFJRyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxpQkFBdkMsQ0FERCxFQUVDTCxJQUFJRyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QiwwQkFBeEIsRUFBb0QsaUJBQXBELENBRkQ7QUFJQTtBQXZFSyxHQUFQO0FBeUVBOztBQUVEOzs7OztBQUtBLFVBQVNRLGFBQVQsQ0FBdUJ4QixRQUF2QixFQUFpQ0MsVUFBakMsRUFBNkM7QUFDNUMxQyxJQUFFMkMsSUFBRixDQUFPO0FBQ05DLFNBQU0sTUFEQTtBQUVOQyxRQUFLaEMsS0FBS3FELFlBRko7QUFHTnBFLFNBQU0sRUFBQzJDLGtCQUFELEVBSEE7QUFJTk0sWUFBUyxpQkFBU0MsUUFBVCxFQUFtQjtBQUMzQkEsZUFBV0MsS0FBS0MsS0FBTCxDQUFXRixRQUFYLENBQVg7O0FBRUEsUUFBSUEsU0FBUyxTQUFULE1BQXdCLElBQTVCLEVBQWtDO0FBQ2pDOUMsYUFBUWlFLE1BQVIsQ0FBZWhCLEtBQWYsQ0FBcUIsTUFBckI7QUFDQVQsZ0JBQVcwQixNQUFYOztBQUVBLFNBQU1DLGFBQXNCckUsRUFBRSwrQkFBRixDQUE1QjtBQUNBLFNBQU1zRSxhQUFzQkQsV0FBV0UsUUFBWCxFQUE1QjtBQUNBLFNBQU1DLHNCQUFzQnhFLEVBQUUsY0FBRixDQUE1Qjs7QUFFQSxTQUFJc0UsV0FBV1QsTUFBWCxHQUFvQixDQUF4QixFQUEyQjtBQUMxQlEsaUJBQVdJLE1BQVgsQ0FBa0JELG9CQUFvQkUsS0FBcEIsR0FBNEJyRSxJQUE1QixFQUFsQjtBQUNBO0FBQ0QsS0FYRCxNQVdPO0FBQ05ILGFBQVFpRSxNQUFSLENBQWVoQixLQUFmLENBQXFCLE1BQXJCO0FBQ0FDLFNBQUlDLElBQUosQ0FBU0YsS0FBVCxDQUFlRyxXQUFmLENBQTJCRixJQUFJRyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxpQkFBdkMsQ0FBM0IsRUFBc0ZULFNBQVMsT0FBVCxDQUF0RjtBQUNBOztBQUVELFdBQU9BLFNBQVMsU0FBVCxDQUFQO0FBQ0EsSUF4Qks7QUF5Qk5nQixVQUFPLGlCQUFXO0FBQ2pCLFdBQU8sS0FBUDtBQUNBO0FBM0JLLEdBQVA7QUE2QkE7O0FBR0Q7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNXLGFBQVQsQ0FBdUJsRSxLQUF2QixFQUE4QjtBQUM3QlIsaUJBQWUsSUFBZjtBQUNBYSxxQkFBbUIsQ0FBbkIsRUFBc0IsSUFBdEI7QUFDQVosVUFBUWdCLE1BQVIsQ0FBZUMsSUFBZixDQUFvQixhQUFwQixFQUNFTyxJQURGLENBQ08wQixJQUFJRyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixzQ0FBeEIsRUFBZ0UsaUJBQWhFLENBRFA7QUFFQXZELFVBQVFnQixNQUFSLENBQWVDLElBQWYsQ0FBb0IsZUFBcEIsRUFDRUssSUFERixDQUNPLFVBRFAsRUFDbUIsSUFEbkI7QUFFQTs7QUFFRDs7Ozs7QUFLQSxVQUFTb0QsY0FBVCxDQUF3Qm5FLEtBQXhCLEVBQStCO0FBQzlCO0FBQ0FBLFFBQU1vRSxjQUFOOztBQUVBO0FBQ0E1RSxpQkFBZSxLQUFmOztBQUVBO0FBQ0EsTUFBTXdDLFdBQWF6QyxFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLFdBQWIsQ0FBbkI7QUFDQSxNQUFNNEMsYUFBYTFDLEVBQUUsSUFBRixFQUFROEUsT0FBUixDQUFnQixJQUFoQixDQUFuQjs7QUFFQTtBQUNBaEUscUJBQW1CLENBQW5CO0FBQ0FaLFVBQVFnQixNQUFSLENBQWVDLElBQWYsQ0FBb0IsZUFBcEIsRUFDRU8sSUFERixDQUNPMEIsSUFBSUcsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsZUFBeEIsRUFBeUMsZUFBekMsQ0FEUCxFQUVFQyxHQUZGLENBRU0sT0FGTixFQUdFcEQsRUFIRixDQUdLLE9BSEwsRUFHYyxVQUFDRyxLQUFEO0FBQUEsVUFBV2tFLGNBQWNsRSxLQUFkLENBQVg7QUFBQSxHQUhkOztBQUtBUCxVQUFRZ0IsTUFBUixDQUFlQyxJQUFmLENBQW9CLGFBQXBCLEVBQ0VPLElBREYsQ0FDTzBCLElBQUlHLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLDZCQUF4QixFQUF1RCxpQkFBdkQsQ0FEUDs7QUFHQXZELFVBQVFnQixNQUFSLENBQWVpQyxLQUFmLENBQXFCO0FBQ3BCNEIsU0FBTSxJQURjO0FBRXBCQyxhQUFVLFFBRlU7QUFHcEJDLGFBQVU7QUFIVSxHQUFyQjs7QUFNQTtBQUNBekMsYUFBV0MsUUFBWCxFQUFxQkMsVUFBckI7QUFDQTs7QUFFRDs7Ozs7QUFLQSxVQUFTd0MsZ0JBQVQsQ0FBMEJ6RSxLQUExQixFQUFpQztBQUNoQztBQUNBQSxRQUFNb0UsY0FBTjs7QUFFQTtBQUNBLE1BQU1wQyxXQUFhekMsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxXQUFiLENBQW5CO0FBQ0EsTUFBTTRDLGFBQWExQyxFQUFFLElBQUYsRUFBUThFLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbkI7O0FBRUE7QUFDQTVFLFVBQVFpRSxNQUFSLENBQWVoRCxJQUFmLENBQW9CLHNDQUFwQixFQUE0RE8sSUFBNUQsQ0FBaUVnQixXQUFXdkIsSUFBWCxDQUFnQixjQUFoQixFQUFnQ08sSUFBaEMsRUFBakU7QUFDQXhCLFVBQVFpRSxNQUFSLENBQWVoQixLQUFmLENBQXFCLE1BQXJCOztBQUVBO0FBQ0EsTUFBTWdDLGlCQUFpQmpGLFFBQVFpRSxNQUFSLENBQWVoRCxJQUFmLENBQW9CLGdCQUFwQixDQUF2QjtBQUNBZ0UsaUJBQ0V6QixHQURGLENBQ00sT0FETixFQUVFcEQsRUFGRixDQUVLLE9BRkwsRUFFYztBQUFBLFVBQU0yRCxjQUFjeEIsUUFBZCxFQUF3QkMsVUFBeEIsQ0FBTjtBQUFBLEdBRmQ7QUFHQTs7QUFFRDs7Ozs7QUFLQSxVQUFTMEMsaUJBQVQsQ0FBMkIzRSxLQUEzQixFQUFrQztBQUNqQztBQUNBLE1BQUlBLFVBQVU0RSxTQUFkLEVBQXlCO0FBQ3hCNUUsU0FBTW9FLGNBQU47QUFDQTs7QUFFRDtBQUNBN0UsSUFBRSx3QkFBRixFQUE0Qm1ELEtBQTVCLENBQWtDLE1BQWxDO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBUzNDLGVBQVQsQ0FBeUJDLEtBQXpCLEVBQWdDO0FBQy9CQSxRQUFNb0UsY0FBTjs7QUFFQTtBQUNBLE1BQU1wQyxXQUFXekMsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxXQUFiLElBQ2RFLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsV0FBYixDQURjLEdBRWRFLEVBQUVTLE1BQU02RSxNQUFSLEVBQWdCQyxNQUFoQixDQUF1QixHQUF2QixFQUE0QnpGLElBQTVCLENBQWlDLFdBQWpDLENBRkg7O0FBSUE7QUFDQTBGLFNBQU9DLElBQVAsQ0FBWTVFLEtBQUs2RSxjQUFMLEdBQXNCLFlBQXRCLEdBQXFDakQsUUFBakQsRUFBMkQsUUFBM0Q7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE1QyxRQUFPOEYsSUFBUCxHQUFjLFVBQVNDLElBQVQsRUFBZTtBQUM1QjVGLElBQUUsaUJBQUYsRUFBcUJNLEVBQXJCLENBQXdCLE9BQXhCLEVBQWlDNEUsZ0JBQWpDOztBQUVBLE1BQUl2RSxRQUFRa0YsZUFBWixFQUE2QjtBQUM1QlQ7QUFDQXBGLEtBQUUsZ0JBQUYsRUFBb0JNLEVBQXBCLENBQXVCLE9BQXZCLEVBQWdDOEUsaUJBQWhDO0FBQ0FwRixLQUFFLG1CQUFGLEVBQXVCTSxFQUF2QixDQUEwQixPQUExQixFQUFtQzhFLGlCQUFuQztBQUNBcEYsS0FBRSwwQkFBRixFQUE4Qk0sRUFBOUIsQ0FBaUMsT0FBakMsRUFBMEM4RSxpQkFBMUM7QUFDQSxHQUxELE1BS087QUFDTnBGLEtBQUUsZ0JBQUYsRUFBb0JNLEVBQXBCLENBQXVCLE9BQXZCLEVBQWdDc0UsY0FBaEM7QUFDQTVFLEtBQUUsbUJBQUYsRUFBdUJNLEVBQXZCLENBQTBCLE9BQTFCLEVBQW1DRSxlQUFuQztBQUNBUixLQUFFLDBCQUFGLEVBQThCTSxFQUE5QixDQUFpQyxPQUFqQyxFQUEwQ0UsZUFBMUM7QUFDQTs7QUFFRG9GO0FBQ0EsRUFmRDs7QUFpQkEsUUFBTy9GLE1BQVA7QUFDQSxDQXhZRiIsImZpbGUiOiJBZG1pbi9KYXZhc2NyaXB0L2NvbnRyb2xsZXJzL3NjaGVtZV9vdmVydmlldy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gc2NoZW1lX292ZXJ2aWV3LmpzIDIwMTctMTItMTVcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5neG1vZHVsZXMuY29udHJvbGxlcnMubW9kdWxlKFxuXHQnc2NoZW1lX292ZXJ2aWV3Jyxcblx0XG5cdFtcblx0XHQnbW9kYWwnXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFUyBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGbGFnIGZvciBjYW5jZWxsaW5nIGV4cG9ydFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRsZXQgY2FuY2VsRXhwb3J0ID0gZmFsc2U7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kYWwgb2JqZWN0c1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkbW9kYWxzID0ge1xuXHRcdFx0J2RlbGV0ZSc6ICQoJy5kZWxldGUtc2NoZW1lLm1vZGFsJyksXG5cdFx0XHQnZXhwb3J0JzogJCgnLmV4cG9ydC1zY2hlbWUubW9kYWwnKVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRG93bmxvYWQgYnV0dG9uLlxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkZG93bmxvYWRCdXR0b24gPSAkKCc8YSAvPicsIHtcblx0XHRcdGNsYXNzOiAnZG93bmxvYWQtZXhwb3J0LWJ1dHRvbicsXG5cdFx0XHRodG1sOiAkKCc8aS8+Jywge2NsYXNzOiAnZmEgZmEtZG93bmxvYWQnfSksXG5cdFx0XHRvbjoge1xuXHRcdFx0XHRjbGljazogZXZlbnQgPT4gX2Rvd25sb2FkRXhwb3J0KGV2ZW50KVxuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmF1bHQgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBkZWZhdWx0cyA9IHtcblx0XHRcdCdzaG93QWNjb3VudEZvcm0nOiB0cnVlXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVSTHMgZm9yIGRlbGV0aW5nIGRpZmZlcmVudCB0eXBlcyBvZiBjb250ZW50XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7e2RlbGV0ZVNjaGVtZTogc3RyaW5nLCBydW5FeHBvcnQ6IHN0cmluZ319XG5cdFx0ICovXG5cdFx0Y29uc3QgdXJscyA9IHtcblx0XHRcdCdkZWxldGVTY2hlbWUnOiAnYWRtaW4ucGhwP2RvPUdvb2dsZVNob3BwaW5nQWpheC9kZWxldGVTY2hlbWUnLFxuXHRcdFx0J3J1bkV4cG9ydCc6ICdhZG1pbi5waHA/ZG89R29vZ2xlU2hvcHBpbmdBamF4L3J1bkV4cG9ydCcsXG5cdFx0XHQnY2xlYXJFeHBvcnQnOiAnYWRtaW4ucGhwP2RvPUdvb2dsZVNob3BwaW5nQWpheC9jbGVhckV4cG9ydCcsXG5cdFx0XHQnZG93bmxvYWRFeHBvcnQnOiAnYWRtaW4ucGhwP2RvPUdvb2dsZVNob3BwaW5nQWpheC9kb3dubG9hZFNjaGVtZUV4cG9ydCdcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gSEVMUEVSIEZVTkNUSU9OU1xuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZXMgdGhlIHByb2dyZXNzIGJhciBpbiB0aGUgZXhwb3J0IG1vZGFsXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge251bWJlcn0gcHJvZ3Jlc3MgIEN1cnJlbnQgcHJvZ3Jlc3MgaW4gcGVyY2VudC5cblx0XHQgKiBAcGFyYW0ge2Jvb2xlYW59IGNhbmNlbGVkICBUcnVlLCBpZiB0aGUgZXhwb3J0IHdhcyBjYW5jZWxlZC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfdXBkYXRlUHJvZ3Jlc3NCYXIocHJvZ3Jlc3MsIGNhbmNlbGVkID0gZmFsc2UpIHtcblx0XHRcdGNvbnN0ICRwcm9ncmVzc0JhciA9ICRtb2RhbHMuZXhwb3J0LmZpbmQoJy5wcm9ncmVzcyAucHJvZ3Jlc3MtYmFyJyk7XG5cdFx0XHRcblx0XHRcdGlmIChjYW5jZWxlZCkge1xuXHRcdFx0XHQkcHJvZ3Jlc3NCYXIucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHQkcHJvZ3Jlc3NCYXIuYWRkQ2xhc3MoJ3Byb2dyZXNzLWJhci1kYW5nZXInKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAoIU51bWJlci5pc0ludGVnZXIocHJvZ3Jlc3MpIHx8IHByb2dyZXNzIDwgMCkge1xuXHRcdFx0XHRwcm9ncmVzcyA9IDA7XG5cdFx0XHR9IGVsc2UgaWYgKHByb2dyZXNzID4gMTAwKSB7XG5cdFx0XHRcdHByb2dyZXNzID0gMTAwO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkcHJvZ3Jlc3NCYXIucmVtb3ZlQ2xhc3MoJ3Byb2dyZXNzLWJhci1kYW5nZXInKTtcblx0XHRcdCRwcm9ncmVzc0Jhci5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRpZiAocHJvZ3Jlc3MgPT09IDEwMCkge1xuXHRcdFx0XHQkcHJvZ3Jlc3NCYXIucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkcHJvZ3Jlc3NCYXIucHJvcCgnYXJpYS12YWx1ZW5vdycsIHByb2dyZXNzKTtcblx0XHRcdCRwcm9ncmVzc0Jhci5jc3MoJ3dpZHRoJywgcHJvZ3Jlc3MgKyAnJScpO1xuXHRcdFx0JHByb2dyZXNzQmFyLnRleHQocHJvZ3Jlc3MgKyAnJScpO1xuXHRcdH1cblx0XHRcblx0XHRcblx0XHQvKipcblx0XHQgKiBSZXR1cm5zIHRoZSBjdXJyZW50IHRpbWVcblx0XHQgKlxuXHRcdCAqIFRoZSB0aW1lIGlzIG5lZWRlZCBmb3IgYW4gdXBkYXRlIG9mIHRoZSBleHBvcnQgc2NoZW1lIG92ZXJ2aWV3IGFmdGVyIGFuIGV4cG9ydC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfZ2V0Q3VycmVudEV4cG9ydFRpbWUoKSB7XG5cdFx0XHRjb25zdCBkYXRhICAgID0gbmV3IERhdGUoKTtcblx0XHRcdGNvbnN0IHllYXIgICAgPSBkYXRhLmdldEZ1bGxZZWFyKCk7XG5cdFx0XHRjb25zdCBtb250aCAgID0gKChkYXRhLmdldE1vbnRoKCkgKyAxIDwgMTApID8gJzAnICsgZGF0YS5nZXRNb250aCgpICsgMSA6IGRhdGEuZ2V0TW9udGgoKSArIDEpO1xuXHRcdFx0Y29uc3QgZGF5ICAgICA9ICgoZGF0YS5nZXREYXRlKCkgPCAxMCkgPyAnMCcgKyBkYXRhLmdldERhdGUoKSA6IGRhdGEuZ2V0RGF0ZSgpKTtcblx0XHRcdGNvbnN0IGhvdXIgICAgPSAoKGRhdGEuZ2V0SG91cnMoKSA8IDEwKSA/ICcwJyArIGRhdGEuZ2V0SG91cnMoKSA6IGRhdGEuZ2V0SG91cnMoKSk7XG5cdFx0XHRjb25zdCBtaW51dGUgID0gKChkYXRhLmdldE1pbnV0ZXMoKSA8IDEwKSA/ICcwJyArIGRhdGEuZ2V0TWludXRlcygpIDogZGF0YS5nZXRNaW51dGVzKCkpO1xuXHRcdFx0Y29uc3Qgc2Vjb25kcyA9ICcwMCc7XG5cdFx0XHRcblx0XHRcdHJldHVybiB5ZWFyICsgJy0nICsgbW9udGggKyAnLScgKyBkYXkgKyAnICcgKyBob3VyICsgJzonICsgbWludXRlICsgJzonICsgc2Vjb25kcztcblx0XHR9XG5cdFx0XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUnVuIHNjaGVtZSBleHBvcnRcblx0XHQgKlxuXHRcdCAqIFJ1bnMgdGhlIGluaXRpYWwgYW5kIGFsbCBmdXJ0aGVyIHBvc3QgY2FsbHMgdG8gdGhlIEdvb2dsZSBTaG9wcGluZyBhamF4IGNvbnRyb2xsZXIgdG8gY3JlYXRlIHRoZSBjc3YgZXhwb3J0LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9ydW5FeHBvcnQoc2NoZW1lSWQsICRzY2hlbWVSb3cpIHtcblx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdHR5cGU6IFwiUE9TVFwiLFxuXHRcdFx0XHR1cmw6IHVybHMucnVuRXhwb3J0LFxuXHRcdFx0XHRkYXRhOiB7c2NoZW1lSWR9LFxuXHRcdFx0XHRzdWNjZXNzOiBmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdHJlc3BvbnNlID0gSlNPTi5wYXJzZShyZXNwb25zZSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0Ly8gSGlkZSBtb2RhbCBhbmQgc2hvdyBlcnJvciBtb2RhbCBpZiBleHBvcnQgd2FzIG5vdCBzdWNjZXNzZnVsXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlWydzdWNjZXNzJ10gPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0XHQkbW9kYWxzLmV4cG9ydC5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0VSUk9SX1RJVExFJywgJ2dvb2dsZV9zaG9wcGluZycpLCByZXNwb25zZVsnZXJyb3InXSk7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIENhbmNlbCBleHBvcnQgaWYgY2FuY2VsIGJ1dHRvbiB3YXMgY2xpY2tlZFxuXHRcdFx0XHRcdGlmIChjYW5jZWxFeHBvcnQgPT09IHRydWUpIHtcblx0XHRcdFx0XHRcdC8vIFVwZGF0ZSBtb2RhbCB3aXRoIGluZm9ybWF0aW9ucyBhYm91dCBleHBvcnQgY2FuY2VsZWRcblx0XHRcdFx0XHRcdCRtb2RhbHMuZXhwb3J0LmZpbmQoJy5tb2RhbC10ZXh0Jylcblx0XHRcdFx0XHRcdFx0LnRleHQoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0VYUE9SVF9TQ0hFTUVfTU9EQUxfQ0FOQ0VMRUQnLCAnZ29vZ2xlX3Nob3BwaW5nJykpO1xuXHRcdFx0XHRcdFx0JG1vZGFscy5leHBvcnQuZmluZCgnYnV0dG9uLmNhbmNlbCcpXG5cdFx0XHRcdFx0XHRcdC50ZXh0KGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdCVVRUT05fQ0xPU0UnLCAnYWRtaW5fYnV0dG9ucycpKVxuXHRcdFx0XHRcdFx0XHQub2ZmKCdjbGljaycpXG5cdFx0XHRcdFx0XHRcdC5vbignY2xpY2snLCAoKSA9PiAkbW9kYWxzLmV4cG9ydC5tb2RhbCgnaGlkZScpKTtcblx0XHRcdFx0XHRcdCRtb2RhbHMuZXhwb3J0LmZpbmQoJ2J1dHRvbi5jYW5jZWwnKVxuXHRcdFx0XHRcdFx0XHQucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdC8vIEFqYXggY2FsbCB0byBjbGVhbiB1cCB0aGUgZXhwb3J0XG5cdFx0XHRcdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHRcdFx0XHR0eXBlOiBcIlBPU1RcIixcblx0XHRcdFx0XHRcdFx0dXJsOiB1cmxzLmNsZWFyRXhwb3J0LFxuXHRcdFx0XHRcdFx0XHRkYXRhOiB7J3NjaGVtZUlkJzogc2NoZW1lSWR9XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBVcGRhdGUgcHJvZ3Jlc3MgYmFyIGFuZCBtYWtlIGFub3RoZXIgYWpheCBjYWxsIGlmIGV4cG9ydCBpcyBub3QgY29tcGxldGVseSBkb25lXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlWydyZXBlYXQnXSA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdFx0X3VwZGF0ZVByb2dyZXNzQmFyKHJlc3BvbnNlWydwcm9ncmVzcyddKTtcblx0XHRcdFx0XHRcdF9ydW5FeHBvcnQoc2NoZW1lSWQsICRzY2hlbWVSb3cpO1xuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBVcGRhdGUgcHJvZ3Jlc3NiYXIgdG8gMTAwJSBhbmQgZXhwb3J0IG1vZGFsXG5cdFx0XHRcdFx0X3VwZGF0ZVByb2dyZXNzQmFyKDEwMCk7XG5cdFx0XHRcdFx0Y29uc3QgJGNhbmNlbEJ1dHRvbiA9ICRtb2RhbHMuZXhwb3J0LmZpbmQoJ2J1dHRvbi5jYW5jZWwnKTtcblx0XHRcdFx0XHQkY2FuY2VsQnV0dG9uXG5cdFx0XHRcdFx0XHQudGV4dChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnQlVUVE9OX0NMT1NFJywgJ2FkbWluX2J1dHRvbnMnKSlcblx0XHRcdFx0XHRcdC5vZmYoJ2NsaWNrJylcblx0XHRcdFx0XHRcdC5vbignY2xpY2snLCAoKSA9PiAkbW9kYWxzLmV4cG9ydC5tb2RhbCgnaGlkZScpKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBVcGRhdGUgZXhwb3J0IGRhdGUgZm9yIHRoaXMgc2NoZW1lIGluIHRoZSBvdmVydmlld1xuXHRcdFx0XHRcdCRzY2hlbWVSb3cuZmluZCgnLmxhc3QtZXhwb3J0JykuaHRtbChcblx0XHRcdFx0XHRcdCc8YSBjbGFzcz1cImRvd25sb2FkLWV4cG9ydFwiIGhyZWY9XCIjXCIgZGF0YS1zY2hlbWUtaWQ9XCInICsgc2NoZW1lSWQgKyAnXCI+J1xuXHRcdFx0XHRcdFx0KyBfZ2V0Q3VycmVudEV4cG9ydFRpbWUoKSArICc8L2E+J1xuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0JHNjaGVtZVJvdy5maW5kKCcubGFzdC1leHBvcnQnKS5maW5kKCcuZG93bmxvYWQtZXhwb3J0Jykub24oJ2NsaWNrJywgX2Rvd25sb2FkRXhwb3J0KTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBBZGQgZG93bmxvYWQgYnV0dG9uIHRvIHRoZSByb3csIGlmIG5vbmUgZXhpc3QgeWV0LlxuXHRcdFx0XHRcdGlmICghJHNjaGVtZVJvdy5maW5kKCcuZG93bmxvYWQtZXhwb3J0LWJ1dHRvbicpLmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0JGRvd25sb2FkQnV0dG9uLmF0dHIoJ2RhdGEtc2NoZW1lLWlkJywgc2NoZW1lSWQpO1xuXHRcdFx0XHRcdFx0JHNjaGVtZVJvdy5maW5kKCcuYWN0aW9ucy1jb250YWluZXInKS5wcmVwZW5kKCRkb3dubG9hZEJ1dHRvbik7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9LFxuXHRcdFx0XHRlcnJvcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0JG1vZGFscy5leHBvcnQubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShcblx0XHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdFUlJPUl9USVRMRScsICdnb29nbGVfc2hvcHBpbmcnKSxcblx0XHRcdFx0XHRcdGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdFUlJPUl9FWFBPUlRfQUpBWF9GQUlMRUQnLCAnZ29vZ2xlX3Nob3BwaW5nJylcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVsZXRlIHNjaGVtZVxuXHRcdCAqXG5cdFx0ICogUnVucyB0aGUgcG9zdCBjYWxsIHRvIHRoZSBHb29nbGUgU2hvcHBpbmcgYWpheCBoYW5kbGVyIHRvIGRlbGV0ZSB0aGUgZ2l2ZW4gc2NoZW1lXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2RlbGV0ZVNjaGVtZShzY2hlbWVJZCwgJHNjaGVtZVJvdykge1xuXHRcdFx0JC5hamF4KHtcblx0XHRcdFx0dHlwZTogXCJQT1NUXCIsXG5cdFx0XHRcdHVybDogdXJscy5kZWxldGVTY2hlbWUsXG5cdFx0XHRcdGRhdGE6IHtzY2hlbWVJZH0sXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0cmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAocmVzcG9uc2VbJ3N1Y2Nlc3MnXSA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdFx0JG1vZGFscy5kZWxldGUubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0XHRcdCRzY2hlbWVSb3cucmVtb3ZlKCk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdGNvbnN0ICR0YWJsZUJvZHkgICAgICAgICAgPSAkKCcuc2NoZW1lcy1vdmVydmlldyB0YWJsZSB0Ym9keScpO1xuXHRcdFx0XHRcdFx0Y29uc3QgJHRhYmxlUm93cyAgICAgICAgICA9ICR0YWJsZUJvZHkuY2hpbGRyZW4oKTtcblx0XHRcdFx0XHRcdGNvbnN0ICRlbXB0eVRhYmxlVGVtcGxhdGUgPSAkKCcjZW1wdHktdGFibGUnKTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0aWYgKCR0YWJsZVJvd3MubGVuZ3RoIDwgMSkge1xuXHRcdFx0XHRcdFx0XHQkdGFibGVCb2R5LmFwcGVuZCgkZW1wdHlUYWJsZVRlbXBsYXRlLmNsb25lKCkuaHRtbCgpKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0JG1vZGFscy5kZWxldGUubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0XHRcdGpzZS5saWJzLm1vZGFsLnNob3dNZXNzYWdlKGpzZS5jb3JlLmxhbmcudHJhbnNsYXRlKCdFUlJPUl9USVRMRScsICdnb29nbGVfc2hvcHBpbmcnKSwgcmVzcG9uc2VbJ2Vycm9yJ10pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHRyZXR1cm4gcmVzcG9uc2VbJ3N1Y2Nlc3MnXTtcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXJyb3I6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlciBmb3IgdGhlIHRoZSBjbGljayBldmVudCBvZiB0aGUgY2FuY2VsIGJ1dHRvbiBpbiB0aGUgZXhwb3J0IG1vZGFsXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdCBjb250YWlucyBpbmZvcm1hdGlvbiBvZiB0aGUgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2NhbmNlbEV4cG9ydChldmVudCkge1xuXHRcdFx0Y2FuY2VsRXhwb3J0ID0gdHJ1ZTtcblx0XHRcdF91cGRhdGVQcm9ncmVzc0JhcigwLCB0cnVlKTtcblx0XHRcdCRtb2RhbHMuZXhwb3J0LmZpbmQoJy5tb2RhbC10ZXh0Jylcblx0XHRcdFx0LnRleHQoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0VYUE9SVF9TQ0hFTUVfTU9EQUxfV0lMTF9CRV9DQU5DRUxFRCcsICdnb29nbGVfc2hvcHBpbmcnKSk7XG5cdFx0XHQkbW9kYWxzLmV4cG9ydC5maW5kKCdidXR0b24uY2FuY2VsJylcblx0XHRcdFx0LnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsaWNrIGhhbmRsZXIgZm9yIHRoZSBzdGFydCBleHBvcnQgaWNvbnNcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfb25FeHBvcnRTdGFydChldmVudCkge1xuXHRcdFx0Ly8gUHJldmVudCBkZWZhdWx0IGFjdGlvbi5cblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcblx0XHRcdC8vIFJlc2V0IGZsYWcgZm9yIGV4cG9ydCBjYW5jZWxlZFxuXHRcdFx0Y2FuY2VsRXhwb3J0ID0gZmFsc2U7XG5cdFx0XHRcblx0XHRcdC8vIENvbGxlY3QgdGhlIHNjaGVtZSBpZCBhbmQgdGhlIGFzc29jaWF0ZWQgdGFibGUgcm93XG5cdFx0XHRjb25zdCBzY2hlbWVJZCAgID0gJCh0aGlzKS5kYXRhKCdzY2hlbWUtaWQnKTtcblx0XHRcdGNvbnN0ICRzY2hlbWVSb3cgPSAkKHRoaXMpLmNsb3Nlc3QoJ3RyJyk7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgZXhwb3J0IG1vZGFsXG5cdFx0XHRfdXBkYXRlUHJvZ3Jlc3NCYXIoMCk7XG5cdFx0XHQkbW9kYWxzLmV4cG9ydC5maW5kKCdidXR0b24uY2FuY2VsJylcblx0XHRcdFx0LnRleHQoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0JVVFRPTl9DQU5DRUwnLCAnYWRtaW5fYnV0dG9ucycpKVxuXHRcdFx0XHQub2ZmKCdjbGljaycpXG5cdFx0XHRcdC5vbignY2xpY2snLCAoZXZlbnQpID0+IF9jYW5jZWxFeHBvcnQoZXZlbnQpKTtcblx0XHRcdFxuXHRcdFx0JG1vZGFscy5leHBvcnQuZmluZCgnLm1vZGFsLXRleHQnKVxuXHRcdFx0XHQudGV4dChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVhQT1JUX1NDSEVNRV9NT0RBTF9NRVNTQUdFJywgJ2dvb2dsZV9zaG9wcGluZycpKTtcblx0XHRcdFxuXHRcdFx0JG1vZGFscy5leHBvcnQubW9kYWwoe1xuXHRcdFx0XHRzaG93OiB0cnVlLFxuXHRcdFx0XHRiYWNrZHJvcDogXCJzdGF0aWNcIixcblx0XHRcdFx0a2V5Ym9hcmQ6IGZhbHNlXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gU3RhcnQgZXhwb3J0XG5cdFx0XHRfcnVuRXhwb3J0KHNjaGVtZUlkLCAkc2NoZW1lUm93KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciBmb3IgdGhlIGRlbGV0ZSBzY2hlbWUgaWNvbnNcblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2hvd0RlbGV0ZU1vZGFsKGV2ZW50KSB7XG5cdFx0XHQvLyBQcmV2ZW50IGRlZmF1bHQgYWN0aW9uLlxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFxuXHRcdFx0Ly8gQ29sbGVjdCB0aGUgc2NoZW1lIGlkIGFuZCB0aGUgYXNzb2NpYXRlZCB0YWJsZSByb3dcblx0XHRcdGNvbnN0IHNjaGVtZUlkICAgPSAkKHRoaXMpLmRhdGEoJ3NjaGVtZS1pZCcpO1xuXHRcdFx0Y29uc3QgJHNjaGVtZVJvdyA9ICQodGhpcykuY2xvc2VzdCgndHInKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBtb2RhbFxuXHRcdFx0JG1vZGFscy5kZWxldGUuZmluZCgnZmllbGRzZXQuc2NoZW1lLWRhdGEgZGl2LnNjaGVtZS1uYW1lJykudGV4dCgkc2NoZW1lUm93LmZpbmQoJy5zY2hlbWUtbmFtZScpLnRleHQoKSk7XG5cdFx0XHQkbW9kYWxzLmRlbGV0ZS5tb2RhbCgnc2hvdycpO1xuXHRcdFx0XG5cdFx0XHQvLyBIYW5kbGUgZGVsZXRlIGNvbmZpcm1hdGlvbiBtb2RhbCBidXR0b24gY2xpY2sgZXZlbnRcblx0XHRcdGNvbnN0ICRjb25maXJtQnV0dG9uID0gJG1vZGFscy5kZWxldGUuZmluZCgnYnV0dG9uLmNvbmZpcm0nKTtcblx0XHRcdCRjb25maXJtQnV0dG9uXG5cdFx0XHRcdC5vZmYoJ2NsaWNrJylcblx0XHRcdFx0Lm9uKCdjbGljaycsICgpID0+IF9kZWxldGVTY2hlbWUoc2NoZW1lSWQsICRzY2hlbWVSb3cpKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSGFuZGxlciBmb3IgdGhlIGFjY291bnQgbW9kYWwsIHRoYXQgd2lsbCBiZSBkaXNwbGF5ZWQgaWYgdGhlIHVzZXIgaXMgbm90IGNvbm5lY3RlZCB3aXRoIEdvb2dsZSBBZFdvcmRzXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdCBjb250YWlucyBpbmZvcm1hdGlvbiBvZiB0aGUgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3Nob3dBY2NvdW50TW9kYWwoZXZlbnQpIHtcblx0XHRcdC8vIFByZXZlbnQgZGVmYXVsdCBhY3Rpb24uXG5cdFx0XHRpZiAoZXZlbnQgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBTaG93IG1vZGFsXG5cdFx0XHQkKCcuYWR3b3Jkcy1hY2NvdW50Lm1vZGFsJykubW9kYWwoJ3Nob3cnKTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciBmb3IgdGhlIGRvd25sb2FkIGV4cG9ydCBsaW5rXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdCBjb250YWlucyBpbmZvcm1hdGlvbiBvZiB0aGUgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2Rvd25sb2FkRXhwb3J0KGV2ZW50KSB7XG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHQvLyBDb2xsZWN0IHRoZSBzY2hlbWUgaWQgYW5kIHRoZSBhc3NvY2lhdGVkIHRhYmxlIHJvd1xuXHRcdFx0Y29uc3Qgc2NoZW1lSWQgPSAkKHRoaXMpLmRhdGEoJ3NjaGVtZS1pZCcpXG5cdFx0XHRcdD8gJCh0aGlzKS5kYXRhKCdzY2hlbWUtaWQnKVxuXHRcdFx0XHQ6ICQoZXZlbnQudGFyZ2V0KS5wYXJlbnQoJ2EnKS5kYXRhKCdzY2hlbWUtaWQnKTtcblx0XHRcdFxuXHRcdFx0Ly8gT3BlbiBleHBvcnQgZm9yIGRvd25sb2FkXG5cdFx0XHR3aW5kb3cub3Blbih1cmxzLmRvd25sb2FkRXhwb3J0ICsgXCImc2NoZW1lSWQ9XCIgKyBzY2hlbWVJZCwgJ19ibGFuaycpO1xuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JCgnYS5kZWxldGUtc2NoZW1lJykub24oJ2NsaWNrJywgX3Nob3dEZWxldGVNb2RhbCk7XG5cdFx0XHRcblx0XHRcdGlmIChvcHRpb25zLnNob3dBY2NvdW50Rm9ybSkge1xuXHRcdFx0XHRfc2hvd0FjY291bnRNb2RhbCgpO1xuXHRcdFx0XHQkKCdhLnN0YXJ0LWV4cG9ydCcpLm9uKCdjbGljaycsIF9zaG93QWNjb3VudE1vZGFsKTtcblx0XHRcdFx0JCgnYS5kb3dubG9hZC1leHBvcnQnKS5vbignY2xpY2snLCBfc2hvd0FjY291bnRNb2RhbCk7XG5cdFx0XHRcdCQoJ2EuZG93bmxvYWQtZXhwb3J0LWJ1dHRvbicpLm9uKCdjbGljaycsIF9zaG93QWNjb3VudE1vZGFsKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCQoJ2Euc3RhcnQtZXhwb3J0Jykub24oJ2NsaWNrJywgX29uRXhwb3J0U3RhcnQpO1xuXHRcdFx0XHQkKCdhLmRvd25sb2FkLWV4cG9ydCcpLm9uKCdjbGljaycsIF9kb3dubG9hZEV4cG9ydCk7XG5cdFx0XHRcdCQoJ2EuZG93bmxvYWQtZXhwb3J0LWJ1dHRvbicpLm9uKCdjbGljaycsIF9kb3dubG9hZEV4cG9ydCk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pOyJdfQ==
