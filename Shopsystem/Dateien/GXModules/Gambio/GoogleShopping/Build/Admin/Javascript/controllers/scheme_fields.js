'use strict';

/* --------------------------------------------------------------
 scheme_fields.js 2017-11-20
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gxmodules.controllers.module('scheme_fields', ['loading_spinner', 'modal', jse.source + '/vendor/jquery-ui/jquery-ui.min.css', jse.source + '/vendor/jquery-ui/jquery-ui.js'], function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Deleting modal object
  *
  * @type {jQuery}
  */
	var $modals = {
		'edit': $('.scheme-field.modal'),
		'delete': $('.delete-field.modal')
	};

	/**
  * Sortable list
  *
  * @type {jQuery}
  */
	var $sortableList = $('.fields-list');

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * URLs for deleting different types of content
  *
  * @type {{deleteScheme: string, runExport: string}}
  */
	var urls = {
		'getFieldData': 'admin.php?do=GoogleShoppingAjax/getSchemeFieldData',
		'saveFieldData': 'admin.php?do=GoogleShoppingAjax/storeSchemeField',
		'deleteField': 'admin.php?do=GoogleShoppingAjax/deleteSchemeField',
		'updateStatus': 'admin.php?do=GoogleShoppingAjax/setSchemeFieldStatus',
		'saveSorting': 'admin.php?do=GoogleShoppingAjax/saveSchemeFieldsSorting'
	};

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// HELPER FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Delete field
  *
  * Runs the post call to the Google Shopping ajax handler to delete the given scheme
  */
	function _deleteField(schemeId, fieldId, $fieldRow) {
		$.ajax({
			type: "POST",
			url: urls.deleteField,
			data: {
				'schemeId': schemeId,
				'fieldId': fieldId
			},
			success: function success(response) {
				response = JSON.parse(response);

				if (response['success'] === true) {
					$fieldRow.remove();
				}

				$modals.delete.modal('hide');
			},
			error: function error() {
				$modals.delete.modal('hide');
			}
		});
	}

	/**
  * Updates the field modal
  *
  * Resets the modal and changes the title and the input values depending of the action
  */
	function _updateFieldModal(action) {
		var fieldData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

		if (fieldData === undefined) {
			fieldData = {
				'schemeId': '',
				'name': '',
				'value': '',
				'default': ''
			};
		}

		if (action === 'create') {
			$modals.edit.find('.modal-title').text(jse.core.lang.translate('FIELD_MODAL_TITLE_CREATE', 'google_shopping'));
		} else if (action === 'edit') {
			$modals.edit.find('.modal-title').text(jse.core.lang.translate('FIELD_MODAL_TITLE_EDIT', 'google_shopping'));
		}

		$modals.edit.find('select.collective-field option').prop("selected", false);
		$modals.edit.find('select.field-variable').val('');
		$modals.edit.find('div.collective-field').addClass('hidden');
		$modals.edit.find('input[name="schemeId"]').val(fieldData.schemeId);
		$modals.edit.find('input[name="name"]').val(fieldData.name);
		$modals.edit.find('input[name="value"]').val(fieldData.value);
		$modals.edit.find('input[name="default"]').val(fieldData.default);
		$modals.edit.find('div.field-variable-description').text('').addClass('hidden');
	}

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Click handler for the create field button
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _showCreateModal(event) {
		// Prevent default action.
		event.preventDefault();

		// Show field creation modal
		_updateFieldModal('create');
		$modals.edit.modal('show');
	}

	/**
  * Click handler for the edit field icons
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _showEditModal(event) {
		// Prevent default action.
		event.preventDefault();

		// Collect field id
		var fieldId = $(this).data('field-id');

		// Fetch data with ajax controller ...
		$.ajax({
			type: "GET",
			url: urls.getFieldData + '&fieldId=' + fieldId,
			success: function success(response) {
				response = JSON.parse(response);

				// Display modal with field data on success
				if (response.success === true) {
					_updateFieldModal('edit', {
						'schemeId': fieldId,
						'name': response.data.field_name,
						'value': response.data.field_content,
						'default': response.data.field_content_default
					});
					$modals.edit.modal('show');
				}
			},
			error: function error() {
				jse.libs.modal.showMessage(jse.core.lang.translate('ERROR_TITLE', 'google_shopping'), jse.core.lang.translate('ERROR_AJAX_FAILED', 'google_shopping'));
			}
		});
	}

	/**
  * Click handler for the delete field icons
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _showDeleteModal(event) {
		// Prevent default action.
		event.preventDefault();

		// Collect several data
		var schemeId = $('.schemeId').val();
		var fieldId = $(this).data('field-id');
		var $fieldRow = $(this).closest('li');

		// Show modal
		$.ajax({
			type: "GET",
			url: urls.getFieldData + '&fieldId=' + fieldId,
			success: function success(response) {
				response = JSON.parse(response);

				// Display modal with field data on success
				if (response.success === true) {
					$modals.delete.find('fieldset.field-data div.field-name').text(response.data.field_name);
					$modals.delete.find('fieldset.field-data div.field-content').text(response.data.field_content);
					$modals.delete.find('fieldset.field-data div.field-content-default').text(response.data.field_content_default);
				}
				$modals.delete.modal('show');
			},
			error: function error() {
				jse.libs.modal.showMessage(jse.core.lang.translate('ERROR_TITLE', 'google_shopping'), jse.core.lang.translate('ERROR_AJAX_FAILED', 'google_shopping'));
			}
		});

		// Handle delete confirmation modal button click event
		var $confirmButton = $modals.delete.find('button.confirm');
		$confirmButton.off('click').on('click', function () {
			return _deleteField(schemeId, fieldId, $fieldRow);
		});
	}

	/**
  * Click handler for the scheme field status switcher
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _updateFieldStatus(event) {
		// Collect several data
		var schemeId = $('.schemeId').val();
		var fieldId = $(this).data('field-id');
		var newStatus = 0;
		if ($(this).is(':checked')) {
			newStatus = 1;
		}

		// Call ajax controller to update field status
		$.ajax({
			type: "POST",
			url: urls.updateStatus,
			data: {
				'schemeId': schemeId,
				'fieldId': fieldId,
				'newStatus': newStatus
			},
			success: function success(response) {
				response = JSON.parse(response);

				if (response.success === true) {
					$modals.edit.modal('hide');
				}
			},
			error: function error() {
				jse.libs.modal.showMessage(jse.core.lang.translate('ERROR_TITLE', 'google_shopping'), jse.core.lang.translate('ERROR_AJAX_FAILED', 'google_shopping'));
			}
		});
	}

	/**
  * Sorting event handler for sortable plugin
  *
  * Makes a call to the ajax controller after a sorting event
  *
  * @param {object} event jQuery event object contains information of the event.
  * @param {object} ui    Sortable list (ul) object with new sort order
  */
	function _saveSorting(event, ui) {
		if (!ui.item.parent().is('ul')) {
			$sortableList.sortable('cancel');
		}

		$.ajax({
			url: urls.saveSorting,
			method: 'POST',
			data: {
				'schemeId': $('.schemeId').val(),
				'sorting': $sortableList.sortable('toArray', { attribute: 'data-field-id' })
			},
			success: function success(response) {
				response = JSON.parse(response);

				if (response.success === false) {
					jse.libs.modal.showMessage(jse.core.lang.translate('ERROR_TITLE', 'google_shopping'), jse.core.lang.translate('ERROR_SORTING_FAILED', 'google_shopping'));
				}
			},
			error: function error() {
				jse.libs.modal.showMessage(jse.core.lang.translate('ERROR_TITLE', 'google_shopping'), jse.core.lang.translate('ERROR_SORTING_FAILED', 'google_shopping'));
			}
		});
	}

	/**
  * Click handler to save scheme field data
  *
  * Collects the scheme field data from the edit/create modal and makes an ajax call to save them into the db.
  */
	function _saveFieldData() {
		// Collect several data
		var schemeId = $('.schemeId').val();
		var fieldId = $modals.edit.find('input[name="schemeId"]').val();
		var $fieldRow = $this.find('li[data-field-id="' + fieldId + '"]');

		var fieldName = $modals.edit.find('input[name="name"]').val();
		var fieldValue = $modals.edit.find('input[name="value"]').val();
		var fieldDefault = $modals.edit.find('input[name="default"]').val();

		// Make ajax call to ajax controller to save field data
		$.ajax({
			type: "POST",
			url: urls.saveFieldData,
			data: {
				'schemeId': schemeId,
				'fieldId': fieldId,
				'fieldName': fieldName,
				'fieldValue': fieldValue,
				'fieldDefault': fieldDefault
			},
			success: function success(response) {
				response = JSON.parse(response);

				if (response.success === true) {
					if (fieldId === '') {
						$this.find('.fields-list').append('\n\t\t\t\t\t\t\t\t<li class="scheme-field" data-field-id="' + response.fieldId + '">\n\t\t\t\t\t\t\t\t\t<span class="col-md-5 field-name">' + fieldName.replace(/</g, '&lt;').replace(/>/g, '&gt;') + '</span>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<span class="col-md-2 status">\n\t\t\t\t\t\t\t\t\t\t<span class="gx-container" data-gx-widget="switcher">\n\t\t\t\t\t\t\t\t\t\t\t<input type="checkbox"\n\t\t\t\t\t\t\t\t\t\t\t       class="field-status"\n\t\t\t\t\t\t\t\t\t\t\t       data-field-id="' + response.fieldId + '"\n\t\t\t\t\t\t\t\t\t\t\t\t   name="' + fieldName + '_status"\n\t\t\t\t\t\t\t\t\t\t\t\t   value="1" checked/>\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<span class="col-md-5 actions">\n\t\t\t\t\t\t\t\t\t\t<span class="actions-container">\n\t\t\t\t\t\t\t\t\t\t\t<a class="edit-field" href="#" data-field-id="' + response.fieldId + '">\n\t\t\t\t\t\t\t\t\t\t\t\t<i class="fa fa-pencil"></i>\n\t\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t\t<a class="delete-field" href="#" data-field-id="' + response.fieldId + '">\n\t\t\t\t\t\t\t\t\t\t\t\t<i class="fa fa-trash-o"></i>\n\t\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t\t<a class="sort-handle">\n\t\t\t\t\t\t\t\t\t\t\t\t<i class="fa fa-sort"></i>\n\t\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t');

						$this.find('.fields-list .scheme-field:last a.edit-field').on('click', _showEditModal);
						$this.find('.fields-list .scheme-field:last a.delete-field').on('click', _showDeleteModal);
						$this.find('.fields-list .scheme-field:last input.field-status').on('change', _updateFieldStatus);

						gx.widgets.init($this);
					} else {
						$fieldRow.find('.field-name').text(fieldName.replace(/</g, '&lt;').replace(/>/g, '&gt;'));
					}

					$modals.edit.modal('hide');
				}
			}
		});
	}

	/**
  * Change handler for field variable in field modal
  *
  * Displays the collective fields dropdown if field variable dropdown has the correct value
  */
	function _fieldVariableChanged() {
		$modals.edit.find('div.field-variable-description').text($(this).find('option:selected')[0]['title']).removeClass('hidden');

		if ($(this).val() === 'collective_field') {
			$modals.edit.find('div.collective-field').removeClass('hidden');
		} else {
			$modals.edit.find('div.collective-field').addClass('hidden');
		}
	}

	/**
  * Click handler for add field variable button
  *
  * Adds the text for the field variable to the field value input
  */
	function _addFieldVariable(event) {
		// Prevent default action.
		event.preventDefault();

		// Collect some data
		var currentValue = $modals.edit.find('input[name="value"]').val();
		var variable = $modals.edit.find('select.field-variable').val();

		if (variable === 'collective_field') {
			// Collect selected attributes, properties, additional fields and their sources
			var atttributes = $modals.edit.find('select.collective-field').val();
			var sources = [];
			$modals.edit.find('select.collective-field').children('optgroup').each(function () {
				if ($(this).children('option:selected').length > 0) {
					sources.push($(this).data('source'));
				}
			});

			// Add text for collective field
			$modals.edit.find('input[name="value"]').val(currentValue + '{collective_field||' + atttributes.join(';') + '||' + sources.join(';') + '}');
		} else {
			// Add text for normal field varaible
			$modals.edit.find('input[name="value"]').val(currentValue + '{' + variable + '}');
		}
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		$('a.create-field').on('click', _showCreateModal);
		$('a.edit-field').on('click', _showEditModal);
		$('a.delete-field').on('click', _showDeleteModal);
		$('input.field-status').on('change', _updateFieldStatus);

		$modals.edit.find('button.confirm').on('click', _saveFieldData);
		$modals.edit.find('select.field-variable').on('change', _fieldVariableChanged);
		$modals.edit.find('a.add-field-variable').on('click', _addFieldVariable);

		$sortableList.sortable({
			items: 'li.scheme-field',
			axis: 'y',
			cursor: 'move',
			handle: '.sort-handle',
			containment: 'document',
			placeholder: 'sort-placeholder'
		}).on('sortupdate', _saveSorting).disableSelection();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvY29udHJvbGxlcnMvc2NoZW1lX2ZpZWxkcy5qcyJdLCJuYW1lcyI6WyJneG1vZHVsZXMiLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCIkbW9kYWxzIiwiJHNvcnRhYmxlTGlzdCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsInVybHMiLCJfZGVsZXRlRmllbGQiLCJzY2hlbWVJZCIsImZpZWxkSWQiLCIkZmllbGRSb3ciLCJhamF4IiwidHlwZSIsInVybCIsImRlbGV0ZUZpZWxkIiwic3VjY2VzcyIsInJlc3BvbnNlIiwiSlNPTiIsInBhcnNlIiwicmVtb3ZlIiwiZGVsZXRlIiwibW9kYWwiLCJlcnJvciIsIl91cGRhdGVGaWVsZE1vZGFsIiwiYWN0aW9uIiwiZmllbGREYXRhIiwidW5kZWZpbmVkIiwiZWRpdCIsImZpbmQiLCJ0ZXh0IiwiY29yZSIsImxhbmciLCJ0cmFuc2xhdGUiLCJwcm9wIiwidmFsIiwiYWRkQ2xhc3MiLCJuYW1lIiwidmFsdWUiLCJkZWZhdWx0IiwiX3Nob3dDcmVhdGVNb2RhbCIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJfc2hvd0VkaXRNb2RhbCIsImdldEZpZWxkRGF0YSIsImZpZWxkX25hbWUiLCJmaWVsZF9jb250ZW50IiwiZmllbGRfY29udGVudF9kZWZhdWx0IiwibGlicyIsInNob3dNZXNzYWdlIiwiX3Nob3dEZWxldGVNb2RhbCIsImNsb3Nlc3QiLCIkY29uZmlybUJ1dHRvbiIsIm9mZiIsIm9uIiwiX3VwZGF0ZUZpZWxkU3RhdHVzIiwibmV3U3RhdHVzIiwiaXMiLCJ1cGRhdGVTdGF0dXMiLCJfc2F2ZVNvcnRpbmciLCJ1aSIsIml0ZW0iLCJwYXJlbnQiLCJzb3J0YWJsZSIsInNhdmVTb3J0aW5nIiwibWV0aG9kIiwiYXR0cmlidXRlIiwiX3NhdmVGaWVsZERhdGEiLCJmaWVsZE5hbWUiLCJmaWVsZFZhbHVlIiwiZmllbGREZWZhdWx0Iiwic2F2ZUZpZWxkRGF0YSIsImFwcGVuZCIsInJlcGxhY2UiLCJneCIsIndpZGdldHMiLCJpbml0IiwiX2ZpZWxkVmFyaWFibGVDaGFuZ2VkIiwicmVtb3ZlQ2xhc3MiLCJfYWRkRmllbGRWYXJpYWJsZSIsImN1cnJlbnRWYWx1ZSIsInZhcmlhYmxlIiwiYXR0dHJpYnV0ZXMiLCJzb3VyY2VzIiwiY2hpbGRyZW4iLCJlYWNoIiwibGVuZ3RoIiwicHVzaCIsImpvaW4iLCJkb25lIiwiaXRlbXMiLCJheGlzIiwiY3Vyc29yIiwiaGFuZGxlIiwiY29udGFpbm1lbnQiLCJwbGFjZWhvbGRlciIsImRpc2FibGVTZWxlY3Rpb24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsVUFBVUMsV0FBVixDQUFzQkMsTUFBdEIsQ0FDQyxlQURELEVBR0MsQ0FDQyxpQkFERCxFQUVDLE9BRkQsRUFHSUMsSUFBSUMsTUFIUiwwQ0FJSUQsSUFBSUMsTUFKUixvQ0FIRCxFQVVDLFVBQVNDLElBQVQsRUFBZTtBQUNkOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsUUFBUUMsRUFBRSxJQUFGLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVTtBQUNmLFVBQVFELEVBQUUscUJBQUYsQ0FETztBQUVmLFlBQVVBLEVBQUUscUJBQUY7QUFGSyxFQUFoQjs7QUFLQTs7Ozs7QUFLQSxLQUFNRSxnQkFBZ0JGLEVBQUUsY0FBRixDQUF0Qjs7QUFFQTs7Ozs7QUFLQSxLQUFNRyxXQUFXLEVBQWpCOztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVVKLEVBQUVLLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJMLElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1RLE9BQU87QUFDWixrQkFBZ0Isb0RBREo7QUFFWixtQkFBaUIsa0RBRkw7QUFHWixpQkFBZSxtREFISDtBQUlaLGtCQUFnQixzREFKSjtBQUtaLGlCQUFlO0FBTEgsRUFBYjs7QUFRQTs7Ozs7QUFLQSxLQUFNWCxTQUFTLEVBQWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFVBQVNZLFlBQVQsQ0FBc0JDLFFBQXRCLEVBQWdDQyxPQUFoQyxFQUF5Q0MsU0FBekMsRUFBb0Q7QUFDbkRWLElBQUVXLElBQUYsQ0FBTztBQUNOQyxTQUFNLE1BREE7QUFFTkMsUUFBS1AsS0FBS1EsV0FGSjtBQUdOaEIsU0FBTTtBQUNMLGdCQUFZVSxRQURQO0FBRUwsZUFBV0M7QUFGTixJQUhBO0FBT05NLFlBQVMsaUJBQVNDLFFBQVQsRUFBbUI7QUFDM0JBLGVBQVdDLEtBQUtDLEtBQUwsQ0FBV0YsUUFBWCxDQUFYOztBQUVBLFFBQUlBLFNBQVMsU0FBVCxNQUF3QixJQUE1QixFQUFrQztBQUNqQ04sZUFBVVMsTUFBVjtBQUNBOztBQUVEbEIsWUFBUW1CLE1BQVIsQ0FBZUMsS0FBZixDQUFxQixNQUFyQjtBQUNBLElBZks7QUFnQk5DLFVBQU8saUJBQVc7QUFDakJyQixZQUFRbUIsTUFBUixDQUFlQyxLQUFmLENBQXFCLE1BQXJCO0FBQ0E7QUFsQkssR0FBUDtBQW9CQTs7QUFFRDs7Ozs7QUFLQSxVQUFTRSxpQkFBVCxDQUEyQkMsTUFBM0IsRUFBMEQ7QUFBQSxNQUF2QkMsU0FBdUIsdUVBQVhDLFNBQVc7O0FBQ3pELE1BQUlELGNBQWNDLFNBQWxCLEVBQTZCO0FBQzVCRCxlQUFZO0FBQ1gsZ0JBQVksRUFERDtBQUVYLFlBQVEsRUFGRztBQUdYLGFBQVMsRUFIRTtBQUlYLGVBQVc7QUFKQSxJQUFaO0FBTUE7O0FBRUQsTUFBSUQsV0FBVyxRQUFmLEVBQXlCO0FBQ3hCdkIsV0FBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixjQUFsQixFQUNFQyxJQURGLENBQ09qQyxJQUFJa0MsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsMEJBQXhCLEVBQW9ELGlCQUFwRCxDQURQO0FBRUEsR0FIRCxNQUdPLElBQUlSLFdBQVcsTUFBZixFQUF1QjtBQUM3QnZCLFdBQVEwQixJQUFSLENBQWFDLElBQWIsQ0FBa0IsY0FBbEIsRUFDRUMsSUFERixDQUNPakMsSUFBSWtDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLHdCQUF4QixFQUFrRCxpQkFBbEQsQ0FEUDtBQUVBOztBQUVEL0IsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixnQ0FBbEIsRUFBb0RLLElBQXBELENBQXlELFVBQXpELEVBQXFFLEtBQXJFO0FBQ0FoQyxVQUFRMEIsSUFBUixDQUFhQyxJQUFiLENBQWtCLHVCQUFsQixFQUEyQ00sR0FBM0MsQ0FBK0MsRUFBL0M7QUFDQWpDLFVBQVEwQixJQUFSLENBQWFDLElBQWIsQ0FBa0Isc0JBQWxCLEVBQTBDTyxRQUExQyxDQUFtRCxRQUFuRDtBQUNBbEMsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQix3QkFBbEIsRUFBNENNLEdBQTVDLENBQWdEVCxVQUFVakIsUUFBMUQ7QUFDQVAsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixvQkFBbEIsRUFBd0NNLEdBQXhDLENBQTRDVCxVQUFVVyxJQUF0RDtBQUNBbkMsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixxQkFBbEIsRUFBeUNNLEdBQXpDLENBQTZDVCxVQUFVWSxLQUF2RDtBQUNBcEMsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQix1QkFBbEIsRUFBMkNNLEdBQTNDLENBQStDVCxVQUFVYSxPQUF6RDtBQUNBckMsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixnQ0FBbEIsRUFDRUMsSUFERixDQUNPLEVBRFAsRUFFRU0sUUFGRixDQUVXLFFBRlg7QUFHQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU0ksZ0JBQVQsQ0FBMEJDLEtBQTFCLEVBQWlDO0FBQ2hDO0FBQ0FBLFFBQU1DLGNBQU47O0FBRUE7QUFDQWxCLG9CQUFrQixRQUFsQjtBQUNBdEIsVUFBUTBCLElBQVIsQ0FBYU4sS0FBYixDQUFtQixNQUFuQjtBQUNBOztBQUVEOzs7OztBQUtBLFVBQVNxQixjQUFULENBQXdCRixLQUF4QixFQUErQjtBQUM5QjtBQUNBQSxRQUFNQyxjQUFOOztBQUVBO0FBQ0EsTUFBTWhDLFVBQVVULEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsVUFBYixDQUFoQjs7QUFFQTtBQUNBRSxJQUFFVyxJQUFGLENBQU87QUFDTkMsU0FBTSxLQURBO0FBRU5DLFFBQUtQLEtBQUtxQyxZQUFMLEdBQW9CLFdBQXBCLEdBQWtDbEMsT0FGakM7QUFHTk0sWUFBUyxpQkFBU0MsUUFBVCxFQUFtQjtBQUMzQkEsZUFBV0MsS0FBS0MsS0FBTCxDQUFXRixRQUFYLENBQVg7O0FBRUE7QUFDQSxRQUFJQSxTQUFTRCxPQUFULEtBQXFCLElBQXpCLEVBQStCO0FBQzlCUSx1QkFBa0IsTUFBbEIsRUFBMEI7QUFDekIsa0JBQVlkLE9BRGE7QUFFekIsY0FBUU8sU0FBU2xCLElBQVQsQ0FBYzhDLFVBRkc7QUFHekIsZUFBUzVCLFNBQVNsQixJQUFULENBQWMrQyxhQUhFO0FBSXpCLGlCQUFXN0IsU0FBU2xCLElBQVQsQ0FBY2dEO0FBSkEsTUFBMUI7QUFNQTdDLGFBQVEwQixJQUFSLENBQWFOLEtBQWIsQ0FBbUIsTUFBbkI7QUFDQTtBQUNELElBaEJLO0FBaUJOQyxVQUFPLGlCQUFXO0FBQ2pCMUIsUUFBSW1ELElBQUosQ0FBUzFCLEtBQVQsQ0FBZTJCLFdBQWYsQ0FDQ3BELElBQUlrQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxpQkFBdkMsQ0FERCxFQUVDcEMsSUFBSWtDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1CQUF4QixFQUE2QyxpQkFBN0MsQ0FGRDtBQUlBO0FBdEJLLEdBQVA7QUF3QkE7O0FBRUQ7Ozs7O0FBS0EsVUFBU2lCLGdCQUFULENBQTBCVCxLQUExQixFQUFpQztBQUNoQztBQUNBQSxRQUFNQyxjQUFOOztBQUVBO0FBQ0EsTUFBTWpDLFdBQVdSLEVBQUUsV0FBRixFQUFla0MsR0FBZixFQUFqQjtBQUNBLE1BQU16QixVQUFVVCxFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLFVBQWIsQ0FBaEI7QUFDQSxNQUFNWSxZQUFZVixFQUFFLElBQUYsRUFBUWtELE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBbEI7O0FBRUE7QUFDQWxELElBQUVXLElBQUYsQ0FBTztBQUNOQyxTQUFNLEtBREE7QUFFTkMsUUFBS1AsS0FBS3FDLFlBQUwsR0FBb0IsV0FBcEIsR0FBa0NsQyxPQUZqQztBQUdOTSxZQUFTLGlCQUFTQyxRQUFULEVBQW1CO0FBQzNCQSxlQUFXQyxLQUFLQyxLQUFMLENBQVdGLFFBQVgsQ0FBWDs7QUFFQTtBQUNBLFFBQUlBLFNBQVNELE9BQVQsS0FBcUIsSUFBekIsRUFBK0I7QUFDOUJkLGFBQVFtQixNQUFSLENBQWVRLElBQWYsQ0FBb0Isb0NBQXBCLEVBQTBEQyxJQUExRCxDQUErRGIsU0FBU2xCLElBQVQsQ0FBYzhDLFVBQTdFO0FBQ0EzQyxhQUFRbUIsTUFBUixDQUFlUSxJQUFmLENBQW9CLHVDQUFwQixFQUE2REMsSUFBN0QsQ0FBa0ViLFNBQVNsQixJQUFULENBQWMrQyxhQUFoRjtBQUNBNUMsYUFBUW1CLE1BQVIsQ0FBZVEsSUFBZixDQUFvQiwrQ0FBcEIsRUFDRUMsSUFERixDQUNPYixTQUFTbEIsSUFBVCxDQUFjZ0QscUJBRHJCO0FBRUE7QUFDRDdDLFlBQVFtQixNQUFSLENBQWVDLEtBQWYsQ0FBcUIsTUFBckI7QUFDQSxJQWRLO0FBZU5DLFVBQU8saUJBQVc7QUFDakIxQixRQUFJbUQsSUFBSixDQUFTMUIsS0FBVCxDQUFlMkIsV0FBZixDQUNDcEQsSUFBSWtDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGFBQXhCLEVBQXVDLGlCQUF2QyxDQURELEVBRUNwQyxJQUFJa0MsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsbUJBQXhCLEVBQTZDLGlCQUE3QyxDQUZEO0FBSUE7QUFwQkssR0FBUDs7QUF1QkE7QUFDQSxNQUFNbUIsaUJBQWlCbEQsUUFBUW1CLE1BQVIsQ0FBZVEsSUFBZixDQUFvQixnQkFBcEIsQ0FBdkI7QUFDQXVCLGlCQUNFQyxHQURGLENBQ00sT0FETixFQUVFQyxFQUZGLENBRUssT0FGTCxFQUVjO0FBQUEsVUFBTTlDLGFBQWFDLFFBQWIsRUFBdUJDLE9BQXZCLEVBQWdDQyxTQUFoQyxDQUFOO0FBQUEsR0FGZDtBQUdBOztBQUVEOzs7OztBQUtBLFVBQVM0QyxrQkFBVCxDQUE0QmQsS0FBNUIsRUFBbUM7QUFDbEM7QUFDQSxNQUFNaEMsV0FBV1IsRUFBRSxXQUFGLEVBQWVrQyxHQUFmLEVBQWpCO0FBQ0EsTUFBTXpCLFVBQVVULEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsVUFBYixDQUFoQjtBQUNBLE1BQUl5RCxZQUFZLENBQWhCO0FBQ0EsTUFBSXZELEVBQUUsSUFBRixFQUFRd0QsRUFBUixDQUFXLFVBQVgsQ0FBSixFQUE0QjtBQUMzQkQsZUFBWSxDQUFaO0FBQ0E7O0FBRUQ7QUFDQXZELElBQUVXLElBQUYsQ0FBTztBQUNOQyxTQUFNLE1BREE7QUFFTkMsUUFBS1AsS0FBS21ELFlBRko7QUFHTjNELFNBQU07QUFDTCxnQkFBWVUsUUFEUDtBQUVMLGVBQVdDLE9BRk47QUFHTCxpQkFBYThDO0FBSFIsSUFIQTtBQVFOeEMsWUFBUyxpQkFBU0MsUUFBVCxFQUFtQjtBQUMzQkEsZUFBV0MsS0FBS0MsS0FBTCxDQUFXRixRQUFYLENBQVg7O0FBRUEsUUFBSUEsU0FBU0QsT0FBVCxLQUFxQixJQUF6QixFQUErQjtBQUM5QmQsYUFBUTBCLElBQVIsQ0FBYU4sS0FBYixDQUFtQixNQUFuQjtBQUNBO0FBQ0QsSUFkSztBQWVOQyxVQUFPLGlCQUFXO0FBQ2pCMUIsUUFBSW1ELElBQUosQ0FBUzFCLEtBQVQsQ0FBZTJCLFdBQWYsQ0FDQ3BELElBQUlrQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixhQUF4QixFQUF1QyxpQkFBdkMsQ0FERCxFQUVDcEMsSUFBSWtDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLG1CQUF4QixFQUE2QyxpQkFBN0MsQ0FGRDtBQUlBO0FBcEJLLEdBQVA7QUFzQkE7O0FBRUQ7Ozs7Ozs7O0FBUUEsVUFBUzBCLFlBQVQsQ0FBc0JsQixLQUF0QixFQUE2Qm1CLEVBQTdCLEVBQWlDO0FBQ2hDLE1BQUksQ0FBQ0EsR0FBR0MsSUFBSCxDQUFRQyxNQUFSLEdBQWlCTCxFQUFqQixDQUFvQixJQUFwQixDQUFMLEVBQWdDO0FBQy9CdEQsaUJBQWM0RCxRQUFkLENBQXVCLFFBQXZCO0FBQ0E7O0FBRUQ5RCxJQUFFVyxJQUFGLENBQU87QUFDTkUsUUFBS1AsS0FBS3lELFdBREo7QUFFTkMsV0FBUSxNQUZGO0FBR05sRSxTQUFNO0FBQ0wsZ0JBQVlFLEVBQUUsV0FBRixFQUFla0MsR0FBZixFQURQO0FBRUwsZUFBV2hDLGNBQWM0RCxRQUFkLENBQXVCLFNBQXZCLEVBQWtDLEVBQUNHLFdBQVcsZUFBWixFQUFsQztBQUZOLElBSEE7QUFPTmxELFlBQVMsaUJBQVNDLFFBQVQsRUFBbUI7QUFDM0JBLGVBQVdDLEtBQUtDLEtBQUwsQ0FBV0YsUUFBWCxDQUFYOztBQUVBLFFBQUlBLFNBQVNELE9BQVQsS0FBcUIsS0FBekIsRUFBZ0M7QUFDL0JuQixTQUFJbUQsSUFBSixDQUFTMUIsS0FBVCxDQUFlMkIsV0FBZixDQUNDcEQsSUFBSWtDLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLGFBQXhCLEVBQXVDLGlCQUF2QyxDQURELEVBRUNwQyxJQUFJa0MsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0Isc0JBQXhCLEVBQWdELGlCQUFoRCxDQUZEO0FBSUE7QUFDRCxJQWhCSztBQWlCTlYsVUFBTyxpQkFBVztBQUNqQjFCLFFBQUltRCxJQUFKLENBQVMxQixLQUFULENBQWUyQixXQUFmLENBQ0NwRCxJQUFJa0MsSUFBSixDQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsYUFBeEIsRUFBdUMsaUJBQXZDLENBREQsRUFFQ3BDLElBQUlrQyxJQUFKLENBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixzQkFBeEIsRUFBZ0QsaUJBQWhELENBRkQ7QUFJQTtBQXRCSyxHQUFQO0FBd0JBOztBQUVEOzs7OztBQUtBLFVBQVNrQyxjQUFULEdBQTBCO0FBQ3pCO0FBQ0EsTUFBTTFELFdBQVdSLEVBQUUsV0FBRixFQUFla0MsR0FBZixFQUFqQjtBQUNBLE1BQU16QixVQUFVUixRQUFRMEIsSUFBUixDQUFhQyxJQUFiLENBQWtCLHdCQUFsQixFQUE0Q00sR0FBNUMsRUFBaEI7QUFDQSxNQUFNeEIsWUFBWVgsTUFBTTZCLElBQU4sQ0FBVyx1QkFBdUJuQixPQUF2QixHQUFpQyxJQUE1QyxDQUFsQjs7QUFFQSxNQUFNMEQsWUFBWWxFLFFBQVEwQixJQUFSLENBQWFDLElBQWIsQ0FBa0Isb0JBQWxCLEVBQXdDTSxHQUF4QyxFQUFsQjtBQUNBLE1BQU1rQyxhQUFhbkUsUUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixxQkFBbEIsRUFBeUNNLEdBQXpDLEVBQW5CO0FBQ0EsTUFBTW1DLGVBQWVwRSxRQUFRMEIsSUFBUixDQUFhQyxJQUFiLENBQWtCLHVCQUFsQixFQUEyQ00sR0FBM0MsRUFBckI7O0FBRUE7QUFDQWxDLElBQUVXLElBQUYsQ0FBTztBQUNOQyxTQUFNLE1BREE7QUFFTkMsUUFBS1AsS0FBS2dFLGFBRko7QUFHTnhFLFNBQU07QUFDTCxnQkFBWVUsUUFEUDtBQUVMLGVBQVdDLE9BRk47QUFHTCxpQkFBYTBELFNBSFI7QUFJTCxrQkFBY0MsVUFKVDtBQUtMLG9CQUFnQkM7QUFMWCxJQUhBO0FBVU50RCxZQUFTLGlCQUFTQyxRQUFULEVBQW1CO0FBQzNCQSxlQUFXQyxLQUFLQyxLQUFMLENBQVdGLFFBQVgsQ0FBWDs7QUFFQSxRQUFJQSxTQUFTRCxPQUFULEtBQXFCLElBQXpCLEVBQStCO0FBQzlCLFNBQUlOLFlBQVksRUFBaEIsRUFBb0I7QUFDbkJWLFlBQU02QixJQUFOLENBQVcsY0FBWCxFQUEyQjJDLE1BQTNCLENBQWtDLCtEQUNXdkQsU0FBU1AsT0FEcEIsZ0VBRU0wRCxVQUFVSyxPQUFWLENBQWtCLElBQWxCLEVBQXdCLE1BQXhCLEVBQ3JDQSxPQURxQyxDQUM3QixJQUQ2QixFQUN2QixNQUR1QixDQUZOLCtTQVNKeEQsU0FBU1AsT0FUTCw0Q0FVaEIwRCxTQVZnQiwyVEFpQm9CbkQsU0FBU1AsT0FqQjdCLG9LQW9Cc0JPLFNBQVNQLE9BcEIvQix3VEFBbEM7O0FBK0JBVixZQUFNNkIsSUFBTixDQUFXLDhDQUFYLEVBQ0V5QixFQURGLENBQ0ssT0FETCxFQUNjWCxjQURkO0FBRUEzQyxZQUFNNkIsSUFBTixDQUFXLGdEQUFYLEVBQ0V5QixFQURGLENBQ0ssT0FETCxFQUNjSixnQkFEZDtBQUVBbEQsWUFBTTZCLElBQU4sQ0FBVyxvREFBWCxFQUNFeUIsRUFERixDQUNLLFFBREwsRUFDZUMsa0JBRGY7O0FBR0FtQixTQUFHQyxPQUFILENBQVdDLElBQVgsQ0FBZ0I1RSxLQUFoQjtBQUNBLE1BeENELE1Bd0NPO0FBQ05XLGdCQUFVa0IsSUFBVixDQUFlLGFBQWYsRUFBOEJDLElBQTlCLENBQW1Dc0MsVUFBVUssT0FBVixDQUFrQixJQUFsQixFQUF3QixNQUF4QixFQUFnQ0EsT0FBaEMsQ0FBd0MsSUFBeEMsRUFBOEMsTUFBOUMsQ0FBbkM7QUFDQTs7QUFFRHZFLGFBQVEwQixJQUFSLENBQWFOLEtBQWIsQ0FBbUIsTUFBbkI7QUFDQTtBQUNEO0FBNURLLEdBQVA7QUE4REE7O0FBRUQ7Ozs7O0FBS0EsVUFBU3VELHFCQUFULEdBQWlDO0FBQ2hDM0UsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixnQ0FBbEIsRUFDRUMsSUFERixDQUNPN0IsRUFBRSxJQUFGLEVBQVE0QixJQUFSLENBQWEsaUJBQWIsRUFBZ0MsQ0FBaEMsRUFBbUMsT0FBbkMsQ0FEUCxFQUVFaUQsV0FGRixDQUVjLFFBRmQ7O0FBSUEsTUFBSTdFLEVBQUUsSUFBRixFQUFRa0MsR0FBUixPQUFrQixrQkFBdEIsRUFBMEM7QUFDekNqQyxXQUFRMEIsSUFBUixDQUFhQyxJQUFiLENBQWtCLHNCQUFsQixFQUEwQ2lELFdBQTFDLENBQXNELFFBQXREO0FBQ0EsR0FGRCxNQUVPO0FBQ041RSxXQUFRMEIsSUFBUixDQUFhQyxJQUFiLENBQWtCLHNCQUFsQixFQUEwQ08sUUFBMUMsQ0FBbUQsUUFBbkQ7QUFDQTtBQUNEOztBQUVEOzs7OztBQUtBLFVBQVMyQyxpQkFBVCxDQUEyQnRDLEtBQTNCLEVBQWtDO0FBQ2pDO0FBQ0FBLFFBQU1DLGNBQU47O0FBRUE7QUFDQSxNQUFNc0MsZUFBZTlFLFFBQVEwQixJQUFSLENBQWFDLElBQWIsQ0FBa0IscUJBQWxCLEVBQXlDTSxHQUF6QyxFQUFyQjtBQUNBLE1BQU04QyxXQUFXL0UsUUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQix1QkFBbEIsRUFBMkNNLEdBQTNDLEVBQWpCOztBQUVBLE1BQUk4QyxhQUFhLGtCQUFqQixFQUFxQztBQUNwQztBQUNBLE9BQU1DLGNBQWNoRixRQUFRMEIsSUFBUixDQUFhQyxJQUFiLENBQWtCLHlCQUFsQixFQUE2Q00sR0FBN0MsRUFBcEI7QUFDQSxPQUFJZ0QsVUFBVSxFQUFkO0FBQ0FqRixXQUFRMEIsSUFBUixDQUFhQyxJQUFiLENBQWtCLHlCQUFsQixFQUE2Q3VELFFBQTdDLENBQXNELFVBQXRELEVBQWtFQyxJQUFsRSxDQUF1RSxZQUFXO0FBQ2pGLFFBQUlwRixFQUFFLElBQUYsRUFBUW1GLFFBQVIsQ0FBaUIsaUJBQWpCLEVBQW9DRSxNQUFwQyxHQUE2QyxDQUFqRCxFQUFvRDtBQUNuREgsYUFBUUksSUFBUixDQUFhdEYsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxRQUFiLENBQWI7QUFDQTtBQUNELElBSkQ7O0FBTUE7QUFDQUcsV0FBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixxQkFBbEIsRUFDRU0sR0FERixDQUNNNkMsZUFBZSxxQkFBZixHQUF1Q0UsWUFBWU0sSUFBWixDQUFpQixHQUFqQixDQUF2QyxHQUErRCxJQUEvRCxHQUFzRUwsUUFBUUssSUFBUixDQUFhLEdBQWIsQ0FBdEUsR0FBMEYsR0FEaEc7QUFFQSxHQWJELE1BYU87QUFDTjtBQUNBdEYsV0FBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixxQkFBbEIsRUFBeUNNLEdBQXpDLENBQTZDNkMsZUFBZSxHQUFmLEdBQXFCQyxRQUFyQixHQUFnQyxHQUE3RTtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBckYsUUFBT2dGLElBQVAsR0FBYyxVQUFTYSxJQUFULEVBQWU7QUFDNUJ4RixJQUFFLGdCQUFGLEVBQW9CcUQsRUFBcEIsQ0FBdUIsT0FBdkIsRUFBZ0NkLGdCQUFoQztBQUNBdkMsSUFBRSxjQUFGLEVBQWtCcUQsRUFBbEIsQ0FBcUIsT0FBckIsRUFBOEJYLGNBQTlCO0FBQ0ExQyxJQUFFLGdCQUFGLEVBQW9CcUQsRUFBcEIsQ0FBdUIsT0FBdkIsRUFBZ0NKLGdCQUFoQztBQUNBakQsSUFBRSxvQkFBRixFQUF3QnFELEVBQXhCLENBQTJCLFFBQTNCLEVBQXFDQyxrQkFBckM7O0FBRUFyRCxVQUFRMEIsSUFBUixDQUFhQyxJQUFiLENBQWtCLGdCQUFsQixFQUFvQ3lCLEVBQXBDLENBQXVDLE9BQXZDLEVBQWdEYSxjQUFoRDtBQUNBakUsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQix1QkFBbEIsRUFBMkN5QixFQUEzQyxDQUE4QyxRQUE5QyxFQUF3RHVCLHFCQUF4RDtBQUNBM0UsVUFBUTBCLElBQVIsQ0FBYUMsSUFBYixDQUFrQixzQkFBbEIsRUFBMEN5QixFQUExQyxDQUE2QyxPQUE3QyxFQUFzRHlCLGlCQUF0RDs7QUFFQTVFLGdCQUNFNEQsUUFERixDQUNXO0FBQ1QyQixVQUFPLGlCQURFO0FBRVRDLFNBQU0sR0FGRztBQUdUQyxXQUFRLE1BSEM7QUFJVEMsV0FBUSxjQUpDO0FBS1RDLGdCQUFhLFVBTEo7QUFNVEMsZ0JBQWE7QUFOSixHQURYLEVBU0V6QyxFQVRGLENBU0ssWUFUTCxFQVNtQkssWUFUbkIsRUFVRXFDLGdCQVZGOztBQVlBUDtBQUNBLEVBdkJEOztBQXlCQSxRQUFPN0YsTUFBUDtBQUNBLENBamVGIiwiZmlsZSI6IkFkbWluL0phdmFzY3JpcHQvY29udHJvbGxlcnMvc2NoZW1lX2ZpZWxkcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gc2NoZW1lX2ZpZWxkcy5qcyAyMDE3LTExLTIwXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuZ3htb2R1bGVzLmNvbnRyb2xsZXJzLm1vZHVsZShcblx0J3NjaGVtZV9maWVsZHMnLFxuXHRcblx0W1xuXHRcdCdsb2FkaW5nX3NwaW5uZXInLFxuXHRcdCdtb2RhbCcsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS11aS9qcXVlcnktdWkubWluLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2pxdWVyeS11aS9qcXVlcnktdWkuanNgXG5cdF0sXG5cdFxuXHRmdW5jdGlvbihkYXRhKSB7XG5cdFx0J3VzZSBzdHJpY3QnO1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIFZBUklBQkxFUyBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWxldGluZyBtb2RhbCBvYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtqUXVlcnl9XG5cdFx0ICovXG5cdFx0Y29uc3QgJG1vZGFscyA9IHtcblx0XHRcdCdlZGl0JzogJCgnLnNjaGVtZS1maWVsZC5tb2RhbCcpLFxuXHRcdFx0J2RlbGV0ZSc6ICQoJy5kZWxldGUtZmllbGQubW9kYWwnKVxuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU29ydGFibGUgbGlzdFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cblx0XHQgKi9cblx0XHRjb25zdCAkc29ydGFibGVMaXN0ID0gJCgnLmZpZWxkcy1saXN0Jyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGRlZmF1bHRzID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmluYWwgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBVUkxzIGZvciBkZWxldGluZyBkaWZmZXJlbnQgdHlwZXMgb2YgY29udGVudFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge3tkZWxldGVTY2hlbWU6IHN0cmluZywgcnVuRXhwb3J0OiBzdHJpbmd9fVxuXHRcdCAqL1xuXHRcdGNvbnN0IHVybHMgPSB7XG5cdFx0XHQnZ2V0RmllbGREYXRhJzogJ2FkbWluLnBocD9kbz1Hb29nbGVTaG9wcGluZ0FqYXgvZ2V0U2NoZW1lRmllbGREYXRhJyxcblx0XHRcdCdzYXZlRmllbGREYXRhJzogJ2FkbWluLnBocD9kbz1Hb29nbGVTaG9wcGluZ0FqYXgvc3RvcmVTY2hlbWVGaWVsZCcsXG5cdFx0XHQnZGVsZXRlRmllbGQnOiAnYWRtaW4ucGhwP2RvPUdvb2dsZVNob3BwaW5nQWpheC9kZWxldGVTY2hlbWVGaWVsZCcsXG5cdFx0XHQndXBkYXRlU3RhdHVzJzogJ2FkbWluLnBocD9kbz1Hb29nbGVTaG9wcGluZ0FqYXgvc2V0U2NoZW1lRmllbGRTdGF0dXMnLFxuXHRcdFx0J3NhdmVTb3J0aW5nJzogJ2FkbWluLnBocD9kbz1Hb29nbGVTaG9wcGluZ0FqYXgvc2F2ZVNjaGVtZUZpZWxkc1NvcnRpbmcnXG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEhFTFBFUiBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBEZWxldGUgZmllbGRcblx0XHQgKlxuXHRcdCAqIFJ1bnMgdGhlIHBvc3QgY2FsbCB0byB0aGUgR29vZ2xlIFNob3BwaW5nIGFqYXggaGFuZGxlciB0byBkZWxldGUgdGhlIGdpdmVuIHNjaGVtZVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9kZWxldGVGaWVsZChzY2hlbWVJZCwgZmllbGRJZCwgJGZpZWxkUm93KSB7XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR0eXBlOiBcIlBPU1RcIixcblx0XHRcdFx0dXJsOiB1cmxzLmRlbGV0ZUZpZWxkLFxuXHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0J3NjaGVtZUlkJzogc2NoZW1lSWQsXG5cdFx0XHRcdFx0J2ZpZWxkSWQnOiBmaWVsZElkXG5cdFx0XHRcdH0sXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0cmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAocmVzcG9uc2VbJ3N1Y2Nlc3MnXSA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdFx0JGZpZWxkUm93LnJlbW92ZSgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkbW9kYWxzLmRlbGV0ZS5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRlcnJvcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0JG1vZGFscy5kZWxldGUubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZXMgdGhlIGZpZWxkIG1vZGFsXG5cdFx0ICpcblx0XHQgKiBSZXNldHMgdGhlIG1vZGFsIGFuZCBjaGFuZ2VzIHRoZSB0aXRsZSBhbmQgdGhlIGlucHV0IHZhbHVlcyBkZXBlbmRpbmcgb2YgdGhlIGFjdGlvblxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF91cGRhdGVGaWVsZE1vZGFsKGFjdGlvbiwgZmllbGREYXRhID0gdW5kZWZpbmVkKSB7XG5cdFx0XHRpZiAoZmllbGREYXRhID09PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0ZmllbGREYXRhID0ge1xuXHRcdFx0XHRcdCdzY2hlbWVJZCc6ICcnLFxuXHRcdFx0XHRcdCduYW1lJzogJycsXG5cdFx0XHRcdFx0J3ZhbHVlJzogJycsXG5cdFx0XHRcdFx0J2RlZmF1bHQnOiAnJ1xuXHRcdFx0XHR9O1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAoYWN0aW9uID09PSAnY3JlYXRlJykge1xuXHRcdFx0XHQkbW9kYWxzLmVkaXQuZmluZCgnLm1vZGFsLXRpdGxlJylcblx0XHRcdFx0XHQudGV4dChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRklFTERfTU9EQUxfVElUTEVfQ1JFQVRFJywgJ2dvb2dsZV9zaG9wcGluZycpKTtcblx0XHRcdH0gZWxzZSBpZiAoYWN0aW9uID09PSAnZWRpdCcpIHtcblx0XHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJy5tb2RhbC10aXRsZScpXG5cdFx0XHRcdFx0LnRleHQoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0ZJRUxEX01PREFMX1RJVExFX0VESVQnLCAnZ29vZ2xlX3Nob3BwaW5nJykpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQkbW9kYWxzLmVkaXQuZmluZCgnc2VsZWN0LmNvbGxlY3RpdmUtZmllbGQgb3B0aW9uJykucHJvcChcInNlbGVjdGVkXCIsIGZhbHNlKTtcblx0XHRcdCRtb2RhbHMuZWRpdC5maW5kKCdzZWxlY3QuZmllbGQtdmFyaWFibGUnKS52YWwoJycpO1xuXHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ2Rpdi5jb2xsZWN0aXZlLWZpZWxkJykuYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ2lucHV0W25hbWU9XCJzY2hlbWVJZFwiXScpLnZhbChmaWVsZERhdGEuc2NoZW1lSWQpO1xuXHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ2lucHV0W25hbWU9XCJuYW1lXCJdJykudmFsKGZpZWxkRGF0YS5uYW1lKTtcblx0XHRcdCRtb2RhbHMuZWRpdC5maW5kKCdpbnB1dFtuYW1lPVwidmFsdWVcIl0nKS52YWwoZmllbGREYXRhLnZhbHVlKTtcblx0XHRcdCRtb2RhbHMuZWRpdC5maW5kKCdpbnB1dFtuYW1lPVwiZGVmYXVsdFwiXScpLnZhbChmaWVsZERhdGEuZGVmYXVsdCk7XG5cdFx0XHQkbW9kYWxzLmVkaXQuZmluZCgnZGl2LmZpZWxkLXZhcmlhYmxlLWRlc2NyaXB0aW9uJylcblx0XHRcdFx0LnRleHQoJycpXG5cdFx0XHRcdC5hZGRDbGFzcygnaGlkZGVuJyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIEVWRU5UIEhBTkRMRVJTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciBmb3IgdGhlIGNyZWF0ZSBmaWVsZCBidXR0b25cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBqUXVlcnkgZXZlbnQgb2JqZWN0IGNvbnRhaW5zIGluZm9ybWF0aW9uIG9mIHRoZSBldmVudC5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2hvd0NyZWF0ZU1vZGFsKGV2ZW50KSB7XG5cdFx0XHQvLyBQcmV2ZW50IGRlZmF1bHQgYWN0aW9uLlxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFxuXHRcdFx0Ly8gU2hvdyBmaWVsZCBjcmVhdGlvbiBtb2RhbFxuXHRcdFx0X3VwZGF0ZUZpZWxkTW9kYWwoJ2NyZWF0ZScpO1xuXHRcdFx0JG1vZGFscy5lZGl0Lm1vZGFsKCdzaG93Jyk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENsaWNrIGhhbmRsZXIgZm9yIHRoZSBlZGl0IGZpZWxkIGljb25zXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgalF1ZXJ5IGV2ZW50IG9iamVjdCBjb250YWlucyBpbmZvcm1hdGlvbiBvZiB0aGUgZXZlbnQuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3Nob3dFZGl0TW9kYWwoZXZlbnQpIHtcblx0XHRcdC8vIFByZXZlbnQgZGVmYXVsdCBhY3Rpb24uXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHQvLyBDb2xsZWN0IGZpZWxkIGlkXG5cdFx0XHRjb25zdCBmaWVsZElkID0gJCh0aGlzKS5kYXRhKCdmaWVsZC1pZCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBGZXRjaCBkYXRhIHdpdGggYWpheCBjb250cm9sbGVyIC4uLlxuXHRcdFx0JC5hamF4KHtcblx0XHRcdFx0dHlwZTogXCJHRVRcIixcblx0XHRcdFx0dXJsOiB1cmxzLmdldEZpZWxkRGF0YSArICcmZmllbGRJZD0nICsgZmllbGRJZCxcblx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRyZXNwb25zZSA9IEpTT04ucGFyc2UocmVzcG9uc2UpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vIERpc3BsYXkgbW9kYWwgd2l0aCBmaWVsZCBkYXRhIG9uIHN1Y2Nlc3Ncblx0XHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2VzcyA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdFx0X3VwZGF0ZUZpZWxkTW9kYWwoJ2VkaXQnLCB7XG5cdFx0XHRcdFx0XHRcdCdzY2hlbWVJZCc6IGZpZWxkSWQsXG5cdFx0XHRcdFx0XHRcdCduYW1lJzogcmVzcG9uc2UuZGF0YS5maWVsZF9uYW1lLFxuXHRcdFx0XHRcdFx0XHQndmFsdWUnOiByZXNwb25zZS5kYXRhLmZpZWxkX2NvbnRlbnQsXG5cdFx0XHRcdFx0XHRcdCdkZWZhdWx0JzogcmVzcG9uc2UuZGF0YS5maWVsZF9jb250ZW50X2RlZmF1bHRcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0JG1vZGFscy5lZGl0Lm1vZGFsKCdzaG93Jyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9LFxuXHRcdFx0XHRlcnJvcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfVElUTEUnLCAnZ29vZ2xlX3Nob3BwaW5nJyksXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfQUpBWF9GQUlMRUQnLCAnZ29vZ2xlX3Nob3BwaW5nJylcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciBmb3IgdGhlIGRlbGV0ZSBmaWVsZCBpY29uc1xuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9zaG93RGVsZXRlTW9kYWwoZXZlbnQpIHtcblx0XHRcdC8vIFByZXZlbnQgZGVmYXVsdCBhY3Rpb24uXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHQvLyBDb2xsZWN0IHNldmVyYWwgZGF0YVxuXHRcdFx0Y29uc3Qgc2NoZW1lSWQgPSAkKCcuc2NoZW1lSWQnKS52YWwoKTtcblx0XHRcdGNvbnN0IGZpZWxkSWQgPSAkKHRoaXMpLmRhdGEoJ2ZpZWxkLWlkJyk7XG5cdFx0XHRjb25zdCAkZmllbGRSb3cgPSAkKHRoaXMpLmNsb3Nlc3QoJ2xpJyk7XG5cdFx0XHRcblx0XHRcdC8vIFNob3cgbW9kYWxcblx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdHR5cGU6IFwiR0VUXCIsXG5cdFx0XHRcdHVybDogdXJscy5nZXRGaWVsZERhdGEgKyAnJmZpZWxkSWQ9JyArIGZpZWxkSWQsXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0cmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQvLyBEaXNwbGF5IG1vZGFsIHdpdGggZmllbGQgZGF0YSBvbiBzdWNjZXNzXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnN1Y2Nlc3MgPT09IHRydWUpIHtcblx0XHRcdFx0XHRcdCRtb2RhbHMuZGVsZXRlLmZpbmQoJ2ZpZWxkc2V0LmZpZWxkLWRhdGEgZGl2LmZpZWxkLW5hbWUnKS50ZXh0KHJlc3BvbnNlLmRhdGEuZmllbGRfbmFtZSk7XG5cdFx0XHRcdFx0XHQkbW9kYWxzLmRlbGV0ZS5maW5kKCdmaWVsZHNldC5maWVsZC1kYXRhIGRpdi5maWVsZC1jb250ZW50JykudGV4dChyZXNwb25zZS5kYXRhLmZpZWxkX2NvbnRlbnQpO1xuXHRcdFx0XHRcdFx0JG1vZGFscy5kZWxldGUuZmluZCgnZmllbGRzZXQuZmllbGQtZGF0YSBkaXYuZmllbGQtY29udGVudC1kZWZhdWx0Jylcblx0XHRcdFx0XHRcdFx0LnRleHQocmVzcG9uc2UuZGF0YS5maWVsZF9jb250ZW50X2RlZmF1bHQpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQkbW9kYWxzLmRlbGV0ZS5tb2RhbCgnc2hvdycpO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRlcnJvcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfVElUTEUnLCAnZ29vZ2xlX3Nob3BwaW5nJyksXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfQUpBWF9GQUlMRUQnLCAnZ29vZ2xlX3Nob3BwaW5nJylcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Ly8gSGFuZGxlIGRlbGV0ZSBjb25maXJtYXRpb24gbW9kYWwgYnV0dG9uIGNsaWNrIGV2ZW50XG5cdFx0XHRjb25zdCAkY29uZmlybUJ1dHRvbiA9ICRtb2RhbHMuZGVsZXRlLmZpbmQoJ2J1dHRvbi5jb25maXJtJyk7XG5cdFx0XHQkY29uZmlybUJ1dHRvblxuXHRcdFx0XHQub2ZmKCdjbGljaycpXG5cdFx0XHRcdC5vbignY2xpY2snLCAoKSA9PiBfZGVsZXRlRmllbGQoc2NoZW1lSWQsIGZpZWxkSWQsICRmaWVsZFJvdykpO1xuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDbGljayBoYW5kbGVyIGZvciB0aGUgc2NoZW1lIGZpZWxkIHN0YXR1cyBzd2l0Y2hlclxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF91cGRhdGVGaWVsZFN0YXR1cyhldmVudCkge1xuXHRcdFx0Ly8gQ29sbGVjdCBzZXZlcmFsIGRhdGFcblx0XHRcdGNvbnN0IHNjaGVtZUlkID0gJCgnLnNjaGVtZUlkJykudmFsKCk7XG5cdFx0XHRjb25zdCBmaWVsZElkID0gJCh0aGlzKS5kYXRhKCdmaWVsZC1pZCcpO1xuXHRcdFx0bGV0IG5ld1N0YXR1cyA9IDA7XG5cdFx0XHRpZiAoJCh0aGlzKS5pcygnOmNoZWNrZWQnKSkge1xuXHRcdFx0XHRuZXdTdGF0dXMgPSAxO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHQvLyBDYWxsIGFqYXggY29udHJvbGxlciB0byB1cGRhdGUgZmllbGQgc3RhdHVzXG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR0eXBlOiBcIlBPU1RcIixcblx0XHRcdFx0dXJsOiB1cmxzLnVwZGF0ZVN0YXR1cyxcblx0XHRcdFx0ZGF0YToge1xuXHRcdFx0XHRcdCdzY2hlbWVJZCc6IHNjaGVtZUlkLFxuXHRcdFx0XHRcdCdmaWVsZElkJzogZmllbGRJZCxcblx0XHRcdFx0XHQnbmV3U3RhdHVzJzogbmV3U3RhdHVzXG5cdFx0XHRcdH0sXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0cmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2VzcyA9PT0gdHJ1ZSkge1xuXHRcdFx0XHRcdFx0JG1vZGFscy5lZGl0Lm1vZGFsKCdoaWRlJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9LFxuXHRcdFx0XHRlcnJvcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfVElUTEUnLCAnZ29vZ2xlX3Nob3BwaW5nJyksXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfQUpBWF9GQUlMRUQnLCAnZ29vZ2xlX3Nob3BwaW5nJylcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogU29ydGluZyBldmVudCBoYW5kbGVyIGZvciBzb3J0YWJsZSBwbHVnaW5cblx0XHQgKlxuXHRcdCAqIE1ha2VzIGEgY2FsbCB0byB0aGUgYWpheCBjb250cm9sbGVyIGFmdGVyIGEgc29ydGluZyBldmVudFxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSB1aSAgICBTb3J0YWJsZSBsaXN0ICh1bCkgb2JqZWN0IHdpdGggbmV3IHNvcnQgb3JkZXJcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfc2F2ZVNvcnRpbmcoZXZlbnQsIHVpKSB7XG5cdFx0XHRpZiAoIXVpLml0ZW0ucGFyZW50KCkuaXMoJ3VsJykpIHtcblx0XHRcdFx0JHNvcnRhYmxlTGlzdC5zb3J0YWJsZSgnY2FuY2VsJyk7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdHVybDogdXJscy5zYXZlU29ydGluZyxcblx0XHRcdFx0bWV0aG9kOiAnUE9TVCcsXG5cdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHQnc2NoZW1lSWQnOiAkKCcuc2NoZW1lSWQnKS52YWwoKSxcblx0XHRcdFx0XHQnc29ydGluZyc6ICRzb3J0YWJsZUxpc3Quc29ydGFibGUoJ3RvQXJyYXknLCB7YXR0cmlidXRlOiAnZGF0YS1maWVsZC1pZCd9KVxuXHRcdFx0XHR9LFxuXHRcdFx0XHRzdWNjZXNzOiBmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdHJlc3BvbnNlID0gSlNPTi5wYXJzZShyZXNwb25zZSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnN1Y2Nlc3MgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0XHRqc2UubGlicy5tb2RhbC5zaG93TWVzc2FnZShcblx0XHRcdFx0XHRcdFx0anNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ0VSUk9SX1RJVExFJywgJ2dvb2dsZV9zaG9wcGluZycpLFxuXHRcdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfU09SVElOR19GQUlMRUQnLCAnZ29vZ2xlX3Nob3BwaW5nJylcblx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9LFxuXHRcdFx0XHRlcnJvcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UoXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfVElUTEUnLCAnZ29vZ2xlX3Nob3BwaW5nJyksXG5cdFx0XHRcdFx0XHRqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnRVJST1JfU09SVElOR19GQUlMRUQnLCAnZ29vZ2xlX3Nob3BwaW5nJylcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2xpY2sgaGFuZGxlciB0byBzYXZlIHNjaGVtZSBmaWVsZCBkYXRhXG5cdFx0ICpcblx0XHQgKiBDb2xsZWN0cyB0aGUgc2NoZW1lIGZpZWxkIGRhdGEgZnJvbSB0aGUgZWRpdC9jcmVhdGUgbW9kYWwgYW5kIG1ha2VzIGFuIGFqYXggY2FsbCB0byBzYXZlIHRoZW0gaW50byB0aGUgZGIuXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX3NhdmVGaWVsZERhdGEoKSB7XG5cdFx0XHQvLyBDb2xsZWN0IHNldmVyYWwgZGF0YVxuXHRcdFx0Y29uc3Qgc2NoZW1lSWQgPSAkKCcuc2NoZW1lSWQnKS52YWwoKTtcblx0XHRcdGNvbnN0IGZpZWxkSWQgPSAkbW9kYWxzLmVkaXQuZmluZCgnaW5wdXRbbmFtZT1cInNjaGVtZUlkXCJdJykudmFsKCk7XG5cdFx0XHRjb25zdCAkZmllbGRSb3cgPSAkdGhpcy5maW5kKCdsaVtkYXRhLWZpZWxkLWlkPVwiJyArIGZpZWxkSWQgKyAnXCJdJyk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IGZpZWxkTmFtZSA9ICRtb2RhbHMuZWRpdC5maW5kKCdpbnB1dFtuYW1lPVwibmFtZVwiXScpLnZhbCgpO1xuXHRcdFx0Y29uc3QgZmllbGRWYWx1ZSA9ICRtb2RhbHMuZWRpdC5maW5kKCdpbnB1dFtuYW1lPVwidmFsdWVcIl0nKS52YWwoKTtcblx0XHRcdGNvbnN0IGZpZWxkRGVmYXVsdCA9ICRtb2RhbHMuZWRpdC5maW5kKCdpbnB1dFtuYW1lPVwiZGVmYXVsdFwiXScpLnZhbCgpO1xuXHRcdFx0XG5cdFx0XHQvLyBNYWtlIGFqYXggY2FsbCB0byBhamF4IGNvbnRyb2xsZXIgdG8gc2F2ZSBmaWVsZCBkYXRhXG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR0eXBlOiBcIlBPU1RcIixcblx0XHRcdFx0dXJsOiB1cmxzLnNhdmVGaWVsZERhdGEsXG5cdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHQnc2NoZW1lSWQnOiBzY2hlbWVJZCxcblx0XHRcdFx0XHQnZmllbGRJZCc6IGZpZWxkSWQsXG5cdFx0XHRcdFx0J2ZpZWxkTmFtZSc6IGZpZWxkTmFtZSxcblx0XHRcdFx0XHQnZmllbGRWYWx1ZSc6IGZpZWxkVmFsdWUsXG5cdFx0XHRcdFx0J2ZpZWxkRGVmYXVsdCc6IGZpZWxkRGVmYXVsdFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRzdWNjZXNzOiBmdW5jdGlvbihyZXNwb25zZSkge1xuXHRcdFx0XHRcdHJlc3BvbnNlID0gSlNPTi5wYXJzZShyZXNwb25zZSk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnN1Y2Nlc3MgPT09IHRydWUpIHtcblx0XHRcdFx0XHRcdGlmIChmaWVsZElkID09PSAnJykge1xuXHRcdFx0XHRcdFx0XHQkdGhpcy5maW5kKCcuZmllbGRzLWxpc3QnKS5hcHBlbmQoYFxuXHRcdFx0XHRcdFx0XHRcdDxsaSBjbGFzcz1cInNjaGVtZS1maWVsZFwiIGRhdGEtZmllbGQtaWQ9XCJgICsgcmVzcG9uc2UuZmllbGRJZCArIGBcIj5cblx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwiY29sLW1kLTUgZmllbGQtbmFtZVwiPmAgKyBmaWVsZE5hbWUucmVwbGFjZSgvPC9nLCAnJmx0OycpXG5cdFx0XHRcdFx0XHRcdFx0XHQucmVwbGFjZSgvPi9nLCAnJmd0OycpICsgYDwvc3Bhbj5cblx0XHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJjb2wtbWQtMiBzdGF0dXNcIj5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJneC1jb250YWluZXJcIiBkYXRhLWd4LXdpZGdldD1cInN3aXRjaGVyXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGlucHV0IHR5cGU9XCJjaGVja2JveFwiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0ICAgICAgIGNsYXNzPVwiZmllbGQtc3RhdHVzXCJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgICAgICAgZGF0YS1maWVsZC1pZD1cImAgKyByZXNwb25zZS5maWVsZElkICsgYFwiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgICBuYW1lPVwiYCArIGZpZWxkTmFtZSArIGBfc3RhdHVzXCJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdCAgIHZhbHVlPVwiMVwiIGNoZWNrZWQvPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3NwYW4+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8L3NwYW4+XG5cdFx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwiY29sLW1kLTUgYWN0aW9uc1wiPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cImFjdGlvbnMtY29udGFpbmVyXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJlZGl0LWZpZWxkXCIgaHJlZj1cIiNcIiBkYXRhLWZpZWxkLWlkPVwiYCArIHJlc3BvbnNlLmZpZWxkSWQgKyBgXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8aSBjbGFzcz1cImZhIGZhLXBlbmNpbFwiPjwvaT5cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8L2E+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJkZWxldGUtZmllbGRcIiBocmVmPVwiI1wiIGRhdGEtZmllbGQtaWQ9XCJgICsgcmVzcG9uc2UuZmllbGRJZCArIGBcIj5cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtdHJhc2gtb1wiPjwvaT5cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8L2E+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJzb3J0LWhhbmRsZVwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGkgY2xhc3M9XCJmYSBmYS1zb3J0XCI+PC9pPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvYT5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PC9zcGFuPlxuXHRcdFx0XHRcdFx0XHRcdFx0PC9zcGFuPlxuXHRcdFx0XHRcdFx0XHRcdDwvbGk+XG5cdFx0XHRcdFx0XHRcdGApO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnLmZpZWxkcy1saXN0IC5zY2hlbWUtZmllbGQ6bGFzdCBhLmVkaXQtZmllbGQnKVxuXHRcdFx0XHRcdFx0XHRcdC5vbignY2xpY2snLCBfc2hvd0VkaXRNb2RhbCk7XG5cdFx0XHRcdFx0XHRcdCR0aGlzLmZpbmQoJy5maWVsZHMtbGlzdCAuc2NoZW1lLWZpZWxkOmxhc3QgYS5kZWxldGUtZmllbGQnKVxuXHRcdFx0XHRcdFx0XHRcdC5vbignY2xpY2snLCBfc2hvd0RlbGV0ZU1vZGFsKTtcblx0XHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnLmZpZWxkcy1saXN0IC5zY2hlbWUtZmllbGQ6bGFzdCBpbnB1dC5maWVsZC1zdGF0dXMnKVxuXHRcdFx0XHRcdFx0XHRcdC5vbignY2hhbmdlJywgX3VwZGF0ZUZpZWxkU3RhdHVzKTtcblx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdGd4LndpZGdldHMuaW5pdCgkdGhpcyk7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHQkZmllbGRSb3cuZmluZCgnLmZpZWxkLW5hbWUnKS50ZXh0KGZpZWxkTmFtZS5yZXBsYWNlKC88L2csICcmbHQ7JykucmVwbGFjZSgvPi9nLCAnJmd0OycpKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0JG1vZGFscy5lZGl0Lm1vZGFsKCdoaWRlJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogQ2hhbmdlIGhhbmRsZXIgZm9yIGZpZWxkIHZhcmlhYmxlIGluIGZpZWxkIG1vZGFsXG5cdFx0ICpcblx0XHQgKiBEaXNwbGF5cyB0aGUgY29sbGVjdGl2ZSBmaWVsZHMgZHJvcGRvd24gaWYgZmllbGQgdmFyaWFibGUgZHJvcGRvd24gaGFzIHRoZSBjb3JyZWN0IHZhbHVlXG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2ZpZWxkVmFyaWFibGVDaGFuZ2VkKCkge1xuXHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ2Rpdi5maWVsZC12YXJpYWJsZS1kZXNjcmlwdGlvbicpXG5cdFx0XHRcdC50ZXh0KCQodGhpcykuZmluZCgnb3B0aW9uOnNlbGVjdGVkJylbMF1bJ3RpdGxlJ10pXG5cdFx0XHRcdC5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG5cdFx0XHRcblx0XHRcdGlmICgkKHRoaXMpLnZhbCgpID09PSAnY29sbGVjdGl2ZV9maWVsZCcpIHtcblx0XHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ2Rpdi5jb2xsZWN0aXZlLWZpZWxkJykucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ2Rpdi5jb2xsZWN0aXZlLWZpZWxkJykuYWRkQ2xhc3MoJ2hpZGRlbicpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvKipcblx0XHQgKiBDbGljayBoYW5kbGVyIGZvciBhZGQgZmllbGQgdmFyaWFibGUgYnV0dG9uXG5cdFx0ICpcblx0XHQgKiBBZGRzIHRoZSB0ZXh0IGZvciB0aGUgZmllbGQgdmFyaWFibGUgdG8gdGhlIGZpZWxkIHZhbHVlIGlucHV0XG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2FkZEZpZWxkVmFyaWFibGUoZXZlbnQpIHtcblx0XHRcdC8vIFByZXZlbnQgZGVmYXVsdCBhY3Rpb24uXG5cdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XG5cdFx0XHQvLyBDb2xsZWN0IHNvbWUgZGF0YVxuXHRcdFx0Y29uc3QgY3VycmVudFZhbHVlID0gJG1vZGFscy5lZGl0LmZpbmQoJ2lucHV0W25hbWU9XCJ2YWx1ZVwiXScpLnZhbCgpO1xuXHRcdFx0Y29uc3QgdmFyaWFibGUgPSAkbW9kYWxzLmVkaXQuZmluZCgnc2VsZWN0LmZpZWxkLXZhcmlhYmxlJykudmFsKCk7XG5cdFx0XHRcblx0XHRcdGlmICh2YXJpYWJsZSA9PT0gJ2NvbGxlY3RpdmVfZmllbGQnKSB7XG5cdFx0XHRcdC8vIENvbGxlY3Qgc2VsZWN0ZWQgYXR0cmlidXRlcywgcHJvcGVydGllcywgYWRkaXRpb25hbCBmaWVsZHMgYW5kIHRoZWlyIHNvdXJjZXNcblx0XHRcdFx0Y29uc3QgYXR0dHJpYnV0ZXMgPSAkbW9kYWxzLmVkaXQuZmluZCgnc2VsZWN0LmNvbGxlY3RpdmUtZmllbGQnKS52YWwoKTtcblx0XHRcdFx0bGV0IHNvdXJjZXMgPSBbXTtcblx0XHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ3NlbGVjdC5jb2xsZWN0aXZlLWZpZWxkJykuY2hpbGRyZW4oJ29wdGdyb3VwJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRpZiAoJCh0aGlzKS5jaGlsZHJlbignb3B0aW9uOnNlbGVjdGVkJykubGVuZ3RoID4gMCkge1xuXHRcdFx0XHRcdFx0c291cmNlcy5wdXNoKCQodGhpcykuZGF0YSgnc291cmNlJykpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvLyBBZGQgdGV4dCBmb3IgY29sbGVjdGl2ZSBmaWVsZFxuXHRcdFx0XHQkbW9kYWxzLmVkaXQuZmluZCgnaW5wdXRbbmFtZT1cInZhbHVlXCJdJylcblx0XHRcdFx0XHQudmFsKGN1cnJlbnRWYWx1ZSArICd7Y29sbGVjdGl2ZV9maWVsZHx8JyArIGF0dHRyaWJ1dGVzLmpvaW4oJzsnKSArICd8fCcgKyBzb3VyY2VzLmpvaW4oJzsnKSArICd9Jyk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQvLyBBZGQgdGV4dCBmb3Igbm9ybWFsIGZpZWxkIHZhcmFpYmxlXG5cdFx0XHRcdCRtb2RhbHMuZWRpdC5maW5kKCdpbnB1dFtuYW1lPVwidmFsdWVcIl0nKS52YWwoY3VycmVudFZhbHVlICsgJ3snICsgdmFyaWFibGUgKyAnfScpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0JCgnYS5jcmVhdGUtZmllbGQnKS5vbignY2xpY2snLCBfc2hvd0NyZWF0ZU1vZGFsKTtcblx0XHRcdCQoJ2EuZWRpdC1maWVsZCcpLm9uKCdjbGljaycsIF9zaG93RWRpdE1vZGFsKTtcblx0XHRcdCQoJ2EuZGVsZXRlLWZpZWxkJykub24oJ2NsaWNrJywgX3Nob3dEZWxldGVNb2RhbCk7XG5cdFx0XHQkKCdpbnB1dC5maWVsZC1zdGF0dXMnKS5vbignY2hhbmdlJywgX3VwZGF0ZUZpZWxkU3RhdHVzKTtcblx0XHRcdFxuXHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ2J1dHRvbi5jb25maXJtJykub24oJ2NsaWNrJywgX3NhdmVGaWVsZERhdGEpO1xuXHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ3NlbGVjdC5maWVsZC12YXJpYWJsZScpLm9uKCdjaGFuZ2UnLCBfZmllbGRWYXJpYWJsZUNoYW5nZWQpO1xuXHRcdFx0JG1vZGFscy5lZGl0LmZpbmQoJ2EuYWRkLWZpZWxkLXZhcmlhYmxlJykub24oJ2NsaWNrJywgX2FkZEZpZWxkVmFyaWFibGUpO1xuXHRcdFx0XG5cdFx0XHQkc29ydGFibGVMaXN0XG5cdFx0XHRcdC5zb3J0YWJsZSh7XG5cdFx0XHRcdFx0aXRlbXM6ICdsaS5zY2hlbWUtZmllbGQnLFxuXHRcdFx0XHRcdGF4aXM6ICd5Jyxcblx0XHRcdFx0XHRjdXJzb3I6ICdtb3ZlJyxcblx0XHRcdFx0XHRoYW5kbGU6ICcuc29ydC1oYW5kbGUnLFxuXHRcdFx0XHRcdGNvbnRhaW5tZW50OiAnZG9jdW1lbnQnLFxuXHRcdFx0XHRcdHBsYWNlaG9sZGVyOiAnc29ydC1wbGFjZWhvbGRlcidcblx0XHRcdFx0fSlcblx0XHRcdFx0Lm9uKCdzb3J0dXBkYXRlJywgX3NhdmVTb3J0aW5nKVxuXHRcdFx0XHQuZGlzYWJsZVNlbGVjdGlvbigpO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fVxuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7Il19
