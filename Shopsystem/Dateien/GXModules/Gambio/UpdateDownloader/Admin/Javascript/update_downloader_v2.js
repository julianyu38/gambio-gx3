/* ------------------------------------------------------------
 update_downloader.js 2018-08-07
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * AutoUpdater Controller
 */
(function() {
	'use strict';
	
	/**
	 * AutoUpdater main container
	 *
	 * @type {Object}
	 */
	const $this = $('.update-downloader-container');
	
	/**
	 * URLs
	 *
	 * @type {{deleteScheme: string, runExport: string}}
	 */
	const urls = {
		'downloadUpdate': 'admin.php?do=AutoUpdaterAjax/installUpdate',
	};
	
	
	/**
	 * AutoUpdater main container
	 *
	 * @type {Object}
	 */
	const $modal = {
		"downloadUpdates": $('.download-updates.modal'),
		"uninstalledUpdates": $('.uninstalled-updates.modal'),
		"updateDetails": $('.update-details.modal'),
		"info": $('.info.modal')
	};
	
	
	/**
	 * Total number of selected updates
	 *
	 * @type {int}
	 */
	let selectedUpdatesTotal = 0;
	
	
	/**
	 * Total number of currently installed updates
	 *
	 * @type {int}
	 */
	let installedUpdatesTotal = 0;
	
	/**
	 * Makes a redirect to the Gambio Updater.
	 */
	function gotoGambioUpdater() {
		window.location.href = '../gambio_updater/';
	}
	
	/**
	 * Click handler for "download all updates" button
	 *
	 * @param {MessageEvent} event Triggered event.
	 */
	function showUpdateDetails(event) {
		event.preventDefault();
		
		const id = $(this).data('id');
		const $wrapper = $this.find('div.update-wrapper[data-id="' + id + '"]');
		
		const title = $wrapper.find('h4.update-title').text();
		const description = $wrapper.find('div.update-description').html();
		
		$modal['updateDetails'].find('.modal-title').text(title);
		$modal['updateDetails'].find('.modal-body')
			.html(description)
			.prepend($wrapper.find('div.update-icon').clone().addClass('pull-right'));
		$modal['updateDetails'].modal('show');
	}
	
	/**
	 * Handles the module initialization.
	 */
	function updateSelectionChanged() {
		const $button = $('input.download-updates');
		selectedUpdatesTotal = $this.find('input.update-selection:checked').size();
		
		if (selectedUpdatesTotal === 0) {
			$button.val($button.data('textPlural').replace('%s', selectedUpdatesTotal)).prop('disabled', true);
		} else if (selectedUpdatesTotal > 1) {
			$button.val($button.data('textPlural').replace('%s', selectedUpdatesTotal)).prop('disabled', false);
		} else {
			$button.val($button.data('textSingle').replace('%s', selectedUpdatesTotal)).prop('disabled', false);
		}
	}
	
	/**
	 * Handles the module initialization.
	 */
	function changeUpdateSelection() {
		const $wrapper = $(this).closest('.update-wrapper');
		
		if ($wrapper.find('input.update-selection').is(':disabled')) {
			return;
		}
		
		const id = $wrapper.data('id');
		const isChecked = $wrapper.find('input.update-selection').is(':checked');
		
		$wrapper.find('input.update-selection').single_checkbox('checked', !isChecked);
		if (!isChecked) {
			$wrapper.addClass('active');
		} else {
			$wrapper.removeClass('active');
		}
		
		updateSelectionChanged();
	}
	
	/**
	 * Handles download updates button.
	 */
	function downloadUpdates(event) {
		event.preventDefault();
		
		$('input.download-updates').prop('disabled', true);
		
		selectedUpdatesTotal = $this.find('input.update-selection:checked').size();
		installedUpdatesTotal = 0;
		
		_updateProgressBar(0);
		$modal['downloadUpdates'].find('.errors').hide();
		$modal['downloadUpdates'].find('.installed-updates').hide();
		$modal['downloadUpdates'].find('h4.errors span').text('');
		$modal['downloadUpdates'].find('div.errors span').html('');
		$modal['downloadUpdates'].find('div.errors ul').html('');
		$modal['downloadUpdates'].find('ul.installed-updates').html('');
		$modal['downloadUpdates'].find('.modal-footer').hide();
		$modal['downloadUpdates'].modal({
			show: true,
			backdrop: "static",
			keyboard: false
		});
		//$modal['downloadUpdates'].find('button.cancel')
		//	.off('click')
		//	.on('click', (event) => cancelDownload(event));
		
		downloadUpdate();
	}
	
	/**
	 * Makes an ajax request to download a single selected update.
	 */
	function downloadUpdate() {
		const $selectedUpdates = $this.find('input.update-selection:checked');
		
		if ($selectedUpdates.size() === 0 && installedUpdatesTotal === 0) {
			$modal['downloadUpdates'].modal('hide');
			$('input.download-updates').prop('disabled', false);
			
			return;
		} else if ($selectedUpdates.size() === 0 && installedUpdatesTotal > 0) {
			gotoGambioUpdater();
			return;
		}
		
		const receipt = $this.find('input.update-selection:checked').first().val();
		const $currentUpdate = $this.find('div[data-receipt="' + receipt + '"]').first();
		const updateTitle = $currentUpdate.find('h4.update-title').text().trim();
		
		$.ajax({
			type: "POST",
			url: urls.downloadUpdate,
			data: {
				'selectedUpdate': receipt
			},
			success: function(response) {
				try {
					response = JSON.parse(response);
				} catch(e) {
					response = {
						'success': false,
						'error': jse.core.lang.translate('UNEXPECTED_ERROR', 'update_downloader'),
						'list': undefined
					};
				}
				
				if (!response.success) {
					_updateProgressBar(100, true);
					$modal['downloadUpdates'].find('.progress .progress-bar').removeClass('active');
					$modal['downloadUpdates'].find('h4.errors span').text(updateTitle);
					$modal['downloadUpdates'].find('div.errors span').html(response.error);
					$modal['downloadUpdates'].find('div.errors ul').html(response.list);
					$modal['downloadUpdates'].find('.errors').show();
					$modal['downloadUpdates'].find('.modal-footer').show();
					
					if (installedUpdatesTotal > 0) {
						$modal['downloadUpdates'].find('.installed-updates').show();
						$modal['downloadUpdates'].find('.modal-footer .close-button').on('click', (event) => {
							event.preventDefault();
							gotoGambioUpdater();
						});
					}
					
					return;
				}
				installedUpdatesTotal++;
				_updateProgressBar(Math.ceil((installedUpdatesTotal / selectedUpdatesTotal) * 100));
				$modal['downloadUpdates'].find('ul.installed-updates').append('<li>' + updateTitle + '</li>');
				$currentUpdate
					.find('input.update-selection')
					.single_checkbox('checked', false)
					.trigger('change');
				
				downloadUpdate();
			}
		});
	}
	
	/**
	 * Updates the progress bar
	 *
	 * @param {number} progress  Current progress in percent.
	 * @param {boolean} canceled  True, if the job was canceled.
	 */
	function _updateProgressBar(progress, canceled = false) {
		const $progressBar = $modal['downloadUpdates'].find('.progress .progress-bar');
		
		if (!Number.isInteger(progress) || progress < 0) {
			progress = 0;
		} else if (progress > 100) {
			progress = 100;
		}
		
		$progressBar.prop('aria-valuenow', progress);
		$progressBar.css('width', progress + '%');
		$progressBar.text(progress + '%');
		
		if (canceled) {
			$progressBar.removeClass('active');
			$progressBar.addClass('progress-bar-danger');
			return;
		}
		
		$progressBar.removeClass('progress-bar-danger');
		$progressBar.addClass('active');
		if (progress === 100) {
			$progressBar.removeClass('active');
		}
	}
	
	/**
	 * Handles the module initialization.
	 */
	function onInit() {
		selectedUpdatesTotal = $this.find('input.update-selection:checked').size();
		
		$this.find('a.update-details').on('click', showUpdateDetails);
		$this.find('input.update-selection').on('change', updateSelectionChanged);
		$this.find('h4.update-title').on('click', changeUpdateSelection);
		$this.find('div.update-icon').on('click', changeUpdateSelection);
		$this.find('div.update-description-short').on('click', changeUpdateSelection);
		$('input.download-updates[type="submit"]').on('click', downloadUpdates);
		$this.find('.tooltip-icon').on('click', () => {
			$modal['info'].modal('show')
		});
		
		if ($this.data('showInfo') === true) {
			$modal['info'].modal({
				show: true,
				backdrop: "static",
				keyboard: false
			});
		} else if ($modal['uninstalledUpdates'].length) {
			$modal['uninstalledUpdates'].modal({
				show: true,
				backdrop: "static",
				keyboard: false
			});
		}
	}
	
	document.addEventListener('DOMContentLoaded', onInit);
})();