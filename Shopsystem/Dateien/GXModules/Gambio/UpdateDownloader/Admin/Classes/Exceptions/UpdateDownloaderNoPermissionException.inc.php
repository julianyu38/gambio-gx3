<?php
/* --------------------------------------------------------------
   UpdateDownloaderNoPermissionException.inc.php 2018-01-09
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class UpdateDownloaderNoPermissionException
 */
class UpdateDownloaderNoPermissionException extends Exception
{
	
}