<?php
/* --------------------------------------------------------------
   UpdateDownloaderInterface.inc.php 2018-02-08
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface UpdateDownloaderInterface
 */
interface UpdateDownloaderInterface
{
	/**
	 * HubConnectorUpdater constructor.
	 *
	 * @throws UpdateDownloaderServerNotAvailableException
	 */
	public function __construct();
	
	
	/**
	 * Returns the files with could not be created.
	 *
	 * @return array
	 */
	public function getNotPermittedFiles();
	
	
	/**
	 * Returns the updates which are not available.
	 *
	 * @return array
	 */
	public function getNotAvailableUpdates();
	
	
	/**
	 * Returns an array with not restored updates.
	 *
	 * @return array
	 */
	public function getNotRestoredBackups();
	
	
	/**
	 * Checks the backup directory for not restored backups.
	 *
	 * @return array
	 */
	public function checkForUnrestoredBackups();
	
	
	/**
	 * Returns the update status as an array. The array contains the keys body and info, which contains the body and
	 * header information of the curl request that was made.
	 *
	 * @return array
	 */
	public function getUpdatesStatus();
	
	
	/**
	 * Returns an array with all update versions which are already downloaded.
	 *
	 * @return array
	 */
	public function getDownloadedUpdates();
	
	
	/**
	 * Starts the download of all available updates.
	 *
	 * @param array $selectedUpdates An array which contains all receipt filenames of updates to be downloaded.
	 *
	 * @throws \UpdateDownloaderMD5HashException
	 * @throws \UpdateDownloaderNoPermissionException
	 * @throws \UpdateDownloaderNoUpdateAvailableException
	 * @throws \UpdateDownloaderUpdateNotAvailableException
	 * @throws \UpdateDownloaderZipCorruptedException
	 */
	public function downloadUpdates($selectedUpdates);
	
	
	/**
	 * Starts the installation of all available downloads. Updates will be downloaded bevore installation, if the
	 * "download before update" option is active.
	 *
	 * @param array $selectedUpdates An array which contains all receipt filenames of updates to be downloaded.
	 *
	 * @throws \Exception
	 * @throws \UpdateDownloaderBackupRestoreFailedException
	 * @throws \UpdateDownloaderNoUpdateAvailableException
	 * @throws \UpdateDownloaderUpdateFailedException
	 * @throws \UpdateDownloaderUpdatesNotDownloadedException
	 * @throws \UpdateDownloaderNotRestoredBackupException
	 */
	public function installUpdates($selectedUpdates);
}