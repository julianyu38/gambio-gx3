<?php
/* --------------------------------------------------------------
   AutoUpdaterAjaxController.inc.php 2018-05-23
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

MainFactory::load_class('AdminHttpViewController');

/**
 * Class AutoUpdaterAjaxController
 */
class AutoUpdaterAjaxController extends AdminHttpViewController
{
	/**
	 * @var CI_DB_query_builder
	 */
	protected $db;
	
	/**
	 * @var \messageStack_ORIGIN
	 */
	private $messageStack;
	
	/**
	 * @var \LanguageTextManager
	 */
	private $textPhrases;
	
	/**
	 * @var string
	 */
	private $updateServer;
	
	
	/**
	 * Initial method for this controller.
	 */
	public function init()
	{
		$this->db           = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$this->messageStack = $GLOBALS['messageStack'];
		$this->textPhrases  = MainFactory::create('LanguageTextManager', 'update_downloader',
		                                          $_SESSION['languages_id']);
		
		$this->updateServer = gm_get_conf('UPDATE_DOWNLOADER_NOTIFY_URL');
		if(empty($this->updateServer))
		{
			$this->updateServer = 'https://updates.gambio-support.de/v2/callingHome.php';
		}
	}
	
	
	/**
	 * Installs a single update.
	 *
	 * @return \RedirectHttpControllerResponse
	 *
	 * @throws \Exception
	 */
	public function actionInstallUpdate()
	{
		try
		{
			$selectedUpdate = $this->_getPostData('selectedUpdate');
			if($selectedUpdate === null)
			{
				return MainFactory::create('JsonHttpControllerResponse', [
					'success' => false,
					'error'   => $this->textPhrases->get_text('NO_UPDATE_SELECTED')
				]);
			}
			else
			{
				$selectedUpdate = [$selectedUpdate => $selectedUpdate];
			}
			
			$updateDownloader = MainFactory::create('UpdateDownloader');
			$updateDownloader->installUpdates($selectedUpdate);
		}
		catch(UpdateDownloaderServerNotAvailableException $e)
		{
			$this->callingHome('UpdateServer did not answer me');
			
			return MainFactory::create('JsonHttpControllerResponse', [
				'success' => false,
				'error'   => $this->textPhrases->get_text('NO_UPDATES'),
				'list'    => ''
			]);
		}
		catch(UpdateDownloaderUpdateFailedException $e)
		{
			$error = $e->getMessage();
			$list  = '';
			if($updateDownloader->getNotPermittedFiles() > 0)
			{
				foreach($updateDownloader->getNotPermittedFiles() as $notPermittedFile)
				{
					$notPermittedFile = str_replace(DIR_FS_CATALOG . DIRECTORY_SEPARATOR, '', $notPermittedFile);
					$notPermittedFile = str_replace(realpath(DIR_FS_CATALOG) . DIRECTORY_SEPARATOR, '',
					                                $notPermittedFile);
					$list             .= '<li>' . sprintf($this->textPhrases->get_text('ERROR_PERMISSON'),
					                                      $notPermittedFile) . '</li>';
				}
			}
			
			$this->callingHome('Had an error while downloading/unpacking some updates', $selectedUpdate,
			                   $updateDownloader->getNotPermittedFiles(), $updateDownloader->getNotRestoredBackups());
			
			return MainFactory::create('JsonHttpControllerResponse',
			                           ['success' => false, 'error' => $error, 'list' => $list]);
		}
		catch(UpdateDownloaderNotRestoredBackupException $e)
		{
			$error = $e->getMessage();
			$list  = '';
			if($updateDownloader->getNotRestoredBackups() > 0)
			{
				foreach($updateDownloader->getNotRestoredBackups() as $updateName => $backupPath)
				{
					$list .= '<li>' . sprintf($this->textPhrases->get_text('UNRESTORED_UPDATE_BACKUP'), $backupPath,
					                          $updateName) . '</li>';
				}
			}
			
			$this->callingHome('User had an unrestored backup', $selectedUpdate,
			                   $updateDownloader->getNotPermittedFiles(), $updateDownloader->getNotRestoredBackups());
			
			return MainFactory::create('JsonHttpControllerResponse',
			                           ['success' => false, 'error' => $error, 'list' => $list]);
		}
		catch(Exception $e)
		{
			$this->callingHome('Had a critical error while downloading/unpacking some updates', $selectedUpdate,
			                   $updateDownloader->getNotPermittedFiles(), $updateDownloader->getNotRestoredBackups(),
			                   $e);
			$error = str_replace(DIR_FS_CATALOG . DIRECTORY_SEPARATOR, '', $e->getMessage());
			$error = str_replace(realpath(DIR_FS_CATALOG) . DIRECTORY_SEPARATOR, '', $error);
			
			return MainFactory::create('JsonHttpControllerResponse',
			                           ['success' => false, 'error' => $error, 'list' => '']);
		}
		
		$dataCache = DataCache::get_instance();
		if($dataCache->key_exists('auto-updater', true))
		{
			$dataCache->add_data('auto-updater', $selectedUpdate, true);
		}
		else
		{
			$dataCache->set_data('auto-updater', $selectedUpdate, true);
		}
		
		$this->callingHome('Downloaded successfully some updates', $selectedUpdate,
		                   $updateDownloader->getNotPermittedFiles(), $updateDownloader->getNotRestoredBackups());
		
		return MainFactory::create('JsonHttpControllerResponse', ['success' => true, 'error' => '', 'list' => '']);
	}
	
	
	/**
	 * Sends some data about the update process to the update server.
	 *
	 * @param string         $message
	 * @param array          $selectedUpdates
	 * @param array          $notPermittedFiles
	 * @param array          $notRestoredBackups
	 * @param Exception|null $exception
	 */
	protected function callingHome($message,
	                               $selectedUpdates = [],
	                               $notPermittedFiles = [],
	                               $notRestoredBackups = [],
	                               $exception = null)
	{
		if($exception !== null)
		{
			$exception = [
				'message' => $exception->getMessage(),
				'trace'   => $exception->getTrace()
			];
		}
		
		$options = [
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => [
				'message'            => $message,
				'shopVersion'        => gm_get_conf('INSTALLED_VERSION'),
				'shopUrl'            => HTTP_SERVER . DIR_WS_CATALOG,
				'shopKey'            => $this->getShopKey(),
				'selectedUpdates'    => json_encode(array_keys($selectedUpdates)),
				'notPermittedFiles'  => json_encode($notPermittedFiles),
				'notRestoredBackups' => json_encode($notRestoredBackups),
				'exception'          => json_encode($exception),
			],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL            => $this->updateServer,
		];
		
		$curlHandle = curl_init();
		curl_setopt_array($curlHandle, $options);
		curl_exec($curlHandle);
		curl_close($curlHandle);
	}
	
	
	/**
	 * Returns the shop key from database.
	 *
	 * @return mixed
	 */
	protected function getShopKey()
	{
		$query   = $this->db->select('configuration_value')
		                    ->from('configuration')
		                    ->where('configuration_key', 'GAMBIO_SHOP_KEY')
		                    ->get();
		$shopKey = $query->row();
		if(isset($shopKey->configuration_value))
		{
			return $shopKey->configuration_value;
		}
		
		return null;
	}
}