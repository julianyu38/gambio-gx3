<?php
/* --------------------------------------------------------------
   AutoUpdaterController.inc.php 2018-07-30
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

MainFactory::load_class('AdminHttpViewController');

/**
 * Class UpdateDownloaderController
 */
class AutoUpdaterController extends AdminHttpViewController
{
	/**
	 * @var CI_DB_query_builder
	 */
	protected $db;
	
	/**
	 * @var \messageStack_ORIGIN
	 */
	private $messageStack;
	
	/**
	 * @var \LanguageTextManager
	 */
	private $textPhrases;
	
	/**
	 * @var \UserConfigurationService
	 */
	protected $userConfigurationService;
	
	/**
	 * @var string
	 */
	private $updateServer;
	
	
	/**
	 * Initial method for this controller.
	 */
	public function init()
	{
		$this->db                       = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$this->messageStack             = $GLOBALS['messageStack'];
		$this->textPhrases              = MainFactory::create('LanguageTextManager', 'update_downloader',
		                                                      $_SESSION['languages_id']);
		$this->userConfigurationService = StaticGXCoreLoader::getService('UserConfiguration');
		
		$this->updateServer = gm_get_conf('UPDATE_DOWNLOADER_NOTIFY_URL');
		if(empty($this->updateServer))
		{
			$this->updateServer = 'https://updates.gambio-support.de/v2/callingHome.php';
		}
	}
	
	
	/**
	 * Default action.
	 * Shows the current and available version and also all available updates.
	 *
	 * @return \AdminLayoutHttpControllerResponse
	 */
	public function actionDefault()
	{
		if(gm_get_conf('UPDATE_DOWNLOADER_ACCEPT_DATA_PRIVACY') !== 'true')
		{
			return $this->actionDataPrivacy();
		}
		
		$title    = new NonEmptyStringType($this->textPhrases->get_text('PAGE_TITLE'));
		$template = new ExistingFile(new NonEmptyStringType(DIR_FS_CATALOG
		                                                    . 'GXModules/Gambio/UpdateDownloader/Admin/Html/overview.html'));
		
		try
		{
			$updateDownloader = MainFactory::create('UpdateDownloader');
			$data             = MainFactory::create('KeyValueCollection', [
				'updateStatus'       => $updateDownloader->getUpdatesStatus(),
				'uninstalledUpdates' => $this->checkForUninstalledUpdates(),
				'showInfo'           => $this->userConfigurationService->getUserConfiguration(new IdType($_SESSION['customer_id']),
				                                                                              'auto-updater-show-info')
				                        !== 'v2.0.2',
			]);
			
			$this->userConfigurationService->setUserConfiguration(new IdType($_SESSION['customer_id']),
			                                                      'auto-updater-show-info', 'v2.0.2');
		}
		catch(UpdateDownloaderServerNotAvailableException $e)
		{
			$data = MainFactory::create('KeyValueCollection', []);
		}
		
		$assets = MainFactory::create('AssetCollection');
		$assets->add(MainFactory::create('Asset',
		                                 '../GXModules/Gambio/UpdateDownloader/Admin/Styles/update_downloader_v2.css'));
		$assets->add(MainFactory::create('Asset', '../JSEngine/build/libs/loading_spinner.min.js'));
		$assets->add(MainFactory::create('Asset',
		                                 '../GXModules/Gambio/UpdateDownloader/Admin/Javascript/update_downloader_v2.js'));
		$assets->add(MainFactory::create('Asset', 'update_downloader.lang.inc.php'));
		
		return MainFactory::create('AdminLayoutHttpControllerResponse', $title, $template, $data, $assets);
	}
	
	
	/**
	 * Install updates action.
	 * May download all available updates and unzips them. Checks the file permissions and copy the files into the shop.
	 *
	 * @return \RedirectHttpControllerResponse
	 *
	 * @throws \Exception
	 */
	public function actionInstallUpdates()
	{
		try
		{
			$selectedUpdates = $this->_getPostData('selectedUpdates');
			$selectedUpdates = ($selectedUpdates !== null) ? $selectedUpdates : [];
			
			$updateDownloader = MainFactory::create('UpdateDownloader');
			$updateStatus     = $updateDownloader->getUpdatesStatus();
			foreach($updateStatus['body']['updates'] as $update)
			{
				if($update['required'])
				{
					$selectedUpdates[$update['receipt']] = $update['receipt'];
				}
			}
			$updateDownloader->installUpdates($selectedUpdates);
		}
		catch(UpdateDownloaderServerNotAvailableException $e)
		{
			$this->messageStack->add_session($this->textPhrases->get_text('NO_UPDATES'), 'info');
			$this->callingHome('UpdateServer did not answer me');
			
			return MainFactory::create('RedirectHttpControllerResponse', 'admin.php?do=AutoUpdater');
		}
		catch(UpdateDownloaderUpdateFailedException $e)
		{
			$this->messageStack->add_session($e->getMessage(), 'error');
			if($updateDownloader->getNotPermittedFiles() > 0)
			{
				foreach($updateDownloader->getNotPermittedFiles() as $notPermittedFile)
				{
					$this->messageStack->add_session(sprintf($this->textPhrases->get_text('ERROR_PERMISSON'),
					                                         $notPermittedFile), 'info');
				}
			}
			
			$this->callingHome('Had an error while downloading/unpacking some updates', $selectedUpdates,
			                   $updateDownloader->getNotPermittedFiles(), $updateDownloader->getNotRestoredBackups(),
			                   $e);
			
			return MainFactory::create('RedirectHttpControllerResponse', 'admin.php?do=AutoUpdater');
		}
		catch(UpdateDownloaderNotRestoredBackupException $e)
		{
			$this->messageStack->add_session($e->getMessage(), 'error');
			if($updateDownloader->getNotRestoredBackups() > 0)
			{
				foreach($updateDownloader->getNotRestoredBackups() as $updateName => $backupPath)
				{
					$this->messageStack->add_session(sprintf($this->textPhrases->get_text('UNRESTORED_UPDATE_BACKUP'),
					                                         $backupPath, $updateName), 'info');
				}
			}
			
			$this->callingHome('User had an unrestored backup', $selectedUpdates,
			                   $updateDownloader->getNotPermittedFiles(), $updateDownloader->getNotRestoredBackups(),
			                   $e);
			
			return MainFactory::create('RedirectHttpControllerResponse', 'admin.php?do=AutoUpdater');
		}
		catch(Exception $e)
		{
			if(!($e instanceof UpdateDownloaderUpdatesNotDownloadedException
			     || $e instanceof UpdateDownloaderUpdateNotAvailableException
			     || $e instanceof UpdateDownloaderBackupRestoreFailedException
			     || $e instanceof UpdateDownloaderNoUpdateAvailableException))
			{
				# throw all exceptions that doesn't come from updater
				throw $e;
			}
			
			$this->messageStack->add_session($e->getMessage(), 'error');
			$this->callingHome('Had a critical error while downloading/unpacking some updates', $selectedUpdates,
			                   $updateDownloader->getNotPermittedFiles(), $updateDownloader->getNotRestoredBackups(),
			                   $e);
			
			return MainFactory::create('RedirectHttpControllerResponse', 'admin.php?do=AutoUpdater');
		}
		
		if(count($selectedUpdates) > 0)
		{
			$dataCache = DataCache::get_instance();
			if($dataCache->key_exists('auto-updater', true))
			{
				$dataCache->add_data('auto-updater', $selectedUpdates, true);
			}
			else
			{
				$dataCache->set_data('auto-updater', $selectedUpdates, true);
			}
			
			$this->callingHome('Downloaded successfully some updates', $selectedUpdates,
			                   $updateDownloader->getNotPermittedFiles(), $updateDownloader->getNotRestoredBackups());
			
			return MainFactory::create('RedirectHttpControllerResponse', '../gambio_updater');
		}
		
		return MainFactory::create('RedirectHttpControllerResponse', 'admin.php?do=AutoUpdater');
	}
	
	
	/**
	 * Data privacy page.
	 *
	 * Displays the data privacy information.
	 *
	 * @return \AdminLayoutHttpControllerResponse
	 */
	public function actionDataPrivacy()
	{
		$title    = new NonEmptyStringType($this->textPhrases->get_text('PAGE_TITLE'));
		$template = new ExistingFile(new NonEmptyStringType(DIR_FS_CATALOG
		                                                    . 'GXModules/Gambio/UpdateDownloader/Admin/Html/data_privacy.html'));
		
		return MainFactory::create('AdminLayoutHttpControllerResponse', $title, $template);
	}
	
	
	/**
	 * Accept data privacy action.
	 *
	 * This action will enable the communication between the auto updater and the update server.
	 *
	 * @return \AdminLayoutHttpControllerResponse
	 */
	public function actionAcceptDataPrivacy()
	{
		gm_set_conf('UPDATE_DOWNLOADER_ACCEPT_DATA_PRIVACY', 'true');
		
		return MainFactory::create('RedirectHttpControllerResponse', 'admin.php?do=AutoUpdater');
	}
	
	
	/**
	 * Dissent data privacy action.
	 *
	 * This action will disable the communication between the auto updater and the update server.
	 *
	 * @return \AdminLayoutHttpControllerResponse
	 */
	public function actionDissentDataPrivacy()
	{
		gm_set_conf('UPDATE_DOWNLOADER_ACCEPT_DATA_PRIVACY', 'false');
		
		return MainFactory::create('RedirectHttpControllerResponse', 'admin.php?do=AutoUpdater');
	}
	
	
	/**
	 * Checks the version info directory and version history database table for uninstalled updates.
	 *
	 * @return bool Returns true, if there is an uninstalled update, otherwise false.
	 */
	public function checkForUninstalledUpdates()
	{
		$uninstalledUpdates = false;
		$dataCache          = DataCache::get_instance();
		if($dataCache->key_exists('auto-updater', true))
		{
			$downloadedUpdates = $dataCache->get_data('auto-updater', true);
			
			if(count($downloadedUpdates) > 0)
			{
				foreach($downloadedUpdates as $updateReceipt)
				{
					if(!file_exists(DIR_FS_CATALOG . 'version_info/' . $updateReceipt))
					{
						continue;
					}
					
					$version = $this->determineVersion(new NonEmptyStringType(DIR_FS_CATALOG . 'version_info/'
					                                                          . $updateReceipt));
					if(!empty($version) && !$this->checkVersionIsInstalled($version))
					{
						$uninstalledUpdates = true;
					}
					else
					{
						unset($downloadedUpdates[$updateReceipt]);
					}
				}
				
				$dataCache->set_data('auto-updater', $downloadedUpdates, true);
			}
		}
		
		return $uninstalledUpdates;
	}
	
	
	/**
	 * Determines the version of the version info items while considering known edge cases.
	 *
	 * @param \NonEmptyStringType $versionInfoFilePath
	 *
	 * @return string $version
	 */
	protected function determineVersion(NonEmptyStringType $versionInfoFilePath)
	{
		$versionInfoFilePath = $versionInfoFilePath->asString();
		$version             = ' ';
		
		$directoryItemFile = fopen($versionInfoFilePath, 'r');
		while($line = fgets($directoryItemFile))
		{
			if(strpos($line, 'version:') === 0)
			{
				$version = trim(str_replace('version:', '', $line));
				
				break;
			}
		}
		fclose($directoryItemFile);
		
		return trim($version);
	}
	
	
	/**
	 * Checks the version history database table if the given version is installed.
	 *
	 * @return bool Returns true, if the given version is installed, otherwise false.
	 */
	protected function checkVersionIsInstalled($version)
	{
		$db        = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$installed = $db->select()->from('version_history')->where(['name' => $version])->or_where([
			                                                                                           'name' => 'v'
			                                                                                                     . $version
		                                                                                           ])->get()->num_rows()
		             > 0;
		
		return $installed;
	}
	
	
	/**
	 * Sends some data about the update process to the update server.
	 *
	 * @param string         $message
	 * @param array          $selectedUpdates
	 * @param array          $notPermittedFiles
	 * @param array          $notRestoredBackups
	 * @param Exception|null $exception
	 */
	protected function callingHome($message,
	                               $selectedUpdates = [],
	                               $notPermittedFiles = [],
	                               $notRestoredBackups = [],
	                               $exception = null)
	{
		if($exception !== null)
		{
			$exception = [
				'message' => $exception->getMessage(),
				'trace'   => $exception->getTrace()
			];
		}
		
		$options = [
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => [
				'message'            => $message,
				'shopVersion'        => gm_get_conf('INSTALLED_VERSION'),
				'shopUrl'            => HTTP_SERVER . DIR_WS_CATALOG,
				'shopKey'            => $this->getShopKey(),
				'selectedUpdates'    => json_encode(array_keys($selectedUpdates)),
				'notPermittedFiles'  => json_encode($notPermittedFiles),
				'notRestoredBackups' => json_encode($notRestoredBackups),
				'exception'          => json_encode($exception),
			],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL            => $this->updateServer,
		];
		
		$curlHandle = curl_init();
		curl_setopt_array($curlHandle, $options);
		curl_exec($curlHandle);
		curl_close($curlHandle);
	}
	
	
	/**
	 * Returns the shop key from database.
	 *
	 * @return mixed
	 */
	protected function getShopKey()
	{
		$query   = $this->db->select('configuration_value')
		                    ->from('configuration')
		                    ->where('configuration_key', 'GAMBIO_SHOP_KEY')
		                    ->get();
		$shopKey = $query->row();
		if(isset($shopKey->configuration_value))
		{
			return $shopKey->configuration_value;
		}
		
		return null;
	}
}