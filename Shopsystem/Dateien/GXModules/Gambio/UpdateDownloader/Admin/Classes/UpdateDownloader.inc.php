<?php
/* --------------------------------------------------------------
   UpdateDownloader.inc.php 2018-07-30
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

MainFactory::load_class('UpdateDownloaderInterface');

/**
 * Class UpdateDownloader
 */
class UpdateDownloader implements UpdateDownloaderInterface
{
	/**
	 * @var CI_DB_query_builder
	 */
	protected $db;
	
	/**
	 * @var string
	 */
	protected $updateDownloaderVersion;
	
	/**
	 * @var array
	 */
	protected $downloadedUpdates;
	
	/**
	 * @var messageStack_ORIGIN
	 */
	protected $messageStack;
	
	/**
	 * @var LanguageTextManager
	 */
	protected $textPhrases;
	
	/**
	 * @var string
	 */
	protected $backupsDirectory;
	
	/**
	 * @var string
	 */
	protected $updatesDirectory;
	
	/**
	 * @var string
	 */
	protected $updateServer;
	
	/**
	 * @var array
	 */
	protected $updateStatus;
	
	/**
	 * @var string
	 */
	protected $langCode;
	
	/**
	 * @var array
	 */
	protected $noPermission;
	
	/**
	 * @var array
	 */
	protected $notAvailable;
	
	/**
	 * @var int
	 */
	protected $writingPermission;
	
	
	/**
	 * UpdateDownloader constructor.
	 *
	 * @throws UpdateDownloaderDataPrivacyNotAcceptedException
	 * @throws UpdateDownloaderServerNotAvailableException
	 */
	public function __construct()
	{
		if(gm_get_conf('UPDATE_DOWNLOADER_ACCEPT_DATA_PRIVACY') !== 'true')
		{
			throw new UpdateDownloaderDataPrivacyNotAcceptedException('Data privacy not accepted.');
		}
		
		$this->db                = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$this->textPhrases       = MainFactory::create('LanguageTextManager', 'update_downloader',
		                                               $_SESSION['languages_id']);
		$this->backupsDirectory  = DIR_FS_CATALOG . 'cache/update_downloader/backups';
		$this->updatesDirectory  = DIR_FS_CATALOG . 'cache/update_downloader/updates';
		$this->updateServer      = gm_get_conf('UPDATE_DOWNLOADER_CHECK_URL');
		$this->noPermission      = [];
		$this->notAvailable      = [];
		$this->unrestoredBackup  = [];
		$this->writingPermission = $this->determineCorrectFilePermission();
		
		$languageProvider = MainFactory::create('LanguageProvider', $this->db);
		$this->langCode   = strtolower($languageProvider->getCodeById(new IdType((int)$_SESSION['languages_id'])));
		
		if(empty($this->updateServer))
		{
			$this->updateServer = 'https://updates.gambio-support.de/v2/check.php';
		}
		
		$this->checkUpdates();
		$this->checkDownloadedUpdates();
	}
	
	
	/**
	 * Returns the files with could not be created.
	 *
	 * @return array
	 */
	public function getNotPermittedFiles()
	{
		return array_unique($this->noPermission);
	}
	
	
	/**
	 * Returns the updates which are not available.
	 *
	 * @return array
	 */
	public function getNotAvailableUpdates()
	{
		return array_unique($this->notAvailable);
	}
	
	
	/**
	 * Returns the updates which are not available.
	 *
	 * @return array
	 */
	public function getNotRestoredBackups()
	{
		return array_unique($this->unrestoredBackup);
	}
	
	
	/**
	 * Checks the backup directory for not restored backups.
	 *
	 * @return array
	 */
	public function checkForUnrestoredBackups()
	{
		if(!file_exists($this->backupsDirectory) || !is_dir($this->backupsDirectory))
		{
			return false;
		}
		
		$unrestoredBackups = scandir($this->backupsDirectory);
		foreach($this->updateStatus['body']['updates'] as $updateKey => $update)
		{
			if(in_array($updateKey, $unrestoredBackups))
			{
				$this->unrestoredBackup[$update['name'][$this->langCode]] = $this->backupsDirectory
				                                                            . DIRECTORY_SEPARATOR . $updateKey
				                                                            . DIRECTORY_SEPARATOR;
				
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Returns the update status as an array. The array contains the keys body and info, which contains the body and
	 * header information of the curl request that was made.
	 *
	 * @return array
	 */
	public function getUpdatesStatus()
	{
		return $this->updateStatus;
	}
	
	
	/**
	 * Returns an array with all update versions which are already downloaded.
	 *
	 * @return array
	 */
	public function getDownloadedUpdates()
	{
		return $this->downloadedUpdates;
	}
	
	
	/**
	 * Starts the download of all available updates.
	 *
	 * @param array $selectedUpdates An array which contains all receipt filenames of updates to be downloaded.
	 *
	 * @throws \UpdateDownloaderMD5HashException
	 * @throws \UpdateDownloaderNoPermissionException
	 * @throws \UpdateDownloaderNoUpdateAvailableException
	 * @throws \UpdateDownloaderUpdateNotAvailableException
	 * @throws \UpdateDownloaderZipCorruptedException
	 */
	public function downloadUpdates($selectedUpdates)
	{
		$this->deleteUpdatesDirectory();
		
		if(count($this->updateStatus['body']['updates']) === 0)
		{
			throw new UpdateDownloaderNoUpdateAvailableException($this->textPhrases->get_text('NO_UPDATES'));
		}
		
		if(!class_exists('ZipArchive'))
		{
			foreach($this->updateStatus['body']['updates'] as $updateKey => $update)
			{
				if(!in_array($update['receipt'], $selectedUpdates))
				{
					continue;
				}
				
				$this->downloadUpdateNoZip($update, $updateKey);
			}
			
			$this->checkDownloadedUpdates();
			
			return;
		}
		
		foreach($this->updateStatus['body']['updates'] as $updateKey => $update)
		{
			if(!in_array($update['receipt'], $selectedUpdates))
			{
				continue;
			}
			
			$this->downloadUpdateZip($update, $updateKey);
		}
		
		$this->checkDownloadedUpdates();
		
		return;
	}
	
	
	/**
	 * Starts the installation of all available downloads. Updates will be downloaded bevore installation, if the
	 * "download before update" option is active.
	 *
	 * @param array $selectedUpdates An array which contains all receipt filenames of updates to be downloaded.
	 *
	 * @throws \Exception
	 * @throws \UpdateDownloaderBackupRestoreFailedException
	 * @throws \UpdateDownloaderNoUpdateAvailableException
	 * @throws \UpdateDownloaderUpdateFailedException
	 * @throws \UpdateDownloaderUpdatesNotDownloadedException
	 * @throws \UpdateDownloaderUpdateNotAvailableException
	 * @throws \UpdateDownloaderNotRestoredBackupException
	 *
	 */
	public function installUpdates($selectedUpdates)
	{
		if(count($this->updateStatus['body']['updates']) === 0)
		{
			throw new UpdateDownloaderNoUpdateAvailableException($this->textPhrases->get_text('NO_UPDATES'));
		}
		
		if($this->checkForUnrestoredBackups())
		{
			throw new UpdateDownloaderNotRestoredBackupException($this->textPhrases->get_text('UNRESTORED_BACKUP'));
		}
		
		$this->downloadUpdates($selectedUpdates);
		
		if(count($selectedUpdates) !== count($this->downloadedUpdates))
		{
			throw new UpdateDownloaderUpdatesNotDownloadedException($this->textPhrases->get_text('UPDATES_NOT_DOWNLOADED'));
		}
		
		if(count($this->updateStatus['body']['updates']) > 0)
		{
			foreach($this->updateStatus['body']['updates'] as $updateKey => $update)
			{
				if(!in_array($update['receipt'], $selectedUpdates))
				{
					continue;
				}
				
				try
				{
					$this->checkPermission($updateKey);
					$this->backupFiles($updateKey);
					$this->copyFiles($updateKey);
					$this->resetVersionHistoryName($update['versionHistoryName']);
				}
				catch(Exception $e)
				{
					if(!($e instanceof UpdateDownloaderNoPermissionException
					     || $e instanceof UpdateDownloaderBackupCreationFailedException
					     || $e instanceof UpdateDownloaderCopyFailedException))
					{
						# throw all exceptions that doesn't come from updater
						throw $e;
					}
					
					$this->restoreBackup($updateKey);
					$this->deleteBackupsDirectory();
					$this->deleteUpdatesDirectory();
					
					throw new UpdateDownloaderUpdateFailedException(sprintf($this->textPhrases->get_text('UPDATE_FAILED'),
					                                                        $update['name'][$this->langCode]));
				}
			}
			
			$this->deleteBackupsDirectory();
			$this->deleteUpdatesDirectory();
		}
		else
		{
			throw new UpdateDownloaderNoUpdateAvailableException($this->textPhrases->get_text('NO_UPDATES'));
		}
	}
	
	
	/**
	 * Checks the update server for new updates. Saves the result internally.
	 *
	 * @throws \UpdateDownloaderServerNotAvailableException
	 */
	protected function checkUpdates()
	{
		$dataCache         = DataCache::get_instance();
		$downloadedUpdates = [];
		if($dataCache->key_exists('auto-updater', true))
		{
			$downloadedUpdates = $dataCache->get_data('auto-updater', true);
		}
		$this->checkFileOwnerCompatibility($ftpUser, $webUser);
		
		$this->updateStatus = [];
		$options            = [
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => [
				'shopVersion'       => gm_get_conf('INSTALLED_VERSION'),
				'shopUrl'           => HTTP_SERVER . DIR_WS_CATALOG,
				'shopKey'           => $this->getShopKey(),
				'versionHistory'    => json_encode($this->getVersionHistory()),
				'versionReceipts'   => json_encode($this->getVersionReceipts()),
				'downloadedUpdates' => json_encode($downloadedUpdates),
				'ftpUser'           => $ftpUser,
				'webUser'           => $webUser,
			],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL            => $this->updateServer,
		];
		
		$curlHandle = curl_init();
		curl_setopt_array($curlHandle, $options);
		$this->updateStatus['body'] = curl_exec($curlHandle);
		$this->updateStatus['body'] = json_decode($this->updateStatus['body'], true);
		$this->updateStatus['info'] = curl_getinfo($curlHandle);
		curl_close($curlHandle);
		
		if($this->updateStatus['info']['http_code'] !== 200)
		{
			throw new UpdateDownloaderServerNotAvailableException($this->textPhrases->get_text('NO_UPDATES'));
		}
	}
	
	
	/**
	 * Checks the updates directory for downloaded updates. Saves the result internally.
	 */
	protected function checkDownloadedUpdates()
	{
		$this->downloadedUpdates = [];
		
		if(file_exists($this->updatesDirectory) && is_dir($this->updatesDirectory)
		   && count($this->updateStatus['body']['updates']) > 0)
		{
			foreach($this->updateStatus['body']['updates'] as $updateKey => $update)
			{
				$updateDir = $this->updatesDirectory . DIRECTORY_SEPARATOR . $updateKey;
				if(file_exists($updateDir) && is_dir($updateDir))
				{
					$this->downloadedUpdates[] = $updateKey;
				}
			}
		}
	}
	
	
	/**
	 * Downloads a single update als a zip archiv and unpacks it into in the updates directory.
	 * If the zip extension is not available on the webserver, the downloadUpdateNoZip method will be used as a
	 * fallback.
	 *
	 * @param array  $update
	 * @param string $updateKey
	 *
	 * @throws \UpdateDownloaderMD5HashException
	 * @throws \UpdateDownloaderNoPermissionException
	 * @throws \UpdateDownloaderUpdateNotAvailableException
	 * @throws \UpdateDownloaderZipCorruptedException
	 */
	protected function downloadUpdateZip($update, $updateKey)
	{
		if(!file_exists($this->updatesDirectory) || !is_dir($this->updatesDirectory))
		{
			$createDir = $this->createDirectory($this->updatesDirectory);
			if($createDir === false)
			{
				$this->noPermission[] = $this->updatesDirectory;
				throw new UpdateDownloaderNoPermissionException();
			}
		}
		
		$curl = curl_init($update['zip']);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		curl_exec($curl);
		$fileExists = curl_getinfo($curl, CURLINFO_HTTP_CODE) === 200;
		curl_close($curl);
		
		if(!$fileExists)
		{
			$this->downloadUpdateNoZip($update, $updateKey);
		}
		else
		{
			$zipFile = $this->updatesDirectory . DIRECTORY_SEPARATOR . $updateKey . '.zip';
			$this->downloadExternalFile($update['zip'], $zipFile);
			
			if(!file_exists($zipFile))
			{
				$this->noPermission[] = $zipFile;
				throw new UpdateDownloaderNoPermissionException();
			}
			
			try
			{
				if(isset($update['zip_hash']) && md5_file($zipFile) !== $update['zip_hash'])
				{
					throw new UpdateDownloaderMD5HashException(sprintf($this->textPhrases->get_text('ERROR_MD5_HASH'),
					                                                   substr($update['zip'],
					                                                          strrpos($update['zip'], '/') + 1)));
				}
				
				$this->unzipUpdate($update, $updateKey);
				@unlink($zipFile);
			}
			catch(Exception $e)
			{
				if(!($e instanceof UpdateDownloaderZipCorruptedException
				     || $e instanceof UpdateDownloaderMD5HashException))
				{
					# throw all exceptions that doesn't come from updater
					throw $e;
				}
				
				@unlink($zipFile);
				$this->downloadUpdateNoZip($update, $updateKey);
			}
		}
	}
	
	
	/**
	 * Downloads all update files from a single update into in the updates directory.
	 * This method is a fallback, if the zip extension is not available on the webserver.
	 *
	 * @param array  $update
	 * @param string $updateKey
	 *
	 * @throws \UpdateDownloaderNoPermissionException
	 * @throws \UpdateDownloaderUpdateNotAvailableException
	 * @throws \UpdateDownloaderMD5HashException
	 */
	protected function downloadUpdateNoZip($update, $updateKey)
	{
		$updateDir = $this->updatesDirectory . DIRECTORY_SEPARATOR . $updateKey;
		
		if(!file_exists($updateDir) || !is_dir($updateDir))
		{
			$createDir = $this->createDirectory($updateDir);
			if($createDir === false)
			{
				$this->noPermission[] = $updateDir;
				throw new UpdateDownloaderNoPermissionException();
			}
		}
		
		if(count($update['filelist']) === 0)
		{
			$this->deleteDirectory($updateDir);
			$this->notAvailable[] = $update['name'][$this->langCode];
			throw new UpdateDownloaderUpdateNotAvailableException(sprintf($this->textPhrases->get_text('UPDATE_NOT_AVAILABLE'),
			                                                              $update['name'][$this->langCode]));
		}
		
		foreach($update['filelist'] as $updateFile)
		{
			$curl = curl_init($updateFile['source']);
			curl_setopt($curl, CURLOPT_NOBODY, true);
			curl_exec($curl);
			$fileExists = curl_getinfo($curl, CURLINFO_HTTP_CODE) === 200;
			curl_close($curl);
			
			if(!$fileExists)
			{
				$this->deleteDirectory($updateDir);
				throw new UpdateDownloaderUpdateNotAvailableException(sprintf($this->textPhrases->get_text('UPDATE_NOT_AVAILABLE'),
				                                                              $update['name'][$this->langCode]));
			}
			else
			{
				$updateFileDestination    = $updateDir . DIRECTORY_SEPARATOR . $updateFile['destination'];
				$updateFileDestinationDir = substr($updateFileDestination, 0,
				                                   strrpos($updateFileDestination, DIRECTORY_SEPARATOR));
				
				if(!file_exists($updateFileDestinationDir) || !is_dir($updateFileDestinationDir))
				{
					$createDir = $this->createDirectory($updateFileDestinationDir);
					if($createDir === false)
					{
						$this->noPermission[] = $updateFileDestinationDir;
						throw new UpdateDownloaderNoPermissionException();
					}
				}
				
				$this->downloadExternalFile($updateFile['source'], $updateFileDestination);
				
				if(!file_exists($updateFileDestination))
				{
					$this->deleteUpdatesDirectory();
					$this->noPermission[] = $updateFileDestination;
					throw new UpdateDownloaderNoPermissionException();
				}
				elseif(isset($updateFile['hash']) && md5_file($updateFileDestination) !== $updateFile['hash'])
				{
					$this->deleteUpdatesDirectory();
					throw new UpdateDownloaderMD5HashException(sprintf($this->textPhrases->get_text('ERROR_MD5_HASH'),
					                                                   str_replace($updateDir . DIRECTORY_SEPARATOR, '',
					                                                               $updateFileDestination)));
				}
			}
		}
	}
	
	
	/**
	 * Unpacks the zip archive of a downloaded update.
	 *
	 * @param array  $update
	 * @param string $updateKey
	 *
	 * @throws \UpdateDownloaderNoPermissionException
	 * @throws \UpdateDownloaderZipCorruptedException
	 */
	protected function unzipUpdate($update, $updateKey)
	{
		$zipName   = $updateKey . '.zip';
		$updateDir = $this->updatesDirectory . DIRECTORY_SEPARATOR . $updateKey;
		
		if(file_exists($updateDir) && is_dir($updateDir))
		{
			$deleteDir = $this->deleteDirectory($updateDir);
			
			if($deleteDir === false)
			{
				$this->noPermission[] = $updateDir;
				throw new UpdateDownloaderNoPermissionException();
			}
		}
		
		$createDir = $this->createDirectory($updateDir);
		if($createDir === false)
		{
			$this->noPermission[] = $updateDir;
			throw new UpdateDownloaderNoPermissionException();
		}
		
		$zip          = new ZipArchive;
		$zipOpen      = @$zip->open($this->updatesDirectory . DIRECTORY_SEPARATOR . $zipName);
		$zipExtracted = @$zip->extractTo($updateDir);
		$zipClosed    = @$zip->close();
		if($zipOpen !== true || $zipExtracted === false || $zipClosed === false)
		{
			$this->deleteDirectory($updateDir);
			throw new UpdateDownloaderZipCorruptedException(sprintf($this->textPhrases->get_text('ERROR_ZIP_CORRUPTED'),
			                                                        $update['name'][$this->langCode]));
		}
	}
	
	
	/**
	 * Checks the permission (creation and writing of files and directories) for an update.
	 *
	 * @param $updateKey
	 *
	 * @throws \UpdateDownloaderNoPermissionException
	 */
	protected function checkPermission($updateKey)
	{
		$updateDir   = $this->updatesDirectory . DIRECTORY_SEPARATOR . $updateKey . DIRECTORY_SEPARATOR;
		$updateFiles = $this->getDirectoryFiles($updateDir);
		
		if(count($updateFiles) > 0)
		{
			foreach($updateFiles as $updateFile)
			{
				$shopTestFile = str_replace(realpath($updateDir), realpath(DIR_FS_CATALOG),
				                            $updateFile . '.permission_check');
				$shopTestDir  = substr($shopTestFile, 0, strrpos($shopTestFile, DIRECTORY_SEPARATOR));
				
				if(!file_exists($shopTestDir) || !is_dir($shopTestDir))
				{
					$dirCreated = $this->createDirectory($shopTestDir);
					if($dirCreated === false)
					{
						$this->noPermission[] = $shopTestDir;
						continue;
					}
				}
				elseif(file_exists($shopTestDir) && is_dir($shopTestDir) && !is_writeable($shopTestDir))
				{
					$this->noPermission[] = $shopTestDir;
					continue;
				}
				
				$fileOpen    = @fopen($shopTestFile, 'w');
				$fileWritten = @fwrite($fileOpen, 'permission test');
				$fileClosed  = @fclose($fileOpen);
				if($fileOpen === false || $fileWritten === false || $fileClosed === false)
				{
					$this->noPermission[] = $shopTestFile;
					continue;
				}
				
				@unlink($shopTestFile);
			}
		}
		
		if(count($this->noPermission) > 0)
		{
			throw new UpdateDownloaderNoPermissionException();
		}
	}
	
	
	/**
	 * Creates a backup.
	 *
	 * @param $updateKey
	 *
	 * @throws \UpdateDownloaderBackupCreationFailedException
	 * @throws \UpdateDownloaderNoPermissionException
	 */
	protected function backupFiles($updateKey)
	{
		$backupDir   = $this->backupsDirectory . DIRECTORY_SEPARATOR . $updateKey . DIRECTORY_SEPARATOR;
		$updateDir   = $this->updatesDirectory . DIRECTORY_SEPARATOR . $updateKey . DIRECTORY_SEPARATOR;
		$updateFiles = $this->getDirectoryFiles($updateDir);
		$this->deleteDirectory($backupDir);
		
		if(!file_exists($backupDir) || !is_dir($backupDir))
		{
			$dirCreated = $this->createDirectory($backupDir);
			if($dirCreated === false)
			{
				$this->noPermission[] = $backupDir;
				throw new UpdateDownloaderNoPermissionException();
			}
		}
		
		if(count($updateFiles) > 0)
		{
			foreach($updateFiles as $updateFile)
			{
				$shopFile      = str_replace(realpath($updateDir), realpath(DIR_FS_CATALOG), $updateFile);
				$backupFile    = str_replace(realpath($updateDir), realpath($backupDir), $updateFile);
				$backupFileDir = substr($backupFile, 0, strrpos($backupFile, DIRECTORY_SEPARATOR));
				
				if(file_exists($shopFile))
				{
					
					if(!file_exists($backupFileDir) || !is_dir($backupFileDir))
					{
						$this->createDirectory($backupFileDir);
					}
					
					$fileCopied = @copy($shopFile, $backupFile);
					@chmod($backupFile, $this->writingPermission);
					
					if(!$fileCopied)
					{
						$this->noPermission[] = $backupFile;
						throw new UpdateDownloaderNoPermissionException();
					}
					elseif(filesize($shopFile) !== filesize($backupFile))
					{
						throw new UpdateDownloaderBackupCreationFailedException(sprintf($this->textPhrases->get_text('ERROR_BACKUP_CREATION_FAILED'),
						                                                                $shopFile));
					}
					
					if($owner = @fileowner(DIR_FS_CATALOG) !== false)
					{
						@chown($backupFile, $owner);
					}
				}
			}
		}
	}
	
	
	/**
	 * Restores a backup.
	 *
	 * @param $updateKey
	 *
	 * @throws \UpdateDownloaderBackupRestoreFailedException
	 */
	protected function restoreBackup($updateKey)
	{
		$backupDir   = $this->backupsDirectory . DIRECTORY_SEPARATOR . $updateKey . DIRECTORY_SEPARATOR;
		$updateDir   = $this->updatesDirectory . DIRECTORY_SEPARATOR . $updateKey . DIRECTORY_SEPARATOR;
		$updateFiles = $this->getDirectoryFiles($updateDir);
		
		if(count($updateFiles) > 0)
		{
			foreach($updateFiles as $updateFile)
			{
				$shopFile   = str_replace(realpath($updateDir), realpath(DIR_FS_CATALOG), $updateFile);
				$backupFile = str_replace(realpath($updateDir), realpath($backupDir), $updateFile);
				
				if(file_exists($backupFile) && file_exists($shopFile))
				{
					$deleted = @unlink($shopFile);
					if($deleted === false)
					{
						throw new UpdateDownloaderBackupRestoreFailedException(sprintf($this->textPhrases->get_text('ERROR_BACKUP_RESTORE_FAILED'),
						                                                               $this->backupsDirectory . '/'
						                                                               . $updateKey));
					}
					
					@unlink($shopFile);
					$fileCopied = @copy($backupFile, $shopFile);
					@chmod($shopFile, $this->writingPermission);
					
					if(!$fileCopied || filesize($shopFile) !== filesize($backupFile))
					{
						throw new UpdateDownloaderBackupRestoreFailedException(sprintf($this->textPhrases->get_text('ERROR_BACKUP_RESTORE_FAILED'),
						                                                               $this->backupsDirectory . '/'
						                                                               . $updateKey));
					}
					
					if($owner = @fileowner(DIR_FS_CATALOG) !== false)
					{
						@chown($shopFile, $owner);
					}
				}
			}
		}
	}
	
	
	/**
	 * Copies the update files of a single update into the shop.
	 *
	 * @param $updateKey
	 *
	 * @throws \UpdateDownloaderNoPermissionException
	 * @throws \UpdateDownloaderCopyFailedException
	 */
	protected function copyFiles($updateKey)
	{
		$updateDir   = $this->updatesDirectory . DIRECTORY_SEPARATOR . $updateKey . DIRECTORY_SEPARATOR;
		$updateFiles = $this->getDirectoryFiles($updateDir);
		
		if(count($updateFiles) > 0)
		{
			foreach($updateFiles as $updateFile)
			{
				$shopFile = str_replace(realpath($updateDir), realpath(DIR_FS_CATALOG), $updateFile);
				$shopDir  = substr($shopFile, 0, strrpos($shopFile, DIRECTORY_SEPARATOR));
				
				if(!file_exists($shopDir) || !is_dir($shopDir))
				{
					$this->createDirectory($shopDir);
				}
				
				@unlink($shopFile);
				$fileCopied = @copy($updateFile, $shopFile);
				@chmod($shopFile, $this->writingPermission);
				
				if(!$fileCopied)
				{
					$this->noPermission[] = $shopFile;
					throw new UpdateDownloaderNoPermissionException();
				}
				elseif(filesize($updateFile) !== filesize($shopFile))
				{
					throw new UpdateDownloaderCopyFailedException(sprintf($this->textPhrases->get_text('ERROR_COPY_FAILED'),
					                                                      $updateFile, $shopFile));
				}
				
				if($owner = @fileowner(DIR_FS_CATALOG) !== false)
				{
					@chown($shopFile, $owner);
				}
			}
		}
	}
	
	
	/**
	 * Removes the version history entry of an update by a given version history name.
	 *
	 * @param $versionHistoryName string
	 */
	protected function resetVersionHistoryName($versionHistoryName)
	{
		$this->db->delete('version_history', ['name' => $versionHistoryName]);
	}
	
	
	/**
	 * Returns an array with all files of a directory and his subdirectories.
	 *
	 * @param string $dir
	 *
	 * @return array
	 */
	protected function getDirectoryFiles($dir)
	{
		$return = [];
		$files  = scandir($dir);
		foreach($files as $file)
		{
			if($file !== '.' && $file !== '..')
			{
				$path = realpath($dir . DIRECTORY_SEPARATOR . $file);
				if(!is_dir($path))
				{
					$return[] = $path;
				}
				else
				{
					$return = array_merge($return, $this->getDirectoryFiles($path));
				}
			}
		}
		
		return $return;
	}
	
	
	/**
	 * Deletes a directory. Return true on success and false otherwise.
	 *
	 * @param $dir
	 *
	 * @return bool
	 */
	protected function deleteDirectory($dir)
	{
		if(!file_exists($dir))
		{
			return true;
		}
		
		$files = array_diff(scandir($dir), ['.', '..']);
		foreach($files as $file)
		{
			if(is_dir($dir . DIRECTORY_SEPARATOR . $file))
			{
				$deleted = $this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $file);
				if($deleted === false)
				{
					return false;
				}
			}
			else
			{
				$deleted = @unlink($dir . DIRECTORY_SEPARATOR . $file);
				if($deleted === false)
				{
					return false;
				}
			}
		}
		
		return rmdir($dir);
	}
	
	
	/**
	 * Deletes the backups directory. Return true on success and false otherwise.
	 *
	 * @return bool
	 */
	protected function deleteBackupsDirectory()
	{
		return $this->deleteDirectory($this->backupsDirectory);
	}
	
	
	/**
	 * Deletes the updates directory. Return true on success and false otherwise.
	 *
	 * @return bool
	 */
	protected function deleteUpdatesDirectory()
	{
		return $this->deleteDirectory($this->updatesDirectory);
	}
	
	
	/**
	 * Returns the shop key from database.
	 *
	 * @return mixed
	 */
	protected function getShopKey()
	{
		$query   = $this->db->select('configuration_value')
		                    ->from('configuration')
		                    ->where('configuration_key', 'GAMBIO_SHOP_KEY')
		                    ->get();
		$shopKey = $query->row();
		if(isset($shopKey->configuration_value))
		{
			return $shopKey->configuration_value;
		}
		
		return null;
	}
	
	
	/**
	 * Returns the version history from database.
	 *
	 * @return mixed
	 */
	protected function getVersionHistory()
	{
		$dbData = $this->db->select()->from('version_history')->get()->result_array();
		
		return $dbData;
	}
	
	
	/**
	 * Returns the version receipts from filesystem.
	 *
	 * @return mixed
	 */
	protected function getVersionReceipts()
	{
		return scandir(DIR_FS_CATALOG . 'version_info');
	}
	
	
	/**
	 * Return true on success, otherwise false.
	 *
	 * @param string $dir Path of the directory.
	 *
	 * @return bool
	 */
	protected function createDirectory($dir)
	{
		if(substr($dir, -1) === DIRECTORY_SEPARATOR)
		{
			$dir = substr($dir, 0, strlen($dir) - 1);
		}
		
		if(is_dir($dir))
		{
			return true;
		}
		elseif(file_exists($dir))
		{
			return false;
		}
		
		$parentDirectory = substr($dir, 0, strrpos($dir, DIRECTORY_SEPARATOR));
		if(!is_dir($parentDirectory))
		{
			$dirCreated = $this->createDirectory($parentDirectory);
			if(!$dirCreated)
			{
				return false;
			}
		}
		
		$dirCreated = @mkdir($dir, 0777, true);
		@chmod($dir, 0777);
		
		return $dirCreated;
	}
	
	
	/**
	 * Downloads an external file with curl.
	 */
	protected function downloadExternalFile($sourcePath, $destinationPath)
	{
		@set_time_limit(0); // unlimited max execution time
		$destination = fopen($destinationPath, 'w');
		$curl        = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_FILE    => $destination,
			CURLOPT_TIMEOUT => 300, // set this to 8 hours so we dont timeout on big files
			CURLOPT_URL     => $sourcePath,
		]);
		curl_exec($curl);
		curl_close($curl);
		@fclose($destination);
	}
	
	
	/**
	 * Checks if web und ftp user are different.
	 *
	 * @param int $ftpUser Contains the value of the ftp user after execution.
	 * @param int $webUser Contains the value of the web user after execution.
	 *
	 * @return bool Return true if they are different, otherwise false.
	 */
	public function checkFileOwnerCompatibility(&$ftpUser = 0, &$webUser = 0)
	{
		$ftpUser = @fileowner(DIR_FS_CATALOG . 'index.php');
		
		$checkFilePath = DIR_FS_CATALOG . 'cache/AutoUpdater_FileOwnerCheck';
		@unlink($checkFilePath);
		$checkFile = @fopen($checkFilePath, 'w');
		@fwrite($checkFile, 'permission test');
		@fclose($checkFile);
		$webUser = @fileowner($checkFilePath);
		@unlink($checkFilePath);
		
		if($ftpUser != 0 && $ftpUser === $webUser)
		{
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Determines with file permission is correct.
	 *
	 * @return int
	 */
	protected static function determineCorrectFilePermission()
	{
		if(is_writeable(DIR_FS_CATALOG . 'export'))
		{
			$file = @fopen(DIR_FS_CATALOG . 'export/permission-test.php', 'w');
			@fwrite($file, '<?php echo "test ok";');
			@fclose($file);
			@chmod(DIR_FS_CATALOG . 'export/permission-test.php', 0777);
			
			$file = @fopen(DIR_FS_CATALOG . 'export/permission-test2.php', 'w');
			@fclose($file);
			@chmod(DIR_FS_CATALOG . 'export/permission-test2.php', 0755);
			
			$curlHandle = @curl_init();
			@curl_setopt_array($curlHandle, array(
				CURLOPT_URL            => HTTP_SERVER . DIR_WS_CATALOG . 'export/permission-test.php',
				CURLOPT_CONNECTTIMEOUT => 10,
				CURLOPT_RETURNTRANSFER => true,
			));
			$response = @curl_exec($curlHandle);
			$header   = @curl_getinfo($curlHandle);
			@curl_close($curlHandle);
			
			$_755IsAccessable = isset($header['http_code']) && $header['http_code'] !== 200 && $response === 'test ok';
			$_755IsWritable   = is_writeable(DIR_FS_CATALOG . 'export/permission-test2.php');
			
			@unlink(DIR_FS_CATALOG . 'export/permission-test.php');
			@unlink(DIR_FS_CATALOG . 'export/permission-test2.php');
			
			if($_755IsAccessable && $_755IsWritable)
			{
				return 0755;
			}
		}
		
		return 0777;
	}
}