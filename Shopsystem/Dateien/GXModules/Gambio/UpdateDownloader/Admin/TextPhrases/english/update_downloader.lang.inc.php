<?php
/* --------------------------------------------------------------
	update_downloader.lang.inc.php 2018-08-07
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'ACTION_DETAILS'                => 'Show details',
	'ACTION_DOWNLOAD_UPDATE'        => 'Download %s package',
	'ACTION_DOWNLOAD_UPDATES'       => 'Download %s packages',
	'ACTION_DOWNLOADING_UPDATES'    => 'Downloading selected updates ...',
	'DATA_PRIVACY_AGREE_BUTTON'     => 'Activate update search',
	'DATA_PRIVACY_DISAGREE_BUTTON'  => 'Deactivate update search',
	'DATA_PRIVACY_TEXT'             => '<p>If you want to use the AutoUpdate for downloading updates automatically, technical shop and server information will be transferred to and processed on a Gambio server, to detect compatible and matching updates for your shop. No personal or trade related data like products, sales, customers, etc. will be transmitted. There is no need for an active support contract with the Gambio GmbH for using the AutoUpdater.</p><p>Of course your data will not be given to third parties and only be used for the purposes of service delivery.</p><p>The AutoUpdater can only be used after accepting the processing of data. Further transmissions of data can be deactivated by clicking the "Deactivate update search" button.</p><p>For further information see our <a href="https://www.gambio.com/data-protection.html" target="_blank">privacy policy</a>.</p>',
	'DATA_PRIVACY_TITLE'            => 'Information about data processing',
	'ERROR'                         => 'The following error occured while update "<span></span>"',
	'ERROR_BACKUP_CREATION_FAILED'  => 'Could not create backup for version "%s".',
	'ERROR_BACKUP_RESTORE_FAILED'   => 'Could not restore backup for directory "%s". Please copy the content of this directory into the main directory of the shop to restore the backup manually.',
	'ERROR_COPY_FAILED'             => 'Could not copy file "%s" to "%s".',
	'ERROR_MD5_HASH'                => 'The hash of "%s" does not match the expected hash.',
	'ERROR_PERMISSON'               => 'Writing permissions for creating or changing "%s" are missing.',
	'ERROR_SUBTEXT_SOME_SUCCESSFUL' => 'After closing this modal you will be redirected to the Gambio Updater, because the following updates had been successfully downloaded',
	'ERROR_ZIP_CORRUPTED'           => 'The zip file of update "%s" seems to be corrupted and could not be unpacked. All update files will be downloaded separately.',
	'INFO_MODAL_TEXT'               => '<p>With the Gambio AutoUpdater you are able to download and integrate updates from Gambio into your shop easily. Afterwards you a able to install the updates with the Gambio Updater.</p><p>Please note that downloading and installing updates mean file and database changes in your shop.</p><p>We recommend to always make backups from your shop files and database.</p>',
	'INFO_MODAL_TITLE'              => 'Information about the AutoUpdater',
	'NO_UPDATE_SELECTED'            => 'There has been no update selected.',
	'NO_UPDATES'                    => 'There are no updates or modules available.',
	'NOTICE_TEXT'                   => 'After downloading the updates you will be automatically redirected to the Gambio Updater. Click on the icon for further information.',
	'PAGE_TITLE'                    => 'Gambio AutoUpdater',
	'UNEXPECTED_ERROR'              => 'An unexpected error accrued.',
	'UNINSTALLED_UPDATES_BUTTON'    => 'Open Gambio Updater',
	'UNINSTALLED_UPDATES_TEXT'      => '<p>We noticed some uninstalled Updates. Please check the Gambio Updater and make sure all updates are installed.</p><p><b>Only with all Updates installed we can verify new updates correctly!</b></p>',
	'UNINSTALLED_UPDATES_TITLE'     => 'Found uninstalled Updates',
	'UNRESTORED_BACKUP'             => 'Found a not restored backup of an available update.',
	'UNRESTORED_UPDATE_BACKUP'      => 'There is an not restored backup in "%s" for the update "%s".',
	'UPDATE_FAILED'                 => 'Update "%s" could not be unpacked into the shop.',
	'UPDATE_NOT_AVAILABLE'          => 'Update "%s" could not be (completely) downloaded.',
	'UPDATES_AVAILABLE'             => 'Available updates',
	'UPDATES_NOT_DOWNLOADED'        => 'Not all available update were downloaded.',
	'VERSION_FROM'                  => 'From version',
	'VERSION_TO'                    => 'to',
);