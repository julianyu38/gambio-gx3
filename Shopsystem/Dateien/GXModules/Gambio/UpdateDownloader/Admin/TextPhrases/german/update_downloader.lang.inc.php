<?php
/* --------------------------------------------------------------
	update_downloader.lang.inc.php 2018-08-08
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'ACTION_DETAILS'                => 'Details anzeigen',
	'ACTION_DOWNLOAD_UPDATE'        => '%s Paket herunterladen',
	'ACTION_DOWNLOAD_UPDATES'       => '%s Pakete herunterladen',
	'ACTION_DOWNLOADING_UPDATES'    => 'Ausgewählte Updates werden heruntergeladen ...',
	'DATA_PRIVACY_AGREE_BUTTON'     => 'Updatesuche aktivieren',
	'DATA_PRIVACY_DISAGREE_BUTTON'  => 'Updatesuche deaktivieren',
	'DATA_PRIVACY_TEXT'             => '<p>Wenn Sie den AutoUpdater zum automatischen Herunterladen von Updates nutzen möchten, werden technisch relevante Shop- und Server-Informationen an Gambio eigene Server übermittelt und dort verarbeitet, damit kompatible und passende Updates für Ihren Shop vorgeschlagen werden können. Es werden keine personen- oder handelsbezogenen Daten wie Artikel, Umsätze, Kunden, etc. übertragen. Es wird kein aktiver Supportvertrag mit der Gambio GmbH benötigt, um den AutoUpdater nutzen zu können.</p><p>Selbstverständlich werden wir die übertragenen Daten zu Ihrem Shop nicht an Dritte weitergeben und nur in dem Rahmen nutzen, der für die Erbringung der Leistung notwendig ist.</p><p>Der AutoUpdater kann nur genutzt werden, wenn Sie der Datenverarbeitung zustimmen. Eine weitere Übertragung kann jederzeit durch Klick auf den Button "Updatesuche deaktivieren" für die Zukunft deaktiviert werden.</p><p>Näheres erfahren Sie in unserer <a href="https://www.gambio.de/Datenschutzerklaerung.html" target="_blank">Datenschutzerklärung</a>.</p>',
	'DATA_PRIVACY_TITLE'            => 'Hinweis zur Datenverarbeitung',
	'ERROR'                         => 'Folgender Fehler ist bei Update "<span></span>" aufgetreten',
	'ERROR_BACKUP_CREATION_FAILED'  => 'Das Backup für Update "%s" konnte nicht erstellt werden.',
	'ERROR_BACKUP_RESTORE_FAILED'   => 'Das Backup im Ordner "%s" konnte nicht wiederhergestellt werden. Bitte kopieren Sie den Inhalt des Ordners in das Hauptverzeichnis des Shops um das Backup manuel wiederherzustellen.',
	'ERROR_COPY_FAILED'             => 'Die Datei "%s" konnte nicht nach "%s" kopiert werden.',
	'ERROR_MD5_HASH'                => 'Der MD5 Hash von "%s" stimmt nicht mit dem erwarteten Hash überein.',
	'ERROR_PERMISSON'               => 'Es fehlen Schreibrechte um die Datei bzw. den Ordner "%s" anzulegen bzw. zu verändern.',
	'ERROR_SUBTEXT_SOME_SUCCESSFUL' => 'Da die folgenden Updates erfolgreich heruntergeladen wurden, werden Sie nach dem Schließen des Modals zum Gambio Updater weitergeleitet',
	'ERROR_ZIP_CORRUPTED'           => 'Das Zip-Archiv des Updates "%s" scheint beschädigt zu sein und konnte nicht entpackt werden.',
	'INFO_MODAL_TEXT'               => '<p>Über den Gambio AutoUpdater können Sie einfach und bequem von Gambio bereitgestellte Updates herunterladen und automatisch in Ihren Onlineshop integrieren lassen. Im Anschluss des Herunterladens können Sie die Updates ganz einfach über den Gambio Updater installieren.</p><p>Bitte beachten Sie dabei, dass beim Vorgang des Herunterladens in Ihrem Shop Dateien verändert werden und das Ausführen des Gambio Updaters Änderungen an der Datenbank zur Folge haben kann.</p><p>Wir bitten Sie daher immer eine Sicherung Ihrer Dateien und Datenbank durchzuführen.</p>',
	'INFO_MODAL_TITLE'              => 'Information zum AutoUpdater',
	'NO_UPDATE_SELECTED'            => 'Es wurde kein Update ausgewählt.',
	'NO_UPDATES'                    => 'Aktuell stehen keine Updates oder Module zur Verfügung.',
	'NOTICE_TEXT'                   => 'Nach dem Herunterladen der Updates werden Sie automatisch zum Gambio Updater weitergeleitet. Klicken Sie auf das Icon für weitere Informationen.',
	'PAGE_TITLE'                    => 'Gambio AutoUpdater',
	'UNEXPECTED_ERROR'              => 'Es ist ein unerwarteter Fehler aufgetreten.',
	'UNINSTALLED_UPDATES_BUTTON'    => 'Gambio Updater öffnen',
	'UNINSTALLED_UPDATES_TEXT'      => '<p>Wir haben uninstallierte Updates erkannt. Überprüfen Sie bitte, den Gambio Updater und stellen Sie sicher, dass alle Updates installiert sind.</p><p><b>Erst wenn alle Updates installiert sind, kann zuverlässig nach neuen Updates gesucht werden!</b></p>',
	'UNINSTALLED_UPDATES_TITLE'     => 'Uninstallierte Updates gefunden',
	'UNRESTORED_BACKUP'             => 'Es wurde ein nicht wiederhergestelltes Backup für ein verfügbares Update gefunden.',
	'UNRESTORED_UPDATE_BACKUP'      => 'Es existiert in "%s" ein nicht wiederhergestelltes Backup für das Update "%s".',
	'UPDATE_FAILED'                 => 'Update "%s" konnte nicht in den Shop entpackt werden.',
	'UPDATE_NOT_AVAILABLE'          => 'Update "%s" konnte nicht (vollständig) heruntergeladen werden.',
	'UPDATES_AVAILABLE'             => 'Verfügbare Updates',
	'UPDATES_NOT_DOWNLOADED'        => 'Es wurden nicht alle verfügbaren Updates heruntergeladen.',
	'VERSION_FROM'                  => 'Von Version',
	'VERSION_TO'                    => 'auf',
);