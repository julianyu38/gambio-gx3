<?php

/* --------------------------------------------------------------
   twofactorauth.lang.inc.php 2018-01-12
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
    'title' => 'Two Factor Authentication',
    'description' => 'Add an additional layer of security to your customer accounts. By activating the two factor authentication, the customers need an one-time-password to log in.',
];

