<?php

/* --------------------------------------------------------------
   AuthConfigurationStorageEntryNotFoundException.inc.php 2018-01-10
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class representing an exception that is thrown when an entry is not found
 */
class AuthConfigurationStorageEntryNotFoundException extends Exception
{
}