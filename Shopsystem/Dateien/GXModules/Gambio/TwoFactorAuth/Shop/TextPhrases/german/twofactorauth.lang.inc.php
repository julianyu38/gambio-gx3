<?php

/* --------------------------------------------------------------
   twofactorauth.lang.inc.php 2018-01-22
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
    'NAVBAR_TITLE_1_ACCOUNT_EDIT' => 'Zwei-Faktor-Authentifizierung',
    'configure' => 'Zwei-Faktor-Authentifizierung konfigurieren',
    'deactivated' => 'Die Zwei-Faktor-Authentifizierung wurde für Ihr Konto deaktiviert',
    'activated' => 'Die Zwei-Faktor-Authentifizierung wurde für Ihr Konto aktiviert',
    'token_invalid' => 'Der eingegebene Code ist nicht gültig. Bitte versuchen Sie es noch einmal',
    'edit_content_text' => '<p>Die Zwei-Faktor-Authentifizierung ist für Ihr Konto aktiviert. Wir empfehlen Ihnen diese Einstellung nicht zu deaktivieren.</p>',
    'deactivate' => 'Deaktivieren',
    'setup' => 'Einrichten',
    'continue' => 'Weiter',
    'back' => 'Zurück',
    'finalize' => 'Einrichtung abschließen',
    'step1_text' => '<p>Mit der Zwei-Faktor-Authentifizierung schützen Sie Ihr Benutzerkonto zusätzlich mit einem Einmal-Passwort, das von einer Authentifizierungs-App auf Ihrem Smartphone generiert wird. Sie können dafür die kostenlose App "Google Authenticator" für <a href="https://itunes.apple.com/us/app/google-authenticator/id388497605" target="_blank">iOS</a> oder <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">Android</a> benutzen.</p><p>Wenn Sie die Zwei-Faktor-Authentifizierung nutzen möchten, können Sie über den nachfolgendem Button zum Einrichtungsassistenten gelangen, der Sie Schritt für Schritt durch die Einrichtung leiten wird.</p>',
    'step2_text' => '<p>Bitte scannen Sie den nachfolgenden QR-Code mit der Authentifizierungs-App ein.</p>',
    'step3_text' => '<p>Bitte geben Sie den 6-stelligen Code ein, der Ihnen in der App angezeigt wird.</p>',
    'login_token_prompt' => '<p>Bitte geben Sie den 6-stelligen Zwei-Faktor-Authentifizierungscode ein.</p>',
    'confirm' => 'Bestätigen'
];

