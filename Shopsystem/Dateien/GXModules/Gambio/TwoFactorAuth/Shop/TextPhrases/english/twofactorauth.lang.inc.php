<?php

/* --------------------------------------------------------------
   twofactorauth.lang.inc.php 2018-01-12
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
    'title' => 'Two Factor Authentication',
    'configure' => 'Configure Two Factor Authentication',
    'deactivated' => 'Two Factor Authentication has been deactivated for your account',
    'activated' => 'Two Factor Authentication has been activated for your account',
    'token_invalid' => 'The provided code is invalid. Please try again',
    'edit_content_text' => '<p>Two Factor Authentication is activated for your account. We do not recommend to deactivate this setting.</p>',
    'deactivate' => 'Deactivate',
    'setup' => 'Setup',
    'continue' => 'Continue',
    'back' => 'Back',
    'finalize' => 'Finalize the setup',
    'step1_text' => '<p>Secure your account using the Two Factor Authentication which adds an additional layer of security to your login. You need a smartphone with a camera and the app "Google Authenticator" for <a href="https://itunes.apple.com/us/app/google-authenticator/id388497605" target="_blank">iOS</a> or <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">Android</a>, which is free.</p><p>To setup the Two Factor Authentication, please click on the button.</p>',
    'step2_text' => '<p>Please scan the following QR code with your smartphone using the authenticator app.</p>',
    'step3_text' => '<p>Please type the 6-digit code, that is shown in the app.</p>',
    'login_token_prompt' => '<p>Please type in the Two Factor Authentication token.</p>',
    'confirm' => 'Confirm'
];

