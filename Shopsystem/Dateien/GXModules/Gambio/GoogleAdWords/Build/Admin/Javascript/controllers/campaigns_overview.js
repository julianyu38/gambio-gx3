'use strict';

/* --------------------------------------------------------------
 campaigns_overview.js 2017-12-14
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gxmodules.controllers.module('campaigns_overview', ['modal'], function (data) {
	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this);

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Campaigns Overview
  *
  * @type {*|jQuery|HTMLElement}
  */
	var $campaignsOverview = $('.campaigns-overview');

	/**
  * Final Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Handler for the account modal, that will be displayed if the user is not connected with Google AdWords
  *
  * @param {object} event jQuery event object contains information of the event.
  */
	function _showAccountModal(event) {
		if (event !== undefined) {
			event.preventDefault();
		}

		$('.adwords-account.modal').modal('show');
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		if (options.showAccountForm) {
			_showAccountModal();
			$campaignsOverview.on('click', 'td', _showAccountModal);
		}

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvY29udHJvbGxlcnMvY2FtcGFpZ25zX292ZXJ2aWV3LmpzIl0sIm5hbWVzIjpbImd4bW9kdWxlcyIsImNvbnRyb2xsZXJzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwiJGNhbXBhaWduc092ZXJ2aWV3Iiwib3B0aW9ucyIsImV4dGVuZCIsIl9zaG93QWNjb3VudE1vZGFsIiwiZXZlbnQiLCJ1bmRlZmluZWQiLCJwcmV2ZW50RGVmYXVsdCIsIm1vZGFsIiwiaW5pdCIsImRvbmUiLCJzaG93QWNjb3VudEZvcm0iLCJvbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxVQUFVQyxXQUFWLENBQXNCQyxNQUF0QixDQUNDLG9CQURELEVBR0MsQ0FDQyxPQURELENBSEQsRUFPQyxVQUFTQyxJQUFULEVBQWU7QUFDZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVcsRUFBakI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMscUJBQXFCRixFQUFFLHFCQUFGLENBQTNCOztBQUVBOzs7OztBQUtBLEtBQU1HLFVBQVVILEVBQUVJLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkgsUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsVUFBU1EsaUJBQVQsQ0FBMkJDLEtBQTNCLEVBQWtDO0FBQ2pDLE1BQUlBLFVBQVVDLFNBQWQsRUFBeUI7QUFDeEJELFNBQU1FLGNBQU47QUFDQTs7QUFFRFIsSUFBRSx3QkFBRixFQUE0QlMsS0FBNUIsQ0FBa0MsTUFBbEM7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUFaLFFBQU9hLElBQVAsR0FBYyxVQUFDQyxJQUFELEVBQVU7QUFDdkIsTUFBSVIsUUFBUVMsZUFBWixFQUE2QjtBQUM1QlA7QUFDQUgsc0JBQW1CVyxFQUFuQixDQUFzQixPQUF0QixFQUErQixJQUEvQixFQUFxQ1IsaUJBQXJDO0FBQ0E7O0FBRURNO0FBQ0EsRUFQRDs7QUFTQSxRQUFPZCxNQUFQO0FBQ0EsQ0FoRkYiLCJmaWxlIjoiQWRtaW4vSmF2YXNjcmlwdC9jb250cm9sbGVycy9jYW1wYWlnbnNfb3ZlcnZpZXcuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gY2FtcGFpZ25zX292ZXJ2aWV3LmpzIDIwMTctMTItMTRcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG5neG1vZHVsZXMuY29udHJvbGxlcnMubW9kdWxlKFxyXG5cdCdjYW1wYWlnbnNfb3ZlcnZpZXcnLFxyXG5cdFxyXG5cdFtcclxuXHRcdCdtb2RhbCdcclxuXHRdLFxyXG5cdFxyXG5cdGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdCd1c2Ugc3RyaWN0JztcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBWQVJJQUJMRVMgREVGSU5JVElPTlxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUge2pRdWVyeX1cclxuXHRcdCAqL1xyXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIERlZmF1bHQgT3B0aW9uc1xyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IGRlZmF1bHRzID0ge307XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogQ2FtcGFpZ25zIE92ZXJ2aWV3XHJcblx0XHQgKlxyXG5cdFx0ICogQHR5cGUgeyp8alF1ZXJ5fEhUTUxFbGVtZW50fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCAkY2FtcGFpZ25zT3ZlcnZpZXcgPSAkKCcuY2FtcGFpZ25zLW92ZXJ2aWV3Jyk7XHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRmluYWwgT3B0aW9uc1xyXG5cdFx0ICpcclxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xyXG5cdFx0XHJcblx0XHQvKipcclxuXHRcdCAqIE1vZHVsZSBPYmplY3RcclxuXHRcdCAqXHJcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxyXG5cdFx0ICovXHJcblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHQvLyBFVkVOVCBIQU5ETEVSU1xyXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0XHRcclxuXHRcdC8qKlxyXG5cdFx0ICogSGFuZGxlciBmb3IgdGhlIGFjY291bnQgbW9kYWwsIHRoYXQgd2lsbCBiZSBkaXNwbGF5ZWQgaWYgdGhlIHVzZXIgaXMgbm90IGNvbm5lY3RlZCB3aXRoIEdvb2dsZSBBZFdvcmRzXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IGpRdWVyeSBldmVudCBvYmplY3QgY29udGFpbnMgaW5mb3JtYXRpb24gb2YgdGhlIGV2ZW50LlxyXG5cdFx0ICovXHJcblx0XHRmdW5jdGlvbiBfc2hvd0FjY291bnRNb2RhbChldmVudCkge1xyXG5cdFx0XHRpZiAoZXZlbnQgIT09IHVuZGVmaW5lZCkge1xyXG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdCQoJy5hZHdvcmRzLWFjY291bnQubW9kYWwnKS5tb2RhbCgnc2hvdycpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXHJcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRcdFxyXG5cdFx0bW9kdWxlLmluaXQgPSAoZG9uZSkgPT4ge1xyXG5cdFx0XHRpZiAob3B0aW9ucy5zaG93QWNjb3VudEZvcm0pIHtcclxuXHRcdFx0XHRfc2hvd0FjY291bnRNb2RhbCgpO1xyXG5cdFx0XHRcdCRjYW1wYWlnbnNPdmVydmlldy5vbignY2xpY2snLCAndGQnLCBfc2hvd0FjY291bnRNb2RhbCk7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdGRvbmUoKTtcclxuXHRcdH07XHJcblx0XHRcclxuXHRcdHJldHVybiBtb2R1bGU7XHJcblx0fSk7XHJcbiJdfQ==
