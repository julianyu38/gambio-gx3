'use strict';

/* --------------------------------------------------------------
 adwords_overview_columns.js 2017-11-21
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

jse.libs.adwords_overview_columns = jse.libs.adwords_overview_columns || {};

/**
 * ## Adwords Table Column Definitions
 *
 * This module defines the column definition of the adwords overview table. They can be overridden by other
 * scripts by modifying the array with new columns, or by replacing the property values of the contained
 * fields.
 *
 * @module Admin/Libs/adwords_overview_columns
 * @exports jse.libs.adwords_overview_columns
 */
(function (exports) {

	'use strict';

	exports.status = exports.status || {
		data: 'status',
		minWidth: '100px',
		widthFactor: 1,
		className: 'status',
		searchable: false,
		render: function render(data, type, full, meta) {
			return '<input type="checkbox" ' + (data ? "checked" : "") + ' class="campaign-status-checkbox" data-id="' + full.DT_RowData.id + '" />';
		}
	};

	exports.name = exports.name || {
		data: 'name',
		minWidth: '150px',
		widthFactor: 1,
		render: function render(data, type, full, meta) {
			return '<span title="' + full.name + '">' + data + '</span>';
		}
	};

	exports.dailyBudget = exports.dailyBudget || {
		data: 'dailyBudget',
		minWidth: '100px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			return '<span title="' + full.dailyBudget + '" data-budget="' + full.DT_RowData.dailyBudget + '" data-budget-html="' + data + '" data-id="' + full.DT_RowData.id + '" class="daily-budget">' + data + '</span>';
		}
	};

	exports.clicks = exports.clicks || {
		data: 'clicks',
		minWidth: '100px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			return '<span title="' + full.clicks + '">' + data + '</span>';
		}
	};

	exports.impressions = exports.impressions || {
		data: 'impressions',
		minWidth: '125px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			return '<span title="' + full.impressions + '">' + data + '</span>';
		}
	};

	exports.clickThroughRate = exports.clickThroughRate || {
		data: 'clickThroughRate',
		minWidth: '100px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			return '<span title="' + full.clickThroughRate + '">' + data + '</span>';
		}
	};

	exports.costPerClick = exports.costPerClick || {
		data: 'costPerClick',
		minWidth: '100px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			return '<span title="' + full.costPerClick + '">' + data + '</span>';
		}
	};

	exports.costs = exports.costs || {
		data: 'costs',
		minWidth: '100px',
		widthFactor: 1,
		className: 'numeric',
		render: function render(data, type, full, meta) {
			return '<span title="' + full.costs + '">' + data + '</span>';
		}
	};

	exports.actions = exports.actions || {
		data: null,
		minWidth: '350px',
		widthFactor: 1,
		className: 'actions',
		orderable: false,
		searchable: false,
		render: function render(data, type, full, meta) {
			return '\t\t\t\t\t\n\t\t\t\t\t<div class="pull-left"></div>\n\t\t\t\t\t<div class="pull-right action-list visible-on-hover">\n\t\t\t\t\t\t<i class="fa fa-pencil row-edit" title="' + jse.core.lang.translate('edit', 'buttons') + '"></i>\n\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\tclass="btn btn-default btn-save"></button>\n\t\t\t\t\t\t<button type="button"\n\t\t\t\t\t\t\t\tclass="btn btn-default btn-cancel"></button>\n\t\t\t\t\t</div>\n\t\t\t\t';
		}
	};
})(jse.libs.adwords_overview_columns);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvbGlicy9hZHdvcmRzX292ZXJ2aWV3X2NvbHVtbnMuanMiXSwibmFtZXMiOlsianNlIiwibGlicyIsImFkd29yZHNfb3ZlcnZpZXdfY29sdW1ucyIsImV4cG9ydHMiLCJzdGF0dXMiLCJkYXRhIiwibWluV2lkdGgiLCJ3aWR0aEZhY3RvciIsImNsYXNzTmFtZSIsInNlYXJjaGFibGUiLCJyZW5kZXIiLCJ0eXBlIiwiZnVsbCIsIm1ldGEiLCJEVF9Sb3dEYXRhIiwiaWQiLCJuYW1lIiwiZGFpbHlCdWRnZXQiLCJjbGlja3MiLCJpbXByZXNzaW9ucyIsImNsaWNrVGhyb3VnaFJhdGUiLCJjb3N0UGVyQ2xpY2siLCJjb3N0cyIsImFjdGlvbnMiLCJvcmRlcmFibGUiLCJjb3JlIiwibGFuZyIsInRyYW5zbGF0ZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxJQUFJQyxJQUFKLENBQVNDLHdCQUFULEdBQW9DRixJQUFJQyxJQUFKLENBQVNDLHdCQUFULElBQXFDLEVBQXpFOztBQUVBOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxVQUFTQyxPQUFULEVBQWtCOztBQUVsQjs7QUFFQUEsU0FBUUMsTUFBUixHQUFpQkQsUUFBUUMsTUFBUixJQUFrQjtBQUNsQ0MsUUFBTSxRQUQ0QjtBQUVsQ0MsWUFBVSxPQUZ3QjtBQUdsQ0MsZUFBYSxDQUhxQjtBQUlsQ0MsYUFBVyxRQUp1QjtBQUtsQ0MsY0FBWSxLQUxzQjtBQU1sQ0MsUUFOa0Msa0JBTTNCTCxJQU4yQixFQU1yQk0sSUFOcUIsRUFNZkMsSUFOZSxFQU1UQyxJQU5TLEVBTUg7QUFDOUIsdUNBQWtDUixPQUFPLFNBQVAsR0FBbUIsRUFBckQsb0RBQXNHTyxLQUFLRSxVQUFMLENBQWdCQyxFQUF0SDtBQUNBO0FBUmlDLEVBQW5DOztBQVdBWixTQUFRYSxJQUFSLEdBQWViLFFBQVFhLElBQVIsSUFBZ0I7QUFDOUJYLFFBQU0sTUFEd0I7QUFFOUJDLFlBQVUsT0FGb0I7QUFHOUJDLGVBQWEsQ0FIaUI7QUFJOUJHLFFBSjhCLGtCQUl2QkwsSUFKdUIsRUFJakJNLElBSmlCLEVBSVhDLElBSlcsRUFJTEMsSUFKSyxFQUlDO0FBQzlCLDRCQUF1QkQsS0FBS0ksSUFBNUIsVUFBcUNYLElBQXJDO0FBQ0E7QUFONkIsRUFBL0I7O0FBU0FGLFNBQVFjLFdBQVIsR0FBc0JkLFFBQVFjLFdBQVIsSUFBdUI7QUFDNUNaLFFBQU0sYUFEc0M7QUFFNUNDLFlBQVUsT0FGa0M7QUFHNUNDLGVBQWEsQ0FIK0I7QUFJNUNDLGFBQVcsU0FKaUM7QUFLNUNFLFFBTDRDLGtCQUtyQ0wsSUFMcUMsRUFLL0JNLElBTCtCLEVBS3pCQyxJQUx5QixFQUtuQkMsSUFMbUIsRUFLYjtBQUM5Qiw0QkFBdUJELEtBQUtLLFdBQTVCLHVCQUF5REwsS0FBS0UsVUFBTCxDQUFnQkcsV0FBekUsNEJBQTJHWixJQUEzRyxtQkFBNkhPLEtBQUtFLFVBQUwsQ0FBZ0JDLEVBQTdJLCtCQUF5S1YsSUFBeks7QUFDQTtBQVAyQyxFQUE3Qzs7QUFVQUYsU0FBUWUsTUFBUixHQUFpQmYsUUFBUWUsTUFBUixJQUFrQjtBQUNsQ2IsUUFBTSxRQUQ0QjtBQUVsQ0MsWUFBVSxPQUZ3QjtBQUdsQ0MsZUFBYSxDQUhxQjtBQUlsQ0MsYUFBVyxTQUp1QjtBQUtsQ0UsUUFMa0Msa0JBSzNCTCxJQUwyQixFQUtyQk0sSUFMcUIsRUFLZkMsSUFMZSxFQUtUQyxJQUxTLEVBS0g7QUFDOUIsNEJBQXVCRCxLQUFLTSxNQUE1QixVQUF1Q2IsSUFBdkM7QUFDQTtBQVBpQyxFQUFuQzs7QUFVQUYsU0FBUWdCLFdBQVIsR0FBc0JoQixRQUFRZ0IsV0FBUixJQUF1QjtBQUM1Q2QsUUFBTSxhQURzQztBQUU1Q0MsWUFBVSxPQUZrQztBQUc1Q0MsZUFBYSxDQUgrQjtBQUk1Q0MsYUFBVyxTQUppQztBQUs1Q0UsUUFMNEMsa0JBS3JDTCxJQUxxQyxFQUsvQk0sSUFMK0IsRUFLekJDLElBTHlCLEVBS25CQyxJQUxtQixFQUtiO0FBQzlCLDRCQUF1QkQsS0FBS08sV0FBNUIsVUFBNENkLElBQTVDO0FBQ0E7QUFQMkMsRUFBN0M7O0FBVUFGLFNBQVFpQixnQkFBUixHQUEyQmpCLFFBQVFpQixnQkFBUixJQUE0QjtBQUN0RGYsUUFBTSxrQkFEZ0Q7QUFFdERDLFlBQVUsT0FGNEM7QUFHdERDLGVBQWEsQ0FIeUM7QUFJdERDLGFBQVcsU0FKMkM7QUFLdERFLFFBTHNELGtCQUsvQ0wsSUFMK0MsRUFLekNNLElBTHlDLEVBS25DQyxJQUxtQyxFQUs3QkMsSUFMNkIsRUFLdkI7QUFDOUIsNEJBQXVCRCxLQUFLUSxnQkFBNUIsVUFBaURmLElBQWpEO0FBQ0E7QUFQcUQsRUFBdkQ7O0FBVUFGLFNBQVFrQixZQUFSLEdBQXVCbEIsUUFBUWtCLFlBQVIsSUFBd0I7QUFDOUNoQixRQUFNLGNBRHdDO0FBRTlDQyxZQUFVLE9BRm9DO0FBRzlDQyxlQUFhLENBSGlDO0FBSTlDQyxhQUFXLFNBSm1DO0FBSzlDRSxRQUw4QyxrQkFLdkNMLElBTHVDLEVBS2pDTSxJQUxpQyxFQUszQkMsSUFMMkIsRUFLckJDLElBTHFCLEVBS2Y7QUFDOUIsNEJBQXVCRCxLQUFLUyxZQUE1QixVQUE2Q2hCLElBQTdDO0FBQ0E7QUFQNkMsRUFBL0M7O0FBVUFGLFNBQVFtQixLQUFSLEdBQWdCbkIsUUFBUW1CLEtBQVIsSUFBaUI7QUFDaENqQixRQUFNLE9BRDBCO0FBRWhDQyxZQUFVLE9BRnNCO0FBR2hDQyxlQUFhLENBSG1CO0FBSWhDQyxhQUFXLFNBSnFCO0FBS2hDRSxRQUxnQyxrQkFLekJMLElBTHlCLEVBS25CTSxJQUxtQixFQUtiQyxJQUxhLEVBS1BDLElBTE8sRUFLRDtBQUM5Qiw0QkFBdUJELEtBQUtVLEtBQTVCLFVBQXNDakIsSUFBdEM7QUFDQTtBQVArQixFQUFqQzs7QUFVQUYsU0FBUW9CLE9BQVIsR0FBa0JwQixRQUFRb0IsT0FBUixJQUFtQjtBQUNwQ2xCLFFBQU0sSUFEOEI7QUFFcENDLFlBQVUsT0FGMEI7QUFHcENDLGVBQWEsQ0FIdUI7QUFJcENDLGFBQVcsU0FKeUI7QUFLcENnQixhQUFXLEtBTHlCO0FBTXBDZixjQUFZLEtBTndCO0FBT3BDQyxRQVBvQyxrQkFPN0JMLElBUDZCLEVBT3ZCTSxJQVB1QixFQU9qQkMsSUFQaUIsRUFPWEMsSUFQVyxFQU9MO0FBQzlCLHlMQUc2Q2IsSUFBSXlCLElBQUosQ0FBU0MsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE1BQXhCLEVBQWdDLFNBQWhDLENBSDdDO0FBVUE7QUFsQm1DLEVBQXJDO0FBb0JBLENBeEdELEVBd0dHM0IsSUFBSUMsSUFBSixDQUFTQyx3QkF4R1oiLCJmaWxlIjoiQWRtaW4vSmF2YXNjcmlwdC9saWJzL2Fkd29yZHNfb3ZlcnZpZXdfY29sdW1ucy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gYWR3b3Jkc19vdmVydmlld19jb2x1bW5zLmpzIDIwMTctMTEtMjFcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5qc2UubGlicy5hZHdvcmRzX292ZXJ2aWV3X2NvbHVtbnMgPSBqc2UubGlicy5hZHdvcmRzX292ZXJ2aWV3X2NvbHVtbnMgfHwge307XG5cbi8qKlxuICogIyMgQWR3b3JkcyBUYWJsZSBDb2x1bW4gRGVmaW5pdGlvbnNcbiAqXG4gKiBUaGlzIG1vZHVsZSBkZWZpbmVzIHRoZSBjb2x1bW4gZGVmaW5pdGlvbiBvZiB0aGUgYWR3b3JkcyBvdmVydmlldyB0YWJsZS4gVGhleSBjYW4gYmUgb3ZlcnJpZGRlbiBieSBvdGhlclxuICogc2NyaXB0cyBieSBtb2RpZnlpbmcgdGhlIGFycmF5IHdpdGggbmV3IGNvbHVtbnMsIG9yIGJ5IHJlcGxhY2luZyB0aGUgcHJvcGVydHkgdmFsdWVzIG9mIHRoZSBjb250YWluZWRcbiAqIGZpZWxkcy5cbiAqXG4gKiBAbW9kdWxlIEFkbWluL0xpYnMvYWR3b3Jkc19vdmVydmlld19jb2x1bW5zXG4gKiBAZXhwb3J0cyBqc2UubGlicy5hZHdvcmRzX292ZXJ2aWV3X2NvbHVtbnNcbiAqL1xuKGZ1bmN0aW9uKGV4cG9ydHMpIHtcblx0XG5cdCd1c2Ugc3RyaWN0Jztcblx0XG5cdGV4cG9ydHMuc3RhdHVzID0gZXhwb3J0cy5zdGF0dXMgfHwge1xuXHRcdGRhdGE6ICdzdGF0dXMnLFxuXHRcdG1pbldpZHRoOiAnMTAwcHgnLFxuXHRcdHdpZHRoRmFjdG9yOiAxLFxuXHRcdGNsYXNzTmFtZTogJ3N0YXR1cycsXG5cdFx0c2VhcmNoYWJsZTogZmFsc2UsXG5cdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcblx0XHRcdHJldHVybiBgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiICR7KGRhdGEgPyBcImNoZWNrZWRcIiA6IFwiXCIpfSBjbGFzcz1cImNhbXBhaWduLXN0YXR1cy1jaGVja2JveFwiIGRhdGEtaWQ9XCIke2Z1bGwuRFRfUm93RGF0YS5pZH1cIiAvPmBcblx0XHR9XG5cdH07XG5cdFxuXHRleHBvcnRzLm5hbWUgPSBleHBvcnRzLm5hbWUgfHwge1xuXHRcdGRhdGE6ICduYW1lJyxcblx0XHRtaW5XaWR0aDogJzE1MHB4Jyxcblx0XHR3aWR0aEZhY3RvcjogMSxcblx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xuXHRcdFx0cmV0dXJuIGA8c3BhbiB0aXRsZT1cIiR7ZnVsbC5uYW1lfVwiPiR7ZGF0YX08L3NwYW4+YFxuXHRcdH1cblx0fTtcblx0XG5cdGV4cG9ydHMuZGFpbHlCdWRnZXQgPSBleHBvcnRzLmRhaWx5QnVkZ2V0IHx8IHtcblx0XHRkYXRhOiAnZGFpbHlCdWRnZXQnLFxuXHRcdG1pbldpZHRoOiAnMTAwcHgnLFxuXHRcdHdpZHRoRmFjdG9yOiAxLFxuXHRcdGNsYXNzTmFtZTogJ251bWVyaWMnLFxuXHRcdHJlbmRlcihkYXRhLCB0eXBlLCBmdWxsLCBtZXRhKSB7XG5cdFx0XHRyZXR1cm4gYDxzcGFuIHRpdGxlPVwiJHtmdWxsLmRhaWx5QnVkZ2V0fVwiIGRhdGEtYnVkZ2V0PVwiJHtmdWxsLkRUX1Jvd0RhdGEuZGFpbHlCdWRnZXR9XCIgZGF0YS1idWRnZXQtaHRtbD1cIiR7ZGF0YX1cIiBkYXRhLWlkPVwiJHtmdWxsLkRUX1Jvd0RhdGEuaWR9XCIgY2xhc3M9XCJkYWlseS1idWRnZXRcIj4ke2RhdGF9PC9zcGFuPmBcblx0XHR9XG5cdH07XG5cdFxuXHRleHBvcnRzLmNsaWNrcyA9IGV4cG9ydHMuY2xpY2tzIHx8IHtcblx0XHRkYXRhOiAnY2xpY2tzJyxcblx0XHRtaW5XaWR0aDogJzEwMHB4Jyxcblx0XHR3aWR0aEZhY3RvcjogMSxcblx0XHRjbGFzc05hbWU6ICdudW1lcmljJyxcblx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xuXHRcdFx0cmV0dXJuIGA8c3BhbiB0aXRsZT1cIiR7ZnVsbC5jbGlja3N9XCI+JHtkYXRhfTwvc3Bhbj5gXG5cdFx0fVxuXHR9O1xuXHRcblx0ZXhwb3J0cy5pbXByZXNzaW9ucyA9IGV4cG9ydHMuaW1wcmVzc2lvbnMgfHwge1xuXHRcdGRhdGE6ICdpbXByZXNzaW9ucycsXG5cdFx0bWluV2lkdGg6ICcxMjVweCcsXG5cdFx0d2lkdGhGYWN0b3I6IDEsXG5cdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYycsXG5cdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcblx0XHRcdHJldHVybiBgPHNwYW4gdGl0bGU9XCIke2Z1bGwuaW1wcmVzc2lvbnN9XCI+JHtkYXRhfTwvc3Bhbj5gXG5cdFx0fVxuXHR9O1xuXHRcblx0ZXhwb3J0cy5jbGlja1Rocm91Z2hSYXRlID0gZXhwb3J0cy5jbGlja1Rocm91Z2hSYXRlIHx8IHtcblx0XHRkYXRhOiAnY2xpY2tUaHJvdWdoUmF0ZScsXG5cdFx0bWluV2lkdGg6ICcxMDBweCcsXG5cdFx0d2lkdGhGYWN0b3I6IDEsXG5cdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYycsXG5cdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcblx0XHRcdHJldHVybiBgPHNwYW4gdGl0bGU9XCIke2Z1bGwuY2xpY2tUaHJvdWdoUmF0ZX1cIj4ke2RhdGF9PC9zcGFuPmBcblx0XHR9XG5cdH07XG5cdFxuXHRleHBvcnRzLmNvc3RQZXJDbGljayA9IGV4cG9ydHMuY29zdFBlckNsaWNrIHx8IHtcblx0XHRkYXRhOiAnY29zdFBlckNsaWNrJyxcblx0XHRtaW5XaWR0aDogJzEwMHB4Jyxcblx0XHR3aWR0aEZhY3RvcjogMSxcblx0XHRjbGFzc05hbWU6ICdudW1lcmljJyxcblx0XHRyZW5kZXIoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xuXHRcdFx0cmV0dXJuIGA8c3BhbiB0aXRsZT1cIiR7ZnVsbC5jb3N0UGVyQ2xpY2t9XCI+JHtkYXRhfTwvc3Bhbj5gXG5cdFx0fVxuXHR9O1xuXHRcblx0ZXhwb3J0cy5jb3N0cyA9IGV4cG9ydHMuY29zdHMgfHwge1xuXHRcdGRhdGE6ICdjb3N0cycsXG5cdFx0bWluV2lkdGg6ICcxMDBweCcsXG5cdFx0d2lkdGhGYWN0b3I6IDEsXG5cdFx0Y2xhc3NOYW1lOiAnbnVtZXJpYycsXG5cdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcblx0XHRcdHJldHVybiBgPHNwYW4gdGl0bGU9XCIke2Z1bGwuY29zdHN9XCI+JHtkYXRhfTwvc3Bhbj5gXG5cdFx0fVxuXHR9O1xuXHRcblx0ZXhwb3J0cy5hY3Rpb25zID0gZXhwb3J0cy5hY3Rpb25zIHx8IHtcblx0XHRkYXRhOiBudWxsLFxuXHRcdG1pbldpZHRoOiAnMzUwcHgnLFxuXHRcdHdpZHRoRmFjdG9yOiAxLFxuXHRcdGNsYXNzTmFtZTogJ2FjdGlvbnMnLFxuXHRcdG9yZGVyYWJsZTogZmFsc2UsXG5cdFx0c2VhcmNoYWJsZTogZmFsc2UsXG5cdFx0cmVuZGVyKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcblx0XHRcdHJldHVybiBgXHRcdFx0XHRcdFxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJwdWxsLWxlZnRcIj48L2Rpdj5cblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicHVsbC1yaWdodCBhY3Rpb24tbGlzdCB2aXNpYmxlLW9uLWhvdmVyXCI+XG5cdFx0XHRcdFx0XHQ8aSBjbGFzcz1cImZhIGZhLXBlbmNpbCByb3ctZWRpdFwiIHRpdGxlPVwiJHtqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZWRpdCcsICdidXR0b25zJyl9XCI+PC9pPlxuXHRcdFx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdFx0XHRjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBidG4tc2F2ZVwiPjwvYnV0dG9uPlxuXHRcdFx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdFx0XHRjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBidG4tY2FuY2VsXCI+PC9idXR0b24+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdGA7XG5cdFx0fVxuXHR9O1xufSkoanNlLmxpYnMuYWR3b3Jkc19vdmVydmlld19jb2x1bW5zKTsgIl19
