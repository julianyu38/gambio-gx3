'use strict';

/* --------------------------------------------------------------
 init.js 2017-12-06
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Invoices Table Controller
 *
 * This controller initializes the main invoices table with a new jQuery DataTables instance.
 */
gxmodules.controllers.module('init', [jse.source + '/vendor/datatables/jquery.dataTables.min.css', jse.source + '/vendor/datatables/jquery.dataTables.min.js', jse.source + '/vendor/momentjs/moment.min.js', gxmodules.source + '/libs/adwords_overview_columns', 'datatable', 'modal', 'xhr'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------

	/**
  * Module Selector
  *
  * @type {jQuery}
  */

	var $this = $(this).find('.campaigns-overview-table');

	/**
  * Default Options
  *
  * @type {object}
  */
	var defaults = {
		getTotalsUrl: 'admin.php?do=AdWordsCampaignsOverviewAjax/getTotals',
		getTotalsWithRangeUrl: 'admin.php?do=AdWordsCampaignsOverviewAjax/getTotalsForRange&range=',
		updateDailyBudgetUrl: 'admin.php?do=AdWordsCampaignsOverviewAjax/updateDailyBudget',
		updateStatusUrl: 'admin.php?do=AdWordsCampaignsOverviewAjax/updateStatus'
	};

	var dateRangePickerSelector = '.daterangepicker-helper';

	var dateRangeSelector = '#adwords-date-range';

	/**
  * Date range picker element
  *
  * @type {jQuery}
  */
	var $dateRangePicker = $this.find(dateRangePickerSelector);

	/**
  * Final Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Instance
  *
  * @type {Object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// FUNCTIONS
	// ------------------------------------------------------------------------

	/**
  * Get Initial Table Order
  *
  * @param {Object} parameters Contains the URL parameters.
  * @param {Object} columns Contains the column definitions.
  *
  * @return {Array[]}
  */
	function _getOrder(parameters, columns) {
		var index = 1; // Order by name column by default.
		var direction = 'asc'; // Order ASC by default. 

		// Apply initial table sort. 
		if (parameters.sort) {
			direction = parameters.sort.charAt(0) === '-' ? 'desc' : 'asc';
			var columnName = parameters.sort.slice(1);

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = columns[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var column = _step.value;

					if (column.name === columnName) {
						index = columns.indexOf(column);
						break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		} else if (data.activeColumns.indexOf('name') > -1) {
			// Order by name if possible.
			index = data.activeColumns.indexOf('name') - 1;
		}

		return [[index, direction]];
	}

	/**
  * Load table data.
  */
	function _loadDataTable(value) {
		var columns = jse.libs.datatable.prepareColumns($this, jse.libs.adwords_overview_columns, data.activeColumns);
		var parameters = $.deparam(window.location.search.slice(1));
		var pageLength = 0;

		var query = undefined !== value ? '&dateRange=' + value : '';
		var url = jse.core.config.get('appUrl') + '/admin/admin.php?do=AdWordsCampaignsOverviewAjax/DataTable' + query;

		if (undefined !== value) {
			$this.DataTable().ajax.url(url).load();
			_loadTotals(value);
		} else {
			jse.libs.datatable.create($this, {
				autoWidth: false,
				dom: 't',
				pageLength: pageLength,
				displayStart: parseInt(parameters.page) ? (parseInt(parameters.page) - 1) * pageLength : 0,
				serverSide: true,
				language: jse.libs.datatable.getTranslations(jse.core.config.get('languageCode')),
				ajax: {
					url: url,
					type: 'POST',
					data: {
						pageToken: jse.core.config.get('pageToken')
					}
				},
				orderCellsTop: true,
				order: _getOrder(parameters, columns),
				searchCols: [],
				columns: columns
			});
		}
	}

	/**
  * Load totals table data.
  */
	function _loadTotals(value) {
		var query = undefined !== value ? '&dateRange=' + value : '';

		$.ajax({
			type: "GET",
			url: options.getTotalsUrl + '&pageToken=' + jse.core.config.get('pageToken') + query,
			success: function success(response) {
				response = JSON.parse(response);

				$this.find('.total-row .total-budget').removeClass('loading');

				if (response['success'] === true) {
					$this.find('.total-row .total-budget').text(response['data']['dailyBudget']);
					$this.find('.total-row .total-clicks').text(response['data']['clicks']);
					$this.find('.total-row .total-impressions').text(response['data']['impressions']);
					$this.find('.total-row .total-ctr').text(response['data']['clickThroughRate']);
					$this.find('.total-row .total-average-cpc').text(response['data']['costPerClick']);
					$this.find('.total-row .total-costs').text(response['data']['costs']);
				}

				return response['success'];
			},
			error: function error() {
				$this.find('.total-row .total-budget').removeClass('loading');

				return false;
			}
		});
	}

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------

	/**
  * Handles the click event of the edit icon.
  *
  * Makes the daily budget value editable.
  *
  * @param event
  */
	function _clickEdit(event) {
		var $dailyBudget = $(this).closest('tr').find('.daily-budget');
		var dailyBudgetValue = $dailyBudget.data('budget');

		// cancel editing for all rows
		$this.find('.btn-cancel').each(function () {
			_clickCancel.call(this);
		});

		$(this).hide();
		$(this).closest('tr').find('.btn-save').text(jse.core.lang.translate('save', 'buttons')).show();
		$(this).closest('tr').find('.btn-cancel').text(jse.core.lang.translate('cancel', 'buttons')).show();

		$dailyBudget.html('<input type="text" class="form-control daily-budget-input" value="' + dailyBudgetValue + '" />');

		$dailyBudget.find('.daily-budget-input').on('keyup', function (event) {
			if (event.keyCode === 13) {
				$(this).closest('tr').find('.btn-save').click();
			}
		});
	}

	/**
  * Update daily budget.
  *
  * @param event
  */
	function _clickSave(event) {
		var $dailyBudget = $(this).closest('tr').find('.daily-budget');
		var $dailyBudgetInput = $(this).closest('tr').find('.daily-budget-input');
		var campaignId = $(this).closest('tr').find('.daily-budget').data('id');
		var $saveButton = $(this);
		var $cancelButton = $(this).closest('tr').find('.btn-cancel');
		var $editIcon = $(this).closest('tr').find('.row-edit');

		$dailyBudget.addClass('loading');

		_clickCancel();

		$.ajax({
			type: "POST",
			url: options.updateDailyBudgetUrl,
			data: {
				id: campaignId,
				dailyBudget: $dailyBudgetInput.val(),
				pageToken: jse.core.config.get('pageToken')
			},
			success: function success(response) {
				response = JSON.parse(response);

				if (response['success'] === true) {
					$dailyBudget.html(response['data']['dailyBudgetHtml']);
					$dailyBudget.attr('title', response['data']['dailyBudgetHtml']);
					$dailyBudget.data('budget', response['data']['dailyBudget']);
					$dailyBudget.data('budget-html', response['data']['dailyBudgetHtml']);

					$saveButton.hide();
					$cancelButton.hide();
					$editIcon.show();

					$dailyBudget.removeClass('loading');
					$this.find('.total-row .total-budget').addClass('loading');

					_loadTotals();
				}

				return response['success'];
			},
			error: function error() {
				$dailyBudget.removeClass('loading');

				return false;
			}
		});
	}

	/**
  * Cancel edit mode for daily budget
  *
  * @param event
  */
	function _clickCancel(event) {
		var $dailyBudget = $(this).closest('tr').find('.daily-budget');
		var $cancelButton = $(this);
		var $saveButton = $(this).closest('tr').find('.btn-save');
		var $editIcon = $(this).closest('tr').find('.row-edit');

		$dailyBudget.html($dailyBudget.data('budget-html'));

		$saveButton.hide();
		$cancelButton.hide();
		$editIcon.show();
	}

	/**
  * Change campaign status.
  *
  * @param event
  */
	function _changeStatus(event) {
		$.ajax({
			type: "POST",
			url: options.updateStatusUrl,
			data: {
				id: $(this).data('id'),
				status: +$(this).is(':checked'), // cast to int
				pageToken: jse.core.config.get('pageToken')
			},
			success: function success(response) {
				return response['success'];
			},
			error: function error() {
				return false;
			}
		});
	}

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	module.init = function (done) {
		_loadDataTable();
		_loadTotals();

		$this.on('change', '.campaign-status-checkbox', _changeStatus);
		$this.on('change', dateRangePickerSelector, function (event) {
			$this.DataTable().search(event.target.value).draw();
			_loadTotals();
		});
		$(dateRangeSelector).on('change', function () {
			var value = $(this).val();
			_loadDataTable(value);
		});

		// Add table error handler.
		jse.libs.datatable.error($this, function (event, settings, techNote, message) {
			var title = 'DataTables ' + jse.core.lang.translate('error', 'messages');
			jse.libs.modal.showMessage(title, message);
		});

		// Add draw event handler. 
		$this.on('draw.dt', function () {
			$this.find('tbody').attr('data-gx-widget', 'switcher');
			gx.widgets.init($this); // Initialize the switcher widget.

			if (!options.showAccountForm) {
				$this.find('.row-edit').on('click', _clickEdit);
				$this.find('.btn-save').on('click', _clickSave);
				$this.find('.btn-cancel').on('click', _clickCancel);
			}
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvY29udHJvbGxlcnMvaW5pdC5qcyJdLCJuYW1lcyI6WyJneG1vZHVsZXMiLCJjb250cm9sbGVycyIsIm1vZHVsZSIsImpzZSIsInNvdXJjZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJmaW5kIiwiZGVmYXVsdHMiLCJnZXRUb3RhbHNVcmwiLCJnZXRUb3RhbHNXaXRoUmFuZ2VVcmwiLCJ1cGRhdGVEYWlseUJ1ZGdldFVybCIsInVwZGF0ZVN0YXR1c1VybCIsImRhdGVSYW5nZVBpY2tlclNlbGVjdG9yIiwiZGF0ZVJhbmdlU2VsZWN0b3IiLCIkZGF0ZVJhbmdlUGlja2VyIiwib3B0aW9ucyIsImV4dGVuZCIsIl9nZXRPcmRlciIsInBhcmFtZXRlcnMiLCJjb2x1bW5zIiwiaW5kZXgiLCJkaXJlY3Rpb24iLCJzb3J0IiwiY2hhckF0IiwiY29sdW1uTmFtZSIsInNsaWNlIiwiY29sdW1uIiwibmFtZSIsImluZGV4T2YiLCJhY3RpdmVDb2x1bW5zIiwiX2xvYWREYXRhVGFibGUiLCJ2YWx1ZSIsImxpYnMiLCJkYXRhdGFibGUiLCJwcmVwYXJlQ29sdW1ucyIsImFkd29yZHNfb3ZlcnZpZXdfY29sdW1ucyIsImRlcGFyYW0iLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInNlYXJjaCIsInBhZ2VMZW5ndGgiLCJxdWVyeSIsInVuZGVmaW5lZCIsInVybCIsImNvcmUiLCJjb25maWciLCJnZXQiLCJEYXRhVGFibGUiLCJhamF4IiwibG9hZCIsIl9sb2FkVG90YWxzIiwiY3JlYXRlIiwiYXV0b1dpZHRoIiwiZG9tIiwiZGlzcGxheVN0YXJ0IiwicGFyc2VJbnQiLCJwYWdlIiwic2VydmVyU2lkZSIsImxhbmd1YWdlIiwiZ2V0VHJhbnNsYXRpb25zIiwidHlwZSIsInBhZ2VUb2tlbiIsIm9yZGVyQ2VsbHNUb3AiLCJvcmRlciIsInNlYXJjaENvbHMiLCJzdWNjZXNzIiwicmVzcG9uc2UiLCJKU09OIiwicGFyc2UiLCJyZW1vdmVDbGFzcyIsInRleHQiLCJlcnJvciIsIl9jbGlja0VkaXQiLCJldmVudCIsIiRkYWlseUJ1ZGdldCIsImNsb3Nlc3QiLCJkYWlseUJ1ZGdldFZhbHVlIiwiZWFjaCIsIl9jbGlja0NhbmNlbCIsImNhbGwiLCJoaWRlIiwibGFuZyIsInRyYW5zbGF0ZSIsInNob3ciLCJodG1sIiwib24iLCJrZXlDb2RlIiwiY2xpY2siLCJfY2xpY2tTYXZlIiwiJGRhaWx5QnVkZ2V0SW5wdXQiLCJjYW1wYWlnbklkIiwiJHNhdmVCdXR0b24iLCIkY2FuY2VsQnV0dG9uIiwiJGVkaXRJY29uIiwiYWRkQ2xhc3MiLCJpZCIsImRhaWx5QnVkZ2V0IiwidmFsIiwiYXR0ciIsIl9jaGFuZ2VTdGF0dXMiLCJzdGF0dXMiLCJpcyIsImluaXQiLCJkb25lIiwidGFyZ2V0IiwiZHJhdyIsInNldHRpbmdzIiwidGVjaE5vdGUiLCJtZXNzYWdlIiwidGl0bGUiLCJtb2RhbCIsInNob3dNZXNzYWdlIiwiZ3giLCJ3aWRnZXRzIiwic2hvd0FjY291bnRGb3JtIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLFVBQVVDLFdBQVYsQ0FBc0JDLE1BQXRCLENBQ0MsTUFERCxFQUdDLENBQ0lDLElBQUlDLE1BRFIsbURBRUlELElBQUlDLE1BRlIsa0RBR0lELElBQUlDLE1BSFIscUNBSUlKLFVBQVVJLE1BSmQscUNBS0MsV0FMRCxFQU1DLE9BTkQsRUFPQyxLQVBELENBSEQsRUFhQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsRUFBUUMsSUFBUixDQUFhLDJCQUFiLENBQWQ7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsV0FBVztBQUNoQkMsZ0JBQWMscURBREU7QUFFaEJDLHlCQUF1QixvRUFGUDtBQUdoQkMsd0JBQXNCLDZEQUhOO0FBSWhCQyxtQkFBaUI7QUFKRCxFQUFqQjs7QUFPQSxLQUFNQywwQkFBMEIseUJBQWhDOztBQUVBLEtBQU1DLG9CQUFvQixxQkFBMUI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsbUJBQW1CVixNQUFNRSxJQUFOLENBQVdNLHVCQUFYLENBQXpCOztBQUVBOzs7OztBQUtBLEtBQU1HLFVBQVVWLEVBQUVXLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQlQsUUFBbkIsRUFBNkJKLElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ILFNBQVMsRUFBZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FBUUEsVUFBU2lCLFNBQVQsQ0FBbUJDLFVBQW5CLEVBQStCQyxPQUEvQixFQUF3QztBQUN2QyxNQUFJQyxRQUFRLENBQVosQ0FEdUMsQ0FDeEI7QUFDZixNQUFJQyxZQUFZLEtBQWhCLENBRnVDLENBRWhCOztBQUV2QjtBQUNBLE1BQUlILFdBQVdJLElBQWYsRUFBcUI7QUFDcEJELGVBQVlILFdBQVdJLElBQVgsQ0FBZ0JDLE1BQWhCLENBQXVCLENBQXZCLE1BQThCLEdBQTlCLEdBQW9DLE1BQXBDLEdBQTZDLEtBQXpEO0FBQ0EsT0FBTUMsYUFBYU4sV0FBV0ksSUFBWCxDQUFnQkcsS0FBaEIsQ0FBc0IsQ0FBdEIsQ0FBbkI7O0FBRm9CO0FBQUE7QUFBQTs7QUFBQTtBQUlwQix5QkFBbUJOLE9BQW5CLDhIQUE0QjtBQUFBLFNBQW5CTyxNQUFtQjs7QUFDM0IsU0FBSUEsT0FBT0MsSUFBUCxLQUFnQkgsVUFBcEIsRUFBZ0M7QUFDL0JKLGNBQVFELFFBQVFTLE9BQVIsQ0FBZ0JGLE1BQWhCLENBQVI7QUFDQTtBQUNBO0FBQ0Q7QUFUbUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVwQixHQVZELE1BVU8sSUFBSXZCLEtBQUswQixhQUFMLENBQW1CRCxPQUFuQixDQUEyQixNQUEzQixJQUFxQyxDQUFDLENBQTFDLEVBQTZDO0FBQUU7QUFDckRSLFdBQVFqQixLQUFLMEIsYUFBTCxDQUFtQkQsT0FBbkIsQ0FBMkIsTUFBM0IsSUFBcUMsQ0FBN0M7QUFDQTs7QUFFRCxTQUFPLENBQUMsQ0FBQ1IsS0FBRCxFQUFRQyxTQUFSLENBQUQsQ0FBUDtBQUNBOztBQUVEOzs7QUFHQSxVQUFTUyxjQUFULENBQXdCQyxLQUF4QixFQUErQjtBQUM5QixNQUFNWixVQUFVbEIsSUFBSStCLElBQUosQ0FBU0MsU0FBVCxDQUFtQkMsY0FBbkIsQ0FBa0M5QixLQUFsQyxFQUF5Q0gsSUFBSStCLElBQUosQ0FBU0csd0JBQWxELEVBQ2ZoQyxLQUFLMEIsYUFEVSxDQUFoQjtBQUVBLE1BQU1YLGFBQWFiLEVBQUUrQixPQUFGLENBQVVDLE9BQU9DLFFBQVAsQ0FBZ0JDLE1BQWhCLENBQXVCZCxLQUF2QixDQUE2QixDQUE3QixDQUFWLENBQW5CO0FBQ0EsTUFBTWUsYUFBYSxDQUFuQjs7QUFFQSxNQUFJQyxRQUFRQyxjQUFjWCxLQUFkLEdBQXNCLGdCQUFnQkEsS0FBdEMsR0FBOEMsRUFBMUQ7QUFDQSxNQUFJWSxNQUFNMUMsSUFBSTJDLElBQUosQ0FBU0MsTUFBVCxDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEIsSUFBZ0MsNERBQWhDLEdBQ1BMLEtBREg7O0FBR0EsTUFBSUMsY0FBY1gsS0FBbEIsRUFBeUI7QUFDeEIzQixTQUFNMkMsU0FBTixHQUFrQkMsSUFBbEIsQ0FBdUJMLEdBQXZCLENBQTJCQSxHQUEzQixFQUFnQ00sSUFBaEM7QUFDQUMsZUFBWW5CLEtBQVo7QUFDQSxHQUhELE1BR087QUFDTjlCLE9BQUkrQixJQUFKLENBQVNDLFNBQVQsQ0FBbUJrQixNQUFuQixDQUEwQi9DLEtBQTFCLEVBQWlDO0FBQ2hDZ0QsZUFBVyxLQURxQjtBQUVoQ0MsU0FBSyxHQUYyQjtBQUdoQ2IsMEJBSGdDO0FBSWhDYyxrQkFBY0MsU0FBU3JDLFdBQVdzQyxJQUFwQixJQUE0QixDQUFDRCxTQUFTckMsV0FBV3NDLElBQXBCLElBQTRCLENBQTdCLElBQWtDaEIsVUFBOUQsR0FBMkUsQ0FKekQ7QUFLaENpQixnQkFBWSxJQUxvQjtBQU1oQ0MsY0FBVXpELElBQUkrQixJQUFKLENBQVNDLFNBQVQsQ0FBbUIwQixlQUFuQixDQUFtQzFELElBQUkyQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCLENBQW5DLENBTnNCO0FBT2hDRSxVQUFNO0FBQ0xMLGFBREs7QUFFTGlCLFdBQU0sTUFGRDtBQUdMekQsV0FBTTtBQUNMMEQsaUJBQVc1RCxJQUFJMkMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUROO0FBSEQsS0FQMEI7QUFjaENnQixtQkFBZSxJQWRpQjtBQWVoQ0MsV0FBTzlDLFVBQVVDLFVBQVYsRUFBc0JDLE9BQXRCLENBZnlCO0FBZ0JoQzZDLGdCQUFZLEVBaEJvQjtBQWlCaEM3QztBQWpCZ0MsSUFBakM7QUFtQkE7QUFDRDs7QUFFRDs7O0FBR0EsVUFBUytCLFdBQVQsQ0FBcUJuQixLQUFyQixFQUE0QjtBQUMzQixNQUFJVSxRQUFRQyxjQUFjWCxLQUFkLEdBQXNCLGdCQUFnQkEsS0FBdEMsR0FBOEMsRUFBMUQ7O0FBRUExQixJQUFFMkMsSUFBRixDQUFPO0FBQ05ZLFNBQU0sS0FEQTtBQUVOakIsUUFBSzVCLFFBQVFQLFlBQVIsR0FBdUIsYUFBdkIsR0FBdUNQLElBQUkyQyxJQUFKLENBQVNDLE1BQVQsQ0FBZ0JDLEdBQWhCLENBQW9CLFdBQXBCLENBQXZDLEdBQTBFTCxLQUZ6RTtBQUdOd0IsWUFBUyxpQkFBU0MsUUFBVCxFQUFtQjtBQUMzQkEsZUFBV0MsS0FBS0MsS0FBTCxDQUFXRixRQUFYLENBQVg7O0FBRUE5RCxVQUFNRSxJQUFOLENBQVcsMEJBQVgsRUFBdUMrRCxXQUF2QyxDQUFtRCxTQUFuRDs7QUFFQSxRQUFJSCxTQUFTLFNBQVQsTUFBd0IsSUFBNUIsRUFBa0M7QUFDakM5RCxXQUFNRSxJQUFOLENBQVcsMEJBQVgsRUFBdUNnRSxJQUF2QyxDQUE0Q0osU0FBUyxNQUFULEVBQWlCLGFBQWpCLENBQTVDO0FBQ0E5RCxXQUFNRSxJQUFOLENBQVcsMEJBQVgsRUFBdUNnRSxJQUF2QyxDQUE0Q0osU0FBUyxNQUFULEVBQWlCLFFBQWpCLENBQTVDO0FBQ0E5RCxXQUFNRSxJQUFOLENBQVcsK0JBQVgsRUFBNENnRSxJQUE1QyxDQUFpREosU0FBUyxNQUFULEVBQWlCLGFBQWpCLENBQWpEO0FBQ0E5RCxXQUFNRSxJQUFOLENBQVcsdUJBQVgsRUFBb0NnRSxJQUFwQyxDQUF5Q0osU0FBUyxNQUFULEVBQWlCLGtCQUFqQixDQUF6QztBQUNBOUQsV0FBTUUsSUFBTixDQUFXLCtCQUFYLEVBQTRDZ0UsSUFBNUMsQ0FBaURKLFNBQVMsTUFBVCxFQUFpQixjQUFqQixDQUFqRDtBQUNBOUQsV0FBTUUsSUFBTixDQUFXLHlCQUFYLEVBQXNDZ0UsSUFBdEMsQ0FBMkNKLFNBQVMsTUFBVCxFQUFpQixPQUFqQixDQUEzQztBQUNBOztBQUVELFdBQU9BLFNBQVMsU0FBVCxDQUFQO0FBQ0EsSUFsQks7QUFtQk5LLFVBQU8saUJBQVc7QUFDakJuRSxVQUFNRSxJQUFOLENBQVcsMEJBQVgsRUFBdUMrRCxXQUF2QyxDQUFtRCxTQUFuRDs7QUFFQSxXQUFPLEtBQVA7QUFDQTtBQXZCSyxHQUFQO0FBeUJBOztBQUdEO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFVBQVNHLFVBQVQsQ0FBb0JDLEtBQXBCLEVBQTJCO0FBQzFCLE1BQU1DLGVBQWVyRSxFQUFFLElBQUYsRUFBUXNFLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JyRSxJQUF0QixDQUEyQixlQUEzQixDQUFyQjtBQUNBLE1BQU1zRSxtQkFBbUJGLGFBQWF2RSxJQUFiLENBQWtCLFFBQWxCLENBQXpCOztBQUVBO0FBQ0FDLFFBQU1FLElBQU4sQ0FBVyxhQUFYLEVBQTBCdUUsSUFBMUIsQ0FBK0IsWUFBVztBQUN6Q0MsZ0JBQWFDLElBQWIsQ0FBa0IsSUFBbEI7QUFDQSxHQUZEOztBQUlBMUUsSUFBRSxJQUFGLEVBQVEyRSxJQUFSO0FBQ0EzRSxJQUFFLElBQUYsRUFBUXNFLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JyRSxJQUF0QixDQUEyQixXQUEzQixFQUF3Q2dFLElBQXhDLENBQTZDckUsSUFBSTJDLElBQUosQ0FBU3FDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QixNQUF4QixFQUFnQyxTQUFoQyxDQUE3QyxFQUF5RkMsSUFBekY7QUFDQTlFLElBQUUsSUFBRixFQUFRc0UsT0FBUixDQUFnQixJQUFoQixFQUFzQnJFLElBQXRCLENBQTJCLGFBQTNCLEVBQTBDZ0UsSUFBMUMsQ0FBK0NyRSxJQUFJMkMsSUFBSixDQUFTcUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLFFBQXhCLEVBQWtDLFNBQWxDLENBQS9DLEVBQTZGQyxJQUE3Rjs7QUFFQVQsZUFBYVUsSUFBYixDQUFrQix1RUFBdUVSLGdCQUF2RSxHQUNmLE1BREg7O0FBR0FGLGVBQWFwRSxJQUFiLENBQWtCLHFCQUFsQixFQUF5QytFLEVBQXpDLENBQTRDLE9BQTVDLEVBQXFELFVBQVNaLEtBQVQsRUFBZ0I7QUFDcEUsT0FBSUEsTUFBTWEsT0FBTixLQUFrQixFQUF0QixFQUEwQjtBQUN6QmpGLE1BQUUsSUFBRixFQUFRc0UsT0FBUixDQUFnQixJQUFoQixFQUFzQnJFLElBQXRCLENBQTJCLFdBQTNCLEVBQXdDaUYsS0FBeEM7QUFDQTtBQUNELEdBSkQ7QUFLQTs7QUFFRDs7Ozs7QUFLQSxVQUFTQyxVQUFULENBQW9CZixLQUFwQixFQUEyQjtBQUMxQixNQUFNQyxlQUFlckUsRUFBRSxJQUFGLEVBQVFzRSxPQUFSLENBQWdCLElBQWhCLEVBQXNCckUsSUFBdEIsQ0FBMkIsZUFBM0IsQ0FBckI7QUFDQSxNQUFNbUYsb0JBQW9CcEYsRUFBRSxJQUFGLEVBQVFzRSxPQUFSLENBQWdCLElBQWhCLEVBQXNCckUsSUFBdEIsQ0FBMkIscUJBQTNCLENBQTFCO0FBQ0EsTUFBTW9GLGFBQWFyRixFQUFFLElBQUYsRUFBUXNFLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JyRSxJQUF0QixDQUEyQixlQUEzQixFQUE0Q0gsSUFBNUMsQ0FBaUQsSUFBakQsQ0FBbkI7QUFDQSxNQUFNd0YsY0FBY3RGLEVBQUUsSUFBRixDQUFwQjtBQUNBLE1BQU11RixnQkFBZ0J2RixFQUFFLElBQUYsRUFBUXNFLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JyRSxJQUF0QixDQUEyQixhQUEzQixDQUF0QjtBQUNBLE1BQU11RixZQUFZeEYsRUFBRSxJQUFGLEVBQVFzRSxPQUFSLENBQWdCLElBQWhCLEVBQXNCckUsSUFBdEIsQ0FBMkIsV0FBM0IsQ0FBbEI7O0FBRUFvRSxlQUFhb0IsUUFBYixDQUFzQixTQUF0Qjs7QUFFQWhCOztBQUVBekUsSUFBRTJDLElBQUYsQ0FBTztBQUNOWSxTQUFNLE1BREE7QUFFTmpCLFFBQUs1QixRQUFRTCxvQkFGUDtBQUdOUCxTQUFNO0FBQ0w0RixRQUFJTCxVQURDO0FBRUxNLGlCQUFhUCxrQkFBa0JRLEdBQWxCLEVBRlI7QUFHTHBDLGVBQVc1RCxJQUFJMkMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUhOLElBSEE7QUFRTm1CLFlBQVMsaUJBQVNDLFFBQVQsRUFBbUI7QUFDM0JBLGVBQVdDLEtBQUtDLEtBQUwsQ0FBV0YsUUFBWCxDQUFYOztBQUVBLFFBQUlBLFNBQVMsU0FBVCxNQUF3QixJQUE1QixFQUFrQztBQUNqQ1Esa0JBQWFVLElBQWIsQ0FBa0JsQixTQUFTLE1BQVQsRUFBaUIsaUJBQWpCLENBQWxCO0FBQ0FRLGtCQUFhd0IsSUFBYixDQUFrQixPQUFsQixFQUEyQmhDLFNBQVMsTUFBVCxFQUFpQixpQkFBakIsQ0FBM0I7QUFDQVEsa0JBQWF2RSxJQUFiLENBQWtCLFFBQWxCLEVBQTRCK0QsU0FBUyxNQUFULEVBQWlCLGFBQWpCLENBQTVCO0FBQ0FRLGtCQUFhdkUsSUFBYixDQUFrQixhQUFsQixFQUFpQytELFNBQVMsTUFBVCxFQUFpQixpQkFBakIsQ0FBakM7O0FBRUF5QixpQkFBWVgsSUFBWjtBQUNBWSxtQkFBY1osSUFBZDtBQUNBYSxlQUFVVixJQUFWOztBQUVBVCxrQkFBYUwsV0FBYixDQUF5QixTQUF6QjtBQUNBakUsV0FBTUUsSUFBTixDQUFXLDBCQUFYLEVBQXVDd0YsUUFBdkMsQ0FBZ0QsU0FBaEQ7O0FBRUE1QztBQUNBOztBQUVELFdBQU9nQixTQUFTLFNBQVQsQ0FBUDtBQUNBLElBNUJLO0FBNkJOSyxVQUFPLGlCQUFXO0FBQ2pCRyxpQkFBYUwsV0FBYixDQUF5QixTQUF6Qjs7QUFFQSxXQUFPLEtBQVA7QUFDQTtBQWpDSyxHQUFQO0FBbUNBOztBQUVEOzs7OztBQUtBLFVBQVNTLFlBQVQsQ0FBc0JMLEtBQXRCLEVBQTZCO0FBQzVCLE1BQU1DLGVBQWVyRSxFQUFFLElBQUYsRUFBUXNFLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JyRSxJQUF0QixDQUEyQixlQUEzQixDQUFyQjtBQUNBLE1BQU1zRixnQkFBZ0J2RixFQUFFLElBQUYsQ0FBdEI7QUFDQSxNQUFNc0YsY0FBY3RGLEVBQUUsSUFBRixFQUFRc0UsT0FBUixDQUFnQixJQUFoQixFQUFzQnJFLElBQXRCLENBQTJCLFdBQTNCLENBQXBCO0FBQ0EsTUFBTXVGLFlBQVl4RixFQUFFLElBQUYsRUFBUXNFLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JyRSxJQUF0QixDQUEyQixXQUEzQixDQUFsQjs7QUFFQW9FLGVBQWFVLElBQWIsQ0FBa0JWLGFBQWF2RSxJQUFiLENBQWtCLGFBQWxCLENBQWxCOztBQUVBd0YsY0FBWVgsSUFBWjtBQUNBWSxnQkFBY1osSUFBZDtBQUNBYSxZQUFVVixJQUFWO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsVUFBU2dCLGFBQVQsQ0FBdUIxQixLQUF2QixFQUE4QjtBQUM3QnBFLElBQUUyQyxJQUFGLENBQU87QUFDTlksU0FBTSxNQURBO0FBRU5qQixRQUFLNUIsUUFBUUosZUFGUDtBQUdOUixTQUFNO0FBQ0w0RixRQUFJMUYsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxJQUFiLENBREM7QUFFTGlHLFlBQVEsQ0FBQy9GLEVBQUUsSUFBRixFQUFRZ0csRUFBUixDQUFXLFVBQVgsQ0FGSixFQUU0QjtBQUNqQ3hDLGVBQVc1RCxJQUFJMkMsSUFBSixDQUFTQyxNQUFULENBQWdCQyxHQUFoQixDQUFvQixXQUFwQjtBQUhOLElBSEE7QUFRTm1CLFlBQVMsaUJBQVNDLFFBQVQsRUFBbUI7QUFDM0IsV0FBT0EsU0FBUyxTQUFULENBQVA7QUFDQSxJQVZLO0FBV05LLFVBQU8saUJBQVc7QUFDakIsV0FBTyxLQUFQO0FBQ0E7QUFiSyxHQUFQO0FBZUE7O0FBRUQ7QUFDQTtBQUNBOztBQUVBdkUsUUFBT3NHLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJ6RTtBQUNBb0I7O0FBRUE5QyxRQUFNaUYsRUFBTixDQUFTLFFBQVQsRUFBbUIsMkJBQW5CLEVBQWdEYyxhQUFoRDtBQUNBL0YsUUFBTWlGLEVBQU4sQ0FBUyxRQUFULEVBQW1CekUsdUJBQW5CLEVBQTRDLFVBQUM2RCxLQUFELEVBQVc7QUFDdERyRSxTQUFNMkMsU0FBTixHQUFrQlIsTUFBbEIsQ0FBeUJrQyxNQUFNK0IsTUFBTixDQUFhekUsS0FBdEMsRUFBNkMwRSxJQUE3QztBQUNBdkQ7QUFDQSxHQUhEO0FBSUE3QyxJQUFFUSxpQkFBRixFQUFxQndFLEVBQXJCLENBQXdCLFFBQXhCLEVBQWtDLFlBQVc7QUFDNUMsT0FBSXRELFFBQVExQixFQUFFLElBQUYsRUFBUTRGLEdBQVIsRUFBWjtBQUNBbkUsa0JBQWVDLEtBQWY7QUFDQSxHQUhEOztBQUtBO0FBQ0E5QixNQUFJK0IsSUFBSixDQUFTQyxTQUFULENBQW1Cc0MsS0FBbkIsQ0FBeUJuRSxLQUF6QixFQUFnQyxVQUFTcUUsS0FBVCxFQUFnQmlDLFFBQWhCLEVBQTBCQyxRQUExQixFQUFvQ0MsT0FBcEMsRUFBNkM7QUFDNUUsT0FBTUMsUUFBUSxnQkFBZ0I1RyxJQUFJMkMsSUFBSixDQUFTcUMsSUFBVCxDQUFjQyxTQUFkLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLENBQTlCO0FBQ0FqRixPQUFJK0IsSUFBSixDQUFTOEUsS0FBVCxDQUFlQyxXQUFmLENBQTJCRixLQUEzQixFQUFrQ0QsT0FBbEM7QUFDQSxHQUhEOztBQUtBO0FBQ0F4RyxRQUFNaUYsRUFBTixDQUFTLFNBQVQsRUFBb0IsWUFBTTtBQUN6QmpGLFNBQU1FLElBQU4sQ0FBVyxPQUFYLEVBQW9CNEYsSUFBcEIsQ0FBeUIsZ0JBQXpCLEVBQTJDLFVBQTNDO0FBQ0FjLE1BQUdDLE9BQUgsQ0FBV1gsSUFBWCxDQUFnQmxHLEtBQWhCLEVBRnlCLENBRUQ7O0FBRXhCLE9BQUksQ0FBQ1csUUFBUW1HLGVBQWIsRUFBOEI7QUFDN0I5RyxVQUFNRSxJQUFOLENBQVcsV0FBWCxFQUF3QitFLEVBQXhCLENBQTJCLE9BQTNCLEVBQW9DYixVQUFwQztBQUNBcEUsVUFBTUUsSUFBTixDQUFXLFdBQVgsRUFBd0IrRSxFQUF4QixDQUEyQixPQUEzQixFQUFvQ0csVUFBcEM7QUFDQXBGLFVBQU1FLElBQU4sQ0FBVyxhQUFYLEVBQTBCK0UsRUFBMUIsQ0FBNkIsT0FBN0IsRUFBc0NQLFlBQXRDO0FBQ0E7QUFDRCxHQVREOztBQVdBeUI7QUFDQSxFQWpDRDs7QUFtQ0EsUUFBT3ZHLE1BQVA7QUFDQSxDQXJWRiIsImZpbGUiOiJBZG1pbi9KYXZhc2NyaXB0L2NvbnRyb2xsZXJzL2luaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGluaXQuanMgMjAxNy0xMi0wNlxuIEdhbWJpbyBHbWJIXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbi8qKlxuICogSW52b2ljZXMgVGFibGUgQ29udHJvbGxlclxuICpcbiAqIFRoaXMgY29udHJvbGxlciBpbml0aWFsaXplcyB0aGUgbWFpbiBpbnZvaWNlcyB0YWJsZSB3aXRoIGEgbmV3IGpRdWVyeSBEYXRhVGFibGVzIGluc3RhbmNlLlxuICovXG5neG1vZHVsZXMuY29udHJvbGxlcnMubW9kdWxlKFxuXHQnaW5pdCcsXG5cdFxuXHRbXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmNzc2AsXG5cdFx0YCR7anNlLnNvdXJjZX0vdmVuZG9yL2RhdGF0YWJsZXMvanF1ZXJ5LmRhdGFUYWJsZXMubWluLmpzYCxcblx0XHRgJHtqc2Uuc291cmNlfS92ZW5kb3IvbW9tZW50anMvbW9tZW50Lm1pbi5qc2AsXG5cdFx0YCR7Z3htb2R1bGVzLnNvdXJjZX0vbGlicy9hZHdvcmRzX292ZXJ2aWV3X2NvbHVtbnNgLFxuXHRcdCdkYXRhdGFibGUnLFxuXHRcdCdtb2RhbCcsXG5cdFx0J3hocidcblx0XSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEVTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIFNlbGVjdG9yXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICR0aGlzID0gJCh0aGlzKS5maW5kKCcuY2FtcGFpZ25zLW92ZXJ2aWV3LXRhYmxlJyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGRlZmF1bHRzID0ge1xuXHRcdFx0Z2V0VG90YWxzVXJsOiAnYWRtaW4ucGhwP2RvPUFkV29yZHNDYW1wYWlnbnNPdmVydmlld0FqYXgvZ2V0VG90YWxzJyxcblx0XHRcdGdldFRvdGFsc1dpdGhSYW5nZVVybDogJ2FkbWluLnBocD9kbz1BZFdvcmRzQ2FtcGFpZ25zT3ZlcnZpZXdBamF4L2dldFRvdGFsc0ZvclJhbmdlJnJhbmdlPScsXG5cdFx0XHR1cGRhdGVEYWlseUJ1ZGdldFVybDogJ2FkbWluLnBocD9kbz1BZFdvcmRzQ2FtcGFpZ25zT3ZlcnZpZXdBamF4L3VwZGF0ZURhaWx5QnVkZ2V0Jyxcblx0XHRcdHVwZGF0ZVN0YXR1c1VybDogJ2FkbWluLnBocD9kbz1BZFdvcmRzQ2FtcGFpZ25zT3ZlcnZpZXdBamF4L3VwZGF0ZVN0YXR1cydcblx0XHR9O1xuXHRcdFxuXHRcdGNvbnN0IGRhdGVSYW5nZVBpY2tlclNlbGVjdG9yID0gJy5kYXRlcmFuZ2VwaWNrZXItaGVscGVyJztcblx0XHRcblx0XHRjb25zdCBkYXRlUmFuZ2VTZWxlY3RvciA9ICcjYWR3b3Jkcy1kYXRlLXJhbmdlJztcblx0XHRcblx0XHQvKipcblx0XHQgKiBEYXRlIHJhbmdlIHBpY2tlciBlbGVtZW50XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7alF1ZXJ5fVxuXHRcdCAqL1xuXHRcdGNvbnN0ICRkYXRlUmFuZ2VQaWNrZXIgPSAkdGhpcy5maW5kKGRhdGVSYW5nZVBpY2tlclNlbGVjdG9yKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaW5hbCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBJbnN0YW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge09iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBHZXQgSW5pdGlhbCBUYWJsZSBPcmRlclxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtZXRlcnMgQ29udGFpbnMgdGhlIFVSTCBwYXJhbWV0ZXJzLlxuXHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBjb2x1bW5zIENvbnRhaW5zIHRoZSBjb2x1bW4gZGVmaW5pdGlvbnMuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJuIHtBcnJheVtdfVxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9nZXRPcmRlcihwYXJhbWV0ZXJzLCBjb2x1bW5zKSB7XG5cdFx0XHRsZXQgaW5kZXggPSAxOyAvLyBPcmRlciBieSBuYW1lIGNvbHVtbiBieSBkZWZhdWx0LlxuXHRcdFx0bGV0IGRpcmVjdGlvbiA9ICdhc2MnOyAvLyBPcmRlciBBU0MgYnkgZGVmYXVsdC4gXG5cdFx0XHRcblx0XHRcdC8vIEFwcGx5IGluaXRpYWwgdGFibGUgc29ydC4gXG5cdFx0XHRpZiAocGFyYW1ldGVycy5zb3J0KSB7XG5cdFx0XHRcdGRpcmVjdGlvbiA9IHBhcmFtZXRlcnMuc29ydC5jaGFyQXQoMCkgPT09ICctJyA/ICdkZXNjJyA6ICdhc2MnO1xuXHRcdFx0XHRjb25zdCBjb2x1bW5OYW1lID0gcGFyYW1ldGVycy5zb3J0LnNsaWNlKDEpO1xuXHRcdFx0XHRcblx0XHRcdFx0Zm9yIChsZXQgY29sdW1uIG9mIGNvbHVtbnMpIHtcblx0XHRcdFx0XHRpZiAoY29sdW1uLm5hbWUgPT09IGNvbHVtbk5hbWUpIHtcblx0XHRcdFx0XHRcdGluZGV4ID0gY29sdW1ucy5pbmRleE9mKGNvbHVtbik7XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0gZWxzZSBpZiAoZGF0YS5hY3RpdmVDb2x1bW5zLmluZGV4T2YoJ25hbWUnKSA+IC0xKSB7IC8vIE9yZGVyIGJ5IG5hbWUgaWYgcG9zc2libGUuXG5cdFx0XHRcdGluZGV4ID0gZGF0YS5hY3RpdmVDb2x1bW5zLmluZGV4T2YoJ25hbWUnKSAtIDE7XG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdHJldHVybiBbW2luZGV4LCBkaXJlY3Rpb25dXTtcblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTG9hZCB0YWJsZSBkYXRhLlxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9sb2FkRGF0YVRhYmxlKHZhbHVlKSB7XG5cdFx0XHRjb25zdCBjb2x1bW5zID0ganNlLmxpYnMuZGF0YXRhYmxlLnByZXBhcmVDb2x1bW5zKCR0aGlzLCBqc2UubGlicy5hZHdvcmRzX292ZXJ2aWV3X2NvbHVtbnMsXG5cdFx0XHRcdGRhdGEuYWN0aXZlQ29sdW1ucyk7XG5cdFx0XHRjb25zdCBwYXJhbWV0ZXJzID0gJC5kZXBhcmFtKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc2xpY2UoMSkpO1xuXHRcdFx0Y29uc3QgcGFnZUxlbmd0aCA9IDA7XG5cdFx0XHRcblx0XHRcdGxldCBxdWVyeSA9IHVuZGVmaW5lZCAhPT0gdmFsdWUgPyAnJmRhdGVSYW5nZT0nICsgdmFsdWUgOiAnJztcblx0XHRcdGxldCB1cmwgPSBqc2UuY29yZS5jb25maWcuZ2V0KCdhcHBVcmwnKSArICcvYWRtaW4vYWRtaW4ucGhwP2RvPUFkV29yZHNDYW1wYWlnbnNPdmVydmlld0FqYXgvRGF0YVRhYmxlJ1xuXHRcdFx0XHQrIHF1ZXJ5O1xuXHRcdFx0XG5cdFx0XHRpZiAodW5kZWZpbmVkICE9PSB2YWx1ZSkge1xuXHRcdFx0XHQkdGhpcy5EYXRhVGFibGUoKS5hamF4LnVybCh1cmwpLmxvYWQoKTtcblx0XHRcdFx0X2xvYWRUb3RhbHModmFsdWUpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0anNlLmxpYnMuZGF0YXRhYmxlLmNyZWF0ZSgkdGhpcywge1xuXHRcdFx0XHRcdGF1dG9XaWR0aDogZmFsc2UsXG5cdFx0XHRcdFx0ZG9tOiAndCcsXG5cdFx0XHRcdFx0cGFnZUxlbmd0aCxcblx0XHRcdFx0XHRkaXNwbGF5U3RhcnQ6IHBhcnNlSW50KHBhcmFtZXRlcnMucGFnZSkgPyAocGFyc2VJbnQocGFyYW1ldGVycy5wYWdlKSAtIDEpICogcGFnZUxlbmd0aCA6IDAsXG5cdFx0XHRcdFx0c2VydmVyU2lkZTogdHJ1ZSxcblx0XHRcdFx0XHRsYW5ndWFnZToganNlLmxpYnMuZGF0YXRhYmxlLmdldFRyYW5zbGF0aW9ucyhqc2UuY29yZS5jb25maWcuZ2V0KCdsYW5ndWFnZUNvZGUnKSksXG5cdFx0XHRcdFx0YWpheDoge1xuXHRcdFx0XHRcdFx0dXJsLFxuXHRcdFx0XHRcdFx0dHlwZTogJ1BPU1QnLFxuXHRcdFx0XHRcdFx0ZGF0YToge1xuXHRcdFx0XHRcdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpXG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRvcmRlckNlbGxzVG9wOiB0cnVlLFxuXHRcdFx0XHRcdG9yZGVyOiBfZ2V0T3JkZXIocGFyYW1ldGVycywgY29sdW1ucyksXG5cdFx0XHRcdFx0c2VhcmNoQ29sczogW10sXG5cdFx0XHRcdFx0Y29sdW1uc1xuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHR9XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTG9hZCB0b3RhbHMgdGFibGUgZGF0YS5cblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfbG9hZFRvdGFscyh2YWx1ZSkge1xuXHRcdFx0bGV0IHF1ZXJ5ID0gdW5kZWZpbmVkICE9PSB2YWx1ZSA/ICcmZGF0ZVJhbmdlPScgKyB2YWx1ZSA6ICcnO1xuXHRcdFx0XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR0eXBlOiBcIkdFVFwiLFxuXHRcdFx0XHR1cmw6IG9wdGlvbnMuZ2V0VG90YWxzVXJsICsgJyZwYWdlVG9rZW49JyArIGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpICsgcXVlcnksXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0cmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHQkdGhpcy5maW5kKCcudG90YWwtcm93IC50b3RhbC1idWRnZXQnKS5yZW1vdmVDbGFzcygnbG9hZGluZycpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmIChyZXNwb25zZVsnc3VjY2VzcyddID09PSB0cnVlKSB7XG5cdFx0XHRcdFx0XHQkdGhpcy5maW5kKCcudG90YWwtcm93IC50b3RhbC1idWRnZXQnKS50ZXh0KHJlc3BvbnNlWydkYXRhJ11bJ2RhaWx5QnVkZ2V0J10pO1xuXHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnLnRvdGFsLXJvdyAudG90YWwtY2xpY2tzJykudGV4dChyZXNwb25zZVsnZGF0YSddWydjbGlja3MnXSk7XG5cdFx0XHRcdFx0XHQkdGhpcy5maW5kKCcudG90YWwtcm93IC50b3RhbC1pbXByZXNzaW9ucycpLnRleHQocmVzcG9uc2VbJ2RhdGEnXVsnaW1wcmVzc2lvbnMnXSk7XG5cdFx0XHRcdFx0XHQkdGhpcy5maW5kKCcudG90YWwtcm93IC50b3RhbC1jdHInKS50ZXh0KHJlc3BvbnNlWydkYXRhJ11bJ2NsaWNrVGhyb3VnaFJhdGUnXSk7XG5cdFx0XHRcdFx0XHQkdGhpcy5maW5kKCcudG90YWwtcm93IC50b3RhbC1hdmVyYWdlLWNwYycpLnRleHQocmVzcG9uc2VbJ2RhdGEnXVsnY29zdFBlckNsaWNrJ10pO1xuXHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnLnRvdGFsLXJvdyAudG90YWwtY29zdHMnKS50ZXh0KHJlc3BvbnNlWydkYXRhJ11bJ2Nvc3RzJ10pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHRyZXR1cm4gcmVzcG9uc2VbJ3N1Y2Nlc3MnXTtcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXJyb3I6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCR0aGlzLmZpbmQoJy50b3RhbC1yb3cgLnRvdGFsLWJ1ZGdldCcpLnJlbW92ZUNsYXNzKCdsb2FkaW5nJyk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdFx0XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRcblx0XHQvKipcblx0XHQgKiBIYW5kbGVzIHRoZSBjbGljayBldmVudCBvZiB0aGUgZWRpdCBpY29uLlxuXHRcdCAqXG5cdFx0ICogTWFrZXMgdGhlIGRhaWx5IGJ1ZGdldCB2YWx1ZSBlZGl0YWJsZS5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSBldmVudFxuXHRcdCAqL1xuXHRcdGZ1bmN0aW9uIF9jbGlja0VkaXQoZXZlbnQpIHtcblx0XHRcdGNvbnN0ICRkYWlseUJ1ZGdldCA9ICQodGhpcykuY2xvc2VzdCgndHInKS5maW5kKCcuZGFpbHktYnVkZ2V0Jyk7XG5cdFx0XHRjb25zdCBkYWlseUJ1ZGdldFZhbHVlID0gJGRhaWx5QnVkZ2V0LmRhdGEoJ2J1ZGdldCcpO1xuXHRcdFx0XG5cdFx0XHQvLyBjYW5jZWwgZWRpdGluZyBmb3IgYWxsIHJvd3Ncblx0XHRcdCR0aGlzLmZpbmQoJy5idG4tY2FuY2VsJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0X2NsaWNrQ2FuY2VsLmNhbGwodGhpcyk7XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JCh0aGlzKS5oaWRlKCk7XG5cdFx0XHQkKHRoaXMpLmNsb3Nlc3QoJ3RyJykuZmluZCgnLmJ0bi1zYXZlJykudGV4dChqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnc2F2ZScsICdidXR0b25zJykpLnNob3coKTtcblx0XHRcdCQodGhpcykuY2xvc2VzdCgndHInKS5maW5kKCcuYnRuLWNhbmNlbCcpLnRleHQoanNlLmNvcmUubGFuZy50cmFuc2xhdGUoJ2NhbmNlbCcsICdidXR0b25zJykpLnNob3coKTtcblx0XHRcdFxuXHRcdFx0JGRhaWx5QnVkZ2V0Lmh0bWwoJzxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhaWx5LWJ1ZGdldC1pbnB1dFwiIHZhbHVlPVwiJyArIGRhaWx5QnVkZ2V0VmFsdWVcblx0XHRcdFx0KyAnXCIgLz4nKTtcblx0XHRcdFxuXHRcdFx0JGRhaWx5QnVkZ2V0LmZpbmQoJy5kYWlseS1idWRnZXQtaW5wdXQnKS5vbigna2V5dXAnLCBmdW5jdGlvbihldmVudCkge1xuXHRcdFx0XHRpZiAoZXZlbnQua2V5Q29kZSA9PT0gMTMpIHtcblx0XHRcdFx0XHQkKHRoaXMpLmNsb3Nlc3QoJ3RyJykuZmluZCgnLmJ0bi1zYXZlJykuY2xpY2soKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFVwZGF0ZSBkYWlseSBidWRnZXQuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0gZXZlbnRcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfY2xpY2tTYXZlKGV2ZW50KSB7XG5cdFx0XHRjb25zdCAkZGFpbHlCdWRnZXQgPSAkKHRoaXMpLmNsb3Nlc3QoJ3RyJykuZmluZCgnLmRhaWx5LWJ1ZGdldCcpO1xuXHRcdFx0Y29uc3QgJGRhaWx5QnVkZ2V0SW5wdXQgPSAkKHRoaXMpLmNsb3Nlc3QoJ3RyJykuZmluZCgnLmRhaWx5LWJ1ZGdldC1pbnB1dCcpO1xuXHRcdFx0Y29uc3QgY2FtcGFpZ25JZCA9ICQodGhpcykuY2xvc2VzdCgndHInKS5maW5kKCcuZGFpbHktYnVkZ2V0JykuZGF0YSgnaWQnKTtcblx0XHRcdGNvbnN0ICRzYXZlQnV0dG9uID0gJCh0aGlzKTtcblx0XHRcdGNvbnN0ICRjYW5jZWxCdXR0b24gPSAkKHRoaXMpLmNsb3Nlc3QoJ3RyJykuZmluZCgnLmJ0bi1jYW5jZWwnKTtcblx0XHRcdGNvbnN0ICRlZGl0SWNvbiA9ICQodGhpcykuY2xvc2VzdCgndHInKS5maW5kKCcucm93LWVkaXQnKTtcblx0XHRcdFxuXHRcdFx0JGRhaWx5QnVkZ2V0LmFkZENsYXNzKCdsb2FkaW5nJyk7XG5cdFx0XHRcblx0XHRcdF9jbGlja0NhbmNlbCgpO1xuXHRcdFx0XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR0eXBlOiBcIlBPU1RcIixcblx0XHRcdFx0dXJsOiBvcHRpb25zLnVwZGF0ZURhaWx5QnVkZ2V0VXJsLFxuXHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0aWQ6IGNhbXBhaWduSWQsXG5cdFx0XHRcdFx0ZGFpbHlCdWRnZXQ6ICRkYWlseUJ1ZGdldElucHV0LnZhbCgpLFxuXHRcdFx0XHRcdHBhZ2VUb2tlbjoganNlLmNvcmUuY29uZmlnLmdldCgncGFnZVRva2VuJylcblx0XHRcdFx0fSxcblx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24ocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRyZXNwb25zZSA9IEpTT04ucGFyc2UocmVzcG9uc2UpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdGlmIChyZXNwb25zZVsnc3VjY2VzcyddID09PSB0cnVlKSB7XG5cdFx0XHRcdFx0XHQkZGFpbHlCdWRnZXQuaHRtbChyZXNwb25zZVsnZGF0YSddWydkYWlseUJ1ZGdldEh0bWwnXSk7XG5cdFx0XHRcdFx0XHQkZGFpbHlCdWRnZXQuYXR0cigndGl0bGUnLCByZXNwb25zZVsnZGF0YSddWydkYWlseUJ1ZGdldEh0bWwnXSk7XG5cdFx0XHRcdFx0XHQkZGFpbHlCdWRnZXQuZGF0YSgnYnVkZ2V0JywgcmVzcG9uc2VbJ2RhdGEnXVsnZGFpbHlCdWRnZXQnXSk7XG5cdFx0XHRcdFx0XHQkZGFpbHlCdWRnZXQuZGF0YSgnYnVkZ2V0LWh0bWwnLCByZXNwb25zZVsnZGF0YSddWydkYWlseUJ1ZGdldEh0bWwnXSk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdCRzYXZlQnV0dG9uLmhpZGUoKTtcblx0XHRcdFx0XHRcdCRjYW5jZWxCdXR0b24uaGlkZSgpO1xuXHRcdFx0XHRcdFx0JGVkaXRJY29uLnNob3coKTtcblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0JGRhaWx5QnVkZ2V0LnJlbW92ZUNsYXNzKCdsb2FkaW5nJyk7XG5cdFx0XHRcdFx0XHQkdGhpcy5maW5kKCcudG90YWwtcm93IC50b3RhbC1idWRnZXQnKS5hZGRDbGFzcygnbG9hZGluZycpO1xuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRfbG9hZFRvdGFscygpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHRyZXR1cm4gcmVzcG9uc2VbJ3N1Y2Nlc3MnXTtcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXJyb3I6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCRkYWlseUJ1ZGdldC5yZW1vdmVDbGFzcygnbG9hZGluZycpO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENhbmNlbCBlZGl0IG1vZGUgZm9yIGRhaWx5IGJ1ZGdldFxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIGV2ZW50XG5cdFx0ICovXG5cdFx0ZnVuY3Rpb24gX2NsaWNrQ2FuY2VsKGV2ZW50KSB7XG5cdFx0XHRjb25zdCAkZGFpbHlCdWRnZXQgPSAkKHRoaXMpLmNsb3Nlc3QoJ3RyJykuZmluZCgnLmRhaWx5LWJ1ZGdldCcpO1xuXHRcdFx0Y29uc3QgJGNhbmNlbEJ1dHRvbiA9ICQodGhpcyk7XG5cdFx0XHRjb25zdCAkc2F2ZUJ1dHRvbiA9ICQodGhpcykuY2xvc2VzdCgndHInKS5maW5kKCcuYnRuLXNhdmUnKTtcblx0XHRcdGNvbnN0ICRlZGl0SWNvbiA9ICQodGhpcykuY2xvc2VzdCgndHInKS5maW5kKCcucm93LWVkaXQnKTtcblx0XHRcdFxuXHRcdFx0JGRhaWx5QnVkZ2V0Lmh0bWwoJGRhaWx5QnVkZ2V0LmRhdGEoJ2J1ZGdldC1odG1sJykpO1xuXHRcdFx0XG5cdFx0XHQkc2F2ZUJ1dHRvbi5oaWRlKCk7XG5cdFx0XHQkY2FuY2VsQnV0dG9uLmhpZGUoKTtcblx0XHRcdCRlZGl0SWNvbi5zaG93KCk7XG5cdFx0fVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENoYW5nZSBjYW1wYWlnbiBzdGF0dXMuXG5cdFx0ICpcblx0XHQgKiBAcGFyYW0gZXZlbnRcblx0XHQgKi9cblx0XHRmdW5jdGlvbiBfY2hhbmdlU3RhdHVzKGV2ZW50KSB7XG5cdFx0XHQkLmFqYXgoe1xuXHRcdFx0XHR0eXBlOiBcIlBPU1RcIixcblx0XHRcdFx0dXJsOiBvcHRpb25zLnVwZGF0ZVN0YXR1c1VybCxcblx0XHRcdFx0ZGF0YToge1xuXHRcdFx0XHRcdGlkOiAkKHRoaXMpLmRhdGEoJ2lkJyksXG5cdFx0XHRcdFx0c3RhdHVzOiArJCh0aGlzKS5pcygnOmNoZWNrZWQnKSwgLy8gY2FzdCB0byBpbnRcblx0XHRcdFx0XHRwYWdlVG9rZW46IGpzZS5jb3JlLmNvbmZpZy5nZXQoJ3BhZ2VUb2tlbicpXG5cdFx0XHRcdH0sXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHJlc3BvbnNlWydzdWNjZXNzJ107XG5cdFx0XHRcdH0sXG5cdFx0XHRcdGVycm9yOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0X2xvYWREYXRhVGFibGUoKTtcblx0XHRcdF9sb2FkVG90YWxzKCk7XG5cdFx0XHRcblx0XHRcdCR0aGlzLm9uKCdjaGFuZ2UnLCAnLmNhbXBhaWduLXN0YXR1cy1jaGVja2JveCcsIF9jaGFuZ2VTdGF0dXMpO1xuXHRcdFx0JHRoaXMub24oJ2NoYW5nZScsIGRhdGVSYW5nZVBpY2tlclNlbGVjdG9yLCAoZXZlbnQpID0+IHtcblx0XHRcdFx0JHRoaXMuRGF0YVRhYmxlKCkuc2VhcmNoKGV2ZW50LnRhcmdldC52YWx1ZSkuZHJhdygpO1xuXHRcdFx0XHRfbG9hZFRvdGFscygpO1xuXHRcdFx0fSk7XG5cdFx0XHQkKGRhdGVSYW5nZVNlbGVjdG9yKS5vbignY2hhbmdlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGxldCB2YWx1ZSA9ICQodGhpcykudmFsKCk7XG5cdFx0XHRcdF9sb2FkRGF0YVRhYmxlKHZhbHVlKTtcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBBZGQgdGFibGUgZXJyb3IgaGFuZGxlci5cblx0XHRcdGpzZS5saWJzLmRhdGF0YWJsZS5lcnJvcigkdGhpcywgZnVuY3Rpb24oZXZlbnQsIHNldHRpbmdzLCB0ZWNoTm90ZSwgbWVzc2FnZSkge1xuXHRcdFx0XHRjb25zdCB0aXRsZSA9ICdEYXRhVGFibGVzICcgKyBqc2UuY29yZS5sYW5nLnRyYW5zbGF0ZSgnZXJyb3InLCAnbWVzc2FnZXMnKTtcblx0XHRcdFx0anNlLmxpYnMubW9kYWwuc2hvd01lc3NhZ2UodGl0bGUsIG1lc3NhZ2UpO1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdC8vIEFkZCBkcmF3IGV2ZW50IGhhbmRsZXIuIFxuXHRcdFx0JHRoaXMub24oJ2RyYXcuZHQnLCAoKSA9PiB7XG5cdFx0XHRcdCR0aGlzLmZpbmQoJ3Rib2R5JykuYXR0cignZGF0YS1neC13aWRnZXQnLCAnc3dpdGNoZXInKTtcblx0XHRcdFx0Z3gud2lkZ2V0cy5pbml0KCR0aGlzKTsgLy8gSW5pdGlhbGl6ZSB0aGUgc3dpdGNoZXIgd2lkZ2V0LlxuXHRcdFx0XHRcblx0XHRcdFx0aWYgKCFvcHRpb25zLnNob3dBY2NvdW50Rm9ybSkge1xuXHRcdFx0XHRcdCR0aGlzLmZpbmQoJy5yb3ctZWRpdCcpLm9uKCdjbGljaycsIF9jbGlja0VkaXQpO1xuXHRcdFx0XHRcdCR0aGlzLmZpbmQoJy5idG4tc2F2ZScpLm9uKCdjbGljaycsIF9jbGlja1NhdmUpO1xuXHRcdFx0XHRcdCR0aGlzLmZpbmQoJy5idG4tY2FuY2VsJykub24oJ2NsaWNrJywgX2NsaWNrQ2FuY2VsKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
