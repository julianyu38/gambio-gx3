'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
 * File: iframeResizer.js
 * Desc: Force iframes to size to content.
 * Requires: iframeResizer.contentWindow.js to be loaded into the target frame.
 * Doc: https://github.com/davidjbradshaw/iframe-resizer
 * Author: David J. Bradshaw - dave@bradshaw.net
 * Contributor: Jure Mav - jure.mav@gmail.com
 * Contributor: Reed Dadoune - reed@dadoune.com
 */

;(function (window) {
	'use strict';

	var count = 0,
	    logEnabled = false,
	    hiddenCheckEnabled = false,
	    msgHeader = 'message',
	    msgHeaderLen = msgHeader.length,
	    msgId = '[iFrameSizer]',
	    //Must match iframe msg ID
	msgIdLen = msgId.length,
	    pagePosition = null,
	    requestAnimationFrame = window.requestAnimationFrame,
	    resetRequiredMethods = { max: 1, scroll: 1, bodyScroll: 1, documentElementScroll: 1 },
	    settings = {},
	    timer = null,
	    logId = 'Host Page',
	    defaults = {
		autoResize: true,
		bodyBackground: null,
		bodyMargin: null,
		bodyMarginV1: 8,
		bodyPadding: null,
		checkOrigin: true,
		inPageLinks: false,
		enablePublicMethods: true,
		heightCalculationMethod: 'bodyOffset',
		id: 'iFrameResizer',
		interval: 32,
		log: false,
		maxHeight: Infinity,
		maxWidth: Infinity,
		minHeight: 0,
		minWidth: 0,
		resizeFrom: 'parent',
		scrolling: false,
		sizeHeight: true,
		sizeWidth: false,
		tolerance: 0,
		widthCalculationMethod: 'scroll',
		closedCallback: function closedCallback() {},
		initCallback: function initCallback() {},
		messageCallback: function messageCallback() {
			warn('MessageCallback function not defined');
		},
		resizedCallback: function resizedCallback() {},
		scrollCallback: function scrollCallback() {
			return true;
		}
	};

	function addEventListener(obj, evt, func) {
		/* istanbul ignore else */ // Not testable in PhantonJS
		if ('addEventListener' in window) {
			obj.addEventListener(evt, func, false);
		} else if ('attachEvent' in window) {
			//IE
			obj.attachEvent('on' + evt, func);
		}
	}

	function removeEventListener(el, evt, func) {
		/* istanbul ignore else */ // Not testable in phantonJS
		if ('removeEventListener' in window) {
			el.removeEventListener(evt, func, false);
		} else if ('detachEvent' in window) {
			//IE
			el.detachEvent('on' + evt, func);
		}
	}

	function setupRequestAnimationFrame() {
		var vendors = ['moz', 'webkit', 'o', 'ms'],
		    x;

		// Remove vendor prefixing if prefixed and break early if not
		for (x = 0; x < vendors.length && !requestAnimationFrame; x += 1) {
			requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
		}

		if (!requestAnimationFrame) {
			log('setup', 'RequestAnimationFrame not supported');
		}
	}

	function getMyID(iframeId) {
		var retStr = 'Host page: ' + iframeId;

		if (window.top !== window.self) {
			if (window.parentIFrame && window.parentIFrame.getId) {
				retStr = window.parentIFrame.getId() + ': ' + iframeId;
			} else {
				retStr = 'Nested host page: ' + iframeId;
			}
		}

		return retStr;
	}

	function formatLogHeader(iframeId) {
		return msgId + '[' + getMyID(iframeId) + ']';
	}

	function isLogEnabled(iframeId) {
		return settings[iframeId] ? settings[iframeId].log : logEnabled;
	}

	function log(iframeId, msg) {
		output('log', iframeId, msg, isLogEnabled(iframeId));
	}

	function info(iframeId, msg) {
		output('info', iframeId, msg, isLogEnabled(iframeId));
	}

	function warn(iframeId, msg) {
		output('warn', iframeId, msg, true);
	}

	function output(type, iframeId, msg, enabled) {
		if (true === enabled && 'object' === _typeof(window.console)) {
			console[type](formatLogHeader(iframeId), msg);
		}
	}

	function iFrameListener(event) {
		function resizeIFrame() {
			function resize() {
				setSize(messageData);
				setPagePosition(iframeId);
			}

			ensureInRange('Height');
			ensureInRange('Width');

			syncResize(resize, messageData, 'init');
		}

		function processMsg() {
			var data = msg.substr(msgIdLen).split(':');

			return {
				iframe: settings[data[0]].iframe,
				id: data[0],
				height: data[1],
				width: data[2],
				type: data[3]
			};
		}

		function ensureInRange(Dimension) {
			var max = Number(settings[iframeId]['max' + Dimension]),
			    min = Number(settings[iframeId]['min' + Dimension]),
			    dimension = Dimension.toLowerCase(),
			    size = Number(messageData[dimension]);

			log(iframeId, 'Checking ' + dimension + ' is in range ' + min + '-' + max);

			if (size < min) {
				size = min;
				log(iframeId, 'Set ' + dimension + ' to min value');
			}

			if (size > max) {
				size = max;
				log(iframeId, 'Set ' + dimension + ' to max value');
			}

			messageData[dimension] = '' + size;
		}

		function isMessageFromIFrame() {
			function checkAllowedOrigin() {
				function checkList() {
					var i = 0,
					    retCode = false;

					log(iframeId, 'Checking connection is from allowed list of origins: ' + checkOrigin);

					for (; i < checkOrigin.length; i++) {
						if (checkOrigin[i] === origin) {
							retCode = true;
							break;
						}
					}
					return retCode;
				}

				function checkSingle() {
					var remoteHost = settings[iframeId].remoteHost;
					log(iframeId, 'Checking connection is from: ' + remoteHost);
					return origin === remoteHost;
				}

				return checkOrigin.constructor === Array ? checkList() : checkSingle();
			}

			var origin = event.origin,
			    checkOrigin = settings[iframeId].checkOrigin;

			if (checkOrigin && '' + origin !== 'null' && !checkAllowedOrigin()) {
				throw new Error('Unexpected message received from: ' + origin + ' for ' + messageData.iframe.id + '. Message was: ' + event.data + '. This error can be disabled by setting the checkOrigin: false option or by providing of array of trusted domains.');
			}

			return true;
		}

		function isMessageForUs() {
			return msgId === ('' + msg).substr(0, msgIdLen) && msg.substr(msgIdLen).split(':')[0] in settings; //''+Protects against non-string msg
		}

		function isMessageFromMetaParent() {
			//Test if this message is from a parent above us. This is an ugly test, however, updating
			//the message format would break backwards compatibity.
			var retCode = messageData.type in { 'true': 1, 'false': 1, 'undefined': 1 };

			if (retCode) {
				log(iframeId, 'Ignoring init message from meta parent page');
			}

			return retCode;
		}

		function getMsgBody(offset) {
			return msg.substr(msg.indexOf(':') + msgHeaderLen + offset);
		}

		function forwardMsgFromIFrame(msgBody) {
			log(iframeId, 'MessageCallback passed: {iframe: ' + messageData.iframe.id + ', message: ' + msgBody + '}');
			callback('messageCallback', {
				iframe: messageData.iframe,
				message: JSON.parse(msgBody)
			});
			log(iframeId, '--');
		}

		function getPageInfo() {
			var bodyPosition = document.body.getBoundingClientRect(),
			    iFramePosition = messageData.iframe.getBoundingClientRect();

			return JSON.stringify({
				iframeHeight: iFramePosition.height,
				iframeWidth: iFramePosition.width,
				clientHeight: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
				clientWidth: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
				offsetTop: parseInt(iFramePosition.top - bodyPosition.top, 10),
				offsetLeft: parseInt(iFramePosition.left - bodyPosition.left, 10),
				scrollTop: window.pageYOffset,
				scrollLeft: window.pageXOffset
			});
		}

		function sendPageInfoToIframe(iframe, iframeId) {
			function debouncedTrigger() {
				trigger('Send Page Info', 'pageInfo:' + getPageInfo(), iframe, iframeId);
			}

			debouce(debouncedTrigger, 32);
		}

		function startPageInfoMonitor() {
			function setListener(type, func) {
				function sendPageInfo() {
					if (settings[id]) {
						sendPageInfoToIframe(settings[id].iframe, id);
					} else {
						stop();
					}
				}

				['scroll', 'resize'].forEach(function (evt) {
					log(id, type + evt + ' listener for sendPageInfo');
					func(window, evt, sendPageInfo);
				});
			}

			function stop() {
				setListener('Remove ', removeEventListener);
			}

			function start() {
				setListener('Add ', addEventListener);
			}

			var id = iframeId; //Create locally scoped copy of iFrame ID

			start();

			settings[id].stopPageInfo = stop;
		}

		function stopPageInfoMonitor() {
			if (settings[iframeId] && settings[iframeId].stopPageInfo) {
				settings[iframeId].stopPageInfo();
				delete settings[iframeId].stopPageInfo;
			}
		}

		function checkIFrameExists() {
			var retBool = true;

			if (null === messageData.iframe) {
				warn(iframeId, 'IFrame (' + messageData.id + ') not found');
				retBool = false;
			}
			return retBool;
		}

		function getElementPosition(target) {
			var iFramePosition = target.getBoundingClientRect();

			getPagePosition(iframeId);

			return {
				x: Math.floor(Number(iFramePosition.left) + Number(pagePosition.x)),
				y: Math.floor(Number(iFramePosition.top) + Number(pagePosition.y))
			};
		}

		function scrollRequestFromChild(addOffset) {
			/* istanbul ignore next */ //Not testable in Karma
			function reposition() {
				pagePosition = newPosition;
				scrollTo();
				log(iframeId, '--');
			}

			function calcOffset() {
				return {
					x: Number(messageData.width) + offset.x,
					y: Number(messageData.height) + offset.y
				};
			}

			function scrollParent() {
				if (window.parentIFrame) {
					window.parentIFrame['scrollTo' + (addOffset ? 'Offset' : '')](newPosition.x, newPosition.y);
				} else {
					warn(iframeId, 'Unable to scroll to requested position, window.parentIFrame not found');
				}
			}

			var offset = addOffset ? getElementPosition(messageData.iframe) : { x: 0, y: 0 },
			    newPosition = calcOffset();

			log(iframeId, 'Reposition requested from iFrame (offset x:' + offset.x + ' y:' + offset.y + ')');

			if (window.top !== window.self) {
				scrollParent();
			} else {
				reposition();
			}
		}

		function scrollTo() {
			if (false !== callback('scrollCallback', pagePosition)) {
				setPagePosition(iframeId);
			} else {
				unsetPagePosition();
			}
		}

		function findTarget(location) {
			function jumpToTarget() {
				var jumpPosition = getElementPosition(target);

				log(iframeId, 'Moving to in page link (#' + hash + ') at x: ' + jumpPosition.x + ' y: ' + jumpPosition.y);
				pagePosition = {
					x: jumpPosition.x,
					y: jumpPosition.y
				};

				scrollTo();
				log(iframeId, '--');
			}

			function jumpToParent() {
				if (window.parentIFrame) {
					window.parentIFrame.moveToAnchor(hash);
				} else {
					log(iframeId, 'In page link #' + hash + ' not found and window.parentIFrame not found');
				}
			}

			var hash = location.split('#')[1] || '',
			    hashData = decodeURIComponent(hash),
			    target = document.getElementById(hashData) || document.getElementsByName(hashData)[0];

			if (target) {
				jumpToTarget();
			} else if (window.top !== window.self) {
				jumpToParent();
			} else {
				log(iframeId, 'In page link #' + hash + ' not found');
			}
		}

		function callback(funcName, val) {
			return chkCallback(iframeId, funcName, val);
		}

		function actionMsg() {

			if (settings[iframeId].firstRun) firstRun();

			switch (messageData.type) {
				case 'close':
					closeIFrame(messageData.iframe);
					break;
				case 'message':
					forwardMsgFromIFrame(getMsgBody(6));
					break;
				case 'scrollTo':
					scrollRequestFromChild(false);
					break;
				case 'scrollToOffset':
					scrollRequestFromChild(true);
					break;
				case 'pageInfo':
					sendPageInfoToIframe(settings[iframeId].iframe, iframeId);
					startPageInfoMonitor();
					break;
				case 'pageInfoStop':
					stopPageInfoMonitor();
					break;
				case 'inPageLink':
					findTarget(getMsgBody(9));
					break;
				case 'reset':
					resetIFrame(messageData);
					break;
				case 'init':
					resizeIFrame();
					callback('initCallback', messageData.iframe);
					callback('resizedCallback', messageData);
					break;
				default:
					resizeIFrame();
					callback('resizedCallback', messageData);
			}
		}

		function hasSettings(iframeId) {
			var retBool = true;

			if (!settings[iframeId]) {
				retBool = false;
				warn(messageData.type + ' No settings for ' + iframeId + '. Message was: ' + msg);
			}

			return retBool;
		}

		function iFrameReadyMsgReceived() {
			for (var iframeId in settings) {
				trigger('iFrame requested init', createOutgoingMsg(iframeId), document.getElementById(iframeId), iframeId);
			}
		}

		function firstRun() {
			settings[iframeId].firstRun = false;
		}

		var msg = event.data,
		    messageData = {},
		    iframeId = null;

		if ('[iFrameResizerChild]Ready' === msg) {
			iFrameReadyMsgReceived();
		} else if (isMessageForUs()) {
			messageData = processMsg();
			iframeId = logId = messageData.id;

			if (!isMessageFromMetaParent() && hasSettings(iframeId)) {
				log(iframeId, 'Received: ' + msg);

				if (checkIFrameExists() && isMessageFromIFrame()) {
					actionMsg();
				}
			}
		} else {
			info(iframeId, 'Ignored: ' + msg);
		}
	}

	function chkCallback(iframeId, funcName, val) {
		var func = null,
		    retVal = null;

		if (settings[iframeId]) {
			func = settings[iframeId][funcName];

			if ('function' === typeof func) {
				retVal = func(val);
			} else {
				throw new TypeError(funcName + ' on iFrame[' + iframeId + '] is not a function');
			}
		}

		return retVal;
	}

	function closeIFrame(iframe) {
		var iframeId = iframe.id;

		log(iframeId, 'Removing iFrame: ' + iframeId);
		if (iframe.parentNode) {
			iframe.parentNode.removeChild(iframe);
		}
		chkCallback(iframeId, 'closedCallback', iframeId);
		log(iframeId, '--');
		delete settings[iframeId];
	}

	function getPagePosition(iframeId) {
		if (null === pagePosition) {
			pagePosition = {
				x: window.pageXOffset !== undefined ? window.pageXOffset : document.documentElement.scrollLeft,
				y: window.pageYOffset !== undefined ? window.pageYOffset : document.documentElement.scrollTop
			};
			log(iframeId, 'Get page position: ' + pagePosition.x + ',' + pagePosition.y);
		}
	}

	function setPagePosition(iframeId) {
		if (null !== pagePosition) {
			window.scrollTo(pagePosition.x, pagePosition.y);
			log(iframeId, 'Set page position: ' + pagePosition.x + ',' + pagePosition.y);
			unsetPagePosition();
		}
	}

	function unsetPagePosition() {
		pagePosition = null;
	}

	function resetIFrame(messageData) {
		function reset() {
			setSize(messageData);
			trigger('reset', 'reset', messageData.iframe, messageData.id);
		}

		log(messageData.id, 'Size reset requested by ' + ('init' === messageData.type ? 'host page' : 'iFrame'));
		getPagePosition(messageData.id);
		syncResize(reset, messageData, 'reset');
	}

	function setSize(messageData) {
		function setDimension(dimension) {
			messageData.iframe.style[dimension] = messageData[dimension] + 'px';
			log(messageData.id, 'IFrame (' + iframeId + ') ' + dimension + ' set to ' + messageData[dimension] + 'px');
		}

		function chkZero(dimension) {
			//FireFox sets dimension of hidden iFrames to zero.
			//So if we detect that set up an event to check for
			//when iFrame becomes visible.

			/* istanbul ignore next */ //Not testable in PhantomJS
			if (!hiddenCheckEnabled && '0' === messageData[dimension]) {
				hiddenCheckEnabled = true;
				log(iframeId, 'Hidden iFrame detected, creating visibility listener');
				fixHiddenIFrames();
			}
		}

		function processDimension(dimension) {
			setDimension(dimension);
			chkZero(dimension);
		}

		var iframeId = messageData.iframe.id;

		if (settings[iframeId]) {
			if (settings[iframeId].sizeHeight) {
				processDimension('height');
			}
			if (settings[iframeId].sizeWidth) {
				processDimension('width');
			}
		}
	}

	function syncResize(func, messageData, doNotSync) {
		/* istanbul ignore if */ //Not testable in PhantomJS
		if (doNotSync !== messageData.type && requestAnimationFrame) {
			log(messageData.id, 'Requesting animation frame');
			requestAnimationFrame(func);
		} else {
			func();
		}
	}

	function trigger(calleeMsg, msg, iframe, id) {
		function postMessageToIFrame() {
			var target = settings[id].targetOrigin;
			log(id, '[' + calleeMsg + '] Sending msg to iframe[' + id + '] (' + msg + ') targetOrigin: ' + target);
			iframe.contentWindow.postMessage(msgId + msg, target);
		}

		function iFrameNotFound() {
			warn(id, '[' + calleeMsg + '] IFrame(' + id + ') not found');
		}

		function chkAndSend() {
			if (iframe && 'contentWindow' in iframe && null !== iframe.contentWindow) {
				//Null test for PhantomJS
				postMessageToIFrame();
			} else {
				iFrameNotFound();
			}
		}

		id = id || iframe.id;

		if (settings[id]) {
			chkAndSend();
		}
	}

	function createOutgoingMsg(iframeId) {
		return iframeId + ':' + settings[iframeId].bodyMarginV1 + ':' + settings[iframeId].sizeWidth + ':' + settings[iframeId].log + ':' + settings[iframeId].interval + ':' + settings[iframeId].enablePublicMethods + ':' + settings[iframeId].autoResize + ':' + settings[iframeId].bodyMargin + ':' + settings[iframeId].heightCalculationMethod + ':' + settings[iframeId].bodyBackground + ':' + settings[iframeId].bodyPadding + ':' + settings[iframeId].tolerance + ':' + settings[iframeId].inPageLinks + ':' + settings[iframeId].resizeFrom + ':' + settings[iframeId].widthCalculationMethod;
	}

	function setupIFrame(iframe, options) {
		function setLimits() {
			function addStyle(style) {
				if (Infinity !== settings[iframeId][style] && 0 !== settings[iframeId][style]) {
					iframe.style[style] = settings[iframeId][style] + 'px';
					log(iframeId, 'Set ' + style + ' = ' + settings[iframeId][style] + 'px');
				}
			}

			function chkMinMax(dimension) {
				if (settings[iframeId]['min' + dimension] > settings[iframeId]['max' + dimension]) {
					throw new Error('Value for min' + dimension + ' can not be greater than max' + dimension);
				}
			}

			chkMinMax('Height');
			chkMinMax('Width');

			addStyle('maxHeight');
			addStyle('minHeight');
			addStyle('maxWidth');
			addStyle('minWidth');
		}

		function newId() {
			var id = options && options.id || defaults.id + count++;
			if (null !== document.getElementById(id)) {
				id = id + count++;
			}
			return id;
		}

		function ensureHasId(iframeId) {
			logId = iframeId;
			if ('' === iframeId) {
				iframe.id = iframeId = newId();
				logEnabled = (options || {}).log;
				logId = iframeId;
				log(iframeId, 'Added missing iframe ID: ' + iframeId + ' (' + iframe.src + ')');
			}

			return iframeId;
		}

		function setScrolling() {
			log(iframeId, 'IFrame scrolling ' + (settings[iframeId].scrolling ? 'enabled' : 'disabled') + ' for ' + iframeId);
			iframe.style.overflow = false === settings[iframeId].scrolling ? 'hidden' : 'auto';
			iframe.scrolling = false === settings[iframeId].scrolling ? 'no' : 'yes';
		}

		//The V1 iFrame script expects an int, where as in V2 expects a CSS
		//string value such as '1px 3em', so if we have an int for V2, set V1=V2
		//and then convert V2 to a string PX value.
		function setupBodyMarginValues() {
			if ('number' === typeof settings[iframeId].bodyMargin || '0' === settings[iframeId].bodyMargin) {
				settings[iframeId].bodyMarginV1 = settings[iframeId].bodyMargin;
				settings[iframeId].bodyMargin = '' + settings[iframeId].bodyMargin + 'px';
			}
		}

		function checkReset() {
			// Reduce scope of firstRun to function, because IE8's JS execution
			// context stack is borked and this value gets externally
			// changed midway through running this function!!!
			var firstRun = settings[iframeId].firstRun,
			    resetRequertMethod = settings[iframeId].heightCalculationMethod in resetRequiredMethods;

			if (!firstRun && resetRequertMethod) {
				resetIFrame({ iframe: iframe, height: 0, width: 0, type: 'init' });
			}
		}

		function setupIFrameObject() {
			if (Function.prototype.bind) {
				//Ignore unpolyfilled IE8.
				settings[iframeId].iframe.iFrameResizer = {

					close: closeIFrame.bind(null, settings[iframeId].iframe),

					resize: trigger.bind(null, 'Window resize', 'resize', settings[iframeId].iframe),

					moveToAnchor: function moveToAnchor(anchor) {
						trigger('Move to anchor', 'moveToAnchor:' + anchor, settings[iframeId].iframe, iframeId);
					},

					sendMessage: function sendMessage(message) {
						message = JSON.stringify(message);
						trigger('Send Message', 'message:' + message, settings[iframeId].iframe, iframeId);
					}
				};
			}
		}

		//We have to call trigger twice, as we can not be sure if all
		//iframes have completed loading when this code runs. The
		//event listener also catches the page changing in the iFrame.
		function init(msg) {
			function iFrameLoaded() {
				trigger('iFrame.onload', msg, iframe);
				checkReset();
			}

			addEventListener(iframe, 'load', iFrameLoaded);
			trigger('init', msg, iframe);
		}

		function checkOptions(options) {
			if ('object' !== (typeof options === 'undefined' ? 'undefined' : _typeof(options))) {
				throw new TypeError('Options is not an object');
			}
		}

		function copyOptions(options) {
			for (var option in defaults) {
				if (defaults.hasOwnProperty(option)) {
					settings[iframeId][option] = options.hasOwnProperty(option) ? options[option] : defaults[option];
				}
			}
		}

		function getTargetOrigin(remoteHost) {
			return '' === remoteHost || 'file://' === remoteHost ? '*' : remoteHost;
		}

		function processOptions(options) {
			options = options || {};
			settings[iframeId] = {
				firstRun: true,
				iframe: iframe,
				remoteHost: iframe.src.split('/').slice(0, 3).join('/')
			};

			checkOptions(options);
			copyOptions(options);

			settings[iframeId].targetOrigin = true === settings[iframeId].checkOrigin ? getTargetOrigin(settings[iframeId].remoteHost) : '*';
		}

		function beenHere() {
			return iframeId in settings && 'iFrameResizer' in iframe;
		}

		var iframeId = ensureHasId(iframe.id);

		if (!beenHere()) {
			processOptions(options);
			setScrolling();
			setLimits();
			setupBodyMarginValues();
			init(createOutgoingMsg(iframeId));
			setupIFrameObject();
		} else {
			warn(iframeId, 'Ignored iFrame, already setup.');
		}
	}

	function debouce(fn, time) {
		if (null === timer) {
			timer = setTimeout(function () {
				timer = null;
				fn();
			}, time);
		}
	}

	/* istanbul ignore next */ //Not testable in PhantomJS
	function fixHiddenIFrames() {
		function checkIFrames() {
			function checkIFrame(settingId) {
				function chkDimension(dimension) {
					return '0px' === settings[settingId].iframe.style[dimension];
				}

				function isVisible(el) {
					return null !== el.offsetParent;
				}

				if (isVisible(settings[settingId].iframe) && (chkDimension('height') || chkDimension('width'))) {
					trigger('Visibility change', 'resize', settings[settingId].iframe, settingId);
				}
			}

			for (var settingId in settings) {
				checkIFrame(settingId);
			}
		}

		function mutationObserved(mutations) {
			log('window', 'Mutation observed: ' + mutations[0].target + ' ' + mutations[0].type);
			debouce(checkIFrames, 16);
		}

		function createMutationObserver() {
			var target = document.querySelector('body'),
			    config = {
				attributes: true,
				attributeOldValue: false,
				characterData: true,
				characterDataOldValue: false,
				childList: true,
				subtree: true
			},
			    observer = new MutationObserver(mutationObserved);

			observer.observe(target, config);
		}

		var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

		if (MutationObserver) createMutationObserver();
	}

	function resizeIFrames(event) {
		function resize() {
			sendTriggerMsg('Window ' + event, 'resize');
		}

		log('window', 'Trigger event: ' + event);
		debouce(resize, 16);
	}

	/* istanbul ignore next */ //Not testable in PhantomJS
	function tabVisible() {
		function resize() {
			sendTriggerMsg('Tab Visable', 'resize');
		}

		if ('hidden' !== document.visibilityState) {
			log('document', 'Trigger event: Visiblity change');
			debouce(resize, 16);
		}
	}

	function sendTriggerMsg(eventName, event) {
		function isIFrameResizeEnabled(iframeId) {
			return 'parent' === settings[iframeId].resizeFrom && settings[iframeId].autoResize && !settings[iframeId].firstRun;
		}

		for (var iframeId in settings) {
			if (isIFrameResizeEnabled(iframeId)) {
				trigger(eventName, event, document.getElementById(iframeId), iframeId);
			}
		}
	}

	function setupEventListeners() {
		addEventListener(window, 'message', iFrameListener);

		addEventListener(window, 'resize', function () {
			resizeIFrames('resize');
		});

		addEventListener(document, 'visibilitychange', tabVisible);
		addEventListener(document, '-webkit-visibilitychange', tabVisible); //Andriod 4.4
		addEventListener(window, 'focusin', function () {
			resizeIFrames('focus');
		}); //IE8-9
		addEventListener(window, 'focus', function () {
			resizeIFrames('focus');
		});
	}

	function factory() {
		function init(options, element) {
			function chkType() {
				if (!element.tagName) {
					throw new TypeError('Object is not a valid DOM element');
				} else if ('IFRAME' !== element.tagName.toUpperCase()) {
					throw new TypeError('Expected <IFRAME> tag, found <' + element.tagName + '>');
				}
			}

			if (element) {
				chkType();
				setupIFrame(element, options);
				iFrames.push(element);
			}
		}

		function warnDeprecatedOptions(options) {
			if (options && options.enablePublicMethods) {
				warn('enablePublicMethods option has been removed, public methods are now always available in the iFrame');
			}
		}

		var iFrames;

		setupRequestAnimationFrame();
		setupEventListeners();

		return function iFrameResizeF(options, target) {
			iFrames = []; //Only return iFrames past in on this call

			warnDeprecatedOptions(options);

			switch (typeof target === 'undefined' ? 'undefined' : _typeof(target)) {
				case 'undefined':
				case 'string':
					Array.prototype.forEach.call(document.querySelectorAll(target || 'iframe'), init.bind(undefined, options));
					break;
				case 'object':
					init(options, target);
					break;
				default:
					throw new TypeError('Unexpected data type (' + (typeof target === 'undefined' ? 'undefined' : _typeof(target)) + ')');
			}

			return iFrames;
		};
	}

	function createJQueryPublicMethod($) {
		if (!$.fn) {
			info('', 'Unable to bind to jQuery, it is not fully loaded.');
		} else if (!$.fn.iFrameResize) {
			$.fn.iFrameResize = function $iFrameResizeF(options) {
				function init(index, element) {
					setupIFrame(element, options);
				}

				return this.filter('iframe').each(init).end();
			};
		}
	}

	if (window.jQuery) {
		createJQueryPublicMethod(jQuery);
	}

	window.iFrameResize = window.iFrameResize || factory();
})(window || {});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvdmVuZG9yL2lmcmFtZV9yZXNpemVyLmpzIl0sIm5hbWVzIjpbIndpbmRvdyIsImNvdW50IiwibG9nRW5hYmxlZCIsImhpZGRlbkNoZWNrRW5hYmxlZCIsIm1zZ0hlYWRlciIsIm1zZ0hlYWRlckxlbiIsImxlbmd0aCIsIm1zZ0lkIiwibXNnSWRMZW4iLCJwYWdlUG9zaXRpb24iLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJyZXNldFJlcXVpcmVkTWV0aG9kcyIsIm1heCIsInNjcm9sbCIsImJvZHlTY3JvbGwiLCJkb2N1bWVudEVsZW1lbnRTY3JvbGwiLCJzZXR0aW5ncyIsInRpbWVyIiwibG9nSWQiLCJkZWZhdWx0cyIsImF1dG9SZXNpemUiLCJib2R5QmFja2dyb3VuZCIsImJvZHlNYXJnaW4iLCJib2R5TWFyZ2luVjEiLCJib2R5UGFkZGluZyIsImNoZWNrT3JpZ2luIiwiaW5QYWdlTGlua3MiLCJlbmFibGVQdWJsaWNNZXRob2RzIiwiaGVpZ2h0Q2FsY3VsYXRpb25NZXRob2QiLCJpZCIsImludGVydmFsIiwibG9nIiwibWF4SGVpZ2h0IiwiSW5maW5pdHkiLCJtYXhXaWR0aCIsIm1pbkhlaWdodCIsIm1pbldpZHRoIiwicmVzaXplRnJvbSIsInNjcm9sbGluZyIsInNpemVIZWlnaHQiLCJzaXplV2lkdGgiLCJ0b2xlcmFuY2UiLCJ3aWR0aENhbGN1bGF0aW9uTWV0aG9kIiwiY2xvc2VkQ2FsbGJhY2siLCJpbml0Q2FsbGJhY2siLCJtZXNzYWdlQ2FsbGJhY2siLCJ3YXJuIiwicmVzaXplZENhbGxiYWNrIiwic2Nyb2xsQ2FsbGJhY2siLCJhZGRFdmVudExpc3RlbmVyIiwib2JqIiwiZXZ0IiwiZnVuYyIsImF0dGFjaEV2ZW50IiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImVsIiwiZGV0YWNoRXZlbnQiLCJzZXR1cFJlcXVlc3RBbmltYXRpb25GcmFtZSIsInZlbmRvcnMiLCJ4IiwiZ2V0TXlJRCIsImlmcmFtZUlkIiwicmV0U3RyIiwidG9wIiwic2VsZiIsInBhcmVudElGcmFtZSIsImdldElkIiwiZm9ybWF0TG9nSGVhZGVyIiwiaXNMb2dFbmFibGVkIiwibXNnIiwib3V0cHV0IiwiaW5mbyIsInR5cGUiLCJlbmFibGVkIiwiY29uc29sZSIsImlGcmFtZUxpc3RlbmVyIiwiZXZlbnQiLCJyZXNpemVJRnJhbWUiLCJyZXNpemUiLCJzZXRTaXplIiwibWVzc2FnZURhdGEiLCJzZXRQYWdlUG9zaXRpb24iLCJlbnN1cmVJblJhbmdlIiwic3luY1Jlc2l6ZSIsInByb2Nlc3NNc2ciLCJkYXRhIiwic3Vic3RyIiwic3BsaXQiLCJpZnJhbWUiLCJoZWlnaHQiLCJ3aWR0aCIsIkRpbWVuc2lvbiIsIk51bWJlciIsIm1pbiIsImRpbWVuc2lvbiIsInRvTG93ZXJDYXNlIiwic2l6ZSIsImlzTWVzc2FnZUZyb21JRnJhbWUiLCJjaGVja0FsbG93ZWRPcmlnaW4iLCJjaGVja0xpc3QiLCJpIiwicmV0Q29kZSIsIm9yaWdpbiIsImNoZWNrU2luZ2xlIiwicmVtb3RlSG9zdCIsImNvbnN0cnVjdG9yIiwiQXJyYXkiLCJFcnJvciIsImlzTWVzc2FnZUZvclVzIiwiaXNNZXNzYWdlRnJvbU1ldGFQYXJlbnQiLCJnZXRNc2dCb2R5Iiwib2Zmc2V0IiwiaW5kZXhPZiIsImZvcndhcmRNc2dGcm9tSUZyYW1lIiwibXNnQm9keSIsImNhbGxiYWNrIiwibWVzc2FnZSIsIkpTT04iLCJwYXJzZSIsImdldFBhZ2VJbmZvIiwiYm9keVBvc2l0aW9uIiwiZG9jdW1lbnQiLCJib2R5IiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwiaUZyYW1lUG9zaXRpb24iLCJzdHJpbmdpZnkiLCJpZnJhbWVIZWlnaHQiLCJpZnJhbWVXaWR0aCIsImNsaWVudEhlaWdodCIsIk1hdGgiLCJkb2N1bWVudEVsZW1lbnQiLCJpbm5lckhlaWdodCIsImNsaWVudFdpZHRoIiwiaW5uZXJXaWR0aCIsIm9mZnNldFRvcCIsInBhcnNlSW50Iiwib2Zmc2V0TGVmdCIsImxlZnQiLCJzY3JvbGxUb3AiLCJwYWdlWU9mZnNldCIsInNjcm9sbExlZnQiLCJwYWdlWE9mZnNldCIsInNlbmRQYWdlSW5mb1RvSWZyYW1lIiwiZGVib3VuY2VkVHJpZ2dlciIsInRyaWdnZXIiLCJkZWJvdWNlIiwic3RhcnRQYWdlSW5mb01vbml0b3IiLCJzZXRMaXN0ZW5lciIsInNlbmRQYWdlSW5mbyIsInN0b3AiLCJmb3JFYWNoIiwic3RhcnQiLCJzdG9wUGFnZUluZm8iLCJzdG9wUGFnZUluZm9Nb25pdG9yIiwiY2hlY2tJRnJhbWVFeGlzdHMiLCJyZXRCb29sIiwiZ2V0RWxlbWVudFBvc2l0aW9uIiwidGFyZ2V0IiwiZ2V0UGFnZVBvc2l0aW9uIiwiZmxvb3IiLCJ5Iiwic2Nyb2xsUmVxdWVzdEZyb21DaGlsZCIsImFkZE9mZnNldCIsInJlcG9zaXRpb24iLCJuZXdQb3NpdGlvbiIsInNjcm9sbFRvIiwiY2FsY09mZnNldCIsInNjcm9sbFBhcmVudCIsInVuc2V0UGFnZVBvc2l0aW9uIiwiZmluZFRhcmdldCIsImxvY2F0aW9uIiwianVtcFRvVGFyZ2V0IiwianVtcFBvc2l0aW9uIiwiaGFzaCIsImp1bXBUb1BhcmVudCIsIm1vdmVUb0FuY2hvciIsImhhc2hEYXRhIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJnZXRFbGVtZW50c0J5TmFtZSIsImZ1bmNOYW1lIiwidmFsIiwiY2hrQ2FsbGJhY2siLCJhY3Rpb25Nc2ciLCJmaXJzdFJ1biIsImNsb3NlSUZyYW1lIiwicmVzZXRJRnJhbWUiLCJoYXNTZXR0aW5ncyIsImlGcmFtZVJlYWR5TXNnUmVjZWl2ZWQiLCJjcmVhdGVPdXRnb2luZ01zZyIsInJldFZhbCIsIlR5cGVFcnJvciIsInBhcmVudE5vZGUiLCJyZW1vdmVDaGlsZCIsInVuZGVmaW5lZCIsInJlc2V0Iiwic2V0RGltZW5zaW9uIiwic3R5bGUiLCJjaGtaZXJvIiwiZml4SGlkZGVuSUZyYW1lcyIsInByb2Nlc3NEaW1lbnNpb24iLCJkb05vdFN5bmMiLCJjYWxsZWVNc2ciLCJwb3N0TWVzc2FnZVRvSUZyYW1lIiwidGFyZ2V0T3JpZ2luIiwiY29udGVudFdpbmRvdyIsInBvc3RNZXNzYWdlIiwiaUZyYW1lTm90Rm91bmQiLCJjaGtBbmRTZW5kIiwic2V0dXBJRnJhbWUiLCJvcHRpb25zIiwic2V0TGltaXRzIiwiYWRkU3R5bGUiLCJjaGtNaW5NYXgiLCJuZXdJZCIsImVuc3VyZUhhc0lkIiwic3JjIiwic2V0U2Nyb2xsaW5nIiwib3ZlcmZsb3ciLCJzZXR1cEJvZHlNYXJnaW5WYWx1ZXMiLCJjaGVja1Jlc2V0IiwicmVzZXRSZXF1ZXJ0TWV0aG9kIiwic2V0dXBJRnJhbWVPYmplY3QiLCJGdW5jdGlvbiIsInByb3RvdHlwZSIsImJpbmQiLCJpRnJhbWVSZXNpemVyIiwiY2xvc2UiLCJhbmNob3IiLCJzZW5kTWVzc2FnZSIsImluaXQiLCJpRnJhbWVMb2FkZWQiLCJjaGVja09wdGlvbnMiLCJjb3B5T3B0aW9ucyIsIm9wdGlvbiIsImhhc093blByb3BlcnR5IiwiZ2V0VGFyZ2V0T3JpZ2luIiwicHJvY2Vzc09wdGlvbnMiLCJzbGljZSIsImpvaW4iLCJiZWVuSGVyZSIsImZuIiwidGltZSIsInNldFRpbWVvdXQiLCJjaGVja0lGcmFtZXMiLCJjaGVja0lGcmFtZSIsInNldHRpbmdJZCIsImNoa0RpbWVuc2lvbiIsImlzVmlzaWJsZSIsIm9mZnNldFBhcmVudCIsIm11dGF0aW9uT2JzZXJ2ZWQiLCJtdXRhdGlvbnMiLCJjcmVhdGVNdXRhdGlvbk9ic2VydmVyIiwicXVlcnlTZWxlY3RvciIsImNvbmZpZyIsImF0dHJpYnV0ZXMiLCJhdHRyaWJ1dGVPbGRWYWx1ZSIsImNoYXJhY3RlckRhdGEiLCJjaGFyYWN0ZXJEYXRhT2xkVmFsdWUiLCJjaGlsZExpc3QiLCJzdWJ0cmVlIiwib2JzZXJ2ZXIiLCJNdXRhdGlvbk9ic2VydmVyIiwib2JzZXJ2ZSIsIldlYktpdE11dGF0aW9uT2JzZXJ2ZXIiLCJyZXNpemVJRnJhbWVzIiwic2VuZFRyaWdnZXJNc2ciLCJ0YWJWaXNpYmxlIiwidmlzaWJpbGl0eVN0YXRlIiwiZXZlbnROYW1lIiwiaXNJRnJhbWVSZXNpemVFbmFibGVkIiwic2V0dXBFdmVudExpc3RlbmVycyIsImZhY3RvcnkiLCJlbGVtZW50IiwiY2hrVHlwZSIsInRhZ05hbWUiLCJ0b1VwcGVyQ2FzZSIsImlGcmFtZXMiLCJwdXNoIiwid2FybkRlcHJlY2F0ZWRPcHRpb25zIiwiaUZyYW1lUmVzaXplRiIsImNhbGwiLCJxdWVyeVNlbGVjdG9yQWxsIiwiY3JlYXRlSlF1ZXJ5UHVibGljTWV0aG9kIiwiJCIsImlGcmFtZVJlc2l6ZSIsIiRpRnJhbWVSZXNpemVGIiwiaW5kZXgiLCJmaWx0ZXIiLCJlYWNoIiwiZW5kIiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiOzs7O0FBQUE7Ozs7Ozs7Ozs7QUFXQSxDQUFDLENBQUMsVUFBU0EsTUFBVCxFQUFpQjtBQUNsQjs7QUFFQSxLQUNDQyxRQUF3QixDQUR6QjtBQUFBLEtBRUNDLGFBQXdCLEtBRnpCO0FBQUEsS0FHQ0MscUJBQXdCLEtBSHpCO0FBQUEsS0FJQ0MsWUFBd0IsU0FKekI7QUFBQSxLQUtDQyxlQUF3QkQsVUFBVUUsTUFMbkM7QUFBQSxLQU1DQyxRQUF3QixlQU56QjtBQUFBLEtBTTBDO0FBQ3pDQyxZQUF3QkQsTUFBTUQsTUFQL0I7QUFBQSxLQVFDRyxlQUF3QixJQVJ6QjtBQUFBLEtBU0NDLHdCQUF3QlYsT0FBT1UscUJBVGhDO0FBQUEsS0FVQ0MsdUJBQXdCLEVBQUNDLEtBQUksQ0FBTCxFQUFPQyxRQUFPLENBQWQsRUFBZ0JDLFlBQVcsQ0FBM0IsRUFBNkJDLHVCQUFzQixDQUFuRCxFQVZ6QjtBQUFBLEtBV0NDLFdBQXdCLEVBWHpCO0FBQUEsS0FZQ0MsUUFBd0IsSUFaekI7QUFBQSxLQWFDQyxRQUF3QixXQWJ6QjtBQUFBLEtBZUNDLFdBQXdCO0FBQ3ZCQyxjQUE0QixJQURMO0FBRXZCQyxrQkFBNEIsSUFGTDtBQUd2QkMsY0FBNEIsSUFITDtBQUl2QkMsZ0JBQTRCLENBSkw7QUFLdkJDLGVBQTRCLElBTEw7QUFNdkJDLGVBQTRCLElBTkw7QUFPdkJDLGVBQTRCLEtBUEw7QUFRdkJDLHVCQUE0QixJQVJMO0FBU3ZCQywyQkFBNEIsWUFUTDtBQVV2QkMsTUFBNEIsZUFWTDtBQVd2QkMsWUFBNEIsRUFYTDtBQVl2QkMsT0FBNEIsS0FaTDtBQWF2QkMsYUFBNEJDLFFBYkw7QUFjdkJDLFlBQTRCRCxRQWRMO0FBZXZCRSxhQUE0QixDQWZMO0FBZ0J2QkMsWUFBNEIsQ0FoQkw7QUFpQnZCQyxjQUE0QixRQWpCTDtBQWtCdkJDLGFBQTRCLEtBbEJMO0FBbUJ2QkMsY0FBNEIsSUFuQkw7QUFvQnZCQyxhQUE0QixLQXBCTDtBQXFCdkJDLGFBQTRCLENBckJMO0FBc0J2QkMsMEJBQTRCLFFBdEJMO0FBdUJ2QkMsa0JBQTRCLDBCQUFVLENBQUUsQ0F2QmpCO0FBd0J2QkMsZ0JBQTRCLHdCQUFVLENBQUUsQ0F4QmpCO0FBeUJ2QkMsbUJBQTRCLDJCQUFVO0FBQUNDLFFBQUssc0NBQUw7QUFBOEMsR0F6QjlEO0FBMEJ2QkMsbUJBQTRCLDJCQUFVLENBQUUsQ0ExQmpCO0FBMkJ2QkMsa0JBQTRCLDBCQUFVO0FBQUMsVUFBTyxJQUFQO0FBQWE7QUEzQjdCLEVBZnpCOztBQTZDQSxVQUFTQyxnQkFBVCxDQUEwQkMsR0FBMUIsRUFBOEJDLEdBQTlCLEVBQWtDQyxJQUFsQyxFQUF1QztBQUN0Qyw0QkFEc0MsQ0FDWDtBQUMzQixNQUFJLHNCQUFzQnBELE1BQTFCLEVBQWlDO0FBQ2hDa0QsT0FBSUQsZ0JBQUosQ0FBcUJFLEdBQXJCLEVBQXlCQyxJQUF6QixFQUErQixLQUEvQjtBQUNBLEdBRkQsTUFFTyxJQUFJLGlCQUFpQnBELE1BQXJCLEVBQTRCO0FBQUM7QUFDbkNrRCxPQUFJRyxXQUFKLENBQWdCLE9BQUtGLEdBQXJCLEVBQXlCQyxJQUF6QjtBQUNBO0FBQ0Q7O0FBRUQsVUFBU0UsbUJBQVQsQ0FBNkJDLEVBQTdCLEVBQWdDSixHQUFoQyxFQUFvQ0MsSUFBcEMsRUFBeUM7QUFDeEMsNEJBRHdDLENBQ2I7QUFDM0IsTUFBSSx5QkFBeUJwRCxNQUE3QixFQUFvQztBQUNuQ3VELE1BQUdELG1CQUFILENBQXVCSCxHQUF2QixFQUEyQkMsSUFBM0IsRUFBaUMsS0FBakM7QUFDQSxHQUZELE1BRU8sSUFBSSxpQkFBaUJwRCxNQUFyQixFQUE0QjtBQUFFO0FBQ3BDdUQsTUFBR0MsV0FBSCxDQUFlLE9BQUtMLEdBQXBCLEVBQXdCQyxJQUF4QjtBQUNBO0FBQ0Q7O0FBRUQsVUFBU0ssMEJBQVQsR0FBcUM7QUFDcEMsTUFDQ0MsVUFBVSxDQUFDLEtBQUQsRUFBUSxRQUFSLEVBQWtCLEdBQWxCLEVBQXVCLElBQXZCLENBRFg7QUFBQSxNQUVDQyxDQUZEOztBQUlBO0FBQ0EsT0FBS0EsSUFBSSxDQUFULEVBQVlBLElBQUlELFFBQVFwRCxNQUFaLElBQXNCLENBQUNJLHFCQUFuQyxFQUEwRGlELEtBQUssQ0FBL0QsRUFBa0U7QUFDakVqRCwyQkFBd0JWLE9BQU8wRCxRQUFRQyxDQUFSLElBQWEsdUJBQXBCLENBQXhCO0FBQ0E7O0FBRUQsTUFBSSxDQUFFakQscUJBQU4sRUFBNkI7QUFDNUJxQixPQUFJLE9BQUosRUFBWSxxQ0FBWjtBQUNBO0FBQ0Q7O0FBRUQsVUFBUzZCLE9BQVQsQ0FBaUJDLFFBQWpCLEVBQTBCO0FBQ3pCLE1BQUlDLFNBQVMsZ0JBQWNELFFBQTNCOztBQUVBLE1BQUk3RCxPQUFPK0QsR0FBUCxLQUFlL0QsT0FBT2dFLElBQTFCLEVBQStCO0FBQzlCLE9BQUloRSxPQUFPaUUsWUFBUCxJQUF1QmpFLE9BQU9pRSxZQUFQLENBQW9CQyxLQUEvQyxFQUFxRDtBQUNwREosYUFBUzlELE9BQU9pRSxZQUFQLENBQW9CQyxLQUFwQixLQUE0QixJQUE1QixHQUFpQ0wsUUFBMUM7QUFDQSxJQUZELE1BRU87QUFDTkMsYUFBUyx1QkFBcUJELFFBQTlCO0FBQ0E7QUFDRDs7QUFFRCxTQUFPQyxNQUFQO0FBQ0E7O0FBRUQsVUFBU0ssZUFBVCxDQUF5Qk4sUUFBekIsRUFBa0M7QUFDakMsU0FBT3RELFFBQVEsR0FBUixHQUFjcUQsUUFBUUMsUUFBUixDQUFkLEdBQWtDLEdBQXpDO0FBQ0E7O0FBRUQsVUFBU08sWUFBVCxDQUFzQlAsUUFBdEIsRUFBK0I7QUFDOUIsU0FBTzdDLFNBQVM2QyxRQUFULElBQXFCN0MsU0FBUzZDLFFBQVQsRUFBbUI5QixHQUF4QyxHQUE4QzdCLFVBQXJEO0FBQ0E7O0FBRUQsVUFBUzZCLEdBQVQsQ0FBYThCLFFBQWIsRUFBc0JRLEdBQXRCLEVBQTBCO0FBQ3pCQyxTQUFPLEtBQVAsRUFBYVQsUUFBYixFQUFzQlEsR0FBdEIsRUFBMEJELGFBQWFQLFFBQWIsQ0FBMUI7QUFDQTs7QUFFRCxVQUFTVSxJQUFULENBQWNWLFFBQWQsRUFBdUJRLEdBQXZCLEVBQTJCO0FBQzFCQyxTQUFPLE1BQVAsRUFBY1QsUUFBZCxFQUF1QlEsR0FBdkIsRUFBMkJELGFBQWFQLFFBQWIsQ0FBM0I7QUFDQTs7QUFFRCxVQUFTZixJQUFULENBQWNlLFFBQWQsRUFBdUJRLEdBQXZCLEVBQTJCO0FBQzFCQyxTQUFPLE1BQVAsRUFBY1QsUUFBZCxFQUF1QlEsR0FBdkIsRUFBMkIsSUFBM0I7QUFDQTs7QUFFRCxVQUFTQyxNQUFULENBQWdCRSxJQUFoQixFQUFxQlgsUUFBckIsRUFBOEJRLEdBQTlCLEVBQWtDSSxPQUFsQyxFQUEwQztBQUN6QyxNQUFJLFNBQVNBLE9BQVQsSUFBb0IscUJBQW9CekUsT0FBTzBFLE9BQTNCLENBQXhCLEVBQTJEO0FBQzFEQSxXQUFRRixJQUFSLEVBQWNMLGdCQUFnQk4sUUFBaEIsQ0FBZCxFQUF3Q1EsR0FBeEM7QUFDQTtBQUNEOztBQUVELFVBQVNNLGNBQVQsQ0FBd0JDLEtBQXhCLEVBQThCO0FBQzdCLFdBQVNDLFlBQVQsR0FBdUI7QUFDdEIsWUFBU0MsTUFBVCxHQUFpQjtBQUNoQkMsWUFBUUMsV0FBUjtBQUNBQyxvQkFBZ0JwQixRQUFoQjtBQUNBOztBQUVEcUIsaUJBQWMsUUFBZDtBQUNBQSxpQkFBYyxPQUFkOztBQUVBQyxjQUFXTCxNQUFYLEVBQWtCRSxXQUFsQixFQUE4QixNQUE5QjtBQUNBOztBQUVELFdBQVNJLFVBQVQsR0FBcUI7QUFDcEIsT0FBSUMsT0FBT2hCLElBQUlpQixNQUFKLENBQVc5RSxRQUFYLEVBQXFCK0UsS0FBckIsQ0FBMkIsR0FBM0IsQ0FBWDs7QUFFQSxVQUFPO0FBQ05DLFlBQVF4RSxTQUFTcUUsS0FBSyxDQUFMLENBQVQsRUFBa0JHLE1BRHBCO0FBRU4zRCxRQUFRd0QsS0FBSyxDQUFMLENBRkY7QUFHTkksWUFBUUosS0FBSyxDQUFMLENBSEY7QUFJTkssV0FBUUwsS0FBSyxDQUFMLENBSkY7QUFLTmIsVUFBUWEsS0FBSyxDQUFMO0FBTEYsSUFBUDtBQU9BOztBQUVELFdBQVNILGFBQVQsQ0FBdUJTLFNBQXZCLEVBQWlDO0FBQ2hDLE9BQ0MvRSxNQUFPZ0YsT0FBTzVFLFNBQVM2QyxRQUFULEVBQW1CLFFBQVE4QixTQUEzQixDQUFQLENBRFI7QUFBQSxPQUVDRSxNQUFPRCxPQUFPNUUsU0FBUzZDLFFBQVQsRUFBbUIsUUFBUThCLFNBQTNCLENBQVAsQ0FGUjtBQUFBLE9BR0NHLFlBQVlILFVBQVVJLFdBQVYsRUFIYjtBQUFBLE9BSUNDLE9BQU9KLE9BQU9aLFlBQVljLFNBQVosQ0FBUCxDQUpSOztBQU1BL0QsT0FBSThCLFFBQUosRUFBYSxjQUFjaUMsU0FBZCxHQUEwQixlQUExQixHQUE0Q0QsR0FBNUMsR0FBa0QsR0FBbEQsR0FBd0RqRixHQUFyRTs7QUFFQSxPQUFJb0YsT0FBS0gsR0FBVCxFQUFjO0FBQ2JHLFdBQUtILEdBQUw7QUFDQTlELFFBQUk4QixRQUFKLEVBQWEsU0FBU2lDLFNBQVQsR0FBcUIsZUFBbEM7QUFDQTs7QUFFRCxPQUFJRSxPQUFLcEYsR0FBVCxFQUFjO0FBQ2JvRixXQUFLcEYsR0FBTDtBQUNBbUIsUUFBSThCLFFBQUosRUFBYSxTQUFTaUMsU0FBVCxHQUFxQixlQUFsQztBQUNBOztBQUVEZCxlQUFZYyxTQUFaLElBQXlCLEtBQUtFLElBQTlCO0FBQ0E7O0FBR0QsV0FBU0MsbUJBQVQsR0FBOEI7QUFDN0IsWUFBU0Msa0JBQVQsR0FBNkI7QUFDNUIsYUFBU0MsU0FBVCxHQUFvQjtBQUNuQixTQUNDQyxJQUFJLENBREw7QUFBQSxTQUVDQyxVQUFVLEtBRlg7O0FBSUF0RSxTQUFJOEIsUUFBSixFQUFhLDBEQUEwRHBDLFdBQXZFOztBQUVBLFlBQU8yRSxJQUFJM0UsWUFBWW5CLE1BQXZCLEVBQStCOEYsR0FBL0IsRUFBb0M7QUFDbkMsVUFBSTNFLFlBQVkyRSxDQUFaLE1BQW1CRSxNQUF2QixFQUErQjtBQUM5QkQsaUJBQVUsSUFBVjtBQUNBO0FBQ0E7QUFDRDtBQUNELFlBQU9BLE9BQVA7QUFDQTs7QUFFRCxhQUFTRSxXQUFULEdBQXNCO0FBQ3JCLFNBQUlDLGFBQWN4RixTQUFTNkMsUUFBVCxFQUFtQjJDLFVBQXJDO0FBQ0F6RSxTQUFJOEIsUUFBSixFQUFhLGtDQUFnQzJDLFVBQTdDO0FBQ0EsWUFBT0YsV0FBV0UsVUFBbEI7QUFDQTs7QUFFRCxXQUFPL0UsWUFBWWdGLFdBQVosS0FBNEJDLEtBQTVCLEdBQW9DUCxXQUFwQyxHQUFrREksYUFBekQ7QUFDQTs7QUFFRCxPQUNDRCxTQUFjMUIsTUFBTTBCLE1BRHJCO0FBQUEsT0FFQzdFLGNBQWNULFNBQVM2QyxRQUFULEVBQW1CcEMsV0FGbEM7O0FBSUEsT0FBSUEsZUFBZ0IsS0FBRzZFLE1BQUgsS0FBYyxNQUE5QixJQUF5QyxDQUFDSixvQkFBOUMsRUFBb0U7QUFDbkUsVUFBTSxJQUFJUyxLQUFKLENBQ0wsdUNBQXVDTCxNQUF2QyxHQUNBLE9BREEsR0FDVXRCLFlBQVlRLE1BQVosQ0FBbUIzRCxFQUQ3QixHQUVBLGlCQUZBLEdBRW9CK0MsTUFBTVMsSUFGMUIsR0FHQSxvSEFKSyxDQUFOO0FBTUE7O0FBRUQsVUFBTyxJQUFQO0FBQ0E7O0FBRUQsV0FBU3VCLGNBQVQsR0FBeUI7QUFDeEIsVUFBT3JHLFVBQVcsQ0FBQyxLQUFLOEQsR0FBTixFQUFXaUIsTUFBWCxDQUFrQixDQUFsQixFQUFvQjlFLFFBQXBCLENBQVgsSUFBOEM2RCxJQUFJaUIsTUFBSixDQUFXOUUsUUFBWCxFQUFxQitFLEtBQXJCLENBQTJCLEdBQTNCLEVBQWdDLENBQWhDLEtBQXNDdkUsUUFBM0YsQ0FEd0IsQ0FDOEU7QUFDdEc7O0FBRUQsV0FBUzZGLHVCQUFULEdBQWtDO0FBQ2pDO0FBQ0E7QUFDQSxPQUFJUixVQUFVckIsWUFBWVIsSUFBWixJQUFvQixFQUFDLFFBQU8sQ0FBUixFQUFVLFNBQVEsQ0FBbEIsRUFBb0IsYUFBWSxDQUFoQyxFQUFsQzs7QUFFQSxPQUFJNkIsT0FBSixFQUFZO0FBQ1h0RSxRQUFJOEIsUUFBSixFQUFhLDZDQUFiO0FBQ0E7O0FBRUQsVUFBT3dDLE9BQVA7QUFDQTs7QUFFRCxXQUFTUyxVQUFULENBQW9CQyxNQUFwQixFQUEyQjtBQUMxQixVQUFPMUMsSUFBSWlCLE1BQUosQ0FBV2pCLElBQUkyQyxPQUFKLENBQVksR0FBWixJQUFpQjNHLFlBQWpCLEdBQThCMEcsTUFBekMsQ0FBUDtBQUNBOztBQUVELFdBQVNFLG9CQUFULENBQThCQyxPQUE5QixFQUFzQztBQUNyQ25GLE9BQUk4QixRQUFKLEVBQWEsc0NBQXFDbUIsWUFBWVEsTUFBWixDQUFtQjNELEVBQXhELEdBQTZELGFBQTdELEdBQTZFcUYsT0FBN0UsR0FBdUYsR0FBcEc7QUFDQUMsWUFBUyxpQkFBVCxFQUEyQjtBQUMxQjNCLFlBQVFSLFlBQVlRLE1BRE07QUFFMUI0QixhQUFTQyxLQUFLQyxLQUFMLENBQVdKLE9BQVg7QUFGaUIsSUFBM0I7QUFJQW5GLE9BQUk4QixRQUFKLEVBQWEsSUFBYjtBQUNBOztBQUVELFdBQVMwRCxXQUFULEdBQXNCO0FBQ3JCLE9BQ0NDLGVBQWlCQyxTQUFTQyxJQUFULENBQWNDLHFCQUFkLEVBRGxCO0FBQUEsT0FFQ0MsaUJBQWlCNUMsWUFBWVEsTUFBWixDQUFtQm1DLHFCQUFuQixFQUZsQjs7QUFJQSxVQUFPTixLQUFLUSxTQUFMLENBQWU7QUFDckJDLGtCQUFjRixlQUFlbkMsTUFEUjtBQUVyQnNDLGlCQUFjSCxlQUFlbEMsS0FGUjtBQUdyQnNDLGtCQUFjQyxLQUFLckgsR0FBTCxDQUFTNkcsU0FBU1MsZUFBVCxDQUF5QkYsWUFBbEMsRUFBZ0RoSSxPQUFPbUksV0FBUCxJQUFzQixDQUF0RSxDQUhPO0FBSXJCQyxpQkFBY0gsS0FBS3JILEdBQUwsQ0FBUzZHLFNBQVNTLGVBQVQsQ0FBeUJFLFdBQWxDLEVBQWdEcEksT0FBT3FJLFVBQVAsSUFBc0IsQ0FBdEUsQ0FKTztBQUtyQkMsZUFBY0MsU0FBU1gsZUFBZTdELEdBQWYsR0FBc0J5RCxhQUFhekQsR0FBNUMsRUFBa0QsRUFBbEQsQ0FMTztBQU1yQnlFLGdCQUFjRCxTQUFTWCxlQUFlYSxJQUFmLEdBQXNCakIsYUFBYWlCLElBQTVDLEVBQWtELEVBQWxELENBTk87QUFPckJDLGVBQWMxSSxPQUFPMkksV0FQQTtBQVFyQkMsZ0JBQWM1SSxPQUFPNkk7QUFSQSxJQUFmLENBQVA7QUFVQTs7QUFFRCxXQUFTQyxvQkFBVCxDQUE4QnRELE1BQTlCLEVBQXFDM0IsUUFBckMsRUFBOEM7QUFDN0MsWUFBU2tGLGdCQUFULEdBQTJCO0FBQzFCQyxZQUNDLGdCQURELEVBRUMsY0FBY3pCLGFBRmYsRUFHQy9CLE1BSEQsRUFJQzNCLFFBSkQ7QUFNQTs7QUFFRG9GLFdBQVFGLGdCQUFSLEVBQXlCLEVBQXpCO0FBQ0E7O0FBR0QsV0FBU0csb0JBQVQsR0FBK0I7QUFDOUIsWUFBU0MsV0FBVCxDQUFxQjNFLElBQXJCLEVBQTBCcEIsSUFBMUIsRUFBK0I7QUFDOUIsYUFBU2dHLFlBQVQsR0FBdUI7QUFDdEIsU0FBSXBJLFNBQVNhLEVBQVQsQ0FBSixFQUFpQjtBQUNoQmlILDJCQUFxQjlILFNBQVNhLEVBQVQsRUFBYTJELE1BQWxDLEVBQXlDM0QsRUFBekM7QUFDQSxNQUZELE1BRU87QUFDTndIO0FBQ0E7QUFDRDs7QUFFRCxLQUFDLFFBQUQsRUFBVSxRQUFWLEVBQW9CQyxPQUFwQixDQUE0QixVQUFTbkcsR0FBVCxFQUFhO0FBQ3hDcEIsU0FBSUYsRUFBSixFQUFRMkMsT0FBUXJCLEdBQVIsR0FBYyw0QkFBdEI7QUFDQUMsVUFBS3BELE1BQUwsRUFBWW1ELEdBQVosRUFBZ0JpRyxZQUFoQjtBQUNBLEtBSEQ7QUFJQTs7QUFFRCxZQUFTQyxJQUFULEdBQWU7QUFDZEYsZ0JBQVksU0FBWixFQUF1QjdGLG1CQUF2QjtBQUNBOztBQUVELFlBQVNpRyxLQUFULEdBQWdCO0FBQ2ZKLGdCQUFZLE1BQVosRUFBb0JsRyxnQkFBcEI7QUFDQTs7QUFFRCxPQUFJcEIsS0FBS2dDLFFBQVQsQ0F4QjhCLENBd0JYOztBQUVuQjBGOztBQUVBdkksWUFBU2EsRUFBVCxFQUFhMkgsWUFBYixHQUE0QkgsSUFBNUI7QUFDQTs7QUFFRCxXQUFTSSxtQkFBVCxHQUE4QjtBQUM3QixPQUFJekksU0FBUzZDLFFBQVQsS0FBc0I3QyxTQUFTNkMsUUFBVCxFQUFtQjJGLFlBQTdDLEVBQTBEO0FBQ3pEeEksYUFBUzZDLFFBQVQsRUFBbUIyRixZQUFuQjtBQUNBLFdBQU94SSxTQUFTNkMsUUFBVCxFQUFtQjJGLFlBQTFCO0FBQ0E7QUFDRDs7QUFFRCxXQUFTRSxpQkFBVCxHQUE0QjtBQUMzQixPQUFJQyxVQUFVLElBQWQ7O0FBRUEsT0FBSSxTQUFTM0UsWUFBWVEsTUFBekIsRUFBaUM7QUFDaEMxQyxTQUFLZSxRQUFMLEVBQWMsYUFBV21CLFlBQVluRCxFQUF2QixHQUEwQixhQUF4QztBQUNBOEgsY0FBVSxLQUFWO0FBQ0E7QUFDRCxVQUFPQSxPQUFQO0FBQ0E7O0FBRUQsV0FBU0Msa0JBQVQsQ0FBNEJDLE1BQTVCLEVBQW1DO0FBQ2xDLE9BQUlqQyxpQkFBaUJpQyxPQUFPbEMscUJBQVAsRUFBckI7O0FBRUFtQyxtQkFBZ0JqRyxRQUFoQjs7QUFFQSxVQUFPO0FBQ05GLE9BQUdzRSxLQUFLOEIsS0FBTCxDQUFZbkUsT0FBT2dDLGVBQWVhLElBQXRCLElBQThCN0MsT0FBT25GLGFBQWFrRCxDQUFwQixDQUExQyxDQURHO0FBRU5xRyxPQUFHL0IsS0FBSzhCLEtBQUwsQ0FBWW5FLE9BQU9nQyxlQUFlN0QsR0FBdEIsSUFBOEI2QixPQUFPbkYsYUFBYXVKLENBQXBCLENBQTFDO0FBRkcsSUFBUDtBQUlBOztBQUVELFdBQVNDLHNCQUFULENBQWdDQyxTQUFoQyxFQUEwQztBQUN6Qyw2QkFEeUMsQ0FDYjtBQUM1QixZQUFTQyxVQUFULEdBQXFCO0FBQ3BCMUosbUJBQWUySixXQUFmO0FBQ0FDO0FBQ0F0SSxRQUFJOEIsUUFBSixFQUFhLElBQWI7QUFDQTs7QUFFRCxZQUFTeUcsVUFBVCxHQUFxQjtBQUNwQixXQUFPO0FBQ04zRyxRQUFHaUMsT0FBT1osWUFBWVUsS0FBbkIsSUFBNEJxQixPQUFPcEQsQ0FEaEM7QUFFTnFHLFFBQUdwRSxPQUFPWixZQUFZUyxNQUFuQixJQUE2QnNCLE9BQU9pRDtBQUZqQyxLQUFQO0FBSUE7O0FBRUQsWUFBU08sWUFBVCxHQUF1QjtBQUN0QixRQUFJdkssT0FBT2lFLFlBQVgsRUFBd0I7QUFDdkJqRSxZQUFPaUUsWUFBUCxDQUFvQixjQUFZaUcsWUFBVSxRQUFWLEdBQW1CLEVBQS9CLENBQXBCLEVBQXdERSxZQUFZekcsQ0FBcEUsRUFBc0V5RyxZQUFZSixDQUFsRjtBQUNBLEtBRkQsTUFFTztBQUNObEgsVUFBS2UsUUFBTCxFQUFjLHVFQUFkO0FBQ0E7QUFDRDs7QUFFRCxPQUNDa0QsU0FBU21ELFlBQVlOLG1CQUFtQjVFLFlBQVlRLE1BQS9CLENBQVosR0FBcUQsRUFBQzdCLEdBQUUsQ0FBSCxFQUFLcUcsR0FBRSxDQUFQLEVBRC9EO0FBQUEsT0FFQ0ksY0FBY0UsWUFGZjs7QUFJQXZJLE9BQUk4QixRQUFKLEVBQWEsZ0RBQThDa0QsT0FBT3BELENBQXJELEdBQXVELEtBQXZELEdBQTZEb0QsT0FBT2lELENBQXBFLEdBQXNFLEdBQW5GOztBQUVBLE9BQUdoSyxPQUFPK0QsR0FBUCxLQUFlL0QsT0FBT2dFLElBQXpCLEVBQThCO0FBQzdCdUc7QUFDQSxJQUZELE1BRU87QUFDTko7QUFDQTtBQUNEOztBQUVELFdBQVNFLFFBQVQsR0FBbUI7QUFDbEIsT0FBSSxVQUFVbEQsU0FBUyxnQkFBVCxFQUEwQjFHLFlBQTFCLENBQWQsRUFBc0Q7QUFDckR3RSxvQkFBZ0JwQixRQUFoQjtBQUNBLElBRkQsTUFFTztBQUNOMkc7QUFDQTtBQUNEOztBQUVELFdBQVNDLFVBQVQsQ0FBb0JDLFFBQXBCLEVBQTZCO0FBQzVCLFlBQVNDLFlBQVQsR0FBdUI7QUFDdEIsUUFBSUMsZUFBZWhCLG1CQUFtQkMsTUFBbkIsQ0FBbkI7O0FBRUE5SCxRQUFJOEIsUUFBSixFQUFhLDhCQUE0QmdILElBQTVCLEdBQWlDLFVBQWpDLEdBQTRDRCxhQUFhakgsQ0FBekQsR0FBMkQsTUFBM0QsR0FBa0VpSCxhQUFhWixDQUE1RjtBQUNBdkosbUJBQWU7QUFDZGtELFFBQUdpSCxhQUFhakgsQ0FERjtBQUVkcUcsUUFBR1ksYUFBYVo7QUFGRixLQUFmOztBQUtBSztBQUNBdEksUUFBSThCLFFBQUosRUFBYSxJQUFiO0FBQ0E7O0FBRUQsWUFBU2lILFlBQVQsR0FBdUI7QUFDdEIsUUFBSTlLLE9BQU9pRSxZQUFYLEVBQXdCO0FBQ3ZCakUsWUFBT2lFLFlBQVAsQ0FBb0I4RyxZQUFwQixDQUFpQ0YsSUFBakM7QUFDQSxLQUZELE1BRU87QUFDTjlJLFNBQUk4QixRQUFKLEVBQWEsbUJBQWlCZ0gsSUFBakIsR0FBc0IsOENBQW5DO0FBQ0E7QUFDRDs7QUFFRCxPQUNDQSxPQUFXSCxTQUFTbkYsS0FBVCxDQUFlLEdBQWYsRUFBb0IsQ0FBcEIsS0FBMEIsRUFEdEM7QUFBQSxPQUVDeUYsV0FBV0MsbUJBQW1CSixJQUFuQixDQUZaO0FBQUEsT0FHQ2hCLFNBQVdwQyxTQUFTeUQsY0FBVCxDQUF3QkYsUUFBeEIsS0FBcUN2RCxTQUFTMEQsaUJBQVQsQ0FBMkJILFFBQTNCLEVBQXFDLENBQXJDLENBSGpEOztBQUtBLE9BQUluQixNQUFKLEVBQVc7QUFDVmM7QUFDQSxJQUZELE1BRU8sSUFBRzNLLE9BQU8rRCxHQUFQLEtBQWEvRCxPQUFPZ0UsSUFBdkIsRUFBNEI7QUFDbEM4RztBQUNBLElBRk0sTUFFQTtBQUNOL0ksUUFBSThCLFFBQUosRUFBYSxtQkFBaUJnSCxJQUFqQixHQUFzQixZQUFuQztBQUNBO0FBQ0Q7O0FBRUQsV0FBUzFELFFBQVQsQ0FBa0JpRSxRQUFsQixFQUEyQkMsR0FBM0IsRUFBK0I7QUFDOUIsVUFBT0MsWUFBWXpILFFBQVosRUFBcUJ1SCxRQUFyQixFQUE4QkMsR0FBOUIsQ0FBUDtBQUNBOztBQUVELFdBQVNFLFNBQVQsR0FBb0I7O0FBRW5CLE9BQUd2SyxTQUFTNkMsUUFBVCxFQUFtQjJILFFBQXRCLEVBQWdDQTs7QUFFaEMsV0FBT3hHLFlBQVlSLElBQW5CO0FBQ0EsU0FBSyxPQUFMO0FBQ0NpSCxpQkFBWXpHLFlBQVlRLE1BQXhCO0FBQ0E7QUFDRCxTQUFLLFNBQUw7QUFDQ3lCLDBCQUFxQkgsV0FBVyxDQUFYLENBQXJCO0FBQ0E7QUFDRCxTQUFLLFVBQUw7QUFDQ21ELDRCQUF1QixLQUF2QjtBQUNBO0FBQ0QsU0FBSyxnQkFBTDtBQUNDQSw0QkFBdUIsSUFBdkI7QUFDQTtBQUNELFNBQUssVUFBTDtBQUNDbkIsMEJBQXFCOUgsU0FBUzZDLFFBQVQsRUFBbUIyQixNQUF4QyxFQUErQzNCLFFBQS9DO0FBQ0FxRjtBQUNBO0FBQ0QsU0FBSyxjQUFMO0FBQ0NPO0FBQ0E7QUFDRCxTQUFLLFlBQUw7QUFDQ2dCLGdCQUFXM0QsV0FBVyxDQUFYLENBQVg7QUFDQTtBQUNELFNBQUssT0FBTDtBQUNDNEUsaUJBQVkxRyxXQUFaO0FBQ0E7QUFDRCxTQUFLLE1BQUw7QUFDQ0g7QUFDQXNDLGNBQVMsY0FBVCxFQUF3Qm5DLFlBQVlRLE1BQXBDO0FBQ0EyQixjQUFTLGlCQUFULEVBQTJCbkMsV0FBM0I7QUFDQTtBQUNEO0FBQ0NIO0FBQ0FzQyxjQUFTLGlCQUFULEVBQTJCbkMsV0FBM0I7QUFqQ0Q7QUFtQ0E7O0FBRUQsV0FBUzJHLFdBQVQsQ0FBcUI5SCxRQUFyQixFQUE4QjtBQUM3QixPQUFJOEYsVUFBVSxJQUFkOztBQUVBLE9BQUksQ0FBQzNJLFNBQVM2QyxRQUFULENBQUwsRUFBd0I7QUFDdkI4RixjQUFVLEtBQVY7QUFDQTdHLFNBQUtrQyxZQUFZUixJQUFaLEdBQW1CLG1CQUFuQixHQUF5Q1gsUUFBekMsR0FBb0QsaUJBQXBELEdBQXdFUSxHQUE3RTtBQUNBOztBQUVELFVBQU9zRixPQUFQO0FBQ0E7O0FBRUQsV0FBU2lDLHNCQUFULEdBQWlDO0FBQ2hDLFFBQUssSUFBSS9ILFFBQVQsSUFBcUI3QyxRQUFyQixFQUE4QjtBQUM3QmdJLFlBQVEsdUJBQVIsRUFBZ0M2QyxrQkFBa0JoSSxRQUFsQixDQUFoQyxFQUE0RDRELFNBQVN5RCxjQUFULENBQXdCckgsUUFBeEIsQ0FBNUQsRUFBOEZBLFFBQTlGO0FBQ0E7QUFDRDs7QUFFRCxXQUFTMkgsUUFBVCxHQUFvQjtBQUNuQnhLLFlBQVM2QyxRQUFULEVBQW1CMkgsUUFBbkIsR0FBOEIsS0FBOUI7QUFDQTs7QUFFRCxNQUNDbkgsTUFBTU8sTUFBTVMsSUFEYjtBQUFBLE1BRUNMLGNBQWMsRUFGZjtBQUFBLE1BR0NuQixXQUFXLElBSFo7O0FBS0EsTUFBRyxnQ0FBZ0NRLEdBQW5DLEVBQXVDO0FBQ3RDdUg7QUFDQSxHQUZELE1BRU8sSUFBSWhGLGdCQUFKLEVBQXFCO0FBQzNCNUIsaUJBQWNJLFlBQWQ7QUFDQXZCLGNBQWMzQyxRQUFROEQsWUFBWW5ELEVBQWxDOztBQUVBLE9BQUksQ0FBQ2dGLHlCQUFELElBQThCOEUsWUFBWTlILFFBQVosQ0FBbEMsRUFBd0Q7QUFDdkQ5QixRQUFJOEIsUUFBSixFQUFhLGVBQWFRLEdBQTFCOztBQUVBLFFBQUtxRix1QkFBdUJ6RCxxQkFBNUIsRUFBbUQ7QUFDbERzRjtBQUNBO0FBQ0Q7QUFDRCxHQVhNLE1BV0E7QUFDTmhILFFBQUtWLFFBQUwsRUFBYyxjQUFZUSxHQUExQjtBQUNBO0FBRUQ7O0FBR0QsVUFBU2lILFdBQVQsQ0FBcUJ6SCxRQUFyQixFQUE4QnVILFFBQTlCLEVBQXVDQyxHQUF2QyxFQUEyQztBQUMxQyxNQUNDakksT0FBTyxJQURSO0FBQUEsTUFFQzBJLFNBQVMsSUFGVjs7QUFJQSxNQUFHOUssU0FBUzZDLFFBQVQsQ0FBSCxFQUFzQjtBQUNyQlQsVUFBT3BDLFNBQVM2QyxRQUFULEVBQW1CdUgsUUFBbkIsQ0FBUDs7QUFFQSxPQUFJLGVBQWUsT0FBT2hJLElBQTFCLEVBQStCO0FBQzlCMEksYUFBUzFJLEtBQUtpSSxHQUFMLENBQVQ7QUFDQSxJQUZELE1BRU87QUFDTixVQUFNLElBQUlVLFNBQUosQ0FBY1gsV0FBUyxhQUFULEdBQXVCdkgsUUFBdkIsR0FBZ0MscUJBQTlDLENBQU47QUFDQTtBQUNEOztBQUVELFNBQU9pSSxNQUFQO0FBQ0E7O0FBRUQsVUFBU0wsV0FBVCxDQUFxQmpHLE1BQXJCLEVBQTRCO0FBQzNCLE1BQUkzQixXQUFXMkIsT0FBTzNELEVBQXRCOztBQUVBRSxNQUFJOEIsUUFBSixFQUFhLHNCQUFvQkEsUUFBakM7QUFDQSxNQUFJMkIsT0FBT3dHLFVBQVgsRUFBdUI7QUFBRXhHLFVBQU93RyxVQUFQLENBQWtCQyxXQUFsQixDQUE4QnpHLE1BQTlCO0FBQXdDO0FBQ2pFOEYsY0FBWXpILFFBQVosRUFBcUIsZ0JBQXJCLEVBQXNDQSxRQUF0QztBQUNBOUIsTUFBSThCLFFBQUosRUFBYSxJQUFiO0FBQ0EsU0FBTzdDLFNBQVM2QyxRQUFULENBQVA7QUFDQTs7QUFFRCxVQUFTaUcsZUFBVCxDQUF5QmpHLFFBQXpCLEVBQWtDO0FBQ2pDLE1BQUcsU0FBU3BELFlBQVosRUFBeUI7QUFDeEJBLGtCQUFlO0FBQ2RrRCxPQUFJM0QsT0FBTzZJLFdBQVAsS0FBdUJxRCxTQUF4QixHQUFxQ2xNLE9BQU82SSxXQUE1QyxHQUEwRHBCLFNBQVNTLGVBQVQsQ0FBeUJVLFVBRHhFO0FBRWRvQixPQUFJaEssT0FBTzJJLFdBQVAsS0FBdUJ1RCxTQUF4QixHQUFxQ2xNLE9BQU8ySSxXQUE1QyxHQUEwRGxCLFNBQVNTLGVBQVQsQ0FBeUJRO0FBRnhFLElBQWY7QUFJQTNHLE9BQUk4QixRQUFKLEVBQWEsd0JBQXNCcEQsYUFBYWtELENBQW5DLEdBQXFDLEdBQXJDLEdBQXlDbEQsYUFBYXVKLENBQW5FO0FBQ0E7QUFDRDs7QUFFRCxVQUFTL0UsZUFBVCxDQUF5QnBCLFFBQXpCLEVBQWtDO0FBQ2pDLE1BQUcsU0FBU3BELFlBQVosRUFBeUI7QUFDeEJULFVBQU9xSyxRQUFQLENBQWdCNUosYUFBYWtELENBQTdCLEVBQStCbEQsYUFBYXVKLENBQTVDO0FBQ0FqSSxPQUFJOEIsUUFBSixFQUFhLHdCQUFzQnBELGFBQWFrRCxDQUFuQyxHQUFxQyxHQUFyQyxHQUF5Q2xELGFBQWF1SixDQUFuRTtBQUNBUTtBQUNBO0FBQ0Q7O0FBRUQsVUFBU0EsaUJBQVQsR0FBNEI7QUFDM0IvSixpQkFBZSxJQUFmO0FBQ0E7O0FBRUQsVUFBU2lMLFdBQVQsQ0FBcUIxRyxXQUFyQixFQUFpQztBQUNoQyxXQUFTbUgsS0FBVCxHQUFnQjtBQUNmcEgsV0FBUUMsV0FBUjtBQUNBZ0UsV0FBUSxPQUFSLEVBQWdCLE9BQWhCLEVBQXdCaEUsWUFBWVEsTUFBcEMsRUFBMkNSLFlBQVluRCxFQUF2RDtBQUNBOztBQUVERSxNQUFJaUQsWUFBWW5ELEVBQWhCLEVBQW1CLDhCQUE0QixXQUFTbUQsWUFBWVIsSUFBckIsR0FBMEIsV0FBMUIsR0FBc0MsUUFBbEUsQ0FBbkI7QUFDQXNGLGtCQUFnQjlFLFlBQVluRCxFQUE1QjtBQUNBc0QsYUFBV2dILEtBQVgsRUFBaUJuSCxXQUFqQixFQUE2QixPQUE3QjtBQUNBOztBQUVELFVBQVNELE9BQVQsQ0FBaUJDLFdBQWpCLEVBQTZCO0FBQzVCLFdBQVNvSCxZQUFULENBQXNCdEcsU0FBdEIsRUFBZ0M7QUFDL0JkLGVBQVlRLE1BQVosQ0FBbUI2RyxLQUFuQixDQUF5QnZHLFNBQXpCLElBQXNDZCxZQUFZYyxTQUFaLElBQXlCLElBQS9EO0FBQ0EvRCxPQUNDaUQsWUFBWW5ELEVBRGIsRUFFQyxhQUFhZ0MsUUFBYixHQUNBLElBREEsR0FDT2lDLFNBRFAsR0FFQSxVQUZBLEdBRWFkLFlBQVljLFNBQVosQ0FGYixHQUVzQyxJQUp2QztBQU1BOztBQUVELFdBQVN3RyxPQUFULENBQWlCeEcsU0FBakIsRUFBMkI7QUFDMUI7QUFDQTtBQUNBOztBQUVBLDZCQUwwQixDQUtFO0FBQzVCLE9BQUksQ0FBQzNGLGtCQUFELElBQXVCLFFBQVE2RSxZQUFZYyxTQUFaLENBQW5DLEVBQTBEO0FBQ3pEM0YseUJBQXFCLElBQXJCO0FBQ0E0QixRQUFJOEIsUUFBSixFQUFhLHNEQUFiO0FBQ0EwSTtBQUNBO0FBQ0Q7O0FBRUQsV0FBU0MsZ0JBQVQsQ0FBMEIxRyxTQUExQixFQUFvQztBQUNuQ3NHLGdCQUFhdEcsU0FBYjtBQUNBd0csV0FBUXhHLFNBQVI7QUFDQTs7QUFFRCxNQUFJakMsV0FBV21CLFlBQVlRLE1BQVosQ0FBbUIzRCxFQUFsQzs7QUFFQSxNQUFHYixTQUFTNkMsUUFBVCxDQUFILEVBQXNCO0FBQ3JCLE9BQUk3QyxTQUFTNkMsUUFBVCxFQUFtQnRCLFVBQXZCLEVBQW1DO0FBQUVpSyxxQkFBaUIsUUFBakI7QUFBNkI7QUFDbEUsT0FBSXhMLFNBQVM2QyxRQUFULEVBQW1CckIsU0FBdkIsRUFBbUM7QUFBRWdLLHFCQUFpQixPQUFqQjtBQUE0QjtBQUNqRTtBQUNEOztBQUVELFVBQVNySCxVQUFULENBQW9CL0IsSUFBcEIsRUFBeUI0QixXQUF6QixFQUFxQ3lILFNBQXJDLEVBQStDO0FBQzlDLDBCQUQ4QyxDQUNwQjtBQUMxQixNQUFHQSxjQUFZekgsWUFBWVIsSUFBeEIsSUFBZ0M5RCxxQkFBbkMsRUFBeUQ7QUFDeERxQixPQUFJaUQsWUFBWW5ELEVBQWhCLEVBQW1CLDRCQUFuQjtBQUNBbkIseUJBQXNCMEMsSUFBdEI7QUFDQSxHQUhELE1BR087QUFDTkE7QUFDQTtBQUNEOztBQUVELFVBQVM0RixPQUFULENBQWlCMEQsU0FBakIsRUFBMkJySSxHQUEzQixFQUErQm1CLE1BQS9CLEVBQXNDM0QsRUFBdEMsRUFBeUM7QUFDeEMsV0FBUzhLLG1CQUFULEdBQThCO0FBQzdCLE9BQUk5QyxTQUFTN0ksU0FBU2EsRUFBVCxFQUFhK0ssWUFBMUI7QUFDQTdLLE9BQUlGLEVBQUosRUFBTyxNQUFNNkssU0FBTixHQUFrQiwwQkFBbEIsR0FBNkM3SyxFQUE3QyxHQUFnRCxLQUFoRCxHQUFzRHdDLEdBQXRELEdBQTBELGtCQUExRCxHQUE2RXdGLE1BQXBGO0FBQ0FyRSxVQUFPcUgsYUFBUCxDQUFxQkMsV0FBckIsQ0FBa0N2TSxRQUFROEQsR0FBMUMsRUFBK0N3RixNQUEvQztBQUNBOztBQUVELFdBQVNrRCxjQUFULEdBQXlCO0FBQ3hCakssUUFBS2pCLEVBQUwsRUFBUSxNQUFNNkssU0FBTixHQUFrQixXQUFsQixHQUE4QjdLLEVBQTlCLEdBQWlDLGFBQXpDO0FBQ0E7O0FBRUQsV0FBU21MLFVBQVQsR0FBcUI7QUFDcEIsT0FBR3hILFVBQVUsbUJBQW1CQSxNQUE3QixJQUF3QyxTQUFTQSxPQUFPcUgsYUFBM0QsRUFBMEU7QUFBRTtBQUMzRUY7QUFDQSxJQUZELE1BRU87QUFDTkk7QUFDQTtBQUNEOztBQUVEbEwsT0FBS0EsTUFBTTJELE9BQU8zRCxFQUFsQjs7QUFFQSxNQUFHYixTQUFTYSxFQUFULENBQUgsRUFBaUI7QUFDaEJtTDtBQUNBO0FBRUQ7O0FBRUQsVUFBU25CLGlCQUFULENBQTJCaEksUUFBM0IsRUFBb0M7QUFDbkMsU0FBT0EsV0FDTixHQURNLEdBQ0E3QyxTQUFTNkMsUUFBVCxFQUFtQnRDLFlBRG5CLEdBRU4sR0FGTSxHQUVBUCxTQUFTNkMsUUFBVCxFQUFtQnJCLFNBRm5CLEdBR04sR0FITSxHQUdBeEIsU0FBUzZDLFFBQVQsRUFBbUI5QixHQUhuQixHQUlOLEdBSk0sR0FJQWYsU0FBUzZDLFFBQVQsRUFBbUIvQixRQUpuQixHQUtOLEdBTE0sR0FLQWQsU0FBUzZDLFFBQVQsRUFBbUJsQyxtQkFMbkIsR0FNTixHQU5NLEdBTUFYLFNBQVM2QyxRQUFULEVBQW1CekMsVUFObkIsR0FPTixHQVBNLEdBT0FKLFNBQVM2QyxRQUFULEVBQW1CdkMsVUFQbkIsR0FRTixHQVJNLEdBUUFOLFNBQVM2QyxRQUFULEVBQW1CakMsdUJBUm5CLEdBU04sR0FUTSxHQVNBWixTQUFTNkMsUUFBVCxFQUFtQnhDLGNBVG5CLEdBVU4sR0FWTSxHQVVBTCxTQUFTNkMsUUFBVCxFQUFtQnJDLFdBVm5CLEdBV04sR0FYTSxHQVdBUixTQUFTNkMsUUFBVCxFQUFtQnBCLFNBWG5CLEdBWU4sR0FaTSxHQVlBekIsU0FBUzZDLFFBQVQsRUFBbUJuQyxXQVpuQixHQWFOLEdBYk0sR0FhQVYsU0FBUzZDLFFBQVQsRUFBbUJ4QixVQWJuQixHQWNOLEdBZE0sR0FjQXJCLFNBQVM2QyxRQUFULEVBQW1CbkIsc0JBZDFCO0FBZUE7O0FBRUQsVUFBU3VLLFdBQVQsQ0FBcUJ6SCxNQUFyQixFQUE0QjBILE9BQTVCLEVBQW9DO0FBQ25DLFdBQVNDLFNBQVQsR0FBb0I7QUFDbkIsWUFBU0MsUUFBVCxDQUFrQmYsS0FBbEIsRUFBd0I7QUFDdkIsUUFBS3BLLGFBQWFqQixTQUFTNkMsUUFBVCxFQUFtQndJLEtBQW5CLENBQWQsSUFBNkMsTUFBTXJMLFNBQVM2QyxRQUFULEVBQW1Cd0ksS0FBbkIsQ0FBdkQsRUFBa0Y7QUFDakY3RyxZQUFPNkcsS0FBUCxDQUFhQSxLQUFiLElBQXNCckwsU0FBUzZDLFFBQVQsRUFBbUJ3SSxLQUFuQixJQUE0QixJQUFsRDtBQUNBdEssU0FBSThCLFFBQUosRUFBYSxTQUFPd0ksS0FBUCxHQUFhLEtBQWIsR0FBbUJyTCxTQUFTNkMsUUFBVCxFQUFtQndJLEtBQW5CLENBQW5CLEdBQTZDLElBQTFEO0FBQ0E7QUFDRDs7QUFFRCxZQUFTZ0IsU0FBVCxDQUFtQnZILFNBQW5CLEVBQTZCO0FBQzVCLFFBQUk5RSxTQUFTNkMsUUFBVCxFQUFtQixRQUFNaUMsU0FBekIsSUFBb0M5RSxTQUFTNkMsUUFBVCxFQUFtQixRQUFNaUMsU0FBekIsQ0FBeEMsRUFBNEU7QUFDM0UsV0FBTSxJQUFJYSxLQUFKLENBQVUsa0JBQWdCYixTQUFoQixHQUEwQiw4QkFBMUIsR0FBeURBLFNBQW5FLENBQU47QUFDQTtBQUNEOztBQUVEdUgsYUFBVSxRQUFWO0FBQ0FBLGFBQVUsT0FBVjs7QUFFQUQsWUFBUyxXQUFUO0FBQ0FBLFlBQVMsV0FBVDtBQUNBQSxZQUFTLFVBQVQ7QUFDQUEsWUFBUyxVQUFUO0FBQ0E7O0FBRUQsV0FBU0UsS0FBVCxHQUFnQjtBQUNmLE9BQUl6TCxLQUFPcUwsV0FBV0EsUUFBUXJMLEVBQXBCLElBQTJCVixTQUFTVSxFQUFULEdBQWM1QixPQUFuRDtBQUNBLE9BQUssU0FBU3dILFNBQVN5RCxjQUFULENBQXdCckosRUFBeEIsQ0FBZCxFQUEwQztBQUN6Q0EsU0FBS0EsS0FBSzVCLE9BQVY7QUFDQTtBQUNELFVBQU80QixFQUFQO0FBQ0E7O0FBRUQsV0FBUzBMLFdBQVQsQ0FBcUIxSixRQUFyQixFQUE4QjtBQUM3QjNDLFdBQU0yQyxRQUFOO0FBQ0EsT0FBSSxPQUFLQSxRQUFULEVBQWtCO0FBQ2pCMkIsV0FBTzNELEVBQVAsR0FBWWdDLFdBQVl5SixPQUF4QjtBQUNBcE4saUJBQWEsQ0FBQ2dOLFdBQVcsRUFBWixFQUFnQm5MLEdBQTdCO0FBQ0FiLFlBQU0yQyxRQUFOO0FBQ0E5QixRQUFJOEIsUUFBSixFQUFhLDhCQUE2QkEsUUFBN0IsR0FBdUMsSUFBdkMsR0FBOEMyQixPQUFPZ0ksR0FBckQsR0FBMkQsR0FBeEU7QUFDQTs7QUFHRCxVQUFPM0osUUFBUDtBQUNBOztBQUVELFdBQVM0SixZQUFULEdBQXVCO0FBQ3RCMUwsT0FBSThCLFFBQUosRUFBYSx1QkFBdUI3QyxTQUFTNkMsUUFBVCxFQUFtQnZCLFNBQW5CLEdBQStCLFNBQS9CLEdBQTJDLFVBQWxFLElBQWdGLE9BQWhGLEdBQTBGdUIsUUFBdkc7QUFDQTJCLFVBQU82RyxLQUFQLENBQWFxQixRQUFiLEdBQXdCLFVBQVUxTSxTQUFTNkMsUUFBVCxFQUFtQnZCLFNBQTdCLEdBQXlDLFFBQXpDLEdBQW9ELE1BQTVFO0FBQ0FrRCxVQUFPbEQsU0FBUCxHQUF3QixVQUFVdEIsU0FBUzZDLFFBQVQsRUFBbUJ2QixTQUE3QixHQUF5QyxJQUF6QyxHQUFnRCxLQUF4RTtBQUNBOztBQUVEO0FBQ0E7QUFDQTtBQUNBLFdBQVNxTCxxQkFBVCxHQUFnQztBQUMvQixPQUFLLGFBQVcsT0FBTzNNLFNBQVM2QyxRQUFULEVBQW1CdkMsVUFBdEMsSUFBdUQsUUFBTU4sU0FBUzZDLFFBQVQsRUFBbUJ2QyxVQUFwRixFQUFnRztBQUMvRk4sYUFBUzZDLFFBQVQsRUFBbUJ0QyxZQUFuQixHQUFrQ1AsU0FBUzZDLFFBQVQsRUFBbUJ2QyxVQUFyRDtBQUNBTixhQUFTNkMsUUFBVCxFQUFtQnZDLFVBQW5CLEdBQWtDLEtBQUtOLFNBQVM2QyxRQUFULEVBQW1CdkMsVUFBeEIsR0FBcUMsSUFBdkU7QUFDQTtBQUNEOztBQUVELFdBQVNzTSxVQUFULEdBQXFCO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBLE9BQ0NwQyxXQUFxQnhLLFNBQVM2QyxRQUFULEVBQW1CMkgsUUFEekM7QUFBQSxPQUVDcUMscUJBQXFCN00sU0FBUzZDLFFBQVQsRUFBbUJqQyx1QkFBbkIsSUFBOENqQixvQkFGcEU7O0FBSUEsT0FBSSxDQUFDNkssUUFBRCxJQUFhcUMsa0JBQWpCLEVBQW9DO0FBQ25DbkMsZ0JBQVksRUFBQ2xHLFFBQU9BLE1BQVIsRUFBZ0JDLFFBQU8sQ0FBdkIsRUFBMEJDLE9BQU0sQ0FBaEMsRUFBbUNsQixNQUFLLE1BQXhDLEVBQVo7QUFDQTtBQUNEOztBQUVELFdBQVNzSixpQkFBVCxHQUE0QjtBQUMzQixPQUFHQyxTQUFTQyxTQUFULENBQW1CQyxJQUF0QixFQUEyQjtBQUFFO0FBQzVCak4sYUFBUzZDLFFBQVQsRUFBbUIyQixNQUFuQixDQUEwQjBJLGFBQTFCLEdBQTBDOztBQUV6Q0MsWUFBZTFDLFlBQVl3QyxJQUFaLENBQWlCLElBQWpCLEVBQXNCak4sU0FBUzZDLFFBQVQsRUFBbUIyQixNQUF6QyxDQUYwQjs7QUFJekNWLGFBQWVrRSxRQUFRaUYsSUFBUixDQUFhLElBQWIsRUFBa0IsZUFBbEIsRUFBbUMsUUFBbkMsRUFBNkNqTixTQUFTNkMsUUFBVCxFQUFtQjJCLE1BQWhFLENBSjBCOztBQU16Q3VGLG1CQUFlLHNCQUFTcUQsTUFBVCxFQUFnQjtBQUM5QnBGLGNBQVEsZ0JBQVIsRUFBeUIsa0JBQWdCb0YsTUFBekMsRUFBaURwTixTQUFTNkMsUUFBVCxFQUFtQjJCLE1BQXBFLEVBQTJFM0IsUUFBM0U7QUFDQSxNQVJ3Qzs7QUFVekN3SyxrQkFBZSxxQkFBU2pILE9BQVQsRUFBaUI7QUFDL0JBLGdCQUFVQyxLQUFLUSxTQUFMLENBQWVULE9BQWYsQ0FBVjtBQUNBNEIsY0FBUSxjQUFSLEVBQXVCLGFBQVc1QixPQUFsQyxFQUEyQ3BHLFNBQVM2QyxRQUFULEVBQW1CMkIsTUFBOUQsRUFBcUUzQixRQUFyRTtBQUNBO0FBYndDLEtBQTFDO0FBZUE7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxXQUFTeUssSUFBVCxDQUFjakssR0FBZCxFQUFrQjtBQUNqQixZQUFTa0ssWUFBVCxHQUF1QjtBQUN0QnZGLFlBQVEsZUFBUixFQUF3QjNFLEdBQXhCLEVBQTRCbUIsTUFBNUI7QUFDQW9JO0FBQ0E7O0FBRUQzSyxvQkFBaUJ1QyxNQUFqQixFQUF3QixNQUF4QixFQUErQitJLFlBQS9CO0FBQ0F2RixXQUFRLE1BQVIsRUFBZTNFLEdBQWYsRUFBbUJtQixNQUFuQjtBQUNBOztBQUVELFdBQVNnSixZQUFULENBQXNCdEIsT0FBdEIsRUFBOEI7QUFDN0IsT0FBSSxxQkFBb0JBLE9BQXBCLHlDQUFvQkEsT0FBcEIsRUFBSixFQUFnQztBQUMvQixVQUFNLElBQUluQixTQUFKLENBQWMsMEJBQWQsQ0FBTjtBQUNBO0FBQ0Q7O0FBRUQsV0FBUzBDLFdBQVQsQ0FBcUJ2QixPQUFyQixFQUE2QjtBQUM1QixRQUFLLElBQUl3QixNQUFULElBQW1Cdk4sUUFBbkIsRUFBNkI7QUFDNUIsUUFBSUEsU0FBU3dOLGNBQVQsQ0FBd0JELE1BQXhCLENBQUosRUFBb0M7QUFDbkMxTixjQUFTNkMsUUFBVCxFQUFtQjZLLE1BQW5CLElBQTZCeEIsUUFBUXlCLGNBQVIsQ0FBdUJELE1BQXZCLElBQWlDeEIsUUFBUXdCLE1BQVIsQ0FBakMsR0FBbUR2TixTQUFTdU4sTUFBVCxDQUFoRjtBQUNBO0FBQ0Q7QUFDRDs7QUFFRCxXQUFTRSxlQUFULENBQTBCcEksVUFBMUIsRUFBcUM7QUFDcEMsVUFBUSxPQUFPQSxVQUFQLElBQXFCLGNBQWNBLFVBQXBDLEdBQWtELEdBQWxELEdBQXdEQSxVQUEvRDtBQUNBOztBQUVELFdBQVNxSSxjQUFULENBQXdCM0IsT0FBeEIsRUFBZ0M7QUFDL0JBLGFBQVVBLFdBQVcsRUFBckI7QUFDQWxNLFlBQVM2QyxRQUFULElBQXFCO0FBQ3BCMkgsY0FBVyxJQURTO0FBRXBCaEcsWUFBVUEsTUFGVTtBQUdwQmdCLGdCQUFhaEIsT0FBT2dJLEdBQVAsQ0FBV2pJLEtBQVgsQ0FBaUIsR0FBakIsRUFBc0J1SixLQUF0QixDQUE0QixDQUE1QixFQUE4QixDQUE5QixFQUFpQ0MsSUFBakMsQ0FBc0MsR0FBdEM7QUFITyxJQUFyQjs7QUFNQVAsZ0JBQWF0QixPQUFiO0FBQ0F1QixlQUFZdkIsT0FBWjs7QUFFQWxNLFlBQVM2QyxRQUFULEVBQW1CK0ksWUFBbkIsR0FBa0MsU0FBUzVMLFNBQVM2QyxRQUFULEVBQW1CcEMsV0FBNUIsR0FBMENtTixnQkFBZ0I1TixTQUFTNkMsUUFBVCxFQUFtQjJDLFVBQW5DLENBQTFDLEdBQTJGLEdBQTdIO0FBQ0E7O0FBRUQsV0FBU3dJLFFBQVQsR0FBbUI7QUFDbEIsVUFBUW5MLFlBQVk3QyxRQUFaLElBQXdCLG1CQUFtQndFLE1BQW5EO0FBQ0E7O0FBRUQsTUFBSTNCLFdBQVcwSixZQUFZL0gsT0FBTzNELEVBQW5CLENBQWY7O0FBRUEsTUFBSSxDQUFDbU4sVUFBTCxFQUFnQjtBQUNmSCxrQkFBZTNCLE9BQWY7QUFDQU87QUFDQU47QUFDQVE7QUFDQVcsUUFBS3pDLGtCQUFrQmhJLFFBQWxCLENBQUw7QUFDQWlLO0FBQ0EsR0FQRCxNQU9PO0FBQ05oTCxRQUFLZSxRQUFMLEVBQWMsZ0NBQWQ7QUFDQTtBQUNEOztBQUVELFVBQVNvRixPQUFULENBQWlCZ0csRUFBakIsRUFBb0JDLElBQXBCLEVBQXlCO0FBQ3hCLE1BQUksU0FBU2pPLEtBQWIsRUFBbUI7QUFDbEJBLFdBQVFrTyxXQUFXLFlBQVU7QUFDNUJsTyxZQUFRLElBQVI7QUFDQWdPO0FBQ0EsSUFITyxFQUdMQyxJQUhLLENBQVI7QUFJQTtBQUNEOztBQUVELDJCQXJ6QmtCLENBcXpCVTtBQUM1QixVQUFTM0MsZ0JBQVQsR0FBMkI7QUFDMUIsV0FBUzZDLFlBQVQsR0FBdUI7QUFDdEIsWUFBU0MsV0FBVCxDQUFxQkMsU0FBckIsRUFBK0I7QUFDOUIsYUFBU0MsWUFBVCxDQUFzQnpKLFNBQXRCLEVBQWdDO0FBQy9CLFlBQU8sVUFBVTlFLFNBQVNzTyxTQUFULEVBQW9COUosTUFBcEIsQ0FBMkI2RyxLQUEzQixDQUFpQ3ZHLFNBQWpDLENBQWpCO0FBQ0E7O0FBRUQsYUFBUzBKLFNBQVQsQ0FBbUJqTSxFQUFuQixFQUF1QjtBQUN0QixZQUFRLFNBQVNBLEdBQUdrTSxZQUFwQjtBQUNBOztBQUVELFFBQUlELFVBQVV4TyxTQUFTc08sU0FBVCxFQUFvQjlKLE1BQTlCLE1BQTBDK0osYUFBYSxRQUFiLEtBQTBCQSxhQUFhLE9BQWIsQ0FBcEUsQ0FBSixFQUErRjtBQUM5RnZHLGFBQVEsbUJBQVIsRUFBNkIsUUFBN0IsRUFBdUNoSSxTQUFTc08sU0FBVCxFQUFvQjlKLE1BQTNELEVBQWtFOEosU0FBbEU7QUFDQTtBQUNEOztBQUVELFFBQUssSUFBSUEsU0FBVCxJQUFzQnRPLFFBQXRCLEVBQStCO0FBQzlCcU8sZ0JBQVlDLFNBQVo7QUFDQTtBQUNEOztBQUVELFdBQVNJLGdCQUFULENBQTBCQyxTQUExQixFQUFvQztBQUNuQzVOLE9BQUksUUFBSixFQUFhLHdCQUF3QjROLFVBQVUsQ0FBVixFQUFhOUYsTUFBckMsR0FBOEMsR0FBOUMsR0FBb0Q4RixVQUFVLENBQVYsRUFBYW5MLElBQTlFO0FBQ0F5RSxXQUFRbUcsWUFBUixFQUFxQixFQUFyQjtBQUNBOztBQUVELFdBQVNRLHNCQUFULEdBQWlDO0FBQ2hDLE9BQ0MvRixTQUFTcEMsU0FBU29JLGFBQVQsQ0FBdUIsTUFBdkIsQ0FEVjtBQUFBLE9BR0NDLFNBQVM7QUFDUkMsZ0JBQXdCLElBRGhCO0FBRVJDLHVCQUF3QixLQUZoQjtBQUdSQyxtQkFBd0IsSUFIaEI7QUFJUkMsMkJBQXdCLEtBSmhCO0FBS1JDLGVBQXdCLElBTGhCO0FBTVJDLGFBQXdCO0FBTmhCLElBSFY7QUFBQSxPQVlDQyxXQUFXLElBQUlDLGdCQUFKLENBQXFCWixnQkFBckIsQ0FaWjs7QUFjQVcsWUFBU0UsT0FBVCxDQUFpQjFHLE1BQWpCLEVBQXlCaUcsTUFBekI7QUFDQTs7QUFFRCxNQUFJUSxtQkFBbUJ0USxPQUFPc1EsZ0JBQVAsSUFBMkJ0USxPQUFPd1Esc0JBQXpEOztBQUVBLE1BQUlGLGdCQUFKLEVBQXNCVjtBQUN0Qjs7QUFHRCxVQUFTYSxhQUFULENBQXVCN0wsS0FBdkIsRUFBNkI7QUFDNUIsV0FBU0UsTUFBVCxHQUFpQjtBQUNoQjRMLGtCQUFlLFlBQVU5TCxLQUF6QixFQUErQixRQUEvQjtBQUNBOztBQUVEN0MsTUFBSSxRQUFKLEVBQWEsb0JBQWtCNkMsS0FBL0I7QUFDQXFFLFVBQVFuRSxNQUFSLEVBQWUsRUFBZjtBQUNBOztBQUVELDJCQWozQmtCLENBaTNCVTtBQUM1QixVQUFTNkwsVUFBVCxHQUFzQjtBQUNyQixXQUFTN0wsTUFBVCxHQUFpQjtBQUNoQjRMLGtCQUFlLGFBQWYsRUFBNkIsUUFBN0I7QUFDQTs7QUFFRCxNQUFHLGFBQWFqSixTQUFTbUosZUFBekIsRUFBMEM7QUFDekM3TyxPQUFJLFVBQUosRUFBZSxpQ0FBZjtBQUNBa0gsV0FBUW5FLE1BQVIsRUFBZSxFQUFmO0FBQ0E7QUFDRDs7QUFFRCxVQUFTNEwsY0FBVCxDQUF3QkcsU0FBeEIsRUFBa0NqTSxLQUFsQyxFQUF3QztBQUN2QyxXQUFTa00scUJBQVQsQ0FBK0JqTixRQUEvQixFQUF5QztBQUN4QyxVQUFPLGFBQWE3QyxTQUFTNkMsUUFBVCxFQUFtQnhCLFVBQWhDLElBQ0xyQixTQUFTNkMsUUFBVCxFQUFtQnpDLFVBRGQsSUFFTCxDQUFDSixTQUFTNkMsUUFBVCxFQUFtQjJILFFBRnRCO0FBR0E7O0FBRUQsT0FBSyxJQUFJM0gsUUFBVCxJQUFxQjdDLFFBQXJCLEVBQThCO0FBQzdCLE9BQUc4UCxzQkFBc0JqTixRQUF0QixDQUFILEVBQW1DO0FBQ2xDbUYsWUFBUTZILFNBQVIsRUFBa0JqTSxLQUFsQixFQUF3QjZDLFNBQVN5RCxjQUFULENBQXdCckgsUUFBeEIsQ0FBeEIsRUFBMERBLFFBQTFEO0FBQ0E7QUFDRDtBQUNEOztBQUVELFVBQVNrTixtQkFBVCxHQUE4QjtBQUM3QjlOLG1CQUFpQmpELE1BQWpCLEVBQXdCLFNBQXhCLEVBQWtDMkUsY0FBbEM7O0FBRUExQixtQkFBaUJqRCxNQUFqQixFQUF3QixRQUF4QixFQUFrQyxZQUFVO0FBQUN5USxpQkFBYyxRQUFkO0FBQXlCLEdBQXRFOztBQUVBeE4sbUJBQWlCd0UsUUFBakIsRUFBMEIsa0JBQTFCLEVBQTZDa0osVUFBN0M7QUFDQTFOLG1CQUFpQndFLFFBQWpCLEVBQTBCLDBCQUExQixFQUFxRGtKLFVBQXJELEVBTjZCLENBTXFDO0FBQ2xFMU4sbUJBQWlCakQsTUFBakIsRUFBd0IsU0FBeEIsRUFBa0MsWUFBVTtBQUFDeVEsaUJBQWMsT0FBZDtBQUF3QixHQUFyRSxFQVA2QixDQU8yQztBQUN4RXhOLG1CQUFpQmpELE1BQWpCLEVBQXdCLE9BQXhCLEVBQWdDLFlBQVU7QUFBQ3lRLGlCQUFjLE9BQWQ7QUFBd0IsR0FBbkU7QUFDQTs7QUFHRCxVQUFTTyxPQUFULEdBQWtCO0FBQ2pCLFdBQVMxQyxJQUFULENBQWNwQixPQUFkLEVBQXNCK0QsT0FBdEIsRUFBOEI7QUFDN0IsWUFBU0MsT0FBVCxHQUFrQjtBQUNqQixRQUFHLENBQUNELFFBQVFFLE9BQVosRUFBcUI7QUFDcEIsV0FBTSxJQUFJcEYsU0FBSixDQUFjLG1DQUFkLENBQU47QUFDQSxLQUZELE1BRU8sSUFBSSxhQUFha0YsUUFBUUUsT0FBUixDQUFnQkMsV0FBaEIsRUFBakIsRUFBZ0Q7QUFDdEQsV0FBTSxJQUFJckYsU0FBSixDQUFjLG1DQUFpQ2tGLFFBQVFFLE9BQXpDLEdBQWlELEdBQS9ELENBQU47QUFDQTtBQUNEOztBQUVELE9BQUdGLE9BQUgsRUFBWTtBQUNYQztBQUNBakUsZ0JBQVlnRSxPQUFaLEVBQXFCL0QsT0FBckI7QUFDQW1FLFlBQVFDLElBQVIsQ0FBYUwsT0FBYjtBQUNBO0FBQ0Q7O0FBRUQsV0FBU00scUJBQVQsQ0FBK0JyRSxPQUEvQixFQUF3QztBQUN2QyxPQUFJQSxXQUFXQSxRQUFRdkwsbUJBQXZCLEVBQTRDO0FBQzNDbUIsU0FBSyxvR0FBTDtBQUNBO0FBQ0Q7O0FBRUQsTUFBSXVPLE9BQUo7O0FBRUE1TjtBQUNBc047O0FBRUEsU0FBTyxTQUFTUyxhQUFULENBQXVCdEUsT0FBdkIsRUFBK0JyRCxNQUEvQixFQUFzQztBQUM1Q3dILGFBQVUsRUFBVixDQUQ0QyxDQUM5Qjs7QUFFZEUseUJBQXNCckUsT0FBdEI7O0FBRUEsa0JBQWVyRCxNQUFmLHlDQUFlQSxNQUFmO0FBQ0EsU0FBSyxXQUFMO0FBQ0EsU0FBSyxRQUFMO0FBQ0NuRCxXQUFNc0gsU0FBTixDQUFnQjFFLE9BQWhCLENBQXdCbUksSUFBeEIsQ0FDQ2hLLFNBQVNpSyxnQkFBVCxDQUEyQjdILFVBQVUsUUFBckMsQ0FERCxFQUVDeUUsS0FBS0wsSUFBTCxDQUFVL0IsU0FBVixFQUFxQmdCLE9BQXJCLENBRkQ7QUFJQTtBQUNELFNBQUssUUFBTDtBQUNDb0IsVUFBS3BCLE9BQUwsRUFBYXJELE1BQWI7QUFDQTtBQUNEO0FBQ0MsV0FBTSxJQUFJa0MsU0FBSixDQUFjLG1DQUFnQ2xDLE1BQWhDLHlDQUFnQ0EsTUFBaEMsS0FBd0MsR0FBdEQsQ0FBTjtBQVpEOztBQWVBLFVBQU93SCxPQUFQO0FBQ0EsR0FyQkQ7QUFzQkE7O0FBRUQsVUFBU00sd0JBQVQsQ0FBa0NDLENBQWxDLEVBQW9DO0FBQ25DLE1BQUksQ0FBQ0EsRUFBRTNDLEVBQVAsRUFBVztBQUNWMUssUUFBSyxFQUFMLEVBQVEsbURBQVI7QUFDQSxHQUZELE1BRU8sSUFBSSxDQUFDcU4sRUFBRTNDLEVBQUYsQ0FBSzRDLFlBQVYsRUFBdUI7QUFDN0JELEtBQUUzQyxFQUFGLENBQUs0QyxZQUFMLEdBQW9CLFNBQVNDLGNBQVQsQ0FBd0I1RSxPQUF4QixFQUFpQztBQUNwRCxhQUFTb0IsSUFBVCxDQUFjeUQsS0FBZCxFQUFxQmQsT0FBckIsRUFBOEI7QUFDN0JoRSxpQkFBWWdFLE9BQVosRUFBcUIvRCxPQUFyQjtBQUNBOztBQUVELFdBQU8sS0FBSzhFLE1BQUwsQ0FBWSxRQUFaLEVBQXNCQyxJQUF0QixDQUEyQjNELElBQTNCLEVBQWlDNEQsR0FBakMsRUFBUDtBQUNBLElBTkQ7QUFPQTtBQUNEOztBQUVELEtBQUlsUyxPQUFPbVMsTUFBWCxFQUFtQjtBQUFFUiwyQkFBeUJRLE1BQXpCO0FBQW1DOztBQUV4RG5TLFFBQU82UixZQUFQLEdBQXNCN1IsT0FBTzZSLFlBQVAsSUFBdUJiLFNBQTdDO0FBRUEsQ0E3OUJBLEVBNjlCRWhSLFVBQVUsRUE3OUJaIiwiZmlsZSI6IkFkbWluL0phdmFzY3JpcHQvdmVuZG9yL2lmcmFtZV9yZXNpemVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIEZpbGU6IGlmcmFtZVJlc2l6ZXIuanNcbiAqIERlc2M6IEZvcmNlIGlmcmFtZXMgdG8gc2l6ZSB0byBjb250ZW50LlxuICogUmVxdWlyZXM6IGlmcmFtZVJlc2l6ZXIuY29udGVudFdpbmRvdy5qcyB0byBiZSBsb2FkZWQgaW50byB0aGUgdGFyZ2V0IGZyYW1lLlxuICogRG9jOiBodHRwczovL2dpdGh1Yi5jb20vZGF2aWRqYnJhZHNoYXcvaWZyYW1lLXJlc2l6ZXJcbiAqIEF1dGhvcjogRGF2aWQgSi4gQnJhZHNoYXcgLSBkYXZlQGJyYWRzaGF3Lm5ldFxuICogQ29udHJpYnV0b3I6IEp1cmUgTWF2IC0ganVyZS5tYXZAZ21haWwuY29tXG4gKiBDb250cmlidXRvcjogUmVlZCBEYWRvdW5lIC0gcmVlZEBkYWRvdW5lLmNvbVxuICovXG5cblxuOyhmdW5jdGlvbih3aW5kb3cpIHtcblx0J3VzZSBzdHJpY3QnO1xuXG5cdHZhclxuXHRcdGNvdW50ICAgICAgICAgICAgICAgICA9IDAsXG5cdFx0bG9nRW5hYmxlZCAgICAgICAgICAgID0gZmFsc2UsXG5cdFx0aGlkZGVuQ2hlY2tFbmFibGVkICAgID0gZmFsc2UsXG5cdFx0bXNnSGVhZGVyICAgICAgICAgICAgID0gJ21lc3NhZ2UnLFxuXHRcdG1zZ0hlYWRlckxlbiAgICAgICAgICA9IG1zZ0hlYWRlci5sZW5ndGgsXG5cdFx0bXNnSWQgICAgICAgICAgICAgICAgID0gJ1tpRnJhbWVTaXplcl0nLCAvL011c3QgbWF0Y2ggaWZyYW1lIG1zZyBJRFxuXHRcdG1zZ0lkTGVuICAgICAgICAgICAgICA9IG1zZ0lkLmxlbmd0aCxcblx0XHRwYWdlUG9zaXRpb24gICAgICAgICAgPSBudWxsLFxuXHRcdHJlcXVlc3RBbmltYXRpb25GcmFtZSA9IHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUsXG5cdFx0cmVzZXRSZXF1aXJlZE1ldGhvZHMgID0ge21heDoxLHNjcm9sbDoxLGJvZHlTY3JvbGw6MSxkb2N1bWVudEVsZW1lbnRTY3JvbGw6MX0sXG5cdFx0c2V0dGluZ3MgICAgICAgICAgICAgID0ge30sXG5cdFx0dGltZXIgICAgICAgICAgICAgICAgID0gbnVsbCxcblx0XHRsb2dJZCAgICAgICAgICAgICAgICAgPSAnSG9zdCBQYWdlJyxcblxuXHRcdGRlZmF1bHRzICAgICAgICAgICAgICA9IHtcblx0XHRcdGF1dG9SZXNpemUgICAgICAgICAgICAgICAgOiB0cnVlLFxuXHRcdFx0Ym9keUJhY2tncm91bmQgICAgICAgICAgICA6IG51bGwsXG5cdFx0XHRib2R5TWFyZ2luICAgICAgICAgICAgICAgIDogbnVsbCxcblx0XHRcdGJvZHlNYXJnaW5WMSAgICAgICAgICAgICAgOiA4LFxuXHRcdFx0Ym9keVBhZGRpbmcgICAgICAgICAgICAgICA6IG51bGwsXG5cdFx0XHRjaGVja09yaWdpbiAgICAgICAgICAgICAgIDogdHJ1ZSxcblx0XHRcdGluUGFnZUxpbmtzICAgICAgICAgICAgICAgOiBmYWxzZSxcblx0XHRcdGVuYWJsZVB1YmxpY01ldGhvZHMgICAgICAgOiB0cnVlLFxuXHRcdFx0aGVpZ2h0Q2FsY3VsYXRpb25NZXRob2QgICA6ICdib2R5T2Zmc2V0Jyxcblx0XHRcdGlkICAgICAgICAgICAgICAgICAgICAgICAgOiAnaUZyYW1lUmVzaXplcicsXG5cdFx0XHRpbnRlcnZhbCAgICAgICAgICAgICAgICAgIDogMzIsXG5cdFx0XHRsb2cgICAgICAgICAgICAgICAgICAgICAgIDogZmFsc2UsXG5cdFx0XHRtYXhIZWlnaHQgICAgICAgICAgICAgICAgIDogSW5maW5pdHksXG5cdFx0XHRtYXhXaWR0aCAgICAgICAgICAgICAgICAgIDogSW5maW5pdHksXG5cdFx0XHRtaW5IZWlnaHQgICAgICAgICAgICAgICAgIDogMCxcblx0XHRcdG1pbldpZHRoICAgICAgICAgICAgICAgICAgOiAwLFxuXHRcdFx0cmVzaXplRnJvbSAgICAgICAgICAgICAgICA6ICdwYXJlbnQnLFxuXHRcdFx0c2Nyb2xsaW5nICAgICAgICAgICAgICAgICA6IGZhbHNlLFxuXHRcdFx0c2l6ZUhlaWdodCAgICAgICAgICAgICAgICA6IHRydWUsXG5cdFx0XHRzaXplV2lkdGggICAgICAgICAgICAgICAgIDogZmFsc2UsXG5cdFx0XHR0b2xlcmFuY2UgICAgICAgICAgICAgICAgIDogMCxcblx0XHRcdHdpZHRoQ2FsY3VsYXRpb25NZXRob2QgICAgOiAnc2Nyb2xsJyxcblx0XHRcdGNsb3NlZENhbGxiYWNrICAgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG5cdFx0XHRpbml0Q2FsbGJhY2sgICAgICAgICAgICAgIDogZnVuY3Rpb24oKXt9LFxuXHRcdFx0bWVzc2FnZUNhbGxiYWNrICAgICAgICAgICA6IGZ1bmN0aW9uKCl7d2FybignTWVzc2FnZUNhbGxiYWNrIGZ1bmN0aW9uIG5vdCBkZWZpbmVkJyk7fSxcblx0XHRcdHJlc2l6ZWRDYWxsYmFjayAgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG5cdFx0XHRzY3JvbGxDYWxsYmFjayAgICAgICAgICAgIDogZnVuY3Rpb24oKXtyZXR1cm4gdHJ1ZTt9XG5cdFx0fTtcblxuXHRmdW5jdGlvbiBhZGRFdmVudExpc3RlbmVyKG9iaixldnQsZnVuYyl7XG5cdFx0LyogaXN0YW5idWwgaWdub3JlIGVsc2UgKi8gLy8gTm90IHRlc3RhYmxlIGluIFBoYW50b25KU1xuXHRcdGlmICgnYWRkRXZlbnRMaXN0ZW5lcicgaW4gd2luZG93KXtcblx0XHRcdG9iai5hZGRFdmVudExpc3RlbmVyKGV2dCxmdW5jLCBmYWxzZSk7XG5cdFx0fSBlbHNlIGlmICgnYXR0YWNoRXZlbnQnIGluIHdpbmRvdyl7Ly9JRVxuXHRcdFx0b2JqLmF0dGFjaEV2ZW50KCdvbicrZXZ0LGZ1bmMpO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHJlbW92ZUV2ZW50TGlzdGVuZXIoZWwsZXZ0LGZ1bmMpe1xuXHRcdC8qIGlzdGFuYnVsIGlnbm9yZSBlbHNlICovIC8vIE5vdCB0ZXN0YWJsZSBpbiBwaGFudG9uSlNcblx0XHRpZiAoJ3JlbW92ZUV2ZW50TGlzdGVuZXInIGluIHdpbmRvdyl7XG5cdFx0XHRlbC5yZW1vdmVFdmVudExpc3RlbmVyKGV2dCxmdW5jLCBmYWxzZSk7XG5cdFx0fSBlbHNlIGlmICgnZGV0YWNoRXZlbnQnIGluIHdpbmRvdyl7IC8vSUVcblx0XHRcdGVsLmRldGFjaEV2ZW50KCdvbicrZXZ0LGZ1bmMpO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHNldHVwUmVxdWVzdEFuaW1hdGlvbkZyYW1lKCl7XG5cdFx0dmFyXG5cdFx0XHR2ZW5kb3JzID0gWydtb3onLCAnd2Via2l0JywgJ28nLCAnbXMnXSxcblx0XHRcdHg7XG5cblx0XHQvLyBSZW1vdmUgdmVuZG9yIHByZWZpeGluZyBpZiBwcmVmaXhlZCBhbmQgYnJlYWsgZWFybHkgaWYgbm90XG5cdFx0Zm9yICh4ID0gMDsgeCA8IHZlbmRvcnMubGVuZ3RoICYmICFyZXF1ZXN0QW5pbWF0aW9uRnJhbWU7IHggKz0gMSkge1xuXHRcdFx0cmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93W3ZlbmRvcnNbeF0gKyAnUmVxdWVzdEFuaW1hdGlvbkZyYW1lJ107XG5cdFx0fVxuXG5cdFx0aWYgKCEocmVxdWVzdEFuaW1hdGlvbkZyYW1lKSl7XG5cdFx0XHRsb2coJ3NldHVwJywnUmVxdWVzdEFuaW1hdGlvbkZyYW1lIG5vdCBzdXBwb3J0ZWQnKTtcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBnZXRNeUlEKGlmcmFtZUlkKXtcblx0XHR2YXIgcmV0U3RyID0gJ0hvc3QgcGFnZTogJytpZnJhbWVJZDtcblxuXHRcdGlmICh3aW5kb3cudG9wICE9PSB3aW5kb3cuc2VsZil7XG5cdFx0XHRpZiAod2luZG93LnBhcmVudElGcmFtZSAmJiB3aW5kb3cucGFyZW50SUZyYW1lLmdldElkKXtcblx0XHRcdFx0cmV0U3RyID0gd2luZG93LnBhcmVudElGcmFtZS5nZXRJZCgpKyc6ICcraWZyYW1lSWQ7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRyZXRTdHIgPSAnTmVzdGVkIGhvc3QgcGFnZTogJytpZnJhbWVJZDtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRyZXR1cm4gcmV0U3RyO1xuXHR9XG5cblx0ZnVuY3Rpb24gZm9ybWF0TG9nSGVhZGVyKGlmcmFtZUlkKXtcblx0XHRyZXR1cm4gbXNnSWQgKyAnWycgKyBnZXRNeUlEKGlmcmFtZUlkKSArICddJztcblx0fVxuXG5cdGZ1bmN0aW9uIGlzTG9nRW5hYmxlZChpZnJhbWVJZCl7XG5cdFx0cmV0dXJuIHNldHRpbmdzW2lmcmFtZUlkXSA/IHNldHRpbmdzW2lmcmFtZUlkXS5sb2cgOiBsb2dFbmFibGVkO1xuXHR9XG5cblx0ZnVuY3Rpb24gbG9nKGlmcmFtZUlkLG1zZyl7XG5cdFx0b3V0cHV0KCdsb2cnLGlmcmFtZUlkLG1zZyxpc0xvZ0VuYWJsZWQoaWZyYW1lSWQpKTtcblx0fVxuXG5cdGZ1bmN0aW9uIGluZm8oaWZyYW1lSWQsbXNnKXtcblx0XHRvdXRwdXQoJ2luZm8nLGlmcmFtZUlkLG1zZyxpc0xvZ0VuYWJsZWQoaWZyYW1lSWQpKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHdhcm4oaWZyYW1lSWQsbXNnKXtcblx0XHRvdXRwdXQoJ3dhcm4nLGlmcmFtZUlkLG1zZyx0cnVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIG91dHB1dCh0eXBlLGlmcmFtZUlkLG1zZyxlbmFibGVkKXtcblx0XHRpZiAodHJ1ZSA9PT0gZW5hYmxlZCAmJiAnb2JqZWN0JyA9PT0gdHlwZW9mIHdpbmRvdy5jb25zb2xlKXtcblx0XHRcdGNvbnNvbGVbdHlwZV0oZm9ybWF0TG9nSGVhZGVyKGlmcmFtZUlkKSxtc2cpO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIGlGcmFtZUxpc3RlbmVyKGV2ZW50KXtcblx0XHRmdW5jdGlvbiByZXNpemVJRnJhbWUoKXtcblx0XHRcdGZ1bmN0aW9uIHJlc2l6ZSgpe1xuXHRcdFx0XHRzZXRTaXplKG1lc3NhZ2VEYXRhKTtcblx0XHRcdFx0c2V0UGFnZVBvc2l0aW9uKGlmcmFtZUlkKTtcblx0XHRcdH1cblxuXHRcdFx0ZW5zdXJlSW5SYW5nZSgnSGVpZ2h0Jyk7XG5cdFx0XHRlbnN1cmVJblJhbmdlKCdXaWR0aCcpO1xuXG5cdFx0XHRzeW5jUmVzaXplKHJlc2l6ZSxtZXNzYWdlRGF0YSwnaW5pdCcpO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIHByb2Nlc3NNc2coKXtcblx0XHRcdHZhciBkYXRhID0gbXNnLnN1YnN0cihtc2dJZExlbikuc3BsaXQoJzonKTtcblxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0aWZyYW1lOiBzZXR0aW5nc1tkYXRhWzBdXS5pZnJhbWUsXG5cdFx0XHRcdGlkOiAgICAgZGF0YVswXSxcblx0XHRcdFx0aGVpZ2h0OiBkYXRhWzFdLFxuXHRcdFx0XHR3aWR0aDogIGRhdGFbMl0sXG5cdFx0XHRcdHR5cGU6ICAgZGF0YVszXVxuXHRcdFx0fTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBlbnN1cmVJblJhbmdlKERpbWVuc2lvbil7XG5cdFx0XHR2YXJcblx0XHRcdFx0bWF4ICA9IE51bWJlcihzZXR0aW5nc1tpZnJhbWVJZF1bJ21heCcgKyBEaW1lbnNpb25dKSxcblx0XHRcdFx0bWluICA9IE51bWJlcihzZXR0aW5nc1tpZnJhbWVJZF1bJ21pbicgKyBEaW1lbnNpb25dKSxcblx0XHRcdFx0ZGltZW5zaW9uID0gRGltZW5zaW9uLnRvTG93ZXJDYXNlKCksXG5cdFx0XHRcdHNpemUgPSBOdW1iZXIobWVzc2FnZURhdGFbZGltZW5zaW9uXSk7XG5cblx0XHRcdGxvZyhpZnJhbWVJZCwnQ2hlY2tpbmcgJyArIGRpbWVuc2lvbiArICcgaXMgaW4gcmFuZ2UgJyArIG1pbiArICctJyArIG1heCk7XG5cblx0XHRcdGlmIChzaXplPG1pbikge1xuXHRcdFx0XHRzaXplPW1pbjtcblx0XHRcdFx0bG9nKGlmcmFtZUlkLCdTZXQgJyArIGRpbWVuc2lvbiArICcgdG8gbWluIHZhbHVlJyk7XG5cdFx0XHR9XG5cblx0XHRcdGlmIChzaXplPm1heCkge1xuXHRcdFx0XHRzaXplPW1heDtcblx0XHRcdFx0bG9nKGlmcmFtZUlkLCdTZXQgJyArIGRpbWVuc2lvbiArICcgdG8gbWF4IHZhbHVlJyk7XG5cdFx0XHR9XG5cblx0XHRcdG1lc3NhZ2VEYXRhW2RpbWVuc2lvbl0gPSAnJyArIHNpemU7XG5cdFx0fVxuXG5cblx0XHRmdW5jdGlvbiBpc01lc3NhZ2VGcm9tSUZyYW1lKCl7XG5cdFx0XHRmdW5jdGlvbiBjaGVja0FsbG93ZWRPcmlnaW4oKXtcblx0XHRcdFx0ZnVuY3Rpb24gY2hlY2tMaXN0KCl7XG5cdFx0XHRcdFx0dmFyXG5cdFx0XHRcdFx0XHRpID0gMCxcblx0XHRcdFx0XHRcdHJldENvZGUgPSBmYWxzZTtcblxuXHRcdFx0XHRcdGxvZyhpZnJhbWVJZCwnQ2hlY2tpbmcgY29ubmVjdGlvbiBpcyBmcm9tIGFsbG93ZWQgbGlzdCBvZiBvcmlnaW5zOiAnICsgY2hlY2tPcmlnaW4pO1xuXG5cdFx0XHRcdFx0Zm9yICg7IGkgPCBjaGVja09yaWdpbi5sZW5ndGg7IGkrKykge1xuXHRcdFx0XHRcdFx0aWYgKGNoZWNrT3JpZ2luW2ldID09PSBvcmlnaW4pIHtcblx0XHRcdFx0XHRcdFx0cmV0Q29kZSA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRyZXR1cm4gcmV0Q29kZTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGZ1bmN0aW9uIGNoZWNrU2luZ2xlKCl7XG5cdFx0XHRcdFx0dmFyIHJlbW90ZUhvc3QgID0gc2V0dGluZ3NbaWZyYW1lSWRdLnJlbW90ZUhvc3Q7XG5cdFx0XHRcdFx0bG9nKGlmcmFtZUlkLCdDaGVja2luZyBjb25uZWN0aW9uIGlzIGZyb206ICcrcmVtb3RlSG9zdCk7XG5cdFx0XHRcdFx0cmV0dXJuIG9yaWdpbiA9PT0gcmVtb3RlSG9zdDtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdHJldHVybiBjaGVja09yaWdpbi5jb25zdHJ1Y3RvciA9PT0gQXJyYXkgPyBjaGVja0xpc3QoKSA6IGNoZWNrU2luZ2xlKCk7XG5cdFx0XHR9XG5cblx0XHRcdHZhclxuXHRcdFx0XHRvcmlnaW4gICAgICA9IGV2ZW50Lm9yaWdpbixcblx0XHRcdFx0Y2hlY2tPcmlnaW4gPSBzZXR0aW5nc1tpZnJhbWVJZF0uY2hlY2tPcmlnaW47XG5cblx0XHRcdGlmIChjaGVja09yaWdpbiAmJiAoJycrb3JpZ2luICE9PSAnbnVsbCcpICYmICFjaGVja0FsbG93ZWRPcmlnaW4oKSkge1xuXHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoXG5cdFx0XHRcdFx0J1VuZXhwZWN0ZWQgbWVzc2FnZSByZWNlaXZlZCBmcm9tOiAnICsgb3JpZ2luICtcblx0XHRcdFx0XHQnIGZvciAnICsgbWVzc2FnZURhdGEuaWZyYW1lLmlkICtcblx0XHRcdFx0XHQnLiBNZXNzYWdlIHdhczogJyArIGV2ZW50LmRhdGEgK1xuXHRcdFx0XHRcdCcuIFRoaXMgZXJyb3IgY2FuIGJlIGRpc2FibGVkIGJ5IHNldHRpbmcgdGhlIGNoZWNrT3JpZ2luOiBmYWxzZSBvcHRpb24gb3IgYnkgcHJvdmlkaW5nIG9mIGFycmF5IG9mIHRydXN0ZWQgZG9tYWlucy4nXG5cdFx0XHRcdCk7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGlzTWVzc2FnZUZvclVzKCl7XG5cdFx0XHRyZXR1cm4gbXNnSWQgPT09ICgoJycgKyBtc2cpLnN1YnN0cigwLG1zZ0lkTGVuKSkgJiYgKG1zZy5zdWJzdHIobXNnSWRMZW4pLnNwbGl0KCc6JylbMF0gaW4gc2V0dGluZ3MpOyAvLycnK1Byb3RlY3RzIGFnYWluc3Qgbm9uLXN0cmluZyBtc2dcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBpc01lc3NhZ2VGcm9tTWV0YVBhcmVudCgpe1xuXHRcdFx0Ly9UZXN0IGlmIHRoaXMgbWVzc2FnZSBpcyBmcm9tIGEgcGFyZW50IGFib3ZlIHVzLiBUaGlzIGlzIGFuIHVnbHkgdGVzdCwgaG93ZXZlciwgdXBkYXRpbmdcblx0XHRcdC8vdGhlIG1lc3NhZ2UgZm9ybWF0IHdvdWxkIGJyZWFrIGJhY2t3YXJkcyBjb21wYXRpYml0eS5cblx0XHRcdHZhciByZXRDb2RlID0gbWVzc2FnZURhdGEudHlwZSBpbiB7J3RydWUnOjEsJ2ZhbHNlJzoxLCd1bmRlZmluZWQnOjF9O1xuXG5cdFx0XHRpZiAocmV0Q29kZSl7XG5cdFx0XHRcdGxvZyhpZnJhbWVJZCwnSWdub3JpbmcgaW5pdCBtZXNzYWdlIGZyb20gbWV0YSBwYXJlbnQgcGFnZScpO1xuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gcmV0Q29kZTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRNc2dCb2R5KG9mZnNldCl7XG5cdFx0XHRyZXR1cm4gbXNnLnN1YnN0cihtc2cuaW5kZXhPZignOicpK21zZ0hlYWRlckxlbitvZmZzZXQpO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGZvcndhcmRNc2dGcm9tSUZyYW1lKG1zZ0JvZHkpe1xuXHRcdFx0bG9nKGlmcmFtZUlkLCdNZXNzYWdlQ2FsbGJhY2sgcGFzc2VkOiB7aWZyYW1lOiAnKyBtZXNzYWdlRGF0YS5pZnJhbWUuaWQgKyAnLCBtZXNzYWdlOiAnICsgbXNnQm9keSArICd9Jyk7XG5cdFx0XHRjYWxsYmFjaygnbWVzc2FnZUNhbGxiYWNrJyx7XG5cdFx0XHRcdGlmcmFtZTogbWVzc2FnZURhdGEuaWZyYW1lLFxuXHRcdFx0XHRtZXNzYWdlOiBKU09OLnBhcnNlKG1zZ0JvZHkpXG5cdFx0XHR9KTtcblx0XHRcdGxvZyhpZnJhbWVJZCwnLS0nKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRQYWdlSW5mbygpe1xuXHRcdFx0dmFyXG5cdFx0XHRcdGJvZHlQb3NpdGlvbiAgID0gZG9jdW1lbnQuYm9keS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxcblx0XHRcdFx0aUZyYW1lUG9zaXRpb24gPSBtZXNzYWdlRGF0YS5pZnJhbWUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cblx0XHRcdHJldHVybiBKU09OLnN0cmluZ2lmeSh7XG5cdFx0XHRcdGlmcmFtZUhlaWdodDogaUZyYW1lUG9zaXRpb24uaGVpZ2h0LFxuXHRcdFx0XHRpZnJhbWVXaWR0aDogIGlGcmFtZVBvc2l0aW9uLndpZHRoLFxuXHRcdFx0XHRjbGllbnRIZWlnaHQ6IE1hdGgubWF4KGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQsIHdpbmRvdy5pbm5lckhlaWdodCB8fCAwKSxcblx0XHRcdFx0Y2xpZW50V2lkdGg6ICBNYXRoLm1heChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50V2lkdGgsICB3aW5kb3cuaW5uZXJXaWR0aCAgfHwgMCksXG5cdFx0XHRcdG9mZnNldFRvcDogICAgcGFyc2VJbnQoaUZyYW1lUG9zaXRpb24udG9wICAtIGJvZHlQb3NpdGlvbi50b3AsICAxMCksXG5cdFx0XHRcdG9mZnNldExlZnQ6ICAgcGFyc2VJbnQoaUZyYW1lUG9zaXRpb24ubGVmdCAtIGJvZHlQb3NpdGlvbi5sZWZ0LCAxMCksXG5cdFx0XHRcdHNjcm9sbFRvcDogICAgd2luZG93LnBhZ2VZT2Zmc2V0LFxuXHRcdFx0XHRzY3JvbGxMZWZ0OiAgIHdpbmRvdy5wYWdlWE9mZnNldFxuXHRcdFx0fSk7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gc2VuZFBhZ2VJbmZvVG9JZnJhbWUoaWZyYW1lLGlmcmFtZUlkKXtcblx0XHRcdGZ1bmN0aW9uIGRlYm91bmNlZFRyaWdnZXIoKXtcblx0XHRcdFx0dHJpZ2dlcihcblx0XHRcdFx0XHQnU2VuZCBQYWdlIEluZm8nLFxuXHRcdFx0XHRcdCdwYWdlSW5mbzonICsgZ2V0UGFnZUluZm8oKSxcblx0XHRcdFx0XHRpZnJhbWUsXG5cdFx0XHRcdFx0aWZyYW1lSWRcblx0XHRcdFx0KTtcblx0XHRcdH1cblxuXHRcdFx0ZGVib3VjZShkZWJvdW5jZWRUcmlnZ2VyLDMyKTtcblx0XHR9XG5cblxuXHRcdGZ1bmN0aW9uIHN0YXJ0UGFnZUluZm9Nb25pdG9yKCl7XG5cdFx0XHRmdW5jdGlvbiBzZXRMaXN0ZW5lcih0eXBlLGZ1bmMpe1xuXHRcdFx0XHRmdW5jdGlvbiBzZW5kUGFnZUluZm8oKXtcblx0XHRcdFx0XHRpZiAoc2V0dGluZ3NbaWRdKXtcblx0XHRcdFx0XHRcdHNlbmRQYWdlSW5mb1RvSWZyYW1lKHNldHRpbmdzW2lkXS5pZnJhbWUsaWQpO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRzdG9wKCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0WydzY3JvbGwnLCdyZXNpemUnXS5mb3JFYWNoKGZ1bmN0aW9uKGV2dCl7XG5cdFx0XHRcdFx0bG9nKGlkLCB0eXBlICsgIGV2dCArICcgbGlzdGVuZXIgZm9yIHNlbmRQYWdlSW5mbycpO1xuXHRcdFx0XHRcdGZ1bmMod2luZG93LGV2dCxzZW5kUGFnZUluZm8pO1xuXHRcdFx0XHR9KTtcblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gc3RvcCgpe1xuXHRcdFx0XHRzZXRMaXN0ZW5lcignUmVtb3ZlICcsIHJlbW92ZUV2ZW50TGlzdGVuZXIpO1xuXHRcdFx0fVxuXG5cdFx0XHRmdW5jdGlvbiBzdGFydCgpe1xuXHRcdFx0XHRzZXRMaXN0ZW5lcignQWRkICcsIGFkZEV2ZW50TGlzdGVuZXIpO1xuXHRcdFx0fVxuXG5cdFx0XHR2YXIgaWQgPSBpZnJhbWVJZDsgLy9DcmVhdGUgbG9jYWxseSBzY29wZWQgY29weSBvZiBpRnJhbWUgSURcblxuXHRcdFx0c3RhcnQoKTtcblxuXHRcdFx0c2V0dGluZ3NbaWRdLnN0b3BQYWdlSW5mbyA9IHN0b3A7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gc3RvcFBhZ2VJbmZvTW9uaXRvcigpe1xuXHRcdFx0aWYgKHNldHRpbmdzW2lmcmFtZUlkXSAmJiBzZXR0aW5nc1tpZnJhbWVJZF0uc3RvcFBhZ2VJbmZvKXtcblx0XHRcdFx0c2V0dGluZ3NbaWZyYW1lSWRdLnN0b3BQYWdlSW5mbygpO1xuXHRcdFx0XHRkZWxldGUgc2V0dGluZ3NbaWZyYW1lSWRdLnN0b3BQYWdlSW5mbztcblx0XHRcdH1cblx0XHR9XG5cblx0XHRmdW5jdGlvbiBjaGVja0lGcmFtZUV4aXN0cygpe1xuXHRcdFx0dmFyIHJldEJvb2wgPSB0cnVlO1xuXG5cdFx0XHRpZiAobnVsbCA9PT0gbWVzc2FnZURhdGEuaWZyYW1lKSB7XG5cdFx0XHRcdHdhcm4oaWZyYW1lSWQsJ0lGcmFtZSAoJyttZXNzYWdlRGF0YS5pZCsnKSBub3QgZm91bmQnKTtcblx0XHRcdFx0cmV0Qm9vbCA9IGZhbHNlO1xuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIHJldEJvb2w7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gZ2V0RWxlbWVudFBvc2l0aW9uKHRhcmdldCl7XG5cdFx0XHR2YXIgaUZyYW1lUG9zaXRpb24gPSB0YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cblx0XHRcdGdldFBhZ2VQb3NpdGlvbihpZnJhbWVJZCk7XG5cblx0XHRcdHJldHVybiB7XG5cdFx0XHRcdHg6IE1hdGguZmxvb3IoIE51bWJlcihpRnJhbWVQb3NpdGlvbi5sZWZ0KSArIE51bWJlcihwYWdlUG9zaXRpb24ueCkgKSxcblx0XHRcdFx0eTogTWF0aC5mbG9vciggTnVtYmVyKGlGcmFtZVBvc2l0aW9uLnRvcCkgICsgTnVtYmVyKHBhZ2VQb3NpdGlvbi55KSApXG5cdFx0XHR9O1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIHNjcm9sbFJlcXVlc3RGcm9tQ2hpbGQoYWRkT2Zmc2V0KXtcblx0XHRcdC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovICAvL05vdCB0ZXN0YWJsZSBpbiBLYXJtYVxuXHRcdFx0ZnVuY3Rpb24gcmVwb3NpdGlvbigpe1xuXHRcdFx0XHRwYWdlUG9zaXRpb24gPSBuZXdQb3NpdGlvbjtcblx0XHRcdFx0c2Nyb2xsVG8oKTtcblx0XHRcdFx0bG9nKGlmcmFtZUlkLCctLScpO1xuXHRcdFx0fVxuXG5cdFx0XHRmdW5jdGlvbiBjYWxjT2Zmc2V0KCl7XG5cdFx0XHRcdHJldHVybiB7XG5cdFx0XHRcdFx0eDogTnVtYmVyKG1lc3NhZ2VEYXRhLndpZHRoKSArIG9mZnNldC54LFxuXHRcdFx0XHRcdHk6IE51bWJlcihtZXNzYWdlRGF0YS5oZWlnaHQpICsgb2Zmc2V0Lnlcblx0XHRcdFx0fTtcblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gc2Nyb2xsUGFyZW50KCl7XG5cdFx0XHRcdGlmICh3aW5kb3cucGFyZW50SUZyYW1lKXtcblx0XHRcdFx0XHR3aW5kb3cucGFyZW50SUZyYW1lWydzY3JvbGxUbycrKGFkZE9mZnNldD8nT2Zmc2V0JzonJyldKG5ld1Bvc2l0aW9uLngsbmV3UG9zaXRpb24ueSk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0d2FybihpZnJhbWVJZCwnVW5hYmxlIHRvIHNjcm9sbCB0byByZXF1ZXN0ZWQgcG9zaXRpb24sIHdpbmRvdy5wYXJlbnRJRnJhbWUgbm90IGZvdW5kJyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0dmFyXG5cdFx0XHRcdG9mZnNldCA9IGFkZE9mZnNldCA/IGdldEVsZW1lbnRQb3NpdGlvbihtZXNzYWdlRGF0YS5pZnJhbWUpIDoge3g6MCx5OjB9LFxuXHRcdFx0XHRuZXdQb3NpdGlvbiA9IGNhbGNPZmZzZXQoKTtcblxuXHRcdFx0bG9nKGlmcmFtZUlkLCdSZXBvc2l0aW9uIHJlcXVlc3RlZCBmcm9tIGlGcmFtZSAob2Zmc2V0IHg6JytvZmZzZXQueCsnIHk6JytvZmZzZXQueSsnKScpO1xuXG5cdFx0XHRpZih3aW5kb3cudG9wICE9PSB3aW5kb3cuc2VsZil7XG5cdFx0XHRcdHNjcm9sbFBhcmVudCgpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmVwb3NpdGlvbigpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIHNjcm9sbFRvKCl7XG5cdFx0XHRpZiAoZmFsc2UgIT09IGNhbGxiYWNrKCdzY3JvbGxDYWxsYmFjaycscGFnZVBvc2l0aW9uKSl7XG5cdFx0XHRcdHNldFBhZ2VQb3NpdGlvbihpZnJhbWVJZCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHR1bnNldFBhZ2VQb3NpdGlvbigpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGZpbmRUYXJnZXQobG9jYXRpb24pe1xuXHRcdFx0ZnVuY3Rpb24ganVtcFRvVGFyZ2V0KCl7XG5cdFx0XHRcdHZhciBqdW1wUG9zaXRpb24gPSBnZXRFbGVtZW50UG9zaXRpb24odGFyZ2V0KTtcblxuXHRcdFx0XHRsb2coaWZyYW1lSWQsJ01vdmluZyB0byBpbiBwYWdlIGxpbmsgKCMnK2hhc2grJykgYXQgeDogJytqdW1wUG9zaXRpb24ueCsnIHk6ICcranVtcFBvc2l0aW9uLnkpO1xuXHRcdFx0XHRwYWdlUG9zaXRpb24gPSB7XG5cdFx0XHRcdFx0eDoganVtcFBvc2l0aW9uLngsXG5cdFx0XHRcdFx0eToganVtcFBvc2l0aW9uLnlcblx0XHRcdFx0fTtcblxuXHRcdFx0XHRzY3JvbGxUbygpO1xuXHRcdFx0XHRsb2coaWZyYW1lSWQsJy0tJyk7XG5cdFx0XHR9XG5cblx0XHRcdGZ1bmN0aW9uIGp1bXBUb1BhcmVudCgpe1xuXHRcdFx0XHRpZiAod2luZG93LnBhcmVudElGcmFtZSl7XG5cdFx0XHRcdFx0d2luZG93LnBhcmVudElGcmFtZS5tb3ZlVG9BbmNob3IoaGFzaCk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0bG9nKGlmcmFtZUlkLCdJbiBwYWdlIGxpbmsgIycraGFzaCsnIG5vdCBmb3VuZCBhbmQgd2luZG93LnBhcmVudElGcmFtZSBub3QgZm91bmQnKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHR2YXJcblx0XHRcdFx0aGFzaCAgICAgPSBsb2NhdGlvbi5zcGxpdCgnIycpWzFdIHx8ICcnLFxuXHRcdFx0XHRoYXNoRGF0YSA9IGRlY29kZVVSSUNvbXBvbmVudChoYXNoKSxcblx0XHRcdFx0dGFyZ2V0ICAgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChoYXNoRGF0YSkgfHwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeU5hbWUoaGFzaERhdGEpWzBdO1xuXG5cdFx0XHRpZiAodGFyZ2V0KXtcblx0XHRcdFx0anVtcFRvVGFyZ2V0KCk7XG5cdFx0XHR9IGVsc2UgaWYod2luZG93LnRvcCE9PXdpbmRvdy5zZWxmKXtcblx0XHRcdFx0anVtcFRvUGFyZW50KCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRsb2coaWZyYW1lSWQsJ0luIHBhZ2UgbGluayAjJytoYXNoKycgbm90IGZvdW5kJyk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gY2FsbGJhY2soZnVuY05hbWUsdmFsKXtcblx0XHRcdHJldHVybiBjaGtDYWxsYmFjayhpZnJhbWVJZCxmdW5jTmFtZSx2YWwpO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGFjdGlvbk1zZygpe1xuXG5cdFx0XHRpZihzZXR0aW5nc1tpZnJhbWVJZF0uZmlyc3RSdW4pIGZpcnN0UnVuKCk7XG5cblx0XHRcdHN3aXRjaChtZXNzYWdlRGF0YS50eXBlKXtcblx0XHRcdGNhc2UgJ2Nsb3NlJzpcblx0XHRcdFx0Y2xvc2VJRnJhbWUobWVzc2FnZURhdGEuaWZyYW1lKTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICdtZXNzYWdlJzpcblx0XHRcdFx0Zm9yd2FyZE1zZ0Zyb21JRnJhbWUoZ2V0TXNnQm9keSg2KSk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAnc2Nyb2xsVG8nOlxuXHRcdFx0XHRzY3JvbGxSZXF1ZXN0RnJvbUNoaWxkKGZhbHNlKTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICdzY3JvbGxUb09mZnNldCc6XG5cdFx0XHRcdHNjcm9sbFJlcXVlc3RGcm9tQ2hpbGQodHJ1ZSk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAncGFnZUluZm8nOlxuXHRcdFx0XHRzZW5kUGFnZUluZm9Ub0lmcmFtZShzZXR0aW5nc1tpZnJhbWVJZF0uaWZyYW1lLGlmcmFtZUlkKTtcblx0XHRcdFx0c3RhcnRQYWdlSW5mb01vbml0b3IoKTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICdwYWdlSW5mb1N0b3AnOlxuXHRcdFx0XHRzdG9wUGFnZUluZm9Nb25pdG9yKCk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAnaW5QYWdlTGluayc6XG5cdFx0XHRcdGZpbmRUYXJnZXQoZ2V0TXNnQm9keSg5KSk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAncmVzZXQnOlxuXHRcdFx0XHRyZXNldElGcmFtZShtZXNzYWdlRGF0YSk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAnaW5pdCc6XG5cdFx0XHRcdHJlc2l6ZUlGcmFtZSgpO1xuXHRcdFx0XHRjYWxsYmFjaygnaW5pdENhbGxiYWNrJyxtZXNzYWdlRGF0YS5pZnJhbWUpO1xuXHRcdFx0XHRjYWxsYmFjaygncmVzaXplZENhbGxiYWNrJyxtZXNzYWdlRGF0YSk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0cmVzaXplSUZyYW1lKCk7XG5cdFx0XHRcdGNhbGxiYWNrKCdyZXNpemVkQ2FsbGJhY2snLG1lc3NhZ2VEYXRhKTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRmdW5jdGlvbiBoYXNTZXR0aW5ncyhpZnJhbWVJZCl7XG5cdFx0XHR2YXIgcmV0Qm9vbCA9IHRydWU7XG5cblx0XHRcdGlmICghc2V0dGluZ3NbaWZyYW1lSWRdKXtcblx0XHRcdFx0cmV0Qm9vbCA9IGZhbHNlO1xuXHRcdFx0XHR3YXJuKG1lc3NhZ2VEYXRhLnR5cGUgKyAnIE5vIHNldHRpbmdzIGZvciAnICsgaWZyYW1lSWQgKyAnLiBNZXNzYWdlIHdhczogJyArIG1zZyk7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiByZXRCb29sO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGlGcmFtZVJlYWR5TXNnUmVjZWl2ZWQoKXtcblx0XHRcdGZvciAodmFyIGlmcmFtZUlkIGluIHNldHRpbmdzKXtcblx0XHRcdFx0dHJpZ2dlcignaUZyYW1lIHJlcXVlc3RlZCBpbml0JyxjcmVhdGVPdXRnb2luZ01zZyhpZnJhbWVJZCksZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWZyYW1lSWQpLGlmcmFtZUlkKTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRmdW5jdGlvbiBmaXJzdFJ1bigpIHtcblx0XHRcdHNldHRpbmdzW2lmcmFtZUlkXS5maXJzdFJ1biA9IGZhbHNlO1xuXHRcdH1cblxuXHRcdHZhclxuXHRcdFx0bXNnID0gZXZlbnQuZGF0YSxcblx0XHRcdG1lc3NhZ2VEYXRhID0ge30sXG5cdFx0XHRpZnJhbWVJZCA9IG51bGw7XG5cblx0XHRpZignW2lGcmFtZVJlc2l6ZXJDaGlsZF1SZWFkeScgPT09IG1zZyl7XG5cdFx0XHRpRnJhbWVSZWFkeU1zZ1JlY2VpdmVkKCk7XG5cdFx0fSBlbHNlIGlmIChpc01lc3NhZ2VGb3JVcygpKXtcblx0XHRcdG1lc3NhZ2VEYXRhID0gcHJvY2Vzc01zZygpO1xuXHRcdFx0aWZyYW1lSWQgICAgPSBsb2dJZCA9IG1lc3NhZ2VEYXRhLmlkO1xuXG5cdFx0XHRpZiAoIWlzTWVzc2FnZUZyb21NZXRhUGFyZW50KCkgJiYgaGFzU2V0dGluZ3MoaWZyYW1lSWQpKXtcblx0XHRcdFx0bG9nKGlmcmFtZUlkLCdSZWNlaXZlZDogJyttc2cpO1xuXG5cdFx0XHRcdGlmICggY2hlY2tJRnJhbWVFeGlzdHMoKSAmJiBpc01lc3NhZ2VGcm9tSUZyYW1lKCkgKXtcblx0XHRcdFx0XHRhY3Rpb25Nc2coKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0gZWxzZSB7XG5cdFx0XHRpbmZvKGlmcmFtZUlkLCdJZ25vcmVkOiAnK21zZyk7XG5cdFx0fVxuXG5cdH1cblxuXG5cdGZ1bmN0aW9uIGNoa0NhbGxiYWNrKGlmcmFtZUlkLGZ1bmNOYW1lLHZhbCl7XG5cdFx0dmFyXG5cdFx0XHRmdW5jID0gbnVsbCxcblx0XHRcdHJldFZhbCA9IG51bGw7XG5cblx0XHRpZihzZXR0aW5nc1tpZnJhbWVJZF0pe1xuXHRcdFx0ZnVuYyA9IHNldHRpbmdzW2lmcmFtZUlkXVtmdW5jTmFtZV07XG5cblx0XHRcdGlmKCAnZnVuY3Rpb24nID09PSB0eXBlb2YgZnVuYyl7XG5cdFx0XHRcdHJldFZhbCA9IGZ1bmModmFsKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoZnVuY05hbWUrJyBvbiBpRnJhbWVbJytpZnJhbWVJZCsnXSBpcyBub3QgYSBmdW5jdGlvbicpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHJldHVybiByZXRWYWw7XG5cdH1cblxuXHRmdW5jdGlvbiBjbG9zZUlGcmFtZShpZnJhbWUpe1xuXHRcdHZhciBpZnJhbWVJZCA9IGlmcmFtZS5pZDtcblxuXHRcdGxvZyhpZnJhbWVJZCwnUmVtb3ZpbmcgaUZyYW1lOiAnK2lmcmFtZUlkKTtcblx0XHRpZiAoaWZyYW1lLnBhcmVudE5vZGUpIHsgaWZyYW1lLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoaWZyYW1lKTsgfVxuXHRcdGNoa0NhbGxiYWNrKGlmcmFtZUlkLCdjbG9zZWRDYWxsYmFjaycsaWZyYW1lSWQpO1xuXHRcdGxvZyhpZnJhbWVJZCwnLS0nKTtcblx0XHRkZWxldGUgc2V0dGluZ3NbaWZyYW1lSWRdO1xuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0UGFnZVBvc2l0aW9uKGlmcmFtZUlkKXtcblx0XHRpZihudWxsID09PSBwYWdlUG9zaXRpb24pe1xuXHRcdFx0cGFnZVBvc2l0aW9uID0ge1xuXHRcdFx0XHR4OiAod2luZG93LnBhZ2VYT2Zmc2V0ICE9PSB1bmRlZmluZWQpID8gd2luZG93LnBhZ2VYT2Zmc2V0IDogZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbExlZnQsXG5cdFx0XHRcdHk6ICh3aW5kb3cucGFnZVlPZmZzZXQgIT09IHVuZGVmaW5lZCkgPyB3aW5kb3cucGFnZVlPZmZzZXQgOiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wXG5cdFx0XHR9O1xuXHRcdFx0bG9nKGlmcmFtZUlkLCdHZXQgcGFnZSBwb3NpdGlvbjogJytwYWdlUG9zaXRpb24ueCsnLCcrcGFnZVBvc2l0aW9uLnkpO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHNldFBhZ2VQb3NpdGlvbihpZnJhbWVJZCl7XG5cdFx0aWYobnVsbCAhPT0gcGFnZVBvc2l0aW9uKXtcblx0XHRcdHdpbmRvdy5zY3JvbGxUbyhwYWdlUG9zaXRpb24ueCxwYWdlUG9zaXRpb24ueSk7XG5cdFx0XHRsb2coaWZyYW1lSWQsJ1NldCBwYWdlIHBvc2l0aW9uOiAnK3BhZ2VQb3NpdGlvbi54KycsJytwYWdlUG9zaXRpb24ueSk7XG5cdFx0XHR1bnNldFBhZ2VQb3NpdGlvbigpO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHVuc2V0UGFnZVBvc2l0aW9uKCl7XG5cdFx0cGFnZVBvc2l0aW9uID0gbnVsbDtcblx0fVxuXG5cdGZ1bmN0aW9uIHJlc2V0SUZyYW1lKG1lc3NhZ2VEYXRhKXtcblx0XHRmdW5jdGlvbiByZXNldCgpe1xuXHRcdFx0c2V0U2l6ZShtZXNzYWdlRGF0YSk7XG5cdFx0XHR0cmlnZ2VyKCdyZXNldCcsJ3Jlc2V0JyxtZXNzYWdlRGF0YS5pZnJhbWUsbWVzc2FnZURhdGEuaWQpO1xuXHRcdH1cblxuXHRcdGxvZyhtZXNzYWdlRGF0YS5pZCwnU2l6ZSByZXNldCByZXF1ZXN0ZWQgYnkgJysoJ2luaXQnPT09bWVzc2FnZURhdGEudHlwZT8naG9zdCBwYWdlJzonaUZyYW1lJykpO1xuXHRcdGdldFBhZ2VQb3NpdGlvbihtZXNzYWdlRGF0YS5pZCk7XG5cdFx0c3luY1Jlc2l6ZShyZXNldCxtZXNzYWdlRGF0YSwncmVzZXQnKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFNpemUobWVzc2FnZURhdGEpe1xuXHRcdGZ1bmN0aW9uIHNldERpbWVuc2lvbihkaW1lbnNpb24pe1xuXHRcdFx0bWVzc2FnZURhdGEuaWZyYW1lLnN0eWxlW2RpbWVuc2lvbl0gPSBtZXNzYWdlRGF0YVtkaW1lbnNpb25dICsgJ3B4Jztcblx0XHRcdGxvZyhcblx0XHRcdFx0bWVzc2FnZURhdGEuaWQsXG5cdFx0XHRcdCdJRnJhbWUgKCcgKyBpZnJhbWVJZCArXG5cdFx0XHRcdCcpICcgKyBkaW1lbnNpb24gK1xuXHRcdFx0XHQnIHNldCB0byAnICsgbWVzc2FnZURhdGFbZGltZW5zaW9uXSArICdweCdcblx0XHRcdCk7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gY2hrWmVybyhkaW1lbnNpb24pe1xuXHRcdFx0Ly9GaXJlRm94IHNldHMgZGltZW5zaW9uIG9mIGhpZGRlbiBpRnJhbWVzIHRvIHplcm8uXG5cdFx0XHQvL1NvIGlmIHdlIGRldGVjdCB0aGF0IHNldCB1cCBhbiBldmVudCB0byBjaGVjayBmb3Jcblx0XHRcdC8vd2hlbiBpRnJhbWUgYmVjb21lcyB2aXNpYmxlLlxuXG5cdFx0XHQvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqLyAgLy9Ob3QgdGVzdGFibGUgaW4gUGhhbnRvbUpTXG5cdFx0XHRpZiAoIWhpZGRlbkNoZWNrRW5hYmxlZCAmJiAnMCcgPT09IG1lc3NhZ2VEYXRhW2RpbWVuc2lvbl0pe1xuXHRcdFx0XHRoaWRkZW5DaGVja0VuYWJsZWQgPSB0cnVlO1xuXHRcdFx0XHRsb2coaWZyYW1lSWQsJ0hpZGRlbiBpRnJhbWUgZGV0ZWN0ZWQsIGNyZWF0aW5nIHZpc2liaWxpdHkgbGlzdGVuZXInKTtcblx0XHRcdFx0Zml4SGlkZGVuSUZyYW1lcygpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIHByb2Nlc3NEaW1lbnNpb24oZGltZW5zaW9uKXtcblx0XHRcdHNldERpbWVuc2lvbihkaW1lbnNpb24pO1xuXHRcdFx0Y2hrWmVybyhkaW1lbnNpb24pO1xuXHRcdH1cblxuXHRcdHZhciBpZnJhbWVJZCA9IG1lc3NhZ2VEYXRhLmlmcmFtZS5pZDtcblxuXHRcdGlmKHNldHRpbmdzW2lmcmFtZUlkXSl7XG5cdFx0XHRpZiggc2V0dGluZ3NbaWZyYW1lSWRdLnNpemVIZWlnaHQpIHsgcHJvY2Vzc0RpbWVuc2lvbignaGVpZ2h0Jyk7IH1cblx0XHRcdGlmKCBzZXR0aW5nc1tpZnJhbWVJZF0uc2l6ZVdpZHRoICkgeyBwcm9jZXNzRGltZW5zaW9uKCd3aWR0aCcpOyB9XG5cdFx0fVxuXHR9XG5cblx0ZnVuY3Rpb24gc3luY1Jlc2l6ZShmdW5jLG1lc3NhZ2VEYXRhLGRvTm90U3luYyl7XG5cdFx0LyogaXN0YW5idWwgaWdub3JlIGlmICovICAvL05vdCB0ZXN0YWJsZSBpbiBQaGFudG9tSlNcblx0XHRpZihkb05vdFN5bmMhPT1tZXNzYWdlRGF0YS50eXBlICYmIHJlcXVlc3RBbmltYXRpb25GcmFtZSl7XG5cdFx0XHRsb2cobWVzc2FnZURhdGEuaWQsJ1JlcXVlc3RpbmcgYW5pbWF0aW9uIGZyYW1lJyk7XG5cdFx0XHRyZXF1ZXN0QW5pbWF0aW9uRnJhbWUoZnVuYyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdGZ1bmMoKTtcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiB0cmlnZ2VyKGNhbGxlZU1zZyxtc2csaWZyYW1lLGlkKXtcblx0XHRmdW5jdGlvbiBwb3N0TWVzc2FnZVRvSUZyYW1lKCl7XG5cdFx0XHR2YXIgdGFyZ2V0ID0gc2V0dGluZ3NbaWRdLnRhcmdldE9yaWdpbjtcblx0XHRcdGxvZyhpZCwnWycgKyBjYWxsZWVNc2cgKyAnXSBTZW5kaW5nIG1zZyB0byBpZnJhbWVbJytpZCsnXSAoJyttc2crJykgdGFyZ2V0T3JpZ2luOiAnK3RhcmdldCk7XG5cdFx0XHRpZnJhbWUuY29udGVudFdpbmRvdy5wb3N0TWVzc2FnZSggbXNnSWQgKyBtc2csIHRhcmdldCApO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGlGcmFtZU5vdEZvdW5kKCl7XG5cdFx0XHR3YXJuKGlkLCdbJyArIGNhbGxlZU1zZyArICddIElGcmFtZSgnK2lkKycpIG5vdCBmb3VuZCcpO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGNoa0FuZFNlbmQoKXtcblx0XHRcdGlmKGlmcmFtZSAmJiAnY29udGVudFdpbmRvdycgaW4gaWZyYW1lICYmIChudWxsICE9PSBpZnJhbWUuY29udGVudFdpbmRvdykpeyAvL051bGwgdGVzdCBmb3IgUGhhbnRvbUpTXG5cdFx0XHRcdHBvc3RNZXNzYWdlVG9JRnJhbWUoKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdGlGcmFtZU5vdEZvdW5kKCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0aWQgPSBpZCB8fCBpZnJhbWUuaWQ7XG5cblx0XHRpZihzZXR0aW5nc1tpZF0pIHtcblx0XHRcdGNoa0FuZFNlbmQoKTtcblx0XHR9XG5cblx0fVxuXG5cdGZ1bmN0aW9uIGNyZWF0ZU91dGdvaW5nTXNnKGlmcmFtZUlkKXtcblx0XHRyZXR1cm4gaWZyYW1lSWQgK1xuXHRcdFx0JzonICsgc2V0dGluZ3NbaWZyYW1lSWRdLmJvZHlNYXJnaW5WMSArXG5cdFx0XHQnOicgKyBzZXR0aW5nc1tpZnJhbWVJZF0uc2l6ZVdpZHRoICtcblx0XHRcdCc6JyArIHNldHRpbmdzW2lmcmFtZUlkXS5sb2cgK1xuXHRcdFx0JzonICsgc2V0dGluZ3NbaWZyYW1lSWRdLmludGVydmFsICtcblx0XHRcdCc6JyArIHNldHRpbmdzW2lmcmFtZUlkXS5lbmFibGVQdWJsaWNNZXRob2RzICtcblx0XHRcdCc6JyArIHNldHRpbmdzW2lmcmFtZUlkXS5hdXRvUmVzaXplICtcblx0XHRcdCc6JyArIHNldHRpbmdzW2lmcmFtZUlkXS5ib2R5TWFyZ2luICtcblx0XHRcdCc6JyArIHNldHRpbmdzW2lmcmFtZUlkXS5oZWlnaHRDYWxjdWxhdGlvbk1ldGhvZCArXG5cdFx0XHQnOicgKyBzZXR0aW5nc1tpZnJhbWVJZF0uYm9keUJhY2tncm91bmQgK1xuXHRcdFx0JzonICsgc2V0dGluZ3NbaWZyYW1lSWRdLmJvZHlQYWRkaW5nICtcblx0XHRcdCc6JyArIHNldHRpbmdzW2lmcmFtZUlkXS50b2xlcmFuY2UgK1xuXHRcdFx0JzonICsgc2V0dGluZ3NbaWZyYW1lSWRdLmluUGFnZUxpbmtzICtcblx0XHRcdCc6JyArIHNldHRpbmdzW2lmcmFtZUlkXS5yZXNpemVGcm9tICtcblx0XHRcdCc6JyArIHNldHRpbmdzW2lmcmFtZUlkXS53aWR0aENhbGN1bGF0aW9uTWV0aG9kO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0dXBJRnJhbWUoaWZyYW1lLG9wdGlvbnMpe1xuXHRcdGZ1bmN0aW9uIHNldExpbWl0cygpe1xuXHRcdFx0ZnVuY3Rpb24gYWRkU3R5bGUoc3R5bGUpe1xuXHRcdFx0XHRpZiAoKEluZmluaXR5ICE9PSBzZXR0aW5nc1tpZnJhbWVJZF1bc3R5bGVdKSAmJiAoMCAhPT0gc2V0dGluZ3NbaWZyYW1lSWRdW3N0eWxlXSkpe1xuXHRcdFx0XHRcdGlmcmFtZS5zdHlsZVtzdHlsZV0gPSBzZXR0aW5nc1tpZnJhbWVJZF1bc3R5bGVdICsgJ3B4Jztcblx0XHRcdFx0XHRsb2coaWZyYW1lSWQsJ1NldCAnK3N0eWxlKycgPSAnK3NldHRpbmdzW2lmcmFtZUlkXVtzdHlsZV0rJ3B4Jyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gY2hrTWluTWF4KGRpbWVuc2lvbil7XG5cdFx0XHRcdGlmIChzZXR0aW5nc1tpZnJhbWVJZF1bJ21pbicrZGltZW5zaW9uXT5zZXR0aW5nc1tpZnJhbWVJZF1bJ21heCcrZGltZW5zaW9uXSl7XG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdWYWx1ZSBmb3IgbWluJytkaW1lbnNpb24rJyBjYW4gbm90IGJlIGdyZWF0ZXIgdGhhbiBtYXgnK2RpbWVuc2lvbik7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0Y2hrTWluTWF4KCdIZWlnaHQnKTtcblx0XHRcdGNoa01pbk1heCgnV2lkdGgnKTtcblxuXHRcdFx0YWRkU3R5bGUoJ21heEhlaWdodCcpO1xuXHRcdFx0YWRkU3R5bGUoJ21pbkhlaWdodCcpO1xuXHRcdFx0YWRkU3R5bGUoJ21heFdpZHRoJyk7XG5cdFx0XHRhZGRTdHlsZSgnbWluV2lkdGgnKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBuZXdJZCgpe1xuXHRcdFx0dmFyIGlkID0gKChvcHRpb25zICYmIG9wdGlvbnMuaWQpIHx8IGRlZmF1bHRzLmlkICsgY291bnQrKyk7XG5cdFx0XHRpZiAgKG51bGwgIT09IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlkKSl7XG5cdFx0XHRcdGlkID0gaWQgKyBjb3VudCsrO1xuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIGlkO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGVuc3VyZUhhc0lkKGlmcmFtZUlkKXtcblx0XHRcdGxvZ0lkPWlmcmFtZUlkO1xuXHRcdFx0aWYgKCcnPT09aWZyYW1lSWQpe1xuXHRcdFx0XHRpZnJhbWUuaWQgPSBpZnJhbWVJZCA9ICBuZXdJZCgpO1xuXHRcdFx0XHRsb2dFbmFibGVkID0gKG9wdGlvbnMgfHwge30pLmxvZztcblx0XHRcdFx0bG9nSWQ9aWZyYW1lSWQ7XG5cdFx0XHRcdGxvZyhpZnJhbWVJZCwnQWRkZWQgbWlzc2luZyBpZnJhbWUgSUQ6ICcrIGlmcmFtZUlkICsnICgnICsgaWZyYW1lLnNyYyArICcpJyk7XG5cdFx0XHR9XG5cblxuXHRcdFx0cmV0dXJuIGlmcmFtZUlkO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIHNldFNjcm9sbGluZygpe1xuXHRcdFx0bG9nKGlmcmFtZUlkLCdJRnJhbWUgc2Nyb2xsaW5nICcgKyAoc2V0dGluZ3NbaWZyYW1lSWRdLnNjcm9sbGluZyA/ICdlbmFibGVkJyA6ICdkaXNhYmxlZCcpICsgJyBmb3IgJyArIGlmcmFtZUlkKTtcblx0XHRcdGlmcmFtZS5zdHlsZS5vdmVyZmxvdyA9IGZhbHNlID09PSBzZXR0aW5nc1tpZnJhbWVJZF0uc2Nyb2xsaW5nID8gJ2hpZGRlbicgOiAnYXV0byc7XG5cdFx0XHRpZnJhbWUuc2Nyb2xsaW5nICAgICAgPSBmYWxzZSA9PT0gc2V0dGluZ3NbaWZyYW1lSWRdLnNjcm9sbGluZyA/ICdubycgOiAneWVzJztcblx0XHR9XG5cblx0XHQvL1RoZSBWMSBpRnJhbWUgc2NyaXB0IGV4cGVjdHMgYW4gaW50LCB3aGVyZSBhcyBpbiBWMiBleHBlY3RzIGEgQ1NTXG5cdFx0Ly9zdHJpbmcgdmFsdWUgc3VjaCBhcyAnMXB4IDNlbScsIHNvIGlmIHdlIGhhdmUgYW4gaW50IGZvciBWMiwgc2V0IFYxPVYyXG5cdFx0Ly9hbmQgdGhlbiBjb252ZXJ0IFYyIHRvIGEgc3RyaW5nIFBYIHZhbHVlLlxuXHRcdGZ1bmN0aW9uIHNldHVwQm9keU1hcmdpblZhbHVlcygpe1xuXHRcdFx0aWYgKCgnbnVtYmVyJz09PXR5cGVvZihzZXR0aW5nc1tpZnJhbWVJZF0uYm9keU1hcmdpbikpIHx8ICgnMCc9PT1zZXR0aW5nc1tpZnJhbWVJZF0uYm9keU1hcmdpbikpe1xuXHRcdFx0XHRzZXR0aW5nc1tpZnJhbWVJZF0uYm9keU1hcmdpblYxID0gc2V0dGluZ3NbaWZyYW1lSWRdLmJvZHlNYXJnaW47XG5cdFx0XHRcdHNldHRpbmdzW2lmcmFtZUlkXS5ib2R5TWFyZ2luICAgPSAnJyArIHNldHRpbmdzW2lmcmFtZUlkXS5ib2R5TWFyZ2luICsgJ3B4Jztcblx0XHRcdH1cblx0XHR9XG5cblx0XHRmdW5jdGlvbiBjaGVja1Jlc2V0KCl7XG5cdFx0XHQvLyBSZWR1Y2Ugc2NvcGUgb2YgZmlyc3RSdW4gdG8gZnVuY3Rpb24sIGJlY2F1c2UgSUU4J3MgSlMgZXhlY3V0aW9uXG5cdFx0XHQvLyBjb250ZXh0IHN0YWNrIGlzIGJvcmtlZCBhbmQgdGhpcyB2YWx1ZSBnZXRzIGV4dGVybmFsbHlcblx0XHRcdC8vIGNoYW5nZWQgbWlkd2F5IHRocm91Z2ggcnVubmluZyB0aGlzIGZ1bmN0aW9uISEhXG5cdFx0XHR2YXJcblx0XHRcdFx0Zmlyc3RSdW4gICAgICAgICAgID0gc2V0dGluZ3NbaWZyYW1lSWRdLmZpcnN0UnVuLFxuXHRcdFx0XHRyZXNldFJlcXVlcnRNZXRob2QgPSBzZXR0aW5nc1tpZnJhbWVJZF0uaGVpZ2h0Q2FsY3VsYXRpb25NZXRob2QgaW4gcmVzZXRSZXF1aXJlZE1ldGhvZHM7XG5cblx0XHRcdGlmICghZmlyc3RSdW4gJiYgcmVzZXRSZXF1ZXJ0TWV0aG9kKXtcblx0XHRcdFx0cmVzZXRJRnJhbWUoe2lmcmFtZTppZnJhbWUsIGhlaWdodDowLCB3aWR0aDowLCB0eXBlOidpbml0J30pO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIHNldHVwSUZyYW1lT2JqZWN0KCl7XG5cdFx0XHRpZihGdW5jdGlvbi5wcm90b3R5cGUuYmluZCl7IC8vSWdub3JlIHVucG9seWZpbGxlZCBJRTguXG5cdFx0XHRcdHNldHRpbmdzW2lmcmFtZUlkXS5pZnJhbWUuaUZyYW1lUmVzaXplciA9IHtcblxuXHRcdFx0XHRcdGNsb3NlICAgICAgICA6IGNsb3NlSUZyYW1lLmJpbmQobnVsbCxzZXR0aW5nc1tpZnJhbWVJZF0uaWZyYW1lKSxcblxuXHRcdFx0XHRcdHJlc2l6ZSAgICAgICA6IHRyaWdnZXIuYmluZChudWxsLCdXaW5kb3cgcmVzaXplJywgJ3Jlc2l6ZScsIHNldHRpbmdzW2lmcmFtZUlkXS5pZnJhbWUpLFxuXG5cdFx0XHRcdFx0bW92ZVRvQW5jaG9yIDogZnVuY3Rpb24oYW5jaG9yKXtcblx0XHRcdFx0XHRcdHRyaWdnZXIoJ01vdmUgdG8gYW5jaG9yJywnbW92ZVRvQW5jaG9yOicrYW5jaG9yLCBzZXR0aW5nc1tpZnJhbWVJZF0uaWZyYW1lLGlmcmFtZUlkKTtcblx0XHRcdFx0XHR9LFxuXG5cdFx0XHRcdFx0c2VuZE1lc3NhZ2UgIDogZnVuY3Rpb24obWVzc2FnZSl7XG5cdFx0XHRcdFx0XHRtZXNzYWdlID0gSlNPTi5zdHJpbmdpZnkobWVzc2FnZSk7XG5cdFx0XHRcdFx0XHR0cmlnZ2VyKCdTZW5kIE1lc3NhZ2UnLCdtZXNzYWdlOicrbWVzc2FnZSwgc2V0dGluZ3NbaWZyYW1lSWRdLmlmcmFtZSxpZnJhbWVJZCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9O1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdC8vV2UgaGF2ZSB0byBjYWxsIHRyaWdnZXIgdHdpY2UsIGFzIHdlIGNhbiBub3QgYmUgc3VyZSBpZiBhbGxcblx0XHQvL2lmcmFtZXMgaGF2ZSBjb21wbGV0ZWQgbG9hZGluZyB3aGVuIHRoaXMgY29kZSBydW5zLiBUaGVcblx0XHQvL2V2ZW50IGxpc3RlbmVyIGFsc28gY2F0Y2hlcyB0aGUgcGFnZSBjaGFuZ2luZyBpbiB0aGUgaUZyYW1lLlxuXHRcdGZ1bmN0aW9uIGluaXQobXNnKXtcblx0XHRcdGZ1bmN0aW9uIGlGcmFtZUxvYWRlZCgpe1xuXHRcdFx0XHR0cmlnZ2VyKCdpRnJhbWUub25sb2FkJyxtc2csaWZyYW1lKTtcblx0XHRcdFx0Y2hlY2tSZXNldCgpO1xuXHRcdFx0fVxuXG5cdFx0XHRhZGRFdmVudExpc3RlbmVyKGlmcmFtZSwnbG9hZCcsaUZyYW1lTG9hZGVkKTtcblx0XHRcdHRyaWdnZXIoJ2luaXQnLG1zZyxpZnJhbWUpO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGNoZWNrT3B0aW9ucyhvcHRpb25zKXtcblx0XHRcdGlmICgnb2JqZWN0JyAhPT0gdHlwZW9mIG9wdGlvbnMpe1xuXHRcdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdPcHRpb25zIGlzIG5vdCBhbiBvYmplY3QnKTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRmdW5jdGlvbiBjb3B5T3B0aW9ucyhvcHRpb25zKXtcblx0XHRcdGZvciAodmFyIG9wdGlvbiBpbiBkZWZhdWx0cykge1xuXHRcdFx0XHRpZiAoZGVmYXVsdHMuaGFzT3duUHJvcGVydHkob3B0aW9uKSl7XG5cdFx0XHRcdFx0c2V0dGluZ3NbaWZyYW1lSWRdW29wdGlvbl0gPSBvcHRpb25zLmhhc093blByb3BlcnR5KG9wdGlvbikgPyBvcHRpb25zW29wdGlvbl0gOiBkZWZhdWx0c1tvcHRpb25dO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gZ2V0VGFyZ2V0T3JpZ2luIChyZW1vdGVIb3N0KXtcblx0XHRcdHJldHVybiAoJycgPT09IHJlbW90ZUhvc3QgfHwgJ2ZpbGU6Ly8nID09PSByZW1vdGVIb3N0KSA/ICcqJyA6IHJlbW90ZUhvc3Q7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gcHJvY2Vzc09wdGlvbnMob3B0aW9ucyl7XG5cdFx0XHRvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblx0XHRcdHNldHRpbmdzW2lmcmFtZUlkXSA9IHtcblx0XHRcdFx0Zmlyc3RSdW5cdDogdHJ1ZSxcblx0XHRcdFx0aWZyYW1lXHRcdDogaWZyYW1lLFxuXHRcdFx0XHRyZW1vdGVIb3N0XHQ6IGlmcmFtZS5zcmMuc3BsaXQoJy8nKS5zbGljZSgwLDMpLmpvaW4oJy8nKVxuXHRcdFx0fTtcblxuXHRcdFx0Y2hlY2tPcHRpb25zKG9wdGlvbnMpO1xuXHRcdFx0Y29weU9wdGlvbnMob3B0aW9ucyk7XG5cblx0XHRcdHNldHRpbmdzW2lmcmFtZUlkXS50YXJnZXRPcmlnaW4gPSB0cnVlID09PSBzZXR0aW5nc1tpZnJhbWVJZF0uY2hlY2tPcmlnaW4gPyBnZXRUYXJnZXRPcmlnaW4oc2V0dGluZ3NbaWZyYW1lSWRdLnJlbW90ZUhvc3QpIDogJyonO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGJlZW5IZXJlKCl7XG5cdFx0XHRyZXR1cm4gKGlmcmFtZUlkIGluIHNldHRpbmdzICYmICdpRnJhbWVSZXNpemVyJyBpbiBpZnJhbWUpO1xuXHRcdH1cblxuXHRcdHZhciBpZnJhbWVJZCA9IGVuc3VyZUhhc0lkKGlmcmFtZS5pZCk7XG5cblx0XHRpZiAoIWJlZW5IZXJlKCkpe1xuXHRcdFx0cHJvY2Vzc09wdGlvbnMob3B0aW9ucyk7XG5cdFx0XHRzZXRTY3JvbGxpbmcoKTtcblx0XHRcdHNldExpbWl0cygpO1xuXHRcdFx0c2V0dXBCb2R5TWFyZ2luVmFsdWVzKCk7XG5cdFx0XHRpbml0KGNyZWF0ZU91dGdvaW5nTXNnKGlmcmFtZUlkKSk7XG5cdFx0XHRzZXR1cElGcmFtZU9iamVjdCgpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR3YXJuKGlmcmFtZUlkLCdJZ25vcmVkIGlGcmFtZSwgYWxyZWFkeSBzZXR1cC4nKTtcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBkZWJvdWNlKGZuLHRpbWUpe1xuXHRcdGlmIChudWxsID09PSB0aW1lcil7XG5cdFx0XHR0aW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblx0XHRcdFx0dGltZXIgPSBudWxsO1xuXHRcdFx0XHRmbigpO1xuXHRcdFx0fSwgdGltZSk7XG5cdFx0fVxuXHR9XG5cblx0LyogaXN0YW5idWwgaWdub3JlIG5leHQgKi8gIC8vTm90IHRlc3RhYmxlIGluIFBoYW50b21KU1xuXHRmdW5jdGlvbiBmaXhIaWRkZW5JRnJhbWVzKCl7XG5cdFx0ZnVuY3Rpb24gY2hlY2tJRnJhbWVzKCl7XG5cdFx0XHRmdW5jdGlvbiBjaGVja0lGcmFtZShzZXR0aW5nSWQpe1xuXHRcdFx0XHRmdW5jdGlvbiBjaGtEaW1lbnNpb24oZGltZW5zaW9uKXtcblx0XHRcdFx0XHRyZXR1cm4gJzBweCcgPT09IHNldHRpbmdzW3NldHRpbmdJZF0uaWZyYW1lLnN0eWxlW2RpbWVuc2lvbl07XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRmdW5jdGlvbiBpc1Zpc2libGUoZWwpIHtcblx0XHRcdFx0XHRyZXR1cm4gKG51bGwgIT09IGVsLm9mZnNldFBhcmVudCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZiAoaXNWaXNpYmxlKHNldHRpbmdzW3NldHRpbmdJZF0uaWZyYW1lKSAmJiAoY2hrRGltZW5zaW9uKCdoZWlnaHQnKSB8fCBjaGtEaW1lbnNpb24oJ3dpZHRoJykpKXtcblx0XHRcdFx0XHR0cmlnZ2VyKCdWaXNpYmlsaXR5IGNoYW5nZScsICdyZXNpemUnLCBzZXR0aW5nc1tzZXR0aW5nSWRdLmlmcmFtZSxzZXR0aW5nSWQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdGZvciAodmFyIHNldHRpbmdJZCBpbiBzZXR0aW5ncyl7XG5cdFx0XHRcdGNoZWNrSUZyYW1lKHNldHRpbmdJZCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gbXV0YXRpb25PYnNlcnZlZChtdXRhdGlvbnMpe1xuXHRcdFx0bG9nKCd3aW5kb3cnLCdNdXRhdGlvbiBvYnNlcnZlZDogJyArIG11dGF0aW9uc1swXS50YXJnZXQgKyAnICcgKyBtdXRhdGlvbnNbMF0udHlwZSk7XG5cdFx0XHRkZWJvdWNlKGNoZWNrSUZyYW1lcywxNik7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gY3JlYXRlTXV0YXRpb25PYnNlcnZlcigpe1xuXHRcdFx0dmFyXG5cdFx0XHRcdHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2JvZHknKSxcblxuXHRcdFx0XHRjb25maWcgPSB7XG5cdFx0XHRcdFx0YXR0cmlidXRlcyAgICAgICAgICAgIDogdHJ1ZSxcblx0XHRcdFx0XHRhdHRyaWJ1dGVPbGRWYWx1ZSAgICAgOiBmYWxzZSxcblx0XHRcdFx0XHRjaGFyYWN0ZXJEYXRhICAgICAgICAgOiB0cnVlLFxuXHRcdFx0XHRcdGNoYXJhY3RlckRhdGFPbGRWYWx1ZSA6IGZhbHNlLFxuXHRcdFx0XHRcdGNoaWxkTGlzdCAgICAgICAgICAgICA6IHRydWUsXG5cdFx0XHRcdFx0c3VidHJlZSAgICAgICAgICAgICAgIDogdHJ1ZVxuXHRcdFx0XHR9LFxuXG5cdFx0XHRcdG9ic2VydmVyID0gbmV3IE11dGF0aW9uT2JzZXJ2ZXIobXV0YXRpb25PYnNlcnZlZCk7XG5cblx0XHRcdG9ic2VydmVyLm9ic2VydmUodGFyZ2V0LCBjb25maWcpO1xuXHRcdH1cblxuXHRcdHZhciBNdXRhdGlvbk9ic2VydmVyID0gd2luZG93Lk11dGF0aW9uT2JzZXJ2ZXIgfHwgd2luZG93LldlYktpdE11dGF0aW9uT2JzZXJ2ZXI7XG5cblx0XHRpZiAoTXV0YXRpb25PYnNlcnZlcikgY3JlYXRlTXV0YXRpb25PYnNlcnZlcigpO1xuXHR9XG5cblxuXHRmdW5jdGlvbiByZXNpemVJRnJhbWVzKGV2ZW50KXtcblx0XHRmdW5jdGlvbiByZXNpemUoKXtcblx0XHRcdHNlbmRUcmlnZ2VyTXNnKCdXaW5kb3cgJytldmVudCwncmVzaXplJyk7XG5cdFx0fVxuXG5cdFx0bG9nKCd3aW5kb3cnLCdUcmlnZ2VyIGV2ZW50OiAnK2V2ZW50KTtcblx0XHRkZWJvdWNlKHJlc2l6ZSwxNik7XG5cdH1cblxuXHQvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqLyAgLy9Ob3QgdGVzdGFibGUgaW4gUGhhbnRvbUpTXG5cdGZ1bmN0aW9uIHRhYlZpc2libGUoKSB7XG5cdFx0ZnVuY3Rpb24gcmVzaXplKCl7XG5cdFx0XHRzZW5kVHJpZ2dlck1zZygnVGFiIFZpc2FibGUnLCdyZXNpemUnKTtcblx0XHR9XG5cblx0XHRpZignaGlkZGVuJyAhPT0gZG9jdW1lbnQudmlzaWJpbGl0eVN0YXRlKSB7XG5cdFx0XHRsb2coJ2RvY3VtZW50JywnVHJpZ2dlciBldmVudDogVmlzaWJsaXR5IGNoYW5nZScpO1xuXHRcdFx0ZGVib3VjZShyZXNpemUsMTYpO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHNlbmRUcmlnZ2VyTXNnKGV2ZW50TmFtZSxldmVudCl7XG5cdFx0ZnVuY3Rpb24gaXNJRnJhbWVSZXNpemVFbmFibGVkKGlmcmFtZUlkKSB7XG5cdFx0XHRyZXR1cm5cdCdwYXJlbnQnID09PSBzZXR0aW5nc1tpZnJhbWVJZF0ucmVzaXplRnJvbSAmJlxuXHRcdFx0XHRcdHNldHRpbmdzW2lmcmFtZUlkXS5hdXRvUmVzaXplICYmXG5cdFx0XHRcdFx0IXNldHRpbmdzW2lmcmFtZUlkXS5maXJzdFJ1bjtcblx0XHR9XG5cblx0XHRmb3IgKHZhciBpZnJhbWVJZCBpbiBzZXR0aW5ncyl7XG5cdFx0XHRpZihpc0lGcmFtZVJlc2l6ZUVuYWJsZWQoaWZyYW1lSWQpKXtcblx0XHRcdFx0dHJpZ2dlcihldmVudE5hbWUsZXZlbnQsZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWZyYW1lSWQpLGlmcmFtZUlkKTtcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBzZXR1cEV2ZW50TGlzdGVuZXJzKCl7XG5cdFx0YWRkRXZlbnRMaXN0ZW5lcih3aW5kb3csJ21lc3NhZ2UnLGlGcmFtZUxpc3RlbmVyKTtcblxuXHRcdGFkZEV2ZW50TGlzdGVuZXIod2luZG93LCdyZXNpemUnLCBmdW5jdGlvbigpe3Jlc2l6ZUlGcmFtZXMoJ3Jlc2l6ZScpO30pO1xuXG5cdFx0YWRkRXZlbnRMaXN0ZW5lcihkb2N1bWVudCwndmlzaWJpbGl0eWNoYW5nZScsdGFiVmlzaWJsZSk7XG5cdFx0YWRkRXZlbnRMaXN0ZW5lcihkb2N1bWVudCwnLXdlYmtpdC12aXNpYmlsaXR5Y2hhbmdlJyx0YWJWaXNpYmxlKTsgLy9BbmRyaW9kIDQuNFxuXHRcdGFkZEV2ZW50TGlzdGVuZXIod2luZG93LCdmb2N1c2luJyxmdW5jdGlvbigpe3Jlc2l6ZUlGcmFtZXMoJ2ZvY3VzJyk7fSk7IC8vSUU4LTlcblx0XHRhZGRFdmVudExpc3RlbmVyKHdpbmRvdywnZm9jdXMnLGZ1bmN0aW9uKCl7cmVzaXplSUZyYW1lcygnZm9jdXMnKTt9KTtcblx0fVxuXG5cblx0ZnVuY3Rpb24gZmFjdG9yeSgpe1xuXHRcdGZ1bmN0aW9uIGluaXQob3B0aW9ucyxlbGVtZW50KXtcblx0XHRcdGZ1bmN0aW9uIGNoa1R5cGUoKXtcblx0XHRcdFx0aWYoIWVsZW1lbnQudGFnTmFtZSkge1xuXHRcdFx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ09iamVjdCBpcyBub3QgYSB2YWxpZCBET00gZWxlbWVudCcpO1xuXHRcdFx0XHR9IGVsc2UgaWYgKCdJRlJBTUUnICE9PSBlbGVtZW50LnRhZ05hbWUudG9VcHBlckNhc2UoKSkge1xuXHRcdFx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIDxJRlJBTUU+IHRhZywgZm91bmQgPCcrZWxlbWVudC50YWdOYW1lKyc+Jyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0aWYoZWxlbWVudCkge1xuXHRcdFx0XHRjaGtUeXBlKCk7XG5cdFx0XHRcdHNldHVwSUZyYW1lKGVsZW1lbnQsIG9wdGlvbnMpO1xuXHRcdFx0XHRpRnJhbWVzLnB1c2goZWxlbWVudCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gd2FybkRlcHJlY2F0ZWRPcHRpb25zKG9wdGlvbnMpIHtcblx0XHRcdGlmIChvcHRpb25zICYmIG9wdGlvbnMuZW5hYmxlUHVibGljTWV0aG9kcykge1xuXHRcdFx0XHR3YXJuKCdlbmFibGVQdWJsaWNNZXRob2RzIG9wdGlvbiBoYXMgYmVlbiByZW1vdmVkLCBwdWJsaWMgbWV0aG9kcyBhcmUgbm93IGFsd2F5cyBhdmFpbGFibGUgaW4gdGhlIGlGcmFtZScpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHZhciBpRnJhbWVzO1xuXG5cdFx0c2V0dXBSZXF1ZXN0QW5pbWF0aW9uRnJhbWUoKTtcblx0XHRzZXR1cEV2ZW50TGlzdGVuZXJzKCk7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gaUZyYW1lUmVzaXplRihvcHRpb25zLHRhcmdldCl7XG5cdFx0XHRpRnJhbWVzID0gW107IC8vT25seSByZXR1cm4gaUZyYW1lcyBwYXN0IGluIG9uIHRoaXMgY2FsbFxuXG5cdFx0XHR3YXJuRGVwcmVjYXRlZE9wdGlvbnMob3B0aW9ucyk7XG5cblx0XHRcdHN3aXRjaCAodHlwZW9mKHRhcmdldCkpe1xuXHRcdFx0Y2FzZSAndW5kZWZpbmVkJzpcblx0XHRcdGNhc2UgJ3N0cmluZyc6XG5cdFx0XHRcdEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoXG5cdFx0XHRcdFx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCggdGFyZ2V0IHx8ICdpZnJhbWUnICksXG5cdFx0XHRcdFx0aW5pdC5iaW5kKHVuZGVmaW5lZCwgb3B0aW9ucylcblx0XHRcdFx0KTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICdvYmplY3QnOlxuXHRcdFx0XHRpbml0KG9wdGlvbnMsdGFyZ2V0KTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdVbmV4cGVjdGVkIGRhdGEgdHlwZSAoJyt0eXBlb2YodGFyZ2V0KSsnKScpO1xuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gaUZyYW1lcztcblx0XHR9O1xuXHR9XG5cblx0ZnVuY3Rpb24gY3JlYXRlSlF1ZXJ5UHVibGljTWV0aG9kKCQpe1xuXHRcdGlmICghJC5mbikge1xuXHRcdFx0aW5mbygnJywnVW5hYmxlIHRvIGJpbmQgdG8galF1ZXJ5LCBpdCBpcyBub3QgZnVsbHkgbG9hZGVkLicpO1xuXHRcdH0gZWxzZSBpZiAoISQuZm4uaUZyYW1lUmVzaXplKXtcblx0XHRcdCQuZm4uaUZyYW1lUmVzaXplID0gZnVuY3Rpb24gJGlGcmFtZVJlc2l6ZUYob3B0aW9ucykge1xuXHRcdFx0XHRmdW5jdGlvbiBpbml0KGluZGV4LCBlbGVtZW50KSB7XG5cdFx0XHRcdFx0c2V0dXBJRnJhbWUoZWxlbWVudCwgb3B0aW9ucyk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRyZXR1cm4gdGhpcy5maWx0ZXIoJ2lmcmFtZScpLmVhY2goaW5pdCkuZW5kKCk7XG5cdFx0XHR9O1xuXHRcdH1cblx0fVxuXG5cdGlmICh3aW5kb3cualF1ZXJ5KSB7IGNyZWF0ZUpRdWVyeVB1YmxpY01ldGhvZChqUXVlcnkpOyB9XG5cdFxuXHR3aW5kb3cuaUZyYW1lUmVzaXplID0gd2luZG93LmlGcmFtZVJlc2l6ZSB8fCBmYWN0b3J5KCk7XG5cbn0pKHdpbmRvdyB8fCB7fSk7XG4iXX0=
