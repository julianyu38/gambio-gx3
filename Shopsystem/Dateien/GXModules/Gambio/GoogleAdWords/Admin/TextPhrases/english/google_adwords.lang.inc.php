<?php
/* --------------------------------------------------------------
   google_adwords.lang.inc.php 2017-12-06
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'connect_account_page_title'        => 'Link Google AdWords account',
	'page_title'                        => 'Google AdWords Campaigns',
	'table_head_status'                 => 'Status',
	'table_head_campaign_name'          => 'Campaign',
	'table_head_budget'                 => 'Budget (per day)',
	'table_head_clicks'                 => 'Clicks',
	'table_head_impressions'            => 'Impressions',
	'table_head_ctr'                    => 'CTR',
	'table_head_average_cpc'            => 'CPC Ø',
	'table_head_costs'                  => 'Costs',
	'table_head_daterange_picker_title' => 'Filter by a specific date range',
	'table_foot_total'                  => 'Total',
	'authorization_success'             => 'Link with Google AdWords successful',
	
	'TODAY'               => 'Today',
	'YESTERDAY'           => 'Yesterday',
	'THIS_WEEK_SUN_TODAY' => 'This week (Sun - Today)',
	'THIS_WEEK_MON_TODAY' => 'This week (Mon - Today)',
	'LAST_7_DAYS'         => 'Last 7 days',
	'LAST_WEEK_SUN_SAT'   => 'Last week (Sun - Sat)',
	'LAST_WEEK'           => 'Last week (Mon - Sun)',
	'LAST_BUSINESS_WEEK'  => 'Last working week (Mon - Fri)',
	'LAST_14_DAYS'        => 'Last 14 days',
	'THIS_MONTH'          => 'This month',
	'LAST_30_DAYS'        => 'Last 30 days',
	'LAST_MONTH'          => 'Last month',
	'TOTAL_TIME'          => 'All time'
];