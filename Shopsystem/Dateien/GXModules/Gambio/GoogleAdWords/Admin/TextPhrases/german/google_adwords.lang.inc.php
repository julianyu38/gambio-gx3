<?php
/* --------------------------------------------------------------
   google_adwords.lang.inc.php 2017-12-06
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'connect_account_page_title'        => 'Google AdWords Account verknüpfen',
	'page_title'                        => 'Google AdWords Kampagnen',
	'table_head_status'                 => 'Status',
	'table_head_campaign_name'          => 'Kampagne',
	'table_head_budget'                 => 'Budget (pro Tag)',
	'table_head_clicks'                 => 'Klicks',
	'table_head_impressions'            => 'Impressions',
	'table_head_ctr'                    => 'CTR',
	'table_head_average_cpc'            => 'CPC Ø',
	'table_head_costs'                  => 'Kosten',
	'table_head_daterange_picker_title' => 'Nach einem bestimmten Datumsbereich filtern',
	'table_foot_total'                  => 'Gesamt',
	'authorization_success'             => 'Verknüpfung mit Google AdWords erfolgreich',
	
	'TODAY'               => 'Heute',
	'YESTERDAY'           => 'Gestern',
	'THIS_WEEK_SUN_TODAY' => 'Diese Woche (So-heute)',
	'THIS_WEEK_MON_TODAY' => 'Diese Woche (Mo-heute)',
	'LAST_7_DAYS'         => 'Letzte 7 Tage',
	'LAST_WEEK_SUN_SAT'   => 'Letzte Woche (So-Sa)',
	'LAST_WEEK'           => 'Letzte Woche (Mo-So)',
	'LAST_BUSINESS_WEEK'  => 'Letzte Arbeitswoche(Mo-Fr)',
	'LAST_14_DAYS'        => 'Die letzten 14 Tage',
	'THIS_MONTH'          => 'Diesen Monat',
	'LAST_30_DAYS'        => 'Die letzten 30 Tage',
	'LAST_MONTH'          => 'Letzten Monat',
	'TOTAL_TIME'          => 'Gesamter Zeitraum'
];