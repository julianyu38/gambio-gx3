<?php
/* --------------------------------------------------------------
   GoogleAdWordsController.inc.php 2017-11-27
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GoogleAdWordsController
 *
 * This controller displays the Google AdWords page.
 *
 * @category System
 * @package  AdminHttpViewControllers
 */
class GoogleAdWordsController extends AdminHttpViewController
{
	/**
	 * @var string
	 */
	protected $jsBaseUrl;
	
	/**
	 * @var string
	 */
	protected $stylesBaseUrl;
	
	/**
	 * @var string
	 */
	protected $templatesBaseUrl;
	
	/**
	 * @var \LanguageTextManager
	 */
	protected $languageTextManager;
	
	/**
	 * @var \CI_DB_query_builder
	 */
	protected $db;
	
	/**
	 * @var GoogleConfigurationStorage
	 */
	protected $configurationStorage;
	
	
	/**
	 * Initialize the controller.
	 */
	public function init()
	{
		$this->jsBaseUrl            = '../GXModules/Gambio/GoogleAdWords/Build/Admin/Javascript';
		$this->stylesBaseUrl        = '../GXModules/Gambio/GoogleAdWords/Build/Admin/Styles';
		$this->templatesBaseUrl     = DIR_FS_CATALOG . 'GXModules/Gambio/GoogleAdWords/Admin/Html';
		$this->languageTextManager  = MainFactory::create('LanguageTextManager', 'google_adwords',
		                                                  $_SESSION['languages_id']);
		$this->db                   = StaticGXCoreLoader::getDatabaseQueryBuilder();
		$this->configurationStorage = new GoogleConfigurationStorage($this->db,
		                                                             GoogleConfigurationStorage::SCOPE_GENERAL);
	}
	
	
	/**
	 * Displays the Google AdWords overview page.
	 *
	 * @return \AdminLayoutHttpControllerResponse|bool
	 */
	public function actionDefault()
	{
		$title    = new NonEmptyStringType($this->languageTextManager->get_text('page_title'));
		$template = new ExistingFile(new NonEmptyStringType($this->templatesBaseUrl . '/campaigns_overview.html'));
		
		$accountConnectionData = $this->_accountConnectionData();
		
		$pageData = [
			'campaignsDateRanges' => $this->_campaignsDataRangeSelectionArray(),
			'pageToken'           => $_SESSION['coo_page_token']->generate_token(),
			'authSuccess'         => (string)$this->_getQueryParameter('success') === '1' ? : false,
			'show_account_form'   => $this->configurationStorage->get('connection-status') ? 'false' : 'true',
		];
		
		$data = MainFactory::create('KeyValueCollection', array_merge($pageData, $accountConnectionData));
		
		$assetsArray = [
			MainFactory::create('Asset', $this->stylesBaseUrl . '/campaigns_overview.css'),
			MainFactory::create('Asset', 'google_adwords.lang.inc.php'),
		];
		
		$assets = MainFactory::create('AssetCollection', $assetsArray);
		
		return MainFactory::create('AdminLayoutHttpControllerResponse', $title, $template, $data, $assets);
	}
	
	
	private function _campaignsDataRangeSelectionArray()
	{
		return [
			'TODAY'               => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('TODAY')
			],
			'YESTERDAY'           => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('YESTERDAY')
			],
			'THIS_WEEK_SUN_TODAY' => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('THIS_WEEK_SUN_TODAY')
			],
			'THIS_WEEK_MON_TODAY' => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('THIS_WEEK_MON_TODAY')
			],
			'LAST_7_DAYS'         => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('LAST_7_DAYS')
			],
			'LAST_WEEK_SUN_SAT'   => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('LAST_WEEK_SUN_SAT')
			],
			'LAST_WEEK'           => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('LAST_WEEK')
			],
			'LAST_BUSINESS_WEEK'  => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('LAST_BUSINESS_WEEK')
			],
			'LAST_14_DAYS'        => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('LAST_14_DAYS')
			],
			'THIS_MONTH'          => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('THIS_MONTH')
			],
			'LAST_30_DAYS'        => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('LAST_30_DAYS')
			],
			'LAST_MONTH'          => [
				'selected' => false,
				'value'    => $this->languageTextManager->get_text('LAST_MONTH')
			],
			''                    => [
				'selected' => true,
				'value'    => $this->languageTextManager->get_text('TOTAL_TIME')
			]
		];
	}
	
	
	private function _accountConnectionData()
	{
		return [
			'error'     => (int)$this->_getQueryParameter('error')
		];
	}
}