<?php
/* --------------------------------------------------------------
   AmazonSingleSignonService.inc.php 2017-10-09
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class AmazonSingleSignonService extends AbstractSingleSignonService
{
    protected $mode = 'live';

    protected function getLogger()
    {
        $logger = MainFactory::create('SingleSignonLogger', true, 'amazon');
        return $logger;
    }
    
    
    public function getAuthorizationLink()
	{
		$scope                             = 'profile postal_code payments:widget payments:shipping_address';
		//$authorizationEndpoint             = 'https://www.amazon.com/ap/oa';
        $authorizationEndpoint             = 'https://eu.account.amazon.com/ap/oa';
		$state                             = sha1($this->generateRandomBytes());
		$_SESSION['amazon_oauth_states']   = isset($_SESSION['amazon_oauth_states']) ?
			$_SESSION['amazon_oauth_states'] : [];
		$_SESSION['amazon_oauth_states'][] = $state;
		$_SESSION['amazon_oauth_states']   = array_slice($_SESSION['amazon_oauth_states'], -10);
		$loginParams = [
            'client_id'     => $this->clientId,
            'scope'         => $scope,
            'response_type' => 'code',
            'redirect_uri'  => $this->redirectUri,
            'state'         => $state,
        ];
		if($this->mode === 'sandbox')
		{
		    $loginParams['sandbox'] = true;
        }
		$loginUrl                          = $authorizationEndpoint . '?' . http_build_query($loginParams);
		$this->getLogger()->debug('created authorization URL: {url}', ['url' => $loginUrl]);
		
		return $loginUrl;
	}
	
	
	public function processAuthorizationCode($code)
	{
	    $this->getLogger()->debug('processing authorization code {code}', ['code' => $code]);
		$tokenEndpoint    = 'https://api.amazon.com/auth/o2/token';
		$tokenRequestData = [
			'grant_type'    => 'authorization_code',
			'code'          => $code,
			'redirect_uri'  => $this->redirectUri,
			'client_id'     => $this->clientId,
			'client_secret' => $this->clientSecret,
		];
		$tokenRequest     = MainFactory::create('RestRequest', 'POST', $tokenEndpoint, $tokenRequestData);
		$tokenRestService = MainFactory::create('RestService');
		$tokenResponse    = $tokenRestService->performRequest($tokenRequest);
		$this->getLogger()->debug('token result: {result}', ['result' => (string)$tokenResponse->getResponseCode()]);
		$tokensData       = json_decode($tokenResponse->getResponseBody(), true);
		
		if(!empty($tokensData['error']) || empty($tokensData['access_token']))
		{
			throw new Exception($tokensData['error'] . ' - ' . $tokensData['error_description']);
		}
		
		$profileUrl            = 'https://api.amazon.com/user/profile';
		$profileRequestHeaders = [
			'Authorization: Bearer ' . $tokensData['access_token'],
		];
		$profileRequest        = MainFactory::create('RestRequest', 'GET', $profileUrl, [], $profileRequestHeaders);
		$profileResponse       = $tokenRestService->performRequest($profileRequest);
        $this->getLogger()->debug('profile result: {result}', ['result' => $profileResponse->getResponseCode()]);
		$profileData           = json_decode($profileResponse->getResponseBody(), true);
		
		$addressVerified = true;

		setcookie('amazon_Login_accessToken', $tokensData['access_token'], 0, '/', '', true);
		setcookie('amazon_Login_state_cache', '');
		
		$password = $this->makePassword();
		
		$ssoData = [
			'iss'                 => 'amazon.com',
			'sub'                 => $profileData['user_id'],
			'customer_collection' => [
				'email_address'          => $profileData['email'],
				'email_address_confirm'  => $profileData['email'],
				'email_address_verified' => $addressVerified,
				'password'               => $password,
				'confirmation'           => $password,
			],
			'tokens'              => $tokensData,
			'access_token'        => $tokensData['access_token'],
			'identity'            => $profileData,
		];
		
		return $ssoData;
	}
    
    /**
     * Sets operation mode.
     *
     * Valid modes are 'sandbox' and 'live'. Default is 'live'.
     *
     * @param $mode
     *
     * @throws UnsupportedPaypalModeException
     */
    public function setMode($mode)
    {
        if(!in_array($mode, ['live', 'sandbox'], true))
        {
            throw new UnsupportedPaypalModeException('mode unsupported: ' . $mode);
        }
        $this->mode = $mode;
    }

}
