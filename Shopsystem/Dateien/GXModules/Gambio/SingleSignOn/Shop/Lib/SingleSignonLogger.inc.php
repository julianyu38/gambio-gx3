<?php
/* --------------------------------------------------------------
   SingleSignonLogger.inc.php 2017-10-25
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * PSR-3-inspired logger for SingleSignOn
 *
 * Class SingleSignonLogger
 */
class SingleSignonLogger
{
    /**
     * @var LogControl
     */
    protected $logger;
    
    /**
     * @var string
     */
    protected $subsystem;
    
    const LOG_GROUP = 'widgets';
    const LOG_FILE  = 'singlesignon';
    
    public function __construct($enabled = true, $subsystem = '')
    {
        $this->logger = LogControl::get_instance((bool)$enabled);
        $this->subsystem = $subsystem;
    }
    
    
    /**
     * Logs a debug message
     *
     * @param       $message
     * @param array $context
     */
    public function debug($message, $context = [])
    {
        foreach($context as $placeholder => $value) {
            $message = str_replace('{' . $placeholder . '}', $value, $message);
        }
        $logfile = self::LOG_FILE;
        if(!empty($this->subsystem)) {
            $logfile .= '.' . $this->subsystem;
        }
        $this->logger->notice($message, self::LOG_GROUP, $logfile);
    }
}
