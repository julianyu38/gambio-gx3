<?php
/* --------------------------------------------------------------
   SSOLoginBoxContentView.inc.php 2017-09-26
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class SSOLoginBoxContentView extends SSOLoginBoxContentView_parent
{
	public function prepare_data()
	{
		parent::prepare_data();
		
		$ssoServices     = [];
		$moduleInstalled = (bool)gm_get_conf('MODULE_CENTER_SINGLESIGNON_INSTALLED');
		if($moduleInstalled === true)
		{
			$ssoConfiguration = MainFactory::create('SingleSignonConfigurationStorage');
			$loginUrl         = xtc_href_link('shop.php', 'do=SingleSignOn/Redirect', 'SSL', false, false, false, true,
			                                  true);
			if((bool)$ssoConfiguration->get('services/google/active') === true)
			{
				$ssoServices['googleLoginUrl'] = $loginUrl . '&amp;service=google';
			}
			if((bool)$ssoConfiguration->get('services/facebook/active') === true)
			{
				$ssoServices['facebookLoginUrl'] = $loginUrl . '&amp;service=facebook';
			}
			if((bool)$ssoConfiguration->get('services/paypal/active') === true)
			{
				$ssoServices['paypalLoginUrl'] = $loginUrl . '&amp;service=paypal';
			}
			if((bool)$ssoConfiguration->get('services/amazon/active') === true)
			{
				$ssoServices['amazonLoginUrl'] = $loginUrl . '&amp;service=amazon';
			}
			
			$this->set_content_data('sso', $ssoServices);
		}
	}
}
