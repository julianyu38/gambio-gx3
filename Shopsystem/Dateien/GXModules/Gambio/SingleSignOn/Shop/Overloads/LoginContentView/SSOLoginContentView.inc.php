<?php
/* --------------------------------------------------------------
   SSOLoginContentView.inc.php 2018-02-08
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class SSOLoginContentView extends SSOLoginContentView_parent
{
	public function prepare_data()
	{
		parent::prepare_data();
		
		$ssoData          = [];
		$moduleInstalled = (bool)gm_get_conf('MODULE_CENTER_SINGLESIGNON_INSTALLED');
		if($moduleInstalled === true)
		{
			$ssoConfiguration = MainFactory::create('SingleSignonConfigurationStorage');
			$loginUrl         = xtc_href_link('shop.php', 'do=SingleSignOn/Redirect', 'SSL', false, false, false, true,
			                                  true);
			if((bool)$ssoConfiguration->get('services/google/active') === true)
			{
				$ssoData['googleLoginUrl'] = $loginUrl . '&amp;service=google';
			}
			if((bool)$ssoConfiguration->get('services/facebook/active') === true)
			{
				$ssoData['facebookLoginUrl'] = $loginUrl . '&amp;service=facebook';
			}
			if((bool)$ssoConfiguration->get('services/paypal/active') === true)
			{
				$ssoData['paypalLoginUrl'] = $loginUrl . '&amp;service=paypal';
			}
			if((bool)$ssoConfiguration->get('services/amazon/active') === true)
			{
				$ssoData['amazonLoginUrl'] = $loginUrl . '&amp;service=amazon';
			}
		}
		$this->set_content_data('ssoData', $ssoData);
	}
}
