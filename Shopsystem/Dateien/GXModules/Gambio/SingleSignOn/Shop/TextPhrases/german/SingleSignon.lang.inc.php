<?php
/* --------------------------------------------------------------
   SingleSignon.lang.inc.php 2017-10-30
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

// GERMAN

$t_language_text_section_content_array = [
    'connected'            => 'verbunden',
    'connected_logins'     => 'Verbundene Login-Dienste',
    'disconnect'           => 'Verbindung trennen',
    'loading_address_book' => 'Ihr Amazon-Adressbuch wird geladen.',
    'login_heading'        => 'Login mit …',
    'login_with_amazon'    => 'Amazon',
    'login_with_facebook'  => 'Facebook',
    'login_with_google'    => 'Google',
    'login_with_paypal'    => 'PayPal',
    'not_connected'        => 'nicht verbunden',
    'note_sso'             => 'Nutzen Sie ein bestehendes Konto bei einem der folgenden Anbieter, um sich im Shop zu registrieren und anzumelden.',
    'choose_address_title' => 'Möchten Sie eine Adresse aus Ihrem Amazon-Konto übernehmen?',
    'unsupported_country'  => 'Eine Adresse in dem gewählten Land kann leider nicht verwendet werden.',
];
