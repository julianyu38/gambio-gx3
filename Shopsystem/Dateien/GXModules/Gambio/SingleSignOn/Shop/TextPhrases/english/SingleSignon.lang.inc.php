<?php
/* --------------------------------------------------------------
   SingleSignon.lang.inc.php 2017-10-30
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

// ENGLISH

$t_language_text_section_content_array = [
    'connected'            => 'connected',
    'connected_logins'     => 'Connected Login Services',
    'disconnect'           => 'disconnect',
    'loading_address_book' => 'Loading Amazon address book …',
    'login_with_amazon'    => 'Login with Amazon',
    'login_with_facebook'  => 'Login with Facebook',
    'login_with_google'    => 'Login with Google',
    'login_with_paypal'    => 'Login with PayPal',
    'not_connected'        => 'not connected',
    'note_sso'             => '',
    'choose_address_title' => 'Would you like to use an address from your Amazon address book?',
    'unsupported_country'  => 'This shop does not support delivery to the country selected.',
];
