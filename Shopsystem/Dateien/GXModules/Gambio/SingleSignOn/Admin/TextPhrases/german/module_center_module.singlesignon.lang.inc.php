<?php
/* --------------------------------------------------------------
   module_center_module.singlesignon.lang.inc.php 2017-10-05
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/* GERMAN */

$t_language_text_section_content_array = [
	'singlesignon_description'    => 'Single Sign-on-Dienste erlauben Ihnen und ihren Kunden sich mit ihren Amazon-, PayPal-, Google- und Facebook-Konten bequem in den Shop einzuloggen.',
	'singlesignon_title'          => 'Single Sign-on',
	'sso_client_id'               => 'Client-ID',
	'sso_client_id_sandbox'       => 'Client-ID (Sandbox)',
	'sso_client_secret'           => 'Client-Schlüssel',
	'sso_client_secret_sandbox'   => 'Client-Schlüssel (Sandbox)',
	'sso_configuration_saved'     => 'Einstellungen gespeichert',
	'sso_javascript_origin'       => 'Javascript Origin',
	'sso_login_with_amazon'       => 'Einloggen mit Amazon',
	'sso_login_with_facebook'     => 'Einloggen mit Facebook',
	'sso_login_with_google'       => 'Einloggen mit Google',
	'sso_login_with_paypal'       => 'Einloggen mit Paypal',
	'sso_redirect_uri'            => 'Weiterleitungs-URL',
	'sso_save_configuration'      => 'Einstellungen speichern',
	'sso_use_amazon_sandbox'      => 'Sandbox-Modus verwenden',
	'sso_use_login_with_amazon'   => 'Einloggen mit Amazon erlauben',
	'sso_use_login_with_facebook' => 'Einloggen mit Facebook erlauben',
	'sso_use_login_with_google'   => 'Einloggen mit Google erlauben',
	'sso_use_login_with_paypal'   => 'Einloggen mit Paypal erlauben',
	'sso_use_paypal_sandbox'      => 'Sandbox-Modus verwenden',
];

