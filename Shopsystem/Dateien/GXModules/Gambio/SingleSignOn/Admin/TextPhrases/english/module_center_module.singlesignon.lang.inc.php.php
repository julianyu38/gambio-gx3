<?php
/* --------------------------------------------------------------
   module_center_module.singlesignon.lang.inc.php 2017-10-05
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/* ENGLISH */

$t_language_text_section_content_array = [
	'singlesignon_description'    => 'Single sign-on services allow you and your customers to easily log into the shop with their Amazon, PayPal, Google and Facebook accounts.',
	'singlesignon_title'          => 'Single Sign-on',
	'sso_client_id'               => 'Client ID',
	'sso_client_id_sandbox'       => 'Client ID (Sandbox)',
	'sso_client_secret'           => 'Client Key',
	'sso_client_secret_sandbox'   => 'Client Key (Sandbox)',
	'sso_configuration_saved'     => 'Configuration saved',
	'sso_javascript_origin'       => 'Javascript Origin',
	'sso_login_with_amazon'       => 'Einloggen mit Amazon',
	'sso_login_with_facebook'     => 'Log in with Facebook',
	'sso_login_with_google'       => 'Log in with Google',
	'sso_login_with_paypal'       => 'Log in with Paypal',
	'sso_redirect_uri'            => 'Redirect URI',
	'sso_save_configuration'      => 'Save configuration',
	'sso_use_amazon_sandbox'      => 'Use sandbox mode',
	'sso_use_login_with_amazon'   => 'Allow log-in with Amazon',
	'sso_use_login_with_facebook' => 'Allow log-in with Facebook',
	'sso_use_login_with_google'   => 'Allow log-in with Google',
	'sso_use_login_with_paypal'   => 'Allow log-in with Paypal',
	'sso_use_paypal_sandbox'      => 'Use sandbox mode',
];

