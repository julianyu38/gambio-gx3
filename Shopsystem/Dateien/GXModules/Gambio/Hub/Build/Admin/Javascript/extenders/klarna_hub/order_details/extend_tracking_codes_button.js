'use strict';

/* --------------------------------------------------------------
 extend_tracking_codes_button.js 2017-11-02
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Extends the order status modal with a "notify-klarna" checkbox.
 *
 * Notice: This method will use jQuery UI modal for the prompt.
 */
(function () {
	'use strict';

	/**
  * Initializes the module.
  *
  * @private
  */

	var init = function init() {
		// Tracking code selectors.
		var $trackingCodeText = void 0;
		var $shippingCompanySelect = void 0;

		// Create prompt dialog (will be initially hidden).
		var $dialog = $('<div/>', {
			'class': 'klarna-hub dialog shipment-tracking hidden',
			'html': [$('<p/>', {
				'text': KlarnaHub.Config.lang.TRANSMIT_SHIPMENT_NUMBER_TO_KLARNA
			})]
		});

		// Bind button click event handler.
		var bindTrackingCodesButtonEvent = function bindTrackingCodesButtonEvent() {
			$shippingCompanySelect = $('#parcel_services_dropdown');
			$trackingCodeText = $('#parcel_service_tracking_code');

			var $addTrackingCodeButton = $('span.add_tracking_code');

			$addTrackingCodeButton.on('click', function () {
				if ($shippingCompanySelect.val() === '') {
					return;
				}

				if ($trackingCodeText.val() === '') {
					return;
				}

				$dialog.dialog('open');
			});
		};

		var buttons = [{
			text: KlarnaHub.Config.lang.NO,
			click: function click() {
				return $dialog.dialog('close');
			}
		}, {
			text: KlarnaHub.Config.lang.NOTIFY_KLARNA,
			click: function click() {
				var shippingCompany = $shippingCompanySelect.find('option:selected').text();
				var trackingNumber = $trackingCodeText.val();

				KlarnaHub.Api.executeAddTrackingCode(shippingCompany, trackingNumber).then(function () {
					return location.reload();
				});
			}
		}];

		$dialog.dialog({
			autoOpen: false,
			width: 500,
			height: 'auto',
			resizable: false,
			modal: true,
			title: KlarnaHub.Config.lang.SHIPMENT_TRACKING,
			dialogClass: 'gx-container',
			close: bindTrackingCodesButtonEvent,
			buttons: buttons
		});

		$dialog.parent().find('.ui-dialog-buttonpane button:last-child').addClass('btn-primary');

		bindTrackingCodesButtonEvent();
	};

	KlarnaHub.on('ready', function () {
		return init();
	});
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZGV0YWlscy9leHRlbmRfdHJhY2tpbmdfY29kZXNfYnV0dG9uLmpzIl0sIm5hbWVzIjpbImluaXQiLCIkdHJhY2tpbmdDb2RlVGV4dCIsIiRzaGlwcGluZ0NvbXBhbnlTZWxlY3QiLCIkZGlhbG9nIiwiJCIsIktsYXJuYUh1YiIsIkNvbmZpZyIsImxhbmciLCJUUkFOU01JVF9TSElQTUVOVF9OVU1CRVJfVE9fS0xBUk5BIiwiYmluZFRyYWNraW5nQ29kZXNCdXR0b25FdmVudCIsIiRhZGRUcmFja2luZ0NvZGVCdXR0b24iLCJvbiIsInZhbCIsImRpYWxvZyIsImJ1dHRvbnMiLCJ0ZXh0IiwiTk8iLCJjbGljayIsIk5PVElGWV9LTEFSTkEiLCJzaGlwcGluZ0NvbXBhbnkiLCJmaW5kIiwidHJhY2tpbmdOdW1iZXIiLCJBcGkiLCJleGVjdXRlQWRkVHJhY2tpbmdDb2RlIiwidGhlbiIsImxvY2F0aW9uIiwicmVsb2FkIiwiYXV0b09wZW4iLCJ3aWR0aCIsImhlaWdodCIsInJlc2l6YWJsZSIsIm1vZGFsIiwidGl0bGUiLCJTSElQTUVOVF9UUkFDS0lORyIsImRpYWxvZ0NsYXNzIiwiY2xvc2UiLCJwYXJlbnQiLCJhZGRDbGFzcyJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7OztBQUtBLENBQUMsWUFBVztBQUNYOztBQUVBOzs7Ozs7QUFLQSxLQUFNQSxPQUFPLFNBQVBBLElBQU8sR0FBTTtBQUNsQjtBQUNBLE1BQUlDLDBCQUFKO0FBQ0EsTUFBSUMsK0JBQUo7O0FBRUE7QUFDQSxNQUFNQyxVQUFVQyxFQUFFLFFBQUYsRUFBWTtBQUMzQixZQUFTLDRDQURrQjtBQUUzQixXQUFRLENBQ1BBLEVBQUUsTUFBRixFQUFVO0FBQ1QsWUFBUUMsVUFBVUMsTUFBVixDQUFpQkMsSUFBakIsQ0FBc0JDO0FBRHJCLElBQVYsQ0FETztBQUZtQixHQUFaLENBQWhCOztBQVNBO0FBQ0EsTUFBTUMsK0JBQStCLFNBQS9CQSw0QkFBK0IsR0FBTTtBQUMxQ1AsNEJBQXlCRSxFQUFFLDJCQUFGLENBQXpCO0FBQ0FILHVCQUFvQkcsRUFBRSwrQkFBRixDQUFwQjs7QUFFQSxPQUFNTSx5QkFBeUJOLEVBQUUsd0JBQUYsQ0FBL0I7O0FBRUFNLDBCQUF1QkMsRUFBdkIsQ0FBMEIsT0FBMUIsRUFBbUMsWUFBTTtBQUN4QyxRQUFJVCx1QkFBdUJVLEdBQXZCLE9BQWlDLEVBQXJDLEVBQXlDO0FBQ3hDO0FBQ0E7O0FBRUQsUUFBSVgsa0JBQWtCVyxHQUFsQixPQUE0QixFQUFoQyxFQUFvQztBQUNuQztBQUNBOztBQUVEVCxZQUFRVSxNQUFSLENBQWUsTUFBZjtBQUNBLElBVkQ7QUFXQSxHQWpCRDs7QUFtQkEsTUFBTUMsVUFBVSxDQUNmO0FBQ0NDLFNBQU1WLFVBQVVDLE1BQVYsQ0FBaUJDLElBQWpCLENBQXNCUyxFQUQ3QjtBQUVDQyxVQUFPO0FBQUEsV0FBTWQsUUFBUVUsTUFBUixDQUFlLE9BQWYsQ0FBTjtBQUFBO0FBRlIsR0FEZSxFQUtmO0FBQ0NFLFNBQU1WLFVBQVVDLE1BQVYsQ0FBaUJDLElBQWpCLENBQXNCVyxhQUQ3QjtBQUVDRCxVQUFPLGlCQUFNO0FBQ1osUUFBTUUsa0JBQWtCakIsdUJBQXVCa0IsSUFBdkIsQ0FBNEIsaUJBQTVCLEVBQStDTCxJQUEvQyxFQUF4QjtBQUNBLFFBQU1NLGlCQUFpQnBCLGtCQUFrQlcsR0FBbEIsRUFBdkI7O0FBRUFQLGNBQVVpQixHQUFWLENBQWNDLHNCQUFkLENBQXFDSixlQUFyQyxFQUFzREUsY0FBdEQsRUFDRUcsSUFERixDQUNPO0FBQUEsWUFBTUMsU0FBU0MsTUFBVCxFQUFOO0FBQUEsS0FEUDtBQUVBO0FBUkYsR0FMZSxDQUFoQjs7QUFpQkF2QixVQUFRVSxNQUFSLENBQWU7QUFDZGMsYUFBVSxLQURJO0FBRWRDLFVBQU8sR0FGTztBQUdkQyxXQUFRLE1BSE07QUFJZEMsY0FBVyxLQUpHO0FBS2RDLFVBQU8sSUFMTztBQU1kQyxVQUFPM0IsVUFBVUMsTUFBVixDQUFpQkMsSUFBakIsQ0FBc0IwQixpQkFOZjtBQU9kQyxnQkFBYSxjQVBDO0FBUWRDLFVBQU8xQiw0QkFSTztBQVNkSztBQVRjLEdBQWY7O0FBWUFYLFVBQVFpQyxNQUFSLEdBQWlCaEIsSUFBakIsQ0FBc0IseUNBQXRCLEVBQWlFaUIsUUFBakUsQ0FBMEUsYUFBMUU7O0FBRUE1QjtBQUNBLEVBbkVEOztBQXFFQUosV0FBVU0sRUFBVixDQUFhLE9BQWIsRUFBc0I7QUFBQSxTQUFNWCxNQUFOO0FBQUEsRUFBdEI7QUFDQSxDQTlFRCIsImZpbGUiOiJBZG1pbi9KYXZhc2NyaXB0L2V4dGVuZGVycy9rbGFybmFfaHViL29yZGVyX2RldGFpbHMvZXh0ZW5kX3RyYWNraW5nX2NvZGVzX2J1dHRvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBleHRlbmRfdHJhY2tpbmdfY29kZXNfYnV0dG9uLmpzIDIwMTctMTEtMDJcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogRXh0ZW5kcyB0aGUgb3JkZXIgc3RhdHVzIG1vZGFsIHdpdGggYSBcIm5vdGlmeS1rbGFybmFcIiBjaGVja2JveC5cclxuICpcclxuICogTm90aWNlOiBUaGlzIG1ldGhvZCB3aWxsIHVzZSBqUXVlcnkgVUkgbW9kYWwgZm9yIHRoZSBwcm9tcHQuXHJcbiAqL1xyXG4oZnVuY3Rpb24oKSB7XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEluaXRpYWxpemVzIHRoZSBtb2R1bGUuXHJcblx0ICpcclxuXHQgKiBAcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdGNvbnN0IGluaXQgPSAoKSA9PiB7XHJcblx0XHQvLyBUcmFja2luZyBjb2RlIHNlbGVjdG9ycy5cclxuXHRcdGxldCAkdHJhY2tpbmdDb2RlVGV4dDtcclxuXHRcdGxldCAkc2hpcHBpbmdDb21wYW55U2VsZWN0O1xyXG5cdFx0XHJcblx0XHQvLyBDcmVhdGUgcHJvbXB0IGRpYWxvZyAod2lsbCBiZSBpbml0aWFsbHkgaGlkZGVuKS5cclxuXHRcdGNvbnN0ICRkaWFsb2cgPSAkKCc8ZGl2Lz4nLCB7XHJcblx0XHRcdCdjbGFzcyc6ICdrbGFybmEtaHViIGRpYWxvZyBzaGlwbWVudC10cmFja2luZyBoaWRkZW4nLFxyXG5cdFx0XHQnaHRtbCc6IFtcclxuXHRcdFx0XHQkKCc8cC8+Jywge1xyXG5cdFx0XHRcdFx0J3RleHQnOiBLbGFybmFIdWIuQ29uZmlnLmxhbmcuVFJBTlNNSVRfU0hJUE1FTlRfTlVNQkVSX1RPX0tMQVJOQVxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdF1cclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHQvLyBCaW5kIGJ1dHRvbiBjbGljayBldmVudCBoYW5kbGVyLlxyXG5cdFx0Y29uc3QgYmluZFRyYWNraW5nQ29kZXNCdXR0b25FdmVudCA9ICgpID0+IHtcclxuXHRcdFx0JHNoaXBwaW5nQ29tcGFueVNlbGVjdCA9ICQoJyNwYXJjZWxfc2VydmljZXNfZHJvcGRvd24nKTtcclxuXHRcdFx0JHRyYWNraW5nQ29kZVRleHQgPSAkKCcjcGFyY2VsX3NlcnZpY2VfdHJhY2tpbmdfY29kZScpO1xyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3QgJGFkZFRyYWNraW5nQ29kZUJ1dHRvbiA9ICQoJ3NwYW4uYWRkX3RyYWNraW5nX2NvZGUnKTtcclxuXHRcdFx0XHJcblx0XHRcdCRhZGRUcmFja2luZ0NvZGVCdXR0b24ub24oJ2NsaWNrJywgKCkgPT4ge1xyXG5cdFx0XHRcdGlmICgkc2hpcHBpbmdDb21wYW55U2VsZWN0LnZhbCgpID09PSAnJykge1xyXG5cdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoJHRyYWNraW5nQ29kZVRleHQudmFsKCkgPT09ICcnKSB7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdCRkaWFsb2cuZGlhbG9nKCdvcGVuJyk7XHJcblx0XHRcdH0pO1xyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgYnV0dG9ucyA9IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRleHQ6IEtsYXJuYUh1Yi5Db25maWcubGFuZy5OTyxcclxuXHRcdFx0XHRjbGljazogKCkgPT4gJGRpYWxvZy5kaWFsb2coJ2Nsb3NlJylcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRleHQ6IEtsYXJuYUh1Yi5Db25maWcubGFuZy5OT1RJRllfS0xBUk5BLFxyXG5cdFx0XHRcdGNsaWNrOiAoKSA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCBzaGlwcGluZ0NvbXBhbnkgPSAkc2hpcHBpbmdDb21wYW55U2VsZWN0LmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpLnRleHQoKTtcclxuXHRcdFx0XHRcdGNvbnN0IHRyYWNraW5nTnVtYmVyID0gJHRyYWNraW5nQ29kZVRleHQudmFsKCk7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdEtsYXJuYUh1Yi5BcGkuZXhlY3V0ZUFkZFRyYWNraW5nQ29kZShzaGlwcGluZ0NvbXBhbnksIHRyYWNraW5nTnVtYmVyKVxyXG5cdFx0XHRcdFx0XHQudGhlbigoKSA9PiBsb2NhdGlvbi5yZWxvYWQoKSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRdO1xyXG5cdFx0XHJcblx0XHQkZGlhbG9nLmRpYWxvZyh7XHJcblx0XHRcdGF1dG9PcGVuOiBmYWxzZSxcclxuXHRcdFx0d2lkdGg6IDUwMCxcclxuXHRcdFx0aGVpZ2h0OiAnYXV0bycsXHJcblx0XHRcdHJlc2l6YWJsZTogZmFsc2UsXHJcblx0XHRcdG1vZGFsOiB0cnVlLFxyXG5cdFx0XHR0aXRsZTogS2xhcm5hSHViLkNvbmZpZy5sYW5nLlNISVBNRU5UX1RSQUNLSU5HLFxyXG5cdFx0XHRkaWFsb2dDbGFzczogJ2d4LWNvbnRhaW5lcicsXHJcblx0XHRcdGNsb3NlOiBiaW5kVHJhY2tpbmdDb2Rlc0J1dHRvbkV2ZW50LFxyXG5cdFx0XHRidXR0b25zXHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0JGRpYWxvZy5wYXJlbnQoKS5maW5kKCcudWktZGlhbG9nLWJ1dHRvbnBhbmUgYnV0dG9uOmxhc3QtY2hpbGQnKS5hZGRDbGFzcygnYnRuLXByaW1hcnknKTtcclxuXHRcdFxyXG5cdFx0YmluZFRyYWNraW5nQ29kZXNCdXR0b25FdmVudCgpXHJcblx0fTtcclxuXHRcclxuXHRLbGFybmFIdWIub24oJ3JlYWR5JywgKCkgPT4gaW5pdCgpKTtcclxufSkoKTsgIl19
