'use strict';

/* --------------------------------------------------------------
 extend_cancel_order_action.js 2017-11-03
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Extends cancel-order row action (will call the respective KlarnaHub callback).
 */
(function () {
	'use strict';

	/**
  * Initializes the module.
  *
  * @private
  */

	var init = function init() {
		var $form = $('#multi_cancel_confirm_form');

		var $sourceControlGroup = $form.find('.single-checkbox:first').closest('.control-group');

		var $controlGroup = $sourceControlGroup.clone(true);

		var $label = $controlGroup.find('label');
		$label.text(KlarnaHub.Config.lang.NOTIFY_KLARNA);

		var $singleCheckbox = $controlGroup.find('.single-checkbox');
		var $checkbox = $controlGroup.find('input:checkbox');
		$checkbox.addClass('notify-klarna');

		$controlGroup.insertBefore($sourceControlGroup.next());

		$form.on('submit', function (event) {
			event.preventDefault();

			if (!$checkbox.prop('checked')) {
				$form[0].submit();
				return;
			}

			KlarnaHub.Api.executeCancelOrder().then(function () {
				return $form[0].submit();
			});
		});

		$('.js-button-dropdown .cancel-order').on('click', function () {
			$checkbox.prop('checked', true);
			$singleCheckbox.addClass('checked');
		});
	};

	KlarnaHub.on('ready', function () {
		return init();
	});
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZGV0YWlscy9leHRlbmRfY2FuY2VsX29yZGVyX2FjdGlvbi5qcyJdLCJuYW1lcyI6WyJpbml0IiwiJGZvcm0iLCIkIiwiJHNvdXJjZUNvbnRyb2xHcm91cCIsImZpbmQiLCJjbG9zZXN0IiwiJGNvbnRyb2xHcm91cCIsImNsb25lIiwiJGxhYmVsIiwidGV4dCIsIktsYXJuYUh1YiIsIkNvbmZpZyIsImxhbmciLCJOT1RJRllfS0xBUk5BIiwiJHNpbmdsZUNoZWNrYm94IiwiJGNoZWNrYm94IiwiYWRkQ2xhc3MiLCJpbnNlcnRCZWZvcmUiLCJuZXh0Iiwib24iLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwicHJvcCIsInN1Ym1pdCIsIkFwaSIsImV4ZWN1dGVDYW5jZWxPcmRlciIsInRoZW4iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7O0FBR0EsQ0FBQyxZQUFXO0FBQ1g7O0FBRUE7Ozs7OztBQUtBLEtBQU1BLE9BQU8sU0FBUEEsSUFBTyxHQUFNO0FBQ2xCLE1BQU1DLFFBQVFDLEVBQUUsNEJBQUYsQ0FBZDs7QUFFQSxNQUFNQyxzQkFBc0JGLE1BQU1HLElBQU4sQ0FBVyx3QkFBWCxFQUFxQ0MsT0FBckMsQ0FBNkMsZ0JBQTdDLENBQTVCOztBQUVBLE1BQU1DLGdCQUFnQkgsb0JBQW9CSSxLQUFwQixDQUEwQixJQUExQixDQUF0Qjs7QUFFQSxNQUFNQyxTQUFTRixjQUFjRixJQUFkLENBQW1CLE9BQW5CLENBQWY7QUFDQUksU0FBT0MsSUFBUCxDQUFZQyxVQUFVQyxNQUFWLENBQWlCQyxJQUFqQixDQUFzQkMsYUFBbEM7O0FBRUEsTUFBTUMsa0JBQWtCUixjQUFjRixJQUFkLENBQW1CLGtCQUFuQixDQUF4QjtBQUNBLE1BQU1XLFlBQVlULGNBQWNGLElBQWQsQ0FBbUIsZ0JBQW5CLENBQWxCO0FBQ0FXLFlBQVVDLFFBQVYsQ0FBbUIsZUFBbkI7O0FBRUFWLGdCQUFjVyxZQUFkLENBQTJCZCxvQkFBb0JlLElBQXBCLEVBQTNCOztBQUVBakIsUUFBTWtCLEVBQU4sQ0FBUyxRQUFULEVBQW1CLFVBQUNDLEtBQUQsRUFBVztBQUM3QkEsU0FBTUMsY0FBTjs7QUFFQSxPQUFJLENBQUNOLFVBQVVPLElBQVYsQ0FBZSxTQUFmLENBQUwsRUFBZ0M7QUFDL0JyQixVQUFNLENBQU4sRUFBU3NCLE1BQVQ7QUFDQTtBQUNBOztBQUVEYixhQUFVYyxHQUFWLENBQWNDLGtCQUFkLEdBQW1DQyxJQUFuQyxDQUF3QztBQUFBLFdBQU16QixNQUFNLENBQU4sRUFBU3NCLE1BQVQsRUFBTjtBQUFBLElBQXhDO0FBQ0EsR0FURDs7QUFXQXJCLElBQUUsbUNBQUYsRUFBdUNpQixFQUF2QyxDQUEwQyxPQUExQyxFQUFtRCxZQUFNO0FBQ3hESixhQUFVTyxJQUFWLENBQWUsU0FBZixFQUEwQixJQUExQjtBQUNBUixtQkFBZ0JFLFFBQWhCLENBQXlCLFNBQXpCO0FBQ0EsR0FIRDtBQUlBLEVBL0JEOztBQWlDQU4sV0FBVVMsRUFBVixDQUFhLE9BQWIsRUFBc0I7QUFBQSxTQUFNbkIsTUFBTjtBQUFBLEVBQXRCO0FBQ0EsQ0ExQ0QiLCJmaWxlIjoiQWRtaW4vSmF2YXNjcmlwdC9leHRlbmRlcnMva2xhcm5hX2h1Yi9vcmRlcl9kZXRhaWxzL2V4dGVuZF9jYW5jZWxfb3JkZXJfYWN0aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGV4dGVuZF9jYW5jZWxfb3JkZXJfYWN0aW9uLmpzIDIwMTctMTEtMDNcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogRXh0ZW5kcyBjYW5jZWwtb3JkZXIgcm93IGFjdGlvbiAod2lsbCBjYWxsIHRoZSByZXNwZWN0aXZlIEtsYXJuYUh1YiBjYWxsYmFjaykuXHJcbiAqL1xyXG4oZnVuY3Rpb24oKSB7XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEluaXRpYWxpemVzIHRoZSBtb2R1bGUuXHJcblx0ICpcclxuXHQgKiBAcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdGNvbnN0IGluaXQgPSAoKSA9PiB7XHJcblx0XHRjb25zdCAkZm9ybSA9ICQoJyNtdWx0aV9jYW5jZWxfY29uZmlybV9mb3JtJyk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRzb3VyY2VDb250cm9sR3JvdXAgPSAkZm9ybS5maW5kKCcuc2luZ2xlLWNoZWNrYm94OmZpcnN0JykuY2xvc2VzdCgnLmNvbnRyb2wtZ3JvdXAnKTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgJGNvbnRyb2xHcm91cCA9ICRzb3VyY2VDb250cm9sR3JvdXAuY2xvbmUodHJ1ZSk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRsYWJlbCA9ICRjb250cm9sR3JvdXAuZmluZCgnbGFiZWwnKTtcclxuXHRcdCRsYWJlbC50ZXh0KEtsYXJuYUh1Yi5Db25maWcubGFuZy5OT1RJRllfS0xBUk5BKTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgJHNpbmdsZUNoZWNrYm94ID0gJGNvbnRyb2xHcm91cC5maW5kKCcuc2luZ2xlLWNoZWNrYm94Jyk7XHJcblx0XHRjb25zdCAkY2hlY2tib3ggPSAkY29udHJvbEdyb3VwLmZpbmQoJ2lucHV0OmNoZWNrYm94Jyk7XHJcblx0XHQkY2hlY2tib3guYWRkQ2xhc3MoJ25vdGlmeS1rbGFybmEnKTtcclxuXHRcdFxyXG5cdFx0JGNvbnRyb2xHcm91cC5pbnNlcnRCZWZvcmUoJHNvdXJjZUNvbnRyb2xHcm91cC5uZXh0KCkpO1xyXG5cdFx0XHJcblx0XHQkZm9ybS5vbignc3VibWl0JywgKGV2ZW50KSA9PiB7XHJcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoISRjaGVja2JveC5wcm9wKCdjaGVja2VkJykpIHtcclxuXHRcdFx0XHQkZm9ybVswXS5zdWJtaXQoKTsgXHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRLbGFybmFIdWIuQXBpLmV4ZWN1dGVDYW5jZWxPcmRlcigpLnRoZW4oKCkgPT4gJGZvcm1bMF0uc3VibWl0KCkpO1xyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdCQoJy5qcy1idXR0b24tZHJvcGRvd24gLmNhbmNlbC1vcmRlcicpLm9uKCdjbGljaycsICgpID0+IHtcclxuXHRcdFx0JGNoZWNrYm94LnByb3AoJ2NoZWNrZWQnLCB0cnVlKTsgXHJcblx0XHRcdCRzaW5nbGVDaGVja2JveC5hZGRDbGFzcygnY2hlY2tlZCcpO1xyXG5cdFx0fSk7IFxyXG5cdH07XHJcblx0XHJcblx0S2xhcm5hSHViLm9uKCdyZWFkeScsICgpID0+IGluaXQoKSk7XHJcbn0pKCk7IFxyXG4iXX0=
