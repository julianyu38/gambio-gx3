'use strict';

/* --------------------------------------------------------------
 disable_other_order_edit_buttons.js 2017-11-03
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Disables the rest order edit buttons (address and payment, shipping and sums).
 */
(function () {
	'use strict';

	/**
  * Initializes the module.
  *
  * @private
  */

	var init = function init() {
		var $links = $('.page-nav-tabs a');

		$links.each(function (index, link) {
			$(link).css({
				'opacity': 0.5,
				'background': '#FFF'
			}).attr('href', '#').off('click');
		});
	};

	KlarnaHub.on('ready', function () {
		return init();
	});
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZWRpdC9kaXNhYmxlX290aGVyX29yZGVyX2VkaXRfYnV0dG9ucy5qcyJdLCJuYW1lcyI6WyJpbml0IiwiJGxpbmtzIiwiJCIsImVhY2giLCJpbmRleCIsImxpbmsiLCJjc3MiLCJhdHRyIiwib2ZmIiwiS2xhcm5hSHViIiwib24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7O0FBR0EsQ0FBQyxZQUFXO0FBQ1g7O0FBRUE7Ozs7OztBQUtBLEtBQU1BLE9BQU8sU0FBUEEsSUFBTyxHQUFNO0FBQ2xCLE1BQU1DLFNBQVNDLEVBQUUsa0JBQUYsQ0FBZjs7QUFFQUQsU0FBT0UsSUFBUCxDQUFZLFVBQUNDLEtBQUQsRUFBUUMsSUFBUixFQUFpQjtBQUM1QkgsS0FBRUcsSUFBRixFQUNFQyxHQURGLENBQ007QUFDSixlQUFXLEdBRFA7QUFFSixrQkFBYztBQUZWLElBRE4sRUFLRUMsSUFMRixDQUtPLE1BTFAsRUFLZSxHQUxmLEVBTUVDLEdBTkYsQ0FNTSxPQU5OO0FBT0EsR0FSRDtBQVNBLEVBWkQ7O0FBY0FDLFdBQVVDLEVBQVYsQ0FBYSxPQUFiLEVBQXNCO0FBQUEsU0FBTVYsTUFBTjtBQUFBLEVBQXRCO0FBQ0EsQ0F2QkQiLCJmaWxlIjoiQWRtaW4vSmF2YXNjcmlwdC9leHRlbmRlcnMva2xhcm5hX2h1Yi9vcmRlcl9lZGl0L2Rpc2FibGVfb3RoZXJfb3JkZXJfZWRpdF9idXR0b25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGRpc2FibGVfb3RoZXJfb3JkZXJfZWRpdF9idXR0b25zLmpzIDIwMTctMTEtMDNcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogRGlzYWJsZXMgdGhlIHJlc3Qgb3JkZXIgZWRpdCBidXR0b25zIChhZGRyZXNzIGFuZCBwYXltZW50LCBzaGlwcGluZyBhbmQgc3VtcykuXHJcbiAqL1xyXG4oZnVuY3Rpb24oKSB7XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEluaXRpYWxpemVzIHRoZSBtb2R1bGUuXHJcblx0ICpcclxuXHQgKiBAcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdGNvbnN0IGluaXQgPSAoKSA9PiB7XHJcblx0XHRjb25zdCAkbGlua3MgPSAkKCcucGFnZS1uYXYtdGFicyBhJyk7XHJcblx0XHRcclxuXHRcdCRsaW5rcy5lYWNoKChpbmRleCwgbGluaykgPT4ge1xyXG5cdFx0XHQkKGxpbmspXHJcblx0XHRcdFx0LmNzcyh7XHJcblx0XHRcdFx0XHQnb3BhY2l0eSc6IDAuNSxcclxuXHRcdFx0XHRcdCdiYWNrZ3JvdW5kJzogJyNGRkYnLFxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LmF0dHIoJ2hyZWYnLCAnIycpXHJcblx0XHRcdFx0Lm9mZignY2xpY2snKTtcclxuXHRcdH0pO1xyXG5cdH07XHJcblx0XHJcblx0S2xhcm5hSHViLm9uKCdyZWFkeScsICgpID0+IGluaXQoKSk7XHJcbn0pKCk7Il19
