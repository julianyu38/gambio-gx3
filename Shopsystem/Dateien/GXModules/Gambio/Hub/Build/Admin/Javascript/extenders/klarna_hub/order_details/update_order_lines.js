'use strict';

/* --------------------------------------------------------------
 update_order_lines.js 2017-12-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Triggers the update of the shop order lines recalculation so that both Klarna and shop share the same price amounts.
 */
$(function () {
	'use strict';

	/**
  * Initializes the module.
  *
  * @private
  */

	var init = function init() {
		var orderId = KlarnaHub.Lib.getUrlParameter('oID');

		if (!orderId) {
			return;
		}

		var $orderDetailsTable = $('#order-details-table');

		var $spinner = jse.libs.loading_spinner.show($orderDetailsTable);

		var url = 'orders_edit.php?action=save_order&oID=' + orderId;

		var data = {
			customers_status_id: '',
			oID: orderId,
			cID: '',
			recalculate: '1'
		};

		$.post(url, data).done(function (response) {
			var $html = $(response);

			$orderDetailsTable.replaceWith($html.find('#order-details-table').parent().html());

			jse.libs.loading_spinner.hide($spinner);
		});
	};

	KlarnaHub.on('ready', function () {
		return init();
	});
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZGV0YWlscy91cGRhdGVfb3JkZXJfbGluZXMuanMiXSwibmFtZXMiOlsiJCIsImluaXQiLCJvcmRlcklkIiwiS2xhcm5hSHViIiwiTGliIiwiZ2V0VXJsUGFyYW1ldGVyIiwiJG9yZGVyRGV0YWlsc1RhYmxlIiwiJHNwaW5uZXIiLCJqc2UiLCJsaWJzIiwibG9hZGluZ19zcGlubmVyIiwic2hvdyIsInVybCIsImRhdGEiLCJjdXN0b21lcnNfc3RhdHVzX2lkIiwib0lEIiwiY0lEIiwicmVjYWxjdWxhdGUiLCJwb3N0IiwiZG9uZSIsInJlc3BvbnNlIiwiJGh0bWwiLCJyZXBsYWNlV2l0aCIsImZpbmQiLCJwYXJlbnQiLCJodG1sIiwiaGlkZSIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7OztBQUdBQSxFQUFFLFlBQVc7QUFDWjs7QUFFQTs7Ozs7O0FBS0EsS0FBTUMsT0FBTyxTQUFQQSxJQUFPLEdBQU07QUFDbEIsTUFBTUMsVUFBVUMsVUFBVUMsR0FBVixDQUFjQyxlQUFkLENBQThCLEtBQTlCLENBQWhCOztBQUVBLE1BQUksQ0FBQ0gsT0FBTCxFQUFjO0FBQ2I7QUFDQTs7QUFFRCxNQUFNSSxxQkFBcUJOLEVBQUUsc0JBQUYsQ0FBM0I7O0FBRUEsTUFBTU8sV0FBV0MsSUFBSUMsSUFBSixDQUFTQyxlQUFULENBQXlCQyxJQUF6QixDQUE4Qkwsa0JBQTlCLENBQWpCOztBQUVBLE1BQU1NLE1BQU0sMkNBQTJDVixPQUF2RDs7QUFFQSxNQUFNVyxPQUFPO0FBQ1pDLHdCQUFxQixFQURUO0FBRVpDLFFBQUtiLE9BRk87QUFHWmMsUUFBSyxFQUhPO0FBSVpDLGdCQUFhO0FBSkQsR0FBYjs7QUFPQWpCLElBQUVrQixJQUFGLENBQU9OLEdBQVAsRUFBWUMsSUFBWixFQUNFTSxJQURGLENBQ08sVUFBQ0MsUUFBRCxFQUFjO0FBQ25CLE9BQU1DLFFBQVFyQixFQUFFb0IsUUFBRixDQUFkOztBQUVBZCxzQkFBbUJnQixXQUFuQixDQUErQkQsTUFBTUUsSUFBTixDQUFXLHNCQUFYLEVBQW1DQyxNQUFuQyxHQUE0Q0MsSUFBNUMsRUFBL0I7O0FBRUFqQixPQUFJQyxJQUFKLENBQVNDLGVBQVQsQ0FBeUJnQixJQUF6QixDQUE4Qm5CLFFBQTlCO0FBQ0EsR0FQRjtBQVFBLEVBNUJEOztBQThCQUosV0FBVXdCLEVBQVYsQ0FBYSxPQUFiLEVBQXNCO0FBQUEsU0FBTTFCLE1BQU47QUFBQSxFQUF0QjtBQUNBLENBdkNEIiwiZmlsZSI6IkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZGV0YWlscy91cGRhdGVfb3JkZXJfbGluZXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gdXBkYXRlX29yZGVyX2xpbmVzLmpzIDIwMTctMTItMThcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogVHJpZ2dlcnMgdGhlIHVwZGF0ZSBvZiB0aGUgc2hvcCBvcmRlciBsaW5lcyByZWNhbGN1bGF0aW9uIHNvIHRoYXQgYm90aCBLbGFybmEgYW5kIHNob3Agc2hhcmUgdGhlIHNhbWUgcHJpY2UgYW1vdW50cy5cclxuICovXHJcbiQoZnVuY3Rpb24oKSB7XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEluaXRpYWxpemVzIHRoZSBtb2R1bGUuXHJcblx0ICpcclxuXHQgKiBAcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdGNvbnN0IGluaXQgPSAoKSA9PiB7XHJcblx0XHRjb25zdCBvcmRlcklkID0gS2xhcm5hSHViLkxpYi5nZXRVcmxQYXJhbWV0ZXIoJ29JRCcpO1xyXG5cdFx0XHJcblx0XHRpZiAoIW9yZGVySWQpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRjb25zdCAkb3JkZXJEZXRhaWxzVGFibGUgPSAkKCcjb3JkZXItZGV0YWlscy10YWJsZScpO1xyXG5cdFx0XHJcblx0XHRjb25zdCAkc3Bpbm5lciA9IGpzZS5saWJzLmxvYWRpbmdfc3Bpbm5lci5zaG93KCRvcmRlckRldGFpbHNUYWJsZSk7XHJcblx0XHRcclxuXHRcdGNvbnN0IHVybCA9ICdvcmRlcnNfZWRpdC5waHA/YWN0aW9uPXNhdmVfb3JkZXImb0lEPScgKyBvcmRlcklkO1xyXG5cdFx0XHJcblx0XHRjb25zdCBkYXRhID0ge1xyXG5cdFx0XHRjdXN0b21lcnNfc3RhdHVzX2lkOiAnJyxcclxuXHRcdFx0b0lEOiBvcmRlcklkLFxyXG5cdFx0XHRjSUQ6ICcnLFxyXG5cdFx0XHRyZWNhbGN1bGF0ZTogJzEnXHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHQkLnBvc3QodXJsLCBkYXRhKVxyXG5cdFx0XHQuZG9uZSgocmVzcG9uc2UpID0+IHtcclxuXHRcdFx0XHRjb25zdCAkaHRtbCA9ICQocmVzcG9uc2UpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdCRvcmRlckRldGFpbHNUYWJsZS5yZXBsYWNlV2l0aCgkaHRtbC5maW5kKCcjb3JkZXItZGV0YWlscy10YWJsZScpLnBhcmVudCgpLmh0bWwoKSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0anNlLmxpYnMubG9hZGluZ19zcGlubmVyLmhpZGUoJHNwaW5uZXIpO1xyXG5cdFx0XHR9KTtcclxuXHR9O1xyXG5cdFxyXG5cdEtsYXJuYUh1Yi5vbigncmVhZHknLCAoKSA9PiBpbml0KCkpO1xyXG59KTsgIl19
