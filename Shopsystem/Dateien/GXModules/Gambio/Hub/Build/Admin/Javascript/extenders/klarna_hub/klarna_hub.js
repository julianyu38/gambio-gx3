'use strict';

/* --------------------------------------------------------------
 klarna_hub.js 2018-02-09
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * KlarnaHub Module
 *
 * Important:
 *
 * This script must explicitly be called with the "appUrl" GET parameter and optionally with the "orderNumber" and
 * "moduleCode" GET parameters.
 *
 * Example:
 *
 * http://shop.de/../../klarna_hub.js?appUrl=shop.de/&orderNumber=10&moduleCode=KlarnaPaynowHub
 *
 * Events:
 *
 * - ready Called whenever the document and KlarnaHub configuration are loaded.
 * 
 * @module KlarnaHub
 */
(function () {
	'use strict';

	/**
  * KlarnaHub event callback functions.
  *
  * @type {Object}
  */

	var callbacks = {
		ready: []
	};

	/**
  * App URL.
  *
  * @type {String}
  */
	var appUrl = void 0;

	/**
  * Shop order number/ID.
  *
  * @type {String|null}
  */
	var orderNumber = void 0;

	/**
  * Gambio Hub module code.
  *
  * @type {String|null}
  */
	var moduleCode = void 0;

	/**
  * Register KlarnaHub event callback.
  *
  * @param {String} event Event name.
  * @param {Function} callback Event callback function.
  * 
  * @public
  */
	var on = function on(event, callback) {
		if (!callbacks[event]) {
			throw new Error('Invalid event type provided: ' + event);
		}

		callbacks[event].push(callback);
	};

	/**
  * Get current script URL. 
  * 
  * @return {String} Returns the "src" attribute of the current script. 
  * 
  * @private
  */
	var getScriptUrl = function getScriptUrl() {
		var $script = $('script').filter(function (index, script) {
			return script.src.includes('appUrl');
		});

		return $script.attr('src');
	};

	/**
  * Initializes the KlarnaHub module.
  *
  * Call this method once the DOM is ready or the JSEngine has finished loading the modules.
  * 
  * @private
  */
	var init = function init() {
		var scriptUrl = getScriptUrl();
		appUrl = KlarnaHub.Lib.getUrlParameter('appUrl', scriptUrl);
		orderNumber = KlarnaHub.Lib.getUrlParameter('orderNumber', scriptUrl);
		moduleCode = KlarnaHub.Lib.getUrlParameter('moduleCode', scriptUrl);

		KlarnaHub.Api.getConfiguration(appUrl, orderNumber, moduleCode).then(function (configuration) {
			window.KlarnaHub.Config = configuration;

			if (!configuration.clientKey) {
				return; // The client is not connected to Hub any more. 
			}

			callbacks.ready.forEach(function (callback) {
				return callback(configuration);
			});
		});
	};

	// Export 
	window.KlarnaHub = Object.assign({}, { on: on }, window.KlarnaHub);

	// Initialize
	document.addEventListener('JSENGINE_INIT_FINISHED', function () {
		return init();
	});
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIva2xhcm5hX2h1Yi5qcyJdLCJuYW1lcyI6WyJjYWxsYmFja3MiLCJyZWFkeSIsImFwcFVybCIsIm9yZGVyTnVtYmVyIiwibW9kdWxlQ29kZSIsIm9uIiwiZXZlbnQiLCJjYWxsYmFjayIsIkVycm9yIiwicHVzaCIsImdldFNjcmlwdFVybCIsIiRzY3JpcHQiLCIkIiwiZmlsdGVyIiwiaW5kZXgiLCJzY3JpcHQiLCJzcmMiLCJpbmNsdWRlcyIsImF0dHIiLCJpbml0Iiwic2NyaXB0VXJsIiwiS2xhcm5hSHViIiwiTGliIiwiZ2V0VXJsUGFyYW1ldGVyIiwiQXBpIiwiZ2V0Q29uZmlndXJhdGlvbiIsInRoZW4iLCJ3aW5kb3ciLCJDb25maWciLCJjb25maWd1cmF0aW9uIiwiY2xpZW50S2V5IiwiZm9yRWFjaCIsIk9iamVjdCIsImFzc2lnbiIsImRvY3VtZW50IiwiYWRkRXZlbnRMaXN0ZW5lciJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkEsQ0FBQyxZQUFXO0FBQ1g7O0FBRUE7Ozs7OztBQUtBLEtBQU1BLFlBQVk7QUFDakJDLFNBQU87QUFEVSxFQUFsQjs7QUFJQTs7Ozs7QUFLQSxLQUFJQyxlQUFKOztBQUVBOzs7OztBQUtBLEtBQUlDLG9CQUFKOztBQUVBOzs7OztBQUtBLEtBQUlDLG1CQUFKOztBQUVBOzs7Ozs7OztBQVFBLEtBQU1DLEtBQUssU0FBTEEsRUFBSyxDQUFDQyxLQUFELEVBQVFDLFFBQVIsRUFBcUI7QUFDL0IsTUFBSSxDQUFDUCxVQUFVTSxLQUFWLENBQUwsRUFBdUI7QUFDdEIsU0FBTSxJQUFJRSxLQUFKLENBQVUsa0NBQWtDRixLQUE1QyxDQUFOO0FBQ0E7O0FBRUROLFlBQVVNLEtBQVYsRUFBaUJHLElBQWpCLENBQXNCRixRQUF0QjtBQUNBLEVBTkQ7O0FBUUE7Ozs7Ozs7QUFPQSxLQUFNRyxlQUFlLFNBQWZBLFlBQWUsR0FBTTtBQUMxQixNQUFNQyxVQUFVQyxFQUFFLFFBQUYsRUFBWUMsTUFBWixDQUFtQixVQUFTQyxLQUFULEVBQWdCQyxNQUFoQixFQUF3QjtBQUMxRCxVQUFPQSxPQUFPQyxHQUFQLENBQVdDLFFBQVgsQ0FBb0IsUUFBcEIsQ0FBUDtBQUNBLEdBRmUsQ0FBaEI7O0FBSUEsU0FBT04sUUFBUU8sSUFBUixDQUFhLEtBQWIsQ0FBUDtBQUNBLEVBTkQ7O0FBUUE7Ozs7Ozs7QUFPQSxLQUFNQyxPQUFPLFNBQVBBLElBQU8sR0FBTTtBQUNsQixNQUFNQyxZQUFZVixjQUFsQjtBQUNBUixXQUFTbUIsVUFBVUMsR0FBVixDQUFjQyxlQUFkLENBQThCLFFBQTlCLEVBQXdDSCxTQUF4QyxDQUFUO0FBQ0FqQixnQkFBY2tCLFVBQVVDLEdBQVYsQ0FBY0MsZUFBZCxDQUE4QixhQUE5QixFQUE2Q0gsU0FBN0MsQ0FBZDtBQUNBaEIsZUFBYWlCLFVBQVVDLEdBQVYsQ0FBY0MsZUFBZCxDQUE4QixZQUE5QixFQUE0Q0gsU0FBNUMsQ0FBYjs7QUFFQUMsWUFBVUcsR0FBVixDQUFjQyxnQkFBZCxDQUErQnZCLE1BQS9CLEVBQXVDQyxXQUF2QyxFQUFvREMsVUFBcEQsRUFDRXNCLElBREYsQ0FDTyx5QkFBaUI7QUFDdEJDLFVBQU9OLFNBQVAsQ0FBaUJPLE1BQWpCLEdBQTBCQyxhQUExQjs7QUFFQSxPQUFJLENBQUNBLGNBQWNDLFNBQW5CLEVBQThCO0FBQzdCLFdBRDZCLENBQ3JCO0FBQ1I7O0FBRUQ5QixhQUFVQyxLQUFWLENBQWdCOEIsT0FBaEIsQ0FBd0I7QUFBQSxXQUFZeEIsU0FBU3NCLGFBQVQsQ0FBWjtBQUFBLElBQXhCO0FBQ0EsR0FURjtBQVVBLEVBaEJEOztBQWtCQTtBQUNBRixRQUFPTixTQUFQLEdBQW1CVyxPQUFPQyxNQUFQLENBQWMsRUFBZCxFQUFrQixFQUFDNUIsTUFBRCxFQUFsQixFQUF3QnNCLE9BQU9OLFNBQS9CLENBQW5COztBQUVBO0FBQ0FhLFVBQVNDLGdCQUFULENBQTBCLHdCQUExQixFQUFvRDtBQUFBLFNBQU1oQixNQUFOO0FBQUEsRUFBcEQ7QUFDQSxDQTlGRCIsImZpbGUiOiJBZG1pbi9KYXZhc2NyaXB0L2V4dGVuZGVycy9rbGFybmFfaHViL2tsYXJuYV9odWIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4ga2xhcm5hX2h1Yi5qcyAyMDE4LTAyLTA5XHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIEtsYXJuYUh1YiBNb2R1bGVcclxuICpcclxuICogSW1wb3J0YW50OlxyXG4gKlxyXG4gKiBUaGlzIHNjcmlwdCBtdXN0IGV4cGxpY2l0bHkgYmUgY2FsbGVkIHdpdGggdGhlIFwiYXBwVXJsXCIgR0VUIHBhcmFtZXRlciBhbmQgb3B0aW9uYWxseSB3aXRoIHRoZSBcIm9yZGVyTnVtYmVyXCIgYW5kXHJcbiAqIFwibW9kdWxlQ29kZVwiIEdFVCBwYXJhbWV0ZXJzLlxyXG4gKlxyXG4gKiBFeGFtcGxlOlxyXG4gKlxyXG4gKiBodHRwOi8vc2hvcC5kZS8uLi8uLi9rbGFybmFfaHViLmpzP2FwcFVybD1zaG9wLmRlLyZvcmRlck51bWJlcj0xMCZtb2R1bGVDb2RlPUtsYXJuYVBheW5vd0h1YlxyXG4gKlxyXG4gKiBFdmVudHM6XHJcbiAqXHJcbiAqIC0gcmVhZHkgQ2FsbGVkIHdoZW5ldmVyIHRoZSBkb2N1bWVudCBhbmQgS2xhcm5hSHViIGNvbmZpZ3VyYXRpb24gYXJlIGxvYWRlZC5cclxuICogXHJcbiAqIEBtb2R1bGUgS2xhcm5hSHViXHJcbiAqL1xyXG4oZnVuY3Rpb24oKSB7XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEtsYXJuYUh1YiBldmVudCBjYWxsYmFjayBmdW5jdGlvbnMuXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7T2JqZWN0fVxyXG5cdCAqL1xyXG5cdGNvbnN0IGNhbGxiYWNrcyA9IHtcclxuXHRcdHJlYWR5OiBbXVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQXBwIFVSTC5cclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtTdHJpbmd9XHJcblx0ICovXHJcblx0bGV0IGFwcFVybDtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTaG9wIG9yZGVyIG51bWJlci9JRC5cclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtTdHJpbmd8bnVsbH1cclxuXHQgKi9cclxuXHRsZXQgb3JkZXJOdW1iZXI7XHJcblx0XHJcblx0LyoqXHJcblx0ICogR2FtYmlvIEh1YiBtb2R1bGUgY29kZS5cclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtTdHJpbmd8bnVsbH1cclxuXHQgKi9cclxuXHRsZXQgbW9kdWxlQ29kZTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBSZWdpc3RlciBLbGFybmFIdWIgZXZlbnQgY2FsbGJhY2suXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gZXZlbnQgRXZlbnQgbmFtZS5cclxuXHQgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFjayBFdmVudCBjYWxsYmFjayBmdW5jdGlvbi5cclxuXHQgKiBcclxuXHQgKiBAcHVibGljXHJcblx0ICovXHJcblx0Y29uc3Qgb24gPSAoZXZlbnQsIGNhbGxiYWNrKSA9PiB7XHJcblx0XHRpZiAoIWNhbGxiYWNrc1tldmVudF0pIHtcclxuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGV2ZW50IHR5cGUgcHJvdmlkZWQ6ICcgKyBldmVudCk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdGNhbGxiYWNrc1tldmVudF0ucHVzaChjYWxsYmFjayk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBHZXQgY3VycmVudCBzY3JpcHQgVVJMLiBcclxuXHQgKiBcclxuXHQgKiBAcmV0dXJuIHtTdHJpbmd9IFJldHVybnMgdGhlIFwic3JjXCIgYXR0cmlidXRlIG9mIHRoZSBjdXJyZW50IHNjcmlwdC4gXHJcblx0ICogXHJcblx0ICogQHByaXZhdGVcclxuXHQgKi9cclxuXHRjb25zdCBnZXRTY3JpcHRVcmwgPSAoKSA9PiB7XHJcblx0XHRjb25zdCAkc2NyaXB0ID0gJCgnc2NyaXB0JykuZmlsdGVyKGZ1bmN0aW9uKGluZGV4LCBzY3JpcHQpIHtcclxuXHRcdFx0cmV0dXJuIHNjcmlwdC5zcmMuaW5jbHVkZXMoJ2FwcFVybCcpO1xyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdHJldHVybiAkc2NyaXB0LmF0dHIoJ3NyYycpO1xyXG5cdH07IFxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEluaXRpYWxpemVzIHRoZSBLbGFybmFIdWIgbW9kdWxlLlxyXG5cdCAqXHJcblx0ICogQ2FsbCB0aGlzIG1ldGhvZCBvbmNlIHRoZSBET00gaXMgcmVhZHkgb3IgdGhlIEpTRW5naW5lIGhhcyBmaW5pc2hlZCBsb2FkaW5nIHRoZSBtb2R1bGVzLlxyXG5cdCAqIFxyXG5cdCAqIEBwcml2YXRlXHJcblx0ICovXHJcblx0Y29uc3QgaW5pdCA9ICgpID0+IHtcclxuXHRcdGNvbnN0IHNjcmlwdFVybCA9IGdldFNjcmlwdFVybCgpOyBcclxuXHRcdGFwcFVybCA9IEtsYXJuYUh1Yi5MaWIuZ2V0VXJsUGFyYW1ldGVyKCdhcHBVcmwnLCBzY3JpcHRVcmwpO1xyXG5cdFx0b3JkZXJOdW1iZXIgPSBLbGFybmFIdWIuTGliLmdldFVybFBhcmFtZXRlcignb3JkZXJOdW1iZXInLCBzY3JpcHRVcmwpO1xyXG5cdFx0bW9kdWxlQ29kZSA9IEtsYXJuYUh1Yi5MaWIuZ2V0VXJsUGFyYW1ldGVyKCdtb2R1bGVDb2RlJywgc2NyaXB0VXJsKTtcclxuXHRcdFxyXG5cdFx0S2xhcm5hSHViLkFwaS5nZXRDb25maWd1cmF0aW9uKGFwcFVybCwgb3JkZXJOdW1iZXIsIG1vZHVsZUNvZGUpXHJcblx0XHRcdC50aGVuKGNvbmZpZ3VyYXRpb24gPT4ge1xyXG5cdFx0XHRcdHdpbmRvdy5LbGFybmFIdWIuQ29uZmlnID0gY29uZmlndXJhdGlvbjtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoIWNvbmZpZ3VyYXRpb24uY2xpZW50S2V5KSB7XHJcblx0XHRcdFx0XHRyZXR1cm47IC8vIFRoZSBjbGllbnQgaXMgbm90IGNvbm5lY3RlZCB0byBIdWIgYW55IG1vcmUuIFxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRjYWxsYmFja3MucmVhZHkuZm9yRWFjaChjYWxsYmFjayA9PiBjYWxsYmFjayhjb25maWd1cmF0aW9uKSk7XHJcblx0XHRcdH0pO1xyXG5cdH07XHJcblx0XHJcblx0Ly8gRXhwb3J0IFxyXG5cdHdpbmRvdy5LbGFybmFIdWIgPSBPYmplY3QuYXNzaWduKHt9LCB7b259LCB3aW5kb3cuS2xhcm5hSHViKTtcclxuXHRcclxuXHQvLyBJbml0aWFsaXplXHJcblx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignSlNFTkdJTkVfSU5JVF9GSU5JU0hFRCcsICgpID0+IGluaXQoKSk7XHJcbn0pKCk7Il19
