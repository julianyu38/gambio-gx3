'use strict';

/* --------------------------------------------------------------
 extend_order_status_modal.js 2017-11-03
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Extends the order status modal with a "notify-klarna-hub" checkbox.
 */
(function () {
	'use strict';

	/**
  * Used for bulk actions.
  *
  * @type {Number[]}
  */

	var selectedKlarnaHubOrderNumbers = [];

	/**
  * Initializes the module.
  *
  * @private
  */
	var init = function init() {
		var $modal = $('.modal.status');
		var $select = $('#status-dropdown');
		var $orderStatusSave = $modal.find('.btn.save');
		var moduleCodes = ['KlarnaHub', 'KlarnaPaynowHub', 'KlarnaPaylaterHub', 'KlarnaSliceitHub', 'KlarnaBanktransferHub'];

		// Add the notify-klarna-hub checkbox (hidden by default).
		var $sourceListItem = $modal.find('.single-checkbox:first').closest('li');

		var $listItem = $sourceListItem.clone(true);
		$listItem.addClass('hidden');

		var $label = $listItem.find('label');
		$label.text(KlarnaHub.Config.lang.NOTIFY_KLARNA);

		var $singleCheckbox = $listItem.find('.single-checkbox');
		var $checkbox = $listItem.find('input:checkbox');
		$checkbox.addClass('notify-klarna');
		$checkbox.on('change', function (event) {
			return $checkbox.parent().toggleClass('checked', $checkbox.prop('checked'));
		});

		$listItem.insertBefore($sourceListItem.next());

		// Bind status type change event.
		$select.on('change', function () {
			var hasCorrectStatus = $select.val() && $select.val() == KlarnaHub.Config.orderStatusShipped;
			var hasSelectedOrders = KlarnaHub.Config.orderNumber !== null || selectedKlarnaHubOrderNumbers.length;

			if (hasCorrectStatus && hasSelectedOrders) {
				$listItem.removeClass('hidden');
				$checkbox.prop('checked', true);
				$singleCheckbox.addClass('checked');
			} else {
				$listItem.addClass('hidden');
				$checkbox.prop('checked', false);
				$singleCheckbox.removeClass('checked');
			}
		});

		// Bind order status save button click event.
		$orderStatusSave.on('click', function () {
			var hasCorrectStatus = $select.val() && $select.val() == KlarnaHub.Config.orderStatusShipped;
			var hasSelectedOrders = KlarnaHub.Config.orderNumber !== null || selectedKlarnaHubOrderNumbers.length;

			if (!hasCorrectStatus || !hasSelectedOrders || !$checkbox.prop('checked')) {
				return;
			}

			var orderNumbers = selectedKlarnaHubOrderNumbers.length ? selectedKlarnaHubOrderNumbers : [KlarnaHub.Config.orderNumber];

			orderNumbers.forEach(function (orderNumber) {
				KlarnaHub.Config.orderNumber = orderNumber;
				KlarnaHub.Config.moduleCode = $table.find('tr#' + orderNumber).data('gambioHubModule');

				if ($select.val() == KlarnaHub.Config.orderStatusShipped) {
					KlarnaHub.Api.executeFullCapture();
				}
			});
		});

		var $table = $('.orders.overview table');

		$table.on('click', '.change-status, .tooltip-order-status-history', function (event) {
			var $row = $(event.target).closest('tr');
			var orderNumber = $row.data('id');
			var moduleCode = $row.data('gambioHubModule');

			if (orderNumber && moduleCode && moduleCodes.includes(moduleCode)) {
				KlarnaHub.Config.orderNumber = orderNumber;
				KlarnaHub.Config.moduleCode = moduleCode;
			} else {
				KlarnaHub.Config.orderNumber = null;
				KlarnaHub.Config.moduleCode = null;
			}
		});

		$modal.on('show.bs.modal', function () {
			return $select.trigger('change');
		});

		$modal.on('hide.bs.modal', function () {
			KlarnaHub.Config.orderNumber = null;
			KlarnaHub.Config.moduleCode = null;
			selectedKlarnaHubOrderNumbers = [];
		});

		var $bulkAction = $('.bulk-action');

		$bulkAction.on('click', '.change-status', function () {
			selectedKlarnaHubOrderNumbers = KlarnaHub.Lib.getSelectedKlarnaHubOrderNumbers();
		});
	};

	KlarnaHub.on('ready', function () {
		return init();
	});
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfb3ZlcnZpZXcvZXh0ZW5kX29yZGVyX3N0YXR1c19tb2RhbC5qcyJdLCJuYW1lcyI6WyJzZWxlY3RlZEtsYXJuYUh1Yk9yZGVyTnVtYmVycyIsImluaXQiLCIkbW9kYWwiLCIkIiwiJHNlbGVjdCIsIiRvcmRlclN0YXR1c1NhdmUiLCJmaW5kIiwibW9kdWxlQ29kZXMiLCIkc291cmNlTGlzdEl0ZW0iLCJjbG9zZXN0IiwiJGxpc3RJdGVtIiwiY2xvbmUiLCJhZGRDbGFzcyIsIiRsYWJlbCIsInRleHQiLCJLbGFybmFIdWIiLCJDb25maWciLCJsYW5nIiwiTk9USUZZX0tMQVJOQSIsIiRzaW5nbGVDaGVja2JveCIsIiRjaGVja2JveCIsIm9uIiwiZXZlbnQiLCJwYXJlbnQiLCJ0b2dnbGVDbGFzcyIsInByb3AiLCJpbnNlcnRCZWZvcmUiLCJuZXh0IiwiaGFzQ29ycmVjdFN0YXR1cyIsInZhbCIsIm9yZGVyU3RhdHVzU2hpcHBlZCIsImhhc1NlbGVjdGVkT3JkZXJzIiwib3JkZXJOdW1iZXIiLCJsZW5ndGgiLCJyZW1vdmVDbGFzcyIsIm9yZGVyTnVtYmVycyIsImZvckVhY2giLCJtb2R1bGVDb2RlIiwiJHRhYmxlIiwiZGF0YSIsIkFwaSIsImV4ZWN1dGVGdWxsQ2FwdHVyZSIsIiRyb3ciLCJ0YXJnZXQiLCJpbmNsdWRlcyIsInRyaWdnZXIiLCIkYnVsa0FjdGlvbiIsIkxpYiIsImdldFNlbGVjdGVkS2xhcm5hSHViT3JkZXJOdW1iZXJzIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7OztBQUdBLENBQUMsWUFBVztBQUNYOztBQUVBOzs7Ozs7QUFLQSxLQUFJQSxnQ0FBZ0MsRUFBcEM7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsT0FBTyxTQUFQQSxJQUFPLEdBQU07QUFDbEIsTUFBTUMsU0FBU0MsRUFBRSxlQUFGLENBQWY7QUFDQSxNQUFNQyxVQUFVRCxFQUFFLGtCQUFGLENBQWhCO0FBQ0EsTUFBTUUsbUJBQW1CSCxPQUFPSSxJQUFQLENBQVksV0FBWixDQUF6QjtBQUNBLE1BQU1DLGNBQWMsQ0FDbkIsV0FEbUIsRUFFbkIsaUJBRm1CLEVBR25CLG1CQUhtQixFQUluQixrQkFKbUIsRUFLbkIsdUJBTG1CLENBQXBCOztBQVFBO0FBQ0EsTUFBTUMsa0JBQWtCTixPQUFPSSxJQUFQLENBQVksd0JBQVosRUFBc0NHLE9BQXRDLENBQThDLElBQTlDLENBQXhCOztBQUVBLE1BQU1DLFlBQVlGLGdCQUFnQkcsS0FBaEIsQ0FBc0IsSUFBdEIsQ0FBbEI7QUFDQUQsWUFBVUUsUUFBVixDQUFtQixRQUFuQjs7QUFFQSxNQUFNQyxTQUFTSCxVQUFVSixJQUFWLENBQWUsT0FBZixDQUFmO0FBQ0FPLFNBQU9DLElBQVAsQ0FBWUMsVUFBVUMsTUFBVixDQUFpQkMsSUFBakIsQ0FBc0JDLGFBQWxDOztBQUVBLE1BQU1DLGtCQUFrQlQsVUFBVUosSUFBVixDQUFlLGtCQUFmLENBQXhCO0FBQ0EsTUFBTWMsWUFBWVYsVUFBVUosSUFBVixDQUFlLGdCQUFmLENBQWxCO0FBQ0FjLFlBQVVSLFFBQVYsQ0FBbUIsZUFBbkI7QUFDQVEsWUFBVUMsRUFBVixDQUFhLFFBQWIsRUFBdUIsVUFBQ0MsS0FBRDtBQUFBLFVBQVdGLFVBQVVHLE1BQVYsR0FBbUJDLFdBQW5CLENBQStCLFNBQS9CLEVBQTBDSixVQUFVSyxJQUFWLENBQWUsU0FBZixDQUExQyxDQUFYO0FBQUEsR0FBdkI7O0FBRUFmLFlBQVVnQixZQUFWLENBQXVCbEIsZ0JBQWdCbUIsSUFBaEIsRUFBdkI7O0FBRUE7QUFDQXZCLFVBQVFpQixFQUFSLENBQVcsUUFBWCxFQUFxQixZQUFNO0FBQzFCLE9BQU1PLG1CQUFtQnhCLFFBQVF5QixHQUFSLE1BQ3BCekIsUUFBUXlCLEdBQVIsTUFBaUJkLFVBQVVDLE1BQVYsQ0FBaUJjLGtCQUR2QztBQUVBLE9BQU1DLG9CQUFvQmhCLFVBQVVDLE1BQVYsQ0FBaUJnQixXQUFqQixLQUFpQyxJQUFqQyxJQUF5Q2hDLDhCQUE4QmlDLE1BQWpHOztBQUVBLE9BQUlMLG9CQUFvQkcsaUJBQXhCLEVBQTJDO0FBQzFDckIsY0FBVXdCLFdBQVYsQ0FBc0IsUUFBdEI7QUFDQWQsY0FBVUssSUFBVixDQUFlLFNBQWYsRUFBMEIsSUFBMUI7QUFDQU4sb0JBQWdCUCxRQUFoQixDQUF5QixTQUF6QjtBQUNBLElBSkQsTUFJTztBQUNORixjQUFVRSxRQUFWLENBQW1CLFFBQW5CO0FBQ0FRLGNBQVVLLElBQVYsQ0FBZSxTQUFmLEVBQTBCLEtBQTFCO0FBQ0FOLG9CQUFnQmUsV0FBaEIsQ0FBNEIsU0FBNUI7QUFDQTtBQUNELEdBZEQ7O0FBZ0JBO0FBQ0E3QixtQkFBaUJnQixFQUFqQixDQUFvQixPQUFwQixFQUE2QixZQUFNO0FBQ2xDLE9BQU1PLG1CQUFtQnhCLFFBQVF5QixHQUFSLE1BQ3BCekIsUUFBUXlCLEdBQVIsTUFBaUJkLFVBQVVDLE1BQVYsQ0FBaUJjLGtCQUR2QztBQUVBLE9BQU1DLG9CQUFvQmhCLFVBQVVDLE1BQVYsQ0FBaUJnQixXQUFqQixLQUFpQyxJQUFqQyxJQUF5Q2hDLDhCQUE4QmlDLE1BQWpHOztBQUVBLE9BQUksQ0FBQ0wsZ0JBQUQsSUFBcUIsQ0FBQ0csaUJBQXRCLElBQTJDLENBQUNYLFVBQVVLLElBQVYsQ0FBZSxTQUFmLENBQWhELEVBQTJFO0FBQzFFO0FBQ0E7O0FBRUQsT0FBTVUsZUFBZW5DLDhCQUE4QmlDLE1BQTlCLEdBQ2xCakMsNkJBRGtCLEdBQ2MsQ0FBQ2UsVUFBVUMsTUFBVixDQUFpQmdCLFdBQWxCLENBRG5DOztBQUdBRyxnQkFBYUMsT0FBYixDQUFxQix1QkFBZTtBQUNuQ3JCLGNBQVVDLE1BQVYsQ0FBaUJnQixXQUFqQixHQUErQkEsV0FBL0I7QUFDQWpCLGNBQVVDLE1BQVYsQ0FBaUJxQixVQUFqQixHQUE4QkMsT0FBT2hDLElBQVAsU0FBa0IwQixXQUFsQixFQUFpQ08sSUFBakMsQ0FBc0MsaUJBQXRDLENBQTlCOztBQUVBLFFBQUluQyxRQUFReUIsR0FBUixNQUFpQmQsVUFBVUMsTUFBVixDQUFpQmMsa0JBQXRDLEVBQTBEO0FBQ3pEZixlQUFVeUIsR0FBVixDQUFjQyxrQkFBZDtBQUNBO0FBQ0QsSUFQRDtBQVFBLEdBcEJEOztBQXNCQSxNQUFNSCxTQUFTbkMsRUFBRSx3QkFBRixDQUFmOztBQUVBbUMsU0FBT2pCLEVBQVAsQ0FBVSxPQUFWLEVBQW1CLCtDQUFuQixFQUFvRSxVQUFDQyxLQUFELEVBQVc7QUFDOUUsT0FBTW9CLE9BQU92QyxFQUFFbUIsTUFBTXFCLE1BQVIsRUFBZ0JsQyxPQUFoQixDQUF3QixJQUF4QixDQUFiO0FBQ0EsT0FBTXVCLGNBQWNVLEtBQUtILElBQUwsQ0FBVSxJQUFWLENBQXBCO0FBQ0EsT0FBTUYsYUFBYUssS0FBS0gsSUFBTCxDQUFVLGlCQUFWLENBQW5COztBQUVBLE9BQUlQLGVBQWVLLFVBQWYsSUFBNkI5QixZQUFZcUMsUUFBWixDQUFxQlAsVUFBckIsQ0FBakMsRUFBbUU7QUFDbEV0QixjQUFVQyxNQUFWLENBQWlCZ0IsV0FBakIsR0FBK0JBLFdBQS9CO0FBQ0FqQixjQUFVQyxNQUFWLENBQWlCcUIsVUFBakIsR0FBOEJBLFVBQTlCO0FBQ0EsSUFIRCxNQUdPO0FBQ050QixjQUFVQyxNQUFWLENBQWlCZ0IsV0FBakIsR0FBK0IsSUFBL0I7QUFDQWpCLGNBQVVDLE1BQVYsQ0FBaUJxQixVQUFqQixHQUE4QixJQUE5QjtBQUNBO0FBQ0QsR0FaRDs7QUFjQW5DLFNBQU9tQixFQUFQLENBQVUsZUFBVixFQUEyQjtBQUFBLFVBQU1qQixRQUFReUMsT0FBUixDQUFnQixRQUFoQixDQUFOO0FBQUEsR0FBM0I7O0FBRUEzQyxTQUFPbUIsRUFBUCxDQUFVLGVBQVYsRUFBMkIsWUFBTTtBQUNoQ04sYUFBVUMsTUFBVixDQUFpQmdCLFdBQWpCLEdBQStCLElBQS9CO0FBQ0FqQixhQUFVQyxNQUFWLENBQWlCcUIsVUFBakIsR0FBOEIsSUFBOUI7QUFDQXJDLG1DQUFnQyxFQUFoQztBQUNBLEdBSkQ7O0FBTUEsTUFBTThDLGNBQWMzQyxFQUFFLGNBQUYsQ0FBcEI7O0FBRUEyQyxjQUFZekIsRUFBWixDQUFlLE9BQWYsRUFBd0IsZ0JBQXhCLEVBQTBDLFlBQU07QUFDL0NyQixtQ0FBZ0NlLFVBQVVnQyxHQUFWLENBQWNDLGdDQUFkLEVBQWhDO0FBQ0EsR0FGRDtBQUlBLEVBbEdEOztBQW9HQWpDLFdBQVVNLEVBQVYsQ0FBYSxPQUFiLEVBQXNCO0FBQUEsU0FBTXBCLE1BQU47QUFBQSxFQUF0QjtBQUNBLENBcEhEIiwiZmlsZSI6IkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfb3ZlcnZpZXcvZXh0ZW5kX29yZGVyX3N0YXR1c19tb2RhbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBleHRlbmRfb3JkZXJfc3RhdHVzX21vZGFsLmpzIDIwMTctMTEtMDNcclxuIEdhbWJpbyBHbWJIXHJcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxyXG4gQ29weXJpZ2h0IChjKSAyMDE3IEdhbWJpbyBHbWJIXHJcbiBSZWxlYXNlZCB1bmRlciB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgKFZlcnNpb24gMilcclxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblxyXG4vKipcclxuICogRXh0ZW5kcyB0aGUgb3JkZXIgc3RhdHVzIG1vZGFsIHdpdGggYSBcIm5vdGlmeS1rbGFybmEtaHViXCIgY2hlY2tib3guXHJcbiAqL1xyXG4oZnVuY3Rpb24oKSB7XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFVzZWQgZm9yIGJ1bGsgYWN0aW9ucy5cclxuXHQgKlxyXG5cdCAqIEB0eXBlIHtOdW1iZXJbXX1cclxuXHQgKi9cclxuXHRsZXQgc2VsZWN0ZWRLbGFybmFIdWJPcmRlck51bWJlcnMgPSBbXTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBJbml0aWFsaXplcyB0aGUgbW9kdWxlLlxyXG5cdCAqXHJcblx0ICogQHByaXZhdGVcclxuXHQgKi9cclxuXHRjb25zdCBpbml0ID0gKCkgPT4ge1xyXG5cdFx0Y29uc3QgJG1vZGFsID0gJCgnLm1vZGFsLnN0YXR1cycpO1xyXG5cdFx0Y29uc3QgJHNlbGVjdCA9ICQoJyNzdGF0dXMtZHJvcGRvd24nKTtcclxuXHRcdGNvbnN0ICRvcmRlclN0YXR1c1NhdmUgPSAkbW9kYWwuZmluZCgnLmJ0bi5zYXZlJyk7XHJcblx0XHRjb25zdCBtb2R1bGVDb2RlcyA9IFtcclxuXHRcdFx0J0tsYXJuYUh1YicsXHJcblx0XHRcdCdLbGFybmFQYXlub3dIdWInLFxyXG5cdFx0XHQnS2xhcm5hUGF5bGF0ZXJIdWInLFxyXG5cdFx0XHQnS2xhcm5hU2xpY2VpdEh1YicsXHJcblx0XHRcdCdLbGFybmFCYW5rdHJhbnNmZXJIdWInXHJcblx0XHRdO1xyXG5cdFx0XHJcblx0XHQvLyBBZGQgdGhlIG5vdGlmeS1rbGFybmEtaHViIGNoZWNrYm94IChoaWRkZW4gYnkgZGVmYXVsdCkuXHJcblx0XHRjb25zdCAkc291cmNlTGlzdEl0ZW0gPSAkbW9kYWwuZmluZCgnLnNpbmdsZS1jaGVja2JveDpmaXJzdCcpLmNsb3Nlc3QoJ2xpJyk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRsaXN0SXRlbSA9ICRzb3VyY2VMaXN0SXRlbS5jbG9uZSh0cnVlKTtcclxuXHRcdCRsaXN0SXRlbS5hZGRDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRsYWJlbCA9ICRsaXN0SXRlbS5maW5kKCdsYWJlbCcpO1xyXG5cdFx0JGxhYmVsLnRleHQoS2xhcm5hSHViLkNvbmZpZy5sYW5nLk5PVElGWV9LTEFSTkEpO1xyXG5cdFx0XHJcblx0XHRjb25zdCAkc2luZ2xlQ2hlY2tib3ggPSAkbGlzdEl0ZW0uZmluZCgnLnNpbmdsZS1jaGVja2JveCcpO1xyXG5cdFx0Y29uc3QgJGNoZWNrYm94ID0gJGxpc3RJdGVtLmZpbmQoJ2lucHV0OmNoZWNrYm94Jyk7XHJcblx0XHQkY2hlY2tib3guYWRkQ2xhc3MoJ25vdGlmeS1rbGFybmEnKTtcclxuXHRcdCRjaGVja2JveC5vbignY2hhbmdlJywgKGV2ZW50KSA9PiAkY2hlY2tib3gucGFyZW50KCkudG9nZ2xlQ2xhc3MoJ2NoZWNrZWQnLCAkY2hlY2tib3gucHJvcCgnY2hlY2tlZCcpKSk7XHJcblx0XHRcclxuXHRcdCRsaXN0SXRlbS5pbnNlcnRCZWZvcmUoJHNvdXJjZUxpc3RJdGVtLm5leHQoKSk7XHJcblx0XHRcclxuXHRcdC8vIEJpbmQgc3RhdHVzIHR5cGUgY2hhbmdlIGV2ZW50LlxyXG5cdFx0JHNlbGVjdC5vbignY2hhbmdlJywgKCkgPT4ge1xyXG5cdFx0XHRjb25zdCBoYXNDb3JyZWN0U3RhdHVzID0gJHNlbGVjdC52YWwoKSBcclxuXHRcdFx0XHQmJiAoJHNlbGVjdC52YWwoKSA9PSBLbGFybmFIdWIuQ29uZmlnLm9yZGVyU3RhdHVzU2hpcHBlZCk7XHJcblx0XHRcdGNvbnN0IGhhc1NlbGVjdGVkT3JkZXJzID0gS2xhcm5hSHViLkNvbmZpZy5vcmRlck51bWJlciAhPT0gbnVsbCB8fCBzZWxlY3RlZEtsYXJuYUh1Yk9yZGVyTnVtYmVycy5sZW5ndGg7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoaGFzQ29ycmVjdFN0YXR1cyAmJiBoYXNTZWxlY3RlZE9yZGVycykge1xyXG5cdFx0XHRcdCRsaXN0SXRlbS5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XHJcblx0XHRcdFx0JGNoZWNrYm94LnByb3AoJ2NoZWNrZWQnLCB0cnVlKTtcclxuXHRcdFx0XHQkc2luZ2xlQ2hlY2tib3guYWRkQ2xhc3MoJ2NoZWNrZWQnKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHQkbGlzdEl0ZW0uYWRkQ2xhc3MoJ2hpZGRlbicpO1xyXG5cdFx0XHRcdCRjaGVja2JveC5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xyXG5cdFx0XHRcdCRzaW5nbGVDaGVja2JveC5yZW1vdmVDbGFzcygnY2hlY2tlZCcpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Ly8gQmluZCBvcmRlciBzdGF0dXMgc2F2ZSBidXR0b24gY2xpY2sgZXZlbnQuXHJcblx0XHQkb3JkZXJTdGF0dXNTYXZlLm9uKCdjbGljaycsICgpID0+IHtcclxuXHRcdFx0Y29uc3QgaGFzQ29ycmVjdFN0YXR1cyA9ICRzZWxlY3QudmFsKCkgXHJcblx0XHRcdFx0JiYgKCRzZWxlY3QudmFsKCkgPT0gS2xhcm5hSHViLkNvbmZpZy5vcmRlclN0YXR1c1NoaXBwZWQpO1xyXG5cdFx0XHRjb25zdCBoYXNTZWxlY3RlZE9yZGVycyA9IEtsYXJuYUh1Yi5Db25maWcub3JkZXJOdW1iZXIgIT09IG51bGwgfHwgc2VsZWN0ZWRLbGFybmFIdWJPcmRlck51bWJlcnMubGVuZ3RoO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKCFoYXNDb3JyZWN0U3RhdHVzIHx8ICFoYXNTZWxlY3RlZE9yZGVycyB8fCAhJGNoZWNrYm94LnByb3AoJ2NoZWNrZWQnKSkge1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3Qgb3JkZXJOdW1iZXJzID0gc2VsZWN0ZWRLbGFybmFIdWJPcmRlck51bWJlcnMubGVuZ3RoXHJcblx0XHRcdFx0PyBzZWxlY3RlZEtsYXJuYUh1Yk9yZGVyTnVtYmVycyA6IFtLbGFybmFIdWIuQ29uZmlnLm9yZGVyTnVtYmVyXTtcclxuXHRcdFx0XHJcblx0XHRcdG9yZGVyTnVtYmVycy5mb3JFYWNoKG9yZGVyTnVtYmVyID0+IHtcclxuXHRcdFx0XHRLbGFybmFIdWIuQ29uZmlnLm9yZGVyTnVtYmVyID0gb3JkZXJOdW1iZXI7XHJcblx0XHRcdFx0S2xhcm5hSHViLkNvbmZpZy5tb2R1bGVDb2RlID0gJHRhYmxlLmZpbmQoYHRyIyR7b3JkZXJOdW1iZXJ9YCkuZGF0YSgnZ2FtYmlvSHViTW9kdWxlJyk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKCRzZWxlY3QudmFsKCkgPT0gS2xhcm5hSHViLkNvbmZpZy5vcmRlclN0YXR1c1NoaXBwZWQpIHtcclxuXHRcdFx0XHRcdEtsYXJuYUh1Yi5BcGkuZXhlY3V0ZUZ1bGxDYXB0dXJlKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHRjb25zdCAkdGFibGUgPSAkKCcub3JkZXJzLm92ZXJ2aWV3IHRhYmxlJyk7XHJcblx0XHRcclxuXHRcdCR0YWJsZS5vbignY2xpY2snLCAnLmNoYW5nZS1zdGF0dXMsIC50b29sdGlwLW9yZGVyLXN0YXR1cy1oaXN0b3J5JywgKGV2ZW50KSA9PiB7XHJcblx0XHRcdGNvbnN0ICRyb3cgPSAkKGV2ZW50LnRhcmdldCkuY2xvc2VzdCgndHInKTtcclxuXHRcdFx0Y29uc3Qgb3JkZXJOdW1iZXIgPSAkcm93LmRhdGEoJ2lkJyk7XHJcblx0XHRcdGNvbnN0IG1vZHVsZUNvZGUgPSAkcm93LmRhdGEoJ2dhbWJpb0h1Yk1vZHVsZScpO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKG9yZGVyTnVtYmVyICYmIG1vZHVsZUNvZGUgJiYgbW9kdWxlQ29kZXMuaW5jbHVkZXMobW9kdWxlQ29kZSkpIHtcclxuXHRcdFx0XHRLbGFybmFIdWIuQ29uZmlnLm9yZGVyTnVtYmVyID0gb3JkZXJOdW1iZXI7XHJcblx0XHRcdFx0S2xhcm5hSHViLkNvbmZpZy5tb2R1bGVDb2RlID0gbW9kdWxlQ29kZTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRLbGFybmFIdWIuQ29uZmlnLm9yZGVyTnVtYmVyID0gbnVsbDtcclxuXHRcdFx0XHRLbGFybmFIdWIuQ29uZmlnLm1vZHVsZUNvZGUgPSBudWxsO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0JG1vZGFsLm9uKCdzaG93LmJzLm1vZGFsJywgKCkgPT4gJHNlbGVjdC50cmlnZ2VyKCdjaGFuZ2UnKSk7XHJcblx0XHRcclxuXHRcdCRtb2RhbC5vbignaGlkZS5icy5tb2RhbCcsICgpID0+IHtcclxuXHRcdFx0S2xhcm5hSHViLkNvbmZpZy5vcmRlck51bWJlciA9IG51bGw7XHJcblx0XHRcdEtsYXJuYUh1Yi5Db25maWcubW9kdWxlQ29kZSA9IG51bGw7XHJcblx0XHRcdHNlbGVjdGVkS2xhcm5hSHViT3JkZXJOdW1iZXJzID0gW107XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgJGJ1bGtBY3Rpb24gPSAkKCcuYnVsay1hY3Rpb24nKTtcclxuXHRcdFxyXG5cdFx0JGJ1bGtBY3Rpb24ub24oJ2NsaWNrJywgJy5jaGFuZ2Utc3RhdHVzJywgKCkgPT4ge1xyXG5cdFx0XHRzZWxlY3RlZEtsYXJuYUh1Yk9yZGVyTnVtYmVycyA9IEtsYXJuYUh1Yi5MaWIuZ2V0U2VsZWN0ZWRLbGFybmFIdWJPcmRlck51bWJlcnMoKTtcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0fTtcclxuXHRcclxuXHRLbGFybmFIdWIub24oJ3JlYWR5JywgKCkgPT4gaW5pdCgpKTtcclxufSkoKTsgIl19
