'use strict';

/* --------------------------------------------------------------
 klarna_hub_lib.js 2017-11-02
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * larnaHub Lib Module
 * 
 * Helper methods for KlarnaHub modules. 
 * 
 * @module KlarnaHub.Lib
 */
(function () {
	'use strict';

	/**
  * Legacy pages flag.
  *
  * @type {Boolean}
  */

	var legacy = !$.fn.modal;

	/**
  * Returns URL GET parameter value.
  *
  * @param {String} name Variable name to be returned.
  * @param {String} url URL to be parsed.
  *
  * @return {String}
  *
  * @public
  */
	var getUrlParameter = function getUrlParameter(name, url) {
		if (!url) {
			url = window.location.href;
		}

		name = name.replace(/[\[\]]/g, '\\$&');

		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');

		var results = regex.exec(url);

		if (!results) {
			return null;
		}

		if (!results[2]) {
			return '';
		}

		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	};

	/**
  * Shows message dialog to the user (legacy).
  *
  * This method makes use of the jQuery UI modal component.
  *
  * @param {String} title Dialog title.
  * @param {String} message Dialog message.
  * @param {Object[]} [buttons] Dialog buttons (use jQuery UI dialog format).
  *
  * @return {jQuery} Returns dialog jQuery selector.
  *
  * @private
  */
	var showMessageLegacy = function showMessageLegacy(title, message, buttons) {
		var $dialog = $('<div/>', {
			'html': [$('<div/>', {
				'html': message
			})]
		}).appendTo('body');

		if (!buttons) {
			buttons = [{
				text: KlarnaHub.Config ? KlarnaHub.Config.lang.CLOSE : 'Close',
				click: function click() {
					$dialog.dialog('close').remove();
				}
			}];
		}

		$dialog.dialog({
			autoOpen: true,
			width: 500,
			height: 'auto',
			resizable: false,
			modal: true,
			title: title,
			dialogClass: 'gx-container',
			buttons: buttons
		});

		return $dialog;
	};

	/**
  * Shows message dialog to the user (modern).
  *
  * This method makes use of the Bootstrap modal component.
  *
  * @param {String} title Dialog title.
  * @param {String} message Dialog message.
  * @param {Object[]} [buttons] Dialog buttons (use jQuery UI dialog format).
  *
  * @return {jQuery} Returns dialog jQuery selector.
  *
  * @private
  */
	var showMessageModern = function showMessageModern(title, message, buttons) {
		var html = '<div class="modal fade" tabindex="-1" role="dialog">\n\t\t\t\t\t\t<div class="modal-dialog">\n\t\t\t\t\t\t\t<div class="modal-content">\n\t\t\t\t\t\t\t\t<div class="modal-header">\n\t\t\t\t\t\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close">\n\t\t\t\t\t\t\t\t\t\t<span aria-hidden="true">&times;</span>\n\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t<h4 class="modal-title">' + title + '</h4>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class="modal-body">\n\t\t\t\t\t                ' + message + '\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class="modal-footer"></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>';

		var $modal = $(html).appendTo('body');

		if (!buttons) {
			buttons = [{
				title: KlarnaHub.Config ? KlarnaHub.Config.lang.CLOSE : 'Close',
				class: 'btn btn-default',
				callback: function callback() {
					return $modal.modal('hide');
				}
			}];
		}

		buttons.forEach(function (button) {
			var $button = $('<button/>', {
				'text': button.title,
				'class': button.class || 'btn btn-default'
			}).appendTo($modal.find('.modal-footer'));

			if (button.callback) {
				$button.on('click', button.callback);
			}
		});

		$modal.on('hidden.bs.modal', function () {
			return $modal.remove();
		});

		$modal.modal('show');

		return $modal;
	};

	/**
  * Shows message dialog to the user.
  *
  * @param {String} title Dialog title.
  * @param {String} message Dialog message.
  * @param {Object[]} [buttons] Dialog buttons (use jQuery UI dialog format).
  *
  * @public
  */
	var showMessage = legacy ? showMessageLegacy : showMessageModern;

	/**
  * Handles KlarnaHub related errors.
  *
  * @param {Error} error Error object.
  * 
  * @public 
  */
	var handleError = function handleError(error) {
		if (KlarnaHub.Config && !KlarnaHub.Config.debug) {
			return;
		}

		console.group('KlarnaHub Error');
		console.error(!KlarnaHub.Config ? 'Unexpected error during KlarnaHub initialization.' : 'An unexpected error occurred.');
		console.error(error);
		console.groupEnd();

		showMessage('Klarna', KlarnaHub.Config.lang.UNEXPECTED_REQUEST_ERROR);
	};

	/**
  * Returns selected KlarnaHub order numbers (works only in orders overview). 
  * 
  * @return {Number[]} 
  * 
  * @public
  */
	var getSelectedKlarnaHubOrderNumbers = function getSelectedKlarnaHubOrderNumbers() {
		var $table = $('.orders.overview .table-main');

		if (!$table.length) {
			throw new Error('This method can only be used in the orders overview page.');
		}

		var moduleCodes = ['KlarnaHub', 'KlarnaPaynowHub', 'KlarnaPaylaterHub', 'KlarnaSliceitHub', 'KlarnaBanktransferHub'];

		var selectedKlarnaHubOrders = [];

		$table.find('tbody input:checkbox:checked').each(function (index, checkbox) {
			var _$$parents$data = $(checkbox).parents('tr').data(),
			    id = _$$parents$data.id,
			    gambioHubModule = _$$parents$data.gambioHubModule;

			if (moduleCodes.includes(gambioHubModule)) {
				selectedKlarnaHubOrders.push(id);
			}
		});

		return selectedKlarnaHubOrders;
	};

	// Export
	window.KlarnaHub = window.KlarnaHub || {};
	window.KlarnaHub.Lib = Object.assign({}, {
		getUrlParameter: getUrlParameter,
		showMessage: showMessage,
		handleError: handleError,
		getSelectedKlarnaHubOrderNumbers: getSelectedKlarnaHubOrderNumbers
	}, window.KlarnaHub.Lib);
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIva2xhcm5hX2h1Yl9saWIuanMiXSwibmFtZXMiOlsibGVnYWN5IiwiJCIsImZuIiwibW9kYWwiLCJnZXRVcmxQYXJhbWV0ZXIiLCJuYW1lIiwidXJsIiwid2luZG93IiwibG9jYXRpb24iLCJocmVmIiwicmVwbGFjZSIsInJlZ2V4IiwiUmVnRXhwIiwicmVzdWx0cyIsImV4ZWMiLCJkZWNvZGVVUklDb21wb25lbnQiLCJzaG93TWVzc2FnZUxlZ2FjeSIsInRpdGxlIiwibWVzc2FnZSIsImJ1dHRvbnMiLCIkZGlhbG9nIiwiYXBwZW5kVG8iLCJ0ZXh0IiwiS2xhcm5hSHViIiwiQ29uZmlnIiwibGFuZyIsIkNMT1NFIiwiY2xpY2siLCJkaWFsb2ciLCJyZW1vdmUiLCJhdXRvT3BlbiIsIndpZHRoIiwiaGVpZ2h0IiwicmVzaXphYmxlIiwiZGlhbG9nQ2xhc3MiLCJzaG93TWVzc2FnZU1vZGVybiIsImh0bWwiLCIkbW9kYWwiLCJjbGFzcyIsImNhbGxiYWNrIiwiZm9yRWFjaCIsIiRidXR0b24iLCJidXR0b24iLCJmaW5kIiwib24iLCJzaG93TWVzc2FnZSIsImhhbmRsZUVycm9yIiwiZXJyb3IiLCJkZWJ1ZyIsImNvbnNvbGUiLCJncm91cCIsImdyb3VwRW5kIiwiVU5FWFBFQ1RFRF9SRVFVRVNUX0VSUk9SIiwiZ2V0U2VsZWN0ZWRLbGFybmFIdWJPcmRlck51bWJlcnMiLCIkdGFibGUiLCJsZW5ndGgiLCJFcnJvciIsIm1vZHVsZUNvZGVzIiwic2VsZWN0ZWRLbGFybmFIdWJPcmRlcnMiLCJlYWNoIiwiaW5kZXgiLCJjaGVja2JveCIsInBhcmVudHMiLCJkYXRhIiwiaWQiLCJnYW1iaW9IdWJNb2R1bGUiLCJpbmNsdWRlcyIsInB1c2giLCJMaWIiLCJPYmplY3QiLCJhc3NpZ24iXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7OztBQU9BLENBQUMsWUFBVztBQUNYOztBQUVBOzs7Ozs7QUFLQSxLQUFNQSxTQUFTLENBQUNDLEVBQUVDLEVBQUYsQ0FBS0MsS0FBckI7O0FBRUE7Ozs7Ozs7Ozs7QUFVQSxLQUFNQyxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQUNDLElBQUQsRUFBT0MsR0FBUCxFQUFlO0FBQ3RDLE1BQUksQ0FBQ0EsR0FBTCxFQUFVO0FBQ1RBLFNBQU1DLE9BQU9DLFFBQVAsQ0FBZ0JDLElBQXRCO0FBQ0E7O0FBRURKLFNBQU9BLEtBQUtLLE9BQUwsQ0FBYSxTQUFiLEVBQXdCLE1BQXhCLENBQVA7O0FBRUEsTUFBTUMsUUFBUSxJQUFJQyxNQUFKLENBQVcsU0FBU1AsSUFBVCxHQUFnQixtQkFBM0IsQ0FBZDs7QUFFQSxNQUFNUSxVQUFVRixNQUFNRyxJQUFOLENBQVdSLEdBQVgsQ0FBaEI7O0FBRUEsTUFBSSxDQUFDTyxPQUFMLEVBQWM7QUFDYixVQUFPLElBQVA7QUFDQTs7QUFFRCxNQUFJLENBQUNBLFFBQVEsQ0FBUixDQUFMLEVBQWlCO0FBQ2hCLFVBQU8sRUFBUDtBQUNBOztBQUVELFNBQU9FLG1CQUFtQkYsUUFBUSxDQUFSLEVBQVdILE9BQVgsQ0FBbUIsS0FBbkIsRUFBMEIsR0FBMUIsQ0FBbkIsQ0FBUDtBQUNBLEVBcEJEOztBQXNCQTs7Ozs7Ozs7Ozs7OztBQWFBLEtBQU1NLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQUNDLEtBQUQsRUFBUUMsT0FBUixFQUFpQkMsT0FBakIsRUFBNkI7QUFDdEQsTUFBTUMsVUFBVW5CLEVBQUUsUUFBRixFQUFZO0FBQzNCLFdBQVEsQ0FDUEEsRUFBRSxRQUFGLEVBQVk7QUFDWCxZQUFRaUI7QUFERyxJQUFaLENBRE87QUFEbUIsR0FBWixFQU9kRyxRQVBjLENBT0wsTUFQSyxDQUFoQjs7QUFTQSxNQUFJLENBQUNGLE9BQUwsRUFBYztBQUNiQSxhQUFVLENBQ1Q7QUFDQ0csVUFBTUMsVUFBVUMsTUFBVixHQUFtQkQsVUFBVUMsTUFBVixDQUFpQkMsSUFBakIsQ0FBc0JDLEtBQXpDLEdBQWlELE9BRHhEO0FBRUNDLFdBQU8saUJBQU07QUFDWlAsYUFDRVEsTUFERixDQUNTLE9BRFQsRUFFRUMsTUFGRjtBQUdBO0FBTkYsSUFEUyxDQUFWO0FBVUE7O0FBRURULFVBQVFRLE1BQVIsQ0FBZTtBQUNkRSxhQUFVLElBREk7QUFFZEMsVUFBTyxHQUZPO0FBR2RDLFdBQVEsTUFITTtBQUlkQyxjQUFXLEtBSkc7QUFLZDlCLFVBQU8sSUFMTztBQU1kYyxlQU5jO0FBT2RpQixnQkFBYSxjQVBDO0FBUWRmO0FBUmMsR0FBZjs7QUFXQSxTQUFPQyxPQUFQO0FBQ0EsRUFuQ0Q7O0FBcUNBOzs7Ozs7Ozs7Ozs7O0FBYUEsS0FBTWUsb0JBQW9CLFNBQXBCQSxpQkFBb0IsQ0FBQ2xCLEtBQUQsRUFBUUMsT0FBUixFQUFpQkMsT0FBakIsRUFBNkI7QUFDdEQsTUFBTWlCLHFhQU8yQm5CLEtBUDNCLDJHQVVlQyxPQVZmLDJJQUFOOztBQWlCQSxNQUFNbUIsU0FBU3BDLEVBQUVtQyxJQUFGLEVBQVFmLFFBQVIsQ0FBaUIsTUFBakIsQ0FBZjs7QUFFQSxNQUFJLENBQUNGLE9BQUwsRUFBYztBQUNiQSxhQUFVLENBQ1Q7QUFDQ0YsV0FBT00sVUFBVUMsTUFBVixHQUFtQkQsVUFBVUMsTUFBVixDQUFpQkMsSUFBakIsQ0FBc0JDLEtBQXpDLEdBQWlELE9BRHpEO0FBRUNZLFdBQU8saUJBRlI7QUFHQ0MsY0FBVTtBQUFBLFlBQU1GLE9BQU9sQyxLQUFQLENBQWEsTUFBYixDQUFOO0FBQUE7QUFIWCxJQURTLENBQVY7QUFPQTs7QUFFRGdCLFVBQVFxQixPQUFSLENBQWdCLGtCQUFVO0FBQ3pCLE9BQU1DLFVBQVV4QyxFQUFFLFdBQUYsRUFBZTtBQUM5QixZQUFReUMsT0FBT3pCLEtBRGU7QUFFOUIsYUFBU3lCLE9BQU9KLEtBQVAsSUFBZ0I7QUFGSyxJQUFmLEVBSWRqQixRQUpjLENBSUxnQixPQUFPTSxJQUFQLENBQVksZUFBWixDQUpLLENBQWhCOztBQU1BLE9BQUlELE9BQU9ILFFBQVgsRUFBcUI7QUFDcEJFLFlBQVFHLEVBQVIsQ0FBVyxPQUFYLEVBQW9CRixPQUFPSCxRQUEzQjtBQUNBO0FBQ0QsR0FWRDs7QUFZQUYsU0FBT08sRUFBUCxDQUFVLGlCQUFWLEVBQTZCO0FBQUEsVUFBTVAsT0FBT1IsTUFBUCxFQUFOO0FBQUEsR0FBN0I7O0FBRUFRLFNBQU9sQyxLQUFQLENBQWEsTUFBYjs7QUFFQSxTQUFPa0MsTUFBUDtBQUNBLEVBL0NEOztBQWtEQTs7Ozs7Ozs7O0FBU0EsS0FBTVEsY0FBYzdDLFNBQVNnQixpQkFBVCxHQUE2Qm1CLGlCQUFqRDs7QUFFQTs7Ozs7OztBQU9BLEtBQU1XLGNBQWMsU0FBZEEsV0FBYyxDQUFDQyxLQUFELEVBQVc7QUFDOUIsTUFBSXhCLFVBQVVDLE1BQVYsSUFBb0IsQ0FBQ0QsVUFBVUMsTUFBVixDQUFpQndCLEtBQTFDLEVBQWlEO0FBQ2hEO0FBQ0E7O0FBRURDLFVBQVFDLEtBQVIsQ0FBYyxpQkFBZDtBQUNBRCxVQUFRRixLQUFSLENBQWMsQ0FBQ3hCLFVBQVVDLE1BQVgsR0FBb0IsbURBQXBCLEdBQTBFLCtCQUF4RjtBQUNBeUIsVUFBUUYsS0FBUixDQUFjQSxLQUFkO0FBQ0FFLFVBQVFFLFFBQVI7O0FBRUFOLGNBQVksUUFBWixFQUFzQnRCLFVBQVVDLE1BQVYsQ0FBaUJDLElBQWpCLENBQXNCMkIsd0JBQTVDO0FBQ0EsRUFYRDs7QUFhQTs7Ozs7OztBQU9BLEtBQU1DLG1DQUFtQyxTQUFuQ0EsZ0NBQW1DLEdBQU07QUFDOUMsTUFBTUMsU0FBU3JELEVBQUUsOEJBQUYsQ0FBZjs7QUFFQSxNQUFJLENBQUNxRCxPQUFPQyxNQUFaLEVBQW9CO0FBQ25CLFNBQU0sSUFBSUMsS0FBSixDQUFVLDJEQUFWLENBQU47QUFDQTs7QUFFRCxNQUFNQyxjQUFjLENBQ25CLFdBRG1CLEVBRW5CLGlCQUZtQixFQUduQixtQkFIbUIsRUFJbkIsa0JBSm1CLEVBS25CLHVCQUxtQixDQUFwQjs7QUFRQSxNQUFNQywwQkFBMEIsRUFBaEM7O0FBRUFKLFNBQU9YLElBQVAsQ0FBWSw4QkFBWixFQUE0Q2dCLElBQTVDLENBQWlELFVBQUNDLEtBQUQsRUFBUUMsUUFBUixFQUFxQjtBQUFBLHlCQUN2QzVELEVBQUU0RCxRQUFGLEVBQVlDLE9BQVosQ0FBb0IsSUFBcEIsRUFBMEJDLElBQTFCLEVBRHVDO0FBQUEsT0FDOURDLEVBRDhELG1CQUM5REEsRUFEOEQ7QUFBQSxPQUMxREMsZUFEMEQsbUJBQzFEQSxlQUQwRDs7QUFHckUsT0FBSVIsWUFBWVMsUUFBWixDQUFxQkQsZUFBckIsQ0FBSixFQUEyQztBQUMxQ1AsNEJBQXdCUyxJQUF4QixDQUE2QkgsRUFBN0I7QUFDQTtBQUNELEdBTkQ7O0FBUUEsU0FBT04sdUJBQVA7QUFDQSxFQTFCRDs7QUE0QkE7QUFDQW5ELFFBQU9nQixTQUFQLEdBQW1CaEIsT0FBT2dCLFNBQVAsSUFBb0IsRUFBdkM7QUFDQWhCLFFBQU9nQixTQUFQLENBQWlCNkMsR0FBakIsR0FBdUJDLE9BQU9DLE1BQVAsQ0FBYyxFQUFkLEVBQWtCO0FBQ3hDbEUsa0NBRHdDO0FBRXhDeUMsMEJBRndDO0FBR3hDQywwQkFId0M7QUFJeENPO0FBSndDLEVBQWxCLEVBS3BCOUMsT0FBT2dCLFNBQVAsQ0FBaUI2QyxHQUxHLENBQXZCO0FBTUEsQ0FyT0QiLCJmaWxlIjoiQWRtaW4vSmF2YXNjcmlwdC9leHRlbmRlcnMva2xhcm5hX2h1Yi9rbGFybmFfaHViX2xpYi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBrbGFybmFfaHViX2xpYi5qcyAyMDE3LTExLTAyXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIGxhcm5hSHViIExpYiBNb2R1bGVcclxuICogXHJcbiAqIEhlbHBlciBtZXRob2RzIGZvciBLbGFybmFIdWIgbW9kdWxlcy4gXHJcbiAqIFxyXG4gKiBAbW9kdWxlIEtsYXJuYUh1Yi5MaWJcclxuICovXHJcbihmdW5jdGlvbigpIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTGVnYWN5IHBhZ2VzIGZsYWcuXHJcblx0ICpcclxuXHQgKiBAdHlwZSB7Qm9vbGVhbn1cclxuXHQgKi9cclxuXHRjb25zdCBsZWdhY3kgPSAhJC5mbi5tb2RhbDtcclxuXHRcclxuXHQvKipcclxuXHQgKiBSZXR1cm5zIFVSTCBHRVQgcGFyYW1ldGVyIHZhbHVlLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgVmFyaWFibGUgbmFtZSB0byBiZSByZXR1cm5lZC5cclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gdXJsIFVSTCB0byBiZSBwYXJzZWQuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtTdHJpbmd9XHJcblx0ICpcclxuXHQgKiBAcHVibGljXHJcblx0ICovXHJcblx0Y29uc3QgZ2V0VXJsUGFyYW1ldGVyID0gKG5hbWUsIHVybCkgPT4ge1xyXG5cdFx0aWYgKCF1cmwpIHtcclxuXHRcdFx0dXJsID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdG5hbWUgPSBuYW1lLnJlcGxhY2UoL1tcXFtcXF1dL2csICdcXFxcJCYnKTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgcmVnZXggPSBuZXcgUmVnRXhwKCdbPyZdJyArIG5hbWUgKyAnKD0oW14mI10qKXwmfCN8JCknKTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgcmVzdWx0cyA9IHJlZ2V4LmV4ZWModXJsKTtcclxuXHRcdFxyXG5cdFx0aWYgKCFyZXN1bHRzKSB7XHJcblx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRpZiAoIXJlc3VsdHNbMl0pIHtcclxuXHRcdFx0cmV0dXJuICcnO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRyZXR1cm4gZGVjb2RlVVJJQ29tcG9uZW50KHJlc3VsdHNbMl0ucmVwbGFjZSgvXFwrL2csICcgJykpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2hvd3MgbWVzc2FnZSBkaWFsb2cgdG8gdGhlIHVzZXIgKGxlZ2FjeSkuXHJcblx0ICpcclxuXHQgKiBUaGlzIG1ldGhvZCBtYWtlcyB1c2Ugb2YgdGhlIGpRdWVyeSBVSSBtb2RhbCBjb21wb25lbnQuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gdGl0bGUgRGlhbG9nIHRpdGxlLlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlIERpYWxvZyBtZXNzYWdlLlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0W119IFtidXR0b25zXSBEaWFsb2cgYnV0dG9ucyAodXNlIGpRdWVyeSBVSSBkaWFsb2cgZm9ybWF0KS5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm4ge2pRdWVyeX0gUmV0dXJucyBkaWFsb2cgalF1ZXJ5IHNlbGVjdG9yLlxyXG5cdCAqXHJcblx0ICogQHByaXZhdGVcclxuXHQgKi9cclxuXHRjb25zdCBzaG93TWVzc2FnZUxlZ2FjeSA9ICh0aXRsZSwgbWVzc2FnZSwgYnV0dG9ucykgPT4ge1xyXG5cdFx0Y29uc3QgJGRpYWxvZyA9ICQoJzxkaXYvPicsIHtcclxuXHRcdFx0J2h0bWwnOiBbXHJcblx0XHRcdFx0JCgnPGRpdi8+Jywge1xyXG5cdFx0XHRcdFx0J2h0bWwnOiBtZXNzYWdlXHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XVxyXG5cdFx0fSlcclxuXHRcdFx0LmFwcGVuZFRvKCdib2R5Jyk7XHJcblx0XHRcclxuXHRcdGlmICghYnV0dG9ucykge1xyXG5cdFx0XHRidXR0b25zID0gW1xyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdHRleHQ6IEtsYXJuYUh1Yi5Db25maWcgPyBLbGFybmFIdWIuQ29uZmlnLmxhbmcuQ0xPU0UgOiAnQ2xvc2UnLFxyXG5cdFx0XHRcdFx0Y2xpY2s6ICgpID0+IHtcclxuXHRcdFx0XHRcdFx0JGRpYWxvZ1xyXG5cdFx0XHRcdFx0XHRcdC5kaWFsb2coJ2Nsb3NlJylcclxuXHRcdFx0XHRcdFx0XHQucmVtb3ZlKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRdO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQkZGlhbG9nLmRpYWxvZyh7XHJcblx0XHRcdGF1dG9PcGVuOiB0cnVlLFxyXG5cdFx0XHR3aWR0aDogNTAwLFxyXG5cdFx0XHRoZWlnaHQ6ICdhdXRvJyxcclxuXHRcdFx0cmVzaXphYmxlOiBmYWxzZSxcclxuXHRcdFx0bW9kYWw6IHRydWUsXHJcblx0XHRcdHRpdGxlLFxyXG5cdFx0XHRkaWFsb2dDbGFzczogJ2d4LWNvbnRhaW5lcicsXHJcblx0XHRcdGJ1dHRvbnNcclxuXHRcdH0pO1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gJGRpYWxvZztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNob3dzIG1lc3NhZ2UgZGlhbG9nIHRvIHRoZSB1c2VyIChtb2Rlcm4pLlxyXG5cdCAqXHJcblx0ICogVGhpcyBtZXRob2QgbWFrZXMgdXNlIG9mIHRoZSBCb290c3RyYXAgbW9kYWwgY29tcG9uZW50LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IHRpdGxlIERpYWxvZyB0aXRsZS5cclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBEaWFsb2cgbWVzc2FnZS5cclxuXHQgKiBAcGFyYW0ge09iamVjdFtdfSBbYnV0dG9uc10gRGlhbG9nIGJ1dHRvbnMgKHVzZSBqUXVlcnkgVUkgZGlhbG9nIGZvcm1hdCkuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtqUXVlcnl9IFJldHVybnMgZGlhbG9nIGpRdWVyeSBzZWxlY3Rvci5cclxuXHQgKlxyXG5cdCAqIEBwcml2YXRlXHJcblx0ICovXHJcblx0Y29uc3Qgc2hvd01lc3NhZ2VNb2Rlcm4gPSAodGl0bGUsIG1lc3NhZ2UsIGJ1dHRvbnMpID0+IHtcclxuXHRcdGNvbnN0IGh0bWwgPSBgPGRpdiBjbGFzcz1cIm1vZGFsIGZhZGVcIiB0YWJpbmRleD1cIi0xXCIgcm9sZT1cImRpYWxvZ1wiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibW9kYWwtZGlhbG9nXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjbG9zZVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCIgYXJpYS1sYWJlbD1cIkNsb3NlXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4gYXJpYS1oaWRkZW49XCJ0cnVlXCI+JnRpbWVzOzwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9idXR0b24+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCI+JHt0aXRsZX08L2g0PlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxyXG5cdFx0XHRcdFx0ICAgICAgICAgICAgICAgICR7bWVzc2FnZX1cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDwvZGl2PmA7XHJcblx0XHRcclxuXHRcdGNvbnN0ICRtb2RhbCA9ICQoaHRtbCkuYXBwZW5kVG8oJ2JvZHknKTtcclxuXHRcdFxyXG5cdFx0aWYgKCFidXR0b25zKSB7XHJcblx0XHRcdGJ1dHRvbnMgPSBbXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0dGl0bGU6IEtsYXJuYUh1Yi5Db25maWcgPyBLbGFybmFIdWIuQ29uZmlnLmxhbmcuQ0xPU0UgOiAnQ2xvc2UnLFxyXG5cdFx0XHRcdFx0Y2xhc3M6ICdidG4gYnRuLWRlZmF1bHQnLFxyXG5cdFx0XHRcdFx0Y2FsbGJhY2s6ICgpID0+ICRtb2RhbC5tb2RhbCgnaGlkZScpXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRdO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRidXR0b25zLmZvckVhY2goYnV0dG9uID0+IHtcclxuXHRcdFx0Y29uc3QgJGJ1dHRvbiA9ICQoJzxidXR0b24vPicsIHtcclxuXHRcdFx0XHQndGV4dCc6IGJ1dHRvbi50aXRsZSxcclxuXHRcdFx0XHQnY2xhc3MnOiBidXR0b24uY2xhc3MgfHwgJ2J0biBidG4tZGVmYXVsdCdcclxuXHRcdFx0fSlcclxuXHRcdFx0XHQuYXBwZW5kVG8oJG1vZGFsLmZpbmQoJy5tb2RhbC1mb290ZXInKSk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoYnV0dG9uLmNhbGxiYWNrKSB7XHJcblx0XHRcdFx0JGJ1dHRvbi5vbignY2xpY2snLCBidXR0b24uY2FsbGJhY2spO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0JG1vZGFsLm9uKCdoaWRkZW4uYnMubW9kYWwnLCAoKSA9PiAkbW9kYWwucmVtb3ZlKCkpO1xyXG5cdFx0XHJcblx0XHQkbW9kYWwubW9kYWwoJ3Nob3cnKTtcclxuXHRcdFxyXG5cdFx0cmV0dXJuICRtb2RhbDtcclxuXHR9O1xyXG5cdFxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNob3dzIG1lc3NhZ2UgZGlhbG9nIHRvIHRoZSB1c2VyLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IHRpdGxlIERpYWxvZyB0aXRsZS5cclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBEaWFsb2cgbWVzc2FnZS5cclxuXHQgKiBAcGFyYW0ge09iamVjdFtdfSBbYnV0dG9uc10gRGlhbG9nIGJ1dHRvbnMgKHVzZSBqUXVlcnkgVUkgZGlhbG9nIGZvcm1hdCkuXHJcblx0ICpcclxuXHQgKiBAcHVibGljXHJcblx0ICovXHJcblx0Y29uc3Qgc2hvd01lc3NhZ2UgPSBsZWdhY3kgPyBzaG93TWVzc2FnZUxlZ2FjeSA6IHNob3dNZXNzYWdlTW9kZXJuO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEhhbmRsZXMgS2xhcm5hSHViIHJlbGF0ZWQgZXJyb3JzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtFcnJvcn0gZXJyb3IgRXJyb3Igb2JqZWN0LlxyXG5cdCAqIFxyXG5cdCAqIEBwdWJsaWMgXHJcblx0ICovXHJcblx0Y29uc3QgaGFuZGxlRXJyb3IgPSAoZXJyb3IpID0+IHtcclxuXHRcdGlmIChLbGFybmFIdWIuQ29uZmlnICYmICFLbGFybmFIdWIuQ29uZmlnLmRlYnVnKSB7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Y29uc29sZS5ncm91cCgnS2xhcm5hSHViIEVycm9yJyk7XHJcblx0XHRjb25zb2xlLmVycm9yKCFLbGFybmFIdWIuQ29uZmlnID8gJ1VuZXhwZWN0ZWQgZXJyb3IgZHVyaW5nIEtsYXJuYUh1YiBpbml0aWFsaXphdGlvbi4nIDogJ0FuIHVuZXhwZWN0ZWQgZXJyb3Igb2NjdXJyZWQuJyk7XHJcblx0XHRjb25zb2xlLmVycm9yKGVycm9yKTtcclxuXHRcdGNvbnNvbGUuZ3JvdXBFbmQoKTtcclxuXHRcdFxyXG5cdFx0c2hvd01lc3NhZ2UoJ0tsYXJuYScsIEtsYXJuYUh1Yi5Db25maWcubGFuZy5VTkVYUEVDVEVEX1JFUVVFU1RfRVJST1IpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogUmV0dXJucyBzZWxlY3RlZCBLbGFybmFIdWIgb3JkZXIgbnVtYmVycyAod29ya3Mgb25seSBpbiBvcmRlcnMgb3ZlcnZpZXcpLiBcclxuXHQgKiBcclxuXHQgKiBAcmV0dXJuIHtOdW1iZXJbXX0gXHJcblx0ICogXHJcblx0ICogQHB1YmxpY1xyXG5cdCAqL1xyXG5cdGNvbnN0IGdldFNlbGVjdGVkS2xhcm5hSHViT3JkZXJOdW1iZXJzID0gKCkgPT4ge1xyXG5cdFx0Y29uc3QgJHRhYmxlID0gJCgnLm9yZGVycy5vdmVydmlldyAudGFibGUtbWFpbicpOyBcclxuXHRcdFxyXG5cdFx0aWYgKCEkdGFibGUubGVuZ3RoKSB7XHJcblx0XHRcdHRocm93IG5ldyBFcnJvcignVGhpcyBtZXRob2QgY2FuIG9ubHkgYmUgdXNlZCBpbiB0aGUgb3JkZXJzIG92ZXJ2aWV3IHBhZ2UuJyk7IFxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRjb25zdCBtb2R1bGVDb2RlcyA9IFtcclxuXHRcdFx0J0tsYXJuYUh1YicsXHJcblx0XHRcdCdLbGFybmFQYXlub3dIdWInLFxyXG5cdFx0XHQnS2xhcm5hUGF5bGF0ZXJIdWInLFxyXG5cdFx0XHQnS2xhcm5hU2xpY2VpdEh1YicsXHJcblx0XHRcdCdLbGFybmFCYW5rdHJhbnNmZXJIdWInXHJcblx0XHRdOyBcclxuXHRcdFxyXG5cdFx0Y29uc3Qgc2VsZWN0ZWRLbGFybmFIdWJPcmRlcnMgPSBbXTtcclxuXHRcdFxyXG5cdFx0JHRhYmxlLmZpbmQoJ3Rib2R5IGlucHV0OmNoZWNrYm94OmNoZWNrZWQnKS5lYWNoKChpbmRleCwgY2hlY2tib3gpID0+IHtcclxuXHRcdFx0Y29uc3Qge2lkLCBnYW1iaW9IdWJNb2R1bGV9ID0gJChjaGVja2JveCkucGFyZW50cygndHInKS5kYXRhKCk7IFxyXG5cdFx0XHRcclxuXHRcdFx0aWYgKG1vZHVsZUNvZGVzLmluY2x1ZGVzKGdhbWJpb0h1Yk1vZHVsZSkpIHtcclxuXHRcdFx0XHRzZWxlY3RlZEtsYXJuYUh1Yk9yZGVycy5wdXNoKGlkKTtcdFxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0cmV0dXJuIHNlbGVjdGVkS2xhcm5hSHViT3JkZXJzO1xyXG5cdH1cclxuXHRcclxuXHQvLyBFeHBvcnRcclxuXHR3aW5kb3cuS2xhcm5hSHViID0gd2luZG93LktsYXJuYUh1YiB8fCB7fTtcclxuXHR3aW5kb3cuS2xhcm5hSHViLkxpYiA9IE9iamVjdC5hc3NpZ24oe30sIHtcclxuXHRcdGdldFVybFBhcmFtZXRlcixcclxuXHRcdHNob3dNZXNzYWdlLFxyXG5cdFx0aGFuZGxlRXJyb3IsXHJcblx0XHRnZXRTZWxlY3RlZEtsYXJuYUh1Yk9yZGVyTnVtYmVyc1xyXG5cdH0sIHdpbmRvdy5LbGFybmFIdWIuTGliKTtcclxufSkoKTsiXX0=
