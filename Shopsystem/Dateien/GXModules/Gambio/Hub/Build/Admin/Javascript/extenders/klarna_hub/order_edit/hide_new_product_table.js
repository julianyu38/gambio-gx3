'use strict';

/* --------------------------------------------------------------
 hide_new_product_table.js 2017-11-03
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Hides the new product table for KlarnaHub orders.
 */
(function () {
  'use strict';

  // Hide the table directory (no need to wait till the page has finished loading).

  document.querySelector('.orders-edit-table:last-of-type').classList.add('hidden');
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZWRpdC9oaWRlX25ld19wcm9kdWN0X3RhYmxlLmpzIl0sIm5hbWVzIjpbImRvY3VtZW50IiwicXVlcnlTZWxlY3RvciIsImNsYXNzTGlzdCIsImFkZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7QUFHQSxDQUFDLFlBQVc7QUFDWDs7QUFFQTs7QUFDQUEsV0FBU0MsYUFBVCxDQUF1QixpQ0FBdkIsRUFBMERDLFNBQTFELENBQW9FQyxHQUFwRSxDQUF3RSxRQUF4RTtBQUNBLENBTEQiLCJmaWxlIjoiQWRtaW4vSmF2YXNjcmlwdC9leHRlbmRlcnMva2xhcm5hX2h1Yi9vcmRlcl9lZGl0L2hpZGVfbmV3X3Byb2R1Y3RfdGFibGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gaGlkZV9uZXdfcHJvZHVjdF90YWJsZS5qcyAyMDE3LTExLTAzXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxNyBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIEhpZGVzIHRoZSBuZXcgcHJvZHVjdCB0YWJsZSBmb3IgS2xhcm5hSHViIG9yZGVycy5cclxuICovXHJcbihmdW5jdGlvbigpIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0Ly8gSGlkZSB0aGUgdGFibGUgZGlyZWN0b3J5IChubyBuZWVkIHRvIHdhaXQgdGlsbCB0aGUgcGFnZSBoYXMgZmluaXNoZWQgbG9hZGluZykuXHJcblx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm9yZGVycy1lZGl0LXRhYmxlOmxhc3Qtb2YtdHlwZScpLmNsYXNzTGlzdC5hZGQoJ2hpZGRlbicpO1xyXG59KSgpOyJdfQ==
