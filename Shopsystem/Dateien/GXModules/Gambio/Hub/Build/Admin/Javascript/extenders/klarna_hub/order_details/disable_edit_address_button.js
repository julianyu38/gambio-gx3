'use strict';

/* --------------------------------------------------------------
 disable_edit_address_button.js 2017-11-02
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Disables the order address edit button.
 */
(function () {
	'use strict';

	/**
  * Initializes the module.
  * 
  * @private
  */

	var init = function init() {
		var $links = $('.frame-head .head-link a');

		$links.each(function (index, link) {
			var $link = $(link);

			if (!$link.attr('href').includes('edit_action=address')) {
				return true;
			}

			$link.parent().append($('<span/>', {
				'text': $link.text().trim()
			})).css({
				'opacity': .6,
				'color': 'gray',
				'background': 'none',
				'cursor': 'not-allowed'
			});

			$link.remove();

			return false;
		});
	};

	KlarnaHub.on('ready', function () {
		return init();
	});
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZGV0YWlscy9kaXNhYmxlX2VkaXRfYWRkcmVzc19idXR0b24uanMiXSwibmFtZXMiOlsiaW5pdCIsIiRsaW5rcyIsIiQiLCJlYWNoIiwiaW5kZXgiLCJsaW5rIiwiJGxpbmsiLCJhdHRyIiwiaW5jbHVkZXMiLCJwYXJlbnQiLCJhcHBlbmQiLCJ0ZXh0IiwidHJpbSIsImNzcyIsInJlbW92ZSIsIktsYXJuYUh1YiIsIm9uIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7OztBQUdBLENBQUMsWUFBVztBQUNYOztBQUVBOzs7Ozs7QUFLQSxLQUFNQSxPQUFPLFNBQVBBLElBQU8sR0FBTTtBQUNsQixNQUFNQyxTQUFTQyxFQUFFLDBCQUFGLENBQWY7O0FBRUFELFNBQU9FLElBQVAsQ0FBWSxVQUFDQyxLQUFELEVBQVFDLElBQVIsRUFBaUI7QUFDNUIsT0FBTUMsUUFBUUosRUFBRUcsSUFBRixDQUFkOztBQUVBLE9BQUksQ0FBQ0MsTUFBTUMsSUFBTixDQUFXLE1BQVgsRUFBbUJDLFFBQW5CLENBQTRCLHFCQUE1QixDQUFMLEVBQXlEO0FBQ3hELFdBQU8sSUFBUDtBQUNBOztBQUVERixTQUNFRyxNQURGLEdBRUVDLE1BRkYsQ0FHRVIsRUFBRSxTQUFGLEVBQWE7QUFDWixZQUFRSSxNQUFNSyxJQUFOLEdBQWFDLElBQWI7QUFESSxJQUFiLENBSEYsRUFPRUMsR0FQRixDQU9NO0FBQ0osZUFBVyxFQURQO0FBRUosYUFBUyxNQUZMO0FBR0osa0JBQWMsTUFIVjtBQUlKLGNBQVU7QUFKTixJQVBOOztBQWNBUCxTQUFNUSxNQUFOOztBQUVBLFVBQU8sS0FBUDtBQUNBLEdBeEJEO0FBeUJBLEVBNUJEOztBQThCQUMsV0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBc0I7QUFBQSxTQUFNaEIsTUFBTjtBQUFBLEVBQXRCO0FBQ0EsQ0F2Q0QiLCJmaWxlIjoiQWRtaW4vSmF2YXNjcmlwdC9leHRlbmRlcnMva2xhcm5hX2h1Yi9vcmRlcl9kZXRhaWxzL2Rpc2FibGVfZWRpdF9hZGRyZXNzX2J1dHRvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBkaXNhYmxlX2VkaXRfYWRkcmVzc19idXR0b24uanMgMjAxNy0xMS0wMlxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBEaXNhYmxlcyB0aGUgb3JkZXIgYWRkcmVzcyBlZGl0IGJ1dHRvbi5cclxuICovXHJcbihmdW5jdGlvbigpIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0LyoqXHJcblx0ICogSW5pdGlhbGl6ZXMgdGhlIG1vZHVsZS5cclxuXHQgKiBcclxuXHQgKiBAcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdGNvbnN0IGluaXQgPSAoKSA9PiB7XHJcblx0XHRjb25zdCAkbGlua3MgPSAkKCcuZnJhbWUtaGVhZCAuaGVhZC1saW5rIGEnKTtcclxuXHRcdFxyXG5cdFx0JGxpbmtzLmVhY2goKGluZGV4LCBsaW5rKSA9PiB7XHJcblx0XHRcdGNvbnN0ICRsaW5rID0gJChsaW5rKTsgXHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoISRsaW5rLmF0dHIoJ2hyZWYnKS5pbmNsdWRlcygnZWRpdF9hY3Rpb249YWRkcmVzcycpKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdCRsaW5rXHJcblx0XHRcdFx0LnBhcmVudCgpXHJcblx0XHRcdFx0LmFwcGVuZChcclxuXHRcdFx0XHRcdCQoJzxzcGFuLz4nLCB7XHJcblx0XHRcdFx0XHRcdCd0ZXh0JzogJGxpbmsudGV4dCgpLnRyaW0oKVxyXG5cdFx0XHRcdFx0fSlcclxuXHRcdFx0XHQpXHJcblx0XHRcdFx0LmNzcyh7XHJcblx0XHRcdFx0XHQnb3BhY2l0eSc6IC42LFxyXG5cdFx0XHRcdFx0J2NvbG9yJzogJ2dyYXknLFxyXG5cdFx0XHRcdFx0J2JhY2tncm91bmQnOiAnbm9uZScsXHJcblx0XHRcdFx0XHQnY3Vyc29yJzogJ25vdC1hbGxvd2VkJ1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcclxuXHRcdFx0JGxpbmsucmVtb3ZlKCk7XHJcblx0XHRcdFxyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9KTtcclxuXHR9O1xyXG5cdFxyXG5cdEtsYXJuYUh1Yi5vbigncmVhZHknLCAoKSA9PiBpbml0KCkpO1xyXG59KSgpOyAiXX0=
