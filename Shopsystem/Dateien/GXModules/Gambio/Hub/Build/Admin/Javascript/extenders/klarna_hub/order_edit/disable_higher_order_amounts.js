'use strict';

/* --------------------------------------------------------------
 disable_higher_order_amounts.js 2017-11-08
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Disable higher order amounts of order edit form.
 */
(function () {
	'use strict';

	/**
  * Initializes the module.
  */

	var init = function init() {
		var $form = $('[name="product_edit"]');

		$form.each(function (index, form) {
			var $form = $(form);

			$form.data('originalValues', {
				index: index,
				products_quantity: $form.find('[name="products_quantity"]').val(),
				products_tax: $form.find('[name="products_tax"]').val(),
				products_price: $form.find('[name="products_price"]').val()
			});
		});

		var $target = $('[name="products_quantity"], [name="products_tax"], [name="products_price"]');

		$target.on('keyup keypress', function (event) {
			var keyCode = event.keyCode || event.which;

			if (keyCode === 13) {
				event.preventDefault();
				return false;
			}
		}).on('change', function (event) {
			var $input = $(event.target);

			var originalValues = $input.closest('form').data('originalValues');

			var fieldName = $input.attr('name');

			var originalValue = originalValues[fieldName];

			if (isNaN(originalValue)) {
				return;
			}

			if (Number($input.val()) > Number(originalValue)) {
				KlarnaHub.Lib.showMessage('Klarna', KlarnaHub.Config.lang.ONLY_LOWER_AMOUNTS_ARE_ALLOWED);
				event.target.value = originalValue;
			}
		});
	};

	KlarnaHub.on('ready', function () {
		return init();
	});
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZWRpdC9kaXNhYmxlX2hpZ2hlcl9vcmRlcl9hbW91bnRzLmpzIl0sIm5hbWVzIjpbImluaXQiLCIkZm9ybSIsIiQiLCJlYWNoIiwiaW5kZXgiLCJmb3JtIiwiZGF0YSIsInByb2R1Y3RzX3F1YW50aXR5IiwiZmluZCIsInZhbCIsInByb2R1Y3RzX3RheCIsInByb2R1Y3RzX3ByaWNlIiwiJHRhcmdldCIsIm9uIiwiZXZlbnQiLCJrZXlDb2RlIiwid2hpY2giLCJwcmV2ZW50RGVmYXVsdCIsIiRpbnB1dCIsInRhcmdldCIsIm9yaWdpbmFsVmFsdWVzIiwiY2xvc2VzdCIsImZpZWxkTmFtZSIsImF0dHIiLCJvcmlnaW5hbFZhbHVlIiwiaXNOYU4iLCJOdW1iZXIiLCJLbGFybmFIdWIiLCJMaWIiLCJzaG93TWVzc2FnZSIsIkNvbmZpZyIsImxhbmciLCJPTkxZX0xPV0VSX0FNT1VOVFNfQVJFX0FMTE9XRUQiLCJ2YWx1ZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBOzs7QUFHQSxDQUFDLFlBQVc7QUFDWDs7QUFFQTs7OztBQUdBLEtBQU1BLE9BQU8sU0FBUEEsSUFBTyxHQUFNO0FBQ2xCLE1BQU1DLFFBQVFDLEVBQUUsdUJBQUYsQ0FBZDs7QUFFQUQsUUFBTUUsSUFBTixDQUFXLFVBQUNDLEtBQUQsRUFBUUMsSUFBUixFQUFpQjtBQUMzQixPQUFNSixRQUFRQyxFQUFFRyxJQUFGLENBQWQ7O0FBRUFKLFNBQU1LLElBQU4sQ0FBVyxnQkFBWCxFQUE2QjtBQUM1QkYsZ0JBRDRCO0FBRTVCRyx1QkFBbUJOLE1BQU1PLElBQU4sQ0FBVyw0QkFBWCxFQUF5Q0MsR0FBekMsRUFGUztBQUc1QkMsa0JBQWNULE1BQU1PLElBQU4sQ0FBVyx1QkFBWCxFQUFvQ0MsR0FBcEMsRUFIYztBQUk1QkUsb0JBQWdCVixNQUFNTyxJQUFOLENBQVcseUJBQVgsRUFBc0NDLEdBQXRDO0FBSlksSUFBN0I7QUFNQSxHQVREOztBQVdBLE1BQU1HLFVBQVVWLEVBQUUsNEVBQUYsQ0FBaEI7O0FBRUFVLFVBQ0VDLEVBREYsQ0FDSyxnQkFETCxFQUN1QixVQUFDQyxLQUFELEVBQVc7QUFDaEMsT0FBTUMsVUFBVUQsTUFBTUMsT0FBTixJQUFpQkQsTUFBTUUsS0FBdkM7O0FBRUEsT0FBSUQsWUFBWSxFQUFoQixFQUFvQjtBQUNuQkQsVUFBTUcsY0FBTjtBQUNBLFdBQU8sS0FBUDtBQUNBO0FBQ0QsR0FSRixFQVNFSixFQVRGLENBU0ssUUFUTCxFQVNlLFVBQUNDLEtBQUQsRUFBVztBQUN4QixPQUFNSSxTQUFTaEIsRUFBRVksTUFBTUssTUFBUixDQUFmOztBQUVBLE9BQU1DLGlCQUFpQkYsT0FBT0csT0FBUCxDQUFlLE1BQWYsRUFBdUJmLElBQXZCLENBQTRCLGdCQUE1QixDQUF2Qjs7QUFFQSxPQUFNZ0IsWUFBWUosT0FBT0ssSUFBUCxDQUFZLE1BQVosQ0FBbEI7O0FBRUEsT0FBTUMsZ0JBQWdCSixlQUFlRSxTQUFmLENBQXRCOztBQUVBLE9BQUlHLE1BQU1ELGFBQU4sQ0FBSixFQUEwQjtBQUN6QjtBQUNBOztBQUVELE9BQUlFLE9BQU9SLE9BQU9ULEdBQVAsRUFBUCxJQUF1QmlCLE9BQU9GLGFBQVAsQ0FBM0IsRUFBa0Q7QUFDakRHLGNBQVVDLEdBQVYsQ0FBY0MsV0FBZCxDQUEwQixRQUExQixFQUFvQ0YsVUFBVUcsTUFBVixDQUFpQkMsSUFBakIsQ0FBc0JDLDhCQUExRDtBQUNBbEIsVUFBTUssTUFBTixDQUFhYyxLQUFiLEdBQXFCVCxhQUFyQjtBQUNBO0FBQ0QsR0ExQkY7QUEyQkEsRUEzQ0Q7O0FBNkNBRyxXQUFVZCxFQUFWLENBQWEsT0FBYixFQUFzQjtBQUFBLFNBQU1iLE1BQU47QUFBQSxFQUF0QjtBQUNBLENBcEREIiwiZmlsZSI6IkFkbWluL0phdmFzY3JpcHQvZXh0ZW5kZXJzL2tsYXJuYV9odWIvb3JkZXJfZWRpdC9kaXNhYmxlX2hpZ2hlcl9vcmRlcl9hbW91bnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIGRpc2FibGVfaGlnaGVyX29yZGVyX2Ftb3VudHMuanMgMjAxNy0xMS0wOFxyXG4gR2FtYmlvIEdtYkhcclxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTcgR2FtYmlvIEdtYkhcclxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxyXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBEaXNhYmxlIGhpZ2hlciBvcmRlciBhbW91bnRzIG9mIG9yZGVyIGVkaXQgZm9ybS5cclxuICovXHJcbihmdW5jdGlvbigpIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0LyoqXHJcblx0ICogSW5pdGlhbGl6ZXMgdGhlIG1vZHVsZS5cclxuXHQgKi9cclxuXHRjb25zdCBpbml0ID0gKCkgPT4ge1xyXG5cdFx0Y29uc3QgJGZvcm0gPSAkKCdbbmFtZT1cInByb2R1Y3RfZWRpdFwiXScpO1xyXG5cdFx0XHJcblx0XHQkZm9ybS5lYWNoKChpbmRleCwgZm9ybSkgPT4ge1xyXG5cdFx0XHRjb25zdCAkZm9ybSA9ICQoZm9ybSk7XHJcblx0XHRcdFxyXG5cdFx0XHQkZm9ybS5kYXRhKCdvcmlnaW5hbFZhbHVlcycsIHtcclxuXHRcdFx0XHRpbmRleCxcclxuXHRcdFx0XHRwcm9kdWN0c19xdWFudGl0eTogJGZvcm0uZmluZCgnW25hbWU9XCJwcm9kdWN0c19xdWFudGl0eVwiXScpLnZhbCgpLFxyXG5cdFx0XHRcdHByb2R1Y3RzX3RheDogJGZvcm0uZmluZCgnW25hbWU9XCJwcm9kdWN0c190YXhcIl0nKS52YWwoKSxcclxuXHRcdFx0XHRwcm9kdWN0c19wcmljZTogJGZvcm0uZmluZCgnW25hbWU9XCJwcm9kdWN0c19wcmljZVwiXScpLnZhbCgpXHJcblx0XHRcdH0pO1xyXG5cdFx0fSk7XHJcblx0XHRcclxuXHRcdGNvbnN0ICR0YXJnZXQgPSAkKCdbbmFtZT1cInByb2R1Y3RzX3F1YW50aXR5XCJdLCBbbmFtZT1cInByb2R1Y3RzX3RheFwiXSwgW25hbWU9XCJwcm9kdWN0c19wcmljZVwiXScpO1xyXG5cdFx0XHJcblx0XHQkdGFyZ2V0XHJcblx0XHRcdC5vbigna2V5dXAga2V5cHJlc3MnLCAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRjb25zdCBrZXlDb2RlID0gZXZlbnQua2V5Q29kZSB8fCBldmVudC53aGljaDtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoa2V5Q29kZSA9PT0gMTMpIHtcclxuXHRcdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdFx0XHQub24oJ2NoYW5nZScsIChldmVudCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0ICRpbnB1dCA9ICQoZXZlbnQudGFyZ2V0KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRjb25zdCBvcmlnaW5hbFZhbHVlcyA9ICRpbnB1dC5jbG9zZXN0KCdmb3JtJykuZGF0YSgnb3JpZ2luYWxWYWx1ZXMnKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRjb25zdCBmaWVsZE5hbWUgPSAkaW5wdXQuYXR0cignbmFtZScpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGNvbnN0IG9yaWdpbmFsVmFsdWUgPSBvcmlnaW5hbFZhbHVlc1tmaWVsZE5hbWVdO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChpc05hTihvcmlnaW5hbFZhbHVlKSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoTnVtYmVyKCRpbnB1dC52YWwoKSkgPiBOdW1iZXIob3JpZ2luYWxWYWx1ZSkpIHtcclxuXHRcdFx0XHRcdEtsYXJuYUh1Yi5MaWIuc2hvd01lc3NhZ2UoJ0tsYXJuYScsIEtsYXJuYUh1Yi5Db25maWcubGFuZy5PTkxZX0xPV0VSX0FNT1VOVFNfQVJFX0FMTE9XRUQpO1xyXG5cdFx0XHRcdFx0ZXZlbnQudGFyZ2V0LnZhbHVlID0gb3JpZ2luYWxWYWx1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdH07XHJcblx0XHJcblx0S2xhcm5hSHViLm9uKCdyZWFkeScsICgpID0+IGluaXQoKSk7XHJcbn0pKCk7ICJdfQ==
