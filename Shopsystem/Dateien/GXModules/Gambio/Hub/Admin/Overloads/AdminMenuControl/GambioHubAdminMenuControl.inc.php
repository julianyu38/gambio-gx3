<?php

/* --------------------------------------------------------------
   GambioHubAdminMenuControl.inc.php 2018-04-04
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GambioHubAdminMenuControl
 * 
 * Replace the old modules page menu item link with Gambio Hub.  
 */
class GambioHubAdminMenuControl extends GambioHubAdminMenuControl_parent
{
	/**
	 * Replaces the payment methods link. 
	 * 
	 * It'll take care of the current version and reference the compatibility page if necessary.
	 * 
	 * @param int $p_customers_id
	 *
	 * @return array 
	 */
	public function get_menu_array($p_customers_id)
	{
		$menu_array = parent::get_menu_array($p_customers_id);
		
		if(class_exists(AdminAccessService::class))
		{
			$adminAccessService = StaticGXCoreLoader::getService('AdminAccess');
			if(!$adminAccessService->checkReadingPermissionForController(new NonEmptyStringType('HubConfiguration/PaymentMethods'),
			                                                             new IdType((int)$_SESSION['customer_id'])))
			{
				return $menu_array;
			}
		}
		
		$klarnaHubInstalled = gm_get_conf('GAMBIO_HUB_CLIENT_KEY') !== null && 
                              gm_get_conf('GAMBIO_HUB_CLIENT_KEY') !== '' &&
                              gm_get_conf('GAMBIO_HUB_REMOTE_CONFIG_KLARNAHUB_DEBUGLOGGING') !== null &&
		                      gm_get_conf('GAMBIO_HUB_REMOTE_CONFIG_KLARNAHUB_DEBUGLOGGING') !== '';
		
		foreach($menu_array as &$menu_block)
		{
			if($menu_block['id'] === 'BOX_HEADING_MODULES' || $menu_block['id'] === 'BOX_HEADING_FAVORITES')
			{
				foreach($menu_block['menuitems'] as &$menu_item)
				{
					if(strpos($menu_item['link'], 'modules.php?set=payment') !== false)
					{
						$menu_item['link'] =  !class_exists('\AdminLayoutHttpControllerResponse')
							? DIR_WS_ADMIN . 'gambio_hub.php?page=payment_methods'
							: DIR_WS_ADMIN . 'admin.php?do=HubConfiguration/PaymentMethods';
					}
				}
			}

			if($klarnaHubInstalled === false &&
			   ($menu_block['id'] === 'BOX_HEADING_HUB' || $menu_block['id'] === 'BOX_HEADING_FAVORITES'))
			{
				foreach($menu_block['menuitems'] as $menu_block_idx => &$menu_item_hub)
				{
					if(strpos($menu_item_hub['link'], 'admin.php') !== false &&
					   strpos($menu_item_hub['link_param'], 'do=KlarnaHubSettlements') !== false)
					{
						unset($menu_block['menuitems'][$menu_block_idx]);
					}
				}
			}
		}
		
		return $menu_array;
	}
}
