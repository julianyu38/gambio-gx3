<?php
/* --------------------------------------------------------------
   MoneyOrderPlusHubAdminMenuControl.inc.php 2018-04-17
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class MoneyOrderPlusHubAdminMenuControl
 *
 * Toggles the display of the money order plus menu item.
 */
class MoneyOrderPlusHubAdminMenuControl extends MoneyOrderPlusHubAdminMenuControl_parent
{
	/**
	 * @var string
	 */
	protected $configurationKey = 'GAMBIO_HUB_MONEY_ORDER_PLUS_HUB_USER_INTERFACE_URL';
	
	
	/**
	 * Replaces the payment methods link.
	 *
	 * It'll take care of the current version and reference the compatibility page if necessary.
	 *
	 * @param int $p_customers_id
	 *
	 * @return array
	 */
	public function get_menu_array($p_customers_id)
	{
		$menu_array = parent::get_menu_array($p_customers_id);
		
		if(class_exists(AdminAccessService::class))
		{
			$adminAccessService = StaticGXCoreLoader::getService('AdminAccess');
			
			if(!$adminAccessService->checkReadingPermissionForController(new NonEmptyStringType('MoneyOrderPlusHub'),
			                                                             new IdType((int)$_SESSION['customer_id'])))
			{
				return $menu_array;
			}
		}
		
		$connectedToHub = $this->_isConnectedToHub();
		
		$moneyOrderPlusInstalled = $this->_isMoneyOrderPlusInstalled();
		
		// Remove the menu item if the the shop is not connected to Hub or the module is not installed/configured.
		foreach($menu_array as &$menu_block)
		{
			if(($connectedToHub === false || $moneyOrderPlusInstalled === false)
			   && ($menu_block['id'] === 'BOX_HEADING_ORDERS' || $menu_block['id'] === 'BOX_HEADING_FAVORITES'))
			{
				foreach($menu_block['menuitems'] as $menu_block_idx => &$menu_item_hub)
				{
					if(strpos($menu_item_hub['link'], 'admin.php') !== false
					   && strpos($menu_item_hub['link_param'], 'do=MoneyOrderPlusHub') !== false)
					{
						unset($menu_block['menuitems'][$menu_block_idx]);
					}
				}
			}
		}
		
		return $menu_array;
	}
	
	
	/**
	 * Checks whether the shop is connector to hub.
	 *
	 * @return bool
	 */
	protected function _isConnectedToHub()
	{
		return gm_get_conf('GAMBIO_HUB_CLIENT_KEY') !== null
		       && gm_get_conf('GAMBIO_HUB_CLIENT_KEY') !== '';
	}
	
	
	/**
	 * Checks whether money order plus is installed and properly configured.
	 *
	 * @return bool
	 */
	protected function _isMoneyOrderPlusInstalled()
	{
		return gm_get_conf($this->configurationKey) !== null && gm_get_conf($this->configurationKey) !== '';
	}
}