<?php
/* --------------------------------------------------------------
   GambioHubOrderListGenerator.inc.php 2018-01-17
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GambioHubOrderListGenerator
 *
 * Enables order filtering with Gambio Hub modules and the output of gambio hub payment data.
 */
class GambioHubOrderListGenerator extends GambioHubOrderListGenerator_parent
{
	/**
	 * Filter the order records.
	 *
	 * This method contains the filtering logic. It can be overloaded in order to provide a custom filtering logic.
	 *
	 * @param array           $filterParameters Contains the column slug-names and their values.
	 * @param IntType|null    $startIndex       The start index of the wanted array to be returned (default = null).
	 * @param IntType|null    $maxCount         Maximum amount of items which should be returned (default = null).
	 * @param StringType|null $orderBy          A string which defines how the items should be ordered (default = null).
	 *
	 * @return CI_DB_result
	 *
	 * @throws BadMethodCallException
	 */
	protected function _filter(array $filterParameters,
	                           IntType $startIndex = null,
	                           IntType $maxCount = null,
	                           StringType $orderBy = null)
	{
		if(!is_array($filterParameters['paymentMethod']))
		{
			return parent::_filter($filterParameters, $startIndex, $maxCount, $orderBy); // No payment method filtering
		}
		
		// Extract Gambio Hub options from filtering (will be handled by this method).
		$hubPaymentMethods = [];
		
		// None Gambio Hub payment methods.
		$paymentMethods = [];
		
		foreach($filterParameters['paymentMethod'] as $index => $paymentMethod)
		{
			if(strpos($paymentMethod, 'Hub') !== false)
			{
				$hubPaymentMethods[] = $paymentMethod;
			}
			else
            {
                $paymentMethods[] = $paymentMethod;
            }
            unset($filterParameters['paymentMethod'][$index]);
		}
		
		if (empty($filterParameters['paymentMethod']))
		{
			// Disable payment method filtering as there aren't any options left.
			unset($filterParameters['paymentMethod']);
		}
		
		$this->db->group_start()->where('orders.gambio_hub_module', array_shift($hubPaymentMethods));
		foreach($hubPaymentMethods as $hubPaymentMethod)
		{
			$this->db->or_where('orders.gambio_hub_module', $hubPaymentMethod);
		}
        foreach($paymentMethods as $paymentMethod)
        {
	        if($paymentMethod === self::FILTER_NO_VALUE)
	        {
		        $paymentMethod = '';
	        }
	        
            $this->db->or_where('orders.payment_method', $paymentMethod);
        }
		$this->db->group_end();
		
		return parent::_filter($filterParameters, $startIndex, $maxCount, $orderBy);
	}

    /**
     * Returns a string for the ::_select() method which contains column names of the orders table.
     *
     * @return string
     */
    protected function _ordersColumns()
    {
        $additionalColumns = ', orders.gambio_hub_module, orders.gambio_hub_module_title';

        return parent::_ordersColumns() . $additionalColumns;
    }

    /**
     * Creates and returns whether an order shipping or payment type instance by the given row data and type argument.
     *
     * @param string $type Whether 'shipping' or 'payment', used to determine the expected order type class.
     * @param array  $row  Row array with data about the order type.
     *
     * @return OrderShippingType|OrderPaymentType
     *
     * @throws InvalidArgumentException
     */
    protected function _createOrderType($type, array $row)
    {
        if ($type === 'payment' && $row['gambio_hub_module']) {
            $paymentTitle = new StringType($row['gambio_hub_module_title']);
            $paymentModule = new StringType($row['gambio_hub_module']);

            return MainFactory::create('OrderPaymentType', $paymentTitle, $paymentModule);
        }

        return parent::_createOrderType($type, $row);
    }
}