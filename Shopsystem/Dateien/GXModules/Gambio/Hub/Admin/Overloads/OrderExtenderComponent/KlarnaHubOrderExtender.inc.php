<?php
/* --------------------------------------------------------------
   KlarnaHubOrderExtender.inc.php 2018-04-20
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

require_once DIR_FS_CATALOG . 'inc/xtc_format_price.inc.php';

/**
 * Class KlarnaHubOrderExtender
 */
class KlarnaHubOrderExtender extends KlarnaHubOrderExtender_parent
{
	/**
	 * @var \HubAssetHelper
	 */
	protected $hubAssetHelper;
	
	/**
	 * Proceed with the execution of the extender.
	 */
	public function proceed()
	{
		parent::proceed();
		
		$installedVersion = gm_get_conf('INSTALLED_VERSION');
		$this->hubAssetHelper = MainFactory::create('HubAssetHelper', $installedVersion);
		
		$module = $this->_getModuleData();
		
		$klarnaHubModuleCodes = [
			'KlarnaHub',
			'KlarnaPaylaterHub',
			'KlarnaPaynowHub',
			'KlarnaSliceitHub',
			'KlarnaBanktransferHub'
		];
		
		if(in_array($module['code'], $klarnaHubModuleCodes, false))
		{
			$this->_addKlarnaHubOrderDetailsScript($module)->_recalculateOrderAmount($module)->addContent();
		}
	}
	
	
	/**
	 * Returns array containing the Hub module "code" and "title" properties.
	 *
	 * @return array
	 */
	protected function _getModuleData()
	{
		$sql   = '
			SELECT 
				`gambio_hub_module` AS `code`, 
				`gambio_hub_module_title` AS `title` 
			FROM `orders` 
			WHERE `orders_id` = ' . (int)$_GET['oID'];
		$query = xtc_db_query($sql);
		
		return xtc_db_fetch_array($query);
	}
	
	
	/**
	 * Loads the Gambio Hub order details JavaScript file.
	 *
	 * The script will adjust the order details page for Gambio Hub compatibility. Check the order_details.js for
	 * further information.
	 *
	 * @param array $module Contains the module "code" and "title" properties.
	 *
	 * @return KlarnaHubOrderExtender Returns same instance for chained method calls.
	 */
	protected function _addKlarnaHubOrderDetailsScript(array $module)
	{
		$debug = file_exists(DIR_FS_CATALOG . '.dev-environment');
		
		$postfix = $debug ? '' : '.min';
		
		$baseUrl = HTTP_SERVER . DIR_WS_CATALOG . $this->hubAssetHelper->getScriptsBaseUrl();
		
		$queryParams = [
			'appUrl'      => DIR_WS_CATALOG,
			'moduleCode'  => $module['code'],
			'orderNumber' => $_GET['oID']
		];
		
		$this->v_output_buffer['order_status'] .= '
            <script src="' . $baseUrl . '/vendor/fetch' . $postfix . '.js"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/klarna_hub' . $postfix . '.js?'
		                                          . http_build_query($queryParams, null, '&') . '"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/klarna_hub' . $postfix . '.js?'
		                                          . http_build_query($queryParams, null, '&') . '"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/klarna_hub_api' . $postfix . '.js"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/klarna_hub_lib' . $postfix . '.js"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/order_details/disable_edit_address_button' . $postfix . '.js"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/order_details/disable_edit_button_dropdown' . $postfix . '.js"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/order_details/extend_cancel_order_action' . $postfix . '.js"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/order_details/extend_order_status_modal' . $postfix . '.js"></script>
            <script src="' . $baseUrl . '/extenders/klarna_hub/order_details/extend_tracking_codes_button' . $postfix . '.js"></script>
        ';
		
		return $this;
	}
	
	
	/**
	 * Recalculates order amount.
	 *
	 * KlarnaHub orders that are marked for recalculation need to send their new amounts to Klarna.
	 *
	 * @param array $module Contains the module "code" and "title" properties.
	 *
	 * @return KlarnaHubOrderExtender Returns same instance for chained method calls.
	 */
	protected function _recalculateOrderAmount(array $module)
	{
		if(!array_key_exists('oID', $_GET))
		{
			return $this;
		}
		
		try
		{
			$hubClientKeyConfiguration = MainFactory::create('HubClientKeyConfiguration');
			$clientKey                 = $hubClientKeyConfiguration->getClientKey();
		}
		catch(Exception $exception)
		{
			return $this; // The client is not connected to Hub so do not proceed with the recalculation.
		}
		
		$orderNumberValue = $_GET['oID'];
		
		$key = 'GAMBIO_HUB_KLARNA_HUB_RECALCULATE';
		
		$recalculate = json_decode((string)gm_get_conf($key), true);
		
		if(is_array($recalculate) && array_key_exists($orderNumberValue, $recalculate))
		{
			$klarnaHubFactory = MainFactory::create('KlarnaHubFactory');
			
			$moduleCode  = new NonEmptyStringType($module['code']);
			$orderNumber = new NonEmptyStringType($orderNumberValue);
			
			$klarnaHubCallbackClient = $klarnaHubFactory->createCallbackClient($moduleCode, $orderNumber);
			
			$queryBuilder = StaticGXCoreLoader::getDatabaseQueryBuilder();
			
			$subtotal = (float)$queryBuilder->get_where('orders_total', [
				'orders_id' => $orderNumberValue,
				'class'     => 'ot_subtotal'
			])->row()->value;
			
			$additionalAmounts = $recalculate[$orderNumberValue];
			
			$queryBuilder->update('orders_total', [
				'text'  => xtc_format_price($additionalAmounts['newShippingCosts'], '1', false),
				'value' => $additionalAmounts['newShippingCosts']
			], [
				                      'orders_id' => $_GET['oID'],
				                      'class'     => 'ot_shipping'
			                      ]);
			
			$queryBuilder->update('orders_total', [
				'text'  => xtc_format_price(-1 * $additionalAmounts['newVoucherAmount'], '1', false),
				'value' => -1 * $additionalAmounts['newVoucherAmount']
			], [
				                      'orders_id' => $_GET['oID'],
				                      'class'     => 'ot_coupon'
			                      ]);
			
			$klarnaHubConfiguration = $klarnaHubFactory->createConfiguration();
			
			$configuration = $klarnaHubConfiguration->asArray($clientKey, $moduleCode, $orderNumber);
			
			$klarnaHubOrder = MainFactory::create('KlarnaHubOrder', StaticGXCoreLoader::getDatabaseQueryBuilder(),
			                                      $configuration['order'], $configuration['klarnaOrder']);
			
			$reduceInvoiceAmountData = [
				'newInvoiceAmount' => KlarnaHubPrice::sanitize(($subtotal
				                                                - (float)$additionalAmounts['newVoucherAmount']
				                                                + (float)$additionalAmounts['newShippingCosts']) * 100),
				'newShippingCosts' => KlarnaHubPrice::sanitize((float)$additionalAmounts['newShippingCosts'] * 100),
				'newVoucherAmount' => KlarnaHubPrice::sanitize((float)$additionalAmounts['newVoucherAmount'] * 100),
				'comment'          => '',
				'orderLines'       => $klarnaHubOrder->getUpdatedOrderLines()
			];
			
			try
			{
				$klarnaHubCallbackClient->executeReduceInvoiceAmount($reduceInvoiceAmountData);
				
				// Trigger shop order lines recalculation and update the order lines table element.
				$debug = file_exists(DIR_FS_CATALOG . '.dev-environment');
				
				$postfix = $debug ? '' : '.min';
				
				$this->v_output_buffer['order_status'] .= '
		            <script src="' . DIR_WS_CATALOG
				                                          . $this->hubAssetHelper->getScriptsBaseUrl() . '/extenders/klarna_hub/order_details/update_order_lines'
				                                          . $postfix . '.js"></script>
		        ';
			}
			catch(Exception $exception)
			{
				$languageTextManager = MainFactory::create('LanguageTextManager', 'gambio_hub_klarna_hub',
				                                           $_SESSION['languages_id']);
				
				// Append the error in the message stack manually (cause message stack contents are already rendered).
				$html = '
					<div class="alert alert-danger">
						' . $languageTextManager->get_text('KLARNA_RECALCULATION_FAILED') . '
					</div>
				';
				
				$this->v_output_buffer['order_status'] .= '
		            <script>
		                $(function() {
	                        $(".message_stack_container").append(' . json_encode($html) . ');
		                });
		            </script>
		        ';
			}
			
			unset($recalculate[$orderNumberValue]);
			
			gm_set_conf($key, json_encode($recalculate));
		}
		
		return $this;
	}
}