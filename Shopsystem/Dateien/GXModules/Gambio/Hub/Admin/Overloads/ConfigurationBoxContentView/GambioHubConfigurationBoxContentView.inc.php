<?php
/* --------------------------------------------------------------
   GambioHubConfigurationBoxContentView.inc.php 2018-04-23
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GambioHubConfigurationBoxContentView
 */
class GambioHubConfigurationBoxContentView extends GambioHubConfigurationBoxContentView_parent
{
	/**
	 * Appends JavaScript to the Gambio Hub configuration page.
	 *
	 * The appended JavaScript will create a button that will allow users to manually reset the Hub Client Key.
	 *
	 * @return string
	 */
	public function get_html()
	{
		$html = parent::get_html();
		
		if(isset($_GET['set'], $_GET['module'], $_GET['action'])
		   && $_GET['set'] === 'payment'
		   && $_GET['module'] === 'gambio_hub'
		   && $_GET['action'] === 'edit')
		{
			$html .= '
				<script>
					var button = document.createElement("button"); 
					button.className = "btn btn-danger";
					button.innerHTML = "Reset Hub Client Key"; 
					button.style.margin = "0 0 24px 0";
					button.onclick = function() {
						if (!window.confirm("Are you sure you want to proceed?")) {
							return; 
						}
						
					    $.post("admin.php?do=HubConfigurationAjax/deleteClientKey")
					        .done(function() { window.location.reload(); });
					}; 
				
					document.getElementsByClassName(\'configuration-box-content\')[0].appendChild(button); 
				</script>
			';
		}
		
		return $html;
	}
}