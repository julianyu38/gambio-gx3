<?php
/* --------------------------------------------------------------
	admin_menu.lang.inc.php 2018-04-20
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'BOX_HEADING_HUB' => 'Gambio Hub',
	'BOX_HUB_ACCOUNT' => 'Account',
	'BOX_HUB_MONEY_ORDER_PLUS' => 'Zahlungsabgleich',
	'BOX_HUB_PAYMENT_METHODS' => 'Zahlungsweisen',
    'BOX_HUB_TRANSACTIONS' => 'Transaktionen'
);
