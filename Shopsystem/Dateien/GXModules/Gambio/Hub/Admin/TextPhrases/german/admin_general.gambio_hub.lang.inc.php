<?php
/* --------------------------------------------------------------
	admin_general.lang.inc.php 2017-02-17
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
    'BOX_HEADING_HUB_PAYMENT' => 'Gambio Payment Hub',
    'BOX_HEADING_HUB_PAYMENT_MISC' => 'Sonstige',
	'TEXT_HUB_CONNECTED' => 'HUB verbunden',
	'TEXT_HUB_DISCONNECTED' => 'HUB nicht verbunden'
);
