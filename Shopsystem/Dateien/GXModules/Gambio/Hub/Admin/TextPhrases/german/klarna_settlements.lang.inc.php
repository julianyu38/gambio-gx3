<?php
/* --------------------------------------------------------------
   klarna_settlements.lang.inc.php 2018-03-20
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'data_not_received'              => 'Daten konnten nicht abgerufen werden.',
	'end_date'                       => 'bis',
	'error_retrieving_data_from_hub' => 'Fehler beim Abruf der Daten vom Gambio Hub.',
	'heading'                        => 'Klarna Settlements',
	'klarna_settlements'             => 'Klarna Settlements',
	'page'                           => 'Seite',
	'page_first'                     => 'erste',
	'page_last'                      => 'letzte',
	'page_next'                      => 'nächste',
	'page_prev'                      => 'vorherige',
	'pages_of'                       => 'von',
	'show'                           => 'anzeigen',
	'start_date'                     => 'Zeitraum von',
);
