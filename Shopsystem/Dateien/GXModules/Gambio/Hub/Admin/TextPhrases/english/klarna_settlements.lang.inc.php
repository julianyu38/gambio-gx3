<?php
/* --------------------------------------------------------------
   klarna_settlements.lang.inc.php 2018-03-20
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'data_not_received'              => 'Data could not be retrieved.',
	'end_date'                       => 'until',
	'error_retrieving_data_from_hub' => 'Error retrieving data from Gambio Hub.',
	'heading'                        => 'Klarna Settlements',
	'klarna_settlements'             => 'Klarna Settlements',
	'page'                           => 'page',
	'page_first'                     => 'first',
	'page_last'                      => 'last',
	'page_next'                      => 'next',
	'page_prev'                      => 'previous',
	'pages_of'                       => 'of',
	'show'                           => 'show',
	'start_date'                     => 'Date from',
);
