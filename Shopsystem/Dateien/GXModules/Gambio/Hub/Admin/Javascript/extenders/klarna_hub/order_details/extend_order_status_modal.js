/* --------------------------------------------------------------
 extend_order_status_modal.js 2017-11-02
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Extends the order status modal with a "notify-klarna-hub" checkbox.
 */
(function() {
	'use strict';
	
	/**
	 * Initializes the module. 
	 * 
	 * @private
	 */
	const init = () => {
		const $form = $('#update_orders_status_form')
		const $select = $form.find('[name="gm_status"]');
		
		// Add the notify-klarna-hub checkbox (hidden by default).
		const $sourceControlGroup = $form.find('.single-checkbox:first').parent();
		const $controlGroup = $sourceControlGroup.clone(true);
		$controlGroup.addClass('hidden');
		
		const $label = $controlGroup.find('label');
		$label.text(KlarnaHub.Config.lang.NOTIFY_KLARNA);
		
		const $singleCheckbox = $controlGroup.find('.single-checkbox');
		const $checkbox = $controlGroup.find('input');
		$checkbox.attr('name', 'gm_notify_klarna');
		$controlGroup.insertBefore($sourceControlGroup.next());
		
		// Bind form submit event.
		$form.on('submit', (event) => {
			event.preventDefault();
			
			if (($select.val() && $select.val() != KlarnaHub.Config.orderStatusShipped) || !$checkbox.prop('checked')) {
				$form[0].submit();
				return;
			}
			
			KlarnaHub.Api.executeFullCapture().then(() => $form[0].submit()); 
		});
		
		// Bind status type change event.
		$select.on('change', (event) => {
			if (event.target.value && event.target.value == KlarnaHub.Config.orderStatusShipped) {
				$controlGroup.removeClass('hidden');
				$checkbox.prop('checked', true);
				$singleCheckbox.addClass('checked');
			} else {
				$controlGroup.addClass('hidden');
				$checkbox.prop('checked', false);
				$singleCheckbox.removeClass('checked');
			}
		});
	};
	
	KlarnaHub.on('ready', () => init()); 	
})(); 