/* --------------------------------------------------------------
 extend_tracking_codes_button.js 2017-11-02
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Extends the order status modal with a "notify-klarna" checkbox.
 *
 * Notice: This method will use jQuery UI modal for the prompt.
 */
(function() {
	'use strict';
	
	/**
	 * Initializes the module.
	 *
	 * @private
	 */
	const init = () => {
		// Tracking code selectors.
		let $trackingCodeText;
		let $shippingCompanySelect;
		
		// Create prompt dialog (will be initially hidden).
		const $dialog = $('<div/>', {
			'class': 'klarna-hub dialog shipment-tracking hidden',
			'html': [
				$('<p/>', {
					'text': KlarnaHub.Config.lang.TRANSMIT_SHIPMENT_NUMBER_TO_KLARNA
				})
			]
		});
		
		// Bind button click event handler.
		const bindTrackingCodesButtonEvent = () => {
			$shippingCompanySelect = $('#parcel_services_dropdown');
			$trackingCodeText = $('#parcel_service_tracking_code');
			
			const $addTrackingCodeButton = $('span.add_tracking_code');
			
			$addTrackingCodeButton.on('click', () => {
				if ($shippingCompanySelect.val() === '') {
					return;
				}
				
				if ($trackingCodeText.val() === '') {
					return;
				}
				
				$dialog.dialog('open');
			});
		};
		
		const buttons = [
			{
				text: KlarnaHub.Config.lang.NO,
				click: () => $dialog.dialog('close')
			},
			{
				text: KlarnaHub.Config.lang.NOTIFY_KLARNA,
				click: () => {
					const shippingCompany = $shippingCompanySelect.find('option:selected').text();
					const trackingNumber = $trackingCodeText.val();
					
					KlarnaHub.Api.executeAddTrackingCode(shippingCompany, trackingNumber)
						.then(() => location.reload());
				}
			}
		];
		
		$dialog.dialog({
			autoOpen: false,
			width: 500,
			height: 'auto',
			resizable: false,
			modal: true,
			title: KlarnaHub.Config.lang.SHIPMENT_TRACKING,
			dialogClass: 'gx-container',
			close: bindTrackingCodesButtonEvent,
			buttons
		});
		
		$dialog.parent().find('.ui-dialog-buttonpane button:last-child').addClass('btn-primary');
		
		bindTrackingCodesButtonEvent()
	};
	
	KlarnaHub.on('ready', () => init());
})(); 