/* --------------------------------------------------------------
 extend_order_edit_form.js 2018-02-21
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Extends the order edit form.
 *
 * If the "recalculate" checkbox is checked the order will be marked with a recalculation flag that will later be
 * used to forward the amount changes to Klarna.
 */
(function() {
	'use strict';
	
	/**
	 * Form Submit Buttons Selector
	 * 
	 * @type {String}
	 */
	const formSubmitButtonsSelector = '.bottom-save-bar input:button, form[name="save_order"] input:submit';
	
	/**
	 * Initializes the module.
	 *
	 * @private
	 */
	const init = () => {
		const $form = $('[name="save_order"]');
		const $checkbox = $('[name="recalculate"]');
		
		// Create additional amounts dialog.
		const html = [];
		
		const voucherOrderLine = KlarnaHub.Config.klarnaOrder.order_lines.find(orderLine => orderLine.reference
			=== 'ot_coupon');
		
		const oldVoucherAmount = voucherOrderLine ? Math.abs(voucherOrderLine.total_amount) / 100 : 0;
		
		if (voucherOrderLine) {
			const oldVoucherAmountDisplay = oldVoucherAmount.toLocaleString(KlarnaHub.Config.klarnaOrder.locale, {
				style: 'currency',
				currency: KlarnaHub.Config.klarnaOrder.purchase_currency,
				currencyDisplay: 'code',
				useGrouping: false
			}).replace(',', '.');
			
			html.push(
				$('<div/>', {
					'class': 'control-group',
					'html': [
						$('<label/>', {
							'text': KlarnaHub.Config.lang.OLD_VOUCHER_AMOUNT,
							'style': 'width:70%'
						}),
						$('<span/>', {
							'text': oldVoucherAmountDisplay
						})
					]
				})
			);
			
			html.push(
				$('<div/>', {
					'class': 'control-group',
					'html': [
						$('<label/>', {
							'text': KlarnaHub.Config.lang.NEW_VOUCHER_AMOUNT,
							'style': 'width:70%'
						}),
						$('<div/>', {
							'class': 'input-group',
							'html': [
								$('<input/>', {
									'class': 'new-voucher-amount',
									'value': oldVoucherAmountDisplay.replace(/[^\d.-]/g, '')
								}),
								$('<div/>', {
									'class': 'input-group-addon',
									'text': oldVoucherAmountDisplay.replace(/[\d.-]/g, '')
								})
							]
						})
					]
				})
			)
		}
		
		const shippingOrderLine = KlarnaHub.Config.klarnaOrder.order_lines.find(orderLine => orderLine.reference
			=== 'ot_shipping');
		
		const oldShippingCosts = shippingOrderLine ? Math.abs(shippingOrderLine.total_amount / 100) : 0;
		
		if (shippingOrderLine) {
			const oldShippingCostsDisplay = oldShippingCosts.toLocaleString(KlarnaHub.Config.klarnaOrder.locale, {
				style: 'currency',
				currency: KlarnaHub.Config.klarnaOrder.purchase_currency,
				currencyDisplay: 'code',
				useGrouping: false
			}).replace(',', '.');
			
			html.push(
				$('<div/>', {
					'class': 'control-group',
					'html': [
						$('<label/>', {
							'text': KlarnaHub.Config.lang.OLD_SHIPPING_COSTS,
							'style': 'width:70%'
						}),
						$('<span/>', {
							'text': oldShippingCostsDisplay
						})
					]
				})
			);
			
			html.push(
				$('<div/>', {
					'class': 'control-group',
					'html': [
						$('<label/>', {
							'text': KlarnaHub.Config.lang.NEW_SHIPPING_COSTS,
							'style': 'width:70%'
						}),
						$('<div/>', {
							'class': 'input-group',
							'html': [
								$('<input/>', {
									'class': 'new-shipping-costs',
									'value': oldShippingCostsDisplay.replace(/[^\d.-]/g, '')
								}),
								$('<div/>', {
									'class': 'input-group-addon',
									'text': oldShippingCostsDisplay.replace(/[\d.-]/g, '')
								})
							]
						})
					]
				})
			);
		}
		
		const $dialog = $('<div/>', {html}).appendTo('body');
		
		$dialog.dialog({
			autoOpen: false,
			width: 500,
			height: 'auto',
			resizable: false,
			modal: true,
			title: 'Klarna',
			dialogClass: 'gx-container additional-costs-dialog ',
			buttons: [
				{
					text: KlarnaHub.Config.lang.CLOSE,
					click: () => $dialog.dialog('close')
				},
				{
					text: KlarnaHub.Config.lang.NOTIFY_KLARNA,
					click: () => {
						$dialog.find('.validate').remove('validate error');
						
						const $newVoucherAmount = $dialog.find('.new-voucher-amount'); 
						
						const newVoucherAmount = $newVoucherAmount.length ? $newVoucherAmount.val().replace(',', '.') : oldVoucherAmount;
						
						if (newVoucherAmount && isNaN(newVoucherAmount)) {
							$newVoucherAmount.addClass('validate error');
						}
						
						const $newShippingCosts = $dialog.find('.new-shipping-costs');
						
						const newShippingCosts = $newShippingCosts.length ? $newShippingCosts.val().replace(',', '.') : oldShippingCosts;
						
						if (newShippingCosts && isNaN(newShippingCosts)) {
							$newShippingCosts.addClass('validate error');
						}
						
						if ($dialog.find('.validate.error').length) {
							return;
						}
						
						// Activate the recalculation flag for this order.
						KlarnaHub.Api.activateRecalculationFlag(parseFloat(newVoucherAmount), parseFloat(newShippingCosts))
							.then(() => $form[0].submit());
					}
				}
			],
			open: () => $('.additional-costs-dialog .ui-dialog-buttonset button:last').addClass('btn btn-primary')
		});
		
		$form.on('submit', (event) => {
			event.preventDefault();
			
			if (!$checkbox.prop('checked')) {
				$form[0].submit();
				return;
			}
			
			if (!html.length) {
				KlarnaHub.Api.activateRecalculationFlag(0, 0).then(() => $form[0].submit());
			} else {
				$dialog.dialog('open');
			}
		});
		
		$(formSubmitButtonsSelector).prop('disabled', false);
	};
	
	// Pre-check the recalculation checkbox. 
	$(document).on('JSENGINE_INIT_FINISHED', () => $('[name="recalculate"]').prop('checked', true).trigger('change'));
	
	// Disable the close button until KlarnaHub is ready.
	$(document).on('ready', () => $(formSubmitButtonsSelector).prop('disabled', true));
	
	KlarnaHub.on('ready', () => init());
})();