<?php
/* --------------------------------------------------------------
   MoneyOrderPlusHubController.inc.php 2018-05-31
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use \HubPublic\Http\CurlRequest;

/**
 * Class MoneyOrderPlusHubController
 *
 * @category   System
 * @package    AdminHttpViewControllers
 * @extends    AdminHttpViewController
 */
class MoneyOrderPlusHubController extends AdminHttpViewController
{
	/**
	 * @var string
	 */
	const GENERATE_ACCESS_TOKEN_ACTION = 'generate-access-token';
	
	/**
	 * @var string
	 */
	protected $configurationKey = 'GAMBIO_HUB_MONEY_ORDER_PLUS_HUB_USER_INTERFACE_URL';
	
	/**
	 * @var string
	 */
	protected $scriptsBaseUrl;
	
	/**
	 * @var string
	 */
	protected $stylesBaseUrl;
	
	/**
	 * @var string
	 */
	protected $minPostfix;
	
	/**
	 * @var LanguageTextManager
	 */
	protected $languageTextManager;
	
	/**
	 * @var HubAssetHelper
	 */
	protected $hubAssetHelper;
	
	/**
	 * @var \HubClientKeyConfiguration
	 */
	protected $clientKeyConfiguration;
	
	
	/**
	 * Initialize the controller.
	 */
	public function init()
	{
		if(isset($_SESSION['coo_page_token']) == false)
		{
			throw new RuntimeException('Page Token Generator not found.'); // CSRF Protection
		}
		
		$installedVersion             = gm_get_conf('INSTALLED_VERSION');
		$this->hubAssetHelper         = MainFactory::create('HubAssetHelper', $installedVersion);
		$this->clientKeyConfiguration = MainFactory::create('HubClientKeyConfiguration');
		
		$this->scriptsBaseUrl      = DIR_WS_CATALOG . $this->hubAssetHelper->getScriptsBaseUrl();
		$this->stylesBaseUrl       = DIR_WS_CATALOG . $this->hubAssetHelper->getStylesBaseUrl();
		$this->minPostfix          = file_exists(DIR_FS_CATALOG . '.dev-environment') ? '' : '.min';
		$this->languageTextManager = MainFactory::create('LanguageTextManager', 'gambio_hub_money_order_plus_hub',
		                                                 $_SESSION['languages_id']);
	}
	
	
	/**
	 * Renders the money order interface page.
	 *
	 * @return HttpControllerResponseInterface
	 */
	public function actionDefault()
	{
		$title = new NonEmptyStringType($this->languageTextManager->get_text('PAGE_TITLE',
		                                                                     'gambio_hub_money_order_plus_hub'));
		
		$template = new ExistingFile(new NonEmptyStringType(DIR_FS_CATALOG
		                                                    . $this->hubAssetHelper->getTemplatesBasePath()
		                                                    . '/money_order_plus.html'));
		
		$userInterfaceUrl = gm_get_conf($this->configurationKey);
		
		if(empty($userInterfaceUrl))
		{
			return MainFactory::create(RedirectHttpControllerResponse::class,
			                           DIR_WS_ADMIN . 'admin.php?do=HubConfiguration/PaymentMethods');
		}
		
		$accessToken = $this->generateAccessToken();
		
		if(empty($accessToken))
		{
			return MainFactory::create(RedirectHttpControllerResponse::class,
			                           DIR_WS_ADMIN . 'admin.php?do=HubConfiguration/PaymentMethods');
		}
		
		$data = MainFactory::create('KeyValueCollection', [
			'iframe_source_url' => $userInterfaceUrl . '?language=' . $_SESSION['language_code'] . '&accessToken='
			                       . $accessToken
		]);
		
		$assetsArray = [
			MainFactory::create('Asset', $this->scriptsBaseUrl . '/vendor/promise' . $this->minPostfix . '.js'),
			MainFactory::create('Asset', $this->scriptsBaseUrl . '/vendor/fetch' . $this->minPostfix . '.js'),
			MainFactory::create('Asset', $this->scriptsBaseUrl . '/vendor/iframe_resizer' . $this->minPostfix . '.js'),
			MainFactory::create('Asset', $this->stylesBaseUrl . '/gambio_hub' . $this->minPostfix . '.css')
		];
		
		$assets = MainFactory::create('AssetCollection', $assetsArray);
		
		return MainFactory::create('AdminLayoutHttpControllerResponse', $title, $template, $data, $assets);
	}
	
	
	/**
	 * Generates access token for MoneyOrderPlusHub.
	 *
	 * @return string
	 */
	private function generateAccessToken()
	{
		$params = [
			'action'     => self::GENERATE_ACCESS_TOKEN_ACTION,
			'source'     => 'shop',
			'client_key' => $this->clientKeyConfiguration->get()
		];
		
		$url = MODULE_PAYMENT_GAMBIO_HUB_URL . '/payment_modules/MoneyOrderPlusHub/callback?'
		       . http_build_query($params, null, '&');
		
		$request = new CurlRequest();
		
		$response = $request->setUrl($url)->execute();
		
		if($response->getStatusCode() !== 201)
		{
			LogControl::get_instance()
			          ->warning('Could not generate MoneyOrderPlus access token, invalid response code: '
			                    . $response->getStatusCode(), 'hub', 'errors');
			
			return null;
		}
		
		$body = json_decode($response->getBody(), true);
		
		if(!array_key_exists('accessToken', $body) || empty($body['accessToken']))
		{
			LogControl::get_instance()->warning('Response body does not contain accessToken or accessToken is empty: '
			                                    . $response->getBody(), 'hub', 'errors');
			
			return null;
		}
		
		return $body['accessToken'];
	}
}