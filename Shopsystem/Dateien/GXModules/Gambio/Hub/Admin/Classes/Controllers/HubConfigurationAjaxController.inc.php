<?php
/* --------------------------------------------------------------
   HubConfigurationAjaxController.inc.php 2018-01-19
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use \HubPublic\Exceptions\CurlRequestException;
use \HubPublic\Http\CurlRequest;

/**
 * Class GambioHubConfigurationAjaxController
 *
 * This controller contains AJAX functionality related to the configuration pages of the Gambio Hub module.
 *
 * @category   System
 * @package    AdminHttpViewControllers
 * @extends    AdminHttpViewController
 */
class HubConfigurationAjaxController extends AdminHttpViewController
{
    /**
     * Regular expression to verify a hexadecimal color value.
     *
     * @var string
     */
    protected $colorHexCodeRegex = '/^#[a-f0-9]{6}$/i';
    
    /**
     * @var array Extra features this hubConnector supports
     */
    protected $features = [
        'redirected_installation'
    ];
    
    
    /**
     * Get the shop data.
     *
     * If a Hub Client Key exists it will be returned instead of the shop data so that the merchant
     * data can be loaded from the customer portal.
     *
     * @return JsonHttpControllerResponse Returns the shop data.
     */
    public function actionGetShopData()
    {
        /**
         * @var CountryService  $countryService
         * @var CustomerService $customerService
         */
        $countryService  = StaticGXCoreLoader::getService('Country');
        $customerService = StaticGXCoreLoader::getService('Customer');
        
        $customerData = $customerService->getCustomerById(new IdType($_SESSION['customer_id']));
        
        // Authentication hash.
        $authHash = AuthHashCreator::create();
        $state    = STORE_ZONE ? (string)$countryService->getCountryZoneById(new IdType(STORE_ZONE))->getName() : '';
        
        $response = [
            'gender'      => (string)$customerData->getGender(),
            'firstname'   => TRADER_FIRSTNAME,
            'lastname'    => TRADER_NAME,
            'street'      => TRADER_STREET,
            'housenumber' => TRADER_STREET_NUMBER,
            'postcode'    => TRADER_ZIPCODE,
            'city'        => TRADER_LOCATION,
            'state'       => $state,
            'country'     => (string)$countryService->getCountryById(new IdType(STORE_COUNTRY))->getName(),
            'company'     => COMPANY_NAME,
            'email'       => (string)$customerData->getEmail(),
            'telephone'   => TRADER_TEL,
            'shopname'    => STORE_NAME,
            'shopkey'     => GAMBIO_SHOP_KEY,
            'authhash'    => $authHash->asString(),
            'version'     => ltrim(gm_get_conf('INSTALLED_VERSION'), 'v'),
            'url'         => HTTP_SERVER . DIR_WS_CATALOG,
            'features'    => $this->features
        ];
        
        try {
            $hubClientKeyConfiguration = MainFactory::create('HubClientKeyConfiguration');
            
            $hubClientKey = $hubClientKeyConfiguration->get();
            
            $response['clientkey'] = $hubClientKey;
        } catch (Exception $exception) {
            // The app will continue in demo mode.
        }
        
        return MainFactory::create('JsonHttpControllerResponse', $response);
    }
    
    
    /**
     * Starts the creation of the session key.
     *
     * @return JsonHttpControllerResponse Returns the session key data.
     */
    public function actionCreateSessionKey()
    {
        // Start a new gambio hub session.
        $serviceFactory         = MainFactory::create('HubServiceFactory');
        $sessionKeyService      = $serviceFactory->createHubSessionKeyService();
        $clientKeyConfiguration = MainFactory::create('HubClientKeyConfiguration');
        $curlRequest            = new CurlRequest();
        $logControl             = LogControl::get_instance();
        $hubSettings            = MainFactory::create('HubSettings', gm_get_conf('GAMBIO_HUB_CURL_TIMEOUT'));
        $sessionsApiClient      = MainFactory::create('HubSessionsApiClient', MODULE_PAYMENT_GAMBIO_HUB_URL,
            $sessionKeyService, $clientKeyConfiguration, $curlRequest, $logControl, $hubSettings);
        $authHash               = AuthHashCreator::create();
        
        try {
            $shopUrl      = HTTP_SERVER . DIR_WS_CATALOG;
            $languageCode = new LanguageCode(new StringType(strtoupper(DEFAULT_LANGUAGE)));
            
            $response['gambio_hub_session_key'] = $sessionsApiClient->startSession($authHash, $shopUrl, $languageCode);
        } catch (UnexpectedValueException $exception) {
            // Send error.
            http_response_code(500);
            $response = AjaxException::response($exception);
        } catch (CurlRequestException $exception) {
            // Send error.
            http_response_code(500);
            $response = AjaxException::response($exception);
        }
        
        return MainFactory::create('JsonHttpControllerResponse', $response);
    }
    
    
    /**
     * Returns the Gambio Hub Translations for the frontend.
     *
     * @return JsonHttpControllerResponse Returns the translation data.
     */
    public function actionGetTranslations()
    {
        try {
            $languageTextManager = MainFactory::create('LanguageTextManager', 'gambio_hub_account',
                $_SESSION['languages_id']);
            $response            = $languageTextManager->get_section_array();
        } catch (Exception $exception) {
            http_response_code(500);
            $response = AjaxException::response($exception);
        }
        
        return MainFactory::create('JsonHttpControllerResponse', $response);
    }
    
    
    /**
     * Returns all available order statuses and the default one.
     *
     * @return JsonHttpControllerResponse Returns the order statuses data.
     */
    public function actionGetOrderStatuses()
    {
        // Response.
        $response = [];
        
        // Order status key-value array (ID is the key, and name is the value).
        $availableOrderStatuses = [];
        
        // CodeIgniter query builder.
        $queryBuilder = StaticGXCoreLoader::getDatabaseQueryBuilder();
        
        // Language ID.
        $languageId = (int)$_SESSION['languages_id'];
        
        // Order statuses fetch query.
        $orderStatuses = $queryBuilder->get_where('orders_status', ['language_id' => $languageId])->result_array();
        
        // Iterate over each query result row and append the ID and name to the order status array.
        foreach ($orderStatuses as $orderStatus) {
            // Order status ID.
            $id = (int)$orderStatus['orders_status_id'];
            
            // Order status name.
            $name = (string)$orderStatus['orders_status_name'];
            
            // Append data to order status array.
            $availableOrderStatuses[$id] = $name;
        }
        
        // Append data to response.
        $response['default'] = (int)DEFAULT_ORDERS_STATUS_ID;
        $response['all']     = $availableOrderStatuses;
        
        return MainFactory::create('JsonHttpControllerResponse', $response);
    }
    
    
    /**
     * Returns the style edit configuration values.
     *
     * @return JsonHttpControllerResponse Style edit configuration values.
     */
    public function actionGetStyleEditConfiguration()
    {
        $response = [];
        
        try {
            $currentTemplate = CURRENT_TEMPLATE;
            
            $styleEditFilePath = new NonEmptyStringType(DIR_FS_CATALOG . 'StyleEdit3/bootstrap.inc.php');
            $styleEditFile     = new ExistingFile($styleEditFilePath);
            
            include_once $styleEditFile->getFilePath();
            
            $styleEditServiceFactory = new StyleEdit\Factories\ServiceFactory();
            $styleEditReadService    = $styleEditServiceFactory->createReadService($currentTemplate);
            
            $styleData = $styleEditReadService->findActiveStyleConfig();
            
            if ($styleData === null) {
                try {
                    // get default template style data
                    $styleData = $styleEditReadService->getBoilerplateByName('boilerplate1');
                } catch (\Exception $e) {
                    $styleData = null;
                }
            }
            
            if ($styleData) {
                // Get JSON data.
                $styleData = $styleData->getJsonDataArray();
                
                // Get settings.
                $entries = $styleData['settings'];
                
                // Get entries.
                $entries = array_map(function ($setting) {
                    return $setting['entries'];
                }, $entries);
                
                // Merge all entries.
                $entries = call_user_func_array('array_merge', $entries);
                
                // Get only entries of color type.
                $entries = array_filter($entries, function ($entry) {
                    return ($entry['type'] === 'color');
                });
                
                // Transform into a simple array.
                $entries = array_map(function ($entry) {
                    return [
                        'name'  => $entry['name'],
                        'value' => $entry['value']
                    ];
                }, $entries);
                
                // Keep only hexadecimal color codes or references.
                // Other values like 'darken()' and other SCSS stuff will be removed.
                $entries = array_filter($entries, function ($entry) {
                    $value = $entry['value'];
                    
                    $isValueHexCode   = (boolean)preg_match($this->colorHexCodeRegex, $value);
                    $isValueReference = (strpos($value, '$') !== false);
                    
                    return ($isValueHexCode || $isValueReference);
                });
                
                // Reset the array indices.
                $entries = array_values($entries);
                
                // Resolve references.
                $entries = array_map(function ($entry) use ($entries) {
                    $name             = $entry['name'];
                    $value            = $entry['value'];
                    $isValueReference = (strpos($value, '$') !== false);
                    
                    if ($isValueReference) {
                        $referencedName = substr($value, 1);
                        $allNames       = array_column($entries, 'name');
                        $matchedKey     = array_search($referencedName, $allNames, true);
                        $hasMatched     = ($matchedKey !== false);
                        
                        if ($hasMatched) {
                            $value = $entries[$matchedKey]['value'];
                        }
                    }
                    
                    return [
                        'name'  => $name,
                        'value' => $value,
                    ];
                }, $entries);
                
                // The resolved variables is the response.
                $response = $entries;
            }
        } catch (Exception $exception) {
            $response = [];
        }
        
        return MainFactory::create('JsonHttpControllerResponse', $response);
    }
    
    
    /**
     * Deletes the Gambio Hub Client Key
     *
     * @return \JsonHttpControllerResponse
     */
    public function actionDeleteClientKey()
    {
        gm_set_conf('GAMBIO_HUB_CLIENT_KEY', '');
        
        return MainFactory::create('JsonHttpControllerResponse', ['success' => true]);
    }
}