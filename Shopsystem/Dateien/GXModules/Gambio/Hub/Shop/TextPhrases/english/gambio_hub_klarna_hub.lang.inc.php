<?php
/* --------------------------------------------------------------
	gambio_hub_klarna_hub.lang.inc.php 2017-11-09
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'NOTIFY_KLARNA'                      => 'Notify Klarna',
	'FULL_CAPTURE'                       => 'Full Capture',
	'KLARNA_COMMUNICATION_FAILED'        => 'The communication against Klarna has failed.',
	'KLARNA_RECALCULATION_FAILED'        => 'Klarna recalculation has failed.',
	'SHIPMENT_TRACKING'                  => 'Shipment Tracking',
	'TRANSMIT_SHIPMENT_NUMBER_TO_KLARNA' => 'Transmit shipment number to Klarna?',
	'NO'                                 => 'No',
	'CLOSE'                              => 'Close',
	'UNEXPECTED_REQUEST_ERROR'           => 'Unexpected request error.',
	'ERROR_CODE'                         => 'Error Code',
	'CORRELATION_ID'                     => 'Correlation ID',
	'ERROR'                              => 'Error',
	'ONLY_LOWER_AMOUNTS_ARE_ALLOWED'     => 'Only lower amounts are allowed.',
	'OLD_VOUCHER_AMOUNT'                 => 'Old voucher amount',
	'NEW_VOUCHER_AMOUNT'                 => 'New voucher amount',
	'OLD_SHIPPING_COSTS'                 => 'Old shipping costs',
	'NEW_SHIPPING_COSTS'                 => 'New shipping costs',
    'InvoiceNote'                        => 'The payment is processed by Klarna. Please take note of the payment information sent by Klarna if you have not yet paid your purchase.',
];
