<?php
/* --------------------------------------------------------------
   gambio_hub.lang.inc.php 2018-10-26
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'hubMoneyOrderPayTo'                               => 'Our bank details:',
	'MODULE_PAYMENT_GAMBIO_HUB_ACCOUNT_APP_URL_DESC'   => 'Use the app for Hub account related configuration.',
	'MODULE_PAYMENT_GAMBIO_HUB_ACCOUNT_APP_URL_TITLE'  => 'Gambio Hub Account App URL',
	'MODULE_PAYMENT_GAMBIO_HUB_ALLOWED_DESC'           => 'Please enter the zones <b>individually</b> that should be allowed to use this module (e.g. US, UK (leave blank to allow all zones))',
	'MODULE_PAYMENT_GAMBIO_HUB_ALLOWED_TITLE'          => 'Allowed Zones',
	'MODULE_PAYMENT_GAMBIO_HUB_ERROR'                  => 'An error occurred during the execution of your payment.',
	'MODULE_PAYMENT_GAMBIO_HUB_IP_LIST_URL_DESC'       => 'Url to list of IPs allowed to make requests to the shop callback interface.',
	'MODULE_PAYMENT_GAMBIO_HUB_IP_LIST_URL_TITLE'      => 'Gambio Hub IP list URL',
	'MODULE_PAYMENT_GAMBIO_HUB_IP_WHITELIST_DESC'      => 'Comma separated IPs that are whitelisted (e.g. for shops that run behind Proxies). Partial values are possible (e.g. 192.168.0).',
	'MODULE_PAYMENT_GAMBIO_HUB_IP_WHITELIST_TITLE'     => 'Gambio Hub IP Whitelist',
    'MODULE_PAYMENT_GAMBIO_HUB_REST_ACTIONS_URL_DESC'  => 'Url to list of allowed actions, that the hub can do via the shop REST-API.',
    'MODULE_PAYMENT_GAMBIO_HUB_REST_ACTIONS_URL_TITLE' => 'Gambio Hub REST Action List URL',
	'MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID_DESC'   => 'Set the status of orders made using this payment module to this value',
	'MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID_TITLE'  => 'Set Order Status',
	'MODULE_PAYMENT_GAMBIO_HUB_SETTINGS_APP_URL_DESC'  => 'Use the app for Hub related configuration.',
	'MODULE_PAYMENT_GAMBIO_HUB_SETTINGS_APP_URL_TITLE' => 'Gambio Hub Settings App URL',
	'MODULE_PAYMENT_GAMBIO_HUB_SORT_ORDER_DESC'        => 'Display sort order; the lowest value is displayed first.',
	'MODULE_PAYMENT_GAMBIO_HUB_SORT_ORDER_TITLE'       => 'Display Sort Order',
	'MODULE_PAYMENT_GAMBIO_HUB_STATUS_DESC'            => 'Do you want to accept payments through the Gambio Hub?',
	'MODULE_PAYMENT_GAMBIO_HUB_STATUS_TITLE'           => 'Enable Gambio Hub',
	'MODULE_PAYMENT_GAMBIO_HUB_TEXT_DESCRIPTION'       => 'Gambio Hub',
	'MODULE_PAYMENT_GAMBIO_HUB_TEXT_INFO'              => 'Payment through the Gambio Hub',
	'MODULE_PAYMENT_GAMBIO_HUB_TEXT_TITLE'             => 'Gambio Hub',
	'MODULE_PAYMENT_GAMBIO_HUB_URL_DESC'               => 'Include the API version in the URL e.g. http://domain.de/api/v1',
	'MODULE_PAYMENT_GAMBIO_HUB_URL_TITLE'              => 'Gambio Hub API URL',
	'MODULE_PAYMENT_GAMBIO_HUB_ZONE_DESC'              => 'When a zone is selected, this payment method will be enabled for that zone only.',
	'MODULE_PAYMENT_GAMBIO_HUB_ZONE_TITLE'             => 'Payment Zone',
    'MODULE_PAYMENT_GAMBIO_HUB_REMOVE_CLIENTKEY_TEXT'  => 'Delete Hub ClientKey'
);
