<?php
/* --------------------------------------------------------------
	gambio_hub_payment_methods.lang.inc.php 2017-01-27
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'             => 'Payment Methods',
	'TAB_PAYMENT_HUB'        => 'Gambio Payment Hub',
	'TAB_PAYMENT_MISC'       => 'Miscellaneous'
];