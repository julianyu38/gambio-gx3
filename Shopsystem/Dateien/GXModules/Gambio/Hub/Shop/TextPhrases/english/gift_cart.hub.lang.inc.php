<?php
/* --------------------------------------------------------------
   gift_card.hub.lang.inc.php 2017-10-19
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
    'text_info_balance_will_be_used'     => 'Your balance will be used in checkout.',
    'text_info_balance_will_not_be_used' => 'Your balance will not be used in checkout.',
    'text_link_donotusebalance'          => 'Do not use balance',
    'text_link_usebalance'               => 'Use balance',
];
