<?php
/* --------------------------------------------------------------
	gambio_hub_account.lang.inc.php 2017-01-30
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                        => 'Account',
	'SAVE_SHOP_DATA_ERROR'              => 'The shop data couldn\'t be saved.',
	'SAVE_SHOP_DATA_SUCCESS'            => 'The shop data were saved successfully.',
	'SUCCESS'                           => 'Success',
	'ERROR'                             => 'Error',
	'GET_SHOP_DATA_ERROR'               => 'The shop data couldn\'t be loaded.',
	'GET_ORDER_STATUS_ERROR'            => 'The order statuses couldn\'t be loaded.',
	'CREATE_SESSION_ERROR'              => 'The session couldn\'t be started.',
	'UPDATE_MODULE_CONFIGURATION_ERROR' => 'The module configuration could\'t be updated.'
];