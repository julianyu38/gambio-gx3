<?php
/* --------------------------------------------------------------
	gambio_hub_money_order_plus_hub.lang.inc.php 2018-04-20
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE' => 'Payment Matching',
];
