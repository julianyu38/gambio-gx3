<?php
/* --------------------------------------------------------------
	gambio_hub_klarna_hub.lang.inc.php 2017-11-09
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'NOTIFY_KLARNA'                      => 'Klarna benachrichtigen',
	'FULL_CAPTURE'                       => 'Full Capture',
	'KLARNA_COMMUNICATION_FAILED'        => 'Die Kommunikation ggü. Klarna ist fehlgeschlagen.',
	'KLARNA_RECALCULATION_FAILED'        => 'Die Klarna-Nachberechnung ist fehlgeschlagen.',
	'SHIPMENT_TRACKING'                  => 'Sendungsverfolgung',
	'TRANSMIT_SHIPMENT_NUMBER_TO_KLARNA' => 'Sendungsnummer an Klarna übermitteln?',
	'NO'                                 => 'Nein',
	'CLOSE'                              => 'Schließen',
	'UNEXPECTED_REQUEST_ERROR'           => 'Unerwarteter Fehler bei der Abfrage.',
	'ERROR_CODE'                         => 'Fehlercode',
	'CORRELATION_ID'                     => 'Korrelations-ID',
	'ERROR'                              => 'Fehler',
	'ONLY_LOWER_AMOUNTS_ARE_ALLOWED'     => 'Der neue Betrag muss niedriger oder gleich dem der ursprünglichen Bestellung sein.',
	'OLD_VOUCHER_AMOUNT'                 => 'Alter Gutscheinbetrag',
	'NEW_VOUCHER_AMOUNT'                 => 'Neuer Gutscheinbetrag',
	'OLD_SHIPPING_COSTS'                 => 'Alte Versandkosten',
	'NEW_SHIPPING_COSTS'                 => 'Neue Versandkosten',
    'InvoiceNote'                        => 'Abgesicherte Zahlung mit Klarna. Die Zahlung wird durch unseren Partner Klarna abgewickelt. Bitte beachten Sie die von Klarna versandten Zahlungsinformationen. Sie können alle mit Klarna gezahlten Bestellungen auf klarna.com oder in der Klarna App einsehen und offene Beträge dort einfach mit einem Klick bezahlen.',
];
