<?php
/* --------------------------------------------------------------
   gambio_hub.lang.inc.php 2018-10-26
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'hubMoneyOrderPayTo'                               => 'Unsere Bankverbindung:',
	'MODULE_PAYMENT_GAMBIO_HUB_ACCOUNT_APP_URL_DESC'   => 'Nutzen Sie den App für Hub Konto spezifische Konfiguration.',
	'MODULE_PAYMENT_GAMBIO_HUB_ACCOUNT_APP_URL_TITLE'  => 'Gambio Hub Konto App URL',
	'MODULE_PAYMENT_GAMBIO_HUB_ALLOWED_DESC'           => 'Geben Sie <b>einzeln</b> die Zonen an, welche für dieses Modul erlaubt sein sollen. (z.B. AT,DE (wenn leer, werden alle Zonen erlaubt))',
	'MODULE_PAYMENT_GAMBIO_HUB_ALLOWED_TITLE'          => 'Erlaubte Zonen',
	'MODULE_PAYMENT_GAMBIO_HUB_ERROR'                  => 'Bei der Ausführung der Zahlung ist ein Fehler aufgetreten.',
	'MODULE_PAYMENT_GAMBIO_HUB_IP_LIST_URL_DESC'       => 'Url zu Liste mit IPs, die Anfragen an die Shop-Callback-Schnittstelle ausführen dürfen.',
	'MODULE_PAYMENT_GAMBIO_HUB_IP_LIST_URL_TITLE'      => 'Gambio Hub IP-Liste URL',
	'MODULE_PAYMENT_GAMBIO_HUB_IP_WHITELIST_DESC'      => 'Komma separierte IPs die whitegelisted sind (z. B. für Shops die durch Proxies laufen). Teil-Werte sind auch möglich (z. B. 192.168.0).',
	'MODULE_PAYMENT_GAMBIO_HUB_IP_WHITELIST_TITLE'     => 'Gambio Hub IP-Whitelist',
    'MODULE_PAYMENT_GAMBIO_HUB_REST_ACTIONS_URL_DESC'  => 'Url zu Liste mit Aktionen, die das Hub über die REST-API des Shops ausführen darf.',
	'MODULE_PAYMENT_GAMBIO_HUB_REST_ACTIONS_URL_TITLE' => 'Gambio Hub REST-Aktionen-Liste URL',
	'MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID_DESC'   => 'Bestellungen, welche mit diesem Modul gemacht werden, auf diesen Status setzen',
	'MODULE_PAYMENT_GAMBIO_HUB_ORDER_STATUS_ID_TITLE'  => 'Bestellstatus festlegen',
	'MODULE_PAYMENT_GAMBIO_HUB_SETTINGS_APP_URL_DESC'  => 'Nutzen Sie den App für Hub spezifische Konfiguration.',
	'MODULE_PAYMENT_GAMBIO_HUB_SETTINGS_APP_URL_TITLE' => 'Gambio Hub Settings App URL',
	'MODULE_PAYMENT_GAMBIO_HUB_SORT_ORDER_DESC'        => 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.',
	'MODULE_PAYMENT_GAMBIO_HUB_SORT_ORDER_TITLE'       => 'Anzeigereihenfolge',
	'MODULE_PAYMENT_GAMBIO_HUB_STATUS_DESC'            => 'Möchten Sie Zahlungen über das Gambio Hub akzeptieren?',
	'MODULE_PAYMENT_GAMBIO_HUB_STATUS_TITLE'           => 'Gambio Hub aktivieren',
	'MODULE_PAYMENT_GAMBIO_HUB_TEXT_DESCRIPTION'       => 'Gambio Hub',
	'MODULE_PAYMENT_GAMBIO_HUB_TEXT_INFO'              => 'Bezahlung über das Gambio Hub',
	'MODULE_PAYMENT_GAMBIO_HUB_TEXT_TITLE'             => 'Gambio Hub',
	'MODULE_PAYMENT_GAMBIO_HUB_URL_DESC'               => 'Inkludieren Sie die API-Version in URL z. B. http://domain.de/api/v1',
	'MODULE_PAYMENT_GAMBIO_HUB_URL_TITLE'              => 'Gambio Hub API URL',
	'MODULE_PAYMENT_GAMBIO_HUB_ZONE_DESC'              => 'Wenn eine Zone ausgewählt ist, gilt die Zahlungsmethode nur für diese Zone.',
	'MODULE_PAYMENT_GAMBIO_HUB_ZONE_TITLE'             => 'Zahlungszone',
    'MODULE_PAYMENT_GAMBIO_HUB_REMOVE_CLIENTKEY_TEXT'  => 'Lösche Hub ClientKey'
);
