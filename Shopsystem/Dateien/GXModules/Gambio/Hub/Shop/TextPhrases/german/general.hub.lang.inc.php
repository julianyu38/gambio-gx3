<?php
/* --------------------------------------------------------------
   general.hub.lang.inc.php 2017-10-19
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
    'NAVBAR_GV_REDEEM' => 'Gutschein/Guthaben einlösen',
];
