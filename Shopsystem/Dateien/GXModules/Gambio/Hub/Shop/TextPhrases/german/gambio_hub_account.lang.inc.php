<?php
/* --------------------------------------------------------------
	gambio_hub_account.lang.inc.php 2017-02-27
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                        => 'Account',
	'SAVE_SHOP_DATA_ERROR'              => 'Die Shopdaten konnten nicht gespeichert werden.',
	'SAVE_SHOP_DATA_SUCCESS'            => 'Die Shopdaten wurden erfolgreich gespeichert.',
	'SUCCESS'                           => 'Erfolg',
	'ERROR'                             => 'Fehler',
	'GET_SHOP_DATA_ERROR'               => 'Die Shopdaten konnten nicht geladen werden.',
	'GET_ORDER_STATUS_ERROR'            => 'Die Bestellstatus konnten nicht geladen werden.',
	'CREATE_SESSION_ERROR'              => 'Die Hub Session konnte nicht gestartet werden.',
	'UPDATE_MODULE_CONFIGURATION_ERROR' => 'Die Modulkonfiguration konnte nicht aktualisiert werden.'
];