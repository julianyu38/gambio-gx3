<?php
/* --------------------------------------------------------------
   GambioHubCheckoutConfirmation.inc.php 2017-02-14
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

require_once DIR_FS_CATALOG . 'inc/get_transfer_charge_text.inc.php'; // Required in older shop versions.

/**
 * Class GambioHubCheckoutConfirmation
 */
class GambioHubCheckoutConfirmation extends GambioHubCheckoutConfirmation_parent
{
	/**
	 * @var string
	 */
	protected $cashOnDeliveryModuleCode = 'CashOnDeliveryHub';
	
	
	public function prepare_data()
	{
		parent::prepare_data();
		
		if($_SESSION['payment'] === 'gambio_hub')
		{
			$this->content_array['PAYMENT_METHOD'] = $_SESSION['gambio_hub_payments'][$_SESSION['gambio_hub_selection']]['title'];
			
			if($_SESSION['gambio_hub_selection'] === $this->cashOnDeliveryModuleCode)
			{
				$this->content_array['COD_INFO'] = get_transfer_charge_text($this->coo_order->info['shipping_class'],
				                                                            $this->coo_order->delivery['country']['iso_code_2'],
				                                                            $this->coo_xtc_price->cStatus['customers_status_id'],
				                                                            $this->coo_xtc_price->actualCurr);
			}
		}
	}
}