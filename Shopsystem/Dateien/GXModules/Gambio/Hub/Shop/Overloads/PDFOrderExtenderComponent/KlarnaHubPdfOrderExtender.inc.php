<?php
/* --------------------------------------------------------------
   KlarnaHubPdfOrderExtender.inc.php 2017-11-09
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class KlarnaHubPdfOrderExtender extends KlarnaHubPdfOrderExtender_parent
{
    const DEFAULT_FOOTER_COLUMN = 3;
    
    public function extendOrderInfo($orderInfo)
    {
        $orderInfo = parent::extendOrderInfo($orderInfo);
        if($_GET['type'] === 'invoice')
        {
            $orderService = StaticGXCoreLoader::getService('OrderRead');
            $order = $orderService->getOrderById(new IdType((int)$_GET['oID']));
            $paymentType = $order->getPaymentType();
            $paymentModule = $paymentType->getModule();
    
            if(in_array($paymentModule, ['KlarnaPaylaterHub', 'KlarnaPaynowHub', 'KlarnaSliceitHub', 'KlarnaBanktransferHub'], true))
            {
                $languageId = $this->v_data_array['order']->info['languages_id'];
                $language = MainFactory::create('LanguageTextManager', 'gambio_hub_klarna_hub', $languageId);
                $orderInfo['KlarnaHub'] = [
                    0 => 'Klarna',
                    1 => $language->get_text('InvoiceNote'),
                ];
        
            }
        }
        return $orderInfo;
    }
    
    public function extendPdfFooter($footer)
    {
        $footer = parent::extendPdfFooter($footer);
    
        if($_GET['type'] === 'invoice')
        {
            $orderService  = StaticGXCoreLoader::getService('OrderRead');
            $order         = $orderService->getOrderById(new IdType((int)$_GET['oID']));
            $paymentType   = $order->getPaymentType();
            $paymentModule = $paymentType->getModule();
    
            if (in_array($paymentModule, ['KlarnaPaylaterHub', 'KlarnaPaynowHub', 'KlarnaSliceitHub', 'KlarnaBanktransferHub'], true)) {
                $replaceFooterColumn = gm_get_conf('GAMBIO_HUB_KLARNA_HUB_INVOICECOLUMN');
                if (null === $replaceFooterColumn) {
                    $replaceFooterColumn = self::DEFAULT_FOOTER_COLUMN;
                }
                $replaceFooterColumn = (int)$replaceFooterColumn;
                if ($replaceFooterColumn >= 0) {
                    $footer[$replaceFooterColumn] = '';
                }
            }
        }
        
        return $footer;
    }

}
