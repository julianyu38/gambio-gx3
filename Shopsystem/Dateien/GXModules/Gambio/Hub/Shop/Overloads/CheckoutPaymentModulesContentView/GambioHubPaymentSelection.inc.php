<?php
/* --------------------------------------------------------------
   GambioHubPaymentSelection.inc.php 2017-02-17
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GambioHubPaymentSelection
 */
class GambioHubPaymentSelection extends GambioHubPaymentSelection_parent
{
	/**
	 * @var string
	 */
	protected $payPalModuleCode = 'PayPalHub';
	
	
	/**
	 * Adds gambio hub module code to selected payment method name to make preselection of radio input possible.
	 *
	 * @param $paymentMethod
	 */
	public function set_selected_payment_method($paymentMethod)
	{
		if($paymentMethod === 'gambio_hub' && array_key_exists('gambio_hub_selection', $_SESSION))
		{
			$paymentMethod .= '-' . $_SESSION['gambio_hub_selection'];
		}
		
		parent::set_selected_payment_method($paymentMethod);
	}
	
	
	/**
	 * Adds new variable for Smarty, which defines if Paypal Plus is currently in use or not.
	 */
	public function prepare_data()
	{
		parent::prepare_data();
		
		if(array_key_exists('gambio_hub_payments', $_SESSION)
		   && array_key_exists($this->payPalModuleCode, $_SESSION['gambio_hub_payments'])
		)
		{
			$this->set_content_data('use_paypal_plus',
			                        $_SESSION['gambio_hub_payments'][$this->payPalModuleCode]['configuration']['use_plus']);
		}
	}
}