<?php
/* --------------------------------------------------------------
   GambioHubOrderTotal.inc.php 2017-10-12
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class GambioHubOrderTotal extends GambioHubOrderTotal_parent {
    public function process()
    {
        $order_total_array = [];
        foreach($this->modules as $moduleFile)
        {
            $class = basename($moduleFile, '.php');
            if ($GLOBALS[$class]->enabled) {
                $preProcessOrder = clone $GLOBALS['order'];
                $GLOBALS[$class]->process();
                $summaryOfChanges = [
                    'subtotal'      => $GLOBALS['order']->info['subtotal'] - $preProcessOrder->info['subtotal'],
                    'total'         => $GLOBALS['order']->info['total']    - $preProcessOrder->info['total'],
                    'tax'           => $GLOBALS['order']->info['tax']      - $preProcessOrder->info['tax'],
                    'shipping_cost' => $GLOBALS['order']->info['shipping_cost']
                                       - $preProcessOrder->info['shipping_cost'],
                    'tax_groups'    => []
                ];
                if(is_array($GLOBALS['order']->info['tax_groups']))
                {
                    foreach($GLOBALS['order']->info['tax_groups'] as $taxGroup => $taxGroupValue) {
                        $summaryOfChanges['tax_groups'][$taxGroup] = $GLOBALS['order']->info['tax_groups'][$taxGroup] -
                                                                     $preProcessOrder->info['tax_groups'][$taxGroup];
                    }
                }
                $processingResult = $GLOBALS[$class]->output;
                $processingResult = is_array($processingResult) ? $processingResult : [];
                foreach($processingResult as $otOutput) {
                    if((xtc_not_null($otOutput['title']) && xtc_not_null($otOutput['text'])) ||
                       $GLOBALS[$class]->code === 'ot_gm_tax_free')
                    {
                        if($class === 'ot_discount')
                        {
                            $summaryOfChanges['total'] += $otOutput['value'];
                        }
                        $order_total_array[] = array_merge(
                            [
                                'title' => 'not set',
                                'text'  => 'not set',
                                'value' => 0.00,
                            ],
                            $otOutput,
                            [
                                'code'       => $GLOBALS[$class]->code,
                                'sort_order' => $GLOBALS[$class]->sort_order,
                                'changes'    => $summaryOfChanges,
                            ]
                        );
                    }
                }
            }
        }
        
        return $order_total_array;
    }
}
