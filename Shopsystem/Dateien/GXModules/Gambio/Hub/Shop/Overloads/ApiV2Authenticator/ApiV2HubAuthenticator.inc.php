<?php
/* --------------------------------------------------------------
   ApiV2HubAuthenticator.inc.php 2018-11-01
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use \HubPublic\Http\CurlRequest;

/**
 * Class ApiV2HubAuthenticator
 */
class ApiV2HubAuthenticator extends ApiV2HubAuthenticator_parent
{
	/**
	 * Authorize request with Hub Credentials
	 *
	 * This method overloads the basic authentication to use special hub credentials.
	 * As a fallback this overload calls the basic auth of the parent.
	 *
	 * @param string $controllerName Name of the parent controller for this api call.
	 *
	 * @throws HttpApiV2Exception If request does not provide the "Authorization" header or if the
	 *                            credentials are invalid.
	 *
	 * @throws InvalidArgumentException If the username or password values are invalid.
	 */
	public function authorize($controllerName)
	{
		// Check if Hub authentication is allowed
		if(gm_get_conf('GAMBIO_HUB_ALLOW_REST_ACTIONS') === 'false')
		{
			parent::authorize($controllerName);
			
			return;
		}
		
		// Check for hub ip
		try
		{
			$this->checkIp();
			
			// Check for client key
			if($this->api->request->headers->get('X-CLIENT-KEY') !== gm_get_conf('GAMBIO_HUB_CLIENT_KEY'))
			{
				throw new RuntimeException('Forbidden', 403);
			}
			
			// Check if wanted resource is allowed
			$this->checkResource();
		}
		catch(\RuntimeException $exception)
		{
			parent::authorize($controllerName);
		}
	}
	
	
	/**
	 * Checks the IP of this request. Only the Hub IP is allowed to make these kind of requests.
	 */
	protected function checkIp()
	{
		$curlRequest = new CurlRequest();
		$response    = $curlRequest->setUrl(MODULE_PAYMENT_GAMBIO_HUB_IP_LIST_URL)->execute();
		
		if($response->getStatusCode() !== 200)
		{
			throw new RuntimeException('Forbidden', 403);
		}
		
		$ipList = @json_decode($response->getBody(), true);
		$ip     = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
		
		if(empty($ip))
		{
			throw new RuntimeException('Forbidden', 403);
		}
		
		if(is_array($ipList))
		{
			$valid = false;
			foreach($ipList as $hubIp)
			{
				if($hubIp === '*' || strpos($ip, $hubIp) === 0)
				{
					$valid = true;
					break;
				}
			}
			
			// Check with IP whitelist (comma separated IP values).
			if (!$valid && defined('MODULE_PAYMENT_GAMBIO_HUB_IP_WHITELIST'))
			{
				$ipWhitelist = explode(',', MODULE_PAYMENT_GAMBIO_HUB_IP_WHITELIST);
				
				foreach($ipWhitelist as $ipWhitelistEntry) {
					if(empty($ipWhitelistEntry))
					{
						continue;
					}
					
					// Will also match partial IP values like "192.168.0".
					if ($ipWhitelistEntry === '*' || strpos($ip, trim($ipWhitelistEntry)) !== false)
					{
						$valid = true;
						break;
					}
				}
			}
			
			if($valid)
			{
				return;
			}
		}
		
		throw new RuntimeException('Forbidden', 403);
	}
	
	
	/**
	 * Checks the wanted resource of this request.
	 */
	protected function checkResource()
	{
		$curlRequest = new CurlRequest();
		$response    = $curlRequest->setUrl(MODULE_PAYMENT_GAMBIO_HUB_REST_ACTIONS_URL)->execute();
		
		if($response->getStatusCode() !== 200)
		{
			throw new RuntimeException('Allowed rest actions for hub authentication could not be found.', 403);
		}
		
		$resourceList = @json_decode($response->getBody(), true);
		$resource     = substr($this->api->request->getResourceUri(), 3);
		$method       = strtoupper($this->api->request->getMethod());
		
		if(is_array($resourceList) && isset($resourceList[$method]))
		{
			$valid = false;
			foreach($resourceList[$method] as $allowedResource)
			{
				if($allowedResource === '*' || strpos($resource, $allowedResource) === 0)
				{
					$valid = true;
					break;
				}
			}
			
			if($valid)
			{
				return;
			}
		}
		
		throw new RuntimeException('Forbidden', 403);
	}
}