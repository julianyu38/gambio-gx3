<?php
/* --------------------------------------------------------------
   GambioHubPrintOrder.inc.php 2017-02-10
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GambioHubPrintOrder
 */
class GambioHubPrintOrder extends GambioHubPrintOrder_parent
{
	public function add_order_data()
	{
		parent::add_order_data();
		
		if($this->coo_order->info['payment_method'] === 'gambio_hub')
		{
			$query  = 'SELECT `gambio_hub_module_title` FROM `orders` WHERE `orders_id` = '
			          . (int)$this->coo_order->info['orders_id'];
			$result = xtc_db_query($query);
			
			$this->content_array['PAYMENT_METHOD'] = xtc_db_fetch_array($result)['gambio_hub_module_title'];
		}
	}
}