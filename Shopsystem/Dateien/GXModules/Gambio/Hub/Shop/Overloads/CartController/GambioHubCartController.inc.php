<?php
/* --------------------------------------------------------------
   GambioHubCartController.inc.php 2017-10-19
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class GambioHubCartController extends GambioHubCartController_parent
{
    public function actionUseBalance()
    {
        $_SESSION['cot_gv'] = true;
        $shoppingCartUrl = xtc_href_link('shopping_cart.php');
        return MainFactory::create('RedirectHttpControllerResponse', $shoppingCartUrl);
    }
    
    public function actionDoNotUseBalance()
    {
        $_SESSION['cot_gv'] = false;
        $shoppingCartUrl = xtc_href_link('shopping_cart.php');
        return MainFactory::create('RedirectHttpControllerResponse', $shoppingCartUrl);
    }
}
