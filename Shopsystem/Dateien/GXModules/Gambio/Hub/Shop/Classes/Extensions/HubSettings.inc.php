<?php
/* --------------------------------------------------------------
   HubSettings.inc.php 2017-07-04
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class HubSettings
 *
 * @category   System
 * @package    Extensions
 * @subpackage GambioHub
 */
class HubSettings
{
	/**
	 * @var int
	 */
	protected $curlTimeout;
	
	
	/**
	 * HubSettings constructor.
	 *
	 * @param int $curlTimeout curl timeout in seconds
	 */
	public function __construct($curlTimeout)
	{
		$this->curlTimeout = (int)$curlTimeout;
	}
	
	
	/**
	 * Returns curl timeout in seconds.
	 * 
	 * @return int
	 */
	public function getCurlTimeout()
	{
		return $this->curlTimeout;
	}
}