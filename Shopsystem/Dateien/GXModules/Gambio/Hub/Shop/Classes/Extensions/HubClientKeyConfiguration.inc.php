<?php

/* --------------------------------------------------------------
   HubClientKeyConfiguration.inc.php 2016-11-18
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use \HubPublic\ValueObjects\HubClientKey;

/**
 * Class HubClientKeyConfiguration
 *
 * @category   System
 * @package    Extensions
 * @subpackage GambioHub
 */
class HubClientKeyConfiguration implements HubClientKeyConfigurationInterface
{
	/**
	 * Configuration key.
	 *
	 * @var string
	 */
	protected $configurationKey = 'GAMBIO_HUB_CLIENT_KEY';
	
	
	/**
	 * Sets the hub client key in the database configuration table.
	 *
	 * @param \HubPublic\ValueObjects\HubClientKey $clientKey The client key to be saved.
	 *
	 * @return HubClientKeyConfiguration Returns same instance for chained method calls.
	 */
	public function set(HubClientKey $clientKey)
	{
		try
		{
			$this->get();
			
			$sql = '
				UPDATE gm_configuration SET gm_value = "' . $clientKey->asString() . '" 
				WHERE gm_key = "' . $this->configurationKey . '"
            ';
		}
		catch(Exception $exception)
		{
			$sql = '
				INSERT INTO gm_configuration (gm_key, gm_value, gm_group_id, gm_sort_order) 
				VALUES("' . $this->configurationKey . '", "' . $clientKey->asString() . '", 0, 0)
			';
		}
		
		xtc_db_query($sql, 'db_link', false);
		
		return $this;
	}
	
	
	/**
	 * Returns the hub client key from the database.
	 *
	 * @return string Returns the hub client key.
	 *
	 * @throws RuntimeException If no Gambio Hub exists.
	 */
	public function get()
	{
		$sql    = 'SELECT * FROM gm_configuration WHERE gm_key = "' . $this->configurationKey . '"';
		$query  = xtc_db_query($sql, 'db_link', false);
		$result = xtc_db_fetch_array($query);
		
		if(!$result || !array_key_exists('gm_value', $result))
		{
			throw new RuntimeException('Gambio Hub client key was not found!');
		}
		
		return $result['gm_value'];
	}
	
	
	/**
	 * Returns the hub client key as a HubClientKey instance.
	 *
	 * @return \HubPublic\ValueObjects\HubClientKey
	 */
	public function getClientKey()
	{
		return new HubClientKey($this->get());
	}
}