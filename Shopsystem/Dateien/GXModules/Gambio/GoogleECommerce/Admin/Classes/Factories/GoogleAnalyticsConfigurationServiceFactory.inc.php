<?php
/* --------------------------------------------------------------
   GoogleAnalyticsConfigurationServiceFactory.inc.php 2018-04-13
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GoogleAnalyticsConfigurationServiceFactory
 *
 * @package GoogleAnalytics
 */
class GoogleAnalyticsConfigurationServiceFactory implements GoogleAnalyticsConfigurationServiceFactoryInterface
{
	/**
	 * @var GoogleAnalyticsConfigurationReadService
	 */
	protected static $readService;
	
	/**
	 * @var GoogleAnalyticsConfigurationWriteService
	 */
	protected static $writeService;
	
	/**
	 * @var GoogleConfigurationStorage
	 */
	protected static $storage;
	
	
	/**
	 * Creates (and in memory caches) the Google Analytics Configuration Read Service.
	 *
	 * @return \GoogleAnalyticsConfigurationReadServiceInterface
	 */
	public static function readService()
	{
		if(null === static::$readService)
		{
			static::$readService = MainFactory::create(GoogleAnalyticsConfigurationReadService::class,
			                                           static::_storage());
		}
		
		return static::$readService;
	}
	
	
	/**
	 * Creates (and in memory caches) the Google Analytics Configuration Write Service.
	 *
	 * @return \GoogleAnalyticsConfigurationWriteServiceInterface
	 */
	public static function writeService()
	{
		if(null === static::$writeService)
		{
			static::$writeService = MainFactory::create(GoogleAnalyticsConfigurationWriteService::class,
			                                            static::_storage());
		}
		
		return static::$writeService;
	}
	
	
	/**
	 * Creates (and in memory caches) the Google Configuration Storage component.
	 *
	 * @return \GoogleConfigurationStorage
	 */
	protected static function _storage()
	{
		if(null === static::$storage)
		{
			static::$storage = MainFactory::create(GoogleConfigurationStorage::class,
			                                       StaticGXCoreLoader::getDatabaseQueryBuilder(),
			                                       GoogleConfigurationStorage::SCOPE_ANALYTICS);
		}
		
		return static::$storage;
	}
}