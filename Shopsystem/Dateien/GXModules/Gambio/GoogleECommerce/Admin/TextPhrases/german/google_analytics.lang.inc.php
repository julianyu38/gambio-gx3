<?php
/* --------------------------------------------------------------
   google_analytics.lang.inc.php 2018-04-05
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'page_title' => 'Google Analytics',
	
	'tracking_code_label' => 'UA Tracking Code',
	
	'invalid_ua_tracking_code_error' => 'Ungültiger Universal Analytics Tracking Code',
	'invalid_names_error'            => 'Die Namen dürfen nicht leer sein!',
	'not_connected_error'            => 'Bitte verknüpfen Sie Ihren Google AdWords Account, um fortfahren zu können.',
	
	'gx_ga_chart_content_label'         => 'Charts',
	'gx_ga_configuration_content_label' => 'Konfiguration',
	
	'gx_ga_ua_tracking_code_label' => 'UA Tracking Code',
	'gx_ga_enabled_label'          => 'Google Analytics aktivieren',
	'gx_ga_anonymize_ip_label'     => 'IP-Adresse anonymisieren',
	'gx_ga_price_net_label'        => 'Netto-Preise tracken',
	
	'gx_ga_box_impression_label'  => 'Boxen Impressionen',
	'gx_ga_list_impression_label' => 'Listen Impressionen',
	'gx_ga_product_click_label'   => 'Klick auf Produkt',
	'gx_ga_product_details_label' => 'Detailansicht von Produkt',
	'gx_ga_shopping_cart_label'   => 'Warenkorb Aktionen',
	'gx_ga_checkout_label'        => 'Checkout',
	
	'gx_ga_main_configuration_label' => 'Haupteinstellungen',
	'gx_ga_tracking_options_label'   => 'Tracking Optionen',
	'gx_ga_box_names_label'          => 'Boxennamen',
	'gx_ga_list_names_label'         => 'Listennamen',
	'gx_ga_advanced_settings_label'  => 'Erweiterte Einstellungen',
	
	'gx_ga_box_bestseller_label' => 'Bestseller',
	'gx_ga_box_specials_label'   => 'Angebote',
	'gx_ga_box_whats_new_label'  => 'Was ist neu',
	
	'gx_ga_list_bestseller_label'     => 'Bestseller',
	'gx_ga_list_specials_label'       => 'Angebote',
	'gx_ga_list_whats_new_label'      => 'Was ist neu',
	'gx_ga_list_available_soon_label' => 'Bald verfügbar',
	'gx_ga_list_top_products_label'   => 'Empfehlungen',
	
	'gx_ga_enable_dev_mode_label' => 'Entwickler-Modus aktivieren',
	
	'gx_ga_analytics_disabled_txt' => 'Google-Analytics Tracking wurde deaktiviert.',
	'gx_ga_opt_out_label'          => 'Google-Analytics opt-out url'
];
