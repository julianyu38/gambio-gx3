<?php
/* --------------------------------------------------------------
   google_analytics.lang.inc.php 2018-04-05
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'page_title' => 'Google Analytics',
	
	'tracking_code_label' => 'UA Tracking Code',
	
	'invalid_ua_tracking_code_error' => 'Invalid Universal Analytics Tracking Code',
	'invalid_names_error'            => 'The names must not be empty!',
	'not_connected_error'            => 'Please link your Google AdWords account to continue.',
	
	'gx_ga_chart_content_label'         => 'Charts',
	'gx_ga_configuration_content_label' => 'Configuration',
	
	'gx_ga_ua_tracking_code_label' => 'UA Tracking Code',
	'gx_ga_enabled_label'          => 'Enabled',
	'gx_ga_anonymize_ip_label'     => 'Anonymize IP',
	'gx_ga_price_net_label'        => 'Track net prices',
	
	'gx_ga_box_impression_label'  => 'Boxes impressions',
	'gx_ga_list_impression_label' => 'Lists impressions',
	'gx_ga_product_click_label'   => 'Product click',
	'gx_ga_product_details_label' => 'Product details',
	'gx_ga_shopping_cart_label'   => 'Shopping cart',
	'gx_ga_checkout_label'        => 'Checkout',
	
	'gx_ga_main_configuration_label' => 'Main settings',
	'gx_ga_tracking_options_label'   => 'Tracking options',
	'gx_ga_box_names_label'          => 'Box names',
	'gx_ga_list_names_label'         => 'List names',
	'gx_ga_advanced_settings_label'  => 'Advanced settings',
	
	'gx_ga_box_bestseller_label' => 'Bestseller',
	'gx_ga_box_specials_label'   => 'Specials',
	'gx_ga_box_whats_new_label'  => 'What\'s new',
	
	'gx_ga_list_bestseller_label'     => 'Bestseller',
	'gx_ga_list_specials_label'       => 'Specials',
	'gx_ga_list_whats_new_label'      => 'What\'s new',
	'gx_ga_list_available_soon_label' => 'Available soon',
	'gx_ga_list_top_products_label'   => 'Top Products',
	
	'gx_ga_enable_dev_mode_label' => 'Enable development mode',
	
	'gx_ga_analytics_disabled_txt' => 'Google-Analytics Tracking was deactivated.',
	'gx_ga_opt_out_label'          => 'Google-Analytics opt-out url'
];
