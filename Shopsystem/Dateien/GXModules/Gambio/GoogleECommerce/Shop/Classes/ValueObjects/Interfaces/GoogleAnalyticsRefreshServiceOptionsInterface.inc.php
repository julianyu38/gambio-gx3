<?php
/* --------------------------------------------------------------
   GoogleAnalyticsRefreshServiceOptionsInterface.inc.php 2018-06-19
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Interface GoogleAnalyticsRefreshServiceOptionsInterface
 */
interface GoogleAnalyticsRefreshServiceOptionsInterface
{
	/**
	 * @return string
	 */
	public function root();
	
	
	/**
	 * @return string
	 */
	public function webServer();
	
	
	/**
	 * @return string
	 */
	public function webServerWithoutProtocol();
}
