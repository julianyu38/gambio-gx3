<?php
/* --------------------------------------------------------------
   GoogleAnalyticsProxyController.inc.php 2018-06-18
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use GuzzleHttp\Client;

class GoogleAnalyticsProxyController extends HttpViewController
{
	/**
	 * Proxies the google analytics request through the shop system.
	 */
	public function actionTunnelRequest()
	{
		session_write_close();
		$query = $this->_getQueryParametersCollection()->getArray();
		
		// remove controller and action from do query param
		$doQueryParams = explode('/', $query['do']);
		array_shift($doQueryParams);
		array_shift($doQueryParams);
		
		// create the path which gets append to the www.google-analytics.com domain
		$gPath = '';
		foreach($doQueryParams as $doQueryParam)
		{
			$gPath .= '/' . $doQueryParam;
		}
		
		// parses the google path to extract the version query param
		$parsedGPath = parse_url($gPath);
		if(array_key_exists('query', $parsedGPath))
		{
			$querySegments = explode('=', $parsedGPath['query']);
			$query         = array_merge([$querySegments[0] => $querySegments[1]], $query);
		}
		unset($query['do']);
		
		// creates the final google analytics url
		$gUrl = 'https://www.google-analytics.com' . $parsedGPath['path'];
		
		// sends the analytics data to the google servers
		$finalUrl = $gUrl . '?' . http_build_query($query);
		$gCurl = curl_init($finalUrl);
		$gResponse = curl_exec($gCurl);
		curl_close($gCurl);
		
		header('Content-Type: image/gif');
		echo $gResponse;
		die;
		
		return MainFactory::create(JsonHttpControllerResponse::class,
		                           ['status' => true, 'url' => $finalUrl]);
	}
}