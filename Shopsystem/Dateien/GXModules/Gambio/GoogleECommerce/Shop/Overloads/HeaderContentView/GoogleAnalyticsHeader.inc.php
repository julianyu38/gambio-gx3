<?php

/* --------------------------------------------------------------
   GoogleAnalyticsHeader.inc.php 2018-04-10
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class representing the header content view overload for Google Analytics
 */
class GoogleAnalyticsHeader extends GoogleAnalyticsHeader_parent
{
	/**
	 * Prepare data
	 */
	public function prepare_data()
	{
		$service      = GoogleAnalyticsConfigurationServiceFactory::readService();
		$trackingCode = $service->uaTrackingCode();
		
		$this->content_array['google_analytics_enabled'] = $service->enabled();
		
		if($service->enabled() && $trackingCode !== '')
		{
			$this->_setAnalyticsData($service, $trackingCode);
		}
		
		parent::prepare_data();
	}
	
	
	/**
	 * Sets the analytics template data for the head.html template file.
	 *
	 * @param \GoogleAnalyticsConfigurationReadServiceInterface $readService  Read service.
	 * @param string                                            $trackingCode Tracking code.
	 */
	private function _setAnalyticsData(GoogleAnalyticsConfigurationReadServiceInterface $readService, $trackingCode)
	{
		$isDevModeEnabled = $readService->isDevModeEnabled();
		
		if($isDevModeEnabled)
		{
			$this->content_array['google_analytics_tracking_code']        = $trackingCode;
			$this->content_array['google_analytics_anonymize_ip_enabled'] = $readService->ipAnonymizationEnabled();
			$this->content_array['google_analytics_dev_mode_enabled']     = true;
			
			return;
		}
		
		$jsPublicPath  = DIR_WS_CATALOG . 'public/';
		$gtagFile      = $jsPublicPath
		                 . $readService->analyticsFileName(GoogleAnalyticsJsFile::gtag(GoogleAnalyticsUaTrackingCode::create($trackingCode)));
		$analyticsFile = $jsPublicPath . $readService->analyticsFileName(GoogleAnalyticsJsFile::analytics());
		$ecPluginFile  = $jsPublicPath . $readService->analyticsFileName(GoogleAnalyticsJsFile::ecPlugin());
		
		$this->content_array['google_analytics_tracking_code']        = $trackingCode;
		$this->content_array['google_analytics_gtag_js_file']         = $gtagFile;
		$this->content_array['google_analytics_analytics_js_file']    = $analyticsFile;
		$this->content_array['google_analytics_ec_plugin_js_file']    = $ecPluginFile;
		$this->content_array['google_analytics_anonymize_ip_enabled'] = $readService->ipAnonymizationEnabled();
		$this->content_array['google_analytics_dev_mode_enabled']     = false;
	}
}