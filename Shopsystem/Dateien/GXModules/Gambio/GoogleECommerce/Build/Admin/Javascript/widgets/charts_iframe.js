'use strict';

/* --------------------------------------------------------------
 google_connection_modal.js 2018-05-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx_google_analytics.widgets.module('charts_iframe', ['xhr'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Widget Reference
  *
  * @type {object}
  */

	var $this = $(this);

	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final Widget Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	/**
  * CSS styles for manually rendered iframe.
  * @type {{width: string, border: string}}
  */
	var iFrameStyles = {
		width: '100%',
		border: 'none',
		minHeight: '1000px',
		height: '100%'
	};

	var $iframe = void 0;

	// ------------------------------------------------------------------------
	// CALLBACKS
	// ------------------------------------------------------------------------
	var _renderIFrame = function _renderIFrame(response) {
		$iframe = $('<iframe/>').on('load', function () {
			return _loaded(response.accessToken);
		}).css(iFrameStyles).attr('src', response.appUrl + '#analytics/charts?lang=' + response.lang);

		$this.parent().empty().append($iframe);

		// window.addEventListener('message', function(e) {
		// 	if (e.data.type === 'send_iframe_height') {
		// 		$iframe.css({
		// 			height: (Math.ceil(Number(e.data.height)) + 5) + '.px'
		// 		});
		// 	}
		// });
	};

	var _loaded = function _loaded(accessToken) {
		$iframe[0].contentWindow.postMessage({
			type: 'send_access_token',
			accessToken: accessToken
		}, '*');
		$iframe[0].contentWindow.postMessage({
			type: 'request_iframe_height'
		}, '*');
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		jse.libs.xhr.get({
			url: './admin.php?do=GoogleOAuthAjax/getAccessToken'
		}).done(_renderIFrame);

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvd2lkZ2V0cy9jaGFydHNfaWZyYW1lLmpzIl0sIm5hbWVzIjpbImd4X2dvb2dsZV9hbmFseXRpY3MiLCJ3aWRnZXRzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsImlGcmFtZVN0eWxlcyIsIndpZHRoIiwiYm9yZGVyIiwibWluSGVpZ2h0IiwiaGVpZ2h0IiwiJGlmcmFtZSIsIl9yZW5kZXJJRnJhbWUiLCJvbiIsIl9sb2FkZWQiLCJyZXNwb25zZSIsImFjY2Vzc1Rva2VuIiwiY3NzIiwiYXR0ciIsImFwcFVybCIsImxhbmciLCJwYXJlbnQiLCJlbXB0eSIsImFwcGVuZCIsImNvbnRlbnRXaW5kb3ciLCJwb3N0TWVzc2FnZSIsInR5cGUiLCJpbml0IiwiZG9uZSIsImpzZSIsImxpYnMiLCJ4aHIiLCJnZXQiLCJ1cmwiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7Ozs7Ozs7QUFVQUEsb0JBQW9CQyxPQUFwQixDQUE0QkMsTUFBNUIsQ0FDQyxlQURELEVBR0MsQ0FBQyxLQUFELENBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXLEVBQWpCOztBQUVBOzs7OztBQUtBLEtBQU1DLFVBQVVGLEVBQUVHLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQkYsUUFBbkIsRUFBNkJILElBQTdCLENBQWhCOztBQUVBOzs7OztBQUtBLEtBQU1ELFNBQVMsRUFBZjs7QUFFQTs7OztBQUlBLEtBQU1PLGVBQWU7QUFDcEJDLFNBQU8sTUFEYTtBQUVwQkMsVUFBUSxNQUZZO0FBR3BCQyxhQUFXLFFBSFM7QUFJcEJDLFVBQVE7QUFKWSxFQUFyQjs7QUFRQSxLQUFJQyxnQkFBSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFNQyxnQkFBZ0IsU0FBaEJBLGFBQWdCLFdBQVk7QUFDakNELFlBQVVULEVBQUUsV0FBRixFQUNSVyxFQURRLENBQ0wsTUFESyxFQUNHO0FBQUEsVUFBTUMsUUFBUUMsU0FBU0MsV0FBakIsQ0FBTjtBQUFBLEdBREgsRUFFUkMsR0FGUSxDQUVKWCxZQUZJLEVBR1JZLElBSFEsQ0FHSCxLQUhHLEVBR0lILFNBQVNJLE1BQVQsR0FBa0IseUJBQWxCLEdBQThDSixTQUFTSyxJQUgzRCxDQUFWOztBQUtBbkIsUUFBTW9CLE1BQU4sR0FBZUMsS0FBZixHQUF1QkMsTUFBdkIsQ0FBOEJaLE9BQTlCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFmRDs7QUFpQkEsS0FBTUcsVUFBVSxTQUFWQSxPQUFVLGNBQWU7QUFDOUJILFVBQVEsQ0FBUixFQUFXYSxhQUFYLENBQXlCQyxXQUF6QixDQUFxQztBQUNwQ0MsU0FBTSxtQkFEOEI7QUFFcENWO0FBRm9DLEdBQXJDLEVBR0csR0FISDtBQUlBTCxVQUFRLENBQVIsRUFBV2EsYUFBWCxDQUF5QkMsV0FBekIsQ0FBcUM7QUFDcENDLFNBQU07QUFEOEIsR0FBckMsRUFFRyxHQUZIO0FBR0EsRUFSRDs7QUFVQTtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBM0IsUUFBTzRCLElBQVAsR0FBYyxVQUFTQyxJQUFULEVBQWU7QUFDNUJDLE1BQUlDLElBQUosQ0FBU0MsR0FBVCxDQUFhQyxHQUFiLENBQWlCO0FBQ2hCQyxRQUFLO0FBRFcsR0FBakIsRUFFR0wsSUFGSCxDQUVRaEIsYUFGUjs7QUFJQWdCO0FBQ0EsRUFORDs7QUFRQSxRQUFPN0IsTUFBUDtBQUNBLENBckdGIiwiZmlsZSI6IkFkbWluL0phdmFzY3JpcHQvd2lkZ2V0cy9jaGFydHNfaWZyYW1lLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiBnb29nbGVfY29ubmVjdGlvbl9tb2RhbC5qcyAyMDE4LTA1LTIyXG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuZ3hfZ29vZ2xlX2FuYWx5dGljcy53aWRnZXRzLm1vZHVsZShcblx0J2NoYXJ0c19pZnJhbWUnLFxuXHRcblx0Wyd4aHInXSxcblx0XG5cdGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcblx0XHQndXNlIHN0cmljdCc7XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVkFSSUFCTEUgREVGSU5JVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFdpZGdldCBSZWZlcmVuY2Vcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIERlZmF1bHQgT3B0aW9ucyBmb3IgV2lkZ2V0XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IGRlZmF1bHRzID0ge307XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRmluYWwgV2lkZ2V0IE9wdGlvbnNcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3Qgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgZGF0YSk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogTW9kdWxlIE9iamVjdFxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBtb2R1bGUgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBDU1Mgc3R5bGVzIGZvciBtYW51YWxseSByZW5kZXJlZCBpZnJhbWUuXG5cdFx0ICogQHR5cGUge3t3aWR0aDogc3RyaW5nLCBib3JkZXI6IHN0cmluZ319XG5cdFx0ICovXG5cdFx0Y29uc3QgaUZyYW1lU3R5bGVzID0ge1xuXHRcdFx0d2lkdGg6ICcxMDAlJyxcblx0XHRcdGJvcmRlcjogJ25vbmUnLFxuXHRcdFx0bWluSGVpZ2h0OiAnMTAwMHB4Jyxcblx0XHRcdGhlaWdodDogJzEwMCUnXG5cdFx0fTtcblx0XHRcblx0XHRcblx0XHRsZXQgJGlmcmFtZTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBDQUxMQkFDS1Ncblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHRjb25zdCBfcmVuZGVySUZyYW1lID0gcmVzcG9uc2UgPT4ge1xuXHRcdFx0JGlmcmFtZSA9ICQoJzxpZnJhbWUvPicpXG5cdFx0XHRcdC5vbignbG9hZCcsICgpID0+IF9sb2FkZWQocmVzcG9uc2UuYWNjZXNzVG9rZW4pKVxuXHRcdFx0XHQuY3NzKGlGcmFtZVN0eWxlcylcblx0XHRcdFx0LmF0dHIoJ3NyYycsIHJlc3BvbnNlLmFwcFVybCArICcjYW5hbHl0aWNzL2NoYXJ0cz9sYW5nPScgKyByZXNwb25zZS5sYW5nKTtcblx0XHRcdFxuXHRcdFx0JHRoaXMucGFyZW50KCkuZW1wdHkoKS5hcHBlbmQoJGlmcmFtZSk7XG5cdFx0XHRcblx0XHRcdC8vIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdtZXNzYWdlJywgZnVuY3Rpb24oZSkge1xuXHRcdFx0Ly8gXHRpZiAoZS5kYXRhLnR5cGUgPT09ICdzZW5kX2lmcmFtZV9oZWlnaHQnKSB7XG5cdFx0XHQvLyBcdFx0JGlmcmFtZS5jc3Moe1xuXHRcdFx0Ly8gXHRcdFx0aGVpZ2h0OiAoTWF0aC5jZWlsKE51bWJlcihlLmRhdGEuaGVpZ2h0KSkgKyA1KSArICcucHgnXG5cdFx0XHQvLyBcdFx0fSk7XG5cdFx0XHQvLyBcdH1cblx0XHRcdC8vIH0pO1xuXHRcdH07XG5cdFx0XG5cdFx0Y29uc3QgX2xvYWRlZCA9IGFjY2Vzc1Rva2VuID0+IHtcblx0XHRcdCRpZnJhbWVbMF0uY29udGVudFdpbmRvdy5wb3N0TWVzc2FnZSh7XG5cdFx0XHRcdHR5cGU6ICdzZW5kX2FjY2Vzc190b2tlbicsXG5cdFx0XHRcdGFjY2Vzc1Rva2VuXG5cdFx0XHR9LCAnKicpO1xuXHRcdFx0JGlmcmFtZVswXS5jb250ZW50V2luZG93LnBvc3RNZXNzYWdlKHtcblx0XHRcdFx0dHlwZTogJ3JlcXVlc3RfaWZyYW1lX2hlaWdodCdcblx0XHRcdH0sICcqJyk7XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSB3aWRnZXQsIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0anNlLmxpYnMueGhyLmdldCh7XG5cdFx0XHRcdHVybDogJy4vYWRtaW4ucGhwP2RvPUdvb2dsZU9BdXRoQWpheC9nZXRBY2Nlc3NUb2tlbidcblx0XHRcdH0pLmRvbmUoX3JlbmRlcklGcmFtZSk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH1cbik7XG4iXX0=
