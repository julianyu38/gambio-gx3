'use strict';

/* --------------------------------------------------------------
 google_connection_iframe.js 2018-05-18
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx_google_oauth.widgets.module('google_connection_iframe', [], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Widget Reference
  *
  * @type {object}
  */

	var $this = $(this);

	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	var defaults = {};

	/**
  * Final Widget Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	/**
  * CSS styles for manually rendered iframe.
  * @type {{width: string, border: string}}
  */
	var iFrameStyles = {
		width: '100%',
		border: 'none',
		'min-height': '180px'
	};

	/**
  * Manually rendered iframe element.
  * Member is available after ::renderIframe function execution.
  */
	var $connectionIFrame = void 0;

	// ------------------------------------------------------------------------
	// FUNCTIONALITY
	// ------------------------------------------------------------------------
	/**
  * Validates widgets and check that all required options exists.
  */
	var validateOptions = function validateOptions() {
		optionExist('url');
		optionExist('origin');
		optionExist('language');
		optionExist('error');
		optionExist('from');
		optionExist('version');
	};

	/**
  * Checks if given option exists and throws error if not.
  *
  * @param {string} option
  */
	var optionExist = function optionExist(option) {
		if (!(option in options)) {
			throw new Error('Required option "' + option + '" is missing');
		}
	};

	// ------------------------------------------------------------------------
	// EVENT HANDLERS
	// ------------------------------------------------------------------------
	/**
  * Renders (manually) the "google connecting iframe".
  * The iframe gets rendered by javascript to improve the post messaging.
  */
	var renderIframe = function renderIframe() {
		var iFrameUrl = options.url + '?origin=' + options.origin + '&from=' + options.from + '&version=' + options.version + '&language=' + options.language + '&error=' + options.error;

		$connectionIFrame = $('<iframe/>').attr('src', './GXModules/Gambio/GoogleOAuth/Admin/Html/initial_iframe_content.html').on('load', requestIframeHeight).css(iFrameStyles);
		$this.parent().empty().append($connectionIFrame);
		$connectionIFrame.attr('src', iFrameUrl);
	};

	/**
  * Requests the iframe height via post message.
  * @param e
  */
	var requestIframeHeight = function requestIframeHeight(e) {
		e.target.contentWindow.postMessage({
			type: 'request_iframe_height'
		}, '*');
	};

	/**
  * Post message response processing for iframe height.
  * @param e
  */
	var responseIframeHeight = function responseIframeHeight(e) {
		if (e.data.type === 'response_iframe_height') {
			$connectionIFrame.css({ 'height': e.data.height.toString() + '.px' });
		}
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		validateOptions();

		window.addEventListener('message', responseIframeHeight);
		renderIframe();

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvd2lkZ2V0cy9nb29nbGVfY29ubmVjdGlvbl9pZnJhbWUuanMiXSwibmFtZXMiOlsiZ3hfZ29vZ2xlX29hdXRoIiwid2lkZ2V0cyIsIm1vZHVsZSIsImRhdGEiLCIkdGhpcyIsIiQiLCJkZWZhdWx0cyIsIm9wdGlvbnMiLCJleHRlbmQiLCJpRnJhbWVTdHlsZXMiLCJ3aWR0aCIsImJvcmRlciIsIiRjb25uZWN0aW9uSUZyYW1lIiwidmFsaWRhdGVPcHRpb25zIiwib3B0aW9uRXhpc3QiLCJvcHRpb24iLCJFcnJvciIsInJlbmRlcklmcmFtZSIsImlGcmFtZVVybCIsInVybCIsIm9yaWdpbiIsImZyb20iLCJ2ZXJzaW9uIiwibGFuZ3VhZ2UiLCJlcnJvciIsImF0dHIiLCJvbiIsInJlcXVlc3RJZnJhbWVIZWlnaHQiLCJjc3MiLCJwYXJlbnQiLCJlbXB0eSIsImFwcGVuZCIsImUiLCJ0YXJnZXQiLCJjb250ZW50V2luZG93IiwicG9zdE1lc3NhZ2UiLCJ0eXBlIiwicmVzcG9uc2VJZnJhbWVIZWlnaHQiLCJoZWlnaHQiLCJ0b1N0cmluZyIsImluaXQiLCJkb25lIiwid2luZG93IiwiYWRkRXZlbnRMaXN0ZW5lciJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxnQkFBZ0JDLE9BQWhCLENBQXdCQyxNQUF4QixDQUNDLDBCQURELEVBR0MsRUFIRCxFQUtDLFVBQVNDLElBQVQsRUFBZTs7QUFFZDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQUtBLEtBQU1DLFFBQVFDLEVBQUUsSUFBRixDQUFkOztBQUVBOzs7OztBQUtBLEtBQU1DLFdBQVcsRUFBakI7O0FBRUE7Ozs7O0FBS0EsS0FBTUMsVUFBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUQsU0FBUyxFQUFmOztBQUVBOzs7O0FBSUEsS0FBTU8sZUFBZTtBQUNwQkMsU0FBTyxNQURhO0FBRXBCQyxVQUFRLE1BRlk7QUFHcEIsZ0JBQWM7QUFITSxFQUFyQjs7QUFNQTs7OztBQUlBLEtBQUlDLDBCQUFKOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxLQUFNQyxrQkFBa0IsU0FBbEJBLGVBQWtCLEdBQU07QUFDN0JDLGNBQVksS0FBWjtBQUNBQSxjQUFZLFFBQVo7QUFDQUEsY0FBWSxVQUFaO0FBQ0FBLGNBQVksT0FBWjtBQUNBQSxjQUFZLE1BQVo7QUFDQUEsY0FBWSxTQUFaO0FBQ0EsRUFQRDs7QUFTQTs7Ozs7QUFLQSxLQUFNQSxjQUFjLFNBQWRBLFdBQWMsU0FBVTtBQUM3QixNQUFJLEVBQUVDLFVBQVVSLE9BQVosQ0FBSixFQUEwQjtBQUN6QixTQUFNLElBQUlTLEtBQUosQ0FBVSxzQkFBc0JELE1BQXRCLEdBQStCLGNBQXpDLENBQU47QUFDQTtBQUNELEVBSkQ7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQSxLQUFNRSxlQUFlLFNBQWZBLFlBQWUsR0FBTTtBQUMxQixNQUFNQyxZQUFZWCxRQUFRWSxHQUFSLEdBQWMsVUFBZCxHQUEyQlosUUFBUWEsTUFBbkMsR0FBNEMsUUFBNUMsR0FBdURiLFFBQVFjLElBQS9ELEdBQXNFLFdBQXRFLEdBQ2ZkLFFBQVFlLE9BRE8sR0FDRyxZQURILEdBQ2tCZixRQUFRZ0IsUUFEMUIsR0FDcUMsU0FEckMsR0FDaURoQixRQUFRaUIsS0FEM0U7O0FBR0FaLHNCQUFvQlAsRUFBRSxXQUFGLEVBQ2xCb0IsSUFEa0IsQ0FDYixLQURhLEVBQ04sdUVBRE0sRUFFbEJDLEVBRmtCLENBRWYsTUFGZSxFQUVQQyxtQkFGTyxFQUdsQkMsR0FIa0IsQ0FHZG5CLFlBSGMsQ0FBcEI7QUFJQUwsUUFBTXlCLE1BQU4sR0FBZUMsS0FBZixHQUF1QkMsTUFBdkIsQ0FBOEJuQixpQkFBOUI7QUFDQUEsb0JBQWtCYSxJQUFsQixDQUF1QixLQUF2QixFQUE4QlAsU0FBOUI7QUFDQSxFQVZEOztBQVlBOzs7O0FBSUEsS0FBTVMsc0JBQXNCLFNBQXRCQSxtQkFBc0IsSUFBSztBQUNoQ0ssSUFBRUMsTUFBRixDQUFTQyxhQUFULENBQXVCQyxXQUF2QixDQUFtQztBQUNsQ0MsU0FBTTtBQUQ0QixHQUFuQyxFQUVHLEdBRkg7QUFHQSxFQUpEOztBQU1BOzs7O0FBSUEsS0FBTUMsdUJBQXVCLFNBQXZCQSxvQkFBdUIsSUFBSztBQUNqQyxNQUFJTCxFQUFFN0IsSUFBRixDQUFPaUMsSUFBUCxLQUFnQix3QkFBcEIsRUFBOEM7QUFDN0N4QixxQkFBa0JnQixHQUFsQixDQUFzQixFQUFDLFVBQVVJLEVBQUU3QixJQUFGLENBQU9tQyxNQUFQLENBQWNDLFFBQWQsS0FBMkIsS0FBdEMsRUFBdEI7QUFDQTtBQUNELEVBSkQ7O0FBTUE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQXJDLFFBQU9zQyxJQUFQLEdBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQzVCNUI7O0FBRUE2QixTQUFPQyxnQkFBUCxDQUF3QixTQUF4QixFQUFtQ04sb0JBQW5DO0FBQ0FwQjs7QUFFQXdCO0FBQ0EsRUFQRDs7QUFTQSxRQUFPdkMsTUFBUDtBQUNBLENBM0lGIiwiZmlsZSI6IkFkbWluL0phdmFzY3JpcHQvd2lkZ2V0cy9nb29nbGVfY29ubmVjdGlvbl9pZnJhbWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGdvb2dsZV9jb25uZWN0aW9uX2lmcmFtZS5qcyAyMDE4LTA1LTE4XG4gR2FtYmlvIEdtYkhcbiBodHRwOi8vd3d3LmdhbWJpby5kZVxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxuIFJlbGVhc2VkIHVuZGVyIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSAoVmVyc2lvbiAyKVxuIFtodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXVxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuZ3hfZ29vZ2xlX29hdXRoLndpZGdldHMubW9kdWxlKFxuXHQnZ29vZ2xlX2Nvbm5lY3Rpb25faWZyYW1lJyxcblx0XG5cdFtdLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogV2lkZ2V0IFJlZmVyZW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zIGZvciBXaWRnZXRcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBGaW5hbCBXaWRnZXQgT3B0aW9uc1xuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBkYXRhKTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBNb2R1bGUgT2JqZWN0XG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG1vZHVsZSA9IHt9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENTUyBzdHlsZXMgZm9yIG1hbnVhbGx5IHJlbmRlcmVkIGlmcmFtZS5cblx0XHQgKiBAdHlwZSB7e3dpZHRoOiBzdHJpbmcsIGJvcmRlcjogc3RyaW5nfX1cblx0XHQgKi9cblx0XHRjb25zdCBpRnJhbWVTdHlsZXMgPSB7XG5cdFx0XHR3aWR0aDogJzEwMCUnLFxuXHRcdFx0Ym9yZGVyOiAnbm9uZScsXG5cdFx0XHQnbWluLWhlaWdodCc6ICcxODBweCdcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1hbnVhbGx5IHJlbmRlcmVkIGlmcmFtZSBlbGVtZW50LlxuXHRcdCAqIE1lbWJlciBpcyBhdmFpbGFibGUgYWZ0ZXIgOjpyZW5kZXJJZnJhbWUgZnVuY3Rpb24gZXhlY3V0aW9uLlxuXHRcdCAqL1xuXHRcdGxldCAkY29ubmVjdGlvbklGcmFtZTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBGVU5DVElPTkFMSVRZXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0LyoqXG5cdFx0ICogVmFsaWRhdGVzIHdpZGdldHMgYW5kIGNoZWNrIHRoYXQgYWxsIHJlcXVpcmVkIG9wdGlvbnMgZXhpc3RzLlxuXHRcdCAqL1xuXHRcdGNvbnN0IHZhbGlkYXRlT3B0aW9ucyA9ICgpID0+IHtcblx0XHRcdG9wdGlvbkV4aXN0KCd1cmwnKTtcblx0XHRcdG9wdGlvbkV4aXN0KCdvcmlnaW4nKTtcblx0XHRcdG9wdGlvbkV4aXN0KCdsYW5ndWFnZScpO1xuXHRcdFx0b3B0aW9uRXhpc3QoJ2Vycm9yJyk7XG5cdFx0XHRvcHRpb25FeGlzdCgnZnJvbScpO1xuXHRcdFx0b3B0aW9uRXhpc3QoJ3ZlcnNpb24nKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIENoZWNrcyBpZiBnaXZlbiBvcHRpb24gZXhpc3RzIGFuZCB0aHJvd3MgZXJyb3IgaWYgbm90LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IG9wdGlvblxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbkV4aXN0ID0gb3B0aW9uID0+IHtcblx0XHRcdGlmICghKG9wdGlvbiBpbiBvcHRpb25zKSkge1xuXHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1JlcXVpcmVkIG9wdGlvbiBcIicgKyBvcHRpb24gKyAnXCIgaXMgbWlzc2luZycpO1xuXHRcdFx0fVxuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gRVZFTlQgSEFORExFUlNcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvKipcblx0XHQgKiBSZW5kZXJzIChtYW51YWxseSkgdGhlIFwiZ29vZ2xlIGNvbm5lY3RpbmcgaWZyYW1lXCIuXG5cdFx0ICogVGhlIGlmcmFtZSBnZXRzIHJlbmRlcmVkIGJ5IGphdmFzY3JpcHQgdG8gaW1wcm92ZSB0aGUgcG9zdCBtZXNzYWdpbmcuXG5cdFx0ICovXG5cdFx0Y29uc3QgcmVuZGVySWZyYW1lID0gKCkgPT4ge1xuXHRcdFx0Y29uc3QgaUZyYW1lVXJsID0gb3B0aW9ucy51cmwgKyAnP29yaWdpbj0nICsgb3B0aW9ucy5vcmlnaW4gKyAnJmZyb209JyArIG9wdGlvbnMuZnJvbSArICcmdmVyc2lvbj0nXG5cdFx0XHRcdCsgb3B0aW9ucy52ZXJzaW9uICsgJyZsYW5ndWFnZT0nICsgb3B0aW9ucy5sYW5ndWFnZSArICcmZXJyb3I9JyArIG9wdGlvbnMuZXJyb3I7XG5cdFx0XHRcblx0XHRcdCRjb25uZWN0aW9uSUZyYW1lID0gJCgnPGlmcmFtZS8+Jylcblx0XHRcdFx0LmF0dHIoJ3NyYycsICcuL0dYTW9kdWxlcy9HYW1iaW8vR29vZ2xlT0F1dGgvQWRtaW4vSHRtbC9pbml0aWFsX2lmcmFtZV9jb250ZW50Lmh0bWwnKVxuXHRcdFx0XHQub24oJ2xvYWQnLCByZXF1ZXN0SWZyYW1lSGVpZ2h0KVxuXHRcdFx0XHQuY3NzKGlGcmFtZVN0eWxlcyk7XG5cdFx0XHQkdGhpcy5wYXJlbnQoKS5lbXB0eSgpLmFwcGVuZCgkY29ubmVjdGlvbklGcmFtZSk7XG5cdFx0XHQkY29ubmVjdGlvbklGcmFtZS5hdHRyKCdzcmMnLCBpRnJhbWVVcmwpO1xuXHRcdH07XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogUmVxdWVzdHMgdGhlIGlmcmFtZSBoZWlnaHQgdmlhIHBvc3QgbWVzc2FnZS5cblx0XHQgKiBAcGFyYW0gZVxuXHRcdCAqL1xuXHRcdGNvbnN0IHJlcXVlc3RJZnJhbWVIZWlnaHQgPSBlID0+IHtcblx0XHRcdGUudGFyZ2V0LmNvbnRlbnRXaW5kb3cucG9zdE1lc3NhZ2Uoe1xuXHRcdFx0XHR0eXBlOiAncmVxdWVzdF9pZnJhbWVfaGVpZ2h0J1xuXHRcdFx0fSwgJyonKTtcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIFBvc3QgbWVzc2FnZSByZXNwb25zZSBwcm9jZXNzaW5nIGZvciBpZnJhbWUgaGVpZ2h0LlxuXHRcdCAqIEBwYXJhbSBlXG5cdFx0ICovXG5cdFx0Y29uc3QgcmVzcG9uc2VJZnJhbWVIZWlnaHQgPSBlID0+IHtcblx0XHRcdGlmIChlLmRhdGEudHlwZSA9PT0gJ3Jlc3BvbnNlX2lmcmFtZV9oZWlnaHQnKSB7XG5cdFx0XHRcdCRjb25uZWN0aW9uSUZyYW1lLmNzcyh7J2hlaWdodCc6IGUuZGF0YS5oZWlnaHQudG9TdHJpbmcoKSArICcucHgnfSk7XG5cdFx0XHR9XG5cdFx0fTtcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBJTklUSUFMSVpBVElPTlxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEluaXRpYWxpemUgbWV0aG9kIG9mIHRoZSB3aWRnZXQsIGNhbGxlZCBieSB0aGUgZW5naW5lLlxuXHRcdCAqL1xuXHRcdG1vZHVsZS5pbml0ID0gZnVuY3Rpb24oZG9uZSkge1xuXHRcdFx0dmFsaWRhdGVPcHRpb25zKCk7XG5cdFx0XHRcblx0XHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdtZXNzYWdlJywgcmVzcG9uc2VJZnJhbWVIZWlnaHQpO1xuXHRcdFx0cmVuZGVySWZyYW1lKCk7XG5cdFx0XHRcblx0XHRcdGRvbmUoKTtcblx0XHR9O1xuXHRcdFxuXHRcdHJldHVybiBtb2R1bGU7XG5cdH0pO1xuIl19
