'use strict';

/* --------------------------------------------------------------
 google_connection_modal.js 2018-05-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

gx_google_oauth.widgets.module('google_connection_modal', ['xhr'], function (data) {

	'use strict';

	// ------------------------------------------------------------------------
	// VARIABLE DEFINITION
	// ------------------------------------------------------------------------

	/**
  * Widget Reference
  *
  * @type {object}
  */

	var $this = $(this);

	/**
  * Default Options for Widget
  *
  * @type {object}
  */
	var defaults = {
		'from': 'GoogleOAuth',
		'error': '0'
	};

	/**
  * Final Widget Options
  *
  * @type {object}
  */
	var options = $.extend(true, {}, defaults, data);

	/**
  * Module Object
  *
  * @type {object}
  */
	var module = {};

	// ------------------------------------------------------------------------
	// CALLBACKS
	// ------------------------------------------------------------------------
	/**
  * Renders the connection modal.
  *
  * @param {object} response XmlHttpRequests response.
  * @private
  */
	var _renderModal = function _renderModal(response) {
		$this.addClass('adwords-account modal fade').attr('tabindex', '-1').attr('role', 'dialog');

		var $modalDialog = $('<div/>', {
			'class': 'modal-dialog'
		}).appendTo($this);

		var $modalContent = $('<div/>', {
			'class': 'modal-content'
		}).appendTo($modalDialog);

		_renderModalHeader(response).appendTo($modalContent);
		_renderModalBody(response).appendTo($modalContent);

		gx_google_oauth.widgets.init($this);

		$this.modal('show');
	};

	// ------------------------------------------------------------------------
	// UTILITY
	// ------------------------------------------------------------------------
	/**
  * Renders the modal header.
  *
  * @param {object} response XmlHttpRequests response.
  * @param {string} response.modalTitle Title of modal.
  *
  * @returns {jQuery|HTMLElement}
  * @private
  */
	var _renderModalHeader = function _renderModalHeader(response) {
		var $modalHeader = $('<div/>', {
			'class': 'modal-header'
		});

		var $modalHeaderCloseBtn = $('<button/>', {
			'type': 'button',
			'class': 'close',
			'data-dismiss': 'modal',
			'aria-label': 'close'
		}).appendTo($modalHeader);

		$('<span/>', {
			'aria-hidden': 'true',
			'html': '&times;'
		}).appendTo($modalHeaderCloseBtn);

		$('<h4/>', {
			'class': 'modal-title',
			'text': response.modalTitle
		}).appendTo($modalHeader);

		return $modalHeader;
	};

	/**
  * Renders the modal body.
  *
  * @param {object} response XmlHttpRequests response.
  * @param {string} response.iFrameUrl Url of connection iframe.
  * @param {string} response.origin Shop origin, required for redirection of api.
  * @param {string} response.language Language code of currently activated language.
  * @param {string} response.version Current shop version.
  * @returns {jQuery|HTMLElement}
  * @private
  */
	var _renderModalBody = function _renderModalBody(response) {
		var $modalBody = $('<div/>', {
			'class': 'modal-body'
		});

		$('<div/>', {
			'data-gx_google_oauth-widget': 'google_connection_iframe',
			'data-google_connection_iframe-url': response.iFrameUrl,
			'data-google_connection_iframe-origin': response.origin,
			'data-google_connection_iframe-language': response.language,
			'data-google_connection_iframe-error': options.error,
			'data-google_connection_iframe-from': options.from,
			'data-google_connection_iframe-version': response.version
		}).appendTo($modalBody);

		return $modalBody;
	};

	// ------------------------------------------------------------------------
	// INITIALIZATION
	// ------------------------------------------------------------------------

	/**
  * Initialize method of the widget, called by the engine.
  */
	module.init = function (done) {
		jse.libs.xhr.get({ url: './admin.php?do=GoogleOAuthAjax/connectionModal' }).done(function (response) {
			if (!response.connected) {
				_renderModal(response);
			}
		}).fail(function () {
			throw new Error('Failed to load modal data!');
		});

		done();
	};

	return module;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFkbWluL0phdmFzY3JpcHQvd2lkZ2V0cy9nb29nbGVfY29ubmVjdGlvbl9tb2RhbC5qcyJdLCJuYW1lcyI6WyJneF9nb29nbGVfb2F1dGgiLCJ3aWRnZXRzIiwibW9kdWxlIiwiZGF0YSIsIiR0aGlzIiwiJCIsImRlZmF1bHRzIiwib3B0aW9ucyIsImV4dGVuZCIsIl9yZW5kZXJNb2RhbCIsImFkZENsYXNzIiwiYXR0ciIsIiRtb2RhbERpYWxvZyIsImFwcGVuZFRvIiwiJG1vZGFsQ29udGVudCIsIl9yZW5kZXJNb2RhbEhlYWRlciIsInJlc3BvbnNlIiwiX3JlbmRlck1vZGFsQm9keSIsImluaXQiLCJtb2RhbCIsIiRtb2RhbEhlYWRlciIsIiRtb2RhbEhlYWRlckNsb3NlQnRuIiwibW9kYWxUaXRsZSIsIiRtb2RhbEJvZHkiLCJpRnJhbWVVcmwiLCJvcmlnaW4iLCJsYW5ndWFnZSIsImVycm9yIiwiZnJvbSIsInZlcnNpb24iLCJkb25lIiwianNlIiwibGlicyIsInhociIsImdldCIsInVybCIsImNvbm5lY3RlZCIsImZhaWwiLCJFcnJvciJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7Ozs7OztBQVVBQSxnQkFBZ0JDLE9BQWhCLENBQXdCQyxNQUF4QixDQUNDLHlCQURELEVBR0MsQ0FBQyxLQUFELENBSEQsRUFLQyxVQUFTQyxJQUFULEVBQWU7O0FBRWQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFLQSxLQUFNQyxRQUFRQyxFQUFFLElBQUYsQ0FBZDs7QUFFQTs7Ozs7QUFLQSxLQUFNQyxXQUFXO0FBQ2hCLFVBQVEsYUFEUTtBQUVoQixXQUFTO0FBRk8sRUFBakI7O0FBS0E7Ozs7O0FBS0EsS0FBTUMsVUFBVUYsRUFBRUcsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CRixRQUFuQixFQUE2QkgsSUFBN0IsQ0FBaEI7O0FBRUE7Ozs7O0FBS0EsS0FBTUQsU0FBUyxFQUFmOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQSxLQUFNTyxlQUFlLFNBQWZBLFlBQWUsV0FBWTtBQUNoQ0wsUUFBTU0sUUFBTixDQUFlLDRCQUFmLEVBQTZDQyxJQUE3QyxDQUFrRCxVQUFsRCxFQUE4RCxJQUE5RCxFQUFvRUEsSUFBcEUsQ0FBeUUsTUFBekUsRUFBaUYsUUFBakY7O0FBRUEsTUFBTUMsZUFBZVAsRUFBRSxRQUFGLEVBQVk7QUFDaEMsWUFBUztBQUR1QixHQUFaLEVBRWxCUSxRQUZrQixDQUVUVCxLQUZTLENBQXJCOztBQUlBLE1BQU1VLGdCQUFnQlQsRUFBRSxRQUFGLEVBQVk7QUFDakMsWUFBUztBQUR3QixHQUFaLEVBRW5CUSxRQUZtQixDQUVWRCxZQUZVLENBQXRCOztBQUlBRyxxQkFBbUJDLFFBQW5CLEVBQTZCSCxRQUE3QixDQUFzQ0MsYUFBdEM7QUFDQUcsbUJBQWlCRCxRQUFqQixFQUEyQkgsUUFBM0IsQ0FBb0NDLGFBQXBDOztBQUVBZCxrQkFBZ0JDLE9BQWhCLENBQXdCaUIsSUFBeEIsQ0FBNkJkLEtBQTdCOztBQUVBQSxRQUFNZSxLQUFOLENBQVksTUFBWjtBQUNBLEVBakJEOztBQW1CQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0EsS0FBTUoscUJBQXFCLFNBQXJCQSxrQkFBcUIsV0FBWTtBQUN0QyxNQUFNSyxlQUFlZixFQUFFLFFBQUYsRUFBWTtBQUNoQyxZQUFTO0FBRHVCLEdBQVosQ0FBckI7O0FBSUEsTUFBTWdCLHVCQUF1QmhCLEVBQUUsV0FBRixFQUFlO0FBQzNDLFdBQVEsUUFEbUM7QUFFM0MsWUFBUyxPQUZrQztBQUczQyxtQkFBZ0IsT0FIMkI7QUFJM0MsaUJBQWM7QUFKNkIsR0FBZixFQUsxQlEsUUFMMEIsQ0FLakJPLFlBTGlCLENBQTdCOztBQU9BZixJQUFFLFNBQUYsRUFBYTtBQUNaLGtCQUFlLE1BREg7QUFFWixXQUFRO0FBRkksR0FBYixFQUdHUSxRQUhILENBR1lRLG9CQUhaOztBQUtBaEIsSUFBRSxPQUFGLEVBQVc7QUFDVixZQUFTLGFBREM7QUFFVixXQUFRVyxTQUFTTTtBQUZQLEdBQVgsRUFHR1QsUUFISCxDQUdZTyxZQUhaOztBQUtBLFNBQU9BLFlBQVA7QUFDQSxFQXZCRDs7QUF5QkE7Ozs7Ozs7Ozs7O0FBV0EsS0FBTUgsbUJBQW1CLFNBQW5CQSxnQkFBbUIsV0FBWTtBQUNwQyxNQUFNTSxhQUFhbEIsRUFBRSxRQUFGLEVBQVk7QUFDOUIsWUFBUztBQURxQixHQUFaLENBQW5COztBQUlBQSxJQUFFLFFBQUYsRUFBWTtBQUNYLGtDQUErQiwwQkFEcEI7QUFFWCx3Q0FBcUNXLFNBQVNRLFNBRm5DO0FBR1gsMkNBQXdDUixTQUFTUyxNQUh0QztBQUlYLDZDQUEwQ1QsU0FBU1UsUUFKeEM7QUFLWCwwQ0FBdUNuQixRQUFRb0IsS0FMcEM7QUFNWCx5Q0FBc0NwQixRQUFRcUIsSUFObkM7QUFPWCw0Q0FBeUNaLFNBQVNhO0FBUHZDLEdBQVosRUFRR2hCLFFBUkgsQ0FRWVUsVUFSWjs7QUFVQSxTQUFPQSxVQUFQO0FBQ0EsRUFoQkQ7O0FBa0JBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0FyQixRQUFPZ0IsSUFBUCxHQUFjLFVBQVNZLElBQVQsRUFBZTtBQUM1QkMsTUFBSUMsSUFBSixDQUFTQyxHQUFULENBQWFDLEdBQWIsQ0FBaUIsRUFBQ0MsS0FBSyxnREFBTixFQUFqQixFQUNFTCxJQURGLENBQ08sb0JBQVk7QUFDakIsT0FBSSxDQUFDZCxTQUFTb0IsU0FBZCxFQUF5QjtBQUN4QjNCLGlCQUFhTyxRQUFiO0FBQ0E7QUFDRCxHQUxGLEVBTUVxQixJQU5GLENBTU8sWUFBTTtBQUNYLFNBQU0sSUFBSUMsS0FBSixDQUFVLDRCQUFWLENBQU47QUFDQSxHQVJGOztBQVVBUjtBQUNBLEVBWkQ7O0FBY0EsUUFBTzVCLE1BQVA7QUFDQSxDQWhLRiIsImZpbGUiOiJBZG1pbi9KYXZhc2NyaXB0L3dpZGdldHMvZ29vZ2xlX2Nvbm5lY3Rpb25fbW9kYWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuIGdvb2dsZV9jb25uZWN0aW9uX21vZGFsLmpzIDIwMTgtMDUtMjJcbiBHYW1iaW8gR21iSFxuIGh0dHA6Ly93d3cuZ2FtYmlvLmRlXG4gQ29weXJpZ2h0IChjKSAyMDE4IEdhbWJpbyBHbWJIXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXG4gW2h0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxdXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5neF9nb29nbGVfb2F1dGgud2lkZ2V0cy5tb2R1bGUoXG5cdCdnb29nbGVfY29ubmVjdGlvbl9tb2RhbCcsXG5cdFxuXHRbJ3hociddLFxuXHRcblx0ZnVuY3Rpb24oZGF0YSkge1xuXHRcdFxuXHRcdCd1c2Ugc3RyaWN0Jztcblx0XHRcblx0XHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblx0XHQvLyBWQVJJQUJMRSBERUZJTklUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogV2lkZ2V0IFJlZmVyZW5jZVxuXHRcdCAqXG5cdFx0ICogQHR5cGUge29iamVjdH1cblx0XHQgKi9cblx0XHRjb25zdCAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogRGVmYXVsdCBPcHRpb25zIGZvciBXaWRnZXRcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgZGVmYXVsdHMgPSB7XG5cdFx0XHQnZnJvbSc6ICdHb29nbGVPQXV0aCcsXG5cdFx0XHQnZXJyb3InOiAnMCdcblx0XHR9O1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIEZpbmFsIFdpZGdldCBPcHRpb25zXG5cdFx0ICpcblx0XHQgKiBAdHlwZSB7b2JqZWN0fVxuXHRcdCAqL1xuXHRcdGNvbnN0IG9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIGRhdGEpO1xuXHRcdFxuXHRcdC8qKlxuXHRcdCAqIE1vZHVsZSBPYmplY3Rcblx0XHQgKlxuXHRcdCAqIEB0eXBlIHtvYmplY3R9XG5cdFx0ICovXG5cdFx0Y29uc3QgbW9kdWxlID0ge307XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gQ0FMTEJBQ0tTXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0LyoqXG5cdFx0ICogUmVuZGVycyB0aGUgY29ubmVjdGlvbiBtb2RhbC5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSByZXNwb25zZSBYbWxIdHRwUmVxdWVzdHMgcmVzcG9uc2UuXG5cdFx0ICogQHByaXZhdGVcblx0XHQgKi9cblx0XHRjb25zdCBfcmVuZGVyTW9kYWwgPSByZXNwb25zZSA9PiB7XG5cdFx0XHQkdGhpcy5hZGRDbGFzcygnYWR3b3Jkcy1hY2NvdW50IG1vZGFsIGZhZGUnKS5hdHRyKCd0YWJpbmRleCcsICctMScpLmF0dHIoJ3JvbGUnLCAnZGlhbG9nJyk7XG5cdFx0XHRcblx0XHRcdGNvbnN0ICRtb2RhbERpYWxvZyA9ICQoJzxkaXYvPicsIHtcblx0XHRcdFx0J2NsYXNzJzogJ21vZGFsLWRpYWxvZydcblx0XHRcdH0pLmFwcGVuZFRvKCR0aGlzKTtcblx0XHRcdFxuXHRcdFx0Y29uc3QgJG1vZGFsQ29udGVudCA9ICQoJzxkaXYvPicsIHtcblx0XHRcdFx0J2NsYXNzJzogJ21vZGFsLWNvbnRlbnQnXG5cdFx0XHR9KS5hcHBlbmRUbygkbW9kYWxEaWFsb2cpO1xuXHRcdFx0XG5cdFx0XHRfcmVuZGVyTW9kYWxIZWFkZXIocmVzcG9uc2UpLmFwcGVuZFRvKCRtb2RhbENvbnRlbnQpO1xuXHRcdFx0X3JlbmRlck1vZGFsQm9keShyZXNwb25zZSkuYXBwZW5kVG8oJG1vZGFsQ29udGVudCk7XG5cdFx0XHRcblx0XHRcdGd4X2dvb2dsZV9vYXV0aC53aWRnZXRzLmluaXQoJHRoaXMpO1xuXHRcdFx0XG5cdFx0XHQkdGhpcy5tb2RhbCgnc2hvdycpO1xuXHRcdH07XG5cdFx0XG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0Ly8gVVRJTElUWVxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8qKlxuXHRcdCAqIFJlbmRlcnMgdGhlIG1vZGFsIGhlYWRlci5cblx0XHQgKlxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSByZXNwb25zZSBYbWxIdHRwUmVxdWVzdHMgcmVzcG9uc2UuXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHJlc3BvbnNlLm1vZGFsVGl0bGUgVGl0bGUgb2YgbW9kYWwuXG5cdFx0ICpcblx0XHQgKiBAcmV0dXJucyB7alF1ZXJ5fEhUTUxFbGVtZW50fVxuXHRcdCAqIEBwcml2YXRlXG5cdFx0ICovXG5cdFx0Y29uc3QgX3JlbmRlck1vZGFsSGVhZGVyID0gcmVzcG9uc2UgPT4ge1xuXHRcdFx0Y29uc3QgJG1vZGFsSGVhZGVyID0gJCgnPGRpdi8+Jywge1xuXHRcdFx0XHQnY2xhc3MnOiAnbW9kYWwtaGVhZGVyJ1xuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGNvbnN0ICRtb2RhbEhlYWRlckNsb3NlQnRuID0gJCgnPGJ1dHRvbi8+Jywge1xuXHRcdFx0XHQndHlwZSc6ICdidXR0b24nLFxuXHRcdFx0XHQnY2xhc3MnOiAnY2xvc2UnLFxuXHRcdFx0XHQnZGF0YS1kaXNtaXNzJzogJ21vZGFsJyxcblx0XHRcdFx0J2FyaWEtbGFiZWwnOiAnY2xvc2UnXG5cdFx0XHR9KS5hcHBlbmRUbygkbW9kYWxIZWFkZXIpO1xuXHRcdFx0XG5cdFx0XHQkKCc8c3Bhbi8+Jywge1xuXHRcdFx0XHQnYXJpYS1oaWRkZW4nOiAndHJ1ZScsXG5cdFx0XHRcdCdodG1sJzogJyZ0aW1lczsnXG5cdFx0XHR9KS5hcHBlbmRUbygkbW9kYWxIZWFkZXJDbG9zZUJ0bik7XG5cdFx0XHRcblx0XHRcdCQoJzxoNC8+Jywge1xuXHRcdFx0XHQnY2xhc3MnOiAnbW9kYWwtdGl0bGUnLFxuXHRcdFx0XHQndGV4dCc6IHJlc3BvbnNlLm1vZGFsVGl0bGVcblx0XHRcdH0pLmFwcGVuZFRvKCRtb2RhbEhlYWRlcik7XG5cdFx0XHRcblx0XHRcdHJldHVybiAkbW9kYWxIZWFkZXI7XG5cdFx0fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBSZW5kZXJzIHRoZSBtb2RhbCBib2R5LlxuXHRcdCAqXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IHJlc3BvbnNlIFhtbEh0dHBSZXF1ZXN0cyByZXNwb25zZS5cblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gcmVzcG9uc2UuaUZyYW1lVXJsIFVybCBvZiBjb25uZWN0aW9uIGlmcmFtZS5cblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gcmVzcG9uc2Uub3JpZ2luIFNob3Agb3JpZ2luLCByZXF1aXJlZCBmb3IgcmVkaXJlY3Rpb24gb2YgYXBpLlxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSByZXNwb25zZS5sYW5ndWFnZSBMYW5ndWFnZSBjb2RlIG9mIGN1cnJlbnRseSBhY3RpdmF0ZWQgbGFuZ3VhZ2UuXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHJlc3BvbnNlLnZlcnNpb24gQ3VycmVudCBzaG9wIHZlcnNpb24uXG5cdFx0ICogQHJldHVybnMge2pRdWVyeXxIVE1MRWxlbWVudH1cblx0XHQgKiBAcHJpdmF0ZVxuXHRcdCAqL1xuXHRcdGNvbnN0IF9yZW5kZXJNb2RhbEJvZHkgPSByZXNwb25zZSA9PiB7XG5cdFx0XHRjb25zdCAkbW9kYWxCb2R5ID0gJCgnPGRpdi8+Jywge1xuXHRcdFx0XHQnY2xhc3MnOiAnbW9kYWwtYm9keSdcblx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHQkKCc8ZGl2Lz4nLCB7XG5cdFx0XHRcdCdkYXRhLWd4X2dvb2dsZV9vYXV0aC13aWRnZXQnOiAnZ29vZ2xlX2Nvbm5lY3Rpb25faWZyYW1lJyxcblx0XHRcdFx0J2RhdGEtZ29vZ2xlX2Nvbm5lY3Rpb25faWZyYW1lLXVybCc6IHJlc3BvbnNlLmlGcmFtZVVybCxcblx0XHRcdFx0J2RhdGEtZ29vZ2xlX2Nvbm5lY3Rpb25faWZyYW1lLW9yaWdpbic6IHJlc3BvbnNlLm9yaWdpbixcblx0XHRcdFx0J2RhdGEtZ29vZ2xlX2Nvbm5lY3Rpb25faWZyYW1lLWxhbmd1YWdlJzogcmVzcG9uc2UubGFuZ3VhZ2UsXG5cdFx0XHRcdCdkYXRhLWdvb2dsZV9jb25uZWN0aW9uX2lmcmFtZS1lcnJvcic6IG9wdGlvbnMuZXJyb3IsXG5cdFx0XHRcdCdkYXRhLWdvb2dsZV9jb25uZWN0aW9uX2lmcmFtZS1mcm9tJzogb3B0aW9ucy5mcm9tLFxuXHRcdFx0XHQnZGF0YS1nb29nbGVfY29ubmVjdGlvbl9pZnJhbWUtdmVyc2lvbic6IHJlc3BvbnNlLnZlcnNpb25cblx0XHRcdH0pLmFwcGVuZFRvKCRtb2RhbEJvZHkpO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gJG1vZGFsQm9keTtcblx0XHR9O1xuXHRcdFxuXHRcdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXHRcdC8vIElOSVRJQUxJWkFUSU9OXG5cdFx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cdFx0XG5cdFx0LyoqXG5cdFx0ICogSW5pdGlhbGl6ZSBtZXRob2Qgb2YgdGhlIHdpZGdldCwgY2FsbGVkIGJ5IHRoZSBlbmdpbmUuXG5cdFx0ICovXG5cdFx0bW9kdWxlLmluaXQgPSBmdW5jdGlvbihkb25lKSB7XG5cdFx0XHRqc2UubGlicy54aHIuZ2V0KHt1cmw6ICcuL2FkbWluLnBocD9kbz1Hb29nbGVPQXV0aEFqYXgvY29ubmVjdGlvbk1vZGFsJ30pXG5cdFx0XHRcdC5kb25lKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHRpZiAoIXJlc3BvbnNlLmNvbm5lY3RlZCkge1xuXHRcdFx0XHRcdFx0X3JlbmRlck1vZGFsKHJlc3BvbnNlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5mYWlsKCgpID0+IHtcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0ZhaWxlZCB0byBsb2FkIG1vZGFsIGRhdGEhJyk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XG5cdFx0XHRkb25lKCk7XG5cdFx0fTtcblx0XHRcblx0XHRyZXR1cm4gbW9kdWxlO1xuXHR9KTtcbiJdfQ==
