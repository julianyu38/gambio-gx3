<?php
/* --------------------------------------------------------------
 AdWordsCurl.inc.php 2017-12-11
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

use HubPublic\Http\CurlRequest;

/**
 * Class AdWordsCurl
 */
class AdWordsCurl
{
	/**
	 * @var string
	 */
	protected $clientCustomerId;
	
	/**
	 * @var \CI_DB_query_builder
	 */
	protected $db;
	
	/**
	 * @var \GoogleConfigurationStorage
	 */
	protected $configurationStorage;
	
	
	/**
	 * AdWordsCurl constructor.
	 *
	 * @param \CI_DB_query_builder $db
	 */
	public function __construct(CI_DB_query_builder $db)
	{
		$this->db                   = $db;
		$this->configurationStorage = new GoogleConfigurationStorage($this->db,
		                                                             GoogleConfigurationStorage::SCOPE_AUTH);
	}
	
	
	/**
	 * Sends a cURL request to the Google AdWords API to fetch campaigns of client.
	 *
	 * @return \HubPublic\ValueObjects\HttpResponse
	 */
	public function campaignsOverview($dateRange = null)
	{
		$url = $dateRange ? $this->_baseUrl() . '/' . $dateRange : $this->_baseUrl();
		
		return (new CurlRequest())->setUrl($url)->setOption(CURLOPT_HTTPHEADER, [
			'X-Refresh-Token: ' . $this->configurationStorage->get('refresh-token'),
		])->execute();
	}
	
	
	/**
	 * Sends a cURL request to the Google AdWords API to update the budget of an AdWords campaign.
	 *
	 * @param \AdWordsCampaignId $campaignId Id of campaign to be updated.
	 * @param \AdWordsBudget     $budget     New budget value for campaign.
	 *
	 * @return \HubPublic\ValueObjects\HttpResponse
	 */
	public function updateBudget(AdWordsCampaignId $campaignId, AdWordsBudget $budget)
	{
		$requestUrl = $this->_baseUrl() . '/' . $campaignId->id() . '/updateBudget';
		
		return (new CurlRequest())->setUrl($requestUrl)->setOption(CURLOPT_HTTPHEADER, [
			'X-Refresh-Token: ' . $this->configurationStorage->get('refresh-token'),
			'Content-Type: application/json'
		])->setOption(CURLOPT_CUSTOMREQUEST, 'PATCH')->setOption(CURLOPT_POSTFIELDS, json_encode([
			                                                                                         'budget' => $budget->budget()
		                                                                                         ]))->execute();
	}
	
	
	/**
	 * Sends a cURL request to the Google AdWords API to change the status of an AdWords campaign.
	 *
	 * @param \AdWordsCampaignId     $campaignId Id of campaign to be updated.
	 * @param \AdWordsCampaignStatus $status     New campaign state.
	 *
	 * @return \HubPublic\ValueObjects\HttpResponse
	 */
	public function changeStatus(AdWordsCampaignId $campaignId, AdWordsCampaignStatus $status)
	{
		$requestUrl = $this->_baseUrl() . '/' . $campaignId->id() . '/changeStatus';
		
		return (new CurlRequest())->setUrl($requestUrl)->setOption(CURLOPT_HTTPHEADER, [
			'X-Refresh-Token: ' . $this->configurationStorage->get('refresh-token'),
			'Content-Type: application/json'
		])->setOption(CURLOPT_CUSTOMREQUEST, 'PATCH')->setOption(CURLOPT_POSTFIELDS, json_encode([
			                                                                                         'status' => $status->active()
		                                                                                         ]))->execute();
	}
	
	
	/**
	 * Determines and returns the client customer id.
	 *
	 * @return string
	 */
	protected function _clientCustomerId()
	{
		if(null !== $this->clientCustomerId)
		{
			return $this->clientCustomerId;
		}
		$result = $this->db->from('google_adwords_client_customers')->where('primary', 1)->get()->row_array();
		
		if(count($result) === 0)
		{
			throw new UnexpectedValueException('Client customer id was not found');
		}
		$this->clientCustomerId = $result['client_customer_id'];
		
		return $this->clientCustomerId;
	}
	
	
	/**
	 * Base url for Google AdWords requests.
	 *
	 * @return string
	 */
	protected function _baseUrl()
	{
        return $this->configurationStorage->get('api-url', GoogleConfigurationStorage::SCOPE_GENERAL) . '/client/'
               . $this->_clientCustomerId() . '/campaigns';
    }
}