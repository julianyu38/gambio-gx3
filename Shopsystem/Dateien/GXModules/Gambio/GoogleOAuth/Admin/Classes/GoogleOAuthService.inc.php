<?php
/* --------------------------------------------------------------
   GoogleOAuthService.inc.php 2018-05-25
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

use GuzzleHttp\Client;

/**
 * Class GoogleOAuthService
 */
class GoogleOAuthService
{
    /**
     * @var \GoogleConfigurationStorage
     */
    protected $storage;
    
    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;
    
    
    /**
     * GoogleOAuthService constructor.
     *
     * @param \GoogleConfigurationStorage $storage    Access to google configurations.
     * @param \GuzzleHttp\Client          $httpClient Http client to fetch new token if necessary.
     */
    public function __construct(GoogleConfigurationStorage $storage, Client $httpClient)
    {
        $this->storage    = $storage;
        $this->httpClient = $httpClient;
    }
    
    
    /**
     * Named constructor of GoogleOAuthService.
     *
     * @param \GoogleConfigurationStorage $storage    Access to google configurations.
     * @param \GuzzleHttp\Client          $httpClient Http client to fetch new token if necessary.
     *
     * @return \GoogleOAuthService New instance.
     */
    public static function create(GoogleConfigurationStorage $storage = null, Client $httpClient = null)
    {
        return MainFactory::create(static::class, $storage ? : MainFactory::create(GoogleConfigurationStorage::class,
            StaticGXCoreLoader::getDatabaseQueryBuilder()), $httpClient ? : new Client());
    }
    
    
    /**
     * Returns the google oauth access token.
     * If the token is already expired, a new token is exchanged with the refresh token.
     *
     * @return string
     */
    public function getAccessToken()
    {
        $expirationTimestamp = $this->storage->get('expiration-timestamp', GoogleConfigurationStorage::SCOPE_AUTH);
        
        if ($expirationTimestamp <= time()) {
            $apiUrl   = $this->storage->get('api-url', GoogleConfigurationStorage::SCOPE_GENERAL);
            $endpoint = '/auth/refresh_access_token';
            
            $response  = $this->httpClient->get($apiUrl . $endpoint, [
                'headers' => [
                    'X-Refresh-Token' => $this->storage->get('refresh-token', GoogleConfigurationStorage::SCOPE_AUTH)
                ]
            ]);
            $tokenInfo = json_decode($response->getBody()->getContents(), true);
            
            $accessToken         = $tokenInfo['access_token'];
            $expirationTimestamp = time() + $tokenInfo['expires_in'];
            
            $this->storage->set('access-token', $accessToken, GoogleConfigurationStorage::SCOPE_AUTH);
            $this->storage->set('expiration-timestamp', $expirationTimestamp, GoogleConfigurationStorage::SCOPE_AUTH);
            
            return $accessToken;
        }
        
        return $this->storage->get('access-token', GoogleConfigurationStorage::SCOPE_AUTH);
    }
}
