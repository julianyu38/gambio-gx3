'use strict';

/* --------------------------------------------------------------
 DebugBar.js 2018-02-23
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Debug Bar JavaScript Enhancements
 *
 * This module will perform some adjustments to the default PHP Debug Bar instance.
 */
$(function () {
	'use strict';

	var $debugBar = $('.phpdebugbar');

	if (!$debugBar.length) {
		return; // The Debug Bar is not loaded on this page or could not be detected.
	}

	// Remove the message counter from "Help" section.
	var $help = $debugBar.find('.phpdebugbar-text').filter(function () {
		return $(this).text().indexOf('Help') !== -1;
	});

	if ($help.length) {
		$help.next().remove(); // Remove the counter element.		
	}

	// Remove unnecessary line break element added by the Debug Bar when minimized.
	$debugBar.on('click', '.phpdebugbar-close-btn', function () {
		$debugBar.next('br').remove();
	});

	$debugBar.on('click', '.phpdebugbar-restore-btn', function () {
		$('<br/>').insertAfter($debugBar);
	});

	if ($debugBar.hasClass('phpdebugbar-closed')) {
		$debugBar.next('br').remove();
	}

	// Correct initial display of Debug Bar in admin layout pages.
	if ($('aside#main-menu').length) {
		setTimeout(function () {
			phpdebugbar.resize();
		}, 2000);
	}

	// Set default initial Debug Bar state to minimized.
	phpdebugbar.minimize();
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIlNob3AvRGVidWdCYXIuanMiXSwibmFtZXMiOlsiJCIsIiRkZWJ1Z0JhciIsImxlbmd0aCIsIiRoZWxwIiwiZmluZCIsImZpbHRlciIsInRleHQiLCJpbmRleE9mIiwibmV4dCIsInJlbW92ZSIsIm9uIiwiaW5zZXJ0QWZ0ZXIiLCJoYXNDbGFzcyIsInNldFRpbWVvdXQiLCJwaHBkZWJ1Z2JhciIsInJlc2l6ZSIsIm1pbmltaXplIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7O0FBS0FBLEVBQUUsWUFBVztBQUNaOztBQUVBLEtBQUlDLFlBQVlELEVBQUUsY0FBRixDQUFoQjs7QUFFQSxLQUFJLENBQUNDLFVBQVVDLE1BQWYsRUFBdUI7QUFDdEIsU0FEc0IsQ0FDZDtBQUNSOztBQUVEO0FBQ0EsS0FBSUMsUUFBUUYsVUFBVUcsSUFBVixDQUFlLG1CQUFmLEVBQW9DQyxNQUFwQyxDQUEyQyxZQUFXO0FBQ2pFLFNBQU9MLEVBQUUsSUFBRixFQUFRTSxJQUFSLEdBQWVDLE9BQWYsQ0FBdUIsTUFBdkIsTUFBbUMsQ0FBQyxDQUEzQztBQUNBLEVBRlcsQ0FBWjs7QUFJQSxLQUFJSixNQUFNRCxNQUFWLEVBQWtCO0FBQ2pCQyxRQUFNSyxJQUFOLEdBQWFDLE1BQWIsR0FEaUIsQ0FDTTtBQUN2Qjs7QUFFRDtBQUNBUixXQUFVUyxFQUFWLENBQWEsT0FBYixFQUFzQix3QkFBdEIsRUFBZ0QsWUFBVztBQUMxRFQsWUFBVU8sSUFBVixDQUFlLElBQWYsRUFBcUJDLE1BQXJCO0FBQ0EsRUFGRDs7QUFJQVIsV0FBVVMsRUFBVixDQUFhLE9BQWIsRUFBc0IsMEJBQXRCLEVBQWtELFlBQVc7QUFDNURWLElBQUUsT0FBRixFQUFXVyxXQUFYLENBQXVCVixTQUF2QjtBQUNBLEVBRkQ7O0FBSUEsS0FBSUEsVUFBVVcsUUFBVixDQUFtQixvQkFBbkIsQ0FBSixFQUE4QztBQUM3Q1gsWUFBVU8sSUFBVixDQUFlLElBQWYsRUFBcUJDLE1BQXJCO0FBQ0E7O0FBRUQ7QUFDQSxLQUFJVCxFQUFFLGlCQUFGLEVBQXFCRSxNQUF6QixFQUFpQztBQUNoQ1csYUFBVyxZQUFXO0FBQ3JCQyxlQUFZQyxNQUFaO0FBQ0EsR0FGRCxFQUVHLElBRkg7QUFHQTs7QUFFRDtBQUNBRCxhQUFZRSxRQUFaO0FBQ0EsQ0F4Q0QiLCJmaWxlIjoiU2hvcC9EZWJ1Z0Jhci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiBEZWJ1Z0Jhci5qcyAyMDE4LTAyLTIzXHJcbiBHYW1iaW8gR21iSFxyXG4gaHR0cDovL3d3dy5nYW1iaW8uZGVcclxuIENvcHlyaWdodCAoYykgMjAxOCBHYW1iaW8gR21iSFxyXG4gUmVsZWFzZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIChWZXJzaW9uIDIpXHJcbiBbaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbF1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIERlYnVnIEJhciBKYXZhU2NyaXB0IEVuaGFuY2VtZW50c1xyXG4gKlxyXG4gKiBUaGlzIG1vZHVsZSB3aWxsIHBlcmZvcm0gc29tZSBhZGp1c3RtZW50cyB0byB0aGUgZGVmYXVsdCBQSFAgRGVidWcgQmFyIGluc3RhbmNlLlxyXG4gKi9cclxuJChmdW5jdGlvbigpIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblx0XHJcblx0dmFyICRkZWJ1Z0JhciA9ICQoJy5waHBkZWJ1Z2JhcicpO1xyXG5cdFxyXG5cdGlmICghJGRlYnVnQmFyLmxlbmd0aCkge1xyXG5cdFx0cmV0dXJuOyAvLyBUaGUgRGVidWcgQmFyIGlzIG5vdCBsb2FkZWQgb24gdGhpcyBwYWdlIG9yIGNvdWxkIG5vdCBiZSBkZXRlY3RlZC5cclxuXHR9XHJcblx0XHJcblx0Ly8gUmVtb3ZlIHRoZSBtZXNzYWdlIGNvdW50ZXIgZnJvbSBcIkhlbHBcIiBzZWN0aW9uLlxyXG5cdHZhciAkaGVscCA9ICRkZWJ1Z0Jhci5maW5kKCcucGhwZGVidWdiYXItdGV4dCcpLmZpbHRlcihmdW5jdGlvbigpIHtcclxuXHRcdHJldHVybiAkKHRoaXMpLnRleHQoKS5pbmRleE9mKCdIZWxwJykgIT09IC0xO1xyXG5cdH0pO1xyXG5cdFxyXG5cdGlmICgkaGVscC5sZW5ndGgpIHtcclxuXHRcdCRoZWxwLm5leHQoKS5yZW1vdmUoKTsgLy8gUmVtb3ZlIHRoZSBjb3VudGVyIGVsZW1lbnQuXHRcdFxyXG5cdH1cclxuXHRcclxuXHQvLyBSZW1vdmUgdW5uZWNlc3NhcnkgbGluZSBicmVhayBlbGVtZW50IGFkZGVkIGJ5IHRoZSBEZWJ1ZyBCYXIgd2hlbiBtaW5pbWl6ZWQuXHJcblx0JGRlYnVnQmFyLm9uKCdjbGljaycsICcucGhwZGVidWdiYXItY2xvc2UtYnRuJywgZnVuY3Rpb24oKSB7XHJcblx0XHQkZGVidWdCYXIubmV4dCgnYnInKS5yZW1vdmUoKTtcclxuXHR9KTtcclxuXHRcclxuXHQkZGVidWdCYXIub24oJ2NsaWNrJywgJy5waHBkZWJ1Z2Jhci1yZXN0b3JlLWJ0bicsIGZ1bmN0aW9uKCkge1xyXG5cdFx0JCgnPGJyLz4nKS5pbnNlcnRBZnRlcigkZGVidWdCYXIpO1xyXG5cdH0pO1xyXG5cdFxyXG5cdGlmICgkZGVidWdCYXIuaGFzQ2xhc3MoJ3BocGRlYnVnYmFyLWNsb3NlZCcpKSB7XHJcblx0XHQkZGVidWdCYXIubmV4dCgnYnInKS5yZW1vdmUoKTtcclxuXHR9XHJcblx0XHJcblx0Ly8gQ29ycmVjdCBpbml0aWFsIGRpc3BsYXkgb2YgRGVidWcgQmFyIGluIGFkbWluIGxheW91dCBwYWdlcy5cclxuXHRpZiAoJCgnYXNpZGUjbWFpbi1tZW51JykubGVuZ3RoKSB7XHJcblx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRwaHBkZWJ1Z2Jhci5yZXNpemUoKTtcclxuXHRcdH0sIDIwMDApO1xyXG5cdH1cclxuXHJcblx0Ly8gU2V0IGRlZmF1bHQgaW5pdGlhbCBEZWJ1ZyBCYXIgc3RhdGUgdG8gbWluaW1pemVkLlxyXG5cdHBocGRlYnVnYmFyLm1pbmltaXplKCk7XHJcbn0pOyJdfQ==
