<?php
/* --------------------------------------------------------------
   module_center_module.checkout_loading_spinner.lang.inc.php 2018-04-16
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * English phrases
 */
$t_language_text_section_content_array = [
	'checkout_loading_spinner_description'                => 'Show a loading spinner in the checkout process',
	'checkout_loading_spinner_error_saving_configuration' => 'An error has occured while saving the configuration',
	'checkout_loading_spinner_is_enabled'                 => 'Active',
	'checkout_loading_spinner_saved'                      => 'Configuration saved',
	'checkout_loading_spinner_text'                       => 'Loading payment methods',
	'checkout_loading_spinner_timeout'                    => 'Show after timeout (seconds)',
	'checkout_loading_spinner_title'                      => 'Checkout loading spinner'
];