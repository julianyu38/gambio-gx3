<?php

/* --------------------------------------------------------------
   module_center_module.checkout_loading_spinner.lang.inc.php 2018-04-16
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * German phrases
 */
$t_language_text_section_content_array = [
	'checkout_loading_spinner_description'                => 'Loading-Spinner im Checkout-Prozess anzeigen',
	'checkout_loading_spinner_error_saving_configuration' => 'Fehler beim Speichern der Konfiguration',
	'checkout_loading_spinner_is_enabled'                 => 'Aktiviert',
	'checkout_loading_spinner_saved'                      => 'Konfiguration gespeichert',
	'checkout_loading_spinner_text'                       => 'Zahlungsweisen werden geladen',
	'checkout_loading_spinner_timeout'                    => 'Nach Timeout anzeigen (Sekunden)',
	'checkout_loading_spinner_title'                      => 'Checkout Loading-Spinner'
];