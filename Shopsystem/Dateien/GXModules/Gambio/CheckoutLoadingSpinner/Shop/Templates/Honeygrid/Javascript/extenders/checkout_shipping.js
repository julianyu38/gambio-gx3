/* --------------------------------------------------------------
 checkout_shipping.js 2018-04-13
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2018 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * Checkout shipping extender
 */
(function() {
    document.querySelector('#main > div > form').addEventListener('submit', window.showCheckoutLoadingSpinner);
})();


