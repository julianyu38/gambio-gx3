<?php
/* --------------------------------------------------------------
   module_center_module.ampify.lang.inc.php 2017-03-23
   meco media & communication GmbH
   http://www.meco-media.com
   Copyright (c) 2017 meco media & communication GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   -------------------------------------------------------------- 
*/

$t_language_text_section_content_array = array
(
    'ampify_title' => 'AMPIFY',
    'ampify_description' => 'AMPIFY provides Accelerated Mobile Pages for your shop.',
    'ampify_legend_configuration' => 'AMPIFY Configuration',
    'ampify_label_module_active' => 'Activate Module',
    'ampify_label_key' => 'Token',
    'ampify_tooltip_key_hint' => 'Please enter your AMPIFY token.',
    'ampify_save_config_success' => 'The configuration has been saved.'
);