<?php
/* --------------------------------------------------------------
   module_center_module.ampify.lang.inc.php 2017-03-23
   meco media & communication GmbH
   http://www.meco-media.com
   Copyright (c) 2017 meco media & communication GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   -------------------------------------------------------------- 
*/

$t_language_text_section_content_array = array
(
    'ampify_title' => 'AMPIFY',
    'ampify_description' => 'AMPIFY stellt Accelerated Mobile Pages für Ihren Shop zur Verfügung.',
    'ampify_legend_configuration' => 'AMPIFY Konfiguration',
    'ampify_label_module_active' => 'Modul aktiviert',
    'ampify_label_key' => 'Token',
    'ampify_tooltip_key_hint' => 'Tragen Sie den Token ein, den Sie von AMPIFY erhalten haben',
    'ampify_save_config_success' => 'Die Einstellungen wurden gespeichert.'
);