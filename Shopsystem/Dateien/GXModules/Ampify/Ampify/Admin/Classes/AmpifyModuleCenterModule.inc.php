<?php

/* --------------------------------------------------------------
   AmpifyModuleCenterModule.inc.php 2017-02-27
   meco media & communication GmbH
   http://www.meco-media.com
   Copyright (c) 2017 meco media & communication GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class AmpifyModuleCenterModule extends AbstractModuleCenterModule
{
	// protected $tableName = 'foo_bar_configuration';
	
	
	protected function _init()
	{
		$this->title       = $this->languageTextManager->get_text('ampify_title');
		$this->description = $this->languageTextManager->get_text('ampify_description');
		$this->sortOrder   = 99999;
	}
}