<?php

/* --------------------------------------------------------------
   AmpifyModuleCenterModuleController.inc.php 2017-02-27
   meco media & communication GmbH
   http://www.meco-media.com
   Copyright (c) 2017 meco media & communication GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class AmpifyModuleCenterModuleController extends AbstractModuleCenterModuleController
{
	protected $db;

	protected $options = [
		'AMPIFY_ACTIVE', 
		'AMPIFY_KEY'
	];
	
	protected $errors = [];
	
	
	protected function _init()
	{
		$this->pageTitle = $this->languageTextManager->get_text('ampify_title');
		$this->contentView->set_template_dir(DIR_FS_CATALOG . 'GXModules/Ampify/Ampify/Admin/Html/');
	}
	
	
	public function actionDefault()
	{
		$pageTitle         = new NonEmptyStringType($this->pageTitle);
		$templateData      = MainFactory::create('KeyValueCollection', ['config' => $this->_readConfig(), 'errors' => $this->errors]);
		$template          = new ExistingFile(new NonEmptyStringType(DIR_FS_CATALOG . 'GXModules/Ampify/Ampify/Admin/Html/ampify_configuration.html'));
		$assetCollection   = MainFactory::create('AssetCollection', []);
		return MainFactory::create('AdminLayoutHttpControllerResponse', $pageTitle, $template, $templateData, $assetCollection, $contentNavigation);
	}
	
	
	public function actionProcess()
	{
		if($this->_getPostDataCollection()->keyExists('delete_config'))
		{
			return $this->_deleteConfig();
		}
		
		if($this->_validate($this->_getPostDataCollection()->getArray()))
		{
			$this->_saveConfig($this->_getPostDataCollection()->getArray());
			$GLOBALS['messageStack']->add($this->languageTextManager->get_text('ampify_save_config_success'), 'success');
		}
		
		return $this->actionDefault();
	}
	
	
	protected function _deleteConfig()
	{
		return $this->actionDefault();
	}
	
	
	protected function _validate(array $postData)
	{
		return 0 === count($this->errors);
	}
	
	
	protected function _saveConfig(array $postData)
	{
		foreach ($this->options as $key) {
			gm_set_conf($key, (!empty($postData[$key])) ? $postData[$key] : 0);
		}
	}
	
	
	protected function _readConfig()
	{
		$config = [];
		foreach ($this->options as $key) {
			$config[$key] = gm_get_conf($key);
		}
		return $config;
	}
}