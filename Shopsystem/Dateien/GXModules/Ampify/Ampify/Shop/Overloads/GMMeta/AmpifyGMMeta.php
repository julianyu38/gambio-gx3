<?php
/* --------------------------------------------------------------
   AMPifyGMMeta.php 2018-08-30
   meco media & communication GmbH
   http://www.meco-media.com
   Copyright (c) 2018 meco media & communication GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
  --------------------------------------------------------------
 */

/*
 * 	class GMMeta 	
 */ 

class AMPifyGMMeta extends AMPifyGMMeta_parent
{
	/**
	 * @var DataCache
	 */
	protected $dataCache;
	
	/**
	 * @var string
	 */
	protected $cacheKey = 'accelerated-mobile-pages';

	/**
	 * @var string
	 */
	protected $ampifyUrl = 'https://ampify.it/';

	/**
	 * @var boolean
	 */
	protected $installed;

	/**
	 * @var boolean
	 */
	protected $active;

	/**
	 * @var string
	 */
	protected $key;

	public function __construct($p_output = true)
	{
		parent::__construct(false);
		$this->dataCache = DataCache::get_instance();

		$this->installed = gm_get_conf('MODULE_CENTER_AMPIFY_INSTALLED');
		$this->active = gm_get_conf('AMPIFY_ACTIVE');
		$this->key = gm_get_conf('AMPIFY_KEY');
	}

	/**
	 * Extend meta-tags if AMP page available
	 *
	 * @return bool|string Returns the JSON string or false if the cache is outdated.
	 */
	public function get_base_meta()
	{

		// Check if module is active
		if (empty($this->installed) || empty($this->active) || empty($this->key)) {
			return parent::get_base_meta();
		}

		$meta = '';

		// Get data from cache or via webservice
		$jsonString = $this->_getCacheResponse();
		if ($jsonString === false)
		{
			$jsonString = $this->_requestAmpUrls();
		}
		$ampUrlMap = @json_decode($jsonString, true);

		// Remove XTCsid from query string in requested url
		$requestUrl = GM_HTTP_SERVER . gm_get_env_info('REQUEST_URI');
		list($requestUrl, $query) = explode('?', $requestUrl);
		if (!empty($query)) {

			$queryParts = explode('&', $query);
			if (!empty($queryParts)) 
			{
				foreach ($queryParts as $key => $queryPart) 
				{
					list($var, $value) = explode('=', $queryPart);
					if ($var == 'XTCsid') {
						unset($queryParts[$key]);
					}
				}
			}
			if (!empty($queryParts)) 
			{
				$requestUrl .= '?' . implode('&', $queryParts);
			}
		}
		
		// Compare requested url with AMP map
		if (!empty($ampUrlMap) && is_array($ampUrlMap) && !empty($ampUrlMap[$requestUrl])) 
		{
			$ampUrl = $ampUrlMap[$requestUrl];
			$meta = sprintf('<link rel="amphtml" href="%s">', $ampUrl);
		}
		
		return 
			$meta . PHP_EOL .
			parent::get_base_meta();
	}
	
	/**
	 * Get the cached response
	 *
	 * @return bool|string Returns the JSON string or false if the cache is outdated.
	 */
	protected function _getCacheResponse()
	{
		// Clear cache via GET-variable
		if (!empty($_GET['clear-amp-cache']) && $_GET['clear-amp-cache'] == $this->key)
		{
			return false;
		}

		// Cache is not created
		if (!$this->dataCache->key_exists($this->cacheKey, true))
		{
			return false;
		}

		// Read cache; break if older than 5 minutes
		$cacheData = $this->dataCache->get_persistent_data($this->cacheKey);
		if ($cacheData['timestamp'] <= mktime(date('H'), date('i') - (60 * 24), date('s'), date('m'), date('d'), date('Y'))) {
			return false;
		}

		return $cacheData['response'];
	}

	/**
	 * Receive AMP-pages array from Ampify-Server
	 *
	 * @return bool|string Returns the JSON string or false if the cache is outdated.
	 */
	protected function _requestAmpUrls()
	{

		// Rest request
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $this->ampifyUrl . 'de/amp-site-pages/sitemap/' . $this->key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);
        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        // Response data
        $urls = (array) $response->ampSitePages;

        // Change urls to shop internal
		$connectionType = GM_HTTP_SERVER === HTTPS_SERVER ? 'SSL' : 'NONSSL';
        foreach ($urls as $shopUrl => &$ampUrl) {
        	$ampUrl = xtc_href_link('amp/' . str_replace($this->ampifyUrl, '', $ampUrl), '', $connectionType);
        }

        // save url array to session
		$jsonString = json_encode($urls);
		$cacheData = [
			'timestamp' => time(),
			'response'  => $jsonString
		];

		// Write data into cache file and return
		$this->dataCache->write_persistent_data($this->cacheKey, $cacheData);
		return $jsonString;
	}
}