<?php
/* --------------------------------------------------------------
    AmpifyAmpifyApplicationTopPrimalExtenderComponent.inc.php 2017-12-05
    meco media & communication GmbH
    http://www.meco-media.com
    Copyright (c) 2017 meco media & communication GmbH
    Released under the GNU General Public License (Version 2)
    [http://www.gnu.org/licenses/gpl-2.0.html]
    --------------------------------------------------------------
*/

class AmpifyAmpifyApplicationTopPrimalExtenderComponent extends AmpifyAmpifyApplicationTopPrimalExtenderComponent_parent
{
    public function proceed()
    {
        parent::proceed();

        // extract pages uuid from amp url
        // and load its AMP-content from ampify.it
        preg_match('/\/amp\/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})$/', $_SERVER['REQUEST_URI'], $matches);
        if (!empty($matches[1])) {
            echo self::urlGetContents('https://ampify.it/' . $matches[1]);
            exit;
        }
    }

    protected static function urlGetContents($url) 
    {
        if (!function_exists('curl_init')){ 
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}