<?php

/* --------------------------------------------------------------
	DSGVONewsletterContentView.inc.php 2018-05-18
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

/**
 * Class representing the newsletter view overload for DSGVO
 */
class DSGVONewsletterContentView extends DSGVONewsletterContentView_parent {
    /**
     * Prepare data
     */
    public function prepare_data()
    {
        parent::prepare_data();

        if (!$this->form_send) {
            return;
        }

        $agreementCustomer = StaticGXCoreLoader::getService('AgreementWrite')->createCustomer(
            new StringType(''),
            MainFactory::create('AgreementCustomerEmail', $this->email_address)
        );

        AgreementStoreHelper::store(
            new IdType($_SESSION['languages_id']),
            LegalTextType::PRIVACY,
            $agreementCustomer,
            new NonEmptyStringType('LOG_IP_ACCOUNT_NEWSLETTER')
        );
    }
}