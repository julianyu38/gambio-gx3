<?php

/* --------------------------------------------------------------
	SeoTagsHeaderExtender.inc.php 2018-06-15
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

class SeoTagsHeaderExtender extends SeoTagsHeaderExtender_parent
{
	public function proceed()
	{
		parent::proceed();
		
		$seoBoost     = MainFactory::create_object('GMSEOBoost', [], true);
		$languageCode = new LanguageCode(new StringType(strtoupper($_SESSION['language_code'])));
		$url          = '';
		$html         = '';
		$robots       = 'index,follow';
		
		$requestUri        = strtok(gm_get_env_info('REQUEST_URI'), '?');
		$trimmedRequestUri = preg_replace('/^\//', '', preg_replace('/^' . strtolower($languageCode) . '/', '',
		                                                            substr($requestUri, strlen(DIR_WS_CATALOG))));
		$getParams         = xtc_get_all_get_params([
			                                            'language',
			                                            'gm_boosted_category',
			                                            'gm_boosted_product',
			                                            'gm_boosted_content',
			                                            'cat',
			                                            'cPath',
			                                            'products_id',
			                                            'info',
			                                            'coID',
                                                        'products_qty'
		                                            ]);
		
		$getParamsArray = parse_url($getParams, PHP_URL_QUERY);
		parse_str($getParamsArray, $getArray);
		$getArray = array_keys($getArray);
		
		$connectionType = (getenv('HTTPS') == '1' || getenv('HTTPS') == 'on')
			? 'SSL'
			: 'NONSSL';
		
		$languageProvider = MainFactory::create('LanguageProvider', StaticGXCoreLoader::getDatabaseQueryBuilder());
		$activeCodes      = $languageProvider->getActiveCodes();
		$defaultLangCode  = $languageProvider->getDefaultLanguageCode();
		
		$noIndexKeys = [
			'feature_categories_id',
			'filter_categories_id',
			'filter_fv_id',
			'filter_id',
			'filter_price_max',
			'filter_price_min',
			'keywords',
			'listing_count',
			'listing_sort',
			'page',
			'value_conjunction'
		];
		
		$noRelPrevNext = [
			'feature_categories_id',
			'filter_categories_id',
			'filter_fv_id',
			'filter_id',
			'filter_price_max',
			'filter_price_min',
			'listing_count',
			'listing_sort',
			'value_conjunction'
		];
		
		foreach($noIndexKeys as $key)
		{
			if(in_array($key, $getArray))
			{
				$robots = 'noindex,follow';
				break;
			}
		}
		
		if(isset($GLOBALS['product']) && $GLOBALS['product']->isProduct === true)
		{
			/** @var ProductReadService $productReadService */
			$productReadService = StaticGXCoreLoader::getService('ProductRead');
			$product            = $productReadService->getProductById(new IdType((int)$GLOBALS['actual_products_id']));
			
			if($seoBoost->boost_products)
			{
				$altUrl = $seoBoost->get_boosted_product_url($GLOBALS['actual_products_id']);
				
				if(substr($altUrl, 2, 1) === '/')
				{
					$altUrl = substr($altUrl, 3);
				}
				
				if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link(strtolower($languageCode) . '/' . $altUrl,
					                                                        $getParams, $connectionType, true, true,
					                                                        false, true, true) . '" />' . "\n\t\t";
				}
				elseif($activeCodes->count() > 1)
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($altUrl, $getParams . 'language='
					                                                                 . strtolower($languageCode),
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
				}
				else
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($altUrl, $getParams, $connectionType, true,
					                                                        true, false, true, true) . '" />'
					         . "\n\t\t";
				}
			}
			else
			{
				try
				{
					$productLinkParams = xtc_product_link($GLOBALS['actual_products_id'],
					                                      $product->getName($languageCode),
					                                      $product->getUrlKeywords($languageCode),
					                                      $_SESSION['languages_id']);
				}
				catch(InvalidArgumentException $e)
				{
					$productLinkParams = xtc_product_link($GLOBALS['actual_products_id'], '', '',
					                                      $_SESSION['languages_id']);
				}
				
				$url = FILENAME_PRODUCT_INFO;
				
				if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link(strtolower($languageCode) . '/' . $url,
					                                                        $getParams . $productLinkParams,
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
				}
				elseif($activeCodes->count() > 1)
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($url, $getParams . 'language='
					                                                              . strtolower($languageCode) . '&'
					                                                              . $productLinkParams, $connectionType,
					                                                        true, true, false, true, true) . '" />'
					         . "\n\t\t";
				}
				else
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($url, $getParams . $productLinkParams,
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
				}
			}
			
			if($robots === 'index,follow' && $activeCodes->count() > 1)
			{
				if(gm_get_conf('GM_SEO_BOOST_PRODUCTS') === 'true')
				{
					if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
					{
						$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($defaultLangCode
						                                                                             . '/' . $altUrl,
						                                                                             $getParams,
						                                                                             $connectionType,
						                                                                             true, true, false,
						                                                                             true, true)
						         . '" />' . "\n\t\t";
					}
					else
					{
						$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($altUrl, $getParams
						                                                                                      . 'language='
						                                                                                      . $defaultLangCode,
						                                                                             $connectionType,
						                                                                             true, true, false,
						                                                                             true, true)
						         . '" />' . "\n\t\t";
					}
				}
				elseif(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
				{
					$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($defaultLangCode . '/'
					                                                                             . $url, $getParams
					                                                                                     . $productLinkParams,
					                                                                             $connectionType, true,
					                                                                             true, false, true,
					                                                                             true) . '" />'
					         . "\n\t\t";
				}
				else
				{
					$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($url, $getParams
					                                                                                   . 'language='
					                                                                                   . $defaultLangCode
					                                                                                   . '&'
					                                                                                   . $productLinkParams,
					                                                                             $connectionType, true,
					                                                                             true, false, true,
					                                                                             true) . '" />'
					         . "\n\t\t";
				}
				
				foreach($activeCodes as $code)
				{
					$langCode   = new LanguageCode(new StringType($code->asString()));
					$languageId = $languageProvider->getIdByCode($langCode);
					
					if($seoBoost->boost_products)
					{
						$altUrl = $seoBoost->get_boosted_product_url($GLOBALS['actual_products_id'], '', $languageId);
						
						if(substr($altUrl, 2, 1) === '/')
						{
							$altUrl = substr($altUrl, 3);
						}
						
						if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
						{
							$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
							         . xtc_href_link(strtolower($code) . '/' . $altUrl, $getParams, $connectionType,
							                         true, true, false, true, true) . '" />' . "\n\t\t";
						}
						else
						{
							$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
							         . xtc_href_link($altUrl, $getParams . 'language=' . strtolower($code),
							                         $connectionType, true, true, false, true, true) . '" />'
							         . "\n\t\t";
						}
					}
					else
					{
						try
						{
							$productLinkParams = xtc_product_link($GLOBALS['actual_products_id'],
							                                      $product->getName($langCode),
							                                      $product->getUrlKeywords($langCode), $languageId);
						}
						catch(InvalidArgumentException $e)
						{
							continue;
						}
						
						$url = FILENAME_PRODUCT_INFO;
						
						if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
						{
							$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
							         . xtc_href_link(strtolower($code) . '/' . $url, $getParams . $productLinkParams,
							                         $connectionType, true, true, false, true, true) . '" />'
							         . "\n\t\t";
						}
						else
						{
							$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
							         . xtc_href_link($url, $getParams . 'language=' . strtolower($code) . '&'
							                               . $productLinkParams, $connectionType, true, true, false,
							                         true, true) . '" />' . "\n\t\t";
						}
					}
				}
			}
		}
		elseif(isset($_GET['cat']) && isset($GLOBALS['current_category_id']) && !empty($GLOBALS['current_category_id']))
		{
			/** @var CategoryReadService $categoryReadService */
			$categoryReadService = StaticGXCoreLoader::getService('CategoryRead');
			$category            = $categoryReadService->getCategoryById(new IdType((int)$GLOBALS['current_category_id']));
			
			if($seoBoost->boost_categories)
			{
				$altUrl = $seoBoost->get_boosted_category_url($GLOBALS['current_category_id']);
				
				if(substr($altUrl, 2, 1) === '/')
				{
					$altUrl = substr($altUrl, 3);
				}
				
				if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link(strtolower($languageCode) . '/' . $altUrl,
					                                                        $getParams, $connectionType, true, true,
					                                                        false, true, true) . '" />' . "\n\t\t";
				}
				elseif($activeCodes->count() > 1)
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($altUrl, $getParams . 'language='
					                                                                 . strtolower($languageCode),
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
				}
				else
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($altUrl, $getParams, $connectionType, true,
					                                                        true, false, true, true) . '" />'
					         . "\n\t\t";
				}
			}
			else
			{
				try
				{
					$categoryLinkParams = xtc_category_link($GLOBALS['current_category_id'],
					                                        $category->getName($languageCode),
					                                        $category->getUrlKeywords($languageCode),
					                                        $_SESSION['languages_id']);
				}
				catch(InvalidArgumentException $e)
				{
					$categoryLinkParams = xtc_category_link($GLOBALS['current_category_id'], '', '',
					                                        $_SESSION['languages_id']);
				}
				
				if(isset($_GET['cPath']) && !empty($_GET['cPath']))
				{
					$categoryLinkParams .= '&cPath=' . $_GET['cPath'];
				}
				
				$url = FILENAME_DEFAULT;
				
				if($activeCodes->count() > 1)
				{
					if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
					{
						$html .= '<link rel="canonical" href="' . xtc_href_link(strtolower($languageCode) . '/' . $url,
						                                                        $getParams . $categoryLinkParams,
						                                                        $connectionType, true, true, false,
						                                                        true, true) . '" />' . "\n\t\t";
					}
					else
					{
						$html .= '<link rel="canonical" href="' . xtc_href_link($url, $getParams . 'language='
						                                                              . strtolower($languageCode) . '&'
						                                                              . $categoryLinkParams,
						                                                        $connectionType, true, true, false,
						                                                        true, true) . '" />' . "\n\t\t";
					}
				}
				else
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($url, $getParams . $categoryLinkParams,
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
				}
			}
			
			if($activeCodes->count() > 1)
			{
				if(gm_get_conf('GM_SEO_BOOST_CATEGORIES') === 'true')
				{
					$langCode   = new LanguageCode(new StringType($defaultLangCode));
					$languageId = $languageProvider->getIdByCode($langCode);
					
					$altUrl = $seoBoost->get_boosted_category_url($GLOBALS['current_category_id'], $languageId);
					
					if(substr($altUrl, 2, 1) === '/')
					{
						$altUrl = substr($altUrl, 3);
					}
					
					if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
					{
						$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($defaultLangCode
						                                                                             . '/' . $altUrl,
						                                                                             $getParams,
						                                                                             $connectionType,
						                                                                             true, true, false,
						                                                                             true, true)
						         . '" />' . "\n\t\t";
					}
					else
					{
						$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($altUrl, $getParams
						                                                                                      . 'language='
						                                                                                      . strtolower($languageCode),
						                                                                             $connectionType,
						                                                                             true, true, false,
						                                                                             true, true)
						         . '" />' . "\n\t\t";
					}
				}
				else
				{
					$langCode   = new LanguageCode(new StringType($defaultLangCode));
					$languageId = $languageProvider->getIdByCode($langCode);
					
					try
					{
						$categoryLinkParams = xtc_category_link($GLOBALS['actual_products_id'],
						                                        $category->getName($langCode),
						                                        $category->getUrlKeywords($langCode), $languageId);
					}
					catch(InvalidArgumentException $e)
					{
						//just go on
					}
					
					if(isset($_GET['cPath']) && !empty($_GET['cPath']))
					{
						$categoryLinkParams .= '&cPath=' . $_GET['cPath'];
					}
					$url = FILENAME_DEFAULT;
					
					if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
					{
						$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($defaultLangCode
						                                                                             . '/' . $url,
						                                                                             $getParams
						                                                                             . $categoryLinkParams,
						                                                                             $connectionType,
						                                                                             true, true, false,
						                                                                             true, true)
						         . '" />' . "\n\t\t";
					}
					else
					{
						$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($url, $getParams
						                                                                                   . 'language='
						                                                                                   . strtolower($defaultLangCode)
						                                                                                   . '&'
						                                                                                   . $categoryLinkParams,
						                                                                             $connectionType,
						                                                                             true, true, false,
						                                                                             true, true)
						         . '" />' . "\n\t\t";
					}
				}
				
				foreach($activeCodes as $code)
				{
					$langCode   = new LanguageCode(new StringType($code->asString()));
					$languageId = $languageProvider->getIdByCode($langCode);
					
					if($seoBoost->boost_categories)
					{
						$altUrl = $seoBoost->get_boosted_category_url($GLOBALS['current_category_id'], $languageId);
						
						if(substr($altUrl, 2, 1) === '/')
						{
							$altUrl = substr($altUrl, 3);
						}
						
						if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
						{
							$altUrl = strtolower($langCode) . '/' . $altUrl;
						}
					}
					else
					{
						try
						{
							$categoryLinkParams = xtc_category_link($GLOBALS['actual_products_id'],
							                                        $category->getName($langCode),
							                                        $category->getUrlKeywords($langCode), $languageId);
						}
						catch(InvalidArgumentException $e)
						{
							continue;
						}
						
						if(isset($_GET['cPath']) && !empty($_GET['cPath']))
						{
							$categoryLinkParams .= '&cPath=' . $_GET['cPath'];
						}
						
						if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
						{
							$altUrl = strtolower($langCode) . '/' . FILENAME_DEFAULT;
						}
						else
						{
							$altUrl = FILENAME_DEFAULT;
						}
					}
					
					if($html === '')
					{
						$html .= '<link rel="alternate" hreflang="' . strtolower($languageCode) . '" href="'
						         . xtc_href_link($url, $getParams . 'language=' . strtolower($code), $connectionType,
						                         true, true, false, true, true) . '" />' . "\n\t\t";
					}
					
					if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
					{
						$html .= '<link rel="alternate" hreflang="' . strtolower($langCode) . '" href="'
						         . xtc_href_link($altUrl, $getParams . $categoryLinkParams, $connectionType, true, true,
						                         false, true, true) . '" />' . "\n\t\t";
					}
					else
					{
						$html .= '<link rel="alternate" hreflang="' . strtolower($langCode) . '" href="'
						         . xtc_href_link($altUrl, $getParams . 'language=' . strtolower($langCode) . '&'
						                                  . $categoryLinkParams, $connectionType, true, true, false,
						                         true, true) . '" />' . "\n\t\t";
					}
				}
			}
			
			$relNextPrev = '';
			if(!empty($_SESSION['relPrevUrl']))
			{
				$relNextPrev .= '<link rel="prev" href="' . $_SESSION['relPrevUrl'];
				
				if($activeCodes->count() > 1 && gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') !== 'true')
				{
					if(strpos($_SESSION['relPrevUrl'], '?') !== false)
					{
						$relNextPrev .= htmlspecialchars('&language=') . strtolower($languageCode);
					}
					else
					{
						$relNextPrev .= '?language=' . strtolower($languageCode);
					}
				}
				
				$relNextPrev .= '" />' . "\n\t\t";
			}
			
			if(!empty($_SESSION['relNextUrl']))
			{
				$relNextPrev .= '<link rel="next" href="' . $_SESSION['relNextUrl'];
				
				if($activeCodes->count() > 1 && gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') !== 'true')
				{
					if(strpos($_SESSION['relNextUrl'], '?') !== false)
					{
						$relNextPrev .= htmlspecialchars('&language=') . strtolower($languageCode);
					}
					else
					{
						$relNextPrev .= '?language=' . strtolower($languageCode);
					}
				}
				
				$relNextPrev .= '" />' . "\n\t\t";
			}
			
			foreach($noRelPrevNext as $key)
			{
				if(in_array($key, $getArray))
				{
					$relNextPrev = '';
					break;
				}
			}
			
			$_SESSION['relPrevUrl'] = '';
			$_SESSION['relNextUrl'] = '';
			
			$html .= $relNextPrev;
		}
		elseif(isset($_GET['coID']))
		{
			
			$coID = (int)$_GET['coID'];
			
			if($seoBoost->boost_content)
			{
				$contentId = $seoBoost->get_content_id_by_content_group($coID);
				$altUrl    = $seoBoost->get_boosted_content_url($contentId);
				
				if(substr($altUrl, 2, 1) === '/')
				{
					$altUrl = substr($altUrl, 3);
				}
				
				if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link(strtolower($languageCode) . '/' . $altUrl,
					                                                        $getParams, $connectionType, true, true,
					                                                        false, true, true) . '" />' . "\n\t\t";
				}
				elseif($activeCodes->count() > 1)
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($altUrl, $getParams . 'language='
					                                                                 . strtolower($languageCode),
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
				}
				else
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($altUrl, $getParams, $connectionType, true,
					                                                        true, false, true, true) . '" />'
					         . "\n\t\t";
				}
			}
			else
			{
				$url = FILENAME_CONTENT;
				
				if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link(strtolower($languageCode) . '/' . $url,
					                                                        $getParams . 'coID=' . $coID,
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
				}
				elseif($activeCodes->count() > 1)
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($url, $getParams . 'language='
					                                                              . strtolower($languageCode) . '&coID='
					                                                              . $coID, $connectionType, true, true,
					                                                        false, true, true) . '" />' . "\n\t\t";
				}
				else
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($url, $getParams . 'coID=' . $coID,
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
				}
			}
			
			if($robots === 'index,follow' && $activeCodes->count() > 1)
			{
				if(gm_get_conf('GM_SEO_BOOST_CONTENT') === 'true')
				{
					$langCode   = new LanguageCode(new StringType($defaultLangCode));
					$languageId = $languageProvider->getIdByCode($langCode);
					
					$contentId = $seoBoost->get_content_id_by_content_group($coID, $languageId);
					$altUrl    = $seoBoost->get_boosted_content_url($contentId, $languageId);
					
					if(substr($altUrl, 2, 1) === '/')
					{
						$altUrl = substr($altUrl, 3);
					}
					
					if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
					{
						$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($defaultLangCode
						                                                                             . '/' . $altUrl,
						                                                                             $getParams,
						                                                                             $connectionType,
						                                                                             true, true, false,
						                                                                             true, true)
						         . '" />' . "\n\t\t";
					}
					else
					{
						$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($altUrl, $getParams
						                                                                                      . 'language='
						                                                                                      . $defaultLangCode,
						                                                                             $connectionType,
						                                                                             true, true, false,
						                                                                             true, true)
						         . '" />' . "\n\t\t";
					}
				}
				elseif(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
				{
					$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($defaultLangCode . '/'
					                                                                             . $url,
					                                                                             $getParams . 'coID='
					                                                                             . $coID,
					                                                                             $connectionType, true,
					                                                                             true, false, true,
					                                                                             true) . '" />'
					         . "\n\t\t";
				}
				else
				{
					$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($url, $getParams
					                                                                                   . 'language='
					                                                                                   . $defaultLangCode
					                                                                                   . '&coID='
					                                                                                   . $coID,
					                                                                             $connectionType, true,
					                                                                             true, false, true,
					                                                                             true) . '" />'
					         . "\n\t\t";
				}
				
				foreach($activeCodes as $code)
				{
					$langCode   = new LanguageCode(new StringType($code->asString()));
					$languageId = $languageProvider->getIdByCode($langCode);
					
					if($seoBoost->boost_content)
					{
						$contentId = $seoBoost->get_content_id_by_content_group($coID, $languageId);
						$altUrl    = $seoBoost->get_boosted_content_url($contentId, $languageId);
						
						if(substr($altUrl, 2, 1) === '/')
						{
							$altUrl = substr($altUrl, 3);
						}
						
						if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
						{
							$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
							         . xtc_href_link(strtolower($code) . '/' . $altUrl, $getParams, $connectionType,
							                         true, true, false, true, true) . '" />' . "\n\t\t";
						}
						else
						{
							$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
							         . xtc_href_link($altUrl, $getParams . 'language=' . strtolower($code),
							                         $connectionType, true, true, false, true, true) . '" />'
							         . "\n\t\t";
						}
					}
					elseif(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
					{
						$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
						         . xtc_href_link(strtolower($code) . '/' . $url, $getParams . 'coID=' . $coID,
						                         $connectionType, true, true, false, true, true) . '" />' . "\n\t\t";
					}
					else
					{
						$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
						         . xtc_href_link($url, $getParams . 'language=' . strtolower($code) . '&coID=' . $coID,
						                         $connectionType, true, true, false, true, true) . '" />' . "\n\t\t";
					}
				}
			}
		}
		
		elseif(strpos(strtolower(gm_get_env_info("PHP_SELF")), 'index.php') !== false)
		{
			$t_index = '';
			
			if(gm_get_conf('SUPPRESS_INDEX_IN_URL') !== 'true')
			{
				$t_index .= 'index.php';
			}
			
			if(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
			{
				
				$html .= '<link rel="canonical" href="' . xtc_href_link(strtolower($languageCode) . '/' . $t_index,
				                                                        $getParams, $connectionType, true, true, false,
				                                                        true, true) . '" />' . "\n\t\t";
				
				if($activeCodes->count() > 1)
				{
					$html .= '<link rel="alternate" hreflang="x-default" href="'
					         . xtc_href_link(strtolower($defaultLangCode) . '/' . $t_index, $getParams, $connectionType,
					                         true, true, false, true, true) . '" />' . "\n\t\t";
					
					foreach($activeCodes as $code)
					{
						$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
						         . xtc_href_link(strtolower($code) . '/' . $t_index, $getParams, $connectionType, true,
						                         true, false, true, true) . '" />' . "\n\t\t";
					}
				}
			}
			else
			{
				if($activeCodes->count() > 1)
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($t_index, $getParams . 'language='
					                                                                  . strtolower($languageCode),
					                                                        $connectionType, true, true, false, true,
					                                                        true) . '" />' . "\n\t\t";
					
					$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($t_index, $getParams
					                                                                                       . 'language='
					                                                                                       . strtolower($defaultLangCode),
					                                                                             $connectionType, true,
					                                                                             true, false, true,
					                                                                             true) . '" />'
					         . "\n\t\t";
					
					foreach($activeCodes as $code)
					{
						$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
						         . xtc_href_link($t_index, $getParams . 'language=' . strtolower($code),
						                         $connectionType, true, true, false, true, true) . '" />' . "\n\t\t";
					}
				}
				else
				{
					$html .= '<link rel="canonical" href="' . xtc_href_link($t_index, $getParams, $connectionType, true,
					                                                        true, false, true, true) . '" />'
					         . "\n\t\t";
				}
			}
		}
		elseif(gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') === 'true')
		{
			
			$html .= '<link rel="canonical" href="' . xtc_href_link(strtolower($languageCode) . '/'
			                                                        . $trimmedRequestUri, $getParams, $connectionType,
			                                                        true, true, false, true, true) . '" />' . "\n\t\t";
			
			if($activeCodes->count() > 1)
			{
				$html .= '<link rel="alternate" hreflang="x-default" href="'
				         . xtc_href_link(strtolower($defaultLangCode) . '/' . $trimmedRequestUri, $getParams,
				                         $connectionType, true, true, false, true, true) . '" />' . "\n\t\t";
				
				foreach($activeCodes as $code)
				{
					$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
					         . xtc_href_link(strtolower($code) . '/' . $trimmedRequestUri, $getParams, $connectionType,
					                         true, true, false, true, true) . '" />' . "\n\t\t";
				}
			}
		}
		else
		{
			if($activeCodes->count() > 1)
			{
				$html .= '<link rel="canonical" href="' . xtc_href_link($trimmedRequestUri, $getParams . 'language='
				                                                                            . strtolower($languageCode),
				                                                        $connectionType, true, true, false, true, true)
				         . '" />' . "\n\t\t";
				
				$html .= '<link rel="alternate" hreflang="x-default" href="' . xtc_href_link($trimmedRequestUri,
				                                                                             $getParams . 'language='
				                                                                             . strtolower($defaultLangCode),
				                                                                             $connectionType, true,
				                                                                             true, false, true, true)
				         . '" />' . "\n\t\t";
				
				foreach($activeCodes as $code)
				{
					$html .= '<link rel="alternate" hreflang="' . strtolower($code) . '" href="'
					         . xtc_href_link($trimmedRequestUri, $getParams . 'language=' . strtolower($code),
					                         $connectionType, true, true, false, true, true) . '" />' . "\n\t\t";
				}
			}
			else
			{
				$html .= '<link rel="canonical" href="' . xtc_href_link($trimmedRequestUri, $getParams, $connectionType,
				                                                        true, true, false, true, true) . '" />'
				         . "\n\t\t";
			}
		}
		
		$relNextPrev = '';
		if(!empty($_SESSION['relPrevUrl']))
		{
			$relNextPrev .= '<link rel="prev" href="' . $_SESSION['relPrevUrl'];
			
			if($activeCodes->count() > 1 && gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') !== 'true')
			{
				if(strpos($_SESSION['relPrevUrl'], '?') !== false)
				{
					$relNextPrev .= htmlspecialchars('&language=') . strtolower($languageCode);
				}
				else
				{
					$relNextPrev .= '?language=' . strtolower($languageCode);
				}
			}
			
			$relNextPrev .= '" />' . "\n\t\t";
		}
		
		if(!empty($_SESSION['relNextUrl']))
		{
			$relNextPrev .= '<link rel="next" href="' . $_SESSION['relNextUrl'];
			
			if($activeCodes->count() > 1 && gm_get_conf('USE_SEO_BOOST_LANGUAGE_CODE') !== 'true')
			{
				if(strpos($_SESSION['relNextUrl'], '?') !== false)
				{
					$relNextPrev .= htmlspecialchars('&language=') . strtolower($languageCode);
				}
				else
				{
					$relNextPrev .= '?language=' . strtolower($languageCode);
				}
			}
			
			$relNextPrev .= '" />' . "\n\t\t";
		}
		
		foreach($noRelPrevNext as $key)
		{
			if(in_array($key, $getArray))
			{
				$relNextPrev = '';
				break;
			}
		}
		
		$_SESSION['relPrevUrl'] = '';
		$_SESSION['relNextUrl'] = '';
		
		$html .= $relNextPrev;
		
		$html = '<meta name="robots" content="' . $robots . '" />' . "\n\t\t" . $html;
		
		$this->v_output_buffer[] = $html;
	}
}
