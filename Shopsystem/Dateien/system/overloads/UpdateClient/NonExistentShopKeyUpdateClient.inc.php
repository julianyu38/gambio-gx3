<?php

/* --------------------------------------------------------------
   NonExistentShopKeyUpdateClient.inc.php 2018-02-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class NonExistentShopKeyUpdateClient
 */
class NonExistentShopKeyUpdateClient extends NonExistentShopKeyUpdateClient_parent
{
    /**
     * Whether the shop has a shop key
     */
    protected $hasShopKey;

    /**
     * NonExistentShopKeyUpdateClient constructor
     */
    public function __construct()
    {
        $this->hasShopKey = defined('GAMBIO_SHOP_KEY') && trim(GAMBIO_SHOP_KEY) !== '';
        parent::__construct();
    }

    /**
     * Set URL
     */
    protected function _setUrl()
    {
        if ($this->hasShopKey) {
            parent::_setUrl();
            return;
        }

        $url = ExternalLinks::SHOP_MESSAGES;
        $this->url = $url;
    }

    /**
     * Set parameters
     */
    protected function _setParameters()
    {
        if ($this->hasShopKey) {
            parent::_setParameters();
            return;
        }

        include(DIR_FS_CATALOG . 'release_info.php');

        $parameters = [];
        $parameters[] = 'shop_version=' . rawurlencode($gx_version);
        $this->parameters = $parameters;
    }
}