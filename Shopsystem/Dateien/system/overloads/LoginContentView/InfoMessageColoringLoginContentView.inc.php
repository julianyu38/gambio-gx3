<?php
/* --------------------------------------------------------------
   InfoMessageColoringLoginContentView.inc.php 2018-05-17
   Gambio GmbH
   http://www.gambio.de
   Copyright © 2018 Gambio GmbH
   --------------------------------------------------------------
*/

/**
 * Class InfoMessageColoringLoginContentView
 * 
 * Set a message type for success messages to give them a proper styling
 */
class InfoMessageColoringLoginContentView extends InfoMessageColoringLoginContentView_parent
{
    /**
     * Prepare data
     */
    public function prepare_data()
    {
        parent::prepare_data();
        
        $this->content_array['message_type'] = 'danger';
        if ($this->info_message === SUCCESS_PASSWORD_UPDATED || $this->info_message === TEXT_PASSWORD_SAVED) 
        {
            $this->content_array['message_type'] = 'success';
        }
    }
}