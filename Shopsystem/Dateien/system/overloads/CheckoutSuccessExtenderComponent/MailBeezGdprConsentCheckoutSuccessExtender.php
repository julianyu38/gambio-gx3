<?php

/*
 * This Extender adds the option to give GDPR consents
 *
 */

class MailBeezGdprConsentCheckoutSuccessExtender extends MailBeezGdprConsentCheckoutSuccessExtender_parent
{
    /**
     * Overloaded "proceed" method.
     */
    public function proceed()
    {
        parent::proceed();

        if (defined('MAILBEEZ_MAILHIVE_STATUS') && MAILBEEZ_MAILHIVE_STATUS == 'True') {
            if (file_exists(MH_DIR_FS_CATALOG . MH_ROOT_PATH . 'configbeez/config_gdpr_consent/includes/inc.gdpr_consent.php')) {
                include_once(MH_DIR_FS_CATALOG . MH_ROOT_PATH . 'configbeez/config_gdpr_consent/includes/inc.gdpr_consent.php');

                $customer_id = $this->v_data_array['coo_order']->customer['id'];
                $email_address = $this->v_data_array['coo_order']->customer['email_address'];

                $gdpr = new mh_gdpr_render_integration();
                $output = $gdpr->render(array('customer_id' => $customer_id, 'email_address' => $email_address, 'template' => 'gambio_checkoutsuccess', 'mode' => 'slim'));
                $this->html_output_array[] = $output;
            }
        }
    }

}