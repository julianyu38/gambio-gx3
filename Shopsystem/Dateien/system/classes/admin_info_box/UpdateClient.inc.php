<?php

/* --------------------------------------------------------------
   UpdateClient.inc.php 2018-02-28
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

require_once(DIR_FS_CATALOG . 'gm/classes/JSON.php');

/**
 * Class UpdateClient
 */
class UpdateClient 
{
	protected $url = '';

	protected $parameters = [];
	
	public function __construct()
	{
		$this->_setUrl();
        $this->_setParameters();
	}

	public function load_url()
	{
		/* @var LoadUrl $loadUrl */
		$loadUrl = MainFactory::create_object('LoadUrl');

        $url = $this->url . '?' . implode('&', $this->parameters);

		$serverResponse = $loadUrl->load_url($url, array('Accept: application/json'), 'width="100%" height="85" scrolling="no" marginheight="8" marginwidth="0" frameborder="0"', false, false);
		$c_serverResponse = (string)$serverResponse;

		$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
		$responseArray = $json->decode($c_serverResponse);

		return $responseArray;
	}

	protected function _setUrl()
	{
		$url = ExternalLinks::DYNAMIC_SHOP_MESSAGES_URL;
		$this->url = $url;
	}

	protected function _setParameters() {
        include(DIR_FS_CATALOG . 'release_info.php');

        $parameters = [];
        $parameters[] = 'shop_version=' . rawurlencode($gx_version);
        $parameters[] = 'shop_url=' . rawurlencode(HTTP_SERVER . DIR_WS_CATALOG);
        $parameters[] = 'shop_key=' . rawurlencode(GAMBIO_SHOP_KEY);
        $parameters[] = 'language=' . rawurlencode($_SESSION['language_code']);
        $parameters[] = 'server_path=' . rawurlencode(rtrim(DIR_FS_CATALOG, '/'));
        $this->parameters = $parameters;
    }
}
