<?php
/* --------------------------------------------------------------
  MainFactory.inc.php 2017-03-06
  Gambio GmbH
  http://www.gambio.de
  Copyright (c) 2017 Gambio GmbH
  Released under the GNU General Public License (Version 2)
  [http://www.gnu.org/licenses/gpl-2.0.html]
  --------------------------------------------------------------
 */

# static use!

class MainFactory
{
	public static $s_coo_class_registry_array = array();
	public static $s_coo_overload_registry = NULL;
	
	function __construct(){}

	public static function load_origin_class($p_class_name, $p_class_path = false)
	{
		# cancel, if requested class is already defined
		if(class_exists($p_class_name, false))
		{
			return;
		}
		
		$dataCache            = DataCache::get_instance();
		$mainFactoryCacheName = 'MainFactory-' . APPLICATION_RUN_MODE;
		
		$mainFactoryCache = [];
		if($dataCache->key_exists($mainFactoryCacheName, true))
		{
			$mainFactoryCache = $dataCache->get_data($mainFactoryCacheName, true);
		}
		
		$cacheFileName = 'origin-' . $p_class_name . '-' . substr(md5((string)$p_class_path), 0, 8);
		
		if(array_key_exists($cacheFileName, $mainFactoryCache))
		{
			eval($mainFactoryCache[$cacheFileName]);
			
			return;
		}
		
		$loadOriginClassCache = [];
		if($dataCache->key_exists('MainFactory-loadOrigin', true))
		{
			$loadOriginClassCache = $dataCache->get_data('MainFactory-loadOrigin', true);
		}
		
		if(!array_key_exists($cacheFileName, $loadOriginClassCache))
		{
			$loadOriginClassCache[$cacheFileName] = [];
		}
		elseif(array_key_exists('tasks', $loadOriginClassCache[$cacheFileName]))
		{
			$fileContent = '';
			$success     = true;
			
			foreach($loadOriginClassCache[$cacheFileName]['tasks'] as $task)
			{
				if(array_key_exists('require', $task))
				{
					include_once $task['require'];
					$fileContent .= "include_once '" . $task['require'] . "';\n";
				}
				elseif(array_key_exists('eval', $task))
				{
					if(!class_exists($task['eval']['class'], false) && class_exists($task['eval']['parent'], false))
					{
						eval($task['eval']['code']);
						$fileContent .= "if(!class_exists('" . $task['eval']['class'] . "', false) && class_exists('" . $task['eval']['parent'] . "', false))\n{\n";
						$fileContent .= "\t" . $task['eval']['code'] . "\n";
						$fileContent .= "}\n";
					}
					else
					{
						$success = false;
					}
				}
			}
			
			if($success)
			{
				$mainFactoryCache[$cacheFileName] = $fileContent;
				$dataCache->set_data($mainFactoryCacheName, $mainFactoryCache, true);
				
				return true;
			}
		}
		
		$t_base_class_name = $p_class_name . '_ORIGIN';
		$t_base_class_file = $p_class_path;

		# source for alias of requested class; use base as default
		$t_operation_class_name = $t_base_class_name;

		# include base class, if class_path given
		if($p_class_path !== false)
		{
			if(!class_exists($t_base_class_name, false))
			{
				require_once($t_base_class_file);
			}
			
			$taskKey = substr(md5($t_base_class_file), 0, 8);
			$loadOriginClassCache[$cacheFileName]['tasks'][$taskKey] = ['require' => $t_base_class_file];
		}

		if(class_exists($t_base_class_name, false) == true) #ELSE: included base_class_file is not an _ORIGIN
		{
			# check class chain and init
			$coo_overload_registry = self::get_overload_registry();
			$coo_overload_registry->init_class_chain($t_base_class_name, $p_class_name,
			                                         $loadOriginClassCache[$cacheFileName]);

			# class chain found?
			$t_overload_class_name = $coo_overload_registry->get($t_base_class_name);
			if($t_overload_class_name != NULL)
			{
				# use overloading class from chain
				if(is_object($GLOBALS['coo_debugger']))
				{
					$GLOBALS['coo_debugger']->log('overload found: ' . $p_class_name . ' => ' . $t_overload_class_name, 'class_overloading');
				}
				$t_operation_class_name = $t_overload_class_name;
			}

			# prepare extended "_origin"
			$t_eval_code = 'class ' . $p_class_name . ' extends ' . $t_operation_class_name . ' {}';
			if(!class_exists($p_class_name, false))
			{
				eval($t_eval_code);
				
				$loadOriginClassCache[$cacheFileName]['tasks'][] = [
					'eval' => [
						'class' => $p_class_name,
						'parent' => $t_operation_class_name,
						'code'  => $t_eval_code
					]
				];
			}
			
			$dataCache->set_data('MainFactory-loadOrigin', $loadOriginClassCache, true);
		}
	}

	public static function load_class($p_class_name, array &$cache = null)
	{
		# cancel, if requested class is already defined
		if(class_exists($p_class_name, false) && $cache === null)
		{
			return true;
		}
		
		# cancel, if requested interface is already defined
		if(interface_exists($p_class_name, false) && $cache === null)
		{
			return true;
		}
		
		$dataCache            = DataCache::get_instance();
		$mainFactoryCacheName = 'MainFactory-' . APPLICATION_RUN_MODE;
		
		$mainFactoryCache = [];
		if($dataCache->key_exists($mainFactoryCacheName, true))
		{
			$mainFactoryCache = $dataCache->get_data($mainFactoryCacheName, true);
		}
		
		$cacheKey = 'class-' . $p_class_name;
		
		if(array_key_exists($cacheKey, $mainFactoryCache))
		{
			eval($mainFactoryCache[$cacheKey]);
			
			return true;
		}
		
		$loadClassCache = [];
		if($dataCache->key_exists('MainFactory-load', true))
		{
			$loadClassCache = $dataCache->get_data('MainFactory-load', true);
		}
		
		$coo_class_registry = self::get_class_registry();
		
		if(!array_key_exists($cacheKey, $loadClassCache))
		{
			$loadClassCache[$cacheKey] = [];
		}
		elseif($cache === null && array_key_exists('tasks', $loadClassCache[$cacheKey]))
		{
			$fileContent = '';
			$success     = true;
			
			foreach($loadClassCache[$cacheKey]['tasks'] as $task)
			{
				if(array_key_exists('require', $task))
				{
					include_once $task['require'];
					$fileContent .= "include_once '" . $task['require'] . "';\n";
				}
				elseif(array_key_exists('eval', $task))
				{
					# get file path to given class
					$t_class_file = $coo_class_registry->get($task['eval']['parent']);
					
					if($t_class_file)
					{
						if(!class_exists($task['eval']['parent'], false))
						{
							include_once $t_class_file;
						}
						
						$fileContent .= "if(!class_exists('" . $task['eval']['parent'] . "', false))\n{\n";
						$fileContent .= "\tinclude_once '" . $t_class_file ."';\n";
						$fileContent .= "}\n";
					}
					
					if(!class_exists($task['eval']['class'], false) && $t_class_file)
					{
						eval($task['eval']['code']);
						$fileContent .= "if(!class_exists('" . $task['eval']['class'] . "', false))\n{\n";
						$fileContent .= "\t" . $task['eval']['code'] . "\n";
						$fileContent .= "}\n";
					}
					else
					{
						$success = false;
					}
				}
			}
			
			if($success)
			{
				$mainFactoryCache[$cacheKey] = $fileContent;
				$dataCache->add_data($mainFactoryCacheName, $mainFactoryCache, true);
				
				return true;
			}
		}
		
		# PART I: load requested base class
		# include definition of requested class
		if(class_exists($p_class_name, false) == false)
		{
			# get file path to given class
			$t_class_file = $coo_class_registry->get($p_class_name);

			if(isset($t_class_file) == false || empty($t_class_file) == true)
			{
				return false;
			}
			# include class definition
			include_once($t_class_file);
			
			$loadClassCache[$cacheKey]['tasks'][] = ['require' => $t_class_file];
			
			if($cache !== null)
			{
				$cache['tasks'][] = ['require' => $t_class_file];
			}
		}

		# PART II: check overloading classes
		$coo_overload_registry = self::get_overload_registry();

		# init chain and set final class to registry
		if($cache !== null)
		{
			$coo_overload_registry->init_class_chain($p_class_name, false, $cache);
		}
		else
		{
			$coo_overload_registry->init_class_chain($p_class_name, false, $loadClassCache[$cacheKey]);
		}
		
		$dataCache->set_data('MainFactory-load', $loadClassCache, true);
		
		return true;
	}

	/*
	 * MainFactory::create('Class', $argument1, $argument2, ...)
	 *
	 * short form for
	 *
	 * MainFactory::create_object('Class', array($argument1, $argument2, ...))
	 */
	public static function create($p_class_name)
	{
		$t_args_array = func_get_args();

		// remove first argument ($p_class_name)
		array_shift($t_args_array);

		$t_class_object = MainFactory::create_object($p_class_name, $t_args_array);

		if($t_class_object === false)
		{
			throw new InvalidArgumentException('Class not found in registry: ' . $p_class_name);
		}
		return $t_class_object;
	}

	public static function create_object($p_class_name, $p_args_array = array(), $p_use_singleton = false)
	{
		$dataCache            = DataCache::get_instance();
		$mainFactoryCacheName = 'MainFactory-' . APPLICATION_RUN_MODE;
		
		$mainFactoryCache = [];
		if($dataCache->key_exists($mainFactoryCacheName, true))
		{
			$mainFactoryCache = $dataCache->get_data($mainFactoryCacheName, true);
		}
		
		$argsCount = is_array($p_args_array) ? count($p_args_array) : 0;
		$cacheKey  = 'create-' . $p_class_name . '-' . $argsCount . '-' . (int)$p_use_singleton;
		
		if(array_key_exists($cacheKey , $mainFactoryCache))
		{
			eval($mainFactoryCache[$cacheKey]);
			
			return $outputObject;
		}
		
		$coo_stop_watch = LogControl::get_instance()->get_stop_watch();
		$coo_stop_watch->start('create_object_' . $p_class_name);
		
		$createObjectCache = [];
		if($dataCache->key_exists('MainFactory-create', true))
		{
			$createObjectCache = $dataCache->get_data('MainFactory-create', true);
		}
		
		if(!array_key_exists($cacheKey , $createObjectCache))
		{
			$createObjectCache[$cacheKey] = [];
		}
		elseif(($p_use_singleton && array_key_exists('instance', $createObjectCache[$cacheKey]))
		       || (!$p_use_singleton && array_key_exists('new', $createObjectCache[$cacheKey])))
		{
			$fileContent = '';
			$success     = true;
			$classKey    = $p_use_singleton ? 'instance' : 'new';
			
			if(array_key_exists($argsCount, $createObjectCache[$cacheKey][$classKey]))
			{
				$createClassCode = $createObjectCache[$cacheKey][$classKey][$argsCount];
				
				if(array_key_exists('tasks', $createObjectCache[$cacheKey]))
				{
					$fileContent .= "if(class_exists('" . $createObjectCache[$cacheKey][$classKey]['operationClass'] . "', false))\n{\n";
					$fileContent .= "\t" . $createClassCode ."\n";
					$fileContent .= "\treturn;\n";
					$fileContent .= "}\n";
					
					$registry = self::get_class_registry();
					
					foreach($createObjectCache[$cacheKey]['tasks'] as $task)
					{
						if(array_key_exists('require', $task))
						{
							include_once $task['require'];
							$fileContent .= "include_once '" . $task['require'] . "';\n";
						}
						elseif(array_key_exists('eval', $task))
						{
							# get file path to given class
							$t_class_file = $registry->get($task['eval']['parent']);
							
							if($t_class_file)
							{
								if(!class_exists($task['eval']['parent'], false))
								{
									include_once $t_class_file;
								}
								
								$fileContent .= "if(!class_exists('" . $task['eval']['parent'] . "', false))\n{\n";
								$fileContent .= "\tinclude_once '" . $t_class_file . "';\n";
								$fileContent .= "}\n";
							}
							
							$fileContent .= "if(!class_exists('" . $task['eval']['class'] . "', false))\n{\n";
							$fileContent .= "\t" . $task['eval']['code'] ."\n";
							$fileContent .= "}\n";
							
							if(!class_exists($task['eval']['class'], false) && $t_class_file)
							{
								eval($task['eval']['code']);
							}
							else
							{
								$success = false;
							}
						}
					}
				}
			}
			else
			{
				$success = false;
			}
			
			$fileContent .= $createClassCode . "\n";
			
			$mainFactoryCache[$cacheKey] = $fileContent;
			$dataCache->add_data($mainFactoryCacheName, $mainFactoryCache, true);
			
			if($success)
			{
				eval($createClassCode);
				
				$coo_stop_watch->stop('create_object_' . $p_class_name);
				return $outputObject;
			}
		}
		
		$t_operation_class_name = $p_class_name;

		# load/include original class definition
		$t_load_success = self::load_class($p_class_name, $createObjectCache[$cacheKey]);
		if($t_load_success === false)
		{
			$dataCache->clear_cache();
			
			$template = defined('CURRENT_TEMPLATE') ? ' (Template: ' . CURRENT_TEMPLATE . ')' : '';
			trigger_error('Class not found in registry: ' . $p_class_name . $template . '. The registry cache was cleared. Please reload the page.', E_USER_ERROR);
			
			return false;
		}

		# try loading overload-version
		$coo_overload_registry = self::get_overload_registry('coo_overload_registry');
		$coo_overload_registry->init_class_chain($p_class_name, false, $createObjectCache[$cacheKey]);
		$t_overload_class_name = $coo_overload_registry->get($p_class_name);
		if($t_overload_class_name !== null)
		{
			if(is_object($GLOBALS['coo_debugger']))
			{
				$GLOBALS['coo_debugger']->log('overload found: ' . $p_class_name . ' => ' . $t_overload_class_name, 'class_overloading');
			}
			$t_operation_class_name = $t_overload_class_name;
		}

		# build constructor string for eval()
		$t_constructor_args_string = '';

		if(is_array($p_args_array) && count($p_args_array))
		{
			$t_constructor_args_array = array();
			
			foreach($p_args_array as $key => &$item)
			{
				$t_constructor_args_array[] = '$p_args_array[' . $key . ']';
			}
			
			$t_constructor_args_string .= implode(', ', $t_constructor_args_array);
		}

		# create object
		$outputObject = false;

		if($p_use_singleton === true)
		{
			# use get_instance instead of new
			# $coo_object = ClassName::get_instance($p_param1, $p_param2);
			$t_eval_code = '$outputObject = ' . $t_operation_class_name . '::get_instance(' . $t_constructor_args_string . ');';
			
			$createObjectCache[$cacheKey]['instance'][$argsCount]       = $t_eval_code;
			$createObjectCache[$cacheKey]['instance']['operationClass'] = $t_operation_class_name;
		}
		else
		{
			# ORIGIN-class used without new-operator?
			if($t_operation_class_name !== $p_class_name)
			{
				$t_parents_array = array_values(class_parents($t_operation_class_name));
				$t_last_parent = $t_parents_array[count($t_parents_array) - 1];

				if(substr($t_last_parent, -7) === '_ORIGIN')
				{
					if(class_exists($p_class_name, false) === false)
					{
						$t_eval_code = 'class ' . $p_class_name . ' extends ' . $t_operation_class_name . ' {}';
						eval($t_eval_code);
						
						$createObjectCache[$cacheKey]['tasks'][] = [
							'eval' => [
								'class' => $p_class_name,
								'parent' => $t_operation_class_name,
								'code'  => $t_eval_code
							]
						];
					}
					$t_operation_class_name = $p_class_name;
				}
			}

			# use new for creating a new instance
			$t_eval_code = '$outputObject = new ' . $t_operation_class_name . '(' . $t_constructor_args_string . ');';
			
			$createObjectCache[$cacheKey]['new'][$argsCount]       = $t_eval_code;
			$createObjectCache[$cacheKey]['new']['operationClass'] = $t_operation_class_name;
		}

		if($t_overload_class_name !== null && is_object($GLOBALS['coo_debugger']))
		{
			$GLOBALS['coo_debugger']->log('eval: ' . $t_eval_code, 'class_overloading');
		}
		
		$dataCache->set_data('MainFactory-create', $createObjectCache, true);
		
		eval($t_eval_code);

		if($outputObject === false)
		{
			trigger_error('create_object failed: ' . $t_operation_class_name, E_USER_ERROR);
		}

		$coo_stop_watch->stop('create_object_' . $p_class_name);
		return $outputObject;
	}
	
	public static function get_class_registry()
	{
		$currentTemplate = (defined('CURRENT_TEMPLATE')) ? CURRENT_TEMPLATE : 'template_blank';
		
		$coo_class_registry = null;
		if(array_key_exists($currentTemplate, self::$s_coo_class_registry_array))
		{
			$coo_class_registry = self::$s_coo_class_registry_array[$currentTemplate];
		}
		
		if($coo_class_registry === null)
		{
			# try building object from cache
			$t_cache_key = 'ClassRegistry_' . $currentTemplate;
			$coo_cache = DataCache::get_instance();
			if($coo_cache->key_exists($t_cache_key, true))
			{
				#use cached object
				$coo_class_registry = $coo_cache->get_data($t_cache_key);
			}
			else
			{
				# build new registry object
				# directories to be scanned by ClassRegistry
				$t_scan_dirs_array = array(
					DIR_FS_CATALOG . 'admin/includes/classes',
					DIR_FS_CATALOG . 'admin/includes/gm/classes',
					DIR_FS_CATALOG . 'gm/classes',
					DIR_FS_CATALOG . 'gm/properties',
					DIR_FS_CATALOG . 'templates/' . $currentTemplate . '/source/classes',
					DIR_FS_CATALOG . 'system/controls',
					DIR_FS_CATALOG . 'system/data',
					DIR_FS_CATALOG . 'system/views',
					DIR_FS_CATALOG . 'system/request_port',
					DIR_FS_CATALOG . 'system/overloads',
					DIR_FS_CATALOG . 'system/extender',
					DIR_FS_CATALOG . 'system/classes',
					DIR_FS_CATALOG . 'system/core',
					DIR_FS_CATALOG . 'GXEngine',
					DIR_FS_CATALOG . 'GXMainComponents',
					DIR_FS_CATALOG . 'GXUserComponents',
					DIR_FS_CATALOG . 'GXModules'
				);
				
				foreach($t_scan_dirs_array as $t_key => $t_dir)
				{
					if(is_dir($t_dir) === false)
					{
						unset($t_scan_dirs_array[$t_key]);
					}
				}
				
				$coo_class_registry = ClassRegistry::get_instance();
				
				foreach($t_scan_dirs_array as $t_dir_item)
				{
					$coo_class_registry->scan_dir($t_dir_item, true);
				}
				
				#write object to cache
				$coo_cache->set_data($t_cache_key, $coo_class_registry, true);
			}
			self::$s_coo_class_registry_array[$currentTemplate] = $coo_class_registry;
		}
		
		return $coo_class_registry;
	}
	
	public static function get_overload_registry()
	{
		$coo_overload_registry = self::$s_coo_overload_registry;
		if($coo_overload_registry === NULL)
		{
			$coo_overload_registry = ClassOverloadRegistry::get_instance();
			$coo_overload_registry->set_class_overload_system_dir(DIR_FS_CATALOG . 'system/overloads/');
			$coo_overload_registry->set_class_overload_user_dir(DIR_FS_CATALOG . 'GXUserComponents/overloads/');
			
			self::$s_coo_overload_registry = $coo_overload_registry;
		}
		return $coo_overload_registry;
	}
}
