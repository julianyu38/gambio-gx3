<?php
/* --------------------------------------------------------------
   GXModulesCache.inc.php 2018-04-13
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

/**
 * Class GXModulesCache
 */
class GXModulesCache
{
	/**
	 * Returns an array of all files in the GXModules directory as absolute file path.
	 *
	 * Notice: Only files are considered, so empty directories are skipped.
	 *
	 * @return array
	 */
	public static function getFiles()
	{
		$dataCache = DataCache::get_instance();
		$cacheKey  = 'GXModules-files';
		
		$files = [];
		if($dataCache->key_exists($cacheKey, true))
		{
			$files = $dataCache->get_data($cacheKey, true);
		}
		else
		{
			$iterator = new RecursiveDirectoryIterator(DIR_FS_CATALOG . 'GXModules');
			
			foreach(new RecursiveIteratorIterator($iterator) as $file)
			{
				if($file->getFilename() !== '.' && $file->getFilename() !== '..')
				{
					$files[] = str_replace('\\', '/', (string)$file);
				}
			}
			
			$dataCache->set_data($cacheKey, $files, true);
		}
		
		return $files;
	}
	
	
	/**
	 * Returns an array of installed module files in the GXModules directory as absolute file path.
	 *
	 * Notice: Only files are considered, so empty directories are skipped.
	 *
	 * @return array
	 */
	public static function getInstalledModuleFiles()
	{
		static $checkedMatches;
		if($checkedMatches === null)
		{
			$checkedMatches = [];
		}
		
		$allFiles              = self::getFiles();
		$installedModulesFiles = [];
		
		$dataCache = DataCache::get_instance();
		$cacheKey  = 'installed-GXModules-files';
		if($dataCache->key_exists($cacheKey, true))
		{
			return $dataCache->get_data($cacheKey, true);
		}
		elseif(!isset($GLOBALS['db_link']))
		{
			return $allFiles;
		}
		
		foreach($allFiles as $file)
		{
			$moduleIsActive = true;
			preg_match("/GXModules\/([^\/]+\/[^\/]+)/", $file, $matches);
			
			if(!array_key_exists($matches[0], $checkedMatches) && is_dir(DIR_FS_CATALOG . $matches[0])
			   && file_exists(DIR_FS_CATALOG . $matches[0] . '/GXModule.json'))
			{
				$gxModuleConfig = json_decode(file_get_contents(DIR_FS_CATALOG . $matches[0] . '/GXModule.json'), true);
				if(array_key_exists('forceIncludingFiles', $gxModuleConfig) && $gxModuleConfig['forceIncludingFiles'])
				{
					$moduleIsActive = true;
				}
				else
				{
					$namespace            = 'modules/' . str_replace('/', '', $matches[1]);
					$configurationStorage = MainFactory::create('ConfigurationStorage', $namespace);
					$moduleIsActive       = (bool)$configurationStorage->get('active');
				}
			}
			elseif(array_key_exists($matches[0], $checkedMatches))
			{
				$moduleIsActive = $checkedMatches[$matches[0]];
			}
			
			if($moduleIsActive)
			{
				$installedModulesFiles[] = str_replace('\\', '/', (string)$file);
			}
			
			$checkedMatches[$matches[0]] = $moduleIsActive;
		}
		
		$dataCache->set_data($cacheKey, $installedModulesFiles, true);
		
		# Clear MainFactory cache to remove Overloads that may be exist in that cache.
		$dataCache->clear_cache('MainFactory-create');
		$dataCache->clear_cache('MainFactory-load');
		$dataCache->clear_cache('MainFactory-loadOrigin');
		
		return $installedModulesFiles;
	}
}