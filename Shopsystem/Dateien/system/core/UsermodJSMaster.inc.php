<?php
/* --------------------------------------------------------------
   UsermodJSMaster.inc.php 2018-04-11
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class UsermodJSMaster
{
	public $v_page = '';

	public function __construct($p_page = 'Global')
	{
		$this->set_page($p_page);
	}
	
	
	public function set_page($p_page)
	{
		$this->v_page = basename($p_page);
	}
	
	
	public function get_page()
	{
		return $this->v_page;
	}
	
	
	public function get_files()
	{
		$dataCache = DataCache::get_instance();
		$cacheKey  = 'js_usermods-' . CURRENT_TEMPLATE. '-' . $this->get_page();
		
		if($dataCache->key_exists($cacheKey, true))
		{
			return $dataCache->get_data($cacheKey, true);
		}
		
		$t_files_array = array();

		$t_coo_cached_directory = new CachedDirectory(DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/usermod/javascript/Global');

		while(false !== ($t_entry = $t_coo_cached_directory->read() ))
		{
			if ($t_entry[0] !== '.' && substr($t_entry, -3) === '.js')
			{
				$t_files_array[] = DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/usermod/javascript/Global/' . basename($t_entry);
			}
		}

		if($this->get_page() !== 'Global')
		{
			$t_coo_cached_directory->set_path(DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/usermod/javascript/' . $this->get_page());

			while(false !== ($t_entry = $t_coo_cached_directory->read() ))
			{
				if ($t_entry[0] !== '.' && substr($t_entry, -3) === '.js')
				{
					$t_files_array[] = DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/usermod/javascript/' . $this->get_page() . '/' . basename($t_entry);
				}				
			}
		}
		
		$gxModuleFiles = GXModulesCache::getInstalledModuleFiles();
		
		foreach($gxModuleFiles as $file)
		{
			if(substr($file, -3) === '.js'
			   && (stripos($file, '/Templates/' . CURRENT_TEMPLATE . '/Javascript/' . $this->get_page() . '/') !== false
			       || stripos($file, '/Templates/All/Javascript/' . $this->get_page() . '/') !== false))
			{
				$t_files_array[] = $file;
			}
		}
		
		$dataCache->set_data($cacheKey, $t_files_array, true);
		
		return $t_files_array;
	}
}