<?php
/* --------------------------------------------------------------
   ClassOverloadRegistry.inc.php 2018-04-10
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2018 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

class ClassOverloadRegistry extends Registry
{
	public $v_class_overload_system_dir = '';
	public $v_class_overload_user_dir   = '';
	
	public $v_admin_prefixed_classes_array = array(
		'language',
		'messageStack',
		'order',
		'shoppingCart',
		'splitPageResults'
	);
	
	protected static $cachedDirectories = array();
	
	protected static $classNameMappings = null;
	
	
	/*
	 * constructor
	 */
	public function __construct()
	{
		if(is_object($GLOBALS['coo_debugger']))
		{
			$GLOBALS['coo_debugger']->log('ClassOverloadRegistry() by ' . gm_get_env_info('REQUEST_URI'),
			                              'ClassOverloadRegistry');
		}
	}

	public static function &get_instance()
	{
		static $s_instance;
		
		if($s_instance === null)
		{
			$s_instance = new ClassOverloadRegistry();
		}
		
		return $s_instance;
	}
	
	
	public function set_class_overload_system_dir($p_path)
	{
		$this->v_class_overload_system_dir = $p_path;
	}
	
	
	public function get_class_overload_system_dir()
	{
		$t_output = $this->v_class_overload_system_dir;
		
		return $t_output;
	}
	
	
	public function set_class_overload_user_dir($p_path)
	{
		$this->v_class_overload_user_dir = $p_path;
	}
	
	
	public function get_class_overload_user_dir()
	{
		$t_output = $this->v_class_overload_user_dir;
		
		return $t_output;
	}
	
	
	public function get_directory_classes($p_class_overload_dir)
	{
		$t_found_classes_array = array();
		
		if(!empty(self::$cachedDirectories[$p_class_overload_dir]))
		{
			$t_coo_cached_directory = self::$cachedDirectories[$p_class_overload_dir];
			$t_coo_cached_directory->reset_count_index();
		}
		else
		{
			$t_coo_cached_directory                         = new CachedDirectory($p_class_overload_dir);
			self::$cachedDirectories[$p_class_overload_dir] = $t_coo_cached_directory;
		}
		
		if($t_coo_cached_directory->is_dir($p_class_overload_dir) == false)
		{
			# return empty array, if overload directory not found
			return $t_found_classes_array;
		}
		
		$t_file_pattern = '.php';
		
		while(($t_entry = $t_coo_cached_directory->read()) !== false)
		{
			if(substr($t_entry, 0, 1) === '.')
			{
				continue;
			}
			
			# entry is a file and ends with '.php'
			if($t_coo_cached_directory->is_file($p_class_overload_dir . '/' . $t_entry)
			   && strpos($t_entry, $t_file_pattern, strlen($t_entry) - strlen($t_file_pattern)) > 0
			)
			{
				$t_system_class_path = $p_class_overload_dir . '/' . $t_entry;
				$t_user_class_path   = DIR_FS_CATALOG . 'GXUserComponents/' . $t_entry;
				
				if($t_coo_cached_directory->file_exists($t_user_class_path))
				{
					$t_found_classes_array[] = $t_user_class_path;
				}
				else
				{
					$t_found_classes_array[] = $t_system_class_path;
				}
			}
		}
		
		return $t_found_classes_array;
	}
	
	
	public function init_class_chain($p_base_class_name, $p_overload_subdir = false, array &$cache = [])
	{
		# set overload directory
		$t_system_base_dir = $this->get_class_overload_system_dir();
		$t_user_base_dir   = $this->get_class_overload_user_dir();
		
		if($p_overload_subdir === false)
		{
			$t_class_overload_system_dir       = $t_system_base_dir . $p_base_class_name;
			$t_class_overload_user_dir         = $t_user_base_dir . $p_base_class_name;
			$t_class_overload_system_dir_admin = $t_system_base_dir . 'Admin-' . $p_base_class_name;
			$t_class_overload_user_dir_admin   = $t_user_base_dir . 'Admin-' . $p_base_class_name;
		}
		else
		{
			$t_class_overload_system_dir       = $t_system_base_dir . $p_overload_subdir;
			$t_class_overload_user_dir         = $t_user_base_dir . $p_overload_subdir;
			$t_class_overload_system_dir_admin = $t_system_base_dir . 'Admin-' . $p_overload_subdir;
			$t_class_overload_user_dir_admin   = $t_user_base_dir . 'Admin-' . $p_overload_subdir;
		}
		
		# search class files
		
		$t_admin_prefix_class = false;
		if(in_array($p_base_class_name, $this->v_admin_prefixed_classes_array) === true)
		{
			$t_admin_prefix_class = true;
		}
		if(in_array($p_overload_subdir, $this->v_admin_prefixed_classes_array) === true)
		{
			$t_admin_prefix_class = true;
		}
		
		if(APPLICATION_RUN_MODE == 'backend' && $t_admin_prefix_class === true)
		{
			$t_system_overload_classes = $this->get_directory_classes($t_class_overload_system_dir_admin);
			$t_user_overload_classes   = $this->get_directory_classes($t_class_overload_user_dir_admin);
		}
		else
		{
			$t_system_overload_classes = $this->get_directory_classes($t_class_overload_system_dir);
			$t_user_overload_classes   = $this->get_directory_classes($t_class_overload_user_dir);
		}
		
		$className   = $p_overload_subdir ? : $p_base_class_name;
		$classPrefix = APPLICATION_RUN_MODE === 'backend' && $t_admin_prefix_class === true ? 'Admin-' : '';
		$className   = $classPrefix . $className;
		
		$gxModulesOverloadClasses = $this->_getGxModulesOverloadClasses($className);
		
		sort($t_system_overload_classes);
		sort($gxModulesOverloadClasses);
		sort($t_user_overload_classes);
		
		$t_found_classes_array = array_merge($t_system_overload_classes, $gxModulesOverloadClasses,
		                                     $t_user_overload_classes);
		
		# debug: show found chain
		if(is_object($GLOBALS['coo_debugger']))
		{
			$GLOBALS['coo_debugger']->log('class chain ' . $p_base_class_name . ': ' . print_r($t_found_classes_array,
			                                                                                   true),
			                              'class_overloading');
		}
		
		# create class-aliases
		$t_parent_class  = $p_base_class_name;
		$t_current_class = '';
		
		foreach($t_found_classes_array as $classFilePath)
		{
			$t_file_name = basename($classFilePath);
			
			$t_current_class = strtok($t_file_name, '.');
			$t_new_class     = $t_current_class . '_parent';
			
			# prepare extended "_parent"
			$t_eval_code = 'class ' . $t_new_class . ' extends ' . $t_parent_class . ' {}';
			
			$evalCodeFound    = false;
			$requireCodeFound = false;
			
			if(array_key_exists('tasks', $cache))
			{
				foreach($cache['tasks'] as $task)
				{
					if(array_key_exists('eval', $task) && $task['eval']['code'] === $t_eval_code)
					{
						$evalCodeFound = true;
					}
					elseif(array_key_exists('require', $task) && $task['require'] === $classFilePath)
					{
						$requireCodeFound = true;
					}
				}
			}
			
			if(!$evalCodeFound)
			{
				$cache['tasks'][] = [
					'eval' => [
						'class'  => $t_new_class,
						'parent' => $t_parent_class,
						'code'   => $t_eval_code
					]
				];
			}
			
			if(!$requireCodeFound)
			{
				$cache['tasks'][] = ['require' => $classFilePath];
			}
			
			if(!class_exists($t_new_class, false))
			{
				eval($t_eval_code);
				
				# include extending class
				include_once $classFilePath;
			}
			
			# look for extension of extending class
			$this->init_class_chain($t_current_class, false, $cache);
			$t_found_extension_class = $this->get($t_current_class);
			
			if(empty($t_found_extension_class) === false)
			{
				$t_current_class = $t_found_extension_class;
			}
			
			# prepare parent-name for next round
			$t_parent_class = $t_current_class;
		}
		
		# save final class in registry
		if(count($t_found_classes_array))
		{
			$t_last_chain_class = $t_current_class;
			$this->set($p_base_class_name, $t_last_chain_class);
			
			# final overloading class found
			return true;
		}
		
		# overload directory seems empty
		return false;
	}
	
	
	/**
	 * Get GXModules overload classes
	 *
	 * @param $className class name
	 *
	 * @return array
	 */
	protected function _getGxModulesOverloadClasses($className)
	{
		if(defined('UNIT_TEST_RUNNING') && constant('UNIT_TEST_RUNNING')){
			return [];
		}
		static $overloadDirs;
		static $moduleOverloadClasses = [];
		
		if($overloadDirs === null)
		{
			$overloadDirs = [];
			$files        = GXModulesCache::getInstalledModuleFiles();
			
			foreach($files as $file)
			{
				$strpos = stripos($file, 'overloads');
				
				if($strpos)
				{
					$dir                = substr($file, 0, $strpos + strlen('overloads/'));
					$overloadDirs[$dir] = $dir;
				}
			}
		}
		
		if(!isset($moduleOverloadClasses[$className]))
		{
			$moduleOverloadClasses[$className] = [];
			
			foreach($overloadDirs as $dir)
			{
				$moduleOverloadClasses[$className] = array_merge($moduleOverloadClasses[$className],
				                                                 $this->get_directory_classes($dir . $className));
			}
		}
		
		return $moduleOverloadClasses[$className];
	}
}
