<?php
/*
 * Shopgate GmbH
 * http://www.shopgate.com
 * Copyright © 2012-2015 Shopgate GmbH
 *
 * Released under the GNU General Public License (Version 2)
 * [http://www.gnu.org/licenses/gpl-2.0.html]
 */

class ShopgateItemAttributeModel extends ShopgateObject
{
    /**
     * read products attrubute data from database
     *
     * @param int    $productsId
     * @param bool   $normalizeKey
     * @param string $normalizationDelimiter
     *
     * @return array
     *
     * @throws ShopgateLibraryException
     */
    public function getAttributesFromDatabase($productsId, $normalizeKey = false, $normalizationDelimiter = '-')
    {
        $attributes = array();

        $query = sprintf(
            "
            SELECT
                *
            FROM %s AS a
                WHERE a.products_id = %s",
            TABLE_PRODUCTS_ATTRIBUTES,
            $productsId
        );

        $result = xtc_db_query($query);

        if (!$result) {
            throw new ShopgateLibraryException(
                ShopgateLibraryException::PLUGIN_DATABASE_ERROR,
                "Shopgate Plugin - Error checking for table \"" . TABLE_PRODUCTS_ATTRIBUTES . "\".",
                true
            );
        }

        // structure the data in a way so it can be accessed in an easy fashion
        while ($row = xtc_db_fetch_array($result)) {
            $key = $row['products_attributes_id'];
            if ($normalizeKey) {
                $key = implode($normalizationDelimiter, array($row['options_id'], $row['options_values_id']));
            }
            $attributes[$key] = $row;
        }

        return $attributes;
    }

    /**
     * @param int $productsId
     *
     * @return array
     *
     * @throws ShopgateLibraryException
     */
    public function getPropertyCombinationsFromDatabase($productsId)
    {
        $attributes = array();

        $query = sprintf(
            "
            SELECT
                *
            FROM %s AS a
                WHERE a.products_id = %s",
            TABLE_PRODUCTS_PROPERTIES_COMBIS,
            $productsId
        );

        $result = xtc_db_query($query);

        if (!$result) {
            throw new ShopgateLibraryException(
                ShopgateLibraryException::PLUGIN_DATABASE_ERROR,
                "Shopgate Plugin - Error checking for table \"" . TABLE_PRODUCTS_PROPERTIES_COMBIS . "\".",
                true
            );
        }

        // structure the data in a way so it can be accessed in an easy fashion
        while ($row = xtc_db_fetch_array($result)) {
            $attributes[$row['products_properties_combis_id']] = $row;
        }

        return $attributes;
    }
}
