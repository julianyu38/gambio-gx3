<?php
/*
 * Shopgate GmbH
 * http://www.shopgate.com
 * Copyright © 2012-2015 Shopgate GmbH
 * 
 * Released under the GNU General Public License (Version 2)
 * [http://www.gnu.org/licenses/gpl-2.0.html]
 */

/**
 * Class ShopgateItemPropertyModel
 */
class ShopgateItemPropertyModel
{
    /** @var string */
    private $name;
    /** @var ShopgateItemPropertyValueModel[] */
    private $values;
    
    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->values = array();
        $this->name   = $name;
    }
    
    /**
     * @param ShopgateItemPropertyValueModel $shopgateItemPropertyValue
     */
    public function addValue(ShopgateItemPropertyValueModel $shopgateItemPropertyValue)
    {
        $this->values[] = $shopgateItemPropertyValue;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * @return ShopgateItemPropertyValueModel[]
     */
    public function getValues()
    {
        return $this->values;
    }
    
    /**
     * @param ShopgateItemPropertyValueModel[] $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }
}