<?php
/*
 * Shopgate GmbH
 * http://www.shopgate.com
 * Copyright © 2012-2015 Shopgate GmbH
 * 
 * Released under the GNU General Public License (Version 2)
 * [http://www.gnu.org/licenses/gpl-2.0.html]
 */
 
class ShopgateCouponHelper
{
    /**
     * If the current order item (product) is a child product the item number was
     * generated in the schema <productId>_<attributeId>.
     * This function returns the shop system product id
     *
     * @param ShopgateOrderItem $sgOrderItem
     *
     * @return string
     */
    public static function getProductIdFromCartItem(ShopgateOrderItem $sgOrderItem)
    {
        $id = $sgOrderItem->getParentItemNumber();
        if (empty($id)) {
            $ids   = $sgOrderItem->getItemNumber();
            $idArr = explode('_', $ids);
            $id    = $idArr[0];
        }

        return $id;
    }

    /**
     * Calculate the complete amount of all items in a specific shopping cart
     *
     * @param ShopgateCart $cart
     *
     * @return float|int
     */
    public static function getCompleteAmount(ShopgateCart $cart)
    {
        $completeAmount = 0;
        foreach ($cart->getItems() as $item) {
            // It seems to happen that unit_amount_with_tax is not set in every case for method checkCart
            if ($item->getUnitAmountWithTax()) {
                $itemAmount = $item->getUnitAmountWithTax();
            } elseif ($item->getTaxPercent() > 0) {
                $itemAmount = $item->getUnitAmount() * (1 + ($item->getTaxPercent()/100));
            } else {
                $itemAmount = $item->getUnitAmount();
            }

            $completeAmount += $itemAmount * $item->getQuantity();
        }

        return $completeAmount;
    }
}
