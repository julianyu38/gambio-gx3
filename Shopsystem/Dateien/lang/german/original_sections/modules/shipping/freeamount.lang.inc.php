<?php
/* --------------------------------------------------------------
	freeamount.lang.inc.php 2018-02-16
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'MODULE_SHIPPING_FREEAMOUNT_ALLOWED_DESC' => 'Geben Sie <b>einzeln</b> die Zonen an, in welche ein Versand möglich sein soll. (z.B. AT,DE (lassen Sie dieses Feld leer, wenn Sie alle Zonen erlauben wollen))',
	'MODULE_SHIPPING_FREEAMOUNT_ALLOWED_TITLE' => 'Erlaubte Versandzonen',
	'MODULE_SHIPPING_FREEAMOUNT_AMOUNT_DESC' => 'Mindestbestellwert, damit der Versand kostenlos ist?',
	'MODULE_SHIPPING_FREEAMOUNT_AMOUNT_TITLE' => 'Mindestbetrag',
	'MODULE_SHIPPING_FREEAMOUNT_DISPLAY_DESC' => 'Möchten Sie anzeigen, wenn der Mindestbetrag zur VK-freien Lieferung nicht erreicht ist?',
	'MODULE_SHIPPING_FREEAMOUNT_DISPLAY_TITLE' => 'Anzeige aktivieren',
	'MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER' => 'Sortierreihenfolge',
	'MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER_DESC' => 'Reihenfolge der Anzeige',
	'MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER_TITLE' => 'Sortierreihenfolge',
	'MODULE_SHIPPING_FREEAMOUNT_STATUS_DESC' => 'Möchten Sie Versandkostenfreie Lieferung anbieten?',
	'MODULE_SHIPPING_FREEAMOUNT_STATUS_TITLE' => 'Versandkostenfreie Lieferung aktivieren',
	'MODULE_SHIPPING_FREEAMOUNT_TEXT_DESCRIPTION' => 'Versandkostenfreie Lieferung',
	'MODULE_SHIPPING_FREEAMOUNT_TEXT_TITLE' => 'Versandkostenfrei',
	'MODULE_SHIPPING_FREEAMOUNT_TEXT_WAY' => 'ab %s Warenwert können Sie versandkostenfreie Lieferung nutzen',
	'MODULE_SHIPPING_FREEAMOUNT_TAX_CLASS_DESC' => 'Folgende Steuerklasse an Versandkosten anwenden',
	'MODULE_SHIPPING_FREEAMOUNT_TAX_CLASS_TITLE' => 'Steuerklasse',
);