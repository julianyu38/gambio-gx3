<?php
/*
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
*/

$t_language_text_section_content_array = array
(
	'MODULE_PAYMENT_HPDDPG_TEXT_TITLE' => 'Versicherte Lastschrift',
	'MODULE_PAYMENT_HPDDPG_TEXT_DESC' => 'Versicherte Lastschrift &uuml;ber Heidelberger Payment GmbH',
	'MODULE_PAYMENT_HPDDPG_TEXT_INFO' => 'Kauf auf Lastschrift',
	'MODULE_PAYMENT_HPDDPG_SALUTATION' => 'Anrede',
	'MODULE_PAYMENT_HPDDPG_FEMALE' => 'Frau',
	'MODULE_PAYMENT_HPDDPG_MALE' => 'Herr',
	'MODULE_PAYMENT_HPDDPG_BIRTHDATE' => 'Geburtsdatum',
	'MODULE_PAYMENT_HPDDPG_BIRTHYEAR' => 'Jahr',
	'MODULE_PAYMENT_HPDDPG_BIRTHMONTH' => 'Monat',
	'MODULE_PAYMENT_HPDDPG_BIRTHDAY' => 'Tag',
	'MODULE_PAYMENT_HPDDPG_TEST_ACCOUNT_TITLE' => 'Test Account',
	'MODULE_PAYMENT_HPDDPG_TEST_ACCOUNT_DESC' => 'Im Sandbox Mode sollen folgende E-Mail-Accounts testen können. (Komma getrennt)',
	'MODULE_PAYMENT_HPDDPG_PROCESSED_STATUS_ID_TITLE' => 'Bestellstatus - Erfolgreich',
	'MODULE_PAYMENT_HPDDPG_PROCESSED_STATUS_ID_DESC' => 'Dieser Status wird gesetzt wenn die Bezahlung erfolgreich war.',
	'MODULE_PAYMENT_HPDDPG_PENDING_STATUS_ID_TITLE' => 'Bestellstatus - Wartend',
	'MODULE_PAYMENT_HPDDPG_PENDING_STATUS_ID_DESC' => 'Dieser Status wird gesetzt wenn die der Kunde auf einem Fremdsystem ist.',
	'MODULE_PAYMENT_HPDDPG_CANCELED_STATUS_ID_TITLE' => 'Bestellstatus - Abbruch',
	'MODULE_PAYMENT_HPDDPG_CANCELED_STATUS_ID_DESC' => 'Dieser Status wird gesetzt wenn die Bezahlung abgebrochen wurde.',
	'MODULE_PAYMENT_HPDDPG_STATUS_TITLE' => 'Modul aktivieren',
	'MODULE_PAYMENT_HPDDPG_STATUS_DESC' => 'Möchten Sie das Modul aktivieren?',
	'MODULE_PAYMENT_HPDDPG_SORT_ORDER_TITLE' => 'Anzeigereihenfolge',
	'MODULE_PAYMENT_HPDDPG_SORT_ORDER_DESC' => 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.',
	'MODULE_PAYMENT_HPDDPG_ZONE_TITLE' => 'Zahlungszone',
	'MODULE_PAYMENT_HPDDPG_ZONE_DESC' => 'Wenn eine Zone ausgewählt ist, gilt die Zahlungsmethode nur für diese Zone.',
	'MODULE_PAYMENT_HPDDPG_ALLOWED_TITLE' => 'Erlaubte Zonen',
	'MODULE_PAYMENT_HPDDPG_ALLOWED_DESC' => 'Geben Sie <b>einzeln</b> die Zonen an, welche für dieses Modul erlaubt sein sollen. (z.B. AT,DE (wenn leer, werden alle Zonen erlaubt))',
	'MODULE_PAYMENT_HPDDPG_EMAIL_TEXT' => '{LEGALNOTE}<br><br>
	<b>Wir ziehen einen Betrag von {CURRENCY} {AMOUNT} von folgendem Konto ein:</b><br>
	<br>
	Empf&auml;nger: {ACC_OWNER} <br>
	Kontonr.:  {ACC_NUMBER} <br>
	BLZ: {ACC_BANKCODE} <br>
	Bank: {ACC_BANKNAME} <br>
	IBAN: {ACC_IBAN}<br>
	BIC: {ACC_BIC}<br>
	<br>'
);