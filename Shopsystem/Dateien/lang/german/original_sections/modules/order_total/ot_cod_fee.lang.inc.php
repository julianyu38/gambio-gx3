<?php
/* --------------------------------------------------------------
	ot_cod_fee.lang.inc.php 2016-12-16
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'MODULE_ORDER_TOTAL_COD_FEE_DESCRIPTION' => 'Berechnung der Nachnahmegebühr',
	'MODULE_ORDER_TOTAL_COD_FEE_RULES_DESC' => '&lt;ISO2-Code&gt;:&lt;Preis&gt;, ...<br />00 als ISO2-Code ermöglicht den Nachnahmeversand in alle Länder. Wenn 00 verwendet wird, muss dieses als letztes Argument eingetragen werden. Wenn kein 00:9.99 eingetragen ist, wird der Nachnahmeversand ins Ausland nicht berechnet (nicht möglich).<br />',
	'MODULE_ORDER_TOTAL_COD_FEE_RULES_TITLE' => 'Nachnahmegebühr je installierter Versandart',
	'MODULE_ORDER_TOTAL_COD_FEE_SORT_ORDER_DESC' => 'Anzeigereihenfolge',
	'MODULE_ORDER_TOTAL_COD_FEE_SORT_ORDER_TITLE' => 'Sortierreihenfolge',
	'MODULE_ORDER_TOTAL_COD_FEE_STATUS_DESC' => 'Berechnung der Nachnahmegebühr',
	'MODULE_ORDER_TOTAL_COD_FEE_STATUS_TITLE' => 'Nachnahmegebühr',
	'MODULE_ORDER_TOTAL_COD_FEE_TAX_CLASS_DESC' => 'Wählen Sie eine Steuerklasse.',
	'MODULE_ORDER_TOTAL_COD_FEE_TAX_CLASS_TITLE' => 'Steuerklasse',
	'MODULE_ORDER_TOTAL_COD_FEE_TITLE' => 'Nachnahmegebühr',
	'MODULE_ORDER_TOTAL_COD_FEE_TRANSFER_CHARGE_DESC' => '&lt;ISO2-Code&gt;:&lt;Preis&gt;, ...<br />00 als ISO2-Code ermöglicht den Nachnahmeversand in alle Länder. Wenn 00 verwendet wird, muss dieses als letztes Argument eingetragen werden. Es wird kein Hinweistext ausgegeben, falls für ein Land mit entsprechender Versandart kein Wert hinlegt worden ist.<br />',
	'MODULE_ORDER_TOTAL_COD_FEE_TRANSFER_CHARGE_INFORMATION' => '<b>Hinweis zu Nachnahmekosten:</b> Ein Übermittlungsentgelt in Höhe von%s wird zusätzlich zum Nachnahmebetrag vom Paketzusteller kassiert.</b>',
	'MODULE_ORDER_TOTAL_COD_FEE_TRANSFER_CHARGE_TITLE' => 'Übermittlungsentgelt je installierter Versandart'
);