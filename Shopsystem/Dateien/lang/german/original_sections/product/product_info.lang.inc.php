<?php
/* --------------------------------------------------------------
	product_info.lang.inc.php 2017-06-20
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'add_to_cart'               => 'In den Warenkorb',
	'text_abroad_shipping_info' => 'Ausland abweichend',
	'text_customizer_tab'       => 'Customize',
	'text_description'          => 'Beschreibung',
	'text_free_shipping'        => 'versandkostenfrei',
	'text_graduated_qty_1'      => 'Erlaubt ist nur eine Bestellmenge in ',
	'text_graduated_qty_2'      => 'er Schritten!',
	'text_min_order'            => 'Mindestbestellmenge: ',
	'text_model'                => 'Art.Nr.:',
	'text_pieces'               => 'Stück',
	'text_price_offer'          => 'Woanders günstiger?',
	'text_print'                => 'Datenblatt drucken',
	'text_selled'               => 'Bereits verkauft:',
	'text_shippingtime'         => 'Lieferzeit:',
	'text_status'               => 'Status:',
	'text_stock'                => 'Lagerbestand:',
	'text_tell_a_friend'        => 'Frage zum Produkt',
	'text_weight'               => 'Versandgewicht: ',
	'text_weight_qty_unit'      => 'Stück',
	'text_weight_unit'          => 'kg je ',
	'text_window_close'         => 'Schließen',
	'text_under_18'             => 'Dieses Produkt ist nicht verfügbar für Personen unter 18 Jahren!',
	'shipping_after_available'  => 'Lieferung erfolgt nach Erscheinen',
	'unknown_shippingtime'      => 'Unbekannte Lieferzeit',
);