<?php
/* --------------------------------------------------------------
	gm_security.lang.inc.php 2018-02-23
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'GM_CALLBACK_SERVICE_VVCODE' => 'Sicherheitscodeabfrage in "Callback Service"-Modul anzeigen?',
	'GM_CAPTCHA_TYPE' => 'Art der Sicherheitsabfrage',
	'GM_CAPTCHA_TYPE_RECAPTCHA_NAME' => 'reCAPTCHA',
    'GM_CAPTCHA_TYPE_RECAPTCHA_V2_NAME' => 'reCaptcha v2',
	'GM_CAPTCHA_TYPE_STANDARD_NAME' => 'Standard',
	'GM_CONTACT_VVCODE' => 'Sicherheitscodeabfrage in Kontaktformular anzeigen?',
	'GM_CREATE_ACCOUNT_VVCODE' => 'Sicherheitsabfrage in Account-Erstellung anzeigen?',
    'GM_CREATE_GUEST_ACCOUNT_VVCODE' => 'Sicherheitsabfrage in Gast-Account-Erstellung anzeigen?',
	'GM_FORGOT_PASSWORD_VVCODE' => 'Sicherheitscodeabfrage in "Passwort vergessen?" anzeigen?',
	'GM_GUESTBOOK_VVCODE' => 'Sicherheitscodeabfrage im Gästebuch anzeigen?',
	'GM_LOGIN_TIMELINE' => 'Zeitraum (Sek.)',
	'GM_PASSWORD_ENCRYPTION' => 'Passwortverschlüsselung',
	'GM_PASSWORD_ENCRYPTION_TYPE' => 'Standardverschlüsselung für Kundenpasswörter',
	'GM_PASSWORD_ENCRYPTION_TYPE_INFO' => 'Wählen Sie password_hash, um Passwörter maximal stark zu verschlüsseln (empfohlen). Wählen Sie MD5, sofern Sie externe Schnittstellen nutzen, die direkt über die Datenbank Passwörter abgleichen und keine password_hash-verschlüsselten Passwörter unterstützen.',
	'GM_PASSWORD_ENCRYPTION_TYPE_MD5' => 'md5',
	'GM_PASSWORD_ENCRYPTION_TYPE_PASSWORD_HASH' => 'password_hash',
	'GM_PASSWORD_REENCRYPT' => 'Kundenpasswörter beim nächsten Login nötigenfalls mit der aktuellen Standardverschlüsselung erneut verschlüsseln? (empfohlen)',
	'GM_PASSWORD_REENCRYPT_WARNING' => 'Aktivieren Sie diese Option, wenn Sie die Standardverschlüsselung wechseln und Kunden (auch Admin-Konten!) sich weiterhin mit Ihren bisher gespeicherten Passwörtern einloggen können sollen. Andernfalls ist der Login nicht mehr möglich und ein neues Passwort muss über die "Passwort vergessen"-Funktion gesetzt werden.',
	'GM_PRICE_OFFER_VVCODE' => 'Sicherheitscodeabfrage in "Woanders günstiger?"-Modul anzeigen?',
	'GM_NEWSLETTER_VVCODE' => 'Sicherheitscodeabfrage im Newsletterformular anzeigen?',
	'GM_RECAPTCHA_PRIVATE_KEY' => 'Private key',
	'GM_RECAPTCHA_PUBLIC_KEY' => 'Public key',
	'GM_REVIEWS_VVCODE' => 'Sicherheitscodeabfrage in "Bewertungen"-Modul anzeigen?',
	'GM_SEARCH_TIMELINE' => 'Zeitraum (Sek.)',
	'GM_SEARCH_TIMEOUT' => 'Timeout (Sek.)',
	'GM_SEARCH_TRYOUT' => 'Anzahl Suchen loggen',
	'GM_SECURITY_SUCCESS' => 'Die Änderungen wurden erfolgreich übernommen.',
	'GM_SEC_TIMEOUT' => 'Timeout (Sek.)',
	'GM_SEC_TRYOUT' => 'Versuche (min. 2)',
	'GM_TELL_A_FRIEND_VVCODE' => 'Sicherheitscodeabfrage in "Frage zum Produkt"-Modul anzeigen?',
	'GM_TITLE_LOGIN' => 'Login-Tracker',
	'GM_TITLE_SEARCH' => 'Such-Tracker',
	'GM_TITLE_VVCODE' => 'Sicherheitscodeabfragen',
	'HEADING_SUB_TITLE' => 'Gambio',
	'HEADING_TITLE' => 'Sicherheitscenter'
);