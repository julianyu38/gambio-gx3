<?php
/* --------------------------------------------------------------
	gm_sitemap.lang.inc.php 2017-09-19
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'GM_SITEMAP_CHANGEFREQ' => 'Standard-Änderungsfrequenz',
	'GM_SITEMAP_PRIORITY' => 'Standard-Priorität',
	'HEADING_TITLE' => 'Sitemap Generator',
	'MENU_TITLE_GM_SITEMAP' => 'Sitemap generieren',
	'MENU_TITLE_GM_SITEMAP_CONF' => 'Sitemap konfigurieren',
	'TITLE_ALWAYS' => 'Immer',
	'TITLE_CREATE_SITEMAP' => 'Sitemap generieren',
	'TITLE_DAILY' => 'Täglich',
	'TITLE_HOURLY' => 'Stündlich',
	'TITLE_MONTHLY' => 'Monatlich',
	'TITLE_NEVER' => 'Nie',
	'TITLE_PING_ASK' => 'Ask.com automatisch in Kenntnis setzen',
	'TITLE_PING_GOOGLE' => 'Google automatisch in Kenntnis setzen',
	'TITLE_PING_LIVE' => 'Live Search automatisch in Kenntnis setzen',
	'TITLE_PING_YAHOO' => 'Yahoo! automatisch in Kenntnis setzen',
	'TITLE_WEEKLY' => 'Wöchentlich',
	'TITLE_YEARLY' => 'Jährlich',
    'MENU_TITLE_GM_SITEMAP_PUBLISH_ALL' => 'Alle veröffentlichen',
    'PUBLISH_ALL' => 'Veröffentlichen',
    'PUBLISH_ALL_TEXT' => 'Alle Kategorien und Produkte in Sitemap veröffentlichen',
    'TITLE_GENERATE' => 'Klicken Sie Ausführen, um die Sitemap zu generieren',
);