<?php
/* --------------------------------------------------------------
	gm_product_images.lang.inc.php 2018-04-09
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'GM_CATEGORIES_IMAGE_ALT_TEXT'        => 'Alternativtext',
	'GM_CATEGORIES_IMAGE_NAME'                  => 'Neuer Dateiname',
	'GM_PRODUCTS_ALT_TEXT'                      => 'Alternativtext',
	'GM_PRODUCTS_FILENAME'                      => 'Dateiname',
	'GM_RENAME_IMAGE_INVALID_FILE_FORMAT'       => 'Der neue Dateiname "%s" besitzt ein ungültiges Dateiformat und wurde ignoriert.',
	'GM_TITLE_REDIRECT'                         => ' <strong> - Diese Datei war bereits vorhanden und musste daher umbenannt werden.</strong>',
	'GM_TITLE_REDIRECT_MULTI'                   => ' <strong> - Einige Dateien waren bereits vorhanden und mussten daher umbenannt werden.</strong>',
	'GM_UPLOAD_IMAGE_INVALID_FILE_FORMAT'       => 'Die Datei "%s" besitzt ein ungültiges Dateiformat und wurde dem Artikel nicht hinzugefügt.',
	'GM_UPLOAD_IMAGE_MODAL_ERROR'               => 'Fehler',
	'GM_UPLOAD_IMAGE_MODAL_INVALID_FILE_FORMAT' => 'Die ausgewählte Datei besitzt ein ungültiges Dateiformat.',
);