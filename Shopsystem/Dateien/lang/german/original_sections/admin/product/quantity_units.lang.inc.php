<?php
/* --------------------------------------------------------------
	quantity_units.lang.inc.php 2015-01-02 gm
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2015 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'HEADING_SUB_TITLE' => 'Gambio',
	'HEADING_TITLE' => 'Mengeneinheiten',
	'MENU_TITLE_QUANTITYUNITS' => 'Einheiten (eingerichtete)',
	'MENU_TITLE_QUANTITYUNIT_NAME' => 'Mengeneinheit',
	'MENU_TITLE_QUANTITYUNIT_NEW' => 'Einheit (neu)',
	'TEXT_BTN_DELETE' => 'löschen',
	'TEXT_BTN_LOAD' => 'laden',
	'TEXT_BTN_NEW' => 'anlegen',
	'TEXT_BTN_SAVE' => 'speichern',
	'TEXT_BTN_UPDATE' => 'speichern',
	'TEXT_DELETE' => 'löschen',
	'TEXT_LANG_ALL' => 'alle Sprachen',
	'TEXT_LANG_SHOP' => 'aktuelle Sprache',
	'TEXT_NEW_QUANTITYUNIT' => 'Neue Mengeneinheit',
	'TEXT_QUANTITYUNIT' => 'Mengeneinheit bearbeiten',
	'TEXT_QUANTITYUNIT_REMOVE' => 'Mengeneinheit löschen',
	'TEXT_QUANTITYUNIT_NAME' => 'Name Mengeneinheit (gewählt)',
	'ERROR_INVALID_INPUT_FIELDS' => 'Es muss mindestens ein Feld ausgefüllt werden',
	'TEXT_INFO_ADD_SUCCESS' => 'Mengeneinheit hinzugefügt',
	'TEXT_INFO_DELETE_SUCCESS' => 'Die Mengeneinheit wurde gelöscht',
	'TEXT_INFO_EDIT_SUCCESS' => 'Die Mengeneinheit ist bearbeitet worden',
	'TEXT_INFO_DELETE_INTRO' => 'Sind Sie sicher, dass Sie diese Mengeneinheit löschen möchten?',
	'TEXT_INFO_EDIT_INTRO' => 'Bitte führen Sie alle notwendigen Änderungen durch.',
	'TEXT_INFO_INSERT_INTRO' => 'Bitte geben Sie die neue Mengeneinheit mit allen relevanten Daten ein.',
);