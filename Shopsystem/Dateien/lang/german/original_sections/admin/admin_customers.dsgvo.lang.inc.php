<?php
/* --------------------------------------------------------------
	admin_customers.lang.inc.php 2017-01-19
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
    'delete_personal_data' => 'Personenbezogene Daten löschen',
    'export_personal_data' => 'Personenbezogene Daten exportieren',
    'select_personal_data_base_data' => 'Stammdaten inkl. Adressen',
    'select_personal_data_orders' => 'Bestellungen, Rechnungen und Lieferscheine',
    'select_personal_data_withdrawals' => 'Widerrufe',
    'select_personal_data_agreements' => 'Einwilligungen',
    'select_personal_data_emails' => 'E-Mails',
    'select_personal_data_carts' => 'Warenkörbe',
    'select_personal_data_reviews' => 'Bewertungen',
    'select_personal_data_newsletter_subscriptions' => 'Newsletter-Anmeldungen'
);
