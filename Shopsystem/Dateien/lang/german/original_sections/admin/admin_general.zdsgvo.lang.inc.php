<?php
/* --------------------------------------------------------------
	admin_general.zdsgvo.lang.inc.php 2018-05-15
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
    'ENTRY_ADMIN_LOG' => 'Adminaktivitäten protokollieren:',
);
