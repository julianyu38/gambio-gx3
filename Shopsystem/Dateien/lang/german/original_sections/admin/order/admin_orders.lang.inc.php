<?php
/* --------------------------------------------------------------
	admin_orders.lang.inc.php 2017-12-06
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'ADD_TRACKING_NUMBER_ERROR'         => 'Die Sendungsnummer konnte aufgrund eines unerwarteten Fehlers nicht hinzugefügt werden!',
	'ADD_TRACKING_NUMBER_SUCCESS'       => 'Die Sendungsnummer wurde erfolgreich gespeichert!',
	'CANCEL_ORDERS_ERROR'               => 'Die Bestellungen konnten aufgrund eines unerwarteten Fehlers nicht storniert werden!',
	'CANCEL_ORDERS_SUCCESS'             => 'Die Bestellungen wurden erfolgreich storniert!',
	'COLUMN'                            => 'Spalte',
	'COLUMNS'                           => 'Spalten',
	'COUNTRY'                           => 'Land',
	'CREATE_MISSING_DOCUMENTS'          => 'Fehlende Dokumente erstellen.',
	'CUSTOMER'                          => 'Kunde',
	'DATE'                              => 'Datum',
	'DELETE_ORDERS_ERROR'               => 'Die Bestellungen konnten aufgrund eines unerwarteten Fehlers nicht gelöscht werden!',
	'DELETE_ORDERS_SUCCESS'             => 'Die Bestellungen wurden erfolgreich gelöscht!',
	'DELETE_PDF_CONFIRM_MESSAGE'        => 'Möchten Sie diese PDF wirklich löschen?',
	'DISPLAY_TOOLTIPS_LABEL'            => 'Tooltips aktivieren',
	'DOWNLOAD_DOCUMENTS'                => 'Dokumente herunterladen',
	'GET_ORDERS_WITHOUT_DOCUMENT_ERROR' => 'Die Bestellungen ohne Dokumenten konnte aufgrund eines unerwarteten Fehlers nicht abgeholt werden!',
	'GROUP'                             => 'Gruppe',
	'NUMBER'                            => 'Nr.',
	'ORDER_WEIGHT_UNIT'                 => 'Kg',
	'ORDERS_WITHOUT_DOCUMENT'           => 'Folgende Bestellungen haben kein Dokument',
	'PAGE_TITLE'                        => 'Bestellungen',
	'PAYMENT'                           => 'Zahlung',
	'ROW'                               => 'Zeilen',
	'ROW_HEIGHT_LABEL'                  => 'Höhe',
	'ROW_HEIGHT_LARGE'                  => 'Groß',
	'ROW_HEIGHT_MEDIUM'                 => 'Mittel',
	'ROW_HEIGHT_SMALL'                  => 'Klein',
	'SETTINGS'                          => 'Einstellungen',
	'SHIPPING'                          => 'Versand',
	'STATUS'                            => 'Status',
	'SUM'                               => 'Summe',
	'TOTAL_WEIGHT'                      => 'Gewicht',
	'VOUCHER'                           => 'Gutschein',
];