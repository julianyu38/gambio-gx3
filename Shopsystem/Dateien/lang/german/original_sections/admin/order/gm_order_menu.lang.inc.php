<?php
/* --------------------------------------------------------------
	gm_order_menu.lang.inc.php 2018-02-16
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'CHANGE_ORDER_STATUS_SUBJECT'                => 'Ihre Bestellung ',
	'INVOICE_BULK_SUBJECT'                       => 'Ihre Rechnung der Nr. ',
	'INVOICE_SUBJECT'                            => 'Ihre Rechnung der Bestellung Nr. ',
	'INVOICE_SUBJECT_FROM'                       => ' vom ',
	'INVOICE_SUBJECT_WITH_ORDER_ID'              => ' mit der Bestellnummer ',
	'ORDER_SUBJECT'                              => 'Ihre Bestellbestätigung der Bestellung Nr. ',
	'ORDER_SUBJECT_FROM'                         => ' vom ',
	'TEXT_BULK_INVOICE_DATE_SUBJECT_INFO'        => '{INVOICE_DATE} oder {DATE} - Rechnungsdatum',
	'TEXT_BULK_INVOICE_NUM_SUBJECT_INFO'         => '{INVOICE_NUM} oder {INVOICE_ID} - Rechnungsnummer',
	'TEXT_BULK_ORDER_DATE_SUBJECT_INFO'          => '{ORDER_DATE} - Bestelldatum',
	'TEXT_BULK_ORDER_ID_SUBJECT_INFO'            => '{ORDER_ID} - ID der Bestellung',
	'TEXT_NO_INVOICE_INFO'                       => 'Für diese Bestellung existiert noch keine Rechnung. Beim Versenden wird eine Rechnung erstellt.',
	'TITLE_ALREADY_CANCELED'                     => 'Diese Bestellung wurde bereits storniert.',
	'TITLE_BREAK'                                => 'Beenden',
	'TITLE_BULK_INVOICE'                         => 'Sollen die Rechnungen versendet werden?',
	'TITLE_BULK_INVOICE_CREATE_MISSING_INVOICES' => 'Fehlende Rechnungen erzeugen',
	'TITLE_BULK_ORDER_CONFIRM'                   => 'Sollen die Bestellbestätigungen versendet werden?',
	'TITLE_GM_CANCEL'                            => 'Stornieren',
	'TITLE_GM_CANCEL_SUBJECT_1'                  => 'Ihre Bestellung Nr. ',
	'TITLE_GM_CANCEL_SUBJECT_2'                  => ' wurde am ',
	'TITLE_GM_CANCEL_SUBJECT_3'                  => ' storniert.',
	'TITLE_GM_NOTIFY'                            => 'Kunde benachrichtigen',
	'TITLE_GM_NOTIFY_COMMENTS'                   => 'Kommentare mitsenden',
	'TITLE_GM_REACTIVATEARTICLE'                 => 'Artikelstatus zurücksetzen',
	'TITLE_GM_RESHIPP'                           => 'Lieferstatus neu berechnen',
	'TITLE_GM_RESTOCK'                           => 'Artikelanzahl dem Lager gutschreiben',
	'TITLE_INVOICE'                              => 'Rechnung per E-Mail senden',
	'TITLE_NEW_INVOICE_CONFIRM'                  => 'Soll eine neue Rechnung an den Kunden ',
	'TITLE_INVOICE_CONFIRM'                      => 'Soll eine Rechnung an den Kunden ',
	'TITLE_INVOICE_CONFIRMED'                    => 'verschickt werden?',
	'TITLE_INVOICES'                             => 'Rechnungen',
	'TITLE_LANG'                                 => 'Sprache',
	'TITLE_MAIL'                                 => 'E-Mail',
	'TITLE_ORDER'                                => 'Bestellbestätigung per E-Mail senden',
	'TITLE_ORDER_CONFIRM'                        => 'Soll eine Bestellbestätigung an den Kunden ',
	'TITLE_ORDER_CONFIRMED'                      => 'verschickt werden?',
	'TITLE_SEND'                                 => 'Senden',
	'TITLE_SUBJECT'                              => 'Betreff',
	'TITLE_SUBJECT_INFO'                         => 'Verfügbare Platzhalter',
	'TITLE_DATE'                                 => 'Datum'
);