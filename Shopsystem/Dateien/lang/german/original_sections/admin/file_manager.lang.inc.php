<?php
/* --------------------------------------------------------------
	file_manager.lang.inc.php 2017-03-24
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'ACTIONS'              => 'Aktionen',
	'CONFIRM_DELETE_TITLE' => 'Datei löschen',
	'CONFIRM_DELETE_TEXT'  => 'Soll die Datei wirklich gelöscht werden?',
	'CREATE'               => 'Erstellen',
	'CREATE_NEW_FOLDER'    => 'Neuer Ordner',
	'DATE_MODIFIED'        => 'Änderungsdatum',
	'DOWNLOAD'             => 'Downloads',
	'DRAG_INFORMATION'     => 'Dateien hier hineinziehen oder',
	'EMPTY_DIRECTORY'      => 'Dieses Verzeichnis ist leer',
    'ERROR_NO_FILEMANAGER' => 'Der Dateimanager ist nicht installiert. Bitte installieren Sie den Filemanager, um seine Funktionalität nutzen zu können.',
	'FILENAME'             => 'Dateiname',
	'HEADING_TITLE'        => 'Dateimanager',
	'IMAGES'               => 'Sonstige Bilder',
	'INFO_ORIGINAL_IMAGES' => 'Bei einem Bild-Import müssen die Bilder in dieses Verzeichnis hochgeladen werden. Bilder in diesem Verzeichnis werden vom Image Processing verarbeitet.',
	'MEDIA'                => 'Medien',
	'PRODUCT_IMAGES'       => 'Artikelbilder',
	'SIZE'                 => 'Größe',
);