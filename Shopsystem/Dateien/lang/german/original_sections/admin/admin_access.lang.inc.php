<?php
/* --------------------------------------------------------------
	admin_access.lang.inc.php 2018-01-04
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'button_create_role'                      => 'Neue Zugriffsrolle',
	'error_ajax_request_failed'               => 'Es ist ein unerwarteter Fehler aufgetreten.',
	'error_invalid_id'                        => 'Aufruf erfolgte für eine ungültige ID.',
	'error_invalid_value'                     => 'Aufruf erfolgte für eine ungültigen Wert.',
	'error_modal_title'                       => 'Fehler',
	'heading_title'                           => 'Rollen und Berechtigungen',
	'headline_admins_overview_assigned_roles' => 'Zugewiesene Zugriffsrollen',
	'headline_admins_overview_name'           => 'Admin',
	'headline_edit_admin_assign_role'         => 'Rolle zuweisen',
	'headline_edit_admin_name'                => 'Zugriffsrolle',
	'headline_permissions_overview_deleting'  => 'Löschen',
	'headline_permissions_overview_name'      => 'Berechtigung',
	'headline_permissions_overview_reading'   => 'Zugriff erlauben',
	'headline_permissions_overview_writing'   => 'Schreiben',
	'headline_roles_overview_name'            => 'Zugriffsrollen',
	'overview_title_admins'                   => 'Admins',
	'overview_title_assigned_roles'           => 'Rollenzuweisung für ',
	'overview_title_granted_permissions'      => 'Berechtigungen für ',
	'overview_title_roles'                    => 'Zugriffsrollen',
	'role_modal_create_title'                 => 'Zugriffsrolle erstellen',
	'role_modal_delete_body'                  => 'Soll die folgende Rolle wirklich gelöscht werden?',
	'role_modal_delete_title'                 => 'Zugriffsrolle löschen',
	'role_modal_description'                  => 'Beschreibung',
	'role_modal_edit_title'                   => 'Zugriffsrolle bearbeiten',
	'role_modal_name'                         => 'Name',
	'role_modal_sort_order'                   => 'Sortierreihenfolge',
	'sub_navigation_admins'                   => 'Admins verwalten',
	'sub_navigation_permissions'              => 'Berechtigungen verwalten',
	'sub_navigation_role_assignment'          => 'Rollenzuweisung',
	'sub_navigation_roles'                    => 'Zugriffsrollen verwalten',
	'text_all_groups'                         => 'Alle Berechtigungen',
	'text_assigned_roles'                     => 'Zugriffsrollen',
	'text_empty_admins_overview_list'         => 'Es sind derzeit keine Admins vorhanden.',
	'text_empty_edit_admin_overview_list'     => 'Es sind derzeit keine Zugriffsrollen vorhanden. Sie müssen zunächst eine <a href="admin.php?do=AdminAccess/manageRoles">Zugriffsrolle anlegen</a> um diese einem Admin zuzuweisen.',
	'text_empty_overview_list'                => 'Es sind derzeit keine Einträge vorhanden.',
	'text_empty_permissions_overview_list'    => 'Es sind derzeit keine Berechtigungen vorhanden.',
	'text_empty_roles_overview_list'          => 'Es sind derzeit keine Zugriffsrollen vorhanden.',
	'text_main_admin'                         => 'Hauptadmin',
	'text_unknown_group_name'                 => 'Unbekannte Module',
	'text_saved_sorting'                      => 'Die neue Sortierung wurde erfolgreich gespeichert.',
	'tooltip_admins_overview'                 => 'Der Hauptadmin kann nicht verwaltet werden, da diesem immer alle Rechte gewährt werden.',
	'tooltip_all_groups'                      => 'Diese Berechtigung steht stellvertretend für alle Berechtigung.',
	'tooltip_unknown_group'                   => 'Diese Berechtigung fasst alle unbekannten Module zusammen. Es wird empfohlen, dass diese Berechtigung immer erlaubt wird.',
);