<?php
/* --------------------------------------------------------------
	robots_download.lang.inc.php 2017-05-11
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'HEADING_SUB_TITLE' => 'Robots Steuerdatei erstellen und speichern oder downloaden',
	'HEADING_TITLE' => 'Robots Datei',
	'ROBOTS_OBSOLETE' => 'Die robots.txt Datei ist veraltet! Generieren Sie hier eine aktuelle Version!',
	'ROBOTS_SUBMIT' => 'Generieren und downloaden',
	'ROBOTS_SAVE_SUBMIT' => 'Generieren und speichern',
	'TEXT_ROBOTS_DOWNLOAD' => 'Downloaden Sie die robots.txt Datei und laden Sie sie in das Hauptverzeichnis Ihres Webservers.',
	'TEXT_ROBOTS_SAVE' => 'Generieren Sie die robots.txt Datei. Sie wird im Hauptverzeichnis Ihres Webservers gespeichert.',
	'ROBOTS_SAVE_SUCCESS' => 'Die robots.text wurde erfolgreich erstellt und im Hauptverzeichnis gespeichert',
	'ROBOTS_SAVE_ERROR' => 'Die robots.text konnte nicht erstellt und im Hauptverzeichnis gespeichert werden. Bitte überprüfen Sie die Dateiberechtigungen.'
);