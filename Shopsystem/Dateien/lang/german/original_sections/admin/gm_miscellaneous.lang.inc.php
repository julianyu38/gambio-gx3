<?php
/* --------------------------------------------------------------
	gm_miscellaneous.lang.inc.php 2018-04-11
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'BUTTON_EXECUTE'                             => 'Anwenden',
	'BUTTON_SAVE'                                => 'Speichern',
	'GM_CAT_STOCK'                               => 'Lagerbestände in <strong>allen</strong> Kategorien zu jedem Artikel anzeigen (Haken gesetzt) / nicht anzeigen (kein Haken) lassen? Die Option ist unabhängig von der unteren.',
	'GM_CAT_STOCK_SUCCESS'                       => 'Die Anpassung der Kategorien war erfolgreich.',
	'GM_DELETE_IMAGES'                           => 'Alle Original-Artikelbilder unwiderruflich löschen?',
	'GM_DELETE_IMAGES_ADVICE_1'                  => ' Datei konnte nicht gelöscht werden, da die erforderlichen Lösch- oder Benutzerrechte fehlen.',
	'GM_DELETE_IMAGES_ADVICE_2'                  => ' Dateien konnten nicht gelöscht werden, da die erforderlichen Lösch- oder Benutzerrechte fehlen.',
	'GM_DELETE_IMAGES_MESSAGE_1'                 => 'Es wurden ',
	'GM_DELETE_IMAGES_MESSAGE_2'                 => ' von ',
	'GM_DELETE_IMAGES_MESSAGE_3'                 => ' Dateien aus dem images/product_images/original_images Verzeichnis gelöscht.',
	'GM_DELETE_IMAGES_TITLE'                     => 'Artikelbilder löschen',
	'GM_DELETE_UNUSED_IMAGES'                    => 'Alle nicht verwendeten Artikelbilder unwiderruflich löschen?',
	'GM_DELETE_UNUSED_IMAGES_MESSAGE_1'          => 'Es wurden ',
	'GM_DELETE_UNUSED_IMAGES_MESSAGE_2'          => ' von ',
	'GM_DELETE_UNUSED_IMAGES_MESSAGE_3'          => ' Dateien aus den images/product_images/... Verzeichnissen gelöscht.',
	'GM_MISCELLANEOUS_SUCCESS'                   => 'Einstellungen wurde aktualisiert.',
	'GM_ORDER_STATUS_CANCEL_ID'                  => 'ID in der MySQL-Tabelle "orders_status" für den Bestellstatus der Stornierung. Diese ID sollte nur geändert werden, wenn die neue ID entsprechend bekannt ist oder diese noch nicht gesetzt worden ist. Im Standard sollte hier der Wert "99" stehen.',
	'GM_PRODUCT_STOCK'                           => 'Lagerbestände in <strong>allen</strong> Artikeln in der Artikeldetailansicht anzeigen (Haken gesetzt) / nicht anzeigen (kein Haken)? Die Option ist unabhängig von der oberen.',
	'GM_PRODUCT_STOCK_SUCCESS'                   => 'Die Anpassung der Artikel war erfolgreich.',
	'GM_TAX_FREE'                                => 'Kleinunternehmerreglung: "Kein Steuerausweis gem. Kleinuntern.-Reg. §19 UStG" statt der MwSt.-Angabe bei jedem Preis anzeigen lassen',
	'GM_TELL_A_FRIEND'                           => '"Frage zum Produkt"-Modul aktivieren?',
	'GM_TITLE_STAT'                              => 'Bestellstatus',
	'GM_TITLE_STATS'                             => 'Statistiken löschen',
	'GM_TITLE_STOCK'                             => 'Lagerbestände anzeigen',
	'GM_TRUNCATE_FLYOVER'                        => 'Zeichenanzahl nach der die Artikelnamen im "Flyover" gekürzt werden.',
	'GM_TRUNCATE_FLYOVER_TEXT'                   => 'Zeichenanzahl nach der der Artikelkurztext im "Flyover" gekürzt wird.',
	'GM_TRUNCATE_PRODUCTS_HISTORY'               => 'Max. Zeichen für Artikelnamen in der Menübox "Bestellübersicht"',
	'GM_TRUNCATE_PRODUCTS_NAME'                  => 'Max. Zeichen für Artikelnamen auf der Startseite',
	'GRADUATED_ASSIGN'                           => 'Bei der Preisermittlung die Zusammenfassung von Artikelbeständen nicht unterbinden',
	'GRADUATED_ASSIGN_INFO'                      => 'Wenn diese Option deaktiviert wird, werden zum Beispiel gleiche GX Customizer Artikel im Warenkorb nicht mehr zusammengefasst und keine Staffelpreise für diese angewendet.',
	'HEADING_TITLE'                              => 'Allgemeines',
	'PRODUCT_REVIEW_NAME'                        => 'Art der Namensdarstellung bei zukünftigen Produktrezensionen',
	'PRODUCT_REVIEW_NAME_OPTION_SHORT_FIRSTNAME' => 'Vorname kürzen',
	'PRODUCT_REVIEW_NAME_OPTION_SHORT_LASTNAME'  => 'Nachname kürzen',
	'PRODUCT_REVIEW_NAME_OPTION_SHORT_NOTHING'   => 'Name nicht kürzen',
	'SHOW_OLD_DISCOUNT_PRICE_TEXT'               => 'Normalpreis bei rabattierten Preisen anzeigen',
	'SHOW_OLD_GROUP_PRICE_TEXT'                  => 'Normalpreis bei reduzierten Kundengruppen- und Staffelpreisen anzeigen',
	'SHOW_OLD_SPECIAL_PRICE_INFO'                => 'Mit der Aktivierung dieser Option, wird der hinterlegte Artikelpreis als unverbindliche Preisempfehlung (UVP) angezeigt. Bitte beachten Sie, dass es sich bei diesem Preis auch tatsächlich um eine gültige unverbindliche Preisempfehlung handeln muss. Handelt es sich nicht um eine unverbindliche Preisempfehlung, sondern z. B. um den ehemaligen Preis im Shop, muss die Bezeichnung "UVP" angepasst werden. Die Bezeichnung können Sie unter "Texte anpassen" ändern. Um rechtliche Probleme zu vermeiden, empfehlen wir Ihnen sich juristisch beraten zu lassen.',
	'SHOW_OLD_SPECIAL_PRICE_TEXT'                => 'UVP bei Sonderangebotspreisen anzeigen',
	'TITLE_STAT_EXTERN_KEWORDS'                  => 'Externe Suchwörter',
	'TITLE_STAT_IMPRESSIONS'                     => 'Seitenaufrufe',
	'TITLE_STAT_INTERN_KEWORDS'                  => 'Interne Suchwörter',
	'TITLE_STAT_PRODUCTS_PURCHASED'              => 'Verkaufte Artikel',
	'TITLE_STAT_PRODUCTS_VIEWED'                 => 'Besuchte Artikel',
	'TITLE_STAT_USER_INFO'                       => 'Benutzerinfo',
	'TITLE_STAT_VISTORS'                         => 'Besucher',
);