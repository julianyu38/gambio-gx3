<?php
/* --------------------------------------------------------------
	gm_id_starts.lang.inc.php 2016-10-17
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'GM_ID_STARTS_CUSTOMERS_ERROR' => 'Die Kundennummer ist kleiner als der Minimumwert oder ist keine Zahl.',
	'GM_ID_STARTS_NEXT_CUSTOMER_ID' => 'Nächste Kundennummer: ',
	'GM_ID_STARTS_NEXT_ORDER_ID' => 'Nächste Bestellnummer: ',
	'GM_ID_STARTS_NO_SUCCESS' => 'Einige Änderungen konnten nicht erfolgreich durchgeführt werden.',
	'GM_ID_STARTS_ORDERS_ERROR' => 'Die Bestellnummer ist kleiner als der Minimumwert oder ist keine Zahl.',
	'GM_ID_STARTS_SUCCESS' => 'Die Änderungen wurden erfolgreich durchgeführt.',
	'GM_ID_STARTS_TEXT' => 'Hier können Sie die Nummernkreise für Bestellungen und Kunden anpassen. Beachten Sie, dass nur numerische Werte erlaubt sind.',
	'GM_INVOICE_ID' => 'Format Rechnungsnummer: ',
	'GM_INVOICE_ID_PLACEMENT' => 'Platzhalter Rechnungsnummer {INVOICE_ID}',
	'GM_NEXT_FREE_ID_TEXT' => 'Nächste freie Nummer nach der bisher höchsten',
	'GM_NEXT_GAP_TEXT' => 'Nächste freie Lücke',
	'GM_NEXT_ID_TEXT' => 'Hier können Sie die Nummernkreise für Rechnungs- und Lieferscheinnummer anpassen. Beachten Sie, das diese beiden Nummern fortlaufend sind und jeweils erst bei der Erstellung bzw. Versand von Rechnung und Lieferschein generiert werden. Sollten Sie das Format nachträglich anpassen, so wird das neue Format auch erst in neu generierten Rechnungen und Lieferscheinen angewendet.',
	'GM_NEXT_INVOICE_ID' => 'Nächste Rechnungsnummer: ',
	'GM_NEXT_INVOICE_ID_ERROR' => 'Die Rechnungsnummer ist bereits vergeben oder ist keine Zahl.',
	'GM_NEXT_INVOICE_ID_SUCCESS' => 'Die Rechnungsnummer wurde übernommen.',
	'GM_NEXT_PACKING_ID_ERROR' => 'Die Lieferscheinnummer ist bereits vergeben oder ist keine Zahl.',
	'GM_NEXT_PACKING_ID_SUCCESS' => 'Die Lieferscheinnummer wurde übernommen.',
	'GM_NEXT_PACKINGS_ID' => 'Nächste Lieferscheinnummer: ',
	'GM_PACKING_ID_PLACEMENT' => 'Platzhalter Lieferscheinnummer {DELIVERY_ID}',
	'GM_PACKINGS_ID' => 'Format Lieferscheinnummer: ',
	'GM_TITLE_ID' => 'Bestell- und Kundennummer',
	'GM_TITLE_NEXT_ID' => 'Rechnungs- und Lieferscheinnummer',
	'HEADING_SUB_TITLE' => 'Gambio',
	'HEADING_TITLE' => 'Nummernkreise'
);