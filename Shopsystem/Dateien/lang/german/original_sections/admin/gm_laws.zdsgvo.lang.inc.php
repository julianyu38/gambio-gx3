<?php
/* --------------------------------------------------------------
	gm_laws.zdsgvo.lang.inc.php 2018-05-16
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'BUTTON_EXPORT'                     => 'Exportieren',
	'TITLE_EXPORT_AGREEMENTS'           => 'Einwilligungsprotokoll exportieren',
	'TEXT_EXPORT_AGREEMENTS'            => 'Erstellt eine CSV-Datei, die alle Kunden beinhaltet, die den Rechtstexten zugestimmt haben',
	'TITLE_PRIVACY_WITHDRAWAL_WEB_FORM' => 'im Widerrufs-Webformular',
	'TITLE_PRIVACY_GV_SEND'             => 'im Guthaben versenden Formular',
	'TEXT_LOG_IP_SHIPPING'              => 'bei Bestätigung der Datenweitergabe an Transportunternehmen',
	'TITLE_PRIVACY_CHECKOUT_SHIPPING'   => 'im Bestellvorgang unter "Versandadresse bearbeiten"',
	'TITLE_PRIVACY_CHECKOUT_PAYMENT'    => 'im Bestellvorgang unter "Rechnungsadresse bearbeiten"',
);