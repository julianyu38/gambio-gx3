<?php
/* --------------------------------------------------------------
   sliders.lang.inc.php 2016-11-22
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                             => 'Teaser-Slider',
	'NAME'                                   => 'Name',
	'AMOUNT_OF_SLIDES'                       => 'Anzahl der Slides',
	'IS_ON_STARTPAGE'                        => 'Startseite',
	'SPEED'                                  => 'Geschwindigkeit',
	'MASTER_DATA'                            => 'Teaser-Slider-Stammdaten',
	'SLIDES'                                 => 'Slides',
	'THUMBNAIL'                              => 'Vorschaubild',
	'BREAKPOINT_XS'                          => 'Smartphone',
	'BREAKPOINT_SM'                          => 'Tablet Portrait',
	'BREAKPOINT_MD'                          => 'Tablet Landscape',
	'BREAKPOINT_LG'                          => 'Desktop',
	'TITLE'                                  => 'Titel',
	'ALT_TEXT'                               => 'Alt-Text',
	'LINK'                                   => 'Link',
	'LINK_TARGET_BLANK'                      => 'In neuem Fenster öffnen',
	'LINK_TARGET_SELF'                       => 'Im selben Fenster öffnen',
	'DELETE_SLIDE_MODAL_TITLE'               => 'Slide löschen',
	'DELETE_SLIDER_MODAL_TITLE'              => 'Slider löschen',
	'DELETE_SLIDE_MODAL_TEXT'                => 'Möchten Sie den Slide wirklich löschen?',
	'DELETE_SLIDER_MODAL_TEXT'               => 'Möchten Sie den Slider wirklich löschen?',
	'DELETE_SLIDER_ERROR_TITLE'              => 'Slider konnte nicht gelöscht werden',
	'DELETE_SLIDER_ERROR_TEXT'               => 'Der gewählte Slider konnte leider nicht gelöscht werden',
	'IMAGE_MAP_MODAL_TITLE'                  => 'Linkbereiche bearbeiten',
	'SAVE_SLIDER_FIRST_NOTICE_TEXT'          => 'Bitte speichern Sie zuerst den Slider, um die Linkbereiche zu bearbeiten',
	'SLIDE_SORT_BUTTON_TEXT'                 => 'Slides sortieren',
	'SLIDE_SORT_BUTTON_EXIT_TEXT'            => 'Sortieren beenden',
	'SLIDE_CREATE_BUTTON_TEXT'               => 'Slide hinzufügen',
	'SLIDER_START_PAGE_ERROR_TITLE'          => 'Slider konnte nicht gesetzt werden',
	'SLIDER_START_PAGE_ERROR_TEXT'           => 'Der Slider konnte nicht als Startseiten-Slider festgelegt werden',
	'DELETE_IMAGE_MODAL_TITLE'               => 'Bild löschen',
	'DELETE_IMAGE_MODAL_TEXT'                => 'Dieses Bild wird bereits woanders verwendet. Wenn Sie dieses Bild löschen, wird es an allen betroffenen Stellen gelöscht. Fortfahren?',
	'INVALID_FILE_MODAL_TITLE'               => 'Ungültige Datei',
	'INVALID_FILE_MODAL_TEXT'                => 'Die von Ihnen ausgewählte Datei ist keine gültige Bilddatei. Bitte geben Sie eine andere Datei an.',
	'FILENAME_ALREADY_USED_MODAL_TITLE'      => 'Dateiname ist bereits vorhanden',
	'FILENAME_ALREADY_USED_MODAL_TEXT'       => 'Das von Ihnen gewählte Bild steht bereits in der Auswahl zur Verfügung. Bitte geben Sie eine andere Datei an oder benennen Sie die Datei um.',
	'PLACEHOLDER_TITLE'                      => 'Slide-Titel',
	'PLACEHOLDER_ALT_TEXT'                   => 'Alternativer Text',
	'PLACEHOLDER_LINK'                       => 'http://mein-shop.de',
	'FORM_SUBMIT_ERROR_MODAL_TITLE'          => 'Fehler beim Speichern des Sliders',
	'FORM_SUBMIT_ERROR_MODAL_TEXT'           => 'Slider konnte nicht gespeichert werden. Bitte überprüfen Sie die Angaben und versuchen Sie es erneut',
	'NEW_SLIDE'                              => 'Neuer Slide',
	'EXIT_CONFIRMATION_TEXT'                 => 'Möchten Sie sicher die Seite verlassen? Die Änderungen am Slider gehen verloren',
	'NO_ENTRIES'                             => 'Keine Einträge vorhanden',
	'DO_NOT_USE'                             => 'Nicht verwenden',
	'LINK_AREA'                              => 'Linkbereich',
	'LINK_TITLE'                             => 'Linkbereich-Titel',
	'LINK_URL'                               => 'Linkbereich-URL',
	'LINK_TARGET'                            => 'Linkbereich-Ziel',
	'CREATE_LINK_AREA'                       => 'Neuen Linkbereich erstellen',
	'DRAW_IMAGE_AREA'                        => 'Legen Sie hier den verlinkten Bereich fest',
	'PLEASE_SELECT'                          => 'Bitte wählen',
	'NEW_LINKED_AREA'                        => 'Neuer verlinkter Bereich',
	'IMAGE_MAP_USAGE_INFO_LEFT_CLICK'        => 'Benutzen Sie die linke Maustaste um ein Pfad zu zeichnen oder um Ecken hinzuzufügen oder zu verschieben.',
	'IMAGE_MAP_USAGE_INFO_RIGHT_CLICK'       => 'Mit der rechten Maustaste können Ecken entfernt werden.',
	'RESET_PATH'                             => 'Pfad entfernen',
	'MISSING_PATH_OR_LINK_TITLE_MODAL_TITLE' => 'Fehlender Pfad oder Linkbereich-Titel',
	'MISSING_PATH_OR_LINK_TITLE_MODAL_TEXT'  => 'Bitte zeichnen Sie zuerst ein Pfad und geben Sie ein Linkbereich-Titel ein, bevor Sie den Linkbereich übernehmen.',
    'IMAGE_MAP_URL_INFO'                     => 'Für externe Links ist es erforderlich die URL mit http:// bzw. https:// anzugeben.'
];