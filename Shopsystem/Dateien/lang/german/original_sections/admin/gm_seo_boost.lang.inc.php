<?php
/* --------------------------------------------------------------
	gm_seo_boost.lang.inc.php 2017-03-15
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'GM_BOX_TITLE' => 'Suchmaschinenfreundliche URLs Pro',
	'GM_FORM_REPAIR' => 'URLs reparieren',
	'GM_FORM_REPAIR_SUCCESS' => 'Korrektur abgeschlossen!',
	'GM_FORM_SUBMIT' => 'Speichern',
	'GM_SEO_BOOST_TEXT' => 'Um den SEO Boost nutzen zu können, muss Ihr Server mod_rewrite unterstützen.',
	'GM_TEXT_CATEGORIES' => 'Suchmaschinenfreundliche URLs Pro für Kategorieseiten aktivieren',
	'GM_TEXT_CONTENT' => 'Suchmaschinenfreundliche URLs Pro für Content-Manager-Seiten aktivieren',
	'GM_TEXT_PRODUCTS' => 'Suchmaschinenfreundliche URLs Pro für Artikeldetailseiten aktivieren',
	'GM_TEXT_SHORT_URLS' => 'Kurze Produkt-URLs',
	'HEADING_SUB_TITLE' => 'Gambio',
	'HEADING_TITLE' => 'SEO Boost',
	'TEXT_SUPPRESS_INDEX_IN_URL' => 'index.php Suffix in zugehörigen URLs entfernen',
	'USE_SEO_BOOST_LANGUAGE_CODE' => 'Sprachcode in URLs integrieren'
);
