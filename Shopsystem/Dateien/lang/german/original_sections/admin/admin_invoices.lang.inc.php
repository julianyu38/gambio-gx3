<?php
/* --------------------------------------------------------------
	admin_invoices.lang.inc.php 2016-10-04
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                             => 'Rechnungen',
	'BULK_DOWNLOAD_INVOICES'                 => 'Rechnungen (Sammel-PDF)',
	'CANCELLATION_INVOICE'                   => 'Stornorechnung',
	'COLUMN'                                 => 'Spalte',
	'COLUMNS'                                => 'Spalten',
	'COUNTRY'                                => 'Land',
	'DELETE_INVOICES_ERROR'                  => 'Die Rechnungen konnten aufgrund eines unerwarteten Fehlers nicht gelöscht werden!',
	'DELETE_INVOICES_SUCCESS'                => 'Die Rechnungen wurden erfolgreich gelöscht!',
	'GROUP'                                  => 'Gruppe',
	'INVOICE_NUMBER'                         => 'RE-Nr.',
	'INVOICE_DATE'                           => 'RE-Datum',
	'ORDER_DATE'                             => 'Bestelldatum',
	'ORDER_NUMBER'                           => 'Bestellnr.',
	'PAYMENT'                                => 'Zahlung',
	'CUSTOMER'                               => 'Kunde',
	'ROW'                                    => 'Zeilen',
	'ROW_HEIGHT_LABEL'                       => 'Höhe',
	'ROW_HEIGHT_SMALL'                       => 'Klein',
	'ROW_HEIGHT_MEDIUM'                      => 'Mittel',
	'ROW_HEIGHT_LARGE'                       => 'Groß',
	'STATUS'                                 => 'Status',
	'SETTINGS'                               => 'Einstellungen',
	'SUM'                                    => 'Summe',
	'TEXT_INFO_HEADING_MULTI_DELETE_INVOICE' => 'Rechnungen löschen',
	'TEXT_INFO_MULTI_DELETE_INTRO'           => 'Sind Sie sicher, das Sie diese Rechnungen löschen möchten?',
	'TEXT_SELECTED_INVOICES'                 => 'Ausgewählte Rechnungen',
	'TEXT_BULK_PDF_ERROR'                    => 'Es sind maximal %d Rechnungen für das Sammel-PDF erlaubt. Bitte wählen Sie weniger
				Rechnungen aus oder passen Sie die <a href="%s">Einstellung</a> an.',
	'HEADING_BULK_PDF_ERROR'                 => 'Zu viele Rechnungen ausgewählt',
];