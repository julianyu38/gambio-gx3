<?php
/* --------------------------------------------------------------
	gm_sitemap_creator.lang.inc.php 2017-05-04
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'TITLE_SITEMAP_ANSWER' => 'Antwort von ',
	'TITLE_SITEMAP_ANSWER2' => ' lesen',
	'TITLE_SITEMAP_CREATED' => '<span class="gm_strong">Die Sitemap wurde erstellt.</span><br /><br />',
	'TITLE_SITEMAP_CREATED2' => 'Es wurden %s Links generiert. Sie können die Sitemap unter folgendem Link aufrufen: %s',
	'TITLE_SITEMAP_FAILED' => '<strong>Dieser Dienst konnte nicht automatisch von der Aktualisierung Ihrer Sitemap in Kenntnis gesetzt werden, da weder die Funktion "fsockopen" noch "curl" auf diesem Server zur Verfügung stehen.',
	'TITLE_SITEMAP_PING' => 'Google hat Ihnen folgende Nachricht nach Übermittelung der Sitemap zukommen lassen: ',
	'TITLE_SITEMAP_PUBLISH_ALL_SUCCESS' => 'Alle Kategorien und Produkte wurden in der Sitemap veröffentlicht.'
);