<?php
/* --------------------------------------------------------------
   static_seo_urls.lang.inc.php 2017-05-29
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                                => 'Statische Seiten',
	'CONTENT_MANAGER'                           => 'Content Manager',
	'PRODUCTS_CONTENT'                          => 'Artikel Content',
	'NAME'                                      => 'Name',
	'MASTER_DATA'                               => 'Stammdaten',
	'SEO_DATA'                                  => 'SEO Daten',
	'SITEMAP_ENTRY_OVERVIEW'                    => 'Sitemap',
	'SITEMAP_ENTRY'                             => 'In die Sitemap aufnehmen',
	'CHANGEFREQ'                                => 'Änderungsfrequenz in der Sitemap',
	'PRIORITY'                                  => 'Priorität in der Sitemap',
	'ROBOTS_ENTRY_OVERVIEW'                     => 'Robots-Disallow',
	'ROBOTS_ENTRY'                              => 'Eintrag in robots.txt (disallow)',
	'STATIC_SEO_URL_CONTENT_CREATE_BUTTON_TEXT' => 'Inhalt hinzufügen',
	'TITLE'                                     => 'Meta Title',
	'DESCRIPTION'                               => 'Meta Description',
	'KEYWORDS'                                  => 'Meta Keywords',
	'NO_ENTRIES'                                => 'Keine Einträge',
	'DELETE_STATIC_SEO_URL_MODAL_TITLE'         => 'Statische Seite löschen',
	'DELETE_STATIC_SEO_URL_MODAL_TEXT'          => 'Sind Sie sicher, dass Sie den Eintrag für die Statische Seite löschen wollen?',
	'DELETE_STATIC_SEO_URL_ERROR_TITLE'         => 'Die statische Seite konnte nicht gelöscht werden',
	'DELETE_STATIC_SEO_URL_ERROR_TEXT'          => 'Die gewählte statische Seite konnte leider nicht gelöscht werden.',
	'FORM_SUBMIT_ERROR_MODAL_TITLE'             => 'Fehler beim Speichern der statischen Seite',
	'FORM_SUBMIT_ERROR_MODAL_TEXT'              => 'Die statische Seite konnte nicht gespeichert werden. Bitte überprüfen Sie die Angaben und versuchen Sie es erneut.',
	'STATIC_SEO_URL_CHECKBOX_ERROR_TITLE'       => 'Es ist ein Fehler aufgetreten',
	'STATIC_SEO_URL_CHECKBOX_ERROR_TEXT'        => 'Bei der Status-Änderung der statischen Seite ist ein Fehler aufgetreten.',
	'CHANGEFREQ_ALWAYS'                         => 'immer',
	'CHANGEFREQ_HOURLY'                         => 'stündlich',
	'CHANGEFREQ_DAILY'                          => 'täglich',
	'CHANGEFREQ_WEEKLY'                         => 'wöchentlich',
	'CHANGEFREQ_MONTHLY'                        => 'monatlich',
	'CHANGEFREQ_YEARLY'                         => 'jährlich',
	'CHANGEFREQ_NEVER'                          => 'niemals',
];