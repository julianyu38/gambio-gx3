<?php
/* --------------------------------------------------------------
	paypal3installments_module.lang.inc.php 2016-09-30
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2015 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_ALLOWED_DESC'     => 'Geben Sie <b>einzeln</b> die Zonen an, welche f&uuml;r dieses Modul erlaubt sein sollen. (z.B. AT,DE (wenn leer, werden alle Zonen erlaubt))',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_ALLOWED_TITLE'    => 'Erlaubte Zonen',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_SORT_ORDER_DESC'  => 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_SORT_ORDER_TITLE' => 'Anzeigereihenfolge',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_STATUS_DESC'      => 'M&ouml;chten Sie Zahlungen auf Raten via PayPal akzeptieren?',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_STATUS_TITLE'     => 'PayPal-Ratenzahlungsmodul aktivieren',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_DESCRIPTION' => 'Einfach, sicher und schnell bezahlen – auf Raten',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_INFO'        => 'Info: Bezahlung auf Raten mit PayPal',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_TITLE'       => 'Ratenzahlung Powered by PayPal',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_TITLE_ADMIN' => 'Ratenzahlung Powered by PayPal',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_ZONE_DESC'        => 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_ZONE_TITLE'       => 'Zahlungszone'
);
