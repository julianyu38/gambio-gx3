<?php

/* --------------------------------------------------------------
	responsivefilemanager.lang.inc.php 2017-09-29
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'module_title'                             => 'Dateimanager',
	'module_description'                       => 'Einbindung des responsiven Dateimanagers',
	'config_use_in_ckeditor'                   => 'Dateimanager in CKEditor einbinden',
	'config_use_in_product_and_category_pages' => 'Dateimanager in Kategorien- und Artikelseiten einbinden',
	'config_use_in_manufacturer_pages'         => 'Dateimanager in Hersteller-Seiten einbinden',
	'config_use_in_content_manager_pages'      => 'Dateimanager in Content-Manager-Seiten einbinden',
	'config_use_in_banner_manager_pages'       => 'Dateimanager in Banner-Manager-Seiten einbinden',
	'config_use_in_attribute_pages'            => 'Dateimanager in Artikelattribute-Seiten einbinden',
	'config_use_in_shipping_status_pages'      => 'Dateimanager in Lieferstatus-Seiten einbinden',
	'config_use_in_email_pages'                => 'Dateimanager in Emails-Seite einbinden',
	'button_submit'                            => 'Speichern'
];