<?php
/* --------------------------------------------------------------
	parcelshopfinder.lang.inc.php 2017-07-14
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'ab_additional_info'                 => 'Adresszusatz',
	'ab_city'                            => 'Stadt',
	'ab_country'                         => 'Land',
	'ab_firstname'                       => 'Vorname',
	'ab_house_number'                    => 'Hausnummer',
	'ab_lastname'                        => 'Nachname',
	'ab_postcode'                        => 'Postleitzahl',
	'ab_postnumber'                      => 'Postnummer',
	'ab_street_address'                  => 'Straße',
	'back_to_search'                     => 'Neue Suche',
	'button_find_parcelshops'            => 'Suche Packstation/Filiale',
	'button_to_psf'                      => 'DHL',
	'cfg_api_key'                        => 'API-Schlüssel',
	'cfg_label_note_maps_keys'           => 'Maps-Zugangsdaten',
	'cfg_map_type'                       => 'Kartentyp',
	'cfg_map_type_dynamic'               => 'dynamisch',
	'cfg_map_type_none'                  => 'keine',
	'cfg_map_type_static'                => 'statisch',
	'cfg_maximum_list_entries'           => 'Anzahl Einträge (max.)',
	'cfg_note_maps_keys'                 => 'Wie Sie die nötigen Zugangsdaten für die Google-Maps-Schnittstelle bekommen, erfahren Sie hier:',
	'cfg_note_maps_keys_link'            => 'Google Maps API-Schlüssel',
	'cfg_save'                           => 'speichern',
	'cfg_url_signature_secret'           => 'URL-Signatur-Secret',
	'configuration_heading'              => 'DHL Paketshops Konfiguration',
	'configuration_map_api'              => 'Zugangsdaten für Kartendarstellung per Google Maps (optional)',
	'configuration_resultlist'           => 'Ergebnisliste',
	'distance'                           => 'Entfernung',
	'error_invalid_postnumber'           => 'Die eingegebene Postnummer ist nicht gültig.',
	'filter_both'                        => 'Filialen und Packstationen',
	'filter_offices'                     => 'Filialen',
	'filter_packstations'                => 'Packstationen',
	'heading_csa'                        => 'Versand an DHL Packstation, Postfiliale oder Paketshop',
	'heading_parcelshopfinder'           => 'DHL Packstationen und Filialen',
	'make_new_ab_entry'	                 => 'übernehmen',
	'module_description'                 => 'Ermöglicht Kunden das einfache und schnelle Auffinden der nächstgelegenen Paketshops, Packstationen und Postfilialen. <br /><br /> Nur für Honeygrid.',
	'module_title'                       => 'DHL Paketshops',
    'near'                               => 'in der Nähe von',
	'new_ab_entry'                       => 'Ihre Daten',
	'new_search'                         => 'Suche anpassen',
	'no_result'                          => 'Ihre Suche lieferte leider kein Ergebnis.',
	'note_for_button_to_psf'             => 'Unter "Adresse ändern" haben Sie zusätzlich auch die Möglichkeit DHL Packstationen und Filialen auszuwählen',
	'open_today'                         => 'Heute geöffnet:',
	'opening_hours'                      => 'Öffnungszeiten',
	'openinghours_and'                   => ' u. ',
	'openinghours_day_fr'                => 'Fr',
	'openinghours_day_mo'                => 'Mo',
	'openinghours_day_sa'                => 'Sa',
	'openinghours_day_su'                => 'So',
	'openinghours_day_th'                => 'Do',
	'openinghours_day_tu'                => 'Di',
	'openinghours_day_we'                => 'Mi',
	'parcel_shop_description'            => 'Zustellort',
	'parcelshopfinder_breadcrumb'        => 'DHL Packstationen und Filialen finden',
	'parcelshopfinder_result_breadcrumb' => 'DHL Packstationen und Filialen in Ihrer Nähe',
	'prepare_ab_entry'                   => 'auswählen',
	'psf_name'                           => 'Lieferung an',
	'search_for'                         => 'Suche nach',
	'your_search_address'                => 'Ihre Suche',
);
