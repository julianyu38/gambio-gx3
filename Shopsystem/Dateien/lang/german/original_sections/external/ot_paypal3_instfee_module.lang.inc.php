<?php
/* --------------------------------------------------------------
	ot_ppinstfee_module.lang.inc.php 2016-10-11
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_DESCRIPTION'              => 'Summe der Bestellung mit Finanzierungkosten (Ratenzahlung Powered by PayPal)',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_SORT_ORDER_DESC'          => 'Anzeigereihenfolge.',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_SORT_ORDER_TITLE'         => 'Sortierreihenfolge',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_STATUS_DESC'              => 'Möchten Sie die Gesamtbestellsumme mit Finanzierungskosten anzeigen?',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_STATUS_TITLE'             => 'Summe mit Finanzierungkosten anzeigen',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_FEE_TITLE'                => 'Finanzierungkosten',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_TITLE'                    => 'Gesamtbetrag (mit Finanzierungkosten)',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_TITLE_ADMIN'              => 'Gesamtbetrag (für Ratenzahlung Powered by PayPal)',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_FEE_PAYMENT_METHOD_TITLE' => 'Ratenzahlung Powered by PayPal',
);
