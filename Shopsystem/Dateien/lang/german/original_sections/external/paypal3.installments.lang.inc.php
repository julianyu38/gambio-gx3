<?php
/* --------------------------------------------------------------
	paypal3.installments.lang.inc.php 2017-01-25
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'amount_too_high'                => 'Für Beträge über 5000 EUR können wir Ratenzahlung Powered by PayPal leider nicht anbieten.',
	'by_amount_of'                   => 'in Höhe von je',
	'effective_apr'                  => 'effektiver Jahreszins',
	'error_retrieving_installments_info' => 'Beim Abruf der Informationen für die Ratenzahlung Powered by PayPal ist ein Fehler aufgetreten, bitte versuchen Sie es zu einem späteren Zeitpunkt erneut.',
	'financing_from'                 => 'Finanzierung ab',
	'generic_installments'           => 'Ratenzahlung',
	'generic_intro'                  => 'Zahlen Sie bei uns bequem und einfach mit',
	'generic_intro_checkout_payment' => 'Sie können diesen Einkauf auch finanzieren!',
	'generic_intro_product'          => 'Sie können diesen Artikel auch finanzieren!',
	'generic_intro_shopping_cart'    => 'Sie können diesen Einkauf auch finanzieren!',
	'in'                             => 'in',
	'info_on_financing_options'      => 'Informationen zu möglichen Raten',
	'info_representative_example'    => 'Zugleich repräsentatives Beispiel gem. § 6a PAngV',
	'installments'                   => 'Raten',
	'installments_of'                => 'in Höhe von je',
	'intro_non_qualifying'           => 'Mögliche Optionen für weitere Darlehensbeträge',
	'lender'                         => 'Darlehensgeber',
	'min_amount'                     => 'Darlehensbetrag mind.',
	'monthly'                        => 'monatliche',
	'monthly_payments_dative'        => 'monatlichen Raten',
	'monthly_payments_nominative'    => 'monatliche Raten',
	'net_loan_amount'                => 'Nettodarlehensbetrag',
	'nominal_rate'                   => 'fester Sollzinssatz',
	'out_of_bounds'                  => 'Finanzierung verfügbar ab 99 EUR bis 5000 EUR Warenkorbwert',
	'pay_by_installments'            => 'Ratenzahlung',
	'pay_by_monthly_installments'    => 'Zahlen Sie bequem und einfach in monatlichen Raten',
	'paypal_installments_box_title'  => 'Ratenzahlung Powered by PayPal',
	'per_month'                      => 'pro Monat',
	'plan'                           => 'Plan',
	'popup_infotext'                 => 'Ihre Ratenzahlung und den passenden Finanzierungsplan können Sie im Rahmen des Bestellprozesses auswählen. Ihr Antrag erfolgt komplett online und ist in wenigen Schritten hier im Shop abgeschlossen.',
	'powered_by'                     => 'Powered by',
	'representative_example'         => 'Repräsentatives Beispiel gem. § 6a PAngV',
	'sorry_no_qualifying_options'    => 'Für den obigen Darlehensbetrag steht Ratenzahlung leider nicht zur Verfügung.',
	'total_cost'                     => 'zu zahlender Gesamtbetrag',
	'total_interest'                 => 'Zinsbetrag',
	'with_installments_by_paypal'    => 'mit Ratenzahlung Powered by PayPal',
);
