<?php
/* --------------------------------------------------------------
	gv_send.lang.inc.php 2017-06-20 gm
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'current_balance' => 'Ihr aktuelles Guthaben beträgt: ',
	'entry_amount' => 'Gutscheinwert',
	'entry_email' => 'Empfänger E-Mail',
	'entry_message' => 'Ihre Nachricht',
	'entry_name' => 'Empfänger',
	'error_entry_amount_check' => '<span class="errorText">Ungültiger Wert</span>',
	'error_entry_amount_check_NAN' => '<span class="errorText">Ungültiger Wert! Eingabe ist keine Zahl.</span>',
	'error_entry_amount_check_OOR' => '<span class="errorText">Ungültiger Wert! Eingabe muss größer 0 und kleiner gleich dem Guthaben sein.</span>',
	'error_entry_email_address_check' => '<span class="errorText">Ungültige E-Mail-Adresse</span>',
	'heading_gvsend' => 'Gutschein versenden',
	'heading_text' => '<br />Bitte geben Sie hier Ihre persönlichen Angaben zum Gutschein an.<br />',
	'text_required' => '* notwendige Angaben',
	'text_required_sign' => ' *',
	'text_success' => 'Glückwunsch, Ihr Gutschein wurde versandt!'
);