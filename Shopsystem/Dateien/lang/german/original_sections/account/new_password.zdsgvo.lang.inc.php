<?php
/* --------------------------------------------------------------
	new_password.dsgvo.inc.php 2018-05-09
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'label_confirm_password' => 'Passwort bestätigen',
	'label_new_password'     => 'Neues Passwort',
	'text_step3'             => 'Schritt 3:',
	'text_step3_info'        => 'Geben Sie bitte ein neues Passwort ein.',
);