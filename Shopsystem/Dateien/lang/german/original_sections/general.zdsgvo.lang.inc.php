<?php
/* --------------------------------------------------------------
	general.dsgvo.inc.php 2018-05-09
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'TEXT_EMAIL_PASSWORD_NEW_PASSWORD' => 'Ihr Passwort wurde geändert',
	'TEXT_LINK_MAIL_SENDED'            => 'Ihre Anfrage zum Setzen eines neuen Passworts muss von Ihnen erst bestätigt werden.<br />Deshalb wurde Ihnen vom System, sofern ein Kundenkonto für die angegebene E-Mail Adresse existiert, eine E-Mail mit einem Bestätigungslink geschickt. Bitte klicken Sie nach dem Erhalt der E-Mail auf den Link, um ein neues Passwort zu setzen. Andernfalls kann kein neues Passwort eingerichtet werden!',
	'TEXT_PASSWORD_SAVED'              => 'Das neue Passwort wurde erfolgreich gespeichert.',
);