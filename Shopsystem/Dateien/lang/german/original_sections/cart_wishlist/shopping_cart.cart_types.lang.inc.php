<?php
/* --------------------------------------------------------------
	shopping_cart.cart_types.lang.inc.php 2016-11-07
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/


$t_language_text_section_content_array = array
(
	'cart_content_mixed'    => 'Ihr Warenkorb enthält immaterielle Güter (wie z.B. Gutscheine, Downloadartikel, etc.), die Ihre Zahlungsartenauswahl im Folgenden einschränken werden (z.B. keine Finanzierungsmöglichkeit).',
	'cart_content_physical' => '',
	'cart_content_virtual'  => 'Ihr Warenkorb enthält immaterielle Güter (wie z.B. Gutscheine, Downloadartikel, etc.), die Ihre Zahlungsartenauswahl im Folgenden einschränken werden (z.B. keine Finanzierungsmöglichkeit).',
);
