<?php
/* --------------------------------------------------------------
	checkout_payment.lang.inc.php 2018-02-13
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'heading_payment' => 'Zahlungsweise',
	'text_accept_agb' => 'Ich akzeptiere Ihre allgemeinen Geschäfts- und Lieferbedingungen',
	'text_accept_transport_conditions' => 'Ich willige ein',
	'text_accept_withdrawal' => 'Ich habe das Widerrufsrecht zur Kenntnis genommen',
	'text_address' => 'Bitte wählen Sie die gewünschte Rechnungsadresse für Ihre Bestellung aus.',
	'text_agb' => 'Bitte bestätigen Sie unsere AGB',
	'text_comments' => 'Fügen Sie hier Ihre Anmerkungen zu dieser Bestellung ein',
	'text_confirm' => 'Bestätigen',
	'text_continue' => 'Zur Bestätigung Ihrer Bestellung',
	'text_finished' => 'Fertig!',
	'text_gccover' => 'Ihr Kupon deckt den Warenwert.',
	'text_payment' => 'Bitte wählen Sie die gewünschte Zahlungsweise aus.',
	'text_payment_info' => 'Zahlungsinformationen',
	'text_ship' => 'Versand',
	'text_pay' => 'Zahlung',
	'text_shipping_info' => 'Versandinformationen',
	'text_shoppingcart' => 'Warenkorb',
	'text_transport_conditions' => 'Bitte erteilen Sie Ihre Einwilligung.',
	'text_withdrawal' => 'Bitte bestätigen Sie unser Widerrufsrecht',
	'text_yourdata' => 'Ihre Daten',
	'title_address' => 'Rechnungsadresse',
	'title_agb' => 'Allgemeine Geschäftsbedingungen',
	'title_comments' => 'Ihre Anmerkungen',
	'title_continue' => 'Fortsetzung des Bestellvorganges',
	'title_payment' => 'Zahlungsweise',
	'title_transport_conditions' => 'Datenweitergabe an Transportunternehmen',
	'title_shipping_company' => 'Transportunternehmen',
	'title_withdrawal' => 'Widerrufsrecht'
);