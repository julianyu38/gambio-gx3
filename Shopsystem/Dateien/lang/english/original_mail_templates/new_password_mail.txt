Dear {if $GENDER == 'm'}Mr {elseif $GENDER == 'f'}Miss/Ms/Mrs {/if}{$NAME},

You receive this mail, because you made an inquiry to set a new password.
Your password has been successfully changed in the course of this inquiry.

We wish you further much fun with our offer!

{$EMAIL_SIGNATURE_TEXT}
