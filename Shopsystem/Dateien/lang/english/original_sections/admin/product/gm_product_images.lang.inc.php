<?php
/* --------------------------------------------------------------
	gm_product_images.lang.inc.php 2018-04-09
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'GM_CATEGORIES_IMAGE_ALT_TEXT'              => 'Alternative text',
	'GM_CATEGORIES_IMAGE_NAME'                  => 'New filename',
	'GM_PRODUCTS_ALT_TEXT'                      => 'Alternative text',
	'GM_PRODUCTS_FILENAME'                      => 'Filename',
	'GM_RENAME_IMAGE_INVALID_FILE_FORMAT'       => 'The filename "%s" has an invalid file extension and has been ignored.',
	'GM_TITLE_REDIRECT'                         => ' <strong> - This file existed already. File was renamed.</strong>',
	'GM_TITLE_REDIRECT_MULTI'                   => ' <strong> - Certain files existed already. Files were renamed.</strong>',
	'GM_UPLOAD_IMAGE_INVALID_FILE_FORMAT'       => 'The file "%s" has an invalid file extension and has not been added to the product.',
	'GM_UPLOAD_IMAGE_MODAL_ERROR'               => 'Error',
	'GM_UPLOAD_IMAGE_MODAL_INVALID_FILE_FORMAT' => 'The selected file has an invalid file extension.',
);