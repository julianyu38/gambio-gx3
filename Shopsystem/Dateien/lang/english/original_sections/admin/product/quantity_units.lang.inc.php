<?php
/* --------------------------------------------------------------
	quantity_units.lang.inc.php 2015-01-05 gm
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2015 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'HEADING_SUB_TITLE' => 'Gambio',
	'HEADING_TITLE' => 'Quantity units',
	'ERROR_INVALID_INPUT_FIELDS' => 'At least one field is required',
	'MENU_TITLE_QUANTITYUNITS' => 'Quantity units (configured)',
	'MENU_TITLE_QUANTITYUNIT_NAME' => 'Quantity Units',
	'MENU_TITLE_QUANTITYUNIT_NEW' => 'Quantity unit (new)',
	'TEXT_BTN_DELETE' => 'delete',
	'TEXT_BTN_LOAD' => 'load',
	'TEXT_BTN_NEW' => 'add',
	'TEXT_BTN_SAVE' => 'save',
	'TEXT_BTN_UPDATE' => 'update',
	'TEXT_DELETE' => 'delete',
	'TEXT_LANG_ALL' => 'all languages',
	'TEXT_LANG_SHOP' => 'actual language',
	'TEXT_NEW_QUANTITYUNIT' => 'new quantity unit',
	'TEXT_QUANTITYUNIT_REMOVE' => 'Are you sure you want to delete this quantity unit?',
	'TEXT_QUANTITYUNIT' => 'edit quantity unit',
	'TEXT_QUANTITYUNIT_NAME' => 'Quantity unit name (selected)',
	'TEXT_INFO_ADD_SUCCESS' => 'Quantity unit added successfully',
	'TEXT_INFO_DELETE_SUCCESS' => 'Quantity unit successful removed',
	'TEXT_INFO_EDIT_SUCCESS' => 'Quantity unit successful edited',
	'TEXT_INFO_EDIT_INTRO' => 'Please make any necessary changes',
	'TEXT_INFO_INSERT_INTRO' => 'Please enter the new quantity unit with its related data',
	'TEXT_INFO_DELETE_INTRO' => 'Are you sure you want to delete this quantity unit?',
);