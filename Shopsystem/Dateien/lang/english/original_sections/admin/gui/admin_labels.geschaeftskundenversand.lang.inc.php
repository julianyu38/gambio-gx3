<?php
/* --------------------------------------------------------------
	admin_labels.geschaeftskundenversand.lang.inc.php 2016-07-29
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'gkv_label_get'                        => 'DHL Label (GKV)',
);
