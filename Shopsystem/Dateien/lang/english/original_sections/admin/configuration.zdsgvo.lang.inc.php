<?php
/* --------------------------------------------------------------
	configuration.zdsgvo.lang.inc.php 2018-05-14
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
    'DELETE_GUEST_ACCOUNT_CRON_TITLE' => 'Cronjob-url for deleting guest accounts:',
    'GENDER_MANDATORY_TITLE'          => 'Gender as mandatory field',
    'GENDER_MANDATORY_DESC'           => 'Make the gender a mandatory field',
);
