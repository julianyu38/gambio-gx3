<?php
/* --------------------------------------------------------------
	robots_download.lang.inc.php 2017-05-11
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'HEADING_SUB_TITLE' => 'Create Robots control file and save or download',
	'HEADING_TITLE' => 'Robots Generator',
	'ROBOTS_OBSOLETE' => 'The robots.txt file is obsolete! Generate the latest version here!',
	'ROBOTS_SUBMIT' => 'Generate and download',
	'ROBOTS_SAVE_SUBMIT' => 'Generate and save',
	'TEXT_ROBOTS_DOWNLOAD' => 'Download the robots.txt file and upload it to the main directory of your webserver.',
	'TEXT_ROBOTS_SAVE' => 'Generate the robots.txt file. It will be saved to your document root.',
    'ROBOTS_SAVE_SUCCESS' => 'Die robots.text wurde erfolgreich erstellt und im Hauptverzeichnis gespeichert',
	'ROBOTS_SAVE_ERROR' => 'Die robots.text konnte nicht erstellt und im Hauptverzeichnis gespeichert werden. Bitte überprüfen Sie die Dateiberechtigungen.'
);