<?php
/* --------------------------------------------------------------
	admin_access.lang.inc.php 2018-01-04
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'button_create_role'                      => 'New access role',
	'error_ajax_request_failed'               => 'An unexpected error occurred.',
	'error_invalid_id'                        => 'Invocation was made for an invalid ID.',
	'error_invalid_value'                     => 'Invocation was made for an invalid value.',
	'error_modal_title'                       => 'Error',
	'heading_title'                           => 'Roles and Permissions',
	'headline_admins_overview_assigned_roles' => 'Assigned access roles',
	'headline_admins_overview_name'           => 'Admin',
	'headline_edit_admin_assign_role'         => 'Assign access role',
	'headline_edit_admin_name'                => 'Access role',
	'headline_permissions_overview_deleting'  => 'Delete',
	'headline_permissions_overview_name'      => 'Permission',
	'headline_permissions_overview_reading'   => 'Allow access',
	'headline_permissions_overview_writing'   => 'Writing',
	'headline_roles_overview_name'            => 'Access roles',
	'overview_title_admins'                   => 'Admins',
	'overview_title_assigned_roles'           => 'Role assignment for ',
	'overview_title_granted_permissions'      => 'Permissions for ',
	'overview_title_roles'                    => 'Access role',
	'role_modal_create_title'                 => 'Create access role',
	'role_modal_delete_body'                  => 'Are you sure to delete the following access role?',
	'role_modal_delete_title'                 => 'Delete access role',
	'role_modal_description'                  => 'Description',
	'role_modal_edit_title'                   => 'Edit access role',
	'role_modal_name'                         => 'Name',
	'role_modal_sort_order'                   => 'Sort order',
	'sub_navigation_admins'                   => 'Manage admins',
	'sub_navigation_permissions'              => 'Manage permissions',
	'sub_navigation_role_assignment'          => 'Role assignment',
	'sub_navigation_roles'                    => 'Manage access roles',
	'text_assigned_roles'                     => 'Access role',
	'text_empty_admins_overview_list'         => 'There are no admins available.',
	'text_empty_edit_admin_overview_list'     => 'There are no access roles available. You need to <a href="admin.php?do=AdminAccess/manageRoles">create a access role</a>, before assigning it to an admin.',
	'text_empty_overview_list'                => 'There are no entries available.',
	'text_empty_permissions_overview_list'    => 'There are no permissions available.',
	'text_empty_roles_overview_list'          => 'There are no access roles available.',
	'text_main_admin'                         => 'Main admin',
	'text_all_groups'                         => 'All permissions',
	'text_saved_sorting'                      => 'The new sorting has been successfully saved.',
	'text_unknown_group_name'                 => 'Unknown modules',
	'tooltip_admins_overview'                 => 'The main admin can not be manged, because all permissions will be granted for him always.',
	'tooltip_unknown_reading_group'           => 'This permission summarizes all unknown modules.',
	'tooltip_all_groups'                      => 'This permission represents all existing permissions. It is recommended to grant this permission.',
);