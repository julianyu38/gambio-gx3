<?php
/* --------------------------------------------------------------
   image_processing.lang.inc.php 2017-03-08
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   -------------------------------------------------------------- 
*/

$t_language_text_section_content_array = array
(
    'image_processing_current_file'             => 'Last processed image',
    'image_processing_error'                    => 'Not all images were successfully processed. Information can be found in the <a href="show_logs.php">log</a>.',
    'image_processing_file_not_found'           => 'The given imagefile could not be found. The batch processing failed.',
    'image_processing_label'                    => 'Batch processing for image editing',
    'image_processing_startimage_nr'            => 'Number of starting image',
    'image_processing_startimage_nr_info'       => 'This input field contains the number of an image with which this batch will start. All previous images will be skipped.',
    'image_processing_startimage_file'          => 'Filename of starting image',
    'image_processing_startimage_file_info'     => 'This input field contains the filename of an image with which this batch will start. All previous images will be skipped.',
    'image_processing_success'                  => 'All images were successfully processed.',
    'image_processing_tab_image_options'        => 'Image Options',
    'image_processing_text_info'                => 'This process may take some time and should be interrupted under any circumstances!',
    'image_processing_title'                    => 'Image Processing'
);