<?php
/* --------------------------------------------------------------
	gm_laws.zdsgvo.lang.inc.php 2018-05-16
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'BUTTON_EXPORT'                     => 'Export',
	'TITLE_EXPORT_AGREEMENTS'           => 'Export legal text agreements',
	'TEXT_EXPORT_AGREEMENTS'            => 'Creates a CSV file with all customers which have agreed to the legal texts of the shop',
	'TITLE_PRIVACY_WITHDRAWAL_WEB_FORM' => 'in withdrawal web form',
	'TITLE_PRIVACY_GV_SEND'             => 'in send voucher value form',
	'TEXT_LOG_IP_SHIPPING'              => 'Save IP at transfer of shipping information confirmation',
);