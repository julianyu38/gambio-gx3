<?php
/* --------------------------------------------------------------
	admin_customers.lang.inc.php 2017-01-19
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
    'delete_personal_data' => 'Delete personal data',
    'export_personal_data' => 'Export personal data',
    'select_personal_data_base_data' => 'Base data and addresses',
    'select_personal_data_orders' => 'Orders, invoices and delivery notes',
    'select_personal_data_withdrawals' => 'Withdrawals',
    'select_personal_data_agreements' => 'Agreements',
    'select_personal_data_emails' => 'Emails',
    'select_personal_data_carts' => 'Shopping carts',
    'select_personal_data_reviews' => 'Reviews',
    'select_personal_data_newsletter_subscriptions' => 'Newsletter subscriptions'
);
