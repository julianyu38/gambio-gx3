<?php
/* --------------------------------------------------------------
	admin_invoices.lang.inc.php 2016-10-04
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                             => 'Invoices',
	'BULK_DOWNLOAD_INVOICES'                 => 'Invoices (Bulk-PDF)',
	'CANCELLATION_INVOICE'                   => 'Cancellation Invoice',
	'COLUMN'                                 => 'Column',
	'COLUMNS'                                => 'Spalten',
	'COUNTRY'                                => 'Country',
	'DELETE_INVOICES_ERROR'                  => 'The invoices could not be deleted due to an unexpected error!',
	'DELETE_INVOICES_SUCCESS'                => 'The invoices were deleted successfully!',
	'GROUP'                                  => 'Group',
	'INVOICE_NUMBER'                         => 'Invoice-No.',
	'INVOICE_DATE'                           => 'Invoice-Date',
	'ORDER_DATE'                             => 'Order-Date',
	'ORDER_NUMBER'                           => 'Order-No.',
	'PAYMENT'                                => 'Payment',
	'CUSTOMER'                               => 'Customer',
	'ROW'                                    => 'Row',
	'ROW_HEIGHT_LABEL'                       => 'Height',
	'ROW_HEIGHT_SMALL'                       => 'small',
	'ROW_HEIGHT_MEDIUM'                      => 'medium',
	'ROW_HEIGHT_LARGE'                       => 'large',
	'SUM'                                    => 'Sum',
	'STATUS'                                 => 'Order Status',
	'SETTINGS'                               => 'Settings',
	'TEXT_INFO_HEADING_MULTI_DELETE_INVOICE' => 'Delete invoices',
	'TEXT_INFO_MULTI_DELETE_INTRO'           => 'Are you sure you want to delete this invoices?',
	'TEXT_SELECTED_INVOICES'                 => 'Selected Invoices',
	'TEXT_BULK_PDF_ERROR'                    => 'The maximal amount of allowed invoices for the bulk pdf is %d. Please select less invoices 
	                            or adjust the the <a href="%s">settings</a>.',
	'HEADING_BULK_PDF_ERROR'                 => 'Too many invoices selected',
];