<?php
/* --------------------------------------------------------------
	gm_seo_boost.lang.inc.php 2017-03-15
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'GM_BOX_TITLE' => 'Search engine friendly URL Pros',
	'GM_FORM_REPAIR' => 'Repair URLs',
	'GM_FORM_REPAIR_SUCCESS' => 'Errors successfully fixed!',
	'GM_FORM_SUBMIT' => 'Save',
	'GM_SEO_BOOST_TEXT' => 'To use "SEO BOOST" your server must support "mod_rewrite".',
	'GM_TEXT_CATEGORIES' => 'Activate search engine friendly URLs for categories',
	'GM_TEXT_CONTENT' => 'Activate search engine friendly URLs for the content manager',
	'GM_TEXT_PRODUCTS' => 'Activate search engine friendly URLs for the article page',
	'GM_TEXT_SHORT_URLS' => 'Short product-URLs',
	'HEADING_SUB_TITLE' => 'Gambio',
	'HEADING_TITLE' => 'SEO Boost',
        'TEXT_SUPPRESS_INDEX_IN_URL' => 'Suppress usage of index.php postfix in corresponding URLs',
	'USE_SEO_BOOST_LANGUAGE_CODE' => 'Add language code to URLs'
);
