<?php
/* --------------------------------------------------------------
	admin_orders.lang.inc.php 2017-12-06
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'ADD_TRACKING_NUMBER_ERROR'         => 'The tracking number could not be added due to an unexpected error!',
	'ADD_TRACKING_NUMBER_SUCCESS'       => 'The tracking number was added successfully.',
	'CANCEL_ORDERS_ERROR'               => 'The orders could not be cancelled due to an unexpected error!',
	'CANCEL_ORDERS_SUCCESS'             => 'The orders were cancelled successfully!',
	'COLUMN'                            => 'Column',
	'COLUMNS'                           => 'Columns',
	'COUNTRY'                           => 'Country',
	'CREATE_MISSING_DOCUMENTS'          => 'Create missing documents.',
	'CUSTOMER'                          => 'Customer',
	'DATE'                              => 'Date',
	'DELETE_ORDERS_ERROR'               => 'The orders could not be deleted due to an unexpected error!',
	'DELETE_ORDERS_SUCCESS'             => 'The orders were deleted successfully!',
	'DELETE_PDF_CONFIRM_MESSAGE'        => 'Do you really want to delete this PDF?',
	'DISPLAY_TOOLTIPS_LABEL'            => 'Activate tooltips',
	'DOWNLOAD_DOCUMENTS'                => 'Download Documents',
	'GET_ORDERS_WITHOUT_DOCUMENT_ERROR' => 'The orders without documents could not be fetched due to an unexpected error!',
	'GROUP'                             => 'Group',
	'NUMBER'                            => 'Nr.',
	'ORDER_WEIGHT_UNIT'                 => 'Kg',
	'ORDERS_WITHOUT_DOCUMENT'           => 'The following orders do not have a document',
	'PAGE_TITLE'                        => 'Orders',
	'PAYMENT'                           => 'Payment',
	'ROW'                               => 'Row',
	'ROW_HEIGHT_LABEL'                  => 'Height',
	'ROW_HEIGHT_LARGE'                  => 'Large',
	'ROW_HEIGHT_MEDIUM'                 => 'Medium',
	'ROW_HEIGHT_SMALL'                  => 'Small',
	'SETTINGS'                          => 'Settings',
	'SHIPPING'                          => 'Shipping',
	'STATUS'                            => 'Status',
	'SUM'                               => 'Sum',
	'TOTAL_WEIGHT'                      => 'Weight',
    'VOUCHER'                           => 'Voucher',
);