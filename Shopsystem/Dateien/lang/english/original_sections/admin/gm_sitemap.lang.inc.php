<?php
/* --------------------------------------------------------------
	gm_sitemap.lang.inc.php 2017-09-19
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'GM_SITEMAP_CHANGEFREQ' => 'Default Change Frequency',
	'GM_SITEMAP_PRIORITY' => 'Default Priority',
	'HEADING_TITLE' => 'Sitemap Generator',
	'MENU_TITLE_GM_SITEMAP' => 'Create Sitemap',
	'MENU_TITLE_GM_SITEMAP_CONF' => 'Configure Sitemap',
	'TITLE_ALWAYS' => 'always',
	'TITLE_CREATE_SITEMAP' => 'Create Sitemap',
	'TITLE_DAILY' => 'daily',
	'TITLE_HOURLY' => 'hourly',
	'TITLE_MONTHLY' => 'monthly',
	'TITLE_NEVER' => 'never',
	'TITLE_PING_ASK' => 'Ping Ask.com',
	'TITLE_PING_GOOGLE' => 'Ping Google',
	'TITLE_PING_LIVE' => 'Ping Live Search',
	'TITLE_PING_YAHOO' => 'Ping Yahoo!',
	'TITLE_WEEKLY' => 'weekly',
	'TITLE_YEARLY' => 'annually',
    'MENU_TITLE_GM_SITEMAP_PUBLISH_ALL' => 'Publish all',
    'PUBLISH_ALL' => 'Publish',
    'PUBLISH_ALL_TEXT' => 'Publish all categories and products in sitemap',
    'TITLE_GENERATE' => 'Click on execute, in order to generate the sitemap',
);