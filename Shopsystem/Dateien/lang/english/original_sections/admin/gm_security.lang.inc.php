<?php
/* --------------------------------------------------------------
	gm_security.lang.inc.php 2018-02-23
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'GM_CALLBACK_SERVICE_VVCODE' => 'Activate verification code on the "Callback Service" page?',
	'GM_CAPTCHA_TYPE' => 'Verification type',
	'GM_CAPTCHA_TYPE_RECAPTCHA_NAME' => 'reCAPTCHA',
	'GM_CAPTCHA_TYPE_RECAPTCHA_V2_NAME' => 'reCaptcha v2',
	'GM_CAPTCHA_TYPE_STANDARD_NAME' => 'Standard',
	'GM_CONTACT_VVCODE' => 'Activate verification code on the "Contact Us" page?',
    'GM_CREATE_ACCOUNT_VVCODE' => 'Activate verification on "Create account" page?',
    'GM_CREATE_GUEST_ACCOUNT_VVCODE' => 'Activate verification on "Create guest account" page?',
	'GM_FORGOT_PASSWORD_VVCODE' => 'Activate verification code on the "Forgot password?" page?',
	'GM_GUESTBOOK_VVCODE' => 'Activate verification code on the "Guestbook" page?',
	'GM_LOGIN_TIMELINE' => 'Time period (sec.)',
	'GM_PASSWORD_ENCRYPTION' => 'Password encryption',
	'GM_PASSWORD_ENCRYPTION_TYPE_INFO' => 'Select password_hash for strongly encrypted passwords (recommended). Select MD5 if you use external interfaces that synchronize passwords directly through the database and do not support password_hash encrypted passwords.',
	'GM_PASSWORD_ENCRYPTION_TYPE' => 'Standard encryption for customer passwords',
	'GM_PASSWORD_ENCRYPTION_TYPE_MD5' => 'md5',
	'GM_PASSWORD_ENCRYPTION_TYPE_PASSWORD_HASH' => 'password_hash',
	'GM_PASSWORD_REENCRYPT' => 'Encrypt the customer passwords again, at next login, with the current standard encryption, if necessary? (recommended)',
	'GM_PASSWORD_REENCRYPT_WARNING' => 'Enable this option, if you change the default encryption and customers (including admin accounts!) should still log in with their previously stored passwords. Otherwise, the login is no longer possible and a new password has to be set by using the "Password forgotten"-function.',
	'GM_PRICE_OFFER_VVCODE' => 'Activate verification code on the "Found Cheaper?" page?',
	'GM_NEWSLETTER_VVCODE' => 'Activate verification code on the "Newsletter" page?',
	'GM_RECAPTCHA_PRIVATE_KEY' => 'Private key',
	'GM_RECAPTCHA_PUBLIC_KEY' => 'Public key',
	'GM_REVIEWS_VVCODE' => 'Activate verification code on the "Reviews" page?',
	'GM_SEARCH_TIMELINE' => 'Time period (sec.)',
	'GM_SEARCH_TIMEOUT' => 'Timeout (sec.)',
	'GM_SEARCH_TRYOUT' => 'Search attemps',
	'GM_SECURITY_SUCCESS' => 'Changes were successfully updated.',
	'GM_SEC_TIMEOUT' => 'Timeout (sec.)',
	'GM_SEC_TRYOUT' => 'Login attemps (min. 2)',
	'GM_TELL_A_FRIEND_VVCODE' => 'Activate verification code on the "Ask Product Question" page?',
	'GM_TITLE_LOGIN' => 'Login Tracker',
	'GM_TITLE_SEARCH' => 'Search Tracker',
	'GM_TITLE_VVCODE' => 'Activate verification',
	'HEADING_SUB_TITLE' => 'Gambio',
	'HEADING_TITLE' => 'Security Center'
);