<?php
/* --------------------------------------------------------------
	file_manager.lang.inc.php 2017-03-24
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'ACTIONS'              => 'Actions',
	'CONFIRM_DELETE_TITLE' => 'Delete file',
	'CONFIRM_DELETE_TEXT'  => 'Are you sure you want to delete this file?',
	'CREATE'               => 'Create',
	'CREATE_NEW_FOLDER'    => 'Create new Folder',
	'DATE_MODIFIED'        => 'Modified',
	'DOWNLOAD'             => 'Downloads',
	'DRAG_INFORMATION'     => 'Drag files here to upload or',
	'EMPTY_DIRECTORY'      => 'This folder is empty',
	'ERROR_NO_FILEMANAGER' => 'Filemanager is not installed. Please install the Filemanager to use its functionality.',
	'FILENAME'             => 'Name',
	'HEADING_TITLE'        => 'File Manager',
	'IMAGES'               => 'Other images',
	'INFO_ORIGINAL_IMAGES' => 'Upload images to this directory for image imports. Images that are uploaded to this directory will be processed by the Image Processing.',
	'MEDIA'                => 'Media',
	'PRODUCT_IMAGES'       => 'Product images',
	'SIZE'                 => 'Size',
);