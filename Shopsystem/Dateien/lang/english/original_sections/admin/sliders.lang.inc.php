<?php
/* --------------------------------------------------------------
   sliders.lang.inc.php 2016-11-22
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2016 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                             => 'Teaser-Slider',
	'NAME'                                   => 'Name',
	'AMOUNT_OF_SLIDES'                       => 'Amount of slides',
	'IS_ON_STARTPAGE'                        => 'Start Page',
	'SPEED'                                  => 'Speed',
	'MASTER_DATA'                            => 'Teaser-Slider master data',
	'SLIDES'                                 => 'Slides',
	'THUMBNAIL'                              => 'Thumbnail',
	'BREAKPOINT_XS'                          => 'Smartphone',
	'BREAKPOINT_SM'                          => 'Tablet Portrait',
	'BREAKPOINT_MD'                          => 'Tablet Landscape',
	'BREAKPOINT_LG'                          => 'Desktop',
	'TITLE'                                  => 'Title',
	'ALT_TEXT'                               => 'Alternative text',
	'LINK'                                   => 'Link',
	'LINK_TARGET_BLANK'                      => 'Open in new window',
	'LINK_TARGET_SELF'                       => 'Open in same window',
	'DELETE_SLIDE_MODAL_TITLE'               => 'Delete slide',
	'DELETE_SLIDER_MODAL_TITLE'              => 'Delete slider',
	'DELETE_SLIDE_MODAL_TEXT'                => 'Do you really want to delete the slide?',
	'DELETE_SLIDER_MODAL_TEXT'               => 'Do you really want to delete the slider?',
	'DELETE_SLIDER_ERROR_TITLE'              => 'Slider could not be deleted',
	'DELETE_SLIDER_ERROR_TEXT'               => 'The chose Slider could unfortunately not be deleted.',
	'IMAGE_MAP_MODAL_TITLE'                  => 'Edit link areas',
	'SAVE_SLIDER_FIRST_NOTICE_TEXT'          => 'To edit the link areas, the slider needs to be saved first',
	'SLIDE_SORT_BUTTON_TEXT'                 => 'Sort slides',
	'SLIDE_SORT_BUTTON_EXIT_TEXT'            => 'Quit sorting',
	'SLIDE_CREATE_BUTTON_TEXT'               => 'Add slide',
	'SLIDER_START_PAGE_ERROR_TITLE'          => 'Slider could not be set',
	'SLIDER_START_PAGE_ERROR_TEXT'           => 'The slider could not be set for the start page.',
	'DELETE_IMAGE_MODAL_TITLE'               => 'Delete image',
	'DELETE_IMAGE_MODAL_TEXT'                => 'This image is already in use somewhere else. If you delete this image, it will be deleted from all affected places. Continue?',
	'INVALID_FILE_MODAL_TITLE'               => 'Invalid file',
	'INVALID_FILE_MODAL_TEXT'                => 'The file you have chosen is not a valid image file. Please choose another file.',
	'FILENAME_ALREADY_USED_MODAL_TITLE'      => 'File name is already in use',
	'FILENAME_ALREADY_USED_MODAL_TEXT'       => 'The file you have chosen is already present in the select box. Please rename the selected file or choose another file.',
	'PLACEHOLDER_TITLE'                      => 'Slide title',
	'PLACEHOLDER_ALT_TEXT'                   => 'Alternative text',
	'PLACEHOLDER_LINK'                       => 'http://my-shop.com',
	'FORM_SUBMIT_ERROR_MODAL_TITLE'          => 'Error while saving the slider',
	'FORM_SUBMIT_ERROR_MODAL_TEXT'           => 'Slider could not be saved. Please revise the form data and try again',
	'NEW_SLIDE'                              => 'New slide',
	'EXIT_CONFIRMATION_TEXT'                 => 'Do you really want to leave the page? Your changes will be lost',
	'NO_ENTRIES'                             => 'There are no entries',
	'DO_NOT_USE'                             => 'Do not use',
	'LINK_AREA'                              => 'Link area',
	'LINK_TITLE'                             => 'Link area title',
	'LINK_URL'                               => 'Link area URL',
	'LINK_TARGET'                            => 'Link area target',
	'CREATE_LINK_AREA'                       => 'Create new link area',
	'DRAW_IMAGE_AREA'                        => 'Draw the link area here',
	'PLEASE_SELECT'                          => 'Please select',
	'NEW_LINKED_AREA'                        => 'New image area',
	'IMAGE_MAP_USAGE_INFO_LEFT_CLICK'        => 'Left click to draw a path or to add or move a point.',
	'IMAGE_MAP_USAGE_INFO_RIGHT_CLICK'       => 'Right click to remove a point.',
	'RESET_PATH'                             => 'Remove path',
	'MISSING_PATH_OR_LINK_TITLE_MODAL_TITLE' => 'Missing path or link area title',
	'MISSING_PATH_OR_LINK_TITLE_MODAL_TEXT'  => 'Please draw a path first and enter the link area title before saving the link area.',
	'IMAGE_MAP_URL_INFO'                     => 'For external links it is necessary to specify the URL with http:// or https://.'
];