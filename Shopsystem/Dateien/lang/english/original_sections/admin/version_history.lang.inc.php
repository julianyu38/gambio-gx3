<?php
/* --------------------------------------------------------------
	version_history.lang.inc.php 2017-05-12
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                   => 'Version History',
	'TABLE_HEAD_NAME'              => 'Name',
	'TABLE_HEAD_VERSION'           => 'Version',
	'TABLE_HEAD_INSTALLATION_DATE' => 'Installation date',
	'TABLE_HEAD_VENDOR'            => 'Vendor',
	'TABLE_FOOT_HEADLINE'          => 'Legend:',
	'TABLE_FOOT_TEXT_INSTALLED'    => 'Installed Update',
	'TABLE_FOOT_TEXT_FORCED'       => 'Forced version',
	'TABLE_FOOT_TEXT_CURRENT'      => 'Current version',
	'TABLE_FOOT_TEXT_ORIGIN'       => 'Initial installation',
	'TABLE_HEAD_CHANGELOG'         => 'Changelog',
	'TABLE_FOOT_TEXT_FAILED'       => 'Missing Update'
];