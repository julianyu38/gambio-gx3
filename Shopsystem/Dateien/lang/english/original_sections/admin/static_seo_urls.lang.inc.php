<?php
/* --------------------------------------------------------------
   static_seo_urls.lang.inc.php 2017-05-29
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2017 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'PAGE_TITLE'                                => 'Static Pages',
	'CONTENT_MANAGER'                           => 'Content Manager',
	'PRODUCTS_CONTENT'                          => 'Products Content',
	'NAME'                                      => 'Name',
	'MASTER_DATA'                               => 'Master data',
	'SEO_DATA'                                  => 'SEO data',
	'SITEMAP_ENTRY_OVERVIEW'                    => 'Sitemap',
	'SITEMAP_ENTRY'                             => 'Link in sitemap',
	'CHANGEFREQ'                                => 'Change frequency in sitemap',
	'PRIORITY'                                  => 'Priority in sitemap ',
	'ROBOTS_ENTRY_OVERVIEW'                     => 'Robots-Disallow',
	'ROBOTS_ENTRY'                              => 'Add to robots.txt (disallow)',
	'STATIC_SEO_URL_CONTENT_CREATE_BUTTON_TEXT' => 'Add content',
	'TITLE'                                     => 'Meta title',
	'DESCRIPTION'                               => 'Meta description',
	'KEYWORDS'                                  => 'Meta keywords',
	'NO_ENTRIES'                                => 'No entries',
	'DELETE_STATIC_SEO_URL_MODAL_TITLE'         => 'Delete static page',
	'DELETE_STATIC_SEO_URL_MODAL_TEXT'          => 'Are you sure you want to delete this static page?',
	'DELETE_STATIC_SEO_URL_ERROR_TITLE'         => 'The static page could not be deleted',
	'DELETE_STATIC_SEO_URL_ERROR_TEXT'          => 'The chosen static page could unfortunately not be deleted.',
	'FORM_SUBMIT_ERROR_MODAL_TITLE'             => 'Fehler beim Speichern der statischen Seite',
	'FORM_SUBMIT_ERROR_MODAL_TEXT'              => 'The static page could not be saved. Please revise the form data and try again.',
	'STATIC_SEO_URL_CHECKBOX_ERROR_TITLE'       => 'An error occured',
	'STATIC_SEO_URL_CHECKBOX_ERROR_TEXT'        => 'An error occured while changing the static pages state.',
	'CHANGEFREQ_ALWAYS'                         => 'always',
	'CHANGEFREQ_HOURLY'                         => 'hourly',
	'CHANGEFREQ_DAILY'                          => 'daily',
	'CHANGEFREQ_WEEKLY'                         => 'weekly',
	'CHANGEFREQ_MONTHLY'                        => 'monthly',
	'CHANGEFREQ_YEARLY'                         => 'yearly',
	'CHANGEFREQ_NEVER'                          => 'never',
];