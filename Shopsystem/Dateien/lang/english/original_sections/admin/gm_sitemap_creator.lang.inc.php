<?php
/* --------------------------------------------------------------
	gm_sitemap_creator.lang.inc.php 2017-05-04
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'TITLE_SITEMAP_ANSWER' => 'Read answer from ',
	'TITLE_SITEMAP_ANSWER2' => '',
	'TITLE_SITEMAP_CREATED' => '<span class="gm_strong">The sitemap has been created successfully.</span><br /><br />',
	'TITLE_SITEMAP_CREATED2' => '%s links have been created. You can access the sitemap using the following URL: %s',
	'TITLE_SITEMAP_FAILED' => '<strong>Google cannot be notified of this action, because the fsockopen and curl functions are not available on this server.',
	'TITLE_SITEMAP_PING' => 'Google sent the following response: ',
	'TITLE_SITEMAP_PUBLISH_ALL_SUCCESS' => 'All categories and products have been published in the sitemap.'
);