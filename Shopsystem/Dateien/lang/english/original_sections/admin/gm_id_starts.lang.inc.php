<?php
/* --------------------------------------------------------------
	gm_id_starts.lang.inc.php 2016-10-17
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'GM_ID_STARTS_CUSTOMERS_ERROR' => 'The order number value is below the minimum or is not a number.',
	'GM_ID_STARTS_NEXT_CUSTOMER_ID' => 'Next customer number: ',
	'GM_ID_STARTS_NEXT_ORDER_ID' => 'Next order number: ',
	'GM_ID_STARTS_NO_SUCCESS' => 'Some changes were not possible.',
	'GM_ID_STARTS_ORDERS_ERROR' => 'The customer number value is below the minimum or is not a number.',
	'GM_ID_STARTS_SUCCESS' => 'Changed successfully.',
	'GM_ID_STARTS_TEXT' => 'Here you can change the starting numbers for orders and customers. Only numbers are possible.',
	'GM_INVOICE_ID' => 'format invoice ID: ',
	'GM_INVOICE_ID_PLACEMENT' => 'placement invoice ID {INVOICE_ID}',
	'GM_NEXT_FREE_ID_TEXT' => 'Next free number after the highest ever',
	'GM_NEXT_GAP_TEXT' => 'Next free gap',
	'GM_NEXT_ID_TEXT' => 'Here you can change the initial numbers for the invoice and the packing slip.',
	'GM_NEXT_INVOICE_ID' => 'next invoice ID: ',
	'GM_NEXT_INVOICE_ID_ERROR' => 'The invoice ID already exists or is not a number.',
	'GM_NEXT_INVOICE_ID_SUCCESS' => 'invoice ID saved',
	'GM_NEXT_PACKING_ID_ERROR' => 'The packing slip ID already exists or is not a number.',
	'GM_NEXT_PACKING_ID_SUCCESS' => 'packing slip ID has been saved.',
	'GM_NEXT_PACKINGS_ID' => 'next packing slip ID: ',
	'GM_PACKING_ID_PLACEMENT' => 'placement packing slip ID {DELIVERY_ID}',
	'GM_PACKINGS_ID' => 'format packing slip ID: ',
	'GM_TITLE_ID' => 'orders and customers',
	'GM_TITLE_NEXT_ID' => 'invoice and packing slip',
	'HEADING_SUB_TITLE' => 'Gambio',
	'HEADING_TITLE' => 'ID Starts'
);