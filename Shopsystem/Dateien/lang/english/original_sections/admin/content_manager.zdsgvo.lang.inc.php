<?php
/* --------------------------------------------------------------
	content_manager.zdsgvo.lang.inc.php 2018-05-16
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
    'TITLE_CONTENT_VERSION' => 'Version',
);