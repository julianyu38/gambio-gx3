<?php
/* --------------------------------------------------------------
	postfinanceag_visa.lang.inc.php 2015-01-05 gm
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2015 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_TEXT_TITLE' => 'Twint',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_TEXT_DESCRIPTION' => 'Postfinance Twint<br />Information: <a href="http://www.postfinance.ch/pf/content/de/seg/biz/product/eserv/epay/seller/offer.html" target="_blank">www.postfinance.ch</a><br />Support/Installation service: <a href="http://www.swisswebxperts.ch/postfinance.php" target="_blank">www.swisswebXperts.ch</a>',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_TEXT_INFO' => '',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_TEXT_ERROR' => 'Twint - Transaction failure',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_ERROR' => 'The payment has been denied by the Twint!',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_ALLOWED_TITLE' => 'Allowed zones',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_ALLOWED_DESC' => 'Enter the zones <b>separately</b> in which this module is allowed (e.x. CH,AT,DE (leave blank for all zones))',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_STATUS_TITLE' => 'Activate Twint Payment',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_STATUS_DESC' => 'Do you want accept payments with Twint?',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_CURRENCY_TITLE' => 'Currency',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_CURRENCY_DESC' => 'Used currency by Twint',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_SORT_ORDER_TITLE' => 'Order show',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_SORT_ORDER_DESC' => 'Order showing. Deeper is shown first',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_ZONE_TITLE' => 'Paymentzone',
	'MODULE_PAYMENT_POSTFINANCEAG_TWINT_ZONE_DESC' => 'If a zone is selected then this payment method takes effect'
);