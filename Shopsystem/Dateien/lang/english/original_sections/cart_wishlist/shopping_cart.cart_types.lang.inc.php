<?php
/* --------------------------------------------------------------
	shopping_cart.cart_types.lang.inc.php 2016-11-07
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/


$t_language_text_section_content_array = array
(
	'cart_content_mixed'    => 'Your shopping cart contains non-physical goods (e.g. coupons, downloadable products). This can cause your choice of payment methods to be limited (e.g. no payment by installments).',
	'cart_content_physical' => '',
	'cart_content_virtual'  => 'Your shopping cart contains non-physical goods (e.g. coupons, downloadable products). This can cause your choice of payment methods to be limited (e.g. no payment by installments).',
);
