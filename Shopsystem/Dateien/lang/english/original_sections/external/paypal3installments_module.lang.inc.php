<?php
/* --------------------------------------------------------------
	paypal3installments_module.lang.inc.php 2016-09-30
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2015 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_ALLOWED_DESC'     => 'Please enter zones in which to use this module. (e.g. AT,DE (all zones allowed if left empty))',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_ALLOWED_TITLE'    => 'Allowed zones',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_SORT_ORDER_DESC'  => 'Display order (smallest number first)',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_SORT_ORDER_TITLE' => 'Display order',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_STATUS_DESC'      => 'Accept payments via installments powered by PayPal?',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_STATUS_TITLE'     => 'Activate PayPal Installments payment module',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_DESCRIPTION' => 'Faster, safer, simpler - we accept Installments powered by PayPal',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_INFO'        => 'Pay with Installments powered by PayPal',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_TITLE'       => 'Installments Powered by PayPal',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_TEXT_TITLE_ADMIN' => 'Installments Powered by PayPal',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_ZONE_DESC'        => 'If a zone is selected, payment method is available for that zone only.',
	'MODULE_PAYMENT_PAYPAL3_INSTALLMENTS_ZONE_TITLE'       => 'Payment zone'
);

