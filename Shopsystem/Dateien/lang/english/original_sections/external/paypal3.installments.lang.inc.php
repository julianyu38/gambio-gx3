<?php
/* --------------------------------------------------------------
	paypal3.installments.lang.inc.php 2017-01-25
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'amount_too_high'                => 'Installments Powered by PayPal is only available for amounts of up to 5000 EUR. We apologize for the inconvenience.',
	'by_amount_of'                   => 'by amount of',
	'effective_apr'                  => 'effective annual percentage rate',
	'error_retrieving_installments_info' => 'We encountered a technical error, please try again later.',
	'financing_from'                 => 'Financing from',
	'generic_installments'           => 'Installments',
	'generic_intro'                  => 'Pay simply and comfortably with',
	'generic_intro_checkout_payment' => 'You can pay for these products by installments!',
	'generic_intro_product'          => 'You can pay for this product by installments!',
	'generic_intro_shopping_cart'    => 'You can pay for these products by installments!',
	'in'                             => 'in',
	'info_on_financing_options'      => 'Information on financing options',
	'info_representative_example'    => 'Also representative example as per § 6a PAngV',
	'installments'                   => 'Installments',
	'installments_of'                => 'of',
	'intro_non_qualifying'           => 'More options for possible credit amounts',
	'lender'                         => 'Lender',
	'min_amount'                     => 'Minimum amount',
	'monthly'                        => 'monthly',
	'monthly_payments_dative'        => 'monthly payments',
	'monthly_payments_nominative'    => 'monthly payments',
	'net_loan_amount'                => 'net loan amount',
	'nominal_rate'                   => 'nominal rate',
	'out_of_bounds'                  => 'Financing available from 99 EUR to 5000 EUR cart value',
	'pay_by_installments'            => 'Pay by Installments',
	'pay_by_monthly_installments'    => 'Pay simply and comfortably by monthly installments',
	'paypal_installments_box_title'  => 'Installments Powered by PayPal',
	'per_month'                      => 'per month',
	'plan'                           => 'plan',
	'popup_infotext'                 => 'You can choose your installments and financing plan during checkout. Your application is processed entirely online and will be completed with few steps here in our shop.',
	'powered_by'                     => 'Powered by',
	'representative_example'         => 'Representative example as per § 6a PAngV',
	'sorry_no_qualifying_options'    => 'For the above mentioned loan amount financing is not available.',
	'total_cost'                     => 'total cost',
	'total_interest'                 => 'interest amount',
	'with_installments_by_paypal'    => 'with Installments Powered by PayPal',
);
