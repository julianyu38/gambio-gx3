<?php

/* --------------------------------------------------------------
	responsivefilemanager.lang.inc.php 2017-09-29
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$t_language_text_section_content_array = [
	'module_title'                             => 'File manager',
	'module_description'                       => 'Includes the responsive file manager',
	'config_use_in_ckeditor'                   => 'Include the file manager in the CKEditor',
	'config_use_in_product_and_category_pages' => 'Include the file manager in categories and products pages',
	'config_use_in_manufacturer_pages'         => 'Include the file manager in manufacturer pages',
	'config_use_in_content_manager_pages'      => 'Include the file manager in content manager pages',
	'config_use_in_banner_manager_pages'       => 'Include the file manager in banner manager pages',
	'config_use_in_attribute_pages'            => 'Include the file manager in product attributes pages',
	'config_use_in_shipping_status_pages'      => 'Include the file manager in shipping status pages',
	'config_use_in_email_pages'                => 'Include the file manager in the emails page',
	'button_submit'                            => 'Save'
];