<?php
/* --------------------------------------------------------------
	ot_ppinstfee_module.lang.inc.php 2016-10-11
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2016 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
	--------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_DESCRIPTION'              => 'Total with financing cost (Installments Powered by PayPal)',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_SORT_ORDER_DESC'          => 'Display sort order.',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_SORT_ORDER_TITLE'         => 'Sort order',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_STATUS_DESC'              => 'Display order total with financing cost?',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_STATUS_TITLE'             => 'Display total with financing cost',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_FEE_TITLE'                => 'Financing cost',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_TITLE'                    => 'Total (with financing cost)',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_TITLE_ADMIN'              => 'Total (for Installments Powered by PayPal)',
	'MODULE_ORDER_TOTAL_PAYPAL3_INSTFEE_FEE_PAYMENT_METHOD_TITLE' => 'Installments Powered by PayPal',
);
