<?php
/* --------------------------------------------------------------
	general.dsgvo.inc.php 2018-05-09
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'TEXT_EMAIL_PASSWORD_NEW_PASSWORD' => 'Your password has been changed',
	'TEXT_LINK_MAIL_SENDED'            => 'Your inquiry to set a new password must be confirmed by you personally.<br />You will therefore receive an email with your personal confirmation code link, if there is an account with the provided email address. Once you have received the email, please click on the hyperlink and you will be able to set a new pasword. If you do not click on the link, you will not be able to set a new password!',
	'TEXT_PASSWORD_SAVED'              => 'The new password has been succesfully saved.',
);