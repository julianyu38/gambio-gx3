<?php
/* --------------------------------------------------------------
	account_delete.lang.inc.php 2017-10-23 gm
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'heading_delete_account' => 'Delete Account',
	'label_annotation' => 'Your message to us',
	'title_delete_account' => 'Comment'
);