<?php
/* --------------------------------------------------------------
	create_account.lang.inc.php 2017-09-18
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2015 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = [
    'heading_create_account'       => 'Information on your customer account',
    'heading_create_guest_account' => 'Your guest account',
    'premade_password'             => 'We have assigned a password for you. You can enter a password of your own choice if you want to log in to this shop with your e-mail address and a password.',
    'text_additional_info'         => 'Additional information',
    'text_b2b_status'              => 'Tradesperson',
    'text_birthdate'               => 'Date of birth',
    'text_city'                    => 'Town',
    'text_code'                    => 'Postcode/ZIP',
    'text_company'                 => 'Company name',
    'text_confirmation'            => 'Enter password again',
    'text_country'                 => 'Country',
    'text_email'                   => 'Email address',
    'text_email_confirm'           => 'Enter email again',
    'text_fax'                     => 'Fax number',
    'text_female'                  => 'Miss/Ms/Mrs',
    'text_firstname'               => 'Firstname',
    'text_gender'                  => 'Gender',
    'text_house_number'            => 'No.',
    'text_intro_password'          => '',
    'text_lastname'                => 'Lastname',
    'text_male'                    => 'Mr',
    'text_must'                    => '* necessary information',
    'text_newsletter'              => 'Newsletter',
    'text_no'                      => 'No',
    'text_password'                => 'Enter a password',
    'text_state'                   => 'County',
    'text_street'                  => 'Street',
    'text_street_number'           => 'Street/No.',
    'text_suburb'                  => 'House/Flat/etc.',
    'text_tel'                     => 'Phone number',
    'text_ustid'                   => 'vat No.',
    'text_yes'                     => 'Yes',
    'title_address'                => 'your postal address',
    'title_company'                => 'Company dates',
    'title_contact'                => 'Your contact informationen',
    'title_customer_upload'        => 'File-Upload',
    'title_options'                => 'Options',
    'title_password'               => 'Use a password to secure your information.',
    'title_personal'               => 'Your personal details',
    'title_privacy'                => 'privacy',
    'title_captcha'                => 'Security Code',
    'text_input_captcha'           => 'Enter security code'
];
