<?php
/* --------------------------------------------------------------
	address_book_process.lang.inc.php 2017-04-05
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'gender_female' => 'Miss/Ms/Mrs',
	'gender_male' => 'Mr',
	'heading_address' => 'Address book',
	'label_address' => 'Standard postal address',
	'label_city' => 'Town',
	'label_code' => 'Postcode (ZIP code)',
	'label_company' => 'Company name',
	'label_country' => 'Country',
	'label_firstname' => 'First name',
	'label_gender' => 'Gender',
	'label_lastname' => 'Last name',
	'label_standard' => 'Standard address',
	'label_state' => 'State',
	'label_street' => 'Street',
	'label_street_number' => 'No./Street',
	'label_house_number' => 'No.',
	'label_house_number_long' => 'House no.',
	'label_suburb' => 'House/Flat/etc.',
	'label_ustid' => 'VAT No.',
	'text_b2b_status' => 'Tradesperson',
	'text_delete' => 'Would you like to delete the postal address? This action is irreversible.',
	'text_must' => '* necessary information',
	'text_no' => 'No',
	'text_yes' => 'Yes',
	'title_address' => 'Your postal address',
	'title_delete' => 'Delete entry'
);