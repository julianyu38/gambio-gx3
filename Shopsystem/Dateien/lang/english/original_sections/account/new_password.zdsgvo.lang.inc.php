<?php
/* --------------------------------------------------------------
	new_password.dsgvo.inc.php 2018-05-09
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array(
	'label_confirm_password' => 'Confirm password',
	'label_new_password'     => 'New password',
	'text_step3'             => 'Step 3:',
	'text_step3_info'        => 'Please set a new password.',
);