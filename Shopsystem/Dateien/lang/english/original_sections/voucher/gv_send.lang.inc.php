<?php
/* --------------------------------------------------------------
	gv_send.lang.inc.php 2017-06-20 gm
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'current_balance' => 'Your current voucher value: ',
	'entry_amount' => 'Voucher value',
	'entry_email' => 'Recipient email address',
	'entry_message' => 'Your message to the recipient',
	'entry_name' => 'Recipient name',
	'error_entry_amount_check' => '&nbsp;&nbsp;<span class="errorText">Invalid value</span>',
	'error_entry_amount_check_NAN' => '&nbsp;&nbsp;<span class="errorText">Invalid value! Given input is not a number.</span>',
	'error_entry_amount_check_OOR' => '&nbsp;&nbsp;<span class="errorText">Invalid value! Given input must be greater than 0 and less equal then your voucher value.</span>',
	'error_entry_email_address_check' => '&nbsp;&nbsp;<span class="errorText">Invalid email address</span>',
	'heading_gvsend' => 'Send voucher',
	'heading_text' => '<br />Please complete your personal details for the voucher. For any further questions regarding vouchers, please consult the Q&As <a href="gv_faq.php">here</a>.<br />',
	'text_required' => '* required',
	'text_required_sign' => ' *',
	'text_success' => 'Congratulations, your voucher has been sent!'
);