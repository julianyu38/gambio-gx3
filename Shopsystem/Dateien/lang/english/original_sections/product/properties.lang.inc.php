<?php
/* --------------------------------------------------------------
	properties.lang.inc.php 2018-06-29
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2018 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'admin_name' => 'Admin name',
	'attention_properties_in_combis_count' => 'Attention: This property is still present in %count% combinations!',
	'attention_properties_values_in_combis_count' => 'Attention: This property value is still present in %count% combinations!',
	'gross_price' => 'Gross price',
	'name' => 'Name',
    'no_properties_message' => 'No data available',
	'price' => 'Price',
	'products_model' => 'Product model',
	'products_model_short' => 'Model',
	'properties' => 'Properties',
	'property' => 'Property',
	'property_add' => 'Add property',
	'property_delete' => 'Delete Property',
	'property_edit' => 'Edit property',
	'property_value_add' => 'Add property value',
	'property_value_delete' => 'Delete property value',
	'property_value_edit' => 'Edit property value',
	'sort_order' => 'Sort order',
	'sort_order_short' => 'Sort',
	'value' => 'Value',
	'want_to_delete_property' => 'Do you want to delete this property?',
	'want_to_delete_property_value' => 'Do you want to delete this property value?'
);