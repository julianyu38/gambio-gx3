<?php
/* --------------------------------------------------------------
	new_products.lang.inc.php 2017-05-10 gm
	Gambio GmbH
	http://www.gambio.de
	Copyright (c) 2017 Gambio GmbH
	Released under the GNU General Public License (Version 2)
	[http://www.gnu.org/licenses/gpl-2.0.html]
   --------------------------------------------------------------
*/

$t_language_text_section_content_array = array
(
	'heading_advice' => 'Our advice',
	'heading_text' => 'New product',
	'text_details' => 'Details',
    'text_no_new_products' => 'At this moment there are no new products. Come back later so you do not miss a new product.'
);