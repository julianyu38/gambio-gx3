DROP TABLE IF EXISTS `slide_images`;
CREATE TABLE IF NOT EXISTS `slide_images` (
  `slide_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `slide_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `breakpoint` enum('xs','sm','md','lg') NOT NULL DEFAULT 'lg',
  `image` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`slide_image_id`),
  UNIQUE KEY `slide_image_id` (`slide_image_id`,`slide_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;