DROP TABLE IF EXISTS `products_properties_combis_values`;
CREATE TABLE `products_properties_combis_values` (
	`products_properties_combis_values_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`products_properties_combis_id` int(10) unsigned NOT NULL DEFAULT '0',
	`properties_values_id` int(10) unsigned DEFAULT NULL,
	PRIMARY KEY (`products_properties_combis_values_id`),
	UNIQUE KEY `unique_value_assignment` (`products_properties_combis_id`,`properties_values_id`),
	KEY `products_properties_combis_values_id` (`products_properties_combis_values_id`,`products_properties_combis_id`,`properties_values_id`),
	KEY `products_properties_combis_id` (`products_properties_combis_id`,`properties_values_id`),
	KEY `properties_values_id` (`properties_values_id`,`products_properties_combis_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

INSERT INTO `products_properties_combis_values` VALUES
	(1, 1, 1),
	(2, 1, 4),
	(3, 2, 1),
	(4, 2, 5),
	(5, 3, 1),
	(6, 3, 6),
	(7, 4, 2),
	(8, 4, 4),
	(9, 5, 2),
	(10, 5, 5),
	(11, 6, 2),
	(12, 6, 6),
	(13, 7, 3),
	(14, 7, 4),
	(15, 8, 3),
	(16, 8, 5),
	(17, 9, 3),
	(18, 9, 6);