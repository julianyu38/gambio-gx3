DROP TABLE IF EXISTS `slides`;
CREATE TABLE IF NOT EXISTS `slides` (
  `slide_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL NOT NULL DEFAULT '0',
  `slider_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alt_text` varchar(255) NOT NULL DEFAULT '',
  `url` text NOT NULL,
  `url_target` varchar(16) NOT NULL DEFAULT '_self',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`slide_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;