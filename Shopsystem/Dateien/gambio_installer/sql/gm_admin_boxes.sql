DROP TABLE IF EXISTS `gm_admin_boxes`;
CREATE TABLE `gm_admin_boxes` (
	`boxes_id` int(11) NOT NULL AUTO_INCREMENT,
	`customers_id` int(11) NOT NULL DEFAULT '0',
	`box_key` varchar(64) NOT NULL DEFAULT '',
	`box_status` tinyint(4) NOT NULL DEFAULT '0',
	PRIMARY KEY (`boxes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

INSERT INTO `gm_admin_boxes` (`boxes_id`, `customers_id`, `box_key`, `box_status`) VALUES
	(530, 4, 'BOX_HEADING_GAMBIO_SEO', 1),
	(587, 4, 'BOX_HEADING_GAMBIO', 1),
	(581, 4, 'BOX_HEADING_CUSTOMERS', 1),
	(529, 4, 'BOX_HEADING_PRODUCTS', 1),
	(532, 4, 'BOX_HEADING_STATISTICS', 1),
	(589, 4, 'BOX_HEADING_MODULES', 1),
	(533, 4, 'BOX_HEADING_TOOLS', 1),
	(583, 4, 'BOX_HEADING_GV_ADMIN', 1),
	(535, 4, 'BOX_HEADING_ZONE', 1),
	(536, 4, 'BOX_HEADING_CONFIGURATION', 1),
	(394, 7, 'BOX_HEADING_GAMBIO', 1),
	(269, 7, 'BOX_HEADING_GAMBIO_SEO', 1),
	(501, 8, 'BOX_HEADING_MODULES', 1),
	(502, 8, 'BOX_HEADING_STATISTICS', 1),
	(223, 6, 'BOX_HEADING_GAMBIO', 1),
	(225, 6, 'BOX_HEADING_GAMBIO_SEO', 1),
	(320, 6, 'BOX_HEADING_CUSTOMERS', 1),
	(514, 8, 'BOX_HEADING_GAMBIO_SEO', 1),
	(372, 7, 'BOX_HEADING_PRODUCTS', 1),
	(547, 8, 'BOX_HEADING_CUSTOMERS', 1),
	(378, 7, 'BOX_HEADING_CUSTOMERS', 1),
	(306, 7, 'BOX_HEADING_ZONE', 1),
	(505, 8, 'BOX_HEADING_ZONE', 1),
	(500, 8, 'BOX_HEADING_PRODUCTS', 1),
	(366, 4, 'last', 0),
	(557, 0, 'BOX_HEADING_GAMBIO', 1),
	(508, 8, 'BOX_HEADING_CONFIGURATION', 1),
	(504, 8, 'BOX_HEADING_GV_ADMIN', 1),
	(503, 8, 'BOX_HEADING_TOOLS', 1),
	(432, 24, 'BOX_HEADING_GAMBIO_SEO', 1),
	(434, 24, 'BOX_HEADING_CUSTOMERS', 1),
	(436, 24, 'BOX_HEADING_GV_ADMIN', 1),
	(438, 24, 'BOX_HEADING_GAMBIO', 1),
	(456, 24, 'BOX_HEADING_ZONE', 1),
	(458, 24, 'BOX_HEADING_CONFIGURATION', 1),
	(450, 24, 'BOX_HEADING_STATISTICS', 1),
	(454, 24, 'BOX_HEADING_TOOLS', 1),
	(462, 24, 'BOX_HEADING_PRODUCTS', 1),
	(464, 25, 'BOX_HEADING_GAMBIO', 1),
	(510, 25, 'BOX_HEADING_CUSTOMERS', 1),
	(516, 8, 'BOX_HEADING_GAMBIO', 1),
	(541, 44, 'BOX_HEADING_GAMBIO', 1),
	(543, 44, 'BOX_HEADING_GV_ADMIN', 1),
	(545, 44, 'BOX_HEADING_STATISTICS', 1),
	(549, 44, 'BOX_HEADING_CUSTOMERS', 1),
	(597, 1, 'BOX_HEADING_PRODUCTS', 1),
	(601, 2, 'BOX_HEADING_GAMBIO', 1),
	(561, 2, 'BOX_HEADING_PRODUCTS', 1),
	(559, 2, 'BOX_HEADING_CUSTOMERS', 1),
	(565, 2, 'BOX_HEADING_GAMBIO_SEO', 1),
	(567, 2, 'BOX_HEADING_MODULES', 1),
	(569, 3, 'BOX_HEADING_GAMBIO', 1),
	(575, 2, 'BOX_HEADING_TOOLS', 1),
	(577, 3, 'BOX_HEADING_GAMBIO_SEO', 1),
	(593, 1, 'BOX_HEADING_GAMBIO_SEO', 1),
	(603, 1, 'BOX_HEADING_GAMBIO', 1);