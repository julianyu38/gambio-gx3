DROP TABLE IF EXISTS `configuration_storage`;
CREATE TABLE `configuration_storage` (
  `key` varchar(255) NOT NULL DEFAULT '',
  `value` text DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `configuration_storage` (`key`, `value`) VALUES
  ('modules/admin/responsivefilemanager/use_in_attribute_pages', '1'),
  ('modules/admin/responsivefilemanager/use_in_banner_manager_pages', '1'),
  ('modules/admin/responsivefilemanager/use_in_ckeditor', '1'),
  ('modules/admin/responsivefilemanager/use_in_content_manager_pages', '1'),
  ('modules/admin/responsivefilemanager/use_in_manufacturer_pages', '1'),
  ('modules/admin/responsivefilemanager/use_in_product_and_category_pages', '1'),
  ('modules/admin/responsivefilemanager/use_in_shipping_status_pages', '1'),
  ('modules/admin/responsivefilemanager/use_in_email_pages', '1'),
  ('modules/GambioSunnyCash/active', '1');
