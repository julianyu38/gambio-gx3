ALTER DATABASE DEFAULT CHARACTER SET 'utf8' DEFAULT COLLATE 'utf8_general_ci';

DROP TABLE IF EXISTS
`personal_offers_by_customers_status_0`,
`personal_offers_by_customers_status_1`,
`personal_offers_by_customers_status_2`,
`personal_offers_by_customers_status_3`,
`personal_offers_by_customers_status_4`,
`personal_offers_by_customers_status_5`,
`personal_offers_by_customers_status_6`,
`personal_offers_by_customers_status_7`,
`personal_offers_by_customers_status_8`,
`personal_offers_by_customers_status_9`,
`personal_offers_by_customers_status_10`,
`module_newsletter_temp_1`,
`module_newsletter_temp_2`,
`module_newsletter_temp_3`,
`module_newsletter_temp_4`,
`module_newsletter_temp_5`,
`module_newsletter_temp_6`,
`module_newsletter_temp_7`,
`module_newsletter_temp_8`,
`module_newsletter_temp_9`,
`module_newsletter_temp_10`;