DROP TABLE IF EXISTS `properties_values_description`;
CREATE TABLE `properties_values_description` (
	`properties_values_description_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`properties_values_id` int(10) unsigned NOT NULL DEFAULT '0',
	`language_id` int(10) unsigned NOT NULL DEFAULT '0',
	`values_name` varchar(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`properties_values_description_id`),
	UNIQUE KEY `unique_description` (`properties_values_id`,`language_id`),
	KEY `properties_values_description_id` (`properties_values_description_id`,`properties_values_id`,`language_id`),
	KEY `properties_values_id` (`properties_values_id`,`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

INSERT INTO `properties_values_description` VALUES
	(1, 1, 2, 'S'),
	(2, 1, 1, 'S'),
	(3, 2, 2, 'M'),
	(4, 2, 1, 'M'),
	(5, 3, 2, 'L'),
	(6, 3, 1, 'L'),
	(7, 4, 2, 'Gold'),
	(8, 4, 1, 'Gold'),
	(9, 5, 2, 'Rot'),
	(10, 5, 1, 'Red'),
	(11, 6, 2, 'Schwarz'),
	(12, 6, 1, 'Black');