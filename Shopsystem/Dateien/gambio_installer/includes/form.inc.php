<h2><?= HEADING_VERSION_INFO ?></h2>

<div class="notice_field">
    <h3>Automatischer Updater für Shop-Module</h3>
    <p>
        Mit dem neuen Updater für Shop-Module können Module bequem aktualisiert werden.<br /> Sobald neue Updates für Module
        im Shop verfügbar sind, erhalten Shopbetreiber im Gambio Admin eine Meldung über die Admin-Info-Box. In dieser
        werden alle verfügbaren Updates für die Module angezeigt und können dann per Knopfdruck installiert werden.
    </p>
    
    <h3>Danke-Gutscheine - SunnyCash Schnittstelle</h3>
    <p>
        Über das neue Danke-Gutschein-Modul können sich Kunden nach Abschluss der Bestellung einen Dankeschön-Gutschein aus
        einer vordefinierten Auswahl aussuchen. Die Gutscheine werden über die Schnittstelle zu unserem Partner SunnyCash
        (SunnyCash.de) geladen und direkt auf der Checkout-Success Seite angezeigt. Dabei werden keinerlei Kunden- oder
        Bestelldaten übertragen.<br /> Einkäufe werden somit positiv verstärkt und die Kundenbindung wird gesteigert. Das
        Modul ist standardmäßig aktiviert und kann über den Gambio Admin konfiguriert und selbstverständlich auch
        deaktiviert werden.
    </p>
    
    <h3>AMPIFY-Modul für Artikeldetailseiten</h3>
    <p>
        Mit dem AMPIFY-Modul können aus Artikeldetailsseiten in Gambio Shops nun AMP-Seiten erzeugt werden. Hierfür muss der
        Link zur XML-Sitemap des Shops oder eine CSV-Datei unter ampify.it hochgeladen werden und das Modul im Shop
        installiert sein und schon stehen die AMP-Seiten für den Shop zur Verfügung.<br /> Diese für Mobilgeräte optimierten
        Seiten können dann von Google in den Suchergebnissen gelistet und schneller angezeigt werden.
    </p>
    
    <h3>Neuer sicherer Login über 2-Faktor-Authentifizierung (OTP)</h3>
    <p>
        Erstmals steht in der Shop-Version 3.9 die 2-Faktor-Authentifizierung über so genannte “One-Time-Pads” (OTP) zur
        Verfügung. Dabei wird der Login im Shop mit Hilfe eines QR-Codes über Authenticator Apps (wie z.B. Google
        Authenticator) ermöglicht.<br /> Für den Login wird dabei ein einmal gültiges und immer neues Passwort vergeben,
        welches eine nur kurze Gültigkeit hat. Durch die Verwendung eines sich so stetig ändernden Passworts und der
        Authentifizierung mit Apphilfe wird der Login in den Shop noch sicherer, da auch von Dritten mitgeschnittene Daten
        nicht mehr helfen einen Login zu erzwingen.
    </p>
    
    <h3>Überarbeitete Rechteverwaltung für Administratoren</h3>
    <p>
        Die Rechteverwaltung für Admin-Konten wurde grundlegend überarbeitet. So können nun Rechte für Admins im Shop
        differenzierter vergeben werden und auch die Zugriffe auf Unterbereiche erlaubt oder eingeschränkt werden. Neu ist
        dabei auch die Vergabe von Rechten auf API-Funktionen. Externe Software, die Artikel einspielen und abrufen kann,
        kann so zum Beispiel, bei entsprechender Rechtevergabe, keine Bestellungen und Kunden mehr abrufen.<br /> Ebenfalls
        können Rollen mit bestimmten Admin-Rechten angelegt werden, eine Rolle ist dabei eine einfache Zusammenfassung einer
        wählbaren Auswahl von Rechten. Per Zuweisung von Rollen können dann die gewünschten Zugriffsrechte direkt und
        einfach auf neu angelegte Admin-Konten übertragen werden.
    </p>
    <p>
        Für die neue Rechteverwaltung wurde der Menüpunkt <strong>Kunden > Rollen und Berechtigungen</strong> hinzugefügt.
    </p>
    
    <h3>Mehrsprachige Rechtstexte für IT-Recht Kanzlei</h3>
    <p>
        Für Kunden der IT-Recht Kanzlei stehen nun Rechtstexte auch für folgende Sprachräume zur Verfügung:
    </p>
    <ul>
        <li>be (Weißrussisch)</li>
        <li>da (Dänisch)</li>
        <li>en (Englisch)</li>
        <li>es (Spanisch)</li>
        <li>fr (Französisch)</li>
        <li>nl (Niederländisch)</li>
        <li>sv (Schwedisch)</li>
    </ul>
    
    <h3>Neu gestaltete Artikelhersteller-Seite</h3>
    <p>
        Der Menüpunkt <strong>Artikel > Hersteller</strong> wurde technisch neu aufgesetzt und überarbeitet. Die
        Informationen für Hersteller werden nun in einem übersichtlich gestalteten PopUp eingetragen.
    </p>
    
    <h3>Tooltipps können in Bestellübersicht deaktiviert werden</h3>
    <p>
        Die Tooltipps in der Bestellübersicht können nun deaktiviert werden. Die entsprechende Einstellung befindet sich bei
        den allgemeinen Einstellungen für die Bestellübersicht. Der Aufruf der Bestellübersicht beschleunigt sich so für
        Händler, die die Zusatzinformationen in den Hovern der Übersicht nicht benötigen.
    </p>
    
    <h3>Gambio Updater und Gambio Installer im neuen Design</h3>
    <p>
        Die Oberflächen des Gambio Updaters und des Gambio Installers wurden an das neue Design des Shops angepasst und
        bieten so ein stimmiges Gesamtbild.
    </p>
    
    <h3>Vereinfachte Erstellung von Konfigurationsoberflächen für Modulautoren</h3>
    <p>
        In der Shop-Version 3.9 wurde die Erstellung von Konfigurationsoberflächen für Modulentwickler im Gambio Admin
        vereinfacht, so dass grafische Bedienoberflächen über einfache json-Dateien statt mit grösserer Programmierarbeit
        erzeugt werden können.<br /> Es ist somit nicht mehr notwendig dafür viele eigene PHP-Dateien zu erstellen, welche
        die gewünschten Oberflächen im neuen Modul abbilden. Neue Bereiche können einfach über vorhandene Standardelemente
        umgesetzt werden, die nach Zweck angepasst werden können.
    </p>
    <p>
        Hierzu zählen zum Beispiel:
    </p>
    <ul>
        <li>Eingabefelder</li>
        <li>Dropdown-Menüs</li>
        <li>Mehrfachauswahlfelder</li>
        <li>Checkboxen</li>
        <li>Länderauswahlfelder</li>
        <li>Felder zur Auswahl der Kundengruppen</li>
    </ul>
    
    <h3>Neue Services über die REST-API</h3>
    <p>
        Wir arbeiten bereits seit längerem daran externe Systeme wie Warenwirtschaften, etc. einen sauberen und
        standardisierten Zugang zu im Shop gespeicherten Daten zu bieten. Auch in diesem Entwicklungszyklus haben wir die
        Möglichkeiten, auf was im Shop zugegriffen werden kann, wieder erweitert. Wird zum Beispiel eine externe
        Versandsoftware eingesetzt, die Label für eingegangene Bestellungen erzeugt, kann diese ab nun die Trackingnummer
        für den Kunden über einen neuen Kanal zurück in den Shop schreiben, so dass der Kunde informiert werden kann und die
        Daten zusammengefasst an einer Stelle und synchron vorliegen.
    </p>
    <p>
        Neue Knotenpunkte sind hier:
    </p>
    <ul>
        <li>WithdrawalService (Widerrufe)</li>
        <li>ReviewService (Bewertungen)</li>
        <li>PacelTrackingService (Tracking im Paketversand)</li>
    </ul>
</div>
<p>
    <input type="checkbox" id="module-confirmation" /> <label for="module-confirmation"><?= LABEL_VERSION_INFO_CONFIRMATION ?></label>
</p>

<form action="index.php?language=<?php echo rawurlencode($_GET['language']); ?>">
    <input id="button_install"
           type="submit"
           name="proceed"
           value="<?= BUTTON_CONTINUE ?>"
           class="btn btn-primary btn-lg"
           disabled="">
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#button_install').prop('disabled', true);
        $('body').on('change', '#module-confirmation', function() {
            if ($(this).is(':checked')) {
                $('#button_install').prop('disabled', false);
            }
            else {
                $('#button_install').prop('disabled', true);
            }
        });
    });
</script>