<?php

namespace HubPublic\Exceptions;

/**
 * Class BadSerializerValueException
 *
 * Use this exception i.e. if the JSON string is malformed, empty or is missing information for the serializers.
 *
 * @package HubPublic\Exceptions
 */
class BadSerializerValueException extends HubWarningException
{
}