<?php

namespace HubPublic\Exceptions;

/**
 * Class HubException
 *
 * Basic exception. All other exceptions of the hub project have to be an instance of this exception.
 *
 * @package HubPublic\Exceptions
 */
class HubException extends \Exception
{
}