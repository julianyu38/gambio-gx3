<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidAuthHashException
 *
 * Use this exception i.e. if provided authorization hash is invalid or malformed.
 *
 * @package HubPublic\Exceptions
 */
class InvalidAuthHashException extends HubWarningException
{
}