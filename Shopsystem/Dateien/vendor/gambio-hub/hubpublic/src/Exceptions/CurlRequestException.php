<?php

namespace HubPublic\Exceptions;

/**
 * Class CurlRequestException
 *
 * This exception gets thrown by \HubPublic\Http\CurlRequest
 *
 * @package HubPublic\Exceptions
 */
class CurlRequestException extends HubWarningException
{
    /**
     * cURL information as returned by curl_getinfo()
     *
     * @var array
     */
    protected $curlInfo;
    /**
     * CurlRequestException constructor.
     *
     * @param string      $message  The Exception message to throw.
     * @param array|false $curlInfo The cURL information array, retrieved with "curl_getinfo".
     */
    public function __construct($message, $curlInfo)
    {
        $this->curlInfo = $curlInfo ?: [];
        parent::__construct($message);
    }
    /**
     * Returns detailed information regarding the outcome of a cURL request
     *
     * @return array of cURL data or empty array if unavailable
     */
    public function getCurlInfo()
    {
        return $this->curlInfo;
    }
}