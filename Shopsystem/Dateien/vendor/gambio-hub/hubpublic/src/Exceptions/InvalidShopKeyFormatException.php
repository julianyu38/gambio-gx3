<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidShopKeyFormatException
 *
 * Use this exception i.e. if the provided shop key is malformed.
 *
 * @package HubPublic\Exceptions
 */
class InvalidShopKeyFormatException extends HubNoticeException
{
}