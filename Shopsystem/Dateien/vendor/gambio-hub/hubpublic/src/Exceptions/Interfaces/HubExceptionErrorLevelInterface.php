<?php

namespace HubPublic\Exceptions\Interfaces;

/**
 * Interface HubExceptionErrorLevelInterface
 *
 * @package HubPublic\Exceptions\Interfaces
 */
interface HubExceptionErrorLevelInterface
{
    /**
     * Returns the exception error level.
     *
     * @return string Error level.
     */
    public function getErrorLevel();
}