<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidCollectionItemException
 *
 * Use this exception i.e. if an item, which should be added to a collection, is invalid.
 *
 * @package HubPublic\Exceptions
 */
class InvalidCollectionItemException extends HubWarningException
{
}