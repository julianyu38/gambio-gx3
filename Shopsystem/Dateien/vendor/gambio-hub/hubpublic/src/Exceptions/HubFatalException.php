<?php

namespace HubPublic\Exceptions;

use HubPublic\Exceptions\Interfaces\HubExceptionErrorLevelInterface;
/**
 * Class HubFatalException
 *
 * This exception represents a fatal hub exception. Use this exception if a fatal error occurs.
 *
 * @package HubPublic\Exceptions
 */
class HubFatalException extends HubException implements HubExceptionErrorLevelInterface
{
    /**
     * Error level.
     *
     * @var string
     */
    private $errorLevel = 'fatal';
    /**
     * Return the error level.
     *
     * @return string
     */
    public function getErrorLevel()
    {
        return $this->errorLevel;
    }
}