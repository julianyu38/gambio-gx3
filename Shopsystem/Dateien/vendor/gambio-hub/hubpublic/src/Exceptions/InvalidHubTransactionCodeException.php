<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidHubTransactionCodeException
 *
 * Use this exception i.e. if the provided hub transaction code is invalid or malformed.
 *
 * @package HubPublic\Exceptions
 */
class InvalidHubTransactionCodeException extends HubWarningException
{
}