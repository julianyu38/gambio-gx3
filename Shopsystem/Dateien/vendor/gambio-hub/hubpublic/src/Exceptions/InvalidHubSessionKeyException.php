<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidHubSessionKeyException
 *
 * Use this exception i.e. if provided hub session key is invalid or malformed.
 *
 * @package HubPublic\Exceptions
 */
class InvalidHubSessionKeyException extends InvalidKeyException
{
}