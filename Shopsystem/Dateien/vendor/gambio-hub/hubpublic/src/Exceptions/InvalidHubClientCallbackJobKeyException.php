<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidHubClientCallbackJobKeyException
 *
 * Use this exception i.e. if provided hub callback client job key is invalid or malformed.
 *
 * @package HubPublic\Exceptions
 */
class InvalidHubClientCallbackJobKeyException extends InvalidKeyException
{
}