<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidHubOrderCodeException
 *
 * Use this exception i.e. if provided hub order code is invalid or malformed.
 *
 * @package HubPublic\Exceptions
 */
class InvalidHubOrderCodeException extends InvalidKeyException
{
}