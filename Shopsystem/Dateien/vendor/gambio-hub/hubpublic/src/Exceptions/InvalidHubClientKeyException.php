<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidHubClientKeyException
 *
 * Use this exception i.e. if provided hub client key is invalid or malformed.
 *
 * @package HubPublic\Exceptions
 */
class InvalidHubClientKeyException extends HubNoticeException
{
}