<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidKeyException
 *
 * Basic key exception. All other key exceptions (e.g. the InvalidHubSessionKeyException) have to be an
 * instance of this exception.
 *
 * @package HubPublic\Exceptions
 */
class InvalidKeyException extends HubNoticeException
{
}