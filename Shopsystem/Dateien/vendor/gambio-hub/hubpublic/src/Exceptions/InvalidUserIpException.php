<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidUserIpException
 * 
 * Use this exception i.e. if a provided user IP is invalid. 
 * 
 * @package HubPublic\Exceptions
 */
class InvalidUserIpException extends HubNoticeException
{
}