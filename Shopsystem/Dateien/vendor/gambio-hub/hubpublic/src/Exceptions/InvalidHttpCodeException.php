<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidHttpCodeException
 *
 * Use this exception i.e. if a provided HTTP result code is out of range
 *
 * @package HubPublic\Exceptions
 */
class InvalidHttpCodeException extends HubWarningException
{
}