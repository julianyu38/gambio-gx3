<?php

namespace HubPublic\Exceptions;

/**
 * Class InvalidShopKeyException
 *
 * Use this exception i.e. if the provided shop key is invalid.
 *
 * @package HubPublic\Exceptions
 */
class InvalidShopKeyException extends HubNoticeException
{
}