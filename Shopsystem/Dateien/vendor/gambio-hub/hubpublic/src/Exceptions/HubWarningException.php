<?php

namespace HubPublic\Exceptions;

use HubPublic\Exceptions\Interfaces\HubExceptionErrorLevelInterface;
/**
 * Class HubWarningException
 *
 * This exception represents a warning hub exception. Use this exception, if a error occurs which is at a warning level.
 *
 * @package HubPublic\Exceptions
 */
class HubWarningException extends HubException implements HubExceptionErrorLevelInterface
{
    /**
     * Error level.
     *
     * @var string
     */
    private $errorLevel = 'warning';
    /**
     * Return the error level.
     *
     * @return string
     */
    public function getErrorLevel()
    {
        return $this->errorLevel;
    }
}