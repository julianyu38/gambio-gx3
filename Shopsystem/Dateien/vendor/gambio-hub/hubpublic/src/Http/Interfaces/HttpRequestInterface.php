<?php

namespace HubPublic\Http\Interfaces;

use HubPublic\ValueObjects\HttpResponse;
/**
 * Interface HttpRequestInterface
 *
 * @package HubPublic\Http\Interfaces
 */
interface HttpRequestInterface
{
    /**
     * Sets the url of the curl request.
     *
     * @param string $url Target address of curl request.
     *
     * @return HttpRequestInterface Same instance for chained method calls.
     */
    public function setUrl($url);
    /**
     * Sets a curl option.
     *
     * Equivalent to curl_setopt().
     *
     * @param int   $name  Name of the option. Use the PHP internal curl-constants as argument.
     * @param mixed $value Option value.
     *
     * @return HttpRequestInterface Same instance for chained method calls.
     */
    public function setOption($name, $value);
    /**
     * Executes a curl request with the given options.
     *
     * @return \HubPublic\ValueObjects\HttpResponse Response of the curl request.
     */
    public function execute();
}