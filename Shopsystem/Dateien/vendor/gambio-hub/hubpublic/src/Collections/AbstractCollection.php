<?php

namespace HubPublic\Collections;

use HubPublic\Exceptions\InvalidCollectionItemException;
/**
 * Class AbstractCollection
 *
 * @package HubPublic\Collections
 */
abstract class AbstractCollection implements \Countable
{
    /**
     * Content Collection
     *
     * @var array
     */
    protected $items = [];
    /**
     * Add an item to the collection.
     *
     * @param mixed $item Item which should be added to the collection
     *
     * @throws \HubPublic\Exceptions\InvalidCollectionItemException if item is not valid.
     *
     * @return $this|AbstractCollection Same instance for chained method calls.
     */
    public function add($item)
    {
        if (!$this->_isValid($item)) {
            throw new InvalidCollectionItemException($this->_getExceptionMessage($item));
        }
        $this->items[] = $item;
        return $this;
    }
    /**
     * Get the collection as an array.
     *
     * @return array Collection in array format
     */
    public function asArray()
    {
        return $this->items;
    }
    /**
     * Count elements of collection items.
     * @link http://php.net/manual/en/countable.count.php
     *
     * @return int The custom count as an integer.
     */
    public function count()
    {
        return count($this->items);
    }
    /**
     * Validate the item.
     *
     * @param mixed $item Item which should be validated
     *
     * @return bool true if $item is valid | false if $item is invalid
     */
    protected function _isValid($item)
    {
        return is_object($item) && is_a($item, $this->_getValidType());
    }
    /**
     * Build and return the exception message for invalid item types.
     *
     * @param mixed $item Invalid item for which the exception message is generated.
     *
     * @return string Exception message
     */
    protected function _getExceptionMessage($item)
    {
        return 'Invalid item type "' . gettype($item) . '", expected "' . $this->_getValidType() . '".';
    }
    /**
     * Return the valid type of an item.
     *
     * @return string Valid Type
     */
    protected abstract function _getValidType();
}