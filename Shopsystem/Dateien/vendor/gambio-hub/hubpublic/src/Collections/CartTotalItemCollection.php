<?php

namespace HubPublic\Collections;

/**
 * Class CartTotalItemCollection
 *
 * @package HubPublic\Collections
 */
class CartTotalItemCollection extends AbstractCollection
{
    /**
     * CartTotalItemCollection only contains CartTotalItem-objects.
     *
     * @return string String of allowed types
     */
    protected function _getValidType()
    {
        return '\\HubPublic\\ValueObjects\\CartTotalItem';
        // Use string for PHP compatibility.
    }
}