<?php

namespace HubPublic\Collections;

/**
 * Class CartItemCollection
 *
 * @package HubPublic\Collections
 */
class CartItemCollection extends AbstractCollection
{
    /**
     * CartItemCollection only contains CartItem-objects.
     *
     * @return string String of allowed types
     */
    protected function _getValidType()
    {
        return '\\HubPublic\\ValueObjects\\CartItem';
        // Use string for PHP compatibility.
    }
}