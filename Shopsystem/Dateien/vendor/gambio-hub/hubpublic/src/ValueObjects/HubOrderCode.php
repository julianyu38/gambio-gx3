<?php

namespace HubPublic\ValueObjects;

use HubPublic\Exceptions\InvalidHubOrderCodeException;
class HubOrderCode
{
    /**
     * Order code value
     *
     * @var string
     */
    private $value;
    /**
     * HubOrderCode constructor.
     *
     * @throws \HubPublic\Exceptions\InvalidHubOrderCodeException If the given HubOrderCode is not in a valid
     *                                                               format
     *
     * @param string $value Hub order code value
     */
    public function __construct($value)
    {
        if ($this->_formatIsValid($value) !== true) {
            throw new InvalidHubOrderCodeException('The given HubOrderCode is not in a valid format. The entered key was: "' . $value . '"');
        }
        $this->value = $value;
    }
    /**
     * Get the instance value as string.
     *
     * @return string HubOrderCode value as string
     */
    public function asString()
    {
        return $this->value;
    }
    /**
     * Checks if the HubOrderCode is in the correct format.
     *
     * A 64 characters long string with the format "GH-OC-[date]-[hash]-XX" whereas [date] with
     * format "YYYYMMDD" and [hash] as a random 46-character hexadecimal number.
     *
     * GH = Gambio Hub
     * CK = HubOrderCode
     * XX = represents the end of the HubOrderCode
     *
     * @param string $value The key that should be validated
     *
     * @return bool true if format is valid | false if format is invalid
     */
    private function _formatIsValid($value)
    {
        return strlen($value) === 64 && preg_match('/GH-OC-\\d{8}-[a-f0-9]{46}-XX/', $value) === 1;
    }
}