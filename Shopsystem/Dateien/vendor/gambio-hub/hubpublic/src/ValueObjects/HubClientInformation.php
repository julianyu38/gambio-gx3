<?php

namespace HubPublic\ValueObjects;

/**
 * Class HubClientInformation
 *
 * Provides information about a client of the hub
 *
 * @package HubPublic\ValueObjects
 */
class HubClientInformation
{
    /**
     * Hub client key
     *
     * @var HubClientKey
     */
    private $key;
    /**
     * Client version
     *
     * @var string;
     */
    private $version;
    /**
     * Client URL
     *
     * @var
     */
    private $url;
    /**
     * HubClientInformation constructor.
     *
     * @param \HubPublic\ValueObjects\HubClientKey $key     HubClientKey instance
     * @param string                               $version Client version
     * @param string                               $url     Client URL
     */
    public function __construct(HubClientKey $key, $version, $url)
    {
        $this->key = $key;
        $this->version = $version;
        $this->url = $url;
    }
    /**
     * Returns a hub client key.
     *
     * @return HubClientKey Hub client key
     */
    public function getClientKey()
    {
        return $this->key;
    }
    /**
     * Returns the client version.
     *
     * @return string Client version
     */
    public function getClientVersion()
    {
        return $this->version;
    }
    /**
     * Returns the client URL.
     *
     * @return string Client URL
     */
    public function getClientUrl()
    {
        return $this->url;
    }
}