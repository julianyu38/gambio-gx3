<?php

namespace HubPublic\ValueObjects\Builder;

use HubPublic\ValueObjects\CustomerInformation as CustomerInformationValueObject;
use HubPublic\ValueObjects\OrderContent as OrderContentValueObject;
/**
 * Class OrderContent
 *
 * @package HubPublic\ValueObjects\Builder
 */
class OrderContent
{
    /**
     * Customer information
     *
     * @var \HubPublic\ValueObjects\CustomerInformation
     */
    private $customer;
    /**
     * Amount
     *
     * @var float
     */
    private $amount;
    /**
     * Currency code
     *
     * @var string
     */
    private $currencyCode;
    /**
     * Language code
     *
     * @var string
     */
    private $languageCode;
    /**
     * Payment method
     *
     * @var string
     */
    private $paymentMethod;
    /**
     * Shipping method
     *
     * @var string
     */
    private $shippingMethod;
    /**
     * Customer number
     *
     * @var string
     */
    private $customerNumber;
    /**
     * Order date time
     *
     * @var \DateTime
     */
    private $orderDateTime;
    /**
     * Order number
     *
     * @var string
     */
    private $orderNumber;
    /**
     * Invoice date time
     *
     * @var \DateTime
     */
    private $invoiceDateTime;
    /**
     * Invoice number
     *
     * @var  string
     */
    private $invoiceNumber;
    /**
     * Sets the customer information
     *
     * @param \HubPublic\ValueObjects\CustomerInformation $customer CustomerInformation instance, provides customer
     *                                                              information
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setCustomer(CustomerInformationValueObject $customer)
    {
        $this->customer = $customer;
        return $this;
    }
    /**
     * Sets the amount.
     *
     * @param float $amount Amount that should be set
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }
    /**
     * Sets the currency code.
     *
     * @param string $currencyCode Code that should be set
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = trim($currencyCode);
        return $this;
    }
    /**
     * Sets the language code.
     *
     * @param string $languageCode Code that should be set
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setLanguageCode($languageCode)
    {
        $this->languageCode = trim($languageCode);
        return $this;
    }
    /**
     * Sets the payment method.
     *
     * @param string $paymentMethod Method that should be set
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = trim($paymentMethod);
        return $this;
    }
    /**
     * Sets the payment method.
     *
     * @param string $shippingMethod Method that should be set
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setShippingMethod($shippingMethod)
    {
        $this->shippingMethod = trim($shippingMethod);
        return $this;
    }
    /**
     * Sets the customer number.
     *
     * @param string $customerNumber Number that should be set
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setCustomerNumber($customerNumber = null)
    {
        $this->customerNumber = trim((string) $customerNumber);
        return $this;
    }
    /**
     * Sets the order date time.
     *
     * @param \DateTime $orderDateTime DateTime-Object that provides the date time information
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setOrderDateTime(\DateTime $orderDateTime = null)
    {
        $this->orderDateTime = $orderDateTime;
        return $this;
    }
    /**
     * Sets the order number
     *
     * @param string $orderNumber Number that should be set
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setOrderNumber($orderNumber = null)
    {
        $this->orderNumber = trim((string) $orderNumber);
        return $this;
    }
    /**
     * Sets the invoice date time
     *
     * @param \DateTime $invoiceDateTime DateTime-Object that provides the date time information
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setInvoiceDateTime(\DateTime $invoiceDateTime = null)
    {
        $this->invoiceDateTime = $invoiceDateTime;
        return $this;
    }
    /**
     * Sets the invoice number
     *
     * @param string $invoiceNumber Number that should be set
     *
     * @return \HubPublic\ValueObjects\Builder\OrderContent For chained method calls
     */
    public function setInvoiceNumber($invoiceNumber = null)
    {
        $this->invoiceNumber = trim((string) $invoiceNumber);
        return $this;
    }
    /**
     * Creates an OrderContent value object.
     *
     * @return \HubPublic\ValueObjects\OrderContent
     */
    public function build()
    {
        return new OrderContentValueObject($this->customer, $this->amount, $this->currencyCode, $this->languageCode, $this->paymentMethod, $this->shippingMethod, $this->customerNumber, $this->orderDateTime, $this->orderNumber, $this->invoiceDateTime, $this->invoiceNumber);
    }
}