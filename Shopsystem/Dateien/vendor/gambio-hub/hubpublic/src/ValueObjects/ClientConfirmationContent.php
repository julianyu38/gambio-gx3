<?php

namespace HubPublic\ValueObjects;

/**
 * Class ClientConfirmationContent
 *
 * @package HubPublic\ValueObjects
 */
class ClientConfirmationContent
{
    /**
     * Order payment instructions.
     *
     * @var string
     */
    private $orderPaymentInstructions;
    /**
     * Additional module values.
     *
     * @var array
     */
    private $additionalModuleValues;
    /**
     * ClientConfirmationContent constructor.
     *
     * @param string $orderPaymentInstructions Order payment instructions.
     * @param array  $additionalModuleValues   Additional module values.
     */
    public function __construct($orderPaymentInstructions, array $additionalModuleValues)
    {
        $this->orderPaymentInstructions = $orderPaymentInstructions;
        $this->additionalModuleValues = $additionalModuleValues;
    }
    /**
     * If available, it returns the order payment instructions. Otherwise, an empty string is returned.
     *
     * @return string
     */
    public function getOrderPaymentInstructions()
    {
        return $this->orderPaymentInstructions;
    }
    /**
     * Returns additional module values.
     *
     * @return array
     */
    public function getAdditionalModuleValues()
    {
        return $this->additionalModuleValues;
    }
}