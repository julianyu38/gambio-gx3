<?php

namespace HubPublic\ValueObjects;

/**
 * Class HttpResponse
 *
 * @package HubPublic\ValueObjects
 */
class HttpResponse
{
    /**
     * @var int
     */
    private $statusCode;
    /**
     * @var array
     */
    private $headers;
    /**
     * @var string
     */
    private $body;
    /**
     * CurlResponse constructor.
     *
     * @param int    $statusCode The response status code.
     * @param string $body       The response body.
     */
    public function __construct($statusCode, array $headers, $body)
    {
        $this->statusCode = $statusCode;
        $this->headers = $headers;
        $this->body = $body;
    }
    /**
     * Returns the HTTP status code of the cURL response.
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
    /**
     * Returns the HTTP headers of the cURL response.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }
    /**
     * Returns the body content of the cURL response.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }
}