<?php

namespace HubPublic\ValueObjects;

/**
 * Class CartTotalItem
 *
 * An auxiliary cart item, such as shipping cost, discount or surcharge
 *
 * @package HubPublic\ValueObjects
 */
class CartTotalItem
{
    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $title;
    /**
     * @var float
     */
    private $value;
    /**
     * @var float
     */
    private $changeTotal;
    /**
     * @var float
     */
    private $changeTax;
    /**
     * @var float
     */
    private $changeShippingCost;
    /**
     * CartTotalItem constructor.
     *
     * @param string $code
     * @param string $title
     * @param float  $value
     * @param float  $changeTotal
     * @param float  $changeTax
     * @param float  $changeShippingCost
     */
    public function __construct($code, $title, $value, $changeTotal, $changeTax, $changeShippingCost)
    {
        $this->code = $code;
        $this->title = $title;
        $this->value = $value;
        $this->changeTotal = $changeTotal;
        $this->changeTax = $changeTax;
        $this->changeShippingCost = $changeShippingCost;
    }
    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * @return float
     */
    public function getChangeTotal()
    {
        return $this->changeTotal;
    }
    /**
     * @return float
     */
    public function getChangeTax()
    {
        return $this->changeTax;
    }
    /**
     * @return float
     */
    public function getChangeShippingCost()
    {
        return $this->changeShippingCost;
    }
}