<?php

/* --------------------------------------------------------------
   HubClientKeySerializer.php 2017-07-14
   Gambio GmbH
   http://www.gambio.de
   Copyright © 2017 Gambio GmbH
   Released under the MIT License
   [https://opensource.org/licenses/MIT]
   --------------------------------------------------------------
*/
namespace HubPublic\Serializers;

use HubPublic\Exceptions\BadSerializerValueException;
use HubPublic\Serializers\Interfaces\SerializerInterface;
use HubPublic\ValueObjects\HubClientKey;
/**
 * Class HubClientKeySerializer
 *
 * @package HubPublic\Serializers
 */
class HubClientKeySerializer implements SerializerInterface
{
    /**
     * Deserialize a JSON string. 
     * 
     * @param string $string JSON string that contains the data.
     *
     * @return \HubPublic\ValueObjects\HubClientKey Returns the deserialized client key. 
     * 
     * @throws \HubPublic\Exceptions\BadSerializerValueException If the argument is empty or is malformed.
     */
    public function deserialize($string)
    {
        if (empty($string)) {
            throw new BadSerializerValueException('Given string is empty.');
        }
        $json = json_decode($string, true);
        // error for malformed json strings
        if ($json === null && json_last_error() > 0) {
            throw new BadSerializerValueException('Provided JSON string is malformed and could not be parsed: ' . $string);
        }
        return new HubClientKey($json['clientKey']);
    }
    /**
     * Serialize a value to a JSON string or array. 
     * 
     * @param mixed $value Content to be serialized. 
     * @param bool  $encode Defines whether to encode into JSON or return an associative array. 
     *
     * @return array|string Returns the serialized object. 
     * 
     * @throws \HubPublic\Exceptions\BadSerializerValueException If the argument is invalid.
     */
    public function serialize($value, $encode = true)
    {
        if (!is_object($value) || !$value instanceof HubClientKey) {
            throw new BadSerializerValueException('Argument is not a HubClientKey: ' . gettype($value));
        }
        $json = ['clientKey' => $value->asString()];
        return $encode ? json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) : $json;
    }
}