<?php

namespace HubPublic\Serializers;

use HubPublic\Exceptions\BadSerializerValueException;
use HubPublic\Serializers\Interfaces\SerializerInterface;
use HubPublic\ValueObjects\ClientSessionInformation;
use HubPublic\ValueObjects\HubSessionKey;
/**
 * Class ClientSessionInformationSerializer
 *
 * @package HubPublic\Serializers
 */
class ClientSessionInformationSerializer implements SerializerInterface
{
    /**
     * Deserialize a JSON string.
     *
     * @param string $string JSON string that contains the data.
     *
     * @return \HubPublic\ValueObjects\ClientSessionInformation New ClientSessionInformation instance that contains the
     *                                                          deserialized data
     * @throws \HubPublic\Exceptions\BadSerializerValueException If provided JSON string is malformed.
     */
    public function deserialize($string)
    {
        if (empty($string)) {
            throw new BadSerializerValueException('Given string is empty.');
        }
        $json = json_decode($string, true);
        // error for malformed json strings
        if ($json === null && json_last_error() > 0) {
            throw new BadSerializerValueException('Provided JSON string is malformed and could not be parsed: ' . $string);
        }
        $json['languageCode'] = filter_var($json['languageCode'], FILTER_SANITIZE_STRING);
        $json['currency'] = filter_var($json['currency'], FILTER_SANITIZE_STRING);
        $json['userAgent'] = filter_var($json['userAgent'], FILTER_SANITIZE_STRING);
        return new ClientSessionInformation(new HubSessionKey($json['sessionKey']), $json['languageCode'], $json['currency'], $json['userIp'], $json['userAgent']);
    }
    /**
     * Serialize a value to a JSON string or array.
     *
     * @param \HubPublic\ValueObjects\ClientSessionInformation $value  Content to be serialized.
     * @param bool                                             $encode Serialize to string?
     *
     * @throws \HubPublic\Exceptions\BadSerializerValueException if $value is not an object or is not an instance of
     *                                                          ClientSessionInformation
     * @return array|string Serialized JSON string or array of given content.
     */
    public function serialize($value, $encode = true)
    {
        if (!is_object($value) || !$value instanceof ClientSessionInformation) {
            throw new BadSerializerValueException('Argument is not a ClientSessionInformation: ' . gettype($value));
        }
        $json = ['sessionKey' => $value->getHubSessionKey()->asString(), 'languageCode' => $value->getLanguageCode(), 'currency' => $value->getCurrencyCode(), 'userIp' => $value->getUserIp(), 'userAgent' => $value->getUserAgent()];
        return $encode ? json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) : $json;
    }
}