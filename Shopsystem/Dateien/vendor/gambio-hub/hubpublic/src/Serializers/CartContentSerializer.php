<?php

namespace HubPublic\Serializers;

use HubPublic\Collections\CartItemCollection;
use HubPublic\Collections\CartTotalItemCollection;
use HubPublic\ValueObjects\CartContent;
use HubPublic\ValueObjects\CartItem;
use HubPublic\Exceptions\BadSerializerValueException;
use HubPublic\Serializers\Interfaces\SerializerInterface;
use HubPublic\ValueObjects\CartTotalItem;
/**
 * Class CartContentSerializer
 *
 * @package HubPublic\Serializers
 */
class CartContentSerializer implements SerializerInterface
{
    /**
     * Deserialize a JSON string.
     *
     * @param string $string JSON string that contains the data.
     *
     * @return \HubPublic\ValueObjects\CartContent CartContent instance that contains the deserialized data
     * @throws \HubPublic\Exceptions\BadSerializerValueException If JSON string is malformed.
     */
    public function deserialize($string)
    {
        if (empty($string)) {
            throw new BadSerializerValueException('Given string is empty.');
        }
        $json = json_decode($string, true);
        // error for malformed json strings
        if ($json === null && json_last_error() > 0) {
            throw new BadSerializerValueException('Provided JSON string is malformed and could not be parsed: ' . $string);
        }
        $cartItemCollection = $this->_getCartItemCollection($json);
        $cartTotalItemCollection = $this->_getCartTotalItemCollection($json);
        return new CartContent($cartItemCollection, (double) filter_var($json['totalPrice'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), (double) filter_var($json['shippingCost'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), $cartTotalItemCollection);
    }
    /**
     * Serialize a value to a JSON string or array.
     *
     * @param \HubPublic\ValueObjects\CartContent $cartContent Content to be serialized.
     * @param bool                                $encode      Serialize to string?
     *
     * @return array|string Serialized JSON string or array of given content.
     *
     * @throws \HubPublic\Exceptions\BadSerializerValueException If passed argument is not a CartContent.
     */
    public function serialize($cartContent, $encode = true)
    {
        if (!is_object($cartContent) || !$cartContent instanceof CartContent) {
            throw new BadSerializerValueException('Argument is not a CartContent: ' . gettype($cartContent));
        }
        $json = ['cartItems' => $this->_serializeCartItems($cartContent->getCartItemCollection()), 'totalPrice' => $cartContent->getTotalPrice(), 'shippingCost' => $cartContent->getShippingCost(), 'cartTotalItems' => $this->_serializeCartTotalItems($cartContent->getCartTotalItemCollection())];
        return $encode ? json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) : $json;
    }
    /**
     * Create a CartItemCollection from json cart item data.
     *
     * @param array $json Array of cart items
     *
     * @return \HubPublic\Collections\CartItemCollection CartItemCollection instance that contains the deserialized data
     * @throws \HubPublic\Exceptions\BadSerializerValueException If cart item information are invalid
     */
    private function _getCartItemCollection(array $json)
    {
        $cartItemCollection = new CartItemCollection();
        foreach ($json['cartItems'] as $cartItem) {
            $ean = filter_var($cartItem['ean'], FILTER_SANITIZE_STRING);
            $name = str_replace(['<', '>'], '', $cartItem['name']);
            $price = (double) filter_var($cartItem['price'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $quantity = (double) filter_var($cartItem['quantity'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $attributes = [];
            $imageUrl = filter_var($cartItem['imageUrl'], FILTER_SANITIZE_URL);
            $categoryPath = filter_var($cartItem['categoryPath'], FILTER_SANITIZE_STRING);
            $mpn = filter_var($cartItem['mpn'], FILTER_SANITIZE_STRING);
            $productUrl = filter_var($cartItem['productUrl'], FILTER_SANITIZE_URL);
            $quantityUnit = filter_var($cartItem['quantityUnit'], FILTER_SANITIZE_STRING);
            $reference = filter_var($cartItem['reference'], FILTER_SANITIZE_STRING);
            $type = (string) (int) filter_var($cartItem['type'], FILTER_SANITIZE_STRING);
            $brand = filter_var($cartItem['brand'], FILTER_SANITIZE_STRING);
            $tax = (double) filter_var($cartItem['tax'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            if (array_key_exists('attributes', $cartItem)) {
                $attributes = [];
                foreach ($cartItem['attributes'] as $key => $values) {
                    $attributes[$key] = str_replace(['<', '>'], '', $values);
                }
            }
            $cartItemCollection->add(new CartItem($ean, $name, $price, $quantity, $attributes, $imageUrl, $categoryPath, $mpn, $productUrl, $quantityUnit, $reference, $type, $brand, $tax));
        }
        return $cartItemCollection;
    }
    private function _getCartTotalItemCollection($json)
    {
        $cartTotalItemCollection = new CartTotalItemCollection();
        if (array_key_exists('cartTotalItems', $json) && is_array($json['cartTotalItems'])) {
            foreach ($json['cartTotalItems'] as $cartTotalItem) {
                $code = filter_var($cartTotalItem['code'], FILTER_SANITIZE_STRING);
                $title = filter_var($cartTotalItem['title'], FILTER_SANITIZE_STRING);
                $value = (double) filter_var($cartTotalItem['value'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                $changeTotal = (double) filter_var($cartTotalItem['changeTotal'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                $changeTax = (double) filter_var($cartTotalItem['changeTax'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                $changeShippingCost = (double) filter_var($cartTotalItem['changeShippingCost'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                $cartTotalItemCollection->add(new CartTotalItem($code, $title, $value, $changeTotal, $changeTax, $changeShippingCost));
            }
        }
        return $cartTotalItemCollection;
    }
    /**
     * Serialize the CartItem collection.
     *
     * @param \HubPublic\Collections\CartItemCollection $collection
     *
     * @return array
     */
    private function _serializeCartItems(CartItemCollection $collection)
    {
        $json = [];
        /** @var CartItem $cartItem */
        foreach ($collection->asArray() as $cartItem) {
            $json[] = ['ean' => $cartItem->getEan(), 'name' => $cartItem->getName(), 'price' => $cartItem->getPrice(), 'quantity' => $cartItem->getQuantity(), 'attributes' => $cartItem->getAttributes(), 'imageUrl' => $cartItem->getImageUrl(), 'categoryPath' => $cartItem->getCategoryPath(), 'mpn' => $cartItem->getMpn(), 'productUrl' => $cartItem->getProductUrl(), 'quantityUnit' => $cartItem->getQuantityUnit(), 'reference' => $cartItem->getReference(), 'type' => $cartItem->getType(), 'brand' => $cartItem->getBrand(), 'tax' => $cartItem->getTax()];
        }
        return $json;
    }
    /**
     * Serializes a CartTotalItemCollection.
     *
     * @param \HubPublic\Collections\CartTotalItemCollection $totalItems
     *
     * @return array
     */
    private function _serializeCartTotalItems(CartTotalItemCollection $totalItems)
    {
        $json = [];
        /** @var \HubPublic\ValueObjects\CartTotalItem $totalItem */
        foreach ($totalItems->asArray() as $totalItem) {
            $json[] = ['code' => $totalItem->getCode(), 'title' => $totalItem->getTitle(), 'value' => $totalItem->getValue(), 'changeTotal' => $totalItem->getChangeTotal(), 'changeTax' => $totalItem->getChangeTax(), 'changeShippingCost' => $totalItem->getChangeShippingCost()];
        }
        return $json;
    }
}