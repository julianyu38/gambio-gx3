<?php

namespace HubPublic\Serializers;

use HubPublic\Serializers\Interfaces\SerializerInterface;
use HubPublic\ValueObjects\OrderContent;
use HubPublic\Exceptions\BadSerializerValueException;
/**
 * Class OrderContentSerializer
 *
 * @package HubPublic\Serializers
 */
class OrderContentSerializer implements SerializerInterface
{
    /**
     * Customer serializer instance
     *
     * @var \HubPublic\Serializers\CustomerInformationSerializer
     */
    private $customerInformationSerializer;
    /**
     * OrderContentSerializer constructor.
     *
     * @param \HubPublic\Serializers\CustomerInformationSerializer $customerInformationSerializer
     */
    public function __construct(CustomerInformationSerializer $customerInformationSerializer)
    {
        $this->customerInformationSerializer = $customerInformationSerializer;
    }
    /**
     * Deserialize a JSON string.
     *
     * @param string $string JSON string that contains the data.
     *
     * @return \HubPublic\ValueObjects\OrderContent New OrderContent instance that contains the deserialized data
     * @throws \HubPublic\Exceptions\BadSerializerValueException If provided JSON string is malformed or provides
     *                                                          invalid values.
     */
    public function deserialize($string)
    {
        if (empty($string)) {
            throw new BadSerializerValueException('Given string is empty.');
        }
        $json = json_decode($string, true);
        // error for malformed json strings
        if ($json === null && json_last_error() > 0) {
            throw new BadSerializerValueException('Provided JSON string is malformed and could not be parsed: ' . $string);
        }
        $customer = $this->customerInformationSerializer->deserialize(json_encode($json['customer']));
        return new OrderContent($customer, (double) filter_var($json['amount'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), filter_var($json['currencyCode'], FILTER_SANITIZE_STRING), filter_var($json['languageCode'], FILTER_SANITIZE_STRING), filter_var($json['paymentMethod'], FILTER_SANITIZE_STRING), filter_var($json['shippingMethod'], FILTER_SANITIZE_STRING), filter_var($json['customerNumber'], FILTER_SANITIZE_STRING), new \DateTime($json['orderDateTime']), filter_var($json['orderNumber'], FILTER_SANITIZE_STRING), new \DateTime($json['invoiceDate']), filter_var($json['invoiceNumber'], FILTER_SANITIZE_STRING));
    }
    /**
     * Serialize a value to a JSON string or array.
     *
     * @param \HubPublic\ValueObjects\OrderContent $orderContent OrderContent to be serialized.
     * @param bool                                 $encode       Serialize to string?
     *
     * @return array|string Serialized JSON string or array of given content.
     *
     * @throws \HubPublic\Exceptions\BadSerializerValueException if $orderContent is not an object or is not an instance
     *                                                          of OrderContent
     */
    public function serialize($orderContent, $encode = true)
    {
        if (!is_object($orderContent) || !$orderContent instanceof OrderContent) {
            throw new BadSerializerValueException('Argument is not a OrderContent: ' . gettype($orderContent));
        }
        $orderDateTime = $orderContent->getOrderDateTime();
        $invoiceDateTime = $orderContent->getInvoiceDateTime();
        $json = ['customer' => $this->customerInformationSerializer->serialize($orderContent->getCustomer(), $encode), 'amount' => $orderContent->getAmount(), 'currencyCode' => $orderContent->getCurrencyCode(), 'languageCode' => $orderContent->getLanguageCode(), 'paymentMethod' => $orderContent->getPaymentMethod(), 'shippingMethod' => $orderContent->getShippingMethod(), 'customerNumber' => $orderContent->getCustomerNumber(), 'orderDateTime' => $orderDateTime->format('Y-m-d H:i:s'), 'orderNumber' => $orderContent->getOrderNumber(), 'invoiceDate' => $invoiceDateTime->format('Y-m-d H:i:s'), 'invoiceNumber' => $orderContent->getInvoiceNumber()];
        return $encode ? json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) : $json;
    }
}