<?php

namespace HubPublic\Serializers;

use HubPublic\Serializers\Interfaces\SerializerInterface;
use HubPublic\Exceptions\BadSerializerValueException;
use HubPublic\ValueObjects\HubClientInformation;
use HubPublic\ValueObjects\HubClientKey;
/**
 * Class HubClientInformationSerializer
 *
 * @package HubPublic\Serializers
 */
class HubClientInformationSerializer implements SerializerInterface
{
    /**
     * Deserialize a JSON string.
     *
     * @param string $string JSON string that contains the data.
     *
     * @return \HubPublic\ValueObjects\HubClientInformation New HubClientInformationString that contains the
     *                                                         deserialized data
     * @throws \HubPublic\Exceptions\BadSerializerValueException If the argument is empty or is missing information
     */
    public function deserialize($string)
    {
        if (empty($string)) {
            throw new BadSerializerValueException('Given string is empty.');
        }
        $json = json_decode($string, true);
        // error for malformed json strings
        if ($json === null && json_last_error() > 0) {
            throw new BadSerializerValueException('Provided JSON string is malformed and could not be parsed: ' . $string);
        }
        $json['version'] = filter_var($json['version'], FILTER_SANITIZE_STRING);
        $json['url'] = filter_var($json['url'], FILTER_SANITIZE_URL);
        return new HubClientInformation(new HubClientKey($json['key']), $json['version'], $json['url']);
    }
    /**
     * Serialize a value to a JSON string or array.
     *
     * @param HubClientInformation $hubClientInformation Content to be serialized.
     * @param bool                 $encode               Serialize to string?
     *
     * @return array|string Serialized JSON string or array of given content.
     *
     * @throws \HubPublic\Exceptions\BadSerializerValueException If passed argument is not a
     *                                                          HubPaymentModuleDescriptionCollection
     */
    public function serialize($hubClientInformation, $encode = true)
    {
        if (!is_object($hubClientInformation) || !$hubClientInformation instanceof HubClientInformation) {
            throw new BadSerializerValueException('Argument is not a HubClientInformation: ' . gettype($hubClientInformation));
        }
        $json = ['key' => $hubClientInformation->getClientKey()->asString(), 'version' => $hubClientInformation->getClientVersion(), 'url' => $hubClientInformation->getClientUrl()];
        return $encode ? json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) : $json;
    }
}