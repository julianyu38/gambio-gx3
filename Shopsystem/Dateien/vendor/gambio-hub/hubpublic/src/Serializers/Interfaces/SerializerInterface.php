<?php

namespace HubPublic\Serializers\Interfaces;

/**
 * Interface DeserializeInterface
 *
 * @package HubPublic\Serializers\Interfaces
 */
interface SerializerInterface
{
    /**
     * Serialize a value to a JSON string, or to an array if $encode is set to false.
     *
     * @param mixed $value  Content to be serialized.
     * @param bool  $encode Serialize to string?
     */
    public function serialize($value, $encode = true);
    /**
     * Deserialize a JSON string.
     *
     * @param string $string JSON string that contains the data.
     */
    public function deserialize($string);
}