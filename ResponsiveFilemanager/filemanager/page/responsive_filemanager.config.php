<?php
/* --------------------------------------------------------------
 responsive_filemanager.config.php 2017-09-22
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * These keys will overwrite the existing config keys from the responsive file manager.
 *
 * In order to use this config file, a 'page' GET parameter has to be added to the responsive
 * file manager URL like this:
 *     ...&page=responsive_filemanager.
 */
return array(
	'delete_files'   => true,
	'hidden_files'   => [
		'PayPal-SDK-LICENSE.txt',
		'GPL-LIZENZUEBERSETZUNG.txt',
		'GPL-LICENSE.txt'
	],
	'delete_folders' => true,
	'rename_folders' => true,
	'hidden_folders' => [
		'actindo',
		'admin',
		'cache',
		'callback',
		'debug',
		'ext',
		'gambio_installer',
		'gambio_updater',
		'gm',
		'GProtector',
		'GXEngine',
		'GXMainComponents',
		'GXModules',
		'GXUserComponents',
		'inc',
		'includes',
		'JSEngine',
		'lang',
		'lettr',
		'logfiles',
		'PdfCreator',
		'pub',
		'public',
		'ResponsiveFilemanager',
		'shopgate',
		'StyleEdit',
		'StyleEdit3',
		'system',
		'templates',
		'templates_c',
		'vendor',
		'version_info'
	],
	'create_folders' => true
);
