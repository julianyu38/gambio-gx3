<?php
/* --------------------------------------------------------------
 content_manager_product_contents.config.php 2017-09-21
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

/**
 * These keys will overwrite the existing config keys from the responsive file manager.
 *
 * In order to use this config file, a 'page' GET parameter has to be added to the responsive
 * file manager URL like this:
 *     ...&page=content_manager_product_contents.
 */

// Add PHP to the allowed file extensions.
return array(
	
	// The ext array, is created in the config.php as the last statement of the file.
	$ext[] = 'php'
);
