<?php
/* --------------------------------------------------------------
 filemanager.php 2017-09-19
 Gambio GmbH
 http://www.gambio.de
 Copyright (c) 2017 Gambio GmbH
 Released under the GNU General Public License (Version 2)
 [http://www.gnu.org/licenses/gpl-2.0.html]
 --------------------------------------------------------------
 */

// Import the customer status, so we can check if the customer has admin rights.
$currentDirectory = getcwd();
define('SUPPRESS_REDIRECT', true);
chdir('../../admin/');
require_once('includes/application_top.php');
chdir($currentDirectory);

// Do nothing if the user has no admin rights.
if ($_SESSION['customers_status']['customers_status_id'] !== '0')
{
    die('Access not allowed');
}

if (session_status() !== PHP_SESSION_ACTIVE)
{
	session_start();
}

// finally, we just include the file managers dialog.php
include './dialog.php';
